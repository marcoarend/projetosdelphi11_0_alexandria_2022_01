object DModG: TDModG
  Height = 756
  Width = 1146
  PixelsPerInch = 96
  object Tb_Empresas: TMySQLTable
    Database = Dmod.ZZDB
    TableName = '_empresas_'
    Left = 384
    Top = 20
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    DataSet = Tb_Empresas
    Left = 384
    Top = 72
  end
  object MySyncDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 20
    Top = 20
  end
  object MyLocDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 96
    Top = 23
  end
  object DBDmk: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 156
    Top = 20
  end
  object MyPID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 228
    Top = 20
  end
  object AllID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 464
    Top = 16
  end
end
