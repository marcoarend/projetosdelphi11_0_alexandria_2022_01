object DModG: TDModG
  Height = 756
  Width = 1146
  PixelsPerInch = 96
  object Tb_Empresas: TMySQLTable
    Database = Dmod.ZZDB
    TableName = '_empresas_'
    Left = 384
    Top = 20
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    DataSet = Tb_Empresas
    Left = 384
    Top = 72
  end
end
