program DACxFRX_3;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  Module in 'Module.pas' {Dmod: TDataModule},
  ModuleGeral in 'ModuleGeral.pas' {DModG: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
