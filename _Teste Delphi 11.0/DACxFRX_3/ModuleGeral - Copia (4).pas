﻿unit ModuleGeral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, frxClass, frxDBSet, stdctrls, Variants, Menus,
  ExtCtrls, Registry, TypInfo, Grids, DBGrids, ShellApi, Vcl.Buttons, Vcl.ToolWin;

type
  THackDBGrid = class(TDBGrid);
  THackStringGrid = class(TStringGrid);
  TDModG = class(TDataModule)
    QrUpdPID1: TMySQLQuery;
    QrUpdL: TMySQLQuery;
    QrAux_: TMySQLQuery;
    DsSituacao: TDataSource;
    QrUpdPID2: TMySQLQuery;
    QrAuxPID1: TMySQLQuery;
    QrTermiServ: TMySQLQuery;
    QrTermiServSerialKey: TWideStringField;
    QrTermiServSerialNum: TWideStringField;
    QrDefEnti: TMySQLQuery;
    QrDefEntiCodigo: TIntegerField;
    QrDefEntiFilial: TIntegerField;
    QrDefEntiCliInt: TIntegerField;
    QrDefEntiNO_ENT: TWideStringField;
    QrParOrfRep: TMySQLQuery;
    QrParOrfRepData: TDateField;
    QrParOrfRepCarteira: TIntegerField;
    QrParOrfRepControle: TIntegerField;
    QrParOrfRepDescricao: TWideStringField;
    QrParOrfRepCredito: TFloatField;
    QrParOrfRepVencimento: TDateField;
    QrParOrfRepCompensado: TDateField;
    QrParOrfRepFatNum: TFloatField;
    QrParOrfRepFatParcela: TIntegerField;
    QrParOrfRepMez: TIntegerField;
    QrParOrfRepCliente: TIntegerField;
    QrParOrfRepCliInt: TIntegerField;
    QrParOrfRepForneceI: TIntegerField;
    QrParOrfRepDepto: TIntegerField;
    QrParOrfRepAtrelado: TIntegerField;
    DsParOrfRep: TDataSource;
    Tb_Empresas: TMySQLTable;
    Tb_EmpresasCodCliInt: TIntegerField;
    Tb_EmpresasCodEnti: TIntegerField;
    Tb_EmpresasCodFilial: TIntegerField;
    Tb_EmpresasNome: TWideStringField;
    Tb_EmpresasAtivo: TSmallintField;
    Ds_Empresas: TDataSource;
    Tb_Indi_Pags: TMySQLTable;
    Tb_Indi_PagsCodigo: TIntegerField;
    Tb_Indi_PagsNome: TWideStringField;
    Tb_Indi_PagsAtivo: TSmallintField;
    Ds_Indi_Pags: TDataSource;
    QrAllUpd: TMySQLQuery;
    QrAllAux: TMySQLQuery;
    MySyncDB: TMySQLDatabase;
    MyLocDB: TMySQLDatabase;
    DBDmk: TMySQLDatabase;
    MyPID_DB: TMySQLDatabase;
    AllID_DB: TMySQLDatabase;
    RV_CEP_DB: TMySQLDatabase;
    QrRV_CEP: TMySQLQuery;
  private
    { Private declarations }
  public
    { Public declarations }

end;

var
  DModG: TDModG;

implementation


{$R *.DFM}


end.

