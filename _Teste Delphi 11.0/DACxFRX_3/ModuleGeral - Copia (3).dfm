object DModG: TDModG
  Height = 756
  Width = 1146
  PixelsPerInch = 96
  object QrUpdPID1: TMySQLQuery
    Database = MyPID_DB
    Left = 800
    Top = 52
  end
  object QrUpdL: TMySQLQuery
    Database = RV_CEP_DB
    Left = 672
    Top = 52
  end
  object QrAux_: TMySQLQuery
    Database = RV_CEP_DB
    Left = 748
    Top = 116
  end
  object DsSituacao: TDataSource
    Left = 672
    Top = 104
  end
  object QrLocCI: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, PercJuros, PercMulta, VTCBBNITAR'
      'FROM cond'
      'WHERE Cliente=:P0'
      '')
    Left = 820
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCIPercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrLocCIPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrLocCIVTCBBNITAR: TFloatField
      FieldName = 'VTCBBNITAR'
    end
  end
  object QrUpdPID2: TMySQLQuery
    Database = RV_CEP_DB
    Left = 800
    Top = 100
  end
  object QrLocLogr: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM listalograd'
      'WHERE Nome=:P0'
      '')
    Left = 820
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocLogrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLancto: TMySQLQuery
    Database = RV_CEP_DB
    Left = 820
    Top = 388
    object QrLanctoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLanctoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLanctoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLanctoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLanctoNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLanctoNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrLanctoData: TDateField
      FieldName = 'Data'
    end
    object QrLanctoVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLanctoBanco1: TIntegerField
      FieldName = 'Banco1'
    end
  end
  object QrPPI: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet,'
      'ppi.Texto, ppi.Lancto, ppi.Docum'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR'
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR'
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'AND ptk.Controle=:P0'
      'ORDER BY DataD, Conta'
      '')
    Left = 816
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPPINO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPPINO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPPILimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPISaiu: TIntegerField
      FieldName = 'Saiu'
      MaxValue = 1
    end
    object QrPPIDataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRecebeu: TIntegerField
      FieldName = 'Recebeu'
      MaxValue = 1
    end
    object QrPPIDataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRetornou: TIntegerField
      FieldName = 'Retornou'
      MaxValue = 1
    end
    object QrPPIDataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIDataSai_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataSai_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIDataRec_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRec_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIDataRet_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRet_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPPIRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPPITexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrPPILancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
    end
  end
  object QrPTK: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT ptc.Nome, ptk.Controle Lote, ptk.Mez'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR '
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR '
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY ptk.Mez, ptk.Controle')
    Left = 816
    Top = 172
    object QrPTKNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPTKLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrPTKMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPTKMes_TXT: TWideStringField
      DisplayWidth = 5
      FieldKind = fkCalculated
      FieldName = 'Mes_TXT'
      Size = 5
      Calculated = True
    end
  end
  object DsPTK: TDataSource
    DataSet = QrPTK
    Left = 844
    Top = 172
  end
  object DsPPI: TDataSource
    DataSet = QrPPI
    Left = 844
    Top = 220
  end
  object QrPKA: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT  ptc.Nome, ptc.Codigo Lote, ptk.Mez,'
      'ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ('
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY DataD, Conta'
      '')
    Left = 816
    Top = 272
    object QrPKANome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPKALote: TIntegerField
      FieldName = 'Lote'
      Required = True
    end
    object QrPKAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPKADataE: TDateField
      FieldName = 'DataE'
    end
    object QrPKAConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPKARetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPKANO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPKANO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPKALimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKASaiu: TIntegerField
      FieldName = 'Saiu'
    end
    object QrPKADataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKALimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKARecebeu: TIntegerField
      FieldName = 'Recebeu'
    end
    object QrPKADataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKALimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKARetornou: TIntegerField
      FieldName = 'Retornou'
    end
    object QrPKADataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKADataSai_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataSai_TXT'
      Size = 8
      Calculated = True
    end
    object QrPKADataRec_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRec_TXT'
      Size = 8
      Calculated = True
    end
    object QrPKADataRet_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRet_TXT'
      Size = 8
      Calculated = True
    end
  end
  object DsPKA: TDataSource
    DataSet = QrPKA
    Left = 844
    Top = 272
  end
  object QrAuxPID1: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    Top = 100
  end
  object QrTermiServ: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SerialKey, SerialNum'
      'FROM terminais'
      'WHERE Servidor = 1'
      'ORDER BY Terminal')
    Left = 612
    Top = 52
    object QrTermiServSerialKey: TWideStringField
      FieldName = 'SerialKey'
      Size = 32
    end
    object QrTermiServSerialNum: TWideStringField
      FieldName = 'SerialNum'
      Size = 32
    end
  end
  object QrDefEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Filial, CliInt'
      'FROM entidades '
      'WHERE Codigo=:P0')
    Left = 736
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDefEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefEntiFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrDefEntiCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDefEntiNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrParOrfRep: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    object QrParOrfRepData: TDateField
      DisplayWidth = 12
      FieldName = 'Data'
    end
    object QrParOrfRepCarteira: TIntegerField
      DisplayWidth = 12
      FieldName = 'Carteira'
    end
    object QrParOrfRepControle: TIntegerField
      DisplayWidth = 12
      FieldName = 'Controle'
    end
    object QrParOrfRepDescricao: TWideStringField
      DisplayWidth = 57
      FieldName = 'Descricao'
      Size = 100
    end
    object QrParOrfRepCredito: TFloatField
      DisplayWidth = 12
      FieldName = 'Credito'
    end
    object QrParOrfRepVencimento: TDateField
      DisplayWidth = 12
      FieldName = 'Vencimento'
    end
    object QrParOrfRepCompensado: TDateField
      DisplayWidth = 13
      FieldName = 'Compensado'
    end
    object QrParOrfRepFatNum: TFloatField
      DisplayWidth = 12
      FieldName = 'FatNum'
    end
    object QrParOrfRepFatParcela: TIntegerField
      DisplayWidth = 12
      FieldName = 'FatParcela'
    end
    object QrParOrfRepMez: TIntegerField
      DisplayWidth = 12
      FieldName = 'Mez'
    end
    object QrParOrfRepCliente: TIntegerField
      DisplayWidth = 12
      FieldName = 'Cliente'
    end
    object QrParOrfRepCliInt: TIntegerField
      DisplayWidth = 12
      FieldName = 'CliInt'
    end
    object QrParOrfRepForneceI: TIntegerField
      DisplayWidth = 12
      FieldName = 'ForneceI'
    end
    object QrParOrfRepDepto: TIntegerField
      DisplayWidth = 12
      FieldName = 'Depto'
    end
    object QrParOrfRepAtrelado: TIntegerField
      DisplayWidth = 12
      FieldName = 'Atrelado'
    end
  end
  object DsParOrfRep: TDataSource
    DataSet = QrParOrfRep
    Left = 864
    Top = 48
  end
  object Tb_Empresas: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_empresas_'
    Left = 960
    Top = 4
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    DataSet = Tb_Empresas
    Left = 960
    Top = 56
  end
  object Tb_Indi_Pags: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_indi_pags_'
    Left = 964
    Top = 104
    object Tb_Indi_PagsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Tb_Indi_PagsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_Indi_PagsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Indi_Pags: TDataSource
    DataSet = Tb_Indi_Pags
    Left = 964
    Top = 156
  end
  object QrEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Tipo, IE, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENTI,'
      'IF(Tipo=0, EUF, PUF) + 0.000  UF_Cod,'
      'IF(Tipo=0, ECodMunici, PCodMunici)  + 0.000 CodMunici,'
      'IF(Tipo=0, ECodiPais, PCodiPais) + 0.000 CodiPais'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 960
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiUF_Cod: TFloatField
      FieldName = 'UF_Cod'
      Required = True
    end
    object QrEntiNO_ENTI: TWideStringField
      FieldName = 'NO_ENTI'
      Size = 100
    end
    object QrEntiCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
    object QrEntiCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
  end
  object QrEntiCliInt: TMySQLQuery
    Database = RV_CEP_DB
    Left = 960
    Top = 264
  end
  object QrControle: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 924
    Top = 404
  end
  object QrPerfis: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM PerfisIts pit'
      
        'LEFT JOIN PerfisItsPerf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 988
    Top = 404
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrUTC: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SYSDATE() DATA_LOC, '
      'UTC_TIMESTAMP() DATA_UTC, '
      'SYSDATE()- UTC_TIMESTAMP() UTC_Dif')
    Left = 960
    Top = 320
    object QrUTCDATA_LOC: TDateTimeField
      FieldName = 'DATA_LOC'
      Required = True
    end
    object QrUTCDATA_UTC: TDateTimeField
      FieldName = 'DATA_UTC'
      Required = True
    end
    object QrUTCUTC_Dif: TFloatField
      FieldName = 'UTC_Dif'
      Required = True
    end
  end
  object QrAllUpd: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 56
  end
  object QrAllAux: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 104
  end
  object QrEntNFS: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Tipo, ent.IE, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ELograd, ent.PLograd) + 0.000 Lograd,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) Rua,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 Numero,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) Compl,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) Bairro,'
      
        'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CodMunici' +
        ','
      'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000  UF_Cod,'
      'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CodiPais,'
      'IF(ent.Tipo=0, ent.ECEP, ent.PCEP) + 0.000 CEP,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TE1,'
      'ent.NIRE,'
      'llo.Nome NO_Lograd'
      'FROM entidades ent'
      'LEFT JOIN listalograd llo ON llo.Codigo='
      '  IF(ent.Tipo=0, ent.PLograd, ent.ELograd)'
      'WHERE ent.Codigo=:P0'
      '')
    Left = 1012
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntNFSTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntNFSIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntNFSCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntNFSCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntNFSNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEntNFSRua: TWideStringField
      FieldName = 'Rua'
      Size = 60
    end
    object QrEntNFSNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntNFSCompl: TWideStringField
      FieldName = 'Compl'
      Size = 60
    end
    object QrEntNFSBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 60
    end
    object QrEntNFSUF_Cod: TFloatField
      FieldName = 'UF_Cod'
      Required = True
    end
    object QrEntNFSCodiPais: TFloatField
      FieldName = 'CodiPais'
      Required = True
    end
    object QrEntNFSNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEntNFSNO_Lograd: TWideStringField
      FieldName = 'NO_Lograd'
      Size = 15
    end
    object QrEntNFSTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntNFSLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrEntNFSCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntNFSCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
  end
  object QrSelCods: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM _selcods_'
      '')
    Left = 960
    Top = 458
    object QrSelCodsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSelCodsNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrSelCodsNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrSelCodsNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrSelCodsNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrSelCodsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSelCodsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
      MaxValue = 1
    end
  end
  object DsSelCods: TDataSource
    DataSet = QrSelCods
    Left = 1020
    Top = 458
  end
  object QrProvisorio1: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ece.Codigo NULO, ect.Codigo, ect.Controle '
      'FROM enticontat ect'
      'LEFT JOIN enticonent ece ON '
      '  ece.Controle=ect.Controle'
      '  AND ece.Codigo=ect.Codigo'
      'WHERE ece.Codigo IS Null'
      '')
    Left = 880
    Top = 336
    object QrProvisorio1NULO: TIntegerField
      FieldName = 'NULO'
      Required = True
    end
    object QrProvisorio1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProvisorio1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrWebParams: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesgerl')
    Left = 1048
    Top = 292
  end
  object MySyncDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 596
    Top = 4
  end
  object MyLocDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 672
    Top = 7
  end
  object DBDmk: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 732
    Top = 4
  end
  object MyPID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 804
    Top = 4
  end
  object AllID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 1040
  end
  object RV_CEP_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 644
    Top = 540
  end
  object QrRV_CEP: TMySQLQuery
    Database = RV_CEP_DB
    Left = 644
    Top = 592
  end
end
