﻿unit ModuleGeral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, frxClass, frxDBSet, stdctrls, Variants, Menus,
  ExtCtrls, Registry, TypInfo, Grids, DBGrids, ShellApi, Vcl.Buttons, Vcl.ToolWin;

type
  THackDBGrid = class(TDBGrid);
  THackStringGrid = class(TStringGrid);
  TDModG = class(TDataModule)
    QrUpdPID1: TMySQLQuery;
    QrUpdL: TMySQLQuery;
    QrAux_: TMySQLQuery;
    DsSituacao: TDataSource;
    QrLocCI: TMySQLQuery;
    QrLocCICodigo: TIntegerField;
    QrLocCIPercJuros: TFloatField;
    QrLocCIPercMulta: TFloatField;
    QrLocCIVTCBBNITAR: TFloatField;
    QrUpdPID2: TMySQLQuery;
    QrLocLogr: TMySQLQuery;
    QrLocLogrCodigo: TIntegerField;
    QrLancto: TMySQLQuery;
    QrLanctoDebito: TFloatField;
    QrLanctoCredito: TFloatField;
    QrLanctoFornecedor: TIntegerField;
    QrLanctoCliente: TIntegerField;
    QrLanctoNOMECLIENTE: TWideStringField;
    QrLanctoNOMEFORNECEDOR: TWideStringField;
    QrLanctoData: TDateField;
    QrLanctoVencimento: TDateField;
    QrLanctoBanco1: TIntegerField;
    QrPPI: TMySQLQuery;
    QrPPIDataE: TDateField;
    QrPPINO_RETORNA: TWideStringField;
    QrPPINO_Motivo: TWideStringField;
    QrPPILimiteSai: TDateField;
    QrPPISaiu: TIntegerField;
    QrPPIDataSai: TDateTimeField;
    QrPPILimiteRem: TDateField;
    QrPPIRecebeu: TIntegerField;
    QrPPIDataRec: TDateTimeField;
    QrPPILimiteRet: TDateField;
    QrPPIRetornou: TIntegerField;
    QrPPIDataRet: TDateTimeField;
    QrPPIDataSai_TXT: TWideStringField;
    QrPPIDataRec_TXT: TWideStringField;
    QrPPIDataRet_TXT: TWideStringField;
    QrPPIConta: TIntegerField;
    QrPPIRetorna: TSmallintField;
    QrPPITexto: TWideStringField;
    QrPPILancto: TIntegerField;
    QrPPIDocum: TFloatField;
    QrPTK: TMySQLQuery;
    QrPTKNome: TWideStringField;
    QrPTKLote: TIntegerField;
    QrPTKMez: TIntegerField;
    QrPTKMes_TXT: TWideStringField;
    DsPTK: TDataSource;
    DsPPI: TDataSource;
    QrPKA: TMySQLQuery;
    QrPKANome: TWideStringField;
    QrPKALote: TIntegerField;
    QrPKAMez: TIntegerField;
    QrPKADataE: TDateField;
    QrPKAConta: TIntegerField;
    QrPKARetorna: TSmallintField;
    QrPKANO_RETORNA: TWideStringField;
    QrPKANO_Motivo: TWideStringField;
    QrPKALimiteSai: TDateField;
    QrPKASaiu: TIntegerField;
    QrPKADataSai: TDateTimeField;
    QrPKALimiteRem: TDateField;
    QrPKARecebeu: TIntegerField;
    QrPKADataRec: TDateTimeField;
    QrPKALimiteRet: TDateField;
    QrPKARetornou: TIntegerField;
    QrPKADataRet: TDateTimeField;
    QrPKADataSai_TXT: TWideStringField;
    QrPKADataRec_TXT: TWideStringField;
    QrPKADataRet_TXT: TWideStringField;
    DsPKA: TDataSource;
    QrAuxPID1: TMySQLQuery;
    QrTermiServ: TMySQLQuery;
    QrTermiServSerialKey: TWideStringField;
    QrTermiServSerialNum: TWideStringField;
    QrDefEnti: TMySQLQuery;
    QrDefEntiCodigo: TIntegerField;
    QrDefEntiFilial: TIntegerField;
    QrDefEntiCliInt: TIntegerField;
    QrDefEntiNO_ENT: TWideStringField;
    QrParOrfRep: TMySQLQuery;
    QrParOrfRepData: TDateField;
    QrParOrfRepCarteira: TIntegerField;
    QrParOrfRepControle: TIntegerField;
    QrParOrfRepDescricao: TWideStringField;
    QrParOrfRepCredito: TFloatField;
    QrParOrfRepVencimento: TDateField;
    QrParOrfRepCompensado: TDateField;
    QrParOrfRepFatNum: TFloatField;
    QrParOrfRepFatParcela: TIntegerField;
    QrParOrfRepMez: TIntegerField;
    QrParOrfRepCliente: TIntegerField;
    QrParOrfRepCliInt: TIntegerField;
    QrParOrfRepForneceI: TIntegerField;
    QrParOrfRepDepto: TIntegerField;
    QrParOrfRepAtrelado: TIntegerField;
    DsParOrfRep: TDataSource;
    Tb_Empresas: TMySQLTable;
    Tb_EmpresasCodCliInt: TIntegerField;
    Tb_EmpresasCodEnti: TIntegerField;
    Tb_EmpresasCodFilial: TIntegerField;
    Tb_EmpresasNome: TWideStringField;
    Tb_EmpresasAtivo: TSmallintField;
    Ds_Empresas: TDataSource;
    Tb_Indi_Pags: TMySQLTable;
    Tb_Indi_PagsCodigo: TIntegerField;
    Tb_Indi_PagsNome: TWideStringField;
    Tb_Indi_PagsAtivo: TSmallintField;
    Ds_Indi_Pags: TDataSource;
    QrEnti: TMySQLQuery;
    QrEntiTipo: TSmallintField;
    QrEntiIE: TWideStringField;
    QrEntiCNPJ: TWideStringField;
    QrEntiCPF: TWideStringField;
    QrEntiUF_Cod: TFloatField;
    QrEntiNO_ENTI: TWideStringField;
    QrEntiCodMunici: TFloatField;
    QrEntiCodiPais: TFloatField;
    QrEntiCliInt: TMySQLQuery;
    QrControle: TMySQLQuery;
    QrPerfis: TMySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrUTC: TMySQLQuery;
    QrUTCDATA_LOC: TDateTimeField;
    QrUTCDATA_UTC: TDateTimeField;
    QrUTCUTC_Dif: TFloatField;
    QrAllUpd: TMySQLQuery;
    QrAllAux: TMySQLQuery;
    QrEntNFS: TMySQLQuery;
    QrEntNFSTipo: TSmallintField;
    QrEntNFSIE: TWideStringField;
    QrEntNFSCNPJ: TWideStringField;
    QrEntNFSCPF: TWideStringField;
    QrEntNFSNO_ENT: TWideStringField;
    QrEntNFSRua: TWideStringField;
    QrEntNFSNumero: TFloatField;
    QrEntNFSCompl: TWideStringField;
    QrEntNFSBairro: TWideStringField;
    QrEntNFSUF_Cod: TFloatField;
    QrEntNFSCodiPais: TFloatField;
    QrEntNFSNIRE: TWideStringField;
    QrEntNFSNO_Lograd: TWideStringField;
    QrEntNFSTE1: TWideStringField;
    QrEntNFSLograd: TFloatField;
    QrEntNFSCEP: TFloatField;
    QrEntNFSCodMunici: TFloatField;
    QrSelCods: TMySQLQuery;
    QrSelCodsNivel1: TIntegerField;
    QrSelCodsNivel2: TIntegerField;
    QrSelCodsNivel3: TIntegerField;
    QrSelCodsNivel4: TIntegerField;
    QrSelCodsNivel5: TIntegerField;
    QrSelCodsNome: TWideStringField;
    QrSelCodsAtivo: TSmallintField;
    DsSelCods: TDataSource;
    QrProvisorio1: TMySQLQuery;
    QrProvisorio1NULO: TIntegerField;
    QrProvisorio1Codigo: TIntegerField;
    QrProvisorio1Controle: TIntegerField;
    QrWebParams: TMySQLQuery;
    MySyncDB: TMySQLDatabase;
    MyLocDB: TMySQLDatabase;
    DBDmk: TMySQLDatabase;
    MyPID_DB: TMySQLDatabase;
    AllID_DB: TMySQLDatabase;
    RV_CEP_DB: TMySQLDatabase;
    QrRV_CEP: TMySQLQuery;
  private
    { Private declarations }
  public
    { Public declarations }

end;

var
  DModG: TDModG;

implementation


{$R *.DFM}


end.

