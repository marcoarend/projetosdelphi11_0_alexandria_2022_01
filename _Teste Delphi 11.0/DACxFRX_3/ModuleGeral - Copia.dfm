object DModG: TDModG
  Height = 756
  Width = 1146
  PixelsPerInch = 96
  object QrSCE: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,'
      'sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,'
      'gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,'
      'cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,'
      'pl.Codigo CODIGOplano, pl.Nome NOMEplano,'
      ''
      'pls.PerAnt PL_PEA, pls.PerAtu PL_PED, '
      'pls.SdoAtu PL_ATU, pls.SdoFut PL_FUT, '
      ''
      'cjs.PerAnt CJ_PEA, cjs.PerAtu CJ_PED, '
      'cjs.SdoAtu CJ_ATU, cjs.SdoFut CJ_FUT, '
      ''
      'grs.PerAnt GR_PEA, grs.PerAtu GR_PED, '
      'grs.SdoAtu GR_ATU, grs.SdoFut GR_FUT, '
      ''
      'sgs.PerAnt SG_PEA, sgs.PerAtu SG_PED,'
      'sgs.SdoAtu SG_ATU, sgs.SdoFut SG_FUT,'
      ''
      'cos.PerAnt CO_PEA, cos.PerAtu CO_PED,'
      'cos.SdoAtu CO_ATU, cos.SdoFut CO_FUT,'
      ''
      'pl.CtrlaSdo PL_CS, cj.CtrlaSdo CJ_CS, '
      'gr.CtrlaSdo GR_CS, sg.CtrlaSdo SG_CS, '
      'co.CtrlaSdo CO_CS'
      ''
      'FROM plano pl'
      'LEFT JOIN conjuntos cj ON pl.Codigo=cj.plano'
      'LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo'
      'LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo'
      ''
      
        'LEFT JOIN planosdo  pls ON pls.Codigo=pl.Codigo AND pls.Entidade' +
        '=:P0'
      
        'LEFT JOIN conjunsdo cjs ON cjs.Codigo=cj.Codigo AND cjs.Entidade' +
        '=:P1'
      
        'LEFT JOIN grupossdo grs ON grs.Codigo=gr.Codigo AND grs.Entidade' +
        '=:P2'
      
        'LEFT JOIN subgrusdo sgs ON sgs.Codigo=sg.Codigo AND sgs.Entidade' +
        '=:P3'
      
        'LEFT JOIN contassdo cos ON cos.Codigo=co.Codigo AND cos.Entidade' +
        '=:P4'
      ''
      'WHERE (co.Ativo in ( "V" ) OR co.Ativo IS NULL)'
      'AND (co.Codigo>0 OR co.Codigo IS NULL)'
      'AND ( (cos.PerAnt <> 0) OR (cos.PerAtu <> 0) '
      '  OR (cos.SdoAtu <> 0) OR (cos.SdoFut <> 0) )'
      'ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo')
    Left = 432
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSCECODIGOCONTA: TIntegerField
      FieldName = 'CODIGOCONTA'
    end
    object QrSCENOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrSCECODIGOSUBGRUPO: TIntegerField
      FieldName = 'CODIGOSUBGRUPO'
    end
    object QrSCENOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrSCECODIGOGRUPO: TIntegerField
      FieldName = 'CODIGOGRUPO'
    end
    object QrSCENOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrSCECODIGOCONJUNTO: TIntegerField
      FieldName = 'CODIGOCONJUNTO'
    end
    object QrSCENOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrSCECODIGOplano: TIntegerField
      FieldName = 'CODIGOplano'
      Required = True
    end
    object QrSCENOMEplano: TWideStringField
      FieldName = 'NOMEplano'
      Required = True
      Size = 50
    end
    object QrSCEPL_ATU: TFloatField
      FieldName = 'PL_ATU'
    end
    object QrSCEPL_FUT: TFloatField
      FieldName = 'PL_FUT'
    end
    object QrSCECJ_ATU: TFloatField
      FieldName = 'CJ_ATU'
    end
    object QrSCECJ_FUT: TFloatField
      FieldName = 'CJ_FUT'
    end
    object QrSCEGR_ATU: TFloatField
      FieldName = 'GR_ATU'
    end
    object QrSCEGR_FUT: TFloatField
      FieldName = 'GR_FUT'
    end
    object QrSCESG_ATU: TFloatField
      FieldName = 'SG_ATU'
    end
    object QrSCESG_FUT: TFloatField
      FieldName = 'SG_FUT'
    end
    object QrSCECO_ATU: TFloatField
      FieldName = 'CO_ATU'
    end
    object QrSCECO_FUT: TFloatField
      FieldName = 'CO_FUT'
    end
    object QrSCEPL_CS: TSmallintField
      FieldName = 'PL_CS'
      Required = True
    end
    object QrSCECJ_CS: TSmallintField
      FieldName = 'CJ_CS'
    end
    object QrSCEGR_CS: TSmallintField
      FieldName = 'GR_CS'
    end
    object QrSCESG_CS: TSmallintField
      FieldName = 'SG_CS'
    end
    object QrSCECO_CS: TSmallintField
      FieldName = 'CO_CS'
    end
    object QrSCEPL_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PL_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEPL_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PL_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECJ_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CJ_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECJ_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CJ_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEGR_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GR_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEGR_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GR_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCESG_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SG_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCESG_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SG_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECO_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CO_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECO_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CO_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEPL_PEA: TFloatField
      FieldName = 'PL_PEA'
    end
    object QrSCEPL_PED: TFloatField
      FieldName = 'PL_PED'
    end
    object QrSCECJ_PEA: TFloatField
      FieldName = 'CJ_PEA'
    end
    object QrSCECJ_PED: TFloatField
      FieldName = 'CJ_PED'
    end
    object QrSCEGR_PEA: TFloatField
      FieldName = 'GR_PEA'
    end
    object QrSCEGR_PED: TFloatField
      FieldName = 'GR_PED'
    end
    object QrSCESG_PEA: TFloatField
      FieldName = 'SG_PEA'
    end
    object QrSCESG_PED: TFloatField
      FieldName = 'SG_PED'
    end
    object QrSCECO_PEA: TFloatField
      FieldName = 'CO_PEA'
    end
    object QrSCECO_PED: TFloatField
      FieldName = 'CO_PED'
    end
    object QrSCEPL_PEA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PL_PEA_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEPL_PED_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PL_PED_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECJ_PEA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CJ_PEA_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECJ_PED_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CJ_PED_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEGR_PEA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GR_PEA_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCEGR_PED_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GR_PED_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCESG_PEA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SG_PEA_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCESG_PED_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SG_PED_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECO_PEA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CO_PEA_TXT'
      Size = 50
      Calculated = True
    end
    object QrSCECO_PED_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CO_PED_TXT'
      Size = 50
      Calculated = True
    end
  end
  object frxDsSCE: TfrxDBDataset
    UserName = 'frxDsSCE'
    CloseDataSource = False
    DataSet = QrSCE
    BCDToCurrency = False
    
    Left = 432
    Top = 48
  end
  object QrCST: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(SdoAtu) SdoAtu, SUM(SdoFut) SdoFut, '
      'SUM(PerAnt) PerAnt, SUM(PerAtu) PerAtu '
      'FROM contassdo'
      'WHERE Entidade=:P0'
      '')
    Left = 476
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCSTSdoAtu: TFloatField
      FieldName = 'SdoAtu'
    end
    object QrCSTSdoFut: TFloatField
      FieldName = 'SdoFut'
    end
    object QrCSTPerAnt: TFloatField
      FieldName = 'PerAnt'
    end
    object QrCSTPerAtu: TFloatField
      FieldName = 'PerAtu'
    end
  end
  object frxDsCST: TfrxDBDataset
    UserName = 'frxDsCST'
    CloseDataSource = False
    DataSet = QrCST
    BCDToCurrency = False
    
    Left = 476
    Top = 48
  end
  object QrMaster: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT cm.Dono, cm.Versao, ma.*'
      'FROM controle cm'
      'LEFT JOIN entidades te ON te.Codigo=cm.Dono'
      'LEFT JOIN master ma ON ma.CNPJ=te.CNPJ')
    Left = 12
    Top = 8
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrMasterSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
    object QrMasterMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMasterMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Size = 30
    end
    object QrMasterMonitorar: TSmallintField
      FieldName = 'Monitorar'
    end
    object QrMasterLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrMasterDistorcao: TIntegerField
      FieldName = 'Distorcao'
    end
    object QrMasterDataI: TDateField
      FieldName = 'DataI'
    end
    object QrMasterDataF: TDateField
      FieldName = 'DataF'
    end
    object QrMasterHoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMasterHora: TTimeField
      FieldName = 'Hora'
    end
    object QrMasterMasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMasterSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
    object QrMasterChekF: TWideStringField
      FieldName = 'ChekF'
      Size = 32
    end
    object QrMasterDiasExtra: TIntegerField
      FieldName = 'DiasExtra'
    end
    object QrMasterChekMonit: TWideStringField
      FieldName = 'ChekMonit'
      Size = 32
    end
    object QrMasterLicTel: TWideStringField
      FieldName = 'LicTel'
      Size = 40
    end
    object QrMasterLicMail: TWideStringField
      FieldName = 'LicMail'
      Size = 255
    end
    object QrMasterLicInfo: TWideStringField
      FieldName = 'LicInfo'
      Size = 32
    end
    object QrMasterAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMasterAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMasterHabilModulos: TWideMemoField
      FieldName = 'HabilModulos'
      BlobType = ftWideMemo
    end
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    
    Left = 68
    Top = 8
  end
  object frxDsDono: TfrxDBDataset
    UserName = 'frxDsDono'
    CloseDataSource = False
    DataSet = QrDono
    BCDToCurrency = False
    
    Left = 68
    Top = 56
  end
  object QrDepto: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Unidade NOMEDEPTO'
      'FROM condimov '
      'WHERE Conta=:P0')
    Left = 8
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDeptoNOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Required = True
      Size = 10
    end
  end
  object QrEndereco: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1,'
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT,'
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT,'
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF,'
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG,'
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_,'
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA,'
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      '/*'
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO,'
      '*/'
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL,'
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO,'
      'IF(en.Tipo=0, en.ECidade    , en.PCidade) CIDADE,'
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD,'
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF,'
      'IF(en.Tipo=0, en.EPais      , en.PPais  ) Pais,'
      '/*'
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd,'
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP,'
      '*/'
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1,'
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX,'
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL,'
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO,'
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF,'
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI,'
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS,'
      ''
      'RG, SSP, DataRG'
      'FROM entidades en'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 8
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderecoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEnderecoCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrEnderecoNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEnderecoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderecoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEnderecoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEnderecoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderecoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderecoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderecoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEnderecoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderecoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderecoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEnderecoTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEnderecoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderecoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderecoENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrEnderecoPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrEnderecoCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEnderecoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEnderecoE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEnderecoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrEnderecoNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEnderecoNO_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEnderecoENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEnderecoPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEnderecoNUMERO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'NUMERO'
      Calculated = True
    end
    object QrEnderecoELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEnderecoPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEnderecoLOGRAD: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'LOGRAD'
      Calculated = True
    end
    object QrEnderecoECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEnderecoPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEnderecoCEP: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CEP'
      Calculated = True
    end
    object QrEnderecoE_MIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_MIN'
      Size = 255
      Calculated = True
    end
    object QrEnderecoEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEnderecoRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEnderecoSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEnderecoDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEnderecoTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEnderecoENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEnderecoCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrEnderecoCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrEnderecoSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrEnderecoRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 60
    end
  end
  object frxDsEndereco: TfrxDBDataset
    UserName = 'frxDsEndereco'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Cadastro=Cadastro'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'NIRE_=NIRE_'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'Tipo=Tipo'
      'TE1=TE1'
      'FAX=FAX'
      'ENatal=ENatal'
      'PNatal=PNatal'
      'CEP_TXT=CEP_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'FAX_TXT=FAX_TXT'
      'TE1_TXT=TE1_TXT'
      'NATAL_TXT=NATAL_TXT'
      'Respons1=Respons1'
      'NO_2_ENT=NO_2_ENT'
      'NO_TIPO_DOC=NO_TIPO_DOC'
      'ENumero=ENumero'
      'PNumero=PNumero'
      'NUMERO=NUMERO'
      'ELograd=ELograd'
      'PLograd=PLograd'
      'LOGRAD=LOGRAD'
      'ECEP=ECEP'
      'PCEP=PCEP'
      'CEP=CEP'
      'E_MIN=E_MIN'
      'EMAIL=EMAIL'
      'RG=RG'
      'SSP=SSP'
      'DataRG=DataRG'
      'TRATO=TRATO'
      'ENDEREF=ENDEREF'
      'CODMUNICI=CODMUNICI'
      'CODPAIS=CODPAIS'
      'SITE=SITE')
    DataSet = QrEndereco
    BCDToCurrency = False
    
    Left = 68
    Top = 104
  end
  object QrPerf: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pip.Libera, pip.Olha, pip.Inclui, '
      'pip.Altera, pip.Exclui, pit.Janela'
      'FROM perfjan pit'
      'LEFT JOIN perfits pip ON pip.Janela=pit.Janela'
      'LEFT JOIN senhas sen ON sen.Perfil=pip.Codigo'
      'WHERE sen.Numero=:P0')
    Left = 128
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfOlha: TSmallintField
      FieldName = 'Olha'
    end
    object QrPerfInclui: TSmallintField
      FieldName = 'Inclui'
    end
    object QrPerfAltera: TSmallintField
      FieldName = 'Altera'
    end
    object QrPerfExclui: TSmallintField
      FieldName = 'Exclui'
    end
    object Qrperfjanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 30
    end
  end
  object Qrperfjan: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Janela, Nome, Descricao '
      'FROM perfjan'
      'WHERE Janela=:P0')
    Left = 128
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrperfjanJanela: TWideStringField
      FieldName = 'Janela'
      Size = 30
    end
    object QrperfjanNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrperfjanDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrSB: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome, 0 CodUsu'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 188
    Top = 3
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TWideStringField
      DisplayWidth = 255
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 255
    end
    object QrSBCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 248
    Top = 3
  end
  object QrSB2: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 188
    Top = 47
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftWideString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrSB2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 248
    Top = 47
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 248
    Top = 95
  end
  object QrSB3: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 188
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrSB3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrSB4: TMySQLQuery
    Database = RV_CEP_DB
    Left = 188
    Top = 143
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSB4CodUsu: TWideStringField
      FieldName = 'CodUsu'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 248
    Top = 143
  end
  object QrEndereco2: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1,'
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOME_EN' +
        'T, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO_2_EN' +
        'T, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      '/* '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      '*/'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      '/*'
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad,'
      'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,'
      '*/'
      'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,'
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 24
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEndereco2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEndereco2Cadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrEndereco2NOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEndereco2CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndereco2IE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndereco2NIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEndereco2RUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEndereco2COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndereco2BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndereco2CIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEndereco2NOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndereco2NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndereco2Pais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEndereco2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEndereco2TE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEndereco2FAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndereco2ENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrEndereco2PNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrEndereco2Respons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrEndereco2CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEndereco2NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEndereco2E_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEndereco2CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEndereco2FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEndereco2TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEndereco2NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEndereco2NO_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEndereco2ENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEndereco2PNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEndereco2NUMERO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'NUMERO'
      Calculated = True
    end
    object QrEndereco2ELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEndereco2PLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEndereco2LOGRAD: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'LOGRAD'
      Calculated = True
    end
    object QrEndereco2ECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEndereco2PCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEndereco2CEP: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CEP'
      Calculated = True
    end
    object QrEndereco2SITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
  end
  object QrEmpresas: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial, ent.CliInt, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE'
      'FROM senhasits sei'
      'LEFT JOIN entidades ent ON ent.Codigo=sei.Empresa'
      'WHERE sei.Numero=:P0'
      'AND ent.Codigo < -10')
    Left = 24
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresasNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrEmpresasCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresasIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresasNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 96
    Top = 244
  end
  object QrDuplCodUsu: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Nome'
      'FROM graatrcad')
    Left = 88
    Top = 348
    object QrDuplCodUsuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrUsuCot: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM cambiousu usu'
      'WHERE Codigo=:P0')
    Left = 24
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrTemCot: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM cambiocot'
      'WHERE DataC=:P0')
    Left = 24
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrEntrega: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOME_EN' +
        'T,'
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F,'
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG,'
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_,'
      'en.LRua RUA,'
      'en.LNumero NUMERO,'
      'en.LCompl  COMPL,'
      'en.LBairro BAIRRO,'
      'en.LCidade  CIDADE,'
      'llo.Nome   NOMELOGRAD,'
      'ufs.Nome   NOMEUF,'
      'en.LPais   Pais,'
      'en.LLograd Lograd,'
      'en.LTel    TE1,'
      'en.LCEP    CEP,'
      'en.LFax     FAX'
      'FROM entidades en'
      'LEFT JOIN ufs ufs ON ufs.Codigo=en.LUF'
      'LEFT JOIN listalograd llo ON llo.Codigo=en.LLograd'
      'WHERE en.Codigo=:P0')
    Left = 168
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
      Origin = 'entidades.L_Ativo'
    end
    object QrEntregaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntregaCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntregaENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntregaPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntregaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntregaRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEntregaNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEntregaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntregaIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntregaNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEntregaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntregaNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEntregaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEntregaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntregaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEntregaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEntregaLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEntregaCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntregaE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEntregaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrEntAtrIts: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT eac.Nome NOMEATRIBCAD, eai.controle,'
      'eai.Codigo, eai.CodUsu, eai.Nome NOMEATRIBITS'
      'FROM entatrits eai'
      'LEFT JOIN entatrcad eac ON eac.Codigo=eai.Codigo'
      'WHERE eac.Codigo IN ('
      '  SELECT eda.EntAtrCad'
      '  FROM entdefatr eda'
      '  WHERE eda.Entidade=1'
      ')'
      'ORDER BY NOMEATRIBCAD, NOMEATRIBITS, eai.Codigo, eai.controle')
    Left = 320
    Top = 8
    object QrEntAtrItsNOMEATRIBCAD: TWideStringField
      FieldName = 'NOMEATRIBCAD'
      Size = 30
    end
    object QrEntAtrItscontrole: TIntegerField
      FieldName = 'controle'
      Required = True
    end
    object QrEntAtrItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntAtrItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEntAtrItsNOMEATRIBITS: TWideStringField
      FieldName = 'NOMEATRIBITS'
      Required = True
      Size = 50
    end
  end
  object QrEntAtrDef: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT eda.controle '
      'FROM entdefatr eda'
      'WHERE Entidade=:P0'
      'AND eda.EntAtrCad=:P1'#10
      '')
    Left = 320
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntAtrDefcontrole: TIntegerField
      FieldName = 'controle'
      Required = True
    end
  end
  object QrNexInt: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT MAX(OriCtrl) Codigo'
      'FROM stqmovits'
      'WHERE Tipo=0')
    Left = 388
    Top = 316
    object QrNexIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrAgora: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 20
    Top = 447
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrGetUFX: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT uf.Nome'
      'FROM ufs uf '
      
        'LEFT JOIN entidades ent ON IF(ent.Tipo=0,ent.EUF,ent.PUF)=uf.Cod' +
        'igo'
      'WHERE ent.Codigo=:P0'#10
      #10)
    Left = 188
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGetUFXNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object QrAux: TMySQLQuery
    Database = RV_CEP_DB
    Left = 80
    Top = 448
  end
  object QrFiliLog: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Filial, '
      'IF(Tipo=0,RazaoSocial,Nome) NomeEmp'
      'FROM entidades'
      'WHERE Codigo < -10'
      'AND Filial in (1,2,3,4,5)')
    Left = 220
    Top = 400
    object QrFiliLogCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliLogFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliLogNomeEmp: TWideStringField
      FieldName = 'NomeEmp'
      Required = True
      Size = 100
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = RV_CEP_DB
    AllowSequenced = False
    AutoCalcFields = False
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, emp.* '
      'FROM paramsemp emp'
      'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo'
      'WHERE emp.Codigo=:P0'
      '')
    Left = 288
    Top = 540
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpUF_DistDFeInt: TWideStringField
      DisplayWidth = 10
      FieldName = 'UF_DistDFeInt'
      Origin = 'paramsemp.UF_DistDFeInt'
      Size = 10
    end
    object QrParamsEmpSimplesEst: TSmallintField
      FieldName = 'SimplesEst'
      Origin = 'paramsemp.SimplesEst'
    end
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'paramsemp.Codigo'
    end
    object QrParamsEmpMoeda: TIntegerField
      FieldName = 'Moeda'
      Origin = 'paramsemp.Moeda'
    end
    object QrParamsEmpSituacao: TIntegerField
      FieldName = 'Situacao'
      Origin = 'paramsemp.Situacao'
    end
    object QrParamsEmpRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'paramsemp.RegrFiscal'
    end
    object QrParamsEmpFretePor: TSmallintField
      FieldName = 'FretePor'
      Origin = 'paramsemp.FretePor'
    end
    object QrParamsEmpTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Origin = 'paramsemp.TipMediaDD'
    end
    object QrParamsEmpFatSemPrcL: TSmallintField
      FieldName = 'FatSemPrcL'
      Origin = 'paramsemp.FatSemPrcL'
    end
    object QrParamsEmpBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
      Origin = 'paramsemp.BalQtdItem'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
      Origin = 'paramsemp.Associada'
    end
    object QrParamsEmpFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
    end
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
      Origin = 'paramsemp.AssocModNF'
    end
    object QrParamsEmpCtaProdVen: TIntegerField
      FieldName = 'CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrParamsEmpfaturasep: TWideStringField
      FieldName = 'faturasep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrParamsEmpfaturaseq: TSmallintField
      FieldName = 'faturaseq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrParamsEmpFaturaDta: TSmallintField
      FieldName = 'FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrParamsEmpTxtProdVen: TWideStringField
      FieldName = 'TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrParamsEmpLogo3x1: TWideStringField
      FieldName = 'Logo3x1'
      Origin = 'paramsemp.Logo3x1'
      Size = 255
    end
    object QrParamsEmpTipCalcJuro: TSmallintField
      FieldName = 'TipCalcJuro'
      Origin = 'paramsemp.TipCalcJuro'
    end
    object QrParamsEmpCtaServico: TIntegerField
      FieldName = 'CtaServico'
      Origin = 'paramsemp.CtaServico'
    end
    object QrParamsEmpTxtServico: TWideStringField
      FieldName = 'TxtServico'
      Origin = 'paramsemp.TxtServico'
      Size = 100
    end
    object QrParamsEmpDupServico: TWideStringField
      FieldName = 'DupServico'
      Origin = 'paramsemp.DupServico'
      Size = 3
    end
    object QrParamsEmpCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
      Origin = 'paramsemp.CtaFretPrest'
    end
    object QrParamsEmpTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Origin = 'paramsemp.TxtFretPrest'
      Size = 100
    end
    object QrParamsEmpDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Origin = 'paramsemp.DupFretPrest'
      Size = 3
    end
    object QrParamsEmpDupProdVen: TWideStringField
      FieldName = 'DupProdVen'
      Origin = 'paramsemp.DupProdVen'
      Size = 3
    end
    object QrParamsEmpPedVdaMudLista: TSmallintField
      FieldName = 'PedVdaMudLista'
      Origin = 'paramsemp.PedVdaMudLista'
    end
    object QrParamsEmpPedVdaMudPrazo: TSmallintField
      FieldName = 'PedVdaMudPrazo'
      Origin = 'paramsemp.PedVdaMudPrazo'
    end
    object QrParamsEmpFisFax: TWideStringField
      FieldName = 'FisFax'
      Origin = 'paramsemp.FisFax'
      Size = 30
    end
    object QrParamsEmpFisContNom: TWideStringField
      FieldName = 'FisContNom'
      Origin = 'paramsemp.FisContNom'
      Size = 28
    end
    object QrParamsEmpFisContTel: TWideStringField
      FieldName = 'FisContTel'
      Origin = 'paramsemp.FisContTel'
      Size = 30
    end
    object QrParamsEmpSINTEGRA_Path: TWideStringField
      FieldName = 'SINTEGRA_Path'
      Origin = 'paramsemp.SINTEGRA_Path'
      Size = 255
    end
    object QrParamsEmpFaturaNum: TSmallintField
      FieldName = 'FaturaNum'
      Origin = 'paramsemp.FaturaNum'
    end
    object QrParamsEmpEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'paramsemp.EntiTipCto'
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
      Origin = 'paramsemp.pCredSNMez'
    end
    object QrParamsEmpUsaReferen: TSmallintField
      FieldName = 'UsaReferen'
      Origin = 'paramsemp.UsaReferen'
    end
    object QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField
      FieldName = 'SPED_EFD_IND_PERFIL'
      Origin = 'paramsemp.SPED_EFD_IND_PERFIL'
      Size = 1
    end
    object QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField
      FieldName = 'SPED_EFD_IND_ATIV'
      Origin = 'paramsemp.SPED_EFD_IND_ATIV'
    end
    object QrParamsEmpSPED_EFD_CadContador: TIntegerField
      FieldName = 'SPED_EFD_CadContador'
      Origin = 'paramsemp.SPED_EFD_CadContador'
    end
    object QrParamsEmpSPED_EFD_CRCContador: TWideStringField
      FieldName = 'SPED_EFD_CRCContador'
      Origin = 'paramsemp.SPED_EFD_CRCContador'
      Size = 15
    end
    object QrParamsEmpSPED_EFD_EscriContab: TIntegerField
      FieldName = 'SPED_EFD_EscriContab'
      Origin = 'paramsemp.SPED_EFD_EscriContab'
    end
    object QrParamsEmpSPED_EFD_EnderContab: TSmallintField
      FieldName = 'SPED_EFD_EnderContab'
      Origin = 'paramsemp.SPED_EFD_EnderContab'
    end
    object QrParamsEmpSPED_EFD_Path: TWideStringField
      FieldName = 'SPED_EFD_Path'
      Origin = 'paramsemp.SPED_EFD_Path'
      Size = 255
    end
    object QrParamsEmpNFetpEmis: TSmallintField
      FieldName = 'NFetpEmis'
      Origin = 'paramsemp.NFetpEmis'
    end
    object QrParamsEmpNFeVerConDes: TFloatField
      FieldName = 'NFeVerConDes'
      Origin = 'paramsemp.NFeVerConDes'
    end
    object QrParamsEmpPediVdaNElertas: TIntegerField
      FieldName = 'PediVdaNElertas'
      Origin = 'paramsemp.PediVdaNElertas'
    end
    object QrParamsEmpEstq0UsoCons: TSmallintField
      FieldName = 'Estq0UsoCons'
      Origin = 'paramsemp.Estq0UsoCons'
    end
    object QrParamsEmpDpsNumero: TIntegerField
      FieldName = 'DpsNumero'
      Origin = 'paramsemp.DpsNumero'
    end
    object QrParamsEmpEntiTipCt1: TIntegerField
      FieldName = 'EntiTipCt1'
      Origin = 'paramsemp.EntiTipCt1'
    end
    object QrParamsEmpTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
      Origin = 'paramsemp.TZD_UTC'
    end
    object QrParamsEmpCartEmisHonFun: TIntegerField
      FieldName = 'CartEmisHonFun'
      Origin = 'paramsemp.CartEmisHonFun'
    end
    object QrParamsEmpCtaServicoPg: TIntegerField
      FieldName = 'CtaServicoPg'
      Origin = 'paramsemp.CtaServicoPg'
    end
    object QrParamsEmpCtaProdCom: TIntegerField
      FieldName = 'CtaProdCom'
      Origin = 'paramsemp.CtaProdCom'
    end
    object QrParamsEmpCTeversao: TFloatField
      FieldName = 'CTeversao'
      Origin = 'paramsemp.CTeversao'
    end
    object QrParamsEmpSPED_EFD_Peri_E100: TSmallintField
      FieldName = 'SPED_EFD_Peri_E100'
      Origin = 'paramsemp.SPED_EFD_Peri_E100'
    end
    object QrParamsEmpSPED_EFD_Peri_E500: TSmallintField
      FieldName = 'SPED_EFD_Peri_E500'
      Origin = 'paramsemp.SPED_EFD_Peri_E500'
    end
    object QrParamsEmpSPED_EFD_Peri_K100: TSmallintField
      FieldName = 'SPED_EFD_Peri_K100'
      Origin = 'paramsemp.SPED_EFD_Peri_K100'
    end
    object QrParamsEmpSPED_EFD_ICMS_IPI_VersaoGuia: TWideStringField
      FieldName = 'SPED_EFD_ICMS_IPI_VersaoGuia'
      Origin = 'paramsemp.SPED_EFD_ICMS_IPI_VersaoGuia'
      Size = 15
    end
    object QrParamsEmpSPED_EFD_MovSubPrd: TSmallintField
      FieldName = 'SPED_EFD_MovSubPrd'
      Origin = 'paramsemp.SPED_EFD_MovSubPrd'
    end
    object QrParamsEmpRetImpost: TSmallintField
      FieldName = 'RetImpost'
      Origin = 'paramsemp.RetImpost'
    end
    object QrParamsEmpFreteRpICMS: TFloatField
      FieldName = 'FreteRpICMS'
      Origin = 'paramsemp.FreteRpICMS'
    end
    object QrParamsEmpFreteRpPIS: TFloatField
      FieldName = 'FreteRpPIS'
      Origin = 'paramsemp.FreteRpPIS'
    end
    object QrParamsEmpFreteRpCOFINS: TFloatField
      FieldName = 'FreteRpCOFINS'
      Origin = 'paramsemp.FreteRpCOFINS'
    end
    object QrParamsEmpCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrParamsEmpNFSeCertAviExpi: TSmallintField
      FieldName = 'NFSeCertAviExpi'
      Origin = 'paramsemp.NFSeCertAviExpi'
    end
    object QrParamsEmpDirNFSeDPSGer: TWideStringField
      FieldName = 'DirNFSeDPSGer'
      Origin = 'paramsemp.DirNFSeDPSGer'
      Size = 255
    end
    object QrParamsEmpDirNFSeDPSAss: TWideStringField
      FieldName = 'DirNFSeDPSAss'
      Origin = 'paramsemp.DirNFSeDPSAss'
      Size = 255
    end
    object QrParamsEmpNFSeCertDigital: TWideStringField
      FieldName = 'NFSeCertDigital'
      Origin = 'paramsemp.NFSeCertDigital'
      Size = 255
    end
    object QrParamsEmpNFSe_indFinalCpl: TSmallintField
      FieldName = 'NFSe_indFinalCpl'
      Origin = 'paramsemp.NFSe_indFinalCpl'
    end
    object QrParamsEmpNFSeCertValidad: TDateField
      FieldName = 'NFSeCertValidad'
      Origin = 'paramsemp.NFSeCertValidad'
    end
    object QrParamsEmpNFSeTipCtoMail: TIntegerField
      FieldName = 'NFSeTipCtoMail'
      Origin = 'paramsemp.NFSeTipCtoMail'
    end
    object QrParamsEmpNFSeAmbiente: TSmallintField
      FieldName = 'NFSeAmbiente'
      Origin = 'paramsemp.NFSeAmbiente'
    end
    object QrParamsEmpNFSeWSHomologa: TWideStringField
      FieldName = 'NFSeWSHomologa'
      Origin = 'paramsemp.NFSeWSHomologa'
      Size = 255
    end
    object QrParamsEmpNFSeWSProducao: TWideStringField
      FieldName = 'NFSeWSProducao'
      Origin = 'paramsemp.NFSeWSProducao'
      Size = 255
    end
    object QrParamsEmpNFSeSerieRps: TWideStringField
      FieldName = 'NFSeSerieRps'
      Origin = 'paramsemp.NFSeSerieRps'
      Size = 5
    end
    object QrParamsEmpNFSeVersao: TFloatField
      FieldName = 'NFSeVersao'
      Origin = 'paramsemp.NFSeVersao'
    end
    object QrParamsEmpNFSeMetodo: TIntegerField
      FieldName = 'NFSeMetodo'
      Origin = 'paramsemp.NFSeMetodo'
    end
    object QrParamsEmpNFSeCodMunici: TIntegerField
      FieldName = 'NFSeCodMunici'
      Origin = 'paramsemp.NFSeCodMunici'
    end
    object QrParamsEmpNfse_WhatsApp_EntiTipCto: TIntegerField
      FieldName = 'Nfse_WhatsApp_EntiTipCto'
      Origin = 'paramsemp.Nfse_WhatsApp_EntiTipCto'
    end
    object QrParamsEmpDupMultiSV: TWideStringField
      FieldName = 'DupMultiSV'
      Size = 3
    end
    object QrParamsEmpCtaMultiSV: TIntegerField
      FieldName = 'CtaMultiSV'
    end
    object QrParamsEmpTxtMultiSV: TWideStringField
      FieldName = 'TxtMultiSV'
      Size = 100
    end
  end
  object QrThread: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT CONNECTION_ID() ID; ')
    Left = 80
    Top = 396
    object QrThreadID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
  end
  object QrUpdPID1: TMySQLQuery
    Database = MyPID_DB
    Left = 800
    Top = 52
  end
  object QrSenhas: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.IP_Default, Funcionario, '
      'pe.Descricao Perfil_TXT'
      'FROM senhas se'
      'LEFT JOIN perfis pe ON pe.Codigo = se.Perfil'
      'WHERE se.Login=:P1')
    Left = 428
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
    object QrSenhasPerfil_TXT: TWideStringField
      FieldName = 'Perfil_TXT'
      Size = 50
    end
  end
  object QrUpdL: TMySQLQuery
    Database = RV_CEP_DB
    Left = 672
    Top = 52
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 96
    Top = 292
  end
  object QrStqCenCad: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT CodUsu, Codigo, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 24
    Top = 292
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrAux_: TMySQLQuery
    Database = RV_CEP_DB
    Left = 748
    Top = 116
  end
  object DsFiliLog: TDataSource
    DataSet = QrFiliLog
    Left = 288
    Top = 396
  end
  object QrFiliaisSP: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Codigo Entidade, pem.Codigo Empresa'
      'FROM entidades ent'
      'LEFT JOIN paramsemp pem ON pem.Codigo=ent.Codigo'
      'WHERE pem.Codigo IS NULL'
      'AND ent.Codigo < -10')
    Left = 220
    Top = 448
    object QrFiliaisSPEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrFiliaisSPEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
  end
  object QrCambioMda: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 388
    Top = 268
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 432
    Top = 268
  end
  object DsSituacao: TDataSource
    DataSet = QrPediSit
    Left = 672
    Top = 104
  end
  object QrCliIntLog: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT eci.CodEnti'
      'FROM enticliint eci'
      'LEFT JOIN senhas snh ON snh.Funcionario=eci.AccManager'
      'LEFT JOIN senhasits sei ON eci.CodEnti=sei.Empresa'
      'WHERE eci.CodCliInt=1'
      'AND '
      '('
      '  eci.AccManager=0'
      '  OR '
      '  snh.Numero=30'
      '  OR'
      '  sei.Numero=30'
      ')')
    Left = 220
    Top = 348
    object QrCliIntLogCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
  end
  object DsCliIntLog: TDataSource
    DataSet = QrCliIntLog
    Left = 288
    Top = 348
  end
  object QrCliIntUni: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 220
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliIntUniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCliIntUni: TDataSource
    DataSet = QrCliIntUni
    Left = 288
    Top = 300
  end
  object QrLocCI: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, PercJuros, PercMulta, VTCBBNITAR'
      'FROM cond'
      'WHERE Cliente=:P0'
      '')
    Left = 820
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCIPercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrLocCIPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrLocCIVTCBBNITAR: TFloatField
      FieldName = 'VTCBBNITAR'
    end
  end
  object QrUpdPID2: TMySQLQuery
    Database = RV_CEP_DB
    Left = 800
    Top = 100
  end
  object QrLocLogr: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM listalograd'
      'WHERE Nome=:P0'
      '')
    Left = 820
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocLogrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrPEM_T: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao, pem.Controle, '
      'pem.Ordem, pem.Tipo, pem.CidID_Img'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'ORDER BY Ordem')
    Left = 628
    Top = 460
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_TTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_TDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrPEM_TControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPEM_TOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPEM_TTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPEM_TCidID_Img: TWideStringField
      FieldName = 'CidID_Img'
      Size = 32
    end
  end
  object QrPEM_2: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao, pem.Controle,'
      'pem.CidID_Img'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND Tipo=2')
    Left = 628
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_2Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrPEM_2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPEM_2CidID_Img: TWideStringField
      FieldName = 'CidID_Img'
      Size = 32
    end
  end
  object QrPEM_1: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND Tipo=1')
    Left = 628
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_1Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object QrPEM_0: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND Tipo=0')
    Left = 628
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_0Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_0Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 692
    Top = 220
  end
  object QrPreEmail: TMySQLQuery
    Database = RV_CEP_DB
    Left = 628
    Top = 220
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPreEmailSMTPServer: TWideStringField
      FieldName = 'SMTPServer'
      Size = 50
    end
    object QrPreEmailSend_Name: TWideStringField
      FieldName = 'Send_Name'
      Size = 50
    end
    object QrPreEmailSend_Mail: TWideStringField
      FieldName = 'Send_Mail'
      Size = 50
    end
    object QrPreEmailPass_Mail: TWideStringField
      FieldName = 'Pass_Mail'
      Size = 30
    end
    object QrPreEmailLogi_Name: TWideStringField
      FieldName = 'Logi_Name'
      Size = 30
    end
    object QrPreEmailLogi_Pass: TWideStringField
      FieldName = 'Logi_Pass'
      Size = 30
    end
    object QrPreEmailLogi_Auth: TSmallintField
      FieldName = 'Logi_Auth'
    end
    object QrPreEmailMail_Titu: TWideStringField
      FieldName = 'Mail_Titu'
      Size = 100
    end
    object QrPreEmailSaudacao: TWideStringField
      FieldName = 'Saudacao'
      Size = 100
    end
    object QrPreEmailNaoEnvBloq: TSmallintField
      FieldName = 'NaoEnvBloq'
    end
    object QrPreEmailPorta_mail: TIntegerField
      FieldName = 'Porta_mail'
    end
    object QrPreEmailLogi_SSL: TSmallintField
      FieldName = 'Logi_SSL'
    end
    object QrPreEmailEmailConta: TIntegerField
      FieldName = 'EmailConta'
    end
  end
  object QrPreEmMsg: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ELT(pem.Tipo+1, '
      #39'Plano'#39', '#39'HTML'#39', '#39'Imagem'#39') NOMETIPO, pem.* '
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'ORDER BY Ordem, Controle, Tipo')
    Left = 628
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreEmMsgNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Size = 10
    end
    object QrPreEmMsgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'preemmsg.Codigo'
    end
    object QrPreEmMsgControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'preemmsg.Controle'
    end
    object QrPreEmMsgOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'preemmsg.Ordem'
    end
    object QrPreEmMsgTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'preemmsg.Tipo'
    end
    object QrPreEmMsgTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'preemmsg.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPreEmMsgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'preemmsg.Lk'
    end
    object QrPreEmMsgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'preemmsg.DataCad'
    end
    object QrPreEmMsgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'preemmsg.DataAlt'
    end
    object QrPreEmMsgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'preemmsg.UserCad'
    end
    object QrPreEmMsgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'preemmsg.UserAlt'
    end
    object QrPreEmMsgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'preemmsg.AlterWeb'
    end
    object QrPreEmMsgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'preemmsg.Ativo'
      MaxValue = 1
    end
    object QrPreEmMsgDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'preemmsg.Descricao'
      Size = 255
    end
  end
  object DsPreEmMsg: TDataSource
    DataSet = QrPreEmMsg
    Left = 692
    Top = 268
  end
  object QrLancto: TMySQLQuery
    Database = RV_CEP_DB
    Left = 820
    Top = 388
    object QrLanctoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLanctoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLanctoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLanctoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLanctoNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLanctoNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrLanctoData: TDateField
      FieldName = 'Data'
    end
    object QrLanctoVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLanctoBanco1: TIntegerField
      FieldName = 'Banco1'
    end
  end
  object QrPPI: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet,'
      'ppi.Texto, ppi.Lancto, ppi.Docum'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR'
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR'
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'AND ptk.Controle=:P0'
      'ORDER BY DataD, Conta'
      '')
    Left = 816
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPPINO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPPINO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPPILimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPISaiu: TIntegerField
      FieldName = 'Saiu'
      MaxValue = 1
    end
    object QrPPIDataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRecebeu: TIntegerField
      FieldName = 'Recebeu'
      MaxValue = 1
    end
    object QrPPIDataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRetornou: TIntegerField
      FieldName = 'Retornou'
      MaxValue = 1
    end
    object QrPPIDataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIDataSai_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataSai_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIDataRec_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRec_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIDataRet_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRet_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPPIRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPPITexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrPPILancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
    end
  end
  object QrPTK: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT ptc.Nome, ptk.Controle Lote, ptk.Mez'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR '
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR '
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY ptk.Mez, ptk.Controle')
    Left = 816
    Top = 172
    object QrPTKNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPTKLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrPTKMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPTKMes_TXT: TWideStringField
      DisplayWidth = 5
      FieldKind = fkCalculated
      FieldName = 'Mes_TXT'
      Size = 5
      Calculated = True
    end
  end
  object DsPTK: TDataSource
    DataSet = QrPTK
    Left = 844
    Top = 172
  end
  object DsPPI: TDataSource
    DataSet = QrPPI
    Left = 844
    Top = 220
  end
  object QrMaius: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, RazaoSocial, Nome'
      'FROM entidades'
      '')
    Left = 320
    Top = 200
    object QrMaiusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMaiusRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrMaiusNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrPKA: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT  ptc.Nome, ptc.Codigo Lote, ptk.Mez,'
      'ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ('
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY DataD, Conta'
      '')
    Left = 816
    Top = 272
    object QrPKANome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPKALote: TIntegerField
      FieldName = 'Lote'
      Required = True
    end
    object QrPKAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPKADataE: TDateField
      FieldName = 'DataE'
    end
    object QrPKAConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPKARetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPKANO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPKANO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPKALimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKASaiu: TIntegerField
      FieldName = 'Saiu'
    end
    object QrPKADataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKALimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKARecebeu: TIntegerField
      FieldName = 'Recebeu'
    end
    object QrPKADataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKALimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKARetornou: TIntegerField
      FieldName = 'Retornou'
    end
    object QrPKADataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKADataSai_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataSai_TXT'
      Size = 8
      Calculated = True
    end
    object QrPKADataRec_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRec_TXT'
      Size = 8
      Calculated = True
    end
    object QrPKADataRet_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRet_TXT'
      Size = 8
      Calculated = True
    end
  end
  object DsPKA: TDataSource
    DataSet = QrPKA
    Left = 844
    Top = 272
  end
  object QrUEFE: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT MAX(Data) Data'
      'FROM lctoencer'
      'WHERE Entidade=:P0'
      '')
    Left = 428
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUEFEData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object QrMeses: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT'
      '((DAY(LAST_DAY(:P0)) + 0.0000)  -'
      '(DAY(:P1) + 0.0000)) /'
      '((DAY(LAST_DAY(:P2))+0.0000)) Ini,'
      'PERIOD_DIFF(:P3,:P4)-1 Meio,'
      '(DAY(:P5) + 0.0000)/'
      '(DAY(LAST_DAY(:P6)) + 0.0000) Fim'
      '')
    Left = 688
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrMesesIni: TFloatField
      FieldName = 'Ini'
    end
    object QrMesesMeio: TFloatField
      FieldName = 'Meio'
    end
    object QrMesesFim: TFloatField
      FieldName = 'Fim'
    end
  end
  object QrSMIN: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Qtde, CustoAll'
      'FROM stqmovitsa'
      'WHERE GraGruX=:P0'
      'AND Empresa=:P1'
      'AND Qtde>0'
      'ORDER BY DataHora DESC')
    Left = 448
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSMINQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSMINCustoAll: TFloatField
      FieldName = 'CustoAll'
    end
  end
  object QrSMIX: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT smia.GraGruX, ggx.GraGru1, SUM(smia.Qtde) QTDE '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux ggx ON ggx.controle=smia.GraGruX'
      'WHERE smia.Ativo=1'
      'AND smia.GraGruX=:P0'
      'AND smia.Empresa=:P1'
      'GROUP BY smia.GraGruX')
    Left = 488
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSMIXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSMIXQTDE: TFloatField
      FieldName = 'QTDE'
    end
    object QrSMIXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrX: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT smia.GraGruX'
      'FROM stqmovitsa smia'
      'WHERE smia.Ativo=1'
      'AND Qtde>0')
    Left = 524
    Top = 440
    object QrXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrGraGruVal: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle '
      'FROM gragruval'
      'WHERE GraGruX=:P0'
      'AND GraCusPrc=:P1'
      'AND Entidade=:P2'
      '')
    Left = 560
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrGraGruValControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,'
      'gg1.Nivel2, gg1.Nivel3, gg1.Nivel4, gg1.Nivel5'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE ggx.Controle=:P0')
    Left = 560
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruXNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Size = 50
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 30
    end
    object QrGraGruXSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
    object QrGraGruXNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGraGruXNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGraGruXGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrGraGruXGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrGraGruXNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrGraGruXNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
  end
  object QrLmz: TMySQLQuery
    Database = RV_CEP_DB
    Left = 320
    Top = 104
    object QrLmzMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLmzControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLmzID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLmzData: TDateField
      FieldName = 'Data'
    end
  end
  object QrUmz: TMySQLQuery
    Database = RV_CEP_DB
    Left = 320
    Top = 152
    object QrUmzMez: TIntegerField
      FieldName = 'Mez'
    end
  end
  object PMEnti: TPopupMenu
    Left = 544
    Top = 4
    object ModeloA1: TMenuItem
      Caption = 'Modelo &A'
    end
    object ModeloB1: TMenuItem
      Caption = 'Modelo &B'
    end
  end
  object QrCerrado: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT MAX(Data) Data'
      'FROM lctoencer'
      'WHERE carteira IN'
      '('
      '  SELECT Codigo'
      '  FROM carteiras'
      '  WHERE ForneceI=:P0'
      ')')
    Left = 136
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCerradoData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object QrProtoMail: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM protomail'
      'ORDER BY NivelEmail')
    Left = 729
    Top = 457
    object QrProtoMailDepto_Cod: TIntegerField
      FieldName = 'Depto_Cod'
      Origin = 'protomail.Depto_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailDepto_Txt: TWideStringField
      FieldName = 'Depto_Txt'
      Origin = 'protomail.Depto_Txt'
      Size = 100
    end
    object QrProtoMailEntid_Cod: TIntegerField
      FieldName = 'Entid_Cod'
      Origin = 'protomail.Entid_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailEntid_Txt: TWideStringField
      FieldName = 'Entid_Txt'
      Origin = 'protomail.Entid_Txt'
      Size = 100
    end
    object QrProtoMailTaref_Cod: TIntegerField
      FieldName = 'Taref_Cod'
      Origin = 'protomail.Taref_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailTaref_Txt: TWideStringField
      FieldName = 'Taref_Txt'
      Origin = 'protomail.Taref_Txt'
      Size = 100
    end
    object QrProtoMailDeliv_Cod: TIntegerField
      FieldName = 'Deliv_Cod'
      Origin = 'protomail.Deliv_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailDeliv_Txt: TWideStringField
      FieldName = 'Deliv_Txt'
      Origin = 'protomail.Deliv_Txt'
      Size = 100
    end
    object QrProtoMailDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protomail.DataE'
    end
    object QrProtoMailDataE_Txt: TWideStringField
      FieldName = 'DataE_Txt'
      Origin = 'protomail.DataE_Txt'
      Size = 30
    end
    object QrProtoMailDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protomail.DataD'
    end
    object QrProtoMailDataD_Txt: TWideStringField
      FieldName = 'DataD_Txt'
      Origin = 'protomail.DataD_Txt'
      Size = 30
    end
    object QrProtoMailProtoLote: TIntegerField
      FieldName = 'ProtoLote'
      Origin = 'protomail.ProtoLote'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailNivelEmail: TSmallintField
      FieldName = 'NivelEmail'
      Origin = 'protomail.NivelEmail'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailItem: TIntegerField
      FieldName = 'Item'
      Origin = 'protomail.Item'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailNivelDescr: TWideStringField
      FieldName = 'NivelDescr'
      Origin = 'protomail.NivelDescr'
      Size = 50
    end
    object QrProtoMailProtocolo: TIntegerField
      FieldName = 'Protocolo'
      Origin = 'protomail.Protocolo'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailRecipEmeio: TWideStringField
      FieldName = 'RecipEmeio'
      Size = 100
    end
    object QrProtoMailRecipNome: TWideStringField
      FieldName = 'RecipNome'
      Size = 100
    end
    object QrProtoMailRecipProno: TWideStringField
      FieldName = 'RecipProno'
    end
    object QrProtoMailRecipItem: TIntegerField
      FieldName = 'RecipItem'
    end
    object QrProtoMailBloqueto: TFloatField
      FieldName = 'Bloqueto'
    end
    object QrProtoMailVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrProtoMailValor: TFloatField
      FieldName = 'Valor'
    end
    object QrProtoMailPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrProtoMailCondominio: TWideStringField
      FieldName = 'Condominio'
      Size = 100
    end
    object QrProtoMailIDEmeio: TIntegerField
      FieldName = 'IDEmeio'
    end
  end
  object QrEntiMail: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ema.EMail'
      'FROM entimail ema'
      'WHERE ema.Codigo=:P0'
      'AND ema.EntiTipCto=:P1  '
      'ORDER BY ema.Ordem, ema.Conta'#10
      '')
    Left = 692
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
  end
  object QrAuxPID1: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    Top = 100
  end
  object QrCtrlGeral: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM ctrlgeral')
    Left = 220
    Top = 252
    object QrCtrlGeralDmkNetPath: TWideStringField
      FieldName = 'DmkNetPath'
      Origin = 'ctrlgeral.DmkNetPath'
      Size = 255
    end
    object QrCtrlGeralDuplicLct: TSmallintField
      FieldName = 'DuplicLct'
      Origin = 'ctrlgeral.DuplicLct'
    end
    object QrCtrlGeralVctAutDefU: TSmallintField
      FieldName = 'VctAutDefU'
    end
    object QrCtrlGeralVctAutLast: TDateTimeField
      FieldName = 'VctAutLast'
    end
    object QrCtrlGeralAbaIniApp: TIntegerField
      FieldName = 'AbaIniApp'
    end
    object QrCtrlGeralMyPathsFrx: TSmallintField
      FieldName = 'MyPathsFrx'
    end
    object QrCtrlGeralFixBugsApp: TLargeintField
      FieldName = 'FixBugsApp'
    end
    object QrCtrlGeralFixBugsGrl: TLargeintField
      FieldName = 'FixBugsGrl'
    end
    object QrCtrlGeralLastBackup: TDateTimeField
      FieldName = 'LastBackup'
    end
    object QrCtrlGeralLastBckpIntrvl: TTimeField
      FieldName = 'LastBckpIntrvl'
    end
    object QrCtrlGeralUsaGenCtb: TSmallintField
      FieldName = 'UsaGenCtb'
    end
    object QrCtrlGeralPwdLibFunc: TWideStringField
      FieldName = 'PwdLibFunc'
      Size = 60
    end
    object QrCtrlGeralLimiCredIni: TFloatField
      FieldName = 'LimiCredIni'
    end
    object QrCtrlGeralCNPJaToken: TWideStringField
      FieldName = 'CNPJaToken'
      Size = 511
    end
    object QrCtrlGeralExigeNumDocCNPJCPF: TSmallintField
      FieldName = 'ExigeNumDocCNPJCPF'
    end
  end
  object QrCliAplic: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Controle, FimData, FimChek'
      'FROM cliaplic'
      'WHERE Codigo=:P0'
      'AND Aplicativo=:P1')
    Left = 544
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCliAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCliAplicFimChek: TWideStringField
      FieldName = 'FimChek'
      Size = 50
    end
    object QrCliAplicFimData: TDateField
      FieldName = 'FimData'
    end
  end
  object QrEntDmk: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, CNPJ, CPF) CPFJ,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'IF(Tipo=0,ETe1, PTe1) LicTel,'
      'IF(Tipo=0, EEmail, PEmail) LicMail'
      'FROM entidades'
      'WHERE CNPJ=:P0'
      'OR CPF=:P1')
    Left = 544
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntDmkCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntDmkNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEntDmkCPFJ: TWideStringField
      FieldName = 'CPFJ'
      Size = 18
    end
    object QrEntDmkLicTel: TWideStringField
      FieldName = 'LicTel'
    end
    object QrEntDmkLicMail: TWideStringField
      FieldName = 'LicMail'
    end
  end
  object QrCliAplicLi: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM cliaplicli'
      'WHERE Controle=:P0'
      'AND SerialKey=:P1'
      'AND SerialNum=:P2')
    Left = 544
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCliAplicLiConta: TAutoIncField
      FieldName = 'Conta'
    end
    object QrCliAplicLiNomePC: TWideStringField
      FieldName = 'NomePC'
      Size = 255
    end
    object QrCliAplicLiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCliAplicLiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCliAplicLiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCliAplicLiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCliAplicLiSerialKey: TWideStringField
      FieldName = 'SerialKey'
      Size = 32
    end
    object QrCliAplicLiSerialNum: TWideStringField
      FieldName = 'SerialNum'
      Size = 32
    end
    object QrCliAplicLiCadastro: TDateTimeField
      FieldName = 'Cadastro'
    end
    object QrCliAplicLiConsultaDH: TDateTimeField
      FieldName = 'ConsultaDH'
    end
    object QrCliAplicLiConsultaNV: TIntegerField
      FieldName = 'ConsultaNV'
    end
  end
  object QrTermiServ: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SerialKey, SerialNum'
      'FROM terminais'
      'WHERE Servidor = 1'
      'ORDER BY Terminal')
    Left = 612
    Top = 52
    object QrTermiServSerialKey: TWideStringField
      FieldName = 'SerialKey'
      Size = 32
    end
    object QrTermiServSerialNum: TWideStringField
      FieldName = 'SerialNum'
      Size = 32
    end
  end
  object QrTerminal: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM terminais'
      'WHERE SerialKey=:P0'
      'AND SerialNum=:P1')
    Left = 544
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrTerminalLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrTerminalSerialKey: TWideStringField
      FieldName = 'SerialKey'
      Size = 32
    end
    object QrTerminalSerialNum: TWideStringField
      FieldName = 'SerialNum'
      Size = 32
    end
    object QrTerminalServidor: TSmallintField
      FieldName = 'Servidor'
    end
    object QrTerminalNomePC: TWideStringField
      FieldName = 'NomePC'
      Size = 255
    end
    object QrTerminalDescriPC: TWideStringField
      FieldName = 'DescriPC'
      Size = 50
    end
    object QrTerminalVersaoMin: TWideStringField
      FieldName = 'VersaoMin'
      Size = 13
    end
    object QrTerminalVersaoMax: TWideStringField
      FieldName = 'VersaoMax'
      Size = 13
    end
  end
  object QrWAgora: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 544
    Top = 244
    object QrWAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrWAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrWAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrWAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrWAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrWAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrWAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrDefEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Filial, CliInt'
      'FROM entidades '
      'WHERE Codigo=:P0')
    Left = 736
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDefEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefEntiFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrDefEntiCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDefEntiNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsLastMorto: TDataSource
    DataSet = QrLastMorto
    Left = 428
    Top = 412
  end
  object QrLastMorto: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT lae.Data, SUM(lae.SaldoIni) SaldoIni,'
      'SUM(lae.Creditos) Creditos, SUM(lae.Debitos) Debitos,'
      'SUM(lae.SaldoFim) SaldoFim, TabLct,'
      'CONCAT(YEAR(lae.Data), "/", LPAD(MONTH(lae.Data), 2, "0")) AM'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'AND TabLct="D"'
      'GROUP BY Data'
      'ORDER BY AM DESC'
      'LIMIT 1')
    Left = 384
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastMortoData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLastMortoSaldoIni: TFloatField
      FieldName = 'SaldoIni'
    end
    object QrLastMortoCreditos: TFloatField
      FieldName = 'Creditos'
    end
    object QrLastMortoDebitos: TFloatField
      FieldName = 'Debitos'
    end
    object QrLastMortoSaldoFim: TFloatField
      FieldName = 'SaldoFim'
    end
    object QrLastMortoTabLct: TWideStringField
      FieldName = 'TabLct'
      Size = 1
    end
    object QrLastMortoAM: TWideStringField
      FieldName = 'AM'
      Size = 7
    end
  end
  object DsLastEncer: TDataSource
    DataSet = QrLastEncer
    Left = 428
    Top = 364
  end
  object QrLastEncer: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT lae.Data, SUM(lae.SaldoIni) SaldoIni,'
      'SUM(lae.Creditos) Creditos, SUM(lae.Debitos) Debitos,'
      'SUM(lae.SaldoFim) SaldoFim, TabLct,'
      'CONCAT(YEAR(lae.Data), "/", LPAD(MONTH(lae.Data), 2, "0")) AM'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'GROUP BY Data'
      'ORDER BY AM DESC'
      'LIMIT 1')
    Left = 388
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastEncerData: TDateField
      FieldName = 'Data'
    end
    object QrLastEncerSaldoIni: TFloatField
      FieldName = 'SaldoIni'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLastEncerCreditos: TFloatField
      FieldName = 'Creditos'
    end
    object QrLastEncerDebitos: TFloatField
      FieldName = 'Debitos'
    end
    object QrLastEncerSaldoFim: TFloatField
      FieldName = 'SaldoFim'
    end
    object QrLastEncerTabLct: TWideStringField
      FieldName = 'TabLct'
      Size = 1
    end
    object QrLastEncerAM: TWideStringField
      FieldName = 'AM'
      Size = 7
    end
  end
  object QrParOrfRep: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    object QrParOrfRepData: TDateField
      DisplayWidth = 12
      FieldName = 'Data'
    end
    object QrParOrfRepCarteira: TIntegerField
      DisplayWidth = 12
      FieldName = 'Carteira'
    end
    object QrParOrfRepControle: TIntegerField
      DisplayWidth = 12
      FieldName = 'Controle'
    end
    object QrParOrfRepDescricao: TWideStringField
      DisplayWidth = 57
      FieldName = 'Descricao'
      Size = 100
    end
    object QrParOrfRepCredito: TFloatField
      DisplayWidth = 12
      FieldName = 'Credito'
    end
    object QrParOrfRepVencimento: TDateField
      DisplayWidth = 12
      FieldName = 'Vencimento'
    end
    object QrParOrfRepCompensado: TDateField
      DisplayWidth = 13
      FieldName = 'Compensado'
    end
    object QrParOrfRepFatNum: TFloatField
      DisplayWidth = 12
      FieldName = 'FatNum'
    end
    object QrParOrfRepFatParcela: TIntegerField
      DisplayWidth = 12
      FieldName = 'FatParcela'
    end
    object QrParOrfRepMez: TIntegerField
      DisplayWidth = 12
      FieldName = 'Mez'
    end
    object QrParOrfRepCliente: TIntegerField
      DisplayWidth = 12
      FieldName = 'Cliente'
    end
    object QrParOrfRepCliInt: TIntegerField
      DisplayWidth = 12
      FieldName = 'CliInt'
    end
    object QrParOrfRepForneceI: TIntegerField
      DisplayWidth = 12
      FieldName = 'ForneceI'
    end
    object QrParOrfRepDepto: TIntegerField
      DisplayWidth = 12
      FieldName = 'Depto'
    end
    object QrParOrfRepAtrelado: TIntegerField
      DisplayWidth = 12
      FieldName = 'Atrelado'
    end
  end
  object DsParOrfRep: TDataSource
    DataSet = QrParOrfRep
    Left = 864
    Top = 48
  end
  object QrCheque: TMySQLQuery
    Database = RV_CEP_DB
    Left = 768
    Top = 388
    object QrChequeBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrChequeDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrChequeCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrChequeFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrChequeCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrChequeNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrChequeNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrChequeData: TDateField
      FieldName = 'Data'
    end
    object QrChequeVencimento: TDateField
      FieldName = 'Vencimento'
    end
  end
  object QrFavoritos3: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Nivel3, Titulo '
      'FROM favoritos3'
      'WHERE Usuario=:P0'
      'ORDER BY Ordem, Nivel3, Titulo'
      '')
    Left = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFavoritos3Nivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrFavoritos3Titulo: TWideStringField
      FieldName = 'Titulo'
      Size = 50
    end
  end
  object QrFavoritos2: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Nivel2, Titulo '
      'FROM favoritos2'
      'WHERE Nivel3=:P0'
      'ORDER BY Ordem, Reordem, Titulo')
    Left = 376
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFavoritos2Nivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrFavoritos2Titulo: TWideStringField
      FieldName = 'Titulo'
      Size = 50
    end
  end
  object QrFavoritos1: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM favoritos1'
      'WHERE Nivel2=:P0')
    Left = 376
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFavoritos1Descri1: TWideStringField
      FieldName = 'Descri1'
      Size = 15
    end
    object QrFavoritos1Descri2: TWideStringField
      FieldName = 'Descri2'
      Size = 15
    end
    object QrFavoritos1Descri3: TWideStringField
      FieldName = 'Descri3'
      Size = 15
    end
    object QrFavoritos1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFavoritos1Classe: TWideStringField
      FieldName = 'Classe'
      Size = 255
    end
  end
  object Tb_Empresas: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_empresas_'
    Left = 960
    Top = 4
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    DataSet = Tb_Empresas
    Left = 960
    Top = 56
  end
  object Tb_Indi_Pags: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_indi_pags_'
    Left = 964
    Top = 104
    object Tb_Indi_PagsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Tb_Indi_PagsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_Indi_PagsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Indi_Pags: TDataSource
    DataSet = Tb_Indi_Pags
    Left = 964
    Top = 156
  end
  object QrEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Tipo, IE, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENTI,'
      'IF(Tipo=0, EUF, PUF) + 0.000  UF_Cod,'
      'IF(Tipo=0, ECodMunici, PCodMunici)  + 0.000 CodMunici,'
      'IF(Tipo=0, ECodiPais, PCodiPais) + 0.000 CodiPais'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 960
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiUF_Cod: TFloatField
      FieldName = 'UF_Cod'
      Required = True
    end
    object QrEntiNO_ENTI: TWideStringField
      FieldName = 'NO_ENTI'
      Size = 100
    end
    object QrEntiCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
    object QrEntiCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
  end
  object QrEntiCliInt: TMySQLQuery
    Database = RV_CEP_DB
    Left = 960
    Top = 264
  end
  object QrControle: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 924
    Top = 404
  end
  object QrUsTx: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ut.TxtMeu '
      'FROM usertxts ut'
      'WHERE TxtSys <> TxtMeu'
      'AND TxtSys=:P0'
      'ORDER BY ut.Ordem')
    Left = 128
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsTxTxtMeu: TWideStringField
      FieldName = 'TxtMeu'
      Size = 255
    end
  end
  object QrPerfis: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM PerfisIts pit'
      
        'LEFT JOIN PerfisItsPerf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 988
    Top = 404
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrSSit: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 456
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrBoss: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 372
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Origin = 'master.MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'master.CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Origin = 'master.MasSenha'
      Size = 30
    end
  end
  object QrBSit: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 400
    Top = 172
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrUTC: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SYSDATE() DATA_LOC, '
      'UTC_TIMESTAMP() DATA_UTC, '
      'SYSDATE()- UTC_TIMESTAMP() UTC_Dif')
    Left = 960
    Top = 320
    object QrUTCDATA_LOC: TDateTimeField
      FieldName = 'DATA_LOC'
      Required = True
    end
    object QrUTCDATA_UTC: TDateTimeField
      FieldName = 'DATA_UTC'
      Required = True
    end
    object QrUTCUTC_Dif: TFloatField
      FieldName = 'UTC_Dif'
      Required = True
    end
  end
  object QrOpcoesGerl: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesgerl')
    Left = 288
    Top = 252
  end
  object QrAllUpd: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 56
  end
  object QrAllAux: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 104
  end
  object QrEntNFS: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Tipo, ent.IE, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ELograd, ent.PLograd) + 0.000 Lograd,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) Rua,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 Numero,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) Compl,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) Bairro,'
      
        'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CodMunici' +
        ','
      'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000  UF_Cod,'
      'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CodiPais,'
      'IF(ent.Tipo=0, ent.ECEP, ent.PCEP) + 0.000 CEP,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TE1,'
      'ent.NIRE,'
      'llo.Nome NO_Lograd'
      'FROM entidades ent'
      'LEFT JOIN listalograd llo ON llo.Codigo='
      '  IF(ent.Tipo=0, ent.PLograd, ent.ELograd)'
      'WHERE ent.Codigo=:P0'
      '')
    Left = 1012
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntNFSTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntNFSIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntNFSCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntNFSCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntNFSNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEntNFSRua: TWideStringField
      FieldName = 'Rua'
      Size = 60
    end
    object QrEntNFSNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntNFSCompl: TWideStringField
      FieldName = 'Compl'
      Size = 60
    end
    object QrEntNFSBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 60
    end
    object QrEntNFSUF_Cod: TFloatField
      FieldName = 'UF_Cod'
      Required = True
    end
    object QrEntNFSCodiPais: TFloatField
      FieldName = 'CodiPais'
      Required = True
    end
    object QrEntNFSNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEntNFSNO_Lograd: TWideStringField
      FieldName = 'NO_Lograd'
      Size = 15
    end
    object QrEntNFSTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntNFSLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrEntNFSCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntNFSCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
  end
  object QrLocFilial: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT CodEnti, CodFilial'
      'FROM enticliint'
      'WHERE CodEnti=:P0')
    Left = 384
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocFilialCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrLocFilialCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
  end
  object QrCidade: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Nome '
      'FROM dtb_munici'
      'WHERE Codigo=:P0'
      '')
    Left = 128
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCidadeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrSelCods: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM _selcods_'
      '')
    Left = 960
    Top = 458
    object QrSelCodsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSelCodsNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrSelCodsNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrSelCodsNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrSelCodsNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrSelCodsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSelCodsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
      MaxValue = 1
    end
  end
  object DsSelCods: TDataSource
    DataSet = QrSelCods
    Left = 1020
    Top = 458
  end
  object QrFiliaisOF: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Codigo Entidade, fil.Codigo Empresa'
      'FROM entidades ent'
      'LEFT JOIN opcoesfili fil ON fil.Codigo=ent.Codigo'
      'WHERE fil.Codigo IS NULL'
      'AND ent.Codigo < -10'
      '')
    Left = 288
    Top = 448
    object QrFiliaisOFEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFiliaisOFEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
  end
  object QrCartToPrn: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT COUNT(env.Codigo) ITENS'
      'FROM diarcemenv env'
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'WHERE env.Metodo=3'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() ')
    Left = 540
    Top = 344
    object QrCartToPrnITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrProvisorio1: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ece.Codigo NULO, ect.Codigo, ect.Controle '
      'FROM enticontat ect'
      'LEFT JOIN enticonent ece ON '
      '  ece.Controle=ect.Controle'
      '  AND ece.Codigo=ect.Codigo'
      'WHERE ece.Codigo IS Null'
      '')
    Left = 880
    Top = 336
    object QrProvisorio1NULO: TIntegerField
      FieldName = 'NULO'
      Required = True
    end
    object QrProvisorio1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProvisorio1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object PMSaveCfgMenu: TPopupMenu
    Left = 436
    Top = 96
    object SalvarOrdenaodascolunasdagrade2: TMenuItem
      Caption = '&Salvar ordena'#231#227'o das colunas da grade'
    end
    object CarregarOrdenaopadrodascolunasdagrade1: TMenuItem
      Caption = '&Carregar ordena'#231#227'o padr'#227'o das colunas da grade'
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object esteXLS1: TMenuItem
      Caption = 'Salva dados em XLS e abre Excel'#174
    end
  end
  object QrMD5: TMySQLQuery
    Database = RV_CEP_DB
    Left = 156
    Top = 292
    object QrMD5Texto: TWideStringField
      FieldName = 'Texto'
      Size = 60
    end
  end
  object QrNomes: TMySQLQuery
    Database = RV_CEP_DB
    Left = 156
    Top = 340
    object QrNomesTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 255
    end
  end
  object frxDsEndereco2: TfrxDBDataset
    UserName = 'frxDsEndereco2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Cadastro=Cadastro'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'NIRE_=NIRE_'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'Tipo=Tipo'
      'TE1=TE1'
      'FAX=FAX'
      'ENatal=ENatal'
      'PNatal=PNatal'
      'Respons1=Respons1'
      'CEP_TXT=CEP_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'FAX_TXT=FAX_TXT'
      'TE1_TXT=TE1_TXT'
      'NATAL_TXT=NATAL_TXT'
      'NO_TIPO_DOC=NO_TIPO_DOC'
      'ENumero=ENumero'
      'PNumero=PNumero'
      'NUMERO=NUMERO'
      'ELograd=ELograd'
      'PLograd=PLograd'
      'LOGRAD=LOGRAD'
      'ECEP=ECEP'
      'PCEP=PCEP'
      'CEP=CEP'
      'SITE=SITE')
    DataSet = QrEndereco2
    BCDToCurrency = False
    
    Left = 68
    Top = 200
  end
  object QrSB5: TMySQLQuery
    Database = RV_CEP_DB
    Left = 188
    Top = 191
    object QrSB5Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSB5CodUsu: TLargeintField
      FieldName = 'CodUsu'
    end
    object QrSB5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB5: TDataSource
    DataSet = QrSB5
    Left = 248
    Top = 191
  end
  object QrCambios: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM cambios'
      'ORDER BY Data DESC')
    Left = 760
    Top = 220
    object QrCambiosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambiosValor: TFloatField
      FieldName = 'Valor'
    end
    object QrCambiosDataC: TDateField
      FieldName = 'DataC'
    end
  end
  object DsCambios: TDataSource
    DataSet = QrCambios
    Left = 764
    Top = 268
  end
  object QrWebParams: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesgerl')
    Left = 1048
    Top = 292
  end
  object QrUpdSync: TMySQLQuery
    Database = RV_CEP_DB
    Left = 560
    Top = 56
  end
  object QrDono: TMySQLQuery
    Database = RV_CEP_DB
    AfterOpen = QrDonoAfterOpen
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2, ECEP, PCEP, EUF, PUF,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O,'
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO,'
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F,'
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG,'
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_,'
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA,'
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE,'
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD,'
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF,'
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ','
      
        '/*CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lo' +
        'grad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        '*/'
      'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,'
      
        'CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX,'
      'IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) + 0.000 CODMUNICI '
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 8
    Top = 56
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrDonoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrDonoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrDonoTE2: TWideStringField
      FieldName = 'TE2'
    end
    object QrDonoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrDonoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrDonoECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrDonoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TWideStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Size = 60
    end
    object QrDonoRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Size = 60
    end
    object QrDonoCEP: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CEP'
      Calculated = True
    end
    object QrDonoECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrDonoPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrDonoEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrDonoPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrDonoUF: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'UF'
      Calculated = True
    end
    object QrDonoFONES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FONES'
      Size = 255
      Calculated = True
    end
    object QrDonoCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
    end
  end
  object QrPrmsEmpNFe: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT'
      'PreMailAut,'
      'PreMailCan,'
      'PreMailEveCCe,'
      'DirNFeProt,'
      'DirNFeRWeb,'
      'SCAN_Ser,'
      'SCAN_nNF,'
      'infRespTec_xContato,'
      'infRespTec_CNPJ,'
      'infRespTec_fone,'
      'infRespTec_email,'
      'infRespTec_idCSRT,'
      'infRespTec_CSRT,'
      'infRespTec_Usa,'
      'UF_WebServ,'
      'SiglaCustm,'
      'UF_Servico,'
      'SimplesFed,'
      'InfoPerCuz,'
      'NFeSerNum,'
      'NFeSerVal,'
      'NFeSerAvi,'
      'UF_MDeMDe,'
      'UF_MDeDes,'
      'UF_MDeNFe,'
      'NFeUF_EPEC,'
      'DirEnvLot,'
      'DirRec,'
      'DirProRec,'
      'DirNFeAss,'
      'DirNFeGer,'
      'DirDen,'
      'DirPedRec,'
      'DirPedSta,'
      'DirSta,'
      'DirPedCan,'
      'DirCan,'
      'DirPedInu,'
      'DirInu,'
      'DirPedSit,'
      'DirSit,'
      'DirEveEnvLot,'
      'DirEveRetLot,'
      'DirEvePedCCe,'
      'DirEveRetCCe,'
      'DirEvePedCan,'
      'DirEveRetCan,'
      'DirEveProcCCe,'
      'DirRetNfeDes,'
      'DirEvePedMDe,'
      'DirEveRetMDe,'
      'DirDowNFeDes,'
      'DirDowNFeNFe,'
      'DirDistDFeInt,'
      'DirRetDistDFeInt,'
      'DirDowNFeCnf,'
      'versao,'
      'ide_mod,'
      'ide_tpAmb,'
      'DirSchema,'
      'NT2018_05v120,'
      'AppCode,'
      'AssDigMode,'
      'MyEmailNFe,'
      'NFeInfCpl,'
      'CRT,'
      'CSOSN,'
      'pCredSNAlq,'
      'pCredSNAlq,'
      'NFeVerStaSer,'
      'NFeVerEnvLot,'
      'NFeVerConLot,'
      'NFeVerCanNFe,'
      'NFeVerInuNum,'
      'NFeVerConNFe,'
      'NFeVerLotEve,'
      'NFeVerConsCad,'
      'NFeVerDistDFeInt,'
      'NFeVerDowNFe,'
      'ide_tpImp,'
      'NFeItsLin,'
      'NFeFTRazao,'
      'NFeFTEnder,'
      'NFeFTFones,'
      'NFeMaiusc,'
      'NFeShowURL,'
      'PathLogoNF,'
      'DirDANFEs,'
      'NFe_indFinalCpl,'
      'NoDANFEMail,'
      'NFeNT2013_003LTT,'
      ''
      'CerDigital'
      'FROM paramsemp '
      'WHERE Codigo=0'
      '')
    Left = 288
    Top = 584
    object QrPrmsEmpNFePreMailAut: TIntegerField
      FieldName = 'PreMailAut'
      Origin = 'paramsemp.PreMailAut'
      Required = True
    end
    object QrPrmsEmpNFePreMailCan: TIntegerField
      FieldName = 'PreMailCan'
      Origin = 'paramsemp.PreMailCan'
      Required = True
    end
    object QrPrmsEmpNFePreMailEveCCe: TIntegerField
      FieldName = 'PreMailEveCCe'
      Origin = 'paramsemp.PreMailEveCCe'
      Required = True
    end
    object QrPrmsEmpNFeDirNFeProt: TWideStringField
      FieldName = 'DirNFeProt'
      Origin = 'paramsemp.DirNFeProt'
      Size = 255
    end
    object QrPrmsEmpNFeDirNFeRWeb: TWideStringField
      FieldName = 'DirNFeRWeb'
      Origin = 'paramsemp.DirNFeRWeb'
      Size = 255
    end
    object QrPrmsEmpNFeSCAN_Ser: TIntegerField
      FieldName = 'SCAN_Ser'
      Origin = 'paramsemp.SCAN_Ser'
      Required = True
    end
    object QrPrmsEmpNFeSCAN_nNF: TIntegerField
      FieldName = 'SCAN_nNF'
      Origin = 'paramsemp.SCAN_nNF'
      Required = True
    end
    object QrPrmsEmpNFeinfRespTec_xContato: TWideStringField
      FieldName = 'infRespTec_xContato'
      Origin = 'paramsemp.infRespTec_xContato'
      Required = True
      Size = 60
    end
    object QrPrmsEmpNFeinfRespTec_CNPJ: TWideStringField
      FieldName = 'infRespTec_CNPJ'
      Origin = 'paramsemp.infRespTec_CNPJ'
      Required = True
      Size = 14
    end
    object QrPrmsEmpNFeinfRespTec_fone: TWideStringField
      FieldName = 'infRespTec_fone'
      Origin = 'paramsemp.infRespTec_fone'
      Required = True
      Size = 14
    end
    object QrPrmsEmpNFeinfRespTec_email: TWideStringField
      FieldName = 'infRespTec_email'
      Origin = 'paramsemp.infRespTec_email'
      Required = True
      Size = 60
    end
    object QrPrmsEmpNFeinfRespTec_idCSRT: TWideStringField
      FieldName = 'infRespTec_idCSRT'
      Origin = 'paramsemp.infRespTec_idCSRT'
      Required = True
      Size = 2
    end
    object QrPrmsEmpNFeinfRespTec_CSRT: TWideStringField
      FieldName = 'infRespTec_CSRT'
      Origin = 'paramsemp.infRespTec_CSRT'
      Size = 120
    end
    object QrPrmsEmpNFeinfRespTec_Usa: TSmallintField
      FieldName = 'infRespTec_Usa'
      Origin = 'paramsemp.infRespTec_Usa'
      Required = True
    end
    object QrPrmsEmpNFeUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Origin = 'paramsemp.UF_WebServ'
      Required = True
      Size = 2
    end
    object QrPrmsEmpNFeSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'paramsemp.SiglaCustm'
      Size = 15
    end
    object QrPrmsEmpNFeUF_Servico: TWideStringField
      FieldName = 'UF_Servico'
      Origin = 'paramsemp.UF_Servico'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
      Origin = 'paramsemp.SimplesFed'
      Required = True
    end
    object QrPrmsEmpNFeInfoPerCuz: TSmallintField
      FieldName = 'InfoPerCuz'
      Origin = 'paramsemp.InfoPerCuz'
      Required = True
    end
    object QrPrmsEmpNFeNFeSerNum: TWideStringField
      FieldName = 'NFeSerNum'
      Origin = 'paramsemp.NFeSerNum'
      Size = 255
    end
    object QrPrmsEmpNFeNFeSerVal: TDateField
      FieldName = 'NFeSerVal'
      Origin = 'paramsemp.NFeSerVal'
      Required = True
    end
    object QrPrmsEmpNFeNFeSerAvi: TSmallintField
      FieldName = 'NFeSerAvi'
      Origin = 'paramsemp.NFeSerAvi'
      Required = True
    end
    object QrPrmsEmpNFeUF_MDeMDe: TWideStringField
      FieldName = 'UF_MDeMDe'
      Origin = 'paramsemp.UF_MDeMDe'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeUF_MDeDes: TWideStringField
      FieldName = 'UF_MDeDes'
      Origin = 'paramsemp.UF_MDeDes'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeUF_MDeNFe: TWideStringField
      FieldName = 'UF_MDeNFe'
      Origin = 'paramsemp.UF_MDeNFe'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeNFeUF_EPEC: TWideStringField
      FieldName = 'NFeUF_EPEC'
      Origin = 'paramsemp.NFeUF_EPEC'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeDirEnvLot: TWideStringField
      FieldName = 'DirEnvLot'
      Origin = 'paramsemp.DirEnvLot'
      Size = 255
    end
    object QrPrmsEmpNFeDirRec: TWideStringField
      FieldName = 'DirRec'
      Origin = 'paramsemp.DirRec'
      Size = 255
    end
    object QrPrmsEmpNFeDirProRec: TWideStringField
      FieldName = 'DirProRec'
      Origin = 'paramsemp.DirProRec'
      Size = 255
    end
    object QrPrmsEmpNFeDirNFeAss: TWideStringField
      FieldName = 'DirNFeAss'
      Origin = 'paramsemp.DirNFeAss'
      Size = 255
    end
    object QrPrmsEmpNFeDirNFeGer: TWideStringField
      FieldName = 'DirNFeGer'
      Origin = 'paramsemp.DirNFeGer'
      Size = 255
    end
    object QrPrmsEmpNFeDirDen: TWideStringField
      FieldName = 'DirDen'
      Origin = 'paramsemp.DirDen'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedRec: TWideStringField
      FieldName = 'DirPedRec'
      Origin = 'paramsemp.DirPedRec'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedSta: TWideStringField
      FieldName = 'DirPedSta'
      Origin = 'paramsemp.DirPedSta'
      Size = 255
    end
    object QrPrmsEmpNFeDirSta: TWideStringField
      FieldName = 'DirSta'
      Origin = 'paramsemp.DirSta'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedCan: TWideStringField
      FieldName = 'DirPedCan'
      Origin = 'paramsemp.DirPedCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirCan: TWideStringField
      FieldName = 'DirCan'
      Origin = 'paramsemp.DirCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedInu: TWideStringField
      FieldName = 'DirPedInu'
      Origin = 'paramsemp.DirPedInu'
      Size = 255
    end
    object QrPrmsEmpNFeDirInu: TWideStringField
      FieldName = 'DirInu'
      Origin = 'paramsemp.DirInu'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedSit: TWideStringField
      FieldName = 'DirPedSit'
      Origin = 'paramsemp.DirPedSit'
      Size = 255
    end
    object QrPrmsEmpNFeDirSit: TWideStringField
      FieldName = 'DirSit'
      Origin = 'paramsemp.DirSit'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveEnvLot: TWideStringField
      FieldName = 'DirEveEnvLot'
      Origin = 'paramsemp.DirEveEnvLot'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetLot: TWideStringField
      FieldName = 'DirEveRetLot'
      Origin = 'paramsemp.DirEveRetLot'
      Size = 255
    end
    object QrPrmsEmpNFeDirEvePedCCe: TWideStringField
      FieldName = 'DirEvePedCCe'
      Origin = 'paramsemp.DirEvePedCCe'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetCCe: TWideStringField
      FieldName = 'DirEveRetCCe'
      Origin = 'paramsemp.DirEveRetCCe'
      Size = 255
    end
    object QrPrmsEmpNFeDirEvePedCan: TWideStringField
      FieldName = 'DirEvePedCan'
      Origin = 'paramsemp.DirEvePedCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetCan: TWideStringField
      FieldName = 'DirEveRetCan'
      Origin = 'paramsemp.DirEveRetCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveProcCCe: TWideStringField
      FieldName = 'DirEveProcCCe'
      Origin = 'paramsemp.DirEveProcCCe'
      Size = 255
    end
    object QrPrmsEmpNFeDirSchema: TWideStringField
      FieldName = 'DirSchema'
      Origin = 'paramsemp.DirSchema'
      Size = 255
    end
    object QrPrmsEmpNFeversao: TFloatField
      FieldName = 'versao'
      Origin = 'paramsemp.versao'
      Required = True
    end
    object QrPrmsEmpNFeide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'paramsemp.ide_mod'
      Required = True
    end
    object QrPrmsEmpNFeide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'paramsemp.ide_tpAmb'
      Required = True
    end
    object QrPrmsEmpNFeNT2018_05v120: TSmallintField
      FieldName = 'NT2018_05v120'
      Origin = 'paramsemp.NT2018_05v120'
      Required = True
    end
    object QrPrmsEmpNFeAppCode: TSmallintField
      FieldName = 'AppCode'
      Origin = 'paramsemp.AppCode'
      Required = True
    end
    object QrPrmsEmpNFeAssDigMode: TSmallintField
      FieldName = 'AssDigMode'
      Origin = 'paramsemp.AssDigMode'
      Required = True
    end
    object QrPrmsEmpNFeMyEmailNFe: TWideStringField
      FieldName = 'MyEmailNFe'
      Origin = 'paramsemp.MyEmailNFe'
      Size = 255
    end
    object QrPrmsEmpNFeCRT: TSmallintField
      FieldName = 'CRT'
      Origin = 'paramsemp.CRT'
      Required = True
    end
    object QrPrmsEmpNFeCSOSN: TIntegerField
      FieldName = 'CSOSN'
      Origin = 'paramsemp.CSOSN'
      Required = True
    end
    object QrPrmsEmpNFepCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
      Origin = 'paramsemp.pCredSNAlq'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerStaSer: TFloatField
      FieldName = 'NFeVerStaSer'
      Origin = 'paramsemp.NFeVerStaSer'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerEnvLot: TFloatField
      FieldName = 'NFeVerEnvLot'
      Origin = 'paramsemp.NFeVerEnvLot'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerConLot: TFloatField
      FieldName = 'NFeVerConLot'
      Origin = 'paramsemp.NFeVerConLot'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerCanNFe: TFloatField
      FieldName = 'NFeVerCanNFe'
      Origin = 'paramsemp.NFeVerCanNFe'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerInuNum: TFloatField
      FieldName = 'NFeVerInuNum'
      Origin = 'paramsemp.NFeVerInuNum'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerConNFe: TFloatField
      FieldName = 'NFeVerConNFe'
      Origin = 'paramsemp.NFeVerConNFe'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerLotEve: TFloatField
      FieldName = 'NFeVerLotEve'
      Origin = 'paramsemp.NFeVerLotEve'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerConsCad: TFloatField
      FieldName = 'NFeVerConsCad'
      Origin = 'paramsemp.NFeVerConsCad'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerDistDFeInt: TFloatField
      FieldName = 'NFeVerDistDFeInt'
      Origin = 'paramsemp.NFeVerDistDFeInt'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerDowNFe: TFloatField
      FieldName = 'NFeVerDowNFe'
      Origin = 'paramsemp.NFeVerDowNFe'
      Required = True
    end
    object QrPrmsEmpNFeide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Origin = 'paramsemp.ide_tpImp'
      Required = True
    end
    object QrPrmsEmpNFeNFeItsLin: TSmallintField
      FieldName = 'NFeItsLin'
      Origin = 'paramsemp.NFeItsLin'
      Required = True
    end
    object QrPrmsEmpNFeNFeFTRazao: TSmallintField
      FieldName = 'NFeFTRazao'
      Origin = 'paramsemp.NFeFTRazao'
      Required = True
    end
    object QrPrmsEmpNFeNFeFTEnder: TSmallintField
      FieldName = 'NFeFTEnder'
      Origin = 'paramsemp.NFeFTEnder'
      Required = True
    end
    object QrPrmsEmpNFeNFeFTFones: TSmallintField
      FieldName = 'NFeFTFones'
      Origin = 'paramsemp.NFeFTFones'
      Required = True
    end
    object QrPrmsEmpNFePathLogoNF: TWideStringField
      FieldName = 'PathLogoNF'
      Origin = 'paramsemp.PathLogoNF'
      Size = 255
    end
    object QrPrmsEmpNFeDirDANFEs: TWideStringField
      FieldName = 'DirDANFEs'
      Origin = 'paramsemp.DirDANFEs'
      Size = 255
    end
    object QrPrmsEmpNFeNFe_indFinalCpl: TSmallintField
      FieldName = 'NFe_indFinalCpl'
      Origin = 'paramsemp.NFe_indFinalCpl'
      Required = True
    end
    object QrPrmsEmpNFeNoDANFEMail: TSmallintField
      FieldName = 'NoDANFEMail'
      Origin = 'paramsemp.NoDANFEMail'
      Required = True
    end
    object QrPrmsEmpNFeNFeMaiusc: TSmallintField
      FieldName = 'NFeMaiusc'
      Origin = 'paramsemp.NFeMaiusc'
      Required = True
    end
    object QrPrmsEmpNFeNFeShowURL: TWideStringField
      FieldName = 'NFeShowURL'
      Origin = 'paramsemp.NFeShowURL'
      Size = 255
    end
    object QrPrmsEmpNFeNFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
      Origin = 'paramsemp.NFeNT2013_003LTT'
      Required = True
    end
    object QrPrmsEmpNFeCSC: TWideStringField
      FieldName = 'CSC'
      Origin = 'paramsemp.CSC'
      Size = 36
    end
    object QrPrmsEmpNFeCSCpos: TWideStringField
      FieldName = 'CSCpos'
      Origin = 'paramsemp.CSCpos'
      Size = 6
    end
    object QrPrmsEmpNFeTipoPrintNFCe: TWideStringField
      FieldName = 'TipoPrintNFCe'
      Size = 60
    end
    object QrPrmsEmpNFeNomePrintNFCe: TWideStringField
      FieldName = 'NomePrintNFCe'
      Size = 60
    end
    object QrPrmsEmpNFeIPPrintNFCe: TWideStringField
      FieldName = 'IPPrintNFCe'
      Size = 60
    end
    object QrPrmsEmpNFeAutoCutNFCe: TSmallintField
      FieldName = 'AutoCutNFCe'
    end
    object QrPrmsEmpNFeCertDigPfxCam: TWideStringField
      FieldName = 'CertDigPfxCam'
      Size = 255
    end
    object QrPrmsEmpNFeCertDigPfxPwd: TWideStringField
      FieldName = 'CertDigPfxPwd'
      Size = 255
    end
    object QrPrmsEmpNFeCertDigPfxWay: TSmallintField
      FieldName = 'CertDigPfxWay'
    end
    object QrPrmsEmpNFeNFeUsoCEST: TSmallintField
      FieldName = 'NFeUsoCEST'
    end
  end
  object MySQLQuery1: TMySQLQuery
    Database = RV_CEP_DB
    Left = 552
    Top = 592
  end
  object MySQLDatabase1: TMySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 476
    Top = 632
  end
  object MyPID_CompressDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 80
    Top = 508
  end
  object MyCompressDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 76
    Top = 556
  end
  object MySyncDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 596
    Top = 4
  end
  object MyLocDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 672
    Top = 7
  end
  object DBDmk: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 732
    Top = 4
  end
  object MyPID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 804
    Top = 4
  end
  object AllID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 1040
  end
  object QrPediSit: TMySQLQuery
    Database = AllID_DB
    Left = 672
    Top = 152
    object QrPediSitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediSitNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object RV_CEP_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 644
    Top = 540
  end
  object QrRV_CEP: TMySQLQuery
    Database = RV_CEP_DB
    Left = 644
    Top = 592
  end
end
