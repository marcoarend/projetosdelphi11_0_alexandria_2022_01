﻿unit ModuleGeral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, frxClass, frxDBSet, stdctrls, Variants, Menus,
  ExtCtrls, Registry, TypInfo, Grids, DBGrids, ShellApi, Vcl.Buttons, Vcl.ToolWin;

type
  THackDBGrid = class(TDBGrid);
  THackStringGrid = class(TStringGrid);
  TDModG = class(TDataModule)
    Tb_Empresas: TMySQLTable;
    Tb_EmpresasCodCliInt: TIntegerField;
    Tb_EmpresasCodEnti: TIntegerField;
    Tb_EmpresasCodFilial: TIntegerField;
    Tb_EmpresasNome: TWideStringField;
    Tb_EmpresasAtivo: TSmallintField;
    Ds_Empresas: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }

end;

var
  DModG: TDModG;

implementation


{$R *.DFM}


end.

