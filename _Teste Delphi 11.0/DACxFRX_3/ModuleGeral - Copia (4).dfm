object DModG: TDModG
  Height = 756
  Width = 1146
  PixelsPerInch = 96
  object QrUpdPID1: TMySQLQuery
    Database = MyPID_DB
    Left = 800
    Top = 52
  end
  object QrUpdL: TMySQLQuery
    Database = RV_CEP_DB
    Left = 672
    Top = 52
  end
  object QrAux_: TMySQLQuery
    Database = RV_CEP_DB
    Left = 748
    Top = 116
  end
  object DsSituacao: TDataSource
    Left = 672
    Top = 104
  end
  object QrUpdPID2: TMySQLQuery
    Database = RV_CEP_DB
    Left = 800
    Top = 100
  end
  object QrAuxPID1: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    Top = 100
  end
  object QrTermiServ: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SerialKey, SerialNum'
      'FROM terminais'
      'WHERE Servidor = 1'
      'ORDER BY Terminal')
    Left = 612
    Top = 52
    object QrTermiServSerialKey: TWideStringField
      FieldName = 'SerialKey'
      Size = 32
    end
    object QrTermiServSerialNum: TWideStringField
      FieldName = 'SerialNum'
      Size = 32
    end
  end
  object QrDefEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Filial, CliInt'
      'FROM entidades '
      'WHERE Codigo=:P0')
    Left = 736
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDefEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefEntiFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrDefEntiCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDefEntiNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrParOrfRep: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    object QrParOrfRepData: TDateField
      DisplayWidth = 12
      FieldName = 'Data'
    end
    object QrParOrfRepCarteira: TIntegerField
      DisplayWidth = 12
      FieldName = 'Carteira'
    end
    object QrParOrfRepControle: TIntegerField
      DisplayWidth = 12
      FieldName = 'Controle'
    end
    object QrParOrfRepDescricao: TWideStringField
      DisplayWidth = 57
      FieldName = 'Descricao'
      Size = 100
    end
    object QrParOrfRepCredito: TFloatField
      DisplayWidth = 12
      FieldName = 'Credito'
    end
    object QrParOrfRepVencimento: TDateField
      DisplayWidth = 12
      FieldName = 'Vencimento'
    end
    object QrParOrfRepCompensado: TDateField
      DisplayWidth = 13
      FieldName = 'Compensado'
    end
    object QrParOrfRepFatNum: TFloatField
      DisplayWidth = 12
      FieldName = 'FatNum'
    end
    object QrParOrfRepFatParcela: TIntegerField
      DisplayWidth = 12
      FieldName = 'FatParcela'
    end
    object QrParOrfRepMez: TIntegerField
      DisplayWidth = 12
      FieldName = 'Mez'
    end
    object QrParOrfRepCliente: TIntegerField
      DisplayWidth = 12
      FieldName = 'Cliente'
    end
    object QrParOrfRepCliInt: TIntegerField
      DisplayWidth = 12
      FieldName = 'CliInt'
    end
    object QrParOrfRepForneceI: TIntegerField
      DisplayWidth = 12
      FieldName = 'ForneceI'
    end
    object QrParOrfRepDepto: TIntegerField
      DisplayWidth = 12
      FieldName = 'Depto'
    end
    object QrParOrfRepAtrelado: TIntegerField
      DisplayWidth = 12
      FieldName = 'Atrelado'
    end
  end
  object DsParOrfRep: TDataSource
    DataSet = QrParOrfRep
    Left = 864
    Top = 48
  end
  object Tb_Empresas: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_empresas_'
    Left = 960
    Top = 4
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    DataSet = Tb_Empresas
    Left = 960
    Top = 56
  end
  object Tb_Indi_Pags: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_indi_pags_'
    Left = 964
    Top = 104
    object Tb_Indi_PagsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Tb_Indi_PagsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_Indi_PagsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Indi_Pags: TDataSource
    DataSet = Tb_Indi_Pags
    Left = 964
    Top = 156
  end
  object QrAllUpd: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 56
  end
  object QrAllAux: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 104
  end
  object MySyncDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 596
    Top = 4
  end
  object MyLocDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 672
    Top = 7
  end
  object DBDmk: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 732
    Top = 4
  end
  object MyPID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 804
    Top = 4
  end
  object AllID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 1040
  end
  object RV_CEP_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 644
    Top = 540
  end
  object QrRV_CEP: TMySQLQuery
    Database = RV_CEP_DB
    Left = 644
    Top = 592
  end
end
