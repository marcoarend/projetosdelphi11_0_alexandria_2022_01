object DModG: TDModG
  Height = 756
  Width = 1146
  PixelsPerInch = 96
  object QrUpdPID1: TMySQLQuery
    Database = MyPID_DB
    Left = 800
    Top = 52
  end
  object QrUpdL: TMySQLQuery
    Database = RV_CEP_DB
    Left = 672
    Top = 52
  end
  object QrAux_: TMySQLQuery
    Database = RV_CEP_DB
    Left = 748
    Top = 116
  end
  object DsSituacao: TDataSource
    DataSet = QrPediSit
    Left = 672
    Top = 104
  end
  object QrLocCI: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, PercJuros, PercMulta, VTCBBNITAR'
      'FROM cond'
      'WHERE Cliente=:P0'
      '')
    Left = 820
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCIPercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrLocCIPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrLocCIVTCBBNITAR: TFloatField
      FieldName = 'VTCBBNITAR'
    end
  end
  object QrUpdPID2: TMySQLQuery
    Database = RV_CEP_DB
    Left = 800
    Top = 100
  end
  object QrLocLogr: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM listalograd'
      'WHERE Nome=:P0'
      '')
    Left = 820
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocLogrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrPEM_T: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao, pem.Controle, '
      'pem.Ordem, pem.Tipo, pem.CidID_Img'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'ORDER BY Ordem')
    Left = 628
    Top = 460
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_TTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_TDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrPEM_TControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPEM_TOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrPEM_TTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPEM_TCidID_Img: TWideStringField
      FieldName = 'CidID_Img'
      Size = 32
    end
  end
  object QrPEM_2: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao, pem.Controle,'
      'pem.CidID_Img'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND Tipo=2')
    Left = 628
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_2Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrPEM_2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPEM_2CidID_Img: TWideStringField
      FieldName = 'CidID_Img'
      Size = 32
    end
  end
  object QrPEM_1: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND Tipo=1')
    Left = 628
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_1Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object QrPEM_0: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pem.Texto, pem.Descricao'
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND Tipo=0')
    Left = 628
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPEM_0Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPEM_0Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 692
    Top = 220
  end
  object QrPreEmail: TMySQLQuery
    Database = RV_CEP_DB
    Left = 628
    Top = 220
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPreEmailSMTPServer: TWideStringField
      FieldName = 'SMTPServer'
      Size = 50
    end
    object QrPreEmailSend_Name: TWideStringField
      FieldName = 'Send_Name'
      Size = 50
    end
    object QrPreEmailSend_Mail: TWideStringField
      FieldName = 'Send_Mail'
      Size = 50
    end
    object QrPreEmailPass_Mail: TWideStringField
      FieldName = 'Pass_Mail'
      Size = 30
    end
    object QrPreEmailLogi_Name: TWideStringField
      FieldName = 'Logi_Name'
      Size = 30
    end
    object QrPreEmailLogi_Pass: TWideStringField
      FieldName = 'Logi_Pass'
      Size = 30
    end
    object QrPreEmailLogi_Auth: TSmallintField
      FieldName = 'Logi_Auth'
    end
    object QrPreEmailMail_Titu: TWideStringField
      FieldName = 'Mail_Titu'
      Size = 100
    end
    object QrPreEmailSaudacao: TWideStringField
      FieldName = 'Saudacao'
      Size = 100
    end
    object QrPreEmailNaoEnvBloq: TSmallintField
      FieldName = 'NaoEnvBloq'
    end
    object QrPreEmailPorta_mail: TIntegerField
      FieldName = 'Porta_mail'
    end
    object QrPreEmailLogi_SSL: TSmallintField
      FieldName = 'Logi_SSL'
    end
    object QrPreEmailEmailConta: TIntegerField
      FieldName = 'EmailConta'
    end
  end
  object QrPreEmMsg: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ELT(pem.Tipo+1, '
      #39'Plano'#39', '#39'HTML'#39', '#39'Imagem'#39') NOMETIPO, pem.* '
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'ORDER BY Ordem, Controle, Tipo')
    Left = 628
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreEmMsgNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Size = 10
    end
    object QrPreEmMsgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'preemmsg.Codigo'
    end
    object QrPreEmMsgControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'preemmsg.Controle'
    end
    object QrPreEmMsgOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'preemmsg.Ordem'
    end
    object QrPreEmMsgTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'preemmsg.Tipo'
    end
    object QrPreEmMsgTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'preemmsg.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPreEmMsgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'preemmsg.Lk'
    end
    object QrPreEmMsgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'preemmsg.DataCad'
    end
    object QrPreEmMsgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'preemmsg.DataAlt'
    end
    object QrPreEmMsgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'preemmsg.UserCad'
    end
    object QrPreEmMsgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'preemmsg.UserAlt'
    end
    object QrPreEmMsgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'preemmsg.AlterWeb'
    end
    object QrPreEmMsgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'preemmsg.Ativo'
      MaxValue = 1
    end
    object QrPreEmMsgDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'preemmsg.Descricao'
      Size = 255
    end
  end
  object DsPreEmMsg: TDataSource
    DataSet = QrPreEmMsg
    Left = 692
    Top = 268
  end
  object QrLancto: TMySQLQuery
    Database = RV_CEP_DB
    Left = 820
    Top = 388
    object QrLanctoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLanctoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLanctoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLanctoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLanctoNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLanctoNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrLanctoData: TDateField
      FieldName = 'Data'
    end
    object QrLanctoVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLanctoBanco1: TIntegerField
      FieldName = 'Banco1'
    end
  end
  object QrPPI: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet,'
      'ppi.Texto, ppi.Lancto, ppi.Docum'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR'
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR'
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'AND ptk.Controle=:P0'
      'ORDER BY DataD, Conta'
      '')
    Left = 816
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPPINO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPPINO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPPILimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPISaiu: TIntegerField
      FieldName = 'Saiu'
      MaxValue = 1
    end
    object QrPPIDataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRecebeu: TIntegerField
      FieldName = 'Recebeu'
      MaxValue = 1
    end
    object QrPPIDataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRetornou: TIntegerField
      FieldName = 'Retornou'
      MaxValue = 1
    end
    object QrPPIDataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIDataSai_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataSai_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIDataRec_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRec_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIDataRet_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRet_TXT'
      Size = 8
      Calculated = True
    end
    object QrPPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPPIRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPPITexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrPPILancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
    end
  end
  object QrPTK: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT ptc.Nome, ptk.Controle Lote, ptk.Mez'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR '
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR '
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY ptk.Mez, ptk.Controle')
    Left = 816
    Top = 172
    object QrPTKNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPTKLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrPTKMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPTKMes_TXT: TWideStringField
      DisplayWidth = 5
      FieldKind = fkCalculated
      FieldName = 'Mes_TXT'
      Size = 5
      Calculated = True
    end
  end
  object DsPTK: TDataSource
    DataSet = QrPTK
    Left = 844
    Top = 172
  end
  object DsPPI: TDataSource
    DataSet = QrPPI
    Left = 844
    Top = 220
  end
  object QrPKA: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT  ptc.Nome, ptc.Codigo Lote, ptk.Mez,'
      'ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ('
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY DataD, Conta'
      '')
    Left = 816
    Top = 272
    object QrPKANome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPKALote: TIntegerField
      FieldName = 'Lote'
      Required = True
    end
    object QrPKAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPKADataE: TDateField
      FieldName = 'DataE'
    end
    object QrPKAConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPKARetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPKANO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPKANO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPKALimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKASaiu: TIntegerField
      FieldName = 'Saiu'
    end
    object QrPKADataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKALimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKARecebeu: TIntegerField
      FieldName = 'Recebeu'
    end
    object QrPKADataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKALimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKARetornou: TIntegerField
      FieldName = 'Retornou'
    end
    object QrPKADataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPKADataSai_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataSai_TXT'
      Size = 8
      Calculated = True
    end
    object QrPKADataRec_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRec_TXT'
      Size = 8
      Calculated = True
    end
    object QrPKADataRet_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataRet_TXT'
      Size = 8
      Calculated = True
    end
  end
  object DsPKA: TDataSource
    DataSet = QrPKA
    Left = 844
    Top = 272
  end
  object QrMeses: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT'
      '((DAY(LAST_DAY(:P0)) + 0.0000)  -'
      '(DAY(:P1) + 0.0000)) /'
      '((DAY(LAST_DAY(:P2))+0.0000)) Ini,'
      'PERIOD_DIFF(:P3,:P4)-1 Meio,'
      '(DAY(:P5) + 0.0000)/'
      '(DAY(LAST_DAY(:P6)) + 0.0000) Fim'
      '')
    Left = 688
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrMesesIni: TFloatField
      FieldName = 'Ini'
    end
    object QrMesesMeio: TFloatField
      FieldName = 'Meio'
    end
    object QrMesesFim: TFloatField
      FieldName = 'Fim'
    end
  end
  object QrProtoMail: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM protomail'
      'ORDER BY NivelEmail')
    Left = 729
    Top = 457
    object QrProtoMailDepto_Cod: TIntegerField
      FieldName = 'Depto_Cod'
      Origin = 'protomail.Depto_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailDepto_Txt: TWideStringField
      FieldName = 'Depto_Txt'
      Origin = 'protomail.Depto_Txt'
      Size = 100
    end
    object QrProtoMailEntid_Cod: TIntegerField
      FieldName = 'Entid_Cod'
      Origin = 'protomail.Entid_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailEntid_Txt: TWideStringField
      FieldName = 'Entid_Txt'
      Origin = 'protomail.Entid_Txt'
      Size = 100
    end
    object QrProtoMailTaref_Cod: TIntegerField
      FieldName = 'Taref_Cod'
      Origin = 'protomail.Taref_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailTaref_Txt: TWideStringField
      FieldName = 'Taref_Txt'
      Origin = 'protomail.Taref_Txt'
      Size = 100
    end
    object QrProtoMailDeliv_Cod: TIntegerField
      FieldName = 'Deliv_Cod'
      Origin = 'protomail.Deliv_Cod'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailDeliv_Txt: TWideStringField
      FieldName = 'Deliv_Txt'
      Origin = 'protomail.Deliv_Txt'
      Size = 100
    end
    object QrProtoMailDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protomail.DataE'
    end
    object QrProtoMailDataE_Txt: TWideStringField
      FieldName = 'DataE_Txt'
      Origin = 'protomail.DataE_Txt'
      Size = 30
    end
    object QrProtoMailDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protomail.DataD'
    end
    object QrProtoMailDataD_Txt: TWideStringField
      FieldName = 'DataD_Txt'
      Origin = 'protomail.DataD_Txt'
      Size = 30
    end
    object QrProtoMailProtoLote: TIntegerField
      FieldName = 'ProtoLote'
      Origin = 'protomail.ProtoLote'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailNivelEmail: TSmallintField
      FieldName = 'NivelEmail'
      Origin = 'protomail.NivelEmail'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailItem: TIntegerField
      FieldName = 'Item'
      Origin = 'protomail.Item'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailNivelDescr: TWideStringField
      FieldName = 'NivelDescr'
      Origin = 'protomail.NivelDescr'
      Size = 50
    end
    object QrProtoMailProtocolo: TIntegerField
      FieldName = 'Protocolo'
      Origin = 'protomail.Protocolo'
      DisplayFormat = '000000;-000000; '
    end
    object QrProtoMailRecipEmeio: TWideStringField
      FieldName = 'RecipEmeio'
      Size = 100
    end
    object QrProtoMailRecipNome: TWideStringField
      FieldName = 'RecipNome'
      Size = 100
    end
    object QrProtoMailRecipProno: TWideStringField
      FieldName = 'RecipProno'
    end
    object QrProtoMailRecipItem: TIntegerField
      FieldName = 'RecipItem'
    end
    object QrProtoMailBloqueto: TFloatField
      FieldName = 'Bloqueto'
    end
    object QrProtoMailVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrProtoMailValor: TFloatField
      FieldName = 'Valor'
    end
    object QrProtoMailPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrProtoMailCondominio: TWideStringField
      FieldName = 'Condominio'
      Size = 100
    end
    object QrProtoMailIDEmeio: TIntegerField
      FieldName = 'IDEmeio'
    end
  end
  object QrEntiMail: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ema.EMail'
      'FROM entimail ema'
      'WHERE ema.Codigo=:P0'
      'AND ema.EntiTipCto=:P1  '
      'ORDER BY ema.Ordem, ema.Conta'#10
      '')
    Left = 692
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
  end
  object QrAuxPID1: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    Top = 100
  end
  object QrTermiServ: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SerialKey, SerialNum'
      'FROM terminais'
      'WHERE Servidor = 1'
      'ORDER BY Terminal')
    Left = 612
    Top = 52
    object QrTermiServSerialKey: TWideStringField
      FieldName = 'SerialKey'
      Size = 32
    end
    object QrTermiServSerialNum: TWideStringField
      FieldName = 'SerialNum'
      Size = 32
    end
  end
  object QrDefEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Filial, CliInt'
      'FROM entidades '
      'WHERE Codigo=:P0')
    Left = 736
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDefEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDefEntiFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrDefEntiCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDefEntiNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrParOrfRep: TMySQLQuery
    Database = RV_CEP_DB
    Left = 864
    object QrParOrfRepData: TDateField
      DisplayWidth = 12
      FieldName = 'Data'
    end
    object QrParOrfRepCarteira: TIntegerField
      DisplayWidth = 12
      FieldName = 'Carteira'
    end
    object QrParOrfRepControle: TIntegerField
      DisplayWidth = 12
      FieldName = 'Controle'
    end
    object QrParOrfRepDescricao: TWideStringField
      DisplayWidth = 57
      FieldName = 'Descricao'
      Size = 100
    end
    object QrParOrfRepCredito: TFloatField
      DisplayWidth = 12
      FieldName = 'Credito'
    end
    object QrParOrfRepVencimento: TDateField
      DisplayWidth = 12
      FieldName = 'Vencimento'
    end
    object QrParOrfRepCompensado: TDateField
      DisplayWidth = 13
      FieldName = 'Compensado'
    end
    object QrParOrfRepFatNum: TFloatField
      DisplayWidth = 12
      FieldName = 'FatNum'
    end
    object QrParOrfRepFatParcela: TIntegerField
      DisplayWidth = 12
      FieldName = 'FatParcela'
    end
    object QrParOrfRepMez: TIntegerField
      DisplayWidth = 12
      FieldName = 'Mez'
    end
    object QrParOrfRepCliente: TIntegerField
      DisplayWidth = 12
      FieldName = 'Cliente'
    end
    object QrParOrfRepCliInt: TIntegerField
      DisplayWidth = 12
      FieldName = 'CliInt'
    end
    object QrParOrfRepForneceI: TIntegerField
      DisplayWidth = 12
      FieldName = 'ForneceI'
    end
    object QrParOrfRepDepto: TIntegerField
      DisplayWidth = 12
      FieldName = 'Depto'
    end
    object QrParOrfRepAtrelado: TIntegerField
      DisplayWidth = 12
      FieldName = 'Atrelado'
    end
  end
  object DsParOrfRep: TDataSource
    DataSet = QrParOrfRep
    Left = 864
    Top = 48
  end
  object QrCheque: TMySQLQuery
    Database = RV_CEP_DB
    Left = 768
    Top = 388
    object QrChequeBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrChequeDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrChequeCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrChequeFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrChequeCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrChequeNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrChequeNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrChequeData: TDateField
      FieldName = 'Data'
    end
    object QrChequeVencimento: TDateField
      FieldName = 'Vencimento'
    end
  end
  object Tb_Empresas: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_empresas_'
    Left = 960
    Top = 4
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    DataSet = Tb_Empresas
    Left = 960
    Top = 56
  end
  object Tb_Indi_Pags: TMySQLTable
    Database = RV_CEP_DB
    TableName = '_indi_pags_'
    Left = 964
    Top = 104
    object Tb_Indi_PagsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Tb_Indi_PagsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_Indi_PagsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Indi_Pags: TDataSource
    DataSet = Tb_Indi_Pags
    Left = 964
    Top = 156
  end
  object QrEnti: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT Tipo, IE, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENTI,'
      'IF(Tipo=0, EUF, PUF) + 0.000  UF_Cod,'
      'IF(Tipo=0, ECodMunici, PCodMunici)  + 0.000 CodMunici,'
      'IF(Tipo=0, ECodiPais, PCodiPais) + 0.000 CodiPais'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 960
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiUF_Cod: TFloatField
      FieldName = 'UF_Cod'
      Required = True
    end
    object QrEntiNO_ENTI: TWideStringField
      FieldName = 'NO_ENTI'
      Size = 100
    end
    object QrEntiCodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
    object QrEntiCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
  end
  object QrEntiCliInt: TMySQLQuery
    Database = RV_CEP_DB
    Left = 960
    Top = 264
  end
  object QrControle: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 924
    Top = 404
  end
  object QrPerfis: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM PerfisIts pit'
      
        'LEFT JOIN PerfisItsPerf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 988
    Top = 404
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrUTC: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT SYSDATE() DATA_LOC, '
      'UTC_TIMESTAMP() DATA_UTC, '
      'SYSDATE()- UTC_TIMESTAMP() UTC_Dif')
    Left = 960
    Top = 320
    object QrUTCDATA_LOC: TDateTimeField
      FieldName = 'DATA_LOC'
      Required = True
    end
    object QrUTCDATA_UTC: TDateTimeField
      FieldName = 'DATA_UTC'
      Required = True
    end
    object QrUTCUTC_Dif: TFloatField
      FieldName = 'UTC_Dif'
      Required = True
    end
  end
  object QrAllUpd: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 56
  end
  object QrAllAux: TMySQLQuery
    Database = RV_CEP_DB
    Left = 1020
    Top = 104
  end
  object QrEntNFS: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Tipo, ent.IE, ent.CNPJ, ent.CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ELograd, ent.PLograd) + 0.000 Lograd,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) Rua,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 Numero,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) Compl,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) Bairro,'
      
        'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CodMunici' +
        ','
      'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000  UF_Cod,'
      'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CodiPais,'
      'IF(ent.Tipo=0, ent.ECEP, ent.PCEP) + 0.000 CEP,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TE1,'
      'ent.NIRE,'
      'llo.Nome NO_Lograd'
      'FROM entidades ent'
      'LEFT JOIN listalograd llo ON llo.Codigo='
      '  IF(ent.Tipo=0, ent.PLograd, ent.ELograd)'
      'WHERE ent.Codigo=:P0'
      '')
    Left = 1012
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntNFSTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntNFSIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntNFSCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntNFSCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntNFSNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrEntNFSRua: TWideStringField
      FieldName = 'Rua'
      Size = 60
    end
    object QrEntNFSNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEntNFSCompl: TWideStringField
      FieldName = 'Compl'
      Size = 60
    end
    object QrEntNFSBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 60
    end
    object QrEntNFSUF_Cod: TFloatField
      FieldName = 'UF_Cod'
      Required = True
    end
    object QrEntNFSCodiPais: TFloatField
      FieldName = 'CodiPais'
      Required = True
    end
    object QrEntNFSNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEntNFSNO_Lograd: TWideStringField
      FieldName = 'NO_Lograd'
      Size = 15
    end
    object QrEntNFSTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntNFSLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrEntNFSCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntNFSCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
  end
  object QrSelCods: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM _selcods_'
      '')
    Left = 960
    Top = 458
    object QrSelCodsNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSelCodsNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrSelCodsNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrSelCodsNivel4: TIntegerField
      FieldName = 'Nivel4'
    end
    object QrSelCodsNivel5: TIntegerField
      FieldName = 'Nivel5'
    end
    object QrSelCodsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSelCodsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
      MaxValue = 1
    end
  end
  object DsSelCods: TDataSource
    DataSet = QrSelCods
    Left = 1020
    Top = 458
  end
  object QrProvisorio1: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT ece.Codigo NULO, ect.Codigo, ect.Controle '
      'FROM enticontat ect'
      'LEFT JOIN enticonent ece ON '
      '  ece.Controle=ect.Controle'
      '  AND ece.Codigo=ect.Codigo'
      'WHERE ece.Codigo IS Null'
      '')
    Left = 880
    Top = 336
    object QrProvisorio1NULO: TIntegerField
      FieldName = 'NULO'
      Required = True
    end
    object QrProvisorio1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProvisorio1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCambios: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM cambios'
      'ORDER BY Data DESC')
    Left = 760
    Top = 220
    object QrCambiosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambiosValor: TFloatField
      FieldName = 'Valor'
    end
    object QrCambiosDataC: TDateField
      FieldName = 'DataC'
    end
  end
  object DsCambios: TDataSource
    DataSet = QrCambios
    Left = 764
    Top = 268
  end
  object QrWebParams: TMySQLQuery
    Database = RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesgerl')
    Left = 1048
    Top = 292
  end
  object MySyncDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 596
    Top = 4
  end
  object MyLocDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 672
    Top = 7
  end
  object DBDmk: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 732
    Top = 4
  end
  object MyPID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 804
    Top = 4
  end
  object AllID_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 1040
  end
  object QrPediSit: TMySQLQuery
    Database = AllID_DB
    Left = 672
    Top = 152
    object QrPediSitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediSitNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object RV_CEP_DB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 644
    Top = 540
  end
  object QrRV_CEP: TMySQLQuery
    Database = RV_CEP_DB
    Left = 644
    Top = 592
  end
end
