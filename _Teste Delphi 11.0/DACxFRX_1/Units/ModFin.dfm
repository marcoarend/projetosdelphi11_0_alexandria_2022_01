object DMFin: TDMFin
  Height = 454
  Width = 611
  PixelsPerInch = 96
  object QrCreditos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.Mez, SUM(lct.Credito) Credito, '
      'lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,'
      'lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, '
      'lct.SubPgto1, '
      'con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU'
      'FROM syndic.lct0001a lct'
      'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lct.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE lct.Tipo <> 2'
      'AND lct.Credito > 0'
      'AND lct.Genero>0'
      'AND lct.Sit IN (0,1,2,3)'
      'AND car.ForneceI = :P0'
      'AND lct.Data BETWEEN :P1 AND :P2'
      'GROUP BY lct.Genero '
      ', lct.Mez'
      ', lct.SubPgto1'
      'ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,'
      'NOMESGR, con.OrdemLista, NOMECON, Mez, Data')
    Left = 24
    Top = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCreditosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrCreditosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCreditosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrCreditosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrCreditosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrCreditosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrCreditosNOMECON_2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECON_2'
      Size = 255
      Calculated = True
    end
    object QrCreditosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrCreditosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCreditosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCreditosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCreditosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrCreditosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCreditosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrCreditosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrCreditosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCreditosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object frxDsCreditos: TfrxDBDataset
    UserName = 'frxDsCreditos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Mez=Mez'
      'Credito=Credito'
      'NOMECON=NOMECON'
      'NOMESGR=NOMESGR'
      'NOMEGRU=NOMEGRU'
      'MES=MES'
      'NOMECON_2=NOMECON_2'
      'SubPgto1=SubPgto1'
      'Controle=Controle'
      'Sub=Sub'
      'Carteira=Carteira'
      'Cartao=Cartao'
      'Vencimento=Vencimento'
      'Compensado=Compensado'
      'Sit=Sit'
      'Genero=Genero'
      'Tipo=Tipo')
    DataSet = QrCreditos
    BCDToCurrency = False
    
    Left = 144
    Top = 28
  end
end
