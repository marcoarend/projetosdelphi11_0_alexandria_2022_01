unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons;

type
  TFmPrincipal = class(TForm)
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

{$R *.dfm}

uses Teste;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTeste, FmTeste);
  FmTeste.ShowModal;
  FmTeste.Destroy;
end;

end.
