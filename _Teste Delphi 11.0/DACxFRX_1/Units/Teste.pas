unit Teste;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, Data.DB, mySQLDbTables,
  frxDBSet, Vcl.StdCtrls, Vcl.Buttons;

type
  TFmTeste = class(TForm)
    frxFIN_BALAN_001_001: TfrxReport;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FmTeste: TFmTeste;

implementation

uses ModFin, ModuleFin;

{$R *.dfm}

procedure TFmTeste.BitBtn1Click(Sender: TObject);
begin
  frxFIN_BALAN_001_001.PrepareReport(True);
  frxFIN_BALAN_001_001.ShowPreparedReport();
end;

end.
