program DACxFRX;

uses
  Vcl.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  ModFin in '..\Units\ModFin.pas' {DMFin: TDataModule},
  Teste in '..\Units\Teste.pas' {FmTeste},
  ModuleFin in '..\Units\ModuleFin.pas' {DModFin: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TFmTeste, FmTeste);
  Application.CreateForm(TDMFin, DMFin);
  Application.CreateForm(TDModFin, DModFin);
  Application.Run;
end.
