program Project13;

uses
  Vcl.Forms,
  CashBal,
  Module,
  ModuleFin,
  Principal in 'Units\Principal.pas' {FmPrincipal};

uses
  Vcl.Forms,
  Unit15 in 'Unit15.pas' {Form15},
  CashBal,
  Module,
  ModuleFin - Copia in 'Units\ModuleFin - Copia.pas' {DModFin: TDataModule};

uses
  Vcl.Forms,
  Unit15 in 'Unit15.pas' {Form15},
  CashBal,
  Module - Copia in 'Units\Module - Copia.pas' {Dmod: TDataModule};

uses
  Vcl.Forms,
  Unit15 in 'Unit15.pas' {Form15},
  CashBal - Copia in 'Units\CashBal - Copia.pas' {FmCashBal};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmCashBal, FmCashBal);
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TDModFin, DModFin);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
