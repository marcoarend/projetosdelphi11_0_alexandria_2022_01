unit ModuleFin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, StdCtrls, ExtCtrls, DBCtrls, DBGrids, Variants,
  dmkGeral, dmkEditDateTimePicker, TypInfo, frxClass, frxDBSet, DmkDAC_PF,
  UnDmkProcFunc, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox;

type
  TDModFin = class(TDataModule)
    QrAPL: TmySQLQuery;
    QrAPLCredito: TFloatField;
    QrAPLDebito: TFloatField;
    QrAPLMoraVal: TFloatField;
    QrAPLMultaVal: TFloatField;
    QrAPLData: TDateField;
    QrVLA: TmySQLQuery;
    QrVLACredito: TFloatField;
    QrVLADebito: TFloatField;
    QrVLAEndossan: TFloatField;
    QrVLAEndossad: TFloatField;
    QrVLATipo: TSmallintField;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    QrTransfSerieCH: TWideStringField;
    QrUDAPC: TmySQLQuery;
    QrTrfCtas: TmySQLQuery;
    QrTrfCtasData: TDateField;
    QrTrfCtasTipo: TSmallintField;
    QrTrfCtasCarteira: TIntegerField;
    QrTrfCtasControle: TIntegerField;
    QrTrfCtasGenero: TIntegerField;
    QrTrfCtasDebito: TFloatField;
    QrTrfCtasCredito: TFloatField;
    QrTrfCtasDocumento: TFloatField;
    QrTrfCtasSerieCH: TWideStringField;
    QrSaldo: TmySQLQuery;
    QrSaldoValor: TFloatField;
    QrLocLct: TmySQLQuery;
    QrLocLctData: TDateField;
    QrLocLctTipo: TSmallintField;
    QrLocLctCarteira: TIntegerField;
    QrLocLctControle: TIntegerField;
    QrLocLctSub: TSmallintField;
    QrLocLctAutorizacao: TIntegerField;
    QrLocLctGenero: TIntegerField;
    QrLocLctDescricao: TWideStringField;
    QrLocLctNotaFiscal: TIntegerField;
    QrLocLctDebito: TFloatField;
    QrLocLctCredito: TFloatField;
    QrLocLctCompensado: TDateField;
    QrLocLctDocumento: TFloatField;
    QrLocLctSit: TIntegerField;
    QrLocLctVencimento: TDateField;
    QrLocLctLk: TIntegerField;
    QrLocLctFatID: TIntegerField;
    QrLocLctFatParcela: TIntegerField;
    QrLocLctID_Pgto: TIntegerField;
    QrLocLctID_Sub: TSmallintField;
    QrLocLctFatura: TWideStringField;
    QrLocLctBanco: TIntegerField;
    QrLocLctLocal: TIntegerField;
    QrLocLctCartao: TIntegerField;
    QrLocLctLinha: TIntegerField;
    QrLocLctOperCount: TIntegerField;
    QrLocLctLancto: TIntegerField;
    QrLocLctPago: TFloatField;
    QrLocLctMez: TIntegerField;
    QrLocLctFornecedor: TIntegerField;
    QrLocLctCliente: TIntegerField;
    QrLocLctMoraDia: TFloatField;
    QrLocLctMulta: TFloatField;
    QrLocLctProtesto: TDateField;
    QrLocLctDataCad: TDateField;
    QrLocLctDataAlt: TDateField;
    QrLocLctUserCad: TSmallintField;
    QrLocLctUserAlt: TSmallintField;
    QrLocLctDataDoc: TDateField;
    QrLocLctCtrlIni: TIntegerField;
    QrLocLctNivel: TIntegerField;
    QrLocLctVendedor: TIntegerField;
    QrLocLctAccount: TIntegerField;
    QrLocLctFatNum: TFloatField;
    QrLocLcto: TmySQLQuery;
    QrLocLctoData: TDateField;
    QrLocLctoControle: TIntegerField;
    QrLocLctoTipo: TSmallintField;
    QrLocLctoCarteira: TIntegerField;
    QrLocLctoCliInt: TIntegerField;
    QrLocCta: TmySQLQuery;
    QrLocCtaMensal: TWideStringField;
    QrLocUHs: TmySQLQuery;
    QrLocUHsDepto: TIntegerField;
    QrLocUHsNO_DEPTO: TWideStringField;
    QrELSCI: TmySQLQuery;
    QrELSCIData: TDateField;
    QrELSCITipo: TSmallintField;
    QrELSCICarteira: TIntegerField;
    QrELSCIAutorizacao: TIntegerField;
    QrELSCIGenero: TIntegerField;
    QrELSCIDescricao: TWideStringField;
    QrELSCINotaFiscal: TIntegerField;
    QrELSCIDebito: TFloatField;
    QrELSCICredito: TFloatField;
    QrELSCICompensado: TDateField;
    QrELSCIDocumento: TFloatField;
    QrELSCISit: TIntegerField;
    QrELSCIVencimento: TDateField;
    QrELSCILk: TIntegerField;
    QrELSCIFatID: TIntegerField;
    QrELSCIFatParcela: TIntegerField;
    QrELSCICONTA: TIntegerField;
    QrELSCINOMECONTA: TWideStringField;
    QrELSCINOMEEMPRESA: TWideStringField;
    QrELSCINOMESUBGRUPO: TWideStringField;
    QrELSCINOMEGRUPO: TWideStringField;
    QrELSCINOMECONJUNTO: TWideStringField;
    QrELSCINOMESIT: TWideStringField;
    QrELSCIAno: TFloatField;
    QrELSCIMENSAL: TWideStringField;
    QrELSCIMENSAL2: TWideStringField;
    QrELSCIBanco: TIntegerField;
    QrELSCILocal: TIntegerField;
    QrELSCIFatura: TWideStringField;
    QrELSCISub: TSmallintField;
    QrELSCICartao: TIntegerField;
    QrELSCILinha: TIntegerField;
    QrELSCIPago: TFloatField;
    QrELSCISALDO: TFloatField;
    QrELSCIID_Sub: TSmallintField;
    QrELSCIMez: TIntegerField;
    QrELSCIFornecedor: TIntegerField;
    QrELSCIcliente: TIntegerField;
    QrELSCIMoraDia: TFloatField;
    QrELSCINOMECLIENTE: TWideStringField;
    QrELSCINOMEFORNECEDOR: TWideStringField;
    QrELSCITIPOEM: TWideStringField;
    QrELSCINOMERELACIONADO: TWideStringField;
    QrELSCIOperCount: TIntegerField;
    QrELSCILancto: TIntegerField;
    QrELSCIMulta: TFloatField;
    QrELSCIATRASO: TFloatField;
    QrELSCIJUROS: TFloatField;
    QrELSCIDataDoc: TDateField;
    QrELSCINivel: TIntegerField;
    QrELSCIVendedor: TIntegerField;
    QrELSCIAccount: TIntegerField;
    QrELSCIMes2: TLargeintField;
    QrELSCIProtesto: TDateField;
    QrELSCIDataCad: TDateField;
    QrELSCIDataAlt: TDateField;
    QrELSCIUserCad: TSmallintField;
    QrELSCIUserAlt: TSmallintField;
    QrELSCIControle: TIntegerField;
    QrELSCIID_Pgto: TIntegerField;
    QrELSCICtrlIni: TIntegerField;
    QrELSCIFatID_Sub: TIntegerField;
    QrELSCIICMS_P: TFloatField;
    QrELSCIICMS_V: TFloatField;
    QrELSCIDuplicata: TWideStringField;
    QrELSCICOMPENSADO_TXT: TWideStringField;
    QrELSCICliInt: TIntegerField;
    QrELSCIDepto: TIntegerField;
    QrELSCIDescoPor: TIntegerField;
    QrELSCIPrazo: TSmallintField;
    QrELSCIForneceI: TIntegerField;
    QrELSCIQtde: TFloatField;
    QrELSCIEmitente: TWideStringField;
    QrELSCIAgencia: TIntegerField;
    QrELSCIContaCorrente: TWideStringField;
    QrELSCICNPJCPF: TWideStringField;
    QrELSCIDescoVal: TFloatField;
    QrELSCIDescoControle: TIntegerField;
    QrELSCIUnidade: TIntegerField;
    QrELSCINFVal: TFloatField;
    QrELSCIAntigo: TWideStringField;
    QrELSCIExcelGru: TIntegerField;
    QrELSCISerieCH: TWideStringField;
    QrELSCISERIE_CHEQUE: TWideStringField;
    QrELSCIDoc2: TWideStringField;
    QrELSCINOMEFORNECEI: TWideStringField;
    QrELSCIMoraVal: TFloatField;
    QrELSCIMultaVal: TFloatField;
    QrELSCICNAB_Sit: TSmallintField;
    QrELSCIBanco1: TIntegerField;
    QrELSCIAgencia1: TIntegerField;
    QrELSCIConta1: TWideStringField;
    QrELSCITipoCH: TSmallintField;
    QrELSCIAlterWeb: TSmallintField;
    QrELSCIReparcel: TIntegerField;
    QrELSCIID_Quit: TIntegerField;
    QrELSCIAtrelado: TIntegerField;
    QrELSCIAtivo: TSmallintField;
    QrELSCIFatNum: TFloatField;
    QrELSCIProtocolo: TIntegerField;
    QrELSCIPagMul: TFloatField;
    QrELSCIPagJur: TFloatField;
    QrELSCICART_DONO: TIntegerField;
    QrELSCINOMECARTEIRA: TWideStringField;
    QrCliInt: TmySQLQuery;
    QrCliIntPBB: TSmallintField;
    QrSTCP: TmySQLQuery;
    QrSTCPSUMMOV: TFloatField;
    QrSTCPSDOANT: TFloatField;
    QrSTCPSUMCRE: TFloatField;
    QrSTCPSUMDEB: TFloatField;
    QrSTCPSDOFIM: TFloatField;
    frxDsSTCP: TfrxDBDataset;
    QrExtratos: TmySQLQuery;
    QrExtratosLinha: TAutoIncField;
    QrExtratosDataM: TDateField;
    QrExtratosTexto: TWideStringField;
    QrExtratosDocum: TWideStringField;
    QrExtratosNotaF: TIntegerField;
    QrExtratosCredi: TFloatField;
    QrExtratosDebit: TFloatField;
    QrExtratosSaldo: TFloatField;
    QrExtratosCartO: TIntegerField;
    QrExtratosCartC: TIntegerField;
    QrExtratosCartN: TWideStringField;
    QrExtratosSdIni: TIntegerField;
    QrExtratosTipoI: TIntegerField;
    QrExtratosUnida: TWideStringField;
    QrExtratosCTipN: TWideStringField;
    QrExtratosDebCr: TWideStringField;
    frxDsExtratos: TfrxDBDataset;
    DsExtratos: TDataSource;
    QrZer: TmySQLQuery;
    QrZerCartC: TIntegerField;
    QrZerCredi: TFloatField;
    QrZerDebit: TFloatField;
    QrPesqCar: TmySQLQuery;
    QrPesqCarCarteira: TIntegerField;
    QrPesqCarInicial: TFloatField;
    QrPesqCarSALDO: TFloatField;
    QrPesqCarNome: TWideStringField;
    QrPesqCarNome2: TWideStringField;
    QrPesqCarOrdemCart: TIntegerField;
    QrPesqCarTipoCart: TIntegerField;
    QrPesqAgr: TmySQLQuery;
    QrPesqAgrContasAgr: TIntegerField;
    QrPesqAgrNOMECART: TWideStringField;
    QrPesqAgrNOME2CART: TWideStringField;
    QrPesqAgrCarteira: TIntegerField;
    QrPesqAgrData: TDateField;
    QrPesqAgrDescricao: TWideStringField;
    QrPesqAgrCREDITO: TFloatField;
    QrPesqAgrDEBITO: TFloatField;
    QrPesqAgrSerieCH: TWideStringField;
    QrPesqAgrDocumento: TFloatField;
    QrPesqAgrDoc2: TWideStringField;
    QrPesqAgrMez: TIntegerField;
    QrPesqAgrApto: TIntegerField;
    QrPesqAgrNOMEAGR: TWideStringField;
    QrPesqAgrInfoDescri: TSmallintField;
    QrPesqAgrMensal: TSmallintField;
    QrPesqAgrNOMECLI: TWideStringField;
    QrPesqAgrOrdemCart: TIntegerField;
    QrPesqAgrTipoCart: TIntegerField;
    QrPesqAgrNotaFiscal: TIntegerField;
    QrPesqRes: TmySQLQuery;
    QrPesqResNOMECART: TWideStringField;
    QrPesqResNOME2CART: TWideStringField;
    QrPesqResCarteira: TIntegerField;
    QrPesqResData: TDateField;
    QrPesqResDescricao: TWideStringField;
    QrPesqResCredito: TFloatField;
    QrPesqResDebito: TFloatField;
    QrPesqResMez: TIntegerField;
    QrPesqResSerieCH: TWideStringField;
    QrPesqResDocumento: TFloatField;
    QrPesqResOrdemCart: TIntegerField;
    QrPesqResDepto: TIntegerField;
    QrPesqResTipoCart: TIntegerField;
    QrPesqResNotaFiscal: TIntegerField;
    QrPesqSum: TmySQLQuery;
    QrPesqSumContasAgr: TIntegerField;
    QrPesqSumNOMECART: TWideStringField;
    QrPesqSumNOME2CART: TWideStringField;
    QrPesqSumCarteira: TIntegerField;
    QrPesqSumData: TDateField;
    QrPesqSumDescricao: TWideStringField;
    QrPesqSumCREDITO: TFloatField;
    QrPesqSumDEBITO: TFloatField;
    QrPesqSumMez: TIntegerField;
    QrPesqSumOrdemCart: TIntegerField;
    QrPesqSumTipoCart: TIntegerField;
    QrPesqSumNotaFiscal: TIntegerField;
    QrSdoTrf: TmySQLQuery;
    QrSdoTrfCarteira: TLargeintField;
    QrSdoTrfSDO_CRE: TFloatField;
    QrSdoTrfSDO_DEB: TFloatField;
    frxDsSdoExcl: TfrxDBDataset;
    QrSdoExcl: TmySQLQuery;
    QrSdoExclCarteira: TIntegerField;
    QrSdoExclNome: TWideStringField;
    QrSdoExclNome2: TWideStringField;
    QrSdoExclOrdemCart: TIntegerField;
    QrSdoExclSDO_CAD: TFloatField;
    QrSdoExclSDO_ANT: TFloatField;
    QrSdoExclSDO_INI: TFloatField;
    QrSdoExclMOV_CRE: TFloatField;
    QrSdoExclMOV_DEB: TFloatField;
    QrSdoExclTRF_CRE: TFloatField;
    QrSdoExclSDO_FIM: TFloatField;
    QrSdoExclTRF_DEB: TFloatField;
    QrSdoExclKGT: TLargeintField;
    QrSdoExclTipo: TIntegerField;
    QrSdoExclNO_TipoCart: TWideStringField;
    QrSdoMov: TmySQLQuery;
    QrSdoMovCarteira: TIntegerField;
    QrSdoMovSDO_CRE: TFloatField;
    QrSdoMovSDO_DEB: TFloatField;
    QrSdoCtas: TmySQLQuery;
    QrSdoCtasCarteira: TIntegerField;
    QrSdoCtasNome: TWideStringField;
    QrSdoCtasNome2: TWideStringField;
    QrSdoCtasOrdemCart: TIntegerField;
    QrSdoCtasSDO_CAD: TFloatField;
    QrSdoCtasSDO_ANT: TFloatField;
    QrSdoCtasSDO_INI: TFloatField;
    QrSdoCtasMOV_CRE: TFloatField;
    QrSdoCtasMOV_DEB: TFloatField;
    QrSdoCtasTRF_CRE: TFloatField;
    QrSdoCtasSDO_FIM: TFloatField;
    QrSdoCtasTRF_DEB: TFloatField;
    QrSdoCtasKGT: TLargeintField;
    QrSdoCtasTipo: TIntegerField;
    QrSdoCtasNO_TipoCart: TWideStringField;
    frxDsSdoCtas: TfrxDBDataset;
    QrCTCN: TmySQLQuery;
    QrSNC: TmySQLQuery;
    QrSNCNOME: TWideStringField;
    QrSNCSUMMOV: TFloatField;
    QrSNCSDOANT: TFloatField;
    QrSNCSUMCRE: TFloatField;
    QrSNCSUMDEB: TFloatField;
    QrSNCSDOFIM: TFloatField;
    QrSNCCodigo: TIntegerField;
    QrSNCCodPla: TIntegerField;
    QrSNCCodCjt: TIntegerField;
    QrSNCCodGru: TIntegerField;
    QrSNCCodSgr: TIntegerField;
    QrSNG: TmySQLQuery;
    QrSNGNivel: TIntegerField;
    QrSNGGenero: TIntegerField;
    QrSNGNomeGe: TWideStringField;
    QrSNGNomeNi: TWideStringField;
    QrSNGSumMov: TFloatField;
    QrSNGSdoAnt: TFloatField;
    QrSNGSumCre: TFloatField;
    QrSNGSumDeb: TFloatField;
    QrSNGSdoFim: TFloatField;
    QrSNGCtrla: TSmallintField;
    QrSNGSeleci: TSmallintField;
    QrSNGCodPla: TIntegerField;
    QrSNGCodCjt: TIntegerField;
    QrSNGCodGru: TIntegerField;
    QrSNGCodSgr: TIntegerField;
    QrSNGCODIGOS_TXT: TWideStringField;
    QrSNGOrdena: TWideStringField;
    QrSNGKGT: TLargeintField;
    frxDsSNG: TfrxDBDataset;
    QrSaldosNiv: TmySQLQuery;
    DsSaldosNiv: TDataSource;
    frxDsSaldosNiv: TfrxDBDataset;
    QrSdoPar: TmySQLQuery;
    QrSdoParSdoAnt: TFloatField;
    QrSdoParCredito: TFloatField;
    QrSdoParDebito: TFloatField;
    QrSdoParMovim: TFloatField;
    DsSdoPar: TDataSource;
    QrNiv1: TmySQLQuery;
    QrNiv1Genero: TIntegerField;
    QrNiv1Subgrupo: TIntegerField;
    QrNiv1Grupo: TIntegerField;
    QrNiv1Conjunto: TIntegerField;
    QrNiv1Plano: TIntegerField;
    QrNiv1Movim: TFloatField;
    QrNiv1NO_Cta: TWideStringField;
    QrNiv1OR_Cta: TIntegerField;
    QrNiv1NO_Sgr: TWideStringField;
    QrNiv1OR_SGr: TIntegerField;
    QrNiv1NO_Gru: TWideStringField;
    QrNiv1OR_Gru: TIntegerField;
    QrNiv1NO_Cjt: TWideStringField;
    QrNiv1OR_Cjt: TIntegerField;
    QrNiv1NO_Pla: TWideStringField;
    QrNiv1OR_Pla: TIntegerField;
    QrNiv2: TmySQLQuery;
    QrNiv2Genero: TLargeintField;
    QrNiv2Subgrupo: TIntegerField;
    QrNiv2Grupo: TIntegerField;
    QrNiv2Conjunto: TIntegerField;
    QrNiv2Plano: TIntegerField;
    QrNiv2Movim: TFloatField;
    QrNiv2NO_Cta: TWideStringField;
    QrNiv2OR_Cta: TLargeintField;
    QrNiv2NO_Sgr: TWideStringField;
    QrNiv2OR_SGr: TIntegerField;
    QrNiv2NO_Gru: TWideStringField;
    QrNiv2OR_Gru: TIntegerField;
    QrNiv2NO_Cjt: TWideStringField;
    QrNiv2OR_Cjt: TIntegerField;
    QrNiv2NO_Pla: TWideStringField;
    QrNiv2OR_Pla: TIntegerField;
    QrNiv3: TmySQLQuery;
    QrNiv3Genero: TLargeintField;
    QrNiv3Subgrupo: TLargeintField;
    QrNiv3Grupo: TIntegerField;
    QrNiv3Conjunto: TIntegerField;
    QrNiv3Plano: TIntegerField;
    QrNiv3Movim: TFloatField;
    QrNiv3NO_Cta: TWideStringField;
    QrNiv3OR_Cta: TLargeintField;
    QrNiv3NO_Sgr: TWideStringField;
    QrNiv3OR_SGr: TLargeintField;
    QrNiv3NO_Gru: TWideStringField;
    QrNiv3OR_Gru: TIntegerField;
    QrNiv3NO_Cjt: TWideStringField;
    QrNiv3OR_Cjt: TIntegerField;
    QrNiv3NO_Pla: TWideStringField;
    QrNiv3OR_Pla: TIntegerField;
    QrNiv4: TmySQLQuery;
    QrNiv4Genero: TLargeintField;
    QrNiv4Subgrupo: TLargeintField;
    QrNiv4Grupo: TLargeintField;
    QrNiv4Conjunto: TIntegerField;
    QrNiv4Plano: TIntegerField;
    QrNiv4Movim: TFloatField;
    QrNiv4NO_Cta: TWideStringField;
    QrNiv4OR_Cta: TLargeintField;
    QrNiv4NO_Sgr: TWideStringField;
    QrNiv4OR_SGr: TLargeintField;
    QrNiv4NO_Gru: TWideStringField;
    QrNiv4OR_Gru: TLargeintField;
    QrNiv4NO_Cjt: TWideStringField;
    QrNiv4OR_Cjt: TIntegerField;
    QrNiv4NO_Pla: TWideStringField;
    QrNiv4OR_Pla: TIntegerField;
    QrNiv5: TmySQLQuery;
    QrNiv5Genero: TLargeintField;
    QrNiv5Subgrupo: TLargeintField;
    QrNiv5Grupo: TLargeintField;
    QrNiv5Conjunto: TLargeintField;
    QrNiv5Plano: TIntegerField;
    QrNiv5Movim: TFloatField;
    QrNiv5NO_Cta: TWideStringField;
    QrNiv5OR_Cta: TLargeintField;
    QrNiv5NO_Sgr: TWideStringField;
    QrNiv5OR_SGr: TLargeintField;
    QrNiv5NO_Gru: TWideStringField;
    QrNiv5OR_Gru: TLargeintField;
    QrNiv5NO_Cjt: TWideStringField;
    QrNiv5OR_Cjt: TLargeintField;
    QrNiv5NO_Pla: TWideStringField;
    QrNiv5OR_Pla: TIntegerField;
    QrNivMov: TmySQLQuery;
    QrNivMovCredito: TFloatField;
    QrNivMovDebito: TFloatField;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    frxDsResumo: TfrxDBDataset;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    frxDsSaldoA: TfrxDBDataset;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    DsDebitos: TDataSource;
    frxDsDebitos: TfrxDBDataset;
    frxDsCreditos: TfrxDBDataset;
    DsCreditos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    QrRealizado: TmySQLQuery;
    QrRealizadoValor: TFloatField;
    QrCIni: TmySQLQuery;
    QrCIniCodigo: TIntegerField;
    QrCIniMinimo: TIntegerField;
    QrCIniMaximo: TIntegerField;
    QrCIniPendenMesSeg: TSmallintField;
    QrCIniCalculMesSeg: TSmallintField;
    QrCIniNome: TWideStringField;
    QrCIts1: TmySQLQuery;
    QrCIts1Codigo: TIntegerField;
    QrCIts1Nome: TWideStringField;
    QrCIts1Periodo: TIntegerField;
    QrCIts1Tipo: TIntegerField;
    QrCIts1Fator: TFloatField;
    QrCIts2: TmySQLQuery;
    QrCIts2Codigo: TIntegerField;
    QrCIts2Nome: TWideStringField;
    QrCIts2Periodo: TIntegerField;
    QrCIts2Tipo: TIntegerField;
    QrCIts2Fator: TFloatField;
    QrItsCtas: TmySQLQuery;
    QrItsCtasConta: TIntegerField;
    QrValFator: TmySQLQuery;
    QrValFatorValor: TFloatField;
    QrCtasResMes2: TmySQLQuery;
    QrCtasResMes2SeqImp: TIntegerField;
    QrCtasResMes2Conta: TIntegerField;
    QrCtasResMes2Nome: TWideStringField;
    QrCtasResMes2Periodo: TIntegerField;
    QrCtasResMes2Tipo: TIntegerField;
    QrCtasResMes2Fator: TFloatField;
    QrCtasResMes2ValFator: TFloatField;
    QrCtasResMes2Devido: TFloatField;
    QrCtasResMes2Pago: TFloatField;
    QrCtasResMes2Diferenca: TFloatField;
    QrCtasResMes2Acumulado: TFloatField;
    QrCtasAnt: TmySQLQuery;
    QrCtasAntConta: TIntegerField;
    QrCtasAntAcumulado: TFloatField;
    QrCtasAntNome: TWideStringField;
    QrPrevItO: TmySQLQuery;
    QrPrevItOCodigo: TIntegerField;
    QrPrevItOControle: TIntegerField;
    QrPrevItOConta: TIntegerField;
    QrPrevItOValor: TFloatField;
    QrPrevItOPrevBaI: TIntegerField;
    QrPrevItOTexto: TWideStringField;
    QrPrevItOLk: TIntegerField;
    QrPrevItODataCad: TDateField;
    QrPrevItODataAlt: TDateField;
    QrPrevItOUserCad: TIntegerField;
    QrPrevItOUserAlt: TIntegerField;
    QrPrevItOPrevBaC: TIntegerField;
    QrPrevItOEmitVal: TFloatField;
    QrPrevItOEmitSit: TIntegerField;
    QrPrevItOSubGrupo: TIntegerField;
    QrPrevItONOMECONTA: TWideStringField;
    QrPrevItONOMESUBGRUPO: TWideStringField;
    QrLocPerX: TmySQLQuery;
    QrLocPerXCodigo: TIntegerField;
    QrSuBloq: TmySQLQuery;
    QrSuBloqMultaVal: TFloatField;
    QrSuBloqMoraVal: TFloatField;
    QrSuBloqNOMECONTA: TWideStringField;
    QrSuBloqORIGINAL: TFloatField;
    QrSuBloqKGT: TLargeintField;
    QrSuBloqPAGO: TFloatField;
    QrPgBloq: TmySQLQuery;
    QrPgBloqCredito: TFloatField;
    QrPgBloqMultaVal: TFloatField;
    QrPgBloqMoraVal: TFloatField;
    QrPgBloqNOMEPROPRIET: TWideStringField;
    QrPgBloqUH: TWideStringField;
    QrPgBloqNOMECONTA: TWideStringField;
    QrPgBloqMez: TIntegerField;
    QrPgBloqVencimento: TDateField;
    QrPgBloqDocumento: TFloatField;
    QrPgBloqORIGINAL: TFloatField;
    QrPgBloqData: TDateField;
    QrPgBloqMES: TWideStringField;
    QrPgBloqDATA_TXT: TWideStringField;
    frxDsTotalSaldo: TfrxDBDataset;
    frxSaldos: TfrxReport;
    QrSaldos: TmySQLQuery;
    QrSaldosNome: TWideStringField;
    QrSaldosSaldo: TFloatField;
    QrSaldosTipo: TIntegerField;
    QrSaldosCodigo: TIntegerField;
    QrConsigna: TmySQLQuery;
    QrConsignaNome: TWideStringField;
    QrConsignaSaldo: TFloatField;
    QrTotalSaldo: TmySQLQuery;
    QrTotalSaldoNome: TWideStringField;
    QrTotalSaldoSaldo: TFloatField;
    QrTotalSaldoTipo: TIntegerField;
    QrTotalSaldoNOMETIPO: TWideStringField;
    QrDuplCH: TmySQLQuery;
    QrDuplCHData: TDateField;
    QrDuplCHControle: TIntegerField;
    QrDuplCHDescricao: TWideStringField;
    QrDuplCHCredito: TFloatField;
    QrDuplCHDebito: TFloatField;
    QrDuplCHNotaFiscal: TIntegerField;
    QrDuplCHCompensado: TDateField;
    QrDuplCHMez: TIntegerField;
    QrDuplCHFornecedor: TIntegerField;
    QrDuplCHCliente: TIntegerField;
    QrDuplCHNOMECART: TWideStringField;
    QrDuplCHNOMECLI: TWideStringField;
    QrDuplCHNOMEFNC: TWideStringField;
    QrDuplCHDocumento: TFloatField;
    QrDuplCHSerieCH: TWideStringField;
    QrDuplCHCarteira: TIntegerField;
    QrDuplCHTERCEIRO: TWideStringField;
    QrDuplCHMES: TWideStringField;
    QrDuplCHCHEQUE: TWideStringField;
    QrDuplCHCOMP_TXT: TWideStringField;
    DsDuplCH: TDataSource;
    QrDuplNF: TmySQLQuery;
    QrDuplNFData: TDateField;
    QrDuplNFControle: TIntegerField;
    QrDuplNFDescricao: TWideStringField;
    QrDuplNFCredito: TFloatField;
    QrDuplNFDebito: TFloatField;
    QrDuplNFNotaFiscal: TIntegerField;
    QrDuplNFCompensado: TDateField;
    QrDuplNFMez: TIntegerField;
    QrDuplNFFornecedor: TIntegerField;
    QrDuplNFCliente: TIntegerField;
    QrDuplNFNOMECART: TWideStringField;
    QrDuplNFNOMECLI: TWideStringField;
    QrDuplNFNOMEFNC: TWideStringField;
    QrDuplNFDocumento: TFloatField;
    QrDuplNFSerieCH: TWideStringField;
    QrDuplNFCarteira: TIntegerField;
    QrDuplNFTERCEIRO: TWideStringField;
    QrDuplNFMES: TWideStringField;
    QrDuplNFCHEQUE: TWideStringField;
    QrDuplNFCOMP_TXT: TWideStringField;
    DsDuplNF: TDataSource;
    QrDuplVal: TmySQLQuery;
    QrDuplValData: TDateField;
    QrDuplValControle: TIntegerField;
    QrDuplValDescricao: TWideStringField;
    QrDuplValCredito: TFloatField;
    QrDuplValDebito: TFloatField;
    QrDuplValNotaFiscal: TIntegerField;
    QrDuplValCompensado: TDateField;
    QrDuplValMez: TIntegerField;
    QrDuplValFornecedor: TIntegerField;
    QrDuplValCliente: TIntegerField;
    QrDuplValNOMECART: TWideStringField;
    QrDuplValNOMECLI: TWideStringField;
    QrDuplValNOMEFNC: TWideStringField;
    QrDuplValDocumento: TFloatField;
    QrDuplValSerieCH: TWideStringField;
    QrDuplValCarteira: TIntegerField;
    QrDuplValTERCEIRO: TWideStringField;
    QrDuplValMES: TWideStringField;
    QrDuplValCHEQUE: TWideStringField;
    QrDuplValCOMP_TXT: TWideStringField;
    DsDuplVal: TDataSource;
    QrAPLItens: TFloatField;
    QrSdoCtSdo: TmySQLQuery;
    QrSdoCtSdoSdoIni: TFloatField;
    QrSdoCarts: TmySQLQuery;
    QrSdoCartsInicial: TFloatField;
    QrSdo: TmySQLQuery;
    QrSdoCodigo: TIntegerField;
    QrSdoNOMECTA: TWideStringField;
    QrSdoSdoIni: TFloatField;
    QrCrt: TmySQLQuery;
    QrCrtCodigo: TIntegerField;
    QrCrtNome: TWideStringField;
    QrCrtInicial: TFloatField;
    QrCrtDIFERENCA: TFloatField;
    QrECI: TmySQLQuery;
    QrECITipoTabLct: TSmallintField;
    QrPendG: TmySQLQuery;
    QrPendGCliente: TIntegerField;
    QrPendGFornecedor: TIntegerField;
    QrPendGMez: TIntegerField;
    QrPendGCredito: TFloatField;
    QrPendGNOMEPROPRIET: TWideStringField;
    QrPendGUnidade: TWideStringField;
    QrPendGNOMEMEZ: TWideStringField;
    QrPendGVencimento: TDateField;
    QrPendGFatNum: TFloatField;
    QrRep: TmySQLQuery;
    QrRepCodigo: TIntegerField;
    QrReploginID: TWideStringField;
    QrRepautentica: TWideMemoField;
    QrRepCodCliEsp: TIntegerField;
    QrRepCodCliEnt: TIntegerField;
    QrRepCodigoEnt: TIntegerField;
    QrRepCodigoEsp: TIntegerField;
    QrRepVlrOrigi: TFloatField;
    QrRepVlrMulta: TFloatField;
    QrRepVlrJuros: TFloatField;
    QrRepVlrTotal: TFloatField;
    QrRepDataP: TDateTimeField;
    QrRepNovo: TSmallintField;
    QrRepTxaJur: TFloatField;
    QrRepTxaMul: TFloatField;
    QrRepLk: TIntegerField;
    QrRepDataCad: TDateField;
    QrRepDataAlt: TDateField;
    QrRepUserCad: TIntegerField;
    QrRepUserAlt: TIntegerField;
    QrRepAlterWeb: TSmallintField;
    QrRepAtivo: TSmallintField;
    QrRepUnidade: TWideStringField;
    QrRepNOMEPROP: TWideStringField;
    QrRepori: TmySQLQuery;
    QrReporiBloqOrigi: TIntegerField;
    QrReporiVlrOrigi: TFloatField;
    QrReporiVlrMulta: TFloatField;
    QrReporiVlrJuros: TFloatField;
    QrReporiVlrTotal: TFloatField;
    QrRepBPP: TmySQLQuery;
    QrRepBPPCodigo: TIntegerField;
    QrRepBPPControle: TAutoIncField;
    QrRepBPPParcela: TIntegerField;
    QrRepBPPVlrOrigi: TFloatField;
    QrRepBPPVlrMulta: TFloatField;
    QrRepBPPVlrJuros: TFloatField;
    QrRepBPPLk: TIntegerField;
    QrRepBPPDataCad: TDateField;
    QrRepBPPDataAlt: TDateField;
    QrRepBPPUserCad: TIntegerField;
    QrRepBPPUserAlt: TIntegerField;
    QrRepBPPAlterWeb: TSmallintField;
    QrRepBPPLancto: TIntegerField;
    QrRepBPPValBol: TFloatField;
    QrRepBPPVencimento: TDateField;
    QrRepBPPVlrTotal: TFloatField;
    QrRepBPPAtivo: TSmallintField;
    QrRepBPPFatNum: TFloatField;
    QrReplan: TmySQLQuery;
    QrReplanCredito: TFloatField;
    QrReplanVencimento: TDateField;
    QrReplanVLRORIG: TFloatField;
    QrReplanVLRMULTA: TFloatField;
    QrReplanVLRJUROS: TFloatField;
    QrReplanVLRTOTAL: TFloatField;
    QrReplanReparcel: TIntegerField;
    QrReplanFatNum: TFloatField;
    QrRepAbe: TmySQLQuery;
    QrRepAbeUnidade: TWideStringField;
    QrRepAbeData: TDateField;
    QrRepAbeVencimento: TDateField;
    QrRepAbeControle: TIntegerField;
    QrRepAbeDescricao: TWideStringField;
    QrRepAbeCredito: TFloatField;
    QrRepAbeCompensado: TDateField;
    QrRepAbeMoraDia: TFloatField;
    QrRepAbeMulta: TFloatField;
    QrRepAbeDepto: TIntegerField;
    QrRepAbeSit: TIntegerField;
    QrRepAbePago: TFloatField;
    QrRepAbePagMul: TFloatField;
    QrRepAbePagJur: TFloatField;
    QrRepAbeNOMEPROP: TWideStringField;
    QrRepAbeAtrelado: TIntegerField;
    QrRepAbeCompensado_TXT: TWideStringField;
    QrRepAbeFatNum: TFloatField;
    QrPendAll: TmySQLQuery;
    QrPendAllUnidade: TWideStringField;
    QrPendAllCredito: TFloatField;
    QrPendAllTIPO: TWideStringField;
    QrPendAllKGT: TLargeintField;
    QrPendAllBLOQUETOS: TLargeintField;
    QrPendSum: TmySQLQuery;
    QrPendSumCredito: TFloatField;
    frxDsPendAll: TfrxDBDataset;
    frxDsPendSum: TfrxDBDataset;
    QrLcts: TmySQLQuery;
    QrLctsData: TDateField;
    QrLctsTipo: TSmallintField;
    QrLctsCarteira: TIntegerField;
    QrLctsControle: TIntegerField;
    QrLctsSub: TSmallintField;
    QrDelLct: TmySQLQuery;
    QrTransfSub: TIntegerField;
    QrTrfCtasSub: TSmallintField;
    QrSaldosNivNivel: TIntegerField;
    QrSaldosNivCO_Cta: TIntegerField;
    QrSaldosNivCO_SGr: TIntegerField;
    QrSaldosNivCO_Gru: TIntegerField;
    QrSaldosNivCO_Cjt: TIntegerField;
    QrSaldosNivCO_Pla: TIntegerField;
    QrSaldosNivNO_Cta: TWideStringField;
    QrSaldosNivNO_SGr: TWideStringField;
    QrSaldosNivNO_Gru: TWideStringField;
    QrSaldosNivNO_Cjt: TWideStringField;
    QrSaldosNivNO_Pla: TWideStringField;
    QrSaldosNivOL_Cta: TIntegerField;
    QrSaldosNivOL_SGr: TIntegerField;
    QrSaldosNivOL_Gru: TIntegerField;
    QrSaldosNivOL_Cjt: TIntegerField;
    QrSaldosNivOL_Pla: TIntegerField;
    QrSaldosNivSdoAnt: TFloatField;
    QrSaldosNivCredito: TFloatField;
    QrSaldosNivDebito: TFloatField;
    QrSaldosNivMovim: TFloatField;
    QrSaldosNivEX_Cta: TIntegerField;
    QrSaldosNivEX_SGr: TIntegerField;
    QrSaldosNivEX_Gru: TIntegerField;
    QrSaldosNivEX_Cjt: TIntegerField;
    QrSaldosNivEX_Pla: TIntegerField;
    QrSaldosNivID_SEQ: TIntegerField;
    QrSaldosNivKGT: TLargeintField;
    QrSaldosNivAtivo: TSmallintField;
    Tb_Pla_Ctas: TmySQLTable;
    Tb_Pla_CtasNivel: TSmallintField;
    Tb_Pla_CtascN5: TIntegerField;
    Tb_Pla_CtasnN5: TWideStringField;
    Tb_Pla_CtascN4: TIntegerField;
    Tb_Pla_CtasnN4: TWideStringField;
    Tb_Pla_CtascN3: TIntegerField;
    Tb_Pla_CtasnN3: TWideStringField;
    Tb_Pla_CtascN2: TIntegerField;
    Tb_Pla_CtasnN2: TWideStringField;
    Tb_Pla_CtascN1: TIntegerField;
    Tb_Pla_CtasnN1: TWideStringField;
    Tb_Pla_CtasAtivo: TSmallintField;
    Ds_Pla_Ctas: TDataSource;
    QrFats3: TmySQLQuery;
    QrFats3Data: TDateField;
    QrFats3Tipo: TSmallintField;
    QrFats3Carteira: TIntegerField;
    QrFats3Controle: TIntegerField;
    QrFats3Sub: TSmallintField;
    QrFats2: TmySQLQuery;
    QrFats2Data: TDateField;
    QrFats2Tipo: TSmallintField;
    QrFats2Carteira: TIntegerField;
    QrFats2Controle: TIntegerField;
    QrFats2Sub: TSmallintField;
    QrFats1: TmySQLQuery;
    QrFats1Data: TDateField;
    QrFats1Tipo: TSmallintField;
    QrFats1Carteira: TIntegerField;
    QrFats1Controle: TIntegerField;
    QrFats1Sub: TSmallintField;
    QrContaFat: TmySQLQuery;
    QrContaFatPlaGen: TIntegerField;
    QrCtaSemFat: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrPesqAgrSit: TIntegerField;
    QrPesqResSit: TIntegerField;
    QrPesqSumSit: TIntegerField;
    QrSemConta: TmySQLQuery;
    DsSemConta: TDataSource;
    QrLancto1: TmySQLQuery;
    QrLancto1Sub: TSmallintField;
    QrLancto1Descricao: TWideStringField;
    QrLancto1Credito: TFloatField;
    QrLancto1Debito: TFloatField;
    QrLancto1NotaFiscal: TIntegerField;
    QrLancto1Documento: TFloatField;
    QrLancto1Vencimento: TDateField;
    QrLancto1Carteira: TIntegerField;
    QrLancto1Data: TDateField;
    QrLancto1NOMECARTEIRA: TWideStringField;
    QrLancto1TIPOCARTEIRA: TIntegerField;
    QrLancto1CARTEIRABANCO: TIntegerField;
    QrLancto1Cliente: TIntegerField;
    QrLancto1Fornecedor: TIntegerField;
    QrLancto1DataDoc: TDateField;
    QrLancto1Nivel: TIntegerField;
    QrLancto1Vendedor: TIntegerField;
    QrLancto1Account: TIntegerField;
    QrLancto1Controle: TIntegerField;
    QrLancto1CtrlIni: TIntegerField;
    QrLancto1Genero: TIntegerField;
    QrLancto1Mez: TIntegerField;
    QrLancto1Duplicata: TWideStringField;
    QrLancto1Doc2: TWideStringField;
    QrLancto1SerieCH: TWideStringField;
    QrLancto1MoraDia: TFloatField;
    QrLancto1Multa: TFloatField;
    QrLancto1ICMS_P: TFloatField;
    QrLancto1ICMS_V: TFloatField;
    QrLancto1CliInt: TIntegerField;
    QrLancto1Depto: TIntegerField;
    QrLancto1DescoPor: TIntegerField;
    QrLancto1ForneceI: TIntegerField;
    QrLancto1Unidade: TIntegerField;
    QrLancto1Qtde: TFloatField;
    QrLancto1Qtd2: TFloatField;
    QrLancto1FatID: TIntegerField;
    QrLancto1FatID_Sub: TIntegerField;
    QrLancto1DescoVal: TFloatField;
    QrLancto1NFVal: TFloatField;
    QrLancto1FatParcela: TIntegerField;
    QrLancto1FatNum: TFloatField;
    DsLancto1: TDataSource;
    DsLancto2: TDataSource;
    QrLancto2: TmySQLQuery;
    QrLancto2Tipo: TSmallintField;
    QrLancto2Credito: TFloatField;
    QrLancto2Debito: TFloatField;
    QrLancto2Sub: TSmallintField;
    QrLancto2Carteira: TIntegerField;
    QrLancto2Controle: TIntegerField;
    QrExtratosTEXTO_TXT: TWideStringField;
    QrPesqResNOMECONTA: TWideStringField;
    QrPsqLct1: TmySQLQuery;
    QrPsqLct1Pagamentos: TLargeintField;
    QrPsqCrt1: TmySQLQuery;
    QrPsqCrt1Codigo: TIntegerField;
    QrPsqCrt1Tipo: TIntegerField;
    QrUmLct: TmySQLQuery;
    QrECICodCliInt: TIntegerField;
    QrLE: TmySQLQuery;
    QrLEItens: TLargeintField;
    QrAPLDescoVal: TFloatField;
    QrLctsFatID: TIntegerField;
    QrPesqAgrUH: TWideStringField;
    QrPesqResUH: TWideStringField;
    QrCtaNSdo: TmySQLQuery;
    QrCtaNSdoCodigo: TIntegerField;
    QrTransfDuplicata: TWideStringField;
    QrLocLctoTab: TWideStringField;
    QrCarteira: TmySQLQuery;
    QrCarteiraTipo: TIntegerField;
    QrContasEnt: TmySQLQuery;
    QrContasEntCntrDebCta: TWideStringField;
    QrContasEntCodExporta: TWideStringField;
    QrDuplEntValData: TmySQLQuery;
    DsDuplEntValData: TDataSource;
    QrDuplEntValDataData: TDateField;
    QrDuplEntValDataControle: TIntegerField;
    QrDuplEntValDataDescricao: TWideStringField;
    QrDuplEntValDataCredito: TFloatField;
    QrDuplEntValDataDebito: TFloatField;
    QrDuplEntValDataNotaFiscal: TIntegerField;
    QrDuplEntValDataCompensado: TDateField;
    QrDuplEntValDataMez: TIntegerField;
    QrDuplEntValDataFornecedor: TIntegerField;
    QrDuplEntValDataCliente: TIntegerField;
    QrDuplEntValDataNOMECART: TWideStringField;
    QrDuplEntValDataNOMECLI: TWideStringField;
    QrDuplEntValDataNOMEFNC: TWideStringField;
    QrDuplEntValDataDocumento: TFloatField;
    QrDuplEntValDataSerieCH: TWideStringField;
    QrDuplEntValDataCarteira: TIntegerField;
    QrDuplEntValDataTERCEIRO: TWideStringField;
    QrDuplEntValDataMES: TWideStringField;
    QrDuplEntValDataCHEQUE: TWideStringField;
    QrDuplEntValDataCOMP_TXT: TWideStringField;
    QrFinOpcoes: TmySQLQuery;
    QrLancto1CentroCusto: TIntegerField;
    QrLancto1VctoOriginal: TDateField;
    QrLancto1ModeloNF: TWideStringField;
    QrLocFIts: TMySQLQuery;
    QrLocFItsPARCELA: TIntegerField;
    QrLocFItsCodigo: TIntegerField;
    QrLocFItsControle: TIntegerField;
    QrLocFItsConta: TIntegerField;
    QrLocFItsLancto: TLargeintField;
    QrLocFItsValor: TFloatField;
    QrLocFItsVencto: TDateField;
    procedure QrSdoExclCalcFields(DataSet: TDataSet);
    procedure QrSdoCtasCalcFields(DataSet: TDataSet);
    procedure QrSNGCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrResumoCalcFields(DataSet: TDataSet);
    procedure QrSaldoACalcFields(DataSet: TDataSet);
    procedure QrDebitosCalcFields(DataSet: TDataSet);
    procedure QrCreditosCalcFields(DataSet: TDataSet);
    procedure QrPgBloqCalcFields(DataSet: TDataSet);
    procedure QrTotalSaldoCalcFields(DataSet: TDataSet);
    procedure frxSaldosGetValue(const VarName: string; var Value: Variant);
    procedure QrDuplCHCalcFields(DataSet: TDataSet);
    procedure QrDuplNFCalcFields(DataSet: TDataSet);
    procedure QrDuplValCalcFields(DataSet: TDataSet);
    procedure QrPendGCalcFields(DataSet: TDataSet);
    procedure QrRepAfterScroll(DataSet: TDataSet);
    procedure QrRepAbeCalcFields(DataSet: TDataSet);
    procedure QrDuplEntValDataCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FSaldos, FExtratoCC, FSdoNiveis, FSaldosNiv: String;
  public
    { Public declarations }
    FCtasResMes: String;
    //
    procedure AtualizaContasHistSdo3(Entidade: Integer; LaAviso1, LaAviso2: TLabel;
              MezLastEncerr: Integer; TabLctA: String);
    procedure AtualizaContasMensais2(PeriodoIni, PeriodoFim, Genero: Integer;
              PB1, PB2, PB3: TProgressBar; ST1, ST2, ST3: TLabel;
              DtEncer, DtMorto: TDateTime; TabLctA, TabLctB, TabLctD: String);
    function  BloquetosDeCobrancaProcessados(Periodo, CliInt: Integer;
              TabLctA, TabLctB, TabLctD: String): Boolean;
    function  CarregaItensTipoDoc(Compo: TControl): Boolean;
    function  ConfereSalddosIniciais_Carteiras_x_Contas(CliInt: Integer): Boolean;
    function  ExisteLancamentoSemCliInt(TabLct: String): Boolean;
    function  GeraBalanceteExtrato(Periodo, Entidade: Integer; PB: TProgressBar;
              LaAviso1: TLabel; DtEncerr, DtMorto: TDateTime;
              TabLctA, TabLctB, TabLctD: String(*; CliInt_: Integer*)): Boolean;
    function  GeraBalanceteRecDebi(Periodo, CliInt: Integer; Acordos,
              Periodos, Textos: Boolean; DtEncer, DtMorto: TDateTime;
              TabLctA, TabLctB, TabLctD: String): Boolean;
    procedure ImprimeSaldos(CliInt: Integer);
    function  Reopen_APL_VLA(const ID_Pgto, SitAnt: Integer; var Data: String;
              var Pago, MoraVal, MultaVal, DescoVal: Double; var SitAtu: Integer;
              const DataCompQuit: String; const TbLctA: String): Boolean;
    function  ReopenCliInt(CliInt: Integer): Boolean;
    procedure ReopenCreditosEDebitos(CliInt: Integer; DataI, DataF: String;
              Acordos, Periodos, Textos, NaoAgruparNada: Boolean; TabLct: String);
    function  ReopenPendG(Entidade, CliInt: Integer; TabLctA: String): Boolean;
    function  ReopenPrevItO(CliInt, Periodo: Integer; TabPrvA,
              TabPriA: String): Boolean;
    function  ReopenReparcelAbe(CliEnti: Integer; TabLctA: String): Boolean;
    function  ReopenReparcelNew(CliEnti, Periodo, PBB: Integer): Boolean;
    procedure SaldosDeContasControladas3(CliInt, Mez: Integer;
              QrSaldosNiv: TmySQLQuery);
    function  SaldosNiveisPeriodo_2(Entidade, Periodo, Controla: Integer;
              TodosGeneros: Boolean; PB1, PB2, PB3, PB4, PB5: TProgressBar;
              QrToOpen: TmySQLQuery): Boolean;
    function  ReopenPendAll(Entidade, CliInt, Periodo: Integer; TabLctA:
              String): Boolean;
    procedure GeraDadosSaldos(Data: TDateTime; Exclusivo, NaoZerados:
             Boolean);
    // Financeiro novo
    function  EntidadeHabilitadadaParaFinanceiroNovo(Entidade: Integer;
              ShowMsgSdos, ShowMsgMigra: Boolean): TStatusFinNovo;
    function  HahContasSemFat(AvisaCanc: Boolean): Boolean;
    function  ObtemDescricaoDeFatID(FatID: Integer): String;
    function  ObtemGeneroDeFatID(const FatID: Integer; var Genero: Integer;
              tpPagto: TTipoPagto; AvisaErro: Boolean): Boolean;
    function  ObtemGenCtbDeFatID(const FatID: Integer; var Genero: Integer;
              tpPagto: TTipoPagto; AvisaErro: Boolean): Boolean;
    function  DefineValorSit(const TipoCart, Sit: Integer; const InfoCred,
              InfoDeb, InfoPago: Double; const PermitePrazo, NaoZera: Boolean;
              var ResCred, ResDeb, ResVal, SdoCr, SdoDB: Double): Boolean;
    function  VerificaSit(const Sit: Integer; const InfoCred, InfoDeb: Double;
              var RetCred, RetDeb: Double): Boolean;
    function  FaltaPlaGenFatID(LaAviso1, LaAviso2: TLabel): Integer;
    function  FaltaPlaGenTaxas(LaAviso1, LaAviso2: TLabel): Integer;
    function  FaltaPlaGenOcorP(LaAviso1, LaAviso2: TLabel): Integer;
    function  QuitaOuPgPartDeDocumento(Valor: Double; CtrlIni, CtrlOri: Integer;
              Documento: Double; Duplicata: String; DataQuit: TDateTime;
              Descricao: String; TaxasVal, MultaVal, MoraVal, DescoVal: Double;
              QrCrt, QrLct: TmySQLQuery; RecalcSdoCart: Boolean;
              TabLctA: String; UsaOutrosFatDfs: Boolean; FatID, FatID_Sub:
              Integer; FatNum: Double; FatParcela, FatParcRef, FatGrupo: Integer;
              ValPende: Double; OutraCartCompensa: Integer; Antigo: String = '';
              PermiteQuitarControleZero: Boolean = False): Boolean;
    function  PagamtParcialDeDocumento(Valor, Credito, Debito: Double; CtrlIni,
              CtrlOri: Integer; Documento: Double; SerieCH, Duplicata: String;
              DataPag, Vencimento, DataDoc, Compensado: TDateTime;
              Descricao: String; QrCrt,
              QrLct: TmySQLQuery; RecalcSdoCart: Boolean; TabLctA: String;
              UsaOutrosFatDfs: Boolean; FatID, FatID_Sub: Integer; FatNum: Double;
              FatParcela, FatParcRef, FatGrupo, Nivel, Carteira, Sit, Tipo, Genero,
              NotaFiscal, Cliente, Fornecedor, ID_Pgto, Vendedor, Account,
              DescoPor, CliInt, ForneceI, Depto, CentroCusto: Integer;
              Mez: String; MoraDia, Multa, DescoVal, TaxasVal, MultaVal, MoraVal: Double;
              OldCarteira: Integer; Antigo: String = '';
              PermitePagarControleZero: Boolean = False): Boolean;
    function  QuitacaoTotalDeDocumento(Valor: Double; CtrlIni, CtrlOri: Integer;
              Documento: Double; Duplicata: String; DataQuit: TDateTime;
              Descricao: String; TaxasVal, MultaVal, MoraVal, DescoVal: Double;
              QrCrt, QrLct: TmySQLQuery; RecalcSdoCart: Boolean;
              TabLctA: String; UsaOutrosFatDfs: Boolean; FatID, FatID_Sub:
              Integer; FatNum: Double; FatParcela, FatGrupo: Integer;
              Antigo: String = '';
              PermiteQuitarControleZero: Boolean = False;
              OutraCartCompensa: Integer = 0): Boolean;
    function  AbreLanctosAtrelados(Controle, Sub: Integer; TabLctA: String;
              PermiteQuitarControleZero: Boolean = False): Boolean;
    procedure FolowStepLct(Controle, RelaCtrl, Acao: Integer);
    function  CorrigeTabXxxY(const TbXxxY: String): String;
    function  AbreLanctoByCtrlSub(LctCtrl, LctSub: Integer; TabLct: String;
              PermiteZeroRegistros: Boolean = False): Boolean;
    function  FaturamentosQuitados(TabLctA: String;
              FatID, FatNum: Integer): Integer;
    function  DataUltimoLct(TabLctA: String): TDateTime;
    function  ContaDaTarifaBancariaCNAB(const CNAB_Cfg, Ocorrencia: Integer;
              var NomeOcorrencia: String; var Carteira: Integer): Integer;
    procedure InserirContasSdo(Codigo, Entidade: Integer);
    function  ObtemTipoDeCarteira(Carteira: Integer): Integer;
    procedure ReopenContasEnt(Conta, CodEnti: Integer);
    procedure ReopenNivelSel(QrNivelSel: TmySQLQuery; EdGenPlaCta: TdmkEditCB;
              CBGenPlaCta: TdmkDBLookupComboBox; RGNivPlaCta: TRadioGroup);
    procedure ReopenFinOpcoes();
    procedure GeraReciboImpCabLocacao(FatID, LocFCab_Controle, Emitente,
              Beneficiario: Integer; ValorPago: Double; TextoRecibo: String);
    // Fim financeiro novo
  end;

var
  DModFin: TDModFin;

implementation

uses UnInternalConsts, MyDBCheck, UnMyObjects, ModuleGeral, Module,
//Forms
FinOrfao1, ContasHistAtz,
//Fim Forms
UnFinanceiro, UCreate, UCreateFin, UMySQLModule, MyListas, ReciboImpCab;

{$R *.dfm}


{ TDModFin }

function TDModFin.AbreLanctoByCtrlSub(LctCtrl, LctSub: Integer; TabLct: String;
PermiteZeroRegistros: Boolean = False): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUmLct, Dmod.MyDB, [
  'SELECT * FROM ' + TabLct,
  'WHERE Controle<>0 ',
  'AND Controle=' + Geral.FF0(LctCtrl),
  'AND Sub=' + Geral.FF0(LctSub),
  '']);
  case QrUmLct.RecordCount of
    0: Result := PermiteZeroRegistros;
    1: Result := True;
    else Result := False;
  end;
  if not Result then
  begin
    Geral.MB_Aviso('Foram localizados ' + Geral.FF0(
    QrUmLct.RecordCount) + ' registros para o lan�amento: ' + sLineBreak +
    'Controle = ' + Geral.FF0(LctCtrl) + sLineBreak +
    'Sub = ' + Geral.FF0(LctSub));
  end;
end;

function TDModFin.AbreLanctosAtrelados(Controle, Sub: Integer; TabLctA: String;
PermiteQuitarControleZero: Boolean = False): Boolean;
begin
  QrLancto2.Close;
  QrLancto1.Close;
  //
(*
  QrLancto1.SQL.Clear;
  QrLancto1.SQL.Add('SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito,');
  QrLancto1.SQL.Add('la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data,');
  QrLancto1.SQL.Add('ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA ,');
  QrLancto1.SQL.Add('ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor,');
  QrLancto1.SQL.Add('la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account,');
  QrLancto1.SQL.Add('la.Genero, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, la.MoraDia,');
  QrLancto1.SQL.Add('la.Multa, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, la.DescoPor,');
  QrLancto1.SQL.Add('la.ForneceI, la.Unidade, la.Qtde, la.FatID, la.FatID_Sub,');
  QrLancto1.SQL.Add('la.FatNum, la.DescoVal, la.NFVal, la.FatParcela');
  {#### Colocar B e D?}
  QrLancto1.SQL.Add('FROM ' + TabLctA + ' la, Carteiras ca');
  QrLancto1.SQL.Add('WHERE la.Controle=:P0');
  QrLancto1.SQL.Add('AND la.Sub=:P1');
  QrLancto1.SQL.Add('AND ca.Codigo=la.Carteira');
  QrLancto1.Params[00].AsFloat := Controle;
  QrLancto1.Params[01].AsInteger := Sub;
  QrLancto1. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrLancto1, Dmod.MyDB, [
  'SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito, ',
  'la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data, ',
  'ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA , ',
  'ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor, ',
  'la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account, la.CentroCusto, ',
  'la.Genero, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, la.MoraDia, ',
  'la.Multa, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, la.DescoPor, ',
  'la.ForneceI, la.Unidade, la.Qtde, la.Qtd2, la.FatID, la.FatID_Sub, ',
  'la.FatNum, la.DescoVal, la.NFVal, la.FatParcela, ',
  'la.VctoOriginal, la.ModeloNF ',
  'FROM ' + TabLctA + ' la, Carteiras ca ',
  'WHERE la.Controle=' + Geral.FF0(Controle),
  'AND la.Sub=' + Geral.FF0(Sub),
  'AND ca.Codigo=la.Carteira ',
  '']);
  //
  if (QrLancto1.RecordCount = 0) and (PermiteQuitarControleZero = False)  then
  begin
    Result := False;
    //
    Geral.MB_Erro('Registro n�o encontrado para ser quitado! Controle: ' +
      Geral.FF0(Controle));
  end else
  begin
    if (QrLancto1.RecordCount > 0) or  (PermiteQuitarControleZero = False) then
    begin

  (*
      QrLancto2.Close;
      QrLancto2.SQL.Clear;
      QrLancto2.SQL.Add('SELECT Tipo, Credito, Debito, Controle, Sub, Carteira');
      {#### Colocar B e D?}
      QrLancto2.SQL.Add('FROM ' + TabLctA);
      QrLancto2.SQL.Add('WHERE ID_Pgto=:P0');
      QrLancto2.SQL.Add('AND ID_Sub=:P1');
      QrLancto2.Params[00].AsFloat := Controle;
      QrLancto2.Params[01].AsInteger := QrLancto1Sub.Value;
      QrLancto2. O p e n ;
  *)
      UnDmkDAC_PF.AbreMySQLQuery0(QrLancto2, Dmod.MyDB, [
      'SELECT Tipo, Credito, Debito, Controle, Sub, Carteira ',
      'FROM ' + TabLctA,
      'WHERE ID_Pgto=' + Geral.FF0(Controle),
      'AND ID_Sub=' + Geral.FF0(QrLancto1Sub.Value),
      '']);
    end;
    //
    Result := True;
  end;
end;

procedure TDModFin.AtualizaContasHistSdo3(Entidade: Integer;
LaAviso1, LaAviso2: TLabel; MezLastEncerr: Integer; TabLctA: String);
var
  Entidade_TXT: String;
begin
  Screen.Cursor := crHourGlass;
  //
  Entidade_TXT := FormatFloat('0', Entidade);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando itens orf�os');
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(*) Itens');
  Dmod.QrAux.SQL.Add('FROM ' + TabLctA + '');
  Dmod.QrAux.SQL.Add('WHERE Genero=-5');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.FieldByName('Itens').AsInteger > 0 then
  begin
    if DBCheck.CriaFm(TFmContasHistAtz, FmContasHistAtz, afmoLiberado) then
    begin
      with FmContasHistAtz do
      begin
        Show;
        QrOrfaos.Close;
        UnDmkDAC_PF.AbreQuery(QrOrfaos, Dmod.MyDB);
        //
        ConsertaLancamento();
        if QrOrfaos.RecordCount = 0 then
          Destroy
        else begin
          MyObjects.Informa2(LaAviso1, LaAviso2, False, 'ERRO... ' +
          FormatFloat('0', QrOrfaos.RecordCount) +
          ' registros n�o puderam ser corrigidos.');
        end;
      end;
    end;
  end;
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Atualizando tabela de saldos de contas');
  //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  //  MUDADO EM 2014-04-03 :: Marcelo Testar no SYNDIC !!!!!!
  //  MUDADO EM 2014-04-03 :: Marcelo Testar no SYNDIC !!!!!!
  //  MUDADO EM 2014-04-03 :: Marcelo Testar no SYNDIC !!!!!!
  //  MUDADO EM 2014-04-03 :: Marcelo Testar no SYNDIC !!!!!!
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO contassdo');
  Dmod.QrUpd.SQL.Add('SELECT Codigo, :P0 Entidade, 0 SdoIni,');
  Dmod.QrUpd.SQL.Add('0 SdoAtu, 0 SdoFut, 0 Lk, Date(SYSDATE()) DataCad,');
  Dmod.QrUpd.SQL.Add('"1899-12-30" DataAlt, :P1 UserCad, 0 UserAlt, 0 PerAnt, 0 PerAtu, ');
  Dmod.QrUpd.SQL.Add('"" Texto, 0 info, 1 AlterWeb, 1 Ativo, 0 ValMorto, 0 ValIniOld');
  Dmod.QrUpd.SQL.Add('FROM contas');
  Dmod.QrUpd.SQL.Add('WHERE Codigo>0');
  Dmod.QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE ');
  Dmod.QrUpd.SQL.Add('DataAlt=Date(SYSDATE()), UserAlt=:P2, AlterWeb=1');
  Dmod.QrUpd.Params[00].AsInteger := Entidade;
  Dmod.QrUpd.Params[01].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.ExecSQL;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtaNSdo, Dmod.MyDB, [
  'SELECT con.Codigo ',
  'FROM contas con ',
  'LEFT JOIN contassdo csd ON con.Codigo=csd.Codigo ',
  //'WHERE (con.Codigo IS NULL) OR (csd.Codigo IS NULL) ',
  'WHERE csd.Codigo IS NULL ',
  'AND con.Codigo > 0 ',
  '']);
  QrCtaNSdo.First;
  while not QrCtaNSdo.Eof do
  begin
    InserirContasSdo(QrCtaNSdoCodigo.Value, Entidade);
    //
    QrCtaNSdo.Next;
  end;
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Excluindo itens da tabela de saldos de n�veis');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM contasmov ');
  Dmod.QrUpd.SQL.Add('WHERE CliInt=' + Entidade_TXT);
  Dmod.QrUpd.SQL.Add('AND Mez > ' + FormatFloat('0', MezLastEncerr));
  //Dmod.QrUpd.Params[00].AsInteger := Entidade;
  //Dmod.QrUpd.Params[01].AsInteger := MezLastEncerr;
  Dmod.QrUpd.ExecSQL;
  //
  //Geral.MB_('Excluindo itens da tabela de saldos de n�veis:' + sLineBreak +
    //Dmod.QrUpd.SQL.Text, 'Mensagem', MB_OK+MB_ICONINFORMATION);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Inserindo itens na tabela de saldos de n�veis');
  //
  // CUIDADO Inclus�o segue ordem de campos na cria��o da tabela!!!!
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO contasmov');
  Dmod.QrUpd.SQL.Add('SELECT lct.CliInt, lct.Genero,');
  // 2011-02-13 ERRO no MySQL 5.1
  // N�o pode ter o nome de um campo da tabela
  //Dmod.QrUpd.SQL.Add('((YEAR(lct.Data)-2000) * 100)+MONTH(lct.Data) MEZ,');
  Dmod.QrUpd.SQL.Add('DATE_FORMAT(lct.Data, "%y%m") mes_, ');
  // FIM 2011-02-13
  Dmod.QrUpd.SQL.Add('cta.Subgrupo, sgr.Grupo, gru.Conjunto, cjt.Plano,');
  Dmod.QrUpd.SQL.Add('SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,');
  Dmod.QrUpd.SQL.Add('SUM(lct.Credito-lct.Debito) Movim,');
  Dmod.QrUpd.SQL.Add('0 Lk, Date(SYSDATE()) DataCad, 0 DataAlt,');
  Dmod.QrUpd.SQL.Add(FormatFloat('0', VAR_USUARIO) + ' UserCad, 0 UserAlt, ');
  // Ini 2020-01-24
  //Dmod.QrUpd.SQL.Add('1 Ativo, 1 AlterWeb');
  Dmod.QrUpd.SQL.Add('1 Ativo, 1 AlterWeb, 1 AWServerID, 1 AWStatSinc');
  // Fim 2020-01-24
  // Colocar aqui em sequencia de cria��o os campos novos!!!
  Dmod.QrUpd.SQL.Add('FROM ' + TabLctA + ' lct');
  Dmod.QrUpd.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lct.Genero');
  Dmod.QrUpd.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
  Dmod.QrUpd.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
  Dmod.QrUpd.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
  //Dmod.QrUpd.SQL.Add('WHERE lct.Genero > 0');

    //if Uppercase(Application.Title = 'SYNDIC') then
    //Dmod.QrUpd.SQL.Add('AND ((lct.Tipo<>2) OR (lct.Tipo=2 AND FatID=0))');
  //
  Dmod.QrUpd.SQL.Add('WHERE lct.CliInt=' + Entidade_TXT);

  //Erro nos saldos das contas, pois nos extratos tem a conta -5 => Outras carteiras proveniente do Tipo 2 (emiss�o)
  Dmod.QrUpd.SQL.Add('AND lct.Tipo <> 2');
  Dmod.QrUpd.SQL.Add('AND lct.Sit IN(' + CO_LIST_SITS_OKA + ')');
  Dmod.QrUpd.SQL.Add('AND DATE_FORMAT(lct.Data, "%y%m") > ' + FormatFloat('0', MezLastEncerr));

  Dmod.QrUpd.SQL.Add('GROUP BY lct.CliInt, lct.Genero, mes_');
  Dmod.QrUpd.SQL.Add('ORDER BY lct.CliInt, lct.Genero, mes_');
  //Dmod.QrUpd.Params[00].AsInteger := VAR_USUARIO;
  //Dmod.QrUpd.Params[01].AsInteger := Entidade;
  //Geral.MB_('Inserindo itens na tabela de saldos de n�veis:' + sLineBreak +
    //Dmod.QrUpd.SQL.Text, 'Mensagem', MB_OK+MB_ICONINFORMATION);
  //Geral.MB_Teste(Dmod.QrUpd.SQL.Text);
  try
    Dmod.QrUpd.ExecSQL;
  except
    on E: Exception do
    begin
(*
SELECT DISTINCT lct.Genero, cta.Codigo
FROM lct 0001a lct
LEFT JOIN contas cta ON cta.Codigo=lct.Genero
WHERE cta.Codigo IS NULL
*)
      Geral.MB_Erro(E.Message);
    end;
  end;



{
  //  2010-??-??
  // CUIDADO Inclus�o segue ordem de campos na cria��o da tabela!!!!
  //
  // 2011-02-13 - ERRO MySQL 5.1 (Duplica Indice no GROUP BY!
  // aqui fa�o GROUP BY 2 vezes para ver se funciona!!!!
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _contasmov_;');
  DModG.QrUpdPID1.SQL.Add('CREATE TABLE _contasmov_ IGNORE ');
  DModG.QrUpdPID1.SQL.Add('SELECT lct.CliInt, lct.Genero,');
  DModG.QrUpdPID1.SQL.Add('((YEAR(lct.Data)-2000) * 100)+MONTH(lct.Data) MEZ,');
  DModG.QrUpdPID1.SQL.Add('cta.Subgrupo, sgr.Grupo, gru.Conjunto, cjt.Plano,');
  DModG.QrUpdPID1.SQL.Add('SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,');
  DModG.QrUpdPID1.SQL.Add('SUM(lct.Credito-lct.Debito) Movim,');
  DModG.QrUpdPID1.SQL.Add('0 Lk, Date(SYSDATE()) DataCad, 0 DataAlt,');
  DModG.QrUpdPID1.SQL.Add(FormatFloat('0', VAR_USUARIO) + ' UserCad, 0 UserAlt,');
  DModG.QrUpdPID1.SQL.Add('1 Ativo, 1 AlterWeb');
  DModG.QrUpdPID1.SQL.Add('FROM ' + TabLctA + ' lct');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=lct.Genero');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.Grupo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
  DModG.QrUpdPID1.SQL.Add('WHERE lct.CliInt=' + Entidade_TXT);
  DModG.QrUpdPID1.SQL.Add('AND lct.Tipo <> 2');
  DModG.QrUpdPID1.SQL.Add('GROUP BY lct.CliInt, lct.Genero, MEZ');
  DModG.QrUpdPID1.SQL.Add('ORDER BY lct.CliInt, lct.Genero, MEZ');
  DModG.QrUpdPID1.SQL.Add(';');
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TMeuDB + '.contasmov');
  DModG.QrUpdPID1.SQL.Add('SELECT CliInt, Genero, MEZ,');
  DModG.QrUpdPID1.SQL.Add('Subgrupo, Grupo, Conjunto, Plano,');
  DModG.QrUpdPID1.SQL.Add('SUM(Credito) Credito, SUM(Debito) Debito,');
  DModG.QrUpdPID1.SQL.Add('SUM(Movim) Movim,');
  DModG.QrUpdPID1.SQL.Add('Lk, DataCad, DataAlt,');
  DModG.QrUpdPID1.SQL.Add('UserCad, UserAlt,');
  DModG.QrUpdPID1.SQL.Add('Ativo, AlterWeb');
  DModG.QrUpdPID1.SQL.Add('FROM _contasmov_');
  DModG.QrUpdPID1.SQL.Add('GROUP BY CliInt, Genero, MEZ');
  DModG.QrUpdPID1.SQL.Add('ORDER BY CliInt, Genero, MEZ;');
  DModG.QrUpdPID1.ExecSQL;
}  //
  // Fim 2011-02-13
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  //
  Screen.Cursor := crDefault;
end;

procedure TDModFin.AtualizaContasMensais2(PeriodoIni, PeriodoFim,
  Genero: Integer; PB1, PB2, PB3: TProgressBar; ST1, ST2, ST3: TLabel;
  DtEncer, DtMorto: TDateTime; TabLctA, TabLctB, TabLctD: String);
  //
  function DefineTabela(Periodo: Integer): String;
  var
    DtI: TDateTime;
  begin
    DtI := Geral.PeriodoToDate(Periodo, 1, False);
    if DtI > DtEncer then
    begin
    // TabLctA
      //TLW := tlwA;
      //FldIni := 'car.SdoFimB';
      Result := TabLctA;
    end else
    if DtI > DtMorto then
    begin
    // TabLctB
      //TLW := tlwB;
      //FldIni := '0.00';
      Result := TabLctB;
    end
    else begin
    // TabLctD
      //TLW := tlwD;
      //FldIni := '0.00';
      Result := TabLctD;
    end;
  end;
  //
  procedure InsereRegistro(Codigo, Periodo, Tipo: Integer;
    Fator, ValFator, Devido: Double; Nome, TabLct: String);
  var
    Pago: Double;
  begin
    QrRealizado.Close;
{
SELECT SUM(la.Credito-la.Debito) Valor
FROM lct la
WHERE la.Genero=:P0
AND la.Tipo <> 2
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=:P1
}
    QrRealizado.SQL.Clear;
    QrRealizado.SQL.Add('SELECT SUM(la.Credito-la.Debito) Valor');
    QrRealizado.SQL.Add('FROM ' + TabLct + ' la');
    QrRealizado.SQL.Add('WHERE la.Genero=' + FormatFloat('0', Codigo));
    QrRealizado.SQL.Add('AND la.Tipo <> 2');
    QrRealizado.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' +
      FormatFloat('0', Periodo));
    QrRealizado.SQL.Add('');
    {
    QrRealizado.Params[0].AsInteger := Codigo;
    QrRealizado.Params[1].AsInteger := Periodo;
    }
    UnDmkDAC_PF.AbreQuery(QrRealizado, Dmod.MyDB);
    Pago := QrRealizadoValor.Value;
    DModG.QrUpdPID1.Params[00].AsInteger := Codigo;
    DModG.QrUpdPID1.Params[01].AsString  := Nome;
    DModG.QrUpdPID1.Params[02].AsInteger := Periodo;
    DModG.QrUpdPID1.Params[03].AsInteger := Tipo;
    DModG.QrUpdPID1.Params[04].AsFloat   := Fator;
    DModG.QrUpdPID1.Params[05].AsFloat   := ValFator;
    DModG.QrUpdPID1.Params[06].AsFloat   := -Devido;
    DModG.QrUpdPID1.Params[07].AsFloat   := -Pago;
    DModG.QrUpdPID1.ExecSQL;
  end;
const
  Txt = 'WHERE la.Genero in (%s)';
var
  ValFator, Devido, Acuml, Difer, Fator: Double;
  i, n, p, PeriodoHoje, Conta: Integer;
  TabLctX, ContasSelc: String;
  DtI: TDateTime;
begin
  //////////////////////////////////////////////////////////////////////////////
  //  Contas Mensais
  //////////////////////////////////////////////////////////////////////////////
  PeriodoHoje := Geral.Periodo2000(date);
  Conta       := 0;
  Acuml       := 0;
  //
  //FCtasResMes := UCriar.RecriaTempTable('CtasResMes', DModG.QrUpdPID1, False);
  FCtasResMes :=
    UCriarFin.RecriaTempTableNovo(ntrtt_CtasResMes, DMOdG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCtasResMes + ' SET ');
  DModG.QrUpdPID1.SQL.Add('Conta     =:P00,  ');
  DModG.QrUpdPID1.SQL.Add('Nome      =:P01, ');
  DModG.QrUpdPID1.SQL.Add('Periodo   =:P02, ');
  DModG.QrUpdPID1.SQL.Add('Tipo      =:P03, ');
  DModG.QrUpdPID1.SQL.Add('Fator     =:P04, ');
  DModG.QrUpdPID1.SQL.Add('ValFator  =:P05, ');
  DModG.QrUpdPID1.SQL.Add('Devido    =:P06, ');
  DModG.QrUpdPID1.SQL.Add('Pago      =:P07');
  //
  QrCIni.Close;
  QrCIni.SQL.Clear;
  QrCIni.SQL.Add('SELECT co.Codigo, MIN(ci.Periodo) Minimo, co.Nome, ');
  QrCIni.SQL.Add('MAX(ci.Periodo) Maximo, co.PendenMesSeg, co.CalculMesSeg');
  QrCIni.SQL.Add('FROM contas co');
  QrCIni.SQL.Add('LEFT JOIN contasits ci ON ci.Codigo=co.Codigo');
  QrCIni.SQL.Add('WHERE ci.Codigo IS NOT NULL');
  QrCIni.SQL.Add('AND co.Ativo=1');
  if Genero <> 0 then
    QrCIni.SQL.Add('AND co.Codigo="' + IntToStr(Genero)+'"');
  if PeriodoFim > PeriodoIni then
    QrCIni.SQL.Add('AND ci.Periodo<="' + IntToStr(PeriodoFim)+'"');
  QrCIni.SQL.Add('GROUP BY co.Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrCIni, Dmod.MyDB);
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max := QrCIni.RecordCount;
  end;
  if ST1 <> nil then
  begin
    ST1.Caption := 'Pesquisando valores de pend�ncias por conta mensal...';
    ST1.Update;
  end;
  Application.ProcessMessages;
  while not QrCIni.Eof do
  begin
    if PB1 <> nil then PB1.Position := PB1.Position + 1;
    if PeriodoFim < QrCIniMaximo.Value then
      n := PeriodoFim else n := QrCIniMaximo.Value;
    if PB2 <> nil then
    begin
      PB2.Position := 0;
      PB2.Max := n - QrCIniMinimo.Value + 1;
    end;
    if ST2 <> nil then
    begin
      ST2.Caption := 'Informando pend�ncias de "' + QrCIniNome.Value + '"...';
      ST2.Update;
    end;
    Application.ProcessMessages;
    for i := QrCIniMinimo.Value to n do
    begin
      if PB2 <> nil then PB2.Position := PB2.Position + 1;
      QrCIts1.Close;
      QrCIts1.Params[00].AsInteger := QrCIniCodigo.Value;
      QrCIts1.Params[01].AsInteger := i;
      UnDmkDAC_PF.AbreQuery(QrCIts1, Dmod.MyDB);
      if QrCIniCalculMesSeg.Value = 1 then
      begin
        QrCIts2.Close;
        QrCIts2.Params[00].AsInteger := QrCIniCodigo.Value;
        QrCIts2.Params[01].AsInteger := i-1;
        UnDmkDAC_PF.AbreQuery(QrCIts2, Dmod.MyDB);
      end;
      //
      if QrCIniCalculMesSeg.Value = 1 then
        Devido := QrCIts2Fator.Value
      else
        Devido := QrCIts1Fator.Value;
      if QrCIts1Tipo.Value = 0 then
      begin
        ValFator := QrCIts1Fator.Value;
      end else begin
        p := i;
        if QrCIniCalculMesSeg.Value = 1 then p := p - 1;
        QrItsCtas.Close;
        QrItsCtas.Params[0].AsInteger := QrCIniCodigo.Value;
        QrItsCtas.Params[1].AsInteger := p;
        UnDmkDAC_PF.AbreQuery(QrItsCtas, Dmod.MyDB);
        ContasSelc := '';
        if PB3 <> nil then
        begin
          PB3.Position := 0;
          PB3.Max      := QrItsCtas.RecordCount;
        end;
        if ST3 <> nil then
        begin
          ST3.Caption := 'Pesquisando contas fonte de "' + QrCIniNome.Value + '"...';
          ST3.Update;
        end;
        Application.ProcessMessages;
        while not QrItsCtas.Eof do
        begin
          if PB3 <> nil then PB3.Position := PB3.Position + 1;
          ContasSelc := ContasSelc + IntToStr(QrItsCtasConta.Value) +',';
          QrItsCtas.Next;
        end;
        ContasSelc := Copy(ContasSelc, 1, Length(ContasSelc)-1);
        //
        if Trim(ContasSelc) = '' then
        begin
          ValFator := 0;
          Devido   := 0;
        end else begin
          TabLctX := DefineTabela(p);

          QrValFator.Close;
          QrValFator.SQL.Clear;
          QrValFator.SQL.Add('SELECT SUM(la.Credito-la.Debito) Valor');
          QrValFator.SQL.Add('FROM ' + TabLctX + ' la');
          QrValFator.SQL.Add(Format(Txt, [ContasSelc]));
          QrValFator.SQL.Add('AND la.Tipo <> 2');
          QrValFator.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))='
            + FormatFloat('0', p));
          {
          QrValFator.Params[0].AsInteger := p;
          }
          //
          UnDmkDAC_PF.AbreQuery(QrValFator, Dmod.MyDB);
          ValFator := QrValFatorValor.Value;
          Devido := QrValFatorValor.Value * Devido / 100;
        end;
      end;
      case QrCIniPendenMesSeg.Value of
        0: if PeriodoHoje < i then Fator := 0 else Fator := 1;
        1: Fator := 1;
        2:
        begin
         if PeriodoHoje = i then Fator := dmkPF.PorcentagemMes
         else if PeriodoHoje < i then Fator := 0 else Fator := 1;
        end;
        else Fator := 1;
      end;
      //ver positivos ou s� negativos!
      if Devido > 0 then
        Devido := Devido * Fator
      else
        if Devido < 0 then
          ShowMessage(Geral.FFT(Devido, 2, siNegativo));
      TabLctX := DefineTabela(i);
      InsereRegistro(QrCIniCodigo.Value, i, QrCIts1Tipo.Value,
        QrCIts1Fator.Value, ValFator, Devido, QrCIniNome.Value, TabLctX);
    end;
    if ST2 <> nil then
    begin
      ST2.Caption := '...';
      ST2.Update;
    end;
    if ST3 <> nil then
    begin
      ST3.Caption := '...';
      ST3.Update;
    end;
    Application.ProcessMessages;
    QrCIni.Next;
  end;
  //
(*
  QrCtasResMes2.Close;
  QrCtasResMes2.Database := DModG.MyPID_DB;
  QrCtasResMes2.SQL.Clear;
  QrCtasResMes2.SQL.Add('SELECT * FROM ' + FCtasResMes);
  QrCtasResMes2. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtasResMes2, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FCtasResMes,
  '']);
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max      := QrCtasResMes2.RecordCount;
  end;
  if ST1 <> nil then
  begin
    ST1.Caption := 'Atualizando valores acumulados das pend�ncias ...';
    ST1.Update;
  end;
  Application.ProcessMessages;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FCtasResMes + ' SET ');
  DModG.QrUpdPID1.SQL.Add('Diferenca =:P00,');
  DModG.QrUpdPID1.SQL.Add('Acumulado =:P01 WHERE ');
  DModG.QrUpdPID1.SQL.Add('Conta     =:Pa AND ');
  DModG.QrUpdPID1.SQL.Add('Periodo   =:Pb');
  while not QrCtasResMes2.Eof do
  begin
    if PB1 <> nil then PB1.Position := PB1.Position + 1;
    if Conta <> QrCtasResMes2Conta.Value then
    begin
      Conta := QrCtasResMes2Conta.Value;
      Acuml := 0;
    end;
    Difer := QrCtasResMes2Devido.Value + QrCtasResMes2Pago.Value;
    Acuml := Acuml + Difer;
    DModG.QrUpdPID1.Params[00].AsFloat   := Difer;
    DModG.QrUpdPID1.Params[01].AsFloat   := Acuml;
    DModG.QrUpdPID1.Params[02].AsFloat   := QrCtasResMes2Conta.Value;
    DModG.QrUpdPID1.Params[03].AsFloat   := QrCtasResMes2Periodo.Value;
    DModG.QrUpdPID1.ExecSQL;
    QrCtasResMes2.Next;
  end;
(*
  QrCtasAnt.Close;
  QrCtasAnt.Database := DModG.MyPID_DB;
  QrCtasAnt.Params[0].AsInteger := PeriodoIni;
  QrCtasAnt. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtasAnt, DModG.MyPID_DB, [
  'SELECT Conta, Nome, SUM(Devido+Pago) Acumulado ',
  'FROM ' + FCtasResMes,
  'WHERE Periodo<' + Geral.FF0(PeriodoIni),
  'GROUP BY Conta ',
  '']);
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max      := QrCtasAnt.RecordCount;
  end;
  if ST1 <> nil then
  begin
    ST1.Caption := 'Pesquisando pend�ncias anteriores das contas mensais...';
    ST1.Update;
  end;
  Application.ProcessMessages;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+FCtasResMes+' SET ');
  DModG.QrUpdPID1.SQL.Add('Conta     =:P00, ');
  DModG.QrUpdPID1.SQL.Add('Nome      =:P01, ');
  DModG.QrUpdPID1.SQL.Add('Periodo   =:P02, ');
  DModG.QrUpdPID1.SQL.Add('Tipo      =:P03, ');
  DModG.QrUpdPID1.SQL.Add('Acumulado =:P04, ');
  DModG.QrUpdPID1.SQL.Add('SeqImp    =-1');
  while not QrCtasAnt.Eof do
  begin
    if PB1 <> nil then PB1.Position := PB1.Position + 1;
    DModG.QrUpdPID1.Params[00].AsInteger := QrCtasAntConta.Value;
    DModG.QrUpdPID1.Params[01].AsString  := QrCtasAntNome.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := -1;
    DModG.QrUpdPID1.Params[03].AsInteger := -1;
    DModG.QrUpdPID1.Params[04].AsFloat   := QrCtasAntAcumulado.Value;
    DModG.QrUpdPID1.ExecSQL;
    QrCtasAnt.Next;
  end;
  if ST1 <> nil then ST1.Caption := '...';
  if ST2 <> nil then ST2.Caption := '...';
  if ST3 <> nil then ST3.Caption := '...';
  Application.ProcessMessages;
end;

function TDModFin.BloquetosDeCobrancaProcessados(Periodo, CliInt: Integer;
  TabLctA, TabLctB, TabLctD: String): Boolean;

  procedure GeraParteSQL_PgBloq(TabLct, CliInt, Mez: String);
  begin
    QrPgBloq.SQL.Add('SELECT IF(lan.Compensado<2, 0, IF(car.Tipo=2,');
    QrPgBloq.SQL.Add('lan.Credito + lan.PagMul + lan.PagJur, lan.Credito)) Credito,');
    QrPgBloq.SQL.Add('IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal)) MultaVal,');
    QrPgBloq.SQL.Add('IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal)) MoraVal,');
    QrPgBloq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
    QrPgBloq.SQL.Add('ent.Nome END NOMEPROPRIET, ');
    //
    if VAR_KIND_DEPTO = kdUH then
      QrPgBloq.SQL.Add('imv.Unidade UH, ')
    else
      QrPgBloq.SQL.Add('"" UH, ');
    //
    QrPgBloq.SQL.Add('con.Nome NOMECONTA, lan.Mez, lan.Vencimento,');
    QrPgBloq.SQL.Add('lan.Documento, lan.Credito ORIGINAL, lan.Compensado Data,');
    QrPgBloq.SQL.Add('IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, "%d/%m/%Y")) DATA_TXT,');
    QrPgBloq.SQL.Add('con.OrdemLista');
    QrPgBloq.SQL.Add('FROM ' + TabLct + ' lan');
    QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov imv ON imv.Conta=lan.Depto');
      QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lan.ForneceI');
    end else
      QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lan.Cliente');
    //
    QrPgBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas con ON con.Codigo=lan.Genero');
    QrPgBloq.SQL.Add('WHERE car.Tipo in (0,2)');
    QrPgBloq.SQL.Add('AND lan.FatID in (600,601)');
    QrPgBloq.SQL.Add('AND car.ForneceI=' + CliInt);
    QrPgBloq.SQL.Add('AND lan.Mez=' + Mez);
  end;

  procedure GeraParteSQL_SuBloq(TabLct, CliInt, Mez: String);
  begin
    QrSuBloq.SQL.Add('SELECT SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2,');
    QrSuBloq.SQL.Add('lan.Credito + lan.PagMul + lan.PagJur, lan.Credito))) PAGO,');
    QrSuBloq.SQL.Add('SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal))) MultaVal,');
    QrSuBloq.SQL.Add('SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal))) MoraVal,');
    QrSuBloq.SQL.Add('con.Nome NOMECONTA, SUM(lan.Credito) ORIGINAL, 0 KGT');
    QrSuBloq.SQL.Add('FROM ' + TabLct + ' lan');
    QrSuBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    //
    if VAR_KIND_DEPTO = kdUH then
      QrSuBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov imv ON imv.Conta=lan.Depto');
    //
    QrSuBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lan.ForneceI');
    QrSuBloq.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas con ON con.Codigo=lan.Genero');
    QrSuBloq.SQL.Add('WHERE car.Tipo in (0,2)');
    QrSuBloq.SQL.Add('AND lan.FatID in (600,601)');
    QrSuBloq.SQL.Add('AND car.ForneceI=' + CliInt);
    QrSuBloq.SQL.Add('AND lan.Mez=' + Mez);
    QrSuBloq.SQL.Add('GROUP BY NOMECONTA');
  end;

var
  Mez_TXT, CliInt_TXT: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Mez_TXT    := Geral.FF0(dmkPF.PeriodoToAnoMes(Periodo-1));
    CliInt_TXT := Geral.FF0(CliInt);
    //
    // Verificar erro de soma de pagamento
    // por enquanto estou usando "credito" + "PagJur" + "PagMul" o certo � "Pago"
    QrPgBloq.Close;
    QrPgBloq.SQL.Clear;
    //
    QrPgBloq.Close;
    QrPgBloq.SQL.Clear;
    QrPgBloq.SQL.Add('DROP TABLE IF EXISTS _BloqCobrProc_Pg_;');
    QrPgBloq.SQL.Add('CREATE TABLE _BloqCobrProc_Pg_ IGNORE ');
    QrPgBloq.SQL.Add('');
    GeraParteSQL_PgBloq(TabLctA, CliInt_TXT, Mez_TXT);
    QrPgBloq.SQL.Add('UNION');
    GeraParteSQL_PgBloq(TabLctB, CliInt_TXT, Mez_TXT);
    QrPgBloq.SQL.Add('UNION');
    GeraParteSQL_PgBloq(TabLctD, CliInt_TXT, Mez_TXT);
    QrPgBloq.SQL.Add(';');
    QrPgBloq.SQL.Add('');
    QrPgBloq.SQL.Add('SELECT * FROM _BloqCobrProc_Pg_');
    QrPgBloq.SQL.Add('ORDER BY UH, Documento, OrdemLista, NOMECONTA;');
    QrPgBloq.SQL.Add('');
    QrPgBloq.SQL.Add('DROP TABLE IF EXISTS _BloqCobrProc_Pg_;');
    QrPgBloq.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrPgBloq, DModG.MyPID_DB);

    //ShowMessage(IntToStr(QrPgBloq.RecordCount));
    //   DEMORADO
    // Mesmo erro acima colocado linha "AND lan.Compensado>0" al�m de
    //  "credito" + "PagJur" + "PagMul" o certo � "Pago"
    // QrSuBloqKGT.Value > KeepGroupTogether
    QrSuBloq.Close;
    QrSuBloq.SQL.Clear;

    QrSuBloq.Close;
    QrSuBloq.SQL.Clear;
    QrSuBloq.SQL.Add('DROP TABLE IF EXISTS _BloqCobrProc_Su_;');
    QrSuBloq.SQL.Add('CREATE TABLE _BloqCobrProc_Su_ IGNORE ');
    QrSuBloq.SQL.Add('');
    GeraParteSQL_SuBloq(TabLctA, CliInt_TXT, Mez_TXT);
    QrSuBloq.SQL.Add('UNION');
    GeraParteSQL_SuBloq(TabLctB, CliInt_TXT, Mez_TXT);
    QrSuBloq.SQL.Add('UNION');
    GeraParteSQL_SuBloq(TabLctD, CliInt_TXT, Mez_TXT);
    QrSuBloq.SQL.Add(';');
    QrSuBloq.SQL.Add('');
    QrSuBloq.SQL.Add('SELECT * FROM _BloqCobrProc_Su_');
    QrSuBloq.SQL.Add('GROUP BY NOMECONTA;');
    QrSuBloq.SQL.Add('');
    QrSuBloq.SQL.Add('DROP TABLE IF EXISTS _BloqCobrProc_Su_;');
    QrSuBloq.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrSuBloq, DModG.MyPID_DB);
    Result := True;
    //
    if Result = False then
      Geral.MB_Aviso('N�o foi poss�vel processar os bloquetos de cobran�a!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDModFin.CarregaItensTipoDoc(Compo: TControl): Boolean;
var
  PropInfo: PPropInfo;
begin
  Result := False;
  if Compo <> nil then
  begin
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Items');
    if PropInfo <> nil then
    begin
      TRadioGroup(Compo).Items.Clear;
      TRadioGroup(Compo).Items.Add('Outros');
      TRadioGroup(Compo).Items.Add('Cheque');
      TRadioGroup(Compo).Items.Add('DOC');
      TRadioGroup(Compo).Items.Add('TED');
      TRadioGroup(Compo).Items.Add('Esp�cie');
      TRadioGroup(Compo).Items.Add('Bloqueto');
      TRadioGroup(Compo).Items.Add('Duplicata');
      TRadioGroup(Compo).Items.Add('TEF');
      TRadioGroup(Compo).Items.Add('D�bito c/c');
    end;
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Values');
    if PropInfo <> nil then
    begin
      TDBRadioGroup(Compo).Values.Clear;
      TDBRadioGroup(Compo).Values.Add('0');
      TDBRadioGroup(Compo).Values.Add('1');
      TDBRadioGroup(Compo).Values.Add('2');
      TDBRadioGroup(Compo).Values.Add('3');
      TDBRadioGroup(Compo).Values.Add('4');
      TDBRadioGroup(Compo).Values.Add('5');
      TDBRadioGroup(Compo).Values.Add('6');
      TDBRadioGroup(Compo).Values.Add('7');
      TDBRadioGroup(Compo).Values.Add('8');
    end;
  end;
end;

function TDModFin.ConfereSalddosIniciais_Carteiras_x_Contas(
  CliInt: Integer): Boolean;
begin

  QrSdoCarts.Close;
  QrSdoCarts.Params[0].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(QrSdoCarts, Dmod.MyDB);
  //
  QrSdoCtSdo.Close;
  QrSdoCtSdo.Params[0].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(QrSdoCtSdo, Dmod.MyDB);
  //
  if (QrSdoCartsInicial.Value <> 0) or (QrSdoCtSdoSdoIni.Value <> 0) then
  begin
    Result := False;
    Geral.MB_Aviso('Saldos iniciais das carteiras e saldos ' +
    'iniciais das contas devem estar zeradas para o cliente interno ' +
    IntToStr(CliInt) + ':' + sLineBreak + 'Saldo das carteiras: ' +
    Geral.FFT(QrSdoCartsInicial.Value, 2, siNegativo) + sLineBreak +
    'Saldo das contas: ' + Geral.FFT(QrSdoCtSdoSdoIni.Value, 2, siNegativo));
  end else Result := True;
end;

function TDModFin.ContaDaTarifaBancariaCNAB(const CNAB_Cfg, Ocorrencia: Integer;
var NomeOcorrencia: String; var Carteira: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cfg.CtaRetTar, cfr.Genero, cfr.Nome, cfg.CartRetorno ',
    'FROM cnab_cfg cfg ',
    'LEFT JOIN cnab_cfor cfr ON cfr.Codigo=cfg.Codigo ',
    'WHERE cfg.Codigo=' + Geral.FF0(CNAB_Cfg),
    'AND (cfr.Ocorrencia=' + Geral.FF0(Ocorrencia),
    '     OR ',
    '     cfr.Ocorrencia IS NULL) ',
    '']);
    //
    if Qry.FieldByName('Genero').AsInteger > 0 then
    begin
      Result := Qry.FieldByName('Genero').AsInteger;
      NomeOcorrencia := Qry.FieldByName('Nome').AsString;
    end else
    begin
      Result := Qry.FieldByName('CtaRetTar').AsInteger;
      NomeOcorrencia := 'Tarifa CNAB';
    end;
    Carteira := Qry.FieldByName('CartRetorno').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TDModFin.CorrigeTabXxxY(const TbXxxY: String): String;
begin
  if pos('.', TbXxxY) > 0 then
    Result := TbXxxY
  else
    Result := TMeuDB + '.' + TbXxxY;
end;

procedure TDModFin.DataModuleCreate(Sender: TObject);
var
  i: Integer;
begin

  if Dmod = nil then
  begin
    Geral.MB_Erro('� necess�rio criar o "DMod" antes do "ModuleFin"');
    Halt(0);
  end;
  for i := 0 to ComponentCount - 1 do
  begin
    if (Components[i] is TmySQLQuery) then
    begin
      TmySQLQuery(Components[i]).Close;
      //N�o funciona se Dmod ainda n�o tiver sido criado!
      TmySQLQuery(Components[i]).Database := Dmod.MyDB;
    end else
    if (Components[i] is TfrxDBDataSet) then
    begin
      TfrxDBDataSet(Components[i]).Enabled := True;
      TfrxDBDataSet(Components[i]).OpenDataSource := True;
    end;
  end;
  Tb_Pla_Ctas.TableName := '_pla_ctas_';
  QrSNG.Database    := DModG.MyPID_DB;
  QrAPL.Database    := DModG.MyPID_DB;
  QrPgBloq.Database := DModG.MyPID_DB;
  QrSuBloq.Database := DModG.MyPID_DB;
  QrRepLan.Database := DModG.MyPID_DB;
  //
  ReopenFinOpcoes();
end;

procedure TDModFin.ReopenFinOpcoes();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFinOpcoes, Dmod.MyDB, [
    'SELECT PlanoPadrao ',
    'FROM paramsemp ',
    'WHERE Codigo=' + Geral.FF0(DmodG.QrFiliLogCodigo.Value),
    '']);
end;

function TDModFin.DataUltimoLct(TabLctA: String): TDateTime;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MAX(Data) Data FROM ' + TabLctA);
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  //
  Result := Dmod.QrAux.FieldByName('Data').AsDateTime;
  if Result < 2 then
    Result := Int(Date) + 90;  // s� por garantia?!
end;

function TDModFin.DefineValorSit(const TipoCart, Sit: Integer; const InfoCred,
  InfoDeb, InfoPago: Double; const PermitePrazo, NaoZera: Boolean;
  var ResCred, ResDeb, ResVal, SdoCr, SdoDB: Double): Boolean;
var
  Val: Double;
begin
  ResVal := 0;
  ResCred := 0;
  ResDeb := 0;
  Result := VerificaSit(Sit, Val, Val, Val, Val) ;
  if not Result then
    Exit;
  if NaoZera then
  begin
    if (TipoCart = 2) and (Sit=1) then
    begin
      ResVal := (InfoCred - InfoDeb) - InfoPago;
      ResCred := InfoCred;
      ResDeb := InfoDeb;
      if ResCred > 0 then
      begin
        SdoCr := ResVal;
        SdoDB := 0;
      end else begin
        SdoCr := 0;
        SdoDB := -ResVal;
      end;
    end else
    begin
      ResVal := InfoCred - InfoDeb;
      ResCred := InfoCred;
      ResDeb := InfoDeb;
      //
      SdoCr := ResCred;
      SdoDb := ResDeb;
    end;
    //
    Exit;
  end;
  //
  case TipoCart of
    0, 1:
    begin
      case Sit of
        0..3:
        begin
          ResVal := InfoCred - InfoDeb;
          ResCred := InfoCred;
          ResDeb := InfoDeb;
          //
          SdoCr := ResCred;
          SdoDb := ResDeb;
        end;
      end;
    end;
    2:
    begin
      case Sit of
        0:
        begin
          if PermitePrazo then
          begin
            ResVal := InfoCred - InfoDeb;
            ResCred := InfoCred;
            ResDeb := InfoDeb;
            //
            SdoCr := ResCred;
            SdoDb := ResDeb;
          end;
        end;
        1:
        begin
          if PermitePrazo then
          begin
            ResVal := (InfoCred - InfoDeb) - InfoPago;
            ResCred := InfoCred;
            ResDeb := InfoDeb;
            //
            if ResCred > 0 then
            begin
              SdoCr := ResVal;
              SdoDB := 0;
            end else begin
              SdoCr := 0;
              SdoDB := -ResVal;
            end;
          end;
        end;
        2,3:
        begin
          ResVal := InfoCred - InfoDeb;
          ResCred := InfoCred;
          ResDeb := InfoDeb;
          //
          SdoCr := ResCred;
          SdoDb := ResDeb;
        end;
      end;
    end;
    else
    begin
      Result := False;
      Geral.MB_Erro('Tipo de carteira n�o definida: ' + Geral.FF0(Sit));
    end;
  end;
end;

function TDModFin.EntidadeHabilitadadaParaFinanceiroNovo(Entidade: Integer;
  ShowMsgSdos, ShowMsgMigra: Boolean): TStatusFinNovo;

  function VerificaTabelas(Codigo: Integer): Boolean;

    procedure VerificaTabela(TabNome, TabBase: String);
    begin
      if not DBCheck.TabelaExiste(TabNome) then
        DBCheck.CriaTabela(Dmod.MyDB, TabNome, TabBase, nil, actCreate, False,
        nil, nil, nil, nil);
    end;

  begin
    Dmod.QrMas.Close;
    Dmod.QrMas.Database := Dmod.MyDB;
    Dmod.QrMas.SQL.Clear;
    Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + TMeuDB);
    UnDmkDAC_PF.AbreQuery(Dmod.QrMas, Dmod.MyDB);
    //
    VerificaTabela(DModG.NomeTab(TMeuDB, ntLct, False, ttA, Codigo), LAN_CTOS);
    VerificaTabela(DModG.NomeTab(TMeuDB, ntLct, False, ttB, Codigo), LAN_CTOS);
    VerificaTabela(DModG.NomeTab(TMeuDB, ntLct, False, ttD, Codigo), LAN_CTOS);
    //
    Result := True;
  end;

var
  Continua, Mostra: Boolean;
  TipoTabLct, CodCliInt: Integer;
begin
  Result := sfnAntigo;
  //
  QrCrt.Close;
  QrCrt.Params[0].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQuery(QrCrt, Dmod.MyDB);
  //
  QrSdo.Close;
  QrSdo.Params[0].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQuery(QrSdo, Dmod.MyDB);
  //
  Continua := (QrCrt.RecordCount = 0) and (QrSdo.RecordCount = 0);
  //
  QrECI.Close;
  if Continua then
  begin
    QrECI.Params[0].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQuery(QrECI, Dmod.MyDB);
    //
    Continua := QrECITipoTabLct.Value = 1; // j� migrou
    if Continua then
      Result := sfnNovoFin
    else begin
      Result := sfnFaltaMigrar;
      if ShowMsgMigra then
      begin
        Mostra := True;
        if QrECITipoTabLct.Value = 0 then
        begin
          if Geral.MB_Pergunta('A entidade ' + FormatFloat('0', Entidade) +
          ' n�o est� habilitada para o novo gerenciamento financeiro.' + sLineBreak+
          'Deseja habilit�-la agora?') = ID_YES then
          begin
            TipoTabLct := 1;
            CodCliInt := QrECICodCliInt.Value;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
            'TipoTabLct'], ['CodCliInt'], [
            TipoTabLct], [CodCliInt], True) then
            begin
              Mostra := not VerificaTabelas(CodCliInt);
            end;
          end;
        end;
        //
        if Mostra then
        Geral.MB_Aviso('A entidade ' + FormatFloat('0', Entidade) +
        ' n�o est� habilitada para o novo gerenciamento financeiro pois ' +
        'ainda n�o teve seus lan�amentos migrados!');
      end;
    end;
  end else
  if ShowMsgSdos then
    Geral.MB_Aviso('A entidade ' + FormatFloat('0', Entidade) +
    ' n�o est� habilitada para o novo gerenciamento financeiro pois possui ' +
    'carteiras e/ou contas com saldo inicial, o que n�o � permitido!');
end;

function TDModFin.ExisteLancamentoSemCliInt(TabLct: String): Boolean;
begin
  QrELSCI.Close;
  QrELSCI.SQL.Clear;
  QrELSCI.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrELSCI.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrELSCI.SQL.Add('/*ci.Unidade UH,*/ la.*, ct.Codigo CONTA, ca.Prazo,');
  QrELSCI.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1,');
  QrELSCI.SQL.Add('ca.ForneceI CART_DONO, ca.Nome NOMECARTEIRA,');
  QrELSCI.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrELSCI.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrELSCI.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,');
  QrELSCI.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
  QrELSCI.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
  QrELSCI.SQL.Add('IF(la.ForneceI=0, "", IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,');
  QrELSCI.SQL.Add('IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,');
  QrELSCI.SQL.Add('IF(la.Cliente>0,');
  QrELSCI.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
  QrELSCI.SQL.Add('  IF (la.Fornecedor>0,');
  QrELSCI.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO');
  QrELSCI.SQL.Add('FROM ' + TabLct + ' la');
  QrELSCI.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrELSCI.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrELSCI.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrELSCI.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrELSCI.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrELSCI.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrELSCI.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrELSCI.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrELSCI.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  QrELSCI.SQL.Add('/*LEFT JOIN condimov  ci ON ci.Conta=la.Depto*/');
  QrELSCI.SQL.Add('WHERE la.CliInt =0');
  QrELSCI.SQL.Add('AND la.Controle <> 0');
  QrELSCI.SQL.Add('ORDER BY la.Data, la.Controle');
  UnDmkDAC_PF.AbreQuery(QrELSCI, Dmod.MyDB);
  //
  Result := QrELSCI.RecordCount > 0;
  if Result then
  begin
    Geral.MB_Erro('A��o abortada! Existem lan�amentos com ' +
    'cliente interno indefinido!' + sLineBreak + 'AVISE A DERMATEK!');
    //
    if DBCheck.CriaFm(TFmFinOrfao1, FmFinOrfao1, afmoNegarComAviso) then
    begin
      FmFinOrfao1.FTabLct := TabLct;
      FmFinOrfao1.ShowModal;
      FmFinOrfao1.Destroy;
    end;
  end;
end;

function TDModFin.FaltaPlaGenFatID(LaAviso1, LaAviso2: TLabel): Integer;
var
  Texto: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSemConta, Dmod.MyDB, [
  'SELECT FatID Codigo, Nome ',
  'FROM ctafat0 ',
  'WHERE PlaGen=0 ',
  '']);
  Result := QrSemConta.RecordCount;
  if Result > 0 then
  begin
    Texto := 'Existem ' + Geral.FF0(Result) +
    ' cadastros de contas de faturamento sem defini��o de conta do plano de contas!';
    Geral.MB_Aviso(Texto);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
  end;
end;

function TDModFin.FaltaPlaGenOcorP(LaAviso1, LaAviso2: TLabel): Integer;
var
  Texto: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSemConta, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM ocorbank ',
  'WHERE PlaGen=0 ',
  'AND Codigo<>0 ',
  'AND Codigo IN ( ',
  '  SELECT DISTINCT Ocorrencia ',
  '  FROM ocorreu ',
  ') ',
  '']);
  Result := QrSemConta.RecordCount;
  if Result > 0 then
  begin
    Texto := 'Existem ' + Geral.FF0(Result) +
    ' cadastros de ocorr�ncias sem defini��o de conta do plano de contas!';
    Geral.MB_Aviso(Texto);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
  end;
end;

function TDModFin.FaltaPlaGenTaxas(LaAviso1, LaAviso2: TLabel): Integer;
var
  Texto: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSemConta, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM taxas ',
  'WHERE PlaGen=0 ',
  'AND Codigo>0 ', // N�o pode ter plagen no c�digo -1!
  '']);
  Result := QrSemConta.RecordCount;
  if Result > 0 then
  begin
    Texto := 'Existem ' + Geral.FF0(Result) +
    ' cadastros de taxas (de c�digo positivo) sem defini��o de conta do plano de contas!';
    Geral.MB_Aviso(Texto);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
  end;
end;

function TDModFin.FaturamentosQuitados(TabLctA: String;
  FatID, FatNum: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLE, Dmod.MyDB, [
    'SELECT COUNT(lct.Controle) Itens ',
    'FROM ' + TabLctA + ' lct ',
    'LEFT JOIN carteiras car ON car.Codigo = lct.Carteira ',
    'WHERE lct.FatID=' + Geral.FF0(FatID),
    'AND lct.FatNum=' + Geral.FF0(FatNum),
    'AND (lct.Sit > 0 OR lct.Compensado > 2) ',
    'AND car.Tipo = 2 ', //Emiss�o (Os bancos e caixas j� lan�am quitados)
    '']);
  //
  Result := QrLEItens.Value;
end;

procedure TDModFin.FolowStepLct(Controle, RelaCtrl, Acao: Integer);
var
  CodSeq: Integer;
  DtHrCad: String;
begin
  //CodSeq := UMyMod.BuscaProximoCtrlGeral('LctStep');
  CodSeq := UMyMod.BPGS1I32('LctStep', 'CodSeq', '', '', tsPos, stIns, 0);
  DtHrCad := DModG.ObtemAgoraTxt();
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctstep', False, [
  'DtHrCad', 'Controle', 'RelaCtrl', 'Acao', 'UserCad'], ['CodSeq'], [
  DtHrCad, Controle, RelaCtrl, Acao, VAR_USUARIO], [CodSeq], False);
end;

procedure TDModFin.frxSaldosGetValue(const VarName: string; var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := DModG.QrEmpresasNOMEFILIAL.Value;
end;

function TDmodFin.GeraBalanceteExtrato(Periodo, Entidade: Integer; PB:
TProgressBar; LaAviso1: TLabel; DtEncerr, DtMorto: TDateTime; TabLctA, TabLctB,
TabLctD: String(*; CliInt_: Integer*)): Boolean;
var
  //CliInt_Txt,
  DataA, DataI, DataF, Texto, Docum, NomeCart, DebCr, FldIni,
  TabLctX, Entidade_Txt: String;
  Saldo, Valor, Credito, Debito: Double;
  CartC: Integer;
  DtA, DtI, DtF: TDateTime;
  //TLW: TTabLctToWork; // = (tlwA, tlwB, tlwD);
begin
  Result := False;
  MyObjects.Informa(LaAviso1, True, 'Preparando pesquisa para gerar extrato');
  Screen.Cursor := crHourGlass;
  //
  DtA   := dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo)-1;
  DtI   := dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
  DtF   := dmkPF.UltimoDiaDoPeriodo_Date(Periodo);
  DataA := Geral.FDT(DtA, 1);
  DataI := Geral.FDT(DtI, 1);
  DataF := Geral.FDT(DtF, 1);
  //
  //CliInt_Txt := FormatFloat('0', CliInt);
  Entidade_Txt := FormatFloat('0', Entidade);
  // Definir tabelas de lct
  if DtI > DtEncerr then
  begin
  // TabLctA
    //TLW := tlwA;
    FldIni := 'car.SdoFimB';
    TabLctX := TabLctA;
  end else
  if DtI > DtMorto then
  begin
  // TabLctB
    //TLW := tlwB;
    FldIni := '0.00';
    TabLctX := TabLctB;
  end
  else begin
  // TabLctD
    //TLW := tlwD;
    FldIni := '0.00';
    TabLctX := TabLctD;
  end;
  //
  // informar o saldo total mesmo que n�o informe os saldos dos n�veis
  QrSTCP.Close;
  QrSTCP.Params[0].AsInteger := Entidade;
  QrSTCP.Params[1].AsInteger := Periodo;
  UnDmkDAC_PF.AbreQuery(QrSTCP, Dmod.MyDB);
  //
  QrExtratos.Close;
  QrExtratos.DataBase := DModG.MyPID_DB;
  QrZer.Close;
  QrZer.DataBase := DModG.MyPID_DB;
  //
  FExtratoCC := UCriar.RecriaTempTable('ExtratoCC', DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO extratocc (DataM,Texto,Docum,NotaF,');
  DModG.QrUpdPID1.SQL.Add('Credi,Debit,Saldo,CartO,CartC,CartN,SdIni,TipoI,');
  DModG.QrUpdPID1.SQL.Add('Unida,CTipN,DebCr) Values(:P00,:P01,:P02,:P03,:P04,');
  DModG.QrUpdPID1.SQL.Add(':P05,:P06,:P07,:P08,:P09,:P10,:P11,:P12,:P13,:P14)');

   //  Saldos Iniciais
  MyObjects.Informa(LaAviso1, True, 'Pesquisando saldos iniciais das carteiras');
  QrPesqCar.Close;
  QrPesqCar.SQL.Clear;
  {
  QrPesqCar.SQL.Add('SELECT car.Codigo Carteira, car.Inicial, car.Nome,');
  QrPesqCar.SQL.Add('car.Nome2, car.Ordem OrdemCart, car.Tipo TipoCart,');
  QrPesqCar.SQL.Add('(');
  QrPesqCar.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
  QrPesqCar.SQL.Add('  FROM lctctos lct');
  QrPesqCar.SQL.Add('  WHERE lct.Carteira=car.Codigo');
  QrPesqCar.SQL.Add('  AND lct.Data < :P0');
  QrPesqCar.SQL.Add(') SALDO');
  QrPesqCar.SQL.Add('FROM carteiras car');
  QrPesqCar.SQL.Add('WHERE car.ForneceI=:P1');
  QrPesqCar.SQL.Add('AND car.Tipo <> 2');
  QrPesqCar.Params[00].AsString  := DataI;
  QrPesqCar.Params[01].AsInteger := Entidade;
  }
  QrPesqCar.SQL.Add('SELECT car.Codigo Carteira, ' + FldIni + ' Inicial, car.Nome,');
  QrPesqCar.SQL.Add('car.Nome2, car.Ordem OrdemCart, car.Tipo TipoCart,');
  QrPesqCar.SQL.Add('(');
  QrPesqCar.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
  QrPesqCar.SQL.Add('  FROM ' + TabLctX + ' lct');
  QrPesqCar.SQL.Add('  WHERE lct.Carteira=car.Codigo');
  QrPesqCar.SQL.Add('  AND lct.Data < "' + DataI + '"');
  QrPesqCar.SQL.Add(') SALDO');
  QrPesqCar.SQL.Add('FROM carteiras car');
  QrPesqCar.SQL.Add('WHERE car.ForneceI=' + Entidade_Txt);
  QrPesqCar.SQL.Add('AND car.Tipo <> 2');
  UnDmkDAC_PF.AbreQuery(QrPesqCar, Dmod.MyDB);
  //
  // Agrupamentos de Contas
  MyObjects.Informa(LaAviso1, True, 'Pesquisando movimento de agrupamentos de contas no per�odo');
  QrPesqAgr.Close;
  QrPesqAgr.SQL.Clear;
  QrPesqAgr.SQL.Add('SELECT con.ContasAgr, car.Nome NOMECART, car.Nome2 NOME2CART,');
  QrPesqAgr.SQL.Add('car.Ordem OrdemCart, car.Tipo TipoCart,');
  QrPesqAgr.SQL.Add('lct.Sit, lct.Carteira, lct.Data, lct.Descricao,');
  QrPesqAgr.SQL.Add('SUM(lct.Credito) CREDITO,');
  QrPesqAgr.SQL.Add('SUM(lct.Debito) DEBITO, lct.SerieCH, lct.Documento, lct.Doc2,');
  QrPesqAgr.SQL.Add('lct.Mez, lct.NotaFiscal,');
  QrPesqAgr.SQL.Add('lct.Depto Apto, cag.Nome NOMEAGR, cag.InfoDescri, cag.Mensal,');
  //
  if VAR_KIND_DEPTO = kdUH then
    QrPesqAgr.SQL.Add('imv.Unidade UH,')
  else
    QrPesqAgr.SQL.Add('"" UH,');
  //
  QrPesqAgr.SQL.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NOMECLI');
  QrPesqAgr.SQL.Add('FROM ' + TabLctX + ' lct');
  QrPesqAgr.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrPesqAgr.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lct.Genero');
  QrPesqAgr.SQL.Add('LEFT JOIN contasagr cag ON cag.Codigo=con.ContasAgr');
  //
  if VAR_KIND_DEPTO = kdUH then
    QrPesqAgr.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lct.Depto');
  //
  QrPesqAgr.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lct.Cliente');
  QrPesqAgr.SQL.Add('WHERE car.Tipo <> 2');
  QrPesqAgr.SQL.Add('AND con.ContasAgr>0');
  QrPesqAgr.SQL.Add('AND car.ForneceI=' + Entidade_Txt);
  QrPesqAgr.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
  QrPesqAgr.SQL.Add('GROUP BY con.ContasAgr, lct.Data, lct.Cliente, lct.Fornecedor,');
  QrPesqAgr.SQL.Add('lct.CliInt, lct.Depto, lct.Mez, lct.Carteira');
  QrPesqAgr.SQL.Add('ORDER BY car.Nome, car.Codigo, lct.Data, lct.Controle,');
  QrPesqAgr.SQL.Add('lct.Credito DESC, lct.Debito');
  (*
  QrPesqAgr.Params[00].AsInteger := Entidade;
  QrPesqAgr.Params[01].AsString  := DataI;
  QrPesqAgr.Params[02].AsString  := DataF;
  *)
  UnDmkDAC_PF.AbreQuery(QrPesqAgr, Dmod.MyDB);
  // Soma itens semelhantes
  MyObjects.Informa(LaAviso1, True, 'Pesquisando movimento de contas semelhantes no per�odo');
  QrPesqSum.Close;
  QrPesqSum.SQL.Clear;
  QrPesqSum.SQL.Add('SELECT con.ContasAgr, car.Nome NOMECART, ');
  QrPesqSum.SQL.Add('car.Nome2 NOME2CART, car.Tipo TipoCart,car.Ordem OrdemCart,');
  // lct.Sit: Para prevenir sit sem implementa��o
  QrPesqSum.SQL.Add('lct.Sit, lct.Carteira, lct.Data, lct.Descricao, lct.NotaFiscal,');
  QrPesqSum.SQL.Add('SUM(lct.Credito) CREDITO, SUM(lct.Debito) DEBITO, lct.Mez');
  QrPesqSum.SQL.Add('FROM ' + TabLctX + ' lct');
  QrPesqSum.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrPesqSum.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lct.Genero');
  QrPesqSum.SQL.Add('/*LEFT JOIN contassum csu ON csu.Codigo=con.ContasSum*/');
  QrPesqSum.SQL.Add('WHERE car.Tipo <> 2');
  QrPesqSum.SQL.Add('AND con.ContasAgr=0');
  QrPesqSum.SQL.Add('AND con.ContasSum>0');
  QrPesqSum.SQL.Add('AND car.ForneceI=' + Entidade_Txt);
  QrPesqSum.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
  QrPesqSum.SQL.Add('GROUP BY con.Codigo, lct.Data, lct.Mez, lct.Descricao, lct.Sit');
  QrPesqSum.SQL.Add('ORDER BY car.Nome, car.Codigo, lct.Data, lct.Controle,');
  QrPesqSum.SQL.Add('lct.Credito DESC, lct.Debito');
  (*
  QrPesqSum.Params[00].AsInteger := Entidade;
  QrPesqSum.Params[01].AsString  := DataI;
  QrPesqSum.Params[02].AsString  := DataF;
  *)
  UnDmkDAC_PF.AbreQuery(QrPesqSum, Dmod.MyDB);
  // Soma itens restantes
  MyObjects.Informa(LaAviso1, True, 'Pesquisando movimento de contas n�o agrup�veis nem semelhantes no per�odo');
  QrPesqRes.Close;
  QrPesqRes.SQL.Clear;
  QrPesqRes.SQL.Add('SELECT con.Nome NOMECONTA, ');
  QrPesqRes.SQL.Add('car.Nome NOMECART, car.Nome2 NOME2CART, car.Tipo TipoCart,');
  QrPesqRes.SQL.Add('car.Ordem OrdemCart, lct.Sit, lct.Carteira, lct.Data, lct.Descricao, lct.Credito,');
  QrPesqRes.SQL.Add('lct.Debito, lct.Mez, lct.SerieCH, lct.Documento, lct.Depto, lct.NotaFiscal');
  //QrPesqRes.SQL.Add('/*, imv.Unidade*/');
  //
  if VAR_KIND_DEPTO = kdUH then
    QrPesqRes.SQL.Add(',imv.Unidade UH')
  else
    QrPesqRes.SQL.Add(',"" UH');
  //
  QrPesqRes.SQL.Add('FROM ' + TabLctX + ' lct');
  QrPesqRes.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrPesqRes.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lct.Genero');
  //QrPesqRes.SQL.Add('/*LEFT JOIN condimov  imv ON imv.Conta=lct.Depto*/');
  //
  if VAR_KIND_DEPTO = kdUH then
    QrPesqRes.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lct.Depto');
  //
  QrPesqRes.SQL.Add('');
  QrPesqRes.SQL.Add('WHERE car.Tipo <> 2');
  QrPesqRes.SQL.Add('AND con.ContasAgr=0');
  QrPesqRes.SQL.Add('AND con.ContasSum=0');
  QrPesqRes.SQL.Add('AND car.ForneceI=' + Entidade_Txt);
  QrPesqRes.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
{
  QrPesqRes.Params[00].AsInteger := Entidade;
  QrPesqRes.Params[01].AsString  := DataI;
  QrPesqRes.Params[02].AsString  := DataF;
}
  UnDmkDAC_PF.AbreQuery(QrPesqRes, Dmod.MyDB);

  //
  MyObjects.Informa(LaAviso1, True, '');
  if PB <> nil then
  begin
    PB.Position := 0;
    PB.Visible := True;
    PB.Max := QrPesqCar.RecordCount + QrPesqAgr.RecordCount +
              QrPesqSum.RecordCount + QrPesqRes.RecordCount;
    PB.Update;
    Application.ProcessMessages;
  end;

  //  Saldos Iniciais
  MyObjects.Informa(LaAviso1, True, 'Definindo saldos iniciais das carteiras');
  if QrPesqCar.RecordCount > 0 then
  begin
    QrPesqCar.First;
    while not QrPesqCar.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      Valor := QrPesqCarInicial.Value + QrPesqCarSALDO.Value;
{
      if not VerificaSit(QrPesqCarSit.Value, Valor, Valor) then
        Exit;
}
      if Valor < 0 then
      begin
        Credito := 0;
        Debito := -Valor;
      end else begin
        Credito := Valor;
        Debito  := 0;
      end;
      Docum := '';
      if Credito > 0 then DebCr := 'C' else DebCr := 'D';
      if Trim(QrPesqCarNome2.Value) <> '' then NomeCart := QrPesqCarNome2.Value
      else NomeCart := QrPesqCarNome.Value;
      DModG.QrUpdPID1.Params[00].AsString  := DataA;
      DModG.QrUpdPID1.Params[01].AsString  := 'S A L D O';//Texto;
      DModG.QrUpdPID1.Params[02].AsString  := '';//Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := 0;// NotaF
      DModG.QrUpdPID1.Params[04].AsFloat   := Credito;
      DModG.QrUpdPID1.Params[05].AsFloat   := Debito;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0;// n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqCarOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqCarCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 1; // Sim
      DModG.QrUpdPID1.Params[11].AsInteger := 0; // Saldo Inicial
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := dmkPF.GetNomeTipoCart(QrPesqCarTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqCar.Next;
    end;
  end;

  // Agrupamentos de Contas
  MyObjects.Informa(LaAviso1, True, 'Definindo movimento de agrupamentos de contas no per�odo');
  if QrPesqAgr.RecordCount > 0 then
  begin
    QrPesqAgr.First;
    while not QrPesqAgr.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      Texto := QrPesqAgrNOMEAGR.Value;
      case QrPesqAgrInfoDescri.Value of
        1: Texto := Texto + ': ' + QrPesqAgrUH.Value;
        2: Texto := Texto + ': ' + QrPesqAgrNOMECLI.Value;
      end;
      if (QrPesqAgrMensal.Value > 0) and (QrPesqAgrMez.Value > 0)then
        Texto := Texto + ' - ' + dmkPF.MezToFDT(QrPesqAgrMez.Value, 0, 104);
      //
      Docum := QrPesqAgrSerieCH.Value;
      if Trim(QrPesqAgrNOME2CART.Value) <> '' then
        NomeCart := QrPesqAgrNOME2CART.Value
      else
        NomeCart := QrPesqAgrNOMECART.Value;
      if QrPesqAgrDocumento.Value >0 then
        Docum := Docum + FormatFloat('000000', QrPesqAgrDocumento.Value);
      if QrPesqAgrCREDITO.Value > 0 then
        DebCr := 'C'
      else
        DebCr := 'D';
      if not VerificaSit(QrPesqAgrSit.Value, QrPesqAgrCREDITO.Value,
      QrPesqAgrDEBITO.Value, Credito, Debito) then
        Exit;
      if Trim(Texto) = '' then
        Texto := QrPesqAgrNOMEAGR.Value;
      //
      DModG.QrUpdPID1.Params[00].AsString  := Geral.FDT(QrPesqAgrData.Value, 1);
      DModG.QrUpdPID1.Params[01].AsString  := Texto;
      DModG.QrUpdPID1.Params[02].AsString  := Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := QrPesqAgrNotaFiscal.Value;
      DModG.QrUpdPID1.Params[04].AsFloat   := Credito;
      DModG.QrUpdPID1.Params[05].AsFloat   := Debito;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqAgrOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqAgrCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 0; // N�o
      DModG.QrUpdPID1.Params[11].AsInteger := 1; // Agrupamentos de contas
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := dmkPF.GetNomeTipoCart(QrPesqAgrTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';//QrPesqAgrUH.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqAgr.Next;
    end;
  end;

  // Soma itens semelhantes
  MyObjects.Informa(LaAviso1, True, 'Definindo movimento de contas semelhantes no per�odo');
  if QrPesqSum.RecordCount > 0 then
  begin
    QrPesqSum.First;
    while not QrPesqSum.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      //
      Docum := '';
      if Trim(QrPesqSumNOME2CART.Value) <> '' then
        NomeCart := QrPesqSumNOME2CART.Value
      else
        NomeCart := QrPesqSumNOMECART.Value;
      if QrPesqSumCREDITO.Value > 0 then DebCr := 'C' else DebCr := 'D';
      if not VerificaSit(QrPesqSumSit.Value, QrPesqSumCREDITO.Value,
      QrPesqSumDEBITO.Value, Credito, Debito) then
        Exit;
      DModG.QrUpdPID1.Params[00].AsString  := Geral.FDT(QrPesqSumData.Value, 1);
      DModG.QrUpdPID1.Params[01].AsString  := QrPesqSumDescricao.Value;
      DModG.QrUpdPID1.Params[02].AsString  := Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := QrPesqSumNotaFiscal.Value;
      DModG.QrUpdPID1.Params[04].AsFloat   := QrPesqSumCREDITO.Value;
      DModG.QrUpdPID1.Params[05].AsFloat   := QrPesqSumDEBITO.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqSumOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqSumCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 0; // N�o
      DModG.QrUpdPID1.Params[11].AsInteger := 2; // Soma itens semelhantes
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := dmkPF.GetNomeTipoCart(QrPesqSumTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqSum.Next;
    end;
  end;

  // Soma itens restantantes
  MyObjects.Informa(LaAviso1, True, 'Defifnindo movimento de contas n�o agrup�veis nem semelhantes no per�odo');
  if QrPesqRes.RecordCount > 0 then
  begin
    QrPesqRes.First;
    while not QrPesqRes.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      //
      Docum := QrPesqResSerieCH.Value;
      if Trim(QrPesqResNOME2CART.Value) <> '' then
        NomeCart := QrPesqResNOME2CART.Value
      else
        NomeCart := QrPesqResNOMECART.Value;
      if QrPesqResDocumento.Value >0 then
        Docum := Docum + FormatFloat('000000', QrPesqResDocumento.Value);
      //
      if Trim(QrPesqResNOME2CART.Value) <> '' then
        NomeCart := QrPesqResNOME2CART.Value
      else
        NomeCart := QrPesqResNOMECART.Value;
      if QrPesqResCredito.Value > 0 then DebCr := 'C' else DebCr := 'D';
      if not VerificaSit(QrPesqResSit.Value, QrPesqResCredito.Value,
      QrPesqResDebito.Value, Credito, Debito) then
        Exit;
      if Trim(QrPesqResDescricao.Value) <> '' then
        Texto := QrPesqResDescricao.Value
      else  
        Texto := QrPesqResNOMECONTA.Value;
      //
      DModG.QrUpdPID1.Params[00].AsString  := Geral.FDT(QrPesqResData.Value, 1);
      DModG.QrUpdPID1.Params[01].AsString  := Texto;
      DModG.QrUpdPID1.Params[02].AsString  := Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := QrPesqResNotaFiscal.Value;
      DModG.QrUpdPID1.Params[04].AsFloat   := Credito;
      DModG.QrUpdPID1.Params[05].AsFloat   := Debito;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqResOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqResCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 0; // N�o
      DModG.QrUpdPID1.Params[11].AsInteger := 3; // Soma itens restantes
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := dmkPF.GetNomeTipoCart(QrPesqResTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';//QrPesqResUnidade.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqRes.Next;
    end;
  end;
  //
  // Excluir carteiras que n�o tem movimento e nem saldo
  MyObjects.Informa(LaAviso1, True, 'Limpando dados desnecess�rios');
  DModG.QrUpdPID2.SQL.Clear;
  DModG.QrUpdPID2.SQL.Add('DELETE FROM extratocc WHERE CartC=:P0');
  QrZer.Close;
  UnDmkDAC_PF.AbreQuery(QrZer, DModG.MyPID_DB);
  while not QrZer.Eof do
  begin
    if (QrZerCredi.Value = 0) and (QrZerDebit.Value = 0) then
    begin
      DModG.QrUpdPID2.Params[0].AsInteger := QrZerCartC.Value;
      DModG.QrUpdPID2.ExecSQL;
    end;
    //
    QrZer.Next;
  end;
  // FIM - Excluir carteiras que n�o tem movimento e nem saldo
  //
  MyObjects.Informa(LaAviso1, True, 'Atribuindo saldo final de cada linha do extrato');
  DModG.QrUpdPID2.SQL.Clear;
  DModG.QrUpdPID2.SQL.Add('UPDATE extratocc SET Saldo=:P0 ');
  DModG.QrUpdPID2.SQL.Add('WHERE Linha=:P1 ');
  DModG.QrUpdPID2.SQL.Add('');
  Saldo := 0;
  QrExtratos.Close;
  QrExtratos.SQL.Clear;
  QrExtratos.SQL.Add('SELECT * FROM extratocc');
  QrExtratos.SQL.Add('ORDER BY CartO, CartN, CartC, DataM, SdIni, DebCr, ');
  QrExtratos.SQL.Add('Unida,  Docum, TipoI');
  UnDmkDAC_PF.AbreQuery(QrExtratos, DModG.MyPID_DB);
  QrExtratos.First;
  CartC := -1000;
  while not QrExtratos.Eof do
  begin
    if CartC <> QrExtratosCartC.Value then
    begin
      Saldo := QrExtratosCredi.Value - QrExtratosDebit.Value;
      CartC := QrExtratosCartC.Value;
    end else begin
      Saldo := Saldo + QrExtratosCredi.Value - QrExtratosDebit.Value;
    end;
    //
    DModG.QrUpdPID2.Params[00].AsFloat   := Saldo;
    DModG.QrUpdPID2.Params[01].AsInteger := QrExtratosLinha.Value;
    DModG.QrUpdPID2.ExecSQL;
    //
    QrExtratos.Next;
  end;
  MyObjects.Informa(LaAviso1, True, 'Abrindo resultados que ir�o gerar o relat�rio');
  QrExtratos.Close;
  UnDmkDAC_PF.AbreQuery(QrExtratos, DModG.MyPID_DB);
  //
{
SELECT IF(car.Tipo<>2,lct.Carteira, bco.Codigo) Carteira,
SUM(lct.Credito) SDO_CRE, SUM(lct.Debito) SDO_DEB
FROM lctctos lct
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
LEFT JOIN carteiras bco ON bco.Codigo=car.Banco
WHERE lct.Genero = -1
AND car.ForneceI=:P0
AND lct.Data BETWEEN :P1 AND :P2
GROUP BY Carteira
}
  QrSdoTrf.Close;
  QrSdoTrf.SQL.Clear;
  QrSdoTrf.SQL.Add('SELECT IF(car.Tipo<>2,lct.Carteira, bco.Codigo) Carteira,');
  QrSdoTrf.SQL.Add('SUM(lct.Credito) SDO_CRE, SUM(lct.Debito) SDO_DEB');
  QrSdoTrf.SQL.Add('FROM ' + TabLctX + ' lct');
  QrSdoTrf.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrSdoTrf.SQL.Add('LEFT JOIN carteiras bco ON bco.Codigo=car.Banco');
  QrSdoTrf.SQL.Add('WHERE lct.Genero = -1');
  QrSdoTrf.SQL.Add('AND car.ForneceI=' + Entidade_Txt);
  QrSdoTrf.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
  QrSdoTrf.SQL.Add('GROUP BY Carteira');
{
  QrSdoTrf.Params[00].AsInteger := Entidade;
  QrSdoTrf.Params[01].AsString  := DataI;
  QrSdoTrf.Params[02].AsString  := DataF;
}
  //parei aqui
  UnDmkDAC_PF.AbreQuery(QrSdoTrf, Dmod.MyDB);
  //
  QrSdoMov.Close;
{
SELECT lct.Carteira,
SUM(lct.Credito) SDO_CRE, SUM(lct.Debito) SDO_DEB
FROM lctctos lct
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
WHERE (lct.Genero > 0
OR lct.Genero=-10)
AND car.Tipo <> 2
AND car.ForneceI=:P0
AND lct.Data BETWEEN :P1 AND :P2
GROUP BY lct.Carteira
}
  QrSdoMov.SQL.Clear;
  QrSdoMov.SQL.Add('SELECT lct.Carteira,');
  QrSdoMov.SQL.Add('SUM(lct.Credito) SDO_CRE, SUM(lct.Debito) SDO_DEB');
  QrSdoMov.SQL.Add('FROM ' + TabLctX + ' lct');
  QrSdoMov.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrSdoMov.SQL.Add('WHERE (lct.Genero > 0');
  QrSdoMov.SQL.Add('OR lct.Genero=-10)');
  QrSdoMov.SQL.Add('AND car.Tipo <> 2');
  QrSdoMov.SQL.Add('AND car.ForneceI=' + Entidade_Txt);
  QrSdoMov.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
  QrSdoMov.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
  QrSdoMov.SQL.Add('GROUP BY lct.Carteira');
{
  QrSdoMov.Params[00].AsInteger := Entidade;
  QrSdoMov.Params[01].AsString  := DataI;
  QrSdoMov.Params[02].AsString  := DataF;
}
  UnDmkDAC_PF.AbreQuery(QrSdoMov, Dmod.MyDB);
  //
  QrSdoCtas.Close;
{
SELECT 0 KGT, car.Codigo Carteira, car.Inicial SDO_CAD, car.Nome,
car.Nome2, car.Ordem OrdemCart, car.Tipo,
ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS�O","?") NO_TipoCart,
(
  SELECT SUM(lct.Credito-lct.Debito)
  FROM lctctos lct
  WHERE lct.Carteira=car.Codigo
  AND lct.Data < :P0
) SDO_ANT
FROM carteiras car
WHERE car.ForneceI=:P1
AND car.Tipo <> 2
AND
(
  car.Ativo=1
  OR
  car.Saldo <> 0
)
AND car.Exclusivo=0

}
  QrSdoCtas.SQL.Clear;
  QrSdoCtas.SQL.Add('SELECT 0 KGT, car.Codigo Carteira, ' + FldIni + ' SDO_CAD, ');
  QrSdoCtas.SQL.Add('car.Nome, car.Nome2, car.Ordem OrdemCart, car.Tipo,');
  QrSdoCtas.SQL.Add('ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS�O","?") NO_TipoCart,');
  QrSdoCtas.SQL.Add('(');
  QrSdoCtas.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
  QrSdoCtas.SQL.Add('  FROM ' + TabLctX + ' lct');
  QrSdoCtas.SQL.Add('  WHERE lct.Carteira=car.Codigo');
  QrSdoCtas.SQL.Add('  AND lct.Data < "' + DataI + '"');
  QrSdoCtas.SQL.Add(') SDO_ANT');
  QrSdoCtas.SQL.Add('FROM carteiras car');
  QrSdoCtas.SQL.Add('WHERE car.ForneceI=' + Entidade_Txt);
  QrSdoCtas.SQL.Add('AND car.Tipo <> 2');
  QrSdoCtas.SQL.Add('AND');
  QrSdoCtas.SQL.Add('(');
  QrSdoCtas.SQL.Add('  car.Ativo=1');
  QrSdoCtas.SQL.Add('  OR');
  QrSdoCtas.SQL.Add('  car.Saldo <> 0');
  QrSdoCtas.SQL.Add(')');
  QrSdoCtas.SQL.Add('AND car.Exclusivo=0');
{
  QrSdoCtas.Params[00].AsString  := DataI;
  QrSdoCtas.Params[01].AsInteger := Entidade;
}
  //dmkPF.LeMeuSQLy(QrSdoCtas, '', nil, False, True);
  UnDmkDAC_PF.AbreQuery(QrSdoCtas, Dmod.MyDB);
  //
  QrSdoExcl.Close;
{
SELECT 0 KGT, car.Codigo Carteira, car.Inicial SDO_CAD, car.Nome,
car.Nome2, car.Ordem OrdemCart, car.Tipo,
ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS�O","?") NO_TipoCart,
(
  SELECT SUM(lct.Credito-lct.Debito)
  FROM lctctos lct
  WHERE lct.Carteira=car.Codigo
  AND lct.Data < :P0
) SDO_ANT
FROM carteiras car
WHERE car.ForneceI=:P1
AND car.Tipo <> 2
AND
(
  car.Ativo=1
  OR
  car.Saldo <> 0
)
AND car.Exclusivo=1
}
  QrSdoExcl.SQL.Clear;
  QrSdoExcl.SQL.Add('SELECT 0 KGT, car.Codigo Carteira, ' + FldIni + ' SDO_CAD, car.Nome,');
  QrSdoExcl.SQL.Add('car.Nome2, car.Ordem OrdemCart, car.Tipo,');
  QrSdoExcl.SQL.Add('ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS�O","?") NO_TipoCart,');
  QrSdoExcl.SQL.Add('(');
  QrSdoExcl.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
  QrSdoExcl.SQL.Add('  FROM ' + TabLctX + ' lct');
  QrSdoExcl.SQL.Add('  WHERE lct.Carteira=car.Codigo');
  QrSdoExcl.SQL.Add('  AND lct.Data < "' + DataI + '"');
  QrSdoExcl.SQL.Add(') SDO_ANT');
  QrSdoExcl.SQL.Add('FROM carteiras car');
  QrSdoExcl.SQL.Add('WHERE car.ForneceI=' + Entidade_Txt);
  QrSdoExcl.SQL.Add('AND car.Tipo <> 2');
  QrSdoExcl.SQL.Add('AND');
  QrSdoExcl.SQL.Add('(');
  QrSdoExcl.SQL.Add('  car.Ativo=1');
  QrSdoExcl.SQL.Add('  OR');
  QrSdoExcl.SQL.Add('  car.Saldo <> 0');
  QrSdoExcl.SQL.Add(')');
  QrSdoExcl.SQL.Add('AND car.Exclusivo=1');

{
  QrSdoExcl.Params[00].AsString  := DataI;
  QrSdoExcl.Params[01].AsInteger := Entidade;
}
  UnDmkDAC_PF.AbreQuery(QrSdoExcl, Dmod.MyDB);
  //
  MyObjects.Informa(LaAviso1, False, '...');
  // N�o ocultar. � usado depois
  //if PB <> nil then PB.Visible := False;
  if PB <> nil then
    PB.Position := 0;
  Screen.Cursor := crDefault;
  //
  Result := True;
end;

function TDModFin.GeraBalanceteRecDebi(Periodo, CliInt: Integer; Acordos,
  Periodos, Textos: Boolean; DtEncer, DtMorto: TDateTime;
  TabLctA, TabLctB, TabLctD: String): Boolean;
var
  FldIni, TabLctX, DataI, DataF: String;
  DtI: TDateTime;
begin
  DtI   := dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
{
  DtF   := dmkPF.UltimoDiaDoPeriodo_Date(Periodo);
  DataA := Geral.FDT(DtA, 1);
  DataI := Geral.FDT(DtI, 1);
  DataF := Geral.FDT(DtF, 1);
  //
  CliInt_Txt := FormatFloat('0', CliInt);
  Entidade_Txt := FormatFloat('0', Entidade);
  // Definir tabelas de lct
}
  if DtI > DtEncer then
  begin
  // TabLctA
    //TLW := tlwA;
    FldIni := 'car.SdoFimB';
    TabLctX := TabLctA;
  end else
  if DtI > DtMorto then
  begin
  // TabLctB
    //TLW := tlwB;
    FldIni := '0.00';
    TabLctX := TabLctB;
  end
  else begin
  // TabLctD
    //TLW := tlwD;
    FldIni := '0.00';
    TabLctX := TabLctD;
  end;
  //
  Screen.Cursor := crHourGlass;
  DataI         := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo), 1);
  DataF         := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(Periodo), 1);
  //
  ReopenCreditosEDebitos(CliInt, DataI, DataF, Acordos, Periodos, Textos,
    False, TabLctX);
  //
  QrSaldoA.Close;
{
SELECT SUM(car.Inicial) Inicial,
(
  SELECT SUM(lct.Credito-lct.Debito)
  FROM lctctos lct
  LEFT JOIN carteiras crt ON crt.Codigo=lct.Carteira
  WHERE crt.Tipo <> 2
  AND crt.ForneceI=:P0
  AND lct.Data < :P1
) SALDO
FROM carteiras car
WHERE car.ForneceI=:P2
AND car.Tipo <> 2
}
  QrSaldoA.SQL.Clear;
  QrSaldoA.SQL.Add('SELECT SUM(' + FldIni + ') Inicial,');
  QrSaldoA.SQL.Add('(');
  QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
  QrSaldoA.SQL.Add('  FROM ' + TabLctX + ' lct');
  QrSaldoA.SQL.Add('  LEFT JOIN carteiras crt ON crt.Codigo=lct.Carteira');
  QrSaldoA.SQL.Add('  WHERE crt.Tipo <> 2');
  QrSaldoA.SQL.Add('  AND crt.ForneceI=' + FormatFloat('0', CliInt));
  QrSaldoA.SQL.Add('  AND lct.Data < "' + DataI + '"');
  QrSaldoA.SQL.Add(') SALDO');
  QrSaldoA.SQL.Add('FROM carteiras car');
  QrSaldoA.SQL.Add('WHERE car.ForneceI=' + FormatFloat('0', CliInt));
  QrSaldoA.SQL.Add('AND car.Tipo <> 2');
  {
  QrSaldoA.Params[00].AsInteger := CliInt;
  QrSaldoA.Params[01].AsString  := DataI;
  QrSaldoA.Params[02].AsInteger := CliInt;
  }
  UnDmkDAC_PF.AbreQuery(QrSaldoA, Dmod.MyDB);

  // Deve ser ap�s SaldoA
  QrResumo.Close;
{
SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito
FROM lctctos lct
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
WHERE lct.Tipo <> 2
AND lct.Genero > 0
AND car.ForneceI = :P0
AND lct.Data BETWEEN :P1 AND :P2
}
  QrResumo.SQL.Clear;
  QrResumo.SQL.Add('SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito');
  QrResumo.SQL.Add('FROM ' + TabLctX + ' lct');
  QrResumo.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrResumo.SQL.Add('WHERE lct.Tipo <> 2');
  QrResumo.SQL.Add('AND lct.Genero > 0');
  // 2011-12-31
  QrResumo.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
  // Fim 2011-12-31
  QrResumo.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
  QrResumo.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
  {
  QrResumo.Params[00].AsInteger := CliInt;
  QrResumo.Params[01].AsString  := DataI;
  QrResumo.Params[02].AsString  := DataF;
  }
  UnDmkDAC_PF.AbreQuery(QrResumo, Dmod.MyDB);

  Result := True;
end;

procedure TDModFin.GeraDadosSaldos(Data: TDateTime; Exclusivo,
  NaoZerados: Boolean);

  procedure InsereRegistros(Qry: TmySQLQuery; Tipo: Integer);
  begin
    Qry.First;
    //
    while not Qry.Eof do
    begin
      if (NaoZerados = False) or (Qry.FieldByName('Saldo').AsFloat <> 0) then
      begin
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSaldos, False, [
        'CartCodi', 'CartTipo', 'CartNiv2',
        'Nome', 'Saldo', 'Tipo'], [
        ], [
        Qry.FieldByName('CartCodi').AsInteger,
        Qry.FieldByName('CartTipo').AsInteger,
        Qry.FieldByName('CartNiv2').AsInteger,
        Qry.FieldByName('Nome').AsString,
        Qry.FieldByName('Saldo').AsFloat,
        Tipo], [
        ], False);
      end;
      //
      Qry.Next;
    end;
  end;

var
  DtTxt: String;
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DmodFin);
  Qry.Database := Dmod.MyDB;
  //
  DtTxt := FormatDateTime(VAR_FORMATDATE, Data);
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  FSaldos := UCriarFin.RecriaTempTableNovo(ntrtt_Saldos, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ca.Codigo CartCodi, ca.Tipo CartTipo, ca.CartNiv2, ',
  'ca.Nome, (SUM(la.Credito-la.Debito) + ca.Inicial) Saldo',
  'FROM ' + VAR_LCT + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'WHERE la.Tipo < 2',
  'AND la.Data<="' + DtTxt + '"',
  'AND ca.Exclusivo<=' + FormatFloat('0', Geral.BoolToInt(Exclusivo)),
  'GROUP BY ca.Nome',
  '']);
  InsereRegistros(Qry, 1);
  //
  (* A tabela consignacoesits n�o existe mais
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT 0 CartCodi, -1 CartTipo, 0 CartNiv2, ',
  'co.Descricao Nome, SUM(ci.Debito-ci.Credito) Saldo',
  'FROM consignacoesits ci, Consignacoes co',
  'WHERE ci.Codigo=co.Codigo',
  'AND ci.Data<="' + DtTxt + '"',
  'GROUP BY co.Codigo',
  '']);
  InsereRegistros(Qry, 2);
  *)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT 0 CartCodi, -1 CartTipo, 0 CartNiv2, ',
  'ca.Nome,',
  ' SUM(la.Credito - la.Debito) SALDO',
  'FROM ' + VAR_LCT + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'WHERE la.Tipo=2',
  'AND la.Data<="' + DtTxt + '"',
  'AND (la.Compensado="1899/12/30" ',
  ' OR Compensado="" OR la.Compensado<2 ',
  '  OR la.Compensado>"' + DtTxt + '")',
  'GROUP BY la.Carteira',
  '']);
  InsereRegistros(Qry, 3);
end;

procedure TDModFin.GeraReciboImpCabLocacao(FatID, LocFCab_Controle, Emitente,
  Beneficiario: Integer; ValorPago: Double; TextoRecibo: String);
  //
  procedure IncluiReciboImpCab(const Valor: Double; var NewCod: Integer);
  var
    emit_CNPJ, emit_CPF, emit_IE,
    DtRecibo, DtPrnted, CNPJCPF, DtConfrmad, Nome: String;
    Codigo: Integer;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := 0;
    NewCod         := 0;
    DtRecibo       := Geral.FDT(DModG.ObtemAgora(), 109);
    DtPrnted       := '1899-12-31 12:00:00';
    DModG.ObtemCNPJ_CPF_IE_DeEntidade(Emitente, emit_CNPJ, emit_CPF, emit_IE);
    if emit_CNPJ <> EmptyStr then
      CNPJCPF      := emit_CNPJ
    else
      CNPJCPF      := emit_CPF;
    DtConfrmad     := '1899-12-31 12:00:00';
    Nome           := TextoRecibo;
    //

    //
    Codigo := UMyMod.BPGS1I32('reciboimpcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'reciboimpcab', False, [
    'DtRecibo', 'DtPrnted', 'Emitente',
    'Beneficiario', 'CNPJCPF', 'Valor',
    'DtConfrmad', 'Nome'], [
    'Codigo'], [
    DtRecibo, DtPrnted, Emitente,
    Beneficiario, CNPJCPF, Valor,
    DtConfrmad, Nome], [
    Codigo], True) then
    begin
      NewCod := Codigo;
    end;
  end;
  //
  procedure IncluiReciboImpDstAtual(const Codigo: Integer);
  var
    Controle, LctCtrl, LctSub: Integer;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    //Codigo         := ;
    Controle       := 0;
    LctCtrl        := QrLocFItsLancto.Value;
    LctSub         := 0;
    //
    Controle := UMyMod.BPGS1I32('reciboimpdst', 'Controle', '', '', tsPos, SQLType, Controle);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'reciboimpdst', False, [
    'Codigo', 'LctCtrl', 'LctSub'], [
    'Controle'], [
    Codigo, LctCtrl, LctSub], [
    Controle], True);
  end;
var
  I, Codigo: Integer;
  Continua: Boolean;
  Corda: String;
  ValorPndt, TmpVal: Double;
  ItsDesneces: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocFIts, Dmod.MyDB, [
  'SELECT lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(LocFCab_Controle),
  'AND FatID IN(0, '+ Geral.FF0(FatID) + ') ',
  '']);
  //
  Codigo := 0;
  IncluiReciboImpCab(ValorPago, Codigo);
  if Codigo <> 0 then
  begin
    QrLocFIts.First;
    while not QrLocFIts.Eof do
    begin
      IncluiReciboImpDstAtual(Codigo);
      //
      QrLocFIts.Next;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'locfcab', False, [
    'ReciboImp'], ['Controle'], [Codigo], [LocFCab_Controle], True) then
    begin
      if DBCheck.CriaFm(TFmReciboImpCab, FmReciboImpCab, afmoNegarComAviso) then
      begin
        FmReciboImpCab.FTabLctA := ''; // TabLctA;
        FmReciboImpCab.FNewCod := Codigo;
        FmReciboImpCab.FValPag := ValorPago;
        FmReciboImpCab.FModuleLctX := nil; // ModuleLctX;

        FmReciboImpCab.LocCod(Codigo, Codigo);
        // N�o precisa! J� inclu�do acima pelo QrLocFIts!!!
        //if FmReciboImpCab.QrReciboImpCabCodigo.Value = Codigo then
          //FmReciboImpCab.InserePagamento(ValorPago);
        FmReciboImpCab.ShowModal;
        FmReciboImpCab.Destroy;
      end;
    end;
    //
  end;
end;

function TDModFin.HahContasSemFat(AvisaCanc: Boolean): Boolean;
var
  TxtCanc: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtaSemFat, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctafat0 ',
  'WHERE FatID=0 ',
  'OR PlaGen=0 ',
  '']);
  //
  Result := QrCtaSemFat.RecordCount > 0;
  if Result then
  begin
    if AvisaCanc then
      TxtCanc := 'A��o cancelada!' + sLineBreak
    else
      TxtCanc := '';
    //
    Geral.MB_Aviso(TxtCanc + 'Existem ' + Geral.FF0(
    QrCtaSemFat.RecordCount) + ' contas de faturamento indefinidas!');
  end;
end;

procedure TDModFin.ImprimeSaldos(CliInt: Integer);
begin
  if DModG.QrEmpresas.Locate('Codigo', CliInt, []) then
  begin
    QrSaldos.Close;
    QrSaldos.Params[0].AsInteger := CliInt;
    UnDmkDAC_PF.AbreQuery(QrSaldos, Dmod.MyDB);
    while not QrSaldos.Eof do
    begin
      UFinanceiro.RecalcSaldoCarteira(QrSaldosCodigo.Value, nil, nil, False, False);
      QrSaldos.Next;
    end;
    //
    FSaldos := UCriarFin.RecriaTempTableNovo(ntrtt_Saldos, DModG.QrUpdPID1, False);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSaldos + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Nome=:P0, Saldo=:P1, Tipo=:P2');
    QrSaldos.Close;
    QrSaldos.Params[0].AsInteger := CliInt;
    UnDmkDAC_PF.AbreQuery(QrSaldos, Dmod.MyDB);
    while not QrSaldos.Eof do
    begin
      DModG.QrUpdPID1.Params[00].AsString  := QrSaldosNome.Value;
      DModG.QrUpdPID1.Params[01].AsFloat   := QrSaldosSaldo.Value;
      DModG.QrUpdPID1.Params[02].AsInteger := 1;
      DModG.QrUpdPID1.ExecSQL;
      QrSaldos.Next;
    end;
    //
    (* A tabela consignacoesits n�o existe mais
    QrConsigna.Close;
    QrConsigna.Params[0].AsInteger := CliInt;
    UnDmkDAC_PF.AbreQuery(QrConsigna, Dmod.MyDB);
    while not QrConsigna.Eof do
    begin
      DModG.QrUpdPID1.Params[00].AsString  := QrConsignaNome.Value;
      DModG.QrUpdPID1.Params[01].AsFloat   := QrConsignaSaldo.Value;
      DModG.QrUpdPID1.Params[02].AsInteger := 2;
      DModG.QrUpdPID1.ExecSQL;
      QrConsigna.Next;
    end;
    QrConsigna.Close;
    *)
    QrSaldos.Close;
(*
    QrTotalSaldo.Close;
    QrTotalSaldo.Database := DModG.MyPID_DB;
    QrTotalSaldo.Close;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrTotalSaldo, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FSaldos,
    '']);
    //
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    MyObjects.frxDefineDataSets(frxSaldos, [
      DModG.frxDsDono,
      frxDsTotalSaldo
    ]);
    MyObjects.frxMostra(frxSaldos, 'Saldos');
  end else
    Geral.MB_Aviso('N�o foi poss�vel localizar o cliente interno: ' +
    FormatFloat('0', CliInt));
end;

procedure TDModFin.InserirContasSdo(Codigo, Entidade: Integer);
var
  Texto: String;
  Info: Integer;
  SdoIni, SdoAtu, SdoFut, PerAnt, PerAtu, ValMorto, ValIniOld: Double;
begin
  Entidade       := -11;
  SdoIni         := 0;
  SdoAtu         := 0;
  SdoFut         := 0;
  PerAnt         := 0;
  PerAtu         := 0;
  Texto          := '';
  Info           := 0;
  ValMorto       := 0;
  ValIniOld      := 0;
  //
  // Result :=
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contassdo', False, [
  'SdoIni', 'SdoAtu', 'SdoFut',
  'PerAnt', 'PerAtu', 'Texto',
  'Info', 'ValMorto', 'ValIniOld'], [
  'Codigo', 'Entidade'], [
  SdoIni, SdoAtu, SdoFut,
  PerAnt, PerAtu, Texto,
  Info, ValMorto, ValIniOld], [
  Codigo, Entidade], True);
end;

function TDModFin.ObtemDescricaoDeFatID(FatID: Integer): String;
var
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DmodFin);
  try
    Qry.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Nome ',
    'FROM ctafat0 ',
    'WHERE FatID=' + Geral.FF0(FatID),
    '']);
    //
    Result := Qry.FieldByName('Nome').AsString;
  finally
    Qry.Free;
  end;
end;

function TDModFin.ObtemGenCtbDeFatID(const FatID: Integer; var Genero: Integer;
  tpPagto: TTipoPagto; AvisaErro: Boolean): Boolean;
begin
  // Fazer????
  Genero := 0;
  Result := True;
end;

function TDModFin.ObtemGeneroDeFatID(const FatID: Integer; var Genero: Integer;
              tpPagto: TTipoPagto; AvisaErro: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Genero := 0;
  if MyObjects.FIC(FatID = 0, nil,
  'ERRO! "FatID" n�o definido para inclus�o de lan�amento!', AvisaErro) then
    Exit;
  Qry:= TmySQLQuery.Create(DmodFin);
  try
    Qry.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cf0.PlaGen, cta.Credito, cta.Debito ',
    'FROM ctafat0 cf0 ',
    'LEFT JOIN contas cta ON cta.Codigo=cf0.PlaGen ',
    'WHERE cf0.FatID=' + Geral.FF0(FatID),
    '']);
    Genero := Qry.FieldByName('PlaGen').AsInteger;
    //
    if MyObjects.FIC(Genero = 0, nil, 'ERRO! "FatID" n� ' + Geral.FF0(FatID) +
    ' sem conta definida para inclus�o de lan�amento!', AvisaErro) then
      Exit;
    if MyObjects.FIC((tpPagto = tpCred)
    and (Uppercase(Qry.FieldByName('Credito').AsString) <> 'V'), nil,
    'ERRO! "FatID" n� ' + Geral.FF0(FatID) +',  conta n� ' +
    Geral.FF0(Genero) + ' n�o � de cr�dito!', AvisaErro) then
      Exit;
    if MyObjects.FIC((tpPagto = tpDeb)
    and (Uppercase(Qry.FieldByName('Debito').AsString) <> 'V'), nil,
    'ERRO! "FatID" n� ' + Geral.FF0(FatID) +', conta n� ' +
    Geral.FF0(Genero) + ' n�o � de d�bito!', AvisaErro) then
      Exit;
    //
    Result := Genero <> 0;
  finally
    Qry.Free;
  end;
end;

function TDModFin.ObtemTipoDeCarteira(Carteira: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteira, Dmod.MyDB, [
  'SELECT Tipo ',
  'FROM carteiras ',
  'WHERE Codigo=' + Geral.FF0(Carteira),
  '']);
  //
  Result := QrCarteiraTipo.Value;
  if QrCarteira.RecordCount = 0 then
    Geral.MB_Erro('Carteira n�o definida em "TDModFin.ObtemTipoDeCarteira()"');
end;

function TDModFin.PagamtParcialDeDocumento(Valor, Credito, Debito: Double; CtrlIni,
  CtrlOri: Integer; Documento: Double; SerieCH, Duplicata: String;
  DataPag, Vencimento, DataDoc, Compensado: TDateTime;
  Descricao: String; QrCrt,
  QrLct: TmySQLQuery; RecalcSdoCart: Boolean; TabLctA: String;
  UsaOutrosFatDfs: Boolean; FatID, FatID_Sub: Integer; FatNum: Double;
  FatParcela, FatParcRef, FatGrupo, Nivel, Carteira, Sit, Tipo, Genero, NotaFiscal,
  Cliente, Fornecedor, ID_Pgto, Vendedor, Account, DescoPor, CliInt, ForneceI,
  Depto, CentroCusto: Integer;
  Mez: String;
  MoraDia, Multa, DescoVal, TaxasVal, MultaVal, MoraVal: Double;
  OldCarteira: Integer; Antigo: String = '';
  PermitePagarControleZero: Boolean = False): Boolean;
var
  Controle: Integer;
  Cred, Debi: Double;
  Data: String;
begin
  Cred := 0;
  Debi := 0;
{
  Nivel := FmCartPgto.FQrLct.FieldByName('Nivel').AsInteger + 1;
  if FmCartPgto.FQrLct.FieldByName('CtrlIni').AsInteger > 0 then
    CtrlIni := FmCartPgto.FQrLct.FieldByName('CtrlIni').AsInteger
  else CtrlIni := FmCartPgto.FQrLct.FieldByName('Controle').AsInteger;
  //
  Valor := Geral.DMV(EdValor.Text);
  if FmCartPgto.FQrLct.FieldByName('Credito').AsFloat > 0 then Credito := Valor else Credito := 0;
  if FmCartPgto.FQrLct.FieldByName('Debito').AsFloat > 0 then Debito := Valor else Debito := 0;
  if RGCarteira.ItemIndex < 0 then
  begin
    ShowMessage('Defina uma carteira!');
    RGCarteira.SetFocus;
    Exit;
  end;
  if CBCarteira.KeyValue <> NULL then Carteira := CBCarteira.KeyValue else
  begin
    ShowMessage('Defina um item de carteira!');
    EdCarteira.SetFocus;
    Exit;
  end;
  if Carteira < 1 then
  begin
    ShowMessage('Defina um item de carteira maior que zero!');
    EdCarteira.SetFocus;
    Exit;
  end;
  if (Valor = 0)  then
  begin
    ShowMessage('Defina um valor.');
    EdValor.SetFocus;
    Exit;
  end;
  Mes := FmCartPgto.FQrLct.FieldByName('MENSAL2').AsString;
  if (EdDoc.Enabled = False) then Doc := 0
  else Doc := Geral.DMV(EdDoc.Text);
  if (TPVencimento.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, TPData.Date)
  else Vencimento := FormatDateTime(VAR_FORMATDATE, TPVencimento.Date);
  if RGCarteira.ItemIndex = 2 then Sit := 0 else Sit := 3;
  //TipoAnt := StrToInt(EdTipoAnt.Caption);
  OldCarteira := EdOldCarteira.ValueVariant;
  //
}
  if Carteira = 0 then
  begin
    Geral.MB_Erro('Carteira n�o definida para a fun��o "TDModFin.PagamtParcialDeDocumento"!');
    Result := False;
    Exit;
  end;
  //
  Debi := Debito;
  Cred := Credito;
  Data := Geral.FDT(DataPag, 1);
  //
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_SerieCH        := SerieCH;
  FLAN_Documento      := Documento;
  FLAN_Data           := Data;
  FLAN_Tipo           := Tipo;
  FLAN_Carteira       := Carteira;
  FLAN_Credito        := Cred;
  FLAN_Debito         := Debi;
  FLAN_Genero         := Genero;
  FLAN_NotaFiscal     := NotaFiscal;
  FLAN_Vencimento     := Geral.FDT(Vencimento, 1);
  FLAN_Mez            := Geral.IMV('0' + Mez);
  FLAN_Descricao      := Descricao;
  FLAN_Sit            := Sit;
  FLAN_Cliente        := Cliente;
  FLAN_Fornecedor     := Fornecedor;
  FLAN_ID_Pgto        := ID_Pgto;
  FLAN_DataCad        := Geral.FDT(Date, 1);
  FLAN_UserCad        := VAR_USUARIO;
  FLAN_DataDoc        := Geral.FDT(DataDoc, 1);
  FLAN_MoraDia        := MoraDia;
  FLAN_Multa          := Multa;
  FLAN_CtrlIni        := CtrlIni;
  FLAN_Nivel          := Nivel;
  FLAN_Vendedor       := Vendedor;
  FLAN_Account        := Account;
  FLAN_CentroCusto    := CentroCusto;
  FLAN_DescoPor       := DescoPor;
  FLAN_DescoVal       := DescoVal;
  FLAN_TaxasVal       := TaxasVal;
  FLAN_MultaVal       := MultaVal;
  FLAN_MoraVal        := MoraVal;
  FLAN_CliInt         := CliInt;
  FLAN_ForneceI       := ForneceI;
  FLAN_Depto          := Depto;
  FLAN_Compensado     := Geral.FDT(Compensado, 1);
  FLAN_FatID          := FatID;
  FLAN_FatID_Sub      := FatID_Sub;
  FLAN_FatNum         := FatNum;
  FLAN_FatParcela     := FatParcela;
  FLAN_FatParcRef     := FatParcRef;
  FLAN_FatGrupo       := FatGrupo;
  FLAN_Duplicata      := Duplicata;
  FLAN_Antigo         := Antigo;
  //
  Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle', TabLctA, VAR_LCT, 'Controle');
  FLAN_Controle   := Controle;
  VAR_LANCTO2     := Controle;
  VAR_DATAINSERIR := DataPag;
  //
  if UFinanceiro.InsereLancamento(TabLctA) then
  begin
    Result := True;
    if Carteira <> 0 then
      UFinanceiro.RecalcSaldoCarteira(Carteira, nil, nil, False, False);
    if OldCarteira <> 0 then
      UFinanceiro.RecalcSaldoCarteira(OldCarteira,  nil, nil, False, False);
  end else
    Result := False;
end;

procedure TDModFin.QrCreditosCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrCreditosSubPgto1.Value = 0 then
    SubPgto1 := ''
  else
    SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';
  //
  QrCreditosNOMECON_2.Value := QrCreditosNOMECON.Value + SubPgto1;
  //
  QrCreditosMES.Value := dmkPF.MezToMesEAno(QrCreditosMez.Value);
end;

procedure TDModFin.QrDebitosCalcFields(DataSet: TDataSet);
begin
  QrDebitosMES.Value := dmkPF.MezToMesEAno(QrDebitosMez.Value);
  QrDebitosMES2.Value := dmkPF.MezToMesEAnoCurto(QrDebitosMez.Value);
  //
  QrDebitosSERIE_DOC.Value := Trim(QrDebitosSerieCH.Value);
  if QrDebitosDocumento.Value <> 0 then QrDebitosSERIE_DOC.Value :=
    QrDebitosSERIE_DOC.Value + FormatFloat('000000', QrDebitosDocumento.Value);
  //
  QrDebitosNF_TXT.Value := FormatFloat('000000;-000000; ', QrDebitosNotaFiscal.Value);
  //
end;

procedure TDModFin.QrDuplCHCalcFields(DataSet: TDataSet);
begin
  QrDuplCHTERCEIRO.Value := QrDuplCHNOMEFNC.Value + QrDuplCHNOMECLI.Value;
  QrDuplCHMES.Value := dmkPF.MezToFDT(QrDuplCHMez.Value, 0, 104);
  QrDuplCHCHEQUE.Value := Trim(QrDuplCHSerieCH.Value) +
    FormatFloat('000000', QrDuplCHDocumento.Value);
  if QrDuplCHCompensado.Value = 0 then QrDuplCHCOMP_TXT.Value := '' else
    QrDuplCHCOMP_TXT.Value := Geral.FDT(QrDuplCHCompensado.Value, 2);
end;

procedure TDModFin.QrDuplEntValDataCalcFields(DataSet: TDataSet);
begin
  QrDuplEntValDataTERCEIRO.Value := QrDuplEntValDataNOMEFNC.Value +
                                    QrDuplEntValDataNOMECLI.Value;
  QrDuplEntValDataMES.Value      := dmkPF.MezToFDT(QrDuplEntValDataMez.Value, 0, 104);
  QrDuplEntValDataCHEQUE.Value   := Trim(QrDuplEntValDataSerieCH.Value) +
                                    FormatFloat('000000', QrDuplEntValDataDocumento.Value);
  //
  if QrDuplEntValDataCompensado.Value = 0 then
    QrDuplEntValDataCOMP_TXT.Value := ''
  else
    QrDuplEntValDataCOMP_TXT.Value := Geral.FDT(QrDuplEntValDataCompensado.Value, 2);
end;

procedure TDModFin.QrDuplNFCalcFields(DataSet: TDataSet);
begin
  QrDuplNFTERCEIRO.Value := QrDuplNFNOMEFNC.Value + QrDuplNFNOMECLI.Value;
  QrDuplNFMES.Value := dmkPF.MezToFDT(QrDuplNFMez.Value, 0, 104);
  QrDuplNFCHEQUE.Value := Trim(QrDuplNFSerieCH.Value) +
    FormatFloat('000000', QrDuplNFDocumento.Value);
  if QrDuplNFCompensado.Value = 0 then QrDuplNFCOMP_TXT.Value := '' else
    QrDuplNFCOMP_TXT.Value := Geral.FDT(QrDuplNFCompensado.Value, 2);
end;

procedure TDModFin.QrDuplValCalcFields(DataSet: TDataSet);
begin
  QrDuplValTERCEIRO.Value := QrDuplValNOMEFNC.Value + QrDuplValNOMECLI.Value;
  QrDuplValMES.Value := dmkPF.MezToFDT(QrDuplValMez.Value, 0, 104);
  QrDuplValCHEQUE.Value := Trim(QrDuplValSerieCH.Value) +
    FormatFloat('000000', QrDuplValDocumento.Value);
  if QrDuplValCompensado.Value = 0 then QrDuplValCOMP_TXT.Value := '' else
    QrDuplValCOMP_TXT.Value := Geral.FDT(QrDuplValCompensado.Value, 2);
end;

procedure TDModFin.QrPendGCalcFields(DataSet: TDataSet);
begin
  if QrPendGMez.Value > 0 then QrPendGNOMEMEZ.Value :=
    dmkPF.MezToFDT(QrPendGMez.Value, 0, 104)
  else QrPendGNOMEMEZ.Value := '';
end;

procedure TDModFin.QrPgBloqCalcFields(DataSet: TDataSet);
begin
  //QrPgBloqMES.Value := dmkPF.MezToMesEAno(QrPgBloqMez.Value);
end;

procedure TDModFin.QrRepAbeCalcFields(DataSet: TDataSet);
begin
  QrRepAbeCompensado_TXT.Value := dmkPF.FDT_NULO(QrRepAbeCompensado.Value, 12);
end;

procedure TDModFin.QrRepAfterScroll(DataSet: TDataSet);
  procedure GeraParteSQL_RepLan(TabLct, Reparcel: String;
    InsereOrderBy: Boolean = False);
  begin
    QrReplan.SQL.Add('SELECT Reparcel, FatNum, SUM(Credito) Credito, Vencimento');
    QrReplan.SQL.Add('FROM ' + TabLct);
    QrReplan.SQL.Add('WHERE Reparcel=' + Reparcel);
    QrReplan.SQL.Add('GROUP BY FatNum');

    if InsereOrderBy then
      QrReplan.SQL.Add('ORDER BY Vencimento');
  end;
var
  RepCodTxt, TabLctA, TabLctB, TabLctD: String;
begin
  // testar no 244
  QrRepBPP.Close;
  QrRepori.Close;
  QrReplan.Close;
  //
  if QrRepCodigo.Value <> 0 then
  begin
    RepCodTxt := FormatFloat('0', QrRepCodigo.Value);
    TabLctA   := DModG.NomeTab(TMeuDB, ntLct, False, ttA, QrRepCodCliEsp.Value);
    TabLctB   := DModG.NomeTab(TMeuDB, ntLct, False, ttB, QrRepCodCliEsp.Value);
    TabLctD   := DModG.NomeTab(TMeuDB, ntLct, False, ttD, QrRepCodCliEsp.Value);
    //
    QrRepBPP.Close;
    QrRepBPP.Params[0].AsInteger := QrRepCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrRepBPP, Dmod.MyDB);
    //
    QrRepori.Close;
    QrRepori.Params[0].AsInteger := QrRepCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrRepori, Dmod.MyDB);
    //
    QrReplan.Close;
    QrReplan.SQL.Clear;
    //
    QrRepLan.SQL.Add('DROP TABLE IF EXISTS _MOD_FIN_REPLAN_1_;');
    QrRepLan.SQL.Add('CREATE TABLE _MOD_FIN_REPLAN_1_ IGNORE ');
    QrRepLan.SQL.Add('');
    GeraParteSQL_RepLan(TabLctA, RepCodTxt);
    QrRepLan.SQL.Add('UNION');
    GeraParteSQL_RepLan(TabLctB, RepCodTxt);
    QrRepLan.SQL.Add('UNION');
    GeraParteSQL_RepLan(TabLctD, RepCodTxt, True);
    QrRepLan.SQL.Add(';');
    QrRepLan.SQL.Add('');
    (* Mudado em 20/05/2015 => Ini
    QrRepLan.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
    *)
    QrRepLan.SQL.Add('SELECT Reparcel, FatNum, Credito, Vencimento ');
    //Mudado em 20/05/2015 => Fim
    QrRepLan.SQL.Add('FROM _MOD_FIN_REPLAN_1_;');
    QrRepLan.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrRepLan, DmodG.MyPID_DB);
  end;
end;

procedure TDModFin.QrResumoCalcFields(DataSet: TDataSet);
begin
  QrResumoSALDO.Value := QrResumoCredito.Value + QrResumoDebito.Value;
  QrResumoFINAL.Value := QrResumoSALDO.Value + QrSaldoATOTAL.Value;
end;

procedure TDModFin.QrSaldoACalcFields(DataSet: TDataSet);
begin
  QrSaldoATOTAL.Value := QrSaldoAInicial.Value + QrSaldoASALDO.Value;
end;

procedure TDModFin.QrSdoCtasCalcFields(DataSet: TDataSet);
begin
  QrSdoCtasSDO_INI.Value := QrSdoCtasSDO_CAD.Value + QrSdoCtasSDO_ANT.Value;
  QrSdoCtasSDO_FIM.Value := QrSdoCtasSDO_INI.Value +
                            QrSdoCtasMOV_CRE.Value - QrSdoCtasMOV_DEB.Value +
                            QrSdoCtasTRF_CRE.Value - QrSdoCtasTRF_DEB.Value;
end;

procedure TDModFin.QrSdoExclCalcFields(DataSet: TDataSet);
begin
  QrSdoExclSDO_INI.Value := QrSdoExclSDO_CAD.Value + QrSdoExclSDO_ANT.Value;
  QrSdoExclSDO_FIM.Value := QrSdoExclSDO_INI.Value +
                            QrSdoExclMOV_CRE.Value - QrSdoExclMOV_DEB.Value +
                            QrSdoExclTRF_CRE.Value - QrSdoExclTRF_DEB.Value;
end;

procedure TDModFin.QrSNGCalcFields(DataSet: TDataSet);
begin
  QrSNGCODIGOS_TXT.Value := FormatFloat('0', QrSNGCodPla.Value);
  if QrSNGNivel.Value < 5 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('00', QrSNGCodCjt.Value);
  if QrSNGNivel.Value < 4 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('000', QrSNGCodGru.Value);
  if QrSNGNivel.Value < 3 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('0000', QrSNGCodSgr.Value);
  if QrSNGNivel.Value < 2 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('00000', QrSNGGenero.Value);
  QrSNGCODIGOS_TXT.Value := QrSNGCODIGOS_TXT.Value + ' - ' + QrSNGNomeGe.Value;
end;

procedure TDModFin.QrTotalSaldoCalcFields(DataSet: TDataSet);
begin
  if QrTotalSaldoTipo.Value = 1 then
       QrTotalSaldoNOMETIPO.Value := 'Em carteira'
  else QrTotalSaldoNOMETIPO.Value := 'Consignado';
end;

function TDModFin.QuitacaoTotalDeDocumento(Valor: Double; CtrlIni, CtrlOri: Integer;
  Documento: Double; Duplicata: String; DataQuit: TDateTime; Descricao: String;
  TaxasVal, MultaVal, MoraVal, DescoVal: Double;
  QrCrt, QrLct: TmySQLQuery; RecalcSdoCart: Boolean;
  TabLctA: String; UsaOutrosFatDfs: Boolean;
  FatID, FatID_Sub: Integer; FatNum: Double; FatParcela, FatGrupo: Integer;
  Antigo: String = '';
  PermiteQuitarControleZero: Boolean = False;
  OutraCartCompensa: Integer = 0): Boolean;
var
  Texto: PChar;
  //Doc,
  Controle, Controle2, Credito, Debito: Double;
  Sub, Carteira: Integer;
  Data: String;
begin
  Result := False;
  //
  Data := Geral.FDT(DataQuit, 1);
  if QrLancto1Debito.Value > 0 then
  begin
    Debito := Valor;
    Credito := 0;
  end else begin
    Debito := 0;
    Credito := Valor;
  end;
  FLAN_Nivel := QrLancto1Nivel.Value + 1;
  if CtrlIni = -1000 then
    Geral.MB_Aviso('"CtrlIni" n�o definido! Ser� usado o "Controle"');
  if CtrlIni > 0 then
    FLAN_CtrlIni := Trunc(CtrlIni)
  else
    FLAN_CtrlIni := Trunc(CtrlOri);
  //
  Controle := QrLancto1Controle.Value;
  Sub := QrLancto1Sub.Value;
  //Documento := Geral.DMV(EdDoc.Text);
  //
  if (Controle <> 0) and (QrLancto2.RecordCount <> 0) then
  begin
    if ((QrLancto2Credito.Value + QrLancto2Debito.Value) =
      (QrLancto1Credito.Value + QrLancto1Debito.Value))
    and (QrLancto2.RecordCount = 1) and (QrLancto2Tipo.Value = 1) then
    begin
      Texto := PChar('J� existe uma quita��o, e ela possui o mesmo valor,'+
      ' deseja difini-la como quita��o desta emiss�o ?');
      if Geral.MB_Pergunta(Texto) = ID_YES then
      begin
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
        Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
        Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Tipo=2');
        Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
        Dmod.QrUpdM.Params[3].AsFloat := Controle;
//        Dmod.QrUpdM.Params[2].AsInteger := Sub; precisa??
        Dmod.QrUpdM.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Sit', 'Compensado'], ['Controle', 'Tipo'], [
        3, Data], [Controle, 2], True, '', TabLctA);
        //
        if RecalcSdoCart then
        begin
          if OutraCartCompensa = 0 then
            UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value, QrCrt, QrLct, True, True)
          else
            UFinanceiro.RecalcSaldoCarteira(OutraCartCompensa, QrCrt, QrLct, True, True);
        end;
        //
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Data=:P0,');
        Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P3 AND Tipo=1 AND ID_Pgto>0');
        Dmod.QrUpdM.Params[0].AsString := Data;
        Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
        Dmod.QrUpdM.Params[3].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
        }
        if Controle > 0 then
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Data'], ['ID_Pgto', 'Tipo'], [
          Data], [Controle, 1], True, '', TabLctA);
        end;
        if RecalcSdoCart then
        begin
          if OutraCartCompensa = 0 then
             UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value, QrCrt, QrLct, True, True)
          else
             UFinanceiro.RecalcSaldoCarteira(OutraCartCompensa, QrCrt, QrLct, True, True);
        end;
      end;
    end else
    begin
      Geral.MB_Aviso('Quita��o cancelada! ' + sLineBreak +
        'J� existe pagamento(s) parcial(is)/rolagem(ns) para esta emiss�o!');
{ T O D O : Fazer relat�rio para aqui e LctPgVarios }
    end;
  end else
  begin
    if OutraCartCompensa = 0 then
      Carteira := QrLancto1CARTEIRABANCO.Value
    else
      Carteira := OutraCartCompensa;
    //
    if Carteira = 0 then
    begin
      Geral.MB_Erro('Carteira n�o definida para a fun��o "TDModFin.QuitacaoTotalDeDocumento"!');
      Result := False;
      Exit;
    end;
    //
    Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      TabLctA, LAN_CTOS, 'Controle');
    if (Controle <> 0) then
    begin
      //
      {
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, ');
      Dmod.QrUpdM.SQL.Add('Compensado=:P0, Documento=:P1, Descricao=:P2, ');
      Dmod.QrUpdM.SQL.Add('ID_Quit=:P3, DataAlt=:Pa, UserAlt=:Pb');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pc AND Sub=:Pd AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := Data;
      Dmod.QrUpdM.Params[01].AsFloat   := Documento;
      Dmod.QrUpdM.Params[02].AsString  := EdDescricao.Text;
      Dmod.QrUpdM.Params[03].AsFloat   := Controle2;
      //
      Dmod.QrUpdM.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[05].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[06].AsFloat   := Controle;
      Dmod.QrUpdM.Params[07].AsInteger := Sub;
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Sit', 'Compensado', 'Documento', 'Descricao', 'ID_Quit'], [
      'Controle', 'Sub', 'Tipo'], [
      3, Data, Documento, Descricao, Controle2], [
      Controle, Sub, 2], True, '', TabLctA);
      //
      if RecalcSdoCart then
      begin
        UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value, QrCrt, QrLct, True, True);
      end;
    end;
    //
    UFinanceiro.LancamentoDefaultVARS;
    FLAN_Data        := Data;
    FLAN_Vencimento  := Geral.FDT(QrLancto1Vencimento.Value, 1);
    FLAN_DataCad     := Geral.FDT(Date, 1);
    FLAN_Mez         := QrLancto1Mez.Value;
    FLAN_Descricao   := Descricao;
    //
    FLAN_Compensado  := Data;
    //
    FLAN_Duplicata   := QrLancto1Duplicata.Value;
    FLAN_Doc2        := QrLancto1Doc2.Value;
    FLAN_SerieCH     := QrLancto1SerieCH.Value;

    FLAN_Documento   := Trunc(QrLancto1Documento.Value);
    FLAN_Tipo        := 1;
    FLAN_Carteira    := Carteira;
    FLAN_Credito     := Credito;
    FLAN_Debito      := Debito;
    FLAN_Genero      := QrLancto1Genero.Value;
    FLAN_NotaFiscal  := QrLancto1NotaFiscal.Value;
    FLAN_Sit         := 3;
    FLAN_Cartao      := 0;
    FLAN_Linha       := 0;
    FLAN_Fornecedor  := QrLancto1Fornecedor.Value;
    FLAN_Cliente     := QrLancto1Cliente.Value;
    FLAN_MoraDia     := QrLancto1MoraDia.Value;
    FLAN_Multa       := QrLancto1Multa.Value;
    //FLAN_UserCad     := VAR_USUARIO;
    //FLAN_DataDoc     := Geral.FDT(Date, 1);
    FLAN_Vendedor    := QrLancto1Vendedor.Value;
    FLAN_Account     := QrLancto1Account.Value;
    FLAN_CentroCusto := QrLancto1CentroCusto.Value;
    FLAN_ICMS_P      := QrLancto1ICMS_P.Value;
    FLAN_ICMS_V      := QrLancto1ICMS_V.Value;
    FLAN_CliInt      := QrLancto1CliInt.Value;
    FLAN_Depto       := QrLancto1Depto.Value;
    FLAN_DescoPor    := QrLancto1DescoPor.Value;
    FLAN_ForneceI    := QrLancto1ForneceI.Value;
    FLAN_DescoVal    := QrLancto1DescoVal.Value;
    FLAN_NFVal       := QrLancto1NFVal.Value;
    FLAN_Unidade     := QrLancto1Unidade.Value;
    FLAN_Qtde        := QrLancto1Qtde.Value;
    FLAN_Qtd2        := QrLancto1Qtd2.Value;
    FLAN_VctoOriginal := Geral.FDT(QrLancto1VctoOriginal.Value, 1);
    FLAN_ModeloNF     := QrLancto1ModeloNF.Value;
    //
    if UsaOutrosFatDfs then
    begin
      FLAN_FatID      := FatID;
      FLAN_FatID_Sub  := FatID_Sub;
      FLAN_FatNum     := FatNum;
      FLAN_FatParcela := FatParcela;
      FLAN_FatParcRef := FatParcela;
      FLAN_FatGrupo   := FatGrupo;
    end else
    begin
      FLAN_FatID      := QrLancto1FatID.Value;
      FLAN_FatID_Sub  := QrLancto1FatID_Sub.Value;
      FLAN_FatNum     := Trunc(QrLancto1FatNum.Value);
      FLAN_FatParcela := QrLancto1FatParcela.Value;
      FLAN_FatParcRef := 0; //??
      FLAN_FatGrupo   := 0;
    end;
    FLAN_TaxasVal := TaxasVal;
    FLAN_MultaVal := MultaVal;
    FLAN_MoraVal  := MoraVal;
    //
    FLAN_DescoVal   := DescoVal;
    FLAN_ID_Pgto    := Round(Controle);
    FLAN_Controle   := Round(Controle2);
    FLAN_Antigo     := Antigo;
    //
    Result := UFinanceiro.InsereLancamento(TabLctA);
    //incluir na tabela de LctStep
    DmodFin.FolowStepLct(FLAN_ID_Pgto, FLAN_Controle, CO_STEP_LCT_INSPGC);
    //
    if OutraCartCompensa = 0 then
      UFinanceiro.RecalcSaldoCarteira(QrLancto1CARTEIRABANCO.Value, nil, nil, True, False)
    else
      UFinanceiro.RecalcSaldoCarteira(OutraCartCompensa, nil, nil, True, False);
    //
    if (Controle <> 0) and (RecalcSdoCart) then
    begin
      UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value, QrCrt, QrLct, True, True);
    end;
  end;
end;

function TDModFin.QuitaOuPgPartDeDocumento(Valor: Double; CtrlIni, CtrlOri: Integer;
  Documento: Double; Duplicata: String; DataQuit: TDateTime;
  Descricao: String; TaxasVal, MultaVal, MoraVal, DescoVal: Double;
  QrCrt, QrLct: TmySQLQuery; RecalcSdoCart: Boolean;
  TabLctA: String; UsaOutrosFatDfs: Boolean; FatID, FatID_Sub:
  Integer; FatNum: Double; FatParcela, FatParcRef, FatGrupo: Integer;
  ValPende: Double; OutraCartCompensa: Integer; Antigo: String = '';
  PermiteQuitarControleZero: Boolean = False): Boolean;
var
  Credito, Debito, MoraDia, Multa: Double;
  DataPag, Vencimento, DataDoc, Compensado: TDateTime;
  Nivel, OldCarteira, Tipo, Carteira, NotaFiscal, Cliente, Fornecedor, Genero,
  ID_Pgto, Vendedor, Account, CentroCusto, DescoPor, CliInt, ForneceI, Depto,
  Sit: Integer;
  SerieCH, Mez: String;
begin
 //
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqLct1, Dmod.MyDB, [
  'SELECT COUNT(lct.Controle) Pagamentos ',
  'FROM ' + TabLctA + ' lct ',
  'WHERE lct.ID_Pgto=' + Geral.FF0(CtrlOri),
  '']);
  //
  // Se ir� ficar pend�ncia...
  if (ValPende >= 0.01)
  // ... ou se j� teve algum pagamento
  or (QrPsqLct1Pagamentos.Value > 0) then
  begin
    if (PermiteQuitarControleZero = False) or (QrLct <> nil) then
    begin
      if QrLct = nil then
      begin
        Geral.MB_Erro('Inclus�o de pagamento autom�tico cancelado!' + sLineBreak +
        '"QrLct" indefinido!');
        //
        Exit;
      end;
      if (QrLct.RecordCount <> 1) and (PermiteQuitarControleZero = False) then
      begin
        Geral.MB_Erro('Inclus�o de pagamento autom�tico cancelado!' + sLineBreak +
        '"QrLct" n�o pode ter mais ou menos que 1 (um) registro!');
        //
        Exit;
      end;
      OldCarteira    := QrLct.FieldByName('Carteira').AsInteger;
      Genero         := QrLct.FieldByName('Genero').AsInteger;
      Nivel          := QrLct.FieldByName('Nivel').AsInteger + 1;
      Descricao      := QrLct.FieldByName('Descricao').AsString;
      Cliente        := QrLct.FieldByName('Cliente').AsInteger;
      Fornecedor     := QrLct.FieldByName('Fornecedor').AsInteger;
      Vendedor       := QrLct.FieldByName('Vendedor').AsInteger;
      Account        := QrLct.FieldByName('Account').AsInteger;
      CentroCusto    := QrLct.FieldByName('CentroCusto').AsInteger;
      CliInt         := QrLct.FieldByName('CliInt').AsInteger;
      ForneceI       := QrLct.FieldByName('ForneceI').AsInteger;
      Depto          := QrLct.FieldByName('Depto').AsInteger;
      Mez            := dmkPF.ITS_Null(QrLct.FieldByName('Mez').AsInteger);
      //
      //Pode usar outros FatID, FatID_Sub, FatNum e FatParcela
      if not UsaOutrosFatDfs then
      begin
        FatID          := QrLct.FieldByName('FatID').AsInteger;
        FatID_Sub      := QrLct.FieldByName('FatID_Sub').AsInteger;
        FatNum         := QrLct.FieldByName('FatNum').AsInteger;
        FatParcela     := QrLct.FieldByName('FatParcela').AsInteger;
      end;
    end else
    begin
      OldCarteira    := 0;
      Genero         := 0;
      Nivel          := 0;
      Descricao      := '';
      Cliente        := 0;
      Fornecedor     := 0;
      Vendedor       := 0;
      Account        := 0;
      CentroCusto    := 0;
      CliInt         := 0;
      ForneceI       := 0;
      Depto          := 0;
      Mez            := '';

    end;
    //
    if Valor > 0 then
    begin
      Credito := Valor;
      Debito := 0;
    end else begin
      Credito := 0;
      Debito := - Valor;
    end;
    DataPag := DataQuit;
    Vencimento := DataQuit;
    DataDoc := DataQuit;
    Compensado := DataQuit;
      SerieCH        := '';
      NotaFiscal     := 0;
      ID_Pgto        := CtrlOri;
      MoraDia        := 0;
      Multa          := 0;
      DescoPor       := 0;
    //
    // Somente carteiras positivas!
    if OutraCartCompensa > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCrt1, Dmod.MyDB, [
      'SELECT Codigo, Tipo ',
      'FROM Carteiras ',
      'WHERE Codigo=' + Geral.FF0(OutraCartCompensa),
      '']);
    end else begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCrt1, Dmod.MyDB, [
      'SELECT Codigo, Tipo ',
      'FROM Carteiras ',
      'WHERE Codigo = ( ',
      '     SELECT Banco ',
      '     FROM carteiras ',
      '     WHERE Codigo=' + Geral.FF0(OldCarteira) + ') ',
      '']);
    end;
    //
    Tipo           := QrPsqCrt1Tipo.Value;
    Carteira       := QrPsqCrt1Codigo.Value;
    Sit            := 2;
    //
    if (QrPsqCrt1Tipo.Value <> 1) and (PermiteQuitarControleZero = False) then
    begin
      Geral.MB_Aviso('Inclus�o de pagamento autom�tico cancelado!' + sLineBreak +
      'A carteira ' + Geral.FF0(Carteira) +
      ' n�o � uma carteira banc�ria v�lida!');
      //
      Exit;
    end;
    //
    Result := PagamtParcialDeDocumento(Valor, Credito, Debito, CtrlIni,
    CtrlOri, Documento, SerieCH, Duplicata,
    DataPag, Vencimento, DataDoc, Compensado,
    Descricao, QrCrt, QrLct, RecalcSdoCart, TabLctA,
    UsaOutrosFatDfs, FatID, FatID_Sub, FatNum,
    FatParcela, FatParcRef, FatGrupo, Nivel, Carteira, Sit, Tipo, Genero,
    NotaFiscal, Cliente, Fornecedor, ID_Pgto, Vendedor, Account,
    DescoPor, CliInt, ForneceI, Depto, CentroCusto,
    Mez, MoraDia, Multa, DescoVal, TaxasVal, MultaVal, MoraVal,
    OldCarteira, Antigo, PermiteQuitarControleZero);
    //
  end else
  begin
    Result := QuitacaoTotalDeDocumento(Valor, CtrlIni, CtrlOri,
      Documento, Duplicata, DataQuit,
      Descricao, TaxasVal, MultaVal, MoraVal, DescoVal,
      QrCrt, QrLct, RecalcSdoCart,
      TabLctA, UsaOutrosFatDfs, FatID, FatID_Sub,
      FatNum, FatParcela, FatGrupo, Antigo, PermiteQuitarControleZero,
      OutraCartCompensa);
  end;
end;

function TDModFin.ReopenCliInt(CliInt: Integer): Boolean;
begin
  QrCliInt.Close;
  if Trim(VAR_NOME_TAB_CLIINT) <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCliInt, Dmod.MyDB, [
      'SELECT PBB ',
      'FROM ' + LowerCase(VAR_NOME_TAB_CLIINT),
      'WHERE Codigo=' + FormatFloat('0', CliInt),
      '']);
    //
    Result := QrCliInt.RecordCount > 0;
  end else Result := False;
end;

procedure TDModFin.ReopenContasEnt(Conta, CodEnti: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContasEnt, Dmod.MyDB, [
    'SELECT * ',
    'FROM contasent ',
    'WHERE Codigo=' + Geral.FF0(Conta),
    'AND Entidade=' + Geral.FF0(CodEnti),
    '']);
end;

procedure TDModFin.ReopenCreditosEDebitos(CliInt: Integer; DataI, DataF: String;
  Acordos, Periodos, Textos, NaoAgruparNada: Boolean; TabLct: String);
begin
  //// C R � D I T O S
{
SELECT lct.Mez, SUM(lct.Credito) Credito,
lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,
lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero,
lct.SubPgto1,
con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU
FROM lct lct
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
LEFT JOIN contas    con ON con.Codigo=lct.Genero
LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo
LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo
WHERE lct.Tipo <> 2
AND lct.Credito > 0
AND lct.Genero>0
AND car.ForneceI = :P0
AND lct.Data BETWEEN :P1 AND :P2
GROUP BY lct.Genero, lct.Mez
/*, lct.SubPgto1*/
ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,
NOMESGR, con.OrdemLista, NOMECON, Mez, Data

}
  QrCreditos.Close;
  QrCreditos.SQL.Clear;
  if NaoAgruparNada = True then
    QrCreditos.SQL.Add('SELECT lct.Mez, lct.Credito, ')
  else
    QrCreditos.SQL.Add('SELECT lct.Mez, SUM(lct.Credito) Credito, ');
  QrCreditos.SQL.Add('lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,');
  QrCreditos.SQL.Add('lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, ');
  QrCreditos.SQL.Add('lct.SubPgto1, ');
  QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU');
  QrCreditos.SQL.Add('FROM ' + TabLct + ' lct');
  QrCreditos.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrCreditos.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lct.Genero');
  QrCreditos.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
  QrCreditos.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
  QrCreditos.SQL.Add('WHERE lct.Tipo <> 2');
  QrCreditos.SQL.Add('AND lct.Credito <> 0');
  QrCreditos.SQL.Add('AND lct.Genero>0');
  // 2011-12-31
  QrCreditos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
  // Fim 2011-12-31
  QrCreditos.SQL.Add('AND car.ForneceI = :P0');
  QrCreditos.SQL.Add('AND lct.Data BETWEEN :P1 AND :P2');
  if NaoAgruparNada = False then
  begin
    QrCreditos.SQL.Add('GROUP BY lct.Genero ');
    if Periodos then
      QrCreditos.SQL.Add(', lct.Mez');
    if Acordos then
      QrCreditos.SQL.Add(', lct.SubPgto1');
  end;
  QrCreditos.SQL.Add('ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,');
  QrCreditos.SQL.Add('NOMESGR, con.OrdemLista, NOMECON, Mez, Data');
  QrCreditos.SQL.Add('');
  //
  QrCreditos.Params[00].AsInteger := CliInt;
  QrCreditos.Params[01].AsString  := DataI;
  QrCreditos.Params[02].AsString  := DataF;
  UMyMod.AbreQuery(QrCreditos, Dmod.MyDB, 'TDmodFin.ReopenCreditosEDebitos()');
  //


  //// D � B I T O S
  QrDebitos.Close;
{
SELECT lct.Controle, lct.Sub, lct.Carteira, lct.Cartao,
lct.Vencimento, lct.Sit, lct.Genero, lct.Tipo,
IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",
DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,
IF(COUNT(lct.Data)>1, "V�rias", DATE_FORMAT(lct.Data, "%d/%m/%y")) DATA,
lct.Descricao, SUM(lct.Debito) DEBITO,
IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,
IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,
IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,
IF(COUNT(lct.Data)>1, 0, lct.Mez) MEZ, lct.Compensado,
con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,

COUNT(lct.Data) ITENS

FROM lct lct
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
LEFT JOIN contas    con ON con.Codigo=lct.Genero
LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo
LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo
WHERE lct.Tipo <> 2
AND lct.Debito > 0
AND lct.Genero>0
AND car.ForneceI = :P0
AND lct.Data BETWEEN :P1 AND :P2
GROUP BY lct.Descricao
ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,
NOMESGR, con.OrdemLista, NOMECON, Data

}
  QrDebitos.SQL.Clear;
  if Textos and (NaoAgruparNada = False) then
  begin
    QrDebitos.SQL.Add('SELECT IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",');
    QrDebitos.SQL.Add('DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,');
    QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "V�rias", DATE_FORMAT(lct.Data, "%d/%m/%y")) DATA,');
    QrDebitos.SQL.Add('lct.Descricao, SUM(lct.Debito) DEBITO,');
    QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,');
    QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,');
    QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,');
    QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Mez) MEZ, lct.Compensado,');
    QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrDebitos.SQL.Add('COUNT(lct.Data) ITENS, ');
  end else begin
    QrDebitos.SQL.Add('SELECT IF(lct.Compensado<2,"", DATE_FORMAT(');
    QrDebitos.SQL.Add('lct.Compensado, "%d/%m/%y")) COMPENSADO_TXT,');
    QrDebitos.SQL.Add('DATE_FORMAT(lct.Data, "%d/%m/%y") DATA, ');
    QrDebitos.SQL.Add('lct.Descricao, lct.Debito, ');
    QrDebitos.SQL.Add('IF(lct.NotaFiscal = 0, 0, lct.NotaFiscal) NOTAFISCAL,');
    QrDebitos.SQL.Add('IF(lct.SerieCH = "", "", lct.SerieCH) SERIECH,');
    QrDebitos.SQL.Add('IF(lct.Documento = 0, 0, lct.Documento) DOCUMENTO,');
    QrDebitos.SQL.Add('IF(lct.Mez = 0, 0, lct.Mez) MEZ, lct.Compensado,');
    QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrDebitos.SQL.Add('0 ITENS, ');
  end;
  QrDebitos.SQL.Add('lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, ');
  QrDebitos.SQL.Add('lct.Tipo, lct.Controle, lct.Sub, lct.Carteira, lct.Cartao');
  QrDebitos.SQL.Add('FROM ' + TabLct + ' lct');
  QrDebitos.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrDebitos.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lct.Genero');
  QrDebitos.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
  QrDebitos.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
  QrDebitos.SQL.Add('WHERE lct.Tipo <> 2');
  QrDebitos.SQL.Add('AND lct.Debito > 0');
  QrDebitos.SQL.Add('AND lct.Genero>0');
  // 2011-12-31
  QrDebitos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
  // Fim 2011-12-31
  QrDebitos.SQL.Add('AND car.ForneceI = :P0');
  QrDebitos.SQL.Add('AND lct.Data BETWEEN :P1 AND :P2');

  if NaoAgruparNada = False then
  begin
    if Textos then
      QrDebitos.SQL.Add('GROUP BY lct.Descricao');
  end;
  QrDebitos.SQL.Add('ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,');
  QrDebitos.SQL.Add('NOMESGR, con.OrdemLista, NOMECON, Compensado, Descricao');
  QrDebitos.SQL.Add('');
  QrDebitos.SQL.Add('');
  QrDebitos.Params[00].AsInteger := CliInt;
  QrDebitos.Params[01].AsString  := DataI;
  QrDebitos.Params[02].AsString  := DataF;
  UMyMod.AbreQuery(QrDebitos, Dmod.MyDB, 'TDmodFin.ReopenCreditosEDebitos()');
end;

procedure TDModFin.ReopenNivelSel(QrNivelSel: TmySQLQuery;
  EdGenPlaCta: TdmkEditCB; CBGenPlaCta: TdmkDBLookupComboBox;
  RGNivPlaCta: TRadioGroup);
var
  Nivel: String;
begin
  EdGenPlaCta.ValueVariant := 0;
  CBGenPlaCta.KeyValue     := Null;
  //
  Nivel :=  UFinanceiro.NomeDoNivelSelecionado(1, RGNivPlaCta.ItemIndex);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNivelSel, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM ' + Nivel,
  'WHERE Codigo > 0',
  'ORDER BY Nome ',
  '']);
end;

function TDModFin.ReopenPendAll(Entidade, CliInt, Periodo: Integer; TabLctA:
String): Boolean;
var
  DataFim: String;
begin
  try
    DataFim := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(Periodo), 1);
    //
    QrPendAll.Close;
    QrPendAll.SQL.Clear;
    QrPendSum.Close;
    QrPendSum.SQL.Clear;
    QrPendAll.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendAll.SQL.Add('imv.Unidade, "Quota condominial" TIPO, 0 KGT,');
    end else begin
      QrPendAll.SQL.Add('"  " Unidade, "   " TIPO, 0 KGT,');
    end;
    QrPendAll.SQL.Add('COUNT(DISTINCT lan.FatNum) BLOQUETOS');
    QrPendAll.SQL.Add('FROM ' + TabLctA + ' lan');
    QrPendAll.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendAll.SQL.Add('WHERE car.Tipo=2');
    QrPendAll.SQL.Add('AND lan.Reparcel=0');
    QrPendAll.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('AND imv.Codigo=' + FormatFloat('0', CliInt));
    QrPendAll.SQL.Add('AND lan.Depto>0');
    QrPendAll.SQL.Add('AND lan.Sit<2');
    QrPendAll.SQL.Add('AND lan.Vencimento <= "'+DataFim+'"');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('GROUP BY imv.Unidade')
    else
      QrPendAll.SQL.Add('GROUP BY lan.Depto');
    QrPendAll.SQL.Add('');
    QrPendAll.SQL.Add('UNION');
    QrPendAll.SQL.Add('');
    QrPendAll.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendAll.SQL.Add('imv.Unidade, "Reparcelamento" TIPO, 0 KGT,');
    end else begin
      QrPendAll.SQL.Add('"  " Unidade, "   " TIPO, 0 KGT,');
    end;
    QrPendAll.SQL.Add('COUNT(DISTINCT lan.FatNum) BLOQUETOS');
    QrPendAll.SQL.Add('FROM ' + TabLctA + ' lan');
    QrPendAll.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendAll.SQL.Add('WHERE lan.Atrelado>0');
    QrPendAll.SQL.Add('AND lan.Genero=-10');
    QrPendAll.SQL.Add('AND car.Tipo=2');
    QrPendAll.SQL.Add('AND lan.Sit < 2');
    QrPendAll.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    QrPendAll.SQL.Add('AND lan.Vencimento <= "'+DataFim+'"');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('GROUP BY imv.Unidade')
    else
      QrPendAll.SQL.Add('GROUP BY lan.Depto');
    QrPendAll.SQL.Add('');
    QrPendAll.SQL.Add('ORDER BY Unidade');
    (*
    QrPendAll.Params[00].AsInteger := Entidade;
    QrPendAll.Params[01].AsInteger := CliInt;
    QrPendAll.Params[02].AsString  := DataFim;
    QrPendAll.Params[03].AsInteger := Entidade;
    QrPendAll.Params[04].AsString  := DataFim;
    *)
    UMyMod.AbreQuery(QrPendAll, Dmod.MyDB, 'ReopenPendAll()');
    //
    QrPendSum.SQL.Add('SELECT SUM(lan.Credito) Credito');
    QrPendSum.SQL.Add('FROM ' + TabLctA + ' lan');
    QrPendSum.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendSum.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendSum.SQL.Add('WHERE (car.Tipo=2');
    QrPendSum.SQL.Add('AND lan.Reparcel=0');
    QrPendSum.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendSum.SQL.Add('AND imv.Codigo=' + FormatFloat('0', CliInt));
    QrPendSum.SQL.Add('AND lan.Depto>0');
    QrPendSum.SQL.Add('AND lan.Sit<2');
    QrPendSum.SQL.Add('AND lan.Vencimento <="'+DataFim+'")');
    QrPendSum.SQL.Add('');
    QrPendSum.SQL.Add('OR');
    QrPendSum.SQL.Add('');
    QrPendSum.SQL.Add('(lan.Atrelado>0');
    QrPendSum.SQL.Add('AND lan.Genero=-10');
    QrPendSum.SQL.Add('AND car.Tipo=2');
    QrPendSum.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    QrPendSum.SQL.Add('AND lan.Vencimento <="'+DataFim+'"');
    QrPendSum.SQL.Add('AND lan.Sit < 2)');
    UMyMod.AbreQuery(QrPendSum, Dmod.MyDB, 'ReopenPendAll()');
    //
    Result := True;
  except
    //Result := False;
    raise;
  end;
end;

function TDModFin.ReopenPendG(Entidade, CliInt: Integer; TabLctA: String): Boolean;
begin
  try
    QrPendG.Close;
    QrPendG.SQL.Clear;
    QrPendG.SQL.Add('SELECT lan.Cliente, lan.Fornecedor, lan.FatNum,');
    QrPendG.SQL.Add('lan.Mez, SUM(lan.Credito) Credito, lan.Vencimento,');
    QrPendG.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrPendG.SQL.Add('ELSE ent.Nome END NOMEPROPRIET');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendG.SQL.Add(', imv.Unidade')
    else
      QrPendG.SQL.Add(', "? ? ?" Unidade');
    QrPendG.SQL.Add('FROM ' + TabLctA + ' lan');
    QrPendG.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPendG.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendG.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendG.SQL.Add('WHERE car.Tipo=2');
    QrPendG.SQL.Add('AND lan.Reparcel=0');
    QrPendG.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade)); // //QrCondCliente.Value;
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendG.SQL.Add('AND imv.Codigo=' + FormatFloat('0', CliInt)); //QrCondCodigo.Value;
    QrPendG.SQL.Add('AND lan.Depto>0');
    QrPendG.SQL.Add('AND lan.Sit<2');
    QrPendG.SQL.Add('AND lan.Vencimento < SYSDATE()');
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendG.SQL.Add('GROUP BY imv.Controle, imv.Unidade, lan.Mez, lan.Vencimento, lan.FatNum');
      QrPendG.SQL.Add('ORDER BY imv.Controle, imv.Unidade, lan.Mez, lan.Vencimento, lan.FatNum');
    end else begin
      QrPendG.SQL.Add('GROUP BY lan.Depto, lan.Mez, lan.Vencimento, lan.FatNum');
      QrPendG.SQL.Add('ORDER BY lan.Depto, lan.Mez, lan.Vencimento, lan.FatNum');
    end;
    UMyMod.AbreQuery(QrPendG, Dmod.MyDB, 'Pend�ncias de integrantes do cliente');
    //
    Result := True;
  except
    //Result := False;
    raise;
  end;
end;

function TDModFin.ReopenPrevItO(CliInt, Periodo: Integer; TabPrvA,
TabPriA: String): Boolean;
var
  Codigo: Integer;
begin
  Result := False;
  try
  case CO_DMKID_APP of
    4, 43: //Syndi2, Syndi3
    begin
      QrLocPerX.Close;
      QrLocPerX.SQL.Clear;
      QrLocPerX.SQL.Add('SELECT Codigo');
      QrLocPerX.SQL.Add('FROM ' + TabPrvA);
      QrLocPerX.SQL.Add('WHERE Cond=:P0');
      QrLocPerX.SQL.Add('AND Periodo=:P1');
      QrLocPerX.SQL.Add('');
      QrLocPerX.Params[0].AsInteger := CliInt;
      QrLocPerX.Params[1].AsInteger := Periodo - 1;
      UnDmkDAC_PF.AbreQuery(QrLocPerX, Dmod.MyDB);
      //
      Codigo := QrLocPerXCodigo.Value;
      //
      if Codigo = 0 then Codigo := -10000;
      //
      QrPrevItO.Close;
      QrPrevItO.SQL.Clear;
      QrPrevItO.SQL.Add('SELECT pit.*, con.SubGrupo,');
      QrPrevItO.SQL.Add('con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO');
      QrPrevItO.SQL.Add('FROM ' + TabPriA + ' pit');
      QrPrevItO.SQL.Add('LEFT JOIN contas con ON con.Codigo=pit.Conta');
      QrPrevItO.SQL.Add('LEFT JOIN subgrupos sgo ON sgo.Codigo=con.SubGrupo');
      QrPrevItO.SQL.Add('WHERE pit.Codigo=:P0');
      QrPrevItO.SQL.Add('ORDER BY NOMESUBGRUPO, NOMECONTA');
      QrPrevItO.Params[0].AsInteger := Codigo;
      UnDmkDAC_PF.AbreQuery(QrPrevItO, Dmod.MyDB);
      //
      Result := True;
    end
    else
    begin
      Geral.MB_Info('Aplicativo sem defini��o das tabelas de Provis�o de Arrecada��o!' +
        sLineBreak + 'AVISE A DERMATEK');
    end;
  end;
  except
    Geral.MB_Erro('N�o foi poss�vel abrir as tabelas de Provis�o de Arrecada��o!' +
      sLineBreak + 'AVISE A DERMATEK');
  end;
end;

function TDModFin.ReopenReparcelAbe(CliEnti: Integer; TabLctA: String): Boolean;
begin
  Result := False;
  try
    QrRepAbe.Close;
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrRepAbe.SQL.Clear;
      QrRepAbe.SQL.Add('SELECT imv.Unidade, lan.Data, lan.Vencimento, lan.Controle,');
      QrRepAbe.SQL.Add('lan.Descricao, lan.Credito, lan.Compensado, lan.FatNum,');
      QrRepAbe.SQL.Add('lan.MoraDia, lan.Multa, lan.Depto, lan.Sit,');
      QrRepAbe.SQL.Add('lan.Pago, lan.PagMul, lan.PagJur,  lan.Atrelado,');
      QrRepAbe.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
      QrRepAbe.SQL.Add('FROM ' + TabLctA + ' lan');
      QrRepAbe.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrRepAbe.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
      QrRepAbe.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
      QrRepAbe.SQL.Add('WHERE lan.Atrelado>0');
      QrRepAbe.SQL.Add('AND lan.Genero=-10');
      QrRepAbe.SQL.Add('AND car.Tipo=2');
      QrRepAbe.SQL.Add('AND car.ForneceI=:P0');
      QrRepAbe.SQL.Add('AND lan.Sit < 2');
      QrRepAbe.SQL.Add('ORDER BY NOMEPROP, lan.Atrelado, lan.Vencimento');
      QrRepAbe.Params[0].AsInteger := CliEnti;//QrCondCliente.Value;
      UnDmkDAC_PF.AbreQuery(QrRepAbe, Dmod.MyDB);
      Result := QrRepAbe.State <> dsInactive;
      if not Result then Geral.MB_Aviso(
      'N�o foi poss�vel detectar reparcelamentos em aberto!');
    end;
  except
    raise;
  end;
end;

function TDModFin.ReopenReparcelNew(CliEnti, Periodo, PBB: Integer): Boolean;
var
  DataI, DataF: String;
begin
  //Result := False;
  try
    DataI := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(Periodo - 1 + PBB), 1);
    DataF := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(Periodo - 1 + PBB), 1);
    //
    QrRep.Close;
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrRep.SQL.Clear;
      QrRep.SQL.Add('SELECT bpa.*, cim.Unidade,');
      QrRep.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
      QrRep.SQL.Add('FROM bloqparc bpa');
      QrRep.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=bpa.CodigoEnt');
      QrRep.SQL.Add('LEFT JOIN condimov  cim ON cim.Conta=bpa.CodigoEsp');
      QrRep.SQL.Add('WHERE bpa.CodCliEnt=' + FormatFloat('0', CliEnti));
      QrRep.SQL.Add('AND bpa.DataP BETWEEN "' + DataI + '" AND "' + DataF + '"');
      QrRep.SQL.Add('ORDER BY bpa.DataP, cim.Unidade');
      QrRep.SQL.Add('');
      {
      QrRep.Params[00].AsInteger := CliEnti;//QrCondCliente.Value;
      QrRep.Params[01].AsString  := DataI;
      QrRep.Params[02].AsString  := DataF;
      }
      UnDmkDAC_PF.AbreQuery(QrRep, Dmod.MyDB);
    end;
    //
    Result := QrRep.State <> dsInactive;
    if not Result then Geral.MB_Aviso(
    'N�o foi poss�vel identificar reparcelamentos!');
  except
    raise;
  end;
end;

function TDModFin.Reopen_APL_VLA(const ID_Pgto, SitAnt: Integer;
  var Data: String; var Pago, MoraVal, MultaVal, DescoVal: Double;
  var SitAtu: Integer; const DataCompQuit: String; const TbLctA: String): Boolean;
var
  D_APL, C_APL, D_VLA, C_VLA,
  SaldoDeb, SaldoCred, SaldoXXX, Endossan, Endossad: Double;
  ID_Pgto_Txt, TabLctA, TabLctB, TabLctD: String;
begin
  Result := False;
  // 2011-12-31
  TabLctA := CorrigeTabXxxY(TbLctA);
  // Fim 2011-12-31
  SitAtu := -9;
  Data   := '';
  ID_Pgto_Txt := FormatFloat('0', ID_Pgto);
  TabLctB := Copy(TabLctA, 1, Length(TabLctA) - 1) + 'b';
  TabLctD := Copy(TabLctA, 1, Length(TabLctA) - 1) + 'd';
  // Endosso
  QrVLA.Close;
  QrVLA.SQL.Clear;
{
  QrVLA.SQL.Add('SELECT Credito, Debito, Endossan, Endossad, Tipo');
  QrVLA.SQL.Add('FROM ' + lct);
  QrVLA.SQL.Add('WHERE Sub=0');
  QrVLA.SQL.Add('AND Controle=:P0');
  QrVLA.Params[0].AsInteger := ID_Pgto;
}
  QrVLA.SQL.Add('SELECT Credito, Debito, Endossan, Endossad, Tipo');
  QrVLA.SQL.Add('FROM ' + TabLctA);
  QrVLA.SQL.Add('WHERE Sub=0');
  QrVLA.SQL.Add('AND Controle=' + ID_Pgto_Txt);
  QrVLA.SQL.Add('UNION');
  QrVLA.SQL.Add('SELECT Credito, Debito, Endossan, Endossad, Tipo');
  QrVLA.SQL.Add('FROM ' + TabLctB);
  QrVLA.SQL.Add('WHERE Sub=0');
  QrVLA.SQL.Add('AND Controle=' + ID_Pgto_Txt);
  QrVLA.SQL.Add('UNION');
  QrVLA.SQL.Add('SELECT Credito, Debito, Endossan, Endossad, Tipo');
  QrVLA.SQL.Add('FROM ' + TabLctD);
  QrVLA.SQL.Add('WHERE Sub=0');
  QrVLA.SQL.Add('AND Controle=' + ID_Pgto_Txt);
  QrVLA.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrVLA, Dmod.MyDB);
  //
  // N�o � emiss�o
  if QrVLATipo.Value <> 2 then Exit;
  //
  // Pagamentos emitidos
  QrAPL.Close;
  QrAPL.SQL.Clear;
{
  QrAPL.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito,');
  QrAPL.SQL.Add('SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,');
  QrAPL.SQL.Add('MAX(Data) Data, COUNT(Controle) Itens');
  QrAPL.SQL.Add('FROM ' + lct + ' WHERE ID_Pgto=:P0');
  QrAPL.Params[0].AsInteger := ID_Pgto;
}
  QrAPL.SQL.Add('DROP TABLE IF EXISTS _MODULEFIN_APL_;');
  QrAPL.SQL.Add('');
  QrAPL.SQL.Add('CREATE TABLE _MODULEFIN_APL_ IGNORE ');
  QrAPL.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito,');
  QrAPL.SQL.Add('SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,');
  QrAPL.SQL.Add('SUM(DescoVal) DescoVal, MAX(Data) Data, COUNT(Controle) Itens');
  QrAPL.SQL.Add('FROM ' + TabLctA);
  QrAPL.SQL.Add('WHERE ID_Pgto=' + ID_Pgto_Txt);
  QrAPL.SQL.Add('UNION');
  QrAPL.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito,');
  QrAPL.SQL.Add('SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,');
  QrAPL.SQL.Add('SUM(DescoVal) DescoVal, MAX(Data) Data, COUNT(Controle) Itens');
  QrAPL.SQL.Add('FROM ' + TabLctB);
  QrAPL.SQL.Add('WHERE ID_Pgto=' + ID_Pgto_Txt);
  QrAPL.SQL.Add('UNION');
  QrAPL.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito,');
  QrAPL.SQL.Add('SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,');
  QrAPL.SQL.Add('SUM(DescoVal) DescoVal, MAX(Data) Data, COUNT(Controle) Itens');
  QrAPL.SQL.Add('FROM ' + TabLctD);
  QrAPL.SQL.Add('WHERE ID_Pgto=' + ID_Pgto_Txt);
  QrAPL.SQL.Add(';');
  QrAPL.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito,');
  QrAPL.SQL.Add('SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,');
  QrAPL.SQL.Add('SUM(DescoVal) DescoVal, MAX(Data) Data, SUM(Itens) Itens');
  QrAPL.SQL.Add('FROM _MODULEFIN_APL_;');
  QrAPL.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrAPL, DModG.MyPID_DB);
  //
  if QrVLA.RecordCount <> 1 then
  begin
    // Atualiza��o de saldo de lan�amento de transfer�ncia. ERRO. Fazer espec�fico?
    Geral.MB_Aviso('Atualiza��o de saldo de lan�amento inv�lido!' +
    sLineBreak + 'ID_Pgto = ' + IntToStr(ID_Pgto) + sLineBreak + 'AVISE A DERMATEK');
    //Endossan    := 0;
    //Endossad    := 0;
    //SaldoDeb    := 0;
    //SaldoCred   := 0;
    Pago        := 0;
    //Debito      := 0;
    //Credito     := 0;
    MoraVal     := 0;
    MultaVal    := 0;
    DescoVal    := 0;
  end else begin
    //Result := True;
    //
    Endossan   := QrVLAEndossan.Value;
    Endossad   := QrVLAEndossad.Value;
    //
    D_APL      := QrAPLDebito.Value;
    C_APL      := QrAPLCredito.Value;
    Pago       := C_APL + D_APL  + Endossan + Endossad;
    //
    MoraVal    := QrAPLMoraVal.Value;
    MultaVal   := QrAPLMultaVal.Value;
    DescoVal   := QrAPLDescoVal.Value;
    //
    D_VLA      := QrVLADebito.Value;//QrAPLDebito.Value;
    C_VLA      := QrVLACredito.Value;//QrAPLCredito.Value;
    if D_VLA > 0 then
    begin
      //SaldoCred  := 0;
      if (Pago - MoraVal - MultaVal + DescoVal) = D_VLA then
        SaldoDeb := 0
      else if Pago > D_VLA then
        SaldoDeb := 0
      else
        SaldoDeb := - (D_VLA  - Pago);
      Pago       := - Pago;
      SaldoXXX   := SaldoDeb;
    end else begin
      if (Pago - MoraVal - MultaVal + DescoVal) = C_VLA then
        SaldoCred := 0
      else if Pago > C_VLA then
        SaldoCred := 0
      else
        SaldoCred    := C_VLA  - Pago;
      //SaldoDeb    := 0;
      SaldoXXX    := SaldoCred;
    end;

    // SIT
    if SaldoXXX = 0 then
    begin
      if QrAPLItens.Value > 1 then
        SitAtu := 2
      else
        SitAtu := 3;
    end else
    if Pago <> 0 then
      SitAtu := 1
    else
      SitAtu := 0;

    // DATA
    if (SitAtu in ([2,3])) then
    begin
      if QrAPL.RecordCount > 0 then
        Data := Geral.FDT(QrAPLData.Value, 1)
      else
      if (DataCompQuit <> '') and
      (DataCompQuit <> '0000-00-00') and (DataCompQuit <> '1899-12-30') and
      (DataCompQuit <> '0000/00/00') and (DataCompQuit <> '1899/12/30') then
        Data := DataCompQuit
      else begin
        Geral.MB_Erro(
        'ERRO na obten��o de dados para compensa��o / quita��o' + sLineBreak +
        'Avise a Dermatek!');
        Exit;
      end;
    end;
    //

    Result := True;
  end;
end;

procedure TDModFin.SaldosDeContasControladas3(CliInt, Mez: Integer;
QrSaldosNiv: TmySQLQuery);
var
  SdoAnt, Credito, Debito, SdoFim: Double;
  ID_SEQ: Integer;
begin
  QrSaldosNiv.Close;
  QrSaldosNiv.DataBase := DModG.MyPID_DB;
  QrSdoPar.Close;
  QrSdoPar.DataBase := DModG.MyPID_DB;
  //
  FSaldosNiv := UCriar.RecriaTempTable('SaldosNiv', DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add(
  'INSERT INTO saldosniv (Nivel,CO_Cta,CO_SGr,CO_Gru,CO_Cjt,CO_Pla,' +
  'NO_Cta,NO_SGr,NO_Gru,NO_Cjt,NO_Pla,OL_Cta,OL_SGr,OL_Gru,OL_Cjt,' +
  'OL_Pla,SdoAnt,Credito,Debito,Movim,EX_Cta,EX_SGr,EX_Gru,EX_Cjt,EX_Pla,' +
  'ID_SEQ,Ativo) VALUES (:P0,:P1,:P2,:P3,:P4,:P5,:P6,:P7,:P8,:P9,:P10,:P11,' +
  ':P12,:P13,:P14,:P15,:P16,:P17,:P18,:P19,:P20,:P21,:P22,:P23,:P24,:P25,:P26)');
  //
  ID_SEQ := 0;

  //  CONTAS
  QrNiv1.Close;
  QrNiv1.Params[00].AsInteger := CliInt;
  QrNiv1.Params[01].AsInteger := CliInt;
  QrNiv1.Params[02].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrNiv1, Dmod.MyDB);
  if QrNiv1.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT mov.Credito, mov.Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Genero=:P0');
    while not QrNiv1.Eof do
    begin
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv1Genero.Value;
      UnDmkDAC_PF.AbreQuery(QrNivMov, Dmod.MyDB);
      SdoAnt := QrNiv1Movim.Value - QrNivMovCredito.Value + QrNivMovDebito.Value;
      DModG.QrUpdPID1.Params[00].AsInteger := 1;

      DModG.QrUpdPID1.Params[01].AsInteger := QrNiv1Genero.Value;
      DModG.QrUpdPID1.Params[02].AsInteger := QrNiv1Subgrupo.Value;
      DModG.QrUpdPID1.Params[03].AsInteger := QrNiv1Grupo.Value;
      DModG.QrUpdPID1.Params[04].AsInteger := QrNiv1Conjunto.Value;
      DModG.QrUpdPID1.Params[05].AsInteger := QrNiv1Plano.Value;

      DModG.QrUpdPID1.Params[06].AsString  := QrNiv1NO_Cta.Value;
      DModG.QrUpdPID1.Params[07].AsString  := QrNiv1NO_SGr.Value;
      DModG.QrUpdPID1.Params[08].AsString  := QrNiv1NO_Gru.Value;
      DModG.QrUpdPID1.Params[09].AsString  := QrNiv1NO_Cjt.Value;
      DModG.QrUpdPID1.Params[10].AsString  := QrNiv1NO_Pla.Value;

      DModG.QrUpdPID1.Params[11].AsInteger := QrNiv1OR_Cta.Value;
      DModG.QrUpdPID1.Params[12].AsInteger := QrNiv1OR_Sgr.Value;
      DModG.QrUpdPID1.Params[13].AsInteger := QrNiv1OR_Gru.Value;
      DModG.QrUpdPID1.Params[14].AsInteger := QrNiv1OR_Cjt.Value;
      DModG.QrUpdPID1.Params[15].AsInteger := QrNiv1OR_Pla.Value;

      DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
      DModG.QrUpdPID1.Params[17].AsFloat   := QrNivMovCredito.Value;
      DModG.QrUpdPID1.Params[18].AsFloat   := QrNivMovDebito.Value;
      DModG.QrUpdPID1.Params[19].AsFloat   := QrNiv1Movim.Value;

      DModG.QrUpdPID1.Params[20].AsInteger := QrNiv1Genero.Value;
      DModG.QrUpdPID1.Params[21].AsInteger := QrNiv1Subgrupo.Value;
      DModG.QrUpdPID1.Params[22].AsInteger := QrNiv1Grupo.Value;
      DModG.QrUpdPID1.Params[23].AsInteger := QrNiv1Conjunto.Value;
      DModG.QrUpdPID1.Params[24].AsInteger := QrNiv1Plano.Value;

      DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
      DModG.QrUpdPID1.Params[26].AsInteger := 1;
      //
      DModG.QrUpdPID1.ExecSQL;
      QrNiv1.Next;
    end;
  end;

  // SUBGRUPOS

  QrNiv2.Close;
  QrNiv2.Params[00].AsInteger := CliInt;
  QrNiv2.Params[01].AsInteger := CliInt;
  QrNiv2.Params[02].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrNiv2, Dmod.MyDB);
  if QrNiv2.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Subgrupo=:P0');
    while not QrNiv2.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_SGr=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv2Subgrupo.Value;
      UnDmkDAC_PF.AbreQuery(QrSdoPar, DModG.MyPID_DB);
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv2Subgrupo.Value;
      UnDmkDAC_PF.AbreQuery(QrNivMov, Dmod.MyDB);
      SdoAnt  := QrNiv2Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv2Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 2;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv2Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv2Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv2Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv2Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv2Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv2NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv2NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv2NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv2NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv2NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv2OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv2OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv2OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv2OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv2OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv2Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv2Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv2Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv2Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv2Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 1;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv2.Next;
    end;
  end;

  // GRUPOS

  QrNiv3.Close;
  QrNiv3.Params[00].AsInteger := CliInt;
  QrNiv3.Params[01].AsInteger := CliInt;
  QrNiv3.Params[02].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrNiv3, Dmod.MyDB);
  if QrNiv3.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Grupo=:P0');
    while not QrNiv3.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Gru=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv3Grupo.Value;
      UnDmkDAC_PF.AbreQuery(QrSdoPar, DModG.MyPID_DB);
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv3Grupo.Value;
      UnDmkDAC_PF.AbreQuery(QrNivMov, Dmod.MyDB);
      SdoAnt  := QrNiv3Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv3Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 3;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv3Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv3Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv3Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv3Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv3Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv3NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv3NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv3NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv3NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv3NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv3OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv3OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv3OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv3OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv3OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv3Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv3Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv3Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv3Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv3Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 1;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv3.Next;
    end;
  end;

  // CONJUNTOS

  QrNiv4.Close;
  QrNiv4.Params[00].AsInteger := CliInt;
  QrNiv4.Params[01].AsInteger := CliInt;
  QrNiv4.Params[02].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrNiv4, Dmod.MyDB);
  if QrNiv4.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Conjunto=:P0');
    while not QrNiv4.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Cjt=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv4Conjunto.Value;
      UnDmkDAC_PF.AbreQuery(QrSdoPar, DModG.MyPID_DB);
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv4Conjunto.Value;
      UnDmkDAC_PF.AbreQuery(QrNivMov, Dmod.MyDB);
      SdoAnt  := QrNiv4Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv4Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 4;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv4Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv4Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv4Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv4Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv4Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv4NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv4NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv4NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv4NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv4NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv4OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv4OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv4OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv4OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv4OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv4Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv4Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv4Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv4Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv4Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 1;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv4.Next;
    end;
  end;

  // PLANOS

  QrNiv5.Close;
  QrNiv5.Params[00].AsInteger := CliInt;
  QrNiv5.Params[01].AsInteger := CliInt;
  QrNiv5.Params[02].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrNiv5, Dmod.MyDB);
  if QrNiv5.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Plano=:P0');
    while not QrNiv5.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Pla=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv5Plano.Value;
      UnDmkDAC_PF.AbreQuery(QrSdoPar, DModG.MyPID_DB);
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv5Plano.Value;
      UnDmkDAC_PF.AbreQuery(QrNivMov, Dmod.MyDB);
      SdoAnt  := QrNiv5Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv5Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 1;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv5Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv5Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv5Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv5Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv5Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv5NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv5NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv5NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv5NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv5NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv5OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv5OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv5OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv5OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv5OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv5Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv5Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv5Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv5Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv5Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 5;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv5.Next;
    end;
  end;

  //

  QrSaldosNiv.Close;
  QrSaldosNiv.SQL.Clear;
  QrSaldosNiv.SQL.Add('SELECT *, 0 KGT FROM saldosniv');
  QrSaldosNiv.SQL.Add('ORDER BY OL_Pla, NO_Pla, OL_Cjt, NO_Cjt, OL_Gru, ');
  QrSaldosNiv.SQL.Add('NO_Gru, OL_SGr, NO_SGr, OL_Cta, NO_Cta;');
  UnDmkDAC_PF.AbreQuery(QrSaldosNiv, DModG.MyPID_DB);
end;

function TDmodFin.SaldosNiveisPeriodo_2(Entidade, Periodo, Controla: Integer;
TodosGeneros: Boolean; PB1, PB2, PB3, PB4, PB5: TProgressBar;
QrToOpen: TmySQLQuery): Boolean;
var
  i: integer;
  NomeNi, Tab, Ord: String;
  PB: TProgressBar;
begin
  FSdoNiveis := UCriar.RecriaTempTable('sdoniveis', DModG.QrUpdPID1, False);
  //
  NomeNi := '';
  // informar o saldo total mesmo que n�o informe os saldos dos n�veis
  QrSTCP.Close;
  QrSTCP.Params[0].AsInteger := Entidade;
  QrSTCP.Params[1].AsInteger := Periodo;
  UnDmkDAC_PF.AbreQuery(QrSTCP, Dmod.MyDB);
  // QrClienteTemContasNiv
  QrCTCN.Close;
  QrCTCN.Params[0].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQuery(QrCTCN, Dmod.MyDB);
  //
  if DmodFin.QrCTCN.RecordCount = 0 then
  begin
    Geral.MB_Aviso('O Cliente interno selecionado n�o possui ' +
    'nenhum cadastro de n�veis de contas controlados!' + sLineBreak + 'Para ' +
    'configurar, clique no bot�o "Config." (caso haja no formul�rio atual).' +
    'Ou selecione no menu geral: Cadastros > Financeiros > Plano de contas > ' +
    'N�veis de saldos (Config.)');
    Result := False;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    for i := 1 to 5 do
    begin
      case i of
        1: PB := PB1;
        2: PB := PB2;
        3: PB := PB3;
        4: PB := PB4;
        5: PB := PB5;
        else PB := nil;
      end;
      if PB <> nil then
        PB.Position := 0;
    end;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO sdoniveis SET');
    DmodG.QrUpdPID1.SQL.Add('Nivel=:P00, ');
    DmodG.QrUpdPID1.SQL.Add('Genero=:P01, ');
    DmodG.QrUpdPID1.SQL.Add('NomeGe=:P02, ');
    DmodG.QrUpdPID1.SQL.Add('NomeNi=:P03, ');
    DmodG.QrUpdPID1.SQL.Add('SumMov=:P04, ');
    DmodG.QrUpdPID1.SQL.Add('SdoAnt=:P05, ');
    DmodG.QrUpdPID1.SQL.Add('SumCre=:P06, ');
    DmodG.QrUpdPID1.SQL.Add('SumDeb=:P07, ');
    DmodG.QrUpdPID1.SQL.Add('SdoFim=:P08, ');
    DmodG.QrUpdPID1.SQL.Add('CodPla=:P09, ');
    DmodG.QrUpdPID1.SQL.Add('CodCjt=:P10, ');
    DmodG.QrUpdPID1.SQL.Add('CodGru=:P11, ');
    DmodG.QrUpdPID1.SQL.Add('CodSgr=:P12, ');
    DmodG.QrUpdPID1.SQL.Add('Ordena=:P13 ');


    {
    DmodG.QrUpdPID1.SQL.Add('Ctrla =:P09, ');
    DmodG.QrUpdPID1.SQL.Add('Seleci=:P10 ');
    }
    // N�veis
    for i := 1 to 5 do
    begin
      case i of
        1: Tab := 'Cta';
        2: Tab := 'Sgr';
        3: Tab := 'Gru';
        4: Tab := 'Cjt';
        5: Tab := 'Pla';
        else Tab := '???';
      end;
      case I of
        1: NomeNi := 'Conta';
        2: NomeNi := 'Sub-grupo';
        3: NomeNi := 'Grupo';
        4: NomeNi := 'Conjunto';
        5: NomeNi := 'Plano';
        else NomeNi := '? ? ?';
      end;
      QrSNC.Close;
      QrSNC.SQL.Clear;
      QrSNC.SQL.Add('SELECT ' + Tab + '.Codigo,  ' + Tab + '.Nome NOME, ');
      QrSNC.SQL.Add('SUM(his.SumCre-his.SumDeb) SUMMOV,');
      QrSNC.SQL.Add('SUM(his.SdoAnt) SDOANT, SUM(his.SumCre) SUMCRE,');
      QrSNC.SQL.Add('SUM(his.SumDeb) SUMDEB, SUM(his.SdoFim) SDOFIM,');
      QrSNC.SQL.Add('pla.Codigo CodPla, cjt.Codigo CodCjt, ');
      QrSNC.SQL.Add('gru.Codigo CodGru, sgr.Codigo CodSgr ');
      QrSNC.SQL.Add('FROM contashis his');
      QrSNC.SQL.Add('LEFT JOIN contas    cta ON cta.Codigo=his.Codigo');
      QrSNC.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
      QrSNC.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
      QrSNC.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
      QrSNC.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
      if not TodosGeneros then
        QrSNC.SQL.Add('LEFT JOIN contasniv niv ON ' + Tab + '.Codigo=niv.Genero');
      QrSNC.SQL.Add('WHERE his.Entidade=' + FormatFloat('0', Entidade));
      QrSNC.SQL.Add('AND his.Periodo=' + FormatFloat('0', Periodo));
      if TodosGeneros then
        QrSNC.SQL.Add('AND ' + Tab + '.CtrlaSdo>' + FormatFloat('0', Controla))
      else
        QrSNC.SQL.Add('AND niv.Nivel=' + IntToStr(i) +
        ' AND niv.Entidade=' + FormatFloat('0', Entidade));
      QrSNC.SQL.Add('');
      QrSNC.SQL.Add('GROUP BY ' + Tab + '.Codigo');
      QrSNC.SQL.Add('ORDER BY ' + Tab + '.OrdemLista, ' + Tab + '.Nome');
      //
      UnDmkDAC_PF.AbreQuery(QrSNC, Dmod.MyDB);
      case i of
        1: PB := PB1;
        2: PB := PB2;
        3: PB := PB3;
        4: PB := PB4;
        5: PB := PB5;
        else PB := nil;
      end;
      if PB <> nil then
      begin
        PB.Max := QrSnc.RecordCount;
        if PB.Max = 0 then PB.Max := 1;
      end;
      QrSNC.First;
      while not QrSNC.Eof do
      begin
        if PB <> nil then
          PB.Position := PB.Position + 1;
        Ord := FormatFloat('0', QrSNCCodPla.Value);
        if i < 5 then Ord := Ord + '.' + FormatFloat('00', QrSNCCodCjt.Value);
        if i < 4 then Ord := Ord + '.' + FormatFloat('000', QrSNCCodGru.Value);
        if i < 3 then Ord := Ord + '.' + FormatFloat('0000', QrSNCCodSgr.Value);
        if i < 2 then Ord := Ord + '.' + FormatFloat('00000', QrSNCCodigo.Value);
        DmodG.QrUpdPID1.Params[00].AsInteger := i; // n�vel
        DmodG.QrUpdPID1.Params[01].AsInteger := QrSNCCodigo.Value;
        DmodG.QrUpdPID1.Params[02].AsString  := QrSNCNOME.Value;
        DmodG.QrUpdPID1.Params[03].AsString  := NomeNi; // Precisa?
        DmodG.QrUpdPID1.Params[04].AsFloat   := QrSNCSUMMOV.Value;
        DmodG.QrUpdPID1.Params[05].AsFloat   := QrSNCSDOANT.Value;
        DmodG.QrUpdPID1.Params[06].AsFloat   := QrSNCSUMCRE.Value;
        DmodG.QrUpdPID1.Params[07].AsFloat   := QrSNCSUMDEB.Value;
        DmodG.QrUpdPID1.Params[08].AsFloat   := QrSNCSDOFIM.Value;
        DmodG.QrUpdPID1.Params[09].AsInteger := QrSNCCodPla.Value;
        DmodG.QrUpdPID1.Params[10].AsInteger := QrSNCCodCjt.Value;
        DmodG.QrUpdPID1.Params[11].AsInteger := QrSNCCodGru.Value;
        DmodG.QrUpdPID1.Params[12].AsInteger := QrSNCCodSgr.Value;
        DmodG.QrUpdPID1.Params[13].AsString  := Ord;

        DmodG.QrUpdPID1.ExecSQL;
        //
        QrSNC.Next;
      end;
      if PB <> nil then
        PB.Position := PB.Max;
    end;
    QrToOpen.Close;
    UnDmkDAC_PF.AbreQuery(QrToOpen, DModG.MyPID_DB);
    //
    Result := True;
    if PB1 <> nil then
      PB1.Position := 0;
    if PB2 <> nil then
      PB2.Position := 0;
    if PB3 <> nil then
      PB3.Position := 0;
    if PB4 <> nil then
      PB4.Position := 0;
    if PB5 <> nil then
      PB5.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDModFin.VerificaSit(const Sit: Integer; const InfoCred,
  InfoDeb: Double; var RetCred, RetDeb: Double): Boolean;
begin
  case Sit of
    0..3:
    begin
      RetCred := InfoCred;
      RetDeb := InfoDeb;
      Result := True;
    end;
    4..6:
    begin
      RetCred := 0;
      RetDeb := 0;
      Result := True;
    end
    else
    begin
      RetCred := 0;
      RetDeb := 0;
      Result := False;
      Geral.MB_Erro('Situa��o de lan�amento n�o definida: ' +
        Geral.FF0(Sit));
    end;
  end;
end;

//
(* PgBloq
SELECT IF(lan.Compensado<2, 0, IF(car.Tipo=2,
lan.Credito + lan.PagMul + lan.PagJur, lan.Credito)) Credito,
IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal)) MultaVal,
IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal)) MoraVal,
CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE
ent.Nome END NOMEPROPRIET, imv.Unidade UH,
con.Nome NOMECONTA, lan.Mez, lan.Vencimento,
lan.Documento, lan.Credito ORIGINAL, lan.Compensado Data,
IF(lan.Compensado<2, "", DATE_FORMAT(lan.Compensado, "%d/%m/%Y")) DATA_TXT
FROM lct lan
LEFT JOIN carteiras car ON car.Codigo=lan.Carteira
LEFT JOIN condimov imv ON imv.Conta=lan.Depto
LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI
LEFT JOIN contas con ON con.Codigo=lan.Genero
WHERE car.Tipo in (0,2)
AND lan.FatID in (600,601)
AND car.ForneceI=:P0
AND lan.Mez=:P1

ORDER BY UH, lan.Documento, con.OrdemLista, NOMECONTA
*)

(*  QrSuBloq
SELECT SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2,
lan.Credito + lan.PagMul + lan.PagJur, lan.Credito))) PAGO,
SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal))) MultaVal,
SUM(IF(lan.Compensado<2, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal))) MoraVal,
con.Nome NOMECONTA, SUM(lan.Credito) ORIGINAL, 0 KGT
FROM lct lan
LEFT JOIN carteiras car ON car.Codigo=lan.Carteira
LEFT JOIN condimov imv ON imv.Conta=lan.Depto
LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI
LEFT JOIN contas con ON con.Codigo=lan.Genero
WHERE car.Tipo in (0,2)
AND lan.FatID in (600,601)
AND car.ForneceI=:P0
AND lan.Mez=:P1

GROUP BY NOMECONTA
*)

end.
