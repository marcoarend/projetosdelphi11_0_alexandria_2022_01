unit CashBal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, dmkGeral, Grids, DBGrids, dmkCheckGroup, Math, dmkDBGrid,
  dmkDBGridDAC, dmkCheckBox, Mask, TypInfo, UnDmkProcFunc, DmkDAC_PF, dmkImage,
  frxExportPDF, frxDock, frxCtrls, UnDmkEnums, frxExportBaseDialog;

type
  TFmCashBal = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    frxBal_A_01: TfrxReport;
    frxBal_A_02: TfrxReport;
    QrEntiRespon: TmySQLQuery;
    QrEntiResponNO_RESPON: TWideStringField;
    QrEntiResponNO_CARGO: TWideStringField;
    frxDsEntiRespon: TfrxDBDataset;
    frxFIN_BALAN_001_001: TfrxReport;
    QrEntiCfgRel_01: TmySQLQuery;
    QrEntiCfgRel_01Codigo: TIntegerField;
    QrEntiCfgRel_01Controle: TIntegerField;
    QrEntiCfgRel_01RelTipo: TIntegerField;
    QrEntiCfgRel_01RelTitu: TWideStringField;
    QrEntiCfgRel_01Ordem: TIntegerField;
    QrEntiCfgRel_01RelPrcr: TWideMemoField;
    QrEntiCfgRel_01RelItem: TIntegerField;
    QrEntiCfgRel_01Genero: TIntegerField;
    frxDsEntiCfgRel_01: TfrxDBDataset;
    frxBal_A_03: TfrxReport;
    QrArr1: TmySQLQuery;
    QrArr1NO_ENT: TWideStringField;
    QrArr1Cliente: TIntegerField;
    QrArr1ANO: TLargeintField;
    QrArr1MES: TLargeintField;
    QrArr1Valor: TFloatField;
    QrAnt1: TmySQLQuery;
    QrAnt1Cliente: TIntegerField;
    QrAnt1Valor: TFloatField;
    frxDsArre1: TfrxDBDataset;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DsArre1: TDataSource;
    QrArr2: TmySQLQuery;
    QrArr2NO_ENT: TWideStringField;
    QrArr2Cliente: TIntegerField;
    QrArr2Mez: TIntegerField;
    QrArr2Valor: TFloatField;
    frxDsArre2: TfrxDBDataset;
    DBGrid2: TDBGrid;
    DsArre2: TDataSource;
    frxBal_A_04: TfrxReport;
    QrAnt2: TmySQLQuery;
    QrAnt2NO_ENT: TWideStringField;
    QrAnt2Cliente: TIntegerField;
    QrAnt2Valor: TFloatField;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet5: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet6: TTabSheet;
    DBGrid5: TDBGrid;
    frxBal_A_06: TfrxReport;
    QrLCS: TmySQLQuery;
    QrLCSNOMEPLANO: TWideStringField;
    QrLCSPLANO: TIntegerField;
    QrLCSNOMECONJUNTO: TWideStringField;
    QrLCSConjunto: TIntegerField;
    QrLCSNOMEGRUPO: TWideStringField;
    QrLCSGrupo: TIntegerField;
    QrLCSNOMESUBGRUPO: TWideStringField;
    QrLCSSubGrupo: TIntegerField;
    QrLCSNOMECONTA: TWideStringField;
    QrLCSGenero: TIntegerField;
    QrLCSValor: TFloatField;
    frxDsLCS: TfrxDBDataset;
    frxBal_A_07: TfrxReport;
    frxBal_A_08: TfrxReport;
    QrCtasResMes: TmySQLQuery;
    QrCtasResMesConta: TIntegerField;
    QrCtasResMesNome: TWideStringField;
    QrCtasResMesPeriodo: TIntegerField;
    QrCtasResMesTipo: TIntegerField;
    QrCtasResMesFator: TFloatField;
    QrCtasResMesValFator: TFloatField;
    QrCtasResMesDevido: TFloatField;
    QrCtasResMesPago: TFloatField;
    QrCtasResMesDiferenca: TFloatField;
    QrCtasResMesAcumulado: TFloatField;
    QrCtasResMesNOME_PERIODO: TWideStringField;
    frxDsCtasResMes: TfrxDBDataset;
    frxBal_A_09: TfrxReport;
    QrExclusivos: TmySQLQuery;
    QrExclusivosMovim: TFloatField;
    frxDsExclusivos: TfrxDBDataset;
    QrExclMov: TmySQLQuery;
    QrExclMovCredito: TFloatField;
    QrExclMovDebito: TFloatField;
    QrExclusivosCREDITO: TFloatField;
    QrExclusivosDEBITO: TFloatField;
    QrExclusivosANTERIOR: TFloatField;
    QrOrdinarios: TmySQLQuery;
    QrOrdiMov: TmySQLQuery;
    QrOrdiMovCredito: TFloatField;
    QrOrdiMovDebito: TFloatField;
    QrOrdinariosMovim: TFloatField;
    QrOrdinariosCREDITO: TFloatField;
    QrOrdinariosDEBITO: TFloatField;
    QrOrdinariosANTERIOR: TFloatField;
    frxDsOrdinarios: TfrxDBDataset;
    QrAnt1NO_ENT: TWideStringField;
    QrEntiCfgRel_01LogoPath: TWideStringField;
    Panel7: TPanel;
    QrDat1: TmySQLQuery;
    QrDat2: TmySQLQuery;
    QrDat1Cliente: TIntegerField;
    QrDat1NO_ENT: TWideStringField;
    QrDat1Valr01: TFloatField;
    QrDat1Valr02: TFloatField;
    QrDat1Valr03: TFloatField;
    QrDat1Valr04: TFloatField;
    QrDat1Valr05: TFloatField;
    QrDat1Valr06: TFloatField;
    QrDat1Valr07: TFloatField;
    QrDat1Valr08: TFloatField;
    QrDat1Valr09: TFloatField;
    QrDat1Valr10: TFloatField;
    QrDat1Valr11: TFloatField;
    QrDat1Valr12: TFloatField;
    QrDat1ValrA0: TFloatField;
    QrDat1ValrM0: TFloatField;
    QrDat1ValrA1: TFloatField;
    QrDat1ValrM1: TFloatField;
    QrDat1SEQ: TIntegerField;
    QrDat2Cliente: TIntegerField;
    QrDat2NO_ENT: TWideStringField;
    QrDat2Valr01: TFloatField;
    QrDat2Valr02: TFloatField;
    QrDat2Valr03: TFloatField;
    QrDat2Valr04: TFloatField;
    QrDat2Valr05: TFloatField;
    QrDat2Valr06: TFloatField;
    QrDat2Valr07: TFloatField;
    QrDat2Valr08: TFloatField;
    QrDat2Valr09: TFloatField;
    QrDat2Valr10: TFloatField;
    QrDat2Valr11: TFloatField;
    QrDat2Valr12: TFloatField;
    QrDat2ValrA0: TFloatField;
    QrDat2ValrM0: TFloatField;
    QrDat2ValrA1: TFloatField;
    QrDat2ValrM1: TFloatField;
    QrDat2SEQ: TIntegerField;
    QrLC2: TmySQLQuery;
    QrSSdo: TmySQLQuery;
    QrSSdoCodigo: TIntegerField;
    QrSSdoValor: TFloatField;
    QrLC2NOMEPLANO: TWideStringField;
    QrLC2PLANO: TIntegerField;
    QrLC2NOMECONJUNTO: TWideStringField;
    QrLC2Conjunto: TIntegerField;
    QrLC2NOMEGRUPO: TWideStringField;
    QrLC2Grupo: TIntegerField;
    QrLC2NOMESUBGRUPO: TWideStringField;
    QrLC2SubGrupo: TIntegerField;
    QrLC2NOMECONTA: TWideStringField;
    QrLC2Genero: TIntegerField;
    QrLC2Valor: TFloatField;
    QrLC2OL_PLA: TIntegerField;
    QrLC2OL_CJT: TIntegerField;
    QrLC2OL_GRU: TIntegerField;
    QrLC2OL_SGR: TIntegerField;
    QrLC2OL_CON: TIntegerField;
    frxBal_A_07_: TfrxReport;
    QrSN1: TmySQLQuery;
    QrSN2: TmySQLQuery;
    QrSN1Movim: TFloatField;
    QrSN2Valor: TFloatField;
    QrLC2vSGR: TFloatField;
    QrLC2vGRU: TFloatField;
    QrLC2vCJT: TFloatField;
    QrLC2vPLA: TFloatField;
    QrEntiResponObserv: TWideStringField;
    frxBal_A_11: TfrxReport;
    frxBal_A_12B: TfrxReport;
    frxBal_A_12A: TfrxReport;
    QrEntiCfgRel_01FonteTam: TSmallintField;
    frxBal_A_13: TfrxReport;
    frxBal_A_14: TfrxReport;
    frxBal_A_15: TfrxReport;
    Panel5: TPanel;
    Panel8: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGFormato: TRadioGroup;
    GroupBox2: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    GroupBox1: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    LaAviso: TLabel;
    Panel6: TPanel;
    LaSub1: TLabel;
    LaSub2: TLabel;
    LaSub3: TLabel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    Panel9: TPanel;
    QrLista: TmySQLQuery;
    DsLista: TDataSource;
    QrListaControle: TIntegerField;
    QrListaRelTitu: TWideStringField;
    QrListaOrdem: TIntegerField;
    QrListaAtivo: TSmallintField;
    QrSels: TmySQLQuery;
    QrSelsItens: TLargeintField;
    frxFIN_BALAN_001_002: TfrxReport;
    CGResumido: TdmkCheckGroup;
    QrDebitos: TmySQLQuery;
    QrDebitosDebito: TFloatField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    frxDsDebitos: TfrxDBDataset;
    CkPaginar: TdmkCheckBox;
    TPData: TdmkEditDateTimePicker;
    Label2: TLabel;
    CGTipoDoc: TdmkCheckGroup;
    frxBal_A_16: TfrxReport;
    Qr16: TmySQLQuery;
    TabSheet3: TTabSheet;
    DBGrid6: TDBGrid;
    Ds16: TDataSource;
    frxDs16: TfrxDBDataset;
    Qr16NO_CART: TWideStringField;
    Qr16Carteira: TIntegerField;
    Qr16SerieCH: TWideStringField;
    Qr16Documento: TFloatField;
    Qr16Debito: TFloatField;
    Qr16Qtde: TFloatField;
    Qr16Descricao: TWideStringField;
    Qr16Data: TDateField;
    Qr16Genero: TIntegerField;
    Qr16NO_GENERO: TWideStringField;
    Qr16NotaFiscal: TIntegerField;
    Qr16Vencimento: TDateField;
    Qr16Compensado: TDateField;
    Qr16Controle: TIntegerField;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    EdDtEncer: TDBEdit;
    Label5: TLabel;
    EdDtMorto: TDBEdit;
    frxDsPgBloq: TfrxDBDataset;
    frxDsSuBloq: TfrxDBDataset;
    frxDsPrevItO: TfrxDBDataset;
    frxDsPendG: TfrxDBDataset;
    frxDsRep: TfrxDBDataset;
    frxDsRepBPP: TfrxDBDataset;
    frxDsRepLan: TfrxDBDataset;
    frxDsRepAbe: TfrxDBDataset;
    QrDebitosGenero: TIntegerField;
    QrPendMPE: TmySQLQuery;
    frxDsPendMPE: TfrxDBDataset;
    QrPendMPENome: TWideStringField;
    QrPendMPEConta: TIntegerField;
    QrPendMPEDevido: TFloatField;
    QrPendMPEPago: TFloatField;
    QrPendMPESALDO: TFloatField;
    frxDsTotalMPE: TfrxDBDataset;
    QrTotalMPE: TmySQLQuery;
    QrTotalMPESALDO: TFloatField;
    PnListaCompl: TPanel;
    Panel11: TPanel;
    DBGLista: TdmkDBGridDAC;
    RGOpcoesListas: TRadioGroup;
    QrListaFonteTam: TIntegerField;
    QrListaRelItem: TIntegerField;
    BtReordena: TBitBtn;
    frxBal_A_05: TfrxReport;
    QrEmp: TmySQLQuery;
    QrEmpCodigo: TIntegerField;
    QrEmpNO_UF: TWideStringField;
    QrEmpNO_EMPRESA: TWideStringField;
    QrEmpCIDADE: TWideStringField;
    QrEmpCO_SHOW: TLargeintField;
    QrEmpCliInt: TIntegerField;
    QrEntiCfgRel_01CpaLin1FonNom: TWideStringField;
    QrEntiCfgRel_01CpaLin1FonTam: TSmallintField;
    QrEntiCfgRel_01CpaLin1MrgSup: TIntegerField;
    QrEntiCfgRel_01CpaLin1AltLin: TIntegerField;
    QrEntiCfgRel_01CpaLin1AliTex: TSmallintField;
    QrEntiCfgRel_01CpaLin2FonNom: TWideStringField;
    QrEntiCfgRel_01CpaLin2FonTam: TSmallintField;
    QrEntiCfgRel_01CpaLin2MrgSup: TIntegerField;
    QrEntiCfgRel_01CpaLin2AltLin: TIntegerField;
    QrEntiCfgRel_01CpaLin2AliTex: TSmallintField;
    QrEntiCfgRel_01CpaLin3FonNom: TWideStringField;
    QrEntiCfgRel_01CpaLin3FonTam: TSmallintField;
    QrEntiCfgRel_01CpaLin3MrgSup: TIntegerField;
    QrEntiCfgRel_01CpaLin3AltLin: TIntegerField;
    QrEntiCfgRel_01CpaLin3AliTex: TSmallintField;
    QrEntiCfgRel_01CpaImgMrgSup: TIntegerField;
    QrEntiCfgRel_01CpaImgMrgEsq: TIntegerField;
    QrEntiCfgRel_01CpaImgAlt: TIntegerField;
    QrEntiCfgRel_01CpaImgLar: TIntegerField;
    QrEntiCfgRel_01CpaImgStre: TSmallintField;
    QrEntiCfgRel_01CpaImgProp: TSmallintField;
    QrEntiCfgRel_01CpaImgTran: TSmallintField;
    QrEntiCfgRel_01CpaImgTranCol: TIntegerField;
    frxPDFExport: TfrxPDFExport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel10: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSdoIni: TBitBtn;
    BitBtn2: TBitBtn;
    Label3: TLabel;
    EdTempo: TdmkEdit;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    BtConfigura: TBitBtn;
    CkSemMilhar: TCheckBox;
    CkResMes43: TCheckBox;
    RG10HowShowA: TRadioGroup;
    frxDsEmp: TfrxDBDataset;
    RGTabelaChart: TRadioGroup;
    QrPendMPEKGT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrCtasResMesCalcFields(DataSet: TDataSet);
    procedure QrExclusivosCalcFields(DataSet: TDataSet);
    procedure QrOrdinariosCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrDat1CalcFields(DataSet: TDataSet);
    procedure QrDat2CalcFields(DataSet: TDataSet);
    procedure BtSdoIniClick(Sender: TObject);
    procedure RGFormatoClick(Sender: TObject);
    procedure RGOpcoesListasClick(Sender: TObject);
    procedure BtReordenaClick(Sender: TObject);
    procedure QrListaBeforeClose(DataSet: TDataSet);
    procedure QrListaAfterOpen(DataSet: TDataSet);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure frxFIN_BALAN_001_001GetValue(const VarName: string;
      var Value: Variant);
    procedure frxFIN_BALAN_001_002GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    VAR_REL_GEREN: Integer;
    FDataI, FDataF: String;
    FAtzContasMensais2: Boolean ;
    FCfgRel: String;
    FPeriodoAtu, FPeriodoAnt, FPeriodoIni, FPeriodoFim: Integer;
    FArre1, FArre2, FDataAnt, FDataIni, FDataBal, FDataAtu: String;
    FResumido_01, FResumido_02, FResumido_03,
    FResumido_04, FResumido_05, FResumido_06, FResumido_07: Boolean;
    FIni, FFim, FTempo: TDateTime;
    //function PeriodoNaoDefinido(CliInt, Periodo: Integer): Boolean;
    function frxComposite(frxAtu: TfrxReport; var ClearLastReport: Boolean):
             Boolean;
    //procedure PreparaBal_A_03();
    //procedure PreparaBal_A_04();
    procedure PreparaBal_A_03();
    procedure PreparaBal_A_04();
    procedure AtualizaItensAImprimir();
    procedure ImprimeReduzido(Mez: Integer; Exporta: Boolean);
    procedure ImprimeCompleto(Mez, PBB: Integer; Exporta: Boolean);
    function  ChCompensadosPeriodo(Empresa: Integer): Boolean;
    procedure ReopenExclusivos(Mez: Integer);
    procedure ImprimeBalancete(Exporta: Boolean);
    procedure ExportaBalancete(Frx: TfrxReport; PrepRep: Boolean);
    procedure frxReport000GetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
  public
    { Public declarations }
    //FPBB: Integer;
    FConfiguInicial, FPaginar: Boolean;
    FEntidade, FCliInt,
    FClienteInicial,
    FPeriodoInicial: Integer;
    FTabAriA, FTabCnsA, FTabPriA, FTabPrvA,
    FEnt_Txt, FTabLctA, FTabLctB, FTabLctD(*, FTabLctX*): String;
  end;

  var
  FmCashBal: TFmCashBal;

implementation

uses UnInternalConsts, UnFinanceiro, ModuleGeral, ModuleFin, Module, UCreate,
  UnMyObjects, MeuFrx, Resmes, (*ContasHistSdo3,*) (*CashBalIni,*) MyDBCheck,
  UMySQLModule, {$IfDef CO_DMKID_APP_0004} Principal, {$EndIf} MyListas,
  EntiCfgRel, UnReordena;

const
  FCaminhoEMail = CO_DIR_RAIZ_DMK + '\FTP\Balancetes\';

{$R *.DFM}

procedure TFmCashBal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCashBal.BtTudoClick(Sender: TObject);
begin
  //CGItensImp.Value :=  M L A G e r a l.MaxValueCheckGroup(CGItensImp.Items.Count);
  case RGFormato.ItemIndex of
    //0: DBGLista.AtivaTodos;   Parei aqui Alexandria
    1: CGResumido.SetMaxValue;
  end;
end;

function TFmCashBal.ChCompensadosPeriodo(Empresa: Integer): Boolean;

  procedure GeraParteSQL(TabLct, TiposDoc, Emp_TXT: String);
  begin
    Qr16.SQL.Add('SELECT car.Nome NO_CART, lan.Carteira, lan.SerieCH, ');
    Qr16.SQL.Add('lan.Documento,lan.Debito, lan.Qtde, lan.Descricao, lan.Data, ');
    Qr16.SQL.Add('lan.Genero,cta.Nome NO_GENERO, lan.Controle, lan.NotaFiscal,');
    Qr16.SQL.Add('lan.Vencimento, lan.Compensado, car.TipoDoc');
    Qr16.SQL.Add('FROM ' + TabLCT + ' lan');
    Qr16.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    Qr16.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=lan.Genero');
    Qr16.SQL.Add('WHERE car.Tipo=2');
    Qr16.SQL.Add('AND lan.Debito > 0');
    Qr16.SQL.Add('AND car.TipoDoc IN ('+TiposDoc+')');
    Qr16.SQL.Add('AND car.ForneceI=' + Emp_TXT);
    Qr16.SQL.Add('AND lan.Compensado BETWEEN "' + FDataI + '" AND "' + FDataF + '"');
  end;

var
  TiposDoc, Emp_TXT: String;
begin
  Result   := False;
  TiposDoc := CGTipoDoc.GetIndexesChecked(True, 0);
  //
  if TiposDoc = '' then
  begin
    Geral.MB_Aviso('Nenhum tipo de documento foi definido'+sLineBreak+
      'O relat�rio de documentos compensados no per�odo n�o ser� gerado!');
    Exit;
  end;
  //
  Emp_TXT := Geral.FF0(Empresa);
  //
  Screen.Cursor := crHourGlass;
  try
    Qr16.Close;
    Qr16.SQL.Clear;
    //
    Qr16.Close;
    Qr16.SQL.Clear;
    Qr16.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_001_16;');
    Qr16.SQL.Add('CREATE TABLE _FIN_BALAN_001_16 IGNORE ');
    Qr16.SQL.Add('');
    GeraParteSQL(FTabLctA, TiposDoc, Emp_TXT);
    Qr16.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, TiposDoc, Emp_TXT);
    Qr16.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, TiposDoc, Emp_TXT);
    Qr16.SQL.Add(';');
    Qr16.SQL.Add('');
    Qr16.SQL.Add('SELECT * FROM _FIN_BALAN_001_16');
    Qr16.SQL.Add('ORDER BY TipoDoc, NO_CART, Carteira, SerieCH, Documento;');
    Qr16.SQL.Add('');
    Qr16.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_001_16;');
    Qr16.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(Qr16, DModG.MyPID_DB);
    //
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashBal.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    AtualizaItensAImprimir();
end;

procedure TFmCashBal.EdEmpresaExit(Sender: TObject);
begin
  AtualizaItensAImprimir();
end;

procedure TFmCashBal.FormActivate(Sender: TObject);
var
  Mes, Ano: Word;
begin
  MyObjects.CorIniComponente();
  //
  if not FConfiguInicial then
  begin
    FConfiguInicial := True;
    //
    if FClienteInicial <> 0 then
    begin
      if DModG.QrEmpresas.Locate('Codigo', FClienteInicial, []) then
      begin
        EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
        CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
      end;
    end;
    if FPeriodoInicial > 0 then
    begin
      dmkPF.PeriodoDecode(FPeriodoInicial, Ano, Mes);
      //
      CBAno.Text      := Geral.FF0(Ano);
      CBMes.ItemIndex := Mes - 1;
    end;
  end;
end;

procedure TFmCashBal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCashBal.PreparaBal_A_03();

  procedure IncluiLinhaSQLAnoAtu(CliCod: Integer; CliNom, SQL: String; ValMeses: Array of Double);
  var
    I: Integer;
    Vals: String;
    Tot, Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    Tot  := 0;
    //
    for I := 0 to 11 do
    begin
      Vals := Vals + dmkPF.FFP(ValMeses[I], 2) + ',';
      Tot  := Tot + ValMeses[I];
    end;
    //
    Mid  := Tot / (CBMes.ItemIndex + 1);
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    //
    if QrAnt1.Locate('Cliente', CliCod, []) then
      Tot := QrAnt1Valor.Value
    else
      Tot := 0;
    //
    Mid  := Tot / 12;
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat1.SQL.Add(SQL + Vals);
  end;

  procedure IncluiLinhaSQLAnoAnt(CliCod: Integer; CliNom, SQL: String; ValAnt: Double);
  var
    I: Integer;
    Vals: String;
    Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    //
    for I := 0 to 13 do
      Vals := Vals + dmkPF.FFP(0, 2) + ',';
    //
    Mid  := ValAnt / 12;
    Vals := Vals + dmkPF.FFP(ValAnt, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat1.SQL.Add(SQL + Vals);
  end;

var
  ValMeses: array[0..11] of Double;
  I, Cli: Integer;
  Gen_Txt, Vals, Nom, SQL: String;
begin
  MyObjects.Informa(LaSub1, True, 'Abrindo tabelas de consulta');
  //
  FArre1 := UCriar.RecriaTempTableNovo(ntrttArre, DmodG.QrUpdPID1, False, 0, 'Arre1');
  //
  QrDat1.Close;
  QrDat1.SQL.Clear;
  //
  QrAnt1.Close;
  QrAnt1.SQL.Clear;
  //
  Gen_Txt := Geral.FF0(QrEntiCfgRel_01Genero.Value);
  //
  QrAnt1.SQL.Add('DROP TABLE IF EXISTS _ANT1_GEN_;');
  QrAnt1.SQL.Add('');
  QrAnt1.SQL.Add('CREATE TABLE _ANT1_GEN_ IGNORE ');
  QrAnt1.SQL.Add('');
  QrAnt1.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT,');
  QrAnt1.SQL.Add('lct.Cliente, SUM(lct.Credito) Valor');
  QrAnt1.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrAnt1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrAnt1.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrAnt1.SQL.Add('AND lct.Data >= "' + FDataAnt + '"');
  QrAnt1.SQL.Add('AND lct.Data < "' + FDataIni + '"');
  QrAnt1.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrAnt1.SQL.Add('GROUP BY lct.Cliente');
  QrAnt1.SQL.Add('UNION');
  QrAnt1.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT,');
  QrAnt1.SQL.Add('lct.Cliente, SUM(lct.Credito) Valor');
  QrAnt1.SQL.Add('FROM ' + FTabLctB + ' lct');
  QrAnt1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrAnt1.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrAnt1.SQL.Add('AND lct.Data >= "' + FDataAnt + '"');
  QrAnt1.SQL.Add('AND lct.Data < "' + FDataIni + '"');
  QrAnt1.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrAnt1.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  QrAnt1.SQL.Add('GROUP BY lct.Cliente');
  QrAnt1.SQL.Add('UNION');
  QrAnt1.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT,');
  QrAnt1.SQL.Add('lct.Cliente, SUM(lct.Credito) Valor');
  QrAnt1.SQL.Add('FROM ' + FTabLctD + ' lct');
  QrAnt1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrAnt1.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrAnt1.SQL.Add('AND lct.Data >= "' + FDataAnt + '"');
  QrAnt1.SQL.Add('AND lct.Data < "' + FDataIni + '"');
  QrAnt1.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrAnt1.SQL.Add('GROUP BY lct.Cliente');
  QrAnt1.SQL.Add(';');
  QrAnt1.SQL.Add('SELECT NO_ENT, Cliente, SUM(Valor) VALOR');
  QrAnt1.SQL.Add('FROM _ANT1_GEN_');
  QrAnt1.SQL.Add('GROUP BY Cliente;');
  QrAnt1.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrAnt1, DModG.MyPID_DB);
  //
  QrArr1.Close;
  QrArr1.SQL.Clear;
  QrArr1.SQL.Add('DROP TABLE IF EXISTS _ARR1_GEN_;');
  QrArr1.SQL.Add('');
  QrArr1.SQL.Add('CREATE TABLE _ARR1_GEN_ IGNORE ');
  QrArr1.SQL.Add('');
  QrArr1.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  QrArr1.SQL.Add('NO_ENT, lct.Cliente,');
  QrArr1.SQL.Add('YEAR(lct.Data) ANO, MONTH(lct.Data) MES,');
  QrArr1.SQL.Add('SUM(lct.Credito) Valor');
  QrArr1.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrArr1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrArr1.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrArr1.SQL.Add('AND lct.Data BETWEEN "' + FDataIni + '" AND "' + FDataAtu + '"');
  QrArr1.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrArr1.SQL.Add('GROUP BY lct.Cliente, ANO, MES');
  QrArr1.SQL.Add('UNION');
  QrArr1.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  QrArr1.SQL.Add('NO_ENT, lct.Cliente,');
  QrArr1.SQL.Add('YEAR(lct.Data) ANO, MONTH(lct.Data) MES,');
  QrArr1.SQL.Add('SUM(lct.Credito) Valor');
  QrArr1.SQL.Add('FROM ' + FTabLctB + ' lct');
  QrArr1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrArr1.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrArr1.SQL.Add('AND lct.Data BETWEEN "' + FDataIni + '" AND "' + FDataAtu + '"');
  QrArr1.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrArr1.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  QrArr1.SQL.Add('GROUP BY lct.Cliente, ANO, MES');
  QrArr1.SQL.Add('UNION');
  QrArr1.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  QrArr1.SQL.Add('NO_ENT, lct.Cliente,');
  QrArr1.SQL.Add('YEAR(lct.Data) ANO, MONTH(lct.Data) MES,');
  QrArr1.SQL.Add('SUM(lct.Credito) Valor');
  QrArr1.SQL.Add('FROM ' + FTabLctD + ' lct');
  QrArr1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrArr1.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrArr1.SQL.Add('AND lct.Data BETWEEN "' + FDataIni + '" AND "' + FDataAtu + '"');
  QrArr1.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrArr1.SQL.Add('GROUP BY lct.Cliente, ANO, MES');
  QrArr1.SQL.Add(';');
  QrArr1.SQL.Add('SELECT NO_ENT, Cliente, ANO, MES, SUM(Valor) VALOR');
  QrArr1.SQL.Add('FROM _ARR1_GEN_');
  QrArr1.SQL.Add('GROUP BY Cliente, ANO, MES');
  QrArr1.SQL.Add('ORDER BY Cliente, ANO, MES');
  QrArr1.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrArr1, DModG.MyPID_DB);
  //
  SQL := 'INSERT INTO arre1 (Cliente,NO_ENT,Valr01,Valr02,Valr03,Valr04,' +
           'Valr05,Valr06,Valr07,Valr08,Valr09,Valr10,Valr11,Valr12,ValrA1,ValrM1,' +
           'ValrA0,ValrM0,Ativo) Values(';
  //
  QrArr1.First;
  //
  Vals := '';
  Cli  := QrArr1Cliente.Value;
  Nom  := QrArr1NO_ENT.Value;
  //
  for I := 0 to 11 do
    ValMeses[I] := 0;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo selecionado');
  //
  PB1.Position := 0;
  PB1.Max      := QrArr1.RecordCount;
  //
  while not QrArr1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    //
    MyObjects.Informa(LaSub2, True, QrArr1NO_ENT.Value);
    //
    if Cli <> QrArr1Cliente.Value then
    begin
      if Cli > -100000 then
      begin
        IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
        //
        Cli := QrArr1Cliente.Value;
        Nom := QrArr1NO_ENT.Value;
      end;
      for I := 0 to 11 do
        ValMeses[I] := 0;
    end;
    ValMeses[QrArr1MES.Value-1] := QrArr1Valor.Value;
    //
    QrArr1.Next;
  end;
  IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
  //
  // Entidades que tem movimento no ano anterior mas n�o no atual!!
  for I := 0 to 11 do
    ValMeses[I] := 0;
  //
  QrAnt1.First;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo anterior');
  //
  PB1.Position := 0;
  PB1.Max      := QrAnt1.RecordCount;
  //
  while not QrAnt1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    //
    MyObjects.Informa(LaSub2, True, QrAnt1NO_ENT.Value);
    //
    Cli := QrAnt1Cliente.Value;
    Nom := QrAnt1NO_ENT.Value;
    //
    if not QrArr1.Locate('Cliente', Cli, []) then
      IncluiLinhaSQLAnoAnt(Cli, Nom, SQL, QrAnt1Valor.Value);
    //
    QrAnt1.Next;
  end;
  //
  MyObjects.Informa(LaSub1, True, 'Abrindo tabela com resultados');
  //
  QrDat1.ExecSQL;
  //
  QrDat1.SQL.Clear;
  QrDat1.SQL.Add('SELECT * FROM arre1 ');
  QrDat1.SQL.Add('ORDER BY NO_ENT; ');
  UnDmkDAC_PF.AbreQuery(QrDat1, DModG.MyPID_DB);
  //
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  //
  PB1.Position := 0;
end;

procedure TFmCashBal.PreparaBal_A_04();

  procedure IncluiLinhaSQLAnoAtu(CliCod: Integer; CliNom, SQL: String; ValMeses: Array of Double);
  var
    I: Integer;
    Vals: String;
    Tot, Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    Tot  := 0;
    //
    for I := 0 to 11 do
    begin
      Vals := Vals + dmkPF.FFP(ValMeses[I], 2) + ',';
      Tot  := Tot + ValMeses[I];
    end;
    //
    Mid  := Tot / (CBMes.ItemIndex + 1);
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    //
    if QrAnt2.Locate('Cliente', CliCod, []) then
      Tot := QrAnt2Valor.Value
    else
      Tot := 0;
    //
    Mid  := Tot / 12;
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat2.SQL.Add(SQL + Vals);
  end;

  procedure IncluiLinhaSQLAnoAnt(CliCod: Integer; CliNom, SQL: String; ValAnt: Double);
  var
    I: Integer;
    Vals: String;
    Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    //
    for I := 0 to 13 do
      Vals := Vals + dmkPF.FFP(0, 2) + ',';
    //
    Mid  := ValAnt / 12;
    Vals := Vals + dmkPF.FFP(ValAnt, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat2.SQL.Add(SQL + Vals);
  end;

var
  ValMeses: array[0..11] of Double;
  I, Cli, Mes: Integer;
  PerAntTxt, PerIniTxt, PerFimTxt, Gen_Txt, Vals, Nom, SQL: String;

begin
  MyObjects.Informa(LaSub1, True, 'Abrindo tabelas de consulta');
  //
  FArre2 := UCriar.RecriaTempTableNovo(ntrttArre, DmodG.QrUpdPID1, False, 0, 'Arre2');
  //
  QrDat2.Close;
  QrDat2.SQL.Clear;
  //
  Gen_Txt   := Geral.FF0(QrEntiCfgRel_01Genero.Value);
  PerAntTxt := Geral.FF0(FPeriodoAnt);
  PerIniTxt := Geral.FF0(FPeriodoIni);
  PerFimTxt := Geral.FF0(FPeriodoFim);
  //
  QrAnt2.Close;
  QrAnt2.SQL.Clear;
  QrAnt2.SQL.Add('DROP TABLE IF EXISTS _ANT2_GEN_;');
  QrAnt2.SQL.Add('');
  QrAnt2.SQL.Add('CREATE TABLE _ANT2_GEN_ IGNORE ');
  QrAnt2.SQL.Add('');
  QrAnt2.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT,');
  QrAnt2.SQL.Add('lct.Cliente,  SUM(lct.Credito) Valor');
  QrAnt2.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrAnt2.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrAnt2.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrAnt2.SQL.Add('AND lct.Mez BETWEEN ' + PerAntTxt + ' AND ' + PerIniTxt);
  QrAnt2.SQL.Add('AND lct.Data <= "' + FDataAtu + '"');
  QrAnt2.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrAnt2.SQL.Add('GROUP BY lct.Cliente');
  QrAnt2.SQL.Add('UNION');
  QrAnt2.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT,');
  QrAnt2.SQL.Add('lct.Cliente,  SUM(lct.Credito) Valor');
  QrAnt2.SQL.Add('FROM ' + FTabLctB + ' lct');
  QrAnt2.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrAnt2.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrAnt2.SQL.Add('AND lct.Mez BETWEEN ' + PerAntTxt + ' AND ' + PerIniTxt);
  QrAnt2.SQL.Add('AND lct.Data <= "' + FDataAtu + '"');
  QrAnt2.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrAnt2.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  QrAnt2.SQL.Add('GROUP BY lct.Cliente');
  QrAnt2.SQL.Add('UNION');
  QrAnt2.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT,');
  QrAnt2.SQL.Add('lct.Cliente,  SUM(lct.Credito) Valor');
  QrAnt2.SQL.Add('FROM ' + FTabLctD + ' lct');
  QrAnt2.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrAnt2.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrAnt2.SQL.Add('AND lct.Mez BETWEEN ' + PerAntTxt + ' AND ' + PerIniTxt);
  QrAnt2.SQL.Add('AND lct.Data <= "' + FDataAtu + '"');
  QrAnt2.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrAnt2.SQL.Add('GROUP BY lct.Cliente');
  QrAnt2.SQL.Add(';');
  QrAnt2.SQL.Add('SELECT NO_ENT, Cliente, SUM(Valor) VALOR');
  QrAnt2.SQL.Add('FROM _ANT2_GEN_');
  QrAnt2.SQL.Add('GROUP BY Cliente;');
  QrAnt2.SQL.Add('');
  // M L A G e r a l .LeMeuSQLy(QrAnt2, '', nil, True, True);
  UnDmkDAC_PF.AbreQuery(QrAnt2, DModG.MyPID_DB);
  //
  QrArr2.Close;
  QrArr2.SQL.Clear;
  QrArr2.SQL.Add('DROP TABLE IF EXISTS _ARR2_GEN_;');
  QrArr2.SQL.Add('');
  QrArr2.SQL.Add('CREATE TABLE _ARR2_GEN_ IGNORE ');
  QrArr2.SQL.Add('');
  QrArr2.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  QrArr2.SQL.Add('NO_ENT, lct.Cliente, Mez, SUM(lct.Credito) Valor');
  QrArr2.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrArr2.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrArr2.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrArr2.SQL.Add('AND lct.Mez BETWEEN ' + PerIniTxt + ' AND ' + PerFimTxt);
  QrArr2.SQL.Add('AND lct.Data <= "' + FDataAtu + '"');
  QrArr2.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrArr2.SQL.Add('GROUP BY lct.Cliente, Mez');
  QrArr2.SQL.Add('UNION');
  QrArr2.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  QrArr2.SQL.Add('NO_ENT, lct.Cliente, Mez, SUM(lct.Credito) Valor');
  QrArr2.SQL.Add('FROM ' + FTabLctB + ' lct');
  QrArr2.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrArr2.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrArr2.SQL.Add('AND lct.Mez BETWEEN ' + PerIniTxt + ' AND ' + PerFimTxt);
  QrArr2.SQL.Add('AND lct.Data <= "' + FDataAtu + '"');
  QrArr2.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrArr2.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  QrArr2.SQL.Add('GROUP BY lct.Cliente, Mez');
  QrArr2.SQL.Add('UNION');
  QrArr2.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)');
  QrArr2.SQL.Add('NO_ENT, lct.Cliente, Mez, SUM(lct.Credito) Valor');
  QrArr2.SQL.Add('FROM ' + FTabLctD + ' lct');
  QrArr2.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lct.Cliente');
  QrArr2.SQL.Add('WHERE lct.Genero=' + Gen_Txt);
  QrArr2.SQL.Add('AND lct.Mez BETWEEN ' + PerIniTxt + ' AND ' + PerFimTxt);
  QrArr2.SQL.Add('AND lct.Data <= "' + FDataAtu + '"');
  QrArr2.SQL.Add('AND lct.CliInt=' + FEnt_Txt);
  QrArr2.SQL.Add('GROUP BY lct.Cliente, Mez');
  QrArr2.SQL.Add(';');
  QrArr2.SQL.Add('SELECT NO_ENT, Cliente, MEZ, SUM(Valor) VALOR');
  QrArr2.SQL.Add('FROM _ARR2_GEN_');
  QrArr2.SQL.Add('GROUP BY Cliente, MEZ');
  QrArr2.SQL.Add('ORDER BY Cliente, MEZ');
  //  M L A G e r a  l.LeMeuSQLy(QrArr2, '', nil, True, True);
  UnDmkDAC_PF.AbreQuery(QrArr2, DModG.MyPID_DB);
  //
  SQL := 'INSERT INTO arre2 (Cliente,NO_ENT,Valr01,Valr02,Valr03,Valr04,' +
           'Valr05,Valr06,Valr07,Valr08,Valr09,Valr10,Valr11,Valr12,ValrA1,ValrM1,' +
           'ValrA0,ValrM0,Ativo) Values(';
  //
  QrArr2.First;
  //
  Vals := '';
  Cli  := QrArr2Cliente.Value;
  Nom  := QrArr2NO_ENT.Value;
  //
  for I := 0 to 11 do
    ValMeses[I] := 0;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo selecionado');
  //
  PB1.Position := 0;
  PB1.Max      := QrArr2.RecordCount;
  //
  while not QrArr2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    //
    MyObjects.Informa(LaSub2, True, QrArr2NO_ENT.Value);
    //
    if Cli <> QrArr2Cliente.Value then
    begin
      if Cli > -100000 then
      begin
        IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
        //
        Cli := QrArr2Cliente.Value;
        Nom := QrArr2NO_ENT.Value;
      end;
      for I := 0 to 11 do
        ValMeses[I] := 0;
    end;
    Mes             := QrArr2Mez.Value mod 100;
    ValMeses[Mes-1] := QrArr2Valor.Value;
    //
    QrArr2.Next;
  end;
  IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
  //
  // Entidades que tem movimento no ano anterior mas n�o no atual!!
  for I := 0 to 11 do
    ValMeses[I] := 0;
  //
  QrAnt2.First;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo anterior');
  //
  PB1.Position := 0;
  PB1.Max      := QrAnt2.RecordCount;
  //
  while not QrAnt2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    //
    MyObjects.Informa(LaSub2, True, QrAnt2NO_ENT.Value);
    //
    Cli := QrAnt2Cliente.Value;
    Nom := QrAnt2NO_ENT.Value;
    //
    if not QrArr2.Locate('Cliente', Cli, []) then
      IncluiLinhaSQLAnoAnt(Cli, Nom, SQL, QrAnt2Valor.Value);
    //
    QrAnt2.Next;
  end;
  //
  MyObjects.Informa(LaSub1, True, 'Abrindo tabela com resultados');
  //
  if QrDat2.SQL.Text <> '' then
    QrDat2.ExecSQL;
  //
  QrDat2.SQL.Clear;
  QrDat2.SQL.Add('SELECT * FROM arre2 ');
  QrDat2.SQL.Add('ORDER BY NO_ENT; ');
  UnDmkDAC_PF.AbreQuery(QrDat2, DModG.MyPID_DB);
  //
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  //
  PB1.Position := 0;
end;

procedure TFmCashBal.QrCtasResMesCalcFields(DataSet: TDataSet);
begin
  QrCtasResMesNOME_PERIODO.Value := dmkPF.MesEAnoDoPeriodo(QrCtasResMesPeriodo.Value);
end;

procedure TFmCashBal.QrDat1CalcFields(DataSet: TDataSet);
begin
  QrDat1SEQ.Value := QrDat1.RecNo;
end;

procedure TFmCashBal.QrDat2CalcFields(DataSet: TDataSet);
begin
  QrDat2SEQ.Value := QrDat2.RecNo;
end;

procedure TFmCashBal.QrExclusivosCalcFields(DataSet: TDataSet);
begin
  QrExclusivosCREDITO.Value  := QrExclMovCredito.Value;
  QrExclusivosDEBITO.Value   := QrExclMovDebito.Value;
  QrExclusivosANTERIOR.Value := QrExclusivosMovim.Value -
                                  QrExclMovDebito.Value +
                                  QrExclMovCredito.Value;
end;

procedure TFmCashBal.QrListaAfterOpen(DataSet: TDataSet);
begin
  BtReordena.Enabled := QrLista.RecordCount > 0;
end;

procedure TFmCashBal.QrListaBeforeClose(DataSet: TDataSet);
begin
  BtReordena.Enabled := False;
end;

procedure TFmCashBal.QrOrdinariosCalcFields(DataSet: TDataSet);
begin
  QrOrdinariosCREDITO.Value  := QrOrdiMovCredito.Value;
  QrOrdinariosDEBITO.Value   := QrOrdiMovDebito.Value;
  QrOrdinariosANTERIOR.Value := QrOrdinariosMovim.Value +
                                  QrOrdiMovDebito.Value -
                                  QrOrdiMovCredito.Value;
end;

procedure TFmCashBal.RGOpcoesListasClick(Sender: TObject);
begin
  AtualizaItensAImprimir();
end;

procedure TFmCashBal.ReopenExclusivos(Mez: Integer);
begin
  QrExclMov.Close;
  QrExclMov.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
  QrExclMov.Params[01].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrExclMov, Dmod.MyDB);
  //
  QrExclusivos.Close;
  QrExclusivos.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
  QrExclusivos.Params[01].AsInteger := Mez;
  UnDmkDAC_PF.AbreQuery(QrExclusivos, Dmod.MyDB);
end;

procedure TFmCashBal.RGFormatoClick(Sender: TObject);
begin
  PnListaCompl.Visible := False;
  CGResumido.Visible   := False;
  //
  case RGFormato.ItemIndex of
    0: PnListaCompl.Visible := True;
    1: CGResumido.Visible   := True;
  end;
end;

procedure TFmCashBal.FormCreate(Sender: TObject);
var
  I: Integer;
  N: TfrxDatasetItem;
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.frxConfiguraPDF(frxPDFExport);
  DmodFin.CarregaItensTipoDoc(CGTipoDoc);
  //
  CGTipoDoc.Value  := 2;
  CGResumido.Align := alClient;
  //
  CGResumido.SetMaxValue;
  //
  FCfgRel         := UCriar.RecriaTempTableNovo(ntrttCfgRel, DmodG.QrUpdPID1, False, 0, 'CfgRel');
  FClienteInicial := 0;
  FPeriodoInicial := -1000;
  FConfiguInicial := False;
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa(LaAviso, False, '...');
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  MyObjects.Informa(LaSub3, False, '...');
  //
  QrArr1.Database       := DModG.MyPID_DB;
  QrAnt1.Database       := DModG.MyPID_DB;
  QrArr2.Database       := DModG.MyPID_DB;
  QrAnt2.Database       := DModG.MyPID_DB;
  QrDat1.Database       := DModG.MyPID_DB;
  QrDat2.Database       := DModG.MyPID_DB;
  QrSSdo.Database       := DModG.MyPID_DB;
  QrSN1.Database        := DModG.MyPID_DB;
  QrSN2.Database        := DModG.MyPID_DB;
  QrSels.Database       := DModG.MyPID_DB;
  QrLista.Database      := DModG.MyPID_DB;
  QrCtasResMes.Database := DModG.MyPID_DB;
  Qr16.Database         := DModG.MyPID_DB;
  QrDebitos.Database    := DModG.MyPID_DB;
  QrPendMPE.Database    := DModG.MyPID_DB;
  QrTotalMPE.Database   := DModG.MyPID_DB;
  //
  //  ? ? ?
  //  DmodFin.QrExtratos.Database := DModG.MyPID_DB;
  //
  TPData.Date := Date;
  EdDtEncer.DataSource := DModG.DsLastEncer;
  EdDtMorto.DataSource := DModG.DsLastMorto;
  //
  frxDsPrevItO.DataSet := DModFin.QrPrevItO;
  frxDsPgBloq.DataSet  := DModFin.QrPgBloq;
  frxDsSuBloq.DataSet  := DModFin.QrSuBloq;
  frxDsPendG.DataSet   := DModFin.QrPendG;
  frxDsRep.DataSet     := DModFin.QrRep;
  frxDsRepAbe.DataSet  := DModFin.QrRepAbe;
  frxDsRepBPP.DataSet  := DModFin.QrRepBPP;
  frxDsRepLan.DataSet  := DModFin.QrRepLan;
  //
  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TfrxReport then
    begin
      N := TfrxReport(Components[I]).DataSets.Find(DmodG.frxDsDono);
      if N = nil then
        TfrxReport(Components[I]).DataSets.Add(DmodG.frxDsDono);
      TfrxReport(Components[I]).EnabledDataSets.Add(DmodG.frxDsDono);
    end;
  end;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmCashBal.AtualizaItensAImprimir();
var
  //I,
  Entidade, Empresa: Integer;
  //X: String;
  //Lista: TStringList;
begin
  Empresa  := EdEmpresa.ValueVariant;
  Entidade := DModG.QrEmpresasCodigo.Value;
  //
  if Empresa = 0 then
    Exit;
  //
  case RGOpcoesListas.ItemIndex of
    0:
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DmodG.MyPID_DB, [
        'DROP TABLE IF EXISTS cfgrel; ',
        'CREATE TABLE cfgrel IGNORE ',
        'SELECT Controle, RelTitu, Genero, ',
        'Ordem, FonteTam, RelItem, Ativo ',
        'FROM ' + TMeuDB + '.enticfgrel ',
        'WHERE RelTipo=1 ',
        'AND Codigo=' + Geral.FF0(Entidade) + '; ',
        '']);
    end;
    1:
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DmodG.MyPID_DB, [
        'DROP TABLE IF EXISTS cfgrel; ',
        'CREATE TABLE cfgrel IGNORE ',
        'SELECT Controle, RelTitu, Genero, ',
        'Ordem, FonteTam, RelItem, Ativo ',
        'FROM ' + TMeuDB + '.enticfgrel ',
        'WHERE RelTipo=1 ',
        'AND Codigo=0; ', // Codigo 0: Relat�rios indicados para o aplicativo
        '']);
    end;
    2:
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DmodG.MyPID_DB, [
        'DROP TABLE IF EXISTS cfgrel; ',
        'CREATE TABLE cfgrel IGNORE ',
        'SELECT Controle, RelTitu, Genero, ',
        'Ordem, FonteTam, RelItem, Ativo ',
        'FROM ' + TMeuDB + '.enticfgrel ',
        'WHERE RelTipo=1 ',
        'AND Codigo=-1; ', // Codigo -1: Todos relat�rios
        '']);
      (*
      Lista := TStringList.Create;
      try
        DModG.ListaDeItensDeBalancete(tcrBalanceteLoc, False, Lista);
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('DELETE FROM cfgrel;');
        for I := 0 to Lista.Count - 1 do
        begin
          X := Geral.FF0(I+1);
          DModG.QrUpdPID1.SQL.Add('INSERT INTO cfgrel SET Controle=' + X +
          ', RelTitu="' + Lista[I] + '", Genero=0, Ordem=' + X +
          ', FonteTam=1, RelItem=' + X + ', Ativo=1;');
        end;
        //
        DModG.QrUpdPID1.ExecSQL;
      finally
        Lista.Free;
      end;
      *)
   end;
  end;
  QrLista.Close;
  UnDmkDAC_PF.AbreQuery(QrLista, DModG.MyPID_DB);
  //
  BtSdoIni.Enabled := (EdEmpresa.ValueVariant <> 0) and (Entidade <> 0);
  //
  // 2010-09-11
  DModG.ReopenDtEncerMorto(Entidade);
  // FIm 2010-09-11
  //
end;

procedure TFmCashBal.BitBtn1Click(Sender: TObject);
(*
var
  DataI, DataF: String;
  Ano, Mes: Word;
*)
begin
  // N�o fazer!!! N�o vale a pena!!!
  /////////////////////
  EXIT;
  /////////////////
  ///
  //
  (*
  if DmodFin.ExisteLancamentoSemCliInt() then Exit;
  Screen.Cursor := crHourGlass;
  //
  //AindaNaoAtzCtas := True;
  //
  FPeriodoAtu := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
  DataI := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu), 1);
  DataF := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 1);
  //
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  //Mez := ((Ano - 2000) * 100) + Mes;
  FDataAnt := Geral.FDT(EncodeDate(Ano-1,1,1), 1);
  FDataIni := Geral.FDT(EncodeDate(Ano  ,1,1), 1);
  FPeriodoIni := (Ano - 2000) * 100 + 1;
  FPeriodoFim := (Ano - 2000) * 100 + 12;
  FPeriodoAnt := FPeriodoIni - 100;
  if Mes = 12 then
  begin
    Ano := Ano + 1;
    Mes := 1;
  end else Mes := Mes + 1;
  FDataAtu := Geral.FDT(EncodeDate(Ano, Mes, 1)-1, 1);
  //
  //
  if Geral.MB_Pergunta('Confirma o envio de todos lan�amentos de ' +
  'contas correntes e caixas emitidos at� a data abaixo e os lan�amentos de ' +
  'emiss�es quitados ou totalmente pagos at� a data abaixo para o cliente ' +
  'interno "' + QrEmpNO_EMPRESA.Value + '"?'sLineBreak'Data limite: ' +
  Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 2))) = ID_YES then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE ' + VAR LCT + ' lan, carteiras car');
    DMod.QrUpd.SQL.Add('SET lan.Sit=3');
    DMod.QrUpd.SQL.Add('WHERE lan.Carteira=car.Codigo');
    DMod.QrUpd.SQL.Add('AND car.Tipo <> 2');
    DMod.QrUpd.SQL.Add('AND lan.Sit < 2');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('REPLACE INTO lanctoz ');
    DMod.QrUpd.SQL.Add('SELECT lan.*');
    DMod.QrUpd.SQL.Add('FROM ' + VAR LCT + ' lan');
    DMod.QrUpd.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DMod.QrUpd.SQL.Add('WHERE Sit>1');
    DMod.QrUpd.SQL.Add('AND car.ForneceI=:P0');
    DMod.QrUpd.SQL.Add('AND Data <=:P1');
    DMod.QrUpd.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
    DMod.QrUpd.Params[01].AsString  := DataF;
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DELETE lan.*');
    DMod.QrUpd.SQL.Add('FROM ' + VAR LCT + ' lan, carteiras car');
    DMod.QrUpd.SQL.Add('WHERE car.Codigo=lan.Carteira');
    DMod.QrUpd.SQL.Add('AND lan.Sit>1');
    DMod.QrUpd.SQL.Add('AND car.ForneceI=:P0');
    DMod.QrUpd.SQL.Add('AND Data <=:P1');
    DMod.QrUpd.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
    DMod.QrUpd.Params[01].AsString  := DataF;
    DMod.QrUpd.ExecSQL;
    //
  end;

  //
  Screen.Cursor := crDefault;
  *)
end;

procedure TFmCashBal.BitBtn2Click(Sender: TObject);
var
  I: Integer;
  PI_A: PPropInfo;
  Nome: String;
  DataSet: TfrxDataSet;
begin
  for I := 0 to frxBal_A_01.ComponentCount - 1 do
  begin
    PI_A := GetPropInfo(frxBal_A_01.Components[I].ClassInfo, 'DataSet');
    //
    if PI_A <> nil then
    begin
      DataSet := TfrxMemoView(TComponent(frxBal_A_01.Components[I])).DataSet;
      //
      if DataSet <> nil then
      begin
        Nome := DataSet.Name;
        //
        ShowMessage(frxBal_A_01.Components[I].Name + ': "' + Nome + '"');
      end;
    end;
  end;
end;

procedure TFmCashBal.BtSdoIniClick(Sender: TObject);
begin
  (*
  Screen.Cursor := crHourGlass;
  try
    FPeriodoAtu := ((Geral.IMV(CBAno.Text) - 2000) * 12) + CBMes.ItemIndex + 1;
    // Atualizar hist�rico dos saldos das contas
    DmodFin.AtualizaContasHistSdo3(DModG.QrEmpresasCodigo.Value, LaSub1);
    //
    if DBCheck.CriaFm(TFmCashBalIni, FmCashBalIni, afmoNegarComAviso) then
    begin
      // Abrir tabelas dos saldos de contas e seus n�veis (sub-grupos, grupos, conjuntos e planos)
      DModFin.SaldosNiveisPeriodo_2(DModG.QrEmpresasCodigo.Value, FPeriodoAtu, -1,
        False, PB1, PB1, PB1, PB1, PB1, FmCashBalIni.QrSNG);
      //
      MyObjects.Informa(LaSub1, True, 'Verificando saldos iniciais das contas controladas');
      FmCashBalIni.AtualizaIniOld();
      MyObjects.Informa(LaSub1, False, '...');
      FmCashBalIni.ShowModal;
      FmCashBalIni.Destroy;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  *)
end;

procedure TFmCashBal.BtConfiguraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCfgRel, FmEntiCfgRel, afmoNegarComAviso) then
  begin
    FmEntiCfgRel.ShowModal;
    FmEntiCfgRel.Destroy;
    //
    AtualizaItensAImprimir;
  end;
end;

procedure TFmCashBal.BtNenhumClick(Sender: TObject);
begin
  //CGItensImp.Value := 0;
  case RGFormato.ItemIndex of
    //0: DBGLista.DesativaTodos();   Parei aqui Alexandria
    1: CGResumido.Value := 0; 
  end;
end;

procedure TFmCashBal.BtOKClick(Sender: TObject);
begin
  ImprimeBalancete(False);
end;

procedure TFmCashBal.BtReordenaClick(Sender: TObject);

  (*
  procedure AtualizaOrdemDoItem(Ordem, Controle: Integer);
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'cfgrel', False, [
      'Ordem'], ['Controle'], [Ordem], [Controle], False);
  end;
  *)

(*
var
  Ordem: Double;
  I, N, K: Integer;
*)
begin
  if (QrLista.State <> dsInactive) and (QrLista.RecordCount > 0) then
  begin
    UReordena.ReordenaItens(QrLista, Dmod.QrUpd, 'enticfgrel',
      'Ordem', 'Controle', 'RelTitu', '', '', '', nil);
    //
    AtualizaItensAImprimir;
  end;
  (*
  if dmkPF.ObtemValorDouble(Ordem, 0) then
  begin
    N := Trunc(Ordem);
    //
    if (N < 1) or (N > QrLista.RecordCount) then
    begin
      Geral.MB_Aviso('A ordem ' + Geral.FF0(N) + ' n�o existe!');
      Exit;
    end;
    //
    I := 0;
    K := QrListaControle.Value;
    //
    AtualizaOrdemDoItem(N, K);
    //
    QrLista.First;
    //
    while not QrLista.Eof do
    begin
      if K <> QrListaControle.Value then
      begin
        I := I + 1;
        if (I = N) then
          I := I + 1;
        //
        AtualizaOrdemDoItem(I, QrListaControle.Value);
      end;
      QrLista.Next;
    end;
    //
    QrLista.Close;
    UnDmkDAC_PF.AbreQuery(QrLista, DModG.MyPID_DB);
    QrLista.Locate('Controle', K, []);
  end;
  *)
end;

function TFmCashBal.frxComposite(frxAtu: TfrxReport; var ClearLastReport:
Boolean): Boolean;
var
  s: TMemoryStream;
begin
  Result := False;
  try
    if frxAtu <> nil then
    begin
      (*
      if VAR_REL_GEREN = 10 then
        frxFIN_BALAN_001_001.OnGetValue := frxFIN_BALAN_001_001.OnGetValue
      else
        frxFIN_BALAN_001_001.OnGetValue := frxAtu.OnGetValue;
      *)
      //
      frxFIN_BALAN_001_001.OnUserFunction := frxAtu.OnUserFunction;
      frxFIN_BALAN_001_001.ReportOptions.Name := 'Balancete';
      //  mudado de frxBal para frxAtu
      frxAtu.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      //end;
      //
      //frxAtu.Variables['LogoBancoExiste'] := True;//FileExists(QrCondBcoLogoPath.Value);
      //frxAtu.Variables['LogoBancoPath'] := QuotedStr(QrCondBcoLogoPath.Value);
      //
      //frxAtu.Variables['LogoEmpresaExiste'] := True;//FileExists(QrCondCliLogoPath.Value);
      //frxAtu.Variables['LogoEmpresaPath'] := QuotedStr(QrCondCliLogoPath.Value);
       //  FIM mudado de frxBal para frxAtu


      // agregar frx selecionado ao frxBal (frx virtual que est� agrupando v�rios frx para imprimir tudo junto)
      s := TMemoryStream.Create;
      //
      frxAtu.SaveToStream(s);
      //
      s.Position := 0;
      //
      frxFIN_BALAN_001_001.LoadFromStream(s);
      frxFIN_BALAN_001_001.PrepareReport(ClearLastReport);
      //
      ClearLastReport := False;
      //
      Result := True;
    end else
      Geral.MB_Erro('ERRO. "frxCond?" n�o definido.');
  except
    raise;
  end;
end;

procedure TFmCashBal.frxFIN_BALAN_001_001GetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxFIN_BALAN_001_001, VarName, Value)
end;

procedure TFmCashBal.frxFIN_BALAN_001_002GetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxFIN_BALAN_001_002, VarName, Value)
end;

procedure TFmCashBal.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
var
  Ord: String;
  Nivel: Integer;
begin
  //Capa Ini
  //Linha 1
  if      VarName = 'VARF_CpaLin1FonNom' then Value := QrEntiCfgRel_01CpaLin1FonNom.Value
  else if VarName = 'VARF_CpaLin1FonTam' then Value := QrEntiCfgRel_01CpaLin1FonTam.Value
  else if VarName = 'VARF_CpaLin1MrgSup' then Value := Int(QrEntiCfgRel_01CpaLin1MrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin1AltLin' then Value := Int(QrEntiCfgRel_01CpaLin1AltLin.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin1AliTex' then Value := QrEntiCfgRel_01CpaLin1AliTex.Value
  //Linha 2
  else if VarName = 'VARF_CpaLin2FonNom' then Value := QrEntiCfgRel_01CpaLin2FonNom.Value
  else if VarName = 'VARF_CpaLin2FonTam' then Value := QrEntiCfgRel_01CpaLin2FonTam.Value
  else if VarName = 'VARF_CpaLin2MrgSup' then Value := Int(QrEntiCfgRel_01CpaLin2MrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin2AltLin' then Value := Int(QrEntiCfgRel_01CpaLin2AltLin.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin2AliTex' then Value := QrEntiCfgRel_01CpaLin2AliTex.Value
  //Linha 3
  else if VarName = 'VARF_CpaLin3FonNom' then Value := QrEntiCfgRel_01CpaLin3FonNom.Value
  else if VarName = 'VARF_CpaLin3FonTam' then Value := QrEntiCfgRel_01CpaLin3FonTam.Value
  else if VarName = 'VARF_CpaLin3MrgSup' then Value := Int(QrEntiCfgRel_01CpaLin3MrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin3AltLin' then Value := Int(QrEntiCfgRel_01CpaLin3AltLin.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin3AliTex' then Value := QrEntiCfgRel_01CpaLin3AliTex.Value
  //Logo
  else if VarName = 'VARF_CpaImgMrgSup'  then Value := Int(QrEntiCfgRel_01CpaImgMrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgMrgEsq'  then Value := Int(QrEntiCfgRel_01CpaImgMrgEsq.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgAlt'     then Value := Int(QrEntiCfgRel_01CpaImgAlt.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgLar'     then Value := Int(QrEntiCfgRel_01CpaImgLar.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgStre'    then Value := QrEntiCfgRel_01CpaImgStre.Value
  else if VarName = 'VARF_CpaImgProp'    then Value := QrEntiCfgRel_01CpaImgProp.Value
  else if VarName = 'VARF_CpaImgTran'    then Value := QrEntiCfgRel_01CpaImgTran.Value
  else if VarName = 'VARF_CpaImgTranCol' then Value := QrEntiCfgRel_01CpaImgTranCol.Value
  else if VarName = 'VARF_TEMUH'         then Value := Geral.BoolToInt(VAR_KIND_DEPTO = kdUH)
  //Capa Fim
  else if VarName = 'VARF_PERIODO_TXT' then
    Value := Geral.Maiusculas(MyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes), False)
  else if VarName = 'VARF_PAGINAR' then
    Value := CkPaginar.Checked
  else if AnsiCompareText(VarName, 'VARF_ULTIMODIA') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu)) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE6,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  (*
  else if AnsiCompareText(VarName, 'VARF_SALDO_INICIAL') = 0 then Value :=
    DCond.SaldoInicialCarteira(dmkPF.PrimeiroDiaDoPeriodo_Date(
      FPeriodoAtu), QrExtratosCarteira.Value)
  *)
  else if AnsiCompareText(VarName, 'MES_ANO ') = 0 then Value :=
    dmkPF.MesEAnoDoPeriodoLongo(FPeriodoAtu)
  else if AnsiCompareText(VarName, 'CIDADE  ') = 0 then Value :=
    QrEmpCIDADE.Value
  else if AnsiCompareText(VarName, 'SIGLA_UF') = 0 then Value :=
    QrEmpNO_UF.Value
  else if AnsiCompareText(VarName, 'DATA_ATU') = 0 then Value :=
    Geral.FDT(Date, 6)
  else if AnsiCompareText(VarName, 'DATA_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  //
  //Evitar erro de usu�rio ( ou pr�prio)
  //
  (*
  else begin
    Geral.MB_Aviso('A vari�vel "' + VarName + '" n�o est� ' +
    'implementada nesta impress�o! Verifique o texto do Parecer do Conselho ' +
    'no cadastro do condom�nio (Gerencia Clientes). Se o problema persistir ' +
    'entre em contato com a DERMATEK!'), );
    Value := '';
  end;
  *)
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_ANT') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu-1))
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  (*
  else if AnsiCompareText(VarName, 'VARF_TOPONOMCON') = 0 then
    Value := int(Dmod.QrControle.FieldByName('BAL_TopoNomCon').AsInteger / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_TOPOTITULO') = 0 then
    Value := int(Dmod.QrControleFieldByName('BAL_TopoTitulo').AsInteger / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_TOPOPERIOD') = 0 then
    Value := int(Dmod.QrControleFieldByName('BAL_TopoPeriod').AsInteger / VAR_frCM)
  *)
  else if Copy(VarName, 1, 5) = 'VMES_' then
    Value := Copy(VarName, 6) + ' / ' + CBAno.Text
  else if VarName = 'VARF_ANO1' then
    Value := 'Total ' + CBAno.Text
  else if VarName = 'VARF_ANO0' then
    Value := 'Total ' + Geral.FF0(Geral.IMV(CBAno.Text) -1)
  else if VarName = 'VARF_MID1' then
    Value := 'M�dia ' + CBAno.Text
  else if VarName = 'VARF_MID0' then
    Value := 'M�dia ' + Geral.FF0(Geral.IMV(CBAno.Text) -1)
  else if VarName = 'LogoBalanceteExiste' then
    Value := FileExists(QrEntiCfgRel_01LogoPath.Value)
  else if VarName = 'LogoBalancetePath' then
    Value := QrEntiCfgRel_01LogoPath.Value
  else if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'VARF_RESUMIDO_01' then Value := FResumido_01
  else if VarName = 'VARF_RESUMIDO_02' then Value := FResumido_02
  else if VarName = 'VARF_RESUMIDO_03' then Value := FResumido_03
  else if VarName = 'VARF_RESUMIDO_04' then Value := FResumido_04
  else if VarName = 'VARF_RESUMIDO_05' then Value := FResumido_05
  else if VarName = 'VARF_RESUMIDO_06' then Value := FResumido_06
  else if VarName = 'VARF_RESUMIDO_07' then Value := FResumido_07
  else if VarName ='VARF_DATA' then Value := TPData.Date
  else if VarName = 'VARF_CTACOCODNOM' then
  begin
    Nivel := DModFin.QrSaldosNivNivel.Value;
    Ord   := Geral.FF0(DModFin.QrSaldosNivCO_Pla.Value);
    //
    if Nivel < 5 then Ord := Ord + '.' + FormatFloat('00', DModFin.QrSaldosNivCO_Cjt.Value);
    if Nivel < 4 then Ord := Ord + '.' + FormatFloat('000', DModFin.QrSaldosNivCO_Gru.Value);
    if Nivel < 3 then Ord := Ord + '.' + FormatFloat('0000', DModFin.QrSaldosNivCO_SGr.Value);
    if Nivel < 2 then Ord := Ord + '.' + FormatFloat('00000', DModFin.QrSaldosNivCO_Cta.Value);

    case DModFin.QrSaldosNivNivel.Value of
      1: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Cta.Value;
      2: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_SGr.Value;
      3: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Gru.Value;
      4: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Cjt.Value;
      5: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Pla.Value;
    end;
  end
////////////////////////////////////////////////////////////////////////////////
  else
    if VAR_REL_GEREN = 10 then
      FmResMes.frxReport000GetValue(frxFIN_BALAN_001_001, VarName, Value)
end;

procedure TFmCashBal.ImprimeBalancete(Exporta: Boolean);
var
  //CLR: Boolean; //CLR = ClearLastReport
  PBB, (*RelItem, *)Mez, Ano, Mes, Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  //
  FAtzContasMensais2 := False;
  FIni               := Now();
  Screen.Cursor      := crHourGlass;
  try
    QrSels.Close;
    UnDmkDAC_PF.AbreQuery(QrSels, DModG.MyPID_DB);
    //
    if RGFormato.ItemIndex = 0 then
      if MyObjects.FIC(QrSelsItens.Value = 0, nil, 'Selecione pelo menos um relat�rio!') then Exit;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmp, Dmod.MyDB, [
      'SELECT en.Codigo, uf.Nome NO_UF, en.CliInt,',
      'FLOOR(IF(en.Codigo<-10,en.Filial,en.CliInt)) CO_SHOW,',
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_EMPRESA,',
      'IF(en.Tipo=0,en.ECidade,en.PCidade) CIDADE',
      'FROM entidades en',
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)',
      'WHERE en.Codigo=' + Geral.FF0(DModG.QrEmpresasCodigo.Value),
      '']);
    //
    FEntidade := DModG.QrEmpresasCodigo.Value;
    FCliInt   := EdEmpresa.ValueVariant;
    FEnt_Txt  := Geral.FF0(FEntidade);
    FTabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, FCliInt);
    FTabLctB  := DModG.NomeTab(TMeuDB, ntLct, False, ttB, FCliInt);
    FTabLctD  := DModG.NomeTab(TMeuDB, ntLct, False, ttD, FCliInt);
    //FTabLctX  := Copy(FTabLctA, 1, Length(FTabLctA)-1);
    FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, FCliInt);
    FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, FCliInt);
    FTabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, FCliInt);
    FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, FCliInt);
    //
    MyObjects.Informa(LaAviso, True, 'Verificando lan�amentos sem Cliente Interno');
    //
    if DmodFin.ExisteLancamentoSemCliInt(FTabLctA) then Exit;
    //
    MyObjects.Informa(LaAviso, True, 'Preparando consultas');
    //
    FPeriodoAtu := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
    FDataI      := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu), 1);
    FDataF      := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 1);
    //
    // Abre tabela com dados espec�ficos do cliente interno selecionado!
    if DmodFin.ReopenCliInt(FEntidade) then
      PBB := DmodFin.QrCliIntPBB.Value
    else
      PBB := 1;
    //
    Ano := Geral.IMV(CBAno.Text);
    Mes := CBMes.ItemIndex + 1;
    Mez := ((Ano - 2000) * 100) + Mes;
    //
    FDataAnt := Geral.FDT(EncodeDate(Ano-1,1,1), 1);
    FDataIni := Geral.FDT(EncodeDate(Ano  ,1,1), 1);
    FDataBal := Geral.FDT(EncodeDate(Ano, Mes, 1), 1);
    //
    FPeriodoIni := (Ano - 2000) * 100 + 1;
    FPeriodoFim := (Ano - 2000) * 100 + 12;
    FPeriodoAnt := FPeriodoIni - 100;
    //
    if Mes = 12 then
    begin
      Ano := Ano + 1;
      Mes := 1;
    end else
      Mes := Mes + 1;
    //
    FDataAtu := Geral.FDT(EncodeDate(Ano, Mes, 1)-1, 1);
    //
    //ReopenExclusivos(Mez)
    //
    case RGFormato.ItemIndex of
      0: ImprimeCompleto(Mez, PBB, Exporta);
      1: ImprimeReduzido(Mez, Exporta);
      else Geral.MB_Erro('Formato de relat�rio n�o implementado!');
    end;
  finally
    MyObjects.Informa(LaAviso, False, '...');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashBal.ImprimeCompleto(Mez, PBB: Integer; Exporta: Boolean);
var
  CLR, AindaNaoAtzCtas: Boolean;
  ComparaSdoNiveis, RelItem, Responsaveis: Integer;
  Num10, ImpA_11, ImpA_12, ImpA_13, ImpA_14, ImpA_15, ImpA_16: Boolean;
  Diferenca: Double;
  //Arquivo: String;
begin
  Screen.Cursor := crHourGlass;
  try
    ImpA_11 := False;
    ImpA_12 := False;
    ImpA_13 := False;
    ImpA_14 := False;
    ImpA_15 := False;
    ImpA_16 := False;
    Num10   := False;
    CLR     := True;
    //
    AindaNaoAtzCtas  := True;
    ComparaSdoNiveis := 0;
    //
    QrEntiCfgRel_01.Close;
    //
    case RGOpcoesListas.ItemIndex of
      0: QrEntiCfgRel_01.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
      1: QrEntiCfgRel_01.Params[0].AsInteger := 0;
      2: QrEntiCfgRel_01.Params[0].AsInteger := -1;
    end;
    UnDmkDAC_PF.AbreQuery(QrEntiCfgRel_01, Dmod.MyDB);
    //
    QrOrdiMov.Close;
    QrOrdiMov.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
    QrOrdiMov.Params[01].AsInteger := Mez;
    UnDmkDAC_PF.AbreQuery(QrOrdiMov, Dmod.MyDB);
    //
    QrOrdinarios.Close;
    QrOrdinarios.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
    QrOrdinarios.Params[01].AsInteger := Mez;
    UnDmkDAC_PF.AbreQuery(QrOrdinarios, Dmod.MyDB);
    //
    QrLista.First;
    while not QrEntiCfgRel_01.Eof do
    //while not QrLista.Eof do
    begin
      RelItem       := QrEntiCfgRel_01RelItem.Value;
      VAR_REL_GEREN := RelItem;
      //RelItem := QrListaRelItem.Value;
      //
      if QrLista.Locate('Controle', QrEntiCfgRel_01Controle.Value, [])
        and (QrListaAtivo.Value = 1) then
      //if QrListaAtivo.Value = 1 then
      begin
        MyObjects.Informa(LaAviso, True, 'Gerando sub-relat�rio ID = ' + Geral.FF0(RelItem));
        case RelItem of
          02:
          begin
            QrEntiRespon.Close;
            QrEntiRespon.SQL.Clear;
            QrEntiRespon.SQL.Add('SELECT er.Nome NO_RESPON, ec.Nome NO_CARGO,');
            QrEntiRespon.SQL.Add('er.Observ, er.MandatoIni, er.MandatoFim');
            QrEntiRespon.SQL.Add('FROM entirespon er');
            QrEntiRespon.SQL.Add('LEFT JOIN enticargos ec ON ec.Codigo=er.Cargo');
            QrEntiRespon.SQL.Add('WHERE er.Codigo=:P0');
            QrEntiRespon.SQL.Add('ORDER BY er.OrdemLista, er.Nome');
            QrEntiRespon.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
            UnDmkDAC_PF.AbreQuery(QrEntiRespon, Dmod.MyDB);
            Responsaveis := QrEntiRespon.RecordCount;
            //
            QrEntiRespon.Close;
            QrEntiRespon.SQL.Clear;
            QrEntiRespon.SQL.Add('SELECT er.Nome NO_RESPON, ec.Nome NO_CARGO,');
            QrEntiRespon.SQL.Add('er.Observ, er.MandatoIni, er.MandatoFim');
            QrEntiRespon.SQL.Add('FROM entirespon er');
            QrEntiRespon.SQL.Add('LEFT JOIN enticargos ec ON ec.Codigo=er.Cargo');
            QrEntiRespon.SQL.Add('WHERE er.Codigo=' + Geral.FF0(DModG.QrEmpresasCodigo.Value));
            //QrEntiRespon.SQL.Add('AND "' + FDataIni + '" BETWEEN MandatoIni AND MandatoFim');
            QrEntiRespon.SQL.Add('AND MandatoIni <= "' + FDataBal + '"');
            QrEntiRespon.SQL.Add('AND MandatoFim >= "' + FDataBal + '"');
            QrEntiRespon.SQL.Add('ORDER BY er.OrdemLista, er.Nome');
            //QrEntiRespon.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
            //QrEntiRespon.Params[01].AsString  := FDataIni;
            UnDmkDAC_PF.AbreQuery(QrEntiRespon, Dmod.MyDB);
            if Responsaveis > QrEntiRespon.RecordCount then Geral.MB_Aviso(
              Geral.FF0(Responsaveis - QrEntiRespon.RecordCount) +
              ' respons�veis que assinam foram desconsiderados por estarem fora ' +
              'do mandato neste per�odo!');
          end;
          03: PreparaBal_A_03();
          04: PreparaBal_A_04();
          05:
          begin
            (*
            DModFin.SaldosNiveisPeriodo_2(DModG.QrEmpresasCodigo.Value, FPeriodoAtu, -1,
              False, PB1, PB1, PB1, PB1, PB1, DModFin.QrSNG);

para
            DmodFin.AtualizaContasHistSdo3(DModG.QrEmpresasCodigo.Value, nil/*LaSub1*/,
              DModG.MezLastEncerr(), FTabLctA);
            DmodFin.SaldosDeContasControladas3(DModG.QrEmpresasCodigo.Value, Mez, DModFin.QrSNG);

qual dos dois � o correto?
            *)
            // Extrato
            DModFin.GeraBalanceteExtrato(FPeriodoAtu, FEntidade, PB1, LaSub1,
            DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
            FTabLctA, FTabLctB, FTabLctD(*, FCliInt*));
            // Saldos das contas controladas
            DmodFin.AtualizaContasHistSdo3(DModG.QrEmpresasCodigo.Value,
              nil, nil(*LaSub1*), DModG.MezLastEncerr(), FTabLctA);
            DmodFin.SaldosDeContasControladas3(DModG.QrEmpresasCodigo.Value, Mez, DModFin.QrSaldosNiv);

            (*
            DModFin.SaldosNiveisPeriodo_2(DModG.QrEmpresasCodigo.Value, FPeriodoAtu, -1,
              False, PB1, PB1, PB1, PB1, PB1, DModFin.QrSNG);
            *)

          end;
          06:
          begin
            if AindaNaoAtzCtas then
            begin
              DmodFin.AtualizaContasHistSdo3(DModG.QrEmpresasCodigo.Value, LaSub1,
                nil, DModG.MezLastEncerr(), FTabLctA);
              AindaNaoAtzCtas := False;
            end;
            DmodFin.SaldosDeContasControladas3(DModG.QrEmpresasCodigo.Value, Mez,
              DmodFin.QrSaldosNiv);
            //
            ComparaSdoNiveis := ComparaSdoNiveis + 1;
            //
            MyObjects.frxDefineDataSets(frxBal_A_06, [
              DmodG.frxDsDono,
              frxDsEntiCfgRel_01,
              frxDsEmp,
              DModFin.frxDsSaldosNiv,
              frxDsExclusivos,
              frxDsOrdinarios
              ]);
          end;
          07:
          begin
            if AindaNaoAtzCtas then
            begin
              DmodFin.AtualizaContasHistSdo3(DModG.QrEmpresasCodigo.Value, LaSub1,
              nil, DModG.MezLastEncerr(), FTabLctA);
              //
              AindaNaoAtzCtas := False;
            end;
            DModG.QrUpdPID1.SQL.Clear;
            DModG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _LCS_;');
            DModG.QrUpdPID1.ExecSQL;
            //
            DModG.QrUpdPID1.SQL.Clear;
            DModG.QrUpdPID1.SQL.Add('CREATE TABLE _LCS_ IGNORE ');
            DModG.QrUpdPID1.SQL.Add('SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO,');
            DModG.QrUpdPID1.SQL.Add('cjt.Nome NOMECONJUNTO, gru.Conjunto,');
            DModG.QrUpdPID1.SQL.Add('gru.Nome NOMEGRUPO, sgr.Grupo,');
            DModG.QrUpdPID1.SQL.Add('sgr.Nome NOMESUBGRUPO, con.SubGrupo,');
            DModG.QrUpdPID1.SQL.Add('con.Nome NOMECONTA, mov.Genero,');
            DModG.QrUpdPID1.SQL.Add('SUM(mov.Movim) Valor, ');
            DModG.QrUpdPID1.SQL.Add('pla.OrdemLista OL_PLA, cjt.OrdemLista OL_CJT,');
            DModG.QrUpdPID1.SQL.Add('gru.OrdemLista OL_GRU, sgr.OrdemLista OL_SGR,');
            DModG.QrUpdPID1.SQL.Add('con.OrdemLista OL_CON,');
            DModG.QrUpdPID1.SQL.Add('SUM(0.00) vSGR, SUM(0.00) vGRU, SUM(0.00) vCJT, SUM(0.00) vPLA');
            DModG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.contasmov mov');
            DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.contas    con ON con.Codigo=mov.Genero');
            DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.subgrupos sgr ON sgr.Codigo=mov.SubGrupo');
            DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.grupos    gru ON gru.Codigo=mov.Grupo');
            DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.conjuntos cjt ON cjt.Codigo=mov.Conjunto');
            DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.plano     pla ON pla.Codigo=mov.Plano');
            DModG.QrUpdPID1.SQL.Add('WHERE mov.CliInt=:P0');
            DModG.QrUpdPID1.SQL.Add('AND mov.Mez <=:P1');
            DModG.QrUpdPID1.SQL.Add('AND mov.Movim <> 0');
            DModG.QrUpdPID1.SQL.Add('GROUP BY mov.Genero');
            DModG.QrUpdPID1.SQL.Add('ORDER BY OL_PLA, NOMEPLANO, OL_CJT,');
            DModG.QrUpdPID1.SQL.Add('NOMECONJUNTO, OL_GRU, NOMEGRUPO, OL_SGR,');
            DModG.QrUpdPID1.SQL.Add('NOMESUBGRUPO, OL_CON, NOMECONTA, mov.Mez');
            DModG.QrUpdPID1.SQL.Add('');
            DModG.QrUpdPID1.SQL.Add('');
            DModG.QrUpdPID1.SQL.Add('');
            DModG.QrUpdPID1.SQL.Add('');
            DModG.QrUpdPID1.SQL.Add('');
            DModG.QrUpdPID1.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
            DModG.QrUpdPID1.Params[01].AsInteger := Mez;
            DModG.QrUpdPID1.ExecSQL;
            // SUB-GRUPO
            QrSSdo.Close;
            QrSSdo.SQL.Clear;
            QrSSdo.SQL.Add('SELECT SubGrupo Codigo, SUM(Valor) Valor');
            QrSSdo.SQL.Add('FROM _lcs_');
            QrSSdo.SQL.Add('GROUP BY SubGrupo');
            UnDmkDAC_PF.AbreQuery(QrSSdo, DModG.MyPID_DB);
            while not QrSSdo.Eof do
            begin
              DModG.QrUpdPID1.SQL.Clear;
              DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vSGR=:P0');
              DModG.QrUpdPID1.SQL.Add('WHERE SubGrupo=:P1');
              DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
              DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
              DModG.QrUpdPID1.ExecSQL;
              QrSSdo.Next;
            end;
            // GRUPO
            QrSSdo.Close;
            QrSSdo.SQL.Clear;
            QrSSdo.SQL.Add('SELECT Grupo Codigo, SUM(Valor) Valor');
            QrSSdo.SQL.Add('FROM _lcs_');
            QrSSdo.SQL.Add('GROUP BY Grupo');
            UnDmkDAC_PF.AbreQuery(QrSSdo, DModG.MyPID_DB);
            while not QrSSdo.Eof do
            begin
              DModG.QrUpdPID1.SQL.Clear;
              DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vGRU=:P0');
              DModG.QrUpdPID1.SQL.Add('WHERE Grupo=:P1');
              DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
              DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
              DModG.QrUpdPID1.ExecSQL;
              QrSSdo.Next;
            end;
            //
            // CONJUNTO
            QrSSdo.Close;
            QrSSdo.SQL.Clear;
            QrSSdo.SQL.Add('SELECT Conjunto Codigo, SUM(Valor) Valor');
            QrSSdo.SQL.Add('FROM _lcs_');
            QrSSdo.SQL.Add('GROUP BY Conjunto');
            UnDmkDAC_PF.AbreQuery(QrSSdo, DModG.MyPID_DB);
            while not QrSSdo.Eof do
            begin
              DModG.QrUpdPID1.SQL.Clear;
              DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vCJT=:P0');
              DModG.QrUpdPID1.SQL.Add('WHERE Conjunto=:P1');
              DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
              DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
              DModG.QrUpdPID1.ExecSQL;
              QrSSdo.Next;
            end;
            // PLANO
            QrSSdo.Close;
            QrSSdo.SQL.Clear;
            QrSSdo.SQL.Add('SELECT PLANO Codigo, SUM(Valor) Valor');
            QrSSdo.SQL.Add('FROM _lcs_');
            QrSSdo.SQL.Add('GROUP BY PLANO');
            UnDmkDAC_PF.AbreQuery(QrSSdo, DModG.MyPID_DB);
            while not QrSSdo.Eof do
            begin
              DModG.QrUpdPID1.SQL.Clear;
              DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vPLA=:P0');
              DModG.QrUpdPID1.SQL.Add('WHERE PLANO=:P1');
              DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
              DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
              DModG.QrUpdPID1.ExecSQL;
              QrSSdo.Next;
            end;
            //
            QrLC2.Close;
            QrLC2.Database := DModG.MyPID_DB;
            UnDmkDAC_PF.AbreQuery(QrLC2, DModG.MyPID_DB);
            //
            ComparaSdoNiveis := ComparaSdoNiveis + 1;
            //
            MyObjects.frxDefineDataSets(frxBal_A_07, [
              DmodG.frxDsDono,
              frxDsEntiCfgRel_01,
              frxDsEmp,
              frxDsLCS,
              frxDsExclusivos,
              frxDsOrdinarios
              ]);
          end;
          08:
          begin
            DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, DModG.QrEmpresasCodigo.Value,
              CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked,
              DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
              FTabLctA, FTabLctB, FTabLctD);
          end;
          09:
          begin
            DmodFin.AtualizaContasMensais2(FPeriodoAtu -11, FPeriodoAtu, 0,
              PB1, PB2, PB3, LaSub1, LaSub2, LaSub3,
              DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
              FTabLctA, FTabLctB, FTabLctD);
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrCtasResMes, DModG.MyPID_DB, [
              'SELECT * ',
              'FROM ' + DModFin.FCtasResMes,
              'WHERE SeqImp=-1 ',
              'OR Periodo BETWEEN ' + Geral.FF0(FPeriodoAtu - 11),
              ' AND ' + Geral.FF0(FPeriodoAtu),
              'ORDER BY Nome, Conta, SeqImp, Periodo ',
              '']);
            //
            MyObjects.frxDefineDataSets(frxBal_A_09, [
              frxDsCtasResMes,
              //
              frxDsEntiCfgRel_01,
              frxDsEmp,
              DModG.frxDsDono
              ], False);
          end;
          10:
          begin
            if MyObjects.CriaForm_AcessoTotal(TFmResmes, FmResMes) then
            begin
              Num10                 := True;
              FmResMes.FDtEncer     := DModG.QrLastEncerData.Value;
              FmResMes.FDtMorto     := DModG.QrLastMortoData.Value;
              FmResMes.FTabLctA     := FTabLctA;
              FmResMes.FTabLctB     := FTabLctB;
              FmResMes.FTabLctD     := FTabLctD;
              FmResMes.FHowShowA    := RG10HowShowA.ItemIndex;
              FmResMes.FResMes43    := CkResMes43.Checked;
              FmResMes.FEntidade    := DModG.QrEmpresasCodigo.Value;
              FmResMes.FPaginar     := CkPaginar.Checked;
              FmResMes.FPontoMilhar := not CkSemMilhar.Checked;
              //
              case RGTabelaChart.ItemIndex of
                  0: FmResMes.FTabelaChart := 'Genero';
                  1: FmResMes.FTabelaChart := 'Subgrupo';
                  2: FmResMes.FTabelaChart := 'Grupo';
                else FmResMes.FTabelaChart := '??TabelaChart??';
              end;

              case RGTabelaChart.ItemIndex of
                  0: FmResMes.FCampoChart := 'NomeConta';
                  1: FmResMes.FCampoChart := 'NomeSubgrupo';
                  2: FmResMes.FCampoChart := 'NomeGrupo';
                else FmResMes.FCampoChart := '??CampoChart??';
              end;

              case RGTabelaChart.ItemIndex of
                  0: FmResMes.FOutrosChart := 'Outras Contas';
                  1: FmResMes.FOutrosChart := 'Outros Subgrupos';
                  2: FmResMes.FOutrosChart := 'Outros Grupos';
                else FmResMes.FOutrosChart := '????';
              end;

              FmResMes.ResultadoGeral(FPeriodoAtu - 11, PB1, PB2, PB3, LaSub1, LaSub2, LaSub3);
            end;
          end;
          (*  2011-08-01
          11: ImpA_11 := DModFin.ReopenPrevItO(DModG.QrEmpresasCodigo.Value, FPeriodoAtu,
            FTabPrvA, FTabPriA);
          *)
          11:
          begin
            MyObjects.frxDefineDataSets(frxBal_A_11, [
              DModG.frxDsDono,
              frxDsEntiCfgRel_01,
              frxDsEmp,
              frxDsPrevItO
              ], False);
            //
            ImpA_11 := DModFin.ReopenPrevItO(DModG.QrEmpresasFilial.Value, FPeriodoAtu, FTabPrvA, FTabPriA);
          end;
          // Fim 2011-08-01
          12: ImpA_12 := DModFin.BloquetosDeCobrancaProcessados(FPeriodoAtu,
                           DModG.QrEmpresasCodigo.Value, FTabLctA, FTabLctB, FTabLctD);
          13: ImpA_13 := DmodFin.ReopenPendG(DModG.QrEmpresasCodigo.Value,
                           DModG.QrEmpresasFilial.Value, FTabLctA);
          // testar no 244
          14:
          begin
            MyObjects.frxDefineDataSets(frxBal_A_14, [
              DModG.frxDsDono,
              frxDsEntiCfgRel_01,
              frxDsEmp,
              frxDsRep,
              frxDsRepBPP,
              frxDsRepLan
              ], False);
            ImpA_14 := DModFin.ReopenReparcelNew(DModG.QrEmpresasCodigo.Value, FPeriodoAtu, PBB);
          end;
          15:
          begin
            MyObjects.frxDefineDataSets(frxBal_A_15, [
              DModG.frxDsDono,
              frxDsEntiCfgRel_01,
              frxDsEmp,
              frxDsRepAbe
              ], False);
            ImpA_15 := DModFin.ReopenReparcelAbe(DModG.QrEmpresasCodigo.Value, FTabLctA);
          end;
          16:
          begin
            MyObjects.frxDefineDataSets(frxBal_A_16, [
              DModG.frxDsDono,
              frxDsEntiCfgRel_01,
              frxDsEmp,
              frxDs16
              ], False);

            ImpA_16 := ChCompensadosPeriodo(DModG.QrEmpresasCodigo.Value);
          end;
        end;
        ReopenExclusivos(Mez);
        MyObjects.Informa(LaAviso, True, 'Gerando sub-relatorio ID = ' + Geral.FF0(RelItem));
        case RelItem of
          01: frxComposite(frxBal_A_01, CLR); // Capa
          02: frxComposite(frxBal_A_02, CLR); // Parecer revisores
          03: frxComposite(frxBal_A_03, CLR); // Entradas na data
          04: frxComposite(frxBal_A_04, CLR); // Entradas por m�s de compet�ncia
          05: frxComposite(frxBal_A_05, CLR); // Extrato contas correntes e caixas
          06: frxComposite(frxBal_A_06, CLR); // Saldos de contas controladas
          07: frxComposite(frxBal_A_07, CLR); // Saldos de todas contas
          08: frxComposite(frxBal_A_08, CLR); // Demosntr. Receitas e Despesas Periodo
          09: frxComposite(frxBal_A_09, CLR); // Mensalidades pr�-estipuladas
          10:
          begin
            VAR_REL_GEREN := 10;
            //
            if CkResMes43.Checked then
            begin
              FmResMes.VerificaTipoAgrupa();
              frxComposite(FmResMes.ImprimeResMesExclusivo(43, PB1, PB2, LaSub1), CLR)
            end else
              frxComposite(FmResMes.ImprimeResMesExclusivo(42, PB1, PB2, LaSub1), CLR);
          end;
          11: if ImpA_11 then frxComposite(frxBal_A_11, CLR); // Composi��o da provis�o efetuada para a arrecada��o
          12: if ImpA_12 then // Processamento de bloquetos de cobran�a
              begin
                case QrEntiCfgRel_01FonteTam.Value of
                //case QrListaFonteTam.Value of
                  0:   frxComposite(frxBal_A_12A, CLR);
                  1,2: frxComposite(frxBal_A_12B, CLR);
                end;
              end;
          13: if ImpA_13 then frxComposite(frxBal_A_13, CLR); // Pend�ncias de Clientes (Cond�minos)
          14: if ImpA_14 then frxComposite(frxBal_A_14, CLR); // Reparcelamentos de integrantes no per�odo (Cond�minos)
          15: if ImpA_15 then frxComposite(frxBal_A_15, CLR); // Reparcelamentos de integrantes em aberto (Cond�minos)
          16: if ImpA_16 then frxComposite(frxBal_A_16, CLR); // Cheques compensados no per�odo
          else Geral.MB_Erro('O item "' + Geral.FF0(RelItem) +
            '" n�o foi implementado no balancete!' +
            sLineBreak + 'AVISE A DERMATEK!');
        end;
      end;
      QrEntiCfgRel_01.Next;
      //QrLista.Next;
    end;
    if not Exporta then //Imprime
    begin
      MyObjects.Informa(LaAviso, True, 'Preparando Balancete');
      //
      Application.CreateForm(TFmMeufrx, FmMeufrx);
      //
      frxFIN_BALAN_001_001.Preview := FmMeufrx.PvVer;
      FmMeufrx.PvVer.OutlineWidth  := frxFIN_BALAN_001_001.PreviewOptions.OutlineWidth;
      FmMeufrx.PvVer.Zoom          := frxFIN_BALAN_001_001.PreviewOptions.Zoom;
      FmMeufrx.PvVer.ZoomMode      := frxFIN_BALAN_001_001.PreviewOptions.ZoomMode;
      FmMeufrx.UpdateZoom;
    end else
    begin  //Exporta
      MyObjects.Informa(LaAviso, True, 'Exportando Balancete');
    end;
    FFim                 := Now();
    FTempo               := FFim - FIni;
    EdTempo.ValueVariant := FTempo;
    //
    if ComparaSdoNiveis = 2 then
    begin
      QrSN1.Close;
      UnDmkDAC_PF.AbreQuery(QrSN1, DModG.MyPID_DB);
      //
      QrSN2.Close;
      UnDmkDAC_PF.AbreQuery(QrSN2, DModG.MyPID_DB);
      //
      Diferenca := QrSN1Movim.Value -QrSN2Valor.Value;
      //
      if (Diferenca >= 0.01) or (Diferenca <= -0.01) then
        Geral.MB_Aviso(
          'ATEN��O: Saldo total dos N�veis do Plano de Contas ' + sLineBreak +
          'n�o confere com a soma de saldos das contas controladas!' + sLineBreak +
          'Verifique se n�o h� n�veis contas n�o controlas com saldo!' + sLineBreak +
          'DIFEREN�A: ' + FormatFloat('#,###,###,###,##0.00', Diferenca));
    end;
    if not Exporta then //Imprime
    begin
      FmMeufrx.ShowModal;
      FmMeufrx.Destroy;
    end else
    begin //Exporta
      ExportaBalancete(frxFIN_BALAN_001_001, False);
    end;
    //
    if Num10 then
      FmResmes.Destroy;
  finally
    MyObjects.Informa(LaAviso, False, '...');
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashBal.ImprimeReduzido(Mez: Integer; Exporta: Boolean);

  procedure PreparaSQLDebi(DataI, DataF: String);

    procedure GeraParteSQL_Debi(TabLct, DataI, DataF: String);
    begin
      QrDebitos.SQL.Add('SELECT SUM(lan.Debito) Debito, lan.Genero, ');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU, ');
      QrDebitos.SQL.Add('gru.OrdemLista OL_GRU, sgr.OrdemLista OL_SGR, ');
      QrDebitos.SQL.Add('con.OrdemLista OL_CON');
      QrDebitos.SQL.Add('FROM ' + TabLct + ' lan');
      QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
      QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lan.Genero');
      QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
      QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
      QrDebitos.SQL.Add('WHERE lan.Tipo < 2');
      QrDebitos.SQL.Add('AND lan.Debito > 0');
      QrDebitos.SQL.Add('AND lan.Genero>0');
      QrDebitos.SQL.Add('AND car.ForneceI = ' + Geral.FF0(DModG.QrEmpresasCodigo.Value));
      QrDebitos.SQL.Add('AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
      QrDebitos.SQL.Add('GROUP BY lan.Genero');
      (*
      QrDebitos.Params[00].AsInteger := DModG.QrEmpresasCodigo.Value;
      QrDebitos.Params[01].AsString  := FDataI;
      QrDebitos.Params[02].AsString  := FDataF;
      UnDmkDAC_PF.AbreQuery(QrDebitos, DModG.MyPID_DB);
      *)
    end;

  begin
    QrDebitos.Close;
    QrDebitos.Database := DModG.MyPID_DB;
    QrDebitos.SQL.Clear;
    //
    QrDebitos.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_001_DEBI_;');
    QrDebitos.SQL.Add('CREATE TABLE _FIN_BALAN_001_DEBI_ IGNORE ');
    QrDebitos.SQL.Add('');
    GeraParteSQL_Debi(FTabLctA, DataI, DataF);
    QrDebitos.SQL.Add('UNION');
    GeraParteSQL_Debi(FTabLctB, DataI, DataF);
    QrDebitos.SQL.Add('UNION');
    GeraParteSQL_Debi(FTabLctD, DataI, DataF);
    QrDebitos.SQL.Add(';');
    QrDebitos.SQL.Add('');
    QrDebitos.SQL.Add('SELECT * FROM _FIN_BALAN_001_DEBI_');
    QrDebitos.SQL.Add('GROUP BY Genero');
    QrDebitos.SQL.Add('ORDER BY OL_GRU, NOMEGRU, OL_SGR,');
    QrDebitos.SQL.Add('NOMESGR, OL_CON, NOMECON;');
    QrDebitos.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_001_DEBI_;');
  end;

var
  //ArqExport, ArqExportNome,
  DataI, DataF: String;
begin
  FResumido_01 := False;
  FResumido_02 := False;
  FResumido_03 := False;
  FResumido_04 := False;
  FResumido_05 := False;
  FResumido_06 := False;
  FResumido_07 := False;
  //
  DModFin.GeraBalanceteRecDebi(FPeriodoAtu, DModG.QrEmpresasCodigo.Value,
    CkAcordos.Checked,CkPeriodos.Checked, CkTextos.Checked,
    DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
    FTabLctA, FTabLctB, FTabLctD);
  //
  if Geral.IntInConjunto(1, CGResumido.Value) then
    FResumido_01 := True;
    (*
    DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, DModG.QrEmpresasCodigo.Value,
    CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked);
    *)
  if Geral.IntInConjunto(2, CGResumido.Value) then
  begin
    PreparaSQLDebi(FDataI, FDataF);
    UnDmkDAC_PF.AbreQuery(QrDebitos, DModG.MyPID_DB);
    //
    FResumido_02 := True;
  end else
  begin
    // Apenas para evitar erro!
    DataI := Geral.FDT(0, 1);
    DataF := Geral.FDT(0, 1);
    //
    PreparaSQLDebi(DataI, DataF);
  end;
  //
  //Mudar o nome do FrxDsDebitos por causa do DModFin.FrxDs�bitos ???
  //
  DmodFin.ReopenPendAll(DModG.QrEmpresasCodigo.Value,
    DModG.QrEmpresasFilial.Value, FPeriodoAtu, FTabLctA);
  //
  if Geral.IntInConjunto(4, CGResumido.Value) then
    FResumido_03 := True;
  //
  DmodFin.GeraBalanceteExtrato(FPeriodoAtu, DModG.QrEmpresasCodigo.Value, PB1,
    LaSub1, DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value, FTabLctA,
    FTabLctB, FTabLctD(*, CliInt?*));
  //
  if Geral.IntInConjunto(8, CGResumido.Value) then
    FResumido_04 := True;
    //DModFin.GeraBalanceteExtrato(FPeriodoAtu, DModG.QrEmpresasCodigo.Value, PB1, LaSub1,);
  //
  DModFin.GeraBalanceteExtrato(FPeriodoAtu, FEntidade, PB1, LaSub1,
    DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
    FTabLctA, FTabLctB, FTabLctD(*, FCliInt*));
  //
  // Saldos das contas controladas
  DmodFin.AtualizaContasHistSdo3(DModG.QrEmpresasCodigo.Value,
    nil, nil(*LaSub1*), DModG.MezLastEncerr(), FTabLctA);
  //
  DmodFin.SaldosDeContasControladas3(DModG.QrEmpresasCodigo.Value, Mez, DModFin.QrSaldosNiv);
  (*
  FResumido_05 :=
  DModFin.SaldosNiveisPeriodo_2(DModG.QrEmpresasCodigo.Value, FPeriodoAtu, -1,
  False, PB1, PB1, PB1, PB1, PB1, DModFin.QrSNG);
  *)
  //
  if Geral.IntInConjunto(16, CGResumido.Value) then
    FResumido_05 := True;
  //
  DmodFin.AtualizaContasMensais2(FPeriodoAtu -11, FPeriodoAtu, 0,
    PB1, PB2, PB3, LaSub1, LaSub2, LaSub3,
    DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
    FTabLctA, FTabLctB, FTabLctD);
  //
  QrPendMPE.Close;
  UnDmkDAC_PF.AbreQuery(QrPendMPE, DModG.MyPID_DB);
  //
  QrTotalMPE.Close;
  UnDmkDAC_PF.AbreQuery(QrTotalMPE, DModG.MyPID_DB);
  //
  if Geral.IntInConjunto(32, CGResumido.Value) then
    FResumido_06 := True;
  //
  (*
    DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, DModG.QrEmpresasCodigo.Value,
    CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked, )
  *)
  DModFin.GeraBalanceteRecDebi(FPeriodoAtu, DModG.QrEmpresasCodigo.Value,
    CkAcordos.Checked,CkPeriodos.Checked, CkTextos.Checked,
    DModG.QrLastEncerData.Value, DModG.QrLastMortoData.Value,
    FTabLctA, FTabLctB, FTabLctD);
  if Geral.IntInConjunto(64, CGResumido.Value) then
  begin
    if (FResumido_01 = False) then
      FResumido_07 := True;
    if FResumido_07 and (FResumido_04 = False) then
      DmodFin.ReopenPendAll(DModG.QrEmpresasCodigo.Value, DModG.QrEmpresasFilial.Value, FPeriodoAtu,
        FTabLctA);
    // a function DmodFin.ReopenPendAll retornar� false seo aplicativo for diferente de "S Y N D I C"
    FResumido_07 := True;
  end;
  //
  ReopenExclusivos(Mez);
  if Exporta then //Exporta
  begin
    ExportaBalancete(frxFIN_BALAN_001_002, True);
  end else //Imprime
    MyObjects.frxMostra(frxFIN_BALAN_001_002, 'Balancete Demonstrativo (Reduzido)');
end;

procedure TFmCashBal.ExportaBalancete(Frx: TfrxReport; PrepRep: Boolean);
var
  ArqExport, ArqExportNome: String;
begin
  ForceDirectories(FCaminhoEMail);
  //
  ArqExport     := FCaminhoEMail + 'Balancete_' + CBMes.Text + '_' + CBAno.Text + '.pdf';
  ArqExportNome := 'Balancete ' + CBMes.Text + ' ' + CBAno.Text;
  //
  if FileExists(ArqExport) then
    DeleteFile(ArqExport);
  //
  frxPDFExport.FileName := ArqExport;
  frxPDFExport.Title    := 'Balancete';
  //
  if PrepRep then
    Frx.PrepareReport();
  //
  Frx.Export(frxPDFExport);
  //
  {$IfDef CO_DMKID_APP_0004}
  //FmPrincipal.MostraUploads(EdEmpresa.ValueVariant, ArqExport, ArqExportNome, True);
  {$EndIf}
end;

end.

