unit Module;

//Lic_Dmk.LiberaUso5

interface

uses
  Winapi.Windows, Vcl.Dialogs, StdCtrls,
  (*Messages, Graphics, Controls, Forms,
  DBTables, FileCtrl, UMySQLModule, dmkEdit,
  Winsock, MySQLBatch, frxClass, frxDBSet,
  Variants, ABSMain,  ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts;*)
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Vcl.Forms,
  // ZCF2,  UnGrl_Vars, UnGrl_Consts,
  DmkGeral, UnInternalConsts,UnDmkEnums,
  mySQLDirectQuery;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrIdx: TmySQLQuery;
    QrNTV: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrControle: TmySQLQuery;
    QrNTI: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrUpdU: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    ZZDB: TmySQLDatabase;
    QrUpdM: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterENumero: TFloatField;
    QrMasterUsaAccMngr: TSmallintField;
    QrControleSoMaiusculas: TWideStringField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControleMoeda: TWideStringField;
    QrControleCodigo: TIntegerField;
    QrControleDono: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleTravaCidade: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleCidade: TWideStringField;
    QrControleVerBcoTabs: TIntegerField;
    QrOpcoesApp: TmySQLQuery;
    QrOPcoesGrl: TMySQLQuery;
    QrOPcoesGrlERPNameByCli: TWideStringField;
    DqAux: TMySQLDirectQuery;
    QrOpcoesApU: TMySQLQuery;
    QrOpcoesApUCodigo: TIntegerField;
    QrOpcoesApUHabFaccao: TSmallintField;
    QrOpcoesApUHabTextil: TSmallintField;
    QrOpcoesApUNO_HabFaccao: TWideStringField;
    QrOpcoesApUNO_HabTextil: TWideStringField;
    QrOpcoesApULogin: TWideStringField;
    QrOpcoesApUHabFacConfeccao: TSmallintField;
    QrOpcoesApUHabTexTecelagem: TSmallintField;
    QrOpcoesApUHabTexTinturaria: TSmallintField;
    QrSumBtl: TMySQLQuery;
    QrSumBtlQtReal: TFloatField;
    QrAu2: TMySQLQuery;
    QrControleCasasProd: TIntegerField;
    QrLocY: TMySQLQuery;
    QrLocYRecord: TIntegerField;
    QrAux3: TMySQLQuery;
    QrOpcoesAppDirImgDesp: TWideStringField;
    QrOpcoesAppMailsDstn: TWideMemoField;
    QrOpcoesAppMailsDsKm: TWideMemoField;
    QrOpcoesAppLogoEmpresa: TWideMemoField;
    QrMasterVersao: TLargeintField;
  private
    { Private declarations }
  public
    { Public declarations }

    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;

    function  DModG_ObtemAgora(UTC: Boolean = False): TDateTime;
    procedure ReopenControle();
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure ReopenOpcoesApp();
    procedure ReopenOpcoesApU(Usuario: Integer);
    function  TabelasQueNaoQueroCriar(): String;
    //

  end;

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ Teste
uses
 UnGOTOy, Principal,
  DmkDAC_PF,Servidor, ModuleGeral, VerifiDB, UnLic_Dmk,
  //ModuleFin,
  MyListas, UnDmkWeb, UnDmkProcFunc, UnExes_ProjGroupVars,
  Descanso, UnMyObjects, UMySQLDB, UMySQLModule, MyDBCheck;
}

{ TDmod }

function TDmod.DModG_ObtemAgora(UTC: Boolean): TDateTime;
begin
  Result := Now()
end;

procedure TDmod.ReopenControle;
begin
end;


procedure TDmod.ReopenOpcoesApp();
begin
end;

procedure TDmod.ReopenOpcoesApU(Usuario: Integer);
begin
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  // Compatibilidade
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  // Compatibilidade
end;

end.
