object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 15
  object DBGrid1: TDBGrid
    Left = 0
    Top = 129
    Width = 624
    Height = 312
    Align = alClient
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 129
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 1
    object Button1: TButton
      Left = 16
      Top = 16
      Width = 91
      Height = 25
      Caption = 'MySQL OLAP'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 113
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Fast Report'
      TabOrder = 1
      OnClick = Button2Click
    end
    object EdNome: TdmkEdit
      Left = 104
      Top = 71
      Width = 177
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'Marco Luciano Arend'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'Marco Luciano Arend'
      ValWarn = False
    end
    object Dmk: TBitBtn
      Left = 16
      Top = 70
      Width = 75
      Height = 25
      Caption = 'Dmk'
      TabOrder = 3
      OnClick = DmkClick
    end
    object Camera1: TCamera
      Left = 363
      Top = 1
      Width = 260
      Height = 127
      FichierVideo = 'Video.avi'
      FichierImage = 'Imagen.bmp'
      Align = alRight
      ExplicitLeft = 344
      ExplicitTop = -51
      ExplicitHeight = 180
    end
  end
  object MySQLDatabase1: TMySQLDatabase
    DatabaseName = 'bluederm'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'DatabaseName=bluederm'
      'PWD=wkljweryhvbirt')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 136
    Top = 216
  end
  object MySQLQuery1: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM entidades')
    Left = 264
    Top = 208
  end
  object MyDB: TMySQLDatabase
    DatabaseName = 'bluederm'
    DesignOptions = []
    UserName = 'root'
    Host = '192.168.100.60'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'Host=192.168.100.60'
      'DatabaseName=bluederm')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 48
    Top = 380
  end
  object DataSource1: TDataSource
    DataSet = MySQLQuery1
    Left = 256
    Top = 296
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44579.927236388890000000
    ReportOptions.LastChange = 44579.927236388890000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 392
    Top = 328
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object Memo1: TfrxMemoView
        AllowVectorExport = True
        Left = 249.448980000000000000
        Top = 230.551330000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Frame.Typ = []
        Memo.UTF8W = (
          'Ol'#225' Mundo')
      end
    end
  end
  object ZipForge1: TZipForge
    ExtractCorruptedFiles = False
    CompressionLevel = clFastest
    CompressionMode = 1
    CurrentVersion = '6.94 '
    SpanningMode = smNone
    SpanningOptions.AdvancedNaming = False
    SpanningOptions.FirstVolumeSize = 0
    SpanningOptions.VolumeSize = vsAutoDetect
    SpanningOptions.CustomVolumeSize = 65536
    Options.FlushBuffers = True
    Options.OEMFileNames = True
    InMemory = False
    Zip64Mode = zmDisabled
    UnicodeFilenames = False
    EncryptionMethod = caPkzipClassic
    Left = 416
    Top = 208
  end
  object ZConnection1: TZConnection
    ControlsCodePage = cCP_UTF16
    Catalog = ''
    HostName = ''
    Port = 0
    Database = ''
    User = ''
    Password = ''
    Protocol = ''
    Left = 344
    Top = 184
  end
end
