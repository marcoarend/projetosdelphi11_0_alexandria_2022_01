object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 15
  object BtnGet: TButton
    Left = 80
    Top = 104
    Width = 75
    Height = 25
    Caption = 'GET'
    TabOrder = 0
    OnClick = BtnGetClick
  end
  object MeGet: TMemo
    Left = 168
    Top = 80
    Width = 185
    Height = 89
    Lines.Strings = (
      'MeGet')
    TabOrder = 1
  end
  object BtnPost: TButton
    Left = 80
    Top = 264
    Width = 75
    Height = 25
    Caption = 'POST'
    TabOrder = 2
    OnClick = BtnPostClick
  end
  object MePost: TMemo
    Left = 168
    Top = 240
    Width = 185
    Height = 89
    Lines.Strings = (
      'MePost')
    TabOrder = 3
  end
  object EdGet: TEdit
    Left = 168
    Top = 184
    Width = 185
    Height = 23
    TabOrder = 4
    Text = 'EdGet'
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Params = <>
    Response = RESTResponse1
    Left = 448
    Top = 120
  end
  object RESTClient1: TRESTClient
    Params = <>
    Left = 456
    Top = 264
  end
  object RESTResponse1: TRESTResponse
    Left = 520
    Top = 216
  end
end
