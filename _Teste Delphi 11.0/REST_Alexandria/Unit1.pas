unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, REST.Types, Vcl.StdCtrls,
  Data.Bind.Components, Data.Bind.ObjectScope, REST.Client, System.JSON;

type
  TForm1 = class(TForm)
    BtnGet: TButton;
    MeGet: TMemo;
    BtnPost: TButton;
    MePost: TMemo;
    RESTRequest1: TRESTRequest;
    RESTClient1: TRESTClient;
    EdGet: TEdit;
    RESTResponse1: TRESTResponse;
    procedure BtnGetClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BtnGetClick(Sender: TObject);
var
  Url, Logradouro: String;
  Client: TRESTClient;
  Request: TRESTRequest;
begin
  Url := 'http://dermatek.com.br/api/';

  Client := TRESTClient.Create(Url);

  Request := TRESTRequest.Create(nil);
  Request.Client := Client;
  Request.Method := rmGET;
  Request.Resource := 'servicos/cep';

  Request.AddParameter('token', 'ws00ok0g0cwggcw8oscko4ks4k4oko8csk48kc40', TRESTRequestParameterKind.pkHTTPHEADER);

  Request.AddParameter('cep', '87005090', TRESTRequestParameterKind.pkGETorPOST);

  Request.Execute;
  MeGet.Lines.Text := Request.Response.Content;

  Request.Response.GetSimpleValue('logradouro', Logradouro);
  EdGet.Text := Logradouro;

  ShowMessage(IntToStr(Request.Response.StatusCode));

  Request := nil;
end;

procedure TForm1.BtnPostClick(Sender: TObject);
var
  Url: String;
  Client: TRESTClient;
  Request: TRESTRequest;
  Json: TJSONValue;
begin
  Url := 'http://dermatek.com.br/api/';

  Client := TRESTClient.Create(Url);

  Request := TRESTRequest.Create(nil);
  Request.Client := Client;
  Request.Method := rmPOST;
  Request.Resource := 'sql/executa/v2';

  Request.AddParameter('token', 'ws00ok0g0cwggcw8oscko4ks4k4oko8csk48kc40', TRESTRequestParameterKind.pkHTTPHEADER);

  Request.AddParameter('cnpj', '96734892000123', TRESTRequestParameterKind.pkREQUESTBODY);
  Request.AddParameter('ambiente', 'production', TRESTRequestParameterKind.pkREQUESTBODY);
  //Request.AddParameter('sql', 'SELECT * FROM entidades LIMIT 1', TRESTRequestParameterKind.pkREQUESTBODY);
  Request.AddParameter('sql', 'SELECT 1', TRESTRequestParameterKind.pkREQUESTBODY);

  Request.Execute;
  MePost.Lines.Text := Request.Response.Content;

  Json := Request.Response.JSONValue; //Devolve o resultado em um json

  ShowMessage(IntToStr(Request.Response.StatusCode));

  Request := nil;
end;

end.
