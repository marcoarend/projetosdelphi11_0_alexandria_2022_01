object DModG: TDModG
  Height = 201
  Width = 272
  PixelsPerInch = 96
  object Tb_Empresas: TMySQLTable
    Database = Dmod.ZZDB
    AllowSequenced = False
    AutoCalcFields = False
    DefaultIndex = False
    TableName = '_empresas_'
    ReopenOnIndexChange = False
    Left = 56
    Top = 32
    object Tb_EmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object Tb_EmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object Tb_EmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object Tb_EmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Tb_EmpresasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_Empresas: TDataSource
    AutoEdit = False
    DataSet = Tb_Empresas
    Enabled = False
    Left = 56
    Top = 84
  end
end
