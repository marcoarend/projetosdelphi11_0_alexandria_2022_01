﻿unit ModuleGeral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, stdctrls, ExtCtrls;

type
  TDModG = class(TDataModule)
    Tb_Empresas: TMySQLTable;
    Tb_EmpresasCodCliInt: TIntegerField;
    Tb_EmpresasCodEnti: TIntegerField;
    Tb_EmpresasCodFilial: TIntegerField;
    Tb_EmpresasNome: TWideStringField;
    Tb_EmpresasAtivo: TSmallintField;
    Ds_Empresas: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }

end;

var
  DModG: TDModG;

implementation


{$R *.DFM}


end.

