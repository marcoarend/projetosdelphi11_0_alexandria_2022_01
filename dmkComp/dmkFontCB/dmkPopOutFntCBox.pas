unit dmkPopOutFntCBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkCBox, dmkGeral, UnDmkEnums;

type
  TdmkPopOutFntCBox = class(TCustomComboBox)
  private
    { Private declarations }
    //FmCBox: TFmCBox;
    FPopWidth, FPopHeight: Integer;
    //FFontName,
    FFonteNome: String;
    FMoveUsed: Boolean;
    NumUsed: Integer;
    DDown: Boolean;
    //fOnClick: TNotifyEvent;
    FCustEdit: TCustomEdit;

    FQryName: String;
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FOldValor: String;

    procedure SetQryName(Value: String);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetOldValor(Value: String);

    procedure CNCOmmand( Var Message: TWMCommand ); message CN_COMMAND;
    procedure SetPopHeight(High:integer);
    procedure SetPopWidth(Wide:Integer);
    //procedure SetFontName(FntNme:String);
    procedure SetFonteNome(NomeFonte: String);
    //
  protected
    { Protected declarations }
    FText:String;
  public
    { Public declarations }
   constructor Create(AOwner: TComponent); override;
   destructor  Destroy; override;

    procedure CreateWindowHandle(const Params: TCreateParams); override;
   procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
  published
    { Published declarations }
    //procedure OnEnter; Override;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDropDown;
    property OnEndDrag;
    property OnExit;
    //property OnEnter;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDrag;

    property Color;
    property Ctl3D;
    property Cursor;
    property DragCursor;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    property Height;
    property HelpContext;
    property Hint;
    property ImeMode;
    property ImeName;
    property Left;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopUpMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    //property Click:TNotifyEvent read fOnClick  write fOnClick;
    property MoveUsedToTop:Boolean read FMoveUsed  write FMoveUsed;
    procedure SetPText(ntext:String);
    property PreviewText:String read FText  write SetPText;
    property PopUpWidth:Integer read FPopWidth  write SetPopWidth default 300;
    property PopUpHeight:Integer read FPopHeight  write SetPopHeight default 50;
    //property FontName:String read FFontName  write SetFontName;
    property FonteNome:String read FFonteNome  write SetFonteNome;
    property AttachedEdit: TCustomEdit read FCustEdit  write FCustEdit;


    property QryName: String read FQryName  write SetQryName;
    property QryCampo: String read FQryCampo  write SetQryCampo;
    property UpdCampo: String read FUpdCampo  write SetUpdCampo;
    property UpdType: TUpdType read FUpdType  write SetUpdType;
    property OldValor: String read FOldValor  write SetOldValor;

    procedure DropDown; override;
    procedure Click; override;
    procedure DrawItem( Index: Integer; Rect: TRect; State: TOwnerDrawState); override;

  end;

procedure Register;

implementation

procedure register;
begin
  RegisterComponents('DERMATEK', [TdmkPopOutFntCBox]);
end;

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.CMMouseEnter(var Message: TMessage);
//Var pnt:TPoint;
begin
  inherited;
end;

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.CMMouseLeave(var Message: TMessage);
begin
  inherited;
end;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.setPText(nText:String);
begin
  FText := nText;
  //FmCBox.Panel1.Caption := FText;
end;

procedure TdmkPopOutFntCBox.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkPopOutFntCBox.SetQryName(Value: String);
begin
  if Value <> FQryName then
  begin
    FQryName := Value;
  end;
end;

procedure TdmkPopOutFntCBox.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkPopOutFntCBox.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.DrawItem( Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
{
  with self.Canvas Do
  begin
    FillREct(Rect);
    Textout(4,Rect.Top, self.Items[Index]);
    if odFocused in state then
    begin
      //FmCBox.Panel1.Font.Name := Self.Text;
      FmCBox.Panel1.Font.Name := Self.Items[Index];
      if PreViewText = '' then
        FmCBox.Panel1.Caption := 'AaBbYyZz';
    end;
    if (FMoveUsed) and (Index = NumUsed) then
    begin
      MoveTo(0, Rect.Bottom - 3);
      LineTo(Width, Rect.Bottom - 3);
      MoveTo(0, Rect.Bottom-1);
      LineTo(Width, Rect.Bottom-1);
    end;
  end;
}
end;

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.DropDown;
{
var
  pnt: TPoint;
}
begin
  DDown := True;
  if Assigned(FCustEdit) then
  begin
    if FCustEdit.SelLength > 0 then
      PreviewText := FCustEdit.SelText
    else
      PreViewText := 'AaBbYyZz';
  end;
  if Items.Count = 0 then
  begin
    try
      Items.Assign(Screen.Fonts);
      FFonteNome := FonteNome;
      Text := FonteNome;
      Itemindex := Items.IndexOf(FonteNome);
    except

    end;
  end;
  inherited Dropdown;
{
  pnt.x := (Self.Left )+ Self.Width;
  pnt.y := (Self.Top )+ Self.Height;
  pnt := Parent.ClientToScreen(pnt);
  FmCBox.Top := pnt.y;
  FmCBox.Left := pnt.x;

  if FmCBox.Left + FmCBox.Width > Screen.Width then
  begin
    pnt.x := (Self.Left );
    pnt := Parent.ClientToScreen(pnt);
    //FmCBox.Top := pnt.y;
    FmCBox.Left := pnt.x - FmCBox.Width;
  end;
  if FmCBox.Top + FmCBox.Height > Screen.Height then
  begin
    pnt.y := (Self.Top);
    pnt := Parent.ClientToScreen(pnt);
    //FmCBox.Top := pnt.y;
    FmCBox.Top := pnt.y - FmCBox.Height;
  end;

  //ShowMessage(inttostr(FmCBox.Width));
  ShowWindow(FmCBox.handle, SW_SHOWNA);
}
end;

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.Click;
//var ind:integer;
begin
 if DDown = false then
 begin
   FFonteNome := Items[ItemIndex];
   inherited Click;
 end;
  //Itemindex := 0;
  //FFontName := Items[ItemIndex];
  //Inherited Click;
end;

//------------------------------------------------------------------------------

constructor TdmkPopOutFntCBox.Create(AOwner: TComponent);
//var i:integer;
begin
  inherited Create(AOwner);
{
  //FmCBox := TFmCBox.create(nil);
  FmCBox := TFmCBox.create(Self);
  FmCBox.Parent:= TWinControl(AOwner);
}
end;

procedure TdmkPopOutFntCBox.CreateWindowHandle(const Params: TCreateParams);
begin
  inherited;

{

  //Movido do Create!
  FmCBox.Height := 0;
  FmCBox.Width := 0;
  FmCBox.Visible := True;
  ShowWindow(FmCBox.Handle,SW_HIDE );
  FPopWidth := 300;
  FPopHeight := 75;
  FmCBox.Height := 75;
  FmCBox.Width := 300;
  FFonteNome := 'MS Sans Serif';
  Text := 'MS Sans Serif';
  //Itemindex := Items.IndexOf('MS Sans Serif');
  Style := csOwnerDrawFixed;
  //Style := csDropDown;
  FmCBox.Color := ClWhite;
  //PreViewText := 'ABCabcXYZ123.?';
  FMoveUsed := True;
  NumUsed := -1;
  DDown := False;
  //
  Items.Assign(Screen.Fonts);
}
end;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

destructor TdmkPopOutFntCBox.Destroy;
begin
{
  FmCBox.Free;
}
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.CNCommand(var Message: TWMCommand);
var
  ind: Integer;
begin
  if Message.NotifyCode = CBN_CLOSEUP then
  begin
    DDown := False;
    if (FMoveUsed = True) and (ItemIndex <> 0) then
    begin
      ind := ItemIndex;
      if ItemIndex = -1 then
        Exit;
      Items.Move(ItemIndex, 0);
      ItemIndex := 0;
      if ind > NumUsed then
        inc(NumUsed);
    end;
    //
{
    ShowWindow(FmCBox.Handle,SW_HIDE ) ;
}
    Text := FFonteNome;
    FonteNome := Text;
    inherited click;
  end
  else
    inherited;
end;

//------------------------------------------------------------------------------

procedure TdmkPopOutFntCBox.SetPopHeight(High:integer);
begin
  FPopHeight := High;
{
  FmCBox.Height := High;
}
end;
//------------------------------------------------------------------------------
procedure TdmkPopOutFntCBox.SetPopWidth(wide:integer);
begin
  FPopWidth := Wide;
{
  FmCBox.Width := Wide;
}
end;
//------------------------------------------------------------------------------
procedure TdmkPopOutFntCBox.SetFonteNome(NomeFonte: String);
var
  I: Integer;
begin
  for I := 0 to Items.Count do
  begin
    if Items[I] = NomeFonte then
    begin
      ItemIndex := I;
      FFonteNome := NomeFonte;
      Text := NomeFonte;
      Caption := NomeFonte;
      Font.Name := NomeFonte;
      Break;
    end;
  end;
end;

{
procedure TdmkPopOutFntCBox.SetFontName(FntNme:String);
begin
  Itemindex := Items.IndexOf(FntNme);
  FFontName := FntNme;
  Text := FntNme;
  FmCBox.Panel1.Font.Name := FntNme;
end;
}

procedure TdmkPopOutFntCBox.SetOldValor(Value: String);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

end.
