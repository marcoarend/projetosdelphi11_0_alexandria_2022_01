unit dmkSelLista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, dmkGeral, dmkEdit;

type
  TFmdmkSelLista = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Grade: TStringGrid;
    PnCabeca: TPanel;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FResult: Integer;
    FLista: array of String;
    FEdit: TdmkEdit;
  end;

  var
  FmdmkSelLista: TFmdmkSelLista;

implementation

{$R *.DFM}

procedure TFmdmkSelLista.BtSaidaClick(Sender: TObject);
begin
  FResult := - 1;
  Close;
end;

procedure TFmdmkSelLista.FormActivate(Sender: TObject);
begin
  //MLAGeral.CorIniComponente;
end;

procedure TFmdmkSelLista.FormResize(Sender: TObject);
begin
  //MLAGeral.LoadTextBitmapToLMDPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmdmkSelLista.FormCreate(Sender: TObject);
begin
  FResult := -1;
  Grade.Cells[0,0] := 'C�digo';
  Grade.Cells[1,0] := 'Descri��o';
end;

procedure TFmdmkSelLista.GradeDblClick(Sender: TObject);
begin
  FResult := Grade.Row - 1;
  VAR_TEXTO_SEL := FLista[FResult];
  FEdit.ValueVariant := FResult;
  Close;
end;

end.
