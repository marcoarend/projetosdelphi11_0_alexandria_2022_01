object FmdmkSelLista: TFmdmkSelLista
  Left = 530
  Top = 206
  Caption = 'SEL-LISTA-000 :: [Titulo]'
  ClientHeight = 358
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 310
    Width = 457
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 40
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      Visible = False
    end
    object Panel2: TPanel
      Left = 319
      Top = 1
      Width = 137
      Height = 46
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 4
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 310
    Align = alClient
    TabOrder = 0
    object Grade: TStringGrid
      Left = 1
      Top = 53
      Width = 455
      Height = 256
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      ColCount = 2
      DefaultColWidth = 120
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
      TabOrder = 0
      OnDblClick = GradeDblClick
      ExplicitLeft = 5
      ColWidths = (
        120
        203)
    end
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 455
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GB_M: TGroupBox
        Left = 0
        Top = 0
        Width = 455
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object LaTitulo1A: TLabel
          Left = 9
          Top = 9
          Width = 111
          Height = 32
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[TITULO]'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clGradientActiveCaption
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Visible = False
        end
        object LaTitulo1B: TLabel
          Left = 11
          Top = 11
          Width = 111
          Height = 32
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[TITULO]'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object LaTitulo1C: TLabel
          Left = 10
          Top = 10
          Width = 111
          Height = 32
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[TITULO]'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clHotLight
          Font.Height = -27
          Font.Name = 'Arial'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
      end
    end
  end
end
