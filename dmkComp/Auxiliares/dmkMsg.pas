unit dmkMsg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ShellAPI, dmkImage, UnDmkEnums,
  // ini Delphi 20 Alexandria
  (*UnInternalConsts*)dmkGeral;
  // fim Delphi 20 Alexandria

type
  TFmdmkMsg = class(TForm)
    ReAviso: TRichEdit;
    Panel1: TPanel;
    ImageErro: TImage;
    ImageQues: TImage;
    ImageInfo: TImage;
    ImageWarn: TImage;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    BtYes: TBitBtn;
    BtNo: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BtCancel: TBitBtn;
    PnCabeca: TPanel;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Timer1: TTimer;
    PB1: TProgressBar;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtYesClick(Sender: TObject);
    procedure BtNoClick(Sender: TObject);
    procedure BtCancelClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FResult: Integer;
    FEmEspera: Boolean;
    //
    procedure AtivaEspera(TempoCiclo: Integer);
  end;

  var
  FmdmkMsg: TFmdmkMsg;

implementation

{$R *.DFM}

procedure TFmdmkMsg.AtivaEspera(TempoCiclo: Integer);
begin
  if TempoCiclo > 0 then
    Timer1.Interval := TempoCiclo
  else
    Timer1.Interval := 1000;
  BtYes.Enabled := False;
  BtCancel.Enabled := False;
  BtNo.Enabled := False;
  Timer1.Enabled := False;
end;

procedure TFmdmkMsg.BitBtn1Click(Sender: TObject);
{
var
  Mail: String;
  //a: TStringList;
  I: Integer;
}
begin
{
  ReAviso.SelectAll;
  ReAviso.CopyToClipboard;
  Mail := 'mailto: marco@dermatek.com.br;marcelo@dermatek.com.br?subject=' +
    'Erro no ' + Application.Title + ' vers�o ' + APP_VERSAO + '&body=';
  for I := 0 to ReAviso.Lines.Count -1 do
    Mail := Mail + ' ' + ReAviso.Lines[I];
  ShellExecute(GetDesktopWindow, 'open', pchar(Mail), nil, nil, sw_ShowNormal);
}
  ShowMessage('Este recurso n�o est� mais dispon�vel!' + sLineBreak +
	'Precione a tecla F1 do teclado e crie uma solicita��o!');
end;

procedure TFmdmkMsg.BitBtn2Click(Sender: TObject);
var
  Caminho: String;
begin
  Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
  if FileExists(Caminho) then
    DeleteFile(Caminho);
  ReAviso.Lines.SaveToFile(Caminho);
  ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
end;

procedure TFmdmkMsg.BtCancelClick(Sender: TObject);
begin
  FResult := ID_CANCEL;
  Close;
end;

procedure TFmdmkMsg.BtNoClick(Sender: TObject);
begin
  FResult := ID_NO;
  Close;
end;

procedure TFmdmkMsg.BtYesClick(Sender: TObject);
begin
  FResult := ID_YES;
  Close;
end;

procedure TFmdmkMsg.FormActivate(Sender: TObject);
begin
  //MLAGeral.CorIniComponente;
end;

procedure TFmdmkMsg.FormCreate(Sender: TObject);
begin
  FResult  := ID_CANCEL;
  FEmEspera := False;
  FormResize(Self);
end;

procedure TFmdmkMsg.FormResize(Sender: TObject);
var
  P: Integer;
begin
  //MLAGeral.LoadTextBitmapToLMDPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
  P := (Width - 64) div 2;
  ImageErro.Left := P;
  ImageWarn.Left := P;
  ImageQues.Left := P;
  ImageInfo.Left := P;
end;

procedure TFmdmkMsg.Timer1Timer(Sender: TObject);
begin
  PB1.Position := VAR_CAN_CLOSE_FORM_POS;
  if VAR_CAN_CLOSE_FORM_MSG then
  begin
    Timer1.Enabled := False;
    Close;
  end;
end;

end.
