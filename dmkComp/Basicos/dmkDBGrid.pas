unit dmkDBGrid;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Grids, DB, DBGrids, ExtCtrls, StdCtrls, TypInfo,
  dmkEdit;
  // ini Delphi 28 Alexandria
  //mySQLDBTables,
  //UnInternalConsts2;
  // fim Delphi 28 Alexandria


type

  ///	<summary>
  ///	  teste
  ///	</summary>
  ///	<remarks>
  ///	  <para>
  ///	    kjsdjsdbjsd af ef edf dsf sdf sdf d
  ///	  </para>
  ///	</remarks>
  TdmkDBGrid = class(TCustomDBGrid)
  private
    FGrid3D: Boolean;
    FBoolAsCheck: Boolean;
    FFieldToSort: String;
    FFieldSorted: String;
    FFieldsCalcToOrder: TStrings;
    FSortOrder: Integer;
    procedure SetGrid3D(Value : Boolean);
    procedure SetBoolAsCheck(Value : Boolean);
    procedure SetFieldsCalcToOrder(Value: TStrings);
    procedure DrawArrow(ACanvas: TCanvas; const ARect: TRect);
    function ObtemTexto(Campo: Tfield): String;
    function TitleOffset: Byte;
  protected
    procedure DrawCell(ACol, ARow: Longint;ARect: TRect; AState: TGridDrawState); override;
    function  CanEditShow: Boolean; override;
    function  IsBoolCheck: Boolean; virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure KeyPress(var Key: Char); override;
    function IsMySQLBoolean(ACol: Integer): Boolean;
    //
    procedure SetParent(Value: TWinControl); override;
  public
    constructor Create(AOwner : TComponent); override;
    destructor  Destroy; override;
    property Canvas;
    property SelectedRows;

    procedure TitleClick(Column: TColumn); override;

  published
    property Align;
    property BoolAsCheck : boolean read FBoolAsCheck write SetBoolAsCheck default True;
    property BorderStyle;
    property Columns stored StoreColumns;
    property Color default clBtnFace;
    property Ctl3D;
    property DataSource;
    property DefaultDrawing default False;
    property DragCursor;
    property DragMode;
    property Enabled;
    property FixedColor;
    property Font;
    property SortOrder: Integer read FSortOrder write FSortOrder default 0;
    property Grid3D : boolean read FGrid3D write SetGrid3D default True;
    property Options;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property TitleFont;
    property Visible;
    property OnCellClick;
    property OnColEnter;
    property OnColExit;
    property OnColumnMoved;
    property OnDrawDataCell;
    property OnDrawColumnCell;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEditButtonClick;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDrag;
    property FieldsCalcToOrder: TStrings read FFieldsCalcToOrder write SetFieldsCalcToOrder; {^1^}
    //
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
  end;
  TdmkDBGridOptions = set of TDBGridOption;

procedure Register;

implementation

uses dmkGeral;

function Max( Frst,Sec : LongInt ): LongInt;
begin
  if Frst >= Sec then
    Result := Frst
  else
    Result := Sec
end;

procedure DrawCheck( ACanvas: TCanvas; const ARect: TRect; Checked:Boolean );
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;
  B          : TRect;
  DrawBitmap : TBitmap;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      Height := Max(Height, Bottom - Top);
      Width := Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := ((Left+Right)-Dimension) shr 1;
      Right  := Left+Dimension;
    end;
    Frame3d(ACanvas, TempRect, clBtnShadow, clBtnHighLight, 1);
    Frame3d(ACanvas, TempRect, clBlack, clBlack, 1);
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := clWindow;
      FillRect(TempRect);
    end;
    if Checked then
    begin
      with ACanvas,TempRect do
      begin
        Pen.Color := clBlack;
        Pen.Width := 1;
        MoveTo( Left+1,Top+2 );
        LineTo( Right-2,Bottom-1);
        MoveTo( Left+1,Top+1);
        LineTo( Right-1,Bottom-1);
        MoveTo( Left+2,Top+1);
        LineTo( Right-1,Bottom-2);

        MoveTo( Left+1,Bottom-3);
        LineTo( Right-2,Top);
        MoveTo( Left+1,Bottom-2);
        LineTo( Right-1,Top);
        MoveTo( Left+2,Bottom-2);
        LineTo( Right-1,Top+1);
      end;
    end;
    ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;
  finally
    DrawBitmap.Free;
  end;
end;

constructor TdmkDBGrid.Create(AOwner : TComponent);
{var
  i, j, k: Integer;}
begin
  inherited Create(AOwner);

  Canvas.Brush.Color  := FixedColor;
  Grid3d              := True;
  FBoolAsCheck        := True;
  DefaultDrawing      := False;

  Options := Options - [dgEditing];

  FFieldsCalcToOrder := TStringList.Create;
end;

destructor TdmkDBGrid.Destroy;
begin
  inherited Destroy;

  FFieldsCalcToOrder.Free;
end;

procedure TdmkDBGrid.SetParent(Value: TWinControl);
begin
  inherited SetParent(Value);
end;

procedure TdmkDBGrid.SetFieldsCalcToOrder(Value: TStrings);
begin
  if FFieldsCalcToOrder.Text <> Value.Text then
    FFieldsCalcToOrder.Assign(Value);
end;

function TdmkDBGrid.IsMySQLBoolean(ACol: Integer): Boolean;
var
  MaxValue: Integer;
begin
  Result := Columns[ACol-1].Field.DataType = ftSmallint;
  if Result and (Datasource <> nil) then
  begin
    if GetPropInfo(DataSource.DataSet.FieldByName(
    Columns[ACol-1].Field.FieldName).ClassInfo, 'MaxValue') <> nil then
    begin
      MaxValue := GetPropValue(
        DataSource.DataSet.FieldByName(Columns[ACol-1].Field.FieldName), 'MaxValue');
      Result := MaxValue = 1;
    end else Result := False;
  end;
end;

procedure TdmkDBGrid.DrawArrow(ACanvas: TCanvas; const ARect: TRect);
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  (*OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;*)
  B          : TRect;
  DrawBitmap : TBitmap;
  i, k: Integer;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      //FSQL.Text := '';
      //FSQL.Add('!Width: '  + IntToStr(Arect.Right-Arect.Left));
      Left := Right - 13;
      Height := Max(Height, Bottom - Top);
      Width := 13;//Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
      {FSQL.Add('!Right: '  + IntToStr(Right));
      FSQL.Add('!Left: '   + IntToStr(Left));
      FSQL.Add('!Bottom: ' + IntToStr(Bottom));
      FSQL.Add('!Top: '    + IntToStr(Top));
      FSQL.Add('');}
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := (({Left+}Right)-Dimension)-2;// shr 1;
      Right  := Left+Dimension;
    end;
    //Parei Aqui
    (*Frame3d(ACanvas, TempRect, clBtnShadow, clBtnHighLight, 1);
    Frame3d(ACanvas, TempRect, clBlack, clBlack, 1);*
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := clWindow;
      FillRect(TempRect);
    end;*)
    with ACanvas,TempRect do
    begin
      if FSortOrder = 0{ASCendente} then
      begin
        for i := Left + 1 to Left + 7 do Pixels[i, Top] := $0099A8AC;
        k := 0;
        for i := Top + 1 to Top + 7 do
        begin
          inc(k, 1);
          Pixels[Left + 1 + (k div 2), i] := $0099A8AC;
          Pixels[Left + 1 + (6 -(k div 2)), i] := $0099A8AC;
        end;
      end else
      begin
        for i := Left + 1 to Left + 7 do Pixels[i, Top + 7] := $0099A8AC;
        k := 0;
        for i := Top + 6 downto Top do
        begin
          inc(k, 1);
          Pixels[Left + 1 + (k div 2), i] := $0099A8AC;
          Pixels[Left + 1 + (6 -(k div 2)), i] := $0099A8AC;
        end;
      end;
    end;
    (*ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;*)
  finally
    DrawBitmap.Free;
  end;
end;

procedure DrawCheck2(ACanvas: TCanvas; const ARect: TRect; Checked: Integer);
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;
  B          : TRect;
  DrawBitmap : TBitmap;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      Height := Max(Height, Bottom - Top);
      Width := Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := 13;//ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := ((Left+Right)-Dimension) shr 1;
      Right  := Left+Dimension;
    end;
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := $00F2F3EF;
      FillRect(TempRect);
    end;
    with ACanvas,TempRect do
    begin
      Pen.Color := $00838481;
      Pen.Width := 1;
      MoveTo(Left , Top-1);
      LineTo(Right-1, Top-1);
      LineTo(Right-1, Bottom-1);
      LineTo(Left , Bottom-1);
      LineTo(Left, Top-1);
    end;
    if Checked > 0 then
    begin
      with ACanvas,TempRect do
      begin
        if Checked = 1 then
          Pen.Color := clBlack
        else
          Pen.Color := clSilver; //$00E0E0E0;
        //
        Pen.Width := 1;

        MoveTo( Left + 3, Top + 4);
        LineTo( Left + 6, Top + 7);

        MoveTo( Left +  3, Top +  5);
        LineTo( Left +  6, Top +  8);

        MoveTo( Left +  3, Top +  6);
        LineTo( Left +  6, Top +  9);

        MoveTo( Left +  6, Top +  5);
        LineTo( Left + 10, Top +  1);

        MoveTo( Left +  6, Top +  6);
        LineTo( Left + 10, Top +  2);

        MoveTo( Left +  6, Top +  7);
        LineTo( Left + 10, Top +  3);

        Pen.Color := clSilver;

        MoveTo( Left +  3, Top +  7);
        LineTo( Left +  6, Top + 10);

        MoveTo( Left +  6, Top +  8);
        LineTo( Left + 10, Top +  4);

      end;
    end;
    ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;
  finally
    DrawBitmap.Free;
  end;
end;

procedure WriteText(ACanvas: TCanvas; ARect: TRect; DX, DY: Integer;
  const Text: string; Alignment: TAlignment);
const
  AlignFlags : array [TAlignment] of Integer =
    ( DT_LEFT or DT_WORDBREAK or DT_EXPANDTABS or DT_NOPREFIX,
      DT_RIGHT or DT_WORDBREAK or DT_EXPANDTABS or DT_NOPREFIX,
      DT_CENTER or DT_WORDBREAK or DT_EXPANDTABS or DT_NOPREFIX );
var
  B, R: TRect;
  I, Left: Integer;
  DrawBitmap : TBitmap;
  OrigRect: TRect;
  //OldColor: TColor;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    I := ColorToRGB(ACanvas.Brush.Color);
    if Integer(GetNearestColor(ACanvas.Handle, I)) = I then
    begin
      case Alignment of
        taLeftJustify:
          Left := OrigRect.Left + DX;
        taRightJustify:
          Left := OrigRect.Right - ACanvas.TextWidth(Text) - 3;
      else
        Left := OrigRect.Left + (OrigRect.Right - OrigRect.Left) shr 1
          - (ACanvas.TextWidth(Text) shr 1);
      end;
      (*OldColor := ACanvas.Font.Color;
      if ACanvas.Font.Color =  'then
        ACanvas.Font.Color := clBlue;*)
      ExtTextOut(ACanvas.Handle, Left, OrigRect.Top + DY, ETO_OPAQUE or
        ETO_CLIPPED, @OrigRect, PChar(Text), Length(Text), nil);
      //ACanvas.Font.Color := OldColor;
    end
    else begin
      with DrawBitmap, OrigRect do
      begin
        Width := Max(Width, Right - Left);
        Height := Max(Height, Bottom - Top);
        R := Rect(DX, DY, Right - Left - 1, Bottom - Top - 1);
        B := Rect(0, 0, Right - Left, Bottom - Top);
      end;
      with DrawBitmap.Canvas do
      begin
        Font := ACanvas.Font;
        Font.Color := ACanvas.Font.Color;
        Brush := ACanvas.Brush;
        Brush.Style := bsSolid;
        FillRect(B);
        SetBkMode(Handle, TRANSPARENT);
        //OldColor := Font.Color;
        //if Font.Color = clWhite then
          //Font.Color := clYellow;
        DrawText(Handle, PChar(Text), Length(Text), R, AlignFlags[Alignment]);
        //Font.Color := OldColor;
      end;
      ACanvas.CopyRect(OrigRect, DrawBitmap.Canvas, B);
    end;
  finally
    DrawBitmap.Free;
  end;
end;

{ dmkDBGrid3D}

// N�o existe
{procedure TdmkDBGrid.Enter;
begin
  for i := 0 to SQLFields.Count - 1 do
  begin
    k := 0;
    for j := 0 to Columns.Count - 1 do
      if SQLFields.IndexOf(Columns[Col -1].FieldName) = i then inc(k);
    if k = 0 then
      Application.MessageBox(PChar('O campo de atualiza��o "' + SQLFields[i] +
      ' n�o possui coluna associada!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  inherited OnEnter;
end;}

{procedure TdmkDBGrid.SetParent(Value: TWinControl);
begin
  if (Owner = nil) or not (csDestroying in Owner.ComponentState) then
    FdmkEdit.Parent := Value;
  inherited SetParent(Value);
end;}

function TdmkDBGrid.TitleOffset: Byte;
begin
    if dgTitles in Options then
        Result := 1
    else
        Result := 0;
end;

procedure TdmkDBGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
  function RowIsMultiSelected: Boolean;
  var
    Index, OldActive: Integer;
  begin
      Result := False;
      if Datalink.Active then
      begin
          OldActive := DataLink.ActiveRecord;
          try
              Datalink.ActiveRecord := ARow - TitleOffset;
              Result := (dgMultiSelect in Options)
                        and SelectedRows.Find(Datalink.DataSet.Bookmark, Index);
          finally
              Datalink.ActiveRecord := OldActive;
          end;
      end;
  end;

var
  DrawColumn : TColumn;
  OldActive  : Integer;
  Value      : Integer;
  ValueStr   : String;
  OldColor: TColor;
begin
  if FGrid3D then DefaultDrawing := False;
  Canvas.FillRect( ARect );
  inherited DrawCell(ACol, ARow, ARect, AState);
  {if FdmkEdit.Visible = True then
  begin
    R := CellRect(ACol, ARow);
    R.Left   := R.Left   + Left + 1;
    if R.Left = FdmkEdit.Left then Exit;
  end;  }

  if (ARow = 0) and (ACol >0) then
  begin
    // For�a mostrar s� desenho
    //Columns[ACol-1].ReadOnly := True;
    // Desenha
    if (Columns[ACol - 1].FieldName = FFieldToSort) then
      DrawArrow(Canvas, ARect);
  end;
  if FGrid3D and ([dgRowLines, dgColLines] * Options =
      [dgRowLines, dgColLines]) then
  begin
    if not (gdFixed in AState) then
    begin
      if (ACol <= Columns.Count) and assigned(Columns[ACol-1].Field) then
      begin
        DrawColumn := Columns[ACol-1];
        if FBoolAsCheck and
        ((Columns[ACol-1].Field.DataType = ftBoolean) or (IsMySQLBoolean(ACol))) then
        begin
          OldActive := DataLink.ActiveRecord;
          try
            DataLink.ActiveRecord := ARow-1;
            if Assigned(DrawColumn.Field) then
            begin
              Value := DrawColumn.Field.AsInteger;
              DrawCheck2(Canvas, ARect, Value)
            end;
          finally
            DataLink.ActiveRecord := OldActive;
          end;
        end
        else begin
          OldActive := DataLink.ActiveRecord;
          try
            DataLink.ActiveRecord := ARow-1;
            if Assigned(DrawColumn.Field) then
            begin
              OldColor := Font.Color;

              // 2013-07-01
              //if (gdRowSelected in AState) then
              if RowIsMultiSelected then
              begin
                // Cor celulas
  // ini Delphi 28 Alexandria
                Canvas.Brush.Color := clBtnShadow;
                //Canvas.Brush.Color := VAR_DMK_COR_csButtonShadow;
  // ini Delphi 28 Alexandria
                //Canvas.Font.Color := clBlack;  e o vermelho dos textos em destaque?
                Canvas.FillRect(ARect);
              end else
              if (gdSelected in AState) and (Focused = False) then
              begin
                // Cor celulas
                Canvas.Brush.Color := clWindow;
                Canvas.Font.Color := clWindowText;
                Canvas.FillRect(ARect);
              end else
              if (gdSelected in AState) then
              begin
                if Focused = True then
                begin
  // ini Delphi 28 Alexandria
                  Canvas.Brush.Color := clBtnShadow;
                  //Canvas.Brush.Color := VAR_DMK_COR_csButtonShadow;
  // ini Delphi 28 Alexandria
                  //Canvas.Font.Color := clBlack;  e o vermelho dos textos em destaque?
                  Canvas.FillRect(ARect);
                end else
                if (not (gdFocused in AState) or
                 ([
                    DBGrids.TDBGridOption(goDrawFocusSelected),
                    DBGrids.TDBGridOption(goRowSelect)
                 ] * Options <> [])) then
                begin
                  // Ver o que fazer!
                  Canvas.Brush.Color := clGreen;
                  Canvas.Font.Color  := clRed;
                  Canvas.FillRect(ARect);
                end;
              end else begin
                if Canvas.Brush.Color = Font.Color then
                begin
                  if Font.Color = clWhite then
                    Font.Color := clBlack
                  else
                    Font.Color := clWhite;
                end;
                // FIM 2013-07-01
              end;
              ValueStr := '';
              if DataSource.DataSet.State <> dsInactive then
                if DataSource.DataSet.RecordCount > 0 then
                  ValueStr := ObtemTexto(DrawColumn.Field);
              WriteText(Canvas, ARect, 2, 2, ValueStr, Columns[ACol-1].Alignment);
              Font.Color := OldColor;
            end;
          finally
            DataLink.ActiveRecord := OldActive;
          end;
        end;
      end;
    end;
    with ARect,Canvas do
    begin
      if (gdFixed in AState) then
        Frame3d(Canvas,ARect,clBtnHighLight,clBtnShadow,2)
      else begin
        Pen.Color := clBtnHighLight;
        PolyLine([Point(Left,Bottom-1), Point(Left,Top), Point(Right,Top)]);
        Pen.Color := clBtnShadow;
        PolyLine([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top-1)]);
      end;
    end;
  end;
end;

procedure TdmkDBGrid.SetGrid3D(Value : Boolean);
begin
  if FGrid3D<>Value then
  begin
    FGrid3D:=Value;
    DefaultDrawing := not FGrid3d;
    FBoolAsCheck := FGrid3d;
    Invalidate;
  end;
end;

procedure TdmkDBGrid.SetBoolAsCheck(Value : Boolean);
begin
  if not FGrid3D then
  begin
    if FBoolAsCheck then
    begin
      FBoolAsCheck := False;
      Invalidate;
    end;
    Exit;
  end;
  if FBoolAsCheck<>Value then
  begin
    FBoolAsCheck:= Value;
    Invalidate;
  end;
end;

function TdmkDBGrid.IsBoolCheck: Boolean;
begin
  Result := False;
  if (FGrid3D=true) and ([dgRowLines, dgColLines] * Options =
      [dgRowLines, dgColLines]) then
  begin
    if assigned(Columns[Col-1].Field) then
    begin
      if (Columns[Col-1].Field.DataType = ftBoolean) and FBoolAsCheck then
        Result := True;
    end;
  end;
end;

function TdmkDBGrid.CanEditShow: Boolean;
begin
  Result := not IsBoolCheck and inherited CanEditShow;
end;

procedure TdmkDBGrid.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  Cell  : TGridCoord;
  OldCol: Integer;
  OldRow: Integer;
  State : Boolean;
  Rect  : TRect;
  OldBrush: TBrush;
  OldPen  : TPen;
begin
  OldCol := Col;
  OldRow := Row;
  inherited MouseDown(Button, Shift, X, Y);
  if ((Columns[Col-1].FieldName = '')) then Exit;
  Cell := MouseCoord(X, Y);
  if FBoolAsCheck and (Columns[Col-1].Field <> nil) and
  ((Columns[Col-1].Field.DataType = ftBoolean) or (IsMySQLBoolean(Col))) then
  //if IsBoolCheck then
  begin
    if DataLink.Active and (Cell.X=OldCol) and (Cell.Y=OldRow) then
    begin
      //if DataLink.DataSet is TmySQLTable then
      if Lowercase(DataLink.DataSet.ClassName) = Lowercase('TmySQLTable') then
      begin
        if DataLink.DataSet.State = dsBrowse then
          DataLink.DataSet.Edit;
        if (DataLink.DataSet.State = dsEdit) or (DataLink.DataSet.State = dsInsert) then
        begin
          if Columns[Col-1].Field.DataType = ftBoolean then
          begin
            Columns[Col-1].Field.AsBoolean := not Columns[Col-1].Field.AsBoolean;
            State := Columns[Col-1].Field.AsBoolean;
          end else begin
            // ?????? Evitar erro
            if Columns[Col-1].Field.AsInteger = 1 then
            begin
              Columns[Col-1].Field.AsInteger := 1;
              State := True;
            end else begin
              Columns[Col-1].Field.AsInteger := 0;
              State := False;
            end;
          end;

          Rect := BoxRect( Col,Row,Col,Row );
          with Canvas, Rect do
          begin
            OldBrush := Brush;
            OldPen   := Pen;
            Brush.Color := clHighLight;
            DrawCheck(Canvas, Rect, State);
            Pen.Color := clBtnHighLight;
            PolyLine([Point(Left,Bottom-1), Point(Left,Top), Point(Right,Top)]);
            Pen.Color := clBtnShadow;
            PolyLine([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top-1)]);
            Brush := OldBrush;
            Pen   := OldPen;
          end;
        end;
      end;
    end;
  end;
end;

procedure TdmkDBGrid.KeyPress(var Key: Char);
var
  OldBrush : TBrush;
  OldPen   : TPen;
  State    : Boolean;
  Rect     : TRect;
begin
  if not (dgAlwaysShowEditor in Options) and (Key = #13) and IsBoolCheck then
  begin
    if DataLink.Active then
    begin
      if DataLink.DataSet.State = dsBrowse then
        DataLink.DataSet.Edit;
      if (DataLink.DataSet.State = dsEdit) or (DataLink.DataSet.State = dsInsert) then
      begin
        Columns[Col-1].Field.AsBoolean := not Columns[Col-1].Field.AsBoolean;
        State := Columns[Col-1].Field.AsBoolean;
        Rect := BoxRect( Col,Row,Col,Row );
        with Canvas, Rect do
        begin
          OldBrush := Brush;
          OldPen   := Pen;
          Brush.Color := clHighLight;
          DrawCheck(Canvas, Rect, State);
          Pen.Color := clBtnHighLight;
          PolyLine([Point(Left,Bottom-1), Point(Left,Top), Point(Right,Top)]);
          Pen.Color := clBtnShadow;
          PolyLine([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top-1)]);
          Brush := OldBrush;
          Pen   := OldPen;
        end;
      end;
    end;
  end;
  inherited KeyPress(Key);
end;

procedure TdmkDBGrid.TitleClick(Column: TColumn);
  function SortTxt(Fields: String; Order: Integer): String;
  begin
    case Order of
      0: Result := Fields + ' ASC';
      1: Result := Fields + ' DESC';
      else Result := ' ';
    end;
  end;
  var
    Invert: Boolean;
    i, p: Integer;
    NewFldToOrd, NewFldOrded, aux1, aux2: String;
begin
  inherited TitleClick(Column);
  try
    Screen.Cursor := crHourGlass;
    Invert := False;
    if Column.FieldName <> FFieldToSort then
    begin
      NewFldToOrd := Column.FieldName;
      NewFldOrded := Column.FieldName;
      for i := 0 to FFieldsCalcToOrder.Count -1 do
      begin
        p := pos('=', FFieldsCalcToOrder[i]);
        aux1 := Copy(FFieldsCalcToOrder[i], 1, p-1);
        aux2 := Copy(FFieldsCalcToOrder[i], p+1, Length(FFieldsCalcToOrder[i]));
        if (aux1 = Column.FieldName) then
        begin
          if (aux2 = FFieldSorted) then
          begin
            Invert := True;
            NewFldOrded := aux2;
            Break;
          end else
            NewFldOrded := aux2;
        end;
      end;
      FFieldToSort := NewFldToOrd;
      FFieldSorted := NewFldOrded;
    end else begin
      Invert := True;
      if FFieldToSort = '' then
        FFieldToSort := Column.FieldName;
      if FFieldSorted = '' then
        FFieldSorted := Column.FieldName;
    end;
    if Invert then
    begin
      if FSortOrder = 0 then
        FSortOrder := 1
      else
        FSortOrder := 0;
    end;
    if DataSource <> nil then
      if GetPropInfo(DataSource.DataSet.ClassInfo, 'SortFieldNames') <> nil then
        SetPropValue(DataSource.DataSet, 'SortFieldNames',
          SortTxt(FFieldSorted, FSortOrder));
    Invalidate;
  finally
    Screen.Cursor := crDefault;
  end;    
end;

function TdmkDBGrid.ObtemTexto(Campo: Tfield): String;
begin
  case Campo.DataType of
    ftFloat, ftCurrency :
    begin
      if TFloatField(Campo).DisplayFormat <> '' then
        Result := FormatFloat(TFloatField(Campo).DisplayFormat, Campo.AsFloat)
      else
        Result :=  Campo.AsString;
    end;

    ftWord, ftSmallint, ftInteger, ftLargeint:
    begin
      if TIntegerField(Campo).DisplayFormat <> '' then
        Result := FormatFloat(TIntegerField(Campo).DisplayFormat, Campo.AsInteger)
      else
        Result :=  Campo.AsString;
    end;

    ftDate, ftTime, ftDateTime:
    begin
      if TDateTimeField(Campo).DisplayFormat <> '' then
        Result := FormatDateTime(TDateTimeField(Campo).DisplayFormat, Campo.AsDateTime)
      else
        Result :=  Campo.AsString;
    end;

    else Result :=  Campo.AsString;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkDBGrid]);
end;

end.
