unit dmkRichEdit;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, ComCtrls, dmkGeral, UnDmkEnums;

type
  TdmkRichEdit = class(TRichEdit)
  private
    { Private declarations }
    FQryName: String;
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FOldValor: String;

    procedure SetQryName(Value: String);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetOldValor(Value: String);
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property QryName: String read FQryName write SetQryName;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property OldValor: String read FOldValor write SetOldValor;
  end;

procedure Register;

implementation

procedure TdmkRichEdit.SetOldValor(Value: String);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkRichEdit.SetQryName(Value: String);
begin
  if Value <> FQryName then
  begin
    FQryName := Value;
  end;
end;

procedure TdmkRichEdit.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkRichEdit.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkRichEdit.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkRichEdit]);
end;

end.
