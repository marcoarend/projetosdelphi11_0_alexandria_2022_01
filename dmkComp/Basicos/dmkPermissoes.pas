unit dmkPermissoes;

interface

uses
  SysUtils, Classes;

type
  TdmkPermissoes = class(TComponent)
  private
    { Private declarations }
    FCanIns01: TComponent;
    FCanIns02: TComponent;
    FCanIns03: TComponent;
    FCanIns04: TComponent;
    FCanIns05: TComponent;
    //
    FCanUpd01: TComponent;
    FCanUpd02: TComponent;
    FCanUpd03: TComponent;
    FCanUpd04: TComponent;
    FCanUpd05: TComponent;
    //
    FCanDel01: TComponent;
    FCanDel02: TComponent;
    FCanDel03: TComponent;
    FCanDel04: TComponent;
    FCanDel05: TComponent;
    //
    procedure SetCanIns01(Value: TComponent);
    procedure SetCanIns02(Value: TComponent);
    procedure SetCanIns03(Value: TComponent);
    procedure SetCanIns04(Value: TComponent);
    procedure SetCanIns05(Value: TComponent);
    //
    procedure SetCanUpd01(Value: TComponent);
    procedure SetCanUpd02(Value: TComponent);
    procedure SetCanUpd03(Value: TComponent);
    procedure SetCanUpd04(Value: TComponent);
    procedure SetCanUpd05(Value: TComponent);
    //
    procedure SetCanDel01(Value: TComponent);
    procedure SetCanDel02(Value: TComponent);
    procedure SetCanDel03(Value: TComponent);
    procedure SetCanDel04(Value: TComponent);
    procedure SetCanDel05(Value: TComponent);
    //
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property CanIns01: TComponent read FCanIns01 write SetCanIns01;
    property CanIns02: TComponent read FCanIns02 write SetCanIns02;
    property CanIns03: TComponent read FCanIns03 write SetCanIns03;
    property CanIns04: TComponent read FCanIns04 write SetCanIns04;
    property CanIns05: TComponent read FCanIns05 write SetCanIns05;
    //
    property CanUpd01: TComponent read FCanUpd01 write SetCanUpd01;
    property CanUpd02: TComponent read FCanUpd02 write SetCanUpd02;
    property CanUpd03: TComponent read FCanUpd03 write SetCanUpd03;
    property CanUpd04: TComponent read FCanUpd04 write SetCanUpd04;
    property CanUpd05: TComponent read FCanUpd05 write SetCanUpd05;
    //
    property CanDel01: TComponent read FCanDel01 write SetCanDel01;
    property CanDel02: TComponent read FCanDel02 write SetCanDel02;
    property CanDel03: TComponent read FCanDel03 write SetCanDel03;
    property CanDel04: TComponent read FCanDel04 write SetCanDel04;
    property CanDel05: TComponent read FCanDel05 write SetCanDel05;
    //
  end;

procedure Register;

implementation

// Pode incluir
procedure TdmkPermissoes.SetCanIns01(Value: TComponent);
begin
  if Value <> FCanIns01 then
  begin
    FCanIns01 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanIns02(Value: TComponent);
begin
  if Value <> FCanIns02 then
  begin
    FCanIns02 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanIns03(Value: TComponent);
begin
  if Value <> FCanIns03 then
  begin
    FCanIns03 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanIns04(Value: TComponent);
begin
  if Value <> FCanIns01 then
  begin
    FCanIns01 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanIns05(Value: TComponent);
begin
  if Value <> FCanIns05 then
  begin
    FCanIns05 := Value;
  end;
end;

// Pode alterar
procedure TdmkPermissoes.SetCanUpd01(Value: TComponent);
begin
  if Value <> FCanUpd01 then
  begin
    FCanUpd01 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanUpd02(Value: TComponent);
begin
  if Value <> FCanUpd02 then
  begin
    FCanUpd02 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanUpd03(Value: TComponent);
begin
  if Value <> FCanUpd03 then
  begin
    FCanUpd03 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanUpd04(Value: TComponent);
begin
  if Value <> FCanUpd01 then
  begin
    FCanUpd01 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanUpd05(Value: TComponent);
begin
  if Value <> FCanUpd05 then
  begin
    FCanUpd05 := Value;
  end;
end;

// Pode excluir
procedure TdmkPermissoes.SetCanDel01(Value: TComponent);
begin
  if Value <> FCanDel01 then
  begin
    FCanDel01 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanDel02(Value: TComponent);
begin
  if Value <> FCanDel02 then
  begin
    FCanDel02 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanDel03(Value: TComponent);
begin
  if Value <> FCanDel03 then
  begin
    FCanDel03 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanDel04(Value: TComponent);
begin
  if Value <> FCanDel01 then
  begin
    FCanDel01 := Value;
  end;
end;

procedure TdmkPermissoes.SetCanDel05(Value: TComponent);
begin
  if Value <> FCanDel05 then
  begin
    FCanDel05 := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkPermissoes]);
end;

end.
