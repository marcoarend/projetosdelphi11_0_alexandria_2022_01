    // ini Delphi 28 Alexandria
//$I dmk.inc}
    // fim Delphi 28 Alexandria
unit dmkDBGridDAC;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Grids, DB, DBGrids, ExtCtrls, StdCtrls, TypInfo,
  dmkEdit,
  // ini Delphi 28 Alexandria
  //mySQLDBTables,
  // fim Delphi 28 Alexandria
  UnDmkEnums;

type

  TAfterSQLExecEvent = procedure(Sender: TObject) of object;
  TAfterReopenAfterSQLExecEvent = procedure(Sender: TObject) of object;

  TdmkDBGridDAC = class(TCustomDBGrid)
  private
    FAfterSQLExec: TAfterSQLExecEvent;
    FAfterReopenAfterSQLExec: TAfterReopenAfterSQLExecEvent;
    //
    FSQL: TStrings; {^_^}
    FGrid3D: Boolean;
    FBoolAsCheck: Boolean;
    FSortField: String;
    FSortOrder: Integer;
    FFiltraDmk: Boolean;
    //
    //FSQLMyDB: TmySQLDatabase;
    FSQLTable: String;
    FSQLFieldsToChange: TStrings;
    FSQLIndexesOnUpdate: TStrings;
    //
    FGrayField: String;
    //
    FEdit: TdmkEdit;
    {orgWindowProc: TWndMethod;
    FMsg: Cardinal;
    FBuilding: Boolean;
    FMouseInControl: Boolean;
    FLButtonBressed: Boolean;
    FBressed: Boolean;
    FIsKeyDown: Boolean;
    FMyControl: TControl;}
    procedure SetQuery(Value: TStrings);  {^_^}
    procedure SetSQLFieldsToChange(Value: TStrings);  {^1^}
    procedure SetSQLIndexesOnUpdate(Value: TStrings);  {^1^}
    procedure SetGrid3D(Value : Boolean);
    procedure SetBoolAsCheck(Value: Boolean);
    procedure DrawArrow(ACanvas: TCanvas; const ARect: TRect);
    procedure MostraEdit(Col: Integer);
    function LocateValorDeCampo(Campo: String): String;
    function SQLValorDeCampo(Campo: String): String;
    function SQLValorDoEdit(): String;
    function CorrigeValorDouble(Valor: String): String;
    function CorrigeValorData(Valor: String): String;
    function ObtemTexto(Campo: Tfield): String;
    function UpdateCampo(ACol: Integer; NovoVal: String): Boolean;
    procedure SetFiltraDmk(Value: Boolean);
    //
  protected
    procedure DrawCell(ACol, ARow: Longint;ARect: TRect; AState: TGridDrawState); override;
    function  CanEditShow: Boolean; override;
    function  IsBoolCheck: Boolean; virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure KeyPress(var Key: Char); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    function IsMySQLBoolean(ACol: Integer): Boolean;
    //
    //function GetSQLMyDB: TmySQLDatabase; virtual;
    function GetSQLTable: string; virtual;
    //procedure SetSQLMyDB(const Value: string); virtual;
    procedure SetSQLTable(const Value: string); virtual;
    //
    function GetGrayField: string; virtual;
    procedure SetGrayField(const Value: string); virtual;
    //
    procedure SetParent(Value: TWinControl); override;
    function GetMyEditText: string; virtual;
    function GetEditForceNextYear: Boolean; virtual;
    procedure SetMyEditText(const Value: string); virtual;
    procedure SetEditForceNextYear(Value: Boolean);
    procedure EditExit(Sender: TObject);
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    {procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PaintEdit;
    procedure ControlSubClass(Control: TControl; var Message: TMessage);
    procedure PaintControlXP;}
    procedure AfterSQLExec();
    procedure AfterReopenAfterSQLExec();
    // ini Delphi 28 Alexandria
    //function Ativacao(Ativa: Boolean): Boolean;
    // ini Delphi 28 Alexandria
  public
    constructor Create(AOwner : TComponent); override;
    destructor  Destroy; override;
    property Canvas;
    property SelectedRows;

    procedure TitleClick(Column: TColumn); override;
    procedure TitleClickPesq(Column: TColumn);
    procedure CellClick(Column: TColumn); override;
    // ini Delphi 28 Alexandria
    //function AtivaTodos(): Boolean;
    //function DesativaTodos(): Boolean;
    // fim Delphi 28 Alexandria
    function GetSortField(): String;

  published
    property OnAfterSQLExec: TAfterSQLExecEvent read FAfterSQLExec write FAfterSQLExec;
    property OnAfterReopenAfterSQLExec: TAfterReopenAfterSQLExecEvent read FAfterReopenAfterSQLExec write FAfterReopenAfterSQLExec;
    property SQL: TStrings read FSQL write SetQuery; {^_^}
    property SQLFieldsToChange: TStrings read FSQLFieldsToChange write SetSQLFieldsToChange; {^1^}
    property SQLIndexesOnUpdate: TStrings read FSQLIndexesOnUpdate write SetSQLIndexesOnUpdate; {^1^}
    property Align;
    property BoolAsCheck : Boolean read FBoolAsCheck write SetBoolAsCheck default True;
    property BorderStyle;
    property Columns stored StoreColumns;
    property Color default clBtnFace;
    property Ctl3D;
    property DataSource;
    property DefaultDrawing default False;
    property DragCursor;
    property DragMode;
    property Enabled;
    property FixedColor;
    property Font;
    property SortOrder: Integer read FSortOrder write FSortOrder default 0;
    property Grid3D : boolean read FGrid3D write SetGrid3D default True;
    property Options;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property TitleFont;
    property Visible;
    property OnCellClick;
    property OnColEnter;
    property OnColExit;
    property OnColumnMoved;
    property OnDrawDataCell;
    property OnDrawColumnCell;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEditButtonClick;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDrag;
    //
    //
    //property SQLMyDB: TMySQLDatabase read GetSQLMyDB write SetSQLMyDB;
    property SQLTable: string read GetSQLTable write SetSQLTable;
    property GrayField: string read GetGrayField write SetGrayField;
    //property SQLFields: string read GetSQLFields write SetSQLFields;
    property EditText: string read GetMyEditText write SetMyEditText;
    property EditForceNextYear: Boolean read GetEditForceNextYear write SetEditForceNextYear;
    //
    property FiltraDmk: Boolean read FFiltraDmk write SetFiltraDmk default False;
  end;

procedure Register;

implementation

uses dmkGeral;

{ TCampo }

function TdmkDBGridDAC.GetSortField(): String;
begin
  Result := FSortField;
end;

function TdmkDBGridDAC.GetSQLTable: string;
begin
  Result := FSQLTable;
end;

procedure TdmkDBGridDAC.SetSQLTable(const Value: string);
begin
  FSQLTable := Value;
end;

{function TdmkDBGridDAC.GetSQLMyDB: TmySQLDatabase;
begin
  Result := FSQLMyDB;
end;

procedure TdmkDBGridDAC.SetSQLMyDB(const Value: TmySQLDatabase);
begin
  FSQLMyDB := Value;
end;}


//fim TCampo


function Max( Frst,Sec : LongInt ): LongInt;
begin
  if Frst >= Sec then
    Result := Frst
  else
    Result := Sec
end;

procedure DrawCheck(ACanvas: TCanvas; const ARect: TRect; Checked: Boolean;
  GrayField: String; ValCor: Byte);
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;
  B          : TRect;
  DrawBitmap : TBitmap;
  CheckColor : TColor;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      Height := Max(Height, Bottom - Top);
      Width := Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := ((Left+Right)-Dimension) shr 1;
      Right  := Left+Dimension;
    end;
    Frame3d(ACanvas, TempRect, clBtnShadow, clBtnHighLight, 1);
    Frame3d(ACanvas, TempRect, clBlack, clBlack, 1);
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := clWindow;
      FillRect(TempRect);
    end;
    if Checked then
    begin
      with ACanvas,TempRect do
      begin
        if GrayField <> '' then
        begin
          if ValCor = 0 then
            CheckColor := clSilver
          else
            CheckColor := clBlack;
        end else CheckColor := clBlack;
        Pen.Color := CheckColor;
        Pen.Width := 1;
        MoveTo( Left+1,Top+2 );
        LineTo( Right-2,Bottom-1);
        MoveTo( Left+1,Top+1);
        LineTo( Right-1,Bottom-1);
        MoveTo( Left+2,Top+1);
        LineTo( Right-1,Bottom-2);

        MoveTo( Left+1,Bottom-3);
        LineTo( Right-2,Top);
        MoveTo( Left+1,Bottom-2);
        LineTo( Right-1,Top);
        MoveTo( Left+2,Bottom-2);
        LineTo( Right-1,Top+1);
      end;
    end;
    ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;
  finally
    DrawBitmap.Free;
  end;
end;

constructor TdmkDBGridDAC.Create(AOwner : TComponent);
{var
  i, j, k: Integer;}
begin
  inherited Create(AOwner);

  Canvas.Brush.Color  := FixedColor;
  Grid3d:= True;
  FBoolAsCheck   := True;
  DefaultDrawing := False;

  FSQL := TStringList.Create;
  FSQLFieldsToChange := TStringList.Create;
  FSQLIndexesOnUpdate := TStringList.Create;
  //
  Options := Options - [dgEditing];
  //TStringList(SQL).OnChange := QueryChanged;
  //
  FEdit := TdmkEdit.Create(nil);
  //FEdit.AutoSelect := True;
  //FMyControl := FEdit;
  FEdit.Visible := False;
  FEdit.Name := '';
  FEdit.Text := '';
  FEdit.OnExit := EditExit;
  FEdit.OnKeyDown := EditKeyDown;
  //FEdit.OnKeyUp   := EditKeyUp;
  FEdit.BorderStyle := bsNone;
  //FFiltraDmk     := False;
  //
end;

    // ini Delphi 28 Alexandria
{
function TdmkDBGridDAC.DesativaTodos(): Boolean;
begin
  Result := Ativacao(False);
end;
}
    // fim Delphi 28 Alexandria

destructor TdmkDBGridDAC.Destroy;
begin
  try
    SetParent(nil);
    FSQL.Free;
    FSQLFieldsToChange.Free;
    FSQLIndexesOnUpdate.Free;
    if FEdit <> nil then
      FEdit.Parent := nil;
    if (FEdit <> nil) and (FEdit.Parent = nil) then
      FEdit.Free;
  finally
    inherited Destroy;
  end;
end;

procedure TdmkDBGridDAC.SetParent(Value: TWinControl);
begin
  if (Owner = nil) or not (csDestroying in Owner.ComponentState) then
    FEdit.Parent := self;
  inherited SetParent(Value);
end;

function TdmkDBGridDAC.IsMySQLBoolean(ACol: Integer): Boolean;
var
  MaxValue: Integer;
begin
  if ACol = -2 then
  begin
    Result := False;
    Exit;
  end;
  Result := Columns[ACol].Field.DataType = ftSmallint;
  if Result and (Datasource <> nil) then
  begin
    if GetPropInfo(DataSource.DataSet.FieldByName(
    Columns[ACol].Field.FieldName).ClassInfo, 'MaxValue') <> nil then
    begin
      MaxValue := GetPropValue(
        DataSource.DataSet.FieldByName(Columns[ACol].Field.FieldName), 'MaxValue');
      Result := MaxValue = 1;
    end else Result := False;
  end;
end;

procedure TdmkDBGridDAC.DrawArrow(ACanvas: TCanvas; const ARect: TRect);
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  (*OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;*)
  B          : TRect;
  DrawBitmap : TBitmap;
  i, k: Integer;

begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      //FSQL.Text := '';
      //FSQL.Add('!Width: '  + IntToStr(Arect.Right-Arect.Left));
      Left := Right - 13;
      Height := Max(Height, Bottom - Top);
      Width := 13;//Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
      {FSQL.Add('!Right: '  + IntToStr(Right));
      FSQL.Add('!Left: '   + IntToStr(Left));
      FSQL.Add('!Bottom: ' + IntToStr(Bottom));
      FSQL.Add('!Top: '    + IntToStr(Top));
      FSQL.Add('');}
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := (({Left+}Right)-Dimension)-2;// shr 1;
      Right  := Left+Dimension;
    end;
    //Parei Aqui
    (*Frame3d(ACanvas, TempRect, clBtnShadow, clBtnHighLight, 1);
    Frame3d(ACanvas, TempRect, clBlack, clBlack, 1);*
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := clWindow;
      FillRect(TempRect);
    end;*)
    with ACanvas,TempRect do
    begin
      if FSortOrder = 0{ASCendente} then
      begin
        for i := Left + 1 to Left + 7 do Pixels[i, Top] := $0099A8AC;
        k := 0;
        for i := Top + 1 to Top + 7 do
        begin
          inc(k, 1);
          Pixels[Left + 1 + (k div 2), i] := $0099A8AC;
          Pixels[Left + 1 + (6 -(k div 2)), i] := $0099A8AC;
        end;
      end else
      begin
        for i := Left + 1 to Left + 7 do Pixels[i, Top + 7] := $0099A8AC;
        k := 0;
        for i := Top + 6 downto Top do
        begin
          inc(k, 1);
          Pixels[Left + 1 + (k div 2), i] := $0099A8AC;
          Pixels[Left + 1 + (6 -(k div 2)), i] := $0099A8AC;
        end;
      end;
    end;
    (*ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;*)
  finally
    DrawBitmap.Free;
  end;
end;

procedure DrawCheck2(ACanvas: TCanvas; const ARect: TRect; Checked: Boolean;
GrayField: String; ValCor: Byte);
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;
  B          : TRect;
  DrawBitmap : TBitmap;
  CheckColor : TColor;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      Height := Max(Height, Bottom - Top);
      Width := Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := 13;//ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := ((Left+Right)-Dimension) shr 1;
      Right  := Left+Dimension;
    end;
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := $00F2F3EF;
      FillRect(TempRect);
    end;
    with ACanvas,TempRect do
    begin
      Pen.Color := $00838481;
      Pen.Width := 1;
      MoveTo(Left , Top-1);
      LineTo(Right-1, Top-1);
      LineTo(Right-1, Bottom-1);
      LineTo(Left , Bottom-1);
      LineTo(Left, Top-1);
    end;
    if Checked then
    begin
      if GrayField <> '' then
      begin
        if ValCor = 0 then
          CheckColor := clSilver
        else
          CheckColor := clBlack;
      end else CheckColor := clBlack;
      with ACanvas,TempRect do
      begin
        Pen.Color := CheckColor;
        Pen.Width := 1;

        MoveTo( Left + 3, Top + 4);
        LineTo( Left + 6, Top + 7);

        MoveTo( Left +  3, Top +  5);
        LineTo( Left +  6, Top +  8);

        MoveTo( Left +  3, Top +  6);
        LineTo( Left +  6, Top +  9);

        MoveTo( Left +  6, Top +  5);
        LineTo( Left + 10, Top +  1);

        MoveTo( Left +  6, Top +  6);
        LineTo( Left + 10, Top +  2);

        MoveTo( Left +  6, Top +  7);
        LineTo( Left + 10, Top +  3);

        Pen.Color := clSilver;

        MoveTo( Left +  3, Top +  7);
        LineTo( Left +  6, Top + 10);

        MoveTo( Left +  6, Top +  8);
        LineTo( Left + 10, Top +  4);

      end;
    end;
    ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;
  finally
    DrawBitmap.Free;
  end;
end;

procedure WriteText(ACanvas: TCanvas; ARect: TRect; DX, DY: Integer;
  const Text: string; Alignment: TAlignment);
const
  AlignFlags : array [TAlignment] of Integer =
    ( DT_LEFT or DT_WORDBREAK or DT_EXPANDTABS or DT_NOPREFIX,
      DT_RIGHT or DT_WORDBREAK or DT_EXPANDTABS or DT_NOPREFIX,
      DT_CENTER or DT_WORDBREAK or DT_EXPANDTABS or DT_NOPREFIX );
var
  B, R: TRect;
  I, Left: Integer;
  DrawBitmap : TBitmap;
  OrigRect: TRect;
  //OldColor: TColor;
begin
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    I := ColorToRGB(ACanvas.Brush.Color);
    if Integer(GetNearestColor(ACanvas.Handle, I)) = I then
    begin
      case Alignment of
        taLeftJustify:
          Left := OrigRect.Left + DX;
        taRightJustify:
          Left := OrigRect.Right - ACanvas.TextWidth(Text) - 3;
      else
        Left := OrigRect.Left + (OrigRect.Right - OrigRect.Left) shr 1
          - (ACanvas.TextWidth(Text) shr 1);
      end;
      (*OldColor := ACanvas.Font.Color;
      if ACanvas.Font.Color =  'then
        ACanvas.Font.Color := clBlue;*)
      ExtTextOut(ACanvas.Handle, Left, OrigRect.Top + DY, ETO_OPAQUE or
        ETO_CLIPPED, @OrigRect, PChar(Text), Length(Text), nil);
      //ACanvas.Font.Color := OldColor;
    end
    else begin
      with DrawBitmap, OrigRect do
      begin
        Width := Max(Width, Right - Left);
        Height := Max(Height, Bottom - Top);
        R := Rect(DX, DY, Right - Left - 1, Bottom - Top - 1);
        B := Rect(0, 0, Right - Left, Bottom - Top);
      end;
      with DrawBitmap.Canvas do
      begin
        Font := ACanvas.Font;
        Font.Color := ACanvas.Font.Color;
        Brush := ACanvas.Brush;
        Brush.Style := bsSolid;
        FillRect(B);
        SetBkMode(Handle, TRANSPARENT);
        //OldColor := Font.Color;
        //if Font.Color = clWhite then
          //Font.Color := clYellow;
        DrawText(Handle, PChar(Text), Length(Text), R, AlignFlags[Alignment]);
        //Font.Color := OldColor;
      end;
      ACanvas.CopyRect(OrigRect, DrawBitmap.Canvas, B);
    end;
  finally
    DrawBitmap.Free;
  end;
end;

{ dmkDBGrid3D}

// N�o existe
{procedure TdmkDBGridDAC.Enter;
begin
  for i := 0 to SQLFields.Count - 1 do
  begin
    k := 0;
    for j := 0 to Columns.Count - 1 do
      if SQLFields.IndexOf(Columns[Col -1].FieldName) = i then inc(k);
    if k = 0 then
      Geral.MensagemBox('O campo de atualiza��o "' + SQLFields[i] +
      ' n�o possui coluna associada!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  inherited OnEnter;
end;}

{procedure TdmkDBGridDAC.SetParent(Value: TWinControl);
begin
  if (Owner = nil) or not (csDestroying in Owner.ComponentState) then
    FdmkEdit.Parent := Value;
  inherited SetParent(Value);
end;}

procedure TdmkDBGridDAC.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
var
  DrawColumn : TColumn;
  OldActive  : Integer;
  Value      : Boolean;
  ValueStr   : String;
  OldColor   : TColor;
  ValCor     : Byte;
begin
  if FGrid3D then DefaultDrawing := False;
  Canvas.FillRect( ARect );
  inherited DrawCell(ACol, ARow, ARect, AState);
  {if FdmkEdit.Visible = True then
  begin
    R := CellRect(ACol, ARow);
    R.Left   := R.Left   + Left + 1;
    if R.Left = FdmkEdit.Left then Exit;
  end;  }

  if (ARow = 0) and (ACol >0) then
  begin
    // For�a mostrar s� desenho
    //Columns[ACol-1].ReadOnly := True;
    // Desenha
    if (Columns[ACol - 1].FieldName = FSortField) then
      DrawArrow(Canvas, ARect);
  end;
  if FGrid3D and ([dgRowLines, dgColLines] * Options =
      [dgRowLines, dgColLines]) then
  begin
    if not (gdFixed in AState) then
    begin
      if (ACol <= Columns.Count) and assigned(Columns[ACol-1].Field) then
      begin
        DrawColumn := Columns[ACol-1];
        if FBoolAsCheck and
        ((Columns[ACol-1].Field.DataType = ftBoolean) or (IsMySQLBoolean(ACol-1))) then
        begin
          OldActive := DataLink.ActiveRecord;
          try
            DataLink.ActiveRecord := ARow-1;
            if Assigned(DrawColumn.Field) then
            begin
              Value := DrawColumn.Field.AsInteger = 1;
              if FGrayField <> '' then
                ValCor := DataSource.DataSet.FieldByName(FGrayField).AsInteger
              else
                ValCor := 0;
              DrawCheck2(Canvas, ARect, Value, FGrayField, ValCor)
            end;
          finally
            DataLink.ActiveRecord := OldActive;
          end;
        end
        else begin
          OldActive := DataLink.ActiveRecord;
          try
            DataLink.ActiveRecord := ARow-1;
            if Assigned(DrawColumn.Field) then
            begin
              // Cor celulas
              //Canvas.Brush.Color := clFuchsia;
              //Font.Color := clBlue;
              // Parei Aqui
              //Canvas.Brush.Color := Color;
              //
              OldColor := Font.Color;
              if Canvas.Brush.Color = Font.Color then
              begin
                if Font.Color = clWhite then
                  Font.Color := clBlack
                else
                  Font.Color := clWhite;
              end;
              ValueStr := ObtemTexto(DrawColumn.Field);
              WriteText(Canvas, ARect, 2, 2, ValueStr, Columns[ACol-1].Alignment);
              Font.Color := OldColor;
            end;
          finally
            DataLink.ActiveRecord := OldActive;
          end;
        end;
      end;
    end;
    with ARect,Canvas do
    begin
      if (gdFixed in AState) then
        Frame3d(Canvas,ARect,clBtnHighLight,clBtnShadow,2)
      else begin
        Pen.Color := clBtnHighLight;
        PolyLine([Point(Left,Bottom-1), Point(Left,Top), Point(Right,Top)]);
        Pen.Color := clBtnShadow;
        PolyLine([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top-1)]);
      end;
    end;
  end;
end;

procedure TdmkDBGridDAC.SetGrayField(const Value: string);
begin
  FGrayField := Value;
end;

procedure TdmkDBGridDAC.SetGrid3D(Value : Boolean);
begin
  if FGrid3D<>Value then
  begin
    FGrid3D:=Value;
    DefaultDrawing := not FGrid3d;
    FBoolAsCheck := FGrid3d;
    Invalidate;
  end;
end;

procedure TdmkDBGridDAC.SetBoolAsCheck(Value : Boolean);
begin
  if not FGrid3D then
  begin
    if FBoolAsCheck then
    begin
      FBoolAsCheck := False;
      Invalidate;
    end;
    Exit;
  end;
  if FBoolAsCheck<>Value then
  begin
    FBoolAsCheck:= Value;
    Invalidate;
  end;
end;

function TdmkDBGridDAC.IsBoolCheck: Boolean;
begin
  Result := False;
  if (FGrid3D=true) and ([dgRowLines, dgColLines] * Options =
      [dgRowLines, dgColLines]) then
  begin
    if assigned(Columns[Col-1].Field) then
    begin
      if (Columns[Col-1].Field.DataType = ftBoolean) and FBoolAsCheck then
        Result := True;
    end;
  end;
end;

    // ini Delphi 28 Alexandria
{
function TdmkDBGridDAC.AtivaTodos(): Boolean;
begin
  Result := Ativacao(True);
end;
}
    // ini Delphi 28 Alexandria

function TdmkDBGridDAC.CanEditShow: Boolean;
begin
  Result := not IsBoolCheck and inherited CanEditShow;
end;

procedure TdmkDBGridDAC.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  Cell  : TGridCoord;
  OldCol: Integer;
  OldRow: Integer;
  State : Boolean;
  Rect  : TRect;
  OldBrush: TBrush;
  OldPen  : TPen;
  ValCor  : Byte;
begin
  OldCol := Col;
  OldRow := Row;
  inherited MouseDown(Button, Shift, X, Y);
  Cell := MouseCoord(X, Y);
  if IsBoolCheck then
  begin
    if DataLink.Active and (Cell.X=OldCol) and (Cell.Y=OldRow) then
    begin
      if DataLink.DataSet.State = dsBrowse then
        DataLink.DataSet.Edit;
      if (DataLink.DataSet.State = dsEdit) or (DataLink.DataSet.State = dsInsert) then
      begin
        Columns[Col-1].Field.AsBoolean := not Columns[Col-1].Field.AsBoolean;
        State := Columns[Col-1].Field.AsBoolean;
        Rect := BoxRect( Col,Row,Col,Row );
        if FGrayField <> '' then
          ValCor := DataSource.DataSet.FieldByName(FGrayField).AsInteger
        else
          ValCor := 0;
        with Canvas, Rect do
        begin
          OldBrush := Brush;
          OldPen   := Pen;
          Brush.Color := clHighLight;
          DrawCheck(Canvas, Rect, State, FGrayField, ValCor);
          Pen.Color := clBtnHighLight;
          PolyLine([Point(Left,Bottom-1), Point(Left,Top), Point(Right,Top)]);
          Pen.Color := clBtnShadow;
          PolyLine([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top-1)]);
          Brush := OldBrush;
          Pen   := OldPen;
        end;
      end;
    end;
  end;
end;

procedure TdmkDBGridDAC.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
  Cell: TGridCoord;
  //SaveState: TGridState;
begin
  inherited MouseUp(Button, Shift, X, Y);
  if (Button = mbRight) and (Cell.X >= 1) and (Cell.Y >= 0) then
  Cell := MouseCoord(X,Y);
  if Cell.Y < 1 then
  begin
    try
      TitleClickPesq(Columns[RawToDataColumn(Cell.X)]);
    except
      //Geral.MB_Erro('TitleClickPesq(Columns[RawToDataColumn(Cell.X)]):' +
      //#13#10 + 'X = ' + Geral.FF0(X));
    end;
  end;
  //else
    //CellClickLeft(Columns[SelectedIndex]);
end;

procedure TdmkDBGridDAC.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if key in([VK_F2, VK_RETURN]) then
    MostraEdit(Col-1);
end;

procedure TdmkDBGridDAC.KeyPress(var Key: Char);
var
  OldBrush : TBrush;
  OldPen   : TPen;
  State    : Boolean;
  Rect     : TRect;
  ValCor   : Byte;
begin
  if not (dgAlwaysShowEditor in Options) and (Key = #13) and IsBoolCheck then
  begin
    if DataLink.Active then
    begin
      if DataLink.DataSet.State = dsBrowse then
        DataLink.DataSet.Edit;
      if (DataLink.DataSet.State = dsEdit) or (DataLink.DataSet.State = dsInsert) then
      begin
        Columns[Col-1].Field.AsBoolean := not Columns[Col-1].Field.AsBoolean;
        State := Columns[Col-1].Field.AsBoolean;
        Rect := BoxRect( Col,Row,Col,Row );
        if FGrayField <> '' then
          ValCor := DataSource.DataSet.FieldByName(FGrayField).AsInteger
        else
          ValCor := 0;
        with Canvas, Rect do
        begin
          OldBrush := Brush;
          OldPen   := Pen;
          Brush.Color := clHighLight;
          DrawCheck(Canvas, Rect, State, FGrayField, ValCor);
          Pen.Color := clBtnHighLight;
          PolyLine([Point(Left,Bottom-1), Point(Left,Top), Point(Right,Top)]);
          Pen.Color := clBtnShadow;
          PolyLine([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top-1)]);
          Brush := OldBrush;
          Pen   := OldPen;
        end;
      end;
    end;
  end;
  inherited KeyPress(Key);
end;

procedure TdmkDBGridDAC.TitleClick(Column: TColumn);
  function SortTxt(Fields: String; Order: Integer): String;
  begin
    case Order of
      0: Result := Fields + ' ASC';
      1: Result := Fields + ' DESC';
      else Result := ' ';
    end;
  end;
begin
  inherited TitleClick(Column);
  if Column.FieldName <> FSortField then
    FSortField := Column.FieldName
  else if FSortOrder = 0 then
    FSortOrder := 1
  else
    FSortOrder := 0;
  if DataSource <> nil then
    if GetPropInfo(DataSource.DataSet.ClassInfo, 'SortFieldNames') <> nil then
      SetPropValue(DataSource.DataSet, 'SortFieldNames',
        SortTxt(FSortField, FSortOrder));
  Invalidate;
end;

procedure TdmkDBGridDAC.TitleClickPesq(Column: TColumn);
const
  dIni = '/*IniDmkFilter*/';
  dFim = '/*FimDmkFilter*/';
var
  // , FieldName, TxtUsr, TxtDmk
  Texto: String;
  //Ini, Fim: Integer;
  TextoSQL, Novo: String;
  Lista: TStringList;
  I, P: Integer;
  OK: Boolean;
begin
  Texto :=  '';
  if not FiltraDmk then
  begin
    Geral.MensagemBox('N�o � permitido filtros nesta grade!' + #13#10 +
    'Caso queira ativar o filtro, entre em contato com a dermatek!',
    'Informa��o', MB_OK+MB_ICONINFORMATION, 0);
    Exit;
  end;
  if InputQuery('Filtro (use % para m�scara)', 'Informe o texto para o filtro:',
  Texto) then
  begin
    OK := False;
    if Texto <> '' then
      Novo := '(' + Column.FieldName + ' LIKE ''' + Texto + '''' + ')'
    else Novo := '';
    TextoSQL := Trim(DataSource.DataSet.Filter);
    Lista := TStringList.Create;
    try
      P := pos(#13#10, TextoSQL);
      while P > 0 do
      begin
        Lista.Add(Copy(TextoSQL, 1, P-1));
        TextoSQL := Copy(TextoSQL, P+4);
      end;
      if TextoSQL <> '' then
        Lista.Add(TextoSQL);
      for I := 0 to Lista.Count - 1 do
      begin
        if Pos(Column.FieldName, Lista[I]) > 0 then
        begin
          Lista[I] := Novo;
          OK := True;
        end;
      end;
      if not OK then
      begin
        if Lista.Count > 0 then
          Lista.Add('AND');
        Lista.Add(Novo);
      end;
      Novo := '';
      OK := False;
      for I := 0 to Lista.Count - 1 do
      begin
        if Lista[I] <> '' then
        begin
          if OK or (Lista[I] <> 'AND') then
            Novo := Lista[I] + #13#10;
        end;
      end;
      DataSource.DataSet.Filter := Novo;

      if Novo <> '' then
      begin
         DataSource.DataSet.Filtered := True;
         DataSource.DataSet.FilterOptions := [foCaseInsensitive];
      end else begin
         DataSource.DataSet.Filtered := True;
         DataSource.DataSet.FilterOptions := [foCaseInsensitive];
      end;
    finally
      Lista.Free;
    end;
  end;
end;

procedure TdmkDBGridDAC.SetQuery(Value: TStrings);
begin
  if FSQL.Text <> Value.Text then
    FSQL.Assign(Value);
end;

procedure TdmkDBGridDAC.SetSQLFieldsToChange(Value: TStrings);
begin
  if FSQLFieldsToChange.Text <> Value.Text then
    FSQLFieldsToChange.Assign(Value);
end;

procedure TdmkDBGridDAC.SetSQLIndexesOnUpdate(Value: TStrings);
begin
  if FSQLIndexesOnUpdate.Text <> Value.Text then
    FSQLIndexesOnUpdate.Assign(Value);
end;

procedure TdmkDBGridDAC.MostraEdit(Col: Integer);
  procedure ShowEdit(Alinha: TAlignment; Txt: String);
  var
    R: TRect;
  begin
    R := CellRect(Col + 1, Row);
    R.Left   := R.Left   + Left;
    R.Right  := R.Right  + Left;
    R.Top    := R.Top;
    R.Bottom := R.Bottom;
    with FEdit do
    begin
      Left := R.Left +1;//- 2;
      Top := R.Top + 2;// -1;
      Width := (R.Right + 2) - R.Left - 6;
      Height := R.Bottom + 2 - R.Top - 4;
      //
      Text := Txt;
      Visible := True;
      if Visible then SetFocus;
    end;
    //CanSelect := True;
  end;
var
  Coluna: TColumn;
  Campo: TField;
  //
  NovoVal: Integer;
begin
{
  FEdit.Visible := False;
  if DataSource = nil then Exit;
  if Row = 0 then Exit;
  Coluna := Columns[Col];
  Campo := DataSource.DataSet.FieldByName(Coluna.FieldName);
  if SQLFieldsToChange.IndexOf(Coluna.FieldName) > -1 then
  begin
    if Campo.FieldKind = fkData then
    begin
      FEdit.Text := '';
      case Campo.DataType of

        ftWord, ftSmallint, ftInteger, ftLargeint, ftAutoInc: FEdit.FormatType := dmktfInteger;

        ftFloat, ftCurrency: FEdit.FormatType := dmktfDouble;

        ftString, ftWideString, ftMemo, ftWideMemo: FEdit.FormatType := dmktfString;

        ftDate: FEdit.FormatType := dmktfDate;

        ftTime: FEdit.FormatType := dmktfTime;

        ftDateTime: FEdit.FormatType := dmktfDateTime;

        else
        begin
          Geral.MensagemBox('Tipo de dado desconhecido no edit do dmkDBGridEdit!', 'Avise a Dermatek', MB_OK+MB_ICONWARNING, 0);
          FEdit.FormatType := dmktfString;
       end;
      (*TFieldType = (ftUnknown,
        ftBoolean, , ftBCD,
        ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftGraphic, ftFmtMemo,
        ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
        ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
        ftVariant, ftInterface, ftIDispatch, ftGuid);*)
      end;

      //DataSource.DataSet.FieldByName(Coluna := 0;

      case Campo.DataType of
        ftWord, ftSmallint, ftInteger, ftLargeint, ftAutoInc:
        begin
          if IsMySQLBoolean(Col) then
          begin
            case Campo.AsInteger of
              0: NovoVal := 1;
              1: NovoVal := 0;
              else NovoVal := -1;
            end;
            if NovoVal <> -1 then
              UpdateCampo(Col, IntToStr(NovoVal))
            else ShowEdit(taRightJustify, IntToStr(Campo.AsInteger));
          end else
            ShowEdit(taRightJustify, IntToStr(Campo.AsInteger));
        end;
        ftFloat, ftCurrency:
          ShowEdit(taRightJustify, FloatToStr(Campo.AsFloat));

        ftString, ftWideString, ftMemo, ftWideMemo:
          ShowEdit(taLeftJustify, Campo.AsString);

        ftDate:
          ShowEdit(taCenter, FormatDateTime('dd/mm/yyyy', Campo.AsDateTime));

        ftTime:
          ShowEdit(taCenter, FormatDateTime('hh:nn:ss', Campo.AsDateTime));

        ftDateTime:
          ShowEdit(taCenter, FormatDateTime('dd/mm/yyyy hh:nn:ss', Campo.AsDateTime));

        else
        begin
          Geral.MensagemBox('Tipo de dado desconhecido no edit do dmkDBGridEdit!',
            'Avise a Dermatek', MB_OK+MB_ICONWARNING, 0);
          ShowEdit(taCenter, Campo.AsString);
       end;
      (*TFieldType = (ftUnknown,
        ftBoolean, , ftBCD,
        ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftGraphic, ftFmtMemo,
        ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
        ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
        ftVariant, ftInterface, ftIDispatch, ftGuid);*)
      end;

      //PaintEdit;
    end;
  end;
  }
end;

procedure TdmkDBGridDAC.CellClick(Column: TColumn);
begin
  inherited CellClick(Column);
  MostraEdit(Column.Index);
end;

function TdmkDBGridDAC.GetMyEditText: string;
begin
  Result := FEdit.Text;
end;

function TdmkDBGridDAC.GetEditForceNextYear: Boolean;
begin
  Result := FEdit.ForceNextYear;
end;

function TdmkDBGridDAC.GetGrayField: string;
begin
  Result := FGrayField;
end;

procedure TdmkDBGridDAC.SetMyEditText(const Value: string);

begin
  FEdit.Text := Value;
end;

procedure TdmkDBGridDAC.SetEditForceNextYear(Value: Boolean);
begin
  FEdit.ForceNextYear := Value;
end;

procedure TdmkDBGridDAC.SetFiltraDmk(Value: Boolean);
begin
  FFiltraDmk := Value;
end;

procedure TdmkDBGridDAC.EditExit;
begin
  FEdit.Visible := False;
  if Visible then SetFocus;
end;

function TdmkDBGridDAC.UpdateCampo(ACol: Integer; NovoVal: String): Boolean;
    // ini Delphi 28 Alexandria
{
var
  Valor, Avisos, Indices, s: String;
  i, k: Integer;
  myArray: array of Variant;
  FMyQuery: TmySQLQuery;
  // fim Delphi 28 Alexandria
}
begin
    // ini Delphi 28 Alexandria
   Geral.MB_Info('"TdmkDBGridDAC.UpdateCampo()" desativado. Solicite a reativa��o � DERMATEK');
{
  Result := False;
  Avisos := '';
  if (Trim(FSQLTable) = '') then Avisos := Avisos +
    'N�o h� "SQLTable" definido para atualiza��o dos dados!' + Chr(13) + Chr(10);
  Indices := '';
  k := 0;
  for i := 0 to FSQLIndexesOnUpdate.Count -1 do
  begin
    if Trim(FSQLIndexesOnUpdate[i]) <> '' then
    begin
      k := k + 1;
      Indices := Indices + ';' + Trim(FSQLIndexesOnUpdate[i]);
    end;
  end;
  if Length(Indices) > 0 then Indices := Copy(Indices, 2, Length(Indices));
  if Indices = '' then Avisos := Avisos +
    'N�o h� "SQLIndexesOnChange" definidos para atualiza��o dos dados!' + Chr(13) + Chr(10);

  if Avisos <> '' then
  begin
    Geral.MensagemBox(Avisos,
    'Aviso de cancelamento de atualiza��o de dados', MB_OK+ MB_ICONINFORMATION, 0);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FMyQuery := TmySQLQuery.Create(Self);
    FMyQuery.Database := TMySQLQuery(DataSource.Dataset).Database;
    FMyQuery.Close;
    FMyQuery.SQL.Clear;
    FMyQuery.SQL.Add('UPDATE '+ FSQLTable + ' SET ');
    FMyQuery.SQL.Add(Columns[ACol].FieldName + ' = ' + NovoVal);
    SetLength(myArray, k);
    Valor := SQLValorDeCampo(SQLIndexesOnUpdate[0]);
    myArray[0] := LocateValorDeCampo(SQLIndexesOnUpdate[0]);
    s := Valor;
    FMyQuery.SQL.Add('WHERE ' + FSQLIndexesOnUpdate[0] + '=' + Valor);
    k := 0;
    for i := 1 to SQLIndexesOnUpdate.Count -1 do
    begin
      if Trim(FSQLIndexesOnUpdate[i]) <> '' then
      begin
        Valor := SQLValorDeCampo(FSQLIndexesOnUpdate[i]);
        FMyQuery.SQL.Add('AND ' + FSQLIndexesOnUpdate[i] + '=' + Valor);
        inc(k, 1);
        myArray[k] := LocateValorDeCampo(FSQLIndexesOnUpdate[i]);
        s := s + ', ' + Valor;
      end;
    end;
    FSQL.Text := FMyQuery.SQL.Text;
    try
      FMyQuery.ExecSQL;
    except
      ShowMessage(FMyQuery.SQL.Text);
      raise;
    end;  
    Result := True;
    //FEdit.Visible := False;
    if Visible then SetFocus;
    //OnMyOwnEvent();
    AfterSQLExec();
    DataSource.DataSet.Close;
    DataSource.DataSet.Open;
    AfterReopenAfterSQLExec();
    DataSource.DataSet.Locate((Indices), myArray, []);
    if FMyQuery <> nil then
    begin
      FMyQuery.Close;
      FMyQuery.Database := nil;
      FMyQuery.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

    // ini Delphi 28 Alexandria
{
function TdmkDBGridDAC.Ativacao(Ativa: Boolean): Boolean;
var
  NovoVal, (*Valor, *)Avisos, Indices(*, s*): String;
  i, k: Integer;
  myArray: array of Variant;
  FMyQuery: TmySQLQuery;
begin
  Result := False;
  Avisos := '';
  if (Trim(FSQLTable) = '') then Avisos := Avisos +
    'N�o h� "SQLTable" definido para atualiza��o dos dados!' + Chr(13) + Chr(10);
  Indices := '';
  k := 0;
  for i := 0 to FSQLIndexesOnUpdate.Count -1 do
  begin
    if Trim(FSQLIndexesOnUpdate[i]) <> '' then
    begin
      k := k + 1;
      Indices := Indices + ';' + Trim(FSQLIndexesOnUpdate[i]);
    end;
  end;
  if Length(Indices) > 0 then Indices := Copy(Indices, 2, Length(Indices));
  if Indices = '' then Avisos := Avisos +
    'N�o h� "SQLIndexesOnChange" definidos para atualiza��o dos dados!' + Chr(13) + Chr(10);
  if Trim(FSQLFieldsToChange[0]) = '' then Avisos := Avisos +
    'N�o h� "SQLFieldsToChange" definidos para atualiza��o dos dados!' + Chr(13) + Chr(10);
  //
  if Avisos <> '' then
  begin
    Geral.MensagemBox(Avisos,
    'Aviso de cancelamento de atualiza��o de dados', MB_OK+ MB_ICONINFORMATION, 0);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    if Ativa then
      NovoVal := '1'
    else
      NovoVal := '0';
    //
    FMyQuery := TmySQLQuery.Create(Self);
    FMyQuery.Database := TMySQLQuery(DataSource.Dataset).Database;
    FMyQuery.Close;
    FMyQuery.SQL.Clear;
    FMyQuery.SQL.Add('UPDATE '+ FSQLTable + ' SET ');
    FMyQuery.SQL.Add(FSQLFieldsToChange[0] + ' = ' + NovoVal);
    SetLength(myArray, k);
    myArray[0] := LocateValorDeCampo(SQLIndexesOnUpdate[0]);
    (*
    Valor := SQLValorDeCampo(SQLIndexesOnUpdate[0]);
    s := Valor;
    FMyQuery.SQL.Add('WHERE ' + FSQLIndexesOnUpdate[0] + '=' + Valor);
    *)
    k := 0;
    for i := 1 to SQLIndexesOnUpdate.Count -1 do
    begin
      if Trim(FSQLIndexesOnUpdate[i]) <> '' then
      begin
(*
        Valor := SQLValorDeCampo(FSQLIndexesOnUpdate[i]);
        FMyQuery.SQL.Add('AND ' + FSQLIndexesOnUpdate[i] + '=' + Valor);
        s := s + ', ' + Valor;
*)
        inc(k, 1);
        myArray[k] := LocateValorDeCampo(FSQLIndexesOnUpdate[i]);
      end;
    end;
    FSQL.Text := FMyQuery.SQL.Text;
    try
      FMyQuery.ExecSQL;
    except
      ShowMessage(FMyQuery.SQL.Text);
      raise;
    end;
    Result := True;
    if Visible then SetFocus;
    AfterSQLExec();
    DataSource.DataSet.Close;
    DataSource.DataSet.Open;
    AfterReopenAfterSQLExec();
    DataSource.DataSet.Locate((Indices), myArray, []);
    if FMyQuery <> nil then
    begin
      FMyQuery.Close;
      FMyQuery.Database := nil;
      FMyQuery.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TdmkDBGridDAC.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
{var
  Valor, NovoVal, Avisos, Indices, s: String;
  i, k, ACol: Integer;
  myArray: array of Variant;
  FMyQuery: TmySQLQuery;
}
begin
  case Key of
    VK_ESCAPE: FEdit.Visible := False;
    VK_RETURN, VK_TAB, VK_UP, VK_DOWN:
    begin
      {
      Avisos := '';
      if (Trim(FSQLTable) = '') then Avisos := Avisos +
        'N�o h� "SQLTable" definido para atualiza��o dos dados!' + Chr(13) + Chr(10);
      Indices := '';
      k := 0;
      for i := 0 to FSQLIndexesOnUpdate.Count -1 do
      begin
        if Trim(FSQLIndexesOnUpdate[i]) <> '' then
        begin
          k := k + 1;
          Indices := Indices + ';' + Trim(FSQLIndexesOnUpdate[i]);
        end;
      end;
      if Length(Indices) > 0 then Indices := Copy(Indices, 2, Length(Indices));
      if Indices = '' then Avisos := Avisos +
        'N�o h� "SQLIndexesOnChange" definidos para atualiza��o dos dados!' + Chr(13) + Chr(10);

      if Avisos <> '' then
      begin
        Geral.MensagemBox(Avisos),
        'Aviso de cancelamento de atualiza��o de dados', MB_OK+ MB_ICONINFORMATION);
        Exit;
      end;
      Screen.Cursor := crHourGlass;
      try
        Acol := Col;
        NovoVal := SQLValorDoEdit;
        FMyQuery := TmySQLQuery.Create(Self);
        FMyQuery.Database := TMySQLQuery(DataSource.Dataset).Database;
        FMyQuery.Close;
        FMyQuery.SQL.Clear;
        FMyQuery.SQL.Add('UPDATE '+ FSQLTable + ' SET ');
        FMyQuery.SQL.Add(Columns[Col -1].FieldName + ' = ' + NovoVal);
        SetLength(myArray, k);
        Valor := SQLValorDeCampo(SQLIndexesOnUpdate[0]);
        myArray[0] := LocateValorDeCampo(SQLIndexesOnUpdate[0]);
        s := Valor;
        FMyQuery.SQL.Add('WHERE ' + FSQLIndexesOnUpdate[0] + '=' + Valor);
        k := 0;
        for i := 1 to SQLIndexesOnUpdate.Count -1 do
        begin
          if Trim(FSQLIndexesOnUpdate[i]) <> '' then
          begin
            Valor := SQLValorDeCampo(FSQLIndexesOnUpdate[i]);
            FMyQuery.SQL.Add('AND ' + FSQLIndexesOnUpdate[i] + '=' + Valor);
            inc(k, 1);
            myArray[k] := LocateValorDeCampo(FSQLIndexesOnUpdate[i]);
            s := s + ', ' + Valor;
          end;
        end;
        FSQL.Text := FMyQuery.SQL.Text;
        FMyQuery.ExecSQL;
        //FEdit.Visible := False;
        if Visible then SetFocus;
        //OnMyOwnEvent();
        AfterSQLExec();
        DataSource.DataSet.Close;
        DataSource.DataSet.Open;
        AfterReopenAfterSQLExec();
        DataSource.DataSet.Locate((Indices), myArray, []);
        Col := ACol;
        if FMyQuery <> nil then
        begin
          FMyQuery.Close;
          FMyQuery.Database := nil;
          FMyQuery.Free;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
      }

     // ini Delphi 28 Alexandria
     UpdateCampo(Col-1, SQLValorDoEdit);
    // ini Delphi 28 Alexandria

      if (Key in([VK_TAB, VK_UP, VK_DOWN])) then
      begin
        case Key of
          VK_TAB: if Col = ColCount-1 then Col := 1 else Col := Col + 1;
          VK_UP: if Row > 1 then Row := Row-1;
          VK_DOWN: if Row < RowCount - 1 then Row := Row + 1;
        end;
        Invalidate;
        if Visible then SetFocus;
        key := 13;
      end;
    end;
  end;
end;

{procedure TdmkDBGridDAC.EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if FEdit.Visible then
    PaintEdit;
end;}

{procedure TdmkDBGridDAC.PaintEdit;
var
  C: TControlCanvas;
  R: TRect;
  BorderColor: TColor;
begin

  C := TControlCanvas.Create;
  try
    C.Control := FMyControl;

    //XPMenu.SetGlobalColor(C);

    (*if TEdit(Control).Ctl3D <> false then
    begin
      FBuilding := true;
      TEdit(Control).Ctl3D := false;
    end;

    if (FmouseinControl) or (FIsFocused) then
      borderColor := NewColor(C, XPMenu.FFSelectBorderColor, 60)
    else
      borderColor := GetShadeColor(C, Control.Parent.Brush.Color, 60);


    if FBorderStyle = bsNone then
    begin
      if (FmouseinControl) and (not FIsFocused) then
        //borderColor := NewColor(C, Control.Parent.Brush.Color, 60)
        borderColor := NewColor(C, MergColor([TEdit(Control).Color,Control.Parent.Brush.Color]), 40)

      else
        borderColor := TEdit(Control).Color;
    end;*)

    BorderColor := $00A28059;//$00B99D7F;
    R := FMyControl.ClientRect;

    C.Pen.Color := BorderColor;
    //C.Brush.Color := $00B6EFDE;//bsClear;
    C.Brush.Style := bsClear;

    C.Rectangle(R.Left, R.Top, R.Right, R.Bottom);
  finally
    C.Free;
  end;
end;

procedure TdmkDBGridDAC.ControlSubClass(Control: TControl; var Message: TMessage);

begin
  //Call original WindowProc FIRST. We are trying to emulate inheritance, so
  //original WindowProc must handle all messages before we do.


  (*if ((Message.Msg = WM_PAINT) and ((Control is TGraphicControl))) or
    ((Control.ClassName = 'TDBLookupComboBox') and (Message.Msg = WM_NCPAINT)) then
     Message.Result := 1
  else
       //: "Marcus Paulo Tavares" <marcuspt@terra.com.br>*)
    orgWindowProc(Message);

  (*if (XPMenu <> nil) and (not XPMenu.FActive)  then
  begin
    try
      Message.Result := 1;
      if Control <> nil then
      begin
        Control.WindowProc := orgWindowProc;
        if Control is TCustomEdit then
          TEdit(Control).Ctl3D := FCtl3D;
        if Control is TCustomRichEdit then
          TRichEdit(Control).BorderStyle := FBorderStyle;
        if Control.ClassName = 'TDBLookupComboBox' then
          TComboBox(Control).Ctl3D := FCtl3D;
        if Control is TCustomListBox then
          TListBox(Control).BorderStyle := FBorderStyle;
        if Control is TCustomListView then
          TListView(Control).BorderStyle := FBorderStyle;
        if Control is TCustomTreeView then
          TTreeView(Control).BorderStyle := FBorderStyle;
        Control := nil;
        Free;
      end;
      exit;
    except
      exit;
    end;
  end;*)

  FMsg := Message.Msg;
  case Message.Msg of


    EM_GETMODIFY, // For edit
    CM_INVALIDATE:
      begin
        FBuilding := true
      end;

    CM_PARENTCOLORCHANGED:
    begin
      PaintControlXP;
    end;

    WM_DESTROY:
      begin
       if not FBuilding then
       begin
         try
          if Control <> nil then
           begin
             Control.WindowProc := orgWindowProc;
             FBuilding := false;
             Free;
           end;
         except
         end;
         //FBuilding := false;
       end;
       Exit;
      end;

    WM_PAINT:
      begin
        FBuilding := false;
        PaintControlXP;
      end;

    CM_MOUSEENTER:
      if TControl(Control).Enabled then
       begin
//        if FmouseInControl then exit;
        FmouseInControl := true;
        if Control is TGraphicControl then
        begin
          Control.Repaint;
          exit;
        end;
        PaintControlXP;


        (*if Control is TGraphicControl then
        begin
          if not FMouseInControl and Control.Enabled
            and (GetCapture = 0) then
          begin
            FMouseInControl := True;
            Control.Repaint;
          end;
        end
        else
        begin
          FmouseInControl := true;
          PaintControlXP;
        end;*)


      end;
    CM_MOUSELEAVE:
     if TControl(Control).Enabled then
      begin
        FmouseInControl := false;
        if Control is TGraphicControl then
        begin
          Control.Invalidate;
          exit;
        end;
        PaintControlXP;


        (*if Control is TGraphicControl then
        begin
          if FMouseInControl and Control.Enabled then
          begin
            FMouseInControl := False;
            Control.Invalidate;
          end;
        end
        else
        begin
          FmouseInControl := false;
          PaintControlXP;
        end;*)
      end;

    WM_MOUSEMOVE:
      begin
        if TControl(Control).Enabled and (Control.ClassName = 'TUpDown') then
          PaintControlXP;
      end;
    WM_LBUTTONDOWN:
      begin
        FLButtonBressed := true;
        PaintControlXP;
      end;

    WM_LBUTTONUP:
      begin
       FLButtonBressed := false;
       if Control is TGraphicControl then
       begin
         Control.Repaint;
         exit;
       end;
       PaintControlXP;
      end;

    WM_KEYDOWN:
      if Message.WParam = VK_SPACE then
      begin
       FBressed := true;
       if not FIsKeyDown then
         PaintControlXP;
       FIsKeyDown := true;
      end;

    WM_KEYUP:
      if Message.WParam = VK_SPACE then
      begin
        FBressed := false;
        FIsKeyDown := false;
        PaintControlXP;
      end;

    WM_SETFOCUS:
      begin
        FmouseInControl := true;
        PaintControlXP;
      end;
    WM_KILLFOCUS:
      begin
        FmouseInControl := false;
        PaintControlXP;
      end;
    CM_FOCUSCHANGED:    //??
      PaintControlXP;

    CM_EXIT:
      begin
        FmouseInControl := false;
        PaintControlXP;
      end;

    BM_SETCHECK:
      begin
        FmouseInControl := false;
        PaintControlXP;
      end;
    BM_GETCHECK:
      begin
        FmouseInControl := false;
        PaintControlXP;
      end;
    CM_ENABLEDCHANGED:
      begin
        if (Message.WParam = 0) then FmouseInControl := false;//Dirk Bottcher <dirk.boettcher@gmx.net>
        PaintControlXP;
      end;

    CM_TEXTCHANGED:
      begin
        PaintControlXP;
      end;


    CM_CTL3DCHANGED, CM_PARENTCTL3DCHANGED:
      begin
        FBuilding := true;
      end;
    WM_LBUTTONDBLCLK:    //for button, check
      begin
        (*if (Control is TButton) or
           (Control is TSpeedButton) or
           (Control is TCheckBox)  then
          Control.Perform(WM_LBUTTONDOWN, Message.WParam , Longint(Message.LParam));*)
      end;
    //CN_DRAWITEM,
    BM_SETSTATE:
      begin
        PaintControlXP;   // button
      end;
    WM_WINDOWPOSCHANGED, CN_PARENTNOTIFY:     // Moving From parent to other
      begin
        FBuilding := true
      end;
    WM_NCPAINT:
      begin
        (*if (Control is TCustomListBox) or (Control is TCustomTreeView) or
         (Control is TCustomListBox)
        then
          PaintNCWinControl;*)
      end;
  end;

end;

procedure TdmkDBGridDAC.PaintControlXP;
begin
  (*if Control is TCustomRichEdit then
    PaintRichEdit
  else*)
  if FMyControl is TCustomEdit then
    PaintEdit;
end;*}

procedure TdmkDBGridDAC.AfterReopenAfterSQLExec;
begin
  if Assigned(OnAfterReopenAfterSQLExec) then
    OnAfterReopenAfterSQLExec(Self);
end;

procedure TdmkDBGridDAC.AfterSQLExec();
begin
  if Assigned(OnAfterSQLExec) then
    OnAfterSQLExec(Self);
end;

function TdmkDBGridDAC.SQLValorDoEdit(): String;
begin
  case DataSource.DataSet.FieldByName(Columns[Col -1].FieldName).DataType of
    ftWord, ftSmallint, ftInteger, ftLargeint, ftAutoInc: Result := FEdit.Text;

    ftFloat, ftCurrency: Result := CorrigeValorDouble(FEdit.Text);

    ftString, ftWideString, ftMemo, ftWideMemo: Result := '"' + FEdit.Text + '"';

    ftDate: Result := '"' + CorrigeValorData(FEdit.Text) + '"';

    ftTime: Result := '"' + FEdit.Text + '"';

    ftDateTime: Result := '"' + FEdit.Text + '"';
  end;
end;

function TdmkDBGridDAC.SQLValorDeCampo(Campo: String): String;
var
  i: integer;
begin
  Result := '""';
  for i := 0 to DataSource.DataSet.FieldCount - 1 do
  begin
    if DataSource.DataSet.Fields[i].FieldName = Campo then
    begin
      case DataSource.DataSet.FieldByName(Campo).DataType of
        ftWord, ftSmallint, ftInteger, ftLargeint, ftAutoInc: Result :=
        IntToStr(DataSource.DataSet.FieldByName(Campo).AsInteger);

        ftFloat, ftCurrency: Result := CorrigeValorDouble(
        FloatToStr(DataSource.DataSet.FieldByName(Campo).AsFloat));

        ftString, ftWideString, ftMemo, ftWideMemo: Result := '"' +
        DataSource.DataSet.FieldByName(Campo).AsString + '"';

        ftDate: Result := '"' + FormatDateTime('yyyy/mm/dd',
        DataSource.DataSet.FieldByName(Campo).AsDateTime) + '"';

        ftTime: Result := '"' + FormatDateTime('hh:nn:ss',
        DataSource.DataSet.FieldByName(Campo).AsDateTime) + '"';

        ftDateTime: Result := '"' + FormatDateTime('yyyy/mm/dd hh:nn:ss',
        DataSource.DataSet.FieldByName(Campo).AsDateTime) + '"'

        else Geral.MensagemBox('Tipo de dado desconhecido: ' +
        IntToStr(Integer(DataSource.DataSet.FieldByName(Campo).DataType)),
        'ERRO', MB_OK+MB_ICONERROR, 0);
      end;
    end;
  end;
end;

function TdmkDBGridDAC.LocateValorDeCampo(Campo: String): String;
var
  i: integer;
begin
  Result := '""';
  for i := 0 to DataSource.DataSet.FieldCount - 1 do
  begin
    if Uppercase(DataSource.DataSet.Fields[i].FieldName) = uppercase(Campo) then
    begin
      case DataSource.DataSet.FieldByName(Campo).DataType of
        ftWord, ftSmallint, ftInteger, ftLargeint, ftAutoInc: Result :=
        IntToStr(DataSource.DataSet.FieldByName(Campo).AsInteger);

        ftFloat, ftCurrency: Result := CorrigeValorDouble(
        FloatToStr(DataSource.DataSet.FieldByName(Campo).AsFloat));

        ftString, ftWideString, ftMemo, ftWideMemo: Result := '"' +
        DataSource.DataSet.FieldByName(Campo).AsString + '"';

        // Transformar em double para locate
        ftDate: Result := FormatDateTime('dd/mm/yyyy',
        DataSource.DataSet.FieldByName(Campo).AsDateTime);

        ftTime: Result := '"' + FormatDateTime('hh:nn:ss',
        DataSource.DataSet.FieldByName(Campo).AsDateTime) + '"';

        ftDateTime: Result := '"' + FormatDateTime('yyyy/mm/dd hh:nn:ss',
        DataSource.DataSet.FieldByName(Campo).AsDateTime) + '"'

        else Geral.MensagemBox('Tipo de dado desconhecido: ' +
        IntToStr(Integer(DataSource.DataSet.FieldByName(Campo).DataType)),
        'ERRO', MB_OK+MB_ICONERROR, 0);
      end;
    end;
  end;
end;

function TdmkDBGridDAC.CorrigeValorDouble(Valor: String): String;
var
  i, k: Integer;
  Txt: String;
begin
  k := Length(Valor);
  if k > 0 then
  begin
    Txt := '';
    for i := 0 to k do
    begin
      //if Valor[i] in (['-', '.', ',', '0'..'9']) then
{$IFDEF DELPHI12_UP}
      if CharInSet(Valor[i], (['-', '.', ',', '0'..'9'])) then
{$ELSE}
      if Valor[i] in (['-', '.', ',', '0'..'9']) then
{$ENDIF}
        Txt := Txt + Valor[i];
    end;
    if pos(',', Txt) > 0 then
    begin
      while pos('.', Txt) > 0 do
        Delete(Txt, pos('.', Txt), 1);
     k := pos(',', Txt);
     if k > 0 then
     begin
       Delete(Txt, k, 1);
       Insert('.', Txt, k);
     end;
    end;
  end else Txt := Valor;
  if Txt = '' then Txt := '0';
  Result := Txt;
end;

function TdmkDBGridDAC.CorrigeValorData(Valor: String): String;
begin
  if Geral.EhDataBR then
  begin
    Result  := FormatDateTime('yyyy/MM/dd',
      Geral.ValidaDataBr(Valor, True, FEdit.ForceNextYear));
  end else
  begin
    ShowMessage('ShortDateFormat = ' + {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat);
    Result := Valor;
  end;
end;

function TdmkDBGridDAC.ObtemTexto(Campo: Tfield): String;
begin
  case Campo.DataType of
{
        ftString, ftWideString, ftMemo, ftWideMemo: FEdit.FormatType := dmktfString;
}
    ftFloat, ftCurrency :
    begin
      if TFloatField(Campo).DisplayFormat <> '' then
        Result := FormatFloat(TFloatField(Campo).DisplayFormat, Campo.AsFloat)
      else
        Result :=  Campo.AsString;
    end;

    ftWord, ftSmallint, ftInteger, ftLargeint, ftAutoInc:
    begin
      if TIntegerField(Campo).DisplayFormat <> '' then
        Result := FormatFloat(TIntegerField(Campo).DisplayFormat, Campo.AsInteger)
      else
        Result :=  Campo.AsString;
    end;

    ftDate, ftTime, ftDateTime:
    begin
      if TDateTimeField(Campo).DisplayFormat <> '' then
        Result := FormatDateTime(TDateTimeField(Campo).DisplayFormat, Campo.AsDateTime)
      else
        Result :=  Campo.AsString;
    end;

    else Result :=  Campo.AsString;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkDBGridDAC]);
end;


{ procedure DrawArrowBitmao();
var
  I, K, MyTop, MyLeft: Integer;
  Bitmap: TBitmap;
begin

  Bitmap := TBitmap.Create;
  Bitmap.Mask(clWhite);
  Bitmap.Canvas.Brush.Color := clBlue;
  Bitmap.Canvas.Pen.Color := clFuchsia;
  Brush.Color := clYellow;
  Bitmap.Width := 13;
  Bitmap.Height := 13;

  MyTop  := 1;
  MyLeft := 1;

      //with Image1.Canvas do
      with Bitmap.Canvas do
      begin
        //if SortOrder = TSortArrowDirection.sadAscending then  // 0(*ASCendente*) then
        begin
          for i := MyLeft + 1 to MyLeft + 7 do Pixels[i, MyTop] := clRed; //$0099A8AC;
          k := 0;
          for i := MyTop + 1 to MyTop + 7 do
          begin
            inc(k, 1);
            Pixels[MyLeft + 1 + (k div 2), i] := clRed; //$0099A8AC;
            Pixels[MyLeft + 1 + (6 -(k div 2)), i] := clRed; //$0099A8AC;
          end;
        end;
      end;

  Image1.Picture.Bitmap.Assign(Bitmap);
  Bitmap.Free;
end;
}
end.
