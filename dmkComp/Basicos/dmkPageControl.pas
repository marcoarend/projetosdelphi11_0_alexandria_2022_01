unit dmkPageControl;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.ComCtrls;

type
  TdmkPageControl = class(TPageControl)
  private
    { Private declarations }
    FEhAncora: Boolean;

    procedure SetEhAncora(Value: Boolean);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property EhAncora: Boolean read FEhAncora write SetEhAncora;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkPageControl]);
end;

{ TdmkPageControl }

constructor TdmkPageControl.Create(AOwner: TComponent);
begin
  inherited;
  //
  SetEhAncora(False);
end;

procedure TdmkPageControl.SetEhAncora(Value: Boolean);
begin
  if Value <> FEhAncora then
    FEhAncora := Value;
end;

end.
