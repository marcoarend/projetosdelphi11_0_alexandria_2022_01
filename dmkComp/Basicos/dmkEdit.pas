// ini Delphi 28 Alexandria
//{$I dmk.inc}
// fim Delphi 28 Alexandria

unit dmkEdit;

interface

uses
  Windows, StdCtrls, Classes, SysUtils, Controls, Forms, Graphics, Menus,
  Messages, Dialogs, dmkGeral, Variants, ClipBrd, UnDmkEnums;

type
  TDmkMathOper = (dmkmthopeNone=0, dmkmthopeSoma=1, dmkmthopeSubtrai=2,
                  dmkmthopeMultiplica=3, dmkmthopeDivide=4);
  TdmkEdit = class(TCustomMemo)
  //TCMExit = TWMNoParams;
  private
  { Private declarations }
    ds,sinal : Boolean;
    FLeftZeros: Integer;
    FNoEnterToTab: Boolean;
    FNoForceUppercase: Boolean;
    FHoraFormat: TdmkHoraFmt;
    //FTexto: String;
    FQryName: String;
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FLinkMsk: String;
    FOldValor: Variant;
    FTxtPesq: String;


    FOnRedefinido: TNotifyEvent;
    FOldRedef, FNewRedef: Variant;

    function  TrimAllNoDouble(s : String): String;
    function  TrimAllNoInteger(s : String): String;
    function  GetValue: Variant;
    function  GetTexto: String;

    procedure SetLinkMsk(Value: String);
    procedure SetQryName(Value: String);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetValue(Valor: Variant);
    procedure SetFormat(Value: TAllFormat);
    procedure SetMsk(Value: TMskType);
    procedure SetDecSize(Value: Integer);
    procedure SetLeftZeros(Value: Integer);
    procedure SetNoEnterToTab(Value: Boolean);
    procedure SetNoForceUppercase(Value: Boolean);
    procedure SetValMin(Value: String);
    procedure SetValMax(Value: String);
    procedure SetForceNextYear(Value: Boolean);
    procedure SetDataFormat(Value: TdmkDataFmt);
    procedure SetHoraFormat(Value: TdmkHoraFmt); virtual;
    procedure SetTexto(Value: String);
    procedure SetObrigatorio(Value: Boolean);
    procedure SetPermiteNulo(Value: Boolean);
    procedure SetOldValor(Value: Variant);
    procedure SetValWarn(Value: Boolean);
    procedure SetTxtPesq(Value: String);

    procedure OnFim;//(Sender: TObject);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure WMRButtonDown(var Message: TWMRButtonDown); message WM_RBUTTONDOWN;
    // Copiado da VCL
    function StringToTime(const S: string): Double;
    function TryStringToTime(const S: string; var Value: TDateTime; var Fator:
             Integer): Boolean;
    function ScaneiaTime(const S: string; var Pos: Integer;
             var Time: TDateTime): Boolean; overload;
  protected
    ftipo: TAllFormat;
    fMsk: TMskType;
    FDecSize: Integer;
    FValMin: String;
    FValMax: String;
    FValWarn: Boolean;
    FForceNextYear: Boolean;
    FDataFormat: TdmkDataFmt;
    FObrigatorio: Boolean;
    FPermiteNulo: Boolean;
    FTempVal, FTempRes: (*array of*) Variant;
    FTempOpe: TDmkMathOper;

    procedure KeyPress(var Key: Char); override;
    function  CalcMathOpe(Val1, Val2: Variant; Operacao: TDmkMathOper): Variant;
  public
  { Public declarations }
    FComponentePai: String;
    constructor Create(AOwner: TComponent); override;

  published
    property Align;
    property Alignment;
    property BorderStyle;
    property CharCase;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property MaxLength;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property WantTabs;
    property FormatType: TAllFormat read FTipo write SetFormat;
    property MskType: TMskType read FMsk write SetMsk;
    property DecimalSize: Integer read FDecSize write SetDecSize;
    property LeftZeros: Integer read FLeftZeros write SetLeftZeros;
    property NoEnterToTab: Boolean read FNoEnterToTab write SetNoEnterToTab;
    property NoForceUppercase: Boolean read FNoForceUppercase write SetNoForceUppercase;
    property ValMin: String read FValMin write SetValMin;
    property ValMax: String read FValMax write SetValMax;
    property ForceNextYear: Boolean read FForceNextYear write SetForceNextYear;
    property DataFormat: TdmkDataFmt read FDataFormat write SetDataFormat;
    property HoraFormat: TdmkHoraFmt read FHoraFormat write SetHoraFormat;
    property Texto: String read GetTexto write SetTexto;
    property LinkMsk: String read FLinkMsk write SetLinkMsk;
    property QryName: String read FQryName write SetQryName;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property Obrigatorio: Boolean read FObrigatorio write SetObrigatorio;
    property PermiteNulo: Boolean read FPermiteNulo write SetPermiteNulo;
    property ValueVariant: Variant read GetValue write SetValue;
    property OldValor: Variant read FOldValor write SetOldValor;
    property ValWarn: Boolean read FValWarn write SetValWarn;
    property TxtPesq: String read FTxtPesq write SetTXTPesq;
    //
    property PasswordChar;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;

    property OnRedefinido: TNotifyEvent read FOnRedefinido write FOnRedefinido;
  end;

procedure Register;

implementation

//uses dmkGeral;

procedure ScanBlanks(const S: string; var Pos: Integer);
var
  I: Integer;
begin
  I := Pos;
  while (I <= Length(S)) and (S[I] = ' ') do Inc(I);
  Pos := I;
end;

function ScanNumber(const S: string; var Pos: Integer;
  var Number: Word; var CharCount: Byte): Boolean;
var
  I: Integer;
  N: Word;
begin
  Result := False;
  CharCount := 0;
  ScanBlanks(S, Pos);
  I := Pos;
  N := 0;
  //while (I <= Length(S)) and (S[I] in ['0'..'9']) and (N < 1000) do
{$IFDEF DELPHI12_UP}
  while (I <= Length(S)) and (CharInSet(S[I], ['0'..'9'])) and (N < 1000) do
{$ELSE}
  while (I <= Length(S)) and (S[I] in ['0'..'9']) and (N < 1000) do
{$ENDIF}
  begin
    N := N * 10 + (Ord(S[I]) - Ord('0'));
    Inc(I);
  end;
  if I > Pos then
  begin
    CharCount := I - Pos;
    Pos := I;
    Number := N;
    Result := True;
  end;
end;

function ScanString(const S: string; var Pos: Integer;
  const Symbol: string): Boolean;
begin
  Result := False;
  if Symbol <> '' then
  begin
    ScanBlanks(S, Pos);
    if AnsiCompareText(Symbol, Copy(S, Pos, Length(Symbol))) = 0 then
    begin
      Inc(Pos, Length(Symbol));
      Result := True;
    end;
  end;
end;

function ScanChar(const S: string; var Pos: Integer; Ch: Char): Boolean;
begin
  Result := False;
  ScanBlanks(S, Pos);
  if (Pos <= Length(S)) and (S[Pos] = Ch) then
  begin
    Inc(Pos);
    Result := True;
  end;
end;

constructor TdmkEdit.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  FormatType        := dmktfString;
  FMsk              := fmtNone;
  Height            := 21;
  Width             := 80;
  WordWrap	        := False;
  Texto             := '';
  FNoEnterToTab     := False;
  FNoForceUppercase := False;
  {
  Alignment	 := taLeftJustify;
  Text           := FormatFloat('0.00', 0);
  FValMin        := '';
  FValMax        := '';
  FormatType     := dmktfDouble;
  FHoraFormat    := dmkhfShort;
  FDataFormat    := dmkdfLong;
  FDecSize	 := 2;
  FLeftZeros     := 0;
  FForceNextYear := False;
  }
  //OnExit	 := OnFim;
  ds := False;
  sinal := False;
  FPermiteNulo := False;
  //
  FOldRedef := Null;
  FNewRedef := Null;
  FValWarn  := False;
  //
  FTempVal := Null;
  FTempRes := Null;
  FTempOpe := dmkmthopeNone;
end;

procedure TdmkEdit.CMExit(var Message: TCMExit);
begin
  OnFim;
  FNewRedef := Self.ValueVariant;
  if FOldRedef <> FNewRedef then
  if Assigned(FOnRedefinido) then
    FOnRedefinido(Self);
  inherited;
  if Obrigatorio then
    if varType(ValueVariant) = VarNull then
      if Enabled and Visible then
        SetFocus;
end;

procedure TdmkEdit.OnFim;//(Sender: TObject);
begin
  if (FTempVal <> Null) (*or (FTempRes <> Null)*) or (FTempOpe <> dmkmthopeNone) then
  begin
    if FTempOpe <> dmkmthopeNone then
    begin
      FTempRes := CalcMathOpe(FTempVal, Self.ValueVariant, FTempOpe);
      FTempOpe := dmkmthopeNone;
    end;
    ValueVariant := FTempRes;
    FTempVal := Null;
    FTempRes := Null;
  end else
  SetValue(GetValue);
end;

procedure TdmkEdit.SetFormat(Value: TAllFormat);
begin
  if Value = Null then Exit;
  if Value <> FTipo then
  begin
    FTipo := Value;
    case FTipo of
      dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64: Alignment := taRightJustify;
      dmktfMesAno, dmktf_AAAAMM, dmktf_AAAA_MM: Alignment := taCenter;
      else Alignment := taLeftJustify;
    end;
    SetValue(GetValue);
    Invalidate;
  end;
end;

procedure TdmkEdit.SetMsk(Value: TMskType);
begin
  if Value = Null then Exit;
  if Value <> FMsk then
  begin
    FMsk := Value;
    SetValue(GetValue);
    Invalidate;
  end;
end;

procedure TdmkEdit.SetDecSize(Value: Integer);
begin
  if Value <> FDecSize then
  begin
    FDecSize := Value;
    if FormatType = dmktfDouble then SetValue(GetValue);
  end;
end;

procedure TdmkEdit.SetLeftZeros(Value: Integer);
begin
  if Value <> FLeftZeros then
  begin
    FLeftZeros := Value;
    if FormatType in ([dmktfInteger, dmktfLongint, dmktfInt64, dmktfDouble]) then
      SetValue(GetValue);
  end;
end;

procedure TdmkEdit.SetNoEnterToTab(Value: Boolean);
begin
  if Value <> FNoEnterToTab then
  begin
    FNoEnterToTab := Value;
    if FormatType in ([dmktfInteger, dmktfLongint, dmktfInt64, dmktfDouble]) then
      SetValue(GetValue);
  end;
end;

procedure TdmkEdit.SetNoForceUppercase(Value: Boolean);
begin
  if Value <> FNoForceUppercase then
    FNoForceUppercase := Value;
end;

procedure TdmkEdit.SetValMin(Value: String);
var
  Txt: String;
  ValMin: Double;
  P: Integer;
begin
  if Value <> FValMin then
  begin
    FValMin := Value;
    // 2009-08-01
    if FormatType in ([dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64]) then
    begin
      Txt := Geral.SoNumeroEVirgula_TT(FValMin);
      if Txt <> '' then
      begin
        try
(*
          P := pos('.', FValMin);
          if P > 0 then
            FValMin[P] := ',';
          ValMin := StrToFloat(FValMin);
*)
          ValMin := StrToFloat(Txt);
          if GetValue < ValMin then
            SetValue(ValMin);
        except
          // nada
        end;
      end;
    end;
  end;
end;

procedure TdmkEdit.SetValMax(Value: String);
var
  Txt: String;
  ValMax: Double;
begin
  if Value <> FValMax then
  begin
    FValMax := Value;
    // 2009-08-01
    if FormatType in ([dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64]) then
    begin
      Txt := Geral.SoNumeroEVirgula_TT(FValMax);
      if Txt <> '' then
      begin
        try
          //ValMax := StrToFloat(FValMax);
          ValMax := StrToFloat(Txt);
          if GetValue > ValMax then
            SetValue(ValMax);
        except
          // nada
        end;
      end;
    end;
  end;
end;

procedure TdmkEdit.SetForceNextYear(Value: Boolean);
begin
  if Value <> FForceNextYear then
  begin
    FForceNextYear := Value;
  end;
end;

procedure TdmkEdit.SetObrigatorio(Value: Boolean);
begin
  if Value <> FObrigatorio then
  begin
    FObrigatorio := Value;
  end;
end;

procedure TdmkEdit.SetPermiteNulo(Value: Boolean);
begin
  if Value <> FPermiteNulo then
  begin
    FPermiteNulo := Value;
  end;
end;

function TdmkEdit.ScaneiaTime(const S: string; var Pos: Integer;
  var Time: TDateTime): Boolean;
var
  BaseHour: Integer;
  Hour, Min, Sec, MSec: Word;
  Junk: Byte;
begin
  Result := False;
  BaseHour := -1;
  if ScanString(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeAMString) or ScanString(S, Pos, 'AM') then
    BaseHour := 0
  else if ScanString(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimePMString) or ScanString(S, Pos, 'PM') then
    BaseHour := 12;
  if BaseHour >= 0 then ScanBlanks(S, Pos);
  if not ScanNumber(S, Pos, Hour, Junk) then Exit;
  Min := 0;
  Sec := 0;
  MSec := 0;
  if ScanChar(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeSeparator) then
  begin
    if not ScanNumber(S, Pos, Min, Junk) then Exit;
    if ScanChar(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeSeparator) then
    begin
      if not ScanNumber(S, Pos, Sec, Junk) then Exit;
      if ScanChar(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator) then
        if not ScanNumber(S, Pos, MSec, Junk) then Exit;
    end;
  end;
  if BaseHour < 0 then
    if ScanString(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeAMString) or ScanString(S, Pos, 'AM') then
      BaseHour := 0
    else
      if ScanString(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimePMString) or ScanString(S, Pos, 'PM') then
        BaseHour := 12;
  if BaseHour >= 0 then
  begin
    if (Hour = 0) or (Hour > 12) then Exit;
    if Hour = 12 then Hour := 0;
    Inc(Hour, BaseHour);
  end;
  ScanBlanks(S, Pos);
  Result := TryEncodeTime(Hour, Min, Sec, MSec, Time);
end;

procedure TdmkEdit.SetDataFormat(Value: TdmkDataFmt);
begin
  if Value <> FDataFormat then
  begin
    FDataFormat := Value;
    if FormatType in ([dmktfDate, dmktfMesAno, dmktf_AAAAMM, dmktf_AAAA_MM]) then
      SetValue(GetValue);
  end;
end;

procedure TdmkEdit.SetHoraFormat(Value: TdmkHoraFmt);
begin
  if Value <> FHoraFormat then
  begin
    FHoraFormat := Value;
    Invalidate;
    if FormatType = dmktfTime then
      SetValue(GetValue);
  end;
end;

procedure TdmkEdit.SetValue(Valor: Variant);
var
  MinD, MaxD, DifD, TZD: Extended;
  P, N: Integer;
  Ano, Mes, Aux: String;
begin
  MaxD := 0;
  MinD := 0;
  if (FMsk <> fmtNone) and (FTipo <> dmktfString) then
    Geral.MensagemDeErro(
      'Para usar m�scara de formata��o � necess�rio que o valor seja do tipo texto!',
      'dmkEdit.SetValue() ' + sLineBreak +
      Name);
  if PermiteNulo and (varType(Valor) = VarNull) then
  begin
    if (FTipo <> dmktfMesAno) and (FTipo <> dmktf_AAAAMM) and (FTipo <> dmktf_AAAA_MM) then
    Aux := '';
  end else begin
    if VarType(Valor) = VarNull then
    begin
      if FTipo in ([dmktfString, dmktfMesAno, dmktf_AAAAMM, dmktf_AAAA_MM]) then
        Valor := ''
      else
        Valor := 0;
    end;
    // 2009-09-05
    if (VarType(Valor) = VarString)
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
    or (VarType(Valor) = VarUString)
{$ENDIF}
     then
    begin
      if FTipo in ([dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64]) then
        if Valor = '' then Valor := '0';
    end;
    if FTipo in ([dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64]) then
    begin
      if FValMin = '' then
      begin
        case FTipo of
          dmktfInteger: MinD := -high(integer);
          dmktfLongint: MinD := -high(Longint);
          dmktfInt64  : MinD := -high(Int64);
          dmktfDouble : MinD := -high(Int64);
        end;
      end else
        MinD := Geral.DMV(FValMin);
      //
      if FValMax = '' then
      begin
        case FTipo of
          dmktfInteger: MaxD := high(integer);
          dmktfLongInt: MaxD := high(Longint);
          dmktfInt64  : MaxD := 1.7e308;
          dmktfDouble : MaxD := 1.7e308;
        end;
      end else
        MaxD := Geral.DMV(FValMax);
      //
      DifD := MaxD - MinD;
      if (DifD < 0.00000000001) and (MinD = 0) then
      begin
        MinD := 0;
        MaxD := 0;
      end;
      //if Valor = Null then Valor := 0;
    end else begin
      MinD := 0;
      MaxD := 0;
    end;
    if FValWarn then
    begin
      if Valor < MinD then
        Geral.MB_Aviso('Valor n�o pode ser menor que ' + FloatToStr(MinD));
      if Valor > MaxD then
        Geral.MB_Aviso('Valor n�o pode ser maior que ' + FloatToStr(MaxD));
    end;
    case FTipo of
      dmktfDouble:
      begin
        if MinD <> 0 then
        begin
          if MaxD <> 0 then
            Aux := Geral.TFT_MinMax(FloatToStr(Valor), MinD, MaxD, FDecSize)
          else
            Aux := Geral.TFT_Min(FloatToStr(Valor), MinD, FDecSize);
        end else
          if MaxD <> 0 then
            Aux := Geral.TFT_Max(FloatToStr(Valor), MaxD, FDecSize)
          else
            Aux := Geral.TFT(FloatToStr(Valor), FDecSize, siPositivo);
        if FDecSize = 0 then
        begin
          Aux := Geral.SoNumeroESinal_TT(Aux);
          if LeftZeros <> 0 then
            Aux := Geral.TFD(Aux, LeftZeros, siPositivo);
        end;
      end;
      dmktfInteger, dmktfLongInt, dmktfInt64:
      begin
        if MinD <> 0 then
        begin
          if MaxD <> 0 then
            Aux := Geral.TFT_MinMax(FloatToStr(Valor), MinD, MaxD, 0)
          else
            Aux := Geral.TFT_Min(FloatToStr(Valor), MinD, 0);
        end else
          if MaxD <> 0 then
            Aux := Geral.TFT_Max(FloatToStr(Valor), MaxD, 0)
          else
            Aux := Geral.TFT(FloatToStr(Valor), 0, siNegativo);
        if LeftZeros <> 0 then
          Aux := Geral.TFD(Aux, LeftZeros, siNegativo)
        else // for�ar a tirar os thousand separator
          Aux := Geral.TFD(Aux, 1, siNegativo)
      end;
      dmktfDate:
      begin
        if Valor < 1 then
          aux := ''
        else
          aux := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat, Valor);
      end;
      dmktfTime:
      begin
        if varType(Valor) = VarNull then Aux := '' else
        begin
          if pos(':', Valor) > 0 then
          begin
            if pos('/', Valor) > 0 then
              Valor := Copy(Valor, pos(' ', Valor) + 1);
{$IFDEF DELPHI12_UP}
            Valor := StrToTime(Valor);
{$ELSE}
            Valor := StrToTime(Valor);
{$ENDIF}
          end;
          case FHoraFormat of
            dmkhfShort: aux := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortTimeFormat, Valor);
            dmkhfLong:  aux := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}LongTimeFormat, Valor);
            dmkhfMiliSeconds, dmkhfExtended:  aux := FormatDateTime('hh:nn:ss,zzz', Valor);
            dmkhfTZD_UTC:
            begin
              if Valor < 0 then
              begin
                aux := '-';
                TZD := - Valor;
              end else begin
                aux := '+';
                TZD := Valor;
              end;
              aux := aux +
                FormatDateTime(FormatSettings.ShortTimeFormat, Valor);
            end;
          end;
        end;
      end;
      dmktfDateTime:
      begin
        if (varType(Valor) = VarString)
{$IFDEF DELPHI12_UP}
        or (varType(Valor) = VarUString)
{$ENDIF}
        then begin
          if (Valor = '0000-00-00 00:00')
          or (Valor = '0000-00-00 00:00:00')
          or (Valor = '0000-00-00 00:00:00,000') then
            Aux := ''
          else
            Aux := Valor;
        end else
        if varType(Valor) = VarNull then Aux := ''
        else
        case FHoraFormat of
          dmkhfShort: aux := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat + ' ' + {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortTimeFormat, Valor);
          dmkhfLong:  aux := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat + ' ' + {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}LongTimeFormat, Valor);
          dmkhfMiliSeconds, dmkhfExtended:  aux := FormatDateTime({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat + ' hh:nn:ss,zzz', Valor);
          dmkhfTZD_UTC:
          begin
            if Valor < 0 then
            begin
              aux := '-';
              TZD := - Valor;
            end else begin
              aux := '+';
              TZD := Valor;
            end;
            aux := aux +
              FormatDateTime(FormatSettings.ShortTimeFormat, Valor);
          end;
        end;
      end;
      dmktfMesAno:
      begin
        Aux := Geral.VariantToString(Valor);
        if (Aux <> '') then
        // 2012-09-15
        if (varType(Valor) = VarString)
{$IFDEF DELPHI12_UP}
        or (varType(Valor) = VarUString)
{$ENDIF} then
          aux := Valor
        else
        begin
          // Fim 2012-09-15
          case DataFormat of
            dmkdfShort: aux := FormatDateTime('mm/yy', Valor);
            dmkdfLong:  aux := FormatDateTime('mm/yyyy', Valor);
          end;
        end;
      end;
      dmktf_AAAAMM:
      begin
        Aux := Geral.VariantToString(Valor);
        N := Length(Aux);
        // 2015-07-17
        if N = 19 then
        begin
          Aux := Copy(Aux, 1, 10);
          N := Length(Aux);
        end;
        // Fim 2015-07-17
        P := pos('/', Aux);
        case N of
          // 2015-07-17
          //0:; //
          0: Aux := ''; // data '0'
          // Fim 2015-07-17
          1: Aux := ''; // data '0'
          6:; // aaaamm
          7:  // aaaa/mm ou mm/aaaa
          begin
            if P = 3 then
            begin
              Mes := Copy(aux, 1, 2);
              Ano := Copy(aux, 4);
            end else begin
              Mes := Copy(aux, 6);
              Ano := Copy(aux, 1, 4);
            end;
            Aux := Ano + Mes;
          end;
          {
          8: // ddmmaaaa ou aaaammdd
          begin

            if P = 3 then
            begin
              Mes := Copy(aux, 4, 2);
              Ano := Copy(aux, 7);
            end else begin
              Mes := Copy(aux, 6);
              Ano := Copy(aux, 1, 4);
            end;
            Aux := Ano + Mes;
          end;
          }
          10: // dd/mm/aaaa ou aaaa/mm/dd
          begin
            if P = 3 then
            begin
              Mes := Copy(aux, 4, 2);
              Ano := Copy(aux, 7);
            end else begin
              Mes := Copy(aux, 6);
              Ano := Copy(aux, 1, 4);
            end;
            Aux := Ano + Mes;
          end;
          else Geral.MB_Erro('Erro na convers�o da data! AVISE A DERMATEK' +
          sLineBreak + 'dmkEdit.SetValue()');
        end;
        if (Aux <> '') then
        begin
          while length(Aux) < 6 do
            aux := '0' + aux;
          Aux := Copy(Aux, 5, 2) + '/' +Copy(Aux, 1, 4);
        end;
      end;
      dmktf_AAAA_MM:
      begin
        Aux := Geral.VariantToString(Valor);
        N := Length(Aux);
        if N = 19 then
        begin
          Aux := Copy(Aux, 1, 10);
          N := Length(Aux);
        end;
        P := pos('/', Aux);
        case N of
          0: Aux := ''; // data '0'
          1: Aux := ''; // data '0'
          6:; // aaaamm
          7:  // aaaa-mm ou mm-aaaa
          begin
            if P = 3 then
            begin
              Mes := Copy(aux, 1, 2);
              Ano := Copy(aux, 4);
            end else begin
              Mes := Copy(aux, 6);
              Ano := Copy(aux, 1, 4);
            end;
            Aux := Ano + Mes;
          end;
          10: // dd-mm-aaaa ou aaaa-mm-dd
          begin
            if P = 3 then
            begin
              Mes := Copy(aux, 4, 2);
              Ano := Copy(aux, 7);
            end else begin
              Mes := Copy(aux, 6);
              Ano := Copy(aux, 1, 4);
            end;
            Aux := Ano + Mes;
          end;
          else Geral.MB_Erro('Erro na convers�o da data! AVISE A DERMATEK' +
          sLineBreak + 'dmkEdit.SetValue()');
        end;
        if (Aux <> '') then
        begin
          while length(Aux) < 6 do
            aux := '0' + aux;
          Aux := Copy(Aux, 1, 4) + '-' +Copy(Aux, 5, 2);
        end;
      end;
      dmktfString:
      begin
        if varType(Valor) = VarNull then Aux := '' else
        begin
          // N�o pode - tem letras
          //Aux := Geral.SoNumero_TT(Valor);
          case FMsk of
            fmtCPFJ    :
            begin
              Aux := Valor;
              Geral.CNPJ_Valida(TWinControl(Self), Aux);
            end;
            fmtCPFJ_NFe    :
            begin
              Aux := Valor;
              Geral.CNPJ_Valida(TWinControl(Self), Aux);
              Aux := Geral.SoNumero_TT(Aux);
            end;
            //
            fmtCEP     : Aux := Geral.FormataCEP_TT(Valor);
            //
            fmtCEP_NFe : Aux := Geral.SoNumero_TT(Geral.FormataCEP_TT(Valor));
            fmtTelCurto: Aux := String(Geral.FormataTelefone_TT_Curto(AnsiString(Valor)));
            fmtTelLongo: Aux := String(Geral.FormataTelefone_TT(AnsiString(Valor)));
            fmtTel_NFe : Aux := String(Geral.FormataTelefone_TT_NFe(Valor));
            fmtNCM     : Aux := Geral.FormataNCM(Valor);
            fmtCBO2002 : Aux := Geral.FormataCBO2002(Valor);
            fmtNumRua  : Aux := String(Geral.FormataNumeroDeRua('?', Valor, False));
            fmtIE      : Aux := Geral.Formata_IE(Valor, FLinkMsk, FLinkMsk);
            fmtIE_NFe  : Aux := Geral.SoNumero_TT(Geral.Formata_IE(Valor, FLinkMsk, FLinkMsk));
            fmtUF      : Aux := Geral.FormataUF(Valor);
            fmtVersao  : Aux := Geral.FormataVersao(Valor);
            fmtChaveNFe: Aux := Geral.FormataChaveNFe(Valor, cfcnDANFE);
            fmtNone    : Aux := Valor;
            else         Aux := Valor;
          end;
        end;
      end;
      else
        if varType(Valor) = VarNull then Aux := '' else Aux := Valor;
    end;
  end;
  if Text <> Aux then
  begin
    Text := Aux;
    //FTexto := Text;
  end;
end;

procedure TdmkEdit.SetValWarn(Value: Boolean);
begin
  if FValWarn <> Value then
    FValWarn := Value;
end;

function TdmkEdit.StringToTime(const S: string): Double;
var
  Fator: Integer;
  Data: TDateTime;
begin
  if not TryStringToTime(S, Data, Fator) then
    Geral.MensagemBox('Data inv�lida: ' + S, 'Erro', MB_OK+MB_ICONERROR, 0)
  else
    Result := Data * Fator;
end;

function TdmkEdit.GetValue: Variant;
var
  aux, d, h: String;
  hh, nn, ss, zzz: String;
  Res: Variant;
  k, I, J: integer;
  Data: TDateTime;
  Fator: Integer;
begin
  aux := Text;
{
  try
}
    if FormatType = dmktfDouble then
    begin
      aux := Trim(aux);
      if aux = '-' then aux := '0.00';
      if (aux = '') and PermiteNulo then Res := Null else
      begin
        if aux = '' then aux := '0';
        Res := StrToFloat(TrimAllNoDouble(aux));
      end;
    end else
    if FormatType = dmktfInteger then
    begin
      aux := Trim(aux);
      if aux = '-' then aux := '0';
      try
        if (aux = '') and PermiteNulo then Res := Null else
        begin
          if aux = '' then aux := '0';
          try
            Res := Geral.IMV(TrimAllNoInteger(aux));
          except
            Geral.MensagemBox('"' + aux + '" n�o � um n�mero v�lido para o componenete:' + #13#10 +
            Self.Name, 'ERRO', MB_OK+MB_ICONERROR, 0);
          end;
        end;
      except
        Res := '';
      end;
    end else
    if FormatType = dmktfLongInt then
    begin
      aux := Trim(aux);
      if aux = '-' then aux := '0';
      try
        if (aux = '') and PermiteNulo then Res := Null else
        begin
          if aux = '' then aux := '0';
          Res := Geral.IMV(TrimAllNoInteger(aux));
        end;
      except
        Res := '';
      end;
    end else
    if FormatType = dmktfInt64 then
    begin
      aux := Trim(aux);
      if aux = '-' then aux := '0';
      try
        if (aux = '') and PermiteNulo then Res := Null else
        begin
          if aux = '' then aux := '0';
          Res := Geral.I64(TrimAllNoInteger(aux));
        end;
      except
        Res := '';
      end;
    end else
    if FormatType = dmktfDate then
    begin
      if aux = '' then aux := '0' else
      if aux = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DateSeparator then aux := '0';
      if Geral.EhDataBR then
        Res := Geral.ValidaDataBr(aux, True, FForceNextYear, False)
      else
        if aux = '0' then
          Res := 0
        else
          Res := StrToDate(aux);
    end else
    if FormatType = dmktfTime then
    begin
      if aux = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeSeparator then aux := '0';
      if HoraFormat in ([dmkhfMiliSeconds, dmkhfExtended]) then
      begin
        hh := '';
        nn := '';
        ss := '';
        zzz := '';
        J := 0;
        for I := Length(aux) downto 1 do
        begin
          //if aux[I] in ([':', ',']) then
{$IFDEF DELPHI12_UP}
          if CharInSet(aux[I], ([':', ','])) then
{$ELSE}
          if aux[I] in ([':', ',']) then
{$ENDIF}
            J := J + 1
          else begin
            case J of
              0: zzz := aux[I] + zzz;
              1: ss := aux[I] + ss;
              2: nn := aux[I] + nn;
              3: hh := aux[I] + hh;
            end;
          end;
        end;
        k := pos(',', aux);
        while Length(zzz) < 3 do
        begin
          if k > 0 then
            zzz := zzz + '0'
          else
            zzz := '0' + zzz;
        end;
        while Length(ss) < 2 do
          ss := '0' + ss;
        while Length(nn) < 2 do
          nn := '0' + nn;
        while Length(hh) < 2 do
          hh := '0' + hh;
        aux := hh + ':' + nn + ':' + ss  + ',' + zzz;
      end else
      if HoraFormat = dmkhfTZD_UTC then
      begin
        if pos(':', aux) = 0 then
        begin
          case Length(Aux) of
            4: Insert(':', Aux, 3);
            5: Insert(':', Aux, 4);
            6: begin Insert(':', Aux, 5); Insert(':', Aux, 3); end;
            7: begin Insert(':', Aux, 6); Insert(':', Aux, 4); end;
          end;
        end;
      end else
      begin
        if pos(':', aux) = 0 then
        begin
          case Length(Aux) of
            3: Insert(':', Aux, 2);
            4: Insert(':', Aux, 3);
            5: begin Insert(':', Aux, 4); Insert(':', Aux, 2); end;
            6: begin Insert(':', Aux, 5); Insert(':', Aux, 3); end;
            //9: begin Insert(':', Aux, 5); Insert(':', Aux, 3); end;
          end;
        end;
      end;
      if aux <> '' then
      begin
        Data := StringToTime(aux);
        // 2011-06-25
        // N�o funciona de outra forma!
        Res := Double(Data);
        // assim j� n�o funciona!
        // que coisa!
        //ShowMessage(FormatDateTime('00:00:00.000', res));
      end else
        Res := Null;
    end else
    if FormatType = dmktfDateTime then
    begin
      // TZD UTC
      Fator := 1;
      if aux = '00:00'  then
      begin
        Result := 0;
        Exit;
      end;
      if Length(aux) > 0 then
      begin
        if (aux[1]= '-') or (aux[1] = '+') then
        begin
          if aux[1]= '-' then
            Fator := -1;
          aux := Copy(aux, 2);
        end;
      end;
      // Fim TZD UTC
      if aux = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DateSeparator then aux := '0';
      if aux = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeSeparator then aux := '0';
      if aux = ' '           then aux := '0';
      k := pos(' ', aux);
      if k > 0 then
      begin
        d := Copy(aux, 1, k-1);
        h := Copy(aux, k, Length(aux) - k+1);
        if Geral.EhDataBR then
          Res := Geral.ValidaDataBr(d, True, FForceNextYear, False) + StrToTime(h)
        else
          Res := StrToDateTime(aux);
      end else
      if Geral.EhDataBR then
        Res := Geral.ValidaDataBr(aux, True, FForceNextYear, False)
      else
        Res := StrToDate(aux) * Fator;
    end else
    if FormatType in ([dmktfMesAno, dmktf_AAAAMM, dmktf_AAAA_MM]) then
    begin
      if aux = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeSeparator then Res := Null else
      if aux = '0' then Res := Null else
      if aux = '' then Res := Null else
      begin
        Res := Null;
        Data := Geral.MTD2(aux, FObrigatorio and Enabled);
        if Data > 0 then
          Res := Data;
      end;
    end else
    if FormatType = dmktfString then
    begin
      Res := aux;
    end else
      Res := aux;
{
  except
    Geral.MB_Erro('N�o foi poss�vel obter o valor de "' +
    Self.Name + '". dmkEdit.GetValue())');
    //raise;
  end;
}
  Result := Variant(Res);
end;

function TdmkEdit.TrimAllNoDouble(s : String): String;
var
  i : Integer;
begin
  while Pos(' ', s) > 0 do
    Delete (s, Pos(' ', s), 1);
  while Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, s) > 0 do
    Delete (s, Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, s), 1);

  i := Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}CurrencyString, s);
  if i > 0 then
    Delete(S, I, Length({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}CurrencyString));
  Result := s;
end;

function TdmkEdit.TrimAllNoInteger(s : String): String;
var
  i : Integer;
begin
  while Pos(' ', s) > 0 do
    Delete (s, Pos(' ', s), 1);
  while Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, s) > 0 do
    Delete (s, Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, s), 1);

  i := Pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator, s);
  if i = 0 then i := Length(s);
  Result := Copy(s, 1, i);
end;

function TdmkEdit.TryStringToTime(const S: string; var Value: TDateTime; var
Fator: Integer): Boolean;
var
  Pos: Integer;
begin
  Fator := 1;
  Pos := 1;
  if Length(S) > 0 then
  begin
    if S[1] = '-' then
    begin
      Fator := -1;
      Pos := 2;
    end else
    if S[1] = '+' then
      Pos := 2;
  end;
  Result := ScaneiaTime(S, Pos, Value) and (Pos > Length(S));
end;

function TdmkEdit.CalcMathOpe(Val1, Val2: Variant;
  Operacao: TDmkMathOper): Variant;
begin
  case Operacao of
    dmkmthopeSoma       : Result := Val1 + Val2;
    dmkmthopeSubtrai    : Result := Val1 - Val2;
    dmkmthopeMultiplica : Result := Val1 * Val2;
    dmkmthopeDivide:
    begin
      if (VarType(Val1) = vtInteger)
      and (VarType(Val2) = vtInteger) then
        Result := Val1 div Val2
      else
        Result := Val1 / Val2;
    end;
    else Result := Null;
  end;
end;

procedure TdmkEdit.CMEnter(var Message: TCMEnter);
begin
  SelectAll;
  FOldRedef := Self.ValueVariant;
  inherited;
end;

procedure TdmkEdit.KeyPress(var Key: Char);
var
  aux : String;
begin
  if FTipo = dmktfDouble then
  begin
    if key = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator then
    key := {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator;
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Key, ['0'..'9',FormatSettings.DecimalSeparator,'-',chr(8)])) then
{$ELSE}
    if not (Key in ['0'..'9', DecimalSeparator,'-',chr(8)]) then
{$ENDIF}
    begin
      Key := #0;
      inherited KeyPress(Key);
      Exit;
    end;
    aux := {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator;
    if key = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DecimalSeparator then
      if Pos(aux,Text) <> 0 then Key := #0;

    if key = '-' then
      if ((Length(Text) > 0) and (Self.SelLength = Length(Text)))
      or (Self.SelStart = 0) then
          key := '-'
        else Key := #0;
  end else if FTipo in ([dmktfInteger, dmktfLongInt, dmktfInt64]) then
  begin
    //if not (Key in ['0'..'9',ThousandSeparator,'-',chr(8)]) then
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Key, ['0'..'9',FormatSettings.ThousandSeparator,'-',chr(8)])) then
{$ELSE}
    if not (Key in ['0'..'9',ThousandSeparator,'-',chr(8)]) then
{$ENDIF}
    begin
      Key := #0;
      inherited KeyPress(Key);
      Exit;
    end;
    if key = '-' then
      if ((Length(Text) > 0) and (Self.SelLength = Length(Text)))
      or (Self.SelStart = 0) then
          key := '-'
        else Key := #0;
  end else if FTipo = dmktfDate then
  begin
    //if not (Key in ['0'..'9', FormatSettings.DateSeparator, chr(8)]) then
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Key, ['0'..'9', FormatSettings.DateSeparator, chr(8)])) then
{$ELSE}
    if not (Key in ['0'..'9', DateSeparator, chr(8)]) then
{$ENDIF}
    begin
      Key := #0;
      inherited KeyPress(Key);
      Exit;
    end;
  end else if FTipo = dmktfTime then
  begin
    //if not (Key in ['0'..'9', FormatSettings.TimeSeparator, chr(8), ',']) then
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Key, ['0'..'9', FormatSettings.TimeSeparator, chr(8), ','])) then
{$ELSE}
    if not (Key in ['0'..'9', TimeSeparator, chr(8), ',']) then
{$ENDIF}
    begin
      if (FHoraFormat = dmkhfTZD_UTC) and (CharInSet(Key, ['-', '+'])) then
        // nada
      else
      begin
        Key := #0;
        inherited KeyPress(Key);
        Exit;
      end;
    end;
  end else if FTipo = dmktfDateTime then
  begin
    //if not (Key in ['0'..'9', FormatSettings.TimeSeparator, FormatSettings.DateSeparator, ' ', chr(8), ',']) then
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Key, ['0'..'9', FormatSettings.TimeSeparator, FormatSettings.DateSeparator, ' ', chr(8), ','])) then
{$ELSE}
    if not (Key in ['0'..'9', TimeSeparator, DateSeparator, ' ', chr(8), ',']) then
{$ENDIF}
    begin
      Key := #0;
      inherited KeyPress(Key);
      Exit;
    end;
  end else if FTipo in ([dmktfMesAno, dmktf_AAAAMM, dmktf_AAAA_MM]) then
  begin
    //if not (Key in ['0'..'9', FormatSettings.DateSeparator, ' ', chr(8), chr(22)]) then
{$IFDEF DELPHI12_UP}
    if not (CharInSet(Key, ['0'..'9', FormatSettings.DateSeparator, ' ', chr(8), chr(22)])) then
{$ELSE}
    if not (Key in ['0'..'9', DateSeparator, ' ', chr(8), chr(22)]) then
{$ENDIF}
    begin
      Key := #0;
{
      if Key = VK_F4 then
        Self.Text := Geral.FDT(Date, 2);
}
      inherited KeyPress(Key);
      Exit;
    end;
  end;
  inherited KeyPress(Key);
end;

procedure TdmkEdit.SetTexto(Value: String);
begin
  Text := Value;
  if Texto <> Text then Texto := Text;
end;

procedure TdmkEdit.SetTxtPesq(Value: String);
begin
  if Value <> FTxtPesq then
  begin
    FTxtPesq := Value;
  end;
end;

function TdmkEdit.GetTexto: String;
begin
  Result := Text;
end;

procedure TdmkEdit.SetLinkMsk(Value: String);
begin
  if Value <> FLinkMsk then
  begin
    FLinkMsk := Value;
    SetValue(GetValue);
  end;
end;

procedure TdmkEdit.SetOldValor(Value: Variant);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkEdit.SetQryName(Value: String);
begin
  if Value <> FQryName then
  begin
    FQryName := Value;
  end;
end;

procedure TdmkEdit.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkEdit.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkEdit.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure TdmkEdit.WMKeyDown(var Message: TWMKeyDown);
  function LimpaTexto(Text: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(Text) do
    begin
      case Ord(Text[I]) of
        10: ;
        13: ;
        else Result := Result + Text[I];
      end;
    end;
  end;
var
  Periodo, Increm: Int64;
  //Key: Char;
  ShiftState: TShiftState;
  Valor: Double;
  Str, TxtClipBrd: String;
  V: Variant;
  Chave: Integer;
begin
  Chave := Message.CharCode;
  if Chave = VK_RETURN then
  begin
    Keybd_Event(VK_TAB,0,0,0);
    Exit;
  end;
  if ssCtrl in KeyboardStateToShiftState then
  begin
    case Chave of
      (*A*)65: SelectAll;
      (*C*)67: CopyToClipBoard;
      (*V*)86:
      begin
        if (not Self.ReadOnly) and (Self.FormatType <> dmktfString) then
        begin
          ClipBoard.Open;
          TxtClipBrd := Clipboard.AsText;
          TxtClipBrd := LimpaTexto(TxtClipBrd);
          WordWrap := True;
          Lines.Clear;
          WordWrap := False;
          Text := TxtClipBrd;
          ClipBoard.Close;
          Keybd_Event(VK_END,0,0,0);
        end;
      end;
      (*X*)88:
      if not Self.ReadOnly then
        CutToClipBoard;
      (*Z*)90:
      if not Self.ReadOnly then
        Undo;
    end;
  end;
(*
  if ssShift in KeyboardStateToShiftState then
  begin
    case Chave of
      Ord('+'), 187:  FTempOpe := dmkmthopeSoma;
      Ord('*'), 56:  FTempOpe := dmkmthopeMultiplica;
    end;
  end;
*)
(*
  key := Char(Chave);
  case key of
   //#13: key := #9;
   ^A: // "a" OU "A"
   begin
     if ssCtrl in ShiftState then // CTRL + A
       SelectAll;
   end;
   ^C: // "c" OU "C"
   begin
     if ssCtrl in ShiftState then // CTRL + C
       CopyToClipBoard;
   end;
   ^V: // "v" OU "V"
   begin
     if ssCtrl in ShiftState then // CTRL + V
     begin
       ClipBoard.Open;
       TxtClipBrd := Clipboard.AsText;
       TxtClipBrd := LimpaTexto(TxtClipBrd);
       WordWrap := True;
       Lines.Clear;
       WordWrap := False;
       Text := TxtClipBrd;
       //Texto := TxtClipBrd;
       //ValueVariant := TxtClipBrd;
       ClipBoard.Close;
       Keybd_Event(VK_END,0,0,0);
     end;
   end;
   ^X: // "x" OU "X"
   begin
     if ssCtrl in ShiftState then // CTRL + X
       CutToClipBoard;
   end;
   ^Z: // "z" OU "Z"
   begin
     if ssCtrl in ShiftState then // CTRL + Z
       Undo;
   end;
  end;
*)
  if not Self.ReadOnly then
  begin
    case FTipo of
      dmktfDouble, dmktfInteger, dmktfLongInt, dmktfInt64:
      begin
        if Chave in ([VK_DOWN, VK_UP]) then
        begin
          Increm := 0;
          if Chave = VK_DOWN then Increm := -1;
          if Chave = VK_UP   then Increm := +1;
          ShiftState := KeyDataToShiftState(Message.KeyData);
          if ssShift in ShiftState then Increm := Increm *  10;
          if ssCtrl  in ShiftState then Increm := Increm * 100;
          if Increm <> 0 then
          begin
            case FTipo of
              dmktfDouble:
              begin
                Valor := Increm / 100;
                if (GetValue <> Null) then Valor := Valor + GetValue;
                SetValue(Valor);
              end;
              dmktfInteger, dmktfLongInt, dmktfInt64:
              begin
                if (GetValue <> Null) then Increm := Increm + GetValue;
                SetValue(Increm);
              end;
            end;
          end;
        end else
        if (Chave in ([Ord('+'), Ord('-'), Ord('*'), Ord('/'), 193(*/*)]))
        or (
          (Chave in ([56,187, 189])) and (ssShift in KeyboardStateToShiftState )
        )
        then
        begin
          if (FTempVal <> Null) and (FTempOpe <> dmkmthopeNone) then
          begin
            FTempRes := CalcMathOpe(FTempVal, Self.ValueVariant, FTempOpe);
            FTempOpe := dmkmthopeNone;
            FTempVal := Null;
            Self.ValueVariant := FTempRes;
            SelectAll;
          end;
          //
          if ssShift in KeyboardStateToShiftState then
          begin
            case Chave of
              56:   FTempOpe := dmkmthopeMultiplica;
              187:  FTempOpe := dmkmthopeSoma;
              189:  FTempOpe := dmkmthopeSubtrai;
            end;
            //Self.Text := '';
          end else
          begin
            case Chave of
              Ord('-'), 189:  FTempOpe := dmkmthopeSubtrai;
              Ord('/'), 193:  FTempOpe := dmkmthopeDivide;
            end;
            //Self.Text := '';
          end;
          if (FTempVal <> Null) and (FTempOpe <> dmkmthopeNone) then
          begin
            FTempVal := CalcMathOpe(FTempVal, Self.ValueVariant, FTempOpe);
            FTempOpe := dmkmthopeNone;
            Self.ValueVariant := FTempVal;
            SelectAll;
          end else
          begin
            FTempVal := Self.ValueVariant;
            SelectAll;
          end;
        end;
      end;
      dmktfDate, dmktfTime, dmktfDateTime: ;
      dmktfMesAno, dmktf_AAAAMM:
      begin
        if Chave in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
        begin
          try
            V := GetValue;
            if V <> Null then
              Str := Geral.VariantToString(GetValue)
            else
              Str := '';
            if (Str = '') or (Chave = VK_F5) then
              Periodo := Geral.DtP(Date)
            else begin
              Periodo := Geral.MTP(Text, Obrigatorio);
              case Chave of
                VK_DOWN: Periodo := Periodo -1;
                VK_UP:   Periodo := Periodo +1;
                VK_F4:   Periodo := Periodo -1;
                VK_F5:   Periodo := Periodo   ;
              end;
            end;
          except
            Periodo := Geral.DtP(Date);
          end;
          SetValue(Geral.PTD(Periodo, 1));
        end;
      end;
      dmktf_AAAA_MM:
      begin
        if Chave in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
        begin
          try
            V := GetValue;
            if V <> Null then
              Str := Geral.VariantToString(GetValue)
            else
              Str := '';
            if (Str = '') or (Chave = VK_F5) then
              Periodo := Geral.DtP(Date)
            else begin
              Periodo := Geral.MTP2(Text, Obrigatorio);
              case Chave of
                VK_DOWN: Periodo := Periodo -1;
                VK_UP:   Periodo := Periodo +1;
                VK_F4:   Periodo := Periodo -1;
                VK_F5:   Periodo := Periodo   ;
              end;
            end;
          except
            Periodo := Geral.DtP(Date);
          end;
          SetValue(Geral.PTD(Periodo, 1));
        end;
      end;
{
      dmktf_AAAA_MM:
      begin
        if Chave in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
        begin
          try
            V := GetValue;
            if V <> Null then
              Str := Geral.VariantToString(GetValue)
            else
              Str := '';
            if (Str = '') or (Chave = VK_F5) then
              Periodo := Geral.DtP2(Date)
            else begin
              Periodo := Geral.MTP2(Text, Obrigatorio);
              case Chave of
                VK_DOWN: Periodo := Periodo -1;
                VK_UP:   Periodo := Periodo +1;
                VK_F4:   Periodo := Periodo -1;
                VK_F5:   Periodo := Periodo   ;
              end;
            end;
          except
            Periodo := Geral.DtP2(Date);
          end;
          SetValue(Geral.PTD2(Periodo, 1));
        end;
      end;
}
    end;
  end;
  inherited;
  SetTexto(Text);
end;

procedure TdmkEdit.WMRButtonDown(var Message: TWMRButtonDown);
begin
  SetFocus;
  inherited;
end;

procedure TdmkEdit.CMTextChanged(var Message: TMessage);
begin
  SetTexto(Text);
  inherited;
  if Assigned(FOnRedefinido) then
    FOnRedefinido(Self);
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEdit]);
end;

end.



