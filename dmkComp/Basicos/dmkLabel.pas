unit dmkLabel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkGeral, UnDmkEnums;

type
  TOnTextChangeEvent = procedure(Sender: TObject) of object;

  TdmkLabel = class(TLabel)
  private
    { Private declarations }
    // 2012-28-10
    FOnChange: TNotifyEvent;
    // Fim 2012-28-10
    FTextChange: TOnTextChangeEvent;

    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FSQLType: TSQLType;
    FOldValor: Variant;

    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetSQLType(Value: TSQLType);
    procedure SetOldValor(Value: Variant);

    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    // 2012-28-10
    function GetCaption: string;
    procedure SetCaption(const Value: string);
    // Fim 2012-28-10

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property SQLType: TSQLType read FSQLType write SetSQLType;
    property OnTextChange: TOnTextChangeEvent read FTextChange write FTextChange;
    property OldValor: Variant read FOldValor write SetOldValor;

    // 2012-28-10
    property Caption: string read GetCaption write SetCaption;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    // Fim 2012-28-10
  end;

procedure Register;

implementation

constructor TdmkLabel.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  SQLType     := stNil;
end;

function TdmkLabel.GetCaption: string;
begin
  Result := inherited Caption; 
end;

procedure TdmkLabel.SetCaption(const Value: string);
begin
  inherited Caption := Value;
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TdmkLabel.SetOldValor(Value: Variant);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkLabel.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkLabel.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkLabel.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure TdmkLabel.SetSQLType(Value: TSQLType);
  // N�o mover para dmkGeral! Provoca erro por usar TSQLType
  function  NomeTipoSQL(Tipo: TSQLType): String;
  begin
    case Tipo of
      stCpy: Result := 'C�pia';
      stDel: Result := 'Exclus�o';
      stIns: Result := 'Inclus�o';
      stLok: Result := 'Travado';
      stUpd: Result := 'Altera��o';
      stUnd: Result := 'Desiste';
      stPsq: Result := 'Pesquisa';
      else   Result := '???????';
    end;
end;

begin
  if Value <> FSQLType then
    FSQLType := Value;
  if Value <> stNil then
    if Self.Caption <> NomeTipoSQL(Value) then
      Self.Caption := NomeTipoSQL(Value);
end;

procedure TdmkLabel.CMTextChanged(var Message: TMessage);
begin
  Invalidate;
  //MessageBox(0, 'Text change', 'Aviso', MB_OK+MB_ICONINFORMATION);
    if Assigned(OnTextChange) then
    OnTextChange(Self);
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkLabel]);
end;

end.
