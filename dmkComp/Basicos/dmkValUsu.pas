unit dmkValUsu;

interface

uses
  Windows,  Messages, SysUtils, Classes,  Graphics, Controls,  Forms,
  Dialogs,  DBCtrls,  StdCtrls, dmkEditCB, dmkGeral, Variants, DB,
  ExtCtrls, UnDmkEnums;

type
  TdmkValUsu = class(TComponent)
  private
    { Private declarations }
    FQryCampo: String;
    FUpdCampo: String;
    FRefCampo: String;
    FUpdType: TUpdType;
    FValUsuVal: Variant;
    FdmkEditCB: TdmkEditCB;
    FPanel: TPanel;
    FOldValor: Variant;

    function GetValue: Variant;
    procedure SetValue(Value: Variant);
    procedure SetdmkEditCB(Value: TdmkEditCB);
    procedure SetPanel(Value: TPanel);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetRefCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetOldValor(Value: Variant);
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure Modified;
  published
    { Published declarations }
    property dmkEditCB: TdmkEditCB read FdmkEditCB write SetdmkEditCB;
    property Panel: TPanel read FPanel write SetPanel;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property RefCampo: String read FRefCampo write SetRefCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property ValueVariant: Variant read GetValue write SetValue;
    property OldValor: Variant read FOldValor write SetOldValor;
  end;

procedure Register;

implementation

procedure TdmkValUsu.SetdmkEditCB(Value: TdmkEditCB);
begin
  if Value <> FdmkEditCB then
  begin
    FdmkEditCB := Value;
  end;
end;

procedure TdmkValUsu.SetPanel(Value: TPanel);
begin
  if Value <> FPanel then
  begin
    FPanel := Value;
  end;
end;

procedure TdmkValUsu.Modified;
var
  DataSet: TDataSet;
  TemValor: Boolean;
begin
  if FdmkEditCB <> nil then
  begin
    TemValor := False;
    if FdmkEditCB.DBLookupComboBox <> nil then
      if FdmkEditCB.DBLookupComboBox.ListSource <> nil then
        if FdmkEditCB.DBLookupComboBox.ListSource.DataSet <> nil then
        begin
          DataSet := FdmkEditCB.DBLookupComboBox.ListSource.DataSet;
          if DataSet.State <> dsInactive then
          begin
            if DataSet.Locate(FRefCampo, FValUsuVal, []) then
            begin
              if FdmkEditCB.ValueVariant <>
              DataSet.FieldByName(FdmkEditCB.DBLookupComboBox.KeyField).AsVariant then
              begin
                FdmkEditCB.ValueVariant :=
                DataSet.FieldByName(FdmkEditCB.DBLookupComboBox.KeyField).AsVariant;
              end;
              TemValor := True;
            end;
          end;
        end;
    if not TemValor then
      FdmkEditCB.Text := '';
  end;
end;

function TdmkValUsu.GetValue: Variant;
var
  DataSet: TDataSet;
begin
  if FdmkEditCB <> nil then
    if FdmkEditCB.DBLookupComboBox <> nil then
      if FdmkEditCB.DBLookupComboBox.ListSource <> nil then
        if FdmkEditCB.DBLookupComboBox.ListSource.DataSet <> nil then
        begin
          DataSet := FdmkEditCB.DBLookupComboBox.ListSource.DataSet;
          if DataSet.State <> dsInactive then
          begin
            try
              if FdmkEditCB.ValueVariant <> varEmpty then
                SetValue(DataSet.FieldByName(FRefCampo).AsVariant)
              else
                SetValue(varEmpty);
            except
              Geral.MB_Erro('"RefCampo" n�o definido no componente "dmkValUsu"!');
              SetValue(Unassigned);
            end;
          end;
        end;
  Result := FValUsuVal;
end;

procedure TdmkValUsu.SetValue(Value: Variant);
begin
  if VarType(Value) <> VarType(FValUsuVal) then
  begin
     FValUsuVal := Unassigned;
     TVarData(FValUsuVal).VType := VarType(Value);
  end;
  if Value <> FValUsuVal then
  begin
    FValUsuVal := Value;
    Modified;
  end;
end;

procedure TdmkValUsu.SetOldValor(Value: Variant);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkValUsu.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkValUsu.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkValUsu.SetRefCampo(Value: String);
begin
  if Value <> FRefCampo then
  begin
    FRefCampo := Value;
  end;
end;

procedure TdmkValUsu.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkValUsu]);
end;

end.
