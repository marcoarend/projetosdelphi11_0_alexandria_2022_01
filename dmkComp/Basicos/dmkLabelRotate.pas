unit dmkLabelRotate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MMSystem,ExtCtrls,Buttons,StdCtrls;

type
  TOnMouseOverEvent = procedure(Sender: TObject) of object;
  TOnMouseOutEvent = procedure(Sender: TObject) of object;
  TAngulo = (ag0,ag45,ag90,ag135,ag180,ag225,ag270,ag315);
  TdmkLabelRotate = class(TGraphicControl)
  private

    FAngulo:TAngulo;
    FZaokretanje:integer;
    FOnMouseOver: TOnMouseOverEvent;
    FOnMouseOut: TOnMouseOutEvent;
    FText3D:boolean;

    procedure DrawCaption;
    procedure DrawCaptionEnabled;
    procedure DrawCaption3D;
    procedure Krecenje;
    procedure SetAngulo(value:TAngulo);
    function GetTransparent: Boolean;
    procedure SetTransparent(Value: Boolean);
    procedure SetText3D(AText3D: Boolean);

    procedure CMMouseEnter(var AMsg: TMessage);
              message CM_MOUSEENTER;
    procedure CMMouseLeave(var AMsg: TMessage);
              message CM_MOUSELEAVE;
    procedure CmTextChanged(var Message: TWmNoParams);
              message CM_TEXTCHANGED;
    procedure CMSysColorChange(var Message: TMessage);
              message CM_SYSCOLORCHANGE;
    procedure CMColorChanged(var Message: TMessage);
              message CM_COLORCHANGED;
    procedure CmEnabledChanged(var Message: TWmNoParams);
              message CM_ENABLEDCHANGED;
    procedure CmParentColorChanged(var Message: TWMNoParams);
              message CM_PARENTCOLORCHANGED;
    procedure CmVisibleChanged(var Message: TWmNoParams);
              message CM_VISIBLECHANGED;
    procedure CmFontChanged(var Message: TWMNoParams);
              message CM_FONTCHANGED;

  protected
    procedure Paint;override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure IntToAngle(const value:integer);
    
  published
    property Text3D:Boolean read FText3D write SetText3D default False;
    property Transparent: Boolean read GetTransparent write SetTransparent default False;
    property Angle:TAngulo read FAngulo write SetAngulo default ag45;
    property Caption;
    property Font;
    property Color;
    property ParentColor;
    property Enabled;
    property Visible;
    property Align;
    property ShowHint;
    property DragCursor;
    property DragMode;

    property OnClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseUp;
    property OnMouseMove;
    property OnStartDrag;
    property OnMouseEnter: TOnMouseOverEvent read FOnMouseOver write FOnMouseOver;
    property OnMouseExit: TOnMouseOutEvent read FOnMouseOut write FOnMouseOut;
    property PopupMenu;
    

       {$IFDEF PDJ_D3}
  {$ELSE}
    property Anchors;
    property Constraints;
    property DragKind;
  {$ENDIF}

  end;

procedure Register;

implementation
var   tf : TFont;
Krec:TColor;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkLabelRotate]);
end;

constructor TdmkLabelRotate.Create(AOwner: TComponent);
begin
inherited Create(AOwner);
SetBounds(0, 0, 90, 90);
ControlStyle := ControlStyle + [csOpaque];
FAngulo:=ag45;
FZaokretanje:=450;
FText3D:=False;
with Font do
begin
Name:='Arial';
Size:=10;
Style:=Font.Style + [fsBold];
Color:=clRed;
end;
end;

destructor TdmkLabelRotate.Destroy;
begin
inherited Destroy;
end;

procedure TdmkLabelRotate.SetText3D(AText3D: Boolean);
begin
FText3D:=AText3D;
Invalidate;
end;


function TdmkLabelRotate.GetTransparent: Boolean;
begin
  Result := not (csOpaque in ControlStyle);
end;

procedure TdmkLabelRotate.SetTransparent(Value: Boolean);
begin
  if Transparent <> Value then
  begin
    if Value then
      ControlStyle := ControlStyle - [csOpaque] else
      ControlStyle := ControlStyle + [csOpaque];
    Invalidate;
  end;
end;

procedure TdmkLabelRotate.IntToAngle( const value:integer);
begin
Case value of
0: SetAngulo(ag0);
45: SetAngulo(ag45);
90: SetAngulo(ag90);
135: SetAngulo(ag135);
180: SetAngulo(ag180);
225: SetAngulo(ag225);
270: SetAngulo(ag270);
315: SetAngulo(ag315);
end;
end;

procedure TdmkLabelRotate.SetAngulo(value:TAngulo);
begin
if value<>FAngulo then
begin
FAngulo:=value;
Case FAngulo of
  ag0: FZaokretanje:=0;
 ag45: FZaokretanje:=450;
 ag90: FZaokretanje:=900;
ag135: FZaokretanje:=1350;
ag180: FZaokretanje:=1800;
ag225: FZaokretanje:=2250;
ag270: FZaokretanje:=2700;
ag315: FZaokretanje:=3150;
end;
Invalidate;
end;
end;

procedure TdmkLabelRotate.Paint;
begin
Krecenje;
DrawCaption;
end;


procedure TdmkLabelRotate.Krecenje;
begin
if transparent then exit;
with canvas do
begin
brush.color:=color;
fillrect(clientrect);
end;
end;


procedure TdmkLabelRotate.DrawCaption;
var
  lf : TLogFont;
begin
  with Canvas do begin
    Font.Assign(Self.Font);
    Krec:=Font.Color;
    tf := TFont.Create;
    brush.style:=bsclear;
    try
      tf.Assign(Font);
      GetObject(tf.Handle, sizeof(lf), @lf);
      lf.lfEscapement := FZaokretanje;
      lf.lfOrientation := FZaokretanje;
      tf.Handle := CreateFontIndirect(lf);
      Font.Assign(tf);
    finally

    end;
    if (Enabled) and (FText3D) then DrawCaption3D;
    if not Enabled then DrawCaptionEnabled;
    Case FZaokretanje of
    0:   TextOut((Width-TextWidth(Caption))div 2,(Height-TextHeight(Caption))div 2, caption);
    450: TextOut(TextHeight(Caption) div 3,Height-TextHeight(Caption), caption);
    900: TextOut((Width-TextHeight(Caption)) div 2,Height-(Height-TextWidth(Caption))div 2, caption);
    1350:TextOut(Width-TextHeight(Caption),Height-TextHeight(Caption) div 3, caption);
    1800:TextOut(Width-((Width-TextWidth(Caption))div 2),((Height-TextHeight(Caption))div 2)+TextHeight(Caption), caption);
    2250:TextOut(Width-TextHeight(Caption) div 3,TextHeight(Caption), caption);
    2700:TextOut(((Width-TextHeight(Caption))div 2)+TextHeight(Caption),(Height-Textwidth(Caption))div 2, caption);
    3150:TextOut(TextHeight(Caption),TextHeight(Caption) div 3, caption);
    end;
  end;
   tf.Free;
end;

procedure TdmkLabelRotate.DrawCaptionEnabled;
begin
with canvas do
  begin
  Font.Assign(tf);
  brush.style:=bsclear;
   font.Color :=clBtnHighlight;
    Case FZaokretanje of
    0:   TextOut(1+(Width-TextWidth(Caption))div 2,1+(Height-TextHeight(Caption))div 2, caption);
    450: TextOut(1+TextHeight(Caption) div 3,1+Height-TextHeight(Caption), caption);
    900: TextOut(1+(Width-TextHeight(Caption)) div 2,1+Height-(Height-TextWidth(Caption))div 2, caption);
    1350:TextOut(1+Width-TextHeight(Caption),1+Height-TextHeight(Caption) div 3, caption);
    1800:TextOut(1+Width-((Width-TextWidth(Caption))div 2),1+((Height-TextHeight(Caption))div 2)+TextHeight(Caption), caption);
    2250:TextOut(1+Width-TextHeight(Caption) div 3,1+TextHeight(Caption), caption);
    2700:TextOut(1+((Width-TextHeight(Caption))div 2)+TextHeight(Caption),1+(Height-Textwidth(Caption))div 2, caption);
    3150:TextOut(1+TextHeight(Caption),1+TextHeight(Caption) div 3, caption);
    end;

  font.color :=clBtnShadow;
  end;
end;

procedure TdmkLabelRotate.DrawCaption3D;
begin
with canvas do
  begin
  Font.Assign(tf);
  brush.style:=bsclear;
   font.Color :=clBtnHighlight;
    case FZaokretanje of
    0:   TextOut(((Width-TextWidth(Caption))div 2)-1,((Height-TextHeight(Caption))div 2)-1, caption);
    450: TextOut((TextHeight(Caption) div 3)-1,(Height-TextHeight(Caption))-1, caption);
    900: TextOut(((Width-TextHeight(Caption)) div 2)-1,(Height-(Height-TextWidth(Caption))div 2)-1, caption);
    1350:TextOut((Width-TextHeight(Caption))-1,(Height-TextHeight(Caption) div 3)-1, caption);
    1800:TextOut((Width-((Width-TextWidth(Caption))div 2))-1,(((Height-TextHeight(Caption))div 2)+TextHeight(Caption))-1, caption);
    2250:TextOut((Width-TextHeight(Caption) div 3)-1,(TextHeight(Caption))-1, caption);
    2700:TextOut((((Width-TextHeight(Caption))div 2)+TextHeight(Caption))-1,((Height-Textwidth(Caption))div 2)-1, caption);
    3150:TextOut((TextHeight(Caption))-1,(TextHeight(Caption) div 3)-1, caption);
    end;

  font.color :=Krec;
  end;
end;

procedure TdmkLabelRotate.CMMouseEnter(var AMsg: TMessage);
begin
if Assigned(FOnMouseOver) then FOnMouseOver(Self);
end;

procedure TdmkLabelRotate.CMMouseLeave(var AMsg: TMessage);
begin
if Assigned(FOnMouseOut) then FOnMouseOut(Self);
end;

procedure TdmkLabelRotate.CmTextChanged(var Message: TWmNoParams);
begin
  inherited;
  Invalidate;
end;

procedure TdmkLabelRotate.CMSysColorChange(var Message: TMessage);
begin
  inherited;
  Invalidate;
end;

procedure TdmkLabelRotate.CMColorChanged(var Message: TMessage);
begin
  inherited;
  Invalidate;
end;

procedure TdmkLabelRotate.CmEnabledChanged(var Message: TWmNoParams);
begin
  inherited;
  Invalidate;
end;

procedure TdmkLabelRotate.CmParentColorChanged(var Message: TWMNoParams);
begin
  inherited;
 Invalidate;
end;

procedure TdmkLabelRotate.CmVisibleChanged(var Message: TWmNoParams);
begin
  inherited;
end;

procedure TdmkLabelRotate.CmFontChanged(var Message: TWMNoParams);
begin
  inherited;
  Invalidate;
end;


end.
