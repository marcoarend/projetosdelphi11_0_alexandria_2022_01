unit dmkEditEdTxt;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, dmkEdit,
  UnDmkEnums(*, dmkEditEdIdx*);

type
  TdmkEditEdTxt = class(TdmkEdit)
  private
    { Private declarations }
    //FdmkEditEdIdx: TdmkEditEdIdx;
    FdmkEditEdTxt: TdmkEditEdTxt;

    //procedure SetdmkEditEdIdx(Value: TdmkEditEdIdx);
    procedure SetdmkEditEdTxt(Value: TdmkEditEdTxt);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property dmkEditEdTxt: TdmkEditEdTxt read FdmkEditEdTxt write SetdmkEditEdTxt;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditEdTxt]);
end;

{ TdmkEditEdTxt }

constructor TdmkEditEdTxt.Create(AOwner: TComponent);
begin
  inherited;
  ReadOnly               := True;
  TabStop                := False;
  //AutoSetIfOnlyOneReg    := TSetValCompo.setregOnlyManual;
  Width	  	             := 300;
  Text                   := '';
  FormatType             := dmktfString;
  //FValMin                := IntToStr(-2147483647);
  ValueVariant           := '';
  //
  //FOldRedef              := Null;
  //FNewRedef              := Null;

end;

destructor TdmkEditEdTxt.Destroy;
begin
  try
    if FdmkEditEdTxt <> nil then
      FdmkEditEdTxt := nil;
    FdmkEditEdTxt.Free;
  finally
    inherited;
  end;
end;

procedure TdmkEditEdTxt.SetdmkEditEdTxt(Value: TdmkEditEdTxt);
begin
  if Value <> FdmkEditEdTxt then
  begin
    FdmkEditEdTxt := Value;
  end;
end;

end.
