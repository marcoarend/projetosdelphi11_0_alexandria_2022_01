﻿unit dmkDBGridTRB;

interface

uses
  System.SysUtils, System.Classes, Winapi.Windows, Vcl.Controls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Graphics, Types, Messages, DB, StdCtrls, dmkVariable, Menus,
  Variants, TypInfo, dmkGeral;

type
  TCustomTRBDBGrid = class;
  //
  TSQLEnh = class(TCollectionItem)
  private
    FName: string;
    FFieldName: String;
    FKeyField: String;
    FTableAlias: String;
    FSQLCondition: String;
    FSQLFilter: TdmkSQLFilter;
    //
    FSQLSortOrder: TdmkSQLSortOrder;
    //FSequenceFilter: TdmkSequenceFilter;
    // Integer
    FIntMin: Int64;
    FIntMax: Int64;
    FIntInterleaved: String;
    FIntSeqFilter: TdmkSequenceFilter;
    //
    FOnTitleRgnClick: TNotifyEvent;
    procedure SetFieldName(const Value: String);
    procedure SetKeyField(const Value: String);
    procedure SetTableAlias(const Value: String);
    procedure SetSQLFilter(const Value: TdmkSQLFilter);
    procedure SetSQLSortOrder(const Value: TdmkSQLSortOrder);
    //procedure SetSequenceFilter(const Value: TdmkSequenceFilter);
  protected
    function GetDisplayName: String; override;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(aSource: TPersistent); override;
    function GetNamePath: string; override;
  published
    property Name: string read FName write FName;
    property FieldName: String read FFieldName write SetFieldName;
    property KeyField: String read FKeyField write SetKeyField;
    property TableAlias: String read FTableAlias write SetTableAlias;
    property SQLFilter: TdmkSQLFilter read FSQLFilter write SetSQLFilter;
    property SQLSortOrder: TdmkSQLSortOrder read FSQLSortOrder write SetSQLSortOrder;
    //property SQLSequenceFilter: TdmkSequenceFilter read FSequenceFilter write SetSequenceFilter;
    //
    property OnTitleRgnClick: TNotifyEvent read FOnTitleRgnClick write FOnTitleRgnClick;
  end;

  TSQLEnhs = class(TCollection)
  private
    FGrid: TCustomTRBDBGrid;
    function GetSQLEnh(Index: Integer): TSQLEnh;
    procedure SetSQLEnh(Index: Integer; const Value: TSQLEnh);
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(aGrid: TCustomTRBDBGrid);
    function Add: TSQLEnh;
    property Items[Index: Integer]: TSQLEnh read GetSQLEnh write SetSQLEnh; default;
  end;

  TSQLBasic = class(TPersistent)
  private
    FGrid: TCustomTRBDBGrid;
    FBaseSQLSelect: TStrings;
    FSQLConditionStart: TdmkSQLConditionStart;
    procedure SetBaseSQLSelect(Value: TStrings);
    procedure SetSQLConditionStart(Value: TdmkSQLConditionStart);
  public
    constructor Create(aGrid: TCustomTRBDBGrid);
  published
    property BaseSQLSelect: TStrings read FBaseSQLSelect write SetBaseSQLSelect;
    property SQLConditionStart: TdmkSQLConditionStart read FSQLConditionStart write SetSQLConditionStart;
  end;

  TPaintInfo = record
    //MouseInCol: Integer; // the column that the mouse is in
    //ColPressed: Boolean; // a column has been pressed
    //ColPressedIdx: Integer; // idx of the pressed column
    //ColSizing: Boolean; // currently sizing a column
    //ColMoving: Boolean; // currently moving a column
    //Indicators: TImageList; // Lista de imagens de indicadores { TODO : CFDBGRID }
    //SortArrows: TImageList;
    //IndicatorsWidth: Byte; // Largura da coluna de indicadores. Em estado normal � 17 em estado multiselect � 31 { TODO : CFDBGRID }
    BmpTitleDown: Vcl.Graphics.TBitmap;
    BmpTitleDownFilter: Vcl.Graphics.TBitmap;
    BmpTitleMasterFilter: Vcl.Graphics.TBitmap;
    SQLImgStatusHWSize: Integer;
  end;

  TCustomTRBDBGrid = class(TCustomDBGrid)
  private
    { Private declarations }
    FPaintInfo: TPaintInfo;
    FSQLEnhs: TSQLEnhs;
    FSQLBasic: TSQLBasic;
    //
    FDHStart: TDateTime;
    FDHEnd: TDateTime;
    // Integer Filter
    FSelectedSQLEnh: TSQLEnh;
    //
    FFilterMascSet: Int32;
    FSortOrder: TdmkSQLSortOrder;
    //
    FIncNames: Integer;
    //
    procedure DrawArrow3(ACanvas: TCanvas; const ARect: TRect; Filtered: Boolean);
    procedure DrawArrow1(ACanvas: TCanvas; const ARect: TRect; Filtered: Boolean);
    procedure SetSelectedSQLEnh(Value: TSQLEnh);
  protected
    { Protected declarations }
    procedure DrawCell(ACol, ARow: Longint;ARect: TRect; AState: TGridDrawState); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseTitleClick(Cell: TGridCoord; X, Y: Integer);
    procedure MouseTitleCornerMasterClick(MX, MY: Integer);
    property SQLEnhs: TSQLEnhs read FSQLEnhs write FSQLEnhs;
    property SQLBasic: TSQLBasic read FSQLBasic write FSQLBasic;
    property SelectedSQLEnh: TSQLEnh read FSelectedSQLEnh write SetSelectedSQLEnh;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    //
    procedure ClearAllSearch();
    procedure FilterByIntInterleaved();
    procedure FilterByIntRange();

  end;

  TdmkDBGridTRB = class (TCustomTRBDBGrid)
  private
    { Private declarations }
    FColResize: TNotifyEvent;
  protected
    procedure ColWidthsChanged; override;
  public
    { Public declarations }
    property Canvas;
    property SelectedRows;
  published
    { Published declarations }
    property Align;
    property Anchors;
    property BiDiMode;
    property BorderStyle;
    property Color;
    [Stored(False)]
    property Columns stored False;
    property Constraints;
    property Ctl3D;
    property DataSource;
    property DefaultDrawing;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DrawingStyle;
    property Enabled;
    property FixedColor;
    property GradientEndColor;
    property GradientStartColor;
    property Font;
    property ImeMode;
    property ImeName;
    property Options;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property TitleFont;
    property Touch;
    property Visible;
    property StyleElements;
    //
    property SQLEnhs;
    property SQLBasic;
    property SelectedSQLEnh;
    //
    property OnCellClick;
    property OnColEnter;
    property OnColExit;
    property OnColumnMoved;
    property OnDrawDataCell;  { obsolete }
    property OnDrawColumnCell;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEditButtonClick;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGesture;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnStartDock;
    property OnStartDrag;
    property OnTitleClick;
    // ini Dermatek ...
////////////////////////////////////////////////////////////////////////////////
    // Descendente do TCustomDBGrid.ColWidthsChanged >> Ações após mudar largura
    // de qualquer coluna do DBGrid
      // Uso aqui:
      // * Reposicionar o BitBtns (SpeedButton?) na linha de títulos.
    property OnColResize: TNotifyEvent read FColResize Write FColResize;
////////////////////////////////////////////////////////////////////////////////
    // ... fim Dermatek.
  end;
  // ... fim do TDBGrid.
////////////////////////////////////////////////////////////////////////////////

procedure Register;

implementation

uses Themes
   , GraphUtil
   , Forms
   , ImgList
   , UxTheme,
   UnTRB_PF,
   UnTRBForms;

 {$R dmkDBGridTRB.res}

const

  //CO_ARROW_WIDTH = 16;

  bmTitleDownFiltered = 'DMKDBTITLEDWNFILTERED';
  bmTitleDown         = 'DMKDBTITLEDOWN';
  bmTitleMaster       = 'DMKDBTITLEMASTER';
  //


function Max(Frst, Sec: LongInt): LongInt;
begin
  if Frst >= Sec then
    Result := Frst
  else
    Result := Sec
end;

function GetAlias(SQLEnh: TSQLEnh): String;
begin
  if Trim(SQLEnh.TableAlias) <> EmptyStr then
    Result := Trim(SQLEnh.TableAlias) + '.'
  else
    Result := EmptyStr;
end;

function ConditonEAlias(const SQLEnh: TSQLEnh; var Alias(*, Condition*): String): Boolean;
const
  sProcName = 'ConditonEAlias()';
begin
  Result := False;
  if SQLEnh.KeyField = EmptyStr then
  begin
    Geral.MB_Erro('KeyField indefinido para ' + SQLEnh.Name +
    ' em ' + sProcName);
  end else
  begin
    SQLEnh.FSQLCondition := EmptyStr;
    Alias := GetAlias(SQLEnh);
    Result := True;
  end;
end;

procedure ReopenQuery(Self: TCustomTRBDBGrid);
  function UpdateQuery(DataSet: TDataSet; SQL: String): Boolean;
  var
    PropInfo: PPropInfo;
    SQLObj: TStrings;
  begin
    Result := False;
    try
      PropInfo := GetPropInfo(DataSet, 'SQL', [tkClass]);
      if not Assigned(PropInfo) then Exit;
      SQLObj := TStrings(GetObjectProp(DataSet, PropInfo, TStrings));
      if not Assigned(SQLObj) then Exit;
      DataSet.Close;
      SQLObj.Text := SQL;
      DataSet.Open;
      Result := True;
    except
      // nothing
    end;
  end;
var
  SQL: String;
  I: Integer;
  W_A: String;
  DS: TDataSource;
  Qr: TDataSet;
begin
  SQL := Self.FSQLBasic.BaseSQLSelect.Text;
  if Self.FSQLBasic.FSQLConditionStart = TdmkSQLConditionStart.dmkcsWhere then
    W_A := ' WHERE '
  else
    W_A := ' AND ';
  for I := 0 to Self.FSQLEnhs.Count - 1 do
  begin
    if Self.FSQLEnhs[I].FSQLCondition <> EmptyStr then
    begin
      SQL := SQL + sLineBreak + W_A + Self.FSQLEnhs[I].FSQLCondition;
      W_A := ' AND ';
    end;
  end;
  //Geral.MB_Teste(SQL);
  if Self.DataLink.DataSet <> nil then
    UpdateQuery(Self.DataLink.DataSet, SQL);
end;


{ TCustomTRBDBGrid }

procedure TCustomTRBDBGrid.ClearAllSearch();
var
  I: Integer;
begin
  for I := 0 to Self.FSQLEnhs.Count - 1 do
    Self.FSQLEnhs[I].FSQLCondition := EmptyStr;
  Self.Invalidate;
  ReopenQuery(Self);
end;

constructor TCustomTRBDBGrid.Create(AOwner: TComponent);
var
  Bmp: TBitmap;
begin
  inherited;
  ZeroMemory(@FPaintInfo,SizeOf(TPaintInfo));
  //
  FIncNames := 0;
  //
  FDHStart := Int(Date) - 90;
  FDHEnd := Date;
  //
  FSelectedSQLEnh := nil;
  //
  FFilterMascSet := 7; // max until now (2020-01-05)
  FSortOrder := TdmkSQLSortOrder.dmksoASC;
  //
  //
  FSQLEnhs := TSQLEnhs.Create(Self);
  FSQLBasic := TSQLBasic.Create(Self);
  //
  FPaintInfo.BmpTitleDown := TBitmap.Create;
  FPaintInfo.BmpTitleDown.LoadFromResourceName(HInstance, bmTitleDown);
  FPaintInfo.BmpTitleDown.Canvas.Brush.Color := clFuchsia;
  //
  FPaintInfo.BmpTitleDownFilter := TBitmap.Create;
  FPaintInfo.BmpTitleDownFilter.LoadFromResourceName(HInstance, bmTitleDownFiltered);
  FPaintInfo.BmpTitleDownFilter.Canvas.Brush.Color := clFuchsia;
  //
  FPaintInfo.BmpTitleMasterFilter := TBitmap.Create;
  FPaintInfo.BmpTitleMasterFilter.LoadFromResourceName(HInstance, bmTitleMaster);
  FPaintInfo.BmpTitleMasterFilter.Canvas.Brush.Color := clFuchsia;
  //
  //
  FPaintInfo.SQLImgStatusHWSize := Max(FPaintInfo.BmpTitleDown.Width, FPaintInfo.BmpTitleDown.Height)
end;

destructor TCustomTRBDBGrid.Destroy;
begin
  FPaintInfo.BmpTitleDown.Free;
  FPaintInfo.BmpTitleDownFilter.Free;
  //
  FSQLBasic.FBaseSQLSelect.Free;
  FSQLBasic.Free;
  FSQLEnhs.Free;
  //
  inherited;
end;

procedure TCustomTRBDBGrid.DrawArrow1(ACanvas: TCanvas; const ARect: TRect;
  Filtered: Boolean);
var
  TempRect: TRect;
  Dimension: Integer;
begin
//ver largura e altura!
  Dimension := FPaintInfo.SQLImgStatusHWSize;
  //
  TempRect := ARect;
  TempRect.Top:=TempRect.Top+1;
  TempRect.Bottom:=TempRect.Bottom+1;
  with TempRect do
  begin
    Top    := ((Bottom + Top) - Dimension) shr 1;
    Bottom := Top+Dimension;
    Left   := Right - Dimension + 6;
    Right  := Left + Dimension;
  end;
  ACanvas.Copyrect(TempRect, FPaintInfo.BmpTitleMasterFilter.Canvas, rect(0, 0, Dimension, Dimension - 6))
end;

procedure TCustomTRBDBGrid.DrawArrow3(ACanvas: TCanvas; const ARect: TRect;
  Filtered: Boolean);
var
  TempRect: TRect;
  Dimension: Integer;
begin
  Dimension := FPaintInfo.SQLImgStatusHWSize;
  //
  TempRect := ARect;
  TempRect.Top:=TempRect.Top+1;
  TempRect.Bottom:=TempRect.Bottom+1;
  with TempRect do
  begin
    Top    := ((Bottom + Top) - Dimension) shr 1;
    Bottom := Top+Dimension;
    Left   := Right - Dimension;
    Right  := Left + Dimension;
  end;
  if Filtered then
    ACanvas.Copyrect(TempRect, FPaintInfo.BmpTitleDownFilter.Canvas, rect(0, 0, Dimension, Dimension))
  else
    ACanvas.Copyrect(TempRect, FPaintInfo.BmpTitleDown.Canvas, rect(0, 0, Dimension, Dimension));
end;

procedure TCustomTRBDBGrid.DrawCell(ACol, ARow: Longint; ARect: TRect;
  AState: TGridDrawState);
var
  I: Integer;
  UpperCaseFieldName: String;
  Filtered: Boolean;
begin
  Canvas.FillRect(ARect);
  inherited DrawCell(ACol, ARow, ARect, AState);

  if (ARow = 0) and (ACol = 0) then
    DrawArrow1(Canvas, ARect, (*Filtered*)False)
  else
  if (ARow = 0) and (ACol >0) then
  begin
    // Desenha
    UpperCaseFieldName := AnsiUppercase(Columns[ACol - 1].FieldName);
    for I := 0 to FSQLEnhs.Count - 1 do
    begin
      if AnsiUppercase(FSQLEnhs[I].FieldName) = UpperCaseFieldName then
      begin
        Filtered := FSQLEnhs[I].FSQLCondition <> EmptyStr;
        DrawArrow3(Canvas, ARect, Filtered);
        Break;
      end;
    end;
  end;
end;

procedure TCustomTRBDBGrid.FilterByIntInterleaved();
var
  SQLEnh: TSQLEnh;
  Interleaved: String;
  Field, Alias, (*Condition,*) Str, StrRng, SQLRng, SQLInrtlv, SQL: String;
  Lista: TStringList;
  OK: Boolean;
  I: Integer;
begin
  SQLEnh := Self.SelectedSQLEnh;
  Interleaved := SQLEnh.FIntInterleaved;
  //
  if TRBForms.ShowFormIntegerInterleaved(Interleaved) then
  begin
    //
    SQLEnh.FIntInterleaved := Interleaved;
    //
    if ConditonEAlias(SQLEnh, Alias(*, Condition*)) then
    begin
      if Interleaved <> EmptyStr then
      begin
        //Geral.MB_Teste(Interleaved);
        Field := Alias + SQLEnh.KeyField;
        //
        Lista := TStringList.Create;
        try
          Lista := Geral.IsInterleavedText(Interleaved, OK);
          if OK then
          begin
            SQLInrtlv := EmptyStr;
            SQLRng    := EmptyStr;
            for I := 0 to Lista.Count - 1 do
            begin
              //Geral.MB_Teste(Lista[I]);
              Str := StringReplace(Lista[I], ' ', EmptyStr, [rfReplaceAll]);
              if Str = Geral.SoNumero_TT(Str) then
                SQLInrtlv := SQLInrtlv + ',' + Str
              else
              begin
                //if SQLRng <> EmptyStr then
                  SQLRng := SQLRng + sLineBreak + ' OR (';
                StrRng := StringReplace(Str, '>', ' AND ', [rfReplaceAll]);
                SQLRng := SQLRng + Field + ' BETWEEN ' + StrRng + ') ';
              end;
            end;
            //
            SQL := EmptyStr;
            if SQLInrtlv <> EmptyStr then
            begin
              SQLInrtlv := Copy(SQLInrtlv, 2);
              SQL := Field + ' IN (' + SQLInrtlv + ') ' + sLineBreak;
            end;
            if SQL = EmptyStr then
              StrRng := Copy(StrRng, 4); // eliminate first " OR"
            SQL := SQL + SQLRng;
            //
            if SQL <> EmptyStr then
              SQL := ' (' + sLineBreak + SQL + sLineBreak + ') ';
            SQLEnh.FSQLCondition := SQL;
          end else
          begin
            Geral.MB_Aviso('O texto "' + Str + '" não é válido para filtro de intervalo(s)[2]');
          end;
        finally
          Lista.Free;
        end;
      end else
        SQLEnh.FSQLCondition := EmptyStr;
      //
      ReopenQuery(Self);
    end;
  end;
    //
{
    if ConditonEAlias(SQLEnh, Alias, Condition) then
    begin
      if Interleaved <> EmptyStr then
      begin
        Geral.MB_Teste(Interleaved);
        (*
        SQLEnh.FSQLCondition := Alias + SQLEnh.KeyField +
        ' IN (' + Interleaved + ') ';
        *)
      end else
        SQLEnh.FSQLCondition := EmptyStr;
      //
      ReopenQuery(Self);
    end;
}
end;

procedure TCustomTRBDBGrid.FilterByIntRange();
var
  SQLEnh: TSQLEnh;
  IntMin, IntMax: Int64;
  Alias(*, Condition*): String;
begin
  SQLEnh := Self.SelectedSQLEnh;
  IntMin := SQLEnh.FIntMin;
  IntMax := SQLEnh.FIntMax;
  //
  if TRBForms.ShowFormIntegerRange(IntMin, IntMax) then
  begin
    SQLEnh.FIntMin := IntMin;
    SQLEnh.FIntMax := IntMax;
    //
    if ConditonEAlias(SQLEnh, Alias(*, Condition*)) then
    begin
      SQLEnh.FSQLCondition := Alias + SQLEnh.KeyField +
        ' BETWEEN ' + IntToStr(IntMin) + ' AND ' +
        IntToStr(IntMax);
      ReopenQuery(Self);
    end;
  end;
end;

procedure TCustomTRBDBGrid.MouseTitleClick(Cell: TGridCoord; X, Y: Integer);
const
  sProcName = 'TCustomTRBDBGrid.MouseUp()';
  //
var
  FieldName, Alias, Condition: String;
  I: Integer;
  DHStart, DHEnd: TDateTime;
  SQLEnh: TSQLEnh;
begin
  FieldName := Columns[RawToDataColumn(Cell.X)].FieldName;
  if Cell.X > 0 then
  begin
    for I := 0 to FSQLEnhs.Count - 1 do
    begin
      Self.FSelectedSQLEnh := FSQLEnhs[I];
      //
      if AnsiUppercase(FSQLEnhs[I].FieldName) = AnsiUppercase(FieldName) then
      begin
        case FSQLEnhs[I].FSQLFilter of
          TdmkSQLFilter.dmksfNone:; // nothing
          TdmkSQLFilter.dmksfText:;
          TdmkSQLFilter.dmksfDateRange:;
          TdmkSQLFilter.dmksfTimeRange:;
          TdmkSQLFilter.dmksfDateTimeRange:
          begin
            if TRBForms.ShowFormDateRange(
            FDHStart, FDHEnd, FFilterMascSet, FSortOrder) then
            begin
              FSQLEnhs[I].FSQLCondition := EmptyStr;
              if FDHStart > FDHEnd then
              begin
                DHStart := FDHEnd;
                DHEnd := FDHStart;
              end else
              begin
                DHStart := FDHStart;
                DHEnd := FDHEnd
              end;
              if FSQLEnhs[I].KeyField = EmptyStr then
                Geral.MB_Erro('KeyField indefinido para ' + FSQLEnhs[I].Name +
                ' em ' + sProcName);
              Alias := GetAlias(FSQLEnhs[I]);
              //
              FSQLEnhs[I].FSQLCondition := Alias + FSQLEnhs[I].KeyField +
                ' BETWEEN "' + Geral.FDT(DHStart, 109) + '" AND "' +
                Geral.FDT(DHEND, 109) + '"';
            end;
            //Geral.MB_Teste(FSQLEnhs[I].FSQLCondition);
            ReopenQuery(Self);
          end;
          TdmkSQLFilter.dmksfInteger:;
          TdmkSQLFilter.dmksfIntegerRange:
          begin
            TRB_PF.ShowFormIntegers(Self, X, Y);
          end;
          TdmkSQLFilter.dmksfFloat:;
          TdmkSQLFilter.dmksfFloatRange:;
          else
            Geral.MB_ERRO('SQLFilter não implementado!' + sLineBreak + sProcName)
        end;
        //  Não pode aqui! vai anular variável antes do uso
        //===================================================
        //  if Assigned(FSQLEnhs[I].FOnTitleRgnClick) then
        //    FSQLEnhs[I].FOnTitleRgnClick(Self);
      end;
    end;
    //Self.FSelectedSQLEnh := nil;
  end;
end;

procedure TCustomTRBDBGrid.MouseTitleCornerMasterClick(MX, MY: Integer);
begin
  TRB_PF.ShowMasterPopup(Self, MX, MY);
end;

procedure TCustomTRBDBGrid.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
  //
////////////////////////////////////////////////////////////////////////////////
  // NÃO colocar var aqui!!!
  // Para de funcionar o  MouseCoord(X,Y); !!!!
////////////////////////////////////////////////////////////////////////////////
 //
var
  Cell: TGridCoord;
  DrawInfo: TGridDrawInfo;
  I, Ini, Lst, Fim: Integer;
  MX, MY: Integer;
begin
  MX := Mouse.CursorPos.X;
  MY := Mouse.CursorPos.Y;
  //
////////////////////////////////////////////////////////////////////////////////
  // NÃO implementar mais nada aqui!!!
  // Pode parar de funcionar a function MouseCoord(X,Y); !!!!
////////////////////////////////////////////////////////////////////////////////
  //
  inherited MouseUp(Button, Shift, X, Y);
  //if (Button = mbRight) and (Cell.X >= 1) and (Cell.Y >= 0) then
  if (Button = mbLeft) and (X < 12) and (Y < 16) then
    MouseTitleCornerMasterClick(MX, MY)
  else
  if (Button = mbLeft) and (Cell.X >= 1) and (Cell.Y >= 0) then
  //Cell := Self.MouseCoord(X,Y);
  Cell := MouseCoord(X,Y);
  if Cell.Y < 1 then
  begin
    Ini := 0;
    Fim := 0;
    CalcDrawInfo(DrawInfo);
    for I := 0 to Cell.X do
    begin
      Lst := DrawInfo.Horz.GetExtent(I) + GridLineWidth;
      Fim := Fim + Lst;
    end;
    Ini := Fim - FPaintInfo.SQLImgStatusHWSize;
    if (X >= Ini) and (X <= Fim) then
    begin
      MouseTitleClick(Cell, MX, MY);
  //
////////////////////////////////////////////////////////////////////////////////
  // NÃO implementar mais nada aqui!!!
  // Pode parar de funcionar a function MouseCoord(X,Y); !!!!
////////////////////////////////////////////////////////////////////////////////
  //
    end;
  end;
  //
////////////////////////////////////////////////////////////////////////////////
  // NÃO implementar mais nada aqui!!!
  // Pode parar de funcionar a function MouseCoord(X,Y); !!!!
////////////////////////////////////////////////////////////////////////////////
  //
end;

procedure TCustomTRBDBGrid.SetSelectedSQLEnh(Value: TSQLEnh);
begin
  if FSelectedSQLEnh <> Value then
    FSelectedSQLEnh := Value;
end;

{ TdmkDBGridTRB }

procedure TdmkDBGridTRB.ColWidthsChanged;
begin
  inherited;
  if Assigned(FColResize) then  FColResize(Self);
end;

{ TSQLEnh }

procedure TSQLEnh.Assign(aSource: TPersistent);
begin
  //inherited;
	//FFieldName := TSQLEnh(aSource).FieldName;
	FName := TSQLEnh(aSource).Name;
end;

constructor TSQLEnh.Create(Collection: TCollection);
begin
  inherited;
  FName := ClassName + IntToStr(ID);
  FFieldName := EmptyStr;
  FKeyField := EmptyStr;
  TableAlias := EmptyStr;
  FSQLCondition := EmptyStr;
  FSQLFilter := TdmkSQLFilter.dmksfNone;
  FSQLSortOrder := TdmkSQLSortOrder.dmksoASC;
end;

function TSQLEnh.GetDisplayName: String;
begin
  Result := FName;
end;

function TSQLEnh.GetNamePath: string;
begin
  //Result := Format('%s%d',['MyField', Index])
  //Result := inherited GetNamePath + Format('SQLEnh%d',[Index + 1]);
  Result := GetDisplayName;
end;

procedure TSQLEnh.SetFieldName(const Value: String);
begin
  if FFieldName <> Value then
    FFieldName := Value;
end;

procedure TSQLEnh.SetKeyField(const Value: String);
begin
  if FKeyField <> Value then
    FKeyField := Value;
end;

procedure TSQLEnh.SetSQLFilter(const Value: TdmkSQLFilter);
begin
  if FSQLFilter <> Value then
    FSQLFilter := Value;
end;

procedure TSQLEnh.SetSQLSortOrder(const Value: TdmkSQLSortOrder);
begin
  if FSQLSortOrder <> Value then
    FSQLSortOrder := Value;
end;

procedure TSQLEnh.SetTableAlias(const Value: String);
begin
  if FTableAlias <> Value then
    FTableAlias := Value;
end;


{ TSQLEnhs }

function TSQLEnhs.Add: TSQLEnh;
begin
	Result := TSQLEnh(inherited Add);
end;

constructor TSQLEnhs.Create(aGrid: TCustomTRBDBGrid);
begin
	inherited Create(TSQLEnh);
	FGrid := aGrid;
end;

function TSQLEnhs.GetSQLEnh(Index: Integer): TSQLEnh;
begin
	Result := TSQLEnh(inherited Items[Index]);
end;

procedure TSQLEnhs.SetSQLEnh(Index: Integer; const Value: TSQLEnh);
begin
	Items[Index].Assign(Value);
end;

procedure TSQLEnhs.Update(Item: TCollectionItem);
begin
  inherited;
	FGrid.Invalidate;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkDBGridTRB]);
end;

{ TSQLBasic }

constructor TSQLBasic.Create(aGrid: TCustomTRBDBGrid);
begin
  FBaseSQLSelect := TStringList.Create;
  FSQLConditionStart := TdmkSQLConditionStart.dmkcsAnd;
  //
  FGrid := aGrid;
end;

procedure TSQLBasic.SetBaseSQLSelect(Value: TStrings);
begin
  if FBaseSQLSelect.Text <> Value.Text then
    FBaseSQLSelect.Assign(Value);
end;

procedure TSQLBasic.SetSQLConditionStart(Value: TdmkSQLConditionStart);
begin
  if FSQLConditionStart <> Value then
    FSQLConditionStart := Value;
end;

end.
