unit dmkEditEdTxt;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, dmkEdit,
  UnDmkEnums(*, dmkEditEdIdx*);

type
  TdmkEditEdTxt = class(TdmkEdit)
  private
    { Private declarations }
    FdmkEditEdIdx: TdmkEditEdIdx;

    procedure SetdmkEditEdIdx(Value: TdmkEditEdIdx);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property dmkEditEdIdx: TdmkEditEdIdx read FdmkEditEdIdx write SetdmkEditEdIdx;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditEdTxt]);
end;

{ TdmkEditEdTxt }

constructor TdmkEditEdTxt.Create(AOwner: TComponent);
begin
  inherited;
  ReadOnly               := True;
  TabStop                := False;
  //AutoSetIfOnlyOneReg    := TSetValCompo.setregOnlyManual;
  Width	  	             := 300;
  Text                   := '';
  FormatType             := dmktfString;
  //FValMin                := IntToStr(-2147483647);
  ValueVariant           := '';
  //
  //FOldRedef              := Null;
  //FNewRedef              := Null;

end;

destructor TdmkEditEdTxt.Destroy;
begin
  try
    if FdmkEditEdIdx <> nil then
      FdmkEditEdIdx := nil;
    FdmkEditEdIdx.Free;
  finally
    inherited;
  end;
end;

procedure TdmkEditEdTxt.SetdmkEditEdIdx(Value: TdmkEditEdIdx);
begin
  if Value <> FdmkEditEdIdx then
  begin
    FdmkEditEdIdx := Value;
  end;
end;

end.
