unit dmkEditEdIdx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkEdit, DBCtrls,  DB, Variants, dmkGeral, UnDmkEnums,
  dmkCompoEnums, dmkWinMessages;

type
  TdmkEditEdIdx = class(TdmkEdit)
  private
    { Private declarations }
    FOnSincroLoc: TNotifyEvent;
    FOldRedef, FNewRedef: Variant;
    FDmkEditEdTxt: TdmkEdit;
    FIgnoradmkEdit: Boolean;
    FAutoSetIfOnlyOneReg: TSetValCompo;
    FLocF7CodiFldName: String;
    FLocF7NameFldName: String;
    FLocF7TableName: String;
    FLocF7SQLText: TStrings;
    FLocF7SQLMasc: String;
    FLocF7PreDefProc: TdmkF7PreDefProc;
    FLocF7DmkDataBase: TDmkDBLookup;
    procedure SetDmkEditEdTxt(Value: TdmkEdit);
    procedure SetIgnoradmkEdit(Value: Boolean);
    procedure SetAutoSetIfOnlyOneReg(Value: TSetValCompo);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    //function  TemDataSet(): Boolean;
    procedure SetDmkDataBase(Value: TDmkDBLookup); virtual;
    procedure SetLocF7CodiFldName(Value: String);
    procedure SetLocF7NameFldName(Value: String);
    procedure SetLocF7TableName(Value: String);
    procedure SetLocF7SQLText(Value: TStrings);
    procedure SetLocF7SQLMasc(Value: String);
    procedure SetLocF7PreDefProc(Value: TdmkF7PreDefProc);
  protected
    { Protected declarations }
    procedure Change; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure OnFim();
    // Ini 2018-02-16
    procedure SelIfOnlyOneReg(Forma: TSetValCompo);
    // Fim 2018-02-16
  published
    { Published declarations }
    property DmkEditEdTxt: TDmkEdit read FDmkEditEdTxt write SetDmkEditEdTxt;
    property IgnoradmkEdit: Boolean read FIgnoradmkEdit write SetIgnoradmkEdit;
    property OnSincroLoc: TNotifyEvent read FOnSincroLoc write FOnSincroLoc;
    property AutoSetIfOnlyOneReg: TSetValCompo read FAutoSetIfOnlyOneReg write FAutoSetIfOnlyOneReg;
    property LocF7CodiFldName: String read FLocF7CodiFldName write SetLocF7CodiFldName;
    property LocF7NameFldName: String read FLocF7NameFldName write SetLocF7NameFldName;
    property LocF7TableName: String read FLocF7TableName write SetLocF7TableName;
    //property IgnoraEditCB: Boolean read FIgnoraEditCB write SetIgnoraEditCB;
    property LocF7SQLText: TStrings read FLocF7SQLText write SetLocF7SQLText; {^1^}
    property LocF7SQLMasc: String read FLocF7SQLMasc write SetLocF7SQLMasc;
    property LocF7PreDefProc: TdmkF7PreDefProc read FLocF7PreDefProc write SetLocF7PreDefProc;
    property LocF7DmkDataBase: TDmkDBLookup read FLocF7DmkDataBase write SetDmkDataBase;
  end;

procedure Register;

implementation

uses
  {$IfNDef CompilingCompo}
  ModuleGeral(*, MeuDBUses*);
  {$EndIf}

constructor TdmkEditEdIdx.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  IgnoradmkEdit          := False;
  AutoSetIfOnlyOneReg    := TSetValCompo.setregOnlyManual;
  Width	  	             := 56;
  Text                   := '0';
  FormatType             := dmktfInteger;
  FValMin                := IntToStr(-2147483647);
  ValueVariant           := 0;
  //
  FOldRedef              := Null;
  FNewRedef              := Null;
  //
  FLocF7SQLText := TStringList.Create;
  FLocF7SQLMasc := '$#'; // CO_JOKE
  FLocF7PreDefProc := f7pNone;
end;

destructor TdmkEditEdIdx.Destroy;
begin
  if FDmkEditEdTxt <> nil then
    FDmkEditEdTxt := nil;
    FLocF7SQLText.Free;
  inherited;
end;

procedure TdmkEditEdIdx.SelIfOnlyOneReg(Forma: TSetValCompo);
begin
end;

procedure TdmkEditEdIdx.SetAutoSetIfOnlyOneReg(Value: TSetValCompo);
begin
  if Value <> FAutoSetIfOnlyOneReg then
    FAutoSetIfOnlyOneReg := Value;
end;

procedure TdmkEditEdIdx.SetDmkDataBase(Value: TDmkDBLookup);
begin
  if Value <> FLocF7DmkDataBase then
  begin
    FLocF7DmkDataBase := Value;
    Invalidate;
  end;
end;

procedure TdmkEditEdIdx.SetDmkEditEdTxt(Value: TdmkEdit);
begin
  if Value <> FDmkEditEdTxt then
  begin
    FDmkEditEdTxt := Value;
  end;
end;

procedure TdmkEditEdIdx.SetIgnoradmkEdit(Value: Boolean);
begin
  if Value <> FIgnoradmkEdit then
    FIgnoradmkEdit := Value;
end;

procedure TdmkEditEdIdx.SetLocF7CodiFldName(Value: String);
begin
  if Value <> FLocF7CodiFldName then
  begin
    FLocF7CodiFldName := Value;
  end;
end;

procedure TdmkEditEdIdx.SetLocF7NameFldName(Value: String);
begin
  if Value <> FLocF7NameFldName then
  begin
    FLocF7NameFldName := Value;
  end;
end;

procedure TdmkEditEdIdx.SetLocF7PreDefProc(Value: TdmkF7PreDefProc);
begin
  if Value = Null then Exit;
  if Value <> FLocF7PreDefProc then
    FLocF7PreDefProc := Value;
end;

procedure TdmkEditEdIdx.SetLocF7SQLMasc(Value: String);
begin
  if FLocF7SQLMasc <> Value then
    FLocF7SQLMasc := Value;
end;

procedure TdmkEditEdIdx.SetLocF7SQLText(Value: TStrings);
begin
  if FLocF7SQLText.Text <> Value.Text then
    FLocF7SQLText.Assign(Value);
end;

procedure TdmkEditEdIdx.SetLocF7TableName(Value: String);
begin
  if Value <> FLocF7TableName then
  begin
    FLocF7TableName := Value;
  end;
end;

procedure TdmkEditEdIdx.WMKeyDown(var Message: TWMKeyDown);
begin
  inherited;
  {$IfNDef CompilingCompo}
  if Message.CharCode = VK_F7 then
  begin
    // J� definido em TUnMyObjects.FormMsg()!!!
    //MeuDBUses.VirtualKey_F7('')
  end;
  {$EndIf}
end;

procedure TdmkEditEdIdx.Change();
var
  Codigo: Integer;
begin
  inherited;
  if self.Focused = False then
  begin
    {$IfNDef CompilingCompo}
    Codigo := self.ValueVariant;
    DmodG.BuscaTextoDeDmkEditEdIdx_Int(Codigo, FLocF7DmkDataBase, LocF7CodiFldName,
      FLocF7NameFldName, FLocF7TableName, (*FLocF7SQLText, FLocF7SQLMasc,
      FLocF7PreDefProc,*) DmkEditEdTxt);
    {$EndIf}
  end;
end;

procedure TdmkEditEdIdx.CMEnter(var Message: TCMEnter);
begin
  FOldRedef := Self.ValueVariant;
  SelIfOnlyOneReg(TSetValCompo.setregOnCompoCMEnter);
  inherited;
end;

procedure TdmkEditEdIdx.CMExit(var Message: TCMExit);
begin
  OnFim;
  FNewRedef := Self.ValueVariant;
  if FOldRedef <> FNewRedef then
  if Assigned(FOnSincroLoc) then
    FOnSincroLoc(Self);
  inherited;
end;

procedure TdmkEditEdIdx.OnFim;
var
  Codigo: Integer;
begin
  // N�o funciona!!!!
  //SendMessage(Self.Handle, MSG_DMK_EditEdIdxTxt, 0, 0);
  //Self.Perform(MSG_DMK_EditEdIdxTxt, 0, 0);
  //Self.Perform(WM_NEXTDLGCTL, 0, 0);
////////////////////////////////////////////////////////////////////////////////
  {$IfNDef CompilingCompo}
  Codigo := self.ValueVariant;
  DmodG.BuscaTextoDeDmkEditEdIdx_Int(Codigo, FLocF7DmkDataBase, LocF7CodiFldName,
    FLocF7NameFldName, FLocF7TableName, (*FLocF7SQLText, FLocF7SQLMasc,
    FLocF7PreDefProc,*) DmkEditEdTxt);
  {$EndIf}
////////////////////////////////////////////////////////////////////////////////
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditEdIdx]);
end;

end.
