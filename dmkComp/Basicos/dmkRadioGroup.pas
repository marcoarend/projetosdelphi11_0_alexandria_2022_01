unit dmkRadioGroup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, dmkGeral, UnDmkEnums;

type
  TdmkRadioGroup = class(TRadioGroup)
  private
    { Private declarations }
    FQryName: String;
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FOldValor: Integer;

    procedure SetQryName(Value: String);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetOldValor(Value: Integer);
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property QryName: String read FQryName write SetQryName;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property OldValor: Integer read FOldValor write SetOldValor;
  end;

procedure Register;

implementation

procedure TdmkRadioGroup.SetOldValor(Value: Integer);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkRadioGroup.SetQryName(Value: String);
begin
  if Value <> FQryName then
  begin
    FQryName := Value;
  end;
end;

procedure TdmkRadioGroup.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkRadioGroup.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkRadioGroup.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkRadioGroup]);
end;

end.
