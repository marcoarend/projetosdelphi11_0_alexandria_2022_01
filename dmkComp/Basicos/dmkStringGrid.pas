unit dmkStringGrid;

interface

uses
  SysUtils, Classes, Controls, Grids, Messages, Forms, Windows;

type
  //TGridCracker = class(TStringgrid);
  TdmkStringGrid = class(TStringGrid)
  private
    { Private declarations }
    FDownCell: TGridCoord;
    procedure HeaderClicked(const Cell: TGridCoord);
    procedure SortGrid(ByColumn: Integer;
      Ascending: Boolean);
  protected
    { Protected declarations }
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect;
      AState: TGridDrawState); override;
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkStringGrid]);
end;

{ TdmkStringGrid }

procedure TdmkStringGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  //
  if (Button = mbLeft) and (Shift = [ssLeft]) then
    Self.MouseToCell( X, Y, FDownCell.X, FDownCell.Y )
  else
    with FDownCell do begin
      X:= 0;
      Y:= 0;
    end;
end;

procedure TdmkStringGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
  p: TGridCoord;
  grid: TStringgrid;
begin
  inherited;
  //
  grid := Self;
  if (Button = mbLeft) and (Shift = []) then begin
    grid.MouseToCell( X, Y, p.X, p.Y );
    if CompareMem(@p, @FDownCell, sizeof(p))
       and (p.Y < grid.FixedRows)
       and (p.X >= grid.FixedCols)
    then
      HeaderClicked(p);
  end;
  with FDownCell do begin
    X:= 0;
    Y:= 0;
  end;
end;

procedure TdmkStringGrid.SortGrid(ByColumn: Integer;
  Ascending: Boolean);
  procedure ExchangeGridRows( i, j: Integer );
  Var
    k: Integer;
  begin
    with Self do
      for k:= 0 To ColCount-1 Do
        Cols[k].Exchange(i,j);
  end;
  procedure QuickSort(L, R: Integer);
  var
    I, J: Integer;
    P: String;
  begin
    repeat
      I := L;
      J := R;
      P := Self.Cells[byColumn, (L + R) shr 1];
      repeat
        while CompareStr(Self.Cells[byColumn, I], P) < 0 do Inc(I);
        while CompareStr(Self.Cells[byColumn, J], P) > 0 do Dec(J);
        if I <= J then
        begin
          if I <> J then
            ExchangeGridRows( I, J );
          Inc(I);
          Dec(J);
        end;
      until I > J;
      if L < J then QuickSort(L, J);
      L := I;
    until I >= R;
  end;
 procedure InvertGrid;
   Var
     i, j: Integer;
   begin
     i:= Self.Fixedrows;
     j:= Self.Rowcount-1;
     While i < j do begin
       ExchangeGridRows( I, J );
       Inc( i );
       Dec( j );
     end; { While }
   end;
begin
  Screen.Cursor := crHourglass;
  Self.Perform(WM_SETREDRAW, 0, 0 );
  try
    QuickSort( Self.FixedRows, Self.Rowcount-1 );
    if not ascending then
      InvertGrid;
  finally
    Self.Perform( WM_SETREDRAW, 1, 0 );
    Self.Refresh;
    Screen.Cursor := crDefault;
  end;
end;

procedure TdmkStringGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);
var
  grid: TStringgrid;
  marker: Char;
begin
  inherited;
  //
  grid:= Self as TStringgrid;
  // paint the sort marker on header columns
  if (ACol >= Grid.FixedCols) and (aRow = 0) then
    if Assigned( Grid.Objects[ aCol, aRow ] ) then begin
      if Integer( Grid.Objects[ aCol, aRow ] ) > 0 then
        marker := 't'  // up wedge in Marlett font
      else
        marker := 'u'; // down wedge in Marlett font


      with Grid.canvas do begin
        font.Name := 'Marlett';
        font.Charset := SYMBOL_CHARSET;
        font.Size := 12;
        textout(ARect.Right - TextWidth(marker),
                ARect.Top + 2, marker );
        font := Grid.font;
      end;
    end;
end;

procedure TdmkStringGrid.HeaderClicked(const Cell: TGridCoord);
var
  i: Integer;
begin
  // The header cell stores a flag in the Objects property that signals the
  // current sort order of the grid column. A value of 0 shows no sort marker,
  // 1 means sorted ascending, -1 sorted descending


  // clear markers
  for i:= Self.FixedCols to Self.ColCount-1 Do
    if Assigned( Self.Objects[ i, 0 ] ) and (i <> cell.x) then begin
      Self.Objects[i ,0]:= nil;
      Self.InvalidateCell( i, 0 );
    end;


  // Sort grid on new column. if grid is currently sorted ascending on this
  // column we invert the sort direction, otherwise we sort it ascending.
  if Integer( Self.Objects[cell.x,cell.y] ) = 1 then begin
    SortGrid(cell.x, false );
    Self.Objects[cell.x, 0]:= Pointer(-1);
  end
  else begin
    SortGrid(cell.x, true );
    Self.Objects[cell.x, 0]:= Pointer(1);
  end;
  Self.InvalidateCell( cell.x, cell.y );
end;

end.
