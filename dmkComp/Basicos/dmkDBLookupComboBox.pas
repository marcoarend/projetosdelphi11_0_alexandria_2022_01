unit dmkDBLookupComboBox;

interface

uses
  Windows,  Messages, SysUtils, Classes,  Graphics, Controls,  Forms, Data.DB,
  Dialogs,  DBCtrls,  StdCtrls, dmkEditCB, dmkGeral, Variants, UnDmkEnums;

type
  TdmkDBLookupComboBox = class(TDBLookupComboBox)
  private
    { Private declarations }
    FdmkEditCB: TdmkEditCB;
    FQryName: String;
    FQryCampo: String;
    FLocF7CodiFldName: String;
    FLocF7NameFldName: String;
    FLocF7TableName: String;
    //FIgnoraEditCB: Boolean;
    //FUpdCampo: String;
    FUpdType: TUpdType;
    FIsDropped: Boolean;
    FLocF7SQLText: TStrings;
    FLocF7SQLMasc: String;
    FLocF7PreDefProc: TdmkF7PreDefProc;

    procedure SetdmkEditCB(Value: TdmkEditCB);
    procedure SetQryName(Value: String);
    procedure SetQryCampo(Value: String);
    procedure SetLocF7CodiFldName(Value: String);
    procedure SetLocF7NameFldName(Value: String);
    procedure SetLocF7TableName(Value: String);
    //procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetIsDropped(Value: Boolean);
    //procedure SetIgnoraEditCB(Value: Boolean);
    //procedure CBGetDroppedState(var Message: TMessage); message CB_GETDROPPEDSTATE;
    procedure SetLocF7SQLText(Value: TStrings);
    procedure SetLocF7SQLMasc(Value: String);
    procedure SetLocF7PreDefProc(Value: TdmkF7PreDefProc);
  protected
    { Protected declarations }
    procedure KeyValueChanged; override;
    procedure Click; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DropDown; override;
    procedure CloseUp(Accept: Boolean); override;
    property IsDropped: Boolean read FIsDropped write SetIsDropped;
  published
    { Published declarations }
    property dmkEditCB: TdmkEditCB read FdmkEditCB write SetdmkEditCB;
    property QryName: String read FQryName write SetQryName;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property LocF7CodiFldName: String read FLocF7CodiFldName write SetLocF7CodiFldName;
    property LocF7NameFldName: String read FLocF7NameFldName write SetLocF7NameFldName;
    property LocF7TableName: String read FLocF7TableName write SetLocF7TableName;
    //property IgnoraEditCB: Boolean read FIgnoraEditCB write SetIgnoraEditCB;
    property LocF7SQLText: TStrings read FLocF7SQLText write SetLocF7SQLText; {^1^}
    property LocF7SQLMasc: String read FLocF7SQLMasc write SetLocF7SQLMasc;
    property LocF7PreDefProc: TdmkF7PreDefProc read FLocF7PreDefProc write SetLocF7PreDefProc;
   end;

procedure Register;

implementation

procedure TdmkDBLookupComboBox.KeyValueChanged;
var
  Form: TCustomForm;
  Limpa: Boolean;
begin
  inherited;

  if FdmkEditCB <> nil then
  begin
    if FdmkEditCB.Focused then
      Exit;

    //Para evitar zerar os forms que s�o configurados na abertura
    Form := Geral.GetParentFrom(FdmkEditCB);
    //
    if Form <> nil then
    begin
      if (not Form.Active) or (Screen.ActiveForm.Name <> Form.Name) then
        Exit;
    end;


    if Text = '' then
    begin
      Limpa := False;
      if ListSource = nil then
        Limpa := True
      else
      if (ListSource.DataSet = nil) or (ListSource.DataSet.State = dsInactive) then
        Limpa := True
      else
      if not TDataSet(TDataSource(ListSource).DataSet).Locate(KeyField, KeyValue, []) then
        Limpa := True;
      //
      if Limpa then
        Geral.LimpaValueVariant(FdmkEditCB);
    end;
  end;
end;

procedure TdmkDBLookupComboBox.Loaded;
begin
  inherited;
(* S� funciona se a query j� foi aberta antes de criar o form!
  if ListSource <> nil then
  begin
    if (ListSource.DataSet <> nil) and (ListSource.DataSet.State <> dsInactive) then
      Geral.MB_Info(Geral.FF0(ListSource.DataSet.RecordCount));
  end;
*)
end;

destructor TdmkDBLookupComboBox.Destroy;
begin
  try
    if FdmkEditCB <> nil then
      FdmkEditCB := nil;
    FdmkEditCB.Free;
    FLocF7SQLText.Free;
  finally
    inherited;
  end;
end;

procedure TdmkDBLookupComboBox.CloseUp(Accept: Boolean);
begin
  SetIsDropped(False);
  inherited CloseUp(Accept);
end;

constructor TdmkDBLookupComboBox.Create(AOwner: TComponent);
begin
  //FIgnoraEditCB := False;
  FLocF7SQLText := TStringList.Create;
  FLocF7SQLMasc := '$#'; // CO_JOKE
  FLocF7PreDefProc := f7pNone;
  inherited;
end;

procedure TdmkDBLookupComboBox.SetdmkEditCB(Value: TdmkEditCB);
begin
  if Value <> FdmkEditCB then
  begin
    FdmkEditCB := Value;
  end;
end;

procedure TdmkDBLookupComboBox.SetQryName(Value: String);
begin
  if Value <> FQryName then
  begin
    FQryName := Value;
  end;
end;

procedure TdmkDBLookupComboBox.SetLocF7SQLMasc(Value: String);
begin
  if FLocF7SQLMasc <> Value then
    FLocF7SQLMasc := Value;
end;

procedure TdmkDBLookupComboBox.SetLocF7SQLText(Value: TStrings);
begin
  if FLocF7SQLText.Text <> Value.Text then
    FLocF7SQLText.Assign(Value);
end;

procedure TdmkDBLookupComboBox.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

{
procedure TdmkDBLookupComboBox.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;
}

procedure TdmkDBLookupComboBox.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

{
procedure TdmkDBLookupComboBox.SetIgnoraEditCB(Value: Boolean);
begin
  if Value <> FIgnoraEditCB then
  begin
    FIgnoraEditCB := Value;
  end;
end;
}

procedure TdmkDBLookupComboBox.SetIsDropped(Value: Boolean);
begin
  if Value <> FIsDropped then
  begin
    FIsDropped := Value;
  end;
end;

procedure TdmkDBLookupComboBox.SetLocF7CodiFldName(Value: String);
begin
  if Value <> FLocF7CodiFldName then
  begin
    FLocF7CodiFldName := Value;
  end;
end;

procedure TdmkDBLookupComboBox.SetLocF7NameFldName(Value: String);
begin
  if Value <> FLocF7NameFldName then
  begin
    FLocF7NameFldName := Value;
  end;
end;

procedure TdmkDBLookupComboBox.SetLocF7PreDefProc(Value: TdmkF7PreDefProc);
begin
  if Value = Null then Exit;
  if Value <> FLocF7PreDefProc then
    FLocF7PreDefProc := Value;
end;

procedure TdmkDBLookupComboBox.SetLocF7TableName(Value: String);
begin
  if Value <> FLocF7TableName then
  begin
    FLocF7TableName := Value;
  end;
end;

procedure TdmkDBLookupComboBox.Click;
var
  Continua: Boolean;
  xVT1, xVT2: String;
begin
  inherited;
  if FdmkEditCB <> nil then
  begin
    if (FdmkEditCB.ValueVariant <> Null) and (KeyValue <> Null) and
    (VarType(KeyValue) <> VarType(FdmkEditCB.ValueVariant)) and
    (FdmkEditCB.ValueVariant <> '0') then
    begin
      xVT1 := Trim(Geral.NomeVarType(VarType(FdmkEditCB.ValueVariant)));
      xVT2 := Trim(Geral.NomeVarType(VarType(KeyValue)));
      if (xVT2 = 'String') and (xVT1 = 'UString') then
        Continua := True
      else
      if (KeyValue = '0') and (FdmkEditCB.ValueVariant = 0) then
        Continua := True
      else begin
        if (FdmkEditCB.FormatType <> dmktfInteger)
        and (FdmkEditCB.FormatType <> dmktfInt64)
        and (FdmkEditCB.FormatType <> dmktfLongint) then
        begin
          Continua := False;
          FdmkEditCB.ValueVariant := Null;
          Application.MessageBox(PChar('Tipos de dados diferentes entre os componentes "' +
          Self.Name + '" (tipo = ' + Geral.NomeVarType(VarType(KeyValue)) + ') e "' +
          FdmkEditCB.Name + '" (tipo = ' + Geral.NomeVarType(VarType(FdmkEditCB.ValueVariant)) +
          '). AVISE A DERMATEK!'), 'Aviso', MB_OK+MB_ICONERROR);
        end else Continua := True;
      end;
    end else Continua := True;
    if Continua then
    begin
      //if KeyValue <> Null then
      //begin
        if FdmkEditCB.ValueVariant <> KeyValue then
        try
          {
          if FIgnoraEditCB then
          begin
            if (KeyValue = Null) or (KeyValue = 0) or (KeyValue = '0') then
            begin
              // N�o faz nada!
              ShowMessage('KeyValue ignorado!');
            end else
              FdmkEditCB.ValueVariant := KeyValue;
          end else
          }
            FdmkEditCB.ValueVariant := KeyValue;
        except
          // nada ?
        end;
      //end;
      SetIsDropped(False);
    end;
  end;
//CBN_SELCHANGE
end;

procedure TdmkDBLookupComboBox.DropDown;
begin
  inherited;
  SetIsDropped(True);
end;

procedure TdmkDBLookupComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if Key = VK_TAB then
  begin
    if IsDropped then SetIsDropped(False);
  end;
  if Key = VK_RETURN then
  begin
    // 2009-02-24
    if IsDropped then
      Self.Click;
    // fim
    SetIsDropped(False);
    Key := VK_TAB;
  end;
  inherited KeyDown(Key, Shift);
end;

{
procedure TdmkDBLookupComboBox.CBGetDroppedState(var Message: TMessage);
begin
  ShowMessage('Dropped?');
end;
}

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkDBLookupComboBox]);
end;

end.
