// ini Delphi 28 Alexandria
//{$I dmk.inc}
// fim Delphi 28 Alexandria
//
unit dmkEditDateTimePicker;
{
Data-aware date-time picker made of borderless TDBEdit embedded into TDateTimePicker

Features:
*Looks like TDateTimePicker, is rendered themed on XP
*can handle NULL values: shows empty field when not focused and date part placeholders (for example: __/__/__ ) when focused
*allows masked input (uses default mask, when no EditMask defined on the field)
*after completing one date part input (eg. day), caret goes to the next part (eg. month), ie. no need to type date separator characters
*clearing the field (with backspace or del) makes NULL value
*Drop-down calendar can show week numbers (like TMonthCalendar and unlike TDateTimePicker)

It's freeware.

Author: Aivar Annamaa

http://www.hot.ee/aivarannamaa/DBEditDateTimePicker.zip
}
interface

uses
  Windows, Messages, SysUtils, Classes, Controls, ComCtrls, Forms,
  {$IFDEF DELPHI12_UP}
  System.Types, TypInfo,
  {$ENDIF}
  CommCtrl, Dialogs, mask, dmkEdit, dmkGeral, Variants, UnDmkEnums,
  UnComps_Vars;

type
  TdmkEditDateTimePicker = class;

  TDateTimePickerdmkEdit = class(TdmkEdit)
  private
    FAutoApplyEditMask: Boolean;
    FChanging: Boolean;
    FDefaultEditMask: String; //TEditMask
    //
    function GetPicker : TdmkEditDateTimePicker;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    //procedure CMChange(var Message: TCMChange); message CM_CHANGE;
  protected
    procedure KeyPress(var Key: Char); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure Change; override;
    procedure MostraErro(Sender: TObject; E: Exception);

  public
    property DefaultEditMask : String read FDefaultEditMask write FDefaultEditMask;
    property AutoApplyEditMask : Boolean read FAutoApplyEditMask write FAutoApplyEditMask;
    procedure GoBackCompo();
  published
    property ValueVariant;
  end;

  TdmkEditDateTimePicker = class(TDateTimePicker)
  private
    FdmkEdit : TDateTimePickerdmkEdit;
    FDefaultEditMask: String;
    FAutoApplyEditMask: Boolean;
    IgnoreChange : Boolean;
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FOldValor: TDateTime;
    FLastDayTimePicker: TDateTimePicker;
    FDatePurpose: TDatePurpose;
    FMinDatePurpose: TDateTime;
    FMaxDatePurpose: TDateTime;
    FAtuData: TDateTime;
    //FdmkEditWidth: Integer;
    //procedure SetdmkEditWidth(Value: Integer);
    //
    //FDateTime: TDateTime;
    {
    function GetDataField: string;
    function GetDataSource: TDataSource;
    function GetField: TField;
    procedure SetDataField(const Value: string);
    procedure SetDataSource(const Value: TDataSource);
    procedure SetFieldValue;
    }
    FOnRedefInPlace: TNotifyEvent;
    //
    procedure SetdmkEditValue;
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
    procedure DoDropDown;
    procedure DoCloseUp;
    procedure PrepareCalendar;
    procedure AddStyle (Handle : HWND; Value : Integer);
    procedure CNNotify (var Message: TWMNotify); message CN_NOTIFY;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    function GetButtonWidth : Integer;
    function CanEdit : Boolean;
    procedure SetDefaultEditMask(const Value: String);
    procedure SetAutoApplyEditMask(const Value: Boolean);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetOldValor(Value: TDateTime);
    procedure SetLastDayTimePicker(Value: TDateTimePicker);
    procedure SetDatePurose(const Value: TDatePurpose);
    {
    procedure SetDateTime(Value: TDateTime);
    procedure SetTime(Value: TTime);
    procedure SetDate(Value: TDate);
    function GetTime: TTime;
    function GetDate: TDate;
    }
  protected
    function MsgSetDateTime(Value: TSystemTime): Boolean; override;
    //
    //procedure AfterConstruction; override;
    procedure Resize; override;
    procedure Change; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
  public
    procedure AfterConstruction; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    constructor Create(AOwner: TComponent); override;
    procedure ToggleCalendar;
    //property Field : TField read GetField;
    property dmkEdit : TDateTimePickerdmkEdit read FdmkEdit;
    function Focused : Boolean; override;
    procedure SetFocus; override;
    procedure SetaDatasMinimaEMaximaAuto(Virtualmente: Boolean);
    procedure SetaDataSafe(Data: TDateTime);
  published
    //property DataField : string read GetDataField  write SetDataField;
    //property DataSource : TDataSource read GetDataSource write SetDataSource;
    property ReadOnly : Boolean read GetReadOnly write SetReadOnly;
    property WeekNumbers;
    property DefaultEditMask : String read FDefaultEditMask write SetDefaultEditMask;
    property AutoApplyEditMask : Boolean read FAutoApplyEditMask write SetAutoApplyEditMask;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property OldValor: TDateTime read FOldValor write SetOldValor;
    property LastDayTimePicker: TDateTimePicker read FLastDayTimePicker write SetLastDayTimePicker;
    property DatePurpose: TDatePurpose read FDatePurpose write SetDatePurose;
    //property dmkEditWidth: Integer read FdmkEditWidth write SetdmkEditWidth;
    //property OnChange: TNotifyEvent read FOnChange write FOnChange; override;
    {
    property DateTime: TDateTime read FDateTime write SetDateTime;
    property Time: TTime read GetTime write SetTime;
    property Date: TDate read GetDate write SetDate;
    }
    // ini 2021-01-11 teste! Mostra, mas � perigoso mostrar!?
    property Edit: TDateTimePickerdmkEdit read FdmkEdit;
    property OnRedefInPlace: TNotifyEvent read FOnRedefInPlace write FOnRedefInPlace;
    // fim 2021-01-11 teste! Mostra, mas � perigoso mostrar!?
  end;

procedure Register;

implementation

{ TdmkEditDateTimePicker }

procedure TdmkEditDateTimePicker.AddStyle(Handle: HWND; Value: Integer);
var
	Style : Integer;
begin
	if (Handle = 0) then
    begin
    	exit;
    end;

    Style := GetWindowLong(Handle, GWL_STYLE);
    Style := Style or Value;
    SetWindowLong(Handle, GWL_STYLE, Style);
end;

procedure TdmkEditDateTimePicker.AfterConstruction;
begin
  inherited;
  //
  SetaDatasMinimaEMaximaAuto(False);
end;

function TdmkEditDateTimePicker.CanEdit: Boolean;
begin
{	result := (Field <> nil)
       	and (not ReadOnly)
    	and (Field.DataSet.CanModify);
}
  Result := not ReadOnly;
end;

procedure TdmkEditDateTimePicker.CMEnter(var Message: TCMEnter);
begin
	if (FdmkEdit <> nil) then
  begin
    FdmkEdit.SetFocus;
    //
    SetaDatasMinimaEMaximaAuto(False);
  end;
	inherited;
end;

constructor TdmkEditDateTimePicker.Create(AOwner: TComponent);
begin
  FDefaultEditMask(*.EditMask*) := '!99/99/99;1;_';
    FMinDatePurpose := 0;
    FMaxDatePurpose := 0;
  //
    FAutoApplyEditMask := True;
	inherited;
    FdmkEdit := TDateTimePickerdmkEdit.Create(Self);
    FdmkEdit.BorderStyle := bsNone;
    //
    FdmkEdit.ParentColor := True;
    FdmkEdit.Parent := Self;
    //FdmkEdit.AutoSize := True;

    FdmkEdit.AutoApplyEditMask := AutoApplyEditMask;
    FdmkEdit.DefaultEditMask   := DefaultEditMask;
    FdmkEdit.TabStop           := False;
    //
    FdmkEdit.FormatType        := dmktfDate;
    FdmkEdit.ValueVariant      := Self.DateTime;
    FdmkEdit.FComponentePai    := 'dmkEditDateTimePicker';
    FdmkEdit.FChanging         := False;
    //FDateTime               := Self.DateTime;

    // Gera erro
    //FdmkEdit.Width := Self.ClientWidth - 12;
    //FdmkEdit.Height := Self.ClientHeight - 2;// - GetButtonWidth + 12;
    FdmkEdit.Top    := 2;
    FdmkEdit.Left   := 2;
    FdmkEdit.Height := Self.Height - 4;
    FdmkEdit.Width := Self.Width - (GetButtonWidth * 2);
    //FdmkEdit.Brush.Color  := $00B1B4BA;
    //
    FAtuData := Self.Date;
    SetaDatasMinimaEMaximaAuto(False);
end;

procedure TdmkEditDateTimePicker.DoCloseUp;
var
	ButtonWidth: Integer;
begin
	ButtonWidth:=GetButtonWidth;
	PostMessage(Self.Handle, WM_LBUTTONDOWN, 0,
    	Integer(SMALLPOINT(Self.ClientWidth-ButtonWidth-2, ButtonWidth)));
end;

procedure TdmkEditDateTimePicker.DoDropDown;
var
	XWidht: Integer;
begin
	XWidht := GetButtonWidth div 2;
	PostMessage(Self.Handle, WM_LBUTTONDOWN, 0,
    	Integer(SMALLPOINT(Self.ClientWidth-XWidht, XWidht)));
end;

function TdmkEditDateTimePicker.GetButtonWidth: Integer;
begin
	result := GetSystemMetrics(SM_CXVSCROLL);
end;

procedure TdmkEditDateTimePicker.GetChildren(Proc: TGetChildProc;
  Root: TComponent);
begin
	inherited;
    //Proc (FdmkEdit);
end;

function TdmkEditDateTimePicker.GetReadOnly: Boolean;
begin
	result := FdmkEdit.ReadOnly;
end;

procedure TdmkEditDateTimePicker.PrepareCalendar;
var
  Calhandle : HWND;
  CalRect : TRect;
  ST: TSystemTime;
begin
  SetaDatasMinimaEMaximaAuto(False);
  //
  Calhandle := self.GetCalendarHandle;

  if Calhandle = 0 then
  begin
    //Calhandle.
    Exit;
  end;

  IgnoreChange := True;
  if (DmkEdit.ValueVariant = Null) or
  DmkEdit.ValueVariant = 0 then
  //if (Field <> nil) and (Field.IsNull) then
  begin
    DateTimeToSystemTime(SysUtils.Date, ST);
    Self.Date := SysUtils.Date;
    DmkEdit.ValueVariant := Self.Date;
    //FDateTime := Now;
  end
  else
  begin
    //DateTimeToSystemTime(Field.AsDateTime, ST);
    DateTimeToSystemTime(DmkEdit.ValueVariant, ST);
    if Self.Date <> DmkEdit.ValueVariant then
      Self.Date := DmkEdit.ValueVariant;
    //FDateTime := DmkEdit.ValueVariant;;
  end;
  MonthCal_SetCurSel (CalHandle, ST);
  IgnoreChange := false;
  {}
  if Self.WeekNumbers then
  begin
  //        addStyle (calhandle, MCS_WEEKNUMBERS);
      AddStyle (CalHandle, 4);
  end;

      //   Get the minimum size of the DropDown calendar that will show all
      //   ' of the calendar.
      //    SendMessage mCal_hWnd, MCM_GETMINREQRECT, 0, objRect
      SendMessage (Calhandle, 4105, 0, Integer(@calRect));
  //' ..and size the DropDown calendar to the size required.
  // MoveWindow mCal_hWnd, 0, 0, objRect.Right + 2, objRect.Bottom, True

      MoveWindow (CalHandle, 0, 0, CalRect.Right +2, CalRect.Bottom, true);
end;

procedure TdmkEditDateTimePicker.SetdmkEditValue;
begin
  if CanEdit then
  begin
    if Int(dmkEdit.ValueVariant) <> Int(self.Date) then
      dmkEdit.ValueVariant := Date;
  end;

end;

{
procedure TdmkEditDateTimePicker.SetdmkEditWidth(Value: Integer);
begin
  FdmkEditWidth := Value;
  if dmkEdit.Width <> Value then
      dmkEdit.Width := Value;
end;
}

procedure TdmkEditDateTimePicker.SetReadOnly(const Value: Boolean);
begin
	FdmkEdit.ReadOnly := Value;
end;

procedure TdmkEditDateTimePicker.ToggleCalendar;
begin
	if not Self.DroppedDown then
    begin
        DoDropDown;
    end
    else
    begin
    	DoCloseUp;
    end;
end;

{TDateTimePickerdmkEdit}

procedure TDateTimePickerdmkEdit.CMEnter(var Message: TCMEnter);
begin
	inherited;
end;

procedure TDateTimePickerdmkEdit.CMExit(var Message: TCMExit);
//var
	{OriginalMask : String;//TEditMask;}
  //Redefiniu: Boolean;
begin
  inherited;
  //Meu
  if ValueVariant <> Null then
  begin
    if ValueVariant <> GetPicker.Date then
    begin
      GetPicker.Date := ValueVariant;
      // ini 2021-01-11
      if Assigned(GetPicker.FOnRedefInPlace) then
        GetPicker.FOnRedefInPlace(Self);
      // fim 2021-01-11
    end;
  end;
end;

function TDateTimePickerdmkEdit.GetPicker: TdmkEditDateTimePicker;
begin
	if Parent is TdmkEditDateTimePicker then
    begin
        result := TdmkEditDateTimePicker (Parent);
    end
    else
    begin
    	result := nil;
    end;
end;

procedure TDateTimePickerdmkEdit.GoBackCompo();
{
  function MyFindNextControl(CurControl: TWinControl;
    GoForward, CheckTabStop, CheckParent: Boolean): TWinControl;
  var
    I, StartIndex: Integer;
    List: TList;
  begin
    Result := nil;
    List := TList.Create;
    try
      GetTabOrderList(List);
      if List.Count > 0 then
      begin
        StartIndex := List.IndexOf(CurControl);
        if StartIndex = -1 then
          if GoForward then StartIndex := List.Count - 1 else StartIndex := 0;
        I := StartIndex;
        repeat
          if GoForward then
          begin
            Inc(I);
            if I = List.Count then I := 0;
          end else
          begin
            if I = 0 then I := List.Count;
            Dec(I);
          end;
          CurControl := TWinControl(List[I]);
          if CurControl.CanFocus and
            (not CheckTabStop or CurControl.TabStop) and
            (not CheckParent or (CurControl.Parent = Self)) then
            Result := CurControl;
        until (Result <> nil) or (I = StartIndex);
      end;
    finally
      List.Free;
    end;
end;
}
var
{
  MesAno, Ano, Mes: String;
  Data: TDateTime;
}
  Pai: TwinControl;
// ini Delphi 28 Alexandria
{
  Cjt: TdmkEditDateTimePicker;
  Ant, Atu, Avo, Nxt: TComponent;
  PropInfo: PPropInfo;
  I, Indice: Integer;
}
  Avo: TComponent;
  I: Integer;
// fim Delphi 28 Alexandria
  Form: TForm;
  Conta, StartIndex: Integer;
  Lista: TList;
  Achou, CurControl: TWinControl;
  //
  ListaStr: String;
begin
  Pai := Self.Parent;
  if Pai <> nil then
  begin
    //Indice := Self.ComponentIndex;
    if Pai.ClassName = 'TdmkEditDateTimePicker' then
    begin
      Avo   := Pai;
      Form  := nil;
      Conta := 0;
      while (Avo <> nil) and (Form = nil) and (Conta < 4096) do
      begin
        Avo := TControl(Avo).Parent;
        if Avo is TForm then
          Form := TForm(Avo);
        Conta := Conta + 1;
      end;
      if Form <> nil then
      begin
        Lista := TList.Create;
        try
          Form.GetTabOrderList(Lista);
          ListaStr := '';
          (*
          for I := 0 to Lista.Count -1 do
          begin
            ListaStr := ListaStr + Geral.FF0(I) + ' - ' + TWinControl(Lista.Items[I]).Name + sLinebreak;

          end;
          Geral.MB_Info(ListaStr);
          *)
          StartIndex := Lista.IndexOf(Pai);
          if StartIndex = -1 then
            StartIndex := 0;
          I := StartIndex;
          Achou := nil;
          repeat
            if I = 0 then
              I := Lista.Count;
            I := I - 1;
            CurControl := TWinControl(Lista[I]);
            if CurControl.CanFocus and CurControl.TabStop then
              Achou := CurControl;
          until (Achou <> nil) or (I = StartIndex);
          if Achou <> nil then
            Achou.SetFocus;
        finally
          Lista.Free;
        end;
      end;
    end;
  end;
end;

procedure TDateTimePickerdmkEdit.KeyDown(var Key: Word; Shift: TShiftState);
  procedure IncrementaDias();
  begin
    if Self.ValueVariant = 0 then
    begin
       Self.ValueVariant := Trunc(Now());
    end else
    begin
      case Key of
        (*33*)VK_PRIOR: IncMonth(Self.ValueVariant - 1);
        (*34*)VK_NEXT:  IncMonth(Self.ValueVariant + 1);
        (*35*)VK_END :  Self.ValueVariant := Geral.UltimoDiaDoMes(Self.ValueVariant);
        (*36*)VK_HOME:  Self.ValueVariant := Geral.PrimeiroDiaDoMes(Self.ValueVariant);
        (*37*)VK_LEFT:  Self.ValueVariant := Self.ValueVariant - 7;
        (*38*)VK_UP:    Self.ValueVariant := Self.ValueVariant + 1;
        (*39*)VK_RIGHT: Self.ValueVariant := Self.ValueVariant + 7;
        (*40*)VK_DOWN:  Self.ValueVariant := Self.ValueVariant - 1;
      end;
    end;
  end;
var
  MesAno, Ano, Mes: String;
  Data: TDateTime;
// ini Delphi 28 Alexandria
  {
  Pai: TwinControl;
  Cjt: TdmkEditDateTimePicker;
  Ant, Atu, Avo, Nxt: TComponent;
  I, Indice: Integer;
  PropInfo: PPropInfo;
  Form: TForm;
  Conta, StartIndex: Integer;
  Lista: TList;
  Achou, CurControl: TWinControl;
  }
// fim Delphi 28 Alexandria
begin
  if (Key = VK_F3) then
  begin
    MesAno := Geral.FDT(Geral.PrimeiroDiaDoMes(IncMonth(Date, -2)), 14);
    if InputQuery('Per�odo da Pesquisa', 'Informe o per�odo no formato MM/AAAA',
    MesAno) then
    begin
      Ano := Copy(MesAno, 4);
      Mes := Copy(MesAno, 1, 2);
      try
        Data := EncodeDate(Geral.IMV(Ano), Geral.IMV(Mes), 1);
        Self.ValueVariant := Data;
        if GetPicker <> nil then
        begin
          if GetPicker.FLastDayTimePicker <> nil then
          GetPicker.FLastDayTimePicker.Date :=  Geral.PrimeiroDiaDoMes(IncMonth(Data, 1)) -1;
        end;
      except
        Geral.MensagemBox('M�s/Ano inv�lido! Informe no formato MM/AAAA!',
        'Aviso', MB_OK+MB_ICONWARNING, 0);
      end;
    end;
  end else
  if (Key = VK_F4) and (GetPicker <> nil) then
  begin
    GetPicker.ToggleCalendar;
    //Geral.MB_Info(Self.ClassName);
  end else
  {
  if (Key = 16) then
  begin
    //Screen.ActiveForm.Perform(WM_NEXTDLGCTL, 1, 0);
    //Control := FindNextControl(nil, False, True, False);
    //if Control <> nil then Control.SetFocus;
  end else
  }
  if Key = VK_RETURN then
    Key := VK_TAB
  else
  if (Key in [33..40]) then
    IncrementaDias()
 //
  ;
  inherited;
end;

procedure TdmkEditDateTimePicker.Resize;
begin
	inherited;
    FdmkEdit.Width := Self.Width - (GetButtonWidth * 2);
    FdmkEdit.Height := Self.Height - 4;// - GetButtonWidth + 12;
    FdmkEdit.Update;
    FdmkEdit.Resize;
end;

procedure TdmkEditDateTimePicker.SetDatePurose(const Value: TDatePurpose);
begin
  if Value <> FDatePurpose then
    FDatePurpose := Value;
end;

procedure TdmkEditDateTimePicker.SetDefaultEditMask(const Value: String);
begin
	FDefaultEditMask := Value;
    if FdmkEdit <> nil then
    begin
        FdmkEdit.DefaultEditMask := Value;
    end;
  // em tempo de execu��o s� funciona aqui e na cria��o. No Resize n�o funciona
  //FdmkEdit.Top := 2; // n�o funciona
  FdmkEdit.Width := Self.Width - (GetButtonWidth * 2); //
  FdmkEdit.Height := Self.Height - 4;// - GetButtonWidth + 12;
  FdmkEdit.Update;
  FdmkEdit.Resize;
end;

procedure TDateTimePickerdmkEdit.KeyPress(var Key: Char);
begin
{    if (Field <> nil)
    	and (Field.CanModify)
    	and (Field.IsNull)
        and (Ord (Key) >= Ord('0'))
        and (Ord (Key) <= Ord('9')) then
    begin
    	Field.DataSet.Edit;
        Field.AsDateTime := SysUtils.Date;
    end;
}
  inherited;
end;

procedure TDateTimePickerdmkEdit.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited;
end;

procedure TDateTimePickerdmkEdit.MostraErro(Sender: TObject; E: Exception);
begin
//
end;

procedure TdmkEditDateTimePicker.SetaDataSafe(Data: TDateTime);
var
  Erro: Integer;
begin
  if (Self.MinDate > 2) or (Self.MaxDate > 2) then
  begin
    Erro := 0;
    if (Self.MinDate > 2)
    and (Self.MinDate > Data) then
      Erro := 1;
    if (Self.MaxDate > 2)
    and (Self.MaxDate < Data) then
      Erro := 2;
    case Erro of
      0: Self.Date := Data;
      1:
      begin
        //Data := Self.MinDate;
        Self.Date := Self.MinDate;
        //
        Geral.MB_Erro('Data n�o pode ser menor que a data m�nima: ' +
        Geral.FDT(Self.MinDate, 2));
      end;
      2:
      begin
        //Data := Self.MinDate;
        Self.Date := Self.MinDate;
        //
        //
        Geral.MB_Erro('Data n�o pode ser maior que a data m�xima: ' +
        Geral.FDT(Self.MaxDate, 2));
      end;
    end;
  end else
    Self.Date := Data;
end;

procedure TdmkEditDateTimePicker.SetaDatasMinimaEMaximaAuto(Virtualmente: Boolean);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  //AtuData := Self.Date;
  case FDatePurpose of
    dmkdpNone: ;     // Nada?
    dmkdpGeneric: ;  // Nada?
    dmkdpSPED_EFD_MIN,
    dmkdpSPED_EFD_MAX,
    dmkdpSPED_EFD_MIN_MAX:
    begin
      if (FDatePurpose = dmkdpSPED_EFD_MIN)
      or (FDatePurpose = dmkdpSPED_EFD_MIN_MAX) then
      begin
        if Virtualmente then
          FMinDatePurpose := VAR_Data_SPEDEFDEnce_MovimXX
        else
          if Self.MinDate <> VAR_Data_SPEDEFDEnce_MovimXX then
          begin
            Self.MinDate := VAR_Data_SPEDEFDEnce_MovimXX;
            // Bug no MsgSetDateTime!!
            try
            if (FAtuData > VAR_Data_SPEDEFDEnce_MovimXX)
            and ((MinDate > 0) and (FAtuData >= MinDate))   // 2023-12-25
            and ((MaxDate > 0) and (FAtuData <= MaxDate))   // 2023-12-25
            then
              if Self.Date <> FAtuData then
                Self.Date := FAtuData;
            except

            end;
          end;
      end;
      ////////////////////////////////////////////
      if (FDatePurpose = dmkdpSPED_EFD_MAX)
      or (FDatePurpose = dmkdpSPED_EFD_MIN_MAX) then
      begin
        if VAR_DATA_SERVIDOR > 2 then
          Data := Int(VAR_DATA_SERVIDOR)
        else
          Data := Int(Now());
        //
        DecodeDate(Data, Ano, Mes, Dia);
        Data := EncodeDate(Ano, Mes, 1) - 1;
        if Self.MinDate > Data then
          Data := Self.MinDate;
        if Virtualmente then
          FMaxDatePurpose := Data
        else
          if Self.MaxDate <> Data then
          begin
            Self.MaxDate := Data;
            // Bug no MsgSetDateTime!!
            if (FAtuData < Data)
            and ((MinDate > 0) and (FAtuData >= MinDate))   // 2023-12-25
            and ((MaxDate > 0) and (FAtuData <= MaxDate))   // 2023-12-25
            then
              if Self.Date <> FAtuData then
                Self.Date := FAtuData;
          end;
      end;
    end;
    dmkdpInsumMovimMin,
    dmkdpInsumMovimMinMax:
    begin
      if (FDatePurpose = dmkdpInsumMovimMin)
      or (FDatePurpose = dmkdpInsumMovimMinMax) then
      begin
        if Virtualmente then
          FMinDatePurpose := VAR_Data_Insum_Movim
        else
        begin
          // ini 2023-12-25
          if Self.Date < VAR_Data_Insum_Movim then
            Self.Date := VAR_Data_Insum_Movim;
          // fim 2023-12-25
          if Self.MinDate <> VAR_Data_Insum_Movim then
            Self.MinDate := VAR_Data_Insum_Movim;
        end;
      end;
    end;
    (*dmkdpFinance*)else Geral.MB_Erro(
      '"DatePorpouse" indefinido em "dmkEditDateTimePicker.Create()[1]"');
  end;
end;

procedure TdmkEditDateTimePicker.SetAutoApplyEditMask(const Value: Boolean);
begin
	FAutoApplyEditMask := Value;
    if FdmkEdit <> nil then
    begin
        FdmkEdit.AutoApplyEditMask := Value;
    end;
end;

procedure TdmkEditDateTimePicker.Change;
begin
  if not IgnoreChange then
  begin
    if not FdmkEdit.FChanging then
      SetdmkEditValue;
    inherited;
  end;
end;

function TdmkEditDateTimePicker.Focused: Boolean;
begin
	result := dmkEdit.Focused;
end;

procedure TdmkEditDateTimePicker.SetFocus;
begin
	//inherited;
    dmkEdit.SetFocus;
end;

procedure TdmkEditDateTimePicker.SetLastDayTimePicker(Value: TDateTimePicker);
begin
  if Value <> FLastDayTimePicker then
    FLastDayTimePicker := Value;
end;

procedure TdmkEditDateTimePicker.SetOldValor(Value: TDateTime);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkEditDateTimePicker.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkEditDateTimePicker.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkEditDateTimePicker.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure TdmkEditDateTimePicker.CNNotify(var Message: TWMNotify);
{
var
  ST: TSystemTime;
  Calhandle : HWND;
  CalRect : TRect;
  Data: TDateTime;
}
begin

  case Message.NMHdr.code of
    DTN_DROPDOWN :
    begin
      if CanEdit() then
        PrepareCalendar;
    end;
    {
    DTN_CLOSEUP  :
    begin
      if FdmkEdit.ValueVariant <> Self.GetPicker.Date then
      ValueVariant := GetPicker.Date;
    end;
    }
  end;
  inherited;
end;

{
procedure TdmkEditDateTimePicker.CNNotify(var Message: TWMNotify);
var
  DT: TDateTime;
  AllowChange: Boolean;
begin
  with Message, NMHdr^ do
  begin
    Result := 0;
    case code of
      DTN_CLOSEUP:
        begin
          FDroppedDown := False;
          SetDate(SystemTimeToDateTime(FLastChange));
          if Assigned(FOnCloseUp) then FOnCloseUp(Self);
        end;
      DTN_DATETIMECHANGE:
        begin
          with PNMDateTimeChange(NMHdr)^ do
          begin
            if FDroppedDown and (dwFlags = GDT_VALID) then
            begin
              FLastChange := st;
              FDateTime := SystemTimeToDateTime(FLastChange);
            end
            else begin
              if FShowCheckbox and IsBlankSysTime(st) then
                FChecked := False
              else if dwFlags = GDT_VALID then
              begin
                FLastChange := st;
                DT := SystemTimeToDateTime(st);
                if Kind = dtkDate then SetDate(DT)
                else SetTime(DT);
                if FShowCheckbox then FChecked := True;
              end;
            end;
            Change;
          end;
        end;
      DTN_DROPDOWN:
        begin
          DateTimeToSystemTime(Date, FLastChange);
          FDroppedDown := True;
          if Assigned(FOnDropDown) then FOnDropDown(Self);
        end;
      DTN_USERSTRING:
        begin
          AllowChange := Assigned(FOnUserInput);
          with PNMDateTimeString(NMHdr)^ do
          begin
            if AllowChange then
            begin
              DT := 0.0;
              FOnUserInput(Self, pszUserString, DT, AllowChange);
              DateTimeToSystemTime(DT, st);
            end;
            dwFlags := Ord(not AllowChange);
          end;
        end;
    else
      inherited;
    end;
  end;
end;
}
{
procedure TdmkEditDateTimePicker.SetDateTime(Value: TDateTime);
begin
  inherited;
  dmkEdit.ValueVariant := Value;
end;

function TdmkEditDateTimePicker.GetTime: TTime;
begin
  Result := TTime(FDateTime);
end;

function TdmkEditDateTimePicker.GetDate: TDate;
begin
  Result := TDate(FDateTime);
end;

procedure TdmkEditDateTimePicker.SetDate(Value: TDate);
begin
  inherited;
  dmkEdit.ValueVariant := FDateTime;
end;

procedure TdmkEditDateTimePicker.SetTime(Value: TTime);
begin
  inherited;
  dmkEdit.ValueVariant := FDateTime;
end;

procedure TdmkEditDateTimePicker.Click;
begin
  inherited Click;
  if self.Date <> dmkEdit.ValueVariant then
    dmkEdit.ValueVariant := self.Date;
end;
}

function TdmkEditDateTimePicker.MsgSetDateTime(Value: TSystemTime): Boolean;
var
  Data: TDateTime;
begin
  Result := False;
  if not Geral.DoEncodeDate(Value.wYear, Value.wMonth, Value.wDay, Data) then
    Application.MessageBox(PChar('N�o foi poss�vel criar a data com:' +
    Chr(13) + Chr(10) + 'Ano: ' + IntToStr(Value.wYear)+
    Chr(13) + Chr(10) + 'M�s: ' + IntToStr(Value.wMonth)+
    Chr(13) + Chr(10) + 'Dia: ' + IntToStr(Value.wDay)), 'Erro',
    MB_OK + MB_ICONERROR);
  try
    inherited MsgSetDateTime(Value);
    dmkEdit.ValueVariant := SystemTimeToDateTime(Value);
    Result := True;
  except
    //Result := False;
    Geral.MensagemDeErro('N�o foi poss�vel definir a data!', 'dmkEditDateTimePicker.MsgSetDateTime()');
  end;
end;

procedure TdmkEditDateTimePicker.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    Key := VK_TAB;
  //
  inherited;
end;

procedure TdmkEditDateTimePicker.KeyPress(var Key: Char);
begin
  inherited;

end;

{procedure TdmkDateTimePickerEdit.CMChange(var Message: TCMChange);
begin
  inherited;
  ShowMessage('O N    C H A N G E');
end;}

procedure TDateTimePickerdmkEdit.Change;
var
  Data: TDateTime;
  Pos: Integer;
  Erro: Integer;
begin
  inherited;
  //GetPicker.SetaDatasMinimaEMaximaAuto(True);
  GetPicker.SetaDatasMinimaEMaximaAuto(False);
  //ShowMessage('O N    C H A N G E ' + Self.Name);
  //FChanging := True;
  Pos := 1;
  if Geral.ScanDate(Self.Text, Pos, Data) then
  begin
    try
      if (GetPicker.FMinDatePurpose > 2) or (GetPicker.FMaxDatePurpose > 2) then
      begin
        Erro := 0;
        if (GetPicker.FMinDatePurpose > 2)
        and (GetPicker.FMinDatePurpose > Self.ValueVariant) then
          Erro := 1;
        if (GetPicker.FMaxDatePurpose > 2)
        and (GetPicker.FMaxDatePurpose < Self.ValueVariant) then
          Erro := 2;
        case Erro of
          0: GetPicker.Date := Self.ValueVariant;
          1:
          begin
            Self.ValueVariant := GetPicker.FMinDatePurpose;
            GetPicker.Date := GetPicker.FMinDatePurpose;
            //
            Geral.MB_Erro('Data n�o pode ser menor que a data m�nima: ' +
            Geral.FDT(GetPicker.FMinDatePurpose, 2));
          end;
          2:
          begin
            Self.ValueVariant := GetPicker.FMinDatePurpose;
            GetPicker.Date := GetPicker.FMinDatePurpose;
            //
            //
            Geral.MB_Erro('Data n�o pode ser maior que a data m�xima: ' +
            Geral.FDT(GetPicker.FMaxDatePurpose, 2));
          end;
        end;
      end else
        GetPicker.Date := Self.ValueVariant;
    except
      GetPicker.Date := 0;
    end;
    GetPicker.Click;
  end;
  //FChanging := False;
  //GetPicker.FAtuData := Self.ValueVariant;
  GetPicker.FAtuData := GetPicker.Date;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditDateTimePicker]);
end;

end.
