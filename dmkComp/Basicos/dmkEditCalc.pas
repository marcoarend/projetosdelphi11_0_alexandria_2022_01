unit dmkEditCalc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkEdit, DBCtrls,  DB, Variants, dmkGeral, UnDmkEnums;

type
  TdmkEditCalc = class(TdmkEdit)
  private
    { Private declarations }
    FdmkEditCalcA: TdmkEditCalc;
    FCalcType: TCalcType;
    FCalcFrac: TCalcFrac;
    FOnSetCalc: Boolean;
    procedure SetdmkEditCalcA(Value: TdmkEditCalc);
    procedure OnFim;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure SetCalcType(Value: TCalcType);
    procedure SetCalcFrac(Value: TCalcFrac);
  protected
    { Protected declarations }
    procedure Change; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property dmkEditCalcA: TdmkEditCalc read FdmkEditCalcA write SetdmkEditCalcA;
    property CalcType: TCalcType read FCalcType write SetCalcType;
    property CalcFrac: TCalcFrac read FCalcFrac write SetCalcFrac;
  end;

procedure Register;

implementation

constructor TdmkEditCalc.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  Width		       := 80;
  Text           := '0';
  DecimalSize    := 2;
  FormatType     := dmktfDouble;
  CalcFrac       := cfCento;
  FValMin        := '0';
  ValueVariant   := 0;
  FOnSetCalc     := False;
end;

destructor TdmkEditCalc.Destroy;
begin
  if dmkEditCalcA <> nil then
    dmkEditCalcA := nil;
  inherited;
end;

procedure TdmkEditCalc.SetdmkEditCalcA(Value: TdmkEditCalc);
begin
  if Value <> FdmkEditCalcA then
  begin
    FdmkEditCalcA := Value;
  end;
end;

procedure TdmkEditCalc.SetCalcFrac(Value: TCalcFrac);
begin
  if Value <> FCalcFrac then
  begin
    FCalcFrac := Value;
  end;
end;

procedure TdmkEditCalc.SetCalcType(Value: TCalcType);
begin
  if Value <> FCalcType then
  begin
    FCalcType := Value;
  end;
end;

procedure TdmkEditCalc.Change;
var
  Val, d: Double;
begin
  // Deve ser depois !!!!!
  inherited;
  if not Self.FOnSetCalc then
  begin
    if FdmkEditCalcA <> nil then
    begin
      Val := Geral.ConverteValor(
        FdmkEditCalcA.ValueVariant, FdmkEditCalcA.CalcType, FdmkEditCalcA.CalcFrac);
      d := Self.ValueVariant;
      if Val <> d then
      begin
        FdmkEditCalcA.FOnSetCalc := True;
        Val := Geral.ConverteValor(d, CalcType, CalcFrac);
        FdmkEditCalcA.ValueVariant := Val;
        FdmkEditCalcA.FOnSetCalc := False;
      end;
    end;
  end;
end;

procedure TdmkEditCalc.CMExit(var Message: TCMExit);
begin
  OnFim;
  inherited;
end;

procedure TdmkEditCalc.OnFim;
begin
{
  if FDBLookupComboBox <> nil then
  begin
    if FDBLookupComboBox.ListSource.DataSet.State = dsInactive then
    begin
      FDBLookupComboBox.KeyValue := Null;
      Text := '';
    end else begin
      if FDBLookupComboBox.Text = '' then
      begin
        if FDBLookupComboBox.ListSource <> nil then
        begin
          if not TDataSet(TDataSource(
          FDBLookupComboBox.ListSource).DataSet).Locate(
          FDBLookupComboBox.KeyField, ValueVariant, []) then
          begin
            FDBLookupComboBox.KeyValue := Null;
            Text := '';
          end else FDBLookupComboBox.KeyValue := ValueVariant;
        end;
      end;
    end;
  end;
}
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditCalc]);
end;

end.
