unit dmkCompoStore;

interface

uses
  Windows,  Messages, SysUtils, Classes,  Graphics, Controls,  Forms,
  Dialogs,  DBCtrls,  StdCtrls, dmkEditCB, dmkGeral, Variants, DB,
  ExtCtrls;

type
  TdmkCompoStore = class(TComponent)
  private
    { Private declarations }
    FComponent: TComponent;
    procedure SetComponent(Component: TComponent);
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property Component: TComponent read FComponent write SetComponent;
  end;

procedure Register;

implementation

procedure TdmkCompoStore.SetComponent(Component: TComponent);
begin
  if FComponent <> Component then
  begin
    //FComponent := Unassigned;
    FComponent := Component;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkCompoStore]);
end;

end.
