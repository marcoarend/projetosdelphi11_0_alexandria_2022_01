unit dmkDBEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, dmkGeral, UnDmkEnums;

type
  TdmkDBEdit = class(TDBEdit)
  private
    { Private declarations }
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FAlignment: TAlignment;
    FOldValor: Variant;

    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetAlignment(Value: TAlignment);
    procedure SetOldValor(Value: Variant);
  protected
    { Protected declarations }
    procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property Alignment:TAlignment read FAlignment write SetAlignment;
    property OldValor: Variant read FOldValor write SetOldValor;
  end;

procedure Register;

implementation

constructor TdmkDBEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TdmkDBEdit.SetAlignment(Value: TAlignment);
begin
  if Value <> FAlignment then
  begin
    FAlignment := Value;
    RecreateWnd;
  end;
end;

procedure TdmkDBEdit.CreateParams(var Params: TCreateParams);
const
    Alignments : array[TAlignment] of Longint = (ES_LEFT, ES_RIGHT, ES_CENTER);
begin
  inherited CreateParams(Params);
{$WARNINGS OFF}
  Params.Style := Params.Style or Alignments[FAlignment] or ES_MULTILINE;
{$WARNINGS ON}
end;

procedure TdmkDBEdit.SetOldValor(Value: Variant);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkDBEdit.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkDBEdit.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkDBEdit.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkDBEdit]);
end;

end.
