unit dmkEditCB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkEdit, DBCtrls,  DB, Variants, dmkGeral, UnDmkEnums, dmkCompoEnums;

type
  //TSetValCompo = (setregOnlyManual=0, setregOnFormActivate=1, setregOnCompoCMEnter=2);  transferido para dmkCompoEnums em 2022-07-28
  TdmkEditCB = class(TdmkEdit)
  private
    { Private declarations }
    FOnSincroLoc: TNotifyEvent;
    FOldRedef, FNewRedef: Variant;
    FDBLookupComboBox: TDBLookupComboBox;
    FIgnoraDBLookupComboBox: Boolean;
    FAutoSetIfOnlyOneReg: TSetValCompo;
    procedure SetDBLookupComboBox(Value: TDBLookupComboBox);
    procedure SetIgnoraDBLookupComboBox(Value: Boolean);
    procedure SetAutoSetIfOnlyOneReg(Value: TSetValCompo);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    function  TemDataSet(): Boolean;
  protected
    { Protected declarations }
    procedure Change; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure OnFim();
    // Ini 2018-02-16
    procedure SelIfOnlyOneReg(Forma: TSetValCompo);
    // Fim 2018-02-16
  published
    { Published declarations }
    property DBLookupComboBox: TDBLookupComboBox read FDBLookupComboBox write SetDBLookupComboBox;
    property IgnoraDBLookupComboBox: Boolean read FIgnoraDBLookupComboBox write SetIgnoraDBLookupComboBox;
    property OnSincroLoc: TNotifyEvent read FOnSincroLoc write FOnSincroLoc;
    property AutoSetIfOnlyOneReg: TSetValCompo read FAutoSetIfOnlyOneReg write FAutoSetIfOnlyOneReg;
  end;

procedure Register;

implementation

constructor TdmkEditCB.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  IgnoraDBLookupComboBox := False;
  AutoSetIfOnlyOneReg    := TSetValCompo.setregOnlyManual;
  Width		 := 56;
  Text           := '0';
  FormatType     := dmktfInteger;
  FValMin        := IntToStr(-2147483647);
  ValueVariant   := 0;
  //
  FOldRedef := Null;
  FNewRedef := Null;
end;

destructor TdmkEditCB.Destroy;
begin
  if FDBLookupComboBox <> nil then
    FDBLookupComboBox := nil;
  inherited;
end;

procedure TdmkEditCB.SelIfOnlyOneReg(Forma: TSetValCompo);
begin
  // Ini 2018-02-16
  if AutoSetIfOnlyOneReg in (
  [TSetValCompo.setregOnFormActivate, TSetValCompo.setregOnCompoCMEnter]) then
  begin
    if AutoSetIfOnlyOneReg = Forma then
    begin
      if TemDataSet() then
      begin
        if (Self.ValueVariant = null) or (Self.Text = '')
        or ((Self.ValueVariant = 0) and (FDBLookupComboBox.Text = '')) then
        begin
          if FDBLookupComboBox.ListSource.DataSet.State <> dsInactive then
          begin
            if TDataSet(TDataSource(
                FDBLookupComboBox.ListSource).DataSet).RecordCount = 1 then
            begin
              if FDBLookupComboBox.ListSource <> nil then
              begin
                Self.Text := FDBLookupComboBox.ListSource.DataSet.FieldByName(
                FDBLookupComboBox.KeyField).AsString;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  // Fim 2018-02-16
end;

procedure TdmkEditCB.SetAutoSetIfOnlyOneReg(Value: TSetValCompo);
begin
  if Value <> FAutoSetIfOnlyOneReg then
    FAutoSetIfOnlyOneReg := Value;
end;

procedure TdmkEditCB.SetDBLookupComboBox(Value: TDBLookupComboBox);
begin
  if Value <> FDBLookupComboBox then
  begin
    FDBLookupComboBox := Value;
  end;
end;

procedure TdmkEditCB.SetIgnoraDBLookupComboBox(Value: Boolean);
begin
  if Value <> FIgnoraDBLookupComboBox then
    FIgnoraDBLookupComboBox := Value;
end;

function TdmkEditCB.TemDataSet(): Boolean;
begin
  Result := False;
  //
  if FDBLookupComboBox <> nil then
  begin
    if FDBLookupComboBox.ListSource = nil then
    begin
      Geral.MensagemBox('"FDBLookupComboBox.ListSource" n�o definido'
      + sLineBreak + 'Nome: ' + FDBLookupComboBox.Name,
      'ERRO', MB_OK+MB_ICONERROR, 0);
      Exit;
    end;
    if FDBLookupComboBox.ListSource.DataSet = nil then
    begin
      Geral.MensagemBox('"FDBLookupComboBox.ListSource.DataSet" n�o definido'
      + sLineBreak + 'Nome: ' + FDBLookupComboBox.Name,
      'ERRO', MB_OK+MB_ICONERROR, 0);
      Exit;
    end;
    //
    Result := True;
  end;
end;

procedure TdmkEditCB.WMKeyDown(var Message: TWMKeyDown);
var
  Chave: Integer;
  MyCursor: TCursor;
begin
  Chave := Message.CharCode;
  if Chave = VK_F5 then
  begin
    if TemDataSet() then
    begin
      // N�o reabrir se estiver fechado. Pode estar fechado por algum motivo de seguran�a
      if FDBLookupComboBox.ListSource.DataSet.State <> dsInactive then
      begin
        MyCursor := Screen.Cursor;
        Screen.Cursor := crHourGlass;
        try
          //
          FDBLookupComboBox.ListSource.DataSet.Close;
          FDBLookupComboBox.ListSource.DataSet.Open;
          //
          Text := '0';
        finally
          Screen.Cursor := MyCursor;
        end;
      end;
    end;
  end;
  //
  inherited;
end;

procedure TdmkEditCB.Change;
begin
  if FDBLookupComboBox <> nil then
  begin
    //if KeyValue <> Null then
      if FDBLookupComboBox.KeyValue <> ValueVariant then
      begin
        FDBLookupComboBox.KeyValue := ValueVariant;
        //
        if (FDBLookupComboBox.KeyValue = Null) and
        ((ValueVariant <> Null) and (ValueVariant <> 0)) then
        begin
          case FormatType of
            dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64, dmktfDate,
            dmktfTime, dmktfDateTime:
              ValueVariant := 0;
            else
              ValueVariant := '';
          end;
        end;
        //Mudado em 2017-01-27 => In�cio
        //Mudado em 2017-01-27 => Fim
      end;
  end;
  // Deve ser depois !!!!!
  inherited;
end;

procedure TdmkEditCB.CMEnter(var Message: TCMEnter);
begin
  FOldRedef := Self.ValueVariant;
  // Ini 2018-02-16
  SelIfOnlyOneReg(TSetValCompo.setregOnCompoCMEnter);
  // Fim 2018-02-16
  inherited;
end;

procedure TdmkEditCB.CMExit(var Message: TCMExit);
begin
  OnFim;
  FNewRedef := Self.ValueVariant;
  if FOldRedef <> FNewRedef then
  if Assigned(FOnSincroLoc) then
    FOnSincroLoc(Self);
  inherited;
end;

procedure TdmkEditCB.OnFim;
begin
  if TemDataSet() then
  begin
    if FDBLookupComboBox.ListSource.DataSet.State = dsInactive then
    begin
      FDBLookupComboBox.KeyValue := Null;
      if not FIgnoraDBLookupComboBox then
        Text := '';
    end else
    begin
      if FDBLookupComboBox.Text = '' then
      begin
        if FDBLookupComboBox.ListSource <> nil then
        begin
          if not TDataSet(TDataSource(
          FDBLookupComboBox.ListSource).DataSet).Locate(
          FDBLookupComboBox.KeyField, ValueVariant, []) then
          begin
            FDBLookupComboBox.KeyValue := Null;
            if not FIgnoraDBLookupComboBox then
              Text := '';
          end else FDBLookupComboBox.KeyValue := ValueVariant;
        end;
      end;
    end;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditCB]);
end;

end.
