unit dmkCheckBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkGeral, UnDmkEnums;

type
  TdmkCheckBox = class(TCheckBox)
  private
    { Private declarations }
    FQryName: String;
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FValCheck: Char;
    FValUnCheck: Char;
    FOldValor: Char;

    procedure SetQryName(Value: String);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetValCheck(Value: Char);
    procedure SetValUncheck(Value: Char);
    procedure SetOldValor(Value: Char);
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property QryName: String read FQryName write SetQryName;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property ValCheck: Char read FValCheck write SetValCheck;
    property ValUncheck: Char read FValUncheck write SetValUncheck;
    property OldValor: Char read FOldValor write SetOldValor;
  end;

procedure Register;

implementation

procedure TdmkCheckBox.SetOldValor(Value: Char);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkCheckBox.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkCheckBox.SetQryName(Value: String);
begin
  if Value <> FQryName then
  begin
    FQryName := Value;
  end;
end;

procedure TdmkCheckBox.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkCheckBox.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure TdmkCheckBox.SetValCheck(Value: Char);
begin
  if Value <> FValCheck then
  begin
    FValCheck := Value;
  end;
end;

procedure TdmkCheckBox.SetValUncheck(Value: Char);
begin
  if Value <> FValUncheck then
  begin
    FValUncheck := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkCheckBox]);
end;

end.
