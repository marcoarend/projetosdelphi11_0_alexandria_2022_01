unit dmkEditF7;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, dmkEdit;

type
  TdmkEditF7 = class(TdmkEdit)
  private
    { Private declarations }
    FdmkEditCod: TdmkEdit;
    FLocF7CodiFldName: String;
    FLocF7NameFldName: String;
    FLocF7TableName: String;
    procedure SetdmkEditCod(Value: TdmkEdit);
    procedure SetLocF7CodiFldName(Value: String);
    procedure SetLocF7NameFldName(Value: String);
    procedure SetLocF7TableName(Value: String);
  protected
    { Protected declarations }
  public
    { Public declarations }
    destructor Destroy; override;
  published
    { Published declarations }
    property dmkEditCod: TdmkEdit read FdmkEditCod write SetdmkEditCod;
    property LocF7CodiFldName: String read FLocF7CodiFldName write SetLocF7CodiFldName;
    property LocF7NameFldName: String read FLocF7NameFldName write SetLocF7NameFldName;
    property LocF7TableName: String read FLocF7TableName write SetLocF7TableName;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditF7]);
end;

{ TdmkEditF7 }

destructor TdmkEditF7.Destroy;
begin
  if FdmkEditCod <> nil then
    FdmkEditCod := nil;
  FdmkEditCod.Free;
  inherited;
end;

procedure TdmkEditF7.SetdmkEditCod(Value: TdmkEdit);
begin
  if Value <> FdmkEditCod then
  begin
    FdmkEditCod := Value;
  end;
end;

procedure TdmkEditF7.SetLocF7CodiFldName(Value: String);
begin
  if Value <> FLocF7CodiFldName then
  begin
    FLocF7CodiFldName := Value;
  end;
end;

procedure TdmkEditF7.SetLocF7NameFldName(Value: String);
begin
  if Value <> FLocF7NameFldName then
  begin
    FLocF7NameFldName := Value;
  end;
end;

procedure TdmkEditF7.SetLocF7TableName(Value: String);
begin
  if Value <> FLocF7TableName then
  begin
    FLocF7TableName := Value;
  end;
end;

end.
