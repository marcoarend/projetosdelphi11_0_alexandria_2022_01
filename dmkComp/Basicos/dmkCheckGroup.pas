unit dmkCheckGroup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dmkGeral, math,
  DB, DBCtrls, UnDmkEnums;
type

  TCustomCheckGroup = class(TCustomGroupBox)
  private
    FButtons: TList;
    FItems: TStrings;
    FItemIndex: Integer;
    FColumns: Integer;
    FReading: Boolean;
    FUpdating: Boolean;
    procedure ArrangeButtons;
    procedure ButtonClick(Sender: TObject);
    procedure ButtonDblClick(Sender: TObject);
    procedure ItemsChange(Sender: TObject);
    procedure SetButtonCount(Value: Integer);
    procedure SetColumns(Value: Integer);
    procedure SetItemIndex(Value: Integer);
    procedure SetItems(Value: TStrings);
    procedure UpdateButtons;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    function GetCheckedStatus(Index:Integer):Boolean;
    procedure SetCheckedStatus(Index:Integer;Value:Boolean);
    function GetSelCount:integer;

  protected
    procedure ReadState(Reader: TReader); override;
    function CanModify: Boolean; virtual;
    //procedure GetChildren(Proc: TGetChildProc); override;
    property Columns: Integer read FColumns write SetColumns default 1;
    property ItemIndex: Integer read FItemIndex write SetItemIndex default -1;
    property Items: TStrings read FItems write SetItems;
  public
    property Checked[Index: Integer]: Boolean read GetCheckedStatus write SetCheckedStatus;
    property SelCount:Integer read  GetSelCount;
    function MaxValue: Integer;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TdmkCheckGroup = class(TCustomCheckGroup)
  private
    FQryCampo: String;
    FUpdCampo: String;
    FUpdType: TUpdType;
    FOldValor: Integer;

    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetUpdType(Value: TUpdType);
    procedure SetValor(Value: Integer{Extended});
    procedure SetOldValor(Value: Integer);
    function GetValor(): Integer{Extended};
    //function GetCheckedStatus(Index: Integer): Integer;
  published
    property Align;
    property Caption;
    property Color;
    property Columns;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property ItemIndex;
    property Items;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnStartDrag;
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property UpdType: TUpdType read FUpdType write SetUpdType;
    property Value: Integer{Extended} read GetValor write SetValor;
    property OldValor: Integer read FOldValor write SetOldValor;
    //
    procedure SetMaxValue;
    function GetIndexOfCheckedStatus(Index: Integer): Integer;
    function GetValueOfCheckedStatus(Index: Integer): Integer;
    function GetIndexesChecked(VerificaZero: Boolean; Incremento:
             Integer): String;

  end;

  TdmkDBCheckGroup = class(TCustomCheckGroup)
  private
    FDataLink: TFieldDataLink;
    FValue: string;
    //FValues: TStrings;
    FInSetValue: Boolean;
    FOnChange: TNotifyEvent;
    procedure DataChange(Sender: TObject);
    procedure UpdateData(Sender: TObject);
    function GetDataField: WideString;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    function GetButtonValue(Index: Integer): string;
    procedure SetDataField(const Value: WideString);
    procedure SetDataSource(Value: TDataSource);
    procedure SetReadOnly(Value: Boolean);
    procedure SetValue(const Value: string);
    procedure SetItems(Value: TStrings);
    //procedure SetValues(Value: TStrings);
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure CMGetDataLink(var Message: TMessage); message CM_GETDATALINK;
    //  Marco
    procedure SetValor(Value: Integer);
  protected
    procedure Change; dynamic;
    procedure Click; override;
    procedure DblClick; override;
    procedure KeyPress(var Key: Char); override;
    function CanModify: Boolean; override;
    procedure Notification(AComponent: TComponent;
      Operation: TOperation); override;
    property DataLink: TFieldDataLink read FDataLink;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function ExecuteAction(Action: TBasicAction): Boolean; override;
    function UpdateAction(Action: TBasicAction): Boolean; override;
    function UseRightToLeftAlignment: Boolean; override;
    property Field: TField read GetField;
    property ItemIndex;
    property Value: string read FValue write SetValue;
  published
    property Align;
    property Anchors;
    property BiDiMode;
    property Caption;
    property Color;
    property Columns;
    property Constraints;
    property Ctl3D;
    property DataField: WideString read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property Items write SetItems;
    property ParentBackground;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ShowHint;
    property TabOrder;
    property TabStop;
    //property Values: TStrings read FValues write SetValues;
    property Visible;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnClick;
    property OnDblClick;
    property OnContextPopup;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnStartDock;
    property OnStartDrag;
  end;

procedure Register;


implementation

uses Consts;

{ TGroupButton }

type
{$WARNINGS OFF}
  TGroupButton = class(TCheckBox)
  private
	 FInClick: Boolean;
	 procedure CNCommand(var Message: TWMCommand); message CN_COMMAND;
  protected
	 procedure ChangeScale(M, D: Integer); override;
	 procedure KeyDown(var Key: Word; Shift: TShiftState); override;
	 procedure KeyPress(var Key: Char); override;
  public
	 constructor Create(CheckGroup: TCustomCheckGroup); //override;
	 destructor Destroy; override;
  end;
{$WARNINGS ON}

constructor TGroupButton.Create(CheckGroup: TCustomCheckGroup);
begin
  inherited Create(CheckGroup);
  CheckGroup.FButtons.Add(Self);
  Visible := False;
  Enabled := CheckGroup.Enabled;
  ParentShowHint := False;
  OnClick := CheckGroup.ButtonClick;
  OnDblClick := CheckGroup.ButtonDblClick;
  Parent := CheckGroup;
end;

destructor TGroupButton.Destroy;
begin
  TCustomCheckGroup(Owner).FButtons.Remove(Self);
  inherited Destroy;
end;

procedure TGroupButton.CNCommand(var Message: TWMCommand);
begin
  if not FInClick then
  begin
    FInClick := True;
    try
      if ((Message.NotifyCode = BN_CLICKED) or
        (Message.NotifyCode = BN_DOUBLECLICKED)) and
		 TCustomCheckGroup(Parent).CanModify then
        inherited;
    except
      Application.HandleException(Self);
    end;
    FInClick := False;
  end;
end;

procedure TGroupButton.ChangeScale(M, D: Integer);
begin
end;

procedure TGroupButton.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  TCustomCheckGroup(Parent).KeyPress(Key);
  if (Key = #8) or (Key = ' ') then
  begin
    if not TCustomCheckGroup(Parent).CanModify then Key := #0;
  end;
end;

procedure TGroupButton.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  TCustomCheckGroup(Parent).KeyDown(Key, Shift);
end;

  function (*TUnMLAGeral.*)IntInConjunto(*2*)(Numero, Conjunto: Integer): Boolean;
  //var
    //i: Integer;
  begin
    Result := Conjunto and Numero > 0;
    {Result := False;
    if Numero = 1 then
    begin
      if Int(Conjunto / 2) <> (Conjunto / 2) then
      Result := True;
    end else begin
      Conjunto := (Trunc(Conjunto / 2)) * 2;
      while Conjunto > Numero do
      begin
        i := 2;
        while i < Conjunto do i := i * 2;
        if i > Conjunto then i := i div 2;
        Conjunto := Conjunto - i;
        if i = Numero then
        begin
          Result := True;
          Break;
        end;
      end;
      if Conjunto = Numero then
        Result := True;
    end;
    }
  end;

{ TCustomCheckGroup }

constructor TCustomCheckGroup.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := [csSetCaption, csDoubleClicks];
  FButtons := TList.Create;
  FItems := TStringList.Create;
  TStringList(FItems).OnChange := ItemsChange;
  FItemIndex := -1;
  FColumns := 1;
end;

destructor TCustomCheckGroup.Destroy;
begin
  SetButtonCount(0);
  TStringList(FItems).OnChange := nil;
  FItems.Free;
  FButtons.Free;
  inherited Destroy;
end;

procedure TCustomCheckGroup.ArrangeButtons;
var
  ButtonsPerCol, ButtonWidth, ButtonHeight, TopMargin, I: Integer;
  DC: HDC;
  SaveFont: HFont;
  Metrics: TTextMetric;
begin
  if (FButtons.Count <> 0) and not FReading then
  begin
    DC := GetDC(0);
    SaveFont := SelectObject(DC, Font.Handle);
    GetTextMetrics(DC, Metrics);
	 SelectObject(DC, SaveFont);
    ReleaseDC(0, DC);
    ButtonsPerCol := (FButtons.Count + FColumns - 1) div FColumns;
    ButtonWidth := (Width - 10) div FColumns;
    I := Height - Metrics.tmHeight - 5;
    ButtonHeight := I div ButtonsPerCol;
    TopMargin := Metrics.tmHeight + 1 + (I mod ButtonsPerCol) div 2;
	 for I := 0 to FButtons.Count - 1 do
      with TGroupButton(FButtons[I]) do
	   begin
        SetBounds((I div ButtonsPerCol) * ButtonWidth + 8,
          (I mod ButtonsPerCol) * ButtonHeight + TopMargin,
		   ButtonWidth, ButtonHeight);
        Visible := True;
	   end;
  end;
end;

procedure TCustomCheckGroup.ButtonClick(Sender: TObject);
begin
  if not FUpdating then
  begin
	 FItemIndex := FButtons.IndexOf(Sender);
    Click;
  end;
end;

procedure TCustomCheckGroup.ButtonDblClick(Sender: TObject);
begin
  if not FUpdating then
  begin
	 FItemIndex := FButtons.IndexOf(Sender);
    DblClick;
  end;
end;

procedure TCustomCheckGroup.ItemsChange(Sender: TObject);
begin
  if not FReading then
  begin
    if FItemIndex >= FItems.Count then FItemIndex := FItems.Count - 1;
    UpdateButtons;
  end;
end;

function TCustomCheckGroup.MaxValue(): Integer;
begin
  Result := Trunc(power(2, FButtons.Count))-1;
end;

procedure TCustomCheckGroup.ReadState(Reader: TReader);
begin
  FReading := True;
  inherited ReadState(Reader);
  FReading := False;
  UpdateButtons;
end;

procedure TCustomCheckGroup.SetButtonCount(Value: Integer);
begin
  while FButtons.Count < Value do TGroupButton.Create(Self);
  while FButtons.Count > Value do TGroupButton(FButtons.Last).Free;
end;

procedure TCustomCheckGroup.SetColumns(Value: Integer);
begin
  if Value < 1 then Value := 1;
  if Value > 16 then Value := 16;
  if FColumns <> Value then
  begin
	 FColumns := Value;                 ArrangeButtons;
  end;
end;

procedure TCustomCheckGroup.SetItemIndex(Value: Integer);
begin
  if FReading then FItemIndex := Value
  else
  begin
	 if Value < -1 then Value := -1;
	 if Value >= FButtons.Count then Value := FButtons.Count - 1;
	 if Value >= 0 then TGroupButton(FButtons[Value]).Checked := True;
	 FItemIndex:=Value;
  end;
end;


Function TCustomCheckGroup.GetCheckedStatus(Index:Integer):Boolean;
begin
	GetCheckedStatus := TGroupButton(FButtons[Index]).Checked;
end;

Procedure TCustomCheckGroup.SetCheckedStatus(Index:Integer;Value:Boolean);
begin
	TGroupButton(FButtons[Index]).Checked:=Value;
end;

 Function TCustomCheckGroup.GetSelCount:integer;
var
	ix:integer;
	sum:integer;
begin
	sum:=0;
	for ix:= 0 to FButtons.Count-1 do
		if TGroupButton(FButtons[ix]).Checked then sum:=sum + 1;
	GetSelCount:=sum;
end;


procedure TCustomCheckGroup.SetItems(Value: TStrings);
begin
  FItems.Assign(Value);
end;

procedure TCustomCheckGroup.UpdateButtons;
var
  I: Integer;
begin
  SetButtonCount(FItems.Count);
  for I := 0 to FButtons.Count - 1 do
	 TGroupButton(FButtons[I]).Caption := FItems[I];
  if FItemIndex >= 0 then
  begin
	 FUpdating := True;
	 TGroupButton(FButtons[FItemIndex]).Checked := True;
    FUpdating := False;
  end;
  ArrangeButtons;
end;

procedure TCustomCheckGroup.CMEnabledChanged(var Message: TMessage);
var
  I: Integer;
begin
  inherited;
  for I := 0 to FButtons.Count - 1 do
	 TGroupButton(FButtons[I]).Enabled := Enabled;
end;

procedure TCustomCheckGroup.CMFontChanged(var Message: TMessage);
begin
  inherited;
  ArrangeButtons;
end;

procedure TCustomCheckGroup.WMSize(var Message: TWMSize);
begin
  inherited;
  ArrangeButtons;
end;

function TCustomCheckGroup.CanModify: Boolean;
begin
  Result := True;
end;

{
procedure TCustomCheckGroup.GetChildren(Proc: TGetChildProc);
begin
end;
}

{ TdmkCheckGroup }

procedure TdmkCheckGroup.SetMaxValue;
var
  Valor: Integer;
begin
  Valor := Trunc(Power(2, Self.Items.Count) - 1);
  if Valor <> Self.Value then
    Self.Value := Valor;
end;

procedure TdmkCheckGroup.SetOldValor(Value: Integer);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkCheckGroup.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkCheckGroup.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure TdmkCheckGroup.SetUpdType(Value: TUpdType);
begin
  if Value <> FUpdType then
  begin
    FUpdType := Value;
  end;
end;

procedure TdmkCheckGroup.SetValor(Value: Integer{Extended});
var
  Valor, i: Integer{Extended};
begin
  if Value >= power(2, FButtons.Count) then
    Valor := Trunc(power(2, FButtons.Count-1))
  else
    if Value < 0 then Valor := 0
  else
    Valor := Value;
  //
  if Valor <> GetValor then
  begin
    for i:= 0 to FButtons.Count-1 do
      TGroupButton(FButtons[i]).Checked :=
        IntInConjunto(Trunc(power(2, i)), Valor);
  end;
end;

function TdmkCheckGroup.GetIndexesChecked(VerificaZero: Boolean; Incremento:
Integer): String;
var
  I, K: integer;
begin
  Result := '';
  if VerificaZero then
    K := 0
  else
    K := 1;
  for I := K to Self.Items.Count - 1 do
  begin
    if Self.Checked[I] then
      Result := Result + FormatFloat('0', I + Incremento) + ',';
  end;
  if Result <> '' then
    Result := Copy(Result, 1, Length(Result)-1);
end;

function TdmkCheckGroup.GetIndexOfCheckedStatus(Index: Integer): Integer;
begin
	if TGroupButton(FButtons[Index]).Checked then
    Result := Index
  else
    Result := -1;
end;

function TdmkCheckGroup.GetValor(): Integer{Extended};
var
  Valor, k: Integer{Extended};
	i: Integer;
begin
	Valor := 0;
	for i:= 0 to FButtons.Count-1 do
		if TGroupButton(FButtons[i]).Checked then
    begin
      k := Trunc(power(2, i));
      Valor := Valor + k;
    end;
	GetValor := Valor;
end;

function TdmkCheckGroup.GetValueOfCheckedStatus(Index: Integer): Integer;
begin
	if TGroupButton(FButtons[Index]).Checked then
    Result := Trunc(power(2, Index))
  else
    Result := 0;
end;

{ TdmkDBCheckGroup }

constructor TdmkDBCheckGroup.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataLink := TFieldDataLink.Create;
  FDataLink.Control := Self;
  FDataLink.OnDataChange := DataChange;
  FDataLink.OnUpdateData := UpdateData;
  //FValues := TStringList.Create;
end;

destructor TdmkDBCheckGroup.Destroy;
begin
  FDataLink.Free;
  FDataLink := nil;
  //FValues.Free;
  inherited Destroy;
end;

procedure TdmkDBCheckGroup.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FDataLink <> nil) and
    (AComponent = DataSource) then DataSource := nil;
end;

function TdmkDBCheckGroup.UseRightToLeftAlignment: Boolean;
begin
  Result := inherited UseRightToLeftAlignment;
end;

procedure TdmkDBCheckGroup.DataChange(Sender: TObject);
begin
  if FDataLink.Field <> nil then
    Value := FDataLink.Field.Text else
    Value := '';
  if Value = '' then
    SetValor(0)
  else
    SetValor(StrToInt(Value));
end;

procedure TdmkDBCheckGroup.UpdateData(Sender: TObject);
begin
  if FDataLink.Field <> nil then FDataLink.Field.Text := Value;
end;

function TdmkDBCheckGroup.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TdmkDBCheckGroup.SetDataSource(Value: TDataSource);
begin
  FDataLink.DataSource := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

function TdmkDBCheckGroup.GetDataField: WideString;
begin
  Result := FDataLink.FieldName;
end;

procedure TdmkDBCheckGroup.SetDataField(const Value: WideString);
begin
  FDataLink.FieldName := Value;
end;

function TdmkDBCheckGroup.GetReadOnly: Boolean;
begin
  Result := FDataLink.ReadOnly;
end;

procedure TdmkDBCheckGroup.SetReadOnly(Value: Boolean);
begin
  FDataLink.ReadOnly := Value;
end;

function TdmkDBCheckGroup.GetField: TField;
begin
  Result := FDataLink.Field;
end;

function TdmkDBCheckGroup.GetButtonValue(Index: Integer): string;
begin
  {
  if (Index < FValues.Count) and (FValues[Index] <> '') then
    Result := FValues[Index]
  else if Index < Items.Count then
    Result := Items[Index]
  else
    Result := '';
  }
end;

procedure TdmkDBCheckGroup.SetValue(const Value: string);
var
  WasFocused: Boolean;
  I, Index: Integer;
begin
  if FValue <> Value then
  begin
    FInSetValue := True;
    try
      WasFocused := (ItemIndex > -1) and (TGroupButton(FButtons[ItemIndex]).Focused);
      Index := -1;
      for I := 0 to Items.Count - 1 do
        if Value = GetButtonValue(I) then
        begin
          Index := I;
          Break;
        end;
      ItemIndex := Index;
      // Move the focus rect along with the selected index
      if WasFocused and (ItemIndex <> -1) then
        TGroupButton(FButtons[ItemIndex]).SetFocus;
    finally
      FInSetValue := False;
    end;
    FValue := Value;
    Change;
  end;
end;

procedure TdmkDBCheckGroup.CMExit(var Message: TCMExit);
begin
  try
    FDataLink.UpdateRecord;
  except
    if ItemIndex >= 0 then
      TRadioButton(Controls[ItemIndex]).SetFocus else
      TRadioButton(Controls[0]).SetFocus;
    raise;
  end;
  inherited;
end;

procedure TdmkDBCheckGroup.CMGetDataLink(var Message: TMessage);
begin
  Message.Result := Integer(FDataLink);
end;

procedure TdmkDBCheckGroup.Click;
begin
  if not FInSetValue then
  begin
    inherited Click;
    if ItemIndex >= 0 then Value := GetButtonValue(ItemIndex);
    if FDataLink.Editing then FDataLink.Modified;
  end;
end;

procedure TdmkDBCheckGroup.DblClick;
begin
  if not FInSetValue then
  begin
    inherited DblClick;
    if ItemIndex >= 0 then Value := GetButtonValue(ItemIndex);
    if FDataLink.Editing then FDataLink.Modified;
  end;
end;

procedure TdmkDBCheckGroup.SetItems(Value: TStrings);
begin
  Items.Assign(Value);
  DataChange(Self);
end;

{
procedure TdmkDBCheckGroup.SetValues(Value: TStrings);
begin
  FValues.Assign(Value);
  DataChange(Self);
end;
}

procedure TdmkDBCheckGroup.Change;
begin
  if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TdmkDBCheckGroup.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  case Key of
    #8, ' ': FDataLink.Edit;
    #27: FDataLink.Reset;
  end;
end;

function TdmkDBCheckGroup.CanModify: Boolean;
begin
  Result := FDataLink.Edit;
end;

function TdmkDBCheckGroup.ExecuteAction(Action: TBasicAction): Boolean;
begin
  Result := inherited ExecuteAction(Action) or (DataLink <> nil) and
    DataLink.ExecuteAction(Action);
end;

function TdmkDBCheckGroup.UpdateAction(Action: TBasicAction): Boolean;
begin
  Result := inherited UpdateAction(Action) or (DataLink <> nil) and
    DataLink.UpdateAction(Action);
end;

procedure TdmkDBCheckGroup.SetValor(Value: Integer{Extended});
var
  Valor, i: Integer{Extended};
  k: Boolean;
begin
  if Value >= power(2, FButtons.Count) then
    Valor := Trunc(power(2, FButtons.Count-1))
  else
    if Value < 0 then
      Valor := 0
  else
    Valor := Value;
  //
  //if Valor <> GetValor then
 // begin
    for i:= 0 to FButtons.Count-1 do
    begin
      k := IntInConjunto(Trunc(power(2, i)), Valor);
      TGroupButton(FButtons[i]).Checked := k;
    end;
    // Evitar erro no primeiro checkbox
    // se n�o colocar o c�digo a seguir
    // coloca como true mesmo que que seje false!!!
    if FButtons.Count > 0 then
    begin
      k := IntInConjunto(Trunc(power(2, 0)), Valor);
      TGroupButton(FButtons[0]).Checked := k;
    end;
  //end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkCheckGroup, TdmkDBCheckGroup]);
end;

end.
