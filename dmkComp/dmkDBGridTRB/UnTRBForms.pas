unit UnTRBForms;

interface

uses  Vcl.Forms, Vcl.StdCtrls,  Winapi.Messages, Vcl.Controls, System.Types,
  System.Classes, System.SysUtils, Winapi.Windows, Vcl.ExtCtrls, Menus,
  dmkGeral, UnDmkEnums, dmkEditDateTimePicker, dmkEdit, dmkCheckGroup;

//const

type
  TUnTRBForms = class(TObject)
  private
    procedure ShowFormIntervealedIntegersClick(Sender: TObject);
    { Private declarations }
    //procedure KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  public
    { Public declarations }
    function  ShowFormDateRange(var DHStart, DHEnd: TDateTime; var
              FilterMascSet: Int32; var SortOrder: TdmkSQLSortOrder): Boolean;
    function  ShowFormIntegerRange(var IntMin, IntMax: Int64): Boolean;
    function  ShowFormIntegerInterleaved(var Interleaved: String): Boolean;
  end;
var
  TRBForms: TUnTRBForms;

implementation

{ TUnTRBForms }

function TUnTRBForms.ShowFormDateRange(var DHStart, DHEnd: TDateTime; var
  FilterMascSet: Int32; var SortOrder: TdmkSQLSortOrder): Boolean;
const
  DTWidth = 112;
  TPWIdth =  56;
  CompoGap =  8;
var
  FmRTC: TForm;
  LaIni, LaEnd: TLabel;
  //edt: TEdit;
  BtOK: TButton;
  TPIni, TPFim: TdmkEditDateTimePicker;
  EdIni, EdFim: TdmkEdit;
  CGMsk: TdmkCheckGroup;
  RGOrd: TRadioGroup;
  //
  SumTop, SumLeft, GapCol: Integer;
begin
  Result := False;
  //
  SumTop := 0;
  SumLeft := 0;
  GapCol := DTWidth + TPWIdth + CompoGap;

  FmRTC := TForm.Create(nil);
  try
    LaIni := TLabel.Create(FmRTC);
    LaEnd := TLabel.Create(FmRTC);
    //edt := TEdit.Create(FmRTC);
    TPIni := TdmkEditDateTimePicker.Create(FmRTC);
    EdIni := TdmkEdit.Create(FmRTC);
    TPFim := TdmkEditDateTimePicker.Create(FmRTC);
    EdFim := TdmkEdit.Create(FmRTC);
    BtOK := TButton.Create(FmRTC);
    CGMsk := TdmkCheckGroup.Create(FmRTC);
    RGOrd := TRadioGroup.Create(FmRTC);

    FmRTC.BorderStyle := bsDialog;
    FmRTC.Caption := 'Welcome';
    FmRTC.Width := 300;
    FmRTC.Position := poScreenCenter;

    SumTop := SumTop + CompoGap;
    SumLeft := SumLeft + CompoGap;
    LaIni.Parent := FmRTC;
    LaIni.Top := SumTop;
    LaIni.Left := SumLeft;
    LaIni.Caption := 'Data / hora inicial';

    LaEnd.Parent := FmRTC;
    LaEnd.Top := SumTop;
    LaEnd.Left := SumLeft + GapCol;
    LaEnd.Caption := 'Data / hora final';

    SumTop := SumTop + LaIni.Height + CompoGap;

    TPIni.Parent := FmRTC;
    TPIni.Top := SumTop;
    TPIni.Left := SumLeft;
    TPIni.Width := DTWidth;
    TPIni.Date := Int(Date - 30);

    EdIni.Parent := FmRTC;
    EdIni.Top := SumTop;
    EdIni.Left := SumLeft + DTWidth;
    EdIni.Width := TPWIdth;
    EdIni.Text := '00:00:00';
    EdIni.FormatType := TAllFormat.dmktfTime;
    EdIni.HoraFormat := TdmkHoraFmt.dmkhfLong;
    EdIni.ValueVariant := 0;

    TPFim.Parent := FmRTC;
    TPFim.Top := SumTop;
    TPFim.Left := SumLeft + GapCol;
    TPFim.Width := DTWidth;
    TPFim.Date := Int(Date);

    EdFim.Parent := FmRTC;
    EdFim.Top := SumTop;
    EdFim.Left := SumLeft + GapCol + DTWidth;
    EdFim.Width := TPWIdth;
    EdFim.Text := '23:59:59';
    EdFim.FormatType := TAllFormat.dmktfTime;
    EdFim.HoraFormat := TdmkHoraFmt.dmkhfLong;
    EdFim.ValueVariant := 1 - (1/ (60*60*24)); // 23:59:59

    SumTop := SumTop + TPIni.Height + CompoGap;

    RGOrd.Parent := FmRTC;
    RGOrd.Top := SumTop;
    RGOrd.Left := CompoGap;
    RGOrd.Width := GapCol + DTWidth;
    RGOrd.Height := EdIni.Height * 2;
    RGOrd.Columns := 2;
    RGOrd.Caption := ' Ordena��o: ';
    RGOrd.Items.Add('Ascendente');
    RGOrd.Items.Add('Descendente');

    SumTop := SumTop + RGOrd.Height + CompoGap;

    CGMsk.Parent := FmRTC;
    CGMsk.Top := SumTop;
    CGMsk.Left := CompoGap;
    CGMsk.Width := GapCol + DTWidth;
    CGMsk.Height := EdIni.Height * 3;
    CGMsk.Columns := 3;
    CGMsk.Caption := ' M�scara de filtro: ';
    CGMsk.Items.Add('Pr�');
    CGMsk.Items.Add('Entre');
    CGMsk.Items.Add('P�s');
    CGMsk.Value := 7;

    SumTop := SumTop + CGMsk.Height + CompoGap;

    SumLeft := SumLeft + CompoGap + ((DTWidth + TPWIdth) * 2) + CompoGap;

    BtOK.Parent := FmRTC;
    BtOK.Caption := 'OK';
    BtOK.Default := True;
    BtOK.ModalResult := mrOk;
    BtOK.Top := SumTop;
    BtOK.Left := (SumLeft - BtOK.Width) div 2;

    FmRTC.ClientHeight := BtOK.Top + BtOK.Height + CompoGap;
    FmRTC.ClientWidth := SumLeft;

    if FmRTC.ShowModal = mrOk then
    begin
      Result := True;
      DHStart := Int(TPIni.Date) + EdIni.ValueVariant;
      DHEnd := Int(TPFim.Date) + EdFim.ValueVariant;
      FilterMascSet := CGMsk.Value;
      SortOrder := TdmkSQLSortOrder(RGOrd.ItemIndex);
      Geral.MB_Info(
      'Data / hora inicial: ' + Geral.FDT(DHStart, 109) + sLineBreak +
      'Hora / hora final: ' + Geral.FDT(DHEnd, 109) + sLineBreak +
      '');
    end;
  finally
    FmRTC.Free;
  end;
end;

function TUnTRBForms.ShowFormIntegerInterleaved(var Interleaved: String): Boolean;
const
  EdWidth  =  300;
  CompoGap =    4;
  Separador = ',';
var
  FmRTC: TForm;
  LaAll, LaInf: TLabel;
  BtOK: TButton;
  EdAll: TdmkEdit;
  //
  I, SumTop, SumLeft, GapCol: Integer;
  Lista: TStringList;
  Str: String;
  OK: Boolean;
begin
  Result := False;
  //
  SumTop := 0;
  SumLeft := 0;
  GapCol := EdWidth + EdWidth + CompoGap;

  FmRTC := TForm.Create(nil);
  try
    LaAll := TLabel.Create(FmRTC);
    LaInf := TLabel.Create(FmRTC);
    EdAll := TdmkEdit.Create(FmRTC);
    BtOK := TButton.Create(FmRTC);

    FmRTC.BorderStyle := bsDialog;
    FmRTC.Caption := 'N�meros salteados / Intervalos';
    FmRTC.Width := 300;
    FmRTC.Position := poScreenCenter;

    SumTop := SumTop + CompoGap;
    SumLeft := SumLeft + CompoGap;
    LaAll.Parent := FmRTC;
    LaAll.Top := SumTop;
    LaAll.Left := SumLeft;
    LaAll.Caption := 'N�meros:';


    SumTop := SumTop + LaAll.Height + CompoGap;

    EdAll.Parent := FmRTC;
    EdAll.Top := SumTop;
    EdAll.Left := SumLeft;
    EdAll.Width := EdWidth;
    EdAll.Text := '';
    EdAll.FormatType := TAllFormat.dmktfString;
    EdAll.ValueVariant := Interleaved;

    SumTop := SumTop + EdAll.Height + CompoGap;

    SumLeft := SumLeft + CompoGap;
    LaInf.Parent := FmRTC;
    LaInf.Top := SumTop;
    LaInf.Left := SumLeft;
    LaInf.Caption := 'Exemplo: 2, 3, 5>10';

    SumTop := SumTop + EdAll.Height + CompoGap;

    SumLeft := SumLeft + CompoGap + EdWidth + CompoGap;

    BtOK.Parent := FmRTC;
    BtOK.Caption := 'OK';
    BtOK.Default := True;
    BtOK.ModalResult := mrOk;
    BtOK.Top := SumTop;
    BtOK.Left := (SumLeft - BtOK.Width) div 2;

    FmRTC.ClientHeight := BtOK.Top + BtOK.Height + CompoGap + 20;
    FmRTC.ClientWidth := SumLeft;

    if FmRTC.ShowModal = mrOk then
    begin
      Str := EdAll.Text;
      Lista := TStringList.Create;
      try
        Lista := Geral.IsInterleavedText(Str, OK);
        if OK then
        begin
          Interleaved := Str;
          Result := True;
        end else
        begin
          Result := False;
          Geral.MB_Aviso('O texto "' + Str + '" n�o � v�lido para filtro de intervalo(s) [1]');
        end;
      finally
        Lista.Free;
      end;
    end;
  finally
    FmRTC.Free;
  end;
end;

function TUnTRBForms.ShowFormIntegerRange(var IntMin, IntMax: Int64): Boolean;
const
  EdWidth  =  100;
  CompoGap =    4;
var
  FmRTC: TForm;
  LaMin, LaMax: TLabel;
  BtOK: TButton;
  EdMin, EdMax: TdmkEdit;
  //
  SumTop, SumLeft, GapCol: Integer;
begin
  Result := False;
  //
  SumTop := 0;
  SumLeft := 0;
  GapCol := EdWidth + EdWidth + CompoGap;

  FmRTC := TForm.Create(nil);
  try
    LaMin := TLabel.Create(FmRTC);
    LaMax := TLabel.Create(FmRTC);
    EdMin := TdmkEdit.Create(FmRTC);
    EdMax := TdmkEdit.Create(FmRTC);
    BtOK := TButton.Create(FmRTC);

    FmRTC.BorderStyle := bsDialog;
    FmRTC.Caption := 'Intervalo de N�meros';
    FmRTC.Width := 300;
    FmRTC.Position := poScreenCenter;

    SumTop := SumTop + CompoGap;
    SumLeft := SumLeft + CompoGap;
    LaMin.Parent := FmRTC;
    LaMin.Top := SumTop;
    LaMin.Left := SumLeft;
    LaMin.Caption := 'N�mero inicial:';

    LaMax.Parent := FmRTC;
    LaMax.Top := SumTop;
    LaMax.Left := SumLeft + CompoGap + EdWidth;
    LaMax.Caption := 'N�mero final:';

    SumTop := SumTop + LaMin.Height + CompoGap;

    EdMin.Parent := FmRTC;
    EdMin.Top := SumTop;
    EdMin.Left := SumLeft;
    EdMin.Width := EdWidth;
    EdMin.Text := '0';
    EdMin.FormatType := TAllFormat.dmktfInt64;
    EdMin.ValueVariant := IntMin;

    EdMax.Parent := FmRTC;
    EdMax.Top := SumTop;
    EdMax.Left := SumLeft + CompoGap + EdWidth;
    EdMax.Width := EdWidth;
    EdMax.Text := '0';
    EdMax.FormatType := TAllFormat.dmktfInt64;
    EdMax.ValueVariant := IntMax;

    SumTop := SumTop + EdMin.Height + CompoGap;

    SumLeft := SumLeft + CompoGap + ((EdWidth + EdWidth) * 2) + CompoGap;

    BtOK.Parent := FmRTC;
    BtOK.Caption := 'OK';
    BtOK.Default := True;
    BtOK.ModalResult := mrOk;
    BtOK.Top := SumTop;
    BtOK.Left := (SumLeft - BtOK.Width) div 2;

    FmRTC.ClientHeight := BtOK.Top + BtOK.Height + CompoGap + 20;
    FmRTC.ClientWidth := SumLeft;

    if FmRTC.ShowModal = mrOk then
    begin
      Result := True;
      IntMin := EdMin.ValueVariant;
      IntMax := EdMax.ValueVariant;
      if IntMin > IntMax then
      begin
        IntMin := EdMax.ValueVariant;
        IntMax := EdMin.ValueVariant;
      end;
    end;
  finally
    FmRTC.Free;
  end;
end;

procedure TUnTRBForms.ShowFormIntervealedIntegersClick(Sender: TObject);
begin
  Geral.MB_Info('Teste TUnTRBForms.ShowFormIntervealedIntegersClick()');
end;

{
procedure TUnTRBForms.KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    Key := VK_TAB
  else
end;
}

end.
