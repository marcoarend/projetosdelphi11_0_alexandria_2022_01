unit UnTRB_PF;

interface

uses  Vcl.Forms, Vcl.StdCtrls,  Winapi.Messages, Vcl.Controls, System.Types,
  System.Classes, System.SysUtils, Winapi.Windows, Vcl.ExtCtrls, Menus,
  dmkGeral, UnDmkEnums, dmkEditDateTimePicker, dmkEdit, dmkCheckGroup,
  dmkDBGridTRB, UnTRBForms;

//const

type
  MyCustomTRBDBGrid = class(TCustomTRBDBGrid);
  //
  TUnTRB_PF = class(TObject)
  private
    { Private declarations }
    procedure IntRangeClick(Sender: TObject);
    procedure IntInterleavedClick(Sender: TObject);
    procedure ClearAllSearch(Sender: TObject);
  public
    { Public declarations }
    function  ShowFormIntegers(Owner: TComponent; X, Y: Integer): Boolean;
    function  ShowMasterPopup(Owner: TComponent; X, Y: integer): Boolean;
  end;
var
  TRB_PF: TUnTRB_PF;

implementation

{ TUnTRB_PF }

function TUnTRB_PF.ShowFormIntegers(Owner: TComponent; X, Y: Integer): Boolean;
var
  PopupMenu: TPopupMenu;
  MenuItem: TMenuItem;
begin
  Result := False;
  //FIncNames := FIncNames + 1;
  PopupMenu := TPopupMenu.Create(Owner);
  //PopupMenu.Name := 'PMItens' + IntToStr(FIncNames);
  //PopupMenu.OnPopup := PMItensPopup;
  //
  //FIncNames := FIncNames + 1;
  MenuItem := TMenuItem.Create(Owner);
  //MenuItem.Name := 'Intervalo' + IntToStr(FIncNames);
  MenuItem.Caption := '&Intervalo de n�meros';
  MenuItem.OnClick := IntRangeClick;
  PopupMenu.Items.Add(MenuItem);
  //
  //FIncNames := FIncNames + 1;
  MenuItem := TMenuItem.Create(Owner);
  //MenuItem.Name := 'ConhecidosIntercalados' + IntToStr(FIncNames);
  MenuItem.Caption := '&Conhecidos / Intercalados';
  MenuItem.OnClick := IntInterleavedClick;
  PopupMenu.Items.Add(MenuItem);
  //
  //Geral.MB_Info(Owner.Name);

  PopupMenu.Popup(X, Y);
end;

function TUnTRB_PF.ShowMasterPopup(Owner: TComponent; X, Y: integer): Boolean;
var
  PopupMenu: TPopupMenu;
  MenuItem: TMenuItem;
begin
  Result := False;
  //FIncNames := FIncNames + 1;
  PopupMenu := TPopupMenu.Create(Owner);
  //PopupMenu.Name := 'PMItens' + IntToStr(FIncNames);
  //PopupMenu.OnPopup := PMItensPopup;
  //
  //FIncNames := FIncNames + 1;
  MenuItem := TMenuItem.Create(Owner);
  //MenuItem.Name := 'Intervalo' + IntToStr(FIncNames);
  MenuItem.Caption := '&Limpar toda pesquisa';
  MenuItem.OnClick := ClearAllSearch;
  PopupMenu.Items.Add(MenuItem);
  //

  PopupMenu.Popup(X, Y);
end;

procedure TUnTRB_PF.ClearAllSearch(Sender: TObject);
var
  TRB: TdmkDBGridTRB;
begin
  TRB := TdmkDBGridTRB(TComponent(Sender).Owner);
  TRB.ClearAllSearch();
  Sender.Free;
end;

procedure TUnTRB_PF.IntInterleavedClick(Sender: TObject);
var
  TRB: TdmkDBGridTRB;
begin
  TRB := TdmkDBGridTRB(TComponent(Sender).Owner);
  TRB.FilterByIntInterleaved();
  Sender.Free;
end;

procedure TUnTRB_PF.IntRangeClick(Sender: TObject);
var
  TRB: TdmkDBGridTRB;
begin
  TRB := TdmkDBGridTRB(TComponent(Sender).Owner);
  TRB.FilterByIntRange();
  Sender.Free;
end;

end.
