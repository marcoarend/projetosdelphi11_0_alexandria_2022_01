unit dmkEditUM;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.DBCtrls,
  WinApi.Windows, WinApi.Messages,
  UnDmkEnums, dmkEdit, dmkEditCB, dmkDBLookupComboBox;

type
  TdmkEditUM = class(TdmkEdit)
  private
    { Private declarations }
    FEditCbUM: TdmkEditCB;
    FDBLookupCbUM: TdmkDBLookupComboBox;
    procedure SetEditCbUM(Value: TdmkEditCb);
    procedure SetDBLookupCbUM(Value: TdmkDBLookupComboBox);
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  protected
    { Protected declarations }
    procedure Change; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property EditCbUM: TdmkEditCB read FEditCbUM write SetEditCbUM;
    property DBLookupCbUM: TdmkDBLookupComboBox read FDBLookupCbUM write SetDBLookupCbUM;
  end;

procedure Register;

implementation

{$IfNDef CompilingCompo}
uses
  Module, ModProd, UnMySQLCuringa;
{$EndIf}

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditUM]);
end;

{ TdmkEditUM }

procedure TdmkEditUM.Change;
begin
  inherited;
  {$IfNDef CompilingCompo}
  if (FEditCbUM <> nil) and (FDBLookupCbUM <> nil) then
  begin
  if Self.Focused then
    DmProd.PesquisaPorSigla(False, Self, FEditCbUM, FDBLookupCbUM);
  end;
  {$EndIf}
  // Deve ser depois ????
  //inherited;
end;

procedure TdmkEditUM.CMExit(var Message: TCMExit);
begin
  inherited;
  {$IfNDef CompilingCompo}
  if (FEditCbUM <> nil) and (FDBLookupCbUM <> nil) then
  begin
    DmProd.PesquisaPorSigla(True, Self, FEditCbUM, FDBLookupCbUM);
  end;
  {$EndIf}
  // Deve ser depois ????
  //inherited;
end;

constructor TdmkEditUM.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  Width             := 40;
  Texto             := '';
end;

procedure TdmkEditUM.SetDBLookupCbUM(Value: TdmkDBLookupComboBox);
begin
  if Value <> FDBLookupCbUM then
  begin
    FDBLookupCbUM := Value;
  end;
end;

procedure TdmkEditUM.SetEditCbUM(Value: TdmkEditCb);
begin
  if Value <> FEditCbUM then
  begin
    FEditCbUM := Value;
  end;
end;

procedure TdmkEditUM.WMKeyDown(var Message: TWMKeyDown);
begin
  inherited;
  {$IfNDef CompilingCompo}
  if Message.CharCode = VK_F3 then
  begin
    if (FEditCbUM <> nil) and (FDBLookupCbUM <> nil) then
    begin
      CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
      ''(*Extra*), FEditCbUM, FDBLookupCbUM, dmktfInteger);
    end;
  end;
  {$EndIf}
end;

end.
