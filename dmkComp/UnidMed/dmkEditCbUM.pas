unit dmkEditCbUM;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.DBCtrls,
  WinApi.Windows, WinApi.Messages, UnDmkEnums,
  dmkEditCB, dmkEditUM, DmkDBLookupComboBox;

type
  TdmkEditCbUM = class(TdmkEditCB)
  private
    { Private declarations }
    FEditUM: TdmkEditUM;
    procedure SetEditUM(const Value: TdmkEditUM);
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  protected
    { Protected declarations }
    procedure Change; override;
  public
    { Public declarations }
  published
    { Published declarations }
    property EditUM: TdmkEditUM read FEditUM write SetEditUM;
  end;

procedure Register;

implementation

{$IfNDef CompilingCompo}
uses
  Module, ModProd, UnMySQLCuringa;
{$EndIf}

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkEditCbUM]);
end;

{ TdmkEditCbUM }

procedure TdmkEditCbUM.Change;
begin
  inherited;
  {$IfNDef CompilingCompo}
  if FEditUM <> nil then
  begin
    if not FEditUM.Focused then
      DmProd.PesquisaPorCodigo(Self.ValueVariant, FEditUM);
  end;
  {$EndIf}
  // Deve ser depois ????
  //inherited;
end;

procedure TdmkEditCbUM.SetEditUM(const Value: TdmkEditUM);
begin
  if Value <> FEditUM then
  begin
    FEditUM := Value;
  end;
end;

procedure TdmkEditCbUM.WMKeyDown(var Message: TWMKeyDown);
begin
  inherited;
  {$IfNDef CompilingCompo}
  if Message.CharCode = VK_F3 then
  begin
    if (*(FEditCbUM <> nil) and*) (Self.DBLookupComboBox <> nil) then
    begin
      CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
      ''(*Extra*), self, TDmkDBLookupComboBox(Self.DBLookupComboBox), dmktfInteger);
    end;
  end;
  {$EndIf}
end;

end.
