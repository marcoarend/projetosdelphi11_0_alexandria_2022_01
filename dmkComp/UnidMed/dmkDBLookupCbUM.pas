unit dmkDBLookupCbUM;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.DBCtrls,
  WinApi.Windows, WinApi.Messages,
  UnDmkEnums, dmkDBLookupComboBox, dmkEditUM;

type
  TdmkDBLookupCbUM = class(TdmkDBLookupComboBox)
  private
    { Private declarations }
    FEditUM: TdmkEditUM;
    procedure SetEditUM(const Value: TdmkEditUM);
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property EditUM: TdmkEditUM read FEditUM write SetEditUM;
  end;

procedure Register;

implementation

{$IfNDef CompilingCompo}
uses
  Module, ModProd, UnMySQLCuringa;
{$EndIf}

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkDBLookupCbUM]);
end;

{ TdmkDBLookupCbUM }

procedure TdmkDBLookupCbUM.SetEditUM(const Value: TdmkEditUM);
begin
  if Value <> FEditUM then
  begin
    FEditUM := Value;
  end;
end;

procedure TdmkDBLookupCbUM.WMKeyDown(var Message: TWMKeyDown);
begin
  {$IfNDef CompilingCompo}
  if Message.CharCode = VK_F3 then
  begin
    if (Self.dmkEditCB <> nil) (*and (FDBLookupCbUM <> nil)*) then
    begin
      CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
      ''(*Extra*), Self.dmkEditCB, Self, dmktfInteger);
    end;
  end;
  {$EndIf}
end;

end.
