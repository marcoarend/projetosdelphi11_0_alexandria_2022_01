unit UnDmkEnums;

interface

uses  System.SysUtils, System.Types {$IfDef MSWINDOWS}, Winapi.Windows {$EndIf};

const
  CO_ShowKindList_0_RealIndx = 0;
  CO_ShowKindList_1_ShowKind = 1;
  CO_ShowKindList_2_ToolKind = 2;
  //
  //
  MaxPixelCount = 65536;

type
  // LocF7
  TdmkF7PreDefProc = (f7pNone=0, f7pEntidades=1, f7pPrdTamCor=2);
  //Temporario!!!!!!!!!!
  TConvGrandeza = (cgMtoFT, cgFTtoM, cgKGtoLB, cgLBtoKG);
  // Aplicativos
  // B U G S T R O L
  TGraBugsOpera = (gboNenhum, gboServi, gboCaixa);
  TGraBugsServi = (gbsIndef, gbsAplEMon, gbsAplica, gbsMonitora, gbsMonMulti);
  TGraBugsMater = (gbmIndef, gbmEquiEProd, gbmEquipam, gbmProduto);
  TNivelRatifConsumoOS = (nrcosNenhum=0, nrcosProduto=1, nrcosFormula=2,
                         nrcosServico=3, nrcosLocalizador=4);
  // FIM  B U G S T R O L

  // B L U E   D E R M
  TImpDesnate = (impdsnDenate, impdsnDenSubUni, impdsnDenSubAll );
  TTipoCalcCouro = (ptcNaoInfo=0, ptcPecas=1, ptcPesoKg=2, ptcAreaM2=3,
                    ptcAreaP2=4, ptcTotal=5);
  TEstqMovimID = (emidAjuste=0, emidCompra=1, emidVenda=2,
                 emidReclasWE=3, emidBaixa=4, emidIndsWE=5, emidIndsVS=6,
                 emidClassArtVSUni=7, emidReclasVSUni=8, emidForcado=9,
                 emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
                 emidInventario=13, emidClassArtVSMul=14, emidPreReclasse=15,
                 emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,
                 emidEmProcWE=19, emidFinished=20, emidDevolucao=21,
                 emidRetrabalho=22, emidGeraSubProd=23, emidReclasVSMul=24,
                 emidTransfLoc=25, emidEmProcCal=26, emidEmProcCur=27);
//AdicionarNovosVS_emid(): Boolean;
  TEstqMovimNiv = (eminSemNiv=0, eminSorcClass=1, eminDestClass=2,
                   eminSorcIndsWB=3, eminDestIndsWB=4, eminSorcReclass=5,
                   eminDestReclass=6,

                   eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10,

                   eminSorcPreReclas=11, eminDestPreReclas=12,

                   eminDestCurtiVS=13, eminSorcCurtiVS=14, eminBaixCurtiVS=15,

                   eminSdoArtInNat=16, eminSdoArtGerado=17,
                   eminSdoArtClassif=18, eminSdoArtEmOper=19,

                   eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23,

                   eminSdoArtEmWEnd=24, eminSdoFinish=25, eminSdoSubPrd=26,
                   eminSorcLocal=27, eminDestLocal=28,

                   eminSorcCal=29, eminEmCalInn=30, eminDestCal=31, eminEmCalBxa=32,
                   eminSdoArtEmCal=33,

                   eminSorcCur=34, eminEmCurInn=35, eminDestCur=36, eminEmCurBxa=37,
                   eminSdoArtEmCur=38

                   );
  TEstqMovimType = (emitIndef=0, emitIME_I=1, emitIME_C=2, emitIME_P=3,
                    emitPallet=4, emitAvulsos=5, emitAgrupTotalizador=6,
                    emitAgrupItensNew=7, emitAgrupItensBxa=8);
  TEstqSetorID = (esiIndef=0, esiCaleiro=1, esiCurtimento=2, esiRecurtimento=3,
                  esiPreAcab=4, esiAcabamento=5, esiETE=6, esiCaldeira=7,
                  esiGraxaria=8, esiOutros=9);
  TEstqDefMulFldVS = (edmfIndef=0, edmfSrcNiv2=1, edmfMovCod=2, edmfIMEI=3);
  TVSBastidao = (vsbstdND=0, vsbstdIntegral=1, vsbstdLaminado=2,
                 vsbstdDivTripa=3, vsbstdDivCurti=4, vsbstdRebaixad=5,
                 vsbstdDivSemiA=6, vsbstdRebxSemi=7);
  TVSRedefnFrmt = (vsrdfnfrmtNaoInfo=0, vsrdfnfrmtInaltera=1,
                   vsrdfnfrmtDivRacha=2, vsrdfnfrmtParteLados=3,
                   vsrdfnfrmtParteCabCul=4, vsrdfnfrmtCabCulBari=5,
                   vsrdfnfrmtGrupona=6, vsrdfnfrmtRefila=7);
  TVSLnkIDXtr = (lixIndefinido=0, lixPedVenda=1);
  TVSStatPall = (vsspIndefinido=0, vsspMontando=1, vsspDesmontando=2,
                 vsspMontEDesmo=3, vsspEncerrado=4, vsspEncerMont=5,
                 vsspEncerDesmo=6, vsspEncerMontEDesmo=7, vsspRemovido=8);
  TEstqEditGB = (eegbNone=0, eegbQtdOriginal=1, eegbDadosArtigo=2);
  TEstqMotivDel = (emtdWetCurti013=1, emtdWetCurti045=2, emtdWetCurti104=3,
                   emtdWetCurti008A=4, emtdWetCurti008B=5, emtdWetCurti079=6,
                   emtdWetCurti022=7, emtdWetCurti028=8, emtdWetCurti038=9,
                   emtdWetCurti093=10, emtdWetCurti078=11, emtdWetCurti006=12,
                   emtdWetCurti019=13, emtdWetCurti023=14, emtdWetCurti027=15,
                   emtdWetCurti060=16, emtdWetCurti076=17, emtdWetCurti106=18,
                   emtdWetCurti111=19, emtdWetCurti131=20, emtdWetCurti136=21,
                   emtdWetCurti142=22, emtdWetCurti145=23);
  TEstqSubProd = (esubprdOriginal=0, esubprdDerivado=1);
  TEstqNivCtrl = (encPecas=0, encArea=1, encValor=2);
  TPQxTipo = (pqxtipoInventario=0, pqxtipoEntrada=10, pqxtipoPesagem=110,
              pqxtipoETE=150, pqxtipoOutros=190);
  TPQEHowLoad = (pqehlPQE=0, pqehlPQRAjuInn=1, pqehlEntradaCab=2);
  TBxaEstqVS = (bevsIndef=0, bevsNao=1, bevsSim=2);
  TCalcExec = (calcexecIndef=0, calcexec1=1, Calcexec2=2, calcexecPecasPorRendArea=3);
  // FIM  B L U E   D E R M

  // T O O L   R E N T
  TPrmRefQuem = (prqIndefinido=0, prqCliente=1, prqAgente=2);
  TPrmFatorValor = (pfvIndefinido=0, pfvValorTotal=1, pfvValorUnitario=2);
  TPrmFormaRateio = (pfrIndefinido=0, pfrValorTotal=1, pfrValorPorPessoa=2);
  TSrvLCtbDsp = (slcdUnknownToInf=0, slcdNoGerCtaAPag=1, slcdDescoFaturam=2,
                 slcdGeraCtaAPagr=3);
  // FIM  T O O L   R E N T

  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  CT-e
  TTipoConsumoWS_CTe = (tcwscteStatusServico=0, tcwscteRecepcao=1,
               tcwscteRetRecepcao=2, tcwscteEveCancelamento=3,
               tcwsctePediInutilizacao=4, tcwscteConsultaCTe=5,
               tcwscteEveCartaDeCorrecao=6, tcwscteConsultaCadastro=7,
               tcwscteEveEPEC=8);
  TCTeTpEmis = (ctetpemis0=0, ctetpemisNormal=1, ctetpemis2=2, ctetpemis3=3,
               ctetpemisEPEC_SVC=4, ctetpemisContingenciaFSDA=5, ctetpemis6=6,
               ctetpemisSVC_RS=7, ctetpemisSVC_SP=8);
  TCTeTpVeic = (ctetpveicTracao=0, ctetpveicReboque=1);
  TCTeTpRod = (ctetprodNaoAplicavel=0, ctetprodTruck=1, ctetprodToco=2,
               ctetprodCavaloMec=3, ctetprodVAN=4, ctetprodUtilitario=5,
               ctetprodOutros=6);
  TCTeTpCar = (ctetpcarNaoAplicavel=0, ctetpcarAberta=1, ctetpcarFrchadaBau=2,
               ctetpcarGranelera=3, ctetpcarPortaContainer=4, ctetpcarSider=5);
  TCTeTpCTe = (ctetpcteNormal=0, ctetpcteComplValrs=1, ctetpcteAnulacao=2,
               ctetpcteSubstituto=3);
  TCTeMyCST = (ctemycstIndefinido=0, ctemycstCST00Normal=1, ctemycstCST20BCRed=2,
               ctemycstCST40Isensao=3, ctemycstCST41NaoTrib=4, ctemycstCST51Diferido=5,
               ctemycstCST60ST=6, ctemycstCST90Outros=7, ctemycstCST90OutraUF=8,
               ctemycstCSTSimplesNacional=9);
  TCTeForPag = (cteforpagPago=0, cteforpagAPagar=1, cteforpagOutros=2);
  TCTeModal = (ctemodalIndefinido=0, ctemodalRodoviario=1, ctemodalAereo=2,
                ctemodalAquaviario=3, ctemodalFerroviario=4,
                ctemodalDutoviario=5, ctemodalMultimodal=6);
  TCTeTpServ = (ctetpservNormal=0, ctetpservSubcontratacao=1, ctetpservRedespacho=2,
                ctetpservRedespInterm=3, ctetpservServVincMultimodal=4);
  TCTeToma = (ctetomaRemetente=0, ctetomaExpedidor=1, ctetomaRecebedor=2,
              ctetomaDestinatario=3, ctetomaOutros=4);
  TCTeCUnid = (ctecunidM3=0, ctecunidKG=1, ctecunidTON=2, ctecunidUNIDADE=3,
               ctecunidLITROS=4, ctecunidMMBTU=5);
  TCTeMyStatus = (ctemystatusDesencerrada=8, ctemystatusEncerrada=9,
                  ctemystatusCTeDados=10, ctemystatusCTeGerada=20,
                  ctemystatusCTeAssinada=30, ctemystatusLoteRejeitado=40,
                  ctemystatusCTeAddedLote=50, ctemystatusLoteEnvEnviado=60,
                  ctemystatusLoteEnvConsulta=70, ctemystatusEveEPEC_OK=90);
  TCTeMyTpDocInf = (ctemtdiIndef=0, ctemtdiNFe=1, ctemtdiNFsOld=2,
               ctemtdiOutrosDoc=3);
  TCTeServicoStep = (ctesrvStatusServico=0, (*ctesrvEnvioAssincronoLoteCTe*)ctesrvEnvioLoteCTe=1,
                  ctesrvConsultarLoteCTeEnviado=2, ctesrvPedirCancelamentoCTe=3,
                  ctesrvPedirInutilizacaoNumerosCTe=4, ctesrvConsultarCTe=5,
                  ctesrvEventoCartaCorrecao=6, ctesrvConsultaCadastro=7,
                  ctesrvEPEC=8 (*,
                  ctesrvConsultaCTesDestinadas=9, ctesrvDownloadCTes=10,
                  ctesrvConsultaDistribuicaoDFeInteresse=11*));
  TCTeRodoLota =(cterodolotaNao=0, cterodolotaSim=1);
  TCTePropTpProp =(cteptpTAC_Agregado=0, cteptpTAC_Independente=1, cteptpTAC_Outros=2);
  TCTeRespSeg = (ctersRemetente=0, ctersExpedidor=1, ctersRecebedor=2,
                 ctersDestinatario=3, ctersEmitenteDoCTe=4, ctersTomadorDoServico=5);
  TEventosXXe = (evexxCCe,  // Carta de corre��o (NF-e, CT-e)
                 evexxCan,  // Cancelamento (NFe, CT-e, MDF-e)
                 evexxEnc,   // Encerramento (MDF-e)
                 evexxIdC    // Inclusao Condutor (MDF-e)
                 (*eveMDe*)); // Manifesta��o do deestinat�rio
  ///  FIM CT-e


  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  MDF-e
  TTipoConsumoWS_MDFe = (tcwsmdfeStatusServico=0, tcwsmdfeRecepcao=1, tcwsmdfeRetRecepcao=2,
                   tcwsmdfeEveCancelamento=3, tcwsmdfePediInutilizacao=4,
                   tcwsmdfeConsultaMDFe=5, tcwsmdfeEveIncCondutor=6, tcwsmdfe7=7, tcwsmdfeEveEncerramento=8,
                   tcwsmdfeConsultaNaoEncerrados=9 );
  TMDFeTipoEmitente = (mdfetpemitIndef=0, mdfetpemitPrestadorServico=1, mdfetpemiCargaPropria=2);
  TMDFeModal = (mdfemodalIndefinido=0, mdfemodalRodoviario=1, mdfemodalAereo=2,
                mdfemodalAquaviario=3, mdfemodalFerroviario=4);
  TMDFeMyStatus = (mdfemystatusDesencerrada=8, mdfemystatusEncerrada=9,
                  mdfemystatusMDFeDados=10, mdfemystatusMDFeGerada=20,
                  mdfemystatusMDFeAssinada=30, mdfemystatusLoteRejeitado=40,
                  mdfemystatusMDFeAddedLote=50, mdfemystatusLoteEnvEnviado=60,
                  mdfemystatusLoteEnvConsulta=70);
  TMDFeServicoStep = (mdfesrvStatusServico=0, (*mdfesrvEnvioAssincronoLoteMDFe*)mdfesrvEnvioLoteMDFe=1,
                  mdfesrvConsultarLoteMDFeEnviado=2, mdfesrvEventoCancelamento=3,
                  (*mdfesrvPedirInutilizacaoNumerosMDFe=4,*) mdfesrvConsultarMDFe=5,
                  mdfesrvEventoIncCondutor=6, (*mdfesrvConsultaCadastroEntidade=7,*)
                  mdfesrvEventoEncerramento=8 (*inoperante???*)(*,
                  mdfesrvConsultaNaoEncerrados=9(*, mdfesrvDownloadMDFes=10,
                  mdfesrvConsultaDistribuicaoDFeInteresse=11*));
  TMDFeCargaCUnid = (mdfeccuIndef=0, mdfeccuKG=1, mdfeccuTON=2);

  ///  FIM MDF-e


  // TODOS
  TTipoXXe = (tipoxxeNFe=55, tipoxxeCTe=57, tipoxxeMDFe=58);
  TEventoXXe = (evexxe110110CCe=0(*110110*), evexxe110111Can=1(*110111*),
                evexxe110112Enc=2(*110112*), evexxe110113EPEC=3(*110113*),
                evexxe110114IncCondutor=4(*110114*), evexxe110140EPEC=5(*110140*),
                evexxe110160RegMultimodal=6(*110160*),
                evexxe240130AutCTeComplem=7(*240130*),
                evexxe240131CanCTeComplem=8(*240131*),
                evexxe240140CTeSubst=9(*240140*),
                evexxe240150CTeAnul=10(*240150*),
                evexxe240160CTeMultimodal=11(*240150*),
                evexxe310620RegPassManu=12(*310620*),
                evexxe510620RegPassBRId=13(*510620*)
                );
  TTipoNoXML = (tnxAttrStr, tnxTextStr);
  TStatusOSExec = (soseNaoDefinido=0, soseEmSer=1, soseExecutada=2,
    soseCancelada=3);
  TdmkAppID = (dmkappB_L_U_E_D_E_R_M=2, dmkappS_Y_N_D_I_C=4,  dmkappB_U_G_S_T_R_O_L=24);
  // SPED
  TSPED_EFD_K220 = (sek220Indef=0, sek220ImportadoNormal=1, sek220LancadoManual=2);
  TSPED_EFD_Bal = (sebalIndef=0, sebalStqMovItsX=1, sebalEfdIcmsIpiH010=2,
                   sebalPQx=3, sebalVSMovItX=4, sebalEFD_K200=5);
  TGrandezaUnidMed = (gumPeca=0, gumAreaM2=1, gumPesoKG=2, gumVolumeM3=3,
                      gumLinearM=4, gumOutros=5, gumAreaP2=6, gumPesoTon=7);
  TSPED_IndProp_IndEst = (spedipiePropInfPoderInf=0, spedipiePropInfPoderTer=1,
                          spedipiePropTerPoderInf=2, spedipieIndefinido=3);
  // Fim SPED
  // FIM Aplicativos
  {$IfDef MSWINDOWS}
  PRGBTripleArray2 = ^TRGBTripleArray;
  TRGBTripleArray2 = array[0..32767] of TRGBTriple;
  pRGBTripleArray = ^TRGBTripleArray;
  TRGBTripleArray = ARRAY[0..MaxPixelCount-1] OF TRGBTriple;
  {$EndIf}
  //TDmkTypeDiary = (dtdiarNone=0, dtdiarSAC=1, dtdiarSimple=2);
  TDmkModuloApp = (mdlappNenhum=0, mdlappAppMain=1,mdlappAllBase=2,
                   mdlappEntidades=3, mdlappEmpresas=4, mdlappAgenda=5,
                   mdlappAnotacoes=6, mdlappBancos=7, mdlappBloquetos=8,
                   mdlappCNAB=9, mdlappContratos=10, mdlappCRO=11,
                   mdlappDiario=12, mdlappEmail=13, mdlappEstoque=14,
                   mdlappFavoritos=15, mdlappFinanceiro=16, mdlappFPMin=17,
                   mdlappFPMax=18, mdlappFTP=19, mdlappGrade=20, mdlappNFe=21,
                   mdlappNFSe=22, mdlappPerfis=23, mdlappProtocolos=24,
                   mdlappSAC=25, mdlappSAF=26, mdlappSPED=27, mdlappWEB=28,
                   mdlappWTextos=29, mdlappCuns=30, mdlappBina=31,
                   mdlappConciBco=32, mdlappGFat=33, mdlappGPed=34);
  TArrayListaEstatus = array of array[0..5] of Integer;
  TDataTipo = (dtMySQL, dtSystem, dtSystem2, dtSystem3, dtTexto, dtDelphi);
  TDatePurpose = (dmkdpNone=0, dmkdpGeneric=1, dmkdpSPED_EFD_MIN=2,
                  dmkdpSPED_EFD_MAX=3, dmkdpSPED_EFD_MIN_MAX=4, dmkdpFinance=5,
                  dmkdpInsumMovimMin=6, dmkdpInsumMovimMax=7,
                  dmkdpInsumMovimMinMax=8);
  TfrxImpComo = (ficMostra, ficImprime, ficSalva, ficExporta, ficNone);
  TFormCadEnti = (fmcadSelecionar, fmcadEntidades, fmcadEntidade2);
  TDmkDBMSs = (dbmsUndef=0, dbmsMySQL=1, dbmsSQLite=2, dbmsIBLite=3);
  TDmkDBSrc = (dbsrcLocalServer=0, dbsrcLocalUser=1, dbsrcWebServer=2, dbsrcWebUser=3);
  dmkTbCtrl = (tctrlCad=0, tctrlAlt=1, tctrlLok=2, (*tctrlDel, *)tctrlWeb=3, tctrlAti=4);
  TAcaoCriaTabela   = (actCreate=0, actDescribe=1);
  TResultVerify = (rvOK=0, rvErr=1, rvAbort=2);
  TMyMotivo = (mymotDifere=0, mymotSemRef=1);
  TSQLType    = (stIns=0, stUpd=1, stDel=2, stCpy=3, stLok=4, stUnd=5, stPsq=6, stNil=7);
  TUsoXMLQuery = (uxqNoSQL=0, uxqSQLite=1, uxqMySQL=2);
  TAcessFmModo = (afmoSoMaster=0, afmoSoAdmin=1, afmoSoBoss=2, afmoNegarSemAviso=3,
                  afmoNegarComAviso=4, afmoLiberado=5, afmoSemVerificar=6);
  TSinal = (siPositivo=0, siNegativo=1);
  TTipoSinal = (tsPos=0, tsNeg=1, tsDef=2);
  TLogActnID = (laiNone=0, laiOSWCab=1, laiOSWPipMon=2, laiOSPipIts=3);
  TLogAcaoExe = (laeNone=0, laeDownload=1, laeUpload=2, laeSelected=3,
                 laeInsert=4, laeUpdated=5, laeDeleted=6, laeFinished=7);
  TFormaCalcPercent = (fcpercDesconto, fcpercAcrescimo, fcpercPrecoTax);
  TWBRclass = (wbrEntrada, wbrEstoque);
  TEnvioCNAB = (ecnabIndefinido, ecnabRemessa, ecnabRetorno, ecnabBloquet);
  TFormaWBIndsWE = (fiwNone, fiwPesagem, fiwBaixaPrevia);
  TRelAbrange = (relNada, relAbrangeEmpresa, relAbrangeCliente);
  TValCelRecXLS = (vcrxNenhum=0, vcrxSetor=1, vcrxCliente=2, vcrxTecnico=3,
    vcrxAccount=4, vcrxArtigo=5, vcrxEspessura=6, vcrxRendiment=7,
    vcrxRelatA=8, vcrxPesoCouro=9, vcrxCambio=10, vcrxQuantia=11, vcrxData=12,
    vcrxRebaixe=13);
  TFileEncodingType = (fetAnsi=0, fetUnicode=1, fetUnicodeBigEndian=2, fetUTF_8=3);
  TDateEncodeType = (detJustSum=0, detLastDaySameMonth=1, detFirstDayNextMonth=2);
  TDmkDBLookup = (ddluNone=0, ddluMyDB, ddluMyPID_DB, ddluAll_DB);
  TAllFormat  = (dmktfNone, dmktfString, dmktfDouble, dmktfInteger, dmktfLongint,
                 dmktfInt64, dmktfDate, dmktfTime, dmktfDateTime, dmktfMesAno,
                 dmktf_AAAAMM, dmktf_AAAA_MM,
                 // Deve ser o �ltimo:
                 dmktfUnknown);
  TdmkDataFmt = (dmkdfShort, dmkdfLong);
  TdmkHoraFmt = (dmkhfShort=0, dmkhfLong=1, dmkhfMiliSeconds=2, dmkhfExtended=3,
                 dmkhfTZD_UTC=4);
  TDmkSpidAct = (dsaUnkn=0, dsaFrst=1, dsaPrio=2, dsaThis=3, dsaNext=4,
                dsaLast=5, dsaPesq=6, dsaDesc=7, dsaConf=8, dsaCanc=9,
                dsaHome=10, dsaPlus=11, dsaEdit=12, dsaKill=13, dsaImpr=14,
                dsaCfgs=15, dsaClse=16, dsaOlho=17);
  TKndFmDmk = (kfdNoDef=0, kfdNoXtra=1, kfdCadTypA=2);
  TDmkTabKnd = (dtkMainForm=0, dtkOther=1, dtkNone=2);
  TUpdType    = (utYes=0, utNil=1, utIdx=2, utInc=3);
  TMskType    = (fmtNone, fmtCPFJ, fmtCEP, fmtTelCurto, fmtTelLongo, fmtNCM,
                 fmtCBO2002, fmtIE, fmtNumRua, fmtUF, fmtVersao, fmtCPFJ_NFe,
                 fmtCEP_NFe, fmtTel_NFe, fmtIE_NFe, fmtChaveNFe);
  TComoFmtChNFe = (cfcnNone, cfcnDANFE, cfcnFrendly);
  TDmkCanEdit = (dceOnInsUpd=0, dceNever=1, dceAllways=2);
  TNovoCodigo = (ncIdefinido=0, ncControle=1, CtrlGeral=2, ncGerlSeq1=3);
  TDmkEditStyle = (desEdit=0, desClearingEdit=1, desTimeEdit=2, desComboEdit=2,
                desPasswordEdit=3);
  TReceitaSetor = (recsetrNenhum, recsetrRibeira, recsetrRecurtimento, recsetrAcabamento);
  TAquemPag  = (apFornece=0, apCliente=1, apTranspo=2, apNinguem=3);
  TMinMax    = (mmNenhum=0, mmMinimo=1, mmMaximo=2, mmAmbos=3);
  TAppVerNivel = (avnAlfa=0, avnBeta=1, avnEstavel=2);
  TAppVerRevert = (avrNo=0, avrYes=1);
  // // 0 - N�o informado
     // 1 - EntiContat
     // 2 - Entidades
     // 3 - Avulso
  TClasseEnti = (classeentiEmpresa, classeentiCliente, classeentiTransporta);
  TNFeCTide_TipoDoc = (NFeTipoDoc_Desconhecido,
                       NFeTipoDoc_nfe_0, NFeTipoDoc_nfe_1,
                       NFeTipoDoc_enviNFe, NFeTipoDoc_nfeProc,
                       NFeTipoDoc_cancNFe, NFeTipoDoc_retCancNFe,
                       NFeTipoDoc_procCancNFe, NFeTipoDoc_inutNFe,
                       NFeTipoDoc_retInutNFe, NFeTipoDoc_procInutNFe);
  TNFeCodType = (nfeCTide_TipoDoc,  nfeCTide_tpNF,  nfeCTide_indPag,
                 nfeCTide_procEmi, nfeCTide_finNFe, nfeCTide_tpAmb,
                 nfeCTide_tpEmis, nfeCTide_tpImp,  nfeCTModFrete,
                 nfeCTide_indCont,
                 nfeCTide_idDest, nfeCTide_indFinal, nfeCTide_indPres,
                 nfeCTdest_indIEDest, nfeCTmotDesICMS);
  TNFeServicoStep = (nfesrvStatusServico=0, (*nfesrvEnvioAssincronoLoteNFe*)nfesrvEnvioLoteNFe=1,
                  nfesrvConsultarLoteNfeEnviado=2, nfesrvPedirCancelamentoNFe=3,
                  nfesrvPedirInutilizaCaoNumerosNFe=4, nfesrvConsultarNFe=5,
                  nfesrvEnviarLoteEventosNFe=6, nfesrvConsultaCadastroEntidade=7,
                  nfesrvConsultaSitua��oNFE=8 (*inoperante*),
                  nfesrvConsultaNFesDestinadas=9, nfesrvDownloadNFes=10,
                  nfesrvConsultaDistribuicaoDFeInteresse=11);
  TNFeDFeSchemas = (nfedfeschUnknown=0, nfedfeschResNFe=1, nfedfeschProcNFe=2,
                  nfedfeschResEvento=3, nfedfeschProcEvento=4);
  TXXeIndSinc = (nisAssincrono=0, nisSincrono=1);
  TNFeautXML = (naxNaoInfo=0, naxEntiContat=1, naxEntidades=2, naxAvulso=3);
  TNFEAgruRelNFs = (nfearnIndef=0, nfearnCOFP=1, nfearnNCM=2, nfenfearnNatOP=3);
  TIBPTaxTabs = (ibptaxtabNCM=0, ibptaxtabNBS=1, ibptaxtabLC116=2);
  TIBPTaxOrig = (ibptaxoriNaoInfo=0, ibptaxoriNacional=1, ibptaxoriImportado=2);
  TIBPTaxFont = (ibptaxfontNaoInfo=0, ibptaxfontIBPTax=1, ibptaxfontCalculoProprio=2);
  TTpCalcTribNFe = (tctnfeIndefinido=0, tctnfeAutomatico=1, tctnfeManual=2,
                    tctnfeMixto=3);
  TNFeIndFinal = (indfinalNormal=0, indfinalConsumidorFinal=1);
  TindFinalTribNFe = (infFinTribIndefinido=0, infFinTribSoConsumidorFinal=1,
                      infFinTribTodos=2);
  TcSitNFe = (csitnfeDesconhecido=0, csitnfeAutorizado=1, csitnfeDenegado=2);
  TdmkCambios = (cambiosBRL=1, cambiosUSD=2, cambiosEUR=3, cambiosIDX=4);
  TNFeEventos = (nfeeveCCe=110110, nfeeveCan=110111,
                 nfeeveMDeConfirmacao=210200, nfeeveMDeCiencia=210210,
                 nfeeveMDeDesconhece=210220, nfeeveMDeNaoRealizou=210240,
                 nfeeveEPEC=110140);

(*  Desabilitado por causa do multiOS do XE5!
  //TKeyType    = (ktString, ktBoolean, ktInteger, ktCurrency, ktDate, ktTime);
*)

  TFormaCopiaInterfaceCompo = (fcicBitBlt, fcicPaintTo);
  TFormaFlipReverseBmp = (ffrbScanLine, ffrbCopyRect, ffrbStretchBlt);
  TUsaQualPredef = (uqpIndefinido, uqpADefinir, uqpEspecifico, uqpGeral);

  // TdmkDraw...
  TPolyPoint = array of TPoint;
  TPolyDot = array of Integer;
  TImgMouseAction = (maNenhum=0, maSelectImage=1, maSelectColor=2, maDrawPen=3,
                     maDrawBrush=4, maRubber=5, maMarquee=6, maArcCutIni=7,
                     maArcCutFim=8, maSelPenColor=9, maSelBrushColor=10);
  // TUnDmkChalk.NomeToolKind(ToolKind: TToolKind): String;
  TToolKind = (fdNenhum=0, fdLivre=1, fdLinha=2, fdRetangulo=3, fdElipse=4,
               fdPolyline=5, fdRoundRect=6, fdPolygon=7, fdTextBox=8,
               fdPolyBezier=9, fdArc=10, fdAngleArc=11, fdBrush=12,
               fdFigure=13, fdRectArc=14, fdMarquee=15, fdDrag=16);
  TToolKinds = set of TToolKind;
  TToolsUsed = array of array[0..1] of Integer;
  TShowKind = (skNenhuma=0, skDrawVetor=1, skTexto=2);
  TStrDraw = array of array[0..1] of TPoint;
  TStrDraws = array of TStrDraw;

  TShowKindList = array of array[0..2] of Integer;
  // FIM TdmkDraw...



  //
  TTemControle = set of dmkTbCtrl;
  //
  TAppGrupLst = array of array of String;
  TArrSelIdxInt1 = array of Integer;
  TArrSelIdxInt2 = array of array[0..1] of Integer;
  //
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
  //
  TPartOuTodo = (ptNada=0, ptParcial=1, ptTotal=2);
  //
  TTribOperacao = (tribcalcNone=0, tribcalcInclusoBase=1, tribcalcExclusoBase=2(*,
                   tribcalcInclusoNivel=3, tribcalcExclusoNivel=4*));
  //
  TVSRestituicaoInn = (rivsNone=0, rivsPecas=1, rivsPesoKg=2, rivsSalKg=3,
                       rivsValor=4);
  TTabToWork = (ttwA=1, ttwB=2, ttwC=3, ttwD=4);
  //
  TAcaoMath = (amathZera=0, amathSubtrai=1, amathSoma=2, amathSubstitui=3);

  TLastAcao   = (laIns=0, laUpd=1, laDel=2);
  TAlterWeb   = (alDsk=0, alWeb=1, alMob=2);
  TDeviceType = (stDesktop=0, stWeb=1, stMobile=2);

  TUnDmkEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  TextoToSQLType(Tipo: String): TSQLType;
    function  ObtemStr_LogActnID(LogActnID: TLogActnID): String;

  end;
var
  DmkEnums: TUnDmkEnums;

(*
var
  DMK_TYPE_DIARY: TDmkTypeDiary = dtdiarNone;
*)

const
  // Aplicacao de Condicap de pagamento (Prazo)
  CO_APLIC_COND_PAG_FRETE = 8;
  //
  CO_ToolKindsPlainGeometry: TToolKinds = ([fdRetangulo, fdElipse, fdRoundRect, fdPolygon]);
  CO_ToolKindsPolyUpDown: TToolKinds = ([fdPolyLine, fdPolygon]);
  cTemControleSim: TTemControle = ([tctrlLok, tctrlCad, tctrlAlt, tctrlWeb, tctrlAti]);
  cTemControleNao: TTemControle = ([tctrlWeb, tctrlAti]);
  //
  CO_TXT_naxNaoInfo     = 'N�o informado';
  CO_TXT_naxEntiContat  = 'Contatos';
  CO_TXT_naxEntidade    = 'Entidade';
  CO_TXT_naxAvulso      = 'Avulso';
  MaxNFeautXML = Integer(High(TNFeautXML));
  sNFeautXML: array[0..MaxNFeautXML] of string = (
    CO_TXT_naxNaoInfo        , // 0
    CO_TXT_naxEntiContat     , // 1
    CO_TXT_naxEntidade       , // 2
    CO_TXT_naxAvulso           // 3
  );

  CO_TXT_uqpIndefinido  = 'Indefinido';
  CO_TXT_uqpADefinir    = 'A Definir';
  CO_TXT_uqpEspecifico  = 'Espec�fico';
  CO_TXT_uqpGeral       = 'Geral';
  MaxUsaQualPredef = Integer(High(TUsaQualPredef));
  sUsaQualPredef: array[0..MaxUsaQualPredef] of string = (
    CO_TXT_uqpIndefinido   , // 0
    CO_TXT_uqpADefinir     , // 1
    CO_TXT_uqpEspecifico   , // 2
    CO_TXT_uqpGeral         // 3
  );
  MaxListaStatusOSExec = Integer(High(TStatusOSExec));
  sListaStatusOSExec: array[0..MaxListaStatusOSExec] of String = (
  //soseNaoDefinido=0,
  'N�o definido',
  //soseEmSer=1,
  'Em ser',
  //soseExecutada=2,
  'Executada',
  //soseCancelada=3
  'Cancelada'
  );

  CO_TXT_tribcalcNone          = 'Indefinido';
  CO_TXT_tribcalcInclusoBase   = 'Incluso na base';
  CO_TXT_tribcalcExclusoBase   = 'Excluso na base';
  (*CO_TXT_tribcalcInclusoNivel  = 'Incluso no n�vel';
  CO_TXT_tribcalcExclusoNivel  = 'Excluso no n�vel';*)
  MaxTribOperacao = Integer(High(TTribOperacao));
  sListaTribOperacao: array[0..MaxTribOperacao] of string = (
  CO_TXT_tribcalcNone             , // 0
  CO_TXT_tribcalcInclusoBase      , // 1
  CO_TXT_tribcalcExclusoBase      (*, // 2
  CO_TXT_tribcalcInclusoNivel     , // 3
  CO_TXT_tribcalcExclusoNivel     // 4*)
  );

implementation

{ TUnDmkEnums }

function TUnDmkEnums.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy: Result := 'C�pia';
    stDel: Result := 'Exclus�o';
    stIns: Result := 'Inclus�o';
    stLok: Result := 'Travado';
    stUpd: Result := 'Altera��o';
    stUnd: Result := 'Desiste';
    stPsq: Result := 'Pesquisa';
    else   Result := '???????';
  end;
end;

function TUnDmkEnums.ObtemStr_LogActnID(LogActnID: TLogActnID): String;
begin
  if Integer(High(TLogActnID)) > 1 then
    raise Exception.Create('"ObtemStr_LogActnID" aqu�m do m�ximo!');
  case LogActnID of
    laiOSWCab: LowerCase('OSWCab');
    else (*laiNone:*) Result := '???';
  end;
end;

function TUnDmkEnums.TextoToSQLType(Tipo: String): TSQLType;
begin
  if Tipo = 'C�pia' then      Result := stCpy else
  if Tipo = 'Exclus�o' then   Result := stDel else
  if Tipo = 'Inclus�o' then   Result := stIns else
  if Tipo = 'Travado' then    Result := stLok else
  if Tipo = 'Altera��o' then  Result := stUpd else
                              Result := stNil;
end;

end.

{
=======
unit UnDmkEnums;

interface


const
  CO_ShowKindList_0_RealIndx = 0;
  CO_ShowKindList_1_ShowKind = 1;
  CO_ShowKindList_2_ToolKind = 2;
  //
  //
  MaxPixelCount = 65536;

type
  //Temporario!!!!!!!!!!
  TConvGrandeza = (cgMtoFT, cgFTtoM, cgKGtoLB, cgLBtoKG);
  // Aplicativos
  // B U G S T R O L
  TGraBugsOpera = (gboNenhum, gboServi, gboCaixa);
  TGraBugsServi = (gbsIndef, gbsAplEMon, gbsAplica, gbsMonitora, gbsMonMulti);
  TGraBugsMater = (gbmIndef, gbmEquiEProd, gbmEquipam, gbmProduto);
  TNivelRatifConsumoOS = (nrcosNenhum=0, nrcosProduto=1, nrcosFormula=2,
                         nrcosServico=3, nrcosLocalizador=4);
  // FIM  B U G S T R O L

  // B L U E   D E R M
  TImpDesnate = (impdsnDenate, impdsnDenSubUni, impdsnDenSubAll );
  TTipoCalcCouro = (ptcNaoInfo=0, ptcPecas=1, ptcPesoKg=2, ptcAreaM2=3,
                    ptcAreaP2=4, ptcTotal=5);
  TEstqMovimID = (emidAjuste=0, emidCompra=1, emidVenda=2,
                 emidReclasWE=3, emidBaixa=4, emidIndsWE=5, emidIndsVS=6,
                 emidClassArtVSUni=7, emidReclasVSUni=8, emidForcado=9,
                 emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
                 emidInventario=13, emidClassArtVSMul=14, emidPreReclasse=15,
                 emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,
                 emidEmProcWE=19, emidFinished=20, emidDevolucao=21,
                 emidRetrabalho=22, emidGeraSubProd=23, emidReclasVSMul=24,
                 emidTransfLoc=25);
  TEstqMovimNiv = (eminSemNiv=0, eminSorcClass=1, eminDestClass=2,
                   eminSorcIndsWB=3, eminDestIndsWB=4, eminSorcReclass=5,
                   eminDestReclass=6, eminSorcOper=7, eminEmOperInn=8,
                   eminDestOper=9, eminEmOperBxa=10, eminSorcPreReclas=11,
                   eminDestPreReclas=12, eminDestCurtiVS=13, eminSorcCurtiVS=14,
                   eminBaixCurtiVS=15, eminSdoArtInNat=16, eminSdoArtGerado=17,
                   eminSdoArtClassif=18, eminSdoArtEmOper=19, eminSorcWEnd=20,
                   eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23,
                   eminSdoArtEmWEnd=24, eminSdoFinish=25, eminSdoSubPrd=26,
                   eminSorcLocal=27, eminDestLocal=28);
  TEstqMovimType = (emitIndef=0, emitIME_I=1, emitIME_C=2, emitIME_P=3,
                    emitPallet=4, emitAvulsos=5, emitAgrupTotalizador=6,
                    emitAgrupItensNew=7, emitAgrupItensBxa=8);
  TEstqDefMulFldVS = (edmfIndef=0, edmfSrcNiv2=1, edmfMovCod=2, edmfIMEI=3);
  TVSLnkIDXtr = (lixIndefinido=0, lixPedVenda=1);
  TVSStatPall = (vsspIndefinido=0, vsspMontando=1, vsspDesmontando=2,
                 vsspMontEDesmo=3, vsspEncerrado=4, vsspEncerMont=5,
                 vsspEncerDesmo=6, vsspEncerMontEDesmo=7, vsspRemovido=8);
  TEstqEditGB = (eegbNone=0, eegbQtdOriginal=1, eegbDadosArtigo=2);
  TEstqMotivDel = (emtdWetCurti013=1, emtdWetCurti045=2, emtdWetCurti104=3,
                   emtdWetCurti008A=4, emtdWetCurti008B=5, emtdWetCurti079=6,
                   emtdWetCurti022=7, emtdWetCurti028=8, emtdWetCurti038=9,
                   emtdWetCurti093=10, emtdWetCurti078=11, emtdWetCurti006=12,
                   emtdWetCurti019=13, emtdWetCurti023=14, emtdWetCurti027=15,
                   emtdWetCurti060=16, emtdWetCurti076=17, emtdWetCurti106=18,
                   emtdWetCurti111=19, emtdWetCurti131=20, emtdWetCurti136=21);
  TEstqSubProd = (esubprdOriginal=0, esubprdDerivado=1);
  TEstqNivCtrl = (encPecas=0, encArea=1, encValor=2);
  TPQxTipo = (pqxtipoInventario=0, pqxtipoEntrada=10, pqxtipoPesagem=110,
              pqxtipoETE=150, pqxtipoOutros=190);
  // FIM  B L U E   D E R M

  // T O O L   R E N T
  TPrmRefQuem = (prqIndefinido=0, prqCliente=1, prqAgente=2);
  TPrmFatorValor = (pfvIndefinido=0, pfvValorTotal=1, pfvValorUnitario=2);
  TPrmFormaRateio = (pfrIndefinido=0, pfrValorTotal=1, pfrValorPorPessoa=2);
  TSrvLCtbDsp = (slcdUnknownToInf=0, slcdNoGerCtaAPag=1, slcdDescoFaturam=2,
                 slcdGeraCtaAPagr=3);
  // FIM  T O O L   R E N T

  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  CT-e
  TTipoConsumoWS_CTe = (tcwscteStatusServico=0, tcwscteRecepcao=1, tcwscteRetRecepcao=2,
                   tcwscteEveCancelamento=3, tcwsctePediInutilizacao=4, tcwscteConsultaCTe=5,
                   tcwscteEveCartaDeCorrecao=6);
  TCTeTpVeic = (ctetpveicTracao=0, ctetpveicReboque=1);
  TCTeTpRod = (ctetprodNaoAplicavel=0, ctetprodTruck=1, ctetprodToco=2,
               ctetprodCavaloMec=3, ctetprodVAN=4, ctetprodUtilitario=5,
               ctetprodOutros=6);
  TCTeTpCar = (ctetpcarNaoAplicavel=0, ctetpcarAberta=1, ctetpcarFrchadaBau=2,
               ctetpcarGranelera=3, ctetpcarPortaContainer=4, ctetpcarSider=5);
  TCTeTpCTe = (ctetpcteNormal=0, ctetpcteComplValrs=1, ctetpcteAnulacao=2,
               ctetpcteSubstituto=3);
  TCTeMyCST = (ctemycstIndefinido=0, ctemycstCST00Normal=1, ctemycstCST20BCRed=2,
               ctemycstCST40Isensao=3, ctemycstCST41NaoTrib=4, ctemycstCST51Diferido=5,
               ctemycstCST60ST=6, ctemycstCST90Outros=7, ctemycstCST90OutraUF=8,
               ctemycstCSTSimplesNacional=9);
  TCTeForPag = (cteforpagPago=0, cteforpagAPagar=1, cteforpagOutros=2);
  TCTeModal = (ctemodalIndefinido=0, ctemodalRodoviario=1, ctemodalAereo=2,
                ctemodalAquaviario=3, ctemodalFerroviario=4,
                ctemodalDutoviario=5, ctemodalMultimodal=6);
  TCTeTpServ = (ctetpservNormal=0, ctetpservSubcontratacao=1, ctetpservRedespacho=2,
                ctetpservRedespInterm=3, ctetpservServVincMultimodal=4);
  TCTeToma = (ctetomaRemetente=0, ctetomaExpedidor=1, ctetomaRecebedor=2,
              ctetomaDestinatario=3, ctetomaOutros=4);
  TCTeCUnid = (ctecunidM3=0, ctecunidKG=1, ctecunidTON=2, ctecunidUNIDADE=3,
               ctecunidLITROS=4, ctecunidMMBTU=5);
  TCTeMyStatus = (ctemystatusDesencerrada=8, ctemystatusEncerrada=9,
                  ctemystatusCTeDados=10, ctemystatusCTeGerada=20,
                  ctemystatusCTeAssinada=30, ctemystatusLoteRejeitado=40,
                  ctemystatusCTeAddedLote=50, ctemystatusLoteEnvEnviado=60,
                  ctemystatusLoteEnvConsulta=70);
  TCTeMyTpDocInf = (ctemtdiIndef=0, ctemtdiNFe=1, ctemtdiNFsOld=2,
               ctemtdiOutrosDoc=3);
  TCTeServicoStep = (ctesrvStatusServico=0, (*ctesrvEnvioAssincronoLoteCTe*)ctesrvEnvioLoteCTe=1,
                  ctesrvConsultarLoteCTeEnviado=2, ctesrvPedirCancelamentoCTe=3,
                  ctesrvPedirInutilizacaoNumerosCTe=4, ctesrvConsultarCTe=5,
                  ctesrvEventoCartaCorrecao=6(*, ctesrvConsultaCadastroEntidade=7,
                  ctesrvConsultaSitua��oCTe=8 (*inoperante*)(*,
                  ctesrvConsultaCTesDestinadas=9, ctesrvDownloadCTes=10,
                  ctesrvConsultaDistribuicaoDFeInteresse=11*));
  TCTeRodoLota =(cterodolotaNao=0, cterodolotaSim=1);
  TCTePropTpProp =(cteptpTAC_Agregado=0, cteptpTAC_Independente=1, cteptpTAC_Outros=2);
  TCTeRespSeg = (ctersRemetente=0, ctersExpedidor=1, ctersRecebedor=2,
                 ctersDestinatario=3, ctersEmitenteDoCTe=4, ctersTomadorDoServico=5);
  TEventosXXe = (evexxCCe,  // Carta de corre��o (NF-e, CT-e)
                 evexxCan,  // Cancelamento (NFe, CT-e, MDF-e)
                 evexxEnc,   // Encerramento (MDF-e)
                 evexxIdC    // Inclusao Condutor (MDF-e)
                 (*eveMDe*)); // Manifesta��o do deestinat�rio
  ///  FIM CT-e


  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  MDF-e
  TTipoConsumoWS_MDFe = (tcwsmdfeStatusServico=0, tcwsmdfeRecepcao=1, tcwsmdfeRetRecepcao=2,
                   tcwsmdfeEveCancelamento=3, tcwsmdfePediInutilizacao=4,
                   tcwsmdfe5=5, tcwsmdfeEveIncCondutor=6, tcwsmdfe7=7, tcwsmdfeEveEncerramento=8,
                   tcwsmdfeConsultaNaoEncerrados=9 );
  TMDFeTipoEmitente = (mdfetpemitIndef=0, mdfetpemitPrestadorServico=1, mdfetpemiCargaPropria=2);
  TMDFeModal = (mdfemodalIndefinido=0, mdfemodalRodoviario=1, mdfemodalAereo=2,
                mdfemodalAquaviario=3, mdfemodalFerroviario=4);
  TMDFeMyStatus = (mdfemystatusDesencerrada=8, mdfemystatusEncerrada=9,
                  mdfemystatusMDFeDados=10, mdfemystatusMDFeGerada=20,
                  mdfemystatusMDFeAssinada=30, mdfemystatusLoteRejeitado=40,
                  mdfemystatusMDFeAddedLote=50, mdfemystatusLoteEnvEnviado=60,
                  mdfemystatusLoteEnvConsulta=70);
  TMDFeServicoStep = (mdfesrvStatusServico=0, (*mdfesrvEnvioAssincronoLoteMDFe*)mdfesrvEnvioLoteMDFe=1,
                  mdfesrvConsultarLoteMDFeEnviado=2, mdfesrvEventoCancelamento=3,
                  (*mdfesrvPedirInutilizacaoNumerosMDFe=4, mdfesrvConsultarMDFe=5,*)
                  mdfesrvEventoIncCondutor=6, (*mdfesrvConsultaCadastroEntidade=7,*)
                  mdfesrvEventoEncerramento=8 (*inoperante???*)(*,
                  mdfesrvConsultaNaoEncerrados=9(*, mdfesrvDownloadMDFes=10,
                  mdfesrvConsultaDistribuicaoDFeInteresse=11*));
  TMDFeCargaCUnid = (mdfeccuIndef=0, mdfeccuKG=1, mdfeccuTON=2);

  ///  FIM MDF-e


  // TODOS
  TTipoXXe = (tipoxxeNFe=55, tipoxxeCTe=57, tipoxxeMDFe=58);
  TTipoNoXML = (tnxAttrStr, tnxTextStr);
  TStatusOSExec = (soseNaoDefinido=0, soseEmSer=1, soseExecutada=2,
    soseCancelada=3);
  TdmkAppID= (dmkappB_L_U_E_D_E_R_M=2, dmkappS_Y_N_D_I_C=4,  dmkappB_U_G_S_T_R_O_L=24);
  // FIM Aplicativos
  //TDmkTypeDiary = (dtdiarNone=0, dtdiarSAC=1, dtdiarSimple=2);
  TDmkModuloApp = (mdlappNenhum=0, mdlappAppMain=1,mdlappAllBase=2,
                   mdlappEntidades=3, mdlappEmpresas=4, mdlappAgenda=5,
                   mdlappAnotacoes=6, mdlappBancos=7, mdlappBloquetos=8,
                   mdlappCNAB=9, mdlappContratos=10, mdlappCRO=11,
                   mdlappDiario=12, mdlappEmail=13, mdlappEstoque=14,
                   mdlappFavoritos=15, mdlappFinanceiro=16, mdlappFPMin=17,
                   mdlappFPMax=18, mdlappFTP=19, mdlappGrade=20, mdlappNFe=21,
                   mdlappNFSe=22, mdlappPerfis=23, mdlappProtocolos=24,
                   mdlappSAC=25, mdlappSAF=26, mdlappSPED=27, mdlappWEB=28,
                   mdlappWTextos=29, mdlappCuns=30, mdlappBina=31,
                   mdlappConciBco=32, mdlappGFat=33, mdlappGPed=34);
  TArrayListaEstatus = array of array[0..5] of Integer;
  TDataTipo = (dtMySQL, dtSystem, dtSystem2, dtSystem3, dtTexto, dtDelphi);
  TfrxImpComo = (ficMostra, ficImprime, ficSalva, ficExporta, ficNone);
  TFormCadEnti = (fmcadSelecionar, fmcadEntidades, fmcadEntidade2);
  TDmkDBMSs = (dbmsUndef=0, dbmsMySQL=1, dbmsSQLite=2, dbmsIBLite=3);
  TDmkDBSrc = (dbsrcLocalServer=0, dbsrcLocalUser=1, dbsrcWebServer=2, dbsrcWebUser=3);
  dmkTbCtrl = (tctrlCad=0, tctrlAlt=1, tctrlLok=2, (*tctrlDel, *)tctrlWeb=3, tctrlAti=4);
  TAcaoCriaTabela   = (actCreate=0, actDescribe=1);
  TResultVerify = (rvOK=0, rvErr=1, rvAbort=2);
  TMyMotivo = (mymotDifere=0, mymotSemRef=1);
  TSQLType    = (stIns=0, stUpd=1, stDel=2, stCpy=3, stLok=4, stUnd=5, stPsq=6, stNil=7);
  TUsoXMLQuery = (uxqNoSQL=0, uxqSQLite=1, uxqMySQL=2);
  TAcessFmModo = (afmoSoMaster=0, afmoSoAdmin=1, afmoSoBoss=2, afmoNegarSemAviso=3,
                  afmoNegarComAviso=4, afmoLiberado=5, afmoSemVerificar=6);
  TTipoSinal = (tsPos=0, tsNeg=1, tsDef=2);
  TLogActnID = (laiNone=0, laiOSWCab=1, laiOSWPipMon=2, laiOSPipIts=3);
  TLogAcaoExe = (laeNone=0, laeDownload=1, laeUpload=2, laeSelected=3,
                 laeInsert=4, laeUpdated=5, laeDeleted=6, laeFinished=7);
  TFormaCalcPercent = (fcpercDesconto, fcpercAcrescimo, fcpercPrecoTax);
  TWBRclass = (wbrEntrada, wbrEstoque);
  TEnvioCNAB = (ecnabIndefinido, ecnabRemessa, ecnabRetorno, ecnabBloquet);
  TFormaWBIndsWE = (fiwNone, fiwPesagem, fiwBaixaPrevia);
  TRelAbrange = (relNada, relAbrangeEmpresa, relAbrangeCliente);
  TValCelRecXLS = (vcrxNenhum=0, vcrxSetor=1, vcrxCliente=2, vcrxTecnico=3,
    vcrxAccount=4, vcrxArtigo=5, vcrxEspessura=6, vcrxRendiment=7,
    vcrxRelatA=8, vcrxPesoCouro=9, vcrxCambio=10, vcrxQuantia=11, vcrxData=12,
    vcrxRebaixe=13);
  TFileEncodingType = (fetAnsi=0, fetUnicode=1, fetUnicodeBigEndian=2, fetUTF_8=3);
  TDateEncodeType = (detJustSum=0, detLastDaySameMonth=1, detFirstDayNextMonth=2);
  TAllFormat  = (dmktfNone, dmktfString, dmktfDouble, dmktfInteger, dmktfLongint,
                 dmktfInt64, dmktfDate, dmktfTime, dmktfDateTime, dmktfMesAno,
                 dmktf_AAAAMM, dmktf_AAAA_MM,
                 // Deve ser o �ltimo:
                 dmktfUnknown);
  TdmkDataFmt = (dmkdfShort, dmkdfLong);
  TdmkHoraFmt = (dmkhfShort=0, dmkhfLong=1, dmkhfMiliSeconds=2, dmkhfExtended=3,
                 dmkhfTZD_UTC=4);
  TDmkSpidAct = (dsaUnkn=0, dsaFrst=1, dsaPrio=2, dsaThis=3, dsaNext=4,
                dsaLast=5, dsaPesq=6, dsaDesc=7, dsaConf=8, dsaCanc=9,
                dsaHome=10, dsaPlus=11, dsaEdit=12, dsaKill=13, dsaImpr=14,
                dsaCfgs=15, dsaClse=16, dsaOlho=17);
  TKndFmDmk = (kfdNoDef=0, kfdNoXtra=1, kfdCadTypA=2);
  TDmkTabKnd = (dtkMainForm=0, dtkOther=1, dtkNone=2);
  TUpdType    = (utYes=0, utNil=1, utIdx=2, utInc=3);
  TMskType    = (fmtNone, fmtCPFJ, fmtCEP, fmtTelCurto, fmtTelLongo, fmtNCM,
                 fmtCBO2002, fmtIE, fmtNumRua, fmtUF, fmtVersao, fmtCPFJ_NFe,
                 fmtCEP_NFe, fmtTel_NFe, fmtIE_NFe, fmtChaveNFe);
  TComoFmtChNFe = (cfcnNone, cfcnDANFE, cfcnFrendly);
  TDmkCanEdit = (dceOnInsUpd=0, dceNever=1, dceAllways=2);
  TNovoCodigo = (ncIdefinido=0, ncControle=1, CtrlGeral=2, ncGerlSeq1=3);
  TDmkEditStyle = (desEdit=0, desClearingEdit=1, desTimeEdit=2, desComboEdit=2,
                desPasswordEdit=3);
  TReceitaSetor = (recsetrNenhum, recsetrRibeira, recsetrRecurtimento, recsetrAcabamento);
  TAquemPag  = (apFornece=0, apCliente=1, apTranspo=2, apNinguem=3);
  TMinMax    = (mmNenhum=0, mmMinimo=1, mmMaximo=2, mmAmbos=3);
  TAppVerNivel = (avnAlfa=0, avnBeta=1, avnEstavel=2);
  TAppVerRevert = (avrNo=0, avrYes=1);
  // // 0 - N�o informado
     // 1 - EntiContat
     // 2 - Entidades
     // 3 - Avulso
  TClasseEnti = (classeentiEmpresa, classeentiCliente, classeentiTransporta);
  TNFeCTide_TipoDoc = (NFeTipoDoc_Desconhecido,
                       NFeTipoDoc_nfe_0, NFeTipoDoc_nfe_1,
                       NFeTipoDoc_enviNFe, NFeTipoDoc_nfeProc,
                       NFeTipoDoc_cancNFe, NFeTipoDoc_retCancNFe,
                       NFeTipoDoc_procCancNFe, NFeTipoDoc_inutNFe,
                       NFeTipoDoc_retInutNFe, NFeTipoDoc_procInutNFe);
  TNFeCodType = (nfeCTide_TipoDoc,  nfeCTide_tpNF,  nfeCTide_indPag,
                 nfeCTide_procEmi, nfeCTide_finNFe, nfeCTide_tpAmb,
                 nfeCTide_tpEmis, nfeCTide_tpImp,  nfeCTModFrete,
                 nfeCTide_indCont,
                 nfeCTide_idDest, nfeCTide_indFinal, nfeCTide_indPres,
                 nfeCTdest_indIEDest, nfeCTmotDesICMS);
  TNFeServicoStep = (nfesrvStatusServico=0, (*nfesrvEnvioAssincronoLoteNFe*)nfesrvEnvioLoteNFe=1,
                  nfesrvConsultarLoteNfeEnviado=2, nfesrvPedirCancelamentoNFe=3,
                  nfesrvPedirInutilizaCaoNumerosNFe=4, nfesrvConsultarNFe=5,
                  nfesrvEnviarLoteEventosNFe=6, nfesrvConsultaCadastroEntidade=7,
                  nfesrvConsultaSitua��oNFE=8 (*inoperante*),
                  nfesrvConsultaNFesDestinadas=9, nfesrvDownloadNFes=10,
                  nfesrvConsultaDistribuicaoDFeInteresse=11);
  TNFeDFeSchemas = (nfedfeschUnknown=0, nfedfeschResNFe=1, nfedfeschProcNFe=2,
                  nfedfeschResEvento=3, nfedfeschProcEvento=4);
  TXXeIndSinc = (nisAssincrono=0, nisSincrono=1);
  TNFeautXML = (naxNaoInfo=0, naxEntiContat=1, naxEntidades=2, naxAvulso=3);
  TNFEAgruRelNFs = (nfearnIndef=0, nfearnCOFP=1, nfearnNCM=2, nfenfearnNatOP=3);
  TIBPTaxTabs = (ibptaxtabNCM=0, ibptaxtabNBS=1, ibptaxtabLC116=2);
  TIBPTaxOrig = (ibptaxoriNaoInfo=0, ibptaxoriNacional=1, ibptaxoriImportado=2);
  TIBPTaxFont = (ibptaxfontNaoInfo=0, ibptaxfontIBPTax=1, ibptaxfontCalculoProprio=2);
  TTpCalcTribNFe = (tctnfeIndefinido=0, tctnfeAutomatico=1, tctnfeManual=2,
                    tctnfeMixto=3);
  TNFeIndFinal = (indfinalNormal=0, indfinalConsumidorFinal=1);
  TindFinalTribNFe = (infFinTribIndefinido=0, infFinTribSoConsumidorFinal=1,
                      infFinTribTodos=2);
  TcSitNFe = (csitnfeDesconhecido=0, csitnfeAutorizado=1, csitnfeDenegado=2);
  TdmkCambios = (cambiosBRL=1, cambiosUSD=2, cambiosEUR=3, cambiosIDX=4);
  TNFeEventos = (nfeeveCCe=110110, nfeeveCan=110111,
                 nfeeveMDeConfirmacao=210200, nfeeveMDeCiencia=210210,
                 nfeeveMDeDesconhece=210220, nfeeveMDeNaoRealizou=210240,
                 nfeeveEPEC=110140);

(*  Desabilitado por causa do multiOS do XE5!
  //TKeyType    = (ktString, ktBoolean, ktInteger, ktCurrency, ktDate, ktTime);
*)

  TFormaCopiaInterfaceCompo = (fcicBitBlt, fcicPaintTo);
  TFormaFlipReverseBmp = (ffrbScanLine, ffrbCopyRect, ffrbStretchBlt);
  TUsaQualPredef = (uqpIndefinido, uqpADefinir, uqpEspecifico, uqpGeral);

  // TdmkDraw...
  TPolyPoint = array of TPoint;
  TPolyDot = array of Integer;
  TImgMouseAction = (maNenhum=0, maSelectImage=1, maSelectColor=2, maDrawPen=3,
                     maDrawBrush=4, maRubber=5, maMarquee=6, maArcCutIni=7,
                     maArcCutFim=8, maSelPenColor=9, maSelBrushColor=10);
  // TUnDmkChalk.NomeToolKind(ToolKind: TToolKind): String;
  TToolKind = (fdNenhum=0, fdLivre=1, fdLinha=2, fdRetangulo=3, fdElipse=4,
               fdPolyline=5, fdRoundRect=6, fdPolygon=7, fdTextBox=8,
               fdPolyBezier=9, fdArc=10, fdAngleArc=11, fdBrush=12,
               fdFigure=13, fdRectArc=14, fdMarquee=15, fdDrag=16);
  TToolKinds = set of TToolKind;
  TToolsUsed = array of array[0..1] of Integer;
  TShowKind = (skNenhuma=0, skDrawVetor=1, skTexto=2);
  TStrDraw = array of array[0..1] of TPoint;
  TStrDraws = array of TStrDraw;

  TShowKindList = array of array[0..2] of Integer;
  // FIM TdmkDraw...



  //
  TTemControle = set of dmkTbCtrl;
  //
  TAppGrupLst = array of array of String;
  TArrSelIdxInt1 = array of Integer;
  TArrSelIdxInt2 = array of array[0..1] of Integer;
  //
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
  //
  TPartOuTodo = (ptNada=0, ptParcial=1, ptTotal=2);
  //
  TTribOperacao = (tribcalcNone=0, tribcalcInclusoBase=1, tribcalcExclusoBase=2(*,
                   tribcalcInclusoNivel=3, tribcalcExclusoNivel=4*));
  //
  TVSRestituicaoInn = (rivsNone=0, rivsPecas=1, rivsPesoKg=2, rivsSalKg=3,
                       rivsValor=4);
  TTabToWork = (ttwA=1, ttwB=2, ttwC=3, ttwD=4);
  //

  TUnDmkEnums = class(TObject)
  private
     Private declarations
  public
     Public declarations
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  TextoToSQLType(Tipo: String): TSQLType;
    function  ObtemStr_LogActnID(LogActnID: TLogActnID): String;

  end;
var
  DmkEnums: TUnDmkEnums;

(*
var
  DMK_TYPE_DIARY: TDmkTypeDiary = dtdiarNone;
*)

const
  // Aplicacao de Condicap de pagamento (Prazo)
  CO_APLIC_COND_PAG_FRETE = 8;
  //
  CO_ToolKindsPlainGeometry: TToolKinds = ([fdRetangulo, fdElipse, fdRoundRect, fdPolygon]);
  CO_ToolKindsPolyUpDown: TToolKinds = ([fdPolyLine, fdPolygon]);
  cTemControleSim: TTemControle = ([tctrlLok, tctrlCad, tctrlAlt, tctrlWeb, tctrlAti]);
  cTemControleNao: TTemControle = ([tctrlWeb, tctrlAti]);
  //
  CO_TXT_naxNaoInfo     = 'N�o informado';
  CO_TXT_naxEntiContat  = 'Contatos';
  CO_TXT_naxEntidade    = 'Entidade';
  CO_TXT_naxAvulso      = 'Avulso';
  MaxNFeautXML = Integer(High(TNFeautXML));
  sNFeautXML: array[0..MaxNFeautXML] of string = (
    CO_TXT_naxNaoInfo        , // 0
    CO_TXT_naxEntiContat     , // 1
    CO_TXT_naxEntidade       , // 2
    CO_TXT_naxAvulso           // 3
  );

  CO_TXT_uqpIndefinido  = 'Indefinido';
  CO_TXT_uqpADefinir    = 'A Definir';
  CO_TXT_uqpEspecifico  = 'Espec�fico';
  CO_TXT_uqpGeral       = 'Geral';
  MaxUsaQualPredef = Integer(High(TUsaQualPredef));
  sUsaQualPredef: array[0..MaxUsaQualPredef] of string = (
    CO_TXT_uqpIndefinido   , // 0
    CO_TXT_uqpADefinir     , // 1
    CO_TXT_uqpEspecifico   , // 2
    CO_TXT_uqpGeral         // 3
  );
  MaxListaStatusOSExec = Integer(High(TStatusOSExec));
  sListaStatusOSExec: array[0..MaxListaStatusOSExec] of String = (
  //soseNaoDefinido=0,
  'N�o definido',
  //soseEmSer=1,
  'Em ser',
  //soseExecutada=2,
  'Executada',
  //soseCancelada=3
  'Cancelada'
  );

  CO_TXT_tribcalcNone          = 'Indefinido';
  CO_TXT_tribcalcInclusoBase   = 'Incluso na base';
  CO_TXT_tribcalcExclusoBase   = 'Excluso na base';
  (*CO_TXT_tribcalcInclusoNivel  = 'Incluso no n�vel';
  CO_TXT_tribcalcExclusoNivel  = 'Excluso no n�vel';*)
  MaxTribOperacao = Integer(High(TTribOperacao));
  sListaTribOperacao: array[0..MaxTribOperacao] of string = (
  CO_TXT_tribcalcNone             , // 0
  CO_TXT_tribcalcInclusoBase      , // 1
  CO_TXT_tribcalcExclusoBase      (*, // 2
  CO_TXT_tribcalcInclusoNivel     , // 3
  CO_TXT_tribcalcExclusoNivel     // 4*)
  );

implementation

 TUnDmkEnums

function TUnDmkEnums.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy: Result := 'C�pia';
    stDel: Result := 'Exclus�o';
    stIns: Result := 'Inclus�o';
    stLok: Result := 'Travado';
    stUpd: Result := 'Altera��o';
    stUnd: Result := 'Desiste';
    stPsq: Result := 'Pesquisa';
    else   Result := '???????';
  end;
end;

function TUnDmkEnums.ObtemStr_LogActnID(LogActnID: TLogActnID): String;
begin
  if Integer(High(TLogActnID)) > 1 then
    raise Exception.Create('"ObtemStr_LogActnID" aqu�m do m�ximo!');
  case LogActnID of
    laiOSWCab: LowerCase('OSWCab');
    else (*laiNone:*) Result := '???';
  end;
end;

function TUnDmkEnums.TextoToSQLType(Tipo: String): TSQLType;
begin
  if Tipo = 'C�pia' then      Result := stCpy else
  if Tipo = 'Exclus�o' then   Result := stDel else
  if Tipo = 'Inclus�o' then   Result := stIns else
  if Tipo = 'Travado' then    Result := stLok else
  if Tipo = 'Altera��o' then  Result := stUpd else
                              Result := stNil;
end;

end.
}
