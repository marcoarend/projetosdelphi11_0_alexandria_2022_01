unit dmkVariable;

interface

uses
  Windows,  Messages, SysUtils, Classes,  Graphics, Controls,  Forms,
  Dialogs,  DBCtrls,  StdCtrls, dmkEditCB, dmkGeral, Variants, DB,
  ExtCtrls;

type
  TdmkVariable = class(TComponent)
  private
    { Private declarations }
    // 2013-07-19
    FOnChange: TNotifyEvent;
    // Fim 2013-07-19
    FQryCampo: String;
    FUpdCampo: String;
    FValUsuVal: Variant;
    FOldValor: Variant;

    function GetValue: Variant;
    procedure SetValue(Value: Variant);
    procedure SetQryCampo(Value: String);
    procedure SetUpdCampo(Value: String);
    procedure SetOldValor(Value: Variant);
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    property QryCampo: String read FQryCampo write SetQryCampo;
    property UpdCampo: String read FUpdCampo write SetUpdCampo;
    property ValueVariant: Variant read GetValue write SetValue;
    property OldValor: Variant read FOldValor write SetOldValor;
    // 2013-07-19
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    // Fim 2013-07-19
  end;

procedure Register;

implementation

function TdmkVariable.GetValue: Variant;
//var
  //DataSet: TDataSet;
begin
  Result := FValUsuVal;
end;

procedure TdmkVariable.SetValue(Value: Variant);
begin
  if VarType(Value) <> VarType(FValUsuVal) then
  begin
    FValUsuVal := Unassigned;
    TVarData(FValUsuVal).VType := VarType(Value);
  end;
  if Value <> FValUsuVal then
  begin
    FValUsuVal := Value;
    //
    if Assigned(FOnChange) then
      FOnChange(Self);
  end;
end;

procedure TdmkVariable.SetOldValor(Value: Variant);
begin
  if Value <> FOldValor then
  begin
    FOldValor := Value;
  end;
end;

procedure TdmkVariable.SetQryCampo(Value: String);
begin
  if Value <> FQryCampo then
  begin
    FQryCampo := Value;
  end;
end;

procedure TdmkVariable.SetUpdCampo(Value: String);
begin
  if Value <> FUpdCampo then
  begin
    FUpdCampo := Value;
  end;
end;

procedure Register;
begin
  RegisterComponents('Dermatek', [TdmkVariable]);
end;

end.
