unit UnInternalConsts2;

interface

uses  Windows, Graphics, Controls;

var
  IC2_AparenciasTituloPainelItem : Integer;
  IC2_AparenciasTituloPainelTexto : Integer;
  IC2_AparenciasDadosPainel : Integer;
  IC2_AparenciasDadosTexto : Integer;
  IC2_AparenciasGradeAItem : Integer;
  IC2_AparenciasGradeATitulo : Integer;
  IC2_AparenciasGradeATexto : Integer;
  IC2_AparenciasGradeAGrade : Integer;
  IC2_AparenciasGradeBItem : Integer;
  IC2_AparenciasGradeBTitulo : Integer;
  IC2_AparenciasGradeBTexto : Integer;
  IC2_AparenciasGradeBGrade : Integer;
  IC2_AparenciasEditAItem : Integer;
  IC2_AparenciasEditATexto : Integer;
  IC2_AparenciasEditBItem : Integer;
  IC2_AparenciasEditBTexto : Integer;
  IC2_AparenciasConfirmaPainelItem : Integer;
  IC2_AparenciasControlePainelItem : Integer;
  IC2_AparenciasFormPesquisa : Integer;
  IC2_AparenciasLabelPesquisa : Integer;
  IC2_AparenciasLabelDigite : Integer;
  IC2_AparenciasEditPesquisaItem : Integer;
  IC2_AparenciasEditPesquisaTexto : Integer;
  IC2_AparenciasCorFundoA : Integer;
  IC2_AparenciasCorFundoB : Integer;
  IC2_AparenciasEditItemIn : Integer;
  IC2_AparenciasEditItemOut : Integer;
  IC2_AparenciasEditTextIn : Integer;
  IC2_AparenciasEditTextOut : Integer;

  IC2_PadroesMoeda: String;
  IC2_PadroesJuros: Double;
  IC2_PadroesJurosDD: Double;
  IC2_PadroesEnergia: Double;
  IC2_PadroesPECouro: Double;
  IC2_PadroesPerCula: Double;
  IC2_PadroesIAMMP: Double;
  IC2_PadroesDMTMin: Integer;
  IC2_PadroesSucata: Double;
  IC2_PadroesAguaP: Double;
  IC2_PadroesAguaC: Double;
  IC2_PadroesDesperdicio: Double;
  IC2_PadroesClienteI: Integer;
  IC2_PadroesUsaRIPIPQ: String;
  IC2_PadroesCUB: Double;
  IC2_PadroesIndexNome: String;
  IC2_PadroesIndexValor: Double;
  IC2_PadroesIndexFormat: Integer;
  IC2_PadroesDataAtualiza: TDateTime;
  IC2_PadroesAtEstqFin: String;
  IC2_PadroesPerdaCaloria: Double;
  IC2_PadroesRecHidrica: Integer;
  IC2_PadroesRecLinhaTE: Integer;
  IC2_PadroesRecCaldeira: Integer;
  IC2_PadroesLinhasRec: Integer;
  IC2_PadroesImpNomeRec: String;
  IC2_PadroesLinhasTin: Integer;
  IC2_PadroesImpNomeTin: String;
  IC2_PadroesSideral: String;
  IC2_PadroesImpostoM1: Double;
  IC2_PadroesImpostoM2: Double;
  IC2_PadroesImpostoE1: Double;
  IC2_PadroesImpostoE2: Double;
  IC2_PadroesImposto1: Double;
  IC2_PadroesImposto2: Double;
  IC2_PadroesUsaRICMSPQ: String;
  IC2_PadroesTxtGridA: String;
  IC2_PadroesTxtGridB: String;
  IC2_PadroesResolucao: String;
  IC2_PadroesDecimaisRec: Integer;
  IC2_PadroesDecimaisTin: Integer;
  IC2_PadroesMargemEsq: Integer;
  IC2_PadroesResized: String;
  IC2_PadroesEstqMinPQ: Double;
  IC2_PadroesEstqMinAnalogo: Double;
  IC2_PadroesEstqNegativo: String;
  IC2_PadroesAnimate: String;
  IC2_PadroesLoteRecGerenc: Integer;
  IC2_PadroesPQMinDias: Integer;
  IC2_PadroesPQMinUdd: String[1];
  IC2_PadroesPQMinData: TDateTime;
  IC2_PadroesPQMinPeriodo: Integer;
  IC2_PadroesPQMinPeriDias: Integer;
  IC2_PadroesPQMinPeriodic: Integer;
  IC2_PadroesPQMinCompra: TDateTime;
  IC2_PadroesLPA_Indice: Double;
  IC2_PadroesLPA_Const: Double;
  IC2_PadroesLPA_RL1: Double;
  IC2_PadroesLPA_RL2: Double;
  IC2_PadroesLPA_RLsinal: shortint;
  IC2_PadroesLPA_RP1: Double;
  IC2_PadroesLPA_RP2: Double;
  IC2_PadroesLPA_Intervalo: Double;
  //IC2_PadroesIPServer: String[20];

  // MPADORES
  IC2_MPadroesJurosDiv: Integer;
  IC2_MPadroesJurosEmp: Integer;
  IC2_MPadroesMultasDiv: Integer;
  IC2_MPadroesMultasEmp: Integer;

  // WOPCOES
  IC2_WopcoesObsReceita: String;
  IC2_WopcoesTintaBase1000: String;
  IC2_WopcoesTintaCorBase: String;
  IC2_WopcoesTintaCorCompensa: String;
  IC2_WopcoesVerCount: String;
  IC2_WopcoesVerInfo: String;
  IC2_WopcoesAvisarAtEstFin: Integer;
  IC2_WopcoesAtCustosMan: String;
  IC2_WopcoesAtVenalArt: String;
  IC2_WopcoesUsaEstFinArtCli: String;
  IC2_WopcoesVerCustoArtCli: String;
  IC2_WopcoesUsarEstFinPQ: String;
  IC2_WopcoesAtGeralManual: String;
  IC2_WopcoesCustoSobraTinta: String;
  IC2_WopcoesArredCal: ShortInt;
  IC2_WopcoesArredCur: ShortInt;
  IC2_WopcoesArredRec: ShortInt;
  IC2_WopcoesArredAca: ShortInt;
  IC2_WopcoesNaoBxaFulon: String;
  IC2_WopcoesImpRecShowArt: Boolean;
  IC2_WopcoesImpRecShowCor: Boolean;
  IC2_WopcoesImpRecShowLotes: Boolean;
  IC2_WopcoesImpRecShowMedia: Boolean;
  IC2_WopcoesImpRecShowEspessura: Boolean;
  IC2_WopcoesImpRecShowM2: Boolean;
  IC2_WopcoesImpRecShowP2: Boolean;
  IC2_WopcoesBxaCFinBal: Boolean;

  // LOPCOES
  IC2_LopcoesTeclaVirgula: Integer;
  IC2_LopcoesTeclaEnter: Integer;
  IC2_LopcoesTeclaTab: Integer;
  IC2_LopcoesTeclaPonto: Integer;
  IC2_LopcoesSetaDireita: Integer;

    // MASTER  // Usar Dmod.QrMaster
(*  IC2_MasterVersaoPQ: Integer;
  IC2_MasterVersaoMP: Integer;
  IC2_MasterVersaoFN: Integer;
  IC2_MasterNomeEmpresa: String;
*)
  //RELATORIOS
  VIEW_Regua : Boolean;
  VIEW_ItensRec, VIEW_ItensRec1, VIEW_ItensRec2, VIEW_Tintas2: Boolean;
  VIEW_PQEstqF, VIEW_PQEstq, VIEW_PQCom, VIEW_PQComF : Boolean;
  VIEW_PQHist, VIEW_PQMinPQ, VIEW_PQBal , VIEW_PQConsumo : Boolean;
  VIEW_ListRec, VIEW_PQPedOpen, VIEW_PQPedOPenIts : Boolean;
  VIEW_ArtigosCustosL, VIEW_ArtigosCustosM : Boolean;
  VIEW_ArtigosCustosLLista, VIEW_ArtigosCustosMLista : Boolean;
  VIEW_PQPed, VIEW_PQConsigna, VIEW_MP1: Boolean; 

  IC2_Tempo: TDateTime;
  IC2_COMPATU, IC2_COMPANT, IC2_M2P2ANT, IC2_M2P2ATU: TWinControl;
  IC2_CUSTOPESOANT,IC2_CUSTOPESOATU: TWinControl;
  IC2_COMPFM, IC2_NOMEFORMAPP, IC2_M2P2TXT, IC2_CUSTOPESOTXT: String;


  //  N�o testado ainda!!!
  VAR_DMK_COR_csText               : Integer = clWindowText; // csText;
  VAR_DMK_COR_csTitleTextActive    : Integer = clActiveCaption; // csTitleTextActive];
  VAR_DMK_COR_csTitleTextNoActive  : Integer = clInactiveCaption; // csTitleTextNoActive];
  VAR_DMK_COR_csButtonFace         : Integer = clBtnFace; // csButtonFace];
  VAR_DMK_COR_csButtonText         : Integer = clBtnText; // csButtonText];
  VAR_DMK_COR_csButtonHilight      : Integer = clBtnHighlight; // csButtonHilight];
  VAR_DMK_COR_csButtonlight        : Integer = cl3DLight; // csButtonlight];
  VAR_DMK_COR_csButtonShadow       : Integer = clBtnShadow; // csButtonShadow];
  VAR_DMK_COR_csButtonDkshadow     : Integer = cl3DDkShadow; // csButtonDkshadow];
  VAR_DMK_COR_csSelectText         : Integer = clActiveCaption; // csSelectText];
  VAR_DMK_COR_csSelectBg           : Integer = clBackground; // csSelectBg];
  VAR_DMK_COR_csHilightText        : Integer = clHighlightText; // csHilightText];
  VAR_DMK_COR_csHilight            : Integer = clHighlight; // csHilight];
  VAR_DMK_COR_csMenuBar            : Integer = clMenuBar; // csMenuBar];
  VAR_DMK_COR_csMenuBarText        : Integer = clMenuHighlight; // csMenuBarText];
  VAR_DMK_COR_csMenuText           : Integer = clMenuText; // csMenuText];
  VAR_DMK_COR_csMenubg             : Integer = clMenu; // csMenubg];
  VAR_DMK_COR_csCaption            : Integer = clCaptionText; // csCaption];
  VAR_DMK_COR_csScrollbar          : Integer = clScrollBar; // csScrollbar];
  VAR_DMK_COR_csTextDisable        : Integer = clInactiveCaptionText; // csTextDisable];

const
  DermaBD2_Host  = 'mysql50.bs2.com.br';
  DermaBD2_DBN   = 'dermanet';
  DermaBD2_Port  =  3306;
  DermaBD2_UName = 'dermanet';
  DermaBD2_UKhei = 'dHIkJtfM';

implementation

end.
