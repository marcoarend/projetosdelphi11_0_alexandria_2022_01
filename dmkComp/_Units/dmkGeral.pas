﻿(*{$I dmk.inc}*)
unit dmkGeral;
 //http://issfacilnet.maringa.pr.gov.br/iss/iss.htm

interface

uses
{$IfDef DMK_FMX}
  FMX.Forms,
{$Else}
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Grids, Variants, MaskUtils, Registry, ShellAPI,
  DBGrids,
{$EndIf}

{$IFDEF DELPHI12_UP}
  System.UITypes,
{$ENDIF}
  TypInfo, comobj, ShlObj, Winsock, Math, UnMsgInt, FMTBcd, Vcl.Menus,
  // Qry.SQL.Text
  Data.DB, System.Rtti,
  // mySQLDirectQuery
  //mySQLDirectQuery,
  // Fim Qry.SQL.Text
  UnDmkEnums,
  // ini Delphi 20 Alexandria
  //(*UnInternalConsts*)dmkGeral,
  // fim Delphi 20 Alexandria
  UnComps_Vars(*, UnGrl_Vars*);

var
  VAR_TEXTO_SEL: String;
  VAR_FORMATDATE, VAR_FORMATDATE2, VAR_FORMATDATE3, VAR_FORMATDATE4,
  VAR_FORMATDATE5, VAR_FORMATTIME, VAR_FORMATTIME2, VAR_FORMATTIME3,
  VAR_FORMATTIME4, VAR_FORMATTIME5, VAR_FORMATTIME6,
  VAR_FORMATDATE6, VAR_FORMATDATE0, VAR_FORMATTIMESTAMP4,
  VAR_FORMATDATETIME, VAR_FORMATDATETIME1, VAR_FORMATDATETIME2,
  VAR_FORMATDATETIME3, VAR_FORMATDATETIME4,
  VAR_FORMATDATEi, VAR_FORMATDATE7, VAR_FORMATDATE8,
  VAR_FORMATDATE9, VAR_FORMATDATE10, VAR_FORMATDATE11, VAR_FORMATDATE12,
  VAR_FORMATDATE13, VAR_FORMATDATE14, VAR_FORMATDATE15, VAR_FORMATDATE16,
  VAR_FORMATDATE17, VAR_FORMATDATE18, VAR_FORMATDATE19, VAR_FORMATDATE20,
  VAR_FORMATDATE21, VAR_FORMATDATE22, VAR_FORMATDATE23, VAR_FORMATDATE24,
  VAR_FORMATDATE25, VAR_FORMATDATE26,
  VAR_FORMATDATECB4: String;
  VAR_GETDATAI, VAR_GETDATAF: TDateTime;
  //
  VAR_FORMATFLOAT0, VAR_FORMATFLOAT1, VAR_FORMATFLOAT2, VAR_FORMATFLOAT3,
  VAR_FORMATFLOAT4, VAR_FORMATFLOAT5, VAR_FORMATFLOAT6, VAR_FORMATFLOAT7,
  VAR_FORMATFLOAT8, VAR_FORMATFLOAT9, VAR_FORMATFLOAT10: String;
  VAR_UNIDADEPADRAO: Integer;
  //
  VAR_SOMAIUSCULAS, VAR_MAIUSC_MINUSC: Boolean;
  VAR_SAVE_CFG_DBGRID_MENU: TPopupMenu;
  VAR_SAVE_CFG_DBGRID_GRID: TCustomDBGrid;
  VAR_SAVE_CFG_DBGRID_DMKV: TComponent;
  //VAR_CAN_CLOSE_FORM_MSG: Boolean;
  VAR_NAO_USA_KEY_LOCAL_MACHINE: Boolean;
  //
  //VAR_LISTA_BITMAPS_TITLE_GRID: TImageList;
var
  // ini Delphi 20 Alexandria
  VAR_CAN_CLOSE_FORM_MSG: Boolean = False;
  VAR_CAN_CLOSE_FORM_MAX,
  VAR_CAN_CLOSE_FORM_POS: Integer;
  // fim Delphi 20 Alexandria
const
  CO_SetrEmi_INDEFIN = 0;
  CO_SetrEmi_MOLHADO = 1; // Receitas de ribeira
  CO_SetrEmi_ACABATO = 2; // Receitas de Acabamento
  //
  CO_SourcMP_INDEFIN = 0;
  CO_SourcMP_Ribeira = 1; // Controle por kg
  CO_SourcMP_WetSome = 2; // Controle por m2
  CO_SourcMP_VS_BH   = 3; // Controle por kg VS
  CO_SourcMP_VS_WE   = 4; // Controle por m2 VS
  CO_SourcMP_VS_FI   = 5; // Controle por m2 VS
  //
{
  //TAgendaQuestao:
  CO_qagIndefinido = 0;
  CO_qagAvulso     = 1;
  CO_qagOSBgstrl   = 2;
  //
  CO_ttaNenhum     = 0;
  CO_ttaPreDef     = 1;
  CO_ttaTxtFree    = 2;
}
type
  // dmkDBGridTRB
  TdmkSQLFilter = (dmksfNone=0, dmksfText=1, dmksfDateRange=2, dmksfTimeRange=3,
  dmksfDateTimeRange=4, dmksfInteger=5, dmksfIntegerRange=6, dmksfFloat=7,
  dmksfFloatRange=8);
  TdmkSQLConditionStart = (dmkcsWhere=0, dmkcsAnd=1);
  TdmkSQLSortOrder = (dmksoASC=0, dmksoDESC=1);
  TdmkFilterMasc = (dmkfmAny=0, dmkfmBoth=1, dmkfmPrev=2, dmkfmAfter=3, dmkfmNome=4);
  TdmkSequenceFilter = (dmkifNone=0, dmkifRange=1, dmkifInterleaved=2);

(*  Problemas na compilacao do dmkNFSe
  //TSinal = (siPositivo=0, siNegativo=1);
  TFileEncodingType = (fetAnsi=0, fetUnicode=1, fetUnicodeBigEndian=2, fetUTF_8=3);
  TDateEncodeType = (detJustSum=0, detLastDaySameMonth=1, detFirstDayNextMonth=2);
*)
  //////////////////////////////////////////////////////////////////////////////
  // Tipos enumerados com a ordem definida e fixa                             //
  // Cuidado com a ordem!!!                                                   //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  TAgendaQuestao = (qagIndefinido, qagAvulso, qagOSBgstrl);                   //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  TTypTabAtr = (ttaNenhum, ttaPreDef, ttaTxtFree);                            //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  TAgendaAfazer = (aagIndefinido, aagVistoria, aagExecucao, aagEscalonamento, //
  (*colocar outros aqui! ...*)                                                //
  (* deixar aagDesconhecido por último!!!! >>>>>*)aagDesconhecido);           //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  TIDFinalidadeLct = (idflIndefinido, idflProprios, idflCondominios,          //
  idflCursosCiclicos);                                                        //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  TTipoReceitas = (dmktfrmSetrEmi_INDEFIN, dmktfrmSetrEmi_MOLHADO,            //
    dmktfrmSetrEmi_ACABATO(*... CO_SetrEmi_...*));                            //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  TTipoSourceMP = (dmktfrmSourcMP_INDEFIN=0, dmktfrmSourcMP_Ribeira=1,        //
    dmktfrmSourcMP_WetSome=2, dmktfrmSourcMP_VS_BH=3, dmktfrmSourcMP_VS_WE=4, //
    dmktfrmSourcMP_VS_FI=5                                                    //
    (*... CO_SetrEmi_...*));  //                                              //
  // Implementar  na procedure TDModG.VerificaVarsToConsts();                 //
  //////////////////////////////////////////////////////////////////////////////
  ///
  TModoSicroWeb = (mswNone, mswUnico, mswParcial, mswTotal);
  TFormaFieldTxt = (fftString, mswInteger, mswDateTime);
  FUploadAbertos = (ulaNada, ulaLct);
  TbgstDoPip = (gstDoPipNone, gstDoPipAtivaSelec, gstDoPipDesativaNaoSelec);
  TdmkModoInclusao = (dmkmiIndefinido, dmkmiAutomatico, dmkmiUserDefine);
  TDefTruOrFalse = (deftfFalse, deftfTrue, deftfInverse);
  TDisposicao = (dispIndefinido, dispPorLugar, dispPorOpcao);
  TWndCalled = (wcForm, wcService);
  TNomeTab = (ntLct, ntAri, ntCns, ntPri, ntPrv);
  TTipoCfgRel = (tcrBalanceteLoc, tcrBalanceteWeb);
  TMaiEnvType = (meAvul=0, meNFSe=1, meNFe=2, meNFeEveCCE=3, meNFeEveCan=4, meBloq=5,
                 meCobr=6, meCTe=7, meMDFe=8, meBloqCnd=9, meHisAlt=10);
  TComoFastReportGera = (cfrgNone, cfrgMostra, cfrgPrepara, cfrgImprime, cfrgSalva, cfrgCompoe);
  TTipoTab = (tt_, ttA, ttB, ttD);
  TdmkNavegadores = (naviIndef, naviIExplore, naviChromium);
  TTipoBoleto = (tbolNenhum, tbolPreBol, tbolBoleto, tbolLctFin);
  TNFSeQuemPagaIss = (qpiPrestador, qpiTomador, qpiIntermediario, qpiADefinir);
  TNFSeTipoDoc = (tdnfseDPS, tdnfseLoteRPS);
  TdmkArrStr = array of String;
  TdmkArrVar = array of Variant;
  MyArrayLista =  array of array of String;
  MyArrayD2   = array[1..2] of Real;
  MyArrayD22  = array[1..22] of Real;
  TTipoPt2Pt = (pt2ptNo, pt2pt60(*, pt2pt99*));
  TNovoCodigo = (ncIdefinido, ncControle, CtrlGeral, ncGerlSeq1);
  //TSinal = (siPositivo, siNegativo);
  TSelStrI = (ssiDif0);
  TSelStrS = (sssDif_);
  epPesoValor = (epPeso, epValor, epNenhum);
  epHistTipo  = (epSintetico, epAnalitico);
  TTipoPagto = (tpCred, tpDeb, tpAny);
  //TTipoSinal = (tsPos, tsNeg, tsDef);
  // 2011-07-01
  //aeAviso     = (aeMsg, aeMemo, aeNenhum);
  TTipoAviso     = (aeMsg, aeMemo, aeNenhum, aeVAR);
  //

  TPosVirgula = (pvNo, pvPre, pvPos);
  TDateOrder  = (doMDY, doDMY, doYMD);
  TGrandeza   = (grandezaNone, grandezaBytes);
  TMskType    = (fmtNone, fmtCPFJ, fmtCEP, fmtTelCurto, fmtTelLongo, fmtNCM,
                 fmtCBO2002, fmtIE, fmtNumRua, fmtUF, fmtVersao, fmtCPFJ_NFe,
                 fmtCEP_NFe, fmtTel_NFe, fmtIE_NFe, fmtChaveNFe);
                                                          // mysql não armazena milisegundos
  TdmkHoraFmt = (dmkhfShort, dmkhfLong, dmkhfMiliSeconds, dmkhfExtended);
  TdmkDataFmt = (dmkdfShort, dmkdfLong);
  //TdmkItensCheckGroup = (icgIndex, icgValor);
  TKeyType    = (ktString, ktBoolean, ktInteger, ktCurrency, ktDate, ktTime, ktDateTime, ktFloat);
  TCalcType   = (ctM2toP2, ctP2toM2, ctKGtoLB, ctLBtoKG);
  TCalcFrac   = (cfCento, cfQuarto);
  TTipoEntiCod = (tecEntidade, tecCliInt, tecFilial);
  TSelectLetraLct   = (sllNenhum, sllLivre,
                       sllSelecA, sllSelecB, sllSelecC, sllSelecD, sllSelecE, sllSelecF,
                       sllForcaA, sllForcaB, sllForcaC, sllForcaD, sllForcaE, sllForcaF,
                       sllNenhuma);
  TStatusFinNovo    = (sfnAntigo, sfnFaltaMigrar, sfnNovoFin);
  TAjustaAMenor     = (aamNone, aamTxt, aamNum);
  TIncrementoPagina = (incrpagNone, incrpagPreto, incrpagCartaz, incrpagFundo,
                       incrpagRefresh, incrpagFirst, incrpagPrior, incrpagNext,
                       incrpagLast, incrpagVar, incrpagSermao, incrpagAvisos);
  TSelType = (istNenhum, istIgnora, istTodos, istSelecionados, istAtual,
    istPergunta, istDesiste, istMarcados, istExtra1, istExtra2);
  TTipoCadNFe = (entNFe_emit, entNFe_dest, entNFe_transp);
  TColetivoUnidadeTempo = (cutDias, cutSemanas, cutMeses, cutAnos);
  TdmkCharCase = (dmkccLowerCase, dmkccUpperCase, dmkccFistUpper);
  TAcessFormModo = (afmSoMaster, afmSoBoss, afmNegarSemAviso, afmNegarComAviso,
                    afmParcial, afmParcialSemEdicao, afmAcessoTotal);
  TQualDiaMes = (qdmFirst, qdmMidle, qdmLast);
  TAcaoMoverRegistro = (amrNothing, amrOnlyIns, amrOnlyDel, amrInsAndDel);
  TAcaoCriaTabela   = (actCreate, actDescribe);
{
  ///// Movidos para o UnDmkEnums //////////////////////////////////////////////
  dmkTbCtrl = (tctrlCad, tctrlAlt, tctrlLok, (*tctrlDel, *)tctrlWeb, tctrlAti);
  TTemControle = set of dmkTbCtrl;
  TSQLType    = (stIns, stUpd, stDel, stCpy, stLok, stUnd, stPsq, stNil);
  TAcessFmModo = (afmoSoMaster, afmoSoAdmin, afmoSoBoss, afmoNegarSemAviso,
                  afmoNegarComAviso, afmoLiberado);
  TAllFormats = dmktfNone .. dmktfUnknown;
  TAllFormat  = (dmktfNone, dmktfString, dmktfDouble, dmktfInteger, dmktfLongint,
                 dmktfInt64, dmktfDate, dmktfTime, dmktfDateTime, dmktfMesAno,
                 dmktf_AAAAMM, dmktf_AAAA_MM,
                 // Deve ser o último:
                 dmktfUnknown);
  TUpdType    = (utYes, utNil, utIdx, utInc);
  ///// FIM Movidos para o UnDmkEnums //////////////////////////////////////////////
}

  TGeral = class(TObject)
  private
    //function ObtemMenorTamanho(Grandeza: TGrandeza; MedidaNormal,
      //MedidaAtual: Double; NomeMedida: String): Boolean;
    {private declaration}
  public
    {public declaration}
    // Mensagens de erro
    //procedure MostraErro(Sender: TObject; E: Exception);
    //function ErroDeSocket(Errno: word; ErrMsg: String): String;
    //procedure ProcessProperty(Sheet: Variant; Row, Col: integer);
    function  GET_Compo_Val(const Form: TForm; const Objeto: TObject; const Nome, Propriedade:
              String; const Classe: TClass; var Valor: Variant): Boolean;
    procedure MensagemDeErro(Mensagem, QuemChamou: String);
    function  MensagemBox (Text, Caption: WideString; Typ: Integer; TempoCiclo: Integer = 0): Integer;
    function  MB_Aviso(Text: WideString): Integer;
    function  MB_Erro(Text: WideString): Integer;
    function  MB_Info(Text: WideString): Integer;
    function  MB_Pergunta(Text: WideString): Integer;
    function  MB_SQL(Form: TForm; Qry: TDataSet): Integer; overload;
    function  MB_SQL(Form: TForm; Qry: TComponent): Integer; overload;
    function  MB_Teste(Text: WideString): Integer;
    procedure MB_Aguarde(Text: WideString; TempoCiclo: Integer = 1000);
    procedure MensagemArray(Tipo: Integer; Titulo: String; Campos,
              Valores: array of String; Memo: TMemo);
    procedure GetFontSizes(FontName: String; const Items: TStrings);
    // Windows
    function  ReadKey(const Key, Path: String; KeyType: TKeyType; DefValue:
              Variant; xHKEY: HKEY) : Variant;
    function  ReadAppKey(const Key, AppTitle: String; KeyType: TKeyType; DefValue:
              Variant; xHKEY: HKEY): Variant;
    procedure WriteAppKey(const Key, App: String; const Value: Variant; KeyType:
              TKeyType; xHKEY: HKEY);
    function  ReadAppKeyCU(const Key, AppTitle: String; KeyType:
              TKeyType; DefValue: Variant) : Variant;
    function  ReadAppKeyLM(const Key, AppTitle: String; KeyType:
              TKeyType; DefValue: Variant) : Variant;
    function  ReadAppKeyLM_Net(const NomeServ, Key, AppTitle: String; KeyType: TKeyType;
              DefValue: Variant; Retorno: Variant): Boolean;
    procedure WriteAppKeyCU(const Key, App: String; const Value: Variant; KeyType:
              TKeyType);
    function  DelKeyAppKeyCU(const App: String): Boolean;
    function  DelValAppKeyCU(const App, Key: String): Boolean;
    function  ReadAppKeyMULTI_SZ(const Key, AppTitle: String; DefValue: Variant) : Variant;
    procedure WriteAppKeyMULTI_SZ(const Key, App: String; const Value: Variant);
    function  WriteAppKeyLM_Net(const Server, Key, App: String; const Value: Variant;
              KeyType: TKeyType): Boolean;
    procedure WriteAppKeyLM(const Key, App: String; const Value: Variant; KeyType:
              TKeyType);
    procedure WriteAppKeyLM2(const Key, App: String; const Value: Variant; KeyType:
              TKeyType);
    procedure WriteAppKey_Total(const Key, App: String; const Value: Variant; KeyType:
              TKeyType; xHKEY: HKEY);
    function  GiveEveryoneFullAccessToRegistryKey(const RootKey: HKey;
              const RegPath: String; out ErrorMsg: string): Boolean;
    procedure AbrirAppAuxiliar(IP: String; DMKID_APP: Integer; ParamStr1: String = ''; ParamStr2: String = '');//Abre aplicativos auxiliares DermaBK, DermaAD2,...
    function  PastaEspecial(CodPasta: Integer): String;
              // HD em número Integer
    function  GetVolumeSerialNumber(EhOServidor: Boolean; CaminhoServ: String): Int64;
              // HD em número Hexa
    function  SerialNum(FDrive: String): String;
    function  ObtemIP(Retorno: Integer): String;
    function  SplitIPv4(const IPv4: String; var p1, p2, p3, p4: String): Boolean;
    function  IndyComputerName(): string;
    function  GetParentFrom(AControl: TWinControl): TCustomForm;
    // Arquivos
    function  FileSize(fileName : wideString) : Int64;
    function  FileVerInfo(FileName: String; Info: Integer): String;
    function  SalvaTextoEmArquivo(Arquivo, Texto: WideString; Exclui:
              Boolean; FileEncodingType: TFileEncodingType = fetAnsi): Boolean;
    function  SalvaTextoEmArquivoEAbre(Arquivo, Texto: WideString; Exclui:
              Boolean): Boolean;
    function  AbreArquivo(Arquivo: String; Executa: Boolean = False): Boolean;
    function  LeArquivoToMemo(Arquivo: String; Memo: TMemo): Boolean;
    function  LeArquivoToString(const Arquivo: String;
              var TextoDoArquivo: String): Boolean;
    // Numeros
    function  GetNumIntsFmt(Digitos: Integer): String;
    function  IMV(Texto: String): Integer;
    function  IMV_Var(Texto: Variant): Integer;
    function  I64(Texto: String): Int64;
    function  I64MV(Texto: String) : Int64;
    function  TFT(Texto: String; Casas: Integer; Sinal: TSinal): String;
    function  TFT_Dot(Texto: String; Casas: Integer; Sinal: TSinal) : String;
    function  TFT_MinMax(Texto: String; Min, Max: Extended; Casas: Integer): String;
    function  TFT_Min(Texto: String; Min: Double; Casas: Integer): String;
    function  TFT_Max(Texto: String; Max: Double; Casas: Integer): String;
    function  TFT_NULL(Texto: String; Casas: Integer; Sinal: TSinal) : String;
    function  TFD(Texto: String; Digitos: Integer; Sinal: TSinal) : String;
    function  DFT_SPED(Valor: Double; Casas: Integer; Sinal: TSinal): String;
    function  TFT_NTS(Texto: String; Casas: Integer; Sinal: TSinal): String;
    function  DMV(Texto: String): Double;
    function  DMV_Dot(Texto: String): Double;
    function  IntInConjunto(Numero, Conjunto: Integer): Boolean;
    function  FF_0(Numero, Tamanho: Integer): String; overload;
    function  FF_0(Numero: Int64; Tamanho: Integer): String; overload;
    function  FF0(Numero: Integer): String; overload;
    function  FF0(Numero: Int64): String; overload;
    function  FF0_EmptyZero(Numero: Integer): String;
    function  FFN_Dot(Numero: Double): String;
    function  FI64(Numero: Int64): String;
    function  FFI(Numero: Double): String;
    function  FFF(Numero: Double; Digitos: Integer): String;
    function  FFN(Numero, Digitos: Integer): String;
    function  FFN_Inv(Numero, Digitos: Integer): String;
    function  FTX(Numero: Double; Inteiros, Casas: Integer; Sinal: TSinal) : String;
    function  FFT(Numero: Double; Casas: Integer; Sinal: TSinal) : String;
    function  FFT_Dot(Numero: Double; Casas: Integer; Sinal: TSinal) : String;
    procedure  AdequaMedida(const Grandeza: TGrandeza; const MedidaAtual:
              Double; const NivelAtual: Integer; var NovaMedida: Double;
              var NovoNivel: Integer; var NomeMedida: String);//: Boolean;
    function  FFT_Null(Numero: Double; Casas: Integer; Sinal: TSinal) : String;
    function  FFT_MinMaxCasas(Numero: Double; CasasMin, CasasMax: Integer): String;
    function  ConverteValor(Valor: Double; CalcType: TCalcType; CalcFrac: TCalcFrac): Double;
    function  ConverteArea(Area: Double; CalcType: TCalcType; CalcFrac: TCalcFrac): Double;
    function  ConvertePeso(Peso: Double; CalcType: TCalcType; CalcFrac: TCalcFrac): Double;
    function  QuadriVal(Valor: Double): Double;
    function  EscolheMenorDe3_Int(const Val1, Val2, Val3: Integer;
              var Valor, Index: Integer): Boolean;
    function  EscolheMaiorDe3_Int(const Val1, Val2, Val3: Integer;
              var Valor, Index: Integer): Boolean;
    function  BoolToStr(Verdade: Boolean): String;
    function  BoolToNumStr(Verdade: Boolean): String;
    function  BoolToInt(Verdade: Boolean): Integer;
    function  StrToBool(Verdade: String): Boolean;
    function  NumStrToBool(Verdade: String): Boolean;
    function  IntToBool(Verdade: Integer): Boolean;
    function  RoundC(Numero: Double; Casas: Byte): Double;
    function  RoundG(Numero, Gap: Double): Double;
    procedure ComparaRoundC(ValAtual, Numero: Double; Casas: Integer);
    procedure ComparaRoundG(ValAtual, Numero, Gap: Double);
    function  IntToBool_0(Numero: Integer): Boolean;
    function  SoNumeroEPonto_TT(Texto : String) : String;
            //SoNumeroE2Pontos_TT > SoHora_TT !
    function  SoHora_TT(Texto: String; MudaPtPara2Pt: TTipoPt2Pt): String;
    function  Periodo2000(Data: TDateTime) : Integer;
    function  DecimalPonto(sValor: string): string;
    function  STD_109(dh: string): TDateTime;
    function  STD_001(dh: string): TDateTime;
    function  STD_SPED(dh: string): TDateTime;
    function  IntInArray(Inteiro: Integer; ArrayInteiros: array of Integer): Boolean;



    // Data / Hora
    procedure DefineFormatacoes;
    function  DoEncodeDate(Year, Month, Day: Word; var Date: TDateTime): Boolean;
    function  EhDataBR(): Boolean;
    function  ValidaDataBR(Texto: String; PermiteZero: Boolean;
              ForceNextYear: Boolean; MostraMsg: Boolean = True): TDateTime;
    function  ValidaData8Num(Texto: String; AceitaZero, EhBR: Boolean): TDateTime;
    function  ValidaData4Num_SemAno(Texto: String; Ano: Integer; AceitaZero,
              EhBR: Boolean): TDateTime;
    function  ValidaData_YYYY_MM_DD(Data: Variant): TDateTime;
    function  DTP(Data: TDateTime) : Integer; //Periodo2000 - DataToPeriodo
    //function  DTP2(Data: TDateTime) : Integer; //Periodo2000 - DataToPeriodo
    function  DTAM(Data: TDateTime) : Integer; //AnoMes
    function  MTD(Data: String; Obrigatorio: Boolean): TDateTime; // Mensal To DAte
    function  MTD2(Data: String; Obrigatorio: Boolean): TDateTime; // Mensal To DAte
    function  MTP(Data: String; Obrigatorio: Boolean): Variant; // MensalToPeriodo
    function  MTP2(Data: String; Obrigatorio: Boolean): Variant; // MensalToPeriodo
    function  TST(Data: String; Obrigatorio: Boolean): String;
    function  PTM(Periodo: Integer): String; // PeriodoToMensal
    function  PTD(Periodo, DiaX: Integer): TDateTime;  // PeriodoToDate
    function  ScanDate(const S: string; var Pos: Integer;
              var Date: TDateTime): Boolean;
    function  FDT(Data: TDateTime; Tipo: Integer; Nulo: Boolean = False): String;
    function  FDT_Aspas(Data: TDateTime; Tipo: Integer; Nulo: Boolean = False): String;
    function  FDT_AAAAMM(AAAAMM, Tipo: Integer; Nulo: Boolean = False): String;
    function  AAAAMM_To_Date(AAAAMM: Integer; QualDiaMes: TQualDiaMes; Dia: Word = 0): TDateTime;
    function  FDT_TP_Ed(Data: TDateTime; Hora: String; Nulo: Boolean = False): String;
    function  NowTxt(): String;
    function  Data1(Data: TDate): TDate;
    function  PrimeiroDiaDoMes(Data: TDate): TDate;
    function  UltimoDiaDoMes(Data: TDate): TDate;
    function  ObtemAnoMesDeMesStrAno(const Texto: String;
              var Ano, Mes: Integer): Boolean;
    function  IncrementaMes_AnoMes(AnoMes, Meses: Integer): Integer;
    function  IncrementaMes_Ano_Mes(Ano_Mes: String; Meses: Integer): String;
    function  MontaDataPorFmt(const Valor, Fmt: String; const PermiteZero:
              Boolean; var Data: TDateTime): Boolean;
    function  MontaValDblPorFmt(const Valor: String; const Casas: Integer; var
              ValDbl: Double): Boolean;
    function  DataToMez(Data: TDateTime): String;
    function  AnoEMesToMez(Ano, Mes: Integer): Integer;
    function  MesBarraAnoToMez(MesBarraAno: String): String;
    function  ValidaMesAno(Texto: String):TDateTime;
    function  AnoEMesDoMez(Mez: String): String;
    function  MesEAnoDoMez(Mez: String): String;
    function  ValidaDataHoraSQL(Texto: String): TDateTime;
    function  ValidaDataSimples(Texto: String; PermiteZero: Boolean): TDateTime;
    function  ObtemHoraDeDateTime(DataHora: TDateTime): TTime;
    function  STT(Hora, hInc: String; PermiteMaisQ24h: Boolean): TTime;
    function  MesIntDeMesStr3Letras(Sigla: String): Word;
    function  StrToTimeZZZ(Str: String): TTime;
    // Miscelânea
    function  VariantToString(AVar: OleVariant): string;
    function  NomeVarType(VarType: TVarType): String;
    function  WideStringToSQLString(Texto: WideString): WideString;
    function  SiglasWebService(): MyArrayLista;
    function  SelecionaItem(Lista: MyArrayLista; Coluna: Integer; TituloForm:
              String; TitulosColunas: array of String; ScreenWidth: Integer): String;
    function  SelecionaItemLista(Lista: array of String; Coluna: Integer; TituloForm:
              String; TitulosColunas: array of String; ScreenWidth: Integer): String;
    function  CriaSelLista(Lista: MyArrayLista; TituloForm: String;
              TitulosColunas: array of String; ScreenWidth: Integer): Boolean;
    function  CriaSelListaArray(Lista: array of String; TituloForm: String;
              TitulosColunas: array of String; ScreenWidth: Integer): Boolean;
    function  TiraAspasDeTexto(Texto: WideString): WideString;
    function  RichRow(m: TCustomMemo): Longint;
    function  RichCol(m: TCustomMemo): Longint;
    function  FIV(FaltaInfoVariavel: Boolean; Mensagem: String): Boolean;
    function  VerificaDir(var Dir: String; const Barra: Char;
              const Msg: String; ForcaCriar: Boolean): Boolean;
    function  TamanhoArquivo(Arquivo: String; AvisaSeNaoExiste: Boolean): Int64;
    function  SelStrI(Tipo: TSelStrI; Inteiro: Integer; TextoTrue: String;
              ResultFalse: String = ''): String;
    function  SelStrS(Tipo: TSelStrS; CompareStr, TextoTrue: String;
              ResultFalse: String = ''): String;
    function  EncodeDateSafe(Ano, Mes, Dia: Word; DateEncodeType:
              TDateEncodeType): TDateTime;
    function  AnoEMesDoPeriodo(const Periodo: Integer; var Ano, Mes: Integer): Boolean;
    function  PeriodoToDate(Periodo, DiaX: Integer; Safe: Boolean;
              DateEncodeType: TDateEncodeType = detLastDaySameMonth): TDateTime;//DataXDoPeriodo
    function  AnoMesToData(AnoMes, Dia: Integer): TDateTime;
    function  AnoMesToAnoEMes(const AnoMes: Integer; var Ano, Mes: Integer): Boolean;
    function  AnoMesToPeriodo(AnoMes: Integer): Integer;
    function  AnoEMesToPeriodo(Ano, Mes: Integer): Integer;
    // String
    function  StringToMemoryStream(const Str: String): TMemoryStream;
    function  ATA_Str(const Sorc: array of string): TdmkArrStr;
    function  ATA_Var(const Sorc: array of Variant): TdmkArrVar;
    function  ASA_Str(const Sorc1, Sorc2: array of string): TdmkArrStr;
    function  ASA_Var(const Sorc1, Sorc2: array of Variant): TdmkArrVar;
    function  ATS(ListaSQL: array of string; NotEmpty: Boolean = False): String;
    function  ATS_if(Condicao: Boolean; ListaSQL: array of string): String;
    function  TSTA(Itens: TStrings): TdmkArrStr;
    function  MyExtractStrings(Separators, WhiteSpace: TSysCharSet; Content: PChar;
              Strings: TStrings): Integer; // copiado de classes  e modificado
    function  Substitui(Txt, ant, new: string) : string;
    function  SeparaEndereco(const Linha: Integer; const Endereco: String;
              var Numero, Compl, Bairro, xLogr, nLogr, xRua: String;
              TemBairro: Boolean = True;
              Memo: TMemo = nil; Grade: TStringGrid = nil): Boolean;
    procedure SeparaLogradouro(const Linha: Integer; const Logr: String;
              var Sep1, Sep2, Sep3: String;
              Memo: TMemo = nil; Grade: TStringGrid = nil);
    function  SeparaNomes(Texto: String): String;
    function  SeparaPrimeiraOcorrenciaDeTexto(const Separador, Texto: String;
              var PrimeiraOcorrencia, NovoTexto: String): Boolean;
    function  AddStrToStr_Separador(const NewStr, Separador: String; var
              Destino: String): Boolean;
    function  ConsertaTexto_Novo(const Texto: String; var Seq: Integer): String;
    function  CompletaString(Texto, Compl: String; Tamanho: Integer;
              Alinhamento: TAlignment; Corrige: Boolean): String;
    function  CompletaString2(Texto, Compl: String; Tamanho: Integer;
              Alinhamento: TAlignment; Corrige: Boolean; Complemento: String): String;
    function  VariavelToString(Variavel: Variant): String;
    function  SemAcento(Txt: String) : string;
    function  Maiusculas(Txt: String; DefUser: Boolean) : string;
    function  Minusculas(Txt: String; DefUser: Boolean) : string;
    function  EhMinusculas(Texto: String; Forca: Boolean): Boolean;
    function  ColetivoDeUnidadedeTempo(Quantidade: Integer; TipoPeriodo:
              TColetivoUnidadeTempo; dmkCharCase: TdmkCharCase): String;
    function  ObtemTextoDeAllFormat(Format: Integer): String;
    function  FNE(Pre: String; Codigo: Integer; Pos: String; Casas: Integer = 4): String;
    //function ItensDe_dmkCheckGroup(CG: TdmkCheckGroup; Its: TdmkItensCheckGroup;
      //       VerificaZero: Boolean): String;
    function  ReduzNomeDeCaminhoDeArquivo(Arquivo: String; MaxTam: Integer): String;
    function  Igual(A, B: String): Boolean;
    function  MyUTF(Texto: String): String;
    function  MyNPS(Texto: String): String;
    function  TxtSemPipe(Txt: String): String;
    function  ConvertDecimalCharactersText(const Texto: String; var Res: String): Boolean;
    function  IsInterleavedText(const Str: String; var Res: Boolean): TStringList;
    function  CountOccurences(const SubText: string; const Text: string): Integer;
    function  Aspas(Texto: String): String;

    // Formatações
    function  FormataCNAE21(CNAE: String): String;
    function  FormataLC11603(Servico: String): String;
    function  FormataCBO2002(CBO2002: String): String;
    function  FormataCEP_TT(CEP: String; Separador: String = '-';
              Default: String = ''): String;
    function  FormataCEP_NN(CEP: Integer): Integer;
    function  FormataCEP_NT(CEP: Real): AnsiString;
    function  FormataCFOP(CFOP: String): String;
    function  FormataCNPJ_TT(Texto: AnsiString): AnsiString;
    function  FormataCNPJ_TT_NFe(Texto: String): AnsiString;
    function  Formata_IE(IE: String; UF: Variant; LinkMsk: String;
              Tipo: Integer = 0): String;
    function  FormataNCM(NCM: String): String;
    function  FormataNumeroDeRua(Rua: AnsiString; Numero: Variant; Nulo: Boolean): AnsiString;
    procedure ArrumaTelefoneBR_SemZero(const Source: String; var Dest: String);
    function  PareceTelefoneBR(Telefone: String): Boolean;
    function  FormataTelefone_TT(Telefone : AnsiString) : AnsiString;
    function  FormataTelefone_TT_NFe(Telefone : String) : String;
    function  FormataTelefone_TT_Curto(Telefone : AnsiString) : AnsiString;
    function  FormataUF(UF: Variant): String;
    function  FormataVersao(Versao: Variant): String;
    function  FormataVersaoNFe(Versao: Variant): String;
    function  FormataChaveNFe(ChaveNFe: String; Como: TComoFmtChNFe): String;
    //  Usado para formatações
    function  SoNumero1a9_TT(Texto : String) : String;
    function  SoNumeroESinal_TT(Texto : String) : String;
    function  SoNumeroESinalEVirgula_TT(Texto: String) : String;
    function  SoNumeroEVirgula_TT(Texto: String) : String;
    function  SoNumero_TT(Texto : String) : String;
    function  SoNumeroELetra_TT(Texto : String) : String;
    function  SoLetra_TT(Texto : String) : String;
    function  SoDecimalCharacter(const Texto: String): Boolean;
    function  SoNumeroEData_TT(Texto : String) : String;
    function  LimpaNumeroTexto(Texto: String): String;
    function  LimpaNumeroTexto_Dot(Texto: String): String;
    function  FormataCNPJ_TFT(Texto: AnsiString): AnsiString;
    function  CalculaCNPJCPF(Nume : AnsiString) : AnsiString;
    function  TipoDocCNPJCPF(Numero: String): String;
    function  GetCodigoUF_da_SiglaUF(SiglaUF: String): Integer;
    function  SiglaUFValida(SiglaUF: String): Boolean;
    function  GetSiglaUF_do_CodigoUF(CodigoUF: Integer): String;
    function  GetEstadoDaSiglaDaUF(UF: String): String;
    function  GetSiglaDaUFdoEstado(Estado: String): String;
    function  Valida_IE_SemErrLength(IE, UF: String; ln, lv: Integer):Boolean;
    function  Valida_IE(IE: String; UF: Variant; LinkMsk: String;
              ForcaTrue: Boolean = True): Boolean;
    function  Valida_IE_AC(IE, UF: String): Boolean;
    function  Valida_IE_AL(IE, UF: String): Boolean;
    function  Valida_IE_AP(IE, UF: String): Boolean;
    function  Valida_IE_BA(IE, UF: String): Boolean;
    function  Valida_IE_11(IE, UF: String): Boolean; // Módulo 11 => 10 e 11 = 0; CE, ES, AM
    function  Valida_IE_DF(IE, UF: String): Boolean;
    function  Valida_IE_GO(IE, UF: String): Boolean;
    function  Valida_IE_MA(IE, UF: String): Boolean;
    function  Valida_IE_MG(IE, UF: String): Boolean;
    function  Valida_IE_MT(IE, UF: String): Boolean;
    function  Valida_IE_MS(IE, UF: String): Boolean;
    function  Valida_IE_PA(IE, UF: String): Boolean;
    function  Valida_IE_PB(IE, UF: String): Boolean;
    function  Valida_IE_PE(IE, UF: String): Boolean;
    function  Valida_IE_PI(IE, UF: String): Boolean;
    function  Valida_IE_PR(IE, UF: String): Boolean;
    function  Valida_IE_RJ(IE, UF: String): Boolean;
    function  Valida_IE_RN(IE, UF: String): Boolean;
    function  Valida_IE_RO(IE, UF: String): Boolean;
    function  Valida_IE_RR(IE, UF: String): Boolean;
    function  Valida_IE_RS(IE, UF: String): Boolean;
    function  Valida_IE_SC(IE, UF: String): Boolean;  // Modulo 11
    function  Valida_IE_SE(IE, UF: String): Boolean;
    function  Valida_IE_SP(IE, UF: String): Boolean;
    function  Valida_IE_TO(IE, UF: String): Boolean;
    function  VersaoTxt2006(Versao: Int64): String;
    function  VersaoTxt_Vazio(Versao: Int64): String;
    function  VersaoInt2006(Versao: String): Int64;
    function  VersaoAplicTxt(Versao: Int64): String;
    function  CNPJ_Valida(const EdCNPJ: TWinControl; var CNPJ: String): Boolean;
    function  ValidaEMail(const Value: string): Boolean;
    function  RemoveTagsHTML(const s: String): String;
    function  JsonText(Texto: String): String;
    function  JsonBool(Verdade: Boolean): String;
    // Modulos
    function  ModuloEAN13(Numero: String): String;
    function  Modulo10_1e2_9_Back(Numero: String): String;
    function  Modulo10_2e1_X_Back(Numero: String; Tipo: Integer): String;
    function  Modulo11_2a9_Back(Numero: String; Tipo: Integer): String;
    function  Modulo11_2a7_Back(Numero: String; Tipo: Integer): String;
    // IBGE
    function  CodigoUF_to_cUF_IBGE(UF: Integer): Integer;
    function  GetCodigoUF_IBGE_DTB_da_SiglaUF(SiglaUF: String): Integer;
    function  GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF: Integer): String;
    function  ConfereUFeMunici_IBGE(xUF: String; CodMunici: Integer;
              TipoEndereco: String): Boolean;
    // Listas (Tabelas)
    procedure AddLin(var Res: MyArrayLista; var Linha: Integer;
              const Codigo: String; Descricao: array of String);
    function  DescricaoDeArrStrStr(const Codigo: String; const ArrTextos:
              MyArrayLista; var Texto: String; ColLoc, ColSel: Integer): Boolean;
    function  DescricaoDeArrStrStr2(const Codigo: String; const ArrTextos:
              MyArrayLista; var Texto1, Texto2: String; ColLoc, ColSel1, ColSel2: Integer): Boolean;
    function  DescricaoDeArrStrStr3(const Codigo: String; const ArrTextos:
              MyArrayLista; var Texto1, Texto2, Texto3: String;
              ColLoc, ColSel1, ColSel2, ColSel3: Integer): Boolean;
    function  DescricaoEAvisosDeArrStrStr(const Codigo: String; const ArrTextos,
              ArrAvisos: MyArrayLista; var Texto, Aviso: String; ColLocTexto,
              ColSelTexto, ColSelLocAv, ColLocAviso, ColSelAviso: Integer): Integer;
    function  DescricaoEAvisosEItensDeArrStrStr(const Codigo: String;
              const ArrTextos, ArrAvisos: MyArrayLista; var Texto, Aviso, Item1, Item2: String;
              ColLocTexto, ColSelTexto, ColSelLocAv, ColLocAviso, ColSelAviso, CodItem1,
              CodItem2: Integer): Integer;
    function  SINTEGRA_ExpNat(): MyArrayLista;
    function  SINTEGRA_ExpConhTip(): MyArrayLista;
    // ACBr
    function  SeSenao(ACondicao: Boolean; ATrue, AFalse: Variant): Variant;
    function  PosLast(const SubStr, S: AnsiString): Integer;
    function  PosEx(const SubStr, S: AnsiString;
              Offset: Cardinal): Integer;
    function  Implode(Str: TStringList; Delimitador: String): String;
    function  Explode(Str, Separador: String; TamMin: Integer = 0): TStringList;
    function  Explode3(Str, Separador: String; TamMin: Integer = 0): TStringList;
    function  ValidaNVE(var NVE: String): Boolean;
    procedure LimpaValueVariant(Componente: TWinControl);
    function  ide_mod_to_FatID(const ide_mod: Integer; var FatID: Integer):
              Boolean;
    function  FatID_to_ide_mod(const FatID: Integer; var ide_mod: Integer):
              Boolean;
    function  ide_mod_de_ChaveNFe(const ChaveNFe: String; var ide_mod: Integer):
              Boolean;
    procedure ErroVersaoDoCompilador(ProcMessage, E_Message: WideString);
    procedure LarguraAutomaticaGrade(AGrid: TStringGrid; ColIni: Integer = 1);


//    function SINTEGRA_PaisesSISCOMEX(): MyArrayLista;
  end;

var
  Geral: TGeral;
  APP_VERSAO: String;
  GERAL_MODELO_FORM_ENTIDADES: TFormCadEnti = fmcadSelecionar;
  FontSizes: TStrings;

//const
  //cTemControleSim: TTemControle = ([tctrlLok, tctrlCad, tctrlAlt, tctrlWeb, tctrlAti]);
  //cTemControleNao: TTemControle = ([tctrlWeb, tctrlAti]);
  //
//const
  // Tipos de estoque
  //TTipoEstq   = (
  //dmkteBalanco = 0; //VAR_FATID_0000


implementation

uses dmkSelLista, dmkMsg;

const
  TextString_Com : string = 'áéíóúüâêôãõàçÁÉÍÓÚÜÂÊÔÃÕÀÇ²³ºª&';
  TextString_Sem : string = 'aeiouuaeoaoacAEIOUUAEOAOAC23oaE';


function TGeral.DoEncodeDate(Year, Month, Day: Word; var Date: TDateTime): Boolean;
var
  I: Integer;
  DayTable: PDayTable;
begin
  Result := False;
  DayTable := @MonthDays[IsLeapYear(Year)];
  if (Year >= 1) and (Year <= 9999) and (Month >= 1) and (Month <= 12) and
    (Day >= 1) and (Day <= DayTable^[Month]) then
  begin
    for I := 1 to Month - 1 do Inc(Day, DayTable^[I]);
    I := Year - 1;
    Date := I * 365 + I div 4 - I div 100 + I div 400 + Day - DateDelta;
    Result := True;
  end;
end;

function TGeral.SoNumeroESinal_TT(Texto : String) : String;
var
  i:Integer;
begin
  if Trim(Texto) = '' then Result := '' else
  begin
    if Texto[1] = '-' then
      Result := '-'
    else
      Result := '';
    for i := 1 to Length(Texto) do
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

function TGeral.LimpaNumeroTexto(Texto: string): String;
var
  i: Integer;
  c: Boolean;
  Numero: String;
begin
  c := False;
  Numero := '';
  for i := Length(Texto) downto 1 do
  begin
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], ['0'..'9']) then Numero := Texto[i] + Numero
    else if Texto[i] = FormatSettings.ThousandSeparator then
    begin
      // apenas limpa
    end else if Texto[i] = FormatSettings.DecimalSeparator then
    begin
      if (i < Length(Texto)) and (c=False) then
      begin
        Numero := FormatSettings.DecimalSeparator + Numero;
        c := True;
      end;
    end else if (i=1) and (Texto[i] = '-') then
    begin
      if Length(Numero) > 0 then
      if CharInSet(Numero[i], ['0'..'9']) then Numero := '-' + Numero;
{$ELSE}
    if Texto[i] in (['0'..'9']) then Numero := Texto[i] + Numero
    //else if Texto[i] = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator then
    else if Texto[i] = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator then
    begin
      // apenas limpa
    end else if Texto[i] = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}decimalSeparator then
    begin
      if (i < Length(Texto)) and (c=False) then
      begin
        Numero := {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}decimalSeparator + Numero;
        c := True;
      end;
    end else if (i=1) and (Texto[i] = '-') then
    begin
      if Length(Numero) > 0 then
      if Numero[1] in ['0'..'9'] then Numero := '-' + Numero
{$ENDIF}
    end else begin
      Numero := '0';
      Break;
    end;
  end;
  Result := Numero;
end;

function TGeral.LimpaNumeroTexto_Dot(Texto: string): String;
var
  i: Integer;
  c: Boolean;
  Numero: String;
begin
  c := False;
  Numero := '';
  for i := Length(Texto) downto 1 do
  begin
    //if Texto[i] in ['0'..'9'] then Numero := Texto[i] + Numero
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Numero := Texto[i] + Numero
    else if Texto[i] = FormatSettings.ThousandSeparator then
    begin
      if (i < Length(Texto)) and (c=False) then
      begin
        Numero := FormatSettings.DecimalSeparator + Numero;
        c := True;
      end;
    end else if Texto[i] = FormatSettings.DecimalSeparator then
    begin
      // apenas limpa
    end else if (i=1) and (Texto[i] = '-') then
    begin
      if Length(Numero) > 0 then
      if CharInSet(Numero[i], ['0'..'9']) then Numero := '-' + Numero
{$ELSE}
      if Texto[i] in ['0'..'9'] then Numero := Texto[i] + Numero
    else if Texto[i] = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator then
    begin
      if (i < Length(Texto)) and (c=False) then
      begin
        Numero := {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}decimalSeparator + Numero;
        c := True;
      end;
    end else if Texto[i] = {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}decimalSeparator then
    begin
      // apenas limpa
    end else if (i=1) and (Texto[i] = '-') then
    begin
      if Length(Numero) > 0 then
      if Numero[1] in ['0'..'9'] then Numero := '-' + Numero
{$ENDIF}
    end else begin
      Numero := '0';
      Break;
    end;
  end;
  Result := Numero;
end;

procedure TGeral.LimpaValueVariant(Componente: TWinControl);
var
  FormatType: TAllFormat;
begin
  FormatType := GetPropValue(Componente, 'FormatType', False);
  //
  case FormatType of
    dmktfDouble, dmktfInteger, dmktfLongint, dmktfInt64, dmktfDate,
    dmktfTime, dmktfDateTime:
      SetPropValue(Componente, 'ValueVariant', 0);
    else
      SetPropValue(Componente, 'ValueVariant', '');
  end;
end;

function TGeral.IMV(Texto: String): Integer;
var
  Num: String;
  i: Integer;
begin
  Result := 0;
  try
    if Trim(Texto) = '' then Texto := '0';
    Num := '';
    for i := 1 to Length(Texto) do
    begin
      if (Texto[i] = '-') and (i = 1) then Num := '-';
      //if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Num := Num + Texto[i];
{$ELSE}
      if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$ENDIF}
    end;
    if (Texto = '') or (Texto = '-') then
      Result := 0
    else
      Result := StrToInt(Num);
  except
    on EConvertError do
    begin
      Geral.MB_Erro('O texto "' + Texto + '" não é um número inteiro válido!' +
      sLineBreak + 'Geral.IMV()');
      Result := 0;
      //raise;
      raise EAbort.Create('');
    end;
  end;
end;

function TGeral.IMV_Var(Texto: Variant): Integer;
begin
  if Texto = Null then
    Result := 0
  else
    Result := IMV(Texto);
end;

function TGeral.IncrementaMes_AnoMes(AnoMes, Meses: Integer): Integer;
var
  Ano, Mes: Integer;
begin
  //Ano := AnoMes div 100;
  //Mes := AnoMes mod 100;
  AnoMesToAnoEMes(AnoMes, Ano, Mes);
  Mes := Mes + Meses;
  while Mes > 12 do
  begin
    Ano := Ano + 1;
    Mes := Mes - 12;
  end;
  while Mes < 1 do
  begin
    Ano := Ano - 1;
    Mes := Mes + 12;
  end;
  Result := (Ano * 100) + Mes;
end;

function TGeral.IncrementaMes_Ano_Mes(Ano_Mes: String; Meses: Integer): String;
var
  Data: TDateTime;
begin
  Result := Ano_Mes;
  Data := Geral.MTD2(Ano_Mes, True);
  Data := IncMonth(Data, Meses);
  Result := FormatDateTime('YYYY-MM', Data);
end;

function TGeral.IndyComputerName(): string;
var
  i: LongWord;
begin
  SetLength(Result, MAX_COMPUTERNAME_LENGTH + 1);
  i := Length(Result);
  if GetComputerName(@Result[1], i) then
  begin
    SetLength(Result, i);
  end;
end;

function TGeral.IntInArray(Inteiro: Integer;
  ArrayInteiros: array of Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := Low(ArrayInteiros) to High(ArrayInteiros) do
  begin
    if Inteiro = ArrayInteiros[I] then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TGeral.IntInConjunto(Numero, Conjunto: Integer): Boolean;
begin
  Result := Conjunto and Numero > 0;
end;

function TGeral.I64(Texto: String): Int64;
var
  Num: String;
  i: Integer;
begin
  try
    if Trim(Texto) = '' then Texto := '0';
    Num := '';
    for i := 1 to Length(Texto) do
    begin
      if (Texto[i] = '-') and (i = 1) then Num := '-';
      //if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Num := Num + Texto[i];
{$ELSE}
      if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$ENDIF}
    end;
    if (Texto = '') or (Texto = '-') then
      Result := StrToInt64('0')
    else
      Result := StrToInt64(Num);
  except
    //Result := StrToInt64('0');
    raise;
  end;
end;

function TGeral.I64MV(Texto: String): Int64;
var
  Num: String;
  i: Integer;
begin
  try
    if Trim(Texto) = EmptyStr then Texto := '0';
    Num := EmptyStr;
    for i := 1 to Length(Texto) do
    begin
      if (Texto[i] = '-') and (i = 1) then Num := '-';
      //if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Num := Num + Texto[i];
{$ELSE}
      if Texto[i] in ['0'..'9'] then Num := Num + Texto[i];
{$ENDIF}
    end;
    if (Texto = '') or (Texto = '-') then Result := 0 else
    Result := StrToInt64(Num);
  except
    raise EAbort.Create('O texto '+Texto+' é um número inválido.');
    Result := 0;
  end;
end;

function TGeral.ide_mod_de_ChaveNFe(const ChaveNFe: String;
  var ide_mod: Integer): Boolean;
begin
  ide_mod := IMV(Copy(ChaveNFe, 21, 2));
  Result := ide_mod in ([55,65]);
end;

function TGeral.ide_mod_to_FatID(const ide_mod: Integer; var FatID: Integer): Boolean;
const
  sProcName = 'TGeral.ide_mod_to_FatID()';
begin
  case ide_mod of
    55: FatID := 1;
    65: FatID := 2;
    else
    begin
      FatID := 0;
      MB_Erro('ide_mod não implementado em ' + sProcName);
    end;
  end;
  Result := FatID <> 0;
end;

function TGeral.Igual(A, B: String): Boolean;
begin
  Result := AnsiUppercase(A) = AnsiUppercase(B);
end;

function TGeral.TFT(Texto: String; Casas: Integer; Sinal: TSinal) : String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Double;
begin
  TxtForma := '#,###,##0';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    for i := 1 to Casas do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  Resultado := FormatFloat(TxtForma, Flutuante);
  if (Sinal = siPositivo) and (Resultado[1] = '-') then
  begin
    Resultado := Copy(Resultado, 2, Length(Resultado)-1);
    if Resultado = '' then Resultado := '0';
  end;

  Result := Resultado;
end;

function TGeral.TFT_NTS(Texto: String; Casas: Integer; Sinal: TSinal): String;
var
  Aux: String;
begin
  Aux := TFT(Texto, Casas, Sinal);
{$IFDEF DELPHI12_UP}
  while pos(FormatSettings.ThousandSeparator, Aux) > 0 do
    Delete(Aux, pos(FormatSettings.ThousandSeparator, Aux), 1);
{$ELSE}
  while pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, Aux) > 0 do
    Delete(Aux, pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, Aux), 1);
{$ENDIF}
  Result := Aux;
end;

function TGeral.TFT_NULL(Texto: String; Casas: Integer; Sinal: TSinal): String;
var
  Valor: String;
begin
  Valor := TFT(Texto, Casas, Sinal);
  if Geral.DMV(Valor) = 0 then
    Result := ''
  else
    Result := Valor;
end;

function TGeral.EhDataBR(): Boolean;
begin
  Result := Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'D';
end;

function TGeral.EscolheMenorDe3_Int(const Val1, Val2, Val3: Integer; var Valor,
  Index: Integer): Boolean;
begin
  Index := 1;
  if Val2 < Val1 then
  begin
    if Val2 <= Val3 then
      Index := 2
    else
      Index := 3;
  end else if
    Val3 < Val1 then
      Index := 3;
  case Index of
    1: Valor := Val1;
    2: Valor := Val2;
    3: Valor := Val3;
  end;
  Result := True;
end;

function TGeral.Implode(Str: TStringList; Delimitador: String): String;
var
  I: Integer;
begin
  Result := '';
  //
  if Str.Count > 0 then
  begin
    for I := 0 to Str.Count - 1 do
    begin
      Result := Result + Str[I];
      //
      if I <> (Str.Count - 1) then
        Result := Result + Delimitador;
    end;
  end;
end;

function TGeral.Explode(Str, Separador: String; TamMin: Integer = 0): TStringList;
var
  Sep: TSysCharSet;
  SepTxt: AnsiString;
  I: Integer;
begin
  Sep    := [];
  SepTxt := Separador;
  Result := TStringList.Create;
  try
    for i := 1 to Length(SepTxt) do
      Include(Sep, SepTxt[i]);
    //
    ExtractStrings(Sep, [], PChar(Str), Result);
  finally
    ;
  end;
end;

function TGeral.Explode3(Str, Separador: String; TamMin: Integer = 0): TStringList;
var
  Sep: TSysCharSet;
  SepTxt: AnsiString;
  I: Integer;
begin
  Sep    := [];
  SepTxt := Separador;
  Result := TStringList.Create;
  try
    for I := 1 to Length(SepTxt) do
      Include(Sep, SepTxt[I]);
    //
    MyExtractStrings(Sep, [], PChar(Str), Result);
    if TamMin > 0 then
      while Result.Count < TamMin do
        Result.Add('');
  finally
    ;
  end;
end;

function TGeral.EscolheMaiorDe3_Int(const Val1, Val2, Val3: Integer; var Valor,
  Index: Integer): Boolean;
begin
  Index := 1;
  if Val2 > Val1 then
  begin
    if Val2 >= Val3 then
      Index := 2
    else
      Index := 3;
  end else if
    Val3 > Val1 then
      Index := 3;
  case Index of
    1: Valor := Val1;
    2: Valor := Val2;
    3: Valor := Val3;
  end;
  Result := True;
end;

function TGeral.ValidaData4Num_SemAno(Texto: String; Ano: Integer; AceitaZero,
  EhBR: Boolean): TDateTime;
var
  Dia, Mes: String;
  d, m: Integer;
begin
  if AceitaZero then
  begin
    if (Texto = '')
    or (Texto = '0')
    or (Texto = '0000')
    then
    begin
      Result := 0;
      Exit;
    end;
  end;
  if Length(Texto) <> 4  then
  begin
    MB_Aviso('Data no formato desconhecido[1]!');
    Result := 0;
    //
    Exit;
  end;
  if EhBR then
  begin
    Dia := Copy(Texto, 1, 2);
    Mes := Copy(Texto, 3);
  end
  else
  begin
    Mes := Copy(Texto, 1, 2);
    Dia := Copy(Texto, 3);
  end;
  d := IMV(Dia);
  m := IMV(Mes);
  Result := EncodeDate(Ano, m, d);
end;

function TGeral.ValidaData8Num(Texto: String; AceitaZero, EhBR: Boolean): TDateTime;
var
  Dia, Mes, Ano: String;
  d, m, a: Integer;
begin
  if AceitaZero then
  begin
    if (Texto = '')
    or (Texto = '0')
    or (Texto = '00000000')
    then
    begin
      Result := 0;
      Exit;
    end;
  end;
  if Length(Texto) <> 8  then
  begin
    MB_Aviso('Data no formato desconhecido[1]!');
    Result := 0;
    //
    Exit;
  end;
  if EhBR then
  begin
    Dia := Copy(Texto, 1, 2);
    Mes := Copy(Texto, 3, 2);
    Ano := Copy(Texto, 5);
  end
  else
  begin
    Ano := Copy(Texto, 1, 4);
    Mes := Copy(Texto, 5, 2);
    Dia := Copy(Texto, 7);
  end;
  d := IMV(Dia);
  m := IMV(Mes);
  a := IMV(Ano);
  Result := EncodeDate(a, m, d);
end;

function TGeral.ValidaDataBR(Texto: String; PermiteZero: Boolean;
  ForceNextYear: Boolean; MostraMsg: Boolean = True): TDateTime;

  function ValidaData(var Dia, Mes, Ano : Word): TDateTime;
  var
    DiaAtu, MesAtu, AnoAtu: Word;
  begin
    DecodeDate(Date, AnoAtu, MesAtu, DiaAtu);
    //
    if Mes - MesAtu > 8 then
      Ano := Ano - 1;
    //
    Result := EncodeDate(Ano, Mes, Dia);
  end;

var
  Data: TDateTime;
  Dia, Mes, Ano : Word;
  ADia, AMes, AAno : Word;
  TextChar : PChar;
  i, a, b: Integer;
  Compara: String;
begin
  if Texto = '00:00' then
    Texto := FormatDateTime('dd/mm/yyyy', 0)
  else
  if Texto = '0,00' then
    Texto := FormatDateTime('dd/mm/yyyy', Date);
  //Result := 0;
  try
    AAno := 0;
    Compara := '';
    if PermiteZero then
    begin
      for i := 1 to Length(Texto) do
        Compara := Compara + '0';
      if Texto = Compara then
      begin
        Result := 0;
        Exit;
      end;
    end;
    TextChar := PChar(Texto);
    a := Pos('/', Texto);
    b := Length(Texto);
    if (a = 3) and (b = 5) then
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      Dia := IMV(Copy(Texto, 1, 2));
      Mes := IMV(Copy(Texto, 4, 2));
      Data := EncodeDate(Ano, Mes, Dia);
      if (int(Data) < Int(Date)) and ForceNextYear then
        Data := EncodeDate(Ano + 1, Mes, Dia)
      else
        Data := ValidaData(Dia, Mes, Ano);
      Result := Data;
      //Texto := FormatDateTime('ddmmyyyy', Data);
    end else if a > 0 then
    begin
      Delete(Texto, a, 1);
      Dia := IMV(Copy(Texto, 1, a-1));
      b := pos('/', Texto);
      if b > 0 then Ano := StrToInt(Copy(Texto, b+1, Length(Texto)- b)) else
      begin
        DecodeDate(Date, AAno, AMes, ADia);
        Ano := AAno;
      end;
      if Ano < 100 then
        if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
      if b = 0 then b := Length(Texto) + 1;  
      Mes := StrToInt(Copy(Texto, a, b-a));  
      if (Ano=0) or (Mes=0) or (Dia=0) then
        Result := 0
      else begin
        Data := EncodeDate(Ano, Mes, Dia);
        if AAno > 0 then
        begin
          if (Int(Data) < Int(Date)) and ForceNextYear then
            Data := EncodeDate(Ano + 1, Mes, Dia)
          else
            Data := ValidaData(Dia, Mes, Ano);
        end;
        Result := Data;
      end;
    end else
    if (StrRScan(TextChar,'/') = nil) and
    ((Length(Texto) = 4) or
    (Length(Texto) = 6) or
    (Length(Texto) = 8)) then
    begin
      Dia := StrToInt(Texto[1]+Texto[2]);
      Mes := StrToInt(Texto[3]+Texto[4]);
      //////////////////////////////////////
{
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) and
         (Texto[7] in (['0'..'9'])) and
         (Texto[8] in (['0'..'9'])) then
}
{$IFDEF DELPHI12_UP}
      if (CharInSet(Texto[5], ['0'..'9'])) and
         (CharInSet(Texto[6], ['0'..'9'])) and
         (CharInSet(Texto[7], ['0'..'9'])) and
         (CharInSet(Texto[8], ['0'..'9'])) then
{$ELSE}
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) and
         (Texto[7] in (['0'..'9'])) and
         (Texto[8] in (['0'..'9'])) then
{$ENDIF}
        Ano := StrToInt(Texto[5]+Texto[6]+Texto[7]+Texto[8])
      //////////////////////////////////////
      else
{
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) then
}
{$IFDEF DELPHI12_UP}
      if (CharInSet(Texto[5], ['0'..'9'])) and
         (CharInSet(Texto[6], ['0'..'9'])) then
{$ELSE}
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) then
{$ENDIF}
        Ano := StrToInt(Texto[5]+Texto[6])
      else
      begin
        DecodeDate(Date, AAno, AMes, ADia);
        Ano := AAno;
      end;
      if Ano < 100 then
        if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
      if (Ano=0) or (Mes=0) or (Dia=0) then
        Result := 0

      else begin
        //try
          if (not DoEncodedate(Ano, Mes, Dia, Data)) and (MostraMsg = True) then
            MensagemDeErro('Data inválida: ' + Texto, 'Geral.ValidaDataBR()');
        //except
          //EAbort.Create('Data inválida: ' + Texto);
          //raise;
        //end;
        if AAno > 0 then
        begin
          if (Int(Data) < Int(Date)) and ForceNextYear then
            Data := EncodeDate(Ano + 1, Mes, Dia)
          else
            Data := ValidaData(Dia, Mes, Ano);
        end;
        Result := Data;
      end;
    end else
    begin
      try
        Result := StrToDate(Texto);
      except
        Geral.MB_Aviso('A data "' + Texto + '" é inválida!');
        Result := 0;
      end;
    end;
  except
    raise;
  end;
end;

function TGeral.ValidaDataHoraSQL(Texto: String): TDateTime;
var
  Tam: Integer;
  Data, Hora: TDateTime;
begin
  Tam := Length(Texto);
  case Tam of
    10: Result := ValidaData_YYYY_MM_DD(Texto);
    16, 19:
    begin
      Data := ValidaData_YYYY_MM_DD(Copy(Texto, 1, 10));
      Hora := StrToTime(Copy(Texto, 11));
      Result := Data + Hora;
    end;
    else
    begin
      Result := StrToTime(Texto);
    end;
  end;
end;

function TGeral.ValidaDataSimples(Texto: String;
  PermiteZero: Boolean): TDateTime;
var
  Data: TDateTime;
  Dia, Mes, Ano : Word;
  ADia, AMes, AAno : Word;
  TextChar : PChar;
  i: Integer;
  Compara: String;
  Sep: Char;
begin
  try
    Compara := '';
    if pos('-', Texto) > 0 then
      Sep := '-'
    else
    if pos('/', Texto) > 0 then
      Sep := '/'
    else
      Sep := ' ';
    //
    if PermiteZero then
    begin
      for i := 1 to Length(Texto) do
      begin
        if Texto[i] = Sep then
          Compara := Compara + Sep
        else
          Compara := Compara + '0';
      end;
      if Texto = Compara then
      begin
        Result := 0;
        Exit;
      end;
    end;
    TextChar := PChar(Texto);
    if (Pos(Sep, Texto) = 3) and (Length(Texto) = 5) then
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      Dia := IMV(Copy(Texto, 1, 2));
      Mes := IMV(Copy(Texto, 4, 2));
      Data := EncodeDate(Ano, Mes, Dia);
      if Data > Int(Date) then Data := EncodeDate(Ano-1, Mes, Dia);
      Texto := FormatDateTime('ddmmyyyy', Data);
      TextChar := PChar(Texto);
    end;
    if (StrRScan(TextChar,Sep) = nil) and
    ((Length(Texto) = 4) or
    (Length(Texto) = 6) or
    (Length(Texto) = 8)) then
    begin
      Dia := StrToInt(Texto[1]+Texto[2]);
      Mes := StrToInt(Texto[3]+Texto[4]);
      //////////////////////////////////////
{
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) and
         (Texto[7] in (['0'..'9'])) and
         (Texto[8] in (['0'..'9'])) then
}
{$IFDEF DELPHI12_UP}
      if (CharInSet(Texto[5], ['0'..'9'])) and
         (CharInSet(Texto[6], ['0'..'9'])) and
         (CharInSet(Texto[7], ['0'..'9'])) and
         (CharInSet(Texto[8], ['0'..'9'])) then
{$ELSE}
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) and
         (Texto[7] in (['0'..'9'])) and
         (Texto[8] in (['0'..'9'])) then
{$ENDIF}
        Ano := StrToInt(Texto[5]+Texto[6]+Texto[7]+Texto[8])
      //////////////////////////////////////
      else
{
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) then
}
{$IFDEF DELPHI12_UP}
      if (CharInSet(Texto[5], ['0'..'9'])) and
         (CharInSet(Texto[6], ['0'..'9'])) then
{$ELSE}
      if (Texto[5] in (['0'..'9'])) and
         (Texto[6] in (['0'..'9'])) then
{$ENDIF}
        Ano := StrToInt(Texto[5]+Texto[6])
      else
      begin
        DecodeDate(Date, AAno, AMes, ADia);
        Ano := AAno;
      end;
      if Ano < 100 then
        if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;

      if (Ano=0) or (Mes=0) or (Dia=0) then
        Result := 0
      else
        Result := EncodeDate(Ano, Mes, Dia);
    end else
      if Texto = '0000-00-00' then
        Result := 0
      else
        Result := StrToDate(Texto);
  except
    on EConvertError do
    begin
      Geral.MB_Erro(FIN_MSG_DATAINVALIDA);
      Result := 0;
    end;
  end;
end;

function TGeral.ValidaData_YYYY_MM_DD(Data: Variant): TDateTime;
var
  Ano, Mes, Dia: Word;
  Hora: TTime;
begin
  if (Data = Null) or (Data = '') then
  begin
    Result := 0;
    Exit;
  end else begin
    Ano := StrToInt(Copy(Data, 1, 4));
    Mes := StrToInt(Copy(Data, 6, 2));
    Dia := StrToInt(Copy(Data, 9, 2));
    if Ano = 0 then
      Result := 0
    else
      Result := EncodeDate(Ano, Mes, Dia);
    if Length(Data) > 11 then
    begin
      Hora := StrToTime(Copy(Data, 12));
      Result := Result + Hora;
    end;
  end;
end;

function TGeral.ValidaEMail(const Value: string): Boolean;
  function CheckAllowed(const s: string): Boolean;
  var i: Integer;
  begin
    Result:= False;
    for i:= 1 to Length(s) do
      //if not (s[i] in ['a'..'z', 'A'..'Z', '0'..'9', '_', '-', '.']) then Exit;
{$IFDEF DELPHI12_UP}
      if not (CharInSet(s[i], ['a'..'z', 'A'..'Z', '0'..'9', '_', '-', '.'])) then Exit;
{$ELSE}
      if not (s[i] in ['a'..'z', 'A'..'Z', '0'..'9', '_', '-', '.']) then Exit;
{$ENDIF}
    Result:= True;
  end;
var
  i: Integer;
  NamePart, ServerPart: string;
begin
  Result := False;
  i := Pos('@', Value);
  if i = 0 then Exit;
  NamePart := Copy(Value, 1, i - 1);
  ServerPart := Copy(Value, i + 1, Length(Value));
  if (Length(NamePart) = 0) or ((Length(ServerPart) < 5)) then Exit;
  i := Pos('.', ServerPart);
  if (i = 0) or (i > (Length(serverPart) - 2)) then Exit;
  Result:= CheckAllowed(NamePart) and CheckAllowed(ServerPart);
end;

function TGeral.ValidaMesAno(Texto: String): TDateTime;
var
  Mes, Ano : Word;
  TextChar : PChar;
begin
  try
    TextChar := PChar(Texto);
    if (StrRScan(TextChar,'/') = Nil) and
    (Length(Texto) = 4) then
    begin
      Mes := StrToInt(Texto[1]+Texto[2]);
      Ano := StrToInt(Texto[3]+Texto[4]);
      if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
      Result := EncodeDate(Ano, Mes, 1);
    end
    else
    begin
      if (StrRScan(TextChar,'/') = Nil) and
      (Length(Texto) = 6) then
      begin
        Mes := StrToInt(Texto[1]+Texto[2]);
        Ano := StrToInt(Texto[3]+Texto[4]+Texto[5]+Texto[6]);
        if Ano < 100 then
          if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
        Result := EncodeDate(Ano, Mes, 1);
      end
      else
      begin
        if (StrRScan(TextChar,'/') <> Nil) and
        (Length(Texto) = 5) then
        begin
          Mes := StrToInt(Texto[1]+Texto[2]);
          Ano := StrToInt(Texto[4]+Texto[5]);
          if Ano < 100 then
            if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
          Result := EncodeDate(Ano, Mes, 1);
        end
        else
        begin
          if (StrRScan(TextChar,'/') <> Nil) and
          (Length(Texto) = 7) then
          begin
            Mes := StrToInt(Texto[1]+Texto[2]);
            Ano := StrToInt(Texto[4]+Texto[5]+Texto[6]+Texto[7]);
            if Ano < 100 then
              if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
            Result := EncodeDate(Ano, Mes, 1);
          end
          else
            Result := StrToDate(Texto);
        end;
      end;
    end;
  except
    on EConvertError do
    begin
      Geral.MB_Erro(FIN_MSG_DATAINVALIDA);
      Result := 0;
    end;
  end;
end;

function TGeral.ValidaNVE(var NVE: String): Boolean;
var
  Erros: Integer;
begin
  Result := False;
  NVE := Uppercase(NVE);
  if Length(NVE) = 6 then
  begin
    Erros := 0;
    if not (CharInSet(NVE[1], ['A'..'Z'])) then Erros := Erros + 1;
    if not (CharInSet(NVE[2], ['A'..'Z'])) then Erros := Erros + 1;
    if not (CharInSet(NVE[3], ['0'..'9'])) then Erros := Erros + 1;
    if not (CharInSet(NVE[4], ['0'..'9'])) then Erros := Erros + 1;
    if not (CharInSet(NVE[5], ['0'..'9'])) then Erros := Erros + 1;
    if not (CharInSet(NVE[6], ['0'..'9'])) then Erros := Erros + 1;
    //
    Result := Erros = 0;
    if not Result then
      MB_Aviso('NVE "' + NVE + '" inválido!' + sLineBreak +
      'Deve ter duas letras e quatro algarismos!');
  end else
    MB_Aviso('NVE "' + NVE + '" com tamanho inválido!' + sLineBreak +
    'Deve ter seis caracteres mas tem ' + FF0(Length(NVE)));
end;

function TGeral.TFT_MinMax(Texto: String; Min, Max: Extended; Casas: Integer): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Extended;
begin
  if Texto = '' then
  begin
    Result := '';
    Exit;
  end;
  if Min > Max then
  begin
    Exception.Create(PChar('Mínimo maior que máximo na function ' +
    'TFT_MinMax'));
    Result := Texto;
    Exit;
  end;
  TxtForma := '#,###,##0';
  Numero := '';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    for i := 1 to Casas do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  if Flutuante < Min then Flutuante := Min;
  if Flutuante > Max then Flutuante := Max;
  Resultado := FormatFloat(TxtForma, Flutuante);
  Result := Resultado;
end;

function TGeral.TFT_Dot(Texto: String; Casas: Integer; Sinal: TSinal): String;
var
  Numero: String;
  Valor: Double;
  Inteiro, I: Integer;
  Fracao: Double;
begin
  try
    Texto   := Geral.TFT(Texto, Casas, Sinal);
    while Pos('.', Texto) > 0 do Delete(Texto, Pos('.', Texto), 1);
    Valor   := StrToFloat(Texto);
    Inteiro := Trunc(Valor / 1);
    Fracao  := Valor - Inteiro;
    for I := 1 to Casas do
      Fracao := Fracao * 10;
    I := Round(Fracao);
    Numero  := FormatFloat('0', Inteiro);
    if I > 0 then Numero := Numero + '.' + FormatFloat('0', I);
    //
    Result := Numero;
  except
    raise EAbort.Create('');
    Geral.MB_Erro(FIN_MSG_VALORFLOATINVALIDO);
  end;
end;

function TGeral.TFT_Max(Texto: String; Max: Double; Casas: Integer): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Double;
begin
  TxtForma := '#,###,##0';
  Numero := '';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    for i := 1 to Casas do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  if Flutuante > Max then Flutuante := Max;
  Resultado := FormatFloat(TxtForma, Flutuante);
  Result := Resultado;
end;

function TGeral.TFT_Min(Texto: String; Min: Double; Casas: Integer): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Double;
begin
  TxtForma := '#,###,##0';
  Numero := '';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    for i := 1 to Casas do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  if Flutuante < Min then Flutuante := Min;
  Resultado := FormatFloat(TxtForma, Flutuante);
  Result := Resultado;
end;

function TGeral.TFD(Texto: String; Digitos: Integer; Sinal: TSinal) : String;
var
  Val, FmtTxt, FmtNul: String;
  i: integer;
begin
  Val := SoNumeroESinal_TT(TFT(Texto, 0, Sinal));
  for i := 1 to Digitos do FmtTxT := FmtTxT + '0';
  if Texto = '' then
    FmtNul := ' '
  else
    FmtNul := FmtTxt;
  FmtTxT := FmtTxT +';-'+FmtTxT+';' + FmtNul;
  Result := Trim(FormatFloat(FmtTxt, StrToFloat(Val)));
end;

function TGeral.DMV(Texto: String) : Double;
var
  Numero : String;
begin
  Numero := LimpaNumeroTexto(Texto);
  if Numero = '' then Result := 0
  else
    Result := StrToFloat(Numero);
end;

function TGeral.DMV_Dot(Texto: String): Double;
var
  Numero : String;
begin
  Numero := LimpaNumeroTexto_Dot(Texto);
  if Numero = '' then Result := 0
  else
    Result := StrToFloat(Numero);
end;

procedure TGeral.MensagemDeErro(Mensagem, QuemChamou: String);
begin
  MensagemBox (Mensagem + sLineBreak + 'Função: ' + QuemChamou, 'Erro', MB_OK+MB_ICONERROR, 0);
end;

function TGeral.MesBarraAnoToMez(MesBarraAno: String): String;
begin
  Result := '';
  if MesBarraAno <> '' then
  begin
    if Length(MesBarraAno) <> 7 then
    begin
      Geral.MB_Erro('"MesBarraAno" inválido!');
      Result := '';
    end else
    begin
      // 07/2012
      Result :=
        MesBarraAno[6] + MesBarraAno[7] + MesBarraAno[1] + MesBarraAno[2];
    end;
  end;  
end;

function TGeral.MesEAnoDoMez(Mez: String): String;
var
  num, Mes, Ano: Integer;
begin
  if Mez = '' then Result := '' else
  if Mez = '0' then Result := '' else
  begin
    num := IMV(Mez);
    Ano := num div 100;
    Mes := num - (ano * 100);
    //
    // Modificado em 2010-05-11
    //Result := FormatFloat('00', Mes) + '/'+ FormatFloat('0', Ano + 2000);
    if Ano < 1900 then
      Ano := Ano + 2000;
    Result := FormatFloat('00', Mes) + '/' + FormatFloat('0', Ano);
  end;
end;

function TGeral.MesIntDeMesStr3Letras(Sigla: String): Word;
begin
  if Uppercase(Sigla) = Uppercase('Jan') then Result := 1 else
  if Uppercase(Sigla) = Uppercase('Fev') then Result := 2 else
  if Uppercase(Sigla) = Uppercase('Mar') then Result := 3 else
  if Uppercase(Sigla) = Uppercase('Abr') then Result := 4 else
  if Uppercase(Sigla) = Uppercase('Mai') then Result := 5 else
  if Uppercase(Sigla) = Uppercase('Jun') then Result := 6 else
  if Uppercase(Sigla) = Uppercase('Jul') then Result := 7 else
  if Uppercase(Sigla) = Uppercase('Ago') then Result := 8 else
  if Uppercase(Sigla) = Uppercase('Set') then Result := 9 else
  if Uppercase(Sigla) = Uppercase('Out') then Result := 10 else
  if Uppercase(Sigla) = Uppercase('Nov') then Result := 11 else
  if Uppercase(Sigla) = Uppercase('Dez') then Result := 12 else
  begin
    Result := 0;
    MB_Aviso('Sigla de mês inválida: ' + Sigla);
  end;
end;

function TGeral.Modulo10_1e2_9_Back(Numero: String): String;
var
  Peso, i, Soma, Nove, Prox10: Integer;
begin
  Peso := 2;
  Soma := 0;
  for i := Length(Numero) downto 1 do
  begin
    Nove := Peso * StrToInt(Numero[i]);
    if Nove > 9 then Nove := Nove - 9;
    Soma := Soma + Nove;
    if Peso = 2 then Peso := 1 else Peso := 2;
  end;
  Prox10 := ((Soma + 9) div 10) * 10;
  Result := IntToStr(Prox10 - Soma);
end;

function TGeral.Modulo10_2e1_X_Back(Numero: String; Tipo: Integer): String;
var
  nMul, i, Soma: Integer;
  Txt: String;
begin
  nMul := 1;
  Soma := 0;
  Txt := '';
  for i := Length(Numero) downto 1 do
  begin
    if nMul = 2 then nMul := 1 else nMul := 2;
    Txt := FormatFloat('0', (nMul * StrToInt(Numero[i]))) + Txt;
  end;
  for i := 1 to Length(Txt) do
    Soma := Soma + StrToInt(Txt[i]);
  Soma := Soma mod 10;
  //
  Soma := 10 - Soma;
  //
  case Tipo of
    0: if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
    else if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
    //
  end;
end;

function TGeral.Modulo11_2a7_Back(Numero: String; Tipo: Integer): String;
var
  nMul, i, Soma: Integer;
begin
  nMul := 1;
  Soma := 0;
  for i := Length(Numero) downto 1 do
  begin
    nMul := nMul + 1;
    if nMul > 7 then nMul := 2;
    Soma := Soma + (nMul * StrToInt(Numero[i]));
  end;
  Soma := Soma mod 11;
  //
  Soma := 11 - Soma;
  //
  case Tipo of
    0:
    begin
      case Soma of
        10: Result := 'P';
        11: Result := '0';
        else Result := IntToStr(Soma);
      end;
    end else if Soma > 9 then
      Result := '0'
    else
      Result := IntToStr(Soma);
    //
  end;
end;

function TGeral.Modulo11_2a9_Back(Numero: String; Tipo: Integer): String;
var
  nMul, i, Soma: Integer;
begin
  nMul := 1;
  Soma := 0;
  for i := Length(Numero) downto 1 do
  begin
    nMul := nMul + 1;
    if nMul > 9 then nMul := 2;
    Soma := Soma + (nMul * StrToInt(Numero[i]));
  end;
  Soma := Soma mod 11;
  //
  Soma := 11 - Soma;
  //
  case Tipo of
    // CampoLivre
    0: if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
    // 1: Banco do Brasil
    1: if Soma > 9 then Result := 'X' else Result := IntToStr(Soma);
    2: if (Soma > 9) or (Soma < 1) then Result := '1' else Result := IntToStr(Soma);
    else if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
    //
  end;
  if (Tipo = 1) and (Result = '0') then Result := '1';
end;

function TGeral.ModuloEAN13(Numero: String): String;
var
  nMul, i, Soma: Integer;
  Txt: String;
begin
  nMul := 1;
  Soma := 0;
  Txt := '';
  for i := Length(Numero) downto 1 do
  begin
    nMul := 4 - nMul;
    Soma := Soma + (nMul * StrToInt(Numero[i]));
  end;
  //
  Soma := 10 - (Soma mod 10);
  //
  if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
end;

function TGeral.MontaDataPorFmt(const Valor, Fmt: String; const PermiteZero:
  Boolean; var Data: TDateTime): Boolean;
var
  I: Integer;
  AnoI, MesI, DiaI: Word;
  AnoS, MesS, DiaS: String;
begin
  Result := False;
  Data := 0;
  if Length(Valor) <> Length(Fmt) then
  begin
    MB_Erro('O tamanho da formatação não confere com o tamanho da string!' +
    sLineBreak + '"Geral.MontaDataPorFmt()"');
    Exit;
  end;
  if PermiteZero and (SoNumero1a9_TT(Valor) = '') then
  begin
    Result := True;
    Exit;
  end;
  AnoS := '';
  MesS := '';
  DiaS := '';
  for I := 1 to Length(Fmt) do
  begin
    case Ord(Fmt[i]) of
      ord('A'),
      ord('Y'),
      ord('a'),
      ord('y'): AnoS := AnoS + Valor[I];
      ord('M'),
      ord('m'): MesS := MesS + Valor[I];
      ord('D'),
      ord('d'): DiaS := DiaS + Valor[I];
    end;
  end;
  if Length(AnoS) = 2 then
    AnoS := Copy(FormatDateTime('YYYY', Date), 1, 2) + AnoS;
  //
  AnoI := IMV(AnoS);
  MesI := IMV(MesS);
  DiaI := IMV(DiaS);
  //
  Data := EncodeDate(AnoI, MesI, DiaI);
  Result := Data > 1;
end;

function TGeral.MontaValDblPorFmt(const Valor: String; const Casas: Integer;
  var ValDbl: Double): Boolean;
var
  Inte, Flut: String;
begin
  if Casas > 0 then
  begin
    if Casas > Length(Valor) then
    begin
      Inte := '0';
      Flut := Valor;
      while Length(Flut) < casas do
        Flut := '0' + Flut;
    end else
    begin
      Inte := Copy(Valor, 1, Length(Valor) - Casas);
      Flut := Copy(Valor, Length(Valor) - Casas + 1);
    end;
    //
    ValDbl := DMV(Inte + ',' + Flut);
  end else
    ValDbl := DMV(Valor);
  //  
  Result := True;
end;

procedure TGeral.MensagemArray(Tipo: Integer; Titulo: String; Campos,
  Valores: array of String; Memo: TMemo);
var
  I, J: Integer;
  Texto: String;
begin
  J := High(Campos);
  for I := Low(Campos) to J do
    Texto := Texto + '     ' + Campos[I] + ' ' + Valores[I] + sLineBreak;
  //
  if Memo <> nil then
  begin
    Texto := Titulo + sLineBreak + Texto;
    case Tipo of
      //MB_ICONEXCLAMATION,
      MB_ICONWARNING: Memo.Lines.Add('AVISO: ' + Texto);
      MB_ICONERROR: Memo.Lines.Add('ERRO: ' + Texto);
      MB_ICONQUESTION: Memo.Lines.Add('PERGUNTA: ' + Texto);
      //MB_ICONSTOP: Memo.Lines.Add('ATENÇÃO: ' + Texto);
      else Memo.Lines.Add('??: ' + Texto);
    end;
  end else
    MB_Aviso(Texto);
end;

function TGeral. MensagemBox (Text, Caption: WideString; Typ: Integer; TempoCiclo: Integer = 0): Integer;
  function IntInConjunto(Numero, Conjunto: Integer): Boolean;
  begin
    Result := Conjunto and Numero > 0;
  end;
var
  //Logo: TMsgDlgType;
  //Btns: array of TMsgDlgBtn;
  P: Integer;
  Frase, MyTxt: String;
  I, L, Larg, A, Altu: Integer;
  Titulo: String;

begin

  Titulo := Screen.ActiveForm.Caption;
  P := Pos('::', Titulo);
  if P > 0 then
    Titulo := Copy(Titulo, 1, P-1);
  (*
  MessageBox() Flags
  MB_OK = $00000000;
  MB_OKCANCEL = $00000001;
  MB_ABORTRETRYIGNORE = $00000002;
  MB_YESNOCANCEL = $00000003;
  MB_YESNO = $00000004;
  MB_RETRYCANCEL = $00000005;
  MB_ICONHAND = $00000010;
  MB_ICONQUESTION = $00000020;
  MB_ICONEXCLAMATION = $00000030;
  MB_ICONASTERISK = $00000040;
  MB_USERICON = $00000080;
  //MB_ICONWARNING                 = MB_ICONEXCLAMATION;
  //MB_ICONERROR                   = MB_ICONHAND;
  //MB_ICONINFORMATION             = MB_ICONASTERISK;
  //MB_ICONSTOP                    = MB_ICONHAND;
  MB_DEFBUTTON1 = $00000000;
  MB_DEFBUTTON2 = $00000100;
  MB_DEFBUTTON3 = $00000200;
  MB_DEFBUTTON4 = $00000300;
  MB_APPLMODAL = $00000000;
  MB_SYSTEMMODAL = $00001000;
  MB_TASKMODAL = $00002000;
  MB_HELP = $00004000;
  MB_NOFOCUS = $00008000;
  MB_SETFOREGROUND = $00010000;
  MB_DEFAULT_DESKTOP_ONLY = $00020000;
  MB_TOPMOST = $00040000;
  MB_RIGHT = $00080000;
  MB_RTLREADING = $00100000;
  MB_SERVICE_NOTIFICATION = $00200000;
  MB_SERVICE_NOTIFICATION_NT3X = $00040000;
  MB_TYPEMASK = $0000000F;
  MB_ICONMASK = $000000F0;
  MB_DEFMASK = $00000F00;
  MB_MODEMASK = $00003000;
  MB_MISCMASK = $0000C000;
  *)
  // Botões
(*
  case Typ of
    // MB_OK+MB_ICONERROR
    16: Result := TaskMessageDlgPos(Caption, Text, mtError, [mbOk], 0, -1, -1);
    // MB_YESNOCANCEL+MB_ICONQUESTION
    35: Result := TaskMessageDlgPos(Caption, Text, mtWarning, [mbYes,mbNo,mbCancel], 0, -1, -1);
    // MB_OK+MB_ICONWARNING
    48: Result := TaskMessageDlgPos(Caption, Text, mtWarning, [mbOK], 0, -1, -1);
    // MB_OK+MB_ICONINFORMATION
    64: Result := TaskMessageDlgPos(Caption, Text, mtInformation, [mbOK], 0, -1, -1);
    // ? ? ? ? ?
    else Result := TaskMessageDlgPos(Caption, Text+sLineBreak+'ESTA JANELA ESTÁ INDEFINIDA! AVISE A DERMATEK', mtWarning, [mbYes,mbNo], 0, -1, -1);
  end;
*)
  if Typ = null then
    Exit; //Para evitar erros
  Larg := 314;
  Altu :=  66;
  Application.CreateForm(TFmdmkMsg, FmdmkMsg);
  case Typ of
    // Aguarde...
    -1:
    begin
      VAR_CAN_CLOSE_FORM_MSG := False;
      FmdmkMsg.ImageInfo.Visible := True;
      FmdmkMsg.AtivaEspera(TempoCiclo);
      FmdmkMsg.PB1.Max := VAR_CAN_CLOSE_FORM_MAX;
      FmdmkMsg.PB1.Position := 0;
    end;
    // MB_OK+MB_ICONERROR
    16:
    begin
      FmdmkMsg.ImageErro.Visible := True;
      FmdmkMsg.BtYes.Visible     := True;
    end;
    // MB_YESNOCANCEL+MB_ICONQUESTION
    35:
    begin
      FmdmkMsg.ImageQues.Visible := True;
      FmdmkMsg.BtYes.Caption     := '&Sim';
      FmdmkMsg.BtYes.Visible     := True;
      FmdmkMsg.BtNo.Visible      := True;
      FmdmkMsg.BtCancel.Visible  := True;
    end;
    //  MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON1+ MB_APPLMODAL
    36:
    begin
      FmdmkMsg.ImageQues.Visible := True;
      FmdmkMsg.BtYes.Caption     := '&Sim';
      FmdmkMsg.BtYes.Visible     := True;
      FmdmkMsg.BtYes.Default     := True;
      FmdmkMsg.BtNo.Visible      := True;
      //FmdmkMsg.BtCancel.Visible  := True;
    end;
    // MB_OK+MB_ICONWARNING
    48:
    begin
      FmdmkMsg.ImageWarn.Visible := True;
      FmdmkMsg.BtYes.Visible     := True;
    end;
    //  ERRADO!!!!
    // MB_YESNOCANCEL+MB_ICONWARNING
    51:
    begin
      FmdmkMsg.ImageQues.Visible := True;
      FmdmkMsg.BtYes.Caption     := '&Sim';
      FmdmkMsg.BtYes.Visible     := True;
      FmdmkMsg.BtNo.Visible      := True;
      FmdmkMsg.BtCancel.Visible  := True;
    end;
    // MB_YESNO+MB_ICONQUESTION
    52:
    begin
      FmdmkMsg.ImageQues.Visible := True;
      FmdmkMsg.BtYes.Caption     := '&Sim';
      FmdmkMsg.BtYes.Visible     := True;
      FmdmkMsg.BtNo.Visible      := True;
      //FmdmkMsg.BtCancel.Visible  := True;
    end;
    // MB_OK+MB_ICONINFORMATION
    64:
    begin
      FmdmkMsg.ImageInfo.Visible := True;
      FmdmkMsg.BtYes.Visible     := True;
    end;
  end;
  FmdmkMsg.LaTitulo1A.Caption := Caption;
  FmdmkMsg.LaTitulo1B.Caption := Caption;
  FmdmkMsg.LaTitulo1C.Caption := Caption;
  FmdmkMsg.ReAviso.Text := Text;
  with FmdmkMsg.Canvas do
  begin
    Font.Name  := FmdmkMsg.ReAviso.Font.Name;
    Font.Style := FmdmkMsg.ReAviso.Font.Style;
    Font.Size  := FmdmkMsg.ReAviso.Font.Size;
  end;
  MyTxt := Text;
  I := 0;
  P := pos(sLineBreak, MyTxt);
  while (P > 1) or (MyTxt <> '') do
  begin
    // caso não tenha mais quebra de linha:
    if P = 0 then
      P := Length(MyTxt) + 1;
    //

    I := I + 1;
    Frase := Copy(MyTxt, 1, P-1);
    L := FmdmkMsg.Canvas.TextWidth(Frase);
    if L > Larg then
      Larg := L;
    MyTxt := Copy(MyTxt, P + 2);
    P := pos(sLineBreak, MyTxt);
  end;
  L := FmdmkMsg.Canvas.TextWidth(MyTxt) + 6;
  if L > Larg then
    Larg := L;

  // Definir a largura da janela para ver quantas linhas ficam!
  Larg := Round(Larg * 1.1);
  if Larg > FmdmkMsg.Width then
  begin
    if Larg > Screen.Width then
      Larg := Screen.Width;
    FmdmkMsg.Width := Larg;
  end;
  //

  //I := ReAviso.
  A := FmdmkMsg.Canvas.TextHeight(Text);
  if (A * I) > Altu then Altu := (A * I);
  Altu := Altu + FmdmkMsg.Height - FmdmkMsg.ReAviso.Height;
  Altu := Trunc(Altu * 1.1);
  if Altu > FmdmkMsg.Height then
  begin
    if Altu > Screen.Height then
      Altu := Screen.Height;
    FmdmkMsg.Height := Altu;
  end;
  FmdmkMsg.Caption := ' v.' + APP_VERSAO + ' :: ' + Titulo;
  if Typ = -1 then
    FmdmkMsg.Show
  else
  begin
    FmdmkMsg.ShowModal;
    Result := FmdmkMsg.FResult;
    FmdmkMsg.Destroy;
  end;
end;

procedure TGeral.MB_Aguarde(Text: WideString; TempoCiclo: Integer);
begin
  //Result :=
  MensagemBox (Text, 'Aguarde...', -1, TempoCiclo);
end;

function TGeral.MB_Aviso(Text: WideString): Integer;
begin
  Result := MensagemBox (Text, 'Aviso', MB_OK+MB_ICONWARNING, 0);
end;

function TGeral.MB_Erro(Text: WideString): Integer;
begin
  Result := MensagemBox (Text, 'Erro', MB_OK+MB_ICONERROR, 0);
end;

function TGeral.MB_Info(Text: WideString): Integer;
begin
  Result := MensagemBox (Text, 'Informação', MB_OK+MB_ICONINFORMATION, 0);
end;

function TGeral.MB_Pergunta(Text: WideString): Integer;
begin
  Result := MensagemBox (Text, 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION, 0);
end;

function TGeral.MB_SQL(Form: TForm; Qry: TDataSet): Integer;
var
  FSQL: Variant;
  LVarList: TStrings;
  Texto: String;
  I: Integer;
begin
  //FSQL := TStringList.Create;
  //LVarList := TStringList.Create;
  try
    if GET_Compo_Val(Form, Qry, Qry.Name, 'SQL', TDataSet, FSQL) then
    begin

      Result := MensagemBox (String(FSQL), 'Texto SQL', MB_OK+MB_ICONINFORMATION, 0);
    end;
  finally
    //FSQL.Free;
    //LVarList.Free;
  end;
end;

function TGeral.MB_SQL(Form: TForm; Qry: TComponent): Integer;
var
  FSQL: Variant;
  LVarList: TStrings;
  Texto: String;
  I: Integer;
begin
  try
    if GET_Compo_Val(Form, Qry, Qry.Name, 'SQL', TComponent, FSQL) then
    begin
      Result := MensagemBox (String(FSQL), 'Texto SQL', MB_OK+MB_ICONINFORMATION, 0);
    end;
  finally
    //FSQL.Free;
    //LVarList.Free;
  end;
end;


function TGeral.MB_Teste(Text: WideString): Integer;
begin
  Result := MensagemBox (Text, 'Informação', MB_OK+MB_ICONINFORMATION, 0);
end;

function TGeral.MTD(Data: String; Obrigatorio: Boolean): TDateTime;
var
  Fat, i, Separa, Tam: Integer;
  M, A, X0, X1, X2: String;
begin
  Result := 0;
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
      MessageDlg('Data obrigatória!', mtError, [mbOK, mbCancel], 0);
      Exit;
    end;
    if (StrRScan(PChar(Data),'/') = Nil) then
    begin
      if Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'M' then Fat := -1 else Fat := 1;
      case (Tam * Fat) of
        -8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
        -6:
        begin
          if IMV(Data[1]+Data[2])> 12 then
            Data := Data[3]+Data[4]+'/'+Data[1]+Data[2]+'/'+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        -5: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3]+Data[4]+Data[5];
        -4: Data := Data[1]+Data[2]+'/01/'+Data[3]+Data[4];
        -3: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3];
        ////////////////////////////////////////////////////////////////////////
        3: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3];
        4: Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4];
        5: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3]+Data[4]+Data[5];
        6:
        begin
          if IMV(Data[3]+Data[4])> 12 then
            Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4]+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
      end;
    end else begin
      Separa := 0;
      for i := 1 to Tam do
      begin
        if Data[i] = '/' then Separa := Separa + 1;
        //if Data[i] in ['0'..'9'] then
{$IFDEF DELPHI12_UP}
        if CharInSet(Data[i], ['0'..'9']) then
{$ELSE}
        if Data[i] in ['0'..'9'] then
{$ENDIF}
        begin
          case Separa of
            0: X0 := X0 + Data[i];
            1: X1 := X1 + Data[i];
            2: X2 := X2 + Data[i];
          end;
        end;
      end;
      if Separa = 2 then
      begin
        M := X1;
        A := X2;
      end else begin
        M := X0;
        A := X1;
      end;
      if Length(M) = 1 then M := '0' + M;
      if Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'M' then Data := M+'/01/'+A else
         Data := '01/'+M+'/'+A;
    end;
    Result := StrToDate(Data);
  except
    on EConvertError do
    begin
      Result := Null;
    end;
  end;
end;

(*
function TGeral.MTD(Data: String; Obrigatorio: Boolean): TDateTime;
var
  Fat, i, Separa, Tam: Integer;
  M, A, X0, X1, X2: String;
begin
  Result := 0;
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
      MessageDlg('Data obrigatória!', mtError, [mbOK, mbCancel], 0);
      Exit;
    end;
    if (StrRScan(PChar(Data),'/') = Nil) then
    begin
      if Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'M' then Fat := -1 else Fat := 1;
      case (Tam * Fat) of
        -8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
        -6:
        begin
          if IMV(Data[1]+Data[2])> 12 then
            Data := Data[3]+Data[4]+'/'+Data[1]+Data[2]+'/'+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        -5: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3]+Data[4]+Data[5];
        -4: Data := Data[1]+Data[2]+'/01/'+Data[3]+Data[4];
        -3: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3];
        ////////////////////////////////////////////////////////////////////////
        3: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3];
        4: Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4];
        5: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3]+Data[4]+Data[5];
        6:
        begin
          if IMV(Data[3]+Data[4])> 12 then
            Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4]+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
      end;
    end else begin
      Separa := 0;
      for i := 1 to Tam do
      begin
        if Data[i] = '/' then Separa := Separa + 1;
        //if Data[i] in ['0'..'9'] then
{$IFDEF DELPHI12_UP}
        if CharInSet(Data[i], ['0'..'9']) then
{$ELSE}
        if Data[i] in ['0'..'9'] then
{$ENDIF}
        begin
          case Separa of
            0: X0 := X0 + Data[i];
            1: X1 := X1 + Data[i];
            2: X2 := X2 + Data[i];
          end;
        end;
      end;
      if Separa = 2 then
      begin
        M := X1;
        A := X2;
      end else begin
        M := X0;
        A := X1;
      end;
      if Length(M) = 1 then M := '0' + M;
      if Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'M' then Data := M+'/01/'+A else
         Data := '01/'+M+'/'+A;
    end;
    Result := StrToDate(Data);
  except
    on EConvertError do
    begin
      Result := Null;
    end;
  end;
end;
*)

function TGeral.MTD2(Data: String; Obrigatorio: Boolean): TDateTime;
var
  Fat, i, Separa, Tam: Integer;
  M, A, X0, X1, X2: String;
begin
  Result := 0;
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
      MessageDlg('Data obrigatória!', mtError, [mbOK, mbCancel], 0);
      Exit;
    end;
    (*if (StrRScan(PChar(Data),'/') = Nil) then
    begin
      if Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'M' then Fat := -1 else Fat := 1;
      case (Tam * Fat) of
        -8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
        -6:
        begin
          if IMV(Data[1]+Data[2])> 12 then
            Data := Data[3]+Data[4]+'/'+Data[1]+Data[2]+'/'+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        -5: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3]+Data[4]+Data[5];
        -4: Data := Data[1]+Data[2]+'/01/'+Data[3]+Data[4];
        -3: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3];
        ////////////////////////////////////////////////////////////////////////
        3: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3];
        4: Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4];
        5: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3]+Data[4]+Data[5];
        6:
        begin
          if IMV(Data[3]+Data[4])> 12 then
            Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4]+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
      end;
    end else*) begin
      Separa := 0;
      for i := 1 to Tam do
      begin
        if Data[i] = '-' then Separa := Separa + 1;
        // ini 2022-03-09
        if Data[i] = '/' then Separa := Separa + 1;
        // fim 2022-03-09
        //if Data[i] in ['0'..'9'] then
{$IFDEF DELPHI12_UP}
        if CharInSet(Data[i], ['0'..'9']) then
{$ELSE}
        if Data[i] in ['0'..'9'] then
{$ENDIF}
        begin
          case Separa of
            0: X0 := X0 + Data[i];
            1: X1 := X1 + Data[i];
            2: X2 := X2 + Data[i];
          end;
        end;
      end;
      if Separa = 2 then
      begin
        M := X0; //X1;
        A := X1; //X2;
      end else begin
        if Length(X1) = 4 then
        begin
          M := X0;
          A := X1;
        end else
        begin
          M := X1;
          A := X0;
        end;
      end;
      if Length(M) = 1 then M := '0' + M;
      if Uppercase({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1]) = 'M' then
        Data := M+'/01/'+A
      else
        Data := '01/'+M+'/'+A;
    end;
    Result := StrToDate(Data);
  except
    on EConvertError do
    begin
      Result := Null;
    end;
  end;
end;

function TGeral.MTP(Data: String; Obrigatorio: Boolean): Variant;
var
  Tam: Integer;
  M, A: String;
  Mes, Ano, Dia: Word;
  Conf: TDate;
begin
  Result := Null;
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
        MessageDlg('Data obrigatória!', mtError, [mbOK, mbCancel], 0);
      Exit;
    end;
    Conf := MTD(Data, Obrigatorio);
    //
    DecodeDate(Conf, Ano, Mes, Dia);
    M := IntToStr(Mes);
    A := IntToStr(Ano);
    Result := ((Ano - 2000) * 12) + Mes;
  except
    on EConvertError do
    begin
      Result := Null;
    end;
  end;
end;


function TGeral.MTP2(Data: String; Obrigatorio: Boolean): Variant;
var
  Tam: Integer;
  M, A: String;
  Mes, Ano, Dia: Word;
  Conf: TDate;
begin
  Result := Null;
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
        MessageDlg('Data obrigatória!', mtError, [mbOK, mbCancel], 0);
      Exit;
    end;
    Conf := MTD2(Data, Obrigatorio);
    //
    DecodeDate(Conf, Ano, Mes, Dia);
    M := IntToStr(Mes);
    A := IntToStr(Ano);
    Result := ((Ano - 2000) * 12) + Mes;
  except
    on EConvertError do
    begin
      Result := Null;
    end;
  end;
end;

function TGeral.MyExtractStrings(Separators, WhiteSpace: TSysCharSet;
  Content: PChar; Strings: TStrings): Integer;
var
  Head, Tail: PChar;
  EOS, InQuote: Boolean;
  QuoteChar: Char;
  Item: string;
  // Dermatek
  Conta: Integer;
begin
  Result := 0;
  // Dermatek
  Conta := 0;
  // Fim Dermatek
  if (Content = nil) or (Content^=#0) or (Strings = nil) then Exit;
  Tail := Content;
  InQuote := False;
  QuoteChar := #0;
  Strings.BeginUpdate;
  try
    repeat
      //while Tail^ in WhiteSpace + [#13, #10] do Tail := StrNextChar(Tail);
{$IFDEF DELPHI12_UP}
      while CharInSet(Tail^, WhiteSpace + [#13, #10]) do Tail := StrNextChar(Tail);
{$ELSE}
      while Tail^ in WhiteSpace + [#13, #10] do Tail := StrNextChar(Tail);
{$ENDIF}
      Head := Tail;
      while True do
      begin
        //while (InQuote and not (Tail^ in [QuoteChar, #0])) or
{$IFDEF DELPHI12_UP}
        while (InQuote and not (CharInSet(Tail^, [QuoteChar, #0]))) or
{$ELSE}
        while (InQuote and not (Tail^ in [QuoteChar, #0])) or
{$ENDIF}
// Dermatek
          //not (Tail^ in Separators + [#0, #13, #10(*, '''', '"'*)]) do Tail := StrNextChar(Tail);
{$IFDEF DELPHI12_UP}
          not (CharInSet(Tail^, Separators + [#0, #13, #10(*, '''', '"'*)])) do Tail := StrNextChar(Tail);
{$ELSE}
          not (Tail^ in Separators + [#0, #13, #10(*, '''', '"'*)]) do Tail := StrNextChar(Tail);
{$ENDIF}
        (*
        if Tail^ in ['''', '"'] then
        begin
          if (QuoteChar <> #0) and (QuoteChar = Tail^) then
            QuoteChar := #0
          else if QuoteChar = #0 then
            QuoteChar := Tail^;
          InQuote := QuoteChar <> #0;
          Tail := StrNextChar(Tail);
        end else*) Break;
      end;
      EOS := Tail^ = #0;
      // Retirei!
      //if (Head <> Tail) and (Head^ <> #0) then
      // Coloquei no lugar
      if (Conta > 0) and (Tail <> '') then
// Fim Dermatek
      begin
        if Strings <> nil then
        begin
          SetString(Item, Head, Tail - Head);
          Strings.Add(Item);
        end;
        Inc(Result);
      end;
// Dermatek
      Conta := Conta + 1;
// Fim Dermatek
      Tail := StrNextChar(Tail);
    until EOS;
  finally
    Strings.EndUpdate;
  end;
end;

function TGeral.MyNPS(Texto: String): String;
begin
  Result := MyUTF(Texto);
  //
  Result := Geral.Substitui(Result, '.', '');
  Result := Geral.Substitui(Result, '-', '');
  Result := Geral.Substitui(Result, ' ', '');
end;

function TGeral.MyUTF(Texto: String): String;
begin
  Result := Maiusculas(SemAcento(Trim(Texto)), False);
  while pos('  ', Result) > 0 do
    Result := Substitui(Result, '  ', ' ');
end;

function TGeral.NomeVarType(VarType: TVarType): String;
begin
  case VarType of
    varEmpty    : Result := 'Vazio';    //= $0000; { vt_empty        0 }
    varNull     : Result := 'Nulo';     //= $0001; { vt_null         1 }
    varSmallint : Result := 'SmallInt'; //= $0002; { vt_i2           2 }
    varInteger  : Result := 'Integer';  //= $0003; { vt_i4           3 }
    varSingle   : Result := 'Single ';  //= $0004; { vt_r4           4 }
    varDouble   : Result := 'Double ';  //= $0005; { vt_r8           5 }
    varCurrency : Result := 'Currency'; //= $0006; { vt_cy           6 }
    varDate     : Result := 'Date   ';  //= $0007; { vt_date         7 }
    varOleStr   : Result := 'OleStr ';  //= $0008; { vt_bstr         8 }
    varDispatch : Result := 'Dispatch'; //= $0009; { vt_dispatch     9 }
    varError    : Result := 'Error  ';  //= $000A; { vt_error       10 }
    varBoolean  : Result := 'Boolean';  //= $000B; { vt_bool        11 }
    varVariant  : Result := 'Variant';  //= $000C; { vt_variant     12 }
    varUnknown  : Result := 'Unknown';  //= $000D; { vt_unknown     13 }
  //varDecimal  = $000E; { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
  //varUndef0F  = $000F; { undefined      15 } {UNSUPPORTED per Microsoft}
    varShortInt : Result := 'ShortInt'; //= $0010; { vt_i1          16 }
    varByte     : Result := 'Byte   ';  //= $0011; { vt_ui1         17 }
    varWord     : Result := 'Word   ';  //= $0012; { vt_ui2         18 }
    varLongWord : Result := 'LongWord'; //= $0013; { vt_ui4         19 }
    varInt64    : Result := 'Int64  ';  //= $0014; { vt_i8          20 }
  //varWord64   = $0015; { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
  {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

    varStrArg   : Result := 'StrArg';   //= $0048; { vt_clsid       72 }
    varString   : Result := 'String ';  //= $0100; { Pascal string 256 } {not OLE compatible }
    varAny      : Result := 'Any    ';  //= $0101; { Corba any     257 } {not OLE compatible }
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
    varUString  : Result := 'UString '; //= $0102; { Unicode string 258 } {not OLE compatible }: Result := '"' + Variavel + '"';
{$ENDIF}
    // custom types range from $110 (272) to $7FF (2047)

    varTypeMask : Result := 'TypeMask'; //= $0FFF;
    varArray    : Result := 'Array  ';  //= $2000;
    varByRef    : Result := 'ByRef  ';  //= $4000;
    else Result := 'VarType desconhecido: ' + IntToStr(VarType);
  end;
end;

// AgoraTxt Agora_Txt
function TGeral.NowTxt(): String;
begin
  Result := FormatDateTime('hh:nn:ss:zzz', Now()) + ' - ';
end;

function TGeral.NumStrToBool(Verdade: String): Boolean;
begin
  Result := Verdade = '1';
end;

function TGeral.ObtemAnoMesDeMesStrAno(const Texto: String; var Ano,
  Mes: Integer): Boolean;
const
  MesesUpper: array[0..12] of String = (
  'janeiro',
  'fevereiro',
  'março', 'marco',
  'abril',
  'maio',
  'junho',
  'julho',
  'agosto',
  'setembro',
  'outubro',
  'novembro',
  'dezembro');
  UpperMeses: array[0..12] of String = (
  '01', '02', '03', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
  MesesLower: array[0..11] of String = (
  'jan', 'fev', 'mar', 'abr', 'mai', 'jun',
  'jul', 'ago', 'set', 'out', 'nov', 'dez');
  MesesNumrs: array[0..11] of String = (
  '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
var
  txt, s: String;
  I, P: Integer;
  Achou: Boolean;
begin
  Mes := 0;
  Ano := 0;
  s := '';
  Achou := False;
  Txt := AnsiLowercase(Trim(Texto));
  for I := 0 to 12 do
  begin
    s := MesesUpper[I];
    P := pos(s, Txt);
    if P > 0 then
    begin
      Achou := True;
      Mes := IMV(UpperMeses[I]);
      Break;
    end;
  end;
  if not achou then
  begin
    for I := 0 to 11 do
    begin
      s := MesesLower[I];
      P := pos(s, Txt);
      if P > 0 then
      begin
        //Achou := True;
        Mes := IMV(MesesNumrs[I]);
        Break;
      end;
    end;
  end;
  //
  if P = 1 then
    Txt := Copy(Txt, Length(s) + 1)
  else
    Txt := Copy(Txt, 1, P -1);
  Ano := IMV(SoNumero_TT(Txt));
  Result := (Mes > 0) and (Ano > 0);
end;

function TGeral.ObtemHoraDeDateTime(DataHora: TDateTime): TTime;
begin
  // Para retornar como Time e não como Double!
  Result := DataHora - Trunc(DataHora);
end;


function TGeral.ObtemIP(Retorno: Integer): String;
type
  TaPInAddr = array [0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe : PHostEnt;
  pptr : PaPInAddr;
  Buffer : array [0..63] of AnsiChar;
  I : Integer;
  GInitData : TWSADATA;
  Res: String;
begin
  WSAStartup($101, GInitData);
  Result := '';
  GetHostName(Buffer, SizeOf(Buffer));
  phe := GetHostByName(buffer);
  if phe = nil then
    Exit;
  pptr := PaPInAddr(Phe^.h_addr_list);
  I := 0;
  while pptr^[I] <> nil do
  begin
    Res := StrPas(inet_ntoa(pptr^[I]^));
    Inc(I);
  end;
  WSACleanup;
  //
  case Retorno of
      0: Result := StrPas(Phe^.h_name);
      1: Result := Res;
    else Result := '';
  end;
end;

{
Erro no Delphi XE2 -> 03/02/2014
function TGeral.ObtemIP(Retorno: Integer): String;
var
  p: PHostEnt;
  s: array[0..128] of AnsiChar;
  p2: PAnsiChar;
  pptr: PInAddr;
begin
  GetHostName(@s, 128);
  p := GetHostByName(@s);
  pptr := p^.h_addr_list^;
  p2 := iNet_ntoa(^pptr);
  case Retorno of
    0: Result := String(p^.h_Name);
    1: Result := String(p2);
    else Result := '';
  end;
end;
}

function TGeral.ObtemTextoDeAllFormat(Format: Integer): String;
const
  Textos: array[0..Integer(dmktfUnknown)] of String =
    ('Nenhum', 'Texto', 'Flutuante', 'Inteiro', 'Longo', 'Int.64',
     'Data', 'Hora', 'Data/Hora', 'MM/AAAA', 'AAAAMM', 'AAAA-MM',
     //
     'Não sei');
begin
  Result := Textos[Format];
end;

{ Returns a count of the number of occurences of SubText in Text }
function TGeral.CountOccurences(const SubText: string; const Text: string): Integer;
begin
  if (SubText = '') OR (Text = '') OR (Pos(SubText, Text) = 0) then
    Result := 0
  else
    Result := (Length(Text) - Length(StringReplace(Text, SubText, '', [rfReplaceAll]))) div  Length(subtext);
end;  { CountOccurences }

{
function TGeral.ObtemMenorTamanho(Grandeza: TGrandeza; MedidaNormal,
  MedidaAtual: Double; NomeMedida: String): Boolean;
var
  Divisor, Step: Integer;
begin
  if Grandeza = grandezaBytes then
    Divisor := 1024
  else
    Divisor := 1000;
  //

end;
}

function TGeral.TST(Data: String; Obrigatorio: Boolean): String;
var
  Tam: Integer;
  M, A: String;
//  Conf: TDate;
  //Dia,
  Mes, Ano: Word;
  Periodo: Variant;
begin
  Result := '';
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
        MessageDlg('Data obrigatória!', mtError, [mbOK, mbCancel], 0);
      Exit;
    end;
    Periodo := MTP(Data, Obrigatorio);
    if Periodo = Null then Result := '' else
    begin
      Mes := Periodo mod 12;
      Ano := Periodo div 12;
      if Mes = 0 then
      begin
        Mes := 12;
        Ano := Ano -1;
      end;
      M := IntToStr(Mes);
      A := IntToStr(Ano);
      if Length(M) = 1 then M := '0' + M;
      Result := M + '/' + A;
    end;
  except
    on EConvertError do
    begin
      //MessageBox(FIN_MSG_DATAINVALIDA, 'Erro', MB_OK+MB_ICONERROR);
      Result := '';
    end;
  end;
end;

function TGeral.TSTA(Itens: TStrings): TdmkArrStr;
var
  I: Integer;
begin
  SetLength(Result, Itens.Count);
  for I := 0 to Itens.Count - 1 do
    Result[I] := Itens[I];
end;

function TGeral.TxtSemPipe(Txt: String): String;
var
  Ch: Char;
  I: Integer;
  Texto: String;
begin
(*  GUIA_PRATICO_DA_EFD-Versao_2.0.19.pdf de 05/05/2016  - pag 21 a 22
Seção 3 – Preenchimento de campos
  Para campos alfanuméricos, representados por "C", podem ser usados todos os
  caracteres da Tabela ASCII, exceto os caracteres "|" (Pipe, código 124 da
  Tabela ASCII) e os não-imprimíveis (caracteres 00 a 31 da Tabela ASCII).
  Todos os campos alfanuméricos terão tamanho máximo de 255 caracteres, exceto
  se houver indicação distinta, onde, neste caso, este tamanho distinto
  prevalecerá. Não poderão ser informados espaços “em branco” no início ou ao
  final da informação.
  Para campos numéricos, representados por "N", podem ser usados algarismos das
  posições de 48 a 57 da Tabela ASCII.
  Para campos numéricos nos quais há indicação de casas decimais:
    a) deverão ser preenchidos sem os separadores de milhar, sinais ou quaisquer
      outros caracteres (tais como: "." "-" "%"), devendo a vírgula ser utilizada
      como separador decimal (vírgula: caractere 44 da Tabela ASCII);
    b) não há limite de caracteres para os campos numéricos. O caractere *
      (Asterisco) aposto ao lado do tamanho do campo indica que o campo deve ser
      informado com aquela quantidade exata de caracteres;
    c) observar a quantidade máxima de casas decimais que constar no respectivo
      campo (Ex. para os campos alíquota de ICMS com tamanho máximo de 06
      caracteres considerando a vírgula e duas decimais, o valor máximo a ser
      informado é 999,99);
    d) preencher os valores percentuais desprezando-se o símbolo (%), sem
      nenhuma convenção matemática.
      Exemplo (valores monetários, quantidades, percentuais, etc):
        $ 1.129.998,99 -> |1129998,99|
        1.255,42 -> |1255,42|
        234,567 -> |234,567|
        10.000 -> |10000|
        10.000,00 -> |10000| ou |10000,00|
        17,00 % -> |17,00| ou |17|
        18,50 % -> |18,5| ou |18,50|
        30 -> |30|
        1.123,456 Kg -> |1123,456|
        0,010 litros -> |0,010|
        0,00 -> |0| ou |0,00|
        0 -> |0|
        campo vazio -> ||
    e) Caracteres maiúsculos e minúsculos são considerados iguais.
*)
  if Trim(Txt) = '' then Result := Txt else
  begin
    Texto := '';
    for I := 1 to Length(Txt) do
    begin
      Ch := Txt[i];
      //
      if Ord(Ch) < 32 then
        Texto := Texto + ' '
      else
      if Ch = '|' then
        Texto := Texto + '-'
      else
        Texto :=  Texto + Txt[i];
    end;
    Result := Texto;
  end;
end;

function TGeral.UltimoDiaDoMes(Data: TDate): TDate;
begin
  Result := (IncMonth(Data1(Data), 2))-1;
end;

function TGeral.DTP(Data: TDateTime): Integer;
var
  Ano, Mes, Dia: word;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  Result := ((Ano - 2000) * 12) + Mes;
end;

(*
function TGeral.DTP2(Data: TDateTime): Integer;
begin
  Result := DTP(Data);
end;
*)

function TGeral.DTAM(Data: TDateTime): Integer;
var
  Ano, Mes, Dia: word;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  Result := Ano * 100 + Mes;
end;

function TGeral.PTM(Periodo: Integer): String;
var
  Ano, Mes: Word;
begin
  if Periodo < 1 then Result := '' else
  begin
    Ano := (Periodo div 12 )+ 2000;
    Mes := Periodo mod 12;
    if Mes = 0 then
    begin
      Mes := 12;
      Ano := Ano -1;
    end;
    Result := FormatFloat('00', Mes)+'/'+FormatFloat('0000', Ano);
  end;
end;

function TGeral.QuadriVal(Valor: Double): Double;
var
  Sobra, Frac: Double;
begin
  Result := int(Valor);
  Sobra := Valor - Result;
  if Sobra < -0.875 then Frac := -1.00
  else if Sobra < -0.625 then Frac := -0.75
  else if Sobra < -0.375 then Frac := -0.50
  else if Sobra < -0.125 then Frac := -0.25
  else if Sobra < 0.125 then Frac := 0
  else if Sobra < 0.375 then Frac := 0.25
  else if Sobra < 0.625 then Frac := 0.50
  else if Sobra < 0.875 then Frac := 0.75
  else                       Frac := 1;
  //
  Result := Result + Frac;
end;

function TGeral.ReadAppKey(const Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant; xHKEY: HKEY): Variant;
var
  r : TRegistry;
  Path: String;
begin
  Path := 'Software\'+AppTitle;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  //r := TRegistry.Create(KEY_READ);
  //r.RootKey := HKEY_LOCAL_MACHINE;
  r.RootKey := xHKEY;
  try
    r.OpenKeyReadOnly(Path);
    Result := DefValue;
    //if r.KeyExists(Path + '\' + Key) then
    begin
      if r.ValueExists(Key) then
      begin
        case KeyType of
          ktString: Result := r.ReadString(Key);
          ktBoolean: Result := r.ReadBool(Key);
          ktInteger: Result := r.ReadInteger(Key);
          ktCurrency: Result := r.ReadCurrency(Key);
          ktDate: Result := r.ReadDate(Key);
          ktTime: Result := r.ReadTime(Key);
          ktDateTime: Result := r.ReadDateTime(Key);
          //ktBinary: Result := r.ReadBinaryData(Key);
        end;
      end else
      begin
        if xHKEY = HKEY_LOCAL_MACHINE then
          Result :=
            ReadAppKey(Key, AppTitle, KeyType, DefValue, HKEY_CURRENT_USER);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TGeral.ReadAppKeyCU(const Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant): Variant;
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_READ);
  //r.RootKey := HKEY_LOCAL_MACHINE;
  r.RootKey := HKEY_CURRENT_USER;
  try
    r.OpenKeyReadOnly('Software\'+AppTitle);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        ktFloat: Result := r.ReadFloat(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TGeral.ReadAppKeyLM_Net(const NomeServ, Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant; Retorno: Variant): Boolean;
var
  r : TRegistry;
  n: String;
  k: Integer;
begin
  Result := False;
  r := TRegistry.Create(KEY_READ);
  try
    r.RootKey := HKEY_LOCAL_MACHINE;
    n := NomeServ;
    while n[1] = '\' do
      n := Copy(n, 2);
    k := pos('\', n);
    if k > 0 then
      n := Copy(n, 1, k-1);
    n := '\\' + n;
    //
    if r.RegistryConnect(n) then // '\\peters_notebook'
    begin
      Retorno := DefValue;
      // Result deve ser aqui! Não mudar!
      Result := True;
      if r.OpenKeyReadOnly('Software\'+AppTitle) then
      begin
        if r.ValueExists(Key) then
        begin
          case KeyType of
            ktString: Retorno := r.ReadString(Key);
            ktBoolean: Retorno := r.ReadBool(Key);
            ktInteger: Retorno := r.ReadInteger(Key);
            ktCurrency: Retorno := r.ReadCurrency(Key);
            ktDate: Retorno := r.ReadDate(Key);
            ktTime: Retorno := r.ReadTime(Key);
            ktDateTime: Retorno := r.ReadDateTime(Key);
            ktFloat: Retorno := r.ReadFloat(Key);
            //ktBinary: Retorno := r.ReadBinaryData(Key);
          end;
        end;
      end;
    end else Geral.MB_Erro('Não foi possível acessar o servidor "' + n + '"');
  finally
    r.Free;
  end;
end;

function TGeral.ReadKey(const Key, Path: String; KeyType: TKeyType;
  DefValue: Variant; xHKEY: HKEY): Variant;
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_READ);
  r.RootKey := xHKEY;
  try
    r.OpenKeyReadOnly(Path);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        ktFloat: Result := r.ReadFloat(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TGeral.ReduzNomeDeCaminhoDeArquivo(Arquivo: String;
  MaxTam: Integer): String;
var
  Dir, Arq(*, Ext*): String;
  K, Tam: Integer;
begin
  if Length(Arquivo) < MaxTam then
    Result := Arquivo
  else begin
    Arq := ExtractFileName(Arquivo);
    //Ext := ExtractFileExt(Arquivo);
    Dir := ExtractFileDir(Arquivo);
    //
    K := Length(Arq);
    Tam := MaxTam - Length(Arq) - 5;
    if (K > MaxTam) or (Tam < 1) then
      Result := '...' + Copy(Arq, K - MaxTam + 4)
    else begin
      Result := Copy(Dir, 1, Tam) + '...\' + Arq;
    end;
  end;
end;

function TGeral.RemoveTagsHTML(const s: String): String;
var
  i: Integer;
  InTag: Boolean;
begin
  Result := '';
  InTag := False;
  for i := 1 to Length(s) do
  begin
    if s[i] = '<' then
      inTag := True
    else if s[i] = '>' then
      inTag := False
    else if not InTag then
      Result := Result + s[i];
  end;
end;

function TGeral.ReadAppKeyLM(const Key, AppTitle: String; KeyType: TKeyType;
  DefValue: Variant): Variant;
var
  r : TRegistry;

begin
  if VAR_NAO_USA_KEY_LOCAL_MACHINE then
  begin
    Result := ReadAppKeyCU(Key, AppTitle, KeyType, DefValue);
    Exit;
  end;
  r := TRegistry.Create(KEY_READ);
  r.RootKey := HKEY_LOCAL_MACHINE;
  try
    r.OpenKeyReadOnly('Software\'+AppTitle);
    Result := DefValue;
    if r.ValueExists(Key) then
    begin
      case KeyType of
        ktString: Result := r.ReadString(Key);
        ktBoolean: Result := r.ReadBool(Key);
        ktInteger: Result := r.ReadInteger(Key);
        ktCurrency: Result := r.ReadCurrency(Key);
        ktDate: Result := r.ReadDate(Key);
        ktTime: Result := r.ReadTime(Key);
        ktDateTime: Result := r.ReadDateTime(Key);
        ktFloat: Result := r.ReadFloat(Key);
        //ktBinary: Result := r.ReadBinaryData(Key);
      end;
    end;
  finally
    r.Free;
  end;
end;

function TGeral.PareceTelefoneBR(Telefone: String): Boolean;
var
  I: Integer;
  TxtA, TxtB: String;
begin
  TxtA := Trim(Telefone);
  TxtB := '';
  for I := 1 to Length(TxtA) do
  begin
    //if not (TxtA[I] in (['0'..'9',')','(','-',' ','x','X'])) then
{$IFDEF DELPHI12_UP}
      if not (CharInSet(TxtA[i], (['0'..'9',')','(','-',' ','x','X']))) then
{$ELSE}
      if not (TxtA[I] in (['0'..'9',')','(','-',' ','x','X'])) then
{$ENDIF}
      TxtB := TxtB + TxtA[I];
  end;
  Result := Length(TxtB) = 0;
end;

function TGeral.PastaEspecial(CodPasta: Integer): String;
var
  ItemIDList: PItemIDList;
  Txt: String;
  i: Integer;
begin
  // NÀO FUNCIONA!!!!
  // CSIDL_PROGRAMS - programas do usuário
  SetLength(Result, MAX_PATH);
  for i := 1 to MAX_PATH do
    Result[i] := #0;
  SHGetSpecialFolderLocation(Application.Handle, CodPasta, ItemIDList);
  SHGetPathFromIDList(ItemIDList, PChar(Result));
  Result := String(Result);
  Txt := '';
  for i := 1 to Length(Result) do
    if Ord(Result[i]) > 0 then
      Txt := Txt + Result[i]
    else Break;
  Result := Txt;
(*
  CSIDL_PERSONAL = $0005; { My Documents }
{$EXTERNALSYM CSIDL_PERSONAL}
  CSIDL_APPDATA = $001A; { Application Data, new for NT4 }
{$EXTERNALSYM CSIDL_APPDATA}

  CSIDL_LOCAL_APPDATA = $001C; { non roaming, user\Local Settings\Application Data }
{$EXTERNALSYM CSIDL_LOCAL_APPDATA}
  CSIDL_INTERNET_CACHE = $0020;
{$EXTERNALSYM CSIDL_INTERNET_CACHE}
  CSIDL_COOKIES = $0021;
{$EXTERNALSYM CSIDL_COOKIES}
  CSIDL_HISTORY = $0022;
{$EXTERNALSYM CSIDL_HISTORY}
  CSIDL_COMMON_APPDATA = $0023; { All Users\Application Data }
{$EXTERNALSYM CSIDL_COMMON_APPDATA}
  CSIDL_WINDOWS = $0024; { GetWindowsDirectory() }
{$EXTERNALSYM CSIDL_WINDOWS}
  CSIDL_SYSTEM = $0025; { GetSystemDirectory() }
{$EXTERNALSYM CSIDL_SYSTEM}
  CSIDL_PROGRAM_FILES = $0026; { C:\Program Files }
{$EXTERNALSYM CSIDL_PROGRAM_FILES}
  CSIDL_MYPICTURES = $0027; { My Pictures, new for Win2K }
{$EXTERNALSYM CSIDL_MYPICTURES}
  CSIDL_PROGRAM_FILES_COMMON = $002b; { C:\Program Files\Common }
{$EXTERNALSYM CSIDL_PROGRAM_FILES_COMMON}
  CSIDL_COMMON_DOCUMENTS = $002e; { All Users\Documents }
{$EXTERNALSYM CSIDL_COMMON_DOCUMENTS}

  CSIDL_FLAG_CREATE = $8000; { new for Win2K, or this in to force creation of folder }
{$EXTERNALSYM CSIDL_FLAG_CREATE}

  CSIDL_COMMON_ADMINTOOLS = $002f; { All Users\Start Menu\Programs\Administrative Tools }
{$EXTERNALSYM CSIDL_COMMON_ADMINTOOLS}
  CSIDL_ADMINTOOLS = $0030; { <user name>\Start Menu\Programs\Administrative Tools }
{$EXTERNALSYM CSIDL_ADMINTOOLS}
*)

end;

function TGeral.Periodo2000(Data: TDateTime): Integer;
var
  Ano, Mes, Dia: word;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  Result := ((Ano - 2000) * 12) + Mes;
end;

function TGeral.PeriodoToDate(Periodo, DiaX: Integer; Safe: Boolean;
DateEncodeType: TDateEncodeType): TDateTime;
var
  Ano, Mes: Word;
  M: Integer;
begin
  Ano := (Periodo div 12 )+ 2000;
  if Periodo < 0 then
  begin
    Ano := Ano - 1;
    Mes := 12 - (- Periodo) mod 12;
  end
  else
    Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano -1;
  end;
  if Safe then
    Result := EncodeDateSafe(Ano, Mes, DiaX, detLastDaySameMonth)
  else
    Result := EncodeDate(Ano, Mes, DiaX);
end;

function TGeral.PosEx(const SubStr, S: AnsiString; Offset: Cardinal): Integer;
var
  I,X: Integer;
  Len, LenSubStr: Integer;
begin
  if Offset = 1 then
    Result := Pos(SubStr, S)
  else
  begin
    I := Offset;
    LenSubStr := Length(SubStr);
    Len := Length(S) - LenSubStr + 1;
    while I <= Len do
    begin
      if S[I] = SubStr[1] then
      begin
        X := 1;
        while (X < LenSubStr) and (S[I + X] = SubStr[X + 1]) do
          Inc(X);
        if (X = LenSubStr) then
        begin
          Result := I;
          exit;
        end;
      end;
      Inc(I);
    end;
    Result := 0;
  end;
end;

function TGeral.PosLast(const SubStr, S: AnsiString): Integer;
var
  P : Integer;
begin
  Result := 0;
  P := Pos( SubStr, S);
  while P <> 0 do
  begin
     Result := P;
     P := PosEx( SubStr, S, P+1);
  end;
end;

function TGeral.PrimeiroDiaDoMes(Data: TDate): TDate;
begin
  Result := IncMonth(Data1(Data), 1);
end;

function TGeral.PTD(Periodo, DiaX: Integer): TDateTime;
var
  Ano, Mes: Word;
begin
  Ano := (Periodo div 12 )+ 2000;
  Mes := Periodo mod 12;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano -1;
  end;
  Result := EncodeDate(Ano, Mes, DiaX);
end;

function TGeral.TamanhoArquivo(Arquivo: String; AvisaSeNaoExiste: Boolean): Int64;
var
  FileStream : TFileStream;
begin
  if FileExists(Arquivo) then
  begin
    FileStream := TFileStream.Create(Arquivo, fmOpenRead, fmShareExclusive);
    try
      Result := FileStream.Size;
    finally
      if FileStream <> nil then
        FileStream.Free;
    end;
  end else
  begin
    Result := 0;
    if AvisaSeNaoExiste then
      MB_Erro('O Arquivo não foi localizado para ver seu tamanho:' +
      sLineBreak + sLineBreak + Arquivo);
  end;
end;

function CurrentYear: Word;
var
  SystemTime: TSystemTime;
begin
  GetLocalTime(SystemTime);
  Result := SystemTime.wYear;
end;

function GetDateOrder(const DateFormat: string): TDateOrder;
var
  I: Integer;
begin
  Result := doMDY;
  I := 1;
  while I <= Length(DateFormat) do
  begin
    case Chr(Ord(DateFormat[I]) and $DF) of
      'E': Result := doYMD;
      'Y': Result := doYMD;
      'M': Result := doMDY;
      'D': Result := doDMY;
    else
      Inc(I);
      Continue;
    end;
    Exit;
  end;
  Result := doMDY;
end;

procedure ScanToNumber(const S: string; var Pos: Integer);
begin
  //while (Pos <= Length(S)) and not (S[Pos] in ['0'..'9']) do
{$IFDEF DELPHI12_UP}
  while (Pos <= Length(S)) and not (CharInSet(S[Pos], ['0'..'9'])) do
{$ELSE}
  while (Pos <= Length(S)) and not (S[Pos] in ['0'..'9']) do
{$ENDIF}
  begin
    //if S[Pos] in LeadBytes then Inc(Pos);
{$IFDEF DELPHI12_UP}
    if CharInSet(S[Pos], LeadBytes) then Inc(Pos);
{$ELSE}
    if S[Pos] in LeadBytes then Inc(Pos);
{$ENDIF}
    Inc(Pos);
  end;
end;

function GetEraYearOffset(const Name: string): Integer;
var
  I: Integer;
begin
  Result := 0;
  {$IfDef VER230} //XE2
  for I := Low(EraNames) to High(EraNames) do
  begin
    if EraNames[I] = '' then Break;
    if AnsiStrPos(PChar(EraNames[I]), PChar(Name)) <> nil then
    begin
      Result := EraYearOffsets[I];
      Exit;
    end;
  end;
  {$Else} //Berlin, Tokyo, Alexandria
  for I := Low(FormatSettings.EraInfo) to High(FormatSettings.EraInfo) do
  begin
    if FormatSettings.EraInfo[I].EraName = '' then Break;
    if AnsiStrPos(PChar(FormatSettings.EraInfo[I].EraName), PChar(Name)) <> nil then
    begin
      Result := FormatSettings.GetEraYearOffset(FormatSettings.EraInfo[I].EraName);
      Exit;
    end;
  end;
  {$IfEnd}
end;

procedure ScanBlanks(const S: string; var Pos: Integer);
var
  I: Integer;
begin
  I := Pos;
  while (I <= Length(S)) and (S[I] = ' ') do Inc(I);
  Pos := I;
end;

function ScanNumber(const S: string; var Pos: Integer;
  var Number: Word; var CharCount: Byte): Boolean;
var
  I: Integer;
  N: Word;
begin
  Result := False;
  CharCount := 0;
  ScanBlanks(S, Pos);
  I := Pos;
  N := 0;
  //while (I <= Length(S)) and (S[I] in ['0'..'9']) and (N < 1000) do
{$IFDEF DELPHI12_UP}
  while (I <= Length(S)) and (CharInSet(S[I], ['0'..'9'])) and (N < 1000) do
{$ELSE}
  while (I <= Length(S)) and (S[I] in ['0'..'9']) and (N < 1000) do
{$ENDIF}
  begin
    N := N * 10 + (Ord(S[I]) - Ord('0'));
    Inc(I);
  end;
  if I > Pos then
  begin
    CharCount := I - Pos;
    Pos := I;
    Number := N;
    Result := True;
  end;
end;

function ScanChar(const S: string; var Pos: Integer; Ch: Char): Boolean;
begin
  Result := False;
  ScanBlanks(S, Pos);
  if (Pos <= Length(S)) and (S[Pos] = Ch) then
  begin
    Inc(Pos);
    Result := True;
  end;
end;

function TGeral.ScanDate(const S: string; var Pos: Integer;
  var Date: TDateTime): Boolean;
var
  DateOrder: TDateOrder;
  N1, N2, N3, Y, M, D: Word;
  L1, L2, L3, YearLen: Byte;
  EraName : string;
  EraYearOffset: Integer;
  CenturyBase: Integer;

  function EraToYear(Year: Integer): Integer;
  begin
    if SysLocale.PriLangID = LANG_KOREAN then
    begin
      if Year <= 99 then
        Inc(Year, (CurrentYear + Abs(EraYearOffset)) div 100 * 100);
      if EraYearOffset > 0 then
        EraYearOffset := -EraYearOffset;
    end
    else
      Dec(EraYearOffset);
    Result := Year + EraYearOffset;
  end;

begin
  Y := 0;
  M := 0;
  D := 0;
  YearLen := 0;
  Result := False;
  DateOrder := GetDateOrder({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat);
  EraYearOffset := 0;
  if {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat[1] = 'g' then  // skip over prefix text
  begin
    ScanToNumber(S, Pos);
    EraName := Trim(Copy(S, 1, Pos-1));
    EraYearOffset := GetEraYearOffset(EraName);
  end else
  begin
    if AnsiPos('e', {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat) > 0 then
    begin
      {$IfDef VER230} // XE2
      EraYearOffset := EraYearOffsets[1];
      {$ELse} //Berlin, Tokyo, Alexandria
      EraYearOffset := FormatSettings.GetEraYearOffset(FormatSettings.EraInfo[1].EraName);
      {$EndIf}
    end;
  end;
  if not (ScanNumber(S, Pos, N1, L1) and ScanChar(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DateSeparator) and
    ScanNumber(S, Pos, N2, L2)) then Exit;
  if ScanChar(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DateSeparator) then
  begin
    if not ScanNumber(S, Pos, N3, L3) then Exit;
    case DateOrder of
      doMDY: begin Y := N3; YearLen := L3; M := N1; D := N2; end;
      doDMY: begin Y := N3; YearLen := L3; M := N2; D := N1; end;
      doYMD: begin Y := N1; YearLen := L1; M := N2; D := N3; end;
    end;
    if EraYearOffset > 0 then
      Y := EraToYear(Y)
    else if (YearLen <= 2) then
    begin
      CenturyBase := CurrentYear - {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TwoDigitYearCenturyWindow;
      Inc(Y, CenturyBase div 100 * 100);
      if ({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TwoDigitYearCenturyWindow > 0) and (Y < CenturyBase) then
        Inc(Y, 100);
    end;
  end else
  begin
    Y := CurrentYear;
    if DateOrder = doDMY then
    begin
      D := N1; M := N2;
    end else
    begin
      M := N1; D := N2;
    end;
  end;
  ScanChar(S, Pos, {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}DateSeparator);
  ScanBlanks(S, Pos);
  if SysLocale.FarEast and (System.Pos('ddd', {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat) <> 0) then
  begin     // ignore trailing text
    //if ShortTimeFormat[1] in ['0'..'9'] then  // stop at time digit
{$IFDEF DELPHI12_UP}
    if CharInSet(FormatSettings.ShortTimeFormat[1], ['0'..'9']) then  // stop at time digit
{$ELSE}
    if ShortTimeFormat[1] in ['0'..'9'] then  // stop at time digit
{$ENDIF}
      ScanToNumber(S, Pos)
    else  // stop at time prefix
      repeat
        while (Pos <= Length(S)) and (S[Pos] <> ' ') do Inc(Pos);
        ScanBlanks(S, Pos);
      until (Pos > Length(S)) or
        (AnsiCompareText({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeAMString, Copy(S, Pos, Length({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimeAMString))) = 0) or
        (AnsiCompareText({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimePMString, Copy(S, Pos, Length({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}TimePMString))) = 0);
  end;
  Result := DoEncodeDate(Y, M, D, Date);
end;

function TGeral.VariantToString(AVar: OleVariant): string;
var
  i: integer;
  V: olevariant;
begin
  if AVar = null then
  begin
    Result := '';
  end else begin
//In the BeforeNavigate2 event of TWebBrowser, you receive the PostData
//and Header data as OleVariant.  If you simply assign the OleVariant type
//to a string, you may get part of the data or garbage.

//You can convert the OleVariant to String using this function:
    Result := '';
    if Variants.VarType(AVar) = (varVariant or varByRef) then
       V := Variant(TVarData(AVar).VPointer^)
    else V := AVar;

    if VarType(V) = (varByte or varArray) then
        try
          for i:=VarArrayLowBound(V,1) to VarArrayHighBound(V,1) do
             Result := Result + Chr(Byte(V[i]));
        except;
        end
      else Result := V;
//A nice FAQ site on TWebBrowser is:
//http://members.home.com/hfournier/
  end;
end;

////////////////////////////////////////////////////////////////////////////////
///  F O R M A T A Ç Õ E S
////////////////////////////////////////////////////////////////////////////////

function TGeral.FormataTelefone_TT(Telefone : AnsiString) : AnsiString;
var
  T: String;
  Tam, i: Integer;
  Res: String;
begin
  Res := String(Telefone);
  if PareceTelefoneBR(Res) then
  begin
    if Length(Telefone) > 0 then
    if Telefone[1] <> '+' then
    begin
      T := '';
      for i := 1 to Length(Telefone) do
        //if Telefone[i] in (['0'..'9']) then T := T + Telefone[i];
{$IFDEF DELPHI12_UP}
        if CharInSet(Telefone[i], (['0'..'9'])) then T := T + String(Telefone[i]);
{$ELSE}
        if Telefone[i] in (['0'..'9']) then T := T + String(Telefone[i]);
{$ENDIF}
      Tam := Length(T);
      if (Tam = 14) then
      begin
        if (T[1] + T[2] = '55') then //Brasil]
          Res := '0 xx ('+T[3]+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+T[10]+'-'+T[11]+T[12]+T[13]+T[14]
        else
          Res := '+'+T[1]+T[2]+' '+T[3]+T[4]+T[5]+' '+T[6]+T[7]+T[8]+T[9]+T[10]+'-'+T[11]+T[12]+T[13]+T[14]
      end
      else if (Tam = 13) and (T[1] = '0') then
        Res := T[1]+' xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      else if (Tam = 13) and (T[1] <> '0') then
      begin
        if (T[1] + T[2] = '55') then //Brasil]
        begin
          if (T[3] = '0') then
            Res := '0 xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
          else
            Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
        end else
          Res := '+'+T[1]+T[2]+' '+T[3]+T[4]+T[5]+' '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      end
      //
{     2012-08-31 telefone São Paulo
      else if (Tam = 12) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
}
      else if (Tam = 12) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      // Fim 2012-08-31
      else if (Tam = 12) and (T[1] <> '0')then
      begin
        if (T[1] + T[2] = '55') then //Brasil
          Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
        else
          Res := '+'+T[1]+T[2]+' '+T[3]+T[4]+' '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      end
      //
      else if (Tam = 11) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
{     2012-08-31 telefone São Paulo
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
}
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      // Fim 2012-08-31
      else if (Tam = 10) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 09) and (T[1] <> '0')then
      begin
        //Celulares com 9 dígito
        if (T[2] = '9') or (T[2] = '8') or (T[2] = '7') or (T[2] = '6') then
          Res := '0 xx (xx) ' + T[1]+T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
        else
          Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9];
      end
      else if (Tam = 09) and (T[1] = '0')then
        Res := '0 xx (xx) '+T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 08) and (T[1] <> '0')then
        Res := '0 xx (xx) '+T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 08) and (T[1] = '0')then
        Res := '0 xx (xx) '+T[2]+T[3]+T[4]+'- '+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 07) and (T[1] <> '0')then
        Res := '0 xx (xx) '+T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7];
    end;
  end;
  Result := AnsiString(Res);
end;

function TGeral.FormataTelefone_TT_Curto(Telefone : AnsiString) : AnsiString;
var
  T, Res: String;
  Tam, i: Integer;
begin
  Res := String(Telefone);
  if PareceTelefoneBR(Res) then
  begin
    if Length(Telefone) > 0 then
    if Telefone[1] <> '+' then
    begin
      T := '';
      for i := 1 to Length(Telefone) do
        //if Telefone[i] in (['0'..'9']) then T := T + Telefone[i];
{$IFDEF DELPHI12_UP}
        if CharInSet(Telefone[i], (['0'..'9'])) then T := T + String(Telefone[i]);
{$ELSE}
        if Telefone[i] in (['0'..'9']) then T := T + Telefone[i];
{$ENDIF}
      Tam := Length(T);
      if (Tam = 14) then
      begin
        if (T[1] + T[2] = '55') then //Brasil
          Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+T[10]+'-'+T[11]+T[12]+T[13]+T[14]
        else
          Res := T[1]+T[2]+' '+T[4]+T[5]+' '+T[6]+T[7]+T[8]+T[9]+T[10]+'-'+T[11]+T[12]+T[13]+T[14]
      end;
      if (Tam = 13) and (T[1] = '0') then
        Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      else if (Tam = 13) and (T[1] <> '0') then
      begin
        if (T[1] + T[2] = '55') then //Brasil
        begin
          if (T[3] = '0') then
            Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
          else
            Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
        end else
          Res := T[1]+T[2]+' '+T[4]+T[5]+' '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      end
      //
(*     2012-08-31 telefone São Paulo
      else if (Tam = 12) and (T[1] = '0')then
        Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
*)
      else if (Tam = 12) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      // Fim 2012-08-31
      else if (Tam = 12) and (T[1] <> '0')then
      begin
        if (T[1] + T[2] = '55') then //Brasil
          Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
        else
          Res := '+'+T[1]+T[2]+' '+T[3]+T[4]+' '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      end
      //
      else if (Tam = 11) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
(*     2012-08-31 telefone São Paulo
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
*)
      else if (Tam = 11) and (T[1] <> '0')then
         Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      // Fim 2012-08-31
      else if (Tam = 10) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] <> '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 09) and (T[1] <> '0')then
      begin
        //Celulares com 9 dígito
        if (T[2] = '9') or (T[2] = '8') or (T[2] = '7') or (T[2] = '6') then
          Res := T[1]+T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
        else
          Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      end
      else if (Tam = 09) and (T[1] = '0')then
        Res := T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 08) and (T[1] <> '0')then
        Res := T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 08) and (T[1] = '0')then
        Res := T[2]+T[3]+T[4]+'- '+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 07) and (T[1] <> '0')then
        Res := T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7];
    end;
  end;
  Result := AnsiString(Res);
end;

function TGeral.FormataTelefone_TT_NFe(Telefone : String) : String;
var
  n: Integer;
begin
  Result := SoNumero_TT(Telefone);
  n := Length(Result);
  if n > 11 then
    Result := Copy(Result, 1 + n - 10, 10);
end;

function TGeral.FormataUF(UF: Variant): String;
var
  Estado: Integer;
begin
  if VarType(UF) <> vtInteger then
    Estado := GetCodigoUF_da_SiglaUF(UF)
  else
    Estado := UF;
  //
  Result := GetSiglaUF_do_CodigoUF(Estado);
end;

function TGeral.FormataNumeroDeRua(Rua: AnsiString; Numero: Variant; Nulo: Boolean): AnsiString;
var
  Num: String;
begin
// 2012/05/17 > Rua: String
  if Trim(String(Rua)) = '' then
    Result := ''
  else begin
// FIM 2012/05/17
    if VarType(Numero)  <> vtInteger then
      Num := SoNumero_TT(Numero)
    else
      Num := SoNumero_TT(IntToStr(Numero));
    if (Num = '0') or (Num = '') then
    begin
      if Nulo then Result := '' else Result := 'S/N';
    end else Result := AnsiString(Num);
  end;
end;

function TGeral.Data1(Data: TDate): TDate;
var
  Ano, Mes, Dia : word;
begin
  if Data < 32 then
  begin
    Data1 := 0;
    Exit;
  end;
  Decodedate(Data, Ano, Mes, Dia);
  Mes := Mes -1;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano - 1;
  end;
  Data1 := Encodedate(Ano, Mes, 1);
end;

function TGeral.DataToMez(Data: TDateTime): String;
var
  Ano, Mes, Dia: Word;
begin
  if Data < 1 then Result := '' else
  begin
    Decodedate(Data, Ano, Mes, Dia);
    Result := FormatFloat('0', ((Ano - 2000) * 100) + Mes);
  end;
end;

function TGeral.DecimalPonto(sValor: string): string;
begin
  Result := StringReplace(sValor, '.', '',[rfReplaceAll]);
  Result := StringReplace(Result, ',', '.',[rfReplaceAll]);
end;

procedure TGeral.DefineFormatacoes();
begin
  if {$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ShortDateFormat = 'M/d/yyyy' then
  VAR_FORMATDATEi := 'mm/dd/yy' else VAR_FORMATDATEi := 'dd/mm/yy';
  VAR_FORMATDATE0 := 'yy/mm/dd';
  VAR_FORMATDATE  := 'yyyy-mm-dd'; // mudei 2011-05-21
  VAR_FORMATDATE2 := 'dd/mm/yyyy';
  VAR_FORMATDATE3 := 'dd/mm/yy'; // para labels, não usar para cálculo!
  VAR_FORMATDATE5 := 'mmm/yy'; // para labels, não usar para cálculo!
  VAR_FORMATDATE4 := 'yyyymmdd';
  VAR_FORMATDATE6 := 'dd "de" mmmm "de" yyyy';
  VAR_FORMATDATE7 := 'mmmm "de" yyyy';
  VAR_FORMATDATE8 := 'dd "de" mmmm "de" yyyy "às" hh:nn:ss';
  VAR_FORMATDATE9 := 'yyyy/mm/dd hh:nn:ss';
  VAR_FORMATDATE10 := 'mm/dd/YYYY';
  VAR_FORMATDATE11 := 'dd/mm/yyyy hh:nn';
  VAR_FORMATDATE12 := 'dd/mm';
  VAR_FORMATDATE13 := 'yymm'; // Mez
  VAR_FORMATDATE14 := 'mm/yyyy'; // Mes
  VAR_FORMATDATE15 := 'dddd, dd "de" mmmm "de" yyyy'; // Mes
  VAR_FORMATDATE16 := 'dd';
  VAR_FORMATDATE17 := 'mmmm/yy';
  VAR_FORMATDATE18 := 'mmm/yy';
  VAR_FORMATDATE19 := 'mm/yy';
  VAR_FORMATDATE20 := 'yyyymm';
  VAR_FORMATDATE21 := 'yy';
  VAR_FORMATDATE22 := 'mm';
  VAR_FORMATDATE23 := 'ddmmyyyy';
  VAR_FORMATDATE24 := 'mmyyyy';
  VAR_FORMATDATE25 := 'yyyy';
  VAR_FORMATDATE26 := 'yymmdd_hhnnss';

  VAR_FORMATTIMESTAMP4 := 'mm/yy';
  VAR_FORMATTIME := 'hh:nn:ss';
  VAR_FORMATTIME2 := 'hh:nn';
  VAR_FORMATTIME3 := 'hhnn';
  VAR_FORMATTIME4 := 'hh:nn:ss:zzz';
  VAR_FORMATTIME5 := 'hh"h "nn"min "ss "seg"';
  VAR_FORMATTIME6 := 'hh:nn:ss.zzz';
  VAR_FORMATDATE15 := 'dddd, dd "de" mmmm "de" yyyy'; // Mes
  VAR_FORMATDATETIME   := 'dd/mm/yyyy hh:nn:ss';
  VAR_FORMATDATETIME1  := 'dd/mm/yy hh:nn:ss';
  VAR_FORMATDATETIME2  := 'yyyy/mm/dd hh:nn:ss';
  VAR_FORMATDATETIME3  := 'dd/mm/yy hh:nn';
  VAR_FORMATDATETIME4  := 'yyyy-mm-dd hh:nn:ss';
  VAR_FORMATDATECB4    := 'YYYYMMDDhh:nn:ss';
  //
  VAR_FORMATFLOAT0 := '#,###,###,##0';
  VAR_FORMATFLOAT1 := '#,###,###,##0.0';
  VAR_FORMATFLOAT2 := '#,###,###,##0.00';
  VAR_FORMATFLOAT3 := '#,###,###,##0.000';
  VAR_FORMATFLOAT4 := '#,###,###,##0.0000';
  VAR_FORMATFLOAT5 := '#,###,###,##0.00000';
  VAR_FORMATFLOAT6 := '#,###,###,##0.000000';
  VAR_FORMATFLOAT7 := '#,###,###,##0.0000000';
  VAR_FORMATFLOAT8 := '#,###,###,##0.00000000';
  VAR_FORMATFLOAT9 := '#,###,###,##0.000000000';
  VAR_FORMATFLOAT10 := '#,###,###,##0.0000000000';
end;

function TGeral.DelKeyAppKeyCU(const App: String): Boolean;
var
  r : TRegistry;
begin
  Result := false;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_CURRENT_USER;
  try
    Result := r.DeleteKey('Software\' + App);
  finally
    r.Free;
  end;
end;

function TGeral.DelValAppKeyCU(const App, Key: String): Boolean;
var
  r : TRegistry;
begin
  Result := False;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_CURRENT_USER;
  try
    if r.OpenKey('Software\' + App, True) then
      Result := r.DeleteValue(Key);
  finally
    r.Free;
  end;
end;

{$WARNINGS OFF}
function TGeral.FatID_to_ide_mod(const FatID: Integer;
  var ide_mod: Integer): Boolean;
const
  sProcName = 'TGeral.FatID_to_ide_mod()';
begin
  case FatID of
    1: ide_mod := 55;
    2: ide_mod := 65;
    else
    begin
      ide_mod := 0;
      MB_Erro('FatID não implementado em ' + sProcName);
    end;
  end;
  Result := ide_mod <> 0;
end;

function TGeral.FDT(Data: TDateTime; Tipo: Integer; Nulo: Boolean): String;
var
  Res: String;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta função mudar na WEB em:                        ///
  ///  libraries -> Dmk_datetime.php -> fdt                                 ///
  /////////////////////////////////////////////////////////////////////////////

  Result := '';
  Res := '';
  if Nulo and (Data< (1/86400)) and (Tipo <> 111) then Exit; // menos que um segundo
  //Res := FormatDateTime('yyyy/mm/yy hh:nn:ss:zzz', Data);
  case Tipo of
    00: Res := FormatDateTime(VAR_FORMATDATETIME, Data);
    01: Res := FormatDateTime(VAR_FORMATDATE, Data);
    02: Res := FormatDateTime(VAR_FORMATDATE2, Data);
    03: Res := FormatDateTime(VAR_FORMATDATE3, Data);
    04: Res := FormatDateTime(VAR_FORMATDATE4, Data);
    05: Res := FormatDateTime(VAR_FORMATDATE5, Data);
    06: Res := FormatDateTime(VAR_FORMATDATE6, Data);
    07: Res := FormatDateTime(VAR_FORMATDATE7, Data);
    08: Res := FormatDateTime(VAR_FORMATDATE8, Data);
    09: Res := FormatDateTime(VAR_FORMATDATE9, Data);
    10: Res := FormatDateTime(VAR_FORMATDATE10, Data);
    11: Res := FormatDateTime(VAR_FORMATDATE11, Data);
    12: Res := FormatDateTime(VAR_FORMATDATE12, Data);
    13: Res := FormatDateTime(VAR_FORMATDATE13, Data);
    14: Res := FormatDateTime(VAR_FORMATDATE14, Data);
    15: Res := FormatDateTime(VAR_FORMATDATE15, Data);
    16: Res := FormatDateTime(VAR_FORMATDATE16, Data);
    17: Res := FormatDateTime(VAR_FORMATDATE17, Data);
    18: Res := FormatDateTime(VAR_FORMATDATE18, Data);
    19: Res := FormatDateTime(VAR_FORMATDATE19, Data);
    20: Res := FormatDateTime(VAR_FORMATDATE20, Data);
    21: Res := FormatDateTime(VAR_FORMATDATE21, Data);
    22: Res := FormatDateTime(VAR_FORMATDATE22, Data);
    23: Res := FormatDateTime(VAR_FORMATDATE23, Data);
    24: Res := FormatDateTime(VAR_FORMATDATE24, Data);
    25: Res := FormatDateTime(VAR_FORMATDATE25, Data); // 'YYYY'
    26: Res := FormatDateTime(VAR_FORMATDATE26, Data); // 'yymmdd_hhnnss'
    //
   100: Res := FormatDateTime(VAR_FORMATTIME,   Data);
   102: Res := FormatDateTime(VAR_FORMATTIME2,  Data);
   103: Res := FormatDateTime(VAR_FORMATTIME3,  Data);
   104: Res := FormatDateTime(VAR_FORMATTIMESTAMP4, Data);
   105: Res := FormatDateTime(VAR_FORMATDATETIME2,  Data);
   106: Res := FormatDateTime(VAR_FORMATDATETIME1,  Data);
   107: Res := FormatDateTime(VAR_FORMATDATETIME3,  Data);
   108: Res := FormatDateTime(VAR_FORMATTIME4,  Data);
   109: Res := FormatDateTime(VAR_FORMATDATETIME4,  Data);
   110: Res := FormatDateTime(VAR_FORMATTIME5,  Data);
   111: Res := FormatDateTime(VAR_FORMATTIME6,  Data);
    else Res := '* ERRO Res *'
  end;
  if Data = 0 then
  begin
    // data no formato de sistema
    // NÃO MEXER AQUI - estraga SQLs
    case Tipo of
      1: Res := '0000-00-00';
      // NFe 3.10 UTC TZD
      102: Res := '00:00';
      109: Res := '0000-00-00 00:00:00';
      111: Res := '00:00:00.000';
      else Res := '';
    end;
  end;
  // 2012-02-18
  //if Data < 0 then
    //Res := '-' + Res;
  // fim 2012-02-18
  Result := Res;
end;

function TGeral.FDT_AAAAMM(AAAAMM, Tipo: Integer; Nulo: Boolean): String;
var
  Data: TDateTime;
begin
  if AAAAMM = 0 then
    Data := 0
  else
    Data := EncodeDate(AAAAMM div 100, AAAAMM mod 100, 1);
  Result := FDT(Data, Tipo);
end;

function TGeral.FDT_Aspas(Data: TDateTime; Tipo: Integer;
  Nulo: Boolean): String;
begin
  Result := '"' + FDT(Data, Tipo, Nulo) + '"';
end;

function TGeral.FDT_TP_Ed(Data: TDateTime; Hora: String; Nulo: Boolean = False): String;
var
  h: String;
begin
  h := ' ' + Trim(Hora);
  if Length(h) = 6 then
    h :=  h + ':00';
  Result := FDT(Data, 1) + h;
end;

{$WARNINGS ON}

function TGeral.FF0(Numero: Integer): String;
begin
  Result := FormatFloat('0', Numero);
end;

function TGeral.FF0(Numero: Int64): String;
begin
  Result := FormatFloat('0', Numero);
end;

function TGeral.FF0_EmptyZero(Numero: Integer): String;
begin
  if Numero = 0 then
    Result := ''
  else
    Result := FF0(Numero);
end;

function TGeral.FFN_Dot(Numero: Double): String;
begin
  Result := FloatToStr(Numero);
  Result := Substitui(Result, ',', '.');
end;

function TGeral.FI64(Numero: Int64): String;
begin
  Result := IntToStr(Numero);
end;

function TGeral.FFI(Numero: Double): String;
begin
  Result := FormatFloat('0', Numero);
end;

function TGeral.FFN_Inv(Numero, Digitos: Integer): String;
begin
  Result := FF0(Numero);
  //
  while Length(Result) < Digitos  do
    Result := Result + '0';
end;

function TGeral.FFN(Numero, Digitos: Integer): String;
var
  //I: Integer;
  //Zeros: String;
  Fmt: String;
begin
  // Mudei 2012-05-10
{
  case Digitos of
    0:
    begin
      if Numero = 0 then
        Result := ''
      else
        Result := FormatFloat('0', Numero);
    end;
    01: Result := FormatFloat('0', Numero);
    02: Result := FormatFloat('00', Numero);
    03: Result := FormatFloat('000', Numero);
    04: Result := FormatFloat('0000', Numero);
    05: Result := FormatFloat('00000', Numero);
    06: Result := FormatFloat('000000', Numero);
    07: Result := FormatFloat('0000000', Numero);
    08: Result := FormatFloat('00000000', Numero);
    09: Result := FormatFloat('000000000', Numero);
    10: Result := FormatFloat('0000000000', Numero);
    11: Result := FormatFloat('00000000000', Numero);
    12: Result := FormatFloat('000000000000', Numero);
    13: Result := FormatFloat('0000000000000', Numero);
    14: Result := FormatFloat('00000000000000', Numero);
    15: Result := FormatFloat('000000000000000', Numero);
    16: Result := FormatFloat('0000000000000000', Numero);
    17: Result := FormatFloat('00000000000000000', Numero);
    18: Result := FormatFloat('000000000000000000', Numero);
    19: Result := FormatFloat('0000000000000000000', Numero);
    20: Result := FormatFloat('00000000000000000000', Numero);
    else
    begin
      Zeros := '00000000000000000000';
      for I := 21 to Digitos do
        Zeros := Zeros + '0';
      Result := FormatFloat(Zeros, Numero);
    end;
  end;
}
  Fmt := GetNumIntsFmt(Digitos);
  if Fmt = '' then
    Result := ''
  else
    Result := FormatFloat(Fmt, Numero);
end;

function TGeral.FFF(Numero: Double; Digitos: Integer): String;
var
  //I: Integer;
  //Zeros: String;
  Fmt: String;
begin
  // Mudei 2012-05-10
{
  case Digitos of
    0:
    begin
      if Numero = 0 then
        Result := ''
      else
        Result := FormatFloat('0', Numero);
    end;
    01: Result := FormatFloat('0', Numero);
    02: Result := FormatFloat('00', Numero);
    03: Result := FormatFloat('000', Numero);
    04: Result := FormatFloat('0000', Numero);
    05: Result := FormatFloat('00000', Numero);
    06: Result := FormatFloat('000000', Numero);
    07: Result := FormatFloat('0000000', Numero);
    08: Result := FormatFloat('00000000', Numero);
    09: Result := FormatFloat('000000000', Numero);
    10: Result := FormatFloat('0000000000', Numero);
    11: Result := FormatFloat('00000000000', Numero);
    12: Result := FormatFloat('000000000000', Numero);
    13: Result := FormatFloat('0000000000000', Numero);
    14: Result := FormatFloat('00000000000000', Numero);
    15: Result := FormatFloat('000000000000000', Numero);
    16: Result := FormatFloat('0000000000000000', Numero);
    17: Result := FormatFloat('00000000000000000', Numero);
    18: Result := FormatFloat('000000000000000000', Numero);
    19: Result := FormatFloat('0000000000000000000', Numero);
    20: Result := FormatFloat('00000000000000000000', Numero);
    else
    begin
      Zeros := '00000000000000000000';
      for I := 21 to Digitos do
        Zeros := Zeros + '0';
      Result := FormatFloat(Zeros, Numero);
    end;
  end;
}
  Fmt := GetNumIntsFmt(Digitos);
  if Fmt = '' then
    Result := ''
  else
    Result := FormatFloat(Fmt, Numero);
end;

function TGeral.FFT(Numero: Double; Casas: Integer; Sinal: TSinal): String;
begin
  Result := TFT(FormatFloat('0.00000000000000000000', Numero), Casas, Sinal);
end;

function TGeral.FFT_Dot(Numero: Double; Casas: Integer; Sinal: TSinal): String;
var
  Texto: String;
  I: Integer;
begin
  Result := '';
  Texto := TFT(FormatFloat('0.00000000000000000000', Numero), Casas, Sinal);
  for I := 1 to Length(Texto) do
  begin
    if Texto[I] <> '.' then
    begin
      if Texto[I] = ',' then
        Result := Result + '.'
      else
        Result := Result + Texto[I];
    end;
  end;
end;

function TGeral.FFT_MinMaxCasas(Numero: Double; CasasMin,
  CasasMax: Integer): String;
var
  P, K: Integer;
begin
  Result := Geral.TFT(FormatFloat('0.00000000000000000000', Numero), CasasMax, siNegativo);
  K := CasasMax;
  P := Length(Result);
  while (K > CasasMin) and (Result[P] = '0') do
  begin
    K := K - 1;
    P := P - 1;
    Result := Copy(Result, 1, P);
  end;

  P := Length(Result);
  //ile (P > 0) and not (Result[P] in ['0'..'9']) do
{$IFDEF DELPHI12_UP}
  while (P > 0) and not (CharInSet(Result[P], ['0'..'9'])) do
{$ELSE}
  while (P > 0) and not (Result[P] in ['0'..'9']) do
{$ENDIF}
  begin
    P := P - 1;
    Result := Copy(Result, 1, P);
  end;
end;

function TGeral.FFT_Null(Numero: Double; Casas: Integer; Sinal: TSinal): String;
var
  I: Int64;
begin
  I := Round(Numero * IntPower(10, Casas));
  if I = 0 then
    Result := ''
  else
    Result := Geral.TFT(FormatFloat('0.00000000000000000000', Numero), Casas, Sinal);
end;

function TGeral.FF_0(Numero, Tamanho: Integer): String;
begin
  Result := FormatFloat('0', Numero);
  while Length(Result) < Tamanho do
    Result := ' ' + Result;
end;

function TGeral.FF_0(Numero: Int64; Tamanho: Integer): String;
begin
  Result := FormatFloat('0', Numero);
  while Length(Result) < Tamanho do
    Result := ' ' + Result;
end;

function TGeral.FormataCBO2002(CBO2002: String): String;
var
  Txt: String;
begin
  Txt := SoNumero_TT(CBO2002);
  Txt := FormatFloat('000000', IMV(Txt));
  Insert('-', Txt, 5);
  Result := Txt;
end;

function TGeral.FormataCEP_TT(CEP: String; Separador: String = '-';
Default: String = ''): String;
var
  Numeros: String;
begin
  if (CEP = '0') or (CEP = '') or (CEP = '00000-000')then Result := Default else
  begin
    Numeros := SoNumero_TT(CEP);
    if Length(Numeros) <= 5 then Numeros := Numeros + '000';
    while Length(Numeros) < 8 do Numeros := '0' + Numeros;
    Result := Copy(Numeros, 1, 5) + Separador + Copy(Numeros, 6, 3);
  end;
end;

function TGeral.FormataCFOP(CFOP: String): String;
var
  Res: String;
begin
  Res := SoNumero_TT(CFOP);
  if Length(Res) = 4 then
    Result := Res[1] + '.' + Res[2] + Res[3] + Res[4]
  else
    Result := CFOP;
end;

function TGeral.FormataChaveNFe(ChaveNFe: String; Como: TComoFmtChNFe): String;
var
  chave, cDV, Separador: String;
  n: Integer;
begin
  Result := SoNumero_TT(Trim(ChaveNFe));
  if Result = '' then
    Exit;
  n := Length(Result);
  if n <> 44 then
  begin
    MB_Aviso('A chave da NF-e "' + ChaveNFe + '" é inválida!' + sLineBreak +
    'A chave está com ' + IntToStr(n) + ' caracteres quando deveria ter 44.');
    Exit;
  end;
  chave := Copy(Result, 01, 43);
  cDV   := Copy(Result, 44, 01);
  //
  if cDV <> Modulo11_2a9_Back(chave, 0) then
  begin
    MB_Aviso('A chave da NF-e "' + ChaveNFe + '" é inválida!' + sLineBreak +
    'Seu dígito verificador não confere!.');
    Exit;
  end;
  case Como of
    cfcnDANFE:
    begin
      Separador := ' ';
      Result :=
        Copy(Result, 01, 04) + Separador +
        Copy(Result, 05, 04) + Separador +
        Copy(Result, 09, 04) + Separador +
        Copy(Result, 13, 04) + Separador +
        Copy(Result, 17, 04) + Separador +
        Copy(Result, 21, 04) + Separador +
        Copy(Result, 25, 04) + Separador +
        Copy(Result, 29, 04) + Separador +
        Copy(Result, 33, 04) + Separador +
        Copy(Result, 37, 04) + Separador +
        Copy(Result, 41, 04);
    end;
    cfcnFrendly:
    //15-1203-04.333.952/0001-88-55-009-000.000.202-176.112.641-4
    begin
      Result :=
      Copy(Result, 01, 02) + '-' +
      Copy(Result, 03, 04) + '-' +
      Copy(Result, 07, 02) + '.' +
      Copy(Result, 09, 03) + '.' +
      Copy(Result, 11, 03) + '/' +
      Copy(Result, 14, 04) + '-' +
      Copy(Result, 18, 02) + '-' +
      Copy(Result, 20, 02) + '-' +
      Copy(Result, 22, 03) + '-' +
      Copy(Result, 25, 03) + '.' +
      Copy(Result, 28, 03) + '.' +
      Copy(Result, 31, 03) + '-' +
      Copy(Result, 34, 03) + '.' +
      Copy(Result, 37, 03) + '.' +
      Copy(Result, 41, 03) + '-' +
      Copy(Result, 44);
    end;
  end;

end;

function TGeral.FormataCEP_NN(CEP: Integer): Integer;
begin
  Result := IMV(FormatFloat('0', CEP));
end;

function TGeral.FormataCEP_NT(CEP: Real): AnsiString;
var
  Numeros: String;
begin
  Numeros := SoNumero_TT(FloatToStr(CEP));
  if Numeros = '0' then Result := '' else
  begin
    if Length(Numeros)<6 then Numeros := Numeros + '000';
    while Length(Numeros)<8 do Numeros := '0' + Numeros;
    Result := AnsiString(Copy(Numeros, 1, 5) + '-' + Copy(String(Numeros), 6, 3));
  end;
end;

function TGeral.CNPJ_Valida(const EdCNPJ: TWinControl; var CNPJ: String): Boolean;
var
  Num, CNPJ1, CNPJ2: AnsiString;
begin
  Result := False;
  CNPJ1 := AnsiString(SoNumero_TT(String(CNPJ)));
  if CNPJ1 <> '' then
  begin
    Num := CalculaCNPJCPF(CNPJ1);
    if FormataCNPJ_TFT(CNPJ1) <> Num then
    begin
      ShowMessage('Valor inválido para CNPJ / CPF');
      if EdCNPJ <> nil then
      try
        if TWinControl(EdCNPJ).Enabled
        and TWinControl(EdCNPJ).Visible
        and (TCustomEdit(EdCNPJ).ReadOnly = False)
        then
          TWinControl(EdCNPJ).SetFocus;
      except
        ;//
      end;
    end else begin
      CNPJ2 := FormataCNPJ_TT(CNPJ1);
      CNPJ := String(CNPJ2);
      Result := True;
    end;
  end;
end;

function TGeral.CodigoUF_to_cUF_IBGE(UF: Integer): Integer;
begin
  if UF < 0 then UF := UF * -1;
  case UF of
     01: Result := 12;//'AC';
     02: Result := 27;//'AL';
     03: Result := 13;//'AM';
     04: Result := 16;//'AP';
     05: Result := 29;//'BA';
     06: Result := 23;//'CE';
     07: Result := 53;//'DF';
     08: Result := 32;//'ES';
     09: Result := 52;//'GO';
     10: Result := 21;//'MA';
     11: Result := 31;//'MG';
     12: Result := 50;//'MS';
     13: Result := 51;//'MT';
     14: Result := 15;//'PA';
     15: Result := 25;//'PB';
     16: Result := 26;//'PE';
     17: Result := 22;//'PI';
     18: Result := 41;//'PR';
     19: Result := 33;//'RJ';
     20: Result := 24;//'RN';
     21: Result := 11;//'RO';
     22: Result := 14;//'RR';
     23: Result := 43;//'RS';
     24: Result := 42;//'SC';
     25: Result := 28;//'SE';
     26: Result := 35;//'SP';
     27: Result := 17;//'TO';
     else Result := 0;
  end;
end;

function TGeral.ColetivoDeUnidadedeTempo(Quantidade: Integer; TipoPeriodo:
  TColetivoUnidadeTempo; dmkCharCase: TdmkCharCase): String;
begin
  Result := '';
  case TipoPeriodo of
    cutDias:
    begin
      case Quantidade of
        1: Result := 'diário';
        7: Result := 'semanal';
        10: Result := 'decendial';
        15: Result := 'quinzenal';
        else Result := IntToStr(Quantidade) + ' dias';
      end;
    end;
    cutSemanas:
    begin
      case Quantidade of
        1: Result := 'semanal';
        2: Result := 'bisemanal';
        3: Result := 'trisemanal';
        4: Result := 'quadrisemanal';
        5: Result := 'quinquisemanal';
        6: Result := 'sextisemanal';
        7: Result := 'septasemanal';
        8: Result := 'octasemanal';
        9: Result := 'nonasemanal';
        10: Result := 'decasemanal';
        11: Result := 'andecasemanal';
        //12: Result := 'duodecasemanal';
        else Result := IntToStr(Quantidade) + ' meses';
      end;
    end;
    cutMeses:
    begin
      case Quantidade of
        1: Result := 'mensal';
        2: Result := 'bimestral';
        3: Result := 'trimestral';
        4: Result := 'quadrimestral';
        5: Result := 'quinquimestral';
        6: Result := 'semestral';
        7: Result := 'septamestral';
        8: Result := 'octamestral';
        9: Result := 'nonamestral';
        10: Result := 'decamestral';
        11: Result := 'andecamestral';
        12: Result := 'anual';
        else Result := IntToStr(Quantidade) + ' meses';
      end;
    end;
    cutAnos:
    begin
      case Quantidade of
        1: Result := 'anual';
        2: Result := 'bianual';
        3: Result := 'trianual';
        4: Result := 'quadrianual';
        5: Result := 'quinquanual';
        6: Result := 'sextanual';
        7: Result := 'septanual';
        8: Result := 'octanual';
        9: Result := 'nonanual';
        10: Result := 'decanual';
        11: Result := 'andecanual';
        //12: Result := 'duodecanual';
        100: Result := 'século';
        1000: Result := 'milênio';
        else Result := IntToStr(Quantidade) + ' anos';
      end;
    end;
  end;
  if Result <> '' then
  begin
    case dmkCharCase of
      dmkccLowerCase: Result := AnsiLowerCase(Result);
      dmkccUpperCase: Result := AnsiUpperCase(Result);
      dmkccFistUpper: Result := AnsiUpperCase(Result[1]) + AnsiLowerCase(Copy(Result, 2));
    end;
  end;
end;

function TGeral.ConfereUFeMunici_IBGE(xUF: String; CodMunici: Integer;
TipoEndereco: String): Boolean;
var
  cUF: Integer;
  xMunici: String;
begin
  cUF := GetCodigoUF_IBGE_DTB_da_SiglaUF(xUF);
  if (*(cUF > 0) or *)(CodMunici > 0) then
  begin
    xMunici := FormatFloat('0000000', CodMunici);
    if Copy(xMunici, 1, 2) <> FormatFloat('00', cUF) then
    begin
      Result := False;
      // A p p l i c a t i o n . M e s s a g e B o x gera erro
      //ShowMessage(PChar('Código do município não confere com a UF! > ' +
      //TipoEndereco));
      MB_Aviso('Código do município não confere com a UF! > ' + TipoEndereco);
    end else Result := True;
  end else Result := True;
end;

function TGeral.ConvertDecimalCharactersText(const Texto: String; var Res: String): Boolean;
  procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
  begin
     ListOfStrings.Clear;
     ListOfStrings.Delimiter       := Delimiter;
     ListOfStrings.StrictDelimiter := True;
     ListOfStrings.DelimitedText   := Str;
  end;
var
   OutPutList: TStringList;
   I: Integer;
   x, s: String;
begin
  Result := False;
  Res := '';
  OutPutList := TStringList.Create;
  try
    Split(';', Texto, OutPutList) ;
    for I := 0 to OutPutList.Count - 1 do
    begin
      x := OutPutList[I];
      if pos('&#', x) = 1 then
      begin
        s := Copy(x, 3);
        if s <> '' then
          Res := Res + Char(StrToInt(s))
        else
          Res := Res + x;
      end else
        Res := Res + x;
    end;
    Result := True;
  finally
    OutPutList.Free;
  end;
end;

function TGeral.ConverteArea(Area: Double; CalcType: TCalcType; CalcFrac: TCalcFrac): Double;
const
  Conv = 0.09290304;
{
SQL
SELECT  AreaM2,
FLOOR((AreaM2 / 0.09290304)) +
FLOOR(((MOD((AreaM2 / 0.09290304), 1)) +
  0.12499) * 4) * 0.25 AreaP2
}
begin
  Result := 0;
  case CalcType of
    ctM2toP2:
    begin
      case CalcFrac of
        cfCento:  Result := Area / Conv;
        cfQuarto: Result := QuadriVal(Area / Conv);
      end;
    end;
    ctP2toM2:
    begin
      case CalcFrac of
        cfCento:  Result := Area * Conv;
        cfQuarto: Result := QuadriVal(Area * Conv);
      end;
    end;
  end;
end;

function TGeral.ConvertePeso(Peso: Double; CalcType: TCalcType; CalcFrac: TCalcFrac): Double;
const
  Conv = 0.45359237;
{
var
  Valor, Sobra, Frac : Double;
}
begin
  Result := 0;
  case CalcType of
    ctKGtoLB:
    begin
      case CalcFrac of
        cfCento:  Result := Peso / Conv;
        cfQuarto: Result := QuadriVal(Peso / Conv);
      end;
    end;
    ctLBtoKG:
    begin
      case CalcFrac of
        cfCento:  Result := Peso * Conv;
        cfQuarto: Result := QuadriVal(Peso * Conv);
      end;
    end;
  end;
end;

function TGeral.ConverteValor(Valor: Double; CalcType: TCalcType;
  CalcFrac: TCalcFrac): Double;
begin
  Result := 0;
  case CalcType of
    ctM2toP2, ctP2toM2: Result := ConverteArea(Valor, CalcType, CalcFrac);
    ctKGtoLB, ctLBtoKG: Result := ConvertePeso(Valor, CalcType, CalcFrac);
  end;
end;

function TGeral.FormataCNPJ_TT(Texto: AnsiString): AnsiString;
  procedure MensagemErroCPFJ(ContNum: Integer);
  begin
    if ContNum < 14 then
      MensagemDeErro('CPF inválido: ' + String(Texto), 'Geral.FormataCNPJ_TT()')
      //MessageBox('CPF inválido', 'Aviso', MB_OK+MB_ICONWARNING)
    else
      MensagemDeErro('CNPJ inválido: ' + String(Texto), 'Geral.FormataCNPJ_TT()');
      //MessageBox('CNPJ inválido', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  Txt: String;
  //Num,
  Res: String;
  Tam: Integer;
begin
  Txt := SoNumero_TT(String(Texto));
  //
  if (Txt = '') and (String(Texto) <> Txt) then
  begin
    Res := String(Texto);
    Exit;
  end;
  Res := String(Txt);
  if Txt = '' then Exit;
  Tam := Length(Txt);
  //Num := String(CalculaCNPJCPF(Txt);
  //
  //if Txt <> Num then Txt := '?';
  try
    case Tam of
    11:
    Res := Txt[1]+Txt[2]+Txt[3]+'.'+
              Txt[4]+Txt[5]+Txt[6]+'.'+
              Txt[7]+Txt[8]+Txt[9]+'-'+
              Txt[10]+Txt[11];
    14:
      Res := Txt[1]+Txt[2]+'.'+
                Txt[3]+Txt[4]+Txt[5]+'.'+
                Txt[6]+Txt[7]+Txt[8]+'/'+
                Txt[9]+Txt[10]+Txt[11]+Txt[12]+'-'+
                Txt[13]+Txt[14];
    15:
      Res := Txt[1]+Txt[2]+Txt[3]+'.'+
                  Txt[4]+Txt[5]+Txt[6]+'.'+
                  Txt[7]+Txt[8]+Txt[9]+'/'+
                  Txt[10]+Txt[11]+Txt[12]+Txt[13]+'-'+
                  Txt[14]+Txt[15];
      else
      begin
        MensagemErroCPFJ(Tam);
        Exit;
      end;
    end;
  except
    MensagemErroCPFJ(Tam);
  end;
  Result := AnsiString(Res);
end;

function TGeral.FormataCNPJ_TT_NFe(Texto: String): AnsiString;
  procedure MensagemErroCPFJ(ContNum: Integer);
  begin
    if ContNum < 14 then
      MensagemDeErro('CPF inválido: ' + Texto, 'Geral.FormataCNPJ_TT_NFe()')
      //MessageBox('CPF inválido', 'Aviso', MB_OK+MB_ICONWARNING)
    else
      MensagemDeErro('CNPJ inválido: ' + Texto, 'Geral.FormataCNPJ_TT_NFe()');
      //MessageBox('CNPJ inválido', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  //Num,
  Res: String;
  Tam: Integer;
begin
  Texto := SoNumero_TT(Texto);
  Res := Texto;
  if Texto = '' then Exit;
  Tam := Length(Texto);
  //Num := CalculaCNPJCPF(Texto);
  //
  //if Texto <> Num then Texto := '?';
  try
    case Tam of
      11,14: Res := Texto;
      15:
      begin
        if Texto[1] = '0' then
          Res := Copy(Texto, 2)
        else
          MensagemErroCPFJ(Tam);
      end;
      else
      begin
        MensagemErroCPFJ(Tam);
        Exit;
      end;
    end;
  except
    MensagemErroCPFJ(Tam);
  end;
  Result := AnsiString(Res);
end;

function TGeral.FormataLC11603(Servico: String): String;
var
  Txt: String;
  Tam: Integer;
begin
  Txt := SoNumeroELetra_TT(Servico);
  Tam := Length(Txt);
  if Tam = 4 then
    Result := Txt[1] + Txt[2] + '.' + Txt[3] + Txt[4]
  else Result := Txt;
end;

function TGeral.SoNumero_TT(Texto : String) : String;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Texto) do
    // Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i];
{$ELSE}
    if Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
{$ENDIF}

end;

function TGeral.SplitIPv4(const IPv4: String; var p1, p2, p3,
  p4: String): Boolean;
  procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
  begin
     ListOfStrings.Clear;
     ListOfStrings.Delimiter       := Delimiter;
     ListOfStrings.StrictDelimiter := True;
     ListOfStrings.DelimitedText   := Str;
  end;
var
   OutPutList: TStringList;
   Host: String;
begin
  Result := False;
  p1 := '';
  p2 := '';
  p3 := '';
  p4 := '';
  Host := Trim(IPv4);
  if Host <> SoNumeroEPonto_TT(IPv4) then
    Exit;
  OutPutList := TStringList.Create;
  try
    Split('.', IPv4, OutPutList) ;
    if OutPutList.Count = 4 then
    begin
      p1 := OutPutList[0];
      p2 := OutPutList[1];
      p3 := OutPutList[2];
      p4 := OutPutList[3];
    end;
    //
    Result := (p1 <> '') and (p2 <> '') and (p3 <> '') and (p4 <> '');
  finally
    OutPutList.Free;
  end;
end;

function TGeral.STD_001(dh: string): TDateTime;
var
  Ano, Mes, Dia: Word;
begin
  if dh = '0000-00-00' then
    Result := 0
  else
  begin
    Ano := StrToInt(Copy(dh, 01, 04));
    Mes := StrToInt(Copy(dh, 06, 02));
    Dia := StrToInt(Copy(dh, 09, 02));
    //
    Result := EncodeDate(Ano, Mes, Dia);
  end;
end;

function TGeral.STD_109(dh: string): TDateTime;
var
  Ano, Mes, Dia: Word;
begin
  Ano := StrToInt(Copy(dh, 01, 04));
  Mes := StrToInt(Copy(dh, 06, 02));
  Dia := StrToInt(Copy(dh, 09, 02));
  //
  Result := EncodeDate(Ano, Mes, Dia) + StrToTime(Copy(Trim(dh), 12));
end;

function TGeral.STD_SPED(dh: string): TDateTime;
var
  Ano, Mes, Dia: Word;
begin
  if dh = '00000000' then
    Result := 0
  else
  begin
    Dia := StrToInt(Copy(dh, 01, 02));
    Mes := StrToInt(Copy(dh, 03, 02));
    Ano := StrToInt(Copy(dh, 05, 04));
    //
    Result := EncodeDate(Ano, Mes, Dia);
  end;
end;

function TGeral.StringToMemoryStream(const Str: String): TMemoryStream;
{
var
  SourceString: string;
  MemoryStream: TMemoryStream;
begin
  SourceString := 'Hello, how are you doing today?';
  MemoryStream := TMemoryStream.Create;
  try
    // Write the string to the stream. We want to write from SourceString's
    // data, starting at a pointer to SourceString (this returns a pointer to
    // the first character), dereferenced (this gives the actual character).
    MemoryStream.WriteBuffer(Pointer(SourceString)^, Length(SourceString));
    // Go back to the start of the stream
    MemoryStream.Position := 0;
    // Read it back in to make sure it worked.
    SourceString := 'I am doing just fine!';
    // Set the length, so we have space to read into
    SetLength(SourceString, MemoryStream.Size);
    MemoryStream.ReadBuffer(Pointer(SourceString)^, MemoryStream.Size);
    ShowMessage(SourceString);
  finally
    MemoryStream.Free;
  end;
}
var
  MemoryStream: TMemoryStream;
begin
  MemoryStream := TMemoryStream.Create;
  try
    MemoryStream.WriteBuffer(Pointer(Str)^, Length(Str));
    // Go back to the start of the stream
    //MemoryStream.Position := 0;
    Result := MemoryStream;
    Result.Position := 0;
  finally
    MemoryStream.Free;
  end;
end;

function TGeral.StrToTimeZZZ(Str: String): TTime;
var
  p: Integer;
  s1, s2: String;
  Mili: Double;
begin
  s1 := Geral.Substitui(Str, ',', '.');
  p := pos('.', s1);
  s2 := Copy(s1, 1, p - 1);
  Result := StrToTime(s2);
  s2 := Copy(s1, p + 1);
  Mili := Geral.IMV(s2) / 86400000;
  Result := Result + Mili;
end;

function TGeral.STT(Hora, hInc: String; PermiteMaisQ24h: Boolean): TTime;
  function HToTime(h: String): TTime;
  begin
    if h = '' then
      Result := 0
    else
      Result := StrToTime(h);
  end;
var
  Inic, Incr: TTime;
begin
  Inic := HToTime(Hora);
  Incr := HToTime(hInc);
  //
  Result := Inic + Incr;
  //
  if (Result > 1) and (PermiteMaisQ24h = False) then
    Result := 0;
end;

{
function TGeral.Substitui(Txt, ant, new: string): string;
var
  TamT, TamA, (*TamN,*) PosA: Integer;
begin
  if ant = new then
  begin
    Result := Txt;
    Exit;
  end;
  TamT := Length(Txt);
  TamA := Length(ant);
  PosA := Pos(ant ,Txt);
  while PosA > 0 do
  begin
    if PosA = 1 then
      Txt := new+copy(Txt,TamA+1,TamT-TamA)
    else if PosA +  TamA -1 = TamT then
      Txt := copy(Txt,1,PosA-1)+new
    else
      //Txt := copy(Txt,1,PosA-1)+new+copy(Txt,PosA+TamA,TamT-(PosA+TamA-1));
      Txt := copy(Txt,1,PosA-1)+new+copy(Txt,PosA+TamA);
    PosA := pos(ant,Txt);
  end;
  Result := Txt;
end;
}

function TGeral.Substitui(Txt, ant, new: string): string;
var
  TamT, TamA, (*TamN,*) PosA, PosN: Integer;
begin
  if ant = new then
  begin
    Result := Txt;
    Exit;
  end;
  TamT := Length(Txt);
  TamA := Length(ant);
  PosA := Pos(ant ,Txt);
  PosN := 0;
  while PosA > PosN do
  begin
    if PosA = 1 then
      Txt := new+copy(Txt,TamA+1,TamT-TamA)
    else if PosA +  TamA -1 = TamT then
      Txt := copy(Txt,1,PosA-1)+new
    else
      //Txt := copy(Txt,1,PosA-1)+new+copy(Txt,PosA+TamA,TamT-(PosA+TamA-1));
      Txt := copy(Txt,1,PosA-1)+new+copy(Txt,PosA+TamA);
    PosN := PosA;
    PosA := pos(ant,Txt);
  end;
  Result := Txt;
end;

function TGeral.FormataCNAE21(CNAE: String): String;
var
  Txt, L, N: String;
  Tam: Integer;
begin
  Txt := SoNumeroELetra_TT(CNAE);
  Tam := Length(Txt);
  if Tam > 2 then
  begin
    L := Txt[1];
    N := Copy(Txt, 2);
    //
    if N = SoNumero_TT(N) then
    begin
      case Tam of
        3: Result := L + N[1] + '.' + N[2];
        5: Result := L + N[1] + '.' + N[2] + N[3] + '-' + N[4];
        7: Result := L + N[1] + '.' + N[2] + N[3] + '-' + N[4] + '/' + N[5] + N[6];
        else Result := Txt;
      end;
    end else Result := Txt;
  end else Result := Txt;
end;

function TGeral.FormataCNPJ_TFT(Texto: AnsiString): AnsiString;
var
  i: Integer;
begin
  Result := '';
  if Texto = '' then Exit;
  try
    for i := 1 to length(Texto) do
      //if Texto[i] in ['0'..'9'] then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], ['0'..'9']) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in ['0'..'9'] then Result := Result + Texto[i];
{$ENDIF}
  except
    MensagemDeErro('CNPJ / CPF inválido.', 'Geral.FormataCNPJ_TFT()');
  end;
end;

// Adiciona texto em outro texto separado por vírgula
function TGeral.AddStrToStr_Separador(const NewStr, Separador: String; var
Destino: String): Boolean;
begin
  //Result := Destino;
  if Trim(NewStr) <> '' then
  begin
    if Trim(Destino) <> '' then
      Destino := Destino + Separador + NewStr
    else
      Destino := Destino + NewStr;
    Result := True;
  end else Result := False;
end;

procedure TGeral.AdequaMedida(const Grandeza: TGrandeza;
  const MedidaAtual: Double; const NivelAtual: Integer; var NovaMedida: Double;
  var NovoNivel: Integer; var NomeMedida: String);//: Boolean;
var
  Divisor: Integer;
begin
  case Grandeza of
    grandezaBytes: Divisor := 1024;
    grandezaNone:  Divisor := 1;
    else           Divisor := 1000;
  end;
  //
  NovaMedida := MedidaAtual;
  NovoNivel  := NivelAtual;
  //
  if Divisor > 1 then
  begin
    while NovaMedida > Divisor do
    begin
      NovaMedida := NovaMedida / Divisor;
      NovoNivel  := NovoNivel + 1;
    end;
  end else NovoNivel := 0;
  NomeMedida := '??';
  case Grandeza of
    grandezaBytes:
      case NovoNivel of
        0: NomeMedida := 'Bytes';
        1: NomeMedida := 'Kb'; // Kilobyte
        2: NomeMedida := 'Mb'; // Megabyte
        3: NomeMedida := 'Gb'; // Gigabyte
        4: NomeMedida := 'Tb'; // Terabyte
        5: NomeMedida := 'Pb'; // Petabyte
        6: NomeMedida := 'Eb'; // Exabyte
        7: NomeMedida := 'Zb'; // Zettabyte
        8: NomeMedida := 'Yb'; // Yottabyte
      end;
    grandezaNone: NomeMedida := 'x 1';
  end;
end;

function TGeral.AnoEMesDoMez(Mez: String): String;
var
  num, Mes, Ano: Integer;
begin
  if Mez = '' then Result := '' else
  if Mez = '0' then Result := '' else
  begin
    Num := IMV(Mez);
    Ano := num div 100;
    Mes := num mod 100;
    //
    Result := FormatFloat('00', Mes) + '/' + FormatFloat('0000', Ano);
  end;
end;

function TGeral.AnoEMesDoPeriodo(const Periodo: Integer; var Ano, Mes: Integer): Boolean;
begin
  Mes := Periodo mod 12;
  Ano := Periodo div 12;
  if Mes = 0 then
  begin
    Mes := 12;
    Ano := Ano -1;
  end;
  Ano := Ano + 2000;
end;

function TGeral.AnoEMesToMez(Ano, Mes: Integer): Integer;
var
  Data: TDateTime;
begin
  if Ano > 2000 then
    Result := Ano - 2000
  else
    Result := Ano;
  //
  Result := Result * 100 + Mes;
end;

function TGeral.AnoMesToAnoEMes(const AnoMes: Integer; var Ano,
  Mes: Integer): Boolean;
begin
  Ano := AnoMes div 100;
  Mes := AnoMes mod 100;
end;

function TGeral.AnoMesToData(AnoMes, Dia: Integer): TDateTime;
var
  Ano, Mes: Integer;
begin
  //Ano := AnoMes div 100;
  //Mes := AnoMes mod 100;
  AnoMesToAnoEMes(AnoMes, Ano, Mes);
  while Mes > 12 do
  begin
    Ano := Ano + 1;
    Mes := Mes - 12;
  end;
  Result := EncodeDate(Ano, Mes, Dia);
end;

function TGeral.AnoMesToPeriodo(AnoMes: Integer): Integer;
var
  Ano, Mes: Integer;
begin
  //Ano := AnoMes div 100;
  //Mes := AnoMes mod 100;
  AnoMesToAnoEMes(AnoMes, Ano, Mes);
  // ini 2023-07-26
  //Result := ((Ano - 2000) * 12) + Mes;
  Result := AnoEMesToPeriodo(Ano, Mes);
  // fim 2023-07-26
end;

function TGeral.AnoEMesToPeriodo(Ano, Mes: Integer): Integer;
begin
  Result := ((Ano - 2000) * 12) + Mes;
end;

procedure TGeral.ArrumaTelefoneBR_SemZero(const Source: String; var Dest: String);
var
  x: String;
begin
  x := Geral.SoNumero_TT(Source);
  if x <> '' then
  begin
    if Length(x) >= 8 then
    begin
      if x[1] <> '0' then
        x := '0' + x;
    end;
  end;
  Dest := x;
end;

function TGeral.ASA_Str(const Sorc1, Sorc2: array of string): TdmkArrStr;
var
  I, K: Integer;
begin
  K := Length(Sorc1);
  SetLength(Result, K + Length(Sorc2));
  for I := Low(Sorc1) to High(Sorc1) do
    Result[I] := Sorc1[I];
  for I := Low(Sorc2) to High(Sorc2) do
    Result[I + K] := Sorc2[I];
end;

function TGeral.ASA_Var(const Sorc1, Sorc2: array of Variant): TdmkArrVar;
var
  I, K: Integer;
begin
  K := Length(Sorc1);
  SetLength(Result, K + Length(Sorc2));
  for I := Low(Sorc1) to High(Sorc1) do
    Result[I] := Sorc1[I];
  for I := Low(Sorc2) to High(Sorc2) do
    Result[I + K] := Sorc2[I];
end;

function TGeral.Aspas(Texto: String): String;
begin
  Result := '"' + Texto + '"';
end;

// Array to String
function TGeral.ATA_Str(const Sorc: array of String): TdmkArrStr;
var
  I: Integer;
begin
  SetLength(Result, Length(Sorc));
  for I := Low(Sorc) to High(Sorc) do
    Result[I] := Sorc[I];
end;

function TGeral.ATA_Var(const Sorc: array of Variant): TdmkArrVar;
var
  I: Integer;
begin
  SetLength(Result, Length(Sorc));
  for I := Low(Sorc) to High(Sorc) do
    Result[I] := Sorc[I];
end;

function TGeral.ATS(ListaSQL: array of string; NotEmpty: Boolean): String;
var
  I: Integer;
begin
  Result := '';
  for I := Low(ListaSQL) to High(ListaSQL) do
  begin
    if NotEmpty then
    begin
      if ListaSQL[I] <> '' then
        Result := Result + ListaSQL[I] + sLineBreak;
    end else
      Result := Result + ListaSQL[I] + sLineBreak;
  end;
end;

// Array to String if ...
function TGeral.ATS_if(Condicao: Boolean; ListaSQL: array of string): String;
var
  I: Integer;
begin
  Result := '';
  if Condicao then
  begin
    for I := Low(ListaSQL) to High(ListaSQL) do
      Result := Result + ListaSQL[I] + sLineBreak;
  end;
end;

function TGeral.RoundC(Numero: Double; Casas: Byte): Double;
{
Arredondamento  - Arredondar
Tem por objetivo normatizar a representação de dados, de forma a uniformizar as regras de arredondamento.
a)	Quando o número a ser arredondado for menos que 5, conserva o último algarismo.
Ex.: 6,343 = 6,34			(Lp=0,01)
b)	Quando o número a ser arredondado for maior que 5, acrescenta-se um ao último algarismo.
Ex.: 6,3439 = 6,344                     (Lp=0,001)
c)	Quando o número a ser arredondado for igual a 5:
Se depois do 5 tiver número significativo (0) aumenta-se um ao último algarismo.
Ex.: 6,34509 = 6,35                     (Lp= 0,01)
        2,51= 3                         (Lp = 1)

Se depois do 5 não tiver número significativo, observa-se o algarismo anterior, se for par conserva e se for ímpar aumenta.
Ex.: 6,345 = 6,34                         (Lp=0,01)
        14,95 = 15,0                         (Lp=0,1)
}
var
  Fator: Extended;
  Num: Double;
  P, I: Integer;
  Txt, Meio: String;
begin
  Fator := IntPower(10, Casas);
{}
  Num := Numero * Fator;
  Txt := FloatToStr(Num);
  P := pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}decimalSeparator, Txt);
  Meio := Copy(Txt, P + 1);
  if Meio = '5' then
  begin
    I := IMV(Copy(Txt, P-1, 1));
    if I div 2 <> I / 2 then
      Num := Num + 1;
  end;
  Num := Round(Num) / Fator;
  Result := Num;
{}
//  Result := Round(Numero * Fator) / Fator;
end;

procedure TGeral.ComparaRoundC(ValAtual, Numero: Double; Casas: Integer);
var
  GVal: Double;
begin
  GVal := RoundC(Numero, Casas);
  //
  MB_Aviso('Comparação de arredondamento:' + sLineBreak +
  'Atual: ' + FloatToStr(ValAtual) + sLineBreak +
  'Novo: ' + FloatToStr(GVal));
end;

function TGeral.RoundG(Numero, Gap: Double): Double;
var
  Num: Double;
  P, I: Integer;
  Txt, Meio: String;
begin
  //if Numero = 11.555 then
    //ShowMessage('11,555');
  Num := Numero / Gap;
  Txt := FloatToStr(Num);
  P := pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}decimalSeparator, Txt);
  Meio := Copy(Txt, P + 1);
  if Meio = '5' then
  begin
    I := IMV(Copy(Txt, P-1, 1));
    if I div 2 <> I / 2 then
      Num := Num + 1;
  end;
  Result := Round(Num) * Gap;
end;

procedure TGeral.ComparaRoundG(ValAtual, Numero, Gap: Double);
var
  GVal: Double;
begin
  GVal := RoundG(Numero, Gap);
  //
  MB_Aviso('Comparação de arredondamento:' + sLineBreak +
  'Atual: ' + FloatToStr(ValAtual) + sLineBreak +
  'Novo: ' + FloatToStr(GVal));
end;

function TGeral.CalculaCNPJCPF(Nume : AnsiString) : AnsiString;
var
  l : Integer;
  Numero, Res: String;
  i,j,k : Integer;
  Soma : Integer;
  Digito : Integer;
  CNPJ : Boolean;
begin
  if Nume <> '' then
  begin
  Numero := '';
  for l := 1 to (Length(Nume)-2) do
    //if Nume[l] in ['0'..'9'] then
{$IFDEF DELPHI12_UP}
    if CharInSet(Nume[l], ['0'..'9']) then
{$ELSE}
    if Nume[l] in ['0'..'9'] then
{$ENDIF}
      Numero := Numero + Chr(Ord(Nume[l]));
  case length(Numero) of
     9 : CNPJ := False;
    12 : CNPJ := True;
    13 : CNPJ := True;
    else Exit;
  end;
  Res := Numero;
  //Calcula 2 vezes
  for j := 1 to 2 do begin
    k := 2;
    Soma := 0;
    for i := Length(Res) downto 1 do begin
      //Converte o dígito para numérico, multiplica e soma
      Soma := Soma + (Ord(Res[i]) - Ord('0')) * k;
      Inc(k);
      if (k > 9) and CNPJ then
        k := 2;
    end;
    Digito := 11 - Soma mod 11;
    if Digito >= 10 then
      Digito := 0;
    //adiciona o dígito ao Resado
      Res := Res + Chr(Digito + Ord('0'));
    end;
  end;
  Result := AnsiString(Res);
end;

function TGeral.FormataNCM(NCM: String): String;
var
  Txt, Capitulo, Posicao, SubPosicao, Item, SubItem: String;
begin
  Txt := SoNumero_TT(NCM);
  Capitulo   := Copy(Txt, 1, 2);
  Posicao    := Copy(Txt, 3, 2);
  SubPosicao := Copy(Txt, 5, 2);
  Item       := Copy(Txt, 7, 1);
  SubItem    := Copy(Txt, 8, 1);
  //
  Txt := Capitulo + Posicao;
  if Subposicao <> '' then
  begin
    Txt := Txt + '.' + SubPosicao;
    if Item <> '' then
      Txt := Txt + '.' + Item + SubItem;
  end;
  Result := Txt;
end;

function TGeral.Formata_IE(IE: String; UF: Variant; LinkMsk: String;
  Tipo: Integer = 0): String;
const
  Isento = 'ISENTO';
var
  Mascara: String;
  Estado: Integer;
begin
  // não é IE, é RG!
  if Tipo = 1 then
  begin
    Result := IE;
    Exit;
  end;  
  //
  if VarType(UF)  <> vtInteger then
    Estado := GetCodigoUF_da_SiglaUF(UF)
  else
    Estado := UF;
  if Trim(IE) <> '' then
  begin
    if Uppercase(IE) = Isento then
    begin
      Result := Isento;
    end else
    begin
      if Valida_IE(IE, Estado, LinkMsk) then
      begin
        case Estado of
          (*TO*)-27:
          begin
            if Length(IE) = 9 then
              Mascara := '00.000.000-0;0;*'
            else
              Mascara := '00.00.000000-0;0;*';
          end;
          (*SP*)-26:
          begin
            if Length(IE) > 0 then
            if Uppercase(IE[1]) = 'P' then
              Mascara := 'L-000000000.0/000;0;*'
            else Mascara := '000.000.000.000;0;*';
          end;
          (*SE*)-25: Mascara := '00000000-0;0;*';
          (*SC*)-24: Mascara := '000.000.000;0;*';
          (*RS*)-23: Mascara := '000/0000000;0;*';
          (*RR*)-22: Mascara := '00000000-0;0;*';
          (*RO*)-21: Mascara := '0000000000000-0;0;*';
          (*RN*)-20: Mascara := '00.000.000-0;0;*';
          (*RJ*)-19: Mascara := '00.000.00-0;0;*';
          (*PR*)-18: Mascara := '000.00000-00;0;*';
          (*PI*)-17: Mascara := '00000000-0;0;*';
          (*PE*)-16:
          begin
            if Length(IE) = 14 then
              Mascara := '00.0.000.0000000-0;0;*'
            else
              Mascara := '0000000-00;0;*';
          end;
          (*PB*)-15: Mascara := '00.000.000-0;0;*';
          (*PA*)-14: Mascara := '00000000-0;0;*';
          (*MT*)-13: Mascara := '0000000000-0;0;*';
          (*MS*)-12: Mascara := '00.000.000-0;0;*';
          (*MG*)-11: Mascara := '000.000.000/0000;0;*';
          (*MA*)-10: Mascara := '000000000;0;*';
          (*GO*)-09: Mascara := '00.000.000-0;0;*';
          (*ES*)-08: Mascara := '00000000-0;0;*';
          (*DF*)-07: Mascara := '000.00000.000-00;0;*';
          (*CE*)-06: Mascara := '00000000-0;0;*';
          (*BA*)-05: Mascara := '000000-00;0;*';
          (*AP*)-04: Mascara := '000000000;0;*';
          (*AM*)-03: Mascara := '00.000.000-0;0;*';
          (*AL*)-02: Mascara := '000000000;0;*';
          (*AC*)-01: Mascara := '00.00.0000-0;0;*';
          else Mascara := '';
        end;
        //Numero := StrToFloat(SoNumero_TT(IE));
        if Mascara = '' then Result := IE else
        Result := FormatMaskText(Mascara, SoNumero_TT(IE));
      end;
    end;// else Result := IE;
  end else Result := IE;
end;

function TGeral.Valida_IE(IE: String; UF: Variant; LinkMsk: String; ForcaTrue:
Boolean = True): Boolean;
var
  NEstado: String;
  vt: Integer;
begin
  Result := False;
  if (Trim(IE) = '') or (Uppercase(Trim(IE)) = 'ISENTO') or (UF = Null) then
  begin
    Result := True;
    Exit;
  end;
  vt := VarType(UF);
  if (vt = vtString) or (vt = varString) or (vt = varUString)
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
  or (vt = varString)
{$ENDIF}
  then
    UF := GetCodigoUF_da_SiglaUF(UF);
  case UF of
    -27: NEstado := 'Tocantins (TO)';
    -26: NEstado := 'São Paulo (SP)';
    -25: NEstado := 'Sergipe (SE)';
    -24: NEstado := 'Santa Catarina (SC)';
    -23: NEstado := 'Rio Grande do Sul (RS)';
    -22: NEstado := 'Roraima (RR)';
    -21: NEstado := 'Rondônia (RO)';
    -20: NEstado := 'Rio Grande do Norte (RN)';
    -19: NEstado := 'Rio de Janeiro (RJ)';
    -18: NEstado := 'Paraná (PR)';
    -17: NEstado := 'Piauí (PI)';
    -16: NEstado := 'Pernambuco (PE)';
    -15: NEstado := 'Paraíba (PB)';
    -14: NEstado := 'Pará (PA)';
    -13: NEstado := 'Mato Grosso (MT)';
    -12: NEstado := 'Mato Grosso do Sul (MS)';
    -11: NEstado := 'Minas Gerais (MG)';
    -10: NEstado := 'Maranhão (MA)';
    -09: NEstado := 'Goiás (GO)';
    -08: NEstado := 'Espírito Santo (ES)';
    -07: NEstado := 'Distrito Federal (DF)';
    -06: NEstado := 'Ceará (CE)';
    -05: NEstado := 'Bahia (BA)';
    -04: NEstado := 'Amapá (AP)';
    -03: NEstado := 'Amazonas (AM)';
    -02: NEstado := 'Alagoas (AL)';
    -01: NEstado := 'Acre (AC)';
  end;
  if UF = -27 then Result := Valida_IE_TO(IE, NEstado) else
  if UF = -26 then Result := Valida_IE_SP(IE, NEstado) else
  if UF = -25 then Result := Valida_IE_SE(IE, NEstado) else
  if UF = -24 then Result := Valida_IE_SC(IE, NEstado) else
  if UF = -23 then Result := Valida_IE_RS(IE, NEstado) else
  if UF = -22 then Result := Valida_IE_RR(IE, NEstado) else
  if UF = -21 then Result := Valida_IE_RO(IE, NEstado) else
  if UF = -20 then Result := Valida_IE_RN(IE, NEstado) else
  if UF = -19 then Result := Valida_IE_RJ(IE, NEstado) else
  if UF = -18 then Result := Valida_IE_PR(IE, NEstado) else
  if UF = -17 then Result := Valida_IE_PI(IE, NEstado) else
  if UF = -16 then Result := Valida_IE_PE(IE, NEstado) else
  if UF = -15 then Result := Valida_IE_PB(IE, NEstado) else
  if UF = -14 then Result := Valida_IE_PA(IE, NEstado) else
  if UF = -13 then Result := Valida_IE_MT(IE, NEstado) else
  if UF = -12 then Result := Valida_IE_MS(IE, NEstado) else
  if UF = -11 then Result := Valida_IE_MG(IE, NEstado) else
  if UF = -10 then Result := Valida_IE_MA(IE, NEstado) else
  if UF = -09 then Result := Valida_IE_GO(IE, NEstado) else
  if UF = -08 then Result := Valida_IE_11(IE, NEstado) else
  if UF = -07 then Result := Valida_IE_DF(IE, NEstado) else
  if UF = -06 then Result := Valida_IE_11(IE, NEstado) else
  if UF = -05 then Result := Valida_IE_BA(IE, NEstado) else
  if UF = -04 then Result := Valida_IE_AP(IE, NEstado) else
  if UF = -03 then Result := Valida_IE_11(IE, NEstado) else
  if UF = -02 then Result := Valida_IE_AL(IE, NEstado) else
  if UF = -01 then Result := Valida_IE_AC(IE, NEstado) else
  begin
    if UF <> 0 then
    begin
      MB_Aviso('O estado ' + NEstado + ' ainda não tem rotina '+
      'de verificação no aplicativo!');
      Result := False;
    end else
      MB_Aviso('Não foi definido o estado (UF) para verificação da I.E.' +
      sLineBreak + 'LinkMsk = "' + LinkMsk + '"' +
      sLineBreak + 'Geral.Valida_IE()');
    Exit;
  end;
  if Result = False then
  begin
    {MessageBox(0, PChar('Inscrição Estadual inválida para o estado '+
    NEstado+'!'), 'Erro', MB_OK+MB_ICONERROR);
    }
    MB_Aviso('Inscrição Estadual pode ser inválida para o estado ' +
      NEstado + '!');
    if ForcaTrue then
      Result := True;
    Exit;
  end;
end;

function TGeral.Valida_IE_SemErrLength(IE, UF: String; ln, lv: Integer):
Boolean;
var
  x: String;
begin
  x := SoNumero_TT(IE);
  if Length(IE) <> ln+lv then
  begin
    MB_Aviso('Possível inscrição estadual inválida!' + sLineBreak+
    'Estado (UF): ' + UF + sLineBreak + 'Inscrição estadual: ' + IE + sLineBreak +
    'Motivo: Quantidade de dígitos inválido para o estado '+ uf+'.' + sLineBreak +
    //'(MT): Se ó número da IE estiver correto acrescente zeros na frente para que o total de dígitos fique igual a ' + Geral.FF0(ln+lv) + sLineBreak +
    'Se o número da IE estiver correto acrescente zeros na frente para que o total de dígitos fique igual a ' + Geral.FF0(ln+lv) + sLineBreak +
    'Se o total de dígitos for maior que ' + Geral.FF0(ln+lv) + ' remova os zeros excedentes da frente do número!');
    Result := False;
  end else Result := True;
end;

function TGeral.Valida_IE_PR(IE, UF: String): Boolean;
(*
Formato da Inscrição: NNNNNNNN-DD
Cálculo do Primeiro Dígito: Módulo 11 com pesos de 2 a 7,
aplicados da direita para esquerda,  sobre as 8 primeiras posições.
Cálculo do Segundo Dígito: Módulo 11 com pesos de 2 a 7,
aplicados da direita para esquerda,  sobre as 9 primeiras posições
(inclui o primeiro dígito).
1º Passo: Multiplicar, da direita para esquerda, cada algarismo por pesos
de 2 a 7, respectivamente, somar os resultados, calcular o módulo 11 e
subtrair de 11. Se resto for 1 u 0, veja observação abaixo:
1 2 3 4 5 6 7 8 (CAD)
3 2 7 6 5 4 3 2 (pesos)
=> 8x2 + 7x3 + 6x4 + 5x5 + 4x6 + 3x7 + 2x2 + 1x3
=> 16+ 21 + 24 + 25 + 24 + 21 + 4 + 3 = 138
Cálculo de módulo 11 (resto da divisão por 11)
138 / 11 = 12 => resto 6 (módulo11)
11 - 6 = 5 (1º dígito verificador).
2º Passo: Acrescentar o 1º dígito calculado ao CAD.: 123.45678-5

3º Passo: Repetir 2º passo ao CAD do 3º passo.
1 2 3 4 5 6 7 8 5 (CAD)
4 3 2 7 6 5 4 3 2 (pesos)
=> 5x2 + 8x3 + 7x4 + 6x5 + 5x6 + 4x7 + 3x2 + 2x3 1x4
=> 10 + 24 + 28 + 30 + 30 + 28 + 6 + 6 + 4 = 166
Cálculo de módulo 11 (resto da divisão por 11)
166 / 11 = 15 => resto 1 (módulo 11)
11 - 1 =  10
Obs: Como cada dígito significa um algarismo, no caso do resto ser 1 ou 0, gerando "dígitos" 10 e 11, definimos o dígito como sendo 0.

4º Passo: Resultado final - CAD 123.45678-50
*)
const
  vs1 = '32765432'; vs2 = '432765432'; ln = 08; lv= 2;
var
  x, num, ver2, ver1: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
    ver1 := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver1) > 9 then ver1 := '0';
    Num := Num+ver1;
    Soma := 0;
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
    ver2 := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver2) > 9 then ver2 := '0';
    Num := Num + ver2;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_BA(IE, UF: String): Boolean;
(*
O número de inscrição estadual no Cadastro de Contribuintes do Estado da Bahia possui 9 algarismos sendo 7 dígitos inteiros e 2 dígitos verificadores. 

O cálculo dos dígitos verificadores é feito através de pesos (2 a 8 para o cálculo do 2º dígito e 2 a 9 para o 1º dígito). 
O cálculo dos dígitos verificadores se inicia com o 2º dígito que irá servir de peso para o cálculo do 1º dígito. 
O módulo de cálculo depende do valor do segundo dígito da inscrição. 



 
1. Para inscrições cujo segundo dígito é 0, 1, 2, 3, 4, 5, 8 cálculo pelo módulo 10 


Exemplo: I.E. número 1 2 3 4 5 6 7 - 4 8 

1.1. Cálculo do 2º dígito
Pesos de 2 a 8, aplicados em ordem decrescente, atribuindo-se um peso a cada dígito da I.E. conforme exemplo a seguir:
1 2 3 4 5 6 7 
8 7 6 5 4 3 2 pesos Cálculo:
(8 x 1 ) + ( 7 x 2 ) + ( 6 x 3 ) + ( 5 x 4 ) + ( 4 x 5 ) + ( 3 x 6 ) + ( 2 x 7 ) = 112   
O resto da divisão entre o resultado da operação acima 112 e o módulo 10 é igual a 2
Assim 10 (módulo) - 2 (resto da divisão) = 8 (2º dígito verificador da I.E. 1234567-48). 
Obs.: quando o resto for igual a 0 (zero) o digito é igual a 0 (zero). 


1.2. Cálculo do 1º dígito
Pesos de 2 a 9, aplicados em ordem decrescente, atribuindo-se um peso a cada dígito da I.E. já acrescida do 2º dígito de verificação obtido na operação anterior, conforme exemplo a seguir: 
1 2 3 4 5 6 7 8 
9 8 7 6 5 4 3 2 pesos Cálculo:
(9 X 1) + ( 8 x 2 ) + ( 7 x 3 ) + ( 6 x 4 ) + ( 5 x 5 ) + ( 4 x 6 ) + ( 3 x 7 ) + ( 2 x 8 ) = 156 

Obs.: quando o resto for igual a 0 (zero) o digito é igual a 0 (zero)   
O resto da divisão entre o resultado da operação acima 156 e o módulo 10 é igual a 6
Assim 10 (módulo) - 6 (resto da divisão) = 4 (1º dígito verificador da I.E. 1234567-48). 

 
2. Para inscrições cujo segundo dígito é 6, 7 ou 9 cálculo pelo módulo 11 


Exemplo: I.E. número 1 6 2 3 4 5 6 -5 1 

2.1. Cálculo do 2º dígito
Pesos de 2 a 8, aplicados da direita para esquerda, atribuindo um peso a cada dígito da I.E.:
1 6 2 3 4 5 6
8 7 6 5 4 3 2 pesos Cálculo:
(8 X 1) + ( 7 x 6 ) + ( 6 x 2 ) + ( 5 x 3 ) + ( 4 x 4 ) + ( 3 x 5) + ( 2 x 6 ) = 120   
O resto da divisão entre o resultado da operação acima 120 e o módulo 11 é igual a 1
Assim 11 (módulo) - 10 (resto da divisão) = 1 (2º dígito verificador da I.E. 1623456 -51). 
Obs.: quando o resto for igual a 0 (zero) ou 1 (um) o digito é igual a 0 (zero) 


2.2. Cálculo do 1º dígito
Pesos de 2 a 9 aplicados um a um a cada dígito da I.E. incluindo o 2º dígito obtido na operação anterior. 
1 6 2 3 4 5 6 1 
9 8 7 6 5 4 3 2 pesos Cálculo:
(9 x 1) + ( 8 x 6 ) + ( 7 x 2 ) + ( 6 x 3 ) + ( 5 x 4 ) + ( 4 x 5 ) + ( 3 x 6 ) + ( 2 x 1 ) = 149 

Obs.: quando o resto for igual a 0 (zero) ou 1 (um) o digito é igual a 0 (zero)   
O resto da divisão entre o resultado da operação acima 149 e o módulo 11 é igual a 6
Assim 11 (módulo) - 6 (resto da divisão) = 5 (1º dígito verificador da I.E. 1623456-51). 
 
*)
const
  vs1 = '8765432'; vs2 = '98765432'; ln = 07; lv= 2;
var
  x, num, ver2, ver1: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    //if not (x[2] in (['6', '7', '9'])) then
{$IFDEF DELPHI12_UP}
    if not (CharInSet(x[2], (['6', '7', '9']))) then
{$ELSE}
    if not (x[2] in (['6', '7', '9'])) then
{$ENDIF}
    begin
      // Módulo 10
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
      ver2 := IntToStr(10 -(Soma mod 10));
      if StrToInt(ver2) > 9 then ver2 := '0';
      Num := Num + ver2;
      Soma := 0;
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
      ver1 := IntToStr(10 -(Soma mod 10));
      if StrToInt(ver1) > 9 then ver1 := '0';
      Num := Copy(x, 1, ln)+ver1+ver2;
    end else begin
      // Módulo 11
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
      ver2 := IntToStr(11 -(Soma mod 11));
      if StrToInt(ver2) > 9 then ver2 := '0';
      Num := Num + ver2;
      Soma := 0;
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
      ver1 := IntToStr(11 -(Soma mod 11));
      if StrToInt(ver1) > 9 then ver1 := '0';
      Num := Copy(x, 1, ln)+ver1+ver2;
    end;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_11(IE, UF: String): Boolean;
(*
Formato: 8 dígitos + 1 dígito verificador
Exemplo: CGF número 06000001-5
Atribui-se um peso a cada dígito do CGF, excluindo o dígito verificador,
seguindo a ordem abaixo:
0 6 0 0 0 0 0 1
9 8 7 6 5 4 3 2 pesos
Cálculo:
(9 x 0) + (8 x 6) + (7 x 0) + (6 x 0) +
(5 x 0) + (4 x 0) + (3 x 0) + (2 x 1) = 50
O resto da divisão entre o resultado da operação acima 50 e o módulo 11 é igual a 6;
Calculamos 11, que é o módulo, menos 6, resultado obtido acima, dá igual a 5
que é o dígito verificador do CGF 06000001. Caso a diferença der 10 ou 11,
o dígito é 0
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_TO(IE, UF: String): Boolean;
(*
Governo do Estado de Tocantins - Roteiro de Crítica da Inscrição Estadual
Aplica-se o cálculo "módulo 11" com os algarismos 1,2,5,6,7,8,9,10 da Inscrição Estadual, criando o "11º" que é o dígito verificador. Os dígitos "3" e "4" não entram no cálculo, porém podem assumir somente os seguintes valores:

01 = Produtor Rural ( não possui CGC)
02 = Industria e Comércio
03 = Empresas Rudimentares
99 = Empresas do Cadastro Antigo (SUSPENSAS)

Exemplo: 29 01 022783 6
2 9 0 1 0 2 2 7 8 3 6
9 8 - - 7 6 5 4 3 2 -
 SOMA I = ( ( PRIMEIRO * 9) + (SEGUNDO * 8) + (TERCEIRO * 7) + (QUARTO * 6) +
 (QUINTO * 5) +  (SEXTO * 4) + (SÉTIMO * 3) + (OITAVO * 2) )


DIVISÃO MODAL DA "SOMA I" POR "ONZE"

ONDE:

170 = (2 * 9) + (9 * 8) + (0 * 7) + (2 * 6) + (2 * 5) + (7 * 4) + (8 * 3) + (3 * 2)
----->> 170 / 11 = 15
-----» resto = 5 (pois 15 x 11 = 165) 5 > 2
portanto
DÍGITO = 11 - 5 OU SEJA DÍGITO = 6

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  k, i, Soma: Integer;
begin
  //Result := False;
  Soma := 0;
  x := SoNumero_TT(IE);
  if Length(x) = 9 then
    x := Copy(x, 1, 2) + '99' + Copy(x, 3);
  if Valida_IE_SemErrLength(x, UF, ln, lv+2) then
  begin
    k := IMV(Copy(x, 3, 2));
    if not(k in ([1, 2, 3, 99])) then
    begin
      MB_Aviso('Tamanho da incrição estadual inválido!');
    end;
    num := x[1]+x[2]+Copy(x, 5, 6);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    //Num := Num + ver;
    if x[11] = ver then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_SE(IE, UF: String): Boolean;
(*
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_SC(IE, UF: String): Boolean;
(*
Exemplo: inscrição 251.040.852
Atribui-se um peso a cada dígito da inscrição exceto o ultimo digito a esquerda
2 5 1 0 4 0 8 5 (inscrição)
9 8 7 6 5 4 3 2 (pesos)
Cálculo:

(9 x 2) + (8 x 5) + (7 x 1) + (6 x 0) + (5 x 4) + (4 x 0) + (3 x 8) + (2 x 5) = 119

Divida o resultado por 11: 119 / 11 = 10 tendo o resto da divisão igual a 9
Subtraia o resto da divisão (9) de 11 para obter o digito verificador: 11 – 9 = 2
Obs: Quando o resto da divisão for zero (0) ou um (1), o digito será zero (0). 

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_RR(IE, UF: String): Boolean;
(*
*)
const
  vs = '12345678'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if x[1]+x[2] <> '24' then
    begin
      MensagemDeErro('Os dois primeiros dígitos devem ser "24" '+
      'para '+UF+'!', 'Valida_IE_RR()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // Módulo 09
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr((Soma mod 9));
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_RO(IE, UF: String): Boolean;
(*
Formato: 3 dígitos (município) + 5 dígitos (empresa) +1 dígito verificador
Exemplo: Inscrição Estadual número 101002135
• Despreza-se os três primeiros dígitos (101 nesse caso, indicando Porto Velho) e mais o dígito verificador, trabalhando-se somente com os cinco dígitos: 00213
• Atribui-se um peso a cada um dos cinco dígitos da empresa seguindo a ordem abaixo:
0 0 2 1 3 6 5 4 3 2 pesos
• Cálculo: (6 x 0) + (5 x 0) + (4 x 2) + (3 x 1) + (2 x 3) = 17
• O resto da divisão entre o resultado da operação acima 17 e o módulo 11 é igual a 6
• Calculamos 11, que é o módulo, menos 6, resultado obtido acima, dá igual a 5 que é o dígito verificador da Inscrição 10100213
• No caso da diferença calculada no item anterior fornecer como resultado 10 ou 11, subtrai-se 10 da mesma para obter o dígito verificador.
*)
const
  //vs = '65432'; ln = 8; lv= 1;
  vs = '6543298765432'; ln = 13; lv= 1;
var
  x, num, ver: String;
  i, Soma, a, b: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    //num := Copy(x, 4, ln-3);
    num := Copy(x, 1, ln);
    // Módulo 11
    //for i := Length(Num) downto 1 do
      //Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    for i := 1 to Length(Num) do
    begin
      a := StrToInt(Num[i]);
      b := StrToInt(vs[i]);
      Soma := Soma + (a * b);
    end;
    //ver := IntToStr(11 -(Soma mod 11));
    a := Soma mod 11;
    ver := IntToStr(11 - a);
    if StrToInt(ver) > 9 then ver := IntToStr(StrToInt(ver)-10);
    //Num := Copy(x, 1, 3) + Num + ver;
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_AP(IE, UF: String): Boolean;
(*
Formato: 2 dígitos constantes (03) + 6 dígitos (empresa) +1 dígito verificador
Exemplo: Inscrição Estadual número 030123459
• Verifica-se se os dois primeiros dígitos são de fato 03
• Despreza-se o dígito verificador, trabalhando-se somente com os demais 8 dígitos: 03012345
• Define-se dois valores, p e d, de acordo com as seguintes faixas de Inscrição Estadual:
De 03000001 a 03017000 => p = 5 e d = 0
De 03017001 a 03019022 => p = 9 e d = 1
De 03019023 em diante ===>p = 0 e d = 0

No exemplo acima, temos p = 5 e d = 0

Atribui-se um peso a cada um dos oito dígitos da empresa seguindo a ordem abaixo:
0 3 0 1 2 3 4 5
9 8 7 6 5 4 3 2 pesos
Cálculo:
p + (9 x 0) + (8 x 3) + (7 x 0) + (6 x 1) +
+ (5 x 2) + (4 x 3) + (3 x 4) + (2 x 5) = 79
Calculamos 11, que é o módulo, menos 2, resultado obtido acima, e obtemos 9 que é o dígito verificador da Inscrição, que será 030123459, e nesse exemplo estará correta;
No caso da diferença calculada no item anterior fornecer como resultado 10, o dígito verificador será 0, e se o resultado for 11, ele será d.

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  p, d, n, i, Soma: Integer;
begin
  Soma := 0;
  p := 0;
  d := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if x[1]+x[2] <> '03' then
    begin
      MensagemDeErro('Os dois primeiros dígitos devem ser "03" '+
      'para '+UF+'!', 'Valida_IE_AP()');
      Result := False;
      Exit;
    end;
    n := StrToInt(num);
    if ((n>=03000001) and (n<=03017000)) then
    begin
      p := 5;
      d := 0;
    end else
    if ((n>=03017001) and (n<=03019022)) then
    begin
      p := 9;
      d := 1;
    end else
    if ((n>=03019023) and (n<=03100000)) then
    begin
      p := 0;
      d := 0;
    end;
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    //////
    Soma := Soma + p;
    /////
    ver := IntToStr(11 -(Soma mod 11));
    n := StrToInt(ver);
    if n = 10 then ver := '0';
    if n = 11 then ver := IntToStr(d);
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_AC(IE, UF: String): Boolean;
(*
Tamanho da máscara: 9 caracteres
Máscara de utilização: 99.99.9999-9
Críticas da Inscrição Estadual: Somente números
Cálculo do dígito verificador:
Módulo 11 Peso (P) = 98765432
Ex.: Inscrição - 01.40.7423-0
0 1 4 0 7 4 2 3 P . . SOMA  =  99
DIVISOR  =  11
INTEIRO DO RESULTADO  =  9
RESTO  =  0

. . . . . . .  * 2 = 6
. . . . . . * .  3 = 6
. . . . . *  . . 4 = 16
. . . . *  . . . 5 = 35
. . . *  . . . . 6 = 0
. . *  . . . . . 7 = 28
. *  . . . . . . 8 = 8
*  . . . . . . . 9 = 0

Divide-se a soma por 11
Se resto < 2
Dígito = 0
Senão
Dígito = 11- resto
O terceiro e o quarto identificam o município e não podem ser = 0

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
  // >= 2000
  _Cgc2, DV1, DV2: String;
  _An, _Bn, _Cn, _Dn, _En, _Fn, _Gn, _Hn, _In, _Jn, _Kn,
  _S, _R, _N: Integer;

begin
  // válido até 31/12/1999
  if Length(SoNumero_TT(IE)) < 10 then
  begin
    Soma := 0;
    x := SoNumero_TT(IE);
    if Valida_IE_SemErrLength(x, UF, ln, lv) then
    begin
      if StrToInt(x[3]+x[4]) = 0 then
      begin
        MensagemDeErro('Os digitos 3 e 4 não podem ser 0 para '+
        UF+' pois ele indica a cidade!', 'Valida_IE_AC()');
        Result := False;
        Exit;
      end;
      num := Copy(x, 1, ln);
      // Módulo 11
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
      ver := IntToStr(11 -(Soma mod 11));
      if StrToInt(ver) > 9 then ver := '0';
      Num := Num + ver;
      if x = Num then Result := True else Result := False;
    end else Result := False;
  end else
  begin
    // Válido a partir de 01/01/2000
(*
// Rotina para validação do dígito da
inscrição estadual do Estado do Acre

// Separa cada número da Inscrição Estadual,
sem a mascara (9999999999999)

&An = substr(&Cgc2,1 ,1)
&Bn = substr(&Cgc2,2 ,1)
&Cn = substr(&Cgc2,3 ,1)
&Dn = substr(&Cgc2,4 ,1)
&En = substr(&Cgc2,5 ,1)
&Fn = substr(&Cgc2,6 ,1)
&Gn = substr(&Cgc2,7 ,1)
&Hn = substr(&Cgc2,8 ,1)
&In = substr(&Cgc2,9 ,1)
&Jn = substr(&Cgc2,10 ,1)
&Kn = substr(&Cgc2,11 ,1)
//
&S =      (&An*4) + (&Bn*3) + (&Cn*2) + (&Dn*9) + (&En*8) + (&Fn*7) + (&Gn*6) + (&Hn*5) + (&In*4) + (&Jn*3) + (&Kn*2)

//Pega o resto da divisão por 11

&R = int(&S/11)
&S = &S - (&R * 11)

//Se for 1 ou 0, o 1º dígito é 0; caso contrário o dígito é 11 menos o resto da divisão

if (&S = 1)  .OR. (&S = 0) then
   &N = 0
else
   &N = 11 - &S
endif

// Calcula o 2º dígito
   &S =      (&An*5) + (&Bn*4) + (&Cn*3) + (&Dn*2) + (&En*9) + (&Fn*8) + (&Gn*7) + (&Hn*6) + (&In*5) + (&Jn*4) + (&Kn*3) + (&N*2)
   &R = int(&S/11)
   &S = &S - (&R * 11)

   //
   if (&S = 1) .OR. (&S = 0)
      &N = 0
   else
      &N = 11 - &S
   endif

   //
*)
    // Rotina para validação do dígito da
    //inscrição estadual do Estado do Acre

    // Separa cada número da Inscrição Estadual,
    //sem a mascara (9999999999999)
    _Cgc2 := SoNumero_TT(IE);
    _An := IMV(Copy(_Cgc2,1 ,1));
    _Bn := IMV(Copy(_Cgc2,2 ,1));
    _Cn := IMV(Copy(_Cgc2,3 ,1));
    _Dn := IMV(Copy(_Cgc2,4 ,1));
    _En := IMV(Copy(_Cgc2,5 ,1));
    _Fn := IMV(Copy(_Cgc2,6 ,1));
    _Gn := IMV(Copy(_Cgc2,7 ,1));
    _Hn := IMV(Copy(_Cgc2,8 ,1));
    _In := IMV(Copy(_Cgc2,9 ,1));
    _Jn := IMV(Copy(_Cgc2,10 ,1));
    _Kn := IMV(Copy(_Cgc2,11 ,1));
    //
    _S :=      (_An*4) + (_Bn*3) + (_Cn*2) + (_Dn*9) + (_En*8) + (_Fn*7) + (_Gn*6) + (_Hn*5) + (_In*4) + (_Jn*3) + (_Kn*2);

    //Pega o resto da divisão por 11

    _R := _S div 11;
    _S := _S - (_R * 11);

    //Se for 1 ou 0, o 1º dígito é 0; caso contrário o dígito é 11 menos o resto da divisão

    if (_S = 1)  or (_S = 0) then
      _N := 0
    else
      _N := 11 - _S;
    DV1 := FF0(_N);
    // Calcula o 2º dígito
    _S :=      (_An*5) + (_Bn*4) + (_Cn*3) + (_Dn*2) + (_En*9) + (_Fn*8) + (_Gn*7) + (_Hn*6) + (_In*5) + (_Jn*4) + (_Kn*3) + (_N*2);
    _R := _S div 11;
    _S := _S - (_R * 11);

    //
    if (_S = 1) or (_S = 0) then
      _N := 0
    else
      _N := 11 - _S;
    DV2 := FF0(_N);

    //
    Result := Copy(_Cgc2, 12, 2) = DV1 + DV2;
  end;
end;

function TGeral.Valida_IE_MA(IE, UF: String): Boolean;
(*
Formato: numérico
Tamanho: 9 dígitos, sendo
os dois primeiros correspondentes ao código do Estado(12);
os seis seguintes ao número de ordem no cadastro
e o último dígito ao dígito de controle, calculado através do módulo 11.
Exemplo: calcular o dígito verificador X do número de inscrição estadual 12000038X

Passo 1: Multiplica um peso, já definido abaixo, pelo dígito equivalente do número de inscrição:

Nº de Inscrição fornecido  1  2 
 0  0  0  0  3  8  X  
Peso p/Multiplicar  9  8  7  6  5  4  3  2  DV  
 
Passo 2: Soma-se o resultado das multiplicações acima e divide-se por 11.
Soma = (9x 1) + (8x 2) + (7x 0) + (6x 0) + (5x 0) + (4x 0) + (3x 3) + (2x 8) = 50

Passo 3: Se o resto da divisão é 0 ou 1, o dígito é 0. Caso contrário, o dígito é 11 – resto da divisão.
50 dividido por 11 = 4
Resto = 6
Dígito Verificador = 11 – 6 = 5

Inscrição Estadual com dígito verificador: 120000385  
 
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if StrToInt(x[1]+x[2]) <> 12 then
    begin
      MensagemDeErro('Os 2 primeiro digitos devem ser 12, pois '+
      'else indicam o estado: '+UF+'!', 'Valida_IE_MA()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_PA(IE, UF: String): Boolean;
(*
Tamanho:9    Formato: Numérico
Composição: 15-999999-5
15: Número Padrão Pará
999999: Sequência
5: Dígito Verificador (Módulo 11)

Cálculo do dígito verificador Módulo 11
1- Multiplica-se os números, da direita para a esquerda, pelos seguintes valores: 2, 3, 4, 5, 6, 7, 8, 9;

2- Soma-se os resultados intermediários e divide-se por 11;
3- Se o resto da divisão é 0 ou 1, o dígito é 0;
4- Se o resto da divisão não é 0 nem 1, o dígito é = 11 - resto da divisão;
Exemplo: Cálculo para a Inscrição 15999999-5
                    9 x 1 =    9
                    8 x 5 =    40
                    7 x 9 =    63
                    6 x 9 =    54
                    5 x 9 =    45
                    4 x 9 =    36
                    3 x 9 =    27
                    2 x 9 =   18
                                  292

292 : 11 = 26 (Resto 6)
11 - 6 = 5 (Dígito Verificador 5)
 Fechar
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if StrToInt(x[1]+x[2]) <> 15 then
    begin
      MensagemDeErro('Os 2 primeiro digitos devem ser 15, pois '+
      'else indicam o estado: '+UF+'!', 'Valida_IE_PA()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_RJ(IE, UF: String): Boolean;
(*
99.999.99-3
27 654 32

Se o resto da divisão por 11 for menor ou igual à 1 (um),
Então o dígito verificador será = 0 (zero)
Senão, o dígito será 11 (onze) menos o resto

*)
const
  vs = '2765432'; ln = 07; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_RS(IE, UF: String): Boolean;
(*
Formato: 3 dígitos (município) + 6 dígitos (empresa) +1 dígito verificador
Exemplo: Inscrição Estadual número 224/3658792
• Verifica-se se os três primeiros dígitos (número do município) estão na faixa de 001 a 467
• Despreza-se o dígito verificador, trabalhando-se somente com os demais 9 dígitos: 224365879
• Atribui-se um peso a cada um dos nove dígitos seguindo a ordem abaixo:
2 2 4 3 6 5 8 7 9
2 9 8 7 6 5 4 3 2 pesos
• Cálculo:
(2 x 2) + (9 x 2) + (8 x 4) + (7 x 3) + (6 x 6) + + (5 x 5) + (4 x 8) + (3 x 7) + (2 x 9) = 207

• O resto da divisão entre o resultado da operação acima 207 e o módulo 11 é igual a 9
• Calculamos 11, que é o módulo, menos 9, resultado obtido acima, e obtemos 2 que é o dígito verificador da Inscrição, que será 2243658792, e nesse exemplo estará correta.
• No caso da diferença calculada no item anterior fornecer como resultado 10 ou 11, o dígito verificador será 0. 
*)
const
  vs = '298765432'; ln = 09; lv= 1; Cidades = 467;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if StrToInt(x[1]+x[2]+x[3]) > 467 then
    begin
      MensagemDeErro('Os 3 primeiros digitos da inscrição '+
      'estadual do RS devem ser menor que '+
      '467, pois eles indicam a cidade. Caso os dígitos existam, comunique a '+
      'Dermatek!', 'Valida_IE_RS()');
      Result := False;
      Exit;
    end;
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_PE(IE, UF: String): Boolean;
(*
ANTIGO!
O número de inscrição estadual no Cadastro de Contribuintes do Estado de Pernambuco - CACEPE possui 14 algarismos (dígitos), sendo 13 principais e 1 verificador. O cálculo do dígito verificador envolve o uso de "pesos" e da função "módulo" com parâmetro "11", conforme pode ser visto no exemplo abaixo:
Procedimentos de Cálculo (ex. para 18.1.001.0000004-9) 
1. Multiplique cada algarismo principal pelo seu respectivo peso: 
1 8 1 0 0 1 0 0 0 0 0 0 4 (algarismos) 
x x x x x x x x x x x x x . 
5 4 3 2 1 9 8 7 6 5 4 3 2 (pesos) 
= = = = = = = = = = = = = .
5 32 3 0 0 9 0 0 0 0 0 0 8 (produtos) 

2. Some os produtos obtidos para encontrar o total: 
5 + 32 + 3 + 0 + 0 + 9 + 0 + 0 + 0 + 0 + 0 + 0 + 8 = 57 (total dos produtos) 

3. Divida esse total pela constante "11" para obter o resto: 
4. Subtraia esse resto da constante "11" para encontrar o dígito verificador:11 - 2 = 9 
Quando essa diferença for maior que "9", subtraia "10" unidades para obter o valor do dígito verificador, uma vez que o mesmo deve ser sempre representado por apenas um algarismo. 
Rotina de Verificação (exemplo em linguagem Pascal)
O parâmetro "ie" deve ser do tipo String com 14 posições.
Function teste_dv_cacepe (ie: String): Boolean;
     Var a, b, c, d, e, f, g, h, i, j, k, l, m, n, total, dig: Integer;
     Begin
         a := StrToInt(Copy(ie,14,1));
         b := StrToInt(Copy(ie,13,1)) * 2;
         c := StrToInt(Copy(ie,12,1)) * 3;
         d := StrToInt(Copy(ie,11,1)) * 4;
         e := StrToInt(Copy(ie,10,1)) * 5;
         f := StrToInt(Copy(ie,9,1)) * 6;
         g := StrToInt(Copy(ie,8,1)) * 7;
         h := StrToInt(Copy(ie,7,1)) * 8;
         
 i := StrToInt(Copy(ie,6,1)) * 9;
         j := StrToInt(Copy(ie,5,1)) * 1;
         k := StrToInt(Copy(ie,4,1)) * 2;
         l := StrToInt(Copy(ie,3,1)) * 3;
         m := StrToInt(Copy(ie,2,1)) * 4;
         n := StrToInt(Copy(ie,1,1)) * 5;
         total := b + c + d + e + f + g + h + i + j + k + l + m + n;
         dig := 11 - (total Mod 11);
         If dig > 9 Then
              dig := dig - 10;
         If dig <> a Then
            teste_dv_cacepe := False
         Else
            teste_dv_cacepe := True;
    End;  
////////////////////////////////////////////////////////////////////////////////
NOVO!
Dígito Verificador da Nova Inscrição Estadual Pernambuco

Procedimentos de Cálculo (ex. para 0321418-40)

1. Multiplique cada algarismo principal pelo seu respectivo peso:
0   3   2   1   4   1   8   (algarismos)
x   x   x   x   x   x   x
8   7   6   5   4   3   2   (pesos)
=   =   =   =   =   =   =
0   21  12  5   16  3   16  (produtos)                  

2. Some os produtos obtidos para encontrar o total:
0 + 21 + 12 + 5 + 16 + 3 + 16 = 73 (total dos produtos)

3. Divida esse total pela constante "11" para obter o resto:
73/11 = 6, Resto = 7.

4. Se o resto da divisão for igual a 1 ou 0, o primeiro dígito será igual a zero.
   Senão, Subtraia esse resto da constante "11" para encontrar o dígito verificador:
Como o resto é igual a 7, então: 11 – 7 = 4 (primeiro dígito de controle)

5. Multiplique cada algarismo principal pelo seu respectivo peso para obter o segundo
   dígito de controle (o cálculo leva em consideração o primeiro dígito, já calculado)
0   3   2   1   4   1   8   4   (algarismos)
x   x   x   x   x   x   x   x
9   8   7   6   5   4   3   2   (pesos)
=   =   =   =   =   =   =   =
0   24  14  6   20  4   24  8   (produtos)

6. Some os produtos obtidos para encontrar o total:
0 + 24 + 14 + 6 + 20 + 4 + 24 + 8 = 100 (total dos produtos)

7. Divida esse total pela constante "11" para obter o resto:
100/11 = 9, Resto = 1.

8. Se o resto da divisão for igual a 1 ou 0, o dígito será igual à zero.
   Senão, Subtraia esse resto da constante "11" para encontrar o dígito verificador:
Resto igual a 1. Portanto, segundo dígito é igual à zero.
*)
const
  vs = '5432198765432';
  ln1 = 13; lv1 = 1; //Inscrição com 14 digitos
  ln2 = 7; lv2 = 2;  //Inscrição com 9 digitos
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Result := False;
  Soma := 0;
  x := SoNumero_TT(IE);
  case Length(x) of
    14:
    begin
      if Valida_IE_SemErrLength(x, UF, ln1, lv1) then
      begin
        (*if StrToInt(x[1]+x[2]) <> 18 then
        begin
          MB_(('Os 2 primeiro digitos devem ser 18, pois '+
          'else indicam o estado: '+UF+'!'), 'Erro', MB_OK+MB_ICONERROR);
          Result := False;
          Exit;
        end;*)
        num := Copy(x, 1, ln1);
        // Módulo 11 -10
        for i := Length(Num) downto 1 do
          Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
        ver := IntToStr(11 -(Soma mod 11));
        if StrToInt(ver) > 9 then ver := IntToStr(StrToInt(ver)-10);
        Num := Num + ver;
        if x = Num then Result := True else Result := False;
      end else Result := False;
    end;
    else
    begin
      if Valida_IE_SemErrLength(x, UF, ln2, lv2) then
      begin
        num := Copy(x, 1, ln2);
        for i := Length(num) downto 1 do
          Soma := Soma + (StrToInt(Num[i]) * StrToInt(Copy(vs, 7)[i]));
        ver := IntToStr(Soma mod 11);
        if StrToInt(ver) in [0,1] then
          ver := FormatFloat('0', 0)
        else
          ver := IntToStr(11 - StrToInt(ver));
        num  := num + ver;
        Soma := 0;
        for i := Length(num) downto 1 do
          Soma := Soma + (StrToInt(Num[i]) * StrToInt(Copy(vs, 6)[i]));
        ver := IntToStr(Soma mod 11);
        if StrToInt(ver) in [0,1] then
          ver := FormatFloat('0', 0)
        else
          ver := IntToStr(11 - StrToInt(ver));
        num := num + ver;
        if num = SoNumero_TT(IE) then
          Result := True;
      end else Result := False;
    end;
  end;
end;

function TGeral.Valida_IE_PI(IE, UF: String): Boolean;
begin
  Result := Valida_IE_11(IE, UF);
end;

function TGeral.Valida_IE_PB(IE, UF: String): Boolean;
(*
Formato: 8 dígitos + 1 dígito verificador 
Exemplo: CCICMS número 16.004.017-5 
• Atribui-se um peso a cada dígito do CCICMS, excluindo o dígito verificador,
seguindo a ordem abaixo:
1 6 0 0 4 0 1 7
9 8 7 6 5 4 3 2 pesos
• Cálculo:
(9 x 1) + (8 x 6) + (7 x 0) + (6 x 0) +
(5 x 4) + (4 x 0) + (3 x 1) + (2 x 7) = 94
• O resto da divisão entre o resultado da operação acima 94 e o módulo 11é igual a 6
• Calculamos 11, que é o módulo, menos 6, resultado obtido acima, dá igual a 5
que é o dígito verificador do CCICMS 16004017. Caso a diferença der 10 ou 11,
o dígito é 0.
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    (*if StrToInt(x[1]+x[2]) <> 16 then
    begin
      MB_(('Os 2 primeiro digitos devem ser 15, pois '+
      'else indicam o estado: '+UF+'!'), 'Erro', MB_OK+MB_ICONERROR);
      Result := False;
      Exit;
    end;*)
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_AL(IE, UF: String): Boolean;
(*
Governo do Estado de Alagoas - Roteiro de Crítica da Inscrição Estadual
FORMAÇÃO: 24XNNNNND,  sendo:
24 – Código do Estado
X – Tipo de empresa (0-Normal, 3-Produtor Rural, 5-Substituta,
7- Micro-Empresa Ambulante, 8-Micro-Empresa)
NNNNN – Número da empresa
D – Dígito de verificação calculado pelo Módulo11, pêsos 2 à 9 da direita
para a esquerda, exceto D
Exemplo: Número 2 4 0 0 0 0 0 4 D
Pesos 9 8 7 6 5 4 3 2
SOMA = (2 * 4) + (3 * 0) + (4 * 0) + (5 * 0) + (6 * 0) + (7 * 0) + (8 * 4) + (9 * 2) = 58
PRODUTO = 58 * 10 = 580
RESTO = 580 – INTEIRO(580 / 11)*11 = 580 – (52*11) = 8
DÍGITO = 8
OBS: Caso RESTO seja igual a 10 o DÍGITO será 0 (zero)
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  n, i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if StrToInt(x[1]+x[2]) <> 24 then
    begin
      MensagemDeErro('Os 2 primeiro digitos devem ser 24, pois '+
      'else indicam o estado: '+UF+'!', 'Valida_IE_AL()');
      Result := False;
      Exit;
    end;
    n := StrToInt(x[3]);
    if not (n in [0,3,5,7,8]) then
    begin
      MensagemDeErro('O terceiro dígito devem ser um dos '+
      'abaixo demonstrados:'+sLineBreak+
      '0-Normal'+sLineBreak+
      '3-Produtor Rural'+sLineBreak+
      '5-Substituta'+sLineBreak+
      '7- Micro-Empresa Ambulante'+sLineBreak+
      '8-Micro-Empresa'+sLineBreak+''+sLineBreak+
      'para o estado '+UF, 'Valida_IE_AL()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // Módulo 11 pela metade
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    Soma := Soma * 10;
    Soma := Soma mod 11;
    if Soma > 9 then Soma := 0;
    ver := IntToStr(Soma);
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_RN(IE, UF: String): Boolean;
(*
Governo do Estado do Rio Grande do Norte - Roteiro de Crítica da Inscrição Estadual
INSCRIÇÃO  :  20.040.040-1
(Os primeiros dois dígitos são sempre 20)
MEMORIA DE CALCULO:
2 * 9  =  18
0 * 8  =   0
0 * 7  =   0
4 * 6  =  24
0 * 5  =   0
0 * 4  =   0
4 * 3  =  12
0 * 2  =   0
        ----
SOMA  ->  54
MULTIPLICA A SOMA POR 10
MULTIPLICAÇÃO = SOMA * 10 = 54 * 10 = 540
DIVIDE POR 11, OBTENDO O RESTO DA DIVISAO
DIVISÃO = MULTIPLICAÇÃO / 11    =  540 / 11
RESTO (DIVISÃO) = 1
DIGITO VERIFICADOR = RESTO (DIVISÃO)
OBSERVACAO: CASO O DIGITO CALCULADO SEJA 10, CONSIDERAR DIGITO = 0
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    //n := StrToInt(x[3]);
    num := Copy(x, 1, ln);
    // Módulo 11 pela metade
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    Soma := Soma * 10;
    Soma := Soma mod 11;
    if Soma > 9 then Soma := 0;
    ver := IntToStr(Soma);
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_MS(IE, UF: String): Boolean;
(*
O método adotado para se verificar a compatibilidade entre o número principal
da inscrição estadual, composto dos primeiros oito dígitos,
e o seu dígito verificador, representado pelo nono dígito, segue as regras:
o primeiro dígito será sempre representado pelo número 2;
o segundo dígito será sempre representado pelo número 8
O método a que se refere o artigo anterior é assim representado:
A = (1º x 9)+(2º x 8)+(3º x 7)+(4º x 6)+(5º x 5)+(6º x 4)+(7º x 3)+(8º x 2)
R = Resto de A/11
Se R = 0, então D = 0
Se R > 0
T = 11 – R
Se T > 9, então D = 0
Se T < 10, então D = T
Em que D = Dígito Verificador.

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, Num, ver: String;
  n, i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    Num := Copy(x, 1, ln);
    if StrToInt(Num[1]+Num[2]) <> 28 then
    begin
      MensagemDeErro('Os dois primeiros digitos devem ser 28 '+
      'para '+UF+' pois ele indica o estado!', 'Valida_IE_RN()');
      Result := False;
      Exit;
    end;
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(Soma mod 11);
    if ver <> '0' then
    begin
      n := 11-StrToInt(ver);
      if n > 9 then n := 0;
      ver := IntToStr(n);
    end;
    //if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_MT(IE, UF: String): Boolean;
(*
Formato: 10 dígitos + 1 dígito verificador
Exemplo: Inscrição Estadual número 0013000001-9

Atribui-se um peso a cada dígito da Inscrição Estadual seguindo a ordem abaixo:
0 0 1 3 0 0 0 0 0 1 – 9
3 2 9 8 7 6 5 4 3 2 pesos
Cálculo:
(3 x 0) + (2 x 0) + (9 x 1) + (8 x 3) + (7 x 0) + (6 x 0) +
(5 x 0) + (4 x 0) + (3 x 0) + (2 x 1) = 35
O resto da divisão entre o resultado da operação acima 35 e o módulo 11 é igual 2
Pegamos 11, que é o módulo, menos 2, resultado obtido acima, dá igual a 9
que é o dígito verificador da Inscrição Estadual 0013000001-9.
Obs.: Se o resto da divisão for (0) zero ou (1) um, então Dígito Verificador é DV=0

Atenção:
O Cálculo deve sempre considerar todos os dígitos, inclusive zeros a esquerda,
pois todos são significativos.
Caso a inscrição informada não possua 11 dígitos, a mesma deverá ser completada
com zeros a esquerda para que o cálculo seja efetuado corretamente,
como está demonstrado no exemplo acima.
*)
const
  vs = '3298765432'; ln = 10; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else
  begin
    MensagemDeErro('Caso a inscrição informada não possua '+
    '11 dígitos, a mesma deverá ser completada com zeros a esquerda',
    'Valida_IE_MT()');
    Result := False;
  end;
end;

function TGeral.Valida_IE_GO(IE, UF: String): Boolean;
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  n, i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if not (StrToInt(num[1]+num[2]) in ([10, 11, 15])) then
    begin
      MensagemDeErro('Os primeiros 2 dígitos devem ser "10" ou '+
      '"11" ou "15" para o estado '+ UF+'.', 'Valida_IE_GO()');
      Result := False;
      Exit;
    end;
    if num = '11094402' then
    begin
      ver := Copy(x, 9, 1);
      if StrToInt(ver) in ([0, 1]) then Result := True else Result := False;
      Exit;
    end;
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(Soma mod 11);
    if ver <> '0' then
    begin
      if ver = '1' then
      begin
        n := StrToInt(num);
        if (n>=10103105) and (n<=10119997) then ver := '1' else ver := '0';
      end else ver := IntToStr(11 - StrToInt(ver));
    end;
    //if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_DF(IE, UF: String): Boolean;
const
  vs1 = '43298765432'; vs2 = '543298765432'; ln = 11; lv= 2;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // Módulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    ////////////////////////////////////////////////////////////////////////////
    Soma := 0;
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    ////////////////////////////////////////////////////////////////////////////
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TGeral.Valida_IE_MG(IE, UF: String): Boolean;
const
  vs1 = '121212121212'; vs2 = ''; ln = 11; lv= 2;
var
  x, num, ver, Txt: String;
  i, n, Soma: Integer;
begin
  Soma := 0;
  x := SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    //num := Copy(x, 1, ln);
    Num := Copy(x, 1, 3)+'0'+Copy(x, 4, 8);
    for i := Length(Num) downto 1 do
      Txt := Txt + IntToStr((StrToInt(Num[i]) * StrToInt(vs1[i])));
    for i := Length(Txt) downto 1 do
      Soma := Soma + StrToInt(Txt[i]);
    n := 0;
    if (Soma / 10) = (Soma div 10) then ver := '0' else
    begin
      n := (Soma div 10) * 10 + 10;
      n := n - Soma;
    end;
    num := Copy(x, 1, ln) + IntToStr(n);
    n := 1;
    Soma := 0;
    for i := Length(Num) downto 1 do
    begin
      n := n + 1;
      if n = 12 then n := 2;
      Soma := Soma  + (StrToInt(Num[i]) * n);
    end;
    n := Soma mod 11;
    if n < 2 then ver := '0' else ver := IntToStr(11-n);
    Num := Num + Ver;
    ////////////////////////////////////////////////////////////////////////////
    if x = Num then Result := True else Result := False;
  end else Result := False;
(*
Governo do Estado de Minas Gerais - Roteiro de Crítica da Inscrição Estadual

FORMATO GERAL: A1A2A3B1B2B3B4B5B6C1C2D1D2
Onde:

A= Código do Município
B= Número da inscrição
C= Número de ordem do estabelecimento
D= Dígitos de controle

CALCULO DE D1 (primeiro dígito)Para se calcular primeiro dígito procede-se
da seguinte forma:

1. Igualar as casas para o cálculo, o que consiste em inserir o algarismo
zero "0" imediatamente após o número de código do município, desprezando-se
os dígitos de controle.

Exemplo: Número da inscrição: 062 307 904 00 ? ?
Número a ser trabalhado: 062 "0" 307904 00 -- --

2. Multiplica-se os algarismos do número a ser trabalhado, por 1 e por 2,
sucessivamente, da esquerda para a direita.

3. Exemplo:
0  6  2  0  3  0  7  9  0  4  0  0
x  x  x  x  x  x  x  x  x  x  x  x
1  2  1  2  1  2  1  2  1  2  1  2
========================
12  2  0  3  0  7 18  0  8  0  0

4. Soma-se os algarismos (não os produtos) do resultado obtido
Exemplo: 0+1+2+2+0+3+0+7+1+8+0+8+0+0=32

5. Subtrai-se o resultado da soma do ítem anteior, da primeira dezena
exata imediatamente superior:
Exemplo: "40" - 32= 8

6. O resultado encontrado corresponderá ao 1º dígito de controle:
Exemplo: 062307904 00 "8" ?

CÁLCULO DE D2 (segundo dígito)
Para se calcular o 2º dígito, procede-se da seguinte forma:

1. Utiliza-se o número de inscrição acrescido do primeiro dígito encontrado (não há que se igualar as casas com a inserção do "0" zero)

2. Cada algarismo, da direita para a esquerda, é multiplicado, sucessivamente, por 2,3,4,5,6,7,8,9,10,11, quando se reinicia a série 2,3,

Exemplo:
0  6  2  3  0  7  9  0  4  0  0  8
x  x  x  x  x  x  x  x  x  x  x  x
3  2 11 10  9  8  7  6  5  4  3 2
========================
0 12 22 30  0 56 63  0 20  0  0 16

3. Soma-se então os produtos (não os algarismos)
Exemplo: 0+12+22+30+0+56+63+0+20+0+0+16 = 219

4. Divide-se então o resultado por 11:
Exemplo:219 11
109 19
10

5. O resto encontrado na divisão será subtraído de onze, encontrando-se, com o resultado desta última operação, o segundo dígito da inscrição
11 – "10" = 1
Por conseguinte – D2 = 1

Observe que sendo o resto da divisão "1" ou "0" (zero), o dígito igualmente será "0" (zero) sendo desnecessária a subtração acima demonstrada.

Pelos exemplos, o nº da Inscrição trabalhada no presente demonstrativo é:
062.307.904/0081
*)

end;

function TGeral.Valida_IE_SP(IE, UF: String): Boolean;
var
  x, num, ver1, ver2: String;
  n, Soma: Integer;
begin
  Result := False;
  x := SoNumeroELetra_TT(IE);
  if (Uppercase(x[1]) = 'P')  then
  begin
    if Valida_IE_SemErrLength(Copy(x, 2, Length(x)-1), UF, 9, 3) then
    begin
      Num := Copy(x, 2, 8);
      Soma :=
             StrToInt(Num[1])*1 +
             StrToInt(Num[2])*3 +
             StrToInt(Num[3])*4 +
             StrToInt(Num[4])*5 +
             StrToInt(Num[5])*6 +
             StrToInt(Num[6])*7 +
             StrToInt(Num[7])*8 +
             StrToInt(Num[8])*10;
      n := Soma mod 11;
      if n > 9 then n := n - 10;
      if (IntToStr(n) = x[10]) then Result := True else Result := False;
    end;
  end else begin
    x := SoNumero_TT(IE);
    if Valida_IE_SemErrLength(x, UF, 10, 2) then
    begin
      Num := Copy(x, 1, 8);
      Soma :=
           StrToInt(Num[1])*1 +
           StrToInt(Num[2])*3 +
           StrToInt(Num[3])*4 +
           StrToInt(Num[4])*5 +
           StrToInt(Num[5])*6 +
           StrToInt(Num[6])*7 +
           StrToInt(Num[7])*8 +
           StrToInt(Num[8])*10;
      n := Soma mod 11;
      if n > 9 then n := n - 10;
      ver1 := IntToStr(n);
      //////////////////////////////////////////////////////////////////////////
      Num := Copy(x, 1, 11);
      Soma :=
             StrToInt(Num[01])*03 +
             StrToInt(Num[02])*02 +
             StrToInt(Num[03])*10 +
             StrToInt(Num[04])*09 +
             StrToInt(Num[05])*08 +
             StrToInt(Num[06])*07 +
             StrToInt(Num[07])*06 +
             StrToInt(Num[08])*05 +
             StrToInt(Num[09])*04 +
             StrToInt(Num[10])*03 +
             StrToInt(Num[11])*02;
      n := Soma mod 11;
      if n > 9 then n := n - 10;
      ver2 := IntToStr(n);
      if ((ver1 = x[9]) and (ver2 = x[12])) then Result := True else Result := False;
    end else Result := False;
  end;
(*Governo do Estado de São Paulo- Roteiro de Crítica da Inscrição Estadual
Regra Geral: Em todas as contagens de posições de caracteres e cálculos efetuados deverão ser ignorados caracteres diferentes da letra "P" e dos algarismos 0, 1, 2, 3, 4, 5, 6, ,7, 8 e 9.
I – Industriais e comerciantes (exceto produtores rurais a eles não equiparados):
> Formato: 12 dígitos sendo que o 9º e o 12º são dígitos verificadores
> Exemplo: Inscrição Estadual 110.042.490.114
a) Cálculo do 1º dígito verificador, ou seja, do 9º dígito contado a partir da esquerda:
- Atribui-se um peso a cada dígito da Inscrição Estadual seguindo a ordem utilizada abaixo:
1 1 0 0 4 2 4 9 0 1 1 4      PESOS: 1 3 4 5 6 7 8 10
(1 x 1) + (1 x 3) + (0 x 4) + (0 x 5) + (4 x 6) + (2 x 7) + (4 x 8) + (9 x 10) = 1 + 3 + 0 + 0 + 24 + 14 + 32 + 90 = 164
- O dígito verificador será o algarismo mais à direita do resto da divisão do resultado obtido acima (164) por 11 à 164/11 = 14 com resto = 10, então o 1º dígito verificador (9ª posição) é 0

b) Cálculo do 2º dígito verificador, ou seja, do 12º dígito contado a partir da esquerda:
- Atribui-se um peso a cada dígito da Inscrição Estadual seguindo a ordem utilizada abaixo:
1  1  0  0  4  2  4  9   0  1  1  4      PESOS: 3  2 10 9  8  7  6  5  4  3   2
(1 x 3) + (1 x 2) + (0 x 10) + (0 x 9) + (4 x 8) + (2 x 7) + (4 x 6) + (9 x 5) + (0 x 4) + (1 x 3) + (1 x 2) = 3 + 2 + 0 + 0 + 32 + 14 + 24 + 45 + 0 + 3 + 2 = 125
- O dígito verificador será o algarismo mais à direita do resto da divisão do resultado obtido acima (118) por 11 à 125/11 = 11 com resto = 4, então o 2º dígito verificador (12ª posição) é 4
Fica então formada a inscrição 110.042.490.114
II – Inscrição estadual de Produtor Rural (Não equiparado a industrial ou comerciante, cujas inscrições obedecem a Regra descrita no item anterior):
> Formato: - P0MMMSSSSD000
- 13 caracteres dos quais o 10º caracter contado a partir da esquerda ("D") é o dígito verificador
- Inicia sempre com "P" e apresenta a sequência 0MMMSSSSD000, onde:
0MMMSSSS-algarismos que serão utilizados no cálculo do dígito verificador "D"
"D" - Dígito verificador que consiste os 8 (oito)dígitos imediatamente anteriores
000 - 3 (três) dígitos que compõem o nº de inscrição mas não utilizados no cálculo do dígito verificador
> Exemplo: Inscrição Estadual P-01100424.3/002

Cálculo do dígito verificador, ou seja, do 10º dígito contado a partir da esquerda, incluindo-se na contagem a letra "P":
- Atribui-se um peso a cada dígito da Inscrição Estadual a partir do primeiro 0 (zero) seguindo a ordem utilizada abaixo:
0 1 1 0 0 4 2 4 3      PESOS: 1 3 4 5 6 7 8 10
(0x 1) + (1 x 3) + (1 x 4) + (0 x 5) + (0 x 6) + (4 x 7) + (2 x 8) + (4 x 10) = 0 + 3 + 4 + 0 + 0 + 28 + 16 + 40 = 91
- O dígito verificador será o algarismo mais à direita do resto da divisão do resultado obtido acima (91) por 11
> 91/11 = 8 com resto = 3, então o dígito verificador (10ª posição) é 3
Fica então formada a inscrição P-011000424.3/002*)

end;

function TGeral.SoNumero1a9_TT(Texto: String): String;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Texto) do
    //if Texto[i] in (['1'..'9']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], (['1'..'9'])) then Result := Result + Texto[i];
{$ELSE}
    if Texto[i] in (['1'..'9']) then Result := Result + Texto[i];
{$ENDIF}
end;

function TGeral.SoNumeroEData_TT(Texto: String): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Texto) do
  begin
    if CharInSet(Texto[I], (['0'..'9'])) then Result := Result + Texto[i];
    if CharInSet(Texto[I], (['/', '-', ':'])) then Result := Result + Texto[i];
  end;
  if Length(Result) = 18 then //01/01/202023:59:59
    Result := Copy(Result, 1, 10) + ' ' + Copy(Result, 11)
  else
  if Length(Result) = 15 then // 01/01/202023:59
    Result := Copy(Result, 1, 10) + ' ' + Copy(Result, 11)
  else
  if Length(Result) = 16 then // 01/01/2023:59:59
    Result := Copy(Result, 1, 8) + ' ' + Copy(Result, 9)
end;

function TGeral.SoNumeroELetra_TT(Texto : String) : String;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Texto) do
  begin
    {
    if Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
    if Texto[i] in (['A'..'Z']) then Result := Result + Texto[i];
    if Texto[i] in (['a'..'z']) then Result := Result + Texto[i];
    }
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i];
    if CharInSet(Texto[i], (['A'..'Z'])) then Result := Result + Texto[i];
    if CharInSet(Texto[i], (['a'..'z'])) then Result := Result + Texto[i];
{$ELSE}
    if Texto[i] in (['0'..'9']) then Result := Result + Texto[i];
    if Texto[i] in (['A'..'Z']) then Result := Result + Texto[i];
    if Texto[i] in (['a'..'z']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

function TGeral.SoNumeroEPonto_TT(Texto: String): String;
var
  i:Integer;
begin
  Result := '';
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
      //if Texto[i] in (['0'..'9', '.']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], (['0'..'9', '.'])) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in (['0'..'9', '.']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

function TGeral.SoNumeroEVirgula_TT(Texto : String) : String;
var
  i:Integer;
begin
  Result := '';
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
      //if Texto[i] in (['0'..'9', ',']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], (['0'..'9', ','])) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in (['0'..'9', ',']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

function TGeral.SoNumeroESinalEVirgula_TT(Texto : String) : String;
var
  i:Integer;
begin
  Result := '';
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
      //if Texto[i] in (['0'..'9', ',', '-']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], (['0'..'9', ',', '-'])) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in (['0'..'9', ',', '-']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

function TGeral.SoDecimalCharacter(const Texto: String): Boolean;
var
  I, N, T: Integer;
begin
  Result := False;
  T := 0;
  N := 0;
  for I := 1 to Length(Texto) do
  begin
    T := T + 1;
    if CharInSet(Texto[i], (['A'..'Z'])) then Exit;
    if CharInSet(Texto[i], (['a'..'z'])) then Exit;
    if CharInSet(Texto[i], (['0'..'9', '&','#',';',#9,#10,#13])) then N := N + 1;
  end;
  Result := N = T;
end;

function TGeral.SoHora_TT(Texto: String; MudaPtPara2Pt: TTipoPt2Pt): String;
var
  i:Integer;
begin
  Result := '';
  if Length(Texto) > 0 then
  begin
    for i := 1 to Length(Texto) do
    begin
      if Texto[i] = '.' then
      begin
        case MudaPtPara2Pt of
          pt2ptNo: ; //Nada,
          pt2pt60: Result := Result + ':';
          //pt2pt99: Result := Result + ':';
          else MB_Aviso(
          '"MudaPtPara2Pt" não definid em "Geral.SoHora_TT()"');
        end;
      end else
      //if Texto[i] in (['0'..'9', ':']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
      if CharInSet(Texto[i], (['0'..'9', ':'])) then Result := Result + Texto[i];
{$ELSE}
      if Texto[i] in (['0'..'9', ':']) then Result := Result + Texto[i];
{$ENDIF}
    end;
  end;
end;

function TGeral.SoLetra_TT(Texto : String) : String;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Texto) do
  begin
    //if Texto[i] in (['A'..'Z']) then Result := Result + Texto[i];
    //if Texto[i] in (['a'..'z']) then Result := Result + Texto[i];
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[i], (['A'..'Z'])) then Result := Result + Texto[i];
    if CharInSet(Texto[i], (['a'..'z'])) then Result := Result + Texto[i];
{$ELSE}
    if Texto[i] in (['A'..'Z']) then Result := Result + Texto[i];
    if Texto[i] in (['a'..'z']) then Result := Result + Texto[i];
{$ENDIF}
  end;
end;

procedure TGeral.AbrirAppAuxiliar(IP: String; DMKID_APP: Integer; ParamStr1: String = '';
  ParamStr2: String = '');

  function GetWindowsDrive: Char;
  var
    S: string;
  begin
    SetLength(S, MAX_PATH);
    if GetWindowsDirectory(PChar(S), MAX_PATH) > 0 then
      Result := string(S)[1]
    else
      Result := #0;
  end;

var
  App, Caminho, Drive, Params, ParamStr3: String;
  Achou: Boolean;
begin
  case DMKID_APP of
    11: //DermaAD
      App := 'DermaAD';
    12: //DermaAD2
      App := 'DermaAD2';
    13: //DermaBK
      App := 'DermaBK';
    14: //DermaDB
      App := 'DermaDB';
    15: //DermaDB50
      App := 'DermaDB50';
    39: //DermaApps
      App := 'DermaApps';
    else
      App := '';
  end;
  if App = '' then
  begin
    Geral.MB_Aviso('Aplicativo não implementado!');
    Exit;
  end;
  //
  ParamStr3 := IP; // <-Delphi Alexandria   // antes: VAR_IP;
  Drive     := GetWindowsDrive;
  Achou     := False;
  //
  Caminho := Drive + ':\Executaveis\'+ App +'.exe';
  //
  if FileExists(String(Caminho)) then
    Achou := True;
  //
  if not Achou then
  begin
    Caminho := Drive + ':\Executaveis\Auxiliares\'+ App +'.exe';
    //
    if FileExists(String(Caminho)) then
      Achou := True;
  end;
  if not Achou then
  begin
    Caminho := ExtractFilePath(Application.ExeName) + App + '.exe';
    //
    if FileExists(String(Caminho)) then
      Achou := True;
  end;
  Params := '';
  if ParamStr1 <> '' then
    Params := Params + ' ' + ParamStr1
  else
    Params := Params + ' ' + '#';
  if ParamStr2 <> '' then
    Params := Params + ' ' + ParamStr2
  else
    Params := Params + ' ' + '#';
  if ParamStr3 <> '' then
    Params := Params + ' ' + ParamStr3
  else
    Params := Params + ' ' + '#';
  //
  if Achou then
    WinExec(PAnsiChar(AnsiString(Caminho + Params)), SW_SHOW)
  else
    MB_Aviso('Falha ao abrir aplicativo ' + App + '!' +
      sLineBreak + 'Tente abrí-lo manualmente!');
end;

function TGeral.GetCodigoUF_da_SiglaUF(SiglaUF: String): Integer;
var
  Codigo: Integer;
begin
  if SoNumeroESinal_TT(SiglaUF) = SiglaUF then
    Codigo := IMV(SiglaUF)
  else
    Codigo := 0;
  case Codigo of
    -99,-27..-1: Result := Codigo;
    0:
    begin
      if SiglaUF = 'EX' then Result := -99 else
      if SiglaUF = 'AN' then Result := -91 else
      if SiglaUF = 'TO' then Result := -27 else
      if SiglaUF = 'SP' then Result := -26 else
      if SiglaUF = 'SE' then Result := -25 else
      if SiglaUF = 'SC' then Result := -24 else
      if SiglaUF = 'RS' then Result := -23 else
      if SiglaUF = 'RR' then Result := -22 else
      if SiglaUF = 'RO' then Result := -21 else
      if SiglaUF = 'RN' then Result := -20 else
      if SiglaUF = 'RJ' then Result := -19 else
      if SiglaUF = 'PR' then Result := -18 else
      if SiglaUF = 'PI' then Result := -17 else
      if SiglaUF = 'PE' then Result := -16 else
      if SiglaUF = 'PB' then Result := -15 else
      if SiglaUF = 'PA' then Result := -14 else
      if SiglaUF = 'MT' then Result := -13 else
      if SiglaUF = 'MS' then Result := -12 else
      if SiglaUF = 'MG' then Result := -11 else
      if SiglaUF = 'MA' then Result := -10 else
      if SiglaUF = 'GO' then Result := -09 else
      if SiglaUF = 'ES' then Result := -08 else
      if SiglaUF = 'DF' then Result := -07 else
      if SiglaUF = 'CE' then Result := -06 else
      if SiglaUF = 'BA' then Result := -05 else
      if SiglaUF = 'AP' then Result := -04 else
      if SiglaUF = 'AM' then Result := -03 else
      if SiglaUF = 'AL' then Result := -02 else
      if SiglaUF = 'AC' then Result := -01 else
      Result := 0;
    end;
    1..27,99: Result := - Codigo;
    else Result := 0;
  end;
end;

function TGeral.GetSiglaDaUFdoEstado(Estado: String): String;
var
  x: String;
begin
  x := Lowercase(SemAcento(Estado));
  if x ='tocantins'           then Result := 'TO' else
  if x ='sao paulo'           then Result := 'SP' else
  if x ='sergipe'             then Result := 'SE' else
  if x ='santa catarina'      then Result := 'SC' else
  if x ='rio grande do sul'   then Result := 'RS' else
  if x ='roraima'             then Result := 'RR' else
  if x ='rondonia'            then Result := 'RO' else
  if x ='rio grande do norte' then Result := 'RN' else
  if x ='rio de janeiro'      then Result := 'RJ' else
  if x ='parana'              then Result := 'PR' else
  if x ='piaui'               then Result := 'PI' else
  if x ='pernambuco'          then Result := 'PE' else
  if x ='paraiba'             then Result := 'PB' else
  if x ='para'                then Result := 'PA' else
  if x ='mato grosso'         then Result := 'MT' else
  if x ='mato grosso do sul'  then Result := 'MS' else
  if x ='minas gerais'        then Result := 'MG' else
  if x ='maranhao'            then Result := 'MA' else
  if x ='goias'               then Result := 'GO' else
  if x ='espirito santo'      then Result := 'ES' else
  if x ='distrito federal'    then Result := 'DF' else
  if x ='ceara'               then Result := 'CE' else
  if x ='bahia'               then Result := 'BA' else
  if x ='amapa'               then Result := 'AP' else
  if x ='amazonas'            then Result := 'AM' else
  if x ='alagoas'             then Result := 'AL' else
  if x ='acre'                then Result := 'AC' else
                    Result := '??????'
end;

function TGeral.GetSiglaUF_do_CodigoUF(CodigoUF: Integer): String;
begin
  case CodigoUF of
   -100: Result := 'SVAN';
   -143: Result := 'SVRS';
    -91: Result := 'AN';
    -99: Result := 'EX';
    -27: Result := 'TO';
    -26: Result := 'SP';
    -25: Result := 'SE';
    -24: Result := 'SC';
    -23: Result := 'RS';
    -22: Result := 'RR';
    -21: Result := 'RO';
    -20: Result := 'RN';
    -19: Result := 'RJ';
    -18: Result := 'PR';
    -17: Result := 'PI';
    -16: Result := 'PE';
    -15: Result := 'PB';
    -14: Result := 'PA';
    -13: Result := 'MT';
    -12: Result := 'MS';
    -11: Result := 'MG';
    -10: Result := 'MA';
    -09: Result := 'GO';
    -08: Result := 'ES';
    -07: Result := 'DF';
    -06: Result := 'CE';
    -05: Result := 'BA';
    -04: Result := 'AP';
    -03: Result := 'AM';
    -02: Result := 'AL';
    -01: Result := 'AC';

     00: Result := '';

     01: Result := 'AC';
     02: Result := 'AL';
     03: Result := 'AM';
     04: Result := 'AP';
     05: Result := 'BA';
     06: Result := 'CE';
     07: Result := 'DF';
     08: Result := 'ES';
     09: Result := 'GO';
     10: Result := 'MA';
     11: Result := 'MG';
     12: Result := 'MS';
     13: Result := 'MT';
     14: Result := 'PA';
     15: Result := 'PB';
     16: Result := 'PE';
     17: Result := 'PI';
     18: Result := 'PR';
     19: Result := 'RJ';
     20: Result := 'RN';
     21: Result := 'RO';
     22: Result := 'RR';
     23: Result := 'RS';
     24: Result := 'SC';
     25: Result := 'SE';
     26: Result := 'SP';
     27: Result := 'TO';
     91: Result := 'AN';
     99: Result := 'EX';
    100: Result := 'SVAN';
    143: Result := 'SVRS';
    else Result := IntToStr(CodigoUF);
  end;
end;

function TGeral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF: Integer): String;
begin
  case CodigoUF of
     11: Result := 'RO';
     12: Result := 'AC';
     13: Result := 'AM';
     14: Result := 'RR';
     15: Result := 'PA';
     16: Result := 'AP';
     17: Result := 'TO';
     21: Result := 'MA';
     22: Result := 'PI';
     23: Result := 'CE';
     24: Result := 'RN';
     25: Result := 'PB';
     26: Result := 'PE';
     27: Result := 'AL';
     28: Result := 'SE';
     29: Result := 'BA';
     31: Result := 'MG';
     32: Result := 'ES';
     33: Result := 'RJ';
     35: Result := 'SP';
     41: Result := 'PR';
     42: Result := 'SC';
     43: Result := 'RS';
     50: Result := 'MS';
     51: Result := 'MT';
     52: Result := 'GO';
     53: Result := 'DF';
     91: Result := 'AN';
     99: Result := 'EX';
    100: Result := 'SVAN';
    143: Result := 'SVRS';
    else Result := IntToStr(CodigoUF);
  end;
end;

{$WARNINGS OFF}
function TGeral.GetVolumeSerialNumber(EhOServidor: Boolean; CaminhoServ: String): Int64;
  function GetDiskSerialNumber(DriveLatter:String) : DWord;
  var
  //  a,b : Integer;
    buffer : array[0..255] of char;
    B1 : PChar;
    a,b,SerialNumber : DWord;
  begin
    //New(B1);
    B1 := PChar(DriveLatter + ':\');
    //B1 := StrPCopy(B1, DriveLatter + ':\');
    GetVolumeInformation(B1, Buffer, SizeOf(buffer),@SerialNumber,a,b,nil,0);
    Result := SerialNumber;
    //Dispose(B1);
  end;
var
  FindFileName, Pasta: String;
  SR: TSearchRec;
  HFile: Integer;
  FileInfo: BY_HANDLE_FILE_INFORMATION;
  //DoLoop,
  Searched, AbriuArq: Boolean;
  I: Integer;
begin
  if EhOServidor then // se for o servidor
  begin
    Result := GetDiskSerialNumber('C');
    Exit;
  end;
  AbriuArq := True;
  try
    Pasta := CaminhoServ;
    VerificaDir(Pasta, '\', 'Diretório Dermatek do Servidor', False);
    FindFileName := Pasta + '*.*';
    if (FindFirst(FindFileName, FaAnyfile, SR) <> 0) then
    begin
      Searched := False;
      Result := 0;
      MB_Aviso('Não foi possível localizar a pasta: "' + Pasta + '"');
      Exit;
    end else begin
      I := FindFirst(Pasta + '*.*', faAnyFile, SR);
      //j := 0;
      while I = 0 do
      begin
        if (SR.Attr and faDirectory) <> faDirectory then
        begin
          HFile := Createfile(Pchar(Pasta + SR.Name), 0, 0, nil, OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL, 0);
          AbriuArq := hfile <> -1;
          if GetFileInformationByHandle(hFile, FileInfo) then
          begin
            Result := FileInfo.dwVolumeSerialNumber;
          end else Result := 0;
          I := -1;
        end
        else
          I := FindNext(SR);
      end;
    end;
  finally
    if Searched then
    begin
      FindClose(SR);
      if AbriuArq then
        CloseHandle(HFile);
    end;
  end;
end;

function TGeral.GET_Compo_Val(const Form: TForm; const Objeto: TObject; const Nome,
  Propriedade: String; const Classe: TClass; var Valor: Variant): Boolean;
{
procedure TGeral.ProcessProperty(Sheet: Variant; Row, Col: integer);
var
  st, PropName, PropValue: string;
  i1, i2, dotpos: integer;
  cmp: TComponent;
  c : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  IsNumeric: boolean;
begin
  st := String(Sheet.Cells[Row, Col]);
  i1 := Pos('[',st);
  i2 := Pos(']',st);
  PropName := Copy(st,i1+1,i2-i1-1);
  dotpos := Pos('.', PropName);
  if dotpos <> 0 then
  begin
    cmp := Owner.FindComponent(Copy(PropName,1,dotpos-1));
    PropName := Copy(PropName,dotpos+1,length(PropName));
  end
  else
    cmp:=Owner;
  if cmp <> nil then begin

  c := TRttiContext.Create;
  t:=c.GetType(cmp.ClassInfo);
  p:=t.GetProperty(PropName);
  if p <> nil then begin
  IsNumeric:=false;
  if p.PropertyType.ToString = 'TStrings' then PropValue:=p.GetValue(cmp).AsType<TStrings>.Text
  else if p.PropertyType.ToString = 'Boolean' then PropValue:=BoolToStr(p.GetValue(cmp).AsBoolean,true)
  else
  case p.PropertyType.TypeKind of
  tkInteger,
  tkInt64 : begin
  PropValue := IntToStr(p.GetValue(cmp).AsInteger);
  IsNumeric:=true;
  end;
  tkString,
  tkUString,
  tkLString : PropValue := p.GetValue(cmp).AsString;
  tkFloat : begin
  PropValue := FloatToStr(p.GetValue(cmp).AsExtended);
  IsNumeric:=true;
  end;
  else PropValue := '';
  end;
  if IsNumeric and (Trim(Copy(st,1,i1-1))='') and (Trim(Copy(st,i2+1,length(st)))='') then
  Sheet.Cells[Row, Col]:=StrToFloat(PropValue)
  else Sheet.Cells[Row, Col]:=Copy(st,1,i1-1)+PropValue+Copy(st,i2+1,length(st));
  end;
  c.Free;
  end;
end;
}

var
  MyObj: TObject;
  PropInfo: PPropInfo;
  //
  //st, PropValue,
  PropName: string;
  c : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  IsNumeric: Boolean;
begin
  Result := False;
  if Objeto <> nil then
    MyObj := Objeto
  else
  begin
    if (Form <> nil) and (
    (Form.FindComponent(Nome) as Classe) <> nil) then
      MyObj := Form.FindComponent(Nome) as Classe
    else begin
      Geral.MB_Erro('O componente ' + Nome +
      ' não existe ou não pode ser setado!' + sLineBreak + 'AVISE A DERMATEK!');
      Exit;
    end;
  end;
  if MyObj <> nil then
  begin
    PropInfo := GetPropInfo(MyObj.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        Valor := GetPropValue(MyObj, Propriedade, True);
        //
        PropName := Propriedade;
        c := TRttiContext.Create;
        t := c.GetType(MyObj.ClassInfo);
        p := t.GetProperty(PropName);
        if p <> nil then
        begin
{
          IsNumeric := False;
}
          if p.PropertyType.ToString = 'TStrings' then
            Valor := p.GetValue(MyObj).AsType<TStrings>.Text
{
          else
          if p.PropertyType.ToString = 'Boolean' then
            Valor := BoolToStr(p.GetValue(MyObj).AsBoolean, True)
          else
          case p.PropertyType.TypeKind of
            tkInteger,
            tkInt64 :
            begin
              Valor := IntToStr(p.GetValue(MyObj).AsInteger);
              IsNumeric := True;
            end;
            tkString,
            tkUString,
            tkLString : Valor := p.GetValue(MyObj).AsString;
            tkFloat :
            begin
              Valor := FloatToStr(p.GetValue(MyObj).AsExtended);
              IsNumeric:=true;
            end;
            else
              Valor := GetPropValue(MyObj, Propriedade, True);
          end;
          if IsNumeric then
          begin
    (*
            and (Trim(Copy(st,1,i1-1))='')
            and (Trim(Copy(st,i2+1,length(st)))='') then
              Sheet.Cells[Row, Col]:=StrToFloat(PropValue)
            else
              Sheet.Cells[Row, Col]:=Copy(st,1,i1-1)+PropValue+Copy(st,i2+1,length(st));
    *)
          end};
          c.Free;
        end;
        //
        Result := True;
      except
        Geral.MB_Erro('A propriedade ' + Propriedade +
        ' não pode ser setada no componente ' + TComponent(MyObj).Name + '.' +
        sLineBreak + 'AVISE A DERMATEK!');
      end;
    end else
      Geral.MB_Erro('O componente ' + TComponent(MyObj).Name +
      ' não possui a propriedade ' + Propriedade + '.' + sLineBreak +
      'AVISE A DERMATEK!');
  end;
end;

// Não testado!!!!
function TGeral.GiveEveryoneFullAccessToRegistryKey(const RootKey: HKey;
  const RegPath: String; out ErrorMsg: string): Boolean;
var
  Access : LongWord;
  WinResult : LongWord;
  SD : PSecurity_Descriptor;
  LastError : DWORD;
  Reg : TRegistry;
begin
  Result := TRUE;
  ErrorMsg := '';
  if (Win32Platform = VER_PLATFORM_WIN32_NT) then
  begin
////////////////////////////////////////////////////////////////////////////////
    {
    if TOOLS.UserHasAdminToken then
      Access := KEY_ALL_ACCESS
    else
      Access := KEY_READ OR KEY_WRITE;
    }
    Access := KEY_ALL_ACCESS;
////////////////////////////////////////////////////////////////////////////////
    Reg := TRegistry.Create(Access);
    try
      Reg.RootKey := RootKey;
      if NOT Reg.OpenKey(RegPath, TRUE) then
        Exit;
      GetMem(SD, SECURITY_DESCRIPTOR_MIN_LENGTH);
      try
        Result := InitializeSecurityDescriptor(SD, SECURITY_DESCRIPTOR_REVISION);
        if Result then
          Result := SetSecurityDescriptorDacl(SD, TRUE, NIL, FALSE); // Fails here!
        if NOT Result then
          begin
            LastError := WINDOWS.GetLastError;
            if NOT Result then
              ErrorMsg := SYSUTILS.SysErrorMessage(LastError);
          end
        else
          begin
            WinResult := RegSetKeySecurity(Reg.CurrentKey, DACL_SECURITY_INFORMATION, SD);
            Result := (WinResult = ERROR_SUCCESS);
            ErrorMsg := SYSUTILS.SysErrorMessage(WinResult);
          end;
      finally
        FreeMem(SD);
      end;
      Reg.CloseKey;
    finally
      FreeAndNIL(Reg);
    end;
  end;
end; {GiveEveryoneFullAccessToRegistryKey}

{$WARNINGS ON}

function TGeral.GetCodigoUF_IBGE_DTB_da_SiglaUF(SiglaUF: String): Integer;
begin
  if SiglaUF = 'RO' then Result := 11 else
  if SiglaUF = 'AC' then Result := 12 else
  if SiglaUF = 'AM' then Result := 13 else
  if SiglaUF = 'RR' then Result := 14 else
  if SiglaUF = 'PA' then Result := 15 else
  if SiglaUF = 'AP' then Result := 16 else
  if SiglaUF = 'TO' then Result := 17 else
  if SiglaUF = 'MA' then Result := 21 else
  if SiglaUF = 'PI' then Result := 22 else
  if SiglaUF = 'CE' then Result := 23 else
  if SiglaUF = 'RN' then Result := 24 else
  if SiglaUF = 'PB' then Result := 25 else
  if SiglaUF = 'PE' then Result := 26 else
  if SiglaUF = 'AL' then Result := 27 else
  if SiglaUF = 'SE' then Result := 28 else
  if SiglaUF = 'BA' then Result := 29 else
  if SiglaUF = 'MG' then Result := 31 else
  if SiglaUF = 'ES' then Result := 32 else
  if SiglaUF = 'RJ' then Result := 33 else
  if SiglaUF = 'SP' then Result := 35 else
  if SiglaUF = 'PR' then Result := 41 else
  if SiglaUF = 'SC' then Result := 42 else
  if SiglaUF = 'RS' then Result := 43 else
  if SiglaUF = 'MS' then Result := 50 else
  if SiglaUF = 'MT' then Result := 51 else
  if SiglaUF = 'GO' then Result := 52 else
  if SiglaUF = 'DF' then Result := 53 else
  if SiglaUF = 'AN' then Result := 91 else
  if SiglaUF = 'EX' then Result := 99 else
  if SiglaUF = 'SVAN' then Result := 100 else
  if SiglaUF = 'SVRS' then Result := 143 else
  Result := 0;
end;

function TGeral.GetEstadoDaSiglaDaUF(UF: String): String;
begin
  if UF = 'TO' then Result := 'Tocantins'           else
  if UF = 'SP' then Result := 'São Paulo'           else
  if UF = 'SE' then Result := 'Sergipe'             else
  if UF = 'SC' then Result := 'Santa Catarina'      else
  if UF = 'RS' then Result := 'Rio Grande do Sul'   else
  if UF = 'RR' then Result := 'Roraima'             else
  if UF = 'RO' then Result := 'Rondônia'            else
  if UF = 'RN' then Result := 'Rio Grande do Norte' else
  if UF = 'RJ' then Result := 'Rio de Janeiro'      else
  if UF = 'PR' then Result := 'Paraná'              else
  if UF = 'PI' then Result := 'Piauí'               else
  if UF = 'PE' then Result := 'Pernambuco'          else
  if UF = 'PB' then Result := 'Paraíba'             else
  if UF = 'PA' then Result := 'Pará'                else
  if UF = 'MT' then Result := 'Mato Grosso'         else
  if UF = 'MS' then Result := 'Mato Grosso do Sul'  else
  if UF = 'MG' then Result := 'Minas Gerais'        else
  if UF = 'MA' then Result := 'Maranhão'            else
  if UF = 'GO' then Result := 'Goiás'               else
  if UF = 'ES' then Result := 'Espírito Santo'      else
  if UF = 'DF' then Result := 'Distrito Federal'    else
  if UF = 'CE' then Result := 'Ceará'               else
  if UF = 'BA' then Result := 'Bahia'               else
  if UF = 'AP' then Result := 'Amapá'               else
  if UF = 'AM' then Result := 'Amazonas'            else
  if UF = 'AL' then Result := 'Alagoas'             else
  if UF = 'AC' then Result := 'Acre'                else
  if UF = 'AN' then Result := 'Ambiente Nacional'   else
  if UF = 'EX' then Result := 'Exterior'            else
  //
  if UF = 'SVAN' then Result := 'Servidor Virtual da Ambiente Nacional' else
  if UF = 'SVRS' then Result := 'Servidor Virtual do Rio Grande do Sul' else
                    Result := '??????'
end;

procedure TGeral.GetFontSizes(FontName: String; const Items: TStrings);
  function EnumFontSizes(var EnumLogFont: TEnumLogFont;
  PTextMetric: PNewTextMetric; FontType: Integer; Data: LPARAM): Integer;
  export; stdcall;
  var
    size: String;
    i, s: Integer;
    value: Integer;
  begin
    if (FontType and TRUETYPE_FONTTYPE) <> 0 then
    begin
      // if TrueType you can use any size you want
      // so just fill sizes, like here
      FontSizes.Add('8');
      FontSizes.Add('9');
      FontSizes.Add('10');
      FontSizes.Add('11');
      FontSizes.Add('12');
      FontSizes.Add('14');
      FontSizes.Add('16');
      FontSizes.Add('18');
      FontSizes.Add('20');
      FontSizes.Add('22');
      FontSizes.Add('24');
      FontSizes.Add('26');
      FontSizes.Add('28');
      FontSizes.Add('36');
      FontSizes.Add('48');
      FontSizes.Add('72');
      Result := 0;
    end else
    begin
      s := Round((EnumLogFont.elfLogFont.lfHeight -
             PTextMetric.tmInternalLeading) * 72 / Screen.PixelsPerInch);
      size := IntToStr(s);
      Result := 1;
      for i := 0 to FontSizes.Count - 1 do
      begin
        value := StrToInt(FontSizes[i]);
        if value = s then
          Exit;
        if value > s then
        begin
          FontSizes.Insert(i, size);
          Exit;
        end;
      end;
      FontSizes.Add(size);
    end;
  end;
var
  DC: HDC;
begin
  FontSizes := TStringList.Create;
  DC := GetDC(0);
  FontSizes.BeginUpdate;
  try
    FontSizes.Clear;
    if FontName <> '' then
    begin
      //PixelsPerInch := GetDeviceCaps(DC, LOGPIXELSY);
      EnumFontFamilies(DC, PChar(FontName), @EnumFontSizes, Longint(Self));
    end;
  finally
    FontSizes.EndUpdate;
    ReleaseDC(0, DC);
    Items.Assign(FontSizes);
  end;
  FontSizes.Free;
end;

function TGeral.GetNumIntsFmt(Digitos: Integer): String;
var
  I: Integer;
  Zeros: String;
begin
  case Digitos of
    00: Result := '';
    01: Result := '0';
    02: Result := '00';
    03: Result := '000';
    04: Result := '0000';
    05: Result := '00000';
    06: Result := '000000';
    07: Result := '0000000';
    08: Result := '00000000';
    09: Result := '000000000';
    10: Result := '0000000000';
    11: Result := '00000000000';
    12: Result := '000000000000';
    13: Result := '0000000000000';
    14: Result := '00000000000000';
    15: Result := '000000000000000';
    16: Result := '0000000000000000';
    17: Result := '00000000000000000';
    18: Result := '000000000000000000';
    19: Result := '0000000000000000000';
    20: Result := '00000000000000000000';
    else
    begin
      Zeros := '00000000000000000000';
      for I := 21 to Digitos do
        Zeros := Zeros + '0';
      Result := Zeros;
    end;
  end;
end;

function TGeral.GetParentFrom(AControl: TWinControl): TCustomForm;
var
  LOwner: TWinControl;
begin
  LOwner := AControl.Parent;
  if (LOwner = nil) or (LOwner is TCustomForm) then
    Result := TCustomForm(LOwner)
  else
    Result := GetParentForm(LOwner);
end;

function TGeral.FormataVersao(Versao: Variant): String;
begin
  case VarType(Versao) of
    vtInt64, vtInteger: Result := VersaoTxt2006(Versao);
    vtString: Result := VersaoTxt2006(IMV(Versao));
    else MensagemDeErro('"VarType" desconhecido na função ' +
    '"Geral.FormataVersao"', 'FormataVersao()');
  end;
end;

function TGeral.FormataVersaoNFe(Versao: Variant): String;
var
 vt: Word;
begin
  vt := VarType(Versao);
  case vt of
    varInt64, varInteger, varDouble: Result := FFT_Dot(Versao, 2, siNegativo);
    vtString: Result := TFT_Dot(Versao, 2, siNegativo);
    else
    begin
      MensagemDeErro('"VarType" desconhecido na função ' +
      '"Geral.FormataVersaoNFe"' + sLineBreak +
      'Tipo de variável: = ' + VarTypeAsText(vt), 'FormataVersaoNFe()');
    end;
  end;
end;

function TGeral.VersaoTxt2006(Versao: Int64): String;
var
 Ano, Mes, Dia, Com : Int64;
begin
  Ano := Versao Div 100000000;
  Mes := (Versao Mod 100000000) Div 1000000;
  Dia := (Versao Mod 1000000) DIV 10000;
  Com := Versao Mod 10000;
  Result := IntToStr(Ano)+'.'+IntToStr(Mes)+'.'+
            IntToStr(Dia)+'.'+IntToStr(Com);
  APP_VERSAO := Result;          
end;

function TGeral.VersaoTxt_Vazio(Versao: Int64): String;
var
 Major, Minor, Release, Build : Integer;
begin
  Major := Versao Div 1000000;
  Minor := (Versao Mod 1000000) Div 10000;
  Release := (Versao Mod 10000) DIV 100;
  Build := Versao Mod 100;
  Result := IntToStr(Major)+' '+IntToStr(Minor)+' '+
            IntToStr(Release)+' '+IntToStr(Build);
end;

// XMLtoSQL XMLToText XMLToString
function TGeral.WideStringToSQLString(Texto: WideString): WideString;
const
  SP = '\\';
var
  i: integer;
  ServerPath: Boolean;
begin
  Result := '';
  if Texto = '' then exit;
  ServerPath := pos(SP, String(Texto)) = 1;
  for i := 1 to Length(Texto) do
  begin
    if Texto[i] = '\' then Result := Result + '\\' else
    //if Texto[i] = '/' then Result := Result + '//' else
    if Texto[i] = '"' then
    begin
      if (Length(Texto) = i) or (Texto[i+1] <> '"') then
        Result := Result + '""';
    end
    {
    ... colocar aqui outros empecilhos
    else if ... then ...
    }
    else
    if (i > 1) and (Texto[i] = '-') and (Texto[i-1] = '-') then
      Result := Result + ' '
    else
      Result := Result + Texto[i];
  end;

  Result := Substitui(Result, '\\\\', '\\');
  Result := Substitui(Result, '\\\', '\\');
  Result := Substitui(Result, '""""', '""');
  Result := Substitui(Result, '"""', '""');
  //
  if ServerPath then
    Result := SP + Result;
end;

function TGeral.VerificaDir(var Dir: String; const Barra: Char;
const Msg: String; ForcaCriar: Boolean): Boolean;
begin
  Dir :=  Trim(Dir);
  if Dir = '' then
  begin
    Result := False;
    MB_Aviso(('Diretório não informado: ' + Msg));
    Exit;
  end;
  if Dir[Length(Dir)] <> Barra then
    Dir := Dir + Barra;
  if ForcaCriar then
    ForceDirectories(Dir);
  Result := True;
end;

function TGeral.VersaoAplicTxt(Versao: Int64): String;
var
 Ano, Mes, Dia, Com : Int64;
begin
  Ano := Versao Div 100000000;
  Mes := (Versao mod 100000000) div 1000000;
  Dia := (Versao mod 1000000) div 10000;
  Com := Versao mod 10000;
  Result := FormatFloat('00', Ano) + '.' + FormatFloat('00', Mes) + '.' +
            FormatFloat('00', Dia) + '.' + FormatFloat('0000', Com);
  APP_VERSAO := Result;
end;

function TGeral.VersaoInt2006(Versao: String): Int64;
var
  a, m, d, h: String;
  i, t, n: Integer;
begin
  if Trim(Versao) = '' then Versao := '0000000000';
  a := '';
  m := '';
  d := '';
  h := '';
  t := Length(Versao);
  n := 0;
  for i := 1 to t do
  begin
    //if Versao[i] in (['0'..'9']) then
{$IFDEF DELPHI12_UP}
    if CharInSet(Versao[i], (['0'..'9'])) then
{$ELSE}
    if Versao[i] in (['0'..'9']) then
{$ENDIF}
    begin
      case n of
        0: a := a + Versao[i];
        1: m := m + Versao[i];
        2: d := d + Versao[i];
        3: h := h + Versao[i];
      end;
    end else n := n + 1;
  end;
  Result := I64MV(a
    + FormatFloat('00', IMV(m))
    + FormatFloat('00', IMV(d))
    + FormatFloat('0000', IMV(h)))
end;

//function T U n M L A G e r a l.FileVerInfo(const FileName: string; var FileInfo: TStringList): String;
function TGeral.FileVerInfo(FileName: String; Info: Integer): String;
const
  Key: array[1..9] of string = ('CompanyName', 'FileDescription', 'FileVersion',
  'InternalName', 'LegalCopyRight','OriginalFilename','ProductName',
  'ProductVersion','Comments');
(*  KeyBR: array[1..9] of string = ('Empresa', 'Descricao', 'Versao do Arquivo',
  'Nome Interno', 'CopyRight','Nome Original do Arquivo','Produto',
  'Versao do Produto','Comentarios');*)
var
  Dummy, BufferSize: Cardinal;
  Len: Integer;
  LoCharSet, HiCharSet: Word;
  Translate, Return: Pointer;
  StrFileInfo(*, Flags*): String;
(*  TargetOS, TypeArq: String;
  FixedFileInfo: Pointer;*)
  Buffer: PChar;
begin
  Result := '';
  BufferSize := GetFileVersionInfoSize(PChar(FileName), Dummy);
  if BufferSize <> 0 then
  begin
    GetMem(Buffer, Succ(BufferSize));
    try
      if GetFileVersionInfo(PChar(FileName), 0, BufferSize, Buffer) then
      if VerQueryValue(Buffer, '\VarFileInfo\Translation', Translate, UINT(Len)) then
      begin
        LoCharSet := LoWord(Longint(Translate^));
        HiCharSet := HiWord(Longint(Translate^));
        //for i := 1 to 9 do
          StrFileInfo := Format('\StringFileInfo\0%x0%x\%s', [LoCharSet, HiCharSet, Key[Info]]);
          if VerQueryValue(Buffer, PChar(StrFileInfo), Return, UINT(Len)) then
            //FileInfo.Add(KeyBR[i]+': '+PChar(Return));
            Result := (*KeyBR[i]+': '+*)PChar(Return);
//        end;
      end;
    finally
    end
  end;
end;

function TGeral.FIV(FaltaInfoVariavel: Boolean; Mensagem: String): Boolean;
begin
  if FaltaInfoVariavel then
  begin
    Result := True;
    if Mensagem <> '' then
      MB_Aviso(Mensagem);
  end else Result := False;
end;

function TGeral.FNE(Pre: String; Codigo: Integer; Pos: String;
  Casas: Integer): String;
begin
  if Codigo < 0 then
  begin
    Result := '_' + FFN(Codigo, Casas - 1);
  end else
  begin
    Result := FFN(Codigo, Casas);
  end;
  //
  Result := Pre + Result + Pos;
end;

function TGeral.SiglasWebService(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
  const sUF, Sigla, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  sUF;
    Res[Linha][1] :=  Sigla;
    Res[Linha][2] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);

  ////////////////////////////   )(*&¨%$#@!  ///////////////////////////////////
  //                                                                          //
  //                 Atualizado até 2009-11-14                                //
  //                                                                          //
  //* Estados Emissores pela Sefaz Virtual RS (Rio Grande do Sul):            //
  //  AC, AL, AM, AP, DF, MS, PB, RJ, RO, RR, SC, SE e TO.                    //
  //** Estados Emissores pela Sefaz Virtual AN (Ambiente Nacional):           //
  //  CE, ES, MA, PA, PI e RN.                                                //
  //////////////////////////////////////////////////////////////////////////////

  AddLinha(Result, Linha, 'AC'  , 'SVRS', 'Acre');
  AddLinha(Result, Linha, 'AL'  , 'SVRS', 'Alagoas');
  AddLinha(Result, Linha, 'AM'  , 'SVRS', 'Amazonas');
  AddLinha(Result, Linha, 'AP'  , 'SVRS', 'Amapá');
  AddLinha(Result, Linha, 'BA'  , 'BA'  , 'Bahia');
  AddLinha(Result, Linha, 'CE'  , 'SVAN', 'Ceará');
  AddLinha(Result, Linha, 'DF'  , 'SVRS', 'Distrito Federal');
  AddLinha(Result, Linha, 'ES'  , 'SVAN', 'Espírito Santo');
  AddLinha(Result, Linha, 'GO'  , 'GO'  , 'Goiás');
  AddLinha(Result, Linha, 'MA'  , 'SVAN', 'Maranhão');
  AddLinha(Result, Linha, 'MG'  , 'MG'  , 'Minas Gerais');
  AddLinha(Result, Linha, 'MS'  , 'MS'  , 'Mato Grosso do Sul');
  AddLinha(Result, Linha, 'MT'  , 'MT'  , 'Mato Grosso');
  AddLinha(Result, Linha, 'PA'  , 'SVAN', 'Pará');
  AddLinha(Result, Linha, 'PB'  , 'SVRS', 'Paraíba');
  AddLinha(Result, Linha, 'PE'  , 'PE'  , 'Pernambuco');
  AddLinha(Result, Linha, 'PI'  , 'SVAN', 'Piauí');
  AddLinha(Result, Linha, 'PR'  , 'PR'  , 'Paraná');
  AddLinha(Result, Linha, 'RJ'  , 'SVRS', 'Rio de Janeiro');
  AddLinha(Result, Linha, 'RN'  , 'SVAN', 'Rio Grande do Norte');
  AddLinha(Result, Linha, 'RO'  , 'SVRS', 'Rondônia');
  AddLinha(Result, Linha, 'RR'  , 'SVRS', 'Roraima');
  AddLinha(Result, Linha, 'RS'  , 'RS'  , 'Rio Grande do Sul');
  AddLinha(Result, Linha, 'SC'  , 'SVRS', 'Santa Catarina');
  AddLinha(Result, Linha, 'SE'  , 'SVRS', 'Sergipe');
  AddLinha(Result, Linha, 'SP'  , 'SP'  , 'São Paulo');
  AddLinha(Result, Linha, 'TO'  , 'SVRS', 'Tocantins');
  AddLinha(Result, Linha, 'SVRS', 'SVRS', 'SEFAZ Virtual do RS');
  AddLinha(Result, Linha, 'SVSP', 'SVSP', 'SEFAZ Virtual do SP');
  AddLinha(Result, Linha, 'SVAN', 'SVAN', 'SEFAZ Virtual Ambiente Nacional');
  //AddLinha(Result, Linha, 'SCAN', 'SCAN', 'Sistema de Contingência do Ambiente Nacional');
  //AddLinha(Result, Linha, 'DPEC', 'DPEC', 'Declaração Prévia de Emissão em Contingência');
  AddLinha(Result, Linha, 'SCV-AN', 'SCV-AN', 'Sefaz Virtual de Contingência Ambiente Nacional - (SVC-AN)');
  AddLinha(Result, Linha, 'SCV-RS', 'SCV-RS', 'Sefaz Virtual de Contingência Rio Grande do Sul - (SVC-RS)');
end;

function TGeral.SiglaUFValida(SiglaUF: String): Boolean;
begin
  if SiglaUF = 'EX' then Result := True else
  if SiglaUF = 'BR' then Result := True else
  if SiglaUF = 'TO' then Result := True else
  if SiglaUF = 'SP' then Result := True else
  if SiglaUF = 'SE' then Result := True else
  if SiglaUF = 'SC' then Result := True else
  if SiglaUF = 'RS' then Result := True else
  if SiglaUF = 'RR' then Result := True else
  if SiglaUF = 'RO' then Result := True else
  if SiglaUF = 'RN' then Result := True else
  if SiglaUF = 'RJ' then Result := True else
  if SiglaUF = 'PR' then Result := True else
  if SiglaUF = 'PI' then Result := True else
  if SiglaUF = 'PE' then Result := True else
  if SiglaUF = 'PB' then Result := True else
  if SiglaUF = 'PA' then Result := True else
  if SiglaUF = 'MT' then Result := True else
  if SiglaUF = 'MS' then Result := True else
  if SiglaUF = 'MG' then Result := True else
  if SiglaUF = 'MA' then Result := True else
  if SiglaUF = 'GO' then Result := True else
  if SiglaUF = 'ES' then Result := True else
  if SiglaUF = 'DF' then Result := True else
  if SiglaUF = 'CE' then Result := True else
  if SiglaUF = 'BA' then Result := True else
  if SiglaUF = 'AP' then Result := True else
  if SiglaUF = 'AM' then Result := True else
  if SiglaUF = 'AL' then Result := True else
  if SiglaUF = 'AC' then Result := True else
  Result := False;
end;

function TGeral.SelecionaItem(Lista: MyArrayLista; Coluna: Integer; TituloForm:
  String; TitulosColunas: array of String; ScreenWidth: Integer): String;
begin
  if CriaSelLista(Lista, TituloForm, TitulosColunas, ScreenWidth) then
  begin
    if FmdmkSelLista.FResult > -1 then
      Result := Lista[FmdmkSelLista.FResult][Coluna];
    FmdmkSelLista.Destroy;
  end;
end;

function TGeral.SelecionaItemLista(Lista: array of String; Coluna: Integer;
  TituloForm: String; TitulosColunas: array of String;
  ScreenWidth: Integer): String;
var
  Item: Integer;
begin
  Result := EmptyStr;
  if CriaSelListaArray(Lista, TituloForm, TitulosColunas, ScreenWidth) then
  begin
    if FmdmkSelLista.FResult > -1 then
      Result := Lista[FmdmkSelLista.FResult][Coluna];
    FmdmkSelLista.Destroy;
  end;
end;

function TGeral.SelStrI(Tipo: TSelStrI; Inteiro: Integer; TextoTrue,
  ResultFalse: String): String;
var
  Verdadeiro: Boolean;
begin
  case Tipo of
    ssiDif0: Verdadeiro := Inteiro <> 0;
    else
    begin
      MB_Erro('Tipo de comparação não definida em "SelStrI()"');
      Result := ' ? ? ? ?';
      Exit;
    end;
  end;
  if Verdadeiro then
    Result := TextoTrue + FormatFloat('0', Inteiro)
  else
    Result := ResultFalse;
end;

function TGeral.SelStrS(Tipo: TSelStrS; CompareStr, TextoTrue,
  ResultFalse: String): String;
//    Geral.SelStrS(sssDif_, EdDuplicata.Text, 'AND Duplicata='),
var
  Verdadeiro: Boolean;
begin
  case Tipo of
    sssDif_: Verdadeiro := CompareStr <> '';
    else
    begin
      MB_Erro('Tipo de comparação não definida em "SelStrS()"');
      Result := ' ? ? ? ?';
      Exit;
    end;
  end;
  if Verdadeiro then
    Result := TextoTrue + '"' + CompareStr + '"'
  else
    Result := ResultFalse;
end;

function TGeral.CriaSelLista(Lista: MyArrayLista; TituloForm: String;
TitulosColunas: array of String; ScreenWidth: Integer): Boolean;
const
  Tam = 18;
var
  n, i, k, j: Integer;
  t: array of Integer;
begin
  Result := False;
  n := High(Lista);
  if n = -1 then
  begin
    MB_Erro('Lista não definida!');
    Exit;
  end;
  k := High(lista[0]);
  if k = -1 then
  begin
    MB_Erro('Lista sem itens!');
    Exit;
  end;
  SetLength(t, k + 1);
  // Largura mínima das colunas
  for i := 0 to k do
    t[i] := 28;
  if n < 0 then
  begin
    MB_Aviso('Não há itens a escolher para a opção selecionada!');
    Exit;
  end;
  Application.CreateForm(TFmdmkSelLista, FmdmkSelLista);
  begin
    FmdmkSelLista.Caption := TituloForm;
    FmdmkSelLista.LaTitulo1A.Caption := Copy(TituloForm, Tam);
    FmdmkSelLista.LaTitulo1B.Caption := Copy(TituloForm, Tam);
    FmdmkSelLista.LaTitulo1C.Caption := Copy(TituloForm, Tam);
    FmdmkSelLista.Grade.ColCount := k + 1;
    FmdmkSelLista.Grade.RowCount := n + 2;
    for i := Low(TitulosColunas) to High(TitulosColunas) do
      FmdmkSelLista.Grade.Cells[i, 0] := TitulosColunas[i];
    for i := Low(Lista)  to n do
    begin
      for j := Low(lista[i]) to k do
      begin
        if Length(Lista[i][j]) * 8 > t[j] - 8 then
          t[j] := (Length(Lista[i][j]) * 8) + 8;
        //if Length(Lista[i][1]) * 7 > t1 then
          //t1 := Length(Lista[i][1]) * 7;
        FmdmkSelLista.Grade.Cells[j, i + 1] := Lista[i][j];
        //FmdmkSelLista.Grade.Cells[1, i + 1] := Lista[i][1];
      end;
    end;
    n := 0;
    for i := 0 to k do
    begin
      FmdmkSelLista.Grade.ColWidths[i] := t[i];
      n := n + t[i];
    end;
    n := n + 36;
    {
    FmdmkSelLista.Grade.ColWidths[0] := t0;
    FmdmkSelLista.Grade.ColWidths[1] := t1;
    }
    if n > ScreenWidth - 16 then n := ScreenWidth - 16;
    FmdmkSelLista.Width := n;
    FmdmkSelLista.ShowModal;
    Result := True;
  end;
end;

function TGeral.CriaSelListaArray(Lista: array of String; TituloForm: String;
  TitulosColunas: array of String; ScreenWidth: Integer): Boolean;
var
  nnn, i, kkk: Integer;
  ttt: array of Integer;
  JaExiste: Boolean;
begin
  Result := False;
  nnn := High(Lista);
  if nnn = -1 then
  begin
    MB_Erro('Lista não definida!');
    Exit;
  end;
  kkk := High(TitulosColunas);
  if kkk = -1 then
  begin
    MB_Erro('Lista sem itens!');
    Exit;
  end;
  SetLength(ttt, kkk + 1);
  // Largura mínima das colunas
  for i := 0 to kkk do
    ttt[i] := 28;
  if nnn < 0 then
  begin
    MB_Aviso('Não há itens a escolher para a opção selecionada!');
    Exit;
  end;
  JaExiste := FmdmkSelLista <> nil;
  if JaExiste = False then
    Application.CreateForm(TFmdmkSelLista, FmdmkSelLista);
  begin
    //FmdmkSelLista.FEdit := dmkEdit;
    SetLength(FmdmkSelLista.FLista, Length(Lista));
    for I := Low(Lista) to High(Lista) do
      FmdmkSelLista.FLista[I] := Lista[I];
    FmdmkSelLista.Caption := TituloForm;
    FmdmkSelLista.LaTitulo1A.Caption := Copy(TituloForm, 17);
    FmdmkSelLista.LaTitulo1B.Caption := Copy(TituloForm, 17);
    FmdmkSelLista.LaTitulo1C.Caption := Copy(TituloForm, 17);
    FmdmkSelLista.Grade.ColCount := kkk + 1;
    FmdmkSelLista.Grade.RowCount := nnn + 2;
    for i := Low(TitulosColunas) to High(TitulosColunas) do
      FmdmkSelLista.Grade.Cells[i, 0] := TitulosColunas[i];
    for i := 0 to nnn do
    begin
(*    Erro aqui no OnDestroy
      if Length(Lista[i]) * 10 > ttt[i] - 8 then
        ttt[i] := (Length(Lista[i]) * 8) + 8;
*)
      //
      FmdmkSelLista.Grade.Cells[0, i + 1] := Geral.FF0(I);
      FmdmkSelLista.Grade.Cells[1, i + 1] := Lista[i];
    end;
    nnn := 0;
    LarguraAutomaticaGrade(FmdmkSelLista.Grade, 0);
    for i := 0 to kkk do
    begin
      nnn := nnn + ttt[i];
    end;
    nnn := nnn + 36;
    if nnn > ScreenWidth - 16 then nnn := ScreenWidth - 16;
    FmdmkSelLista.Width := nnn;
    //FmdmkSelLista.ShowModal;
    FmdmkSelLista.Show;
    //Item := FmdmkSelLista.FResult;
    Result := True;
    FmdmkSelLista.Destroy;
  end;
end;

function TGeral.TipoDocCNPJCPF(Numero: String): String;
var
  Tam: Integer;
begin
  Tam := Length(SoNumero_TT(Numero));
  case Tam of
    11: Result := 'CPF';
    14..15: Result := 'CNPJ';
    else Result := 'CPF/CNPJ'
  end;
end;

function TGeral.TiraAspasDeTexto(Texto: WideString): WideString;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Texto) do
  begin
    if not (Texto[I] = '''') then
    if not (Texto[I] = '"') then
    if not (Texto[I] = ')') then
    if not (Texto[I] = '`') then
    Result := Result + Texto[I];
  end;
end;

function TGeral.SeparaEndereco(const Linha: Integer; const Endereco: String;
  var Numero, Compl, Bairro, xLogr, nLogr, xRua: String;
  TemBairro: Boolean; Memo: TMemo; Grade: TStringGrid): Boolean;
var
  p1, p2: Integer;
  Ender, Logr, Txt, Ini, Fim, Tmp: String;
  X: Char;
begin
  Numero := '';
  Compl := '';
  Bairro := '';
  xLogr := '';
  nLogr := '';
  xRua := '';
  //
  Ender := Endereco;
  //
  p2  := pos(',', Ender);
  if p2 > 0 then
  begin
    Logr := Trim(Copy(Ender, 1, p2 - 1));
    Ender := Trim(Copy(Ender, p2 + 1));

    // Numero
    if Length(Ender) > 0 then
    begin
      X := Ender[1];
      //if X in (['0'..'9']) then
{$IFDEF DELPHI12_UP}
      if CharInSet(X, (['0'..'9'])) then
{$ELSE}
      if X in (['0'..'9']) then
{$ENDIF}
      begin
        Numero := '';
        //while X in (['0'..'9','.']) do
{$IFDEF DELPHI12_UP}
        while CharInSet(X, (['0'..'9','.'])) do
{$ELSE}
        while X in (['0'..'9','.']) do
{$ENDIF}
        begin
          Numero := Numero + X;
          Ender := Copy(Ender, 2);
          if Length(Ender) > 0 then
            X := Ender[1]
          else X := ' ';
        end;
      end else Numero := '';
    end;

    // Limpar texto apartamento/bairro
    if Length(Ender) > 0 then
    begin
      X := Ender[1];
      //if not (X in (['A'..'Z'])) then
{$IFDEF DELPHI12_UP}
      if not (CharInSet(X, (['A'..'Z']))) then
{$ELSE}
      if not (X in (['A'..'Z'])) then
{$ENDIF}
      begin
        //while (not (X in (['A'..'Z']))) and (Length(Ender) > 0) do
{$IFDEF DELPHI12_UP}
        while (not (CharInSet(X, (['A'..'Z'])))) and (Length(Ender) > 0) do
{$ELSE}
        while (not (X in (['A'..'Z']))) and (Length(Ender) > 0) do
{$ENDIF}
        begin
          Ender := Copy(Ender, 2);
          if Length(Ender) > 0 then
            X := Ender[1]
          else X := 'A';
        end;
      end;
    end;

    // Apto = Bairro
    Compl := '';
    Bairro := '';
    p1 := pos(#150, Ender);
    if p1 = 0 then p1 := pos('-', Ender);
    if p1 > 0 then
    begin
      Compl  := Trim(Copy(Ender, 1, p1 - 1));
      Bairro := Trim(Copy(Ender, p1 + 1));
      //ShowMessage(Compl + ' # ' + Bairro);
    end else begin
      Txt := AnsiUppercase(Ender);
      if pos('APTO', Txt) > 0 then
        Compl := Ender
      else
      if pos('AP.', Txt) > 0 then
        Compl := Ender
      else
      if pos('APT.', Txt) > 0 then
        Compl := Ender
      else
      if pos('SALA', Txt) > 0 then
        Compl := Ender
      else
      if pos('LOJA', Txt) > 0 then
        Compl := Ender
      else
      if pos('CASA', Txt) > 0 then
        Compl := Ender
      else
      if pos('COMERCIO', Txt) > 0 then
        Compl := Ender
      else
      if pos('ZONA', Txt) > 0 then
        Bairro := Ender
      else
      if pos('BAIRRO', Txt) > 0 then
        Bairro := Ender
      else
      if pos('JD', Txt) > 0 then
        Bairro := Ender
      else
      if pos('CJ', Txt) > 0 then
        Bairro := Ender
      else
      if pos('PQ', Txt) > 0 then
        Bairro := Ender
      else
      if pos('V.', Txt) > 0 then
        Bairro := Ender
      else
      if pos('VILA', Txt) > 0 then
        Bairro := Ender
      else
      if pos('CENTRO', Txt) > 0 then
        Bairro := Ender
      else
      if pos('CHÁCARA', Txt) > 0 then
        Bairro := Ender
      else
      if pos('INDUS', Txt) > 0 then
        Bairro := Ender
      else
      if pos('RES', Txt) > 0 then
        Bairro := Ender
      {
      else
      if pos('CJ', Txt) then
        Compl := Ender
      }
      else begin
        if TemBairro then
        begin
          if Trim(Ender) <> '' then
            if MB_Pergunta('"' + Ender + '" é um bairro?') = ID_YES then
              Bairro := Ender
            else
              Compl := Compl + Ender;
        end else Compl := Compl + Ender;
      end;
    end;
    // Parei aqui Fazer
    //Ender := Trim(Ender);
  end else
  begin
    Ender := Trim(Ender);
    // Numero
    if Length(Ender) > 0 then
    begin
      Ini := '';
      Fim := '';
      Tmp := Ender;
      Logr := Ender;
      while Tmp <> '' do
      begin
        //if Tmp[Length(Tmp)] in (['0'..'9','.']) then
{$IFDEF DELPHI12_UP}
        if CharInSet(Tmp[Length(Tmp)], (['0'..'9','.'])) then
{$ELSE}
        if Tmp[Length(Tmp)] in (['0'..'9','.']) then
{$ENDIF}
        begin
          Fim := Tmp[Length(Tmp)] + Fim;
          Tmp := Copy(Tmp, 1, Length(Tmp) -1);
        end else
        begin
          Ini := Tmp;
          Tmp := '';
        end;
      end;
      Fim := SoNumero_TT(Fim);
      if (Fim <> '') and (SoNumero_TT(Ini) = '') then
      begin
        Logr := Trim(Ini);
        Numero := Fim;
      end;
    end;
  end;
  //
  SeparaLogradouro(Linha, Logr, xLogr, nLogr, xRua, Memo, Grade);
  Result := True;
end;

procedure TGeral.SeparaLogradouro(const Linha: Integer; const Logr: String; var Sep1, Sep2,
  Sep3: String; Memo: TMemo; Grade: TStringGrid);
  function ObtemCodigoLogradouro(Texto: String; Codigo: Integer): String;
  begin
    case Codigo of
      -9	: Result := 'AEROPORTO';
      -6	: Result := 'ALAMEDA';
      -10	: Result := 'ÁREA';
      -1	: Result := 'AVENIDA';
      -2	: Result := 'BECO';
      -11	: Result := 'CAMPO';
      -12	: Result := 'CHÁCARA';
      -13	: Result := 'COLÔNIA';
      -14	: Result := 'CONDOMÍNIO';
      -15	: Result := 'CONJUNTO';
      -50	: Result := 'CONTORNO';
      -16	: Result := 'DISTRITO';
      -17	: Result := 'ESPLANADA';
      -18	: Result := 'ESTAÇÃO';
      -3	: Result := 'ESTRADA';
      -19	: Result := 'FAVELA';
      -20	: Result := 'FAZENDA';
      -21	: Result := 'FEIRA';
      -49	: Result := 'GLEBA';
      -22	: Result := 'JARDIM';
      -23	: Result := 'LADEIRA';
      -24	: Result := 'LAGO';
      -25	: Result := 'LAGOA';
      -26	: Result := 'LARGO';
      -47	: Result := 'LINHA';
      -48	: Result := 'LOCALIDADE';
      -27	: Result := 'LOTEAMENTO';
      -28	: Result := 'MORRO';
      -29	: Result := 'NÚCLEO';
      -30	: Result := 'PARQUE';
      -31	: Result := 'PASSARELA';
      -32	: Result := 'PÁTIO';
      -7	: Result := 'PRAÇA';
      -46	: Result := 'PROPRIEDADE';
      -33	: Result := 'QUADRA';
      -34	: Result := 'RECANTO';
      -35	: Result := 'RESIDENCIA';
      -36	: Result := 'RODOVIA';
      -4	: Result := 'RUA';
      -37	: Result := 'SETOR';
      -38	: Result := 'SÍTIO';
      -5	: Result := 'TRAVESSA';
      -39	: Result := 'TRECHO';
      -40	: Result := 'TREVO';
      -41	: Result := 'VALE';
      -42	: Result := 'VEREDA';
      -43	: Result := 'VIA';
      -44	: Result := 'VIADUTO';
      -8	: Result := 'VIELA';
      -45	: Result := 'VILA';
       else Result := '';
    end;
  end;
const
  LogrTxt: array[0..37] of String  =
  ('#','AV','AREA', 'RUA','PRA','TR','TV','ROD','PAS','EST','CHC','BR','TRV','ALA','CJ',
  'CONJ','FAZ','VIL','COJ','DIS','LOT','QDRA','VIA','IRUA','CHACARA','SITIO',
  'QD','QUADRA','MS','PC','CH','PÇ','CONDOMINIO','LOC', 'GLEBA', 'CONTORNO',
  'VIELA', (*...*)'R');
  LogrCod: Array[0..37] of Integer =
  (-4 ,-1  ,-10     ,-4   ,-7   ,-5  ,-5  ,-36  ,-31  ,-3   ,-12  ,-36 ,-5   ,-6   ,-15 ,
  -15   ,-20  ,-45  ,-15  ,-16  ,-27  ,-33   ,-43  ,-4    ,-12      ,-38    ,
  -33 ,-33     ,-36 ,-7  ,-12 ,-7  ,-14         ,-48  ,-49     ,-50        ,
  -8    , (*...*)-4);
var
  Txt, Log(*, Pesq*): String;
  i, p0, p1, p2: Integer;
  Achou: Boolean;
begin
  Txt := AnsiUppercase(Logr);
  if pos('PICADA', Trim(Txt)) = 1 then
    Txt := 'ESTRADA ' + Trim(Txt);
  if pos('ZZ', Trim(Txt)) = 1 then
  begin
    Txt := Trim(Txt);
    while (Length(Txt) > 0) and (Txt[1] = 'Z') do
      Txt := Copy(Txt, 2);
  end;
  p1 := pos('.', Txt);
  p2 := pos(' ', Txt);
  if (p1 > 0) and (p2 > 0) then
  begin
    if p2 > p1 then p0 := p1 else p0 := p2;
  end else if p1 > 0 then p0 := p1
  else p0 := p2;
  //
  Log := Copy(Txt, 1, p0 - 1);
  Sep1 := '';
  Sep2 := '';
  Sep3 := Logr;
  Achou := False;
  for i := 0 to High(LogrTxt) do
  begin
    {
    for J := -45 to -1 do
    begin

    end;
    }
    if Uppercase(Copy(Log, 1, Length(LogrTxt[i]))) = LogrTxt[i] then
    begin
      Sep1 := Copy(Logr, 1, p0);
      Sep2 := IntToStr(LogrCod[i]);
      Sep3 := Trim(Copy(Logr, p0 + 1));
      //Achou := True;
      Exit;
    end;
    //else
      //ShowMessage('Não achou: "' + Log + '" em "' + Txt + '"');
  end;
  if not Achou and (Log <> '') then
  begin
    if Memo <> nil then
      Memo.Lines.Add(Log)
    else
    if Grade <> nil then
    begin
      I := Grade.RowCount;
      if (I > 2) or (Grade.Cells[0, 1] <> '') then
        Grade.RowCount := I + 1
      else
        I := I -1;
      Grade.Cells[0, i] := FF0(I);
      Grade.Cells[1, i] := FF0(Linha);
      Grade.Cells[2, i] := Logr;
    end else
      MB_Aviso('Logradouro não localizado: "' + Log + '"' + sLineBreak +
      'No endereço: "' + Logr + '"');
  end;
end;

function TGeral.SeparaNomes(Texto: String): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Texto) do
  begin
    //if Texto[I] in ['/', '-', ',', ')', '\', ';'] then
{$IFDEF DELPHI12_UP}
    if CharInSet(Texto[I], ['/', '-', ',', ')', '\', ';']) then
{$ELSE}
    if Texto[I] in ['/', '-', ',', ')', '\', ';'] then
{$ENDIF}
    begin
      // Repõe o ")"
      if Texto[I] = ')' then
        Result := Result + Texto[I];
      //
      Result := Result + '|';
    end else
      Result := Result + Texto[I];
    //
  end;
end;

function TGeral.SeparaPrimeiraOcorrenciaDeTexto(const Separador, Texto: String;
  var PrimeiraOcorrencia, NovoTexto: String): Boolean;
var
  P: Integer;
begin
  Result := False;
  if Texto <> '' then
  begin
    P := pos(Separador, Texto);
    if P > 0 then
    begin
      PrimeiraOcorrencia := Copy(Texto, 1, P - 1);
      NovoTexto := Copy(Texto, P + 1);
    end else begin
      PrimeiraOcorrencia := Texto;
      NovoTexto := '';
    end;
    Result := True;
  end;
end;

function TGeral.SerialNum(FDrive: String): String;
var
  nVNameSer: PDWORD;
  pVolName: PChar;
  FSSysFlags, maxCmpLen: DWord;
  pFSBuf: PChar;
  drv: String;
begin
  try
    drv := FDrive + '\';

    GetMem(pVolName, MAX_PATH);
    GetMem(pFSBuf, MAX_PATH);
    GetMem(nVNameSer, MAX_PATH);

    GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
      maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

    Result := IntToStr(nVNameSer^);

    FreeMem(pVolName, MAX_PATH);
    FreeMem(pFSBuf, MAX_PATH);
    FreeMem(nVNameSer, MAX_PATH);
  except
    Result := '';
  end;
end;

function TGeral.SeSenao(ACondicao: Boolean; ATrue, AFalse: Variant): Variant;
begin
  Result := AFalse;
  if ACondicao then
    Result := ATrue;
end;

function TGeral.FTX(Numero: Double; Inteiros, Casas: Integer; Sinal: TSinal) : String;
var
  Num: String;
begin
  Num := FFT(Numero, Casas, Sinal);
  Num := SoNumeroESinal_TT(Num);
  while Length(Num) < Casas + Inteiros do Num := '0' + Num;
  if (Sinal = siNegativo) and (Numero < 0) then
  begin
    if (Num[1] = '0') then
      Num[1] := '-'
    else
      Num := '-' + Num;
  end;    
  Result := Num;
end;

function TGeral.RichRow(m: TCustomMemo): Longint;
begin
  Result := SendMessage(m.Handle, EM_LINEFROMCHAR, m.SelStart, 0);
end;

function TGeral.RichCol(m: TCustomMemo): Longint;
begin
  Result := m.SelStart - SendMessage(m.Handle, EM_LINEINDEX, SendMessage(m.Handle,
    EM_LINEFROMCHAR, m.SelStart, 0), 0);
end;

function TGeral.AAAAMM_To_Date(AAAAMM: Integer; QualDiaMes: TQualDiaMes;
  Dia: Word): TDateTime;
begin
  if AAAAMM = 0 then
    Result := 0
  else begin
    Result := EncodeDate(AAAAMM div 100, AAAAMM mod 100, 1);
    case QualDiaMes of
      qdmFirst: ; // nada
      qdmMidle: Result := Result + Dia - 1;
      qdmLast: Result := IncMonth(Result, 1) - 1;
    end;
  end;
end;

function TGeral.AbreArquivo(Arquivo: String; Executa: Boolean = False): Boolean;
begin
  Result := False;
  //
  if ExtractFileExt(Arquivo) <> '' then
  begin
    if FileExists(Arquivo) then
    begin
      try
        if Executa then
        begin
          if ShellExecute(0, nil, Pchar(Arquivo), nil, nil, SW_SHOWNORMAL) > 32 then
            Result := True;
        end else
        begin
          if ShellExecute(0, nil, 'explorer.exe', Pchar('/select,' + Arquivo), nil, SW_SHOWNORMAL) > 32 then
            Result := True;
        end;
      except
        ;
      end;
    end;
  end else
  begin
    if DirectoryExists(Arquivo) then
    begin
      try
        if ShellExecute(0, nil, Pchar(Arquivo), nil, nil, SW_SHOWNORMAL) > 32 then
          Result := True;
      except
        ;
      end;
    end;
  end;
end;

procedure TGeral.AddLin(var Res: MyArrayLista; var Linha: Integer;
  const Codigo: String; Descricao: array of String);
var
  i: Integer;
begin
  SetLength(Res, Linha+1);
  SetLength(Res[Linha], 2);
  Res[Linha][0] :=  Codigo;
  Res[Linha][1] :=  '';
  for i := Low(Descricao) to High(Descricao) do
    Res[Linha][1] :=  Res[Linha][1] + Descricao[i];
  inc(Linha, 1);
  //
end;

function TGeral.DescricaoDeArrStrStr(const Codigo: String; const ArrTextos:
  MyArrayLista; var Texto: String; ColLoc, ColSel: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  Texto  := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
    if Uppercase(ArrTextos[i][ColLoc]) = Uppercase(Codigo) then
    begin
      Result := True;
      Texto := ArrTextos[i][ColSel];
      Break;
    end;
end;

function TGeral.DescricaoDeArrStrStr2(const Codigo: String; const ArrTextos:
MyArrayLista; var Texto1, Texto2: String; ColLoc, ColSel1, ColSel2: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  Texto1 := '';
  Texto2 := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
    if Uppercase(ArrTextos[i][ColLoc]) = Uppercase(Codigo) then
    begin
      Result := True;
      Texto1 := ArrTextos[i][ColSel1];
      Texto2 := ArrTextos[i][ColSel2];
      Break;
    end;
end;

function TGeral.DescricaoDeArrStrStr3(const Codigo: String;
  const ArrTextos: MyArrayLista; var Texto1, Texto2, Texto3: String; ColLoc,
  ColSel1, ColSel2, ColSel3: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  Texto1 := '';
  Texto2 := '';
  Texto3 := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
    if Uppercase(ArrTextos[i][ColLoc]) = Uppercase(Codigo) then
    begin
      Result := True;
      Texto1 := ArrTextos[i][ColSel1];
      Texto2 := ArrTextos[i][ColSel2];
      Texto3 := ArrTextos[i][ColSel3];
      Break;
    end;
end;

function TGeral.DescricaoEAvisosDeArrStrStr(const Codigo: String; const ArrTextos,
ArrAvisos: MyArrayLista; var Texto, Aviso: String; ColLocTexto, ColSelTexto,
ColSelLocAv, ColLocAviso, ColSelAviso: Integer): Integer;
  procedure AvisoDeArrStr(const CodLoc: String; const ColLoc, ColSel: Integer;
  var Aviso: String; var Res: Integer);
  var
    i: Integer;
  begin
    for i := Low(ArrAvisos) to High(ArrAvisos) do
    if ArrAvisos[i][ColLoc] = CodLoc then
    begin
      Res   := 3;
      if Length(Aviso) = 0 then
        Aviso := ArrAvisos[i][ColSel]
      else
        Aviso := Aviso + sLineBreak + ArrAvisos[i][ColSel];
      Break;
    end;
  end;
var
  i, pI, pF: Integer;
  Codes, SubCod: String;
begin
  Result := 0;
  Texto  := '';
  Aviso  := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
  if ArrTextos[i][ColLocTexto] = Codigo then
  begin
    Result := 1;
    Texto  := Trim(ArrTextos[i][ColSelTexto]);
    Codes  := Trim(ArrTextos[i][ColSelLocAv]);
    Break;
  end;
  //
  if Result = 1 then
  begin
    if Codes  <> '' then
    begin
      Result := 2;
      pI := 1;
      while Length(Codes) > 0 do
      begin
        pF := Pos(',', Codes);
        if pF > 0 then
        begin
          SubCod := Copy(Codes, pI, pF-1);
          Codes  := Copy(Codes, pF+1, Length(Codes));
          AvisoDeArrStr(SubCod, ColLocAviso, ColSelAviso, Aviso, Result);
        end else
        begin
          AvisoDeArrStr(Codes, ColLocAviso, ColSelAviso, Aviso, Result);
          Codes := '';
        end;
      end;
    end;
  end;
end;

function TGeral.DescricaoEAvisosEItensDeArrStrStr(const Codigo: String;
const ArrTextos, ArrAvisos: MyArrayLista; var Texto, Aviso, Item1, Item2: String;
ColLocTexto, ColSelTexto, ColSelLocAv, ColLocAviso, ColSelAviso, CodItem1,
CodItem2: Integer): Integer;
  procedure AvisoDeArrStr(const CodLoc: String; const ColLoc, ColSel: Integer;
  var Aviso: String; var Res: Integer);
  var
    i: Integer;
  begin
    for i := Low(ArrAvisos) to High(ArrAvisos) do
    if ArrAvisos[i][ColLoc] = CodLoc then
    begin
      Res   := 3;
      if Length(Aviso) = 0 then
        Aviso := ArrAvisos[i][ColSel]
      else
        Aviso := Aviso + sLineBreak + ArrAvisos[i][ColSel];
      Break;
    end;
  end;
var
  i, pI, pF: Integer;
  Codes, SubCod: String;
begin
  Result := 0;
  Texto  := '';
  Aviso  := '';
  for i := Low(ArrTextos) to High(ArrTextos) do
  if ArrTextos[i][ColLocTexto] = Codigo then
  begin
    Result := 1;
    Texto  := Trim(ArrTextos[i][ColSelTexto]);
    Item1  := Trim(ArrTextos[i][CodItem1]);
    Item2  := Trim(ArrTextos[i][CodItem2]);
    Codes  := Trim(ArrTextos[i][ColSelLocAv]);
    Break;
  end;
  //
  if Result = 1 then
  begin
    if Codes  <> '' then
    begin
      Result := 2;
      pI := 1;
      while Length(Codes) > 0 do
      begin
        pF := Pos(',', Codes);
        if pF > 0 then
        begin
          SubCod := Copy(Codes, pI, pF-1);
          Codes  := Copy(Codes, pF+1, Length(Codes));
          AvisoDeArrStr(SubCod, ColLocAviso, ColSelAviso, Aviso, Result);
        end else
        begin
          AvisoDeArrStr(Codes, ColLocAviso, ColSelAviso, Aviso, Result);
          Codes := '';
        end;
      end;
    end;
  end;
end;

function TGeral.DFT_SPED(Valor: Double; Casas: Integer; Sinal: TSinal): String;
var
  Aux: String;
begin
  Aux := FFT(Valor, Casas, Sinal);
  while pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, Aux) > 0 do
    Delete(Aux, pos({$IFDEF DELPHI12_UP}FormatSettings.{$ENDIF}ThousandSeparator, Aux), 1);
  // Forçate vírgula
  while pos('.', Aux) > 0 do
    Aux[pos('.', Aux)] := ',';
  Result := Aux;
end;

function TGeral.SINTEGRA_ExpNat(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  AddLin(Result, Linha, '1', ['Exportação Direta']);
  AddLin(Result, Linha, '2', ['Exportação Indireta']);
  AddLin(Result, Linha, '3', ['Exportação Direta - Regime Simplificado']);
  AddLin(Result, Linha, '4', ['Exportação Indireta - Regime Simplificado']);
end;

function TGeral.SINTEGRA_ExpConhTip(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  AddLin(Result, Linha, '01', ['AWB']);
  AddLin(Result, Linha, '02', ['MAWB']);
  AddLin(Result, Linha, '03', ['HAWB']);
  AddLin(Result, Linha, '04', ['COMAT']);
  AddLin(Result, Linha, '06', ['R.EXPRESSAS']);
  AddLin(Result, Linha, '07', ['ETIQ.REXPRESSAS']);
  AddLin(Result, Linha, '08', ['HR.EXPRESSAS']);
  AddLin(Result, Linha, '09', ['AV7']);
  AddLin(Result, Linha, '10', ['BL']);
  AddLin(Result, Linha, '11', ['MBL']);
  AddLin(Result, Linha, '12', ['HBL']);
  AddLin(Result, Linha, '13', ['CRT']);
  AddLin(Result, Linha, '14', ['DSIC']);
  AddLin(Result, Linha, '16', ['COMAT BL']);
  AddLin(Result, Linha, '17', ['RWB']);
  AddLin(Result, Linha, '18', ['HRWB']);
  AddLin(Result, Linha, '19', ['TIF/DTA']);
  AddLin(Result, Linha, '20', ['CP2']);
  AddLin(Result, Linha, '91', ['NÂO IATA']);
  AddLin(Result, Linha, '92', ['MNAO IATA']);
  AddLin(Result, Linha, '93', ['HNAO IATA']);
  AddLin(Result, Linha, '99', ['OUTROS']);
end;

{
function TGeral.SINTEGRA_PaisesSISCOMEX(): MyArrayLista;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//Última atualização: 22/10/2009
  AddLin(Result, Linha, '00132', ['AFEGANISTAO']);
  AddLin(Result, Linha, '00175', ['ALBANIA, REPUBLICA DA']);
  AddLin(Result, Linha, '00230', ['ALEMANHA']);
  AddLin(Result, Linha, '00310', ['BURKINA FASO']);
  AddLin(Result, Linha, '00370', ['ANDORRA']);
  AddLin(Result, Linha, '00400', ['ANGOLA']);
  AddLin(Result, Linha, '00418', ['ANGUILLA']);
  AddLin(Result, Linha, '00434', ['ANTIGUA E BARBUDA']);
  AddLin(Result, Linha, '00477', ['ANTILHAS HOLANDESAS']);
  AddLin(Result, Linha, '00531', ['ARABIA SAUDITA']);
  AddLin(Result, Linha, '00590', ['ARGELIA']);
  AddLin(Result, Linha, '00639', ['ARGENTINA']);
  AddLin(Result, Linha, '00647', ['ARMENIA, REPUBLICA DA']);
  AddLin(Result, Linha, '00655', ['ARUBA']);
  AddLin(Result, Linha, '00698', ['AUSTRALIA']);
  AddLin(Result, Linha, '00728', ['AUSTRIA']);
  AddLin(Result, Linha, '00736', ['AZERBAIJAO, REPUBLICA DO']);
  AddLin(Result, Linha, '00779', ['BAHAMAS, ILHAS']);
  AddLin(Result, Linha, '00809', ['BAHREIN, ILHAS']);
  AddLin(Result, Linha, '00817', ['BANGLADESH']);
  AddLin(Result, Linha, '00833', ['BARBADOS']);
  AddLin(Result, Linha, '00850', ['BELARUS, REPUBLICA DA']);
  AddLin(Result, Linha, '00876', ['BELGICA']);
  AddLin(Result, Linha, '00884', ['BELIZE']);
  AddLin(Result, Linha, '00906', ['BERMUDAS']);
  AddLin(Result, Linha, '00930', ['MIANMAR (BIRMANIA)']);
  AddLin(Result, Linha, '00973', ['BOLIVIA, ESTADO PLURINACIONAL DA']);
  AddLin(Result, Linha, '00981', ['BOSNIA-HERZEGOVINA (REPUBLICA DA)']);
  AddLin(Result, Linha, '01015', ['BOTSUANA']);
  AddLin(Result, Linha, '01058', ['BRASIL']);
  AddLin(Result, Linha, '01082', ['BRUNEI']);
  AddLin(Result, Linha, '01112', ['BULGARIA, REPUBLICA DA']);
  AddLin(Result, Linha, '01155', ['BURUNDI']);
  AddLin(Result, Linha, '01198', ['BUTAO']);
  AddLin(Result, Linha, '01279', ['CABO VERDE, REPUBLICA DE']);
  AddLin(Result, Linha, '01376', ['CAYMAN, ILHAS']);
  AddLin(Result, Linha, '01414', ['CAMBOJA']);
  AddLin(Result, Linha, '01457', ['CAMAROES']);
  AddLin(Result, Linha, '01490', ['CANADA']);
  AddLin(Result, Linha, '01504', ['GUERNSEY, ILHA DO CANAL (INCLUI ALDERNEY E SARK)']);
  AddLin(Result, Linha, '01508', ['JERSEY, ILHA DO CANAL']);
  AddLin(Result, Linha, '01511', ['CANARIAS, ILHAS']);
  AddLin(Result, Linha, '01538', ['CAZAQUISTAO, REPUBLICA DO']);
  AddLin(Result, Linha, '01546', ['CATAR']);
  AddLin(Result, Linha, '01589', ['CHILE']);
  AddLin(Result, Linha, '01600', ['CHINA, REPUBLICA POPULAR']);
  AddLin(Result, Linha, '01619', ['FORMOSA (TAIWAN)']);
  AddLin(Result, Linha, '01635', ['CHIPRE']);
  AddLin(Result, Linha, '01651', ['COCOS(KEELING),ILHAS']);
  AddLin(Result, Linha, '01694', ['COLOMBIA']);
  AddLin(Result, Linha, '01732', ['COMORES, ILHAS']);
  AddLin(Result, Linha, '01775', ['CONGO']);
  AddLin(Result, Linha, '01830', ['COOK, ILHAS']);
  AddLin(Result, Linha, '01872', ['COREIA (DO NORTE), REP.POP.DEMOCRATICA']);
  AddLin(Result, Linha, '01902', ['COREIA (DO SUL), REPUBLICA DA']);
  AddLin(Result, Linha, '01937', ['COSTA DO MARFIM']);
  AddLin(Result, Linha, '01953', ['CROACIA (REPUBLICA DA)']);
  AddLin(Result, Linha, '01961', ['COSTA RICA']);
  AddLin(Result, Linha, '01988', ['COVEITE']);
  AddLin(Result, Linha, '01996', ['CUBA']);
  AddLin(Result, Linha, '02291', ['BENIN']);
  AddLin(Result, Linha, '02321', ['DINAMARCA']);
  AddLin(Result, Linha, '02356', ['DOMINICA,ILHA']);
  AddLin(Result, Linha, '02399', ['EQUADOR']);
  AddLin(Result, Linha, '02402', ['EGITO']);
  AddLin(Result, Linha, '02437', ['ERITREIA']);
  AddLin(Result, Linha, '02445', ['EMIRADOS ARABES UNIDOS']);
  AddLin(Result, Linha, '02453', ['ESPANHA']);
  AddLin(Result, Linha, '02461', ['ESLOVENIA, REPUBLICA DA']);
  AddLin(Result, Linha, '02470', ['ESLOVACA, REPUBLICA']);
  AddLin(Result, Linha, '02496', ['ESTADOS UNIDOS']);
  AddLin(Result, Linha, '02518', ['ESTONIA, REPUBLICA DA']);
  AddLin(Result, Linha, '02534', ['ETIOPIA']);
  AddLin(Result, Linha, '02550', ['FALKLAND (ILHAS MALVINAS)']);
  AddLin(Result, Linha, '02593', ['FEROE, ILHAS']);
  AddLin(Result, Linha, '02674', ['FILIPINAS']);
  AddLin(Result, Linha, '02712', ['FINLANDIA']);
  AddLin(Result, Linha, '02755', ['FRANCA']);
  AddLin(Result, Linha, '02810', ['GABAO']);
  AddLin(Result, Linha, '02852', ['GAMBIA']);
  AddLin(Result, Linha, '02895', ['GANA']);
  AddLin(Result, Linha, '02917', ['GEORGIA, REPUBLICA DA']);
  AddLin(Result, Linha, '02933', ['GIBRALTAR']);
  AddLin(Result, Linha, '02976', ['GRANADA']);
  AddLin(Result, Linha, '03018', ['GRECIA']);
  AddLin(Result, Linha, '03050', ['GROENLANDIA']);
  AddLin(Result, Linha, '03093', ['GUADALUPE']);
  AddLin(Result, Linha, '03131', ['GUAM']);
  AddLin(Result, Linha, '03174', ['GUATEMALA']);
  AddLin(Result, Linha, '03255', ['GUIANA FRANCESA']);
  AddLin(Result, Linha, '03298', ['GUINE']);
  AddLin(Result, Linha, '03310', ['GUINE-EQUATORIAL']);
  AddLin(Result, Linha, '03344', ['GUINE-BISSAU']);
  AddLin(Result, Linha, '03379', ['GUIANA']);
  AddLin(Result, Linha, '03417', ['HAITI']);
  AddLin(Result, Linha, '03450', ['HONDURAS']);
  AddLin(Result, Linha, '03514', ['HONG KONG']);
  AddLin(Result, Linha, '03557', ['HUNGRIA, REPUBLICA DA']);
  AddLin(Result, Linha, '03573', ['IEMEN']);
  AddLin(Result, Linha, '03595', ['MAN, ILHA DE']);
  AddLin(Result, Linha, '03611', ['INDIA']);
  AddLin(Result, Linha, '03654', ['INDONESIA']);
  AddLin(Result, Linha, '03697', ['IRAQUE']);
  AddLin(Result, Linha, '03727', ['IRA, REPUBLICA ISLAMICA DO']);
  AddLin(Result, Linha, '03751', ['IRLANDA']);
  AddLin(Result, Linha, '03794', ['ISLANDIA']);
  AddLin(Result, Linha, '03832', ['ISRAEL']);
  AddLin(Result, Linha, '03867', ['ITALIA']);
  AddLin(Result, Linha, '03913', ['JAMAICA']);
  AddLin(Result, Linha, '03964', ['JOHNSTON, ILHAS']);
  AddLin(Result, Linha, '03999', ['JAPAO']);
  AddLin(Result, Linha, '04030', ['JORDANIA']);
  AddLin(Result, Linha, '04111', ['KIRIBATI']);
  AddLin(Result, Linha, '04200', ['LAOS, REP.POP.DEMOCR.DO']);
  AddLin(Result, Linha, '04235', ['LEBUAN,ILHAS']);
  AddLin(Result, Linha, '04260', ['LESOTO']);
  AddLin(Result, Linha, '04278', ['LETONIA, REPUBLICA DA']);
  AddLin(Result, Linha, '04316', ['LIBANO']);
  AddLin(Result, Linha, '04340', ['LIBERIA']);
  AddLin(Result, Linha, '04383', ['LIBIA']);
  AddLin(Result, Linha, '04405', ['LIECHTENSTEIN']);
  AddLin(Result, Linha, '04421', ['LITUANIA, REPUBLICA DA']);
  AddLin(Result, Linha, '04456', ['LUXEMBURGO']);
  AddLin(Result, Linha, '04472', ['MACAU']);
  AddLin(Result, Linha, '04499', ['MACEDONIA, ANT.REP.IUGOSLAVA']);
  AddLin(Result, Linha, '04502', ['MADAGASCAR']);
  AddLin(Result, Linha, '04525', ['MADEIRA, ILHA DA']);
  AddLin(Result, Linha, '04553', ['MALASIA']);
  AddLin(Result, Linha, '04588', ['MALAVI']);
  AddLin(Result, Linha, '04618', ['MALDIVAS']);
  AddLin(Result, Linha, '04642', ['MALI']);
  AddLin(Result, Linha, '04677', ['MALTA']);
  AddLin(Result, Linha, '04723', ['MARIANAS DO NORTE']);
  AddLin(Result, Linha, '04740', ['MARROCOS']);
  AddLin(Result, Linha, '04766', ['MARSHALL,ILHAS']);
  AddLin(Result, Linha, '04774', ['MARTINICA']);
  AddLin(Result, Linha, '04855', ['MAURICIO']);
  AddLin(Result, Linha, '04880', ['MAURITANIA']);
  AddLin(Result, Linha, '04885', ['MAYOTTE (ILHAS FRANCESAS)']);
  AddLin(Result, Linha, '04901', ['MIDWAY, ILHAS']);
  AddLin(Result, Linha, '04936', ['MEXICO']);
  AddLin(Result, Linha, '04944', ['MOLDAVIA, REPUBLICA DA']);
  AddLin(Result, Linha, '04952', ['MONACO']);
  AddLin(Result, Linha, '04979', ['MONGOLIA']);
  AddLin(Result, Linha, '04985', ['MONTENEGRO']);
  AddLin(Result, Linha, '04995', ['MICRONESIA']);
  AddLin(Result, Linha, '05010', ['MONTSERRAT,ILHAS']);
  AddLin(Result, Linha, '05053', ['MOCAMBIQUE']);
  AddLin(Result, Linha, '05070', ['NAMIBIA']);
  AddLin(Result, Linha, '05088', ['NAURU']);
  AddLin(Result, Linha, '05118', ['CHRISTMAS,ILHA (NAVIDAD)']);
  AddLin(Result, Linha, '05177', ['NEPAL']);
  AddLin(Result, Linha, '05215', ['NICARAGUA']);
  AddLin(Result, Linha, '05258', ['NIGER']);
  AddLin(Result, Linha, '05282', ['NIGERIA']);
  AddLin(Result, Linha, '05312', ['NIUE,ILHA']);
  AddLin(Result, Linha, '05355', ['NORFOLK,ILHA']);
  AddLin(Result, Linha, '05380', ['NORUEGA']);
  AddLin(Result, Linha, '05428', ['NOVA CALEDONIA']);
  AddLin(Result, Linha, '05452', ['PAPUA NOVA GUINE']);
  AddLin(Result, Linha, '05487', ['NOVA ZELANDIA']);
  AddLin(Result, Linha, '05517', ['VANUATU']);
  AddLin(Result, Linha, '05568', ['OMA']);
  AddLin(Result, Linha, '05665', ['PACIFICO,ILHAS DO (POSSESSAO DOS EUA)']);
  AddLin(Result, Linha, '05738', ['PAISES BAIXOS (HOLANDA)']);
  AddLin(Result, Linha, '05754', ['PALAU']);
  AddLin(Result, Linha, '05762', ['PAQUISTAO']);
  AddLin(Result, Linha, '05800', ['PANAMA']);
  AddLin(Result, Linha, '05860', ['PARAGUAI']);
  AddLin(Result, Linha, '05894', ['PERU']);
  AddLin(Result, Linha, '05932', ['PITCAIRN,ILHA']);
  AddLin(Result, Linha, '05991', ['POLINESIA FRANCESA']);
  AddLin(Result, Linha, '06033', ['POLONIA, REPUBLICA DA']);
  AddLin(Result, Linha, '06076', ['PORTUGAL']);
  AddLin(Result, Linha, '06114', ['PORTO RICO']);
  AddLin(Result, Linha, '06238', ['QUENIA']);
  AddLin(Result, Linha, '06254', ['QUIRGUIZ, REPUBLICA']);
  AddLin(Result, Linha, '06289', ['REINO UNIDO']);
  AddLin(Result, Linha, '06408', ['REPUBLICA CENTRO-AFRICANA']);
  AddLin(Result, Linha, '06475', ['REPUBLICA DOMINICANA']);
  AddLin(Result, Linha, '06602', ['REUNIAO, ILHA']);
  AddLin(Result, Linha, '06653', ['ZIMBABUE']);
  AddLin(Result, Linha, '06700', ['ROMENIA']);
  AddLin(Result, Linha, '06750', ['RUANDA']);
  AddLin(Result, Linha, '06769', ['RUSSIA, FEDERACAO DA']);
  AddLin(Result, Linha, '06777', ['SALOMAO, ILHAS']);
  AddLin(Result, Linha, '06858', ['SAARA OCIDENTAL']);
  AddLin(Result, Linha, '06874', ['EL SALVADOR']);
  AddLin(Result, Linha, '06904', ['SAMOA']);
  AddLin(Result, Linha, '06912', ['SAMOA AMERICANA']);
  AddLin(Result, Linha, '06955', ['SAO CRISTOVAO E NEVES,ILHAS']);
  AddLin(Result, Linha, '06971', ['SAN MARINO']);
  AddLin(Result, Linha, '07005', ['SAO PEDRO E MIQUELON']);
  AddLin(Result, Linha, '07056', ['SAO VICENTE E GRANADINAS']);
  AddLin(Result, Linha, '07102', ['SANTA HELENA']);
  AddLin(Result, Linha, '07153', ['SANTA LUCIA']);
  AddLin(Result, Linha, '07200', ['SAO TOME E PRINCIPE, ILHAS']);
  AddLin(Result, Linha, '07285', ['SENEGAL']);
  AddLin(Result, Linha, '07315', ['SEYCHELLES']);
  AddLin(Result, Linha, '07358', ['SERRA LEOA']);
  AddLin(Result, Linha, '07370', ['SERVIA']);
  AddLin(Result, Linha, '07412', ['CINGAPURA']);
  AddLin(Result, Linha, '07447', ['SIRIA, REPUBLICA ARABE DA']);
  AddLin(Result, Linha, '07480', ['SOMALIA']);
  AddLin(Result, Linha, '07501', ['SRI LANKA']);
  AddLin(Result, Linha, '07544', ['SUAZILANDIA']);
  AddLin(Result, Linha, '07560', ['AFRICA DO SUL']);
  AddLin(Result, Linha, '07595', ['SUDAO']);
  AddLin(Result, Linha, '07641', ['SUECIA']);
  AddLin(Result, Linha, '07676', ['SUICA']);
  AddLin(Result, Linha, '07706', ['SURINAME']);
  AddLin(Result, Linha, '07722', ['TADJIQUISTAO, REPUBLICA DO']);
  AddLin(Result, Linha, '07765', ['TAILANDIA']);
  AddLin(Result, Linha, '07803', ['TANZANIA, REP.UNIDA DA']);
  AddLin(Result, Linha, '07820', ['TERRITORIO BRIT.OC.INDICO']);
  AddLin(Result, Linha, '07838', ['DJIBUTI']);
  AddLin(Result, Linha, '07889', ['CHADE']);
  AddLin(Result, Linha, '07919', ['TCHECA, REPUBLICA']);
  AddLin(Result, Linha, '07951', ['TIMOR LESTE']);
  AddLin(Result, Linha, '08001', ['TOGO']);
  AddLin(Result, Linha, '08052', ['TOQUELAU,ILHAS']);
  AddLin(Result, Linha, '08109', ['TONGA']);
  AddLin(Result, Linha, '08150', ['TRINIDAD E TOBAGO']);
  AddLin(Result, Linha, '08206', ['TUNISIA']);
  AddLin(Result, Linha, '08230', ['TURCAS E CAICOS,ILHAS']);
  AddLin(Result, Linha, '08249', ['TURCOMENISTAO, REPUBLICA DO']);
  AddLin(Result, Linha, '08273', ['TURQUIA']);
  AddLin(Result, Linha, '08281', ['TUVALU']);
  AddLin(Result, Linha, '08311', ['UCRANIA']);
  AddLin(Result, Linha, '08338', ['UGANDA']);
  AddLin(Result, Linha, '08451', ['URUGUAI']);
  AddLin(Result, Linha, '08478', ['UZBEQUISTAO, REPUBLICA DO']);
  AddLin(Result, Linha, '08486', ['VATICANO, EST.DA CIDADE DO']);
  AddLin(Result, Linha, '08508', ['VENEZUELA']);
  AddLin(Result, Linha, '08583', ['VIETNA']);
  AddLin(Result, Linha, '08630', ['VIRGENS,ILHAS (BRITANICAS)']);
  AddLin(Result, Linha, '08664', ['VIRGENS,ILHAS (E.U.A.)']);
  AddLin(Result, Linha, '08702', ['FIJI']);
  AddLin(Result, Linha, '08737', ['WAKE, ILHA']);
  AddLin(Result, Linha, '08885', ['CONGO, REPUBLICA DEMOCRATICA DO']);
  AddLin(Result, Linha, '08907', ['ZAMBIA']);
end;
}

function TGeral.ConsertaTexto_Novo(const Texto: String; var Seq: Integer): String;
{
Letra	ID	Code	Result
À	195	128	Ã€
Á	195	129	Ã
Â	195	130	Ã‚
Ã	195	131	Ãƒ
Ç	195	135	Ã‡
È	195	136	Ãˆ
É	195	137	Ã‰
Ê	195	138	ÃŠ
Ì	195	140	ÃŒ
Í	195	141	Ã
Î	195	142	ÃŽ
Ñ	195	145	Ã‘
Ò	195	146	Ã’
Ó	195	147	Ã“
Ô	195	148	Ã”
Õ	195	149	Ã•
Ù	195	153	Ã™
Ú	195	154	Ãš
Û	195	155	Ã›
à	195	160	Ã 
á	195	161	Ã¡
â	195	162	Ã¢
ã	195	163	Ã£
ç	195	167	Ã§
è	195	168	Ã¨
é	195	169	Ã©
ê	195	170	Ãª
ì	195	172	Ã¬
í	195	173	Ã­
î	195	174	Ã®
ñ	195	177	Ã±
ò	195	178	Ã²
ó	195	179	Ã³
ô	195	180	Ã´
õ	195	181	Ãµ
ù	195	185	Ã¹
ú	195	186	Ãº
û	195	187	Ã»
}

{
const
  _CAO_ = #195#135#065#079; CAO = 'ÇÃO';
  _COE_ = #195#135#079#069; COE = 'ÇÕE';
  _CA__ = #195#135#195#131; CA  = 'ÇÃ';
}
var
  I, O, T, E: Integer;
  Novo, X: String;
  Mudou: Boolean;
begin
  Mudou := False;
  Result := Texto;
  Novo   := '';
  T := Length(Texto);
  for i := 1 to T do
  begin
    if X <> '' then
      X := ''
    else
    begin
      O := Ord(Texto[i]);
      E := Ord(Texto[i+1]);
      if (O = 195) and (E > 121) then
      begin
        Mudou := True;
        case E of
          128: X := 'À';
          129: X := 'Á';
          130: X := 'Â';
          131: X := 'Ã';
          135: X := 'Ç';
          136: X := 'È';
          137: X := 'É';
          138: X := 'Ê';
          140: X := 'Ì';
          141: X := 'Í';
          142: X := 'Î';
          145: X := 'Ñ';
          146: X := 'Ò';
          147: X := 'Ó';
          148: X := 'Ô';
          149: X := 'Õ';
          153: X := 'Ù';
          154: X := 'Ú';
          155: X := 'Û';
          160: X := 'à';
          161: X := 'á';
          162: X := 'â';
          163: X := 'ã';
          167: X := 'ç';
          168: X := 'è';
          169: X := 'é';
          170: X := 'ê';
          172: X := 'ì';
          173: X := 'í';
          174: X := 'î';
          177: X := 'ñ';
          178: X := 'ò';
          179: X := 'ó';
          180: X := 'ô';
          181: X := 'õ';
          185: X := 'ù';
          186: X := 'ú';
          187: X := 'û';
          else
          begin
            X := Texto[i];
            MB_Aviso('Não foi possível converter os ' +
            'caracteres especiais #195 + #' + FormatFloat('000', E) + '!');
          end;
        end;
        Novo := Novo + X;
        Seq := Seq + 1;
      end else Novo := Novo + Texto[I];
    end;
  end;
  if Mudou then
  begin
    //ShowMessage('Antigo: ' + Texto + sLineBreak + 'Novo: ' + Novo);
    Result := Novo;
  end;
end;

function TGeral.SalvaTextoEmArquivo(Arquivo, Texto: WideString; Exclui:
Boolean; FileEncodingType: TFileEncodingType): Boolean;
var
  Arq: TextFile;
begin
  //Result := False;
  try
    if Exclui then if FileExists(Arquivo) then DeleteFile(Arquivo);
    try
      ForceDirectories(ExtractFilePath(Arquivo));
    except
      on E: Exception do
      begin
        Geral.MB_Erro(
        'Possível falta de permissão ao criar diretório! Favor criar manualmente!'
        + sLineBreak + sLineBreak + ExtractFilePath(Arquivo) + sLineBreak + sLineBreak +
        E.Message);
      end;
    end;
    AssignFile(Arq, Arquivo);
    ReWrite(Arq);
    case FileEncodingType of
      //fetUTF_8: Write(Arq, $EF+$BB+$BF);
      fetUTF_8: Write(Arq, #$EF+#$BB+#$BF);
      fetAnsi:  // nada;
      else
      begin
        MB_Aviso(
        '"FileEncodingType" não implementado em "TGeral.SalvaTextoEmArquivo()"' +
        sLineBreak + 'Será usada codificação ANSI!');
      end;
    end;
    WriteLn(Arq, Texto);
    CloseFile(Arq);
    Result := FileExists(Arquivo);
  except
    raise;
  end;
end;

function TGeral.SalvaTextoEmArquivoEAbre(Arquivo, Texto: WideString; Exclui:
Boolean): Boolean;
begin
  //Result := False;
  try
    Result := SalvaTextoEmArquivo(Arquivo, Texto, Exclui);
    Result := Result and AbreArquivo(Arquivo);
  except
    raise;
  end;
end;

procedure TGeral.LarguraAutomaticaGrade(AGrid: TStringGrid; ColIni: Integer = 1);
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  Col, Row, T, L: Integer;
  ColWid: array[0..255] of Integer;
begin
  T := Round(CharWid * 5); // 2012-02-25 de "CharWid * 3" para "CharWid * 5"
  for Col := ColIni to 255 do ColWid[Col] := T;
  for Row := 1 to AGrid.RowCount - 1 do
  begin
    for Col := ColIni to 255 do
    begin
      L := Round((Length(AGrid.Cells[Col, Row]) + 1) * CharWid);
      if L > ColWid[Col] then
        ColWid[Col] := L;
    end;
  end;
  for Col := ColIni to AGrid.ColCount -1 do
    AGrid.ColWidths[Col] := ColWid[Col];
end;

function TGeral.LeArquivoToMemo(Arquivo: String; Memo: TMemo): Boolean;
var
  F: TextFile;
  S: String;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  Memo.Lines.Clear;
  //Texto := '';
  try
    if FileExists(Arquivo) then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      while not Eof(F) do
      begin
        Readln(F, S);
        Memo.Lines.Add(S);
      end;
      //Memo.Text := Texto;
      CloseFile(F);
      Result := True;
     Screen.Cursor := crDefault;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TGeral.LeArquivoToString(const Arquivo: String;
  var TextoDoArquivo: String): Boolean;
var
  F: TextFile;
  S: String;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  TextoDoArquivo := '';
  try
    if FileExists(Arquivo) then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      while not Eof(F) do
      begin
        Readln(F, S);
        TextoDoArquivo := TextoDoArquivo + S + sLineBreak;
      end;
      //Memo.Text := Texto;
      CloseFile(F);
      Result := True;
     Screen.Cursor := crDefault;
    end;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TGeral.CompletaString(Texto, Compl: String; Tamanho: Integer;
 Alinhamento: TAlignment; Corrige: Boolean): String;
var
  Txt: String;
  Conta: Double;
begin
  if Length(Compl) <> 1 then
  begin
    MB_Aviso('String de complemeto com tamanho não suportado!');
    Exit;
  end;
  Txt := texto;
  Conta := 0;
  while Length(Txt) < Tamanho do
  begin
    Conta := Conta + 1;
    case Alinhamento of
      taRightJustify: Txt := Compl + Txt;
      taLeftJustify : Txt := Txt + Compl;
      taCenter      :
        if Conta/2 = int(Conta/2) then Txt := Compl + Txt else Txt := Txt + Compl;
    end;
  end;
  if Corrige and (Length(Txt)>Tamanho) then
  begin
    if Alinhamento = taRightJustify then
      Txt := Copy(Txt, Length(Txt)-Tamanho+1, Tamanho)
    else Txt := Copy(Txt, 1, Tamanho);
  end;
  Result := Txt;
end;

function TGeral.CompletaString2(Texto, Compl: String; Tamanho: Integer;
 Alinhamento: TAlignment; Corrige: Boolean; Complemento: String): String;
var
  Txt: String;
  Conta: Double;
  TamCompl: Integer;
begin
  if Length(Compl) <> 1 then
  begin
    MB_Aviso('String de complemeto com tamanho não suportado!');
    Exit;
  end;
  Txt := texto;
  Conta := 0;
  TamCompl :=  Length(Complemento);
  if Length(Txt)+Length(Complemento) > Tamanho then
  Txt := Copy(Txt, 1, Tamanho-TamCompl);
  while Length(Txt) < Tamanho-TamCompl do
  begin
    Conta := Conta + 1;
    case Alinhamento of
      taRightJustify: Txt := Compl + Txt;
      taLeftJustify : Txt := Txt + Compl;
      taCenter      :
        if Conta/2 = int(Conta/2) then Txt := Compl + Txt else Txt := Txt + Compl;
    end;
  end;
  if Corrige and (Length(Txt)>Tamanho-TamCompl) then
  begin
    if Alinhamento = taRightJustify then
      Txt := Copy(Txt, Length(Txt)-(Tamanho-TamCompl)+1, Tamanho-TamCompl)
    else Txt := Copy(Txt, 1, Tamanho-TamCompl);
  end;
  Result := Txt+Complemento;
end;

procedure TGeral.WriteAppKey(const Key, App: String; const Value: Variant; KeyType:
 TKeyType; xHKEY: HKEY);
var
  r : TRegistry;
  ValTxt, Chave: String;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  //
  case TOSVersion.Architecture of
    arIntelX86: r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
    arIntelX64:
    begin
      if xHKEY = HKEY_LOCAL_MACHINE then
        r.RootKey := HKEY_CURRENT_USER
      else
        r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
    end;
    else
    begin
      MB_Aviso(
      'Não foi possível definir a arquitetura do sistema operacional!' +
      'Será considerado 64 Bits!');
      //
      if xHKEY = HKEY_LOCAL_MACHINE then
        r.RootKey := HKEY_CURRENT_USER
      else
        r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
    end;
  end;
  try
    Chave := 'Software\' + App;
    //if r.OpenKey('Software\'+App(*Application.Title*), True) then
    {
    if r.OpenKey(Chave, True) then
    begin
    }
    r.OpenKey(Chave, True);// then
      try
        case KeyType of
          ktString: r.WriteString(Key, Value);
          ktBoolean: r.WriteBool(Key, Value);
          ktInteger: r.WriteInteger(Key, Value);
          ktCurrency: r.WriteCurrency(Key, Value);
          ktDate: r.WriteDate(Key, Value);
          ktTime: r.WriteTime(Key, Value);
          ktDateTime: r.WriteDateTime(Key, Value);
          ktFloat: r.WriteFloat(Key, Value);
          //ktBinary: Result := r.WriteBinaryData(Key);
        end;
      except
        ValTxt := VariavelToString(Value);
        //
        MB_Erro('Não foi possível configurar o valor "' +
        ValTxt + '" para a chave "' + key +
        '" no registro do windows!. ' + sLineBreak +
        'A causa mais provável é falta de permissão do usuário no sistema operacional. ' +
        sLineBreak +
        'O aplicativo não será executado corretamente. Avise a DERMATEK!' +
        sLineBreak + 'procedure TGeral.WriteAppKey()');
        // Desmarcar at'e ver o que fazer para win 8
        //Application.Terminate;
      end;
    {
    end
    else
    begin
      case xHKey of
        HKEY_CLASSES_ROOT: Nome_HKEY := 'HKEY_CLASSES_ROOT';
        HKEY_CURRENT_USER: Nome_HKEY := 'HKEY_CURRENT_USER';
        HKEY_LOCAL_MACHINE: Nome_HKEY := 'HKEY_LOCAL_MACHINE';
        HKEY_USERS: Nome_HKEY := 'HKEY_USERS';
        HKEY_PERFORMANCE_DATA: Nome_HKEY := 'HKEY_PERFORMANCE_DATA';
        HKEY_CURRENT_CONFIG: Nome_HKEY := 'HKEY_CURRENT_CONFIG';
        HKEY_DYN_DATA: Nome_HKEY := 'HKEY_DYN_DATA';
        else Nome_HKEY := 'HKEY_?????';
      end;
      //
      MB_Erro(
      'Não foi possível abrir a seguinte chave no registro do windows: ' +
      sLineBreak +
      'Raiz: ' + Nome_HKEY + sLineBreak +
      'Chave: ' + Chave + sLineBreak +
      'Campo: ' + Key + sLineBreak);
    end;
    }
  finally
    r.Free;
  end;
end;

procedure TGeral.WriteAppKeyCU(const Key, App: String; const Value: Variant;
  KeyType: TKeyType);
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_CURRENT_USER;
  try
    r.OpenKey('Software\'+App(*Application.Title*), True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
      ktDateTime: r.WriteDateTime(Key, Value);
      ktFloat: r.WriteFloat(Key, Value);
      //ktBinary: Result := r.WriteBinaryData(Key);
    end;
  finally
    r.Free;
  end;
end;

function TGeral.WriteAppKeyLM_Net(const Server, Key, App: String; const Value: Variant;
  KeyType: TKeyType): Boolean;
var
  r : TRegistry;
  n: String;
  k: Integer;
begin
  Result := False;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_LOCAL_MACHINE;
  n := Server;
  while n[1] = '\' do
    n := Copy(n, 2);
  k := pos('\', n);
  if k > 0 then
    n := Copy(n, 1, k-1);
  n := '\\' + n;
  //
  if r.RegistryConnect(n) then // '\\Servidor'
  begin
    Result := True;
    if r.OpenKey('Software\'+App(*Application.Title*), True) then
    begin
      try
        case KeyType of
          ktString: r.WriteString(Key, Value);
          ktBoolean: r.WriteBool(Key, Value);
          ktInteger: r.WriteInteger(Key, Value);
          ktCurrency: r.WriteCurrency(Key, Value);
          ktDate: r.WriteDate(Key, Value);
          ktTime: r.WriteTime(Key, Value);
          ktDateTime: r.WriteDateTime(Key, Value);
          ktFloat: r.WriteFloat(Key, Value);
          //ktBinary: Result := r.WriteBinaryData(Key);
        end;
      except
       Geral.MB_Erro(
       'Não foi possível escrever um registro do windows no servidor "' + n + '"');
      end;
    end else Geral.MB_Erro(
    'Não foi possível acessar uma chave do registro do windows no servidor "' + n + '"');
  end else Geral.MB_Erro(
  'Não foi possível acessar o registro do windows no servidor "' + n + '"');
end;

procedure TGeral.WriteAppKeyMULTI_SZ(const Key, App: String; const Value: Variant);
var
  r: TRegistry;
  Res: Integer;
  Str: String;
begin
  r := TRegistry.Create;
  try
    Str := Value;
    r.RootKey := HKEY_CURRENT_USER;
    if not r.OpenKey('\Software\' + App, True) then
      raise Exception.Create('Não foi possível abrir chave!');
    Res := RegSetValueEx(
      r.CurrentKey,     
      PWideChar(Key),
      0,
      REG_MULTI_SZ,
      PChar(Str),
      Length(Str) + 1); 
    if Res <> ERROR_SUCCESS then
      ShowMessage('Falha ao atualizar chave!');
  finally
    r.Free;
  end;
end;

function TGeral.ReadAppKeyMULTI_SZ(const Key, AppTitle: String;
  DefValue: Variant): Variant;
var
  r: TRegistry;
  DataType: Cardinal;
  DataSize: Cardinal;
  Res: Integer;
  Str: String;
begin
  r := TRegistry.Create;
  try
    r.RootKey := HKEY_CURRENT_USER;
    if not r.OpenKeyReadOnly('\Software\' + AppTitle) then
    begin
      Result := DefValue;
      Exit;
    end;
    DataSize := 0;
    {
    Res := RegQueryValueEx(
      r.CurrentKey,
      PAnsiChar(Key),
      nil,
      @DataType,
      nil,
      @DataSize);
    if Res <> ERROR_SUCCESS then
      MB_Aviso('Não foi possível escrever um registro do windows!',
        'Erro', MB_OK+MB_ICONERROR);
    if DataType <> REG_MULTI_SZ then
      raise Exception.Create('Wrong data type');
    }
    SetLength(Str, DataSize - 1);
    if DataSize > 1 then
    begin
      Res := RegQueryValueEx(
        r.CurrentKey,   
        PWideChar(Key),
        nil,
        @DataType,
        PByte(Str),
        @DataSize);     
      if Res <> ERROR_SUCCESS then
        Geral.MB_Erro('Não foi possível escrever um registro do Windows!');
    end;
    if Str <> '' then
      Result := Str
    else
      Result := DefValue;
  finally
    r.Free;
  end;
end;

procedure TGeral.WriteAppKey_Total(const Key, App: String; const Value: Variant;
  KeyType: TKeyType; xHKEY: HKEY);
var
  r : TRegistry;
begin
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := xHKEY; //HKEY_LOCAL_MACHINE;
  try
    r.OpenKey(App, True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
      ktDateTime: r.WriteDateTime(Key, Value);
      ktFloat: r.WriteFloat(Key, Value);
      //ktBinary: Result := r.WriteBinaryData(Key);
    end;
  finally
    r.Free;
  end;
end;

procedure TGeral.WriteAppKeyLM(const Key, App: String; const Value: Variant;
  KeyType: TKeyType);
var
  r : TRegistry;
begin
  // ini 2022-03-01
  WriteAppKeyLM2(Key, App, Value, KeyType);
  // fim 2022-03-01
  if VAR_NAO_USA_KEY_LOCAL_MACHINE then
  begin
    WriteAppKeyCU(Key, App, Value, KeyType);
    Exit;
  end;
  r := TRegistry.Create(KEY_ALL_ACCESS);
  r.RootKey := HKEY_LOCAL_MACHINE;
  try
    r.OpenKey('Software\'+App(*Application.Title*), True);
    case KeyType of
      ktString: r.WriteString(Key, Value);
      ktBoolean: r.WriteBool(Key, Value);
      ktInteger: r.WriteInteger(Key, Value);
      ktCurrency: r.WriteCurrency(Key, Value);
      ktDate: r.WriteDate(Key, Value);
      ktTime: r.WriteTime(Key, Value);
      ktDateTime: r.WriteDateTime(Key, Value);
      ktFloat: r.WriteFloat(Key, Value);
      //ktBinary: Result := r.WriteBinaryData(Key);
    end;
  finally
    r.Free;
  end;
end;

procedure TGeral.WriteAppKeyLM2(const Key, App: String; const Value: Variant;
  KeyType: TKeyType);
var
  r: TRegistry;
  Local: String;
  Res: Boolean;
begin
  r         := TRegistry.Create(KEY_READ);
  r.RootKey := HKEY_LOCAL_MACHINE;
  try
    Local := 'Software\' + App;
    //
    r.Access := KEY_WRITE;
    Res := r.OpenKey(Local, True);
    //
    if not Res then
    begin
      MB_Erro('Não é possível criar chave!' + sLineBreak +
        'Verifique se você está conectado com um usuário do tipo Adminsitrador!');
      Exit();
    end else
    begin
      case KeyType of
        ktString:
          r.WriteString(Key, Value);
        ktBoolean:
          r.WriteBool(Key, Value);
        ktInteger:
          r.WriteInteger(Key, Value);
        ktCurrency:
          r.WriteCurrency(Key, Value);
        ktDate:
          r.WriteDate(Key, Value);
        ktTime:
          r.WriteTime(Key, Value);
        ktDateTime:
          r.WriteDateTime(Key, Value);
        ktFloat:
          r.WriteFloat(Key, Value);
        //ktBinary:
          //Result := r.WriteBinaryData(Key);
      end;
    end;
  finally
    r.CloseKey;
    r.Free;
  end;
end;

function TGeral.VariavelToString(Variavel: Variant): String;
  function Inteiro(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := IntToStr(Integer(Variavel));
  end;
  function Intei64(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := String(Variavel);
  end;
begin
  // para ver a variavel sem aspas use
  //  M L A G e r a l.VariantToString
  try
    case VarType(Variavel) of
      (*
      varEmpty    {= $0000}: Result := '';
      varNull     {= $0001}: Result := 'Null';
      varSmallint {= $0002}: Result := Inteiro(Variavel);
      varInteger  {= $0003}: Result := Inteiro(Variavel);
      varSingle   {= $0004}: Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005}: Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      varCurrency {= $0006}: Result := FormatFloat('0.000000000000000', Variavel);
      varDate     {= $0007}: Result := '"' + FormatDateTime('yyyy-mm-dd', Variavel) + '"';

      varBoolean  {= $000B}: Result := IntToStr(BoolToInt(Variavel));

      varString   {= $0100}: Result := '"' + Variavel + '"';
      *)
      varEmpty    {= $0000;} { vt_empty        0 } : Result := '';
      varNull     {= $0001;} { vt_null         1 } : Result := 'Null';
      varSmallint {= $0002;} { vt_i2           2 } : Result := Inteiro(Variavel);
      varInteger  {= $0003;} { vt_i4           3 } : Result := Inteiro(Variavel);
      varSingle   {= $0004;} { vt_r4           4 } : Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005;} { vt_r8           5 } : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      varCurrency {= $0006;} { vt_cy           6 } : Result := FormatFloat('0.000000000000000', Variavel);
      //varDate     {= $0007;} { vt_date         7 } : Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', Variavel) + '"';
      varDate     {= $0007;} { vt_date         7 } : Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + '"';
      varOleStr   {= $0008;} { vt_bstr         8 } : Result := '"' + WideStringToSQLString(Variavel) + '"';
      varDispatch {= $0009;} { vt_dispatch     9 } : Result := '"' + Variavel + '"';
      varError    {= $000A;} { vt_error       10 } : Result := '"' + Variavel + '"';
      varBoolean  {= $000B;} { vt_bool        11 } : Result := IntToStr(BoolToInt(Variavel));
      varVariant  {= $000C;} { vt_variant     12 } : Result := '"' + Variavel + '"';
      varUnknown  {= $000D;} { vt_unknown     13 } : Result := '"' + Variavel + '"';
    //varDecimal  {= $000E;} { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
    //varUndef0F  {= $000F;} { undefined      15 } {UNSUPPORTED per Microsoft}
      varShortInt {= $0010;} { vt_i1          16 } : Result := Inteiro(Variavel);
      varByte     {= $0011;} { vt_ui1         17 } : Result := Inteiro(Variavel);
      varWord     {= $0012;} { vt_ui2         18 } : Result := Inteiro(Variavel);
      varLongWord {= $0013;} { vt_ui4         19 } : Result := Inteiro(Variavel);
      varInt64    {= $0014;} { vt_i8          20 } : Result := Intei64(Variavel);
    //varWord64   {= $0015;} { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
    {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

      varStrArg   {= $0048;} { vt_clsid       72 } : Result := '"' + Variavel + '"';
      varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Result := '"' + WideStringToSQLString(Variavel) + '"';
      varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Result := '"' + Variavel + '"';
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
{$IfDef VER310_UP}
      varUString  {= $0102;} { Unicode string 258 } {not OLE compatible }: Result := '"' + WideStringToSQLString(String(Variavel)) + '"';
{$Else}
      varUString  {= $0102;} { Unicode string 258 } {not OLE compatible }: Result := '"' + WideStringToSQLString(Variavel) + '"';
{$EndIf}
{$ENDIF}
      //271 {FMTBcdVariantType = 271}                : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      // custom types range from $110 (272) to $7FF (2047)
      //Tipo de variável: 258 => UnicodeString
      else begin
        if (VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType') then
          Result := FloatToStrF(Variavel, ffFixed, 15, 15)
        else
        if (VarTypeAsText(VarType(Variavel)) = 'SQLTimeStampVariantType') then
          //Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', Variavel) + '"'
          Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + '"'
        else begin
          //raise EAbort.Create('Variável não definida!');
          MB_Erro('Variável não definida!' + sLineBreak +
          'Tipo de variável: ' + IntToStr(VarType(Variavel)) + ' => ' +
          VarTypeAsText(VarType(Variavel)));
          Result := '"' + VariantToString(Variavel) + '"';
        end;
      end;
    end;
    if (VarType(Variavel) in ([varSingle, varDouble, varCurrency]))
    or ((VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType')) then
    begin
        if pos(',', Result) > 0 then Result[pos(',', Result)] := '.';
        // Evitar erro em números muito grandes!
        if Length(Result) > 21 then Result := Copy(Result, 1, 21);
    end;
  except
    Geral.MB_Erro('Erro na function "VariavelToString".' + sLineBreak +
    'Tipo de variável: ' + VarTypeAsText(VarType(Variavel)));
  end;
  (*varOleStr   = $0008;
  varDispatch = $0009;
  varError    = $000A;
  varBoolean  = $000B;
  varVariant  = $000C;
  varUnknown  = $000D;
  varByte     = $0011;

  varStrArg   = $0048;

  varString   = $0100;
  varAny      = $0101;
  varTypeMask = $0FFF;
  varArray    = $2000;

  varByRef    = $4000;

The lower twelve bits of a Variant type code (the bits defined by the varTypeMask bit mask) define the type of the Variant. The varArray bit is set if the Variant is an array of the given type. The varByRef bit is set if the Variant is a reference to a value of the given type as opposed to an actual value.

The following table describes the meaning of each of the Variant type codes.

VarType	Contents of Variant

varEmpty	The Variant is Unassigned.
Null	The Variant is Null.
varSmallint	16-bit signed integer (type Smallint).
varInteger	32-bit signed integer (type Integer).
varSingle	Single-precision floating-point value (type Single).
varDouble	Double-precision floating-point value (type Double).
varCurrency	Currency floating-point value (type Currency).
varDate	Date and time value (type TDateTime).
varOleStr	Reference to a dynamically allocated UNICODE string.

varDispatch	Reference to an Automation object (an IDispatch interface pointer).
varError	Operating system error code.
varBoolean	16-bit boolean (type WordBool).
varVariant	A Variant.
varUnknown	Reference to an unknown OLE object (an IUnknown interface pointer).
varByte	A Byte
varStrArg	COM-compatible string.
varString	Reference to a dynamically allocated string. (not COM compatible)
varAny	A CORBA Any value. *)
end;

function TGeral.BoolToStr(Verdade: Boolean): String;
begin
  if Verdade then
    Result := 'V'
  else
    Result := 'F';
end;

function TGeral.BoolToInt(Verdade: Boolean): Integer;
begin
  if Verdade then Result := 1 else Result := 0;
end;

function TGeral.BoolToNumStr(Verdade: Boolean): String;
begin
  if Verdade then Result := '1' else Result := '0';
end;

function TGeral.StrToBool(Verdade: String): Boolean;
begin
  if UpperCase(Verdade) = 'F' then
    Result := False
  else
    Result := True;
end;

function TGeral.IntToBool(Verdade: Integer): Boolean;
begin
  if Verdade = 0 then Result := False else Result := True;
end;

function TGeral.IntToBool_0(Numero: Integer): Boolean;
begin
  if Numero = 0 then
    Result := False
  else
    Result := True;
end;

function TGeral.IsInterleavedText(const Str: String; var Res: Boolean): TStringList;
const
  Separador = ',';
var
  I, j: Integer;
  Erro: Boolean;
  Key: Char;
begin
  Res := False;
  Erro := False;
  Result := TStringList.Create;
  Result := Geral.Explode(Str, Separador);
  for I := 0 to Result.Count - 1 do
  begin
    for J  := Low(Result[I]) to High(Result[I]) do
    begin
      Key := Result[I][J];
      if not (CharInSet(Key, ['0'..'9', ',', '-', '>', ' '])) then
      begin
        Erro := True;
        Break;
      end;
    end;
  end;
  Res := Erro = False;
end;

function TGeral.JsonBool(Verdade: Boolean): String;
begin
  Result := Geral.BoolToNumStr(Verdade);
end;

function TGeral.JsonText(Texto: String): String;
begin
  Result := StringReplace(Texto, '\', '\\',  [rfReplaceAll]);
end;

function  TGeral.SemAcento(Txt : string) : string;
var
  Ch: Char;
  i: Integer;
  Texto: String;
begin
  if trim(Txt) = '' then Result := Txt else
  begin
    Texto := '';
    for i := 1 to Length(Txt) do
    begin
      Ch := Txt[i];
      if Pos(Ch,TextString_Com) <> 0 then
        Texto:= Texto + TextString_Sem[(Pos(Ch, TextString_Com))]
      else Texto :=  Texto + Txt[i];
    end;
    Result := Texto;
  end;
end;

function  TGeral.Maiusculas(Txt : string; DefUser: Boolean) : string;
begin
  Result := '';
  if DefUser and (VAR_SOMAIUSCULAS = False) then Result := Txt
  else Result := AnsiUppercase(Txt);
  {else begin
    Texto := UpperCase(Txt);
    for i := 1 to Length(Texto) do
    begin
      Ch := Texto[i];
      if Pos(Ch,TextStringRowA) <> 0 then
        Result:= Result + TextStringRowB[(Pos(Ch, TextStringRowA))]
      else Result :=  Result + Texto[i];
    end;
  end;
  }
end;

function  TGeral.Minusculas(Txt : string; DefUser: Boolean) : string;
begin
  Result := '';
  Result := AnsiLowerCase(Txt);
  {for i := 1 to Length(Texto) do
  begin
    Ch := Texto[i];
    if Pos(Ch,TextStringRowB) <> 0 then
      Result:= Result + TextStringRowA[(Pos(Ch, TextStringRowB))]
    else Result :=  Result + Texto[i];
  end;
  }
end;

function TGeral.EhMinusculas(Texto: string; Forca: Boolean): Boolean;
var
  i, n, a, z: Integer;
begin
  Result := False;
  if (Forca = False) and (Length(Texto) > 1) then
  begin
    a := Ord('a');
    z := Ord('z');
    for i := 1 to Length(Texto) do
    begin
      n := Ord(Texto[i]);
      if (n > a) and (n < z) then
      begin
       Result := True;
       Break;
      end;
    end;
  end;
end;

function TGeral.EncodeDateSafe(Ano, Mes, Dia: Word;
  DateEncodeType: TDateEncodeType): TDateTime;
var
  M, D, X, Y, Z: Word;
begin
  Y := Ano;
  M := Mes;
  D := Dia;
  while M > 12 do
  begin
    M := M - 12;
    Y := Y + 1;
  end;
  Result := EncodeDate(Y, M, 1);
  Result := Result + D - 1;
  if DateEncodeType <> detJustSum then
  begin
    DecodeDate(Result, X, Y, Z);
    if Y > M then
    begin
      while Y > M do
      begin
        Result := Result - 1;
        DecodeDate(Result, X, Y, Z);
      end;
      if DateEncodeType = detFirstDayNextMonth then
        Result := Result + 1;
    end;
  end;
end;

procedure TGeral.ErroVersaoDoCompilador(ProcMessage, E_Message: WideString);
begin
  MB_Erro(ProcMessage + sLineBreak + 'Código de erro: COMPILER_VERXXX' +
  sLineBreak + E_Message);
end;

function TGeral.FileSize(fileName : wideString) : Int64;
var
  sr : TSearchRec;
begin
{$WARN SYMBOL_PLATFORM OFF}
  if FindFirst(fileName, faAnyFile, sr ) = 0 then
    result := Int64(sr.FindData.nFileSizeHigh) shl Int64(32) + Int64(sr.FindData.nFileSizeLow)
  else
    result := -1;
  //
  FindClose(sr) ;
{$WARN SYMBOL_PLATFORM ON}
end;

{
procedure TForm1.FormCreate(Sender: TObject) ;
 begin

   AddFontResource('c:\FONTS\MyFont.TTF') ;
   SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0) ;
 end;
 
 //Before application terminates we must remove our font:
 procedure TForm1.FormDestroy(Sender: TObject; var Action: TCloseAction) ;
 begin
   RemoveFontResource('C:\FONTS\MyFont.TTF') ;
   SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0) ;


////////////////////////////////////////////////////


procedure TForm1.FormCreate(Sender: TObject);
begin
  Memo1.Font.Handle := GetStockObject(OEM_FIXED_FONT);
end;

procedure TForm1.Memo1KeyPress(Sender: TObject; var Key: Char);
var
  ch: string[1];
begin
  CharToOem(@Key, @ch[1]);
  Key := ch[1];
end;
}

end.
