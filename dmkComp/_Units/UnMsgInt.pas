{  Opera��es: ...Operacao.Value ou (G�nero.Value (-)):


   1  = Transfer�ncias
   2  = Empr�stimos
   3  = D�vidas
   4  = Consigna��es
   5  = Outras carteiras (Outra carteira em Contas)
   6  = Varios (em Import)
   7  = Empresas(Ajuda de custos de Empresas)
   8  = Pagto Fatura
   9  = Pagto Duplicata
   10 = Tranf. Cxa Geral
   11 = Saldo inicial
   12 =
   13 = Duplic. Tranp.
   14 = Duplic. PQ

   Centros: ...Centro.Value:

   1 = Caixas
   2 = Contas
   3 = Emissoes

   ImportItsTipo.Value:

   1 = OK
   2 = Emitido
   3 = Fatura
   4 = Direto
   5 = Consigna
   6 = Empr�stimos
   7 = D�vida
   8 = Emitir

   Estados Brasileiros

   Norte:

   01 - AC - Acre
   02 - AM - Amazonas
   03 - PA - Par�
   04 - AP - Amap�
   05 - RO - Rond�nia
   06 - RR - Roraima

   Nordeste:

   07 - MA - Maranh�o
   08 - PI - Piau�
   09 - CE - Cear�
   10 - RN - Rio Grande do Norte
   11 - PB - Para�ba
   12 - PE - Pernambuca
   13 - AL - Alagoas
   14 - SE - Sergipe
   15 - BA - Bahia

   Centro-Oeste

   16 - MT - Mato Grosso
   17 - MT - Mato Grosso do Sul
   18 - GO - Goias
   19 - TO - Tocantins
   20 - DF - Distrito Federal

   Sudeste

   21 - MG - Minas Gerais
   22 - ES - Espirito Santo
   23 - RJ - Rio de Janeiro
   24 - SP - S�o Paulo

   Sul

   25 - PR - Paran�
   26 - SC - Santa Catarina
   27 - RS - Rio Grande do Sul

}



unit UnMsgInt;
// Unit Internacionaliza��o da aplica��o :  Mensagens;

interface

const
  //TbImportSit.Value :=
  CO_DIRETO = 'DIRETO';
  CO_DUPLIC = 'DUPLIC.';
  CO_DUPLIC2 = 'DUPL???';
  CO_OK = 'OK';
  CO_ERRO = 'ERRO';
  CO_EMITIR = 'EMITIR';
  CO_EMITIDO = 'EMITIDO';
  CO_DIVIDA = 'DIVIDA';
  CO_EMPRESTIMO = 'EMPREST';
  CO_FATURA = 'FATURA';
  CO_CONSIGNA = 'CONSIGN';
  // Fim TbImportSit.Value
  CO_SIM = 'Sim';
  CO_NAO = 'N�o';
  CO_DESCONHECIDO = '(Desconhecido)';
  // Tipos de carteiras:
  CO_CAIXA = 'Caixa';//0
  CO_BANCO = 'Banco';//1
  CO_EMISS = 'Emiss�o';//2
  CO_EMISS_PAGAR  = 'Emiss�o a Pagar';
  CO_EMISS_AMBOS  = 'Emiss�o a Pagar e Receber';
  CO_EMISS_RECEB  = 'Emiss�o a Receber';

  FIN_APPNAME = 'Finan�as';

  FIN_A = ' a ';
  FIN_TRACO = ' - ';
  FIN_INCLUINDO = 'Inclus�o';
  FIN_EDITANDO = 'Altera��o';
  FIN_TRAVADO = 'Travado';

  FIN_JANEIRO = 'Janeiro';
  FIN_FEVEREIRO = 'Fevereiro';
  FIN_MARCO = 'Mar�o';
  FIN_ABRIL = 'Abril';
  FIN_MAIO = 'Maio';
  FIN_JUNHO = 'Junho';
  FIN_JULHO = 'Julho';
  FIN_AGOSTO = 'Agosto';
  FIN_SETEMBRO = 'Setembro';
  FIN_OUTUBRO = 'Outubro';
  FIN_NOVEMBRO = 'Novembro';
  FIN_DEZEMBRO = 'Dezembro';

  FIN_TIT_CONTAS = FIN_APPNAME+FIN_TRACO+'Cadastro de Contas';
  FIN_TIT_CARTEIRAS = FIN_APPNAME+FIN_TRACO+'Cadastro de Caixas';
  FIN_TIT_EMISSOES = FIN_APPNAME+FIN_TRACO+'Cadastro de Emiss�es de Contas';
  FIN_TIT_TIPOS = FIN_APPNAME+FIN_TRACO+'Configura��o de Contas';
  FIN_TIT_MOVIMENTOS = FIN_APPNAME+FIN_TRACO+'Movimento da conta ';
  FIN_TIT_EMPRESTIMOS = FIN_APPNAME+FIN_TRACO+'Controle de Empr�stimos';
  FIN_TIT_CONTATOS = FIN_APPNAME+FIN_TRACO+'Cadastro de Contatos';
  FIN_TIT_GENEROS = FIN_APPNAME+FIN_TRACO+'Cadastro dos Meus G�neros';
  FIN_TIT_GRUPOS = FIN_APPNAME+FIN_TRACO+'Cadastro de Grupos de G�neros';
  FIN_TIT_OPCOES = FIN_APPNAME+FIN_TRACO+'Op��es';
  FIN_TIT_INFLACAO = FIN_APPNAME+FIN_TRACO+'Cadastro de �ndices de Infla��o';
  FIN_TIT_ = FIN_APPNAME+FIN_TRACO+'';


  FIN_TIT_CA_CONSIGNACOES = 'Consigna��es';
  FIN_TIT_CA_ = '';

  FIN_BT_HI_ANTERIOR = 'Vai para o registro anterior';
  FIN_BT_HI_ALTERA = 'Altera o registro atual';
  FIN_BT_HI_CONFIRMA = 'Confirma a inser��o/altera��o';
  FIN_BT_HI_DESISTE = 'Desiste da inser��o/altera��o';
  FIN_BT_HI_EXCLUI = 'Exclui o registro atual';
  FIN_BT_HI_INCLUI = 'Inclui um novo registro';
  FIN_BT_HI_INICIO = 'Vai para o primeiro registro';
  FIN_BT_HI_PROXIMO = 'Vai para o pr�ximo registro';
  FIN_BT_HI_SAIR = 'Sai da janela atual';
  FIN_BT_HI_ULTIMO = 'Vai para o �ltimo registro';
  FIN_BT_HI_CRIAR = 'Exibe janela de cadastros';
  FIN_BT_HI_DESISTEOPERACAO = 'Desiste da opera��o';
  FIN_BT_HI_ = '';

  FIN_BT_CA_ANTERIOR = '';
  //begin Alt+
  FIN_BT_CA_ALTERA = '&Altera';
  FIN_BT_CA_CONFIRMA = '&Confirma';
  FIN_BT_CA_DESISTE = '&Desiste';
  FIN_BT_CA_EXCLUI = '&Exclui';
  FIN_BT_CA_INCLUI = '&Inclui';
  FIN_BT_CA_INICIO = '';
  FIN_BT_CA_PROXIMO = '';
  FIN_BT_CA_SAIR = '';
  FIN_BT_CA_ULTIMO = '';
  FIN_BT_CA_CALCULA = 'Ca&lcular';
  //end Alt+

  //begin Alt+
  FIN_BT_CA_IMPORTA = '&Importa';
  //end Alt+

  //begin Alt+
  FIN_BT_CA_OK = '&OK';
//FIN_BT_CA_DESISTE = '&Desiste';
  //end Alt+

  //begin Alt+
  FIN_SB_CA_DE = '&do...';
  FIN_SB_CA_PARA = '&para o...';
  FIN_SB_CA_CARTEIRA = '&Caixa';
  FIN_SB_CA_CONTA = 'C&onta';
  FIN_SB_CA_EMISSAO = '&Emiss�o';
  FIN_SB_CA_ = '';
  //end Alt+

  FIN_LA_CA_DEBITO = 'D�bito:';
  FIN_LA_CA_CREDITO = 'Cr�dito:';
  FIN_LA_CA_VENCIMENTO = 'Vencimento:';
  FIN_LA_CA_CODIGO = 'C�digo:';
  FIN_LA_CA_DESCRICAO = 'Descri��o:';
  FIN_LA_CA_NOME = 'Nome:';
  FIN_LA_CA_TIPO = 'Tipo:';
  FIN_LA_CA_TITULOTIPO = 'Itens a serem visualizados:';
  FIN_LA_CA_CARTEIRA = 'Caixa:';
  FIN_LA_CA_CONTA = 'Conta:';
  FIN_LA_CA_EMISSAO = 'Emiss�o:';
  FIN_LA_CA_CAIXA = 'Caixa:';
  FIN_LA_CA_SALDO = 'Saldo:';
  FIN_LA_CA_DIFERENCA = 'Diferen�a:';
  FIN_LA_CA_TITULAR = 'Titular:';
  FIN_LA_CA_VALOR = 'Valor:';
  FIN_LA_CA_DATA = 'Data:';
  FIN_LA_CA_DATAINI = '1� parcela:';
  FIN_LA_CA_TRANSFERIR = 'Transferir...';
  FIN_LA_CA_CENTRO = 'centro:';
  FIN_LA_CA_PARCELA = 'Parcela:';
  FIN_LA_CA_PARCELAS = 'Parcelas:';
  FIN_LA_CA_JUROSMES = 'Juros (m�s):';
  FIN_LA_CA_VENCIMENTOS = 'Vencimentos:';
  FIN_LA_CA_PAGO = 'Pago:';
  FIN_LA_CA_PAGOJURO = 'Juro pago:';
  FIN_LA_CA_PAGOMULTAS = 'Multas:';
  FIN_LA_CA_DEVIDO = 'Devido:';
  FIN_LA_CA_ATRASADO = 'Atrasado:';
  FIN_LA_CA_ATUALIZADO = 'Atualizado:';
  FIN_LA_CA_GRUPO = 'Grupo:';
  FIN_LA_CA_LANCAMENTO = 'Lan�amento:';
  FIN_LA_CA_JUROS = 'Juros:';
  FIN_LA_CA_EXTRAS = 'Extras:';
  FIN_LA_CA_EMPRESTIMOS = 'Empr�stimos:';
  FIN_LA_CA_EMPRESTIMO = 'Empr�stimo:';
  FIN_LA_CA_DIVIDAS = 'D�vidas:';
  FIN_LA_CA_DIVIDA = 'D�vida:';
  FIN_LA_CA_CONSIGNACOES = 'Consigna��es:';
  FIN_LA_CA_CONSIGNACAO = 'Consigna��o:';
  FIN_LA_CA_INDICE = '�ndice:';
  FIN_LA_CA_CAMINHO = 'Caminho:';
  FIN_LA_CA_DATAINICIO = 'Data inicial:';
  FIN_LA_CA_DATAFIM = 'Data final:';
  FIN_LA_CA_ID = 'Identificador:';
  FIN_LA_CA_VALORPARCELA = 'Presta��o:';
  FIN_LA_CA_VALORDEBAIXA = 'Valor da Baixa:';
  FIN_LA_CA_GENERO = 'G�nero:';
  FIN_LA_CA_PRINCIPAL = 'Principal:';
  FIN_LA_CA_MULTA = 'Multa:';
  FIN_LA_CA_CONFERE = 'Confer�ncia:';
  FIN_LA_CA_RUA = 'Rua:';
  FIN_LA_CA_COMPL = 'Complemento:';
  FIN_LA_CA_CIDADE = 'Cidade';
  FIN_LA_CA_CEP = 'CEP:';
  FIN_LA_CA_UF = 'UF:';
  FIN_LA_CA_PAIS = 'Pa�s:';
  FIN_LA_CA_NATAL = 'Nascimento:';
  FIN_LA_CA_FONE = 'Telefone:';
  FIN_LA_CA_FAX = 'Fax:';
  FIN_LA_CA_EMAIL = 'E-mail:';
  FIN_LA_CA_CONTATO = 'Contato:';
  FIN_LA_CA_TOTAl = 'Total:';
  FIN_LA_CA_SUBTOTAL = 'Sub-total:';
  FIN_LA_CA_PERIODO = 'Per�odo:';
  FIN_LA_CA_VEICULO = 'Ve�culo:';
  FIN_LA_CA_PLACA = 'Placa:';
  FIN_LA_CA_ = ':';

  FIN_LA_CA_TITULO1 = 'M�DIA DE CONSUMO DE VE�CULO';
  FIN_LA_CA_TITULO2 = 'USO PARTICLULAR DE VE�CULO';
  FIN_LA_CA_DATA2 = 'Data';
  FIN_LA_CA_SELECTEMP = 'Selecione o empr�stimo';
  FIN_LA_CA_SELECTDIV = 'Selecione a d�vida';
  FIN_LA_CA_FATURA = 'Fatura';
  FIN_LA_CA_DEVEDOR = 'Devedor';
  FIN_LA_CA_CREDOR = 'Credor';
  //FIN_LA_CA_CALENDARIO = 'Calend�rio';
  FIN_LA_CA_TRANSFERE = 'Transfer�ncia';
  FIN_LA_CA_DIASUTEIS = 'Somente dias �teis.';
  FIN_LA_CA_NOVAFATURA = 'Cria��o de Nova Fatura';
  FIN_LA_CA_IMPORTAFATURA = 'Importar pend�ncias da fatura anterior';
  FIN_LA_CA_EXPORTAPARAFATURA = 'Exporta Para Fatura';
  FIN_LA_CA_BAIXADEEMISSAO = 'Baixa de Emiss�o';
  FIN_LA_CA_IMPORTAR = 'Importa��o de Extrato';
  FIN_LA_CA_LITROS = 'Litros';
  FIN_LA_CA_KM = 'Km';
  FIN_LA_CA_KM2 = 'Kilometragem';
  FIN_LA_CA_DISTANCIA = 'Dist�ncia (km)';
  FIN_LA_CA_TOTALL = 'Total (L)';
  FIN_LA_CA_MEDIA = 'M�dia (km/L)';
  FIN_LA_CA_MPERIODO = 'M�dia no per�odo';
  FIN_LA_CA_TPERIODO = 'Total de uso particular no per�odo';
  FIN_LA_CA_KML = 'km/L';
  FIN_LA_CA_INICIO = 'In�cio (km)';
  FIN_LA_CA_FIM = 'Fim (km)';

  FIN_CB_CA_AUTORIZACAO = 'Autoriza��o';
  FIN_CB_CA_NOTAFISCAL = 'Nota Fiscal';
  FIN_CB_CA_BANCO = 'Banco';
  FIN_CB_CA_LOCAL = 'Local';
  FIN_CB_CA_COMPENSADO = 'Compensado';
  FIN_CB_CA_ = '';

  FIN_TIT_COL_AUTORIZACAO = 'Autoriza��o';
  FIN_TIT_COL_ = '';

  FIN_GR_TI_DATA = 'Data';
  FIN_GR_TI_GENERO = 'G�nero';
  FIN_GR_TI_DESCRICAO = 'Descri��o';
  FIN_GR_TI_NF = 'N� N.F.';
  FIN_GR_TI_VALORNF = 'Valor N.F.';
  FIN_GR_TI_DEBITO = 'D�bito';
  FIN_GR_TI_CREDITO = 'Cr�dito';
  FIN_GR_TI_VENCIM = 'Vencim.';
  FIN_GR_TI_VENCIMENTO = 'Vencimento';
  FIN_GR_TI_PAGAMENTO = 'Pagamento';
  FIN_GR_TI_DOC = 'N� Doc.';
  FIN_GR_TI_COMPENS = 'Baixa';
  FIN_GR_TI_BCO = 'Bco';
  FIN_GR_TI_LOCAL = 'Local';
  FIN_GR_TI_PARCELA = 'Parcela';
  FIN_GR_TI_VALOR = 'Valor';
  FIN_GR_TI_SERVICO = 'Servi�o';
  FIN_GR_TI_ATUALIZADO = 'Atualizado';
  FIN_GR_TI_SALDO = 'Saldo';
  FIN_GR_TI_PRINCIPAL = 'Princ.';
  FIN_GR_TI_PRINCIPAL2 = 'Principal';
  FIN_GR_TI_JUROS = 'Juros';
  FIN_GR_TI_MULTAS = 'Multas';
  FIN_GR_TI_TOTAL = 'Total';
  FIN_GR_TI_FATURA = 'Fatura';
  FIN_GR_TI_SIT = 'Situa��o';
  FIN_GR_TI_MEMO = 'Hist�rico';
  FIN_GR_TI_L = 'Litros';
  FIN_GR_TI_KM = 'km';
  FIN_GR_TI_KML = 'km/L';
  FIN_GR_TI_KMINI = 'km inicial';
  FIN_GR_TI_KMFIM = 'km final';
  FIN_GR_TI_NUMABASTEC = 'Ab.n�';
  FIN_GR_TI_CLIENTES = 'Clientes';
  FIN_GR_TI_CIDADE = 'Cidade';
  FIN_GR_TI_PRODUTO = 'Produto';
  FIN_GR_TI_BOMBA = 'Bomba';
  FIN_GR_TI_ENCERRINI = 'Abertura';
  FIN_GR_TI_ENCERRFIM = 'Encerrante';
  FIN_GR_TI_AFERICAO = 'Aferi��o';
  FIN_GR_TI_QUANTIDADE = 'Quantidade';
  FIN_GR_TI_ICMS = 'ICMS';
  FIN_GR_TI_SUBSTIT = 'Substit.';
  FIN_GR_TI_IPI = 'IPI';
  FIN_GR_TI_OUTROS = 'Outros';
  FIN_GR_TI_PRECO = 'Pre�o';
  FIN_GR_TI_UNIDADE = 'Unidade';
  FIN_GR_TI_DESCONTO = 'Desconto';
  FIN_GR_TI_PAGO = 'Pago';
  FIN_GR_TI_ABERTO = 'Aberto';
  FIN_GR_TI_CARTEIRA = 'Caixa';
  FIN_GR_TI_ENTRADA = 'Entrada';
  FIN_GR_TI_SAIDA = 'Saida';
  FIN_GR_TI_ = '';

  FIN_RG_TI_OPERACAO = ' Opera��o: ';
  FIN_RG_TI_TIPO = ' Tipo: ';
  FIN_RG_TI_DATAPADRAO = ' Data padr�o: ';
  FIN_RG_TI_ = ' : ';

  FIN_RG_IT_TRANSFERENCIA = 'Transfer�ncia entre centros.';
  FIN_RG_IT_EMPRESTIMOS = 'Empr�stimos.';
  FIN_RG_IT_CONSIGNACOES = 'Consigna��es.';
  FIN_RG_IT_PREFIXADO = 'Prefixado';
  FIN_RG_IT_POSFIXADO = 'P�s-fixado';
  FIN_RG_IT_PREFIXADA = 'Prefixada';
  FIN_RG_IT_POSFIXADA = 'P�s-fixada';
  FIN_RG_IT_HOJE = 'Hoje';
  FIN_RG_IT_ULTIMA = '�ltima';
  FIN_RG_IT_ = '';

  //begin Alt+
  FIN_PM_CA_PREFIXADO = '&Prefixado';
  FIN_PM_CA_POSFIXADO = '&P�s-fixado';
  //end Alt+
  
  //begin Alt+
  FIN_PM_CA_PREFIXADA = '&Prefixada';
  FIN_PM_CA_POSFIXADA = '&P�s-fixada';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_TRANSFERENCIAS = '&Tranfer�ncias';
  FIN_PM_CA_EMPRESTIMOS = '&Empr�stimos';
  FIN_PM_CA_DIVIDAS = '&D�vidas';
  FIN_PM_CA_CONSIGNACOES = '&Consigna��es';
  FIN_PM_CA_FATURAS = '&Faturas';
  FIN_PM_CA_EMPRESAS = 'E&mpresas';
  FIN_PM_CA_BAIXA = '&Baixa';
  FIN_PM_CA_SOMALINHAS = '&Soma linhas';
  FIN_PM_CA_EXTRATO = 'E&xtrato';
  FIN_PM_CA_DUPLICATAS = 'D&uplicatas';
  FIN_PM_CA_PATRIMONIO = '&Patrim�nio';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_EXPORTAR = '&Exportar';
  FIN_PM_CA_VISUALIZAR = '&Visualizar';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_NOVO = '&Novo';
  FIN_PM_CA_EDITAR = '&Editar';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_NOVA = '&Nova';
//  FIN_PM_CA_EDITAR = '&Editar';
  FIN_PM_CA_EXISTENTE = 'E&xistente';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_ARQUIVO = '&Arquivo';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_SAIR = '&Sair';
  //end Alt+

  //begin Alt+
  FIN_PM_CA_COMPRA = '&Compra';
  FIN_PM_CA_VENDA = '&Venda';
  //end Alt+

  //begin Alt+
  FIN_MM_CA_ARQUIVO = '&Arquivo';
  FIN_MM_CA_CADASTRO = '&Cadastros';
  FIN_MM_CA_JANELAS = '&Janelas';
  FIN_MM_CA_FERRAMENTAS = '&Ferramentas';
  //end Alt+
  //begin Alt+
  FIN_MM_CA_SAIR = '&Sair';
  //end Alt+
  //begin Alt+
  FIN_MM_CA_CONTAS = '&Contas';
  FIN_MM_CA_GENEROS = '&G�neros';
  FIN_MM_CA_CARTEIRAS = 'C&arteiras';
  FIN_MM_CA_EMISSOES = '&Emiss�es';
  FIN_MM_CA_CONTATOS = '&Contatos';
  //end Alt+
  //begin Alt+
  FIN_MM_CA_MEUSGENEROS = '&Meus G�neros';
  FIN_MM_CA_GRUPOS = '&Grupos';
  //end Alt+
  //begin Alt+
  FIN_MM_CA_OPCOES = '&Op��es';
  FIN_MM_CA_INDICES = '&Infla��o';
  FIN_MM_CA_IMPORTACAO = 'I&mporta��o';
  //end Alt+
  //begin Alt+
  FIN_PM_CA_CADFORNECEDORES = '&Cadastros';
  //end Alt+
  //begin Alt+
  FIN_PM_CA_CADCLIENTES = '&Cadastros';
  //end Alt+
  //begin Alt+
  FIN_PM_CA_CADPQ = '&Cadastros';
  FIN_PM_CA_ENTRAPQ = '&Entradas';
  FIN_PM_CA_PEDPQ = '&Pedidos';
  FIN_PM_CA_ANALOGOS = '&An�logos';
  //end Alt+
  //begin Alt+
  FIN_PM_CA_CADUFS = '&Cadastro';
  //end Alt+
  //begin Alt+
  FIN_PM_CA_CADIMPFEDERAIS = '&Cadastro';
  //end Alt+
  //begin Alt+
  FIN_PM_CA_FORNECEDORES = '&Fornecedores';
  FIN_PM_CA_CLIENTES = '&Clientes';
  FIN_PM_CA_PQ = 'Insumos &Qu�micos';
  FIN_PM_CA_UFS = '&Estados';
  FIN_PM_CA_IMPFEDERAIS = '&Impostos Federais';
  //end Alt+
  //begin Alt+


  FIN_SPLASH_1 = 'Criando janela principal';
  FIN_SPLASH_2 = 'Carregando tabelas';
  FIN_SPLASH_3 = 'Configurando para usar';
  FIN_SPLASH_4 = 'pela primeira vez';
  FIN_SPLASH_5 = '00:00';

  FIN_1_PROVENTOS = 'Proventos tribut�veis';
  FIN_1_OUTRASENTRADAS = 'Outras Entradas';
  FIN_1_ALIMENTACAO = 'Alimenta��o';
  FIN_1_TRANSPORTE = 'Transporte';
  FIN_1_SAUDE = 'Sa�de';
  FIN_1_LAZER = 'Lazer';
  FIN_1_IGREJA = 'Igreja';
  FIN_1_IMPOSTOS = 'Impostos';
  FIN_1_EDUCACAO = 'Educa��o';
  FIN_1_MORADIA = 'Moradia';
  FIN_1_COMUNICACAO = 'Comunica��o';
  FIN_1_INFORMATICA = 'Inform�tica';
  FIN_1_LAR = 'Lar';
  FIN_1_SEGURANCA = 'Seguran�a';
  FIN_1_OUTREM = 'Outrem';
  FIN_1_VESTES = 'Vestes';
  FIN_1_TAXAS = 'Taxas';
  FIN_1_PATRIMONIO = 'Patrim�nio';
  FIN_1_OUTROS = 'Outros';
  FIN_1_EMPRESA = 'Empresa';

  FIN_1_PROVENTOSDE = 'Proventos de...';
  FIN_1_GANHOS = 'Ganhos';
  FIN_1_ACHADOS = 'Achados';
  FIN_1_LUCROS = 'Lucros';
  FIN_1_JUROSFIN = 'Juros de aplica��o financeira';
  FIN_1_JUROSEMP = 'Juros de empr�stimos';
  FIN_1_MULTASEMP = 'Multas de empr�stimos';
  FIN_1_BONUS = 'B�nus de aplica��o financeira';
  FIN_1_POUPANCA = 'Remun. b�sica de poupan�a';
  FIN_1_VENDAS = 'Vendas de objetos';
  FIN_1_OUTROSPROVENTOS = 'Outras entradas';
  FIN_1_ALIM_LANCHES = 'Lanches';
  FIN_1_ALIM_RESTAURANTES = 'Restaurantes';
  FIN_1_ALIM_SALADA = 'Alimenta��o - salada';
  FIN_1_ALIM_BASICO = 'Alimenta��o - b�sico';
  FIN_1_ALIM_CARNES = 'Alimenta��o - carnes';
  FIN_1_ALIM_SOBREMESAS = 'Alimenta��o - sobremesas';
  FIN_1_ALIM_PETISCOS = 'Alimenta��o - petiscos';
  FIN_1_ALIM_FRUTAS = 'Alimenta��o - frutas';
  FIN_1_ALIM_TEMPEROS = 'Alimenta��o - temperos';
  FIN_1_ALIM_GULOSEIMAS = 'Alimenta��o - guloseimas';
  FIN_1_ALIM_REFRIGERANTES = 'Alimenta��o - refrigerantes';
  FIN_1_ALIM_BEBIDAS = 'Alimenta��o - bebidas';
  FIN_1_ALIM_CHAS = 'Alimenta��o - ch�s';
  FIN_1_ALIM_SUCOS = 'Alimenta��o - sucos';
  FIN_1_ALIM_CAFE = 'Alimenta��o - caf�';
  FIN_1_ALIM_HIGIENE = 'Higi�ne';
  FIN_1_ALIM_COSMETICOS = 'Cosm�ticos';
  FIN_1_ALIM_LIMPEZA = 'Limpeza';
  FIN_1_ALIM_OUTROS = 'Alimenta��o - Outros';
  FIN_1_COMBUSTIVEL = 'Combust�vel';
  FIN_1_PASSES = 'Passagens - Passes';
  FIN_1_ESTACIONAMENTO = 'Estacionamento';
  FIN_1_PEDAGIO = 'Ped�gio';
  FIN_1_REMEDIOS = 'Rem�dios';
  FIN_1_PLANODESAUDE = 'Planos de Sa�de';
  FIN_1_CONSULTAMEDICA = 'Consulta m�dica';
  FIN_1_INTERNACAOHOSPITALAR = 'Interna��o hospitalar';
  FIN_1_EXAMEMEDICO = 'Exames m�dicos';
  FIN_1_RECEPCOES = 'Recep��es festivas';
  FIN_1_DIVERSAO = 'Divers�o';
  FIN_1_CLUBES = 'Clubes (mesalidade)';
  FIN_1_CONTRIBUICAOIGREJA = 'Contribui��o para a igreja';
  FIN_1_OFERTAIGREJA = 'Oferta para a igreja';
  FIN_1_CPMF = 'CPMF';
  FIN_1_FEDERAL = 'Impostos federais';
  FIN_1_ESTADUAL = 'Impostos estaduais';
  FIN_1_MUNICIPAL = 'Impostos municipais';
  FIN_1_BANCO = 'Taxas banc�rias';
  FIN_1_CRQ = 'Conselho de classe (Anuidade)';
  FIN_1_ABQTIC = 'Associa��o trabalhista (anuidade)';
  FIN_1_JUROSDIV = 'Juros de d�vidas';
  FIN_1_MULTASDIV = 'Multas de d�vidas';
  FIN_1_ESCOLA = 'Escola (mensalidade)';
  FIN_1_CURSOS = 'Cursos e palestras';
  FIN_1_LIVROS = 'Livros';
  FIN_1_REVISTAS = 'Revistas';
  FIN_1_ALUGUEL = 'Aluguel';
  FIN_1_CONDOMINIO = 'Condom�nio';
  FIN_1_ELETRICA = 'Energia el�trica';
  FIN_1_AGUA = 'Saneamento';
  FIN_1_GAS = 'Combust�vel residencial';
  FIN_1_TELEFONE = 'Telefone';
  FIN_1_INTERNET = 'Acesso � internet';
  FIN_1_CORREIO = 'Correio';
  FIN_1_SOFT = 'Software';
  FIN_1_HARD = 'Hardwere e acess�rios';
  FIN_1_ESCRITORIO = 'Material de escrit�rio';
  FIN_1_COTIDIANO = 'Cotidiano / outros';
  FIN_1_ELETRONICOS = 'Eletro-eletr�nicos';
  FIN_1_AV = '�udio e v�deo';
  FIN_1_FOTO = 'Fotos e similares';
  FIN_1_UTILIDADES = 'Utilidades dom�sticas';
  FIN_1_MANUTENCAO = 'Manuten��o dom�stica';
  FIN_1_DECORATIVOS = 'Decorativos';
  FIN_1_MOVEIS = 'M�veis';
  FIN_1_SEGUROS = 'Seguros';
  FIN_1_PENSAO = 'Pens�es';
  FIN_1_SAZONAL = 'Sazonal (presentes, natal, etc)';
  FIN_1_DOACAO = 'Doa��es';
  FIN_1_GORJETA = 'Gorjetas';
  FIN_1_ROUPAS = 'Vestes - roupas';
  FIN_1_CALCADOS = 'Vestes - cal�ados';
  FIN_1_ACESSORIOS = 'Vestes - acess�rios';
  FIN_1_CAMA = 'Vestes - cama';
  FIN_1_MESA = 'Vestes - mesa';
  FIN_1_BANHO = 'Vestes - banho';
  FIN_1_COMPRAIMOVEIS = 'Im�veis - compra';
  FIN_1_REFORMAIMOVEIS = 'Im�veis - reforma';
  FIN_1_PERDA = 'Perda/roubo';
  FIN_1_LOTERIAS = 'Loterias/Apostas/Rifas';
  FIN_1_ = '';


  FIN_MSG_CONFIRMATION = 'Confirma��o';
  FIN_MSG_INFORMATION = 'Informa��o';
  FIN_MSG_WARNING = 'Aten��o';
  FIN_MSG_ERROR = 'Erro';

  FIN_MSG_TRANSFERENCIA = 'Transf.';
  FIN_MSG_EMPRESTIMO = 'Empr�stimos';
  FIN_MSG_DIVIDA = 'D�vidas';
  FIN_MSG_CONSIGNACAO = 'Consigna��es';
  FIN_MSG_OUTRASCARTEIRAS = 'Outras carteiras';
  FIN_MSG_VARIOS = 'V�rios';
  FIN_MSG_PAGTOFATURAS = 'Pagto Fatura';

  FIN_MSG_DE = ' de ';
  FIN_MSG_PARA = ' p/ ';

  FIN_MSG_NOTEXCLCARTEIRAS = 'Esta caixa n�o pode ser exclu�da';
  FIN_MSG_NOTEXCLGERAL =     'pois possui movimento.';
  FIN_MSG_NOTEXCLCONTAS = 'Esta conta n�o pode ser exclu�da.';
  FIN_MSG_NOTEXCLEMISSOES = 'Esta emiss�o n�o pode ser exclu�da.';
  FIN_MSG_NOTEXCLCONTATOS = 'Este contato n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLEMPRESAS = 'Esta empresa n�o pode ser exclu�da.';
  FIN_MSG_NOTEXCLFORNECE = 'Este fornecedor n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLGENEROS = 'Este g�nero n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLPRODUTOS = 'Este produto n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLGRUPOS = 'Este grupo n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLINDICE = 'Este �ndice n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLAJCUSTOS = 'Esta ajuda de custos n�o pode ser exclu�da.';
  FIN_MSG_NOTEXCLSERVICOS = 'Este servi�o n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLTURNOS = 'Este tuno n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCLFRENTE = 'Este caixa frentista n�o pode ser exclu�do.';
  FIN_MSG_NOTEXCL = 'Este ... n�o pode ser exclu�do.';

  FIN_MSG_NOTCLOSE1 = 'Voc� est� em modo de inser��o/edi��o.';
  FIN_MSG_NOTCLOSE2 = 'Confirme ou desista antes de sair.';

  FIN_MSG_EXCLCARTEIRAS = 'Confirma a exclus�o desta caixa?';
  FIN_MSG_EXCLCONTAS = 'Confirma a exclus�o desta conta?';
  FIN_MSG_EXCLEMISSOES = 'Confirma a exclus�o desta emiss�o?';
  FIN_MSG_EXCLCONTATOS = 'Confirma a exclus�o deste contato?';
  FIN_MSG_EXCLEMPRESAS = 'Confirma a exclus�o desta empresa?';
  FIN_MSG_EXCLFORNECE = 'Confirma a exclus�o desta empresa?';
  FIN_MSG_EXCLGENEROS = 'Confirma a exclus�o deste g�nero?';
  FIN_MSG_EXCLGRUPOS = 'Confirma a exclus�o deste grupo?';
  FIN_MSG_EXCLPRODUTOS = 'Confirma a exclus�o deste produto?';
  FIN_MSG_EXCLSERVICOS = 'Confirma a exclus�o deste Servi�o?';
  FIN_MSG_EXCLTURNOS = 'Confirma a exclus�o deste turno?';
  FIN_MSG_EXCLFRENTE = 'Confirma a exclus�o deste caixa frentista?';
  FIN_MSG_EXCLACERTOESTQ = 'Confirma a exclus�o deste acerto de estoque?';
  FIN_MSG_EXCLSAIDA = 'Confirma a exclus�o desta sa�da?';
  FIN_MSG_EXCLENTRADA = 'Confirma a exclus�o desta entrada?';
  FIN_MSG_EXCLREFORCO = 'Confirma a exclus�o deste refor�o de caixa?';
//  FIN_MSG_EXCL = 'Confirma a exclus�o dest[?] ?';


  FIN_MSG_ERROFATAL = 'Ocorreu um erro fatal em '+FIN_APPNAME;
  FIN_MSG_SELECTITULAR = 'Selecione um titular.';
  FIN_MSG_VALORDIFZERO = 'Defina um valor diferente de 0.';
  FIN_MSG_CREDEB1 = 'Defina um valor positivo';
  FIN_MSG_CREDEB2 = 'para cr�dito ou d�bito.';
  FIN_MSG_CREDEB3 = 'N�o foi informado nenhum valor';
  FIN_MSG_VALORINVMENQPOS = 'Defina um valor positivo maior que 0.';
  FIN_MSG_VALORINVALIDO = 'O valor n�o � um n�mero v�lido.';
  FIN_MSG_VALORINTINVALIDO = 'O valor n�o � um n�mero inteiro v�lido.';
  FIN_MSG_DATAINVALIDA = 'Data inv�lida.';
  FIN_MSG_NUMINVALIDO = 'N�mero inv�lido.';
  FIN_MSG_VALORNAOCONFERE = 'Os valores n�o conferem.';
  FIN_MSG_EDITCANCEL1_1 = 'Edi��o n�o permitida.';
  FIN_MSG_EDITCANCEL1_2 = 'Este lan�amento possui conex�es.';
  FIN_MSG_EDITCANCEL1_3 = 'Para realizar modifica��es,';
  FIN_MSG_EDITCANCEL1_4 = 'acesse o menu.';
  FIN_MSG_EDITCANCEL2_1 = 'Este lan�amento pertence a uma fatura.';
  FIN_MSG_EDITCANCEL4 = 'Este lan�amento j� possui movimento.';
  FIN_MSG_EDITCANCEL5 = 'Esta tranfer�ncia pertence ao caixa geral.';
  FIN_MSG_POSTCANCEL1 = 'Defina uma descri��o.';
  FIN_MSG_IMPOSSOPER = 'Imposs�vel realizar opera��o.';
  FIN_MSG_OPERNAOPERMIT = 'Opera��o n�o permitida.';
  FIN_MSG_OPERNAOPERMIT1_1 = 'Utilize a caixa emiss�es';
  FIN_MSG_OPERNAOPERMIT1_2 = 'para criar o empr�stimo.';
  FIN_MSG_OPERNAOPERMIT2_1 = 'para criar a d�vida.';
  FIN_MSG_TRANSFERIR = 'Selecione o modo de transfer�ncia.';
  FIN_MSG_CANCELDATAINI1 = 'A data inicial deve ser superior';
  FIN_MSG_CANCELDATAINI2 = 'a data do movimento.';
  FIN_MSG_DATA = 'A data do movimento deve ser informada.';
  FIN_MSG_DEVEDOR = 'Selecione o devedor.';
  FIN_MSG_CREDOR = 'Selecione o credor.';
  FIN_MSG_JURONEGATIVO = 'O juro n�o pode ser menor que zero.';
  FIN_MSG_CALCWARNING1 = 'Deseja realmente recalcular?';
  FIN_MSG_CALCWARNING2 = 'Os dados de vencimentos e pagamentos';
  FIN_MSG_CALCWARNING3 = 'ser�o eliminados!';
  FIN_MSG_ASKESCLUI = 'Confirma a exclus�o deste registro?';
  FIN_MSG_ASKESCLUITRANSF = 'Confirma a exclus�o desta transfer�ncia?';
  FIN_MSG_ASKESCLUIVARIOS1 = 'Confirma a exclus�o de todos';
  FIN_MSG_ASKESCLUIVARIOS2 = 'os registros selecionados?';
  FIN_MSG_ASKLIMPA = 'Confirma a limpeza dos dados importados?';
  FIN_MSG_ASKLIMPA1 = 'Confirma o cancelamento dos dados modificados?';
  FIN_MSG_ASKESCLUITUDO1 = 'Todas parcelas e pagamentos';
  FIN_MSG_ASKESCLUITUDO2 = 'ser�o excluidos.';
  FIN_MSG_ASKESCLUITUDO3 = 'Todos pagamentos desta parcela';
  FIN_MSG_ASKESCLUIPATRIMCOMPRA = 'Confirma a exclus�o desta aquisi��o?';
  FIN_MSG_ASKESCLUIPATRIMVENDA = 'Confirma a exclus�o desta venda?';
  FIN_MSG_ASKCARGARAPIDA1 = 'Para diminur o tempo de carga do';
  FIN_MSG_ASKCARGARAPIDA2 = 'aplicativo, basta desmarcar a caixa';
  FIN_MSG_ASKCARGARAPIDA3 = '"animar o progresso" em op��es.';
  FIN_MSG_ASKCARGARAPIDA4 = 'Deseja desmarc�-la agora?';
  FIN_MSG_ASKZERADOC = 'Deseja zerar o n�mero do documento?';
  FIN_MSG_ASKESCLUICORES1 = 'As cores cadastradas para este';
  FIN_MSG_ASKESCLUICORES2 = 'artigo ser�o juntamente excluidas.';
  FIN_MSG_PGTO = 'Pagamento ';
  FIN_MSG_PARC_ = 'parc. ';
  FIN_MSG_NF_ = ' NF ';
  FIN_MSG_PRINCIPAL = 'principal ';
  FIN_MSG_JUROS = 'juros ';
  FIN_MSG_MULTAS = 'multa ';
  FIN_MSG_POS = 'Pos';
  FIN_MSG_EFETCALC = 'Efetue o c�lculo antes de confirmar.';
  FIN_MSG_ARQUIVOINVALIDO = 'Arquivo inv�lido';
  FIN_MSG_GENERONAOPERDEBITO = 'Este g�nero n�o permite d�bito.';
  FIN_MSG_GENERONAOPERCREDITO = 'Este g�nero n�o permite cr�dito.';
  FIN_MSG_DEFTANQUE = 'Defina um tanque.';
  FIN_MSG_DEFCOMB = 'Defina um combust�vel.';
  FIN_MSG_DEFPARCELA1 = 'Defina uma parcela antes de';
  FIN_MSG_DEFPARCELA2 = 'efetuar um pagamento.';
  FIN_MSG_DEFGROUEMP = 'Defina um grupo ou uma empresa.';
  FIN_MSG_DEFGENERO = 'Defina um G�nero.';
  FIN_MSG_DEFCUSTOVALIDO = 'Defina um custo v�lido';
  FIN_MSG_DEFVALOR1 = 'Defina um valor positivo';
  FIN_MSG_DEFVALOR2 = 'para cr�dito ou d�bito.';
  FIN_MSG_DEFDEVEDOR = 'Defina um devedor';
  FIN_MSG_DEFCREDOR = 'Defina um credor';
  FIN_MSG_DEFDESCRICAO = 'Defina uma descri��o';
  FIN_MSG_DEFFATURA = 'Defina uma fatura';
  FIN_MSG_DEFEMPRESTIMO = 'Defina um empr�stimo.';
  FIN_MSG_DEFEMPRESA = 'Defina uma empresa.';
  FIN_MSG_DEFNOMEEMP = 'Defina um nome para a Empresa.';
  FIN_MSG_DEFLOGIN = 'Defina um login.';
  FIN_MSG_DEFSENHA = 'Defina uma senha.';
  FIN_MSG_DEFPERFIL = 'Defina um perifl de usu�rio.';
  FIN_MSG_DEFDIVIDA = 'Defina uma d�vida.';
  FIN_MSG_DEFCONSIGNACAO = 'Defina uma consigna��o.';
  FIN_MSG_DEFCONTATO = 'Defina um contato.';
  FIN_MSG_DEFDOC = 'Defina um documento.';
  FIN_MSG_DEFEMISSAO = 'Defina uma emiss�o.';
  FIN_MSG_DEFPERIODO = 'Defina um per�odo.';
  FIN_MSG_DEFCODOUDESCRI = 'Defina um c�digo ou uma descri��o.';
  FIN_MSG_DEFFORNECE = 'Defina um fornecedor.';
  FIN_MSG_DEFCLIENTEI = 'Defina um Cliente Interno.';
  FIN_MSG_DEFQUANTIDADE = 'Defina a quantidade.';
  FIN_MSG_DEFPRODUTO = 'Defina o produto.';
  FIN_MSG_DEFTURNO = 'Defina um turno.';
  FIN_MSG_DEFFRENTE = 'Defina um caixa frentista.';
  FIN_MSG_DEFCARTPARACXAGERAL = 'Defina um caixa para o caixa geral!';
  FIN_MSG_DEFNF = 'Defina um valor positivo para a nota fiscal.';
  FIN_MSG_DEFDATAC = 'Defina uma data para a compra.';
  FIN_MSG_DEFDATAV = 'Defina uma data para a venda.';
  FIN_MSG_DEFDATAENTREGA = 'Defina uma data para a entrega.';
  FIN_MSG_DEFTRANSPORTADORA = 'Defina um fornecedor.';
  VAR_MSG_DEFPQ = 'Defina um insumo qu�mico.';
  FIN_MSG_JAEMITIDO = 'Registro j� emitido.';
  FIN_MSG_JABAIXADA = 'Erro. Emiss�o j� baixada.';
  FIN_MSG_IMPORTACAOOK = 'Importa��o realizada com sucesso.';
  FIN_MSG_ATUALIZANDOINI = 'Iniciando atualiza��o';
  FIN_MSG_ATUALIZANDOEMP = 'Atualizando empr�stimos';
  FIN_MSG_ATUALIZANDODIV = 'Atualizando d�vidas';
  FIN_MSG_2521 = 'AVISO IMPORTANTE:';
  FIN_MSG_2522 = 'Devido ao alto n�mero de parcelas';
  FIN_MSG_2523 = 'o empr�stimo ser� confirmado.';
  FIN_MSG_2524 = 'Para modific�-lo ou exclu�-lo,';
  FIN_MSG_2525 = 'edite-o novamente.';
  FIN_MSG_2526 = 'Confirma o c�lculo?';
  FIN_MSG_PARCISUFICIENTE = 'O valor da parcela � insuficiente para gerar juros.';
//  FIN_MSG_PARCISUFICIENTE1 = 'O valor da parcela �';
//  FIN_MSG_PARCISUFICIENTE2 = 'insufiente para gerar juros.';
  FIN_MSG_DEVOLVEEMISSAO1 = 'Deseja realmente retirar';
  FIN_MSG_DEVOLVEEMISSAO2 = 'a emiss�o desta fatura?';
  FIN_MSG_REGISTRONAOLOCALIZADO = 'Registro n�o localizado.';
  FIN_MSG_CARTNAOLOCALIZADA = 'Caixa n�o localizada.';
  FIN_MSG_DOCNAOLOCALIZADO = 'Documento n�o localizado.';
//  FIN_MSG_VALORDOCNAOCOINCIDE1 = 'O documento foi localizado,';
//  FIN_MSG_VALORDOCNAOCOINCIDE2 = 'mas os valores n�o coincidem.';
//  FIN_MSG_VALORESDOCNAOCOINCIDEM1 = 'As emiss�es do documento';
//  FIN_MSG_VALORESDOCNAOCOINCIDEM2 = 'foram localizadas, mas a soma';
//  FIN_MSG_VALORESDOCNAOCOINCIDEM3 = 'dos valores n�o coincidem.';
  FIN_MSG_EMISSAONAOLOCALIZADA = 'Emiss�o n�o localizada.';
  FIN_MSG_CONSIGNAOLOCALIZADA = 'Consigna��o n�o localizada.';
  FIN_MSG_AJUDANAOLOCALIZ = 'Ajuda n�o localizada.';
  FIN_MSG_LINHACOMERRO = 'Existe uma linha com erro.';
  FIN_MSG_LINHASCOMERRO1 = 'Existem ';
  FIN_MSG_LINHASCOMERRO2 = ' linhas com erro.';
  FIN_MSG_AJUDAJAEXISTE1 = 'Esta empresa j� possui movimento';
  FIN_MSG_AJUDAJAEXISTE2 = 'para este per�odo.';
  FIN_MSG_AJUDAJAEXISTE3 = 'Deseja edit�-lo?';
  FIN_MSG_EMPRESAS = 'Empresas';
  FIN_MSG_COMMIT1 = 'Existem opera��es pendentes,';
  FIN_MSG_COMMIT2 = 'Deseja confirma-l�s?';
  FIN_MSG_CONFEXCLFATURA = 'Confirma a exclus�o desta fatura?';
  FIN_MSG_CANCELEXCLFATURA1 = 'Esta fatura n�o pode ser excluida,';
  FIN_MSG_CANCELEXCLFATURA2 = 'pois j� possui movimento';
  FIN_MSG_OPERDEVESERDEBITO = 'A opera��o requer d�bito.';
  FIN_MSG_CONFCANCELBAIXA = 'Confirma o cancelamento desta Baixa?';
  FIN_MSG_CONFCANCELBAIXA2 = 'Baixa n�o localizada. Cancela baixa assim mesmo?';
  FIN_MSG_CONTANAOCAD = 'Conta n�o cadastrada';
  FIN_MSG_ERROFATALPRIMAVEZ = 'Erro Fatal ao iniciar pela primeira vez';
  FIN_MSG_RESULTADO = 'Resultado = ';
  FIN_MSG_NENHUMALINHASELEC = 'N�o foi selecionada nenhuma linha';
  FIN_MSG_SALDOANT = 'SALDO ANTERIOR';
  FIN_MSG_SALDOGERAL = 'SALDO GERAL';
  FIN_MSG_EXTRATO = 'EXTRATO';
  FIN_MSG_GERAL = 'GERAL';
  FIN_MSG_SALDOVENCIDOS = 'Saldo vencidos';
  FIN_MSG_SALDOABERTOS = 'Saldo abertos';
  FIN_MSG_DOCJALOCALIZADO = 'O documento foi localizado.';
//  FIN_MSG_DOCINEXIST1 = 'O documento n�o existe.';
//  FIN_MSG_DOCINEXIST2 = 'Deseja visualizar todos';
//  FIN_MSG_DOCINEXIST3 = 'documentos n�o baixados?';
//  FIN_MSG_DOCINEXIST4 = 'N�o existe nenhum documento';
//  FIN_MSG_DOCINEXIST5 = 'para exibi��o no momento.';
  FIN_MSG_DEFBAIXA = 'Os valores n�o conferem.';
  FIN_MSG_ASKEXCLUIKM1 = 'Deseja realmente excluir todo';
  FIN_MSG_ASKEXCLUIKM2 = 'controle de kilometragem deste ve�culo?';
  FIN_MSG_FATEXPERRO1 = 'Erro ao exportar.';
  FIN_MSG_FATEXPERRO2 = 'Fartura n�o localizada.';
  FIN_MSG_IMPOSSCRIAR = 'Imposs�vel criar o caminho';
  FIN_MSG_DERMATEKBACKUPS = 'C:\Dermatek\Backups';
  FIN_MSG_DERMATEKBACKUPS1 = 'C:\Dermatek';
  FIN_MSG_DIR1 = 'c:\Meus Documentos';
  FIN_MSG_TRANSFNAOLOCALIZADA = 'Transfer�ncia n�o localizada.';
  FIN_MSG_PREVIEWJAEXISTE = 'J� existe uma pr�-visualiza��o.';
  FIN_MSG_RELRESULTPERIODGEN = 'G�neros - Resultados no Per�odo de ';
  FIN_MSG_RELRESULTPERIODGRU = 'Grupos - Resultados no Per�odo de ';
  FIN_MSG_ENDEXCELERRADO = 'Endere�o inv�lido para c�lula do Excel.';
  FIN_MSG_VALORFLOATINVALIDO = 'Valor de ponto flutuante inv�lido.';
  FIN_MSG_DEFGRUPOOUEMPRESA = 'Defina um grupo ou uma empresa.';
  FIN_MSG_DEFGRUPO = 'Defina um grupo.';
  FIN_MSG_DEFCONTA = 'Defina uma Conta.';
  FIN_MSG_EMPRESANAOLOCALIZADA = 'Empresa n�o localizada.';
  FIN_MSG_VEICULONAOLOCALIZADO = 'Ve�culo n�o localizado.';
  FIN_MSG_CONTAGENXLS1 = 'Existem g�neros desta empresa';
  FIN_MSG_CONTAGENXLS2 = 'sem endere�o para o excel.';
  FIN_MSG_CONTAGENXLS3 = 'Deseja continuar assim mesmo?';
  FIN_MSG_ARQUIVONAOFOISALVO = 'O arquivo n�o foi salvo!';
  FIN_MSG_ERROAOGRAVARARQ = 'Erro ao tentar gravar o arquivo.';
  FIN_MSG_ERRONOPROCESSO = 'Ocorreu um ou mais erros no processo!';
  FIN_MSG_ASKVERARQ = 'Deseja visualizar o arquivo?';
  FIN_MSG_ARQUIVONAOLOCALIZ = 'Arquivo n�o localizado!';
  FIN_MSG_OCORRERRO = 'Ocorr�ncia de erro';
  FIN_MSG_NAOEXISTECONTA = 'N�o existe conta cadastrada!';
  FIN_MSG_NAOEXISTETURNO = 'N�o existe turno cadastrado!';
  FIN_MSG_NAOEXISTECOMB = 'N�o existe combust�vel cadastrado!';
  FIN_MSG_NAOEXISTEVEICULO1 = 'N�o existe ve�culo cadastrado';
  FIN_MSG_NAOEXISTEVEICULO2 = 'para esta empresa.';
  FIN_MSG_NAOEXISTEGRUPO = 'N�o existe grupo cadastrado!';
  FIN_MSG_NAOEXISTEEMPRESA = 'N�o existe empresa cadastrada!';
  FIN_MSG_NAOEXISTEFORNECE = 'N�o existe fornecedor cadastrado!';
  FIN_MSG_POSICAOXLS = 'Endere�o = ';
  FIN_MSG_DEFENDXLS1 = 'Antes de exportar a lista de g�neros,';
  FIN_MSG_DEFENDXLS2 = '� necess�rio definir um endere�o em op��es.';
  FIN_MSG_TOTAISNAOCONF = 'Os totais n�o conferem.';
  FIN_MSG_NAOEXISTCXAGERABERTO = 'N�o existe caixa geral aberto.';
  FIN_MSG_FRENTENAOLOCALIZ = 'Caixa frentista n�o localizado.';
  FIN_MSG_TERMEDITCONTA = 'Termine a edi��o em aberto da conta!';
  FIN_MSG_TERMEDITCART = 'Termine a edi��o em aberto do caixa!';
  FIN_MSG_TERMEDITEMISS = 'Termine a edi��o em aberto da emiss�o!';
  FIN_MSG_DATAREFINVALIDA = 'Data de refer�ncia inv�lida.';
  FIN_MSG_EDITNAOPERMIT = 'Edi��o n�o permitida';
  FIN_MSG_EXCLNAOPERMIT = 'Exclus�o n�o permitida';
  FIN_MSG_EXCLEMISSAO = 'Desfa�a a baixa desta emiss�o.';
  FIN_MSG_CXAGERAL = 'Caixa Geral';
  FIN_MSG_ERROPRINT = 'Erro de impress�o. Deseja cancelar encerramento?';
  FIN_MSG_ERROPRINT2 = 'Erro de impress�o. Consulta imprime 2� via.';
  FIN_MSG_CONFIRMAENCERRCXA = 'Confirma o encerramento definitivo do caixa?';
  FIN_MSG_ANTECIPABALANCETE1 = 'A data do �ltimo caixa encerrado';
  FIN_MSG_ANTECIPABALANCETE2 = 'n�o � o �ltimo dia do m�s.';
  FIN_MSG_ANTECIPABALANCETE3 = 'Fazer o balancete assim mesmo?';
  FIN_MSG_IMPOSSBALANCETE = 'N�o existem dados para realizar balancete.';
  FIN_MSG_ALTERCANCEL = 'As altera��es foram canceladas!';
  FIN_MSG_NAODELETE1 = 'Antes de excluir o caixa,';
  FIN_MSG_NAODELETE2 = 'exclua o movimento da caixa.';
  FIN_MSG_SDOANTERIOR = 'Saldo anterior';
  FIN_MSG_EXISTITENS0 = 'Exclus�o cancelada.';
  FIN_MSG_EXISTITENS1 = 'Existem itens pendentes.';
  FIN_MSG_CONSULTACAIXA = 'Emiss�o em consulta';
  FIN_MSG_PRODNAOENC = 'Produto n�o encontrado.';
  FIN_MSG_SERVNAOENC = 'Servi�o n�o encontrado.';
  FIN_MSG_COMBNAOENC = 'Combust�vel n�o encontrado.';
  FIN_MSG_TANQUENAOENC = 'Tanque n�o encontrado.';
  FIN_MSG_ENCERRINVALIDO = 'Encerrante deve ser maior que a abertura.';
  FIN_MSG_BOMBAJADIGIT = 'Bomba j� digitada.';
  FIN_MSG_PRODJADIGIT = 'Produto j� digitado.';
  FIN_MSG_AFERICAOINVALIDA = 'Aferi��o deve ser menor que sa�da.';
  FIN_MSG_CONFFECHAFRENTE = 'Confirma fechamento deste caixa frentista?';
  FIN_MSG_PRODSEMTANQUE1 = 'Este produto est� cadastrado como';
  FIN_MSG_PRODSEMTANQUE2= 'combust�vel, mas n�o possui tanque associado.';
  FIN_MSG_BOMBACOMSALDO = 'Exclus�o cancelada. Bomba com saldo.';
  FIN_MSG_EXCLBOMBA = 'Confirma a exclus�o desta bomba?';
  FIN_MSG_SESSAONAOACESSIVAL = 'Sess�o n�o acess�vel.';
  FIN_MSG_DEFDEPOSITOPOSIT1 = 'Defina um valor positivo para';
  FIN_MSG_DEFDEPOSITOPOSIT2 = 'o estoque do dep�sito.';
  FIN_MSG_DEFDEPOSITMENOSQSALDO1 = 'Defina um valor n�o';
  FIN_MSG_DEFDEPOSITMENOSQSALDO2 = 'maior que o estoque.';
  FIN_MSG_DEFESTQATUALPOSIT1 = 'Defina um valor positivo para o estoque atual.';
  FIN_MSG_DEFESTQATUALMENANT = 'Defina um estoque atual menor que o anterior.';
  FIN_MSG_NENHUMCXAFRENTE = 'N�o existe caixa frentista apto a ser aberto.';
  FIN_MSG_EXISTTANQCADASTR = 'Opera��o cancelada, exite tanque cadastrado.';
  FIN_MSG_CAIXAGERALENCERRADO = 'Opera��o inv�lida. Caixa geral encerrado.';
  FIN_MSG_QUANTNAOCONF = 'Quantidades n�o conferem.';
  FIN_MSG_CXAGERALABERTO1 = 'N�o pode haver caixa geral aberto no';
  FIN_MSG_CXAGERALABERTO2 = 'per�odo da realiza��o do balancete.';
  FIN_MSG_NAOEMPRESACADASTR = 'Licen�a n�o liberada.';
  FIN_MSG_SENHANAOCONFERE = 'Senha de confirma��o n�o � identica.';
  FIN_MSG_PATRCOMPVEND1 = 'A compra deste patrim�nio n�o pode ser exclu�da,';
  FIN_MSG_PATRCOMPVEND2 = 'pois o patrim�nio j� foi vendido.';
  VAR_ACESSONEGADO = 'Login sem permiss�o. Consulte seu superior.';
  VAR_ALTERANEGADO = 'Login sem permiss�o para edi��es.';
  FIN_MSG_NAOLOCALIZUSUARIO = 'Usu�rio n�o localizado.';
  FIN_MSG_CANCELEDITGERAL = 'Para cancelar esta edi��o, cancele o caixa geral.';
  FIN_MSG_PEND = 'Pend�cias e adiantamentos';
  FIN_MSG_BALCONFIRMADO = 'Balancete confirmado.';
  FIN_MSG_SELECTEMISS1 = 'Selecione a fatura pela emiss�o';
  FIN_MSG_SELECTEMISS2 = '(Visualizar) para inserir.';
  FIN_MSG_ERRONALINHA = 'Erro na linha n� ';
  FIN_MSG_GENEROREQUERMES = 'Este g�nero requer o m�s do movimento.';
  FIN_MSG_GENERONAOREQUERMES = 'Este g�nero n�o requer m�s do movimento.';
  FIN_MSG_DEFESPMAIORQMIN = 'Defina o valor esperado maior que o m�nimo.';
  FIN_MSG_GENEROSISTEMA = 'G�nero exclusivo do sistema.';
  FIN_MSG_REGISTROTRAVADO1 = 'Este registro j� est� sendo';
  FIN_MSG_REGISTROTRAVADO2 = 'editado por outro usu�rio.';
  FIN_MSG_REGISTROTRAVADO3_1 = 'editado pelo usu�rio:';
  FIN_MSG_REGISTROTRAVADO3_21 = 'editado por voc�.';
  FIN_MSG_REGISTROTRAVADO3_22 = 'Deseja re-edit�lo?';

  FIN_MSG_REGISTROTRAVADO4_1 = 'Existe um registro da tabela ';
  FIN_MSG_REGISTROTRAVADO4_2 = 'que esta bloqueado pelo usu�rio ';

  FIN_MSG_REGISTROTRAVADO5_1 = 'Existem ';
  FIN_MSG_REGISTROTRAVADO5_2 = ' registros da tabela ';
  FIN_MSG_REGISTROTRAVADO5_3 = 'que est�o bloqueados pelo(s) usu�rio(s):';
  FIN_MSG_DEFMANPGTO = 'Defina a meneira de pagamento.';

  FIN_MSG_TRANSFSOEXCLUIR1 = 'Transfer�ncias n�o podem ser modificadas,';
  FIN_MSG_TRANSFSOEXCLUIR2 = 'apenas exclu�das e refeitas.';
  FIN_MSG_NAOEMISSINCC1 = 'N�o foi encontrada nenhuma';
  FIN_MSG_NAOEMISSINCC2 = 'emiss�o para esta conta.';

  VAR_INSIRAPERFIL = 'Insira pelo menos um perfil!';

  VAR_MSG_ITEMDUPLICADO = 'Registro Duplicado!';

  VAR_MSG_INSUMOQUIMICO = 'Insumos qu�micos';
  VAR_MSG_MATERIAPRIMA = 'Mat�rias-primas';
  VAR_MSG_INSUMOQUIMICOPS = 'Insumos qu�micos (PS)';
  VAR_MSG_MATERIAPRIMAPS = 'Mat�rias-primas (PS)';
  VAR_MSG_FRETEPQ = 'Frete de Insumos qu�micos';
  VAR_MSG_FRETEMP = 'Frete de Mat�rias-primas';
  VAR_MSG_FRETEPQPS = 'Frete de Insumos qu�micos (PS)';
  VAR_MSG_FRETEMPPS = 'Frete de Mat�rias-primas (PS)';
  VAR_MSG_FATURAMPPS = 'Fatura de presta��o de servi�o (MP)';
  VAR_MSG_DEFCARTEIRA = 'Defina uma carteira.';
  VAR_MSG_DUPLICATATRANSP = 'Duplicatas do Transporte';
  VAR_MSG_DUPLICATAPQ = 'Duplicatas dos Insumos Qu�micos';
  VAR_MSG_DUPLICATAMP = 'Duplicatas das Mat�rias-primas';
  VAR_MSG_FATURAMERC = 'Duplicatas de Venda';
  VAR_MSG_EMISSBXNAOEDIT = 'Emiss�es baixadas n�o podem ser editadas.';
  VAR_MSG_MATRICULAS = 'Pagamento de Matr�cula';

  VAR_MSG_ERROEXCLSUCESS = 'Erro excluido com sucesso.';
  VAR_MSG_ACONSELHAREFRESH = '� aconselhavel executar os respectivos REFRESH.';
  VAR_MSG_INTEIROINVALIDO = 'N�mero inteiro inv�lido.';
  

implementation

end.
