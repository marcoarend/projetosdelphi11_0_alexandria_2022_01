unit Module;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Phys, FireDAC.Comp.Client,
  FireDAC.Phys.Oracle, FireDAC.Phys.OracleDef, ZAbstractConnection, ZConnection,
  Data.DB, ZAbstractRODataset, ZAbstractDataset, ZAbstractTable, ZDataset;

type
  TDmod = class(TDataModule)
    ZCVirtualAge: TZConnection;
    ZCLocal: TZConnection;
    ZQuery1: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDmod.DataModuleCreate(Sender: TObject);
begin


  //ZCVirtualAge.Database := '' ????

(*
[282A28E7]{ZPlain250.bpl} Zplainloader.TZNativeLibraryLoader.LoadNativeLibrary + $13F
[148A51B3]{ZCore250.bpl} Zurl.TZURL.GetLibLocation + $8F
[282A3754]{ZPlain250.bpl} Zplaindriver.TZAbstractPlainDriver.Initialize + $48
[282DF61E]{ZPlain250.bpl} Zplainoracledriver.TZOracle9iPlainDriver.Initialize + $12
[2835F56D]{ZDbc250.bpl } Zdbcconnection.TZAbstractDriver.GetPlainDriver + $69
[2836039C]{ZDbc250.bpl } Zdbcconnection.TZAbstractConnection + $C4
[2842B67D]{ZDbc250.bpl } Zdbcoracle.TZOracleDriver.Connect + $19
[2835F0FB]{ZDbc250.bpl } Zdbcconnection.TZAbstractDriver.Connect + $3F
[28325493]{ZDbc250.bpl } Zdbcintfs. + $0
[1A3DD3C5]{ZComponent250.bpl} Zabstractconnection.TZAbstractConnection.Connect + $111
[1A3DCB00]{ZComponent250.bpl} Zabstractconnection.TZAbstractConnection.SetConnected + $3C
[501217B6]{rtl250.bpl  } System.TypInfo.SetOrdProp (Line 2874, "System.TypInfo.pas" + 40) + $2
[211C99F2]{designide250.bpl} DesignEditors.TPropertyEditor.SetOrdValue (Line 840, "DesignEditors.pas" + 2) + $E
[212218F3]{designide250.bpl} VCLEditors.TBooleanProperty.MouseUp (Line 1915, "VCLEditors.pas" + 5) + $10
[213E321D]{vclide250.bpl} PropBox.TCustomPropListBox.ItemMouseUp (Line 1682, "PropBox.pas" + 17) + $1B
[213E3413]{vclide250.bpl} PropBox.TCustomPropListBox.MouseUp (Line 1747, "PropBox.pas" + 1) + $D
[50AD55BC]{vcl250.bpl  } Vcl.Controls.TControl.DoMouseUp (Line 7570, "Vcl.Controls.pas" + 2) + $28
[50AD564A]{vcl250.bpl  } Vcl.Controls.TControl.WMLButtonUp (Line 7583, "Vcl.Controls.pas" + 9) + $6
[213E9334]{vclide250.bpl} IDEInspListBox.TInspListBox.WMLButtonUp (Line 1643, "IDEInspListBox.pas" + 3) + $4
[50AD4C16]{vcl250.bpl  } Vcl.Controls.TControl.WndProc (Line 7326, "Vcl.Controls.pas" + 91) + $6
[0DA45188]{TrackingSystem250.bpl} TrackingSystemHelp.CBTHookProc (Line 211, "TrackingSystemHelp.pas" + 27) + $F
[50AD8FB7]{vcl250.bpl  } Vcl.Controls.TWinControl.IsControlMouseMsg (Line 9952, "Vcl.Controls.pas" + 1) + $9
[50AD97FB]{vcl250.bpl  } Vcl.Controls.TWinControl.WndProc (Line 10197, "Vcl.Controls.pas" + 166) + $6
[50AFE0F1]{vcl250.bpl  } Vcl.StdCtrls.TCustomListBox.WndProc (Line 7243, "Vcl.StdCtrls.pas" + 55) + $5
[50AD8DC8]{vcl250.bpl  } Vcl.Controls.TWinControl.MainWndProc (Line 9896, "Vcl.Controls.pas" + 3) + $6
[5016F4C8]{rtl250.bpl  } System.Classes.CalcJmpOffset (Line 17431, "System.Classes.pas" + 0) + $4
[50AD7F22]{vcl250.bpl  } Vcl.Controls.TWinControl.GetControl (Line 9251, "Vcl.Controls.pas" + 4) + $A
[50C223FF]{vcl250.bpl  } Vcl.Forms.TApplication.ProcessMessage (Line 10613, "Vcl.Forms.pas" + 23) + $1
[50C22442]{vcl250.bpl  } Vcl.Forms.TApplication.HandleMessage (Line 10643, "Vcl.Forms.pas" + 1) + $4
[50C22775]{vcl250.bpl  } Vcl.Forms.TApplication.Run (Line 10781, "Vcl.Forms.pas" + 26) + $3
[005098E2]{bds.exe     } bds.bds (Line 212, "" + 7) + $7
*)
end;

end.
