unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons,
  dmkGeral;

type
  TForm2 = class(TForm)
    BtConectar: TBitBtn;
    Label1: TLabel;
    procedure BtConectarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Module;

{$R *.dfm}

procedure TForm2.BtConectarClick(Sender: TObject);
begin
  Dmod.ZCLocal.Connected := False;
  Dmod.ZCLocal.LibraryLocation := 'C:\Instaladores\Oracle\Oracle Instant Client 32 Bit\instantclient_19_9\oci.dll';
  Dmod.ZCLocal.Protocol := 'oracle';
  //
  Dmod.ZCLocal.Database := 'localhost';
  Dmod.ZCLocal.HostName := 'xe';
  Dmod.ZCLocal.Port     := 1521;
  Dmod.ZCLocal.User     := 'SYSTEM';
  Dmod.ZCLocal.Password := 'SYSTEM';
  //
  try
    Dmod.ZCLocal.Connect;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(Geral.ATS([
      'N�o foi poss�vel conectar em : ',
      'Database: ' + Dmod.ZCLocal.Database,
      'HostName: ' + Dmod.ZCLocal.HostName,
      'Port: '     + Geral.FF0(Dmod.ZCLocal.Port),
      'User: '     + Dmod.ZCLocal.User,
      'Password: ' + Dmod.ZCLocal.Password,
      '',
      'Erro de retorno:',
      '',
      E.Message]));
    end;
  end;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  Application.CreateForm(TDmod, Dmod);
end;

end.
