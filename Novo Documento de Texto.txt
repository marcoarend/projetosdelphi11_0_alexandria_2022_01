
Search Find in files.... => "= class(TDataModule)"




//////////////////////////////////////////////////////////////////////////////////////////////


  private
    { Private declarations }
    FPixelsPerInch: Integer;
    //
    ...
    //
    procedure ReadPixelsPerInch(Reader: TReader);
    procedure WritePixelsPerInch(Writer: TWriter);
    //
    ....



  protected
    procedure DefineProperties(Filer: TFiler); override;
  .... 



  public
    { Public declarations }
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;
    //
    ...







procedure TDm?????.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TDm?????.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDm?????.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;


