unit UnGrl_REST;

interface

uses System.Classes, System.SysUtils, UnDmkEnums, Data.DBXJSON, Db,
  {$IfDef DMK_FMX}
  FMX.Forms, FMX.Controls, UnFMX_DmkHTML2;
  {$Else}
  Vcl.Forms, Vcl.Controls, UnDmkHTML2, Xml.XMLDoc, Winapi.Windows, Xmlxform;
  {$EndIf}

type
  TRestRespTip = (trtArray=0, trtCSV=1, trtJSON=2, trtHTML=3, trtPHP=4,
    trtSerialized=5, trtXML=6);
  TUnGrl_REST = class(TObject)
  private
    // R E S T   D E R M A T E K
    function  Rest_Dermatek_Obtem_Header(Token, Client, App, Modulo,
              Funcao: String; TipResp: TRestRespTip; var URL: String;
              var HeaderParams: array of THttpHeader): Boolean;
    function  Rest_TRestRespTipToString(TipResp: TRestRespTip): String;
    // O A u t h
    function  OAuth2_Dermatek_ConfiguraURL(App_Id: String): String;
    procedure OAuth2_Dermatek_AccessTokenRedirect(const AURL: string;
              var DoCloseWebView: Boolean);
  public
    // R E S T   D E R M A T E K
    {$IfnDef DMK_FMX}
    procedure Rest_XmlToClientDataSet(XMLTransformProvider: TXMLTransformProvider;
              Recurso_nome, REST_Result: String);
    {$EndIf}
    function  Rest_Dermatek_GetURL_OAuth2(): String;
    function  Rest_Dermatek_GetURL(): String;
    function  Rest_Dermatek_Put(Modulo, Funcao: String; TipResp: TRestRespTip;
              Parametros: TStringStream; var Resultado: String;
              Token: String = ''; Client: String = ''; App: String = ''): Integer;
    function  Rest_Dermatek_Post(Modulo, Funcao: String; TipResp: TRestRespTip;
              Parametros: array of THttpParam; var Resultado: String;
              Token: String = ''; Client: String = ''; App: String = ''): Integer;
    // O A u t h
    function  OAuth2_Dermatek_EfetuaLogin(const Form: TForm; var Token: String;
              var Client: String; const App_Id: String = ''): Boolean;
  end;

var
  Grl_REST: TUnGrl_REST;
  Var_Token: String;

const
  CO_Dmk_Rest_Beta = False;
  CO_Dmk_App_Id = '8cwk4g08csg0owsos44cockgwskwks80';

implementation

uses UnGrl_Geral, OAuth, UnGrl_Vars, UnGrlUsuarios;

{ TUnGrl_REST }

function TUnGrl_REST.Rest_Dermatek_GetURL(): String;
begin
  if CO_Dmk_Rest_Beta = True then
    Result := 'http://localhost/Dermatek_BETA/api'
  else
    Result := 'http://www.dermatek.com.br/api';
end;

{$IfnDef DMK_FMX}
procedure TUnGrl_REST.Rest_XmlToClientDataSet(
  XMLTransformProvider: TXMLTransformProvider; Recurso_nome, REST_Result: String);
var
  Txt: TResourceStream;
  XTR: TXMLDocument;
begin
    Txt := TResourceStream.Create(HInstance, Recurso_nome, RT_RCDATA);
    try
      XTR := TXMLDocument.Create(nil);
      XTR.LoadFromStream(Txt);
      //
      XMLTransformProvider.TransformRead.TransformationDocument := XTR.DOMDocument;
      XMLTransformProvider.TransformRead.SourceXml := Trim(REST_Result);
    finally
      Txt.Free;
    end;
end;
{$EndIf}

function TUnGrl_REST.Rest_Dermatek_GetURL_OAuth2: String;
begin
  if CO_Dmk_Rest_Beta = True then
    Result := 'http://localhost/Dermatek_BETA/contas/login'
  else
    Result := 'http://www.dermatek.com.br/contas/login';
end;

function TUnGrl_REST.Rest_Dermatek_Post(Modulo, Funcao: String;
  TipResp: TRestRespTip; Parametros: array of THttpParam; var Resultado: String;
  Token: String = ''; Client: String = ''; App: String = ''): Integer;
var
  HeaderParams: array of THttpHeader;
  URL: String;
begin
  SetLength(HeaderParams, 3);
  //
  if not Rest_Dermatek_Obtem_Header(Token, Client, App, Modulo, Funcao, TipResp,
    URL, HeaderParams)
  then
    Exit;
  //
  {$IfDef DMK_FMX}
  Result := FMX_dmkHTML2.HttpPostURL(URL, HeaderParams, Parametros, Resultado);
  {$Else}
  Result := dmkHTML2.HttpPostURL(URL, HeaderParams, Parametros, Resultado);
  {$EndIf}
end;

function TUnGrl_REST.Rest_Dermatek_Put(Modulo, Funcao: String;
  TipResp: TRestRespTip; Parametros: TStringStream; var Resultado: String;
  Token: String = ''; Client: String = ''; App: String = ''): Integer;
var
  HeaderParams: array of THttpHeader;
  URL: String;
begin
  SetLength(HeaderParams, 3);
  //
  if not Rest_Dermatek_Obtem_Header(Token, Client, App, Modulo, Funcao, TipResp,
    URL, HeaderParams)
  then
    Exit;
  //
  {$IfDef DMK_FMX}
  Result := FMX_dmkHTML2.HttpPutURL(URL, HeaderParams, Parametros, Resultado);
  {$Else}
  Result := dmkHTML2.HttpPutURL(URL, HeaderParams, Parametros, Resultado);
  {$EndIf}
end;

function TUnGrl_REST.OAuth2_Dermatek_EfetuaLogin(const Form: TForm;
  var Token: String; var Client: String; const App_Id: String = ''): Boolean;
var
  URL: String;
  Fm: TFmOAuth;
begin
  Result    := False;
  Var_Token := '';
  URL       := OAuth2_Dermatek_ConfiguraURL(App_Id);
  //
  Fm := TFmOAuth.Create(Form);
  Fm.Caption := 'Dermatek - OAuth';
  Fm.OnAfterRedirect := OAuth2_Dermatek_AccessTokenRedirect;
  Fm.ShowWithURL(URL);
  //
  Fm.Release;
  //
  if Var_Token <> '' then
  begin
    Client := Copy(Var_Token, (Pos('_', Var_Token) + 1));
    Token  := Copy(Var_Token, 1, (Pos('_', Var_Token) - 1));
  end;
  //
  Result := (Token <> '') and (Client <> '');
end;

///////////////////////////////////////////////////////////// PRIVATE //////////

function TUnGrl_REST.Rest_Dermatek_Obtem_Header(Token, Client, App, Modulo,
  Funcao: String; TipResp: TRestRespTip; var URL: String;
  var HeaderParams: array of THttpHeader): Boolean;
var
  Header: array of THttpHeader;
  Resp: String;
  HeaderToken, HeaderApp, HeaderIdClient: THttpHeader;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Modulo = '', nil, 'M�dulo n�o informado!') then Exit;
  if Grl_Geral.FIC(Funcao = '', nil, 'Fun��o n�o informada!') then Exit;
  //
  Resp := Rest_TRestRespTipToString(TipResp);
  URL  := Rest_Dermatek_GetURL() + '/' + Modulo + '/' + Funcao + Resp;
  //
  HeaderToken.Nome  := 'token';
  //
  if Token <> '' then
    HeaderToken.Valor := Token
  else
    HeaderToken.Valor := VAR_API_TOKEN;
  //
  if Grl_Geral.FIC(HeaderToken.Valor  = '', nil, 'Token n�o informado!')  then Exit;
  //
  HeaderApp.Nome := 'app';
  //
  if App <> '' then
    HeaderApp.Valor := App
  else
    HeaderApp.Valor := CO_Dmk_App_Id;
  //
  HeaderIdClient.Nome  := 'client';
  //
  if Client <> '' then
    HeaderIdClient.Valor := Client
  else
    HeaderIdClient.Valor := VAR_API_STR_CLIENTE;
  //
  if Grl_Geral.FIC(HeaderIdClient.Valor = '', nil, 'Cliente n�o informado!') then Exit;
  //
  HeaderParams[0] := HeaderToken;
  HeaderParams[1] := HeaderApp;
  HeaderParams[2] := HeaderIdClient;
  //
  Result := True;
end;

function TUnGrl_REST.Rest_TRestRespTipToString(TipResp: TRestRespTip): String;
begin
  case TipResp of
    trtArray:
      Result := '.array';
    trtCSV:
      Result := '.csv';
    trtJSON:
      Result := '.json';
    trtHTML:
      Result := '.html';
    trtPHP:
      Result := '.php';
    trtSerialized:
      Result := '.serialized';
    trtXML:
      Result := '.xml';
  end;
end;

function TUnGrl_REST.OAuth2_Dermatek_ConfiguraURL(App_Id: String): String;
var
  App: String;
begin
  if App_Id = '' then
    App := CO_Dmk_App_Id
  else
    App := App_Id;
  //
  Result := Rest_Dermatek_GetURL_OAuth2() + '?app_id=' + App + '&response_type=code';
end;

procedure TUnGrl_REST.OAuth2_Dermatek_AccessTokenRedirect(const AURL: string;
  var DoCloseWebView: Boolean);
const
  CO_StrAut = 'authorization=';
var
  LPosAut, LPosCli: integer;
  LToken: string;
begin
  Var_Token := '';
  LPosAut   := Pos(CO_StrAut, AURL);
  //
  if (LPosAut > 0) then
  begin
    LToken := Copy(AURL, LPosAut + Length(CO_StrAut), Length(AURL));
    //
    if (Pos('&', LToken) > 0) then
      LToken := Copy(LToken, 1, Pos('&', LToken) - 1);
    //
    Var_Token := LToken;
    //
    if (LToken <> '') then
      DoCloseWebView := True;
  end;
end;

end.
