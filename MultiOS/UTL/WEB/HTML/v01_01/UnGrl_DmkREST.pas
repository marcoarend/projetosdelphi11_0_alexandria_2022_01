unit UnGrl_DmkREST;

interface

uses System.Classes, System.SysUtils, UnDmkEnums, Data.DBXJSON, Db, System.JSON,
  {$IfDef DMK_FMX}
  FMX.Forms, FMX.Controls, UnFMX_DmkHTML2;
  {$Else}
  Vcl.Forms, Vcl.Controls, UnDmkHTML2, Xml.XMLDoc, Winapi.Windows, Xmlxform;
  {$EndIf}

type
  TRestRespTip = (trtJSON=0, trtCSV=1,trtXML=2);
  TUnGrl_DmkREST = class(TObject)
  private
    function  Rest_GetURL(Developer: Boolean; SSL: Boolean = True): String;
    function  Rest_Obtem_Header(Token, Uri: String; TipResp: TRestRespTip;
              var URL: String; var HeaderParams: array of THttpHeader;
              Developer: Boolean; SSL: Boolean = True): Boolean;
    function  Rest_TRestRespTipToString(TipResp: TRestRespTip): String;
    function  SetReturn(ResulCode: Integer): Boolean;
    function  Ping(SSL: Boolean = False): Boolean;
  public
    //Geral
    function  GetJsonValue(JSONObj: TJSONObject; Param: String): String;

    function  JsonSingleToJsonObject(JSON: String): TJSONObject;

    function  Rest_Get(Uri: String; TipResp: TRestRespTip;
              Parametros: array of THttpParam; var Resultado: String;
              Developer: Boolean = False; Token: String = '';
              SSL: Boolean = True; TimeOut: Integer = 0): Integer;

    function  Rest_Post(Uri: String; TipResp: TRestRespTip;
              Parametros: array of THttpParam; var Resultado: String;
              Developer: Boolean = False; Token: String = '';
              SSL: Boolean = True; TimeOut: Integer = 0): Integer;

    //UnLock
    function  UnLockVendaApp(Developer: Boolean; SerialKey, PIN, CNPJ, NomePC,
              IP, VersaoAtu: String; CodAplic: Integer; OSVersion: String;
              var Vencimento, Status: String; var CliInt: Integer; var
              AtualizadoEm, Mensagem: String; SSL: Boolean = False;
              TimeOut: Integer = 0): Boolean;

    function  UnLockAplic(Developer: Boolean; SerialKey, SerialNum, CNPJ,
              DescriPC, NomePC, IP, VersaoAtu: String; Servidor,
              CodAplic: Integer; AtualizaDescriPC: Boolean; OSVersion,
              DBVersion, IPServ: String; var Vencimento: String;
              var Status: String; var CliInt: Integer; var AtualizadoEm: String;
              var Mensagem: String; SSL: Boolean = False;
              TimeOut: Integer = 3000): Integer;

    //Usu�rios Web
    function  LoginUser(Developer: Boolean; Usuario, Senha: String;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    function  LogoutUser(Developer: Boolean; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  VerificaSeEstaConectado(Developer: Boolean; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  ObtemNomeUsuario(Developer: Boolean; Codigo: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    function  VerificaStatusUsuario(Developer: Boolean; Codigo: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    //Tickets de suporte
    function  WOrdSerInclui(Developer: Boolean; Codigo,
              IdUsuarioSolicitante: Integer; AberturaTimeZone: String; Cliente,
              Solicitante, Assunto: Integer; Descricao, Sistema,
              Impacto: String; Prioridade, Modo, Status, Aplicativo: Integer;
              Versao, Janela, JanelaRel, CompoRel, Tags: String; Responsavel,
              Grupo, EnvMailAbeAdm: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  WOrdSerIncluiCom(Developer: Boolean; IdUsuarioSolicitante, Codigo,
              Controle, Entidade, Para: Integer; ComRes: Integer;
              Nome: String; EnvMail: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  WOrdSerEnc(Developer: Boolean; IdUsuarioSolicitante, Codigo,
              Status: Integer; DataHoraTimeZone: String; Solucao: Integer;
              MotivReopen: String; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  WOrdSerAtualizaRespons(Developer: Boolean; IdUsuarioSolicitante,
              Codigo, Respons, TipoUsuario: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  WOrdSerAtualizaStatus(Developer: Boolean; IdUsuarioSolicitante,
              Codigo, Status, TipoUsuariovar: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  VerificaMaxHistorico(Developer: Boolean; Usuario: String;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    function  EnviaEmailWOrdSer(Developer: Boolean; IdUsuarioSolicitante,
              Tipo, Codigo, Controle: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  InsereWOrdSerArq(Developer: Boolean; IdUsuarioSolicitante,
              Codigo, FtpWebArq: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    //FTP
    function  InsUpdRegistroFTPDiretorio(Developer: Boolean; Codigo, FtpConfig,
              TipoDir, CliInt, Depto, Nivel: Integer; Nome, Pasta,
              Caminho: String; var Mensagem: String;
              SSL: Boolean = False): Boolean;

    function  InsUpdRegistroFTPArquivo(Developer: Boolean; TipSQL: String;
              CliInt, Depto, Nivel, FtpDir: Integer; Tam, Nome,
              ArqNome: String; Origem, Ativo, Codigo: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    function  ObtemDadosFTPDir(Developer: Boolean; Diretorio: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    function  ObtemDadosFTP(Developer: Boolean; FtpConfig: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    //Termos
    function  AtualizaHashTermo(Developer: Boolean; CodUser: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    function  ConfiguraHashTermo(Developer: Boolean; CodUser, Tipo: Integer;
              var Mensagem: String; SSL: Boolean = False): Boolean;

    //Aplicativos
    function  VerificaHistoricoDeVersao(Developer: Boolean; CodAplic,
              Versao: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;
    function  ValidaModulosAplicativo(Developer: Boolean; CNPJCliente: String;
              CodAplic: Integer; var Mensagem: String;
              SSL: Boolean = False): Boolean;
    function  VerificaNovaVersaoDesktop(Developer: Boolean; Arquivo,
              SerialKey, SerialNum: String; CodAplic, VersaoAtual: Int64;
              ForcaBaixa: Boolean; var Mensagem: String;
              SSL: Boolean = False): Boolean;
    //Servicos
    function  ObtemDadosCep(Developer: Boolean; Cep: String; var Mensagem: String;
              SSL: Boolean = False): Boolean;
    function  ObtemDadosCnpj(Developer: Boolean; Cnpj: String; var Mensagem: String;
              SSL: Boolean = False): Boolean;
  end;

var
  Grl_DmkREST: TUnGrl_DmkREST;
  Var_Token: String;

const
  CO_Dmk_URL = 'dermatek.com.br/api/';
  CO_Dmk_URL_DEV = 'dev.dermatek.com.br/api/';
  CO_Dmk_Token = 'ws00ok0g0cwggcw8oscko4ks4k4oko8csk48kc40';
  CO_Dmk_CNPJ = '03143014000152';

implementation

uses UnGrl_Geral, UnGrl_Vars;

{ TUnGrl_REST }

function TUnGrl_DmkREST.LoginUser(Developer: Boolean; Usuario, Senha: String;
  var Mensagem: String; SSL: Boolean = False): Boolean;
var
  ParUser, ParPass: THttpParam;
  ResulCode: Integer;
begin
  with ParUser do
  begin
    Nome  := 'user';
    Valor := Usuario;
  end;
  //
  with ParPass do
  begin
    Nome  := 'pass';
    Valor := Senha;
  end;
  //
  ResulCode := Rest_Post('usuario/login', trtJson, [ParUser,ParPass], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.LogoutUser(Developer: Boolean; var Mensagem: String;
  SSL: Boolean): Boolean;
var
  ResulCode: Integer;
begin
  ResulCode := Rest_Get('usuario/logout', trtJson, [], Mensagem, Developer,
               '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.VerificaSeEstaConectado(Developer: Boolean;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ResulCode: Integer;
begin
  ResulCode := Rest_Get('usuario/logout', trtJson, [], Mensagem, Developer,
               '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.VerificaStatusUsuario(Developer: Boolean;
  Codigo: Integer; var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCodigo: THttpParam;
  ResulCode: Integer;
begin
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  ResulCode := Rest_Get('usuario/status', trtJson, [ParCodigo], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ValidaModulosAplicativo(Developer: Boolean;
  CNPJCliente: String; CodAplic: Integer; var Mensagem: String;
  SSL: Boolean): Boolean;
var
  ParCNPJCliente, ParCodAplic: THttpParam;
  ResulCode: Integer;
begin
  with ParCNPJCliente do
  begin
    Nome  := 'cnpjCliente';
    Valor := CNPJCliente;
  end;
  //
  with ParCodAplic do
  begin
    Nome  := 'codAplic';
    Valor := Grl_Geral.FF0(CodAplic);
  end;
  //
  ResulCode := Rest_Get('valida/aplicativo/modulos', trtJson, [ParCNPJCliente,
               ParCodAplic], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.VerificaHistoricoDeVersao(Developer: Boolean; CodAplic,
  Versao: Integer; var Mensagem: String; SSL: Boolean = False): Boolean;
var
  ParCodAplic, ParVersao: THttpParam;
  ResulCode: Integer;
begin
  with ParCodAplic do
  begin
    Nome  := 'codAplic';
    Valor := Grl_Geral.FF0(CodAplic);
  end;
  //
  with ParVersao do
  begin
    Nome  := 'versao';
    Valor := Grl_Geral.FF0(Versao);
  end;
  //
  ResulCode := Rest_Get('verifica/historico-de-versao', trtJson, [ParCodAplic,
               ParVersao], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.VerificaMaxHistorico(Developer: Boolean;
  Usuario: String; var Mensagem: String; SSL: Boolean): Boolean;
  //Usuario: array of THTTPParam; var Mensagem: String; SSL: Boolean): Boolean;
var
  ParUsuario: THttpParam;
  ResulCode: Integer;
begin
  with ParUsuario do
  begin
    Nome  := 'usuario';
    Valor := Usuario;
  end;
  //
  ResulCode := Rest_Get('wordser/historico/maximo', trtJson, ParUsuario,
               Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.VerificaNovaVersaoDesktop(Developer: Boolean; Arquivo,
  SerialKey, SerialNum: String; CodAplic, VersaoAtual: Int64;
  ForcaBaixa: Boolean; var Mensagem: String; SSL: Boolean = False): Boolean;
var
  ParArquivo, ParCodAplic, ParVersaoAtu, ParForcaBaixa, ParSerialKey,
  ParSerialNum: THttpParam;
  ForcaBaixaStr: String;
  ResulCode: Integer;
begin
  if not Ping(SSL) then //500 - Erro no servidor
  begin
    Mensagem := 'Falha ao verificar vers�o!';
    //
    Result := False;
    Exit;
  end;
  //
  if ForcaBaixa = True then
    ForcaBaixaStr := '1'
  else
    ForcaBaixaStr := '0';
  //
  with ParArquivo do
  begin
    Nome  := 'arquivo';
    Valor := Arquivo;
  end;
  //
  with ParCodAplic do
  begin
    Nome  := 'cod_aplic';
    Valor := Grl_Geral.FF0(CodAplic);
  end;
  //
  with ParVersaoAtu do
  begin
    Nome  := 'versao_atu';
    //Valor := Grl_Geral.FF0(VersaoAtual);
    Valor := Grl_Geral.FF0(VersaoAtual);
  end;
  //
  with ParForcaBaixa do
  begin
    Nome  := 'forca_baixa';
    Valor := ForcaBaixaStr;
  end;
  //
  with ParSerialKey do
  begin
    Nome  := 'serial_key';
    Valor := SerialKey;
  end;
  //
  with ParSerialNum do
  begin
    Nome  := 'serial_num';
    Valor := SerialNum;
  end;
  //
  ResulCode := Rest_Get('aplicativo/versao/desktop', trtJson,
               [ParArquivo, ParCodAplic, ParVersaoAtu, ParForcaBaixa,
               ParSerialKey, ParSerialNum], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.WOrdSerAtualizaRespons(Developer: Boolean;
  IdUsuarioSolicitante, Codigo, Respons, TipoUsuario: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParIdUsuarioSolicitante, ParCodigo, ParRespons, ParTipoUsuario: THttpParam;
  ResulCode: Integer;
begin
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParRespons do
  begin
    Nome  := 'respons';
    Valor := Grl_Geral.FF0(Respons);
  end;
  //
  with ParTipoUsuario do
  begin
    Nome  := 'tipoUsuario';
    Valor := Grl_Geral.FF0(TipoUsuario);
  end;
  //
  ResulCode := Rest_Post('wordser/responsavel/atualiza', trtJson,
               [ParIdUsuarioSolicitante, ParCodigo, ParRespons, ParTipoUsuario],
               Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.WOrdSerAtualizaStatus(Developer: Boolean;
  IdUsuarioSolicitante, Codigo, Status, TipoUsuariovar: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParIdUsuarioSolicitante, ParCodigo, ParStatus, ParTipoUsuariovar: THttpParam;
  ResulCode: Integer;
begin
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParStatus do
  begin
    Nome  := 'status';
    Valor := Grl_Geral.FF0(Status);
  end;
  //
  with ParTipoUsuariovar do
  begin
    Nome  := 'tipoUsuariovar';
    Valor := Grl_Geral.FF0(TipoUsuariovar);
  end;
  //
  ResulCode := Rest_Post('wordser/status/atualiza', trtJson,
               [ParIdUsuarioSolicitante, ParCodigo, ParStatus,
               ParTipoUsuariovar], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.WOrdSerEnc(Developer: Boolean; IdUsuarioSolicitante,
  Codigo, Status: Integer; DataHoraTimeZone: String; Solucao: Integer;
  MotivReopen: String; var Mensagem: String; SSL: Boolean = False) : Boolean;
var
  ParIdUsuarioSolicitante, ParCodigo, ParStatus, ParDataHoraTimeZone,
  ParSolucao, ParMotivReopen: THttpParam;
  ResulCode: Integer;
begin
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParStatus do
  begin
    Nome  := 'status';
    Valor := Grl_Geral.FF0(Status);
  end;
  //
  with ParDataHoraTimeZone do
  begin
    Nome  := 'dataHoraTimeZone';
    Valor := DataHoraTimeZone;
  end;
  //
  with ParSolucao do
  begin
    Nome  := 'solucao';
    Valor := Grl_Geral.FF0(Solucao);
  end;
  //
  with ParMotivReopen do
  begin
    Nome  := 'motivReopen';
    Valor := MotivReopen;
  end;
  //
  ResulCode := Rest_Post('wordser/encerra', trtJson, [ParIdUsuarioSolicitante,
               ParCodigo, ParStatus, ParDataHoraTimeZone, ParSolucao,
               ParMotivReopen], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.WOrdSerInclui(Developer: Boolean; Codigo,
  IdUsuarioSolicitante: Integer; AberturaTimeZone: String; Cliente,
  Solicitante, Assunto: Integer; Descricao, Sistema, Impacto: String;
  Prioridade, Modo, Status, Aplicativo: Integer; Versao, Janela, JanelaRel,
  CompoRel, Tags: String; Responsavel, Grupo, EnvMailAbeAdm: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCodigo, ParIdUsuarioSolicitante, ParAberturaTimeZone, ParCliente,
  ParSolicitante, ParAssunto, ParDescricao, ParSistema, ParImpacto,
  ParPrioridade, ParModo, ParStatus, ParAplicativo, ParVersao, ParJanela,
  ParJanelaRel, ParCompoRel, ParTags, ParResponsavel, ParGrupo,
  ParEnvMailAbeAdm: THttpParam;
  ResulCode: Integer;
begin
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParAberturaTimeZone do
  begin
    Nome  := 'aberturaTimeZone';
    Valor := AberturaTimeZone;
  end;
  //
  with ParCliente do
  begin
    Nome  := 'cliente';
    Valor := Grl_Geral.FF0(Cliente);
  end;
  //
  with ParSolicitante do
  begin
    Nome  := 'solicitante';
    Valor := Grl_Geral.FF0(Solicitante);
  end;
  //
  with ParAssunto do
  begin
    Nome  := 'assunto';
    Valor := Grl_Geral.FF0(Assunto);
  end;
  //
  with ParDescricao do
  begin
    Nome  := 'descricao';
    Valor := Descricao;
  end;
  //
  with ParSistema do
  begin
    Nome  := 'sistema';
    Valor := Sistema;
  end;
  //
  with ParImpacto do
  begin
    Nome  := 'impacto';
    Valor := Impacto;
  end;
  //
  with ParPrioridade do
  begin
    Nome  := 'prioridade';
    Valor := Grl_Geral.FF0(Prioridade);
  end;
  //
  with ParModo do
  begin
    Nome  := 'modo';
    Valor := Grl_Geral.FF0(Modo);
  end;
  //
  with ParStatus do
  begin
    Nome  := 'status';
    Valor := Grl_Geral.FF0(Status);
  end;
  //
  with ParAplicativo do
  begin
    Nome  := 'aplicativo';
    Valor := Grl_Geral.FF0(Aplicativo);
  end;
  //
  with ParVersao do
  begin
    Nome  := 'versao';
    Valor := Versao;
  end;
  //
  with ParJanela do
  begin
    Nome  := 'janela';
    Valor := Janela;
  end;
  //
  with ParJanelaRel do
  begin
    Nome  := 'janelaRel';
    Valor := JanelaRel;
  end;
  //
  with ParCompoRel do
  begin
    Nome  := 'compoRel';
    Valor := CompoRel;
  end;
  //
  with ParTags do
  begin
    Nome  := 'tags';
    Valor := Tags;
  end;
  //
  with ParResponsavel do
  begin
    Nome  := 'responsavel';
    Valor := Grl_Geral.FF0(Responsavel);
  end;
  //
  with ParGrupo do
  begin
    Nome  := 'grupo';
    Valor := Grl_Geral.FF0(Grupo);
  end;
  //
  with ParEnvMailAbeAdm do
  begin
    Nome  := 'envMailAbeAdm';
    Valor := Grl_Geral.FF0(EnvMailAbeAdm);
  end;
  //
  ResulCode := Rest_Post('wordser/inclui', trtJson, [ParCodigo,
               ParIdUsuarioSolicitante, ParAberturaTimeZone, ParCliente,
               ParSolicitante, ParAssunto, ParDescricao, ParSistema, ParImpacto,
               ParPrioridade, ParModo, ParStatus, ParAplicativo, ParVersao,
               ParJanela, ParJanelaRel, ParCompoRel, ParTags, ParResponsavel,
               ParGrupo, ParEnvMailAbeAdm], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.WOrdSerIncluiCom(Developer: Boolean;
  IdUsuarioSolicitante, Codigo, Controle, Entidade, Para: Integer;
  ComRes: Integer; Nome: String; EnvMail: Integer; var Mensagem: String;
  SSL: Boolean): Boolean;
var
  ParIdUsuarioSolicitante, ParCodigo, ParControle, ParEntidade, ParPara,
  ParComRes, ParNome, ParEnvMail: THttpParam;
  ResulCode: Integer;
begin
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParControle do
  begin
    Nome  := 'controle';
    Valor := Grl_Geral.FF0(Controle);
  end;
  //
  with ParEntidade do
  begin
    Nome  := 'entidade';
    Valor := Grl_Geral.FF0(Entidade);
  end;
  //
  with ParPara do
  begin
    Nome  := 'para';
    Valor := Grl_Geral.FF0(Para);
  end;
  //
  with ParComRes do
  begin
    Nome  := 'comRes';
    Valor := Grl_Geral.FF0(ComRes);
  end;
  //
  with ParNome do
  begin
    Nome  := 'nome';
    Valor := Nome;
  end;
  //
  with ParEnvMail do
  begin
    Nome  := 'envMail';
    Valor := Grl_Geral.FF0(EnvMail);
  end;
  //
  ResulCode := Rest_Post('wordser/comentario/inclui', trtJson,
               [ParIdUsuarioSolicitante, ParCodigo, ParControle, ParEntidade,
               ParPara, ParComRes, ParNome, ParEnvMail], Mensagem, Developer,
               '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.AtualizaHashTermo(Developer: Boolean; CodUser: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCodUser: THttpParam;
  ResulCode: Integer;
begin
  with ParCodUser do
  begin
    Nome  := 'cod_user';
    Valor := Grl_Geral.FF0(CodUser);
  end;
  //
  ResulCode := Rest_Post('textos/termo/hash/configura', trtJson, [ParCodUser],
               Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ConfiguraHashTermo(Developer: Boolean; CodUser,
  Tipo: Integer; var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCodUser, ParTipo: THttpParam;
  ResulCode: Integer;
begin
  with ParCodUser do
  begin
    Nome  := 'cod_user';
    Valor := Grl_Geral.FF0(CodUser);
  end;
  //
  with ParTipo do
  begin
    Nome  := 'tipo';
    Valor := Grl_Geral.FF0(Tipo);
  end;
  //
  ResulCode := Rest_Get('textos/termo/hash/atualiza', trtJson, [ParCodUser,
               ParTipo], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.EnviaEmailWOrdSer(Developer: Boolean;
  IdUsuarioSolicitante, Tipo, Codigo, Controle: Integer; var Mensagem: String;
  SSL: Boolean): Boolean;
var
  ParIdUsuarioSolicitante, ParTipo, ParCodigo, ParControle: THttpParam;
  ResulCode: Integer;
begin
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParTipo do
  begin
    Nome  := 'tipo';
    Valor := Grl_Geral.FF0(Tipo);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParControle do
  begin
    Nome  := 'controle';
    Valor := Grl_Geral.FF0(Controle);
  end;
  //
  ResulCode := Rest_Post('wordser/email/envia', trtJson, [ParIdUsuarioSolicitante,
                 ParTipo, ParCodigo, ParControle], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.InsereWOrdSerArq(Developer: Boolean;
  IdUsuarioSolicitante, Codigo, FtpWebArq: Integer; var Mensagem: String;
  SSL: Boolean): Boolean;
var
  ParIdUsuarioSolicitante, ParCodigo, ParFtpWebArq: THttpParam;
  ResulCode: Integer;
begin
  with ParIdUsuarioSolicitante do
  begin
    Nome  := 'idUsuarioSolicitante';
    Valor := Grl_Geral.FF0(IdUsuarioSolicitante);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParFtpWebArq do
  begin
    Nome  := 'ftpWebArq';
    Valor := Grl_Geral.FF0(FtpWebArq);
  end;
  //
  ResulCode := Rest_Post('wordser/arquivos/inclui', trtJson,
               [ParIdUsuarioSolicitante, ParCodigo, ParFtpWebArq], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.InsUpdRegistroFTPArquivo(Developer: Boolean;
  TipSQL: String; CliInt, Depto, Nivel, FtpDir: Integer; Tam, Nome,
  ArqNome: String; Origem, Ativo, Codigo: Integer; var Mensagem: String;
  SSL: Boolean): Boolean;
var
  ParTipSQL, ParCliInt, ParDepto, ParNivel, ParFtpDir, ParTam, ParNome,
  ParArqNome, ParOrigem, ParAtivo, ParCodigo: THttpParam;
  ResulCode: Integer;
begin
  with ParTipSQL do
  begin
    Nome  := 'tipSQL';
    Valor := TipSQL;
  end;
  //
  with ParCliInt do
  begin
    Nome  := 'cliInt';
    Valor := Grl_Geral.FF0(CliInt);
  end;
  //
  with ParDepto do
  begin
    Nome  := 'depto';
    Valor := Grl_Geral.FF0(Depto);
  end;
  //
  with ParNivel do
  begin
    Nome  := 'nivel';
    Valor := Grl_Geral.FF0(Nivel);
  end;
  //
  with ParFtpDir do
  begin
    Nome  := 'ftpDir';
    Valor := Grl_Geral.FF0(FtpDir);
  end;
  //
  with ParTam do
  begin
    Nome  := 'tam';
    Valor := Tam;
  end;
  //
  with ParNome do
  begin
    Nome  := 'nome';
    Valor := Nome;
  end;
  //
  with ParArqNome do
  begin
    Nome  := 'arqNome';
    Valor := ArqNome;
  end;
  //
  with ParOrigem do
  begin
    Nome  := 'origem';
    Valor := Grl_Geral.FF0(Origem);
  end;
  //
  with ParAtivo do
  begin
    Nome  := 'ativo';
    Valor := Grl_Geral.FF0(Ativo);
  end;
  //
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  ResulCode := Rest_Post('ftp/arquivo/atualiza', trtJson, [ParTipSQL,
               ParCliInt, ParDepto, ParNivel, ParFtpDir, ParTam, ParNome,
               ParArqNome, ParOrigem, ParAtivo, ParCodigo], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ObtemDadosCep(Developer: Boolean; Cep: String;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCepConfig: THttpParam;
  ResulCode: Integer;
begin
  with ParCepConfig do
  begin
    Nome  := 'cep';
    Valor := Cep;
  end;
  //
  ResulCode := Rest_Get('servicos/cep', trtJson, [ParCepConfig], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ObtemDadosCnpj(Developer: Boolean; Cnpj: String;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCnpjConfig: THttpParam;
  ResulCode: Integer;
begin
  with ParCnpjConfig do
  begin
    Nome  := 'cnpj';
    Valor := Cnpj;
  end;
  //
  ResulCode := Rest_Get('servicos/cnpj', trtJson, [ParCnpjConfig], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ObtemDadosFTP(Developer: Boolean; FtpConfig: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParFtpConfig: THttpParam;
  ResulCode: Integer;
begin
  with ParFtpConfig do
  begin
    Nome  := 'ftpConfig';
    Valor := Grl_Geral.FF0(FtpConfig);
  end;
  //
  ResulCode := Rest_Get('ftp/obter', trtJson, [ParFtpConfig], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ObtemDadosFTPDir(Developer: Boolean; Diretorio: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParDiretorio: THttpParam;
  ResulCode: Integer;
begin
  with ParDiretorio do
  begin
    Nome  := 'diretorio';
    Valor := Grl_Geral.FF0(Diretorio);
  end;
  //
  ResulCode := Rest_Get('ftp/diretorio/obter', trtJson, [ParDiretorio], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.ObtemNomeUsuario(Developer: Boolean; Codigo: Integer;
  var Mensagem: String; SSL: Boolean): Boolean;
var
  ParCodigo: THttpParam;
  ResulCode: Integer;
begin
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  ResulCode := Rest_Get('usuario/nome', trtJson, [ParCodigo], Mensagem,
               Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.Ping(SSL: Boolean = False): Boolean;
const
  CO_TIMEOUT = 1; //1 segundo
var
  ResulCode: Integer;
  Mensagem: String;
begin
  ResulCode := Rest_Get('ping', trtJson, [], Mensagem, False, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.InsUpdRegistroFTPDiretorio(Developer: Boolean; Codigo,
  FtpConfig, TipoDir, CliInt, Depto, Nivel: Integer; Nome, Pasta,
  Caminho: String; var Mensagem: String; SSL: Boolean = False): Boolean;
var
  ParCodigo, ParFtpConfig, ParTipoDir, ParCliInt, ParDepto, ParNivel, ParNome,
  ParPasta, ParCaminho: THttpParam;
  ResulCode: Integer;
begin
  with ParCodigo do
  begin
    Nome  := 'codigo';
    Valor := Grl_Geral.FF0(Codigo);
  end;
  //
  with ParFtpConfig do
  begin
    Nome  := 'ftpConfig';
    Valor := Grl_Geral.FF0(FtpConfig);
  end;
  //
  with ParTipoDir do
  begin
    Nome  := 'tipoDir';
    Valor := Grl_Geral.FF0(TipoDir);
  end;
  //
  with ParCliInt do
  begin
    Nome  := 'cliInt';
    Valor := Grl_Geral.FF0(CliInt);
  end;
  //
  with ParDepto do
  begin
    Nome  := 'depto';
    Valor := Grl_Geral.FF0(Depto);
  end;
  //
  with ParNivel do
  begin
    Nome  := 'nivel';
    Valor := Grl_Geral.FF0(Nivel);
  end;
  //
  with ParNome do
  begin
    Nome  := 'nome';
    Valor := Nome;
  end;
  //
  with ParPasta do
  begin
    Nome  := 'pasta';
    Valor := Pasta;
  end;
  //
  with ParCaminho do
  begin
    Nome  := 'caminho';
    Valor := Caminho;
  end;
  //
  ResulCode := Rest_Post('ftp/diretorio/atualiza', trtJson, [ParCodigo,
               ParFtpConfig, ParTipoDir, ParCliInt, ParDepto, ParNivel, ParNome,
               ParPasta, ParCaminho], Mensagem, Developer, '', SSL);
  //
  Result := SetReturn(ResulCode);
end;

function TUnGrl_DmkREST.UnLockAplic(Developer: Boolean; SerialKey, SerialNum,
  CNPJ, DescriPC, NomePC, IP, VersaoAtu: String; Servidor, CodAplic: Integer;
  AtualizaDescriPC: Boolean; OSVersion, DBVersion, IPServ: String;
  var Vencimento: String; var Status: String; var CliInt: Integer;
  var AtualizadoEm: String; var Mensagem: String; SSL: Boolean;
  TimeOut: Integer): Integer;
var
  ParCNPJ, ParSerialKey, ParSerialNum, ParCNPJCliente, ParDescriPC, ParNomePC,
  ParIP, ParVersaoAtu, ParServidor, ParCodAplic, ParAtualizaDescriPC,
  ParOSVersion, ParDBVersion, ParIPServ: THttpParam;
  AtualizaDescriPCStr, Resul: String;
  ResulCode: Integer;
  Json: TJSONObject;
begin
  if not Ping(SSL) then //500 - Erro no servidor
  begin
    Mensagem := 'API Dermatek Offline!';
    //
    Result := 500;
    Exit;
  end;
  //
  if AtualizaDescriPC then
    AtualizaDescriPCStr := '1'
  else
    AtualizaDescriPCStr := '0';
  //
  with ParCNPJ do
  begin
    Nome  := 'cnpj';
    Valor := CO_Dmk_CNPJ;
  end;
  //
  with ParSerialKey do
  begin
    Nome  := 'serialKey';
    Valor := SerialKey;
  end;
  //
  with ParSerialNum do
  begin
    Nome  := 'serialNum';
    Valor := SerialNum;
  end;
  //
  with ParCNPJCliente do
  begin
    Nome  := 'cnpj_cliente';
    Valor := CNPJ;
  end;
  //
  with ParDescriPC do
  begin
    Nome  := 'descriPC';
    Valor := DescriPC;
  end;
  //
  with ParNomePC do
  begin
    Nome  := 'nomePC';
    Valor := NomePC;
  end;
  //
  with ParIP do
  begin
    Nome  := 'ip';
    Valor := IP;
  end;
  //
  with ParVersaoAtu do
  begin
    Nome  := 'versaoAtu';
    Valor := VersaoAtu;
  end;
  //
  with ParServidor do
  begin
    Nome  := 'servidor';
    Valor := Grl_Geral.FF0(Servidor);
  end;
  //
  with ParCodAplic do
  begin
    Nome  := 'codAplic';
    Valor := Grl_Geral.FF0(CodAplic);
  end;
  //
  with ParAtualizaDescriPC do
  begin
    Nome  := 'atualizaDescriPC';
    Valor := AtualizaDescriPCStr;
  end;
  //
  with ParOSVersion do
  begin
    Nome  := 'osVersion';
    Valor := OSVersion;
  end;
  //
  with ParDBVersion do
  begin
    Nome  := 'dbVersion';
    Valor := DBVersion;
  end;
  //
  with ParIPServ do
  begin
    Nome  := 'ipServ';
    Valor := IPServ;
  end;
  //
  ResulCode := Rest_Post('valida/licenca/desktop', trtJson, [ParCNPJ,
               ParSerialKey, ParSerialNum, ParCNPJCliente, ParDescriPC,
               ParNomePC, ParIP, ParVersaoAtu, ParServidor, ParCodAplic,
               ParAtualizaDescriPC, ParOSVersion, ParIPServ], Resul, Developer,
               '', SSL, TimeOut);
  //
  if ResulCode = 200 then //200 - Sucesso
  begin
    Json := JsonSingleToJsonObject(Resul);
    //
    Vencimento   := GetJsonValue(Json, 'vencimento');
    Status       := GetJsonValue(Json, 'status');
    CliInt       := Grl_Geral.IMV(GetJsonValue(Json, 'limite_cliint'));
    AtualizadoEm := GetJsonValue(Json, 'atualizado_em');
    //
    Result := ResulCode;
  end else //500 - Erro no servidor / Restante bloquear
  begin
    Mensagem := Resul;
    //
    Result := ResulCode;
  end;
end;

function TUnGrl_DmkREST.UnLockVendaApp(Developer: Boolean; SerialKey, PIN,
  CNPJ, NomePC, IP, VersaoAtu: String; CodAplic: Integer; OSVersion: String;
  var Vencimento, Status: String; var CliInt: Integer; var AtualizadoEm,
  Mensagem: String; SSL: Boolean; TimeOut: Integer): Boolean;
var
  ParCNPJ, ParSerialKey, ParPin, ParCNPJCliente, ParNomePC, ParIP, ParVersaoAtu,
  ParCodAplic, ParOSVersion: THttpParam;
  Resul: String;
  ResulCode: Integer;
  Json: TJSONObject;
begin
  with ParCNPJ do
  begin
    Nome  := 'cnpj';
    Valor := CO_Dmk_CNPJ;
  end;
  //
  with ParSerialKey do
  begin
    Nome  := 'serialKey';
    Valor := SerialKey;
  end;
  //
  with ParPin do
  begin
    Nome  := 'pin';
    Valor := Pin;
  end;
  //
  with ParCNPJCliente do
  begin
    Nome  := 'cnpj_cliente';
    Valor := CNPJ;
  end;
  //
  with ParNomePC do
  begin
    Nome  := 'nomePC';
    Valor := NomePC;
  end;
  //
  with ParIP do
  begin
    Nome  := 'ip';
    Valor := IP;
  end;
  //
  with ParVersaoAtu do
  begin
    Nome  := 'versaoAtu';
    Valor := VersaoAtu;
  end;
  //
  with ParCodAplic do
  begin
    Nome  := 'codAplic';
    Valor := Grl_Geral.FF0(CodAplic);
  end;
  //
  with ParOSVersion do
  begin
    Nome  := 'osVersion';
    Valor := OSVersion;
  end;
  ResulCode := Rest_Post('valida/licenca/venda', trtJson, [ParCNPJ, ParSerialKey,
               ParPin, ParCNPJCliente, ParNomePC, ParIP, ParVersaoAtu,
               ParCodAplic, ParOSVersion], Resul, Developer, '', SSL, TimeOut);
  //
  if ResulCode = 200 then //200 - Sucesso
  begin
    Json := JsonSingleToJsonObject(Resul);
    //
    Vencimento   := GetJsonValue(Json, 'vencimento');
    Status       := GetJsonValue(Json, 'status');
    CliInt       := Grl_Geral.IMV(GetJsonValue(Json, 'limite_cliint'));
    AtualizadoEm := GetJsonValue(Json, 'atualizado_em');
    //
    Result := True;
  end else
  begin //500 - Erro no servidor / Restante bloquear
    Mensagem := Resul;
    //
    if ResulCode = 500 then
    begin
      Grl_Geral.MB_Aviso(Mensagem);
      //
      Result := True;
    end else
      Result := False;
  end;
end;

function TUnGrl_DmkREST.Rest_Get(Uri: String; TipResp: TRestRespTip;
  Parametros: array of THttpParam; var Resultado: String;
  Developer: Boolean = False; Token: String = '';
  SSL: Boolean = True; TimeOut: Integer = 0): Integer;
var
  HeaderParams: array of THttpHeader;
  URL: String;
begin
  SetLength(HeaderParams, 2);
  //
  if not Rest_Obtem_Header(Token, Uri, TipResp, URL, HeaderParams, Developer, SSL) then
    Exit;
  //
  {$IfDef DMK_FMX}
  Result := FMX_dmkHTML2.HttpGetURL(URL, HeaderParams, Parametros, Resultado, TimeOut);
  {$Else}
  Result := dmkHTML2.HttpGetURL(URL, HeaderParams, Parametros, Resultado, TimeOut);
  {$EndIf}
end;

function TUnGrl_DmkREST.Rest_Post(Uri: String; TipResp: TRestRespTip;
  Parametros: array of THttpParam; var Resultado: String;
  Developer: Boolean = False; Token: String = ''; SSL: Boolean = True;
  TimeOut: Integer = 0): Integer;
var
  HeaderParams: array of THttpHeader;
  URL: String;
begin
  SetLength(HeaderParams, 2);
  //
  if not Rest_Obtem_Header(Token, Uri, TipResp, URL, HeaderParams, Developer, SSL) then
    Exit;
  //
  {$IfDef DMK_FMX}
  Result := FMX_dmkHTML2.HttpPostURL(URL, HeaderParams, Parametros, Resultado, TimeOut);
  {$Else}
  Result := dmkHTML2.HttpPostURL(URL, HeaderParams, Parametros, Resultado, TimeOut);
  {$EndIf}
end;

function TUnGrl_DmkREST.GetJsonValue(JSONObj: TJSONObject; Param: String): String;
begin
 Result := JSONObj.Get(Param).JSONValue.Value;
end;

function TUnGrl_DmkREST.JsonSingleToJsonObject(JSON: String): TJSONObject;
var
  JSONVal: TJSONValue;
begin
  JSONVal := TJSONObject.ParseJSONValue(JSON);
  Result  := JSONVal as TJSONObject;
end;

///////////////////////////////////////////////////////////// PRIVATE //////////
function TUnGrl_DmkREST.SetReturn(ResulCode: Integer): Boolean;
begin
  if ResulCode = 200 then
    Result := True
  else
    Result := False;
end;

function TUnGrl_DmkREST.Rest_GetURL(Developer: Boolean; SSL: Boolean = True): String;
var
  Protocol: String;
begin
  if SSL then
    Protocol := 'https://'
  else
    Protocol := 'http://';
  //
  if Developer = True then
    Result := Protocol + CO_Dmk_URL_DEV
  else
    Result := Protocol + CO_Dmk_URL;
end;

function TUnGrl_DmkREST.Rest_Obtem_Header(Token, Uri: String;
  TipResp: TRestRespTip; var URL: String; var HeaderParams: array of THttpHeader;
  Developer: Boolean; SSL: Boolean = True): Boolean;
var
  Output: String;
  HeaderToken, HeaderOutputType: THttpHeader;
begin
  Result := False;
  //
  Output := Rest_TRestRespTipToString(TipResp);
  URL  := Rest_GetURL(Developer, SSL) + Uri;
  //
  HeaderToken.Nome  := 'token';
  //
  if Token <> '' then
    HeaderToken.Valor := Token
  else
    HeaderToken.Valor := CO_Dmk_Token;
  //
  if Grl_Geral.FIC(HeaderToken.Valor  = '', nil, 'Token n�o informado!')  then Exit;
  //
  HeaderOutputType.Nome  := 'outputtype';
  HeaderOutputType.Valor := Output;
  //
  HeaderParams[0] := HeaderToken;
  HeaderParams[1] := HeaderOutputType;
  //
  Result := True;
end;

function TUnGrl_DmkREST.Rest_TRestRespTipToString(TipResp: TRestRespTip): String;
begin
  case TipResp of
    trtJSON:
      Result := 'json';
    trtCSV:
      Result := 'csv';
    trtXML:
      Result := 'xml';
  end;
end;

end.
