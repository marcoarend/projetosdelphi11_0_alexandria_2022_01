unit UnGrl_Sincro;

interface

uses System.Classes, System.SysUtils, System.WideStrUtils, Data.DB,
  Xml.XMLIntf, Xml.XMLDoc,
  {$IfDef DMK_FMX}
  FireDAC.Comp.Client,
  FMX.Forms, FMX.Controls,
  {$Else}
  Vcl.Forms, Vcl.Controls,
  {$IfDef DB_SQLite}
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset,
  {$Else}
  mySQLDbTables,
  {$EndIf}
  {$EndIf}
  UnDmkEnums, UnGrl_DmkDB;

type
  TUnGrl_Sincro = class(TObject)
  private
    procedure SalvaXMLSincro(XML: IXMLDocument; Funcao, Tabela: String);
    procedure SalvaLog(Log, Tabela: String);
    function  LocalizaValorArray(ArrayFilds: array of string; Valor: String): Boolean;
    function  EnviaXMLToWeb(Device: TDeviceType; DataBase: TComponent;
              QueryUpd: TDataSet; Tabela, Token, Client: String; Acao: TLastAcao;
              XMLArq: IXMLDocument; var Msg, Log: String): Boolean;
    function  ExecutSincronismo_Upload(Device: TDeviceType; DataBase: TComponent;
              QueryUpd, QueryAux: TDataSet; Tabela, Token, Client: String;
              Acao: TLastAcao; CamposIndex: array of string; var Msg, Log: String): Boolean;
    function  ExecutaSincronismo_Download(DataBase: TComponent;
              QueryUpd: TDataSet; Tabela, Token, Client: String;
              DataHoraUTC: TDateTime; Acao: TLastAcao; Device: TDeviceType;
              var Msg, Log: String): Boolean;
  public
    function  SincronizaTabela(Device: TDeviceType; DataBase: TComponent; QueryUpd,
              QueryAux: TDataSet; Tabela: String; CamposIndex: array of string;
              var Msg: String): Boolean;
  end;

var
  Grl_Sincro: TUnGrl_Sincro;
const
  MAX_REGS: Integer = 50;

implementation

uses UnGrl_Geral, dmkGeral, UnGrlUsuarios, UnGrl_REST, MyListas;

{ TUnGrl_Sincro }

procedure TUnGrl_Sincro.SalvaXMLSincro(XML: IXMLDocument; Funcao, Tabela: String);
var
  XMLStr: String;
begin
  if CO_Dmk_Rest_Beta then
  begin
    XML.SaveToXML(XMLStr);
    //
    Grl_Geral.SalvaArquivoEmDocumentos(XMLStr, 'DmkSincro_' + Funcao + '_' +
      Tabela, 'xml');
  end;
end;

procedure TUnGrl_Sincro.SalvaLog(Log, Tabela: String);
begin
  if CO_Dmk_Rest_Beta then
    Grl_Geral.SalvaArquivoEmDocumentos(Log, 'DmkSincro_LogSincro_' + Tabela,
      'txt');
end;

function TUnGrl_Sincro.EnviaXMLToWeb(Device: TDeviceType; DataBase: TComponent;
  QueryUpd: TDataSet; Tabela, Token, Client: String; Acao: TLastAcao;
  XMLArq: IXMLDocument; var Msg, Log: String): Boolean;
var
  AlterWeb, i, Res: Integer;
  XMLEnv_Txt, Funcao, Resul: String;
  XMLRes, XMLEnv: TStringStream;
  XMLArqRes: IXMLDocument;
  ParamXml, ParamTabela: THttpParam;
begin
  Result := False;
  XMLRes := TStringStream.Create('');
  XMLEnv := TStringStream.Create('');
  //
  XMLArq.SaveToXML(XMLEnv_Txt);
  //
  XMLEnv_Txt := StringReplace(XMLEnv_Txt, #13, '', [rfReplaceAll]);
  XMLEnv_Txt := StringReplace(XMLEnv_Txt, #10, '', [rfReplaceAll]);
  //
  try
    case Acao of
      laIns:
        Funcao := 'upload_xml_ins';
      laUpd:
        Funcao := 'upload_xml_upd';
      laDel:
        Funcao := 'upload_xml_del';
    end;
    Log := Log + sLineBreak + sLineBreak + 'XML enviado = ' + XMLEnv_Txt;
    //
    ParamXml.Nome         := 'xml';
    ParamXml.Valor        := XMLEnv_Txt;
    ParamXml.ACharse      := 'utf-8';
    ParamXml.AContentType := 'text/xml';
    //
    ParamTabela.Nome         := 'tabela';
    ParamTabela.Valor        := Tabela;
    ParamTabela.ACharse      := '';
    ParamTabela.AContentType := '';

    SalvaXMLSincro(XMLArq, Funcao, Tabela);

    Res := Grl_REST.Rest_Dermatek_Post('sincro', Funcao, trtXML,
            [ParamXml, ParamTabela], Resul, Token, Client);
    //
    if Res <> 0 then
    begin
      Log := Log + sLineBreak + sLineBreak + 'Resposta = ' + Resul;
      //
      if Res = 200 then
      begin
        AlterWeb := Integer(Grl_Geral.ObtemAlterWebDeDeviceType(Device));
        //
        Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [
          'DELETE FROM ' + Tabela,
          'WHERE AlterWeb=' + Grl_Geral.FF0(AlterWeb),
          'AND LastAcao=' + Grl_Geral.FF0(Integer(Acao)),
          '']);
        //
        Result := True;
      end else
        Msg := Msg + sLineBreak + 'Falha ao sincronizar dados!' +
                 sLineBreak + 'Motivo: ' + Resul;
    end else
      Msg := Msg + sLineBreak + 'Falha ao fazer requisi��o!';
    XMLRes.Free;
    XMLEnv.Free;
  except
    XMLRes.Free;
    XMLEnv.Free;
  end;
end;

function TUnGrl_Sincro.ExecutaSincronismo_Download(DataBase: TComponent;
  QueryUpd: TDataSet; Tabela, Token, Client: String; DataHoraUTC: TDateTime;
  Acao: TLastAcao; Device: TDeviceType; var Msg, Log: String): Boolean;
var
  DtaTime, DtaTimeTb, SQL, SQLWhere, SQLFld, SQLVal, SQLFldVal, XMLNode, XMLVal,
  Funcao, Resul: String;
  Res, i, j: Integer;
  XMLArqRes: IXMLDocument;
  ParamTabela, ParamDataHoraUtc: THttpParam;
  Tot: Integer;
begin
  if Acao = laUpd then
  begin
    Grl_Geral.MB_Aviso('A��o n�o implementada');
    Exit;
  end;
  //
  Result    := False;
  DtaTime   := Grl_Geral.FDT(DataHoraUTC, 109);
  DtaTimeTb := Grl_Geral.FDT(Grl_DmkDB.ObtemDataSincro(DataBase, Tabela), 109);
  //
  Log := Log + sLineBreak + sLineBreak + 'Data hora UTC = ' + DtaTime +
           sLineBreak + 'Data �ltimo sincronismo = ' + DtaTimeTb;
  //
  if Acao = laIns then
    Funcao := 'download'
  else
    Funcao := 'download_del';
  //
  Log := Log + sLineBreak + sLineBreak + 'Fun��o = ' + Funcao;
  //
  ParamTabela.Nome         := 'tabela';
  ParamTabela.Valor        := Tabela;
  ParamTabela.ACharse      := '';
  ParamTabela.AContentType := '';
  //
  ParamDataHoraUtc.Nome         := 'datahorautc';
  ParamDataHoraUtc.Valor        := DtaTimeTb;
  ParamDataHoraUtc.ACharse      := '';
  ParamDataHoraUtc.AContentType := '';
  //
  Res := Grl_REST.Rest_Dermatek_Post('sincro', Funcao, trtXML,
          [ParamTabela, ParamDataHoraUtc], Resul, Token, Client);
  //
  if Res <> 0 then
  begin
    XMLArqRes := TXMLDocument.Create(nil);
    XMLArqRes.Active := False;
    try
      Log := Log + sLineBreak + sLineBreak + 'Resposta = ' + Resul;
      //
      if (Res <> 200) then //Sem registros para retornar
      begin
        Msg := Msg + sLineBreak + Resul;
      end else
      if (Res = 200) and (Resul <> '') then
      begin
        XMLArqRes.LoadFromXML(Resul);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        SalvaXMLSincro(XMLArqRes, Funcao, Tabela);
        //
        for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLArqRes.DocumentElement.ChildNodes[i] do
          begin
            if Acao = laIns then
            begin
              SQLFld    := '';
              SQLVal    := '';
              SQLFldVal := '';
              //
              Tot := ChildNodes.Count;
              //
              if Tot > 0 then
              begin
                for j := 0 to ChildNodes.Count - 1 do
                begin
                  XMLNode := ChildNodes[j].ChildNodes[0].NodeName;
                  XMLVal  := ChildNodes[j].ChildNodes[0].Text;
                  //
                  if IsUTF8String(XMLVal) then
                    XMLVal := Utf8ToAnsi(XMLVal);
                  //
                  XMLVal := Copy(XMLVal, 2, Length(XMLVal) - 2);
                  XMLVal := StringReplace(XMLVal, '"', '''', [rfReplaceAll]);
                  XMLVal := '"' + XMLVal + '"';
                  //
                  if j = ChildNodes.Count - 1 then
                  begin
                    SQLFld    := SQLFld + XMLNode;
                    SQLVal    := SQLVal + XMLVal;
                    SQLFldVal := SQLFldVal + XMLNode + '=' + XMLVal + '';
                  end else
                  begin
                    SQLFld    := SQLFld + XMLNode + ',';
                    SQLVal    := SQLVal + XMLVal + ',';
                    SQLFldVal := SQLFldVal + XMLNode + '=' + XMLVal + ',';
                  end;
                end;
                if (SQLFld <> '') and (SQLVal <> '') and (SQLFldVal <> '') then
                begin
                  {$IfDef DMK_FMX}
                  SQL := 'INSERT OR REPLACE INTO ' + Tabela + ' (' + SQLFld +
                           ') VALUES (' + SQLVal + ')';
                  {$Else}
                  {$IfDef DB_SQLite}
                  SQL := 'INSERT OR REPLACE INTO ' + Tabela + ' (' + SQLFld +
                           ') VALUES (' + SQLVal + ')';
                  {$Else}
                  //MySQL
                  SQL := 'INSERT INTO ' + Tabela + ' (' + SQLFld + ') VALUES (' +
                           SQLVal + ') ON DUPLICATE KEY UPDATE ' + SQLFldVal;
                  {$EndIf}
                  {$EndIf}
                  //
                  if not Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [SQL]) then
                  begin
                    Msg := Msg + sLineBreak + 'Falha ao executar a "Query"!';
                    Exit;
                  end;
                end else
                begin
                  Msg := Msg + sLineBreak + 'O XML retornado � inv�lido!';
                  Exit;
                end;
              end;
            end else
            begin
              SQL      := 'DELETE FROM ' + Tabela;
              SQLWhere := '';
              //
              Tot := ChildNodes.Count;
              //
              if Tot > 0 then
              begin
                for j := 0 to ChildNodes.Count - 1 do
                begin
                  XMLNode := ChildNodes[j].ChildNodes[0].NodeName;
                  XMLVal  := ChildNodes[j].ChildNodes[0].Text;
                  //
                  if (XMLNode <> '') and (XMLVal <> '') then
                  begin
                    if j = 0 then
                      SQLWhere := ' WHERE ' + XMLNode + ' = ' + XMLVal
                    else
                      SQLWhere := SQLWhere + ' AND ' + XMLNode + ' = ' + XMLVal;
                  end;
                end;
                if SQLWhere <> '' then
                begin
                  if not Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [SQL, SQLWhere]) then
                  begin
                    Msg := Msg + sLineBreak + 'Falha ao executar a "Query"!';
                    Exit;
                  end;
                end else
                begin
                  Msg := Msg + sLineBreak + 'O XML retornado � inv�lido!';
                  Exit;
                end;
              end;
            end;
          end;
        end;
      end else
        Msg := ''; //N�o tem dados para baixar
    finally
      ;
    end;
  end;
  if Msg = '' then
  begin
    Result := True;
  end;
end;

function TUnGrl_Sincro.LocalizaValorArray(ArrayFilds: array of string;
  Valor: String): Boolean;
var
  k: Integer;
begin
  Result := False;
  //
  for k := Low(ArrayFilds) to High(ArrayFilds) do
  begin
    if ArrayFilds[k] = Valor then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TUnGrl_Sincro.ExecutSincronismo_Upload(Device: TDeviceType;
  DataBase: TComponent; QueryUpd, QueryAux: TDataSet; Tabela, Token,
  Client: String; Acao: TLastAcao; CamposIndex: array of string; var Msg,
  Log: String): Boolean;
var
  XMLArq: IXMLDocument;
  NodeReg0, NodeVal0, NodeReg1, NodeReg, NodeVal, NodeKReg: IXMLNode;
  AlterWeb, i, j: Integer;
  Campo, Valor, SQLCompl: String;
begin
  Result := False;
  //
  XMLArq := TXMLDocument.Create(nil);
  XMLArq.Active := False;
  //
  try
    AlterWeb := Integer(Grl_Geral.ObtemAlterWebDeDeviceType(Device));
    //
    if Acao = laDel then
      SQLCompl := ''
    else
      SQLCompl := 'AND AlterWeb=' + Grl_Geral.FF0(AlterWeb);
    //
    Grl_DmkDB.AbreSQLQuery0(QueryAux, DataBase, [
      'SELECT * ',
      'FROM ' + Tabela,
      'WHERE CliIntSync=' + Grl_Geral.FF0(VAR_API_ID_ENTIDADE),
      SQLCompl,
      'AND LastAcao=' + Grl_Geral.FF0(Integer(Acao)),
      '']);
    //
    {$IfDef DMK_FMX}
    Log := Log + sLineBreak + sLineBreak + TFDQuery(QueryAux).SQL.Text;
    {$Else}
    {$IfDef DB_SQLite}
    Log := Log + sLineBreak + sLineBreak + TZQuery(QueryAux).SQL.Text;
    {$Else}
    Log := Log + sLineBreak + sLineBreak + TMySQLQuery(QueryAux).SQL.Text;
    {$EndIf}
    {$EndIf}
    //
    if QueryAux.RecordCount > 0 then
    begin
      i := 0;
      //
      while not QueryAux.Eof do
      begin
        if i = 0 then
        begin
          XMLArq.Active          := True;
          XMLArq.Version         := '1.0';
          XMLArq.Encoding        := 'utf-8';
          XMLArq.ParseOptions    := [];
          XMLArq.Options         := [];
          XMLArq.DocumentElement := XMLArq.CreateNode(Tabela, ntElement, '');
          //
          NodeReg0 := XMLArq.DocumentElement.AddChild('fld', -1);
        end;
        for j := 0 to QueryAux.Fields.Count -1 do
        begin
          Campo := QueryAux.Fields[j].FieldName;
          //
          if QueryAux.Fields[j].DataType = ftString then
            Valor := Grl_Geral.DmkUtf8Encode(Geral.VariavelToString(QueryAux.Fields[j].Value))
          else
            Valor := Geral.VariavelToString(QueryAux.Fields[j].Value);
          //
          //
          if i = 0 then
          begin
            NodeVal0      := NodeReg0.AddChild('nam', -1);
            NodeVal0.Text := Campo;
          end;
          if j = 0 then
          begin
            NodeReg := XMLArq.DocumentElement.AddChild('reg' + Grl_Geral.FF0(i + 1), -1);
          end;
          NodeVal      := NodeReg.AddChild('val', -1);
          NodeVal.Text := Valor;
        end;
        if ((i + 1) = MAX_REGS) or (QueryAux.RecordCount = (i + 1)) then
        begin
          Log := Log + sLineBreak + sLineBreak + XMLArq.XML.Text;
          //
          if not EnviaXMLToWeb(Device, DataBase, QueryUpd, Tabela, Token,
            Client, Acao, XMLArq, Msg, Log) then
          begin
            Msg := Msg + sLineBreak + 'Fun��o: "TUnGrl_Sincro.ExecutSincronismo_Upload"';
            Log := Log + sLineBreak + sLineBreak + Msg;
            Exit;
          end else
            Result := True;
          //
          i := 0;
        end else
          i := i + 1;
        //
        QueryAux.Next;
      end;
    end else
      Result := True;
  finally
    ;
  end;
end;

function TUnGrl_Sincro.SincronizaTabela(Device: TDeviceType; DataBase: TComponent;
  QueryUpd, QueryAux: TDataSet; Tabela: String; CamposIndex: array of string;
  var Msg: String): Boolean;
var
  DataHora: TDateTime;
  Log, DataHora_Str, Token, Client: string;
begin
  Result       := False;
  DataHora     := Grl_Geral.DateTime_MyTimeZoneToUTC(Now);
  DataHora_Str := Grl_Geral.FDT(DataHora, 109);
  Token        := VAR_API_TOKEN;
  Client       := Grl_Geral.FF0(VAR_API_ID_CLIENTE);
  //
  if VAR_API_ID_USUARIO = 0 then
  begin
    Grl_Geral.MB_Aviso('Usu�rio n�o identificado!');
    Exit;
  end;
  if Token = '' then
  begin
    Grl_Geral.MB_Aviso('Token n�o definido!');
    Exit;
  end;
  //No mobile procurar por sincro no gerenciador de arquivos para achar os arquivos de log
  //
  Log := Tabela + sLineBreak + Grl_Geral.FDT(DataHora, 2) + sLineBreak;
  //
  if not ExecutSincronismo_Upload(Device, DataBase, QueryUpd, QueryAux, Tabela,
    Token, Client, laIns, CamposIndex, Msg, Log) then
  begin
    if CO_Dmk_Rest_Beta then
      SalvaLog(Log, Tabela);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak +
      '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutSincronismo_Upload(Device, DataBase, QueryUpd, QueryAux, Tabela,
    Token, Client, laUpd, CamposIndex, Msg, Log) then
  begin
    if CO_Dmk_Rest_Beta then
      SalvaLog(Log, Tabela);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak +
      '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutSincronismo_Upload(Device, DataBase, QueryUpd, QueryAux, Tabela,
    Token, Client, laDel, CamposIndex, Msg, Log) then
  begin
    if CO_Dmk_Rest_Beta then
    SalvaLog(Log, Tabela);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak +
      '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutaSincronismo_Download(DataBase, QueryUpd, Tabela, Token, Client,
    DataHora, laIns, Device, Msg, Log) then
  begin
    if CO_Dmk_Rest_Beta then
      SalvaLog(Log, Tabela);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak +
      '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutaSincronismo_Download(DataBase, QueryUpd, Tabela, Token, Client,
    DataHora, laDel, Device, Msg, Log) then
  begin
    if CO_Dmk_Rest_Beta then
      SalvaLog(Log, Tabela);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak +
      '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if CO_Dmk_Rest_Beta then
    SalvaLog(Log, Tabela);
  //
  Grl_DmkDB.AtualizaSincroDB(DataBase, Device, Tabela, DataHora_Str);
  //
  Result := True;
end;

end.
