unit UnDmkEnums;

interface

uses  System.SysUtils, System.Types {$IfDef MSWINDOWS}, Winapi.Windows {$EndIf},
  UnInternalConsts;

const
  CO_ShowKindList_0_RealIndx = 0;
  CO_ShowKindList_1_ShowKind = 1;
  CO_ShowKindList_2_ToolKind = 2;
  //
  //
  MaxPixelCount = 65536;

type
  // ini 2023-12-20
  TnfseResponsavelRetencao = ( rtPrestador, ptTomador, itIntermediario ); (*Maringa*)
  TnfseDataPesquisa = ( dtpsqNenhuma, dtpsqEmissao, dtpsqCompetencia ); (*Maringa*)
  TnfseFormaGerarNFSe = (fgnLoteRPS, fgnLoteRPSSincrono, fgnGeracaoNFSe, fgnSubstituicaoNFSe);
  // fim 2023-12-20

  // 0=Aberto, 1=Pago parcial, 2=Pago total, 3=Quitado, 4=Cancelado, 5=Bloqueado
  TSitLct = (sitlctAberto=0, sitlctPagoParcial=1, sitlctPagoTotal=2,
             sitlctQuitado=3, sitlctCancelado=4, sitlctBloqueado=5);
  TStausPedVda = (spvIndef=0, spvCancelado=1, spvSuspenso=2, spvBloqueado=3,
                 spv4=4, spvLiberado=5, spv6=6, spv7=7, spv8=8, spvFinalizado=9,
                 spvTeste=90);
  TAmbAplic = (ambaplIndef=0, ambaplProducao=1, ambaplAmostra=2);
  TDreGerSorcInfoGru = (dgsiIndef=0, dgsiReceita=1, dgsiDevolRec=2, dgsiTribRec=3,
                     dgsiTribDev=4, dgsiCustDirVar=5,
                     //...
                     dgsiLancFin=8, dgsiSubTotais=9);
  TDreGerSorcInfoItm = (dgsiND=0, dgsiICMS=1, dgsiPIS=2, dgsiCOFINS=3, dgsiIPI=4);

  TDreCfgCpcStatus = (dccsIndef=0, dccsAutoIncluso=1, dccsAutoExcluso=2,
                      dccsAutoInclusoFilho=3, dccsAutoExclusoFilho=4,
                      dccsAvulso=5,
                      //...
                      dccsErro=9);
  //0=???, 1=N�o integra, 2=Receita (Venda), 3=Devolu��o de venda
  TRegFisCadIntDRE = (rfcidIndef=0, rfcidNaoIntegra=1, rfcidVenda=2, rfcidDevolVenda=3);
  TtpEmissNF = (tpemiProprio=0, tpemiTerceiros=1, tpemiQualquer=2);
  TtpNF = (tpnfEntrada=0, tpnfSaida=1, tpnfQualquer=2);
  TSet_tpNF = Set of TtpNF;
  TSet_tpEmissNF = Set of TtpEmissNF;
  //
  TTipoNFeEntrada = (tneND=0, tneVP=1, tneRP=2, tneCC=3);
  TTipoNFeRetSrvr = (tnrsND=0, tnrsResumo=1, tnrsCompleta=2);
  //TSPED_EFD_K270_08_Origem = (sek2708oriK230K235=1, sek2708oriK250K255=2,
    //sek2708oriK210K215=3, sek2708oriK260K265=4, sek2708oriK220=5);
  TSimNao = (snNao=0, snSim=1);
  TEFDAtrelamentoNFeComEstq = (eanceIndefinido=0, eanceNaoAtrelavel=1,
                               eanceSohCabecalho=2, eanceSohCabecEItens=3);
  TTipImpostoFddEstMun = (tifemIndef=0, tifemPIS=1, tifemCOFINS=2,
    tifemIPI=3, tifemIR=4, tifemCSLL=5, tifemINSS=6, tifemISSQN=7,
    tifemICMSS=8);
  TTypCtbCadMoF = (tccmfIndef=0, tccmfUsoConsumoIn=1, tccmfEstoqueIn=2,
    tccmfDevolucao=3, tccmfVenda=4, tccmfServicoTomado=5);
  TSetaCampoAtivo = (scaDesativa=0, scaAtiva=1, scaInverte=2);
  TConsultaCadastrEmpresa = (cocaemIndef=0, cocaemReceitaFederal=1, cocaemSINTEGRA=2, cocaemCCC_SVRS=3);
  TCliFrnCondition = (cofcNone=0, cofcBothOr=1, cofcBothAnd=2, cofcCli=3, cofcFrn=4);
  TInfoUserMsg = (iumIndef=0, iumInfo=1, iumNoInfo=2, iumWarn=3);
  TContabTpBalanc = (ctbNenhum=0, ctbLctGenCtb=1, ctbLctGenero=2, ctbOutrCtb=3);
  TContabTpAmbito = (ctaNenhum=0, ctaLctGenOpn=1, ctaLctGenEmi=2, ctaSdoCart=3,
                     ctaValrEstq=4, ctaRecupImpost=5, ctaImobilizado=6,
                     ctaPatrimLiq=7);
  TTipoSaidaRelatorio = (tsrImpressao, tsrExportRTF);
  // LocF7
  TdmkF7PreDefProc = (f7pNone=0, f7pEntidades=1, f7pPrdTamCor=2);
  // Notifica��es Mobile
  //TArrNotifMobile = array of array[0..1] of String;
  TGrandezaArea = (grandareaIndef=0, grandareaAreaM2=1, grandareaAreaP2=2);
  TAcaoDeInstrucaoDeNotificacaoMobile = (ainmIndef=0, ainmChamadoOcorrencia=1);
  // TODOS
  TProtocoloEnvioDeImagem = (pdediIndef=0, pdediHTTP=1, pdediFTP=2, pdediREST=3);
  TFormCreateModo = (fcmNenhum=0, fcmOnlyCreate=1, fcmCreateTryShwM=2);
  //
  //Temporario!!!!!!!!!!
  TConvGrandeza = (cgMtoFT, cgFTtoM, cgKGtoLB, cgLBtoKG);
  // Arrays
  TMyGrlArrInt = array of Integer;
  //
  // Aplicativos
  // T F L
  TLstProdQuim = (lpqIndef=0, lpqRefere=1, lpqSimilar=2, lpqComodit=3,
                  lpqBanhos=4, lpqTextos=5, lpqBranco=6, lpqFamilia=7);
  // E N T I D A D E S
  TUnEntTipo = (uetNenhum, uetCliente1, uetCliente2, uetCliente3, uetCliente4,
    uetFornece1, uetFornece2, uetFornece3, uetFornece4, uetFornece5, uetFornece6,
    uetFornece7, uetFornece8, uetTerceiro);
  // B U G S T R O L
  TGraBugsOpera = (gboNenhum, gboServi, gboCaixa);
  TGraBugsServi = (gbsIndef, gbsAplEMon, gbsAplica, gbsMonitora, gbsMonMulti);
  TGraBugsMater = (gbmIndef, gbmEquiEProd, gbmEquipam, gbmProduto);
  TNivelRatifConsumoOS = (nrcosNenhum=0, nrcosProduto=1, nrcosFormula=2,
                         nrcosServico=3, nrcosLocalizador=4);
  // FIM  B U G S T R O L

  // B L U E   D E R M
  TModoBxaEstq = (mbeIndef=0, mbePeca=0, mbePesoKg=2);
  TQuaisItensQuer = (qiqIndef=0, qiqNenhum=1, qiqInsAlt=2, qiqNaoAltter=3,
                       qiqTodos=4);
  TVSTabelas = (vstabIndef=0, vstabVS_TAB_VMI=1);
  TXXImpImeiKind = (viikIndef=0, viikArtigoGerado=1, viikOrdemOperacao=2,
                    viikOPPreenchida=3, viikOPaPreencher=4, viikProcCurt=5);
  TDestImprFichaPallet = (difpNenhum=0, difpPapelA4=1, difpEtiq2x4=2,
                          difpTermica10x15=3, difpTermica10x22=4);
  TImpDesnate = (impdsnDenate, impdsnDenSubUni, impdsnDenSubAll );
  TTipoCalcCouro = (ptcNaoInfo=0, ptcPecas=1, ptcPesoKg=2, ptcAreaM2=3,
                    ptcAreaP2=4, ptcTotal=5);
  TTipoCalcEstq = (tceSemMovim=0, tceAdiciona=1, tceBaixa=2);
  TEstqMovimID = (emidAjuste=0, emidCompra=1, emidVenda=2,
                 emidReclasWE=3, emidBaixa=4, emidIndsWE=5, emidIndsXX=6,
                 emidClassArtXXUni=7, emidReclasXXUni=8, emidForcado=9,
                 emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
                 emidInventario=13, emidClassArtXXMul=14, emidPreReclasse=15,
                 emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,
                 emidEmProcWE=19, emidFinished=20, emidDevolucao=21,
                 emidRetrabalho=22, emidGeraSubProd=23, emidReclasXXMul=24,
                 emidTransfLoc=25, emidEmProcCal=26, emidEmProcCur=27,
                 emidDesclasse=28, emidCaleado=29, emidEmRibPDA=30,
                 emidEmRibDTA=31, emidEmProcSP=32, emidEmReprRM=33,
                 emidCurtido=34, emidMixInsum=35, emidInnSemCob=36,
                 emidOutSemCob=37, emidEmIndstrlzc=38, emidEmProcCon=39,
                 emidConservado=40, emidEntraExced=41 (*, emidSPCaleirad=42*));

  TEstqMovimNiv = (eminSemNiv=0, eminSorcClass=1, eminDestClass=2,
                   eminSorcIndsWB=3, eminDestIndsWB=4, eminSorcReclass=5,
                   eminDestReclass=6,

                   eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10,

                   eminSorcPreReclas=11, eminDestPreReclas=12,

                   eminDestCurtiXX=13, eminSorcCurtiXX=14, eminBaixCurtiXX=15,

                   eminSdoArtInNat=16, eminSdoArtGerado=17,
                   eminSdoArtClassif=18, eminSdoArtEmOper=19,

                   eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22,
                   eminEmWEndBxa=23, eminSdoArtEmWEnd=24,

                   eminSdoFinish=25,
                   eminSdoSubPrd=26,

                   eminSorcLocal=27, eminDestLocal=28,

                   eminSorcCal=29, eminEmCalInn=30, eminDestCal=31,
                   eminEmCalBxa=32, eminSdoArtEmCal=33,

                   eminSorcCur=34, eminEmCurInn=35, eminDestCur=36,
                   eminEmCurBxa=37, eminSdoArtEmCur=38,

                   eminSorcPDA=39, eminEmPDAInn=40, eminDestPDA=41,
                   eminEmPDABxa=42, eminSdoArtEmPDA=43,

                   eminSorcDTA=44, eminEmDTAInn=45, eminDestDTA=46,
                   eminEmDTABxa=47, eminSdoArtEmDTA=48,

                   eminSorcPSP=49, eminEmPSPInn=50, eminDestPSP=51,
                   eminEmPSPBxa=52, eminSdoArtEmPSP=53,

                   eminSorcRRM=54, eminEmRRMInn=55, eminDestRRM=56,
                   eminEmRRMBxa=57, eminSdoArtEmRRM=58,

                   eminSorcMixInsum=59, eminDestMixInsum=60,

                   eminIndzcSrc=61, eminIndzcInn=62, eminIndzcDst=63,
                   eminIndzcBxa=64,

                   eminSorcCon=65, eminEmConInn=66, eminDestCon=67,
                   eminEmConBxa=68, eminSdoArtEmCon=69);


  TEstqEnvKnd = (eekIndef=0, eekIdustrlz=1, eekTransf=2);
{
  TEstqMovimID = (emidAjuste=0, emidCompra=1, emidVenda=2,
                 emidReclasWE=3, emidBaixa=4, emidIndsWE=5, emidIndsVS=6,
                 emidClassArtVSUni=7, emidReclasVSUni=8, emidForcado=9,
                 emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
                 emidInventario=13, emidClassArtVSMul=14, emidPreReclasse=15,
                 emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,
                 emidEmProcWE=19, emidFinished=20, emidDevolucao=21,
                 emidRetrabalho=22, emidGeraSubProd=23, emidReclasVSMul=24,
                 emidTransfLoc=25, emidEmProcCal=26, emidEmProcCur=27,
                 emidDesclasse=28, emidCaleado=29, emidEmRibPDA=30,
                 emidEmRibDTA=31, emidEmProcSP=32, emidEmReprRM=33,
                 emidCurtido=34, emidMixInsum=35, emidInnSemCob=36,
                 emidOutSemCob=37);
}

  TEstqMovWay = (emwyNon=0, emwySrc=1, emwyDst=2);
  TEstqMovInfo = (eminfIndef=0, eminfIEC=1, eminfICR=2, eminfISC=3);
  TEstqMovimSIDB = (emsidbIndef=0, emsidbSrc=1, emsidbInn=2, emsidbDst=3, emsidbBxa=4);
//AdicionarNovosVS_emid(): Boolean;
(*
  TEstqMovimNiv = (eminSemNiv=0, eminSorcClass=1, eminDestClass=2,
                   eminSorcIndsWB=3, eminDestIndsWB=4, eminSorcReclass=5,
                   eminDestReclass=6,

                   eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10,

                   eminSorcPreReclas=11, eminDestPreReclas=12,

                   eminDestCurtiVS=13, eminSorcCurtiVS=14, eminBaixCurtiVS=15,

                   eminSdoArtInNat=16, eminSdoArtGerado=17,
                   eminSdoArtClassif=18, eminSdoArtEmOper=19,

                   eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22,
                   eminEmWEndBxa=23, eminSdoArtEmWEnd=24,

                   eminSdoFinish=25,
                   eminSdoSubPrd=26,

                   eminSorcLocal=27, eminDestLocal=28,

                   eminSorcCal=29, eminEmCalInn=30, eminDestCal=31,
                   eminEmCalBxa=32, eminSdoArtEmCal=33,

                   eminSorcCur=34, eminEmCurInn=35, eminDestCur=36,
                   eminEmCurBxa=37, eminSdoArtEmCur=38,

                   eminSorcPDA=39, eminEmPDAInn=40, eminDestPDA=41,
                   eminEmPDABxa=42, eminSdoArtEmPDA=43,

                   eminSorcDTA=44, eminEmDTAInn=45, eminDestDTA=46,
                   eminEmDTABxa=47, eminSdoArtEmDTA=48,

                   eminSorcPSP=49, eminEmPSPInn=50, eminDestPSP=51,
                   eminEmPSPBxa=52, eminSdoArtEmPSP=53,

                   eminSorcRRM=54, eminEmRRMInn=55, eminDestRRM=56,
                   eminEmRRMBxa=57, eminSdoArtEmRRM=58,

                   eminSorcMixInsum=59, eminDestMixInsum=60

                   );
*)
  TTipoTrocaGgxVmiPall = (ttvpIndef=0, ttvpNenhum=1, ttvpGraGrux=2, ttvpClaRcl=3);
  TEstqSPED220OMIEM = (esomiemIndef=0, esomiemCRUni=1, esomiemCRMul=2,
                   esomiemOpeProcInn=3, esomiemOpeProcDst=4, esomiemGGXRcl=5,
                   esomiemDesclasse=6
                   );
  TEstagioVS_SPED = (evsspedNone=0, evsspedEncerraVS=1, evsspedGeraSPED=2,
                     evsspedExportaSPED=3);
  TCorrApoFormaLctoPosNegSPED = (caflpnIndef=0, caflpnMovNormEsquecido=1,
                               caflpnDesfazimentoMov=2);
  TTabProdOuSubPrd = (tpspIndef=0, tpspProducao=1, tpspSubProd=2);
  TEstqSPEDTabSorc = (estsND=0, estsVMI=1, estsPQx=2, estsNoSrc=3);
  TTipoRegSPEDProd = (trsp21X=21, trsp23X=23, trsp25X=25, trsp26X=26,
                      trsp29X=29, trsp30X=30);
  TOrigemIDKnd = (oidk000ND=0, oidk220CR_CaC=1, oidk220CR_Mul=2, oidk220CR_VMI=3,
                  oidk220RCL_A=4, oidk220RCL_B=5, oidk220COP_A=6, oidk220COP_B=7,
                  oidk220IDO_A=8, oidk210ProcVS=9, oidk210RibDTA=10,
                  oidk210RibPDA=11, oidk200Balanco=12);
  TOrigemSPEDEFDKnd = (osekND=0, osekEstoque=1, osekClasse=2, osekProducao=3(*,
                       osekDesmonte=4*));
  TSPEDLocalKnd = (slkND=0, slkQualquer=1, slkForcaFornec=2, slkForcaSitio=3,
                   slkPrefFornec=4, slkPrefSitio=5);
  TOrigemOpeProc = (oopND=0, oopPredescarne=1, oopProcCaleiro=2,
                    oopCouroCaleado=3, oopRedescarne=4, oopProcCurtimento=5,
                    oopCouroCurtido=6, oopGeracaoArtigo=7, oopOperacao=8,
                    oopProcRecurtimento=9, oopProcSubProduto=10,
                    oop_011=11,  // Livre
                    oopClassReclass=12, oopReprocReparo=13, oopBalanco=14,
                    oopDiluMistInsum=15,  oopProcConserva=16,
                    oopCouroConservado=17);
  TEstqMovimType = (emitIndef=0, emitIME_I=1, emitIME_C=2, emitIME_P=3,
                    emitPallet=4, emitAvulsos=5, emitAgrupTotalizador=6,
                    emitAgrupItensNew=7, emitAgrupItensBxa=8);
  TEstqSetorID = (esiIndef=0, esiCaleiro=1, esiCurtimento=2, esiRecurtimento=3,
                  esiPreAcab=4, esiAcabamento=5, esiETE=6, esiCaldeira=7,
                  esiGraxaria=8, esiOutros=9);
  TEstqDefMulFldEMxx = (edmfIndef=0, edmfSrcNiv2=1, edmfMovCod=2, edmfIMEI=3);
  TVSBastidao = (vsbstdND=0, vsbstdIntegral=1, vsbstdLaminado=2,
                 vsbstdDivTripa=3, vsbstdDivCurti=4, vsbstdRebaixad=5,
                 vsbstdDivSemiA=6, vsbstdRebxSemi=7, vsbstdGelatina=8,
                 vsbstdTapeteInNat=9);
  TVSUmidade = (vsumidND=0, vsumidOtima=1, vsumidMuitoBoa=2, vsumidBoa=3,
                vsumidBaixa=4, vsumidMuitoBaixa=5, vsumidPessima=6);
  TVSRedefnFrmt = (vsrdfnfrmtNaoInfo=0, vsrdfnfrmtInaltera=1,
                   vsrdfnfrmtDivRacha=2, vsrdfnfrmtParteLados=3,
                   vsrdfnfrmtParteCabCul=4, vsrdfnfrmtCabCulBari=5,
                   vsrdfnfrmtGrupona=6, vsrdfnfrmtRefila=7);
  TVSRendTrans = (vsrtIndef=0, vsrtAreaArea=1, vsrtPesoArea=2, vsrtPesoPeso=3,
                 vsrtAreaPeso=4, vsrtPecaArea=5, vsrtPecaPeso=6, vsrtPecaPeca=7);
  TVSLnkIDXtr = (lixIndefinido=0, lixPedVenda=1);
  TXXStatPall = (xxspIndefinido=0, xxspMontando=1, xxspDesmontando=2,
                 xxspMontEDesmo=3, xxspEncerrado=4, xxspEncerMont=5,
                 xxspEncerDesmo=6, xxspEncerMontEDesmo=7, xxspRemovido=8);
  TEstqEditGB = (eegbNone=0, eegbQtdOriginal=1, eegbDadosArtigo=2);
  TEstqMotivDel = (emtdWetCurti013=1, emtdWetCurti045=2, emtdWetCurti104=3,
                   emtdWetCurti008A=4, emtdWetCurti008B=5, emtdWetCurti079=6,
                   emtdWetCurti022=7, emtdWetCurti028=8, emtdWetCurti038=9,
                   emtdWetCurti093=10, emtdWetCurti078=11, emtdWetCurti006=12,
                   emtdWetCurti019=13, emtdWetCurti023=14, emtdWetCurti027=15,
                   emtdWetCurti060=16, emtdWetCurti076=17, emtdWetCurti106=18,
                   emtdWetCurti111=19, emtdWetCurti131=20, emtdWetCurti136=21,
                   emtdWetCurti142=22, emtdWetCurti145=23, emtdWetCurti161=24,
                   emtdWetCurti173=25, emtdWetCurti208=26, emtdWetCurti237=27);
  TEstqSubProd = (esubprdOriginal=0, esubprdDerivado=1);
  TEstqNivCtrl = (encPecas=0, encArea=1, encValor=2);
  TPQxTipo = (pqxtipoInventario=0, pqxtipoEntrada=10, pqxtipoCorrEntrada=20,
              pqxtipoPesagem=110, pqxtipoCorrSaida=120, pqxtipoDiluMistur=130,
              pqxtipoETE=150, pqxtipoDevolucao=170, pqxtipoEPI=180,
              pqxtipoManutencao=185, pqxtipoOutros=190);
  TPQEHowLoad = (pqehlPQE=0, pqehlPQRAjuInn=1, pqehlEntradaCab=2, pqehlPediVda=3,
                 pqehlPdvNFe=4, pqehlPQWRefaz=5);
  TBxaEstqVS = (bevsIndef=0, bevsNao=1, bevsSim=2);
  TProduziuVS = (przvsIndef=0, przvsNao=1, przvsSim=2);
  TCalcExec = (calcexecIndef=0, calcexec1=1, Calcexec2=2, calcexecPecasPorRendArea=3,
       calcexecPecasPorKgTotalDivMediaKgOri=4);
  TKindLocSelGGXE = (klsggxeIndef=0, klsggxeNotLoc=1, klsggxeLocMul=2, klsggxeLocUni=3);
  // FIM  B L U E   D E R M

  // T O O L   R E N T
  TPrmRefQuem = (prqIndefinido=0, prqCliente=1, prqAgente=2);
  TPrmFatorValor = (pfvIndefinido=0, pfvValorTotal=1, pfvValorUnitario=2);
  TPrmFormaRateio = (pfrIndefinido=0, pfrValorTotal=1, pfrValorPorPessoa=2);
  TSrvLCtbDsp = (slcdUnknownToInf=0, slcdNoGerCtaAPag=1, slcdDescoFaturam=2,
                 slcdGeraCtaAPagr=3);
  // FIM  T O O L   R E N T

  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  CT-e
  TTipoConsumoWS_CTe = (tcwscteStatusServico=0, tcwscteRecepcao=1,
               tcwscteRetRecepcao=2, tcwscteEveCancelamento=3,
               tcwsctePediInutilizacao=4, tcwscteConsultaCTe=5,
               tcwscteEveCartaDeCorrecao=6, tcwscteConsultaCadastro=7,
               tcwscteEveEPEC=8);
  TCTeTpEmis = (ctetpemis0=0, ctetpemisNormal=1, ctetpemis2=2, ctetpemis3=3,
               ctetpemisEPEC_SVC=4, ctetpemisContingenciaFSDA=5, ctetpemis6=6,
               ctetpemisSVC_RS=7, ctetpemisSVC_SP=8);
  TCTeTpVeic = (ctetpveicTracao=0, ctetpveicReboque=1);
  TCTeTpRod = (ctetprodNaoAplicavel=0, ctetprodTruck=1, ctetprodToco=2,
               ctetprodCavaloMec=3, ctetprodVAN=4, ctetprodUtilitario=5,
               ctetprodOutros=6);
  TCTeTpCar = (ctetpcarNaoAplicavel=0, ctetpcarAberta=1, ctetpcarFrchadaBau=2,
               ctetpcarGranelera=3, ctetpcarPortaContainer=4, ctetpcarSider=5);
  TCTeTpCTe = (ctetpcteNormal=0, ctetpcteComplValrs=1, ctetpcteAnulacao=2,
               ctetpcteSubstituto=3);
  TCTeMyCST = (ctemycstIndefinido=0, ctemycstCST00Normal=1, ctemycstCST20BCRed=2,
               ctemycstCST40Isensao=3, ctemycstCST41NaoTrib=4, ctemycstCST51Diferido=5,
               ctemycstCST60ST=6, ctemycstCST90Outros=7, ctemycstCST90OutraUF=8,
               ctemycstCSTSimplesNacional=9);
  TCTeForPag = (cteforpagPago=0, cteforpagAPagar=1, cteforpagOutros=2);
  TCTeModal = (ctemodalIndefinido=0, ctemodalRodoviario=1, ctemodalAereo=2,
                ctemodalAquaviario=3, ctemodalFerroviario=4,
                ctemodalDutoviario=5, ctemodalMultimodal=6);
  TCTeTpServ = (ctetpservNormal=0, ctetpservSubcontratacao=1, ctetpservRedespacho=2,
                ctetpservRedespInterm=3, ctetpservServVincMultimodal=4);
  TCTeToma = (ctetomaRemetente=0, ctetomaExpedidor=1, ctetomaRecebedor=2,
              ctetomaDestinatario=3, ctetomaOutros=4);
  TCTeCUnid = (ctecunidM3=0, ctecunidKG=1, ctecunidTON=2, ctecunidUNIDADE=3,
               ctecunidLITROS=4, ctecunidMMBTU=5);
  TCTeMyStatus = (ctemystatusDesencerrada=8, ctemystatusEncerrada=9,
                  ctemystatusCTeDados=10, ctemystatusCTeGerada=20,
                  ctemystatusCTeAssinada=30, ctemystatusLoteRejeitado=40,
                  ctemystatusCTeAddedLote=50, ctemystatusLoteEnvEnviado=60,
                  ctemystatusLoteEnvConsulta=70, ctemystatusEveEPEC_OK=90);
  TCTeMyTpDocInf = (ctemtdiIndef=0, ctemtdiNFe=1, ctemtdiNFsOld=2,
               ctemtdiOutrosDoc=3);
  TCTeServicoStep = (ctesrvStatusServico=0, (*ctesrvEnvioAssincronoLoteCTe*)ctesrvEnvioLoteCTe=1,
                  ctesrvConsultarLoteCTeEnviado=2, ctesrvPedirCancelamentoCTe=3,
                  ctesrvPedirInutilizacaoNumerosCTe=4, ctesrvConsultarCTe=5,
                  ctesrvEventoCartaCorrecao=6, ctesrvConsultaCadastro=7,
                  ctesrvEPEC=8 (*,
                  ctesrvConsultaCTesDestinadas=9, ctesrvDownloadCTes=10,
                  ctesrvConsultaDistribuicaoDFeInteresse=11*));
  TCTeRodoLota =(cterodolotaNao=0, cterodolotaSim=1);
  TCTePropTpProp =(cteptpTAC_Agregado=0, cteptpTAC_Independente=1, cteptpTAC_Outros=2);
  TCTeRespSeg = (ctersRemetente=0, ctersExpedidor=1, ctersRecebedor=2,
                 ctersDestinatario=3, ctersEmitenteDoCTe=4, ctersTomadorDoServico=5);
  TEventosXXe = (evexxCCe,  // Carta de corre��o (NF-e, CT-e)
                 evexxCan,  // Cancelamento (NFe, CT-e, MDF-e)
                 evexxEnc,   // Encerramento (MDF-e)
                 evexxIdC    // Inclusao Condutor (MDF-e)
                 (*eveMDe*)); // Manifesta��o do deestinat�rio
  ///  FIM CT-e


  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  MDF-e
  TTipoConsumoWS_MDFe = (tcwsmdfeStatusServico=0, tcwsmdfeRecepcao=1, tcwsmdfeRetRecepcao=2,
                   tcwsmdfeEveCancelamento=3, tcwsmdfePediInutilizacao=4,
                   tcwsmdfeConsultaMDFe=5, tcwsmdfeEveIncCondutor=6, tcwsmdfe7=7, tcwsmdfeEveEncerramento=8,
                   tcwsmdfeConsultaNaoEncerrados=9 );
  TMDFeTipoEmitente = (mdfetpemitIndef=0, mdfetpemitPrestadorServico=1, mdfetpemiCargaPropria=2);
  TMDFeModal = (mdfemodalIndefinido=0, mdfemodalRodoviario=1, mdfemodalAereo=2,
                mdfemodalAquaviario=3, mdfemodalFerroviario=4);
  TMDFeMyStatus = (mdfemystatusDesencerrada=8, mdfemystatusEncerrada=9,
                  mdfemystatusMDFeDados=10, mdfemystatusMDFeGerada=20,
                  mdfemystatusMDFeAssinada=30, mdfemystatusLoteRejeitado=40,
                  mdfemystatusMDFeAddedLote=50, mdfemystatusLoteEnvEnviado=60,
                  mdfemystatusLoteEnvConsulta=70);
  TMDFeServicoStep = (mdfesrvStatusServico=0, (*mdfesrvEnvioAssincronoLoteMDFe*)mdfesrvEnvioLoteMDFe=1,
                  mdfesrvConsultarLoteMDFeEnviado=2, mdfesrvEventoCancelamento=3,
                  (*mdfesrvPedirInutilizacaoNumerosMDFe=4,*) mdfesrvConsultarMDFe=5,
                  mdfesrvEventoIncCondutor=6, (*mdfesrvConsultaCadastroEntidade=7,*)
                  mdfesrvEventoEncerramento=8 (*inoperante???*)(*,
                  mdfesrvConsultaNaoEncerrados=9(*, mdfesrvDownloadMDFes=10,
                  mdfesrvConsultaDistribuicaoDFeInteresse=11*));
  TMDFeCargaCUnid = (mdfeccuIndef=0, mdfeccuKG=1, mdfeccuTON=2);

  ///  FIM MDF-e


  // TODOS  T O D O S  GERAL G E R A L
  TWhatPsqHow = (wphIndef=0, wphNon=1, wphUni=2, wphParcial=3, wphAll=4);
  TVerApp = (verappIndef=0, verappAppFile=1, verappAppApp=2, verappWebFile=3);
  TYesNoAsk = (ynaNo=0, ynaYes=1, ynaAsk=2, ynaIndef=3);
  THaveSrcDB = (hsdbIndef=0, hsdbImported=3, hsdbCadastr=6, hsdbToDelete=9);
  TFormaPesquisa = (fpcNenhum, fpcTexto, fpc2, fpc3, fpc4, fpc5, fpc2Nomes,
                    fpcSQL_Livre);
  TOrientTabArqBD = (stabdIndef=0, stabdAcessivelToMorto=1, stabdMortoToAcessivel=2,
                     stabdAcessivelToVersoes=3);
  TCRCTableManage = (crctmIndef=0,
                     crctmStandBy=1,             // N�o usado no CRC
                     crctmERPUpload=2,           // Enviado do ERP ao CRC            crctmCRCUpAndSync
                     crctmCRCUpAndSyncToward=3,  // Enviado do CRC ao CDR e sincronizado no ERP
                     crctmCRCUpNotSyncToward=4,  // Enviado do CRC ao CDR mas n�o sincronizado no ERP
                     crctmAllUpAndSyncToward=5,  // Enviado de qualquer servidor para qualquer servidor e sincronizado
                     crctmCRCUpAndSyncDelSelf=6, // Enviado do CRC ao CDR e sincronizar item deletado com tabela propria (gemea) de dele��o
                     crctmCRCUpAndSyncDelGnrc=7, // Enviado do CRC ao CDR e sincronizar item deletado - tabela gen�rica de dele��o
                     crctmAllUpAndSyncOnlyIns=8  // Enviado de qualquer servidor para qualquer servidor e sincronizado apenas se n�o existe valendo a info do ERP!
                     //crctmCRCUpAndCopyToward=9   // Enviado do CRC ao CDR e copiado no ERP
                     );
  TItemTuplePurpose = (itpIndef=0,
////////////////////////////////////////////////////////////////////////////////
//  itpCDRIncremSync=1:
//  �ndice prim�rio cadastrado no CDR. Atrelar e comutar no ERP ao sincronizar.
                       itpCDRIncremSync=1,
////////////////////////////////////////////////////////////////////////////////
//  itpERPRelatnSync=2:
//  �ndice prim�rio cadastrado no ERP pelo usuario e carregado (upload) no CDR.
//  Apenas copiar o dado do CDR ao ERP ao sincronizar.
                       itpERPRelatnSync=2,
////////////////////////////////////////////////////////////////////////////////
//  itpSysRelatnPrDf=3:
//  Valor fixo, pr�-definido, enumerado, ou pr�-cadastrado pelo pelo usuario ou
//  ERP no ERP e carregado (upload) no CDR. Apenas copiar do CDR para o ERP ao
//  sincronizar.
                       itpSysRelatnPrDf=3,
////////////////////////////////////////////////////////////////////////////////
//  itpCDRRelatnSync=4:
//  Dado cadastrado no CDR por (itpCDRIncremSync) em outra tabela como �ndice
//  prim�rio e usado como dado relacional antes do sincronismo com o ERP.
//  Buscar atrelamento e comutar no ERP;
                       itpCDRRelatnSync=4,
////////////////////////////////////////////////////////////////////////////////
//  itpUsrPrimtivData=5:
//  Dado informado pelo usu�rio livremente (dado primitivo). Copiar no ERP.
                       itpUsrPrimtivData=5,  // Dado primitivo informado pelo usu�rio
////////////////////////////////////////////////////////////////////////////////
//  itpSysOrCalcData=6:
//  Dado qualquer informado pelo ERP.
                       itpSysOrCalcData=6,
////////////////////////////////////////////////////////////////////////////////
//  itpERPSync=7:
//  Dado usado para sincroniza��o entre CDR e ERP.
                       itpERPSync=7,
////////////////////////////////////////////////////////////////////////////////
//  itpAllSrvrIncUniq=8:
//  Dado deve ser unique em todo conjunto de servidores. Liberar listas impresas
//  ou intervalos de codigos para cada servidor ter certeza que o mesmo ID n�o
//  ser� usado por outro servidor. Ex.: Liberar de 1 a 999 para um CDR e 1000 a
//  1999 para outro, etc. Nunca usar um mesmo ID em mais de um servidor.
//  Cadastrar no CDR/ERP copiar para o ERP.
                       itpAllSrvrIncUniq=8,
////////////////////////////////////////////////////////////////////////////////
//  itpERPSrvrIncOver=9:
//  ID. Pode cadastrar no CDR mas o ID cadastrado no ERP sobrep�e o ID do CDR.
//  Ex.: VSEntiMP.Sigla >> VSEntiMP.Codigo=Entidades.Codigo
//  O ID cadastrado no CDR deve ter o mesmo valor que tem ou ter� no ERP.
//  Os dados do CDR s� devem ser copiados ao ERP se ainda n�o existir cadastro
//  no ERP.
                       itpERPSrvrIncOver=9,
////////////////////////////////////////////////////////////////////////////////
//  itpCDRRelSncOrfao=10:
//  Dado cadastrado no CDR por (itpCDRIncremSync) sem ter tabela, onde seria
//  �ndice prim�rio, e usado como dado relacional (ou n�o) antes do sincronismo
//  com o ERP.
//  Buscar atrelamento e comutar no ERP; Ex.: VSMovIts.MovimTwn
                       itpCDRRelSncOrfao=10,
////////////////////////////////////////////////////////////////////////////////
//  itpUsrOrErpPrimtiv=11:
//  Dado informado pelo usu�rio livremente (dado primitivo) em alguns casos e em
//  outros preenchido pelo ERP. Copiar no ERP.
                       itpUsrOrErpPrimtiv=11,
////////////////////////////////////////////////////////////////////////////////
//  itpDeprecado=98:
                       itpDeprecado=12,
////////////////////////////////////////////////////////////////////////////////
//  itpInutilizado=99:
                       itpInutilizado=13,
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//  itpCDRRelSncMulTab=14:
//  Dado cadastrado no CDR por (itpCDRIncremSync) com origem de mais de uma
//  tabela (grupo de tabelas) como �ndice prim�rio e usado como dado relacional
//  antes do sincronismo com o ERP.
//  Buscar atrelamento e comutar no ERP; Ex.: VSMovIts.Codigo
                       itpCDRRelSncMulTab=14,
////////////////////////////////////////////////////////////////////////////////
//  itpCDRDeleteSync=15:
//  �ndice prim�rio cadastrado no CDR. Atrelar e DELETAR no ERP ao sincronizar.
                       itpCDRDeleteSync=15,
////////////////////////////////////////////////////////////////////////////////
//  itpERPPriNoIncRelatSync=16:
//  �ndice prim�rio sem campo incremental, tabela de apoio com indice prim�rio
//  igual � tabela original (clonado) para dados adicionais da tabela de origem.
//  Exemplo VSMovDif.Controle=VSMovIts.Controle
//  Dado cadastrado no CDR. Comutar no ERP ao sincronizar.
                       itpERPPriNoIncRelatSync=16,
////////////////////////////////////////////////////////////////////////////////
//  itpERPPriNoIncNoRelSync=17:
//  �ndice prim�rio sem campo incremental - dado cadastrado no CDR.
//  Apenas copiar no ERP ao sincronizar.
                       itpERPPriNoIncNoRelSync=17);
////////////////////////////////////////////////////////////////////////////////

(*^XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*)
  TSetOfTuplePurpose = set of TItemTuplePurpose;
  TSetOfCRCTableManage = set of TCRCTableManage;
(*^XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*)
  TInfoInconsSta = (infincstaAberto=4, infincstaEngano=8, infincstaDesfeito=12,
                    infincstaReportado=16, infincstaIgnorado=20,
                    infincstaNaoAceito=24, infincstaEmDiscussao=28,
                    infincstaDiscutido=32, infincstaAResolver=36,
                    infincstaNaoResolvido=40, infincstaInsoluvel=44,
                    infincstaResolvido=48);
  TdmkModoExec = (dmodexIndef=0, dmodexAutomatico=1, dmodexManual=2);
  TRetornImpost = (retimpostIndef=1, retimpostNenhum=2, retimpostTodos=3,
                   retimpostICMS=4, retimpostPIS=5, retimpostCOFINS=6,
                   retimpostIPI=7);
  TSemAcentos = (etxtsaNaoMuda=0, etxtsaSemAcento=1(*, etxtsaSPED=2*));
  TTipoXXe = (tipoxxeNFe=55, tipoxxeCTe=57, tipoxxeMDFe=58);
  TEventoXXe = (evexxe110110CCe=0(*110110*), evexxe110111Can=1(*110111*),
                evexxe110112Enc=2(*110112*), evexxe110113EPEC=3(*110113*),
                evexxe110114IncCondutor=4(*110114*), evexxe110140EPEC=5(*110140*),
                evexxe110160RegMultimodal=6(*110160*),
                evexxe240130AutCTeComplem=7(*240130*),
                evexxe240131CanCTeComplem=8(*240131*),
                evexxe240140CTeSubst=9(*240140*),
                evexxe240150CTeAnul=10(*240150*),
                evexxe240160CTeMultimodal=11(*240150*),
                evexxe310620RegPassManu=12(*310620*),
                evexxe510620RegPassBRId=13(*510620*)
                );
  TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
  TFormaAssinaXXe = (faxxeCAPICOM=0, faxxeDotNetFrameWork=1, faxxeACBr=2);
  ////////////////
  TTipoNoXML = (tnxAttrStr, tnxTextStr);
  TStatusOSExec = (soseNaoDefinido=0, soseEmSer=1, soseExecutada=2,
    soseCancelada=3);
  TWhereOrAnd = (woaIndef=0, woaWhere=1, woaAnd=2); // SQL > WHERE ou AND
  TORD_SQL = (sqlordIndef=0, sqlordASC=1, sqlordDESC=2); // SQL > Desc
  TdmkAppID = (dmkappB_L_U_E_D_E_R_M=2, dmkappS_Y_N_D_I_C=4,
               dmkappP_L_A_N_N_I_N_G=6, dmkappB_U_G_S_T_R_O_L=24);
  // SPED
  TSPED_EFD_ComoLancou = (seclIndef=0, seclImportadoNormal=1, seclLancadoManual=2);
  ////// N�o mexer aqui! � do SPED do Fisco! ///////////////////////////////////
  TSPED_EFD_GerBxa = (segbIndef=0, segbBaixa=1, segbGera=2);
  TSPED_EFD_KndRegOrigem = (sek2708oriIndef=0, sek2708oriK230K235=1,
                              sek2708oriK250K255=2, sek2708oriK210K215=3,
                              sek2708oriK260K265=4, sek2708oriK220=5,
                              sek2708oriK291=6, sek2708oriK292=7,
                              sek2708oriK301=8, sek2708oriK302=9);
  ////////////////////////////// FIM Fisco /////////////////////////////////////
  TSPED_EFD_Bal = (sebalIndef=0, sebalStqMovItsX=1, sebalEfdIcmsIpiH010=2,
                   sebalPQx=3, sebalVSMovItX=4, sebalEFD_K200=5);
  TGrandezaUnidMed = (gumPeca=0, gumAreaM2=1, gumPesoKG=2, gumVolumeM3=3,
                      gumLinearM=4, gumOutros=5, gumAreaP2=6, gumPesoTon=7);
  TCouNiv2 = (cn2Indef=0, cn2ProdPrin=1, cn2ProdSec=2, cn2SubPrd=3);
  TSPED_IndProp_IndEst = (spedipiePropInfPoderInf=0, spedipiePropInfPoderTer=1,
                          spedipiePropTerPoderInf=2, spedipieIndefinido=3);
  TEFDExpErr = (efdexerNaoImplem=0, efdexerUnknown=1, efdexerObrigSemCont=2,
                efdexerErrSeqGerLin=3, efdexerTipoFldIncorr=4,
                efdexerDifDecimais=5, efdexerDocInvalido=6, efdexerTamTxtDif=7,
                efdexerTamTxtExce=8, efdexerTipObrigUnkn=9,
                efdexerServicInval=10, efdexerFaltaNCM=11, efdexerErrFldAlfa=12,
                efdexerCFOPUnkn=13, efdexerModFiscInval=14,
                efdexerCFOPInval=15, efdexerValInval=16, efdexerCodMunIndef=17,
                efdexerFldObrigValOp=18, efdexerRegFilhObrig=19);
  // http://www31.receita.fazenda.gov.br/SicalcWeb/calcpfPer_odo.html
  TTipoPeriodoFiscal = (spedperIndef=0, spedperDiario=1, spedperSemanal=2,
                  spedperDecendial=3, spedperQuinzenal=4, spedperMensal=5);
  //
  // Fim SPED
  // FIM Aplicativos
  {$IfDef MSWINDOWS}
  PRGBTripleArray2 = ^TRGBTripleArray;
  TRGBTripleArray2 = array[0..32767] of TRGBTriple;
  pRGBTripleArray = ^TRGBTripleArray;
  TRGBTripleArray = ARRAY[0..MaxPixelCount-1] OF TRGBTriple;
  {$EndIf}
  //TDmkTypeDiary = (dtdiarNone=0, dtdiarSAC=1, dtdiarSimple=2);

  TDmkModuloApp = (mdlappNenhum=0, mdlappAppMain=1,mdlappAllBase=2,
                   mdlappEntidades=3, mdlappEmpresas=4, mdlappAgenda=5,
                   mdlappAnotacoes=6, mdlappBancos=7, mdlappBloquetos=8,
                   mdlappCNAB=9, mdlappContratos=10, mdlappCRO=11,
                   mdlappDiario=12, mdlappEmail=13, mdlappEstoque=14,
                   mdlappFavoritos=15, mdlappFinanceiro=16, mdlappFPMin=17,
                   mdlappFPMax=18, mdlappFTP=19, mdlappGrade=20, mdlappNFe=21,
                   mdlappNFSe=22, mdlappPerfis=23, mdlappProtocolos=24,
                   mdlappSAC=25, mdlappSAF=26, mdlappSPED=27, mdlappWEB=28,
                   mdlappWTextos=29, mdlappCuns=30, mdlappBina=31,
                   mdlappConciBco=32, mdlappGFat=33, mdlappGPed=34,
                   mdlappVS=35, mdlappGraTX=36, mdlappLocalDB=37,
                   mdlappContabil=38);
  TArrayListaEstatus = array of array[0..5] of Integer;
  TDataTipo = (dtMySQL, dtSystem, dtSystem2, dtSystem3, dtTexto, dtDelphi);
  TDatePurpose = (dmkdpNone=0, dmkdpGeneric=1, dmkdpSPED_EFD_MIN=2,
                  dmkdpSPED_EFD_MAX=3, dmkdpSPED_EFD_MIN_MAX=4, dmkdpFinance=5,
                  dmkdpInsumMovimMin=6, dmkdpInsumMovimMax=7,
                  dmkdpInsumMovimMinMax=8);
  TfrxImpComo = (ficMostra, ficImprime, ficSalva, ficExporta, ficNone);
  TFormCadEnti = (fmcadSelecionar, fmcadEntidades, fmcadEntidade2, fmcadEntidade3);
  TDmkDBMSs = (dbmsUndef=0, dbmsMySQL=1, dbmsSQLite=2, dbmsIBLite=3);
  TDmkDBSrc = (dbsrcLocalServer=0, dbsrcLocalUser=1, dbsrcWebServer=2, dbsrcWebUser=3);
  dmkTbCtrl = (tctrlCad=0, tctrlAlt=1, tctrlLok=2, (*tctrlDel, *)tctrlWeb=3,
               tctrlAti=4, tctrlAWSI=5, tctrlNil=6);
  TAcaoCriaTabela   = (actCreate=0, actDescribe=1);
  TResultVerify = (rvOK=0, rvErr=1, rvAbort=2);
  TMyMotivo = (mymotDifere=0, mymotSemRef=1);
  TSQLType    = (stIns=0, stUpd=1, stDel=2, stCpy=3, stLok=4, stUnd=5, stPsq=6,
    stNil=7, stDwnSinc=8, stUpSinc=9, stDwnNoSinc=10);
  //TActSQLTwoTb = (asttIndef=0, asttAtrelado=1, asttMesclado=2); /
  TERPAtrelIns = (atrinsIndef=0, atrinsNoAtrelNoIns=1, atrinsNoAtrelButIns=2,
               atrinsNoInsButAtrel=3, atrinsAtrelAndIns=4);
  TUsoXMLQuery = (uxqNoSQL=0, uxqSQLite=1, uxqMySQL=2);
  TAcessFmModo = (afmoSoMaster=0, afmoSoAdmin=1, afmoSoBoss=2, afmoNegarSemAviso=3,
                  afmoNegarComAviso=4, afmoLiberado=5, afmoSemVerificar=6);
  TSinal = (siPositivo=0, siNegativo=1);
  TCompara = (cmprIndef=0, cmprIgual=1, cmprDiferente=2, cmprMaiorQue=3,
              cmprMenorQue=4, cmprMairOuIgual=5, cmprMenorOuIgual=6);
  TDebCred = (debcredND=0, debcredDeb=1, debcredCred=2, debcredAmbos=3);
  TTipoSinal = (tsPos=0, tsNeg=1, tsDef=2);
  TReopenKind = (rkGrade=0, rkPrint=1);
  TLogActnID = (laiNone=0, laiOSWCab=1, laiOSWPipMon=2, laiOSPipIts=3);
  TLogAcaoExe = (laeNone=0, laeDownload=1, laeUpload=2, laeSelected=3,
                 laeInsert=4, laeUpdated=5, laeDeleted=6, laeFinished=7);
  TFormaCalcPercent = (fcpercDesconto, fcpercAcrescimo, fcpercPrecoTax);
  TWBRclass = (wbrEntrada, wbrEstoque);
  TEnvioCNAB = (ecnabIndefinido, ecnabRemessa, ecnabRetorno, ecnabBloquet);
  TFormaWBIndsWE = (fiwNone, fiwPesagem, fiwBaixaPrevia);
  TRelAbrange = (relNada, relAbrangeEmpresa, relAbrangeCliente);
  TValCelRecXLS = (vcrxNenhum=0, vcrxSetor=1, vcrxCliente=2, vcrxTecnico=3,
    vcrxAccount=4, vcrxArtigo=5, vcrxEspessura=6, vcrxRendiment=7,
    vcrxRelatA=8, vcrxPesoCouro=9, vcrxCambio=10, vcrxQuantia=11, vcrxData=12,
    vcrxRebaixe=13);
  TFileEncodingType = (fetAnsi=0, fetUnicode=1, fetUnicodeBigEndian=2, fetUTF_8=3);
  TDateEncodeType = (detJustSum=0, detLastDaySameMonth=1, detFirstDayNextMonth=2);
  TDmkDBLookup = (ddluNone=0, ddluMyDB, ddluMyPID_DB, ddluAll_DB);
  TAllFormat  = (dmktfNone, dmktfString, dmktfDouble, dmktfInteger, dmktfLongint,
                 dmktfInt64, dmktfDate, dmktfTime, dmktfDateTime, dmktfMesAno,
                 dmktf_AAAAMM, dmktf_AAAA_MM,
                 // Deve ser o �ltimo:
                 dmktfUnknown);
  TdmkDataFmt = (dmkdfShort, dmkdfLong);
  TdmkHoraFmt = (dmkhfShort=0, dmkhfLong=1, dmkhfMiliSeconds=2, dmkhfExtended=3,
                 dmkhfTZD_UTC=4);
  TDmkSpidAct = (dsaUnkn=0, dsaFrst=1, dsaPrio=2, dsaThis=3, dsaNext=4,
                dsaLast=5, dsaPesq=6, dsaDesc=7, dsaConf=8, dsaCanc=9,
                dsaHome=10, dsaPlus=11, dsaEdit=12, dsaKill=13, dsaImpr=14,
                dsaCfgs=15, dsaClse=16, dsaOlho=17);
  TKndFmDmk = (kfdNoDef=0, kfdNoXtra=1, kfdCadTypA=2);
  TDmkTabKnd = (dtkMainForm=0, dtkOther=1, dtkNone=2);
  TUpdType    = (utYes=0, utNil=1, utIdx=2, utInc=3);
  TMskType    = (fmtNone, fmtCPFJ, fmtCEP, fmtTelCurto, fmtTelLongo, fmtNCM,
                 fmtCBO2002, fmtIE, fmtNumRua, fmtUF, fmtVersao, fmtCPFJ_NFe,
                 fmtCEP_NFe, fmtTel_NFe, fmtIE_NFe, fmtChaveNFe);
  TComoFmtChNFe = (cfcnNone, cfcnDANFE, cfcnFrendly);
  TDmkCanEdit = (dceOnInsUpd=0, dceNever=1, dceAllways=2);
  TNovoCodigo = (ncIdefinido=0, ncControle=1, CtrlGeral=2, ncGerlSeq1=3);
  TDmkEditStyle = (desEdit=0, desClearingEdit=1, desTimeEdit=2, desComboEdit=2,
                desPasswordEdit=3);
  TReceitaTipoSetor = (rectipsetrNenhum=0, rectipsetrRibeira=1,
                      rectipsetrRecurtimento=2, rectipsetrAcabamento=3);
  TReceitaRibSetor = (recribsetNaoAplic=0, recribsetCaleiro=1,
                 recribsetCurtimento=2, recribsetRecurtimento=3);
  TAquemPag  = (apFornece=0, apCliente=1, apTranspo=2, apNinguem=3, apComisao=4);
  TMinMax    = (mmNenhum=0, mmMinimo=1, mmMaximo=2, mmAmbos=3);
  TAppVerNivel = (avnAlfa=0, avnBeta=1, avnEstavel=2);
  TAppVerRevert = (avrNo=0, avrYes=1);
  // // 0 - N�o informado
     // 1 - EntiContat
     // 2 - Entidades
     // 3 - Avulso
  TClasseEnti = (classeentiEmpresa, classeentiCliente, classeentiTransporta);
  TNFeCTide_TipoDoc = (NFeTipoDoc_Desconhecido,
                       NFeTipoDoc_nfe_0, NFeTipoDoc_nfe_1,
                       NFeTipoDoc_enviNFe, NFeTipoDoc_nfeProc,
                       NFeTipoDoc_cancNFe, NFeTipoDoc_retCancNFe,
                       NFeTipoDoc_procCancNFe, NFeTipoDoc_inutNFe,
                       NFeTipoDoc_retInutNFe, NFeTipoDoc_procInutNFe);
  TNFeCodType = (nfeCTide_TipoDoc,  nfeCTide_tpNF,  nfeCTide_indPag,
                 nfeCTide_procEmi, nfeCTide_finNFe, nfeCTide_tpAmb,
                 nfeCTide_tpEmis, nfeCTide_tpImp,  nfeCTModFrete,
                 nfeCTide_indCont,
                 nfeCTide_idDest, nfeCTide_indFinal, nfeCTide_indPres,
                 nfeCTdest_indIEDest, nfeCTmotDesICMS, nfeIND_PAG_EFD,
                 nfeIND_FRT_EFD, nfeCOD_SIT_EFD, nfeIND_MOV_EFD,
                 nfeCLAS_ESTAB_IND, nfeTP_CT_e);
  TSPEDCodType = (spedIND_NAT_PJ, spedCOD_INC_TRIB, spedIND_APRO_CRED,
                  spedCOD_TIPO_CONT, spedIND_REG_CUM, spedIND_NAT_FRT,
                  spedNAT_BC_CRED, spedTES_ITENS, spedTES_BC_ITENS);
  TNFeServicoStep = (nfesrvStatusServico=0, (*nfesrvEnvioAssincronoLoteNFe*)nfesrvEnvioLoteNFe=1,
                  nfesrvConsultarLoteNfeEnviado=2, nfesrvPedirCancelamentoNFe=3,
                  nfesrvPedirInutilizaCaoNumerosNFe=4, nfesrvConsultarNFe=5,
                  nfesrvEnviarLoteEventosNFe=6, nfesrvConsultaCadastroEntidade=7,
                  nfesrvConsultaSitua��oNFE=8 (*inoperante*),
                  nfesrvConsultaNFesDestinadas=9, nfesrvDownloadNFes=10,
                  nfesrvConsultaDistribuicaoDFeInteresse=11);
  TNFeDFeSchemas = (nfedfeschUnknown=0, nfedfeschResNFe=1, nfedfeschProcNFe=2,
                  nfedfeschResEvento=3, nfedfeschProcEvento=4);
  TXXeIndSinc = (nisAssincrono=0, nisSincrono=1);
  TNFeautXML = (naxNaoInfo=0, naxEntiContat=1, naxEntidades=2, naxAvulso=3);
  TNFEAgruRelNFs = (nfearnIndef=0, nfearnCOFP=1, nfearnNCM=2, nfearnNatOP=3,
                    nfearnProd=4, nfearnTipPrd=5);
  TIBPTaxTabs = (ibptaxtabNCM=0, ibptaxtabNBS=1, ibptaxtabLC116=2);
  TIBPTaxOrig = (ibptaxoriNaoInfo=0, ibptaxoriNacional=1, ibptaxoriImportado=2);
  TIBPTaxFont = (ibptaxfontNaoInfo=0, ibptaxfontIBPTax=1, ibptaxfontCalculoProprio=2);
  TTpCalcTribNFe = (tctnfeIndefinido=0, tctnfeAutomatico=1, tctnfeManual=2,
                    tctnfeMixto=3);
  TNFeIndFinal = (indfinalNormal=0, indfinalConsumidorFinal=1);
  TindFinalTribNFe = (infFinTribIndefinido=0, infFinTribSoConsumidorFinal=1,
                      infFinTribTodos=2);
  TcSitNFe = (csitnfeDesconhecido=0, csitnfeAutorizado=1, csitnfeDenegado=2);
  TdmkCambios = (cambiosBRL=1, cambiosUSD=2, cambiosEUR=3, cambiosIDX=4);
  TNFeEventos = (nfeeveCCe=110110, nfeeveCan=110111,
                 nfeeveMDeConfirmacao=210200, nfeeveMDeCiencia=210210,
                 nfeeveMDeDesconhece=210220, nfeeveMDeNaoRealizou=210240,
                 nfeeveEPEC=110140);

(*  Desabilitado por causa do multiOS do XE5!
  //TKeyType    = (ktString, ktBoolean, ktInteger, ktCurrency, ktDate, ktTime);
*)

  TFormaCopiaInterfaceCompo = (fcicBitBlt, fcicPaintTo);
  TFormaFlipReverseBmp = (ffrbScanLine, ffrbCopyRect, ffrbStretchBlt);
  TUsaQualPredef = (uqpIndefinido, uqpADefinir, uqpEspecifico, uqpGeral);

  // TdmkDraw...
  TPolyPoint = array of TPoint;
  TPolyDot = array of Integer;
  TImgMouseAction = (maNenhum=0, maSelectImage=1, maSelectColor=2, maDrawPen=3,
                     maDrawBrush=4, maRubber=5, maMarquee=6, maArcCutIni=7,
                     maArcCutFim=8, maSelPenColor=9, maSelBrushColor=10);
  // TUnDmkChalk.NomeToolKind(ToolKind: TToolKind): String;
  TToolKind = (fdNenhum=0, fdLivre=1, fdLinha=2, fdRetangulo=3, fdElipse=4,
               fdPolyline=5, fdRoundRect=6, fdPolygon=7, fdTextBox=8,
               fdPolyBezier=9, fdArc=10, fdAngleArc=11, fdBrush=12,
               fdFigure=13, fdRectArc=14, fdMarquee=15, fdDrag=16);
  TToolKinds = set of TToolKind;
  TToolsUsed = array of array[0..1] of Integer;
  TShowKind = (skNenhuma=0, skDrawVetor=1, skTexto=2);
  TStrDraw = array of array[0..1] of TPoint;
  TStrDraws = array of TStrDraw;

  TShowKindList = array of array[0..2] of Integer;
  // FIM TdmkDraw...



  //
  TTemControle = set of dmkTbCtrl;
  //
  TAppGrupLst = array of array of String;
  TArrSelIdxInt1 = array of Integer;
  TArrSelIdxInt2 = array of array[0..1] of Integer;
  //
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
  //
  TPartOuTodo = (ptIndef=0, ptNada=1, ptParcial=2, ptTotal=3, ptXtra1=4);
  //
  TTribOperacao = (tribcalcNone=0, tribcalcInclusoBase=1, tribcalcExclusoBase=2(*,
                   tribcalcInclusoNivel=3, tribcalcExclusoNivel=4*));
  //
  TVSRestituicaoInn = (rivsNone=0, rivsPecas=1, rivsPesoKg=2, rivsSalKg=3,
                       rivsValor=4);
  TTabToWork = (ttwA=1, ttwB=2, ttwC=3, ttwD=4);
  //
  TAcaoMath = (amathZera=0, amathSubtrai=1, amathSoma=2, amathSubstitui=3);

  TLastAcao   = (laIns=0, laUpd=1, laDel=2);
  TAlterWeb   = (alDsk=0, alWeb=1, alMob=2);
  TDeviceType = (stDesktop=0, stWeb=1, stMobile=2);

  THttpHeader = record
    Nome: String;
    Valor: String;
  end;
  THttpParam = record
    Nome: String;
    Valor: String;
    ACharse: String;
    AContentType: String;
  end;


  TUnDmkEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  TextoToSQLType(Tipo: String): TSQLType;
    function  ObtemStr_LogActnID(LogActnID: TLogActnID): String;
    function  NomeFatID_All(FatID: Integer): String;
    function  ComparaToString(Comparacao: TCompara): String;
  end;
var
  DmkEnums: TUnDmkEnums;

(*
var
  DMK_TYPE_DIARY: TDmkTypeDiary = dtdiarNone;
*)

const
  // Aplicacao de Condicap de pagamento (Prazo)
  CO_PrimaryIncremenPurpose: TSetOfTuplePurpose = [itpCDRIncremSync, itpAllSrvrIncUniq];
  CO_PrimaryNoAndIncPurpose: TSetOfTuplePurpose = [itpCDRIncremSync,
    itpAllSrvrIncUniq, itpERPSrvrIncOver, itpERPPriNoIncRelatSync, itpERPPriNoIncNoRelSync];
  //CO_ToolKindsPlainGeometry: TToolKinds = ([fdRetangulo, fdElipse, fdRoundRect, fdPolygon]);
  //
  CO_SeqCRCTableManageInsUpd: TSetOfCRCTableManage = [
//                     crctmIndef=0,
//                     crctmStandBy=1,             // N�o usado no CRC
//                     crctmERPUpload=2,           // Enviado do ERP ao CRC            crctmCRCUpAndSync
                     crctmCRCUpAndSyncToward,//=3,  // Enviado do CRC ao CDR e sincronizado no ERP
//                     crctmCRCUpNotSyncToward=4,  // Enviado do CRC ao CDR mas n�o sincronizado no ERP
                     crctmAllUpAndSyncToward,//=5  // Enviado de qualquer servidor para qualquer servidor e sincronizado
//                     crctmCRCUpAndSyncDelSelf=6, // Enviado do CRC ao CDR e sincronizar item deletado com tabela propria (gemea) de dele��o
//                     crctmCRCUpAndSyncDelGnrc=7
                     crctmAllUpAndSyncOnlyIns//=8  // Enviado de qualquer servidor para qualquer servidor e sincronizado apenas se n�o existe
  ];
  CO_SeqCRCTableManageDelete: TSetOfCRCTableManage = [
//                     crctmIndef=0,
//                     crctmStandBy=1,             // N�o usado no CRC
//                     crctmERPUpload=2,           // Enviado do ERP ao CRC            crctmCRCUpAndSync
//                     crctmCRCUpAndSyncToward=3,  // Enviado do CRC ao CDR e sincronizado no ERP
//                     crctmCRCUpNotSyncToward=4,  // Enviado do CRC ao CDR mas n�o sincronizado no ERP
//                     crctmAllUpAndSyncToward=5,  // Enviado de qualquer servidor para qualquer servidor e sincronizado
                     crctmCRCUpAndSyncDelSelf,//=6, // Enviado do CRC ao CDR e sincronizar item deletado com tabela propria (gemea) de dele��o
                     crctmCRCUpAndSyncDelGnrc//=7
//                     crctmAllUpAndSyncOnlyIns=8
  ];

  CO_APLIC_COND_PAG_FRETE = 8;

  //
  CO_ToolKindsPlainGeometry: TToolKinds = ([fdRetangulo, fdElipse, fdRoundRect, fdPolygon]);
  CO_ToolKindsPolyUpDown: TToolKinds = ([fdPolyLine, fdPolygon]);
  cTemControleSim: TTemControle = ([tctrlLok, tctrlCad, tctrlAlt, tctrlWeb, tctrlAti, tctrlAWSI]);
  cTemControleNao: TTemControle = ([tctrlWeb, tctrlAti, tctrlAWSI]);
  cTemControleNil: TTemControle = ([tctrlAti]);
  //
  CO_TXT_naxNaoInfo     = 'N�o informado';
  CO_TXT_naxEntiContat  = 'Contatos';
  CO_TXT_naxEntidade    = 'Entidade';
  CO_TXT_naxAvulso      = 'Avulso';
  MaxNFeautXML = Integer(High(TNFeautXML));
  sNFeautXML: array[0..MaxNFeautXML] of string = (
    CO_TXT_naxNaoInfo        , // 0
    CO_TXT_naxEntiContat     , // 1
    CO_TXT_naxEntidade       , // 2
    CO_TXT_naxAvulso           // 3
  );

  //THaveSrcDB = (hsdbIndef=0, hsdbImported=3, hsdbCadastr=6, hsdbExported=9);
  CO_TXT_hsdbIndef       = 'Indefinido';
  CO_TXT_hsdbImported    = 'Importado';
  CO_TXT_hsdbCadastr     = 'Cadastrado';
  CO_TXT_hsdbToDelete    = 'Deletar';
  MaxHaveSrcDB = 3; //Integer(High(THaveSrcDB));
  sHaveSrcDB: array[0..MaxHaveSrcDB] of string = (
    CO_TXT_hsdbIndef      , // 0
    CO_TXT_hsdbImported   , // 3
    CO_TXT_hsdbCadastr    , // 6
    CO_TXT_hsdbToDelete     // 9
  );
  iHaveSrcDB: array[0..MaxHaveSrcDB] of Integer = (
    Integer(THaveSrcDB.hsdbIndef),    // 0
    Integer(THaveSrcDB.hsdbImported), // 3
    Integer(THaveSrcDB.hsdbCadastr),  // 6
    Integer(THaveSrcDB.hsdbToDelete)  // 9
  );

  CO_TXT_uqpIndefinido  = 'Indefinido';
  CO_TXT_uqpADefinir    = 'A Definir';
  CO_TXT_uqpEspecifico  = 'Espec�fico';
  CO_TXT_uqpGeral       = 'Geral';
  MaxUsaQualPredef = Integer(High(TUsaQualPredef));
  sUsaQualPredef: array[0..MaxUsaQualPredef] of string = (
    CO_TXT_uqpIndefinido   , // 0
    CO_TXT_uqpADefinir     , // 1
    CO_TXT_uqpEspecifico   , // 2
    CO_TXT_uqpGeral         // 3
  );

  MaxListaStatusOSExec = Integer(High(TStatusOSExec));
  sListaStatusOSExec: array[0..MaxListaStatusOSExec] of String = (
  //soseNaoDefinido=0,
  'N�o definido',
  //soseEmSer=1,
  'Em ser',
  //soseExecutada=2,
  'Executada',
  //soseCancelada=3
  'Cancelada'
  );

  CO_TXT_tribcalcNone          = 'Indefinido';
  CO_TXT_tribcalcInclusoBase   = 'Incluso na base';
  CO_TXT_tribcalcExclusoBase   = 'Excluso na base';
  (*CO_TXT_tribcalcInclusoNivel  = 'Incluso no n�vel';
  CO_TXT_tribcalcExclusoNivel  = 'Excluso no n�vel';*)
  MaxTribOperacao = Integer(High(TTribOperacao));
  sListaTribOperacao: array[0..MaxTribOperacao] of string = (
  CO_TXT_tribcalcNone             , // 0
  CO_TXT_tribcalcInclusoBase      , // 1
  CO_TXT_tribcalcExclusoBase      (*, // 2
  CO_TXT_tribcalcInclusoNivel     , // 3
  CO_TXT_tribcalcExclusoNivel     // 4*)
  );

  CO_MOTVDEL_MAX = 34;
  ListaMotivosExclusao_Str: array[0..CO_MOTVDEL_MAX] of string =
  (
  (*0*)'Indefinido',
  (*100*)'Altera��o de lan�amento',
  (*101*)'Altera��o de lan�amento (Janela espec�fica de altera��o)',
  (*102*)'Altera��o de transfer�ncia entre carteiras',
  (*103*)'Altera��o de transfer�ncia entre contas',
  (*104*)'Aletar��o de lan�amento com documento f�sico',
  (*105*)'Aletar��o de quita��o parcial',
  (*200*)'Desfazimento de quita��o',
  (*300*)'Exclus�o de lan�amento',
  (*301*)'Exclus�o incondicional',
  (*302*)'Exclus�o de transfer�ncia entre carteiras',
  (*303*)'Exclus�o de transfer�ncia entre contas',
  (*304*)'Exclus�o de quita��o parcial',
  (*305*)'Exclus�o de lan�amento sem cliente interno',
  (*306*)'Exclus�o de lan�amento de arrecada��o',
  (*307*)'Exclus�o de lan�amento de consumo por leitura',
  (*308*)'Exclus�o de lan�amento de boleto',
  (*309*)'Exclus�o de ratifica��o de parcelamento',
  (*310*)'Exclus�o de parcelamento',
  (*311*)'Exclus�o de lan�amento de faturamento',
  (*312*)'Exclus�o de lan�amento de faturamento parcial',
  (*313*)'Exclus�o de lan�amento (SelfGer2)',
  (*314*)'Exclus�o de lan�amento de PQ em duplicata F',
  (*315*)'Exclus�o de lan�amento de PQ em duplicata T',
  (*316*)'Exclus�o de lan�amento de MP em duplicata M',
  (*317*)'Exclus�o de lan�amento de MP em duplicata T',
  (*318*)'Exclus�o de lan�amento de fatura de produto',
  (*319*)'Exclus�o de lan�amento de MP em duplicata C',
  (*320*)'Exclus�o de lan�amento faturamento de frete',
  (*330*)'Exclus�o de lan�amento de servi�o tomado',
  (*331*)'Exclus�o de lan�amento de tributos retidos de servi�o tomado',
  (*996*)'Excus�o de Reduzido duplicado de PQ (Insumo Qu�mico)',
  (*997*)'Excluis�o de registro tabela',
  (*998*)'Cria��o de arquivo de registro de log tempor�rio',
  (*999*)'Cria��o de arquivo de registro de log'
  );

  ListaMotivosExclusao_Int: array[0..CO_MOTVDEL_MAX] of Integer =
  (
    0, 100, 101, 102, 103, 104, 105, 200, 300, 301, 302, 303, 304, 305, 306,
    307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320,
    330, 331,
    //
    996, 997, 998, 999
  );


  CO_TXT_fgxxePascal     = 'CAPICOM';
  CO_TXT_fgxxeCMaisMais  = 'C++';
  CO_TXT_fgxxeACBr       = 'ACBr';
  MaxFormaGerenXXe = Integer(High(TFormaGerenXXe));
  sFormaGerenXXe: array[0..MaxFormaGerenXXe] of string = (
    CO_TXT_fgxxePascal      , // 0
    CO_TXT_fgxxeCMaisMais   , // 1
    CO_TXT_fgxxeACBr         // 2
  );


  CO_TXT_faxxeCAPICOM          = 'CAPICOM';
  CO_TXT_faxxeDotNetFrameWork  = '.Net Framework';
  CO_TXT_faxxeACBr             = 'ACBr';
  MaxFormaAssinaXXe = Integer(High(TFormaAssinaXXe));
  sFormaAssinaXXe: array[0..MaxFormaAssinaXXe] of string = (
    CO_TXT_fgxxePascal      , // 0
    CO_TXT_fgxxeCMaisMais   , // 1
    CO_TXT_fgxxeACBr         // 2
  );

  CO_TXT_sitlctAberto = 'Aberto';
  CO_TXT_sitlctPagoParcial = 'Pago parcial';
  CO_TXT_sitlctPagoTotal = 'Pago total';
  CO_TXT_sitlctQuitado = 'Quitado';
  CO_TXT_sitlctCancelado = 'Cancelado';
  CO_TXT_sitlctBloqueado = 'Bloqueado';
  MaxSitLct = Integer(High(TSitLct));
  sSitLct: array[0..MaxSitLct] of string = (
    CO_TXT_sitlctAberto,
    CO_TXT_sitlctPagoParcial,
    CO_TXT_sitlctPagoTotal,
    CO_TXT_sitlctQuitado,
    CO_TXT_sitlctCancelado,
    CO_TXT_sitlctBloqueado
  );

implementation

{ TUnDmkEnums }

function TUnDmkEnums.ComparaToString(Comparacao: TCompara): String;
begin
  case Comparacao of
    (*0*)cmprIndef: Result := ' ';
    (*1*)cmprIgual: Result := '=';
    (*2*)cmprDiferente: Result := '<>';
    (*3*)cmprMaiorQue: Result := '>';
    (*4*)cmprMenorQue: Result := '<';
    (*5*)cmprMairOuIgual: Result := '>=';
    (*6*)cmprMenorOuIgual: Result := '<=';
    else
      Result := ' ? ? ? ? ';
  end;
end;

function TUnDmkEnums.NomeFatID_All(FatID: Integer): String;
begin
  case FatID of
  //////////////////////////////////////////////////////////////////////////////
  (*PQx*) VAR_FATID__006: Result := 'Balanco (diferenca do lancamento)';
  (*PQx*) VAR_FATID__005: Result := 'Retorno de PQ sem entrada e/ou baixa no estoque para retorno (Retorno antecipado e incorreto)!)';
  (*PQx*) VAR_FATID__004: Result := 'Baixa de Baixa de PQ (de retorno sem emissao de NFe)';
  (*PQx*) VAR_FATID__003: Result := 'Baixa de entrada de PQ (de retorno sem emissao de NFe)';
  (*Nfe*) VAR_FATID__002: Result := 'Baixa retroativa for�ada';
  (*Nfe*) VAR_FATID__001: Result := 'Balanco (diferenca do lancamento)';
  (*Nfe*) VAR_FATID_0000: Result := 'Balanco (lancamento)';
  (*Nfe*) VAR_FATID_0001: Result := 'Faturanento NF-e';
  (*Nfe*) VAR_FATID_0002: Result := 'Faturamento NFC-e';
  (*Nfe*) VAR_FATID_0003: Result := 'Sa�da condicional';
  (*Nfe*) VAR_FATID_0004: Result := 'Retorno Condicional';
  (*Nfe*) VAR_FATID_0005: Result := 'Entrada por compra';
  (*Nfe*) VAR_FATID_0006: Result := 'Frete de compra';// >> 2021-04-29
  (*PQx*) VAR_FATID_0010: Result := 'Entrada de Uso e Consumo por NF (compra ou remessa de industrializacao)';
  (*PQx*) VAR_FATID_0020: Result := 'Entrada de Uso e Consumo por ajuste no balan�o'; // 2023-10-09

  (*Nfe*) VAR_FATID_0013: Result := 'MPinIts (NFe ) no BlueDerm';
  (*PQx*) VAR_FATID_0030: Result := 'Entrada por dilu��o / mistura';
  (*Nfe*) VAR_FATID_0050: Result := 'Importa��o de NFe de emiss�o pr�pria';
  (*Nfe*) VAR_FATID_0051: Result := 'Entrada de Uso e consumo por NFe';
  (*Nfe*) VAR_FATID_0053: Result := 'Registro de Chave NFe por Consulta de Distribuicao de DFe de interesse';
  (*SMI*) VAR_FATID_0061: Result := 'Transferencia entre centros de estoque';
  (*Tel*) VAR_FATID_0070: Result := 'Entrada por importa��o de conta telef�nica';
  (*Lei*) VAR_FATID_0071: Result := 'Entrada por consumo por leitura (empresa)';
  (*Nfe*) VAR_FATID_0099: Result := 'Avulso (Manual)';
  (*PQx*) VAR_FATID_0100: Result := 'Apenas usado para separar Entradas de sa�das!';//!!!!!!!!!!!!!!!
  //
  (*Nfe*) VAR_FATID_0101: Result := 'Gerado por Classifica��o de WB no Blue derm';
  (*Nfe*) VAR_FATID_0102: Result := 'CMPTOut no BlueDerm';
  (*Nfe*) VAR_FATID_0103: Result := 'MPInn no BlueDerm';
  (*Nfe*) VAR_FATID_0104: Result := 'Baixa por classifica��o de WB no Blue derm';
  (*Nfe*) VAR_FATID_0105: Result := 'Gerado por envio para industrializa��o por terceiros no Blue derm';
  (*Nfe*) VAR_FATID_0106: Result := 'Baixa por envio para industrializa��o por terceiros no Blue derm';
  (*Nfe*) VAR_FATID_0107: Result := 'Gerado por reclassifica��o de WB no Blue derm';
  (*Nfe*) VAR_FATID_0108: Result := 'Baixa por reclassifica��o de WB no Blue derm';
  (*PQx*) VAR_FATID_0110: Result := 'Baixa por pesagem';

  (*Nfe*) VAR_FATID_0113: Result := 'MPinIts (NF normal) no BlueDerm';

  (*PQx*) VAR_FATID_0120: Result := 'Baixa de Uso e Consumo por ajuste no balan�o'; // 2023-10-09

  (*PQx*) VAR_FATID_0130: Result := 'Baixa por dilui��o / mistura';
  (*PQx*) VAR_FATID_0150: Result := 'Baixa de pq no tratamento de efluentes (tabela pqt)';
  (*Nfe*) VAR_FATID_0151: Result := 'Entrada de Uso e consumo por NF modelo normal';

  (*PQx*) VAR_FATID_0170: Result := 'Baixa de pq (Devolu��o > tabela pqd)';
  (*PQx*) VAR_FATID_0180: Result := 'Baixa de IPI';
  (*PQx*) VAR_FATID_0185: Result := 'Baixa de Material de Manuten��o';

  (*PQx*) VAR_FATID_0190: Result := 'Baixa de pq (Outros > tabela pqo)';

  (*Lct*) VAR_FATID_0201: Result := 'FatID generico para Lct de faturamento mensal de servicos de NFS-e (NFSe)';
  (*Lct*) VAR_FATID_0202: Result := 'FatID generico para Lct de faturamento mensal de servicos de NFS-e (NFSe) atrav�s de boletos';

  (*Nfe*) VAR_FATID_0213: Result := 'MPinIts (SPED EFD) no BlueDerm';

  (*Nfe*) VAR_FATID_0251: Result := 'Entrada de Uso e consumo por importacao de SPED EFD';
  (*Lct*) VAR_FATID_0301: Result := (*' Cred itor > *)'Compra de Direitos';
  (*Lct*) VAR_FATID_0302: Result := (*' Cred itor > *)'Pagamento de Compra de Direitos';
  (*Lct*) VAR_FATID_0303: Result := (*' Cred itor > *)'Cobran�a de Taxas de Compra de Direitos';
  (*Lct*) VAR_FATID_0304: Result := (*' Cred itor > *)'Cobran�a de ocorr�ncia';
  (*Lct*) VAR_FATID_0305: Result := (*' Cred itor > *)'Juros de cheques devolvidos';
  (*Lct*) VAR_FATID_0306: Result := (*' Cred itor > *)'Juros de duplicatas vencidas';
  (*Lct*) VAR_FATID_0307: Result := (*' Cred itor > *)'Desconto no pagamento de cheques devolvidos';
  (*Lct*) VAR_FATID_0308: Result := (*' Cred itor > *)'Desconto no pagamento de duplicatas vencidas';

  (*Lct*) VAR_FATID_0311: Result := (*' Cred itor > *)'Total quita��o de cheque devolvido';
  (*Lct*) VAR_FATID_0312: Result := (*' Cred itor > *)'Total quita��o de duplicata vencida';

  (*Lct*) VAR_FATID_0322: Result := (*' Cred itor > *)'Cobran�a de Ad Valorem';

  (*Lct*) VAR_FATID_0333: Result := (*' Cred itor > *)'Cobran�a de Fator de Compra';

  (*Lct*) VAR_FATID_0341: Result := (*' Cred itor > *)'Cobran�a de IOC';
  (*Lct*) VAR_FATID_0342: Result := (*' Cred itor > *)'Cobran�a de IOFd';
  (*Lct*) VAR_FATID_0343: Result := (*' Cred itor > *)'Cobran�a de IOFv';
  (*Lct*) VAR_FATID_0344: Result := (*' Cred itor > *)'Cobran�a de CPMF';
  (*Lct*) VAR_FATID_0345: Result := (*' Cred itor > *)'Cobran�a de IRRF';

  (*Lct*) VAR_FATID_0361: Result := (*' Cred itor > *)'Sobra de valores no border�';
  (*Lct*) VAR_FATID_0362: Result := (*' Cred itor > *)'Falta de valores no border�';

  (*Lct*) VAR_FATID_0365: Result := (*' Cred itor > *)'Repasse de cheques de terceiros a clientes em border�';

  (*Lct*) VAR_FATID_0371: Result := (*' Cred itor > *)'Compra de Cr�ditos';

  (*Lct*) VAR_FATID_0372: Result := (*' Cred itor > *)'Lan�amentos financeiros de remessas Duplicatas';
  (*Lct*) VAR_FATID_0373: Result := (*' Cred itor > *)'Lan�amentos financeiros de remessas Cheques';

  (*Lct*) VAR_FATID_0500: Result := (*' LeSew/GigaStore ->*) 'Pagto compras';
  (*Lct*) VAR_FATID_0510: Result := (*' LeSew/GGigaStore ->*) 'Pagto Vendas';
  (*Lct*) VAR_FATID_0600: Result := (*' M�dulo boletos / Syndic / Sinker ->*) 'Arrecada��es';
  (*Lct*) VAR_FATID_0601: Result := (*' M�dulo boletos / Syndic / Sinker ->*) 'Consumos';
  (*Lct*) VAR_FATID_0610: Result := (*' M�dulo boletos / Syndic / Sinker ->*) 'Acertos de d�bitos (Reparcelamento)';
  (*Lct*) VAR_FATID_0700: Result :=  (*LeSew -> *)'Transfer�ncias entre contas';
  (*Lct*) VAR_FATID_0701: Result :=  (*LeSew -> *)'Despesas professor em viagem (ciclo)';
  (*Lct*) VAR_FATID_0702: Result :=  (*LeSew -> *)'Despesas de rateio (ciclo)';
  (*Lct*) VAR_FATID_0703: Result :=  (*LeSew -> *)'Despesas promotor (ciclo)';
  (*Lct*) VAR_FATID_0704: Result :=  (*LeSew -> *)'Despesas promotor (extras)';
  (*Lct*) VAR_FATID_0705: Result :=  (*LeSew -> *)'Pagamento comiss�es professor (extras)';
  (*Lct*) VAR_FATID_0706: Result :=  (*LeSew -> *)'Pagamento comiss�es professor (ciclo - autom�tico)';
  (*Lct*) VAR_FATID_0711: Result :=  (*LeSew -> *)'Rateio de creditos p/ contas (plano) c/ saldo controlado';
  (*Lct*) VAR_FATID_0712: Result :=  (*LeSew -> *)'Reten��o das comiss�es de professores';
  (*Lct*) VAR_FATID_0721: Result :=  (*LeSew -> *)'Venda de mercadorias pelo professor em viagem (ciclo de curso)';
  (*Lct*) VAR_FATID_0731: Result :=  (*LeSew -> *)'Empr�stimos a professores (Cr�dito e d�bito)';
  (*Lct*) VAR_FATID_0750: Result :=  (*LeSew -> *)'transferencias entre carteiras de acerto entre professor e promotor';
  (*Lct*) VAR_FATID_0751: Result :=  (*LeSew -> *)'transferencias entre carteiras de pagamento da comiss�o de alunos do professor';
  (*Lct*) VAR_FATID_0752: Result :=  (*LeSew -> *)'transferencias entre carteiras de devolu��o da comiss�o de alunos do professor';
  (*Lct*) VAR_FATID_0801: Result :=  (*LeSew -> *)'Venda de mercadorias para pontos de venda';
  (*Lct*) VAR_FATID_0810: Result :=  (*LeSew -> *)'Venda de lingerie';
  (*Lct*) VAR_FATID_0851: Result :=  (*LeSew -> *)'Comiss�o de venda de mercadorias nos pontos de venda';
  (*Lct*) VAR_FATID_0901: Result := ' DControl -> Servicos';
  (*Lct*) VAR_FATID_1001: Result := 'Compra de PQ (uso e consumo)';
  (*Lct*) VAR_FATID_1002: Result := 'Frete de compra de PQ';
  (*Lct*) VAR_FATID_1003: Result := 'Compra de MP';
  (*Lct*) VAR_FATID_1004: Result := 'Frete de compra de MP';
  (*Lct*) VAR_FATID_1005: Result := 'Compra de PQ PS';
  (*Lct*) VAR_FATID_1006: Result := 'Frete de compra de PQ PS';
  (*Lct*) VAR_FATID_1007: Result := 'Venda de couro verde';
  (*Lct*) VAR_FATID_1008: Result := 'Frete de venda de couro verde';
  (*???*) VAR_FATID_1009: Result := 'Venda de couro V S (V S Out...)';
  (*Lct*) VAR_FATID_1010: Result := 'Comiss�o na compra de MP';
  (*Lct*) VAR_FATID_1011: Result := 'Transferencia de estoque VS';

  (*Lct*) VAR_FATID_1013: Result := 'Faturamento de pedido';
  (*Lct*) VAR_FATID_1014: Result := 'Frete de faturamento de pedido';
  (*Lct*) VAR_FATID_1015: Result := 'Pedido de compra Igap� (Pedido = FatID_Sub)';
  (*Lct*) VAR_FATID_1016: Result := 'Pedido de venda Igap� (Pedido = FatID_Sub)';

  (*???*) VAR_FATID_1020: Result := 'Pagamento de Mao de obra para terceiros';

  (*Lct*) VAR_FATID_1030: Result := 'Servi�os tomados';
  (*Lct*) VAR_FATID_1031: Result := 'Tributos retidos de servi�os tomados';

  (*Lct*) VAR_FATID_1041: Result := 'Compra de WB/Semi > VSPlCCab';
  (*Lct*) VAR_FATID_1042: Result := 'Frete xde Compra de WB/Semi > VSPlCCab';
  (*Lct*) VAR_FATID_1043: Result := 'Comiss�o de WB/Semi > VSPlCCab';

  //
  (*Nfe*) VAR_FATID_1110: Result := 'Baixa por Pesagem no Blue Derm (Uso e consumo)';
  (*Nfe*) VAR_FATID_1111: Result := 'Baixa por Consumo Extra no Blue Derm (Uso e consumo)';


  (*Nfe*) VAR_FATID_1150: Result := 'Baixa ETE no BlueDerm (Uso e consumo)';
  (*Nfe*) VAR_FATID_1170: Result := 'Devolu��o de Outros no Blue Derm (Uso e consumo)';
  (*Nfe*) VAR_FATID_1190: Result := 'Baixa de Outros no Blue Derm (Uso e consumo)';
  //
  (*Nfe*) VAR_FATID_1801: Result := 'Comiss�es de vendas'; // NFe FatID_Sub > (0=Desconhecido, 1=Faturamento, 2=Recebimento total 3=Recebimento a cada parcela): Result := '
  (*Nfe*) VAR_FATID_1901: Result := 'Frete SPEC EFD'; //  (2101? - mudei 2011-08-20 antes de come�ar a usar!)';
  //
  (*Lct*) VAR_FATID_2101: Result := (*Academy -> *)'Pagamento de Matr�cula';
  (*Lct*) VAR_FATID_2102: Result := (*Academy -> *)'Compra de mercadorias diversas';
  (*Lct*) VAR_FATID_2103: Result := (*Academy -> *)'Transporte de mercadorias compradas';
  (*Lct*) VAR_FATID_2105: Result := (*Academy -> *)'Venda de mercadorias diversas';

  (*Lct*) VAR_FATID_3001: Result := (*'ToolRent -> *)'Faturamento parcial/ final de loca��o';
  (*Lct*) VAR_FATID_3002: Result := (*'ToolRent -> *)'Faturamento de servi�os (SrvL...)';
  (*Lct*) VAR_FATID_3011: Result := (*'ToolRent -> *)'Pagamento de honor�rios (SrvL...)';
  (*Lct*) VAR_FATID_3020: Result := (*'ToolRent -> *)'Transfer�ncia do estoque para disponibiliza��o para loca��o';
  (*Lct*) VAR_FATID_3021: Result := (*'ToolRent -> *)'Entrada para loca��o sem transferir do estoque';
  (*Lct*) VAR_FATID_3030: Result := (*'ToolRent -> *)'Remo��o da loca��o devolvendo para o estoque';
  (*Lct*) VAR_FATID_3031: Result := (*'ToolRent -> *)'Remo��o da loca��o sem devolver para o estoque';

  (*Lct*) VAR_FATID_4001: Result := (*'Bugstrol -> *)'Faturamento de servi�o';
  (*Nfe*) VAR_FATID_4101: Result := (*'Bugstrol -> *)'Baixa na OS (tabela osfrmrec)';
  (*Nfe*) VAR_FATID_4102: Result := (*'Bugstrol -> *)'Baixa na OS (tabela osmomrec)';
  (*Nfe*) VAR_FATID_4103: Result := (*'Bugstrol -> *)'Baixa na OS (tabela ospipitspr > adiciona)';
  (*Nfe*) VAR_FATID_4104: Result := (*'Bugstrol -> *)'Baixa na OS (tabela ospipitspr > substitui)';

  (*Nfe*) VAR_FATID_4201: Result := (*YROd ->*)'Baixa de Remessa de Consigna��o';
  (*Nfe*) VAR_FATID_4202: Result := (*YROd ->*)'Entrada de Remessa de Consigna��o';
  (*Nfe*) VAR_FATID_4203: Result := (*YROd ->*)'Baixa de Retorno de Consigna��o';
  (*Nfe*) VAR_FATID_4204: Result := (*YROd ->*)'Entrada de Retorno de Consigna��o';
  (*Nfe*) VAR_FATID_4205: Result := (*YROd ->*)'Baixa por Venda de Consignado';
  (*Nfe*) VAR_FATID_4206: Result := (*YROd ->*)'Devolu��o de Venda de Consignado';
  (*Lct*) VAR_FATID_4211: Result := (*YROd ->*)'Frete de Remessa de Consigna��o';
  (*Lct*) VAR_FATID_4213: Result := (*YROd ->*)'Frete de Retorno de Consigna��o';
  (*Lct*) VAR_FATID_4215: Result := (*YROd ->*)'Frete de Venda de Consignado';
  (*Lct*) VAR_FATID_4216: Result := (*YROd ->*)'Frete de Devolu��o de Venda de Consignado';

  (*CTe*) VAR_FATID_5001: Result := 'Faturamento de frete';

  (*MDFe*)VAR_FATID_6001: Result := 'Manifesto de Carga';
  //
  (*Lct*) VAR_FATID_7001: Result := (*YROd -> *)'Faturamento parcial/ final de Venda / servi�os';

  (*Agenda*)VAR_FATID_8001: Result := (*Agenda -> *)'Tarefas � partir de lan�amentos financeiros';


  (*Nfe*) VAR_FATID_999999999: Result := 'Emiss�o manual de NFe';
    else Result := ''
  end;
end;

function TUnDmkEnums.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy: Result := 'C�pia';
    stDel: Result := 'Exclus�o';
    stIns: Result := 'Inclus�o';
    stLok: Result := 'Travado';
    stUpd: Result := 'Altera��o';
    stUnd: Result := 'Desiste';
    stPsq: Result := 'Pesquisa';
    else   Result := '???????';
  end;
end;

function TUnDmkEnums.ObtemStr_LogActnID(LogActnID: TLogActnID): String;
begin
  if Integer(High(TLogActnID)) > 1 then
    raise Exception.Create('"ObtemStr_LogActnID" aqu�m do m�ximo!');
  case LogActnID of
    laiOSWCab: LowerCase('OSWCab');
    else (*laiNone:*) Result := '???';
  end;
end;

function TUnDmkEnums.TextoToSQLType(Tipo: String): TSQLType;
begin
  if Tipo = 'C�pia' then      Result := stCpy else
  if Tipo = 'Exclus�o' then   Result := stDel else
  if Tipo = 'Inclus�o' then   Result := stIns else
  if Tipo = 'Travado' then    Result := stLok else
  if Tipo = 'Altera��o' then  Result := stUpd else
                              Result := stNil;
end;

end.

{
=======
unit UnDmkEnums;

interface


const
  CO_ShowKindList_0_RealIndx = 0;
  CO_ShowKindList_1_ShowKind = 1;
  CO_ShowKindList_2_ToolKind = 2;
  //
  //
  MaxPixelCount = 65536;

type
  //Temporario!!!!!!!!!!
  TConvGrandeza = (cgMtoFT, cgFTtoM, cgKGtoLB, cgLBtoKG);
  // Aplicativos
  // B U G S T R O L
  TGraBugsOpera = (gboNenhum, gboServi, gboCaixa);
  TGraBugsServi = (gbsIndef, gbsAplEMon, gbsAplica, gbsMonitora, gbsMonMulti);
  TGraBugsMater = (gbmIndef, gbmEquiEProd, gbmEquipam, gbmProduto);
  TNivelRatifConsumoOS = (nrcosNenhum=0, nrcosProduto=1, nrcosFormula=2,
                         nrcosServico=3, nrcosLocalizador=4);
  // FIM  B U G S T R O L

  // B L U E   D E R M
  TImpDesnate = (impdsnDenate, impdsnDenSubUni, impdsnDenSubAll );
  TTipoCalcCouro = (ptcNaoInfo=0, ptcPecas=1, ptcPesoKg=2, ptcAreaM2=3,
                    ptcAreaP2=4, ptcTotal=5);
  TEstqMovimID = (emidAjuste=0, emidCompra=1, emidVenda=2,
                 emidReclasWE=3, emidBaixa=4, emidIndsWE=5, emidIndsVS=6,
                 emidClassArtVSUni=7, emidReclasVSUni=8, emidForcado=9,
                 emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
                 emidInventario=13, emidClassArtVSMul=14, emidPreReclasse=15,
                 emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,
                 emidEmProcWE=19, emidFinished=20, emidDevolucao=21,
                 emidRetrabalho=22, emidGeraSubProd=23, emidReclasVSMul=24,
                 emidTransfLoc=25);
  TEstqMovimNiv = (eminSemNiv=0, eminSorcClass=1, eminDestClass=2,
                   eminSorcIndsWB=3, eminDestIndsWB=4, eminSorcReclass=5,
                   eminDestReclass=6, eminSorcOper=7, eminEmOperInn=8,
                   eminDestOper=9, eminEmOperBxa=10, eminSorcPreReclas=11,
                   eminDestPreReclas=12, eminDestCurtiVS=13, eminSorcCurtiVS=14,
                   eminBaixCurtiVS=15, eminSdoArtInNat=16, eminSdoArtGerado=17,
                   eminSdoArtClassif=18, eminSdoArtEmOper=19, eminSorcWEnd=20,
                   eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23,
                   eminSdoArtEmWEnd=24, eminSdoFinish=25, eminSdoSubPrd=26,
                   eminSorcLocal=27, eminDestLocal=28);
  TEstqMovimType = (emitIndef=0, emitIME_I=1, emitIME_C=2, emitIME_P=3,
                    emitPallet=4, emitAvulsos=5, emitAgrupTotalizador=6,
                    emitAgrupItensNew=7, emitAgrupItensBxa=8);
  TEstqDefMulFldEMxx = (edmfIndef=0, edmfSrcNiv2=1, edmfMovCod=2, edmfIMEI=3);
  TVSLnkIDXtr = (lixIndefinido=0, lixPedVenda=1);
  TXXStatPall = (vsspIndefinido=0, vsspMontando=1, vsspDesmontando=2,
                 vsspMontEDesmo=3, vsspEncerrado=4, vsspEncerMont=5,
                 vsspEncerDesmo=6, vsspEncerMontEDesmo=7, vsspRemovido=8);
  TEstqEditGB = (eegbNone=0, eegbQtdOriginal=1, eegbDadosArtigo=2);
  TEstqMotivDel = (emtdWetCurti013=1, emtdWetCurti045=2, emtdWetCurti104=3,
                   emtdWetCurti008A=4, emtdWetCurti008B=5, emtdWetCurti079=6,
                   emtdWetCurti022=7, emtdWetCurti028=8, emtdWetCurti038=9,
                   emtdWetCurti093=10, emtdWetCurti078=11, emtdWetCurti006=12,
                   emtdWetCurti019=13, emtdWetCurti023=14, emtdWetCurti027=15,
                   emtdWetCurti060=16, emtdWetCurti076=17, emtdWetCurti106=18,
                   emtdWetCurti111=19, emtdWetCurti131=20, emtdWetCurti136=21);
  TEstqSubProd = (esubprdOriginal=0, esubprdDerivado=1);
  TEstqNivCtrl = (encPecas=0, encArea=1, encValor=2);
  TPQxTipo = (pqxtipoInventario=0, pqxtipoEntrada=10, pqxtipoPesagem=110,
              pqxtipoETE=150, pqxtipoOutros=190);
  // FIM  B L U E   D E R M

  // T O O L   R E N T
  TPrmRefQuem = (prqIndefinido=0, prqCliente=1, prqAgente=2);
  TPrmFatorValor = (pfvIndefinido=0, pfvValorTotal=1, pfvValorUnitario=2);
  TPrmFormaRateio = (pfrIndefinido=0, pfrValorTotal=1, pfrValorPorPessoa=2);
  TSrvLCtbDsp = (slcdUnknownToInf=0, slcdNoGerCtaAPag=1, slcdDescoFaturam=2,
                 slcdGeraCtaAPagr=3);
  // FIM  T O O L   R E N T

  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  CT-e
  TTipoConsumoWS_CTe = (tcwscteStatusServico=0, tcwscteRecepcao=1, tcwscteRetRecepcao=2,
                   tcwscteEveCancelamento=3, tcwsctePediInutilizacao=4, tcwscteConsultaCTe=5,
                   tcwscteEveCartaDeCorrecao=6);
  TCTeTpVeic = (ctetpveicTracao=0, ctetpveicReboque=1);
  TCTeTpRod = (ctetprodNaoAplicavel=0, ctetprodTruck=1, ctetprodToco=2,
               ctetprodCavaloMec=3, ctetprodVAN=4, ctetprodUtilitario=5,
               ctetprodOutros=6);
  TCTeTpCar = (ctetpcarNaoAplicavel=0, ctetpcarAberta=1, ctetpcarFrchadaBau=2,
               ctetpcarGranelera=3, ctetpcarPortaContainer=4, ctetpcarSider=5);
  TCTeTpCTe = (ctetpcteNormal=0, ctetpcteComplValrs=1, ctetpcteAnulacao=2,
               ctetpcteSubstituto=3);
  TCTeMyCST = (ctemycstIndefinido=0, ctemycstCST00Normal=1, ctemycstCST20BCRed=2,
               ctemycstCST40Isensao=3, ctemycstCST41NaoTrib=4, ctemycstCST51Diferido=5,
               ctemycstCST60ST=6, ctemycstCST90Outros=7, ctemycstCST90OutraUF=8,
               ctemycstCSTSimplesNacional=9);
  TCTeForPag = (cteforpagPago=0, cteforpagAPagar=1, cteforpagOutros=2);
  TCTeModal = (ctemodalIndefinido=0, ctemodalRodoviario=1, ctemodalAereo=2,
                ctemodalAquaviario=3, ctemodalFerroviario=4,
                ctemodalDutoviario=5, ctemodalMultimodal=6);
  TCTeTpServ = (ctetpservNormal=0, ctetpservSubcontratacao=1, ctetpservRedespacho=2,
                ctetpservRedespInterm=3, ctetpservServVincMultimodal=4);
  TCTeToma = (ctetomaRemetente=0, ctetomaExpedidor=1, ctetomaRecebedor=2,
              ctetomaDestinatario=3, ctetomaOutros=4);
  TCTeCUnid = (ctecunidM3=0, ctecunidKG=1, ctecunidTON=2, ctecunidUNIDADE=3,
               ctecunidLITROS=4, ctecunidMMBTU=5);
  TCTeMyStatus = (ctemystatusDesencerrada=8, ctemystatusEncerrada=9,
                  ctemystatusCTeDados=10, ctemystatusCTeGerada=20,
                  ctemystatusCTeAssinada=30, ctemystatusLoteRejeitado=40,
                  ctemystatusCTeAddedLote=50, ctemystatusLoteEnvEnviado=60,
                  ctemystatusLoteEnvConsulta=70);
  TCTeMyTpDocInf = (ctemtdiIndef=0, ctemtdiNFe=1, ctemtdiNFsOld=2,
               ctemtdiOutrosDoc=3);
  TCTeServicoStep = (ctesrvStatusServico=0, (*ctesrvEnvioAssincronoLoteCTe*)ctesrvEnvioLoteCTe=1,
                  ctesrvConsultarLoteCTeEnviado=2, ctesrvPedirCancelamentoCTe=3,
                  ctesrvPedirInutilizacaoNumerosCTe=4, ctesrvConsultarCTe=5,
                  ctesrvEventoCartaCorrecao=6(*, ctesrvConsultaCadastroEntidade=7,
                  ctesrvConsultaSitua��oCTe=8 (*inoperante*)(*,
                  ctesrvConsultaCTesDestinadas=9, ctesrvDownloadCTes=10,
                  ctesrvConsultaDistribuicaoDFeInteresse=11*));
  TCTeRodoLota =(cterodolotaNao=0, cterodolotaSim=1);
  TCTePropTpProp =(cteptpTAC_Agregado=0, cteptpTAC_Independente=1, cteptpTAC_Outros=2);
  TCTeRespSeg = (ctersRemetente=0, ctersExpedidor=1, ctersRecebedor=2,
                 ctersDestinatario=3, ctersEmitenteDoCTe=4, ctersTomadorDoServico=5);
  TEventosXXe = (evexxCCe,  // Carta de corre��o (NF-e, CT-e)
                 evexxCan,  // Cancelamento (NFe, CT-e, MDF-e)
                 evexxEnc,   // Encerramento (MDF-e)
                 evexxIdC    // Inclusao Condutor (MDF-e)
                 (*eveMDe*)); // Manifesta��o do deestinat�rio
  ///  FIM CT-e


  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  MDF-e
  TTipoConsumoWS_MDFe = (tcwsmdfeStatusServico=0, tcwsmdfeRecepcao=1, tcwsmdfeRetRecepcao=2,
                   tcwsmdfeEveCancelamento=3, tcwsmdfePediInutilizacao=4,
                   tcwsmdfe5=5, tcwsmdfeEveIncCondutor=6, tcwsmdfe7=7, tcwsmdfeEveEncerramento=8,
                   tcwsmdfeConsultaNaoEncerrados=9 );
  TMDFeTipoEmitente = (mdfetpemitIndef=0, mdfetpemitPrestadorServico=1, mdfetpemiCargaPropria=2);
  TMDFeModal = (mdfemodalIndefinido=0, mdfemodalRodoviario=1, mdfemodalAereo=2,
                mdfemodalAquaviario=3, mdfemodalFerroviario=4);
  TMDFeMyStatus = (mdfemystatusDesencerrada=8, mdfemystatusEncerrada=9,
                  mdfemystatusMDFeDados=10, mdfemystatusMDFeGerada=20,
                  mdfemystatusMDFeAssinada=30, mdfemystatusLoteRejeitado=40,
                  mdfemystatusMDFeAddedLote=50, mdfemystatusLoteEnvEnviado=60,
                  mdfemystatusLoteEnvConsulta=70);
  TMDFeServicoStep = (mdfesrvStatusServico=0, (*mdfesrvEnvioAssincronoLoteMDFe*)mdfesrvEnvioLoteMDFe=1,
                  mdfesrvConsultarLoteMDFeEnviado=2, mdfesrvEventoCancelamento=3,
                  (*mdfesrvPedirInutilizacaoNumerosMDFe=4, mdfesrvConsultarMDFe=5,*)
                  mdfesrvEventoIncCondutor=6, (*mdfesrvConsultaCadastroEntidade=7,*)
                  mdfesrvEventoEncerramento=8 (*inoperante???*)(*,
                  mdfesrvConsultaNaoEncerrados=9(*, mdfesrvDownloadMDFes=10,
                  mdfesrvConsultaDistribuicaoDFeInteresse=11*));
  TMDFeCargaCUnid = (mdfeccuIndef=0, mdfeccuKG=1, mdfeccuTON=2);

  ///  FIM MDF-e


  // TODOS
  TTipoXXe = (tipoxxeNFe=55, tipoxxeCTe=57, tipoxxeMDFe=58);
  TTipoNoXML = (tnxAttrStr, tnxTextStr);
  TStatusOSExec = (soseNaoDefinido=0, soseEmSer=1, soseExecutada=2,
    soseCancelada=3);
  TdmkAppID= (dmkappB_L_U_E_D_E_R_M=2, dmkappS_Y_N_D_I_C=4,  dmkappB_U_G_S_T_R_O_L=24);
  // FIM Aplicativos
  //TDmkTypeDiary = (dtdiarNone=0, dtdiarSAC=1, dtdiarSimple=2);
  TDmkModuloApp = (mdlappNenhum=0, mdlappAppMain=1,mdlappAllBase=2,
                   mdlappEntidades=3, mdlappEmpresas=4, mdlappAgenda=5,
                   mdlappAnotacoes=6, mdlappBancos=7, mdlappBloquetos=8,
                   mdlappCNAB=9, mdlappContratos=10, mdlappCRO=11,
                   mdlappDiario=12, mdlappEmail=13, mdlappEstoque=14,
                   mdlappFavoritos=15, mdlappFinanceiro=16, mdlappFPMin=17,
                   mdlappFPMax=18, mdlappFTP=19, mdlappGrade=20, mdlappNFe=21,
                   mdlappNFSe=22, mdlappPerfis=23, mdlappProtocolos=24,
                   mdlappSAC=25, mdlappSAF=26, mdlappSPED=27, mdlappWEB=28,
                   mdlappWTextos=29, mdlappCuns=30, mdlappBina=31,
                   mdlappConciBco=32, mdlappGFat=33, mdlappGPed=34);
  TArrayListaEstatus = array of array[0..5] of Integer;
  TDataTipo = (dtMySQL, dtSystem, dtSystem2, dtSystem3, dtTexto, dtDelphi);
  TfrxImpComo = (ficMostra, ficImprime, ficSalva, ficExporta, ficNone);
  TFormCadEnti = (fmcadSelecionar, fmcadEntidades, fmcadEntidade2);
  TDmkDBMSs = (dbmsUndef=0, dbmsMySQL=1, dbmsSQLite=2, dbmsIBLite=3);
  TDmkDBSrc = (dbsrcLocalServer=0, dbsrcLocalUser=1, dbsrcWebServer=2, dbsrcWebUser=3);
  dmkTbCtrl = (tctrlCad=0, tctrlAlt=1, tctrlLok=2, (*tctrlDel, *)tctrlWeb=3, tctrlAti=4);
  TAcaoCriaTabela   = (actCreate=0, actDescribe=1);
  TResultVerify = (rvOK=0, rvErr=1, rvAbort=2);
  TMyMotivo = (mymotDifere=0, mymotSemRef=1);
  TSQLType    = (stIns=0, stUpd=1, stDel=2, stCpy=3, stLok=4, stUnd=5, stPsq=6, stNil=7);
  TUsoXMLQuery = (uxqNoSQL=0, uxqSQLite=1, uxqMySQL=2);
  TAcessFmModo = (afmoSoMaster=0, afmoSoAdmin=1, afmoSoBoss=2, afmoNegarSemAviso=3,
                  afmoNegarComAviso=4, afmoLiberado=5, afmoSemVerificar=6);
  TTipoSinal = (tsPos=0, tsNeg=1, tsDef=2);
  TLogActnID = (laiNone=0, laiOSWCab=1, laiOSWPipMon=2, laiOSPipIts=3);
  TLogAcaoExe = (laeNone=0, laeDownload=1, laeUpload=2, laeSelected=3,
                 laeInsert=4, laeUpdated=5, laeDeleted=6, laeFinished=7);
  TFormaCalcPercent = (fcpercDesconto, fcpercAcrescimo, fcpercPrecoTax);
  TWBRclass = (wbrEntrada, wbrEstoque);
  TEnvioCNAB = (ecnabIndefinido, ecnabRemessa, ecnabRetorno, ecnabBloquet);
  TFormaWBIndsWE = (fiwNone, fiwPesagem, fiwBaixaPrevia);
  TRelAbrange = (relNada, relAbrangeEmpresa, relAbrangeCliente);
  TValCelRecXLS = (vcrxNenhum=0, vcrxSetor=1, vcrxCliente=2, vcrxTecnico=3,
    vcrxAccount=4, vcrxArtigo=5, vcrxEspessura=6, vcrxRendiment=7,
    vcrxRelatA=8, vcrxPesoCouro=9, vcrxCambio=10, vcrxQuantia=11, vcrxData=12,
    vcrxRebaixe=13);
  TFileEncodingType = (fetAnsi=0, fetUnicode=1, fetUnicodeBigEndian=2, fetUTF_8=3);
  TDateEncodeType = (detJustSum=0, detLastDaySameMonth=1, detFirstDayNextMonth=2);
  TAllFormat  = (dmktfNone, dmktfString, dmktfDouble, dmktfInteger, dmktfLongint,
                 dmktfInt64, dmktfDate, dmktfTime, dmktfDateTime, dmktfMesAno,
                 dmktf_AAAAMM,
                 // Deve ser o �ltimo:
                 dmktfUnknown);
  TdmkDataFmt = (dmkdfShort, dmkdfLong);
  TdmkHoraFmt = (dmkhfShort=0, dmkhfLong=1, dmkhfMiliSeconds=2, dmkhfExtended=3,
                 dmkhfTZD_UTC=4);
  TDmkSpidAct = (dsaUnkn=0, dsaFrst=1, dsaPrio=2, dsaThis=3, dsaNext=4,
                dsaLast=5, dsaPesq=6, dsaDesc=7, dsaConf=8, dsaCanc=9,
                dsaHome=10, dsaPlus=11, dsaEdit=12, dsaKill=13, dsaImpr=14,
                dsaCfgs=15, dsaClse=16, dsaOlho=17);
  TKndFmDmk = (kfdNoDef=0, kfdNoXtra=1, kfdCadTypA=2);
  TDmkTabKnd = (dtkMainForm=0, dtkOther=1, dtkNone=2);
  TUpdType    = (utYes=0, utNil=1, utIdx=2, utInc=3);
  TMskType    = (fmtNone, fmtCPFJ, fmtCEP, fmtTelCurto, fmtTelLongo, fmtNCM,
                 fmtCBO2002, fmtIE, fmtNumRua, fmtUF, fmtVersao, fmtCPFJ_NFe,
                 fmtCEP_NFe, fmtTel_NFe, fmtIE_NFe, fmtChaveNFe);
  TComoFmtChNFe = (cfcnNone, cfcnDANFE, cfcnFrendly);
  TDmkCanEdit = (dceOnInsUpd=0, dceNever=1, dceAllways=2);
  TNovoCodigo = (ncIdefinido=0, ncControle=1, CtrlGeral=2, ncGerlSeq1=3);
  TDmkEditStyle = (desEdit=0, desClearingEdit=1, desTimeEdit=2, desComboEdit=2,
                desPasswordEdit=3);
  TReceitaTipoSetor = (recsetrNenhum, recsetrRibeira, recsetrRecurtimento, recsetrAcabamento);
  TAquemPag  = (apFornece=0, apCliente=1, apTranspo=2, apNinguem=3);
  TMinMax    = (mmNenhum=0, mmMinimo=1, mmMaximo=2, mmAmbos=3);
  TAppVerNivel = (avnAlfa=0, avnBeta=1, avnEstavel=2);
  TAppVerRevert = (avrNo=0, avrYes=1);
  // // 0 - N�o informado
     // 1 - EntiContat
     // 2 - Entidades
     // 3 - Avulso
  TClasseEnti = (classeentiEmpresa, classeentiCliente, classeentiTransporta);
  TNFeCTide_TipoDoc = (NFeTipoDoc_Desconhecido,
                       NFeTipoDoc_nfe_0, NFeTipoDoc_nfe_1,
                       NFeTipoDoc_enviNFe, NFeTipoDoc_nfeProc,
                       NFeTipoDoc_cancNFe, NFeTipoDoc_retCancNFe,
                       NFeTipoDoc_procCancNFe, NFeTipoDoc_inutNFe,
                       NFeTipoDoc_retInutNFe, NFeTipoDoc_procInutNFe);
  TNFeCodType = (nfeCTide_TipoDoc,  nfeCTide_tpNF,  nfeCTide_indPag,
                 nfeCTide_procEmi, nfeCTide_finNFe, nfeCTide_tpAmb,
                 nfeCTide_tpEmis, nfeCTide_tpImp,  nfeCTModFrete,
                 nfeCTide_indCont,
                 nfeCTide_idDest, nfeCTide_indFinal, nfeCTide_indPres,
                 nfeCTdest_indIEDest, nfeCTmotDesICMS);
  TNFeServicoStep = (nfesrvStatusServico=0, (*nfesrvEnvioAssincronoLoteNFe*)nfesrvEnvioLoteNFe=1,
                  nfesrvConsultarLoteNfeEnviado=2, nfesrvPedirCancelamentoNFe=3,
                  nfesrvPedirInutilizaCaoNumerosNFe=4, nfesrvConsultarNFe=5,
                  nfesrvEnviarLoteEventosNFe=6, nfesrvConsultaCadastroEntidade=7,
                  nfesrvConsultaSitua��oNFE=8 (*inoperante*),
                  nfesrvConsultaNFesDestinadas=9, nfesrvDownloadNFes=10,
                  nfesrvConsultaDistribuicaoDFeInteresse=11);
  TNFeDFeSchemas = (nfedfeschUnknown=0, nfedfeschResNFe=1, nfedfeschProcNFe=2,
                  nfedfeschResEvento=3, nfedfeschProcEvento=4);
  TXXeIndSinc = (nisAssincrono=0, nisSincrono=1);
  TNFeautXML = (naxNaoInfo=0, naxEntiContat=1, naxEntidades=2, naxAvulso=3);
  TNFEAgruRelNFs = (nfearnIndef=0, nfearnCOFP=1, nfearnNCM=2, nfenfearnNatOP=3);
  TIBPTaxTabs = (ibptaxtabNCM=0, ibptaxtabNBS=1, ibptaxtabLC116=2);
  TIBPTaxOrig = (ibptaxoriNaoInfo=0, ibptaxoriNacional=1, ibptaxoriImportado=2);
  TIBPTaxFont = (ibptaxfontNaoInfo=0, ibptaxfontIBPTax=1, ibptaxfontCalculoProprio=2);
  TTpCalcTribNFe = (tctnfeIndefinido=0, tctnfeAutomatico=1, tctnfeManual=2,
                    tctnfeMixto=3);
  TNFeIndFinal = (indfinalNormal=0, indfinalConsumidorFinal=1);
  TindFinalTribNFe = (infFinTribIndefinido=0, infFinTribSoConsumidorFinal=1,
                      infFinTribTodos=2);
  TcSitNFe = (csitnfeDesconhecido=0, csitnfeAutorizado=1, csitnfeDenegado=2);
  TdmkCambios = (cambiosBRL=1, cambiosUSD=2, cambiosEUR=3, cambiosIDX=4);
  TNFeEventos = (nfeeveCCe=110110, nfeeveCan=110111,
                 nfeeveMDeConfirmacao=210200, nfeeveMDeCiencia=210210,
                 nfeeveMDeDesconhece=210220, nfeeveMDeNaoRealizou=210240,
                 nfeeveEPEC=110140);

(*  Desabilitado por causa do multiOS do XE5!
  //TKeyType    = (ktString, ktBoolean, ktInteger, ktCurrency, ktDate, ktTime);
*)

  TFormaCopiaInterfaceCompo = (fcicBitBlt, fcicPaintTo);
  TFormaFlipReverseBmp = (ffrbScanLine, ffrbCopyRect, ffrbStretchBlt);
  TUsaQualPredef = (uqpIndefinido, uqpADefinir, uqpEspecifico, uqpGeral);

  // TdmkDraw...
  TPolyPoint = array of TPoint;
  TPolyDot = array of Integer;
  TImgMouseAction = (maNenhum=0, maSelectImage=1, maSelectColor=2, maDrawPen=3,
                     maDrawBrush=4, maRubber=5, maMarquee=6, maArcCutIni=7,
                     maArcCutFim=8, maSelPenColor=9, maSelBrushColor=10);
  // TUnDmkChalk.NomeToolKind(ToolKind: TToolKind): String;
  TToolKind = (fdNenhum=0, fdLivre=1, fdLinha=2, fdRetangulo=3, fdElipse=4,
               fdPolyline=5, fdRoundRect=6, fdPolygon=7, fdTextBox=8,
               fdPolyBezier=9, fdArc=10, fdAngleArc=11, fdBrush=12,
               fdFigure=13, fdRectArc=14, fdMarquee=15, fdDrag=16);
  TToolKinds = set of TToolKind;
  TToolsUsed = array of array[0..1] of Integer;
  TShowKind = (skNenhuma=0, skDrawVetor=1, skTexto=2);
  TStrDraw = array of array[0..1] of TPoint;
  TStrDraws = array of TStrDraw;

  TShowKindList = array of array[0..2] of Integer;
  // FIM TdmkDraw...



  //
  TTemControle = set of dmkTbCtrl;
  //
  TAppGrupLst = array of array of String;
  TArrSelIdxInt1 = array of Integer;
  TArrSelIdxInt2 = array of array[0..1] of Integer;
  //
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  TcpCalc = (cpJurosMes, cpMulta);
  //
  TPartOuTodo = (ptNada=0, ptParcial=1, ptTotal=2);
  //
  TTribOperacao = (tribcalcNone=0, tribcalcInclusoBase=1, tribcalcExclusoBase=2(*,
                   tribcalcInclusoNivel=3, tribcalcExclusoNivel=4*));
  //
  TVSRestituicaoInn = (rivsNone=0, rivsPecas=1, rivsPesoKg=2, rivsSalKg=3,
                       rivsValor=4);
  TTabToWork = (ttwA=1, ttwB=2, ttwC=3, ttwD=4);
  //

  TUnDmkEnums = class(TObject)
  private
     Private declarations
  public
     Public declarations
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  TextoToSQLType(Tipo: String): TSQLType;
    function  ObtemStr_LogActnID(LogActnID: TLogActnID): String;

  end;
var
  DmkEnums: TUnDmkEnums;

(*
var
  DMK_TYPE_DIARY: TDmkTypeDiary = dtdiarNone;
*)

const
  // Aplicacao de Condicap de pagamento (Prazo)
  CO_APLIC_COND_PAG_FRETE = 8;
  //
  CO_ToolKindsPlainGeometry: TToolKinds = ([fdRetangulo, fdElipse, fdRoundRect, fdPolygon]);
  CO_ToolKindsPolyUpDown: TToolKinds = ([fdPolyLine, fdPolygon]);
  cTemControleSim: TTemControle = ([tctrlLok, tctrlCad, tctrlAlt, tctrlWeb, tctrlAti]);
  cTemControleNao: TTemControle = ([tctrlWeb, tctrlAti]);
  //
  CO_TXT_naxNaoInfo     = 'N�o informado';
  CO_TXT_naxEntiContat  = 'Contatos';
  CO_TXT_naxEntidade    = 'Entidade';
  CO_TXT_naxAvulso      = 'Avulso';
  MaxNFeautXML = Integer(High(TNFeautXML));
  sNFeautXML: array[0..MaxNFeautXML] of string = (
    CO_TXT_naxNaoInfo        , // 0
    CO_TXT_naxEntiContat     , // 1
    CO_TXT_naxEntidade       , // 2
    CO_TXT_naxAvulso           // 3
  );

  CO_TXT_uqpIndefinido  = 'Indefinido';
  CO_TXT_uqpADefinir    = 'A Definir';
  CO_TXT_uqpEspecifico  = 'Espec�fico';
  CO_TXT_uqpGeral       = 'Geral';
  MaxUsaQualPredef = Integer(High(TUsaQualPredef));
  sUsaQualPredef: array[0..MaxUsaQualPredef] of string = (
    CO_TXT_uqpIndefinido   , // 0
    CO_TXT_uqpADefinir     , // 1
    CO_TXT_uqpEspecifico   , // 2
    CO_TXT_uqpGeral         // 3
  );
  MaxListaStatusOSExec = Integer(High(TStatusOSExec));
  sListaStatusOSExec: array[0..MaxListaStatusOSExec] of String = (
  //soseNaoDefinido=0,
  'N�o definido',
  //soseEmSer=1,
  'Em ser',
  //soseExecutada=2,
  'Executada',
  //soseCancelada=3
  'Cancelada'
  );

  CO_TXT_tribcalcNone          = 'Indefinido';
  CO_TXT_tribcalcInclusoBase   = 'Incluso na base';
  CO_TXT_tribcalcExclusoBase   = 'Excluso na base';
  (*CO_TXT_tribcalcInclusoNivel  = 'Incluso no n�vel';
  CO_TXT_tribcalcExclusoNivel  = 'Excluso no n�vel';*)
  MaxTribOperacao = Integer(High(TTribOperacao));
  sListaTribOperacao: array[0..MaxTribOperacao] of string = (
  CO_TXT_tribcalcNone             , // 0
  CO_TXT_tribcalcInclusoBase      , // 1
  CO_TXT_tribcalcExclusoBase      (*, // 2
  CO_TXT_tribcalcInclusoNivel     , // 3
  CO_TXT_tribcalcExclusoNivel     // 4*)
  );

implementation

 TUnDmkEnums

function TUnDmkEnums.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy: Result := 'C�pia';
    stDel: Result := 'Exclus�o';
    stIns: Result := 'Inclus�o';
    stLok: Result := 'Travado';
    stUpd: Result := 'Altera��o';
    stUnd: Result := 'Desiste';
    stPsq: Result := 'Pesquisa';
    else   Result := '???????';
  end;
end;

function TUnDmkEnums.ObtemStr_LogActnID(LogActnID: TLogActnID): String;
begin
  if Integer(High(TLogActnID)) > 1 then
    raise Exception.Create('"ObtemStr_LogActnID" aqu�m do m�ximo!');
  case LogActnID of
    laiOSWCab: LowerCase('OSWCab');
    else (*laiNone:*) Result := '???';
  end;
end;

function TUnDmkEnums.TextoToSQLType(Tipo: String): TSQLType;
begin
  if Tipo = 'C�pia' then      Result := stCpy else
  if Tipo = 'Exclus�o' then   Result := stDel else
  if Tipo = 'Inclus�o' then   Result := stIns else
  if Tipo = 'Travado' then    Result := stLok else
  if Tipo = 'Altera��o' then  Result := stUpd else
                              Result := stNil;
end;

end.
}



