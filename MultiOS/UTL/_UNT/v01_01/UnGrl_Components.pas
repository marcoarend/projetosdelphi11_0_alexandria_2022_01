unit UnGrl_Components;

interface

uses System.Classes,
{$IFDEF DMK_FMX}
FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
FMX.ListView,
{$ENDIF}
System.Types;

type
  TUnGrl_Components = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ListViewAdd(ListView: TListView; Text: String;
              Description: String = '');
    procedure ListViewRemove(ListView: TListView; Index: Integer = -1);
    procedure ListViewClear(ListView: TListView);
    function  ListViewCount(ListView: TListView): Integer;
  end;

var
  Grl_Components: TUnGrl_Components;

implementation

{ TUnGrl_Components }

procedure TUnGrl_Components.ListViewAdd(ListView: TListView; Text: String;
  Description: String = '');
var
  Item: TListViewItem;
begin
  Item := ListView.Items.Add;
  Item.Text := Text;
  //
  if Description <> '' then
    Item.Detail := Description;
end;

procedure TUnGrl_Components.ListViewClear(ListView: TListView);
begin
  ListView.Items.Clear;
end;

function TUnGrl_Components.ListViewCount(ListView: TListView): Integer;
begin
  Result := ListView.Items.Count;
end;

procedure TUnGrl_Components.ListViewRemove(ListView: TListView;
  Index: Integer = -1);
var
  Item: TListItem;
begin
  if Index = -1 then
  begin
    Item := ListView.Selected;
    //
    if Item <> nil then
      ListView.Items.Delete(Item.Index);
  end else
    ListView.Items.Delete(Index);
end;

end.
