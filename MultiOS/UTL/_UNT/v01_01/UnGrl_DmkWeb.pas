unit UnGrl_DmkWeb;

interface

uses System.Classes, System.UITypes, System.SysUtils, System.Variants,
{$IF CompilerVersion > 29} (* Os componentes REST foram implementados no Seattle *)
REST.Client, REST.Types, Net.HttpClient,
{$IFEND}
IdHTTP, IpPeerClient;

type
  TUnGrl_DmkWeb = class(TObject)
  private
    {$IF CompilerVersion > 29}
    procedure ResetRESTComponentsToDefaults(RESTRequest: TRESTRequest;
              RESTResponse: TRESTResponse; RESTClient: TRESTClient);
    {$IFEND}
  public
    {$IF CompilerVersion > 29}
    function  REST_URL_Post(HeaderNam, HeaderVal, ParamsNam,
              ParamsVal: array of string; URL: String; var StatusCode: Integer;
              ContentType: String = 'XML'): String;
    function  REST_XML_PUT(HeaderNam, HeaderVal: array of string; ParamVal,
              URL: String; var StatusCode: Integer;
              ContentType: String = 'XML'): String;
    {$IFEND}
    function  VerificaConexaoWeb(var Msg: String): Integer;
    function  URLPut(URL: String; VarValue, Resultado: TStringStream;
              HeaderNome, HeaderValor: array of String): String;
    function  URLPost(URL: String; VarsValues: array of String;
              Resultado: TStringStream): Boolean;
    function  GetMeaningHTTP_CodeResponse(Code: Integer): String;

  end;

var
  Grl_dmkWeb: TUnGrl_DmkWeb;
const
  CO_DMK_CNPJ = '03143014000152';
  CO_URL = 'http://dermatek.com.br/api/';
  CO_URL_SECUR = 'https://dermatek.com.br/api/';
  CO_TOKEN = 'ws00ok0g0cwggcw8oscko4ks4k4oko8csk48kc40';

implementation

uses
{$IF DEFINED(iOS) or DEFINED(ANDROID)}
Android_FMX_NetworkState;
{$ELSE}
WiniNet, Windows;
{$IFEND}
//UnGrl_Geral;

{ TUnGrl_DmkWeb }

function TUnGrl_DmkWeb.VerificaConexaoWeb(var Msg: String): Integer;
var
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  NS: TUnFMX_NetworkState;
  {$ELSE}
  Conected: Boolean;
  Flags: dword;
  {$ENDIF}
  Res: Integer;
begin
  //-1 => Falha ao verificar
  // 0 => N�o conectado
  // 1 => Conectado via WIFI
  // 2 => Conectado via WWAN
  // 3 => Conectado via Proxy
  // 4 => Conectado via Modem Busy
  // Para Android permiss�o => Access Wifi State
  Msg := '';
  Res := 0;
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    NS := TUnFMX_NetworkState.Create;
    try
      if not NS.IsConnected then
        Res := 0
      else if NS.IsWifiConnected then
        Res := 1
      else if NS.IsMobileConnected then
        Res := 2
      else
        Res := -1;
      Msg := NS.CurrentSSID;
    finally
      NS.Free;
    end;
  {$ELSE}
    Conected := internetgetconnectedstate(@flags, 0);
    if Conected then
    begin
      if (flags and internet_connection_modem) = internet_connection_modem then
        Res := 2
      else if (flags and internet_connection_lan) = internet_connection_lan then
        Res := 1
      else if (flags and internet_connection_proxy) = internet_connection_proxy then
        Res := 3
      else if (flags and internet_connection_modem_busy) = internet_connection_modem_busy then
        Res := 4
      else
        Res := -1;
    end;
  {$ENDIF}
  Result := Res;
end;

function TUnGrl_DmkWeb.URLPut(URL: String; VarValue, Resultado: TStringStream;
  HeaderNome, HeaderValor: array of String): String;
var
  HTTP: TIdHTTP;
  i: Integer;
  Res: String;
begin
  Res  := '';
  //
  if Length(HeaderNome) <> Length(HeaderValor) then
  begin
    //Grl_Geral.MB_Aviso('O header deve possuir a mesma quantidade de itens para: Nome e Valor!');
    Exit;
  end;
  //
  HTTP := TIdHTTP.Create;
  //
  try
    if HTTP.Connected then
      HTTP.Disconnect;
    //
    HTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    //
    if Length(HeaderNome) > 0 then
    begin
      for i := 0 to Length(HeaderNome) do
      begin
        HTTP.Request.CustomHeaders.AddValue(HeaderNome[i], HeaderValor[i]);
      end;
    end;
    //
    Res := HTTP.Put(URL, VarValue);
    //
    Result := Res;
  finally
    HTTP.Free;
  end;
end;

{$if CompilerVersion > 29}
function TUnGrl_DmkWeb.REST_URL_Post(HeaderNam, HeaderVal, ParamsNam,
  ParamsVal: array of string; URL: String; var StatusCode: Integer;
  ContentType: String = 'XML'): String;
var
  Conexao, Resp, i: Integer;
  Msg: String;
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
begin
  StatusCode := 0;
  Result     := '';
  //
  if Length(ParamsNam) <> Length(ParamsVal) then
  begin
    //Grl_Geral.MB_Erro('A quantidade de par�metros e de valores de par�metros devem ser os mesmos!');
    Exit;
  end;
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    //Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
  Conexao := VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    (*Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');*)
    Exit;
  end;
  RESTRequest  := TRESTRequest.Create(nil);
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL     := URL;
    RESTClient.ContentType := ContentType;
    RESTRequest.Method     := TRESTRequestMethod.rmPOST;
    RESTRequest.Client     := RESTClient;
    RESTRequest.Response   := RESTResponse;
    //
    if Length(HeaderNam) > 0 then
    begin
      for i := Low(HeaderNam) to High(HeaderNam) do
      begin
        RESTRequest.Params.AddHeader(HeaderNam[i], HeaderVal[i]);
      end;
    end;
    //
    if Length(ParamsNam) > 0 then
    begin
      for i := Low(ParamsNam) to High(ParamsNam) do
      begin
        Param       := RESTRequest.Params.AddItem;
        Param.name  := UTF8Encode(ParamsNam[i]);
        Param.Value := UTF8Encode(ParamsVal[i]);
        Param.Kind  := TRESTRequestParameterKind.pkGETorPOST;
      end;
    end;
    //
    // Marco
    try
      RESTRequest.Execute;
    except
      //on E: ENetHTTPClientException do
      on E: Exception do
      begin
        StatusCode := -1;
        Result     := E.Message;
        (*Grl_Geral.MB_Erro('F M X - D m k W e b - R U P' + sLineBreak +
        'N�o foi poss�vel acessar o servidor!' +
        sLineBreak +
        'Caso esteja usando wifi sem acesso � internet, tente habilitar somente os dados m�veis!');*)
      end;
    end;
    if StatusCode = -1 then
      Exit;
    //
    StatusCode := RESTResponse.StatusCode;
    //
    Result := RESTResponse.Content;
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;
{$ifend}

{$if CompilerVersion > 29}
function TUnGrl_DmkWeb.REST_XML_PUT(HeaderNam, HeaderVal: array of string;
  ParamVal, URL: String; var StatusCode: Integer;
  ContentType: String = 'XML'): String;
var
  Conexao, Resp, i: Integer;
  Msg: String;
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
begin
  StatusCode := 0;
  Result     := '';
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    //Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
  Conexao := VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    (*Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');*)
    Exit;
  end;
  RESTRequest  := TRESTRequest.Create(nil);
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL     := URL;
    RESTClient.ContentType := ContentType;
    RESTRequest.Method     := TRESTRequestMethod.rmPUT;
    RESTRequest.Client     := RESTClient;
    RESTRequest.Response   := RESTResponse;
    //
    if Length(HeaderNam) > 0 then
    begin
      for i := Low(HeaderNam) to High(HeaderNam) do
      begin
        RESTRequest.Params.AddHeader(HeaderNam[i], HeaderVal[i]);
      end;
    end;
    //
    if Length(ParamVal) > 0 then
    begin
      Param             := RESTRequest.Params.AddItem;
      Param.ContentType := TRESTContentType.ctAPPLICATION_ATOM_XML;
      Param.Kind        := TRESTRequestParameterKind.pkREQUESTBODY;
      Param.Value       := ParamVal;
    end;
    //
    StatusCode := RESTResponse.StatusCode;
    //
    RESTRequest.Execute;
    //
    Result := RESTResponse.Content;
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;
{$ifend}

{$if CompilerVersion > 29}
procedure TUnGrl_DmkWeb.ResetRESTComponentsToDefaults(RESTRequest: TRESTRequest;
  RESTResponse: TRESTResponse; RESTClient: TRESTClient);
begin
  RESTRequest.ResetToDefaults;
  RESTResponse.ResetToDefaults;
  RESTClient.ResetToDefaults;
end;
{$ifend}

function TUnGrl_DmkWeb.GetMeaningHTTP_CodeResponse(Code: Integer): String;
begin
  case Code of
    //
    //ista de c�digos de status HTTP
    //
    //xx Informativa
    //.1
    100: Result :=  'Continuar';
    //.2
    101: Result :=  'Mudando protocolos';
    //.3
    102: Result :=  'Processamento (WebDAV) (RFC 2518)';
    //.4
    122: Result :=  'Pedido-URI muito longo';
    //
    //xx Sucesso
    //.1
    201: Result :=  'Criado';
    //.2
    202: Result :=  'Aceito';
    //.3
    203: Result :=  'n�o-autorizado (desde HTTP/1.1)';
    //.4
    204: Result :=  'Nenhum conte�do';
    //.5
    205: Result :=  'Reset';
    //.6
    206: Result :=  'Conte�do parcial';
    //.7
    207: Result :=  '-Status Multi (WebDAV) (RFC 4918)';
    //
    //xx Redirecionamento
    //.1
    300: Result :=  'M�ltipla escolha';
    //.2
    301: Result :=  'Movido';
    //.3
    302: Result :=  'Encontrado';
    //.4
    303: Result :=  'Consulte Outros';
    //.5
    304: Result :=  'N�o modificado';
    //.6
    305: Result :=  'Use Proxy (desde HTTP/1.1)';
    //.7
    306: Result :=  'Proxy Switch';
    //.8
    307: Result :=  'Redirecionamento tempor�rio (desde HTTP/1.1)';
    //.9
    308: Result :=  'Redirecionamento permanente (RFC 7538[2])';
    //
    //xx Erro de cliente
    //.1
    400: Result :=  'Requisi��o inv�lida';
    //.2
    401: Result :=  'N�o autorizado';
    //.3
    402: Result :=  'Pagamento necess�rio';
    //.4
    403: Result :=  'Proibido';
    //.5
    404: Result :=  'N�o encontrado';
    //.6
    405: Result :=  'M�todo n�o permitido';
    //.7
    406: Result :=  'N�o Aceit�vel';
    //.8
    407: Result :=  'Autentica��o de proxy necess�ria';
    //.9
    408: Result :=  'Tempo de requisi��o esgotou (Timeout)';
    //.10
    409: Result :=  'Conflito geral';
    //.11
    410: Result :=  'Gone';
    //.12
    411: Result :=  'comprimento necess�rio';
    //.13
    412: Result :=  'Pr�-condi��o falhou';
    //.14
    413: Result :=  'Entidade de solicita��o muito grande';
    //.15
    414: Result :=  'Pedido-URI Too Long';
    //.16
    415: Result :=  'Tipo de m�dia n�o suportado';
    //.17
    416: Result :=  'Solicitada de Faixa N�o Satisfat�ria';
    //.18
    417: Result :=  'Falha na expectativa';
    //.19
    418: Result :=  'Eu sou um bule de ch�';
    //.20
    422: Result :=  'Entidade improcess�vel (WebDAV) (RFC 4918)';
    //.21
    423: Result :=  'Fechado (WebDAV) (RFC 4918)';
    //.22
    424: Result :=  'Falha de Depend�ncia (WebDAV) (RFC 4918)';
    //.23
    425: Result :=  'cole��o n�o ordenada (RFC 3648)';
    //.24
    426: Result :=  'Upgrade Obrigat�rio (RFC 2817)';
    //.25
    429: Result :=  'pedidos em excesso';
    //.26
    450: Result :=  'bloqueados pelo Controle de Pais do Windows';
    //.27
//google
    451: Result :=  'Insdispon�vel por quest�es legais (RFC 7725) (google)';
// fim google
    499: Result :=  'cliente fechou Pedido (utilizado em ERPs/VPSA)';
    //
    //xx outros erros
    //.1
    500: Result :=  'Erro interno do servidor (Internal Server Error)';
    //.2
    501: Result :=  'N�o implementado (Not implemented)';
    //.3
    502: Result :=  'Bad Gateway';
    //.4
    503: Result :=  'Servi�o indispon�vel (Service Unavailable)';
    //.5
    504: Result :=  'Gateway Time-Out';
    //.6
    505: Result :=  'HTTP Version not supported';
    //
    // ....
    // ....
    // ....
    // ....
    // ....
    // ....
    //
    else Result :=  'C�digo de retorno HTTP n�o catalogado: ' + IntToStr(Code);
  end;
end;

function TUnGrl_DmkWeb.URLPost(URL: String; VarsValues: array of String;
  Resultado: TStringStream): Boolean;
var
  HTTP: TIdHTTP;
  Vars: TStringList;
  i: Integer;
  Valor: String;
begin
  Vars := TStringList.Create;
  HTTP := TIdHTTP.Create;
  try
    for i := 0 to Length(VarsValues) - 1 do
    begin
      Valor := VarsValues[i];
      if Valor <> '' then
        Vars.Add(Valor);
    end;
    //
    HTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    HTTP.Post(URL, Vars, Resultado);
    //
    Result := True;
  finally
    Vars.Free;
    HTTP.Free;
  end;
end;

end.
