unit UnGrl_Geral;

interface

uses System.SysUtils, System.Types, System.Classes, System.IOUtils, Data.DB,
{$IfDef DMK_FMX}
  FMX.Edit, FMX.Forms,
{$Else}
  Vcl.Controls, Vcl.Forms,
{$EndIf}
UnDmkEnums, IdCoderMIME;

type
  TUnGrl_Geral = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // M E N S A G E N S
    procedure Informa(Label1: TComponent; Aguarde: Boolean; Texto: String);
    function  Informa2(Label1, Label2: TComponent; Aguarde: Boolean; Texto: String): Boolean;
    function  MB_Erro(Text: String): Integer;
    function  MB_Aviso(Text: String): Integer;
    function  MB_Pergunta(Text: String): Integer;
    function  MB_Info(Text: String): Integer;
    function  MB_Teste(Text: String): Integer;
    function  MB_SQL(var Reference; Qry: TDataSet): Integer;
    function  FIC(FaltaInfoCompo: Boolean; Compo: TComponent; Mensagem: String;
              ExibeMsg: Boolean = True): Boolean;
    // N U M E R O S
    function  BoolToInt(Verdade: Boolean): Integer;
    {$IfDef DMK_FMX}
    function  CNPJ_Valida(const EdCNPJ: TCustomEdit; var CNPJ: String): Boolean;
    {$Else}
    function  CNPJ_Valida(const EdCNPJ: TWinControl; var CNPJ: String): Boolean;
    {$EndIf}
    function  CalculaCNPJCPF(Nume: String) : String;
    function  DMV(Texto: String): Double;
    function  FFT(Numero: Double; Casas: Integer; Sinal: TSinal): String;
    function  FFT_Dot(Numero: Double; Casas: Integer; Sinal: TSinal): String;
    function  IntToBool(Verdade: Integer): Boolean;
    function  SoNumero_TT(Texto : String) : String;
    function  SoNumeroELetra_TT(Texto : String) : String;
    function  SoNumeroESinal_TT(Texto : String) : String;
    function  SoNumeroESinalEVirgula_TT(Texto : String) : String;
    function  TFD(Texto: String; Digitos: Integer; Sinal: TSinal) : String;
    function  TFT(Texto: String; Casas: Integer; Sinal: TSinal): String;
    function  TFT_MinMax(Texto: String; Min, Max: Extended; Casas: Integer;
              Sinal: Boolean) : String;
    function  TFT_Min(Texto: String; Min: Double; Casas: Integer; Sinal:
              Boolean): String;
    function  TFT_Max(Texto: String; Max: Double; Casas: Integer; Sinal:
              Boolean): String;
    // F O R M A T A � � E S
    procedure DefineFormatacoes();
    function  IMV(Texto: String): Integer;
    function  FF0(Numero: Integer): String; overload;
    function  FF0(Numero: Int64): String; overload;
    function  FFN(Numero, Digitos: Integer): String;
    function  VariavelToStringAS(Variavel: Variant): String;
    function  VariavelToStringAD(Variavel: Variant): String;
    function  FDT(Data: TDateTime; Tipo: Integer; Nulo: Boolean = False): String;
    function  FormataCBO2002(CBO2002: String): String;
    function  FormataCEP_TT(CEP: String; Separador: String = '-';
              Default: String = ''): String;
    function  FormataChaveNFe(ChaveNFe: String; Como: TComoFmtChNFe): String;
    function  FormataCNPJ_TFT(Texto: String): String;
    function  Formata_IE(IE: String; UF: Variant; LinkMsk: String;
              Tipo: Integer = 0): String;
    function  FormataNCM(NCM: String): String;
    function  FormataNumeroDeRua(Rua: String; Numero: Variant; Nulo: Boolean): String;
    function  FormataTelefone_TT_Curto(Telefone : String): String;
    function  FormataTelefone_TT(Telefone : String): String;
    function  FormataTelefone_TT_NFe(Telefone : String): String;
    function  FormataVersao(Versao: Variant): String;
    function  FormataUF(UF: Variant): String;
    function  PareceTelefoneBR(Telefone: String): Boolean;
    function  GetCodigoUF_da_SiglaUF(SiglaUF: String): Integer;
    function  GetSiglaUF_do_CodigoUF(CodigoUF: Integer): String;
    function  VersaoTxt2006(Versao: Integer): String;
    function  SoNumeroEVirgula_TT(Texto: String) : String;
    // D A T A  /  H O R A
    function  DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
    function  AjustaDataHoraUTC(var dh: String; var UTC: Double): Boolean;
    function  MTD(Data: String; Obrigatorio: Boolean): TDateTime; // Mensal To DAte
    // D I R E T � R I O S
    procedure SalvaArquivoEmDocumentos(ArqConteudo, ArqNome, ArqExt: String);
    // O U T R O S
    function  ObtemAlterWebDeDeviceType(DeviceType: TDeviceType): TAlterWeb;
    function  DmkUtf8Encode(Texto: String): String;
    // S T R I N G S
    function  ATS(ListaSQL: array of string): String;
    function  Implode(Str: TStringList; Delimitador: String): String; overload;
    function  Implode(Str: TStrings; Delimitador: String): String; overload;
    function  Explode(Str, Separador: String; TamMin: Integer = 0): TStringList;
    // A P L I C A T I V O S
    function  LiberaModulo(Aplicativo: Integer; Modulo, HabilModulo: String;
              MostraMsg: Boolean = False): Boolean;
    function  ObtemSiglaModulo(Modulo: TDmkModuloApp): String;
    function  ObtemModuloDeSigla(Sigla: String): TdmkModuloApp;
    procedure InfoSemModulo(Modulo: TDmkModuloApp);
    function  ObtemModuloDeGrupoJan(Janela: String): String;
  end;
var
  Grl_Geral: TUnGrl_Geral;

implementation

uses
{$IfDef DMK_FMX}
  FMX.StdCtrls, FMX.ListBox,
  UnFMX_Geral, UnFMX_DmkProcFunc
{$Else}
  Vcl.StdCtrls,
  dmkGeral,
  {$IfDef uDmkPFMin}
    UnMinimumDmkProcFunc,
  {$Else}
    UnDmkProcFunc,
  {$EndIf}
  //
  {$IfDef uMyObjectsMin}
    UnMinimumMyObjects
  {$Else}
    UnMyObjects
  {$EndIf}
{$EndIf}
;

{ TUnGrl_Geral }

function TUnGrl_Geral.DmkUtf8Encode(Texto: String): String;
var
  aUTF8Str, Res: RawByteString;
  aUnicodeStr: UnicodeString;
begin
  aUTF8Str := UTF8Encode(Texto);
  SetCodePage(aUTF8Str, 0, False);
  aUnicodeStr := UnicodeString(aUTF8Str);
  Result := aUnicodeStr;
end;

function TUnGrl_Geral.DMV(Texto: String): Double;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.DMV(Texto);
  {$Else}
  Result := Geral.DMV(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.Explode(Str, Separador: String;
  TamMin: Integer): TStringList;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.Explode(Str, Separador, TamMin);
  {$Else}
  Result := Geral.Explode(Str, Separador, TamMin);
  {$EndIf}
end;

function TUnGrl_Geral.FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
  Mensagem: String; ExibeMsg: Boolean = True): Boolean;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FIC(FaltaInfoCompo, Mensagem, ExibeMsg);
  {$Else}
  Result := MyObjects.FIC(FaltaInfoCompo, Compo, Mensagem, ExibeMsg);
  {$EndIf}
end;

function TUnGrl_Geral.FormataCBO2002(CBO2002: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataCBO2002(CBO2002);
  {$Else}
  Result := Geral.FormataCBO2002(CBO2002);
  {$EndIf}
end;

function TUnGrl_Geral.FormataCEP_TT(CEP, Separador, Default: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataCEP_TT(CEP, Separador, Default);
  {$Else}
  Result := Geral.FormataCEP_TT(CEP, Separador, Default);
  {$EndIf}
end;

function TUnGrl_Geral.FormataChaveNFe(ChaveNFe: String;
  Como: TComoFmtChNFe): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataChaveNFe(ChaveNFe, Como);
  {$Else}
  Result := Geral.FormataChaveNFe(ChaveNFe, Como);
  {$EndIf}
end;

function TUnGrl_Geral.FormataCNPJ_TFT(Texto: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataCNPJ_TFT(Texto);
  {$Else}
  Result := Geral.FormataCNPJ_TFT(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.FormataNCM(NCM: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataNCM(NCM);
  {$Else}
  Result := Geral.FormataNCM(NCM);
  {$EndIf}
end;

function TUnGrl_Geral.FormataNumeroDeRua(Rua: String; Numero: Variant;
  Nulo: Boolean): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataNumeroDeRua(Rua, Numero, Nulo);
  {$Else}
  Result := Geral.FormataNumeroDeRua(Rua, Numero, Nulo);
  {$EndIf}
end;

function TUnGrl_Geral.FormataTelefone_TT(Telefone: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataTelefone_TT(Telefone);
  {$Else}
  Result := Geral.FormataTelefone_TT(Telefone);
  {$EndIf}
end;

function TUnGrl_Geral.FormataTelefone_TT_Curto(Telefone: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataTelefone_TT_Curto(Telefone);
  {$Else}
  Result := Geral.FormataTelefone_TT_Curto(Telefone);
  {$EndIf}
end;

function TUnGrl_Geral.FormataTelefone_TT_NFe(Telefone: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataTelefone_TT_NFe(Telefone);
  {$Else}
  Result := Geral.FormataTelefone_TT_NFe(Telefone);
  {$EndIf}
end;

function TUnGrl_Geral.FormataUF(UF: Variant): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataUF(UF);
  {$Else}
  Result := Geral.FormataUF(UF);
  {$EndIf}
end;

function TUnGrl_Geral.FormataVersao(Versao: Variant): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FormataVersao(Versao);
  {$Else}
  Result := Geral.FormataVersao(Versao);
  {$EndIf}
end;

function TUnGrl_Geral.Formata_IE(IE: String; UF: Variant; LinkMsk: String;
  Tipo: Integer): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.Formata_IE(IE, UF, LinkMsk, Tipo);
  {$Else}
  Result := Geral.Formata_IE(IE, UF, LinkMsk, Tipo);
  {$EndIf}
end;

function TUnGrl_Geral.GetCodigoUF_da_SiglaUF(SiglaUF: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.GetCodigoUF_da_SiglaUF(SiglaUF);
  {$Else}
  Result := Geral.GetCodigoUF_da_SiglaUF(SiglaUF);
  {$EndIf}
end;

function TUnGrl_Geral.GetSiglaUF_do_CodigoUF(CodigoUF: Integer): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.GetSiglaUF_do_CodigoUF(CodigoUF);
  {$Else}
  Result := Geral.GetSiglaUF_do_CodigoUF(CodigoUF);
  {$EndIf}
end;

function TUnGrl_Geral.FF0(Numero: Int64): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FF0(Numero);
  {$Else}
  Result := Geral.FF0(Numero);
  {$EndIf}
end;

function TUnGrl_Geral.FF0(Numero: Integer): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FF0(Numero);
  {$Else}
  Result := Geral.FF0(Numero);
  {$EndIf}
end;

function TUnGrl_Geral.FFN(Numero, Digitos: Integer): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FFN(Numero, Digitos);
  {$Else}
  Result := Geral.FFN(Numero, Digitos);
  {$EndIf}
end;

function TUnGrl_Geral.FFT(Numero: Double; Casas: Integer;
  Sinal: TSinal): String;
begin
  {$IfDef DMK_FMX}
    Result := FMX_Geral.FFT(Numero, Casas, Sinal);
  {$Else}
    Result := Geral.FFT(Numero, Casas, Sinal);
  {$EndIf}
end;

function TUnGrl_Geral.FFT_Dot(Numero: Double; Casas: Integer;
  Sinal: TSinal): String;
begin
  {$IfDef DMK_FMX}
    Result := FMX_Geral.FFT_Dot(Numero, Casas, Sinal);
  {$Else}
    Result := Geral.FFT_Dot(Numero, Casas, Sinal);
  {$EndIf}
end;

function TUnGrl_Geral.Implode(Str: TStringList; Delimitador: String): String;
begin
  {$IfDef DMK_FMX}
  FMX_Geral.MB_Aviso('Fun��o n�o implementada para esta plataforma!' + sLineBreak +
    'TUnGrl_Geral.Implode(Str: TStringList; Delimitador: String): String;');
  Result := '';
  {$Else}
  Result := Geral.Implode(Str, Delimitador);
  {$EndIf}
end;

function TUnGrl_Geral.Implode(Str: TStrings; Delimitador: String): String;
var
  I: Integer;
begin
  // N�o testado!!!!!
  Result := '';
  if Str.Count > 0 then
  begin
    for I := 0 to Str.Count - 2 do
      Result := Result + Str[I] + Delimitador;
    //
    Result := Result + Str[Str.Count - 1];
  end;
end;

function TUnGrl_Geral.IMV(Texto: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.IMV(Texto);
  {$Else}
  Result := Geral.IMV(Texto);
  {$EndIf}
end;

procedure TUnGrl_Geral.Informa(Label1: TComponent; Aguarde: Boolean; Texto: String);
begin
  {$IfDef DMK_FMX}
  FMX_dmkPF.Informa(TListBoxItem(Label1), Aguarde, Texto);
  {$Else}
  MyObjects.Informa(TLabel(Label1), Aguarde, Texto);
  {$EndIf}
end;

function TUnGrl_Geral.Informa2(Label1, Label2: TComponent; Aguarde: Boolean;
  Texto: String): Boolean;
begin
  {$IfDef DMK_FMX}
  Result := FMX_dmkPF.Informa2(TListBoxItem(Label1), TListBoxItem(Label2),
            Aguarde, Texto);
  {$Else}
  Result := MyObjects.Informa2(TLabel(Label1), TLabel(Label2), Aguarde, Texto);
  {$EndIf}
end;

procedure TUnGrl_Geral.InfoSemModulo(Modulo: TDmkModuloApp);
var
  Texto: String;
begin
  case Modulo of
    (*0*)mdlappNenhum     : Texto := 'Nenhum';
    (*01*)mdlappAppMain   : Texto := 'AppMain';
    (*02*)mdlappAllBase   : Texto := 'AllBase';
    (*03*)mdlappEntidades : Texto := 'Entidades';
    (*04*)mdlappEmpresas  : Texto := 'Empresas';
    (*05*)mdlappAgenda    : Texto := 'Agenda';
    (*06*)mdlappAnotacoes : Texto := 'Anotacoes';
    (*07*)mdlappBancos    : Texto := 'Bancos';
    (*08*)mdlappBloquetos : Texto := 'Bloquetos';
    (*09*)mdlappCNAB      : Texto := 'CNAB';
    (*10*)mdlappContratos : Texto := 'Contratos';
    (*11*)mdlappCRO       : Texto := 'CRO';
    (*12*)mdlappDiario    : Texto := 'Diario';
    (*13*)mdlappEmail     : Texto := 'E-mail';
    (*14*)mdlappEstoque   : Texto := 'Estoque';
    (*15*)mdlappFavoritos : Texto := 'Favoritos';
    (*16*)mdlappFinanceiro: Texto := 'Financeiro';
    (*17*)mdlappFPMin     : Texto := 'FPMin';
    (*18*)mdlappFPMax     : Texto := 'FPMax';
    (*19*)mdlappFTP       : Texto := 'FTP';
    (*20*)mdlappGrade     : Texto := 'Grade';
    (*21*)mdlappNFe       : Texto := 'NFe';
    (*22*)mdlappNFSe      : Texto := 'NFSe';
    (*23*)mdlappPerfis    : Texto := 'Perfis';
    (*24*)mdlappProtocolos: Texto := 'Protocolos';
    (*25*)mdlappSAC       : Texto := 'SAC';
    (*26*)mdlappSAF       : Texto := 'SAF';
    (*27*)mdlappSPED      : Texto := 'SPED';
    (*28*)mdlappWEB       : Texto := 'WEB';
    (*29*)mdlappWTextos   : Texto := 'WTextos';
    (*30*)mdlappCuns      : Texto := 'Cuns';
    (*31*)mdlappBINA      : Texto := 'BINA';
    (*32*)mdlappConciBco  : Texto := 'CMEB';
    (*33*)mdlappGFat      : Texto := 'GFat';
    (*34*)mdlappGPed      : Texto := 'GPed';
    (*35*)mdlappVS        : Texto := 'VS';
    (*36*)mdlappGraTX     : Texto := 'GraTX';
    else Texto := '? ? ? ? ';
  end;
  MB_Aviso('M�dulo "' + Texto + '" n�o habilitado!');
end;

function TUnGrl_Geral.IntToBool(Verdade: Integer): Boolean;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.IntToBool(Verdade);
  {$Else}
  Result := Geral.IntToBool(Verdade);
  {$EndIf}
end;

function TUnGrl_Geral.LiberaModulo(Aplicativo: Integer; Modulo,
  HabilModulo: String; MostraMsg: Boolean): Boolean;
begin
  if (Pos(UpperCase(Modulo), UpperCase(HabilModulo)) > 0) or (Aplicativo = 17) then //DControl
  begin
    Result := True;
  end else
  begin
    if MostraMsg then
      MB_Aviso('Esta janela pertence ao m�dulo ' + Modulo + '.' +
        sLineBreak + 'E este m�dulo n�o est� habilitado!' + sLineBreak +
        'Caso deseje habilit�-lo solicite junto a Dermatek!');
    //
    Result := False;
  end;
end;

function TUnGrl_Geral.AjustaDataHoraUTC(var dh: String;
  var UTC: Double): Boolean;
var
  P, Sinal: Integer;
  A, B: String;
begin
  A := Copy(dh, 1, 19);
  B := Copy(dh, 20);
  P := pos('T', A);
  if P > 0 then
    A[P] := ' ';
  dh := A;
  //
  UTC := 0;
  Sinal := 1;
  if Length(B) > 0 then
  begin
    case Ord(B[1]) of
      Ord('-'):
      begin
        Sinal := -1;
        B := Copy(B, 2);
      end;
      Ord('0')..Ord('9'): ; // nada
      else B := Copy(B, 2);
    end;
    UTC := StrToTime(B) * Sinal;
  end;
  //
  Result := True;
end;

function TUnGrl_Geral.ATS(ListaSQL: array of string): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.ATS(ListaSQL);
  {$Else}
  Result := Geral.ATS(ListaSQL);
  {$EndIf}
end;

function TUnGrl_Geral.BoolToInt(Verdade: Boolean): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.BoolToInt(Verdade);
  {$Else}
  Result := Geral.BoolToInt(Verdade);
  {$EndIf}
end;

function TUnGrl_Geral.CalculaCNPJCPF(Nume: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.CalculaCNPJCPF(Nume);
  {$Else}
  Result := Geral.CalculaCNPJCPF(Nume);
  {$EndIf}
end;

{$IfDef DMK_FMX}
function TUnGrl_Geral.CNPJ_Valida(const EdCNPJ: TCustomEdit;
  var CNPJ: String): Boolean;
begin
  Result := FMX_Geral.CNPJ_Valida(EdCNPJ, CNPJ);
end;
{$Else}
function TUnGrl_Geral.CNPJ_Valida(const EdCNPJ: TWinControl;
  var CNPJ: String): Boolean;
begin
  Result := Geral.CNPJ_Valida(EdCNPJ, CNPJ);
end;
{$EndIf}

function TUnGrl_Geral.DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
begin
  {$IfDef DMK_FMX}
  Result := FMX_dmkPF.DateTime_MyTimeZoneToUTC(DataHora);
  {$Else}
  Result := dmkPF.DateTime_MyTimeZoneToUTC(DataHora);
  {$EndIf}
end;

procedure TUnGrl_Geral.DefineFormatacoes;
begin
  {$IfDef DMK_FMX}
  FMX_Geral.DefineFormatacoes();
  {$Else}
  Geral.DefineFormatacoes();
  {$EndIf}
end;

function TUnGrl_Geral.VariavelToStringAS(Variavel: Variant): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.VariavelToStringAS(Variavel);
  {$Else}
  Result := Geral.VariavelToString(Variavel);
  {$EndIf}
end;

function TUnGrl_Geral.VariavelToStringAD(Variavel: Variant): String;
begin
///////////////////////////////////////////////////////////////////////////////////////
(*  E R R O !!!!! - transforma var em String!!!!
  Result := Variavel;
  {$IfDef DMK_FMX}
  if Pos('!', Variavel) > 0 then  // precisa no SQLIte para evitar erro!
    Result := StringReplace(Variavel, '!', '!!', [rfReplaceAll, rfIgnoreCase]);
  //
  Result := FMX_Geral.VariavelToStringAD(Result);
  {$Else}
  Result := Geral.VariavelToString(Result);
  {$EndIf}
*)
///////////////////////////////////////////////////////////////////////////////////////
///  CUIDADO!!! Evitar erro acima!!!!
  {$IfDef DMK_FMX}
    Result := FMX_Geral.VariavelToStringAD(Variavel);
    if Pos('!', Result) > 0 then  // precisa no SQLIte para evitar erro!
      Result := StringReplace(Variavel, '!', '!!', [rfReplaceAll, rfIgnoreCase]);
  {$Else}
    Result := Geral.VariavelToString(Variavel);
  {$EndIf}
end;

function TUnGrl_Geral.VersaoTxt2006(Versao: Integer): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.VersaoTxt2006(Versao);
  {$Else}
  Result := Geral.VersaoTxt2006(Versao);
  {$EndIf}
end;

function TUnGrl_Geral.ObtemAlterWebDeDeviceType(DeviceType: TDeviceType): TAlterWeb;
begin
  case DeviceType of
    stDesktop:
      Result := alDsk;
    stWeb:
      Result := alWeb;
    stMobile:
      Result := alMob;
  end;
end;

function TUnGrl_Geral.ObtemModuloDeGrupoJan(Janela: String): String;
const
  sProcName = 'UnGrl_Geral.ObtemModuloDeGrupoJan()';
var
  GrupoJan: String;
begin
  GrupoJan := Copy(Janela, 1, 9);
  if GrupoJan = 'WET-CURTI' then
    Result := 'VS'
  //... no final:
  else begin
    Result := '?';
    MB_Erro('"' + GrupoJan + '" n�o implemeentado em ' + sProcName);
  end;
end;

function TUnGrl_Geral.ObtemModuloDeSigla(Sigla: String): TdmkModuloApp;
const
  sProcName = 'UnGrl_Geral.ObtemModuloDeSigla()';
begin
  Result :=  (*0*)mdlappNenhum;
  if LowerCase(Sigla) = 'nenhum'     then Result :=  (*0*)mdlappNenhum      else
  if LowerCase(Sigla) = 'appmain'    then Result :=  (*01*)mdlappAppMain    else
  if LowerCase(Sigla) = 'allbase'    then Result :=  (*02*)mdlappAllBase    else
  if LowerCase(Sigla) = 'entidades'  then Result :=  (*03*)mdlappEntidades  else
  if LowerCase(Sigla) = 'empresas'   then Result :=  (*04*)mdlappEmpresas   else
  if LowerCase(Sigla) = 'agenda'     then Result :=  (*05*)mdlappAgenda     else
  if LowerCase(Sigla) = 'anotacoes'  then Result :=  (*06*)mdlappAnotacoes  else
  if LowerCase(Sigla) = 'bancos'     then Result :=  (*07*)mdlappBancos     else
  if LowerCase(Sigla) = 'bloquetos'  then Result :=  (*08*)mdlappBloquetos  else
  if LowerCase(Sigla) = 'cnab'       then Result :=  (*09*)mdlappCNAB       else
  if LowerCase(Sigla) = 'contratos'  then Result :=  (*10*)mdlappContratos  else
  if LowerCase(Sigla) = 'cro'        then Result :=  (*11*)mdlappCRO        else
  if LowerCase(Sigla) = 'diario'     then Result :=  (*12*)mdlappDiario     else
  if LowerCase(Sigla) = 'e-mail'     then Result :=  (*13*)mdlappEmail      else
  if LowerCase(Sigla) = 'estoque'    then Result :=  (*14*)mdlappEstoque    else
  if LowerCase(Sigla) = 'favoritos'  then Result :=  (*15*)mdlappFavoritos  else
  if LowerCase(Sigla) = 'financeiro' then Result :=  (*16*)mdlappFinanceiro else
  if LowerCase(Sigla) = 'fpmin'      then Result :=  (*17*)mdlappFPMin      else
  if LowerCase(Sigla) = 'fpmax'      then Result :=  (*18*)mdlappFPMax      else
  if LowerCase(Sigla) = 'ftp'        then Result :=  (*19*)mdlappFTP        else
  if LowerCase(Sigla) = 'grade'      then Result :=  (*20*)mdlappGrade      else
  if LowerCase(Sigla) = 'nfe'        then Result :=  (*21*)mdlappNFe        else
  if LowerCase(Sigla) = 'nfse'       then Result :=  (*22*)mdlappNFSe       else
  if LowerCase(Sigla) = 'perfis'     then Result :=  (*23*)mdlappPerfis     else
  if LowerCase(Sigla) = 'protocolos' then Result :=  (*24*)mdlappProtocolos else
  if LowerCase(Sigla) = 'sac'        then Result :=  (*25*)mdlappSAC        else
  if LowerCase(Sigla) = 'saf'        then Result :=  (*26*)mdlappSAF        else
  if LowerCase(Sigla) = 'sped'       then Result :=  (*27*)mdlappSPED       else
  if LowerCase(Sigla) = 'web'        then Result :=  (*28*)mdlappWEB        else
  if LowerCase(Sigla) = 'wtextos'    then Result :=  (*29*)mdlappWTextos    else
  if LowerCase(Sigla) = 'cuns'       then Result :=  (*30*)mdlappCuns       else
  if LowerCase(Sigla) = 'bina'       then Result :=  (*31*)mdlappBINA       else
  if LowerCase(Sigla) = 'cmeb'       then Result :=  (*32*)mdlappConciBco   else
  if LowerCase(Sigla) = 'gfat'       then Result :=  (*33*)mdlappGFat       else
  if LowerCase(Sigla) = 'gped'       then Result :=  (*34*)mdlappGPed       else
  if LowerCase(Sigla) = 'vs'         then Result :=  (*35*)mdlappVS         else
  MB_Aviso('Sigla de M�dulo "' + Sigla + '" n�o habilitado em ' + sProcName);
end;

function TUnGrl_Geral.ObtemSiglaModulo(Modulo: TDmkModuloApp): String;
var
  Sigla: String;
begin
  case Modulo of
    mdlappNenhum:
      Sigla := '';
    mdlappAppMain:
      Sigla := '';
    mdlappAllBase:
      Sigla := '';
    mdlappEntidades:
      Sigla := '';
    mdlappEmpresas:
      Sigla := '';
    mdlappAgenda:
      Sigla := '';
    mdlappAnotacoes:
      Sigla := '';
    mdlappBancos:
      Sigla := '';
    mdlappBloquetos:
      Sigla := 'BLOQ';
    mdlappCNAB:
      Sigla := '';
    mdlappContratos:
      Sigla := 'CNTR';
    mdlappCRO:
      Sigla := '';
    mdlappDiario:
      Sigla := '';
    mdlappEmail:
      Sigla := 'MAIL';
    mdlappEstoque:
      Sigla := '';
    mdlappFavoritos:
      Sigla := '';
    mdlappFinanceiro:
      Sigla := '';
    mdlappFPMin:
      Sigla := '';
    mdlappFPMax:
      Sigla := '';
    mdlappFTP:
      Sigla := '';
    mdlappGrade:
      Sigla := '';
    mdlappNFe:
      Sigla := 'NFe';
    mdlappNFSe:
      Sigla := 'NFSe';
    mdlappPerfis:
      Sigla := '';
    mdlappProtocolos:
      Sigla := '';
    mdlappSAC:
      Sigla := '';
    mdlappSAF:
      Sigla := '';
    mdlappSPED:
      Sigla := '';
    mdlappWEB:
      Sigla := 'WEB';
    mdlappWTextos:
      Sigla := '';
    mdlappCuns:
      Sigla := '';
    mdlappBina:
      Sigla := '';
    mdlappConciBco:
      Sigla := '';
    mdlappGFat:
      Sigla := '';
    mdlappGPed:
      Sigla := '';
    mdlappVS: //=35,
      Sigla := '';
    mdlappGraTX: //=36,
      Sigla := '';
    mdlappLocalDB: //37
      Sigla := '';
  end;
  if Sigla = '' then
    MB_Aviso('A sigla informada na fun��o:' + sLineBreak +
      '"TUnDmkProcFunc.ObtemSiglaModulo" n�o est� implementada!');
  //
  Result := Sigla;
end;

function TUnGrl_Geral.PareceTelefoneBR(Telefone: String): Boolean;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.PareceTelefoneBR(Telefone);
  {$Else}
  Result := Geral.PareceTelefoneBR(Telefone);
  {$EndIf}
end;

function TUnGrl_Geral.FDT(Data: TDateTime; Tipo: Integer;
  Nulo: Boolean = False): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.FDT(Data, Tipo, Nulo);
  {$Else}
  Result := Geral.FDT(Data, Tipo, Nulo);
  {$EndIf}
end;

function TUnGrl_Geral.MB_Pergunta(Text: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MB_Pergunta(Text);
  {$Else}
  Result := Geral.MB_Pergunta(Text);
  {$EndIf}
end;

function TUnGrl_Geral.MB_SQL(var Reference; Qry: TDataSet): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MB_SQL(TForm(Reference), Qry);
  {$Else}
  Result := Geral.MB_SQL(TForm(Reference), Qry);
  {$EndIf}
end;

function TUnGrl_Geral.MTD(Data: String; Obrigatorio: Boolean): TDateTime;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MTD(Data, Obrigatorio);
  {$Else}
  Result := Geral.MTD(Data, Obrigatorio);
  {$EndIf}
end;

procedure TUnGrl_Geral.SalvaArquivoEmDocumentos(ArqConteudo, ArqNome,
  ArqExt: String);
var
  TextFile: TStringList;
  Arquivo, DocDir: String;
begin
  TextFile := TStringList.Create;
  try
    {$IfDef DMK_FMX}
    DocDir := TPath.GetSharedDocumentsPath;
    {$Else}
    DocDir := dmkPF.ObterCaminhoMeusDocumentos;
    {$EndIf}
    Arquivo := TPath.Combine(DocDir, ArqNome + '.' + ArqExt);
    TextFile.Text := Trim(ArqConteudo);
    TextFile.SaveToFile(Arquivo);
  finally
    TextFile.Free;
  end;
end;

function TUnGrl_Geral.SoNumeroELetra_TT(Texto: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.SoNumeroELetra_TT(Texto);
  {$Else}
  Result := Geral.SoNumeroELetra_TT(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.SoNumeroESinalEVirgula_TT(Texto: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.SoNumeroESinalEVirgula_TT(Texto);
  {$Else}
  Result := Geral.SoNumeroESinalEVirgula_TT(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.SoNumeroESinal_TT(Texto: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.SoNumeroESinal_TT(Texto);
  {$Else}
  Result := Geral.SoNumeroESinal_TT(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.SoNumeroEVirgula_TT(Texto: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.SoNumeroEVirgula_TT(Texto);
  {$Else}
  Result := Geral.SoNumeroEVirgula_TT(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.SoNumero_TT(Texto: String): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.SoNumero_TT(Texto);
  {$Else}
  Result := Geral.SoNumero_TT(Texto);
  {$EndIf}
end;

function TUnGrl_Geral.TFD(Texto: String; Digitos: Integer;
  Sinal: TSinal): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.TFD(Texto, Digitos, Sinal);
  {$Else}
  Result := Geral.TFD(Texto, Digitos, Sinal);
  {$EndIf}
end;

function TUnGrl_Geral.TFT(Texto: String; Casas: Integer; Sinal: TSinal): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.TFT(Texto, Casas, Sinal);
  {$Else}
  Result := Geral.TFT(Texto, Casas, Sinal);
  {$EndIf}
end;

function TUnGrl_Geral.TFT_Max(Texto: String; Max: Double; Casas: Integer;
  Sinal: Boolean): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.TFT_Max(Texto, Max, Casas, Sinal);
  {$Else}
  //Result := Geral.TFT_Max(Texto, Max, Casas, Sinal);
  Result := Geral.TFT_Max(Texto, Max, Casas);
  {$EndIf}
end;

function TUnGrl_Geral.TFT_Min(Texto: String; Min: Double; Casas: Integer;
  Sinal: Boolean): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.TFT_Min(Texto, Min, Casas, Sinal);
  {$Else}
  //Result := Geral.TFT_Min(Texto, Min, Casas, Sinal);
  Result := Geral.TFT_Min(Texto, Min, Casas);
  {$EndIf}
end;

function TUnGrl_Geral.TFT_MinMax(Texto: String; Min, Max: Extended;
  Casas: Integer; Sinal: Boolean): String;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.TFT_MinMax(Texto, Min, Max, Casas, Sinal);
  {$Else}
  Result := Geral.TFT_MinMax(Texto, Min, Max, Casas);
  {$EndIf}
end;

function TUnGrl_Geral.MB_Erro(Text: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MB_Erro(Text);
  {$Else}
  Result := Geral.MB_Erro(Text);
  {$EndIf}
end;

function TUnGrl_Geral.MB_Info(Text: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MB_Info(Text);
  {$Else}
  Result := Geral.MB_Info(Text);
  {$EndIf}
end;

function TUnGrl_Geral.MB_Teste(Text: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MB_Info(Text);
  {$Else}
  Result := Geral.MB_Info(Text);
  {$EndIf}
end;

function TUnGrl_Geral.MB_Aviso(Text: String): Integer;
begin
  {$IfDef DMK_FMX}
  Result := FMX_Geral.MB_Aviso(Text);
  {$Else}
  Result := Geral.MB_Aviso(Text);
  {$EndIf}
end;

end.

