unit UnGrl_Vars;

interface

uses
  System.Generics.Collections, System.SysUtils, System.Classes,
  FMX.Memo,
{$IfNDef DMK_FMX}
  FMX.Forms,
  Controls,
  ExtCtrls,
  StdCtrls,
  ComCtrls,
{$Else}
{$EndIf}
{$IfDef MSWINDOWS}
  Vcl.Forms, dmkPageControl,
{$EndIf}
  System.UITypes, System.Variants, Data.DB, UnMyLinguas;
type
  // Pallets
  TPallArr = array of Integer;

var
  VAR_CtaComprUsoCons: Integer = 0;
  VAR_CartComprUsoCons: Integer = -1;
  //
  VAR_MySQL_MARIA_DB: Boolean = False;
  VAR_MSG_START: Boolean = False;
  VAR_FisRegCad_ToAlt: Integer = 0;
  VAR_EvitaErroExterno: Boolean = False;
  VAR_HIDEN_ERR_MSGS_BY_DMK: WideString = '';

  VAR_APP_HABILITADO_IMPRESSAO_MARCA_OU_REFERENCIA: Boolean = False;

  VAR_CertDigPfxLoc: Integer = 1;

{$IfDef MSWINDOWS}
  VAR_AdvToolBarPagerNovo: TdmkPageControl;
{$EndIf}
  //
  VAR_USA_FRX_IMPR_Recibo: Boolean = False; // T o o l r e n t   2
  VAR_FRX_Imprimiu: Boolean = False;
  VAR_Logo2_4_x_1_0Path: String = '';
  VAR_Logo3_4_x_3_4Path: String = '';
  //
  VAR_SEQ_TMP_REL: Integer = 0;
  //
  VAR_NOME_NOVO_GG1: String = '';
  VAR_USA_PED_SIT: Boolean = False;
  VAR_INFO_DATA_HOJE: Boolean = False;
  VAR_APP_EMPARELHA_CODIGO_DO_GG1_COM_O_CONTROLE_DO_GGX: Boolean = False;
  VAR_SCALE_X: Double = 1.000; // Escala responsiva
  VAR_SCALE_Y: Double = 1.000; // Escala responsiva
  VAR_NAO_USA_KEY_LOCAL_MACHINE: Boolean = False;
  // dados para uso no mobile - Id no server
  VAR_COD_DEVICE_IN_SERVER: Integer = 0;
  VAR_PIN_DEVICE_IN_SERVER: String = '';
  VAR_NIK_DEVICE_IN_SERVER: String = 'Usu�rio';
  VAR_CNPJ_DEVICE_IN_SERVER: String = '';
  VAR_IMEI_DEVICE_IN_SERVER: String = '';
  // dados para uso no mobile - opcoes do mobile
  VAR_OPCOES_MOB_FoLocObjIns: Integer = 0;
  VAR_OPCOES_MOB_EXcIdFunc: Integer;
  VAR_OPCOES_MOB_IDCntrCst: Integer;
  //VAR_OPCOES_MOB_ScaleX: Double = 1.0;
  //VAR_OPCOES_MOB_ScaleY: Double = 1.0;
  VAR_ERPNameByCli: String = '';
  VAR_EstiloForms: String = 'AquaGraphite'; //'[Nenhum]';

  VAR_DMK_THEMES_COLOR_Form   ,
  VAR_DMK_THEMES_COLOR_Panel  ,
  VAR_DMK_THEMES_COLOR_Button ,
  VAR_DMK_THEMES_COLOR_Edit   ,
  VAR_DMK_THEMES_COLOR_Text   ,
  VAR_DMK_THEMES_COLOR_HiLigh ,
  VAR_DMK_THEMES_COLOR_DkLigh ,
  VAR_DMK_THEMES_COLOR_DisBak ,
  VAR_DMK_THEMES_COLOR_DisTxt: TColor;
  //
  VAR_DMK_COLOR_TEXT: TAlphaColor = TAlphaColors.RoyalBlue;

  //
  VAR_AMBIENTE_APP: Integer = 1; // 0 = Produ��o, 1 = Treinamento, 2 = Desenvolvimento
  VAR_API_ID_ENTIDADE: Integer = 0;
  VAR_API_ID_USUARIO: Integer = 0;
  VAR_MODAL_RESULT: TModalResult = mrNone; // FmDmkDialog
  VAR_MODAL_MESSAGE: String;
//// Provis�rio: Excluir ap�s uso total em ambiente de produ��o de NFe! ////////
  VAR_NT2018_05v120: Boolean = True;  // 2020-10-11 MLA mudou de False para true
{$IfNDef DMK_FMX}
  VAR_CertDigPfxCam: AnsiString = '';
  VAR_CertDigPfxPwd: AnsiString = '';
  VAR_CertDigPfxWay: Integer = 0;
  VAR_AssDigMode:    Integer = 1; // 1 n�o existe mais! For�ar erro
  VAR_DFeAppCode:    Integer = 1; // 1 n�o existe mais! For�ar erro
  VAR_CertNFeSerNum: String = '';
{$Else}
{$EndIf}
//// FIM Provis�rio ////////////////////////////////////////////////////////////
  //Temporario
  VAR_ABETURAS_QUERY_NORMAL: Integer = 0;
  VAR_ABETURAS_QUERY_DIRECT: Integer = 0;
  //Fim Temporario
  CO_VERSAO: Int64; //ivMsgVersaoDifere
  // CO_VERMLA, CO_VERMCW, CO_VERM28 > App_Consts
  VAR_DBWEB: String;
  // WEB - Licen�a
  VAR_WEB_USR_ID: Integer = 0; // C�digo do usu�rio
  VAR_WEB_USR_TOKEN: String = '';
  VAR_WEB_USR_NOME: String = '';
  VAR_WEB_USR_USUARIO: String = '';
  VAR_WEB_USR_TIPO: Integer = 0;
  VAR_WEB_USR_ENT: Integer = 0; // C�digo da entidade
  VAR_WEB_USR_ENT_NOME: String = ''; //Nome da entidade
  VAR_WEB_USR_DEVICE: Integer = 0; // C�digo do dispositivo dentro da entidade (Usado para sincronismo)
  VAR_WEB_USR_WEBID: String = '';
  VAR_WEB_USR_TZDIFUTC: String = '';
  //
  VAR_WEB_CONECTADO: Integer = 0;
  VAR_WEB_CLIUSER: Integer = 0;
  VAR_WEB_PERUSER: Integer = 0;
  VAR_WEB_NOTIFYAPP: Integer = 0;
  VAR_WEB_TERMOUSO: Integer = 0;
  VAR_WEB_TERMOPRIVA: Integer = 0;
  VAR_WEB_IDLOGIN: String = '';
  VAR_WEB_IDAPI: String = '';
  VAR_WEB_HASHTERMO: String = '';
  //
  // Collect Data Server Repository
  VAR_IS_CollectDataServerRepository: Boolean = False;
  //
  VAR_CAD_STQCENLOC: Integer = 0;
  (*
  VAR_WEB_USER_USRNUM: Integer = 0;
  VAR_WEB_USER_USRID: Integer = 0;
  *)
  // WEB
  VAR_WEB_ID: String = '';
  VAR_TXT_AWServerID: String = '0';
  VAR_InsUpd_AWServerID: Boolean = False;
  // Database
  FTabelas           : TList<TTabelas>;
  FLCampos           : TList<TCampos>;
  FLIndices          : TList<TIndices>;
  FLQeiLnk           : TList<TQeiLnk>;
  FViuControle       : Integer;
  FPergunta          : Boolean;
  FCriarAtivoAlterWeb: Boolean;
  VAR_LOCAL_DB_COMPO_DATASET: TDataSet = nil;
  VAR_LOCAL_DB_COMPO_DATABASE: TComponent = nil;
  //
  VAR_SALVA_REGISTROS_EXCLUIDOS: Boolean = False;
  //
  VAR_TMeuDB_ZDir: String;
  //
  // Entidades
  VAR_FLD_ENT_PRESTADOR: String;
  //
{$IfNDef DMK_FMX}
  VAR_MeDmkAviso: TMemo; // Avisos de DmkForm (form de login);
{$Else}
{$EndIf}
  VAR_VERIFICA_BD_FIELD_DEFAULT_VALUE: Boolean = False; // XE2 to Berlin >> preven��o de erro na migra��o
  VAR_CONTA_ERRO_NULL_DEFAULT: Integer = 0;
  VAR_CONTA_FIELD_IN_DB: Integer = 0;
  VAR_CONTA_TABLE_START: String = '';
  //
  //PushNotificatios
  VAR_PushNotificatios_Memo: TMemo = nil;

  // ini consulta cadastro CCC
  FCCC_RetrornoUtil: Boolean;
  FCCC_cStat: Integer;
  FCCC_xMotivo: String;
  FCCC_IE, FCCC_CNPJ, FCCC_CPF, FCCC_UF, FCCC_IEUnica, FCCC_IEAtual: String;
  FCCC_cSit, FCCC_indCredNFe, FCCC_indCredCTe: Integer;
  FCCC_xNome, FCCC_xRegApur: String;
  FCCC_CNAE: Integer;
  FCCC_dIniAtiv, FCCC_dUltSit: String; //TDateTime;
  FCCC_xLgr, FCCC_xCpl, FCCC_xBairro, FCCC_xMun: String;
  FCCC_nro, FCCC_cMun, FCCC_CEP: Integer;
  // fim consulta cadastro CCC

implementation

end.


