unit UnGrl_Consts;


interface

uses System.UITypes;

const

  CONST_DMK_SOFTWARE_CUSTOMOZADO = 'www.dermatek.com.br - Software Customizado';
  CO_RegimTributar_0_Indefinido       = 0;
  CO_RegimTributar_1_LucroReal        = 1;
  CO_RegimTributar_2_LucroPresumido   = 2;
  CO_RegimTributar_3_LucroArbitrado   = 3;
  CO_RegimTributar_4_SimplesNacional  = 4;

  CO_RegimPisCofins_0_Indefinido      = 0;
  CO_RegimPisCofins_1_NaoCumulativo   = 1;
  CO_RegimPisCofins_2_Cumulativo      = 2;

  CO_RegimCumulativ_0_Indefinido      = 0;
  CO_RegimCumulativ_1_Compet�ncia     = 1;
  CO_RegimCumulativ_2_Caixa           = 2;

  CO_SourceConnect = '_SourceConnect';
  // NFe / NFCe
  CO_MODELO_NFE_55 = 55;
  CO_MODELO_NFE_65 = 65;
  CO_005_StepNFePedido        = 5;
  CO_010_StepNFeDados         = 10;
  CO_020_StepNFeGerada        = 20;
  CO_030_StepNFeAssinada      = 30;
  CO_035_StepNFCeOffLine      = 35;
  CO_040_StepLoteRejeitado    = 40;
  CO_050_StepNFeAdedLote      = 50;
  CO_060_StepLoteEnvEnviado   = 60;
  CO_070_StepLoteEnvConsulta  = 70;
  CO_090_StepNFeCabecalhoDwnl = 90;
  CO_100_NFeAutorizada        = 100;
  CO_101_NFeCancelada         = 101;

  // API Rest Dermatek
  CO_DMK_CNPJ = '03143014000152';
  CO_URL_DEV = 'http://dev.dermatek.com.br/api/';
  CO_URL_PRD = 'http://dermatek.com.br/api/';
  CO_URL_SECUR_DEV = 'https://dev.dermatek.com.br/api/';
  CO_URL_SECUR_PRD = 'https://dermatek.com.br/api/';
  CO_TOKEN = 'ws00ok0g0cwggcw8oscko4ks4k4oko8csk48kc40';
  //
  CO_RUN_TIME_CREATED_PREFIX = '_RUN_TIME_CREATED_';
  CO_RUN_TIME_CREATED_LENGHT = 18; // s� mexer aqui quando mudar o tamanho do CO_RUN_TIME_CREATED_PREFIX!
  // Vibrar
  CO_TEMPO_VIBRA_COMO_BOTAO = 40; // milisegundos
  // Retormos web
  HTTP_200_OK: Integer = 200;
  HTTP_422_ERRO_DE_SEMANTICA_DO_LADO_CLIENTE: Integer = 422;
  // Usu�rios pr� definidos
  CO_USER_SYSTEM = -4; //-4|"SYSTEM [Pr�prio ERP]"');
  CO_USER_ADMIN  = -3; //-3|"ADMIN [Administrador]"');
  CO_USER_BOSS   = -2; //-2|"BOSS [Administrador]"');
  CO_USER_MASTE  = -1; //-1|"MASTER [Admin. DERMATEK]"');
  CO_USER_NONE   =  0; //0|""');
  CO_JOKE_FOTO_ARQNOME = ':PNomeArq';
  CO_JOKE_FOTO_DATAHORA = ':PDataHora';
  //URLs apps
  CO_WEB_URL_APP_REMOTO_SUPORTE = 'http://www.dermatek.com.br/Suporte/TeamViewerQS_pt-idcve6we7w.exe';
  CO_WEB_URL_APP_REMOTO_TREINAMENTOS = 'http://www.dermatek.com.br/Suporte/TeamViewerQS_pt-idcve6we7w.exe';

  CO_WEB_URL_REST_SERVER = 'http://www.dermatek.net.br/dmkREST.php';
  CO_WEB_REQ_CONTENTTYPE_ENCODED = 'application/x-www-form-urlencoded';
  DELETE_FROM = 'DEL'+'ETE FROM ';
  CO_SELECT_ = 'SELECT ';
  // Sincronismo
  CO_MAX_DEVICE_NUM = 3; //Por enquanto o m�ximo de dispositivos ser� 3 casas

  // Lista dos Aplicativos
  CO_APP_ID_BUGSTROL   = 24;
  CO_APP_ID_BUGSMOON   = 33;
  // Fim Lista Aplicativos

  CO_DIR_RAIZ_DMK = 'C:\Dermatek';
  CO_SQLBATCH_DELIM = '|;|';

  // SQL Forms
  CO_STYLE_LOOKUP_DSA_PLUS = 'addbutton';
  CO_STYLE_LOOKUP_DSA_EDIT = 'editbutton';
  CO_STYLE_LOOKUP_DSA_OLHO = 'pausebutton';
  //
  CO_STYLE_LOOKUP_DSA_NDEF = 'roundbutton';
  // Tabbed forms
  CO_FV_MAIN = 'FVMain';

  // Tabelas
  CO_SINCRO_TAB = 'web_sincro';

  //  Geral Apps
  CO_RandStrWeb01 = 'WXeq6ojTRJC70zBP';
{$IFDEF ANDROID}
  CO_RandStrBDLite = 'WXeq6ojTRJC70zBP';
{$Else}
//{$IfDef MSWINDOWS}
  CO_RandStrBDLite = '';
//{$EndIf}
{$EndIf}
  CO_SecureStr_001 = '9qwbdeyte4';
  CO_MASTER = '31415926536';
  CO_USERSP001 = 'wkljweryhvbirt'; // anterior
  CO_USERSPNOW = 'wkljweryhvbirt'; // atual
  CO_USERSPSEQ = 1;
  CO_SPDESCONHECIDA = '852456';
  //
  VAR_DERMA_AD: byte = 2;


implementation

end.

