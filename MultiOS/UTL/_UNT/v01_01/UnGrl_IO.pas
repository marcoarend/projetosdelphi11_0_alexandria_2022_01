unit UnGrl_IO;

interface

uses System.Classes, System.SysUtils, System.Types, IdCoderMIME,
{$IFDEF DMK_FMX}
  FMX.Objects, FMX.Graphics,
{$ELSE}
  Vcl.ExtCtrls, Vcl.Imaging.pngimage, Vcl.Imaging.jpeg,
{$ENDIF}
  System.IOUtils;

type
  TUnGrl_IO = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  FileToBase64(FileName: String): String;
    function  GetApplicationPath(AplicationName: String; Directory: String = ''): String;
    function  GetFilesFromPath(Path: String): TStringList;
    procedure SaveImageInPngFile(Image: TImage; Path, Name: String;
      Quality: Integer = 100);
    procedure SaveImageInJpegFile(Image: TImage; Path, Name: String;
      Quality: Integer = 100);
    procedure DirCreate(Path: String);
    procedure DirDelete(Path: String; Recursive: Boolean = True);
    function  DirExist(Path: String): Boolean;
    function  DirIsEmpty(Path: String): Boolean;
  end;

var
  Grl_IO: TUnGrl_IO;

implementation

{ TUnGrl_IO }

procedure TUnGrl_IO.DirCreate(Path: String);
begin
  TDirectory.CreateDirectory(Path);
end;

procedure TUnGrl_IO.DirDelete(Path: String; Recursive: Boolean = True);
begin
 //Se este par�metro for falso,os diret�rios n�o vazios n�o ser�o exclu�dos
 TDirectory.Delete(Path, Recursive);
end;

function TUnGrl_IO.DirExist(Path: String): Boolean;
begin
  if TDirectory.Exists(Path) then
    Result := True
  else
    Result := False;
end;

function TUnGrl_IO.DirIsEmpty(Path: String): Boolean;
begin
  if TDirectory.IsEmpty(Path) then
    Result := True
  else
    Result := False;
end;

function TUnGrl_IO.FileToBase64(FileName: String): String;
var
  Stream: TFileStream;
  Base64: TIdEncoderMIME;
begin
  Result := '';
  Base64 := TIdEncoderMIME.Create(nil);
  try
    Stream := TFileStream.Create(FileName, fmOpenRead);
    try
      Result := TIdEncoderMIME.EncodeStream(Stream);
    finally
      Stream.Free;
    end;
  finally
    Base64.Free;
  end;
end;

function TUnGrl_IO.GetApplicationPath(AplicationName: String;
  Directory: String = ''): String;
var
  Path: String;
begin
  Path := System.IOUtils.TPath.GetHomePath + System.SysUtils.PathDelim;

  if AplicationName <> '' then
    Path := Path + AplicationName + System.SysUtils.PathDelim;

  if Directory <> '' then
    Path := Path + Directory + System.SysUtils.PathDelim;

  if not DirExist(Path) then
    DirCreate(Path);

  Result := Path;
end;

function TUnGrl_IO.GetFilesFromPath(Path: String): TStringList;
var
  FileList: TStringDynArray;
  S: String;
begin
  Result := TStringList.Create;
  //
  FileList := TDirectory.GetFiles(Path);
  //
  for s in FileList do
    Result.Add(s);
end;

procedure TUnGrl_IO.SaveImageInJpegFile(Image: TImage; Path, Name: String;
  Quality: Integer = 100);
var
  {$IFDEF DMK_FMX}
  SaveParams: TBitmapCodecSaveParams;
  {$ELSE}
  JPG: TJPEGImage;
  {$ENDIF}
begin
  {$IFDEF DMK_FMX}
  SaveParams.Quality := Quality;
  //
  Image.Bitmap.SaveToFile(Path + Name + '.jpg', @SaveParams);
  {$ELSE}
  JPG := TJPEGImage.Create;
  try
    JPG.Assign(Image.Picture.Bitmap);
    JPG.SaveToFile(Path + Name + '.jpg');
  finally
    JPG.Free;
  end;
  {$ENDIF}
end;

procedure TUnGrl_IO.SaveImageInPngFile(Image: TImage; Path, Name: String;
  Quality: Integer = 100);
var
  {$IFDEF DMK_FMX}
  SaveParams: TBitmapCodecSaveParams;
  {$ELSE}
  Png: TPNGObject;
  {$ENDIF}
begin
  {$IFDEF DMK_FMX}
  SaveParams.Quality := Quality;
  //
  Image.Bitmap.SaveToFile(Path + Name + '.png', @SaveParams);
  {$ELSE}
  PNG := TPNGObject.Create;
  try
    PNG.Assign(Image.Picture.Bitmap);
    PNG.SaveToFile(Path + Name + '.png');
  finally
    PNG.Free;
  end;
  {$ENDIF}
end;

end.
