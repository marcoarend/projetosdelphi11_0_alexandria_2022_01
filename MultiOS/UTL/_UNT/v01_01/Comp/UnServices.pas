unit UnServices;

interface

uses System.JSON, UnGrl_DmkREST;

type
  TCep = class
  private
    FTpLogradouro: String;
    FLogradouro: String;
    FBairro: String;
    FCidade: String;
    FUf: String;
  public
    constructor Create(RestJson: String);
    function TpLogradouro(): String;
    function Logradouro(): String;
    function Bairro(): String;
    function Cidade(): String;
    function Uf(): String;
  end;

  TCnpj = class
  private
    FCnpj: String;
    FRazaoSocial: String;
    FFantasia: String;
    FAbertura: String;
    FCep: String;
    FLogradouro: String;
    FNumero: String;
    FComplemento: String;
    FBairro: String;
    FCidade: String;
    FUf: String;
    FCnaeId: String;
    FCnaeNome: String;
  public
    constructor Create(RestJson: String);
    function Cnpj(): String;
    function RazaoSocial(): String;
    function Fantasia(): String;
    function Abertura(): String;
    function Cep(): String;
    function Logradouro(): String;
    function Numero(): String;
    function Complemento(): String;
    function Bairro(): String;
    function Cidade(): String;
    function Uf(): String;
    function CnaeId(): String;
    function CnaeNome(): String;
  end;

implementation

{ TCep }

constructor TCep.Create(RestJson: String);
var
  Json: TJSONObject;
begin
  if RestJson <> '[]' then
  begin
    Json := Grl_DmkREST.JsonSingleToJsonObject(RestJson);
    //
    FTpLogradouro := Grl_DmkREST.GetJsonValue(Json, 'tp_logradouro');
    FLogradouro   := Grl_DmkREST.GetJsonValue(Json, 'logradouro');
    FBairro       := Grl_DmkREST.GetJsonValue(Json, 'bairro');
    FCidade       := Grl_DmkRest.GetJsonValue(Json, 'cidade');
    FUf           := Grl_DmkRest.GetJsonValue(Json, 'uf');
  end;
end;

function TCep.Bairro: String;
begin
  Result := FBairro;
end;

function TCep.Cidade: String;
begin
  Result := FCidade;
end;

function TCep.Logradouro: String;
begin
  Result := FLogradouro;
end;

function TCep.TpLogradouro: String;
begin
  Result := FTpLogradouro;
end;

function TCep.Uf: String;
begin
  Result := FUf;
end;

{ TCnpj }

constructor TCnpj.Create(RestJson: String);
var
  Json: TJSONObject;
begin
  if RestJson <> '[]' then
  begin
    Json := Grl_DmkREST.JsonSingleToJsonObject(RestJson);
    //
    FCnpj        := Grl_DmkREST.GetJsonValue(Json, 'cnpj');
    FRazaoSocial := Grl_DmkREST.GetJsonValue(Json, 'razao_social');
    FFantasia    := Grl_DmkREST.GetJsonValue(Json, 'fantasia');
    FAbertura    := Grl_DmkREST.GetJsonValue(Json, 'abertura');
    FCep         := Grl_DmkREST.GetJsonValue(Json, 'cep');
    FLogradouro  := Grl_DmkREST.GetJsonValue(Json, 'logradouro');
    FNumero      := Grl_DmkREST.GetJsonValue(Json, 'numero');
    FComplemento := Grl_DmkREST.GetJsonValue(Json, 'complemento');
    FBairro      := Grl_DmkREST.GetJsonValue(Json, 'bairro');
    FCidade      := Grl_DmkREST.GetJsonValue(Json, 'cidade');
    FUf          := Grl_DmkREST.GetJsonValue(Json, 'uf');
    FCnaeId      := Grl_DmkREST.GetJsonValue(Json, 'cnae_id');
    FCnaeNome    := Grl_DmkREST.GetJsonValue(Json, 'cnae_nome');
  end;
end;

function TCnpj.Abertura: String;
begin
  Result := FAbertura;
end;

function TCnpj.Uf: String;
begin
  Result := FUf;
end;

function TCnpj.Cidade: String;
begin
  Result := FCidade;
end;

function TCnpj.Bairro: String;
begin
  Result := FBairro;
end;

function TCnpj.Cep: String;
begin
  Result := FCep;
end;

function TCnpj.CnaeId: String;
begin
  Result := FCnaeId;
end;

function TCnpj.CnaeNome: String;
begin
  Result := FCnaeNome;
end;

function TCnpj.Cnpj: String;
begin
  Result := FCnpj;
end;

function TCnpj.Complemento: String;
begin
  Result := FComplemento;
end;

function TCnpj.Fantasia: String;
begin
  Result := FFantasia;
end;

function TCnpj.Logradouro: String;
begin
  Result := FLogradouro;
end;

function TCnpj.Numero: String;
begin
  Result := FNumero;
end;

function TCnpj.RazaoSocial: String;
begin
  Result := FRazaoSocial;
end;

end.
