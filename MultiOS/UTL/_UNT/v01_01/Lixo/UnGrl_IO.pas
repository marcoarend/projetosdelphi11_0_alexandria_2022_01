unit UnGrl_IO;

interface

uses System.Classes, System.SysUtils, IdCoderMIME,
{$IFDEF DMK_FMX}
  FMX.Objects, FMX.Graphics,
{$ELSE}
  Vcl.ExtCtrls, Vcl.Imaging.pngimage, Vcl.Imaging.jpeg,
{$ENDIF}
  System.IOUtils;

type
  TUnGrl_IO = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function FileToBase64(FileName: String): String;
    function GetApplicationPath(AplicationName: String): String;
    procedure SaveImageInPngFile(Image: TImage; Path, Name: String;
      Quality: Integer = 100);
    procedure SaveImageInJpegFile(Image: TImage; Path, Name: String;
      Quality: Integer = 100);
  end;

var
  Grl_IO: TUnGrl_IO;

implementation

{ TUnGrl_IO }

function TUnGrl_IO.FileToBase64(FileName: String): String;
var
  Stream: TFileStream;
  Base64: TIdEncoderMIME;
begin
  Result := '';
  Base64 := TIdEncoderMIME.Create(nil);
  try
    Stream := TFileStream.Create(FileName, fmOpenRead);
    try
      Result := TIdEncoderMIME.EncodeStream(Stream);
    finally
      Stream.Free;
    end;
  finally
    Base64.Free;
  end;
end;

function TUnGrl_IO.GetApplicationPath(AplicationName: String): String;
begin
  Result := System.IOUtils.TPath.GetDocumentsPath + System.SysUtils.PathDelim +
    AplicationName + System.SysUtils.PathDelim;
end;

procedure TUnGrl_IO.SaveImageInJpegFile(Image: TImage; Path, Name: String;
  Quality: Integer = 100);
var
  {$IFDEF DMK_FMX}
  SaveParams: TBitmapCodecSaveParams;
  {$ELSE}
  JPG: TJPEGImage;
  {$ENDIF}
begin
  {$IFDEF DMK_FMX}
  SaveParams.Quality := Quality;
  //
  Image.Bitmap.SaveToFile(Path + Name + '.jpg', @SaveParams);
  {$ELSE}
  JPG := TJPEGImage.Create;
  try
    JPG.Assign(Image.Picture.Bitmap);
    JPG.SaveToFile(Path + Name + '.jpg');
  finally
    JPG.Free;
  end;
  {$ENDIF}
end;

procedure TUnGrl_IO.SaveImageInPngFile(Image: TImage; Path, Name: String;
  Quality: Integer = 100);
var
  {$IFDEF DMK_FMX}
  SaveParams: TBitmapCodecSaveParams;
  {$ELSE}
  Png: TPNGObject;
  {$ENDIF}
begin
  {$IFDEF DMK_FMX}
  SaveParams.Quality := Quality;
  //
  Image.Bitmap.SaveToFile(Path + Name + '.png', @SaveParams);
  {$ELSE}
  PNG := TPNGObject.Create;
  try
    PNG.Assign(Image.Picture.Bitmap);
    PNG.SaveToFile(Path + Name + '.png');
  finally
    PNG.Free;
  end;
  {$ENDIF}
end;

end.
