unit UnGrl_OS;

interface

uses System.SysUtils, System.Classes,
{$IfDef DMK_FMX}
UnGrl_Geral,
{$Else}
dmkGeral,
{$EndIf}
iOSapi.UIKit;

type
  TUnGrl_OS = class(TObject)
  private

  public
    function  ObtemIMEI_MD5: String;
    procedure ObtemDadosDispositivo(var OSName, OSNickName, OSVersion,
              TipoDispositivo: String);
  end;

var
  Grl_OS: TUnGrl_OS;
const
  CO_Msg_NImplementado = 'N�o implementado';

implementation

{ TUnGrl_OS }

function TUnGrl_OS.ObtemIMEI_MD5: String;
begin
  Result := CO_Msg_NImplementado;
end;

procedure TUnGrl_OS.ObtemDadosDispositivo(var OSName, OSNickName,
  OSVersion, TipoDispositivo: String);
var
  Device : UIDevice;
begin
  Device := TUIDevice.Wrap(TUIDevice.OCClass.currentDevice);

  OSName          := TOSVersion.Name;
  OSNickName      := Device.systemName.UTF8String;
  OSVersion       := Device.systemVersion.UTF8String;
  TipoDispositivo := Device.model.UTF8String;
end;

end.
