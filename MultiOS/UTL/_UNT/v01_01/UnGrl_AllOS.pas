unit UnGrl_AllOS;

interface

uses System.SysUtils, System.Classes, UnGrl_OS, UnDmkEnums,

{$IfDef DMK_FMX}
  {$IF DEFINED(ANDROID)}
    Androidapi.Helpers,
  {$EndIf}
  FMX.Forms, UnGrl_Geral;
{$Else}
  Vcl.Forms, dmkGeral;
{$EndIf}


type
  TUnGrl_AllOS = class(TObject)
  private

  public
    procedure ConfiguraDadosDispositivo(var DeviceID, DeviceName, DvcScreenH,
              DvcScreenW, OSName, OSNickName, OSVersion: String);
    procedure EncerraApplicacao();
  end;

var
  Grl_AllOS: TUnGrl_AllOS;

implementation

{ TUnGrl_AllOS }

procedure TUnGrl_AllOS.ConfiguraDadosDispositivo(var DeviceID, DeviceName,
  DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion: String);
begin
  {$IfDef DMK_FMX}
  //DvcScreenH := Grl_Geral.FF0(Screen.Size.Height);
  DvcScreenH := Grl_Geral.FFT(Screen.Size.Height, 0, siNegativo);
  //DvcScreenW := Grl_Geral.FF0(Screen.Size.Width);
  DvcScreenW := Grl_Geral.FFT(Screen.Size.Width, 0, siNegativo);
  {$Else}
  DvcScreenH := Geral.FF0(Screen.Height);
  DvcScreenW := Geral.FF0(Screen.Width);
  {$EndIf}
  DeviceID   := Grl_OS.ObtemIMEI_MD5;
  //
  Grl_OS.ObtemDadosDispositivo(OSName, OSNickName, OSVersion, DeviceName);
end;

// FechaAplicativo // FechaApp
procedure TUnGrl_AllOS.EncerraApplicacao();
begin
  {$IF DEFINED(ANDROID)}
  SharedActivity.finish;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Application.Terminate;
  {$ENDIF}
  //Halt(0);
end;

end.
