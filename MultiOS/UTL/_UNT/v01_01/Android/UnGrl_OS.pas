unit UnGrl_OS;

interface

uses System.SysUtils, System.Classes,
{$IfDef DMK_FMX}
UnGrl_Geral,
{$Else}
dmkGeral,
{$EndIf}
androidapi.Helpers, androidapi.JNI.JavaTypes, androidapi.JNI.Telephony,
Androidapi.JNI.Provider, Androidapi.JNIBridge, androidapi.JNI.Os,
Androidapi.JNI.GraphicsContentViewText;

type
  TAndroidVersion = (
    UNKNOWN,
    BASE,
    BASE_1_1,
    CUPCAKE,
    CUR_DEVELOPMENT,
    DONUT,
    ECLAIR,
    ECLAIR_0_1,
    ECLAIR_MR1,
    FROYO,
    GINGERBREAD,
    GINGERBREAD_MR1,
    HONEYCOMB,
    HONEYCOMB_MR1,
    HONEYCOMB_MR2,
    ICE_CREAM_SANDWICH,
    ICE_CREAM_SANDWICH_MR1,
    JELLY_BEAN,
    JELLY_BEAN_MR1,
    JELLY_BEAN_MR2,
    KITKAT,
    KITKAT_WATCH,
    LOLLIPOP,
    LOLLIPOP_MR1
  );
  TUnGrl_OS = class(TObject)
  private

  public
    function  ObtemIMEI_MD5: String;
    procedure ObtemDadosDispositivo(var OSName, OSNickName, OSVersion,
              TipoDispositivo: String);
  end;

var
  Grl_OS: TUnGrl_OS;
const
  CO_Msg_NImplementado = 'N�o implementado';

implementation

{ TUnGrl_OS }

function TUnGrl_OS.ObtemIMEI_MD5(): String;
var
  Obj: JObject;
  TM: JTelephonyManager;
  Identifier: String;
begin
  Obj := SharedActivityContext.getSystemService(TJContext.JavaClass.TELEPHONY_SERVICE);
  if Obj <> nil then
  begin
    TM := TJTelephonyManager.Wrap( (Obj as ILocalObject).GetObjectID );
    if TM <> nil then
      Identifier := JStringToString(TM.getDeviceId);
  end;
  if Identifier = '' then
    Identifier := JStringToString(TJSettings_Secure.JavaClass.getString(
      SharedActivity.getContentResolver, TJSettings_Secure.JavaClass.ANDROID_ID));
  //
  Result := Identifier;
end;

procedure TUnGrl_OS.ObtemDadosDispositivo(var OSName, OSNickName,
  OSVersion, TipoDispositivo: String);
var
  codename: string;
  version: TAndroidVersion;
begin
  codename        := 'Unknown';
  version         := TAndroidVersion(TJBuild_VERSION.JavaClass.SDK_INT);
  TipoDispositivo := JStringToString(TJBuild.JavaClass.MODEL);

  case version of
    UNKNOWN:
      codename := 'Unknown';
    BASE, BASE_1_1:
      codename := 'Base';
    CUPCAKE:
      codename := 'Cupcake';
    CUR_DEVELOPMENT:
      codename := 'Curent Development';
    DONUT:
      codename := 'Donut';
    ECLAIR, ECLAIR_0_1, ECLAIR_MR1:
      codename := 'Eclair';
    FROYO:
      codename := 'Froyo';
    GINGERBREAD, GINGERBREAD_MR1:
      codename := 'Gingerbread';
    HONEYCOMB, HONEYCOMB_MR1, HONEYCOMB_MR2:
      codename := 'Honeycomb';
    ICE_CREAM_SANDWICH, ICE_CREAM_SANDWICH_MR1:
      codename := 'Ice Cream Sandwich';
    JELLY_BEAN, JELLY_BEAN_MR1, JELLY_BEAN_MR2:
      codename := 'Jelly Bean';
    KITKAT, KITKAT_WATCH:
      codename := 'KitKat';
    LOLLIPOP, LOLLIPOP_MR1:
      codename := 'Lollipop';
  end;
  OSName     := TOSVersion.Name;
  OSNickName := codename;
  OSVersion  := JStringToString(TJBuild_VERSION.JavaClass.RELEASE);
end;

end.
