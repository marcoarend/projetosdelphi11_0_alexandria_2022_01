unit UnGrl_DmkDB;

interface

uses System.Generics.Collections, System.SysUtils, System.Classes,
  System.UITypes, System.Variants, Data.DB,
  TypInfo,
  {$IfDef DMK_FMX}
    {$IF DEFINED(ANDROID)}
    Androidapi.Helpers,
    {$EndIf}
  FMX.Memo, FMX.ListBox, FMX.StdCtrls, FMX.Forms,
  FireDAC.Comp.Client,
  UnFMX_Geral, UnFMX_DmkProcFunc, UnFMX_Grl_Vars,
  {$Else}
  Vcl.Controls, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Forms, Vcl.Graphics,
  Winapi.Windows,
  Vcl.Buttons, Vcl.Mask, Vcl.DBCtrls, dmkRadioGroup, dmkDBLookupCombobox,
  dmkEdit, dmkEditCB, dmkEditDateTimePicker, dmkMemo, dmkCheckGroup, dmkLabel,
  dmkCheckBox, (*dmkPopOutFntCBox,*) dmkRichEdit, dmkDBEdit, dmkImage,

  ExtCtrls, Messages, Dialogs, Menus, UnMsgInt, Math, (*DBTables,*) (*DBIProcs,*)
  Registry, ZCF2, dbGrids, DmkDAC_PF,

  dmkGeral, UnDmkProcFunc, UnInternalConsts, UnMyObjects, UMySQLDB,
  {$IfDef DB_SQLite}
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, FireDAC.Stan.Option,
  {$Else}
  mySQLDbTables,
  {$EndIf}
  {$EndIf}
  UnMyLinguas, UnDmkEnums, UnGrl_Vars, UnGrl_Consts(*, dmkValUsu FMX#err*)
  , UnGrlTemp;

type
  TDBType = (dbtIndef=0, dbtMySQL=1, dbtSQLite=2);
  TUMyArray_Str = array of String;
  TUMyArray_Var = array of Variant;
  TdmkSQLInsert = (dmksqlinsIndef=0, dmksqlinsInsOnly=1, dmksqlinsInsIgnore=2,
                   dmksqlinsInsReplace=3);
  TTipoColuna = (stPrimarias=0, stNaoPrimarias=1, stTodas=2);
  TDriverName = (istMySQL, istSQLite);
  TUnGrl_DmkDB = class(TObject)
  private
    procedure MostraMsgDB_SQLite();
    function  ValidaTipoComp(Componente: array of TComponent; Funcao: String): Boolean;
    procedure Define_Form_ID(Form_ID: String);
  public
    procedure AtualizaVersaoTBControle(DataBase: TComponent;
              Device: TDeviceType; Versao: Integer);
    function  AbreSQLQuery0(Query: TDataSet; DataBase: TComponent;
              SQL: array of String): Boolean;
    function  AsFloat(Query: TDataSet; Column: String): Double;
    function  AbreQuery(Query: TDataSet; DataBase: TComponent): Boolean;
    function  CordaDeQuery(Query: TDataSet; Campo: String): String;
    function  CordasDeQuery(const Query: TDataSet; const Campo: String;
              const MaxItens: Integer; var StrCordas: String): TUMyArray_Str;
    function  EfetuaVerificacoes(DBName: String; Memo: TMemo; Estrutura,
              EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1,
              LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G,
              LaAviso2G, LaTempoI, LaTempoF, LaTempoT: TComponent;
              MeTabsNoNeed: TMemo; PB: TProgressBar; MeFldsNoNeed: TMemo;
              Device: TDeviceType; DriverNome: TDriverName;
              DataBase: TComponent; VersaoAtual: Integer; LBTabToRecriar:
              TListBox): Boolean;
    function  CarregaSQLInsUpd(QrUpd: TDataSet; DataBase: TComponent;
              Tipo: TSQLType; Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String; ValCampos,
              ValIndex: array of Variant; UserDataAlterweb: Boolean;
              HowIns: TdmkSQLInsert; ComplUpd: String; Device: TDeviceType;
              Sincro: Boolean = False): Boolean;
    function  TextoSQLInsUpd(const DBType: TDBType; const Tipo: TSQLType; const
              Tabela: String; const Auto_increment: Boolean; const SQLCampos,
              SQLIndex: array of String; const ValCampos, ValIndex:
              array of Variant; const UserDataAlterweb: Boolean; const HowIns:
              TdmkSQLInsert; const ComplUpd: String; const Device: TDeviceType;
              const Sincro: Boolean; var SQL: String): Boolean;
    function  SQLInsUpd(QrUpd: TDataSet; DataBase: TComponent;
              Tipo: TSQLType; Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String; ValCampos,
              ValIndex: array of Variant; UserDataAlterweb: Boolean;
              HowIns: TdmkSQLInsert; ComplUpd: String; Device: TDeviceType;
              Sincro: Boolean = False): Boolean;
    function  ExecutaSQLQuery0(Query: TDataSet; DataBase: TComponent;
              SQL: array of String): Boolean;
    function  ObtemVersaoAppDB(Query: TDataSet; DataBase: TComponent;
              DriverNome: TDriverName; Device: TDeviceType): Integer;
    function  ObtemCodigoInt(Tabela, Campo: String; Query: TDataSet;
              DataBase: TComponent): Integer;
    function  ObtemCodigoInt_MulFld(Tabela, Campo: String; Query: TDataSet;
              DataBase: TComponent; MulFld: array of String; MulVal: array of
              Variant): Integer;
    function  GetNxtCodigoInt(const Tabela, Campo: String; var SQLType:
              TSQLType; const Atual: Integer): Integer;
    function  GetNxtCodigoInt_MulFld(const Tabela, Campo: String; var SQLType:
              TSQLType; const Atual: Integer; MulFld: array of String;
              MulVal: array of Variant): Integer;
    function  ObtemCodigoInt_Sinc(Tabela, Campo: String; Query: TDataSet;
              DataBase: TComponent; SQLTipo: TSQLType; Device: TDeviceType): Integer;
    function  ValidaDadosCampo(Query: TDataSet; DB: TComponent; Campo,
              Tabela: String; Valor: Integer; PemiteZero: Boolean = False): Boolean;
    function  ObtemIndexDeTabela(Query: TDataSet; CampoCodigo: String; Valor: Integer): Integer;
    procedure MostraSQL(Query: TDataSet; Memo: TMemo; Titulo: String);
    procedure RemoveUsuarios(DataBase: TComponent; DriverNome: TDriverName);
    function  RefreshQuery(Query: TDataSet): Boolean;
    function  ReopenQuery(Query: TDataSet; DataBase: TComponent): Boolean;
    function  CodigoExisteInt1InsUpd(Inteiro: Integer; Tabela, Campo: String;
              Query: TDataSet; DataBase: TComponent): TSQLType;

    // S I N C R O N I S M O  W E B
    procedure AtualizaSincroDB(DataBase: TComponent; Device: TDeviceType;
              Tabela, DataHora: String);
    function  ObtemDataSincro(DataBase: TComponent; Tabela: String): TDateTime;
    // S Q L I T E
    function  LocalizaPriorNextLargeIntQr(Tabela: TDataSet; Campo: TLargeIntField;
              Atual: LargeInt): LargeInt;
    function  CriaFm(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; Titulo: String = ''): Boolean;
    {$IfDef DB_SQLite}
    function  LocalizaPriorNextIntQr(Tabela: TDataSet; Campo: TIntegerField;
              Atual: Integer): Integer;
    function  EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo; Estrutura,
              EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
              LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
              LaTempoT: TComponent; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DataBase: TComponent; LBTabToRecriar:
              TListBox): Boolean;
    function  VerificaEstrutura_SQLite(DBName: String; Memo: TMemo;
              RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
              LaTempoT: TComponent; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DataBase: TComponent; LBTabToRecriar:
              TListBox): Boolean;
    function  VerificaCampos_SQLite(DBName: String; TabelaNome,
              TabelaBase: String; Memo: TMemo; RecriaRegObrig,
              Tem_Del, Tem_Sync: Boolean; LaAviso1B, LaAviso2B, LaAviso1G,
              LaAviso2G: TComponent; MeFldsNoNeed: TMemo;
              DataBase: TComponent; LBTabToRecriar: TListBox): Integer;
    function  CriaTabela_SQLite(DBName: String; TabelaNome, TabelaBase: String;
              Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean; LBIAviso1B,
              LBIAviso2B, LBIAviso1G, LBIAviso2G: TComponent;
              DataBase: TComponent; Tem_Sync: Boolean = False): Boolean;
    function  CampoAtivo_SQLite(DBName: String; TabelaNome, TabelaBase,
              Campo: String; Memo: TMemo; QrNTV: TDataSet;
              DataBase: TComponent): Integer;
    function  ExcluiIndice_SQLite(DBName: String; Tabela, IdxNome,
              Aviso: String; Motivo: TMyMotivo; Memo: TMemo;
              DataBase: TComponent): Integer;
    function  IndiceExiste_SQLite(Tabela, Indice: String; Memo: TMemo;
              DataBase: TComponent): Integer;
    function  RecriaIndice_SQLite(Tabela, IdxNome, Aviso: String;
              Memo: TMemo; DataBase: TComponent): Integer;
    function  CriaIndice_SQLite(Tabela, IdxNome, Aviso: String; Memo: TMemo;
              DataBase: TComponent): Integer;
    function  TabelaExiste_SQLite1(Tabela: String; QrTabs: TDataSet): Boolean;
    function  TabelaExiste_SQLite2(Tabela: String; Query: TDataSet;
              DataBase: TComponent): Boolean;

    function  CampoExiste_SQLite(Tabela, Campo: String; Memo: TMemo;
              QrNTV: TDataSet): Integer;
    function  ExcluiRegistroInt1_SQLite(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TComponent): Integer;
    function  ExcluiRegistroInt1_Sinc_SQLite(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TComponent): Integer;
    {$EndIf}

    {$IfNDef DMK_FMX}
  {$IfDef DB_SQLite}
    function  VerificaRegistrosObrigatoriosSQLite_Inclui(DataBase: TZConnection;
              TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean;
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): TResultVerify;
  {$EndIf}
{
    function  AbreTableZ(Table: TZTable; DB: TZConnection; Aviso: String = ''):
              Boolean;
    procedure LeMeuSQL_Fixo_z(Query: TzQuery; Arquivo: String; Memo: TMemo;
              WinArq, AskShow: Boolean; ForceMsgErr: Boolean = False);
    procedure ExportaRegistrosEntreDBs_NovoZ(TabOrig, TabDest, CondicaoSQL:
              String; BaseOrig, BaseDest: TZConnection; RichEdit: TRichEdit);
    function  ObtemCamposDeTabelaIdenticaZ(DataBase: TZConnection;
              Tabela, Prefix: String): String;// Prefix -> 'la.' = ref tabela
    function  ArrayDeTabelaIdentica_CamposZ(DataBase: TZConnection;
              Tabela: String): TUMyArray_Str;
    function  ArrayDeTabelaIdentica_ValuesZ(Tabela: TZQuery): TUMyArray_Var;
    procedure IncluiRegistroTbZ(Form: TForm; Tabela: TZTable; ComponentToHide,
              ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
    procedure AlteraRegistroTb(Form: TForm; Tabela: TZTable; ComponentToFocus:
              TWinControl; ImgTipo: TdmkImage);
    procedure MostraEdicaoTbZ(Form: TForm);
    procedure OcultaEdicaoTbZ(Form: TForm);
    procedure DesisteRegistroTbZ(Form: TForm; Tabela: TZTable;
              NomeTabela, NomeCampoCodigo: String;
              ImgTipo: TdmkImage; CodigoALiberar: Integer);
    procedure PoeEmLivreZ(Database: TZConnection; TabLivre, Table: String;
              Codigo: Integer);
    procedure UpdUnlockZ(Registro: Integer; Database: TZConnection; Table,
              Field: String);
    procedure ConfirmaRegistroTbZ_Numero(Form: TForm; Tabela: TZTable;
              ImgTipo: TdmkImage; PainelDados, PainelConfirma, PainelControle:
              TPanel; DBGItens: TDBGrid; CampoNumero: String = 'Numero';
              CampoCodUsu: String = '');
    function  VerificaCamposObrigatoriosZ(Tabela: TZTable): Boolean;
    procedure DadosAutomaticosTbZ(Tabela: TZTable);
    procedure BELY_Tb(Tabela: TZTable; Campo: String);
    function  SQLLoc1(Query: TZQuery; Tabela, Campo: String;
              Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
*)
    {$EndIf}

    {$EndIf}
    // M Y S Q L
    {$IfnDef DMK_FMX}
    {$IfDef DB_SQLite}
    {$Else}
    function  ExcluiRegistroInt1_Sinc_MySQL(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TComponent): Integer;
    {$EndIf}
    {$EndIf}
    // B A N C O   D E   D A D O S
    function  GeraCondicaoDeSQL(var TextoSQL: String; const Tabela: String;
              const SQLCampos, Compare: array of String; const ValCampos:
              array of Variant; const FirstCondition, ComplSQL: String):
              Boolean;
    function  QueryTemRegistros(Query: TDataSet): Boolean;
    //
    function  ExcluiRegistroInt1(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TComponent): Integer;
    function  ExcluiRegistroInt1_Sinc(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TComponent): Integer;
    function  ExcluiRegistros(Pergunta: String; QrUpd: TDataset; DataBase:
              TComponent; Tabela: String; SQLCampos, Compare: array of String;
              ValCampos: array of Variant; ComplSQL: String): Boolean;
    function  ObtemListaDeColunasDeTabela(DataBase: TComponent;
              DriverNome: TDriverName; Tabela: String; TipoColuna: TTipoColuna;
              var Colunas: TStringList): Boolean;

    // M U L T I  O S
    procedure GravaAviso(Aviso: String; Memo: TMemo);
    procedure AdicionaDefault_SQLite(var Opcoes: String; Tipo, Padrao: String);
    procedure AdicionaFldNoNeed_SQLite(MeFldsNoNeed: TMemo; Tabela, Campo: String);
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  TabelaAtiva(Tabela: String): Boolean;
    // FechaAplicativo // FechaApp
    procedure EncerraApplicacao();

    // W I N D O W S
 {$IfNDef DMK_FMX}
    // V C L
(*
    procedure ObtemCodigoECodUsu(const EdCodigo, EdCodUsu: TdmkEdit; var
              SQLType: TSQLType; var Codigo, CodUsu: Integer; const NoTabela,
              NoFldCodigo, NoFldCodUsu: String);
    function  ConfigPanelInsUpd(Acao: TSQLType; Form: TForm; Panel: TWinControl;
              Query: TDataSet; WinCtrlsToHide, WinCtrlsToShow: array of TWinControl;
              CompoToFocus: TWinControl; CompoTipo: TControl; Tabela: String):
              Boolean;
*)
 {$EndIf}

  end;

var
  Grl_DmkDB: TUnGrl_DmkDB;
const
  CO_JOKE_SQL = '$#'; // Texto livre no SQLInsUpd

implementation

uses MyListas, UnGrl_Geral, (*UnGrlUsuarios, FMX#err*)
{$IFDEF UsaWSuport} WSuporte, {$EndIf}
{$IfDef DB_SQLite}  {$IfNDef DMK_FMX} ModuleGeralZ, DmkDialog, {$EndIf}  {$EndIf}
Module;

{ TUnGrl_DmkDB }

var
  FListaSQL: TStringList;


function TUnGrl_DmkDB.ValidaTipoComp(Componente: array of TComponent;
  Funcao: String): Boolean;
var
  I: Integer;
  CompClass: String;
  Comp: TComponent;
begin
  Result := False;
  //
  for I := Low(Componente) to High(Componente) do
  begin
    Comp := Componente[I];
    //
    if Comp <> nil then
    begin
      CompClass := Comp.ClassName;
      //
      if UpperCase(CompClass) = 'TFDQUERY' then
        Result := True
      else
      if UpperCase(CompClass) = 'TZQUERY' then
        Result := True
      else
      if UpperCase(CompClass) = 'TMYSQLQUERY' then
        Result := True
      else
      if UpperCase(CompClass) = 'TFDCONNECTION' then
        Result := True
      else
      if UpperCase(CompClass) = 'TZCONNECTION' then
        Result := True
      else
      if UpperCase(CompClass) = 'TMYSQLDATABASE' then
        Result := True
      else
      if UpperCase(CompClass) = 'TLABEL' then
        Result := True
      else
      if UpperCase(CompClass) = 'TLISTBOXITEM' then
        Result := True;
      //
      if not Result then
      begin
        Grl_Geral.MB_Erro('A classe "' + Componente[I].ClassName +
          '" n�o est� implementada!' + sLineBreak + 'Na fun��o: ' + Funcao +
          sLineBreak + 'Avise a DERMATEK!');
        Exit;
      end;
    end else
      Result := True;
  end;
end;

function TUnGrl_DmkDB.AbreSQLQuery0(Query: TDataSet; DataBase: TComponent;
  SQL: array of String): Boolean;
var
  I: Integer;
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  QuerySQL: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  DB: TZConnection;
  QuerySQL: TZQuery;
  {$Else}
  DB: TmySQLDatabase;
  QuerySQL: TmySQLQuery;
  {$EndIf}
  {$EndIf}
begin
  Result := False;
  //
  if not ValidaTipoComp([Query, DataBase], 'TUnGrl_DmkDB.AbreSQLQuery0') then
    Exit;
  //
  {$IfDef DMK_FMX}
  DB       := TFDConnection(DataBase);
  QuerySQL := TFDQuery(Query);
  // Evitar erro em QuerySQL.RecordCount!!!
  QuerySQL.FetchOptions.Mode := FireDAC.Stan.Option.TFDFetchMode.fmAll;
  {$Else}
  {$IfDef DB_SQLite}
  DB       := TZConnection(DataBase);
  QuerySQL := TZQuery(Query);
  {$Else}
  DB       := TmySQLDatabase(DataBase);
  QuerySQL := TmySQLQuery(Query);
  {$EndIf}
  {$EndIf}
  //
  QuerySQL.Close;
  {$IfDef DMK_FMX}
  QuerySQL.Connection := DB;
  {$Else}
  {$IfDef DB_SQLite}
  QuerySQL.Connection := DB;
  {$Else}
  QuerySQL.DataBase := DB;
  {$EndIf}
  {$EndIf}
  //
  try
    QuerySQL.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      QuerySQL.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
      begin
        if SQL[I] <> '' then
          QuerySQL.SQL.Add(SQL[I]);
      end;
    end;
    if QuerySQL.SQL.Text = '' then
    begin
      Grl_Geral.MB_Erro('Texto da SQL indefinido no SQLQuery!' + sLineBreak +
        'Avise a DERMATEK!');
    end else
    begin
      Query.Open;
      //
      Result := True;
    end;
  except
    on e: Exception do
    begin
      if (*(pos(Lowercase('actual: WideString'), Lowercase(e.Message)) > 0)
      and*) (Query.State <> dsInactive) and (Query.RecordCount = 0) then
      begin
        // nada
      end else
      begin
        Grl_Geral.MB_Erro('Erro ao tentar abrir uma SQL no SQLQuery!' +
          sLineBreak + TComponent(QuerySQL.Owner).Name + '.' + QuerySQL.Name +
          sLineBreak + e.Message + sLineBreak + QuerySQL.SQL.Text);
      end;
    end;
  end;
end;

function TUnGrl_DmkDB.EfetuaVerificacoes(DBName: String; Memo: TMemo; Estrutura,
  EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2,
  LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
  LaTempoF, LaTempoT: TComponent; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; Device: TDeviceType; DriverNome: TDriverName;
  DataBase: TComponent; VersaoAtual: Integer; LBTabToRecriar: TListBox): Boolean;
begin
  Result := False;
  //
  case DriverNome of
    istMySQL:
      Grl_Geral.MB_Aviso('N�o implementado!');
    istSQLite:
    begin
      {$IfDef DB_SQLite}
      Result := EfetuaVerificacoes_SQLite(DBName, Memo, Estrutura,
                EstrutLoc, Controle, Pergunta, RecriaRegObrig, LaAvisoP1,
                LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B,
                LaAviso1G, LaAviso2G, LaTempoI, LaTempoF, LaTempoT,
                MeTabsNoNeed, PB, MeFldsNoNeed, DataBase, LBTabToRecriar);
      if Result then
      begin
        if DBName = CO_DBNome then
          AtualizaVersaoTBControle(DataBase, Device, VersaoAtual);
      end;
      {$Else}
      MostraMsgDB_SQLite;
      {$EndIf}
    end;
  end;
end;

procedure TUnGrl_DmkDB.AtualizaVersaoTBControle(DataBase: TComponent;
  Device: TDeviceType; Versao: Integer);
var
  {$IfDef DMK_FMX}
  QueryUpd: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  QueryUpd: TZQuery;
  {$Else}
  QueryUpd: TMySQLQuery;
  {$EndIf}
  {$EndIf}
begin
  {$IfDef DMK_FMX}
  QueryUpd := TFDQuery.Create(DataBase);
  {$Else}
  {$IfDef DB_SQLite}
  QueryUpd := TZQuery.Create(DataBase);
  {$Else}
  QueryUpd := TMySQLQuery.Create(DataBase);
  {$EndIf}
  {$EndIf}
  try
    CarregaSQLInsUpd(QueryUpd, DataBase, stUpd, 'controle', False,
      ['Versao'], ['Codigo'], [Versao], [1], False, dmksqlinsInsOnly, '', Device);
  finally
    if QueryUpd <> nil then
      QueryUpd.Free;
  end;
end;

function TUnGrl_DmkDB.CarregaSQLInsUpd(QrUpd: TDataSet; DataBase: TComponent;
  Tipo: TSQLType; Tabela: String; Auto_increment: Boolean; SQLCampos,
  SQLIndex: array of String; ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; HowIns: TdmkSQLInsert; ComplUpd: String; Device: TDeviceType;
  Sincro: Boolean = False): Boolean;
var
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  QueryUpd, QueryLoc: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  DB: TZConnection;
  QueryUpd, QueryLoc: TZQuery;
  {$Else}
  DB: TMySQLDataBase;
  QueryUpd, QueryLoc: TMySQLQuery;
  {$EndIf}
  {$EndIf}
  LastAcao: TLastAcao;
  AlterWeb: TAlterWeb;
  i, j, k, CliIntSync, Usuario, Var_UserId, Var_EntId: Integer;
  LastModifi: TDateTime;
  Valor, Liga, Data, LastModifi_Txt, Tab, Campos, Valores, SQLWhere: String;
begin
  Result := False;
  //
  if not ValidaTipoComp([QrUpd, DataBase], 'TUnGrl_DmkDB.CarregaSQLInsUpd') then
    Exit;
  //
  {$IfDef DMK_FMX}
  DB       := TFDConnection(DataBase);
  QueryUpd := TFDQuery(QrUpd);
  {$Else}
  {$IfDef DB_SQLite}
  DB       := TZConnection(DataBase);
  QueryUpd := TZQuery(QrUpd);
  {$Else}
  DB       := TMySQLDataBase(DataBase);
  QueryUpd := TMySQLQuery(QrUpd);
  {$EndIf}
  {$EndIf}

  Var_UserId := VAR_WEB_USR_ID;
  Var_EntId  := VAR_WEB_USR_ENT;

  if Var_EntId = 0 then
    Var_EntId := VAR_API_ID_ENTIDADE;
  if Var_UserId = 0 then
    Var_UserId := VAR_API_ID_USUARIO;

  if Sincro = True then
    Usuario := Var_UserId
  else
    Usuario := VAR_USUARIO;

  i := High(SQLCampos);
  j := High(ValCampos);

  if i <> j then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + Grl_Geral.FF0(i+1) + ' campos e ' +
      Grl_Geral.FF0(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;

  i := High(SQLIndex);
  j := High(ValIndex);

  if i <> j then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + Grl_Geral.FF0(i+1) + ' �ndices e ' +
      Grl_Geral.FF0(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;

  if (i >= 0) and Auto_increment then
  begin
    Grl_Geral.MB_Erro('AVISO! Existem ' + Grl_Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  Tab      := LowerCase(Tabela);
  AlterWeb := alDsk;

  case Device of
    stDesktop:
      AlterWeb := alDsk;
    stWeb:
      AlterWeb := alWeb;
    stMobile:
      AlterWeb := alMob;
  end;

  if ((Tipo <> stIns) and (Tipo <> stUpd)) then
  begin
    Grl_Geral.MB_Erro('AVISO: O status da a��o est� definida como ' +
      '"' + Grl_DmkDB.NomeTipoSQL(Tipo) + '"');
  end;

  Data := #39 + Grl_Geral.FDT(Date, 1) + #39;

  QueryUpd.SQL.Clear;
  {$IfDef DMK_FMX}
  QueryUpd.Connection := DB;
  {$Else}
  {$IfDef DB_SQLite}
  QueryUpd.Connection := DB;
  {$Else}
  QueryUpd.DataBase := DB;
  {$EndIf}
  {$EndIf}

  if Tipo = stIns then
  begin
    case HowIns of
      //dmksqlinsIndef=0,
      (*1*)dmksqlinsInsOnly:
        QueryUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' (');
      (*2*)dmksqlinsInsIgnore:
      begin
        // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
        {$IfDef DB_SQLite}
        QueryUpd.SQL.Add('INSERT OR IGNORE INTO ' + Lowercase(Tab) + ' (')
        {$Else}
        QueryUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' (')
        {$EndIf}
      end;
      (*3*)dmksqlinsInsReplace:
      begin
        // n�o gera erro e substitui o registro quando poderia duplicar registro com chave
        {$IfDef DB_SQLite}
        QueryUpd.SQL.Add('INSERT OR REPLACE INTO ' + Lowercase(Tab) + ' (')
        {$Else}
        QueryUpd.SQL.Add('REPLACE INTO ' + Lowercase(Tab) + ' (')
        {$EndIf}
      end;
      else
      begin
        Grl_Geral.MB_Erro('"HowIns" n�o implementado: ' + Grl_Geral.FF0(Integer(HowIns)));
        Exit;
      end;
    end;
  end else
  begin
    QueryUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');

    if UserDataAlterweb then
      QueryUpd.SQL.Add('AlterWeb='+ Grl_Geral.FF0(Integer(AlterWeb)) +', ');
  end;

  Campos  := '';
  Valores := '';
  j       := System.High(SQLCampos);

  for i := System.Low(SQLCampos) to j do
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', ' + SQLCampos[i];
      Valor   := Grl_Geral.VariavelToStringAD(ValCampos[i]);
      Valores := Valores + ', ' + Valor;
    end else
    begin
      if SQLCampos[i] = CO_JOKE_SQL then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
          QueryUpd.SQL.Add(ValCampos[i] + ', ')
        else
          QueryUpd.SQL.Add(ValCampos[i]);
      end else
      begin
        Valor := Grl_Geral.VariavelToStringAD(ValCampos[i]);

        if (i < j) or UserDataAlterweb then
          QueryUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
        else
          QueryUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
      end;
    end;
  end;

  if Sincro then
  begin
    LastModifi     := Grl_Geral.DateTime_MyTimeZoneToUTC(Now);
    LastModifi_Txt := #39 + Grl_Geral.FDT(LastModifi, 109) + #39 ;
    CliIntSync     := Var_EntId;

    if Var_UserId  = 0 then
    begin
      Grl_Geral.MB_Erro('A vari�vel "VAR_WEB_USR_ID / VAR_API_ID_USUARIO" n�o est� definida!');
      Exit;
    end;

    if Var_EntId = 0 then
    begin
      Grl_Geral.MB_Erro('A vari�vel "VAR_WEB_USR_ENT / VAR_API_ID_ENTIDADE" n�o est� definida!');
      Exit;
    end;

    if Tipo = stIns then
    begin
      LastAcao := laIns; //Inclus�o
    end else
    begin
      //Se n�o faz sincronismo, manter o "lastacao" = "inclus�o" por causa do sincronismo
      LastAcao := laUpd;//Altera��o
      SQLWhere := '';

      for k := Low(SQLIndex) to High(SQLIndex) do
      begin
        SQLWhere := SQLWhere + ' AND ' + SQLIndex[k] + '="'+ Grl_Geral.VariavelToStringAD(ValIndex[k]) +'"';
      end;
      {$IfDef DMK_FMX}
      QueryLoc := TFDQuery.Create(DB);
      {$Else}
      {$IfDef DB_SQLite}
      QueryLoc := TZQuery.Create(DB);
      {$Else}
      QueryLoc := TmySQLQuery.Create(DB);
      {$EndIf}
      {$EndIf}
      try
        AbreSQLQuery0(QueryLoc, DataBase, [
          'SELECT LastAcao ',
          'FROM ' + Tabela,
          'WHERE AlterWeb=' + Grl_Geral.FF0(Integer(Grl_Geral.ObtemAlterWebDeDeviceType(Device))),
          SQLWhere,
          '']);
        if QueryLoc.RecordCount > 0 then
        begin
          if QueryLoc.FieldByName('LastAcao').AsInteger = 0 then //Inclus�o
            LastAcao := laIns;
        end;
      finally
        QueryLoc.Free;
      end;
    end;

    if Tipo = stIns then
    begin
      Campos  := Campos + ', LastModifi, LastAcao, CliIntSync';
      Valores := Valores + ', ' + LastModifi_Txt + ', ' + Grl_Geral.FF0(Integer(LastAcao)) +
                   ', ' + Grl_Geral.FF0(CliIntSync);
    end else
      QueryUpd.SQL.Add('LastModifi=' + LastModifi_Txt + ', LastAcao=' +
        Grl_Geral.FF0(Integer(LastAcao)) + ', CliIntSync=' + Grl_Geral.FF0(CliIntSync) + ', ');
  end;

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', DataCad, DataAlt, UserCad, UserAlt, AlterWeb';
      Valores := Valores + ', ' + Data + ', "1899-12-30 12:00", ' + Grl_Geral.FF0(Usuario) +
                   ', 0, ' + Grl_Geral.FF0(Integer(AlterWeb));
    end else
      QueryUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + Grl_Geral.FF0(Usuario));
  end;

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Grl_Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
  end else
  begin
    for i := System.Low(SQLIndex) to System.High(SQLIndex) do
    begin
      if Tipo = stIns then
      begin
        Liga := ', ';
      end else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;

      if SQLIndex[i] = CO_JOKE_SQL then
      begin
        QueryUpd.SQL.Add(Liga + ValIndex[i]);
      end else
      begin
        if Tipo = stIns then
        begin
          Campos  := Campos + ', ' + SQLIndex[i];
          Valor   := Grl_Geral.VariavelToStringAD(ValIndex[i]);
          Valores := Valores + ', ' + Valor;
        end else
        begin
          Valor := Grl_Geral.VariavelToStringAD(ValIndex[i]);
          //
          QueryUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;

  if Tipo = stIns then
  begin
    {$IfDef DMK_FMX}
    Campos  := Campos.Substring(2) + ') VALUES (';
    Valores := Valores.Substring(2) + ');';
    {$Else}
    Campos  := Copy(Campos, 3, Length(Campos)) + ') VALUES (';
    Valores := Copy(Valores, 3, Length(Valores)) + ');';
    {$EndIf}

    QueryUpd.SQL.Add(Campos);
    QueryUpd.SQL.Add(Valores);
  end;

  if Trim(ComplUpd) <> '' then
  begin
    QueryUpd.SQL.Add(ComplUpd);
  end;
  try
    QueryUpd.ExecSQL();
    Result := True;
  except
    on E: Exception do
      Grl_Geral.MB_Erro('ERRO de SQL: ' + sLineBreak +
      E.ClassName + ': ' + E.Message + '.' + sLineBreak +
      QueryUpd.SQL.Text)
  end;
end;

function TUnGrl_DmkDB.CodigoExisteInt1InsUpd(Inteiro: Integer; Tabela, Campo: String;
  Query: TDataSet; DataBase: TComponent): TSQLType;
var
  Atual: Integer;
begin
  Result := stNil;
  if not ValidaTipoComp([Query, DataBase], 'TUnGrl_DmkDB.CodigoExisteInt1InsUpd(') then
    Exit;
  //
  AbreSQLQuery0(Query, DataBase, [
  'SELECT ' + Campo,
  'FROM ' + Tabela,
  'WHERE ' + Campo + '=' + Grl_Geral.FF0(Inteiro),
  '']);
  //
  if Query.RecordCount > 0 then
    Result := stUpd
  else
    Result := stIns;
end;

{$IfDef DB_SQLite}

    {$IfNDef DMK_FMX}
procedure TUnGrl_DmkDB.AlteraRegistroTb(Form: TForm; Tabela: TZTable;
  ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
begin
  Screen.Cursor := crHourGlass;
  try
    MostraEdicaoTbZ(Form);
    if ImgTipo <> nil then ImgTipo.SQLType := stUpd;
    Tabela.Edit;
    if ComponentToFocus <> nil then
      ComponentToFocus.SetFocus;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnGrl_DmkDB.SQLLoc1(Query: TZQuery; Tabela, Campo: String;
  Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
var
  Txt: String;
begin
  Txt := Grl_Geral.VariavelToStringAD(Valor);
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + '=' + Txt);
  AbreQuery(Query, Query.Connection);
  //
  Result := Query.RecordCount;
  //
  if (Result = 0) and (Trim(MsgNaoLoc) <> '') then
    Grl_Geral.MB_Aviso(MsgNaoLoc);
  if (Result > 0) and (Trim(MsgLoc) <> '') then
    Grl_Geral.MB_Aviso(MsgLoc + sLineBreak + 'Itens = ' +
    IntToStr(Result));
end;

procedure TUnGrl_DmkDB.OcultaEdicaoTbZ(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := True else
    if Form.Components[i] is TPanel then
    begin
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
        TPanel(Form.Components[i]).Enabled := False else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
        TPanel(Form.Components[i]).Visible := False else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
        TPanel(Form.Components[i]).Visible := True;
    end;
  end;
end;

procedure TUnGrl_DmkDB.PoeEmLivreZ(Database: TZConnection; TabLivre,
  Table: String; Codigo: Integer);
begin
{ resaervado para o futuro caso necessite!
  QvUpdZ.Close;
  QvUpdZ.Database := Database;
  QvUpdZ.SQL.Clear;
  QvUpdZ.SQL.Add('INSERT INTO '+lowercase(tablivre)+' SET Codigo=:P0, Tabela=:P1');
  QvUpdZ.Params[0].AsInteger := Codigo;
  QvUpdZ.Params[1].AsString := lowercase(Table);
  QvUpdZ.ExecSQL;
}
end;

procedure TUnGrl_DmkDB.MostraEdicaoTbZ(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := False else
    if Form.Components[i] is TPanel then
    begin
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
        TPanel(Form.Components[i]).Enabled := True else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
        TPanel(Form.Components[i]).Visible := True else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
        TPanel(Form.Components[i]).Visible := False;
    end;
  end;
end;

procedure TUnGrl_DmkDB.DadosAutomaticosTbZ(Tabela: TZTable);
begin
  if Tabela.State = dsInsert then
  begin
    Tabela.FieldByName('DataCad').AsDateTime := Date;//FormatDateTime(VAR_FORMATDATE, Date);
    Tabela.FieldByName('UserCad').AsInteger  := VAR_USUARIO;
    if VAR_IDServr <> 0 then
      Tabela.FieldByName('IDServr').AsInteger  := VAR_IDServr;
  end else if Tabela.State = dsEdit then
  begin
    Tabela.FieldByName('DataAlt').AsDateTime := Date;//FormatDateTime(VAR_FORMATDATE, Date);
    Tabela.FieldByName('UserAlt').AsInteger  := VAR_USUARIO;
  end;
end;
procedure TUnGrl_DmkDB.BELY_Tb(Tabela: TZTable; Campo: String);
var
  Atual: Double;
begin
{ Ver se precisa no futuro!
  Screen.Cursor := crHourGlass;
  case Campo of
    belyCodigo   : Atual := Tabela.FieldByName('Codigo').AsInteger;
    belyControle : Atual := Tabela.FieldByName('controle').AsInteger;
    belyNumero   : Atual := Tabela.FieldByName('Numero').AsInteger;
    else Atual   := 0;
  end;
  if Atual < 0.1 then
  begin
    case Campo of
      belyCodigo   : Tabela.FieldByName('Codigo').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'Codigo');
      belyControle : Tabela.FieldByName('controle').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'controle');
      belyNumero   : Tabela.FieldByName('Numero').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'Numero');
      else begin
        Grl_Geral.MB_Erro('ERRO. Campo desconhecido na procedure "BELY"');
      end;
    end;
  end;
  Screen.Cursor := crDefault;
}
end;


function TUnGrl_DmkDB.AbreTableZ(Table: TZTable; DB: TZConnection;
  Aviso: String): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
begin
  try
    Table.Close;
    Table.Connection := DB;
    Table. Open ;
    Result := True;
  except
    Grl_Geral.MB_Erro('N�o foi poss�vel abrir a TABLE: ' + Table.Name + sLineBreak +
    'Tabela: "' + Table.TableName + '"');
    raise;
  end;
end;

{$EndIf}

(*
function TUnGrl_DmkDB.EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo;
  Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1,
  LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
  LaTempoI, LaTempoF, LaTempoT: TComponent; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; DataBase: TComponent): Boolean;
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
begin
  Result := False;
  {$IfDef DMK_FMX}
  Qry := TFDQuery.Create(DataBase);
  {$Else}
  Qry := TZQuery.Create(DataBase);
  {$EndIf}
  try
    Temp.InfoMemoStep('UnGrl_DmkDB.127');
    MeTabsNoNeed.Lines.Clear;

    if Grl_Geral.MB_Pergunta('Ser� realizada uma verifica��o na base de dados: ' +
      sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
      'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
      'perante restaura��o de backup.') <> idYes
    then
      Exit;

    Temp.InfoMemoStep('UnGrl_DmkDB.980');
    Application.ProcessMessages;

    Result    := True;
    FPergunta := Pergunta;
    try
      // verifica se a vers�o atual � mais nova
      // se n�o � impede a verifica��o
      if DBName <> '' then
      begin
        AbreSQLQuery0(Qry, DataBase, [
          'SELECT name FROM sqlite_master ',
          'WHERE type=''table''',
          'AND name LIKE ''Controle''',
          'ORDER BY name;',
          '']);
         Temp.InfoMemoStep('UnGrl_DmkDB.996');
         if Qry.RecordCount > 0 then
        begin
          AbreSQLQuery0(Qry, DataBase, [
            'PRAGMA table_info(controle);',
            '']);
           Temp.InfoMemoStep('UnGrl_DmkDB.1002');
          if Qry.RecordCount > 0 then
          begin
           Temp.InfoMemoStep('UnGrl_DmkDB.1005');
            AbreSQLQuery0(Qry, DataBase, [
              'SELECT Versao FROM controle',
              '']);
            Temp.InfoMemoStep('UnGrl_DmkDB.1009');
            if CO_VERSAO < Qry.FieldByName('Versao').AsLargeInt then
            begin
             Temp.InfoMemoStep('UnGrl_DmkDB.1012');
              Grl_Geral.MB_Aviso('Verifica��o de banco de dados cancelada! ' +
                sLineBreak +  ' A vers�o deste arquivo � inferior a cadastrada ' +
                'no banco de dados.');
              Exit;
            end;
          end;
        end;
      end else
      begin
         Temp.InfoMemoStep('UnGrl_DmkDB.1022');
        Grl_Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
      end;
      //
      Temp.InfoMemoStep('UnGrl_DmkDB.1026');
      VerificaEstrutura_SQLite(DBName, Memo, RecriaRegObrig, LaAviso1R,
        LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
        LaTempoF, LaTempoT, MeTabsNoNeed, PB, MeFldsNoNeed, DataBase);
    except
      raise;
    end;
    Grl_Geral.MB_Aviso('Verifica��o de banco de dados finalizada!');
  finally
    Qry.Free;
  end;
end;
*)

(*
function TUnGrl_DmkDB.EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo;
  Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1,
  LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
  LaTempoI, LaTempoF, LaTempoT: TComponent; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; DataBase: TComponent): Boolean;
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
  sPergunta: String;
  Continua: Boolean;
  FFmDMkDialog: TFmDmkDialog;
  //
  function Verifica(): Boolean;
  begin
    VAR_MODAL_RESULT := System.UITypes.mrNone;
    FmDMkDialog := TFmDMkDialog.Create(nil);
    FmDMkDialog.ShowModal(
    procedure(ModalResult: TModalResult)
    begin
      //E executado quando muda o Modalresult do formulario
      if (ModalResult = 1) or (ModalResult<> 1) then
      begin
        if VAR_MODAL_RESULT = idYes then // 6 then
        begin
          Grl_Geral.MB_Aviso('Verifica��o ser� iniciada!');
          //Application.ProcessMessages;
          try
            // verifica se a vers�o atual � mais nova
            // se n�o � impede a verifica��o
            if DBName <> '' then
            begin
              Temp.InfoMemoStep('Nome BD: ' + DBName);
{
              AbreSQLQuery0(Qry, DataBase, [
                'SELECT name FROM sqlite_master ',
                'WHERE type=''table''',
                'AND name LIKE ''Controle''',
                'ORDER BY name;',
                '']);
               Temp.InfoMemoStep('UnGrl_DmkDB.996');
               if Qry.RecordCount > 0 then
              begin
                AbreSQLQuery0(Qry, DataBase, [
                  'PRAGMA table_info(controle);',
                  '']);
                 Temp.InfoMemoStep('UnGrl_DmkDB.1002');
                if Qry.RecordCount > 0 then
                begin
                 Temp.InfoMemoStep('UnGrl_DmkDB.1005');
                  AbreSQLQuery0(Qry, DataBase, [
                    'SELECT Versao FROM controle',
                    '']);
                  Temp.InfoMemoStep('UnGrl_DmkDB.1009');
                  if CO_VERSAO < Qry.FieldByName('Versao').AsLargeInt then
                  begin
                   Temp.InfoMemoStep('UnGrl_DmkDB.1012');
                    Grl_Geral.MB_Aviso('Verifica��o de banco de dados cancelada! ' +
                      sLineBreak +  ' A vers�o deste arquivo � inferior a cadastrada ' +
                      'no banco de dados.');
                    Exit;
                  end;
                end;
              end;
}
            end else
            begin
               Temp.InfoMemoStep('UnGrl_DmkDB.1022');
              Grl_Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
            end;
            //
{
            Temp.InfoMemoStep('UnGrl_DmkDB.1026');
            VerificaEstrutura_SQLite(DBName, Memo, RecriaRegObrig, LaAviso1R,
              LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
              LaTempoF, LaTempoT, MeTabsNoNeed, PB, MeFldsNoNeed, DataBase);
            Grl_Geral.MB_Aviso('Verifica��o de banco de dados finalizada!');
}
          except
            raise;
          end;
        end;
      end;
    end);
  end;
begin
  sPergunta  := 'Ser� realizada uma verifica��o na base de dados: ' +
    sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
    'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
    'perante restaura��o de backup. Deseja continuar?';
  //
  Result := False;
  {$IfDef DMK_FMX}
  Qry := TFDQuery.Create(DataBase);
  {$Else}
  Qry := TZQuery.Create(DataBase);
  {$EndIf}
  try
    Temp.InfoMemoStep('UnGrl_DmkDB.127');
    MeTabsNoNeed.Lines.Clear;


{    if Grl_Geral.MB_Pergunta(sPergunta) <> idYes
    then
      Exit;
}

    VAR_MODAL_RESULT := System.UITypes.mrNone;
    VAR_MODAL_MESSAGE := sPergunta;
    Verifica();
  finally
    Qry.Free;
  end;
end;
*)

function TUnGrl_DmkDB.EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo;
  Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1,
  LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
  LaTempoI, LaTempoF, LaTempoT: TComponent; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; DataBase: TComponent; LBTabToRecriar: TListBox): Boolean;
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
begin
  Result := False;
  {$IfDef DMK_FMX}
  Qry := TFDQuery.Create(DataBase);
  {$Else}
  Qry := TZQuery.Create(DataBase);
  {$EndIf}
  try
    GrlTemp.InfoMemoStep('UnGrl_DmkDB.127');
    MeTabsNoNeed.Lines.Clear;

(*
    if Grl_Geral.MB_Pergunta('Ser� realizada uma verifica��o na base de dados: ' +
      sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
      'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
      'perante restaura��o de backup.') <> idYes
    then
      Exit;
*)
(*
    Grl_Geral.MB_Aviso('Ser� realizada uma verifica��o na base de dados: ' +
      sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
      'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
      'perante restaura��o de backup.');
*)

    GrlTemp.InfoMemoStep('UnGrl_DmkDB.980');
    Application.ProcessMessages;

    Result    := True;
    FPergunta := Pergunta;
    try
      // verifica se a vers�o atual � mais nova
      // se n�o � impede a verifica��o
      if DBName <> '' then
      begin
        AbreSQLQuery0(Qry, DataBase, [
          'SELECT name FROM sqlite_master ',
          'WHERE type=''table''',
          'AND name LIKE ''Controle''',
          'ORDER BY name;',
          '']);
         GrlTemp.InfoMemoStep('UnGrl_DmkDB.996');
         if Qry.RecordCount > 0 then
        begin
          AbreSQLQuery0(Qry, DataBase, [
            'PRAGMA table_info(controle);',
            '']);
           GrlTemp.InfoMemoStep('UnGrl_DmkDB.1002');
          if Qry.RecordCount > 0 then
          begin
           GrlTemp.InfoMemoStep('UnGrl_DmkDB.1005');
            AbreSQLQuery0(Qry, DataBase, [
              'SELECT Versao FROM controle',
              '']);
            GrlTemp.InfoMemoStep('UnGrl_DmkDB.1009');
            if CO_VERSAO < Qry.FieldByName('Versao').AsLargeInt then
            begin
             GrlTemp.InfoMemoStep('UnGrl_DmkDB.1012');
              Grl_Geral.MB_Aviso('Verifica��o de banco de dados cancelada! ' +
                sLineBreak +  ' A vers�o deste arquivo � inferior a cadastrada ' +
                'no banco de dados.');
              Exit;
            end;
          end;
        end;
      end else
      begin
         GrlTemp.InfoMemoStep('UnGrl_DmkDB.1022');
        Grl_Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
      end;
      //
      GrlTemp.InfoMemoStep('UnGrl_DmkDB.1026');
      VerificaEstrutura_SQLite(DBName, Memo, RecriaRegObrig, LaAviso1R,
        LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
        LaTempoF, LaTempoT, MeTabsNoNeed, PB, MeFldsNoNeed, DataBase,
        LBTabToRecriar);
    except
      raise;
    end;
    Grl_Geral.MB_Aviso('Verifica��o de banco de dados finalizada!');
  finally
    Qry.Free;
  end;
end;


{$EndIf}

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.VerificaEstrutura_SQLite(DBName: String; Memo: TMemo;
  RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B,
  LaAviso1G, LaAviso2G, LaTempoI, LaTempoF, LaTempoT: TComponent;
  MeTabsNoNeed: TMemo; PB: TProgressBar; MeFldsNoNeed: TMemo;
  DataBase: TComponent; LBTabToRecriar: TListBox): Boolean;
var
  i: Integer;
  TabNome, TabBase, Item: String;
  TempoI, TempoF, TempoT: TTime;
  DefTabela: TTabelas;
  Tem_Del, Tem_Sync: Boolean;
  {$IfDef DMK_FMX}
  QrTabs: TFDQuery;
  {$Else}
  QrTabs: TZQuery;
  {$EndIf}
  Texto: String;
begin
  if not ValidaTipoComp([LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G,
    LaAviso2G, LaTempoI, LaTempoF, LaTempoT, DataBase],
    'TUnGrl_DmkDB.VerificaEstrutura_SQLite')
  then
    Exit;
  //
  {$IfDef DMK_FMX}
  QrTabs := TFDQuery.Create(DataBase);
  {$Else}
  QrTabs := TZQuery.Create(DataBase);
  {$EndIf}
  try
    Tem_Del      := False;
    Tem_Sync     := False;
    TempoI       := Now();
    FViuControle := 0;
    Result       := True;

    Grl_Geral.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgEstruturaBD, [DBName]));
    Grl_Geral.Informa(LaTempoI, False, FormatDateTime('hh:nn:ss:zzz', TempoI));

    try
      FTabelas := TList<TTabelas>.Create;

          {$IfDef DMK_FMX}
      MyList.CriaListaTabelas(FTabelas);
          {$Else}
      MyList.CriaListaTabelas(DBName, FTabelas);
          {$EndIf}

      try
        if LaAviso1R <> nil then
          {$IfDef DMK_FMX}
          Texto := TListBoxItem(LaAviso1R).Text
          {$Else}
          Texto := TLabel(LaAviso1R).Caption
          {$EndIf}
        else
          Texto := '';

        Grl_Geral.Informa2(LaAviso1R, LaAviso2R, True, Texto + '(' +
          Grl_Geral.FF0(FTabelas.Count) + ' tabelas)');
        Grl_Geral.Informa2(LaAviso1B, LaAviso2B, True,
          'Verificando estrutura de tabelas j� criadas');

        AbreSQLQuery0(QrTabs, DataBase, [
          'SELECT name FROM sqlite_master ',
          'WHERE type=''table'' ',
          'ORDER BY name; ',
          '']);

        if PB <> nil then
        begin
          PB.Max := QrTabs.RecordCount;
          {$IfDef DMK_FMX}
          PB.Value := 0;
          {$Else}
          PB.Position := 0;
          {$EndIf}
        end;

        QrTabs.First;
        while not QrTabs.Eof do
        begin
          //ERRO! N�o verifica registros obrigatorios da primeira tabela!
          (*
          if PB <> nil then
            {$IfDef DMK_FMX}
            PB.Value := PB.Value + 1;
            {$Else}
            PB.Position := PB.Position + 1;
            {$EndIf}
          // Fazer Primeiro controle !!!
          if FViuControle = 0 then
          begin
            TabNome      := 'controle';
            FViuControle := 1;
          end else
          if FViuControle = 2 then
          begin
            QrTabs.Prior;
            FViuControle   := 3;
            TabNome        := QrTabs.Fields[0].AsString;
          end else
          *)
            TabNome := QrTabs.Fields[0].AsString;

          TabBase := '';

          for i := 0 to FTabelas.Count -1 do
          begin
            DefTabela := FTabelas[i];

            if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
            begin
              TabBase  := DefTabela.TabBase;
              Tem_Del  := DefTabela.Tem_Del;
              Tem_Sync := DefTabela.Tem_Sync;
              Break;
            end;
          end;

          if TabBase = '' then
            TabBase := TabNome;

          Grl_Geral.Informa2(LaAviso1B, LaAviso2B, True,
            'Verificando estrutura de tabelas j� criadas');
          Grl_Geral.Informa2(LaAviso1G, LaAviso2G, True,
            'Verificando tabela "' + TabNome + '"');

          if not Grl_DmkDB.TabelaAtiva(TabNome) then
          begin
            Item := Format(ivTabela_Nome, [TabNome]);

            {$IfDef DMK_FMX}
            if Lowercase(TabNome.Substring(1, 4)) <> 'lct0' then
            {$Else}
            if Lowercase(Copy(TabNome, 2, 4)) <> 'lct0' then
            {$EndIf}
              MeTabsNoNeed.Lines.Add(TabNome);
          end else
          begin
            if VerificaCampos_SQLite(DBName, TabNome, TabBase, Memo,
              RecriaRegObrig, Tem_Del, Tem_Sync, LaAviso1B, LaAviso2B,
              LaAviso1G, LaAviso2G, MeFldsNoNeed, DataBase, LBTabToRecriar) <> idOK then
            begin
              GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
              Exit;
            end;
          end;
          QrTabs.Next;
        end;

        Grl_Geral.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando tabelas a serem criadas');

        if PB <> nil then
        begin
          PB.Max      := FTabelas.Count;
          {$IfDef DMK_FMX}
          PB.Value := 0;
          {$Else}
          PB.Position := 0;
          {$EndIf}
        end;

        for i := 0 to FTabelas.Count -1 do
        begin
          Grl_Geral.Informa2(LaAviso1G, LaAviso2G, True,
            '"' + TabNome + '" verificando se existe');

          if PB <> nil then
            {$IfDef DMK_FMX}
            PB.Value := PB.Value + 1;
            {$Else}
            PB.Position := PB.Position + 1;
            {$EndIf}

          DefTabela := FTabelas[i];
          TabNome   := DefTabela.TabCria;
          TabBase   := DefTabela.TabBase;
          Tem_Del   := DefTabela.Tem_Del;
          Tem_Sync  := DefTabela.Tem_Sync;

          if TabBase = '' then
            TabBase := TabNome;

          if not TabelaExiste_SQLite1(TabNome, QrTabs) then
          begin
            Grl_Geral.Informa2(LaAviso1G, LaAviso2G, True,
              '"' + TabNome + '" criando tabela ');
            //
            CriaTabela_SQLite(DBName, TabNome, TabBase, Memo, actCreate,
              Tem_Del, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
              DataBase, Tem_Sync);
          end;

        end;
        Grl_Geral.Informa2(LaAviso1G, LaAviso2G, True, '');
        //
        QrTabs.Close;
      finally
        FTabelas.Free;
      end;
      Grl_Geral.Informa2(LaAviso1G, LaAviso2G, False, '...');
      Grl_Geral.Informa2(LaAviso1B, LaAviso2B, False, '...');
      Grl_Geral.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));

      TempoF := Now();

      Grl_Geral.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));

      TempoT := TempoF - TempoI;

      if TempoT < 0 then
        TempoT := TempoT + 1;

      Grl_Geral.Informa(LaTempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
    except
      Grl_Geral.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));
      raise;
    end;
  finally
    QrTabs.Free;
  end;
end;


{$EndIf}

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.TabelaExiste_SQLite1(Tabela: String;
  QrTabs: TDataSet): Boolean;
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
  Tab: String;
  P: Integer;
begin
  Result := False;
  //
  if not ValidaTipoComp([QrTabs], 'TUnGrl_DmkDB.TabelaExiste_SQLite1') then
    Exit;
  //
  try
    {$IfDef DMK_FMX}
    Qry := TFDQuery(QrTabs);
    {$Else}
    Qry := TZQuery(QrTabs);
    {$EndIf}
    //
    Qry.First;
    {$IfDef DMK_FMX}
    P := Tabela.IndexOf('.');
    {$Else}
    P := Pos('.', Tabela) - 1;
    {$EndIf}
    if P > 0 then
      {$IfDef DMK_FMX}
      Tab := Tabela.Substring(P + 1)
      {$Else}
      Tab := Copy(Tabela, P + 2)
      {$EndIf}
    else
      Tab := Tabela;
    while not Qry.Eof do
    begin
      if Uppercase(Tab) = Uppercase(Qry.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      Qry.Next;
    end;
  except
    raise;
  end;
end;
{$EndIf}

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.TabelaExiste_SQLite2(Tabela: String; Query: TDataSet;
  DataBase: TComponent): Boolean;
begin
  Grl_DmkDB.AbreSQLQuery0(Query, Database, [
  'SELECT name FROM sqlite_master ',
  'WHERE type=''table''',
  'AND name LIKE ''' + Tabela + '''',
  'ORDER BY name;',
  '']);
  Result := Query.RecordCount > 0;
end;

{$EndIf}


{$IfDef DB_SQLite}
function TUnGrl_DmkDB.CriaTabela_SQLite(DBName: String; TabelaNome,
  TabelaBase: String; Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean;
  LBIAviso1B, LBIAviso2B, LBIAviso1G, LBIAviso2G: TComponent;
  DataBase: TComponent; Tem_Sync: Boolean = False): Boolean;
var
  i, j, k, u, Primarios: Integer;
  Item, Opcoes, Campos: String;
  Indices, Conta: TStringList;
  Texto: TStringList;
  TemControle: TTemControle;
  //
  {$IfDef DMK_FMX}
  QrNTI: TFDQuery;
  {$Else}
  QrNTI: TZQuery;
  {$EndIf}
begin
  Result    := True;
  FLCampos  := TList<TCampos>.Create;
  FLIndices := TList<TIndices>.Create;
  Texto     := TStringList.Create;
  {$IfDef DMK_FMX}
  QrNTI := TFDQuery.Create(DataBase);
  {$Else}
  QrNTI := TZQuery.Create(DataBase);
  {$EndIf}
  try
    TemControle := [];
    MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
    //
    if (tctrlLok in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Lk';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlCad in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DataCad';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAlt in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DataAlt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlCad in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAlt in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlWeb in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'AlterWeb';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAti in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Ativo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if Tem_Del then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserDel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataDel';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MotvDel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o info
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
    if Tem_Sync then
    begin
      New(FRCampos);
      FRCampos.Field      := 'LastModifi';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
      FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
      FRCampos.Null       := '';           //2 => Exclus�o
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliIntSync';
      FRCampos.Tipo       := 'int';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o info
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
    MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
    try
      Texto.Add('CREATE TABLE ' + Lowercase(TabelaNome) + ' (');
      //
      Primarios := 0;

      for i:= 0 to FLCampos.Count - 1 do
      begin
        FRCampos := FLCampos[i];

        if FRCampos.Key = 'PRI' then
          Primarios := Primarios + 1;
      end;

      Item := Format(ivTabela_Nome, [TabelaNome]);

      for i:= 0 to FLCampos.Count - 1 do
      begin
        Opcoes   := myco_;
        FRCampos := FLCampos[i];
        Opcoes   := FRCampos.Tipo;

        {$IfDef DMK_FMX}
        if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) >= 0 then
        {$Else}
        if Pos(Uppercase('auto_increment'), Uppercase(FRCampos.Extra)) > 0 then
        {$EndIf}
          Opcoes := Opcoes + ' AUTOINCREMENT ';

        if FRCampos.Null <> 'YES' then
          Opcoes := Opcoes + ' NOT NULL ';

        if FRCampos.Default <> myco_ then
          AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);

        Opcoes := mycoEspacos2 + FRCampos.Field + mycoEspaco + Opcoes;

        if i < (FLCampos.Count-1) then
          Opcoes := Opcoes + mycoVirgula;

        Texto.Add(Opcoes);
      end;

      Indices            := TStringList.Create;
      Indices.Sorted     := True;
      Indices.Duplicates := (dupIgnore);

      try
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];

          Indices.Add(FRIndices.Key_name);
        end;
        u := Texto.Count-1;

        if Indices.Count > 0 then
          Texto[u] := Texto[u] + mycoVirgula;

        for k := 0 to Indices.Count -1 do
        begin
          Conta  := TStringList.Create;
          Campos := myco_;

          for i := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[i];

            if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
          end;

          for j := 0 to Grl_Geral.IMV(Conta[Conta.Count-1])-1 do
          begin
            for i := 0 to FLIndices.Count -1 do
            begin
              FRIndices := FLIndices.Items[i];
              if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
              begin
                if Campos <> myco_ then Campos := Campos + ',';
                Campos := Campos + FRIndices.Column_name;
              end;
            end;
          end;

          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Uppercase(Indices[k]) = Uppercase('PRIMARY') then
            begin
              if (Primarios > 0) then
                Texto.Add('  CONSTRAINT INDICE_PRIMARIO PRIMARY KEY (' + Campos + ')')
            end else
              Texto.Add('  CONSTRAINT '+ Indices[k] + ' UNIQUE ('+Campos+')');

            if k < (Indices.Count-1) then
            begin
             u        := Texto.Count-1;
             Texto[u] := Texto[u] + mycoVirgula;
            end;
          end;
          Conta.Free;
        end;
      finally
        Indices.Free;
      end;
      Texto.Add(')');
      MostraSQL(QrNTI, Memo, '==== CRIAR TABELA ====');

      if Acao = actCreate then
      begin
        GravaAviso(Item + sLineBreak + Texto.Text, Memo);
        ExecutaSQLQuery0(QrNTI, DataBase, [
          Texto.Text,
          '']);
        GravaAviso(Item + ivCriado, Memo);
      end;
    except
      GravaAviso(Item+ivMsgERROCriar, Memo);
    end;
  finally
    FLCampos.Free;
    FLIndices.Free;
    Texto.Free;
    QrNTI.Free;
  end;
end;
{$EndIf}


procedure TUnGrl_DmkDB.Define_Form_ID(Form_ID: String);
begin
  if Copy(Uppercase(Form_ID), 1, 12) <> 'FRX-PRINT-00' (*2'*) then
    VAR_FORM_ID := Form_ID;
end;



{$IfDef DB_SQLite}

function TUnGrl_DmkDB.CampoAtivo_SQLite(DBName: String; TabelaNome, TabelaBase,
  Campo: String; Memo: TMemo; QrNTV: TDataSet; DataBase: TComponent): Integer;

  function NuloStrToInt(Txt: String): Integer;
  begin
    if Txt = 'YES' then
      Result := 0
    else
      Result := 1;
  end;

var
  i: Integer;
  Item: String;
  {$IfDef DMK_FMX}
  QrIdx, QrPII: TFDQuery;
  {$Else}
  QrIdx, QrPII: TZQuery;
  {$EndIf}
begin
  //if Campo = 'CodInMob' then
    //Grl_Geral.MB_Info('CodInMob');
  Item := Format(ivCampo_Nome, [TabelaNome, Campo]) + mycoEspaco + ivMsgDiferenca;
  Result := 9;
  try
    for i := 0 to FLCampos.Count -1 do
    begin
      FRCampos := FLCampos.Items[i];
      if Uppercase(FRCampos.Field) = Uppercase(Campo) then
      begin
        Result := 0;
        if Uppercase(FRCampos.Tipo)  <>
           Uppercase(QrNTV.FieldByName('Type').AsString) then
        begin
          Result := 1;
          GravaAviso(Item + mycoTypeDataType, Memo);
        end;
        if NuloStrToInt(Uppercase(FRCampos.Null))  <>
           QrNTV.FieldByName('notnull').AsInteger then
        begin
          Result := 1;
          GravaAviso(Item+'"NULL"', Memo);
        end;
        if (Uppercase(FRCampos.Default) <>
           Uppercase(QrNTV.FieldByName('dflt_value').AsString)) then
        if (Uppercase(#39 + FRCampos.Default + #39) <>
           Uppercase(QrNTV.FieldByName('dflt_value').AsString)) then
        begin
          Result := 1;
          GravaAviso(Item + '"DEFAULT"' + sLineBreak +
          'Lista dmk: ' + FRCampos.Default + sLineBreak + 'Obtido do BD: ' +
          QrNTV.FieldByName('dflt_value').AsString, Memo);
        end;
        if (FRCampos.Key  <> '') and (
           QrNTV.FieldByName('pk').AsInteger = 0) then
        begin
          if (FRCampos.Key = 'UNI')
          or (FRCampos.Key = 'MUL') then
          begin
            // SQLite n�o informa
            // ver pelo �ndice:
            {$IfDef DMK_FMX}
            QrIdx := TFDQuery.Create(DataBase);
            QrPII := TFDQuery.Create(DataBase);
            {$Else}
            QrIdx := TZQuery.Create(DataBase);
            QrPII := TZQuery.Create(DataBase);
            {$EndIf}
            try

              AbreSQLQuery0(QrIdx, DataBase, [
                'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
                '']);
              QrIdx.First;
              while not QrIdx.Eof do
              begin
                AbreSQLQuery0(QrPII, DataBase, [
                  'PRAGMA INDEX_INFO(''' + QrIdx.FieldByName('name').AsString + ''')',
                  '']);
                while not QrPII.Eof do
                begin
                  if LowerCase(QrPII.FieldByName('name').AsString) = Lowercase(Campo) then
                  begin
                    Exit;
                  end;
                  //
                  QrPII.Next;
                end;
                //
                QrIdx.Next;
              end;
            finally
              if QrIdx <> nil then
                QrIdx.Free;
              if QrPII <> nil then
                QrPII.Free;
            end;
          end;
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Result = 9 then Result := 3;
            GravaAviso(Item+'"KEY"', Memo);
          end;
        end;
      end;
      if Result < 9 then Exit;
    end;
  except
    raise;
  end;
end;
{$EndIf}

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.CampoExiste_SQLite(Tabela, Campo: String; Memo: TMemo;
  QrNTV: TDataSet): Integer;
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
  Item: String;
begin
  Item   := Format(ivCampo_Nome, [Tabela, Campo]) + mycoEspaco + ivMsgNaoExiste;
  Result := 2;
  //
  if not ValidaTipoComp([QrNTV], 'TUnGrl_DmkDB.CampoExiste_SQLite') then
    Exit;
  //
  {$IfDef DMK_FMX}
  Qry := TFDQuery(QrNTV);
  {$Else}
  Qry := TZQuery(QrNTV);
  {$EndIf}
  try
    Qry.First;
    while not Qry.Eof do
    begin
      if Uppercase(Qry.FieldByName('name').AsString) =
         Uppercase(FRCampos.Field)
      then
        Result := 0;
      if Result < 2 then Exit;
      Qry.Next;
    end;
    if Result = 2 then
      GravaAviso(Item, Memo);
  except
    raise;
  end;
end;
{$EndIf}

function TUnGrl_DmkDB.ExecutaSQLQuery0(Query: TDataSet; DataBase: TComponent;
  SQL: array of String): Boolean;
var
  I: Integer;
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  Qry: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  DB: TZConnection;
  Qry: TZQuery;
  {$Else}
  DB: TmySQLDataBase;
  Qry: TmySQLQuery;
  {$EndIf}
  {$EndIf}
begin
  Result := False;
  //
  if not ValidaTipoComp([Query, DataBase], 'TUnGrl_DmkDB.ExecutaSQLQuery0') then
    Exit;
  //
  {$IfDef DMK_FMX}
  DB  := TFDConnection(DataBase);
  Qry := TFDQuery(Query);
  {$Else}
  {$IfDef DB_SQLite}
  DB  := TZConnection(DataBase);
  Qry := TZQuery(Query);
  {$Else}
  DB  := TmySQLDataBase(DataBase);
  Qry := TmySQLQuery(Query);
  {$EndIf}
  {$EndIf}
  try
    Qry.Close;
    {$IfDef DMK_FMX}
    Qry.Connection := DB;
    {$Else}
    {$IfDef DB_SQLite}
    Qry.Connection := DB;
    {$Else}
    Qry.DataBase := DB;
    {$EndIf}
    {$EndIf}

    if High(SQL) > -1 then
    begin
      Qry.SQL.Clear;

      for I := Low(SQL) to High(SQL) do
        Qry.SQL.Add(SQL[I]);
    end;

    if Qry.SQL.Text = '' then
    begin
      Grl_Geral.MB_Erro('Texto da SQL indefinido no "ExecutaSQLQuery0"!' +
        sLineBreak + 'Avise a DERMATEK!');
      //
      Result := False;
    end else
    begin
      Qry.ExecSQL;
      //
      Result := True;
    end;
  except
    on E: Exception do
    begin
      if pos('database is locked', E.Message) > 0 then
      begin
        Grl_Geral.MB_Erro('Tabela bloqueada!' + sLineBreak +
        'Feche o aplicativo, limpe da mem�ria e reinicie!' + sLineBreak +
          sLineBreak + sLinebreak + E.Message + sLineBreak + Qry.SQL.Text);
          //
        EncerraApplicacao();
        //
      end else
      begin
        Grl_Geral.MB_Erro('Erro ao tentar executar uma SQL no "ExecutaSQLQuery0"!' +
          sLinebreak + E.Message + sLineBreak + Qry.SQL.Text);
      end;
    end;
  end;
end;

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.ExcluiIndice_SQLite(DBName: String; Tabela, IdxNome,
  Aviso: String; Motivo: TMyMotivo; Memo: TMemo; DataBase: TComponent): Integer;
var
  Resp: Integer;
  Txt: String;
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
begin
  if True then
  begin
    case Motivo of
      mymotDifere: Txt := Format('O �ndice %s da Tabela %s difere do esperado e ' +
                            'deve ser excluido.', [IdxNome, Tabela]);
      mymotSemRef: Txt := Format('N�o h� refer�ncia ao �ndice %s da Tabela %s, ' +
                            'que deve ser excluido.', [IdxNome, Tabela]);
    end;
    if not FPergunta then
      Resp := mrNone
    else
      Resp := Grl_Geral.MB_Pergunta(Txt + sLineBreak +
              'Confirma a exclus�o do �ndice ?');
    //
    if Resp = idYes then
    begin
      try
        {$IfDef DMK_FMX}
        Qry := TFDQuery.Create(DataBase);
        {$Else}
        Qry := TZQuery.Create(DataBase);
        {$EndIf}
        try
          ExecutaSQLQuery0(Qry, DataBase, [
            'DROP INDEX IF EXISTS ' + IdxNome + '''',
            '']);
          MostraSQL(Qry, Memo, '+++EXCLUIR INDICE+++');
        finally
          Qry.Free;
        end;
        //
        GravaAviso(Aviso + ivMsgExcluido, Memo);
      except;
        GravaAviso(Aviso + ivMsgERROExcluir, Memo);
        raise;
      end;
    end else
    begin
      GravaAviso(Aviso + ivAbortExclUser, Memo);
      //
      if Resp = idCancel then
      begin
        Result := idAbort;
        Exit;
      end;
    end;
    Result := idOK;
    GravaAviso('O �ndice "' + Tabela + '.' + IdxNome + ' deveria ser exclu�do!', Memo);
    //
  end else
    Result := idOK;
end;
{$EndIf}


procedure TUnGrl_DmkDB.MostraMsgDB_SQLite();
begin
  Grl_Geral.MB_Aviso('Para utilizar um banco de dados do tipo SQLite' +
    sLineBreak + 'Voc� deve definir a vari�vel global "DB_SQLite" no projeto');
end;

procedure TUnGrl_DmkDB.MostraSQL(Query: TDataSet; Memo: TMemo; Titulo: String);
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  Qry: TZQuery;
  {$Else}
  Qry: TMySQLQuery;
  {$EndIf}
  {$EndIf}
  I: Integer;
begin
  if not ValidaTipoComp([Query], 'TUnGrl_DmkDB.MostraSQL') then
    Exit;
  //
  {$IfDef DMK_FMX}
  Qry := TFDQuery(Query);
  {$Else}
  {$IfDef DB_SQLite}
  Qry := TZQuery(Query);
  {$Else}
  Qry := TMySQLQuery(Query);
  {$EndIf}
  {$EndIf}
  //
  Memo.Lines.Add('===== ' + Titulo + ' =====');

  for I := 0 to Qry.SQL.Count -1 do
    Memo.Lines.Add(Qry.SQL[I]);
end;

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.LocalizaPriorNextIntQr(Tabela: TDataSet; Campo:
  TIntegerField; Atual: Integer): Integer;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;
{$EndIf}

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.RecriaIndice_SQLite(Tabela, IdxNome, Aviso: String;
  Memo: TMemo; DataBase: TComponent): Integer;
var
  I, J: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  Qry: TZQuery;
  {$EndIf}
  UNQ: String;
begin
  Result := idOK;
  if not FPergunta then
  begin
    Resp := idYes;
  end else
  begin
    Resp := Grl_Geral.MB_Pergunta('Confirma a alter��o do �ndice ' + IdxNome +
      ' da tabela ' + Tabela + '?)');
  end;
  if Result = idYes then
  begin
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      for J := 0 to Grl_Geral.IMV(Conta[Conta.Count-1])-1 do
      begin
        for I := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[I];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[J]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      {$IfDef DMK_FMX}
      Qry := TFDQuery.Create(DataBase);
      {$Else}
      Qry := TZQuery.Create(DataBase);
      {$EndIf}
      try
        ExecutaSQLQuery0(Qry, DataBase, [
        'DROP INDEX IF EXISTS ' + IdxNome + '''',
        '']);
        //
        {$IfDef DMK_FMX}
        if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
          or (Uppercase(IdxNome.Substring(1, 6)) = 'UNIQUE') then
        {$Else}
        if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
          or (Uppercase(Copy(IdxNome, 2, 6))  = 'UNIQUE') then
        {$EndIf}
          UNQ := ' UNIQUE '
        else
          UNQ := ' ';
        //
        ExecutaSQLQuery0(Qry, DataBase, [
          'CREATE' + UNQ + 'INDEX IF NOT EXISTS ' + LowerCase(IdxNome),
          'ON ' + Lowercase(Tabela) + ' (' + Campos +  ')',
          '']);
        MostraSQL(Qry, Memo, 'CRIAR INDICE');
      finally
        Qry.Free;
      end;
      //
      Result := idOK;
      GravaAviso(Aviso + ivAlterado, Memo);
    except;
      Conta.Free;
      GravaAviso(Aviso + ivMsgERROAlterar, Memo);
      raise;
    end;
  end else
  begin
    GravaAviso(Aviso + ivAbortAlteUser, Memo);
    //
    if Resp = idCancel then
    begin
      Result := idAbort;
      Exit;
    end;
  end;
end;

{$EndIf}


{$IfDef DB_SQLite}
function TUnGrl_DmkDB.CriaIndice_SQLite(Tabela, IdxNome, Aviso: String;
  Memo: TMemo; DataBase: TComponent): Integer;
var
  I, J, K: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
  {$IfDef DMK_FMX}
  QrIts: TFDQuery;
  {$Else}
  QrIts: TZQuery;
  {$EndIf}
  UNQ, IDX: String;
begin
  Result := idOK;
  //
  if not FPergunta then
    Resp := idYes
  else
    Resp := Grl_Geral.MB_Pergunta('Confirma a cria��o do �ndice ' + IdxNome +
      ' da tabela ' + Tabela + '?');
  //
  if Resp = idYes then
  begin
    Conta  := TStringList.Create;
    Campos := myco_;

    for I := 0 to FLIndices.Count -1 do
    begin
      FRIndices := FLIndices.Items[I];

      if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
        Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
    end;

    K := Grl_Geral.IMV(Conta[Conta.Count-1])-1;

    if K = -1 then
      GravaAviso('FRIndices.Seq_in_index inv�lido (0)!' +
        'ERRO na cria��o do �ndice ' + IdxNome + ' na tabela '+ Tabela, Memo);

    for J := 0 to K do
    begin
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];

        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
        begin
          if StrToInt(Conta[J]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
    end;
    {$IfDef DMK_FMX}
    QrIts := TFDQuery.Create(DataBase);
    {$Else}
    QrIts := TZQuery.Create(DataBase);
    {$EndIf}
    try
      try
        AbreSQLQuery0(QrIts, DataBase, [
          'SELECT COUNT(*) _ITENS_ FROM Controle ',
          'GROUP BY Codigo ',
          'ORDER BY _ITENS_ DESC ',
          '']);

        if (QrIts.RecordCount > 0) and (QrIts.FieldByName('_ITENS_').AsFloat > 1) then
        begin
          GravaAviso('O �ndice "' + IdxNome + '" n�o foi inclu�do na tabela "' +
            Tabela + '" pois j� seria criado com viola��o!', Memo);
        end else
        begin
          QrIts.Close;
          QrIts.SQL.Clear;

          {$IfDef DMK_FMX}
          if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
            or (Uppercase(IdxNome.Substring(1, 6)) = 'UNIQUE')
          {$Else}
          if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
            or (Uppercase(Copy(IdxNome, 2, 6)) = 'UNIQUE')
          {$EndIf}
          then
            UNQ := ' UNIQUE '
          else
            UNQ := ' ';

          if LowerCase(IdxNome) = 'primary' then
            IDX := 'idx_' + Tabela + '_primary'
          else
            IDX := IdxNome;
          //
          ExecutaSQLQuery0(QrIts, DataBase, [
            'CREATE' + UNQ + 'INDEX IF NOT EXISTS ' + IDX,
            'ON ' + Lowercase(Tabela) + ' (' + Campos +  ')',
            '']);

          Result := idOK;

          GravaAviso(Aviso + ivCriado, Memo);
        end;
      except;
        MostraSQL(QrIts, Memo,
          'ERRO na cria��o de �ndice na tabela ' + Tabela);
        Conta.Free;
        GravaAviso(Aviso + ivMsgERROCriar, Memo);
        raise;
      end;
    finally
      QrIts.Free;
    end;
  end else
  begin
    GravaAviso(Aviso + ivAbortInclUser, Memo);
    //
    if Resp = idCancel then
    begin
      Result := idAbort;
      Exit;
    end;
  end;
end;
{$EndIf}

function TUnGrl_DmkDB.ObtemVersaoAppDB(Query: TDataSet; DataBase: TComponent;
  DriverNome: TDriverName; Device: TDeviceType): Integer;
var
  SQL: String;
  Versao: Integer;
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  {$Else}
  {$IfDef DB_SQLite}
  DB: TZConnection;
  {$Else}
  DB: TmySQLDataBase;
  {$EndIf}
  {$EndIf}
begin
  //True  = Tabelas j� criadas
  //False = Tabelas n�o criadas
  Versao := 0;
  //
  if not ValidaTipoComp([Query, DataBase], 'TUnGrl_DmkDB.ObtemVersaoAppDB') then
  begin
    Result := Versao;
    Exit;
  end;
  //
  GrlTemp.InfoMemoStep('OK 2240');

  case DriverNome of
    istMySQL:
      begin
        Grl_Geral.MB_Aviso('O Driver "MySQL" n�o est� implementado para a fun��o:' +
          sLineBreak + 'TUnGrl_DmkDB.ObtemVersaoAppDB');
      end;
    istSQLite:
      begin
        GrlTemp.InfoMemoStep('OK 2252');
        {$IfDef DB_SQLite}
            SQL := 'CREATE TABLE IF NOT EXISTS controle (Codigo INT(11) PRIMARY KEY NOT NULL DEFAULT 0, Versao BIGINT(20) NOT NULL DEFAULT 0);';
          {$IfDef DMK_FMX}
            GrlTemp.InfoMemoStep('OK 2257');
            DB := TFDConnection(DataBase);
            DB.ExecSQL(SQL);
            GrlTemp.InfoMemoStep('OK 2263');
          {$Else}
            DB := TZConnection(DataBase);
            DB.ExecuteDirect(SQL);
          {$EndIf}

          AbreSQLQuery0(Query, DataBase, [
            'SELECT Versao ',
            'FROM controle ',
            '']);

          if Query.RecordCount > 0 then
          begin
            Versao := Query.FieldByName('Versao').AsInteger;
            GrlTemp.InfoMemoStep('OK 2273  Vers�o: ' + FMX_Geral.FF0(Versao));
          end else
          begin
            CarregaSQLInsUpd(Query, DataBase, stIns, 'controle', False,
              ['Versao'], ['Codigo'], [0], [1], False, dmksqlinsInsOnly, '', Device);
          end;
          Query.Close;
        {$Else}
          MostraMsgDB_SQLite;
        {$EndIf}
      end;
  end;
  Result := Versao;
end;

function TUnGrl_DmkDB.QueryTemRegistros(Query: TDataSet): Boolean;
begin
  Result := (Query.State <> dsInactive) and (Query.RecordCount >0);
end;

function TUnGrl_DmkDB.ReopenQuery(Query: TDataSet;
  DataBase: TComponent): Boolean;
begin
  Query.Close;
  Result := AbreQuery(Query, DataBase);
end;

function TUnGrl_DmkDB.ObtemCodigoInt(Tabela, Campo: String; Query: TDataSet;
  DataBase: TComponent): Integer;
var
  Atual: Integer;
begin
  if not ValidaTipoComp([Query, DataBase], 'TUnGrl_DmkDB.ObtemCodigoInt') then
  begin
    Result := 0;
    Exit;
  end;
  //
  AbreSQLQuery0(Query, DataBase, [
    'SELECT MAX(' + Campo + ') ' + Campo,
    'FROM ' + Tabela]);
  //
  if Query.RecordCount > 0 then
  begin
    if Query.FieldByName(Campo).AsVariant <> NULL then
    begin
      if Query.FieldByName(Campo).AsString = '' then
        Atual := 0
      else
        Atual := Query.FieldByName(Campo).AsInteger
    end else
      Atual := 0;
  end else
    Atual := 0;
  //
  Result := Atual + 1;
end;

function TUnGrl_DmkDB.ObtemCodigoInt_MulFld(Tabela, Campo: String;
  Query: TDataSet; DataBase: TComponent; MulFld: array of String;
  MulVal: array of Variant): Integer;
const
  sProcName = 'TUnGrl_DmkDB.ObtemCodigoInt_MulFld()';
var
  Atual, I, J: Integer;
  SQL_WHERE: String;
begin
  if not ValidaTipoComp([Query, DataBase], sProcName) then
  begin
    Result := 0;
    Exit;
  end;
  //
  SQL_WHERE := '';


  I := High(MulFld);
  J := High(MulVal);

  if I <> J then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + Grl_Geral.FF0(I + 1) + ' campos e ' +
      Grl_Geral.FF0(j+1) + ' valores para estes campos em "' + sProcName + '"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;


  //Campos  := '';
  //Valores := '';
  //j       := System.High(SQLCampos);

  for I := System.Low(MulFld) to System.High(MulFld) do
  begin
    if I = 0 then
      SQL_WHERE := SQL_WHERE + 'WHERE '
    else
      SQL_WHERE := SQL_WHERE + 'AND ';
    //
    SQL_WHERE := SQL_WHERE + MulFld[I] + '=' +
      Grl_Geral.VariavelToStringAD(MulVal[i]);
  end;
  AbreSQLQuery0(Query, DataBase, [
    'SELECT MAX(' + Campo + ') ' + Campo,
    'FROM ' + Tabela,
    SQL_WHERE,
    '']);
  //
  if Query.RecordCount > 0 then
  begin
    if Query.FieldByName(Campo).AsVariant <> NULL then
    begin
      if Query.FieldByName(Campo).AsString = '' then
        Atual := 0
      else
        Atual := Query.FieldByName(Campo).AsInteger
    end else
      Atual := 0;
  end else
    Atual := 0;
  //
  Result := Atual + 1;
end;

function TUnGrl_DmkDB.ObtemCodigoInt_Sinc(Tabela, Campo: String; Query: TDataSet;
  DataBase: TComponent; SQLTipo: TSQLType; Device: TDeviceType): Integer;
var
  MinCodigo_Str, MaxCodigo_Str: String;
  MaxCasas, MaxCodigo, MinCodigo, Dispositivo, Codigo: Integer;
begin
  Result := 0;

  case Device of
    stDesktop:
      Dispositivo := 0;
    stWeb:
      Dispositivo := 1;
    stMobile:
    begin
      (*
      Dispositivo := VAR_WEB_USR_DEVICE;

      if Dispositivo = 0 then
        Dispositivo := VAR_API_DEVICE;

      if Dispositivo = 0 then
      begin
        Grl_Geral.MB_Aviso('Dispositivo n�o registrado!');
        Exit;
      end;
      *)
      Dispositivo := 0;
      Grl_Geral.MB_Erro('Id do dispositivo n�o implementado na fun��o:' +
        sLineBreak + '"TUnGrl_DmkDB.ObtemCodigoInt_Sinc"');
    end;
    else
      Exit;
  end;

  if SQLTipo = stUpd then
  begin
    Exit;
  end;

  if Length(Grl_Geral.FF0(Dispositivo)) > CO_MAX_DEVICE_NUM then
  begin
    Grl_Geral.MB_Aviso('Limite de dispositivos atingido!' + sLineBreak +
      'Avise a Dermatek!');
    Exit;
  end;

  MaxCasas  := Length(Grl_Geral.FF0(High(Integer))) - CO_MAX_DEVICE_NUM;
  MaxCodigo := Grl_Geral.IMV('1' + Grl_Geral.FFN(0, MaxCasas)) - 1;
  MaxCodigo := Grl_Geral.IMV(Grl_Geral.FF0(Dispositivo) + Grl_Geral.FF0(MaxCodigo));
  MinCodigo := Grl_Geral.IMV(Grl_Geral.FF0(Dispositivo) + Grl_Geral.FFN(0, MaxCasas));

  MinCodigo_Str := Grl_Geral.FF0(MinCodigo);
  MaxCodigo_Str := Grl_Geral.FF0(MaxCodigo);

  AbreSQLQuery0(Query, DataBase, [
    'SELECT MAX(' + Campo + ') Campo ',
    'FROM ' + Tabela,
    'WHERE ' + Campo + ' >= ' + MinCodigo_Str,
    'AND ' + Campo + ' < ' + MaxCodigo_Str,
    '']);

  if Query.RecordCount > 0 then
  begin
    if Query.FieldByName('Campo').AsVariant <> Null then
    begin
      Codigo := Query.FieldByName('Campo').AsVariant + 1;

      if Codigo <= MaxCodigo then
      begin
        Result := Codigo;
      end else
      begin
        Grl_Geral.MB_Aviso('Limite de ID atingido para a tabela '+ Tabela +'!' +
          sLineBreak + 'Avise a Dermatek!');
        //
        Result := 0;
      end;
    end else
    begin
      Result := MinCodigo + 1;
    end;
  end else
  begin
    Result := MinCodigo + 1;
  end;
  Query.Close;
end;

function TUnGrl_DmkDB.ValidaDadosCampo(Query: TDataSet; DB: TComponent;
  Campo, Tabela: String; Valor: Integer; PemiteZero: Boolean = False): Boolean;
var
  Val: String;
begin
  if Valor <> 0 then
  begin
    Val := Grl_Geral.FF0(Valor);
    //
    AbreSQLQuery0(Query, DB, [
      'SELECT ' + Campo,
      'FROM ' + Tabela,
      'WHERE ' + Campo + '=' + Val]);
    //
    if Query.RecordCount > 0 then
      Result := True
    else
      Result := False;
    Query.Close;
  end else
  begin
    if PemiteZero then
      Result := True
    else
      Result := False;
  end;
end;

function TUnGrl_DmkDB.ObtemIndexDeTabela(Query: TDataSet; CampoCodigo: String;
  Valor: Integer): Integer;
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  Qry: TZQuery;
  {$Else}
  Qry: TmySQLQuery;
  {$EndIf}
  {$EndIf}
  Res: Integer;
begin
  Res := -1;
  //
  if not ValidaTipoComp([Query], 'TUnGrl_DmkDB.ObtemIndexDeTabela') then
  begin
    Result := Res;
    Exit;
  end;
  //
  {$IfDef DMK_FMX}
  Qry := TFDQuery(Query);
  {$Else}
  {$IfDef DB_SQLite}
  Qry := TZQuery(Query);
  {$Else}
  Qry := TmySQLQuery(Query);
  {$EndIf}
  {$EndIf}
  //
  if ((Qry.State <> dsInactive) and (Qry.RecordCount > 0) and
    (Qry.Locate(CampoCodigo, Valor, [])))
  then
    Res := Query.RecNo - 1;
  //
  Result := Res;
end;

procedure TUnGrl_DmkDB.RemoveUsuarios(DataBase: TComponent;
  DriverNome: TDriverName);
var
  {$IfDef DMK_FMX}
  Qry: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  Qry: TZQuery;
  {$Else}
  Qry: TmySQLQuery;
  {$EndIf}
  {$EndIf}
begin
  if not ValidaTipoComp([DataBase], 'TUnGrl_DmkDB.RemoveUsuarios') then
    Exit;
  //
  case DriverNome of
    istMySQL:
    begin
      Grl_Geral.MB_Aviso('N�o implementado!');
    end;
    istSQLite:
    begin
      {$IfDef DB_SQLite}
      {$IfDef DMK_FMX}
      Qry := TFDQuery.Create(DataBase);
      {$Else}
      Qry := TZQuery.Create(DataBase);
      {$EndIf}
      try
        ExecutaSQLQuery0(Qry, DataBase, [
          'DELETE FROM usuarios ',
          '']);
      finally
        Qry.Free;
      end;
    {$Else}
    MostraMsgDB_SQLite;
    {$EndIf}
    end;
  end;
end;

function TUnGrl_DmkDB.SQLInsUpd(QrUpd: TDataSet; DataBase: TComponent;
  Tipo: TSQLType; Tabela: String; Auto_increment: Boolean; SQLCampos,
  SQLIndex: array of String; ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; HowIns: TdmkSQLInsert; ComplUpd: String; Device: TDeviceType;
  Sincro: Boolean): Boolean;
begin
 Result := CarregaSQLInsUpd(QrUpd, DataBase, Tipo, Tabela, Auto_increment,
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, HowIns, ComplUpd,
  Device, Sincro);
end;

procedure TUnGrl_DmkDB.AtualizaSincroDB(DataBase: TComponent;
  Device: TDeviceType; Tabela, DataHora: String);
var
  {$IfDef DMK_FMX}
  QueryLoc, QueryUpd: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  QueryLoc, QueryUpd: TZQuery;
  {$Else}
  QueryLoc, QueryUpd: TmySQLQuery;
  {$EndIf}
  {$EndIf}
  Codigo, Var_UserId: Integer;
  SQLTipo: TSQLType;
  USER_ID_Str: String;
begin
  if not ValidaTipoComp([DataBase], 'TUnGrl_DmkDB.AtualizaSincroDB') then
    Exit;
  //
  {$IfDef DMK_FMX}
  QueryLoc := TFDQuery.Create(DataBase);
  QueryUpd := TFDQuery.Create(DataBase);
  {$Else}
  {$IfDef DB_SQLite}
  QueryLoc := TZQuery.Create(DataBase);
  QueryUpd := TZQuery.Create(DataBase);
  {$Else}
  QueryLoc := TmySQLQuery.Create(DataBase);
  QueryUpd := TmySQLQuery.Create(DataBase);
  {$EndIf}
  {$EndIf}
  try
    Var_UserId := VAR_WEB_USR_ID;

    if Var_UserId = 0 then
      Var_UserId := VAR_API_ID_USUARIO;

    USER_ID_Str := Grl_Geral.FF0(Var_UserId);
    //
    AbreSQLQuery0(QueryLoc, DataBase, [
      'SELECT Codigo ',
      'FROM sincrodb ',
      'WHERE Tabela="' + Tabela + '"',
      'AND Usuario=' + USER_ID_Str,
      '']);
    if QueryLoc.RecordCount > 0 then
    begin
      Codigo  := QueryLoc.FieldByName('Codigo').AsInteger;
      SQLTipo := stUpd;
    end else
    begin
      Codigo  := ObtemCodigoInt('sincrodb', 'Codigo', QueryUpd, DataBase);
      SQLTipo := stIns;
    end;
    CarregaSQLInsUpd(QueryUpd, DataBase, SQLTipo, 'sincrodb', False,
      ['Tabela', 'DtaSincroUTC'], ['Usuario', 'Codigo'],
      [Tabela, DataHora], [Var_UserId, Codigo],
      True, dmksqlinsInsOnly, '', Device);
  finally
    QueryLoc.Free;
    QueryUpd.Free;
  end;
end;

function TUnGrl_DmkDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DataBase: TComponent): Integer;
begin
  {$IfDef DMK_FMX}
  Result := ExcluiRegistroInt1_SQLite(Pergunta, Tabela, Campo, Inteiro1, DataBase);
  {$Else}
  {$IfDef DB_SQLite}
  Result := ExcluiRegistroInt1_SQLite(Pergunta, Tabela, Campo, Inteiro1, DataBase);
  {$Else}
  Result := USQLDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro1,
              TmySQLDatabase(DataBase));
  {$EndIf}
  {$EndIf}
end;

function TUnGrl_DmkDB.ExcluiRegistroInt1_Sinc(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DataBase: TComponent): Integer;
begin
  {$IfDef DMK_FMX}
  Result := ExcluiRegistroInt1_Sinc_SQLite(Pergunta, Tabela, Campo, Inteiro1, DataBase);
  {$Else}
  {$IfDef DB_SQLite}
  Result := ExcluiRegistroInt1_Sinc_SQLite(Pergunta, Tabela, Campo, Inteiro1, DataBase);
  {$Else}
  Result := ExcluiRegistroInt1_Sinc_MySQL(Pergunta, Tabela, Campo, Inteiro1, DataBase);
  {$EndIf}
  {$EndIf}
end;

function TUnGrl_DmkDB.ObtemDataSincro(DataBase: TComponent; Tabela: String): TDateTime;
const
  CO_Tempo = 30; //30 minutos antes do �ltimo sincro para evitar erros de hora
var
  {$IfDef DMK_FMX}
  Query: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  Query: TZQuery;
  {$Else}
  Query: TmySQLQuery;
  {$EndIf}
  {$EndIf}
  Var_UserId: Integer;
  USER_ID_Str: String;
begin
  Result := 0;
  //
  if not ValidaTipoComp([DataBase], 'TUnGrl_DmkDB.ObtemDataSincro') then
    Exit;
  //
  {$IfDef DMK_FMX}
  Query := TFDQuery.Create(DataBase);
  {$Else}
  {$IfDef DB_SQLite}
  Query := TZQuery.Create(DataBase);
  {$Else}
  Query := TmySQLQuery.Create(DataBase);
  {$EndIf}
  {$EndIf}
  try
    Var_UserId := VAR_WEB_USR_ID;

    if Var_UserId = 0 then
      Var_UserId := VAR_API_ID_USUARIO;

    USER_ID_Str := Grl_Geral.FF0(Var_UserId);
    //
    AbreSQLQuery0(Query, DataBase, [
      'SELECT DtaSincroUTC, Tabela ',
      'FROM sincrodb ',
      'WHERE Tabela="' + Tabela + '"',
      'AND Usuario=' + USER_ID_Str,
      '']);
    if Query.RecordCount > 0 then
    begin
      Result := Query.FieldByName('DtaSincroUTC').AsDateTime - CO_Tempo;
    end;
  finally
    Query.Free;
  end;
end;

{$IfDef DB_SQLite}
function TUnGrl_DmkDB.ExcluiRegistroInt1_SQLite(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DataBase: TComponent): Integer;
var
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  Qry: TFDQuery;
  {$Else}
  DB: TZConnection;
  Qry: TZQuery;
  {$EndIf}
  Inteiro1_Str: String;
begin
  if not ValidaTipoComp([DataBase], 'TUnGrl_DmkDB.ExcluiRegistroInt1_SQLite') then
  begin
    Result := idNo;
    Exit;
  end;

  {$IfDef DMK_FMX}
  DB  := TFDConnection(DataBase);
  Qry := TFDQuery.Create(DB);
  {$Else}
  DB  := TZConnection(DataBase);
  Qry := TZQuery.Create(DB);
  {$EndIf}
  try
    Qry.Connection := DB;
    //
    Result := idNo;
    //
    if Pergunta = '' then
      Result := idYes
    else
      Result := Grl_Geral.MB_Pergunta(Pergunta);
    //
    if Result = mrYes then
    begin
      Inteiro1_Str := Grl_Geral.FF0(Inteiro1);

      ExecutaSQLQuery0(Qry, DataBase,
        [DELETE_FROM + LowerCase(Tabela),
        'WHERE ' + Campo + '=' + Inteiro1_Str]);
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;
{$EndIf}


{$IfDef DB_SQLite}
function TUnGrl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite(Pergunta, Tabela,
  Campo: String; Inteiro1: Integer; DataBase: TComponent): Integer;
var
  LastModifi, Inteiro1_Str, LastAcao_Str: String;
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  Qry: TFDQuery;
  {$Else}
  DB: TZConnection;
  Qry: TZQuery;
  {$EndIf}
begin
  {$IfDef DMK_FMX}
  DB  := TFDConnection(DataBase);
  Qry := TFDQuery.Create(DB);
  {$Else}
  DB  := TZConnection(DataBase);
  Qry := TZQuery.Create(DB);
  {$EndIf}
  try
    Qry.Connection := DB;
    //
    Result := idNo;
    //
    if Pergunta = '' then
      Result := idYes
    else
      Result := Grl_Geral.MB_Pergunta(Pergunta);
    //
    if Result = idYes then
    begin
      LastModifi   := Grl_Geral.FDT(Grl_Geral.DateTime_MyTimeZoneToUTC(Now), 109);
      Inteiro1_Str := Grl_Geral.FF0(Inteiro1);
      LastAcao_Str := Grl_Geral.FF0(Integer(laDel));
      //
      ExecutaSQLQuery0(Qry, DataBase,
        ['UPDATE ' + LowerCase(Tabela) +
        ' SET LastAcao = ' + LastAcao_Str + ', ',
        ' LastModifi = "' + LastModifi + '"',
        ' WHERE '+ Campo +' = ' + Inteiro1_Str]);
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;
{$EndIf}

{$IfnDef DMK_FMX}
{$IfDef DB_SQLite}
{$Else}
function TUnGrl_DmkDB.ExcluiRegistroInt1_Sinc_MySQL(Pergunta, Tabela,
  Campo: String; Inteiro1: Integer; DataBase: TComponent): Integer;
var
  LastModifi, Inteiro1_Str, LastAcao_Str: String;
  DB: TmySQLDatabase;
  Qry: TmySQLQuery;
begin
  DB  := TmySQLDatabase(DataBase);
  Qry := TmySQLQuery.Create(DB);
  try
    Qry.Database := DB;
    //
    Result := idNo;
    //
    if Pergunta = '' then
      Result := idYes
    else
      Result := Grl_Geral.MB_Pergunta(Pergunta);
    //
    if Result = idYes then
    begin
      LastModifi   := Grl_Geral.FDT(Grl_Geral.DateTime_MyTimeZoneToUTC(Now), 109);
      Inteiro1_Str := Grl_Geral.FF0(Inteiro1);
      LastAcao_Str := Grl_Geral.FF0(Integer(laDel));
      //
      ExecutaSQLQuery0(Qry, DataBase,
        ['UPDATE ' + LowerCase(Tabela) +
        ' SET LastAcao = ' + LastAcao_Str + ', ',
        ' LastModifi = "' + LastModifi + '"',
        ' WHERE '+ Campo +' = ' + Inteiro1_Str]);
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;
{$EndIf}
{$EndIf}

function TUnGrl_DmkDB.ObtemListaDeColunasDeTabela(DataBase: TComponent;
  DriverNome: TDriverName; Tabela: String; TipoColuna: TTipoColuna;
  var Colunas: TStringList): Boolean;
var
  {$IfDef DMK_FMX}
  Query: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  Query: TZQuery;
  {$Else}
  Query: TmySQLQuery;
  {$EndIf}
  {$EndIf}
  Coluna: String;
  Chave: Integer;
begin
  Result := False;

  if not ValidaTipoComp([DataBase], 'TUnGrl_DmkDB.ObtemListaDeColunasDeTabela') then
    Exit;

  Colunas.Clear;

  case DriverNome of
    istMySQL:
    begin
      Grl_Geral.MB_Aviso('N�o implementado!');
     end;
    istSQLite:
    begin
      {$IfDef DB_SQLite}
      {$IfDef DMK_FMX}
      Query := TFDQuery.Create(DataBase);
      {$Else}
      Query := TZQuery.Create(DataBase);
      {$EndIf}
      try
        AbreSQLQuery0(Query, DataBase, [
          'PRAGMA table_info('+ Tabela +') ',
          '']);
        if Query.RecordCount > 0 then
        begin
          Query.First;

          while not Query.Eof do
          begin
            Chave  := Query.FieldByName('pk').AsInteger;
            Coluna := Query.FieldByName('name').AsString;

            case TipoColuna of
              stPrimarias:
              begin
                if Chave = 1 then
                  Colunas.Add(Coluna);
              end;
              stNaoPrimarias:
              begin
                if Chave <> 1 then
                  Colunas.Add(Coluna);
              end;
              stTodas:
              begin
                Colunas.Add(Coluna);
              end;
            end;
            Query.Next;
          end;
          Result := True;
        end;
      finally
        if Query <> nil then
          Query.Free;
      end;
      {$Else}
      MostraMsgDB_SQLite;
      {$EndIf}
    end;
  end;
end;

procedure TUnGrl_DmkDB.AdicionaFldNoNeed_SQLite(MeFldsNoNeed: TMemo; Tabela,
  Campo: String);
var
  Texto: String;
  I, N: Integer;
begin
  Texto := Tabela + '.' + Campo;
  N := -1;
  //
  for I := 0 to MeFldsNoNeed.Lines.Count - 1 do
  begin
    if MeFldsNoNeed.Lines[I] = Texto then
    begin
      N := I;
      Break;
    end;
  end;
  //
  if N = -1 then
    MeFldsNoNeed.Lines.Add(Texto);
end;

function TUnGrl_DmkDB.AsFloat(Query: TDataSet; Column: String): Double;
begin
  if VarType(Query.FieldByName(Column).Value) = varDouble then
    Result := Query.FieldByName(Column).AsFloat
  else
  if Query.FieldByName(Column).Value = '' then
    Result := 0
  else
    Result := Query.FieldByName(Column).AsFloat;
end;

procedure TUnGrl_DmkDB.AdicionaDefault_SQLite(var Opcoes: String; Tipo,
  Padrao: String);
begin
  {$IfDef DMK_FMX}
  if Lowercase(Tipo).IndexOf('int') >= 0 then
  {$Else}
  if Pos('int', Lowercase(Tipo)) > 0 then
  {$EndIf}
    Opcoes := Opcoes + ' DEFAULT ' + Padrao
  else
    Opcoes := Opcoes + ' DEFAULT ' + #39 + Padrao + #39;
end;

function TUnGrl_DmkDB.GeraCondicaoDeSQL(var TextoSQL: String;
  const Tabela: String; const SQLCampos, Compare: array of String;
  const ValCampos: array of Variant; const FirstCondition,
  ComplSQL: String): Boolean;
var
  i, j: Integer;
  Valor: String;
begin
  Result := False;
  TextoSQL := sLineBreak + FirstCondition + ' ';
  //
  i := High(SQLCampos);
  j := High(Compare);
  if i <> j then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' comparadores "GeraCondicaoDeSQL"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  j := High(ValCampos);
  if i <> j then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "GeraCondicaoDeSQL"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Grl_Geral.VariavelToStringAD(ValCampos[i]);
    if (i = 0) then
      TextoSQL := TextoSQL + SQLCampos[i] + Compare[i] + Valor + sLineBreak
    else
      TextoSQL := TextoSQL + 'AND ' + SQLCampos[i] + Compare[i] + Valor + sLineBreak;
  end;
  //
  if Trim(ComplSQL) <> '' then
  begin
    TextoSQL := TextoSQL + ComplSQL + sLineBreak;
  end;
  TextoSQL := TextoSQL + ';';
  Result := True;
end;

function TUnGrl_DmkDB.GetNxtCodigoInt(const Tabela, Campo: String; var
  SQLType: TSQLType; const Atual: Integer): Integer;
begin
  if Atual = 0 then
  begin
    Result  := ObtemCodigoInt(Tabela, Campo, VAR_LOCAL_DB_COMPO_DATASET,
      VAR_LOCAL_DB_COMPO_DATABASE);
    SQLType := stIns;
  end else begin
    Result  := Atual;
    SQLType := stUpd;
  end;
end;

function TUnGrl_DmkDB.GetNxtCodigoInt_MulFld(const Tabela, Campo: String;
  var SQLType: TSQLType; const Atual: Integer; MulFld: array of String;
  MulVal: array of Variant): Integer;
begin
  if Atual = 0 then
  begin
    Result  := ObtemCodigoInt_MulFld(Tabela, Campo, VAR_LOCAL_DB_COMPO_DATASET,
      VAR_LOCAL_DB_COMPO_DATABASE, MulFld, MulVal);
    SQLType := stIns;
  end else begin
    Result  := Atual;
    SQLType := stUpd;
  end;
end;

procedure TUnGrl_DmkDB.GravaAviso(Aviso: String; Memo: TMemo);
begin
  if Memo <> nil then
    Memo.Lines.Add(Aviso);
end;

function TUnGrl_DmkDB.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy:
      Result := 'C�pia';
    stDel:
      Result := 'Exclus�o';
    stIns:
      Result := 'Inclus�o';
    stLok:
      Result := 'Travado';
    stUpd:
      Result := 'Altera��o';
    stUnd:
      Result := 'Desiste';
    stPsq:
      Result := 'Pesquisa';
    else
      Result := '???????';
  end;
end;

procedure TUnGrl_DmkDB.EncerraApplicacao();
begin
  {$IF DEFINED(ANDROID)}
  SharedActivity.finish;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  Application.Terminate;
  {$ENDIF}
  //Halt(0);
end;

function TUnGrl_DmkDB.TabelaAtiva(Tabela: String): Boolean;
var
  I: Integer;
  DefTabela: TTabelas;
begin
  Result := False;
  try
    for I := 0 to FTabelas.Count -1 do
    begin
      DefTabela := FTabelas[I];
      if Uppercase(DefTabela.TabCria) = Uppercase(Tabela) then
      begin
        Result := True;
        Exit;
      end;
    end;
  except
    raise;
  end;
end;



























{$IfNDef DMK_FMX}
  {$IfDef DB_SQLite}

function TUnGrl_DmkDB.VerificaRegistrosObrigatoriosSQLite_Inclui(
  DataBase: TZConnection; TabelaNome, TabelaBase: String; Memo: TMemo; Avisa,
  Recria: Boolean; LaAviso1B, LaAviso2B, LaAviso1G,
  LaAviso2G: TLabel): TResultVerify;
var
  i, j, n, OK, z: Integer;
  ListaCampos, ListaValores: TStringList;
  //ArrCampos: array of String;
  //ArrValues: array of Variant;
  sCampos, sValues: String;
  Linha, Campo, Valor, Item, Virgula, LisSQL, ValSel, ValFld: String;
  Criar: Boolean;
begin
  Result := rvOK;
  try
    FListaSQL := TStringList.Create;
    try
      MyList.CriaListaSQL(TabelaNome, FListaSQL);
      MyList.CompletaListaSQL(TabelaNome, FListaSQL);
      //2015-03-16
 {
       if TabelaNome <> TabelaBase then
        MyList.CompletaListaSQL(TabelaNome, FListaSQL);
      }
//      if TabelaNome = 'cfop2003' then
        //Geral.MB_Info(TabelaNome);
      if TabelaNome <> TabelaBase then
        MyList.CompletaListaSQL(TabelaBase, FListaSQL);
      // FIM 2015-03-16
      if FListaSQL.Count > 1 then
      begin
        ListaCampos := TStringList.Create;
        try
          j := 0;
          Linha :=  FListaSQL[0];
          while j < (Length(Linha)) do
          begin
            OK := 0;
            Campo := '';
            while OK < 1 do
            begin
              j := j + 1;
              if j> Length(Linha) then Break;
              if Linha[j] = CO_APOSTROFO then OK := OK - 1
              //else if Linha[j] = ',' then OK := OK + 1
              else if Linha[j] = Char(124) then // separador = "|"
                OK := OK + 1
              else
                Campo := Campo + Linha[j];
            end;
            ListaCampos.Add(Campo);
          end;
          //
          ListaValores := TStringList.Create;
          try
            // 2012-02-02
            ValSel := '';
            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) then // separador = "|"
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              ValSel := ValSel + ',' + ListaValores[0];
            end;
            ValSel := Copy(ValSel, 2);
            DModGZ.QrNTV.Close;
            DModGZ.QrNTV.Connection := Database;
            DModGZ.QrNTV.SQL.Clear;
            DModGZ.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
            DModGZ.QrNTV.SQL.Add('WHERE ' + ListaCampos[0] + ' IN (' + ValSel + ')');
            try
              AbreQuery(DModGZ.QrNTV, Database);
            except
              GravaAviso(Item + 'n�o � poss�vel verificar se existe.', Memo);
              raise;
            end;

            // Fim 2012-02-02

            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) then // separador = "|"
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              Campo := ListaCampos[0];
              Valor := ListaValores[0];
              MyObjects.Informa2(LaAviso1G, LaAviso2G, True,
                'Verificando tabela "' + TabelaNome +
                '" Verificando registro obrigat�rio "' + Campo + '" > ' + Valor);
              Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);

              // 2012-02-02
              {
              DModGZ.QrNTV.Close;
              DModGZ.QrNTV.Database := Database;
              DModGZ.QrNTV.SQL.Clear;
              DModGZ.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
              DModGZ.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
              try
                UMyMod.AbreQuery(DModGZ.QrNTV, 'TMyDBCheck.VerificaRegistrosObrigatorios_Inclui()');
              except
                GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo);
                raise;
              end;
              //Criar := False;
              /
              if GOTOy.Registros(DModGZ.QrNTV) <> 0 then
              }
              if Valor[1] = '"' then
                ValFld := Copy(Valor, 2, Length(Valor) - 2)
              else
                ValFld := Valor;
              if ValFld = '0000-00-00' then
                ValFld := '0';
              if DModGZ.QrNTV.Locate(Campo, Trim(ValFld), [loCaseInsensitive]) then
              // Fim 2012-02-02
              begin
                if (Recria and
                // Evitar destruir dados pois tabela tem
                // registro obrigatorio modificado p�s inclu�o
                (Uppercase(TabelaNome) <> Uppercase('controle')) and
                (Uppercase(TabelaNome) <> Uppercase('master')) and
                (Uppercase(TabelaNome) <> Uppercase('entidades')) and
                (Uppercase(TabelaNome) <> Uppercase('senhas')))
                //or ( (Uppercase(TabelaNome) = Uppercase('entidades')) and (Valor = '0'))
                then
                begin
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //Grl_Geral.MB_Info(''Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrUpd.Close;
                  Dmod.QrUpd.Connection := Database;
                  Dmod.QrUpd.SQL.Clear;
                  Dmod.QrUpd.SQL.Add(DELETE_FROM + Lowercase(TabelaNome));
                  Dmod.QrUpd.SQL.Add('WHERE '+Campo+' ='+Valor);
                  Dmod.QrUpd.ExecSQL;
                  Criar := True;
                  GravaAviso(Item+'foi exclu�do para ser recriado.', Memo);
                end else
                  Criar := False;
              end else
                Criar := True;
              if Criar then
              begin
                if Avisa and (DModGZ.QrNTV.RecordCount = 0) then
                  GravaAviso(Item+'n�o existe.', Memo);
                try
{
                  Dmod.QrUpd.Close;
                  Dmod.QrUpd.Connection := Database;
                  Dmod.QrUpd.SQL.Clear;
                  Dmod.QrUpd.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrUpd.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
}
                  Dmod.QrUpd.Close;
                  Dmod.QrUpd.Connection := Database;
                  Dmod.QrUpd.SQL.Clear;
                  Dmod.QrUpd.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' (');//' SET ');
                  sCampos := '';
                  sValues := '';
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then
                      Virgula := ','
                    else
                      Virgula := '';
                    //
                    sCampos := sCampos + ListaCampos[n] + Virgula;
                    sValues := sValues + ListaValores[n] + Virgula;
                  end;
                  Dmod.QrUpd.SQL.Add(sCampos + ') VALUES (' + sValues + ')');
{
    SetLength(ArrCampos, ListaCampos.Count);
    SetLength(ArrValues, ListaValores.Count);
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    ArrCampos[n] := ListaCampos[n];
                    ArrValues[n] := ListaValores[n];
                  end;
    SQLInsUpd(Dmod.QrUpd, Database, stIns, LowerCase(TabelaNome), False,
      ArrCampos, [], ArrValues, [], False, dmksqlinsInsOnly, '',
      TDeviceType.stDesktop, False);
}                  //
                  LeMeuSQL_Fixo_z(Dmod.QrUpd, '', Memo, False, False);
                  Dmod.QrUpd.ExecSQL;
                  if avisa then GravaAviso(Item+'inclu�do.', Memo);
                except
                  LisSQL := 'Lista de valores:' + sLineBreak;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + sLineBreak;
                  GravaAviso(LisSQL + Dmod.QrUpd.SQL.Text + sLineBreak + Item+ivMsgERROIncluir, Memo);
                  //raise;
                end;
              end;
            end;
          finally
            ListaValores.Free;
          end;
        finally
          ListaCampos.Free;
        end;
      end;
    finally
      FListaSQL.Free;
      MyObjects.Informa2(LaAviso1B, LaAviso2B, True, '');
      MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '');
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

{$EndIf}
{$EndIf}

























(*
{$IfNDef DMK_FMX}

procedure TUnGrl_DmkDB.ObtemCodigoECodUsu(const EdCodigo, EdCodUsu: TdmkEdit;
  var SQLType: TSQLType; var Codigo, CodUsu: Integer; const NoTabela,
  NoFldCodigo, NoFldCodUsu: String);
begin
  if EdCodigo <> nil then
    Codigo := EdCodigo.ValueVariant
  else
    Codigo := 0;
  //
  if EdCodUsu <> nil then
    CodUsu := EdCodUsu.ValueVariant
  else
    CodUsu := 0;
  //
  if SQLType = stIns then
  begin
    if (Codigo = 0) and (CodUsu <> 0) then
      Codigo := CodUsu
    else
    begin
      if Codigo = 0 then
        Codigo := GetNxtCodigoInt(NoTabela, NoFldCodigo, SQLType, Codigo);
      if CodUsu = 0 then
        CodUsu := GetNxtCodigoInt(NoTabela, NoFldCodUsu, SQLType, CodUsu);
    end;
  end;
end;

function TUnGrl_DmkDB.ConfigPanelInsUpd(Acao: TSQLType; Form: TForm;
  Panel: TWinControl; Query: TDataSet; WinCtrlsToHide,
  WinCtrlsToShow: array of TWinControl; CompoToFocus: TWinControl;
  CompoTipo: TControl; Tabela: String): Boolean;
  //
{
cid-name-type---------------------notnull-dflt_value-----------pk
seq-nome-tipo---------------------naonulo-valordefault---------primarykey
0  "Codigo"	          "INT(11)"      	"1"  	"0"	               "1"
1  "Versao"	          "BIGINT(20)"	  "1"	  "0"	               "0"
2  "DeviceHost"	      "varchar(255)"	"0"		null               "0"
3  "URLBaseAPI"	      "varchar(255)"	"0"		null               "0"
4  "DeviceAssembler"	"int(11)"	      "1"	  "0"	               "0"
5  "Lk"	              "int(11)"	      "0"	  "0"	               "0"
6  "DataCad"	        "date"        	"0"		null               "0"
7  "DataAlt"        	"date"        	"0"		null               "0"
8  "UserCad"        	"int(11)"	      "0"	  "0"	               "0"
9  "UserAlt"        	"int(11)"     	"0"	  "0"	               "0"
10 "AlterWeb"       	"tinyint(1)"	  "1"	  "1"	               "0"
11 "Ativo"	          "tinyint(1)"   	"1"	  "1"	               "0"
12 "DeviceMACAdress"	"varchar(255)"	"1"	  "'??-??-??-??-??-??'"	"0"
13 "DeviceMACAddress"	"varchar(255)"	"1"	  "'??-??-??-??-??-??'"	"0"  //////////////////////////////////////////////////////////////////////////////
}
  function ValorPadrao(Qry: TDataset; Campo: String): Variant;
  var
    Tipo, DefVal: String;
    e, k: Integer;
    d: Double;
    DefVar: Variant;
  begin
    Result := Null;
    //if Qry.Locate('Field', Campo, []) then
    if Qry.Locate('name', Campo, []) then
    begin
      //try
        // SQLite
        DefVar := Qry.FieldByName('dflt_value').AsVariant;
        //if VarType(Qry.FieldByName('dflt_value').AsVariant) = varNull then
        if VarType(DefVar) = varNull then
          DefVal := ''
        else
          // SQLite
          // DefVal := Qry.FieldByName('Default').AsVariant;
          DefVal := DefVar;
      //except
        //DefVal := Trim(Qry.FieldByName('Default').AsWideString);
      //end;
      if String(DefVal) <> '' then
      begin
        Tipo := Uppercase(Qry.FieldByName('Type').AsString);
        k := pos('(', Tipo);
        if k > 0 then
          Tipo := Copy(Tipo, 1, k-1);
        if (Tipo = 'TINYINT') or (Tipo = 'SMALLINT') or (Tipo = 'MEDIUMINT')
        or (Tipo = 'INT') or (Tipo = 'INTEGER') or (Tipo = 'BIGINT') then
          Result := DefVar//Qry.FieldByName('Default').AsVariant
        else
        if (Tipo = 'FLOAT') or (Tipo = 'DOUBLE') or (Tipo = 'DOUBLEPRECISION')
        or (Tipo = 'REAL') or (Tipo = 'DECIMAL') or (Tipo = 'NUMERIC') then
        begin
          //Val(Qry.FieldByName('Default').AsVariant, d, e);
          Val(DefVar, d, e);
          Result := d;
        end else
        if (Tipo = 'CHAR') or (Tipo = 'VARCHAR') or (Tipo = 'TINYBLOB')
        or (Tipo = 'BLOB') or (Tipo = 'MEDIUMBLOB') or (Tipo = 'LONGBLOB')
        or (Tipo = 'TINYTEXT') or (Tipo = 'TEXT') or (Tipo = 'MEDIUMTEXT')
        or (Tipo = 'LONGTEXT') or (Tipo = 'ENUM') or (Tipo = 'SET')
        then
          //Result := Qry.FieldByName('Default').AsVariant
          Result := DefVar
        else
        if (Tipo = 'DATE') or (Tipo = 'DATETIME') or (Tipo = 'TIMESTAMP')
        or (Tipo = 'TIME') or (Tipo = 'YEAR') then
          // Precisa modificar??
          //Result := Qry.FieldByName('Default').AsString
          Result := DefVar
        else
          Grl_Geral.MB_Aviso('O campo do tipo "' + Tipo +
          '" ainda n�o implementado na function "ValorPadrao". ' +
          '   AVISE A DERMATEK!   Isto n�o � uma notofica��o de erro!');
      end;
    end;
  end;
  procedure Mensagem(Campo: String; Objeto: TObject);
  begin
    Grl_Geral.MB_Erro('N�o foi poss�vel definir o valor ' +
    'do campo "' + Campo + '" no componente "' +
    TComponent(Objeto).Name + '"');
  end;
var
  i, j, k: Integer;
  c, Objeto: TComponent;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PropInfo: PPropInfo;
  Campo, s: String;
  Valor: Variant;
  IsOk: Boolean;
  Qry: TZQuery;
  MySou: TDataSource;
  MySet: TDataSet;
  //MyQry: TmySQLQuery;
  MyFld: String;
  Data: TDateTime;
begin
  //IncluiRegistro;
  Screen.Cursor := crHourGlass;
  if Acao = stIns then
  begin
    Qry:= TZQuery.Create(Query.Owner);
    Qry.Connection := TZQuery(Query).Connection;
    if Qry.Connection = nil then
      Qry.Connection := VAR_GOTOzSQLDBNAME; //Dmod.MyDB;
// SQLite
    //Qry.SQL.Add('SHOW FIELDS FROM ' + LowerCase(Tabela));
    Qry.SQL.Add('PRAGMA table_info(' + LowerCase(Tabela) + ')');
//

    Grl_DmkDB.AbreQuery(Qry, Qry.Connection);
  end else Qry := nil;
  for i := 0 to Form.ComponentCount - 1 do
  begin
    IsOk := False;
    Objeto := Form.Components[i];
    if  (Objeto.Name = 'SbNovo')
    or  (Objeto.Name = 'SbNumero')
    or  (Objeto.Name = 'SbNome')
    or  (Objeto.Name = 'SbQuery')
    then begin
      if (Objeto is TBitBtn) then
        TBitBtn(Objeto).Enabled := False
      else
      if (Objeto is TSpeedButton) then
        TSpeedButton(Objeto).Enabled := False;
    end
    else if (Objeto is TdmkValUsu) then
    begin
      if TdmkValUsu(Objeto).Panel <> nil then
      begin
        if TdmkValUsu(Objeto).Panel.Name = Panel.Name then
          IsOK := True
        else
          IsOK := False;
      end else Grl_Geral.MB_Erro('TdmkValUsu sem panel cadastrado!');
    end else begin
      IsOK := False;
      c := TComponent(Form.Components[i]).GetParentComponent;
      while c <> nil do
      begin
        if TComponent(c).Name = Panel.Name then
        begin
          IsOk := True;
          Break;
        end
        else c := TComponent(c).GetParentComponent;
      end;
    end;
    if IsOk then
    begin
      Objeto := Form.Components[i];
      //
      if Acao = stUpd then
      begin
        PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
        PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
        PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
        PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
        if PI_OldValor <> nil then
        begin
          if (PI_DataField <> nil) and (PI_DataSource <> nil) then
          begin
            //
            //PropVal := GetPropValue(Objeto, 'DataSource');
            //MySet   := TDataSource(PropVal);
            //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
            MySou := TDataSource(TDBEdit(Objeto).DataSource);
            MySet := TDataSource(MySou).DataSet;
            //MyQry := TZQuery(TDataSource(MySet));
            MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
            SetPropValue(Objeto, 'OldValor',
              TDataSet(MySet).FieldByName(MyFld).AsVariant);

          end else if PI_QryCampo <> nil then
          begin
            Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
            if Campo <> '' then
            begin
              PropInfo := GetPropInfo(Objeto, 'OldValor');
              if PropInfo <> nil then
              try
                if (Objeto is TdmkRadioGroup)
                and (TZQuery(Query).FieldByName(Campo).AsVariant = Null) then
                // nada
                else
                SetPropValue(Objeto, 'OldValor',
                  TZQuery(Query).FieldByName(Campo).AsVariant);
              except
                if TZQuery(Query).FieldByName(Campo).AsVariant <> Null then
                  Grl_Geral.MB_Erro('N�o foi poss�vel definir o "OldValor" = "'
                  + Grl_Geral.VariantTostring(
                  TZQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                  Campo + '" no objeto "' + TComponent(Objeto).Name + '"');
              end;
            end;
          end;
        end;
      end;
      //fim OldValor
      //
      PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
      if PropInfo <> nil then
      begin
        Campo := GetStrProp(TComponent(Objeto), PropInfo);
        if Campo <> '' then
        begin
          if Acao = stIns then
            Valor := ValorPadrao(Qry, Campo)
          else
            try
              Valor := TZQuery(Query).FieldByName(Campo).AsVariant;
            except
              Grl_Geral.MB_Erro('N�o foi poss�vel definir o valor "' +
              Grl_Geral.VariavelToStringAD(Valor) + '" para o objeto "' +
              TComponent(Objeto).Name + '"');
              //
            end;
          try
            if (Objeto is TdmkEdit) then
            begin
              if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                Valor := Grl_Geral.MesEAnoDoMez(Valor)
              else
              if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                Valor := Grl_Geral.AnoEMesDoMez(Valor);
              // 2012-06-01 0000-00-00 00:00:00
              if (
                 (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
              or (VarType(Valor) = varUString)
{$ENDIF}
              )
              and (TdmkEdit(Objeto).FormatType = dmktfTime)
              and (Length(Valor) = 19) then
              begin
                Valor := Copy(Valor, 12);
              end;
              // FIM 2012-06-01
              TdmkEdit(Objeto).ValueVariant := Valor;
            end
            //
            else if (Objeto is TdmkDBLookupCombobox) then
              TdmkDBLookupCombobox(Objeto).KeyValue := Valor
            //
            else if (Objeto is TdmkEditCB) then
              TdmkEditCB(Objeto).ValueVariant := Valor
            //
            else if (Objeto is TdmkEditDateTimePicker) then
            begin
              if Valor <> Null then
              begin
                if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
                or (VarType(Valor) = varUString)
{$ENDIF}
                then
                begin
                  // 2012-06-01 > 0000-00-00 00:00:00
                  if Length(Valor) = 19 then
                    Valor := Copy(Valor, 1, 10);
                  // Fim 2012-06-01
                  Data := Grl_Geral.ValidaDataSimples(Valor, False);
                  if (Data < TdmkEditDateTimePicker(Objeto).MinDate)
                  and (Acao = stIns) then
                    TdmkEditDateTimePicker(Objeto).Date := TdmkEditDateTimePicker(Objeto).MinDate
                  else
                    TdmkEditDateTimePicker(Objeto).Date := Data;
                end else
                  TdmkEditDateTimePicker(Objeto).Date := Valor;
              end;
            end
            //
            else if (Objeto is TdmkMemo) then
            begin
              if Valor = Null then Valor := '';
              TdmkMemo(Objeto).Text := Valor;
            end
            //
            else if (Objeto is TdmkRadioGroup) then
            begin
              // -1 complica inclus�o !!
              //if (Valor = Null) or (Valor = '') then Valor := -1;
              if (Valor = Null) then Valor := -1;
              if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
              or (VarType(Valor) = varUString)
{$ENDIF}
              then
              begin
                if (Valor = '') then
                  Valor := -1
              end;
              TdmkRadioGroup(Objeto).ItemIndex := Valor;
            end
            //
            else if (Objeto is TdmkCheckBox) then
            begin
              if Valor = Null then Valor := False;
              if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
              or (VarType(Valor) = varUString)
{$ENDIF}
              then
              begin
                s := Uppercase(Valor);
                if (s = 'V') or (s = 'S') or (s = '1') then
                  TdmkCheckBox(Objeto).Checked := True
                else
                if (s = 'F') or (s = 'N') or (s = '') or (s = '0') then
                  TdmkCheckBox(Objeto).Checked := False
                else
                  Grl_Geral.MB_Erro('Valor indefinido para "TdmkCheckBox"!');
              end else
                TdmkCheckBox(Objeto).Checked := Grl_Geral.IntToBool(Valor);
            end
            //
            else if (Objeto is TdmkCheckGroup) then
            begin
              if Valor = Null then Valor := False;
              TdmkCheckGroup(Objeto).Value := Valor;
            end
            //
            else if (Objeto is TdmkPopOutFntCBox) then
            begin
              if TdmkPopOutFntCBox(Objeto).Items.Count = 0 then
                TdmkPopOutFntCBox(Objeto).Items.Assign(Screen.Fonts);
              TdmkPopOutFntCBox(Objeto).FonteNome := Valor;
            end
            //
            else if (Objeto is TdmkRichEdit) then
            begin
              if Valor = Null then
                Valor := '';
              MyObjects.DefineTextoRichEdit(TdmkRichEdit(Objeto), Valor)
            end
            //
            else if (Objeto is TdmkValUsu) then
              TdmkValUsu(Objeto).ValueVariant := Valor
            //

            //

            else if (Objeto is TdmkDBEdit) then begin end //nada
            //
            else Mensagem(Campo, Objeto);
          except
            Mensagem(Campo, Objeto);
          end;
        end;
      end;
    end;
  end;
  Panel.Visible := True;
  //
  i := Low(WinCtrlsToHide);
  j := High(WinCtrlsToHide);
  for k := i to j do WinCtrlsToHide[i].Visible := False;
  //
  i := Low(WinCtrlsToShow);
  j := High(WinCtrlsToShow);
  for k := i to j do WinCtrlsToShow[i].Visible := True;
  //
  if CompoToFocus <> nil then
  try
    if Form.Visible then
      TWinControl(CompoToFocus).SetFocus;
  except
    ;
  end;
  if CompoTipo is TdmkLabel then
    TdmkLabel(CompoTipo).SQLType := Acao
  else
  if CompoTipo is TdmkImage then
    TdmkImage(CompoTipo).SQLType := Acao
  else
    Grl_Geral.MB_Erro(
    'Tipo de componente n�o implementado na fun��o "ConfigPanelInsUpd"');

  Result := True;
  if (Acao = stIns) and (Qry <> nil) then
    Qry.Free;
  Screen.Cursor := crDefault;
end;
{$EndIf}


procedure TUnGrl_DmkDB.UpdUnlockZ(Registro: Integer; Database: TZConnection;
  Table, Field: String);
begin
{ ver se precisa no futuro!
  QvUpdZ.Close;
  QvUpdZ.Database := Database;
  QvUpdZ.SQL.Clear;
  QvUpdZ.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdZ.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdZ.Params[0].AsInteger := Registro;
  QvUpdZ.ExecSQL;
}
end;

procedure TUnGrl_DmkDB.DesisteRegistroTbZ(Form: TForm; Tabela: TZTable;
  NomeTabela, NomeCampoCodigo: String; ImgTipo: TdmkImage;
  CodigoALiberar: Integer);
begin
  Screen.Cursor := crHourGlass;
  OcultaEdicaoTbZ(Form);
  Tabela.Cancel;
  if ImgTipo.SQLType = stIns then
    PoeEmLivreZ(Dmod.MyDB, 'livres', NomeTabela, CodigoALiberar)
  else if ImgTipo.SQLType = stUpd then
    UpdUnlockZ(CodigoALiberar, Dmod.MyDB, NomeTabela, NomeCampoCodigo);
  //
  ImgTipo.SQLType := stLok;
  Screen.Cursor := crDefault;
end;

function TUnGrl_DmkDB.ObtemCamposDeTabelaIdenticaZ(DataBase: TZConnection;
  Tabela, Prefix: String): String;
var
  J: Integer;
  Qry: TZQuery;
begin
  Qry := TZQuery.Create(Dmod);
  try
    Qry.Connection := Database;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT * FROM '+LowerCase(tabela));
    Qry.SQL.Add('LIMIT 1');
    AbreQuery(Qry, Database);
    //
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
    //////////////////////////////////////////////////////////////////////////////
    // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
    Result := ' ' + Prefix + Qry.Fields[0].FieldName + ' ' + sLineBreak;
    for J := 1 to Qry.Fields.Count-1 do
      Result := Result + ', ' + Prefix+Qry.Fields[J].FieldName + ' ' + sLineBreak;
    //////////////////////////////////////////////////////////////////////////////
    // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    //
  finally
    Qry.Free;
  end;
end;

function TUnGrl_DmkDB.ArrayDeTabelaIdentica_CamposZ(DataBase: TZConnection;
  Tabela: String): TUMyArray_Str;
var
  j: Integer;
  Qry: TZQuery;
begin
  Qry := TZQuery.Create(Dmod);
  try
    Qry.Connection := Database;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT * FROM '+LowerCase(tabela));
    Qry.SQL.Add('LIMIT 1');
    AbreQuery(Qry, Database);
    //
    SetLength(Result, Qry.Fields.Count);
    //

    for j := 0 to Qry.Fields.Count-1 do
      Result[j] := Qry.Fields[j].FieldName;
  finally
    Qry.Free;
  end;
end;

function TUnGrl_DmkDB.ArrayDeTabelaIdentica_ValuesZ(
  Tabela: TZQuery): TUMyArray_Var;
var
  j: Integer;
begin
  //
  SetLength(Result, Tabela.Fields.Count);
  //
  for j := 0 to Tabela.Fields.Count-1 do
  begin
    Result[j] := Tabela.Fields[j].AsVariant;
  end;
end;

procedure TUnGrl_DmkDB.ExportaRegistrosEntreDBs_NovoZ(TabOrig, TabDest,
  CondicaoSQL: String; BaseOrig, BaseDest: TZConnection; RichEdit: TRichEdit);
  //
  procedure Info(RichEdit: TRichEdit; TabDest, Texto: String);
  begin
    MyObjects.AdicionaTextoCorRichEdit(RichEdit, clBlue, [],
    FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(TabDest) + Texto + sLineBreak);
{
    if RichEdit <> nil then
    begin
      RichEdit.SelAttributes.Color := clBlue;
      RichEdit.SelText := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(LowerCase(TabDest)) + Texto + sLineBreak + RichEdit.Text;
      RichEdit.Update;
      Application.ProcessMessages;
    end;
}
  end;
var
  Pasta: String;
  //F: TextFile;
  //S,
  AnsiArq: String;
  //PWChArq: PWideChar;
  Campos, Prefix, DataDir, Barra: String;
  //QrL,
  //Query,
  Qr1, Qr2, Qr3: TZQuery;
  ArrCampos: TUMyArray_Str;
  ArrValues: TUMyArray_Var;
begin
{
  Query := TZQuery.Create(Dmod);
  Query.Close;
  Query.Connection := BaseOrig;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + LowerCase(TabOrig));
  Query.SQL.Add(CondicaoSQL);
  if not Grl_DmkDB.AbreQuery(Query, BaseOrig) then
    Exit;
}
  //
  Qr1 := TZQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Connection := BaseOrig;
  Qr1.SQL.Clear;
  //
  Qr2 := TZQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Connection := BaseDest;
  Qr2.SQL.Clear;
  //
  Qr3 := TZQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Connection := BaseDest;
  Qr3.SQL.Clear;
  //
  //
  Qr1.SQL.Clear;
  //Qr1.SQL.Add('SHOW VARIABLES LIKE "datadir"');
  Qr1.SQL.Add('pragma database_list');
  Grl_DmkDB.AbreQuery(Qr1, BaseOrig);
  DataDir := Qr1.FieldByName('file').AsString;
  DataDir := ExtractFileDir(DataDir);
  //
  if DataDir = '' then
    Barra := '\'
  else
  if pos('\', DataDir) > 0 then
    Barra := '\'
  else
    Barra := '/';
  //
  Pasta := 'C:' +
    Barra + 'Dermatek' +
    Barra + 'Web' +
    Barra + Application.Title +
    Barra + 'Data' +
    Barra;
  AnsiArq := Pasta + 'SQL_' + LowerCase(TabDest) + '.txt';
  //PWChArq := PWideChar(AnsiArq);
  if not ForceDirectories(Pasta) then
  begin
    Grl_Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio "' + Pasta +
    '". Tente cri�-lo manualmente!');
    Exit;
  end;
  //
  Campos := '';
  Prefix := '';
  Info(RichEdit, LowerCase(TabDest), ' - Obtendo campos da tabela de destino...');
  Campos := ObtemCamposDeTabelaIdenticaZ(BaseDest, LowerCase(TabDest), Prefix);
  ArrCampos := ArrayDeTabelaIdentica_CamposZ(BaseDest, LowerCase(TabDest));

  //

  Info(RichEdit, LowerCase(TabDest), ' - Obtendo dados da tabela de origem...');
  if FileExists(AnsiArq) then
    DeleteFile(PWideChar(AnsiArq));
  AnsiArq := dmkPF.DuplicaBarras(AnsiArq);
  //PWChArq := PWideChar(AnsiArq);
  if FileExists(AnsiArq) then
    DeleteFile(PWideChar(AnsiArq));
  Application.ProcessMessages;

  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT ' + Campos);
  Qr1.SQL.Add('FROM '+ LowerCase(TabOrig));
  Qr1.SQL.Add( CondicaoSQL );
  // mudado 2011-10-31 s� funcionava no servidor
  //Qr1.SQL.Add('INTO OUTFILE "' + Arquivo + '"');
  //Qr1.ExecSQL;
  if not Grl_DmkDB.AbreQuery(Qr1, Qr1.Connection) then
  begin
    MyObjects.AdicionaTextoCorRichEdit(RichEdit, clRed, [fsBold],
    ' N�o foi poss�vel abrir a tabela  ' + Uppercase(TabDest) + sLineBreak);
    Exit;
  end;
  //

  Info(RichEdit, LowerCase(TabDest), ' - Excluindo registros duplicados ...');
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + LowerCase(TabDest));
  Qr3.SQL.Add(CondicaoSQL);
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //

  Qr1.First;
  Qr2.SQL.Clear;
  Info(RichEdit, LowerCase(TabDest), ' - Inserindo dados no destino...');
  while not Qr1.Eof do
  begin
    ArrValues := ArrayDeTabelaIdentica_ValuesZ(Qr1);
    //
    SQLInsUpd(Qr2, Qr2.Connection, stIns, LowerCase(TabDest), False,
      ArrCampos, [], ArrValues, [], False, dmksqlinsInsOnly, '',
      TDeviceType.stDesktop, False);
    //
    Qr1.Next;
  end;
  //
  Info(RichEdit, LowerCase(TabDest), ' - Finalizando ...');
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE ' + LowerCase(taborig) + ' SET AlterWeb=0');
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.ExecSQL;
  //
  ExecutaSQLQuery0(Qr2, Qr2.Connection, [
  'UPDATE ' + LowerCase(TabDest) +
  ' SET DataAlt="1899-12-30" ',
  ' WHERE DataAlt IS null ',
  '']);
  //
  Info(RichEdit, LowerCase(TabDest), ' - Transfer�ncia de dados finalizada!');
  Info(RichEdit, '', '===============================================');
end;


procedure TUnGrl_DmkDB.LeMeuSQL_Fixo_z(Query: TzQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow: Boolean; ForceMsgErr: Boolean = False);
var
  i: Integer;
  Text1, Text2: TextFile;
  Name, Diretorio: WideString;
  Other: Boolean;
  Texto1, Texto2, Texto3, Texto4: String;
begin
  //Texto1 := '/*'+TmySQLDatabase(Query.Database).DatabaseName+'.'+Query.Name+'*/';
  Texto1 := '/*'+TComponent(Query.Owner).Name + '.' + Query.Name + '*/';
  Texto2 := '/*********** SQL ***********/';
  Texto3 := '/********* FIM SQL *********/';
  Texto4 := sLineBreak;
  if WinArq then
  begin
    Diretorio := CO_DERMATEK_SISTEMA;
    if not DirectoryExists(Diretorio) then ForceDirectories(Diretorio);
    Name := Diretorio + '\SQL.txt';
    if FileExists(Name) then
      DeleteFile(PWideChar(Name));
    AssignFile(Text1, Name);
    ReWrite(Text1);
    WriteLn(Text1, Texto1);
  end;
  if Trim(Arquivo) <> '' then
  begin
    Other := True;
    if FileExists(Arquivo) then
      DeleteFile(PWideChar(Arquivo));
    AssignFile(Text2, Arquivo);
    ReWrite(Text2);
    WriteLn(Text2, Texto1);
  end else Other := False;
  if Memo <> nil then
  begin
    if TZConnection(Query.Connection) <> nil then
    begin
      Memo.Lines.Add(Texto1);
      Memo.Lines.Add(Texto2);
    end;
  end;
  for i := 0 to Query.SQL.Count -1 do
  begin
    if WinArq then WriteLn(Text1, Query.SQL[i]);
    if Other  then WriteLn(Text2, Query.SQL[i]);
    if Memo <> nil then Memo.Lines.Add(Query.SQL[i]);
  end;
  //
  if Query.Params.Count > 0 then
  begin
    Texto4 := Texto4 + '/***** Parametros *******/' + sLineBreak;
    for i := 0 to Query.Params.Count -1 do
    begin
      Texto4 := Texto4 + '/***** ' + Query.Params[i].Name + ' = ' +
        Grl_Geral.VariantToString(Query.Params[i].Value) + ' ******/'  + sLineBreak;
    end;
    Texto4 := Texto4 + '/***** FIM Parametros *******/' + sLineBreak;
  end else begin
    Texto4 := Texto4 + '/*****Query sem parametros*******/' + sLineBreak;
  end;
  if WinArq then WriteLn(Text1, Texto4);
  if Other  then WriteLn(Text2, Texto4);
  if Memo <> nil then Memo.Lines.Add(Texto4);
  //
  if WinArq then CloseFile(Text1);
  if Other  then CloseFile(Text2);
  if TZConnection(Query.Connection) <> nil then
    if Memo <> nil then Memo.Lines.Add(Texto3);
  if AskShow then
  begin
    if (FM_MASTER = 'V') or (Uppercase(VAR_LOGIN) = 'A') or ForceMsgErr then
    begin
      Grl_Geral.MB_Aviso(Texto1 + sLineBreak + Texto2 + sLineBreak + Query.SQL.Text +
      Texto3 + sLineBreak + Texto4);
    end;
  end;
end;

procedure TUnGrl_DmkDB.IncluiRegistroTbZ(Form: TForm; Tabela: TZTable;
  ComponentToHide, ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
begin
  Screen.Cursor := crHourGlass;
  MostraEdicaoTbZ(Form);
  if ComponentToHide <> nil then
    ComponentToHide.Visible := False;
  Tabela.Insert;
  if ComponentToFocus <> nil then
  try
    ComponentToFocus.Visible := True;
    ComponentToFocus.SetFocus;
  except
  end;
  if ImgTipo <> nil then ImgTipo.SQLType := stIns;
  Screen.Cursor := crDefault;
end;


procedure TUnGrl_DmkDB.ConfirmaRegistroTbZ_Numero(Form: TForm; Tabela: TZTable;
  ImgTipo: TdmkImage; PainelDados, PainelConfirma, PainelControle: TPanel;
  DBGItens: TDBGrid; CampoNumero: String = 'Numero'; CampoCodUsu: String = '');
var
  //I: integer;
  SQLType: TSQLType;
begin
  Screen.Cursor := crHourGlass;
  try
    if not (Tabela.State in ([dsInsert, dsEdit])) then
      Tabela.Edit;
    if not VerificaCamposObrigatoriosZ(Tabela) then
      Exit;
    DadosAutomaticosTbZ(Tabela);
    if Tabela.State = dsInsert then
    begin
      if Tabela.FieldByName(CampoNumero).AsInteger = 0 then
      begin
        Tabela.FieldByName(CampoNumero).AsInteger :=
          Grl_DmkDB.GetNxtCodigoInt(Tabela.TableName, CampoNumero, SQLType, 0);
        if Trim(CampoCodUsu) <> '' then
          Tabela.FieldByName(CampoCodUsu).AsInteger :=
            Grl_DmkDB.GetNxtCodigoInt(Tabela.TableName, CampoCodUsu, SQLType, 0);
      end;
    end;
    Tabela.Post;
    if ImgTipo <> nil then
      ImgTipo.SQLType := stLok;
    if PainelDados <> nil then
      PainelDados.Enabled := False;
    if PainelConfirma <> nil then
      PainelConfirma.Visible := False;
    if PainelControle <> nil then
      PainelControle.Visible := True;
    if DBGItens <> nil then
      DBGItens.Visible := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnGrl_DmkDB.VerificaCamposObrigatoriosZ(Tabela: TZTable): Boolean;
var
  i: integer;
begin
  Result := True;
  if not (Tabela.State in ([dsInsert, dsEdit])) then exit;
  for i := 0 to Tabela.FieldCount - 1 do
  begin
    if Uppercase(Tabela.Fields[i].FieldName) = 'NOME'
      then if trim(Tabela.FieldByName('Nome').AsString)= '' then
      begin
        Grl_Geral.MB_Erro('Informa um nome!');
        Screen.Cursor := crDefault;
        Result := False;
      end;
  end;
end;


{$EndIf}
*)

function TUnGrl_DmkDB.CordaDeQuery(Query: TDataSet; Campo: String): String;
var
  I: Integer;
begin
  Result := '';
  Query.First;
  while not Query.Eof do
  begin
    if Result <> '' then
      Result := Result + ', ';
    Result := Result + Grl_Geral.FF0(Query.FieldByName(Campo).AsInteger);
    //
    Query.Next;
  end;
end;

function TUnGrl_DmkDB.CordasDeQuery(const Query: TDataSet; const Campo: String;
const MaxItens: Integer; var StrCordas: String): TUMyArray_Str;
const
  Virgula = ', ';
var
  I, N, Conta: Integer;
  Corda: String;
begin
  Corda     := EmptyStr;
  StrCordas := EmptyStr;
  I := 0;
  N := 0;
  SetLength(Result, N);
  if Query.State <> dsInactive then
  begin
    Query.First;
    while not Query.Eof do
    begin
      if Corda <> EmptyStr then
        Corda := Corda + Virgula;
      Corda := Corda + Grl_Geral.FF0(Query.FieldByName(Campo).AsInteger);
      //
      I := I + 1;
      if I >= MaxItens then
      begin
        SetLength(Result, N + 1);
        Result[N] := Corda;
        if StrCordas <> EmptyStr then
          StrCordas := StrCordas + Virgula;
        StrCordas := StrCordas + Corda;
        Corda     := EmptyStr;
        N         := N + 1;
        I         := 0;
      end;
      Query.Next;
    end;
    if Length(Corda) > 0 then
    begin
      SetLength(Result, N + 1);
      Result[N] := Corda;
      if StrCordas <> EmptyStr then
        StrCordas := StrCordas + Virgula;
      StrCordas := StrCordas + Corda;
    end;
  end;
end;

function TUnGrl_DmkDB.AbreQuery(Query: TDataSet; DataBase: TComponent): Boolean;
const
  sProcName = 'TUnGrl_DmkDB.AbreQuery()';
var
  {$IfDef DMK_FMX}
  DB: TFDConnection;
  QuerySQL: TFDQuery;
  {$Else}
  {$IfDef DB_SQLite}
  DB: TZConnection;
  QuerySQL: TZQuery;
  {$Else}
  DB: TmySQLDataBase;
  QuerySQL: TmySQLQuery;
  {$EndIf}
  {$EndIf}
begin
  Result := False;
  if not ValidaTipoComp([Query, DataBase], 'TUnGrl_DmkDB.AbreQuery') then
    Exit;
  try
    {$IfDef DMK_FMX}
    DB       := TFDConnection(DataBase);
    QuerySQL := TFDQuery(Query);
    {$Else}
    {$IfDef DB_SQLite}
    DB       := TZConnection(DataBase);
    QuerySQL := TZQuery(Query);
    {$Else}
    DB       := TmySQLDataBase(DataBase);
    QuerySQL := TmySQLQuery(Query);
    {$EndIf}
    {$EndIf}
    QuerySQL.Close;
    {$IfDef DMK_FMX}
    QuerySQL.Connection := DB;
    {$Else}
    {$IfDef DB_SQLite}
    QuerySQL.Connection := DB;
    {$Else}
    QuerySQL.DataBase := DB;
    {$EndIf}
    {$EndIf}
    try
      QuerySQL.Open();
      Result := True;
    except
      on E: Exception do
      begin
  {$IfDef DB_SQLite}
  {$IfDef DMK_FMX}
        //if QuerySQL is TZQuery then
        Grl_Geral.MB_Erro(E.Message + slineBreak +
        sProcName + slineBreak +
        'Nome: ' + TFDQuery(QuerySQL).Name + slineBreak +
        'Owner: ' + TFDQuery(QuerySQL).Owner.Name + slineBreak +
        'SQL:' + sLineBreak + TFDQuery(QuerySQL).SQL.Text);
  {$Else}
        //if QuerySQL is TZQuery then
        Grl_Geral.MB_Erro(E.Message + slineBreak +
        sProcName + slineBreak +
        'Nome: ' + TZQuery(QuerySQL).Name + slineBreak +
        'Owner: ' + TZQuery(QuerySQL).Owner.Name + slineBreak +
        'SQL:' + sLineBreak + TZQuery(QuerySQL).SQL.Text);
  {$EndIf}
  {$Else}
        //if QuerySQL is TmySQLQuery then
        Grl_Geral.MB_Erro(E.Message + slineBreak +
        sProcName + slineBreak +
        'Nome: ' + TmySQLQuery(QuerySQL).Name + slineBreak +
        'Owner: ' + TmySQLQuery(QuerySQL).Owner.Name + slineBreak +
        'SQL:' + sLineBreak + TmySQLQuery(QuerySQL).SQL.Text);
  {$EndIf}
      end;
    end;
    //
  finally
    ;
  end;
end;

    {$IfDef DB_SQLite}
function TUnGrl_DmkDB.VerificaCampos_SQLite(DBName: String; TabelaNome,
  TabelaBase: String; Memo: TMemo; RecriaRegObrig, Tem_Del, Tem_Sync: Boolean;
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TComponent; MeFldsNoNeed: TMemo;
  DataBase: TComponent; LBTabToRecriar: TListBox): Integer;
var
  i, k, Resp, EhIdx: Integer;
  Opcoes, Item, Nome, IdxNome: String;
  Indices: TStringList;
  IdxExiste: Boolean;
  TemControle: TTemControle;
  {$IfDef DMK_FMX}
  QrNTV, QrUpd, QrIdx: TFDQuery;
  {$Else}
  QrNTV, QrUpd, QrIdx: TZQuery;
  {$EndIf}
  SQL: String;
begin
  Result := idOK;
  {$IfDef DMK_FMX}
  QrNTV := TFDQuery.Create(DataBase);
  QrUpd := TFDQuery.Create(DataBase);
  QrIdx := TFDQuery.Create(DataBase);
  {$Else}
  QrNTV := TZQuery.Create(DataBase);
  QrUpd := TZQuery.Create(DataBase);
  QrIdx := TZQuery.Create(DataBase);
  {$EndIf}
  try
    AbreSQLQuery0(QrNTV, DataBase, [
      'PRAGMA table_info(' + Lowercase(TabelaNome) + ');',
      '']);
    if QrNTV.RecordCount = 0 then
      Exit;
    try
      FLCampos  := TList<TCampos>.Create;
      FLIndices := TList<TIndices>.Create;
      try
        FCriarAtivoAlterWeb := True;
        TemControle := [];
        MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
        //
        if (tctrlLok in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Lk';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataCad';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataAlt';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserCad';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserAlt';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlWeb in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'AlterWeb';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAti in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Ativo';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if Tem_Del then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'DataDel';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'MotvDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        if Tem_Sync then
        begin
          New(FRCampos);
          FRCampos.Field      := 'LastModifi';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
          FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
          FRCampos.Null       := '';           //2 => Exclus�o
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'CliIntSync';
          FRCampos.Tipo       := 'int';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
        while not QrNTV.Eof do
        begin
          Nome := QrNTV.FieldByName('name').AsString;
          Item := Format(ivCampo_Nome, [TabelaNome, Nome]);

          case CampoAtivo_SQLite(DBName, TabelaNome, TabelaBase, Nome,
            Memo, QrNTV, DataBase) of
            1:
            begin
              try
                Opcoes := FRCampos.Tipo;
                if FRCampos.Null <> 'YES' then
                  Opcoes := Opcoes + ' NOT NULL ';
                if FRCampos.Default <> myco_ then
                  AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);
                //
                if FRCampos.Extra <> myco_ then
                begin
                  {$IfDef DMK_FMX}
                  if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) >= 0 then
                  {$Else}
                  if Pos(Uppercase('auto_increment'), Uppercase(FRCampos.Extra)) > 0 then
                  {$EndIf}
                    Opcoes := Opcoes + ' auto_increment ';
                end;
                Grl_Geral.MB_Aviso('O campo ' + Nome + ' da tabela ' + TabelaNome +
                  ' difere do esperado e o RDBM SQLite n�o permite regulariz�-lo.');
              except
                GravaAviso(Item + ivMsgERROAlterar, Memo);
                raise;
              end;
            end;
            9:
            begin
              AdicionaFldNoNeed_SQLite(MeFldsNoNeed, TabelaNome, Nome);
                GravaAviso('O campo "' + TabelaNome + '.' + Nome +
                '" deveria ser exclu�do!', Memo);
              if LBTabToRecriar <> nil then
                LBTabToRecriar.Items.Add(TabelaNome);
{ Exemplo de exclus�o de campo:
BEGIN TRANSACTION;
CREATE TABLE t1_new (
  foo  TEXT PRIMARY KEY,
  bar  TEXT,
  baz  INTEGER
);

INSERT INTO t1_new SELECT foo, bar, baz FROM t1;
DROP TABLE t1;
ALTER TABLE t1_new RENAME TO t1;
COMMIT;
}
            end;
          end;
          QrNTV.Next;
        end;
        for i:= 0 to FLCampos.Count - 1 do
        begin
          FRCampos := FLCampos[i];
          if CampoExiste_SQLite(TabelaNome, FRCampos.Field, Memo, QrNTV) = 2 then
          begin
            Item := Format(ivCampo_Nome, [TabelaNome, FRCampos.Field]);
            try
              Opcoes := FRCampos.Tipo;
              if FRCampos.Null <> 'YES' then
                Opcoes := Opcoes + ' NOT NULL ';
              if FRCampos.Default <> myco_ then
                AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);
              //
              SQL := mycoALTERTABLE + TabelaNome + mycoADD + FRCampos.Field +
                mycoEspaco + Opcoes;
              //
              if not FPergunta then
                Resp := idYes
              else
                Resp := Grl_Geral.MB_Pergunta('O campo ' + FRCampos.Field +
                        ' n�o existe na tabela ' + TabelaNome + ' e ser� criado.' +
                        sLineBreak + 'Confirma a cria��o do campo?');
              //
//              if Result = idYes then
              if Resp = idYes then
              begin
                ExecutaSQLQuery0(QrUpd, DataBase, [
                  SQL,
                  '']);
                GravaAviso(Item + ivcriado, Memo);
              end else
              begin
                GravaAviso(Item + ivAbortInclUser, Memo);
                if Resp = idCancel then
                begin
                  Result := idAbort;
                  GravaAviso(Item + ivAbortInclUser, Memo);
                  //
                  Exit;
                end;
              end;
            except
              GravaAviso(Item + ivMsgERROCriar, Memo);
              raise;
            end;
          end;
        end;
        Indices := TStringList.Create;
        Indices.Sorted := True;
        Indices.Duplicates := (dupIgnore);
        try
          for k := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[k];
            Indices.Add(FRIndices.Key_name);
          end;
          ExecutaSQLQuery0(QrIdx, DataBase, [
            'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
            '']);

          while not QrIdx.Eof do
          begin
            IdxNome := QrIdx.FieldByName('name').AsString;
            begin
              IdxExiste := False;
              Item := Format(ivIndice_Nome, [TabelaNome, IdxNome]);
              for k := 0 to Indices.Count -1 do
              begin
                if UpperCase(Indices[k]) = 'PRIMARY' then
                begin
                  if UpperCase(IdxNome) =
                    Uppercase('idx_' + TabelaNome + '_primary') then
                      IdxExiste := True
                  else
                  if UpperCase(IdxNome) =
                    Uppercase('sqlite_autoindex_' + TabelaNome + '_1') then
                      IdxExiste := True;
                end else
                if Indices[k] = IdxNome then
                  IdxExiste := True;
              end;
              if not IdxExiste then
              begin
                if not FPergunta then
                  Resp := idYes
                else
                  Resp := Grl_Geral.MB_Pergunta(Format(ivExclIndice, [Application.Title, IdxNome, TabelaNome]));
               //
               if Resp = idYes then
                begin
                  if ExcluiIndice_SQLite(DBName, TabelaNome,
                    IdxNome, Item, mymotSemRef, Memo, DataBase) <> idOK
                  then
                    Result := idAbort;
                end else
                begin
                  GravaAviso(Item + ivAbortExclUser, Memo);
                  //
                  if Resp = idCancel then
                  begin
                    Result := idAbort;
                    Exit;
                  end;
                end;
              end;
            end;
            QrIdx.Next;
          end;
          QrIdx.Close;
          for k := 0 to Indices.Count -1 do
          begin
            EhIdx := IndiceExiste_SQLite(TabelaNome, Indices[k], Memo, DataBase);

            if EhIdx in ([2,3]) then
              if RecriaIndice_SQLite(TabelaNome, Indices[k], Item, Memo, DataBase) <> idOK
              then begin
                Result := idAbort;
                Exit;
              end;
            if EhIdx in ([0]) then
            begin
              if CriaIndice_SQLite(TabelaNome, Indices[k], Item, Memo,
                DataBase) <> idOK then
              begin
                Result := idAbort;
              end;
            end;
          end;
        finally
          Indices.Free;
        end;
        ////
{$IfNDef DMK_FMX}
        VerificaRegistrosObrigatoriosSQLite_Inclui(TZConnection(Database),
          TabelaNome, TabelaBase, Memo, True, RecriaRegObrig,
          TLabel(LaAviso1B), TLabel(LaAviso2B),
          TLabel(LaAviso1G), TLabel(LaAviso2G));
{$EndIf}
      finally
        FLCampos.Free;
        FLIndices.Free;
      end;
    finally
      QrNTV.Close;
    end;
  finally
    if QrNTV <> nil then
      QrNTV.Free;
    if QrUpd <> nil then
      QrUpd.Free;
    if QrIdx <> nil then
      QrIdx.Free;
  end;
end;

function TUnGrl_DmkDB.IndiceExiste_SQLite(Tabela, Indice: String; Memo: TMemo;
  DataBase: TComponent): Integer;
var
  i: Integer;
  Need, Find, Have: Integer;
  Item: String;
  {$IfDef DMK_FMX}
  QrIts: TFDQuery;
  {$Else}
  QrIts: TZQuery;
  {$EndIf}
  MyIDXa0, MyIDXb1, MyIDXb2, MyIDXc1: String;
  isPRI, Continua: Boolean;
begin
  Result := 0;
  //
  if not ValidaTipoComp([DataBase], 'TUnGrl_DmkDB.IndiceExiste_SQLite') then
    Exit;
  //
  Item := Format(ivCampo_Nome, [Tabela, Indice]) + mycoEspaco + 'N�o encontrado:';
  Need := 0;
  Find := 0;
  Have := 0;
  //
  {$IfDef DMK_FMX}
  QrIts := TFDQuery.Create(DataBase);
  {$Else}
  QrIts := TZQuery.Create(DataBase);
  {$EndIf}
  try
    AbreSQLQuery0(QrIts, DataBase, [
      'SELECT name FROM sqlite_master ',
      'WHERE type=''index''',
      'AND Name LIKE ''%_' + LowerCase(Tabela) + '_%''',
      'ORDER BY name; ',
      '']);
    QrIts.First;
    while not QrIts.Eof do
    begin
      QrIts.Next;
    end;

    while not QrIts.Eof do
    begin
      if Uppercase(Indice) = 'PRIMARY' then
      begin
        if Uppercase(QrIts.FieldByName('name').AsString) = Uppercase('idx_' +
        Tabela + '_primary') then
          Have := Have + 1
        else
        if Uppercase(QrIts.FieldByName('name').AsString) =
          Uppercase('sqlite_autoindex_' + Tabela + '_1') then
            Have := Have + 1;
      end else
      if Uppercase(QrIts.FieldByName('name').AsString) =
      Uppercase(Indice) then
        Have := Have + 1;
      QrIts.Next;
    end;

    try
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(Indice) then
        begin
          QrIts.First;

          Need := Need + 1;

          while not QrIts.Eof do
          begin
            MyIDXa0 := Uppercase(QrIts.FieldByName('name').AsString);
            MyIDXb1 := 'idx_' + Tabela + '_primary';
            MyIDXb2 := 'sqlite_autoindex_' + Tabela + '_1';
            MyIDXc1 := FRIndices.Key_name;
            isPRI   := Uppercase(FRIndices.Key_name) = 'PRIMARY';

            Continua :=
            not (
              (
                isPRI
                and (
                  (MyIDXa0 = MyIDXb1)
                  or
                  (MyIDXa0 = MyIDXb2)
                )
              )
              or
              (MyIDXa0 = MyIDXc1)
            );
            if Continua then
            begin
              if Result = 0 then
                Result := 1;
              Find := Find + 1;
            end;
            QrIts.Next;
          end;
          if Find < Need then
          begin
            if Find > 0 then
            begin
              GravaAviso('A coluna ' + FRIndices.Column_name +
                ' n�o foi encontrada no �ndice ' + Indice + '.', Memo);

              Result := 2;
            end else
              GravaAviso('O �ndice ' + Indice + ' n�o existe.', Memo);
          end;
        end;
      end;
    except
      QrIts.Close;
      raise;
    end;
    if Have > Find then
    begin
      GravaAviso('H� excesso de colunas no �ndice ' + Indice + ' da tabela ' + Tabela +  '.', Memo);
      Result := 3;
    end;
  finally
    QrIts.Free;
  end;
end;
{$EndIf}


function TUnGrl_DmkDB.ExcluiRegistros(Pergunta: String; QrUpd: TDataSet;
  DataBase: TComponent; Tabela: String; SQLCampos, Compare: array of String;
  ValCampos: array of Variant; ComplSQL: String): Boolean;
var
  Continua: Boolean;
  TextoSQL: String;
begin
  Result := False;
  if Trim(Pergunta) = '' then Continua := True else
    Continua := Grl_Geral.MB_Pergunta(Pergunta) = idYES;
  if Continua then
  begin
    if GeraCondicaoDeSQL(TextoSQL, Tabela, SQLCampos, Compare, ValCampos,
    'WHERE', ComplSQL) then
    begin
      Result := ExecutaSQLQuery0(QrUpd, DataBase,
        [DELETE_FROM + ' ' + LowerCase(Tabela) + TextoSQL]);
    end;
  end;
end;

function TUnGrl_DmkDB.LocalizaPriorNextLargeIntQr(Tabela: TDataSet;
  Campo: TLargeIntField; Atual: LargeInt): LargeInt;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;

function TUnGrl_DmkDB.CriaFm(InstanceClass: TComponentClass; var Reference;
  ModoAcesso: TAcessFmModo; Titulo: String): Boolean;

  procedure Habilita(Compo: TComponent; Visivel: Boolean);
  var
    PropInfo: PPropInfo;
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, 'Visible');
      if PropInfo <> nil then
        SetPropValue(Compo, 'Visible', Visivel);
    end;
  end;

var
  Instance: TComponent;
  Form, Nome, Form_ID: String;
  MyCursor: TCursor;
  k, i, Acesso: Integer;
  Inclui, Altera, Exclui: Boolean;
  //ModoAntigo: TAcessFormModo;
  Objeto: TObject;
  PropInfo: PPropInfo;
  Continua: Boolean;
begin
  {
  if  (UpperCase(Application.Title) = CO_APP_TITLE_003)//CRED ITOR
  or  (UpperCase(Application.Title) = 'SYNDIC') then
  }

  {$IFDEF UsaWSuport}
 {
  if InstanceClass <> TFmWebBrowser then
  begin
    if FmWebBrowser <> nil then
      FmWebBrowser.Close;
  end else
  }
  if InstanceClass <> TFmWSuporte then
  begin
    if FmWSuporte <> nil then
      FmWSuporte.Close;
  end;
  {$ENDIF}

{
  if (UpperCase(Application.Title) = CO_APP_TITLE_003) then //CRED ITOR
  begin
    case ModoAcesso of
      afmoSoMaster:      ModoAntigo := afmSoMaster;
      afmoSoAdmin:       ModoAntigo := afmSoMaster;
      afmoSoBoss:        ModoAntigo := afmSoBoss;
      afmoNegarSemAviso: ModoAntigo := afmNegarSemAviso;
      afmoNegarComAviso: ModoAntigo := afmNegarComAviso;
      afmoLiberado   : ModoAntigo := afmAcessoTotal;
      afmoSemVerificar  : ModoAntigo := afmAcessoTotal;
      //
      else ModoAntigo := afmNegarComAviso;
    end;
    Result := CriaFormAntigo(InstanceClass, Reference, ModoAntigo);
    Exit;
  end;
}
  Result := False;
  Form := InstanceClass.ClassName;
  //
  if Pos('TFm', Form) = 1 then
    Form := Copy(Form, 4, Length(Form)-3);
{$IfNDef DMK_FMX}
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
{$EndIF }
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
{
      // 2011-09-23
      TForm(Reference).AlphaBlendValue := 0;
      TForm(Reference).AlphaBlend := True;
      // Fim 2011-09-23
}
      ///
      if VAR_SOMAIUSCULAS then
      begin
        with TForm(Reference) do
        begin
          for i := 0 to ComponentCount -1 do
          begin
            Objeto := Components[i];
            PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'CharCase');
            if PropInfo <> nil then
            begin
              Nome := Uppercase(TComponent(Objeto).Name);

              if (Nome <> 'EDEMAIL') and (Nome <> 'CBEMAIL') and
              (Nome <> 'EDPEMAIL') and (Nome <> 'EDEEMAIL') then
              try
                if (GetPropValue(TComponent(Objeto), 'CharCase') = 'ecNormal') then
                begin
                  Continua := True;
                  PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'NoForceUppercase');
                  //
                  if PropInfo <> nil then
                  begin
                    if GetPropValue(TComponent(Objeto), 'NoForceUppercase') = 'True' then
                      Continua := False;
                  end;
                  //
                  if Continua then
                    SetPropValue(Objeto, 'CharCase', 'ecUpperCase');
                end;
              except
                //
              end;
            end;
          end;
        end;
      end;
      {
      if  (UpperCase(Application.Title) = CO_APP_TITLE_003) //CRED ITOR
      or  (UpperCase(Application.Title) = 'SYNDIC') then
      }
{$IfNDef DMK_FMX}
      if (UpperCase(Application.Title) = CO_APP_TITLE_003) then//CRED ITOR
      begin
        Screen.Cursor := MyCursor;
        Result := True;
        Exit;
      end;
{$EndIF }

      ///
      //k := pos('::', TForm(Instance).Caption);
      //if k = 0 then
      k := 13;
      if Titulo <> '' then
        TForm(Instance).Caption := Titulo;
      //
      Form_ID := Copy(TForm(Instance).Caption, 1, k);
      Define_Form_ID(Form_ID);
      // Verifica se a janela existe
{***
      DModG.QrPerfJan.Close;
      DModG.QrPerfJan.Params[0].AsString := Form_ID;
      UnDmkDAC_PF.AbreQuery(DModG.QrPerfJan, Dmod.MyDB);
      if DModG.QrPerfJan.RecordCount = 0 then
      begin
        if (Form_ID <> 'MAS-CADAS-000') and (ModoAcesso <> afmoSemVerificar) then
        begin
          Grl_Geral.MB_Aviso('A janela ' + Form_ID +
          ' n�o est� cadastrada para ser consultada!' + sLineBreak +
          'Informe a DERMATEK!');
          if (VAR_USUARIO >=0) or (Form_ID <> 'FER-VRFBD-001') then
          begin
            TForm(Instance).Destroy;
            Screen.Cursor := MyCursor;
            Exit;
          end;
        end;
      end;
***}
      if VAR_LIBERA_TODOS_FORMS = False then
      begin
{***
        Acesso := AcessoAoForm(Form_ID, ModoAcesso);
***} Acesso := 1;
        if Acesso = 0 then
        begin
          TForm(Instance).Destroy;
{$IF DEFINE MSWINDOWS}
          Screen.Cursor := MyCursor;
{$EndIF}
          Exit;
        end else begin
          Result := True;
          if Acesso < 8 then
          begin
{***
            Inclui := DmodG.QrPerfInclui.Value = 1;
            Altera := DmodG.QrPerfAltera.Value = 1;
            Exclui := DmodG.QrPerfExclui.Value = 1;
            for i := 0 to TComponent(Reference).ComponentCount - 1 do
            begin
              if TComponent(Reference).Components[i] is TdmkPermissoes then
              begin
                if TdmkPermissoes(TComponent(Reference)) <> nil then
                begin
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns01, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns02, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns03, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns04, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns05, Inclui);
                  //
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd01, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd02, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd03, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd04, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd05, Altera);
                  //
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel01, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel02, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel03, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel04, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel05, Exclui);
                  //
                end;
              end;
            end;
***}
          end;
        end;
      end;
    except
      //VOSTRO_1320
      raise;
      TComponent(Reference) := nil;
    end;
{$IfNDef DMK_FMX}
    Screen.Cursor := MyCursor;
{$EndIF}
  finally
{$IfNDef DMK_FMX}
    Screen.Cursor := MyCursor;
{$EndIF }
  end;
end;


function TUnGrl_DmkDB.TextoSQLInsUpd(const DBType: TDBType;
  const Tipo: TSQLType; const Tabela: String; const Auto_increment: Boolean; const SQLCampos,
  SQLIndex: array of String; const ValCampos, ValIndex: array of Variant;
  const UserDataAlterweb: Boolean; const HowIns: TdmkSQLInsert; const ComplUpd: String;
  const Device: TDeviceType; const Sincro: Boolean; var SQL: String): Boolean;
var
  i, j, k, Usuario, CliIntSync, Var_EntId, Var_UserId: Integer;
  Tab, Data, Campos, Valores, Valor, LastModifi_Txt, SQLWhere, Liga: String;
  AlterWeb: TAlterWeb;
  LastAcao: TLastAcao;
  LastModifi: TDateTime;
begin
  SQL := '';
{
  if Var_EntId = 0 then
    Var_EntId := VAR_API_ID_ENTIDADE;
  if Var_UserId = 0 then
    Var_UserId := VAR_API_ID_USUARIO;

  if Sincro = True then
    Usuario := Var_UserId
  else
}
    Usuario := VAR_USUARIO;


  i := High(SQLCampos);
  j := High(ValCampos);

  if i <> j then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + Grl_Geral.FF0(i+1) + ' campos e ' +
      Grl_Geral.FF0(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;

  i := High(SQLIndex);
  j := High(ValIndex);

  if i <> j then
  begin
    Grl_Geral.MB_Erro('ERRO! Existem ' + Grl_Geral.FF0(i+1) + ' �ndices e ' +
      Grl_Geral.FF0(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;

  if (i >= 0) and Auto_increment then
  begin
    Grl_Geral.MB_Erro('AVISO! Existem ' + Grl_Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  Tab      := LowerCase(Tabela);
  AlterWeb := alDsk;

  case Device of
    stDesktop:
      AlterWeb := alDsk;
    stWeb:
      AlterWeb := alWeb;
    stMobile:
      AlterWeb := alMob;
  end;

  if ((Tipo <> stIns) and (Tipo <> stUpd)) then
  begin
    Grl_Geral.MB_Erro('AVISO: O status da a��o est� definida como ' +
      '"' + Grl_DmkDB.NomeTipoSQL(Tipo) + '"');
  end;

  Data := '"' + Grl_Geral.FDT(Date, 1) + '"';

  if Tipo = stIns then
  begin
    case HowIns of
      //dmksqlinsIndef=0,
      (*1*)dmksqlinsInsOnly:
        SQL := SQL + ('INSERT INTO ' + Lowercase(Tab) + ' (');
      (*2*)dmksqlinsInsIgnore:
      begin
        // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
        {$IfDef DB_SQLite}
        SQL := SQL + ('INSERT OR IGNORE INTO ' + Lowercase(Tab) + ' (')
        {$Else}
        SQL := SQL + ('INSERT IGNORE INTO ' + Lowercase(Tab) + ' (')
        {$EndIf}
      end;
      (*3*)dmksqlinsInsReplace:
      begin
        // n�o gera erro e substitui o registro quando poderia duplicar registro com chave
        {$IfDef DB_SQLite}
        SQL := SQL + ('INSERT OR REPLACE INTO ' + Lowercase(Tab) + ' (')
        {$Else}
        SQL := SQL + ('REPLACE INTO ' + Lowercase(Tab) + ' (')
        {$EndIf}
      end;
      else
      begin
        Grl_Geral.MB_Erro('"HowIns" n�o implementado: ' + Grl_Geral.FF0(Integer(HowIns)));
        Exit;
      end;
    end;
  end else
  begin
    SQL := SQL + ('UPDATE ' + Lowercase(Tab) + ' SET ');

    if UserDataAlterweb then
      SQL := SQL + ('AlterWeb='+ Grl_Geral.FF0(Integer(AlterWeb)) +', ');
  end;

  Campos  := '';
  Valores := '';
  j       := System.High(SQLCampos);

  for i := System.Low(SQLCampos) to j do
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', ' + SQLCampos[i];
      Valor   := Grl_Geral.VariavelToStringAD(ValCampos[i]);
      Valores := Valores + ', ' + Valor;
    end else
    begin
      if SQLCampos[i] = CO_JOKE_SQL then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
          SQL := SQL + (ValCampos[i] + ', ')
        else
          SQL := SQL + (ValCampos[i]);
      end else
      begin
        Valor := Grl_Geral.VariavelToStringAD(ValCampos[i]);

        if (i < j) or UserDataAlterweb then
          SQL := SQL + (SQLCampos[i] + '=' + Valor + ', ')
        else
          SQL := SQL + (SQLCampos[i] + '=' + Valor);
      end;
    end;
  end;

  if Sincro then
  begin
    LastModifi     := Grl_Geral.DateTime_MyTimeZoneToUTC(Now);
    LastModifi_Txt := '"' + Grl_Geral.FDT(LastModifi, 109) + '"' ;
    CliIntSync     := Var_EntId;

    if Var_UserId  = 0 then
    begin
      Grl_Geral.MB_Erro('A vari�vel "VAR_WEB_USR_ID / VAR_API_ID_USUARIO" n�o est� definida!');
      Exit;
    end;

    if Var_EntId = 0 then
    begin
      Grl_Geral.MB_Erro('A vari�vel "VAR_WEB_USR_ENT / VAR_API_ID_ENTIDADE" n�o est� definida!');
      Exit;
    end;

(*
    if Tipo = stIns then
    begin
      LastAcao := laIns; //Inclus�o
    end else
    begin
      //Se n�o faz sincronismo, manter o "lastacao" = "inclus�o" por causa do sincronismo
      LastAcao := laUpd;//Altera��o
      SQLWhere := '';

      for k := Low(SQLIndex) to High(SQLIndex) do
      begin
        SQLWhere := SQLWhere + ' AND ' + SQLIndex[k] + '="'+ FMX_Geral.VariavelToStringAD(ValIndex[k]) +'"';
      end;
      {$IfDef DMK_FMX}
      QueryLoc := TFDQuery.Create(DB);
      {$Else}
      {$IfDef DB_SQLite}
      QueryLoc := TZQuery.Create(DB);
      {$Else}
      QueryLoc := TmySQLQuery.Create(DB);
      {$EndIf}
      {$EndIf}
      try
        AbreSQLQuery0(QueryLoc, DataBase, [
          'SELECT LastAcao ',
          'FROM ' + Tabela,
          'WHERE AlterWeb=' + Grl_Geral.FF0(Integer(Grl_Geral.ObtemAlterWebDeDeviceType(Device))),
          SQLWhere,
          '']);
        if QueryLoc.RecordCount > 0 then
        begin
          if QueryLoc.FieldByName('LastAcao').AsInteger = 0 then //Inclus�o
            LastAcao := laIns;
        end;
      finally
        QueryLoc.Free;
      end;
    end;
*)

    if Tipo = stIns then
    begin
      Campos  := Campos + ', LastModifi, LastAcao, CliIntSync';
      Valores := Valores + ', ' + LastModifi_Txt + ', ' + Grl_Geral.FF0(Integer(LastAcao)) +
                   ', ' + Grl_Geral.FF0(CliIntSync);
    end else
      SQL := SQL + ('LastModifi=' + LastModifi_Txt + ', LastAcao=' +
        Grl_Geral.FF0(Integer(LastAcao)) + ', CliIntSync=' + Grl_Geral.FF0(CliIntSync) + ', ');
  end;

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', DataCad, DataAlt, UserCad, UserAlt, AlterWeb';
      Valores := Valores + ', ' + Data + ', "1899-12-30 12:00", ' + Grl_Geral.FF0(Usuario) +
                   ', 0, ' + Grl_Geral.FF0(Integer(AlterWeb));
    end else
      SQL := SQL + ('DataAlt=' + Data + ', UserAlt=' + Grl_Geral.FF0(Usuario));
  end;

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Grl_Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
  end else
  begin
    for i := System.Low(SQLIndex) to System.High(SQLIndex) do
    begin
      if Tipo = stIns then
      begin
        Liga := ', ';
      end else
      begin
        if i = 0 then
          Liga := ' WHERE '
        else
          Liga := ' AND ';
      end;

      if SQLIndex[i] = CO_JOKE_SQL then
      begin
        SQL := SQL + (Liga + ValIndex[i]);
      end else
      begin
        if Tipo = stIns then
        begin
          Campos  := Campos + ', ' + SQLIndex[i];
          Valor   := Grl_Geral.VariavelToStringAD(ValIndex[i]);
          Valores := Valores + ', ' + Valor;
        end else
        begin
          Valor := Grl_Geral.VariavelToStringAD(ValIndex[i]);
          //
          SQL := SQL + (Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;

  if Tipo = stIns then
  begin
    {$IfDef DMK_FMX}
    Campos  := Campos.Substring(2) + ') VALUES (';
    Valores := Valores.Substring(2) + ');';
    {$Else}
    Campos  := Copy(Campos, 3, Length(Campos)) + ') VALUES (';
    Valores := Copy(Valores, 3, Length(Valores)) + ');';
    {$EndIf}

    SQL := SQL + (Campos);
    SQL := SQL + (Valores);
  end;

  if Trim(ComplUpd) <> '' then
  begin
    SQL := SQL + (ComplUpd);
  end;
  Result := True;
{ N�o pode ser Aqui! - Aqui s� carrega!!!
  try
    QueryUpd.ExecSQL();
    Result := True;
  except
    on E: Exception do
      Grl_Geral.MB_Erro('ERRO de SQL: ' + sLineBreak +
      E.ClassName + ': ' + E.Message + '.' + sLineBreak +
      QueryUpd.SQL.Text)
  end;
}
end;

function TUnGrl_DmkDB.RefreshQuery(Query: TDataSet): Boolean;
begin
  Query.Close;
  Query.Open;
end;


end.
