unit UnGrlTemp;

interface

uses UnDmkEnums,
{$IfDef DMK_FMX}
FMX.Memo,
{$Else}
Vcl.StdCtrls,
{$EndIf}
System.UITypes;

type
  TUnGrlTemp = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InfoMemoStep(Texto: String);
  end;
var
  MeSTEP_CODE: TMemo = nil;
  GrlTemp: TUnGrlTemp;


implementation

{ TUnTemp }

procedure TUnGrlTemp.InfoMemoStep(Texto: String);
begin
  if MeSTEP_CODE <> nil then
    MeSTEP_CODE.Text := Texto + sLineBreak + MeSTEP_CODE.Text;
end;

end.
