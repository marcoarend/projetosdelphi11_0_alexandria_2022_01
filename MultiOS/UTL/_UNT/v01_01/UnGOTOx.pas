unit UnGOTOx;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls,
  UnInternalConsts2, Mask, ComCtrls, (*DBTables,*) ResIntStrings, DmkDAC_PF,
  (*DBIProcs,*) Registry, ZCF2, buttons, UnInternalConsts3, UnDmkProcFunc,
  mySQLDbTables, Grids, DBGrids, TypInfo, Winsock, IniFiles, Variants,
  dmkGeral, UnDmkEnums, UMySQLDB;

type
  TUnGOTOx = class(TObject)
  private
    { Private declarations }
    function  LocalizaCodigo(Atual, Codigo: Integer): Boolean;
    function  LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
    function  LocalizaTxtUsu(Atual, Codigo: String): Boolean;
    function  Vai(Para: TVaiPara; Atual: Integer; Sort: String) : String;
  public
    { Public declarations }
    function  CriaLocalizador(Codigo: Integer): Boolean;
    function  LC(Atual, Codigo: Integer): Boolean;
  end;

var
  GOTOx: TUnGOTOx;

implementation

{ TUnGOTOx }

function TUnGOTOx.CriaLocalizador(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOMySQLTABLE.Database <> DefineBaseDados) then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizador". Tabela: '+VAR_GOTOMySQLTABLE.Name);
  if GetPropInfo(VAR_GOTOMySQLTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizador". Tabela: '+VAR_GOTOMySQLTABLE.Name)
  else begin
    VAR_GOTOMySQLTABLE.Close;
    try
      VAR_GOTOMySQLTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
      for i := 0 to VAR_SQL1.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL1[i]);
      // Cuidado erro frequente AND na SQL sem WHERE !!!!
      VAR_GOTOMySQLTABLE.Params[0].AsInteger := Codigo;
      //UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
      UnDmkDAC_PF.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLDBNAME);
      //
      if Registros(VAR_GOTOMySQLTABLE) > 0 then Result := True;
    except;
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(VAR_GOTOMySQLTABLE, '', nil, True, True);
      Geral.MB_Erro('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizador". Tabela: '+VAR_GOTOMySQLTABLE.Name);
      raise;
    end;
  end;
end;

function TUnGOTOx.LC(Atual, Codigo: Integer): Boolean;
begin
  Result := LocalizaCodigo(Atual, Codigo);
end;

function TUnGOTOx.LocalizaCodigo(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    try
      Result := CriaLocalizador(Codigo);
    except
       raise;
    end;
  finally
     if not Result then
     begin
       EAbort.Create('Erro na fun��o "CriaLocalizador".');
     end;
  end;
  try
    if (VAR_GOTOMySQLTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizador(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodigo".');
    raise;
  end;
end;

function TUnGOTOx.LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorCodUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorCodUsu".');
  end;
  try
    if (Registros(VAR_GOTOMySQLTABLE) = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizadorCodUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodUsu".');
    raise;
  end;
end;

function TUnGOTOx.LocalizaTxtUsu(Atual, Codigo: String): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorTxtUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorTxtUsu".');
  end;
  try
    if (Registros(VAR_GOTOMySQLTABLE) = 0) (*and ((Atual <> '') or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizadorTxtUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaTxtUsu".');
    raise;
  end;
end;

function TUnGOTOx.Vai(Para: TVaiPara; Atual: Integer; Sort: String): String;
const
  Mostra = False; //True;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOMySQLTABLE.First else
    if Para = vpPrior then VAR_GOTOMySQLTABLE.Prior else
    if Para = vpNext then VAR_GOTOMySQLTABLE.Next else
    if Para = vpLast then VAR_GOTOMySQLTABLE.Last else
    if Para = vpThis then
      if not VAR_GOTOMySQLTABLE.Locate('Periodo', Geral.Periodo2000(Date), []) then
        Geral.MB_Aviso('Per�odo atual n�o localizado!');
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    QvLocY.Database := DefineBaseDados;
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      }
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocY.SQL.Add('AND ')
        else
          QvLocY.SQL.Add('WHERE ');
        QvLocY.SQL.Add(VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
      end;
      }
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsInteger := Atual;
      Increm := -1;
      Quando := True;
    end else
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
      end;
      }
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsInteger := Atual;
      Increm := 1;
      Quando := True;
    end else
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      }
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocY.SQL.Add('AND ')
        else
          QvLocY.SQL.Add('WHERE ');
        QvLocY.SQL.Add(VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := 2;
    end else
    if Para = vpThis then
    begin
      QvLocY.SQL.Add('SELECT '+VAR_GOTOCAMPO+' Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+'='+IntToStr(Geral.Periodo2000(Date)));
      Quando := True;
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      Increm := 0;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    //
    //QvLocY. O p e n ;
    UMyMod.AbreQuery(QvLocY, QvLocY.Database, 'TUnGOTOy.Vai()');
    //
    if Mostra then
      dmkPF.LeMeuTexto(QvLocY.SQL.Text);
    QvLocY.First;
    if (QvLocY.RecordCount > 0) then
    begin
      if (QvLocY.FieldByName('Record').AsString <> '') then
        if (QvLocY.FieldByName('Record').AsInteger <> null) then
          LocalizaCodigo(Atual, QvLocY.FieldByName('Record').AsInteger);
    end else
    begin
      if Para = vpThis then
      begin
        // Colocar a mensagem no localizador de per�odo
        //Geral.MB_Aviso(VAR_PERIODO_NAO_LOC + ': Atual'));
        Result := VAR_PERIODO_NAO_LOC;
        QvLocY.Close;
        Exit;
      end;
    end;
    QvLocY.Close;
  end;
  if VAR_GOTOMySQLTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOMySQLTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

end.
