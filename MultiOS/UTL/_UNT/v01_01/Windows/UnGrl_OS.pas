unit UnGrl_OS;

interface

uses System.SysUtils, System.Classes,
{$IfDef DMK_FMX}
UnGrl_Geral,
{$Else}
dmkGeral,
{$EndIf}
Winapi.Windows;

type
  TUnGrl_OS = class(TObject)
  private

  public
    function  ObtemIMEI_MD5(): String;
    procedure ObtemDadosDispositivo(var OSName, OSNickName, OSVersion,
              TipoDispositivo: String);
  end;

var
  Grl_OS: TUnGrl_OS;

implementation

uses UnitMD5;

{ TUnGrl_OS }

function TUnGrl_OS.ObtemIMEI_MD5(): String;

  function CalculaValSerialKey2(DataVerifi: String; Web: Boolean): String;

    function SerialNum(FDrive:String): String;
    var
      nVNameSer: PDWORD;
      pVolName: PChar;
      FSSysFlags, maxCmpLen: DWord;
      pFSBuf: PChar;
      drv: String;
    begin
      try
        drv := FDrive + '\';

        GetMem(pVolName, MAX_PATH);
        GetMem(pFSBuf, MAX_PATH);
        GetMem(nVNameSer, MAX_PATH);

        GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
          maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

        {$IfDef DMK_FMX}
        Result := Grl_Geral.FF0(nVNameSer^);
        {$Else}
        Result := Geral.FF0(nVNameSer^);
        {$EndIf}

        FreeMem(pVolName, MAX_PATH);
        FreeMem(pFSBuf, MAX_PATH);
        FreeMem(nVNameSer, MAX_PATH);
      except
        Result := '';
      end;
    end;

    function MacAddress2: String;
    var
      a,b,c,d: LongWord;
      CPUID: String;
    begin
      asm
        push EAX
        push EBX
        push ECX
        push EDX

        mov eax, 1
        db $0F, $A2
        mov a, EAX
        mov b, EBX
        mov c, ECX
        mov d, EDX

        pop EDX
        pop ECX
        pop EBX
        pop EAX

        {
        mov eax, 1
        db $0F, $A2
        mov a, EAX
        mov b, EBX
        mov c, ECX
        mov d, EDX
        }
      end;
      CPUID  := IntToHex(a,8);// + inttohex(b,8) + inttohex(c,8) + inttohex(d,8);
      Result := CPUID;
    end;

  var
    Unidade, HD, MAC: String;
  begin
    //SerialKey = N�mero do HD + N�mero do CPU + Data da �ltima verifica��o
    //
    Unidade := ExtractFileDrive(ParamStr(0));//ExtractFileDrive(Application.ExeName);
    HD      := SerialNum(Unidade);
    MAC     := MacAddress2;
    //
    if Web then
      Result := UnMD5.StrMD5(HD + MAC)
    else
      Result := UnMD5.StrMD5(HD + MAC + DataVerifi);
  end;

begin
  Result := CalculaValSerialKey2('', True);
end;

procedure TUnGrl_OS.ObtemDadosDispositivo(var OSName, OSNickName,
  OSVersion, TipoDispositivo: String);

  function IndyComputerName(): string;
  var
    i: LongWord;
  begin
    SetLength(Result, MAX_COMPUTERNAME_LENGTH + 1);
    i := Length(Result);
    if GetComputerName(@Result[1], i) then begin
      SetLength(Result, i);
    end;
  end;

begin
  OSName          := TOSVersion.Name;
  OSNickName      := TOSVersion.ToString;
  TipoDispositivo := IndyComputerName;

  {$IfDef DMK_FMX}
  OSVersion := Grl_Geral.FF0(TOSVersion.Major) + '.' + Grl_Geral.FF0(TOSVersion.Minor);
  {$Else}
  OSVersion := Geral.FF0(TOSVersion.Major) + '.' + Geral.FF0(TOSVersion.Minor);
  {$EndIf}
end;

end.
