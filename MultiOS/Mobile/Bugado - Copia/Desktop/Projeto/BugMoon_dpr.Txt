program BugMoon;

uses
  System.StartUpCopy,
  FMX.Forms,
  VerifyDB in '..\..\Mobile\Units\VerifyDB.pas' {FmVerifyDB},
  ModApp in '..\..\Mobile\Units\ModApp.pas' {DmModApp},
  UsuarioAdd in '..\..\Mobile\Units\UsuarioAdd.pas' {FmUsuarioAdd},
  Principal in '..\..\Mobile\Units\Principal.pas' {FmPrincipal},
  UnSQLite_PF in '..\..\..\..\AllOS\RDBMs\SQLite\UnSQLite_PF.pas',
  UnGrl_Consts in '..\..\..\Geral\UnGrl_Consts.pas',
  UnGrl_Tabs in '..\..\..\Geral\UnGrl_Tabs.pas',
  UnGrl_Vars in '..\..\..\Geral\UnGrl_Vars.pas',
  UnMyObjects in '..\..\..\Geral\UnMyObjects.pas',
  UnWebReq in '..\..\..\Geral\UnWebReq.pas',
  UnDmkEnums in '..\..\..\Geral\UnDmkEnums.pas',
  UnGeral in '..\..\..\Geral\UnGeral.pas',
  UnApp_Consts in '..\..\Mobile\Units\UnApp_Consts.pas',
  UnApp_Vars in '..\..\Mobile\Units\UnApp_Vars.pas',
  UnDmkSQLite in '..\..\..\..\AllOS\RDBMs\SQLite\UnDmkSQLite.pas',
  UnDmkRDBMs in '..\..\..\..\AllOS\RDBMs\UnDmkRDBMs.pas',
  UnCheckSQLite in '..\..\..\..\AllOS\RDBMs\SQLite\UnCheckSQLite.pas',
  SincroDown in '..\..\Mobile\Units\SincroDown.pas' {FmSincroDown},
  UnMyListas in '..\..\Mobile\Units\UnMyListas.pas',
  ModGerl in '..\..\..\Geral\ModGerl.pas' {DmModGerl},
  UnMyLinguas in '..\..\..\..\AllOS\Listas\UnMyLinguas.pas',
  UnDmkListas in '..\..\..\..\AllOS\Listas\UnDmkListas.pas',
  UnPerfJan_Tabs in '..\..\..\..\AllOS\Listas\UnPerfJan_Tabs.pas',
  UnBugs_Tabs in '..\..\..\..\Aplicativos\Bugstrol\UnBugs_Tabs.pas',
  UnBugMbl_Tabs in '..\..\..\..\Aplicativos\Bugstrol\UnBugMbl_Tabs.pas',
  UnDmkDevice in '..\..\..\..\AllOS\Devices\UnDmkDevice.pas',
  UnitMD5 in '..\..\..\..\..\Delphi\Outros\Encrypt\UnitMD5.pas',
  UnProjGroup_Consts in '..\..\..\..\Aplicativos\Bugstrol\UnProjGroup_Consts.pas',
  UnProjGroup_Vars in '..\..\..\..\Aplicativos\Bugstrol\UnProjGroup_Vars.pas',
  UnDmkDAC in '..\..\..\..\AllOS\RDBMs\UnDmkDAC.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  Application.CreateForm(TDmModApp, DmModApp);
  Application.CreateForm(TFmUsuarioAdd, FmUsuarioAdd);
  Application.CreateForm(TFmSincroDown, FmSincroDown);
  Application.CreateForm(TDmModGerl, DmModGerl);
  Application.Run;
end.