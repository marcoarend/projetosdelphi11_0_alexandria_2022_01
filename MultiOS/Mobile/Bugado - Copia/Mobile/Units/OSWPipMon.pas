unit OSWPipMon;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls, FMX.ListBox, FMX.Layouts, FMX.Colors, FMX.TabControl, FMX.Ani,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  UnGeral;

type
  TFmOSWPipMon = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    ColorBox1: TColorBox;
    Panel1: TPanel;
    LBPIPs: TListBox;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem14: TListBoxItem;
    ListBoxItem16: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    ListBoxItem17: TListBoxItem;
    ListBoxItem18: TListBoxItem;
    ListBoxItem19: TListBoxItem;
    ListBoxItem20: TListBoxItem;
    ListBoxHeader1: TListBoxHeader;
    Label6: TLabel;
    SearchBox1: TSearchBox;
    BitmapAnimation1: TBitmapAnimation;
    BitmapAnimation2: TBitmapAnimation;
    QrOSWPipMon: TFDQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ListBoxItemClick(Sender: TObject);
  public
    { Public declarations }
    FOSWCab: Integer;
    procedure AtualizaPIPs();
  end;

var
  FmOSWPipMon: TFmOSWPipMon;

implementation

uses UnDmkRDBMs, ModApp;

{$R *.fmx}

procedure TFmOSWPipMon.AtualizaPIPs();
var
  LHeader : TListBoxGroupHeader;
  LItem : TListBoxItem;
  I: integer;
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOSWPipMon, DmModApp.FDAppDB, [
  'SELECT * FROM oswpipmon ',
  'WHERE Codigo=' + Geral.FF0(FOSWCab),
  '']);
  //
  LbPIPs.BeginUpdate;
  LbPIPs.Items.Clear;
  //
  LHeader := TListBoxGroupHeader.Create(Owner);
  LHeader.Parent := LbPIPs;
  LHeader.Text := 'PIPs';
  LHeader.Height := LHeader.Height * 2;
  //
  QrOSWPipMon.First;
  while not QrOSWPipMon.Eof do
  begin
    LItem := TListBoxItem.Create(Owner);
    LItem.StyleLookup := 'listboxitembottomdetail';
    LItem.Parent := LBPIPs;
    LItem.Text := QrOSWPipMon.FieldByName('NO_PIP').AsString;
    LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aDetail;
    LItem.ItemData.Detail := QrOSWPipMon.FieldByName('NO_PrgLstCab').AsString;
    //LItem.Data := QrOSWPipMon.FieldByName('NO_PrgLstCab').AsString;
    LItem.OnClick := ListBoxItemClick;
    LItem.Height := LItem.Height * 2;
    LItem.Font.Size := LItem.Font.Size * 2;
    //
    QrOSWPipMon.Next;
  end;
  LbPIPs.EndUpdate;
end;

procedure TFmOSWPipMon.FormCreate(Sender: TObject);
(*
var
  c: Char;
  i: Integer;
  Buffer: String;
  ListBoxItem : TListBoxItem;
  ListBoxGroupHeader : TListBoxGroupHeader;
*)
begin
  //AtualizaPIPs();
{
  ListBox1.BeginUpdate;
  for c := 'a' to 'z' do
  begin
    // Add header ('A' to 'Z') to the List
    ListBoxGroupHeader := TListBoxGroupHeader.Create(ListBox1);
    ListBoxGroupHeader.Text := UpperCase(c);
    ListBox1.AddObject(ListBoxGroupHeader);

    // Add items ('a', 'aa', 'aaa', 'b', 'bb', 'bbb', 'c', ...) to the list
    for i := 1 to 3 do
    begin
      // StringOfChar returns a string with a specified number of repeating characters.
      Buffer := StringOfChar(c, i);
      // Simply add item
      // ListBox1.Items.Add(Buffer);

      // or, you can add items by creating an instance of TListBoxItem by yourself
      ListBoxItem := TListBoxItem.Create(ListBox1);
      ListBoxItem.Text := Buffer;
      // (aNone=0, aMore=1, aDetail=2, aCheckmark=3)
      ListBoxItem.ItemData.Accessory := TListBoxItemData.TAccessory(i);
      ListBox1.AddObject(ListBoxItem);
    end;
  end;
  ListBox1.EndUpdate
}
end;

procedure TFmOSWPipMon.FormShow(Sender: TObject);
begin
  //AtualizaPIPs();
end;

procedure TFmOSWPipMon.ListBoxItemClick(Sender: TObject);
begin
  Geral.MB_Info('ListBoxItemClick(' + TListBoxItem(Sender).Name);
end;

end.
