unit DmkBugMoon;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  FMX.ActnList, FMX.StdCtrls, FMX.Edit, System.Actions, FMX.Effects,
  FMX.Filter.Effects,
  FireDAC.Comp.Client,
  UnGeral, UnGrl_Vars
  (*, FireDAC.Stan.Intf, FireDAC.Phys, FireDAC.Phys.MySQL*);

type
  TFmDmkBugMoon = class(TForm)
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    BottomToolBar: TToolBar;
    BtVerifyDB: TButton;
    TopToolBar: TToolBar;
    Panel1: TPanel;
    BtUsuarioAdd: TButton;
    Panel2: TPanel;
    Label1: TLabel;
    EdUsuario: TEdit;
    Label2: TLabel;
    EdSenha: TEdit;
    BtLogin: TButton;
    PaperSketchEffect1: TPaperSketchEffect;
    StyleBook1: TStyleBook;
    ToolBarLabel: TLabel;
    BtTeste: TButton;
    procedure BtVerifyDBClick(Sender: TObject);
    procedure BtLoginClick(Sender: TObject);
    procedure BtUsuarioAddClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { Private declarations }
    procedure TesteMySQL();
  public
    { Public declarations }
  end;

var
  FmDmkBugMoon: TFmDmkBugMoon;

implementation

uses VerifyDB, ModApp, UsuarioAdd, UnDmkRDBMs, SincroDown, UnApp_Vars,
  UnDmkDevice, OSCabLista, Principal;

{$R *.fmx}

procedure TFmDmkBugMoon.BtVerifyDBClick(Sender: TObject);
begin
  FmVerifyDB.Show;
end;

procedure TFmDmkBugMoon.FormHide(Sender: TObject);
begin
  FmPrincipal.TabControl1.TabIndex := 1;
  FmPrincipal.TabControl1.ActiveTab := FmPrincipal.TabItem2;
  FmPrincipal.Show;
end;

procedure TFmDmkBugMoon.TesteMySQL;
var
  Connection:  TFDConnection;
begin
  Connection := TFDConnection.Create(Self);
  try
{
    Connection.Options.Direct := True;
    Connection.Server := 'server';
    Connection.Port := 3306;
    Connection.Database := 'database_name';
    Connection.Username := 'user_name';
    Connection.Password := 'password';
    Connection.Connect;
}
    Connection.Params.Clear;
    Connection.Params.Add('DriverID=MySQL');
    Connection.Params.Add('Server=127.0.0.1');
    Connection.Params.Add('Database=mysql');
    Connection.Params.Add('Port=3306');
    Connection.Params.Add('CharacterSet=latin1_swedish_ci');
    Connection.Params.Add('User_Name=root');
    Connection.Params.Add('Password=wkljweryhvbirt');
    Connection.Connected := True;

  finally
    Connection.Free;
  end;
end;

procedure TFmDmkBugMoon.BtLoginClick(Sender: TObject);
var
  Qry: TFDQuery;
  Codigo: Integer;
  Token: String;
begin
  Qry := TFDQuery.Create(DmModApp.FDAppDB);
  try
    DmkRDBMs.AbreSQLQuery0(Self, Qry, DmModApp.FDAppDB, [
    'SELECT * FROM usuarios ',
    'WHERE Username=''' + EdUsuario.Text + '''',
    'AND PassWord=''' + EdSenha.Text + '''',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      Token  := Qry.FieldByName('Token').AsString;
      //
      VAR_WEB_USER_ID   := Codigo;
      VAR_WEB_TOKEN_DMK := Token;
      VAR_AGENTE_LOGADO := 1339; // Provisorio
      Geral.MB_Aviso('Agente provisório: ' + Geral.FF0(VAR_AGENTE_LOGADO));
//////////// T E S T E S   A Q U I /////////////////////////////////////////////
      //FmOSCabLista.Show;
      Hide;
////////////////////////////////////////////////////////////////////////////////
      //
      BtTeste.Enabled := True;
    end else
      Geral.MB_Aviso('Login incorreto!')
  finally
    Qry.Free;
  end;
end;

procedure TFmDmkBugMoon.BtUsuarioAddClick(Sender: TObject);
begin
  FmUsuarioAdd.Show;
end;

end.
