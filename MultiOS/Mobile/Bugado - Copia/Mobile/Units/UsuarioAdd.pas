unit UsuarioAdd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.StdCtrls, FMX.Edit, FMX.Layouts, FMX.Memo,
  FireDAC.Comp.Client,
  UnGeral, Xml.xmldom, Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;

type
  TFmUsuarioAdd = class(TForm)
    IdHTTP1: TIdHTTP;
    XMLDocument1: TXMLDocument;
    Panel1: TPanel;
    Memo1: TMemo;
    Panel2: TPanel;
    Label1: TLabel;
    EdUsuario: TEdit;
    Label2: TLabel;
    EdSenha: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmUsuarioAdd: TFmUsuarioAdd;

implementation

uses UnDmkRDBMs, ModApp, UnDmkEnums, UnWebReq;

{$R *.fmx}

procedure TFmUsuarioAdd.Button1Click(Sender: TObject);
var
  ResMsgCod: String;
  Node : IXMLNode;
  //
  Codigo, Entidade: Integer;
  UserName, PassWord, Token: String;
  //
  Resp: TStringStream;
  Resposta: String;
begin
  Resp := TStringStream.Create('');
  try
    UserName := EdUsuario.Text;
    PassWord := EdSenha.Text;
    Memo1.Text := Resp.DataString;
    Resposta := WebReq.SQLArrPOST_TxtStr(Self, 'REST_LoginUser', [
    'Usuario=' + UserName,
    'Senha=' + PassWord]);
    Memo1.Text := Resp.DataString;
    //
    XMLDocument1.LoadFromXML(Resposta);
    Node := XMLDocument1.DocumentElement.ChildNodes.FindNode('usuario');
    if Node <> nil then
    begin
      ResMsgCod := Node.ChildNodes['MsgCod'].Text;
      //
      if (ResMsgCod <> '') and (ResMsgCod = '100') then
      begin
        Codigo   := Geral.IMV(Trim(Node.ChildNodes['UsrCod'].Text));
        Entidade := 0;
        Token    := Trim(Node.ChildNodes['UsrAPI'].Text);
        //
        DmkRDBMs.SQLDelSimple(Self, dbsrcLocalServer, 'usuarios', [
          'Codigo'], [
          Codigo], '');
        //
        if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stIns, 'usuarios', False, [
          'Entidade', 'Token'(*UsrAPI*)], [
          'Codigo'(*srCod*), 'UserName', 'PassWord'
        ], [
          Entidade, Token
        ], [
          Codigo, UserName, PassWord
        ], True) then
          Geral.MB_Info('Usu�rio criado com sucesso!');
      end;
    end;
    Hide;
  finally
    Resp.Free;
  end;
end;

end.

