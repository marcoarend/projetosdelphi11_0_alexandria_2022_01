unit SincroDown;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Memo,
  FireDAC.Comp.Client,
  Xml.xmldom, Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc,
  UnGrl_Vars, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  UnDmkEnums, UnProjGroup_Consts, UnDmkDAC, UnMyObjects;

type
  TValArray = array of Variant;
  TFmSincroDown = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    Panel2: TPanel;
    Button1: TButton;
    XMLDocument1: TXMLDocument;
    QrAny: TFDQuery;
    Panel3: TPanel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB0: TProgressBar;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FSQLOSCab: String;
    FDtaSincro_Int: Integer;
    FDtaSincro_Txt: String;
    //
    function  AtualizaTabelaLocal(Tabela: String; DelFlds:
              array of String; DelVals: array of Variant;
              TextoSQL: String): Boolean;
    function  AtualizaTabelaLocal_1Int(Tabela, Campo: String): String;
    function  AtualizaTabelaLocal_2Int(Tabela: String; OSCab: Integer): String;
    function  BaixaOSsNovasEAlteradas(): Boolean;
    function  EvolucaoAtualizada(Codigo, Evolucao: Integer): Boolean;
    //
(*
    function AtualizaTabelaFixa_GraGruMoW(): Boolean;
    function AtualizaTabelaFixa_PrgCadPrg(): Boolean;
    function AtualizaTabelaFixa_PrgLst(): Boolean;
*)

    //procedure IncluiRegistroArray(Tabela, Campos, Valores);

  public
    { Public declarations }
  end;

var
  FmSincroDown: TFmSincroDown;

implementation

uses UnMyListas, UnWebReq, UnGeral, UnDmkRDBMs, ModApp, ModGerl, UnApp_Vars,
UnProjGroup_Vars;

{$R *.fmx}

(*
function TFmSincroDown.AtualizaTabelaFixa_GraGruMoW: Boolean;
var
  SQL, InFld: String;
begin
  SQL := 'SELECT GraGruX FROM gragrumow ';
  DmkRDBMs.AbreSQLQuery0(Self, QrAny, DmModApp.FDAppDB, [
  SQL,
  '']);
  //
  InFld := '';
  QrAny.First;
  while not QrAny.EOF do
  begin
    InFld := InFld + ', ' + Geral.FF0(QrAny.FieldByName('GraGruX').AsInteger);
    //
    QrAny.Next;
  end;
  SQL := 'SELECT * FROM gragrumow ';
  if Length(InFld) > 0 then
  begin
    InFld := InFld.Substring(2);
    SQL := SQL + sLineBreak + 'WHERE GraGruX NOT IN(' + InFld + ')';
    SQL := SQL + sLineBreak + 'OR DataAlt >= ' + QuotedStr(FDtaSincro_Txt);
  end;
  //
  AtualizaTabelaLocal('GragruMoW', ['GraGruX'], [null], SQL);
end;
*)

(*
function TFmSincroDown.AtualizaTabelaFixa_PrgCadPrg: Boolean;
var
  SQL, InFld: String;
begin
  SQL := 'SELECT Codigo FROM prgcadprg ';
  DmkRDBMs.AbreSQLQuery0(Self, QrAny, DmModApp.FDAppDB, [
  SQL,
  '']);
  //
  InFld := '';
  QrAny.First;
  while not QrAny.EOF do
  begin
    InFld := InFld + ', ' + Geral.FF0(QrAny.FieldByName('Codigo').AsInteger);
    //
    QrAny.Next;
  end;
  SQL := 'SELECT * FROM prgcadprg ';
  if Length(InFld) > 0 then
  begin
    InFld := InFld.Substring(2);
    SQL := SQL + sLineBreak + 'WHERE Codigo NOT IN(' + InFld + ')';
    SQL := SQL + sLineBreak + 'OR DataAlt >= ' + QuotedStr(FDtaSincro_Txt);
  end;
  //
  AtualizaTabelaLocal('PrgCadPrg', ['Codigo'], [null], SQL);
end;
*)

(*
function TFmSincroDown.AtualizaTabelaFixa_PrgLst: Boolean;
begin
/
end;
*)

function TFmSincroDown.AtualizaTabelaLocal(Tabela: String; DelFlds:
array of String; DelVals: array of Variant; TextoSQL: String): Boolean;
const
  SQLExtra = '';
{
var
  //XMLDados: String;
  ArrXMLSQLins: TArrXMLSQLins;
  Registros: Integer;
  Txt: String;
}
begin
  Result := DmkDAC.AtualizaTabelaLocal(Tabela, Self, Memo1, PB2, DelFlds,
    DelVals, TextoSQL, SQLExtra);
  MyObjects.IncProgress(PB1);
end;

function TFmSincroDown.BaixaOSsNovasEAlteradas(): Boolean;
const
  SQLExtra = '';
var
  TabOSCab: TTabIDInt;
  TabOSCabCodigo: TFldIDInt;
  //
  I: Integer;
  //TextoSQL: String;
  //
  //SQL, InFld: String;
  //Qry: TFDQuery;
  Evolucao, Codigo: Integer;
  //
begin
  {
  TextoSQL := Geral.ATS([
  FSQLOSCab,
  '']);
  }
  Result := False;
  if DmkDAC.SQLArrRec_IDInt(Self, FSQLOSCab, TabOSCab) then
  try
    for I := 0 to TabOSCab.Count - 1 do
    begin
      MyObjects.IncProgress(PB0);
      PB1.Value := 0;
      PB1.Max := 5; // Quantidade de tabelas para cada OS!
      TabOSCabCodigo := TabOSCab.Items[I];
      Codigo := TabOSCabCodigo.Value;
      //
      //Geral.MB_Info('Codigo = ' + Geral.FF0(Codigo));

      Evolucao := CO_OSWCAB_EVOLUCAO_02000_COD_DOWN_CLOUD_MOBLE_INI;
      if EvolucaoAtualizada(Codigo, Evolucao) then
      begin
        (*Evolucao := CO_OSWCAB_EVOLUCAO_02000_COD_DOWN_CLOUD_MOBLE_INI;
        if not UnDmkRDBMs.SQLInsUpd(Self, dbsrc: TDmkDBSrc; SQLType: TSQLType;
        Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
        ValCampos, ValIndex: array of Variant; UserDataAlterweb: Boolean;
        ComplUpd: String; InfoSQLOnError, InfoErro: Boolean): Boolean;
        *)
        AtualizaTabelaLocal_2Int('OSPipItsPr', Codigo);
        AtualizaTabelaLocal_2Int('OSPipIts', Codigo);
        AtualizaTabelaLocal_2Int('OSAge', Codigo);
        AtualizaTabelaLocal_2Int('OSWPipMon', Codigo);
        //
        AtualizaTabelaLocal_2Int('OSWCab', Codigo);
        //
        Evolucao := CO_OSWCAB_EVOLUCAO_03000_COD_DOWN_CLOUD_MOBLE_FIM;
        EvolucaoAtualizada(Codigo, Evolucao);
        DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, 'OSWCab', False, [
        'Evolucao'], [], [
        Evolucao], [], True);
        //
      end;
    end;
    Result := True;
  finally
    DmkDAC.FreeRec(TabOSCab);
  end;
end;

procedure TFmSincroDown.Button1Click(Sender: TObject);
var
  DtaSincro: String;
  TabOSCab: TTabIDInt;
  //TabOSCabCodigo: TFldIDInt;
  //
begin
  Geral.MB_Aviso('SQL de OSCab pegando todas OSs!');

  FSQLOSCab := Geral.ATS([
  'SELECT osc.Codigo ',
  'FROM oswcab osc ',
{
  'LEFT JOIN osage age ON age.Codigo=osc.Codigo ',
  'LEFT JOIN oswcabdown ocd ON ocd.Codigo=osc.Codigo ',
  'WHERE age.Agente=' + Geral.FF0(VAR_AGENTE_LOGADO),
  'AND ocd.Codigo IS NULL ',
  'OR (ocd.LstUplWeb < osc.LstUplWeb) ',
}
  '']);
  //
  PB0.Value := 0;
  PB1.Value := 0;
  PB1.Max := 5; // Quantidade de tabelas de cadastros simples
  PB2.Max := 0;
  PB2.Value := 0;
  if DmkDAC.SQLArrRec_IDInt(Self, FSQLOSCab, TabOSCab) then
  try
    PB0.Max := 2 + TabOSCab.Count; // Tabelas Simples + OSs + Upd final
  finally
    DmkDAC.FreeRec(TabOSCab);
  end;
  //
  Memo1.Lines.Clear;
  //obter DtaSincro
  FDtaSincro_Int := Trunc(DmModApp.TbControleDtaSincro.Value);
  FDtaSincro_Txt := Geral.FDT(FDtaSincro_Int, 1);
  //
  AtualizaTabelaLocal_1Int('GraGruMoW', 'GraGruX');
  AtualizaTabelaLocal_1Int('PrgAtrIts', 'Controle');
  AtualizaTabelaLocal_1Int('PrgBinCad', 'Codigo');
  AtualizaTabelaLocal_1Int('PrgCadPrg', 'Codigo');
  AtualizaTabelaLocal_1Int('PrgLstIts', 'Controle');
  MyObjects.IncProgress(PB0);
  //
  BaixaOSsNovasEAlteradas();
  //
  DtaSincro := Geral.FDT(DmModGerl.ObtemAgora(), 109);
  if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, 'Controle', False, [
  'DtaSincro'], [], [
  DtaSincro], [], True) then
    DmModApp.ReopenControle();
    //
  MyObjects.IncProgress(PB0);
  //
  Geral.MB_Info('Sincronização de download concluída com sucesso!');
end;

function TFmSincroDown.EvolucaoAtualizada(Codigo, Evolucao: Integer): Boolean;
var
{
  TabOSCab: TTabIDInt;
  TabOSCabEvolucao: TFldIDInt;
}
  TextoSQL: String;
  //

begin
  //Result := False;
  //
  TextoSQL := Geral.ATS([
  'UPDATE oswcab ',
  'SET Evolucao=' + Geral.FF0(Evolucao),
  'WHERE Codigo=' + Geral.FF0(Codigo),
{
  ';',
  'SELECT Evolucao ',
  'FROM oswcab ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
}
  '']);
  Result := DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, TextoSQL) >= 0;
{
  if DmkDAC.SQLArrRec_IDInt(Self, TextoSQL, TabOSCab) then
  try
    try
      TabOSCabEvolucao := TabOSCab.Items[0];
      Result := TabOSCabEvolucao.Value = Evolucao;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('ERRO ao obter evolução de download:' + sLineBreak +
        E.Message);
      end;
    end;
  finally
    DmkDAC.FreeRec(TabOSCab);
  end;
}
end;

procedure TFmSincroDown.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Clear;
end;

function TFmSincroDown.AtualizaTabelaLocal_1Int(Tabela, Campo: String(*;
  Valor: Integer*)): String;
var
  SQL, InFld, Tab: String;
begin
  Tab := LowerCase(Tabela);
  //
  SQL := 'SELECT ' + Campo + ' FROM ' + LowerCase(Tab);
  DmkRDBMs.AbreSQLQuery0(Self, QrAny, DmModApp.FDAppDB, [
  SQL,
  '']);
  //
  InFld := '';
  QrAny.First;
  while not QrAny.EOF do
  begin
    InFld := InFld + ', ' + Geral.FF0(QrAny.FieldByName(Campo).AsInteger);
    //
    QrAny.Next;
  end;
  SQL := 'SELECT * FROM ' + Tab;
  if Length(InFld) > 0 then
  begin
    InFld := InFld.Substring(2);
    SQL := SQL + sLineBreak + 'WHERE ' + Campo + ' NOT IN(' + InFld + ')';
    SQL := SQL + sLineBreak + 'OR DataAlt >= ' + QuotedStr(FDtaSincro_Txt);
  end;
  //
  AtualizaTabelaLocal(Tab, [Campo], [null], SQL);
end;

function TFmSincroDown.AtualizaTabelaLocal_2Int(Tabela: String; OSCab: Integer): String;
var
  SQL, Tab: String;
begin
  Tab := LowerCase(Tabela);
  //
  SQL := Geral.ATS(['SELECT * FROM ' + Tab,
  'WHERE Codigo=' + Geral.FF0(OSCab),
  '']);
  //
  DmkRDBMs.SQLDelSimple(Self, dbsrcLocalServer, Tabela, [
  'Codigo'], [OSCab], '');
  //
  AtualizaTabelaLocal(Tab, [], [], SQL);
end;

end.
