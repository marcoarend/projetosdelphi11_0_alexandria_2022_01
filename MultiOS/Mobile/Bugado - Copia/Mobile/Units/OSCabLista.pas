// Demonstrates the "MultiDetailListItem" TListView appearance.
// Install the SampleListViewMultiDetailApparancePackage before opening this form.
unit OSCabLista;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox,
  FMX.TabControl, FMX.Objects, MultiDetailAppearanceU, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  UnGeral, UnMyObjects, UnDmkEnums;

type
  TFmOSCabLista = class(TForm)
    ToolBar1: TToolBar;
    ToggleEditMode: TSpeedButton;
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    SpeedButtonLiveBindings: TSpeedButton;
    ToolBar2: TToolBar;
    SpeedButtonFill: TSpeedButton;
    ImageRAD: TImage;
    QrOSWCab: TFDQuery;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonFillClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
  private
    { Private declarations }
    procedure RefreshListaOSs();
  public
    { Public declarations }
  end;

var
  FmOSCabLista: TFmOSCabLista;

implementation

uses UnDmkRDBMs, ModApp, OSWPipMon;

{$R *.fmx}

procedure TFmOSCabLista.FormShow(Sender: TObject);
begin
  RefreshListaOSs();
end;

procedure TFmOSCabLista.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  MyObjects.ShowModal(TFmOSWPipMon, FmOSWPipMon, afmoNegarComAviso);
  FmOSWPipMon.FOSWCab := Geral.IMV(Trim(AItem.Text.Substring(2)));
  FmOSWPipMon.AtualizaPIPs();
end;

procedure TFmOSCabLista.RefreshListaOSs;
var
  I: Integer;
  LItem: TListViewItem;
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOSWCab, DmModApp.FDAppDB, [
  'SELECT * FROM oswcab',
  '']);
  //
  ListViewMultiDetail.BeginUpdate;
  ListViewMultiDetail.Items.Clear;
  //
  try
    QrOSWCab.First;
    while not QrOSWCab.Eof do
    begin
      LItem := ListViewMultiDetail.Items.Add;
      LItem.Text := 'OS ' + Geral.FF0(QrOSWCab.FieldByName('Codigo').AsInteger);//Format('Text %d', [I]);
      // Update data managed by custom appearance
      LItem.Data[TMultiDetailAppearanceNames.Detail1] := QrOSWCab.FieldByName('NO_ENT').AsString;
      LItem.Data[TMultiDetailAppearanceNames.Detail2] := QrOSWCab.FieldByName('NO_SiapTerCad').AsString;
      LItem.Data[TMultiDetailAppearanceNames.Detail3] := QrOSWCab.FieldByName('NO_ENTICONTAT').AsString;
      LItem.BitmapRef := ImageRAD.Bitmap;
      //
      QrOSWCab.Next;
    end;
  finally
    ListViewMultiDetail.EndUpdate;
  end;
end;

procedure TFmOSCabLista.SpeedButtonFillClick(Sender: TObject);
begin
  RefreshListaOSs();
end;

procedure TFmOSCabLista.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmOSCabLista.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;

end.
