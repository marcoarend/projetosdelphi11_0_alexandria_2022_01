unit UnApp_Vars;

interface

uses UnDmkEnums, UnGrl_Vars, UnGeral;

type
  TUnApp_Vars = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  DefineValDeVarsGeraisDoApp(): String;
  end;


var
  App_Vars: TUnApp_Vars;
  //
  VAR_AGENTE_LOGADO: Integer;

implementation

{ TUnApp_Vars }

function TUnApp_Vars.DefineValDeVarsGeraisDoApp: String;
begin
  Geral.DefineFormatacoes();
  //
  VAR_APP_DB_NAME_ONLY := 'bugado';
  TMeuDB := VAR_APP_DB_NAME_ONLY;  ????
  VAR_APP_DB_NAME_E_EXT := 'bugado.s3db';
  VAR_APP_DB_FULLPATH := Geral.DirAppAllOS(VAR_APP_DB_NAME_E_EXT);
  VAR_LIBERA_TODOS_FORMS := False;

end;

end.
