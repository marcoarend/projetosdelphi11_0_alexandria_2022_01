﻿unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  System.Actions, FMX.ActnList, FMX.StdCtrls, FMX.Colors,
  UnApp_Vars, UnGeral, UnDmkDevice, UnMyObjects, UnDMkEnums;

type
  TFmPrincipal = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    Timer1: TTimer;
    LaMsgIni: TLabel;
    Panel1: TPanel;
    ColorBox3: TColorBox;
    BtSincroDown: TButton;
    Label3: TLabel;
    Label2: TLabel;
    BtOSWCab: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure BtSincroDownClick(Sender: TObject);
    procedure BtOSWCabClick(Sender: TObject);
  private
    { Private declarations }
    FIniciadoApp: Boolean;
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses DmkBugMoon, VerifyDB, ModApp, UsuarioAdd, SincroDown, ModGerl, OSCabLista,
 OSWPipMon;


{$R *.fmx}
procedure TFmPrincipal.BtOSWCabClick(Sender: TObject);
{
var
  dlg: TFmOSCabLista;
begin
 // FmOSCabLista.Show;
  dlg := TFmOSCabLista.Create(nil);
  // select current value, if avaialble in the list
  //dlg.ListBox1.ItemIndex := dlg.ListBox1.Items.IndexOf(Edit1.Text);
  dlg.ShowModal(
    procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      // if OK was pressed and an item is selected, pick it
        //if dlg.ListBox1.ItemIndex >= 0 then
          //edit1.Text := dlg.ListBox1.Items [dlg.ListBox1.ItemIndex];
      dlg.DisposeOf;
    end);
}
begin
  MyObjects.ShowModal(TFmOSCabLista, FmOSCabLista, afmoNegarComAviso)
end;

procedure TFmPrincipal.BtSincroDownClick(Sender: TObject);
begin
  FmSincroDown.Show;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  TabControl1.ActiveTab := TabItem1;
  App_Vars.DefineValDeVarsGeraisDoApp();
  Geral.MB_Info(DmkDevice.ObtemIMEI());
end;

procedure TFmPrincipal.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkMenu then
    //Geral.MB_Info('Teste');
    FmOSCabLista.Show;
{
  if Key = vkHardwareBack then
  begin
    if TabControl1.ActiveTab = TabItem2 then
    begin
      ChangeTabAction1.Tab := TabItem1;
      ChangeTabAction1.ExecuteTarget(Self);
      ChangeTabAction1.Tab := TabItem2;
      Key := 0;
    end;
  end;
}
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  if not FIniciadoApp then
    Timer1.Enabled := True;
end;

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
begin
  FmOSCabLista.Show;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
  procedure Info(Msg: String);
  begin
    MyObjects.Informa(LaMsgIni, True, Msg);
  end;
begin
  Timer1.Enabled := False;
  //
  Info('Criando form 1/10');
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  Info('Criando form 2/10');
  Application.CreateForm(TDmModApp, DmModApp);
  Info('Criando form 3/10');
  Application.CreateForm(TFmUsuarioAdd, FmUsuarioAdd);
  Info('Criando form 4/10');
  Application.CreateForm(TFmSincroDown, FmSincroDown);
  Info('Criando form 5/10');
  Application.CreateForm(TDmModGerl, DmModGerl);
  Info('Criando form 6/10');
  //Application.CreateForm(TFmOSCabLista, FmOSCabLista);
  Info('Criando form 7/10');
  //Application.CreateForm(TFmOSWPipMon, FmOSWPipMon);
  //
  FIniciadoApp := True;
  Hide;
  FmDmkBugMoon.Show;
end;

end.
