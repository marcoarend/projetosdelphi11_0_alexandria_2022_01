unit UnMyListas;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnMyLinguas, UnDmkEnums;

type
  TUnMyListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
              var TemControle: TTemControle): Boolean;
    function  CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices:
              TList<TIndices>): Boolean;
    function  CriaListaTabelas(DBMS: TdmkDBMSs; Lista: TList<TTabelas>): Boolean;
    function  CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
  end;

const
  CO_VERSAO = 1310021029;
  CO_DMKID_APP = 33;

var
  MyListas: TUnMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;

implementation

uses UnGrl_Tabs, UnBugs_Tabs, UnBugMbl_Tabs, UnPerfJan_Tabs;

{ TUnMyListas }

function TUnMyListas.CriaListaTabelas(DBMS: TdmkDBMSs;
  Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    Grl_Tabs.CarregaListaTabelas(Lista);
    BugMbl_Tabs.CarregaListaTabelas(Lista);
    Bugs_Tabs.CarregaListaTabelas(Lista);
    PerfJan_Tabs.CarregaListaTabelas(Lista);
    //
    //MyLinguas.AdTbLst(Lista, False, LowerCase('GraGruMoW'), '');
    //
  except
    raise;
    Result := False;
  end;
end;

function TUnMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaBase) = Uppercase('gragrumow') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'GraGruX';
      FLIndices.Add(FRIndices);
      //
    end else
*)
    begin
      Grl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      BugMbl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bugs_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  BugMbl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bugs_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;

function TUnMyListas.CriaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    BugMbl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Bugs_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
  except
    raise;
    Result := False;
  end;
end;

function TUnMyListas.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    BugMbl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bugs_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TUnMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  try
(*
    if Uppercase(Tabela) = Uppercase('gragrumow') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGru1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
*)
    begin
      Grl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      BugMbl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bugs_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    Grl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    BugMbl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bugs_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

end.
