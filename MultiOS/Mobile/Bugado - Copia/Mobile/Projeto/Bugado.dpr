program Bugado;

uses
  System.StartUpCopy,
  FMX.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  VerifyDB in '..\Units\VerifyDB.pas' {FmVerifyDB},
  UnDmkEnums in '..\..\..\Geral\UnDmkEnums.pas',
  UnDmkSQL_PF in '..\..\..\Geral\UnDmkSQL_PF.pas',
  UnGeral in '..\..\..\Geral\UnGeral.pas',
  UnGrlListas in '..\..\..\Geral\UnGrlListas.pas',
  UnMyObjects in '..\..\..\Geral\UnMyObjects.pas',
  UnDBCheck in '..\..\..\Geral\UnDBCheck.pas',
  ModApp in '..\Units\ModApp.pas' {DmModApp},
  UsuarioAdd in '..\Units\UsuarioAdd.pas' {FmUsuarioAdd},
  BugMbl_Tabs in '..\..\..\..\AllOS\Bugstrol\BugMbl_Tabs.pas',
  UnGeral_Tabs in '..\..\..\Geral\UnGeral_Tabs.pas',
  TesteSQL in '..\Units\TesteSQL.pas' {FmTesteSQL},
  UnGeral_Consts in '..\..\..\Geral\UnGeral_Consts.pas',
  UnWebReq in '..\..\..\Geral\UnWebReq.pas',
  UnGeral_Vars in '..\..\..\Geral\UnGeral_Vars.pas',
  UnMyListas in '..\Units\UnMyListas.pas',
  UnApp_Consts in '..\Units\UnApp_Consts.pas',
  SincroDown in '..\Units\SincroDown.pas' {FmSincroDown},
  Bugs_Tabs in '..\..\..\..\Intermedio\Bugs_Tabs.pas',
  dmkListas in '..\..\..\..\AllOS\dmkListas.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  Application.CreateForm(TDmModApp, DmModApp);
  Application.CreateForm(TFmUsuarioAdd, FmUsuarioAdd);
  Application.CreateForm(TFmTesteSQL, FmTesteSQL);
  Application.CreateForm(TFmSincroDown, FmSincroDown);
  Application.Run;
end.
