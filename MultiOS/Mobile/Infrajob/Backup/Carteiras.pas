unit Carteiras;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.Client, Data.DB,
  FireDAC.Comp.DataSet, FMX.Layouts, FMX.ListBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.SearchBox, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, System.Actions,
  FMX.ActnList;

type
  TFmCarteiras = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TbCarteiras: TFDTable;
    QrCarteiras: TFDQuery;
    TbCarteirasCodigo: TIntegerField;
    TbCarteirasNome: TWideMemoField;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbInclui: TSpeedButton;
    Label1: TLabel;
    SbSaida: TSpeedButton;
    SearchBox1: TSearchBox;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    SbVolta: TSpeedButton;
    ListBox3: TListBox;
    ListBoxHeader4: TListBoxHeader;
    SbConfirma: TSpeedButton;
    SbDesiste: TSpeedButton;
    Carteiras: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    CBTIpo: TComboBox;
    EdNome: TEdit;
    ListBoxItem7: TListBoxItem;
    CBPagRec: TComboBox;
    ListBoxItem8: TListBoxItem;
    CkAtivo: TCheckBox;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBITipo: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBIPagRec: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIAtivo: TListBoxItem;
    procedure FormCreate(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  private
    { Private declarations }
    procedure ReopenTbCarteiras;
    procedure ReopenCarteiras(Codigo: Integer);
    function  ObtemTipoCarteiraStr(Tipo: Integer): String;
    function  ObtemPagRecStr(PagRec: Integer): String;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmCarteiras: TFmCarteiras;

implementation

{$R *.fmx}

uses Principal, UnSQLite_PF2;

procedure TFmCarteiras.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenTbCarteiras;
end;

procedure TFmCarteiras.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := USQLite_PF2.GetSelectedValue(ListBox1).ToString.ToInteger();
    ExecuteAction(ChangeTabAction2);
  end;
end;

function TFmCarteiras.ObtemPagRecStr(PagRec: Integer): String;
begin
  case PagRec of
     -1: Result := 'Pagar';
      0: Result := 'Ambos';
      1: Result := 'Receber';
    else Result := 'N�o definido';
  end;
end;

function TFmCarteiras.ObtemTipoCarteiraStr(Tipo: Integer): String;
begin
  if Tipo = 0 then
    Result := 'Caixa'
  else
    Result := 'N�o definido';
end;

procedure TFmCarteiras.ReopenCarteiras(Codigo: Integer);
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrCarteiras, FmPrincipal.MyDB, [
    'SELECT * FROM carteiras WHERE Codigo=' + FormatFloat('0', Codigo),
    '']);
end;

procedure TFmCarteiras.ReopenTbCarteiras;
begin
  TbCarteiras.Close;
  TbCarteiras.Connection := FmPrincipal.MyDB;
  TbCarteiras.Open();
end;

procedure TFmCarteiras.SbConfirmaClick(Sender: TObject);
var
  Codigo, Tipo, PagRec, Ativo: Integer;
  Nome: String;
  SQLTipo: TSQLType;
begin
  Codigo := FCodigo;
  Nome   := EdNome.Text;
  Tipo   := CBTIpo.ItemIndex;
  PagRec := CBPagRec.ItemIndex - 1;
  Ativo  := USQLite_PF2.BoolToInt(CkAtivo.IsChecked);
  //
  if USQLite_PF2.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  if USQLite_PF2.FIC(Tipo < 0, 'Defina o tipo de carteira!') then Exit;
  if USQLite_PF2.FIC(PagRec < -1, 'Defina a caracter�stica!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := USQLite_PF2.ObtemCodigoInt(Self, 'carteiras', 'Codigo',
                 FmPrincipal.QrAux, FmPrincipal.MyDB);
  end else
    SQLTipo := stUpd;
  //
  USQLite_PF2.CarregaSQLInsUpd(FmPrincipal.QrUpd, FmPrincipal.MyDB, SQLTipo,
    'carteiras', False, ['Nome', 'Tipo', 'PagRec', 'Ativo'], ['Codigo'],
    [Nome, Tipo, PagRec, Ativo], [Codigo], True, False, '');
  //
  ReopenTbCarteiras;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmCarteiras.SbExcluiClick(Sender: TObject);
begin
  USQLite_PF2.ExecutaSQLQuery0(Self, FmPrincipal.QrUpd, FmPrincipal.MyDB,
    ['DELETE FROM carteiras WHERE Codigo = ' + FormatFloat('0', FCodigo)]);
  //
  ReopenTbCarteiras;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmCarteiras.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmCarteiras.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmCarteiras.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenCarteiras(FCodigo);
      //
      LBICodigo.Text := FormatFloat('0', QrCarteiras.FieldByName('Codigo').AsInteger);
      LBINome.Text   := QrCarteiras.FieldByName('Nome').AsString;
      LBITipo.Text   := ObtemTipoCarteiraStr(QrCarteiras.FieldByName('Tipo').AsInteger);
      LBIPagRec.Text := ObtemPagRecStr(QrCarteiras.FieldByName('PagRec').AsInteger);
      //
      if QrCarteiras.FieldByName('Ativo').AsInteger = 1 then
        LBIAtivo.Text := 'Ativo'
      else
        LBIAtivo.Text := 'Inativo';
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenCarteiras(FCodigo);
        //
        EdNome.Text        := QrCarteiras.FieldByName('Nome').AsString;
        CBTIpo.ItemIndex   := QrCarteiras.FieldByName('Tipo').AsInteger;
        CBPagRec.ItemIndex := QrCarteiras.FieldByName('PagRec').AsInteger + 1;
        CkAtivo.IsChecked  := USQLite_PF2.IntToBool(QrCarteiras.FieldByName('Ativo').AsInteger);
      end else
      begin
        EdNome.Text        := '';
        CBTIpo.ItemIndex   := 0;
        CBPagRec.ItemIndex := -1;
        CkAtivo.IsChecked  := True;
      end;
    end;
  end;
end;

end.
