unit Contas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.SearchBox, System.Actions, FMX.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TFmContas = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbInclui: TSpeedButton;
    Label1: TLabel;
    SearchBox1: TSearchBox;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    TbContas: TFDTable;
    TbContasCodigo: TIntegerField;
    TbContasNome: TWideMemoField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    QrContas: TFDQuery;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    CkDebito: TCheckBox;
    ListBoxItem3: TListBoxItem;
    CkCredito: TCheckBox;
    ListBoxItem4: TListBoxItem;
    CkAtivo: TCheckBox;
    EdNome: TEdit;
    ClearEditButton1: TClearEditButton;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIDebito: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICredito: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIAtivo: TListBoxItem;
    procedure FormCreate(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  private
    { Private declarations }
    procedure ReopenContas(Codigo: Integer);
    procedure ReopenTbContas;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmContas: TFmContas;

implementation

{$R *.fmx}

uses Principal, UnSQLite_PF2;

procedure TFmContas.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenTbContas;
end;

procedure TFmContas.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := USQLite_PF2.GetSelectedValue(ListBox1).ToString.ToInteger();
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmContas.ReopenContas(Codigo: Integer);
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrContas, FmPrincipal.MyDB, [
    'SELECT * FROM contas WHERE Codigo=' + FormatFloat('0', Codigo),
    '']);
end;

procedure TFmContas.ReopenTbContas;
begin
  TbContas.Close;
  TbContas.Connection := FmPrincipal.MyDB;
  TbContas.Open();
end;

procedure TFmContas.SbConfirmaClick(Sender: TObject);
var
  Codigo, Debito, Credito, Ativo: Integer;
  Nome: String;
  SQLTipo: TSQLType;
begin
  Codigo  := FCodigo;
  Nome    := EdNome.Text;
  Credito := USQLite_PF2.BoolToInt(CkCredito.IsChecked);
  Debito  := USQLite_PF2.BoolToInt(CkDebito.IsChecked);
  Ativo   := USQLite_PF2.BoolToInt(CkAtivo.IsChecked);
  //
  if USQLite_PF2.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := USQLite_PF2.ObtemCodigoInt(Self, 'contas', 'Codigo',
                 FmPrincipal.QrAux, FmPrincipal.MyDB);
  end else
    SQLTipo := stUpd;
  //
  USQLite_PF2.CarregaSQLInsUpd(FmPrincipal.QrUpd, FmPrincipal.MyDB, SQLTipo,
    'contas', False, ['Nome', 'Debito', 'Credito', 'Ativo'], ['Codigo'],
    [Nome, Debito, Credito, Ativo], [Codigo], True, False, '');
  //
  ReopenTbContas;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmContas.SbExcluiClick(Sender: TObject);
begin
  USQLite_PF2.ExecutaSQLQuery0(Self, FmPrincipal.QrUpd, FmPrincipal.MyDB,
    ['DELETE FROM contas WHERE Codigo = ' + FormatFloat('0', FCodigo)]);
  //
  ReopenTbContas;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmContas.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmContas.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmContas.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenContas(FCodigo);
      //
      LBICodigo.Text := FormatFloat('0', QrContas.FieldByName('Codigo').AsInteger);
      LBINome.Text   := QrContas.FieldByName('Nome').AsString;
      //
      if QrContas.FieldByName('Debito').AsInteger = 1 then
        LBIDebito.Text := 'Sim'
      else
        LBIDebito.Text := 'N�o';
      //
      if QrContas.FieldByName('Credito').AsInteger = 1 then
        LBICredito.Text := 'Sim'
      else
        LBICredito.Text := 'N�o';
      //
      //
      if QrContas.FieldByName('Ativo').AsInteger = 1 then
        LBIAtivo.Text := 'Ativo'
      else
        LBIAtivo.Text := 'Inativo';
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenContas(FCodigo);
        //
        EdNome.Text         := QrContas.FieldByName('Nome').AsString;
        CkDebito.IsChecked  := USQLite_PF2.IntToBool(QrContas.FieldByName('Debito').AsInteger);
        CkCredito.IsChecked := USQLite_PF2.IntToBool(QrContas.FieldByName('Credito').AsInteger);
        CkAtivo.IsChecked   := USQLite_PF2.IntToBool(QrContas.FieldByName('Ativo').AsInteger);
      end else
      begin
        EdNome.Text         := '';
        CkDebito.IsChecked  := False;
        CkCredito.IsChecked := False;
        CkAtivo.IsChecked   := True;
      end;
    end;
  end;
end;

end.
