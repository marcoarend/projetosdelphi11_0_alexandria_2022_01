program InfrajobMobile;

uses
  System.StartUpCopy,
  FMX.Forms,
  Carteiras in 'Carteiras.pas' {FmCarteiras},
  Entidades in 'Entidades.pas' {FmEntidades},
  MasterDetail in 'MasterDetail.pas' {MasterDetailForm},
  Principal in 'Principal.pas' {FmPrincipal},
  LctGer in 'LctGer.pas' {FmLctGer},
  Contas in 'Contas.pas' {FmContas},
  UnSQLite_PF2 in 'UnSQLite_PF2.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmContas, FmContas);
  Application.Run;
end.
