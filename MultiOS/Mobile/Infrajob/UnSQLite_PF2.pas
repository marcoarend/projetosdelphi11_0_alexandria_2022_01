//21/10/2014 Unit 100% Compat�vel com o Delphi XE7

unit UnSQLite_PF2;

interface

uses System.SysUtils, System.Classes, System.UITypes, FMX.Memo, FMX.StdCtrls,
  FMX.Forms, FMX.Dialogs, Data.SqlExpr, DB, System.Variants, FireDAC.Comp.Client,
  Data.Bind.Components, System.Rtti, System.Types;

var
  VAR_CADASTRO, VAR_USUARIO: Integer;
  //
  VAR_FORMATDATE, VAR_FORMATDATE2, VAR_FORMATDATE3, VAR_FORMATDATE4,
  VAR_FORMATDATE5, VAR_FORMATTIME, VAR_FORMATTIME2, VAR_FORMATTIME3,
  VAR_FORMATTIME4, VAR_FORMATTIME5,
  VAR_FORMATDATE6, VAR_FORMATDATE0, VAR_FORMATTIMESTAMP4,
  VAR_FORMATDATETIME, VAR_FORMATDATETIME1, VAR_FORMATDATETIME2,
  VAR_FORMATDATETIME3, VAR_FORMATDATETIME4,
  VAR_FORMATDATEi, VAR_FORMATDATE7, VAR_FORMATDATE8,
  VAR_FORMATDATE9, VAR_FORMATDATE10, VAR_FORMATDATE11, VAR_FORMATDATE12,
  VAR_FORMATDATE13, VAR_FORMATDATE14, VAR_FORMATDATE15, VAR_FORMATDATE16,
  VAR_FORMATDATE17, VAR_FORMATDATE18, VAR_FORMATDATE19, VAR_FORMATDATE20,
  VAR_FORMATDATE21, VAR_FORMATDATE22, VAR_FORMATDATE23, VAR_FORMATDATE24,
  VAR_FORMATDATE25, VAR_FORMATDATE26,
  VAR_FORMATDATECB4: String;
  //
  VAR_GETDATAI, VAR_GETDATAF: TDateTime;
  //
  VAR_FORMATFLOAT0, VAR_FORMATFLOAT1, VAR_FORMATFLOAT2, VAR_FORMATFLOAT3,
  VAR_FORMATFLOAT4, VAR_FORMATFLOAT5, VAR_FORMATFLOAT6, VAR_FORMATFLOAT7,
  VAR_FORMATFLOAT8, VAR_FORMATFLOAT9, VAR_FORMATFLOAT10: String;
  VAR_UNIDADEPADRAO: Integer;

type
  TSQLType      = (stIns=0, stUpd=1, stDel=2, stCpy=3, stLok=4, stUnd=5, stPsq=6, stNil=7);
  TUnSQLite_PF2 = class(TObject)
  private
    { Private declarations }
    function  PareceTelefoneBR(Telefone: String): Boolean;
  public
    { Public declarations }
    function  AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
              SQL: array of String): Boolean;
    function  ExecutaSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
              SQL: array of String): Boolean;
    function  IntToBool(Verdade: Integer): Boolean;
    function  BoolToInt(Verdade: Boolean): Integer;
    function  FIC(FaltaInfoCompo: Boolean; Mensagem: String; ExibeMsg:
              Boolean = True): Boolean;
    function  FDT(Data: TDateTime; Tipo: Integer; Nulo: Boolean = False): String;
    function  CarregaSQLInsUpd(QrUpd: TFDQuery; DB: TFDConnection;
              Tipo: TSQLType; Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean; ComplUpd: String): Boolean;
    function  ObtemCodigoInt(Form: TForm; Tabela, Campo: String;
              Query: TFDQuery; DB: TFDConnection): Integer;
    function  GetSelectedValue(AObject: TObject): TValue;
    function  ObtemIndexDeTabela(Query: TFDQuery; CampoCodigo: String; Valor: Integer): Integer;
    function  SoNumero_TT(Texto: String): String;
    function  FormataTelefone_TT_Curto(Telefone: String): String;
    function  ValidaEMail(const Value: String): Boolean;
    function  ValidaDadosCampo(Form: TForm; Query: TFDQuery; DB: TFDConnection;
              Campo, Tabela: String; Valor: Integer;
              PemiteZero: Boolean = False): Boolean;
    function  ValidaDadosCampoConta(Form: TForm; Query: TFDQuery; DB: TFDConnection;
              Valor: Integer; var Credito: Boolean): Boolean;
    procedure DefineFormatacoes();
    // M E N S A G E N S
    procedure MB_Erro(Texto: String);
    procedure MB_Aviso(Texto: String);
    procedure MB_Info(Texto: String);
    // S T R I N G S
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  VariavelToString(Variavel: Variant): String;
    function  WideStringToSQLString(Texto: (*Wide*)String): (*Wide*)String;
    function  VariantToString(AVar: OleVariant): string;
    function  Substitui(Txt, ant, new: string) : string;
  end;


var
  USQLite_PF2: TUnSQLite_PF2;
const
  CO_JOKE_SQL = '$#'; // Texto livre no SQLInsUpd

implementation

{ TUnSQLite_PF2 }

function TUnSQLite_PF2.AbreSQLQuery0(Form: TForm; Query: TFDQuery;
  FD: TFDConnection; SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
  //
begin
  Result := False;
  Query.Close;
  Query.Connection := FD;
  //
  MyCursor := Form.Cursor;
  try
    Form.Cursor := crHourGlass;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      MB_Erro('Texto da SQL indefinido no SQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.Open;
      Result := True;
    end;
    Form.Cursor := MyCursor;
  except
    on e: Exception do
    begin
      MB_Erro('Erro ao tentar abrir uma SQL no SQLQuery!' + sLineBreak +
      TComponent(Query.Owner).Name + '.' + Query.Name + sLineBreak + e.Message +
      sLineBreak + Query.SQL.Text);
      //
      Form.Cursor := MyCursor;
    end;
  end;
end;

function TUnSQLite_PF2.BoolToInt(Verdade: Boolean): Integer;
begin
  if Verdade then Result := 1 else Result := 0;
end;

function TUnSQLite_PF2.CarregaSQLInsUpd(QrUpd: TFDQuery; DB: TFDConnection;
  Tipo: TSQLType; Tabela: String; Auto_increment: Boolean; SQLCampos,
  SQLIndex: array of String; ValCampos, ValIndex: array of Variant;
  UserDataAlterweb, IGNORE: Boolean; ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab, Campos, Valores: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  if (i >= 0) and Auto_increment then
  begin
    MB_Erro('AVISO! Existem ' + IntToStr(i+1) + ' �ndices informados ' +
    'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    MB_Erro('AVISO: O status da a��o est� definida como ' +
    '"' + NomeTipoSQL(Tipo) + '"');
  end;
  Data := #39 + FDT(Date, 1) + #39;
  QrUpd.SQL.Clear;
  QrUpd.Connection := DB;
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' (')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' (');
  end else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  Campos  := '';
  Valores := '';
  j := System.High(SQLCampos);
  for i := System.Low(SQLCampos) to j do
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', ' + SQLCampos[i];
      Valor := VariavelToString(ValCampos[i]);
      Valores := Valores + ', ' + Valor;
    end else
    begin
      if SQLCampos[i] = CO_JOKE_SQL then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
          QrUpd.SQL.Add(ValCampos[i] + ', ')
        else
          QrUpd.SQL.Add(ValCampos[i]);
      end else
      begin
        Valor := VariavelToString(ValCampos[i]);
        if (i < j) or UserDataAlterweb then
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
        else
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
      end;
    end;
  end;

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
      //QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
      Campos  := Campos + ', DataCad, UserCad, AlterWeb';
      Valores := Valores + ', ' + Data + ', ' + IntToStr(VAR_USUARIO) + ', 1';
    end else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else begin

    for i := System.Low(SQLIndex) to System.High(SQLIndex) do
    begin
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        QrUpd.SQL.Add(Liga + ValIndex[i])
      else
      // fim 2011-12-02
      begin
        if Tipo = stIns then
        begin
          Campos  := Campos + ', ' + SQLIndex[i];
          Valor := VariavelToString(ValIndex[i]);
          Valores := Valores + ', ' + Valor;
        end else
        begin
          Valor := VariavelToString(ValIndex[i]);
          QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;
  if Tipo = stIns then
  begin
    Campos  := Campos.Substring(2) + ') VALUES (';
    Valores := Valores.Substring(2) + ')';
    QrUpd.SQL.Add(Campos);
    QrUpd.SQL.Add(Valores);
  end;
  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;
  QrUpd.SQL.Add(';');
  QrUpd.ExecSQL();
  Result := True;
end;

procedure TUnSQLite_PF2.DefineFormatacoes;
begin
  if FormatSettings.ShortDateFormat = 'M/d/yyyy' then
    VAR_FORMATDATEi := 'mm/dd/yy'
  else
    VAR_FORMATDATEi := 'dd/mm/yy';
  VAR_FORMATDATE0 := 'yy/mm/dd';
  VAR_FORMATDATE  := 'yyyy-mm-dd'; // mudei 2011-05-21
  VAR_FORMATDATE2 := 'dd/mm/yyyy';
  VAR_FORMATDATE3 := 'dd/mm/yy'; // para labels, n�o usar para c�lculo!
  VAR_FORMATDATE5 := 'mmm/yy'; // para labels, n�o usar para c�lculo!
  VAR_FORMATDATE4 := 'yyyymmdd';
  VAR_FORMATDATE6 := 'dd "de" mmmm "de" yyyy';
  VAR_FORMATDATE7 := 'mmmm "de" yyyy';
  VAR_FORMATDATE8 := 'dd "de" mmmm "de" yyyy "�s" hh:nn:ss';
  VAR_FORMATDATE9 := 'yyyy/mm/dd hh:nn:ss';
  VAR_FORMATDATE10 := 'mm/dd/YYYY';
  VAR_FORMATDATE11 := 'dd/mm/yyyy hh:nn';
  VAR_FORMATDATE12 := 'dd/mm';
  VAR_FORMATDATE13 := 'yymm'; // Mez
  VAR_FORMATDATE14 := 'mm/yyyy'; // Mes
  VAR_FORMATDATE15 := 'dddd, dd "de" mmmm "de" yyyy'; // Mes
  VAR_FORMATDATE16 := 'dd';
  VAR_FORMATDATE17 := 'mmmm/yy';
  VAR_FORMATDATE18 := 'mmm/yy';
  VAR_FORMATDATE19 := 'mm/yy';
  VAR_FORMATDATE20 := 'yyyymm';
  VAR_FORMATDATE21 := 'yy';
  VAR_FORMATDATE22 := 'mm';
  VAR_FORMATDATE23 := 'ddmmyyyy';
  VAR_FORMATDATE24 := 'mmyyyy';
  VAR_FORMATDATE25 := 'yyyy';
  VAR_FORMATDATE26 := 'yymmdd_hhnnss';

  VAR_FORMATTIMESTAMP4 := 'mm/yy';
  VAR_FORMATTIME := 'hh:nn:ss';
  VAR_FORMATTIME2 := 'hh:nn';
  VAR_FORMATTIME3 := 'hhnn';
  VAR_FORMATTIME4 := 'hh:nn:ss:zzz';
  VAR_FORMATTIME5 := 'hh"h "nn"min "ss "seg"';
  VAR_FORMATDATE15 := 'dddd, dd "de" mmmm "de" yyyy'; // Mes
  VAR_FORMATDATETIME   := 'dd/mm/yyyy hh:nn:ss';
  VAR_FORMATDATETIME1  := 'dd/mm/yy hh:nn:ss';
  VAR_FORMATDATETIME2  := 'yyyy/mm/dd hh:nn:ss';
  VAR_FORMATDATETIME3  := 'dd/mm/yy hh:nn';
  VAR_FORMATDATETIME4  := 'yyyy-mm-dd hh:nn:ss';
  VAR_FORMATDATECB4    := 'YYYYMMDDhh:nn:ss';
  //
  VAR_FORMATFLOAT0 := '#,###,###,##0';
  VAR_FORMATFLOAT1 := '#,###,###,##0.0';
  VAR_FORMATFLOAT2 := '#,###,###,##0.00';
  VAR_FORMATFLOAT3 := '#,###,###,##0.000';
  VAR_FORMATFLOAT4 := '#,###,###,##0.0000';
  VAR_FORMATFLOAT5 := '#,###,###,##0.00000';
  VAR_FORMATFLOAT6 := '#,###,###,##0.000000';
  VAR_FORMATFLOAT7 := '#,###,###,##0.0000000';
  VAR_FORMATFLOAT8 := '#,###,###,##0.00000000';
  VAR_FORMATFLOAT9 := '#,###,###,##0.000000000';
  VAR_FORMATFLOAT10 := '#,###,###,##0.0000000000';
end;

function TUnSQLite_PF2.ExecutaSQLQuery0(Form: TForm; Query: TFDQuery;
  FD: TFDConnection; SQL: array of String): Boolean;
var
  I: Integer;
begin
  Result := False;
  try
    Form.Cursor := crHourGlass;
    Query.Close;
    Query.Connection := FD;
    if High(SQL) > -1 then
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;
    if Query.SQL.Text = '' then
    begin
      MB_Erro('Texto da SQL indefinido no "ExecutaSQLQuery0"!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      //Geral.MB_Info(Query.SQL.Text);
      Query.ExecSQL;
      Result := True;
    end;
    Form.Cursor := crDefault;
  except
    on E: Exception do
    begin
      MB_Erro('Erro ao tentar executar uma SQL no "ExecutaSQLQuery0"!' +
      sLinebreak + E.Message + sLineBreak + Query.SQL.Text);
      //
      Form.Cursor := crDefault;
    end;
  end;
end;

function TUnSQLite_PF2.FDT(Data: TDateTime; Tipo: Integer;
  Nulo: Boolean): String;
var
  Res: String;
begin
  Result := '';
  Res := '';
  if Nulo and (Data< (1/86400)) then Exit; // menos que um segundo
  //Res := FormatDateTime('yyyy/mm/yy hh:nn:ss:zzz', Data);
  case Tipo of
    00: Res := FormatDateTime(VAR_FORMATDATETIME, Data);
    01: Res := FormatDateTime(VAR_FORMATDATE, Data);
    02: Res := FormatDateTime(VAR_FORMATDATE2, Data);
    03: Res := FormatDateTime(VAR_FORMATDATE3, Data);
    04: Res := FormatDateTime(VAR_FORMATDATE4, Data);
    05: Res := FormatDateTime(VAR_FORMATDATE5, Data);
    06: Res := FormatDateTime(VAR_FORMATDATE6, Data);
    07: Res := FormatDateTime(VAR_FORMATDATE7, Data);
    08: Res := FormatDateTime(VAR_FORMATDATE8, Data);
    09: Res := FormatDateTime(VAR_FORMATDATE9, Data);
    10: Res := FormatDateTime(VAR_FORMATDATE10, Data);
    11: Res := FormatDateTime(VAR_FORMATDATE11, Data);
    12: Res := FormatDateTime(VAR_FORMATDATE12, Data);
    13: Res := FormatDateTime(VAR_FORMATDATE13, Data);
    14: Res := FormatDateTime(VAR_FORMATDATE14, Data);
    15: Res := FormatDateTime(VAR_FORMATDATE15, Data);
    16: Res := FormatDateTime(VAR_FORMATDATE16, Data);
    17: Res := FormatDateTime(VAR_FORMATDATE17, Data);
    18: Res := FormatDateTime(VAR_FORMATDATE18, Data);
    19: Res := FormatDateTime(VAR_FORMATDATE19, Data);
    20: Res := FormatDateTime(VAR_FORMATDATE20, Data);
    21: Res := FormatDateTime(VAR_FORMATDATE21, Data);
    22: Res := FormatDateTime(VAR_FORMATDATE22, Data);
    23: Res := FormatDateTime(VAR_FORMATDATE23, Data);
    24: Res := FormatDateTime(VAR_FORMATDATE24, Data);
    25: Res := FormatDateTime(VAR_FORMATDATE25, Data); // 'YYYY'
    26: Res := FormatDateTime(VAR_FORMATDATE26, Data); // 'yymmdd_hhnnss'
    //
   100: Res := FormatDateTime(VAR_FORMATTIME,   Data);
   102: Res := FormatDateTime(VAR_FORMATTIME2,  Data);
   103: Res := FormatDateTime(VAR_FORMATTIME3,  Data);
   104: Res := FormatDateTime(VAR_FORMATTIMESTAMP4, Data);
   105: Res := FormatDateTime(VAR_FORMATDATETIME2,  Data);
   106: Res := FormatDateTime(VAR_FORMATDATETIME1,  Data);
   107: Res := FormatDateTime(VAR_FORMATDATETIME3,  Data);
   108: Res := FormatDateTime(VAR_FORMATTIME4,  Data);
   109: Res := FormatDateTime(VAR_FORMATDATETIME4,  Data);
   110: Res := FormatDateTime(VAR_FORMATTIME5,  Data);
    else Res := '* ERRO Res *'
  end;
  if Data = 0 then
  begin
    case Tipo of
      1: Res := '0000-00-00';
      109: Res := '0000-00-00 00:00:00';
      else Res := '';
    end;
  end;
  Result := Res;
end;

function TUnSQLite_PF2.FIC(FaltaInfoCompo: Boolean; Mensagem: String;
  ExibeMsg: Boolean): Boolean;
begin
  if FaltaInfoCompo then
  begin
    Result := True;
    if (Mensagem <> '') and ExibeMsg then
      MB_Aviso(Mensagem);
  end else
    Result := False;
end;

function TUnSQLite_PF2.FormataTelefone_TT_Curto(Telefone: String): String;
var
  T, Res: String;
  Tam, i: Integer;
begin
  Res := String(Telefone);
  if PareceTelefoneBR(Res) then
  begin
    if Length(Telefone) > 0 then
    if Telefone[1] <> '+' then
    begin
      T := '';
      for i := 1 to Length(Telefone) do
        //if Telefone[i] in (['0'..'9']) then T := T + Telefone[i];
        if CharInSet(Telefone[i], (['0'..'9'])) then T := T + String(Telefone[i]);
      Tam := Length(T);
      if (Tam = 13) then
        Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      //
      else if (Tam = 12) and (T[1] = '0')then
        Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      else if (Tam = 12) and (T[1] <> '0')then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      //
      else if (Tam = 11) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      //
      else if (Tam = 10) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] <> '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 09) and (T[1] <> '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      else if (Tam = 09) and (T[1] = '0')then
        Res := T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 08) and (T[1] <> '0')then
        Res := T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 08) and (T[1] = '0')then
        Res := T[2]+T[3]+T[4]+'- '+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 07) and (T[1] <> '0')then
        Res := T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7];
    end;
  end;
  Result := Res;
end;

function TUnSQLite_PF2.GetSelectedValue(AObject: TObject): TValue;
var
  LEditor: IBindListEditorCommon;
begin
  LEditor := GetBindEditor(AObject, IBindListEditorCommon) as IBindListEditorCommon;
  Result  := LEditor.SelectedValue;
end;

function TUnSQLite_PF2.IntToBool(Verdade: Integer): Boolean;
begin
  if Verdade = 0 then Result := False else Result := True;
end;

procedure TUnSQLite_PF2.MB_Aviso(Texto: String);
begin
  ShowMessage(Texto);
  //Dialog n�o funciona no android
  //MessageDlg(Texto, TMsgDlgType.mtWarning, [TMsgDlgBtn.MbOk], 0);
end;

procedure TUnSQLite_PF2.MB_Erro(Texto: String);
begin
  ShowMessage(Texto);
  //Dialog n�o funciona no android
  //MessageDlg(Texto, TMsgDlgType.mtError, [TMsgDlgBtn.MbOk], 0);
end;

procedure TUnSQLite_PF2.MB_Info(Texto: String);
begin
  ShowMessage(Texto);
  //Dialog n�o funciona no android
  //MessageDlg(Texto, TMsgDlgType.mtInformation, [TMsgDlgBtn.MbOk], 0);
end;

function TUnSQLite_PF2.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy: Result := 'C�pia';
    stDel: Result := 'Exclus�o';
    stIns: Result := 'Inclus�o';
    stLok: Result := 'Travado';
    stUpd: Result := 'Altera��o';
    stUnd: Result := 'Desiste';
    stPsq: Result := 'Pesquisa';
    else   Result := '???????';
  end;
end;

function TUnSQLite_PF2.ObtemCodigoInt(Form: TForm; Tabela, Campo: String;
  Query: TFDQuery; DB: TFDConnection): Integer;
var
  Atual: Integer;
begin
  AbreSQLQuery0(Form, Query, DB, [
    'SELECT MAX(' + Campo + ') ' + Campo,
    'FROM ' + Tabela]);
  //
  if Query.FieldByName(Campo).AsVariant <> Null then
    Atual := Query.FieldByName(Campo).AsVariant
  else
    Atual := 0;
  //
  Result := Atual + 1;
end;

function TUnSQLite_PF2.ObtemIndexDeTabela(Query: TFDQuery; CampoCodigo: String;
  Valor: Integer): Integer;
var
  Codigo, i: Integer;
begin
  i := 0;
  //
  Query.First;
  while not Query.Eof do
  begin
    Codigo := Query.FieldByName(CampoCodigo).AsInteger;
    //
    if Codigo = Valor then
      Break;
    i := i + 1;
    //
    Query.Next;
  end;
  Result := i;
end;

function TUnSQLite_PF2.PareceTelefoneBR(Telefone: String): Boolean;
var
  I: Integer;
  TxtA, TxtB: String;
begin
  TxtA := Trim(Telefone);
  TxtB := '';
  for I := 1 to Length(TxtA) do
  begin
    //if not (TxtA[I] in (['0'..'9',')','(','-',' ','x','X'])) then
    if not (CharInSet(TxtA[i], (['0'..'9',')','(','-',' ','x','X']))) then
      TxtB := TxtB + TxtA[I];
  end;
  Result := Length(TxtB) = 0;
end;

function TUnSQLite_PF2.SoNumero_TT(Texto: String): String;
var
  i:Integer;
begin
  Result := '';
  if Texto <> '' then
  begin
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    for i := 0 to Length(Texto) do
      if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i];
    {$ELSE}
    for i := 1 to Length(Texto) do
      if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i];
    {$ENDIF}
  end;
end;

function TUnSQLite_PF2.Substitui(Txt, ant, new: string): string;
begin
  Result := Txt.Replace(ant, new);
end;

function TUnSQLite_PF2.ValidaDadosCampo(Form: TForm; Query: TFDQuery;
  DB: TFDConnection; Campo, Tabela: String; Valor: Integer;
  PemiteZero: Boolean): Boolean;
begin
  if Valor <> 0 then
  begin
    USQLite_PF2.AbreSQLQuery0(Form, Query, DB, [
      'SELECT ' + Campo,
      'FROM ' + Tabela,
      'WHERE ' + Campo + '=' + FormatFloat('0', Valor)]);
    if Query.RecordCount > 0 then
      Result := True
    else
      Result := False;
    Query.Close;
  end else
  begin
    if PemiteZero then
      Result := True
    else
      Result := False;
  end;
end;

function TUnSQLite_PF2.ValidaDadosCampoConta(Form: TForm; Query: TFDQuery;
  DB: TFDConnection; Valor: Integer; var Credito: Boolean): Boolean;
begin
  Credito := False;
  //
  if Valor <> 0 then
  begin
    USQLite_PF2.AbreSQLQuery0(Form, Query, DB, [
      'SELECT Codigo, ',
      'CASE WHEN Credito = 1 THEN 1 ELSE 0 END ContaCred ',
      'FROM contas ',
      'WHERE Codigo=' + FormatFloat('0', Valor)]);
    if Query.RecordCount > 0 then
    begin
      Credito := IntToBool(Query.FieldByName('ContaCred').AsInteger);
      Result  := True;
    end else
      Result := False;
    Query.Close;
  end else
    Result := False;
end;

function TUnSQLite_PF2.ValidaEMail(const Value: String): Boolean;
begin

end;

function TUnSQLite_PF2.VariantToString(AVar: OleVariant): string;
var
  i: integer;
  V: olevariant;
begin
  if AVar = null then
  begin
    Result := '';
  end else begin
    Result := '';
    if System.Variants.VarType(AVar) = (varVariant or varByRef) then
       V := Variant(TVarData(AVar).VPointer^)
    else V := AVar;

    if VarType(V) = (varByte or varArray) then
        try
          for i:=VarArrayLowBound(V,1) to VarArrayHighBound(V,1) do
             Result := Result + Chr(Byte(V[i]));
        except;
        end
      else Result := V;
  end;
end;

function TUnSQLite_PF2.VariavelToString(Variavel: Variant): String;
  function Inteiro(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := IntToStr(Integer(Variavel));
  end;
begin
  try
    case VarType(Variavel) of
      varEmpty    {= $0000;} { vt_empty        0 } : Result := '';
      varNull     {= $0001;} { vt_null         1 } : Result := 'Null';
      varSmallint {= $0002;} { vt_i2           2 } : Result := Inteiro(Variavel);
      varInteger  {= $0003;} { vt_i4           3 } : Result := Inteiro(Variavel);
      varSingle   {= $0004;} { vt_r4           4 } : Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005;} { vt_r8           5 } : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      varCurrency {= $0006;} { vt_cy           6 } : Result := FormatFloat('0.000000000000000', Variavel);
      varDate     {= $0007;} { vt_date         7 } : Result := #39 + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + #39;
      varOleStr   {= $0008;} { vt_bstr         8 } : Result := #39 + WideStringToSQLString(Variavel) + #39;
      varDispatch {= $0009;} { vt_dispatch     9 } : Result := #39 + Variavel + #39;
      varError    {= $000A;} { vt_error       10 } : Result := #39 + Variavel + #39;
      varBoolean  {= $000B;} { vt_bool        11 } : Result := IntToStr(BoolToInt(Variavel));
      varVariant  {= $000C;} { vt_variant     12 } : Result := #39 + Variavel + #39;
      varUnknown  {= $000D;} { vt_unknown     13 } : Result := #39 + Variavel + #39;
      varShortInt {= $0010;} { vt_i1          16 } : Result := Inteiro(Variavel);
      varByte     {= $0011;} { vt_ui1         17 } : Result := Inteiro(Variavel);
      varWord     {= $0012;} { vt_ui2         18 } : Result := Inteiro(Variavel);
      varLongWord {= $0013;} { vt_ui4         19 } : Result := Inteiro(Variavel);
      varInt64    {= $0014;} { vt_i8          20 } : Result := (*Intei64*)Inteiro(Variavel);
      varStrArg   {= $0048;} { vt_clsid       72 } : Result := #39 + Variavel + #39;
      varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Result := #39 + WideStringToSQLString(Variavel) + #39;
      varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Result := #39 + Variavel + #39;
      varUString  {= $0102;} { Unicode string 258 } {not OLE compatible }: Result := #39 + WideStringToSQLString(Variavel) + #39;
      else begin
        if (VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType') then
          Result := FloatToStrF(Variavel, ffFixed, 15, 15)
        else
        if (VarTypeAsText(VarType(Variavel)) = 'SQLTimeStampVariantType') then
          Result := #39 + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + #39
        else begin
          MB_Erro('Vari�vel n�o definida!' + sLineBreak +
          'Tipo de vari�vel: ' + IntToStr(VarType(Variavel)) + ' => ' +
          VarTypeAsText(VarType(Variavel)));
          Result := #39 + VariantToString(Variavel) + #39;
        end;
      end;
    end;
    if (VarType(Variavel) in ([varSingle, varDouble, varCurrency]))
    or ((VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType')) then
    begin
        if Result.IndexOf(',.') > -1 then
          Result := Result.Replace(',.', '.');
        if Result.IndexOf(',') > -1 then
          Result := Result.Replace(',', '.');
        if Result.Length > 21 then Result := Result.Substring(1, 21);
    end;
  except
    MB_Erro('Erro na function "VariavelToString".' + sLineBreak +
    'Tipo de vari�vel: ' + VarTypeAsText(VarType(Variavel)));
  end;
end;

function TUnSQLite_PF2.WideStringToSQLString(Texto: String): String;
const
  SP = '\\';
var
  i: integer;
  ServerPath: Boolean;
begin
  Result := Texto;
  Result := '';
  if Texto = '' then exit;
  ServerPath := Texto.IndexOf(SP) = 1;
  for i := System.Low(Texto) to System.High(Texto) do
  begin
    if Texto[i] = '\' then Result := Result + '\\' else
    //if Texto[i] = '/' then Result := Result + '//' else
    if Texto[i] = #39 then
    begin
      if (Texto.Length = i) or (Texto[i+1] <> #39) then
        Result := Result + #39#39;
    end
    {
    ... colocar aqui outros empecilhos
    else if ... then ...
    }
    else
    if (i > 1) and (Texto[i] = '-') and (Texto[i-1] = '-') then
      Result := Result + ' '
    else
      Result := Result + Texto[i];
  end;

  Result := Substitui(Result, '\\\\', '\\');
  Result := Substitui(Result, '\\\', '\\');
  Result := Substitui(Result, #39#39#39#39, #39#39);
  Result := Substitui(Result, #39#39#39, #39#39);
  //
  if ServerPath then
    Result := SP + Result;
end;

end.
