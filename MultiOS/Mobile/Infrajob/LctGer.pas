unit LctGer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Edit, FMX.ComboEdit, FMX.ListBox, FMX.StdCtrls,
  FMX.ExtCtrls, FMX.TabControl, FMX.Layouts, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, FMX.DateTimeCtrls,
  FMX.EditBox, FMX.NumberBox, FMX.SearchBox, System.Actions, FMX.ActnList,
  FMX.Ani;

type
  TPesqType = (stCar=0, stCon=1, stEnt=2);
  TFmLctGer = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBox2: TListBox;
    QrPesq: TFDQuery;
    TabItem4: TTabItem;
    LBPesq: TListBox;
    SearchBox1: TSearchBox;
    ListBoxHeader2: TListBoxHeader;
    SbInclui: TSpeedButton;
    SbSaida: TSpeedButton;
    SearchBox2: TSearchBox;
    ListBoxHeader3: TListBoxHeader;
    BindingsList1: TBindingsList;
    QrCarteiras: TFDQuery;
    QrCarteirasNome: TWideMemoField;
    QrCarteirasCodigo: TIntegerField;
    CBCarteiras: TComboBox;
    BindSourceDB2: TBindSourceDB;
    LinkFillControlToField2: TLinkFillControlToField;
    QrLanctos: TFDQuery;
    QrLanctosControle: TIntegerField;
    QrLanctosDescricao: TWideMemoField;
    BindSourceDB1: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ChangeTabAction4: TChangeTabAction;
    QrLanctosDB: TFDQuery;
    ListBoxHeader4: TListBoxHeader;
    SpeedButton1: TSpeedButton;
    BindSourceDB3: TBindSourceDB;
    LinkFillControlToField3: TLinkFillControlToField;
    SbEdita: TSpeedButton;
    SbExclui: TSpeedButton;
    SbVolta: TSpeedButton;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIControle: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICarteira: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIData: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBIConta: TListBoxItem;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBIEntidade: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    LBIValor: TListBoxItem;
    ListBoxGroupHeader8: TListBoxGroupHeader;
    LBIDescricao: TListBoxItem;
    ListBoxGroupHeader9: TListBoxGroupHeader;
    LBINotaFiscal: TListBoxItem;
    ListBoxGroupHeader10: TListBoxGroupHeader;
    LBIVencimento: TListBoxItem;
    Layout1: TLayout;
    ListBox3: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    EdCarteira: TNumberBox;
    SBCarteiras: TSpeedButton;
    ListBoxItem3: TListBoxItem;
    TPData: TDateEdit;
    ListBoxItem2: TListBoxItem;
    EdConta: TNumberBox;
    SBContas: TSpeedButton;
    ListBoxItem4: TListBoxItem;
    EdEntidade: TNumberBox;
    SBEntidade: TSpeedButton;
    ListBoxItem6: TListBoxItem;
    EdValor: TEdit;
    ListBoxItem8: TListBoxItem;
    EdDescri: TEdit;
    ListBoxItem9: TListBoxItem;
    EdNF: TEdit;
    ListBoxItem10: TListBoxItem;
    TPVencto: TDateEdit;
    ListBoxHeader1: TListBoxHeader;
    SbConfirma: TSpeedButton;
    SbDesiste: TSpeedButton;
    QrLanctosData: TWideMemoField;
    SBAbrir: TSpeedButton;
    SBAtualiza: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure CBCarteirasChange(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SBCarteirasClick(Sender: TObject);
    procedure LBPesqItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure SBContasClick(Sender: TObject);
    procedure SBEntidadeClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure FormFocusChanged(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure SBAbrirClick(Sender: TObject);
    procedure SBAtualizaClick(Sender: TObject);
  private
    { Private declarations }
    FKBBounds: TRectF;
    FNeedOffset: Boolean;
    FNEdita: Boolean;
    FPesquisa: TPesqType;
    FCarteira, FPesq: Integer;
    procedure ReopenLanctos(Controle: Integer);
    procedure ReopenQrLanctos(Carteira: Integer);
    procedure ReopenCarteiras();
    procedure ReopenPesq(Tabela, CampoCodigo, CampoNome: String);
    procedure ConfiguraResultadoPesquisa;
    //Form Scroll
    procedure CalcContentBoundsProc(Sender: TObject; var ContentBounds: TRectF);
    procedure UpdateKBBounds;
    procedure RestorePosition;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmLctGer: TFmLctGer;

implementation

{$R *.fmx}

uses Principal, Contas, Carteiras, UnSQLite_PF2, System.Math;

procedure TFmLctGer.CalcContentBoundsProc(Sender: TObject;
  var ContentBounds: TRectF);
begin
  if FNeedOffset and (FKBBounds.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom, 2 * ClientHeight - FKBBounds.Top);
  end;
end;

procedure TFmLctGer.CBCarteirasChange(Sender: TObject);
var
  Carteira: Integer;
begin
  if CBCarteiras.ItemIndex > -1 then
  begin
    Carteira := USQLite_PF2.GetSelectedValue(CBCarteiras).ToString.ToInteger();
    //
    if Carteira <> 0 then
    begin
      FCarteira := Carteira;
      //
      ReopenQrLanctos(Carteira)
    end else
    begin
      FCarteira := 0;
      //
      QrLanctos.Close;
    end;
  end;
end;

procedure TFmLctGer.ConfiguraResultadoPesquisa;
begin
  case FPesquisa of
    stCar: EdCarteira.Value := FPesq;
    stCon: EdConta.Value    := FPesq;
    stEnt: EdEntidade.Value := FPesq;
  end;
end;

procedure TFmLctGer.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ListBox3.OnCalcContentBounds := CalcContentBoundsProc;
  //
  ReopenCarteiras;
end;

procedure TFmLctGer.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds;
end;

procedure TFmLctGer.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds.Create(0, 0, 0, 0);
  FNeedOffset := False;
  RestorePosition;
end;

procedure TFmLctGer.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds := TRectF.Create(Bounds);
  FKBBounds.TopLeft := ScreenToClient(FKBBounds.TopLeft);
  FKBBounds.BottomRight := ScreenToClient(FKBBounds.BottomRight);
  UpdateKBBounds;
end;

procedure TFmLctGer.LBPesqItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FPesq := USQLite_PF2.GetSelectedValue(LBPesq).ToString.ToInteger();
    //
    ConfiguraResultadoPesquisa;
    ExecuteAction(ChangeTabAction3);
  end;
end;

procedure TFmLctGer.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := USQLite_PF2.GetSelectedValue(ListBox1).ToString.ToInteger();
    FNEdita := False;
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmLctGer.ReopenCarteiras;
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrCarteiras, FmPrincipal.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmLctGer.ReopenLanctos(Controle: Integer);
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrLanctosDB, FmPrincipal.MyDB, [
    'SELECT lct.*, ',
    'CASE WHEN con.Credito = 1 THEN 1 ELSE 0 END ContaCred, ',
    'con.Nome ContaTXT, car.Nome CarteiraTXT, ',
    'CASE WHEN ent.Tipo = 0 THEN ent.RazaoSocial ELSE ent.Nome END FornecedorTXT, ',
    'CASE WHEN enb.Tipo = 0 THEN enb.RazaoSocial ELSE enb.Nome END ClienteTXT ',
    'FROM lanctos lct ',
    'LEFT JOIN contas con ON con.Codigo = lct.Genero ',
    'LEFT JOIN carteiras car ON car.Codigo = lct.Carteira ',
    'LEFT JOIN entidades ent ON ent.Codigo = lct.Fornecedor ',
    'LEFT JOIN entidades enb ON enb.Codigo = lct.Cliente ',
    'WHERE lct.Controle=' + FormatFloat('0', Controle),
    '']);
end;

procedure TFmLctGer.ReopenPesq(Tabela, CampoCodigo, CampoNome: String);
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrPesq, FmPrincipal.MyDB, [
    'SELECT  ' + CampoCodigo + ' Codigo, ' + CampoNome + ' Nome ',
    'FROM ' + Tabela,
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
  //
  if QrPesq.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmLctGer.ReopenQrLanctos(Carteira: Integer);
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrLanctos, FmPrincipal.MyDB, [
    'SELECT Data, Controle, Descricao, ',
    'Credito + Debito Valor ',
    'FROM lanctos ',
    'WHERE Carteira=' + FormatFloat('0', Carteira),
    'ORDER BY Data ',
    '']);
  if QrLanctos.RecordCount > 0 then
    SearchBox2.Enabled := True
  else
    SearchBox2.Enabled := False;
end;

procedure TFmLctGer.RestorePosition;
begin
  ListBox3.ViewportPosition := PointF(ListBox3.ViewportPosition.X, 0);
  Layout1.Align := TAlignLayout.Client;
  ListBox3.RealignContent;
end;

procedure TFmLctGer.SBAbrirClick(Sender: TObject);
begin
  case FPesquisa of
    stCar: FmPrincipal.MostraCarteiras;
    stCon: FmPrincipal.MostraContas;
    stEnt: FmPrincipal.MostraEntidades;
  end;
end;

procedure TFmLctGer.SBAtualizaClick(Sender: TObject);
begin
  QrPesq.Close;
  QrPesq.Open();
end;

procedure TFmLctGer.SBCarteirasClick(Sender: TObject);
begin
  FPesquisa := stCar;
  FPesq     := StrToInt(EdCarteira.Text);
  //
  ReopenPesq('carteiras', 'Codigo', 'Nome');
  //
  ExecuteAction(ChangeTabAction4);
end;

procedure TFmLctGer.SbConfirmaClick(Sender: TObject);
var
  Controle, Carteira, Conta, Entidade, Fornece, Cliente, NotaFiscal: Integer;
  Data, Vencimento, Descricao: String;
  Debito, Credito, Valor: Double;
  ContaCred: Boolean;
  SQLTipo: TSQLType;
begin
  Controle   := FCodigo;
  Carteira   := StrToInt(EdCarteira.Text);
  Conta      := StrToInt(EdConta.Text);
  Entidade   := StrToInt(EdEntidade.Text);
  Valor      := StrToFloat(EdValor.Text);
  Descricao  := EdDescri.Text;
  NotaFiscal := StrToInt(EdNF.Text);
  Data       := TPData.Text;
  Vencimento := TPData.Text;
  //
  if not USQLite_PF2.ValidaDadosCampo(Self, FmPrincipal.QrAux,
    FmPrincipal.MyDB, 'Codigo', 'carteiras', Carteira) then
  begin
    USQLite_PF2.MB_Aviso('Carteira n�o definida ou com valor inv�lido!');
    Exit;
  end;
  if not USQLite_PF2.ValidaDadosCampoConta(Self, FmPrincipal.QrAux,
    FmPrincipal.MyDB, Conta, ContaCred) then
  begin
    USQLite_PF2.MB_Aviso('Conta n�o definida ou com valor inv�lido!');
    Exit;
  end;
  if not USQLite_PF2.ValidaDadosCampo(Self, FmPrincipal.QrAux,
    FmPrincipal.MyDB, 'Codigo', 'entidades', Entidade, True) then
  begin
    USQLite_PF2.MB_Aviso('Entidade n�o definida ou com valor inv�lido!');
    Exit;
  end;
  if USQLite_PF2.FIC(Valor = 0, 'Valor n�o definido!') then Exit;
  if USQLite_PF2.FIC(Descricao = '', 'Descri��o n�o definida!') then Exit;  
  //  
  if ContaCred = True then
  begin
    Cliente := Entidade;
    Credito := Valor;
    Fornece := 0;
    Debito  := 0;
  end else
  begin
    Fornece := Entidade;
    Debito  := Valor;
    Cliente := 0;
    Credito := 0;
  end;
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := USQLite_PF2.ObtemCodigoInt(Self, 'lanctos', 'Controle',
                 FmPrincipal.QrAux, FmPrincipal.MyDB);
  end else
    SQLTipo := stUpd;
  //
  USQLite_PF2.CarregaSQLInsUpd(FmPrincipal.QrUpd, FmPrincipal.MyDB, SQLTipo,
    'lanctos', False, 
    ['Carteira', 'Data', 'Genero', 'Cliente', 'Fornecedor', 'Credito', 
    'Debito', 'Descricao', 'NotaFiscal', 'Vencimento'], ['Controle'],
    [Carteira, Data, Conta, Cliente, Fornece, Credito, 
    Debito, Descricao, NotaFiscal, Vencimento], [Controle], True, False, '');
  //
  CBCarteiras.ItemIndex := USQLite_PF2.ObtemIndexDeTabela(QrCarteiras, 'Codigo', Carteira);
  ReopenQrLanctos(Carteira);
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmLctGer.SBEntidadeClick(Sender: TObject);
begin
  FPesquisa := stEnt;
  FPesq     := StrToInt(EdEntidade.Text);
  //
  ReopenPesq('entidades', 'Codigo', 'CASE WHEN Tipo = 0 THEN RazaoSocial ELSE Nome END ');
  //
  ExecuteAction(ChangeTabAction4);
end;

procedure TFmLctGer.SBContasClick(Sender: TObject);
begin
  FPesquisa := stCon;
  FPesq     := StrToInt(EdConta.Text);
  //
  ReopenPesq('contas', 'Codigo', 'Nome');
  //
  ExecuteAction(ChangeTabAction4);
end;

procedure TFmLctGer.SbExcluiClick(Sender: TObject);
var
  Carteira: Integer;
begin
  MessageDlg('Deseja excluir este registro?', System.UITypes.TMsgDlgType.mtInformation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      if AResult = mrYes then
      begin
        USQLite_PF2.ExecutaSQLQuery0(Self, FmPrincipal.QrUpd, FmPrincipal.MyDB,
          ['DELETE FROM lanctos WHERE Controle = ' + FormatFloat('0', FCodigo)]);
        //
        Carteira := USQLite_PF2.GetSelectedValue(CBCarteiras).ToString.ToInteger();
        //
        ReopenQrLanctos(Carteira);
        //
        ExecuteAction(ChangeTabAction1);
      end;
    end);
end;

procedure TFmLctGer.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  FNEdita := False;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmLctGer.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmLctGer.SpeedButton1Click(Sender: TObject);
begin
  ConfiguraResultadoPesquisa;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmLctGer.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenLanctos(FCodigo);
      //
      LBIControle.Text   := FormatFloat('0', QrLanctosDB.FieldByName('Controle').AsInteger);
      LBICarteira.Text   := QrLanctosDB.FieldByName('CarteiraTXT').AsString;
      LBIData.Text       := QrLanctosDB.FieldByName('Data').AsString;
      LBIConta.Text      := QrLanctosDB.FieldByName('ContaTXT').AsString;
      LBIDescricao.Text  := QrLanctosDB.FieldByName('Descricao').AsString;
      LBINotaFiscal.Text := FormatFloat('0', QrLanctosDB.FieldByName('NotaFiscal').AsInteger);
      LBIVencimento.Text := QrLanctosDB.FieldByName('Vencimento').AsString;
      //
      if QrLanctosDB.FieldByName('ContaCred').AsInteger = 1 then
      begin
        LBIEntidade.Text := QrLanctosDB.FieldByName('ClienteTXT').AsString;
        LBIValor.Text    := FloatToStrF(QrLanctosDB.FieldByName('Credito').AsFloat, ffNumber, 8, 2);
      end else
      begin
        LBIEntidade.Text := QrLanctosDB.FieldByName('FornecedorTXT').AsString;
        LBIValor.Text    := FloatToStrF(QrLanctosDB.FieldByName('Debito').AsFloat, ffNumber, 8, 2);
      end;
    end;
    2: //Edita
    begin
      FPesq := 0;
      //
      if FNEdita = False then
      begin
        if FCodigo <> 0 then
        begin
          ReopenLanctos(FCodigo);
          //
          EdCarteira.Value := QrLanctosDB.FieldByName('Carteira').AsInteger;
          EdConta.Value    := QrLanctosDB.FieldByName('Genero').AsInteger;
          TPData.Date      := StrToDate(QrLanctosDB.FieldByName('Data').AsString);
          EdDescri.Text    := QrLanctosDB.FieldByName('Descricao').AsString;
          EdNF.Text        := FormatFloat('0', QrLanctosDB.FieldByName('NotaFiscal').AsInteger);
          TPVencto.Date    := StrToDate(QrLanctosDB.FieldByName('Vencimento').AsString);
          //
          if QrLanctosDB.FieldByName('ContaCred').AsInteger = 1 then
          begin
            EdEntidade.Value := QrLanctosDB.FieldByName('Cliente').AsInteger;
            EdValor.Text     := FloatToStrF(QrLanctosDB.FieldByName('Credito').AsFloat, ffNumber, 8, 2);
          end else
          begin
            EdEntidade.Value := QrLanctosDB.FieldByName('Fornecedor').AsInteger;
            EdValor.Text     := FloatToStrF(QrLanctosDB.FieldByName('Debito').AsFloat, ffNumber, 8, 2);
          end;
        end else
        begin
          EdCarteira.Value := FCarteira;
          EdConta.Value    := 0;
          EdEntidade.Value := 0;
          TPData.Date      := Date;
          EdValor.Text     := '0';
          EdDescri.Text    := '';
          EdNF.Text        := '0';
          TPVencto.Date    := Date;
        end;
        FNEdita := True;
      end;
    end;
    3: //Pesquisa
    begin
      if FPesq <> 0 then
        LBPesq.ItemIndex := USQLite_PF2.ObtemIndexDeTabela(QrPesq, 'Codigo', FPesq);
    end;
  end;
end;

procedure TFmLctGer.UpdateKBBounds;
var
  LFocused : TControl;
  LFocusRect: TRectF;
begin
  FNeedOffset := False;
  if Assigned(Focused) then
  begin
    LFocused := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    LFocusRect.Offset(ListBox3.ViewportPosition);
    if (LFocusRect.IntersectsWith(TRectF.Create(FKBBounds))) and
       (LFocusRect.Bottom > FKBBounds.Top) then
    begin
      FNeedOffset := True;
      Layout1.Align := TAlignLayout.Horizontal;
      ListBox3.RealignContent;
      Application.ProcessMessages;
      ListBox3.ViewportPosition := PointF(ListBox3.ViewportPosition.X,
                                     LFocusRect.Bottom - FKBBounds.Top);
    end;
  end;
  if not FNeedOffset then
    RestorePosition;
end;

end.
