unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.TreeView, FMX.ListBox, FMX.StdCtrls, FMX.MultiView, FMX.Memo, FMX.Objects,
  FMX.Menus, FMX.ListView.Types, FMX.ListView, System.Math.Vectors,
  FMX.Controls3D, FMX.Layers3D, Data.DbxSqlite, Data.DB, Data.SqlExpr,
  System.IOUtils, Data.FMTBcd, Data.DBXDataSnap, IPPeerClient, Data.DBXCommon,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.FMXUI.Wait,
  FireDAC.Comp.UI, FMX.ExtCtrls, FMX.Controls.Presentation;

type
  TFmPrincipal = class(TForm)
    MultiView1: TMultiView;
    MasterToolbar: TToolBar;
    MasterLabel: TLabel;
    SBOcultar: TSpeedButton;
    ListBox1: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem4: TListBoxItem;
    Layout1: TLayout;
    DetailToolbar: TToolBar;
    DetailLabel: TLabel;
    MasterButton: TSpeedButton;
    QrAux: TFDQuery;
    QrUpd: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    MyDB: TFDConnection;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure ListBoxItem1Click(Sender: TObject);
    procedure SBOcultarClick(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure ListBoxItem2Click(Sender: TObject);
    procedure MyDBBeforeConnect(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
    procedure ListBoxItem3Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraLctGer();
  public
    { Public declarations }
    procedure MostraCarteiras();
    procedure MostraContas();
    procedure MostraEntidades();
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  Contas, Carteiras, Entidades, LctGer, UnSQLite_PF2;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SBOcultar.Visible := True;
  {$ELSE}
  SBOcultar.Visible := False;
  {$ENDIF}
  VAR_USUARIO := 0;
  //
 {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  MyDB.Params.Values['Database'] := TPath.Combine(TPath.GetDocumentsPath, 'infrajob.db3');
  {$ENDIF}
  MyDB.Connected := True;
end;

procedure TFmPrincipal.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  MultiView1.HideMaster;
end;

procedure TFmPrincipal.ListBoxItem1Click(Sender: TObject);
begin
  MostraCarteiras();
end;

procedure TFmPrincipal.ListBoxItem2Click(Sender: TObject);
begin
  MostraContas;
end;

procedure TFmPrincipal.ListBoxItem3Click(Sender: TObject);
begin
  MostraEntidades();
end;

procedure TFmPrincipal.ListBoxItem4Click(Sender: TObject);
begin
  MostraLctGer();
end;

procedure TFmPrincipal.MostraCarteiras;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmCarteiras, FmCarteiras);
  FmCarteiras.Show;
  {$ELSE}
  Application.CreateForm(TFmCarteiras, FmCarteiras);
  FmCarteiras.ShowModal;
  FmCarteiras.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.MostraContas;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmContas, FmContas);
  FmContas.Show;
  {$ELSE}
  Application.CreateForm(TFmContas, FmContas);
  FmContas.ShowModal;
  FmContas.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.MostraEntidades;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmEntidades, FmEntidades);
  FmEntidades.Show;
  {$ELSE}
  Application.CreateForm(TFmEntidades, FmEntidades);
  FmEntidades.ShowModal;
  FmEntidades.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.MostraLctGer;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmLctGer, FmLctGer);
  FmLctGer.Show;
  {$ELSE}
  Application.CreateForm(TFmLctGer, FmLctGer);
  FmLctGer.ShowModal;
  FmLctGer.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.MyDBBeforeConnect(Sender: TObject);
begin
 {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  MyDB.Params.Values['Database'] := TPath.Combine(TPath.GetDocumentsPath, 'infrajob.db3');
  {$ENDIF}
end;

procedure TFmPrincipal.SBOcultarClick(Sender: TObject);
begin
  MultiView1.HideMaster;
end;

end.
