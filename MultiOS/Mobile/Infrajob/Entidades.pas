unit Entidades;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.SearchBox, System.Actions, FMX.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TFmEntidades = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbInclui: TSpeedButton;
    Label1: TLabel;
    SearchBox1: TSearchBox;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    QrEntidades: TFDQuery;
    TabControl2: TTabControl;
    TabItem4: TTabItem;
    TabItem5: TTabItem;
    LBJuridica: TListBox;
    LBFisica: TListBox;
    ListBoxItem8: TListBoxItem;
    EdRazaoSocial: TEdit;
    ListBoxItem7: TListBoxItem;
    EdCNPJ: TEdit;
    ListBoxItem6: TListBoxItem;
    EdETe1: TEdit;
    ListBoxItem5: TListBoxItem;
    EdEEmail: TEdit;
    ListBoxItem1: TListBoxItem;
    EdNome: TEdit;
    ListBoxItem2: TListBoxItem;
    EdCPF: TEdit;
    ListBoxItem3: TListBoxItem;
    EdPTe1: TEdit;
    ListBoxItem4: TListBoxItem;
    EdPEmail: TEdit;
    ClearEditButton1: TClearEditButton;
    ClearEditButton2: TClearEditButton;
    ClearEditButton3: TClearEditButton;
    ClearEditButton4: TClearEditButton;
    ClearEditButton5: TClearEditButton;
    ClearEditButton6: TClearEditButton;
    ClearEditButton7: TClearEditButton;
    ClearEditButton8: TClearEditButton;
    ListBoxItem9: TListBoxItem;
    CkEAtivo: TCheckBox;
    ListBoxItem10: TListBoxItem;
    CkPAtivo: TCheckBox;
    TabControl3: TTabControl;
    TabItem6: TTabItem;
    TabItem7: TTabItem;
    ListBox4: TListBox;
    ListBox5: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBIECodigo: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIRazaoSocial: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICNPJ: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIETe1: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBIEEmail: TListBoxItem;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBIPCodigo: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader8: TListBoxGroupHeader;
    LBICPF: TListBoxItem;
    ListBoxGroupHeader9: TListBoxGroupHeader;
    LBIPTe1: TListBoxItem;
    ListBoxGroupHeader10: TListBoxGroupHeader;
    LBIPEmail: TListBoxItem;
    Ativo: TListBoxGroupHeader;
    LBIEAtivo: TListBoxItem;
    ListBoxGroupHeader11: TListBoxGroupHeader;
    LBIPAtivo: TListBoxItem;
    QrEntidadesView: TFDQuery;
    QrEntidadesViewCodigo: TIntegerField;
    QrEntidadesViewNome: TWideStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    procedure FormCreate(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdETe1Exit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdPTe1Exit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntidades(Codigo: Integer);
    procedure ReopenEntidadesView;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmEntidades: TFmEntidades;

implementation

{$R *.fmx}

uses Principal, UnSQLite_PF2;

procedure TFmEntidades.EdCNPJExit(Sender: TObject);
var
  Txt: String;
begin
  Txt := EdCNPJ.Text;
  //
  if Txt <> '' then
    EdCNPJ.Text := USQLite_PF2.SoNumero_TT(Txt);
end;

procedure TFmEntidades.EdCPFExit(Sender: TObject);
var
  Txt: String;
begin
  Txt := EdCPF.Text;
  //
  if Txt <> '' then
    EdCPF.Text := USQLite_PF2.SoNumero_TT(Txt);
end;

procedure TFmEntidades.EdETe1Exit(Sender: TObject);
var
  Txt: String;
begin
  Txt := EdETe1.Text;
  //
  if Txt <> '' then
    EdETe1.Text := USQLite_PF2.SoNumero_TT(Txt);
end;

procedure TFmEntidades.EdPTe1Exit(Sender: TObject);
var
  Txt: String;
begin
  Txt := EdPTe1.Text;
  //
  if Txt <> '' then
    EdPTe1.Text := USQLite_PF2.SoNumero_TT(Txt);
end;

procedure TFmEntidades.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenEntidadesView;
end;

procedure TFmEntidades.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := USQLite_PF2.GetSelectedValue(ListBox1).ToString.ToInteger();
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmEntidades.ReopenEntidades(Codigo: Integer);
begin
  USQLite_PF2.AbreSQLQuery0(Self, QrEntidades, FmPrincipal.MyDB, [
    'SELECT * FROM entidades WHERE Codigo=' + FormatFloat('0', Codigo),
    '']);
end;

procedure TFmEntidades.ReopenEntidadesView;
begin
  QrEntidadesView.Close;
  QrEntidadesView.Connection := FmPrincipal.MyDB;
  QrEntidadesView.Open();
  //
  if QrEntidadesView.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmEntidades.SbConfirmaClick(Sender: TObject);
var
  Tipo, Codigo, Ativo: Integer;
  RazaoSocial, CNPJ, CNPJ_Txt, ETe1, EEmail, Nome, CPF, CPF_Ttx, PTe1, PEmail: String;
  SQLTipo: TSQLType;
begin
  Codigo := FCodigo;
  Tipo   := TabControl2.TabIndex;
  //
  if Tipo = 0 then
  begin
    //Jur�dica
    RazaoSocial := EdRazaoSocial.Text;
    CNPJ        := USQLite_PF2.SoNumero_TT(EdCNPJ.Text);
    CNPJ_Txt    := CNPJ;
    ETe1        := USQLite_PF2.SoNumero_TT(EdETe1.Text);
    EEmail      := EdEEmail.Text;
    Ativo       := USQLite_PF2.BoolToInt(CkEAtivo.IsChecked);
    //
    Nome   := '';
    CPF    := '';
    PTe1   := '';
    PEmail := '';
    //
    if USQLite_PF2.FIC(RazaoSocial = '', 'Defina uma descri��o!') then Exit;
  end else
  begin
    //F�sica
    Nome    := EdNome.Text;
    CPF     := USQLite_PF2.SoNumero_TT(EdCPF.Text);
    CPF_Ttx := CPF;
    PTe1    := USQLite_PF2.SoNumero_TT(EdPTe1.Text);
    PEmail  := EdPEmail.Text;
    Ativo   := USQLite_PF2.BoolToInt(CkPAtivo.IsChecked);
    //
    RazaoSocial := '';
    CNPJ        := '';
    ETe1        := '';
    EEmail      := '';
    //
    if USQLite_PF2.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  end;
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := USQLite_PF2.ObtemCodigoInt(Self, 'entidades', 'Codigo',
                 FmPrincipal.QrAux, FmPrincipal.MyDB);
  end else
    SQLTipo := stUpd;
  //
  USQLite_PF2.CarregaSQLInsUpd(FmPrincipal.QrUpd, FmPrincipal.MyDB, SQLTipo,
    'entidades', False,
    ['Tipo', 'RazaoSocial', 'CNPJ', 'ETe1', 'EEmail', 'Nome',
    'CPF', 'PTe1', 'PEmail', 'Ativo'], ['Codigo'],
    [Tipo, RazaoSocial, CNPJ, ETe1, EEmail, Nome,
    CPF, PTe1, PEmail, Ativo], [Codigo], True, False, '');
  //
  ReopenEntidadesView;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmEntidades.SbExcluiClick(Sender: TObject);
begin
  USQLite_PF2.AbreSQLQuery0(Self, FmPrincipal.QrAux, FmPrincipal.MyDB,
    ['SELECT * FROM lanctos WHERE Fornecedor=' + FormatFloat('0', FCodigo),
    'OR Cliente=' + FormatFloat('0', FCodigo)]);
  //
  if FmPrincipal.QrAux.RecordCount = 0 then
  begin
    MessageDlg('Deseja excluir este registro?', System.UITypes.TMsgDlgType.mtInformation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        if AResult = mrYes then
        begin
          USQLite_PF2.ExecutaSQLQuery0(Self, FmPrincipal.QrUpd, FmPrincipal.MyDB,
            ['DELETE FROM entidades WHERE Codigo = ' + FormatFloat('0', FCodigo)]);
          //
          ReopenEntidadesView;
          ExecuteAction(ChangeTabAction1);
        end;
      end);
  end else
    USQLite_PF2.MB_Aviso('Exclus�o abortada! Esta entidade j� foi utilizada nos lan�amentos financeiros.');
  //
  FmPrincipal.QrAux.Close;
end;

procedure TFmEntidades.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmEntidades.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmEntidades.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenEntidades(FCodigo);
      //
      if QrEntidades.FieldByName('Tipo').AsInteger = 0 then
        TabControl3.TabIndex := 0
      else
        TabControl3.TabIndex := 1;
      //
      LBIECodigo.Text     := FormatFloat('0', QrEntidades.FieldByName('Codigo').AsInteger);
      LBIRazaoSocial.Text := QrEntidades.FieldByName('RazaoSocial').AsString;
      LBICNPJ.Text        := QrEntidades.FieldByName('CNPJ').AsString
      ;
      LBIETe1.Text        := USQLite_PF2.FormataTelefone_TT_Curto(QrEntidades.FieldByName('ETe1').AsString);
      LBIEEmail.Text      := QrEntidades.FieldByName('EEmail').AsString;
      //
      LBIPCodigo.Text := FormatFloat('0', QrEntidades.FieldByName('Codigo').AsInteger);
      LBINome.Text    := QrEntidades.FieldByName('Nome').AsString;
      LBICPF.Text     := QrEntidades.FieldByName('CPF').AsString;
      LBIPTe1.Text    := USQLite_PF2.FormataTelefone_TT_Curto(QrEntidades.FieldByName('PTe1').AsString);
      LBIPEmail.Text  := QrEntidades.FieldByName('PEmail').AsString;
      //
      if QrEntidades.FieldByName('Ativo').AsInteger = 1 then
      begin
        LBIEAtivo.Text := 'Sim';
        LBIPAtivo.Text := 'Sim';
      end else
      begin
        LBIEAtivo.Text := 'N�o';
        LBIPAtivo.Text := 'N�o';
      end;
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenEntidades(FCodigo);
        //
        EdRazaoSocial.Text := QrEntidades.FieldByName('RazaoSocial').AsString;
        EdCNPJ.Text        := QrEntidades.FieldByName('CNPJ').AsString;
        EdETe1.Text        := QrEntidades.FieldByName('ETe1').AsString;
        EdEEmail.Text      := QrEntidades.FieldByName('EEmail').AsString;
        CkEAtivo.IsChecked := USQLite_PF2.IntToBool(QrEntidades.FieldByName('Ativo').AsInteger);
        //
        EdNome.Text        := QrEntidades.FieldByName('Nome').AsString;
        EdCPF.Text         := QrEntidades.FieldByName('CPF').AsString;
        EdPTe1.Text        := QrEntidades.FieldByName('PTe1').AsString;
        EdPEmail.Text      := QrEntidades.FieldByName('PEmail').AsString;
        CkPAtivo.IsChecked := USQLite_PF2.IntToBool(QrEntidades.FieldByName('Ativo').AsInteger);
        //
        if QrEntidades.FieldByName('Tipo').AsInteger = 0 then
          TabControl2.TabIndex := 0
        else
          TabControl2.TabIndex := 1;
      end else
      begin
        EdRazaoSocial.Text := '';
        EdCNPJ.Text        := '';
        EdETe1.Text        := '';
        EdEEmail.Text      := '';
        CkEAtivo.IsChecked := True;
        //
        EdNome.Text        := '';
        EdCPF.Text         := '';
        EdPTe1.Text        := '';
        EdPEmail.Text      := '';
        CkPAtivo.IsChecked := True;
        //
        TabControl2.TabIndex := 0;
      end;
    end;
  end;
end;

end.
