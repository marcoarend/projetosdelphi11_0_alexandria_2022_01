unit SincroSettings;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Layouts, FMX.ListBox,
  FMX.TabControl, FMX.StdCtrls, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  System.Actions, FMX.ActnList, FMX.Edit,
  UnGeral, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  UnDmkEnums;

type
  TFmSincroSettings = class(TForm)
    ToolBar1: TToolBar;
    SettingsMain: TLabel;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    SettingsList1: TListBox;
    LbiDefMmail: TListBoxItem;
    PaymentType: TListBoxItem;
    RenewalType: TListBoxItem;
    SyncSettings: TListBoxGroupHeader;
    SyncUSB: TListBoxItem;
    SyncWifi: TListBoxItem;
    SyncCollections: TListBoxItem;
    SwSyncAUSB: TSwitch;
    SwSyncAWiFi: TSwitch;
    SwSyncAWWAN: TSwitch;
    AccountInfo: TListBoxGroupHeader;
    ToolBar2: TToolBar;
    SettingsDetails: TLabel;
    SettingsList2: TListBox;
    SelectAcctType: TListBoxItem;
    SelectPayment: TListBoxItem;
    AcctTypes: TListBoxGroupHeader;
    PaymentCombo: TComboBox;
    CreditCard: TListBoxItem;
    Check: TListBoxItem;
    BackButton: TSpeedButton;
    SelectRenewal: TListBoxItem;
    RenewalCombo: TComboBox;
    Monthly: TListBoxItem;
    Annually: TListBoxItem;
    Quarterly: TListBoxItem;
    EdDefMail: TEdit;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    procedure ListBoxItemTab1Click(Sender: TObject);
    procedure EdDefMailExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdDefMailChange(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SwSyncAWWANSwitch(Sender: TObject);
    procedure SwSyncAWiFiSwitch(Sender: TObject);
    procedure SwSyncAUSBSwitch(Sender: TObject);
  private
    { Private declarations }
    FEditMode: Boolean;
    procedure SalvaDados();
  public
    { Public declarations }
  end;

var
  FmSincroSettings: TFmSincroSettings;

implementation

uses UnDmkRDBMs, ModApp;

{$R *.fmx}

procedure TFmSincroSettings.EdDefMailChange(Sender: TObject);
begin
  LbiDefMmail.ItemData.Detail := EdDefMail.Text;
end;

procedure TFmSincroSettings.EdDefMailExit(Sender: TObject);
begin
  LbiDefMmail.ItemData.Detail := EdDefMail.Text;
  SalvaDados();
end;

procedure TFmSincroSettings.FormCreate(Sender: TObject);
begin
  FEditMode := False;
end;

procedure TFmSincroSettings.FormHide(Sender: TObject);
begin
  FEditMode := False;
end;

procedure TFmSincroSettings.FormShow(Sender: TObject);
begin
  FEditMode := False;
  //
  TabControl1.ActiveTab := TabItem1;
  DmModApp.ReopenOpcsBgsmn();
  //
  EdDefMail.Text := DmModApp.QrOpcsBgsmn.FieldByName('DefMail').AsString;
  SwSyncAUSB.IsChecked  := Geral.IntToBool(DmModApp.QrOpcsBgsmn.FieldByName('SyncAUSB').AsInteger);
  SwSyncAWiFi.IsChecked := Geral.IntToBool(DmModApp.QrOpcsBgsmn.FieldByName('SyncAWiFi').AsInteger);
  SwSyncAWWAN.IsChecked := Geral.IntToBool(DmModApp.QrOpcsBgsmn.FieldByName('SyncAWWAN').AsInteger);
  //
  FEditMode := True;
end;

procedure TFmSincroSettings.ListBoxItemTab1Click(Sender: TObject);
begin
  ChangeTabAction2.ExecuteTarget(self);
end;

procedure TFmSincroSettings.SalvaDados();
const
  Codigo = 1;
var
  DefMail: String;
  SyncAUSB, SyncAWiFi, SyncAWWAN: Integer;
  SQLType: TSQLType;
begin
  if not FEditMode then
    Exit;
  //
  DefMail   := EdDefMail.Text;
  SyncAUSB  := Geral.BoolToInt(SwSyncAUSB.IsChecked);
  SyncAWiFi := Geral.BoolToInt(SwSyncAWiFi.IsChecked);
  SyncAWWAN := Geral.BoolToInt(SwSyncAWWAN.IsChecked);
  //
  DmModApp.ReopenOpcsBgsmn();
  if DmModApp.QrOpcsBgsmn.RecordCount > 0 then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
  DmModApp.QrOpcsBgsmn.Close;
  if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, SQLType, 'opcsbgsmn', False, [
    'DefMail', 'SyncAUSB', 'SyncAWiFi', 'SyncAWWAN'], [
    'Codigo'
  ], [
    DefMail, SyncAUSB, SyncAWiFi, SyncAWWAN
  ], [
    Codigo
  ], True) then
  begin
    //
  end;
end;

procedure TFmSincroSettings.SwSyncAUSBSwitch(Sender: TObject);
begin
  SalvaDados();
end;

procedure TFmSincroSettings.SwSyncAWiFiSwitch(Sender: TObject);
begin
  SalvaDados();
end;

procedure TFmSincroSettings.SwSyncAWWANSwitch(Sender: TObject);
begin
  SalvaDados();
end;

end.
