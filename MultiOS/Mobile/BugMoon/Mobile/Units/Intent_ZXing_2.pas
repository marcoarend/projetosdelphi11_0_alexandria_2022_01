unit Intent_zxing;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Memo,
  //
  System.Rtti,
  FMX.Helpers.Android,
  Androidapi.JNI.Net,
  Androidapi.JNI.GraphicsContentViewText,
  Androidapi.JNI.JavaTypes,
  FMX.platform,
  FMX.Platform.Android;

type
  TFmIntent_ZXing_2 = class(TForm)
    Memo1: TMemo;
    PanelOpt: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    SBTitle: TSpeedButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBTitleClick(Sender: TObject);
  private
    { Private declarations }
    FClipboardService: IFMXClipboardService;
    FClipboardValue: TValue;
    FZXingCalled: Boolean;
    procedure CallZXing(const ACodeMode: string);
    function IsIntentCallable(const AIntent: JIntent): Boolean;
    function GetZXingIntent: JIntent;
    procedure ClipboardSave;
    procedure ClipboardBack;
    procedure ShowInfo(const AInfo: string);
    function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
    procedure CheckEnvironment;
    procedure OpenURL(const AURL: string);
  public
    { Public declarations }
  end;

var
  FmIntent_ZXing_2: TFmIntent_ZXing_2;

implementation

{$R *.fmx}

const
  CodeModes: array[0..2] of string = ('PRODUCT_MODE', 'QR_CODE_MODE', 'SCAN_MODE');

procedure TFmIntent_ZXing_2.Button1Click(Sender: TObject);
begin
  // 0, 1, 2
  CallZXing(CodeModes[TButton(Sender).Tag]);
end;

procedure TFmIntent_ZXing_2.CallZXing(const ACodeMode: string);
var
  LIntent: JIntent;
begin
  ClipboardSave;
  FClipboardService.SetClipboard('');
  LIntent := GetZXingIntent();
  LIntent.putExtra(StringToJString('SCAN_MODE'), StringToJString(ACodeMode));
  SharedActivity.startActivityForResult(LIntent, 0);
  FZXingCalled := True;
end;

procedure TFmIntent_ZXing_2.CheckEnvironment;
var
  LFMXApplicationEventService: IFMXApplicationEventService;
  LIsZXingCallable: Boolean;
  LStr: string;
  b1, b2: Boolean;
begin
  LStr := '';
  try
    if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService,
    IInterface(LFMXApplicationEventService)) then
      LFMXApplicationEventService.SetApplicationEventHandler(HandleAppEvent)
    else
      LStr := 'Erro. Este SO n�o suporta IFMXApplicationEventService!';
    //
    if not TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService,
    IInterface(FClipboardService)) then
      LStr := LStr + sLineBreak + 'Erro. Este SO n�o suporta IFMXClipboardService!';
    LIsZXingCallable := IsIntentCallable(GetZXingIntent);
    if not LIsZXingCallable then
    begin
      SBTitle.Text := 'Abrir p�gina de download ... ZXing';
      SBTitle.OnClick := SBTitleClick;
      LStr := LStr + sLineBreak + 'N�o foi encontrado o aplicativo ZXing, reinicie ap�s a sua instala��o';
    end else
      SBTitle.Text := 'Google ZXing encontrado!';
    //
    ShowInfo(LStr);
    //
{
  PanelOpt.Enabled := Assigned(LFMXApplicationEventService) and
                      Assigned(FClipboardService) and LIsZXingCallable;
}
    b1 := Assigned(LFMXApplicationEventService);
    b2 := Assigned(FClipboardService);
    PanelOpt.Enabled := b1 and b2 and LIsZXingCallable;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;
end;

procedure TFmIntent_ZXing_2.ClipboardBack;
begin
  FClipboardService.SetClipboard(FClipboardValue);
end;

procedure TFmIntent_ZXing_2.ClipboardSave;
begin
  FClipboardValue := FClipboardService.GetClipboard;
end;

procedure TFmIntent_ZXing_2.FormCreate(Sender: TObject);
begin
  SBTitle.OnClick := nil;
  CheckEnvironment;
end;

function TFmIntent_ZXing_2.GetZXingIntent: JIntent;
const
  GOOGLE_ZXING = 'com.google.zxing.client.android.SCAN';
  GOOGLE_ZXING_PACKAGE = 'com.google.zxing.client.android';
begin
  Result := TJIntent.JavaClass.init(StringToJString(GOOGLE_ZXING));
  Result.setPackage(StringToJString(GOOGLE_ZXING_PACKAGE));
end;

function TFmIntent_ZXing_2.HandleAppEvent(AAppEvent: TApplicationEvent;
  AContext: TObject): Boolean;
var
  LResult: string;
begin
  if FZXingCalled and (AAppEvent = TApplicationEvent.aeBecameActive) then
  begin
    LResult := FClipboardService.GetClipboard.ToString;
    if LResult.IsEmpty then
      ShowInfo('Cancelar digitaliza��o')
    else
      ShowInfo(LResult);
    ClipboardBack;
    FZXingCalled := False;
  end;
  Result := True;
end;

function TFmIntent_ZXing_2.IsIntentCallable(const AIntent: JIntent): Boolean;
var
  LJPackageManager: JPackageManager;
begin
  try
    Result := False;
    if Assigned(AIntent) then
    begin
      LJPackageManager := SharedActivityContext.getPackageManager;
      Result := LJPackageManager.queryIntentActivities(AIntent,
        TJPackageManager.JavaClass.MATCH_DEFAULT_ONLY).size <> 0;
    end;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;
end;

procedure TFmIntent_ZXing_2.OpenURL(const AURL: string);
var
  LIntent: JIntent;
begin
  LIntent := TJIntent.JavaClass.init(TJIntent.JavaClass.ACTION_VIEW,
    TJnet_Uri.JavaClass.parse(StringToJString(AURL)));
  SharedActivity.startActivity(LIntent);
end;

procedure TFmIntent_ZXing_2.ShowInfo(const AInfo: string);
begin
  Memo1.Text := AInfo;
end;

procedure TFmIntent_ZXing_2.SBTitleClick(Sender: TObject);
begin
  OpenURL('http://code.google.com/p/zxing/downloads/list');
end;

end.
