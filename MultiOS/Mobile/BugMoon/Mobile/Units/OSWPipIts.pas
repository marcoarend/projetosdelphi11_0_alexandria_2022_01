unit OSWPipIts;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls, FMX.ListBox, FMX.Ani, FMX.Layouts, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFmOSWPipIts = class(TForm)
    LBPerguntas: TListBox;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem14: TListBoxItem;
    ListBoxItem16: TListBoxItem;
    BitmapAnimation1: TBitmapAnimation;
    BitmapAnimation2: TBitmapAnimation;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    ListBoxItem17: TListBoxItem;
    ListBoxItem18: TListBoxItem;
    ListBoxItem19: TListBoxItem;
    ListBoxItem20: TListBoxItem;
    ListBoxHeader1: TListBoxHeader;
    Label3: TLabel;
    SearchBox1: TSearchBox;
    QrOSPipIts: TFDQuery;
  private
    { Private declarations }
    procedure ListBoxItemClick(Sender: TObject);
  public
    { Public declarations }
    FControle: Integer;
    //
    procedure LimpaPerguntas();
    procedure AtualizaPerguntas();
  end;

var
  FmOSWPipIts: TFmOSWPipIts;

implementation

{$R *.fmx}

uses ModApp, UnDmkRDBMs, UnGeral, UnMyObjects;

{ TFmOSWPipIts }

procedure TFmOSWPipIts.AtualizaPerguntas();
var
  LHeader : TListBoxGroupHeader;
  LItem : TListBoxItem;
  //I: integer;
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT',
  'pbs.Nome BinarSIM, pbn.Nome BinarNAO,',
  'pcp.Nome NO_PERGUNTA,',
  'CASE opi.Respondido WHEN 1 THEN',
  '  CASE Funcoes',
  '    WHEN 0 THEN '' ''',
  '    WHEN 1 THEN CASE opi.RespBin',
  '      WHEN 0 THEN pbn.Nome',
  '      WHEN 1 THEN pbs.Nome',
  '      ELSE '' '' END',
  '    WHEN 2 THEN opi.RespQtd',
  '    WHEN 3 THEN ppr.UsoQtd ' + QuotedStr('unidades da isca ID') +  ' + ppr.GraGruX',
  '    WHEN 4 THEN pai.Nome',
  '    WHEN 5 THEN SUBSTR(opi.RespTxtLvr, 1, 255)',
  '    ELSE ''??6??'' END',
  'ELSE '' '' END RESPOSTA,',
  'opi.*',
  'FROM ospipits opi',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta',
  'LEFT JOIN prgbincad pbs ON pbs.Codigo=opi.BinarCad1',
  'LEFT JOIN prgbincad pbn ON pbn.Codigo=opi.BinarCad0',
  'LEFT JOIN oswpipitsp ppr ON ppr.Conta=opi.Conta',
  'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts',
  'WHERE opi.Controle=' + Geral.FF0(FControle),
  'ORDER BY Controle, SuperOrd, SuperSub, SobreOrd, Ordem, SubOrdem',
  '']);
  //
  LbPerguntas.BeginUpdate;
  LbPerguntas.Items.Clear;
  //
  LHeader := TListBoxGroupHeader.Create(Owner);
  LHeader.Parent := LbPerguntas;
  LHeader.Text := 'Perguntas';
  //
  QrOSPipIts.First;
  while not QrOSPipIts.Eof do
  begin
    LItem := TListBoxItem.Create(Owner);
    LItem.StyleLookup := 'listboxitembottomdetail';
    LItem.Parent := LbPerguntas;
    LItem.Text := QrOSPipIts.FieldByName('NO_PERGUNTA').AsString;
    LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aDetail;
    LItem.ItemData.Detail := QrOSPipIts.FieldByName('RESPOSTA').AsString;
    //LItem.Data := QrOSPipIts.FieldByName/('NO_PrgLstCab').AsString;
    LItem.Tag := QrOSPipIts.FieldByName('Conta').AsInteger;
    LItem.OnClick := ListBoxItemClick;
    LItem.Height := LItem.Height * MyObjects.PlatformListBoxItemFator();
    //LItem.Font.Size := LItem.Font.Size * 2;
    //
    QrOSPipIts.Next;
  end;
  LbPerguntas.EndUpdate;
end;

procedure TFmOSWPipIts.LimpaPerguntas();
begin
  LbPerguntas.BeginUpdate;
  LbPerguntas.Items.Clear;
  LbPerguntas.EndUpdate;
end;

procedure TFmOSWPipIts.ListBoxItemClick(Sender: TObject);
begin
  Geral.MB_Info('ListBoxItemClick(' + Geral.FF0(TListBoxItem(Sender).Tag) + ')'); // Conta
end;

end.
