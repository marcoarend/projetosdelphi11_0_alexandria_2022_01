//http://john.whitham.me.uk/xe5/
unit Intent_zxing;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,

  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Memo, FMX.platform, FMX.Platform.Android,
  FMX.Helpers.Android, FMX.Edit,
  //
  System.Rtti,

  Androidapi.JNI.Net, Androidapi.JNI.GraphicsContentViewText,
  Androidapi.JNI.JavaTypes,

  UnGeral;

type
  TFmIntent_ZXing = class(TForm)
    MeTextoScan: TMemo;
    PanelOpt: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Panel2: TPanel;
    SBTitle: TSpeedButton;
    Panel4: TPanel;
    Label3: TLabel;
    Panel1: TPanel;
    BtOK: TButton;
    BtDesiste: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBTitleClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure MeTextoScanChange(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
  private
    { Private declarations }
    FClipboardService: IFMXClipboardService;
    FClipboardValue: TValue;
    FZXingCalled: Boolean;
    function IsIntentCallable(const AIntent: JIntent): Boolean;
    function GetZXingIntent: JIntent;
    procedure ClipboardSave;
    procedure ClipboardBack;
    function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
    procedure CheckEnvironment;
    procedure OpenURL(const AURL: string);
  public
    { Public declarations }
    FCodigoEscaneado: String;
    FEdit: TEdit;
    //
    procedure CallZXing(const ACodeMode: string);
  end;

var
  FmIntent_ZXing: TFmIntent_ZXing;

implementation

{$R *.fmx}

const
  CodeModes: array[0..2] of string = ('PRODUCT_MODE', 'QR_CODE_MODE', 'SCAN_MODE');

procedure TFmIntent_ZXing.BtDesisteClick(Sender: TObject);
begin
  //ModalResult := mrCancel;
  Hide;
end;

procedure TFmIntent_ZXing.Button1Click(Sender: TObject);
begin
  // 0, 1, 2
  CallZXing(CodeModes[TButton(Sender).Tag]);
end;

procedure TFmIntent_ZXing.CallZXing(const ACodeMode: string);
var
  LIntent: JIntent;
begin
  ClipboardSave;
  FClipboardService.SetClipboard('');
  LIntent := GetZXingIntent();
  LIntent.putExtra(StringToJString('SCAN_MODE'), StringToJString(ACodeMode));
  SharedActivity.startActivityForResult(LIntent, 0);
  FZXingCalled := True;
end;

procedure TFmIntent_ZXing.CheckEnvironment;
var
  LFMXApplicationEventService: IFMXApplicationEventService;
  LIsZXingCallable: Boolean;
  LStr: string;
  b1, b2: Boolean;
begin
  LStr := '';
  try
    if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService,
    IInterface(LFMXApplicationEventService)) then
      LFMXApplicationEventService.SetApplicationEventHandler(HandleAppEvent)
    else
      LStr := 'Erro. Este SO n�o suporta IFMXApplicationEventService!';
    //
    if not TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService,
    IInterface(FClipboardService)) then
      LStr := LStr + sLineBreak + 'Erro. Este SO n�o suporta IFMXClipboardService!';
    LIsZXingCallable := IsIntentCallable(GetZXingIntent);
    if not LIsZXingCallable then
    begin
      SBTitle.Text := 'Abrir p�gina de download ... ZXing';
      SBTitle.OnClick := SBTitleClick;
      LStr := LStr + sLineBreak + 'N�o foi encontrado o aplicativo ZXing, reinicie ap�s a sua instala��o';
    end else
      SBTitle.Text := 'Google ZXing encontrado!';
    //
    if LStr.Trim <> '' then
      Geral.MB_Aviso(LStr);
    //
{
  PanelOpt.Enabled := Assigned(LFMXApplicationEventService) and
                      Assigned(FClipboardService) and LIsZXingCallable;
}
    b1 := Assigned(LFMXApplicationEventService);
    b2 := Assigned(FClipboardService);
    PanelOpt.Enabled := b1 and b2 and LIsZXingCallable;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;
end;

procedure TFmIntent_ZXing.ClipboardBack;
begin
  FClipboardService.SetClipboard(FClipboardValue);
end;

procedure TFmIntent_ZXing.ClipboardSave;
begin
  FClipboardValue := FClipboardService.GetClipboard;
end;

procedure TFmIntent_ZXing.FormCreate(Sender: TObject);
begin
  FCodigoEscaneado := '';
  SBTitle.OnClick := nil;
  CheckEnvironment;
end;

procedure TFmIntent_ZXing.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack then
  begin
    BtDesisteClick(Self);
    //
    Key := 0;
  end;
end;

function TFmIntent_ZXing.GetZXingIntent: JIntent;
const
  GOOGLE_ZXING = 'com.google.zxing.client.android.SCAN';
  GOOGLE_ZXING_PACKAGE = 'com.google.zxing.client.android';
begin
  Result := TJIntent.JavaClass.init(StringToJString(GOOGLE_ZXING));
  Result.setPackage(StringToJString(GOOGLE_ZXING_PACKAGE));
end;

function TFmIntent_ZXing.HandleAppEvent(AAppEvent: TApplicationEvent;
  AContext: TObject): Boolean;
var
  LResult: string;
begin
  if FZXingCalled and (AAppEvent = TApplicationEvent.aeBecameActive) then
  begin
    LResult := FClipboardService.GetClipboard.ToString;
    if LResult.IsEmpty then
      Geral.MB_Aviso('Digitaliza��o cancelada!')
    else
    begin
      FCodigoEscaneado := LResult;
      MeTextoScan.Text := LResult;
      if FEdit <> nil then
      begin
        FEdit.Text := LResult;
        FEdit := nil;
        Hide;
      end else
        Geral.MB_Aviso('FEdit = nil');
    end;
    ClipboardBack;
    FZXingCalled := False;
  end;
  Result := True;
end;

function TFmIntent_ZXing.IsIntentCallable(const AIntent: JIntent): Boolean;
var
  LJPackageManager: JPackageManager;
begin
  Result := False;
  try
    Result := False;
    if Assigned(AIntent) then
    begin
      LJPackageManager := SharedActivityContext.getPackageManager;
      Result := LJPackageManager.queryIntentActivities(AIntent,
        TJPackageManager.JavaClass.MATCH_DEFAULT_ONLY).size <> 0;
    end;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;
end;

procedure TFmIntent_ZXing.MeTextoScanChange(Sender: TObject);
begin
  BtOK.Enabled := MeTextoScan.Text.Trim <> '';
end;

procedure TFmIntent_ZXing.OpenURL(const AURL: string);
var
  LIntent: JIntent;
begin
  LIntent := TJIntent.JavaClass.init(TJIntent.JavaClass.ACTION_VIEW,
    TJnet_Uri.JavaClass.parse(StringToJString(AURL)));
  SharedActivity.startActivity(LIntent);
end;

procedure TFmIntent_ZXing.SBTitleClick(Sender: TObject);
begin
  OpenURL('http://code.google.com/p/zxing/downloads/list');
end;

end.
