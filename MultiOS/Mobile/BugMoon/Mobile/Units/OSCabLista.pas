// Demonstrates the "MultiDetailListItem" TListView appearance.
// Install the SampleListViewMultiDetailApparancePackage before opening this form.
unit OSCabLista;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox,
  FMX.TabControl, FMX.Objects, MultiDetailAppearanceU, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
{$IFDEF ANDROID}
    FMX.Platform.Android,
{$ENDIF}
  UnGeral, UnMyObjects, UnDmkEnums;

type
  TFmOSCabLista = class(TForm)
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    SpeedButtonLiveBindings: TSpeedButton;
    ToolBar2: TToolBar;
    SpeedButtonFill: TSpeedButton;
    QrOSWCab: TFDQuery;
    Image1: TImage;
    Panel4: TPanel;
    Label3: TLabel;
    ToggleEditMode: TSpeedButton;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonFillClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure RefreshListaOSs();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmOSCabLista: TFmOSCabLista;

implementation

uses UnDmkRDBMs, ModApp, OSWPipMon, Principal;

{$R *.fmx}

procedure TFmOSCabLista.FormCreate(Sender: TObject);
begin
  // Evitar uso. Criado ao iniciar o app.
{
  J� setado em tempo de desenvolvimento
  ListViewMultiDetail.ItemAppearanceObjects.ItemObjects.Image.Visible := False;
}
end;

procedure TFmOSCabLista.FormShow(Sender: TObject);
begin
  RefreshListaOSs();
end;

procedure TFmOSCabLista.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  Valor1, Valor2: Variant;
  Localizador, Empresa, LstCusPrd: Integer;
begin
  if not ListViewMultiDetail.EditMode then
  begin
    Localizador := Geral.IMV(Trim(AItem.Text.Substring(2)));
    if DmModApp.ObtemCamposRegUni_2(Self, 'oswcab', 'Codigo', Localizador,
    'Empresa', 'LCPUsed', Valor1, Valor2, True) then
    begin
      Empresa := Valor1;
      LstCusPrd := Valor2;
      //
      FmOSWPipMon.LimpaListaDePMVs();
      FmOSWPipMon.FOSWCab := Localizador;
      FmOSWPipMon.FEmpresa := Empresa;
      FmOSWPipMon.FLstCusPrd := LstCusPrd;
      FmOSWPipMon.Show;
      //
      FmOSWPipMon.AtualizaPMVs();
      //
      DmModApp.LogRastreio(laiOSWCab, laeSelected, Localizador);
    end;
  end;
end;

procedure TFmOSCabLista.RefreshListaOSs;
var
  //I: Integer;
  LItem: TListViewItem;
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOSWCab, DmModApp.FDAppDB, [
  'SELECT * FROM oswcab',
  '']);
  //
  ListViewMultiDetail.BeginUpdate;
  ListViewMultiDetail.Items.Clear;
  //
  try
    QrOSWCab.First;
    while not QrOSWCab.Eof do
    begin
      LItem := ListViewMultiDetail.Items.Add;
      LItem.Text := 'OS ' + Geral.FF0(QrOSWCab.FieldByName('Codigo').AsInteger);//Format('Text %d', [I]);
      // Update data managed by custom appearance
      LItem.Data[TMultiDetailAppearanceNames.Detail1] := QrOSWCab.FieldByName('NO_ENT').AsString;
      LItem.Data[TMultiDetailAppearanceNames.Detail2] := QrOSWCab.FieldByName('NO_SiapTerCad').AsString;
      LItem.Data[TMultiDetailAppearanceNames.Detail3] := QrOSWCab.FieldByName('NO_ENTICONTAT').AsString;
      LItem.BitmapRef := Image1.Bitmap;
      //
      case QrOSWCab.FieldByName('Respondido').AsInteger of
        0: LItem.Accessory := FMX.ListView.Types.TAccessoryType.More;
        1: LItem.Accessory := FMX.ListView.Types.TAccessoryType.Checkmark;
        else LItem.Accessory := FMX.ListView.Types.TAccessoryType.Detail;
      end;
      //
      QrOSWCab.Next;
    end;
  finally
    ListViewMultiDetail.EndUpdate;
  end;
end;

procedure TFmOSCabLista.SpeedButtonFillClick(Sender: TObject);
begin
  RefreshListaOSs();
{$IFDEF ANDROID}
    // Funciona muito bem!!!
    //MainActivity.finish;
{$ENDIF}
end;

procedure TFmOSCabLista.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmOSCabLista.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;

end.
