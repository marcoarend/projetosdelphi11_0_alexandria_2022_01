unit OSWPipMon;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls, FMX.ListBox, FMX.Layouts, FMX.Colors, FMX.TabControl, FMX.Ani,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  UnGeral, UnDmkEnums, UnFMX_Vars, UnProjGroup_Consts;

type
  TFmOSWPipMon = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    Panel1: TPanel;
    LbPMVs: TListBox;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem14: TListBoxItem;
    ListBoxItem16: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    ListBoxItem17: TListBoxItem;
    ListBoxItem18: TListBoxItem;
    ListBoxItem19: TListBoxItem;
    ListBoxItem20: TListBoxItem;
    ListBoxHeader1: TListBoxHeader;
    SearchBox1: TSearchBox;
    BitmapAnimation1: TBitmapAnimation;
    BitmapAnimation2: TBitmapAnimation;
    QrOSWPipMon: TFDQuery;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    EdQRCode: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure EdQRCodeChange(Sender: TObject);
  private
    { Private declarations }
    procedure ListBoxItemClick(Sender: TObject);
    procedure MostraFormOSWPipIts(PMV, RespMetodo: Integer);
  public
    { Public declarations }
    FOSWCab, FEmpresa, FLstCusPrd: Integer;
    procedure AtualizaPMVs();
    procedure LimpaListaDePMVs();
  end;

var
  FmOSWPipMon: TFmOSWPipMon;

implementation

uses UnDmkRDBMs, ModApp, UnDmkDevice,
{$IF DEFINED(ANDROID)}
  Intent_ZXing,
{$ENDIF}
UnMyObjects, OSWPipIts, OSWPipItsRsp;

{$R *.fmx}

procedure TFmOSWPipMon.AtualizaPMVs();
var
  LHeader : TListBoxGroupHeader;
  LItem : TListBoxItem;
  //I: integer;
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOSWPipMon, DmModApp.FDAppDB, [
  'SELECT * FROM oswpipmon ',
  'WHERE Codigo=' + Geral.FF0(FOSWCab),
  'ORDER BY Respondido, NO_PIP ',
  '']);
  //
  LbPMVs.BeginUpdate;
  LbPMVs.Items.Clear;
  //
  LHeader := TListBoxGroupHeader.Create(Owner);
  LHeader.Parent := LbPMVs;
  LHeader.Text := 'PMVs';
  //
  QrOSWPipMon.First;
  while not QrOSWPipMon.Eof do
  begin
    LItem := TListBoxItem.Create(Owner);
    LItem.StyleLookup := 'listboxitembottomdetail';
    LItem.Parent := LBPMVs;
    LItem.Text := QrOSWPipMon.FieldByName('NO_PIP').AsString;
    //LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aDetail;
    case QrOSWPipMon.FieldByName('Respondido').AsInteger of
      0: LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aMore;
      1: LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aCheckmark;
      else LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aDetail;
    end;
    LItem.ItemData.Detail := QrOSWPipMon.FieldByName('NO_PrgLstCab').AsString;
    //LItem.Data := QrOSWPipMon.FieldByName('NO_PrgLstCab').AsString;
    LItem.Tag := QrOSWPipMon.FieldByName('PipCad').AsInteger;
    LItem.OnClick := ListBoxItemClick;
    LItem.Height := LItem.Height * MyObjects.PlatformListBoxItemFator();
    //LItem.Font.Size := LItem.Font.Size * 2;
    //
    QrOSWPipMon.Next;
  end;
  LbPMVs.EndUpdate;
end;

procedure TFmOSWPipMon.EdQRCodeChange(Sender: TObject);
var
  PMV: Integer;
begin
  if EdQrCode.Text <> '' then
  begin
    PMV := Geral.IMV(EdQrCode.Text);
    MostraFormOSWPipIts(PMV, CO_RespMetodo_COD_MOBILE_QRCode);
  end;
end;

procedure TFmOSWPipMon.FormCreate(Sender: TObject);
begin
  // Evitar uso. Criado ao iniciar o app.
end;

procedure TFmOSWPipMon.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack then
    Modalresult := mrCancel;
end;

procedure TFmOSWPipMon.FormShow(Sender: TObject);
begin
  //AtualizaPMVs();
end;

procedure TFmOSWPipMon.LimpaListaDePMVs();
begin
  LbPMVs.BeginUpdate;
  LbPMVs.Items.Clear;
  LbPMVs.EndUpdate;
end;

procedure TFmOSWPipMon.ListBoxItemClick(Sender: TObject);
var
 PMV: Integer;
begin
  //Geral.MB_Info('ListBoxItemClick(' + TListBoxItem(Sender).ItemData.Detail + ')'); > ISCAGEM PARA REODORES
  //Geral.MB_Info('ListBoxItemClick(' + TListBoxItem(Sender).Text + ')');  > P001
  //Geral.MB_Info('ListBoxItemClick(' + Geral.FF0(TListBoxItem(Sender).Tag) + ')'); // Controle
  PMV := TListBoxItem(Sender).Tag;
  MostraFormOSWPipIts(PMV, CO_RespMetodo_COD_MOBILE_Manual);
end;

procedure TFmOSWPipMon.MostraFormOSWPipIts(PMV, RespMetodo: Integer);
var
  Controle: Integer;
begin
  if PMV = 0 then
  begin
    Geral.MB_Aviso('Nenhum PMV foi informado!');
    Exit;
  end;
  //
  if not QrOSWPipMon.Locate('PipCad', PMV) then
  begin
    Geral.MB_Aviso('O PMV ID ' + Geral.FF0(PMV) + ' n�o foi localizado neste lugar!');
    Exit;
  end;
  //
  //FmOSWPipItsRsp.LimpaPerguntas();
  Controle := QrOSWPipMon.FieldByName('Controle').AsInteger;
  FmOSWPipItsRsp.FRespMetodo := RespMetodo;
  FmOSWPipItsRsp.FControle := Controle;
  FmOSWPipItsRsp.FOSCab := FOSWCab;
  FmOSWPipItsRsp.FEmpresa := FEmpresa;
  FmOSWPipItsRsp.FLstCusPrd := FLstCusPrd;
  FmOSWPipItsRsp.FAntUsoQtd := QrOSWPipMon.FieldByName('AntUsoQtd').AsFloat;
  FmOSWPipItsRsp.FAntUsoGGX := QrOSWPipMon.FieldByName('AntUsoGGX').AsInteger;
  FmOSWPipItsRsp.FAntUsoVCD := QrOSWPipMon.FieldByName('AntUsoVCD').AsInteger;
  //FmOSWPipItsRsp.FGraGruX := QrOSWPipMon.FieldByName('GraGruX').AsInteger;
  //FmOSWPipItsRsp.ReopenOSPipIts();
  FmOSWPipItsRsp.ReopenOSPipIts();
  FmOSWPipItsRsp.LaPMVNome.Text := QrOSWPipMon.FieldByName('NO_PIP').AsString;
  FmOSWPipItsRsp.Show;
  FmOSWPipItsRsp.InicializaPerguntas();
end;

procedure TFmOSWPipMon.SpeedButton1Click(Sender: TObject);
var
{$IF DEFINED(ANDROID)}
  //_Intent_ZXing: TForm;
{$ELSEIF DEFINED(MSWINDOWS)}
  CodigoEscaneado: String;
{$ENDIF}
  //Controle,
  PMV: Integer;
begin
{$IF DEFINED(ANDROID)}
  PMV := 0;
{ Tentativa 1.
  _Intent_ZXing := MyObjects.CriaFm_SM(TFmIntent_ZXing);
  _Intent_ZXing.ShowModal(
    procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
        PMV := Geral.IMV(TFmIntent_ZXing(_Intent_ZXing).FCodigoEscaneado);
        MostraFormOSWPipIts(PMV, CO_RespMetodo_COD_MOBILE_QRCode);
        _Intent_ZXing.DisposeOf;
      end else
      begin
        _Intent_ZXing.DisposeOf;
        FmOSWPipMon.Show;
      end;
    end);
}
{ Tentativa 2.
  FmIntent_ZXing.ShowModal(
    procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
        PMV := Geral.IMV(TFmIntent_ZXing(FmIntent_ZXing).FCodigoEscaneado);
        // Deve ser antes!!! Memory Leak ao encerrar aplicativo!
        FmIntent_ZXing.Hide;
        //
        MostraFormOSWPipIts(PMV, CO_RespMetodo_COD_MOBILE_QRCode);
      end else
      begin
        //FmOSWPipMon.Show;
        FmIntent_ZXing.Hide;
      end;
    end);
}
{ Tentativa 3.}
  EdQrCode.Text := '';
  FmIntent_ZXing.FEdit := EdQrCode;
  FmIntent_ZXing.CallZXing('QR Code');
{$ELSEIF DEFINED(IOS)}
   VER!
  C:\_Compilers\MultiOS\iOS\QRCode_XE4\29485_c_243_digo_de_barras_no_delphi_para_ios.ZIP
{$ELSEIF DEFINED(MSWINDOWS)}
  if InputQuery('QRCode', 'Informe o QRCode:', CodigoEscaneado) then
  begin
    PMV := Geral.IMV(CodigoEscaneado);
    MostraFormOSWPipIts(PMV, CO_RespMetodo_COD_MOBILE_QRCode);
  end;
{$ELSE}
  {$IFDEF _SEI_LA_}
    MyName := ExtractFileName(ParamStr(0));
  {$ELSE}
    DmkDevice.PlataformaNaoImplementada('');
  {$ENDIF}
{$IFEND}
end;

end.
