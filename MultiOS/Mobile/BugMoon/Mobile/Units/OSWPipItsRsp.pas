﻿unit OSWPipItsRsp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  System.Actions, FMX.ActnList, FMX.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.Edit, FMX.ListBox,
  FMX.ExtCtrls, dmkCNClearingEdit, dmkCNEdit, dmkCNPanel, FMX.Layouts, FMX.Memo,
  FMX.VirtualKeyboard,
  ControlMover,
  UnProjGroup_Consts, UnPerfJan_Tabs, UnDmkEnums, UnFormsPF, UnMyObjects;

type
  TFmOSWPipItsRsp = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    TabItem5: TTabItem;
    Panel4: TPanel;
    Label6: TLabel;
    TabItem6: TTabItem;
    Panel1: TPanel;
    LaPMVNome: TLabel;
    QrOSPipIts: TFDQuery;
    Panel2: TPanel;
    LaTit_01_BINARIO: TLabel;
    Panel3: TPanel;
    Panel5: TPanel;
    LaTit_02_QUANTIT: TLabel;
    Panel6: TPanel;
    BtOK_2: TButton;
    EdRespQtd: TdmkCNEdit;
    Panel7: TPanel;
    Panel8: TPanel;
    LaTit_03_BXAPROD: TLabel;
    TabControl2: TTabControl;
    TabItem3_1: TTabItem;
    TabItem3_2: TTabItem;
    TabItem3_3: TTabItem;
    Panel9: TPanel;
    Label1: TLabel;
    PnGraGruX: TdmkCNPanel;
    EdGraGruX: TdmkCNEdit;
    CBGraGruX: TdmkCNClearingEdit;
    Panel10: TPanel;
    Panel11: TPanel;
    Label2: TLabel;
    Panel13: TPanel;
    Panel14: TPanel;
    EdNumLote: TClearingEdit;
    Label3: TLabel;
    Panel15: TPanel;
    SBNumLote: TSpeedButton;
    Panel12: TPanel;
    Panel16: TPanel;
    EdNumLaudo: TClearingEdit;
    Label7: TLabel;
    Panel17: TPanel;
    SBNumLaudo: TSpeedButton;
    BtOK_3_1: TButton;
    Panel18: TPanel;
    Panel19: TPanel;
    RBAcaoPrd_TrocNao: TRadioButton;
    RBAcaoPrd_TrocSim: TRadioButton;
    Panel3_2_Sim: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Label8: TLabel;
    Panel23: TPanel;
    Panel24: TPanel;
    EdNumLote_3_2_Sim: TClearingEdit;
    Label9: TLabel;
    Panel25: TPanel;
    SBNumLote_3_2_Sim: TSpeedButton;
    Panel26: TPanel;
    Panel27: TPanel;
    EdNumLaudo_3_2_Sim: TClearingEdit;
    Label10: TLabel;
    Panel28: TPanel;
    SBNumLaudo_3_2_Sim: TSpeedButton;
    Panel29: TPanel;
    BtOK_3_2_Sim: TButton;
    Panel30: TPanel;
    RBAcaoPrd_TiraNao: TRadioButton;
    RBAcaoPrd_TiraSim: TRadioButton;
    Panel31: TPanel;
    LaTit_04_ATRIBUT: TLabel;
    Panel32: TPanel;
    LaTit_05_TXTLIVR: TLabel;
    Panel33: TPanel;
    LaTit_06_NENHUM: TLabel;
    TabControl3: TTabControl;
    TabItem4_1: TTabItem;
    TabItem4_2: TTabItem;
    Panel20: TPanel;
    Panel34: TPanel;
    LaRespAtrIts: TLabel;
    dmkCNPanel1: TdmkCNPanel;
    EdRespAtrIts: TdmkCNEdit;
    CBRespAtrIts: TdmkCNClearingEdit;
    Panel35: TPanel;
    BtOK_4_1: TButton;
    Panel36: TPanel;
    MeRespAtrTxt: TMemo;
    Panel37: TPanel;
    BtOK_4_2: TButton;
    Panel38: TPanel;
    MeRespTxtLvr: TMemo;
    Panel39: TPanel;
    BtOK_5: TButton;
    CkPreencher: TCheckBox;
    QrPrgAtrIts: TFDQuery;
    QrOPI: TFDQuery;
    QrG1Any: TFDQuery;
    RGRespBinNao: TRadioButton;
    RGRespBinSim: TRadioButton;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    QrOri: TFDQuery;
    QrOPIPR: TFDQuery;
    QrGraGruWLot: TFDQuery;
    QrGraGruWLau: TFDQuery;
    QryAft: TFDQuery;
    QrGraGruX: TFDQuery;
    Panel40: TPanel;
    BtRespBinNao: TButton;
    BtRespBinSim: TButton;
    EdQtdUso_3_2_Sim: TSpinBox;
    EdUsoQtd: TSpinBox;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrOSPipItsAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure RGRespBinNaoChange(Sender: TObject);
    procedure RGRespBinSimChange(Sender: TObject);
    procedure BtOK_4_1Click(Sender: TObject);
    procedure SBNumLote_3_2_SimClick(Sender: TObject);
    procedure SBNumLaudo_3_2_SimClick(Sender: TObject);
    procedure BtOK_3_2_SimClick(Sender: TObject);
    procedure RGAcaoPrd_TrocClick(Sender: TObject);
    procedure SBNumLoteClick(Sender: TObject);
    procedure SBNumLaudoClick(Sender: TObject);
    procedure BtOK_3_1Click(Sender: TObject);
    procedure RGAcaoPrd_TiraClick(Sender: TObject);
    procedure BtOK_2Click(Sender: TObject);
    procedure BtOK_4_2Click(Sender: TObject);
    procedure BtOK_5Click(Sender: TObject);
    procedure Panel40Resize(Sender: TObject);
    procedure BtRespBinNaoClick(Sender: TObject);
    procedure BtRespBinSimClick(Sender: TObject);
    procedure EdNumLoteClick(Sender: TObject);
    procedure EdNumLote_3_2_SimClick(Sender: TObject);
    procedure EdNumLaudo_3_2_SimClick(Sender: TObject);
    procedure EdQtdUso_3_2_SimClick(Sender: TObject);
    procedure EdNumLote_3_2_SimChange(Sender: TObject);
    procedure EdNumLote_3_2_SimEnter(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { Private declarations }
    FPreenchendo, FPulaAfterScrool, FOnLoop: Boolean;
    //FListaLoops: array of array[0..3] of Integer;

    //
    //procedure CriaQryLoop(Loops, Nivel: Integer);



////////////////////////////////////////////////////////////////////////////////
///
    FControlMover: TControlMover;
    procedure GetMoveControl(Sender: TObject; FocusedControl: TControl; var MoveControl: TControl);

////////////////////////////////////////////////////////////////////////////////


    function  DesativaPipItsPr(): Integer;
    procedure Executa_01_Binario(Checked: Boolean; Index: Integer);
    procedure Executa_03_02_AcaoPrg(Index: Integer);

    procedure IncluiPipItsPr(Acao, GraGruX, ValCliDd: Integer;
              UsoQtd, UsoCusUni, UsoCusTot: Double; NumLote, NumLaudo: String);
    procedure My_QrOSPipItsAfterScroll();

    procedure PreencheLoteELaudo(GraGruX: Integer; EdLote,
              EdLaudo: TClearingEdit);
    procedure PrxmPergunta();

    procedure RePreencheAdicaoPrd();
    procedure RePreencheBinario();
    procedure RePreenchePrgAtrIts();
    procedure RePreencheQuantitativo();
    procedure RePreencheSubstituicaoPrd();
    procedure RePreencheRetiraPrd();
    procedure RePreencheRespAtrTxt();
    procedure RePreencheRespTxtLvr();

    procedure ReopenGraGruX();
    procedure ReopenQrOPI();
    procedure ReopenQrOPIPR();
    procedure RespPipIts(RespBin: Integer; RespQtd: Double; RespAtrCad,
              RespAtrIts: Integer; RespAtrTxt, RespTxtLvr: String;
              RespPrdBin, DesativSrc: Integer);
    procedure SubstituiPipItsPr(Quantidade: Double; NumLote, NumLaudo: String);
    procedure TrocaIsca(RespPrdBin, Quantidade: Integer;
              NumLote, NumLaudo: String);
{
    function  ShowDmkInputTxt(CustomEdit: TCustomEdit): String;
}
  public
    { Public declarations }
    FOSCab, FLstCusPrd, FEmpresa, FControle(*, FGraGruX*), FRespMetodo: Integer;
    FAntUsoQtd: Double;
    FAntUsoGGX, FAntUsoVCD: Integer;
    //
    procedure InicializaPerguntas();
    procedure OcultaTodosPaineis();
    procedure ReopenPrgAtrIts(PrgAtrCad: Integer);
    procedure ReopenOSPipIts();
  end;

var
  FmOSWPipItsRsp: TFmOSWPipItsRsp;

implementation

uses UnDmkRDBMs, ModApp, UnGeral, ModProd, DmkInputTxt, OSWPipMon, OSCabLista;

const
  FRangeLoops = 31;
var
  RGAcaoPrd_Troc: array[0..1] of TRadioButton;
  RGAcaoPrd_Tira: array[0..1] of TRadioButton;
{
  QrNiveis: array[0..FRangeLoops] of TFDQuery;
  QrProxms: array[0..FRangeLoops] of TFDQuery;
  QtdLoops: array[0..FRangeLoops] of Integer;
  AtuLoops: array[0..FRangeLoops] of Integer;
}
{$R *.fmx}
procedure TFmOSWPipItsRsp.BtOK_2Click(Sender: TObject);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
  RespQtd: Double;
  // Loop
  //Proximo, Nivel, I,
  ConParou, Conta: Int64;
  MaxQtd, Controle, Ativo, Filiacao: Integer;
  Qry: TFDQuery;
begin
  //CO_PRG_LST_QUANTIT = 2;
  RespBin        := 0;
  RespQtd        := EdRespQtd.ValueInt;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  DesativSrc     := 0;
  //
  if QrOSPipIts.FieldByName('LupForma').AsInteger = CO_LupForma_COD_PorLoop then
  begin
    ConParou := QrOSPipIts.FieldByName('Conta').AsLargeInt;
    Qry := TFDQuery.Create(DmModApp);
    try
      Controle := QrOSPipIts.FieldByName('Controle').AsLargeInt;
      Filiacao := QrOSPipIts.FieldByName('Filiacao').AsLargeInt;
      Ativo := 1;
      DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, 'ospipits', False, [
      'Ativo'], ['Controle', 'Filiacao'], [Ativo], [Controle, Filiacao], False);
      //
      DmkRDBMs.AbreSQLQuery0(Self, Qry, DmModApp.FDAppDB, [
      'SELECT MAX(SuperSub) SuperSub ',
      'FROM ospipits ',
      'WHERE Codigo=' + Geral.FF0(QrOSPipIts.FieldByName('Codigo').AsInteger),
      'AND Controle=' + Geral.FF0(QrOSPipIts.FieldByName('Controle').AsInteger),
      'AND Filiacao=' + Geral.FF0(QrOSPipIts.FieldByName('prglstits').AsInteger),
      '']);
      MaxQtd := Qry.FieldByName('SuperSub').AsInteger;
      //
      if Trunc(RespQtd) > MaxQtd then
      begin
        Geral.MB_Aviso('Quantidade inválida! Quantidade máxima: ' +
          Geral.FF0(Qry.FieldByName('SuperSub').AsInteger));
        Exit;
      end;
      if MaxQtd > Trunc(RespQtd) then
      begin
        DmkRDBMs.AbreSQLQuery0(Self, Qry, DmModApp.FDAppDB, [
        'SELECT DISTINCT Conta ',
        'FROM ospipits ',
        'WHERE Codigo=' + Geral.FF0(QrOSPipIts.FieldByName('Codigo').AsInteger),
        'AND Controle=' + Geral.FF0(QrOSPipIts.FieldByName('Controle').AsInteger),
        'AND Filiacao=' + Geral.FF0(QrOSPipIts.FieldByName('prglstits').AsInteger),
        'AND SuperSub>' + Geral.FF0(Trunc(RespQtd)),
        '']);
        Qry.First;
        Ativo := 0;
        while not Qry.Eof do
        begin
          Conta := Qry.FieldByName('Conta').AsLargeInt;
          DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, 'ospipits', False, [
          'Ativo'], ['Conta'], [Ativo], [Conta], False);
          //
          Qry.Next;
        end;
        //
        FPulaAfterScrool := True;
        ReopenOSPipIts();
        QrOSPipIts.Locate('Conta', ConParou);
        FPulaAfterScrool := False;
      end;
    finally
      Qry.Free;
    end;
  end;
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  PrxmPergunta();
end;

procedure TFmOSWPipItsRsp.BtOK_3_1Click(Sender: TObject);
const
  Acao = CO_AcaoPrd_Adic (*1*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
  RespQtd: Double;
  //
  GraGruX, ValCliDd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
  NumLote, NumLaudo: String;
begin
  //CO_PRG_LST_BXAPROD = 3;
  //CO_AcaoPrd_Adic = 1;
  RespBin        := 0;
  RespQtd        := 0.000;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := 1;
  DesativSrc     := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  //
  GraGruX        := EdGraGruX.ValueInt;
  UsoQtd         := EdUsoQtd.Value;
  NumLote        := EdNumLote.Text;
  NumLaudo       := EdNumLaudo.Text;
  // Ver o que fazer!
  ValCliDd       := 0; //EdValCliDd.ValueVariant;
  UsoCusUni      := 0; //EdUsoCusUni.ValueVariant;
  UsoCusTot      := 0; //EdUsoCusTot.ValueVariant;
  // Fim ver o que fazer!
  IncluiPipItsPr(Acao, GraGruX, ValCliDd, UsoQtd, UsoCusUni, UsoCusTot,
    NumLote, NumLaudo);
  //
  PrxmPergunta();
end;

procedure TFmOSWPipItsRsp.BtOK_3_2_SimClick(Sender: TObject);
var
  Quantidade, RespPrdBin: Integer;
  NumLote, NumLaudo: String;
begin
  RespPrdBin := MyObjects.RGItemIndex(RGAcaoPrd_Troc);

  Quantidade := Geral.IMV(EdQtdUso_3_2_Sim.Text);
  NumLote    := EdNumLote_3_2_Sim.Text;
  NumLaudo   := EdNumLaudo_3_2_Sim.Text;
  //
  TrocaIsca(RespPrdBin, Quantidade, NumLote, NumLaudo);
  Panel3_2_Sim.Visible := False;
end;

procedure TFmOSWPipItsRsp.BtOK_4_1Click(Sender: TObject);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_ATRIBUT = 4;
  //CO_AtrTyp_PreCad = 1;
  //
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := QrOSPipIts.FieldByName('PrgAtrCad').AsInteger;
  // CUIDADO com o CodUsu!
  if not DmkRDBMs.ObtemCodigoDeCodUsu(EdRespAtrIts, RespAtrIts,
  'Informe o item do atributo!', 'Controle') then
    Exit;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  DesativSrc     := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  PrxmPergunta();
end;

procedure TFmOSWPipItsRsp.BtOK_4_2Click(Sender: TObject);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_ATRIBUT = 4;
  //CO_AtrTyp_Avulso = 2;
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := MeRespAtrTxt.Text;
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  DesativSrc     := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  PrxmPergunta();
end;

procedure TFmOSWPipItsRsp.BtOK_5Click(Sender: TObject);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_TXTLIVR = 5;
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := MeRespTxtLvr.Text;
  RespPrdBin     := -1;
  DesativSrc     := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  PrxmPergunta();
end;

procedure TFmOSWPipItsRsp.BtRespBinNaoClick(Sender: TObject);
begin
  Executa_01_Binario(True, 0);
end;

procedure TFmOSWPipItsRsp.BtRespBinSimClick(Sender: TObject);
begin
  Executa_01_Binario(True, 1);
end;

{
procedure TFmOSWPipItsRsp.CriaQryLoop(Loops, Nivel: Integer);
var
  I: Integer;
  Conta: Int64;
begin
  if QrNiveis[Nivel] <> nil then
    QrNiveis[Nivel].Free;
  //
  QrNiveis[Nivel] := TFDQuery.Create(DmModApp);
(*
  try
*)
    DmkRDBMs.AbreSQLQuery0(Self, QrNiveis[Nivel], DmModApp.FDAppDB, [
    'SELECT * ',
    'FROM ospipits ',
    'WHERE Codigo=' + Geral.FF0(QrOSPipIts.FieldByName('Codigo').AsInteger),
    'AND Controle=' + Geral.FF0(QrOSPipIts.FieldByName('Controle').AsInteger),
    'AND Filiacao=' + Geral.FF0(QrOSPipIts.FieldByName('prglstits').AsInteger),
    'ORDER BY Controle, SuperOrd, SuperSub, ',
    '  SobreOrd, Ordem, SubOrdem ',
    '']);
    //
    QtdLoops[Nivel] := Loops;
    AtuLoops[Nivel] := 0;
    //
    PrxmPergunta();
    //
(*
  finally
    QrNiveis[Nivel].Free;
  end;
*)
end;
}

function TFmOSWPipItsRsp.DesativaPipItsPr(): Integer;
{
const
  Desativado = 1;
}
var
  Tabela: Integer;
{
  TabTxt: String;
  Conta: Int64;
}
begin
  // Nao adiciona nenhum item na tabela "oswpipitsp"
  // Mas informa no registro fonte da tabela ? que o produto nao deve mais ser monitorado
  Tabela := QrOSPipIts.FieldByName('Tabela').AsInteger;
{
  TabTxt := '';
  case Tabela of
    CO_OSPipIts_OSMONREC   : TabTxt := 'osmonrec';
    CO_OSPipIts_OSPIPITSPR : TabTxt := 'oswpipitsp';
    else
    begin
      Geral.MB_ERRO(
      'Tabela não definida em remoção de isca na procedure "DesativaPipItsPr"');
    end;
  end;
  Conta := QrOSPipIts.FieldByName('TabIdx').AsLargeInt;
  DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, TabTxt, False, [
    'Desativado'], ['Conta'], [Desativado], [Conta], True);
}
  // Não existe a tabela e/ou os dados então deverá ser feito no servidor desktop
  Result := Tabela;
end;

procedure TFmOSWPipItsRsp.EdNumLaudo_3_2_SimClick(Sender: TObject);
begin
  //EdNumLaudo_3_2_Sim.Text := ShowDmkInputTxt(EdNumLaudo_3_2_Sim);
end;

procedure TFmOSWPipItsRsp.EdNumLoteClick(Sender: TObject);
(*
var
  KeyboardService: IFMXVirtualKeyboardService;
*)
begin
(*
  if not KeyboardService.ShowVirtualKeyboard(TFmxObject(Sender)) then
    Geral.MB_Aviso('Impossível mostrar o teclado virtual!');
*)
end;

procedure TFmOSWPipItsRsp.EdNumLote_3_2_SimChange(Sender: TObject);
begin
  FControlMover.SlideControl;
end;

procedure TFmOSWPipItsRsp.EdNumLote_3_2_SimClick(Sender: TObject);
(*
var
  KeyboardService: IFMXVirtualKeyboardService;
*)
begin
//  EdNumLote_3_2_Sim.Text := ShowDmkInputTxt(EdNumLote_3_2_Sim);
  //Self.ShowKeyboard;
(*
  KeyboardService := IFMXVirtualKeyboardService
  if not KeyboardService.ShowVirtualKeyboard(TFmxObject(Sender)) then
    Geral.MB_Aviso('Impossível mostrar o teclado virtual!');
*)
end;

procedure TFmOSWPipItsRsp.EdNumLote_3_2_SimEnter(Sender: TObject);
begin
  EdNumLote_3_2_Sim.SetFocus;
end;

procedure TFmOSWPipItsRsp.EdQtdUso_3_2_SimClick(Sender: TObject);
begin
  //EdQtdUso_3_2_Sim.Text := ShowDmkInputTxt(EdQtdUso_3_2_Sim);
end;

procedure TFmOSWPipItsRsp.Executa_01_Binario(Checked: Boolean; Index: Integer);
  procedure LimpaRegistro();
  var
    RespTxtLvr, RespAtrTxt: String;
    RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
    RespQtd: Double;
  begin
    //CO_PRG_LST_ATRIBUT = 4;
    //CO_AtrTyp_Avulso = 2;
    RespBin        := 0;
    RespQtd        := 0;
    RespAtrCad     := 0;
    RespAtrIts     := 0;
    RespAtrTxt     := '';
    RespTxtLvr     := '';
    RespPrdBin     := -1;
    DesativSrc     := 0;
    //
    RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
      RespPrdBin, DesativSrc);
  end;
const
  Respondido = CO_Respondido_SIM;
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, Nivel, Controle, DesativSrc: Integer;
  RespQtd: Double;
  //Foi: Boolean;
  Conta, Cont2: Int64;
begin
  if FPreenchendo then
    Exit;
  if not Checked then
    Exit;
  //CO_PRG_LST_NENHUMA = 0;
  //CO_PRG_LST_BINARIO = 1;
  RespBin        := Index; //RGRespBin.ItemIndex;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  DesativSrc     := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  if RespBin = 1 then
  begin
    PrxmPergunta();
  end else
  begin
    //CategoryPanelGroup1.Visible := False;
    FPulaAfterScrool := True;
    try
      //
      Nivel    := QrOSPipIts.FieldByName('Nivel').AsInteger;
      Controle := QrOSPipIts.FieldByName('Controle').AsInteger;
      Conta    := QrOSPipIts.FieldByName('Conta').AsLargeInt;
      PrxmPergunta();
      while (QrOSPipIts.FieldByName('Nivel').AsInteger > Nivel) and
      (QrOSPipIts.FieldByName('Controle').AsInteger = Controle) do
      begin
        if Conta = QrOSPipIts.FieldByName('Conta').AsLargeInt then
        begin
          Hide;
          Exit;
        end;
        //
        //
        LimpaRegistro();
        //
        Cont2 := QrOSPipIts.FieldByName('Conta').AsLargeInt;
        PrxmPergunta();
        if Cont2 = QrOSPipIts.FieldByName('Conta').AsLargeInt then
        begin
          Hide;
          Exit;
        end;
      end;
    finally
      FPulaAfterScrool := False;
    end;
    My_QrOSPipItsAfterScroll();
  end;
end;

procedure TFmOSWPipItsRsp.Executa_03_02_AcaoPrg(Index: Integer);
const
  Quantidade = 0;
  NumLote    = '';
  NumLaudo   = '';
var
  RespPrdBin, GraGruX: Integer;
  //Tabela: Integer;
  //TabTxt: String;
begin
  if FPreenchendo then
    Exit;
  if Index < 0 then
    Exit;
  RespPrdBin := Index; //RGAcaoPrd_Troc.ItemIndex;
  //
  case RespPrdBin of
    0: TrocaIsca(RespPrdBin, Quantidade, NumLote, NumLaudo);
    1:
    begin
{
      Tabela := QrOSPipIts.FieldByName('Tabela').AsInteger;
      TabTxt := '';
      case Tabela of
        CO_OSPipIts_OSMONREC   : TabTxt := 'osmonrec';
        CO_OSPipIts_OSPIPITSPR : TabTxt := 'oswpipitsp';
        else
        begin
          Geral.MB_ERRO(
          'Tabela não definida em substituição de isca na procedure "RGAcaoPrd_TrocClick"');
        end;
      end;
      // ... para saber a quantidade!
      //
      DmkRDBMs.AbreSQLQuery0(Self, QrOri, DmModApp.FDAppDB, [
      'SELECT ValCliDd, UsoQtd, UsoCusUni, UsoCusTot ',
      'FROM ' + TabTxt,
      'WHERE Conta=' + Geral.FI64(QrOSPipIts.FieldByName('TabIdx').AsLargeInt),
      '']);
      EdUsoQtd.Value := QrOri.FieldByName('UsoQtd').AsFloat;
}
      EdUsoQtd.Value := FAntUsoQtd;
      //
      // Lote e laudo
      GraGruX := QrOSPipIts.FieldByName('GraGruX').AsInteger;
      PreencheLoteELaudo(GraGruX, EdNumLote_3_2_Sim, EdNumLaudo_3_2_Sim);
      //
      Panel3_2_Sim.Visible := True;
      EdQtdUso_3_2_Sim.SetFocus;
    end;
  end;
end;

procedure TFmOSWPipItsRsp.FormCreate(Sender: TObject);
begin
  // Evitar uso. Criado ao iniciar o app.
(*
  TabControl1.ActiveTab := TabItem1;
*)
  //TabControl1.Align := FMX.Types.TAlignLayout.alClient;
  //TabControl2.Align := FMX.Types.TAlignLayout.alClient;
  FOnLoop := False;

  RGAcaoPrd_Troc[0] := RBAcaoPrd_TrocNao;
  RGAcaoPrd_Troc[1] := RBAcaoPrd_TrocSim;
  //
  RGAcaoPrd_Tira[0] := RBAcaoPrd_TiraNao;
  RGAcaoPrd_Tira[1] := RBAcaoPrd_TiraSim;

  FControlMover := TControlMover.Create(Self);
  FControlMover.OnGetMoveControl := GetMoveControl;

end;

procedure TFmOSWPipItsRsp.FormHide(Sender: TObject);
begin
  //Geral.MB_Info('TFmOSWPipItsRsp.FormHide()');
end;

procedure TFmOSWPipItsRsp.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack then
  begin
    if TabControl1.ActiveTab = TabItem2 then
    begin
{
      ChangeTabAction1.Tab := TabItem1;
      ChangeTabAction1.ExecuteTarget(Self);
      ChangeTabAction1.Tab := TabItem2;
}
      Key := 0;
    end;
  end;
end;

procedure TFmOSWPipItsRsp.FormShow(Sender: TObject);
begin
  TabControl1.ActiveTab := TabItem1;
  OcultaTodosPaineis();
end;

procedure TFmOSWPipItsRsp.GetMoveControl(Sender: TObject;
  FocusedControl: TControl; var MoveControl: TControl);
begin
  MoveControl := TabControl1;
end;

procedure TFmOSWPipItsRsp.IncluiPipItsPr(Acao, GraGruX, ValCliDd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double; NumLote, NumLaudo: String);
const
  EhDiluente = 0;
  Desativado = 0;
  UsoPrc     = 0;
  UsoVal     = 0;
  UsoDec     = 0;
  UsoTot     = 0;
var
  Codigo, Controle: Integer;
  Conta, IDIts: Int64;
begin
  Codigo   := QrOSPipIts.FieldByName('Codigo').AsInteger;
  Controle := QrOSPipIts.FieldByName('Controle').AsInteger;
  Conta    := QrOSPipIts.FieldByName('Conta').AsLargeInt;
  IDIts    := 0;
  //
  IDIts := DmkRDBMs.BPGS1I64_Reaproveita(Self, dbsrcLocalServer,
    'oswpipitsp', 'IDIts', '', '', tsPos, stIns, IDIts);
  DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stIns, 'oswpipitsp', False, [
  //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'oswpipitsp', True, [
  'Codigo', 'Controle', 'Conta',
  'Acao', 'GraGruX', 'UsoQtd',
  'UsoPrc', 'UsoVal', 'UsoDec',
  'UsoTot', 'ValCliDd', 'EhDiluente',
  'Desativado', 'UsoCusUni', 'UsoCusTot',
  'NumLote', 'NumLaudo'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  Acao, GraGruX, UsoQtd,
  UsoPrc, UsoVal, UsoDec,
  UsoTot, ValCliDd, EhDiluente,
  Desativado, UsoCusUni, UsoCusTot,
  NumLote, NumLaudo], [
  IDIts], True);
end;

procedure TFmOSWPipItsRsp.InicializaPerguntas();
{
var
  I: Integer;
}
begin
{
  for I := 0 to FRangeLoops do
  begin
    QrNiveis[I] := nil;
    QrProxms[I] := nil;
    QtdLoops[I] := 0;
    AtuLoops[I] := 0;
  end;
}
  ReopenOSPipIts();
  //My_QrOSPipItsAfterScroll();
end;

procedure TFmOSWPipItsRsp.My_QrOSPipItsAfterScroll();
var
  LaTituloX: TLabel;
  TI1, TI2, TI3: TTabItem;
  Funcoes: Integer;
begin
  if QrOSPipIts.State = dsInactive then
    Exit;
  //TI1 := nil;
  TI2 := nil;
  TI3 := nil;
{
      ChangeTabAction1.Tab := TabItem3;
      ChangeTabAction1.ExecuteTarget(Self);
      ChangeTabAction1.Tab := TabItem2;
}
  if FPulaAfterScrool then
    Exit;
  //CO_PRG_LST_NENHUMA = 0;
  // ------------------------
  //CO_PRG_LST_BINARIO = 1;
  RGRespBinNao.IsChecked := False;
  RGRespBinSim.IsChecked := False;
  //
  //CO_PRG_LST_QUANTIT = 2;
  EdRespQtd.Text := '0';
  //
  //CO_PRG_LST_BXAPROD = 3;
  EdGraGruX.Text := '0';
  CBGraGruX.Text := '';
  EdUsoQtd.Text := '1';
  EdNumLote.Text := '';
  EdNumLaudo.Text := '';

  RBAcaoPrd_TrocNao.IsChecked := False;
  RBAcaoPrd_TrocSim.IsChecked := False;
  EdQtdUso_3_2_Sim.Text := '1';
  EdNumLote_3_2_Sim.Text := '';
  EdNumLaudo_3_2_Sim.Text := '';

  RBAcaoPrd_TiraNao.IsChecked := False;
  RBAcaoPrd_TiraSim.IsChecked := False;
  //
  //CO_PRG_LST_ATRIBUT = 4;
  EdRespAtrIts.Text := '0';
  CBRespAtrIts.Text := '';
  MeRespAtrTxt.Text := '';
  //
  //CO_PRG_LST_TXTLIVR = 5;
  MeRespTxtLvr.Text := '';
  //
  OcultaTodosPaineis();
(*
  try
*)
    Funcoes := QrOSPipIts.FieldByName('Funcoes').AsInteger;
(*
    Geral.MB_Erro('OK Funcoes: ' + Geral.FF0(Funcoes));
  except
    on E: Exception do
    begin
      Geral.MB_Erro('ERRO Funcoes: ' + E.Message);
    end;
  end;
*)  case Funcoes of
    //CO_PRG_LST_NENHUMA = 0;
    CO_PRG_LST_BINARIO (*1*):
    begin
      LaTituloX := LaTit_01_BINARIO; //CategoryPanel1;
      TI1 := TabItem1;
(*
      RGRespBin.Items.Clear;
*)
      RGRespBinNao.Text := QrOSPipIts.FieldByName('BinarNAO').AsString;
      RGRespBinSim.Text := QrOSPipIts.FieldByName('BinarSIM').AsString;
      BtRespBinNao.Text := QrOSPipIts.FieldByName('BinarNAO').AsString;
      BtRespBinSim.Text := QrOSPipIts.FieldByName('BinarSIM').AsString;
      //
      RePreencheBinario();
    end;
    CO_PRG_LST_QUANTIT (*2*):
    begin
      LaTituloX := LaTit_02_QUANTIT; //CategoryPanel2;
      TI1 := TabItem2;
      RePreencheQuantitativo();
    end;
    CO_PRG_LST_BXAPROD (*3*):
    begin
      LaTituloX := LaTit_03_BXAPROD; //CategoryPanel3;
      TI1 := TabItem3;
      case QrOSPipIts.FieldByName('AcaoPrd').AsInteger of
        CO_AcaoPrd_Nada (*0*):
        begin
          TI2 := nil;
        end;
        CO_AcaoPrd_Adic (*1*):
        begin
          TI2 := TabItem3_1;
          if QrGraGruX.State = dsInactive then
            ReopenGraGruX();
          RePreencheAdicaoPrd();
        end;
        CO_AcaoPrd_Troc (*2*):
        begin
          TI2 := TabItem3_2;
          RePreencheSubstituicaoPrd();
        end;
        CO_AcaoPrd_Tira (*3*):
        begin
          TI2 := TabItem3_3;
          RePreencheRetiraPrd();
        end;
      end;
    end;
    CO_PRG_LST_ATRIBUT (*4*):
    begin
      LaTituloX := LaTit_04_ATRIBUT; //CategoryPanel4;
      TI1 := TabItem4;
      case QrOSPipIts.FieldByName('AtrTyp').AsInteger of
        CO_AtrTyp_Indefi: (*0*) ; // Nada
        CO_AtrTyp_PreCad (*1*): // Pré-cadastrado
        begin
          TI3 := TabItem4_1;
          LaRespAtrIts.Text := QrOSPipIts.FieldByName('NO_PrgAtrCad').AsString;
          ReopenPrgAtrIts(QrOSPipIts.FieldByName('PrgAtrCad').AsInteger);
          //
          RePreenchePrgAtrIts();
        end;
        CO_AtrTyp_Avulso (*2*): // Texto avulso
        begin
          TI3 := TabItem4_2;
          RePreencheRespAtrTxt();
        end;
      end;
    end;
    CO_PRG_LST_TXTLIVR (*5*):
    begin
      LaTituloX := LaTit_05_TXTLIVR; //CategoryPanel5;
      TI1 := TabItem5;
      RePreencheRespTxtLvr();
    end;
    else
    begin
      LaTituloX := LaTit_06_NENHUM;
      TI1 := TabItem6;
    end;
  end;
  //
  if TabControl1.ActiveTab <> TI1 then
  begin
    ChangeTabAction1.Tab := TI1;
    ChangeTabAction1.ExecuteTarget(Self);
    ChangeTabAction1.Tab := TabControl1.ActiveTab;
    //
    TI1.SetFocus;
  end;
  LaTituloX.Text := QrOSPipIts.FieldByName('NO_Pergunta').AsString;
  //
  if QrOSPipIts.FieldByName('Funcoes').AsInteger = CO_PRG_LST_BXAPROD then
  begin
    if QrOSPipIts.FieldByName('AcaoPrd').AsInteger > 0 then
    begin
      if TabControl2.ActiveTab <> TI2 then
      begin
        ChangeTabAction1.Tab := TI2;
        ChangeTabAction1.ExecuteTarget(Self);
        ChangeTabAction1.Tab := TabControl2.ActiveTab;
      end;
      //
      TI2.SetFocus;
      if Panel3_2_Sim.Visible then
        // Evitar erro de "Erro pois o panel pai esta invisivel"
        EdQtdUso_3_2_Sim.SetFocus;
    end;
  end;
  //
  if QrOSPipIts.FieldByName('Funcoes').AsInteger = CO_PRG_LST_ATRIBUT then
  begin
    if QrOSPipIts.FieldByName('AtrTyp').AsInteger > 0 then
    begin
      if TabControl3.ActiveTab <> TI3 then
      begin
        ChangeTabAction1.Tab := TI3;
        ChangeTabAction1.ExecuteTarget(Self);
        ChangeTabAction1.Tab := TabControl3.ActiveTab;
        //
        TI3.SetFocus;
      end;
    end;
  end;
end;

procedure TFmOSWPipItsRsp.OcultaTodosPaineis();
(*
var
  I: Integer;
  CategoryPanel: TCategoryPanel;
*)
begin
(*
  for I := 0 to CategoryPanelGroup1.Panels.Count - 1 do
  begin
    CategoryPanel := TCategoryPanel(CategoryPanelGroup1.Panels[I]);
    //
    CategoryPanel.Collapsed := True;
    CategoryPanel.Visible := False;
  end;
  for I := 0 to CategoryPanelGroup2.Panels.Count - 1 do
  begin
    CategoryPanel := TCategoryPanel(CategoryPanelGroup2.Panels[I]);
    //
    CategoryPanel.Collapsed := True;
    CategoryPanel.Visible := False;
  end;
  for I := 0 to CategoryPanelGroup3.Panels.Count - 1 do
  begin
    CategoryPanel := TCategoryPanel(CategoryPanelGroup3.Panels[I]);
    //
    CategoryPanel.Collapsed := True;
    CategoryPanel.Visible := False;
  end;
  CategoryPanel := nil;
  //
*)
  Panel3_2_Sim.Visible := False;
end;

procedure TFmOSWPipItsRsp.Panel40Resize(Sender: TObject);
var
  Largura: Integer;
begin
  Largura := Trunc(Panel40.Width / 2);
  BtRespBinNao.Width := Largura;
end;

procedure TFmOSWPipItsRsp.PreencheLoteELaudo(GraGruX: Integer; EdLote,
  EdLaudo: TClearingEdit);
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrG1Any, DmModApp.FDAppDB, [
  'SELECT ggl.* ',
  'FROM gragruwlot ggl ',
  'LEFT JOIN gragrumow ggm ON ggm.GraGru1=ggl.Nivel1 ',
  'WHERE ggm.GraGruX=' + Geral.FF0(GraGruX),
  'ORDER BY ggl.Controle ',
  '']);
  EdLote.Text := QrG1Any.FieldByName('Nome').AsString;
  //
  DmkRDBMs.AbreSQLQuery0(Self, QrG1Any, DmModApp.FDAppDB, [
  'SELECT ggl.* ',
  'FROM gragruwlau ggl ',
  'LEFT JOIN gragrumow ggm ON ggm.GraGru1=ggl.Nivel1 ',
  'WHERE ggm.GraGruX=' + Geral.FF0(GraGruX),
  'ORDER BY ggl.Controle ',
  '']);
  EdLaudo.Text := QrG1Any.FieldByName('Nome').AsString;
end;

procedure TFmOSWPipItsRsp.PrxmPergunta();
{
  function DefineProximo(Atual: Integer): LargeInt;
  begin
    Result := 0;
    DmkRDBMs.AbreSQLQuery0(Self, QryAft, DmModApp.FDAppDB, [
    QrOSPipIts.SQL.Text]);
    //
    if QryAft.Locate('Conta', Atual, []) then
    begin
      QryAft.Next;
      Result := QryAft.FieldByName('Conta').AsLargeInt;
      if QryAft.Eof and (Result = Atual) then
        Result := -1;
    end else
    begin
      Geral.MB_Erro('Item não localizado: OSPipIts.Conta = ' + IntToStr(Atual));
    end;
  end;
var
  I: Integer;
  Atual, Proximo, Conta: LargeInt;
}
begin
{
  for I := FRangeLoops downto 0 do
  begin
    if QrNiveis[I] <> nil then
    begin
      if QrNiveis[I].Eof then
      begin
        Atual := QrNiveis[I].FieldByName('Conta').AsLargeInt;
        QrNiveis[I].Free;
        Proximo := DefineProximo(Atual);
        case Proximo of
          -1:
          begin
            if AtuLoops[I] >= QtdLoops[I] then
            begin


              Exit;
            end else
            begin
              AtuLoops[I] := AtuLoops[I] + 1;
              QrNiveis[I].First;
              Conta := QrNiveis[I].FieldByName('Conta').AsLargeInt;
              // P a r e i   a q u i
            end;
          end;
          0: ;// ???
          else
          begin
            QrOSPipIts.Locate('Conta', Proximo, []);
            Exit;
          end;
        end;
      end
      else
      begin
        //
        Exit;
      end;
    end;
  end;
  //
}
  QrOSPipIts.Next;
end;

procedure TFmOSWPipItsRsp.QrOSPipItsAfterScroll(DataSet: TDataSet);
const
  Respondido = 1;
var
  RespMetodo, Codigo, Controle: Integer;
  //
  DataHora: String;
  Latitude, Longitude: Double;
  DeviceID: String;
  EntiCad, UserWeb: Integer;
begin
  My_QrOSPipItsAfterScroll();
  if QrOSPipIts.Eof then
  begin
    //ShowMessage('Eof');
    if not FOnLoop then
    begin
      RespMetodo := FRespMetodo;
      Controle := FControle;
      Codigo   := FOSCab;
      DmModApp.ObtemInfoRastreamento(
        DataHora, Latitude, Longitude, DeviceID, EntiCad, UserWeb);
      //
      if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, 'OSWPipMon', False, [
      'Respondido', 'RespMetodo',
      //
      'DataHora', 'Latitude', 'Longitude',
      'DeviceID', 'EntiCad', 'UserWeb'
      ], ['Controle'], [
      Respondido, RespMetodo,
      //
      DataHora, Latitude, Longitude,
      DeviceID, EntiCad, UserWeb
      ], [Controle], True) then
      begin
        FmOSWPipMon.AtualizaPMVs();
        // Erro de Memory Leak no android!
        //FmOSWPipMon.Show;  > O Hide abaixo é suficiente.
        //
        if DmModApp.AtualizaRespondidoOSWCab(FOSCab) = 1 then
          FmOSCabLista.RefreshListaOSs();
        //
        DmModApp.LogRastreio(laiOSWPipMon, laeFinished, Codigo, Controle);
      end;
      Hide;
    end;
  end;
end;

procedure TFmOSWPipItsRsp.ReopenGraGruX();
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrGraGruX, DmModApp.FDAppDB, [
  'SELECT ggx.GraGru1, ggx.GraGruX, ggx.Nome ',
  'FROM gragrumow ggx ',
  'ORDER BY Nome, GraGruX ',
  '']);
end;

procedure TFmOSWPipItsRsp.ReopenOSPipIts();
{
var
  I: Integer;
  Txt: String;
}
begin
  ReopenPrgAtrIts(0);
  ReopenGraGruX();
  //
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT pac.Nome NO_PrgAtrCad, pac.AtrTyp, ',
  'pbs.Nome BinarSIM, pbn.Nome BinarNAO, ',
  'pcp.Nome NO_PERGUNTA, ggx.GraGru1, ',
  '',
  'CASE opi.Respondido WHEN 1 THEN',
  '  CASE Funcoes',
  '    WHEN 0 THEN '' ''',
  '    WHEN 1 THEN CASE opi.RespBin',
  '      WHEN 0 THEN pbn.Nome',
  '      WHEN 1 THEN pbs.Nome',
  '      ELSE '' '' END',
  '    WHEN 2 THEN opi.RespQtd',
  '    WHEN 3 THEN ppr.UsoQtd + '' unidades da isca ID '' + ppr.GraGruX',
  '    WHEN 4 THEN pai.Nome',
  '    WHEN 5 THEN SUBSTR(opi.RespTxtLvr, 1, 255)',
  '    ELSE ''??6??'' END',
  'ELSE '' '' END RESPOSTA,',
  'opi.*',
  'FROM ospipits opi',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta',
  'LEFT JOIN prgbincad pbs ON pbs.Codigo=opi.BinarCad1',
  'LEFT JOIN prgbincad pbn ON pbn.Codigo=opi.BinarCad0',
  'LEFT JOIN oswpipitsp ppr ON ppr.Conta=opi.Conta',
  'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts',
  'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgAtrCad',
  'LEFT JOIN gragrumow ggx ON ggx.GraGruX=opi.GraGruX',
  'WHERE opi.Ativo=1 ',
  'AND opi.Controle=' + Geral.FF0(FControle),
  'ORDER BY Controle, SuperOrd, SuperSub, SobreOrd, Ordem, SubOrdem',
  '']);
  //
(* Lista de campos!!!
  Txt := '';
  for I := 0 to QrOSPipIts.FieldCount -1 do
  begin
    Txt := Txt + Geral.FF0(I) + ' - ' + QrOSPipIts.Fields[I].FieldName + sLineBreak;
    //
  end;
  //
  Geral.MB_Info(Txt);
*)
end;

procedure TFmOSWPipItsRsp.ReopenPrgAtrIts(PrgAtrCad: Integer);
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrPrgAtrIts, DmModApp.FDAppDB, [
  'SELECT CodUsu, Controle, Nome ',
  'FROM prgatrits ',
  'WHERE Codigo=' + Geral.FF0(PrgAtrCad),
  'ORDER BY Nome' +
  '']);
end;

procedure TFmOSWPipItsRsp.ReopenQrOPI();
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOPI, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM ospipits ',
  'WHERE Conta=' + Geral.FI64(QrOSPipIts.FieldByName('Conta').AsLargeInt),
  'AND Respondido=1 ',
  '']);
end;

procedure TFmOSWPipItsRsp.ReopenQrOPIPR();
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrOPIPR, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM oswpipitsp ',
  'WHERE Conta=' + Geral.FI64(QrOPI.FieldByName('Conta').AsLargeInt),
  '']);
end;

procedure TFmOSWPipItsRsp.RePreencheAdicaoPrd;
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      ReopenQrOPIPR();
      //
      EdGraGruX.Text   := Geral.FF0(QrOPIPR.FieldByName('GraGruX').AsInteger);
      CBGraGruX.Text   := Geral.FF0(QrOPIPR.FieldByName('GraGruX').AsInteger);
      EdUsoQtd.Value   := QrOPIPR.FieldByName('UsoQtd').AsFloat;
(*
      EdUsoCusUni.Text := QrOPIPR.FieldByName('UsoCusUni').As;
      EdUsoCusTot.Text := QrOPIPR.FieldByName('UsoCusTot').As;
      EdValCliDd.Text  := QrOPIPR.FieldByName('ValCliDd').As;
*)
      EdNumLote.Text   := QrOPIPR.FieldByName('NumLote').AsString;
      EdNumLaudo.Text  := QrOPIPR.FieldByName('NumLaudo').AsString;
      //
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreencheBinario();
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    //
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      case QrOPI.FieldByName('RespBin').AsInteger of
        0: RGRespBinNao.IsChecked := True;
        1: RGRespBinSim.IsChecked := True;
      end;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreenchePrgAtrIts();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      // CUIDADO com o CodUsu!
      RespAtrIts := QrOPI.FieldByName('RespAtrIts').AsInteger;
      if QrPrgAtrIts.Locate('Controle', RespAtrIts, []) then
      begin
        RespAtrIts := QrPrgAtrIts.FieldByName('CodUsu').AsInteger;
        EdRespAtrIts.Text := Geral.FF0(RespAtrIts);
        CBRespAtrIts.Text := Geral.FF0(RespAtrIts);
      end;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreencheQuantitativo();
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      EdRespQtd.ValueInt := Trunc(QrOPI.FieldByName('RespQtd').AsFloat);
      //
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreencheRespAtrTxt();
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      MeRespAtrTxt.Text := QrOPI.FieldByName('RespAtrTxt').AsString;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreencheRespTxtLvr();
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      MeRespTxtLvr.Text := QrOPI.FieldByName('RespTxtLvr').AsString;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreencheRetiraPrd();
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      //RGAcaoPrd_Tira.ItemIndex := QrOPI.FieldByName('RespPrdBin').AsString;
      case QrOPI.FieldByName('RespPrdBin').AsInteger of
        0: RBAcaoPrd_TiraNao.IsChecked := True;
        1: RBAcaoPrd_TiraSim.IsChecked := True;
      end;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RePreencheSubstituicaoPrd();
var
  //RespAtrIts,
  RespPrdBin: Integer;
begin
  if not CkPreencher.IsChecked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPI.FieldByName('Respondido').AsInteger = 1 then
    begin
      RespPrdBin := QrOPI.FieldByName('RespPrdBin').AsInteger;
      //RGAcaoPrd_Troc.ItemIndex := QrOPI.FieldByName('RespPrdBin').As;
      case RespPrdBin of
        0: RBAcaoPrd_TrocNao.IsChecked := True;
        1: RBAcaoPrd_TrocSim.IsChecked := True;
      end;
      //
      case RespPrdBin of
        0: Panel3_2_Sim.Visible := False;
        1:
        begin
          ReopenQrOPIPR();
          //
          EdQtdUso_3_2_Sim.Value  := QrOPIPR.FieldByName('UsoQtd').AsFloat;
          EdNumLote_3_2_Sim.Text  := QrOPIPR.FieldByName('NumLote').AsString;
          EdNumLaudo_3_2_Sim.Text := QrOPIPR.FieldByName('NumLaudo').AsString;
          //
          Panel3_2_Sim.Visible := True;
          // Erro pois o panel pai esta invisivel
          //EdQtdUso_3_2_Sim.SetFocus;
        end;
      end;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSWPipItsRsp.RespPipIts(RespBin: Integer; RespQtd: Double;
  RespAtrCad, RespAtrIts: Integer; RespAtrTxt, RespTxtLvr: String;
  RespPrdBin, DesativSrc: Integer);
const
  Respondido = CO_Respondido_SIM;
var
  Codigo, Controle: Integer;
  Conta: Int64;
  //
  DataHora: String;
  Latitude, Longitude: Double;
  DeviceID: String;
  EntiCad, UserWeb: Integer;
begin
  Codigo   := QrOSPipIts.FieldByName('Codigo').AsInteger;
  Controle := QrOSPipIts.FieldByName('Controle').AsInteger;
  Conta    := QrOSPipIts.FieldByName('Conta').AsInteger;
  //
  DmModApp.ObtemInfoRastreamento(
    DataHora, Latitude, Longitude, DeviceID, EntiCad, UserWeb);
  //
  if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stUpd, 'ospipits', False, [
  'Respondido', 'RespBin', 'RespQtd',
  'RespAtrCad', 'RespAtrIts', 'RespAtrTxt',
  'RespTxtLvr', 'RespPrdBin', 'DesativSrc',
  //
  'DataHora', 'Latitude', 'Longitude',
  'DeviceID', 'EntiCad', 'UserWeb'
  ], ['Conta'], [
  Respondido, RespBin, RespQtd,
  RespAtrCad, RespAtrIts, RespAtrTxt,
  RespTxtLvr, RespPrdBin, DesativSrc,
  //
  DataHora, Latitude, Longitude,
  DeviceID, EntiCad, UserWeb
  ], [Conta], True) then
  begin
    // Exclui itens de movimentacao estoque caso foram criados em resposta anterior
    DmkRDBMs.SQLDelSimple(Self, dbsrcLocalServer, 'oswpipitsp', [
    'Conta'], [Conta], '');
    //
    DmModApp.LogRastreio(laiOSPipIts, laeFinished, Codigo, Controle, Conta);
  end;
end;

procedure TFmOSWPipItsRsp.RGAcaoPrd_TiraClick(Sender: TObject);
var
  ItemIndex: Integer;
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, DesativSrc: Integer;
  RespQtd: Double;
begin
  ItemIndex := MyObjects.RGItemIndex(RGAcaoPrd_Tira);
  if ItemIndex = -1 then
    Exit;
  //CO_PRG_LST_BXAPROD = 3;
  //CO_AcaoPrd_Tira = 3;
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := ItemIndex;
  //
  if RespPrdBin = 1 then
    DesativSrc := DesativaPipItsPr()
  else
    DesativSrc := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  //
  PrxmPergunta();
end;

procedure TFmOSWPipItsRsp.RGAcaoPrd_TrocClick(Sender: TObject);
var
  ItemIndex: Integer;
begin
  ItemIndex := MyObjects.RGItemIndex(RGAcaoPrd_Troc);
  Executa_03_02_AcaoPrg(ItemIndex);
end;

procedure TFmOSWPipItsRsp.RGRespBinNaoChange(Sender: TObject);
begin
  Executa_01_Binario(TRadioButton(Sender).IsChecked, 0);
end;

procedure TFmOSWPipItsRsp.RGRespBinSimChange(Sender: TObject);
begin
  Executa_01_Binario(TRadioButton(Sender).IsChecked, 1);
end;

procedure TFmOSWPipItsRsp.SBNumLaudo_3_2_SimClick(Sender: TObject);
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrGraGruWLau, DmModApp.FDAppDB, [
  'SELECT ggl.Controle, ggl.Nome ',
  'FROM gragruwlau ggl ',
  'LEFT JOIN gragrumow ggm ON ggm.GraGru1=ggl.Nivel1 ',
  'WHERE ggm.GraGruX=' + Geral.FF0(QrOSPipIts.FieldByName('GraGruX').AsInteger),
  'ORDER BY ggl.Nome ',
  '']);
  FormsPF.ObtemCodiNomeCadastro(
    QrGraGruWLau, 'Controle', 'Nome', nil, EdNumLaudo_3_2_Sim);
end;

procedure TFmOSWPipItsRsp.SBNumLote_3_2_SimClick(Sender: TObject);
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrGraGruWLot, DmModApp.FDAppDB, [
  'SELECT ggl.Controle, ggl.Nome ',
  'FROM gragruwlot ggl ',
  'LEFT JOIN gragrumow ggm ON ggm.GraGru1=ggl.Nivel1 ',
  'WHERE ggm.GraGruX=' + Geral.FF0(QrOSPipIts.FieldByName('GraGruX').AsInteger),
  'ORDER BY ggl.Nome ',
  '']);
  FormsPF.ObtemCodiNomeCadastro(
    QrGraGruWLot, 'Controle', 'Nome', nil, EdNumLote_3_2_Sim);
end;

{
function TFmOSWPipItsRsp.ShowDmkInputTxt(CustomEdit: TCustomEdit): String;
var
  Janela: TFmDmkInputTxt;
  //EdIdx: TdmkCNEdit;
begin
  //KeyValue := '';
  //ListValue := '';
  Janela := TFmDmkInputTxt.Create(nil);
  Janela.MeTxt.Lines.Text := CustomEdit.Text;
  Janela.FCustomEdit := CustomEdit;
  Janela.ShowModal(
    procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
        Janela.FCustomEdit.Text :=Janela.MeTxt.Text;
      end;
      / Erro Memory Leak!
      Janela.DisposeOf;
    end);
end;
}

procedure TFmOSWPipItsRsp.SBNumLoteClick(Sender: TObject);
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrGraGruWLot, DmModApp.FDAppDB, [
  'SELECT ggl.Controle, ggl.Nome ',
  'FROM gragruwlot ggl ',
  'LEFT JOIN gragrumow ggm ON ggm.GraGru1=ggl.Nivel1 ',
  'WHERE ggm.GraGruX=' + Geral.FF0(EdGraGruX.ValueInt),
  'ORDER BY ggl.Nome ',
  '']);
  FormsPF.ObtemCodiNomeCadastro(
    QrGraGruWLot, 'Controle', 'Nome', nil, EdNumLote);
end;

procedure TFmOSWPipItsRsp.SBNumLaudoClick(Sender: TObject);
begin
  DmkRDBMs.AbreSQLQuery0(Self, QrGraGruWLau, DmModApp.FDAppDB, [
  'SELECT ggl.Controle, ggl.Nome ',
  'FROM gragruwlau ggl ',
  'LEFT JOIN gragrumow ggm ON ggm.GraGru1=ggl.Nivel1 ',
  'WHERE ggm.GraGruX=' + Geral.FF0(EdGraGruX.ValueInt),
  'ORDER BY ggl.Nome ',
  '']);
  FormsPF.ObtemCodiNomeCadastro(
    QrGraGruWLau, 'Controle', 'Nome', nil, EdNumLaudo);
end;

procedure TFmOSWPipItsRsp.SubstituiPipItsPr(Quantidade: Double; NumLote,
  NumLaudo: String);
const
  Acao = CO_AcaoPrd_Troc (*2*);
{
var
  Tabela: Integer;
  TabTxt: String;
}
var
  GraGruX, ValCliDd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
begin
  // Desativa atual...
  DesativaPipItsPr();
{
  Tabela := QrOSPipIts.FieldByName('Tabela').AsInteger;
  TabTxt := '';
  case Tabela of
    CO_OSPipIts_OSMONREC   : TabTxt := 'osmonrec';
    CO_OSPipIts_OSPIPITSPR : TabTxt := 'oswpipitsp';
    else
    begin
      Geral.MB_ERRO(
      'Tabela não definida em substituição de isca na procedure "SubstituiPipItsPr"');
    end;
  end;
  // ... para incluir novo!
  //
  DmkRDBMs.AbreSQLQuery0(Self, QrOri, DmModApp.FDAppDB, [
  'SELECT ValCliDd, UsoQtd, UsoCusUni, UsoCusTot ',
  'FROM ' + TabTxt,
  'WHERE Conta=' + Geral.FI64(QrOSPipIts.FieldByName('TabIdx').AsLargeInt),
  '']);
  GraGruX        := QrOSPipIts.FieldByName('GraGrux').AsInteger;
  ValCliDd       := QrOri.FieldByName('ValCliDd').AsInteger;
}
  GraGruX        := FAntUsoGGX;
  ValCliDd       := FAntUsoVCD;
  UsoQtd         := Quantidade; //QrOri.FieldByName('UsoQtd').AsFloat;
  UsoCusUni      := 0;
  UsoCusTot      := 0;
{  Ver como fazer!!!
  if FLstCusPrd = 0 then
    Geral.MB_Aviso('Tabela de custos não definida!')
  else
  begin
    if QrGraGruX.State = dsInactive then
      ReopenGraGruX();
    if QrGraGruX.Locate('GraGruX', GraGruX, []) then
    begin
      if QrGraGruX.FieldByName('CtrlGGV').AsInteger = 0 then
        Geral.MB_Aviso('Preço não definido na tabela de custos!')
      else
        UsoCusUni := QrGraGruX.FieldByName('CustoPreco').AsFloat;
    end else
    begin
      Geral.MB_Aviso('Reduzido não localizado na tabela de precos')
    end;
  end;
  if UsoCusUni = 0 then
  begin
    UsoCusUni := QrOri.FieldByName('UsoCusUni').AsFloat;
    DmModProd.ObtemPreco(UsoCusUni);
  end;
  UsoCusTot := UsoCusUni * UsoQtd;
}
  //
  IncluiPipItsPr(Acao, GraGruX, ValCliDd, UsoQtd, UsoCusUni, UsoCusTot,
    NumLote, NumLaudo);
  //
end;

procedure TFmOSWPipItsRsp.TrocaIsca(RespPrdBin, Quantidade: Integer; NumLote,
  NumLaudo: String);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, DesativSrc: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_BXAPROD = 3;
  //CO_AcaoPrd_Troc = 2;
  //
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  //RespPrdBin     := RGAcaoPrd_Troc.ItemIndex;
  DesativSrc     := 0;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, DesativSrc);
  //
  if RespPrdBin = 1 then
    SubstituiPipItsPr(Quantidade, NumLote, NumLaudo);
  //
  PrxmPergunta();
end;

end.
