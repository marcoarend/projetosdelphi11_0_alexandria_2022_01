unit TesteSQL;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Memo,
  UnGrl_Vars;

type
  TFmTesteSQL = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    Splitter1: TSplitter;
    Memo2: TMemo;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmTesteSQL: TFmTesteSQL;

implementation

uses UnMyListas, UnWebReq, UnGeral;

{$R *.fmx}

procedure TFmTesteSQL.Button1Click(Sender: TObject);
const
  Metodo = 'REST_Dmk_ObtemResultadoSQL';
var
  AplicativoSolicitante, IDUsuarioSolicitante,
  Token, Titulo, SQL: String;
begin
  AplicativoSolicitante := 'AplicativoSolicitante=' + IntToStr(CO_DMKID_APP);
  IDUsuarioSolicitante := 'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USER_ID);
  Token := 'Token=' + VAR_WEB_TOKEN_DMK;
  Titulo := 'Titulo=TesteSQL';
  SQL := Geral.Substitui('SQL=' + Memo2.Text, #10, ' ');
  SQL := Geral.Substitui(SQL, #13, ' ');
  //
  Memo1.Text := WebReq.SQLArrPOST_TxtStr(Self, Metodo, [
    AplicativoSolicitante, IDUsuarioSolicitante, Token, Titulo, SQL
  ]);
end;

procedure TFmTesteSQL.Button2Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
end;

end.
