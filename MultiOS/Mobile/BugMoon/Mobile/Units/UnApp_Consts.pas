unit UnApp_Consts;

interface

uses UnDmkEnums;

const
  CO_DB_APP_MAIN_DB = dbmsSQLite;
  CO_PMV_XML_OnClick       = '[OnClick]';
  CO_PMV_XML_PosicaoPMVIni = '[PosicaoPMVIni]';
  CO_PMV_XML_ZoomPMV       = '[ZoomPMV]';
  CO_PMV_XML_IdPMV         = '[IdPMV]';
  CO_PMV_XML_CorPMV        = '[CorPMV]';
  CO_PMV_XML__             = '[_]';
  CO_PMV_XML_PosicaoPMVFim = '[PosicaoPMVFim]';

implementation

end.
