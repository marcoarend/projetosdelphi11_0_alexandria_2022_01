unit SincroUp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Memo, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  UnGeral, UnProjGroup_Consts, UnMyObjects, UnDmkEnums, UnGrl_Consts;

type
  TFmSincroUp = class(TForm)
    Panel4: TPanel;
    Label3: TLabel;
    Panel1: TPanel;
    Memo1: TMemo;
    Panel2: TPanel;
    Button1: TButton;
    Panel3: TPanel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB0: TProgressBar;
    QrOSWCab: TFDQuery;
    Panel5: TPanel;
    LaAviso: TLabel;
    QrOSPipIts: TFDQuery;
    QrOSWPipItsP: TFDQuery;
    QrOSPips: TFDQuery;
    QrActionLog: TFDQuery;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure Info(Texto: String);
    function  Upload_PorOS(Codigo: Integer): Boolean;
    function  Upload_PorControle(Codigo: Integer): Boolean;
    function  Upload_PorConta(Codigo: Integer): Boolean;
    function  UpdateOSPipItsAtual(var Texto: String; Executa: Boolean): Boolean;
    function  SQL_OSPipItsAtual(): String;
    function  SQL_OSWPipItsPAtual(): String;
    function  SQL_ActnLog(): String;
    function  InsereOSWPipItsPAtual(): Boolean;
  public
    { Public declarations }
  end;

var
  FmSincroUp: TFmSincroUp;

implementation

uses UnDmkRDBMs, ModApp, UnDmkDAC;

{$R *.fmx}

const
  sAtzRespOS = 'OS %s: Atualizando na nuvem resposta %s de %s';

procedure TFmSincroUp.Button1Click(Sender: TObject);
var
  SQLOSCab, OsTxt: String;
  TabOSCab: TTabIDInt;
  TabOSCabEvolucao: TFldIDInt;
  OSCab, Evolucao, ItensUp: Integer;
begin
  ItensUp := 0;
  Memo1.Lines.Clear;
  //
  MyObjects.Informa(LaAviso, True, 'Verificando quantidade de OS aptos ao Upload');
  //
  DmkRDBMs.AbreSQLQuery0(Self, QrOSWCab, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM oswcab ',
  'WHERE Respondido=1 ',
  '']);
  UpLoad_porOS(QrOSWCab.FieldByName('Codigo').AsInteger);
  EXIT;



  //

  if QrOSWCab.RecordCount > 0 then
  begin
    while not QrOSWCab.Eof do
    begin
      OSCab := QrOSWCab.FieldByName('Codigo').AsInteger;
      OsTxt := Geral.FF0(OSCab);
      //
      SQLOSCab := Geral.ATS([
      'SELECT osc.Evolucao ',
      'FROM oswcab osc ',
      'WHERE osc.Codigo=' + OsTxt,
      '']);
      //
      MyObjects.Informa(LaAviso, True, 'Verificando na nuvem a evolu��o da OS ' +
        OsTxt);
      if DmkDAC.SQLArrRec_IDInt(Self, SQLOSCab, TabOSCab) then
      try
        MyObjects.Informa(LaAviso, True, 'Link nuvem: Verifica��o evolu��o da OS ' +
          OsTxt + ' conclu�da');
        if TabOSCab.Count > 0 then
        begin
          TabOSCabEvolucao := TabOSCab.Items[0];
          Evolucao := TabOSCabEvolucao.Value;
          if Evolucao < CO_OSWCAB_EVOLUCAO_03000_COD_DOWN_CLOUD_MOBLE_FIM then
          begin
            Info('OS ' + OsTxt + ' evolu��o inv�lida na nuvem!');
          end else
          begin
            Evolucao := CO_OSWCAB_EVOLUCAO_04000_COD_UPLD_MOBLE_CLOUD_INI;
            MyObjects.Informa(LaAviso, True, 'Atualizando evolu��o da OS ' +
              OsTxt + ' na nuvem: ' + Geral.FF0(Evolucao));
            if DmModApp.EvolucaoAtualizada(OSCab, Evolucao) then
            begin
              // Parei aqui! Subir dados da OSCab!
              if Upload_PorConta(OSCab) then
              begin
                Evolucao := CO_OSWCAB_EVOLUCAO_05000_COD_UPLD_MOBLE_CLOUD_FIM;
                MyObjects.Informa(LaAviso, True, 'Atualizando evolu��o da OS ' +
                  OsTxt + ' na nuvem: ' + Geral.FF0(Evolucao));
                if DmModApp.EvolucaoAtualizada(OSCab, Evolucao) then
                begin
                  ItensUp := ItensUp + 1;
                end;
              end;
            end;
          end;
        end else
          Info('OS n�o encontrada na nuvem: ' + OsTxt);
      finally
        DmkDAC.FreeRec(TabOSCab);
      end;
      //
      QrOSWCab.Next;
    end;
    MyObjects.Informa(LaAviso, False, 'Upload finalizado!  OSs subidas: ' +
      Geral.FF0(ItensUp));
  end else
    Info('N�o h� OS apta a subir!');
end;

procedure TFmSincroUp.Info(Texto: String);
begin
  Memo1.Lines.Add(Texto);
end;

function TFmSincroUp.InsereOSWPipItsPAtual(): Boolean;
var
  NumLote, NumLaudo: String;
  Codigo, Controle, Acao, GraGruX, ValCliDd, EhDiluente, Desativado: Integer;
  Conta, IDIts: Int64;
  UsoQtd, UsoCusUni, UsoCusTot, UsoPrc, UsoVal, UsoDec, UsoTot: Double;
  //
  SQL: String;
begin
  Result := False;
  //
  Codigo         := QrOSWPipItsP.FieldByName('Codigo').AsInteger;
  Controle       := QrOSWPipItsP.FieldByName('Controle').AsInteger;
  Conta          := QrOSWPipItsP.FieldByName('Conta').AsLargeInt;
  IDIts          := QrOSWPipItsP.FieldByName('IDIts').AsLargeInt;
  Acao           := QrOSWPipItsP.FieldByName('Acao').AsInteger;
  GraGruX        := QrOSWPipItsP.FieldByName('GraGruX').AsInteger;
  UsoQtd         := QrOSWPipItsP.FieldByName('UsoQtd').AsFloat;
  UsoCusUni      := QrOSWPipItsP.FieldByName('UsoCusUni').AsFloat;
  UsoCusTot      := QrOSWPipItsP.FieldByName('UsoCusTot').AsFloat;
  UsoPrc         := QrOSWPipItsP.FieldByName('UsoPrc').AsFloat;
  UsoVal         := QrOSWPipItsP.FieldByName('UsoVal').AsFloat;
  UsoDec         := QrOSWPipItsP.FieldByName('UsoDec').AsFloat;
  UsoTot         := QrOSWPipItsP.FieldByName('UsoTot').AsFloat;
  ValCliDd       := QrOSWPipItsP.FieldByName('ValCliDd').AsInteger;
  EhDiluente     := QrOSWPipItsP.FieldByName('EhDiluente').AsInteger;
  Desativado     := QrOSWPipItsP.FieldByName('Desativado').AsInteger;
  NumLote        := QrOSWPipItsP.FieldByName('NumLote').AsString;
  NumLaudo       := QrOSWPipItsP.FieldByName('NumLaudo').AsString;
  //
  if DmkRDBMs.GeraSQLInsUpd(SQL, Self, dbmsMySQL, stUpd, 'oswpipitsp', False, [
  'Codigo', 'Controle', 'Conta',
  'Acao', 'GraGruX', 'UsoQtd',
  'UsoCusUni', 'UsoCusTot', 'UsoPrc',
  'UsoVal', 'UsoDec', 'UsoTot',
  'ValCliDd', 'EhDiluente', 'Desativado',
  'NumLote', 'NumLaudo'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  Acao, GraGruX, UsoQtd,
  UsoCusUni, UsoCusTot, UsoPrc,
  UsoVal, UsoDec, UsoTot,
  ValCliDd, EhDiluente, Desativado,
  NumLote, NumLaudo], [
  IDIts], True) then
  begin
    Result := DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL) >= 0;
  end;
end;

function TFmSincroUp.SQL_ActnLog(): String;
var
  SQL,
  //
  DataHora, DeviceID: String;
  ActionID, AcaoExe, EntiCad, UserWeb: Integer;
  Nivel1, Nivel2, Nivel3, Nivel4, Nivel5, Latitude, Longitude: Double;
begin
  SQL := '';
  //
  ActionID       := QrOSWPipItsP.FieldByName('ActionID').AsInteger;
  Nivel1         := QrOSWPipItsP.FieldByName('Nivel1').AsLargeint;
  Nivel2         := QrOSWPipItsP.FieldByName('Nivel2').AsLargeint;
  Nivel3         := QrOSWPipItsP.FieldByName('Nivel3').AsLargeint;
  Nivel4         := QrOSWPipItsP.FieldByName('Nivel4').AsLargeint;
  Nivel5         := QrOSWPipItsP.FieldByName('Nivel5').AsLargeint;
  AcaoExe        := QrOSWPipItsP.FieldByName('AcaoExe').AsInteger;
  DataHora       := Geral.FDT(QrOSWPipItsP.FieldByName('DataHora').AsDateTime, 109);
  Latitude       := QrOSWPipItsP.FieldByName('Latitude').AsFloat;
  Longitude      := QrOSWPipItsP.FieldByName('Longitude').AsFloat;
  DeviceID       := QrOSWPipItsP.FieldByName('DeviceID').AsString;
  EntiCad        := QrOSWPipItsP.FieldByName('EntiCad').AsInteger;
  UserWeb        := QrOSWPipItsP.FieldByName('UserWeb').AsInteger;
  //
  if DmkRDBMs.GeraSQLInsUpd(SQL, Self, dbmsMySQL, stUpd, 'actnlog', False, [
  'ActionID', 'Nivel1', 'Nivel2',
  'Nivel3', 'Nivel4', 'Nivel5',
  'AcaoExe', 'DataHora', 'Latitude',
  'Longitude', 'DeviceID', 'EntiCad',
  'UserWeb'], [
  ], [
  ActionID, Nivel1, Nivel2,
  Nivel3, Nivel4, Nivel5,
  AcaoExe, DataHora, Latitude,
  Longitude, DeviceID, EntiCad,
  UserWeb], [
  ], True) then
  begin
    Result := SQL;
  end;
end;

function TFmSincroUp.SQL_OSPipItsAtual(): String;
var
  RespQtd: Double;
  //DesativSrc
  RespPrdBin, RespBin, Respondido, RespAtrCad, RespAtrIts: Integer;
  RespAtrTxt, RespTxtLvr: String;
  Conta: Int64;
  //
  //OsTxt, Txt, RespCount, RespItem,
  SQL: String;
  Continua: Boolean;
begin
  Result := '';
  //
  Conta      := QrOSPipIts.FieldByName('Conta').AsInteger;
  //
  RespBin    := QrOSPipIts.FieldByName('RespBin').AsInteger;
  RespQtd    := QrOSPipIts.FieldByName('RespQtd').AsFloat;
  Respondido := QrOSPipIts.FieldByName('Respondido').AsInteger;
  RespAtrCad := QrOSPipIts.FieldByName('RespAtrCad').AsInteger;
  RespAtrIts := QrOSPipIts.FieldByName('RespAtrIts').AsInteger;
  RespAtrTxt := QrOSPipIts.FieldByName('RespAtrTxt').AsString;
  RespTxtLvr := QrOSPipIts.FieldByName('RespTxtLvr').AsString;
  RespPrdBin := QrOSPipIts.FieldByName('RespPrdBin').AsInteger;
  //DesativSrc := QrOSPipIts.FieldByName('DesativSrc').AsInteger;
  // Exclui OSWPipItsPr do OSPipIts atual.
  SQL := 'DELETE FROM oswpipitsp WHERE Conta=' + Geral.FI64(Conta);
  Continua := DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL) >= 0;
  //
  if Continua then
  begin
    Continua :=
    DmkRDBMs.GeraSQLInsUpd(SQL, Self, dbmsMySQL, stUpd, 'ospipits', False, [
    (*'Codigo', 'Controle', 'PrgLstCab',
    'PrgLstIts', 'SuperOrd', 'SuperSub',
    'SobreOrd', 'Ordem', 'SubOrdem',
    'Tabela', 'TabIdx', 'GraGruX',
    'Funcoes', 'Dependente', 'PrgAtrCad',
    'Filiacao', 'Relacao', 'Nivel',
    'NivSeq', 'Pergunta', 'BinarCad0',
    'BinarCad1', 'AcaoPrd', 'LupForma',
    'LupQtdVzs',*) 'Respondido', 'RespBin',
    'RespQtd', 'RespAtrCad', 'RespAtrIts',
    'RespAtrTxt', 'RespTxtLvr', 'RespPrdBin'(*,
    'LupSeqRsp', 'LupInfVzs', 'RespMetodo',
    'DesativSrc'*)], [
    'Conta'], [
    (*Codigo, Controle, PrgLstCab,
    PrgLstIts, SuperOrd, SuperSub,
    SobreOrd, Ordem, SubOrdem,
    Tabela, TabIdx, GraGruX,
    Funcoes, Dependente, PrgAtrCad,
    Filiacao, Relacao, Nivel,
    NivSeq, Pergunta, BinarCad0,
    BinarCad1, AcaoPrd, LupForma,
    LupQtdVzs,*) Respondido, RespBin,
    RespQtd, RespAtrCad, RespAtrIts,
    RespAtrTxt, RespTxtLvr, RespPrdBin(*,
    LupSeqRsp, LupInfVzs, RespMetodo,
    DesativSrc*)], [
    Conta], True);
    //
  end;
  //
  Result := SQL; // + '; ' + sLineBreak;
end;

function TFmSincroUp.SQL_OSWPipItsPAtual(): String;
var
  NumLote, NumLaudo: String;
  Codigo, Controle, Acao, GraGruX, ValCliDd, EhDiluente, Desativado: Integer;
  Conta, IDIts: Int64;
  UsoQtd, UsoCusUni, UsoCusTot, UsoPrc, UsoVal, UsoDec, UsoTot: Double;
  //
  SQL: String;
begin
  Result := '';
  //
  Codigo         := QrOSWPipItsP.FieldByName('Codigo').AsInteger;
  Controle       := QrOSWPipItsP.FieldByName('Controle').AsInteger;
  Conta          := QrOSWPipItsP.FieldByName('Conta').AsLargeInt;
  IDIts          := QrOSWPipItsP.FieldByName('IDIts').AsLargeInt;
  Acao           := QrOSWPipItsP.FieldByName('Acao').AsInteger;
  GraGruX        := QrOSWPipItsP.FieldByName('GraGruX').AsInteger;
  UsoQtd         := QrOSWPipItsP.FieldByName('UsoQtd').AsFloat;
  UsoCusUni      := QrOSWPipItsP.FieldByName('UsoCusUni').AsFloat;
  UsoCusTot      := QrOSWPipItsP.FieldByName('UsoCusTot').AsFloat;
  UsoPrc         := QrOSWPipItsP.FieldByName('UsoPrc').AsFloat;
  UsoVal         := QrOSWPipItsP.FieldByName('UsoVal').AsFloat;
  UsoDec         := QrOSWPipItsP.FieldByName('UsoDec').AsFloat;
  UsoTot         := QrOSWPipItsP.FieldByName('UsoTot').AsFloat;
  ValCliDd       := QrOSWPipItsP.FieldByName('ValCliDd').AsInteger;
  EhDiluente     := QrOSWPipItsP.FieldByName('EhDiluente').AsInteger;
  Desativado     := QrOSWPipItsP.FieldByName('Desativado').AsInteger;
  NumLote        := QrOSWPipItsP.FieldByName('NumLote').AsString;
  NumLaudo       := QrOSWPipItsP.FieldByName('NumLaudo').AsString;
  //
  if DmkRDBMs.GeraSQLInsUpd(SQL, Self, dbmsMySQL, stUpd, 'oswpipitsp', False, [
  'Codigo', 'Controle', 'Conta',
  'Acao', 'GraGruX', 'UsoQtd',
  'UsoCusUni', 'UsoCusTot', 'UsoPrc',
  'UsoVal', 'UsoDec', 'UsoTot',
  'ValCliDd', 'EhDiluente', 'Desativado',
  'NumLote', 'NumLaudo'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  Acao, GraGruX, UsoQtd,
  UsoCusUni, UsoCusTot, UsoPrc,
  UsoVal, UsoDec, UsoTot,
  ValCliDd, EhDiluente, Desativado,
  NumLote, NumLaudo], [
  IDIts], True) then
  begin
    Result := SQL;
  end;
end;

function TFmSincroUp.UpdateOSPipItsAtual(var Texto: String; Executa: Boolean): Boolean;
var
  SQL: String;
begin
  SQL := SQL_OSPipItsAtual();
  // Atualiza OSPipIts atual
  if SQL <> '' then
  begin
    if Executa then
      Result := DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL) >= 0;
    Texto := Texto + SQL;
  end;
end;

function TFmSincroUp.Upload_PorControle(Codigo: Integer): Boolean;
var
  Conta: Int64;
  SQL, Txt, RespCount, RespItem, OsTxt: String;
  Tempo: TDateTime;
begin
  Tempo := Now();
  //
  Result := False;
  //
  OsTxt := Geral.FF0(Codigo);
  // RecordCount Est'a retornando 50!!
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT COUNT(DISTINCT(Controle)) ITENS ',
  'FROM ospipits ',
  'WHERE Codigo=' + OsTxt,
  '']);
  RespCount := Geral.FF0(QrOSPipIts.FieldByName('ITENS').AsInteger);
  //
  //QrOSPipIts.FetchOptions.RowsetSize := High(Integer) div 2;
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPips, DmModApp.FDAppDB, [
  'SELECT DISTINCT(Controle) ',
  'FROM ospipits ',
  'WHERE Codigo=' + OsTxt,
  'ORDER BY Controle ',
  '']);
  QrOSPips.First;
  while not QrOSPips.Eof do
  begin
    DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
    'SELECT * ',
    'FROM ospipits ',
    'WHERE Controle=' + Geral.FF0(QrOSPips.FieldByName('Controle').AsInteger),
    '']);
    QrOSPipIts.First;
    while not QrOSPipIts.Eof do
    begin
      Conta := QrOSPipIts.FieldByName('Conta').AsInteger;
      //
      RespItem :=  Geral.FF0(QrOSPipIts.RecNo);
      Txt := Format(sAtzRespOS, [OsTxt, RespItem, RespCount]);
      MyObjects.Informa(LaAviso, True, Txt);
      if UpdateOSPipItsAtual(SQL, False) then
      begin
        DmkRDBMs.AbreSQLQuery0(Self, QrOSWPipItsP, DmModApp.FDAppDB, [
        'SELECT * ',
        'FROM oswpipitsp ',
        'WHERE Conta=' + Geral.FI64(Conta),
        '']);
        QrOSWPipItsP.First;
        while not QrOSWPipItsP.Eof do
        begin
          InsereOSWPipItsPAtual();
          //
          QrOSWPipItsP.Next;
        end;
      end;
      //
      QrOSPipIts.Next;
    end;
    //Result :=
    //DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL);// >= 0;
    Geral.MB_Info(SQL);
    DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL);// >= 0;
    //
    QrOSPips.Next;
  end;
  //
  Tempo := Now() - Tempo;
  Geral.MB_Info('Tempo: ' + Geral.FDT(Tempo, 108));
  //
  Result := True;
end;

function TFmSincroUp.Upload_PorOS(Codigo: Integer): Boolean;
var
  SQLStatement, SQLBatch: String;
  QtdStatemants: Integer;
  Conta: Int64;
  SQL, Txt, RespCount, RespItem, OsTxt: String;
  Tempo: TDateTime;
begin
  Tempo := Now();
  Result := False;
  SQLBatch := '';
  QtdStatemants := 0;
  OsTxt := Geral.FF0(Codigo);
{
  // RecordCount Est'a retornando 50!!
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM ospipits ',
  'WHERE Codigo=' + OsTxt,
  '']);
  RespCount := Geral.FF0(QrOSPipIts.FieldByName('ITENS').AsInteger);
  //
  //QrOSPipIts.FetchOptions.RowsetSize := High(Integer) div 2;
}
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM ospipits ',
  'WHERE Codigo=' + OsTxt,
  '']);
  //RespCount := Geral.FF0(QrOSPipIts.RecordCount);
  QrOSPipIts.First;
  while not QrOSPipIts.Eof do
  begin
    SQLStatement := SQL_OSPipItsAtual();
    QtdStatemants := QtdStatemants + 1;
    SQLBatch := SQLBatch + SQLStatement + CO_SQLBATCH_DELIM + sLineBreak;
{
    Conta := QrOSPipIts.FieldByName('Conta').AsInteger;
    //
    RespItem :=  Geral.FF0(QrOSPipIts.RecNo);
    Txt := Format(sAtzRespOS, [OsTxt, RespItem, RespCount]);
    MyObjects.Informa(LaAviso, True, Txt);
    if UpdateOSPipItsAtual(SQL, True) then
    begin
    end;
    //
}
    QrOSPipIts.Next;
  end;
  //Result := DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL) >= 0;
  DmkRDBMs.AbreSQLQuery0(Self, QrOSWPipItsP, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM oswpipitsp ',
  'WHERE Conta=' + Geral.FI64(Conta),
  '']);
  QrOSWPipItsP.First;
  while not QrOSWPipItsP.Eof do
  begin
    SQLStatement := SQL_OSWPipItsPAtual();
    QtdStatemants := QtdStatemants + 1;
    SQLBatch := SQLBatch + SQLStatement + CO_SQLBATCH_DELIM + sLineBreak;
    //InsereOSWPipItsPAtual();
    //
    QrOSWPipItsP.Next;
  end;
  //
  DmkRDBMs.AbreSQLQuery0(Self, QrActnLog, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM actnlog ',
  'WHERE ???' +
  'AND ???
  '']);
  QrActnLog.First;
  while not QrActnLog.Eof do
  begin
    SQL_ActnLog();
    //
    QrActnLog.Next;
  end;

  Memo1.Text := SQLBatch;
  //
  Tempo := Now() - Tempo;
  Geral.MB_Info('Tempo: ' + Geral.FDT(Tempo, 108));
  //
  Result := True;
end;

function TFmSincroUp.Upload_PorConta(Codigo: Integer): Boolean;
var
  Conta: Int64;
  SQL, Txt, RespCount, RespItem, OsTxt: String;
  Tempo: TDateTime;
begin
  Tempo := Now();
  //
  Result := False;
  //
  OsTxt := Geral.FF0(Codigo);
  // RecordCount Est'a retornando 50!!
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM ospipits ',
  'WHERE Codigo=' + OsTxt,
  '']);
  RespCount := Geral.FF0(QrOSPipIts.FieldByName('ITENS').AsInteger);
  //
  //QrOSPipIts.FetchOptions.RowsetSize := High(Integer) div 2;
  DmkRDBMs.AbreSQLQuery0(Self, QrOSPipIts, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM ospipits ',
  'WHERE Codigo=' + OsTxt,
  '']);
  //RespCount := Geral.FF0(QrOSPipIts.RecordCount);
  QrOSPipIts.First;
  while not QrOSPipIts.Eof do
  begin
    Conta := QrOSPipIts.FieldByName('Conta').AsInteger;
    //
    RespItem :=  Geral.FF0(QrOSPipIts.RecNo);
    Txt := Format(sAtzRespOS, [OsTxt, RespItem, RespCount]);
    MyObjects.Informa(LaAviso, True, Txt);
    if UpdateOSPipItsAtual(SQL, True) then
    begin
      DmkRDBMs.AbreSQLQuery0(Self, QrOSWPipItsP, DmModApp.FDAppDB, [
      'SELECT * ',
      'FROM oswpipitsp ',
      'WHERE Conta=' + Geral.FI64(Conta),
      '']);
      QrOSWPipItsP.First;
      while not QrOSWPipItsP.Eof do
      begin
        InsereOSWPipItsPAtual();
        //
        QrOSWPipItsP.Next;
      end;
    end;
    //
    QrOSPipIts.Next;
  end;
  //Result := DmkDAC.XMLSQL_ExecutaQuery(Self, stUpd, SQL) >= 0;
  //
  Tempo := Now() - Tempo;
  Geral.MB_Info('Tempo: ' + Geral.FDT(Tempo, 108));
  //
  Result := True;
end;

end.
