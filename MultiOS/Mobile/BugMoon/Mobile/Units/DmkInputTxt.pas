unit DmkInputTxt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.TabControl, FMX.Layouts, FMX.Memo, FMX.Edit, FMX.ListBox,
  ControlMover;

type
  TFmDmkInputTxt = class(TForm)
    tbaMain: TToolBar;
    lblTitle: TLabel;
    tabMain: TTabControl;
    tiMain: TTabItem;
    lsbInfo: TListBox;
    lbiProjectDescription: TListBoxItem;
    MeTxt: TMemo;
    lblInfoDescription: TLabel;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure MeTxtChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure MeTxtEnter(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FControlMover: TControlMover;
    procedure GetMoveControl(Sender: TObject; FocusedControl: TControl; var MoveControl: TControl);
  public
    FCustomEdit: TCustomEdit;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  FmDmkInputTxt: TFmDmkInputTxt;

implementation

{$R *.fmx}

procedure TFmDmkInputTxt.Button1Click(Sender: TObject);
begin
  Hide;
end;

procedure TFmDmkInputTxt.Button2Click(Sender: TObject);
begin
  Hide;
end;

constructor TFmDmkInputTxt.Create(AOwner: TComponent);
begin
  inherited;
  FControlMover := TControlMover.Create(Self);
  FControlMover.OnGetMoveControl := GetMoveControl;
end;

destructor TFmDmkInputTxt.Destroy;
begin
  FControlMover.Free;
  inherited;
end;

procedure TFmDmkInputTxt.FormCreate(Sender: TObject);
begin
  tabMain.SetFocus;
end;

procedure TFmDmkInputTxt.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  ModalResult := mrCancel;
{
  if Key = vkHardwareBack then
  begin
    BtDesisteClick(Self);
    //
    Key := 0;
  end;
}
end;

procedure TFmDmkInputTxt.GetMoveControl(Sender: TObject; FocusedControl: TControl; var MoveControl: TControl);
begin
  MoveControl := tabMain;
end;

// Assign this event to TMemo controls. It needs to move up as more lines are typed
procedure TFmDmkInputTxt.MeTxtChange(Sender: TObject);
begin
  FControlMover.SlideControl;
end;

procedure TFmDmkInputTxt.MeTxtEnter(Sender: TObject);
begin
//  MeTxt.SetFocus;
end;

end.
