﻿unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  System.Actions, FMX.ActnList, FMX.StdCtrls, FMX.Colors,
  UnApp_Vars, UnGeral, UnDmkDevice, UnMyObjects, UnDMkEnums, FMX.Edit,
  dmkVariable, FMX.ListBox, dmkComboBox, FMX.Layouts, FMX.Effects,
  dmkCompoundEdits, dmkCNPanel, dmkCNNumberBox, dmkCNClearingEdit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  dmkCNEdit, dmkCompVars,
  UnMsgDmk, UnGrl_Vars, System.Sensors, FMX.Sensors;

type
  TFmPrincipal = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    LaMsgIni: TLabel;
    Panel1: TPanel;
    BtSincroDown: TButton;
    BtOSWCab: TButton;
    BtSincroSettings: TButton;
    StyleBook1: TStyleBook;
    Panel4: TPanel;
    Label4: TLabel;
    dmkVariable1: TdmkVariable;
    dmkVariable2: TdmkVariable;
    Timer1: TTimer;
    FDQuery1: TFDQuery;
    BtSincroUp: TButton;
    BtTeste: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure BtSincroDownClick(Sender: TObject);
    procedure BtOSWCabClick(Sender: TObject);
    procedure dmkComboBox1Change(Sender: TObject);
    procedure dmkComboBox1Click(Sender: TObject);
    procedure BtSincroSettingsClick(Sender: TObject);
    procedure BtSincroUpClick(Sender: TObject);
    procedure BtTesteClick(Sender: TObject);
  private
    { Private declarations }
    FIniciadoApp: Boolean;
{
////////////////////////////////////////////////////////////////////////////////
Neste código o GPS funciona bem, mas o DisposeOf não reduz o uso do processador!
////////////////////////////////////////////////////////////////////////////////
    procedure Timer2Timer(Sender: TObject);
    FLocationSensor: TLocationSensor;
    //
    procedure CriaLocationSensor();
    procedure LocationSensorLocationChanged(Sender: TObject; const OldLocation,
      NewLocation: TLocationCoord2D);
}
  public
    { Public declarations }
    FObteveLocalizacao: Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses DmkBugMoon, VerifyDB, ModApp, UsuarioAdd, SincroDown, ModGerl, OSCabLista,
{$IF DEFINED(ANDROID)}
Intent_ZXing,
{$ENDIF}
UnDmkRDBMs, OSWPipMon, OSWPipIts, OSWPipItsRsp, ModProd, DmkInputTxt, DmkCNPsq1,
LocaleInfo3,
SincroSettings, SincroUp;


{$R *.fmx}
procedure TFmPrincipal.BtOSWCabClick(Sender: TObject);
begin
  FmOSCabLista.Show;
end;

procedure TFmPrincipal.BtSincroDownClick(Sender: TObject);
begin
  FmSincroDown.Show;
end;

procedure TFmPrincipal.BtSincroSettingsClick(Sender: TObject);
begin
  FmSincroSettings.Show;
end;

procedure TFmPrincipal.BtSincroUpClick(Sender: TObject);
begin
  FmSincroUp.Show;
end;

procedure TFmPrincipal.BtTesteClick(Sender: TObject);
{ 1.
var
  Preco: Double;
begin
  Preco := 123.45;
  if DmModProd.ObtemPreco(Preco) then
  begin
    Geral.MB_Info('Preço: ' + Geral.FFT(Preco, 2, siPositivo));
  end;
}
{ 2.
  procedure ShowMsg();
  begin
    if FDQuery1.Bof and FDQuery1.Eof then
      Geral.MB_Info('All')
    else
    if FDQuery1.Eof then
      Geral.MB_Info('Eof')
    else
    if FDQuery1.Bof then
      Geral.MB_Info('Bof')
    else
      Geral.MB_Info('<->');
  end;
var
  Texto: String;
begin
  DmkRDBMs.AbreSQLQuery0(Self, FDQuery1, DmModApp.FDAppDB, [
  'SELECT * ',
  'FROM ospipits ',
  //'WHERE Conta IN (539,596) ',
  'WHERE Conta = 596 ',
  '']);
  //
  ShowMsg();
  FDQuery1.Next;
  ShowMsg();
  FDQuery1.Next;
  ShowMsg();
  FDQuery1.Last;
  ShowMsg();
}

{ 3.
  DmkRDBMs.AbreSQLQuery0(Self, FDQuery1, DmModApp.FDAppDB, [
  'SELECT name FROM sqlite_master ',
  'WHERE type=''table'' ' ,
  'ORDER BY name;',
  '']);
  Texto := '';
  FDQuery1.First;
  while not FDQuery1.Eof do
  begin
    Texto := Texto + IntToStr(FDQuery1.RecNo) + ' - ' +
      FDQuery1.FieldByName('name').AsString + sLineBreak;
    //
    FDQuery1.Next;
  end;
  //
  Geral.MB_Info(Texto);
}
{ 4.
  FmDmkInputTxt.Show;
}
{ 5.
  MsgDmk.MostraTexto('Teste Memory Leak!');
{ 6.
const
  ACodeMode = 'QR Code';
begin
  FmIntent_ZXing.FEdit := EdQrCode;
  FmIntent_ZXing.CallZXing(ACodeMode);
}
begin
{
  MyObjects.CriaFm(TFmLocaleInfo, FmLocaleInfo);
  FObteveLocalizacao := False;
  FmLocaleInfo.Show;
  Timer2.Enabled := True;
}
  FmLocaleInfo3.Show;
end;

procedure TFmPrincipal.dmkComboBox1Change(Sender: TObject);
begin
{
  if dmkComboBox1.ItemIndex <> -1 then
    Label3.Text := 'Change: ' + dmkComboBox1.IDItems[dmkComboBox1.ItemIndex];
}
end;

procedure TFmPrincipal.dmkComboBox1Click(Sender: TObject);
begin
{
  if dmkComboBox1.ItemIndex <> -1 then
    Label3.Text := 'Click: ' + dmkComboBox1.IDItems[dmkComboBox1.ItemIndex];
}
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  Timer1.Enabled := False;
  App_Vars.DefineValDeVarsGeraisDoApp();
  TabControl1.ActiveTab := TabItem1;
  //
end;

procedure TFmPrincipal.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkMenu then
    FmOSCabLista.Show
  else
  if Key = vkHardwareBack then
  begin
    if VAR_CHAMOU_APP_EXTERNO then
    begin
      Geral.MB_Info('O encerramento será forçado!');
      Halt(0);
{
    if TabControl1.ActiveTab = TabItem2 then
    begin
      ChangeTabAction1.Tab := TabItem1;
      ChangeTabAction1.ExecuteTarget(Self);
      ChangeTabAction1.Tab := TabItem2;
      Key := 0;
}
    end;
  end;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

{
////////////////////////////////////////////////////////////////////////////////
Neste código o GPS funciona bem, mas o DisposeOf não reduz o uso do processador!
////////////////////////////////////////////////////////////////////////////////
procedure TFmPrincipal.Timer2Timer(Sender: TObject);
begin
  if FObteveLocalizacao then
  begin
    Timer2.Enabled := False;
    FObteveLocalizacao := False;
    LaLatitude.Text := Geral.ToFormStr('Latitude', FLatitude);
    LaLongitude.Text := Geral.ToFormStr('Longitude', FLongitude);
    FmLocaleInfo.Hide;
    FmLocaleInfo.DisposeOf;
  end;
end;

procedure TFmPrincipal.CriaLocationSensor();
begin
  FLocationSensor := TLocationSensor.Create(nil);
  try
    FLocationSensor.Active := True;
    //
    FLocationSensor.OnLocationChanged := LocationSensorLocationChanged;
    //(Sender: TObject; const OldLocation, NewLocation: TLocationCoord2D);
  except
    FLocationSensor.DisposeOf;
  end;
end;

procedure TFmPrincipal.LocationSensorLocationChanged(Sender: TObject;
  const OldLocation, NewLocation: TLocationCoord2D);
var
  LLatitude, LLongitude : string;
  LSettings: TFormatSettings;
  LDecSeparator : Char;
begin
  if FLocationSensor = nil then
    Exit;
  LDecSeparator := FormatSettings.DecimalSeparator;
  LSettings := FormatSettings;
  try
    FormatSettings.DecimalSeparator := '.';
    LaLatitude.Text  := Format('%2.6f', [NewLocation.Latitude]);
    LaLongitude.Text := Format('%2.6f', [NewLocation.Longitude]);
    //
    FLocationSensor.Active := False;
    FLocationSensor.OnLocationChanged := nil;
    FLocationSensor.DisposeOf;
    FLocationSensor.Free;
    FLocationSensor := nil;
  finally
    FormatSettings.DecimalSeparator := LDecSeparator;
  end;
end;
////////////////////////////////////////////////////////////////////////////////
 FIM código o GPS funciona bem, mas o DisposeOf não reduz o uso do processador!
////////////////////////////////////////////////////////////////////////////////
}

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
begin
  FmOSCabLista.Show;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
const
{$IF DEFINED(ANDROID)}
  TotFmCriar = 16;
{$ELSEIF DEFINED(MSWINDOWS)}
  TotFmCriar = 15;
{$ENDIF}
var
  AtuFmCriar: Integer;
  //
  procedure CriaFm(InstanceClass: TComponentClass; var Reference);
  var
    Msg: String;
  begin
    AtuFmCriar := AtuFmCriar + 1;
    if AtuFmCriar > TotFmCriar then
      Geral.MB_Info('"AtuFmCriar" por atualizar');
    Msg := 'Criando janela ' + Geral.FF0(AtuFmCriar) + '/' + Geral.FF0(TotFmCriar);
    MyObjects.Informa(LaMsgIni, True, Msg);
    MyObjects.CriaFm(InstanceClass, Reference);
  end;
begin
  Timer1.Enabled := False;
  if FIniciadoApp then
    Exit;
  //
  AtuFmCriar := 0;
  (*15*)CriaFm(TFmLocaleInfo3, FmLocaleInfo3);
  (*01*)CriaFm(TFmDmkBugMoon, FmDmkBugMoon);
  (*02*)CriaFm(TDmModApp, DmModApp);
  (*03*)CriaFm(TDmModGerl, DmModGerl);
  (*04*)CriaFm(TFmVerifyDB, FmVerifyDB);
  (*05*)CriaFm(TFmUsuarioAdd, FmUsuarioAdd);
  (*06*)CriaFm(TFmSincroDown, FmSincroDown);
  (*07*)CriaFm(TFmOSCabLista, FmOSCabLista);
  (*08*)CriaFm(TFmOSWPipMon, FmOSWPipMon);
  (*09*)CriaFm(TFmOSWPipIts, FmOSWPipIts);
  (*10*)CriaFm(TFmOSWPipItsRsp, FmOSWPipItsRsp);
  (*11*)CriaFm(TFmDmkInputTxt, FmDmkInputTxt);
  (*12*)CriaFm(TFmDmkCNPsq1, FmDmkCNPsq1);
  (*13*)CriaFm(TFmSincroSettings, FmSincroSettings);
  (*14*)CriaFm(TFmSincroUp, FmSincroUp);
{$IF DEFINED(ANDROID)}
  (*XX*)CriaFm(TFmIntent_ZXing, FmIntent_ZXing);
{$ENDIF}
  //
  VAR_FmDmkCNPsq1 := FmDmkCNPsq1;
  FIniciadoApp := True;
  Hide;
  FmDmkBugMoon.Show;
end;

end.
