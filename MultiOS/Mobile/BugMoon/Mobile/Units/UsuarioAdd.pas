unit UsuarioAdd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.StdCtrls, FMX.Edit, FMX.Layouts, FMX.Memo,
  FireDAC.Comp.Client,
  UnGeral, Xml.xmldom, Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, FMX.ScrollBox,
  FMX.Controls.Presentation;

type
  TFmUsuarioAdd = class(TForm)
    IdHTTP1: TIdHTTP;
    XMLDocument1: TXMLDocument;
    Panel1: TPanel;
    Memo1: TMemo;
    Panel2: TPanel;
    Label1: TLabel;
    EdUsuario: TEdit;
    Label2: TLabel;
    EdSenha: TEdit;
    Button1: TButton;
    Panel4: TPanel;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure TesteDeviceIdentification(Codigo: Integer; Acesso: String);
  public
    { Public declarations }
  end;

var
  FmUsuarioAdd: TFmUsuarioAdd;

implementation

uses UnDmkRDBMs, ModApp, UnDmkEnums, UnWebReq, UnMyListas, UnGrl_Vars,
UnDmkDevice;

{$R *.fmx}

procedure TFmUsuarioAdd.Button1Click(Sender: TObject);
var
  ResMsgCod: String;
  Node : IXMLNode;
  //
  //Entidade,
  Codigo: Integer;
  UserName, PassWord, Token, UsrID, UsrEnt: String;
  //
  Resp: TStringStream;
  Resposta: String;
begin
  Resp := TStringStream.Create('');
  try
    UserName := EdUsuario.Text;
    PassWord := EdSenha.Text;
    Memo1.Text := Resp.DataString;
    Resposta := WebReq.SQLArrPOST_TxtStr(Self, 'REST_LoginUser', [
    'Usuario=' + UserName,
    'Senha=' + PassWord]);
    Memo1.Text := Resp.DataString;
    //
    XMLDocument1.LoadFromXML(Resposta);
    Node := XMLDocument1.DocumentElement.ChildNodes.FindNode('usuario');
    if Node <> nil then
    begin
      ResMsgCod := Node.ChildNodes['MsgCod'].Text;
      //
      if (ResMsgCod <> '') and (ResMsgCod = '100') then
      begin
        Codigo   := Geral.IMV(Trim(Node.ChildNodes['UsrCod'].Text));
        UsrID    := Trim(Node.ChildNodes['UsrID'].Text);
        UsrEnt   := Trim(Node.ChildNodes['UsrEnt'].Text);
        Token    := Trim(Node.ChildNodes['Token'].Text);
        //
        DmkRDBMs.SQLDelSimple(Self, dbsrcLocalServer, 'usuarios', [
          'Codigo'], [
          Codigo], '');
        //
        if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, stIns, 'usuarios', False, [
          'UsrEnt', 'UsrID', 'Token'(*UsrAPI*)], [
          'Codigo'(*srCod*), 'UserName', 'PassWord'
        ], [
          UsrEnt, UsrID, Token
        ], [
          Codigo, UserName, PassWord
        ], True) then
        begin
          TesteDeviceIdentification(Codigo, Token);
          Geral.MB_Info('Usu�rio criado com sucesso!');
        end;
      end;
    end;
    Hide;
  finally
    Resp.Free;
  end;
end;

procedure TFmUsuarioAdd.FormCreate(Sender: TObject);
begin
  // Evitar uso. Criado ao iniciar o app.
end;

procedure TFmUsuarioAdd.TesteDeviceIdentification(Codigo: Integer; Acesso: String);
const
  Metodo = 'REST_DeviceIdentification';
var
  AplicativoSolicitante, IDUsuarioSolicitante, Token, DeviceID, DeviceName,
  DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion: String;
  Resposta: String;
begin
  AplicativoSolicitante := 'AplicativoSolicitante=' + Geral.FF0(CO_DMKID_APP);
  IDUsuarioSolicitante := 'IDUsuarioSolicitante=' + Geral.FF0((*VAR_WEB_USER_ID*)Codigo);
  Token := 'Token=' + (*VAR_WEB_TOKEN_DMK*)Acesso;
  DeviceID := 'DeviceID=' + DmkDevice.ObtemIMEI_MD5();
  DmkDevice.ObtemDadosAparelho(OSName, DeviceName, OSVersion, OSNickName);
  DeviceName := 'DeviceName=' + DeviceName;
  OSName := 'OSName=' + OSName;
  OSNickName := 'OSNickName=' + OSNickName;
  OSVersion := 'OSVersion=' + OSVersion;
  DvcScreenH := 'DvcScreenH=' + Geral.FF0(Screen.Size.Height);
  DvcScreenW := 'DvcScreenW=' + Geral.FF0(Screen.Size.Width);
  //
  Resposta := WebReq.SQLArrPOST_TxtStr(Self, Metodo, [
  AplicativoSolicitante, IDUsuarioSolicitante, Token, DeviceID, DeviceName,
  DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion]);
  //
  Geral.MB_Aviso(Resposta);
end;

end.

