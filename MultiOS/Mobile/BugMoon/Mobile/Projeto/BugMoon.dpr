program BugMoon;

uses
  System.StartUpCopy,
  FMX.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  VerifyDB in '..\Units\VerifyDB.pas' {FmVerifyDB},
  UnDmkEnums in '..\..\..\Geral\UnDmkEnums.pas',
  UnGeral in '..\..\..\Geral\UnGeral.pas',
  UnGrlListas in '..\..\..\Geral\UnGrlListas.pas',
  UnMyObjects in '..\..\..\Geral\UnMyObjects.pas',
  ModApp in '..\Units\ModApp.pas' {DmModApp},
  UsuarioAdd in '..\Units\UsuarioAdd.pas' {FmUsuarioAdd},
  UnWebReq in '..\..\..\Geral\UnWebReq.pas',
  UnMyListas in '..\Units\UnMyListas.pas',
  UnApp_Consts in '..\Units\UnApp_Consts.pas',
  SincroDown in '..\Units\SincroDown.pas' {FmSincroDown},
  UnMyLinguas in '..\..\..\..\AllOS\Listas\UnMyLinguas.pas',
  UnBugs_Tabs in '..\..\..\..\Aplicativos\Bugstrol\UnBugs_Tabs.pas',
  UnDmkListas in '..\..\..\..\AllOS\Listas\UnDmkListas.pas',
  UnBugMbl_Tabs in '..\..\..\..\Aplicativos\Bugstrol\UnBugMbl_Tabs.pas',
  UnPerfJan_Tabs in '..\..\..\..\AllOS\Listas\UnPerfJan_Tabs.pas',
  UnDmkRDBMs in '..\..\..\..\AllOS\RDBMs\UnDmkRDBMs.pas',
  UnDmkSQLite in '..\..\..\..\AllOS\RDBMs\SQLite\UnDmkSQLite.pas',
  UnSQLite_PF in '..\..\..\..\AllOS\RDBMs\SQLite\UnSQLite_PF.pas',
  UnCheckSQLite in '..\..\..\..\AllOS\RDBMs\SQLite\UnCheckSQLite.pas',
  ModGerl in '..\..\..\Geral\ModGerl.pas' {DmModGerl},
  UnApp_Vars in '..\Units\UnApp_Vars.pas',
  UnDmkDevice in '..\..\..\..\AllOS\Devices\UnDmkDevice.pas',
  UnGrl_Vars in '..\..\..\Geral\UnGrl_Vars.pas',
  UnGrl_Consts in '..\..\..\Geral\UnGrl_Consts.pas',
  UnProjGroup_Consts in '..\..\..\..\Aplicativos\Bugstrol\UnProjGroup_Consts.pas',
  UnDmkDAC in '..\..\..\..\AllOS\RDBMs\UnDmkDAC.pas',
  UnProjGroup_Vars in '..\..\..\..\Aplicativos\Bugstrol\UnProjGroup_Vars.pas',
  UnGrl_Tabs in '..\..\..\Geral\UnGrl_Tabs.pas',
  OSCabLista in '..\Units\OSCabLista.pas' {FmOSCabLista},
  MultiDetailAppearanceU in '..\Units\MultiDetailAppearanceU.pas',
  DmkBugMoon in '..\Units\DmkBugMoon.pas' {FmDmkBugMoon},
  OSWPipMon in '..\Units\OSWPipMon.pas' {FmOSWPipMon},
  UnFMX_Vars in '..\..\..\Geral\UnFMX_Vars.pas',
  OSWPipIts in '..\Units\OSWPipIts.pas' {FmOSWPipIts},
  OSWPipItsRsp in '..\Units\OSWPipItsRsp.pas' {FmOSWPipItsRsp},
  UnMsgDmk in '..\..\..\..\AllOS\FormsPF\UnMsgDmk.pas',
  DmkMsg in '..\..\..\..\AllOS\FormsPF\DmkMsg.pas' {FmDmkMsg},
  UnFormsPF in '..\..\..\..\AllOS\FormsPF\UnFormsPF.pas',
  ModProd in '..\..\..\..\AllOS\Grade\ModProd.pas' {DmModProd},
  Intent_zxing in '..\Units\Intent_zxing.pas' {FmIntent_ZXing},
  DmkInputTxt in '..\Units\DmkInputTxt.pas' {FmDmkInputTxt},
  DmkCNPsq1 in '..\..\..\..\..\Componentes Delphi XE5\dmkComp\Forms\MSWindows\DmkCNPsq1.pas' {FmDmkCNPsq1},
  dmkCompVars in '..\..\..\..\..\Componentes Delphi XE5\dmkComp\Units\dmkCompVars.pas',
  SincroSettings in '..\Units\SincroSettings.pas' {FmSincroSettings},
  SincroUp in '..\Units\SincroUp.pas' {FmSincroUp},
  ControlMover in '..\..\..\Terceiros\Control Mover\ControlMover.pas',
  UnMySQL_PF in '..\..\..\..\AllOS\RDBMs\MySQL\UnMySQL_PF.pas',
  LocaleInfo3 in '..\..\..\..\AllOS\Devices\LocaleInfo3.pas' {FmLocaleInfo3};

{$R *.res}

begin
  Application.Initialize;
  Application.FormFactor.Orientations := [TFormOrientation.soPortrait];
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmLocaleInfo3, FmLocaleInfo3);
  Application.Run;
end.
