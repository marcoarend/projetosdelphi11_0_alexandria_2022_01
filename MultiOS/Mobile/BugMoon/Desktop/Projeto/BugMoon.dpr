program BugMoon;

uses
  System.StartUpCopy,
  FMX.Forms,
  VerifyDB in '..\..\Mobile\Units\VerifyDB.pas' {FmVerifyDB},
  ModApp in '..\..\Mobile\Units\ModApp.pas' {DmModApp},
  UsuarioAdd in '..\..\Mobile\Units\UsuarioAdd.pas' {FmUsuarioAdd},
  DmkBugMoon in '..\..\Mobile\Units\DmkBugMoon.pas' {FmDmkBugMoon},
  UnSQLite_PF in '..\..\..\..\AllOS\RDBMs\SQLite\UnSQLite_PF.pas',
  UnGrl_Consts in '..\..\..\Geral\UnGrl_Consts.pas',
  UnGrl_Tabs in '..\..\..\Geral\UnGrl_Tabs.pas',
  UnGrl_Vars in '..\..\..\Geral\UnGrl_Vars.pas',
  UnMyObjects in '..\..\..\Geral\UnMyObjects.pas',
  UnWebReq in '..\..\..\Geral\UnWebReq.pas',
  UnDmkEnums in '..\..\..\Geral\UnDmkEnums.pas',
  UnGeral in '..\..\..\Geral\UnGeral.pas',
  UnApp_Vars in '..\..\Mobile\Units\UnApp_Vars.pas',
  UnDmkSQLite in '..\..\..\..\AllOS\RDBMs\SQLite\UnDmkSQLite.pas',
  UnDmkRDBMs in '..\..\..\..\AllOS\RDBMs\UnDmkRDBMs.pas',
  UnCheckSQLite in '..\..\..\..\AllOS\RDBMs\SQLite\UnCheckSQLite.pas',
  SincroDown in '..\..\Mobile\Units\SincroDown.pas' {FmSincroDown},
  UnMyListas in '..\..\Mobile\Units\UnMyListas.pas',
  ModGerl in '..\..\..\Geral\ModGerl.pas' {DmModGerl},
  UnMyLinguas in '..\..\..\..\AllOS\Listas\UnMyLinguas.pas',
  UnDmkListas in '..\..\..\..\AllOS\Listas\UnDmkListas.pas',
  UnPerfJan_Tabs in '..\..\..\..\AllOS\Listas\UnPerfJan_Tabs.pas',
  UnBugs_Tabs in '..\..\..\..\Aplicativos\Bugstrol\UnBugs_Tabs.pas',
  UnBugMbl_Tabs in '..\..\..\..\Aplicativos\Bugstrol\UnBugMbl_Tabs.pas',
  UnDmkDevice in '..\..\..\..\AllOS\Devices\UnDmkDevice.pas',
  UnitMD5 in '..\..\..\..\..\Delphi\Outros\Encrypt\UnitMD5.pas',
  UnProjGroup_Consts in '..\..\..\..\Aplicativos\Bugstrol\UnProjGroup_Consts.pas',
  UnProjGroup_Vars in '..\..\..\..\Aplicativos\Bugstrol\UnProjGroup_Vars.pas',
  UnDmkDAC in '..\..\..\..\AllOS\RDBMs\UnDmkDAC.pas',
  OSCabLista in '..\..\Mobile\Units\OSCabLista.pas' {FmOSCabLista},
  MultiDetailAppearanceU in '..\..\Mobile\Units\MultiDetailAppearanceU.pas',
  Principal in '..\..\Mobile\Units\Principal.pas' {FmPrincipal},
  OSWPipMon in '..\..\Mobile\Units\OSWPipMon.pas' {FmOSWPipMon},
  UnFMX_Vars in '..\..\..\Geral\UnFMX_Vars.pas',
  OSWPipIts in '..\..\Mobile\Units\OSWPipIts.pas' {FmOSWPipIts},
  OSWPipItsRsp in '..\..\Mobile\Units\OSWPipItsRsp.pas' {FmOSWPipItsRsp},
  UnApp_Consts in '..\..\Mobile\Units\UnApp_Consts.pas',
  ModProd in '..\..\..\..\AllOS\Grade\ModProd.pas' {DmModProd},
  UnFormsPF in '..\..\..\..\AllOS\FormsPF\UnFormsPF.pas',
  UnMsgDmk in '..\..\..\..\AllOS\FormsPF\UnMsgDmk.pas',
  DmkMsg in '..\..\..\..\AllOS\FormsPF\DmkMsg.pas' {FmDmkMsg},
  DmkInputTxt in '..\..\Mobile\Units\DmkInputTxt.pas' {FmDmkInputTxt},
  DmkCNPsq1 in '..\..\..\..\..\Componentes Delphi XE5\dmkComp\Forms\MSWindows\DmkCNPsq1.pas' {FmDmkCNPsq1},
  dmkCompUtils in '..\..\..\..\..\Componentes Delphi XE5\dmkComp\Units\dmkCompUtils.pas',
  SincroSettings in '..\..\Mobile\Units\SincroSettings.pas' {FmSincroSettings},
  SincroUp in '..\..\Mobile\Units\SincroUp.pas' {FmSincroUp},
  ControlMover in '..\..\..\Terceiros\Control Mover\ControlMover.pas',
  UnMySQL_PF in '..\..\..\..\AllOS\RDBMs\MySQL\UnMySQL_PF.pas',
  LocaleInfo3 in '..\..\..\..\AllOS\Devices\LocaleInfo3.pas' {FmLocaleInfo3};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
