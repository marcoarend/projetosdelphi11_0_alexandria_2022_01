unit ControlMover;

interface

uses
  FMX.Forms, FMX.Controls, System.Types, FMX.Types;

type
  TSaveProperties = record
    Control: TControl;
    Align: TAlignLayout;
    Position: TPointF;
  end;

  TGetMoveControlEvent = procedure(Sender: TObject; FocusedControl: TControl; var MoveControl: TControl) of object;

  TControlMover = class(TObject)
  private
    FForm: TCommonCustomForm;
    FSaveProps: TSaveProperties;
    FVKBounds: TRect;
    FVKVisible: Boolean;
    FOnGetMoveControl: TGetMoveControlEvent;
    procedure DoGetMoveControl;
    function GetFocusedControlOffset(KeyboardRect: TRect): Single;
    function FocusedControl: TControl;
    procedure FormVirtualKeyboardHidden(Sender: TObject; KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject; KeyboardVisible: Boolean; const Bounds: TRect);
  public
    constructor Create(AForm: TCommonCustomForm);
    procedure SlideControl;
    property OnGetMoveControl: TGetMoveControlEvent read FOnGetMoveControl write FOnGetMoveControl;
  end;

implementation

uses
  FMX.Memo;

{ TControlMover }

constructor TControlMover.Create(AForm: TCommonCustomForm);
begin
  inherited Create;
  FForm := AForm;
  FForm.OnVirtualKeyboardShown := FormVirtualKeyboardShown;
  FForm.OnVirtualKeyboardHidden := FormVirtualKeyboardHidden;
end;

procedure TControlMover.FormVirtualKeyboardHidden(Sender: TObject; KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FVKVisible := False;
  if Assigned(FSaveProps.Control) then
  begin
    FSaveProps.Control.AnimateFloat('Position.Y', FSaveProps.Position.Y, 0.1);
    FSaveProps.Control.Align := FSaveProps.Align;
  end;
end;

procedure TControlMover.FormVirtualKeyboardShown(Sender: TObject; KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FVKVisible := True;
  FVKBounds := Bounds;
  DoGetMoveControl;
  if Assigned(FSaveProps.Control) then
  begin
    FSaveProps.Control.Align := TAlignLayout.alNone;
    FSaveProps.Position.Y := FSaveProps.Control.Position.Y;
    SlideControl;
  end;
end;

procedure TControlMover.DoGetMoveControl;
var
  MoveControl: TControl;
begin
  MoveControl := nil;
  if Assigned(FOnGetMoveControl) then
    FOnGetMoveControl(Self, FocusedControl, MoveControl);
  FSaveProps.Control := MoveControl;
end;

function TControlMover.FocusedControl: TControl;
begin
  Result := nil;
  if Assigned(FForm.Focused) and (FForm.Focused.GetObject is TControl) then
    Result := TControl(FForm.Focused.GetObject);
end;

function TControlMover.GetFocusedControlOffset(KeyboardRect: TRect): Single;
var
  Control: TControl;
  ControlPos: TPointF;
  ControlHeight: Single;
  Memo: TMemo;
  Caret: TCaret;
  ARect: TRect;
begin
  Result := 0;
  Control := FocusedControl;
  if Assigned(Control) then
  begin
    ControlPos := Control.LocalToAbsolute(PointF(0, 0));
    ControlHeight := Control.Height;
    if Control is TMemo then
    begin
      Memo := TMemo(Control);
      Caret := Memo.Caret;
      ControlPos.Y := ControlPos.Y + (Caret.Pos.Y - Memo.ViewportPosition.Y);
      ControlHeight := Caret.Size.Height + 4;
    end;
    ARect := KeyboardRect;
    Result := (ControlPos.Y + ControlHeight + 2) - ARect.Top + 21;
    if Result < 0 then
      Result := 0;
  end;
end;

procedure TControlMover.SlideControl;
begin
  if FVKVisible and Assigned(FSaveProps.Control) then
    FSaveProps.Control.AnimateFloat('Position.Y', FSaveProps.Control.Position.Y - GetFocusedControlOffset(FVKBounds), 0.1);
end;

end.
