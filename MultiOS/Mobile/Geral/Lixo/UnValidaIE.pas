unit UnValidaIE;

interface

uses  System.SysUtils, System.UITypes, System.Variants, (*System.IOUtils,
  FMX.Forms, FMX.Dialogs, FMX.Edit, System.MaskUtils,*)
  UnDmkEnums, UnGeral;

type
  TUnValidaIE = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  Valida_IE_SemErrLength(IE, UF: String; ln, lv: Integer):Boolean;
    function  Valida_IE(IE: String; UF: Variant; LinkMsk: String; ForcaTrue:
              Boolean = True): Boolean;var
    function  Valida_IE_AC(IE, UF: String): Boolean;
    function  Valida_IE_AL(IE, UF: String): Boolean;
    function  Valida_IE_AP(IE, UF: String): Boolean;
    function  Valida_IE_BA(IE, UF: String): Boolean;
    function  Valida_IE_11(IE, UF: String): Boolean; // M�dulo 11 => 10 e 11 = 0; CE, ES, AM
    function  Valida_IE_DF(IE, UF: String): Boolean;
    function  Valida_IE_GO(IE, UF: String): Boolean;
    function  Valida_IE_MA(IE, UF: String): Boolean;
    function  Valida_IE_MG(IE, UF: String): Boolean;
    function  Valida_IE_MT(IE, UF: String): Boolean;
    function  Valida_IE_MS(IE, UF: String): Boolean;
    function  Valida_IE_PA(IE, UF: String): Boolean;
    function  Valida_IE_PB(IE, UF: String): Boolean;
    function  Valida_IE_PE(IE, UF: String): Boolean;
    function  Valida_IE_PI(IE, UF: String): Boolean;
    function  Valida_IE_PR(IE, UF: String): Boolean;
    function  Valida_IE_RJ(IE, UF: String): Boolean;
    function  Valida_IE_RN(IE, UF: String): Boolean;
    function  Valida_IE_RO(IE, UF: String): Boolean;
    function  Valida_IE_RR(IE, UF: String): Boolean;
    function  Valida_IE_RS(IE, UF: String): Boolean;
    function  Valida_IE_SC(IE, UF: String): Boolean;  // Modulo 11
    function  Valida_IE_SE(IE, UF: String): Boolean;
    function  Valida_IE_SP(IE, UF: String): Boolean;
    function  Valida_IE_TO(IE, UF: String): Boolean;
  end;
var
  ValidaIE: TUnValidaIE;

implementation

{ TUnValidaIE }

function TUnValidaIE.Valida_IE(IE: String; UF: Variant; LinkMsk: String;
  ForcaTrue: Boolean): Boolean;
var
  NEstado: String;
  vt: Integer;
begin
  Result := False;
  if (Trim(IE) = '') or (Uppercase(Trim(IE)) = 'ISENTO') or (UF = Null) then
  begin
    Result := True;
    Exit;
  end;
  vt := VarType(UF);
  if (vt = vtString) or (vt = varString)
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
  or (vt = varString)
{$ENDIF}
  then
    UF := Geral.GetCodigoUF_da_SiglaUF(UF);
  case UF of
    -27: NEstado := 'Tocantins (TO)';
    -26: NEstado := 'S�o Paulo (SP)';
    -25: NEstado := 'Sergipe (SE)';
    -24: NEstado := 'Santa Catarina (SC)';
    -23: NEstado := 'Rio Grande do Sul (RS)';
    -22: NEstado := 'Roraima (RR)';
    -21: NEstado := 'Rond�nia (RO)';
    -20: NEstado := 'Rio Grande do Norte (RN)';
    -19: NEstado := 'Rio de Janeiro (RJ)';
    -18: NEstado := 'Paran� (PR)';
    -17: NEstado := 'Piau� (PI)';
    -16: NEstado := 'Pernambuco (PE)';
    -15: NEstado := 'Para�ba (PB)';
    -14: NEstado := 'Par� (PA)';
    -13: NEstado := 'Mato Grosso (MT)';
    -12: NEstado := 'Mato Grosso do Sul (MS)';
    -11: NEstado := 'Minas Gerais (MG)';
    -10: NEstado := 'Maranh�o (MA)';
    -09: NEstado := 'Goi�s (GO)';
    -08: NEstado := 'Esp�rito Santo (ES)';
    -07: NEstado := 'Distrito Federal (DF)';
    -06: NEstado := 'Cear� (CE)';
    -05: NEstado := 'Bahia (BA)';
    -04: NEstado := 'Amap� (AP)';
    -03: NEstado := 'Amazonas (AM)';
    -02: NEstado := 'Alagoas (AL)';
    -01: NEstado := 'Acre (AC)';
  end;
  if UF = -27 then Result := Valida_IE_TO(IE, NEstado) else
  if UF = -26 then Result := Valida_IE_SP(IE, NEstado) else
  if UF = -25 then Result := Valida_IE_SE(IE, NEstado) else
  if UF = -24 then Result := Valida_IE_SC(IE, NEstado) else
  if UF = -23 then Result := Valida_IE_RS(IE, NEstado) else
  if UF = -22 then Result := Valida_IE_RR(IE, NEstado) else
  if UF = -21 then Result := Valida_IE_RO(IE, NEstado) else
  if UF = -20 then Result := Valida_IE_RN(IE, NEstado) else
  if UF = -19 then Result := Valida_IE_RJ(IE, NEstado) else
  if UF = -18 then Result := Valida_IE_PR(IE, NEstado) else
  if UF = -17 then Result := Valida_IE_PI(IE, NEstado) else
  if UF = -16 then Result := Valida_IE_PE(IE, NEstado) else
  if UF = -15 then Result := Valida_IE_PB(IE, NEstado) else
  if UF = -14 then Result := Valida_IE_PA(IE, NEstado) else
  if UF = -13 then Result := Valida_IE_MT(IE, NEstado) else
  if UF = -12 then Result := Valida_IE_MS(IE, NEstado) else
  if UF = -11 then Result := Valida_IE_MG(IE, NEstado) else
  if UF = -10 then Result := Valida_IE_MA(IE, NEstado) else
  if UF = -09 then Result := Valida_IE_GO(IE, NEstado) else
  if UF = -08 then Result := Valida_IE_11(IE, NEstado) else
  if UF = -07 then Result := Valida_IE_DF(IE, NEstado) else
  if UF = -06 then Result := Valida_IE_11(IE, NEstado) else
  if UF = -05 then Result := Valida_IE_BA(IE, NEstado) else
  if UF = -04 then Result := Valida_IE_AP(IE, NEstado) else
  if UF = -03 then Result := Valida_IE_11(IE, NEstado) else
  if UF = -02 then Result := Valida_IE_AL(IE, NEstado) else
  if UF = -01 then Result := Valida_IE_AC(IE, NEstado) else
  begin
    if UF <> 0 then
    begin
      Geral.MB_Aviso('O estado ' + NEstado + ' ainda n�o tem rotina '+
      'de verifica��o no aplicativo!');
      Result := False;
    end else
      Geral.MB_Aviso('N�o foi definido o estado (UF) para verifica��o da I.E.' +
      slineBreak + 'LinkMsk = "' + LinkMsk + '"' +
      slineBreak + 'Geral.Valida_IE()');
    Exit;
  end;
  if Result = False then
  begin
    {MessageBox(0, PChar('Inscri��o Estadual inv�lida para o estado '+
    NEstado+'!'), 'Erro', MB_OK+MB_ICONERROR);
    }
    Geral.MB_Erro(
    'Inscri��o Estadual pode ser inv�lida para o estado ' + NEstado + '!');
    if ForcaTrue then
      Result := True;
    Exit;
  end;
end;

function TUnValidaIE.Valida_IE_PR(IE, UF: String): Boolean;
(*
Formato da Inscri��o: NNNNNNNN-DD
C�lculo do Primeiro D�gito: M�dulo 11 com pesos de 2 a 7,
aplicados da direita para esquerda,  sobre as 8 primeiras posi��es.
C�lculo do Segundo D�gito: M�dulo 11 com pesos de 2 a 7,
aplicados da direita para esquerda,  sobre as 9 primeiras posi��es
(inclui o primeiro d�gito).
1� Passo: Multiplicar, da direita para esquerda, cada algarismo por pesos
de 2 a 7, respectivamente, somar os resultados, calcular o m�dulo 11 e
subtrair de 11. Se resto for 1 u 0, veja observa��o abaixo:
1 2 3 4 5 6 7 8 (CAD)
3 2 7 6 5 4 3 2 (pesos)
=> 8x2 + 7x3 + 6x4 + 5x5 + 4x6 + 3x7 + 2x2 + 1x3
=> 16+ 21 + 24 + 25 + 24 + 21 + 4 + 3 = 138
C�lculo de m�dulo 11 (resto da divis�o por 11)
138 / 11 = 12 => resto 6 (m�dulo11)
11 - 6 = 5 (1� d�gito verificador).
2� Passo: Acrescentar o 1� d�gito calculado ao CAD.: 123.45678-5

3� Passo: Repetir 2� passo ao CAD do 3� passo.
1 2 3 4 5 6 7 8 5 (CAD)
4 3 2 7 6 5 4 3 2 (pesos)
=> 5x2 + 8x3 + 7x4 + 6x5 + 5x6 + 4x7 + 3x2 + 2x3 1x4
=> 10 + 24 + 28 + 30 + 30 + 28 + 6 + 6 + 4 = 166
C�lculo de m�dulo 11 (resto da divis�o por 11)
166 / 11 = 15 => resto 1 (m�dulo 11)
11 - 1 =  10
Obs: Como cada d�gito significa um algarismo, no caso do resto ser 1 ou 0, gerando "d�gitos" 10 e 11, definimos o d�gito como sendo 0.

4� Passo: Resultado final - CAD 123.45678-50
*)
const
  vs1 = '32765432'; vs2 = '432765432'; ln = 08; lv= 2;
var
  x, num, ver2, ver1: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
    ver1 := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver1) > 9 then ver1 := '0';
    Num := Num+ver1;
    Soma := 0;
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
    ver2 := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver2) > 9 then ver2 := '0';
    Num := Num + ver2;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_BA(IE, UF: String): Boolean;
(*
O n�mero de inscri��o estadual no Cadastro de Contribuintes do Estado da Bahia possui 9 algarismos sendo 7 d�gitos inteiros e 2 d�gitos verificadores.

O c�lculo dos d�gitos verificadores � feito atrav�s de pesos (2 a 8 para o c�lculo do 2� d�gito e 2 a 9 para o 1� d�gito).
O c�lculo dos d�gitos verificadores se inicia com o 2� d�gito que ir� servir de peso para o c�lculo do 1� d�gito.
O m�dulo de c�lculo depende do valor do segundo d�gito da inscri��o.




1. Para inscri��es cujo segundo d�gito � 0, 1, 2, 3, 4, 5, 8 c�lculo pelo m�dulo 10


Exemplo: I.E. n�mero 1 2 3 4 5 6 7 - 4 8

1.1. C�lculo do 2� d�gito
Pesos de 2 a 8, aplicados em ordem decrescente, atribuindo-se um peso a cada d�gito da I.E. conforme exemplo a seguir:
1 2 3 4 5 6 7
8 7 6 5 4 3 2 pesos C�lculo:
(8 x 1 ) + ( 7 x 2 ) + ( 6 x 3 ) + ( 5 x 4 ) + ( 4 x 5 ) + ( 3 x 6 ) + ( 2 x 7 ) = 112
O resto da divis�o entre o resultado da opera��o acima 112 e o m�dulo 10 � igual a 2
Assim 10 (m�dulo) - 2 (resto da divis�o) = 8 (2� d�gito verificador da I.E. 1234567-48).
Obs.: quando o resto for igual a 0 (zero) o digito � igual a 0 (zero).


1.2. C�lculo do 1� d�gito
Pesos de 2 a 9, aplicados em ordem decrescente, atribuindo-se um peso a cada d�gito da I.E. j� acrescida do 2� d�gito de verifica��o obtido na opera��o anterior, conforme exemplo a seguir:
1 2 3 4 5 6 7 8
9 8 7 6 5 4 3 2 pesos C�lculo:
(9 X 1) + ( 8 x 2 ) + ( 7 x 3 ) + ( 6 x 4 ) + ( 5 x 5 ) + ( 4 x 6 ) + ( 3 x 7 ) + ( 2 x 8 ) = 156

Obs.: quando o resto for igual a 0 (zero) o digito � igual a 0 (zero)
O resto da divis�o entre o resultado da opera��o acima 156 e o m�dulo 10 � igual a 6
Assim 10 (m�dulo) - 6 (resto da divis�o) = 4 (1� d�gito verificador da I.E. 1234567-48).


2. Para inscri��es cujo segundo d�gito � 6, 7 ou 9 c�lculo pelo m�dulo 11


Exemplo: I.E. n�mero 1 6 2 3 4 5 6 -5 1

2.1. C�lculo do 2� d�gito
Pesos de 2 a 8, aplicados da direita para esquerda, atribuindo um peso a cada d�gito da I.E.:
1 6 2 3 4 5 6
8 7 6 5 4 3 2 pesos C�lculo:
(8 X 1) + ( 7 x 6 ) + ( 6 x 2 ) + ( 5 x 3 ) + ( 4 x 4 ) + ( 3 x 5) + ( 2 x 6 ) = 120
O resto da divis�o entre o resultado da opera��o acima 120 e o m�dulo 11 � igual a 1
Assim 11 (m�dulo) - 10 (resto da divis�o) = 1 (2� d�gito verificador da I.E. 1623456 -51).
Obs.: quando o resto for igual a 0 (zero) ou 1 (um) o digito � igual a 0 (zero)


2.2. C�lculo do 1� d�gito
Pesos de 2 a 9 aplicados um a um a cada d�gito da I.E. incluindo o 2� d�gito obtido na opera��o anterior.
1 6 2 3 4 5 6 1
9 8 7 6 5 4 3 2 pesos C�lculo:
(9 x 1) + ( 8 x 6 ) + ( 7 x 2 ) + ( 6 x 3 ) + ( 5 x 4 ) + ( 4 x 5 ) + ( 3 x 6 ) + ( 2 x 1 ) = 149

Obs.: quando o resto for igual a 0 (zero) ou 1 (um) o digito � igual a 0 (zero)
O resto da divis�o entre o resultado da opera��o acima 149 e o m�dulo 11 � igual a 6
Assim 11 (m�dulo) - 6 (resto da divis�o) = 5 (1� d�gito verificador da I.E. 1623456-51).

*)
const
  vs1 = '8765432'; vs2 = '98765432'; ln = 07; lv= 2;
var
  x, num, ver2, ver1: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if not (CharInSet(x[2], (['6', '7', '9']))) then
    begin
      // M�dulo 10
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
      ver2 := IntToStr(10 -(Soma mod 10));
      if StrToInt(ver2) > 9 then ver2 := '0';
      Num := Num + ver2;
      Soma := 0;
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
      ver1 := IntToStr(10 -(Soma mod 10));
      if StrToInt(ver1) > 9 then ver1 := '0';
      Num := Copy(x, 1, ln)+ver1+ver2;
    end else begin
      // M�dulo 11
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
      ver2 := IntToStr(11 -(Soma mod 11));
      if StrToInt(ver2) > 9 then ver2 := '0';
      Num := Num + ver2;
      Soma := 0;
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
      ver1 := IntToStr(11 -(Soma mod 11));
      if StrToInt(ver1) > 9 then ver1 := '0';
      Num := Copy(x, 1, ln)+ver1+ver2;
    end;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_11(IE, UF: String): Boolean;
(*
Formato: 8 d�gitos + 1 d�gito verificador
Exemplo: CGF n�mero 06000001-5
Atribui-se um peso a cada d�gito do CGF, excluindo o d�gito verificador,
seguindo a ordem abaixo:
0 6 0 0 0 0 0 1
9 8 7 6 5 4 3 2 pesos
C�lculo:
(9 x 0) + (8 x 6) + (7 x 0) + (6 x 0) +
(5 x 0) + (4 x 0) + (3 x 0) + (2 x 1) = 50
O resto da divis�o entre o resultado da opera��o acima 50 e o m�dulo 11 � igual a 6;
Calculamos 11, que � o m�dulo, menos 6, resultado obtido acima, d� igual a 5
que � o d�gito verificador do CGF 06000001. Caso a diferen�a der 10 ou 11,
o d�gito � 0
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_TO(IE, UF: String): Boolean;
(*
Governo do Estado de Tocantins - Roteiro de Cr�tica da Inscri��o Estadual
Aplica-se o c�lculo "m�dulo 11" com os algarismos 1,2,5,6,7,8,9,10 da Inscri��o Estadual, criando o "11�" que � o d�gito verificador. Os d�gitos "3" e "4" n�o entram no c�lculo, por�m podem assumir somente os seguintes valores:

01 = Produtor Rural ( n�o possui CGC)
02 = Industria e Com�rcio
03 = Empresas Rudimentares
99 = Empresas do Cadastro Antigo (SUSPENSAS)

Exemplo: 29 01 022783 6
2 9 0 1 0 2 2 7 8 3 6
9 8 - - 7 6 5 4 3 2 -
 SOMA I = ( ( PRIMEIRO * 9) + (SEGUNDO * 8) + (TERCEIRO * 7) + (QUARTO * 6) +
 (QUINTO * 5) +  (SEXTO * 4) + (S�TIMO * 3) + (OITAVO * 2) )


DIVIS�O MODAL DA "SOMA I" POR "ONZE"

ONDE:

170 = (2 * 9) + (9 * 8) + (0 * 7) + (2 * 6) + (2 * 5) + (7 * 4) + (8 * 3) + (3 * 2)
----->> 170 / 11 = 15
-----� resto = 5 (pois 15 x 11 = 165) 5 > 2
portanto
D�GITO = 11 - 5 OU SEJA D�GITO = 6

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  k, i, Soma: Integer;
begin
  //Result := False;
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Length(x) = 9 then
    x := Copy(x, 1, 2) + '99' + Copy(x, 3);
  if Valida_IE_SemErrLength(x, UF, ln, lv+2) then
  begin
    k := Geral.IMV(Copy(x, 3, 2));
    if not(k in ([1, 2, 3, 99])) then
    begin
      Geral.MB_Aviso('Tamanho da incri��o estadual inv�lido!');
    end;
    num := x[1]+x[2]+Copy(x, 5, 6);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    //Num := Num + ver;
    if x[11] = ver then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_SE(IE, UF: String): Boolean;
(*
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_SemErrLength(IE, UF: String; ln,
  lv: Integer): Boolean;
var
  x: String;
begin
  x := Geral.SoNumero_TT(IE);
  if Length(IE) <> ln+lv then
  begin
    Geral.MB_Aviso('Poss�vel inscri��o estadual inv�lida!' + slineBreak +
    'Estado (UF): ' + UF + slineBreak + 'Inscri��o estadual: ' + IE + slineBreak +
    'Motivo: Quantidade de d�gitos inv�lido para o estado '+ uf+'.');
    Result := False;
  end else
    Result := True;
end;

function TUnValidaIE.Valida_IE_SC(IE, UF: String): Boolean;
(*
Exemplo: inscri��o 251.040.852
Atribui-se um peso a cada d�gito da inscri��o exceto o ultimo digito a esquerda
2 5 1 0 4 0 8 5 (inscri��o)
9 8 7 6 5 4 3 2 (pesos)
C�lculo:

(9 x 2) + (8 x 5) + (7 x 1) + (6 x 0) + (5 x 4) + (4 x 0) + (3 x 8) + (2 x 5) = 119

Divida o resultado por 11: 119 / 11 = 10 tendo o resto da divis�o igual a 9
Subtraia o resto da divis�o (9) de 11 para obter o digito verificador: 11 � 9 = 2
Obs: Quando o resto da divis�o for zero (0) ou um (1), o digito ser� zero (0).

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_RR(IE, UF: String): Boolean;
(*
*)
const
  vs = '12345678'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if x[1]+x[2] <> '24' then
    begin
      Geral.MB_Erro('Os dois primeiros d�gitos devem ser "24" para ' + UF + '!' +
      sLineBreak + 'Valida_IE_RR()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // M�dulo 09
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr((Soma mod 9));
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_RO(IE, UF: String): Boolean;
(*
Formato: 3 d�gitos (munic�pio) + 5 d�gitos (empresa) +1 d�gito verificador
Exemplo: Inscri��o Estadual n�mero 101002135
� Despreza-se os tr�s primeiros d�gitos (101 nesse caso, indicando Porto Velho) e mais o d�gito verificador, trabalhando-se somente com os cinco d�gitos: 00213
� Atribui-se um peso a cada um dos cinco d�gitos da empresa seguindo a ordem abaixo:
0 0 2 1 3 6 5 4 3 2 pesos
� C�lculo: (6 x 0) + (5 x 0) + (4 x 2) + (3 x 1) + (2 x 3) = 17
� O resto da divis�o entre o resultado da opera��o acima 17 e o m�dulo 11 � igual a 6
� Calculamos 11, que � o m�dulo, menos 6, resultado obtido acima, d� igual a 5 que � o d�gito verificador da Inscri��o 10100213
� No caso da diferen�a calculada no item anterior fornecer como resultado 10 ou 11, subtrai-se 10 da mesma para obter o d�gito verificador.
*)
const
  vs = '65432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 4, ln-3);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := IntToStr(StrToInt(ver)-10);
    Num := Copy(x, 1, 3) + Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_AP(IE, UF: String): Boolean;
(*
Formato: 2 d�gitos constantes (03) + 6 d�gitos (empresa) +1 d�gito verificador
Exemplo: Inscri��o Estadual n�mero 030123459
� Verifica-se se os dois primeiros d�gitos s�o de fato 03
� Despreza-se o d�gito verificador, trabalhando-se somente com os demais 8 d�gitos: 03012345
� Define-se dois valores, p e d, de acordo com as seguintes faixas de Inscri��o Estadual:
De 03000001 a 03017000 => p = 5 e d = 0
De 03017001 a 03019022 => p = 9 e d = 1
De 03019023 em diante ===>p = 0 e d = 0

No exemplo acima, temos p = 5 e d = 0

Atribui-se um peso a cada um dos oito d�gitos da empresa seguindo a ordem abaixo:
0 3 0 1 2 3 4 5
9 8 7 6 5 4 3 2 pesos
C�lculo:
p + (9 x 0) + (8 x 3) + (7 x 0) + (6 x 1) +
+ (5 x 2) + (4 x 3) + (3 x 4) + (2 x 5) = 79
Calculamos 11, que � o m�dulo, menos 2, resultado obtido acima, e obtemos 9 que � o d�gito verificador da Inscri��o, que ser� 030123459, e nesse exemplo estar� correta;
No caso da diferen�a calculada no item anterior fornecer como resultado 10, o d�gito verificador ser� 0, e se o resultado for 11, ele ser� d.

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  p, d, n, i, Soma: Integer;
begin
  Soma := 0;
  p := 0;
  d := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if x[1]+x[2] <> '03' then
    begin
      Geral.MB_Erro('Os dois primeiros d�gitos devem ser "03" '+
      'para '+UF+'!' + sLineBreak +  'Valida_IE_AP()');
      Result := False;
      Exit;
    end;
    n := StrToInt(num);
    if ((n>=03000001) and (n<=03017000)) then
    begin
      p := 5;
      d := 0;
    end else
    if ((n>=03017001) and (n<=03019022)) then
    begin
      p := 9;
      d := 1;
    end else
    if ((n>=03019023) and (n<=03100000)) then
    begin
      p := 0;
      d := 0;
    end;
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    //////
    Soma := Soma + p;
    /////
    ver := IntToStr(11 -(Soma mod 11));
    n := StrToInt(ver);
    if n = 10 then ver := '0';
    if n = 11 then ver := IntToStr(d);
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_AC(IE, UF: String): Boolean;
(*
Tamanho da m�scara: 9 caracteres
M�scara de utiliza��o: 99.99.9999-9
Cr�ticas da Inscri��o Estadual: Somente n�meros
C�lculo do d�gito verificador:
M�dulo 11 Peso (P) = 98765432
Ex.: Inscri��o - 01.40.7423-0
0 1 4 0 7 4 2 3 P . . SOMA  =  99
DIVISOR  =  11
INTEIRO DO RESULTADO  =  9
RESTO  =  0

. . . . . . .  * 2 = 6
. . . . . . * .  3 = 6
. . . . . *  . . 4 = 16
. . . . *  . . . 5 = 35
. . . *  . . . . 6 = 0
. . *  . . . . . 7 = 28
. *  . . . . . . 8 = 8
*  . . . . . . . 9 = 0

Divide-se a soma por 11
Se resto < 2
D�gito = 0
Sen�o
D�gito = 11- resto
O terceiro e o quarto identificam o munic�pio e n�o podem ser = 0

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
  // >= 2000
  _Cgc2, DV1, DV2: String;
  _An, _Bn, _Cn, _Dn, _En, _Fn, _Gn, _Hn, _In, _Jn, _Kn,
  _S, _R, _N: Integer;

begin
  // v�lido at� 31/12/1999
  if Length(Geral.SoNumero_TT(IE)) < 10 then
  begin
    Soma := 0;
    x := Geral.SoNumero_TT(IE);
    if Valida_IE_SemErrLength(x, UF, ln, lv) then
    begin
      if StrToInt(x[3]+x[4]) = 0 then
      begin
        Geral.MB_Erro('Os digitos 3 e 4 n�o podem ser 0 para '+
        UF+' pois ele indica a cidade!' + sLineBreak +  'Valida_IE_AC()');
        Result := False;
        Exit;
      end;
      num := Copy(x, 1, ln);
      // M�dulo 11
      for i := Length(Num) downto 1 do
        Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
      ver := IntToStr(11 -(Soma mod 11));
      if StrToInt(ver) > 9 then ver := '0';
      Num := Num + ver;
      if x = Num then Result := True else Result := False;
    end else Result := False;
  end else
  begin
    // V�lido a partir de 01/01/2000
(*
// Rotina para valida��o do d�gito da
inscri��o estadual do Estado do Acre

// Separa cada n�mero da Inscri��o Estadual,
sem a mascara (9999999999999)

&An = substr(&Cgc2,1 ,1)
&Bn = substr(&Cgc2,2 ,1)
&Cn = substr(&Cgc2,3 ,1)
&Dn = substr(&Cgc2,4 ,1)
&En = substr(&Cgc2,5 ,1)
&Fn = substr(&Cgc2,6 ,1)
&Gn = substr(&Cgc2,7 ,1)
&Hn = substr(&Cgc2,8 ,1)
&In = substr(&Cgc2,9 ,1)
&Jn = substr(&Cgc2,10 ,1)
&Kn = substr(&Cgc2,11 ,1)
//
&S =      (&An*4) + (&Bn*3) + (&Cn*2) + (&Dn*9) + (&En*8) + (&Fn*7) + (&Gn*6) + (&Hn*5) + (&In*4) + (&Jn*3) + (&Kn*2)

//Pega o resto da divis�o por 11

&R = int(&S/11)
&S = &S - (&R * 11)

//Se for 1 ou 0, o 1� d�gito � 0; caso contr�rio o d�gito � 11 menos o resto da divis�o

if (&S = 1)  .OR. (&S = 0) then
   &N = 0
else
   &N = 11 - &S
endif

// Calcula o 2� d�gito
   &S =      (&An*5) + (&Bn*4) + (&Cn*3) + (&Dn*2) + (&En*9) + (&Fn*8) + (&Gn*7) + (&Hn*6) + (&In*5) + (&Jn*4) + (&Kn*3) + (&N*2)
   &R = int(&S/11)
   &S = &S - (&R * 11)

   //
   if (&S = 1) .OR. (&S = 0)
      &N = 0
   else
      &N = 11 - &S
   endif

   //
*)
    // Rotina para valida��o do d�gito da
    //inscri��o estadual do Estado do Acre

    // Separa cada n�mero da Inscri��o Estadual,
    //sem a mascara (9999999999999)
    _Cgc2 := Geral.SoNumero_TT(IE);
    _An := Geral.IMV(Copy(_Cgc2,1 ,1));
    _Bn := Geral.IMV(Copy(_Cgc2,2 ,1));
    _Cn := Geral.IMV(Copy(_Cgc2,3 ,1));
    _Dn := Geral.IMV(Copy(_Cgc2,4 ,1));
    _En := Geral.IMV(Copy(_Cgc2,5 ,1));
    _Fn := Geral.IMV(Copy(_Cgc2,6 ,1));
    _Gn := Geral.IMV(Copy(_Cgc2,7 ,1));
    _Hn := Geral.IMV(Copy(_Cgc2,8 ,1));
    _In := Geral.IMV(Copy(_Cgc2,9 ,1));
    _Jn := Geral.IMV(Copy(_Cgc2,10 ,1));
    _Kn := Geral.IMV(Copy(_Cgc2,11 ,1));
    //
    _S :=      (_An*4) + (_Bn*3) + (_Cn*2) + (_Dn*9) + (_En*8) + (_Fn*7) + (_Gn*6) + (_Hn*5) + (_In*4) + (_Jn*3) + (_Kn*2);

    //Pega o resto da divis�o por 11

    _R := _S div 11;
    _S := _S - (_R * 11);

    //Se for 1 ou 0, o 1� d�gito � 0; caso contr�rio o d�gito � 11 menos o resto da divis�o

    if (_S = 1)  or (_S = 0) then
      _N := 0
    else
      _N := 11 - _S;
    DV1 := Geral.FF0(_N);
    // Calcula o 2� d�gito
    _S :=      (_An*5) + (_Bn*4) + (_Cn*3) + (_Dn*2) + (_En*9) + (_Fn*8) + (_Gn*7) + (_Hn*6) + (_In*5) + (_Jn*4) + (_Kn*3) + (_N*2);
    _R := _S div 11;
    _S := _S - (_R * 11);

    //
    if (_S = 1) or (_S = 0) then
      _N := 0
    else
      _N := 11 - _S;
    DV2 := Geral.FF0(_N);

    //
    Result := Copy(_Cgc2, 12, 2) = DV1 + DV2;
  end;
end;

function TUnValidaIE.Valida_IE_MA(IE, UF: String): Boolean;
(*
Formato: num�rico
Tamanho: 9 d�gitos, sendo
os dois primeiros correspondentes ao c�digo do Estado(12);
os seis seguintes ao n�mero de ordem no cadastro
e o �ltimo d�gito ao d�gito de controle, calculado atrav�s do m�dulo 11.
Exemplo: calcular o d�gito verificador X do n�mero de inscri��o estadual 12000038X

Passo 1: Multiplica um peso, j� definido abaixo, pelo d�gito equivalente do n�mero de inscri��o:

N� de Inscri��o fornecido  1  2
 0  0  0  0  3  8  X
Peso p/Multiplicar  9  8  7  6  5  4  3  2  DV

Passo 2: Soma-se o resultado das multiplica��es acima e divide-se por 11.
Soma = (9x 1) + (8x 2) + (7x 0) + (6x 0) + (5x 0) + (4x 0) + (3x 3) + (2x 8) = 50

Passo 3: Se o resto da divis�o � 0 ou 1, o d�gito � 0. Caso contr�rio, o d�gito � 11 � resto da divis�o.
50 dividido por 11 = 4
Resto = 6
D�gito Verificador = 11 � 6 = 5

Inscri��o Estadual com d�gito verificador: 120000385

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if StrToInt(x[1]+x[2]) <> 12 then
    begin
      Geral.MB_Erro('Os 2 primeiro digitos devem ser 12, pois '+
      'else indicam o estado: '+UF+'!' + sLineBreak +  'Valida_IE_MA()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_PA(IE, UF: String): Boolean;
(*
Tamanho:9    Formato: Num�rico
Composi��o: 15-999999-5
15: N�mero Padr�o Par�
999999: Sequ�ncia
5: D�gito Verificador (M�dulo 11)

C�lculo do d�gito verificador M�dulo 11
1- Multiplica-se os n�meros, da direita para a esquerda, pelos seguintes valores: 2, 3, 4, 5, 6, 7, 8, 9;

2- Soma-se os resultados intermedi�rios e divide-se por 11;
3- Se o resto da divis�o � 0 ou 1, o d�gito � 0;
4- Se o resto da divis�o n�o � 0 nem 1, o d�gito � = 11 - resto da divis�o;
Exemplo: C�lculo para a Inscri��o 15999999-5
                    9 x 1 =    9
                    8 x 5 =    40
                    7 x 9 =    63
                    6 x 9 =    54
                    5 x 9 =    45
                    4 x 9 =    36
                    3 x 9 =    27
                    2 x 9 =   18
                                  292

292 : 11 = 26 (Resto 6)
11 - 6 = 5 (D�gito Verificador 5)
 Fechar
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if StrToInt(x[1]+x[2]) <> 15 then
    begin
      Geral.MB_Erro('Os 2 primeiro digitos devem ser 15, pois '+
      'else indicam o estado: '+UF+'!' + sLineBreak +  'Valida_IE_PA()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_RJ(IE, UF: String): Boolean;
(*
99.999.99-3
27 654 32

Se o resto da divis�o por 11 for menor ou igual � 1 (um),
Ent�o o d�gito verificador ser� = 0 (zero)
Sen�o, o d�gito ser� 11 (onze) menos o resto

*)
const
  vs = '2765432'; ln = 07; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_RS(IE, UF: String): Boolean;
(*
Formato: 3 d�gitos (munic�pio) + 6 d�gitos (empresa) +1 d�gito verificador
Exemplo: Inscri��o Estadual n�mero 224/3658792
� Verifica-se se os tr�s primeiros d�gitos (n�mero do munic�pio) est�o na faixa de 001 a 467
� Despreza-se o d�gito verificador, trabalhando-se somente com os demais 9 d�gitos: 224365879
� Atribui-se um peso a cada um dos nove d�gitos seguindo a ordem abaixo:
2 2 4 3 6 5 8 7 9
2 9 8 7 6 5 4 3 2 pesos
� C�lculo:
(2 x 2) + (9 x 2) + (8 x 4) + (7 x 3) + (6 x 6) + + (5 x 5) + (4 x 8) + (3 x 7) + (2 x 9) = 207

� O resto da divis�o entre o resultado da opera��o acima 207 e o m�dulo 11 � igual a 9
� Calculamos 11, que � o m�dulo, menos 9, resultado obtido acima, e obtemos 2 que � o d�gito verificador da Inscri��o, que ser� 2243658792, e nesse exemplo estar� correta.
� No caso da diferen�a calculada no item anterior fornecer como resultado 10 ou 11, o d�gito verificador ser� 0.
*)
const
  vs = '298765432'; ln = 09; lv= 1; Cidades = 467;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if StrToInt(x[1]+x[2]+x[3]) > 467 then
    begin
      Geral.MB_Erro('Os 3 primeiros digitos da inscri��o '+
      'estadual do RS devem ser menor que '+
      '467, pois eles indicam a cidade. Caso os d�gitos existam, comunique a '+
      'Dermatek!' + sLineBreak +  'Valida_IE_RS()');
      Result := False;
      Exit;
    end;
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_PE(IE, UF: String): Boolean;
(*
ANTIGO!
O n�mero de inscri��o estadual no Cadastro de Contribuintes do Estado de Pernambuco - CACEPE possui 14 algarismos (d�gitos), sendo 13 principais e 1 verificador. O c�lculo do d�gito verificador envolve o uso de "pesos" e da fun��o "m�dulo" com par�metro "11", conforme pode ser visto no exemplo abaixo:
Procedimentos de C�lculo (ex. para 18.1.001.0000004-9)
1. Multiplique cada algarismo principal pelo seu respectivo peso:
1 8 1 0 0 1 0 0 0 0 0 0 4 (algarismos)
x x x x x x x x x x x x x .
5 4 3 2 1 9 8 7 6 5 4 3 2 (pesos)
= = = = = = = = = = = = = .
5 32 3 0 0 9 0 0 0 0 0 0 8 (produtos)

2. Some os produtos obtidos para encontrar o total:
5 + 32 + 3 + 0 + 0 + 9 + 0 + 0 + 0 + 0 + 0 + 0 + 8 = 57 (total dos produtos)

3. Divida esse total pela constante "11" para obter o resto:
4. Subtraia esse resto da constante "11" para encontrar o d�gito verificador:11 - 2 = 9
Quando essa diferen�a for maior que "9", subtraia "10" unidades para obter o valor do d�gito verificador, uma vez que o mesmo deve ser sempre representado por apenas um algarismo.
Rotina de Verifica��o (exemplo em linguagem Pascal)
O par�metro "ie" deve ser do tipo String com 14 posi��es.
Function teste_dv_cacepe (ie: String): Boolean;
     Var a, b, c, d, e, f, g, h, i, j, k, l, m, n, total, dig: Integer;
     Begin
         a := StrToInt(Copy(ie,14,1));
         b := StrToInt(Copy(ie,13,1)) * 2;
         c := StrToInt(Copy(ie,12,1)) * 3;
         d := StrToInt(Copy(ie,11,1)) * 4;
         e := StrToInt(Copy(ie,10,1)) * 5;
         f := StrToInt(Copy(ie,9,1)) * 6;
         g := StrToInt(Copy(ie,8,1)) * 7;
         h := StrToInt(Copy(ie,7,1)) * 8;

 i := StrToInt(Copy(ie,6,1)) * 9;
         j := StrToInt(Copy(ie,5,1)) * 1;
         k := StrToInt(Copy(ie,4,1)) * 2;
         l := StrToInt(Copy(ie,3,1)) * 3;
         m := StrToInt(Copy(ie,2,1)) * 4;
         n := StrToInt(Copy(ie,1,1)) * 5;
         total := b + c + d + e + f + g + h + i + j + k + l + m + n;
         dig := 11 - (total Mod 11);
         If dig > 9 Then
              dig := dig - 10;
         If dig <> a Then
            teste_dv_cacepe := False
         Else
            teste_dv_cacepe := True;
    End;
////////////////////////////////////////////////////////////////////////////////
NOVO!
D�gito Verificador da Nova Inscri��o Estadual Pernambuco

Procedimentos de C�lculo (ex. para 0321418-40)

1. Multiplique cada algarismo principal pelo seu respectivo peso:
0   3   2   1   4   1   8   (algarismos)
x   x   x   x   x   x   x
8   7   6   5   4   3   2   (pesos)
=   =   =   =   =   =   =
0   21  12  5   16  3   16  (produtos)

2. Some os produtos obtidos para encontrar o total:
0 + 21 + 12 + 5 + 16 + 3 + 16 = 73 (total dos produtos)

3. Divida esse total pela constante "11" para obter o resto:
73/11 = 6, Resto = 7.

4. Se o resto da divis�o for igual a 1 ou 0, o primeiro d�gito ser� igual a zero.
   Sen�o, Subtraia esse resto da constante "11" para encontrar o d�gito verificador:
Como o resto � igual a 7, ent�o: 11 � 7 = 4 (primeiro d�gito de controle)

5. Multiplique cada algarismo principal pelo seu respectivo peso para obter o segundo
   d�gito de controle (o c�lculo leva em considera��o o primeiro d�gito, j� calculado)
0   3   2   1   4   1   8   4   (algarismos)
x   x   x   x   x   x   x   x
9   8   7   6   5   4   3   2   (pesos)
=   =   =   =   =   =   =   =
0   24  14  6   20  4   24  8   (produtos)

6. Some os produtos obtidos para encontrar o total:
0 + 24 + 14 + 6 + 20 + 4 + 24 + 8 = 100 (total dos produtos)

7. Divida esse total pela constante "11" para obter o resto:
100/11 = 9, Resto = 1.

8. Se o resto da divis�o for igual a 1 ou 0, o d�gito ser� igual � zero.
   Sen�o, Subtraia esse resto da constante "11" para encontrar o d�gito verificador:
Resto igual a 1. Portanto, segundo d�gito � igual � zero.
*)
const
  vs = '5432198765432';
  ln1 = 13; lv1 = 1; //Inscri��o com 14 digitos
  ln2 = 7; lv2 = 2;  //Inscri��o com 9 digitos
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Result := False;
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  case Length(x) of
    14:
    begin
      if Valida_IE_SemErrLength(x, UF, ln1, lv1) then
      begin
        (*if StrToInt(x[1]+x[2]) <> 18 then
        begin
          Application.MessageBox(PChar('Os 2 primeiro digitos devem ser 18, pois '+
          'else indicam o estado: '+UF+'!'), 'Erro', MB_OK+MB_ICONERROR);
          Result := False;
          Exit;
        end;*)
        num := Copy(x, 1, ln1);
        // M�dulo 11 -10
        for i := Length(Num) downto 1 do
          Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
        ver := IntToStr(11 -(Soma mod 11));
        if StrToInt(ver) > 9 then ver := IntToStr(StrToInt(ver)-10);
        Num := Num + ver;
        if x = Num then Result := True else Result := False;
      end else Result := False;
    end;
    else
    begin
      if Valida_IE_SemErrLength(x, UF, ln2, lv2) then
      begin
        num := Copy(x, 1, ln2);
        for i := Length(num) downto 1 do
          Soma := Soma + (StrToInt(Num[i]) * StrToInt(Copy(vs, 7)[i]));
        ver := IntToStr(Soma mod 11);
        if StrToInt(ver) in [0,1] then
          ver := FormatFloat('0', 0)
        else
          ver := IntToStr(11 - StrToInt(ver));
        num  := num + ver;
        Soma := 0;
        for i := Length(num) downto 1 do
          Soma := Soma + (StrToInt(Num[i]) * StrToInt(Copy(vs, 6)[i]));
        ver := IntToStr(Soma mod 11);
        if StrToInt(ver) in [0,1] then
          ver := FormatFloat('0', 0)
        else
          ver := IntToStr(11 - StrToInt(ver));
        num := num + ver;
        if num = Geral.SoNumero_TT(IE) then
          Result := True;
      end else Result := False;
    end;
  end;
end;

function TUnValidaIE.Valida_IE_PI(IE, UF: String): Boolean;
begin
  Result := Valida_IE_11(IE, UF);
end;

function TUnValidaIE.Valida_IE_PB(IE, UF: String): Boolean;
(*
Formato: 8 d�gitos + 1 d�gito verificador
Exemplo: CCICMS n�mero 16.004.017-5
� Atribui-se um peso a cada d�gito do CCICMS, excluindo o d�gito verificador,
seguindo a ordem abaixo:
1 6 0 0 4 0 1 7
9 8 7 6 5 4 3 2 pesos
� C�lculo:
(9 x 1) + (8 x 6) + (7 x 0) + (6 x 0) +
(5 x 4) + (4 x 0) + (3 x 1) + (2 x 7) = 94
� O resto da divis�o entre o resultado da opera��o acima 94 e o m�dulo 11� igual a 6
� Calculamos 11, que � o m�dulo, menos 6, resultado obtido acima, d� igual a 5
que � o d�gito verificador do CCICMS 16004017. Caso a diferen�a der 10 ou 11,
o d�gito � 0.
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    (*if StrToInt(x[1]+x[2]) <> 16 then
    begin
      Application.MessageBox(PChar('Os 2 primeiro digitos devem ser 15, pois '+
      'else indicam o estado: '+UF+'!'), 'Erro', MB_OK+MB_ICONERROR);
      Result := False;
      Exit;
    end;*)
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_AL(IE, UF: String): Boolean;
(*
Governo do Estado de Alagoas - Roteiro de Cr�tica da Inscri��o Estadual
FORMA��O: 24XNNNNND,  sendo:
24 � C�digo do Estado
X � Tipo de empresa (0-Normal, 3-Produtor Rural, 5-Substituta,
7- Micro-Empresa Ambulante, 8-Micro-Empresa)
NNNNN � N�mero da empresa
D � D�gito de verifica��o calculado pelo M�dulo11, p�sos 2 � 9 da direita
para a esquerda, exceto D
Exemplo: N�mero 2 4 0 0 0 0 0 4 D
Pesos 9 8 7 6 5 4 3 2
SOMA = (2 * 4) + (3 * 0) + (4 * 0) + (5 * 0) + (6 * 0) + (7 * 0) + (8 * 4) + (9 * 2) = 58
PRODUTO = 58 * 10 = 580
RESTO = 580 � INTEIRO(580 / 11)*11 = 580 � (52*11) = 8
D�GITO = 8
OBS: Caso RESTO seja igual a 10 o D�GITO ser� 0 (zero)
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  n, i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    if StrToInt(x[1]+x[2]) <> 24 then
    begin
      Geral.MB_Erro('Os 2 primeiro digitos devem ser 24, pois '+
      'else indicam o estado: '+UF+'!' + sLineBreak +  'Valida_IE_AL()');
      Result := False;
      Exit;
    end;
    n := StrToInt(x[3]);
    if not (n in [0,3,5,7,8]) then
    begin
      Geral.MB_Erro('O terceiro d�gito devem ser um dos '+
      'abaixo demonstrados:'+slineBreak+
      '0-Normal'+slineBreak+
      '3-Produtor Rural'+slineBreak+
      '5-Substituta'+slineBreak+
      '7- Micro-Empresa Ambulante'+slineBreak+
      '8-Micro-Empresa'+slineBreak+''+slineBreak+
      'para o estado ' + UF + sLineBreak + 'Valida_IE_AL()');
      Result := False;
      Exit;
    end;
    num := Copy(x, 1, ln);
    // M�dulo 11 pela metade
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    Soma := Soma * 10;
    Soma := Soma mod 11;
    if Soma > 9 then Soma := 0;
    ver := IntToStr(Soma);
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_RN(IE, UF: String): Boolean;
(*
Governo do Estado do Rio Grande do Norte - Roteiro de Cr�tica da Inscri��o Estadual
INSCRI��O  :  20.040.040-1
(Os primeiros dois d�gitos s�o sempre 20)
MEMORIA DE CALCULO:
2 * 9  =  18
0 * 8  =   0
0 * 7  =   0
4 * 6  =  24
0 * 5  =   0
0 * 4  =   0
4 * 3  =  12
0 * 2  =   0
        ----
SOMA  ->  54
MULTIPLICA A SOMA POR 10
MULTIPLICA��O = SOMA * 10 = 54 * 10 = 540
DIVIDE POR 11, OBTENDO O RESTO DA DIVISAO
DIVIS�O = MULTIPLICA��O / 11    =  540 / 11
RESTO (DIVIS�O) = 1
DIGITO VERIFICADOR = RESTO (DIVIS�O)
OBSERVACAO: CASO O DIGITO CALCULADO SEJA 10, CONSIDERAR DIGITO = 0
*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    //n := StrToInt(x[3]);
    num := Copy(x, 1, ln);
    // M�dulo 11 pela metade
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    Soma := Soma * 10;
    Soma := Soma mod 11;
    if Soma > 9 then Soma := 0;
    ver := IntToStr(Soma);
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_MS(IE, UF: String): Boolean;
(*
O m�todo adotado para se verificar a compatibilidade entre o n�mero principal
da inscri��o estadual, composto dos primeiros oito d�gitos,
e o seu d�gito verificador, representado pelo nono d�gito, segue as regras:
o primeiro d�gito ser� sempre representado pelo n�mero 2;
o segundo d�gito ser� sempre representado pelo n�mero 8
O m�todo a que se refere o artigo anterior � assim representado:
A = (1� x 9)+(2� x 8)+(3� x 7)+(4� x 6)+(5� x 5)+(6� x 4)+(7� x 3)+(8� x 2)
R = Resto de A/11
Se R = 0, ent�o D = 0
Se R > 0
T = 11 � R
Se T > 9, ent�o D = 0
Se T < 10, ent�o D = T
Em que D = D�gito Verificador.

*)
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, Num, ver: String;
  n, i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    Num := Copy(x, 1, ln);
    if StrToInt(Num[1]+Num[2]) <> 28 then
    begin
      Geral.MB_Erro('Os dois primeiros digitos devem ser 28 '+
      'para '+UF+' pois ele indica o estado!' + sLineBreak +  'Valida_IE_RN()');
      Result := False;
      Exit;
    end;
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(Soma mod 11);
    if ver <> '0' then
    begin
      n := 11-StrToInt(ver);
      if n > 9 then n := 0;
      ver := IntToStr(n);
    end;
    //if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_MT(IE, UF: String): Boolean;
(*
Formato: 10 d�gitos + 1 d�gito verificador
Exemplo: Inscri��o Estadual n�mero 0013000001-9

Atribui-se um peso a cada d�gito da Inscri��o Estadual seguindo a ordem abaixo:
0 0 1 3 0 0 0 0 0 1 � 9
3 2 9 8 7 6 5 4 3 2 pesos
C�lculo:
(3 x 0) + (2 x 0) + (9 x 1) + (8 x 3) + (7 x 0) + (6 x 0) +
(5 x 0) + (4 x 0) + (3 x 0) + (2 x 1) = 35
O resto da divis�o entre o resultado da opera��o acima 35 e o m�dulo 11 � igual 2
Pegamos 11, que � o m�dulo, menos 2, resultado obtido acima, d� igual a 9
que � o d�gito verificador da Inscri��o Estadual 0013000001-9.
Obs.: Se o resto da divis�o for (0) zero ou (1) um, ent�o D�gito Verificador � DV=0

Aten��o:
O C�lculo deve sempre considerar todos os d�gitos, inclusive zeros a esquerda,
pois todos s�o significativos.
Caso a inscri��o informada n�o possua 11 d�gitos, a mesma dever� ser completada
com zeros a esquerda para que o c�lculo seja efetuado corretamente,
como est� demonstrado no exemplo acima.
*)
const
  vs = '3298765432'; ln = 10; lv= 1;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else
  begin
    Geral.MB_Erro('Caso a inscri��o informada n�o possua '+
    '11 d�gitos, a mesma dever� ser completada com zeros a esquerda' +
    sLineBreak + 'Valida_IE_MT()');
    Result := False;
  end;
end;

function TUnValidaIE.Valida_IE_GO(IE, UF: String): Boolean;
const
  vs = '98765432'; ln = 08; lv= 1;
var
  x, num, ver: String;
  n, i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    if not (StrToInt(num[1]+num[2]) in ([10, 11, 15])) then
    begin
      Geral.MB_Erro('Os primeiros 2 d�gitos devem ser "10" ou '+
      '"11" ou "15" para o estado '+ UF+'.' + sLineBreak +  'Valida_IE_GO()');
      Result := False;
      Exit;
    end;
    if num = '11094402' then
    begin
      ver := Copy(x, 9, 1);
      if StrToInt(ver) in ([0, 1]) then Result := True else Result := False;
      Exit;
    end;
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs[i]));
    ver := IntToStr(Soma mod 11);
    if ver <> '0' then
    begin
      if ver = '1' then
      begin
        n := StrToInt(num);
        if (n>=10103105) and (n<=10119997) then ver := '1' else ver := '0';
      end else ver := IntToStr(11 - StrToInt(ver));
    end;
    //if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_DF(IE, UF: String): Boolean;
const
  vs1 = '43298765432'; vs2 = '543298765432'; ln = 11; lv= 2;
var
  x, num, ver: String;
  i, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    num := Copy(x, 1, ln);
    // M�dulo 11
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs1[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    ////////////////////////////////////////////////////////////////////////////
    Soma := 0;
    for i := Length(Num) downto 1 do
      Soma := Soma + (StrToInt(Num[i]) * StrToInt(vs2[i]));
    ver := IntToStr(11 -(Soma mod 11));
    if StrToInt(ver) > 9 then ver := '0';
    Num := Num + ver;
    ////////////////////////////////////////////////////////////////////////////
    if x = Num then Result := True else Result := False;
  end else Result := False;
end;

function TUnValidaIE.Valida_IE_MG(IE, UF: String): Boolean;
const
  vs1 = '121212121212'; vs2 = ''; ln = 11; lv= 2;
var
  x, num, ver, Txt: String;
  i, n, Soma: Integer;
begin
  Soma := 0;
  x := Geral.SoNumero_TT(IE);
  if Valida_IE_SemErrLength(x, UF, ln, lv) then
  begin
    //num := Copy(x, 1, ln);
    Num := Copy(x, 1, 3)+'0'+Copy(x, 4, 8);
    for i := Length(Num) downto 1 do
      Txt := Txt + IntToStr((StrToInt(Num[i]) * StrToInt(vs1[i])));
    for i := Length(Txt) downto 1 do
      Soma := Soma + StrToInt(Txt[i]);
    n := 0;
    if (Soma / 10) = (Soma div 10) then ver := '0' else
    begin
      n := (Soma div 10) * 10 + 10;
      n := n - Soma;
    end;
    num := Copy(x, 1, ln) + IntToStr(n);
    n := 1;
    Soma := 0;
    for i := Length(Num) downto 1 do
    begin
      n := n + 1;
      if n = 12 then n := 2;
      Soma := Soma  + (StrToInt(Num[i]) * n);
    end;
    n := Soma mod 11;
    if n < 2 then ver := '0' else ver := IntToStr(11-n);
    Num := Num + Ver;
    ////////////////////////////////////////////////////////////////////////////
    if x = Num then Result := True else Result := False;
  end else Result := False;
(*
Governo do Estado de Minas Gerais - Roteiro de Cr�tica da Inscri��o Estadual

FORMATO GERAL: A1A2A3B1B2B3B4B5B6C1C2D1D2
Onde:

A= C�digo do Munic�pio
B= N�mero da inscri��o
C= N�mero de ordem do estabelecimento
D= D�gitos de controle

CALCULO DE D1 (primeiro d�gito)Para se calcular primeiro d�gito procede-se
da seguinte forma:

1. Igualar as casas para o c�lculo, o que consiste em inserir o algarismo
zero "0" imediatamente ap�s o n�mero de c�digo do munic�pio, desprezando-se
os d�gitos de controle.

Exemplo: N�mero da inscri��o: 062 307 904 00 ? ?
N�mero a ser trabalhado: 062 "0" 307904 00 -- --

2. Multiplica-se os algarismos do n�mero a ser trabalhado, por 1 e por 2,
sucessivamente, da esquerda para a direita.

3. Exemplo:
0  6  2  0  3  0  7  9  0  4  0  0
x  x  x  x  x  x  x  x  x  x  x  x
1  2  1  2  1  2  1  2  1  2  1  2
========================
12  2  0  3  0  7 18  0  8  0  0

4. Soma-se os algarismos (n�o os produtos) do resultado obtido
Exemplo: 0+1+2+2+0+3+0+7+1+8+0+8+0+0=32

5. Subtrai-se o resultado da soma do �tem anteior, da primeira dezena
exata imediatamente superior:
Exemplo: "40" - 32= 8

6. O resultado encontrado corresponder� ao 1� d�gito de controle:
Exemplo: 062307904 00 "8" ?

C�LCULO DE D2 (segundo d�gito)
Para se calcular o 2� d�gito, procede-se da seguinte forma:

1. Utiliza-se o n�mero de inscri��o acrescido do primeiro d�gito encontrado (n�o h� que se igualar as casas com a inser��o do "0" zero)

2. Cada algarismo, da direita para a esquerda, � multiplicado, sucessivamente, por 2,3,4,5,6,7,8,9,10,11, quando se reinicia a s�rie 2,3,

Exemplo:
0  6  2  3  0  7  9  0  4  0  0  8
x  x  x  x  x  x  x  x  x  x  x  x
3  2 11 10  9  8  7  6  5  4  3 2
========================
0 12 22 30  0 56 63  0 20  0  0 16

3. Soma-se ent�o os produtos (n�o os algarismos)
Exemplo: 0+12+22+30+0+56+63+0+20+0+0+16 = 219

4. Divide-se ent�o o resultado por 11:
Exemplo:219 11
109 19
10

5. O resto encontrado na divis�o ser� subtra�do de onze, encontrando-se, com o resultado desta �ltima opera��o, o segundo d�gito da inscri��o
11 � "10" = 1
Por conseguinte � D2 = 1

Observe que sendo o resto da divis�o "1" ou "0" (zero), o d�gito igualmente ser� "0" (zero) sendo desnecess�ria a subtra��o acima demonstrada.

Pelos exemplos, o n� da Inscri��o trabalhada no presente demonstrativo �:
062.307.904/0081
*)

end;

function TUnValidaIE.Valida_IE_SP(IE, UF: String): Boolean;
var
  x, num, ver1, ver2: String;
  n, Soma: Integer;
begin
  Result := False;
  x := Geral.SoNumeroELetra_TT(IE);
  if (Uppercase(x[1]) = 'P')  then
  begin
    if Valida_IE_SemErrLength(Copy(x, 2, Length(x)-1), UF, 9, 3) then
    begin
      Num := Copy(x, 2, 8);
      Soma :=
             StrToInt(Num[1])*1 +
             StrToInt(Num[2])*3 +
             StrToInt(Num[3])*4 +
             StrToInt(Num[4])*5 +
             StrToInt(Num[5])*6 +
             StrToInt(Num[6])*7 +
             StrToInt(Num[7])*8 +
             StrToInt(Num[8])*10;
      n := Soma mod 11;
      if n > 9 then n := n - 10;
      if (IntToStr(n) = x[10]) then Result := True else Result := False;
    end;
  end else begin
    x := Geral.SoNumero_TT(IE);
    if Valida_IE_SemErrLength(x, UF, 10, 2) then
    begin
      Num := Copy(x, 1, 8);
      Soma :=
           StrToInt(Num[1])*1 +
           StrToInt(Num[2])*3 +
           StrToInt(Num[3])*4 +
           StrToInt(Num[4])*5 +
           StrToInt(Num[5])*6 +
           StrToInt(Num[6])*7 +
           StrToInt(Num[7])*8 +
           StrToInt(Num[8])*10;
      n := Soma mod 11;
      if n > 9 then n := n - 10;
      ver1 := IntToStr(n);
      //////////////////////////////////////////////////////////////////////////
      Num := Copy(x, 1, 11);
      Soma :=
             StrToInt(Num[01])*03 +
             StrToInt(Num[02])*02 +
             StrToInt(Num[03])*10 +
             StrToInt(Num[04])*09 +
             StrToInt(Num[05])*08 +
             StrToInt(Num[06])*07 +
             StrToInt(Num[07])*06 +
             StrToInt(Num[08])*05 +
             StrToInt(Num[09])*04 +
             StrToInt(Num[10])*03 +
             StrToInt(Num[11])*02;
      n := Soma mod 11;
      if n > 9 then n := n - 10;
      ver2 := IntToStr(n);
      if ((ver1 = x[9]) and (ver2 = x[12])) then Result := True else Result := False;
    end else Result := False;
  end;
(*Governo do Estado de S�o Paulo- Roteiro de Cr�tica da Inscri��o Estadual
Regra Geral: Em todas as contagens de posi��es de caracteres e c�lculos efetuados dever�o ser ignorados caracteres diferentes da letra "P" e dos algarismos 0, 1, 2, 3, 4, 5, 6, ,7, 8 e 9.
I � Industriais e comerciantes (exceto produtores rurais a eles n�o equiparados):
> Formato: 12 d�gitos sendo que o 9� e o 12� s�o d�gitos verificadores
> Exemplo: Inscri��o Estadual 110.042.490.114
a) C�lculo do 1� d�gito verificador, ou seja, do 9� d�gito contado a partir da esquerda:
- Atribui-se um peso a cada d�gito da Inscri��o Estadual seguindo a ordem utilizada abaixo:
1 1 0 0 4 2 4 9 0 1 1 4      PESOS: 1 3 4 5 6 7 8 10
(1 x 1) + (1 x 3) + (0 x 4) + (0 x 5) + (4 x 6) + (2 x 7) + (4 x 8) + (9 x 10) = 1 + 3 + 0 + 0 + 24 + 14 + 32 + 90 = 164
- O d�gito verificador ser� o algarismo mais � direita do resto da divis�o do resultado obtido acima (164) por 11 � 164/11 = 14 com resto = 10, ent�o o 1� d�gito verificador (9� posi��o) � 0

b) C�lculo do 2� d�gito verificador, ou seja, do 12� d�gito contado a partir da esquerda:
- Atribui-se um peso a cada d�gito da Inscri��o Estadual seguindo a ordem utilizada abaixo:
1  1  0  0  4  2  4  9   0  1  1  4      PESOS: 3  2 10 9  8  7  6  5  4  3   2
(1 x 3) + (1 x 2) + (0 x 10) + (0 x 9) + (4 x 8) + (2 x 7) + (4 x 6) + (9 x 5) + (0 x 4) + (1 x 3) + (1 x 2) = 3 + 2 + 0 + 0 + 32 + 14 + 24 + 45 + 0 + 3 + 2 = 125
- O d�gito verificador ser� o algarismo mais � direita do resto da divis�o do resultado obtido acima (118) por 11 � 125/11 = 11 com resto = 4, ent�o o 2� d�gito verificador (12� posi��o) � 4
Fica ent�o formada a inscri��o 110.042.490.114
II � Inscri��o estadual de Produtor Rural (N�o equiparado a industrial ou comerciante, cujas inscri��es obedecem a Regra descrita no item anterior):
> Formato: - P0MMMSSSSD000
- 13 caracteres dos quais o 10� caracter contado a partir da esquerda ("D") � o d�gito verificador
- Inicia sempre com "P" e apresenta a sequ�ncia 0MMMSSSSD000, onde:
0MMMSSSS-algarismos que ser�o utilizados no c�lculo do d�gito verificador "D"
"D" - D�gito verificador que consiste os 8 (oito)d�gitos imediatamente anteriores
000 - 3 (tr�s) d�gitos que comp�em o n� de inscri��o mas n�o utilizados no c�lculo do d�gito verificador
> Exemplo: Inscri��o Estadual P-01100424.3/002

C�lculo do d�gito verificador, ou seja, do 10� d�gito contado a partir da esquerda, incluindo-se na contagem a letra "P":
- Atribui-se um peso a cada d�gito da Inscri��o Estadual a partir do primeiro 0 (zero) seguindo a ordem utilizada abaixo:
0 1 1 0 0 4 2 4 3      PESOS: 1 3 4 5 6 7 8 10
(0x 1) + (1 x 3) + (1 x 4) + (0 x 5) + (0 x 6) + (4 x 7) + (2 x 8) + (4 x 10) = 0 + 3 + 4 + 0 + 0 + 28 + 16 + 40 = 91
- O d�gito verificador ser� o algarismo mais � direita do resto da divis�o do resultado obtido acima (91) por 11
> 91/11 = 8 com resto = 3, ent�o o d�gito verificador (10� posi��o) � 3
Fica ent�o formada a inscri��o P-011000424.3/002*)

end;


end.
