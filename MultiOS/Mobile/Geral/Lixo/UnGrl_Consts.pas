unit UnGrl_Consts;

interface

const
  CO_WEB_URL_REST_SERVER = 'http://www.dermatek.net.br/dmkREST.php';
  CO_WEB_REQ_CONTENTTYPE_ENCODED = 'application/x-www-form-urlencoded';
  DELETE_FROM = 'DEL'+'ETE FROM ';

  // Lista dos Aplicativos
  CO_APP_ID_BUGSTROL   = 24;
  CO_APP_ID_BUGSMOON   = 33;
  // Fim Lista Aplicativos

  CO_DIR_RAIZ_DMK = 'C:\Dermatek';
  CO_SQLBATCH_DELIM = '|;|';

  // SQL Forms
  CO_STYLE_LOOKUP_DSA_PLUS = 'addbutton';
  CO_STYLE_LOOKUP_DSA_EDIT = 'editbutton';
  CO_STYLE_LOOKUP_DSA_OLHO = 'pausebutton';
  //
  CO_STYLE_LOOKUP_DSA_NDEF = 'roundbutton';
  // Tabbed forms
  CO_FV_MAIN = 'FVMain';

  //  Geral Apps
  CO_SecureStr_001 = '9qwbdeyte4';
  CO_MASTER = '31415926536';
  CO_USERSP001 = 'wkljweryhvbirt'; // anterior
  CO_USERSPNOW = 'wkljweryhvbirt'; // atual
  CO_USERSPSEQ = 1;
  VAR_DERMA_AD: byte = 1;

implementation

end.
