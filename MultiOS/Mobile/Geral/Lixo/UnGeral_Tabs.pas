unit UnGrl_Tabs;
{ Colocar no MyListas:

Uses PerfJan_Tabs;


//
function TMyListas.CriaListaTabelas(:
      UnGrl_Tabs.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnGrl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnGrl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnGrl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnGrl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnGrl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnGrl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses System.Classes, System.SysUtils,
  Generics.Collections,
  UnMyLinguas, UnDmkEnums;

type
  TUnGrl_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CarregaListaFRCampos(Tabela: String; FRCampos: TCampos; FLCampos:
              TList<TCampos>; var TemControle: TTemControle): Boolean;
    function  CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function  CompletaListaFRCampos(Tabela: String; FRCampos:
              TCampos; FLCampos: TList<TCampos>): Boolean;
    function  CarregaListaFRIndices(TabelaBase: String; FRIndices:
              TIndices; FLIndices: TList<TIndices>): Boolean;
  end;

var
  Grl_Tabs: TUnGrl_Tabs;

implementation

//uses UMySQLModule;


{ TUnGrl_Tabs }

function TUnGrl_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Vers�o do aplicativo';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Usuarios') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo'; //UsrCod retornado no login REST
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UserName';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'PassWord';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Entidade';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Token';  //UsrAPI retornado no login REST
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnGrl_Tabs.CarregaListaFRIndices(TabelaBase: String;
  FRIndices: TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('Controle') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('Usuarios') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'UserName';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'PassWord';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnGrl_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('Controle'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('Usuarios'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnGrl_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
  except
    raise;
    Result := False;
  end;
end;

end.
