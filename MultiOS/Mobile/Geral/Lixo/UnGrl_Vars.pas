unit UnGrl_Vars;

interface

uses UnDmkEnums,
  FireDAC.Comp.Client;

type
  TProcedure = procedure;
  TNotifyEvent = procedure(Sender: TObject) of object;
  TUnGrl_Vars = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AddTODO(Acao: String);
  end;
var
  Grl_Vars: TUnGrl_Vars;
  //
  // Q U A L Q U E R   A P P
  VAR_CHAMOU_APP_EXTERNO: Boolean = False;

  // D B
  VAR_APP_DB_TYPE: TDmkDBMSs;
  VAR_WEB_USER_ID: Integer = 0;
  VAR_WEB_TOKEN_DMK: String = '';
  //VAR_APP_DB_NAME_ONLY,
  TMeuDB: String;
  VAR_APP_DB_NAME_E_EXT: String;
  VAR_APP_DB_FULLPATH: String;
  VAR_RDBMS_DriverID: String;
  VAR_RDBMS_ODBCDriver: String;
  VAR_RDBMS_SERVER: String;
  VAR_RDBMS_PORT: String;
  VAR_RDBMS_PWD: String;
  VAR_RDBMS_USER: String;
  VAR_LOGIN: String;
  VAR_SENHA: String;
  VAR_SQLITE_DB_LOCAL_SERVER: TFDConnection;
  VAR_SQLITE_DB_LOCAL_USER: TFDConnection;
  VAR_SQLITE_DB_WEB_SERVER: TFDConnection;
  VAR_SQLITE_DB_WEB_USER: TFDConnection;
  VAR_PORQUE_VERIFICA: String;
  VAR_ForcaBigIntNeg: Boolean = False;
  // From DB
  VAR_LIB_FILIAIS: String;
  VAR_LIB_EMPRESAS: String;
  VAR_LIB_EMPRESA_SEL: Integer;
  VAR_BOSS: String;
  VAR_SENHATXT: String;
  VAR_NOMEEMPRESA: String;
  VAR_CNPJEMPRESA: String;
  VAR_BOSSLOGIN: String;
  VAR_BOSSSENHA: String;
  VAR_ADMIN: String;
  VAR_FUNCILOGIN: Integer;
  VAR_SKINCONTA: Integer;
  VAR_TODO_IMPLEMENT: String = 'EnterToTab' + sLineBreak;
  //


  // F O R M S
  VAR_LIBERA_TODOS_FORMS: Boolean;
  VAR_FORM_ID: String;
  VAR_APP_MAIN_STYLE: String;
  // acesso a forms
  FM_MASTER : String[1];
  //
  // G E O   L O C A L I Z A � � O
  VAR_LOC_LATI_LONGI: Boolean = False;
  VAR_LATITUDE: Double = 0.0000000000;
  VAR_LONGITUDE: Double = 0.0000000000;
  VAR_LOC_DATA_HORA: TDateTime = 0;
  //
  // M I S C E L A N E A
  VAR_SOMAIUSCULAS: Boolean;
  VAR_DEVICE_ID: String = '';
  //
implementation

{ TUnGrl_Vars }

procedure TUnGrl_Vars.AddTODO(Acao: String);
begin
  VAR_TODO_IMPLEMENT := VAR_TODO_IMPLEMENT + Acao + sLineBreak;
end;

end.


