unit UnWebReq;

interface

uses System.Classes, System.SysUtils,
 fmx.Forms,
 Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc, Xml.adomxmldom,
 IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
 UnGrl_Consts, UnGeral;

type
  TUnWebReq = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  SQLArrPOST_TxtStr(Form: TForm; Metodo: String;
              Parametros: array of String): String;
  end;

var
  WebReq: TUnWebReq;

implementation

{ TUnWebReq }

function TUnWebReq.SQLArrPOST_TxtStr(Form: TForm; Metodo: String;
  Parametros: array of String): String;
var
  Vars: TStringList;
  Resposta: TStringStream;
  IdHTTP: TIdHTTP;
  I: Integer;
  //Texto: String;
begin
  Result := '';
  Vars     := TStringList.Create;
  Resposta := TStringStream.Create('');
  IdHTTP := TIdHTTP.Create(Form);
  //
  //Texto := '';
  try
    Vars.Add('method=' + Metodo);
    for I := System.Low(Parametros) to System.High(Parametros) do
    begin
      if Parametros[I] <> '' then
      begin
        Vars.Add(Parametros[I]);
        //Texto := Texto + '&' + Parametros[I];
      end;
    end;
    //Geral.MB_Aviso(Texto);
    IdHTTP.Request.ContentType := CO_WEB_REQ_CONTENTTYPE_ENCODED;
    IdHTTP.Post(CO_WEB_URL_REST_SERVER, Vars, Resposta);
    //
    Result := Resposta.DataString;
    //Geral.MB_Info(Result);
  finally
    Vars.Free;
    Resposta.Free;
    IdHTTP.Free;
  end;
end;

end.
