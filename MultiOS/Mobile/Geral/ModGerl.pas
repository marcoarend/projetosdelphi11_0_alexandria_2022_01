unit ModGerl;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  UnGrl_Vars, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  UnGeral;

type
  TDmModGerl = class(TForm)
    QvUpdY: TFDQuery;
    QvLivreY: TFDQuery;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ObtemAgora(): TDateTime;
  end;

var
  DmModGerl: TDmModGerl;

implementation

{$R *.fmx}

{ TFmModGerl }

uses UnDmkRDBMS, ModApp;

procedure TDmModGerl.FormCreate(Sender: TObject);
begin
  // Evitar uso. Criado ao iniciar o app.
  // Provisório
  VAR_SOMAIUSCULAS := True;
  // FIM Provisório
end;

function TDmModGerl.ObtemAgora(): TDateTime;
///////////////////////////////////
// ver também > ObtemDataHora    //
///////////////////////////////////
{
var
  Hoje: TDateTime;
  Agora: TTime;
}
begin
  Result := Now();
{
  DmkRDBMs.ReopenDataset(Self, QrAgora);
  Hoje  := EncodeDate(QrAgoraAno.Value, QrAgoraMes.Value, QrAgoraDia.Value);
  Agora := EncodeTime(QrAgoraHora.Value, QrAgoraMINUTO.Value, QrAgoraSEGUNDO.Value, 0);
  Result := Hoje + Agora;
}
end;

end.
