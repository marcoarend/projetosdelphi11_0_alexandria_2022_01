unit UnMyObjects;

interface

uses  System.Classes, System.TypInfo, System.UITypes, System.SysUtils,
  FMX.StdCtrls, FMX.Forms, FMX.ActnList, FMX.Controls, FMX.TabControl,
  FMX.Types, FMX.Ani, FMX.Layouts, FMX.Effects, FMX.Filter, FMX.Grid,
  System.Variants, System.UIConsts, FMX.Styles,
  FMX.ListView.Types, FMX.ListView,
  {$IFNDEF DmkMobile} dmkCompoStore, dmkCompVars, dmkTabItem, dmkFormVars,
  UnDmkTypInfo, dmkSpeedButton, dmkSQLState, UnDmkCompoConsts, UnDmkPF, {$ENDIF}
  UnGeral, UnDmkEnums, UnGrl_Vars, UnGrl_Consts, UnFMX_Vars;

type
  //TFormClass = class of TForm;
  TFormClass = class of TForm;
  HackFmxObject = class(TFmxObject);
  HackTabControl = class(TTabControl);

  TUnMyObjects = class(TObject)
  private
    { Private declarations }
    {$IFNDEF DmkMobile}
    function  _FIX_Compo(MyControl: TControl; Children: Boolean): Integer;
    {$ENDIF}
    function  FormTDINovaAba(TabControl: TTabControl): TTabItem;
    function  FormTDINovaAba2(const TabControl: TTabControl; var TabItem:
              TTabItem; var Layout: Tlayout): Boolean;
  public
    { Public declarations }
    // T L A B E L  (+ ProgressBar)
    procedure IncProgress(PB: TProgressBar);
    procedure Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
    function  Informa2(Label1, Label2: TLabel; Aguarde: Boolean; Texto: String;
              IsThread: Boolean = False): Boolean;
    // T F O R M
{   Erro de Memory Leak com DisposeOf no Android!!!
    function  ShowModal_Var(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo): Variant;
}
    function  CriaFm(InstanceClass: TComponentClass; var Reference): TForm;
    function  CriaFm_SM_NaoUsar(InstanceClass: TComponentClass; var Reference): TForm;
    function  CriaFm_SM(InstanceClass: TComponentClass): TForm;
    {$IFNDEF DmkMobile}
    function  Definido_DmkCNPsq1(InfoNot: Boolean = True): Boolean;
    procedure FechaJanelaTabbed(TabControl: TTabControl);
    function  FormTDICria3(Classe: TFormClass;
              FormVars: String; InOwner: TFmxObject; LayoutOwner: TLayout;
              AnimateDrawer: TFloatAnimation; SbPesq: TSpeedButton;
              Unique: Boolean = False): TForm;
    function  FormTDINovaAba3(const TabControl: TTabControl; var TabItem:
              TdmkTabItem; var Layout: Tlayout): Boolean;
    procedure FormTDIFecha(Form: TForm; AbaQueChamou: TComponent);
    function  FixGr(Form: TForm; Grupo: Integer): Integer;
    function  FixLy(Form: TForm; Layout: TLayout): Integer;
    // L I S T V I E W
    function SelListView(Titulo: String; Itens: array of String;
             Default: Integer): Integer;
    // S Q L
    function  DefineSQLTypeView(SQLType: TSQLType; SbOlho: TComponent):
              Boolean;
    {$ENDIF}
    function  FormTDICria(Classe: TFormClass; InOwner: TFmxObject;
              AnimateDrawer: TFloatAnimation; Unique: Boolean = False): TForm;
    function  FormTDICria2(Classe: TFormClass; InOwner: TFmxObject;
              LayoutOwner: TLayout;
              AnimateDrawer: TFloatAnimation; Unique: Boolean = False): TForm;
    function  FormShow(InstanceClass: TComponentClass; var Reference): TForm;
    //Principal
    //
    // R A D I O G R O U P
    function  RGItemIndex(RadioButons: array of TRadioButton): Integer;
    //
    // L I S T B O X I T E M
    function  PlatformListBoxItemFator(): Double;
    //
    // S T R I N G G R I D
    function  ExcluiLinhaStringGrid(Grade: TStringGrid): Boolean;
    // S T Y L E B O O K
    {$IFNDEF DmkMobile}
    procedure AppStyle(Form: TForm; Define, Carrega: Boolean);
    {$ENDIF}
    procedure ReAplyStyle(Form: TForm; StyleBook: TStyleBook);

  end;
var
  MyObjects: TUnMyObjects;

implementation

{ TUnMyObjects }

{$IFNDEF DmkMobile}
uses SelListView, UnAppMain;
{$ENDIF}

{$IFNDEF DmkMobile}
procedure TUnMyObjects.AppStyle(Form: TForm; Define, Carrega: Boolean);
begin
  if Define then
  begin
    VAR_APP_MAIN_STYLE := DmkPF.ReadAppKeyCU(Application.Title, ['StyleBook'],
      'FileName', ktString, '');
  end;
  if Carrega then
  begin
    if VAR_APP_MAIN_STYLE <> '' then
      if FileExists(VAR_APP_MAIN_STYLE) then
        if VAR_FORM_STYLEBOOK <> nil then
          VAR_FORM_STYLEBOOK.FileName := VAR_APP_MAIN_STYLE;
    if VAR_FORM_STYLEBOOK.Style <> nil then
    begin
      //TStyleManager.SetStyle(TStyleManager.LoadFromFile(VAR_APP_MAIN_STYLE));
      TStyleManager.SetStyle(VAR_FORM_STYLEBOOK.Style);
      ReAplyStyle(Form, VAR_FORM_STYLEBOOK);
    end;
  end;
end;
{$ENDIF}

function TUnMyObjects.CriaFm(InstanceClass: TComponentClass; var Reference): TForm;
begin
  Application.CreateForm(InstanceClass, Reference);
  Result := TForm(Reference);
  Result.StyleBook := VAR_FORM_STYLEBOOK;
end;

function TUnMyObjects.CriaFm_SM_NaoUsar(InstanceClass: TComponentClass; var Reference): TForm;
var
  Instance: TComponent;
  Form: String;
  //MyCursor: TCursor;
  //Campo: String;
begin
  Result := nil;
  Form := InstanceClass.ClassName;
  //MyCursor := Screen.Cursor;
  //Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(nil);
      //
      Result := TForm(Reference);
    except
      TComponent(Reference) := nil;
      raise;
    end;
    with Result do
    begin
      StyleBook := VAR_FORM_STYLEBOOK;
      //BorderStyle := bsNone;
      //Parent := Painel;
      //Show;
      //Align := alClient;
    end;
    //Screen.Cursor := MyCursor;
  finally
    //Screen.Cursor := MyCursor;
  end;
{
begin
  Reference := InstanceClass.Create(nil);
  TForm(Reference).StyleBook := VAR_FORM_STYLEBOOK;
  Result := TForm(Reference);
}
end;

{$IFNDEF DmkMobile}
function TUnMyObjects.DefineSQLTypeView(SQLType: TSQLType; SbOlho: TComponent):
  Boolean;
begin
  Result := False;
  if SbOlho <> nil then
  begin
    if SbOlho is TdmkSQLState then
    begin
      TdmkSQLState(SbOlho).SQLType := SQLType;
      Result := True;
    end else
      Geral.MB_Erro('"SQLTypeView" de classe n�o esperada!');
  end else
    Geral.MB_Erro('"SQLTypeView" n�o definido!');
end;
{$ENDIF}

{$IFNDEF DmkMobile}
function TUnMyObjects.Definido_DmkCNPsq1(InfoNot: Boolean): Boolean;
begin
  Result := VAR_FmDmkCNPsq1 <> nil;
  if not Result and InfoNot then
    Geral.MB_Erro('"VAR_FmDmkCNPsq1" n�o definido!');
end;
{$ENDIF}

function TUnMyObjects.ExcluiLinhaStringGrid(Grade: TStringGrid): Boolean;
var
  X, I, J, Line: Integer;
begin
  Result := False;
  if Geral.MB_Pergunta('Confirma a exclus�o de toda linha ' + Geral.FF0(
  Grade.Selected + 1) + '?') = mrYes then
  begin
    X := 1;
    Line := Grade.Selected;
    for I := Line to Grade.RowCount -1  do
    begin
      for J := 0 to Grade.ColumnCount - X do
      begin
        Grade.Cells[J, I] := Grade.Cells[j, i+1];
      end;
    end;
    for i := 1 to Grade.ColumnCount do Grade.Cells[I, Grade.RowCount - 1] := '';
    Line := Grade.RowCount - x;
    if Line < 2 then
    begin
      //for i := 1 to Grade.ColCount do Grade.Cells[i, Grade.RowCount-1] := '';
    end else Grade.RowCount := Line;
    Result := True;
  end;
end;

{$IFNDEF DmkMobile}
procedure TUnMyObjects.FechaJanelaTabbed(TabControl: TTabControl);
var
  Index, I: Integer;
  TabItem: TTabItem;
  Layout: TLayout;
  Form: TForm;
  CompoStore: TdmkCompoStore;
  AbaQueChamou: TComponent;
begin
  Index := TabControl.TabIndex;
  if Index < 1 then
    Exit;
  TabItem := TabControl.ActiveTab;
  Layout := nil;
  Form   := nil;
  AbaQueChamou := nil;
  for I := 0 to TabItem.ComponentCount - 1 do
  begin
    if TComponent(TabItem.Components[I]) is TLayout then
    begin
      Layout := TLayout(TabItem.Components[I]);
      Break;
    end;
  end;
  if Layout <> nil then
  begin
    for I := 0 to Layout.ComponentCount - 1 do
    begin
      if TComponent(Layout.Components[I]) is TForm then
      begin
        Form := TForm(Layout.Components[I]);
        Break;
      end;
    end;
  end;
  if Form <> nil then
  begin
    for I := 0 to Form.ComponentCount - 1 do
    begin
      if TComponent(Form.Components[I]) is TdmkCompoStore then
      begin
        CompoStore := TdmkCompoStore(Form.Components[I]);
        if LowerCase(CompoStore.Name) = LowerCase('CSTabChamou') then
        begin
          AbaQueChamou := CompoStore.Component;
          if not (AbaQueChamou is TTabItem) then
            AbaQueChamou := nil
          else
            Break;
        end;
      end;
    end;
    Form.Close;
    Form.Destroy;
    //Form.Free; // gera erro
    Form := nil;
  end;
  if Form = nil then
    TabItem.Free;
    if AbaQueChamou <> nil then
      TabControl.TabIndex := TTabItem(AbaQueChamou).Index
    else
      TabControl.TabIndex := TabControl.TabCount - 1;
  AppMain.ReAplyStyle();
end;
{$ENDIF}

{$IFNDEF DmkMobile}
function TUnMyObjects.FixGr(Form: TForm; Grupo: Integer): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Form.ComponentCount - 1 do
  begin
    if Form.Components[I] is TControl then
      Result := Result + _FIX_Compo(TControl(Form.Components[I]), False);
  end;
end;
{$ENDIF}

{$IFNDEF DmkMobile}
function TUnMyObjects.FixLy(Form: TForm; Layout: TLayout): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Layout.ChildrenCount - 1 do
  begin
    if Layout.Children[I] is TControl then
      Result := Result + _FIX_Compo(TControl(Layout.Children[I]), True);
  end;
end;
{$ENDIF}

function TUnMyObjects.FormShow(InstanceClass: TComponentClass;
  var Reference): TForm;
begin
  Result := CriaFm(InstanceClass, Reference);
  Result.ShowModal;
end;

function TUnMyObjects.FormTDICria(Classe: TFormClass; InOwner: TFmxObject;
  AnimateDrawer: TFloatAnimation; Unique: Boolean): TForm;

  procedure InfoErro(Codigo: Integer);
  begin
    Geral.MB_Erro('"InOwner" n�o implementado na function "FormTDICria".' +
    sLineBreak + 'Localizador na function: ' + Geral.FFN(Codigo, 3) +
    sLineBreak + 'Avise a DERMATEK');
  end;

var
  Form: TForm;
  Achou: Boolean;
  I: Integer;
  Ini, Fim: Single;
  ///
  TabControl: TTabControl;
  //PageControl: TPageControl;
  TabItem: TTabItem;
  //TabSheet: TTabSheet;
begin
  Form        := nil;
  Achou       := False;
  I           := 0;
  TabControl := nil;
  //PageControl := nil;
  //
  if InOwner is TTabControl then
  //if InOwner is TPageControl then
    TabControl := TTabControl(InOwner)
  //  PageControl := TPageControl(InOwner)
  else
  if InOwner is TForm then
  begin
    //if TForm(InOwner).Owner is TTabSheet then
    if TForm(InOwner).Owner is TTabItem then
    begin
      TabControl := TTabControl(TTabItem(TForm(InOwner).Owner).Owner);
      //PageControl := TPageControl(TTabSheet(TForm(InOwner).Owner).Owner);
      if not (TabControl is TTabControl) then
      //if not (PageControl is TPageControl) then
        InfoErro(3);
    end else InfoErro(2);
  end else
  if InOwner is TPanel then
  begin
    Result := nil;
    Exit;
  end else InfoErro(1);
  ///
  try
    if Unique then
    begin
      //for I := 0 to PageControl.PageCount - 1 do
      for I := 0 to TabControl.TabCount - 1 do
      begin
        //Form := TForm(Pagecontrol.Pages[I].Components[0]);
        Form := TForm(TabControl.Tabs[I].Components[0]);
        //
        if Form is Classe then
        begin
          Achou := True;
          Break;
        end;
      end;
    end;
    if Achou then
    begin
      //PageControl.ActivePage      := PageControl.Pages[I];
      TabControl.ActiveTab          := TabControl.Tabs[I];
      //PageControl.ActivePageIndex := PageControl.Pages[I].PageIndex;
      TabControl.TabIndex           := TabControl.Tabs[I].Index;
    end else
    begin
      //TabSheet := FormTDINovaAba(PageControl);
      TabItem := FormTDINovaAba(TabControl);
      //TabItem := TabControl.Tabs[0];
      //Form := TFormClass(Classe).Create(TabSheet);
      Form := TFormClass(Classe).Create(TabItem);
      Form.WindowState := TWindowState.wsNormal;
      with Form do
      begin

        //configura o formulario
        FullScreen := True;   // ???
        StyleLookup := 'PanelStyle';

        //Align       := alClient;
        WindowState := TWindowState.wsMaximized;

        BorderStyle := TFmxFormBorderStyle.bsNone;
        Parent      := TabItem;

        {O evento onActive do TForm n�o � executado pq o que se torna ativo
         na verdade � o TTabSheet onde o formulario foi criado. Sendo assim qualquer
         coisa escrita no onActive do formul�rio n�o ser� executado.
         Para contornar esta situa��o nos passamos o evento onActive do Form para o
         evento onEnter do TTabSheet. E assim simulamos com seguran�a o evento onActive}
        //PageControl.ActivePage.OnEnter := OnActivate;
        TabItem.OnEnter := OnActivate;

        //PageControl.ActivePage.Caption := Caption;//transfere o caption do form para o caption da aba
        //PageControl.ActivePage.Caption := Copy(Caption, 1, 13);//transfere o caption do form para o caption da aba
        TabItem.Text := Copy(Caption, 1, 13);//transfere o caption do form para o caption da aba

        {
        PageControl.Enabled := True;
        TWinControl(Sender).Enabled := HabilitaSender;
        }
        Show;//mostra o formul�rio

        {Embora comigo nunca tenha acontecido, algumas pessoas me avisaram sobre
         uma exce��o de focus que a linha abaixo gera em casos bem especificos.
         Eu deixo descomentado e se voc� prefefir pode comentar a linha abaixo.}
        try
          //Perform(WM_NEXTDLGCTL, 0, 0);//muda o foco para o primeiro controle do formulario
          //HackFmxObject(Form).SelectFirst;
          //HackWinControl(Form).SelectNext(HackWinControl(Form).ActiveControl, True, True);
        except
        end;
      end;
    end;
    if AnimateDrawer <> nil then
    begin
      if AnimateDrawer.StopValue > 0 then
      begin
        Ini := AnimateDrawer.StopValue;
        Fim := 0;
        //
        AnimateDrawer.StartValue := Ini;
        AnimateDrawer.StopValue  := Fim;
        AnimateDrawer.Start;
      end;
    end;
  finally
    {
    PageControl.Enabled := True;
    TWinControl(Sender).Enabled := HabilitaSender;
    }
  end;
  //
  Result := Form;
end;

function TUnMyObjects.FormTDICria2(Classe: TFormClass; InOwner: TFmxObject;
LayoutOwner: TLayout;
AnimateDrawer: TFloatAnimation; Unique: Boolean): TForm;
  procedure InfoErro(Codigo: Integer);
  begin
    Geral.MB_Erro('"InOwner" n�o implementado na function "FormTDICria".' +
    sLineBreak + 'Localizador na function: ' + Geral.FFN(Codigo, 3) +
    sLineBreak + 'Avise a DERMATEK');
  end;
const
  LayoutMain = 'LayoutMain';
var
  Form: TForm;
  Achou: Boolean;
  I, J: Integer;
  Ini: Single;
  ///
  TabControl: TTabControl;
  TabItem: TTabItem;
  Layout, Ly: TLayout;
  Panel: TPanel;
  Continua: Boolean;
begin
  Form        := nil;
  Achou       := False;
  I           := 0;
  TabControl  := nil;
  Continua    := False;
  TabItem     := nil;
  //
  if InOwner is TTabControl then
    TabControl := TTabControl(InOwner)
  else
  if InOwner is TForm then
  begin
    if TForm(InOwner).Owner is TTabItem then
    begin
      TabControl := TTabControl(TTabItem(TForm(InOwner).Owner).Owner);
      if not (TabControl is TTabControl) then
        InfoErro(3);
    end else InfoErro(2);
  end else
  if InOwner is TPanel then
  begin
    //Result := nil;
    //Exit;
    Layout := LayoutOwner;
    Continua := Layout <> nil;
(*
    Panel := TPanel(InOwner);
    for J := 0 to Panel.ComponentCount - 1 do
    begin
      if Panel.Components[J] is TLayout then
      begin
        Layout := TLayout(Form.Components[J]);
        Continua := True;
        Break;
      end;
    end;
*)
    if not Continua then
      Geral.MB_Erro('Layout indefinido no Painel!');
  end else InfoErro(1);
  ///
  if Unique and (TabControl <> nil) then
  begin
    for I := 0 to TabControl.TabCount - 1 do
    begin
      Form := TForm(TabControl.Tabs[I].Components[0]);
      //
      if Form is Classe then
      begin
        Achou := True;
        Break;
      end;
    end;
  end;
  if Achou then
  begin
    TabControl.ActiveTab          := TabControl.Tabs[I];
    TabControl.TabIndex           := TabControl.Tabs[I].Index;
  end else
  begin
    if TabControl <> nil then
      Continua := FormTDINovaAba2(TabControl, TabItem, Layout);
    //
    if Continua then
    begin
      //TabItem := TabControl.Tabs[0];
      Form := TFormClass(Classe).Create(Layout);
      Ly := nil;
      for J := 0 to Form.ComponentCount - 1 do
      begin
        if Form.Components[J] is TLayout then
        begin
          if TLayout(Form.Components[J]).Name = LayoutMain then
          begin
            Ly := TLayout(Form.Components[J]);
            Break;
          end;
        end;
      end;
      if Ly <> nil then
        Ly.Parent := Layout
      else
        Geral.MB_Erro('TLayot "' + LayoutMain + '" n�o encontrado!');
      //Form.WindowState := TWindowState.wsNormal;
      //with Form do
      if TabItem <> nil then
      begin
        {O evento onActive do TForm n�o � executado pq o que se torna ativo
         na verdade � o TTabSheet onde o formulario foi criado. Sendo assim qualquer
         coisa escrita no onActive do formul�rio n�o ser� executado.
         Para contornar esta situa��o nos passamos o evento onActive do Form para o
         evento onEnter do TTabSheet. E assim simulamos com seguran�a o evento onActive}
        TabItem.OnEnter := Form.OnActivate;
        //
        // Transfere o caption do form para o caption da aba
        TabItem.Text := Copy(Form.Caption, 1, 13);
        TabControl.ActiveTab := TabItem;
        TabControl.TabIndex  := TabItem.Index;
      end;
    end;
  end;
  if AnimateDrawer <> nil then
  begin
    if AnimateDrawer.StopValue > 0 then
    begin
      Ini := AnimateDrawer.StopValue;
      //
      AnimateDrawer.StartValue := Ini;
      AnimateDrawer.StopValue  := 0;
      AnimateDrawer.Start;
    end;
  end;
  Result := Form;
end;

{$IFNDEF DmkMobile}
function TUnMyObjects.FormTDICria3(Classe: TFormClass;
  FormVars: String; InOwner: TFmxObject; LayoutOwner: TLayout;
  AnimateDrawer: TFloatAnimation; SbPesq: TSpeedButton;
  Unique: Boolean): TForm;
  //
  procedure InfoErro(Codigo: Integer);
  begin
    Geral.MB_Erro('"InOwner" n�o implementado na function "FormTDICria".' +
    sLineBreak + 'Localizador na function: ' + Geral.FFN(Codigo, 3) +
    sLineBreak + 'Avise a DERMATEK');
  end;
const
  LayoutMain = 'LayoutMain';
var
  Form: TForm;
  Achou: Boolean;
  I, J: Integer;
  Ini: Single;
  ///
  TabControl: TTabControl;
  TabItem: TdmkTabItem;
  Layout, Ly: TLayout;
  Panel: TPanel;
  Continua: Boolean;
  //Component: TComponent;
  DmkSpidAct: TDmkSpidAct;
  SpeedButton: TdmkSpeedButton;
begin
  Form        := nil;
  Achou       := False;
  I           := 0;
  TabControl  := nil;
  Continua    := False;
  TabItem     := nil;
  //
  if InOwner is TTabControl then
    TabControl := TTabControl(InOwner)
  else
  if InOwner is TForm then
  begin
    if TForm(InOwner).Owner is TTabItem then
    begin
      TabControl := TTabControl(TTabItem(TForm(InOwner).Owner).Owner);
      if not (TabControl is TTabControl) then
        InfoErro(3);
    end else InfoErro(2);
  end else
  if InOwner is TPanel then
  begin
    //Result := nil;
    //Exit;
    Layout := LayoutOwner;
    Continua := Layout <> nil;
(*
    Panel := TPanel(InOwner);
    for J := 0 to Panel.ComponentCount - 1 do
    begin
      if Panel.Components[J] is TLayout then
      begin
        Layout := TLayout(Form.Components[J]);
        Continua := True;
        Break;
      end;
    end;
*)
    if not Continua then
      Geral.MB_Erro('Layout indefinido no Painel!');
  end else InfoErro(1);
  ///
  if Unique and (TabControl <> nil) then
  begin
    for I := 0 to TabControl.TabCount - 1 do
    begin
      Form := TForm(TabControl.Tabs[I].Components[0]);
      //
      if Form is Classe then
      begin
        Achou := True;
        Break;
      end;
    end;
  end;
  if Achou then
  begin
    TabControl.ActiveTab          := TabControl.Tabs[I];
    TabControl.TabIndex           := TabControl.Tabs[I].Index;
  end else
  begin
    if TabControl <> nil then
      Continua := FormTDINovaAba3(TabControl, TabItem, Layout);
    //
    if Continua then
    begin
      //TabItem := TabControl.Tabs[0];
      Form := TFormClass(Classe).Create(Layout);
      Ly := nil;
      for J := 0 to Form.ComponentCount - 1 do
      begin
        if Form.Components[J] is TLayout then
        begin
          if TLayout(Form.Components[J]).Name = LayoutMain then
          begin
            Ly := TLayout(Form.Components[J]);
              Break;
          end;
        end;
      end;
      if Ly <> nil then
        Ly.Parent := Layout
      else
        Geral.MB_Erro('TLayot "' + LayoutMain + '" n�o encontrado!');
      //Form.WindowState := TWindowState.wsNormal;
      //with Form do
      if TabItem <> nil then
      begin
        {O evento onActive do TForm n�o � executado pq o que se torna ativo
         na verdade � o TTabSheet onde o formulario foi criado. Sendo assim qualquer
         coisa escrita no onActive do formul�rio n�o ser� executado.
         Para contornar esta situa��o nos passamos o evento onActive do Form para o
         evento onEnter do TTabSheet. E assim simulamos com seguran�a o evento onActive}
        TabItem.OnEnter := Form.OnActivate;
        //
        // Transfere o caption do form para o caption da aba
        TabItem.Text := Copy(Form.Caption, 1, 13);
        TabControl.ActiveTab := TabItem;
        TabControl.TabIndex  := TabItem.Index;
        //
        TabItem.DmkFormVars := nil;
        if FormVars <> '' then
        begin
          TabItem.DmkFormVars := Form.FindComponent(FormVars) as TdmkFormVars;
          //Component := Form.FindComponent(FormVars);
          //if Component <> nil then
          //  TabItem.DmkFormVars := TdmkFormVars(Component);
        end;
        //
        if SbPesq <> nil then
          SbPesq.Enabled := Assigned(TabItem.DmkFormVars);
      end;
    end;
  end;
  if AnimateDrawer <> nil then
  begin
    if AnimateDrawer.StopValue > 0 then
    begin
      Ini := AnimateDrawer.StopValue;
      //
      AnimateDrawer.StartValue := Ini;
      AnimateDrawer.StopValue  := 0;
      AnimateDrawer.Start;
    end;
  end;
  Result := Form;
end;
{$ENDIF}

{$IFNDEF DmkMobile}
procedure TUnMyObjects.FormTDIFecha(Form: TForm; AbaQueChamou: TComponent);
var
  TabItem: TdmkTabItem;
  TabControl: TTabControl;
  Anterior: Integer;
  Compo: TComponent;
begin
  if Form.Owner is TApplication then
  begin
    Form.Close;
  end else
  if (Form.Owner is TdmkTabItem)
  or (Form.Owner is TLayout) then
  begin
    if Form.Owner is TLayout then
    begin
      Compo := TLayout(Form.Owner).Owner;
      if Compo is TdmkTabItem then
        TabItem := TdmkTabItem(Compo)
      else
        Geral.MB_Erro('"Owner" de "Layout" desconhecido! Avise a DERMATEK!' +
        slineBreak + 'Nome: ' + TComponent(Compo).Name +
        slineBreak + 'Classe: ' + TComponent(Compo).ClassName);
      //
      if not(TabItem.Owner is TTabControl) then
      begin
        Compo := TTabItem(Compo).Owner;
        if Compo is TdmkTabItem then
          TabItem := TdmkTabItem(Compo)
        else
        if Compo is TTabItem then
          TabItem := TdmkTabItem(Compo)
        else
          Geral.MB_Erro('"Owner" de "Layout" desconhecido! Avise a DERMATEK!' +
          slineBreak + 'Nome: ' + TComponent(Compo).Name +
          slineBreak + 'Classe: ' + TComponent(Compo).ClassName);
      end
      else
        TabControl := TTabControl(TabItem.Owner);
    end;
    Form.Close;
    Form.Destroy;
    //Form.Free; // gera erro
    Form := nil;
    if Form = nil then
    begin
      Anterior := TabItem.Index -1;
      if Anterior < 0 then
        Anterior := 0;
      //FPesqFormVars := nil;
      //QrPesq.Close;
      TabItem.Free;
      if TabControl <> nil then
      begin
        if Anterior < TabControl.TabCount then
          TabControl.TabIndex := Anterior
        else
        if TabControl.TabCount > 0 then
          TabControl.TabIndex := TabControl.TabCount - 1;
      end;
    end;
  end
  else
  if Form.Owner is TPanel then
  begin
    Geral.MB_Aviso('Este "quadro" ser� fechado junto com a janela m�e!');
  end else
  if Form.Owner = nil then
  begin
    if Form.Parent is TPanel then
      Geral.MB_Aviso('Este "quadro" ser� fechado junto com a janela m�e!');
  end
  else
  begin
    Geral.MB_Erro('"Owner" de "Form" desconhecido! Avise a DERMATEK!' +
    slineBreak + 'Nome: ' + TComponent(Form.Owner).Name +
    slineBreak + 'Clase owner: ' + TComponent(Form.Owner).ClassName +
    slineBreak + 'Classe: ' + TComponent(Form).ClassName);
  end;
end;
{$ENDIF}

function TUnMyObjects.FormTDINovaAba(TabControl: TTabControl): TTabItem;
var
  Tab: TTabItem;
begin
  Tab := TTabItem.Create(TabControl);
  //
  //Tab.PageControl := PageControl;
  //Tab. := TabControl;
  Tab.Visible  := True;
  Tab.Text     := 'Carregando...';
  Tab.PopupMenu   := nil;
  //
  TabControl.ActiveTab := Tab;
  TabControl.TabIndex := Tab.Index;
  //
  Result := Tab;
end;

function TUnMyObjects.FormTDINovaAba2(const TabControl: TTabControl;
  var TabItem: TTabItem; var Layout: Tlayout): Boolean;
begin
  TabItem := TTabItem.Create(TabControl);
  TabItem.Parent := TabControl;
  HackTabControl(TabControl).ContentAddObject(TabItem);
  //
  TabItem.Visible  := True;
  TabItem.Text     := 'Carregando...';
  TabItem.PopupMenu   := nil;
  //
  TabControl.ActiveTab := TabItem;
  TabControl.TabIndex := TabItem.Index;
  //
  Layout := TLayout.Create(TabItem);
  Layout.Parent := TabItem;
  Layout.Align := TAlignLayout.alClient;
  //
  Result := True;
end;

{$IFNDEF DmkMobile}
function TUnMyObjects.FormTDINovaAba3(const TabControl: TTabControl;
  var TabItem: TdmkTabItem; var Layout: Tlayout): Boolean;
begin
  TabItem := TdmkTabItem.Create(TabControl);
  TabItem.Parent := TabControl;
  HackTabControl(TabControl).ContentAddObject(TabItem);
  //
  TabItem.Visible  := True;
  TabItem.Text     := 'Carregando...';
  TabItem.PopupMenu   := nil;
  //
  TabControl.ActiveTab := TabItem;
  TabControl.TabIndex := TabItem.Index;
  //
  Layout := TLayout.Create(TabItem);
  Layout.Parent := TabItem;
  Layout.Align := TAlignLayout.alClient;
  //
  Result := True;
end;
{$ENDIF}

function TUnMyObjects.CriaFm_SM(InstanceClass: TComponentClass): TForm;
var
  Reference: TComponent;
var
  Instance: TComponent;
  Form: String;
  //MyCursor: TCursor;
  //Campo: String;
begin
  Result := nil;
  Form := InstanceClass.ClassName;
  //MyCursor := Screen.Cursor;
  //Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(nil);
      //
      Result := TForm(Reference);
      //TComponent(Reference) := nil;
      Reference.Free;
    except
      //TComponent(Reference) := nil;
      Reference.Free;
      raise;
    end;
    with Result do
    begin
      StyleBook := VAR_FORM_STYLEBOOK;
      //BorderStyle := bsNone;
      //Parent := Painel;
      //Show;
      //Align := alClient;
    end;
    //Screen.Cursor := MyCursor;
  finally
    //Screen.Cursor := MyCursor;
  end;
end;


procedure TUnMyObjects.IncProgress(PB: TProgressBar);
begin
  PB.Value := PB.Value + 1;
  PB.Repaint;
  //
  Application.ProcessMessages;
end;

procedure TUnMyObjects.Informa(Label1: TLabel; Aguarde: Boolean; Texto: String);
begin
  if Label1 <> nil then
  begin
    if Aguarde then
      Label1.Text := 'Aguarde... ' + Texto + '...'
    else
      Label1.Text := Texto;
    //
    Label1.Repaint;
    Geral.ProcessaMsgs;
    //
    Application.ProcessMessages;
  end;
end;

function TUnMyObjects.Informa2(Label1, Label2: TLabel; Aguarde: Boolean;
  Texto: String; IsThread: Boolean): Boolean;
 begin
  Result := False;
  try
    if Label1 <> nil then
    begin
      if Aguarde then
        Label1.Text := 'Aguarde... ' + Texto + '...'
      else
        Label1.Text := Texto;
      Label1.Repaint;
    end;
    if Label2 <> nil then
    begin
      if Aguarde then
        Label2.Text := 'Aguarde... ' + Texto + '...'
      else
        Label2.Text := Texto;
      Label1.Repaint;
    end;
    Geral.ProcessaMsgs();
    //
    Result := True;
  except
    // nada
  end;
end;

function TUnMyObjects.PlatformListBoxItemFator(): Double;
begin
{$IFDEF MSWINDOWS}
    Result := 2;
{$ELSE}
    Result := 1.33;
{$ENDIF}
end;

procedure TUnMyObjects.ReAplyStyle(Form: TForm; StyleBook: TStyleBook);
begin
  if Form.StyleBook <> nil then
  begin
    Form.StyleBook := nil;
    Form.StyleBook := StyleBook;
  end;
end;

function TUnMyObjects.RGItemIndex(RadioButons: array of TRadioButton): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := Low(RadioButons) to High(RadioButons) do
  begin
    if RadioButons[I].IsChecked then
    begin
      Result := I;
      Exit;
    end;
  end;
end;

{$IFNDEF DmkMobile}
function TUnMyObjects.SelListView(Titulo: String; Itens: array of String;
  Default: Integer): Integer;
var
  I: Integer;
  LItem: TListViewItem;
begin
  CriaFm(TFmSelListView, FmSelListView);
  with FmSelListView do
  begin
    Caption := Titulo;
    LwLista.BeginUpdate;
    try
      LwLista.Items.Clear;
      for I := Low(Itens) to High(Itens) do
      begin
        LItem := LwLista.Items.Add;
        LItem.Text := Itens[I];
        //
        if I = Default then
        begin
          LwLista.Selected := LItem;
          LwLista.ItemIndex := I;
          LItem.Checked := True;
        end;
  (*
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := QrOSWCab.FieldByName('NO_ENT').AsString;
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := QrOSWCab.FieldByName('NO_SiapTerCad').AsString;
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := QrOSWCab.FieldByName('NO_ENTICONTAT').AsString;
        LItem.BitmapRef := Image1.Bitmap;
        //
        case QrOSWCab.FieldByName('Respondido').AsInteger of
          0: LItem.Accessory := FMX.ListView.Types.TAccessoryType.More;
          1: LItem.Accessory := FMX.ListView.Types.TAccessoryType.Checkmark;
          else LItem.Accessory := FMX.ListView.Types.TAccessoryType.Detail;
        end;
        //
  *)
      end;
    finally
      LwLista.EndUpdate;
    end;
    Result := FmSelListView.DoSel();
    FmSelListView.Destroy;
  end;
end;
{$ENDIF}

{$IFNDEF DmkMobile}
function TUnMyObjects._FIX_Compo(MyControl: TControl; Children: Boolean):
  Integer;
var
  I, J: Integer;
var
  PI: PPropInfo;
  IsNeed: Boolean;
  Texto: String;
  Focou, CanFocus: Boolean;
  ShadowEffectIsNeed: TShadowEffect;
  //
begin
  PI := GetPropInfo(MyControl, sDmkIsNeed);
  if PI <> nil then
  begin
    // precisa ser preenchido
    IsNeed := GetPropValue(MyControl, sDmkIsNeed, False);
    PI := GetPropInfo(MyControl, sText);
    if PI <> nil then
    begin
      Texto := GetPropValue(MyControl, sText);
      // Nao foi preenchido
      if IsNeed and (Texto = EmptyStr) then
      begin
        Result := Result + 1;
        // foca primeiro que achar!
        if not Focou then
        begin
          PI := GetPropInfo(MyControl, sCanFocus);
          if PI <> nil then
          begin
            CanFocus := GetPropValue(MyControl, sCanFocus, False);
            if CanFocus then
            begin
              TControl(MyControl).SetFocus;
              Focou := TControl(MyControl).IsFocused;
            end;
          end;
        end;
        // Colocar efeito!
        PI := GetPropInfo(MyControl, sShadowEffectIsNeed);
        if PI <> nil then
        begin
          ShadowEffectIsNeed := TShadowEffect(DmkTypInfo.GetComponentObjectProperty(MyControl, sShadowEffectIsNeed));
          // Cria sub componente se nao exoste
          if ShadowEffectIsNeed = nil then
          begin
            ShadowEffectIsNeed := TShadowEffect.Create(MyControl);
            ShadowEffectIsNeed.Parent      := MyControl;
            ShadowEffectIsNeed.Distance    := 3.0;
            ShadowEffectIsNeed.Direction   := 45.0;
            ShadowEffectIsNeed.Softness    := 0.5;
            ShadowEffectIsNeed.Opacity     := 1.0;
            ShadowEffectIsNeed.ShadowColor := claRed;
            ShadowEffectIsNeed.Enabled     := False;
            ShadowEffectIsNeed.Name        := sIsNeed + MyControl.Name;
            //
            SetObjectProp(MyControl, PI, ShadowEffectIsNeed, True);
          end;
          // Ativa efeito
          ShadowEffectIsNeed.Enabled := True;
        end;
      end;
    end;
  end;
  // verifica componentes em panel, layout, etc
  if Children then
  begin
    for J := 0 to MyControl.ChildrenCount - 1 do
    begin
      if MyControl.Children[J] is TControl then
        _FIX_Compo(TControl(MyControl.Children[J]), Children);
    end;
  end;
end;
{$ENDIF}

{
function TUnMyObjects.ShowModal_Var(InstanceClass: TComponentClass; var Reference;
  ModoAcesso: TAcessFmModo): Variant;
var
  Instance: TComponent;
  Form: String;
  Referencia: TForm;
begin
  Result := Null;
  Form := InstanceClass.ClassName;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(nil);
      //
      Referencia := TForm(Reference);
    except
      TComponent(Reference) := nil;
      raise;
    end;
    with Referencia do
    begin
      StyleBook := VAR_FORM_STYLEBOOK;
      //Show;
      TForm(Referencia).ShowModal(
        procedure(ModalResult: TModalResult)
        begin
          // if OK was pressed and an item is selected, pick it
          if ModalResult = mrOK then
          begin
            //if TForm(Form).FindComponent(''). as TEdit then
            begin
              // ver como pegar variavel!!!!
              //if dlg.ListBox1.ItemIndex >= 0 then
                //edit1.Text := dlg.ListBox1.Items [dlg.ListBox1.ItemIndex];
            end;
            / erro Memory Leak!
            TForm(Referencia).DisposeOf;
          end;
        end
      );
    end;
    //Screen.Cursor := MyCursor;
  except
    //Screen.Cursor := MyCursor;
    on E: Exception do
    begin
      Geral.MB_Erro('Erro ao criar janela:' + sLineBreak + E.Message);
    end;
  end;
end;
}

end.
