unit UnGrlListas;

interface

uses System.Classes, Generics.Collections;

type

  TCampos = ^TCamposNomes;
  TCamposNomes = record
    Field   : String;
    Tipo    : String;
    Null    : String;
    Key     : String;
    Default : String;
    Extra   : String;
    Coments : String;
  end;

  TIndices = ^TCamposIndices;
  TCamposIndices = record
    //Table:        String;
    Key_name:     String;
    Non_unique:   Int64;
    Seq_in_index: Int64;
    Column_name:  String;
    //Collation:    String;
    //Cardinality:  Variant;
    //Sub_part:     Variant;
    //Pacote:       String;
    //Comment:      String;
  end;

  TTabelas = ^TFldTabelas;
  TFldTabelas = record
    Tem_Del : Boolean;
    TabCria : String;
    TabBase : String;
  end;

  TJanelas = ^TRegistrosJanelas;
  TRegistrosJanelas = record
    ID:        String;
    Nome:      String;
    Descricao: String;
    Modulo:    String;
    //Instance:  TComponentClass;
    //Reference: TForm;
  end;

  TUnGrlListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AdTbLst(Lista: TList<TTabelas>; CampoDel: Boolean; TabCria, TabBase: String);
  end;

//const

var
  GrlListas: TUnGrlListas;

implementation

{ TUnGrlListas }

var
  TabInLst: TTabelas;

procedure TUnGrlListas.AdTbLst(Lista: TList<TTabelas>; CampoDel: Boolean; TabCria,
  TabBase: String);
begin
  New(TabInLst);
  TabInLst.Tem_Del := CampoDel;
  TabInLst.TabCria := TabCria;
  TabInLst.TabBase := TabBase;
  Lista.Add(TabInLst);
end;

end.
