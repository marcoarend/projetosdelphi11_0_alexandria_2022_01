program DmkMail;

uses
  System.StartUpCopy,
  FMX.Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  UnSQLite_PF2 in '..\..\AllOS\RDBMs\SQLite\UnSQLite_PF2.pas',
  MailCfg in 'MailCfg.pas' {FmMailCfg},
  DmkLogin in '..\..\AllOS\Forms\DmkLogin.pas' {FmDmkLogin},
  VerifyDB in '..\..\AllOS\Forms\VerifyDB.pas' {FmVerifyDB},
  UnGeral in '..\Geral\UnGeral.pas',
  UnDmkRDBMs in '..\..\AllOS\RDBMs\UnDmkRDBMs.pas',
  UnDmkEnums in '..\Geral\UnDmkEnums.pas',
  UnValidaIE in '..\Geral\UnValidaIE.pas',
  UnGrl_Consts in '..\Geral\UnGrl_Consts.pas',
  UnGrl_Vars in '..\Geral\UnGrl_Vars.pas',
  UnApp_Consts in 'UnApp_Consts.pas',
  UnDmkSQLite in '..\..\AllOS\RDBMs\SQLite\UnDmkSQLite.pas',
  UnSQLite_PF in '..\..\AllOS\RDBMs\SQLite\UnSQLite_PF.pas',
  UnDmkFireDAC in '..\..\AllOS\RDBMs\UnDmkFireDAC.pas',
  ModGerl in '..\Geral\ModGerl.pas' {DmModGerl},
  ModApp in 'ModApp.pas' {DmModApp},
  UnDmkDevice in '..\..\AllOS\Devices\UnDmkDevice.pas',
  UnitMD5 in '..\..\..\Outros\Encrypt\UnitMD5.pas',
  UnFMX_Vars in '..\Geral\UnFMX_Vars.pas',
  UnDmkDAC in '..\..\AllOS\RDBMs\UnDmkDAC.pas',
  UnMyListas in 'UnMyListas.pas',
  UnMyLinguas in '..\..\AllOS\Listas\UnMyLinguas.pas',
  UnGrl_Tabs in '..\Geral\UnGrl_Tabs.pas',
  UnPerfJan_Tabs in '..\..\AllOS\Listas\UnPerfJan_Tabs.pas',
  UnWebReq in '..\Geral\UnWebReq.pas',
  UnMyObjects in '..\Geral\UnMyObjects.pas',
  UnCheckSQLite in '..\..\AllOS\RDBMs\SQLite\UnCheckSQLite.pas',
  UnMySQL_PF in '..\..\AllOS\RDBMs\MySQL\UnMySQL_PF.pas',
  UnApp_Vars in 'UnApp_Vars.pas',
  UsuarioAdd in '..\..\AllOS\Forms\UsuarioAdd.pas' {FmUsuarioAdd},
  Mail_Tabs in '..\..\..\Outros\Email\Mail_Tabs.pas',
  UnMailAllOS in '..\..\..\Outros\Email\Units\AllOS\UnMailAllOS.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmUsuarioAdd, FmUsuarioAdd);
  Application.CreateForm(TFmMailCfg, FmMailCfg);
  Application.CreateForm(TFmDmkLogin, FmDmkLogin);
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  Application.CreateForm(TDmModGerl, DmModGerl);
  Application.CreateForm(TDmModApp, DmModApp);
  Application.Run;
end.
