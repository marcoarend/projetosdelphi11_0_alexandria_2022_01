unit UnApp_Vars;

interface

uses UnDmkEnums, UnGrl_Vars, UnGeral, UnFMX_Vars;

type
  TUnApp_Vars = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  DefineValDeVarsGeraisDoApp(): String;
  end;


var
  App_Vars: TUnApp_Vars;
  //
  VAR_AGENTE_LOGADO: Integer;

implementation

uses Principal;

{ TUnApp_Vars }

function TUnApp_Vars.DefineValDeVarsGeraisDoApp(): String;
begin
  Geral.DefineFormatacoes();
  //
  VAR_APP_DB_TYPE := dbmsSQLite;
  VAR_APP_DB_NAME_E_EXT := 'dmkmail.s3db';
  VAR_APP_DB_FULLPATH := Geral.DirAppAllOS(VAR_APP_DB_NAME_E_EXT);
  VAR_LIBERA_TODOS_FORMS := False;
  //VAR_FORM_STYLEBOOK := FmPrincipal.StyleBook1;
  VAR_LOC_LATI_LONGI := False;
  VAR_LATITUDE := 0;
  VAR_LONGITUDE := 0;
  VAR_DEVICE_ID := '';
end;

end.
