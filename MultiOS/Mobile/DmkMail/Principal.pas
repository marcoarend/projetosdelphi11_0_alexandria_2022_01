unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.TreeView, FMX.ListBox, FMX.StdCtrls, FMX.MultiView, FMX.Memo, FMX.Objects,
  FMX.Menus, FMX.ListView.Types, FMX.ListView, System.Math.Vectors,
  FMX.Controls3D, FMX.Layers3D, Data.DbxSqlite, Data.DB, Data.SqlExpr,
  System.IOUtils, Data.FMTBcd, Data.DBXDataSnap, IPPeerClient, Data.DBXCommon,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.FMXUI.Wait,
  FireDAC.Comp.UI, FMX.ExtCtrls, FMX.TabControl, FMX.WebBrowser,
  FMX.Controls.Presentation;

type
  TFmPrincipal = class(TForm)
    MultiView1: TMultiView;
    MasterToolbar: TToolBar;
    SBOcultar: TSpeedButton;
    Layout1: TLayout;
    DetailToolbar: TToolBar;
    DetailLabel: TLabel;
    MasterButton: TSpeedButton;
    CBMailCfg: TComboBox;
    SBConta: TSpeedButton;
    SBMenu: TSpeedButton;
    TabControl1: TTabControl;
    Pastar: TTabItem;
    Menu: TTabItem;
    TreeView1: TTreeView;
    ListBox1: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxItem2: TListBoxItem;
    ListView2: TListView;
    WebBrowser1: TWebBrowser;
    procedure SBOcultarClick(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure SBContaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBoxItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraMailCfg();
    procedure MostraVerifyDB;
    procedure MostraDmkLogin();
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  UnSQLite_PF2, MailCfg, DmkLogin, UnApp_Vars, ModApp, UnMyObjects, UnMyListas,
  VerifyDB;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SBOcultar.Visible := True;
  {$ELSE}
  SBOcultar.Visible := False;
  {$ENDIF}
  //
  TabControl1.TabIndex := 0;
  //
  App_Vars.DefineValDeVarsGeraisDoApp();
  //
  MyObjects.CriaFm(TDmModApp, DmModApp);
  //
  DmModApp.ReopenControle;
  //
  DmModApp.QrControle.RecordCount;
  if DmModApp.QrControle.FieldByName('Versao').AsInteger <> CO_VERSAO then
    MostraVerifyDB;
  //
  MostraDmkLogin;
end;

procedure TFmPrincipal.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  MultiView1.HideMaster;
end;

procedure TFmPrincipal.ListBoxItem2Click(Sender: TObject);
begin
  //MostraVerifyDB;
  WebBrowser1.Navigate('http://www.dermatek.com.br');
end;

procedure TFmPrincipal.MostraVerifyDB;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.Show;
  {$ELSE}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.ShowModal;
  FmVerifyDB.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.MostraDmkLogin;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmDmkLogin, FmDmkLogin);
  FmDmkLogin.Show;
  {$ELSE}
  Application.CreateForm(TFmDmkLogin, FmDmkLogin);
  FmDmkLogin.ShowModal;
  FmDmkLogin.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.MostraMailCfg;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmMailCfg, FmMailCfg);
  FmMailCfg.Show;
  {$ELSE}
  Application.CreateForm(TFmMailCfg, FmMailCfg);
  FmMailCfg.ShowModal;
  FmMailCfg.Destroy;
  {$ENDIF}
end;

procedure TFmPrincipal.SBContaClick(Sender: TObject);
begin
  MostraMailCfg();
end;

procedure TFmPrincipal.SBOcultarClick(Sender: TObject);
begin
  MultiView1.HideMaster;
end;

end.
