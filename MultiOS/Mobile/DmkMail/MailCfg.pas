unit MailCfg;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.SearchBox, System.Actions, FMX.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient, IdSMTPBase, IdSMTP, IdIMAP4;

type
  TFmMailCfg = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbInclui: TSpeedButton;
    Label1: TLabel;
    SearchBox1: TSearchBox;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    TbEmailConta: TFDTable;
    TbEmailContaCodigo: TIntegerField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    QrEmailConta: TFDQuery;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    EdNome: TEdit;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIAtivo: TListBoxItem;
    ClearEditButton1: TClearEditButton;
    TbEmailContaNome: TStringField;
    EdSend_Name: TEdit;
    ClearEditButton3: TClearEditButton;
    ListBoxItem5: TListBoxItem;
    EdUsuario: TEdit;
    ClearEditButton4: TClearEditButton;
    ListBoxItem6: TListBoxItem;
    EdSenha: TEdit;
    ClearEditButton5: TClearEditButton;
    ListBoxItem2: TListBoxItem;
    EdSMTPServer: TEdit;
    ClearEditButton2: TClearEditButton;
    ListBoxItem7: TListBoxItem;
    EdPorta_Mail: TEdit;
    ClearEditButton6: TClearEditButton;
    CkLogi_SSLSMTP: TCheckBox;
    ListBoxItem8: TListBoxItem;
    EdReceServer: TEdit;
    ClearEditButton7: TClearEditButton;
    ListBoxItem9: TListBoxItem;
    EdRecePorta_Mail: TEdit;
    ClearEditButton8: TClearEditButton;
    CkReceLogi_SSL: TCheckBox;
    ListBoxItem10: TListBoxItem;
    CkLogi_Auth: TCheckBox;
    ListBoxItem11: TListBoxItem;
    EdLogi_Name: TEdit;
    ClearEditButton9: TClearEditButton;
    ListBoxItem12: TListBoxItem;
    EdLogi_Pass: TEdit;
    ClearEditButton10: TClearEditButton;
    ListBoxItem4: TListBoxItem;
    CkAtivo: TCheckBox;
    SbTestar: TSpeedButton;
    IdSMTP1: TIdSMTP;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IMAP: TIdIMAP4;
    procedure SbSaidaClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure FormShow(Sender: TObject);
    procedure SbTestarClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEmailConta(Codigo: Integer);
    procedure ReopenTbEmailConta;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmMailCfg: TFmMailCfg;

implementation

{$R *.fmx}

uses ModApp, UnSQLite_PF, UnGeral, UnDmkEnums, UnDmkRDBMs, UnGrl_Vars,
  UnMailAllOS;

procedure TFmMailCfg.FormShow(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenTbEmailConta;
end;

procedure TFmMailCfg.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := SQLite_PF.GetSelectedValue(ListBox1).ToString.ToInteger();
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmMailCfg.ReopenEmailConta(Codigo: Integer);
begin
  SQLite_PF.AbreSQLQuery0(Self, QrEmailConta, DmModApp.FDAppDB, [
    'SELECT * FROM emailconta WHERE Codigo=' + FormatFloat('0', Codigo),
    '']);
end;

procedure TFmMailCfg.ReopenTbEmailConta;
begin
  TbEmailConta.Close;
  TbEmailConta.Connection := DmModApp.FDAppDB;
  TbEmailConta.Open();
  //
  if TbEmailConta.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmMailCfg.SbConfirmaClick(Sender: TObject);
var
  Codigo, Ativo, Porta_Mail, RecePorta_Mail, Logi_Auth, Logi_SSLSMTP, ReceLogi_SSL: Integer;
  Nome, Send_Name, Usuario, Senha, SMTPServer, ReceServer, Logi_Name, Logi_Pass: String;
  SQLTipo: TSQLType;
begin
  Codigo         := FCodigo;
  Nome           := EdNome.Text;
  Send_Name      := EdSend_Name.Text;
  Usuario        := EdUsuario.Text;
  Senha          := EdSenha.Text;
  SMTPServer     := EdSMTPServer.Text;
  Porta_Mail     := Geral.IMV(Geral.SoNumero_TT(EdPorta_Mail.Text));
  Logi_SSLSMTP   := Geral.BoolToInt(CkLogi_SSLSMTP.IsChecked);
  ReceServer     := EdReceServer.Text;
  RecePorta_Mail := Geral.IMV(Geral.SoNumero_TT(EdRecePorta_Mail.Text));
  ReceLogi_SSL   := Geral.BoolToInt(CkReceLogi_SSL.IsChecked);
  Logi_Auth      := Geral.BoolToInt(CkLogi_Auth.IsChecked);
  Logi_Name      := EdLogi_Name.Text;
  Logi_Pass      := EdLogi_Pass.Text;
  Ativo          := Geral.BoolToInt(CkAtivo.IsChecked);
  //
  if SQLite_PF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := SQLite_PF.ObtemCodigoInt(Self, 'emailconta', 'Codigo',
                 DmModApp.QrAux, DmModApp.FDAppDB);
  end else
    SQLTipo := stUpd;
  //
  if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, DmModApp.FDAppDB, stIns,
    'emailconta', False,
    ['Nome', 'PermisNiv', 'Send_Name', 'Usuario', 'Password', 'SMTPServer',
    'Porta_Mail', 'Logi_SSL', 'ReceServer', 'RecePorta_Mail', 'ReceLogi_SSL',
    'Logi_Auth', 'Logi_Name', 'Logi_Pass', 'ReceTipo', 'Ativo'], ['Codigo'],
    [Nome, VAR_WEB_USER_ID, Send_Name, Usuario, Senha, SMTPServer,
    Porta_Mail, Logi_SSLSMTP, ReceServer, RecePorta_Mail, ReceLogi_SSL,
    Logi_Auth, Logi_Name, Logi_Pass, 1(*IMAP*), Ativo], [Codigo], True) then
  begin
    ReopenTbEmailConta;
    //
    ExecuteAction(ChangeTabAction1);
  end else
    Geral.MB_Aviso('Falha ao salvar registro!');
end;

procedure TFmMailCfg.SbExcluiClick(Sender: TObject);
begin
  if VAR_WEB_USER_ID <> QrEmailConta.FieldByName('PermisNiv').AsInteger then
  begin
    Geral.MB_Aviso('Voc� n�o tem permiss�o para excluir esta configura��o!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o deste registro?') = mrYes then
  begin
    SQLite_PF.ExecutaSQLQuery0(Self, DmModApp.QrUpd, DmModApp.FDAppDB,
      ['DELETE FROM emailconta WHERE Codigo = ' + FormatFloat('0', FCodigo)]);
    //
    ReopenTbEmailConta;
    //
    ExecuteAction(ChangeTabAction1);
    //
    DmModApp.QrAux.Close;
  end;
end;

procedure TFmMailCfg.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmMailCfg.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmMailCfg.SbTestarClick(Sender: TObject);
var
  Host, Username, Password, SevidorImap: String;
  Port, Logi_SSL: Integer;
begin
  //Verifica SMTP
  //
  Host     := EdSMTPServer.Text;
  Username := EdUsuario.Text;
  Password := EdSenha.Text;
  Port     := Geral.IMV(EdPorta_Mail.Text);
  Logi_SSL := Geral.BoolToInt(CkLogi_SSLSMTP.IsChecked);
  //
  if (Host = '') or (Username = '') or (Password = '') then
  begin
    Geral.MB_Info('Os dados de autentica��o est�o incompletos!' + sLineBreak +
      'Verifique se os campos: Host, Usu�rio e Senha est�o preenchidos!');
    Exit;
  end;
  //
  UMailAllOS.TestarConexaoSMTP(IdSMTP1, IdSSL1, Host, Username, Password, Port,
    Logi_SSL, True);
  //
  //Verifica IMAP
  SevidorImap := EdReceServer.Text;
  //
  if SevidorImap <> '' then
  begin
    Port     := Geral.IMV(EdRecePorta_Mail.Text);
    Logi_SSL := Geral.BoolToInt(CkReceLogi_SSL.IsChecked);
    //
    UMailAllOS.ConectaIMAP(IMAP, IdSSL1, SevidorImap, Username, Password,
      Port, Logi_SSL, True, True);
  end;
end;

procedure TFmMailCfg.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenEmailConta(FCodigo);
      //
      LBICodigo.Text := FormatFloat('0', QrEmailConta.FieldByName('Codigo').AsInteger);
      LBINome.Text   := QrEmailConta.FieldByName('Nome').AsString;
      //
      if QrEmailConta.FieldByName('Ativo').AsInteger = 1 then
        LBIAtivo.Text := 'Ativo'
      else
        LBIAtivo.Text := 'Inativo';
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenEmailConta(FCodigo);
        //
        if VAR_WEB_USER_ID <> QrEmailConta.FieldByName('PermisNiv').AsInteger then
        begin
          Geral.MB_Aviso('Voc� n�o tem permiss�o para editar esta configura��o!');
          Exit;
        end;
        EdNome.Text              := QrEmailConta.FieldByName('Nome').AsString;
        EdSend_Name.Text         := QrEmailConta.FieldByName('Send_Name').AsString;
        EdUsuario.Text           := QrEmailConta.FieldByName('Usuario').AsString;
        EdSenha.Text             := QrEmailConta.FieldByName('Password').AsString;
        EdSMTPServer.Text        := QrEmailConta.FieldByName('SMTPServer').AsString;
        EdPorta_Mail.Text        := Geral.FF0(QrEmailConta.FieldByName('Porta_Mail').AsInteger);
        CkLogi_SSLSMTP.IsChecked := Geral.IntToBool(QrEmailConta.FieldByName('Logi_SSL').AsInteger);
        EdReceServer.Text        := QrEmailConta.FieldByName('ReceServer').AsString;
        EdRecePorta_Mail.Text    := Geral.FF0(QrEmailConta.FieldByName('RecePorta_Mail').AsInteger);
        CkReceLogi_SSL.IsChecked := Geral.IntToBool(QrEmailConta.FieldByName('ReceLogi_SSL').AsInteger);
        CkLogi_Auth.IsChecked    := Geral.IntToBool(QrEmailConta.FieldByName('Logi_Auth').AsInteger);
        EdLogi_Name.Text         := QrEmailConta.FieldByName('Logi_Name').AsString;
        EdLogi_Pass.Text         := QrEmailConta.FieldByName('Logi_Pass').AsString;
        CkAtivo.IsChecked        := Geral.IntToBool(QrEmailConta.FieldByName('Ativo').AsInteger);
      end else
      begin
        EdNome.Text       := '';
        CkAtivo.IsChecked := True;
      end;
    end;
  end;
end;

end.
