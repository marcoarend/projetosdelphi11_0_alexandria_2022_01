unit ModApp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, Data.FMTBcd,
  System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, Data.DB,
  FMX.StdCtrls, FMX.Layouts, FMX.ListBox,
  //
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.Stan.Error, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Comp.Client, FireDAC.Comp.DataSet, FireDAC.Comp.UI,
  FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, FireDAC.Phys.SQLite,

  UnDmkDevice, ModGerl, UnDmkEnums, UnApp_Vars, UnGrl_Vars,
  FireDAC.Phys.SQLiteDef;

type
  TDmModApp = class(TForm)
    ListBox1: TListBox;
    BindingsList1: TBindingsList;
    BindSourceDB1: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    TbControle: TFDTable;
    FDAppDB: TFDConnection;
    QrUpd: TFDQuery;
    QrAux: TFDQuery;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    TbControleDtaSincro: TDateTimeField;
    TbControleCodigo: TIntegerField;
    FDConnection1: TFDConnection;
    QrControle: TFDQuery;
    procedure FormCreate(Sender: TObject);
    procedure FDAppDBBeforeConnect(Sender: TObject);
    procedure FDAppDBAfterConnect(Sender: TObject);
    procedure TbControleAfterOpen(DataSet: TDataSet);
  private
    procedure OnIdle(Sender: TObject; var FDone: Boolean);
    { Private declarations }
  public
    { Public declarations }
    function  ObtemCamposRegUni_2(const Form: TForm; const Tabela, IdxFld:
              String; IdxVal: Integer; const Campo1, Campo2: String;
              var Valor1, Valor2: Variant; const InfoNotGet: Boolean): Boolean;
    procedure ObtemInfoRastreamento(var DataHora: String; var Latitude,
              Longitude: Double; var DeviceID: String; EntiCad, UserWeb: Integer);

    procedure ReopenControle();
    procedure Teste001(Form: TForm);
  end;

var
  DmModApp: TDmModApp;

implementation

uses
  IOUtils, UnGeral, UnDmkRDBMs, UnDmkDAC, Principal, UnSQLite_PF;

{$R *.fmx}

procedure TDmModApp.FDAppDBAfterConnect(Sender: TObject);
begin
  FDAppDB.ExecSQL (Geral.ATS([
    'CREATE TABLE IF NOT EXISTS controle (',
    'Codigo INT(11) PRIMARY KEY NOT NULL DEFAULT 0, ',
    'Versao BIGINT(20) NOT NULL DEFAULT 0 , ',
    'DtaSincro datetime DEFAULT ''0000-00-00 00:00:00'', ',
    'Perfis int(11) DEFAULT 0, ',
    'Senhas int(11) DEFAULT 0, ',
     // mais aqui...
     // ....
     //
    'Lk int(11) DEFAULT ''0'', ',
    'DataCad date, ',
    'DataAlt date, ',
    'UserCad int(11) DEFAULT ''0'', ',
    'UserAlt int(11) DEFAULT ''0'', ',
    'AlterWeb tinyint(1) NOT NULL  DEFAULT 1, ',
    'Ativo tinyint(1) NOT NULL  DEFAULT 1',
    ');']));
  DmkRDBMs.ReopenDataset(Self, TbControle);
end;

procedure TDmModApp.FDAppDBBeforeConnect(Sender: TObject);
begin
  FDAppDB.Params.Values['Database'] := VAR_APP_DB_FULLPATH;
end;

procedure TDmModApp.FormCreate(Sender: TObject);
begin
  // Evitar uso. Criado ao iniciar o app.
  VAR_SQLITE_DB_LOCAL_SERVER := FDAppDB;
  VAR_SQLITE_DB_LOCAL_USER := nil;
  VAR_SQLITE_DB_WEB_SERVER := nil;
  VAR_SQLITE_DB_WEB_USER := nil;
  try
    // For unidirectional dataset, don't refill automatically when dataset is activated
    // because dataset is reactivated everytime use DataSet.First.
    LinkFillControlToField1.AutoActivate := False;
    LinkFillControlToField1.AutoFill := False;
    Application.OnIdle := OnIdle;
    FDAppDB.Connected := True;
    TbControle.Connection := FDAppDB;
    TbControle.Active := True;
    LinkFillControlToField1.BindList.FillList;
  except
    on e: Exception do
    begin
      SHowMessage(e.Message);
    end;
  end;
end;

function TDmModApp.ObtemCamposRegUni_2(const Form: TForm; const Tabela, IdxFld:
String; IdxVal: Integer; const Campo1, Campo2: String; var Valor1, Valor2:
Variant; const InfoNotGet: Boolean): Boolean;
var
  Qry: TFDQuery;
begin
  //Result := False;
  Valor1 := Null;
  Valor2 := Null;
  Qry := TFDQuery.Create(DmModApp);
  try
    DmkRDBMs.AbreSQLQuery0(Self, Qry, DmModApp.FDAppDB, [
    'SELECT ' + Campo1 + ', ' + Campo2 + sLineBreak,
    'FROM ' + Tabela,
    'WHERE ' + IdxFld + '=' + Geral.FF0(IdxVal),
    '']);
    //
    Result := Qry.RecordCount > 0;
    //
    if Result then
    begin
      Valor1 := Qry.FieldByName(Campo1).Value;
      Valor2 := Qry.FieldByName(Campo2).Value;
    end else
    begin
      if InfoNotGet then
        Geral.MB_Erro('N�o foi poss�vel obter valores da registro!');
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmModApp.ObtemInfoRastreamento(var DataHora: String; var Latitude,
Longitude: Double; var DeviceID: String; EntiCad, UserWeb: Integer);
begin
  DataHora  := Geral.FDT(DmModGerl.ObtemAgora, 109);
  Latitude  := VAR_LATITUDE;
  Longitude := VAR_LONGITUDE;
  DeviceID  := DmkDevice.ObtemIMEI_MD5();
  EntiCad   := VAR_AGENTE_LOGADO;
  UserWeb   := VAR_WEB_USER_ID;
end;

procedure TDmModApp.OnIdle(Sender: TObject; var FDone: Boolean);
begin
  //btnDelete.Visible := ListBox1.Selected <> nil;
end;

procedure TDmModApp.ReopenControle;
begin
  SQLite_PF.AbreSQLQuery0(Self, QrControle, FDAppDB, [
    'SELECT * FROM controle WHERE Codigo=1',
    '']);
end;

////////////////////////////////////////////////////////////////////////////////
//  USAR AO CRIAR O DermaDB para Mobile
////////////////////////////////////////////////////////////////////////////////
procedure TDmModApp.TbControleAfterOpen(DataSet: TDataSet);
begin
  if TbControle.RecordCount = 0 then
  begin
    QrUpd.SQL.Text := 'INSERT INTO controle (Codigo) VALUES (1);';
    QrUpd.ExecSQL();
    TbControle.Refresh;
    LinkFillControlToField1.BindList.FillList;
  end;
end;

procedure TDmModApp.Teste001(Form: TForm);
(*
var
  Qry: TFDQuery;
*)
begin
(*
  Qry := TFDQuery.Create(DmModApp);
  try
    DmkSQL_PF.AbreSQLQuery0(Form, Qry, FDAppDB, [
    'SELECT name FROM sqlite_master ',
    'WHERE type=''table''',
    'ORDER BY name;',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Geral.MB_Info('Tabela ' + IntToStr(Qry.RecNo) + ': ' +
      Qry.FieldByName('name').AsString);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
*)
end;

end.
