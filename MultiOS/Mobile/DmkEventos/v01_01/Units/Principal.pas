unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.TabControl, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Edit, FMX.SearchBox,
  {$IF DEFINED(ANDROID)} Android.BarcodeScanner, {$ENDIF}
  FMX.Effects, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, Data.DB, FireDAC.Comp.Client,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.FMXUI.Wait, FireDAC.Comp.UI, FMX.ScrollBox,
  FMX.Memo, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope,
  System.Actions, FMX.ActnList, FMX.Grid, Fmx.Bind.Grid, Data.Bind.Grid;

type
  TFmPrincipal = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    SearchBox1: TSearchBox;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SBEdita: TSpeedButton;
    SBConferido: TSpeedButton;
    SbVolta: TSpeedButton;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBIConvite: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    LBIEmail: TListBoxItem;
    LBITel1: TListBoxItem;
    LBITel2: TListBoxItem;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBIRespons: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    LBIPago: TListBoxItem;
    ListBoxGroupHeader8: TListBoxGroupHeader;
    LBIValor: TListBoxItem;
    ListBoxGroupHeader9: TListBoxGroupHeader;
    LBIConferido: TListBoxItem;
    ListBoxGroupHeader10: TListBoxGroupHeader;
    LBISituacao: TListBoxItem;
    SBDevolvido: TSpeedButton;
    LBMenu: TListBox;
    LBISobre: TListBoxItem;
    LBISincronizar: TListBoxItem;
    ShadowEffect1: TShadowEffect;
    LBIVerificaDB: TListBoxItem;
    ListBoxHeader1: TListBoxHeader;
    SBQrCode: TSpeedButton;
    SBMenu: TSpeedButton;
    MyDB: TFDConnection;
    QrLoc: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    CBEventos: TComboBox;
    QrControle: TFDQuery;
    QrUsuarios: TFDQuery;
    LBIRegistro: TListBoxItem;
    Label1: TLabel;
    QrWEventos: TFDQuery;
    QrWEventosCodigo: TIntegerField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    QrWEventosNome: TStringField;
    QrWEventosIt: TFDQuery;
    QrWEventosItNome: TStringField;
    QrWEventosItConvite: TWideStringField;
    BtRefresh: TSpeedButton;
    BindSourceDB2: TBindSourceDB;
    LinkFillControlToField2: TLinkFillControlToField;
    QrWEventosItControle: TIntegerField;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    QrWEventosItVis: TFDQuery;
    ClearEditButton1: TClearEditButton;
    TabItem4: TTabItem;
    ChangeTabAction4: TChangeTabAction;
    EditButton1: TEditButton;
    QrWEventosItRespons_TXT: TStringField;
    LinkFillControlToField3: TLinkFillControlToField;
    QrWEventosItConvite_Nome: TWideStringField;
    Layout1: TLayout;
    ListBox3: TListBox;
    ListBoxItem1: TListBoxItem;
    EdNome: TEdit;
    ClearEditButton2: TClearEditButton;
    ListBoxItem3: TListBoxItem;
    EdTe1: TEdit;
    ClearEditButton3: TClearEditButton;
    Label2: TLabel;
    ListBoxItem4: TListBoxItem;
    EdTe2: TEdit;
    ClearEditButton4: TClearEditButton;
    Label3: TLabel;
    ListBoxItem2: TListBoxItem;
    EdEmail: TEdit;
    ClearEditButton5: TClearEditButton;
    ListBoxItem5: TListBoxItem;
    CBPago: TComboBox;
    ListBoxItem6: TListBoxItem;
    CBVendido: TComboBox;
    Conferido: TListBoxItem;
    CBConferido: TComboBox;
    ListBoxItem7: TListBoxItem;
    EdValor: TEdit;
    ClearEditButton6: TClearEditButton;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    Layout2: TLayout;
    ListBox5: TListBox;
    ListBoxHeader5: TListBoxHeader;
    LaResPesq: TLabel;
    ListBox4: TListBox;
    ListBoxItem15: TListBoxItem;
    EdNomePesq: TEdit;
    ClearEditButton11: TClearEditButton;
    ListBoxItem12: TListBoxItem;
    EdResponsPesq: TEdit;
    ClearEditButton8: TClearEditButton;
    ListBoxItem11: TListBoxItem;
    CBPagoPesq: TComboBox;
    ListBoxItem10: TListBoxItem;
    CBVendidoPesq: TComboBox;
    ListBoxItem9: TListBoxItem;
    CBConferidoPesq: TComboBox;
    ListBoxHeader4: TListBoxHeader;
    SpeedButton1: TSpeedButton;
    SBPesq: TSpeedButton;
    LinkFillControlToField4: TLinkFillControlToField;
    LaNome: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure SBQrCodeClick(Sender: TObject);
    procedure SBMenuClick(Sender: TObject);
    procedure LBISobreClick(Sender: TObject);
    procedure LBIVerificaDBClick(Sender: TObject);
    procedure LBIRegistroClick(Sender: TObject);
    procedure LBISincronizarClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure CBEventosChange(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure TabControl1Change(Sender: TObject);
    procedure SBDevolvidoClick(Sender: TObject);
    procedure SBConferidoClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdTe1Exit(Sender: TObject);
    procedure EdTe2Exit(Sender: TObject);
    procedure EditButton1Click(Sender: TObject);
    procedure SBPesqClick(Sender: TObject);
    procedure Grid1SelChanged(Sender: TObject);
    procedure ListBox5ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure SearchBox1Change(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FKBBounds: TRectF;
    FCodigo, FControle: Integer;
    FNeedOffset, FNEdita: Boolean;
    {$IF DEFINED(ANDROID)}
    QrCode: TAndroidBarcodeScanner;
    procedure DisplayBarcode(Sender: TAndroidBarcodeScanner; ABarcode: string);
    {$ENDIF}
    procedure ConfiguraBD();
    procedure MostraVerifyDB(Verifica: Boolean);
    procedure ConfiguraDispositivo();
    procedure ReopenWEventos;
    procedure ReopenWEventosIt(Codigo: Integer; Convite: String);
    procedure ReopenWEventosItVis(Controle: Integer);
  public
    { Public declarations }
    procedure ReopenControle();
    procedure ReopenUsuarios();
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses Sobre, UnAllOS_DmkProcFunc, VerifyDB, UnAllOS_DmkDB, DmkLogin, MyListas,
  UnGrl_Vars, SincroDB, UnGeral, UnDmkEnums;

{$R *.fmx}

{$IF DEFINED(ANDROID)}
procedure TFmPrincipal.DisplayBarcode(Sender: TAndroidBarcodeScanner;
  ABarcode: string);
begin
  SearchBox1.Text := stringReplace(ABarcode, sLineBreak, ' ', [rfReplaceAll]);
end;
{$ENDIF}

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  ReopenWEventos;
end;

procedure TFmPrincipal.CBEventosChange(Sender: TObject);
begin
  if CBEventos.ItemIndex > -1 then
    FCodigo := AllOS_dmkPF.GetSelectedValue(CBEventos).ToString.ToInteger();
end;

procedure TFmPrincipal.ConfiguraBD;
var
  Versao: Integer;
begin
  AllOS_dmkDB.ConfiguraDB(MyDB, istSQLite, CO_DBNome);

  Versao := AllOS_dmkDB.ObtemVersaoAppDB(Self, FmPrincipal.QrLoc,
              FmPrincipal.MyDB, istSQLite);

  if Versao < CO_VERSAO then
    MostraVerifyDB(True)
  else
    ReopenWEventos;
end;

procedure TFmPrincipal.ConfiguraDispositivo;
var
  Res: Boolean;
begin
  ReopenControle;
  ReopenUsuarios;
  //
  Geral.DefineFormatacoes;
  //
  if (QrControle.RecordCount > 0) and (QrUsuarios.RecordCount > 0) then
    Res := True
  else
    Res := False;
  //
  if not Res then
  begin
    VAR_WEB_USER_ID     := 0;
    VAR_WEB_TOKEN_DMK   := '';
    LBIRegistro.Enabled := True;
  end else
    LBIRegistro.Enabled := False;
end;

procedure TFmPrincipal.EditButton1Click(Sender: TObject);
begin
  if CBEventos.ItemIndex < 0 then
    Geral.MB_Aviso('Voc� deve selecionar o evento!')
  else
    ExecuteAction(ChangeTabAction4);
end;

procedure TFmPrincipal.EdTe1Exit(Sender: TObject);
var
  Tel: String;
begin
  Tel        := EdTe1.Text;
  EdTe1.Text := Geral.SoNumero_TT(Tel);
end;

procedure TFmPrincipal.EdTe2Exit(Sender: TObject);
var
  Tel: String;
begin
  Tel        := EdTe2.Text;
  EdTe2.Text := Geral.SoNumero_TT(Tel);
end;

procedure TFmPrincipal.EdValorExit(Sender: TObject);
var
  Valor: Double;
begin
  Valor        := Geral.DMV(EdValor.Text);
  EdValor.Text := Geral.FFT(Valor, 2, siPositivo);
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  LBMenu.Visible          := False;
  TabControl1.TabIndex    := 0;

  FCodigo   := 0;
  FControle := 0;

  {$IF DEFINED(ANDROID)}
  FmPrincipal.FullScreen := True;
  SBQrCode.Visible       := True;

  QrCode           := TAndroidBarcodeScanner.Create(true);
  QrCode.OnBarcode := DisplayBarcode;
  {$ELSE}
  FmPrincipal.FullScreen := False;
  SBQrCode.Visible       := False;
  {$ENDIF}
  //Configurando o banco de dados
  ConfiguraBD;

  ConfiguraDispositivo();
end;

procedure TFmPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack  then
  begin
    if Geral.MB_Pergunta('Deseja sair do aplicativo?') <> mrYes then
      Key := 0;
  end;
end;

procedure TFmPrincipal.Grid1SelChanged(Sender: TObject);
begin
  Geral.MB_Aviso('Controle = ' + Geral.FF0(QrWEventosItControle.Value));
end;

procedure TFmPrincipal.LBIRegistroClick(Sender: TObject);
begin
  if (VAR_WEB_USER_ID = 0) and (VAR_WEB_TOKEN_DMK = '') then
  begin
    AllOS_dmkPF.CriaFm_AllOS(TFmDmkLogin, FmDmkLogin, True);
    LBMenu.Visible := False;
  end else
    Geral.MB_Aviso('Dispositivo j� registrado!');
end;

procedure TFmPrincipal.LBISincronizarClick(Sender: TObject);
begin
  AllOS_dmkPF.CriaFm_AllOS(TFmSincroDB, FmSincroDB);
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBISobreClick(Sender: TObject);
begin
  AllOS_dmkPF.CriaFm_AllOS(TFmSobre, FmSobre);
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBIVerificaDBClick(Sender: TObject);
begin
  MostraVerifyDB(False);
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.Selectable then
  begin
    FControle       := AllOS_dmkPF.GetSelectedValue(ListBox1);
    FNEdita         := False;
    SearchBox1.Text := '';

    if FControle <> 0 then
    begin
      QrWEventosIt.Close;
      ExecuteAction(ChangeTabAction2);
    end;
  end;
end;

procedure TFmPrincipal.ListBox5ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.Selectable then
  begin
    SearchBox1.Text := '';
    FControle       := AllOS_dmkPF.GetSelectedValue(ListBox5);
    FNEdita         := False;

    if FControle <> 0 then
    begin
      QrWEventosIt.Close;
      ExecuteAction(ChangeTabAction2);
    end;
  end;
end;

procedure TFmPrincipal.MostraVerifyDB(Verifica: Boolean);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FullScreen := True;
  FmVerifyDB.Show;
  {$ELSE}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FullScreen := False;
  FmVerifyDB.ShowModal;
  FmVerifyDB.Destroy;
  {$ENDIF}
  //
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.ReopenControle;
begin
  AllOS_dmkDB.AbreSQLQuery0(Self, QrControle, MyDB, [
    'SELECT Device, Versao ',
    'FROM controle ',
    '']);
  if QrControle.RecordCount > 0 then
    VAR_WEB_DEVICE_ID   := QrControle.FieldByName('Device').AsInteger;
end;

procedure TFmPrincipal.ReopenUsuarios;
begin
  AllOS_dmkDB.AbreSQLQuery0(Self, QrUsuarios, MyDB, [
    'SELECT Codigo, Tipo, UsrEnt, UsrID, Token ',
    'FROM Usuarios ',
    '']);
  if QrUsuarios.RecordCount > 0 then
  begin
    VAR_WEB_USER_ID     := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USER_USRENT := QrUsuarios.FieldByName('UsrEnt').AsInteger;
    VAR_WEB_USER_USRID  := QrUsuarios.FieldByName('UsrID').AsInteger;
    VAR_WEB_USER_TIPO   := QrUsuarios.FieldByName('Tipo').AsInteger;
    VAR_WEB_TOKEN_DMK   := QrUsuarios.FieldByName('Token').AsString;
  end;
end;

procedure TFmPrincipal.ReopenWEventos;
begin
  AllOS_dmkDB.AbreSQLQuery0(Self, QrWEventos, MyDB, [
    'SELECT Codigo, Nome ',
    'FROM weventos ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmPrincipal.ReopenWEventosIt(Codigo: Integer; Convite: String);
begin
  AllOS_dmkDB.AbreSQLQuery0(Self, QrWEventosIt, MyDB, [
    'SELECT its.Controle, (its.SecurityStr || its.Codigo || its.Controle) AS Convite, ',
    '(its.SecurityStr || its.Codigo || its.Controle || " " || its.Nome) AS Convite_Nome, ',
    'its.Nome, res.Nome Respons_TXT ',
    'FROM weventosit its ',
    'LEFT JOIN weventosre res ON res.Codigo = its.Respons ',
    'WHERE its.Codigo=' + Geral.FF0(Codigo),
    'AND Convite LIKE "%'+ Convite +'%" ',
    'ORDER BY its.Controle ',
    '']);
end;

procedure TFmPrincipal.ReopenWEventosItVis(Controle: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(Self, QrWEventosItVis, MyDB, [
    'SELECT (wit.SecurityStr || wit.Codigo || wit.Controle) AS Convite, ',
    'wit.Nome, wit.Email, wit.Tel1, wit.Tel2, wit.Conferido, ',
    'res.Nome Respons_TXT, wit.Vendido, wit.Pago, ',
    'wit.Valor AS ValorPg, wev.Valor AS ValorCon',
    'FROM weventosit wit',
    'LEFT JOIN weventos wev ON wev.Codigo = wit.Codigo ',
    'LEFT JOIN weventosre res ON res.Codigo = wit.Respons ',
    'WHERE wit.Controle=' + Geral.FF0(Controle),
    '']);
end;

procedure TFmPrincipal.SBConferidoClick(Sender: TObject);
begin
  if FControle = 0 then
  begin
    Geral.MB_Erro('Falha ao obter ID!');
    Exit;
  end;

  if AllOS_dmkDB.CarregaSQLInsUpd(Self, QrLoc, MyDB, stUpd, 'weventosit', False,
    ['Vendido', 'Pago', 'Conferido'], ['Controle'],
    [1, 1, 1], [FControle], True, False, '', stMobile, True) then
  begin
    ExecuteAction(ChangeTabAction1);
  end else
    Geral.MB_Erro('Falha ao atualizar convite!');
end;

procedure TFmPrincipal.SbConfirmaClick(Sender: TObject);
var
  Nome, Tel1, Tel2, Email: String;
  Pago, Vendido, Conferido: Integer;
  Valor: Double;
begin
  Nome      := EdNome.Text;
  Tel1      := Geral.FormataTelefone_TT_Curto(EdTe1.Text);
  Tel2      := Geral.FormataTelefone_TT_Curto(EdTe2.Text);
  Email     := EdEmail.Text;
  Pago      := CBPago.ItemIndex;
  Vendido   := CBVendido.ItemIndex;
  Conferido := CBConferido.ItemIndex;
  Valor     := Geral.DMV(EdValor.Text);

  if Vendido = 2 then
    Pago := 0;

  if FControle = 0 then
  begin
    Geral.MB_Erro('Falha ao obter ID!');
    Exit;
  end;

  if AllOS_dmkDB.CarregaSQLInsUpd(Self, QrLoc, MyDB, stUpd, 'weventosit', False,
    ['Nome', 'Tel1', 'Tel2', 'Email', 'Vendido', 'Pago', 'Conferido', 'Valor'],
    ['Controle'],
    [Nome, Tel1, Tel2, Email, Vendido, Pago, Conferido, Valor],
    [FControle], True, False, '', stMobile, True) then
  begin
    ExecuteAction(ChangeTabAction1);
  end else
    Geral.MB_Erro('Falha ao atualizar convite!');
end;

procedure TFmPrincipal.SBDevolvidoClick(Sender: TObject);
begin
  if FControle = 0 then
  begin
    Geral.MB_Erro('Falha ao obter ID!');
    Exit;
  end;

  if AllOS_dmkDB.CarregaSQLInsUpd(Self, QrLoc, MyDB, stUpd, 'weventosit', False,
    ['Vendido', 'Pago', 'Conferido', 'Valor'], ['Controle'],
    [2, 0, 1, 0], [FControle],
    True, False, '', stMobile, True) then
  begin
    ExecuteAction(ChangeTabAction1);
  end else
    Geral.MB_Erro('Falha ao atualizar convite!');
end;

procedure TFmPrincipal.SBMenuClick(Sender: TObject);
begin
  AllOS_dmkPF.Configura_PopUpDeBotao_AllOS(LBMenu, SBMenu);
end;

procedure TFmPrincipal.SBPesqClick(Sender: TObject);
var
  Nome, Respons, SQL_Conferido, SQL_Situacao, SQL_Pago, SQL_Respons, SQL_Nome: String;
  Conferido, Situacao, Pago: Integer;
begin
  if FCodigo = 0 then
  begin
    Geral.MB_Erro('Falha ao obter ID!');
    Exit;
  end;

  Conferido := CBConferidoPesq.ItemIndex;
  Situacao  := CBVendidoPesq.ItemIndex;
  Pago      := CBPagoPesq.ItemIndex;
  Respons   := EdResponsPesq.Text;
  Nome      := EdNomePesq.Text;

  if Nome <> '' then
    SQL_Nome := 'AND its.Nome LIKE "%'+ Nome +'%"'
  else
    SQL_Nome := '';

  if Respons <> '' then
    SQL_Respons := 'AND res.Nome LIKE "%'+ Respons +'%"'
  else
    SQL_Respons := '';

  if Pago > 0 then
    SQL_Pago := 'AND its.Pago = ' + Geral.FF0(Pago - 1)
  else
    SQL_Pago := '';

  if Situacao > 0 then
    SQL_Situacao := 'AND its.Vendido = ' + Geral.FF0(Situacao - 1)
  else
    SQL_Situacao := '';

  if Conferido > 0 then
    SQL_Conferido := 'AND its.Conferido = ' + Geral.FF0(Conferido - 1)
  else
    SQL_Conferido := '';

  AllOS_dmkDB.AbreSQLQuery0(Self, QrWEventosIt, MyDB, [
    'SELECT its.Controle, (its.SecurityStr || its.Codigo || its.Controle) AS Convite, its.Nome, ',
    '(its.SecurityStr || its.Codigo || its.Controle || " " || its.Nome) AS Convite_Nome, ',
    'res.Nome Respons_TXT ',
    'FROM weventosit its ',
    'LEFT JOIN weventosre res ON res.Codigo = its.Respons',
    'WHERE its.Codigo=' + Geral.FF0(FCodigo),
    SQL_Nome,
    SQL_Respons,
    SQL_Pago,
    SQL_Situacao,
    SQL_Conferido,
    'ORDER BY its.Controle ',
    '']);

  LaResPesq.Text := 'Resultado pesquisa: ' + Geral.FF0(QrWEventosIt.RecordCount);
end;

procedure TFmPrincipal.SBQrCodeClick(Sender: TObject);
begin
  {$IF DEFINED(ANDROID)}
  QrCode.Scan;
  {$ENDIF}
end;

procedure TFmPrincipal.SearchBox1Change(Sender: TObject);
var
  Txt: String;
begin
  if FCodigo <> 0 then
  begin
    Txt := SearchBox1.Text;

    if Length(Txt) > 1 then
      ReopenWEventosIt(FCodigo, Txt);
  end;
end;

procedure TFmPrincipal.TabControl1Change(Sender: TObject);
var
  Vendido, Pago, Conferido: Integer;
  Vendido_Txt, Pago_Txt, Conferido_Txt: String;
  ValorPg, ValorCon, Valor: Double;
begin
  LBMenu.Visible := False;

  case TabControl1.TabIndex of
    0: //Itens
    begin
      QrWEventosIt.Close;
    end;
    1: //Visualiza
    begin
      ReopenWEventosItVis(FControle);

      Vendido   := QrWEventosItVis.FieldByName('Vendido').AsInteger;
      Pago      := QrWEventosItVis.FieldByName('Pago').AsInteger;
      Conferido := QrWEventosItVis.FieldByName('Conferido').AsInteger;
      ValorPg   := QrWEventosItVis.FieldByName('ValorPg').AsFloat;
      ValorCon  := QrWEventosItVis.FieldByName('ValorCon').AsFloat;

      case Vendido of
        0:
          Vendido_Txt := 'N�o vendido';
        1:
          Vendido_Txt := 'Vendido';
        2:
          Vendido_Txt := 'Devolvido';
        else
          Vendido_Txt := 'N�o catalogado';
      end;

      if Pago = 0 then
      begin
        LBIPago.TextSettings.FontColor := TAlphaColorRec.Red;
        Pago_Txt := 'N�o';
      end else
      begin
        LBIPago.TextSettings.FontColor := TAlphaColorRec.Green;
        Pago_Txt := 'Sim';
      end;

      if Conferido = 0 then
        Conferido_Txt := 'N�o'
      else
        Conferido_Txt := 'Sim';

      if ValorPg > 0 then
        Valor := ValorPg
      else
        Valor := ValorCon;

      if (Vendido = 2) or (Conferido = 1) then
        SBDevolvido.Visible := False
      else
        SBDevolvido.Visible := True;

      if Conferido = 1 then
        SBConferido.Visible := False
      else
        SBConferido.Visible := True;

      LBIConvite.Text   := QrWEventosItVis.FieldByName('Convite').AsString;
      LBISituacao.Text  := Vendido_Txt;
      LBIPago.Text      := Pago_Txt;
      LBINome.Text      := QrWEventosItVis.FieldByName('Nome').AsString;
      LBIEmail.Text     := QrWEventosItVis.FieldByName('Email').AsString;
      LBITel1.Text      := QrWEventosItVis.FieldByName('Tel1').AsString;
      LBITel2.Text      := QrWEventosItVis.FieldByName('Tel2').AsString;
      LBIRespons.Text   := QrWEventosItVis.FieldByName('Respons_TXT').AsString;
      LBIValor.Text     := Geral.FFT(Valor, 2, siNegativo);
      LBIConferido.Text := Conferido_Txt;
    end;
    2: //Edita
    begin
      ReopenWEventosItVis(FControle);

      ValorPg   := QrWEventosItVis.FieldByName('ValorPg').AsFloat;
      ValorCon  := QrWEventosItVis.FieldByName('ValorCon').AsFloat;

      if ValorPg > 0 then
        Valor := ValorPg
      else
        Valor := ValorCon;

      LaNome.Text           := QrWEventosItVis.FieldByName('Convite').AsString;
      EdNome.Text           := QrWEventosItVis.FieldByName('Nome').AsString;
      EdTe1.Text            := QrWEventosItVis.FieldByName('Tel1').AsString;
      EdTe2.Text            := QrWEventosItVis.FieldByName('Tel2').AsString;
      EdEmail.Text          := QrWEventosItVis.FieldByName('Email').AsString;
      CBPago.ItemIndex      := QrWEventosItVis.FieldByName('Pago').AsInteger;
      CBVendido.ItemIndex   := QrWEventosItVis.FieldByName('Vendido').AsInteger;
      CBConferido.ItemIndex := QrWEventosItVis.FieldByName('Conferido').AsInteger;
      EdValor.Text          := Geral.FFT(Valor, 2, siNegativo);
    end;
    3: //Pesquisa
    begin
      QrWEventosIt.Close;

      EdNomePesq.Text           := '';
      EdResponsPesq.Text        := '';
      CBPagoPesq.ItemIndex      := 0;
      CBVendidoPesq.ItemIndex   := 0;
      CBConferidoPesq.ItemIndex := 0;

      LaResPesq.Text := 'Resultado pesquisa';
    end;
  end;
end;

end.
