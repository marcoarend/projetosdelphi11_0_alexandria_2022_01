unit MyListas;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnDmkEnums,
  UnMyLinguas;

type
  TMyListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
              var TemControle: TTemControle): Boolean;
    function  CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices:
              TList<TIndices>): Boolean;
    function  CriaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function  CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
  end;

const
  CO_VERSAO = 1506091200;
  CO_DMKID_APP = 40;
  CO_DBNome = 'dmkeventos';

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;

implementation

uses UnGrl_Tabs, WEventos_Tabs;

{ TMyListas }

function TMyListas.CriaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    MyLinguas.AdTbLst(Lista, False, LowerCase('WEventosRe'), '');
    //
    Grl_Tabs.CarregaListaTabelas(Lista);
    WEventos_Tb.CarregaListaTabelas(Lista);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('WEventosRe') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      Grl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WEventos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //Grl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //WEventos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;

function TMyListas.CriaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    //Grl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //WEventos_Tb.CarregaListaSQL(Tabela, FListaSQL);
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    //Grl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //WEventos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('WEventosRe') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    begin
      Grl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WEventos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    Grl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WEventos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

end.
