program DmkEventos;

uses
  System.StartUpCopy,
  FMX.Forms,
  DmkLogin in '..\..\..\..\AllOS\Forms\v_02_00\DmkLogin.pas' {FmDmkLogin},
  Android.BarcodeScanner in '..\..\..\..\AllOS\Units\Android\Android.BarcodeScanner.pas',
  Sobre in '..\..\..\..\AllOS\Forms\v_02_00\Sobre.pas' {FmSobre},
  UnAllOS_DmkProcFunc in '..\..\..\..\AllOS\Units\UnAllOS_DmkProcFunc.pas',
  UnAllOS_DmkDevice in '..\..\..\..\AllOS\Units\UnAllOS_DmkDevice.pas',
  MyListas in '..\Units\MyListas.pas',
  UnMyLinguas in '..\..\..\..\AllOS\Listas\UnMyLinguas.pas',
  VerifyDB in '..\..\..\..\AllOS\Forms\v_02_00\VerifyDB.pas' {FmVerifyDB},
  UnAllOS_DmkDB in '..\..\..\..\AllOS\Units\UnAllOS_DmkDB.pas',
  WEventos_Tabs in '..\..\..\..\..\VCL\MDL\WEB\v01_01\Eventos\WEventos_Tabs.pas',
  UnGeral in '..\..\..\..\AllOS\Units\UnGeral.pas',
  UnDmkEnums in '..\..\..\..\AllOS\Units\UnDmkEnums.pas',
  UnValidaIE in '..\..\..\..\AllOS\Units\UnValidaIE.pas',
  UnGrl_Tabs in '..\..\..\..\AllOS\Units\UnGrl_Tabs.pas',
  UnAllOS_DmkWeb in '..\..\..\..\AllOS\Units\UnAllOS_DmkWeb.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnGrl_Vars in '..\..\..\..\AllOS\Units\UnGrl_Vars.pas',
  UnAllOS_NetworkState in '..\..\..\..\AllOS\Units\UnAllOS_NetworkState.pas',
  Android.NetworkState in '..\..\..\..\AllOS\Units\Android\Android.NetworkState.pas',
  iOS.CaptiveNetwork in '..\..\..\..\AllOS\Units\IOS\iOS.CaptiveNetwork.pas',
  iOS.NetworkState in '..\..\..\..\AllOS\Units\IOS\iOS.NetworkState.pas',
  iOS.SCNetworkReachability in '..\..\..\..\AllOS\Units\IOS\iOS.SCNetworkReachability.pas',
  SincroDB in '..\..\..\..\AllOS\Forms\SincroDB.pas' {FmSincroDB};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
