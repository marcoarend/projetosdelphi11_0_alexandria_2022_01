unit UnProjGroup_Consts;

interface

uses
  UnDmkListas, UnDmkEnums, UnAppEnums;

type  // RMIP
  TItensRMIP = (rmipNenhum=0, rmipCapa=1, rmipParecer=2,
    rmipPizzaMonitsResp=3, rmipLinhaEvoluHistoriResp=4,
    rmipPizzaMonitsDep=5, rmipLinhaEvoluHistoriDep=6,
    rmipResumoOSs=7, rmipComprovantesExecucao=8, rmipListagemSituacoesPMVs=9,
    rmipProdutosUtilizados=10, rmipNCTs=11, rmipSPs=12, rmipAgendaOSsPrev=13,
    rmipBarraEvoluHistoriResp=14, rmipBarraEvoluHistoriDep=15,
    rmipCroquiPMVSColoridos=16);
  TAplicacaoRMIPCfgIts = (arciNenhuma=0, arciEmpresa=1, arciCliente=2,
    arciAmbos=3);
  TStatusOSExec = (soseNaoDefinido=0, soseEmSer=1, soseExecutada=2,
    soseCancelada=3);
  TInsUpdVMIPrcExecID = (iuvpei000=000,
    iuvpei001=001, iuvpei002=002, iuvpei003=003, iuvpei004=004, iuvpei005=005,
    iuvpei006=006, iuvpei007=007, iuvpei008=008, iuvpei009=009, iuvpei010=010,
    iuvpei011=011, iuvpei012=012, iuvpei013=013, iuvpei014=014, iuvpei015=015,
    iuvpei016=016, iuvpei017=017, iuvpei018=018, iuvpei019=019, iuvpei020=020,
    iuvpei021=021, iuvpei022=022, iuvpei023=023, iuvpei024=024, iuvpei025=025,
    iuvpei026=026, iuvpei027=027, iuvpei028=028, iuvpei029=029, iuvpei030=030,
    iuvpei031=031, iuvpei032=032, iuvpei033=033, iuvpei034=034, iuvpei035=035,
    iuvpei036=036, iuvpei037=037, iuvpei038=038, iuvpei039=039, iuvpei040=040,
    iuvpei041=041, iuvpei042=042, iuvpei043=043, iuvpei044=044, iuvpei045=045,
    iuvpei046=046, iuvpei047=047, iuvpei048=048, iuvpei049=049, iuvpei050=050,
    iuvpei051=051, iuvpei052=052, iuvpei053=053, iuvpei054=054, iuvpei055=055,
    iuvpei056=056, iuvpei057=057, iuvpei058=058, iuvpei059=059, iuvpei060=060,
    iuvpei061=061, iuvpei062=062, iuvpei063=063, iuvpei064=064, iuvpei065=065,
    iuvpei066=066, iuvpei067=067, iuvpei068=068, iuvpei069=069, iuvpei070=070,
    iuvpei071=071, iuvpei072=072, iuvpei073=073, iuvpei074=074, iuvpei075=075,
    iuvpei076=076, iuvpei077=077, iuvpei078=078, iuvpei079=079, iuvpei080=080,
    iuvpei081=081, iuvpei082=082, iuvpei083=083, iuvpei084=084, iuvpei085=085,
    iuvpei086=086, iuvpei087=087, iuvpei088=088, iuvpei089=089, iuvpei090=090,
    iuvpei091=091, iuvpei092=092, iuvpei093=093, iuvpei094=094, iuvpei095=095,
    iuvpei096=096, iuvpei097=097, iuvpei098=098, iuvpei099=099, iuvpei100=100,
    iuvpei101=101, iuvpei102=102, iuvpei103=103, iuvpei104=104, iuvpei105=105,
    iuvpei106=106, iuvpei107=107, iuvpei108=108, iuvpei109=109, iuvpei110=110,
    iuvpei111=111, iuvpei112=112, iuvpei113=113, iuvpei114=114, iuvpei115=115,
    iuvpei116=116, iuvpei117=117, iuvpei118=118, iuvpei119=119);

const
  CO_VERMCW: Int64 = 1905160647;
  CO_VERMLA: Int64 = 2112310004;
  //CO_VERM28: Int64 = 2312141402;
  CO_VERM28: Int64 = 2312251732;
  CO_SIGLA_APP = 'BDER';
  CO_DMKID_APP = 2;
  CO_GRADE_APP = False;
  CO_VLOCAL = False;
  CO_EXTRA_LCT_003 = False;
  CO_ADMIN = 'wLfewpwdd45';
  //
const
  CO_CustoMOPc_ZERO = 0;

  CO_TAB_VSMOEnvAvu = 'vsmoenvavu';
  CO_TAB_VSMOEnvEnv = 'vsmoenvenv';
  CO_TAB_VSMOEnvRet = 'vsmoenvret';

  CO_TXT_iuvpei000 = '';

  CO_TXT_iuvpei001 = 'Item de entrada de mat�ria prima';

  CO_TXT_iuvpei002 = 'Cabe�alho de processo de caleiro';
  CO_TXT_iuvpei003 = 'Mat�ria prima origem no processo de caleiro';
  CO_TXT_iuvpei004 = 'Couro PDA origem no processo de caleiro';
  CO_TXT_iuvpei005 = 'Baixa da MP atrav�s do PDA no processo de caleiro';
  CO_TXT_iuvpei006 = 'Gera��o de artigo durante o processo de caleiro';
  CO_TXT_iuvpei007 = 'Baixa de mat�ria prima origem durante o processo de caleiro';
  CO_TXT_iuvpei008 = 'Gera��o de subproduto (UNI) em processo de caleiro';
  CO_TXT_iuvpei009 = 'Gera��o de subproduto (MUL) em processo de caleiro';

  CO_TXT_iuvpei010 = 'Cabe�alho de processo de curtimento';
  CO_TXT_iuvpei011 = 'Mat�ria prima origem no processo de curtimento';
  CO_TXT_iuvpei012 = 'Gera��o autom�tica de couro caleado no curtimento';
  CO_TXT_iuvpei013 = 'Baixa de Couro PDA na gera��o autom�tica de couro caleado no curtimento';
  CO_TXT_iuvpei014 = 'Gera��o de Couro DTA no processo de curtimento';
  CO_TXT_iuvpei015 = 'Baixa de caleado na gera��o do DTA no processo de curtimento';
  CO_TXT_iuvpei016 = 'Gera��o de artigo durante o processo de curtimento';
  CO_TXT_iuvpei017 = 'Baixa de mat�ria prima origem durante processo de curtimento';

  CO_TXT_iuvpei018 = 'Cabe�alho de gera��o de artigo de ribeira';
  CO_TXT_iuvpei019 = 'Gera��o de artigo (UNI) de ribeira de curtimento';
  CO_TXT_iuvpei020 = 'Baixa de mat�ria prima na gera��o de artigo (UNI) de ribeira de curtimento';
  CO_TXT_iuvpei021 = 'Gera��o de artigo (MUL) de ribeira de curtimento';
  CO_TXT_iuvpei022 = 'Baixa de mat�ria prima na gera��o de artigo (MUL) de ribeira de curtimento';
  CO_TXT_iuvpei023 = 'Gera��o de artigo de ribeira direto da barraca';
  CO_TXT_iuvpei024 = 'Baixa de mat�ria prima na gera��o de artigo de ribeira direto da barraca';
  CO_TXT_iuvpei025 = 'Gera��o de artigo de ribeira (Altera��o de In Natura)';
  CO_TXT_iuvpei026 = 'Baixa de mat�ria prima na gera��o de artigo de ribeira (Altera��o de In Natura)';

  CO_TXT_iuvpei027 = 'Classifica��o de artigo de ribeira (UNI) 1/4';
  CO_TXT_iuvpei028 = 'Baixa de artigo de ribeira em classifica��o (UNI) 1/4';
  CO_TXT_iuvpei029 = 'Classifica��o de artigo de ribeira (UNI) 2/4';
  CO_TXT_iuvpei030 = 'Baixa de artigo de ribeira em classifica��o (UNI) 2/4';
  CO_TXT_iuvpei031 = 'Classifica��o de artigo de ribeira (UNI) 3/4';
  CO_TXT_iuvpei032 = 'Baixa de artigo de ribeira em classifica��o (UNI) 3/4';
  CO_TXT_iuvpei033 = 'Classifica��o de artigo de ribeira (UNI) 4/4';
  CO_TXT_iuvpei034 = 'Baixa de artigo de ribeira em classifica��o (UNI) 4/4';

  CO_TXT_iuvpei035 = 'Item de entrada de curtido (em diante) por compra';

  CO_TXT_iuvpei036 = 'Classifica��o de artigo de ribeira (MUL)';
  CO_TXT_iuvpei037 = 'Baixa de artigo de ribeira em classifica��o (MUL)';

  CO_TXT_iuvpei038 = 'Prepara��o de pallet para reclassifica��o 1/2';
  CO_TXT_iuvpei039 = 'Baixa na prepara��o de pallet para reclassifica��o 1/2';
  CO_TXT_iuvpei040 = 'Prepara��o de pallet para reclassifica��o 2/2';
  CO_TXT_iuvpei041 = 'Baixa na prepara��o de pallet para reclassifica��o 2/2';

  CO_TXT_iuvpei042 = 'Reclassifica��o de artigo de ribeira (UNI) 1/4';
  CO_TXT_iuvpei043 = 'Baixa de artigo de ribeira em reclassifica��o (UNI) 1/4';
  CO_TXT_iuvpei044 = 'Reclassifica��o de artigo de ribeira (UNI) 2/4';
  CO_TXT_iuvpei045 = 'Baixa de artigo de ribeira em reclassifica��o (UNI) 2/4';
  CO_TXT_iuvpei046 = 'Reclassifica��o de artigo de ribeira (UNI) 3/4';
  CO_TXT_iuvpei047 = 'Baixa de artigo de ribeira em reclassifica��o (UNI) 3/4';
  CO_TXT_iuvpei048 = 'Reclassifica��o de artigo de ribeira (UNI) 4/4';
  CO_TXT_iuvpei049 = 'Baixa de artigo de ribeira em reclassifica��o (UNI) 4/4';
  CO_TXT_iuvpei050 = 'Reclassifica��o de artigo de ribeira (MUL)';
  CO_TXT_iuvpei051 = 'Baixa de artigo de ribeira em reclassifica��o (MUL)';

  CO_TXT_iuvpei052 = 'Zeramento de estoque de pallet de origem 1/3';
  CO_TXT_iuvpei053 = 'Zeramento de estoque de pallet de origem 2/3';
  CO_TXT_iuvpei054 = 'Zeramento de estoque de pallet de origem 3/3';

  CO_TXT_iuvpei055 = 'Ajuste de estoque';

  CO_TXT_iuvpei056 = 'Entrada de couro sem cobertura';

  CO_TXT_iuvpei057 = 'Baixa for�ada de couro na ribeira 1/2';
  CO_TXT_iuvpei058 = 'Baixa for�ada de couro na ribeira 2/2';

  CO_TXT_iuvpei059 = 'Baixa extra de couro na ribeira 1/2';
  CO_TXT_iuvpei060 = 'Baixa extra de couro na ribeira 2/2';

  CO_TXT_iuvpei061 = 'Corre��o de baixa em reclassifica��o';

  CO_TXT_iuvpei062 = 'Gera��o de couro que est� em opera��o';
  CO_TXT_iuvpei063 = 'Baixa de couro em opera��o por IME-I';
  CO_TXT_iuvpei064 = 'Baixa de couro em opera��o por pallet';
  CO_TXT_iuvpei065 = 'Gera��o de couro resultante de opera��o';
  CO_TXT_iuvpei066 = 'Baixa de couro em opera��o na gera��o de couro resultante de opera��o';

  CO_TXT_iuvpei067 = 'Gera��o de couro que est� em processo';
  CO_TXT_iuvpei068 = 'Baixa de couro em processo por IME-I';
  CO_TXT_iuvpei069 = 'Baixa de couro em processo por pallet';
  CO_TXT_iuvpei070 = 'Gera��o de couro resultante de processo';
  CO_TXT_iuvpei071 = 'Baixa de couro em processo na gera��o de couro resultante de processo';
  CO_TXT_iuvpei072 = 'Alera��o de baixa de couro em processo por IME-I';
  CO_TXT_iuvpei073 = 'Desclassifica��o em processo de semi/acabado';
  CO_TXT_iuvpei074 = 'Baixa de origem na desclassifica��o em processo de semi/acabado';
  CO_TXT_iuvpei075 = 'Gera��o de raspa em processo de semi/acabado';
  CO_TXT_iuvpei076 = 'Baixa de origem na gera��o de raspa em processo de semi/acabado';

  CO_TXT_iuvpei077 = 'Gera��o de couro no destino em transfer�ncia de estoque por IME-I';
  CO_TXT_iuvpei078 = 'Baixa de couro da origem em transfer�ncia de estoque por IME-I';
  CO_TXT_iuvpei079 = 'Gera��o de couro no destino em transfer�ncia de estoque por pallet';
  CO_TXT_iuvpei080 = 'Baixa de couro da origem em transfer�ncia de estoque por pallet';

  CO_TXT_iuvpei081 = 'Gera��o de subproduto em processo em processamento de subproduto';
  CO_TXT_iuvpei082 = 'Baixa de subproduto origem em processamento de subproduto';
  CO_TXT_iuvpei083 = 'Gera��o de subproduto processado em processamento de subproduto';
  CO_TXT_iuvpei084 = 'Baixa de subproduto em processo na gera��o de produto processado em processamento de subproduto';

  CO_TXT_iuvpei085 = 'Entrada por devolu��o definitiva de cliente ';

  CO_TXT_iuvpei086 = 'Entrada por devolu��o de cliente para retrabalho';

  CO_TXT_iuvpei087 = 'Gera��o de couro que est� em reprocesso/reparo';
  CO_TXT_iuvpei088 = 'Baixa de couro em reprocesso/reparo por IME-I';
  CO_TXT_iuvpei089 = 'Baixa de couro em reprocesso/reparo por pallet';
  CO_TXT_iuvpei090 = 'Gera��o de couro resultante de reprocesso/reparo';
  CO_TXT_iuvpei091 = 'Baixa de couro em reprocesso/reparo na gera��o de couro resultante de reprocesso/reparo';

  CO_TXT_iuvpei092 = 'Item de venda de produto com rastreio';
  CO_TXT_iuvpei093 = 'Item de venda de produto por pallet';
  CO_TXT_iuvpei094 = 'Item de venda de produto por peso';
  CO_TXT_iuvpei095 = 'Altera��o de item de venda de produto ';

  CO_TXT_iuvpei096 = '';
  CO_TXT_iuvpei097 = '';
  CO_TXT_iuvpei098 = '';
  CO_TXT_iuvpei099 = '';
  CO_TXT_iuvpei100 = '';
  CO_TXT_iuvpei101 = '';
  CO_TXT_iuvpei102 = '';
  CO_TXT_iuvpei103 = '';
  CO_TXT_iuvpei104 = '';
  CO_TXT_iuvpei105 = '';
  CO_TXT_iuvpei106 = '';
  CO_TXT_iuvpei107 = '';
  CO_TXT_iuvpei108 = '';
  CO_TXT_iuvpei109 = '';
  CO_TXT_iuvpei110 = '';
  CO_TXT_iuvpei111 = 'Gera��o de artigo durante conserva��o de couro in natura';
  CO_TXT_iuvpei112 = 'Mat�ria prima origem na conserva��o';
  CO_TXT_iuvpei113 = 'Gera��o de artigo na conserva��o';
  CO_TXT_iuvpei114 = 'Baixa de mat�ria prima origem na conserva��o';
  CO_TXT_iuvpei115 = 'Entrada excedente de couro';
  CO_TXT_iuvpei116 = 'Gera��o de artigo (MUL de MUL) de ribeira de curtimento';
  CO_TXT_iuvpei117 = 'Baixa de mat�ria prima na gera��o de artigo (MUL de MUL) de ribeira de curtimento';
  CO_TXT_iuvpei118 = 'Gera��o de artigo (IME-C) de ribeira de curtimento';
  CO_TXT_iuvpei119 = 'Baixa de mat�ria prima na gera��o de artigo (IMEC) de ribeira de curtimento';

  MaxIuvpei = 114;
  sListaIuvpeiTxt: array[0..MaxIuvpei] of String = (
    CO_TXT_iuvpei000,
    CO_TXT_iuvpei001,
    CO_TXT_iuvpei002,
    CO_TXT_iuvpei003,
    CO_TXT_iuvpei004,
    CO_TXT_iuvpei005,
    CO_TXT_iuvpei006,
    CO_TXT_iuvpei007,
    CO_TXT_iuvpei008,
    CO_TXT_iuvpei009,
    CO_TXT_iuvpei010,
    CO_TXT_iuvpei011,
    CO_TXT_iuvpei012,
    CO_TXT_iuvpei013,
    CO_TXT_iuvpei014,
    CO_TXT_iuvpei015,
    CO_TXT_iuvpei016,
    CO_TXT_iuvpei017,
    CO_TXT_iuvpei018,
    CO_TXT_iuvpei019,
    CO_TXT_iuvpei020,
    CO_TXT_iuvpei021,
    CO_TXT_iuvpei022,
    CO_TXT_iuvpei023,
    CO_TXT_iuvpei024,
    CO_TXT_iuvpei025,
    CO_TXT_iuvpei026,
    CO_TXT_iuvpei027,
    CO_TXT_iuvpei028,
    CO_TXT_iuvpei029,
    CO_TXT_iuvpei030,
    CO_TXT_iuvpei031,
    CO_TXT_iuvpei032,
    CO_TXT_iuvpei033,
    CO_TXT_iuvpei034,
    CO_TXT_iuvpei035,
    CO_TXT_iuvpei036,
    CO_TXT_iuvpei037,
    CO_TXT_iuvpei038,
    CO_TXT_iuvpei039,
    CO_TXT_iuvpei040,
    CO_TXT_iuvpei041,
    CO_TXT_iuvpei042,
    CO_TXT_iuvpei043,
    CO_TXT_iuvpei044,
    CO_TXT_iuvpei045,
    CO_TXT_iuvpei046,
    CO_TXT_iuvpei047,
    CO_TXT_iuvpei048,
    CO_TXT_iuvpei049,
    CO_TXT_iuvpei050,
    CO_TXT_iuvpei051,
    CO_TXT_iuvpei052,
    CO_TXT_iuvpei053,
    CO_TXT_iuvpei054,
    CO_TXT_iuvpei055,
    CO_TXT_iuvpei056,
    CO_TXT_iuvpei057,
    CO_TXT_iuvpei058,
    CO_TXT_iuvpei059,
    CO_TXT_iuvpei060,
    CO_TXT_iuvpei061,
    CO_TXT_iuvpei062,
    CO_TXT_iuvpei063,
    CO_TXT_iuvpei064,
    CO_TXT_iuvpei065,
    CO_TXT_iuvpei066,
    CO_TXT_iuvpei067,
    CO_TXT_iuvpei068,
    CO_TXT_iuvpei069,
    CO_TXT_iuvpei070,
    CO_TXT_iuvpei071,
    CO_TXT_iuvpei072,
    CO_TXT_iuvpei073,
    CO_TXT_iuvpei074,
    CO_TXT_iuvpei075,
    CO_TXT_iuvpei076,
    CO_TXT_iuvpei077,
    CO_TXT_iuvpei078,
    CO_TXT_iuvpei079,
    CO_TXT_iuvpei080,
    CO_TXT_iuvpei081,
    CO_TXT_iuvpei082,
    CO_TXT_iuvpei083,
    CO_TXT_iuvpei084,
    CO_TXT_iuvpei085,
    CO_TXT_iuvpei086,
    CO_TXT_iuvpei087,
    CO_TXT_iuvpei088,
    CO_TXT_iuvpei089,
    CO_TXT_iuvpei090,
    CO_TXT_iuvpei091,
    CO_TXT_iuvpei092,
    CO_TXT_iuvpei093,
    CO_TXT_iuvpei094,
    CO_TXT_iuvpei095,
    CO_TXT_iuvpei096,
    CO_TXT_iuvpei097,
    CO_TXT_iuvpei098,
    CO_TXT_iuvpei099,
    CO_TXT_iuvpei100,
    CO_TXT_iuvpei101,
    CO_TXT_iuvpei102,
    CO_TXT_iuvpei103,
    CO_TXT_iuvpei104,
    CO_TXT_iuvpei105,
    CO_TXT_iuvpei106,
    CO_TXT_iuvpei107,
    CO_TXT_iuvpei108,
    CO_TXT_iuvpei109,
    CO_TXT_iuvpei110,
    CO_TXT_iuvpei111,
    CO_TXT_iuvpei112,
    CO_TXT_iuvpei113,
    CO_TXT_iuvpei114
  );
  //
  CO_STAT_VSPALLET_0100_DISPONIVEL = 100;
  CO_STAT_VSPALLET_0300_INDICADO   = 300;
  CO_STAT_VSPALLET_0500_RESERVADO  = 500;
  CO_STAT_VSPALLET_0900_PROMETIDO  = 900;


  // Tabelas GraGruY
  //
  CO_GraGruY_GER_VS = '1024, 2048';
  CO_GraGruY_ALL_VS = '1024, 2048, 3072, 4096';
  //


  CO_VS___Cab = 'VS---Cab';
  CO_VSXxxTab = 'VS???Cab';
  CO_TAB_VMI = 'vsmovits';
  CO_TAB_VMB = 'vsmovitb';
  CO_SEL_TAB_VMI = CO_TAB_VMI;
  CO_INS_TAB_VMI = CO_TAB_VMI;
  CO_UPD_TAB_VMI = CO_TAB_VMI;
  CO_DEL_TAB_VMI = CO_TAB_VMI;
  CO_FLD_TAB_VMI = CO_TAB_VMI;
  CO_TAB_TAB_VMI = CO_TAB_VMI;
  //
  CO_SEL_TAB_XMI = CO_TAB_VMI;
  //
  CO_TAB_CRCI = 'crcmovits';
  CO_TAB_CRCB = 'crcmovitb';
  CO_SEL_TAB_CRCI = CO_TAB_CRCI;
  CO_INS_TAB_CRCI = CO_TAB_CRCI;
  CO_UPD_TAB_CRCI = CO_TAB_CRCI;
  CO_DEL_TAB_CRCI = CO_TAB_CRCI;
  CO_FLD_TAB_CRCI = CO_TAB_CRCI;
  CO_TAB_TAB_CRCI = CO_TAB_CRCI;
  //
  CO_TAB_XX_MOV_ID_LOC = 'vsmovidloc';
  //
  CO_DATA_HORA_VMI = 'DataHora';
  CO_DATA_HORA_SMI = 'DataHora';
  CO_DATA_HORA_GRL = 'DataHora';
  CO_TAB_VM_MOV = '_mov_' + CO_TAB_VMI + '_to_b_';
  CO_TAB_PL_LST = '_mov_' + CO_TAB_VMI + '_lista_pall_';
  CO_0_GGXRcl = 0;
  CO_RpICMS = 0.00;
  CO_RpPIS = 0.00;
  CO_RpCOFINS = 0.00;
  CO_RvICMS = 0.00;
  CO_RvPIS = 0.00;
  CO_RvCOFINS = 0.00;
  CO_RpIPI = 0.00;
  CO_RvIPI = 0.00;
  CO_0_JmpMovID: TEstqMovimID = TEstqMovimID.emidAjuste;
  CO_0_JmpNivel1: Integer = 0;
  CO_0_JmpNivel2: Integer = 0;
  CO_0_JmpGGX: Integer = 0;
  CO_0_RmsMovID: TEstqMovimID = TEstqMovimID.emidAjuste;
  CO_0_RmsNivel1: Integer = 0;
  CO_0_RmsNivel2: Integer = 0;
  CO_0_RmsGGX: Integer = 0;
  CO_0_GSPJmpMovID: TEstqMovimID = TEstqMovimID.emidAjuste;
  CO_0_GSPJmpNiv2: Integer = 0;
  CO_0_MovCodPai: Integer = 0;
  CO_0_VmiPai: Integer = 0;
  CO_0_IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef;
  CO_0_IxxFolha: Integer = 0;
  CO_0_IxxLinha: Integer = 0;
  CO_TRUE_ExigeClientMO: Boolean = True;
  CO_TRUE_ExigeFornecMO: Boolean = True;
  CO_TRUE_ExigeStqLoc: Boolean = True;
  CO_FALSE_ExigeClientMO: Boolean = False;
  CO_FALSE_ExigeFornecMO: Boolean = False;
  CO_FALSE_ExigeStqLoc: Boolean = False;
  CO_0_PerceComiss: Integer = 0;
  CO_0_CusKgComiss: Integer = 0;
  CO_0_CustoComiss: Integer = 0;
  CO_0_CredPereImposto: Integer = 0;
  CO_0_CredValrImposto: Integer = 0;
  CO_0_CusFrtAvuls: Integer = 0;

  //
  CO_VS_MaxLin = 24;
  //CO_0_DtCorrApo = 0;

  CO_FLD_TAB_PQX = 'pqx';
  CO_UPD_TAB_PQX = 'pqx';
  CO_INS_TAB_PQX = 'pqx';

  CO_OSWCAB_EVOLUCAO_00000_COD_UPLD_DSKTP_CLOUD_INI = 00000;
  CO_OSWCAB_EVOLUCAO_01000_COD_UPLD_DSKTP_CLOUD_FIM = 01000;
  CO_OSWCAB_EVOLUCAO_02000_COD_DOWN_CLOUD_MOBLE_INI = 02000;
  CO_OSWCAB_EVOLUCAO_03000_COD_DOWN_CLOUD_MOBLE_FIM = 03000;
  CO_OSWCAB_EVOLUCAO_04000_COD_UPLD_MOBLE_CLOUD_INI = 04000;
  CO_OSWCAB_EVOLUCAO_05000_COD_UPLD_MOBLE_CLOUD_FIM = 05000;
  CO_OSWCAB_EVOLUCAO_06000_COD_DOWN_CLOUD_DSKTP_INI = 06000;
  CO_OSWCAB_EVOLUCAO_07000_COD_DOWN_CLOUD_DSKTP_FIM = 07000;

  //
  CO_COD_TAXON_ITS_1  = '1';
  CO_COD_TAXON_ITS_2  = '2';
  CO_COD_TAXON_ITS_3  = '3';
  CO_COD_TAXON_ITS_4  = '4';
  CO_COD_TAXON_ITS_5  = '5';
  CO_COD_TAXON_ITS_6  = '6';
  CO_COD_TAXON_ITS_7  = '7';
  CO_COD_TAXON_ITS_8  = '8';
  CO_COD_TAXON_ITS_9  = '9';
  CO_COD_TAXON_ITS_10 = '10';
  CO_COD_TAXON_ITS_11 = '11';
  CO_COD_TAXON_ITS_12 = '12';
  CO_COD_TAXON_ITS_13 = '13';
  CO_COD_TAXON_ITS_14 = '14';
  CO_COD_TAXON_ITS_15 = '15';
  CO_COD_TAXON_ITS_16 = '16';
  CO_COD_TAXON_ITS_17 = '17';
  CO_COD_TAXON_ITS_18 = '18';
  CO_COD_TAXON_ITS_19 = '19';
  CO_COD_TAXON_ITS_20 = '20';
  CO_COD_TAXON_ITS_21 = '21';
  CO_COD_TAXON_ITS_22 = '22';
  CO_COD_TAXON_ITS_23 = '23';
  CO_COD_TAXON_ITS_24 = '24';
  CO_COD_TAXON_ITS_25 = '25';
  CO_COD_TAXON_ITS_26 = '26';
  CO_COD_TAXON_ITS_27 = '27';
  CO_COD_TAXON_ITS_28 = '28';
  CO_COD_TAXON_ITS_29 = '29';
  CO_COD_TAXON_ITS_30 = '30';
  CO_COD_TAXON_ITS_31 = '31';
  CO_COD_TAXON_ITS_32 = '32';
  CO_COD_TAXON_ITS_33 = '33';
  CO_COD_TAXON_ITS_34 = '34';
  CO_COD_TAXON_ITS_35 = '35';
  CO_COD_TAXON_ITS_36 = '36';
  CO_COD_TAXON_ITS_37 = '37';
  CO_COD_TAXON_ITS_38 = '38';
  CO_COD_TAXON_ITS_39 = '39';
  CO_COD_TAXON_ITS_40 = '40';
  CO_COD_TAXON_ITS_41 = '41';
  CO_COD_TAXON_ITS_42 = '42';
  CO_COD_TAXON_ITS_43 = '43';
  CO_COD_TAXON_ITS_44 = '44';
  CO_COD_TAXON_ITS_45 = '45';
  CO_COD_TAXON_ITS_46 = '46';
  CO_COD_TAXON_ITS_47 = '47';
  CO_COD_TAXON_ITS_48 = '48';
  CO_COD_TAXON_ITS_49 = '49';
  CO_COD_TAXON_ITS_50 = '50';
  CO_COD_TAXON_ITS_51 = '51';
  CO_COD_TAXON_ITS_52 = '52';
  CO_COD_TAXON_ITS_53 = '53';
  CO_COD_TAXON_ITS_54 = '54';
  CO_COD_TAXON_ITS_55 = '55';
  CO_COD_TAXON_ITS_56 = '56';
  CO_COD_TAXON_ITS_57 = '57';
  CO_COD_TAXON_ITS_58 = '58';
  CO_COD_TAXON_ITS_59 = '59';
  CO_COD_TAXON_ITS_60 = '60';
  CO_COD_TAXON_ITS_61 = '61';
  CO_COD_TAXON_ITS_62 = '62';
  CO_COD_TAXON_ITS_63 = '63';
  CO_COD_TAXON_ITS_64 = '64';
  CO_COD_TAXON_ITS_65 = '65';
  CO_COD_TAXON_ITS_66 = '66';
  CO_COD_TAXON_ITS_67 = '67';
  CO_COD_TAXON_ITS_68 = '68';
  CO_COD_TAXON_ITS_69 = '69';
  CO_COD_TAXON_ITS_70 = '70';
  CO_COD_TAXON_ITS_71 = '71';
  CO_COD_TAXON_ITS_72 = '72';
  CO_COD_TAXON_ITS_73 = '73';
  CO_COD_TAXON_ITS_74 = '74';
  CO_COD_TAXON_ITS_75 = '75';
  CO_COD_TAXON_ITS_76 = '76';
  CO_COD_TAXON_ITS_77 = '77';

  //

  CO_BUG_STATUS_0000_ABERTURA = 0000; //|"Abertura de OS"');
  CO_BUG_STATUS_0100_PRE_ATEN = 0100; //|"Pr�-atendimento"');
  CO_BUG_STATUS_0200_VISTORIA = 0200; //|"Vistoria"');
  CO_BUG_STATUS_0300_EM_ORCAM = 0300; //|"Em or�amento"');
  CO_BUG_STATUS_0400_ORCA_PAS = 0400; //|"Or�amento passado"');
  CO_BUG_STATUS_0450_ORCA_OK_ = 0450; //|"Or�amento aprovado"');
  CO_BUG_STATUS_0500_AGEN_EXE = 0500; //|"Agendado a execu��o"');
  CO_BUG_STATUS_0550_AGEN_OK_ = 0550; //|"Confirmado Agendamento"');
  CO_BUG_STATUS_0600_EM_EXECU = 0600; //|"Em execu��o "');
  CO_BUG_STATUS_0700_EXE_FINA = 0700; //|"Execu��o finalizada"');
  CO_BUG_STATUS_0750_CEX_ENVI = 0750; //|"Comprovante Enviado"');
  CO_BUG_STATUS_0800_FATURADO = 0800; //|"Faturado"');
  CO_BUG_STATUS_1000_POS_VEND = 1000; //|"P�s venda"');'
  CO_BUG_STATUS_2000_O_S_BAIX = 2000; //|"OS baixada"');

  //CO_COD_GraTabApp_NemhumGTA = 0; // para todos apps  > est� no UnDmkListas!
  CO_COD_GraTabApp_GraG1EqAp = (*CO_DMKID_APP*)24 * 1000 + 001;
  CO_COD_GraTabApp_GraG1EqMo = (*CO_DMKID_APP*)24 * 1000 + 002;
  CO_COD_GraTabApp_GraG1PrAp = (*CO_DMKID_APP*)24 * 1000 + 003;
  CO_COD_GraTabApp_GraG1PrMo = (*CO_DMKID_APP*)24 * 1000 + 004;
  CO_COD_GraTabApp_GraG1Veic = (*CO_DMKID_APP*)24 * 1000 + 005;

  CO_TXT_GraTabApp_GraG1EqAp = 'Equipamentos de Aplica��o';
  CO_TXT_GraTabApp_GraG1EqMo = 'Equipamentos de Monitoramento';
  CO_TXT_GraTabApp_GraG1PrAp = 'Produtos de Aplica��o';
  CO_TXT_GraTabApp_GraG1PrMo = 'Produtos de Monitoramento';
  CO_TXT_GraTabApp_GraG1Veic = 'Ve�culos';

  MaxGraTabApp = 5;
  sListaGraTabAppCod: array[0..MaxGraTabApp] of Integer = (
  0,//CO_COD_GraTabApp_NemhumGTA,
  CO_COD_GraTabApp_GraG1EqAp,
  CO_COD_GraTabApp_GraG1EqMo,
  CO_COD_GraTabApp_GraG1PrAp,
  CO_COD_GraTabApp_GraG1PrMo,
  CO_COD_GraTabApp_GraG1Veic);

  sListaGraTabAppTxt: array[0..MaxGraTabApp] of String = (
  '',//CO_TXT_GraTabApp_NemhumGTA,
  CO_TXT_GraTabApp_GraG1EqAp,
  CO_TXT_GraTabApp_GraG1EqMo,
  CO_TXT_GraTabApp_GraG1PrAp,
  CO_TXT_GraTabApp_GraG1PrMo,
  CO_TXT_GraTabApp_GraG1Veic);

  CO_TABELA_TIPO_LUGAR_001_SIAPIMADEP = 1;
  CO_TABELA_TIPO_LUGAR_002_MOVAMOVCAD = 2;

  // 0=N�o informado, 1=N�o executado na OS origem, 2=Executado na OS origem
  CO_SRV_HOW_ESTAVA_NAO_INFO = 0;
  CO_SRV_HOW_ESTAVA_NAO_EXEC = 1;
  CO_SRV_HOW_ESTAVA_SIM_EXEC = 2;

  CO_COD_FatoGeradr_INDEF       = 00; // nenhuma a��o
  CO_COD_FatoGeradr_DEPRECADO   = 10; // Deprecado
  CO_COD_FatoGeradr_1aAcao      = 20; // 1� A��o
  CO_COD_FatoGeradr_Corretiva   = 30; // Corretiva
  CO_COD_FatoGeradr_Preventiva  = 40; // Preventiva
  CO_COD_FatoGeradr_Preditiva   = 50; // Preditiva


  CO_FORMUL_FILHA_INICIAL = 1;
  CO_FORMUL_FILHA_POSTERO = 2;
  //
  CO_FORMUL_FILHA_MONIT_EH_FILHA = 1;
  CO_FORMUL_FILHA_MONIT_EH_MONIT = 2;
  //
  CO_FORMUL_FILHA_MONIT_Txt_FILHA = 'APL';
  CO_FORMUL_FILHA_MONIT_Txt_MONIT = 'PIP';
  //
  CO_FORMUL_FILHA_MONIT_EmisStat_ABERTO   = 0;
  CO_FORMUL_FILHA_MONIT_EmisStat_PARCIAL  = 1;
  CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL    = 2;
  CO_FORMUL_FILHA_MONIT_EmisStat_REABERTO = 3;


  // 'Operacao' :: Opera��es realizadas em uma OS
  CO_OS_OPERACAO_SERVICO_DEDETIZACAO     = 1;
  CO_OS_OPERACAO_SERVICO_CAIXADAGUA      = 2;
  (*DEDETIZACAO + CAIXADAGUA             = 3; *)
  CO_OS_OPERACAO_SERVICO_MONITORAMENTO   = 4;
  (*DEDETIZACAO + MONITORAMENTO          = 5; *)
  (*CAIXADAGUA  + MONITORAMENTO          = 6; *)
  (*DEDETIZACAO + CAIXADAGUA + MONITORAM = 7; *)

  // 'HowGerou' :: Como Gerou: 0=Manual, 1=Clonado, 2=Gerada por f�rmulas filhas de OS m�e
  CO_OS_HOWGEROU_OS_MANUAL_USER          = 0; // Manual
  CO_OS_HOWGEROU_OS_CLONANDO_OS          = 1; // Clonado
  CO_OS_HOWGEROU_OS_AUTO_PREVENT         = 2; // OS Mae

  //////////////////////////////  OS automaticas ///////////////////////////////
  //EntiContat, NumContrat, EntPagante, EntContrat, CondicaoPg, CartEmis
  CO_OSFlh_OPT_NAO_DEFINID = 0;
  CO_OSFlh_OPT_DEIXR_VAZIO = 1;
  CO_OSFlh_OPT_DEF_IN_LUGR = 2;
  CO_OSFlh_OPT_FROM_OS_INI = 3;
  //CO_OSFlh_OPT_AGR_AGE_OSs = 4;
  //

  ////////////////////////////  FIM OS automaticas //////////////////////////////

  CO_OSPipIts_NAO_DEF = 0; // Ainda n�o definido
  CO_OSPipIts_SEM_TAB = 1; // Sem tabela, se refere ao pr�prio PIP
  CO_OSPipIts_OSMONREC = 2; // Produtos adicionados na coloca��o do PIP
  CO_OSPipIts_OSPIPITSPR = 3; // Produtos adicionados em monitoramento
  //
  CO_OSPipItsPr_INDEF = 0;
  CO_OSPipItsPr_TROCA = 1;
  CO_OSPipItsPr_INCLU = 2;
  //
  CO_StPipAdPrg_NONE = 0;
  CO_StPipAdPrg_INIT = 1;
  CO_StPipAdPrg_FINI = 2;
  //
  CO_AcaoPrd_Nada = 0;
  CO_AcaoPrd_Adic = 1;
  CO_AcaoPrd_Troc = 2;
  CO_AcaoPrd_Tira = 3;
  //
{ Definido na unit AppListas > CO_PRG_LST_...
  CO_PrgFuncao_Nenhuma = 0; //  1 = Nenhuma            Respondido
  CO_PrgFuncao_Binario = 1; //  2 = Bin�rio            RespBin
  CO_PrgFuncao_Quantit = 2; //  4 = Quantitativo       RespQtd
  CO_PrgFuncao_BxaProd = 3; //  8 = Baixa de Produto   ...multi itens na tabela OSPipItsPr
  CO_PrgFuncao_Atribut = 4; // 16 = Uso de Atributo    RespAtrCad + RespAtrIts + RespAtrTxt
  CO_PrgFuncao_TxtLivr = 5; // 32 = Texto Livre        RespTxtLvr
}
  //
  CO_Respondido_Nao = 0;
  CO_Respondido_SIM = 1;
  CO_Respondido_N_A = 2; // Nao aplicavel
  //
  CO_LupForma_COD_NaoTem  = 0;
  CO_LupForma_COD_Binario = 1;
  CO_LupForma_COD_PorLoop = 2;
  //
  CO_RespMetodo_COD_None = 0;
  CO_RespMetodo_COD_DESKTP_Manual = 1;
  CO_RespMetodo_COD_MOBILE_Manual = 2;
  CO_RespMetodo_COD_MOBILE_QRCode = 3;

  //
  //

  MaxVarsContratos = 59;
  sListaContratos: array[0..MaxVarsContratos] of String = (
  '[NUM_CNTR] N�mero do contrato',
  '[TRAT_CLI] Tratamento ao cliente (empresa, Sr., etc)',
  '[ART_O_A_] Artigo definido singular ("o" ou "a") para o cliente',
  '[ART_OSAS] Artigo definido plural ("os" ou "as") para o cliente',
  '[PREP_ART] Preposi��o + artigo ("ao" ou "�") para o cliente',
  '[NOME_CLI] Nome do cliente',
  '[DOCU_CLI] CNPJ/CPF do cliente',
  '[ENDR_CLI] Endere�o completo do cliente',
  '[NOME_EMP] Nome da contratada',
  '[DOCU_EMP] CNPJ/CPF da contratada',
  '[ENDR_EMP] Endere�o completo da contratada',
  '[NOM_RESP] Nome do respons�vel (Cliente)',
  '[CPF_RESP] CPF do respons�vel (Cliente)',
  '[RGN_RESP] N�mero do RG do respons�vel (Cliente)',
  '[RGO_RESP] �rg�o emissor do RG do respons�vel (Cliente)',
  '[RGD_RESP] Data da emiss�o do RG do respons�vel (Cliente)',
  '[CID_RESP] Cidade resid�ncia do respons�vel (Cliente)',
  '[DATA_CON] Data da confec��o do contrato',
  '[DATA_COE] Data da confec��o do contrato',
  '[DATA_VAL] Data da validade da oferta (Contrato)',
  '[DATA_VEN] Data do 1� vencimento',
  '[DIA_PAGT] Anivers�rio (dia do m�s) da mensalidade',
  '[DATA_INT] Data do in�cio da vig�ncia do contrato',
  '[DATA_REC] Data da rescis�o do contrato',
  '[DATA_PRR] Data da pr�xima renova��o do contrato',
  '[VAL_MENS] Valor da mensalidade',
  '[LOC_ASIN] Local da assinatura',
  '[DAT_ASIN] Data da assinatura',
  '[DAT_ASEX] Data da assinatura por extenso',
  '[PER_MULT] % de multa por atraso',
  '[PER_MORA] % mensal de juros de mora por atraso',
  '[NOM_TES1] Nome da testemunha 1',
  '[CPF_TES1] CPF da testemunha 1',
  '[EN1_TES1] Linha 1 do endere�o da testemunha 1',
  '[EN2_TES1] Linha 2 do endere�o da testemunha 1',
  '[EN3_TES1] Linha 3 do endere�o da testemunha 1',
  '[NOM_TES2] Nome da testemunha 2',
  '[CPF_TES2] CPF da testemunha 2',
  '[EN1_TES2] Linha 1 do endere�o da testemunha 2',
  '[EN2_TES2] Linha 2 do endere�o da testemunha 2',
  '[EN3_TES2] Linha 3 do endere�o da testemunha 2',
  '[TEM_IMPL] Tempo de implementa��o',
  '[TEM_TREI] Tempo de treinamento',
  '[TEM_MONI] Tempo de monitoramento',
  '[PRZ_IMPL] Prazo de in�cio da implementa��o',
  '[DOC_PAGA] Tipo de documento no pagamento (Boleto, cheque)',
  '[SERVICOS] Servi�os prestados',
  //'[DET_SERV] Detalhes dos servi�os',
  '[EST_DESC] Descri��o do estabelecimento',
  '[EST_O_A_] Artigo definido singular ("o" ou "a") para o estabelecimento',
  '[ART_OSAS] Artigo definido plural ("os" ou "as") para o estabelecimento',
  '[ESTB_ART] Preposi��o + artigo ("ao" ou "�") para o estabelecimento',
  '[LOGR_PRP] Preposi��o para o logradouro do cliente',
  '[LOGR_PPE] Preposi��o para o logradouro da empresa',
  '[TIPO_DOC] Tipo de documento (CNPJ ou CPF)',
  '[PRAG_SIM] Pragas alvo inclusas',
  '[PRAG_NAO] Pragas alvo exclusas',
  '[ESCOPOSP] Limpeza de Caixa d`�gua',
  '[SERVICOS] Servi�os oferecidos',
  '[LOCAIS_S] Locais contemplados',
  '');
  //
  MaxUserGenero = 1;
  sListaUserGenero: array[0..MaxUserGenero] of String = ('Nenhuma', 'Ordem de Servi�o');
  //
  MaxTiposMoveis = 4;
  sListaTiposMoveis: array[0..MaxTiposMoveis]of String = ('Desconhecido',
    'M�vel', 'Autom�vel', 'Animal', 'Outro');
  //
  MaxAplicacaoChekLstCab = 0;
  sListaAplicacaoChekLstCab: array[0..MaxAplicacaoChekLstCab] of String = (
  'OS Caixa d''�gua');
  //
  CO_PRG_LST_NENHUMA = 0;
  CO_PRG_LST_BINARIO = 1;
  CO_PRG_LST_QUANTIT = 2;
  CO_PRG_LST_BXAPROD = 3;
  CO_PRG_LST_ATRIBUT = 4;
  CO_PRG_LST_TXTLIVR = 5;
  //
  MaxFuncaoPrgAtrCab = 5;
  sListaFuncaoPrgAtrCab: array[0..MaxFuncaoPrgAtrCab] of String = (
  'Nenhuma', 'Bin�rio', 'Quantitativo', 'Adi/reti. Produto', 'Uso de Atributo',
  'Texto Livre');
  //
  MaxOperacaoesBugs = 2;
  sListaOperacoesBugs: array[0..MaxOperacaoesBugs] of String = (
  'Dedetiza��o', 'Caixa d''�gua', 'Monitoramento');
  //
  MaxPrgLstItsRelacao = 2;
  CO_PrgLstItsRelacao_COD_N_I   = 0;
  CO_PrgLstItsRelacao_COD_PIP   = 1;
  CO_PrgLstItsRelacao_COD_Iscas = 2;
  CO_PrgLstItsRelacao_TXT_N_I   = 'N/I';
  CO_PrgLstItsRelacao_TXT_PIP   = 'PIP';
  CO_PrgLstItsRelacao_TXT_Iscas = 'Iscas';
  sListaPrgLstItsRelacao: array[0..MaxPrgLstItsRelacao] of String = (
  (*0*)CO_PrgLstItsRelacao_TXT_N_I,
  (*1*)CO_PrgLstItsRelacao_TXT_PIP,
  (*2*)CO_PrgLstItsRelacao_TXT_Iscas);
  //
  CO_COD_BUGS_DILUENTE_000 = 0;  // Indefinido
  CO_COD_BUGS_DILUENTE_001 = 1;  // N�o tem
  CO_COD_BUGS_DILUENTE_002 = 2;  // �gua
  CO_COD_BUGS_DILUENTE_003 = 3;  // Produto indicado
  CO_FldEhDil = 'EhDiluente';
  CO_FldRespo = 'Responsa';
  CO_FldAutrz = 'Autorizado';
  CO_FldTudoFeito = 'TudoFeito';
  CO_FldEscolhido = 'Escolhido';
  MaxDiluidoresBugs = 3;
  sListaDiluidoresBugs: array[0..MaxDiluidoresBugs] of String = (
  'Indefinido', 'N�o tem', '�gua', 'Produto indicado');
  //
  MaxAplicacaoTxtGeneric = 3;
  QtdColsAplicacaoTxtGeneric = MaxAplicacaoTxtGeneric + 1;
  sListaAplicacaoTxtGeneric: array[0..MaxAplicacaoTxtGeneric] of String = (
  'OS Execucao', 'Or�amento', 'Fotos Clientes', 'Parecer de Relat�rio');
{
  MaxListaNiveisPragas = 10;
  sListaNiveisPragas: array[0..10] of array[0..2] of String = (
  ('Praga_6', '60', '??'),
  ('Praga_7', '60', 'Filo'),
  ('Praga_8', '60', 'Classe'),
  ('Praga_9', '60', 'Ordem'),
  ('Praga_A', '60', 'Praga'),
  ('Praga_B', '60', 'Sub-ordem'),
  ('Praga_C', '60', 'Super Fam�lia'),
  ('Praga_D', '60', 'Fam�lia'),
  ('Praga_E', '60', 'Sub-fam�lia'),
  ('Praga_F', '60', 'G�ner'),
  ('Praga_Z', '60', 'Esp�cie')
  );
}
  //
  MaxListaEmisStatus = 3;
  sListaEmisStatus: array[0..MaxListaEmisStatus] of String = (
  'Aberto', 'Parcial', 'Total', 'Reaberto');
  //
  MaxOperacoesBugstrol = 7;
  sListaOperacoesBugstrol: array[0..MaxOperacoesBugstrol] of String = (
  'INDEFINIDO!',
  'Dedetiza��o',
  'Caixa d''�gua',
  'MISTA (DL)',
  'Monitoramento',
  'MISTA (DM)',
  'MISTA (LM)',
  'COMPLETA');
  //
  // A��o a ser tomada nesta P�s Venda:
  CO_OS_POS_APLICACAO_000_NENHUMA = 0; //  Nenhuma
  CO_OS_POS_APLICACAO_001_TELEFON = 1; //  Telefonar
  CO_OS_POS_APLICACAO_002_ENVMAIL = 2; //  Enviar email
  CO_OS_POS_APLICACAO_000_NENHUMA_TXT = 'Nenhuma';
  CO_OS_POS_APLICACAO_001_TELEFON_TXT = 'Telefonar';
  CO_OS_POS_APLICACAO_002_ENVMAIL_TXT = 'Enviar email';
  MaxListaAcaoPosVda = 2;
  sListaAcaoPosVda: array[0..MaxListaAcaoPosVda] of String = (
  CO_OS_POS_APLICACAO_000_NENHUMA_TXT,
  CO_OS_POS_APLICACAO_001_TELEFON_TXT,
  CO_OS_POS_APLICACAO_002_ENVMAIL_TXT);
  //
  CO_FRM_INDICUSO_001_COD_Indefinido    = 0;
  CO_FRM_INDICUSO_002_COD_PrevisaoDeUso = 1;
  CO_FRM_INDICUSO_001_COD_UsoDefinitivo = 2;
  //
  MaxListaImpressoesOS = 6;
  sListaImpressoesOS: array[0..MaxListaImpressoesOS] of String = (
  'Cadastro de entidade',
  'Ficha do Lugar',
  'Ficha de vistoria',
  'Or�amento',
  'Ordem de execu��o',
  'Monitoramento',
  'Comprovante de execu��o');

  // RMIP
  //TItensRMIP = (rmipNenhum=0, rmipCapa=1, rmipParecer=2, rmipPizzaMonits=3);
  MaxListaItensRMIP = Integer(High(TItensRMIP));
  sOrdemItensRMIP: array[0..MaxListaItensRMIP] of Integer = (
  0, 1, 2, 3, 4, 14, 5, 6, 15, 7, 8, 9, 10, 11, 12, 13, 16);
  sListaItensRMIP: array[0..MaxListaItensRMIP] of String = (
  //rmipNenhum=0,
  '[Nenhum]',
  //rmipCapa=1,
  'Capa',
  //rmipParecer=2,
  'Parecer',
  //rmipPizzaMonits=3
  'Pizza dos Monitoramentos por Data x Resposta',
  // rmipLinhaEvoluHistori=4
  'Linha de Evolu��o do Hist�rico por Data x Resposta',
  // rmipPizzaMonitsDep=5,
  'Pizza dos Monitoramentos por Data x Depend�ncia',
  //rmipLinhaEvoluHistoriDep=6
  'Linha de Evolu��o do Hist�rico por Data x Depend�ncia',
  //rmipResumoOSs=7
  'Resumo das OSs Executadas',
  //rmipComprovantesExecucao=8
  'Comprovantes de Execu��o',
  //rmipListagemSituacoesPMVs=9
  'Listagem das Situa��es dos PMVs',
  //rmipProdutosUtilizados=10
  'Quantidades Totais de Produtos Utilizados',
  //rmipNCTs=11
  'NCTs',
  //rmipSPs=12
  'Solicita��es de Provid�ncias',
  //rmipAgendaOSsPrev=13
  'Cronograma das OSs Previstas',
  // rmipBarraEvoluHistori=14
  'Barras da Evolu��o do Hist�rico por Data x Resposta',
  //rmipBarraEvoluHistoriDep=15
  'Barras da Evolu��o do Hist�rico por Data x Depend�ncia',
  //rmipCroquiPMVSColoridos=16
  'Croqui com PMVs coloridos'
  );
  MaxListaAplicacaoRMIPCfgIts = Integer(High(TAplicacaoRMIPCfgIts));
  sListaAplicacaoRMIPCfgIts: array[0..MaxListaAplicacaoRMIPCfgIts] of String = (
  //arciNenhuma=0,
  'Nenhuma',
  //arciEmpresa=1,
  'Empresa',
  //arciCliente=2,
  'Cliente',
  //arciAmbos=3
  'Ambos');
  // StatusOS
  //TStatusOSExec = (soseNaoDefinido=0, soseEmSer=1, soseExecutada=2, soseCancelada=3);
  MaxListaStatusOSExec = Integer(High(TStatusOSExec));
  sListaStatusOSExec: array[0..MaxListaStatusOSExec] of String = (
  //soseNaoDefinido=0,
  'N�o definido',
  //soseEmSer=1,
  'Em ser',
  //soseExecutada=2,
  'Executada',
  //soseCancelada=3
  'Cancelada'
  );
  //
  CO_GeraGrafRelatPMV_0001_COD_PizzaMonitsResp      = 0001;
  CO_GeraGrafRelatPMV_0002_COD_LinhaEvoluHistorResp = 0002;
  CO_GeraGrafRelatPMV_0004_COD_PizzaMonitsDep       = 0004;
  CO_GeraGrafRelatPMV_0008_COD_LinhaEvoluHistorDep  = 0008;
  CO_GeraGrafRelatPMV_0016_COD_BarraEvoluHistorResp = 0016;
  CO_GeraGrafRelatPMV_0032_COD_BarraEvoluHistorDep  = 0032;
  CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp      = 'Pizza das revis�es de PMVs por data x resposta.';
  CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistorResp = 'Linha de evolu��o do hist�rico por data x resposta.';
  CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep       = 'Pizza das revis�es de PMVs por data x depend�ncia.';
  CO_GeraGrafRelatPMV_0008_TXT_LinhaEvoluHistorDep  = 'Linha de evolu��o do hist�rico por data x depend�ncia.';
  CO_GeraGrafRelatPMV_0016_TXT_BarraEvoluHistorResp = 'Barra de evolu��o do hist�rico por data x resposta.';
  CO_GeraGrafRelatPMV_0032_TXT_BarraEvoluHistorDep  = 'Barra de evolu��o do hist�rico por data x depend�ncia.';
  MaxGeraGrafRelatPMV = 5;
  sListaGeraGrafRelatPMV: array[0..MaxGeraGrafRelatPMV] of String = (
  CO_GeraGrafRelatPMV_0001_TXT_PizzaMonitsResp,
  CO_GeraGrafRelatPMV_0002_TXT_LinhaEvoluHistorResp,
  CO_GeraGrafRelatPMV_0004_TXT_PizzaMonitsDep,
  CO_GeraGrafRelatPMV_0008_TXT_LinhaEvoluHistorDep,
  CO_GeraGrafRelatPMV_0016_TXT_BarraEvoluHistorResp,
  CO_GeraGrafRelatPMV_0032_TXT_BarraEvoluHistorDep);
  //
  //  CONSTANTES DE TEXTOS!
  CO_NAO_RESPONDIDO = 'N�o Respondido';
  CO_PIP_SemDependencia = 'PMV sem depend�ncia definida';
  CO_PODE_ALTERAR_ESTOQUE_PRODUTO_ABA = 'Couros VS';
  CO_PODE_ALTERAR_ESTOQUE_PRODUTO_CHK = 'Permite alterar estoque de couros VS ap�s invent�rio';
  //
implementation
////////////////////////////////////////////////////////////////////////////////
// vsmovits >> Pendente:                                                      //
// unit VSFchRslCab;
////////////////////////////////////////////////////////////////////////////////



end.
