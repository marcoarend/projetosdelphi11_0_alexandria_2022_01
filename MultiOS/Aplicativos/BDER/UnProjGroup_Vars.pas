unit UnProjGroup_Vars;

interface

uses Vcl.StdCtrls, UnDmkEnums;

var
  //VAR_PERIODO_BAL: Integer = -2;
  VAR_TipoCalcCouro_Compra: TTipoCalcCouro = TTipoCalcCouro.ptcNaoInfo;
  VAR_SeqMenuItem: Integer = 0;
  VAR_REL_GEREN: Integer;
  VAR_FLUX_CXA_DIA: Integer = 0;
  VAR_NOME_TAB_VERIF_IMEI: String;
  VAR_MEMO_DEF_VS_MUL_FRN, VAR_MEMO_DEF_VS_MUL_NFE: TMemo;
  VAR_PQS_ESTQ_NEG: String;
  VAR_BxaVrtPeca, VAR_BxaVrtPeso, VAR_BxaVrtArM2: Double;
  VAR_SdoVrtPeca, VAR_SdoVrtPeso, VAR_SdoVrtArM2: Double;
  VAR_AVISOS_SPEDEFDEnce_Periodo: Integer = 0;
  VAR_VS_PERIODO_BAL: Integer;
  VAR_NFCMO_SerNF: Integer = -1;
  VAR_NFRMP_SerNF: Integer = -1;
  VAR_NFCMO_nNF: Integer = 0;
  VAR_NFRMP_nNF: Integer = 0;
  VAR_IMEI_SEL: Integer = 0;
  // VS_CRC
  VAR_VSInsPalManu: Integer = 0;
  VAR_InfoMulFrnImpVS: Integer = 0;
  VAR_VSWarNoFrn: Integer = 0;
  VAR_VSWarSemNF: Integer = 0;
  VAR_VSImpRandStr: String = '';
  VAR_CRC_StqCen_LOC: String = '';
  VAR_CRC_FMO_BH: String = '';
  VAR_CRC_FMO_WE: String = '';
  VARF_NObrigNFeVS: Integer = 1;
  VAR_SetorCal: Integer = 0;
  VAR_SetorCur: Integer = 0;
  // Fim VS_CRC
  VAR_VSMOEnvAvu: Integer = 0;
  VAR_VSMOEnvEnv: Integer = 0;
  VAR_VSMOEnvRet: Integer = 0;
  VAR_PC_FRETE_VS_NAME: String = '';
  VAR_NOME_COPCab: String = '';

const // que podem mudar  !!
  VAR_CLA_ART_RIB_MAX_BOX_06 = 6;
  VAR_CLA_ART_RIB_MAX_BOX_15 = 15;

implementation



end.
