unit UnProjGroupEnums;

interface

uses  System.SysUtils, System.Types;

//const
  //CO_ShowKindList_0_RealIndx = 0;

type
  TFormaAberturaRegistro  = (foabre_00_Indef = 0,  foabre_01_Abertura = 1,
                             foabre_02_ReAbert = 2);
  TEXIdGrupoTabelas  = (exigt_00_Lancto = 0, exigt_01_Cotacao = 1);
  TEXFormaEdicao  = (exfe_00_PrimaInc = 0, exfe_01_MaisInc = 1,
                     exfe_02_AltDel = 2);
  TEXSeqJan  = (exsj_00_Conta_Basic_Modo = 0, exsj_01_Conta_Modo_Basic = 1,
                exsj_02_Basic_Conta_Modo = 2, exsj_03_Basic_Modo_Conta = 3,
                exsj_04_Modo_Conta_Basic = 4, exsj_05_Modo_Basic_Conta = 5,
                exsj_06_MaisUmItem = 6, exsj_07_AlteraItem = 7,
                exsj_08_ExcluiItem = 8 );
  TStatusAndamento = (
    salIndefinido=0,
    salAberto=1,
    salReaberto=2,
    salEncerrado=3,
    salEnvServer=4,
    salLivre5=5,       //... Livre
    salLivre6=6,       //... Livre
    salLivre7=7,       //... Livre
    salExcluido=8,
    salFinalizado=9);
  TUnProjGroupEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
//    function ObtemNomeSeccaoInsp(SeccaoInsp: TSeccaoInsp): String;
  end;
var
  ProjGroupEnums: TUnProjGroupEnums;


implementation

{ TUnProjGroupEnums }

{
function TUnProjGroupEnums.ObtemNomeSeccaoInsp(SeccaoInsp: TSeccaoInsp): String;
begin
  case SeccaoInsp of
    sccinspNone(*0*)          : Result := '*Indefinido*';
    sccinspTecelagem(*1024*)  : Result := 'Tecelagem';
    sccinspTinturaria(*2048*) : Result := 'Tinturaria';
    sccinspConfeccao(*3072*)  : Result := 'Confec��o';
    else Result := '?s?e?c?a?o?';
  end;
end;
}
end.
