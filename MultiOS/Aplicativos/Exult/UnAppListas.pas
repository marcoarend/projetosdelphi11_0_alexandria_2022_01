unit UnAppListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, UnMyLinguas, UnInternalConsts, dmkGeral, DB,
  mySQLDbTables, UnDmkProcFunc, UnDmkEnums;

const
  MaxVarsContratos = 1;
  sListaContratos: array[0..MaxVarsContratos] of String = (
{
  '[NUM_CNTR] N�mero do contrato',
  '[TRAT_CLI] Tratamento ao cliente (empresa, Sr., etc)',
  '[ART_O_A_] Artigo definido singular ("o" ou "a") para o cliente',
  '[ART_OSAS] Artigo definido plural ("os" ou "as") para o cliente',
  '[PREP_ART] Preposi��o + artigo ("ao" ou "�") para o cliente',
  '[NOME_CLI] Nome do cliente',
  '[DOCU_CLI] CNPJ/CPF do cliente',
  '[ENDR_CLI] Endere�o completo do cliente',
  '[NOM_RESP] Nome do respons�vel (Cliente)',
  '[CPF_RESP] CPF do respons�vel (Cliente)',
  '[RGN_RESP] N�mero do RG do respons�vel (Cliente)',
  '[RGO_RESP] �rg�o emissor do RG do respons�vel (Cliente)',
  '[RGD_RESP] Data da emiss�o do RG do respons�vel (Cliente)',
  '[CID_RESP] Cidade resid�ncia do respons�vel (Cliente)',
  '[DATA_CON] Data da confec��o do contrato',
  '[DATA_VAL] Data da validade da oferta (Contrato)',
  '[DATA_VEN] Data do 1� vencimento',
  '[DIA_PAGT] Anivers�rio (dia do m�s) da mensalidade',
  '[DATA_INT] Data do in�cio da vig�ncia do contrato',
  '[VAL_MENS] Valor da mensalidade',
  '[LOC_ASIN] Local da assinatura',
}
  '[DAT_ASIN] Data da assinatura',
{
  '[PER_MULT] % de multa por atraso',
  '[PER_MORA] % mensal de juros de mora por atraso',
  '[NOM_TES1] Nome da testemunha 1',
  '[CPF_TES1] CPF da testemunha 1',
  '[EN1_TES1] Linha 1 do endere�o da testemunha 1',
  '[EN2_TES1] Linha 2 do endere�o da testemunha 1',
  '[EN3_TES1] Linha 3 do endere�o da testemunha 1',
  '[NOM_TES2] Nome da testemunha 2',
  '[CPF_TES2] CPF da testemunha 2',
  '[EN1_TES2] Linha 1 do endere�o da testemunha 2',
  '[EN2_TES2] Linha 2 do endere�o da testemunha 2',
  '[EN3_TES2] Linha 3 do endere�o da testemunha 2',
  '[TEM_IMPL] Tempo de implementa��o',
  '[TEM_TREI] Tempo de treinamento',
  '[TEM_MONI] Tempo de monitoramento',
  '[PRZ_IMPL] Prazo de in�cio da implementa��o',
  '[DOC_PAGA] Tipo de documento no pagamento (Boleto, cheque)',
  '[SERVICOS] Servi�os prestados',
  '[DET_SERV] Detalhes dos servi�os',
  '[EST_DESC] Descri��o do estabelecimento',
  '[EST_O_A_] Artigo definido singular ("o" ou "a") para o estabelecimento',
  '[ART_OSAS] Artigo definido plural ("os" ou "as") para o estabelecimento',
  '[ESTB_ART] Preposi��o + artigo ("ao" ou "�") para o estabelecimento',
  '[LOGR_PRP] Preposi��o para o logradouro do cliente',
  '[TIPO_DOC] Tipo de documento (CNPJ ou CPF)',
}
  '');
  MaxUserGenero = 1;
  MaxAplicacaoChekLstCab = 0;
  sListaAplicacaoChekLstCab: array[0..MaxAplicacaoChekLstCab] of String = ('');

  sListaUserGenero: array[0..MaxUserGenero] of String = ('Nenhuma', 'Ordem de Servi�o');
  MaxTiposMoveis = 4;
  sListaTiposMoveis: array[0..MaxTiposMoveis]of String = ('Desconhecido',
    'M�vel', 'Autom�vel', 'Animal', 'Outro');

{
  MaxListaNiveisPragas = 10;
  sListaNiveisPragas: array[0..10] of array[0..2] of String = (
  ('Praga_6', '60', '??'),
  ('Praga_7', '60', 'Filo'),
  ('Praga_8', '60', 'Classe'),
  ('Praga_9', '60', 'Ordem'),
  ('Praga_A', '60', 'Praga'),
  ('Praga_B', '60', 'Sub-ordem'),
  ('Praga_C', '60', 'Super Fam�lia'),
  ('Praga_D', '60', 'Fam�lia'),
  ('Praga_E', '60', 'Sub-fam�lia'),
  ('Praga_F', '60', 'G�ner'),
  ('Praga_Z', '60', 'Esp�cie')
  );
}

type
  //TTipoTroca = (trcOA, trcOsAs, trcPara);
  TUnAppListas = class(TObject)
    mySQLQuery1: TmySQLQuery;
  private
    { Private declarations }
    procedure IIA(var Lista: TAppGrupLst; Valores: array of String);

  public
    { Public declarations }
    function  ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
    function  ListaNiveisPragas(): TAppGrupLst;
    procedure ReplaceVarsContratoMSWord(var Doc: Variant;
              QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel);
  end;

var
  AppListas: TUnAppListas;

implementation

uses Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

procedure TUnAppListas.IIA(var Lista: TAppGrupLst; Valores: array of String);
var
  I, K: Integer;
begin
  K := Length(Lista);
  SetLength(Lista, K + 1);
  SetLength(Lista[K], 3);
  Lista[K][0] := Valores[0];
  Lista[K][1] := Valores[1];
  Lista[K][2] := Valores[2];
end;

function TUnAppListas.ListaNiveisPragas: TAppGrupLst;
var
  I: Integer;
begin
  SetLength(Result, 0);
  //
  IIA(Result, ['Praga_A', '60', 'Grupos de Pragas']);
  IIA(Result, ['Praga_Z', '60', 'Pragas']);
end;

function TUnAppListas.ObtemGeneroDeUserGenero(UserGenero: Integer): Integer;
begin
  case UserGenero of
    0: Result := 0;
    //1: Result := CO_DIARIO_ADD_GENERO_ORD_SRV;
    else Result := -1;
  end;
end;

procedure TUnAppListas.ReplaceVarsContratoMSWord(var Doc: Variant;
QrCtr: TmySQLQuery; LaAviso1, LaAviso2: TLabel);
  function Troc(Tag, Val: String): Integer;
  var
    I: Integer;
    Continua: Boolean;
  begin
    //while Doc.Content.Find.Found(FindText := Tag) do
    //for I := 1 to 20 do
    Continua := True;
    while Continua do
      Continua :=
        Doc.Content.Find.Execute(FindText := Tag, ReplaceWith := Val);
  end;
  function ValI(Campo: String): Integer;
  begin
    Result := QrCtr.FieldByName(Campo).AsInteger;
  end;
  function TxtI(Tag: String; Val: Integer; Tipo: TTipoTroca): String;
  begin
    Troc(Tag, dmkPF.FormaDeTratamentoBR(Val, Tipo));
  end;
  procedure CtrI(Tag, Campo: String);
  begin
    Troc(Tag, Geral.FF0(QrCtr.FieldByName(Campo).AsInteger));
  end;
  procedure CtrS(Tag, Campo: String);
  begin
    Troc(Tag, QrCtr.FieldByName(Campo).AsString);
  end;
  function  ValF(Campo: String; Casas: Integer): String;
  begin
    Result :=
      Geral.FFT(QrCtr.FieldByName(Campo).AsFloat, Casas, siPositivo);
  end;
  procedure CtrF(Tag, Campo: String; Casas: Integer);
  begin
    Troc(Tag, ValF(Campo, Casas));
  end;
  procedure CtrD(Tag, Campo: String);
  begin
    Troc(Tag, Geral.FDT(QrCtr.FieldByName(Campo).AsDateTime, 2));
  end;
var
  I, K, N, ArtDefCli, ArtDefLug: Integer;
  Tag: String;
  NOME_EMP, NOME_CLI, NOME_REP, CPFJ_EMP, CPFJ_CLI, CPFJ_REP,
  ENDE_EMP, ENDE_CLI, ENDE_REP, CIDA_EMP, CIDA_CLI, CIDA_REP,
  TempoImpl, TempoMoni, TempoTrei, PrazoImpl, VAL_MENS, PER_MULT, PER_MORA,
  NOM_RESP, CID_RESP, CPF_RESP, RGN_RESP, RGO_RESP, RGD_RESP, Trato, TIPO_DOC,
  NOM_TES1, CPF_TES1, EN1_TES1, EN2_TES1, EN3_TES1,
  NOM_TES2, CPF_TES2, EN1_TES2, EN2_TES2, EN3_TES2: String;

begin
  DmodG.ReopenEndereco(ValI('Contratante'));
  NOME_CLI := DmodG.QrEnderecoNOME_ENT.Value;
  CPFJ_CLI := DmodG.QrEnderecoCNPJ_TXT.Value;
  ENDE_CLI := DmodG.QrEnderecoE_ALL.Value;
  Trato    := DmodG.QrEnderecoTRATO.Value;
  if Length(Geral.SoNumero_TT(CPFJ_CLI)) >= 14 then
    TIPO_DOC := 'CNPJ'
  else
    TIPO_DOC := 'CPF';
  //
  DmodG.ReopenEndereco(ValI('Contratada'));
  NOME_EMP := DmodG.QrEnderecoNOME_ENT.Value;
  CPFJ_EMP := DmodG.QrEnderecoCNPJ_TXT.Value;
  ENDE_EMP := DmodG.QrEnderecoE_ALL.Value;
  CIDA_EMP := DmodG.QrEnderecoCIDADE.Value;
  //

  DmodG.ReopenEndereco(ValI('RepreLegal'));
  NOM_RESP := DmodG.QrEnderecoNOME_ENT.Value;
  CID_RESP := DmodG.QrEnderecoCIDADE.Value;
  CPF_RESP := DmodG.QrEnderecoCNPJ_TXT.Value;
  RGN_RESP := DmodG.QrEnderecoRG.Value;
  RGO_RESP := Trim(DmodG.QrEnderecoSSP.Value);
  if RGO_RESP <> '' then
    RGO_RESP := ' ' + RGO_RESP;
  RGD_RESP := Geral.FDT(DmodG.QrEnderecoDataRG.Value, 2);
  if RGD_RESP <> '' then
    RGD_RESP := ' emitido em ' + RGD_RESP;
  //

  //
  //DmodG.ReopenEndereco(ValI('Testemun01'));
  DModG.ObtemEnderecoEtiqueta3Linhas(ValI('Testemun01'), EN1_TES1, EN2_TES1, EN3_TES1);
  NOM_TES1 := DmodG.QrEnderecoNOME_ENT.Value;
  CPF_TES1 := DmodG.QrEnderecoCNPJ_TXT.Value;

  DModG.ObtemEnderecoEtiqueta3Linhas(ValI('Testemun02'), EN1_TES2, EN2_TES2, EN3_TES2);
  NOM_TES2 := DmodG.QrEnderecoNOME_ENT.Value;
  CPF_TES2 := DmodG.QrEnderecoCNPJ_TXT.Value;

  //

  ArtDefCli :=  ValI('ArtigoDef');
  ArtDefLug :=  ValI('LugArtDef');
  //
  K := ValI('PerDefImpl');
  N := ValI('PerQtdImpl');
  TempoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  K := ValI('PerDefMoni');
  N := ValI('PerQtdMoni');
  TempoMoni := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  K := ValI('PerDefTrei');
  N := ValI('PerQtdTrei');
  TempoTrei := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  K := ValI('PerDefIniI');
  N := ValI('PerQtdIniI');
  PrazoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  VAL_MENS := ValF('ValorMes', 2);
  VAL_MENS := VAL_MENS + ' (' + dmkPF.ExtensoMoney(VAL_MENS) + ')';
  //
  PER_MULT := ValF('PercMulta', 2);
  PER_MULT := PER_MULT + '% (' + dmkPF.ExtensoFloat(PER_MULT) + ')';
  PER_MORA := ValF('PercJuroM', 2);
  PER_MORA := PER_MORA + '% (' + dmkPF.ExtensoFloat(PER_MORA) + ')';
  //

  for I := 0 to MaxVarsContratos do
  begin
    Tag := Copy(sListaContratos[I], 1, pos(']', sListaContratos[I]));
    if Tag <> '' then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Substituindo tag ' + Tag);
      if Tag = '[NUM_CNTR]' then CtrS(Tag, 'Numero') else
      if Tag = '[NOME_CLI]' then Troc(Tag, NOME_CLI) else
      if Tag = '[TRAT_CLI]' then CtrS(Tag, 'Tratamento') else
      if Tag = '[ART_O_A_]' then TxtI(Tag, ArtDefCli, trcOA) else
      if Tag = '[ART_OSAS]' then TxtI(Tag, ArtDefCli, trcOsAs) else
      if Tag = '[PREP_ART]' then TxtI(Tag, ArtDefCli, trcPara) else
      if Tag = '[DOCU_CLI]' then Troc(Tag, CPFJ_CLI) else
      if Tag = '[ENDR_CLI]' then Troc(Tag, ENDE_CLI) else
      if Tag = '[DATA_CON]' then CtrD(Tag, 'DContrato') else
      if Tag = '[DATA_VAL]' then CtrD(Tag, 'DtaPropFim') else
      if Tag = '[TEM_IMPL]' then Troc(Tag, TempoImpl) else
      if Tag = '[TEM_MONI]' then Troc(Tag, TempoMoni) else
      if Tag = '[TEM_TREI]' then Troc(Tag, TempoTrei) else
      if Tag = '[PRZ_IMPL]' then Troc(Tag, PrazoImpl) else
      if Tag = '[VAL_MENS]' then Troc(Tag, VAL_MENS) else
      if Tag = '[DOC_PAGA]' then CtrS(Tag, 'TpPagDocu') else
      if Tag = '[LOC_ASIN]' then Troc(Tag, CIDA_EMP) else
      if Tag = '[DAT_ASIN]' then CtrS(Tag, 'DtaAssina') else
      if Tag = '[SERVICOS]' then CtrS(Tag, 'Servicos01') else
      if Tag = '[DET_SERV]' then CtrS(Tag, 'Servicos02') else
      if Tag = '[NOM_RESP]' then Troc(Tag, NOM_RESP) else
      if Tag = '[CID_RESP]' then Troc(Tag, CID_RESP) else
      if Tag = '[CPF_RESP]' then Troc(Tag, CPF_RESP) else
      if Tag = '[RGN_RESP]' then Troc(Tag, RGN_RESP) else
      if Tag = '[RGO_RESP]' then Troc(Tag, RGO_RESP) else
      if Tag = '[RGD_RESP]' then Troc(Tag, RGD_RESP) else
      if Tag = '[DATA_INT]' then CtrD(Tag, 'DInstal') else
      if Tag = '[DATA_VEN]' then CtrD(Tag, 'DVencimento') else
      if Tag = '[DIA_PAGT]' then CtrI(Tag, 'ddMesVcto') else
      if Tag = '[PER_MULT]' then Troc(Tag, PER_MULT) else
      if Tag = '[PER_MORA]' then Troc(Tag, PER_MORA) else
      if Tag = '[NOM_TES1]' then Troc(Tag, NOM_TES1) else
      if Tag = '[CPF_TES1]' then Troc(Tag, CPF_TES1) else
      if Tag = '[EN1_TES1]' then Troc(Tag, EN1_TES1) else
      if Tag = '[EN2_TES1]' then Troc(Tag, EN2_TES1) else
      if Tag = '[EN3_TES1]' then Troc(Tag, EN3_TES1) else
      if Tag = '[NOM_TES2]' then Troc(Tag, NOM_TES2) else
      if Tag = '[CPF_TES2]' then Troc(Tag, CPF_TES2) else
      if Tag = '[EN1_TES2]' then Troc(Tag, EN1_TES2) else
      if Tag = '[EN2_TES2]' then Troc(Tag, EN2_TES2) else
      if Tag = '[EN3_TES2]' then Troc(Tag, EN3_TES2) else
      if Tag = '[EST_O_A_]' then TxtI(Tag, ArtDefLug, trcOA) else
      if Tag = '[EST_OSAS]' then TxtI(Tag, ArtDefLug, trcOsAs) else
      if Tag = '[ESTB_ART]' then TxtI(Tag, ArtDefLug, trcPara) else
      if Tag = '[EST_DESC]' then CtrS(Tag, 'LugarNome') else
      if Tag = '[LOGR_PRP]' then Troc(Tag, Trato) else
      if Tag = '[TIPO_DOC]' then Troc(Tag, TIPO_DOC) else
      Geral.MensagemBox('Tag n�o implementada: ' + Tag,
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

end.
