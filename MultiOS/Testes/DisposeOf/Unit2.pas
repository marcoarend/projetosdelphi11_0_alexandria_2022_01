unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit;

type
  TForm2 = class(TForm)
    btnPick: TButton;
    Edit1: TEdit;
    procedure btnPickClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Unit1;

{$R *.fmx}

procedure TForm2.btnPickClick(Sender: TObject);
//Second Code Snippet: Calling ShowModal as an Anonymous Method on All Platforms
//With the mobile platforms, you should change this code sightly to allow
//ShowModal to work on all the Supported Target Platforms.
var
  dlg: TForm1;
begin
  dlg := TForm1.Create(nil);
  // select current value, if available in the list
  dlg.ListBox1.ItemIndex := dlg.ListBox1.Items.IndexOf(edit1.Text);

  dlg.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
        // if OK was pressed and an item is selected, pick it
        if dlg.ListBox1.ItemIndex >= 0 then
          edit1.Text := dlg.ListBox1.Items [dlg.ListBox1.ItemIndex];
      end else
        edit1.Text := 'Canceled';
      //
      dlg.DisposeOf;
    end);
end;

end.
