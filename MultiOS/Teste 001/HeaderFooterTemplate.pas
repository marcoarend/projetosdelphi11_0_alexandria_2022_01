unit HeaderFooterTemplate;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Memo, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP;

type
  THeaderFooterForm = class(TForm)
    Header: TToolBar;
    Footer: TToolBar;
    HeaderLabel: TLabel;
    btnResetBadgeNumber: TButton;
    IdHTTP1: TIdHTTP;
    Memo1: TMemo;
    procedure btnResetBadgeNumberClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  HeaderFooterForm: THeaderFooterForm;

implementation

{$R *.fmx}

procedure THeaderFooterForm.btnResetBadgeNumberClick(Sender: TObject);
begin
  Memo1.Lines.Text :=
    //IdHTTP1.Post('http://www.dermatek.net.br/dmkREST.php?method=REST_LoginUser&Usuario=marco@dermatek.com.br&Senha=ielb2002');
    IdHTTP1.Get(
  'http://dermatek.net.br/dmkREST.php?method=REST_LoginUser&Usuario=marco@dermatek.com.br&Senha=ielb2002');
end;

end.
