unit UnGrlUsuarios;

interface

uses System.Classes, System.UITypes, Data.DB, Xml.XMLIntf, Xml.XMLDoc,
  {$IfDef DMK_FMX}
  FMX.Forms, UnFMX_DmkProcFunc,
  {$Else}
  Vcl.Forms, UnDmkWeb,
  {$EndIf}
  UnDmkEnums;

type
  TUnGrlUsuarios = class(TObject)
  private
    { Private declarations }
    procedure LimpaVarsWeb();
    procedure ReopenUsuarios(Device: TDeviceType; DataBase: TComponent;
              QueryUpd, Query: TDataSet; Usuario: Integer);
    function  AtualizaDadosUsuario(Device: TDeviceType; DataBase: TComponent;
              QueryUpd: TDataSet; Arquivo: String; Client_Id: String = ''): Boolean;
    function  ExcluiUsuario(DataBase: TComponent; Query: TDataSet; Codigo,
              Cliente_Id, Device: Integer; App, Msg: String): Boolean;
  public
    { Public declarations }
    function  Efetuar_Login(Form: TForm; Device: TDeviceType; DataBase: TComponent;
              QueryUpd, Query: TDataSet; Usuario: Integer; App_Id: String = ''): Boolean;
    function  ConfiguraNiveis(CodigoApp: Integer; SemRestricao: Boolean = True): TStringList;
  end;

var
  GrlUsuarios: TUnGrlUsuarios;
  VAR_API_ID_USUARIO: Integer = 0;
  VAR_API_ID_ENTIDADE: Integer = 0;
  VAR_API_TOKEN: String = '';
  //VAR_API_APP: String = '';
  //VAR_API_DEVICE: Integer = 0;
  VAR_API_ID_LOGIN: String = '';
  VAR_API_ID_CLIENTE: Integer = 0;
  VAR_API_STR_CLIENTE: String = '';
  VAR_API_NOME: String = '';
  VAR_API_NOME_ENTIDADE: String = '';
  VAR_API_DATA_NASCIMENTO: TDate = 0;
  VAR_API_ID_SEXO: Integer = 0;
  VAR_API_NOME_SEXO: String = '';
  VAR_API_USUARIO: String = '';
  VAR_API_EMAIL: String = '';
  VAR_API_TELEFONE: String = '';
  VAR_API_TIMEZONE: String = '';
  VAR_API_TIMEZONEDIFUTC: String = '';
  VAR_API_PERFIL: Integer = 0;
  VAR_API_TIPO: Integer = 0;

implementation

uses UnGrl_DmkDB, UnGrl_Geral, UnInternalConsts, UnGrl_Vars, UnGrl_REST,
  UnGrlEnt, UnGrl_Consts;

{ TUnGrlUsuarios }

function TUnGrlUsuarios.AtualizaDadosUsuario(Device: TDeviceType;
  DataBase: TComponent; QueryUpd: TDataSet; Arquivo: String;
  Client_Id: String = ''): Boolean;
const
  NomeFuncao = 'Fun��o: TUnGrlUsuarios.CadastraUsuario';
var
  XMLArqRes: IXMLDocument;
var
  I, Codigo, Entidade, DeviceId, Cliente_Id, Sexo, Perfil, Tipo,
  Confirmado, UsrLocal: Integer;
  Token, App, Login_Id, Nome, EntNome, DtaNatal, Usuario, Email, Telefone,
  TimeZone, TimeZoneDifUTC, Cliente_Str: String;
begin
  Result := False;
  //
  XMLArqRes         := TXMLDocument.Create(nil);
  XMLArqRes.Active  := False;
  try
    XMLArqRes.LoadFromXML(Arquivo);
    XMLArqRes.Encoding := 'ISO-10646-UCS-2';
    //
    with XMLArqRes.DocumentElement do
    begin
      Codigo         := Grl_Geral.IMV(ChildNodes['id_usuario'].Text);
      Entidade       := Grl_Geral.IMV(ChildNodes['id_entidade'].Text);
      Token          := ChildNodes['token'].Text;
      //App            := ChildNodes['app'].Text;
      //DeviceId       := Grl_Geral.IMV(ChildNodes['device'].Text);
      Login_Id       := ChildNodes['id_login'].Text;
      Cliente_Id     := Grl_Geral.IMV(ChildNodes['id_cliente'].Text);
      Cliente_Str    := ChildNodes['str_cliente'].Text;
      Nome           := ChildNodes['nome'].Text;
      EntNome        := ChildNodes['nome_entidade'].Text;
      DtaNatal       := ChildNodes['data_nascimento'].Text;
      Sexo           := Grl_Geral.IMV(ChildNodes['id_sexo'].Text);
      Usuario        := ChildNodes['usuario'].Text;
      Email          := ChildNodes['email'].Text;
      Telefone       := ChildNodes['telefone'].Text;
      TimeZone       := ChildNodes['timezone'].Text;
      TimeZoneDifUTC := ChildNodes['timezonedifutc'].Text;
      Perfil         := Grl_Geral.IMV(ChildNodes['perfil'].Text);
      Tipo           := Grl_Geral.IMV(ChildNodes['tipo'].Text);
      Confirmado     := Grl_Geral.IMV(ChildNodes['confirmado'].Text);
      //
      if Device = stDesktop then
        UsrLocal := VAR_USUARIO
      else
        UsrLocal := 0;
      //
      if Grl_Geral.FIC(Token = '', nil, 'Token n�o definido!' + sLineBreak + NomeFuncao) then
        Exit;
      (*
      if Grl_Geral.FIC(App = '', nil, 'App n�o definido!' + sLineBreak + NomeFuncao) then
        Exit;
      *)
      if Grl_Geral.FIC(Codigo = 0, nil, 'Codigo n�o definido!' + sLineBreak + NomeFuncao) then
        Exit;
      if Grl_Geral.FIC(Cliente_Id = 0, nil, 'Id do cliente n�o definido!' + sLineBreak + NomeFuncao) then
        Exit;
      //
      if ExcluiUsuario(DataBase, QueryUpd, Codigo, Cliente_Id, DeviceId, App, '') then
      begin
        if Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [
          'DELETE FROM usuarios ',
          'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
          'AND Cliente_Id=' + Grl_Geral.FF0(Cliente_Id),
          //'AND App=AES_ENCRYPT("' + App + '", "' + CO_RandStrWeb01 + '") ',
          //'AND Device=' + Grl_Geral.FF0(DeviceId),
          '; INSERT INTO usuarios SET ',
          'Entidade=' + Grl_Geral.FF0(Entidade) + ', ',
          'Token=AES_ENCRYPT("' + Token + '", "' + CO_RandStrWeb01 + '"), ',
          'Login_Id="' + Login_Id + '", ',
          'Nome="' + Nome + '", ',
          'EntNome="' + EntNome + '", ',
          'DtaNatal="' + DtaNatal + '", ',
          'Sexo=' + Grl_Geral.FF0(Sexo) + ', ',
          'Usuario="' + Usuario + '", ',
          'Email="' + Email + '", ',
          'Telefone="' + Telefone + '", ',
          'TimeZone="' + TimeZone + '", ',
          'TimeZoneDifUTC="' + TimeZoneDifUTC + '", ',
          'Perfil=' + Grl_Geral.FF0(Perfil) + ', ',
          'Tipo=' + Grl_Geral.FF0(Tipo) + ', ',
          'Confirmado=' + Grl_Geral.FF0(Confirmado) + ', ',
          'UsrLocal=' + Grl_Geral.FF0(UsrLocal) + ', ',
          //'Device=' + Grl_Geral.FF0(DeviceId) + ', ',
          //'App=AES_ENCRYPT("' + App + '", "' + CO_RandStrWeb01 + '"), ',
          'Cliente_Id=' + Grl_Geral.FF0(Cliente_Id) + ', ',
          'Cliente_Str="' + Cliente_Str + '", ',
          'Codigo=' + Grl_Geral.FF0(Codigo),
          ''])
        then
          Result := True;
      end;
    end;
  finally
    ;
  end;
end;

function TUnGrlUsuarios.ConfiguraNiveis(CodigoApp: Integer;
  SemRestricao: Boolean): TStringList;
var
  Niveis: TStringList;
begin
  Niveis := TStringList.Create;
  try
    //Os n�veis s�o espec�ficos de cada aplicativo
    //
    if CodigoApp in [17, 24] then //DControl, Bugstrol
    begin
      if SemRestricao then
        Niveis.Add('Liberado');
      //
      Niveis.Add('Admininstrador');
      Niveis.Add('Parceiro');
      Niveis.Add('Cliente');
      Niveis.Add('Usu�rio');
    end
    else if CodigoApp = 4 then //Syndi2
    begin
      if SemRestricao then
        Niveis.Add('Liberado');
      //
      Niveis.Add('Admininstrador');
      Niveis.Add('Grupo');
      Niveis.Add('S�ndico');
      Niveis.Add('Cond�mino');
    end
    else
    begin
      if SemRestricao then
        Niveis.Add('N�vel 0');
      //
      Niveis.Add('N�vel 1');
      Niveis.Add('N�vel 2');
      Niveis.Add('N�vel 3');
      Niveis.Add('N�vel 4');
    end;
  finally
    Result := Niveis;
  end;
end;

function TUnGrlUsuarios.Efetuar_Login(Form: TForm; Device: TDeviceType;
  DataBase: TComponent; QueryUpd, Query: TDataSet; Usuario: Integer;
  App_Id: String = ''): Boolean;
var
  ResCod: Integer;
  Res, Token, Client: String;
  Param: THttpParam;
begin
  Result := False;
  //
  if Device = stWeb then
  begin
    Grl_Geral.MB_Aviso('A fun��o: "TUnGrlUsuarios.Efetuar_Login" ' +
      sLineBreak + 'n�o est� implementada para a plataforma web!');
    Exit;
  end;
  {$IfnDef DMK_FMX}
  Screen.Cursor := crHourglass;
  {$EndIf}
  try
    ReopenUsuarios(Device, DataBase, QueryUpd, Query, Usuario);
    //
    if (Query.State = dsInactive) or (Query.RecordCount = 0) then
    begin
      if Grl_REST.OAuth2_Dermatek_EfetuaLogin(Form, Token, Client, App_Id) then
      begin
        Param.Nome         := 'device';
        Param.Valor        := Grl_Geral.FF0(Integer(stDesktop));
        Param.ACharse      := '';
        Param.AContentType := '';
        //
        ResCod := Grl_REST.Rest_Dermatek_Post('oauth', 'token_oauth', trtXML, [], Res, Token, Client);
        //
        if ResCod = 200 then
        begin
          if AtualizaDadosUsuario(Device, DataBase, QueryUpd, Res, '') then
          begin
            ReopenUsuarios(Device, DataBase, QueryUpd, Query, Usuario);
            Result := True;
          end;
        end;
      end;
    end else
    begin
      Result := True;
    end;
  finally
    {$IfnDef DMK_FMX}
    Screen.Cursor := crDefault;
    {$EndIf}
  end;
end;

procedure TUnGrlUsuarios.ReopenUsuarios(Device: TDeviceType;
  DataBase: TComponent; QueryUpd, Query: TDataSet; Usuario: Integer);
var
  Codigo, DeviceId, Entidade, Cliente_Id, Sexo, Perfil, Tipo, ResCod,
  Conexao: Integer;
  SQLWhere, Token, App, Login_Id, Nome, EntNome, Cliente_Str, Login, Email,
  Telefone, TimeZone, TimeZoneDifUTC, Res, Msg: String;
  DtaNatal: TDate;
  LstSexo: TStringList;
begin
  if Device = stDesktop then
    SQLWhere := 'WHERE UsrLocal=' + Grl_Geral.FF0(Usuario)
  else
    SQLWhere := 'WHERE Codigo=' + Grl_Geral.FF0(Usuario);
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT *, ',
    //'AES_DECRYPT(App, "' + CO_RandStrWeb01 + '") App_TXT,',
    'AES_DECRYPT(Token, "' + CO_RandStrWeb01 + '") Token_TXT ',
    'FROM usuarios ',
    SQLWhere,
    '']);
  //
  if Query.RecordCount > 0 then
  begin
    Codigo         := Query.FieldByName('Codigo').AsInteger;
    Entidade       := Query.FieldByName('Entidade').AsInteger;
    Token          := Query.FieldByName('Token_TXT').AsString;
    //App            := Query.FieldByName('App_TXT').AsString;
    //DeviceId       := Query.FieldByName('Device').AsInteger;
    Login_Id       := Query.FieldByName('Login_Id').AsString;
    Cliente_Id     := Query.FieldByName('Cliente_Id').AsInteger;
    Cliente_Str    := Query.FieldByName('Cliente_Str').AsString;
    Nome           := Query.FieldByName('Nome').AsString;
    EntNome        := Query.FieldByName('EntNome').AsString;
    DtaNatal       := Query.FieldByName('DtaNatal').AsDateTime;
    Sexo           := Query.FieldByName('Sexo').AsInteger;
    LstSexo        := GrlEnt.ObtemListaSexo;
    Login          := Query.FieldByName('Usuario').AsString;
    Email          := Query.FieldByName('Email').AsString;
    Telefone       := Query.FieldByName('Telefone').AsString;
    TimeZone       := Query.FieldByName('TimeZone').AsString;
    TimeZoneDifUTC := Query.FieldByName('TimeZoneDifUTC').AsString;
    Perfil         := Query.FieldByName('Perfil').AsInteger;
    Tipo           := Query.FieldByName('Tipo').AsInteger;
    //
    {$IfDef DMK_FMX}
    Conexao := FMX_dmkWeb.VerificaConexaoWeb(Msg);
    {$Else}
    Conexao := DmkWeb.VerificaConexaoWeb(Msg);
    {$EndIf}
    //
    if (Conexao <= 0) and (Conexao <> 2) then
      ResCod := 200
    else
      ResCod := Grl_REST.Rest_Dermatek_Post('oauth', 'token_valida_licenca',
                  trtXML, [], Res, Token, Cliente_Str);
    //
    if (ResCod <> 200) and (ResCod <> 0) then
    begin
      ExcluiUsuario(DataBase, QueryUpd, Codigo, Cliente_Id, DeviceId, App, '');
      LimpaVarsWeb;
      Query.Close;
    end else
    begin
      VAR_API_ID_USUARIO      := Codigo;
      VAR_API_ID_ENTIDADE     := Entidade;
      VAR_API_TOKEN           := Token;
      //VAR_API_APP             := App;
      //VAR_API_DEVICE          := DeviceId;
      VAR_API_ID_LOGIN        := Login_Id;
      VAR_API_ID_CLIENTE      := Cliente_Id;
      VAR_API_STR_CLIENTE     := Cliente_Str;
      VAR_API_NOME            := Nome;
      VAR_API_NOME_ENTIDADE   := EntNome;
      VAR_API_DATA_NASCIMENTO := DtaNatal;
      VAR_API_ID_SEXO         := Sexo;
      VAR_API_NOME_SEXO       := LstSexo[Sexo];
      VAR_API_USUARIO         := Login;
      VAR_API_EMAIL           := Email;
      VAR_API_TELEFONE        := Telefone;
      VAR_API_TIMEZONE        := TimeZone;
      VAR_API_TIMEZONEDIFUTC  := TimeZoneDifUTC;
      VAR_API_PERFIL          := Perfil;
      VAR_API_TIPO            := Tipo;
    end;
  end else
  begin
    LimpaVarsWeb;
    Query.Close;
  end;
end;

function TUnGrlUsuarios.ExcluiUsuario(DataBase: TComponent; Query: TDataSet;
  Codigo, Cliente_Id, Device: Integer; App, Msg: String): Boolean;
var
  Res: Integer;
begin
  Result := False;
  //
  if Msg = '' then
    Res := idYes
  else
    Res := Grl_Geral.MB_Pergunta(Msg);
  //
  if Res = mrYes then
  begin
    Result := Grl_DmkDB.ExecutaSQLQuery0(Query, DataBase, [
                'DELETE FROM usuarios',
                'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
                'AND Cliente_Id=' + Grl_Geral.FF0(Cliente_Id),
                //'AND Device=' + Grl_Geral.FF0(Device),
                //'AND App="' + App + '"',
                '']);
  end;
end;

procedure TUnGrlUsuarios.LimpaVarsWeb;
begin
  VAR_API_ID_USUARIO      := 0;
  VAR_API_ID_ENTIDADE     := 0;
  VAR_API_TOKEN           := '';
  //VAR_API_APP             := '';
  //VAR_API_DEVICE          := 0;
  VAR_API_ID_LOGIN        := '';
  VAR_API_ID_CLIENTE      := 0;
  VAR_API_NOME            := '';
  VAR_API_NOME_ENTIDADE   := '';
  VAR_API_DATA_NASCIMENTO := 0;
  VAR_API_ID_SEXO         := 0;
  VAR_API_NOME_SEXO       := '';
  VAR_API_USUARIO         := '';
  VAR_API_EMAIL           := '';
  VAR_API_TELEFONE        := '';
  VAR_API_TIMEZONE        := '';
  VAR_API_TIMEZONEDIFUTC  := '';
  VAR_API_PERFIL          := 0;
  VAR_API_TIPO            := 0;
end;

end.
