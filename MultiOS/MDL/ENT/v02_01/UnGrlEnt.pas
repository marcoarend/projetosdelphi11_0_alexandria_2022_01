unit UnGrlEnt;

interface

uses System.Generics.Collections, System.SysUtils, System.Classes, Data.DB,
  System.UITypes, System.Variants;

type
  TUnGrlEnt = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemListaSexo(): TStringList;
  end;

var
  GrlEnt: TUnGrlEnt;

implementation

{ TUnGrlEnt }

function TUnGrlEnt.ObtemListaSexo: TStringList;
var
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    Lista.Add('N�o informado');
    Lista.Add('Masculino');
    Lista.Add('Feminino');
  finally
    Result := Lista;
  end;
end;

end.
