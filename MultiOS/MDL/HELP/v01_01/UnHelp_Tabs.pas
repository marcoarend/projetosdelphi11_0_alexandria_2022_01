unit UnHelp_Tabs;

{ Colocar no MyListas:

  Uses UnHelp_Tabs;


  //
  function TMyListas.CriaListaTabelas:

  Help_Tb.CarregaListaTabelas(FTabelas);
  //
  function TMyListas.CriaListaSQL:
  Help_Tb.CarregaListaSQL(Tabela, FListaSQL);
  //
  function TMyListas.CompletaListaSQL:

  Help_Tb.ComplementaListaSQL(Tabela, FListaSQL);
  //
  function TMyListas.CriaListaIndices:
  Help_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);

  //
  function TMyListas.CriaListaCampos:
  Help_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
  Help_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
  //
  function TMyListas.CriaListaJanelas:
  Help_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, mysqlDBTables,
  UnGrl_Vars, dmkGeral, UnDmkEnums, DmkDAC_PF;

type
  TUnHelp_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase;
      Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
      FLCampos: TList<TCampos>; DMKID_APP: Integer;
      var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas: TJanelas;
      FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices: TIndices;
      FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ComplementaListaSQL(Tabela: String;
      FListaSQL: TStringList): Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
      FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
  end;

  // const

var
  Help_Tabs: TUnHelp_Tabs;

implementation

uses UMySQLModule;

function TUnHelp_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, LowerCase('helpcab'), '', True);
    MyLinguas.AdTbLst(Lista, False, LowerCase('helpopc'), '', True);
    MyLinguas.AdTbLst(Lista, False, LowerCase('helptopic'), '', True);
    MyLinguas.AdTbLst(Lista, False, LowerCase('helpfaq'), '', True);
    MyLinguas.AdTbLst(Lista, False, LowerCase('helprestr'), '', True);
    MyLinguas.AdTbLst(Lista, False, LowerCase('helprestip'), '');
    //
    if Database.DatabaseName = VAR_DBWEB then
    begin
      MyLinguas.AdTbLst(Lista, False, LowerCase('helpfaqhis'), '');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSer'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerGru'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerImp'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerArq'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerRem'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerTar'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrSeAte'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrSeAteIt'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrSeAteCa'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrSeAteGr'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerCom'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerEnc'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerHis'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerMod'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerPri'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerSta'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerAss'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOSAssGru'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOrdSerOpc'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WOSOpcUsu'), '');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTrei'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTreiIts'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTreiLnk'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTreiUsr'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTreiModo'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTreiOpc'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WTreiStat'), '');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('WFaqIts'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WFaqItsApl'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WFaqItsHis'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WFaqGru'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WFaqOpc'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WHelp'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WHelpApl'), '');
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnHelp_Tabs.CarregaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
end;

function TUnHelp_Tabs.ComplementaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  //
  if Uppercase(Tabela) = Uppercase('helpopc') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end
  else if Uppercase(Tabela) = Uppercase('helprestip') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('     1|"Aplicativo - Local"');
    FListaSQL.Add('     2|  "Aplicativo - Web"');
    FListaSQL.Add('     3|            "M�dulo"');
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerGru') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|           Nome|Ordem');
    FListaSQL.Add('     0|"N�o agrupados"|    0');
    FListaSQL.Add('    -1|      "Abertos"|   -1');
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerOpc') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerPri') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add
        ('Codigo|         Nome|                                                     Descri|Ordem');
    FListaSQL.Add
      ('     0|"Muito baixa"|                                                         ""|    5');
    FListaSQL.Add
      ('     1|      "Baixa"|                      "N�o afeta o trabalho do solicitante"|    4');
    FListaSQL.Add
      ('     2|     "Normal"|  "Afeta o trabalho do solicitante mas pode ser contornado"|    3');
    FListaSQL.Add
      ('     3|       "Alta"|"Afeta o trabalho do solicitante e n�o pode ser contornado"|    2');
    FListaSQL.Add
      ('     4| "Muito alta"|                        "Impede o solicitante de trabalhar"|    1');
  end
  else if Uppercase(Tabela) = Uppercase('WOSOpcUsu') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Usuario');
    FListaSQL.Add('0');
  end
  else if Uppercase(Tabela) = Uppercase('WTreiOpc') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end;
end;

function TUnHelp_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices: TIndices;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('helpcab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('helpopc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('helptopic') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('helpfaq') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('helpfaqhis') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('helprestr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('helprestip') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSer') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerGru') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerArq') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerRem') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerTar') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 2;
    FRIndices.Column_name := 'AgenTarIts';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrSeAte') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrSeAteIt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrSeAteCa') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrSeAteGr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerCom') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerEnc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerHis') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerMod') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerPri') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerSta') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerAss') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOSAssGru') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOrdSerOpc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WOSOpcUsu') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Usuario';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTrei') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTreiIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTreiLnk') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTreiUsr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTreiModo') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTreiOpc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WTreiStat') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WFaqIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WFaqItsApl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WFaqItsHis') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WFaqGru') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WFaqOpc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WHelp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end
  else if Uppercase(Tabela) = Uppercase('WHelpApl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique := 0;
    FRIndices.Key_name := 'PRIMARY';
    FRIndices.Seq_in_index := 1;
    FRIndices.Column_name := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnHelp_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; DMKID_APP: Integer;
  var TemControle: TTemControle): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('helpcab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // 01 - Liberado
      FRCampos.Field := 'Nivel'; // 02 - Boss
      FRCampos.Tipo := 'int(11)'; // 04 - Administrador
      FRCampos.Null := ''; // 08 - Cliente
      FRCampos.Key := ''; // 16 - Usu�rio
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Status'; // 0 => Cadastrando
      FRCampos.Tipo := 'tinyint(1)'; // 1 => Aguardando revis�o
      FRCampos.Null := ''; // 2 => Finalizado
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('helpopc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Texto';
      FRCampos.Tipo := 'mediumtext';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('helptopic') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nivel';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ParentId';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'HelpFaq';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('helpfaq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Texto';
      FRCampos.Tipo := 'mediumtext';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Tags';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '?';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Tipo'; // 01 - FAQ
      FRCampos.Tipo := 'int(11)'; // 02 - Tutorial
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // 01 - Liberado
      FRCampos.Field := 'Nivel'; // 02 - Boss
      FRCampos.Tipo := 'int(11)'; // 04 - Administrador
      FRCampos.Null := ''; // 08 - Cliente
      FRCampos.Key := ''; // 16 - Usu�rio
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Status'; // 0 => Cadastrando
      FRCampos.Tipo := 'tinyint(1)'; // 1 => Aguardando revis�o
      FRCampos.Null := ''; // 2 => Finalizado
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('helpfaqhis') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Util'; // 0 - N�o
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Sim
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome'; // Informar o porque apenas para "N�o"
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHora';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('helprestr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Help';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'HelpTip'; // 0 => Help
      FRCampos.Tipo := 'tinyint(1)'; // 1 => FAQ
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '-1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Item';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'HelpResTip';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '-1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('helprestip') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordser') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Cliente';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Solicit';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Finaliz'; // 0 - Aberto
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Finalizado
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Abertura';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Encerr';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Assunto';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Modo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Descri';
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'WOrdSerEnc';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Prioridade';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Respons';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Status';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Aplicativo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'VersaoApp';
      FRCampos.Tipo := 'varchar(13)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Janela';
      FRCampos.Tipo := 'varchar(13)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'JanelaRel';
      FRCampos.Tipo := 'varchar(13)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CompoRel';
      FRCampos.Tipo := 'varchar(35)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CadWeb'; // 0 - Cadastrado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Cadastrado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AltWeb'; // 0 - Alterado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Alterado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailAbe';
      // 0 - N�o enviar e-mail sobre abertura de OS
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Enviar e-mail sobre abertura de OS
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailEnc';
      // 0 - N�o enviar e-mail sobre encerramento de OS
      FRCampos.Tipo := 'tinyint(1)';
      // 1 - Enviar e-mail sobre encerramento de OS
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Tags'; // Palavras que ajudem a localizar
      FRCampos.Tipo := 'varchar(255)'; // a solicita��o (separadas por ,)
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Impacto'; // No que afeta o servi�o
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Sistema'; // Detalhes do computador
      FRCampos.Tipo := 'varchar(100)';
      // Ex: Sistema operaciona, arquitetura, ...
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'WOrdSerHis';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Grupo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Atrelada';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoAceite';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DuracaoApr';
      FRCampos.Tipo := 'double(15,2)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0.00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordsergru') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'IniData';
      FRCampos.Tipo := 'date';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'FimData';
      FRCampos.Tipo := 'date';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Observ';
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Respons';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus1';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus2';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus3';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus4';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserimp') then
    // Apenas impress�o n�o tem �ndice prim�rio
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UserImp';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHoraHis';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Abertura';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Encerr';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Encerramento';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NOMECLI';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'PersonalName';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AssuntoTXT';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NOMEMOD';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NOMEPRI';
      FRCampos.Tipo := 'varchar(30)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NOMESTA';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'STATUS_TXT';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NOMERESP';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Descri';
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'WOrdSerEnc';
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NOMEAPLIC';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'VersaoApp';
      FRCampos.Tipo := 'varchar(13)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Janela';
      FRCampos.Tipo := 'varchar(13)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'JanelaRel';
      FRCampos.Tipo := 'varchar(13)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CompoRel';
      FRCampos.Tipo := 'varchar(35)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NomeCom';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Entidade';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHoraCom';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserarq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Arquivo'; // C�digo FtpWebArq
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CadWeb'; // 0 - Cadastrado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Cadastrado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AltWeb'; // 0 - Alterado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Alterado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserrem') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'App'; // TeamViewer �nico com suporte por enquanto
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ID';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UserId';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UserName';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DeviceId';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DeviceName';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'StartDate'; // UTC
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EndDate'; // UTC
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'SessionCode';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'SessionCreatedAt'; // UTC
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AssignedUserId';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AssignedAt'; // UTC
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Token';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordsertar') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AgenTarIts';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('worseate') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Atendente';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('worseateit') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Usuario';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Assunto';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Cliente';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Solicit';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Aplicativo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('worseateca') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Grupo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('worseategr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordsercom') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Entidade';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHora';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ComRes'; // 0 - Coment�rio ser� mostrado a todos
      FRCampos.Tipo := 'tinyint(1)';
      // 1 - Coment�rio ser� mostrado somente ao tipo 0, 1 no site
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMail'; // 0 - N�o enviar e-mail sobre coment�rio
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Enviar e-mail sobre coment�rio
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CadWeb'; // 0 - Cadastrado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Cadastrado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AltWeb'; // 0 - Alterado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Alterado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Lido'; // 0 - N�o lido
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Lido
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHoraLido';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Para';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserenc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Descri';
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserhis') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Tarefa'; // 0 - Abertura
      FRCampos.Tipo := 'int(11)'; // 1 - Encerramento
      FRCampos.Null := ''; // 2 - Comentado
      FRCampos.Key := ''; // 3 - Reabertura
      FRCampos.Default := '0'; // 4 - Mudan�a de Status
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CodOrig'; // C�digo de origem da tabela que o criou
      FRCampos.Tipo := 'int(11)'; // para o status comentado
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHora';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Respons';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CadWeb'; // 0 - Cadastrado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Cadastrado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AltWeb'; // 0 - Alterado local
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Alterado na web
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'MotivReopen';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordsermod') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserpri') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Descri';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordsersta') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(20)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // 01 - Liberado
      FRCampos.Field := 'Nivel'; // 02 - Boss
      FRCampos.Tipo := 'int(11)'; // 04 - Administrador
      FRCampos.Null := ''; // 08 - Cliente
      FRCampos.Key := ''; // 16 - Usu�rio
      FRCampos.Default := '1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'WOrdSerEnc';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordserass') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Descri';
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Grupo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Termo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Tarifa';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // 01 - Liberado
      FRCampos.Field := 'Nivel'; // 02 - Boss
      FRCampos.Tipo := 'int(11)'; // 04 - Administrador
      FRCampos.Null := ''; // 08 - Cliente
      FRCampos.Key := ''; // 16 - Usu�rio
      FRCampos.Default := '1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      if DMKID_APP = 17 then
      begin
        New(FRCampos);
        FRCampos.Field := 'SomenteVersaoAtu';
        FRCampos.Tipo := 'tinyint(1)';
        FRCampos.Null := '';
        FRCampos.Key := '';
        FRCampos.Default := '0';
        FRCampos.Extra := '';
        FLCampos.Add(FRCampos);
        //
      end;
    end
    else if Uppercase(Tabela) = Uppercase('wosassgru') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wordseropc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ModPad';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ModPadApl';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'StatPad';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DirFTPPad';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailAbe';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailCom';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailEnc';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailRes';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailSta';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UpdLocInte';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '60';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus1';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus2';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus3';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GruStatus4';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem_CamposFields';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := 'DATAHORAHIS DESC ';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem_CamposTxt';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := 'Data da �ltima modifica��o (Decrescente)';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      // T E A M  V I E W E R
      New(FRCampos);
      FRCampos.Field := 'UsaTeamViewer';
      FRCampos.Tipo := 'tinyint(1)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TokenTeamViewer';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'GrupoTeamViewer';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'URLHelpTeamViewer';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wosopcusu') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Usuario';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem_CamposFields';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := 'DATAHORAHIS DESC ';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem_CamposTxt';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := 'Data da �ltima modifica��o (Decrescente)';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'RecMailAbeEnc'; // Receber e-mail na abertura
      FRCampos.Tipo := 'tinyint(1)'; // e/ou encerramento da OS
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'RecMailCom';
      // Receber e-mail com coment�rios da OS quando n�o conectado
      FRCampos.Tipo := 'tinyint(1)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'RecNotifyAppDesktop'; // Receber notifica��es
      FRCampos.Tipo := 'tinyint(1)'; // no aplicativo
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'AutorizOS'; // As OS's dever�o ser autorizadas
      FRCampos.Tipo := 'tinyint(1)'; // antes de serem enviadas
      FRCampos.Null := ''; // Somente login tipo Cliente
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTrei') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHora'; // UTC
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Encerramento';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Inicio';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Termino';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Entidade';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Solicitante';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Valor';
      FRCampos.Tipo := 'double(15,2)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0.00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Modo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Instrutor';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoUsr';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoAut';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Requisitos';
      // Se tiver colocar um aceite exclusivo para os termos
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Observ';
      // Privado ser� mostrado apenas para o instrutor
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Status'; // 000 = Cadastrando (Cinza)
      FRCampos.Tipo := 'int(11)'; // 101 = Aguardando aprova��o (Amarelo)
      FRCampos.Null := ''; // 102 = Aguardando execu��o (Verde)
      FRCampos.Key := ''; // 301 = N�o aprovado (Vermelho)
      FRCampos.Default := '0'; // 302 = Finalizado (Azul)
      FRCampos.Extra := ''; // 303 = Cancelado (Vermelho)
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Encerrado'; // 0 = N�o
      FRCampos.Tipo := 'tinyint(1)'; // 1 = Sim
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoAceite';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHoraAceite';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UsuarioAceite';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'IPAceite'; // IP usu�rio que autorizou
      FRCampos.Tipo := 'varchar(15)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '127.0.0.1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Termo_Hash'; // Hash do arquivo publicado SHA1
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Autoriz'; // 0 = N�o
      FRCampos.Tipo := 'tinyint(1)'; // 1 = Sim
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '-1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'NAutorizMot'; // Motivo em caso de n�o autoriza��o
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ARemoto_Url';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ARemoto_Token';
      FRCampos.Tipo := 'varchar(32)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMail'; // 0 - N�o enviar e-mail respons�veis
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Enviar e-mail respons�veis
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTreiIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Topico';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DuracaoApr';
      FRCampos.Tipo := 'time';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Status'; // 0 = Em aberto
      FRCampos.Tipo := 'int(11)'; // 1 = Finalizado
      FRCampos.Null := ''; // 2 = N�o finalizado
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Observ';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Duracao';
      FRCampos.Tipo := 'time';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHoraBase'; // Campo apenas para c�lculo
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTreiLnk') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome'; // URL do link
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTreiUsr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Usuario';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Confirma��o de participa��o
      FRCampos.Field := 'ConfPar'; // 0 = N�o confirmou
      FRCampos.Tipo := 'tinyint(1)'; // 1 = Confirmou
      FRCampos.Null := ''; // 2 = Recusou
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos); // Confirma��o de presen�a
      FRCampos.Field := 'Presente'; // 0 = N�o presente
      FRCampos.Tipo := 'tinyint(1)'; // 1 = Presente
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'ConfParMot'; // Motivo em caso recuse participa��o
      FRCampos.Tipo := 'text';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoAceite';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHoraAceite';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'UsuarioAceite';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'IPAceite'; // IP usu�rio que autorizou
      FRCampos.Tipo := 'varchar(15)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '127.0.0.1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Termo_Hash'; // Hash do arquivo publicado SHA1
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMail'; // 0 - N�o enviar e-mail usu�rios
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Enviar e-mail usu�rios
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EnvMailLink'; // 0 - N�o enviar e-mail link
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Enviar e-mail link
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTreiModo') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTreiOpc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'HorasCfgAut'; // Confirmar X horas antes - Autoriza��o
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'HorasCfgPar'; // Confirmar X horas antes - Participa��o
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoAut'; // Termo padr�o - Autoriza��o
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TermoPar'; // Termo padr�o - Participa��o
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EmailAut'; // E-mail - Autoriza��o
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EmailPar'; // E-mail - Participa��o
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'EmailTrei'; // E-mail - Treinamento
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WTreiStat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wfaqits') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '?';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Texto';
      FRCampos.Tipo := 'mediumtext';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Tags';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '?';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Liberado'; // 1 => Acesso ao tutorial sem senha
      FRCampos.Tipo := 'tinyint(1)'; // 2 => Acesso ao tutorial com senha
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Status'; // 0 => Cadastrando
      FRCampos.Tipo := 'tinyint(1)'; // 1 => Finalizado
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'WFaqGru';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wfaqitsapl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Aplicativo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Modulo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wfaqitshis') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Usuario';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Util'; // 0 - N�o
      FRCampos.Tipo := 'tinyint(1)'; // 1 - Sim
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'DataHora';
      FRCampos.Tipo := 'datetime';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0000-00-00 00:00:00';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('wfaqgru') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(50)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '?';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Caminho';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'CaminhoID';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Parent';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nivel';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '1';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Origem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Ordem';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('WFaqOpc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'TextoPad';
      FRCampos.Tipo := 'mediumtext';
      FRCampos.Null := 'YES';
      FRCampos.Key := '';
      FRCampos.Default := '';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('whelp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Nome';
      FRCampos.Tipo := 'varchar(100)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '?';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Link';
      FRCampos.Tipo := 'varchar(255)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '?';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('whelpapl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field := 'Codigo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Controle';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := 'YES';
      FRCampos.Key := 'PRI';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field := 'Aplicativo';
      FRCampos.Tipo := 'int(11)';
      FRCampos.Null := '';
      FRCampos.Key := '';
      FRCampos.Default := '0';
      FRCampos.Extra := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnHelp_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      (*
        New(FRCampos);
        FRCampos.Field      := 'usr_sessions';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
      *)
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnHelp_Tabs.CompletaListaFRJanelas(FRJanelas: TJanelas;
  FLJanelas: TList<TJanelas>): Boolean;
begin
  // HLP-GEREN-001 :: Gerenciamento de Ajudas
  New(FRJanelas);
  FRJanelas.ID := 'HLP-GEREN-001';
  FRJanelas.Nome := 'FmHelpGer';
  FRJanelas.Descricao := 'Gerenciamento de Ajudas';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // HLP-CADAS-001 :: Cadastro de Ajudas
  New(FRJanelas);
  FRJanelas.ID := 'HLP-CADAS-001';
  FRJanelas.Nome := 'FmHelpCad';
  FRJanelas.Descricao := 'Cadastro de Ajudas';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // HLP-CADAS-002 :: Cadastro de Tutoriais
  New(FRJanelas);
  FRJanelas.ID := 'HLP-CADAS-002';
  FRJanelas.Nome := 'FmHelpFaq';
  FRJanelas.Descricao := 'Cadastro de Tutoriais';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // HLP-CADAS-003 :: Tutoriais - Pesquisa
  New(FRJanelas);
  FRJanelas.ID := 'HLP-CADAS-003';
  FRJanelas.Nome := 'FmHelpFaqPsq';
  FRJanelas.Descricao := 'Tutoriais - Pesquisa';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // HLP-OPCAO-001 :: Configura��es de Ajudas
  New(FRJanelas);
  FRJanelas.ID := 'HLP-OPCAO-001';
  FRJanelas.Nome := 'FmHelpOpc';
  FRJanelas.Descricao := 'Configura��es de Ajudas';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  /// ////////////////// WORDSER - IN�CIO ///////////////////////////////////////
  //
  // WEB-ORSER-001 :: Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-001';
  FRJanelas.Nome := 'FmWOrdSer';
  FRJanelas.Descricao := 'Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-002 :: Solicita��es da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-002';
  FRJanelas.Nome := 'FmWOrdSerIts';
  FRJanelas.Descricao := 'Solicita��es da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-003 :: Status da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-003';
  FRJanelas.Nome := 'FmWOrdSerSta';
  FRJanelas.Descricao := 'Status da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-005 :: Coment�rios da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-005';
  FRJanelas.Nome := 'FmWOrdSerCom';
  FRJanelas.Descricao := 'Coment�rios da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-006 :: Grupos de Assuntos
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-006';
  FRJanelas.Nome := 'FmWOrdSerAssGru';
  FRJanelas.Descricao := 'Grupos de Assuntos';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-007 :: Op��es Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-007';
  FRJanelas.Nome := 'FmWOrdSerOpc';
  FRJanelas.Descricao := 'Op��es Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-008 :: Encerramento de Item da O. S.
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-008';
  FRJanelas.Nome := 'FmWOrdSerEnc';
  FRJanelas.Descricao := 'Encerramento de Item da O. S.';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-009 :: Modos de atendimento da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-009';
  FRJanelas.Nome := 'FmWOrdSerMod';
  FRJanelas.Descricao := 'Modos de atendimento da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-010 :: Estat�sticas da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-010';
  FRJanelas.Nome := 'FmWOrdSerImp';
  FRJanelas.Descricao := 'Estat�sticas da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-011 :: Resolu��es da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-011';
  FRJanelas.Nome := 'FmWOrdSerSol';
  FRJanelas.Descricao := 'Resolu��es da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-012 :: Atendentes da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-012';
  FRJanelas.Nome := 'FmWOrdSerAte';
  FRJanelas.Descricao := 'Atendentes da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-014 :: Atendentes da Ordem de Servi�o - Par�metros
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-014';
  FRJanelas.Nome := 'FmWOrdSerAtePar';
  FRJanelas.Descricao := 'Atendentes da Ordem de Servi�o - Par�metros';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-015 :: Assuntos da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-015';
  FRJanelas.Nome := 'FmWOrdSerAss';
  FRJanelas.Descricao := 'Assuntos da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-016 :: Status de Item da O. S.
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-016';
  FRJanelas.Nome := 'FmWOrdSerStaGer';
  FRJanelas.Descricao := 'Status de Item da O. S.';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-017 :: Inclui Solicita��o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-017';
  FRJanelas.Nome := 'FmWOrdSerIncRap';
  FRJanelas.Descricao := 'Inclui Solicita��o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-018 :: Respons�vel da Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-018';
  FRJanelas.Nome := 'FmWOrdSerRes';
  FRJanelas.Descricao := 'Respons�vel da Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-019 :: Grupos de atendentes
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-019';
  FRJanelas.Nome := 'FmWOrdSerAteParGr';
  FRJanelas.Descricao := 'Grupos de atendentes';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-020 :: Grupos de Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-020';
  FRJanelas.Nome := 'FmWOrdSerGru';
  FRJanelas.Descricao := 'Grupos de Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-021 :: Itens do Grupo de Ordem de Servi�o
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-021';
  FRJanelas.Nome := 'FmWOrdSerGruIts';
  FRJanelas.Descricao := 'Itens do Grupo de Ordem de Servi�o';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-ORSER-022 :: Ordem de Servi�o - Tarefas
  New(FRJanelas);
  FRJanelas.ID := 'WEB-ORSER-022';
  FRJanelas.Nome := 'FmWOrdSerTar';
  FRJanelas.Descricao := 'Ordem de Servi�o - Tarefas';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  /// ////////////////// WORDSER - FIM //////////////////////////////////////////
  //
  /// ////////////////// WTREI - IN�CIO /////////////////////////////////////////
  //
  // WEB-WTREI-001 :: Treinamentos - Op��es
  New(FRJanelas);
  FRJanelas.ID := 'WEB-WTREI-001';
  FRJanelas.Nome := 'FmWTreiOpc';
  FRJanelas.Descricao := 'Treinamentos - Op��es';
  FRJanelas.Modulo := 'HELP';
  FLJanelas.Add(FRJanelas);
  //
  /// ////////////////// WTREI - FIM ////////////////////////////////////////////
  //
  /// ///////////////////////////////////////////////////////////////////////////
  /// ///////////////////////////////////////////////////////////////////////////
  Result := True;
  /// ///////////////////////////////////////////////////////////////////////////
  /// ///////////////////////////////////////////////////////////////////////////
end;

end.
