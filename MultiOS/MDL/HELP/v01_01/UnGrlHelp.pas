unit UnGrlHelp;

interface

uses System.Generics.Collections, System.SysUtils, System.Classes, Data.DB,
  System.UITypes, System.Variants, UnDmkEnums, UnInternalConsts, undmkprocfunc;

type
  THelpTip = (thtHelp=0, thtFAQ=1, thtAll=2);
  TUnGrlHelp = class(TObject)
  private
    { Private declarations }
    function  ObtemTagVideo(Txt: String): String;
    function  ObtemTagImg(Txt: String): String;
    function  ObtemTagUrl(Txt: String): String;
    function  SubstituiTag(Txt: WideString; TagI, TagF: String;
              TipoTag: Integer): WideString;
  public
    { Public declarations }
    // A J U D A S
    function  InsUpdAjuda(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Nome: String; Status, Nivel, Ativo: Integer;
              var Codigo: Integer): Boolean;
    function  InsUpdAjudaTopico(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Nome: String; Codigo, Ativo: Integer;
              var Controle: Integer): Boolean;
    function  UpdAjudaTopicoTutorial(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Controle, HelpFaq: Integer): Boolean;
    function  ExcluiHelp(DataBase: TComponent; Query: TDataSet;
              Codigo: Integer; Msg: String): Boolean;
    function  ExcluiHelpFaq(DataBase: TComponent; Query: TDataSet;
              Codigo: Integer; Msg: String): Boolean;
    function  ExcluiHelpRestr(DataBase: TComponent; Query: TDataSet;
              Codigo: Integer; Msg: String): Boolean;
    function  ExcluiAjudaTopico(DataBase: TComponent; Query: TDataSet;
              Controle: Integer; Msg: String): Boolean;
    procedure ReopenHelpCab(DataBase: TComponent; Query: TDataSet);
    procedure ReopenHelpTopic(DataBase: TComponent; Query: TDataSet;
              Codigo: Integer);
    procedure ReopenHelpOpc(DataBase: TComponent; Query: TDataSet);
    // F A Q
    function  InsUpd_FAQ(DataBase: TComponent; Query: TDataSet; Device: TDeviceType;
              Nome, Tags: String; Tipo, Nivel, Status, Ativo: Integer;
              var Codigo: Integer; CompNome: TComponent = nil): Boolean;
    function  Ins_FAQHis(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; DataHora: TDateTime; Usuario, Util,
              Codigo: Integer; var Controle: Integer): Boolean;
    function  Ins_HelpRestr(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Nome: String; Item, HelpResTip, Help: Integer;
              HelpTip: THelpTip; var Codigo: Integer): Boolean;
    function  ConfiguraStatus(MostraBranco: Boolean = False): TStringList;
    procedure ReopenHelpFAQ(DataBase: TComponent; Query: TDataSet;
              Codigo: Integer = 0; Nome: String = ''; Tags: String = '';
              HelpTip: THelpTip = thtAll);
    procedure ReopenHelpRestr(DataBase: TComponent; Query: TDataSet; Help,
              HelpTip, Codigo: Integer);
    // G E R A L
    procedure ReopenHelpRestricao(DataBase: TComponent; Query: TDataSet;
              HelpTip: Integer);
    // H T M L
    function  SubstituiTags(Texto: WideString): WideString;
  end;

var
  GrlHelp: TUnGrlHelp;
const
  CO_HELP_Separador = '|';
  CO_HELP_Img_EXT   = 'EXT';
  CO_HELP_Img_INT   = 'INT';
  CO_HELP_ImgTagI   = '[IMG]';
  CO_HELP_ImgTagF   = '[/IMG]';
  CO_HELP_ImgClass  = 'class="img-responsive" ';
  CO_HELP_VideoTagI = '[VIDEO]';
  CO_HELP_VideoTagF = '[/VIDEO]';
  CO_HELP_UrlTagI   = '[LINK]';
  CO_HELP_UrlTagF   = '[/LINK]';

implementation

uses UnGrl_DmkDB, UnGrl_Geral, UnGrl_Vars, UnGrlUsuarios;

{ TUnGrlHelp }

function TUnGrlHelp.ObtemTagUrl(Txt: String): String;
var
  Res, Target, Url: String;
  Lista: TStringList;
begin
  Res   := Txt;
  Lista := TStringList.Create;
  try
    Lista := Grl_Geral.Explode(Txt, CO_HELP_Separador, 0);
    //
    if Lista.Count > 0 then
    begin
      if Lista[0] = CO_HELP_Img_EXT then
      begin
        Url    := Lista[1];
        Target := 'TARGET="_blank" ';
      end else
      begin
        //N�o executar link para outro tutorial apenas links externos
        //futuramente colocar o url para visualiza��o online
        //Url    := Lista[1];
        Url    := '#';
        Target := '';
      end;
      //
      if Lista.Count = 3 then
        Res := '<A ' + Target + 'HREF="' + Url + '">' + Lista[2] + '</A>'
      else
        Res := '<A ' + Target + 'HREF="' + Url + '">' + Url + '</A>';
    end;
  finally
    Lista.Free;
  end;
  Result := Res;
end;

function TUnGrlHelp.ObtemTagVideo(Txt: String): String;
var
  Res: String;
  Lista: TStringList;
begin
  Res   := Txt;
  Lista := TStringList.Create;
  try
    Lista := Grl_Geral.Explode(Txt, CO_HELP_Separador, 0);
    //
    if Lista.Count > 0 then
    begin
      Res := '<div class="embed-responsive embed-responsive-16by9">' +
             '<iframe class="embed-responsive-item" frameborder="0" ' +
             'src="https://www.youtube.com/embed/' + Lista[0] +'"></iframe></div>';
    end;
  finally
    Lista.Free;
  end;
  Result := Res;
end;

function TUnGrlHelp.ObtemTagImg(Txt: String): String;
var
  Res: String;
  Lista: TStringList;
begin
  Res   := Txt;
  Lista := TStringList.Create;
  try
    Lista := Grl_Geral.Explode(Txt, CO_HELP_Separador, 0);
    //
    if Lista.Count > 0 then
    begin
      if Lista.Count = 2 then
        Res := '<IMG ' + CO_HELP_ImgClass + 'SRC="' + Lista[0] + '" ALT="' +
                 Lista[1] + '"/>'
      else
        Res := '<IMG ' + CO_HELP_ImgClass + 'SRC="' + Lista[0] + '"/>';
    end;
  finally
    Lista.Free;
  end;
  Result := Res;
end;

function TUnGrlHelp.SubstituiTag(Txt: WideString; TagI, TagF: String;
  TipoTag: Integer): WideString;
var
  NTxt: WideString;
  PosI, PosF: Integer;
  Texto: WideString;
  Tag: String;
begin
  Texto := Txt;
  PosI  := Pos(WideString(TagI), Texto);
  //
  if PosI > 0 then
  begin
    NTxt := Copy(Txt, 0, PosI - 1);
    PosF := Pos(WideString(TagF), Texto);
    //
    Tag := Copy(Txt, PosI + Length(TagI), PosF - (PosI + Length(TagI)));
    //
    if TipoTag = 0 then //Img
      Tag := ObtemTagImg(Tag)
    else
    if TipoTag = 1 then //Url
      Tag := ObtemTagUrl(Tag)
    else
    if TipoTag = 2 then //V�deo
      Tag := ObtemTagVideo(Tag);
    //
    NTxt := NTxt + Tag + Copy(Texto, PosF + Length(TagF));
  end else
    NTxt := Txt;
  //
  Result := NTxt;
end;

function TUnGrlHelp.SubstituiTags(Texto: WideString): WideString;
begin
  repeat
    Texto := SubstituiTag(Texto, CO_HELP_ImgTagI, CO_HELP_ImgTagF, 0);
  until (Pos(WideString(CO_HELP_ImgTagI), Texto) = 0);
  //
  repeat
    Texto := SubstituiTag(Texto, CO_HELP_UrlTagI, CO_HELP_UrlTagF, 1);
  until (Pos(WideString(CO_HELP_UrlTagI), Texto) = 0);
  //
  repeat
    Texto := SubstituiTag(Texto, CO_HELP_VideoTagI, CO_HELP_VideoTagF, 2);
  until (Pos(WideString(CO_HELP_VideoTagI), Texto) = 0);
  //
  Result := Texto;
end;

function TUnGrlHelp.InsUpd_FAQ(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; Nome, Tags: String; Tipo, Nivel, Status, Ativo: Integer;
  var Codigo: Integer; CompNome: TComponent = nil): Boolean;
var
  SQLTipo: TSQLType;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Nome = '', CompNome, 'Defina o nome da ajuda!') then Exit;
  if Grl_Geral.FIC(Tipo <= 0, nil, 'Defina o tipo da ajuda!') then Exit;
  if Grl_Geral.FIC(Status < 0, nil, 'Defina o status da ajuda!') then Exit;
  if Grl_Geral.FIC(Nivel <= 0, nil, 'Defina o n�vel de permiss�o!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := Grl_DmkDB.ObtemCodigoInt_Sinc('helpfaq', 'Codigo', Query,
               DataBase, stIns, Device);
  end else
  begin
    SQLTipo := stUpd;
  end;
  if Codigo <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, SQLTipo, 'helpfaq',
              False, ['Nome', 'Tags', 'Tipo', 'Nivel', 'Status', 'Ativo'],
              ['Codigo'], [Nome, Tags, Tipo, Nivel, Status, Ativo], [Codigo],
              True, dmksqlinsInsOnly, '', Device, True);
  end;
end;

function TUnGrlHelp.Ins_FAQHis(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; DataHora: TDateTime; Usuario, Util, Codigo: Integer;
  var Controle: Integer): Boolean;
var
  DataHora_Txt: String;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Codigo = 0, nil, 'C�digo n�o informado!') then Exit;
  if Grl_Geral.FIC(Usuario = 0, nil, 'Usu�rio n�o informado!') then Exit;
  //
  Controle := Grl_DmkDB.ObtemCodigoInt_Sinc('helpfaqhis', 'Controle', Query,
                DataBase, stIns, Device);
  //
  DataHora_Txt := Grl_Geral.FDT(DataHora, 109);
  //
  if Controle <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, stIns, 'helpfaqhis',
                False, ['DataHora', 'Usuario', 'Util', 'Codigo'], ['Controle'],
                [DataHora_Txt, Usuario, Util, Codigo], [Controle], True,
                dmksqlinsInsOnly, '', Device, True);
  end;
end;

function TUnGrlHelp.Ins_HelpRestr(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; Nome: String; Item, HelpResTip, Help: Integer;
  HelpTip: THelpTip; var Codigo: Integer): Boolean;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Help = 0, nil, 'C�digo n�o informado!') then Exit;
  if Grl_Geral.FIC(Item = 0, nil, 'Item n�o informado!') then Exit;
  if Grl_Geral.FIC(HelpResTip = -1, nil, 'Tipo n�o informado!') then Exit;
  if Grl_Geral.FIC(Nome = '', nil, 'Nome n�o informado!') then Exit;
  //
  Codigo := Grl_DmkDB.ObtemCodigoInt_Sinc('helprestr', 'Codigo', Query, DataBase, stIns, Device);
  //
  if Codigo <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, stIns, 'helprestr',
                False, ['Item', 'Nome', 'Help', 'HelpTip', 'HelpResTip'], ['Codigo'],
                [Item, Nome, Help, Integer(HelpTip), HelpResTip], [Codigo], True,
                dmksqlinsInsOnly, '', Device, True);
  end;
end;

procedure TUnGrlHelp.ReopenHelpRestricao(DataBase: TComponent; Query: TDataSet;
  HelpTip: Integer);
var
  Var_UserEnt: Integer;
begin
  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT its.Codigo, its.Item, its.HelpResTip, ',
    'CONCAT(its.Nome, " (", tip.Nome, ")") Nome ',
    'FROM helprestr its ',
    'LEFT JOIN helprestip tip ON tip.Codigo = its.HelpResTip ',
    'WHERE its.CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND its.LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    'AND its.HelpTip=' + Grl_Geral.FF0(HelpTip),
    'GROUP BY its.HelpResTip, its.Item ',
    'ORDER BY its.Nome ',
    '']);
end;

procedure TUnGrlHelp.ReopenHelpCab(DataBase: TComponent; Query: TDataSet);
var
  Var_UserEnt: Integer;
begin
  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM helpcab ',
    'WHERE CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    'AND Ativo = 1 ',
    '']);
end;

procedure TUnGrlHelp.ReopenHelpRestr(DataBase: TComponent; Query: TDataSet;
  Help, HelpTip, Codigo: Integer);
var
  Var_UserEnt: Integer;
begin
  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT tip.Nome HelpResTip_TXT, its.* ',
    'FROM helprestr its ',
    'LEFT JOIN helprestip tip ON tip.Codigo = its.HelpResTip ',
    'WHERE its.CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND its.LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    'AND its.Help=' + Grl_Geral.FF0(Help),
    'AND its.HelpTip=' + Grl_Geral.FF0(HelpTip),
    '']);
  if Codigo > 0 then
    Query.Locate('Codigo', Codigo, []);
end;

procedure TUnGrlHelp.ReopenHelpTopic(DataBase: TComponent; Query: TDataSet;
  Codigo: Integer);
var
  Var_UserEnt: Integer;
begin
  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT top.*, faq.Texto ',
    'FROM helptopic top',
    'LEFT JOIN helpfaq faq ON faq.Codigo = top.HelpFaq',
    'WHERE top.CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND top.LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    'AND top.Ativo = 1 ',
    'AND top.Codigo = ' + Grl_Geral.FF0(Codigo),
    'ORDER BY top.Codigo, top.Ordem, top.Nivel ',
    '']);
end;

procedure TUnGrlHelp.ReopenHelpOpc(DataBase: TComponent; Query: TDataSet);
begin
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM helpopc ',
    '']);
end;

procedure TUnGrlHelp.ReopenHelpFAQ(DataBase: TComponent; Query: TDataSet;
  Codigo: Integer = 0; Nome: String = ''; Tags: String = '';
  HelpTip: THelpTip = thtAll);
var
  Var_UserEnt: Integer;
  ListaStatus, SQLCodigo, SQLNome, SQLTags, SQLHelpTip: String;
begin
  SQLCodigo  := '';
  SQLNome    := '';
  SQLTags    := '';
  SQLHelpTip := '';
  //
  ListaStatus := Grl_Geral.Implode(GrlHelp.ConfiguraStatus(), '", "');
  //
  if Codigo <> 0 then
    SQLCodigo := 'AND Codigo=' + Grl_Geral.FF0(Codigo);
  if Nome <> '' then
    SQLNome := 'AND Nome LIKE "%' + Nome + '%"';
  if Tags <> '' then
    SQLTags := 'AND Tags LIKE "%' + Tags + '%"';
  //
  if HelpTip = thtFAQ then
    SQLHelpTip := 'AND Tipo & 1'
  else if HelpTip = thtHelp then
    SQLHelpTip := 'AND Tipo & 2';
  //
  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM helpfaq',
    'WHERE CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    SQLCodigo,
    SQLNome,
    SQLTags,
    SQLHelpTip,
    'ORDER BY Nome',
    '']);
end;

function TUnGrlHelp.ConfiguraStatus(MostraBranco: Boolean = False): TStringList;
var
  Status: TStringList;
begin
  Status := TStringList.Create;
  try
    if MostraBranco = True then
      Status.Add('N�o informado');
    Status.Add('Cadastrando');
    Status.Add('Aguardando revis�o');
    Status.Add('Finalizado');
  finally
    Result := Status;
  end;
end;

function TUnGrlHelp.ExcluiAjudaTopico(DataBase: TComponent; Query: TDataSet;
  Controle: Integer; Msg: String): Boolean;
begin
  Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc(Msg, 'helptopic', 'Controle',
              Controle, DataBase) = idYes;
end;

function TUnGrlHelp.ExcluiHelpFaq(DataBase: TComponent; Query: TDataSet;
  Codigo: Integer; Msg: String): Boolean;
begin
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM helprestr ',
    'WHERE Help=' + Grl_Geral.FF0(Codigo),
    'AND HelpTip=' + Grl_Geral.FF0(Integer(thtFAQ)),
    '']);
  if Query.RecordCount = 0 then
  begin
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc(Msg, 'helpfaq', 'Codigo',
                Codigo, DataBase) = idYes;
  end else
  begin
    Grl_Geral.MB_Aviso('Este item n�o pode ser exclu�do!' + sLineBreak +
      'Motivo: Este item possu� restri��es!');
    Result := False;
  end;
end;

function TUnGrlHelp.ExcluiHelp(DataBase: TComponent; Query: TDataSet;
  Codigo: Integer; Msg: String): Boolean;
begin
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM helprestr ',
    'WHERE Help=' + Grl_Geral.FF0(Codigo),
    'AND HelpTip=' + Grl_Geral.FF0(Integer(thtHelp)),
    '']);
  if Query.RecordCount = 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
      'SELECT * ',
      'FROM helptopic ',
      'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
      '']);
    if Query.RecordCount = 0 then
    begin
      Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc(Msg, 'helpcab', 'Codigo',
                  Codigo, DataBase) = idYes;
    end else
    begin
      Grl_Geral.MB_Aviso('Este item n�o pode ser exclu�do!' + sLineBreak +
        'Motivo: Este item possu� restri��es!');
      Result := False;
    end;
  end else
  begin
    Grl_Geral.MB_Aviso('Este item n�o pode ser exclu�do!' + sLineBreak +
      'Motivo: Este item possu� restri��es!');
    Result := False;
  end;
end;

function TUnGrlHelp.ExcluiHelpRestr(DataBase: TComponent; Query: TDataSet;
  Codigo: Integer; Msg: String): Boolean;
begin
  Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc(Msg, 'helprestr', 'Codigo',
              Codigo, DataBase) = idYes;
end;

function TUnGrlHelp.InsUpdAjuda(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; Nome: String; Status, Nivel, Ativo: Integer;
  var Codigo: Integer): Boolean;
var
  SQLTipo: TSQLType;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Nome = '', nil, 'Defina o nome da ajuda!') then Exit;
  if Grl_Geral.FIC(Status < 0, nil, 'Defina o status da ajuda!') then Exit;
  if Grl_Geral.FIC(Nivel <= 0, nil, 'Defina o n�vel de permiss�o!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := Grl_DmkDB.ObtemCodigoInt_Sinc('helpcab', 'Codigo', Query,
               DataBase, stIns, Device);
  end else
  begin
    SQLTipo := stUpd;
  end;
  if Codigo <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, SQLTipo, 'helpcab',
              False, ['Nome', 'Status', 'Nivel', 'Ativo'], ['Codigo'], [Nome,
              Status, Nivel, Ativo], [Codigo], True, dmksqlinsInsOnly, '',
              Device, True);
  end;
end;

function TUnGrlHelp.UpdAjudaTopicoTutorial(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; Controle, HelpFaq: Integer): Boolean;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Controle = 0, nil, 'ID n�o informado!') then Exit;
  //
  if Controle <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, stUpd, 'helptopic',
              False, ['HelpFaq'], ['Controle'], [HelpFaq], [Controle], True,
              dmksqlinsInsOnly, '', Device, True);
  end;
end;

function TUnGrlHelp.InsUpdAjudaTopico(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; Nome: String; Codigo, Ativo: Integer;
  var Controle: Integer): Boolean;
var
  SQLTipo: TSQLType;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Codigo = 0, nil, 'C�digo n�o informado!') then Exit;
  if Grl_Geral.FIC(Nome = '', nil, 'Defina o nome do t�pico!') then Exit;
  //
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := Grl_DmkDB.ObtemCodigoInt_Sinc('helptopic', 'Controle', Query,
                DataBase, stIns, Device);
  end else
  begin
    SQLTipo := stUpd;
  end;
  if Controle <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, SQLTipo, 'helptopic',
              False, ['Nome', 'Ativo', 'Codigo'], ['Controle'],
              [Nome, Ativo, Codigo], [Controle], True, dmksqlinsInsOnly, '',
              Device, True);
  end;
end;

end.
