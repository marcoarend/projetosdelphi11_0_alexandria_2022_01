unit UnGrlAgenda;

interface

uses System.Generics.Collections, System.SysUtils, System.Classes, Data.DB,
  System.UITypes, System.Variants, UnDmkEnums, UnInternalConsts, undmkprocfunc;

type
  TListaAgenNegPos = (lanpNegativo=0, lanpPositivo=1, lanpTodos=2, lanpTodosSZero=3);
  TGrupoAgenTar = (gatTarefas=0, gatListas=1, gatRecorrentes=2);
  TUnGrlAgenda = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // T A R E F A S
    function  ExcluiTarefasLista(DataBase: TComponent; Query: TDataSet;
              Codigo: Integer; Msg: String): Boolean;
    function  ExcluiTarefasItem(DataBase: TComponent; Query: TDataSet;
              Controle: Integer; Msg: String; ExcluiOriID: Boolean = False): Boolean;
    function  InsUpdTarefasLista(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Nome, Descri: String; Empresa, Sincro,
              Ativo: Integer; var Codigo: Integer; CompNome: TComponent = nil;
              CompEmpresa: TComponent = nil): Boolean;
    function  InsUpdTarefasItem(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Nome, Descri, Recorrencia: String;
              Duracao, OriNum: Double; DataCad, DataExe: TDate; Prioridade,
              Respons, OriID, Codigo: Integer; var Controle: Integer;
              CompCodigo: TComponent = nil; CompNome: TComponent = nil;
              CompDataExe: TComponent = nil; CompPrioridade: TComponent = nil;
              CompRespons: TComponent = nil): Boolean;
    function  InsUpdTarefasItemParcial(DataBase: TComponent; Query: TDataSet;
              Device: TDeviceType; Descri: String; DataFim: TDate; Realizado,
              Finalizado, Status, Controle: Integer): Boolean;
    procedure ReopenAgenTarGer(DataBase: TComponent; Query: TDataSet;
              Ativo: Integer; NegPos: TListaAgenNegPos = lanpTodos);
    procedure ReopenAgenTarIts(DataBase: TComponent; Query: TDataSet;
              Codigo, Finalizado: Integer; Data: TDate);
    // L I S T A S
    function  ObtemListaPrioridades: TStringList;
    function  ObtemListaStatus: TStringList;
    function  ObtemListaCategorias: TStringList;
    function  ObtemGrupoID(Grupo: TGrupoAgenTar): Integer;
    function  ObtemGrupoNome(Grupo: TGrupoAgenTar): String;
  end;

var
  GrlAgenda: TUnGrlAgenda;
const
  CO_Agend_Categorias_Max = 1;
  CO_Agend_Categorias_Cod: array[0..CO_Agend_Categorias_Max] of integer = (0, 1);
  CO_Agend_Categorias_Txt: array[0..CO_Agend_Categorias_Max] of string = ('Avulso', 'Atendimento eletr�nico');
  CO_Agend_Prioridades: array[0..4] of string = ('Muito baixa', 'Baixa', 'Normal', 'Alta', 'Muito alta');
  CO_Agend_Status: array[0..3] of string = ('Aguardando execu��o', 'Em execu��o', 'Em fase de testes', 'Aguardando libera��o');

implementation

uses UnGrl_DmkDB, UnGrl_Geral, UnGrl_Vars, UnGrlUsuarios;

{ TUnGrlAgenda }

function TUnGrlAgenda.ExcluiTarefasItem(DataBase: TComponent; Query: TDataSet;
  Controle: Integer; Msg: String; ExcluiOriID: Boolean = False): Boolean;
var
  Continua: Boolean;
  SQL: String;
  Var_Usr_Id: Integer;
begin
  Result     := False;
  Continua   := False;
  Var_Usr_Id := VAR_WEB_USR_ID;
  //
  if Var_Usr_Id = 0 then
    Var_Usr_Id := VAR_API_ID_USUARIO;
  //
  if Var_Usr_Id > 0 then
  begin
    if ExcluiOriID = False then
      SQL := 'AND OriID=0 '
    else
      SQL := '';
    //
    Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
      'SELECT * ',
      'FROM agentarits ',
      'WHERE Controle=' + Grl_Geral.FF0(Controle),
      'AND (UserCad=' + Grl_Geral.FF0(Var_Usr_Id) + ' ',
      'OR Respons=' + Grl_Geral.FF0(Var_Usr_Id) + ') ',
      SQL,
      '']);
    Continua := Query.RecordCount > 0;
  end else
    Continua := True;
  //
  if Continua = True then
  begin
    {$IfDef DMK_FMX}
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite(Msg, 'agentarits',
              'Controle', Controle, DataBase) = idYes;
    {$Else}
    {$IfDef DB_SQLite}
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite(Msg, 'agentarits',
              'Controle', Controle, DataBase) = idYes;
    {$Else}
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc_MySQL(Msg, 'agentarits',
              'Controle', Controle, DataBase) = idYes;
    {$EndIf}
    {$EndIf}
  end else
    Grl_Geral.MB_Aviso('Exclus�o abortada! Este item n�o pode ser exclu�do.');
  //
  Query.Close;
end;

function TUnGrlAgenda.ExcluiTarefasLista(DataBase: TComponent; Query: TDataSet;
  Codigo: Integer; Msg: String): Boolean;
begin
  Result := False;
  //
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM agentarits',
    'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
    '']);
  //
  if Query.RecordCount = 0 then
  begin
    {$IfDef DMK_FMX}
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite(Msg, 'agentarcab',
              'Codigo', Codigo, DataBase) = idYes;
    {$Else}
    {$IfDef DB_SQLite}
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite(Msg, 'agentarcab',
              'Codigo', Codigo, DataBase) = idYes;
    {$Else}
    Result := Grl_DmkDB.ExcluiRegistroInt1_Sinc_MySQL(Msg, 'agentarcab',
              'Codigo', Codigo, DataBase) = idYes;
    {$EndIf}
    {$EndIf}
  end else
    Grl_Geral.MB_Aviso('Exclus�o abortada! Esta lista n�o pode ser exclu�da.');
  //
  Query.Close;
end;

function TUnGrlAgenda.InsUpdTarefasItem(DataBase: TComponent; Query: TDataSet;
  Device: TDeviceType; Nome, Descri, Recorrencia: String; Duracao,
  OriNum: Double; DataCad, DataExe: TDate; Prioridade, Respons, OriID,
  Codigo: Integer; var Controle: Integer; CompCodigo: TComponent = nil;
  CompNome: TComponent = nil; CompDataExe: TComponent = nil;
  CompPrioridade: TComponent = nil; CompRespons: TComponent = nil): Boolean;
var
  SQLTipo: TSQLType;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Codigo = 0, CompCodigo, 'Defina a lista!') then Exit;
  if Grl_Geral.FIC(Nome = '', CompNome, 'Defina a tarefa!') then Exit;
  if Grl_Geral.FIC(Prioridade < 0, CompPrioridade, 'Defina a prioridade!') then Exit;
  if Grl_Geral.FIC(Nome = '', CompRespons, 'Defina o respons�vel!') then Exit;
  if Grl_Geral.FIC((Codigo = -1) and (Recorrencia = ''), nil,
    'Para a lista "Tarefas recorrentes" voc� deve informar a recorr�ncia!') then Exit;
  //
  if DataExe > EncodeDate(1900, 01, 01) then
  begin
    if Grl_Geral.FIC(DataExe < Date, CompDataExe,
      'A data de execu��o deve ser maior que a data atual!') then Exit;
  end;
  //
  if Codigo <> -1 then //Lista de tarefas recorrentes
    Recorrencia := '';
  //
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := Grl_DmkDB.ObtemCodigoInt_Sinc('agentarits', 'Controle', Query,
                DataBase, stIns, Device);
  end else
  begin
    SQLTipo := stUpd;
  end;
  if Controle <> 0 then
  begin
    if Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, SQLTipo, 'agentarits',
      False, ['Nome', 'Descri', 'Recorrencia', 'Duracao', 'OriNum', 'Data',
      'DataExe', 'Prioridade', 'Respons', 'OriID', 'Codigo'], ['Controle'],
      [Nome, Descri, Recorrencia, Duracao, OriNum, Grl_Geral.FDT(DataCad, 1),
      Grl_Geral.FDT(DataExe, 1), Prioridade, Respons, OriID, Codigo], [Controle], True,
      dmksqlinsInsOnly, '', Device, True) then
    begin
      if SQLTipo = stIns then
      begin
        Grl_DmkDB.ExecutaSQLQuery0(Query, DataBase, [
          'UPDATE agentarcab ',
          'SET TotalIts = TotalIts + 1 ',
          'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
          '']);
      end;
      Result := True;
    end;
  end;
end;

function TUnGrlAgenda.InsUpdTarefasItemParcial(DataBase: TComponent;
  Query: TDataSet; Device: TDeviceType; Descri: String; DataFim: TDate;
  Realizado, Finalizado, Status, Controle: Integer): Boolean;
var
  DtaFim: String;
begin
  Result := False;
  //
  if Controle <> 0 then
  begin
    if DataFim = 0 then
      DtaFim := '0000-00-00'
    else
      DtaFim :=  Grl_Geral.FDT(DataFim, 1);
    //
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, stUpd, 'agentarits',
              False, ['Descri', 'DataFim', 'Realizado', 'Finalizado',
              'Status'], ['Controle'], [Descri, DtaFim, Realizado,
              Finalizado, Status], [Controle], True, dmksqlinsInsOnly, '',
              Device, True);
  end;
end;

function TUnGrlAgenda.InsUpdTarefasLista(DataBase: TComponent;
  Query: TDataSet; Device: TDeviceType; Nome, Descri: String; Empresa,
  Sincro, Ativo: Integer; var Codigo: Integer; CompNome: TComponent = nil;
  CompEmpresa: TComponent = nil): Boolean;
var
  SQLTipo: TSQLType;
begin
  Result := False;
  //
  if Grl_Geral.FIC(Nome = '', CompNome, 'Defina o nome da lista!') then Exit;
  if Grl_Geral.FIC(Empresa = 0, CompEmpresa, 'Defina a entidade!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := Grl_DmkDB.ObtemCodigoInt_Sinc('agentarcab', 'Codigo', Query,
               DataBase, stIns, Device);
  end else
  begin
    SQLTipo := stUpd;
  end;
  if Codigo <> 0 then
  begin
    Result := Grl_DmkDB.CarregaSQLInsUpd(Query, DataBase, SQLTipo, 'agentarcab',
              False, ['Nome', 'Empresa', 'Descri', 'Sincro', 'Ativo'], ['Codigo'],
              [Nome, Empresa, Descri, Sincro, Ativo], [Codigo], True,
              dmksqlinsInsOnly, '', Device, True);
  end;
end;

procedure TUnGrlAgenda.ReopenAgenTarGer(DataBase: TComponent; Query: TDataSet;
  Ativo: Integer; NegPos: TListaAgenNegPos = lanpTodos);
var
  Var_UserEnt: Integer;
  SQLPositivos, SQLAtivo: String;
begin
  case NegPos of
    lanpNegativo:
      SQLPositivos := 'AND Codigo < 0';
    lanpPositivo:
      SQLPositivos := 'AND Codigo > 0';
    lanpTodos:
      SQLPositivos := '';
    lanpTodosSZero:
      SQLPositivos := 'AND Codigo <> 0';
  end;

  if Ativo in [0,1] then
    SQLAtivo := 'AND Ativo=' + Grl_Geral.FF0(Ativo)
  else
    SQLAtivo := '';

  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;

  //Atualiza total de itens
  Grl_DmkDB.ExecutaSQLQuery0(Query, DataBase, [
    'UPDATE agentarcab atc ',
    'SET atc.TotalIts = ',
    '( ',
    'SELECT COUNT(Codigo) ',
    'FROM agentarits ',
    'WHERE Codigo = atc.Codigo ',
    'AND CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    'AND RecID=0',
    'AND Finalizado=0',
    ') ',
    'WHERE Codigo <> 0',
    SQLAtivo,
    '']);
  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM agentarcab',
    'WHERE Empresa IN(0,' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND CliIntSync IN(0, ' + Grl_Geral.FF0(Var_UserEnt) + ')',
    'AND LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    SQLAtivo,
    SQLPositivos,
    'ORDER BY Ordem, Nome',
    '']);
end;

procedure TUnGrlAgenda.ReopenAgenTarIts(DataBase: TComponent; Query: TDataSet;
  Codigo, Finalizado: Integer; Data: TDate);
var
  Var_UserEnt: Integer;
  SQLCodigo, SQLFinaliz, SQLDataExe: String;
begin
  if Data <> 0 then
    SQLDataExe := 'AND its.DataExe="' + Grl_Geral.FDT(Data, 1) + '"'
  else
    SQLDataExe := '';

  if Finalizado in [0,1] then
    SQLFinaliz := 'AND its.Finalizado=' + Grl_Geral.FF0(Finalizado)
  else
    SQLFinaliz := '';

  if Codigo <> 0 then
  begin
    SQLCodigo := 'AND its.Codigo=' + Grl_Geral.FF0(Codigo) + ' ';
    SQLCodigo := SQLCodigo + 'AND its.RecID=0 ';
  end else
  begin
    SQLCodigo := '';
    SQLCodigo := SQLCodigo + 'AND ((its.RecID=0 AND its.Recorrencia = "") ';
    SQLCodigo := SQLCodigo + 'OR (its.RecID<>0 AND its.Recorrencia=""))';
  end;

  Var_UserEnt := VAR_WEB_USR_ENT;
  //
  if Var_UserEnt = 0 then
    Var_UserEnt := VAR_API_ID_ENTIDADE;

  Grl_DmkDB.AbreSQLQuery0(Query, DataBase, [
    'SELECT its.*, ',
    'IF(sen.Funcionario = 0, sen.Login, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) ResponsTxt',
    'FROM agentarits its',
    'LEFT JOIN senhas sen ON sen.Numero=its.Respons',
    'LEFT JOIN entidades ent ON ent.Codigo = sen.Funcionario',
    'WHERE its.CliIntSync=' + Grl_Geral.FF0(Var_UserEnt),
    'AND its.LastAcao<>' + Grl_Geral.FF0(Integer(laDel)),
    SQLCodigo,
    SQLDataExe,
    SQLFinaliz,
    'ORDER BY its.Finalizado, its.DataExe ASC, its.Prioridade DESC, its.Data ASC, its.Status',
    '']);
end;

function TUnGrlAgenda.ObtemListaPrioridades: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(CO_Agend_Prioridades) - 1 do
      Lista.Add(CO_Agend_Prioridades[I]);
  finally
    Result := Lista;
  end;
end;

function TUnGrlAgenda.ObtemListaStatus: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(CO_Agend_Status) - 1 do
      Lista.Add(CO_Agend_Status[I]);
  finally
    Result := Lista;
  end;
end;

function TUnGrlAgenda.ObtemListaCategorias: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(CO_Agend_Categorias_Txt) - 1 do
      Lista.Add(CO_Agend_Categorias_Txt[I]);
  finally
    Result := Lista;
  end;
end;

function TUnGrlAgenda.ObtemGrupoID(Grupo: TGrupoAgenTar): Integer;
begin
  case Grupo of
    gatTarefas:
      Result := 0;
    gatListas:
      Result := 1;
    gatRecorrentes:
      Result := 2;
  end;
end;

function TUnGrlAgenda.ObtemGrupoNome(Grupo: TGrupoAgenTar): String;
begin
  case Grupo of
    gatTarefas:
      Result := 'Tarefas';
    gatListas:
      Result := 'Listas';
    gatRecorrentes:
      Result := 'Recorrentes';
  end;
end;

end.
