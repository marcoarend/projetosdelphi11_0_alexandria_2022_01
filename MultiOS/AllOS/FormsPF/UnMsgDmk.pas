unit UnMsgDmk;

interface

uses System.UITypes, FMX.Forms, FMX.Edit,
  Data.DB,
  UnGeral;
  {
  System.Classes, System.SysUtils,
  FireDAC.Comp.Client,
  FMX.Layouts, FMX.Memo,
  UnDmkEnums, Variants;
  }

type
  TUnMsgDmk = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraTexto(Texto: String);
  end;


var
  MsgDmk: TUnMsgDmk;

implementation

uses DmkMsg;

{ TUnMsgDmk }

procedure TUnMsgDmk.MostraTexto(Texto: String);
{$IF DEFINED(MSWINDOWS)}
var
  XM: TFmDmkMsg;
{$ENDIF}
begin
{$IF DEFINED(MSWINDOWS)}
  XM := TFmDmkMsg.Create(nil);
  XM.Memo1.Lines.Text := Texto;
  XM.ShowModal(
    procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
        //
      end;
      // Erro Memory Leak no ANDROID!
      XM.DisposeOf;
    end);
{$ELSEIF DEFINED(ANDROID)}
  Geral.MB_Info(Texto);
{$ENDIF}
end;

end.
