unit UnFormsPF;

interface

uses System.UITypes, FMX.Forms, FMX.Edit,
  Data.DB,
  UnGeral, dmkCompVars;
  {
  System.Classes, System.SysUtils,
  FireDAC.Comp.Client,
  FMX.Layouts, FMX.Memo,
  UnDmkEnums, Variants;
  }

type
  TUnFormsPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ObtemCodiNomeCadastro(const Qry: TDataSet;
              const KeyField, ListField: String;
              EdCodigo: TEdit; EdNome: TClearingEdit);
  end;


var
  FormsPF: TUnFormsPF;

implementation

uses DmkCNPsq1, UnMyObjects;

{ TUnFormsPF }

procedure TUnFormsPF.ObtemCodiNomeCadastro(const Qry: TDataSet;
const KeyField, ListField: String;
EdCodigo: TEdit; EdNome: TClearingEdit);
{
var
  XM: TFmDmkCNPsq1;
}
begin
  //Result := False;
{   Erro de Memory Leak com DisposeOf no Android!!!
  XM := TFmDmkCNPsq1.Create(nil);
  XM.FQry := Qry;
  XM.FKeyField := KeyField;
  XM.FListField := ListField;
  XM.FEdCodigo := EdCodigo;
  XM.FEdNome := EdNome;
  if XM.FQry <> nil then
  begin
    if XM.FQry.State <> TDataSetState.dsInactive then
    begin
      XM.PreencheLista();
      XM.ShowModal(
        procedure(ModalResult: TModalResult)
        begin
          if ModalResult = mrOK then
          begin
            if EdCodigo <> nil then
              EdCodigo.Text := XM.FKeyValue;
            if EdNome <> nil then
              EdNome.Text := XM.FListValue;
          end;
      / Erro Memory Leak!
          XM.DisposeOf;
        end);
    end else
      Geral.MB_Erro('"DataSet" fechado em  "TUnFormsPF.ObtemCodiNomeCadastro"!');
  end else
    Geral.MB_Erro('"DataSet" n�o definido em "TUnFormsPF.ObtemCodiNomeCadastro"!');
}
  if MyObjects.Definido_DmkCNPsq1(True) then
  begin
    VAR_FmDmkCNPsq1.FQry := Qry;
    VAR_FmDmkCNPsq1.FKeyField := KeyField;
    VAR_FmDmkCNPsq1.FListField := ListField;
    VAR_FmDmkCNPsq1.FEdCodigo := EdCodigo;
    VAR_FmDmkCNPsq1.FEdNome := EdNome;
    if VAR_FmDmkCNPsq1.FQry <> nil then
    begin
      if VAR_FmDmkCNPsq1.FQry.State <> TDataSetState.dsInactive then
      begin
        VAR_FmDmkCNPsq1.PreencheLista();
        VAR_FmDmkCNPsq1.ShowModal(
          procedure(ModalResult: TModalResult)
          begin
            if ModalResult = mrOK then
            begin
              if EdCodigo <> nil then
                EdCodigo.Text := VAR_FmDmkCNPsq1.FKeyValue;
              if EdNome <> nil then
                EdNome.Text := VAR_FmDmkCNPsq1.FListValue;
              //
              //Result := True;
            end;
            VAR_FmDmkCNPsq1.Hide;
          end);
      end else
        Geral.MB_Erro('"DataSet" fechado em  "TUnFormsPF.ObtemCodiNomeCadastro"!');
    end else
      Geral.MB_Erro('"DataSet" n�o definido em "TUnFormsPF.ObtemCodiNomeCadastro"!');
  end;
end;

end.
