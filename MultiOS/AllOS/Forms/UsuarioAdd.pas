unit UsuarioAdd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, Xml.xmldom,
  Xml.XMLIntf, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, Xml.adomxmldom, Xml.XMLDoc, FMX.Memo, UnGeral;

type
  TFmUsuarioAdd = class(TForm)
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    DetailLabel: TLabel;
    SBSaida: TSpeedButton;
    ListBoxItem1: TListBoxItem;
    EdLogin: TEdit;
    ListBoxItem2: TListBoxItem;
    EdSenha: TEdit;
    ListBoxItem3: TListBoxItem;
    BtCriar: TButton;
    XMLDocument1: TXMLDocument;
    IdHTTP1: TIdHTTP;
    ListBoxItem4: TListBoxItem;
    Memo1: TMemo;
    procedure SBSaidaClick(Sender: TObject);
    procedure BtCriarClick(Sender: TObject);
  private
    { Private declarations }
    procedure TesteDeviceIdentification(Codigo: Integer; Acesso: String);
  public
    { Public declarations }
  end;

var
  FmUsuarioAdd: TFmUsuarioAdd;

implementation

uses UnWebReq, UnDmkRDBMs, UnDmkEnums, UnMyListas, UnDmkDevice, ModApp;

{$R *.fmx}

procedure TFmUsuarioAdd.BtCriarClick(Sender: TObject);
var
  ResMsgCod: String;
  Node : IXMLNode;
  //
  //Entidade,
  Codigo: Integer;
  UserName, PassWord, Token, UsrID, UsrEnt: String;
  //
  Resp: TStringStream;
  Resposta: String;
begin
  Resp := TStringStream.Create('');
  try
    UserName := EdLogin.Text;
    PassWord := EdSenha.Text;
    Memo1.Text := Resp.DataString;
    Resposta := WebReq.SQLArrPOST_TxtStr(Self, 'REST_LoginUser', [
    'Usuario=' + UserName,
    'Senha=' + PassWord]);
    Memo1.Text := Resp.DataString;
    //
    XMLDocument1.LoadFromXML(Resposta);
    Node := XMLDocument1.DocumentElement.ChildNodes.FindNode('usuario');
    if Node <> nil then
    begin
      ResMsgCod := Node.ChildNodes['MsgCod'].Text;
      //
      if (ResMsgCod <> '') and (ResMsgCod = '100') then
      begin
        Codigo   := Geral.IMV(Trim(Node.ChildNodes['UsrCod'].Text));
        UsrID    := Trim(Node.ChildNodes['UsrID'].Text);
        UsrEnt   := Trim(Node.ChildNodes['UsrEnt'].Text);
        Token    := Trim(Node.ChildNodes['Token'].Text);
        //
        DmkRDBMs.SQLDelSimple(Self, dbsrcLocalServer, 'usuarios', [
          'Codigo'], [
          Codigo], '');
        //
        if DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, DmModApp.FDAppDB, stIns, 'usuarios', False, [
          'UsrEnt', 'UsrID', 'Token'(*UsrAPI*)], [
          'Codigo'(*srCod*), 'UserName', 'PassWord'
        ], [
          UsrEnt, UsrID, Token
        ], [
          Codigo, UserName, PassWord
        ], True) then
        begin
          TesteDeviceIdentification(Codigo, Token);
          Geral.MB_Info('Usu�rio criado com sucesso!');
          Close;
        end;
      end else
        Geral.MB_Aviso('Login incorreto!');
    end else
      Geral.MB_Erro('Falha ao criar usu�rio!');
  finally
    Resp.Free;
  end;
end;

procedure TFmUsuarioAdd.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUsuarioAdd.TesteDeviceIdentification(Codigo: Integer; Acesso: String);
const
  Metodo = 'REST_DeviceIdentification';
var
  AplicativoSolicitante, IDUsuarioSolicitante, Token, DeviceID, DeviceName,
  DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion: String;
  Resposta: String;
begin
  AplicativoSolicitante := 'AplicativoSolicitante=' + Geral.FF0(CO_DMKID_APP);
  IDUsuarioSolicitante := 'IDUsuarioSolicitante=' + Geral.FF0((*VAR_WEB_USER_ID*)Codigo);
  Token := 'Token=' + (*VAR_WEB_TOKEN_DMK*)Acesso;
  DeviceID := 'DeviceID=' + DmkDevice.ObtemIMEI_MD5();
  DmkDevice.ObtemDadosAparelho(OSName, DeviceName, OSVersion, OSNickName);
  DeviceName := 'DeviceName=' + DeviceName;
  OSName := 'OSName=' + OSName;
  OSNickName := 'OSNickName=' + OSNickName;
  OSVersion := 'OSVersion=' + OSVersion;
  DvcScreenH := 'DvcScreenH=' + Geral.FF0(Screen.Size.Height);
  DvcScreenW := 'DvcScreenW=' + Geral.FF0(Screen.Size.Width);
  //
  Resposta := WebReq.SQLArrPOST_TxtStr(Self, Metodo, [
  AplicativoSolicitante, IDUsuarioSolicitante, Token, DeviceID, DeviceName,
  DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion]);
  //
  //Geral.MB_Aviso(Resposta);
end;

end.
