unit DmkLogin;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.Menus, FireDAC.Comp.Client, UnGeral, UnGrl_Vars;

type
  TFmDmkLogin = class(TForm)
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    Image1: TImage;
    SBMenu: TSpeedButton;
    ListBoxItem1: TListBoxItem;
    EdLogin: TEdit;
    ListBoxItem2: TListBoxItem;
    EdSenha: TEdit;
    ListBoxItem3: TListBoxItem;
    BtLogin: TButton;
    LBMenu: TListBox;
    ListBoxItem4: TListBoxItem;
    ListBoxItem5: TListBoxItem;
    Timer1: TTimer;
    procedure SBMenuClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBoxItem5Click(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
    procedure BtLoginClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraVerifyDB();
    procedure MostraUsuarioAdd();
    function  RealizaLogin(Usuario, Senha: String): Boolean;
  public
    { Public declarations }
  end;

var
  FmDmkLogin: TFmDmkLogin;

implementation

{$R *.fmx}

uses VerifyDB, UsuarioAdd, ModApp, UnDmkRDBMs, UnApp_Vars, Principal,
  UnSQLite_PF;

procedure TFmDmkLogin.BtLoginClick(Sender: TObject);
begin
  if not RealizaLogin(EdLogin.Text, EdSenha.Text) then
    Geral.MB_Aviso('Login incorreto!')
  else
    Close;
end;

procedure TFmDmkLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if VAR_WEB_TOKEN_DMK = '' then
    Application.Terminate;
end;

procedure TFmDmkLogin.FormCreate(Sender: TObject);
begin
  LBMenu.Visible := False;
end;

procedure TFmDmkLogin.FormShow(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

procedure TFmDmkLogin.ListBoxItem4Click(Sender: TObject);
begin
  MostraUsuarioAdd();
  //
  LBMenu.Visible := False;
end;

procedure TFmDmkLogin.ListBoxItem5Click(Sender: TObject);
begin
  MostraVerifyDB();
  //
  LBMenu.Visible := False;
end;

procedure TFmDmkLogin.MostraVerifyDB;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.Show;
  {$ELSE}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.ShowModal;
  FmVerifyDB.Destroy;
  {$ENDIF}
end;

function TFmDmkLogin.RealizaLogin(Usuario, Senha: String): Boolean;
var
  Qry: TFDQuery;
  Codigo: Integer;
  Token: String;
begin
  Result := False;
  Qry    := TFDQuery.Create(DmModApp.FDAppDB);
  try
    DmkRDBMs.AbreSQLQuery0(Self, Qry, DmModApp.FDAppDB, [
      'SELECT * FROM usuarios ',
      'WHERE Username=''' + Usuario + '''',
      'AND PassWord=''' + Senha + '''',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      Token  := Qry.FieldByName('Token').AsString;
      //
      VAR_WEB_USER_ID   := Codigo;
      VAR_WEB_TOKEN_DMK := Token;
      (*
      VAR_AGENTE_LOGADO := 1339; // Provisorio
      Geral.MB_Aviso('Agente provis�rio: ' + Geral.FF0(VAR_AGENTE_LOGADO));
      *)
      //
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmDmkLogin.MostraUsuarioAdd;
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmUsuarioAdd, FmUsuarioAdd);
  FmUsuarioAdd.Show;
  {$ELSE}
  Application.CreateForm(TFmUsuarioAdd, FmUsuarioAdd);
  FmUsuarioAdd.ShowModal;
  FmUsuarioAdd.Destroy;
  {$ENDIF}
end;

procedure TFmDmkLogin.SBMenuClick(Sender: TObject);
begin
  LBMenu.Visible := not LBMenu.Visible;
  //
  if LBMenu.Visible then
  begin
    LBMenu.ApplyStyleLookup;
    LBMenu.RealignContent;
  end;
end;

procedure TFmDmkLogin.Timer1Timer(Sender: TObject);
var
  Usuario, Senha: String;
begin
  Timer1.Enabled := False;
  //
  SQLite_PF.AbreSQLQuery0(Self, DmModApp.QrAux, DmModApp.FDAppDB, [
    'SELECT * FROM usuarios ',
    '']);
  if DmModApp.QrAux.RecordCount = 1 then
  begin
    //Caso s� tenha um usu�rio faz login autom�tico
    Usuario := DmModApp.QrAux.FieldByName('UserName').AsString;
    Senha   := DmModApp.QrAux.FieldByName('PassWord').AsString;
    //
    if RealizaLogin(Usuario, Senha) then
      Close;
  end
  else if DmModApp.QrAux.RecordCount = 0 then
  begin
    MostraUsuarioAdd;
  end;
end;

end.
