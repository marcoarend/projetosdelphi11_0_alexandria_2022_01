unit SincroDB;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo, Xml.xmldom,
  Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, FireDAC.Comp.Client, UnAllOS_DmkDB;

type
  TFmSincroDB = class(TForm)
    Label1: TLabel;
    ToolBar1: TToolBar;
    SBSincro: TSpeedButton;
    SBSaida: TSpeedButton;
    IdHTTP1: TIdHTTP;
    XMLDocument1: TXMLDocument;
    PnDados: TPanel;
    PB1: TProgressBar;
    AI1: TAniIndicator;
    procedure SBSaidaClick(Sender: TObject);
    procedure SBSincroClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function  ImportarDadosWeb(TabelaOri, TabelaDes, SQLQuery: String): Boolean;
    function  ExecutaSincronismo_Upload(TabelasOri, TabelasDes: array of String;
              Device: TDeviceType): Boolean;
    procedure ExecutaSincronismo_Download(TabelasOri, TabelasDes, SQLs,
              PrefixoTab: Array of String; Device: TDeviceType);
  public
    { Public declarations }
  end;

var
  FmSincroDB: TFmSincroDB;

implementation

uses Principal, UnAllOS_DmkWeb, MyListas, UnGeral, UnGrl_Vars,
  UnDmkEnums, UnAllOS_DmkProcFunc;

{$R *.fmx}

procedure TFmSincroDB.ExecutaSincronismo_Download(TabelasOri, TabelasDes, SQLs,
  PrefixoTab: array of String; Device: TDeviceType);
var
  i, Conexao, Resp, Total: Integer;
  TabelaOri, TabelaDes, SQL, SQLSinc, Prefixo, Msg: String;
  DataSincro: TDateTime;
begin
  if Device <> stMobile then
  begin
    Geral.MB_Aviso('Sincronismo n�o implementado para este tipo de dispositivo!');
    Exit;
  end;

  Total := Length(TabelasOri);

  PB1.Max     := Total;
  PB1.Value   := 0;
  PB1.Visible := True;
  AI1.Visible := True;
  AI1.Enabled := True;

  Conexao := AllOS_dmkWeb.VerificaConexaoWeb(Msg);

  try
    if Conexao = -1 then
    begin
      Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
        'Seu dispositivo n�o est� conectado na internet!');
      Exit;
    end else
    if Conexao = 2 then
    begin
      Resp := Geral.MB_Pergunta('Seu dispositivo est� conectado na internet atrav�s de dados m�veis!' +
        sLineBreak + 'Deseja continuar?');

      if Resp <> mrYes then
        Exit;
    end;

    if Length(TabelasOri) <> Length(SQLs) then
    begin
      Geral.MB_Erro('Total de Tabelas difere do total de SQLs');
      Exit;
    end;

    for i := 0 to Length(TabelasOri) - 1 do
    begin
      TabelaOri  := TabelasOri[i];
      TabelaDes  := TabelasDes[i];
      SQL        := SQLs[i];
      DataSincro := AllOS_dmkDB.ObtemDataSincro(Self, FmPrincipal.MyDB,
                      istSQLite, TabelaOri);

      if DataSincro > 0 then
      begin
        Prefixo := PrefixoTab[i];
        SQLSinc := 'AND '+ Prefixo +'LastModifi > "' + Geral.FDT(DataSincro, 105) + '"';
        SQL     := StringReplace(SQL, '[LastModifi]', SQLSinc, [rfReplaceAll, rfIgnoreCase]);
      end else
        SQL := StringReplace(SQL, '[LastModifi]', ' ', [rfReplaceAll, rfIgnoreCase]);

      if not ImportarDadosWeb(TabelaOri, TabelaDes, SQL) then
      begin
        Geral.MB_Aviso('Falha ao realizar sincronismo!');
        Exit;
      end;

      PB1.Value := PB1.Value + 1;
      Application.ProcessMessages;
    end;
  finally
    AI1.Visible := False;
    AI1.Enabled := False;
    PB1.Visible := False;
  end;
end;

function TFmSincroDB.ExecutaSincronismo_Upload(TabelasOri,
  TabelasDes: array of String; Device: TDeviceType): Boolean;
var
  Conexao, Resp, i, j, k, l, m, n, o, p, q, TotTabelas, MsgCod: Integer;
  TabelaOri, Coluna, Msg, SQLWhere: String;
  Query, QueryUpd: TFDQuery;
  XML: TStringStream;
  Colunas, ColunasPri: TStringList;
  LastModifi, LastAcao, ColunasTxt, ColunasPriTxt, ColunasVal, ColunasValPri: String;
begin
  Result := False;

  if Device <> stMobile then
  begin
    Geral.MB_Aviso('Sincronismo n�o implementado para este tipo de dispositivo!');
    Exit;
  end;

  Conexao := AllOS_dmkWeb.VerificaConexaoWeb(Msg);

  if Conexao = -1 then
  begin
    Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end else
  if Conexao = 2 then
  begin
    Resp := Geral.MB_Pergunta('Seu dispositivo est� conectado na internet atrav�s de dados m�veis!' +
      sLineBreak + 'Deseja continuar?');

    if Resp <> mrYes then
      Exit;
  end;
  TotTabelas := Length(TabelasOri);
  Colunas    := TStringList.Create;
  ColunasPri := TStringList.Create;
  Query      := TFDQuery.Create(FmPrincipal.MyDB);
  QueryUpd   := TFDQuery.Create(FmPrincipal.MyDB);
  XML        := TStringStream.Create('');

  PB1.Max     := TotTabelas;
  PB1.Value   := 0;
  PB1.Visible := True;
  AI1.Visible := True;
  AI1.Enabled := True;

  try
    for i := 0 to TotTabelas - 1 do
    begin
      TabelaOri := TabelasOri[i];

      AllOS_dmkDB.AbreSQLQuery0(Self, Query, FmPrincipal.MyDB, [
        'SELECT * FROM ' + TabelaOri + ' WHERE AlterWeb = 2 ',
        '']);

      if Query.RecordCount > 0 then
      begin
        AllOS_dmkDB.ObtemListaDeColunasDeTabela(Self, FmPrincipal.MyDB,
          istSQLite, TabelaOri, stNaoPrimarias, Colunas);
        AllOS_dmkDB.ObtemListaDeColunasDeTabela(Self, FmPrincipal.MyDB,
          istSQLite, TabelaOri, stPrimarias, ColunasPri);

        Query.First;

        while not Query.Eof do
        begin
          //Obtem lista de valores de colunas n�o prim�rias
          for j := 0 to Colunas.Count - 1 do
          begin
            Coluna := Colunas[j];

            if j = 0 then
              ColunasVal := AllOS_dmkPF.VariantToString(Query.FieldByName(Coluna).AsVariant)
            else
              ColunasVal := ColunasVal + '; ' + AllOS_dmkPF.VariantToString(Query.FieldByName(Coluna).AsVariant);
          end;

          //Obtem lista de valores de colunas prim�rias
          for k := 0 to ColunasPri.Count -1 do
          begin
            if k = 0 then
              ColunasValPri := AllOS_dmkPF.VariantToString(Query.FieldByName(ColunasPri[k]).AsVariant)
            else
              ColunasValPri := ColunasValPri + '; ' + AllOS_dmkPF.VariantToString(Query.FieldByName(ColunasPri[k]).AsVariant);
          end;

          //Obtem lista de nomes de colunas n�o prim�rias
          for l := 0 to Colunas.Count -1 do
          begin
            if l = 0 then
              ColunasTxt := Colunas[l]
            else
              ColunasTxt := ColunasTxt + '; ' + Colunas[l];
          end;

          //Obtem lista de nomes de colunas prim�rias
          for m := 0 to ColunasPri.Count - 1 do
          begin
            if m = 0 then
              ColunasPriTxt := ColunasPri[m]
            else
              ColunasPriTxt := ColunasPriTxt + '; ' + ColunasPri[m];
          end;

          //Obtem LastModifi
          for n := 0 to Colunas.Count - 1 do
          begin
            Coluna := Colunas[n];

            if LowerCase(Coluna) = 'lastmodifi' then
            begin
              LastModifi := AllOS_dmkPF.VariantToString(Query.FieldByName(Colunas[n]).AsVariant);
              Break;
            end;
          end;

          //Obtem LastAcao
          for o := 0 to Colunas.Count do
          begin
            Coluna := Colunas[o];

            if LowerCase(Coluna) = 'lastacao' then
            begin
              LastAcao := AllOS_dmkPF.VariantToString(Query.FieldByName(Colunas[o]).AsVariant);
              Break;
            end;
          end;
          if AllOS_dmkWeb.URLPost(CO_URL_DMKAPI,
            [
            'method=REST_Dmk_SincroUpload',
            'AplicativoSolicitante=' + Geral.FF0(CO_DMKID_APP),
            'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USER_ID),
            'Token=' + VAR_WEB_TOKEN_DMK,
            'Colunas=' + ColunasTxt,
            'ColunasPri=' + ColunasPriTxt,
            'ColunasVal=' + ColunasVal,
            'ColunasValPri=' + ColunasValPri,
            'Tabela=' + TabelaOri,
            'LastModifi=' + LastModifi,
            'LastAcao=' + LastAcao,
            'CodUser=' + Geral.FF0(VAR_WEB_USER_ID)
            ], XML) then
          begin
            XMLDocument1.XML.Clear;
            XMLDocument1.LoadFromStream(XML);

            for q := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
            begin
              with XMLDocument1.DocumentElement.ChildNodes[q] do
              begin
                MsgCod   := Geral.IMV(ChildNodes['MsgCod'].Text);
                SQLWhere := '';

                if MsgCod = 104 then
                begin
                  for p := 0 to ColunasPri.Count -1 do
                  begin
                    if p = 0 then
                      SQLWhere := SQLWhere + ' WHERE '
                    else
                      SQLWhere := SQLWhere + ' AND ';

                    SQLWhere := SQLWhere + ColunasPri[p] + '="'+ AllOS_dmkPF.VariantToString(Query.FieldByName(ColunasPri[p]).AsVariant) +'"';

                    if AllOS_dmkDB.ExecutaSQLQuery0(Self, QueryUpd, FmPrincipal.MyDB, [
                      'DELETE FROM ' + TabelaOri + ' ' + SQLWhere,
                      '']) then
                    begin
                      Result := True;
                    end else
                    begin
                      Result := False;
                      Exit;
                    end;
                  end;
                end else
                  Exit;
              end;
            end;
          end;
          PB1.Value := PB1.Value + 1;
          Application.ProcessMessages;

          Query.Next;
        end;
      end else
        Result := True;
    end;
  finally
    AI1.Enabled := False;
    AI1.Visible := False;
    PB1.Visible := False;

    Colunas.Free;
    ColunasPri.Free;
    Query.Free;
    QueryUpd.Free;
    XML.Free;
  end;
end;

procedure TFmSincroDB.FormCreate(Sender: TObject);
begin
  PB1.Visible := False;
  AI1.Visible := False;
  AI1.Enabled := False;
end;

function TFmSincroDB.ImportarDadosWeb(TabelaOri, TabelaDes, SQLQuery: String): Boolean;
var
  Acao: TSQLType;
  Insere: Boolean;
  Query: TFDQuery;
  XML: TStringStream;
  SQL, NodeNome, NodeValor, DataHora, KeyPri, KeyPriVal: String;
  ItsNode: IXmlNode;
  i, j, k, TotNodes, MsgCod, idx, idxpri: Integer;
  Campos, CamposPri: Array of String;
  Valores, ValoresPri: Array of Variant;
  KeysPri, KeysPriVal: TStringList;
begin
  Query      := TFDQuery.Create(FmPrincipal.MyDB);
  XML        := TStringStream.Create('');
  SQL        := SQLQuery;
  KeysPri    := TStringList.Create;
  KeysPriVal := TStringList.Create;
  KeyPri     := '';
  KeyPriVal  := '';
  Acao       := stIns;

  try
    AllOS_dmkDB.AbreSQLQuery0(Self, Query, FmPrincipal.MyDB, [
      'SELECT * FROM ' + TabelaDes,
      '']);

    if not AllOS_dmkDB.ObtemListaDeColunasDeTabela(Self,
      FmPrincipal.MyDB, istSQLite, TabelaDes, stPrimarias, KeysPri) then
    begin
      Geral.MB_Erro('[0] Falha ao realizar sincronismo!');
      Result := False;
      Exit;
    end;

    if AllOS_dmkWeb.URLPost(CO_URL_DMKAPI,
      [
      'method=REST_Dmk_ObtemResultadoSQL',
      'AplicativoSolicitante=' + Geral.FF0(CO_DMKID_APP),
      'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USER_ID),
      'Token=' + VAR_WEB_TOKEN_DMK,
      'Titulo=' + TabelaOri,
      'SQLType=6', //6
      'SQL=' + SQL
      ], XML) then
    begin
      XMLDocument1.XML.Clear;
      XMLDocument1.LoadFromStream(XML);

      if XMLDocument1.DocumentElement.HasAttribute('DataHora') then
        DataHora := XMLDocument1.DocumentElement.Attributes['DataHora']
      else
        DataHora := '';

      if XMLDocument1.DocumentElement.ChildNodes.FindNode('MsgCod') <> nil then
      begin
        MsgCod := Geral.IMV(XMLDocument1.DocumentElement.ChildNodes[i].ChildNodes['MsgCod'].Text);

        if MsgCod = 201 then //Falha na autentica��o!
        begin
          AllOS_dmkDB.RemoveUsuarios(Self, FmPrincipal.MyDB, istSQLite);
          Close;
          Exit;
        end;
      end;

      for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
      begin
        ItsNode  := XMLDocument1.DocumentElement.ChildNodes[i];
        TotNodes := ItsNode.ChildNodes.Count;

        SetLength(Campos, TotNodes);
        SetLength(Valores, TotNodes);

        SetLength(CamposPri, KeysPri.Count);
        SetLength(ValoresPri, KeysPri.Count);

        idx    := 0;
        idxpri := 0;

        KeysPriVal.Clear;

        for j := 0 to TotNodes - 1 do
        begin
          NodeNome  := ItsNode.ChildNodes[j].NodeName;
          NodeValor := ItsNode.ChildNodes[j].Text;

          if KeysPri.Count > 0 then
          begin
            for k := 0 to KeysPri.Count - 1 do
            begin
              if NodeNome = KeysPri[k] then
              begin
                KeysPriVal.Add(NodeValor);

                SetLength(CamposPri, idxpri + 1);
                CamposPri[idxpri] := KeysPri[k];

                SetLength(ValoresPri, idxpri + 1);
                ValoresPri[idxpri] := NodeValor;

                idxpri := idxpri + 1;
              end else
              begin
                SetLength(Campos, idx + 1);
                Campos[idx] := NodeNome;

                SetLength(Valores, idx + 1);
                Valores[idx] := NodeValor;

                idx := idx + 1;
              end;
            end;
          end;
        end;

        if Query.RecordCount > 0 then
        begin
          if (KeysPri.Count > 0) and (KeysPriVal.Count > 0) then
          begin
            if KeysPri.Count = KeysPriVal.Count then
            begin
              for k := 0 to KeysPri.Count - 1 do
              begin
                if k = 0 then
                  KeyPri := KeysPri[k]
                else
                  KeyPri := KeyPri + '; ' +  KeysPri[k];
              end;

              for k := 0 to KeysPriVal.Count - 1 do
              begin
                if k = 0 then
                  KeyPriVal := KeysPriVal[k]
                else
                  KeyPriVal := KeyPriVal + ', ' + KeysPriVal[k];
              end;

              if Query.Locate(KeyPri, VarArrayOf([KeyPriVal]), []) then
              begin
                Acao   := stUpd;
                Insere := True;
              end else
                Insere := True;
            end else
            begin
              Geral.MB_Erro('[2] Falha ao realizar sincronismo!');
              Result := False;
              Exit;
            end;
          end else
            Insere := True;
        end else
          Insere := True;

        if Insere then
        begin
          if not AllOS_dmkDB.CarregaSQLInsUpd(Self, FmPrincipal.QrLoc, FmPrincipal.MyDB,
            Acao, TabelaDes, False, Campos, CamposPri, Valores, ValoresPri, True, False, '',
            stWeb) then
          begin
            Geral.MB_Erro('[1] Falha ao realizar sincronismo!');
            Result := False;
            Exit;
          end;
        end;
      end;
    end;
    if DataHora <> '' then
      AllOS_dmkDB.AtualizaSincroDB(Self, FmPrincipal.MyDB, istSQLite, TabelaDes, DataHora);

    KeysPri.Free;
    KeysPriVal.Free;
    XML.Free;
    Query.Free;

    Result := True;
  except
    KeysPri.Free;
    KeysPriVal.Free;
    XML.Free;
    Query.Free;

    Result := False;
  end;
end;

procedure TFmSincroDB.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSincroDB.SBSincroClick(Sender: TObject);
var
  WEventosSQL_Compl, WEventosSQL, WEventosItSQL, WEventosReSQL: String;
begin
  case CO_DMKID_APP of
    40: //dmkEventos
    begin
      if ExecutaSincronismo_Upload(['weventos', 'weventosit'], ['weventos', 'weventosit'], stMobile) then
      begin
        case VAR_WEB_USER_TIPO of
          3: //Usu�rio
          begin
            WEventosSQL_Compl := 'WHERE wei.Respons="'+ Geral.FF0(VAR_WEB_USER_ID) +'" AND wev.Encerrado = 0';
          end;
          2: //Respons�vel
          begin
            WEventosSQL_Compl := 'WHERE usr.Entidade="'+ Geral.FF0(VAR_WEB_USER_USRENT) +'" AND wev.Encerrado = 0';
          end
          else //Administrador e Parceiro
          begin
            WEventosSQL_Compl := 'WHERE wev.Encerrado = 0';
          end;
        end;

        WEventosItSQL := 'SELECT wei.* ' +
                         'FROM weventosit wei ' +
                         'LEFT JOIN weventos wev ON wev.Codigo = wei.Codigo ' +
                         'LEFT JOIN wusers usr ON usr.Codigo = wei.Respons ' +
                         WEventosSQL_Compl +
                         ' [LastModifi] ';

        WEventosSQL   := 'SELECT wev.* ' +
                         'FROM weventos wev ' +
                         'LEFT JOIN weventosit wei ON wei.Codigo = wev.Codigo ' +
                         'LEFT JOIN wusers usr ON usr.Codigo = wev.UserCad ' +
                         WEventosSQL_Compl +
                         ' [LastModifi] ' +
                         'GROUP BY wei.Codigo ';
        WEventosReSQL := 'SELECT Codigo, PersonalName Nome ' +
                         'FROM wusers ' +
                         'WHERE Entidade="'+ Geral.FF0(VAR_WEB_USER_USRENT) +'"';

        ExecutaSincronismo_Download(['weventos', 'weventosit', 'wusers'],
                           ['weventos', 'weventosit', 'weventosre'],
                           [WEventosSQL, WEventosItSQL, WEventosReSQL],
                           ['wei.', 'wei.', ''], stMobile);

        Geral.MB_Aviso('Sincronismo finalizado!');
      end else
        Geral.MB_Aviso('Falha ao realizar sincronismo!');
    end;
    else
      Geral.MB_Aviso('Sincronismo n�o implementado para este aplicativo!');
  end;
end;

end.
