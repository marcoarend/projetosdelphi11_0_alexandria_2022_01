unit VerifyDB;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.ListBox, FMX.Memo, System.IniFiles;

type
  TFmVerifyDB = class(TForm)
    ToolBar1: TToolBar;
    DetailLabel: TLabel;
    SBInicia: TSpeedButton;
    SBSaida: TSpeedButton;
    CkRecriar: TCheckBox;
    ListBox1: TListBox;
    LBIAvisoR1: TListBoxItem;
    LBIAvisoG1: TListBoxItem;
    LBIAvisoB1: TListBoxItem;
    LBIAvisoP1: TListBoxItem;
    LBITempoI: TListBoxItem;
    LBITempoF: TListBoxItem;
    LBITempoT: TListBoxItem;
    ListBoxItem1: TListBoxItem;
    Memo1: TMemo;
    ListBoxItem2: TListBoxItem;
    MeFldsNoNeed: TMemo;
    ListBoxItem3: TListBoxItem;
    MeTabsNoNeed: TMemo;
    ListBoxItem4: TListBoxItem;
    PB1: TProgressBar;
    LaAvisoR1: TLabel;
    LaAvisoG1: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoP1: TLabel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    procedure SBSaidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBIniciaClick(Sender: TObject);
  private
    { Private declarations }
    function IniFileReadString(A, B: String): String;
  public
    { Public declarations }
  end;

var
  FmVerifyDB: TFmVerifyDB;

implementation

uses UnMyListas, UnGeral, UnDmkRDBMs, UnGrl_Vars, UnDmkEnums, ModApp;

{$R *.fmx}

procedure TFmVerifyDB.FormShow(Sender: TObject);
begin
  Memo1.Lines.Clear;
end;

function TFmVerifyDB.IniFileReadString(A, B: String): String;
var
  Ini: TIniFile;
  Arquivo: String;
  Texto: TStringList;
begin
  Result := '0';
  Arquivo := Geral.DirAppAllOS('MoMoGa.ini');
  if not FileExists(Arquivo) then
  begin
    Texto := TStringList.Create;
    try
      Texto.Text := '[Teste]';
      Texto.SaveToFile(Arquivo);
    finally
      Texto.Free;
    end;
  end;
  if not FileExists(Arquivo) then
    ShowMessage('Arquivo n�o existe!');
  Ini := TIniFile.Create(Arquivo);
  try
    Result := Ini.ReadString('Versao', 'Numero', '0' );
  finally
    Ini.Free;
  end;
end;

procedure TFmVerifyDB.SBIniciaClick(Sender: TObject);
const
  LaAvisoP2 = nil;
  LaAvisoR2 = nil;
  LaAvisoB2 = nil;
  LaAvisoG2 = nil;
  LaTempoI = nil;
  LaTempoF = nil;
  LaTempoT = nil;
var
  Versao: Int64;
  Verifica: Boolean;
  Resp: Integer;
begin
  Memo1.Lines.Clear;
  Verifica := False;

  Versao := StrToInt64(IniFileReadString('',''));
  if Versao <= CO_VERSAO then Verifica := True
  else begin
    Resp := Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
    'banco de dados atual. N�o � recomendado executar a verifica��o com '+
    'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
    'Confirma assim mesmo a verifica��o?');
    if Resp = mrYes then Verifica := True;
  end;
  Application.ProcessMessages;
  if Verifica then
  begin
    if CkRecriar.IsChecked then
      DmkRDBMs.RecriaDatabase();
    //
    if DmkRDBMs.EfetuaVerificacoes(Self, VAR_APP_DB_TYPE, VAR_APP_DB_NAME_E_EXT,
      Memo1, True(*CkEstrutura.Checked*), True(*CkEstrutLoc.Checked*), True,
      True(*CkPergunta.Checked*), True(*CkRegObrigat.Checked*), LaAvisoP1,
      LaAvisoP2, LaAvisoR1, LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2,
      LaTempoI, LaTempoF, LaTempoT, MeTabsNoNeed, PB1, MeFldsNoNeed) then
    begin
      DmModApp.TbControle.Close;
      DmModApp.TbControle.Open();
      //
      DmkRDBMs.SQLInsUpd(Self, dbsrcLocalServer, DmModApp.FDAppDB, stUpd,
        'controle', False, ['Versao'], ['Codigo'], [CO_VERSAO], [1], True);
    end;
    CkRecriar.IsChecked := False;
  end;
end;

procedure TFmVerifyDB.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

end.
