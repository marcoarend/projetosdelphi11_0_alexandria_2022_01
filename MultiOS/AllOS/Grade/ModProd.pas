unit ModProd;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  UnDmkEnums;

type
  TDmModProd = class(TForm)
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function ObtemPreco(var Preco: Double): Boolean;
    procedure _teste(Valor: Double);
  end;

var
  DmModProd: TDmModProd;

implementation

uses UnMyObjects, UnGeral;

{$R *.fmx}

{ TDmModProd }

procedure TDmModProd.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack then
    Modalresult := mrCancel;
end;

function TDmModProd.ObtemPreco(var Preco: Double): Boolean;
var
  //Resultado,
  NaoInfo: Boolean;
  ValTxt: String;
  ValFlu: Double;
begin
  Preco := 0;
  Result := False;
  //
  NaoInfo := True;
  while NaoInfo do
  begin
    ValTxt := Geral.FFT(Preco, 2, siPositivo);
    Result := InputQuery('INP-QUERY-000 :: Digita��o de Valor de Produto',
    'Custo / pre�o:', ValTxt);
    if Result then
    begin
      ValFlu := Geral.DMV(ValTxt);
      NaoInfo := Geral.FFT(ValFlu, 2, siPositivo) <> ValTxt;
      if NaoInfo then
      begin
        Geral.MB_Aviso('Valor inv�lido: ' + ValTxt);
      end else
        Preco := ValFlu;
    end else
      NaoInfo := False;
  end;
end;

procedure TDmModProd._teste(Valor: Double);
begin
  Geral.MB_Info('Pre�o: ' + Geral.FFT(Valor, 2, siPositivo));
end;

end.

