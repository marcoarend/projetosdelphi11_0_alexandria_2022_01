unit UnDmkListas;

interface


uses
  UnDmkEnums;
{
  SysUtils, Classes, DB,
  UnDmkProcFunc, Variants;
}

const

  CO_COD_GraTabApp_NenhumGTA = 0; // para todos apps
  CO_TXT_GraTabApp_NenhumGTA = 'Cadastro sem v�nculo'; // para todos apps
  //
  MaxAgendaQuestao = 1;
  sListaAgendaQuestao: array[0..MaxAgendaQuestao]of String = ('Avulso', 'O.S.');
  //
  // Planner.pas linha 340
  //TPlannerGradientDirection = (gdHorizontal, gdVertical);
  MaxPlannerGradientDirection = 1;
  sPlannerGradientDirection: array[0..MaxAgendaQuestao]of String = (
    'Horizontal', 'Vertical');
  //

  CO_TXT_WhereOrAnd_00_Indef = '**Indef**';
  CO_TXT_WhereOrAnd_01_Where = 'WHERE';
  CO_TXT_WhereOrAnd_02_And   = 'AND';
  //
  MaxWhereOrAnd = 2;
  sWhereOrAnd: array[0..MaxWhereOrAnd]of String = (
    CO_TXT_WhereOrAnd_00_Indef,
    CO_TXT_WhereOrAnd_01_Where,
    CO_TXT_WhereOrAnd_02_And
  );
  //

  CO_TXT_StausPedVda_00_Indef      = 'Indefinido';
  CO_TXT_StausPedVda_01_Cancelado  = 'Cancelado';
  CO_TXT_StausPedVda_02_Suspenso   = 'Suspenso';
  CO_TXT_StausPedVda_03_Bloqueado  = 'Bloqueado';
  CO_TXT_StausPedVda_04_           = 'N�o implementado';
  CO_TXT_StausPedVda_05_Liberado   = 'Liberado';
  CO_TXT_StausPedVda_06_           = 'N�o implementado';
  CO_TXT_StausPedVda_07_           = 'N�o implementado';
  CO_TXT_StausPedVda_08_           = 'N�o implementado';
  CO_TXT_StausPedVda_09_Finalizado = 'Finalizado';
  //
  MaxStausPedVda = 9;
  sStausPedVda: array[0..MaxStausPedVda]of String = (
    CO_TXT_StausPedVda_00_Indef      ,
    CO_TXT_StausPedVda_01_Cancelado  ,
    CO_TXT_StausPedVda_02_Suspenso   ,
    CO_TXT_StausPedVda_03_Bloqueado  ,
    CO_TXT_StausPedVda_04_           ,
    CO_TXT_StausPedVda_05_Liberado   ,
    CO_TXT_StausPedVda_06_           ,
    CO_TXT_StausPedVda_07_           ,
    CO_TXT_StausPedVda_08_           ,
    CO_TXT_StausPedVda_09_Finalizado
  );
  //
  //
type
  //TTipoTroca = (trcOA, trcOsAs, trcPara);
  TUnDmkListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  DefineTituloCarta(Tipo: Integer; Curto: Boolean): String;
    function  TipoTextoCarta(Tipo, Index: Integer): String;
    function  StatusInformeInconsistente(Status: TInfoInconsSta): String;
  end;

var
  DmkListas: TUnDmkListas;

implementation

{ TUnDmkListas }

function TUnDmkListas.DefineTituloCarta(Tipo: Integer; Curto: Boolean): String;
begin
  if not Curto then
  begin
    case Tipo of
        0: Result := 'Cadastro de Cartas';
        1: Result := 'Cadastro de Contratos de Cursos';
        2: Result := 'Cadastro de Contratos de Loca��o';
        3: Result := 'Cadastro de Contratos de Turmas';
        4: Result := 'Contratos de Fomento Mercantil';
        5: Result := 'Contratos de Loca��o Imobili�ria';
        6: Result := 'Certid�es e Cartas';
        7: Result := 'Cartas de avalia��o';
        9: Result := 'Contratos de Loca��o de Equipamentos';
      else Result := 'T�tulo n�o definido';
    end;
  end else begin
    case Tipo of
        0: Result := 'Cadastro de Cartas';
        1: Result := 'Contratos de Cursos';
        2: Result := 'Contratos de Loca��o';
        3: Result := 'Contratos de Turmas';
        4: Result := 'Contratos de F.M.';
        5: Result := 'Contratos de Loc. Imobil.';
        6: Result := 'Certid�es e Cartas';
        7: Result := 'Cartas de avalia��o';
        9: Result := 'Contratos de Loca��o';
      else Result := 'T�tulo indefinido';
    end;
  end
end;

function TUnDmkListas.StatusInformeInconsistente(
  Status: TInfoInconsSta): String;
begin
  case Status of
    (*04*)infincstaAberto        : Result := 'Aberto';
    (*08*)infincstaEngano        : Result := 'Engano';
    (*12*)infincstaDesfeito      : Result := 'Desfeito';
    (*16*)infincstaReportado     : Result := 'Reportado';
    (*20*)infincstaIgnorado      : Result := 'ignorado';
    (*24*)infincstaNaoAceito     : Result := 'N�o aceito';
    (*28*)infincstaEmDiscussao   : Result := 'Em discuss�o';
    (*32*)infincstaDiscutido     : Result := 'Discutido';
    (*36*)infincstaAResolver     : Result := 'A resolver';
    (*40*)infincstaNaoResolvido  : Result := 'N�o resolvido';
    (*44*)infincstaInsoluvel     : Result := 'Insol�vel';
    (*48*)infincstaResolvido     : Result := 'Resolvido';
    else                           Result := '*** ERRO ERP ***';
  end;
end;

function TUnDmkListas.TipoTextoCarta(Tipo, Index: Integer): String;
begin
  case Tipo of
    4:
    begin
      case Index of
         00: Result := 'Inativo';
         01: Result := 'Contrato';
         02: Result := 'Aditivo';
         03: Result := 'Autoriza��o de Protesto';
         04: Result := 'Carta para Sacado';
         05: Result := 'Carta de Cancelamento de Protesto';
         06: Result := 'Carta para o Emitente do Cheque';
        else Result := 'Desconhecido';
      end;
    end;
    5:
    begin
      case Index of
         00: Result := 'Inativo';
         01: Result := 'T�tulo';
         02: Result := 'Par�grafo';
        else Result := 'Desconhecido';
      end;
    end;
    6: // Syndic
    begin
      case Index of
         00: Result := 'Inativo';
         01: Result := 'Certid�o negativa de d�bitos condominiais';
         02: Result := 'Carta de cobran�a de d�bitos condominiais';
        else Result := 'Desconhecido';
      end;
    end;
    7: // Lesew
    begin
      case Index of
         00: Result := 'Inativo';
         01: Result := 'Carta de avalia��o';
        else Result := 'Desconhecido';
      end;
    end;
    9: // ToolRent
    begin
      case Index of
         00: Result := 'Inativo';
         01: Result := 'Contrato de Loca��o';
        else Result := 'Desconhecido';
      end;
    end;
    else
      Result := '�nico';
  end;
end;

end.
