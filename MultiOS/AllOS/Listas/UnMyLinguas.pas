unit UnMyLinguas;

interface

uses
  System.Generics.Collections, UnDmkEnums;

type
  TLinguas = (myliPortuguesBR);
  TBDProcura = (bdpTabela, bdpCampo);
  TTipoListaTb = (tctCashier, tctBlueSkin);
  TTipoListasTb = set of TTipoListaTb;
  //
  TImpedeDel = ^TImpedeDelCampos;
  TImpedeDelCampos = record
    TableExcl : String;
    TableLook : String;
    CampoLook : String;
  end;

  TFoundInBD = ^TFoundInBDCampos;
  TFoundInBDCampos = record
    Tabela  : Boolean;
    CampoA  : Boolean;
    ValorA  : Variant;
    CampoS  : Boolean;
    ValorS  : Variant;
  end;

  TCampos = ^TCamposNomes;
  TCamposNomes = record
    Field    : String;
    Tipo     : String;
    Null     : String;
    Key      : String;
    Default  : String;
    Extra    : String;
    Coments  : String;
    Purpose  : TItemTuplePurpose;
  end;

  TTabelas = ^TFldTabelas;
  TFldTabelas = record
    Tem_Del  : Boolean;
    Tem_Sync : Boolean;
    Idx_CDR  : Boolean;
    TabCria  : String;
    TabBase  : String;
    TabItDel : String;
    QeiLnkOn : TCRCTableManage;
  end;

  TIndices = ^TCamposIndices;
  TCamposIndices = record
    //Table:        String;
    Key_name:     String;
    Non_unique:   Integer;
    Seq_in_index: Integer;
    Column_name:  String;
    //Collation:    String;
    //Cardinality:  Variant;
    //Sub_part:     Variant;
    //Pacote:       String;
    //Comment:      String;
  end;

  TQeiLnk = ^TFldQeiLnks;
  TFldQeiLnks = record
    AskrTab    : String;
    AskrCol    : String;
    RplyTab    : String;
    RplyCol    : String;
    MTbsCol    : String;
    Purpose    : TItemTuplePurpose;
    Seq_in_pref: Integer;
  end;

  TFldTabRec = ^TFldTabFld;
  TFldTabFld = record
    Codigo:    Integer;
    Valor:     Variant;
  end;

  TArqExpTED = ^TFldExpTED;
  TFldExpTED = record
    Registro:    Integer;
    SubReg:      Integer;
    Segmento:    String;
    SubSeg:      Integer;
    //
    Nome:        String;        
  end;

  TJanelas = ^TRegistrosJanelas;
  TRegistrosJanelas = record
    ID:        String;
    Nome:      String;
    Descricao: String;
    Modulo:    String;
    //Instance:  TComponentClass;
    //Reference: TForm;
  end;

  TUnMyLinguas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure GetInternationalStrings(Lingua: TLinguas);
    procedure AdTbLst(Lista: TList<TTabelas>; CampoDel: Boolean; TabCria,
              TabBase: String; CampoSync: Boolean = False;
              Idx_CDR: Boolean = False;
              CRCTableManage: TCRCTableManage = crctmStandBy;
              NomeTabItensDel: String = 'ctrlexcltb');

  end;

const
  CO_PORCENTAGEM = '%';
  CO_APOSTROFO   = Char( 39);
  CO_CORTE       = Char(216);
  CO_REGISTRADO  = Char(174);
  CO_EURO        = Char(128);
  CO_CIFRAO      = '$';
  //
  mycoErro001         = 'Tipo de campo n�o definido. Erro[001]';
  mycoErro002         = 'Vari�vel n�o definida %s. Erro[002]';
  mycoEstrutTab       = '# Estrutura da tabela ';
  mycoEdSenha         = 'EdSenha';
  mycoSenha           = 'Senha';
  mycoLogin           = 'Login';
  mycoFmLogoff        = 'FmLogoff';
  mycoWM_KEYDOWN      = '"WM_KEYDOWN"';
  mycoVarAsType       = 'VarAsType';
  mycoGRT2004         = 'GRT2004';
  mycoVerBackUp       = 'VER BACKUP SkinTanX 1.0';
  mycoTrue            = 'True';
  mycoFalse           = 'False';
  //
  mycoSELECT          = 'SELECT ';
  mycoSELECTUDOFROM = 'SELECT * FROM ';
  mycoFROM = 'FROM ';
  mycoWHERE           = 'WHERE ';
  mycoORDERBY         = 'ORDER BY ';
  mycoINSERT          = 'INSERT ';
  mycoUPDATE = 'UPDATE ';
  mycoDELETE          = 'DELETE ';
  mycoDROP            = ' DROP ';
  mycoADD             = ' ADD ';
  mycoCHANGE          = ' CHANGE ';
  mycoDROPTABLE       = 'DROP TABLE ';
  mycoALTERTABLE      = 'ALTER TABLE ';
  mycoP0Igual         = ' = :P0 ';
  mycoINTEGER         = 'INTEGER';
  mycoSTRING          = 'STRING';
  mycoVARCHAR         = 'VARCHAR(%d)';
  mycoNUMERIC         = 'NUMERIC(%d,%d)';
  mycoFLOAT           = 'FLOAT';
  mycoDATE            = 'DATE';

  //
  mycoKeyRegistroDoAplicativo = 'RegistroDoAplicativo';
  mycoKeyrkc          = 'rkc';
  mycoKeyTeclaEnter   = 'TeclaEnter';
  mycoKeyTeclaTab     = 'TeclaTab';
  mycoKeyTeclaPri     = 'TeclaPri';
  mycoKeyTeclaNex     = 'TeclaNex';
  mycoKeyMoedaLocal   = 'MoedaLocal';
  mycoKeyLogotipoUsa  = 'LogotipoUsa';
  mycoKeyLogotipoArq  = 'LogotipoArq';
  mycoKeyImpInfoRec   = 'ImpInfoRec';
  mycoKeyCasas1       = 'Casas1';
  mycoKeyCasas2       = 'Casas2';
  mycoKeyCasas3       = 'Casas3';
  mycoKeyCasas4       = 'Casas4';
  mycoKeyCasas5       = 'Casas5';
  mycoKeyCasas6       = 'Casas6';
  mycoKey1000         = '1000';
  mycoKey0100         = '0100';
  mycoKey0010         = '0010';
  mycoKey0001         = '0001';
  mycoKey00_1         = '00,1';
  mycoKey0_01         = '0,01';

  //
  mycoSimboloReal     = 'R$';
  mycoOpen            = 'open';
  mycoBarraInv        = '\';
  mycoEspaco          = ' ';
  mycoEspacos2        = '  ';
  mycoAspasDuplas     = '"';
  mycoColcheteA       = '[';
  mycoColcheteF       = ']';
  mycoIgual           = '=';
  mycoMais            = '+';
  mycoMenos           = '-';
  mycoTraco           = ' - ';
  mycoUnderline12     = '____________';
  mycoVirgula         = ',';
  mycoi               = ', ';
  mycoDoisPontos      = ':';
  mycoDoisPontos_     = ': ';
  mycoPorcent         = '%';
  mycoAsterisco       = '*';
  mycoCerca           = '#';
  mycoUnderline       = '_';
  mycoPonto           = '.';
  myco0               = '0';
  myco00              = '00';
  myco1               = '1';
  myco100             = '100';
  myco1000_           = '1000';
  mycoChaveIni        = '>>';
  mycoChaveFim        = '<<';
  mycoByUS            = '/U$';
  mycoBykg            = '/kg';
  mycoBymm            = '/mm';
  mycoBym2            = '/m�';
  mycoUSBym2          = 'U$/m�';
  mycoUSBykg          = 'U$/kg';
  mycoGrausCelsius    = '�C ';
  //
  mycoFNIgual         = 'FN=';
  mycoFTIgual         = 'FT=';
  mycoFLIgual         = 'FL=';
  mycoFRIgual         = 'FR=';
  mycoFIIgual         = 'FI=';
  mycoINIgual         = 'IN=';
  mycoIFIgual         = 'IF=';
  mycoIPIgual         = 'IP=';
  mycoIUIgual         = 'IU=';
  mycoIDIgual         = 'ID=';
  mycoIIIgual         = 'II=';
  mycoIMIgual         = 'IM=';
  //
  mycoVERS            = '[VERS]';
  mycoOBJE            = '[OBJE]';
  mycoHEAD            = '[HEAD]';
  mycoFORM            = '[FORM]';
  mycoForX            = '[For';
  mycoTitulo          = 'Titulo';
  mycoVersao          = 'Versao';
  mycoLines           = 'Lines';
  mycoCam             = 'Cam';
  mycoCli             = 'Cli';
  mycoCCl             = 'CCl';
  mycoEur             = 'Eur';
  mycoICM             = 'ICM';
  mycoCIC             = 'CIC';
  mycoCIP             = 'CIP';
  mycoCIm             = 'CIm';
  mycoQt              = 'Qt';
  mycoLi              = 'Li';
  mycoDt              = 'Dt';
  mycoSe              = 'Se';
  mycoAr              = 'Ar';
  mycoSu              = 'Su';
  mycoTe              = 'Te';
  mycoAc              = 'Ac';
  mycomm              = 'mm';
  mycokg              = 'kg';
  mycoPc              = 'Pc';
  mycom2              = 'm2';
  mycoOb              = 'Ob';
  mycoGr              = 'Gr';
  mycoFu              = 'Fu';
  mycoP1              = 'P1';
  mycoP2              = 'P2';
  mycoCo              = 'Co';
  mycoRo              = 'Ro';
  mycoLe              = 'Le';
  mycoTo              = 'To';
  mycoPo              = 'Po';
  mycoPd              = 'Pd';
  mycoGC              = 'GC';
  mycoDi              = 'Di';
  mycoPa              = 'Pa';
  mycoOp              = 'OP';
  mycopH              = 'pH';
  mycoBe              = 'Be';
  mycoMi              = 'Mi';
  mycoMa              = 'Ma';
  mycoPL              = 'PL';
  mycoPU              = 'PU';
  mycoPE              = 'PE';
  mycoIP              = 'IP';
  mycoCF              = 'CF';
  mycoRI              = 'RI';
  mycoRF              = 'RF';
  mycoVa              = 'Va';
  //             
  mycoProduto         = 'Produto';
  mycoPorcento        = 'Porcento';
  mycoPeso            = 'Peso';
  mycoRodar           = 'Rodar';
  mycoPreco           = 'Preco';
  mycoObs             = 'Obs';
  mycoProcesso        = 'Processo';
  mycoTemperat        = 'Temperat';
  mycoDiluicao        = 'Diluicao';
  mycoParar           = 'Parar';
  mycoCorte           = 'Corte';
  mycoCaleiro         = 'Caleiro';
  mycoCurtimento      = 'Curtimento';
  mycoRecurtimento    = 'Recurtimento';
  mycoAcabamento      = 'Acabamento';
  mycoCal             = 'Cal';
  mycoCur             = 'Cur';
  mycoRec             = 'Rec';
  mycoAca             = 'Aca';
  mycoCliente         = 'Cliente';
  mycoArtigo          = 'Artigo';
  mycoSubstrato       = 'Substrato';
  mycoTecnico         = 'Tecnico';
  mycoEspessura       = 'Espessura';
  myco_mm             = ' mm';
  mycoPesoT           = 'PesoT';
  myco_kg             = ' kg';
  mycoQuantia         = 'Quantia';
  mycoAccount         = 'Account';
  mycoRendimento      = 'Rendimento';
  myco_kgByPeca       = ' kg/pe�a';
  myco_kgByM2         = ' kg/m�';
  mycoCambio          = 'Cambio';
  mycoData            = 'Data';
  mycoObjetivo        = 'Objetivo';
  mycoTit_RSkg        = 'Tit_RSkg';
  mycoRS_kg           = 'RS_kg';
  mycoTit_USkg        = 'Tit_USkg';
  mycoUS_kg           = 'US_kg';
  mycoTit_RSm2        = 'Tit_RSm2';
  mycoRS_m2           = 'RS_m2';
  mycoTit_USm2        = 'Tit_USm2';
  mycoUS_m2           = 'US_m2';
  mycoTit_CustoT      = 'Tit_CustoT';
  mycoCustoT          = 'CustoT';
  mycoTit_TempoR      = 'Tit_TempoR';
  mycoTempoR          = 'TempoR';
  mycoTit_TempoP      = 'Tit_TempoP';
  mycoTempoP          = 'TempoP';
  mycoTit_TempoA      = 'Tit_TempoA';
  mycoTempoA          = 'TempoA';
  mycoTit_TempoE      = 'Tit_TempoE';
  mycoTempoE          = 'TempoE';
  mycoTit_TempoT      = 'Tit_TempoT';
  mycoTempoT          = 'TempoT';
  mycoNOME_RECEITA    = 'NOME_RECEITA';
  //                  
  mycoCell1           = 'Cell1';
  mycoCell2           = 'Cell2';
  mycoCell3           = 'Cell3';
  mycoTabExcl         = 'TabExcl';
  mycoTabLook         = 'TabLook';
  //
  mycoVARF_ETAPAHEIGHT = 'VARF_ETAPAHEIGHT';
  mycoVARF_ADDPQHEIGHT = 'VARF_ADDPQHEIGHT';
  mycoVARF_RODARHEIGHT = 'VARF_RODARHEIGHT';
  mycoVARF_OBSERHEIGHT = 'VARF_OBSERHEIGHT';
  mycoETAPA           = 'ETAPA';
  mycoADDPQ           = 'ADDPQ';
  mycoOBSER           = 'OBSER';

  mycoFRVer           = ' - %s [ Ver. %s] (Formato %d)  ';
  mycoNUM_FULAO       = 'NUM_FULAO';

  myco                = '';

  //
  mycoTabPrazos       = 'Prazos';
  mycoTabProdutos     = 'Produtos';
  mycoTabPrecos       = 'Precos';
  mycoTabPrecosEnti   = 'PrecosEnti';
  mycoTabPrecosProd   = 'PrecosProd';
  mycoTabPrecosItem   = 'PrecosItem';
  mycoTabPrazosIts    = 'PrazosIts';
  mycoTabEntidades    = 'Entidades';
  mycoTabContatos     = 'Contatos';
  mycoTabMoedas       = 'Moedas';
  mycoTabControle     = 'Controle';
  mycoTabLivres       = 'Livres';
  mycoTabExcluidos    = 'Excluidos';
  mycoTabAccounts     = 'Accounts';
  //
  myco_               = '';
  mycoTypeDataType    = ' DataType ';
  mycoTypeSize        = ' Size ';
  mycoTypePrecision   = ' Precision ';
  mycoTypeRequired    = ' Required ';
  mycoCampoCodigo     = 'Codigo';
  mycoCampoControle   = 'Controle';
  mycoCampoDescricao  = 'Descricao';
  mycoCampoNome       = 'Nome';
  mycoCampoData       = 'Data';
  mycoCampoCliente    = 'Cliente';
  mycoCampoProduto    = 'Produto';
  mycoCampoPrazoCli   = 'PrazoCli';
  mycoCampoPrazoFor   = 'PrazoFor';
  mycoCampoFornecedor = 'Fornecedor';
  mycoCampoCambioU    = 'CambioU';
  mycoCampoCambioE    = 'CambioE';
  mycoCampoAccount    = 'Account';
  mycoCampoMaster     = 'Master';
  mycoCampoVersao     = 'Versao';
  mycoCampoPrazos     = 'Prazos';
  mycoCampoLivres     = 'Livres';
  mycoCampoPrazosIts  = 'PrazosIts';
  mycoCampoEntidades  = 'Entidades';
  mycoCampoProdutos   = 'Produtos';
  mycoCampoPrecos     = 'Precos';
  mycoCampoContatos   = 'Contatos';
  mycoCampoSenhas     = 'Senhas';
  mycoCampoTabela     = 'Tabela';
  mycoCampoID         = 'ID';
  mycoCampoInteiro    = 'Inteiro';
  mycoCampoTexto      = 'Texto';
  mycoCampoFlutuante  = 'Flutuante';
  mycoCampoDataExc    = 'DataExc';
  mycoCampoUserExc    = 'UserExc';
  mycoCampoLogin      = 'Login';
  mycoCampoSenha      = 'Senha';
  mycoCampoTCli       = 'TCli';
  mycoCampoTFor       = 'TFor';
  mycoCampoRua        = 'Rua';
  mycoCampoNumero     = 'Numero';
  mycoCampoCompl      = 'Compl';
  mycoCampoBairro     = 'Bairro';
  mycoCampoCidade     = 'Cidade';
  mycoCampoCEP        = 'CEP';
  mycoCampoUF         = 'UF';
  mycoCampoPais       = 'Pais';
  mycoCampoEMail      = 'EMail';
  mycoCampoTel1       = 'Tel1';
  mycoCampoTel2       = 'Tel2';
  mycoCampoTel3       = 'Tel3';
  mycoCampoFax        = 'Fax';
  mycoCampoNivel      = 'Nivel';
  mycoCampoTradeMark  = 'TradeMark';
  mycoCampoCliCF30dd  = 'CliCF30dd';
  mycoCampoForCF30dd  = 'ForCF30dd';
  mycoCampoNegrito    = 'Negrito';
  mycoCampoEntidade   = 'Entidade';
  mycoCampoNatal      = 'Natal';
  mycoCampoCel1       = 'Cel1';
  mycoCampoCel2       = 'Cel2';
  mycoCampoCel3       = 'Cel3';
  mycoCampoCargo      = 'Cargo';
  mycoCampoParcelas   = 'Parcelas';
  mycoCampoMedia      = 'Media';
  mycoCampoDias       = 'Dias';
  mycoCampoPrecoL     = 'PrecoL';
  mycoCampoPrecoU     = 'PrecoU';
  mycoCampoPrecoE     = 'PrecoE';
  mycoCampoPremio     = 'Premio';
  mycoCampoReferencia = 'Referencia';
  mycoCampoIPI        = 'IPI';
  mycoCampoRIPI       = 'RIPI';
  mycoCampoICMS       = 'ICMS';
  mycoCampoRICMS      = 'RICMS';
  mycoCampoCal        = 'Cal';
  mycoCampoCur        = 'Cur';
  mycoCampoRec        = 'Rec';
  mycoCampoAca        = 'Aca';
  mycoCampoCTE        = 'CTE';
  mycoCampoEmbTipo    = 'EmbTipo';
  mycoCampoEmbPeso    = 'EmbPeso';
  mycoCampoTipo       = 'Tipo';
{
  mycoCampoLk         = 'Lk';
  mycoCampoDataCad    = 'DataCad';
  mycoCampoUserCad    = 'UserCad';
  mycoCampoDataAlt    = 'DataAlt';
  mycoCampoUserAlt    = 'UserAlt';
  mycoCampoAlterWeb   = 'AlterWeb';
  mycoCampoAtivo      = 'Ativo';
}
  mycoCampoTabela_ID  = 'Tabela, ID';
  //
  mycoArrayCodigoNome = 'Codigo;Nome';
  mycoArrayEntidadeControle = 'Entidade; Controle';
  mycoArrayCodigoControle = 'Codigo;Controle';
  //
  mycoTipoInteiro      = 'Inteiro';
  mycoTipoFlutuante    = 'Flutuante';
  mycoTipoTexto        = 'Texto';
  mycoTipoData         = 'Data';
  //
  mycoFormatFloat0     = '#,###,##0';
  mycoFormatFloat1     = '#,###,##0.0';
  mycoFormatFloat2     = '#,###,##0.00';
  mycoFormatFloat3     = '#,###,##0.000';
  mycoFormatFloat4     = '#,###,##0.0000';
  mycoFormatFloat5     = '#,###,##0.00000';
  mycoFormatFloat6     = '#,###,##0.000000';
  mycoFormatDateBR2    = 'dd/mm/yy';
  mycoFormatDateBR4    = 'dd/mm/yyyy';
  mycoFormatDateUS2    = 'mm/dd/yy';
  mycoFormatDateUS4    = 'mm/dd/yyyy';
  mycoFormatMyhhnn     = '00:00';
  //
  mycoPathGradesCalW   = '\Grades\Cal\Width';
  mycoPathArqRecent    = '\Recentes';
  mycoPathOpcoes       = '\Opcoes';
  mycoPathLocDef       = '\Opcoes\LocDef';
  mycoPathDermatek     = '\Dermatek';
  mycoPathLogFiletxt   = '\LogFile.txt';
  mycoNomeMenuAx       = 'A%s';
  mycoTextMenuAx       = '%s. %s';
  mycoPathExcelCurMeg  = 'Excel\Cur.meg';
  mycoPathExcelRecMeg  = 'Excel\Rec.meg';
  mycoExtDB            = '.DB'; 
  //
  mycoCol01            = 'Col01';
  mycoCol02            = 'Col02';
  mycoCol03            = 'Col03';
  mycoCol04            = 'Col04';
  mycoCol05            = 'Col05';
  mycoCol06            = 'Col06';
  mycoCol07            = 'Col07';
  mycoCol08            = 'Col08';
  mycoCol09            = 'Col09';
  mycoCol10            = 'Col10';
  mycoCol11            = 'Col11';
  mycoCol12            = 'Col12';
  mycoCol13            = 'Col13';
  mycoCol14            = 'Col14';
  mycoCol15            = 'Col15';
  mycoCol16            = 'Col16';
  mycoCol17            = 'Col17';
  mycoCol18            = 'Col18';
  mycoCol19            = 'Col19';
  mycoCol20            = 'Col20';
  mycoCol21            = 'Col21';
  mycoCol22            = 'Col22';
  mycoCol23            = 'Col23';
  mycoCol24            = 'Col24';
  mycoCol25            = 'Col25';
  mycoCol26            = 'Col26';
  mycoCol27            = 'Col27';
  mycoCol28            = 'Col28';
  mycoCol29            = 'Col29';
  mycoCol30            = 'Col30';
  mycoCol31            = 'Col31';
  mycoCol32            = 'Col32';
  mycoCol33            = 'Col33';
  mycoCol34            = 'Col34';
  mycoCol35            = 'Col35';
  mycoCol36            = 'Col36';
  mycoCol37            = 'Col37';
  mycoCol38            = 'Col38';
  mycoCol39            = 'Col39';
  //
  mycoPREC             = 'PREC';
  //
  mycoExcelApplication     = 'Excel.Application';
  mycoIniCellExcel         = '12:12';
  mycoExcelFormula1        = '=IF(B12="","",B12*$E$6)';
  mycoExcelFormula2        = '=IF(C12="","",G12*C12)';
  mycoExcelExt             = '.xls';
  //
  mycoVARF_MEMO1HEIGHT     = 'VARF_MEMO1HEIGHT';
  mycoVAR_LINE_A_WIDTH     = 'VAR_LINE_A_WIDTH';
  mycoVAR_LINE_A_TOP       = 'VAR_LINE_A_TOP';
  mycoVARF_ATIVA           = 'VARF_ATIVA';
  mycoVARF_LINHAHEIGHT     = 'VARF_LINHAHEIGHT';
  mycoVARF_DATAVISIBLE     = 'VARF_DATAVISIBLE';
  mycoVARF_LOGOUSAR        = 'VARF_LOGOUSAR';
  mycoVARF_LOGOFILE        = 'VARF_LOGOFILE';

  //
  mycoAvisaHorario         = 'Avisa Horario';
  //
  mycoINSERTControle001 = 'INSERT INTO controle (Versao) Values ("0")';
var
  MyLinguas             : TUnMyLinguas;
  ivTabPerfis         : String;
  //
  ivArquivoRKC        : String;
  ivArquivosSkinTan   : String;
  //
  VAR_FoundInBD       : TFoundInBD;
  ivTabPrazosIts      : String;
  ivTabContatos       : String;
  //
  ivCriado            : String;
  ivAlterado          : String;
  ivResponsavel       : String;
  ivCodigo            : String;
  ivControle          : String;
  ivNome              : String;
  ivPrazo             : String;
  ivPreco             : String;
  ivEntidade          : String;
  ivProduto           : String;
  ivMoeda             : String;
  ivparcela           : String;
  ivContato           : String;
  ivCodigoProduto     : String;
  ivCodigoCliente     : String;
  ivNomeProduto       : String;
  ivSequencia         : String;
  //
  ivDefA              : String;
  ivDefo              : String;
  ivInfoA             : String;
  ivInfoO             : String;
  ivCrieUmA           : String;
  //
  ivSenhaMaster       : String;
  //
  ivMsgVersaoDifere   : String;
  ivMsgVersao         : String;
  ivMsgVersaoBD       : String;
  ivMsgEstruturaBD    : String;
  ivMsgEstruturaBDLoc : String;
  ivMsgFimAnalBD      : String;
  ivMsgFimAnalBDLoc   : String;
  ivMsgERRAnalBD      : String;
  ivMsgRecriandoTabLoc: String;
  ivMsgTabCont        : String;
  ivMsgTabContFimAnal : String;
  ivMsgExclCancel0    : String;
  ivMsgExclCancel1    : String;
  ivMsgDesejaVer1     : String;
  ivMsgErroIniEdit    : String;
  ivMsgErroNovoReg    : String;
  ivMsgErroSalvarArq  : String;
  ivMsgErroProc       : String;
  ivMsgErroNomeRec    : String;
  ivMsgErroEm         : String;
  ivMsgObriNome       : String;
  ivMsgObriTipCadCk   : String;
  ivMsgObriTipCadRG   : String;
  ivMsgObriCambioU    : String;
  ivMsgObriCambioE    : String;
  ivMsgIPINeg         : String;
  ivMsgICMNeg         : String;
  ivMsgCambiJaExiste  : String;
  ivMsgRegNoLoc       : String;
  ivMsgTemPrecoData   : String;
  ivMsgFileNotFound   : String;
  ivMsgFileInvalid    : String;
  ivMsgErroLerArq     : String;
  ivMsgArqDif         : String;
  ivMsgSalvandoArquivo: String;
  ivMsgArqProntoUso   : String;
  ivMsgArqNaoSalvo    : String;
  ivMsgProntoUso      : String;
  ivMsgAbertura       : String;
  ivMsgADefinir       : String;
  ivMsgEmConstrucao   : String;
  ivMsgArqNaoLoc      : String;
  ivMsgCliNaoLoc      : String;
  ivMsgCliNaoDef      : String;
  ivMsgConfExclDeste  : String;
  ivMsgConfExclDesta  : String;
  ivMsgDiferenca      : String;
  ivMsgNaoExiste      : String;
  ivMsgSeraCriada     : String;
  ivMsgFoiCriada      : String;
  ivMsgTabCtrlOK      : String;
  ivMsgTabCtrlNoOK    : String;
  ivMsgInsLivres      : String;
  ivMsgErroTabMaster  : String;
  ivMsgErroTabControle: String;
  ivMsgOpcaoNaoDisp   : String;
  ivMsgValIniIndef    : String;

  //
  ivMsgImpReceita     : String;
  ivMsgImpCancelada   : String;
  ivMsgImpCancTit     : String;
  //
  ivAbrindo_          : String;
  ivAskConfIndefA     : String;
  ivAskConfIndefO     : String;
  ivAskConfProdInativ : String;
  ivMsgAskAltera      : String;
  ivAskLimpezaCelula  : String;
  ivAskLimpezaCelulas : String;
  ivAskNovoDolar      : String;
  ivAskNovoEuro       : String;
  ivAskNovoNomeRec    : String;
  ivTitErro           : String;
  ivTitPerg           : String;
  ivTitInfo           : String;
  ivTitConf           : String;
  ivTitAvis           : String;
  //
  ivFRCustoTotal      : String;
  ivFRTRodado         : String;
  ivFrTParado         : String;
  ivFRTRExtra         : String;
  ivFRTPExtra         : String;
  ivFRTTotal          : String;
  ivFRETAPA_          : String;
  ivFRDiluEm          : String;
  ivFRLitrosAgua      : String;
  ivFRAGrausCelsius   : String;
  ivFRRodar           : String;
  ivFRParar           : String;
  ivFRInicioFinal     : String;
  ivFRControlepH      : String;
  ivFRControleBe      : String;
  ivFRControleGC      : String;
  ivFRControleCorte   : String;
  ivFRAgua1           : String;
  ivFRAgua2           : String;
  ivFRReciclo         : String;
  ivFRkgDe            : String;
  ivFRLitrosDe        : String;
  ivFRRegistrosDepend : String;
  ivSemNome           : String;
  ivSemNome_txt       : String;
  ivCelulaXY          : String;
  //
  ivExcelTit          : String;
  ivExcelTip          : String;
  ivExcelSalva        : String;
  ivExcelSalvaTit     : String;
  //
  //ivFormatIni         : String;
  //ivFormatFim         : String;
  //
  ivTabela_Nome       : String;
  ivCampo_Nome        : String;
  ivValorCampo_Nome   : String;
  ivIndice_Nome       : String;
  ivExclTabela        : String;
  ivExclCampo         : String;
  ivExclIndice        : String;
  ivMsgExcluido       : String;
  ivMsgExcluida       : String;
  ivMsgExcluidaSeria  : String;
  ivMsgERROExcluir    : String;
  ivMsgERROCriar      : String;
  ivMsgERROIncluir    : String;
  ivMsgERROAlterar    : String;
  ivAbortExclUser     : String;
  ivAbortAlteUser     : String;
  ivAbortInclUser     : String;
  //

implementation

var
  TabInLst: TTabelas;

procedure TUnMyLinguas.AdTbLst(Lista: TList<TTabelas>; CampoDel: Boolean;
  TabCria, TabBase: String; CampoSync: Boolean = False;
  Idx_CDR: Boolean = False; CRCTableManage: TCRCTableManage = crctmStandBy;
  NomeTabItensDel: String = 'ctrlexcltb');
begin
  New(TabInLst);
  TabInLst.Tem_Del  := CampoDel;
  TabInLst.Tem_Sync := CampoSync;
  TabInLst.Idx_CDR  := Idx_CDR;
  TabInLst.QeiLnkOn := CRCTableManage;
  TabInLst.TabCria  := TabCria;
  TabInLst.TabBase  := TabBase;
  TabInLst.TabItDel := NomeTabItensDel;
  //
  Lista.Add(TabInLst);
end;

procedure TUnMyLinguas.GetInternationalStrings(Lingua: TLinguas);
begin
  ivTabPerfis := 'Perfis';
  case Lingua of
    myliPortuguesBR:
    begin
      ivArquivoRKC        := 'Arquivo RKC';
      ivArquivosSkinTan   := 'Arquivos de Receitas Skintan';
      //
      ivTabPrazosIts      := '"Parcelas de Prazos"';
      ivTabContatos       := '"Contatos"';
      //
      ivCriado            := 'criado'; //Feito
      ivAlterado          := 'Alterado'; //Feito
      ivResponsavel       := 'Respons�vel';
      ivCodigo            := 'C�digo';
      ivControle          := 'Controle';
      ivNomeProduto       := 'Nome do Produto';
      ivPrazo             := 'Prazo';
      ivPreco             := 'Pre�o';
      ivEntidade          := 'Entidade';
      ivProduto           := 'Produto';
      ivMoeda             := 'Moeda';
      ivparcela           := 'Parcela';
      ivContato           := 'Contato';
      ivCodigoProduto     := 'C�digo do Produto';
      ivCodigoCliente     := 'C�digo do Cliente';
      ivProduto           := 'Produto';
      ivSequencia         := 'Seq��ncia';
      //
      ivDefA              := 'Defina a %s!';
      ivDefO              := 'Defina o %s!';
      ivInfoA             := 'Informe a %s';
      ivInfoO             := 'Informe o %s';
      ivCrieUma           := 'Crie uma %s';
      //
      ivSenhaMaster       := 'Senha Master';
      ivSenhaMaster       := 'Senha Master';
      ivEntidade          := 'Entidade';
      ivProduto           := 'Produto';
      //
      ivMsgVersao         := ' Vers�o: %s  ';
      ivMsgVersaoBD       := '-  Ver.BD: %s  ';  // Vers�o do Banco de Dados
      ivMsgEstruturaBD    := 'Analisando estrutura do Banco de Dados "%s"...';
      ivMsgFimAnalBD      := 'T�rmino de an�lise da estrutura do Banco de Dados "%s".';
      ivMsgEstruturaBDLoc := 'Analisando estrutura do Banco de Dados Local...';
      ivMsgFimAnalBDLoc   := 'T�rmino de an�lise da estrutura do Banco de Dados Local';
      ivMsgTabCont        := 'Analizando tabela de controle...';
      ivMsgTabContFimAnal := 'T�rmino de An�lize da tabela de controle...';
      ivMsgERRAnalBD      := 'ERRO ao analizar a estrutura do Banco de Dados';
      ivMsgRecriandoTabLoc:= 'Recriando tabela local: %s';
      ivMsgVersaoDifere   := 'Vers�o difere do arquivo';
      ivMsgExclCancel0    := 'Exclus�o cancelada!' + sLineBreak;
      ivMsgExclCancel1    := 'Existem %d registros dependentes na tabela %s!';
      ivMsgDesejaVer1     := sLineBreak+'Deseja visualizar os dados?';
      ivMsgErroIniEdit    := 'Erro ao iniciar inclus�o/edi��o!';
      ivMsgErroNovoReg    := 'Erro ao definir novo registro'+sLineBreak+'Campo n�o instanciado:';
      ivMsgErroProc       := 'Erro no processo.';
      ivMsgErroSalvarArq  := 'Erro ao gravar o arquivo.';
      ivMsgErroNomeRec    := 'Nome de receita inv�lido';
      ivMsgErroEm         := 'Erro em %s.';
      ivMsgObriNome       := '� obrigat�rio o preenchimento do campo "Nome"';
      ivMsgObriCambioU    := 'Informe um valor positivo para o d�lar!';
      ivMsgObriCambioE    := 'Informe um valor positivo para o euro!';
      ivMsgObriTipCadCk   := 'Defina pelo menos um tipo de cadastro';
      ivMsgObriTipCadRG   := 'Defina o tipo de cadastro';
      ivMsgIPINeg         := 'IPI Negativo!';
      ivMsgICMNeg         := 'ICMS Negativo!';
      ivMsgCambiJaExiste  := 'J� existe c�mbio para esta data!';
      ivMsgRegNoLoc       := 'Nenhum registro foi localizado';
      ivMsgTemPrecoData   := 'J� existe pre�o para esta data!';
      ivMsgFileNotFound   := 'Arquivo n�o encontrado: ';
      ivMsgFileInvalid    := 'Arquivo inv�lido';
      ivMsgErroLerArq     := 'Erro ao ler arquivo. [%s]';
      ivMsgArqDif         := 'A vers�o do arquivo a ser aberto � superior a'+
                             'vers�o gerada por este aplicativo. Para n�o perder'+
                             'dados, n�o salve o arquivo com o mesmo nome '+
                             'ao utilizar esta vers�o do aplicativo';
      ivMsgSalvandoArquivo:= 'Salvando arquivo ...';
      ivMsgArqProntoUso   := 'Arquivo Gravado! Pronto para uso.';
      ivMsgArqNaoSalvo    := 'O arquivo n�o foi salvo.';
      ivMsgProntoUso      := 'Pronto para uso.';
      ivMsgAbertura       := 'Abertura em %d ms.';
      ivMsgADefinir       := 'A definir.';
      ivMsgArqNaoLoc      := 'Arquivo n�o encontrado.';
      ivMsgCliNaoLoc      := 'Cliente n�o encontrado.';
      ivMsgCliNaoDef      := 'Cliente n�o definido.';
      ivMsgConfExclDeste  := 'Confirma a exclus�o deste %s?';
      ivMsgConfExclDesta  := 'Confirma a exclus�o desta %s?';

      ivMsgEmConstrucao   := 'Em Constru��o.';
      ivMsgArqNaoLoc      := 'Arquivo n�o localizado: %s.';
      ivMsgImpReceita     := 'Impress�o de Receita';
      ivMsgImpCancelada   := 'Impress�o cancelada. Receita vazia.';
      ivMsgImpCancTit     := 'Cancelamento de Impress�o';
      ivMsgDiferenca      := 'Diferen�a:';
      ivMsgNaoExiste      := 'N�o existe.';
      ivMsgSeraCriada     := 'Ser� criada.';
      ivMsgFoiCriada      := ' foi criada.';
      ivMsgTabCtrlOK      := 'Tabela de controle ativada (registro �nico)';
      ivMsgTabCtrlNoOK    := 'ERRO ao ativar tabela de controle (inserir registro �nico)';
      ivMsgInsLivres      := 'Inserido em "Livres" o valor %d para %s.';
      ivMsgErroTabMaster  := 'ERRO ao verificar RecordCount da tabela Master';
      ivMsgErroTabControle:= 'ERRO ao verificar Tabela Controle';
      ivMsgOpcaoNaoDisp   := 'Op��o n�o dispon�vel.';
      ivMsgValIniIndef    := 'Valor inicial indefinido!';

      //
      ivAbrindo_          := 'Abrindo';
      ivAskConfIndefA     := 'Confirma %s indefinida?';
      ivAskConfIndefO     := 'Confirma %s indefinido?';
      ivAskConfProdInativ := 'Confirma este produto como inativo?';
      ivMsgAskAltera      := 'Deseja salvar as altera��es de %s?';
      ivAskLimpezaCelula  := 'Confirma a limpeza desta c�lula?';
      ivAskLimpezaCelulas := 'Confirma a limpeza das c�lulas selecionadas?';
      ivAskNovoDolar      := 'Defina o valor do d�lar:';
      ivAskNovoEuro       := 'Defina o valor do euro:';
      ivAskNovoNomeRec    := 'Defina o nome da receita:';
      //
      ivTitErro           := 'Erro!';
      ivTitPerg           := 'Confirma��o';
      ivTitConf           := 'Confirma��o';
      ivTitInfo           := 'Informa��o';
      ivTitAvis           := 'Aviso!';
      //
      ivFRCustoTotal      := 'Custo Total';
      ivFRTRodado         := 'T. Rodado';
      ivFrTParado         := 'T. Parado';
      ivFRTRExtra         := 'R. Extra';
      ivFRTPExtra         := 'P. Extra';
      ivFRTTotal          := 'T. Total';
      ivFRETAPA_          := 'ETAPA ';
      ivFRDiluEm          := ' - Diluir em ';
      ivFRLitrosAgua      := ' litros de �gua ';
      ivFRAGrausCelsius   := ' a %s �C ';
      ivFRRodar           := 'Rodar ';
      ivFRParar           := 'Parar ';
      ivFRInicioFinal     := '  in�cio:______  final:______';
      ivFRControlepH      := '  pH:_____(%s) ';
      ivFRControleBe      := '  B�:_____(%s) ';
      ivFRControleGC      := '  �C:_____(%s) ';
      ivFRControleCorte   := '  '+CO_CORTE+'/#:_____(%s) ';
      ivFRAgua1           := '�GUA';
      ivFRAgua2           := 'AGUA';
      ivFRReciclo         := 'RECICLO';
      ivFRkgDe            := ' kg de ';
      ivFRLitrosDe        := ' Litros de ';
      ivFRRegistrosDepend := 'Registros Dependentes';
      //
      ivSemNome           := 'Sem Nome';
      ivSemNome_txt       := 'Sem Nome.txt';
      ivCelulaXY          := 'C�lula %d , %d';
      //
      ivExcelTit          := 'Salvar arquivo Excel - SkinTan';
      ivExcelTip          := 'Arquivo EXCEL (*.XLS)|*.xls';
      ivExcelSalva        := 'Deseja visualizar o arquivo %s?';
      ivExcelSalvaTit     := 'Salvar Receita em Formato Excel';
      //
      //ivFormatIni         := '"In�cio: " hh:mm:ss:zzz';
      //ivFormatFim         := '"Final : " hh:mm:ss:zzz';
      //
      ivTabela_Nome       := 'Tabela[%s] ';
      ivCampo_Nome        := 'Tabela[%s].Campo[%s] ';
      ivValorCampo_Nome   := 'Tabela[%s].RegistroObrigatorio[%s] ';
      ivIndice_Nome       := 'Tabela[%s].�ndice[%s] ';
      ivExclTabela        := 'N�o existe refer�cia no aplicativo %s '+
                             '� tabela %s.'+sLineBreak+
                             'Deseja excluir a tabela?';
      ivExclCampo         := 'N�o existe refer�cia no aplicativo %s '+
                             'ao campo %s da tabela %s.'+sLineBreak+
                             'Deseja excluir o campo?';
      ivExclIndice        := 'N�o existe refer�cia no aplicativo %s '+
                             'ao �ndice %s da tabela %s.'+sLineBreak+
                             'Deseja excluir o �ndice?';
      ivMsgExcluido       := 'exclu�do';
      ivMsgExcluida       := 'exclu�da';
      ivMsgExcluidaSeria  := 'deveria ser exclu�da';
      ivMsgERROExcluir    := 'ERRO ao excluir.';
      ivMsgERROCriar      := 'ERRO ao criar.';
      ivMsgERROIncluir    := 'ERRO ao incluir.';
      ivMsgERROAlterar    := 'ERRO ao alterar.';
      ivAbortExclUser     := 'Exclus�o abortada pelo usu�rio.';
      ivAbortAlteUser     := 'Altera��o abortada pelo usu�rio.';
      ivAbortInclUser     := 'Inclus�o abortada pelo usu�rio.';
      //
    end;
  end;
end;

end.

