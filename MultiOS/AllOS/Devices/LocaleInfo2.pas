unit LocaleInfo2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Sensors, System.Sensors, FMX.StdCtrls,
  FMX.Edit, FMX.WebBrowser, FMX.ListBox, FMX.Layouts;

type
  TLocationForm = class(TForm)
    LocationSensor1: TLocationSensor;
    WebBrowser1: TWebBrowser;
    ListBox1: TListBox;
    lbLatitude: TListBoxItem;
    lbLongitude: TListBoxItem;
    ToolBar1: TToolBar;
    LaLocation: TLabel;
    lbErrorRadius: TListBoxItem;
    lbAltitude: TListBoxItem;
    lbSpeed: TListBoxItem;
    lbTrueHeading: TListBoxItem;
    lbMagneticHeading: TListBoxItem;
    lbAddress1: TListBoxItem;
    lbAddress2: TListBoxItem;
    lbCity: TListBoxItem;
    swLocationSensorActive: TSwitch;
    lbStateProvince: TListBoxItem;
    lbPostalCode: TListBoxItem;
    lbCountryRegion: TListBoxItem;
    procedure LocationSensor1LocationChanged(Sender: TObject; const OldLocation,
      NewLocation: TLocationCoord2D);
    procedure sbAccuracyChange(Sender: TObject);
    procedure sbTriggerDistanceChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure swLocationSensorActiveSwitch(Sender: TObject);
    procedure nbTriggerDistanceChange(Sender: TObject);
    procedure nbAccuracyChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTimerCount: Integer;
    FLocation, FData, FState: Integer;
    //
    function ToFormStr(AProp : string; AVal : Single): string;
    function ToFormStrS(AProp : string; AVal : string): string;
    function ToFormStrB(AProp : string; AVal : Boolean): string;
  public
    { Public declarations }
  end;

var
  LocationForm: TLocationForm;

implementation

const
{
  cBorder = 10;
  cND = 'Not defined';

const
  AllCat : TSensorCategories =
  [TSensorCategory.Location, TSensorCategory.Environmental, TSensorCategory.Motion,
  TSensorCategory.Orientation, TSensorCategory.Mechanical, TSensorCategory.Electrical,
  TSensorCategory.Biometric, TSensorCategory.Light, TSensorCategory.Scanner];
}
  cForm = '  %s =' + sLineBreak + '%30s        %3.5f ' + sLineBreak;
  cFormS = '  %s =' + sLineBreak + '%30s        %s ' + sLineBreak;


{$R *.fmx}

procedure TLocationForm.Button1Click(Sender: TObject);
begin
  //nbTriggerDistance.Value := nbTriggerDistance.Value - 1;
end;

procedure TLocationForm.Button2Click(Sender: TObject);
begin
  //nbTriggerDistance.Value := nbTriggerDistance.Value + 1;
end;

procedure TLocationForm.Button3Click(Sender: TObject);
begin
  //nbAccuracy.Value := nbAccuracy.Value - 1;
end;

procedure TLocationForm.Button4Click(Sender: TObject);
begin
  //nbAccuracy.Value := nbAccuracy.Value + 1;
end;

procedure TLocationForm.FormCreate(Sender: TObject);
begin
  FTimerCount := 0;
  FLocation := 0;
  FData := 0;
  FState := 0;
end;

procedure TLocationForm.LocationSensor1LocationChanged(Sender: TObject;
  const OldLocation, NewLocation: TLocationCoord2D);
const
  LGoogleMapsURL: String = 'https://maps.google.com/maps?q=%s,%s&output=embed';
var
  ls : TCustomLocationSensor;
  LProp : TCustomLocationSensor.TProperty;
begin
  { convert the location to latitude and longitude }
  lbLatitude.Text := 'Latitude: ' + NewLocation.Latitude.ToString;
  lbLongitude.Text := 'Longitude: ' + NewLocation.Longitude.ToString;
  //
  try
    ls := TCustomLocationSensor(TCustomSensor(LocationSensor1));
    for LProp in ls.AvailableProperties do
    begin
      case LProp of
        TCustomLocationSensor.TProperty.Latitude:
          lbLatitude.Text := ToFormStr('Latitude', ls.Latitude);
        TCustomLocationSensor.TProperty.Longitude:
          lbLongitude.Text := ToFormStr('Longitude', ls.Longitude);
        TCustomLocationSensor.TProperty.ErrorRadius:
          lbErrorRadius.Text :=  ToFormStr('ErrorRadius', ls.ErrorRadius);
        TCustomLocationSensor.TProperty.Altitude:
          lbAltitude.Text := ToFormStr('Altitude', ls.Altitude);
        TCustomLocationSensor.TProperty.Speed:
          lbSpeed.Text := ToFormStr('Velocidade', ls.Speed);
        TCustomLocationSensor.TProperty.TrueHeading:
          lbTrueHeading.Text := ToFormStr('TrueHeading', ls.TrueHeading);
        TCustomLocationSensor.TProperty.MagneticHeading:
          lbMagneticHeading.Text :=  ToFormStr('MagneticHeading', ls.MagneticHeading);
        TCustomLocationSensor.TProperty.Address1:
          lbAddress1.Text :=  ToFormStrS('Address1', ls.Address1);
        TCustomLocationSensor.TProperty.Address2:
          lbAddress2.Text :=  ToFormStrS('Address2', ls.Address2);
        TCustomLocationSensor.TProperty.City:
          lbCity.Text := ToFormStrS('City', ls.City);
        TCustomLocationSensor.TProperty.StateProvince:
          lbStateProvince.Text := ToFormStrS('StateProvince', ls.StateProvince);
        TCustomLocationSensor.TProperty.PostalCode:
          lbPostalCode.Text := ToFormStrS('PostalCode', ls.PostalCode);
        TCustomLocationSensor.TProperty.CountryRegion:
          lbCountryRegion.Text := ToFormStrS('CountryRegion', ls.CountryRegion);
      end;
    end;
  except
    lbErrorRadius.Text :=  'Erro TCustomSensor';
  end;
////////////////////////////////////////////////////////////////////////////////
  { and track the location via Google Maps }
  //WebBrowser1.Navigate(Format(LGoogleMapsURL, [NewLocation.Latitude.ToString, NewLocation.Longitude.ToString]));
  //
  FLocation := FLocation + 1;
  LaLocation.Text := 'Location shot ' + IntToStr(FLocation);
end;

procedure TLocationForm.nbAccuracyChange(Sender: TObject);
begin
  //LocationSensor1.Distance := nbAccuracy.Value;
end;

procedure TLocationForm.nbTriggerDistanceChange(Sender: TObject);
begin
  //LocationSensor1.Accuracy := nbTriggerDistance.Value;
end;

procedure TLocationForm.sbAccuracyChange(Sender: TObject);
begin
  { set the precision }
  //LocationSensor1.Accuracy := nbAccuracy.Value;
end;

procedure TLocationForm.sbTriggerDistanceChange(Sender: TObject);
begin
  { set the triggering distance }
  //LocationSensor1.Distance := nbTriggerDistance.Value;
end;

procedure TLocationForm.swLocationSensorActiveSwitch(Sender: TObject);
begin
  { activate or deactivate the location sensor }
  LocationSensor1.Active := swLocationSensorActive.IsChecked;
end;

function TLocationForm.ToFormStr(AProp: string; AVal: Single): string;
begin
  Result := Format(cForm,[AProp,'', AVal]);
end;

function TLocationForm.ToFormStrB(AProp: string; AVal: Boolean): string;
begin
  if AVal then
    ToFormStrS(AProp,'True')
  else
    ToFormStrS(AProp, 'False');
end;

function TLocationForm.ToFormStrS(AProp, AVal: string): string;
begin
  Result := Format(cFormS,[AProp,'', AVal]);
end;

end.
