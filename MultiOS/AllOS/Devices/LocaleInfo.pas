unit LocaleInfo;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.ListBox,
  FMX.Layouts,
  System.Sensors, FMX.StdCtrls,
  UnGeral, FMX.Sensors;

type
  TFmLocaleInfo = class(TForm)
    ListBox1: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxItemLatitude: TListBoxItem;
    ListBoxItemLongitude: TListBoxItem;
    Panel1: TPanel;
    lInfo: TLabel;
    Timer1: TTimer;
    lbMain: TListBox;
    LocationSensor1: TLocationSensor;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbMainItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure LocationSensor1LocationChanged(Sender: TObject; const OldLocation,
      NewLocation: TLocationCoord2D);
  private
    { Private declarations }
    FActiveSensor : TCustomSensor;
    FShowInfo: Boolean;
    //
    procedure CreateIfExists(ASensorCategory : TSensorCategory);
    function GetFullInfo(ACategory, AType, AClass, AAvailibleProperties : string): string;
    function GetInfoAboutLocation(ASensor: TCustomSensor): string;
    function GetTypeNameLocation(AType: TLocationSensorType): string;
    function GetSensorCategoryName(ASensorCategory : TSensorCategory) : string;
    function GetSensorType(ASensor: TCustomSensor): string;
    procedure ListBoxItemClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  FmLocaleInfo: TFmLocaleInfo;

implementation

uses Principal;

{$R *.fmx}

const
  cND = 'Not defined';
  cForm = '  %s =' + sLineBreak + '%30s        %3.5f ' + sLineBreak;
  cFormS = '  %s =' + sLineBreak + '%30s        %s ' + sLineBreak;

{ TFmLocaleInfo }

procedure TFmLocaleInfo.CreateIfExists(ASensorCategory: TSensorCategory);
var
  LSensorArray : TSensorArray;
  LSensor : TCustomSensor;
  LHeader : TListBoxGroupHeader;
  LItem : TListBoxItem;
  i : integer;
begin
  LSensorArray := TSensorManager.Current.GetSensorsByCategory(ASensorCategory);
  LHeader := TListBoxGroupHeader.Create(Owner);
  LHeader.Parent := lbMain;
  LHeader.Text := GetSensorCategoryName(ASensorCategory);
  LHeader.Height := LHeader.Height * 2;
  for LSensor in LSensorArray do
//  LSensor := nil; for i := 0 to 3 do
  begin
    LItem := TListBoxItem.Create(Owner);
    LItem.Parent := lbMain;
    LItem.Text := GetSensorType(LSensor);
    LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aDetail;
    LItem.Data := LSensor;
    LItem.OnClick := ListBoxItemClick;
    LItem.Height := LItem.Height * 2;
    LItem.Font.Size := LItem.Font.Size * 2;
    //
    FActiveSensor := LSensor;
    Geral.MB_Info(LSensor.Name);
  end;
  lInfo.Text := GetInfoAboutLocation(FActiveSensor);
  Timer1.Enabled := true;
end;

procedure TFmLocaleInfo.FormCreate(Sender: TObject);
{
var
  LSensorCat : TSensorCategory ;
}
begin
  FActiveSensor := nil;
  FShowInfo := False;
{
  ReAllignComponents;
}
  TSensorManager.Current.Activate();
{
  for LSensorCat in AllCat do
    CreateIfExists(LSensorCat);
}
  CreateIfExists(TSensorCategory.Location);
end;

function TFmLocaleInfo.GetFullInfo(ACategory, AType, AClass,
  AAvailibleProperties: string): string;
begin
  Result := sLineBreak + 'Category:' + sLineBreak
    + '  ' + ACategory + sLineBreak + sLineBreak
    + 'Sensor type:' + sLineBreak
    + '  ' + AType + sLineBreak + sLineBreak
    + 'Base class:' + sLineBreak
    + '  ' + AClass + sLineBreak + sLineBreak
    + 'Available properties:' + sLineBreak
    + AAvailibleProperties;
end;

function TFmLocaleInfo.GetInfoAboutLocation(ASensor: TCustomSensor): string;
var
  ls : TCustomLocationSensor;
  LValues : string;
  LProp : TCustomLocationSensor.TProperty;
begin
  LValues := '';
  ls := TCustomLocationSensor(ASensor);
  if not ls.Started then
    ls.Start;
  for LProp in ls.AvailableProperties do
  begin
    case LProp of
      TCustomLocationSensor.TProperty.Latitude:
      begin
        FmPrincipal.FLatitude := ls.Latitude;
        LValues := LValues + Geral.ToFormStr('Latitude', ls.Latitude);
      end;
      TCustomLocationSensor.TProperty.Longitude:
      begin
        FmPrincipal.FLongitude := ls.Longitude;
        LValues := LValues + Geral.ToFormStr('Longitude', ls.Longitude);
      end;
      TCustomLocationSensor.TProperty.ErrorRadius:
        LValues := LValues + Geral.ToFormStr('ErrorRadius', ls.ErrorRadius);
      TCustomLocationSensor.TProperty.Altitude:
        LValues := LValues + Geral.ToFormStr('Altitude', ls.Altitude);
      TCustomLocationSensor.TProperty.Speed:
        LValues := LValues + Geral.ToFormStr('Speed', ls.Speed);
      TCustomLocationSensor.TProperty.TrueHeading:
        LValues := LValues + Geral.ToFormStr('TrueHeading', ls.TrueHeading);
      TCustomLocationSensor.TProperty.MagneticHeading:
        LValues := LValues + Geral.ToFormStr('MagneticHeading', ls.MagneticHeading);
      TCustomLocationSensor.TProperty.Address1:
        LValues := LValues + Geral.ToFormStrS('Address1', ls.Address1);
      TCustomLocationSensor.TProperty.Address2:
        LValues := LValues + Geral.ToFormStrS('Address2', ls.Address2);
      TCustomLocationSensor.TProperty.City:
        LValues := LValues + Geral.ToFormStrS('City', ls.City);
      TCustomLocationSensor.TProperty.StateProvince:
        LValues := LValues + Geral.ToFormStrS('StateProvince', ls.StateProvince);
      TCustomLocationSensor.TProperty.PostalCode:
        LValues := LValues + Geral.ToFormStrS('PostalCode', ls.PostalCode);
      TCustomLocationSensor.TProperty.CountryRegion:
        LValues := LValues + Geral.ToFormStrS('CountryRegion', ls.CountryRegion);
    end;
  end;
  Result := GetFullInfo(
    GetSensorCategoryName(ASensor.Category),
    GetTypeNameLocation(ls.SensorType),
    ls.ClassName,
    LValues
  ) ;
  //
  FmPrincipal.FObteveLocalizacao := True;
end;

function TFmLocaleInfo.GetSensorCategoryName(
  ASensorCategory: TSensorCategory): string;
begin
  Result := cND ;
  case ASensorCategory of
    TSensorCategory.Location: Result := 'Location' ;
    TSensorCategory.Environmental: Result := 'Environmental' ;
    TSensorCategory.Motion: Result := 'Motion' ;
    TSensorCategory.Orientation: Result := 'Orientation' ;
    TSensorCategory.Mechanical: Result := 'Mechanical' ;
    TSensorCategory.Electrical: Result := 'Electrical' ;
    TSensorCategory.Biometric: Result := 'Biometric' ;
    TSensorCategory.Light: Result := 'Light' ;
    TSensorCategory.Scanner: Result := 'Scanner' ;
  end;
end;

function TFmLocaleInfo.GetSensorType(ASensor: TCustomSensor): string;
begin
  Result := cND;
  case ASensor.Category of
    TSensorCategory.Location: Result := GetTypeNameLocation(TCustomLocationSensor(ASensor).SensorType);
{
    TSensorCategory.Environmental:  Result := GetTypeNameEnv(TCustomEnvironmentalSensor(ASensor).SensorType);
    TSensorCategory.Motion: Result := GetTypeNameMotion(TCustomMotionSensor(ASensor).SensorType) ;
    TSensorCategory.Orientation: Result := GetTypeNameOrientation(TCustomOrientationSensor(ASensor).SensorType);
    TSensorCategory.Mechanical: Result := GetTypeNameMech(TCustomMechanicalSensor(ASensor).SensorType);
    TSensorCategory.Electrical: Result := GetTypeNameElectro(TCustomElectricalSensor(ASensor).SensorType);
    TSensorCategory.Biometric: Result := GetTypeNameBio(TCustomBiometricSensor(ASensor).SensorType);
    TSensorCategory.Light: Result := GetTypeNameLight(TCustomLightSensor(ASensor).SensorType);
    TSensorCategory.Scanner:  Result := GetTypeNameScanner(TCustomScannerSensor(ASensor).SensorType);
}
  end;
end;

function TFmLocaleInfo.GetTypeNameLocation(AType: TLocationSensorType): string;
begin
  case AType of
    TLocationSensorType.GPS: Result := 'GPS';
    TLocationSensorType.Static: Result := 'Static';
    TLocationSensorType.Lookup: Result := 'Lookup';
    TLocationSensorType.Triangulation: Result := 'Triangulation';
    TLocationSensorType.Broadcast: Result := 'Broadcast';
    TLocationSensorType.DeadReckoning: Result := 'DeadReckoning';
    TLocationSensorType.Other: Result := 'Other';
  else
    Result := cND
  end;
end;

procedure TFmLocaleInfo.lbMainItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Assigned(Item.OnClick) then
    Item.OnClick(Item);
end;

procedure TFmLocaleInfo.ListBoxItemClick(Sender: TObject);
begin
  if Sender is TListBoxItem then
    FActiveSensor := TCustomSensor(TListBoxItem(Sender).Data);
  FShowInfo := True;
end;

procedure TFmLocaleInfo.LocationSensor1LocationChanged(Sender: TObject;
  const OldLocation, NewLocation: TLocationCoord2D);
var
  URLString: String;
  LLatitude, LLongitude : string;
  LSettings: TFormatSettings;
  LDecSeparator : Char;
begin
  LDecSeparator := FormatSettings.DecimalSeparator;
  LSettings := FormatSettings;
  try
    FormatSettings.DecimalSeparator := '.';
    // Show current location
    ListBoxItemLatitude.ItemData.Detail  := Format('%2.6f', [NewLocation.Latitude]);
    ListBoxItemLongitude.ItemData.Detail := Format('%2.6f', [NewLocation.Longitude]);

    // Show Map using Google Maps
    LLongitude := FloatToStr(NewLocation.Longitude, LSettings);
    URLString := Format(
      'https://maps.google.com/maps?q=%2.6f,%2.6f&output=embed',
        [ NewLocation.Latitude, NewLocation.Longitude]);
  finally
    FormatSettings.DecimalSeparator := LDecSeparator;
  end;
(*
  WebBrowser1.Navigate(URLString);

  // Setup an instance of TGeocoder
  if not Assigned(FGeocoder) then
  begin
    if Assigned(TGeocoder.Current) then
      FGeocoder := TGeocoder.Current.Create;
    if Assigned(FGeocoder) then
      FGeocoder.OnGeocodeReverse := OnGeocodeReverseEvent;
  end;

  // Translate location to address
  if Assigned(FGeocoder) and not FGeocoder.Geocoding then
    FGeocoder.GeocodeReverse(NewLocation);
*)
end;

procedure TFmLocaleInfo.Timer1Timer(Sender: TObject);
begin
  lInfo.Text := 'Timer';
  sleep(1000);
  lInfo.Text := GetInfoAboutLocation(FActiveSensor);
end;

end.
