﻿unit LocaleInfo3;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  System.Actions, FMX.ActnList, FMX.StdCtrls, FMX.ListBox, FMX.Layouts,
  System.Sensors, System.Math,
  UnGrl_Vars, UnGeral, FMX.Memo;

type
  TFmLocaleInfo3 = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TopToolBar: TToolBar;
    ToolBarLabel: TLabel;
    TopToolBar1: TToolBar;
    ToolBarLabel1: TLabel;
    btnBack: TButton;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    BottomToolBar: TToolBar;
    btnNext: TSpeedButton;
    ListBox1: TListBox;
    lbLatitude: TListBoxItem;
    lbLongitude: TListBoxItem;
    lbErrorRadius: TListBoxItem;
    lbAltitude: TListBoxItem;
    lbSpeed: TListBoxItem;
    lbTrueHeading: TListBoxItem;
    lbMagneticHeading: TListBoxItem;
    lbAddress1: TListBoxItem;
    lbAddress2: TListBoxItem;
    lbCity: TListBoxItem;
    lbStateProvince: TListBoxItem;
    lbPostalCode: TListBoxItem;
    lbCountryRegion: TListBoxItem;
    lbMain: TListBox;
    Timer1: TTimer;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ChangeTabAction3: TChangeTabAction;
    Button1: TButton;
    TabItem3: TTabItem;
    MeDados: TMemo;
    ToolBar1: TToolBar;
    Label2: TLabel;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    //FCriado,
    FRodando: Boolean;
    FTimerCount: Int64;
    FShowInfo: Boolean ;
    FActiveSensor: TCustomSensor;
    //
    procedure CreateIfExists(ASensorCategory: TSensorCategory);
    procedure ListBoxItemClick(Sender: TObject);
  public
    { Public declarations }
    procedure InicializaLocalizacao();

{   Não funciona!
    function  ObtemDadosLocalizacao(): Boolean;
    procedure DesabilitaLocalizacao();
}
    // Geral!!! Mover para outra unit
    function  GetSensorCategoryName(ASensorCategory : TSensorCategory) : string;
    function  GetSensorType(ASensor: TCustomSensor): string;
    function  GetInfoAboutLocation(ASensor : TCustomSensor): string;
    function  GetTypeNameLocation(AType : TLocationSensorType): string ;
  end;

var
  FmLocaleInfo3: TFmLocaleInfo3;

implementation

uses Principal;

{$R *.fmx}


const
  //cBorder = 10;
  cND = 'Not defined';

const
{
  AllCat : TSensorCategories =
  [TSensorCategory.Location, TSensorCategory.Environmental, TSensorCategory.Motion,
  TSensorCategory.Orientation, TSensorCategory.Mechanical, TSensorCategory.Electrical,
  TSensorCategory.Biometric, TSensorCategory.Light, TSensorCategory.Scanner];
}
  cForm = '  %s =' + sLineBreak + '%30s        %3.5f ' + sLineBreak;
  cFormS = '  %s =' + sLineBreak + '%30s        %s ' + sLineBreak;

procedure TFmLocaleInfo3.CreateIfExists(
  ASensorCategory: TSensorCategory);
var
  LSensorArray : TSensorArray;
  LSensor : TCustomSensor;
  LHeader : TListBoxGroupHeader;
  LItem : TListBoxItem;
  i : integer;
begin
  LSensorArray := TSensorManager.Current.GetSensorsByCategory(ASensorCategory);
  LHeader := TListBoxGroupHeader.Create(Owner);
  LHeader.Parent := lbMain;
  LHeader.Text := GetSensorCategoryName(ASensorCategory);
  LHeader.Height := LHeader.Height * 2;
  for LSensor in LSensorArray do
//  LSensor := nil; for i := 0 to 3 do
  begin
    LItem := TListBoxItem.Create(Owner);
    LItem.Parent := lbMain;
    LItem.Text := GetSensorType(LSensor);
    LItem.ItemData.Accessory := TListBoxItemData.TAccessory.aDetail;
    LItem.Data := LSensor;
    LItem.OnClick := ListBoxItemClick;
    LItem.Height := LItem.Height * 2;
    LItem.Font.Size := LItem.Font.Size * 2;
    if LSensor <> nil then
    begin
      FActiveSensor := LSensor;
      {
      Geral.MB_Info('ClassName: ' + LSensor.ClassName + sLineBreak +
      'Description: ' + LSensor.Description + sLineBreak +
      'Manufacturer: ' + LSensor.Manufacturer + sLineBreak +
      'Model: ' + LSensor.Model + sLineBreak +
      'Name: ' + LSensor.Name + sLineBreak +
      'QualifiedClassName: ' + LSensor.QualifiedClassName + sLineBreak +
      'SerialNo: ' + LSensor.SerialNo + sLineBreak +
      'ToString: ' + LSensor.ToString + sLineBreak +
      'UniqueID: ' + LSensor.UniqueID + sLineBreak +
      'UnitName: ' + LSensor.UnitName + sLineBreak +
      'UnitScope: ' + LSensor.UnitScope + sLineBreak);
      }
      // não localiza!!!  LSensor.DisposeOf;
    end;
  end;
  //TSensorArray(LSensorArray).DisposeOf;
  LSensorArray := nil;
end;

{
procedure TFmLocaleInfo3.DesabilitaLocalizacao();
begin
  Timer1.Enabled := False;
  TSensorManager.Current.Deactivate();
  TSensorManager.Current.Free;
  TSensorManager.Current.DisposeOf;
  // Aqui limpa da memória / processador!
  // mas encerra a aplicação!!!
  if Assigned(FActiveSensor) then
  begin
    //FActiveSensor.DisposeOf;
    //FActiveSensor := nil;
  // Não adianta. Não tem jeito!
    FActiveSensor.Destroy;
  end;
  //
  VAR_LOC_LATI_LONGI := False;
  VAR_LOC_DATA_HORA := 0;
  VAR_LATITUDE := 0;
  VAR_LONGITUDE := 0;
end;
}

procedure TFmLocaleInfo3.FormCreate(Sender: TObject);
begin
  { This defines the default active tab at runtime }
  TabControl1.ActiveTab := TabItem1;
{$IFDEF ANDROID}
  { This hides the toolbar back button on Android }
  btnBack.Visible := False;
{$ENDIF}
  //
  //FCriado  := False;
  FRodando := False;
  FTimerCount := 0;
  FShowInfo := False;
  FActiveSensor := nil;
  //
  InicializaLocalizacao();
end;

procedure TFmLocaleInfo3.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack then
  begin
    if TabControl1.ActiveTab = TabItem2 then
    begin
      ChangeTabAction1.Tab := TabItem1;
      ChangeTabAction1.ExecuteTarget(Self);
      ChangeTabAction1.Tab := TabItem2;
      Key := 0;
    end;
  end;
end;

function TFmLocaleInfo3.GetInfoAboutLocation(
  ASensor: TCustomSensor): string;
var
  ls : TCustomLocationSensor;
  LValues : string;
  LProp : TCustomLocationSensor.TProperty;
begin
  LValues := '';
  ls := TCustomLocationSensor(ASensor);
  if not ls.Started then
    ls.Start;
{
  for LProp in ls.AvailableProperties do
  begin
    case LProp of
      TCustomLocationSensor.TProperty.Latitude:
        LValues := LValues + ToFormStr('Latitude', ls.Latitude);
      TCustomLocationSensor.TProperty.Longitude:
        LValues := LValues + ToFormStr('Longitude', ls.Longitude);
      TCustomLocationSensor.TProperty.ErrorRadius:
        LValues := LValues + ToFormStr('ErrorRadius', ls.ErrorRadius);
      TCustomLocationSensor.TProperty.Altitude:
        LValues := LValues + ToFormStr('Altitude', ls.Altitude);
      TCustomLocationSensor.TProperty.Speed:
        LValues := LValues + ToFormStr('Speed', ls.Speed);
      TCustomLocationSensor.TProperty.TrueHeading:
        LValues := LValues + ToFormStr('TrueHeading', ls.TrueHeading);
      TCustomLocationSensor.TProperty.MagneticHeading:
        LValues := LValues + ToFormStr('MagneticHeading', ls.MagneticHeading);
      TCustomLocationSensor.TProperty.Address1:
        LValues := LValues + ToFormStrS('Address1', ls.Address1);
      TCustomLocationSensor.TProperty.Address2:
        LValues := LValues + ToFormStrS('Address2', ls.Address2);
      TCustomLocationSensor.TProperty.City:
        LValues := LValues + ToFormStrS('City', ls.City);
      TCustomLocationSensor.TProperty.StateProvince:
        LValues := LValues + ToFormStrS('StateProvince', ls.StateProvince);
      TCustomLocationSensor.TProperty.PostalCode:
        LValues := LValues + ToFormStrS('PostalCode', ls.PostalCode);
      TCustomLocationSensor.TProperty.CountryRegion:
        LValues := LValues + ToFormStrS('CountryRegion', ls.CountryRegion);
    end;
  end;
  Result := GetFullInfo(
    GetSensorCategoryName(ASensor.Category),
    GetTypeNameLocation(ls.SensorType),
    ls.ClassName,
    LValues
  ) ;
}
  // Marco
  Label1.Text := 'Leituras: ' + IntToStr(FTimerCount);
(*
procedure TLocationForm.LocationSensor1LocationChanged(Sender: TObject;
  const OldLocation, NewLocation: TLocationCoord2D);
const
  LGoogleMapsURL: String = 'https://maps.google.com/maps?q=%s,%s&output=embed';
var
  ls : TCustomLocationSensor;
  LProp : TCustomLocationSensor.TProperty;
begin
  { convert the location to latitude and longitude }
  lbLatitude.Text := 'Latitude: ' + NewLocation.Latitude.ToString;
  lbLongitude.Text := 'Longitude: ' + NewLocation.Longitude.ToString;
  //
  try
    ls := TCustomLocationSensor(TCustomSensor(LocationSensor1));
*)
    for LProp in ls.AvailableProperties do
    begin
      case LProp of
        TCustomLocationSensor.TProperty.Latitude:
        begin
          VAR_LATITUDE := ls.Latitude;
          lbLatitude.Text := Geral.ToFormStr('Latitude', VAR_LATITUDE);
        end;
        TCustomLocationSensor.TProperty.Longitude:
        begin
          VAR_LONGITUDE := ls.Longitude;
          lbLongitude.Text := Geral.ToFormStr('Longitude', VAR_LONGITUDE);
        end;
        TCustomLocationSensor.TProperty.ErrorRadius:
          lbErrorRadius.Text := Geral.ToFormStr('Margem de erro', ls.ErrorRadius);
        TCustomLocationSensor.TProperty.Altitude:
          lbAltitude.Text := Geral.ToFormStr('Altitude', ls.Altitude);
        TCustomLocationSensor.TProperty.Speed:
          lbSpeed.Text := Geral.ToFormStr('Velocidade', ls.Speed);
        TCustomLocationSensor.TProperty.TrueHeading:
          lbTrueHeading.Text := Geral.ToFormStr('Rumo real', ls.TrueHeading);
        TCustomLocationSensor.TProperty.MagneticHeading:
          lbMagneticHeading.Text := Geral.ToFormStr('Rumo magnético', ls.MagneticHeading);
        TCustomLocationSensor.TProperty.Address1:
          lbAddress1.Text := Geral.ToFormStrS('Endereço 1', ls.Address1);
        TCustomLocationSensor.TProperty.Address2:
          lbAddress2.Text := Geral.ToFormStrS('Endereço 2', ls.Address2);
        TCustomLocationSensor.TProperty.City:
          lbCity.Text := Geral.ToFormStrS('Cidade', ls.City);
        TCustomLocationSensor.TProperty.StateProvince:
          lbStateProvince.Text := Geral.ToFormStrS('Estado/Província', ls.StateProvince);
        TCustomLocationSensor.TProperty.PostalCode:
          lbPostalCode.Text := Geral.ToFormStrS('Código postal', ls.PostalCode);
        TCustomLocationSensor.TProperty.CountryRegion:
          lbCountryRegion.Text := Geral.ToFormStrS('Região do País', ls.CountryRegion);
      end;
    end;
    VAR_LOC_DATA_HORA := Now();
    // problema no mswindows
    if IsNAN(VAR_LATITUDE) then
      VAR_LOC_LATI_LONGI := False
    else
    if IsNAN(VAR_LONGITUDE) then
      VAR_LOC_LATI_LONGI := False
    else
    // FIM problema no mswindows
      VAR_LOC_LATI_LONGI :=
      ((VAR_LONGITUDE > 0.000001) or (VAR_LONGITUDE < -0.000001))
      and
      ((VAR_LATITUDE > 0.000001) or (VAR_LATITUDE < -0.000001));
  { and track the location via Google Maps }
  //WebBrowser1.Navigate(Format(LGoogleMapsURL, [NewLocation.Latitude.ToString, NewLocation.Longitude.ToString]));
  //
  lbMain.Visible := False;
  ListBox1.visible := True;
end;

function TFmLocaleInfo3.GetSensorCategoryName(
  ASensorCategory: TSensorCategory): string;
begin
  Result := cND ;
  case ASensorCategory of
    TSensorCategory.Location: Result := 'Localização' ;
    TSensorCategory.Environmental: Result := 'Ambiente' ;
    TSensorCategory.Motion: Result := 'Movimento' ;
    TSensorCategory.Orientation: Result := 'Orientação' ;
    TSensorCategory.Mechanical: Result := 'Mecânica' ;
    TSensorCategory.Electrical: Result := 'Elétrica' ;
    TSensorCategory.Biometric: Result := 'Biométrica' ;
    TSensorCategory.Light: Result := 'Luz' ;
    TSensorCategory.Scanner: Result := 'Scaner' ;
  end;
end;

function TFmLocaleInfo3.GetSensorType(
  ASensor: TCustomSensor): string;
begin
  Result := cND;
  case ASensor.Category of
    TSensorCategory.Location: Result := GetTypeNameLocation(TCustomLocationSensor(ASensor).SensorType);
{
    TSensorCategory.Environmental:  Result := GetTypeNameEnv(TCustomEnvironmentalSensor(ASensor).SensorType);
    TSensorCategory.Motion: Result := GetTypeNameMotion(TCustomMotionSensor(ASensor).SensorType) ;
    TSensorCategory.Orientation: Result := GetTypeNameOrientation(TCustomOrientationSensor(ASensor).SensorType);
    TSensorCategory.Mechanical: Result := GetTypeNameMech(TCustomMechanicalSensor(ASensor).SensorType);
    TSensorCategory.Electrical: Result := GetTypeNameElectro(TCustomElectricalSensor(ASensor).SensorType);
    TSensorCategory.Biometric: Result := GetTypeNameBio(TCustomBiometricSensor(ASensor).SensorType);
    TSensorCategory.Light: Result := GetTypeNameLight(TCustomLightSensor(ASensor).SensorType);
    TSensorCategory.Scanner:  Result := GetTypeNameScanner(TCustomScannerSensor(ASensor).SensorType);
}
  end;
end;

function TFmLocaleInfo3.GetTypeNameLocation(
  AType: TLocationSensorType): string;
begin
  case AType of
    TLocationSensorType.GPS: Result := 'GPS';
    TLocationSensorType.Static: Result := 'Static';
    TLocationSensorType.Lookup: Result := 'Lookup';
    TLocationSensorType.Triangulation: Result := 'Triangulation';
    TLocationSensorType.Broadcast: Result := 'Broadcast';
    TLocationSensorType.DeadReckoning: Result := 'DeadReckoning';
    TLocationSensorType.Other: Result := 'Other';
  else
    Result := cND
  end;
end;

procedure TFmLocaleInfo3.InicializaLocalizacao();
begin
  Timer1.Enabled := True;
  TSensorManager.Current.Activate();
  //if not FCriado then
    CreateIfExists(TSensorCategory.Location);
  //
  ChangeTabAction1.Execute;
end;

procedure TFmLocaleInfo3.ListBoxItemClick(Sender: TObject);
begin
  if Sender is TListBoxItem then
    FActiveSensor := TCustomSensor(TListBoxItem(Sender).Data);
  FShowInfo := True;
end;

{
function TFmLocaleInfo3.ObtemDadosLocalizacao(): Boolean;
begin
  VAR_LOC_LATI_LONGI := False;
  Result := False;
  //
  if not Assigned(FActiveSensor) then
  //if FActiveSensor = nil then
  begin
    InicializaLocalizacao();
  end;
  //
  while VAR_LOC_LATI_LONGI = False do
  begin
    Sleep(50);
    //
    Geral.ProcessaMsgs;
  end;
  //
  MeDados.Lines.Add(Geral.FDT(VAR_LOC_DATA_HORA, 0) + ': ' +
    Geral.ToFormStr('NS ', VAR_LATITUDE) + ' ' +
    Geral.ToFormStr('LE ', VAR_LONGITUDE));
  //
  Result := True;
  //
  DesabilitaLocalizacao();
end;
}

procedure TFmLocaleInfo3.SpeedButton1Click(Sender: TObject);
begin
{
  DesabilitaLocalizacao();
}
end;

procedure TFmLocaleInfo3.SpeedButton2Click(Sender: TObject);
begin
  InicializaLocalizacao();
end;

procedure TFmLocaleInfo3.Timer1Timer(Sender: TObject);
var
  ResultText : string;
  LStep : Single;
begin
  FTimerCount := FTimerCount + 1;
{
  SysDebug(
    'Assigned(FActiveSensor) = ' + BoolToStr(Assigned(FActiveSensor))
    + '| FShowInfo = ' + BoolToStr(FShowInfo)
  );
}
  if Assigned(FActiveSensor) then
  begin
    case FActiveSensor.Category of
      TSensorCategory.Location: ResultText := GetInfoAboutLocation(FActiveSensor);
{
      TSensorCategory.Environmental: ResultText := GetInfoAboutEnv(FActiveSensor);
      TSensorCategory.Motion: ResultText := GetInfoAboutMotion(FActiveSensor);
      TSensorCategory.Orientation: ResultText := GetInfoAboutOrientation(FActiveSensor);
      TSensorCategory.Mechanical: ResultText := GetInfoAboutMechanical(FActiveSensor);
      TSensorCategory.Electrical: ResultText := GetInfoAboutElectro(FActiveSensor);
      TSensorCategory.Biometric: ResultText := GetInfoAboutBiometric(FActiveSensor);
      TSensorCategory.Light: ResultText := GetInfoAboutLight(FActiveSensor);
      TSensorCategory.Scanner: ResultText := GetInfoAboutScanner(FActiveSensor);
}
    end;
    //lInfo.Text := ResultText;
  end;
{
  if not FOnOneScreen then
  begin
    if FShowInfo then
    begin
      if lInfo.Position.Point.X > (cBorder*2) then
      begin
        LStep := Width / 5;
        lInfo.Position.Point := PointF(lInfo.Position.Point.X - LStep, cBorder);
        lbMain.Position.Point := PointF(lbMain.Position.Point.X - LStep, cBorder);
      end;
    end
    else
    begin
      if lbMain.Position.Point.X < cBorder then
      begin
        LStep := Width / 5;
        lInfo.Position.Point := PointF(lInfo.Position.Point.X + LStep, cBorder);
        lbMain.Position.Point := PointF(lbMain.Position.Point.X + LStep, cBorder);
      end;
    end;
  end;
}
end;

end.
