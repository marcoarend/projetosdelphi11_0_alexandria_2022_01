unit UnDmkDevice;

interface


uses System.SysUtils, FMX.Forms,
{$IFDEF IOS}
  iOSapi.UIKit,
{$ENDIF}
{$IF DEFINED(ANDROID)}
Androidapi.JNI.Telephony, Androidapi.JNI.Provider ,
Androidapi.JNIBridge, Androidapi.JNI.GraphicsContentViewText ,
Androidapi.JNI.JavaTypes, FMX.Helpers.Android, androidapi.JNI.Os,
{$ENDIF}
{$IF DEFINED (MSWINDOWS)}
  Winapi.Windows, UnitMD5,
{$ENDIF}
  UnGeral, UnGrl_Vars;

type
  TUnDmkDevice = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemIMEI_MD5(): String;
    function  ObtemDadosAparelho(var OSName, DeviceType, OSVersion, OSNickName:
              String): Boolean;
    procedure PlataformaNaoImplementada(Texto: String);
  end;

var
  DmkDevice: TUnDmkDevice;

//const

implementation

{ TUnDmkDevice }

{$IF DEFINED (ANDROID)}
function TUnDmkDevice.ObtemIMEI_MD5(): String;
var
  Obj: JObject;
  TM: JTelephonyManager;
  Identifier: String;
begin
  if VAR_DEVICE_ID = '' then
  begin
    Obj := SharedActivityContext.getSystemService(TJContext.JavaClass.TELEPHONY_SERVICE);
    if Obj <> nil then
    begin
      TM := TJTelephonyManager.Wrap( (Obj as ILocalObject).GetObjectID );
      if TM <> nil then
        Identifier := JStringToString(TM.getDeviceId);
    end;
    if Identifier = '' then
      Identifier := JStringToString(TJSettings_Secure.JavaClass.getString(
        SharedActivity.getContentResolver, TJSettings_Secure.JavaClass.ANDROID_ID));
    //
    VAR_DEVICE_ID := Identifier;
  end;
  Result := VAR_DEVICE_ID;
end;
{$ENDIF}

{$IF DEFINED (MSWINDOWS)}
function TUnDmkDevice.ObtemIMEI_MD5(): String;
  function CalculaValSerialKey2(DataVerifi: String; Web: Boolean): String;
    function SerialNum(FDrive:String): String;
    var
      nVNameSer: PDWORD;
      pVolName: PChar;
      FSSysFlags, maxCmpLen: DWord;
      pFSBuf: PChar;
      drv: String;
    begin
      try
        drv := FDrive + '\';

        GetMem(pVolName, MAX_PATH);
        GetMem(pFSBuf, MAX_PATH);
        GetMem(nVNameSer, MAX_PATH);

        GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
          maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

        Result := Geral.FF0(nVNameSer^);

        FreeMem(pVolName, MAX_PATH);
        FreeMem(pFSBuf, MAX_PATH);
        FreeMem(nVNameSer, MAX_PATH);
      except
        Result := '';
      end;
    end;

    function MacAddress2: String;
    var
      a,b,c,d: LongWord;
      CPUID: String;
    begin
      asm
        push EAX
        push EBX
        push ECX
        push EDX

        mov eax, 1
        db $0F, $A2
        mov a, EAX
        mov b, EBX
        mov c, ECX
        mov d, EDX

        pop EDX
        pop ECX
        pop EBX
        pop EAX

        {
        mov eax, 1
        db $0F, $A2
        mov a, EAX
        mov b, EBX
        mov c, ECX
        mov d, EDX
        }
      end;
      CPUID  := IntToHex(a,8);// + inttohex(b,8) + inttohex(c,8) + inttohex(d,8);
      Result := CPUID;
    end;
  var
    Unidade, HD, MAC: String;
  begin
    //SerialKey = N�mero do HD + N�mero do CPU + Data da �ltima verifica��o
    //
    Unidade := ExtractFileDrive(ParamStr(0));//ExtractFileDrive(Application.ExeName);
    HD      := SerialNum(Unidade);
    MAC     := MacAddress2;
    //
    if Web then
      Result := UnMD5.StrMD5(HD + MAC)
    else
      Result := UnMD5.StrMD5(HD + MAC + DataVerifi);
  end;
begin
  if VAR_DEVICE_ID = '' then
    VAR_DEVICE_ID := CalculaValSerialKey2('', True);
  Result := VAR_DEVICE_ID;
end;
{$ENDIF}

procedure TUnDmkDevice.PlataformaNaoImplementada(Texto: String);
begin
  Geral.MB_Erro(
  'Ops! Sistema operacional sem implementa��o para a a��o solicitada!');
  if Texto <> '' then
    Geral.MB_Erro(
    'A��o solicitada: ' + Texto);
end;

{$IFDEF IOS}
function TUnDmkDevice.ObtemDadosAparelho(var OSName, DeviceType, OSVersion, OSNickName: String): Boolean;
var
  Device : UIDevice;
begin
  Device := TUIDevice.Wrap(TUIDevice.OCClass.currentDevice);

  OSName := Device.systemName.UTF8String;
  OSNickName := Device.systemVersion.UTF8String;
  OSVersion := Device.systemVersion.UTF8String;
  DeviceType := Device.model.UTF8String;
end;
{$ENDIF}

{$IFDEF ANDROID}
type TAndroidVersion = (
  UNKNOWN,
  BASE,
  BASE_1_1,
  CUPCAKE,
  CUR_DEVELOPMENT,
  DONUT,
  ECLAIR,
  ECLAIR_0_1,
  ECLAIR_MR1,
  FROYO,
  GINGERBREAD,
  GINGERBREAD_MR1,
  HONEYCOMB,
  HONEYCOMB_MR1,
  HONEYCOMB_MR2,
  ICE_CREAM_SANDWICH,
  ICE_CREAM_SANDWICH_MR1,
  JELLY_BEAN,
  JELLY_BEAN_MR1,
  JELLY_BEAN_MR2
);

function TUnDmkDevice.ObtemDadosAparelho(var OSName, DeviceType, OSVersion,
OSNickName: String): Boolean;
var
  codename: string;
  version: TAndroidVersion;
begin
  codename := 'Unknown';
  version := TAndroidVersion(TJBuild_VERSION.JavaClass.SDK_INT);
  DeviceType := JStringToString(TJBuild.JavaClass.MODEL);


  case version of
    UNKNOWN: codename := 'Unknown';
    BASE,
    BASE_1_1: codename := 'Base';
    CUPCAKE: codename := 'Cupcake';
    CUR_DEVELOPMENT: codename := 'Curent Development';
    DONUT: codename := 'Donut';
    ECLAIR,
    ECLAIR_0_1,
    ECLAIR_MR1: codename := 'Eclair';
    FROYO: codename := 'Froyo';
    GINGERBREAD,
    GINGERBREAD_MR1: codename := 'Gingerbread';
    HONEYCOMB,
    HONEYCOMB_MR1,
    HONEYCOMB_MR2: codename := 'Honeycomb';
    ICE_CREAM_SANDWICH,
    ICE_CREAM_SANDWICH_MR1: codename := 'Ice Cream Sandwich';
    JELLY_BEAN,
    JELLY_BEAN_MR1,
    JELLY_BEAN_MR2: codename := 'Jelly Bean';
  end;
  OSVersion := Geral.FF0(Integer(version));
  OSNickName := codename;
  OSVersion := JStringToString(TJBuild_VERSION.JavaClass.RELEASE);
  Result := True;
end;
{$ENDIF}

{$IFDEF MSWINDOWS}
function TUnDmkDevice.ObtemDadosAparelho(var OSName, DeviceType, OSVersion,
OSNickName: String): Boolean;
begin
  OSName := 'Windows 8';
  OSNickName := 'Windows 8.0';
  OSVersion := '8.0';
  DeviceType := 'Conversivel XPS 12';
  //
  Result := False;
end;
{$ENDIF}

end.
