unit UnDmkQr;

interface

uses System.SysUtils, System.Classes,
  FMX.Memo, FMX.StdCtrls, FMX.Forms,
  FireDAC.Comp.Client,
  Data.DB,
  UnDmkEnums, UnGrl_Vars, UnGeral, UnApp_Consts, (*dmkCNClearingEdit,*)
  dmkCNEdit, dmkCNPanel;

type
  TUnDmkQr = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function I32(Qry: TFDQuery; Campo: String): Integer;
    function Str(Qry: TFDQuery; Campo: String): String;
  end;

var
  DmkQr: TUnDmkQr;

implementation

{ TUnDmkQr }

function TUnDmkQr.I32(Qry: TFDQuery; Campo: String): Integer;
begin
  try
    Result := Qry.FieldByName(Campo).AsInteger;
  except
    on E: Exception do
    begin
      Geral.MB_Erro('Erro conversao tipo field:' + sLineBreak +
      'Qry: ' + Qry.Name + sLineBreak +
      'Campo: ' + Campo + sLineBreak +
      'Mensagem: ' + sLineBreak + E.Message);
    end;
  end;
end;

function TUnDmkQr.Str(Qry: TFDQuery; Campo: String): String;
begin
  Result := Qry.FieldByName(Campo).AsString;
end;

end.
