unit UnMySQL_PF;

interface

uses System.Classes, System.SysUtils, System.UITypes, System.Variants,
  FMX.Layouts, FMX.Forms,
  FireDAC.Comp.Client,
  UnGeral, UnDmkEnums, UnDmkFireDAC, UnGrl_Vars,
  {$IFNDEF DmkMobile} dmkEdit, UnDmkPF, {$ENDIF} UnGrl_Consts;

type
  TUnMySQL_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
              SQL: array of String): Boolean;
    function  BPGS1I32(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
    function  BPGS1I32_Reaproveita(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
    function  CarregaSQLInsUpd(var SQLx: String; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String): Boolean;
    function  GetQrCount(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String): Integer;
    function  GetQrVal1Str(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String; var Valor: String): Integer;
    function  GetQrVal1Int(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String; var Valor: Integer): Integer;
    function  SQLInsUpd(Form: TForm; DmkDBSrc: TDmkDBSrc; Tipo: TSQLType;
              Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  GetQrVals(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String; const Campos: array of String;
              var Valores: array of Variant): Boolean;
    {$IFNDEF DmkMobile}
    function  LoadDBParams(): Boolean;
    {$ENDIF}
    function  DefineVarsIniDB(): Boolean;
end;


var
  MySQL_PF: TUnMySQL_PF;

implementation

uses ModGerl, ModApp;

{ TUnMySQL_PF }

{
function TUnMySQL_PF.CarregaSQLInsUpd(var SQL: String; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  SQL := '';
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('AVISO! Existem ' + IntToStr(i+1) + ' �ndices informados ' +
    'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Aviso('AVISO IMPORTANTE: O status da a��o est� definida como ' +
    '"' + DmkEnums.NomeTipoSQL(Tipo) + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  SQL := '';
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      SQL := SQL + 'INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ' + sLineBreak
    else
      SQL := SQL + 'INSERT INTO ' + Lowercase(Tab) + ' SET ' + sLineBreak;
  end else begin
    SQL := SQL + 'UPDATE ' + Lowercase(Tab) + ' SET ' + sLineBreak;
    if UserDataAlterweb then
      SQL := SQL + 'AlterWeb=1, ' + sLineBreak;
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    // 2011-12-10
    if SQLCampos[i] = CO_JOKE_SQL then
    begin
      if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
        SQL := SQL + ValCampos[i] + ', ' + sLineBreak
      else
        SQL := SQL + ValCampos[i] + sLineBreak;
    end else
    // fim 2011-12-10
    begin
      Valor := Geral.VariavelToString(ValCampos[i]);
      if (i < j) or UserDataAlterweb then
       SQL := SQL + SQLCampos[i] + '=' + Valor + ', ' + sLineBreak
      else
        SQL := SQL + SQLCampos[i] + '=' + Valor + sLineBreak;
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
      SQL := SQL + 'DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO) + sLineBreak
    else
      SQL := SQL + 'DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO) + sLineBreak;
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else begin

    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        SQL := SQL + Liga + ValIndex[i] + sLineBreak
      else
      // fim 2011-12-02
      begin
        Valor := Geral.VariavelToString(ValIndex[i]);
        SQL := SQL + Liga + SQLIndex[i] + '=' + Valor + sLineBreak;
      end;
    end;
  end;
  if Trim(ComplUpd) <> '' then
  begin
    SQL := SQL + ComplUpd + sLineBreak;
  end;
  SQL := SQL + ';' + sLineBreak;
  //
  Result := True;
end;
}

{ TUnMySQL_PF }

function TUnMySQL_PF.AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
  SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
  //
begin
  Result := False;
  Query.Close;
  Query.Connection := FD;
  //
  if Form <> nil then
    MyCursor := Form.Cursor;
  try
    if Form <> nil then
      Form.Cursor := crHourGlass;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no SQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.Open;
      Result := True;
    end;
    if Form <> nil then
      Form.Cursor := MyCursor;
  except
    on e: Exception do
    begin
      Geral.MB_Erro('Erro ao tentar abrir uma SQL no SQLQuery!' + sLineBreak +
      TComponent(Query.Owner).Name + '.' + Query.Name + sLineBreak + e.Message +
      sLineBreak + Query.SQL.Text);
      //
      if Form <> nil then
        Form.Cursor := MyCursor;
    end;
  end;
end;

function TUnMySQL_PF.BPGS1I32(Form: TForm; DmkDBSrc: TDmkDBSrc; Tabela, Campo,
  _WHERE, _AND: String; TipoSinal: TTipoSinal; SQLType: TSQLType;
  DefUpd: Integer): Integer;
const
  txtNeg = 'BigIntNeg';
  txtPos = 'BigIntPos';
  txtErr = 'BigInt???';
var
  Tab, Fld, MiM: String;
  Atual, P: Integer;
  Forma: TSQLType;
  Sinal: TTipoSinal;
  //DataBase: TmySQLDatabase;
  Connection: TFDConnection;
begin
  Result := 0;
  Connection := DmkFireDAC.ObtemFDConnection(DmkDBSrc);
  case SQLType of
    stUpd: Result := DefUpd;
    stIns:
    begin
      Tab := LowerCase(Tabela);
      P := pos('.', Tab);
      if P > 0 then
        Tab := Copy(Tab, P + 1);
      //
      Fld := txtErr;
      case TipoSinal of
        tsPos: Fld := TxtPos;
        tsNeg: Fld := txtNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Fld := txtNeg
          else
            Fld := txtPos;
        end;
      end;
      DmModGerl.QvUpdY.Close;
      //DmModGerl.QvUpdY.Database := Database;
      DmkFireDAC.DefineFDConnection(DmModGerl.QvUpdY, DmkDBSrc);

      DmModGerl.QvUpdY.SQL.Clear;
      DmModGerl.QvUpdY.SQL.Add('LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;');
      DmModGerl.QvUpdY.ExecSQL;
      //
      DmModGerl.QvLivreY.Close;
      //DmModGerl.QvLivreY.Database := Database;
      DmkFireDAC.DefineFDConnection(DmModGerl.QvLivreY, DmkDBSrc);
      AbreSQLQuery0(Form, DmModGerl.QvLivreY,  Connection, [
      'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
      'WHERE Tabela=' + QuotedStr(LowerCase(Tab)),
      'AND Campo=' + QuotedStr(LowerCase(Campo)),
      'AND _WHERE=' + QuotedStr(LowerCase(_WHERE)),
      'AND _AND=' + QuotedStr(LowerCase(_AND)),
      '']);
{
      MsgDmk.MostraTexto(DmModGerl.QvLivreY.SQL.Text);
      Exit;
}
      if DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant <> Null then
        Atual := DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant
      else
        Atual := 0;
      // Verificar se j� existe
      AbreSQLQuery0(Form, DmModGerl.QvLivreY,  Connection, [
      'SELECT ' + Campo + ' Codigo FROM ' + LowerCase(Tab),
      'WHERE ' + Campo + '=' + Geral.FF0(Atual),
      Geral.ATS_if(_WHERE<>'', ['AND ' + Lowercase(_WHERE)]),
      Geral.ATS_if(_AND<>'', ['AND ' + LowerCase(_AND)]),
      '']);

      if (Atual = 0) then
        Forma := stIns
      else
        Forma := stUpd;
      // Evitar duplicidade buscando na tabela de inser��o:
      if (Atual = 0) or (DmModGerl.QvLivreY.RecordCount > 0) then
      begin
        case TipoSinal of
          tsPos: MiM := 'MAX(';
          tsNeg: MiM := 'MIN(';
          tsDef:
          begin
            if VAR_ForcaBigIntNeg then
              MiM := 'MIN('
            else
              MiM := 'MAX(';
          end;
          else MiM := 'MINMAX?(';
        end;
        //
        AbreSQLQuery0(Form, DmModGerl.QvLivreY,  Connection, [
        'SELECT ' + MiM + Campo + ') Codigo FROM ' + Tab,
        Geral.ATS_if(_WHERE<>'', ['WHERE ' + LowerCase(_WHERE)]),
        Geral.ATS_if(_AND<>'', ['AND ' + LowerCase(_AND)]),
        '']);
        //
        if DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant <> Null then
          Atual := DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant
        else
          Atual := 0;
      end;
      // Fim evitar duplicidade!
      //
      case TipoSinal of
        tsPos: Sinal := tsPos;
        tsNeg: Sinal := tsNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Sinal := tsNeg
          else
            Sinal := tsPos;
        end;
        else Sinal := tsPos;
      end;

      case Sinal of
        tsPos:
        begin
          if Atual < 1 then
            Result := 1 // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
          else
            Result := Atual + 1;
        end;
        tsNeg:
        begin
          if Atual > -1 then
            Result := -1
          else
            Result := Atual - 1;
        end;
      end;
      //
      DmModGerl.QvLivreY.Close;
      //
      SQLInsUpd(Form, DmkDBSrc, Forma, 'gerlseq1', False, [
      //SQLInsUpd(QvUpdY, Forma, 'gerlseq1', False, [
      Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
      Result], [LowerCase(Tab), LowerCase(Campo), LowerCase(_WHERE), LowerCase(_AND)], True);
      //
      DmModGerl.QvUpdY.SQL.Clear;
      DmModGerl.QvUpdY.SQL.Add('UNLOCK TABLES;');
      DmModGerl.QvUpdY.ExecSQL;
    end;
    else Geral.MB_Erro('"SQLType" n�o implementado no "TUnSQLite_PF.BPGS1I64"');
  end;
end;

function TUnMySQL_PF.BPGS1I32_Reaproveita(Form: TForm; DmkDBSrc: TDmkDBSrc;
  Tabela, Campo, _WHERE, _AND: String; TipoSinal: TTipoSinal; SQLType: TSQLType;
  DefUpd: Integer): Integer;
const
  TabLivre = 'livre2';
var
  Codigo : Integer;
  Continua: Boolean;
  //Database: TmySQLDatabase;
  Connection: TFDConnection;
begin
  Connection := DmkFireDAC.ObtemFDConnection(DmkDBSrc);
  Codigo := 0;
  Continua := True;
  //Database := Dmod.MyDB;
  //
  DmModGerl.QvUpdY.Close;
  //QvUpdY.Database := Database;
  DmkFireDAC.DefineFDConnection(DmModGerl.QvUpdY, DmkDBSrc);

  DmModGerl.QvUpdY.SQL.Clear;
  DmModGerl.QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    Lowercase(Tabela) + ' WRITE;');
  DmModGerl.QvUpdY.ExecSQL;

  DmModGerl.QvUpdY.SQL.Clear;
  DmModGerl.QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre)+' WHERE Codigo < 1');
  DmModGerl.QvUpdY.ExecSQL;

  while Continua do
  begin
    DmModGerl.QvLivreY.Close;
    //DmModGerl.QvLivreY.Database := Database;
    DmkFireDAC.DefineFDConnection(DmModGerl.QvLivreY, DmkDBSrc);
    AbreSQLQuery0(Form, DmModGerl.QvLivreY, Connection, [
    'SELECT Codigo ',
    'FROM ' + TabLivre,
    'WHERE Tabela=' + QuotedStr(Lowercase(Tabela)),
    'AND Campo=' + QuotedStr(Campo),
    'ORDER BY Codigo ',
    'LIMIT 1',
    '']);
    //
    if (DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant <> Null) and
    (DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant > 0) then
    begin
      Codigo := DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant;
      DmModGerl.QvUpdY.Close;
      DmModGerl.QvUpdY.SQL.Clear;
      DmModGerl.QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre));
      DmModGerl.QvUpdY.SQL.Add('WHERE Tabela=' + QuotedStr(Lowercase(Tabela)));
      DmModGerl.QvUpdY.SQL.Add('AND Campo=' + QuotedStr(Campo));
      DmModGerl.QvUpdY.SQL.Add('AND Codigo=' + Geral.FF0(Codigo));
      DmModGerl.QvUpdY.ExecSQL;
      // Evitar duplica��o
      //DmModGerl.QvLivreY.Database := Database;
      DmModGerl.QvLivreY.SQL.Clear;
      DmModGerl.QvLivreY.SQL.Add('SELECT ' + Campo + ' Codigo FROM ' + Lowercase(Tabela));
      DmModGerl.QvLivreY.SQL.Add('WHERE ' + Campo + '=' + Geral.FF0(Codigo));
      DmModGerl.QvLivreY.Open;
      if DmModGerl.QvLivreY.RecordCount > 0 then
      begin
        Codigo := 0;
        Continua := True;
      end else Continua := False;
    end else Continua := False;
  end;
  DmModGerl.QvUpdY.SQL.Clear;
  DmModGerl.QvUpdY.SQL.Add('UNLOCK TABLES;');
  DmModGerl.QvUpdY.ExecSQL;
  //

  if Codigo = 0 then
  begin
    Result := BPGS1I32(Form, DmkDBSrc,
      LowerCase(Tabela), LowerCase(Campo), LowerCase(_WHERE), LowerCase(_AND),
      TipoSinal, SQLType, DefUpd);
  end else Result := Codigo;
end;

function TUnMySQL_PF.CarregaSQLInsUpd(var SQLx: String; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab, Texto, X, Y: String;
begin
  Result := False;
  Texto := '';
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('AVISO! Existem ' + IntToStr(i+1) + ' �ndices informados ' +
    'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Aviso('AVISO IMPORTANTE: O status da a��o est� definida como ' +
    '"' + DmkEnums.NomeTipoSQL(Tipo) + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      Texto := Texto + 'INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ' + sLineBreak
    else
      Texto := Texto + 'INSERT INTO ' + Lowercase(Tab) + ' SET ' + sLineBreak;
  end else begin
    Texto := Texto + 'UPDATE ' + Lowercase(Tab) + ' SET ' + sLineBreak;
    if UserDataAlterweb then
      Texto := Texto + 'AlterWeb=1, ' + sLineBreak;
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    X := ValCampos[I];
    Y := SQLCampos[I];
    //
    if SQLCampos[i] = CO_JOKE_SQL then
    begin
      if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
        Texto := Texto + X + ', ' + sLineBreak
        //Texto := Texto + ValCampos[i] + ', ' + sLineBreak
      else
        Texto := Texto + X + sLineBreak;
        //Texto := Texto + ValCampos[i] + sLineBreak;
    end else
    begin
      Valor := Geral.VariavelToString(ValCampos[i]);
      if (i < j) or UserDataAlterweb then
       //Texto := Texto + SQLCampos[i] + '=' + Valor + ', ' + sLineBreak
       Texto := Texto + Y + '=' + Valor + ', ' + sLineBreak
      else
        //Texto := Texto + SQLCampos[i] + '=' + Valor + sLineBreak;
        Texto := Texto + Y + '=' + Valor + sLineBreak;
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
      Texto := Texto + 'DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO) + sLineBreak
    else
      Texto := Texto + 'DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO) + sLineBreak;
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('Texto com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else begin
    for I := Low(SQLIndex) to High(SQLIndex) do
    begin
      X := ValIndex[I];
      Y := SQLIndex[I];
      //
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        //Texto := Texto + Liga + ValIndex[i] + sLineBreak
        Texto := Texto + Liga + X + sLineBreak
      else
      // fim 2011-12-02
      begin
        Valor := Geral.VariavelToString(ValIndex[i]);
        //Texto := Texto + Liga + SQLIndex[i] + '=' + Valor + sLineBreak;
        Texto := Texto + Liga + Y + '=' + Valor + sLineBreak;
      end;
    end;
  end;
  if Trim(ComplUpd) <> '' then
  begin
    Texto := Texto + ComplUpd + sLineBreak;
  end;
  //Texto := Texto + ';' + sLineBreak;
  Texto := Texto + sLineBreak;
  //
  Result := True;
  SQLx := Texto;
end;

function TUnMySQL_PF.DefineVarsIniDB(): Boolean;
begin
  VAR_RDBMS_DriverID := 'mysql'; // ODBC
  VAR_RDBMS_ODBCDriver := '';  // 'MySQL ODBC 5.3 ANSI Driver';
  VAR_RDBMS_SERVER := '127.0.0.1';
  VAR_RDBMS_PORT := '3306';
  VAR_RDBMS_USER := 'root';
  VAR_RDBMS_PWD  := '';
  //
{
  //VAR_APP_DB_NAME_E_EXT := 'bugstrol.s3db';
  VAR_APP_DB_FULLPATH := Geral.DirAppAllOS(VAR_APP_DB_NAME_E_EXT);
  VAR_LIBERA_TODOS_FORMS := False;
  VAR_LOC_LATI_LONGI := False;
  VAR_LATITUDE := 0;
  VAR_LONGITUDE := 0;
  VAR_DEVICE_ID := '';
}
  //
  Result := TMeuDB <> '';
  if not Result then
    Geral.MB_Erro('VarsIniBD incompletas!');
end;

function TUnMySQL_PF.GetQrCount(const Form: TForm; const FDCon: TFDConnection;
  const SQL: array of String): Integer;
var
  Qry: TFDQuery;
begin
  Result := -1;
  Qry := TFDQuery.Create(DmModApp);
  try
    AbreSQLQuery0(Form, Qry, FDCon, SQL);
    Result := Qry.RecordCount;
  finally
    Qry.Free;
  end;
end;

function TUnMySQL_PF.GetQrVal1Int(const Form: TForm; const FDCon: TFDConnection;
  const SQL: array of String; var Valor: Integer): Integer;
var
  Qry: TFDQuery;
begin
  Result := -1;
  Qry := TFDQuery.Create(DmModApp);
  try
    AbreSQLQuery0(Form, Qry, FDCon, SQL);
    Result := Qry.RecordCount;
    if Result > 0 then
      Valor := Qry.Fields[0].AsInteger
    else
      Valor := 0;
  finally
    Qry.Free;
  end;
end;

function TUnMySQL_PF.GetQrVal1Str(const Form: TForm; const FDCon: TFDConnection;
  const SQL: array of String; var Valor: String): Integer;
var
  Qry: TFDQuery;
begin
  Result := -1;
  Qry := TFDQuery.Create(DmModApp);
  try
    AbreSQLQuery0(Form, Qry, FDCon, SQL);
    Result := Qry.RecordCount;
    if Result > 0 then
      Valor := Qry.Fields[0].AsString
    else
      Valor := '';
  finally
    Qry.Free;
  end;
end;

function TUnMySQL_PF.GetQrVals(const Form: TForm; const FDCon: TFDConnection;
  const SQL, Campos: array of String; var Valores: array of Variant): Boolean;
var
  Qry: TFDQuery;
  I: Integer;
begin
  Result := False;
  Qry := TFDQuery.Create(Form);
  try
    AbreSQLQuery0(Form, Qry, FDCon, SQL);
    if High(Campos) <> High(Valores) then
      Geral.MB_Erro('Qunatidade de campos difere da quantidade de valores!' +
      sLineBreak + 'TUnMySQL_PF.GetQrVals()');
    for I := Low(SQL) to High(SQL) do
    begin
      Valores[I] := Qry.Fields[I].AsVariant;
    end;
    Result := True;
  finally
    Qry.Free;
  end;
end;

{$IFNDEF DmkMobile}
function TUnMySQL_PF.LoadDBParams: Boolean;
  function ObtemPWD(Texto: String): String;
  begin
    Result := DmkPF.CriptografiaSimples_d3(Texto);
    Result := DmkPF.CriptografiaSimples(Result, CO_SecureStr_001);
  end;
  function ReadReg(Campo, Default: String): String;
  begin
    Result := DmkPF.ReadAppKeyLM(
      Application.Title, ['ConexaoBD'], Campo, ktString, Default);
  end;
var
  PWD: String;
begin
  VAR_RDBMS_DriverID   :=  ReadReg('DriverID'   , VAR_RDBMS_DriverID);
  VAR_RDBMS_ODBCDriver :=  ReadReg('ODBCDriver' , VAR_RDBMS_ODBCDriver);
  VAR_RDBMS_SERVER     :=  ReadReg('SERVER'     , VAR_RDBMS_SERVER);
  VAR_RDBMS_PORT       :=  ReadReg('PORT'       , VAR_RDBMS_PORT);
  VAR_RDBMS_USER       :=  ReadReg('USER'       , VAR_RDBMS_USER);
  VAR_RDBMS_PWD        :=  ReadReg('PWD'        , PWD);
  TMeuDB               :=  ReadReg('DB_NAME'    , TMeuDB);
  //
  VAR_RDBMS_PWD := ObtemPWD(VAR_RDBMS_PWD);
  if VAR_RDBMS_PWD = '' then
    VAR_RDBMS_PWD := CO_USERSPNOW;
  //VAR_APP_DB_NAME_ONLY := TMeuDB;
end;
{$ENDIF}

function TUnMySQL_PF.SQLInsUpd(Form: TForm; DmkDBSrc: TDmkDBSrc; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb: Boolean;
  ComplUpd: String; InfoSQLOnError, InfoErro: Boolean): Boolean;
var
  QrUpd: TFDQuery;
  Texto: String;
begin
  QrUpd := TFDQuery.Create(Form);
  DmkFireDAC.DefineFDConnection(QrUpd, DmkDBSrc);
  try
    Result := CarregaSQLInsUpd(Texto, Tipo, lowercase(Tabela), Auto_increment,
    SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, False, ComplUpd);
    if Result then
    begin
      try
        QrUpd.SQL.Text := Texto;
        QrUpd.ExecSQL;
        Result := True;
      except
        on E: Exception do
        begin
          if InfoSQLOnError then
            Geral.MB_Erro(E.Message + sLineBreak + QrUpd.SQL.Text);
          if InfoErro then
            raise;
        end;
      end;
    end else;
  finally
    QrUpd.Free;
  end;
end;

end.
