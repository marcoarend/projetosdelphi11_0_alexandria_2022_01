unit UnDmkFireDAC;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs, Data.Bind.Components,
  Data.Bind.Grid, Data.Bind.DBScope, Data.DB, FireDAC.Comp.DataSet, FMX.Edit,
  FireDAC.Comp.Client, System.Variants, FMX.StdCtrls, FMX.Forms, FMX.Layouts,
  FMX.TabControl, FMX.Types, FMX.Ani, FMX.Controls, System.TypInfo,
  dmkFormVars, UnGeral, UnDmkEnums, dmkTabItem, dmkSpeedButton, UnGrl_Vars,
  UnMyObjects, dmkEdit, UnGrl_Consts;

type
  TUnDmkFireDAC = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function DefineFDConnection(Qry: TFDQuery; DmkDBSrc: TDmkDBSrc): Boolean;
    function ObtemFDConnection(DmkDBSrc: TDmkDBSrc): TFDConnection;
  end;
var
  DmkFireDAC: TUnDmkFireDAC;

implementation

{ TUnDmkFireDAC }

function TUnDmkFireDAC.DefineFDConnection(Qry: TFDQuery;
  DmkDBSrc: TDmkDBSrc): Boolean;
begin
  //Result := False;
  case DmkDBSrc of
    dbsrcLocalServer : Qry.Connection := VAR_SQLITE_DB_LOCAL_SERVER;
    dbsrcLocalUser   : Qry.Connection := VAR_SQLITE_DB_LOCAL_USER;
    dbsrcWebServer   : Qry.Connection := VAR_SQLITE_DB_WEB_SERVER;
    dbsrcWebUser     : Qry.Connection := VAR_SQLITE_DB_WEB_USER;
    else
    begin
      Qry.Connection := nil;
      Geral.MB_Erro('"dbsrc..." n�o definido!');
    end;
  end;
  Result := Qry.Connection <> nil;
end;

function TUnDmkFireDAC.ObtemFDConnection(DmkDBSrc: TDmkDBSrc): TFDConnection;
begin
  //Result := False;
  case DmkDBSrc of
    dbsrcLocalServer : Result := VAR_SQLITE_DB_LOCAL_SERVER;
    dbsrcLocalUser   : Result := VAR_SQLITE_DB_LOCAL_USER;
    dbsrcWebServer   : Result := VAR_SQLITE_DB_WEB_SERVER;
    dbsrcWebUser     : Result := VAR_SQLITE_DB_WEB_USER;
    else
    begin
      Result := nil;
      Geral.MB_Erro('"dbsrc..." n�o definido!');
    end;
  end;
end;

end.
