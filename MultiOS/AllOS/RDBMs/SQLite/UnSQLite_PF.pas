unit UnSQLite_PF;

interface

uses
  System.Classes, System.SysUtils, System.UITypes,
  (*Data.SqlExpr,*) FireDAC.Comp.Client,
  FMX.Layouts, FMX.Memo, FMX.Forms,
  UnGeral, UnDmkEnums, Variants, UnDmkFireDAC, UnGrl_Consts, UnGrl_Vars;

type
  TUnSQLite_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
              SQL: array of String): Boolean;
    function  CarregaSQLInsUpd(QrUpd: TFDQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean; ComplUpd: String): Boolean;
    function  ExecutaSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
              SQL: array of String): Boolean;
    procedure MostraSQL(Query: TFDQuery; Memo: TMemo; Titulo: String);
    function  SQLInsUpd(Form: TForm; DmkDBSrc: TDmkDBSrc; Tipo: TSQLType;
              Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  BPGS1I64(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
    function  BPGS1I64_Reaproveita(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;

  end;


var
  SQLite_PF: TUnSQLite_PF;

implementation

uses UnDmkSQLite, ModGerl, UnMsgDmk;

{ TUnSQLite_PF }

function TUnSQLite_PF.AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
  SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
  //
begin
  Result := False;
  Query.Close;
  Query.Connection := FD;
  //
  MyCursor := Form.Cursor;
  try
    Form.Cursor := crHourGlass;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no SQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.Open;
      Result := True;
    end;
    Form.Cursor := MyCursor;
  except
    on e: Exception do
    begin
      Geral.MB_Erro('Erro ao tentar abrir uma SQL no SQLQuery!' + sLineBreak +
      TComponent(Query.Owner).Name + '.' + Query.Name + sLineBreak + e.Message +
      sLineBreak + Query.SQL.Text);
      //
      Form.Cursor := MyCursor;
    end;
  end;
end;

function TUnSQLite_PF.BPGS1I64(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
  DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
const
  txtNeg = 'BigIntNeg';
  txtPos = 'BigIntPos';
  txtErr = 'BigInt???';
var
  Tab, Fld, MiM: String;
  Atual, P: Int64;
  Forma: TSQLType;
  Sinal: TTipoSinal;
  //DataBase: TmySQLDatabase;
  Connection: TFDConnection;
begin
  Result := 0;
  Connection := DmkFireDAC.ObtemFDConnection(DmkDBSrc);
  case SQLType of
    stUpd: Result := DefUpd;
    stIns:
    begin
      Tab := LowerCase(Tabela);
      P := pos('.', Tab);
      if P > 0 then
        Tab := Copy(Tab, P + 1);
      //
      Fld := txtErr;
      case TipoSinal of
        tsPos: Fld := TxtPos;
        tsNeg: Fld := txtNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Fld := txtNeg
          else
            Fld := txtPos;
        end;
      end;
      DmModGerl.QvUpdY.Close;
      //DmModGerl.QvUpdY.Database := Database;
      DmkFireDAC.DefineFDConnection(DmModGerl.QvUpdY, DmkDBSrc);

{ SQLite n�o suporta LOCK !!
      DmModGerl.QvUpdY.SQL.Clear;
      DmModGerl.QvUpdY.SQL.Add('LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;');
      DmModGerl.QvUpdY.ExecSQL;
}
      //
      DmModGerl.QvLivreY.Close;
      //DmModGerl.QvLivreY.Database := Database;
      DmkFireDAC.DefineFDConnection(DmModGerl.QvLivreY, DmkDBSrc);
      AbreSQLQuery0(Form, DmModGerl.QvLivreY,  Connection, [
      'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
      'WHERE Tabela=' + QuotedStr(LowerCase(Tab)),
      'AND Campo=' + QuotedStr(LowerCase(Campo)),
      'AND _WHERE=' + QuotedStr(LowerCase(_WHERE)),
      'AND _AND=' + QuotedStr(LowerCase(_AND)),
      '']);
{
      MsgDmk.MostraTexto(DmModGerl.QvLivreY.SQL.Text);
      Exit;
}
      if DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant <> Null then
        Atual := DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant
      else
        Atual := 0;
      // Verificar se j� existe
      AbreSQLQuery0(Form, DmModGerl.QvLivreY,  Connection, [
      'SELECT ' + Campo + ' Codigo FROM ' + LowerCase(Tab),
      'WHERE ' + Campo + '=' + Geral.FF0(Atual),
      Geral.ATS_if(_WHERE<>'', ['AND ' + Lowercase(_WHERE)]),
      Geral.ATS_if(_AND<>'', ['AND ' + LowerCase(_AND)]),
      '']);

      if (Atual = 0) then
        Forma := stIns
      else
        Forma := stUpd;
      // Evitar duplicidade buscando na tabela de inser��o:
      if (Atual = 0) or (DmModGerl.QvLivreY.RecordCount > 0) then
      begin
        case TipoSinal of
          tsPos: MiM := 'MAX(';
          tsNeg: MiM := 'MIN(';
          tsDef:
          begin
            if VAR_ForcaBigIntNeg then
              MiM := 'MIN('
            else
              MiM := 'MAX(';
          end;
          else MiM := 'MINMAX?(';
        end;
        //
        AbreSQLQuery0(Form, DmModGerl.QvLivreY,  Connection, [
        'SELECT ' + MiM + Campo + ') Codigo FROM ' + Tab,
        Geral.ATS_if(_WHERE<>'', ['WHERE ' + LowerCase(_WHERE)]),
        Geral.ATS_if(_AND<>'', ['AND ' + LowerCase(_AND)]),
        '']);
        //
        if DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant <> Null then
          Atual := DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant
        else
          Atual := 0;
      end;
      // Fim evitar duplicidade!
      //
      case TipoSinal of
        tsPos: Sinal := tsPos;
        tsNeg: Sinal := tsNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Sinal := tsNeg
          else
            Sinal := tsPos;
        end;
        else Sinal := tsPos;
      end;

      case Sinal of
        tsPos:
        begin
          if Atual < 1 then
            Result := 1 // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
          else
            Result := Atual + 1;
        end;
        tsNeg:
        begin
          if Atual > -1 then
            Result := -1
          else
            Result := Atual - 1;
        end;
      end;
      //
      DmModGerl.QvLivreY.Close;
      //
      SQLInsUpd(Form, DmkDBSrc, Forma, 'gerlseq1', False, [
      //SQLInsUpd(QvUpdY, Forma, 'gerlseq1', False, [
      Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
      Result], [LowerCase(Tab), LowerCase(Campo), LowerCase(_WHERE), LowerCase(_AND)], True);
      //
{ SQLite n�o suporta LOCK !!
      DmModGerl.QvUpdY.SQL.Clear;
      DmModGerl.QvUpdY.SQL.Add('UNLOCK TABLES;');
      DmModGerl.QvUpdY.ExecSQL;
}
    end;
    else Geral.MB_Erro('"SQLType" n�o implementado no "TUnSQLite_PF.BPGS1I64"');
  end;
end;

function TUnSQLite_PF.BPGS1I64_Reaproveita(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
  DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
const
  TabLivre = 'livre2';
var
  Codigo : Int64;
  Continua: Boolean;
  //Database: TmySQLDatabase;
  Connection: TFDConnection;
begin
  Connection := DmkFireDAC.ObtemFDConnection(DmkDBSrc);
  Codigo := 0;
  Continua := True;
  //Database := Dmod.MyDB;
  //
  DmModGerl.QvUpdY.Close;
  //QvUpdY.Database := Database;
  DmkFireDAC.DefineFDConnection(DmModGerl.QvUpdY, DmkDBSrc);

{ SQLite n�o suporta LOCK !!
  DmModGerl.QvUpdY.SQL.Clear;
  DmModGerl.QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    Lowercase(Tabela) + ' WRITE;');
  DmModGerl.QvUpdY.ExecSQL;
}

  DmModGerl.QvUpdY.SQL.Clear;
  DmModGerl.QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre)+' WHERE Codigo < 1');
  DmModGerl.QvUpdY.ExecSQL;

  while Continua do
  begin
    DmModGerl.QvLivreY.Close;
    //DmModGerl.QvLivreY.Database := Database;
    DmkFireDAC.DefineFDConnection(DmModGerl.QvLivreY, DmkDBSrc);
    AbreSQLQuery0(Form, DmModGerl.QvLivreY, Connection, [
    'SELECT Codigo ',
    'FROM ' + TabLivre,
    'WHERE Tabela=' + QuotedStr(Lowercase(Tabela)),
    'AND Campo=' + QuotedStr(Campo),
    'ORDER BY Codigo ',
    'LIMIT 1',
    '']);
    //
    if (DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant <> Null) and
    (DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant > 0) then
    begin
      Codigo := DmModGerl.QvLivreY.FieldByName('Codigo').AsVariant;
      DmModGerl.QvUpdY.Close;
      DmModGerl.QvUpdY.SQL.Clear;
      DmModGerl.QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre));
      DmModGerl.QvUpdY.SQL.Add('WHERE Tabela=' + QuotedStr(Lowercase(Tabela)));
      DmModGerl.QvUpdY.SQL.Add('AND Campo=' + QuotedStr(Campo));
      DmModGerl.QvUpdY.SQL.Add('AND Codigo=' + Geral.FF0(Codigo));
      DmModGerl.QvUpdY.ExecSQL;
      // Evitar duplica��o
      //DmModGerl.QvLivreY.Database := Database;
      DmModGerl.QvLivreY.SQL.Clear;
      DmModGerl.QvLivreY.SQL.Add('SELECT ' + Campo + ' Codigo FROM ' + Lowercase(Tabela));
      DmModGerl.QvLivreY.SQL.Add('WHERE ' + Campo + '=' + Geral.FF0(Codigo));
      DmModGerl.QvLivreY.Open;
      if DmModGerl.QvLivreY.RecordCount > 0 then
      begin
        Codigo := 0;
        Continua := True;
      end else Continua := False;
    end else Continua := False;
  end;
{ SQLite n�o suporta LOCK !!
  DmModGerl.QvUpdY.SQL.Clear;
  DmModGerl.QvUpdY.SQL.Add('UNLOCK TABLES;');
  DmModGerl.QvUpdY.ExecSQL;
}
  //

  if Codigo = 0 then
  begin
    Result := BPGS1I64(Form, DmkDBSrc,
      LowerCase(Tabela), LowerCase(Campo), LowerCase(_WHERE), LowerCase(_AND),
      TipoSinal, SQLType, DefUpd);
  end else Result := Codigo;
end;

function TUnSQLite_PF.CarregaSQLInsUpd(QrUpd: TFDQuery; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab, Campos, Valores: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('AVISO! Existem ' + IntToStr(i+1) + ' �ndices informados ' +
    'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Erro('AVISO: O status da a��o est� definida como ' +
    '"' + Geral.NomeTipoSQL(Tipo) + '"');
  end;
  Data := #39 + Geral.FDT(Date, 1) + #39;
  QrUpd.SQL.Clear;
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' (')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' (');
  end else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  Campos  := '';
  Valores := '';
  j := System.High(SQLCampos);
  for i := System.Low(SQLCampos) to j do
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', ' + SQLCampos[i];
      Valor := Geral.VariavelToString(ValCampos[i]);
      Valores := Valores + ', ' + Valor;
    end else
    begin
      if SQLCampos[i] = CO_JOKE_SQL then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
          QrUpd.SQL.Add(ValCampos[i] + ', ')
        else
          QrUpd.SQL.Add(ValCampos[i]);
      end else
      begin
        Valor := Geral.VariavelToString(ValCampos[i]);
        if (i < j) or UserDataAlterweb then
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
        else
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
      end;
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
      //QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
      Campos  := Campos + ', DataCad, UserCad, AlterWeb';
      Valores := Valores + ', ' + Data + ', ' + IntToStr(VAR_USUARIO) + ', 1';
    end else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else begin

    for i := System.Low(SQLIndex) to System.High(SQLIndex) do
    begin
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        QrUpd.SQL.Add(Liga + ValIndex[i])
      else
      // fim 2011-12-02
      begin
        if Tipo = stIns then
        begin
          Campos  := Campos + ', ' + SQLIndex[i];
          Valor := Geral.VariavelToString(ValIndex[i]);
          Valores := Valores + ', ' + Valor;
        end else
        begin
          Valor := Geral.VariavelToString(ValIndex[i]);
          QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;
  if Tipo = stIns then
  begin
    Campos  := Campos.Substring(2) + ') VALUES (';
    Valores := Valores.Substring(2) + ')';
    QrUpd.SQL.Add(Campos);
    QrUpd.SQL.Add(Valores);
  end;
  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;
  QrUpd.SQL.Add(';');
  Result := True;
  //
  //Geral.MB_Info(QrUpd.SQL.Text);
end;

function TUnSQLite_PF.ExecutaSQLQuery0(Form: TForm; Query: TFDQuery;
FD: TFDConnection; SQL: array of String): Boolean;
var
  I: Integer;
begin
  Result := False;
  try
    Form.Cursor := crHourGlass;
    Query.Close;
    Query.Connection := FD;
    if High(SQL) > -1 then
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no "ExecutaSQLQuery0"!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      //Geral.MB_Info(Query.SQL.Text);
      Query.ExecSQL;
      Result := True;
    end;
    Form.Cursor := crDefault;
  except
    on E: Exception do
    begin
      Geral.MB_Erro('Erro ao tentar executar uma SQL no "ExecutaSQLQuery0"!' +
      sLinebreak + E.Message + sLineBreak + Query.SQL.Text);
      //
      Form.Cursor := crDefault;
    end;
  end;
end;

procedure TUnSQLite_PF.MostraSQL(Query: TFDQuery; Memo: TMemo; Titulo: String);
var
  I: Integer;
begin
  Memo.Lines.Add('===== ' + Titulo + ' =====');
  for I := 0 to Query.SQL.Count -1 do
    Memo.Lines.Add(Query.SQL[I]);
  Memo.Lines.Add('=== FIM '+Titulo+' ===');
end;

function TUnSQLite_PF.SQLInsUpd(Form: TForm; DmkDBSrc: TDmkDBSrc; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean; SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant; UserDataAlterweb: Boolean; ComplUpd: String;
  InfoSQLOnError, InfoErro: Boolean): Boolean;
var
  QrUpd: TFDQuery;
begin
  QrUpd := TFDQuery.Create(Form);
  DmkFireDAC.DefineFDConnection(QrUpd, DmkDBSrc);
  try
    Result := CarregaSQLInsUpd(QrUpd, Tipo, lowercase(Tabela), Auto_increment,
    SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, False, ComplUpd);
    if Result then
    begin
      try
        //Geral.MB_Aviso(QrUpd.SQL.Text);
        QrUpd.ExecSQL;
        Result := True;
      except
        on E: Exception do
        begin
          if InfoSQLOnError then
            //Geral.MB_Erro(QrUpd.SQL.Text);
            MsgDmk.MostraTexto(E.Message + sLineBreak + QrUpd.SQL.Text);
          if InfoErro then
            raise;
        end;
      end;
    end else;
  finally
    QrUpd.Free;
  end;
end;

end.
