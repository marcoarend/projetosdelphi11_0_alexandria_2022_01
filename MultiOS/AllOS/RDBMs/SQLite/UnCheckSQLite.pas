unit UnCheckSQLite;

interface

uses
  System.SysUtils, System.Classes, System.UITypes,
  Generics.Collections,
  FMX.Memo, FMX.StdCtrls, FMX.Forms,
  FireDAC.Comp.Client,
  UnDmkEnums, UnMyLinguas;


type
  TUnCheckSQLite = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AdicionaFldNoNeed(MeFldsNoNeed: TMemo; Tabela, Campo: String);
    function  CampoAtivo(Form: TForm; DBMS: TdmkDBMSs; DBName: String;
              TabelaNome, TabelaBase, Campo: String;
              Memo: TMemo; QrNTV: TFDQuery): Integer;
    function  CampoExiste(Tabela, Campo: String; Memo: TMemo;
              QrNTV: TFDQuery): Integer;
    function  CriaIndice(Form: TForm; DBMS: TdmkDBMSs; DBName: String; Tabela, IdxNome,
              Aviso: String; Memo: TMemo): TResultVerify;
    function  CriaTabela(Form: TForm; DBMS: TdmkDBMSs; DBName: String; TabelaNome,
              TabelaBase: String;
              Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean;
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): Boolean;
    function  EfetuaVerificacoes(Form: TForm; DBMS: TdmkDBMSs; DBName: String;
              Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
              RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2, LaAviso1R,
              LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
              LaTempoF, LaTempoT: TLabel; MeTabsNoNeed: TMemo;
              PB: TProgressBar; MeFldsNoNeed: TMemo): Boolean;
    function  ExcluiIndice(Form: TForm; DBMS: TdmkDBMSs; DBName: String; Tabela, IdxNome,
              Aviso: String; Motivo: TMyMotivo; Memo: TMemo): TResultVerify;
    procedure GravaAviso(Aviso: String; Memo: TMemo; ToFile: Boolean = True);
    function  ImpedeVerificacao(): Boolean;
    function  IndiceExiste(Form: TForm; DBMS: TdmkDBMSs; DBName: String; Tabela, Indice:
              String; Memo: TMemo): Integer;
    function  TabelaAtiva(Tabela: String): Boolean;
    function  VerificaCampos(Form: TForm; DBMS: TdmkDBMSs; DBName: String;
              TabelaNome, TabelaBase: String;
              Memo: TMemo; RecriaRegObrig, Tem_Del: Boolean;
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel;
              MeFldsNoNeed: TMemo): TResultVerify;
    function  VerificaEstrutura(Form: TForm; DBMS: TdmkDBMSs; DBName: String; Memo: TMemo;
              RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G,
              LaTempoI, LaTempoF, LaTempoT: TLabel; MeTabsNoNeed: TMemo;
              PB: TProgressBar; MeFldsNoNeed: TMemo): Boolean;
    function  RecriaIndice(Form: TForm; DBMS: TdmkDBMSs; DBName: String; Tabela, IdxNome,
              Aviso: String; Memo: TMemo): TResultVerify;
    function  TabelaExiste(Tabela: String; QrTabs: TFDQuery): Boolean;
    procedure AdicionaDefault(var Opcoes: String; Tipo, Padrao: String);
  end;

var
  CheckSQLite: TUnCheckSQLite;
  //
  FPergunta  : Boolean;
  FLCampos   : TList<TCampos>;
  FLIndices  : TList<TIndices>;
  FRCampos   : TCampos;
  FRIndices  : TIndices;
  FCriarAtivoAlterWeb: Boolean;

implementation

uses UnMyObjects, UnMyListas, UnGeral, UnSQLite_PF, ModApp;

{ TUnCheckSQLite }

var
  FViuControle: Integer;
  FTabelas   : TList<TTabelas>;

resourceString
  ivAbortAlteUser     = 'Altera��o abortada pelo usu�rio.';
  ivAbortExclUser     = 'Exclus�o abortada pelo usu�rio.';
  ivAbortInclUser     = 'Inclus�o abortada pelo usu�rio.';
  ivAlterado          = 'Alterado'; //Feito
  ivCampo_Nome        = 'Tabela[%s].Campo[%s] ';
  ivCriado            = 'criado'; //Feito
  ivExclIndice        = 'N�o existe refer�cia no aplicativo %s '+
                        'ao �ndice %s da tabela %s.' + sLineBreak +
                        'Deseja excluir o �ndice?';
  ivIndice_Nome       = 'Tabela[%s].�ndice[%s] ';
  ivMsgNaoExiste      = 'N�o existe.';
  ivMsgERROAlterar    = 'ERRO ao alterar.';
  ivMsgERROCriar      = 'ERRO ao criar.';
  ivMsgERROExcluir    = 'ERRO ao excluir.';
  ivMsgEstruturaBD    = 'Analisando estrutura do Banco de Dados "%s"...';
  ivMsgExcluido       = 'exclu�do';
  ivMsgFimAnalBD      = 'T�rmino de an�lise da estrutura do Banco de Dados "%s".';
  ivTabela_Nome       = 'Tabela[%s] ';
  ivMsgDiferenca      = 'Diferen�a:';

  myco_               = '';
  mycoADD             = ' ADD ';
  //mycoAspasDuplas     = #39;//''; // N�o usa aspas! ???
  mycoALTERTABLE      = 'ALTER TABLE ';
  //mycoCHANGE          = ' CHANGE ';
  mycoEspaco          = ' ';
  mycoEspacos2        = '  ';
  mycoTypeDataType    = ' DataType ';
  mycoVirgula         = ',';

procedure TUnCheckSQLite.AdicionaDefault(var Opcoes: String; Tipo, Padrao: String);
begin
  if Lowercase(Tipo).IndexOf('int') > 0 then
    Opcoes := Opcoes + ' DEFAULT ' + Padrao
  else
    Opcoes := Opcoes + ' DEFAULT ' + #39 + Padrao + #39;
end;

procedure TUnCheckSQLite.AdicionaFldNoNeed(MeFldsNoNeed: TMemo; Tabela,
  Campo: String);
var
  Texto: String;
  I, N: Integer;
begin
  Texto := Tabela + '.' + Campo;
  N := -1;
  //
  for I := 0 to MeFldsNoNeed.Lines.Count - 1 do
  begin
    if MeFldsNoNeed.Lines[I] = Texto then
    begin
      N := I;
      Break;
    end;
  end;
  //
  if N = -1 then
    MeFldsNoNeed.Lines.Add(Texto);
end;

function TUnCheckSQLite.CampoAtivo(Form: TForm; DBMS: TdmkDBMSs; DBName, TabelaNome, TabelaBase,
  Campo: String; Memo: TMemo; QrNTV: TFDQuery): Integer;
  function NuloStrToInt(Txt: String): Integer;
  begin
    if Txt = 'YES' then
      Result := 0
    else
      Result := 1;
  end;
var
  i: Integer;
  Item: String;
  QrIdx, QrPII: TFDQuery;
  //Achou: Boolean;
begin
  Item := Format(ivCampo_Nome, [TabelaNome, Campo]) + mycoEspaco + ivMsgDiferenca;
  Result := 9;
  try
    for i := 0 to FLCampos.Count -1 do
    begin
      FRCampos := FLCampos.Items[i];
      if Uppercase(FRCampos.Field) = Uppercase(Campo) then
      begin
        Result := 0;
        if Uppercase(FRCampos.Tipo)  <>
           Uppercase(QrNTV.FieldByName('Type').AsString) then
        begin
          Result := 1;
          GravaAviso(Item + mycoTypeDataType, Memo);
        end;
        if NuloStrToInt(Uppercase(FRCampos.Null))  <>
           QrNTV.FieldByName('notnull').AsInteger then
        begin
          Result := 1;
          GravaAviso(Item+'"NULL"', Memo);
        end;
        if (Uppercase(FRCampos.Default) <>
           Uppercase(QrNTV.FieldByName('dflt_value').AsString)) then
        if (Uppercase(#39 + FRCampos.Default + #39) <>
           Uppercase(QrNTV.FieldByName('dflt_value').AsString)) then
        begin
          Result := 1;
          GravaAviso(Item + '"DEFAULT"' + sLineBreak +
          'Lista dmk: ' + FRCampos.Default + sLineBreak + 'Obtido do BD: ' +
          QrNTV.FieldByName('dflt_value').AsString, Memo);
        end;
(*
        if Uppercase(FRCampos.Extra)  <>
           Uppercase(QrNTV.FieldByName('Extra').AsString) then
        begin
          Result := 1;
          GravaAviso(Item+'"EXTRA"', Memo);
        end;
*)
        if (FRCampos.Key  <> '') and (
           QrNTV.FieldByName('pk').AsInteger = 0) then
        begin
          if (FRCampos.Key = 'UNI')
          or (FRCampos.Key = 'MUL') then
          begin
            // SQLite n�o informa
            // ver pelo �ndice:
            QrIdx := TFDQuery.Create(DmModApp);
            QrPII := TFDQuery.Create(DmModApp);
            try

              SQLite_PF.AbreSQLQuery0(Form, QrIdx, DmModApp.FDAppDB, [
              'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
              '']);
              QrIdx.First;
              while not QrIdx.Eof do
              begin
                SQLite_PF.AbreSQLQuery0(Form, QrPII, DmModApp.FDAppDB, [
                'PRAGMA INDEX_INFO(''' + QrIdx.FieldByName('name').AsString + ''')',
                '']);
                while not QrPII.Eof do
                begin
                  if LowerCase(QrPII.FieldByName('name').AsString) = Lowercase(Campo) then
                  begin
                    (*
                    Achou := True;
                    QrPII.Last;
                    *)
                    Exit;
                  end;
                  //
                  QrPII.Next;
                end;
                //
                QrIdx.Next;
              end;
            finally
              if QrIdx <> nil then
                QrIdx.Free;
              if QrPII <> nil then
                QrPII.Free;
            end;
          end;
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Result = 9 then Result := 3;
            GravaAviso(Item+'"KEY"', Memo);
          end;
        end;
      end;
      if Result < 9 then Exit;
    end;
  except
    raise;
  end;
end;

function TUnCheckSQLite.CampoExiste(Tabela, Campo: String; Memo: TMemo;
  QrNTV: TFDQuery): Integer;
var
  Item: String;
begin
  Item := Format(ivCampo_Nome, [Tabela, Campo]) + mycoEspaco + ivMsgNaoExiste;
  Result := 2;
  try
    QrNTV.First;
    while not QrNTV.Eof do
    begin
      if Uppercase(QrNTV.FieldByName('name').AsString) =
         Uppercase(FRCampos.Field) then Result := 0;
      if Result < 2 then Exit;
      QrNTV.Next;
    end;
    if Result = 2 then
      GravaAviso(Item, Memo);
  except
    raise;
  end;
end;

function TUnCheckSQLite.CriaIndice(Form: TForm; DBMS: TdmkDBMSs; DBName, Tabela, IdxNome,
  Aviso: String; Memo: TMemo): TResultVerify;
var
  I, J, K: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
  QrIts: TFDQuery;
  UNQ, IDX: String;
begin
  Result := rvOK;
  if not FPergunta then Resp := mrYes else
  Resp := Geral.MB_Pergunta('Confirma a cria��o do �ndice ' + IdxNome +
  ' da tabela ' + Tabela + '?');
  if Resp = mrYes then
  begin
    //
    Conta := TStringList.Create;
    Campos := myco_;
    for I := 0 to FLIndices.Count -1 do
    begin
      FRIndices := FLIndices.Items[I];
      if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
        Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
    end;
    K := Geral.IMV(Conta[Conta.Count-1])-1;
    if K = -1 then
      GravaAviso('FRIndices.Seq_in_index inv�lido (0)!' +
      'ERRO na cria��o do �ndice ' + IdxNome + ' na tabela '+ Tabela, Memo);
    for J := 0 to K do
    begin
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
        if StrToInt(Conta[J]) = FRIndices.Seq_in_index then
        begin
          if Campos <> myco_ then Campos := Campos + ',';
          Campos := Campos + FRIndices.Column_name;
        end;
      end;
    end;
{
    Dmod.QrSQL.Close;
    Dmod.QrSQL.Database := DataBase;
    Dmod.QrSQL.SQL.Clear;
    // Verificar se n�o haveria viola��o de �ndice!!!
    Dmod.QrSQL.Close;
    Dmod.QrSQL.SQL.Clear;
    Dmod.QrSQL.SQL.Add('SELECT COUNT(*) _ITENS_ FROM ' + lowercase(Tabela));
    Dmod.QrSQL.SQL.Add('GROUP BY ' + Campos);
    Dmod.QrSQL.SQL.Add('ORDER BY _ITENS_ DESC');
    Dmod.QrSQL.Open;
}
    QrIts := TFDQuery.Create(DmModApp);
    try
      try
        SQLite_PF.AbreSQLQuery0(Form, QrIts, DmModApp.FDAppDB, [
        'SELECT COUNT(*) _ITENS_ FROM Controle ',
        'GROUP BY Codigo ',
        'ORDER BY _ITENS_ DESC ',
        '']);

        if (QrIts.RecordCount > 0) and
        (QrIts.FieldByName('_ITENS_').AsFloat > 1) then
        begin
          GravaAviso('O �ndice "' + IdxNome + '" n�o foi inclu�do na tabela "' +
          Tabela + '" pois j� seria criado com viola��o!', Memo);
        end else begin
          QrIts.Close;
          QrIts.SQL.Clear;
          //
          if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
          or (Uppercase(IdxNome.Substring(1, 6)) = 'UNIQUE') then
            UNQ := ' UNIQUE '
          else
            UNQ := ' ';
          //
          if LowerCase(IdxNome) = 'primary' then
            IDX := 'idx_' + Tabela + '_primary'
          else
            IDX := IdxNome;
          //
          SQLite_PF.ExecutaSQLQuery0(Form, QrIts, DmModApp.FDAppDB, [
          'CREATE' + UNQ + 'INDEX IF NOT EXISTS ' + IDX,
          'ON ' + Lowercase(Tabela) + ' (' + Campos +  ')',
          '']);
          Result := rvOK;
          GravaAviso(Aviso + ivCriado, Memo);
        end;
      except;
        SQLite_PF.MostraSQL(QrIts, Memo,
          'ERRO na cria��o de �ndice na tabela ' + Tabela);
        Conta.Free;
        //Result := rvErr;
        GravaAviso(Aviso + ivMsgERROCriar, Memo);
        raise;
      end;
    finally
      QrIts.Free;
    end;
  end else begin
    GravaAviso(Aviso + ivAbortInclUser, Memo);
    if Resp = mrCancel then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TUnCheckSQLite.CriaTabela(Form: TForm; DBMS: TdmkDBMSs; DBName, TabelaNome,
  TabelaBase: String; Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean;
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): Boolean;
var
  i, j, k, u, Primarios: Integer;
  Item, Opcoes, Campos: String;
  Indices, Conta: TStringList;
  Texto: TStringList;
  TemControle: TTemControle;
  //
  QrNTI: TFDQuery;
begin
  Result := True;
  FLCampos  := TList<TCampos>.Create;
  FLIndices := TList<TIndices>.Create;
  Texto     := TStringList.Create;
  QrNTI     := TFDQuery.Create(DmModApp);
  try
    TemControle := [];
    MyListas.CriaListaCampos(TabelaBase, FLCampos, TemControle);
    //
    if (tctrlLok in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Lk';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlCad in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DataCad';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAlt in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DataAlt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlCad in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAlt in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlWeb in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'AlterWeb';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAti in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Ativo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if Tem_Del then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserDel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataDel';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MotvDel';
      FRCampos.Tipo       := 'int';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o info
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;

{
    New(FRCampos);
    FRCampos.Field      := mycoCampoAlterWeb;
    FRCampos.Tipo       := 'tinyint(1)';
    FRCampos.Null       := '';
    FRCampos.Key        := '';
    FRCampos.Default    := '1';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
    New(FRCampos);
    FRCampos.Field      := mycoCampoAtivo;
    FRCampos.Tipo       := 'tinyint(1)';
    FRCampos.Null       := '';
    FRCampos.Key        := '';
    FRCampos.Default    := '1';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
}
    MyListas.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
    try
      Texto.Add('CREATE TABLE ' + Lowercase(TabelaNome) + ' (');
      //
      Primarios := 0;
      for i:= 0 to FLCampos.Count - 1 do
      begin
        FRCampos := FLCampos[i];
        if FRCampos.Key = 'PRI' then
          Primarios := Primarios + 1;
      end;
      //
      Item := Format(ivTabela_Nome, [TabelaNome]);
      for i:= 0 to FLCampos.Count - 1 do
      begin
        Opcoes := myco_;
        FRCampos := FLCampos[i];
        Opcoes := FRCampos.Tipo;
        //if (FRCampos.Key = 'PRI') and (Primarios = 1) then
          //Opcoes := Opcoes + ' PRIMARY KEY ';
        if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) > 0 then
          Opcoes := Opcoes + ' AUTOINCREMENT ';
        // N�o � aqui! fica junto com o Tipo! Ex: "tinyint(3) unsigned"
        //if Uppercase(FRCampos.Extra).IndexOf(Uppercase('unsigned')) > 0 then
          //Opcoes := Opcoes + ' unsigned ';
        if FRCampos.Null <> 'YES' then
          Opcoes := Opcoes + ' NOT NULL ';
        if FRCampos.Default <> myco_ then
          AdicionaDefault(Opcoes, FRCampos.Tipo, FRCampos.Default);
        if FRCampos.Extra <> myco_ then
        begin
          //if Uppercase(FRCampos.Extra) = Uppercase('auto_increment')
          //then Opcoes := Opcoes + FRCampos.Extra;

        end;
        Opcoes := mycoEspacos2 + FRCampos.Field + mycoEspaco + Opcoes;
        if i < (FLCampos.Count-1) then Opcoes := Opcoes + mycoVirgula;
        Texto.Add(Opcoes);
        //Texto[Texto.Count] := Opcoes;
      end;
      Indices := TStringList.Create;
      Indices.Sorted := True;
      Indices.Duplicates := (dupIgnore);
      try
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          Indices.Add(FRIndices.Key_name);
        end;
        u := Texto.Count-1;
        if Indices.Count > 0 then Texto[u] := Texto[u] + mycoVirgula;
        for k := 0 to Indices.Count -1 do
        begin
          Conta := TStringList.Create;
          Campos := myco_;
          for i := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[i];
            if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
          end;
          for j := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
          begin
            for i := 0 to FLIndices.Count -1 do
            begin
              FRIndices := FLIndices.Items[i];
              if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
              begin
                if Campos <> myco_ then Campos := Campos + ',';
                Campos := Campos + FRIndices.Column_name;
              end;
            end;
          end;
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Uppercase(Indices[k]) = Uppercase('PRIMARY') then
            begin
              if (Primarios > 0) then
                Texto.Add('  CONSTRAINT INDICE_PRIMARIO PRIMARY KEY (' + Campos + ')')
            end
            else //if Uppercase(Indices[k].Substring(1, 6)) = Uppercase('UNIQUE') then
              Texto.Add('  CONSTRAINT '+ Indices[k] + ' UNIQUE ('+Campos+')')
            //else
              //Texto.Add('  INDEX '     + Indices[k] + ' ('+Campos+')');
            ;
            if k < (Indices.Count-1) then
            begin
             u := Texto.Count-1;
             Texto[u] := Texto[u] + mycoVirgula;
            end;
          end;
          Conta.Free;
        end;
      finally
        Indices.Free;
      end;
      Texto.Add(')');
      //Texto.Add('CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
      SQLite_PF.MostraSQL(QrNTI, Memo, '==== CRIAR TABELA ====');
      if Acao = actCreate then
      begin
        {
        for i := 0 to Texto.Count-1 do
          QrNTI.SQL.Add(Texto[i]);
        }
        GravaAviso(Item + sLineBreak + Texto.Text, Memo);
        SQLite_PF.ExecutaSQLQuery0(Form, QrNTI, DmModApp.FDAppDB, [
        Texto.Text,
        '']);
        {
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrNTI, '', Memo, False, False);
        QrNTI.ExecSQL;
        }
        GravaAviso(Item + ivCriado, Memo);
{       VER O QUE FAZER!!!
        VerificaRegistrosObrigatorios_Inclui(Database, TabelaNome, TabelaBase,
          Memo, False, True, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
        VerificaRegistrosDeArquivo(Database, TabelaNome, TabelaBase, Memo, False);
}
      end;
    except
      GravaAviso(Item+ivMsgERROCriar, Memo);
{
      on E: Exception do
        GravaAviso(Item + E.Message + sLineBreak + ivMsgERROCriar, Memo);
}
    end;
  finally
    FLCampos.Free;
    FLIndices.Free;
    Texto.Free;
    QrNTI.Free;
  end;
end;

function TUnCheckSQLite.EfetuaVerificacoes(Form: TForm; DBMS: TdmkDBMSs; DBName: String;
  Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
  RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R,
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
  LaTempoT: TLabel;  MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo): Boolean;
var
  //WinPath : array[0..144] of Char;
  //Name: String;
  //Cursor: TCursor;
  Qry: TFDQuery;
  Res: Integer;
begin
  Result := False;
  Qry := TFDQuery.Create(DmModApp);
  try
    MeTabsNoNeed.Lines.Clear;
  {
    LaTempoI.Caption := '...';
    LaTempoF.Caption := '...';
    LaTempoT.Caption := '...';
    Result := False;
  }
    if ImpedeVerificacao() then
    begin
      Result := False;
      Exit;
    end;

    Res := Geral.MB_Pergunta('Ser� realizada uma verifica��o na base de dados: ' +
    //sLineBrake + 'Servidor: ' + Database.Host +
    sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
    'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
    'perante restaura��o de backup.'(* + sLineBreak +
    'Para fazer um backup clique em "n�o" e feche esta janela.' +
    ' Na janela principal h� um bot�o de backup!' +
    sLineBreak + 'Confirma assim mesmo a verifica��o?'*));
    if Res <> mrYes then
      Exit;
    //Memo.Update;
    Application.ProcessMessages;
    Result := True;
    FPergunta := Pergunta;
    //Cursor := Form.Cursor;
    Form.Cursor := crHourGlass;
    try
      // verifica se a vers�o atual � mais nova
      // se n�o � impede a verifica��o
      if DBName <> '' then
      begin
        SQLite_PF.AbreSQLQuery0(Form, Qry, DmModApp.FDAppDB, [
        'SELECT name FROM sqlite_master ',
        'WHERE type=''table''',
        'AND name LIKE ''Controle''',
        'ORDER BY name;',
        '']);
  (*
        Qry.Close;
        Qry.Database := VAR_GOTOMySQLDBNAME;
        Qry.SQL.Clear;
        Qry.SQL.Add('SHOW TABLES FROM ' + VAR_GOTOMySQLDBNAME.DatabaseName);
        Qry.SQL.Add('LIKE "Controle"');
        Qry.Open;
        SQLite_PF.AbreSQLQuery0(DmModApp.QrUpd, DmModApp.SCMyDB, [
  'SELECT name FROM sqlite_master ',
  'WHERE type=''table''',
  'AND name LIKE ''Controle''',
  'ORDER BY name;',
        '']);
        Geral.MB_Aviso();
  *)
        if Qry.RecordCount > 0 then
        begin
{
          Qry.Close;
          Qry.Database := VAR_GOTOMySQLDBNAME;
          Qry.SQL.Clear;
          Qry.SQL.Add('SHOW COLUMNS FROM controle LIKE "versao"');
          Qry.Open;
}
          SQLite_PF.AbreSQLQuery0(Form, Qry, DmModApp.FDAppDB, [
          'PRAGMA table_info(controle);',
          '']);
          if Qry.RecordCount > 0 then
          begin
{
            Qry.Close;
            Qry.Database := VAR_GOTOMySQLDBNAME;
            Qry.SQL.Clear;
            Qry.SQL.Add('SELECT Versao FROM controle');
            Qry.Open;
}
            //
            SQLite_PF.AbreSQLQuery0(Form, Qry, DmModApp.FDAppDB, [
            'SELECT Versao FROM controle',
            '']);
            if CO_VERSAO < Qry.FieldByName('Versao').AsLargeInt then
            begin
              Geral.MB_Aviso('Verifica��o de banco de dados cancelada! '+
              sLineBreak + ' A vers�o deste arquivo � inferior a cadastrada ' +
              'no banco de dados.');
              Form.Cursor := crDefault;
              Exit;
            end;
          end;
        end;
      end else
        Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
      //
(*
      GetWindowsDirectory(WinPath,144);
      Name := 'C:\Dermatek';
      try
      if not DirectoryExists(Name) then ForceDirectories(Name);
      except
        Geral.MB_ERRO('N�o foi poss�vel criar o diret�rio:' + Name);
      end;
      Name := Name+'\LogFile.txt';
      if FileExists(Name) then DeleteFile(Name);
      AssignFile(FText, Name);
      ReWrite(FText);
      try
        MyList.VerificaOutrosAntes(DataBase, Memo);
        VerificaVariaveis(Memo);
        if Memo <> nil then Memo.Lines.Clear;
        Memo.Refresh;
        Memo.Update;
        // Primeiro VAR_MYARQDB para definir tabelas no final como VAR_GOTOMySQLDBNAME
        if VAR_MYARQDB <> nil then
          VerificaEstrutura(VAR_MYARQDB, Memo, RecriaRegObrig,
            LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
            LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, PB, ClFldsNoNeed)
        else if Memo <> nil then
          //Memo.Lines.Add('Informa��o: "VAR_MYARQDB" n�o definido.');
          MyObjects.Informa2(LaAvisoP1, LaAvisoP2, False, '"VAR_MYARQDB" n�o definido.');
        if Estrutura then
*)
          VerificaEstrutura(Form, DBMS, DBName, Memo, RecriaRegObrig,
            LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
            LaTempoI, LaTempoF, LaTempoT, MeTabsNoNeed, PB, MeFldsNoNeed);
(*
        else begin
          Qry.Close;
          Qry.Database := Database;//VAR_GOTOMySQLDBNAME;
          Dmod.QrSQL.Close;
          Dmod.QrSQL.Database := Database;//VAR_GOTOMySQLDBNAME;
          Dmod.QrIdx.Close;
          Dmod.QrIdx.Database := Database;//VAR_GOTOMySQLDBNAME;
        end;
        VerificaOutrosDepois(DataBase, Memo);
        Geral.WriteAppKey('Versao', Application.Title, CO_VERSAO, ktInteger,
          HKEY_LOCAL_MACHINE);
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE controle SET Versao =:P0');
        Dmod.QrUpd.Params[0].AsInteger := CO_VERSAO;
        Dmod.QrUpd.ExecSQL;
        //
        CloseFile(FText);
      except
        CloseFile(FText);
        raise;
      end;
  *)
      Form.Cursor := crDefault;
    except
      Form.Cursor := crDefault;
      raise;
    end;
    Geral.MB_Aviso('Verifica��o de banco de dados finalizada!');
  finally
    Qry.Free;
  end;
end;


function TUnCheckSQLite.ExcluiIndice(Form: TForm; DBName, Tabela, IdxNome,
  Aviso: String; Motivo: TMyMotivo; Memo: TMemo): TResultVerify;
var
  Resp: Integer;
  Txt: String;
  Qry: TFDQuery;
begin
  if True then
  begin
    case Motivo of
      mymotDifere: Txt := Format('O �ndice %s da Tabela %s difere do esperado e '+
      'deve ser excluido.', [IdxNome, Tabela]);
      mymotSemRef: Txt := Format('N�o h� refer�ncia ao �ndice %s da Tabela %s, '+
      'que deve ser excluido.', [IdxNome, Tabela]);
    end;
    if not FPergunta then
      Resp := mrNone
    else
      Resp := Geral.MB_Pergunta(Txt + sLineBreak + 'Confirma a exclus�o do �ndice ?');
    if Resp = mrYes then
    begin
      try
        Qry := TFDQuery.Create(DmModApp);
        try
          SQLite_PF.ExecutaSQLQuery0(Form, Qry, DmModApp.FDAppDB, [
          'DROP INDEX IF EXISTS ' + IdxNome + '''',
          '']);
          SQLite_PF.MostraSQL(Qry, Memo, '+++EXCLUIR INDICE+++');
        finally
          Qry.Free;
        end;
        //
        GravaAviso(Aviso + ivMsgExcluido, Memo);
      except;
        GravaAviso(Aviso + ivMsgERROExcluir, Memo);
        raise;
      end;
    end else begin
      GravaAviso(Aviso + ivAbortExclUser, Memo);
      if Resp = mrCancel then
      begin
        Result := rvAbort;
        Exit;
      end;
    end;
    Result := rvOK;
    GravaAviso('O �ndice "' + Tabela + '.' + IdxNome + ' deveria ser exclu�do!', Memo);
    //
  end else
    Result := rvOK;
end;

procedure TUnCheckSQLite.GravaAviso(Aviso: String; Memo: TMemo; ToFile: Boolean);
begin
  if Memo <> nil then
  begin
    Memo.Lines.Add(Aviso);
(*
    if ToFile then
      WriteLn(FText, Aviso);
*)
  end;
end;

function TUnCheckSQLite.VerificaCampos(Form: TForm; DBMS: TdmkDBMSs; DBName, TabelaNome,
  TabelaBase: String; Memo: TMemo; RecriaRegObrig, Tem_Del: Boolean; LaAviso1B,
  LaAviso2B, LaAviso1G, LaAviso2G: TLabel; MeFldsNoNeed: TMemo): TResultVerify;
var
  i, k, Resp, EhIdx: Integer;
  Opcoes, Item, Nome, IdxNome: String;
  Indices: TStringList;
  IdxExiste: Boolean;
  TemControle: TTemControle;
  //
  QrNTV, QrUpd, QrIdx: TFDQuery;
  SQL: String;
  //IDIdxTab: String;
  //ItmIdx: Integer;
begin
  Result := rvOK;
  QrNTV := TFDQuery.Create(DmModApp);
  QrUpd := TFDQuery.Create(DmModApp);
  QrIdx := TFDQuery.Create(DmModApp);
  try
(*
    QrNTV.Close;
    QrNTV.Database := Database;
    QrNTV.SQL.Clear;
    QrNTV.SQL.Add('SHOW FIELDS FROM ' + lowercase(TabelaNome));
    UMyMod.AbreQuery(QrNTV, Database, 'TMyCheckSQLite.VerificaCampos() > QrNTV');
*)
    SQLite_PF.AbreSQLQuery0(Form, QrNTV, DmModApp.FDAppDB, [
    'PRAGMA table_info(' + Lowercase(TabelaNome) + ');',
    '']);
    try
      FLCampos  := TList<TCampos>.Create;
      FLIndices := TList<TIndices>.Create;
      try
        FCriarAtivoAlterWeb := True;
        TemControle := [];
        MyListas.CriaListaCampos(TabelaBase, FLCampos, TemControle);
        //
        if (tctrlLok in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Lk';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataCad';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataAlt';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserCad';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserAlt';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlWeb in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'AlterWeb';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAti in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Ativo';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if Tem_Del then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'DataDel';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'MotvDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        MyListas.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
        while not QrNTV.Eof do
        begin
          Nome := QrNTV.FieldByName('name').AsString;
          Item := Format(ivCampo_Nome, [TabelaNome, Nome]);
          case CampoAtivo(Form, DBMS, DBName, TabelaNome, TabelaBase, Nome,
          Memo, QrNTV) of
            1:
            begin
              try
                Opcoes := FRCampos.Tipo;
                if FRCampos.Null <> 'YES' then
                  Opcoes := Opcoes + ' NOT NULL ';
{
                if FRCampos.Default <> myco_ then
                  Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                  FRCampos.Default + mycoAspasDuplas;
}
                if FRCampos.Default <> myco_ then
                  AdicionaDefault(Opcoes, FRCampos.Tipo, FRCampos.Default);
                //
                if FRCampos.Extra <> myco_ then
                begin
                  if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) > 0 then
                    Opcoes := Opcoes + ' auto_increment ';
                end;
{
                QrUpd.Close;
                QrUpd.DataBase := DataBase;
                QrUpd.SQL.Clear;
                QrUpd.SQL.Add(mycoALTERTABLE+TabelaNome+mycoCHANGE+
                  Nome+mycoEspaco+Nome+mycoEspaco+Opcoes);
                if ((not FPergunta) and (not MyList.ExcluiReg))
                then Resp := ID_YES else
}
                Geral.MB_Aviso('O campo ' + Nome +
                  ' da tabela ' + TabelaNome +
                  ' difere do esperado e o RDBM SQLite n�o permite regulariz�-lo.');
(*
////////////////////////////////////////////////////////////////////////////////
                N�o � poss�vel alterar campo no SQLite!
                if Resp = mrYes then
                begin
                  //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', Memo, False, False);
                  GravaAviso(SQL, Memo);
                  SQLite_PF.ExecutaSQLQuery0(Form, QrUpd, DmModApp.FDAppDB, [
                  SQL,
                  '']);
                  GravaAviso(Item + ivAlterado, Memo);
                end else begin
                  GravaAviso(Item + ivAbortAlteUser, Memo);
                  if Resp = mrCancel then
                  begin
                    Result := rvAbort;
                    Exit;
                  end;
                end;
////////////////////////////////////////////////////////////////////////////////
*)
              except
                GravaAviso(Item + ivMsgERROAlterar, Memo);
                raise;
              end;
            end;
            9:
            begin
              AdicionaFldNoNeed(MeFldsNoNeed, TabelaNome, Nome);
              GravaAviso('O campo "' + TabelaNome + '.' + Nome +
                '" deveria ser exclu�do!', Memo);
            end;
          end;
          QrNTV.Next;
        end;
        for i:= 0 to FLCampos.Count - 1 do
        begin
          FRCampos := FLCampos[i];
          if CampoExiste(TabelaNome, FRCampos.Field, Memo, QrNTV) = 2 then
          begin
            Item := Format(ivCampo_Nome, [TabelaNome, FRCampos.Field]);
            try
              Opcoes := FRCampos.Tipo;
              if FRCampos.Null <> 'YES' then
                Opcoes := Opcoes + ' NOT NULL ';
(*
              if FRCampos.Default <> myco_ then
                Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                FRCampos.Default + mycoAspasDuplas;
*)
              if FRCampos.Default <> myco_ then
                AdicionaDefault(Opcoes, FRCampos.Tipo, FRCampos.Default);
              //
{
              QrUpd.Close;
              QrUpd.DataBase := DataBase;
              QrUpd.SQL.Clear;
              QrUpd.SQL.Add(mycoALTERTABLE+TabelaNome+mycoADD+
              FRCampos.Field+mycoEspaco+Opcoes);
}
              SQL := mycoALTERTABLE + TabelaNome + mycoADD + FRCampos.Field +
                mycoEspaco + Opcoes;
              if not FPergunta then Resp := mrYes else
              Resp := Geral.MB_Pergunta('O campo ' + FRCampos.Field +
              ' n�o existe na tabela ' + TabelaNome + ' e ser� criado.' +
              sLineBreak + 'Confirma a cria��o do campo?');
              if Resp = mrYes then
              begin
                SQLite_PF.ExecutaSQLQuery0(Form, QrUpd, DmModApp.FDAppDB, [
                SQL,
                '']);
                GravaAviso(Item + ivcriado, Memo);
              end else begin
                GravaAviso(Item + ivAbortInclUser, Memo);
                if Resp = mrCancel then
                begin
                  Result := rvAbort;
                  GravaAviso(Item + ivAbortInclUser, Memo);
                  //
                  Exit;
                end;
              end;
            except
              GravaAviso(Item + ivMsgERROCriar, Memo);
              raise;
            end;
          end;
        end;
        Indices := TStringList.Create;
        Indices.Sorted := True;
        Indices.Duplicates := (dupIgnore);
        try
          for k := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[k];
            Indices.Add(FRIndices.Key_name);
          end;
          {
          IDIdxTab := '_' + LowerCase(TabelaNome) + '_';
          SQLite_PF.AbreSQLQuery0(Form, QrIdx, DmModApp.FDAppDB, [
          'SELECT name FROM sqlite_master ',
          'WHERE type=''index''',
          'AND Name LIKE ''%' + IDIdxTab + '%''',
          'ORDER BY name; ',
          '']);
          }
          SQLite_PF.ExecutaSQLQuery0(Form, QrIdx, DmModApp.FDAppDB, [
          'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
          '']);

(*
          Dmod.QrIdx.Close;
          Dmod.QrIdx.Database := DataBase;
          Dmod.QrIdx.SQL.Clear;
          Dmod.QrIdx.SQL.Add('SHOW KEYS FROM ' + LowerCase(TabelaNome));
          UMyMod.AbreQuery(Dmod.QrIdx, Database, 'TMyCheckSQLite.VerificaCampos()');
*)
          while not QrIdx.Eof do
          begin
            IdxNome := QrIdx.FieldByName('name').AsString;
            //ItmIdx := IdxNome.IndexOf(IDIdxTab);
            //if ItmIdx > - 1 then
            begin
              IdxExiste := False;
              Item := Format(ivIndice_Nome, [TabelaNome, IdxNome]);
              for k := 0 to Indices.Count -1 do
              begin
                if UpperCase(Indices[k]) = 'PRIMARY' then
                begin
                  if UpperCase(IdxNome) =
                    Uppercase('idx_' + TabelaNome + '_primary') then
                      IdxExiste := True
                  else
                  if UpperCase(IdxNome) =
                    Uppercase('sqlite_autoindex_' + TabelaNome + '_1') then
                      IdxExiste := True;
                end else
                if Indices[k] = IdxNome then
                  IdxExiste := True;
              end;
              if not IdxExiste then
              begin
                if not FPergunta then Resp := mrYes else
                Resp := Geral.MB_Pergunta(Format(ivExclIndice,
                  [Application.Title, IdxNome, TabelaNome]));
                if Resp = mrYes then
                begin
                  if ExcluiIndice(Form, DBMS, DBName, TabelaNome, IdxNome, Item,
                    mymotSemRef, Memo) <> rvOK then
                      Result := rvAbort;
                end else begin
                  GravaAviso(Item + ivAbortExclUser, Memo);
                  if Resp = mrCancel then
                  begin
                    Result := rvAbort;
                    Exit;
                  end;
                end;
              end;
            end;
            QrIdx.Next;
          end;
          QrIdx.Close;
          for k := 0 to Indices.Count -1 do
          begin
            //GravaAviso('1176.TableName: ' + TabelaNome, Memo);
            //GravaAviso('1177.Indices[k]: ' + Indices[k], Memo);
            EhIdx := IndiceExiste(Form, DBMS, DBName, TabelaNome, Indices[k], Memo);
            //GravaAviso('1179.EhIdx: ' + IntToStr(EhIdx), Memo);
            if EhIdx in ([2,3]) then
              if RecriaIndice(Form, DBMS, DBName,  TabelaNome, Indices[k], Item, Memo) <> rvOK
              then begin
                Result := rvAbort;
                Exit;
              end;
            if EhIdx in ([0]) then
            begin
              if CriaIndice(Form, DBMS, DBName,  TabelaNome, Indices[k], Item, Memo) <> rvOK then
              begin
                Result := rvAbort;
              end;
            end;
          end;
        finally
          Indices.Free;
        end;
        ////
(*       FAZER SO SE PRECISAR
        VerificaRegistrosObrigatorios_Inclui(Database, TabelaNome, TabelaBase,
          Memo, True, RecriaRegObrig, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
        VerificaRegistrosDeArquivo(Database, TabelaNome, TabelaBase, Memo, RecriaRegObrig);
*)
      finally
        FLCampos.Free;
        FLIndices.Free;
      end;
    finally
      QrNTV.Close;
    end;
  finally
    if QrNTV <> nil then
      QrNTV.Free;
    if QrUpd <> nil then
      QrUpd.Free;
    if QrIdx <> nil then
      QrIdx.Free;
  end;
end;

function TUnCheckSQLite.VerificaEstrutura(Form: TForm; DBMS: TdmkDBMSs; DBName: String; Memo: TMemo;
             RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B,
             LaAviso2B, LaAviso1G, LaAviso2G,
             LaTempoI, LaTempoF, LaTempoT: TLabel; MeTabsNoNeed: TMemo;
             PB: TProgressBar; MeFldsNoNeed: TMemo): Boolean;
var
  //p,
  //Resp,
  i: Integer;
  //TabA, TabB,
  TabNome, TabBase, Item: String;
  TempoI, TempoF, TempoT: TTime;
  DefTabela: TTabelas;
  Tem_Del: Boolean;
  //
  QrTabs: TFDQuery;
  Texto: String;
begin
  QrTabs := TFDQuery.Create(DmModApp);
  try
    Tem_Del := False;
    TempoI := Time();
    (*if DataBase = Dmod.MyDB then*) FViuControle := 0 (*else FViuControle := 4*);
    Result := True;
    MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgEstruturaBD, [DBName]));
    MyObjects.Informa(LaTempoI, False, FormatDateTime('hh:nn:ss:zzz', TempoI));
    try
      FTabelas := TList<TTabelas>.Create;
      MyListas.CriaListaTabelas(DBMS, FTabelas);
      try
        if LaAviso1R <> nil then
          Texto := LaAviso1R.Text
        else
          Texto := '';
        MyObjects.Informa2(LaAviso1R, LaAviso2R, True, Texto + '(' +
          Geral.FF0(FTabelas.Count) + ' tabelas)');

        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas j� criadas');

  (*      QrTabs.Close;
        QrTabs.Database := DataBase;
        QrTabs.SQL.Clear;
        QrTabs.SQL.Add('SHOW TABLES FROM '+DBName);
        QrTabs.Open;
  *)
        SQLite_PF.AbreSQLQuery0(Form, QrTabs, DmModApp.FDAppDB, [
        'SELECT name FROM sqlite_master ',
        'WHERE type=''table'' ',
        'ORDER BY name; ',
        '']);

        if PB <> nil then
        begin
          PB.Max := QrTabs.RecordCount;
          PB.Value := 0;
        end;
        QrTabs.First;
        while not QrTabs.Eof do
        begin
          if PB <> nil then
            PB.Value := PB.Value + 1;
          // Fazer Primeiro controle !!!
          if FViuControle = 0 then
          begin
            TabNome := 'controle';
            FViuControle := 1;
            try
              DmModApp.ReopenControle();
            except
              TabNome := QrTabs.Fields[0].AsString;
            end;
          end else
          if FViuControle = 2 then
          begin
            QrTabs.Prior;
            FViuControle := 3;
            TabNome := QrTabs.Fields[0].AsString;
          end else TabNome := QrTabs.Fields[0].AsString;


          TabBase := '';
          for i := 0 to FTabelas.Count -1 do
          begin
            DefTabela := FTabelas[i];
            if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
            begin
              TabBase := DefTabela.TabBase;
              Tem_Del := DefTabela.Tem_Del;
              Break;
            end;
          end;
          if TabBase = '' then
            TabBase := TabNome;

          MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas j� criadas');
          MyObjects.Informa2(LaAviso1G, LaAviso2G, True, 'Verificando tabela "' + TabNome + '"');

          if not TabelaAtiva(TabNome) then
          begin
            Item := Format(ivTabela_Nome, [TabNome]);
(*
            if MyListas.ExcluiTab then
            begin
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MensagemBox(Format(ivExclTabela,
              [Application.Title, TabNome]), PChar(ivTitAvis),
              MB_YESNOCANCEL+MB_ICONWARNING);
              if Resp = ID_YES then
              begin
                try
                  // Ver lct
*)
                  if Lowercase(TabNome.Substring(1, 4)) <> 'lct0' then
                    MeTabsNoNeed.Lines.Add(TabNome);
(*
                except
                  GravaAviso(Item+ivMsgERROExcluir, Memo);
                end;
              end else begin
                GravaAviso(Item+ivAbortExclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
                  Screen.Cursor := crDefault;
                  Exit;
                end;
              end;
            end;
*)
          end else begin
            Form.Cursor := crHourGlass;
            if VerificaCampos(Form, DBMS, DBName, TabNome, TabBase, Memo,
            RecriaRegObrig, Tem_Del, LaAviso1B, LaAviso2B, LaAviso1G,
            LaAviso2G, MeFldsNoNeed) <> rvOK then
            begin
              GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
              Form.Cursor := crDefault;
              Exit;
            end;
          end;
          QrTabs.Next;
        end;
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando tabelas a serem criadas');
        if PB <> nil then
        begin
          PB.Max := FTabelas.Count;
          PB.Value := 0;
        end;
        for i := 0 to FTabelas.Count -1 do
        begin
          MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"' + TabNome + '" verificando se existe');
          if PB <> nil then
            PB.Value := PB.Value + 1;
          //
          DefTabela := FTabelas[i];

          TabNome := DefTabela.TabCria;
          TabBase := DefTabela.TabBase;
          Tem_Del := DefTabela.Tem_Del;
          if TabBase = '' then
            TabBase := TabNome;

          if not TabelaExiste(TabNome, QrTabs) then
          begin
              Form.Cursor := crHourGlass;
              MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"' + TabNome + '" criando tabela ');
              CriaTabela(Form, DBMS, DBName, TabNome, TabBase, Memo, actCreate, Tem_Del,
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
            //end;
          end;

        end;
        MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '');
        QrTabs.Close;
      finally
        FTabelas.Free;
      end;
  (*
      if DataBase = Dmod.MyDB then
      begin
        // Na Web N�o D�!
        if TMeuDB <> VAR_DBWEB then
        begin
        end;
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando registros "Janelas"');
        VerificaRegistrosDeJanelas(DataBase, Memo, True, LaAviso1G, LaAviso2G, PB);
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando "Outros Depois"');
        MyList.VerificaOutrosDepois(DataBase, Memo);
      end;
  *)
      MyObjects.Informa2(LaAviso1G, LaAviso2G, False, '...');
      MyObjects.Informa2(LaAviso1B, LaAviso2B, False, '...');
      MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));
      TempoF := Time();
      MyObjects.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));
      TempoT := TempoF - TempoI;
      if TempoT < 0 then
        TempoT := TempoT + 1;
      MyObjects.Informa(LaTempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
    except
      MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));
      raise;
    end;
  finally
    QrTabs.Free;
  end;
end;

{ TUnCheckSQLite }

function TUnCheckSQLite.ImpedeVerificacao(): Boolean;
begin
(*  Usar?
  if CO_VERSAO mod 10000 = 9999 then
  begin
    VAR_VERIFI_DB_CANCEL := True;
    Result := True;
    Geral.MensagemBox('Esta � uma vers�o provis�ria.' + sLineBreak +
    'N�o ser� feita nenhuma verifica��o no banco de dados.' + sLineBreak +
    'Favor reportar qualquer tipo de erro que venha a ocorrer imediatamente ' +
    '� DERMATEK, para ser solucionado antes da vers�o est�vel.',
    'AVISO', MB_OK + MB_ICONWARNING);
    Exit;
  end else
*)
    Result := False;
end;

function TUnCheckSQLite.IndiceExiste(Form: TForm; DBMS: TdmkDBMSs; DBName, Tabela,
  Indice: String; Memo: TMemo): Integer;
var
  i: Integer;
  Need, Find, Have: Integer;
  Item: String;
  QrIts: TFDQuery;
  //
  MyIDXa0, MyIDXb1, MyIDXb2, MyIDXc1: String;
  isPRI, Continua: Boolean;
begin
  Item := Format(ivCampo_Nome, [Tabela, Indice]) + mycoEspaco + 'N�o encontrado:';
  Result := 0;
  Need := 0;
  Find := 0;
  Have := 0;
{
  QrIts.Close;
  QrIts.Database := DataBase;
  QrIts.SQL.Clear;
  QrIts.SQL.Add('SHOW KEYS FROM ' + Lowercase(Tabela));
  QrIts.Open;
}
  QrIts := TFDQuery.Create(DmModApp);
  try
    SQLite_PF.AbreSQLQuery0(Form, QrIts, DmModApp.FDAppDB, [
    'SELECT name FROM sqlite_master ',
    'WHERE type=''index''',
    'AND Name LIKE ''%_' + LowerCase(Tabela) + '_%''',
    'ORDER BY name; ',
    '']);
    QrIts.First;
    while not QrIts.Eof do
    begin
      //GravaAviso('1486.Indice: ' + QrIts.FieldByName('name').AsString, Memo);
      QrIts.Next;
    end;

    //GravaAviso('1490.Registros: ' + IntToStr(QrIts.recordCount), Memo);
    //GravaAviso('1491.Tabela: ' + Tabela, Memo);

    while not QrIts.Eof do
    begin
      //GravaAviso('1495.Indice: ' + Indice, Memo);
      //GravaAviso('1496.QrIts.FieldByName(name).AsString: ' + QrIts.FieldByName('name').AsString, Memo);
      if Uppercase(Indice) = 'PRIMARY' then
      begin
        if Uppercase(QrIts.FieldByName('name').AsString) = Uppercase('idx_' +
        Tabela + '_primary') then
          Have := Have + 1
        else
        if Uppercase(QrIts.FieldByName('name').AsString) =
          Uppercase('sqlite_autoindex_' + Tabela + '_1') then
            Have := Have + 1;
      end else
      if Uppercase(QrIts.FieldByName('name').AsString) =
      Uppercase(Indice) then
        Have := Have + 1;
      QrIts.Next;
    end;
    try
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(Indice) then
        begin
          QrIts.First;
          Need := Need + 1;
          while not QrIts.Eof do
          begin
            MyIDXa0 := Uppercase(QrIts.FieldByName('name').AsString);
            MyIDXb1 := 'idx_' + Tabela + '_primary';
            MyIDXb2 := 'sqlite_autoindex_' + Tabela + '_1';
            MyIDXc1 := FRIndices.Key_name;
            //
            //GravaAviso('1527.MyIDXa0: ' + MyIDXa0, Memo);
            //GravaAviso('1528.MyIDXb1: ' + MyIDXb1, Memo);
            //GravaAviso('1529.MyIDXb2: ' + MyIDXb2, Memo);
            //GravaAviso('1530.MyIDXc1: ' + MyIDXc1, Memo);
            isPRI := Uppercase(FRIndices.Key_name) = 'PRIMARY';

            Continua :=
            not (
              (
                isPRI
                and (
                  (MyIDXa0 = MyIDXb1)
                  or
                  (MyIDXa0 = MyIDXb2)
                )
              )
              or
              (MyIDXa0 = MyIDXc1)
            );
{
            if (Uppercase(FRIndices.Key_name) = 'PRIMARY') then
              MyIDX := 'idx_' + Tabela + '_primary'
            else
              MyIDX := FRIndices.Key_name;
            //
            if (Uppercase(MyIDX)  =
                Uppercase(QrIts.FieldByName('name').AsString)) then
}
            if Continua then
            begin
              if Result = 0 then
                Result := 1;
{             VER COMO FAZER!!!!
              if (FRIndices.Non_unique  =
                 QrIts.FieldByName('Non_unique').AsInteger)
              and (FRIndices.Seq_in_index  =
                 QrIts.FieldByName('Seq_in_index').AsInteger)
              and (Uppercase(FRIndices.Column_name)  =
                 Uppercase(QrIts.FieldByName('Column_name').AsString))
              then
}
                Find := Find + 1;
            end;
            QrIts.Next;
          end;
          if Find < Need then
          begin
            if Find > 0 then
            begin
              GravaAviso('A coluna ' + FRIndices.Column_name +
              ' n�o foi encontrada no �ndice ' + Indice + '.', Memo);
              Result := 2;
            end else GravaAviso('O �ndice ' + Indice + ' n�o existe.', Memo);
          end;
        end;
      end;
    except
      QrIts.Close;
      raise;
    end;
    if Have > Find then
    begin
      GravaAviso('H� excesso de colunas no �ndice ' + Indice + ' da tabela ' +
        Tabela +  '.', Memo);
      Result := 3;
    end;
  finally
    QrIts.Free;
  end;
end;

function TUnCheckSQLite.RecriaIndice(Form: TForm; DBMS: TdmkDBMSs; DBName, Tabela, IdxNome,
  Aviso: String; Memo: TMemo): TResultVerify;
var
  I, J: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
  Qry: TFDQuery;
  UNQ: String;
begin
  Result := rvOK;
  if not FPergunta then
    Resp := mrYes
  else
    Resp := Geral.MB_Pergunta('Confirma a alter��o do �ndice ' + IdxNome +
    ' da tabela ' + Tabela + '?)');
  //
  if Resp = mrYes then
  begin
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      for J := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
      begin
        for I := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[I];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[J]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      Qry := TFDQuery.Create(DmModApp);
      try
        SQLite_PF.ExecutaSQLQuery0(Form, Qry, DmModApp.FDAppDB, [
        'DROP INDEX IF EXISTS ' + IdxNome + '''',
        '']);
        //
        if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
        or (Uppercase(IdxNome.Substring(1, 6)) = 'UNIQUE') then
          UNQ := ' UNIQUE '
        else
          UNQ := ' ';
        //
        SQLite_PF.ExecutaSQLQuery0(Form, Qry, DmModApp.FDAppDB, [
        'CREATE' + UNQ + 'INDEX IF NOT EXISTS ' + LowerCase(IdxNome),
        'ON ' + Lowercase(Tabela) + ' (' + Campos +  ')',
        '']);
        SQLite_PF.MostraSQL(Qry, Memo, 'CRIAR INDICE');
      finally
        Qry.Free;
      end;
      //
      Result := rvOK;
      GravaAviso(Aviso + ivAlterado, Memo);
    except;
      Conta.Free;
      GravaAviso(Aviso + ivMsgERROAlterar, Memo);
      raise;
    end;
  end else begin
    GravaAviso(Aviso + ivAbortAlteUser, Memo);
    if Resp = mrCancel then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TUnCheckSQLite.TabelaAtiva(Tabela: String): Boolean;
var
  I: Integer;
  DefTabela: TTabelas;
begin
  Result := False;
  try
    for I := 0 to FTabelas.Count -1 do
    begin
      DefTabela := FTabelas[I];
      if Uppercase(DefTabela.TabCria) = Uppercase(Tabela) then
      begin
        Result := True;
        Exit;
      end;
    end;
  except
    raise;
  end;
end;

function TUnCheckSQLite.TabelaExiste(Tabela: String; QrTabs: TFDQuery): Boolean;
var
  Tab: String;
  P: Integer;
begin
  Result := False;
  try
    QrTabs.First;
    P := Tabela.IndexOf('.');
    if P > 0 then
      Tab := Tabela.Substring(P + 1)
    else
      Tab := Tabela;
    while not QrTabs.Eof do
    begin
      if Uppercase(Tab) = Uppercase(QrTabs.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      QrTabs.Next;
    end;
  except
    raise;
  end;
end;

end.


