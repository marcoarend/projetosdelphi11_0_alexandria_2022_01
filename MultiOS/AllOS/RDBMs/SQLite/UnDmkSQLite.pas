unit UnDmkSQLite;

interface

uses System.UITypes, System.SysUtils,
  {$IF DEFINED(POSIX)}
  Posix.Unistd,
  {$ENDIF}
  FMX.Forms,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.Stan.Error, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Comp.Client, FireDAC.Comp.DataSet, FireDAC.Comp.UI,
  FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, FireDAC.Phys.SQLite,
  //
  UnDmkEnums, UnGrl_Vars, UnGeral, UnSQLite_PF, UnDmkFireDAC;

type
  TUnDmkSQLite = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  RecriaDatabase(): Boolean;
    function  SQLDelSimple(Form: TForm; DmkDBSrc: TDmkDBSrc; Tabela: String;
              Campos: array of String; Valores: array of Variant;
              SQLExtra: String): Boolean;
{
              Movidos para UnDmkFireDAC
    function  DefineFDConnection(Qry: TFDQuery; DmkDBSrc: TDmkDBSrc): Boolean;
    function  ObtemFDConnection(DmkDBSrc: TDmkDBSrc): TFDConnection;
}
    function  SQLInsertValues(Form: TForm;
              DmkDBSrc: TDmkDBSrc; Tabela, Campos, Valores: String): Boolean;
  end;


var
  DmkSQLite: TUnDmkSQLite;

implementation

{ TUnDmkSQLite }

{
   Movido para DmkFireDAC.ObtemFDConnection

function TUnDmkSQLite.DefineFDConnection(Qry: TFDQuery; DmkDBSrc: TDmkDBSrc): Boolean;
begin
  //Result := False;
  case DmkDBSrc of
    dbsrcLocalServer : Qry.Connection := VAR_SQLITE_DB_LOCAL_SERVER;
    dbsrcLocalUser   : Qry.Connection := VAR_SQLITE_DB_LOCAL_USER;
    dbsrcWebServer   : Qry.Connection := VAR_SQLITE_DB_WEB_SERVER;
    dbsrcWebUser     : Qry.Connection := VAR_SQLITE_DB_WEB_USER;
    else
    begin
      Qry.Connection := nil;
      Geral.MB_Erro('"dbsrc..." n�o definido!');
    end;
  end;
  Result := Qry.Connection <> nil;
end;

function TUnDmkSQLite.ObtemFDConnection(DmkDBSrc: TDmkDBSrc): TFDConnection;
begin
  //Result := False;
  case DmkDBSrc of
    dbsrcLocalServer : Result := VAR_SQLITE_DB_LOCAL_SERVER;
    dbsrcLocalUser   : Result := VAR_SQLITE_DB_LOCAL_USER;
    dbsrcWebServer   : Result := VAR_SQLITE_DB_WEB_SERVER;
    dbsrcWebUser     : Result := VAR_SQLITE_DB_WEB_USER;
    else
    begin
      Result := nil;
      Geral.MB_Erro('"dbsrc..." n�o definido!');
    end;
  end;
end;
}

function TUnDmkSQLite.RecriaDatabase: Boolean;
var
  Exclui: Integer;
begin
  Result := False;
  Exclui := Geral.MB_Pergunta(
  'Ao recriar a base de dados todos os dados ser�o perdidos!' + sLineBreak +
  'Deseja continuar assim mesmo?');
  if Exclui <> mrYes then
    Exit
  else
  begin
    if FileExists(VAR_APP_DB_FULLPATH) then
    begin
      if VAR_SQLITE_DB_LOCAL_SERVER <> nil then
      begin
        VAR_SQLITE_DB_LOCAL_SERVER.Connected := False;
        DeleteFile(VAR_APP_DB_FULLPATH);
        if FileExists(VAR_APP_DB_FULLPATH) then
          Geral.MB_Erro('N�o foi poss�vel excluir o arquivo:' + sLineBreak +
          VAR_APP_DB_FULLPATH)
        else
          VAR_SQLITE_DB_LOCAL_SERVER.Connected := True;
        //
        Result := True;
      end;
    end else
      Geral.MB_Erro('"VAR_SQLITE_DB_LOCAL_SERVER" n�o definido!');
  end;
end;

function TUnDmkSQLite.SQLDelSimple(Form: TForm; DmkDBSrc: TDmkDBSrc;
Tabela: String; Campos: array of String; Valores: array of Variant;
SQLExtra: String): Boolean;
var
  Qry: TFDQuery;
  I, J: Integer;
  Liga, SQL, Valor: String;
begin
  Result := False;
  Qry := TFDQuery.Create(Form);
  try
    DmkFireDAC.DefineFDConnection(Qry, DmkDBSrc);
    I := High(Campos);
    J := High(Valores);
    if I <> J then
    begin
      Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
      IntToStr(j+1) +
      ' valores para estes campos em "UnDmkSQLite.SQLDelSimple"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
      Exit;
    end;
    Liga := 'WHERE ';
    SQL := 'DELETE FROM ' + LowerCase(Tabela) + sLineBreak;
    for I := System.Low(Campos) to High(Campos) do
    begin
      Valor := Geral.VariavelToString(Valores[i]);
      SQL := SQL + Liga + Campos[I] + '=' + Valor + sLineBreak;
      if Liga <> 'AND ' then
        Liga := 'AND ';
    end;
    if SQLExtra <> '' then
      SQL := SQL + sLineBreak + SQLExtra;
    Result := SQLite_PF.ExecutaSQLQuery0(Form, Qry,
      TFDConnection(Qry.Connection), [SQL]);
  finally
    Qry.Free;
  end;
end;

function TUnDmkSQLite.SQLInsertValues(Form: TForm; DmkDBSrc: TDmkDBSrc; Tabela,
  Campos, Valores: String): Boolean;
var
  SQLIns: String;
  FDConnection: TFDConnection;
begin
  FDConnection := nil;
  Result := False;
  SQLIns := 'INSERT INTO ' + LowerCase(Tabela) + ' (' +
    Campos + ') Values (' + Valores + ');';
  case DmkDBSrc of
    dbsrcLocalServer : FDConnection := VAR_SQLITE_DB_LOCAL_SERVER;
    dbsrcLocalUser   : FDConnection := VAR_SQLITE_DB_LOCAL_USER;
    dbsrcWebServer   : FDConnection := VAR_SQLITE_DB_WEB_SERVER;
    dbsrcWebUser     : FDConnection := VAR_SQLITE_DB_WEB_USER;
    else Geral.MB_Erro('"dbsrc..." n�o definido!');
  end;
  if FDConnection <> nil then
  begin
    try
      FDConnection.ExecSQL(SQLIns);
      Result := True;
    except
      on E: Exception do
        Geral.MB_Erro(
        'Erro ao tentar executar uma SQL "UnDmkSQLite.SQLInsertValues"!' + sLineBreak +
        FDConnection.Name +  sLineBreak + e.Message +
        sLineBreak + SQLIns);
    end;
  end else
    Geral.MB_Erro('N�o foi poss�vel executar a SQL inclus�o:' + sLineBreak +
    SQLIns);
end;

end.

