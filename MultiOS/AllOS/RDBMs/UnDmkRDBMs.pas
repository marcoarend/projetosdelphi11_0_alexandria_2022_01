unit UnDmkRDBMs;

interface

uses System.SysUtils, System.Classes,
  FMX.Memo, FMX.StdCtrls, FMX.Forms,
  FireDAC.Comp.Client,
  Data.DB,
  UnDmkEnums, UnGrl_Consts, UnGrl_Vars, UnGeral, UnApp_Consts, UnDmkPF,
  UnMyObjects,
  dmkCNEdit, (*dmkCNClearingEdit,*) dmkCNPanel;

type
  TUnDmkRDBMs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
              SQL: array of String): Boolean;
    function  ConectaRDBM_Main(FDConnection: TFDConnection): Boolean;
    function  GeraSQLInsUpd(var SQL: String; Form: TForm; DmkDBMSs: TDmkDBMSs;
              SQLType: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  EfetuaVerificacoes(Form: TForm; DBMS: TdmkDBMSs; DBName: String;
              Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
              RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2, LaAviso1R,
              LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
              LaTempoF, LaTempoT: TLabel; MeTabsNoNeed: TMemo;
              PB: TProgressBar; MeFldsNoNeed: TMemo): Boolean;
    function  RecriaDatabase(): Boolean;
    function  SQLDelSimple(Form: TForm; DmkDBSrc: TDmkDBSrc; Tabela: String;
              Campos: array of String; Valores: array of Variant;
              SQLExtra: String): Boolean;
    function  SQLInsertValues(Form: TForm; DmkDBMSs: TDmkDBMSs;
              DmkDBSrc: TDmkDBSrc; Tabela, Campos, Valores: String): Boolean;
    function  SQLInsUpd(Form: TForm; DmkDBSrc: TDmkDBSrc;
              SQLType: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  ObtemCodigoDeCodUsu(EdCodUsu: TdmkCNEdit; var Codigo: Integer;
              Aviso: String; Campo: String = 'Codigo'; CodUsu: String =
              'CodUsu'): Boolean;
    function  BPGS1I32(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
    function  BPGS1I32_Reaproveita(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
    function  BPGS1I64(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
    function  BPGS1I64_Reaproveita(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
              DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
    function  ReopenDataSet(Form: TForm; Tabela: TDataSet): Boolean;
    //
    function  SQLExec(Form: TForm; FDCon: TFDConnection; SQL: String): Integer;
    function  GetQrVal1Str(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String; var Valor: String): Integer;
    function  GetQrCount(const Form: TForm; const FDCon: TFDConnection;
              const SQL: String): Integer;
    function  GetQrVal1Int(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String; var Valor: Integer): Integer;
    function  GetQrVals(const Form: TForm; const FDCon: TFDConnection;
              const SQL: array of String; const Campos: array of String;
              var Valores: array of Variant): Boolean;
    function  LoadDBParams(): Boolean;
    function  DefineDatabase(var MeuDB: String; const ExeName: String): Boolean;
    function  DefineVarsIniDB(): Boolean;
    // Realmente precisa?
    //function  StrToSpidAct(Nome: String): TDmkSpidAct;
  end;


var
  DmkRDBMs: TUnDmkRDBMs;

implementation

uses UnDmkSQLite, UnCheckSQLite, UnSQLite_PF, UnMySQL_PF;

{ TUnDmkRDBMs }

resourcestring
  sRDBM_Nao_Definido = 'RDBM n�o definido no procedimento "UnDmkRDBMs.%s"!';

function TUnDmkRDBMs.AbreSQLQuery0(Form: TForm; Query: TFDQuery; FD: TFDConnection;
  SQL: array of String): Boolean;

begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.AbreSQLQuery0(Form, Query, FD, SQL);
    dbmsSQLite: Result := SQLite_PF.AbreSQLQuery0(Form, Query, FD, SQL);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['AbreSQLQuery0']));
  end;
end;

function TUnDmkRDBMs.BPGS1I32(Form: TForm; DmkDBSrc: TDmkDBSrc; Tabela, Campo,
  _WHERE, _AND: String; TipoSinal: TTipoSinal; SQLType: TSQLType;
  DefUpd: Integer): Integer;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.BPGS1I32(Form,
                DmkDBSrc, Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType,
                DefUpd);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['BPGS1I32']));
  end;
end;

function TUnDmkRDBMs.BPGS1I32_Reaproveita(Form: TForm; DmkDBSrc: TDmkDBSrc;
  Tabela, Campo, _WHERE, _AND: String; TipoSinal: TTipoSinal; SQLType: TSQLType;
  DefUpd: Integer): Integer;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.BPGS1I32_Reaproveita(Form,
                DmkDBSrc, Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType,
                DefUpd);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['BPGS1I32_Reaproveita']));
  end;
end;

function TUnDmkRDBMs.BPGS1I64(Form: TForm; (*DmkDBMSs: TDmkDBMSs;*)
  DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsSQLite: Result := SQLite_PF.BPGS1I64(Form,
                DmkDBSrc, Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType,
                DefUpd);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['BPGS1I64']));
  end;
end;

function TUnDmkRDBMs.BPGS1I64_Reaproveita(Form: TForm;
  DmkDBSrc: TDmkDBSrc; Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64): Int64;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsSQLite: Result := SQLite_PF.BPGS1I64_Reaproveita(Form,
                DmkDBSrc, Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType,
                DefUpd);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['BPGS1I64_Reaproveita']));
  end;
end;

function TUnDmkRDBMs.ConectaRDBM_Main(FDConnection: TFDConnection): Boolean;
begin
  if FDConnection.Connected then
  begin
    Grl_Vars.AddTODO('UnDmkRDBMS.160 Mensagem Popup ????');
    Geral.MB_Erro(FDConnection.Name + ' j� est� conectado!!');
    FDConnection.Connected := False;
  end;
  if CO_DB_APP_MAIN_DB = dbmsMySQL then
  begin
    Grl_Vars.AddTODO('Implementar definicoes de variaveis de conexao');
    FDConnection.Params.Text :=
    //'DriverID=MySQL
    'RDBMS=MYSQL' + sLineBreak +
    'DriverID=' + VAR_RDBMS_DriverID + sLineBreak +
    'ODBCDriver=' + VAR_RDBMS_ODBCDriver + sLineBreak +
    'Database=' + TMeuDB + sLineBreak +
    'Server=' + VAR_RDBMS_SERVER + sLineBreak +
    'User_Name=root' + sLineBreak +
    'Password=' + VAR_RDBMS_PWD + sLineBreak +
    'Port=' + VAR_RDBMS_PORT + sLineBreak +
    ////////////////////////////////////////////////////////////////////////////
    ///  Outros iportantes:
    //Compress=False
    'LoginTimeout=10' + sLineBreak +
    'TinyIntFormat=Integer' + sLineBreak +
    '';
    //
    try
      FDConnection.Connected := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('N�o foi poss�vel conectar no banco de dados principal!' +
        sLineBreak + E.Message);
        Application.Terminate;
      end;
    end;
  end;
end;

{
function TUnDmkRDBMs.DefineDatabase(var MeuDB: String; const ExeName: String): Boolean;
var
  U, I, Conta: Integer;
  BDs: array of String;
begin
  Result := False;
  Conta := DmkPF.ReadAppKeyCU(Application.Title, ['Databases'], 'Count', ktInteger, 0);
  Conta := Conta + 1;
  SetLength(BDs, Conta);
  BDs[0] := CO_RDBMS_DEF_DB;
  if Conta > 1 then
  begin
    for I := 1 to Conta -1 do
      BDs[I] := DmkPF.ReadAppKeyCU(Application.Title, ['Databases'],
      FormatFloat('0', I), ktString, MeuDB);
    U := DMkPF.ReadAppKeyCU(Application.Title, [ExeName], 'Last', ktInteger, -1);
    I := MyObjects.SelListView('Sele��o da Base de Dados', BDs, U);
    if I > -1 then
    begin
      MeuDB := BDs[I];
      DmkPF.WriteAppKeyCU(Application.Title, [ExeName], 'Last', I, ktInteger);
    end;
      //
    Result := MeuDB <> '';
    if not Result then
      Halt(0);
  end;
end;
}

function TUnDmkRDBMs.DefineDatabase(var MeuDB: String; const ExeName: String): Boolean;
  function DBsDAIts_Abre(): Boolean;
  var
    Lista: TStrings;
    I: Integer;
    Codigo, Controle: Integer;
    Nome: String;
    Reg: TRegistry;
    Caminho, CamFlds: String;
  begin
    Result := False;
    //
    Reg := TRegistry.Create(KEY_READ);
    Reg.RootKey := HKEY_CURRENT_USER;
    try
      Caminho := CaminhoTabela('DBsDAIts');
      Reg.OpenKeyReadOnly('Software\' + Caminho);
      Lista := TStringList.Create;
      try
        Reg.GetValueNames(Lista);
        for I := 0 to Lista.Count - 1 do
        begin
          Controle := Geral.IMV(Lista[I]);
          Nome := Reg.ReadString(Lista[I]);
          //
          //Codigo := Geral.ReadAppKeyCU()
          CamFlds := CaminhoRegistro(Caminho, Controle);
          Codigo  := Geral.ReadAppKeyCU('Codigo', CamFlds, ktInteger, 0);
          //
          DBsDAIts_CarregaRegistro(Codigo, Controle, Nome);
        end;
      finally
        Lista.Free;
      end;
    finally
      Reg.Free;
    end;
  end;

var
  U, I, Conta: Integer;
  BDs: array of String;
begin
  Result := False;
  Conta := DmkPF.ReadAppKeyCU(Application.Title, ['Databases'], 'Count', ktInteger, 0);
  Conta := Conta + 1;
  SetLength(BDs, Conta);
  BDs[0] := CO_RDBMS_DEF_DB;
  if Conta > 1 then
  begin
    for I := 1 to Conta -1 do
      BDs[I] := DmkPF.ReadAppKeyCU(Application.Title, ['Databases'],
      FormatFloat('0', I), ktString, MeuDB);
    U := DMkPF.ReadAppKeyCU(Application.Title, [ExeName], 'Last', ktInteger, -1);
    I := MyObjects.SelListView('Sele��o da Base de Dados', BDs, U);
    if I > -1 then
    begin
      MeuDB := BDs[I];
      DmkPF.WriteAppKeyCU(Application.Title, [ExeName], 'Last', I, ktInteger);
    end;
      //
    Result := MeuDB <> '';
    if not Result then
      Halt(0);
  end;
end;

function TUnDmkRDBMs.DefineVarsIniDB(): Boolean;
begin
  Result := False;
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL:
      Result := MySQL_PF.DefineVarsIniDB();
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['DefineVarsIniDB']));
  end;
end;

function TUnDmkRDBMs.GeraSQLInsUpd(var SQL: String; Form: TForm; DmkDBMSs: TDmkDBMSs;
  SQLType: TSQLType; Tabela: String; Auto_increment: Boolean; SQLCampos,
  SQLIndex: array of String; ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; ComplUpd: String; InfoSQLOnError,
  InfoErro: Boolean): Boolean;
const
  IGNORE = False;
begin
  Result := False;
  case DmkDBMSs of
    dbmsMySQL:
      Result := MySQL_PF.CarregaSQLInsUpd(SQL, SQLType, Tabela, Auto_increment,
      SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, IGNORE,
      ComplUpd)
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['GeraSQLInsUpd']));
  end;
end;

function TUnDmkRDBMs.GetQrCount(const Form: TForm; const FDCon: TFDConnection;
  const SQL: String): Integer;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.GetQrCount(Form, FDCon, SQL);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['GetQrVal1Str']));
  end;
end;

function TUnDmkRDBMs.GetQrVal1Int(const Form: TForm; const FDCon: TFDConnection;
  const SQL: array of String; var Valor: Integer): Integer;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.GetQrVal1Int(Form, FDCon, SQL, Valor);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['GetQrVal1Str']));
  end;
end;

function TUnDmkRDBMs.GetQrVal1Str(const Form: TForm; const FDCon: TFDConnection;
  const SQL: array of String; var Valor: String): Integer;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.GetQrVal1Str(Form, FDCon, SQL, Valor);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['GetQrVal1Str']));
  end;
end;

function TUnDmkRDBMs.GetQrVals(const Form: TForm; const FDCon: TFDConnection;
  const SQL: array of String; const Campos: array of String; var Valores:
  array of Variant): Boolean;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.GetQrVals(Form, FDCon, SQL, Campos, Valores);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['GetQrVal1Str']));
  end;
end;

function TUnDmkRDBMs.LoadDBParams(): Boolean;
begin
  Result := False;
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result := MySQL_PF.LoadDBParams();
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['EfetuaVerificacoes']));
  end;
end;

function TUnDmkRDBMs.EfetuaVerificacoes(Form: TForm; DBMS: TdmkDBMSs;
  DBName: String; Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
  RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R,
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
  LaTempoT: TLabel; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo): Boolean;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsSQLite:
    Result := CheckSQLite.EfetuaVerificacoes(Form, DBMS, DBName, Memo,
      Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig, LaAvisoP1,
      LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G,
      LaAviso2G, LaTempoI, LaTempoF, LaTempoT, MeTabsNoNeed, PB, MeFldsNoNeed);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['EfetuaVerificacoes']));
  end;
end;

function TUnDmkRDBMs.ObtemCodigoDeCodUsu(EdCodUsu: TdmkCNEdit;
  var Codigo: Integer; Aviso, Campo, CodUsu: String): Boolean;
var
  Query: TDataSet;
  //DB: TdmkCNClearingEdit;
  Pn: TdmkCNPanel;
  CU: Integer;
begin
  Codigo := 0;
  Result := False;
  if EdCodUsu.Text <> '' then
  begin
    Pn := TdmkCNPanel(EdCodUsu.CNPanel);
    if Pn <> nil then
    begin
      Query := TDataSet(Pn.CNDataSet);
      if Query <> nil then
      begin
        CU := Geral.IMV(EdCodUsu.Text);
        if Query.Locate(CodUsu, CU, []) then
        begin
          Codigo := Query.FieldByName(Campo).AsInteger;
          Result := True;
        end;
      end;
    end;
  end;
  if not Result and (Aviso <> '') then
    Geral.MB_Aviso(Aviso);
end;

function TUnDmkRDBMs.RecriaDatabase(): Boolean;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsSQLite: Result := DmkSQLite.RecriaDatabase();
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['RecriaDatabase']));
  end;
end;

function TUnDmkRDBMs.ReopenDataset(Form: TForm; Tabela: TDataSet): Boolean;
var
  NomeOwner: String;
begin
  Result := False;
  try
    Tabela.Close;
    Tabela.Open;
    Result := Tabela.State <> dsInactive;
  except
    on E: Exception do
    begin
      if Tabela.Owner <> nil then
        NomeOwner := TComponent(Tabela.Owner).Name + '.'
      else
        NomeOwner := '';
      //
      Geral.MB_Erro(
      'Erro ao abrir a tabela "' + NomeOwner + Tabela.Name + '"' +
      sLineBreak + E.Message);
    end;
  end;
end;

function TUnDmkRDBMs.SQLDelSimple(Form: TForm; DmkDBSrc: TDmkDBSrc;
Tabela: String; Campos: array of String; Valores: array of Variant;
SQLExtra: String): Boolean;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsSQLite: Result :=
      DmkSQLite.SQLDelSimple(Form, DmkDBSrc, Tabela, Campos, Valores, SQLExtra);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['SQLDelSimple']));
  end;
end;

function TUnDmkRDBMs.SQLExec(Form: TForm; FDCon: TFDConnection; SQL: String): Integer;
begin
  Result := FDCon.ExecSQL(SQL);
end;

function TUnDmkRDBMs.SQLInsertValues(Form: TForm; DmkDBMSs: TDmkDBMSs;
DmkDBSrc: TDmkDBSrc; Tabela, Campos, Valores: String): Boolean;
{
var
  SQLIns: String;
}
begin
  case CO_DB_APP_MAIN_DB of
    dbmsSQLite: Result :=
      DmkSQLite.SQLInsertValues(Form, DmkDBSrc, Tabela, Campos, Valores);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['SQLDelSimple']));
  end;
end;

function TUnDmkRDBMs.SQLInsUpd(Form: TForm; DmkDBSrc: TDmkDBSrc; SQLType: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb: Boolean;
  ComplUpd: String; InfoSQLOnError, InfoErro: Boolean): Boolean;
begin
  case CO_DB_APP_MAIN_DB of
    dbmsMySQL: Result :=
      MySQL_PF.SQLInsUpd(Form, DmkDBSrc, SQLType, Tabela, Auto_increment,
      SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, ComplUpd,
      InfoSQLOnError, InfoErro);
    dbmsSQLite: Result :=
      SQLite_PF.SQLInsUpd(Form, DmkDBSrc, SQLType, Tabela, Auto_increment,
      SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, ComplUpd,
      InfoSQLOnError, InfoErro);
    else
      Geral.MB_Erro(Format(sRDBM_Nao_Definido, ['SQLInsUpd']));
  end;
end;

{
function TUnDmkRDBMs.StrToSpidAct(Nome: String): TDmkSpidAct;
var
  x: String;
begin
  x := LowerCase(Nome);
  if x = 'sbfrst' then Result := dsaFrst else
  if x = 'sbprio' then Result := dsaPrio else
  if x = 'sbthis' then Result := dsaThis else
  if x = 'sbnext' then Result := dsaNext else
  if x = 'sblast' then Result := dsaLast else
  if x = 'sbdesc' then Result := dsaDesc else
  if x = 'sbconf' then Result := dsaconf else
  if x = 'sbcanc' then Result := dsacanc else
  if x = 'sbhome' then Result := dsahome else
  if x = 'sbplus' then Result := dsaplus else
  if x = 'sbedit' then Result := dsaedit else
  if x = 'sbkill' then Result := dsakill else
  if x = 'sbimpr' then Result := dsaimpr else
  if x = 'sbcfgs' then Result := dsacfgs else
  if x = 'sbpesq' then Result := dsaPesq else
  if x = 'sbclse' then Result := dsaClse else
  begin
    Result := dsaUnkn;
    Geral.MB_Erro('Nome n�o implementado em TUnDmkRDBMs.StrToSpidAct()!');
  end;
end;
}

end.
