object FmCadLista: TFmCadLista
  Left = 339
  Top = 185
  Caption = 'CAD-LISTA-001 :: Cadastros de ...'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 195
        Height = 32
        Caption = 'Cadastros de ...'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 195
        Height = 32
        Caption = 'Cadastros de ...'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 195
        Height = 32
        Caption = 'Cadastros de ...'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object DBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 581
        Height = 330
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 480
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCad
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 480
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 581
        Top = 0
        Width = 203
        Height = 330
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 203
          Height = 85
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 46
            Height = 13
            Caption = 'Pesquisa:'
          end
          object EdPesq: TEdit
            Left = 8
            Top = 20
            Width = 185
            Height = 21
            TabOrder = 0
            OnChange = EdPesqChange
          end
          object TBTam: TTrackBar
            Left = 4
            Top = 40
            Width = 193
            Height = 41
            Min = 1
            ParentShowHint = False
            Position = 4
            SelStart = 3
            ShowHint = True
            TabOrder = 1
            OnChange = TBTamChange
          end
        end
        object DBGrid2: TDBGrid
          Left = 0
          Top = 85
          Width = 203
          Height = 245
          Align = alClient
          DataSource = DsPesq
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'd.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 131
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPesq: TABSQuery
    CurrentVersion = '6.01 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 296
    Top = 172
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCad: TDataSource
    DataSet = TbCad
    Left = 324
    Top = 144
  end
  object TbCad: TABSTable
    CurrentVersion = '6.01 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    BeforePost = TbCadBeforePost
    AfterPost = TbCadAfterPost
    OnUpdateRecord = TbCadUpdateRecord
    Exclusive = False
    Left = 296
    Top = 144
    object TbCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbCadNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 324
    Top = 172
  end
end
