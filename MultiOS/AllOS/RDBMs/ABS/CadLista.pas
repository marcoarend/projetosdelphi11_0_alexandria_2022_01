unit CadLista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkABS_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, ABSMain;

type
  TFmCadLista = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGrid;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    QrPesq: TABSQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TStringField;
    DsCad: TDataSource;
    TbCad: TABSTable;
    TbCadCodigo: TIntegerField;
    TbCadNome: TStringField;
    DsPesq: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbCadBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbCadAfterPost(DataSet: TDataSet);
    procedure TbCadUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind;
      var UpdateAction: TUpdateAction);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FAlterou: Boolean;
  public
    { Public declarations }
    //FNovoCodigo: TNovoCodigo;
    FDModTabela: TABSTable;
    FFormQuery: TABSQuery;
    //
    procedure ReabrePesquisa();
  end;

  var
  FmCadLista: TFmCadLista;

implementation

uses UnMyObjects, Module, UMemModule;

{$R *.DFM}

procedure TFmCadLista.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCadLista.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadLista.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmCadLista.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if FAlterou then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_XML);
    Dmod.Atualiza_XML_Genericos(TbCad.TableName, LaAviso1, LaAviso2);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    if FFormQuery <> nil then
    begin
      FFormQuery.Close;
      FFormQuery.Open;
    end;
    if FDModTabela <> nil then
      UMemMod.AbreABSTable1(FDModTabela);
  end;
end;

procedure TFmCadLista.FormCreate(Sender: TObject);
begin
  FAlterou := False;
  //FNovoCodigo := ncIdefinido;
end;

procedure TFmCadLista.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadLista.ReabrePesquisa();
var
  Texto, SQL: String;
  K, N, I: Integer;
begin
  QrPesq.Close;
  Texto := EdPesq.Text;
  K := Length(Texto);
  N := TBTam.Position;
  if N < 3 then
    N := 3;
  if K >= 3 then
  begin
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (Nome LIKE "%' + Copy(Texto, I, N) + '%")' + #13#10;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, 'MEMORY', [
    'SELECT Codigo, Nome ',
    'FROM ' + TbCad.TableName,
    'WHERE (Nome LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
  end;
end;

procedure TFmCadLista.TbCadUpdateRecord(DataSet: TDataSet;
  UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
begin
  FAlterou := True;
  if UpdateKind =  ukDelete then
  begin
    Geral.MensagemBox('Exclus�o n�o permitida!', 'Aviso', MB_OK+MB_ICONWARNING);
    UpdateAction := uaAbort;
  end;
end;

procedure TFmCadLista.TbCadAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TbCadCodigo.Value;
end;

procedure TFmCadLista.TbCadBeforePost(DataSet: TDataSet);
begin
  FAlterou := True;
  if Tbcad.State = dsInsert then
  begin
{
    case FNovoCodigo of
      ncControle:
      TbCadCodigo.Value := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, TbCad.TableName, 'Codigo', [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: TbCadCodigo.Value := UMyMod.BPGS1I32(
        TbCad.TableName, 'Codigo', '', '', tsDef, stIns, 0);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MensagemBox('Tipo de obten��o de novo c�digo indefinido!',
        'ERRO', MB_OK+MB_ICONERROR);
        Halt(0);
        Exit;
      end;
    end;
}
  TbCadCodigo.Value := UMemMod.BuscaNovoCodigo_Int(Dmod.QrAux,
  TbCad.TableName, 'Codigo', [], [], ImgTipo.SQLType, 0, siPositivo, nil);
  end;
end;

procedure TFmCadLista.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
