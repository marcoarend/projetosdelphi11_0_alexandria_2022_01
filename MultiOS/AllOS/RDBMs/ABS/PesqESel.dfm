object FmPesqESel: TFmPesqESel
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-003 :: Pesquisa e Seleciona pela Descri'#231#227'o'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Pesquisa e Seleciona pela Descri'#231#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 446
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'Descri'#231#227'o parcial:'
      end
      object LaDoc: TLabel
        Left = 660
        Top = 4
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
        Visible = False
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 20
        Width = 648
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdPesqChange
      end
      object EdDoc: TdmkEdit
        Left = 660
        Top = 20
        Width = 112
        Height = 21
        TabOrder = 1
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdPesqChange
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 49
      Width = 1008
      Height = 397
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel5: TPanel
        Left = 500
        Top = 0
        Width = 508
        Height = 397
        Align = alClient
        TabOrder = 0
        object Panel6: TPanel
          Left = 1
          Top = 348
          Width = 506
          Height = 48
          Align = alBottom
          TabOrder = 0
          object Panel2: TPanel
            Left = 404
            Top = 1
            Width = 101
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object BtSaida: TBitBtn
              Tag = 13
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
              NumGlyphs = 2
            end
          end
          object BtTodosS: TBitBtn
            Tag = 127
            Left = 8
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Todos'
            TabOrder = 1
            OnClick = BtTodosSClick
            NumGlyphs = 2
          end
          object BtNenhumS: TBitBtn
            Tag = 128
            Left = 102
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Nenhum'
            TabOrder = 2
            OnClick = BtNenhumSClick
            NumGlyphs = 2
          end
          object BtRetira: TBitBtn
            Tag = 12
            Left = 196
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Retira'
            TabOrder = 3
            OnClick = BtRetiraClick
            NumGlyphs = 2
          end
        end
        object DBGSel: TdmkDBGrid
          Left = 1
          Top = 1
          Width = 506
          Height = 347
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 384
              Visible = True
            end>
          Color = clWindow
          DataSource = DsSel
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 384
              Visible = True
            end>
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 500
        Height = 397
        Align = alLeft
        TabOrder = 1
        object DBGPsq: TdmkDBGrid
          Left = 1
          Top = 1
          Width = 498
          Height = 347
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 375
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPsq
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 375
              Visible = True
            end>
        end
        object PainelConfirma: TPanel
          Left = 1
          Top = 348
          Width = 498
          Height = 48
          Align = alBottom
          TabOrder = 1
          object BtAdiciona: TBitBtn
            Tag = 14
            Left = 192
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Adiciona'
            TabOrder = 0
            OnClick = BtAdicionaClick
            NumGlyphs = 2
          end
          object BtTodosP: TBitBtn
            Tag = 127
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Todos'
            TabOrder = 1
            OnClick = BtTodosPClick
            NumGlyphs = 2
          end
          object BtNenhumP: TBitBtn
            Tag = 128
            Left = 98
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Nenhum'
            TabOrder = 2
            OnClick = BtNenhumPClick
            NumGlyphs = 2
          end
        end
      end
    end
  end
  object QrPsq: TABSQuery
    CurrentVersion = '6.01 '
    InMemory = False
    ReadOnly = False
    SQL.Strings = (
      'SELECT * '
      'FROM pesqesel_psq')
    Left = 160
    Top = 124
    object QrPsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqNome: TStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrPsqAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPsq
    Left = 188
    Top = 124
  end
  object QrSel: TABSQuery
    CurrentVersion = '6.01 '
    InMemory = False
    ReadOnly = False
    SQL.Strings = (
      'SELECT * '
      'FROM pesqesel_sel')
    Left = 160
    Top = 152
    object QrSelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelNome: TStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSelAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsSel: TDataSource
    DataSet = QrSel
    Left = 188
    Top = 152
  end
end
