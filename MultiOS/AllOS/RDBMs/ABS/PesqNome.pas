unit PesqNome;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, ABSMain,
  dmkGeral, dmkEdit, dmkEditF7, DBCtrls, (*DBTables,*) dmkDBLookupComboBox,
  dmkDBGrid, UnMyObjects;

type
  TFmPesqNome = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    QrPesq: TABSQuery;
    DsPesq: TDataSource;
    DBGLista: TdmkDBGrid;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    EdDoc: TdmkEdit;
    LaDoc: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGListaDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCB: TDBLookupCombobox;
    FF7: TdmkEdit;
    FND: Boolean;
    FNomeTabela, FNomeCampoNome, FNomeCampoCodigo, FTexto: String;
    FAtivou: Boolean;
  end;

  var
  FmPesqNome: TFmPesqNome;

implementation

uses UnInternalConsts, Module, UnGOTOm;

{$R *.DFM}

procedure TFmPesqNome.BtOKClick(Sender: TObject);
begin
  if QrPesq.State <> dsInactive then
  begin
    VAR_CADASTRO := QrPesq.FieldByName('_Codigo').AsInteger;
    VAR_CAD_NOME := QrPesq.FieldByName('_Nome').AsString;
    Close;
  end;
end;

procedure TFmPesqNome.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqNome.DBGListaDblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmPesqNome.EdPesqChange(Sender: TObject);
var
  Texto, Doc: String;
begin
  QrPesq.Close;
  Texto := EdPesq.ValueVariant;
  Doc := Geral.SoNumero_TT(EdDoc.Text);
  if (Length(Texto) > 2) or (Length(Doc) > 10) then
  begin
    QrPesq.SQL.Clear;
    if FNomeTabela = 'entidades' then
    begin
      QrPesq.SQL.Add('SELECT Codigo _Codigo, IF(Tipo=0,');
      QrPesq.SQL.Add('CONCAT(RazaoSocial, " ", Fantasia),');
      QrPesq.SQL.Add('CONCAT(Nome, " ", Apelido)) _Nome');
      QrPesq.SQL.Add('FROM entidades');
      QrPesq.SQL.Add('WHERE (RazaoSocial LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('OR Nome LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('OR Fantasia LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('OR Apelido LIKE "%' + Texto + '%")');
      if EdDoc.Visible then
      begin
        if Doc <> '' then
          QrPesq.SQL.Add('AND ((CNPJ="' + Doc + '") OR (CPF = "' + Doc + '"))');
      end;
      QrPesq.SQL.Add('ORDER BY _Nome');
    end else
    if Lowercase(FNomeTabela) = 'gragrux' then
    begin
      QrPesq.SQL.Add('SELECT ggx.Controle _Codigo, gg1.Nome _Nome');
      QrPesq.SQL.Add('FROM gragrux ggx');
      QrPesq.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
      QrPesq.SQL.Add('WHERE gg1.Nome LIKE "%' + Texto + '%"');
      QrPesq.SQL.Add('ORDER BY _Nome');
    end else begin
      QrPesq.SQL.Add('SELECT ' + FNomeCampoCodigo + ' _Codigo, ' + FNomeCampoNome +
      ' _Nome FROM ' + FNomeTabela + ' WHERE ' + FNomeCampoNome + ' LIKE "%' +
      Texto + '%"');
    end;
    try
      // Usa nos aplicativos DermaXX
      //UMyMod.AbreQuery(QrPesq, 'TFmPesqNome.EdPesqChange()');
      QrPesq.Open;
    except
      GOTOm.LeMeuSQL_Fixo_m(QrPesq, '', nil, True, True);
    end;
  end;
end;

procedure TFmPesqNome.FormActivate(Sender: TObject);
var
  Query: TABSQuery;
  Texto: String;
  P, P10, P13: Integer;
begin
  MyObjects.CorIniComponente();
  if FCB <> nil then
  begin
    Query := TABSQuery(FCB.ListSource.DataSet);
    if Query <> nil then
    begin
      Texto := Lowercase(Query.SQL.Text);
      P := pos('from', Texto);
      if P > 0 then
      begin
        Texto := Trim(Copy(Texto, P + 4));
        P := pos(' ', Texto);
        P10 := pos(#10, Texto);
        P13 := pos(#13, Texto);
        if (P13 < P) and (P13 > 0) then P := P13;
        if (P10 < P) and (P10 > 0) then P := P10;
        if P > 0 then
        begin
          FNomeTabela := Trim(Copy(Texto, 1, P));
          FNomeCampoNome   := FCB.ListField;
          FNomeCampoCodigo := FCB.KeyField;
          //
          if FCB is TdmkDBLookupComboBox then
          begin
            if TdmkDBLookupComboBox(FCB).LocF7TableName <> '' then
              FNometabela := TdmkDBLookupComboBox(FCB).LocF7TableName;
            if TdmkDBLookupComboBox(FCB).LocF7CodiFldName <> '' then
              FNomeCampoCodigo := TdmkDBLookupComboBox(FCB).LocF7CodiFldName;
            if TdmkDBLookupComboBox(FCB).LocF7NameFldName <> '' then
              FNomeCampoNome := TdmkDBLookupComboBox(FCB).LocF7NameFldName;
          end;
        end;
      end;
    end;
  end
  else
  if FF7 is TdmkEditF7 then
  begin
    if TdmkEditF7(FF7).LocF7TableName <> '' then
      FNometabela := TdmkEditF7(FF7).LocF7TableName;

    if TdmkEditF7(FF7).LocF7CodiFldName <> '' then
      FNomeCampoCodigo := TdmkEditF7(FF7).LocF7CodiFldName;

    if TdmkEditF7(FF7).LocF7NameFldName <> '' then
      FNomeCampoNome := TdmkEditF7(FF7).LocF7NameFldName;
  end else
  if FND then
  begin
    //
  end;

  //

  if FNomeTabela = 'entidades' then
  begin
    LaDoc.Visible := True;
    EdDoc.Visible := True;
    EdPesq.Width  := 648;
  end;
  //
  if not FAtivou then
  begin
    FAtivou := True;
    EdPesq.Text := FTexto;
  end;
end;

procedure TFmPesqNome.FormCreate(Sender: TObject);
begin
  FND := False;
  //QrPesq.Database := ;
  FAtivou := False;
  FTexto := '';
end;

procedure TFmPesqNome.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
