unit MeuDBUses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, DBGrids, Buttons, StdCtrls, Menus, DB, TypInfo,
  frxClass, frxDBSet, frxCross, DBCtrls, dmkDBLookupComboBox, UnMyObjects,
  dmkEdit, dmkEditCB, dmkEditF7, ABSMain, dmkGeral, dmkDBEdit;

type
  TFmMeuDBUses = class(TForm)
    PnImprime: TPanel;
    Grade: TStringGrid;
    DBGrid1: TDBGrid;
    PnConfirma: TPanel;
    Button1: TButton;
    BtImprime: TBitBtn;
    Button2: TButton;
    PMImpressao: TPopupMenu;
    Visualizaimpresso1: TMenuItem;
    DesignerModificarelatrioantesdeimprimir1: TMenuItem;
    ImprimeImprimesemvizualizar1: TMenuItem;
    frxReport1: TfrxReport;
    frxCrossObject1: TfrxCrossObject;
    frxReport2: TfrxReport;
    frxDsPG: TfrxDBDataset;
    DsPG: TDataSource;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PnInativo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TEdit;
    EdCPF: TEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    dmkDBLookupComboBox1: TdmkDBLookupComboBox;
    dmkEditCB1: TdmkEditCB;
    dmkEditF71: TdmkEditF7;
    QrPG: TABSQuery;
    procedure Button2Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Visualizaimpresso1Click(Sender: TObject);
    procedure DesignerModificarelatrioantesdeimprimir1Click(Sender: TObject);
    procedure ImprimeImprimesemvizualizar1Click(Sender: TObject);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FPrintGrid: String;
    FTitulo: String;
    FGrade: TStringGrid;
    FDBGrid: TDBGrid;
    FTipoGrade: Integer;
    FContaCol: Integer;
    procedure Imprime_frx1(Mostra: Byte);
    procedure Imprime_frx2(Mostra: Byte);
  public
    { Public declarations }
    procedure ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
    procedure ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
    procedure ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
    function NaoPermiteExclusao(Tabela, Form : String; Msg: Integer) : Boolean;
    procedure PesquisaNome(Tabela, CampoNome, CampoCodigo: String);
    procedure PesquisaESeleciona(Tabela, CampoNome, CampoCodigo: String);
    procedure VirtualKey_F7(Texto: String);
    procedure ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor: Variant;
              var UpdType: TUpdType);
  end;

var
  FmMeuDBUses: TFmMeuDBUses;

implementation

uses UMemModule, PesqNome, PesqESel, Module,
UnMLAGeral, UnInternalConsts, UnMsgInt(*, UnMyVCLRef*);

{$R *.dfm}

{ TFmMeuDBUses }

procedure TFmMeuDBUses.BtImprimeClick(Sender: TObject);
begin
  Imprime_frx2(1);
end;

procedure TFmMeuDBUses.Button1Click(Sender: TObject);
begin
  Hide;
  PnImprime.Visible := False;
  PnInativo.Visible := True;
end;

procedure TFmMeuDBUses.Button2Click(Sender: TObject);
begin
  //FmPrincipal.SalvaArquivo(EdNome, EdCPF, Grade, Date, 'Dermatek.ini', True);
end;

procedure TFmMeuDBUses.DesignerModificarelatrioantesdeimprimir1Click(
  Sender: TObject);
begin
  case FTipoGrade of
    0: Imprime_Frx2(1);
    1: Imprime_Frx1(1);
  end;
end;

procedure TFmMeuDBUses.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    if FTipoGrade = 0 then
    begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[j - 1, i - 1]]);
    end else begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[i - 1, j - 1]]);
    end;
  end;
end;

procedure TFmMeuDBUses.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'Titulo' then Value := FTitulo;
end;

procedure TFmMeuDBUses.ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
{
var
  Query: TABSQuery;
}
begin
{ TODO : Impress�o DBGrid }
{
  Query := TABSQuery(DBGrid.DataSource.DataSet);
  frxDsPG.DataSet := Query;
  //
  FTipoGrade := 1; // Grade com origem de DataAware. DBGrid ...
  FTitulo := Titulo;
  FDBGrid := DBGrid;
  FGrade := TStringGrid(DBGrid);
  MLAGeral.MostraPopUpNoCentro(PMImpressao);
}
end;

procedure TFmMeuDBUses.ImprimeImprimesemvizualizar1Click(Sender: TObject);
begin
  case FTipoGrade of
    0: Imprime_Frx2(2);
    1: Imprime_Frx1(2);
  end;
end;

procedure TFmMeuDBUses.ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
var
  i, j, c, r: Integer;
  Xt: array of String;
  Xv: array of String;
begin
{ TODO : ImprimeStringGrid }
{
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  c := StringGrid.ColCount;
  r := StringGrid.RowCount;
  FPrintGrid := UCriar.RecriaMemTable('PrintGrid', DmodG.QrUpdPID1, False, c);
  SetLength(Xt, StringGrid.ColCount);
  SetLength(Xv, StringGrid.ColCount);
  for i := 0 to c - 1 do
    Xt[i] := 'Col' + FormatFloat('000', i);
  for i := 0 to r - 1 do
  begin
    for j := 0 to c - 1 do
      Xv[j] := StringGrid.Cells[j,i];
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO printgrid SET ');
    for j := 0 to c - 2 do
      DmodG.QrUpdPID1.SQL.Add(Xt[j] + '="' + dmkPF.DuplicaAspasDuplas(Xv[j]) + '",');
    DmodG.QrUpdPID1.SQL.Add(Xt[c-1] + '="' + dmkPF.DuplicaAspasDuplas(Xv[c-1]) + '"');

    UMyMod.ExecutaQuery(DmodG.QrUpdPID1);//.ExecSQL;
  end;
  FGrade  := StringGrid;
  FDBGrid := nil;
  FTitulo := Titulo;
  QrPg.Database   := DmodG.MyPID_DB;
  frxDsPG.DataSet := QrPG;
  MLAGeral.MostraPopUpNoCentro(PMImpressao);
}
end;

procedure TFmMeuDBUses.ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  FGrade := StringGrid;
  FTitulo := Titulo;
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.Imprime_frx1(Mostra: Byte);
begin
  Imprime_frx2(Mostra);
  ///////
  EXIT;
  ///////
  case Mostra of
    0: MyObjects.frxMostra(frxReport1, FTitulo);
    1: frxReport1.DesignReport;
    2: MyObjects.frxImprime(frxReport1, FTitulo);
  end;
end;

procedure TFmMeuDBUses.Imprime_frx2(Mostra: Byte);
const
  PagePagHei = 279;

  PageBanWid = 0;
  PagePagTop = 117; // >(=1,01) - (1,91 = 200)
  PagePagFot = 100;

  PageBanLef = 100;
  PageBanTop = 0;
  //PageBanWid = 0;
  PageBanHei = 50;
  PageMemHei = 50;
  PageColWid = 210;

var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  Foot: TfrxPageFooter;
  BANLEF, BANTOP, BANWID, BANHEI,
  //BARLEF, BARTOP, BARWID, BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  i,
  L, T, W, H, (*k, *)TopoIni: Integer;
  //Fator: Double;
  Campo: String;
  Menos: Byte;
begin
  Menos := 0;
  //k := -1;
  // Configura p�gina
  while frxReport2.PagesCount > 0 do
    frxReport2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxReport2);
  Page.CreateUniqueName;
  Page.LeftMargin   := 20;
  Page.RightMargin  := 10;
  Page.TopMargin    := 10;
  Page.BottomMargin := 10;
  Page.Height       := Trunc(PagePagHei / VAR_DOTIMP);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(PageBanWid / VAR_DOTIMP);
  H := Trunc(PagePagTop / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  TITULO DO RELAT�RIO
  FContaCol := 0;
  //TopoIni    := 0;
  MEMTOP := 0;
  MEMHEI := 24;//Trunc(PageMemHei / VAR_DOTIMP); 1,59 = 60 0,59=
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 1800;
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 12;
  //
  Memo.Memo.Text := FTitulo;
{
  if CkGrade.Checked then
  begin
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.TopLine.Width := 0.1;
    Memo.Frame.LeftLine.Width := 0.1;
    Memo.Frame.RightLine.Width := 0.1;
    Memo.Frame.BottomLine.Width := 0.1;
  end;
}
  ///  T�tulos dos campos
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    //  TITULOS DAS COLUNAS
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni + 24;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := 'Teste 1'
    else begin
      Memo.Memo.Text := FDBGrid.Columns[i].Title.Caption;
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  // Configura Banda de
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(PageBanTop / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PageBanHei / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.DataSet := frxDsPG;
  {
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  }
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := '[frxDsPG."Col' + FormatFloat('000', i) + '"]'
    else begin
      Campo := FDBGrid.Columns[i].FieldName;
      Memo.Memo.Text := '[frxDsPG."' + Campo + '"]';
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  {
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_TXT1]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_NOME]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  }
  //////////////////////////////////////////////////////////////////////////////
  // Configura Foota
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(269 / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PagePagFot / VAR_DOTIMP);
  Foot := TfrxPageFooter.Create(Page);
  Foot.CreateUniqueName;
  Foot.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  case Mostra of
    0: MyObjects.frxMostra(frxReport2, FTitulo);
    1: frxReport2.DesignReport;
    2: MyObjects.frxImprime(frxReport2, FTitulo);
  end;
end;

function TFmMeuDBUses.NaoPermiteExclusao(Tabela, Form: String;
  Msg: Integer): Boolean;
var
  Loc: Boolean;
begin
 { TODO : MeuDBUses.NaoPermiteExclusao }
{
  if Msg = -2 then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER))
    or ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    then Result := False else
    Application.MessageBox('Acesso Restrito', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if Msg = 0 then
    begin
      Application.MessageBox('Tabela de perfis n�o definida ao verificar acesso.',
      'Acesso a formul�rio', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((VAR_LOGIN = 'ADMIN') and (Uppercase(VAR_SENHA) = VAR_ADMIN) and (VAR_ADMIN <> ''))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  then Result := False
  else begin
    if Dmod.QrPerfis.State <> dsBrowse then
    begin
      Result := False;
      if (Copy(Uppercase(Application.Title), 1, 5) <> 'EXULT')
      and (Copy(Uppercase(Application.Title), 1, 5) <> 'EXUL2') then
      Application.MessageBox('Tabela de perfis n�o est� aberta!. Avise a Dermatek',
        'Acesso a formul�rio', MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    Dmod.QrPerfis.Close;
    Dmod.QrPerfis.Open;
    if not Dmod.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('INSERT INTO perfisits SET Janela=:P0');
        Dmod.QrAux.Params[0].AsString  := Form;
        Dmod.QrAux.ExecSQL;
        Dmod.Privilegios(-1000);
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (Dmod.QrPerfisLibera.Value = 0) or (Loc=False) then
    //if Form = 'F' then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Application.MessageBox('Somente a senha gerencial (BOSS) tem acesso a este formul�rio!',
          'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION)
      else begin
        case Msg of
         -1: ;// nada;
          0: Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          1: Application.MessageBox('Login com acesso parcial!', 'Acesso a formul�rio',
             MB_OK+MB_ICONEXCLAMATION);
          2: Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          3: Application.MessageBox('Login com acesso parcial, sem acesso a edi��o!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
        end;
      end;
    end else Result := False;
  end;
}
end;

procedure TFmMeuDBUses.ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor:
Variant; var UpdType: TUpdType);
var
  Campo: String;
begin
  Campo := GetPropValue(Objeto, 'UpdCampo');
  Valor := TABSDataSet(TDataSource(TdmkDBEdit(Objeto).DataSource).
    DataSet).FieldByName(Campo).Value;
  UpdType := TdmkDBEdit(Objeto).UpdType;
end;

procedure TFmMeuDBUses.PesquisaESeleciona(Tabela, CampoNome,
  CampoCodigo: String);
begin
  VAR_CADASTRO := 0;
  MyObjects.CriaForm_AcessoTotal(TFmPesqESel, FmPesqESel);
  FmPesqESel.FND := True;
  FmPesqESel.FNomeTabela := Tabela;
  FmPesqESel.FNomeCampoNome := CampoNome;
  FmPesqESel.FNomeCampoCodigo := CampoCodigo;
  FmPesqESel.ShowModal;
  FmPesqESel.Destroy;
end;

procedure TFmMeuDBUses.PesquisaNome(Tabela, CampoNome, CampoCodigo: String);
begin
  VAR_CADASTRO := 0;
  MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
  FmPesqNome.FND := True;
  FmPesqNome.FNomeTabela := Tabela;
  FmPesqNome.FNomeCampoNome := CampoNome;
  FmPesqNome.FNomeCampoCodigo := CampoCodigo;
  FmPesqNome.ShowModal;
  FmPesqNome.Destroy;
end;

procedure TFmMeuDBUses.Visualizaimpresso1Click(Sender: TObject);
begin
  case FTipoGrade of
    0: Imprime_Frx2(0);
    1: Imprime_Frx1(0);
  end;
end;

procedure TFmMeuDBUses.VirtualKey_F7(Texto: String);
var
  Controle: TControl;
  OK: Boolean;
begin
  if Screen.ActiveForm = nil then Exit;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB) or
    (Controle is TdmkEditF7);
    if OK then
    begin
      VAR_CADASTRO := 0;
      MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
      FmPesqNome.FTexto := Texto;
      if Controle is TdmkEditF7 then
      begin
        {
        if (Controle is TdmkEditF7) then
          Controle := TdmkEditF7(Controle).dmkEditCod;
        }
        FmPesqNome.FF7 := TdmkEdit(Controle);
        FmPesqNome.FCB := nil;
      end else
      begin
        if (Controle is TdmkEditCB) then
          Controle := TdmkEditCB(Controle).DBLookupComboBox;
        FmPesqNome.FCB := TDBLookupComboBox(Controle);
        FmPesqNome.FF7 := TdmkEdit(Controle);
      end;
      FmPesqNome.ShowModal;
      if VAR_CADASTRO <> 0 then
      begin
        if FmPesqNome.FCB <> nil then
        begin
          TDBLookupComboBox(Controle).KeyValue := VAR_CADASTRO;
          if Controle is TdmkDBLookupComboBox then
            TdmkEditCB(TdmkDBLookupComboBox(Controle).dmkEditCB).ValueVariant := VAR_CADASTRO;
        end
        else
        if FmPesqNome.FF7 <> nil then
        begin
          TdmkEditF7(Controle).Text := VAR_CAD_NOME;
          if TdmkEditF7(Controle).dmkEditCod <> nil then
          begin
            ShowMessage(TdmkEditF7(Controle).Name);
            ShowMessage(TdmkEdit(TdmkEditF7(Controle).dmkEditCod).Name);
            TdmkEdit(TdmkEditF7(Controle).dmkEditCod).ValueVariant := VAR_CADASTRO;
          end;
        end;
      end;
      FmPesqNome.Destroy;
    end;
  end;
end;

end.
