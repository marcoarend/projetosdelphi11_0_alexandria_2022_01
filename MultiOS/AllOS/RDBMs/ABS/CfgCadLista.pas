unit CfgCadLista;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Variants, ABSMain,
  dmkGeral, DB,(* DbTables,*) UndmkProcFunc;

type
  TUnCfgCadLista = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraCadLista(DBName: String; Tabela: String; SizeNome:
              Integer; (*NovoCodigo: TNovoCodigo;*) Titulo: String;
              FormQuery: TABSQuery);
    procedure InsAltItemLstEmRegTab(DestTabela, DestFldItem,
              DestFldValr, SrcTabela, SrcFldCodi, SrcFldNome, _WHERE_: String;
              DefaultItem, DefaultValr: Variant; TipoItem, TipoValr: TAllFormat;
              Tamanho1, Tamanho2: Integer; SQLType: TSQLType; Titulo,
              PromptItem, PromptValr: String; DestNovoCodigo, SrcNovoCodigo:
              TNovoCodigo; SQLIndex: array of String;
              ValIndex: array of Variant; Qry: TABSQuery);
  end;

var
  UnCfgCadLista: TUnCfgCadLista;

implementation

uses CadLista, Module, UMemModule, SelCod, MemDBCheck;

{ TUnCfgCadLista }

procedure TUnCfgCadLista.InsAltItemLstEmRegTab(DestTabela, DestFldItem,
  DestFldValr, SrcTabela, SrcFldCodi, SrcFldNome, _WHERE_: String;
  DefaultItem, DefaultValr: Variant; TipoItem, TipoValr: TAllFormat;
  Tamanho1, Tamanho2: Integer; SQLType: TSQLType; Titulo,
  PromptItem, PromptValr: String; DestNovoCodigo, SrcNovoCodigo:
  TNovoCodigo; SQLIndex: array of String;
  ValIndex: array of Variant; Qry: TABSQuery);
var
  Idx: Integer;
  UsaValr: Boolean;
  //
  procedure InsAlt(Item, Valr: Variant);
  {
  var
    Codigo, Controle, Caracteris: Integer;
  }
  begin
    Idx := 0;
    if SQLType = stIns then
    begin
      if TipoItem = dmktfInteger then
      begin
        Idx := UMemMod.BuscaNovoCodigo_Int(Dmod.QrAux,
        DestTabela, DestFldItem, [], [], stIns, 0, siPositivo, nil);
      end;
      //
      ValIndex[High(ValIndex)] := Idx;
    end;
    if UsaValr then
    begin
      UMemMod.SQLInsUpd(Dmod.QrUpd, SQLType, DestTabela, False, [
      DestFldItem, DestFldValr], SQLIndex, [
      Item, Valr], ValIndex, True);
    end else
    begin
      UMemMod.SQLInsUpd(Dmod.QrUpd, SQLType, DestTabela, False, [
      DestFldItem(*, DestFldValr*)], SQLIndex, [
      Item(*, Valr*)], ValIndex, True);
    end;
    if Qry <> nil then
    begin
      Qry.Close;
      Qry.Open;
      Qry.Locate(SQLIndex[High(SQLIndex)], ValIndex[High(ValIndex)], []);
    end;
  end;
begin
  Application.CreateForm(TFmSelCod, FmSelCod);
  FmSelCod.ImgTipo.SQLType := SQLType;
  FmSelCod.Caption := Copy(FmSelCod.Caption, 1, 17) + Titulo;
  FmSelCod.LaPrompt.Caption := PromptItem;
  FmSelCod.QrSel.Close;
  FmSelCod.QrSel.SQL.Clear;
  FmSelCod.QrSel.SQL.Add('SELECT ' + SrcFldNome + ' Descricao, ' + SrcFldCodi +  ' Codigo');
  FmSelCod.QrSel.SQL.Add('FROM ' + SrcTabela);
  FmSelCod.QrSel.SQL.Add(_WHERE_);
  FmSelCod.QrSel.SQL.Add('ORDER BY ' + SrcFldNome);
  FmSelCod.QrSel.Open;
  //
  FmSelCod.CBSel.LocF7TableName := SrcTabela;
  FmSelCod.CBSel.LocF7CodiFldName := SrcFldCodi;
  FmSelCod.CBSel.LocF7NameFldName := SrcFldNome;
  //
  FmSelCod.FTabela := SrcTabela;
  //FmSelCod.FNovoCodigo := SrcNovoCodigo;
  FmSelCod.SBCadastro.Visible := True;
  //
  if DefaultItem <> Null then
  begin
    FmSelCod.EdSel.ValueVariant := DefaultItem;
    FmSelCod.CBSel.KeyValue     := DefaultItem;
  end;
  UsaValr := TipoValr <> dmktfNone;
  if UsaValr then
  begin
    FmSelCod.LaOrdem.Visible := True;
    FmSelCod.LaOrdem.Caption := PromptValr;
    FmSelCod.dmkEdOrdem.Visible := True;
    FmSelCod.dmkEdOrdem.FormatType := TipoValr;
    case TipoValr of
      //dmktfNone: ;
      dmktfString: ;
      dmktfDouble: FmSelCod.dmkEdOrdem.DecimalSize := Tamanho2;
      dmktfInteger: FmSelCod.dmkEdOrdem.LeftZeros := Tamanho2;
      {
      dmktfLongint: ;
      dmktfInt64: ;
      dmktfDate: ;
      dmktfTime: ;
      dmktfDateTime: ;
      dmktfMesAno: ;
      dmktf_AAAAMM: ;
      dmktfUnknown: ;
      }
    end;
    if DefaultValr <> Null then
      FmSelCod.dmkEdOrdem.ValueVariant := DefaultValr;
  end else begin
    FmSelCod.LaOrdem.Visible := False;
    FmSelCod.dmkEdOrdem.Visible := False;
  end;
  FmSelCod.ShowModal;
  if FmSelCod.FSelected then
  begin
    InsAlt(FmSelCod.EdSel.ValueVariant,
      FmSelCod.dmkEdOrdem.ValueVariant);
  end;
  FmSelCod.Destroy;
end;

procedure TUnCfgCadLista.MostraCadLista(DBName: String; Tabela: String;
  SizeNome: Integer; (*NovoCodigo: TNovoCodigo;*) Titulo: String;
  FormQuery: TABSQuery);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCadLista, FmCadLista, afmoNegarComAviso) then
  begin
    FmCadLista.Caption := 'CAD-LISTA-001 :: ' + Titulo;
    FmCadLista.TbCad.DatabaseName := DBName;
    FmCadLista.TbCad.TableName := Tabela;
    //FmCadLista.FNovoCodigo := NovoCodigo;
    FmCadLista.FFormQuery := FormQuery;
    if SizeNome > 0 then
    begin
      FmCadLista.TbCadNome.DisplayWidth := SizeNome;
      FmCadLista.TbCadNome.Size := SizeNome;
    end;
    FmCadLista.TbCad.Open;
    //
    FmCadLista.ShowModal;
    FmCadLista.Destroy;
    //
  end;
end;

end.
