unit SelCod;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*) Mask,
  UMemModule, ABSMain, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral,
  dmkImage, mySQLDbTables;

type
  TFmSelCod = class(TForm)
    PainelDados: TPanel;
    LaPrompt: TLabel;
    CBSel: TdmkDBLookupComboBox;
    DsSel: TDataSource;
    EdSel: TdmkEditCB;
    LaOrdem: TLabel;
    QrSel: TABSQuery;
    QrSelDescricao: TStringField;
    QrSelCodigo: TIntegerField;
    dmkEdOrdem: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    SBCadastro: TSpeedButton;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelected: Boolean;
    FTabela: String;
    //FNovoCodigo: TNovoCodigo;
    //
    procedure Informa(Texto: String);
  end;

var
  FmSelCod: TFmSelCod;

implementation

uses {$IfNDef SemCfgCadLista} CfgCadLista,{$EndIf}
  UnMyObjects, Module;

{$R *.DFM}

procedure TFmSelCod.BtOKClick(Sender: TObject);
begin
  VAR_SELCOD := Geral.IMV(EdSel.Text);
  if VAR_SELCOD = 0 then
  begin
    Geral.MensagemBox('Nenhuma escolha foi realizada!', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  FSelected  := True;
  Close;
end;

procedure TFmSelCod.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmSelCod.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelCod.Informa(Texto: String);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Texto);
end;

procedure TFmSelCod.SBCadastroClick(Sender: TObject);
begin
{$IfNDef SemCfgCadLista}
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrSel.DatabaseName, FTabela, QrSelDescricao.Size,
  (*FNovoCodigo,*)
  'Cadastro de "' + Geral.Substitui(LaPrompt.Caption, ':', #0) + '"',
  QrSel);
  //
  UMemMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmSelCod.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelCod.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSelected := False;
  VAR_SELCOD := 0;
end;

end.
