object FmMeuDBUses: TFmMeuDBUses
  Left = 0
  Top = 0
  Caption = 'FmMeuDBUses'
  ClientHeight = 484
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PnImprime: TPanel
    Left = 0
    Top = 57
    Width = 635
    Height = 253
    Align = alTop
    TabOrder = 0
    object Grade: TStringGrid
      Left = 100
      Top = 64
      Width = 441
      Height = 137
      ColCount = 2
      DefaultColWidth = 200
      DefaultRowHeight = 18
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
      TabOrder = 0
      RowHeights = (
        18
        18
        18
        18
        18)
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 144
      Top = 24
      Width = 145
      Height = 21
      TabOrder = 1
    end
    object dmkDBLookupComboBox1: TdmkDBLookupComboBox
      Left = 312
      Top = 28
      Width = 145
      Height = 21
      TabOrder = 2
      UpdType = utYes
    end
    object dmkEditCB1: TdmkEditCB
      Left = 492
      Top = 28
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      IgnoraDBLookupComboBox = False
    end
    object dmkEditF71: TdmkEditF7
      Left = 356
      Top = 8
      Width = 80
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'dmkEditF71'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'dmkEditF71'
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 310
    Width = 635
    Height = 133
    Align = alClient
    DataSource = DsPG
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object PnConfirma: TPanel
    Left = 0
    Top = 443
    Width = 635
    Height = 41
    Align = alBottom
    TabOrder = 2
    object Button1: TButton
      Left = 508
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Fecha'
      TabOrder = 0
      OnClick = Button1Click
    end
    object BtImprime: TBitBtn
      Left = 300
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Imprime'
      TabOrder = 1
      OnClick = BtImprimeClick
    end
    object Button2: TButton
      Left = 144
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Grava'
      Enabled = False
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object PnInativo: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 57
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 104
      Top = 8
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label2: TLabel
      Left = 432
      Top = 8
      Width = 58
      Height = 13
      Caption = 'CNPJ / CPF:'
    end
    object EdNome: TEdit
      Left = 104
      Top = 24
      Width = 325
      Height = 21
      TabOrder = 0
    end
    object EdCPF: TEdit
      Left = 432
      Top = 24
      Width = 113
      Height = 21
      TabOrder = 1
    end
  end
  object PMImpressao: TPopupMenu
    Left = 380
    Top = 276
    object Visualizaimpresso1: TMenuItem
      Caption = '&Preview (Visualiza impress'#227'o)'
      OnClick = Visualizaimpresso1Click
    end
    object DesignerModificarelatrioantesdeimprimir1: TMenuItem
      Caption = '&Designer (Modifica relat'#243'rio antes de imprimir)'
      OnClick = DesignerModificarelatrioantesdeimprimir1Click
    end
    object ImprimeImprimesemvizualizar1: TMenuItem
      Caption = '&Imprime (Imprime sem vizualizar)'
      OnClick = ImprimeImprimesemvizualizar1Click
    end
  end
  object frxReport1: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 38007.714423611100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure Cross1OnPrintCell(Memo: TfrxMemoView; RowIndex, Column' +
        'Index, CellIndex: Integer; RowValues, ColumnValues, Value: Varia' +
        'nt);'
      'var'
      '  s1, s2: String;'
      '  i: Integer;                           '
      'begin'
      '  if Length(Memo.Text) > 0 then        '
      '  begin              '
      '    s1 := Trim(Memo.Text);'
      '    s2 := '#39#39';                        '
      '    for i := 1 to Length(s1) do'
      
        '      if s1[i] in (['#39'0'#39'..'#39'9'#39', '#39','#39', '#39'+'#39', '#39'-'#39', '#39'.'#39']) then s2 := s2' +
        ' + s1[i];'
      '    if s2 = s1 then'
      '      Memo.HAlign := haRight    '
      '    else                '
      '      Memo.HAlign := haLeft;        '
      '  end else Cross1Cell0.HAlign := haCenter;'
      
        '  //ShowMessage(IntToStr(ColumnIndex) + '#39'.'#39' + IntToStr(RowIndex)' +
        ' + '#39' = '#39' + s1);'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnBeforePrint = frxReport1BeforePrint
    OnGetValue = frxReport1GetValue
    Left = 196
    Top = 276
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo2: TfrxMemoView
          Left = 11.338590000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          ShowHint = False
          HAlign = haCenter
          Memo.UTF8 = (
            '[Titulo]')
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 102.047310000000000000
        Width = 1046.929810000000000000
        RowCount = 1
        object Cross1: TfrxCrossView
          Left = 11.338590000000000000
          Width = 100.000000000000000000
          Height = 62.000000000000000000
          ShowHint = False
          DownThenAcross = False
          MinWidth = 30
          ShowColumnHeader = False
          ShowColumnTotal = False
          ShowRowHeader = False
          ShowRowTotal = False
          OnPrintCell = 'Cross1OnPrintCell'
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D38223F3E3C63726F73733E3C63656C6C6D656D6F733E3C546672784D656D
            6F56696577204C6566743D2233312C33333835392220546F703D223132322C30
            34373331222057696474683D22363022204865696768743D2232322220526573
            7472696374696F6E733D223234222053686F7748696E743D2246616C73652220
            416C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E4368
            61727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E4865
            696768743D222D392220466F6E742E4E616D653D22417269616C2220466F6E74
            2E5374796C653D223022204672616D652E5479703D2231352220476170583D22
            332220476170593D2233222048416C69676E3D22686143656E74657222205061
            72656E74466F6E743D2246616C7365222056416C69676E3D22766143656E7465
            722220546578743D2230222F3E3C546672784D656D6F56696577205461673D22
            3122204C6566743D22302220546F703D2230222057696474683D223022204865
            696768743D223022205265737472696374696F6E733D2238222053686F774869
            6E743D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C
            736522204672616D652E5479703D2231352220476170583D2233222047617059
            3D2233222048416C69676E3D226861526967687422205374796C653D2263656C
            6C222056416C69676E3D22766143656E7465722220546578743D22222F3E3C54
            6672784D656D6F56696577205461673D223222204C6566743D22302220546F70
            3D2230222057696474683D223022204865696768743D22302220526573747269
            6374696F6E733D2238222053686F7748696E743D2246616C73652220416C6C6F
            7745787072657373696F6E733D2246616C736522204672616D652E5479703D22
            31352220476170583D22332220476170593D2233222048416C69676E3D226861
            526967687422205374796C653D2263656C6C222056416C69676E3D2276614365
            6E7465722220546578743D22222F3E3C546672784D656D6F5669657720546167
            3D223322204C6566743D22302220546F703D2230222057696474683D22302220
            4865696768743D223022205265737472696374696F6E733D2238222053686F77
            48696E743D2246616C73652220416C6C6F7745787072657373696F6E733D2246
            616C736522204672616D652E5479703D2231352220476170583D223322204761
            70593D2233222048416C69676E3D226861526967687422205374796C653D2263
            656C6C222056416C69676E3D22766143656E7465722220546578743D22222F3E
            3C2F63656C6C6D656D6F733E3C63656C6C6865616465726D656D6F733E3C5466
            72784D656D6F56696577204C6566743D22302220546F703D2230222057696474
            683D223022204865696768743D223022205265737472696374696F6E733D2238
            222053686F7748696E743D2246616C73652220416C6C6F774578707265737369
            6F6E733D2246616C736522204672616D652E5479703D2231352220476170583D
            22332220476170593D2233222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C546672784D656D6F56696577204C6566743D2230222054
            6F703D2230222057696474683D223022204865696768743D2230222052657374
            72696374696F6E733D2238222053686F7748696E743D2246616C73652220416C
            6C6F7745787072657373696F6E733D2246616C736522204672616D652E547970
            3D2231352220476170583D22332220476170593D2233222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C2F63656C6C6865616465726D
            656D6F733E3C636F6C756D6E6D656D6F733E3C546672784D656D6F5669657720
            5461673D2231303022204C6566743D22302220546F703D223022205769647468
            3D2232303022204865696768743D22313030303022205265737472696374696F
            6E733D223234222053686F7748696E743D2246616C73652220416C6C6F774578
            7072657373696F6E733D2246616C736522204672616D652E5479703D22313522
            20476170583D22332220476170593D2233222048416C69676E3D22686143656E
            74657222205374796C653D22636F6C756D6E222056416C69676E3D2276614365
            6E7465722220546578743D22222F3E3C2F636F6C756D6E6D656D6F733E3C636F
            6C756D6E746F74616C6D656D6F733E3C546672784D656D6F5669657720546167
            3D2233303022204C6566743D22302220546F703D2230222057696474683D2233
            3222204865696768743D22313030303022205265737472696374696F6E733D22
            38222056697369626C653D2246616C7365222053686F7748696E743D2246616C
            73652220416C6C6F7745787072657373696F6E733D2246616C73652220466F6E
            742E436861727365743D22312220466F6E742E436F6C6F723D22302220466F6E
            742E4865696768743D222D31332220466F6E742E4E616D653D22417269616C22
            20466F6E742E5374796C653D223122204672616D652E5479703D223135222047
            6170583D22332220476170593D2233222048416C69676E3D22686143656E7465
            722220506172656E74466F6E743D2246616C736522205374796C653D22636F6C
            6772616E64222056416C69676E3D22766143656E7465722220546578743D2247
            72616E6420546F74616C222F3E3C2F636F6C756D6E746F74616C6D656D6F733E
            3C636F726E65726D656D6F733E3C546672784D656D6F56696577204C6566743D
            22302220546F703D2230222057696474683D2232303022204865696768743D22
            3022205265737472696374696F6E733D2238222056697369626C653D2246616C
            7365222053686F7748696E743D2246616C73652220416C6C6F77457870726573
            73696F6E733D2246616C736522204672616D652E5479703D2231352220476170
            583D22332220476170593D2233222048416C69676E3D22686143656E74657222
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C546672
            784D656D6F56696577204C6566743D22302220546F703D223022205769647468
            3D2232303022204865696768743D223022205265737472696374696F6E733D22
            38222056697369626C653D2246616C7365222053686F7748696E743D2246616C
            73652220416C6C6F7745787072657373696F6E733D2246616C73652220467261
            6D652E5479703D2231352220476170583D22332220476170593D223322204841
            6C69676E3D22686143656E746572222056416C69676E3D22766143656E746572
            2220546578743D22222F3E3C546672784D656D6F56696577204C6566743D2230
            2220546F703D2230222057696474683D223022204865696768743D2230222052
            65737472696374696F6E733D2238222056697369626C653D2246616C73652220
            53686F7748696E743D2246616C73652220416C6C6F7745787072657373696F6E
            733D2246616C736522204672616D652E5479703D2231352220476170583D2233
            2220476170593D2233222048416C69676E3D22686143656E746572222056416C
            69676E3D22766143656E7465722220546578743D22222F3E3C546672784D656D
            6F56696577204C6566743D22302220546F703D2230222057696474683D223230
            3022204865696768743D223022205265737472696374696F6E733D2238222053
            686F7748696E743D2246616C73652220416C6C6F7745787072657373696F6E73
            3D2246616C736522204672616D652E5479703D2231352220476170583D223322
            20476170593D2233222048416C69676E3D22686143656E746572222056416C69
            676E3D22766143656E7465722220546578743D22222F3E3C2F636F726E65726D
            656D6F733E3C726F776D656D6F733E3C546672784D656D6F5669657720546167
            3D2232303022204C6566743D22302220546F703D2230222057696474683D2232
            303022204865696768743D22313030303022205265737472696374696F6E733D
            223234222053686F7748696E743D2246616C73652220416C6C6F774578707265
            7373696F6E733D2246616C736522204672616D652E5479703D22313522204761
            70583D22332220476170593D2233222048416C69676E3D22686143656E746572
            22205374796C653D22726F77222056416C69676E3D22766143656E7465722220
            546578743D22222F3E3C2F726F776D656D6F733E3C726F77746F74616C6D656D
            6F733E3C546672784D656D6F56696577205461673D2234303022204C6566743D
            22302220546F703D2230222057696474683D22333222204865696768743D2231
            3030303022205265737472696374696F6E733D2238222056697369626C653D22
            46616C7365222053686F7748696E743D2246616C73652220416C6C6F77457870
            72657373696F6E733D2246616C73652220466F6E742E436861727365743D2231
            2220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D222D31
            332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C653D
            223122204672616D652E5479703D2231352220476170583D2233222047617059
            3D2233222048416C69676E3D22686143656E7465722220506172656E74466F6E
            743D2246616C736522205374796C653D22726F776772616E64222056416C6967
            6E3D22766143656E7465722220546578743D224772616E6420546F74616C222F
            3E3C2F726F77746F74616C6D656D6F733E3C63656C6C66756E6374696F6E733E
            3C6974656D20302F3E3C2F63656C6C66756E6374696F6E733E3C636F6C756D6E
            736F72743E3C6974656D20322F3E3C2F636F6C756D6E736F72743E3C726F7773
            6F72743E3C6974656D20322F3E3C2F726F77736F72743E3C2F63726F73733E}
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 177.637910000000000000
        Width = 1046.929810000000000000
        object Memo1: TfrxMemoView
          Left = 551.710592530000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 224
    Top = 276
  end
  object frxReport2: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40111.836308298610000000
    ReportOptions.LastChange = 40111.836308298610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 264
    Top = 276
    Datasets = <
      item
        DataSet = frxDsPG
        DataSetName = 'frxDsPG'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxDsPG: TfrxDBDataset
    UserName = 'frxDsPG'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 292
    Top = 276
  end
  object DsPG: TDataSource
    Left = 348
    Top = 276
  end
  object PopupMenu1: TPopupMenu
    Left = 380
    Top = 276
    object MenuItem1: TMenuItem
      Caption = '&Preview (Visualiza impress'#227'o)'
    end
    object MenuItem2: TMenuItem
      Caption = '&Designer (Modifica relat'#243'rio antes de imprimir)'
    end
    object MenuItem3: TMenuItem
      Caption = '&Imprime (Imprime sem vizualizar)'
    end
  end
  object QrPG: TABSQuery
    CurrentVersion = '6.01 '
    InMemory = False
    ReadOnly = False
    SQL.Strings = (
      'SELECT * '
      'FROM printgrid')
    Left = 320
    Top = 276
  end
end
