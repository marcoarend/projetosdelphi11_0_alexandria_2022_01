unit UnDmkABS_PF;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, ShellAPI, TypInfo,
  comobj, ShlObj, Winsock, Math, About, UnMsgInt, DB,(* DbTables,*) dmkGeral,
  UnInternalConsts, ABSMain;

type
  TUnDmkABS_PF = class(TObject)
  private
    {private declaration}
    // Usar AbreMySQLQuery0 qundo poss�vel!
    function  AbreABSQuery1(Query: TABSQuery;
              SQL: array of String): Boolean;
    procedure MostraQuery(Query: TABSQuery; Aviso: String = '');
  public
    {public declaration}
    procedure LeMeuSQL_Fixo_y(Query: TABSQuery; Arquivo: String; Memo: TMemo;
               WinArq, AskShow: Boolean);
    procedure LeMeuSQLForce_y(Query: TABSQuery; Arquivo: String; Memo: TMemo;
              WinArq, AskShow, Force: Boolean);
    function  AbreABSQuery0(Query: TABSQuery; DB: String;
              SQL: array of String): Boolean;
    function  AbreQuery(Query: TABSQuery; DB: String = 'MEMORY';
              Aviso: String = ''): Boolean;
    function  TabelaExiste(Tabela: String; Qry: TABSQuery;
              Database: String): Boolean;
  end;

var
  DmkABS_PF: TUnDmkABS_PF;

implementation

{ TDmkABS_PF }

function TUnDmkABS_PF.AbreABSQuery0(Query: TABSQuery; DB: String;
  SQL: array of String): Boolean;
begin
  Query.Close;
  Query.DatabaseName := DB;
  //
  AbreABSQuery1(Query, SQL);
end;

function TUnDmkABS_PF.AbreABSQuery1(Query: TABSQuery;
  SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
begin
  //Result := False;
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no ABSQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query. Open ;
      Result := True;
    end;
    Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    Geral.MB_Erro('Erro ao tentar abrir uma SQL no ABSQuery!' + sLineBreak +
    'Avise a DERMATEK!');
    MostraQuery(Query);
    raise;
  end;
end;

function TUnDmkABS_PF.AbreQuery(Query: TABSQuery; DB: String; Aviso: String): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
var
  Texto: String;
begin
  Screen.Cursor := crSQLWait;
  try
    Query.Close;
    Query.DatabaseName := DB;
    Query. Open ;
    Result := True;
    Screen.Cursor := crDefault;
  except
    on E: Exception do
    begin
      Texto := E.Message + sLineBreak;
      MostraQuery(Query, Texto + Aviso);
      Screen.Cursor := crDefault;
      raise;
    end else
    begin
      MostraQuery(Query, Aviso);
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

procedure TUnDmkABS_PF.LeMeuSQLForce_y(Query: TABSQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow, Force: Boolean);
var
  i: Integer;
  WinPath : Array[0..144] of Char;
  Text1, Text2: TextFile;
  Name, Diretorio: String;
  Other: Boolean;
  Texto1, Texto2, Texto3, Texto4: String;
begin
  Texto1 := '/*'+Query.DatabaseName+'.'+Query.Name+'*/';
  Texto2 := '/*********** SQL ***********/';
  Texto3 := '/********* FIM SQL *********/';
  Texto4 := sLineBreak;
  if WinArq then
  begin
    GetWindowsDirectory(WinPath,144);
    Diretorio := StrPas(WinPath)+'\Dermatek';
    if not DirectoryExists(Diretorio) then ForceDirectories(Diretorio);
    Name := Diretorio + '\SQL.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(Text1, Name);
    ReWrite(Text1);
    WriteLn(Text1, Texto1);
  end;
  if Trim(Arquivo) <> '' then
  begin
    Other := True;
    if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text2, Arquivo);
    ReWrite(Text2);
    WriteLn(Text2, Texto1);
  end else Other := False;
  if Memo <> nil then
  begin
    if Query.DatabaseName <> '' then
    begin
      Memo.Lines.Add(Texto1);
      Memo.Lines.Add(Texto2);
    end;
  end;
  for i := 0 to Query.SQL.Count -1 do
  begin
    if WinArq then WriteLn(Text1, Query.SQL[i]);
    if Other  then WriteLn(Text2, Query.SQL[i]);
    if Memo <> nil then Memo.Lines.Add(Query.SQL[i]);
  end;
  //
  if Query.Params.Count > 0 then
  begin
    Texto4 := Texto4 + '/***** Parametros *******/' + sLineBreak;
    for i := 0 to Query.Params.Count -1 do
    begin
      Texto4 := Texto4 + '/***** ' + Query.Params[i].Name + ' = ' +
        Geral.VariantToString(Query.Params[i].Value) + ' ******/'  + sLineBreak;
    end;
    Texto4 := Texto4 + '/***** FIM Parametros *******/' + sLineBreak;
  end else begin
    Texto4 := Texto4 + '/*****Query sem parametros*******/' + sLineBreak;
  end;
  if WinArq then WriteLn(Text1, Texto4);
  if Other  then WriteLn(Text2, Texto4);
  if Memo <> nil then Memo.Lines.Add(Texto4);
  //
  if WinArq then CloseFile(Text1);
  if Other  then CloseFile(Text2);
  if Query.DatabaseName <> '' then
    if Memo <> nil then Memo.Lines.Add(Texto3);
  if AskShow then
  begin
    if (FM_MASTER = 'V') or (Uppercase(VAR_LOGIN) = 'A') or Force then
    begin
      Geral.MB_Aviso(Texto1 + sLineBreak + Texto2 + sLineBreak + Query.SQL.Text +
      Texto3 + sLineBreak + Texto4);
    end;
  end;
end;

procedure TUnDmkABS_PF.LeMeuSQL_Fixo_y(Query: TABSQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow: Boolean);
begin
  LeMeuSQLForce_y(Query, Arquivo, Memo, WinArq, AskShow, True);
end;

procedure TUnDmkABS_PF.MostraQuery(Query: TABSQuery; Aviso: String);
var
  i: Integer;
  NomeParente: String;
  Texto: WideString;
begin
  if Query.Owner <> nil then
    NomeParente := 'Owner: ' + TComponent(Query.Owner).Name
  else
    NomeParente := 'SEM OWNER!';
  Texto := '/* ' + Aviso +
    sLineBreak + NomeParente +
    sLineBreak + ' */' +
    sLineBreak + Query.SQL.Text;
  //
  for i := 1 to Query.ParamCount do
  begin
    Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
      TParam(Query.Params[i-1]).AsString + '*/';
  end;
  Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
  Geral.MB_Erro(Texto);
end;

function TUnDmkABS_PF.TabelaExiste(Tabela: String;
  Qry: TABSQuery; Database: String): Boolean;
begin
  Result := False;
  try
    Qry.Close;
    Qry.DatabaseName := Database;
    Qry.SQL.Clear;
    Qry.SQL.Add('SHOW TABLES FROM '+ Database);
    Qry. Open ;
    //
    Qry.First;
    while not Qry.Eof do
    begin
      if Uppercase(Tabela) = Uppercase(Qry.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      Qry.Next;
    end;
  except
    raise;
  end;
end;

end.
