unit UnDmkDAC;

interface

uses System.SysUtils, System.Variants, System.Classes,
  System.Generics.Collections,
  Xml.xmldom, Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc,
  FMX.Forms, FMX.Memo, FMX.StdCtrls,
  //FireDAC.Comp.Client,
  UnGeral, UnMyListas, UnGrl_Vars, UnDmkEnums, UnWebReq, UnDmkRDBMs;

type
  //TArrDmkDAC = array of array of String;
  // Campos e Valores da cada linha de uma tabela obtidos de XML
  TArrXMLSQLins = array of array of array[0..1] of String;

// G E R A L
{
  TXMLSQLLin = ^TCamposXMLSQLLin;
  TCamposXMLSQLLin = record
    Campos         : String;
    Valores        : String;
  end;
}

// R E C O R D S  //////////////////////////////////////////////////////////////

  // Tabela padr�o de cadastro simples de lista
  TFld_lst_sample = ^TCampos_lst_sample;
  TCampos_lst_sample = record
    Codigo         : Integer;
    Nome           : String;
    Lk             : Integer;
    DataCad        : TDateTime;
    UserCad        : Integer;
    DataAlt        : TDateTime;
    UserAlt        : Integer;
    AlterWeb       : Byte;
    Ativo          : Byte;
  end;

  // Tabela s� de Integer (Codigo)
  TFldIDInt = ^TCamposIDInt;
  TCamposIDInt = record
    Value         : Integer;
  end;
  TTabIDInt = TList<TFldIDInt>;

  // Tabela s� de String (Retorno de qualquer tipo de dado)
  TFldIDStr = ^TCamposIDStr;
  TCamposIDStr = record
    Value         : String;
  end;
  TTabIDStr = TList<TFldIDStr>;

  // F I M   R E C O R D S /////////////////////////////////////////////////////

  TUnDmkDAC = class(TObject)
  private
    { Private declarations }
    //
    function  SQLXML_QueryXML(Form: TForm; SQLType: TSQLType; TextoSQL: String): String;
    procedure SQLXML_DomToTree(const XmlNode: IXMLNode; var ArrXMLSQLins:
              TArrXMLSQLins; var DelFlds: array of String; var DelVals: array of Variant);
    function  SQLArrRec_Ins(Form: TForm; PB: TProgressBar; ArrXMLSQLins: TArrXMLSQLins;
              DmkDBMSs: TDmkDBMSs; DmkDBSrc: TDmkDBSrc;
              Tabela: String): Integer;
    // Utils
    function IndiceDeTextoEmArray(Campo: String; Lista: array of String): Integer;
    // Fim Utils
  public
    { Public declarations }
    procedure AtualizaInformacoes(Memo: TMemo; PB: TProgressBar; Texto: String);
    function  AtualizaTabelaLocal(Tabela: String;
              Form: TForm; Memo: TMemo; PB: TProgressBar;
              DelFlds: array of String; DelVals: array of Variant;
              TextoSQL, SQLExtraDel: String): Boolean;
    procedure FreeRec(var aRecord);
    function  SQLArrRec_IDInt(Form: TForm; TextoSQL: String; var Res:
              TTabIDInt): Boolean;
    function  SQLArrRec_IDStr(Form: TForm; TextoSQL: String; var Res:
              TTabIDStr): Boolean;
    function  SQLXML_LeXML(Form: TForm; SQLType: TSQLType; TextoSQL: String; (*Tabela: String;*)
              DelFlds: array of String; DelVals: array of Variant;
              UsoXMLQuery: TUsoXMLQuery; var ArrXMLSQLins: TArrXMLSQLins): Boolean;
    function  XMLSQL_ExecutaQuery(Form: TForm; SQLType: TSQLType;
              TextoSQL: String): Integer;
  end;
var
  DmkDAC: TUnDmkDAC;

//const

implementation

uses UnMyObjects;

{ TUnDmkDAC }

procedure TUnDmkDAC.AtualizaInformacoes(Memo: TMemo; PB: TProgressBar; Texto: String);
begin
  Memo.Text := Texto + sLineBreak + Memo.Text;
  MyObjects.IncProgress(PB);
end;

function TUnDmkDAC.AtualizaTabelaLocal(Tabela: String;
  Form: TForm; Memo: TMemo; PB: TProgressBar;
  DelFlds: array of String; DelVals: array of Variant;
  TextoSQL, SQLExtraDel: String): Boolean;
var
  //XMLDados: String;
  ArrXMLSQLins: TArrXMLSQLins;
  Registros: Integer;
  Txt: String;
begin
  Result := False;
  //XMLDados := DmkDAC.SQLXML_QueryXML(Self, TextoSQL);
  if SQLXML_LeXML(
  Form, stPsq, TextoSQL, (*Tabela,*) DelFlds, DelVals, uxqSQLite, ArrXMLSQLins) then
  begin
    if High(DelFlds) > -1 then
      DmkRDBMs.SQLDelSimple(Form, dbsrcLocalServer, Tabela, DelFlds, DelVals,
        SQLExtraDel);
    Registros :=
      DmkDAC.SQLArrRec_Ins(Form, PB, ArrXMLSQLins, dbmsSQLite, dbsrcLocalServer, Tabela);
    Result := True;
{
    case Registros of
      0 : Txt := 'Nenhum registro inclu�do/alterado.';
      1 : Txt := 'Um registro inclu�do/alterado.';
      else Txt := 'Registros inclu�dos/alterados: ' + Geral.FF0(Registros);
    end;
}
    Txt := Geral.FF0(Registros);
    AtualizaInformacoes(Memo, PB, Tabela + ' > ' + Txt);
      //AtualizaInformacoes(Tabela + ' > Nenhum registro retornado do servidor.');
      //Geral.MB_Erro('ERRO! Imposs�vel criar elemento ra�z do XML!');
  end;
end;

procedure TUnDmkDAC.FreeRec(var aRecord);
begin
  //Finalize(aRecord);
  FillChar(aRecord, SizeOf(aRecord), 0);
end;

function TUnDmkDAC.IndiceDeTextoEmArray(Campo: String;
  Lista: array of String): Integer;
var
  I: Integer;
  Txt: String;
begin
  Result := -1;
  Txt := LowerCase(Campo);
  for I := Low(Lista) to High(Lista) do
  begin
    if Txt = LowerCase(Lista[I]) then
    begin
      Result := I;
      Exit;
    end;
  end;
end;

function TUnDmkDAC.SQLArrRec_Ins(Form: TForm; PB: TProgressBar; ArrXMLSQLins: TArrXMLSQLins;
  DmkDBMSs: TDmkDBMSs; DmkDBSrc: TDmkDBSrc; Tabela: String): Integer;
var
  I, J: Integer;
  Campos, Valores: String;
begin
  PB.Max := High(ArrXMLSQLins) + 1;
  Result := -1; // -1 = Erro
  try
    for I := Low(ArrXMLSQLins) to High(ArrXMLSQLins) do
    begin
      MyObjects.IncProgress(PB);
      Campos  := '';
      Valores := '';
      //
      for J := Low(ArrXMLSQLins[I]) to High(ArrXMLSQLins[I]) do
      begin
        Campos := Campos + ', ' +  ArrXMLSQLins[I][J][0];
        Valores := Valores + ', ' + #39 + ArrXMLSQLins[I][J][1] + #39;
      end;
      Campos := Campos.Substring(2);
      Valores := Valores.Substring(2);
      if DmkRDBMs.SQLInsertValues(
      Form,  DmkDBMSs, DmkDBSrc, Tabela, Campos, Valores) then
        Result := Result + 1;
    end;
    Result := Result + 1;  // Zero caso n�o tenha erro
  except
    on E: Exception do
      Geral.MB_Erro('ERRO na procedure "UnDmkDAC.SQLArrRec_Ins"' +
      sLineBreak + E.Message);
  end;
end;

function TUnDmkDAC.SQLArrRec_IDInt(Form: TForm; TextoSQL: String;
  var Res: TTabIDInt): Boolean;
var
  I, J: Integer;
  Registros: TTabIDInt;
  Registro: TFldIDInt;
  ArrXMLSQLins: TArrXMLSQLins;
begin
  Result := False;
  Res:= nil;
  //
  if SQLXML_LeXML(Form, stPsq, TextoSQL, [], [], uxqNoSQL, ArrXMLSQLins) then
  try
    try
      Registros := TTabIDInt.Create;
      for I := Low(ArrXMLSQLins) to High(ArrXMLSQLins) do
      begin
        for J := Low(ArrXMLSQLins[I]) to High(ArrXMLSQLins[I]) do
        begin
          New(Registro);
          Registro.Value := Geral.IMV(ArrXMLSQLins[I][J][1]);
          Registros.Add(Registro);
        end;
      end;
      Res := Registros;
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro na fun��o "UnDmkDAC.SQLArrRec_IDInt":' +
          sLineBreak + E.Message);
      end;
    end;
  finally
    FreeRec(ArrXMLSQLins);
  end;
end;

function TUnDmkDAC.SQLArrRec_IDStr(Form: TForm; TextoSQL: String;
  var Res: TTabIDStr): Boolean;
var
  I, J: Integer;
  Registros: TTabIDStr;
  Registro: TFldIDStr;
  ArrXMLSQLins: TArrXMLSQLins;
begin
  Result := False;
  Res:= nil;
  //
  if SQLXML_LeXML(Form, stPsq, TextoSQL, [], [], uxqNoSQL, ArrXMLSQLins) then
  try
    try
      Registros := TTabIDStr.Create;
      for I := Low(ArrXMLSQLins) to High(ArrXMLSQLins) do
      begin
        for J := Low(ArrXMLSQLins[I]) to High(ArrXMLSQLins[I]) do
        begin
          New(Registro);
          Registro.Value := ArrXMLSQLins[I][J][1];
          Registros.Add(Registro);
        end;
      end;
      Res := Registros;
      Result := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro na fun��o "UnDmkDAC.SQLArrRec_IDStr":' +
          sLineBreak + E.Message);
      end;
    end;
  finally
    FreeRec(ArrXMLSQLins);
  end;
end;

procedure TUnDmkDAC.SQLXML_DomToTree(const XmlNode: IXMLNode;
  var ArrXMLSQLins: TArrXMLSQLins; var DelFlds: array of String;
  var DelVals: array of Variant);
var
  //I,
  K, N, Linha: Integer;
  //AttrNode: IXMLNode;
begin
  K := -1;
  //N := 0;
  // skip text nodes and other special cases
  if not (XmlNode.NodeType = ntElement) then
  begin
    //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
    //Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  // add the node itself
  if XmlNode.IsTextElement then
  begin
    if IndiceDeTextoEmArray(XmlNode.NodeName, DelFlds) >= 0 then
    begin
      K := K + 1;
      DelVals[K] := XmlNode.NodeValue;
    end;
    Linha := High(ArrXMLSQLins);
    N := High(ArrXMLSQLins[Linha]) + 2;
    SetLength(ArrXMLSQLins[Linha], N);
    ArrXMLSQLins[Linha][N - 1][0] := XmlNode.NodeName;
(*
{$IFDEF MSWINDOWS}
    ArrXMLSQLins[Linha][N - 1][1] := System.Utf8ToString(XmlNode.NodeValue);
{$ELSE}
*)
    ArrXMLSQLins[Linha][N - 1][1] := XmlNode.NodeValue;
//{$ENDIF}
  end;
(*
  // add attributes
  for I := 0 to xmlNode.AttributeNodes.Count - 1 do
  begin
    AttrNode := xmlNode.AttributeNodes.Nodes[I];
  end;
*)
  // add each child node
  (*if XmlNode.HasChildNodes then
    for I := 0 to xmlNode.ChildNodes.Count - 1 do
      DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);*)
end;

function TUnDmkDAC.SQLXML_LeXML(Form: TForm; SQLType: TSQLType;
  TextoSQL: String; (*Tabela: String;*)
  DelFlds: array of String; DelVals: array of Variant;
  UsoXMLQuery: TUsoXMLQuery; var ArrXMLSQLins: TArrXMLSQLins): Boolean;
  procedure InfoErro(Tipo, Campo, Valor: String);
  begin
    Geral.MB_Erro(Tipo + ' inv�lido: ' + Campo + ' = ' + Valor);
  end;
var
  XMLDocument1: TXMLDocument;
  NodeQuery, NodeRegistro: IXMLNode;
  RecordCount, RecNo, Campo: IXMLNode;
  //I, J,
  N, K, M: Integer;
  //Txt,
  XMLDados: String;
begin
  Result := False;
  N := 0;
  XMLDados := SQLXML_QueryXML(Form, SQLType, TextoSQL);
  if XMLDados.IndexOf('MySQL Error.') > -1 then
    Geral.MB_Erro('MySQL ERROR:' + sLineBreak +
    TextoSQL);
  SetLength(ArrXMLSQLins, 0);
  //
  XMLDocument1 := TXMLDocument.Create(Form);
  XMLDocument1.LoadFromXML(XMLDados);
  NodeQuery := XMLDocument1.DocumentElement.ChildNodes.First;
  if NodeQuery <> nil then
  begin
    RecordCount := XMLDocument1.DocumentElement.AttributeNodes.Nodes[0];
    if RecordCount <> nil then
    begin
      if RecordCount.NodeName = 'RecordCount' then
      begin
        K := Geral.IMV(RecordCount.Text);
        NodeRegistro := XMLDocument1.DocumentElement.ChildNodes.First;
        while NodeRegistro <> nil do
        begin
          N := N + 1;
          RecNo := NodeRegistro.AttributeNodes.Nodes[0];
          if RecNo <> nil then
          begin
            if RecNo.NodeName = 'RecNo' then
            begin
              if N = Geral.IMV(RecNo.Text) then
              begin
                SetLength(ArrXMLSQLins, N);
                for M := Low(DelVals) to High(DelVals) do
                  DelVals[M] := null;
                //
                Campo := NodeRegistro.ChildNodes.First;
                while Campo <> nil do
                begin
                  SQLXML_DomToTree(Campo, ArrXMLSQLins, DelFlds, DelVals);
                  Campo := Campo.NextSibling;
                  //
                end;
              end else
                InfoErro('Valor de atributo', RecNo.NodeName, RecNo.Text);
              NodeRegistro := NodeRegistro.NextSibling;
            end else
              InfoErro('Tipo de Atributo', RecNo.NodeName, RecNo.Text);
          end;
        end;
        if K <> N then
          Geral.MB_Erro('Quantidade de registros informados (' + Geral.FF0(K) +
          ') difere dos inclu�dos (' + Geral.FF0(N) + ')');
        Result := True;
      end else
      if RecordCount.NodeName = 'mysql_affected_rows' then
      begin
        SetLength(ArrXMLSQLins, 1);
        SetLength(ArrXMLSQLins[0], 1);
        ArrXMLSQLins[0][0][0] := RecordCount.NodeName;
        ArrXMLSQLins[0][0][1] := RecordCount.Text;
        Result := True;
      end else
        InfoErro('Tipo de atributo', RecordCount.NodeName, RecordCount.Text);
    end;
  end else
  begin
{
    RecordCount := XMLDocument1.DocumentElement.AttributeNodes[0];
    if RecordCount <> nil then
    begin
      //RecNo := RecordCount.AttributeNodes.Nodes[0];
      if RecordCount.NodeName = 'RecordCount' then
      begin
        K := Geral.IMV(RecordCount.Text);
        if K = 0 then
          Result := True // > Registros = 0
        else
          Geral.MB_Info(XMLDados);

      end else
        InfoErro('Tipo de atributo', RecordCount.NodeName, RecordCount.Text);
    end;
}
  end;
end;

function TUnDmkDAC.SQLXML_QueryXML(Form: TForm; SQLType: TSQLType; TextoSQL: String): String;
const
  Metodo = 'REST_Dmk_ObtemResultadoSQL';
var
  //ResMsgCod
  AplicativoSolicitante, IDUsuarioSolicitante,
  Var_Token, Var_UserId, Token, Titulo, SQL, Resposta, TipoSQL: String;
  //
  //Liga(*, Txt*): String;
begin
  Result := '';
  //Self.Cursor := crHourGlass;
  //try
    Var_UserId := VAR_WEB_USER_ID;
    Var_Token  := VAR_WEB_TOKEN_DMK;
    //
    if Var_UserId = '' then
      Var_UserId := VAR_API_ID_USUARIO;
    if Var_Token = '' then
      Var_Token := VAR_API_TOKEN;
    //
    AplicativoSolicitante := 'AplicativoSolicitante=' + Geral.FF0(CO_DMKID_APP);
    IDUsuarioSolicitante := 'IDUsuarioSolicitante=' + Geral.FF0(Var_UserId);
    Token := 'Token=' + Var_Token;
    Titulo := 'Titulo=TesteSQL';
    TipoSQL := 'SQLType=' + Geral.FF0(Integer(SQLType));
    SQL := Geral.Substitui('SQL=' + TextoSQL, #10, ' ');
    SQL := Geral.Substitui(SQL, #13, ' ');
    //
    Resposta := WebReq.SQLArrPOST_TxtStr(Form, Metodo, [AplicativoSolicitante,
    IDUsuarioSolicitante, Token, Titulo, TipoSQL, SQL
    ]);
    //
    Result := Resposta;
  //finally
    //Self.Cursor := crDefault;
  //end;
end;


function TUnDmkDAC.XMLSQL_ExecutaQuery(Form: TForm; SQLType: TSQLType;
TextoSQL: String): Integer;
var
  //I, J: Integer;
  //Registros: TTabIDInt;
  //Registro: TFldIDInt;
  ArrXMLSQLins: TArrXMLSQLins;
begin
  Result := -1;
  if SQLXML_LeXML(Form, SQLType, TextoSQL, [], [], uxqNoSQL, ArrXMLSQLins) then
  try
    try
      Result := Geral.IMV(ArrXMLSQLins[0][0][1]);
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Erro na fun��o "UnDmkDAC.XMLSQL_ExecutaQuery":' +
          sLineBreak + E.Message);
      end;
    end;
  finally
    FreeRec(ArrXMLSQLins);
  end;
end;

end.
