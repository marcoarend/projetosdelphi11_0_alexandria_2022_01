unit UnFMX_Fotos;

interface

uses  System.SysUtils, System.UITypes, System.Variants, (*System.IOUtils,
  FMX.Forms, FMX.Dialogs, FMX.Edit, System.MaskUtils,*)
  UnDmkEnums, UnFMX_Geral, UnFMX_DmkForms;

type
  TUnFMX_Fotos = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function MostraFormTiraFoto(const IDFoto: String; const AcrescentaDataNoNome:
             Boolean; const SQLExec: String; var NomeArq: String; IdTabela:
             Integer): Boolean;
  end;
var
  FMX_Fotos: TUnFMX_Fotos;

implementation

uses TiraFoto;

{ TUnFMX_Fotos }

function TUnFMX_Fotos.MostraFormTiraFoto(const IDFoto: String; const
  AcrescentaDataNoNome: Boolean; const SQLExec: String; var NomeArq: String;
  IdTabela: Integer):
  Boolean;
begin
  Result := False;
  if FMX_DmkForms.CriaFm_AllOS0(TFmTiraFoto, FmTiraFoto, fcmOnlyCreate, True,
  False) then
  begin
    FmTiraFoto.FIDTabela := IDTabela;
    FmTiraFoto.FIDFoto   := IDFoto;
    FmTiraFoto.FSQLExec  := SQLExec;
    FmTiraFoto.FNomeArq  := NomeArq;
    //
    FMX_DmkForms.ShowModal(FmTiraFoto);
    //
    Result := True;
  end;
end;

end.
