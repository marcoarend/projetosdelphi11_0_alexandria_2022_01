unit TiraFoto;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.NetEncoding, System.IOUtils,
{$IfDef MSWINDOWS}
  Winapi.ShellAPI,
{$EndIf}
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Consts,
  FMX.DialogService.Async,
  FMX.Edit, FMX.Objects, System.Actions, FMX.ActnList, FMX.Layouts,
  FMX.StdActns, FMX.MediaLibrary.Actions;

type
  TFmTiraFoto = class(TForm)
    Image1: TImage;
    ActionList1: TActionList;
    ToolBar1: TToolBar;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    TakePhotoFromCameraAction1: TTakePhotoFromCameraAction;
    TakePhotoFromLibraryAction1: TTakePhotoFromLibraryAction;
    ShowShareSheetAction1: TShowShareSheetAction;
    Button7: TButton;
    procedure TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
    procedure TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
    procedure ShowShareSheetAction1BeforeExecute(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function  Base64FromBitmap(Bitmap: TBitmap): string;
    procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
    function  MostraESalvaImagemObtida(Image: TBitmap): Boolean;
  public
    { Public declarations }
    FIDTabela: Integer;
    FIDFoto, FSQLExec, FNomeArq: String;
  end;

var
  FmTiraFoto: TFmTiraFoto;

implementation

uses UnFMX_Geral, UnFMX_DmkForms, UnGrl_Consts, UnApp_PF,
  Module;

{$R *.fmx}

function TFmTiraFoto.Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
  Encoding: TBase64Encoding;
begin
  Input := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Encode(Input, Output);
        Result := Output.DataString;
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFmTiraFoto.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Input := TStringStream.Create(Base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TFmTiraFoto.Button7Click(Sender: TObject);
begin
{$IfDef MSWINDOWS}
  ShellExecute(0, 'OPEN', PChar('microsoft.windows.camera:'), '', '', 1);
{$EndIf}
end;

procedure TFmTiraFoto.FormCreate(Sender: TObject);
begin
{$IfDef MSWINDOWS}
  Button7.Visible := True;
{$Else}
  Button7.Visible := False;
{$EndIf}
end;

function TFmTiraFoto.MostraESalvaImagemObtida(Image: TBitmap): Boolean;
var
  Agora: TDateTime;
  DataHora, SQL, PNomeARq, PDataHora: String;
begin
  Agora := Now();
  Image1.Bitmap.Assign(Image);
  //
  if FSQLExec <> emptyStr then
  begin
    DataHora := FormatDateTime('_YYYYMMDD_HHNNSS', Agora);
    FNomeArq := FIDFoto + DataHora + '.jpg';
    FNomeArq := System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetCameraPath, FNomeArq);
    Image1.Bitmap.SaveToFile(FNomeArq);
    //
    PDataHora := '"' + CO_JOKE_FOTO_DATAHORA + '"';
    PNomeArq  := '"' + CO_JOKE_FOTO_ARQNOME + '"';
    SQL := FSQLExec;
    SQL := StringReplace(SQL, PNomeArq, FMX_Geral.VariavelToStringAD(FNomeArq), []);
    SQL := StringReplace(SQL, PDataHora, FMX_Geral.VariavelToStringAD(Agora), []);
    Dmod.QrUpd.Connection := Dmod.AllDB;
    Dmod.QrUpd.SQL.Text := SQL;
    Dmod.QrUpd.ExecSQL;
    //
    App_PF.AcoesFinaisDeTirarFoto();
    FMX_DmkForms.FechaFm_AllOS0(FmTiraFoto);
  end;
end;

procedure TFmTiraFoto.ShowShareSheetAction1BeforeExecute(Sender: TObject);
begin
  ShowShareSheetAction1.Bitmap.Assign(Image1.Bitmap);
end;

procedure TFmTiraFoto.TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
begin
  //Configurado no Object Inspector em design time:
  //TakePhotoFromCameraAction1.NeedSaveToAlbum := True;
  //TakePhotoFromCameraAction1.Editable := True;
  //
  // ActionList ser� executado!
  //
  //Image1.Bitmap.Assign(Image);
  MostraESalvaImagemObtida(Image);
end;

procedure TFmTiraFoto.TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
begin
  //Configurado no Object Inspector em design time:
  //TakePhotoFromLibraryAction1.Editable := True;
  //
  // ActionList ser� executado!
  //
  //Image1.Bitmap.Assign(Image);
  MostraESalvaImagemObtida(Image);
end;
// Usado exemplo de:
//http://docwiki.embarcadero.com/RADStudio/Rio/en/Taking_and_Sharing_Pictures_and_Text_Using_Action_Lists
//http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Taking_and_Sharing_Pictures_and_Text_Using_Action_Lists

end.
