unit ChmOcoPux;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Layouts, FMX.Controls.Presentation,
  UnGrl_Vars, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, UnFMX_Grl_Vars;

type
  TFmChmOcoPux = class(TForm)
    VSB: TVertScrollBox;
    Layout1: TLayout;
    Button1: TButton;
    Button2: TButton;
    Image1: TImage;
    QrChmOcoCab: TFDQuery;
    QrChmOcoCabNO_WhoGerEnti: TWideStringField;
    QrChmOcoCabNome: TStringField;
    QrChmOcoCabNO_ONDE: TWideStringField;
    QrChmOcoCabCodigo: TIntegerField;
    QrChmOcoCabKndNome: TShortintField;
    QrChmOcoCabChmTitOco: TIntegerField;
    QrChmOcoCabDescricao: TStringField;
    QrChmOcoCabWhoGerEnti: TIntegerField;
    QrChmOcoCabKndIsWhoDo: TIntegerField;
    QrChmOcoCabWhoDoQtdSel: TIntegerField;
    QrChmOcoCabWhoDoQtdMin: TIntegerField;
    QrChmOcoCabWhoDoQtdMax: TIntegerField;
    QrChmOcoCabKndOnde: TShortintField;
    QrChmOcoCabCodOnde: TIntegerField;
    QrChmOcoCabOndeDescr: TStringField;
    QrChmOcoCabPorqueDescr: TStringField;
    QrChmOcoCabQuandoIni: TDateTimeField;
    QrChmOcoCabQuandoFim: TDateTimeField;
    QrChmOcoCabChmHowCad: TIntegerField;
    QrChmOcoCabHowManyUnd: TStringField;
    QrChmOcoCabHowManyQtd: TFloatField;
    QrChmOcoCabHowMuchMoed: TStringField;
    QrChmOcoCabHowMuchValr: TFloatField;
    QrChmOcoCabPriAbrtDtH: TDateTimeField;
    QrChmOcoCabLstRAbrDtH: TDateTimeField;
    QrChmOcoCabLstFechDtH: TDateTimeField;
    QrChmOcoCabImportancia: TShortintField;
    QrChmOcoCabUrgencia: TShortintField;
    QrChmOcoCabBRK_Nome: TStringField;
    QrChmOcoCabQuandoIni_TXT: TWideStringField;
    QrChmOcoCabQuandoFim_TXT: TWideStringField;
    QrChmOcoCabNO_ChmHowCad: TWideStringField;
    LayMenu: TLayout;
    RectFundo: TRectangle;
    RectMenuBase: TRectangle;
    LaMenuTitulo: TLabel;
    LaMenuSubTitulo: TLabel;
    LaMenuExcluir: TLabel;
    LaMenuLine1: TLine;
    LaMenuLine2: TLine;
    LaMenuVisualizar: TLabel;
    LaMenuDesiste: TLabel;
    QrChmOcoPux: TFDQuery;
    QrChmOcoPuxCodigo: TIntegerField;
    QrChmOcoPuxNome: TStringField;
    QrChmOcoPuxTxtLocal: TStringField;
    QrChmOcoPuxDtHrPush: TDateTimeField;
    QrChmOcoPuxDtHrLido: TDateTimeField;
    QrChmOcoPuxDtHrCatx: TDateTimeField;
    QrChmOcoPuxQuandoIni: TDateTimeField;
    QrChmOcoPuxQuandoFim: TDateTimeField;
    LayTitle: TLayout;
    RecTitle: TRectangle;
    LinBlue: TLine;
    LinGrey: TLine;
    LaTitle: TLabel;
    QrChmOcoCabCloseUser: TIntegerField;
    QrChmOcoCabCloseDtHr: TDateTimeField;
    QrChmOcoCabDESCRI_NO_LINEBREAK: TWideStringField;
    QrChmOcoCabDoneDtHr: TDateTimeField;
    procedure LabelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LaMenuDesisteClick(Sender: TObject);
    procedure LaMenuVisualizarClick(Sender: TObject);
    procedure LaMenuExcluirClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure ExcluiChmOcoPux();
    procedure LabelPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
    procedure VisualizarChamado();
  public
    { Public declarations }
    FCodigo: Integer;
    FDtHrLido: TDateTime;
    //
    procedure CarregaItensExistentes();
    procedure EscondeOMenu();
    procedure ExibeOMenu();
  end;

var
  FmChmOcoPux: TFmChmOcoPux;

implementation

uses Module, UnGrl_Geral, UnApp_Jan, UnGrl_DmkDB, UnFMX_DmkProcFunc, UnFMX_DmkObjects;

{$R *.fmx}


procedure TFmChmOcoPux.CarregaItensExistentes();
  procedure CarregaItemAtual();
  var
    CR: TRectangle;
    L: TText;
    TmpImg: TImage;
    sPeriodo: String;
    T: array of string;
  begin
    sPeriodo := 'Quando: ' + Grl_Geral.FDT(QrChmOcoPuxQuandoIni.Value, 0);
    if QrChmOcoPuxQuandoFim.Value > 2 then
      sPeriodo := sPeriodo + ' at� ' + Grl_Geral.FDT(QrChmOcoPuxQuandoFim.Value, 0);
    T := [QrChmOcoPuxNome.Value,
      'Onde: ' + QrChmOcoPuxTxtLocal.Value,
      sPeriodo];
    //////////////////////////////////////////////////////
    CR := TRectangle.Create(Self);
    CR.Parent := VSB;
    //CR.TagString := sLISTADO;
    FMX_DmkObjects.LabelInRectangleNameRec(CR);
    CR.Align := TAlignLayout.alTop;
    //CR.CalloutPosition := TCalloutPosition.cpLeft;
    CR.Margins.Top := 10;
    CR.Margins.Bottom := 10;
    CR.Margins.Right := 5;
    //CR.Height := 75;

    CR.Fill.Color := VAR_LaItemTexto_Back_Color;
(*
    CR.Stroke.Color := TAlphaColors.LightSteelblue;
    CR.Stroke.Kind := TBrushKind.Solid;
*)
    CR.Stroke.Kind := TBrushKind.None;

    CR.XRadius := 10;
    CR.YRadius := 10;
    CR.Margins.Left := 5;
    CR.Margins.Left := 5;
    CR.Margins.Top  := 10;
    CR.Margins.Left := 5;
    //CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;

    L := TText.Create(Self);
    L.Parent := CR;
    L.Align := TAlignLayout.alClient;
      //'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
    L.Tag       := QrChmOcoPuxCodigo.Value;
    L.TagFloat  := QrChmOcoPuxDtHrLido.Value;
(*
    L.Text      := QrChmOcoPuxNome.Value + sLineBreak +
                   'Onde: ' + QrChmOcoPuxTxtLocal.Value + sLineBreak +
                   //'Enviado: ' + Grl_Geral.FDT(QrChmOcoPuxDtHrCatx.Value, 0);
                   'Quando: ' + Grl_Geral.FDT(QrChmOcoPuxQuandoIni.Value, 0);
*)
    L.Margins.Top    := 5;
    L.Margins.Bottom := 5;
    L.Margins.Left   := 20;
    L.Margins.Right  := 5;

    L.WordWrap := True;
    L.AutoSize := True;
    L.OnPaint := LabelPaint;

    L.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
    L.TextSettings.HorzAlign := TTextAlign.Leading;
    L.HitTest := True;
    L.OnClick := LabelClick;

    CR.Align := TAlignLayout.alBottom;
    CR.Align := TAlignLayout.alTop;


{
    FMX_DmkPF.AlturaRectRunTimeCreateInRectangle(VSB, CR, L, [
      QrChmOcoPuxNome.Value,
      'Onde: ' + QrChmOcoPuxTxtLocal.Value,
      sPeriodo
    ]);
}
    //
    CR.Height := FMX_DmkObjects.AlturaRectRunTimeCreateInRectangle(VSB, CR, L, T);
    L.Text    := Grl_Geral.ATS(T);

  end;
var
  I: Integer;
  Comp: TRectangle;
begin
  for I := FmChmOcoPux.ComponentCount - 1 downto 0 do
  begin
    if FmChmOcoPux.Components[I] is TRectangle then
    begin
      Comp := TRectangle(FmChmOcoPux.Components[I]);
      if FMX_DmkPF.IsLabelRunTimeCreateInRectangle(Comp) then
      begin
        {$IFDEF AUTOREFCOUNT}
          Comp.Parent := nil;
          Comp.Owner.RemoveComponent(Comp);
        {$ELSE}
          Comp.Free;
        {$ENDIF}
        Comp := nil;
        end;
    end;
  end;

  Dmod.ReopenChmOcoPux(QrChmOcoPux, EmptyStr);
  //
  QrChmOcoPux.First;
  while not QrChmOcoPux.Eof do
  begin
    CarregaItemAtual();
    //
    QrChmOcoPux.Next;
  end;

end;

procedure TFmChmOcoPux.EscondeOMenu();
begin
  with FmChmOcoPux do
  begin
    RectMenuBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height + 20,
      0.3,
      TAnimationType.InOut,
      TInterpolationType.Circular);

    RectFundo.AnimateFloat('Opacity', 0, 0.6);

    TThread.CreateAnonymousThread(procedure
    begin
      sleep(800);
      LayMenu.Visible := False;
    end).Start;
  end;
end;

procedure TFmChmOcoPux.LabelClick(Sender: TObject);
var
  LabelN: TLabel;
  Codigo: Integer;
  DtHrLido: TDateTime;
begin
  FMX_DmkPF.VibrarComoBotao();
  LabelN := TLabel(Sender);

  FCodigo    := LabelN.Tag;
  FDtHrLido  := LabelN.TagFloat;

  ExibeOMenu();
end;

procedure TFmChmOcoPux.LabelPaint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
  //TRectangle(TText(Sender).Parent).Height := TText(Sender).Height + 20;
end;

procedure TFmChmOcoPux.LaMenuDesisteClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  EscondeOMenu();
end;

procedure TFmChmOcoPux.LaMenuExcluirClick(Sender: TObject);
var
  sPergunta: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  sPergunta := 'Deseja realmete limpar esta notifica��o? ' + sLineBreak +
  'O chamado de ocorr�ncia continuar� na base da dados!';
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      //MR := AResult;
      case AResult of
        mrYes: ExcluiChmOcoPux();
        mrNo: Grl_Geral.MB_Info('Exclus�o n�o realizada!');
        mrCancel: Grl_Geral.MB_Info('Exclus�o Abortada!');
      end;
    end);
  EscondeOMenu();
end;

procedure TFmChmOcoPux.LaMenuVisualizarClick(Sender: TObject);
begin
  VisualizarChamado();
  EscondeOMenu();
end;

procedure TFmChmOcoPux.VisualizarChamado();
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  Dmod.ReopenChmOcoCad(QrChmOcoCab, EmptyStr);
  if QrChmOcoCab.RecordCount > 0 then
  begin
    if QrChmOcoCab.Locate('Codigo', FCodigo, []) then
    begin
      Dmod.InformaLeituraNotificacao(FCodigo, FDtHrLido);
      App_Jan.MostraFormChmOcoCad(
       // C�digo do chamado
        QrChmOcoCabCodigo.Value,
        // T�tulo
        QrChmOcoCabNome.Value,
        // Quem Chamou
        QrChmOcoCabNO_WhoGerEnti.Value,
        // Descri��o do que fazer
        QrChmOcoCabDescricao.Value,
        // Motivo
        QrChmOcoCabPorqueDescr.Value,
        // T�tulo Procedimento (Fluxo)
        QrChmOcoCabNO_ChmHowCad.Value,
        // Per�odo de execu��o
        QrChmOcoCabQuandoIni.Value,
        QrChmOcoCabQuandoFim.Value,
        // Tem itens
        QrChmOcoCabChmHowCad.Value,
        // Finalizado?
        QrChmOcoCabDoneDtHr.Value,
        //
        nil
        );
    end else
      Grl_Geral.MB_Aviso('Chamado ainda n�o baixado do servidor! [2]');
  end else
  begin
    Grl_Geral.MB_Aviso('Chamado ainda n�o baixado do servidor! [1]');
  end;
end;

procedure TFmChmOcoPux.ExcluiChmOcoPux();
begin
  if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.AllDB, [
  'DELETE FROM chmocopux ',
  'WHERE Codigo=' + Grl_Geral.FF0(FCodigo),
  EmptyStr]) then
  begin
    CarregaItensExistentes();
    Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, EmptyStr);
  end;
end;

procedure TFmChmOcoPux.ExibeOMenu();
begin
  with FmChmOcoPux do
  begin
    RectMenuBase.Width := LayMenu.Width - 16;
    RectMenuBase.Position.X := 8;
    RectMenuBase.Position.Y := LayMenu.Height + 20;

    LayMenu.Visible := True;
    RectFundo.Opacity := 0;
    RectFundo.AnimateFloat('Opacity', 0.4, 0.2);

    RectMenuBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height - 270 - 8,
      0.5,
      TAnimationType.InOut,
      TInterpolationType.Circular);
  end;
end;

procedure TFmChmOcoPux.FormActivate(Sender: TObject);
begin
  VSB.Repaint;
end;

procedure TFmChmOcoPux.FormCreate(Sender: TObject);
begin
  VSB.Align := TAlignLayout.Client;
end;

procedure TFmChmOcoPux.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

end.
