unit Teste_ChmOco;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Layouts, FMX.Controls.Presentation,
  UnGrl_Vars, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFmTeste_ChmOco = class(TForm)
    VSB: TVertScrollBox;
    Layout1: TLayout;
    Button1: TButton;
    Button2: TButton;
    Image1: TImage;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrChmOcoCab: TFDQuery;
    QrChmOcoCabNO_WhoGerEnti: TWideStringField;
    QrChmOcoCabNome: TStringField;
    QrChmOcoCabNO_ONDE: TWideStringField;
    QrChmOcoCabCodigo: TIntegerField;
    QrChmOcoCabKndNome: TShortintField;
    QrChmOcoCabChmTitOco: TIntegerField;
    QrChmOcoCabDescricao: TStringField;
    QrChmOcoCabWhoGerEnti: TIntegerField;
    QrChmOcoCabKndIsWhoDo: TIntegerField;
    QrChmOcoCabWhoDoQtdSel: TIntegerField;
    QrChmOcoCabWhoDoQtdMin: TIntegerField;
    QrChmOcoCabWhoDoQtdMax: TIntegerField;
    QrChmOcoCabKndOnde: TShortintField;
    QrChmOcoCabCodOnde: TIntegerField;
    QrChmOcoCabOndeDescr: TStringField;
    QrChmOcoCabPorqueDescr: TStringField;
    QrChmOcoCabQuandoIni: TDateTimeField;
    QrChmOcoCabQuandoFim: TDateTimeField;
    QrChmOcoCabChmHowCad: TIntegerField;
    QrChmOcoCabHowManyUnd: TStringField;
    QrChmOcoCabHowManyQtd: TFloatField;
    QrChmOcoCabHowMuchMoed: TStringField;
    QrChmOcoCabHowMuchValr: TFloatField;
    QrChmOcoCabPriAbrtDtH: TDateTimeField;
    QrChmOcoCabLstRAbrDtH: TDateTimeField;
    QrChmOcoCabLstFechDtH: TDateTimeField;
    QrChmOcoCabImportancia: TShortintField;
    QrChmOcoCabUrgencia: TShortintField;
    QrChmOcoCabBRK_Nome: TStringField;
    QrChmOcoCabQuandoIni_TXT: TWideStringField;
    QrChmOcoCabQuandoFim_TXT: TWideStringField;
    QrChmOcoCabNO_ChmHowCad: TWideStringField;
    LayMenu: TLayout;
    RectFundo: TRectangle;
    RectBase: TRectangle;
    Label2: TLabel;
    Label3: TLabel;
    LaExcluir: TLabel;
    Line1: TLine;
    Line2: TLine;
    ToolBarTitle: TToolBar;
    Label1: TLabel;
    LaVisualizar: TLabel;
    LaDesiste: TLabel;
    QrChmOcoPux: TFDQuery;
    QrChmOcoPuxCodigo: TIntegerField;
    QrChmOcoPuxNome: TStringField;
    QrChmOcoPuxTxtLocal: TStringField;
    QrChmOcoPuxDtHrPush: TDateTimeField;
    QrChmOcoPuxDtHrLido: TDateTimeField;
    QrChmOcoPuxDtHrCatx: TDateTimeField;
    QrChmOcoPuxQuandoIni: TDateTimeField;
    QrChmOcoPuxQuandoFim: TDateTimeField;
    Line3: TLine;
    Line4: TLine;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure LabelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LaDesisteClick(Sender: TObject);
    procedure LaVisualizarClick(Sender: TObject);
    procedure LaExcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure _CarregaItensExistentes1();
    procedure _CarregaItensExistentes2();
    procedure ExcluiChmOcoPux();
    procedure LabelPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
    procedure VisualizarChamado();
  public
    { Public declarations }
    FCodigo: Integer;
    FDtHrLido: TDateTime;
    //
    procedure CarregaItensExistentes();
    procedure EscondeOMenu();
    procedure ExibeOMenu();
  end;

var
  FmTeste_ChmOco: TFmTeste_ChmOco;

implementation

uses Module, UnGrl_Geral, UnApp_Jan, UnGrl_DmkDB, UnFMX_DmkProcFunc;

{$R *.fmx}

const
  sListado = '_Listado_';

procedure TFmTeste_ChmOco.Button1Click(Sender: TObject);
var
  CR: TCalloutRectangle;
  L: TText;
  TmpImg: TImage;
begin

  CR := TCalloutRectangle.Create(Self);
  CR.Parent := VSB;
  CR.Align := TAlignLayout.alTop;
  CR.CalloutPosition := TCalloutPosition.cpLeft;
  CR.Margins.Top := 10;
  CR.Margins.Bottom := 10;
  CR.Margins.Right := 5;
  CR.Height := 75;

  CR.Fill.Color := VAR_DMK_THEMES_COLOR_HiLigh;
  CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;

  L := TText.Create(Self);
  L.Parent := CR;
  L.Align := TAlignLayout.alClient;
  L.Text := 'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
  L.Margins.Left := 15;
  L.Margins.Right := 5;
  L.Width := CR.Width-20;

  L.WordWrap := True;
  L.AutoSize := True;
  L.OnPaint := LabelPaint;

  L.TextSettings.FontColor := VAR_DMK_THEMES_COLOR_Text;
end;

procedure TFmTeste_ChmOco.Button2Click(Sender: TObject);
var
CR: TCalloutRectangle;
L: TText;
TmpImg: TImage;
begin
  CR := TCalloutRectangle.Create(Self);
  CR.Parent := VSB;
  CR.Align := TAlignLayout.alTop;
  CR.CalloutPosition := TCalloutPosition.cpRight;
  CR.Margins.Top := 10;
  CR.Margins.Bottom := 10;
  CR.Margins.Left := 5;
  CR.Height := 75;

  CR.Fill.Color := VAR_DMK_THEMES_COLOR_HiLigh;
  CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;


  L := TText.Create(Self);
  L.Parent := CR;
  L.Align := TAlignLayout.alClient;
  L.Text := 'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
  L.Margins.Right := 15;
  L.Margins.Left := 5;
  L.Width := CR.Width-20;

  L.WordWrap := True;
  L.AutoSize := True;
  L.OnPaint := LabelPaint;

  L.TextSettings.FontColor := VAR_DMK_THEMES_COLOR_Text;

{
  TmpImg := TImage.Create(Self);
  TmpImg.Parent := CR;
  TmpImg.Align := TAlignLayout.alLeft;
  TmpImg.Bitmap.Assign(Image1.Bitmap);
  TmpImg.Width := 75;
}
end;

procedure TFmTeste_ChmOco.CarregaItensExistentes();
begin
  _CarregaItensExistentes2();
end;

procedure TFmTeste_ChmOco._CarregaItensExistentes1();
  procedure CarregaItemAtual();
  var
    CR: TCalloutRectangle;
    L: TText;
    TmpImg: TImage;

  begin

    CR := TCalloutRectangle.Create(Self);
    CR.Parent := VSB;
    CR.Align := TAlignLayout.alTop;
    CR.CalloutPosition := TCalloutPosition.cpLeft;
    CR.Margins.Top := 10;
    CR.Margins.Bottom := 10;
    CR.Margins.Right := 5;
    CR.Height := 75;

    CR.Fill.Color := VAR_DMK_THEMES_COLOR_HiLigh;
    CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;

    L := TText.Create(Self);
    L.Parent := CR;
    L.Align := TAlignLayout.alClient;
      //'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
    L.Tag       := QrChmOcoPuxCodigo.Value;
    L.TagFloat  := QrChmOcoPuxDtHrLido.Value;
    L.Text      := QrChmOcoPuxNome.Value + sLineBreak +
                   'Onde: ' + QrChmOcoPuxTxtLocal.Value + sLineBreak +
                   //'Enviado: ' + Grl_Geral.FDT(QrChmOcoPuxDtHrCatx.Value, 0);
                   'Quando: ' + Grl_Geral.FDT(QrChmOcoPuxQuandoIni.Value, 0);
    if QrChmOcoPuxQuandoFim.Value > 2 then
    begin
      L.Text := L.Text + ' at� ' + Grl_Geral.FDT(QrChmOcoPuxQuandoFim.Value, 0);
    end;
    L.Margins.Left := 20;
    L.Margins.Right := 5;

    L.WordWrap := True;
    L.AutoSize := True;
    L.OnPaint := LabelPaint;

    L.TextSettings.FontColor := VAR_DMK_THEMES_COLOR_Text;
    L.TextSettings.HorzAlign := TTextAlign.Leading;
    L.HitTest := True;
    L.OnClick := LabelClick;

  end;
begin
  Dmod.ReopenChmOcoPux(QrChmOcoPux, EmptyStr);
  //
  QrChmOcoPux.First;
  while not QrChmOcoPux.Eof do
  begin
    CarregaItemAtual();
    //
    QrChmOcoPux.Next;
  end;

end;

procedure TFmTeste_ChmOco._CarregaItensExistentes2();
  procedure CarregaItemAtual();
  var
    CR: TRectangle;
    L: TText;
    TmpImg: TImage;

  begin

    CR := TRectangle.Create(Self);
    CR.Parent := VSB;
    CR.TagString := sLISTADO;
    CR.Align := TAlignLayout.alTop;
    //CR.CalloutPosition := TCalloutPosition.cpLeft;
    CR.Margins.Top := 10;
    CR.Margins.Bottom := 10;
    CR.Margins.Right := 5;
    CR.Height := 75;

    CR.Fill.Color := TAlphaColors.White;
(*
    CR.Stroke.Color := TAlphaColors.LightSteelblue;
    CR.Stroke.Kind := TBrushKind.Solid;
*)
    CR.Stroke.Kind := TBrushKind.None;

    CR.XRadius := 10;
    CR.YRadius := 10;
    CR.Margins.Left := 5;
    CR.Margins.Left := 5;
    CR.Margins.Top  := 10;
    CR.Margins.Left := 5;
    //CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;

    L := TText.Create(Self);
    L.Parent := CR;
    L.Align := TAlignLayout.alClient;
      //'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
    L.Tag       := QrChmOcoPuxCodigo.Value;
    L.TagFloat  := QrChmOcoPuxDtHrLido.Value;
    L.Text      := QrChmOcoPuxNome.Value + sLineBreak +
                   'Onde: ' + QrChmOcoPuxTxtLocal.Value + sLineBreak +
                   //'Enviado: ' + Grl_Geral.FDT(QrChmOcoPuxDtHrCatx.Value, 0);
                   'Quando: ' + Grl_Geral.FDT(QrChmOcoPuxQuandoIni.Value, 0);
    if QrChmOcoPuxQuandoFim.Value > 2 then
    begin
      L.Text := L.Text + ' at� ' + Grl_Geral.FDT(QrChmOcoPuxQuandoFim.Value, 0);
    end;
    L.Margins.Top    := 5;
    L.Margins.Bottom := 5;
    L.Margins.Left   := 20;
    L.Margins.Right  := 5;

    L.WordWrap := True;
    L.AutoSize := True;
    L.OnPaint := LabelPaint;

    L.TextSettings.FontColor := $FF434343; // VAR_DMK_THEMES_COLOR_Text;
    L.TextSettings.HorzAlign := TTextAlign.Leading;
    L.HitTest := True;
    L.OnClick := LabelClick;

  end;
var
  I: Integer;
  Comp: TRectangle;
begin
  for I := FmTeste_ChmOco.ComponentCount - 1 downto 0 do
  begin
    if FmTeste_ChmOco.Components[I] is TRectangle then
    begin
      Comp := TRectangle(FmTeste_ChmOco.Components[I]);
      if Comp.TagString = sListado then
      begin
(*
        Comp.Destroy;
*)
        {$IFDEF AUTOREFCOUNT}
          Comp.Parent := nil;
          Comp.Owner.RemoveComponent(Comp);
        {$ELSE}
          Comp.Free;
        {$ENDIF}
        Comp := nil;
        end;
    end;
  end;

  Dmod.ReopenChmOcoPux(QrChmOcoPux, EmptyStr);
  //
  QrChmOcoPux.First;
  while not QrChmOcoPux.Eof do
  begin
    CarregaItemAtual();
    //
    QrChmOcoPux.Next;
  end;

end;

procedure TFmTeste_ChmOco.EscondeOMenu();
begin
  with FmTeste_ChmOco do
  begin
    RectBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height + 20,
      0.3,
      TAnimationType.InOut,
      TInterpolationType.Circular);

    RectFundo.AnimateFloat('Opacity', 0, 0.6);

    TThread.CreateAnonymousThread(procedure
    begin
      sleep(800);
      LayMenu.Visible := False;
    end).Start;
  end;
end;

procedure TFmTeste_ChmOco.LabelClick(Sender: TObject);
var
  LabelN: TLabel;
  Codigo: Integer;
  DtHrLido: TDateTime;
begin
  FMX_DmkPF.VibrarComoBotao();
  LabelN := TLabel(Sender);

  FCodigo    := LabelN.Tag;
  FDtHrLido  := LabelN.TagFloat;

  ExibeOMenu();
end;

procedure TFmTeste_ChmOco.LabelPaint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
  TCalloutRectangle(TText(Sender).Parent).Height := TText(Sender).Height + 20;
end;

procedure TFmTeste_ChmOco.LaDesisteClick(Sender: TObject);
begin
  EscondeOMenu();
end;

procedure TFmTeste_ChmOco.LaExcluirClick(Sender: TObject);
var
  sPergunta: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  sPergunta := 'Deseja realmete limpar esta notifica��o? ' + sLineBreak +
  'O chamado de ocorr�ncia continuar� na base da dados!';
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      //MR := AResult;
      case AResult of
        mrYes: ExcluiChmOcoPux();
        mrNo: Grl_Geral.MB_Info('Exclus�o n�o realizada!');
        mrCancel: Grl_Geral.MB_Info('Exclus�o Abortada!');
      end;
    end);
  EscondeOMenu();
end;

procedure TFmTeste_ChmOco.LaVisualizarClick(Sender: TObject);
begin
  VisualizarChamado();
  EscondeOMenu();
end;

procedure TFmTeste_ChmOco.VisualizarChamado();
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  Dmod.ReopenChmOcoCad(QrChmOcoCab);
  if QrChmOcoCab.RecordCount > 0 then
  begin
    if QrChmOcoCab.Locate('Codigo', FCodigo, []) then
    begin
      Dmod.InformaLeituraNotificacao(FCodigo, FDtHrLido);
      App_Jan.MostraFormChmOcoCad(
        // T�tulo
        QrChmOcoCabNome.Value,
        // Quem Chamou
        QrChmOcoCabNO_WhoGerEnti.Value,
        // Descri��o do que fazer
        QrChmOcoCabDescricao.Value,
        // Motivo
        QrChmOcoCabPorqueDescr.Value,
        // T�tulo Procedimento (Fluxo)
        QrChmOcoCabNO_ChmHowCad.Value,
        // Per�odo de execu��o
        QrChmOcoCabQuandoIni.Value,
        QrChmOcoCabQuandoFim.Value,
        // Tem itens
        QrChmOcoCabChmHowCad.Value
        );
    end;
  end else
  begin
    Grl_Geral.MB_Aviso('Chamado ainda n�o baixado do servidor!');
  end;
end;

procedure TFmTeste_ChmOco.ExcluiChmOcoPux();
begin
  if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.AllDB, [
  'DELETE FROM chmocopux ',
  'WHERE Codigo=' + Grl_Geral.FF0(FCodigo),
  EmptyStr]) then
  begin
    CarregaItensExistentes();
    Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, EmptyStr);
  end;
end;

procedure TFmTeste_ChmOco.ExibeOMenu();
begin
  with FmTeste_ChmOco do
  begin
    RectBase.Width := LayMenu.Width - 16;
    RectBase.Position.X := 8;
    RectBase.Position.Y := LayMenu.Height + 20;

    LayMenu.Visible := true;
    RectFundo.Opacity := 0;
    RectFundo.AnimateFloat('Opacity', 0.4, 0.2);

    RectBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height - 270 - 8,
      0.5,
      TAnimationType.InOut,
      TInterpolationType.Circular);
  end;
end;

procedure TFmTeste_ChmOco.FormCreate(Sender: TObject);
begin
{
  FmTeste_ChmOco.Fill.Color := TAlphaColors.White;
  FmTeste_ChmOco.Fill.Kind  := TBrushKind.Solid;
  FmTeste_ChmOco.ToolBar1.TintColor := TAlphaColors.Lightblue;

  LayMenu.Visible := False;
  FCodigo         := 0;
  FDtHrLido       := 0;
end;
}
end;

procedure TFmTeste_ChmOco.FormShow(Sender: TObject);
begin
(*  FmTeste_ChmOco.Fill.Color := TAlphaColors.White;
  FmTeste_ChmOco.Fill.Kind  := TBrushKind.Solid;
  FmTeste_ChmOco.ToolBar1.TintColor := TAlphaColors.Lightblue;
  //
*)
  LayMenu.Visible := False;
  FCodigo         := 0;
  FDtHrLido       := 0;
  CarregaItensExistentes();
  //
*)
end;

end.
