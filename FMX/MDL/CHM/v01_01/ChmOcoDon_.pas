//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit ChmOcoDon;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs,
  FMX.StdCtrls, FMX.Memo, FMX.Edit, FMX.Layouts, FMX.ListBox, FMX.Objects,
  FMX.Controls.Presentation, FMX.ScrollBox, UnDmkEnums;

type
  TFmChmOcoDon = class(TForm)
    VertScrollBox1: TVertScrollBox;
    MainLayout1: TLayout;
    Edit1: TEdit;
    ClearEditButton1: TClearEditButton;
    Layout1: TLayout;
    TitleLabel: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    LaNO_WhoGerEnti: TLabel;
    Layout3: TLayout;
    Label2: TLabel;
    MeDescricao: TMemo;
    Layout4: TLayout;
    Label6: TLabel;
    LaDataIni: TLabel;
    LaDataFim: TLabel;
    Label7: TLabel;
    Layout2: TLayout;
    Label1: TLabel;
    LaNome: TLabel;
    Layout5: TLayout;
    Label4: TLabel;
    MeObservacao: TMemo;
    Layout6: TLayout;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
                                        KeyboardVisible: Boolean;
                                        const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
                                       KeyboardVisible: Boolean;
                                       const Bounds: TRect);
    procedure FormFocusChanged(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FKBBounds: TRectF;
    FNeedOffset: Boolean;
    procedure CalcContentBoundsProc(Sender: TObject;
                                    var ContentBounds: TRectF);
    procedure RestorePosition;
    procedure UpdateKBBounds;
  public
    { Public declarations }
    FSQLType: TSQLType;
    FCodigo, FCtrlInMob, FChmOcoWho, FChmOcoEtp: Integer;
    //
  end;

var
  FmChmOcoDon: TFmChmOcoDon;

implementation

uses System.Math,
UnFMX_DmkProcFunc, UnGrl_DmkDB, UnGrl_Geral, (* UnOVS_Consts, UnApp_Jan,*)
  UnFMX_DmkForms,
  Module, ChmOcoCad;


{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TFmChmOcoDon.FormCreate(Sender: TObject);
begin
  VKAutoShowMode := TVKAutoShowMode.Always;
  VertScrollBox1.OnCalcContentBounds := CalcContentBoundsProc;
end;

procedure TFmChmOcoDon.Button1Click(Sender: TObject);
var
  DoneDtHr, CloseDtHr, Observacao: String;
  Codigo, CtrlInMob, Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
  SQLType: TSQLType;
begin
  SQLType        := FSQLType;
  Codigo         := FCodigo;
  CtrlInMob      := FCtrlInMob;
  //Controle       := FControle;
  ChmOcoEtp      := FChmOcoEtp;
  ChmOcoWho      := FChmOcoWho;
  Observacao     := MeObservacao.Text;
  DoneDtHr       := Grl_Geral.FDT(Now(), 109);
  CloseUser      := 0;
  CloseDtHr      := '0000-00-00 00:00:00';

  //
  CtrlInMob := Grl_DmkDB.GetNxtCodigoInt('chmocodon', 'CtrlInMob', SQLType, CtrlInMob);
  //ou > ? := UMyMod.BPGS1I32('chmocodon', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
  'Codigo', (*'Controle',*) 'ChmOcoEtp',
  'ChmOcoWho', 'Observacao',
  'DoneDtHr', 'CloseUser', 'CloseDtHr'], [
  'CtrlInMob'], [
  Codigo, (*Controle,*) ChmOcoEtp,
  ChmOcoWho, Observacao,
  DoneDtHr, CloseUser, CloseDtHr], [
  CtrlInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    FmChmOcoCad.ReopenChmOcoEtp(CtrlInMob);
    FMX_DmkForms.FechaFm_AllOS0(FmChmOcoDon);
  end;
end;

procedure TFmChmOcoDon.CalcContentBoundsProc(Sender: TObject;
                                       var ContentBounds: TRectF);
begin
  if FNeedOffset and (FKBBounds.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom,
                                2 * ClientHeight - FKBBounds.Top);
  end;
end;

procedure TFmChmOcoDon.RestorePosition;
begin
  VertScrollBox1.ViewportPosition := PointF(VertScrollBox1.ViewportPosition.X, 0);
  MainLayout1.Align := TAlignLayout.Client;
  VertScrollBox1.RealignContent;
end;

procedure TFmChmOcoDon.UpdateKBBounds;
var
  LFocused : TControl;
  LFocusRect: TRectF;
begin
  FNeedOffset := False;
  if Assigned(Focused) then
  begin
    LFocused := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    LFocusRect.Offset(VertScrollBox1.ViewportPosition);
    if (LFocusRect.IntersectsWith(TRectF.Create(FKBBounds))) and
       (LFocusRect.Bottom > FKBBounds.Top) then
    begin
      FNeedOffset := True;
      MainLayout1.Align := TAlignLayout.Horizontal;
      VertScrollBox1.RealignContent;
      Application.ProcessMessages;
      VertScrollBox1.ViewportPosition :=
        PointF(VertScrollBox1.ViewportPosition.X,
               LFocusRect.Bottom - FKBBounds.Top);
    end;
  end;
  if not FNeedOffset then
    RestorePosition;
end;

procedure TFmChmOcoDon.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds;
end;

procedure TFmChmOcoDon.FormVirtualKeyboardHidden(Sender: TObject;
                                           KeyboardVisible: Boolean;
                                           const Bounds: TRect);
begin
  FKBBounds.Create(0, 0, 0, 0);
  FNeedOffset := False;
  RestorePosition;
end;

procedure TFmChmOcoDon.FormVirtualKeyboardShown(Sender: TObject;
                                          KeyboardVisible: Boolean;
                                          const Bounds: TRect);
begin
  FKBBounds := TRectF.Create(Bounds);
  FKBBounds.TopLeft := ScreenToClient(FKBBounds.TopLeft);
  FKBBounds.BottomRight := ScreenToClient(FKBBounds.BottomRight);
  UpdateKBBounds;
end;

end.
