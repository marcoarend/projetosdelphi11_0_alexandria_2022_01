unit ChmOcoCad;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.ScrollBox, FMX.Memo,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, System.Bindings.Outputs, Data.Bind.Components,
  Data.Bind.DBScope, Data.Bind.Grid, Fmx.Bind.Grid, Fmx.Bind.Editors,
  FMX.Objects, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, MultiDetailAppearanceU, FMX.ListView;

type
  TFmChmOcoCad = class(TForm)
    QrChmOcoDon: TFDQuery;
    LayControle: TLayout;
    QrChmOcoDonCodigo: TIntegerField;
    QrChmOcoDonDON_Controle: TIntegerField;
    QrChmOcoDonETP_Controle: TIntegerField;
    QrChmOcoDonDON_DoneDtHr: TDateTimeField;
    QrChmOcoDonDON_MobWebUp: TShortintField;
    QrChmOcoDonDON_CloseUser: TIntegerField;
    QrChmOcoDonDON_CLoseDtHr: TDateTimeField;
    QrChmOcoDonDON_Observacao: TStringField;
    QrChmOcoDonETP_Ordem: TIntegerField;
    QrChmOcoDonETP_Nome: TStringField;
    QrChmOcoDonETP_Descricao: TStringField;
    QrChmOcoDonETP_QuandoIni: TDateTimeField;
    QrChmOcoDonETP_QuandoFim: TDateTimeField;
    QrChmOcoDonETP_CloseUser: TIntegerField;
    QrChmOcoDonETP_CloseDtHr: TDateTimeField;
    QrChmOcoDonETP_DoneDtHr: TDateTimeField;
    QrChmOcoDonTEM_DESCR: TStringField;
    QrChmOcoDonFEITO: TStringField;
    QrChmOcoDonWHO_Controle: TIntegerField;
    VSB: TVertScrollBox;
    LayTitle: TLayout;
    RecTitle: TRectangle;
    LinBlue: TLine;
    LinGrey: TLine;
    LaTitle: TLabel;
    rect_base: TRectangle;
    LaNome: TLabel;
    LaTitNOme: TLabel;
    Layout8: TLayout;
    Layout9: TLayout;
    Layout10: TLayout;
    Rectangle5: TRectangle;
    LaTitPeriodo: TLabel;
    LaConcatPeriodo: TLabel;
    RecDataIni: TRectangle;
    LaDataIni: TLabel;
    Layout1: TLayout;
    RecDataFim: TRectangle;
    LaDataFim: TLabel;
    Rectangle1: TRectangle;
    LaNO_WhoGerEnti: TLabel;
    LaTitNO_WhoGerEnti: TLabel;
    Rectangle4: TRectangle;
    LaDescricao: TLabel;
    LaTitDescricao: TLabel;
    Rectangle7: TRectangle;
    LaPorqueDescr: TLabel;
    LaTitPorqueDescr: TLabel;
    Rectangle8: TRectangle;
    LaNO_ChmHowCad: TLabel;
    LaTitNO_ChmHowCad: TLabel;
    LVChmOcoCad: TListView;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    LayMenu: TLayout;
    RectFundo: TRectangle;
    RectMenuBase: TRectangle;
    LaMenuTitulo: TLabel;
    LaMenuSubTitulo: TLabel;
    LaMenuClickSim: TLabel;
    LaMenuLine1: TLine;
    LaMenuLine2: TLine;
    LaMenuTexto: TLabel;
    LaMenuClickNao: TLabel;
    Rectangle2: TRectangle;
    LaEncerrarChamado: TLabel;
    RectObservacao: TRectangle;
    MeObsEncerr: TMemo;
    RBRealizou_Nao: TRadioButton;
    RBRealizou_Sim: TRadioButton;
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrChmOcoDonCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure LaEncerrarChamadoClick(Sender: TObject);
    procedure LaMenuClickNaoClick(Sender: TObject);
    procedure LVChmOcoCadItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure LaMenuClickSimClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RBRealizou_SimClick(Sender: TObject);
    procedure RBRealizou_NaoClick(Sender: TObject);
  private
    { Private declarations }
    procedure FinalizaCahamadoDeOcorrencia();
    //
    procedure EscondeOMenu();
    procedure ExibeOMenu(Pergunta: String);
    procedure VSBClick(Sender: TObject);
  public
    { Public declarations }
    FChmOcoCad: Integer;
    FDoneDtHr: TDateTime;
    FQrCab: TFDQuery;
    //
    procedure ReopenChmOcoEtp(DON_Controle: Integer);
  end;

var
  FmChmOcoCad: TFmChmOcoCad;

implementation

uses UnFMX_DmkProcFunc,UnGrl_DmkDB, UnGrl_Geral, UnOVS_Consts, UnApp_Jan,
  UnGrl_Vars, UnFMX_DmkForms, UnDmkEnums, UnFMX_DmkObjects,
  //Temporario
  UnFMX_Grl_Vars,
  //Fim Temporario
  Module, ChmOcoDon;

{$R *.fmx}

procedure TFmChmOcoCad.EscondeOMenu();
begin
  with FmChmOcoCad do
  begin
    RectMenuBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height + 20,
      0.3,
      TAnimationType.InOut,
      TInterpolationType.Circular);

    RectFundo.AnimateFloat('Opacity', 0, 0.6);

    TThread.CreateAnonymousThread(procedure
    begin
      sleep(800);
      LayMenu.Visible := False;
    end).Start;
  end;
end;

procedure TFmChmOcoCad.ExibeOMenu(Pergunta: String);
var
  iBoundsH: Extended;
begin
  iBoundsH := FMX_DmkObjects.MenuFloatTopFromBounds(Self, RectMenuBase);
  //
  with FmChmOcoCad do
  begin
    LaMenuTexto.Text := Pergunta;
    RBRealizou_Sim.IsChecked := False;
    RBRealizou_Nao.IsChecked := False;
    MeObsEncerr.Text := '';
    //
    RectMenuBase.Width := LayMenu.Width - 16;
    RectMenuBase.Position.X := 8;
    RectMenuBase.Position.Y := LayMenu.Height + 20 + iBoundsH;

    LayMenu.Visible := True;
    RectFundo.Opacity := 0;
    RectFundo.AnimateFloat('Opacity', 0.4, 0.2);

    RectMenuBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height - RectMenuBase.Height - 8 - iBoundsH,
      0.5,
      TAnimationType.InOut,
      TInterpolationType.Circular);
  end;
end;

procedure TFmChmOcoCad.FinalizaCahamadoDeOcorrencia();
var
  Codigo, WhoDoMobile, MobUpWeb, Realizado: Integer;
  DoneDtHr, ObsEncerr: String;
begin
  Codigo      := FChmOcoCad;
  WhoDoMobile := VAR_COD_DEVICE_IN_SERVER;
  DoneDtHr    := Grl_Geral.FDT(Now(), 109);
  ObsEncerr   := MeObsEncerr.Text;
  if RBRealizou_Nao.IsChecked then
    Realizado := 0
  else
  if RBRealizou_Sim.IsChecked then
    Realizado := 1
  else
  begin
    Realizado := -1;
    Grl_Geral.MB_Aviso('Informe se o chamado foi realizado ou n�o!');
    Exit;
  end;
  if Realizado > -1 then
  begin
    MobUpWeb    := 0; // N�o
    //
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocowho', False, [
    'DoneDtHr', 'Realizado', 'ObsEncerr',
    'MobUpWeb'], [
    'Codigo', 'WhoDoMobile'], [
    DoneDtHr, Realizado, ObsEncerr,
    MobUpWeb], [
    Codigo, WhoDoMobile], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '',
    stMobile) then
    begin
      LayMenu.Visible := False;
      if FQrCab <> nil then
        Grl_DmkDB.RefreshQuery(FQrCab);

      FMX_DmkForms.FechaFm_AllOS0(FmChmOcoCad);
    end;
  end;
end;

procedure TFmChmOcoCad.FormCreate(Sender: TObject);
begin
  VSB.Align := TAlignLayout.Contents;
  LayMenu.Align := TAlignLayout.Contents;
  LayMenu.Visible := False;
(*
  FMX_dmkPF.CorrigeCorLabel(Label1);
  FMX_dmkPF.CorrigeCorLabel(Label2);
  FMX_dmkPF.CorrigeCorLabel(Label3);
  FMX_dmkPF.CorrigeCorLabel(Label4);
  FMX_dmkPF.CorrigeCorLabel(Label5);
  FMX_dmkPF.CorrigeCorLabel(Label6);
  FMX_dmkPF.CorrigeCorLabel(Label7);
  //
*)
  //GdChmOcoEtp.RowCount := 0;
  LVChmOcoCad.Height := 500;
end;

procedure TFmChmOcoCad.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmChmOcoCad.FormShow(Sender: TObject);
begin
  VSB.ViewportPosition := PointF(0(*x*),0(*Y*));
end;

procedure TFmChmOcoCad.LaEncerrarChamadoClick(Sender: TObject);
const
  sFinalizarAssimMesmo = 'Deseja finalizar assim mesmo este chamado de ocorr�ncia?';
var
  ChmOcoCab, EtapasNaoFinalizadas: Integer;
  sPergunta, Texto: String;

begin

  FMX_DmkPF.VibrarComoBotao();
  //
  EtapasNaoFinalizadas := 0;
  if Grl_DmkDB.QueryTemRegistros(QrChmOcoDon) then
  begin
    //
    QrChmOcoDon.First;
    while not QrChmOcoDon.Eof do
    begin
      if QrChmOcoDonDON_DoneDtHr.Value < 2 then
        EtapasNaoFinalizadas := EtapasNaoFinalizadas + 1;
      //
      QrChmOcoDon.Next;
    end;
    case EtapasNaoFinalizadas of
      0: Texto := '';
      1: Texto := 'Existe uma etapa n�o finalizada! ';
      else Texto := 'Existem ' + Grl_Geral.FF0(EtapasNaoFinalizadas) +
                    ' etapas n�o finalizadas! ';
    end;
    sPergunta := Texto + sFinalizarAssimMesmo;
  end else
  begin
    if FDoneDtHr > 2 then
      sPergunta := 'Este chamado j� foi finalizado! ' + sFinalizarAssimMesmo
    else
      sPergunta := 'Deseja realmete finalizar este chamado de ocorr�ncia?';
  end;
  //
  ExibeOMenu(sPergunta);
(*
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      //MR := AResult;
      case AResult of
        mrYes: FinalizaCahamadoDeOcorrencia();
        mrNo: Grl_Geral.MB_Info('Edi��o n�o realizada!');
        mrCancel: Grl_Geral.MB_Info('Edi��o Abortada!');
      end;
    end);
*)
end;

procedure TFmChmOcoCad.LaMenuClickNaoClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  EscondeOMenu();
end;

procedure TFmChmOcoCad.LaMenuClickSimClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  FinalizaCahamadoDeOcorrencia();
end;

procedure TFmChmOcoCad.LVChmOcoCadItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  if not Grl_DmkDB.QueryTemRegistros(QrChmOcoDon) then Exit;
  //
  FMX_DmkPF.VibrarComoBotao();
  //
  App_Jan.MostraFormChmOcoDon(QrChmOcoDonCodigo.Value,
  QrChmOcoDonDON_Controle.Value, QrChmOcoDonETP_Controle.Value,
  QrChmOcoDonWHO_Controle.Value,
  QrChmOcoDonETP_Nome.Value, LaNO_WhoGerEnti.Text,
  QrChmOcoDonETP_Descricao.Value, QrChmOcoDonETP_QuandoIni.Value,
  QrChmOcoDonETP_QuandoFim.Value, QrChmOcoDonDON_Observacao.Value);
end;

procedure TFmChmOcoCad.QrChmOcoDonCalcFields(DataSet: TDataSet);
begin
  if QrChmOcoDonETP_Descricao.Value <> EmptyStr then
    QrChmOcoDonTEM_DESCR.Value := '*'
  else
    QrChmOcoDonTEM_DESCR.Value := ' ';
  //
  if QrChmOcoDonDON_DoneDtHr.Value > 2 then
    QrChmOcoDonFEITO.Value := 'Item realizado'
  else
    QrChmOcoDonFEITO.Value := 'Item por realizar';
  //
end;

procedure TFmChmOcoCad.RBRealizou_NaoClick(Sender: TObject);
begin
  if RBRealizou_Nao.IsChecked then
    RBRealizou_Sim.IsChecked := False;
end;

procedure TFmChmOcoCad.RBRealizou_SimClick(Sender: TObject);
begin
  if RBRealizou_Sim.IsChecked then
    RBRealizou_Nao.IsChecked := False;
end;

procedure TFmChmOcoCad.ReopenChmOcoEtp(DON_Controle: Integer);
begin
  if FChmOcoCad <> 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoDon, Dmod.AllDB, [
(*
    'SELECT * ',
    'FROM chmocoetp ',
    'WHERE Codigo=' + Grl_Geral.FF0(FChmOcoCad),
    'ORDER BY Ordem, Controle ',
*)
    'SELECT don.ChmOcoWho WHO_Controle, ',
    'don.Codigo, don.Controle DON_Controle, ',
    'don.ChmOcoEtp ETP_Controle, don.DoneDtHr DON_DoneDtHr, ',
    'don.MobUpWeb DON_MobWebUp, don.CloseUser DON_CloseUser, ',
    'don.CLoseDtHr DON_CLoseDtHr, don.Observacao ',
    'DON_Observacao, etp.Ordem ETP_Ordem, etp.Nome ETP_Nome, ',
    'etp.Descricao ETP_Descricao, etp.QuandoIni ',
    'ETP_QuandoIni, etp.QuandoFim ETP_QuandoFim, ',
    'etp.CloseUser ETP_CloseUser, etp.CloseDtHr',
    'ETP_CloseDtHr, etp.DoneDtHr ETP_DoneDtHr',
    'FROM chmocodon don',
    'LEFT JOIN chmocoetp etp ON etp.Controle=don.ChmOcoEtp',
    'LEFT JOIN chmocowho who ON who.Controle=don.ChmOcoWho',
    'WHERE don.Codigo=' + Grl_Geral.FF0(FChmOcoCad),
    'AND who.WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
    'ORDER BY etp.Ordem, etp.Controle',
     //'ORDER BY Ordem, Controle ',
     EmptyStr]);
     if QrChmOcoDon.RecordCount > 0 then
     begin
       //GdChmOcoEtp.Visible := True;
       QrChmOcoDon.Locate('DON_Controle', DON_Controle, []);
     end;
  end else
    //GdChmOcoEtp.Visible := False;
  //
end;

procedure TFmChmOcoCad.VSBClick(Sender: TObject);
begin

end;

{/
Fazer coluna de sim/nao para tem descri��o
Fazer coluna de sim/nao para Feito


http://thuliobittencourt.com/blog/registrando-pushnotification/

///////////////////////////////////////////

///////////////////////////  V�deo 1 ///////////////////////////////////////////
https://www.youtube.com/watch?v=wJ64jkWPzlg

///////////////////////////  V�deo 2 ///////////////////////////////////////////
https://www.youtube.com/watch?v=2om39rq-Mms


Chave de API:
AIzaSyAr5MIKvNheeVOxyBydu4-Zjd32CIFRL5Q
N�mero do projeto:
205807968282

///////////////////////////  V�deo 3 ///////////////////////////////////////////
https://www.youtube.com/watch?v=0ncuzTy03iE
}


end.

