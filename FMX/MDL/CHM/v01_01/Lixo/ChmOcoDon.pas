unit ChmOcoDon;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.ScrollBox, FMX.Memo,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, System.Bindings.Outputs, Data.Bind.Components,
  Data.Bind.DBScope, Data.Bind.Grid, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkENums, FMX.Edit, math;

type
  TFmChmOcoDon = class(TForm)
    Layout1: TLayout;
    TitleLabel: TLabel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    VertScrollBox1: TVertScrollBox;
    MainLayout1: TLayout;
    Edit1: TEdit;
    ClearEditButton1: TClearEditButton;
    Layout2: TLayout;
    Label1: TLabel;
    LaNome: TLabel;
    Layout4: TLayout;
    Label6: TLabel;
    LaDataIni: TLabel;
    LaDataFim: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    LaNO_WhoGerEnti: TLabel;
    Layout3: TLayout;
    Label2: TLabel;
    MeDescricao: TMemo;
    Layout5: TLayout;
    Label4: TLabel;
    Memo1: TMemo;
    Layout6: TLayout;
    Button1: TButton;
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormFocusChanged(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
  private
    { Private declarations }
    FKBBounds: TRectF;
    FNeedOffset: Boolean;
    procedure CalcContentBoundsProc(Sender: TObject;
                                    var ContentBounds: TRectF);
    procedure RestorePosition;
    procedure UpdateKBBounds;
  public
    { Public declarations }
    FSQLType: TSQLType;
    FCodigo, FCtrlInMob, FChmOcoWho, FChmOcoEtp: Integer;
    //
    procedure ReopenChmOcoDon(Controle: Integer);
  end;

var
  FmChmOcoDon: TFmChmOcoDon;

implementation

uses UnFMX_DmkProcFunc, UnGrl_DmkDB, UnGrl_Geral, (* UnOVS_Consts, UnApp_Jan,*)
  UnFMX_DmkForms,
  Module, ChmOcoCad;

{$R *.fmx}

procedure TFmChmOcoDon.Button1Click(Sender: TObject);
var
  DoneDtHr, CloseDtHr: String;
  Codigo, CtrlInMob, Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
  SQLType: TSQLType;
begin
  SQLType        := FSQLType;
  Codigo         := FCodigo;
  CtrlInMob      := FCtrlInMob;
  //Controle       := FControle;
  ChmOcoEtp      := FChmOcoEtp;
  ChmOcoWho      := FChmOcoWho;
  DoneDtHr       := Grl_Geral.FDT(Now(), 109);
  CloseUser      := 0;
  CloseDtHr      := '0000-00-00 00:00:00';

  //
  CtrlInMob := Grl_DmkDB.GetNxtCodigoInt('chmocodon', 'CtrlInMob', SQLType, CtrlInMob);
  //ou > ? := UMyMod.BPGS1I32('chmocodon', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
  'Codigo', (*'Controle',*) 'ChmOcoEtp',
  'ChmOcoWho', 'DoneDtHr', 'CloseUser',
  'CloseDtHr'], [
  'CtrlInMob'], [
  Codigo, (*Controle,*) ChmOcoEtp,
  ChmOcoWho, DoneDtHr, CloseUser,
  CloseDtHr], [
  CtrlInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    FmChmOcoCad.ReopenChmOcoEtp(CtrlInMob);
    FMX_DmkForms.FechaFm_AllOS0(FmChmOcoDon);
  end;
end;

procedure TFmChmOcoDon.CalcContentBoundsProc(Sender: TObject;
  var ContentBounds: TRectF);
begin
  if FNeedOffset and (FKBBounds.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom,
                                2 * ClientHeight - FKBBounds.Top);
  end;
end;

procedure TFmChmOcoDon.FormCreate(Sender: TObject);
begin
  FSQLType := stIns;
  VertScrollBox1.Align := TAlignLayout.Contents;
  FMX_dmkPF.CorrigeCorLabel(Label1);
  FMX_dmkPF.CorrigeCorLabel(Label2);
  FMX_dmkPF.CorrigeCorLabel(Label3);
  FMX_dmkPF.CorrigeCorLabel(Label4);
  //FMX_dmkPF.CorrigeCorLabel(Label5);
  FMX_dmkPF.CorrigeCorLabel(Label6);
  FMX_dmkPF.CorrigeCorLabel(Label7);
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
  VKAutoShowMode := TVKAutoShowMode.Always;
  VertScrollBox1.OnCalcContentBounds := CalcContentBoundsProc;
//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//
end;

procedure TFmChmOcoDon.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds;
end;

procedure TFmChmOcoDon.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmChmOcoDon.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds.Create(0, 0, 0, 0);
  FNeedOffset := False;
  RestorePosition;
end;

procedure TFmChmOcoDon.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds := TRectF.Create(Bounds);
  FKBBounds.TopLeft := ScreenToClient(FKBBounds.TopLeft);
  FKBBounds.BottomRight := ScreenToClient(FKBBounds.BottomRight);
  UpdateKBBounds;
end;

procedure TFmChmOcoDon.ReopenChmOcoDon(Controle: Integer);
begin
{
  if FChmHowCad <> 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoEtp, Dmod.AllDB, [
    'SELECT * ',
    'FROM chmocoetp ',
    'WHERE Controle=' + Grl_Geral.FF0(FChmOcoCadCodigo),
    EmptyStr]);
  end;
}
end;

procedure TFmChmOcoDon.RestorePosition;
begin
  VertScrollBox1.ViewportPosition := PointF(VertScrollBox1.ViewportPosition.X, 0);
  MainLayout1.Align := TAlignLayout.Client;
  VertScrollBox1.RealignContent;
end;

procedure TFmChmOcoDon.UpdateKBBounds;
var
  LFocused : TControl;
  LFocusRect: TRectF;
begin
  FNeedOffset := False;
  if Assigned(Focused) then
  begin
    LFocused := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    LFocusRect.Offset(VertScrollBox1.ViewportPosition);
    if (LFocusRect.IntersectsWith(TRectF.Create(FKBBounds))) and
       (LFocusRect.Bottom > FKBBounds.Top) then
    begin
      FNeedOffset := True;
      MainLayout1.Align := TAlignLayout.Horizontal;
      VertScrollBox1.RealignContent;
      Application.ProcessMessages;
      VertScrollBox1.ViewportPosition :=
        PointF(VertScrollBox1.ViewportPosition.X,
               LFocusRect.Bottom - FKBBounds.Top);
    end;
  end;
  if not FNeedOffset then
    RestorePosition;
end;

{/
Fazer coluna de sim/nao para tem descri��o
Fazer coluna de sim/nao para Feito


http://thuliobittencourt.com/blog/registrando-pushnotification/

///////////////////////////////////////////

///////////////////////////  V�deo 1 ///////////////////////////////////////////
https://www.youtube.com/watch?v=wJ64jkWPzlg

///////////////////////////  V�deo 2 ///////////////////////////////////////////
https://www.youtube.com/watch?v=2om39rq-Mms


Chave de API:
AIzaSyAr5MIKvNheeVOxyBydu4-Zjd32CIFRL5Q
N�mero do projeto:
205807968282

///////////////////////////  V�deo 3 ///////////////////////////////////////////
https://www.youtube.com/watch?v=0ncuzTy03iE
}

end.
