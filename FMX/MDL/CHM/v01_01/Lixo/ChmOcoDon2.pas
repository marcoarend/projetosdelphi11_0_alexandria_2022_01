unit ChmOcoDon2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.ScrollBox, FMX.Memo,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, System.Bindings.Outputs, Data.Bind.Components,
  Data.Bind.DBScope, Data.Bind.Grid, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkENums, FMX.Edit, math;

type
  TFmChmOcoDon2 = class(TForm)
    Layout1: TLayout;
    TitleLabel: TLabel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    VertScrollBox1: TVertScrollBox;
    MainLayout1: TLayout;
    Layout2: TLayout;
    Label1: TLabel;
    LaNome: TLabel;
    Layout4: TLayout;
    Label6: TLabel;
    LaDataIni: TLabel;
    LaDataFim: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    Label3: TLabel;
    LaNO_WhoGerEnti: TLabel;
    Layout3: TLayout;
    Label2: TLabel;
    MeDescricao: TMemo;
    Layout5: TLayout;
    Label4: TLabel;
    MeObservacao: TMemo;
    Layout6: TLayout;
    Button1: TButton;
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSQLType: TSQLType;
    FCodigo, FDON_Controle, FETP_Controle, FWHO_Controle: Integer;
    //
    procedure ReopenChmOcoDon(Controle: Integer);
  end;

var
  FmChmOcoDon2: TFmChmOcoDon2;

implementation

uses UnFMX_DmkProcFunc, UnGrl_DmkDB, UnGrl_Geral, (* UnOVS_Consts, UnApp_Jan,*)
  UnFMX_DmkForms, UnGrl_Vars,
  Module, ChmOcoCad;

{$R *.fmx}

procedure TFmChmOcoDon2.Button1Click(Sender: TObject);
var
  //CloseDtHr,
  DoneDtHr, Observacao: String;
  // CloseUser,
  Codigo, Controle, ChmOcoEtp, ChmOcoWho, MobUpWeb: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stUpd;
  Codigo         := FCodigo;
  ChmOcoEtp      := FETP_Controle;
  ChmOcoWho      := FWHO_Controle;
  Controle       := FDON_Controle;
  DoneDtHr       := Grl_Geral.FDT(Now(), 109);
  //CloseUser      := ;
  //CloseDtHr      := ;
  Observacao     := MeObservacao.Text;
  MobUpWeb       := 0; // N�o
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
  'DoneDtHr', //'CloseUser', 'CloseDtHr',
  'Observacao', 'MobUpWeb'], [
  'Codigo', 'ChmOcoEtp', 'ChmOcoWho', 'Controle'], [
  DoneDtHr, //CloseUser, CloseDtHr,
  Observacao, MobUpWeb], [
  Codigo, ChmOcoEtp, ChmOcoWho, Controle], True(*False*),
  TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
{   N�o adianta fazer aqui! desfaz quando recarrega via download!
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocoetp', False, [
    'DoneDtHr'], [
    'Controle'], [
    DoneDtHr], [
    ChmOcoEtp], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile);
    //
}
    FmChmOcoCad.ReopenChmOcoEtp(FDON_Controle);
    FMX_DmkForms.FechaFm_AllOS0(FmChmOcoDon);
  end;
{
var
  DoneDtHr, CloseDtHr, Observacao: String;
  Codigo, CtrlInMob, Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
  SQLType: TSQLType;
begin
  SQLType        := FSQLType;
  Codigo         := FCodigo;
  CtrlInMob      := FCtrlInMob;
  //Controle       := FControle;
  ChmOcoEtp      := FChmOcoEtp;
  ChmOcoWho      := FChmOcoWho;
  Observacao     := MeObservacao.Text;
  DoneDtHr       := Grl_Geral.FDT(Now(), 109);
  CloseUser      := 0;
  CloseDtHr      := '0000-00-00 00:00:00';

  //
  CtrlInMob := Grl_DmkDB.GetNxtCodigoInt('chmocodon', 'CtrlInMob', SQLType, CtrlInMob);
  //ou > ? := UMyMod.BPGS1I32('chmocodon', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
  'Codigo', (*'Controle',*) 'ChmOcoEtp',
  'ChmOcoWho', 'Observacao', 'DoneDtHr',
  'CloseUser', 'CloseDtHr'], [
  'CtrlInMob'], [
  Codigo, (*Controle,*) ChmOcoEtp,
  ChmOcoWho, Observacao, DoneDtHr,
  CloseUser, CloseDtHr], [
  CtrlInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocoetp', False, [
    'DoneDtHr'], [
    'Controle'], [
    DoneDtHr], [
    ChmOcoEtp], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile);
    //
    FmChmOcoCad.ReopenChmOcoEtp(CtrlInMob);
    FMX_DmkForms.FechaFm_AllOS0(FmChmOcoDon);
  end;
}
end;

procedure TFmChmOcoDon2.FormCreate(Sender: TObject);
begin
  FSQLType := stIns;
  VertScrollBox1.Align := TAlignLayout.Contents;
  FMX_dmkPF.CorrigeCorLabel(Label1);
  FMX_dmkPF.CorrigeCorLabel(Label2);
  FMX_dmkPF.CorrigeCorLabel(Label3);
  FMX_dmkPF.CorrigeCorLabel(Label4);
  //FMX_dmkPF.CorrigeCorLabel(Label5);
  FMX_dmkPF.CorrigeCorLabel(Label6);
  FMX_dmkPF.CorrigeCorLabel(Label7);
end;

procedure TFmChmOcoDon2.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmChmOcoDon2.ReopenChmOcoDon(Controle: Integer);
begin
{
  if FChmHowCad <> 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoEtp, Dmod.AllDB, [
    'SELECT * ',
    'FROM chmocoetp ',
    'WHERE Controle=' + Grl_Geral.FF0(FChmOcoCadCodigo),
    EmptyStr]);
  end;
}
end;

{/
Fazer coluna de sim/nao para tem descri��o
Fazer coluna de sim/nao para Feito


http://thuliobittencourt.com/blog/registrando-pushnotification/

///////////////////////////////////////////

///////////////////////////  V�deo 1 ///////////////////////////////////////////
https://www.youtube.com/watch?v=wJ64jkWPzlg

///////////////////////////  V�deo 2 ///////////////////////////////////////////
https://www.youtube.com/watch?v=2om39rq-Mms


Chave de API:
AIzaSyAr5MIKvNheeVOxyBydu4-Zjd32CIFRL5Q
N�mero do projeto:
205807968282

///////////////////////////  V�deo 3 ///////////////////////////////////////////
https://www.youtube.com/watch?v=0ncuzTy03iE
}

end.
