//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit ChmOcoCab;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Gestures, FMX.StdCtrls,
  FMX.Edit, FMX.ExtCtrls, FMX.TabControl, FMX.Layouts, FMX.Memo, FMX.ListBox, FMX.VirtualKeyboard,FMX.Menus,FMX.Platform,
  System.Actions, FMX.ActnList, System.Math, FMX.DateTimeCtrls,
  FMX.Controls.Presentation, FMX.ScrollBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  FMX.Grid.Style, FMX.Grid, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnGrl_Vars;


type
  TFmChmOcoCab = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    TabItem5: TTabItem;
    infoAddress: TEdit;
    infoTelephone: TEdit;
    infoEmail: TEdit;
    Memo1: TMemo;
    efirstName: TEdit;
    edInstitution: TEdit;
    edAdmissionDate: TComboBox;
    edCity: TEdit;
    edGraduationDate: TComboBox;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    ListBoxItem7: TListBoxItem;
    ListBoxItem8: TListBoxItem;
    ListBoxItem9: TListBoxItem;
    ListBoxItem10: TListBoxItem;
    ListBoxItem11: TListBoxItem;
    ListBoxItem12: TListBoxItem;
    ListBoxItem13: TListBoxItem;
    ListBoxItem14: TListBoxItem;
    weEmpName: TEdit;
    weCity: TEdit;
    weOccupiedJob: TEdit;
    weFrom: TComboBox;
    ListBoxItem18: TListBoxItem;
    ListBoxItem19: TListBoxItem;
    ListBoxItem20: TListBoxItem;
    ListBoxItem21: TListBoxItem;
    ListBoxItem22: TListBoxItem;
    weTo: TComboBox;
    ListBoxItem23: TListBoxItem;
    ListBoxItem24: TListBoxItem;
    ListBoxItem25: TListBoxItem;
    ListBoxItem26: TListBoxItem;
    ListBoxItem27: TListBoxItem;
    Button1: TButton;
    ActionList1: TActionList;
    Button2: TButton;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ToolBar1: TToolBar;
    Label2: TLabel;
    NameList: TListBox;
    firstName: TListBoxItem;
    LastName: TListBoxItem;
    PersonalInfoList: TListBox;
    Address: TListBoxItem;
    Phone: TListBoxItem;
    Email: TListBoxItem;
    BirthDate: TListBoxItem;
    EducationList: TListBox;
    Institution: TListBoxItem;
    City: TListBoxItem;
    AdmissionDate: TListBoxItem;
    GraduationDate: TListBoxItem;
    WorkList: TListBox;
    Employer: TListBoxItem;
    EmployerCity: TListBoxItem;
    FromDate: TListBoxItem;
    ToDate: TListBoxItem;
    CurrentJob: TListBoxItem;
    eLastName: TEdit;
    VertScrollBox1: TVertScrollBox;
    MainLayout1: TLayout;
    infoDate: TDateEdit;
    PreviousTabAction: TPreviousTabAction;
    NextTabAction: TNextTabAction;
    TitleAction: TControlAction;
    GestureManager1: TGestureManager;
    QrChmOcoCab: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    GrChmOcoCab: TGrid;
    LinkGridToDataSourceBindSourceDB12: TLinkGridToDataSource;
    QrChmOcoCabNO_ChmHowCad: TStringField;
    QrChmOcoCabNO_WhoGerEnti: TWideStringField;
    QrChmOcoCabNome: TStringField;
    QrChmOcoCabNO_ONDE: TWideStringField;
    QrChmOcoCabCodigo: TIntegerField;
    QrChmOcoCabKndNome: TShortintField;
    QrChmOcoCabChmTitOco: TIntegerField;
    QrChmOcoCabDescricao: TStringField;
    QrChmOcoCabWhoGerEnti: TIntegerField;
    QrChmOcoCabKndIsWhoDo: TIntegerField;
    QrChmOcoCabWhoDoQtdSel: TIntegerField;
    QrChmOcoCabWhoDoQtdMin: TIntegerField;
    QrChmOcoCabWhoDoQtdMax: TIntegerField;
    QrChmOcoCabKndOnde: TShortintField;
    QrChmOcoCabCodOnde: TIntegerField;
    QrChmOcoCabOndeDescr: TStringField;
    QrChmOcoCabPorqueDescr: TStringField;
    QrChmOcoCabQuandoIni: TDateTimeField;
    QrChmOcoCabQuandoFim: TDateTimeField;
    QrChmOcoCabChmHowCad: TIntegerField;
    QrChmOcoCabHowManyUnd: TStringField;
    QrChmOcoCabHowManyQtd: TFloatField;
    QrChmOcoCabHowMuchMoed: TStringField;
    QrChmOcoCabHowMuchValr: TFloatField;
    QrChmOcoCabPriAbrtDtH: TDateTimeField;
    QrChmOcoCabLstRAbrDtH: TDateTimeField;
    QrChmOcoCabLstFechDtH: TDateTimeField;
    QrChmOcoCabImportancia: TShortintField;
    QrChmOcoCabUrgencia: TShortintField;
    QrChmOcoCabBRK_Nome: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    procedure UpdateMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormFocusChanged(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure TabControl1Gesture(Sender: TObject; const EventInfo: TGestureEventInfo; var Handled: Boolean);
    procedure QrChmOcoCabCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure GrChmOcoCabCellClick(const Column: TColumn; const Row: Integer);

  private
    FService: IFMXVirtualKeyboardToolbarService;
    FKBBounds: TRectF;
    FNeedOffset: Boolean;
    procedure CalcContentBoundsProc(Sender: TObject;
                                var ContentBounds: TRectF);
    procedure UpdateKBBounds;
    procedure RestorePosition;
    function  FindListBox(const AObj: TFmxObject): TListBox;
    function  GetIsFilled(const AList: TListBox): Boolean;
    //
    // Meu
    procedure ReopenChmOcoCad();
  public
    { Public declarations }
  end;

var
  FmChmOcoCab: TFmChmOcoCab;

implementation

uses UnFMX_DmkProcFunc, UnGrl_DmkDB, UnGrl_Geral, UnOVS_Consts,
  Module;

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

function TFmChmOcoCab.FindListBox(const AObj: TFmxObject): TListBox;
var
  Obj: TFmxObject;
begin
  Result := nil;
  if AObj.ChildrenCount > 0 then
  begin
    for Obj in AObj.Children do
    begin
      if Obj is TListBox then
        Result := TListBox(Obj)
      else
        Result := FindListBox(Obj);

      if Result <> nil then
        Exit(Result);
    end;
  end;
end;

function TFmChmOcoCab.GetIsFilled(const AList: TListBox): Boolean;
var
  I: Integer;
  Item: TListBoxItem;
  Obj: TFmxObject;
begin
  Result := True;
  for I := 0 to AList.Count - 1 do
  begin
    Item := AList.ItemByIndex(I);
    for Obj in Item.Children do
    begin
      if Obj is TEdit then
        Result := TEdit(Obj).Text <> '';
      if Obj is TDateEdit then
        Result := not TDateEdit(Obj).IsEmpty;
      if Obj is TComboBox then
        Result := TComboBox(Obj).ItemIndex >= 0;

      if not Result then
        Exit(Result);
    end;
    if not Result then
      Exit(Result);
  end;
end;

procedure TFmChmOcoCab.GrChmOcoCabCellClick(const Column: TColumn;
  const Row: Integer);
begin
  if (QrChmOcoCab.RecordCount > 0)  then
  begin
    FMX_DmkPF.VibrarComoBotao();
    TabControl1.Next;
  end;
end;

procedure TFmChmOcoCab.QrChmOcoCabCalcFields(DataSet: TDataSet);
begin
  QrChmOcoCabBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrChmOcoCab, 1, QrChmOcoCabNome.Value);
end;

procedure TFmChmOcoCab.FormCreate(Sender: TObject);
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardToolbarService, IInterface(FService)) then
  begin
    FService.SetToolbarEnabled(True);
    FService.SetHideKeyboardButtonVisibility(True);
  end;
  VertScrollBox1.OnCalcContentBounds := CalcContentBoundsProc;
end;

procedure TFmChmOcoCab.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
var
  LTab: TTabItem;
  LList: TListBox;
begin
  LTab := TabControl1.ActiveTab;

  if Action = NextTabAction then
  begin
    LList := FindListBox(LTab);
    if LList <> nil then
      NextTabAction.Enabled := GetIsFilled(LList);
  end;

  if Action = TitleAction then
    TitleAction.Text := LTab.Text;

  if LTab = TabItem5 then
      UpdateMemo;
end;

procedure TFmChmOcoCab.CalcContentBoundsProc(Sender: TObject; var ContentBounds: TRectF);
begin
  if FNeedOffset and (FKBBounds.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom,
                                2 * ClientHeight - FKBBounds.Top);
  end;
end;

procedure TFmChmOcoCab.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds;
end;

procedure TFmChmOcoCab.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
(*
  if Key = vkHardwareBack then
  begin
    TabControl1.Previous;
    Key := 0;
  end;
*)
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmChmOcoCab.FormShow(Sender: TObject);
begin
  ReopenChmOcoCad();
end;

procedure TFmChmOcoCab.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds.Create(0, 0, 0, 0);
  FNeedOffset := False;
  RestorePosition;
end;

procedure TFmChmOcoCab.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds := TRectF.Create(Bounds);
  FKBBounds.TopLeft := ScreenToClient(FKBBounds.TopLeft);
  FKBBounds.BottomRight := ScreenToClient(FKBBounds.BottomRight);
  UpdateKBBounds;
end;

procedure TFmChmOcoCab.UpdateMemo;
begin
  Memo1.Lines.Clear;
  Memo1.Lines.Add('Personal information');
  Memo1.Lines.Add('Name: ' + eFirstName.Text + ' ' + eLastName.Text);
  Memo1.Lines.Add('Address: ' + infoAddress.Text);
  Memo1.Lines.Add('Telephone: ' + infoTelephone.Text);
  Memo1.Lines.Add('E-mail: ' + infoEmail.Text);
  Memo1.Lines.Add('Date of birth:' + infoDate.Text);
  Memo1.Lines.Add('');

  Memo1.Lines.Add('Education');
  Memo1.Lines.Add(edInstitution.Text + ', ' + edCity.Text + ', ' +
                  edAdmissionDate.Selected.Text + '-' + edGraduationDate.Selected.Text);
  Memo1.Lines.Add('');

  Memo1.Lines.Add('Work experience');
  Memo1.Lines.Add(weOccupiedJob.Text + ' at ' +  weEmpName.Text + ', ' + weCity.Text + ', ' +
                weFrom.Selected.Text + '-' + weTo.Selected.Text );
end;

procedure TFmChmOcoCab.UpdateKBBounds;
var
  LFocused : TControl;
  LFocusRect: TRectF;
begin
  FNeedOffset := False;
  if Assigned(Focused) then
  begin
    LFocused := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    LFocusRect.Offset(VertScrollBox1.ViewportPosition);
    if (LFocusRect.IntersectsWith(TRectF.Create(FKBBounds))) and
       (LFocusRect.Bottom > FKBBounds.Top) then
    begin
      FNeedOffset := True;
      MainLayout1.Align := TAlignLayout.Horizontal;
      VertScrollBox1.RealignContent;
      Application.ProcessMessages;
      VertScrollBox1.ViewportPosition :=
        PointF(VertScrollBox1.ViewportPosition.X,
               LFocusRect.Bottom - FKBBounds.Top);
    end;
  end;
  if not FNeedOffset then
    RestorePosition;
end;

procedure TFmChmOcoCab.ReopenChmOcoCad;
begin
  Grl_DmkDB.AbreSQLQuery0(QrChmOcoCab, Dmod.AllDB, [
  'SELECT chc.Nome NO_ChmHowCad, /**sen.Login NO_UserLog,**/ ',
  ' ',
  '/**IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome) NO_WhoGerEnti,**/   ',
  'CASE (wge.Tipo) ',
  '  WHEN 0 THEN wge.RazaoSocial ',
  '  WHEN 1 THEN wge.Nome ',
  '  ELSE "????" ',
  '  END NO_WhoGerEnti, ',
  ' ',
  '/**IF(oca.chmtitoco<>0, tit.Nome, oca.Nome) NO_Titulo,**/ ',
  'CASE KndOnde ',
  '  WHEN 1 THEN oca.OndeDescr ',
  '  /*WHEN 2 THEN scc.Nome*/ ',
  '  WHEN 3 THEN ovl.Nome ',
  '  ELSE "????" ',
  'END NO_ONDE, ',
  ' ',
  '/**IF(oca.WhoGerEnti<>0, tit.Nome, oca.Nome) NO_chmocowho**/ ',
  ' ',
  'oca.* ',
  ' ',
  'FROM chmococad oca ',
  '/**LEFT JOIN chmtitoco tit ON tit.Codigo=oca.chmtitoco**/ ',
  'LEFT JOIN entidades    wge ON wge.Codigo=oca.WhoGerEnti ',
  '/*LEFT JOIN stqcencad  scc ON scc.Codigo=oca.CodOnde*/ ',
  'LEFT JOIN ovdlocal     ovl ON ovl.Codigo=oca.CodOnde ',
  'LEFT JOIN chmhowcad    chc ON chc.Codigo=oca.ChmHowCad ',
  '/**LEFT JOIN senhas    sen ON sen.Numero=oca.Usercad**/ ',
  ' ',
  'LEFT JOIN chmocowho cow ON cow.Codigo=oca.Codigo ',
  ' ',
  'WHERE cow.WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
  'OR oca.KndIsWhoDo=' + Grl_Geral.FF0(CO_KndIsWhoDo_4_COD_Todos) ,
  ' ',
  EmptyStr]);
end;

procedure TFmChmOcoCab.RestorePosition;
begin
  VertScrollBox1.ViewportPosition := PointF(VertScrollBox1.ViewportPosition.X, 0);
  MainLayout1.Align := TAlignLayout.Client;
  VertScrollBox1.RealignContent;
end;

procedure TFmChmOcoCab.TabControl1Gesture(Sender: TObject; const EventInfo: TGestureEventInfo;
  var Handled: Boolean);
begin
  case EventInfo.GestureID of
    sgiLeft:
      begin
        if NextTabAction.Enabled then
          TabControl1.Next;
        Handled := True;
      end;
    sgiRight:
      begin
        TabControl1.Previous;
        Handled := True;
      end;
  end;
end;

end.
