unit ChmOcoCab3;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FMX.Grid.Style, FMX.Controls.Presentation, FMX.ScrollBox, FMX.Grid, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.Components,
  Data.Bind.DBScope, Data.Bind.Grid, FMX.TabControl,
  UnGrl_Vars, FMX.Layouts, FMX.StdCtrls, FMX.Objects, UnGrl_Consts,
  UnFMX_Grl_Vars, FMX.Memo, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView;

type
  TFmChmOcoCab = class(TForm)
    BindingsList1: TBindingsList;
    BindSourceDB1: TBindSourceDB;
    QrChmOcoCab: TFDQuery;
    QrChmOcoCabNO_WhoGerEnti: TWideStringField;
    QrChmOcoCabNome: TStringField;
    QrChmOcoCabNO_ONDE: TWideStringField;
    QrChmOcoCabCodigo: TIntegerField;
    QrChmOcoCabKndNome: TShortintField;
    QrChmOcoCabChmTitOco: TIntegerField;
    QrChmOcoCabDescricao: TStringField;
    QrChmOcoCabWhoGerEnti: TIntegerField;
    QrChmOcoCabKndIsWhoDo: TIntegerField;
    QrChmOcoCabWhoDoQtdSel: TIntegerField;
    QrChmOcoCabWhoDoQtdMin: TIntegerField;
    QrChmOcoCabWhoDoQtdMax: TIntegerField;
    QrChmOcoCabKndOnde: TShortintField;
    QrChmOcoCabCodOnde: TIntegerField;
    QrChmOcoCabOndeDescr: TStringField;
    QrChmOcoCabPorqueDescr: TStringField;
    QrChmOcoCabQuandoIni: TDateTimeField;
    QrChmOcoCabQuandoFim: TDateTimeField;
    QrChmOcoCabChmHowCad: TIntegerField;
    QrChmOcoCabHowManyUnd: TStringField;
    QrChmOcoCabHowManyQtd: TFloatField;
    QrChmOcoCabHowMuchMoed: TStringField;
    QrChmOcoCabHowMuchValr: TFloatField;
    QrChmOcoCabPriAbrtDtH: TDateTimeField;
    QrChmOcoCabLstRAbrDtH: TDateTimeField;
    QrChmOcoCabLstFechDtH: TDateTimeField;
    QrChmOcoCabImportancia: TShortintField;
    QrChmOcoCabUrgencia: TShortintField;
    QrChmOcoCabBRK_Nome: TStringField;
    GrChmOcoCab: TGrid;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    QrChmOcoCabQuandoIni_TXT: TWideStringField;
    QrChmOcoCabQuandoFim_TXT: TWideStringField;
    QrChmOcoCabNO_ChmHowCad: TWideStringField;
    VSB: TVertScrollBox;
    LayTitle: TLayout;
    RecTitle: TRectangle;
    LinBlue: TLine;
    LinGrey: TLine;
    LaTitle: TLabel;
    Button1: TButton;
    Memo1: TMemo;
    QrChmOcoCabCloseUser: TIntegerField;
    QrChmOcoCabCloseDtHr: TDateTimeField;
    LinkFillControlToField1: TLinkFillControlToField;
    procedure QrChmOcoCabCalcFields(DataSet: TDataSet);
    procedure GrChmOcoCabCellClick(const Column: TColumn; const Row: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrChmOcoCabAfterOpen(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure LabelClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaItens();
  public
    { Public declarations }
    FCodigo: Integer;
    //
    procedure MostraOcorrenciaSelecionadaFromGrid();
    procedure MostraOcorrenciaSelecionadaFromRect(Codigo: Integer);
  end;

var
  FmChmOcoCab: TFmChmOcoCab;

implementation

uses UnFMX_DmkProcFunc, UnGrl_DmkDB, UnGrl_Geral, UnOVS_Consts, UnApp_Jan,
  Module;

{$R *.fmx}

procedure TFmChmOcoCab.Button1Click(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FmChmOcoCab.ComponentCount - 1 do
    if FmChmOcoCab.Components[I] is TRectangle then
      TRectangle(FmChmOcoCab.Components[I]).Repaint;
end;

procedure TFmChmOcoCab.CarregaItens();
  procedure CarregaItemAtual();
  var
    CR: TRectangle;
    L: TText;
    TmpImg: TImage;
    T: array of String;
  begin
    T := [Grl_Geral.FFN(QrChmOcoCabCodigo.Value, 6) + ' - ' + QrChmOcoCabNome.Value];
    //////////////////////////////////////
    CR := TRectangle.Create(Self);
    CR.Parent := VSB;
    FMX_DmkPF.LabelInRectangleNameRec(CR);
    CR.Align := TAlignLayout.alTop;
    //CR.CalloutPosition := TCalloutPosition.cpLeft;
    CR.Margins.Top := 10;
    CR.Margins.Bottom := 10;
    CR.Margins.Right := 5;
    //CR.Height := 75;

    CR.Fill.Color := VAR_LaTexto_Back_Color;
(*
    CR.Stroke.Color := TAlphaColors.LightSteelblue;
    CR.Stroke.Kind := TBrushKind.Solid;
*)
    CR.Stroke.Kind := TBrushKind.None;

    CR.XRadius := 10;
    CR.YRadius := 10;
    CR.Margins.Left := 5;
    CR.Margins.Left := 5;
    CR.Margins.Top  := 10;
    CR.Margins.Left := 5;
    //CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;

    L := TText.Create(Self);
    L.Parent := CR;
    L.Align := TAlignLayout.alClient;
      //'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
    L.Tag       := QrChmOcoCabCodigo.Value;
    //L.TagFloat  := QrChmOcoPuxDtHrLido.Value;
    L.Margins.Top    := 5;
    L.Margins.Bottom := 5;
    L.Margins.Left   := 20;
    L.Margins.Right  := 5;

    L.WordWrap := True;
    L.AutoSize := True;
    //L.OnPaint := FMX_DmkPF.LabelInRectanglePaint;

    L.TextSettings.FontColor := VAR_LaTexto_Font_Color;
    L.TextSettings.HorzAlign := TTextAlign.Leading;
    L.HitTest := True;
    L.OnClick := LabelClick;

    CR.Align := TAlignLayout.alBottom;
    CR.Align := TAlignLayout.alTop;
    //L.Repaint;
(*
    FMX_DmkPF.AlturaRectRunTimeCreateInRectangle1(VSB, CR, L,
    [Grl_Geral.FFN(QrChmOcoCabCodigo.Value, 6) + ' - ' + QrChmOcoCabNome.Value]);
*)
    //
    CR.Height        := FMX_DmkPF.AlturaRectRunTimeCreateInRectangle2(VSB, CR, L, T);
    L.Text           := Grl_Geral.ATS(T);
  end;
var
  I: Integer;
  Comp: TRectangle;
begin
  for I := FmChmOcoCab.ComponentCount - 1 downto 0 do
  begin
    if FmChmOcoCab.Components[I] is TRectangle then
    begin
      Comp := TRectangle(FmChmOcoCab.Components[I]);
      if FMX_DmkPF.IsLabelRunTimeCreateInRectangle(Comp) then
      begin
        {$IFDEF AUTOREFCOUNT}
          Comp.Parent := nil;
          Comp.Owner.RemoveComponent(Comp);
        {$ELSE}
          Comp.Free;
        {$ENDIF}
        Comp := nil;
      end;
    end;
  end;

  //Dmod.ReopenChmOcoPux(QrChmOcoCab, EmptyStr);
  //
  for I := 1 to 10 do
  begin
    QrChmOcoCab.First;
    while not QrChmOcoCab.Eof do
    begin
      CarregaItemAtual();
      //
      QrChmOcoCab.Next;
    end;
  end;

end;

procedure TFmChmOcoCab.FormActivate(Sender: TObject);
var
  Codigo: Integer;
begin
  Memo1.Text := 'FormActivate' + sLineBreak + Memo1.Text;
  VSB.Repaint;
  if FCodigo <> 0 then
  begin
    Codigo := FCodigo;
    FCodigo := 0;
    if QrChmOcoCab.RecordCount > 0 then
     if QrChmOcoCab.Locate('Codigo', Codigo, []) then
       MostraOcorrenciaSelecionadaFromGrid();
  end;
end;

procedure TFmChmOcoCab.FormCreate(Sender: TObject);
begin
///////////////////////
///
  GrChmOcoCab.Visible := False;
  //LayMenu.Align := TAlignLayout.Contents;
  VSB.Align := TAlignLayout.Client;
///
///
  FmChmOcoCab.Fill.Color := TAlphaColors.White;
  FmChmOcoCab.Fill.Kind := TBrushKind.Solid;
  FmChmOcoCab.RecTitle.Fill.Color := TAlphaColors.Lightblue;
  //
  GrChmOcoCab.Align := TAlignLayout.Client;
end;

procedure TFmChmOcoCab.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmChmOcoCab.FormShow(Sender: TObject);
begin
  Dmod.ReopenChmOcoCad(QrChmOcoCab);
  Memo1.Text := 'FormShow' + sLineBreak + Memo1.Text;
end;

procedure TFmChmOcoCab.GrChmOcoCabCellClick(const Column: TColumn;
  const Row: Integer);
begin
  if not Grl_DmkDB.QueryTemRegistros(QrChmOcoCab) then Exit;
  //
  FMX_DmkPF.VibrarComoBotao();
  MostraOcorrenciaSelecionadaFromGrid();
end;

procedure TFmChmOcoCab.LabelClick(Sender: TObject);
var
  LabelN: TLabel;
  Codigo: Integer;
begin
  FMX_DmkPF.VibrarComoBotao();
  LabelN := TLabel(Sender);

  Codigo    := LabelN.Tag;

  MostraOcorrenciaSelecionadaFromRect(Codigo);
end;

procedure TFmChmOcoCab.MostraOcorrenciaSelecionadaFromGrid();
var
  Nome, NO_WhoGerEnti, Descricao, PorqueDescr, NO_ChmHowCad: String;
  QuandoIni, QuandoFim: TDateTime;
  ChmHowCad: Integer;
begin
  Nome          := QrChmOcoCabNome.Value;
  NO_WhoGerEnti := QrChmOcoCabNO_WhoGerEnti.Value;
  Descricao     := QrChmOcoCabDescricao.Value;
  PorqueDescr   := QrChmOcoCabPorqueDescr.Value;
  NO_ChmHowCad  := QrChmOcoCabNO_ChmHowCad.Value;
  QuandoIni   := QrChmOcoCabQuandoIni.Value;
  QuandoFim   := QrChmOcoCabQuandoFim.Value;
  ChmHowCad   := QrChmOcoCabChmHowCad.Value;
  App_Jan.MostraFormChmOcoCad(
    // T�tulo
     Nome,
    // Quem Chamou
    NO_WhoGerEnti,
    // Descri��o do que fazer
    Descricao,
    // Motivo
    PorqueDescr,
    // T�tulo Procedimento (Fluxo)
    NO_ChmHowCad,
    // Per�odo de execu��o
    QuandoIni,
    QuandoFim,
    // Tem itens
    ChmHowCad
  )
end;

procedure TFmChmOcoCab.MostraOcorrenciaSelecionadaFromRect(Codigo: Integer);
begin
 if QrChmOcoCab.Locate('Codigo', Codigo, []) then
   MostraOcorrenciaSelecionadaFromGrid();
end;

procedure TFmChmOcoCab.QrChmOcoCabAfterOpen(DataSet: TDataSet);
begin
  CarregaItens();
end;

procedure TFmChmOcoCab.QrChmOcoCabCalcFields(DataSet: TDataSet);
begin
  QrChmOcoCabBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrChmOcoCab, 1, QrChmOcoCabNome.Value);
end;

end.
