unit ChmOcoDon;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.ScrollBox, FMX.Memo,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, System.Bindings.Outputs, Data.Bind.Components,
  Data.Bind.DBScope, Data.Bind.Grid, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkENums, FMX.Edit, math, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, MultiDetailAppearanceU, FMX.ListView, FMX.Objects;

type
  TFmChmOcoDon = class(TForm)
    LayControle: TLayout;
    LaEncerrarEvento: TLabel;
    VSB: TVertScrollBox;
    LayTitle: TLayout;
    RecTitle: TRectangle;
    LinBlue: TLine;
    LinGrey: TLine;
    LaTitle: TLabel;
    rect_base: TRectangle;
    LaNome: TLabel;
    LaTitNome: TLabel;
    Layout8: TLayout;
    Rectangle5: TRectangle;
    Layout9: TLayout;
    LaConcatPeriodo: TLabel;
    Layout10: TLayout;
    RecDataIni: TRectangle;
    LaDataIni: TLabel;
    LaTitPeriodo: TLabel;
    Layout1: TLayout;
    RecDataFim: TRectangle;
    LaDataFim: TLabel;
    Rectangle1: TRectangle;
    LaNO_WhoGerEnti: TLabel;
    LaTitNO_WhoGerEnti: TLabel;
    Rectangle4: TRectangle;
    LaDescricao: TLabel;
    LaTitDescricao: TLabel;
    Rectangle2: TRectangle;
    LayMenu: TLayout;
    RectFundo: TRectangle;
    RectMenuBase: TRectangle;
    LaMenuTitulo: TLabel;
    LMenuSubTitulo: TLabel;
    LaMenuClickSim: TLabel;
    LaMenuLine1: TLine;
    LaMenuLine2: TLine;
    LaMenuClickNao: TLabel;
    Label1: TLabel;
    RectObservacao: TRectangle;
    MeObservacao: TMemo;
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure LaEncerrarEventoClick(Sender: TObject);
    procedure LaMenuClickSimClick(Sender: TObject);
    procedure LaMenuClickNaoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ExibeOMenu();
    procedure EscondeOMenu();
  public
    { Public declarations }
    FSQLType: TSQLType;
    FCodigo, FDON_Controle, FETP_Controle, FWHO_Controle: Integer;
    FDON_Observacao: String;
    //
    procedure ReopenChmOcoDon(Controle: Integer);
  end;

var
  FmChmOcoDon: TFmChmOcoDon;

implementation

uses UnFMX_DmkProcFunc, UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms, UnGrl_Vars,
  UnFMX_DmkObjects,
  Module, ChmOcoCad;

{$R *.fmx}

procedure TFmChmOcoDon.EscondeOMenu();
begin
  with FmChmOcoDon do
  begin
    RectMenuBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height + 20,
      0.3,
      TAnimationType.InOut,
      TInterpolationType.Circular);

    RectFundo.AnimateFloat('Opacity', 0, 0.6);

    TThread.CreateAnonymousThread(procedure
    begin
      sleep(800);
      LayMenu.Visible := False;
    end).Start;
  end;
end;

procedure TFmChmOcoDon.ExibeOMenu();
var
  iBoundsH: Extended;
begin
  iBoundsH := FMX_DmkObjects.MenuFloatTopFromBounds(Self, RectMenuBase);
  //
  with FmChmOcoDon do
  begin
    //LaMenuTexto.Text := Pergunta;
    RectMenuBase.Width := LayMenu.Width - 16;
    RectMenuBase.Position.X := 8;
    RectMenuBase.Position.Y := LayMenu.Height + 20 + iBoundsH;

    LayMenu.Visible := True;
    RectFundo.Opacity := 0;
    RectFundo.AnimateFloat('Opacity', 0.4, 0.2);

    RectMenuBase.AnimateFloat(
      'Position.Y',
      LayMenu.Height - RectMenuBase.Height - 8 - iBoundsH,
      0.5,
      TAnimationType.InOut,
      TInterpolationType.Circular);
  end;
end;

procedure TFmChmOcoDon.FormCreate(Sender: TObject);
begin
  FSQLType := stIns;
  VSB.Align := TAlignLayout.Contents;
  LayMenu.Align := TAlignLayout.Contents;
  LayMenu.Visible := False;
end;

procedure TFmChmOcoDon.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmChmOcoDon.LaEncerrarEventoClick(Sender: TObject);
begin
  MeObservacao.Text := FDON_Observacao;
  ExibeOMenu();
end;

procedure TFmChmOcoDon.LaMenuClickNaoClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  EscondeOMenu();
end;

procedure TFmChmOcoDon.LaMenuClickSimClick(Sender: TObject);
var
  //CloseDtHr,
  DoneDtHr, Observacao: String;
  // CloseUser,
  Codigo, Controle, ChmOcoEtp, ChmOcoWho, MobUpWeb: Integer;
  SQLType: TSQLType;
begin
  FMX_DmkPF.VibrarComoBotao();
  //

  SQLType        := stUpd;
  Codigo         := FCodigo;
  ChmOcoEtp      := FETP_Controle;
  ChmOcoWho      := FWHO_Controle;
  Controle       := FDON_Controle;
  DoneDtHr       := Grl_Geral.FDT(Now(), 109);
  //CloseUser      := ;
  //CloseDtHr      := ;
  Observacao     := MeObservacao.Text;
  MobUpWeb       := 0; // N�o
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
  'DoneDtHr', //'CloseUser', 'CloseDtHr',
  'Observacao', 'MobUpWeb'], [
  'Codigo', 'ChmOcoEtp', 'ChmOcoWho', 'Controle'], [
  DoneDtHr, //CloseUser, CloseDtHr,
  Observacao, MobUpWeb], [
  Codigo, ChmOcoEtp, ChmOcoWho, Controle], True(*False*),
  TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
(*   N�o adianta fazer aqui! desfaz quando recarrega via download!
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocoetp', False, [
    'DoneDtHr'], [
    'Controle'], [
    DoneDtHr], [
    ChmOcoEtp], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile);
    //
*)
    FmChmOcoCad.ReopenChmOcoEtp(FDON_Controle);
    //EscondeOMenu(); Erro. Componentes podem n�o existir mais!
    LayMenu.Visible := False;
    FMX_DmkForms.FechaFm_AllOS0(FmChmOcoDon);
  end;

{
var
  DoneDtHr, CloseDtHr, Observacao: String;
  Codigo, CtrlInMob, Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
  SQLType: TSQLType;
begin
  SQLType        := FSQLType;
  Codigo         := FCodigo;
  CtrlInMob      := FCtrlInMob;
  //Controle       := FControle;
  ChmOcoEtp      := FChmOcoEtp;
  ChmOcoWho      := FChmOcoWho;
  Observacao     := MeObservacao.Text;
  DoneDtHr       := Grl_Geral.FDT(Now(), 109);
  CloseUser      := 0;
  CloseDtHr      := '0000-00-00 00:00:00';

  //
  CtrlInMob := Grl_DmkDB.GetNxtCodigoInt('chmocodon', 'CtrlInMob', SQLType, CtrlInMob);
  //ou > ? := UMyMod.BPGS1I32('chmocodon', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
  'Codigo', (*'Controle',*) 'ChmOcoEtp',
  'ChmOcoWho', 'Observacao', 'DoneDtHr',
  'CloseUser', 'CloseDtHr'], [
  'CtrlInMob'], [
  Codigo, (*Controle,*) ChmOcoEtp,
  ChmOcoWho, Observacao, DoneDtHr,
  CloseUser, CloseDtHr], [
  CtrlInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocoetp', False, [
    'DoneDtHr'], [
    'Controle'], [
    DoneDtHr], [
    ChmOcoEtp], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile);
    //
    FmChmOcoCad.ReopenChmOcoEtp(CtrlInMob);
    FMX_DmkForms.FechaFm_AllOS0(FmChmOcoDon);
  end;
}
end;

procedure TFmChmOcoDon.ReopenChmOcoDon(Controle: Integer);
begin
{
  if FChmHowCad <> 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoEtp, Dmod.AllDB, [
    'SELECT * ',
    'FROM chmocoetp ',
    'WHERE Controle=' + Grl_Geral.FF0(FChmOcoCadCodigo),
    EmptyStr]);
  end;
}
end;


{/
Fazer coluna de sim/nao para tem descri��o
Fazer coluna de sim/nao para Feito


http://thuliobittencourt.com/blog/registrando-pushnotification/

///////////////////////////////////////////

///////////////////////////  V�deo 1 ///////////////////////////////////////////
https://www.youtube.com/watch?v=wJ64jkWPzlg

///////////////////////////  V�deo 2 ///////////////////////////////////////////
https://www.youtube.com/watch?v=2om39rq-Mms


Chave de API:
AIzaSyAr5MIKvNheeVOxyBydu4-Zjd32CIFRL5Q
N�mero do projeto:
205807968282

///////////////////////////  V�deo 3 ///////////////////////////////////////////
https://www.youtube.com/watch?v=0ncuzTy03iE
}

end.
