unit OpcoesSeqJan;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, UnDmkEnums,
  UnExes_Consts, UnProjGroupEnums;

type
  TFmOpcoesSeqJan = class(TForm)
    Layout1: TLayout;
    Label1: TLabel;
    Layout3: TLayout;
    BtOK: TButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    GBExSeqJan: TGroupBox;
    RB_02_Conta_Modo_Valor: TRadioButton;
    RB_03_Valor_Conta_Modo: TRadioButton;
    RB_04_Valor_Modo_Conta: TRadioButton;
    RB_05_Modo_Conta_Valor: TRadioButton;
    RB_01_Conta_Valor_Modo: TRadioButton;
    RB_06_Modo_Valor_Conta: TRadioButton;
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmOpcoesSeqJan: TFmOpcoesSeqJan;

implementation

uses UnExesM_Vars, UnGrl_Geral, UnGrl_DmkDB, UnFMX_dmkRemoteQuery,
  UnFMX_DmkProcFunc, UnGrl_Vars,
  Module, UnFMX_DmkForms;

{$R *.fmx}

procedure TFmOpcoesSeqJan.BtOKClick(Sender: TObject);
var
  SQLTYpe: TSQLTYpe;
begin
  Dmod.ReopenExesMOpcoes();
  if Dmod.QrExesMOpcoes.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
(*
  if CkAmbiente.IsChecked then
    Ambiente := 1
  else
    Ambiente := 0;
  if CkAllowNoMed.IsChecked then
    AllowNoMed := 1
  else
    AllowNoMed := 0;
*)
  if RB_01_Conta_Valor_Modo.IsChecked then
    VAR_EXSEQJAN_SEL    := CO_00_Conta_Valor_Modo
  else
  if RB_02_Conta_Modo_Valor.IsChecked then
    VAR_EXSEQJAN_SEL    := CO_01_Conta_Modo_Valor
  else
  if RB_03_Valor_Conta_Modo.IsChecked then
    VAR_EXSEQJAN_SEL    := CO_02_Valor_Conta_Modo
  else
  if RB_04_Valor_Modo_Conta.IsChecked then
    VAR_EXSEQJAN_SEL    := CO_03_Valor_Modo_Conta
  else
  if RB_05_Modo_Conta_Valor.IsChecked then
    VAR_EXSEQJAN_SEL    := CO_04_Modo_Conta_Valor
  else
  if RB_06_Modo_Valor_Conta.IsChecked then
    VAR_EXSEQJAN_SEL    := CO_05_Modo_Valor_Conta
  else ;
  //
  FMX_DmkForms.DestroiFm_AllOS0(FmOpcoesSeqJan);
  //
end;

procedure TFmOpcoesSeqJan.FormShow(Sender: TObject);
begin
  case VAR_EXSEQJAN_SEL of
    //CO_00_Conta_Valor_Modo: RB_01_Conta_Valor_Modo.IsChecked := True;
    CO_01_Conta_Modo_Valor: RB_02_Conta_Modo_Valor.IsChecked := True;
    CO_02_Valor_Conta_Modo: RB_03_Valor_Conta_Modo.IsChecked := True;
    CO_03_Valor_Modo_Conta: RB_04_Valor_Modo_Conta.IsChecked := True;
    CO_04_Modo_Conta_Valor: RB_05_Modo_Conta_Valor.IsChecked := True;
    CO_05_Modo_Valor_Conta: RB_06_Modo_Valor_Conta.IsChecked := True;
    else                    RB_01_Conta_Valor_Modo.IsChecked := True;

  end;
end;

end.
