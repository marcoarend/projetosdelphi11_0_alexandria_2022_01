unit OpcoesExesM;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit,
  UnDmkEnums, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, UnGrl_Vars, FMX.DialogService;

type
  TFmOpcoesExesM = class(TForm)
    ToolBar1: TToolBar;
    TitleLabel: TLabel;
    PnScale: TPanel;
    Panel3: TPanel;
    BtOK: TButton;
    CdEXcMobDevCad: TClientDataSet;
    CdEXcMobDevCadCodigo: TIntegerField;
    CdEXcMobDevCadDeviceID: TStringField;
    CdEXcMobDevCadUserNmePdr: TStringField;
    CdEXcMobDevCadPIN: TStringField;
    CdEXcMobDevCadScaleX: TIntegerField;
    CdEXcMobDevCadScaleY: TIntegerField;
    Button1: TButton;
    EdERPNameByCli: TEdit;
    Label3: TLabel;
    CdCtrlGeral: TClientDataSet;
    CdCtrlGeralERPNameByCli: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    LaScaleX: TLabel;
    EdScaleX: TEdit;
    LaScaleY: TLabel;
    EdScaleY: TEdit;
    Panel2: TPanel;
    Label1: TLabel;
    EdMailUser: TEdit;
    GBEstiloForms: TGroupBox;
    RBAir: TRadioButton;
    RBAmakrits: TRadioButton;
    RBAquaGraphite: TRadioButton;
    RBBlend: TRadioButton;
    RBDark: TRadioButton;
    RBGoldenGraphite: TRadioButton;
    RBLight: TRadioButton;
    RBRubyGraphite: TRadioButton;
    RBTransparent: TRadioButton;
    RBNenhum: TRadioButton;
    Panel1: TPanel;
    BtOpcoesAvancadas: TButton;
    BtSincroServer: TButton;
    Panel5: TPanel;
    BtNovaVersao: TButton;
    Panel4: TPanel;
    Label2: TLabel;
    EdEXcIdFunc: TEdit;
    CdEXcMobDevCadEXcIdFunc: TIntegerField;
    Panel6: TPanel;
    BtSeqJan: TButton;
    CdEXcMobDevCadEXSeqJan: TSmallintField;
    procedure BtOKClick(Sender: TObject);
    //
    procedure StyleRadioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure BtOpcoesAvancadasClick(Sender: TObject);
    procedure BtSincroServerClick(Sender: TObject);
    procedure BtNovaVersaoClick(Sender: TObject);
  private
    { Private declarations }
    FEstiloForms: String;
  public
    { Public declarations }
  end;

var
  FmOpcoesExesM: TFmOpcoesExesM;

implementation

uses UnExesM_Vars, UnGrl_Geral, UnGrl_DmkDB, UnFMX_dmkRemoteQuery, UnApp_PF,
  UnFMX_DmkProcFunc, UnFMX_DmkForms,
  OpcoesAvancadas, OpcoesSeqJan,
  UnFMX_DmkUnLic,
  Module;

{$R *.fmx}

procedure TFmOpcoesExesM.BtNovaVersaoClick(Sender: TObject);
begin
  if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
    Exit;
  FMX_DmkForms.CriaFm_AllOS0(TFmOpcoesSeqJan, FmOpcoesSeqJan, fcmCreateTryShwM, True, False);
end;

procedure TFmOpcoesExesM.BtOKClick(Sender: TObject);
var
  I, Codigo, ScaleX, ScaleY, EXcIdFunc, EXSeqJan: Integer;
  SQLType: TSQLType;
  sSQL, ERPNameByCli, EstiloForms, MailUser: String;
  Component: TComponent;
begin
  FMX_DmkPF.VibrarComoBotao();
  SQLType        := stNil;
  Codigo         := VAR_COD_DEVICE_IN_SERVER;
  ScaleX         := Grl_Geral.IMV(EdScaleX.Text);
  ScaleY         := Grl_Geral.IMV(EdScaleY.Text);
  if ScaleX = 0 then
    ScaleX := 100;
  if ScaleY = 0 then
    ScaleY := 100;
  //
  EXcIdFunc   := Grl_Geral.IMV(EdEXcIdFunc.Text);
  EstiloForms := FEstiloForms;
  //
  ERPNameByCli     := EdERPNameByCli.Text;
  MailUser         := EdMailUser.Text;
  //
  EXSeqJan         := VAR_EXSEQJAN_SEL;

  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB,
  stUpd, 'excmobdevatv', False, [
  'ScaleX', 'ScaleY', 'EXcIdFunc',
  'EXSeqJan'], [
  'Codigo'], [
  ScaleX, ScaleY, EXcIdFunc,
  EXSeqJan], [
  Codigo], False,
  TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Dmod.ReopenExesMOpcoes();
    if Dmod.QrExesMOpcoes.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'exesmopcoes', False, [
    'ERPNameByCli', 'EstiloForms', 'MailUser',
    'EXcIdFunc', 'EXSeqJan'], [
    'Codigo'], [
    ERPNameByCli, EstiloForms, MailUser,
    EXcIdFunc, EXSeqJan], [
    1], False,
    TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
    begin
      SQLType := stUpd;
      ///  S E R V E R
      if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'excmobdevcad', False, [
      'ScaleX', 'ScaleY', 'EXcIdFunc',
      'EXSeqJan'], [
      'Codigo'], [
      ScaleX, ScaleY, EXcIdFunc,
      EXSeqJan], [
      Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
      False, sSQL) then
      begin
        if FMX_dmkRemoteQuery.SQL_Executa(sSQL) then
        begin
          // L O C A L
          if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'excmobdevcad', False, [
          'EXcIdFunc', 'EXSeqJan'], [
          'Codigo'], [
          EXcIdFunc, EXSeqJan], [
          Codigo], False,
          TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
          begin
            Dmod.ReopenExesMOpcoes();
            Dmod.ReopenEXcMobDevCad();
            Close;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmOpcoesExesM.BtOpcoesAvancadasClick(Sender: TObject);
var
  Senha: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  Senha := '';
  TDialogService.InputQuery('Opções Avançadas', ['Informe a senha de acesso'], [Senha],
  procedure(const AResult: TModalResult; const AValues: array of string)
  begin
    if AResult = mrOk then
    begin
      Senha := AValues[0];
      if Senha = '82526' then
      begin
        FMX_DmkForms.CriaFm_AllOS0(TFmOpcoesAvancadas, FmOpcoesAvancadas,
          fcmCreateTryShwM, True, False);
      end else
        Grl_Geral.MB_Aviso('Senha incorreta');
    end;
  end);
end;

procedure TFmOpcoesExesM.BtSincroServerClick(Sender: TObject);
var
  //Corda,
  Alias, SQL_JOIN, SQL_WHERE: String;
  Exclui: Boolean;
begin
  Alias := '';
  SQL_JOIN := '';
  Exclui := True;
  //
  FMX_DmkPF.VibrarComoBotao();
  BtSincroServer.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    //InfoTempMemo('Preparando downloads...');
    SQL_WHERE := 'WHERE DeviceID="' + CdEXcMobDevCadDeviceID.Value + '"';
    //InfoTempMemo('Baixando configurações de inspeções', I, K);
    if App_PF.DownloadTabela('excmobdevcad', Alias, 'excmobdevcad', SQL_JOIN, SQL_WHERE, Exclui, True) then
      Exclui := False;
    //
    Dmod.ReopenEXcMobDevCad();
    Grl_Geral.MB_Info('Sincronização finalizada!');
  finally
    BtSincroServer.Enabled := True;
  end;
end;

procedure TFmOpcoesExesM.FormCreate(Sender: TObject);
begin
  LaScaleX.Visible := False;
  LaScaleY.Visible := False;
  EdScaleX.Visible := False;
  EdScaleY.Visible := False;
end;

procedure TFmOpcoesExesM.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOpcoesExesM.FormShow(Sender: TObject);
var
  I: Integer;
  RB: TRadioButton;
begin
  FMX_dmkRemoteQuery.QuerySelect(CdEXcMobDevCad, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN, ',
      // Opcooes
      'ScaleX, ScaleY, EXcIdFunc, EXSeqJan ',
      // Fim Opcoes
      'FROM excmobdevcad',
      'WHERE Codigo=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER) + ''
    ], VAR_AMBIENTE_APP);
  if CdEXcMobDevCad.RecordCount > 0 then
  begin
    EdScaleX.Text := Grl_Geral.FF0(CdEXcMobDevCadScaleX.Value);
    EdScaleY.Text := Grl_Geral.FF0(CdEXcMobDevCadScaleY.Value);
    //
    EdEXcIdFunc.Text := Grl_Geral.FF0(CdEXcMobDevCadEXcIdFunc.Value);
    VAR_EXSEQJAN_SEL := CdEXcMobDevCadEXSeqJan.Value;
  end else
  begin
    EdScaleX.Text := Grl_Geral.FF0(CdEXcMobDevCadScaleX.Value);
    EdScaleY.Text := Grl_Geral.FF0(CdEXcMobDevCadScaleY.Value);
    //
    EdEXcIdFunc.Text := Grl_Geral.FF0(CdEXcMobDevCadEXcIdFunc.Value);
    VAR_EXSEQJAN_SEL := CdEXcMobDevCadEXSeqJan.Value;
  end;
  Dmod.ReopenExesMOpcoes();
  RB := TRadioButton(FmOpcoesExesM.FindComponent('RB' + Dmod.QrExesMOpcoesEstiloForms.Value));
  if RB <> nil then
    RB.IsChecked := True;
  //////////////////////////////////////////////////////////////////////////////
  ///
  FMX_dmkRemoteQuery.QuerySelect(CdCtrlGeral, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT ERPNameByCli ',
      'FROM ctrlgeral',
      'WHERE Codigo=1',
      EmptyStr
    ], VAR_AMBIENTE_APP);
  EdERPNameByCli.Text := CdCtrlGeralERPNameByCli.Value;
  EdMailUser.Text   := Dmod.QrExesMOpcoesMailUser.Value;
end;

procedure TFmOpcoesExesM.StyleRadioClick(Sender: TObject);
var
  I: Integer;
begin
  FEstiloForms := TRadioButton(Sender).Text;
  if FEstiloForms = RBNenhum.Text then
  begin
    if StyleBook <> nil then
      StyleBook := nil;
  end else
  begin
    for I := 0 to FmOpcoesExesM.ComponentCount - 1 do
    begin
      if FmOpcoesExesM.Components[I] is TStyleBook then
        if TStyleBook(FmOpcoesExesM.Components[I]).Name = 'StyleBook_' +
        FEstiloForms then
        begin
          StyleBook := TStyleBook(FmOpcoesExesM.Components[I]);
          Break;
        end;
    end;
  end;
{
  if RBNenhum.IsChecked then
  begin
    if StyleBook <> nil then
      StyleBook := nil;
  end else
  begin
    for I := 0 to FmOpcoesExesM.ComponentCount - 1 do
    begin
      if FmOpcoesExesM.Components[I] is TRadioButton then
        if (TRadioButton(FmOpcoesExesM.Components[I]).GroupName) = Lowercase('EstiloForm') then
          if TRadioButton(FmOpcoesExesM.Components[I]).IsChecked then
            EstiloForms := TRadioButton(FmOpcoesExesM.Components[I]).Text;
    end;

    for I := 0 to FmOpcoesExesM.ComponentCount - 1 do
    begin
      if FmOpcoesExesM.Components[I] is TStyleBook then
        if TStyleBook(FmOpcoesExesM.Components[I]).Name = 'StyleBook_' +
        EstiloForms then
        begin
          StyleBook := TStyleBook(FmOpcoesExesM.Components[I]);
          Break;
        end;
    end;
  end;
}
end;

end.
