unit OViInspSeqNcfChk;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors, UnDmkEnums,
  System.ImageList, FMX.ImgList;

type
  TFmOViInspSeqNcfChk = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    GrContexto: TGrid;
    QrContexto: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    QrContextoNO_Contexto: TStringField;
    QrContextoControle: TIntegerField;
    Panel1: TPanel;
    BtNxtEnd: TButton;
    BtSair: TButton;
    ImageList1: TImageList;
    QrContextoCadContexto: TIntegerField;
    QrContextoITENS: TLargeintField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrContextoBRK_Contexto: TStringField;
    procedure GrContextoCellClick(const Column: TColumn; const Row: Integer);
    procedure BtSairClick(Sender: TObject);
    procedure BtNxtEndClick(Sender: TObject);
    procedure QrContextoAfterOpen(DataSet: TDataSet);
    procedure QrContextoCalcFields(DataSet: TDataSet);
    procedure GrContextoGetValue(Sender: TObject; const ACol, ARow: Integer;
      var Value: TValue);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }

  public
    { Public declarations }
    FFecha: Boolean;
    FCodInMob, FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP, FProduto,
    FCodGrade, FOVcYnsChk: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    //
    //
    procedure ReopenItem();
  end;

var
  FmOViInspSeqNcfChk: TFmOViInspSeqNcfChk;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms,
  UnFMX_DmkProcFunc,
  Module,
  OViInspSeqNcfTop, OViIspCenSel;

{ TFmOViInspSeqMed }

procedure TFmOViInspSeqNcfChk.BtNxtEndClick(Sender: TObject);
var
  SubInspDone, CodInMob, _CIM, PecaAtual, TotalPecas: Integer;
  Proximo: Boolean;
begin
  FMX_DmkPF.VibrarComoBotao();
  Proximo     := False;
  TotalPecas  := Grl_Geral.IMV(FmOViIspCenSel.LaTotalItens.Text);
  if FPecaSeq = TotalPecas then
  begin
    SubInspDone := 2; // 2 = Inconformidades avaliadas
    PecaAtual    := FPecaSeq;
  end else
  begin
    SubInspDone := 0; // 0 = Nada pronto
    PecaAtual    := FPecaSeq + 1;
  end;
  CodInMob    := FCodInMob;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
  'SubInspDone', 'PecaAtual'], [
  'CodInMob'], [
  SubInspDone, PecaAtual], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    FmOViIspCenSel.LoacalizaAberto_QrOVmIspMobCab(Proximo, _CIM);
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfChk);
  end;
end;

procedure TFmOViInspSeqNcfChk.BtSairClick(Sender: TObject);
begin
  FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfChk);
end;

procedure TFmOViInspSeqNcfChk.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViInspSeqNcfChk.GrContextoCellClick(const Column: TColumn;
  const Row: Integer);
begin
  if (QrContexto.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqNcfTop, FmOViInspSeqNcfTop, fcmOnlyCreate, True, False) then
  begin
    //FmOViInspSeqNcfTop.LaLocal.Text    := NO_Local;
    FmOViInspSeqNcfTop.LaArtigo.Text     := FNO_Artigo;
    FmOViInspSeqNcfTop.LaNrOP.Text       := Grl_Geral.FF0(FNrOP);
    FmOViInspSeqNcfTop.LaTamCad.Text     := FCodTam;
    FmOViInspSeqNcfTop.LaItemAtual.Text  := LaItemAtual.Text;
    FmOViInspSeqNcfTop.LaTotalItens.Text := LaTotalItens.Text;
    //
    FmOViInspSeqNcfTop.FCodInMob          := FCodInMob;
    FmOViInspSeqNcfTop.FCtrlInMob         := 0; // Criar Novo
    FmOViInspSeqNcfTop.FPecaSeq           := FPecaSeq;
    FmOViInspSeqNcfTop.FLocal             := FLocal;
    FmOViInspSeqNcfTop.FArtigo            := FArtigo;
    FmOViInspSeqNcfTop.FNrOP              := FNrOP;
    FmOViInspSeqNcfTop.FNrReduzidoOP      := FNrReduzidoOP;
    FmOViInspSeqNcfTop.FProduto           := FProduto;
    FmOViInspSeqNcfTop.FCodGrade          := FCodGrade;
    FmOViInspSeqNcfTop.FNO_Local         := FNO_Local;
    FmOViInspSeqNcfTop.FNO_Artigo        := FNO_Artigo;
    FmOViInspSeqNcfTop.FCodTam           := FCodTam;
    //
    FmOViInspSeqNcfTop.LaNO_Contexto.Text  := QrContextoNO_Contexto.Value;
    FmOViInspSeqNcfTop.FOVcYnsChk          := FOVcYnsChk;
    FmOViInspSeqNcfTop.FOVcYnsChkCtx       := QrContextoControle.Value;
    FmOViInspSeqNcfTop.FCadContexto        := QrContextoCadContexto.Value;
    //
    FmOViInspSeqNcfTop.ReopenItem();
    //
    FMX_DmkForms.ShowModal(FmOViInspSeqNcfTop);
  end;
end;

procedure TFmOViInspSeqNcfChk.GrContextoGetValue(Sender: TObject; const ACol,
  ARow: Integer; var Value: TValue);
begin
  // http://monkeystyler.com/guide/TGrid
end;

procedure TFmOViInspSeqNcfChk.QrContextoAfterOpen(DataSet: TDataSet);
begin
  if QrContexto.RecordCount > 0 then
    GrContexto.Col
end;

procedure TFmOViInspSeqNcfChk.QrContextoCalcFields(DataSet: TDataSet);
begin
  QrContextoBRK_Contexto.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrContexto, 0, QrContextoNO_Contexto.Value);
end;

procedure TFmOViInspSeqNcfChk.ReopenItem();
begin
{
  Grl_DmkDB.AbreSQLQuery0(QrContexto, Dmod.AllDB, [
  'SELECT ycc.Controle, ',
  'yqt.Codigo CadContexto, yqt.Nome NO_CONTEXTO ',
  'FROM ovcynschkctx ycc ',
  'LEFT JOIN ovcynsqstctx yqt ON yqt.Codigo=ycc.Contexto ',
  'WHERE ycc.Codigo=' + Grl_Geral.FF0(FOVcYnsChk),
  '']);
  //Grl_Geral.MB_SQL(FmOViInspSeqNcfChk, QrContexto);
}
  Grl_DmkDB.AbreSQLQuery0(QrContexto, Dmod.AllDB, [
  'SELECT ycc.Controle, ',
  'yqt.Codigo CadContexto, yqt.Nome NO_CONTEXTO, ',
  ' COUNT(imi.PecaSeq) ITENS ',
  'FROM ovcynsqstctx yqt ',
  'LEFT JOIN ovcynschkctx ycc ON yqt.Codigo=ycc.Contexto ',
  'LEFT JOIN ovmispmobinc imi ON imi.ovcynschkctx=ycc.Controle ',
  '  AND imi.CodInMob=' + Grl_Geral.FF0(FCodInMob),
  '  AND imi.PecaSeq=' + Grl_Geral.FF0(FPecaSeq),
  'WHERE ycc.Codigo=' + Grl_Geral.FF0(FOVcYnsChk),
  'GROUP BY yqt.Codigo ',
  '']);
  //Grl_Geral.MB_SQL(FmOViInspSeqNcfChk, QrContexto);
end;

end.
