unit OViInspSeqMedEdit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit,
  FireDAC.Comp.Client,
  Math,
  UnDmkEnums;

type
  TFmOViInspSeqMedEdit = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    Panel1: TPanel;
    Label1: TLabel;
    LaNO_Topico: TLabel;
    Panel2: TPanel;
    Panel5: TPanel;
    Label5: TLabel;
    LaMedidMin: TLabel;
    Label10: TLabel;
    LaMedidCer: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    LaMedidMax: TLabel;
    Panel6: TPanel;
    Panel4: TPanel;
    BtConfirma: TButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    BtMnosMax: TButton;
    Panel7: TPanel;
    Label7: TLabel;
    EdMedidFei: TEdit;
    BtMnosMin: TButton;
    BtMaisMin: TButton;
    BtMaisMax: TButton;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtMnosMaxClick(Sender: TObject);
    procedure BtMnosMinClick(Sender: TObject);
    procedure BtMaisMinClick(Sender: TObject);
    procedure BtMaisMaxClick(Sender: TObject);
    procedure EdMedidFeiEnter(Sender: TObject);
    procedure EdMedidFeiKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FTypedStr: String;
    FTypingOK: Boolean;
    procedure InsAltAtual(MedidFei: Double);
    procedure IncrementaMedida(Incremento: Double);
  public
    { Public declarations }
    FQrItem: TFDQuery;
    CodInMob, CtrlInMob, PecaSeq, OVcYnsMedDim, CodGrade, MedidOky, PontosCrit,
    PontNeg, OVcYnsMedTop, Tobiko: Integer;
    CodTam: String;
    MedidCer, MedidMin, MedidMax: Double;
  end;

var
  FmOViInspSeqMedEdit: TFmOViInspSeqMedEdit;

implementation

uses UnGrl_Geral, UnFMX_Geral, UnFMX_dmkProcFunc, UnGrl_DmkDB, UnFMX_DmkForms,
  UnOVSM_Vars,
  Module,
  OViIspCenSel;

{$R *.fmx}

procedure TFmOViInspSeqMedEdit.BtConfirmaClick(Sender: TObject);
var
  sMedidFei: String;
  MedidFei: Double;
begin
  FMX_DmkPF.VibrarComoBotao();
  //Grl_Geral.MB_Info('EdMedidFei.KeyBoardType.DecimalNumberPad??   ou NumberPad');
  sMedidFei := Grl_Geral.SoNumeroEVirgula_TT(EdMedidFei.Text);
  if FMX_dmkPF.FIC(sMedidFei <> Trim(EdMedidFei.Text), EdMedidFei,
  'Valor inv�lido! S� digite n�meros e v�rgula!') then
  begin
    EdMedidFei.SetFocus;
    Exit;
  end;
  MedidFei := FMX_Geral.DMV(sMedidFei);
  if FMX_dmkPF.FIC(MedidFei < 0.001, EdMedidFei,
  'Valor inv�lido! Valor deve ser diferente de zero!') then
  begin
    EdMedidFei.SetFocus;
    Exit;
  end;
  //
  InsAltAtual(MedidFei);
end;

procedure TFmOViInspSeqMedEdit.BtMaisMaxClick(Sender: TObject);
begin
  IncrementaMedida(VAR_MEDIDAS_INCREMENTO_MAX);
end;

procedure TFmOViInspSeqMedEdit.BtMaisMinClick(Sender: TObject);
begin
  IncrementaMedida(VAR_MEDIDAS_INCREMENTO_MIN);
end;

procedure TFmOViInspSeqMedEdit.BtMnosMaxClick(Sender: TObject);
begin
  IncrementaMedida(-VAR_MEDIDAS_INCREMENTO_MAX);
end;

procedure TFmOViInspSeqMedEdit.BtMnosMinClick(Sender: TObject);
begin
  IncrementaMedida(-VAR_MEDIDAS_INCREMENTO_MIN);
end;

procedure TFmOViInspSeqMedEdit.EdMedidFeiEnter(Sender: TObject);
begin
  FTypedStr := '';
  FTypingOK := True;
  EdMedidFei.CaretPosition := Length(EdMedidFei.Text);
end;

procedure TFmOViInspSeqMedEdit.EdMedidFeiKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  Valor, Divisor: Double;
  Casas: Integer;
begin
  if FTypingOK then
  begin
    if CharInSet(KeyChar, ['0'..'9']) then
    begin
      FTypedStr := FTypedStr + KeyChar;
      Casas := VAR_MEDIDAS_CASAS_DECIMAIS;
      Valor := Grl_Geral.DMV(FTypedStr);
      Divisor := Power(10, Casas);
      Valor := Valor / Divisor;
      EdMedidFei.Text := Grl_Geral.FFT(Valor, Casas, siNegativo);
      EdMedidFei.CaretPosition := Length(EdMedidFei.Text);
    end else
      FTypingOK := False;
  end;
end;

procedure TFmOViInspSeqMedEdit.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViInspSeqMedEdit.FormShow(Sender: TObject);
begin
  try
    EdMedidFei.SetFocus;
  except
    // nada!
  end;
end;

procedure TFmOViInspSeqMedEdit.IncrementaMedida(Incremento: Double);
var
  MedidFei, MedidCer: Double;
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  MedidCer  := Grl_Geral.DMV(LaMedidCer.Text);
  MedidFei := Grl_Geral.DMV(EdMedidFei.Text);
  if MedidFei < 0.01 then
    MedidFei := MedidCer
  else
    MedidFei := MedidFei + Incremento;
  EdMedidFei.Text := Grl_Geral.FFT(MedidFei, VAR_MEDIDAS_CASAS_DECIMAIS, siNegativo);
end;

procedure TFmOViInspSeqMedEdit.InsAltAtual(MedidFei: Double);
//var
  //AchowZeroOuFim: Boolean;
var
(*
  CodTam: String;
  Codigo, CodInMob, CtrlInMob, OVcYnsMedDim, CodGrade, MedidOky, PontNeg, UlWayInz: Integer;
  MedidCer, MedidMin, MedidMax: Double;
*)
  SQLType: TSQLType;
begin
  if CtrlInMob <> 0 then
    SQLType        := stUpd
  else
    SQLType        := stIns;
(*
  Codigo         := ;
  CodInMob       := ;
  CtrlInMob      := ;
  OVcYnsMedDim   := ;
  CodGrade       := ;
  CodTam         := ;
  MedidFei       := ;
*)
  if (MedidFei >= MedidMin) and (MedidFei <= MedidMax) then
  begin
    MedidOky       := 1;
    PontNeg        := 0;
  end else
  begin
    MedidOky       := 0;
    PontNeg        := PontosCrit;
  end;
  //
(*
  MedidCer       := ;
  MedidMin       := ;
  MedidMax       := ;
  UlWayInz       := ;
*)
  //
  //CodInMob := UMyMod.BPGS1I32('ovmispdevmed', 'Codigo', 'CtrlInMob', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  CtrlInMob := Grl_DmkDB.GetNxtCodigoInt('ovmispmobmed', 'CtrlInMob', SQLType, CtrlInMob);
  //if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovmispmobmed', auto_increment?[
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'ovmispmobmed', False, [
  'CodInMob', 'PecaSeq', 'OVcYnsMedDim',
  'CodGrade', 'CodTam', 'MedidFei',
  'MedidOky', 'PontNeg', 'MedidCer',
  'MedidMin', 'MedidMax',(*'UlWayInz',*)
  'OVcYnsMedTop', 'Tobiko'], [
  'CtrlInMob'], [
  CodInMob, PecaSeq, OVcYnsMedDim,
  CodGrade, CodTam, MedidFei,
  MedidOky, PontNeg, MedidCer,
  MedidMin, MedidMax, (*UlWayInz, *)
  OVcYnsMedTop, Tobiko], [
  CtrlInMob], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Grl_DmkDB.AbreQuery(FQrItem, DMod.AllDB);
    FmOViIspCenSel.ReopenPontosNeg();
    FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqMedEdit);
  end;
end;


end.
