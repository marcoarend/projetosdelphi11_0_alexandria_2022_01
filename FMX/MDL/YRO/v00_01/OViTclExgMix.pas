unit OViTclExgMix;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkEnums, UnOVS_Consts;

type
  TFmOViTclExgMix = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    GrTopico: TGrid;
    QrOpcoes: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    LaNO_TOPYKO: TLabel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Panel2: TPanel;
    BtOVmIspMobLvr: TButton;
    QrOpcoesBRK_Opcao: TStringField;
    QrOpcoesTopyko: TIntegerField;
    QrOpcoesCodigo: TIntegerField;
    QrOpcoesNO_OPCAO: TStringField;
    QrOpcoesMagnitude: TIntegerField;
    QrOpcoesPontNeg: TIntegerField;
    procedure GrTopicoCellClick(const Column: TColumn; const Row: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtOVmIspMobLvrClick(Sender: TObject);
    procedure QrOpcoesCalcFields(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure DefineInconformidadeAtual();
    procedure MostraOViItxLvr(CIM: Integer);
  public
    { Public declarations }
    FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP,
    FProduto, FCodGrade, FOVcYnsExgCad, FOVcYnsExgTop, FOVcYnsMixTop: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    //
    //
    procedure ReopenItem();
  end;

var
  FmOViTclExgMix: TFmOViTclExgMix;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms, UnApp_Jan, UnFMX_DmkProcFunc,
  Module, OViTclCenSel, OViTclExgAvl, OViItxLvr;

{ TFmOViInspSeqMed }

procedure TFmOViTclExgMix.BtOVmIspMobLvrClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  MostraOViItxLvr(0);
end;

procedure TFmOViTclExgMix.DefineInconformidadeAtual;
const
  MedidFei = 0;
  MedidOky = 1;
  MedidCer = 0;
  MedidMin = 0;
  MedidMax = 0;
var
  Topyko, Magnitude, PontNeg, OVcYnsExgTop, OVcYnsMixOpc: Integer;
  SQLType: TSQLType;
begin
  //  Deixa incluir quantas vezes quiser
  SQLType        := stIns;
  //CodInMob       := FCodInMob;
  //CtrlInMob      := FCtrlInMob;
  OVcYnsExgTop   := FOVcYnsExgTop;
  OVcYnsMixOpc   := QrOpcoesCodigo.Value;
  //PecaSeq        := FPecaSeq;
  Topyko         := FOVcYnsMixTop;
  Magnitude      := QrOpcoesMagnitude.Value;
  PontNeg        := QrOpcoesPontNeg.Value;
  //
  //CtrlInMob := UMyMod.BPGS1I32('ovmispdevinc', 'Codigo', 'CtrlInMob', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  FCtrlInMob := Grl_DmkDB.GetNxtCodigoInt('ovmitxmobexg', 'CtrlInMob', SQLType, FCtrlInMob);
  //if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovmitxmobexg', auto_increment?[
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'ovmitxmobexg', False, [
  'CodInMob', 'PecaSeq', 'OVcYnsMixOpc',
  'OVcYnsExgTop', 'Topyko', 'CodGrade',
  'CodTam', 'MedidFei', 'MedidOky',
  'Magnitude', 'PontNeg', 'MedidCer',
  'MedidMin', 'MedidMax'(*, 'UlWayInz',
  'QtdFotos'*)], [
  'CtrlInMob'], [
  FCodInMob, FPecaSeq, OVcYnsMixOpc,
  OVcYnsExgTop, Topyko, FCodGrade,
  FCodTam, MedidFei, MedidOky,
  Magnitude, PontNeg, MedidCer,
  MedidMin, MedidMax(*, UlWayInz,
  QtdFotos*)], [
  FCtrlInMob], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    //Grl_DmkDB.AbreQuery(FQrItem, DMod.AllDB);
    FmOViTclCenSel.ReopenPontosNeg();
    FmOViTclExgAvl.ReopenItem();
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViTclExgMix);
  end;
end;

procedure TFmOViTclExgMix.FormCreate(Sender: TObject);
begin
  FCtrlInMob := 0;
end;

procedure TFmOViTclExgMix.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViTclExgMix.GrTopicoCellClick(const Column: TColumn;
  const Row: Integer);
var
  sPergunta: String;
begin
  if (QrOpcoes.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  //Grl_Geral.MB_Pergunta(
  sPergunta := 'Deseja realmente definir a inconformidade "' +
  QrOpcoesNO_OPCAO.Value+ '" do contexto "' + LaNO_TOPYKO.Text +
  '" para a pe�a n� ' + LaItemAtual.Text + '?';
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      case AResult of
        mrYes: DefineInconformidadeAtual();
        mrNo: Grl_Geral.MB_Info('Defin��o n�o realizada!');
        mrCancel: Grl_Geral.MB_Info('Defin��o Abortada!');
      end;
    end);
  // Fim Pergunta!
end;

procedure TFmOViTclExgMix.MostraOViItxLvr(CIM: Integer);
begin
  App_Jan.MostraOViItxLvr(CIM, LaItemAtual.Text,
    LaTotalItens.Text, FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP,
    FNrReduzidoOP, FProduto, FCodGrade, FNO_Local, FNO_Artigo, FCodTam,
    LaNO_TOPYKO.Text, FOVcYnsExgCad, FOVcYnsExgTop, FOVcYnsMixTop,
    (*ReabreOViTclExgAvl*)True);
  //
  FMX_DmkForms.FechaFm_AllOS0(FmOViTclExgMix);
end;

procedure TFmOViTclExgMix.QrOpcoesCalcFields(DataSet: TDataSet);
begin
  QrOpcoesBRK_Opcao.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrTopico, 0, QrOpcoesNO_OPCAO.Value);
end;

procedure TFmOViTclExgMix.ReopenItem();
begin
  Grl_DmkDB.AbreSQLQuery0(QrOpcoes, Dmod.AllDB, [
  'SELECT yct.Topyko, yqt.Codigo, yqt.Nome NO_OPCAO, ',
  'yqt.Magnitude, yqt.PontNeg ',
  'FROM ovcynsexgtop yct ',
  'LEFT JOIN ovcynsmixtop ymt ON ymt.Codigo=yct.Topyko ',
  'LEFT JOIN ovcynsmixopc yqt ON yqt.Codigo=ymt.Codigo',
  'WHERE yqt.Codigo IS NOT NULL ',  // evitar erro no android!
  'AND yct.Codigo=' + Grl_Geral.FF0(FOVcYnsExgCad),
  'AND yct.Controle=' + Grl_Geral.FF0(FOVcYnsExgTop),
  EmptyStr]);
end;

{
function MsgBox(const AMessage: string; const ADialogType: TMsgDlgType; const AButtons: TMsgDlgButtons;
    const ADefaultButton: TMsgDlgBtn ): Integer;
var
    myAns: Integer;
    IsDisplayed: Boolean;
begin
/
    myAns := -1;
    IsDisplayed := False;

While myAns = -1 do
Begin
    if IsDisplayed = False then
    TDialogService.MessageDialog(AMessage, ADialogType, AButtons, ADefaultButton, 0,
            procedure (const AResult: TModalResult)
            begin
                myAns := AResult;
                IsDisplayed := True;
            end);

    IsDisplayed := True;
    Application.ProcessMessages;
End;

Result := myAns;
end;
}

end.
