unit OpcoesAvancadas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, UnDmkEnums;

type
  TFmOpcoesAvancadas = class(TForm)
    Layout1: TLayout;
    Label1: TLabel;
    Layout2: TLayout;
    CkAmbiente: TCheckBox;
    Layout3: TLayout;
    BtOK: TButton;
    Layout4: TLayout;
    CkAllowNoMed: TCheckBox;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Layout5: TLayout;
    CkUsarHTTPS: TCheckBox;
    Layout6: TLayout;
    CkUsarApiDev: TCheckBox;
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmOpcoesAvancadas: TFmOpcoesAvancadas;

implementation

uses UnOVSM_Vars, UnGrl_Geral, UnGrl_DmkDB, UnFMX_dmkRemoteQuery,
  UnFMX_DmkProcFunc, UnGrl_Vars,
  Module, UnFMX_DmkForms;

{$R *.fmx}

procedure TFmOpcoesAvancadas.BtOKClick(Sender: TObject);
var
  SQLTYpe: TSQLTYpe;
  Ambiente, AllowNoMed, UsarHTTPS, UsarApiDev: Integer;
begin
  Dmod.ReopenOVSMOpcoes();
  if Dmod.QrOVSMOpcoes.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
(*
  if CkAmbiente.IsChecked then
    Ambiente := 1
  else
    Ambiente := 0;
  if CkAllowNoMed.IsChecked then
    AllowNoMed := 1
  else
    AllowNoMed := 0;
*)
  Ambiente   := Grl_Geral.BoolToInt(CkAmbiente.IsChecked);
  AllowNoMed := Grl_Geral.BoolToInt(CkAllowNoMed.IsChecked);
  UsarHTTPS  := Grl_Geral.BoolToInt(CkUsarHTTPS.IsChecked);
  UsarApiDev := Grl_Geral.BoolToInt(CkUsarApiDev.IsChecked);
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'ovsmopcoes', False, [
  'Ambiente', 'AllowNoMed', 'UsarHTTPS',
  'UsarApiDev'], [
  'Codigo'], [
  Ambiente, AllowNoMed, UsarHTTPS,
  UsarApiDev], [
  1], False,
  TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Dmod.ReopenOVSMOpcoes();
    FMX_DmkForms.DestroiFm_AllOS0(FmOpcoesAvancadas);
  end;
end;

procedure TFmOpcoesAvancadas.FormCreate(Sender: TObject);
begin
  CkAmbiente.IsChecked   := VAR_AMBIENTE_APP  = 1;  // Testes
  CkAllowNoMed.IsChecked := VAR_PERMITE_NAO_MEDIR;  // Permite n�o medir
  CkUsarHTTPS.IsChecked  := Grl_Geral.IntToBool(Dmod.QrOVSMOpcoesUsarHTTPS.Value);
  CkUsarApiDev.IsChecked := Grl_Geral.IntToBool(Dmod.QrOVSMOpcoesUsarApiDev.Value);
end;

end.
