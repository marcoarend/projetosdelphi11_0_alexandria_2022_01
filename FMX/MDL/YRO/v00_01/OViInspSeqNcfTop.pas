unit OViInspSeqNcfTop;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkEnums, UnOVS_Consts;

type
  TFmOViInspSeqNcfTop = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    GrTopico: TGrid;
    QrTopico: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    QrTopicoNO_Topico: TStringField;
    QrTopicoCodigo: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    LaNO_Contexto: TLabel;
    QrTopicoConta: TIntegerField;
    QrTopicoMagnitude: TIntegerField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Panel2: TPanel;
    BtOVmIspMobLvr: TButton;
    QrTopicoBRK_Topico: TStringField;
    procedure GrTopicoCellClick(const Column: TColumn; const Row: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtOVmIspMobLvrClick(Sender: TObject);
    procedure QrTopicoCalcFields(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure DefineInconformidadeAtual();
    function  DefinePontosNeg(Magnitude: Integer): Double;
    procedure MostraOViInspSeqNcfLvr(CIM: Integer);
  public
    { Public declarations }
    FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP,
    FProduto, FCodGrade, FOVcYnsChk, FOVcYnsChkCtx, FCadContexto: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    //
    //
    procedure ReopenItem();
  end;

var
  FmOViInspSeqNcfTop: TFmOViInspSeqNcfTop;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms, UnApp_Jan, UnFMX_DmkProcFunc,
  Module, OViIspCenSel, OViInspSeqNcfChk, OViInspSeqNcfLvr;

{ TFmOViInspSeqMed }

procedure TFmOViInspSeqNcfTop.BtOVmIspMobLvrClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  MostraOViInspSeqNcfLvr(0);
end;

procedure TFmOViInspSeqNcfTop.DefineInconformidadeAtual;
var
  Topico, Magnitude, PontNeg, OVcYnsChkTop, Contexto, OVcYnsChkCtx: Integer;
  SQLType: TSQLType;
begin
  // 19-11-02 s� estava deixando um t�pico por Contexto!
//  if FCtrlInMob <> 0 then
//    SQLType        := stUpd
//  else
    SQLType        := stIns;
  // Fim 19-11-02
  //
  //CodInMob       := FCodInMob;
  //CtrlInMob      := FCtrlInMob;
  Contexto       := FCadContexto;
  OVcYnsChkCtx   := FOVcYnsChkCtx;
  OVcYnsChkTop   := QrTopicoConta.Value;
  //PecaSeq        := FPecaSeq;
  Topico         := QrTopicoCodigo.Value;
  Magnitude      := QrTopicoMagnitude.Value;
  PontNeg        := Trunc(DefinePontosNeg(Magnitude));

  //
  //CtrlInMob := UMyMod.BPGS1I32('ovmispdevinc', 'Codigo', 'CtrlInMob', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  FCtrlInMob := Grl_DmkDB.GetNxtCodigoInt('ovmispmobinc', 'CtrlInMob', SQLType, FCtrlInMob);
  //if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovmispmobinc', auto_increment?[
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'ovmispmobinc', False, [
  'CodInMob', 'OVcYnsChkTop', 'PecaSeq',
  'Topico', 'Magnitude', 'PontNeg',
  'Contexto', 'OVcYnsChkCtx'], [
  'CtrlInMob'], [
  FCodInMob, OVcYnsChkTop, FPecaSeq,
  Topico, Magnitude, PontNeg,
  Contexto, OVcYnsChkCtx], [
  FCtrlInMob], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    //Grl_DmkDB.AbreQuery(FQrItem, DMod.AllDB);
    FmOViIspCenSel.ReopenPontosNeg();
    FmOViInspSeqNcfChk.ReopenItem();
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfTop);
  end;

end;

function TFmOViInspSeqNcfTop.DefinePontosNeg(Magnitude: Integer): Double;
begin
  // ovcynsqstmag
  case Magnitude of
    1024: Result := FmOViIspCenSel.QrOVcYnsARQCadPontosTole.Value;
    2048: Result := FmOViIspCenSel.QrOVcYnsARQCadPontosGrav.Value;
    3072: Result := FmOViIspCenSel.QrOVcYnsARQCadPontosCrit.Value;
    else
    begin
      Result := 0;
      Grl_Geral.MB_Erro('"Magnitude" n�o implementada em "TFmOViInspSeqNcfTop.DefinePontosNeg()"');
    end;
  end;
end;

procedure TFmOViInspSeqNcfTop.FormCreate(Sender: TObject);
begin
  FCtrlInMob := 0;
end;

procedure TFmOViInspSeqNcfTop.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViInspSeqNcfTop.GrTopicoCellClick(const Column: TColumn;
  const Row: Integer);
var
  sPergunta: String;
begin
  if (QrTopico.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  //Grl_Geral.MB_Pergunta(
  sPergunta := 'Deseja realmente definir a inconformidade "' +
  QrTopicoNO_Topico.Value+ '" do contexto "' + LaNO_Contexto.Text +
  '" para a pe�a n� ' + LaItemAtual.Text + '?';
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      case AResult of
        mrYes: DefineInconformidadeAtual();
        mrNo: Grl_Geral.MB_Info('Defin��o n�o realizada!');
        mrCancel: Grl_Geral.MB_Info('Defin��o Abortada!');
      end;
    end);
  // Fim Pergunta!
end;

procedure TFmOViInspSeqNcfTop.MostraOViInspSeqNcfLvr(CIM: Integer);
begin
(*
  if FMX_DmkForms.CriaFm_AllOS2(TFmOViInspSeqNcfLvr, FmOViInspSeqNcfLvr, True, False) then
  begin
    //FmOViInspSeqNcfLvr.LaLocal.Text     := NO_Local;
    FmOViInspSeqNcfLvr.LaArtigo.Text      := FNO_Artigo;
    FmOViInspSeqNcfLvr.LaNrOP.Text        := Grl_Geral.FF0(FNrOP);
    FmOViInspSeqNcfLvr.LaTamCad.Text      := FCodTam;
    FmOViInspSeqNcfLvr.LaItemAtual.Text   := LaItemAtual.Text;
    FmOViInspSeqNcfLvr.LaTotalItens.Text  := LaTotalItens.Text;
    //
    FmOViInspSeqNcfLvr.FCodInMob          := FCodInMob;
    FmOViInspSeqNcfLvr.FCtrlInMob         := FCtrlInMob;
    FmOViInspSeqNcfLvr.FPecaSeq           := FPecaSeq;
    FmOViInspSeqNcfLvr.FLocal             := FLocal;
    FmOViInspSeqNcfLvr.FArtigo            := FArtigo;
    FmOViInspSeqNcfLvr.FNrOP              := FNrOP;
    FmOViInspSeqNcfLvr.FNrReduzidoOP      := FNrReduzidoOP;
    FmOViInspSeqNcfLvr.FProduto           := FProduto;
    FmOViInspSeqNcfLvr.FCodGrade          := FCodGrade;
    FmOViInspSeqNcfLvr.FNO_Local          := FNO_Local;
    FmOViInspSeqNcfLvr.FNO_Artigo         := FNO_Artigo;
    FmOViInspSeqNcfLvr.FCodTam            := FCodTam;
    //
    FmOViInspSeqNcfLvr.LaNO_Contexto.Text  := LaNO_Contexto.Text;
    FmOViInspSeqNcfLvr.FOVcYnsChk          := FOVcYnsChk;
    FmOViInspSeqNcfLvr.FOVcYnsChkCtx       := FOVcYnsChkCtx;
    FmOViInspSeqNcfLvr.FCadContexto        := FCadContexto;
    //
    FMX_DmkForms.ShowModal(FmOViInspSeqNcfLvr);
    //
*)
  App_Jan.MostraOViInspSeqNcfLvr(CIM, LaItemAtual.Text,
    LaTotalItens.Text, FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP,
    FNrReduzidoOP, FProduto, FCodGrade, FNO_Local, FNO_Artigo, FCodTam,
    LaNO_Contexto.Text, FOVcYnsChk, FOVcYnsChkCtx, FCadContexto,
    (*ReabreOViInspSeqNcfChk*)True);
  //
  FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfTop);
end;

procedure TFmOViInspSeqNcfTop.QrTopicoCalcFields(DataSet: TDataSet);
begin
  QrTopicoBRK_Topico.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrTopico, 0, QrTopicoNO_Topico.Value);
end;

procedure TFmOViInspSeqNcfTop.ReopenItem();
begin
  Grl_DmkDB.AbreSQLQuery0(QrTopico, Dmod.AllDB, [
  'SELECT yct.Conta, yqt.Codigo, yqt.Nome NO_TOPICO, ',
  'yct.Magnitude ',
  'FROM ovcynschktop yct ',
  'LEFT JOIN ovcynsqsttop yqt ON yqt.Codigo=yct.Topico ',
  'WHERE yct.Codigo=' + Grl_Geral.FF0(FOVcYnsChk),
  'AND yct.Controle=' + Grl_Geral.FF0(FOVcYnsChkCtx),
  '']);
end;

{
function MsgBox(const AMessage: string; const ADialogType: TMsgDlgType; const AButtons: TMsgDlgButtons;
    const ADefaultButton: TMsgDlgBtn ): Integer;
var
    myAns: Integer;
    IsDisplayed: Boolean;
begin
/
    myAns := -1;
    IsDisplayed := False;

While myAns = -1 do
Begin
    if IsDisplayed = False then
    TDialogService.MessageDialog(AMessage, ADialogType, AButtons, ADefaultButton, 0,
            procedure (const AResult: TModalResult)
            begin
                myAns := AResult;
                IsDisplayed := True;
            end);

    IsDisplayed := True;
    Application.ProcessMessages;
End;

Result := myAns;
end;
}

end.
