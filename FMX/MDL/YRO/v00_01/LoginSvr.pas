unit LoginSvr;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Controls.Presentation, UnDMkEnums, Data.DB, Datasnap.DBClient,
  UnGrl_Vars, UnFMX_Grl_Vars;

type
  TFmLoginSvr = class(TForm)
    Label8: TLabel;
    EdUserNmePdr: TEdit;
    Label9: TLabel;
    EdCNPJEmpresa: TEdit;
    BtEnviarDados: TButton;
    BtBuscaDados: TButton;
    Label10: TLabel;
    EdPIN: TEdit;
    CdOVcMobDevCad: TClientDataSet;
    CdOVcMobDevCadCodigo: TIntegerField;
    CdOVcMobDevCadDeviceID: TStringField;
    CdOVcMobDevCadUserNmePdr: TStringField;
    CdOVcMobDevCadPIN: TStringField;
    CdAux: TClientDataSet;
    IntegerField1: TIntegerField;
    CdOVcMobDevCadFoLocObjIns: TSmallintField;
    CdOVcMobDevCadScaleX: TIntegerField;
    CdOVcMobDevCadScaleY: TIntegerField;
    Label7: TLabel;
    EdDeviceID: TEdit;
    Label1: TLabel;
    EdDeviceName: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdDvcScreenH: TEdit;
    EdDvcScreenW: TEdit;
    Label5: TLabel;
    Label4: TLabel;
    EdOSName: TEdit;
    Label6: TLabel;
    EdOSVersion: TEdit;
    EdOSNickName: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Button1: TButton;
    CkAmbiente: TCheckBox;
    procedure BtEnviarDadosClick(Sender: TObject);
    procedure BtBuscaDadosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FTentativas: Integer;
    //
    function  AtivarDevice(Codigo: Integer; Nome, DeviceID, UserNmePdr, PIN,
              CNPJ: String; FoLocObjIns, ScaleX, ScaleY: Integer): Boolean;
    function  InsereDadosAtivacaoNoServidor(): Boolean;
  public
    { Public declarations }
  end;

var
  FmLoginSvr: TFmLoginSvr;

implementation

uses UnFMX_DmkProcFunc, UnFMX_dmkRemoteQuery, UnFMX_Geral, UnGrl_DmkDB,
  UnYROM_PF, UnYROM_Vars, UnGrl_AllOS, UnApp_Jan, UnGrl_Geral,
  Module;

{$R *.fmx}

function TFmLoginSvr.AtivarDevice(Codigo: Integer; Nome, DeviceID, UserNmePdr,
  PIN, CNPJ: String; FoLocObjIns, ScaleX, ScaleY: Integer): Boolean;
var
  //DeviceID, UserNmePdr, Nome: String;
  //Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  //Codigo         := ;
  //Nome           := '';
  //DeviceID       := ;
  //UserNmePdr     := ;
  if Lowercase(PIN) = Lowercase(CdOVcMobDevCadPIN.Value) then
  begin

    if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrLoc, Dmod.AllDB,
      [
        'DELETE FROM yocmobdevatv WHERE DeviceID="' + DeviceID + '"',
        ''
      ]) then
    begin
      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB,
      stIns, 'ovcmobdevatv', False, [
      'Nome', 'DeviceID', 'UserNmePdr',
      'PIN', 'CNPJ', 'FoLocObjIns',
      'ScaleX', 'ScaleY'], ['Codigo'],
      [Nome, DeviceID, UserNmePdr,
      PIN, CNPJ, FoLocObjIns,
      ScaleX, ScaleY], [Codigo], False,
      TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
      begin
        if YROM_PF.ObtemCodigoDeAceiteNoServidor() then
        begin
          FMX_Geral.MB_Info('Ol� ' + VAR_NIK_DEVICE_IN_SERVER + '!' + sLineBreak +
          'Este dispositivo foi aceito na empresa!');
          Close;
          App_Jan.MostraFormLoginApp()
        end else
          FMX_Geral.MB_Erro('Falha ao obter dados do usu�rio! (A)');
      end else
        FMX_Geral.MB_Erro('Falha ao ativar usu�rio (B)!');
    end else
      FMX_Geral.MB_Erro('Falha ao ativar usu�rio (A)!');
  end else
  begin
    FTentativas := FTentativas + 1;
    if FTentativas < 3 then
      FMX_Geral.MB_Aviso('PIN n�o confere!')
    else
      Application.Terminate;
  end;
end;

procedure TFmLoginSvr.BtBuscaDadosClick(Sender: TObject);
(*
var
  Nome, DeviceID, UserNmePdr, DeviceName, OSName, OSNickName, OSVersion, DtaHabIni, DtaHabFim, PIN: String;
  Codigo, DvcScreenH, DvcScreenW, Allowed, LastSetAlw: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Nome           := ;
  DeviceID       := ;
  UserNmePdr     := ;
  DeviceName     := ;
  DvcScreenH     := ;
  DvcScreenW     := ;
  OSName         := ;
  OSNickName     := ;
  OSVersion      := ;
  DtaHabIni      := ;
  DtaHabFim      := ;
  Allowed        := ;
  LastSetAlw     := ;
  PIN            := ;
*)
var
  Nome, DeviceID, UserNmePdr, PIN: String;
  Codigo: Integer;
  SQLType: TSQLType;
  //
  CNPJEmpresa, Texto: String;
  FoLocObjIns, ScaleX, ScaleY: Integer;
begin
  FMX_DmkPF.VibrarComoBotao();
  if CkAmbiente.IsChecked then
    VAR_AMBIENTE_APP  := 1  // Testes
  else
    VAR_AMBIENTE_APP  := 0; // Produ��o
  //
  //Result         := False;
  //
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := 'Dispositivo de ' + EdUserNmePdr.Text;
  DeviceID       := EdDeviceID.Text;
  UserNmePdr     := EdUserNmePdr.Text;
  CNPJEmpresa    := EdCNPJEmpresa.Text;
  PIN            := EdPIN.Text;
  //
  if FMX_DmkPF.FIC(CNPJEmpresa = '', EdCNPJEmpresa, 'Informe o CNPJ da Empresa!') then Exit;
  if FMX_DmkPF.FIC(UserNmePdr = '', EdUserNmePdr, 'Informe o nome do respons�vel pelo aparelho!') then Exit;
  if FMX_DmkPF.FIC(PIN = '', EdUserNmePdr, 'Informe o PIN de login no aparelho!') then Exit;
  if FMX_DmkPF.FIC(DeviceID = '', EdDeviceID, 'N�o foi poss�vel obter o IMEI do aparelho!') then Exit;
  //
  if not FMX_dmkRemoteQuery.QuerySelect(CdOVcMobDevCad, CNPJEmpresa,
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN, ',
      // Opcooes
      'ScaleX, ScaleY, FoLocObjIns ',
      // Fim Opcoes
      'FROM ovcmobdevcad',
      'WHERE DeviceID="' + EdDeviceID.Text + '"'
    ], VAR_AMBIENTE_APP) then
  Exit;
  //
  if CdOVcMobDevCad.RecordCount > 0 then
  begin
    if CdOVcMobDevCadCodigo.Value < 0 then
    begin
      FMX_Geral.MB_Aviso(
      'Este dispositivo j� enviou seus dados para ativa��o, mas ainda n�o foi aceito!');
    end else
    begin
      Codigo      := CdOVcMobDevCadCodigo.Value;
      FoLocObjIns := CdOVcMobDevCadFoLocObjIns.Value;
      ScaleX      := CdOVcMobDevCadScaleX.Value;
      ScaleY      := CdOVcMobDevCadScaleY.Value;
      //
      AtivarDevice(Codigo, Nome, DeviceID, UserNmePdr, PIN, CNPJEmpresa,
      FoLocObjIns, ScaleX, ScaleY);
    end;
  end else
  begin
    FMX_Geral.MB_Info(
    'Este dispositivo ainda n�o enviou seus dados para ativa��o na empresa!' + sLineBreak +
    '    1. Clique em "Enviar dados para ativa��o" para que o servidor receba seu IMEI.' + sLineBreak +
    '    2. Contate o administrador e solicite a habilita��o do dispositivo no sistema.' + sLineBreak +
    '    3. Somente o�s a s etapas acima clique em "Buscar dados de ativa��o" para efetivar a habilita��o neste dispositivo.');
  end;
end;

procedure TFmLoginSvr.BtEnviarDadosClick(Sender: TObject);
var
  CNPJEmpresa, CNPJ_Valida: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  if CkAmbiente.IsChecked then
    VAR_AMBIENTE_APP  := 1  // Testes
  else
    VAR_AMBIENTE_APP  := 0; // Produ��o
  //
  CNPJEmpresa    := EdCNPJEmpresa.Text;
  if FMX_DmkPF.FIC(CNPJEmpresa = '', EdCNPJEmpresa, 'Informe o CNPJ da Empresa!') then Exit;
  { Est� com problema na function }
  CNPJ_Valida    := CNPJEmpresa;
(*
  if not Grl_Geral.CNPJ_Valida(EdCNPJEmpresa, CNPJ_Valida) then
    Exit;
*)
  {}
  //

  FMX_dmkRemoteQuery.QuerySelect(CdOVcMobDevCad, CNPJEmpresa,
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN ',
      'FROM ovcmobdevcad',
      'WHERE DeviceID=''' + EdDeviceID.Text + ''''
    ], VAR_AMBIENTE_APP);
  if CdOVcMobDevCad.RecordCount > 0 then
  begin
    if CdOVcMobDevCadCodigo.Value < 0 then
    begin
      FMX_Geral.MB_Aviso(
      'Este dispositivo j� enviou seus dados para ativa��o.' + sLineBreak +
      'Clique em "Buscar dados de ativa��o" para verificar se o dispositivo foi aceito.');
    end else
    begin
      FMX_Geral.MB_Info(
      'Este dispositivo j� est� ativo na empresa!.');
    end;
  end else
  begin
    if InsereDadosAtivacaoNoServidor() then
    begin
      FMX_Geral.MB_Info(
      'Dados enviados com sucesso para a empresa!' + sLineBreak +
      '    1. Contate o administrador e solicite a habilita��o do dispositivo no sistema.' + sLineBreak +
      '    2. Clique em "Buscar dados de ativa��o" para efetivar a habilita��o neste dispositivo.');
    end else
    begin
      FMX_Geral.MB_Erro('N�o foi poss�vel executar a SQL no servidor!');
    end;
  end;
end;

procedure TFmLoginSvr.Button1Click(Sender: TObject);
begin
  EdUserNmePdr.Text := 'Fabricio';
  EdPIN.Text := '1234';
  EdCNPJEmpresa.Text := '02582267000160';
  BtBuscaDadosClick(Self);
end;

procedure TFmLoginSvr.FormCreate(Sender: TObject);
var
  DeviceID, DeviceName, DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion:
  String;
begin
  FTentativas := 0;
  //
  // Dados para ativa��o do dispositivo no servidor
  Grl_AllOS.ConfiguraDadosDispositivo(DeviceID, DeviceName, DvcScreenH,
    DvcScreenW, OSName, OSNickName, OSVersion);
  EdDeviceID.Text     := DeviceID;
  EdDeviceName.Text   := DeviceName;
  EdDvcScreenH.Text   := DvcScreenH;
  EdDvcScreenW.Text   := DvcScreenW;
  EdOSName.Text       := OSName;
  EdOSNickName.Text   := OSNickName;
  EdOSVersion.Text    := OSVersion;
end;

procedure TFmLoginSvr.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

function TFmLoginSvr.InsereDadosAtivacaoNoServidor(): Boolean;
var
  //DtaHabIni, DtaHabFim,
  Nome, DeviceID, UserNmePdr, DeviceName, OSName, OSNickName, OSVersion, PIN: String;
  // Allowed, LastSetAlw
  Codigo, DvcScreenH, DvcScreenW: Integer;
  SQLType: TSQLType;
  //
  CNPJEmpresa, Texto: String;
begin
  Result         := False;
  //
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := 'Dispositivo de ' + EdUserNmePdr.Text;
  DeviceID       := EdDeviceID.Text;
  UserNmePdr     := EdUserNmePdr.Text;
  DeviceName     := EdDeviceName.Text;
  DvcScreenH     := FMX_Geral.IMV(EdDvcScreenH.Text);
  DvcScreenW     := FMX_Geral.IMV(EdDvcScreenW.Text);
  OSName         := EdOSName.Text;
  OSNickName     := EdOSNickName.Text;
  OSVersion      := EdOSVersion.Text;
  //DtaHabIni      := '0000-00-00 00:00:00';
  //DtaHabFim      := '0000-00-00 00:00:00';
  //Allowed        := 0; // N�o
  //LastSetAlw     := 0; //  OVcMobDevAlw.Codigo
  CNPJEmpresa    := EdCNPJEmpresa.Text;
  VAR_CNPJ_DEVICE_IN_SERVER  := CNPJEmpresa;
  PIN            := EdPIN.Text;
  //
  if FMX_DmkPF.FIC(CNPJEmpresa = '', EdCNPJEmpresa, 'Informe o CNPJ da Empresa!') then Exit;
  if FMX_DmkPF.FIC(UserNmePdr = '',EdUserNmePdr, 'Informe o nome do respons�vel pelo aparelho!') then Exit;
  if FMX_DmkPF.FIC(PIN = '', EdPIN, 'Informe PIN a ser usado no aparelho!') then Exit;
  if FMX_DmkPF.FIC(DeviceID = '', EdDeviceID, 'N�o foi poss�vel obter o IMEI do aparelho!') then Exit;
  //
{
  FMX_dmkRemoteQuery.QuerySelect(CdAux, CNPJEmpresa,
    [
      'SELECT MAX(Codigo) Codigo ',
      'FROM ovcmobdevcad',
      ''
    ], VAR_AMBIENTE_APP);
  Codigo := CdAux.FieldByName('Codigo').AsInteger;
  Codigo := Codigo -1;
}
  Codigo  := FMX_DmkRemoteQuery.BPGS1I32W('ovcmobdevcad' , 'Codigo', '', '',
      tsPos, stIns, (*Codigo*)0);
  Codigo := - Codigo;
  //
  //
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, stIns, 'ovcmobdevcad', False, [
  'Nome', 'DeviceID', 'UserNmePdr',
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',
  (*'DtaHabIni', 'DtaHabFim', 'Allowed',
  'LastSetAlw'*)'PIN'], [
  'Codigo'], [
  Nome, DeviceID, UserNmePdr,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,
  (*DtaHabIni, DtaHabFim, Allowed,
  LastSetAlw*)PIN], [
  Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
  False, Texto) then
  begin
    Result := FMX_dmkRemoteQuery.QueryExecute(CNPJEmpresa, Texto, VAR_AMBIENTE_APP);
  end;
end;

end.
