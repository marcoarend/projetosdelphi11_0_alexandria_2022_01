unit OViItxAct;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkEnums, UnOVS_Consts, FMX.Edit, FMX.ListBox;

type
  TFmOViItxAct = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Panel6: TPanel;
    Panel4: TPanel;
    BtExclui: TButton;
    Label5: TLabel;
    LaNO_Topico: TLabel;
    BtAltera: TButton;
    BtTiraFoto: TButton;
    GrOVsItxMobFts: TGrid;
    BLOVsItxMobFts: TBindingsList;
    QrOVmItxMobFts: TFDQuery;
    BsOVsItxMobFts: TBindSourceDB;
    QrOVmItxMobFtsCodInMob: TIntegerField;
    QrOVmItxMobFtsCtrlInMob: TIntegerField;
    QrOVmItxMobFtsDataHora: TDateTimeField;
    QrOVmItxMobFtsNomeArq: TStringField;
    QrOVmItxMobFtsLk: TIntegerField;
    QrOVmItxMobFtsDataCad: TDateField;
    QrOVmItxMobFtsDataAlt: TDateField;
    QrOVmItxMobFtsUserCad: TIntegerField;
    QrOVmItxMobFtsUserAlt: TIntegerField;
    QrOVmItxMobFtsAlterWeb: TShortintField;
    QrOVmItxMobFtsAtivo: TShortintField;
    LinkGridToDataSourceBsOVsItx: TLinkGridToDataSource;
    procedure FormCreate(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure BtTiraFotoClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure ExcluiTopico();
    procedure TiraFotoParaEnviarViaREST();
    procedure TiraFotoParaEnviarViaFTP();
  public
    { Public declarations }
    FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP,
    FProduto, FCodGrade, FOVcYnsExgCad, FOVcYnsExgTop, FOVcYnsMixTop, FIdTabela: Integer;
    FNO_Local, FNO_Artigo, FCodTam, FNO_Topico, FTabela: String;
    //
    procedure ReopenOVmItxMobFts();
  end;

var
  FmOViItxAct: TFmOViItxAct;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms, UnApp_Jan, UnFMX_DmkProcFunc,
  UnGrl_Consts, UnOVSM_Vars, UnApp_PF, UnOVSM_PF,
  Module, OViTclCenSel, UnFMX_Fotos;

{ TFmOViInspSeqMed }

procedure _ReopenOVmItxMobFts();
begin
  FmOViItxAct.ReopenOVmItxMobFts();
end;

procedure TFmOViItxAct.BtAlteraClick(Sender: TObject);
var
  CIM: Integer;
  LaNO_Contexto_Text: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  CIM        := FCtrlInMob;
  LaNO_Contexto_Text := '';
  //
  App_Jan.MostraOViItxLvr(CIM, LaItemAtual.Text,
    LaTotalItens.Text, FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP,
    FNrReduzidoOP, FProduto, FCodGrade, FNO_Local, FNO_Artigo, FCodTam,
    LaNO_Topico.Text, FOVcYnsExgCad, FOVcYnsExgTop, FOVcYnsMixTop,
    (*ReabreOViItxLvr*)False);
{
  App_Jan.MostraOViInspSeqNcfLvr(CIM, LaItemAtual.Text,
    LaTotalItens.Text, FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP,
    FNrReduzidoOP, FProduto, FCodGrade, FNO_Local, FNO_Artigo, FCodTam,
    LaNO_Contexto_Text, FOVcYnsChk, FOVcYnsChkCtx, FCadContexto,
    (*ReabreOViInspSeqNcfChk*)False);
}
  //
  FMX_DmkForms.FechaFm_AllOS0(FmOViItxAct);
end;

procedure TFmOViItxAct.BtExcluiClick(Sender: TObject);
var
  sPergunta: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  sPergunta := 'Deseja realmete excluir o item "' + FNO_Topico + '"?';
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      //MR := AResult;
      case AResult of
        mrYes: ExcluiTopico();
        mrNo: Grl_Geral.MB_Info('Edi��o n�o realizada!');
        mrCancel: Grl_Geral.MB_Info('Edi��o Abortada!');
      end;
    end);
end;

procedure TFmOViItxAct.BtTiraFotoClick(Sender: TObject);
const
  sProcFunc = 'TFmOViItxAct.BtTiraFotoClick()';
begin
  // TProtocoloEnvioDeImagem = (pdediIndef=0, pdediHTTP=1, pdediFTP=2, pdediREST=3);
  case VAR_PROTOCOLO_ENVIO_DE_IMAGEM of
    TProtocoloEnvioDeImagem.pdediFTP: TiraFotoParaEnviarViaFTP();
    TProtocoloEnvioDeImagem.pdediREST: TiraFotoParaEnviarViaREST();
    else Grl_Geral.MB_Aviso('"PROTOCOLO_ENVIO_DE_IMAGEM" indefinido em ' + sProcFunc);
  end;
end;

procedure TFmOViItxAct.ExcluiTopico();
const
  sProcName = 'TFmOViItxAct.ExcluiTopico()';
var
  Tabela: String;
begin
  if FTabela = CO_MEM_FLD_TAB_0_Faccao_Inconformidade then
    Tabela := 'ovmitxmobinc'
  else
  if FTabela = CO_MEM_FLD_TAB_2_Faccao_LivreTexto then
    Tabela := 'ovmitxmoblvr'
  else
(*
  if FTabela = CO_MEM_FLD_TAB_Medida then
    Tabela := 'ovmitxmobinc'
*)
  begin
    Grl_Geral.MB_Erro('Tabela "' + FTabela + '" n�o implementada em ' + sProcName);
    Exit;
  end;
  //
  if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.AllDB, [
  'DELETE FROM ' + Tabela,
  'WHERE CtrlInMob=' + Grl_Geral.FF0(FCtrlInMob),
  EmptyStr]) then
  begin
    FmOViTclCenSel.ReopenPontosNeg();
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViItxAct);
  end;
end;

procedure TFmOViItxAct.FormActivate(Sender: TObject);
begin
{
  Precisa
  FOVcYnsExgTop := 0;
  FOVcYnsMixTop := 0;
}
  ReopenOVmItxMobFts();
end;

procedure TFmOViItxAct.FormCreate(Sender: TObject);
begin
  FCtrlInMob := 0;
end;

procedure TFmOViItxAct.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViItxAct.ReopenOVmItxMobFts();
begin
  Grl_DmkDB.AbreSQLQuery0(QrOVmItxMobFts, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovmitxmobfts ',
  'WHERE IDTabela=' + Grl_Geral.FF0(FIDTabela),
  'AND CodInMob=' + Grl_Geral.FF0(FCodInMob),
  'AND CtrlInMob=' + Grl_Geral.FF0(FCtrlInMob),
  EmptyStr]);
end;

procedure TFmOViItxAct.TiraFotoParaEnviarViaFTP;
begin
(*
Application.CreateForm(TFmCamera, FmCamera);
  FmCamera.FTipoFile   := ttfPng;
  FmCamera.FFilePrefix := 'OVS_1_1_1_';
  FmCamera.FDirectory  := 'proforma';
  FmCamera.Show;
*)
end;

procedure TFmOViItxAct.TiraFotoParaEnviarViaREST();
const
  AcrescentaDataNoNome = True;
var
  IDFoto, NomeArq, SQLExec: String;
begin
(*
  IDFoto := 'OVS_' +
            Grl_Geral.FFN(FCodInMob, 10) + '_' +
            Grl_Geral.FFN(FCtrlInMob, 10);
*)
  IDFoto := OVSM_PF.DefineNomeArquivoFotoInconformidade(FCodInMob, FCtrlInMob, null);
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtSQLite, stIns, 'ovmitxmobfts', False, [
  'CodInMob', 'IdTabela', 'NomeArq'], [
  'CtrlInMob', 'DataHora'], [
  FCodInMob, FIdTabela, CO_JOKE_FOTO_ARQNOME], [
  FCtrlInMob, CO_JOKE_FOTO_DATAHORA], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
  False, SQLExec) then
  begin
  (*
    SQLExec := Grl_Geral.ATS(
    'INSERT INTO ovmitxmobfts (CodInMob, CtrlInMob, DataHora, NomeArq) Values ("0")';
    'INSERT INTO  SET ',
    'CodInMob=' + Grl_Geral.FF0(FCodInMob),
    'CtrlInMob=' + Grl_Geral.FF0(FCtrlInMob),
    'DataHora=:PDataHora',
    'NomeArq=:PNomeArq',
    EmptyStr]);
    //
*)
    if FMX_Fotos.MostraFormTiraFoto(IDFoto, AcrescentaDataNoNome, SQLExec, NomeArq,
    FIdTabela) then
    begin
      VAR_FOTO_COD_IN_MOB  := FCodInMob;
      VAR_FOTO_CTRL_IN_MOB := FCtrlInMob;
      VAR_FOTO_ID_TABELA   := FIdTabela;
      //
      App_PF.AcoesFinaisDeTirarFoto()
    end;
  end;
end;

end.
