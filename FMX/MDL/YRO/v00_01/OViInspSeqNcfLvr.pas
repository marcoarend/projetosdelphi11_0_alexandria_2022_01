unit OViInspSeqNcfLvr;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDmkEnums, UnOVS_Consts, FMX.Edit, FMX.ListBox;

type
  TFmOViInspSeqNcfLvr = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    Panel1: TPanel;
    Label1: TLabel;
    LaNO_Contexto: TLabel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Panel6: TPanel;
    Label7: TLabel;
    EdDescricao: TEdit;
    Panel4: TPanel;
    BtConfirma: TButton;
    CBMagnitude: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    function  DefinePontosNeg(Magnitude: Integer): Double;
  public
    { Public declarations }
    FReabreOViInspSeqNcfChk: Boolean;
    FCodInMob, FCtrlInMob, FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP,
    FProduto, FCodGrade, FOVcYnsChk, FOVcYnsChkCtx, FCadContexto: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
  end;

var
  FmOViInspSeqNcfLvr: TFmOViInspSeqNcfLvr;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms, UnFMX_DmkProcFunc,
  Module, OViIspCenSel, OViInspSeqNcfChk;

{ TFmOViInspSeqMed }

procedure TFmOViInspSeqNcfLvr.BtConfirmaClick(Sender: TObject);
//const
  //Codigo = 0; // s� no desktop
var
  Descricao: String;
  //CodInMob, CtrlInMob, PecaSeq,
  ItmMagnit, Topico, Contexto, Magnitude, PontNeg: Integer;
  SQLType: TSQLType;
begin
  FMX_DmkPF.VibrarComoBotao();
  ItmMagnit      := CBMagnitude.ItemIndex;
  if Grl_Geral.FIC(Trim(EdDescricao.Text) = EmptyStr, EdDescricao, 'Informe a descri��o!') then
    Exit;
  if Grl_Geral.FIC(ItmMagnit = -1, CBMagnitude, 'Informe a magnitude"') then
    Exit;
  //
  if FCtrlInMob <> 0 then
    SQLType        := stUpd
  else
    SQLType        := stIns;
  //Codigo         := ;
  //CodInMob       := ;
  //CtrlInMob      := ;
  //PecaSeq        := ;
  Descricao      := EdDescricao.Text;
  Contexto       := FCadContexto;
  Topico         := 0;
  Magnitude      := CBMagnitude.ItemIndex;
  case Magnitude of
   0: Magnitude := 1024;
   1: Magnitude := 2048;
   2: Magnitude := 3072;
   else
   begin
      Magnitude := 0;
      Grl_Geral.MB_Erro('Magnitude n�o implementada!');
      Exit;
   end;
  end;
  PontNeg        := Trunc(DefinePontosNeg(Magnitude));
  //
  //? := UMyMod.BPGS1I32('ovmispmoblvr', 'Codigo', 'CtrlInMob', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  FCtrlInMob := Grl_DmkDB.GetNxtCodigoInt('ovmispmoblvr', 'CtrlInMob', SQLType, FCtrlInMob);
  //if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovmispmoblvr', auto_increment?[
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'ovmispmoblvr', False, [
  'CodInMob', 'PecaSeq', 'Descricao',
  'Contexto', 'Topico', 'Magnitude',
  'PontNeg'], [
  (*'Codigo',*) 'CtrlInMob'], [
  FCodInMob, FPecaSeq, Descricao,
  Contexto, Topico, Magnitude,
  PontNeg], [
  (*Codigo,*) FCtrlInMob], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    //Grl_DmkDB.AbreQuery(FQrItem, DMod.AllDB);
    FmOViIspCenSel.ReopenPontosNeg();
    if FReabreOViInspSeqNcfChk then
      FmOViInspSeqNcfChk.ReopenItem();
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfLvr);
  end;
end;

function TFmOViInspSeqNcfLvr.DefinePontosNeg(Magnitude: Integer): Double;
begin
  // ovcynsqstmag
  case Magnitude of
    1024: Result := FmOViIspCenSel.QrOVcYnsARQCadPontosTole.Value;
    2048: Result := FmOViIspCenSel.QrOVcYnsARQCadPontosGrav.Value;
    3072: Result := FmOViIspCenSel.QrOVcYnsARQCadPontosCrit.Value;
    else
    begin
      Result := 0;
      Grl_Geral.MB_Erro('"Magnitude" n�o implementada em "TFmOViInspSeqNcfTop.DefinePontosNeg()"');
    end;
  end;
end;

procedure TFmOViInspSeqNcfLvr.FormCreate(Sender: TObject);
begin
  FCtrlInMob := 0;
  FReabreOViInspSeqNcfChk := False;
  try
    EdDescricao.SetFocus;
  except
    // nada!
  end;
end;

procedure TFmOViInspSeqNcfLvr.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

end.
