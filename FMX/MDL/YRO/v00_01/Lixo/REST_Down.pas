unit REST_Down;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.Controls.Presentation, FMX.StdCtrls,
  System.JSON, FMX.ScrollBox, FMX.Memo, UnGrl_Vars;

type
  TFmREST_Down = class(TForm)
    FDPragma: TFDQuery;
    Panel1: TPanel;
    BtBaixarTudo: TButton;
    MeAvisos: TMemo;
    BtFechar: TButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    procedure BtBaixarTudoClick(Sender: TObject);
    procedure Teste(st: String);
    procedure BtFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    sStrInfoDone, sKindInsp: String;
    //
    function  DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE: String;
              ExcluiItensTabela: Boolean = True): Boolean;
    procedure BaixarDadosInspecoesFaccoes();
    procedure BaixarDadosInspecoesTexteis();
    procedure InfoTempMemo(Texto: String);
    procedure InfoDoneMemo(Texto: String);
  public
    { Public declarations }
  end;

var
  FmREST_Down: TFmREST_Down;

implementation

uses UnGrl_DmkDB, UnFMX_Geral, UnFMX_DmkRemoteQuery, UnOVSM_Vars, UnGrl_Geral,
  UnOVS_Consts, UnFMX_DmkUnlic, UnFMX_DmkProcFunc,
  Principal,
  Module;

const
  NAO_EXCLUI = False;

{$R *.fmx}
{

procedure TFmREST_Down.BtBaixarTudoClick(Sender: TObject);
var
  procedure InfoTempMemo(Texto: String);
  begin
    MeAvisos.Text := Texto + '...' + sLineBreak + sStrInfoDone;
  end;
  procedure InfoDoneMemo(Texto: String);
  begin
    Application.ProcessMessages;
    sStrInfoDone := Grl_Geral.FDT(Now(), 109) + ' : ' +Texto + '!' + sLineBreak + sStrInfoDone;
    MeAvisos.Text := sStrInfoDone;
  end;
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
var
  SQL, JSON: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  BtBaixarTudo.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    sStrInfoDone := '';
    InfoTempMemo('Preparando downloads...');
    Alias := '';
    SQL_JOIN := '';
    SQL_WHERE := 'WHERE ZtatusIsp = ' +
      FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
    InfoTempMemo('Baixando configura��es de inspe��es');
    if DownloadTabela('ovgispgercab', Alias, 'ovgispgercab', SQL_JOIN, SQL_WHERE) then
    begin
      InfoDoneMemo('Configura��es de inspe��es baixadas');
      ////////////////////////////////////////////////////////////////////////////
      ///
      /////////////////////   ovdlocal   /////////////////////////////////////////
      InfoTempMemo('Preparando baixa de locais das inspe��es ativas e finalizadas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Local ',
      'FROM ovmispmobcab ',
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT Local ',
      'FROM ovgispgercab ',
      EmptyStr]);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Local');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando locais das inspe��es ativas');
        if DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Locais das inspe��es ativas baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      ///
      /////////////////////   ovdreferencia   ////////////////////////////////////
      InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT SeqGrupo ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'SeqGrupo');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas');
        if DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      ///
      /////////////////////   ovfordemproducao ///////////////////////////////////
      InfoTempMemo('Preparando baixa de reduzidos de OP das inspe��es ativas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT NrReduzidoOP ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'NrReduzidoOP');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Corda + ')';
        InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas');
        if DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////  Itens de Planos de Amostragem e Regime de Qualidade   ///////////
      InfoTempMemo('Preparando baixa itens de Planos de Amostragem e Regime de Qualidade');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsARQ ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade');
        if DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de Planos de Amostragem e Regime de Qualidade baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////////  Planos de Amostragem e Regime de Qualidade   ////////////////
      InfoTempMemo('Preparando baixa de Cabe�alhos de Planos de Amostragem e Regime de Qualidade');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsARQ ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade');
        if DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cabe�alhos de Planos de Amostragem e Regime de Qualidade baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ///
      /////////////////////  Itens de t�picos de Medidas   ///////////////////////
      InfoTempMemo('Preparando baixa de itens de t�picos de Medidas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsMed ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando itens de t�picos de Medidas');
        if DownloadTabela('ovcynsmedtop', Alias, 'ovcynsmedtop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de t�picos de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ////////////////  Itens de medida de t�picos de Medidas   //////////////////
      InfoTempMemo('Preparando baixa de itens de medida de t�picos de Medidas');
  (*  J� foi no item acima!
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsMed ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando itens de medida de t�picos de Medidas');
        if DownloadTabela('ovcynsmeddim', Alias, 'ovcynsmeddim', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de medida de t�picos de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////////////////  Cadastros de tabelas de Medidas   ///////////////////////
      InfoTempMemo('Preparando baixa de cadastros de tabelas de Medidas');
  (*  J� foi no item acima!
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsMed ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de tabelas de Medidas');
        if DownloadTabela('ovcynsmedcad', Alias, 'ovcynsmedcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de cadastros de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////////////////  Cadastros de t�picos de Medidas   ///////////////////////
      InfoTempMemo('Preparando baixa de cadastros de t�picos de Medidas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Tobiko ',
      'FROM ovcynsmedtop ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Tobiko');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de t�picos de Medidas');
        if DownloadTabela('ovcynsgratop', Alias, 'ovcynsgratop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de cadastros de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de contextos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de contextos de inconformidades');
        if DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de check lists de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de check lists de inconformidades');
        if DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de check lists de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de contextos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de checklists de inconformidades');
  (*
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de contextos de de checklists inconformidades');
        if DownloadTabela('ovcynschkctx', Alias, 'ovcynschkctx', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de contextos de checklists de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de t�picos de checklists de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de t�picos de checklists de inconformidades');
  (*
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de t�picos de checklists de inconformidades');
        if DownloadTabela('ovcynschktop', Alias, 'ovcynschktop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de t�picos de checklists de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ///
      //////////////  Cadastros de contextos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Contexto ',
      'FROM ovcynschkctx ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Contexto');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de contextos de inconformidades');
        if DownloadTabela('ovcynsqstctx', Alias, 'ovcynsqstctx', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de t�picos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de t�picos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Topico ',
      'FROM ovcynschktop ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Topico');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de t�picos de inconformidades');
        if DownloadTabela('ovcynsqsttop', Alias, 'ovcynsqsttop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de t�picos de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      //
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      ///////////  D A D O S   B U S C A D O S   D O  ovfordemproducao ///////////
      ////////////////////////////////////////////////////////////////////////////
      ///
      ///////////////////////////   ovdproduto  //////////////////////////////////
      InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Produto ',
      'FROM ovfordemproducao ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Produto');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Controle IN (' + Corda + ')';
        InfoTempMemo('Baixando produtos usados nas inspe��es ativas');
        if DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ///////////////////////////   ovdproduto  //////////////////////////////////
      InfoTempMemo('Preparando baixa de magnitudes de inconformidades');
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := '';
        InfoTempMemo('Baixando magnitudes de inconformidades');
        if DownloadTabela('ovcynsqstmag', Alias, 'ovcynsqstmag', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Magnitudes de inconformidades baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      ///
      ////////////////////////////////////////////////////////////////////////////
      ///////////  D A D O S   G E R A D O S   N O  OVERSEER  ////////////////////
      ////////////////////////////////////////////////////////////////////////////
      ///







      // No final!
      InfoDoneMemo('------------------------------------------');
      InfoDoneMemo('------------------------------------------');
      InfoDoneMemo('');
      InfoDoneMemo('Todas tabelas foram baixadas!');
      InfoDoneMemo('');
      InfoDoneMemo('');
      BtFechar.Visible := True;
      BtBaixarTudo.Visible := False;
    end;
  finally
    BtBaixarTudo.Enabled := True;
  end;
end;
}

procedure TFmREST_Down.BaixarDadosInspecoesFaccoes();
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
begin
  Alias := '';
  SQL_JOIN := '';
  sKindInsp := 'Fac��es';
  SQL_WHERE := 'WHERE ZtatusIsp = ' +
    FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
  //
  InfoTempMemo('Baixando configura��es de inspe��es');
  if DownloadTabela('ovgispgercab', Alias, 'ovgispgercab', SQL_JOIN, SQL_WHERE) then
  begin
    InfoDoneMemo('Configura��es de inspe��es baixadas');
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdlocal   /////////////////////////////////////////
    InfoTempMemo('Preparando baixa de locais das inspe��es ativas e finalizadas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Local ',
    'FROM ovmispmobcab ',
    ' ',
    'UNION ',
    ' ',
    'SELECT DISTINCT Local ',
    'FROM ovgispgercab ',
    EmptyStr]);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Local');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando locais das inspe��es ativas');
      if DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Locais das inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdreferencia   ////////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT SeqGrupo ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'SeqGrupo');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas');
      if DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovfordemproducao ///////////////////////////////////
    InfoTempMemo('Preparando baixa de reduzidos de OP das inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT NrReduzidoOP ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'NrReduzidoOP');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Corda + ')';
      InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas');
      if DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////  Itens de Planos de Amostragem e Regime de Qualidade   ///////////
    InfoTempMemo('Preparando baixa itens de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade');
      if DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de Planos de Amostragem e Regime de Qualidade baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////////  Planos de Amostragem e Regime de Qualidade   ////////////////
    InfoTempMemo('Preparando baixa de Cabe�alhos de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade');
      if DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cabe�alhos de Planos de Amostragem e Regime de Qualidade baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    ///
    /////////////////////  Itens de t�picos de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de itens de t�picos de Medidas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de t�picos de Medidas');
      if DownloadTabela('ovcynsmedtop', Alias, 'ovcynsmedtop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de t�picos de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    ////////////////  Itens de medida de t�picos de Medidas   //////////////////
    InfoTempMemo('Preparando baixa de itens de medida de t�picos de Medidas');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de medida de t�picos de Medidas');
      if DownloadTabela('ovcynsmeddim', Alias, 'ovcynsmeddim', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de medida de t�picos de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////////////////  Cadastros de tabelas de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de tabelas de Medidas');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de tabelas de Medidas');
      if DownloadTabela('ovcynsmedcad', Alias, 'ovcynsmedcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////////////////  Cadastros de t�picos de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de Medidas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Tobiko ',
    'FROM ovcynsmedtop ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Tobiko');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de Medidas');
      if DownloadTabela('ovcynsgratop', Alias, 'ovcynsgratop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de inconformidades');
      if DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de check lists de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de check lists de inconformidades');
      if DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de check lists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de checklists de inconformidades');
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de de checklists inconformidades');
      if DownloadTabela('ovcynschkctx', Alias, 'ovcynschkctx', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de checklists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de t�picos de checklists de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de checklists de inconformidades');
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de checklists de inconformidades');
      if DownloadTabela('ovcynschktop', Alias, 'ovcynschktop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de t�picos de checklists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Contexto ',
    'FROM ovcynschkctx ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Contexto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de inconformidades');
      if DownloadTabela('ovcynsqstctx', Alias, 'ovcynsqstctx', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de t�picos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Topico ',
    'FROM ovcynschktop ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Topico');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de inconformidades');
      if DownloadTabela('ovcynsqsttop', Alias, 'ovcynsqsttop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de t�picos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    //
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///////////  D A D O S   B U S C A D O S   D O  ovfordemproducao ///////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ///////////////////////////   ovdproduto  //////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Produto ',
    'FROM ovfordemproducao ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Produto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Controle IN (' + Corda + ')';
      InfoTempMemo('Baixando produtos usados nas inspe��es ativas');
      if DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
  end;
end;

procedure TFmREST_Down.BaixarDadosInspecoesTexteis();
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
begin
  Alias := '';
  SQL_JOIN := '';
  sKindInsp := 'T�xteis';
  SQL_WHERE := 'WHERE ZtatusIsp = ' +
    FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
  //
  InfoTempMemo('Baixando configura��es de inspe��es');
  if DownloadTabela('ovgitxgercab', Alias, 'ovgitxgercab', SQL_JOIN, SQL_WHERE) then
  begin
    InfoDoneMemo('Configura��es de inspe��es baixadas');
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdlocal   /////////////////////////////////////////
    InfoTempMemo('Preparando baixa de locais das inspe��es ativas e finalizadas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Local ',
    'FROM ovmitxmobcab ',
    ' ',
    'UNION ',
    ' ',
    'SELECT DISTINCT Local ',
    'FROM ovgitxgercab ',
    EmptyStr]);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Local');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando locais das inspe��es ativas');
      if DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE, NAO_EXCLUI) then
      begin
        InfoDoneMemo('Locais das inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
{
    /////////////////////   ovdreferencia   ////////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT SeqGrupo ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'SeqGrupo');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas');
      if DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovfordemproducao ///////////////////////////////////
    InfoTempMemo('Preparando baixa de reduzidos de OP das inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT NrReduzidoOP ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'NrReduzidoOP');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Corda + ')';
      InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas');
      if DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////  Itens de Planos de Amostragem e Regime de Qualidade   ///////////
    InfoTempMemo('Preparando baixa itens de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade');
      if DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de Planos de Amostragem e Regime de Qualidade baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////////  Planos de Amostragem e Regime de Qualidade   ////////////////
    InfoTempMemo('Preparando baixa de Cabe�alhos de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade');
      if DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cabe�alhos de Planos de Amostragem e Regime de Qualidade baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    ///
    /////////////////////  Itens de t�picos de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de itens de t�picos de Medidas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de t�picos de Medidas');
      if DownloadTabela('ovcynsmedtop', Alias, 'ovcynsmedtop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de t�picos de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    ////////////////  Itens de medida de t�picos de Medidas   //////////////////
    InfoTempMemo('Preparando baixa de itens de medida de t�picos de Medidas');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de medida de t�picos de Medidas');
      if DownloadTabela('ovcynsmeddim', Alias, 'ovcynsmeddim', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de medida de t�picos de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////////////////  Cadastros de tabelas de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de tabelas de Medidas');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de tabelas de Medidas');
      if DownloadTabela('ovcynsmedcad', Alias, 'ovcynsmedcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    /////////////////////  Cadastros de t�picos de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de Medidas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Tobiko ',
    'FROM ovcynsmedtop ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Tobiko');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de Medidas');
      if DownloadTabela('ovcynsgratop', Alias, 'ovcynsgratop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de inconformidades');
      if DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de check lists de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de check lists de inconformidades');
      if DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de check lists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de checklists de inconformidades');
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de de checklists inconformidades');
      if DownloadTabela('ovcynschkctx', Alias, 'ovcynschkctx', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de checklists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de t�picos de checklists de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de checklists de inconformidades');
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
*)
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de checklists de inconformidades');
      if DownloadTabela('ovcynschktop', Alias, 'ovcynschktop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de t�picos de checklists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Contexto ',
    'FROM ovcynschkctx ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Contexto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de inconformidades');
      if DownloadTabela('ovcynsqstctx', Alias, 'ovcynsqstctx', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ///
    //////////////  Cadastros de t�picos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Topico ',
    'FROM ovcynschktop ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Topico');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de inconformidades');
      if DownloadTabela('ovcynsqsttop', Alias, 'ovcynsqsttop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de t�picos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    //
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///////////  D A D O S   B U S C A D O S   D O  ovfordemproducao ///////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ///////////////////////////   ovdproduto  //////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Produto ',
    'FROM ovfordemproducao ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Produto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Controle IN (' + Corda + ')';
      InfoTempMemo('Baixando produtos usados nas inspe��es ativas');
      if DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
}
  end;
end;

procedure TFmREST_Down.BtBaixarTudoClick(Sender: TObject);
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
var
  SQL, JSON: String;
begin
  sStrInfoDone := '';
  sKindInsp    := '';
  FMX_DmkPF.VibrarComoBotao();
  BtBaixarTudo.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    InfoTempMemo('Preparando downloads...');
    Alias := '';
    SQL_JOIN := '';
    SQL_WHERE := 'WHERE ZtatusIsp = ' +
      FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////  D A D O S   P A R A   I N S P E � � O   D E   F A C � � O  ////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    BaixarDadosInspecoesFaccoes();
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////  D A D O S   P A R A   I N S P E � � O   D E   T � X T I L  ////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    BaixarDadosInspecoesTexteis();
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////  D A D O S   P A R A   Q U A L Q U E R   I N S P E � � O  //////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////  ovcynsqstmag  ////////////////////////////////
    sKindInsp := 'Comum';
    InfoTempMemo('Preparando baixa de magnitudes de inconformidades');
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := '';
      InfoTempMemo('Baixando magnitudes de inconformidades');
      if DownloadTabela('ovcynsqstmag', Alias, 'ovcynsqstmag', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Magnitudes de inconformidades baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////////  D A D O S   G E R A D O S   N O  OVERSEER  ////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    // Nada!






      // No final!
    sKindInsp := '';
    InfoDoneMemo('------------------------------------------');
    InfoDoneMemo('------------------------------------------');
    InfoDoneMemo('');
    InfoDoneMemo('Todas tabelas foram baixadas!');
    InfoDoneMemo('');
    InfoDoneMemo('');
    BtFechar.Visible := True;
    BtBaixarTudo.Visible := False;
  finally
    BtBaixarTudo.Enabled := True;
  end;
end;

procedure TFmREST_Down.BtFecharClick(Sender: TObject);
begin
  Close;
end;

{
function TFmREST_Down.DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE:
  String): Boolean;
var
  Campos, Lit, SQL, JSON, _Alias, MyAlias: String;
begin
  Result := False;
  Campos := '';
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := FDPragma.FieldByName('Name').AsString + sLineBreak;
  SQL := MyAlias + FDPragma.FieldByName('Name').AsString;
  Lit := FDPragma.FieldByName('Name').AsString;
  FDPragma.Next;
  while not FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + FDPragma.FieldByName('Name').AsString + sLineBreak;
    SQL := SQL + ', ' + MyAlias + FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    FDPragma.Next;
  end;
  //
  //FMX_Geral.MB_Info(SQL);
  //FMX_Geral.MB_Info(Lit);
  SQL :=
    'SELECT ' + SQL + sLineBreak +
    'FROM ' + TabSrc + _Alias + sLineBreak +
    SQL_JOIN + sLineBreak +
    SQL_WHERE + '';
  //
  Lit := 'INSERT INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  //Grl_Geral.MB_Aviso(SQL);
  if FMX_DmkRemoteQuery.JsonText([SQL], JSON) then
  begin
    //FMX_Geral.MB_Info(JSON);
    Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, ['DELETE FROM ' + TabDst]);
    if JSON <> '[]' then
    begin
      Lit := Lit + FMX_DmkRemoteQuery.ParseJson2(JSON);
      //FMX_Geral.MB_Info(Lit);
      Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, [Lit]);
      //
    end;
    Result := True;
  end;
end;
}

function TFmREST_Down.DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE:
  String; ExcluiItensTabela: Boolean): Boolean;
var
  Campos, Lit, SQL, JSON, _Alias, MyAlias: String;
begin
  Result := False;
  Campos := '';
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := FDPragma.FieldByName('Name').AsString + sLineBreak;
  SQL := MyAlias + FDPragma.FieldByName('Name').AsString;
  Lit := FDPragma.FieldByName('Name').AsString;
  FDPragma.Next;
  while not FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + FDPragma.FieldByName('Name').AsString + sLineBreak;
    SQL := SQL + ', ' + MyAlias + FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    FDPragma.Next;
  end;
  //
  //FMX_Geral.MB_Info(SQL);
  //FMX_Geral.MB_Info(Lit);
  SQL :=
    'SELECT ' + SQL + sLineBreak +
    'FROM ' + TabSrc + _Alias + sLineBreak +
    SQL_JOIN + sLineBreak +
    SQL_WHERE + '';
  //
  Lit := 'INSERT INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  //Grl_Geral.MB_Aviso(SQL);
  if FMX_DmkRemoteQuery.JsonText([SQL], JSON) then
  begin
    //FMX_Geral.MB_Info(JSON);
    if ExcluiItensTabela then
      Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, ['DELETE FROM ' + TabDst]);
    if JSON <> '[]' then
    begin
      Lit := Lit + FMX_DmkRemoteQuery.ParseJson2(JSON);
      //FMX_Geral.MB_Info(Lit);
      Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, [Lit]);
      //
    end;
    Result := True;
  end;
end;

procedure TFmREST_Down.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmREST_Down.FormShow(Sender: TObject);
begin
  BtFechar.Visible := False;
  BtBaixarTudo.Visible := True;
  MeAvisos.Lines.Clear;
end;

procedure TFmREST_Down.InfoDoneMemo(Texto: String);
begin
  Application.ProcessMessages;
  sStrInfoDone := Grl_Geral.FDT(Now(), 109) + ' : ' + sKindInsp + ' ' + Texto +
    '!' + sLineBreak + sStrInfoDone;
  MeAvisos.Text := sStrInfoDone;
end;

procedure TFmREST_Down.InfoTempMemo(Texto: String);
begin
  MeAvisos.Text := sKindInsp + ' ' + Texto + '...' + sLineBreak + sStrInfoDone;
end;

procedure TFmREST_Down.Teste(st: String);
//procedure ParseJSonValue;
var
  JSonValue: TJSonValue;
  Codigo, Local: String;
  Branch: String;
  I: Integer;
  //
  LRows, LElements, LItem : TJSONValue;
  LJsonObj, JSonObject: TJSONObject;
begin
   JSonObject := TJSonObject.Create;
   JsonValue := JSonObject.ParseJSONValue(st);
   FMX_Geral.MB_Info(FMX_Geral.FF0(JSonObject.Count));
(*
  LJsonObj    := TJSONObject.ParseJSONValue(st,0, True) as TJSONObject;
  //try
     LRows:=LJsonObj.Get(0).JsonValue;
     LElements:=TJSONObject(TJSONArray(LRows).Get(0)).Get(0).JsonValue;
     LItem :=TJSONObject(TJSONArray(LElements).Get(0)).Get(0).JsonValue;
     //ShowMessage(TJSONObject(LItem).Get('buying_rate').JsonValue.Value);
     FMX_Geral.MB_Info(FMX_Geral.FF0(LJsonObj.Count));
*)


// [{"Codigo":"1","Nome":null,....}....]
   //st := '{"data":{"results":[{"Branch":"ACCT590003"}]}}';
   //JsonValue := TJSonObject.ParseJSONValue(st);
   for I := 0 to 1 do
   begin
     Codigo := JsonValue.GetValue<string>('[' + FMX_Geral.FF0(I) + '].Codigo');
     Local := JsonValue.GetValue<string>('[' + FMX_Geral.FF0(I) + '].Local');
     FMX_Geral.MB_Info(Codigo + ' >> ' + Local);
   end;
   JsonValue.Free;
   JSonObject.Free;
end;

end.
