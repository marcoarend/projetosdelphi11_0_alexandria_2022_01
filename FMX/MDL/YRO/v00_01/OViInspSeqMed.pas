unit OViInspSeqMed;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors,
  UnDMkEnums, UnGrl_Vars;

type
  TFmOViInspSeqMed = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    GrItem: TGrid;
    QrItem: TFDQuery;
    QrItemNome: TStringField;
    QrItemTobiko: TIntegerField;
    QrItemTolerBasCalc: TShortintField;
    QrItemTolerUnMdida: TShortintField;
    QrItemTolerRndPerc: TFloatField;
    QrItemCodigo: TIntegerField;
    QrItemControle: TIntegerField;
    QrItemConta: TIntegerField;
    QrItemCodGrade: TIntegerField;
    QrItemCodTam: TStringField;
    QrItemMedidCer: TFloatField;
    QrItemMedidMax: TFloatField;
    QrItemMedidMin: TFloatField;
    QrItemCodInMob: TIntegerField;
    QrItemCtrlInMob: TIntegerField;
    QrItemPecaSeq: TIntegerField;
    QrItemOVcYnsMedDim: TIntegerField;
    QrItemMedidFei: TFloatField;
    QrItemMedidOky: TShortintField;
    QrItemPontNeg: TIntegerField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    QrItemPontosCrit: TIntegerField;
    Panel4: TPanel;
    BtContinua: TButton;
    QrItemOVcYnsMedTop: TIntegerField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrItemBRK_Nome: TStringField;
    procedure GrItemCellClick(const Column: TColumn; const Row: Integer);
    procedure BtContinuaClick(Sender: TObject);
    procedure QrItemAfterOpen(DataSet: TDataSet);
    procedure QrItemCalcFields(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure GrItemResize(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FCodInMob, (*FOVcYnsMedDim,*) FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP, FProduto,
    FCodGrade, FOVcYnsMed: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    //
    //
    procedure ReopenItem(Item: Integer);
  end;

var
  FmOViInspSeqMed: TFmOViInspSeqMed;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms, UnFMX_DmkProcFunc, UnOVSM_Vars,
  Module,
  OViInspSeqMedEdit, OViIspCenSel;

{ TFmOViInspSeqMed }

procedure TFmOViInspSeqMed.BtContinuaClick(Sender: TObject);
var
  SubInspDone, CodInMob, _CIM: Integer;
  Proximo: Boolean;
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  Proximo     := False;
  SubInspDone := 1; // 1 = Medidas feitas
  CodInMob    := FCodInMob;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
  'SubInspDone'], [
  'CodInMob'], [
  SubInspDone], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    //if
    FmOViIspCenSel.LoacalizaAberto_QrOVmIspMobCab(Proximo, _CIM);
    FmOViIspCenSel.BtContinua.Text := 'Continua';
    FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqMed);
  end;
end;

procedure TFmOViInspSeqMed.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViInspSeqMed.GrItemCellClick(const Column: TColumn;
  const Row: Integer);
var
  Medida: Double;
  Min, Max: String;
begin
  if (QrItem.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqMedEdit, FmOViInspSeqMedEdit,
  fcmOnlyCreate, True, False) then
  begin
    FmOViInspSeqMedEdit.FQrItem := QrItem;
{
    FmOViInspSeqMedEdit.FLocal        := FLocal;
    FmOViInspSeqMedEdit.FArtigo       := FArtigo;
    FmOViInspSeqMedEdit.FNrOP         := FNrOP;
    FmOViInspSeqMedEdit.FNrReduzidoOP := FNrReduzidoOP;
    FmOViInspSeqMedEdit.FProduto      := FProduto;
    FmOViInspSeqMedEdit.FCodGrade     := FCodGrade;
    FmOViInspSeqMedEdit.FNO_Local     := FNO_Local;
    FmOViInspSeqMedEdit.FNO_Artigo    := FNO_Artigo;
    FmOViInspSeqMedEdit.FCodGrade     := FCodGrade;
    FmOViInspSeqMedEdit.FCodTam       := FCodTam;
    FmOViInspSeqMedEdit.FOVcYnsMed    := QrOVgIspGerCabOVcYnsMed.Value;
    //
}
    //FmOViInspSeqMedEdit.LaLocal.Text    := NO_Local;
    FmOViInspSeqMedEdit.LaArtigo.Text     := FNO_Artigo;
    FmOViInspSeqMedEdit.LaNrOP.Text       := Grl_Geral.FF0(FNrOP);
    FmOViInspSeqMedEdit.LaTamCad.Text     := FCodTam;
    FmOViInspSeqMedEdit.LaItemAtual.Text  := LaItemAtual.Text;
    FmOViInspSeqMedEdit.LaTotalItens.Text := LaTotalItens.Text;
    //
    FmOViInspSeqMedEdit.LaNO_Topico.Text  := QrItemNome.Value;
    //
    FmOViInspSeqMedEdit.CodInMob          := FCodInMob; //QrItemCodInMob.Value;
    //
    //FmOViInspSeqMedEdit.OVcYnsMedDim      := FOVcYnsMed; //QrItemOVcYnsMedDim.Value;
    FmOViInspSeqMedEdit.OVcYnsMedDim      := QrItemConta.Value;
    //
    FmOViInspSeqMedEdit.CtrlInMob         := QrItemCtrlInMob.Value;
    FmOViInspSeqMedEdit.OVcYnsMedTop      := QrItemOVcYnsMedTop.Value;
    FmOViInspSeqMedEdit.Tobiko            := QrItemTobiko.Value;
    FmOViInspSeqMedEdit.PecaSeq           := FPecaSeq;
    FmOViInspSeqMedEdit.CodGrade          := QrItemCodGrade.Value;
    FmOViInspSeqMedEdit.CodTam            := QrItemCodTam.Value;
    FmOViInspSeqMedEdit.PontosCrit        := QrItemPontosCrit.Value;
    FmOViInspSeqMedEdit.MedidOky          := 0;
    FmOViInspSeqMedEdit.PontNeg           := 0;
    FmOViInspSeqMedEdit.MedidCer          := QrItemMedidCer.Value;
    FmOViInspSeqMedEdit.MedidMin          := QrItemMedidMin.Value;
    FmOViInspSeqMedEdit.MedidMax          := QrItemMedidMax.Value;
    FmOViInspSeqMedEdit.LaMedidCer.Text   := FloatToStr(QrItemMedidCer.Value);
    FmOViInspSeqMedEdit.LaMedidMin.Text   := FloatToStr(QrItemMedidMin.Value);
    FmOViInspSeqMedEdit.LaMedidMax.Text   := FloatToStr(QrItemMedidMax.Value);
    //
    if QrItemMedidFei.Value > 0 then
      Medida := QrItemMedidFei.Value
    else
      Medida := QrItemMedidCer.Value;
    //
    FmOViInspSeqMedEdit.EdMedidFei.Text   := Grl_Geral.FFT(Medida, VAR_MEDIDAS_CASAS_DECIMAIS, siNegativo);
    //FmOViInspSeqMedEdit.EdMedidFei.Text   := '';
    Min := Grl_Geral.FFT(VAR_MEDIDAS_INCREMENTO_MIN, VAR_MEDIDAS_CASAS_DECIMAIS, siNegativo);
    Max := Grl_Geral.FFT(VAR_MEDIDAS_INCREMENTO_MAX, VAR_MEDIDAS_CASAS_DECIMAIS, siNegativo);
    FmOViInspSeqMedEdit.BtMnosMin.Text    := '-' + Min;
    FmOViInspSeqMedEdit.BtMnosMax.Text    := '-' + Max;
    FmOViInspSeqMedEdit.BtMaisMin.Text    := '+' + Min;
    FmOViInspSeqMedEdit.BtMaisMax.Text    := '+' + Max;
    //
    FMX_DmkForms.ShowModal(FmOViInspSeqMedEdit);
  end;
end;

procedure TFmOViInspSeqMed.GrItemResize(Sender: TObject);
begin
  {$IfDef MSWINDOWS}
  //
  {$Else}
  //Grl_Geral.MB_Info('W = ' + Grl_Geral.FFT(GrItem.Width, 2, siNegativo));
  GrItem.Scale.X := VAR_SCALE_X;
  GrItem.Scale.Y := VAR_SCALE_Y;
  {$EndIf}
end;

procedure TFmOViInspSeqMed.QrItemAfterOpen(DataSet: TDataSet);
var
  Tudo, Done: Integer;
  AchowZeroOuFim: Boolean;
begin
  QrItem.DisableControls;
  try
    Tudo := 0;
    Done := 0;
    QrItem.First;
    while not QrItem.Eof do
    begin
      Tudo := Tudo + 1;
      if QrItemCtrlInMob.Value <> 0 then
        Done := Done + 1;
      //
       QrItem.Next;
    end;
    //
    BtContinua.Enabled := (Tudo = Done) or ((Done = 0) and VAR_PERMITE_NAO_MEDIR);
    QrItem.First;
    AchowZeroOuFim := False;
    //FQrItem.Locate('MedidFei', 0.00, []);
    while AchowZeroOuFim = False do
    begin
      if (QrItem.Eof) or (QrItemMedidFei.Value < 0.001) then
        AchowZeroOuFim := True
      else
        QrItem.Next;
    end;
  finally
    QrItem.EnableControls;
  end;
end;

procedure TFmOViInspSeqMed.QrItemCalcFields(DataSet: TDataSet);
begin
  QrItemBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrItem, 0, QrItemNome.Value);
end;

procedure TFmOViInspSeqMed.ReopenItem(Item: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrItem, Dmod.AllDB, [
  'SELECT ygt.Nome, top.Controle OVcYnsMedTop, ',
  'top.Tobiko, top.TolerBasCalc, top.TolerUnMdida, ',
  'top.TolerRndPerc, dim.Codigo, dim.Controle, dim.Conta, ',
  'dim.CodGrade, dim.CodTam, dim.MedidCer, dim.MedidMax, ',
  'dim.MedidMin, ymc.PontosCrit, imm.* ',
  'FROM ovcynsmeddim dim ',
  'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=dim.Codigo ',
  'LEFT JOIN ovcynsmedtop top ON top.Controle=dim.Controle ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=top.Tobiko ',
  'LEFT JOIN OVmIspMobMed imm ON imm.OVcYnsMedDim=dim.Conta ',
  '  AND imm.CodGrade=dim.CodGrade AND imm.CodTam=dim.CodTam ',
  '  AND imm.PecaSeq=' + Grl_Geral.FF0(Item) +
  '  AND imm.CodInMob=' + Grl_Geral.FF0(FCodInMob),
  'WHERE dim.Codigo=' + Grl_Geral.FF0(FOVcYnsMed),
  'AND dim.CodGrade=' + Grl_Geral.FF0(FCodGrade),
  'AND dim.CodTam="' + FCodTam + '"',
  'ORDER BY ygt.Codigo ',
  '']);
end;

end.
