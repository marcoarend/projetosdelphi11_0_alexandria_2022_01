unit OViTclCenSel;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UnDmkEnums, System.Rtti,
  FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors, UnOVS_Consts,
  FMX.Gestures, FMX.DialogService, FMX.Edit, FMX.Memo,
  UnProjGroupEnums, FMX.Layouts;

type
  TSubInspDone = (sidNadaPronto=0, sidMedidasFeitas=1, sidInconformidadesAvaliadas=2);
  TFmOViTclCenSel = class(TForm)
    TopToolbar: TToolBar;
    DoneButton: TSpeedButton;
    PnLocal: TPanel;
    Label1: TLabel;
    LaLocal: TLabel;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel1: TPanel;
    Label5: TLabel;
    LaPtsNegativos: TLabel;
    Label7: TLabel;
    LaPtsAprovRepr: TLabel;
    Panel2: TPanel;
    PnItemAtual: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    PnInspecao: TPanel;
    QrOVgItxGerCab: TFDQuery;
    QrOVmItxMobCab: TFDQuery;
    QrOVfOrdemProducao: TFDQuery;
    QrOVfOrdemProducaoNrReduzidoOP: TIntegerField;
    QrOVfOrdemProducaoEmpresa: TIntegerField;
    QrOVfOrdemProducaoCiclo: TIntegerField;
    QrOVfOrdemProducaoNrOP: TIntegerField;
    QrOVfOrdemProducaoLocal: TIntegerField;
    QrOVfOrdemProducaoProduto: TIntegerField;
    QrOVfOrdemProducaoSeqGrupo: TIntegerField;
    QrOVfOrdemProducaoTipoOP: TIntegerField;
    QrOVfOrdemProducaoPrioridade: TIntegerField;
    QrOVfOrdemProducaoNrSituacaoOP: TIntegerField;
    QrOVfOrdemProducaoDtInclusao: TDateField;
    QrOVfOrdemProducaoDtPrevisao: TDateField;
    QrOVfOrdemProducaoTipoLocalizacao: TIntegerField;
    QrOVfOrdemProducaoDtEntrada: TDateField;
    QrOVfOrdemProducaoQtReal: TFloatField;
    QrOVfOrdemProducaoQtLocal: TFloatField;
    QrOVfOrdemProducaoCodCategoria: TIntegerField;
    QrOVfOrdemProducaoNrLote: TIntegerField;
    QrOVfOrdemProducaoNrTipoProducaoOP: TStringField;
    QrOVfOrdemProducaoDtPrevRet: TDateField;
    QrOVfOrdemProducaoCodPessoa: TIntegerField;
    QrOVfOrdemProducaoReInsrt: TIntegerField;
    QrOVfOrdemProducaoLastInsrt: TDateTimeField;
    QrOVfOrdemProducaoDestino: TStringField;
    QrItem: TFDQuery;
    QrItemNome: TStringField;
    QrItemTobiko: TIntegerField;
    QrItemTolerBasCalc: TShortintField;
    QrItemTolerUnMdida: TShortintField;
    QrItemTolerRndPerc: TFloatField;
    QrItemCodigo: TIntegerField;
    QrItemControle: TIntegerField;
    QrItemConta: TIntegerField;
    QrItemCodGrade: TIntegerField;
    QrItemCodTam: TStringField;
    QrItemMedidCer: TFloatField;
    QrItemMedidMax: TFloatField;
    QrItemMedidMin: TFloatField;
    QrItemCodInMob: TIntegerField;
    QrItemCtrlInMob: TIntegerField;
    QrItemPecaSeq: TIntegerField;
    QrItemOVcYnsMedDim: TIntegerField;
    QrItemMedidFei: TFloatField;
    QrItemMedidOky: TShortintField;
    QrItemPontNeg: TIntegerField;
    QrPontosNeg: TFDQuery;
    QrPontosNegTabela: TWideStringField;
    QrPontosNegCtrlInMob: TLargeintField;
    QrPontosNegPecaSeq: TLargeintField;
    QrPontosNegNO_Topico: TWideStringField;
    QrPontosNegPontNeg: TLargeintField;
    Grid1: TGrid;
    BtContinua: TButton;
    BLPontosNeg: TBindingsList;
    BindSourceDB1: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    _TmContinua: TTimer;
    BtEncerra: TButton;
    LaSubInspDone: TLabel;
    QrPontosNegBRK_Topico: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    BtFirst: TButton;
    BtNext: TButton;
    Label8: TLabel;
    LaQtLocal: TLabel;
    BtPrior: TButton;
    BtLast: TButton;
    BtPeca: TButton;
    PnPeca: TPanel;
    BtPcOK: TButton;
    Label10: TLabel;
    EdPeca: TEdit;
    PnNome: TPanel;
    Label11: TLabel;
    MeNome: TMemo;
    Label12: TLabel;
    LaPecasItx: TLabel;
    QrPontosNegQtdFotos: TLargeintField;
    QrMax: TFDQuery;
    LaPodeEncerrar: TLabel;
    Layout1: TLayout;
    QrOVmItxMobCabCodigo: TIntegerField;
    QrOVmItxMobCabCodInMob: TIntegerField;
    QrOVmItxMobCabDeviceSI: TIntegerField;
    QrOVmItxMobCabDeviceID: TStringField;
    QrOVmItxMobCabRandmStr: TStringField;
    QrOVmItxMobCabDeviceCU: TIntegerField;
    QrOVmItxMobCabNome: TStringField;
    QrOVmItxMobCabOVgItxGer: TIntegerField;
    QrOVmItxMobCabLocal: TIntegerField;
    QrOVmItxMobCabNrOP: TIntegerField;
    QrOVmItxMobCabSeqGrupo: TIntegerField;
    QrOVmItxMobCabNrReduzidoOP: TIntegerField;
    QrOVmItxMobCabProduto: TIntegerField;
    QrOVmItxMobCabCodGrade: TIntegerField;
    QrOVmItxMobCabCodTam: TStringField;
    QrOVmItxMobCabDtHrAbert: TDateTimeField;
    QrOVmItxMobCabDtHrFecha: TDateTimeField;
    QrOVmItxMobCabOVcYnsExg: TIntegerField;
    QrOVmItxMobCabQtReal: TFloatField;
    QrOVmItxMobCabQtLocal: TFloatField;
    QrOVmItxMobCabPecasItx: TIntegerField;
    QrOVmItxMobCabPecaAtual: TIntegerField;
    QrOVmItxMobCabSubInspDone: TShortintField;
    QrOVmItxMobCabPontosTot: TIntegerField;
    QrOVmItxMobCabInspResul: TIntegerField;
    QrOVmItxMobCabEnviadoWeb: TShortintField;
    QrOVmItxMobCabEmpresa: TIntegerField;
    QrOVmItxMobCabPerExgReal: TFloatField;
    QrOVmItxMobCabDtHrMailSnt: TFloatField;
    QrOVmItxMobCabLk: TIntegerField;
    QrOVmItxMobCabDataCad: TDateField;
    QrOVmItxMobCabDataAlt: TDateField;
    QrOVmItxMobCabUserCad: TIntegerField;
    QrOVmItxMobCabUserAlt: TIntegerField;
    QrOVmItxMobCabAlterWeb: TShortintField;
    QrOVmItxMobCabAtivo: TShortintField;
    QrOVmItxMobCabSeccaoOP: TStringField;
    QrOVmItxMobCabSeccaoMaq: TStringField;
    QrOVgItxGerCabCodigo: TIntegerField;
    QrOVgItxGerCabNome: TStringField;
    QrOVgItxGerCabLocal: TIntegerField;
    QrOVgItxGerCabNrOP: TIntegerField;
    QrOVgItxGerCabSeqGrupo: TIntegerField;
    QrOVgItxGerCabNrReduzidoOP: TIntegerField;
    QrOVgItxGerCabDtHrAbert: TDateTimeField;
    QrOVgItxGerCabDtHrFecha: TDateTimeField;
    QrOVgItxGerCabOVcYnsExg: TIntegerField;
    QrOVgItxGerCabZtatusIsp: TIntegerField;
    QrOVgItxGerCabZtatusDtH: TDateTimeField;
    QrOVgItxGerCabZtatusMot: TIntegerField;
    QrOVgItxGerCabPermiFinHow: TIntegerField;
    QrOVgItxGerCabOVgItxPrfCab: TIntegerField;
    QrOVgItxGerCabAtivMan: TShortintField;
    QrOVgItxGerCabAtivAut: TShortintField;
    QrOVgItxGerCabLk: TIntegerField;
    QrOVgItxGerCabDataCad: TDateField;
    QrOVgItxGerCabDataAlt: TDateField;
    QrOVgItxGerCabUserCad: TIntegerField;
    QrOVgItxGerCabUserAlt: TIntegerField;
    QrOVgItxGerCabAlterWeb: TShortintField;
    QrOVgItxGerCabAtivo: TShortintField;
    LaTitulo: TLabel;
    QrPontosNegMagnitude: TLargeintField;
    PnSeccao: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    QrOVmItxMobCabSegmntInsp: TIntegerField;
    QrOVmItxMobCabSeccaoInsp: TIntegerField;
    QrOVgItxGerCabSegmntInsp: TIntegerField;
    QrOVgItxGerCabSeccaoInsp: TIntegerField;
    LaSeccaoOP: TLabel;
    LaSeccaoMaq: TLabel;
    procedure BtContinuaClick(Sender: TObject);
    procedure _TmContinuaTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure Grid1DrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const Bounds: TRectF; const Row: Integer;
      const Value: TValue; const State: TGridDrawStates);
    procedure QrPontosNegCalcFields(DataSet: TDataSet);
    procedure Grid1CellClick(const Column: TColumn; const Row: Integer);
    procedure BtNextClick(Sender: TObject);
    procedure QrOVfOrdemProducaoAfterOpen(DataSet: TDataSet);
    procedure BtPriorClick(Sender: TObject);
    procedure BtFirstClick(Sender: TObject);
    procedure BtLastClick(Sender: TObject);
    procedure BtPecaClick(Sender: TObject);
    procedure BtPcOKClick(Sender: TObject);
    procedure QrPontosNegAfterOpen(DataSet: TDataSet);
    procedure QrOVcYnsARQItsAfterOpen(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrOVmItxMobCabAfterOpen(DataSet: TDataSet);
    procedure QrOVgItxGerCabAfterScroll(DataSet: TDataSet);
    procedure QrOVgItxGerCabBeforeClose(DataSet: TDataSet);
    procedure LaSeccaoOPClick(Sender: TObject);
    procedure LaSeccaoMaqClick(Sender: TObject);
  private
    { Private declarations }
    procedure ContinuaDeOndeParou();
    procedure EncerraInspecao(PecaAtual: Integer);
    function  ObtemPecasItx(var PecasItx: Integer): Boolean;
    procedure MostraItem(Item: Integer; SubInspDone: TSubInspDone);
    function  GetNome_SubInspDone(SubInspDone: Integer): String;
    function  GetID_SubInsDone(SubInsDone: String): Integer;
    procedure EncerraAntecipado();
    procedure ReopenTabelasAuxiliares();

  public
    { Public declarations }
    //FContinuar: Boolean;
    FLocal, FArtigo, FNrOP, FNrReduzidoOP, FProduto, FCodGrade, FCodigo,
    FBatelada: Integer;
    FNO_Local, FNO_Artigo, FCodTam, FSeccaoOP, FSeccaoMaq: String;
    FStatusAndamentoInspecao: TStatusAndamentoInspecao;
    FFinalizada: Boolean;
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure AtualizaInfoGeral();
    procedure EditaItemPontosNeg();
    function  IncluiNovaInspecao(var OVmItxMobCab: Integer): Boolean;
    function  LoacalizaAberto_QrOVmItxMobCab(const Proximo: Boolean; var
              CodInMob: Integer): Boolean;
    procedure MostraFormOViItxAct(IDTabela: Integer);
    procedure MostraTextoQuandoPodeEncerrar(Quando: Integer);
    procedure ReopenSeFinalizadoEEnviado();
    procedure ReopenPontosNeg();
    procedure VerificaSeExiste();
    function  VerificaSeJaTem(): Boolean;
    procedure AtualizaCampoTexto(Campo, Texto, Titulo, Pergunta: String);

  end;

var
  FmOViTclCenSel: TFmOViTclCenSel;

implementation

uses UnGrl_DmkDB, UnGrl_Geral, UnOVSM_Vars, UnFMX_DmkForms, UnFMX_DmkProcFunc,
  UnGrl_Vars, UnOVSM_PF,
  Module,
  //
  // Tirar!
  OViInspSeqMed, OViInspSeqNcfChk, OViItxAct,
  OViInspSeqNcfLvr, OViInspSeqNcfTop, OViInspSeqMedEdit,
  // Novos:
  OViTclExgAvl;

{$R *.fmx}

{ TFmOViInspCenSel }

procedure TFmOViTclCenSel.AtualizaCampoTexto(Campo, Texto, Titulo, Pergunta: String);
var
  NovoTexto: String;
  CodInMob, _CIM: Integer;
  Proximo: Boolean;
begin
  Proximo := False;
  FMX_DmkPF.VibrarComoBotao();
  if (QrOVmItxMobCab.State = dsInactive)
  or (QrOVmItxMobCabCodInMob.Value = 0 ) then
  begin
    Grl_Geral.MB_Info(
    'ID da inspe��o ainda n�o definida! Inicie a inspe��o e tente novamente!');
    Exit;
  end;
  //SeccaoOP := BtSeccaoOP.Text;
  TDialogService.InputQuery(Titulo, [Pergunta], [Texto],
  procedure(const AResult: TModalResult; const AValues: array of string)
  begin
    if AResult = mrOk then
    begin
      NovoTexto := AValues[0];
      CodInMob := QrOVmItxMobCabCodInMob.Value;
      //
      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmitxmobcab', False, [
      Campo], [
      'CodInMob'], [
      NovoTexto], [
      CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
      begin
        LoacalizaAberto_QrOVmItxMobCab(Proximo, _CIM);
        //
        //FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfChk);
      end;
    end;
  end);
end;

procedure TFmOViTclCenSel.AtualizaInfoGeral();
var
  LimiIsp, LimiMed, LimiChk, PecasItx: Integer;
begin
  LaLocal.Text        := FNO_Local;
  LaArtigo.Text       := FNO_Artigo;
  LaNrOP.Text         := Grl_Geral.FF0(FNrOP);
  LaTamCad.Text       := FCodTam;
(*
  LaPtsNegativos.Text := '';
  LaPtsAprovRepr.Text := '';
  LaPecasTot.Text     := '';
*)
  LaSeccaoOP.Text     := FSeccaoOP;
  LaSeccaoMaq.Text    := FSeccaoMaq;
(*
  EdSeccaoOP.Text     := QrOVmItxMobCabSeccaoOP.Value;
  EdSeccaoMaq.Text    := QrOVmItxMobCabSeccaoMaq.Value;
*)
  LaSubInspDone.Text  := GetNome_SubInspDone(QrOVmItxMobCabSubInspDone.Value);
  LaItemAtual.Text    := Grl_Geral.FF0(QrOVmItxMobCabPecaAtual.Value);
  LaPecasItx.Text   := '1'; //Grl_Geral.FF0(QrOVmItxMobCabPecasItx.Value);
  PecasItx := Trunc(QrOVmItxMobCabPecasItx.Value);
  LimiIsp := PecasItx;
  LimiMed := 0;(*&�%Trunc(QrOVmItxMobCabLimiteMed.Value);*)
  LimiChk := 0;(*&�%Trunc(QrOVmItxMobCabLimiteChk.Value);*)
  if (LimiMed > 0) and (LimiChk > 0) then
  begin
    if LimiMed > LimiChk then
      LimiIsp := LimiMed
    else
      LimiIsp := LimiChk;
  end;
  if LimiIsp > PecasItx then
    LimiIsp := PecasItx;
  //
  LaTotalItens.Text := Grl_Geral.FF0(LimiIsp);
end;

procedure TFmOViTclCenSel.BtContinuaClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  ContinuaDeOndeParou();
end;

procedure TFmOViTclCenSel.BtEncerraClick(Sender: TObject);
var
  //DtHrFecha, Nome, DtHrMailSnt,
  sPergunta: String;
  //CodInMob, PontosTot, InspResul,
  ItemAtual, MaxItWrtn, MaxItNavi: Integer;
  PerMedReal, PerChkReal: Double;
begin
  FMX_DmkPF.VibrarComoBotao();
  //Nome      := MeNome.Text;
  //DtHrFecha := Grl_Geral.FDT(Now(), 109);
  //CodInMob  := QrOVmItxMobCabCodInMob.Value;
  //PontosTot := 0;
  ItemAtual := Grl_Geral.IMV(LaItemAtual.Text);
  MaxItWrtn := QrPontosNegPecaSeq.Value;
  if ItemAtual > MaxItWrtn then
    MaxItNavi := ItemAtual
  else
    MaxItNavi := MaxItWrtn;
  if MaxItNavi < QrOVmItxMobCabPecasItx.Value then
  begin
    sPergunta := 'Deseja realmente encerrar a inspe��o no item ' + Grl_Geral.FF0(
      MaxItNavi) + '?';
    MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        case AResult of
          mrYes: EncerraInspecao(MaxItNavi);
          //mrNo: Grl_Geral.MB_Erro('Encerramento n�o realizado!');
          //mrCancel: Grl_Geral.MB_Erro('Encerramento Abortado!');
        end;
      end);
  end else
    EncerraInspecao(QrOVmItxMobCabPecasItx.Value);
end;

procedure TFmOViTclCenSel.BtFirstClick(Sender: TObject);
begin
  LaItemAtual.Text := '1';
  LaSubInspDone.Text := 'N';
end;

procedure TFmOViTclCenSel.BtLastClick(Sender: TObject);
begin
  LaItemAtual.Text := Grl_Geral.FF0(QrOVmItxMobCabPecaAtual.Value);
  LaSubInspDone.Text := 'N';
end;

procedure TFmOViTclCenSel.BtNextClick(Sender: TObject);
var
 Proximo: Integer;
begin
  Proximo := Grl_Geral.IMV(LaItemAtual.Text) + 1;
  if Proximo <= QrOVmItxMobCabPecaAtual.Value then
  begin
    LaItemAtual.Text := Grl_Geral.FF0(Proximo);
    LaSubInspDone.Text := 'N';
  end;
end;

procedure TFmOViTclCenSel.BtPcOKClick(Sender: TObject);
var
  Peca, TotalItens: Integer;
begin
  FMX_DmkPF.VibrarComoBotao();
  FMX_DmkPF.OcutaTecladoSeExibido();
  //
  if FMX_DmkPF.FIC(EdPeca.Text = EmptyStr, EdPeca, 'Informe a pe�a!') then Exit;
  //
  Peca := Grl_Geral.IMV(EdPeca.Text);
  if FMX_DmkPF.FIC(Peca < 1, EdPeca, 'A pe�a deve ser maior que zero!') then Exit;
  TotalItens := Grl_Geral.IMV(LaTotalItens.Text);
  if FMX_DmkPF.FIC(Peca > TotalItens, EdPeca,
    'A pe�a n�o pode ser maior que o total de pe�as!') then Exit;
  //
  LaItemAtual.Text   := Grl_Geral.FF0(Peca);
  LaSubInspDone.Text := 'N';
  PnPeca.Visible := False;
end;

procedure TFmOViTclCenSel.BtPecaClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
{
// o bot�o cancel n�o funciona!
  Titulo := 'Salto de pe�a';
  Pergunta := 'Informe a pe�a:';
  //
  TDialogService.InputQuery(Titulo, Pergunta, [Peca],
  procedure(const AResult: TModalResult; const AValues: array of string)
  begin
    if AResult = mrOk then
    begin
      Grl_Geral.MB_Aviso(AValues[0]);
    end else
      Close;
  end);
}
  PnPeca.Visible := True;
  if EdPeca.Enabled and EdPeca.Visible then
    EdPeca.SetFocus;
end;

procedure TFmOViTclCenSel.BtPriorClick(Sender: TObject);
var
 Anterior: Integer;
begin
  Anterior := Grl_Geral.IMV(LaItemAtual.Text) -1;
  if Anterior > 0 then
  begin
    LaItemAtual.Text := Grl_Geral.FF0(Anterior);
    LaSubInspDone.Text := 'N';
  end;
end;

procedure TFmOViTclCenSel.ContinuaDeOndeParou();
var
  SubInspDone, Item: Integer;
begin
  Item := Grl_Geral.IMV(LaItemAtual.Text);
  SubInspDone := GetID_SubInsDone(LaSubInspDone.Text);
  if Item = 0 then
    Item := 1;
  MostraItem(Item, TSubInspDone(SubInspDone));
  //
  //BtContinua.Text := 'Pr�xima pe�a';
  BtContinua.Text := 'Continua';
end;

procedure TFmOViTclCenSel.EditaItemPontosNeg();
begin
  LaItemAtual.Text := Grl_Geral.FF0(QrPontosNegPecaSeq.Value);
  if QrPontosNegTabela.Value = CO_MEM_FLD_TAB_3_Textil_Exigencia then
  begin
    LaSubInspDone.Text := 'M';
    MostraFormOViItxAct(3);
  end
  else
  if QrPontosNegTabela.Value = CO_MEM_FLD_TAB_4_Textil_LivreTexto then
  begin
    LaSubInspDone.Text := 'M';
    MostraFormOViItxAct(4);
  end;
end;

procedure TFmOViTclCenSel.EncerraAntecipado();
var
  SubInspDone, CodInMob, _CIM: Integer;
  Proximo: Boolean;
begin
  Proximo     := False;
  SubInspDone := 2; // 2 = Inconformidades avaliadas
  CodInMob    := QrOVmItxMobCabCodInMob.Value;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmitxmobcab', False, [
  'SubInspDone'], [
  'CodInMob'], [
  SubInspDone], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    LoacalizaAberto_QrOVmItxMobCab(Proximo, _CIM);
    //
    //FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfChk);
  end;
end;

procedure TFmOViTclCenSel.EncerraInspecao(PecaAtual: Integer);
var
  DtHrFecha, Nome, DtHrMailSnt: String;
  // PecasToMed, ItnsToMed, TotalMedidasFeitas, TotalMedidasAFazer, LimiteMed,
  CodInMob, PontosTot, InspResul, PecasItx, PecaSeq, Resultado: Integer;
  PerExgReal: Double;
  Val: Variant;
begin
  Nome      := MeNome.Text;
  DtHrFecha := Grl_Geral.FDT(Now(), 109);
  CodInMob  := QrOVmItxMobCabCodInMob.Value;
  //
  PontosTot := 0;
  Resultado := CO_INSP_RESUL_1024_APROVADO;
  QrPontosNeg.DisableControls;
  try
    QrPontosNeg.First;
    while not QrPontosNeg.Eof do
    begin
      PontosTot := PontosTot + QrPontosNegPontNeg.Value;
      //
      if QrPontosNegMagnitude.Value = CO_INSP_RESUL_3072_REJEITADO then
        Resultado := CO_INSP_RESUL_3072_REJEITADO;
      //
      QrPontosNeg.Next;
    end;
(*  Aqui muda!
    if PontosTot <= QrOVcYnsARQItsPtsAprova.Value then
      InspResul := CO_INSP_RESUL_1024_APROVADO
    else
    if PontosTot <= QrOVcYnsARQItsPtsResalv.Value then
      InspResul := CO_INSP_RESUL_2048_APR_RESALV
    else
    //if PontosTot >= QrOVcYnsARQItsPtsRejeit.Value then
      InspResul := CO_INSP_RESUL_3072_REJEITADO;
    //
*)
    if Resultado = CO_INSP_RESUL_3072_REJEITADO then
      InspResul := CO_INSP_RESUL_3072_REJEITADO
    else
    if PontosTot > 0 then
      InspResul := CO_INSP_RESUL_2048_APR_RESALV
    else
      InspResul := CO_INSP_RESUL_1024_APROVADO;
    //
    QrPontosNeg.First;
    //
    PecasItx := QrOVmItxMobCabPecasItx.Value;
    if PecasItx = 0 then
    begin
      //PerMedReal  := 0;
      PerExgReal  := 0;
    end else
    begin
(*&�%
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.AllDB, [
      'SELECT * ',
      'FROM ovmitxmobexg ',
      'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
      EmptyStr]);
      TotalMedidasFeitas := QrMax.RecordCount;
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.AllDB, [
      'SELECT Controle ',
      'FROM ovcynsexgtop ',
      'WHERE Codigo=' + Grl_Geral.FF0(QrOVmItxMobCabOVcYnsExg.Value),
      EmptyStr]);
      ItnsToMed   := QrMax.RecordCount;
*)
(*&�%
      LimiteMed   := QrOVmItxMobCabLimiteMed.Value;
      if (LimiteMed > 0) and (LimiteMed < PecasItx) then
        PecasToMed := LimiteMed
      else*)
(*&�%
        PecasToMed := PecasItx;
*)
(*
      TotalMedidasAFazer := ItnsToMed * PecasToMed;
      if TotalMedidasAFazer = 0 then
        PerMedReal  :=  0
      else
        //PerMedReal  :=  PecaSeq / PecasItx * 100; //% de pe�as medidas
        PerMedReal  := TotalMedidasFeitas / TotalMedidasAFazer * 100; //% de pe�as medidas
*)
      PerExgReal  := PecaAtual / PecasItx * 100; //percentual de pe�as checadas
    end;
    DtHrMailSnt := '0000-00-00 00:00:00'; //data hora envio email
    //
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmitxmobcab', False, [
    'Nome',
    'DtHrFecha', 'PontosTot', 'InspResul',
    'PecaAtual', 'PerExgReal', 'DtHrMailSnt'], [
    'CodInMob'], [
    Nome,
    DtHrFecha, PontosTot, InspResul,
    PecaAtual, PerExgReal, DtHrMailSnt], [
    CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
    begin
      FMX_DmkForms.FechaFm_AllOS0(FmOViTclCenSel);
    end;
  finally
    QrPontosNeg.EnableControls;
  end;
end;

procedure TFmOViTclCenSel.FormCreate(Sender: TObject);
begin
  LaPodeEncerrar.Visible := False;
  TopToolbar.Visible     := False;
  //
  FFinalizada         := False;
  //
  BtPeca.Enabled      := False;
  EdPeca.Enabled      := False;
  PnPeca.Visible      := False;

  (*&�% Desmarcar???*)
  BtEncerra.Enabled   := True;
  BtContinua.Visible  := True;
  (*&�%
  BtContinua.Visible  := False;
  BtEncerra.Enabled   := False;
  *)
  //
  FCodigo             := 0;
  //
  MostraTextoQuandoPodeEncerrar(0);
end;

procedure TFmOViTclCenSel.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
{ Resolve aqui mas gera erro no form principal!
  if Key = vkHardwareBack then
  begin
    Key := vkTab;
    BtEncerra.SetFocus;
  ////////////////////////////////////////////////////////////////////////////////
    EXIT;
  ////////////////////////////////////////////////////////////////////////////////
  end;
}
  try
    FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
  except
    on E: Exception do
      Grl_Geral.MB_Erro(E.Message);
  end;
end;

function TFmOViTclCenSel.GetID_SubInsDone(SubInsDone: String): Integer;
const
  sProcName = 'TFmOViTclCenSel.GetID_SubInsDone()';
begin
  Result := -2;
  if SubInsDone = 'N' then Result := 0 else
  if SubInsDone = 'M' then Result := 1 else
  if SubInsDone = 'I' then Result := 2 else
  begin
    Result := -1;
    Grl_Geral.MB_Erro('SubInsDone "' + SubInsDone + '" indefinido em ' +
    sProcName);
  end;
end;

function TFmOViTclCenSel.GetNome_SubInspDone(SubInspDone: Integer): String;
begin
  Result := '?';
  case TSubInspDone(SubInspDone) of
    TSubInspDone.sidNadaPronto:               Result := 'N';
    TSubInspDone.sidMedidasFeitas:            Result := 'M';
    TSubInspDone.sidInconformidadesAvaliadas: Result := 'I';
  end;
end;

procedure TFmOViTclCenSel.Grid1CellClick(const Column: TColumn;
  const Row: Integer);
var
  sPergunta: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  if FFinalizada then Exit;
  //
  if Row > -1 then
  begin
    sPergunta := 'Deseja editar o item "' + QrPontosNegNO_Topico.Value + '"?';
    MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        //MR := AResult;
        case AResult of
          mrYes: EditaItemPontosNeg();
          mrNo: Grl_Geral.MB_Info('Edi��o n�o realizada!');
          mrCancel: Grl_Geral.MB_Info('Edi��o Abortada!');
        end;
      end);
  end;
end;

procedure TFmOViTclCenSel.Grid1DrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const Bounds: TRectF;
  const Row: Integer; const Value: TValue; const State: TGridDrawStates);
var
  CurX, CurY, SpacePos, Lines, LineHt : integer;
  Sentence, CurWord : string;
  EOS : boolean;
begin
(*
  Grid1.Canvas.FillRect(Rect);
  Inc(Rect.Left, 2);
  CurX := Rect.Left;
  CurY := Rect.Top;
  Lines:= 1;
  Sentence := Grid1.Cells[Col, Row];
  LineHt:= Grid1.Canvas.TextHeight(Sentence);
  EOS := FALSE;
  while (not EOS) do
  begin
    SpacePos := Pos(' ', Sentence);
    if SpacePos > 0 then
    begin
      CurWord := Copy(Sentence, 0, SpacePos);
      Sentence := Copy(Sentence, SpacePos + 1,
                       Length(Sentence) - SpacePos);
    end
    else
    begin
      EOS := TRUE;
      CurWord := Sentence;
    end;
    with Grid1.Canvas do
    begin
      {If current word won't fit in the cell then move down
       to the next line and go back to the left margin }
      if (TextWidth(CurWord) + CurX) > Rect.Right then
      begin
        CurY := CurY + LineHt;
        CurX := Rect.Left;
        Lines:= Lines+1;
      end;
      TextOut(CurX, CurY, CurWord);
      CurX := CurX + TextWidth(CurWord);
    end;
  end;
  if (Grid1.RowHeights[Row] < (StdRowHeight +
     ((Lines-1) * LineHt))) then
    Grid1.RowHeights[Row]:= StdRowHeight +
      ((Lines-1) * LineHt);
*)
end;

function TFmOViTclCenSel.IncluiNovaInspecao(var OVmItxMobCab: Integer): Boolean;
var
  DeviceID, Nome, DtHrAbert, DtHrFecha, CodTam, RandmStr, SeccaoOP, SeccaoMaq:
  String;
  //Codigo,
  Batelada, CodInMob, DeviceSI, DeviceCU, OVgItxGer, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsExg,
  //OVcYnsChk, OVcYnsARQ, LimiteChk, LimiteMed,
  PecasItx,
  PecaAtual, PcIsp, Produto, CodGrade, SubInspDone, Empresa,
  SegmntInsp, SeccaoInsp: Integer;
  QtReal, QtLocal: Double;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stIns;
  //Codigo         := ;
  CodInMob       := 0;
  Batelada       := FBatelada;
  DeviceSI       := VAR_COD_DEVICE_IN_SERVER;
  DeviceID       := VAR_IMEI_DEVICE_IN_SERVER;
  DeviceCU       := 0; // current user. Ainda n�o usado
  Nome           := ''; // No fechammento?
  OVgItxGer      := QrOVgItxGerCabCodigo.Value;
  Local          := FLocal;
  NrOP           := FNrOP;
  SeqGrupo       := FArtigo;
  NrReduzidoOP   := FNrReduzidoOP;
  Produto        := FProduto;
  CodGrade       := FCodGrade;
  CodTam         := FCodTam;
  DtHrAbert      := Grl_Geral.FDT(Now(), 109);
  DtHrFecha      := Grl_Geral.FDT(0, 109);
  OVcYnsExg      := QrOVgItxGerCabOVcYnsExg.Value;
(*
  OVcYnsChk      := QrOVgItxGerCabOVcYnsChk.Value;
  OVcYnsARQ      := QrOVgItxGerCabOVcYnsARQ.Value;
  LimiteChk      := QrOVgItxGerCabLimiteChk.Value;
  LimiteMed      := QrOVgItxGerCabLimiteMed.Value;
*)
  QtReal         := QrOVfOrdemProducaoQtReal.Value;
  QtLocal        := QrOVfOrdemProducaoQtLocal.Value;
  PecasItx       := 0; // Abaixo
  PecaAtual      := 1;
  SubInspDone    := 0;
  Empresa        := QrOVfOrdemProducaoEmpresa.Value;
  //
  if not ObtemPecasItx(PecasItx) then
    Exit;
  //
  RandmStr       := OVSM_PF.GetRandomStr();
  //
  SeccaoOP       := FSeccaoOP; //QrOVgItxGerCabSeccaoOP.Value;
  SeccaoMaq      := FSeccaoMaq; //QrOVgItxGerCabSeccaoMaq.Value;
  SegmntInsp     := QrOVgItxGerCabSegmntInsp.Value;
  SeccaoInsp     := QrOVgItxGerCabSeccaoInsp.Value;
  //
//  CodInMob := UMyMod.BPGS1I32('ovmitxmobcab', 'CodInMob', '', '', tsPosNeg?, stInsUpd?, CodInMob);
  CodInMob := Grl_DmkDB.GetNxtCodigoInt('ovmitxmobcab', 'CodInMob', SQLType, CodInMob);
  //if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovmitxmobcab', auto_increment?[
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stIns, 'ovmitxmobcab', False, [
  (*'Codigo',*)
  'Batelada', 'DeviceSI', 'DeviceID',
  'DeviceCU', 'Nome', 'OVgItxGer',
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'QtReal', 'QtLocal',
  'Produto', 'CodGrade', 'CodTam',
  'DtHrAbert', 'DtHrFecha',
  'OVcYnsExg', (*'OVcYnsChk', 'OVcYnsARQ',
  'LimiteChk', 'LimiteMed',*) 'SubInspDone',
  'PecasItx', 'PecaAtual', 'RandmStr',
  'Empresa', 'SeccaoOP', 'SeccaoMaq',
  'SegmntInsp', 'SeccaoInsp'], [
  'CodInMob'], [
  (*Codigo,*)
  Batelada, DeviceSI, DeviceID,
  DeviceCU, Nome, OVgItxGer,
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, QtReal, QtLocal,
  Produto, CodGrade, CodTam,
  DtHrAbert, DtHrFecha,
  OVcYnsExg, (*OVcYnsChk, OVcYnsARQ,
  LimiteChk, LimiteMed,*) SubInspDone,
  PecasItx, PecaAtual, RandmStr,
  Empresa, SeccaoOP, SeccaoMaq,
  SegmntInsp, SeccaoInsp], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    OVmItxMobCab := CodInMob;
    Result := OVmItxMobCab <> 0;
  end;
end;

procedure TFmOViTclCenSel.LaSeccaoMaqClick(Sender: TObject);
begin
(*
var
  Campo, Texto, Titulo, Pergunta: String;
begin
  if FStatusAndamentoInspecao = TStatusAndamentoInspecao.saiInspAberta then
  begin
    Campo    := 'SeccaoOP';
    Texto    := EdSeccaoOP.Text;
    Titulo   := 'OP de servi�o';
    Pergunta := 'Informe o n�mero da OP';
    AtualizaCampoTexto(Campo, Texto, Titulo, Pergunta);
  end;
*)
end;

procedure TFmOViTclCenSel.LaSeccaoOPClick(Sender: TObject);
begin
(*
var
  Campo, Texto, Titulo, Pergunta: String;
begin
  if FStatusAndamentoInspecao = TStatusAndamentoInspecao.saiInspAberta then
  begin
    Campo    := 'SeccaoMaq';
    Texto    := EdSeccaoMaq.Text;
    Titulo   := 'M�quina';
    Pergunta := 'Informe a identifica��o (n�mero) da m�quina';
    AtualizaCampoTexto(Campo, Texto, Titulo, Pergunta);
  end;
*)
end;

function TFmOViTclCenSel.LoacalizaAberto_QrOVmItxMobCab(const Proximo: Boolean;
  var CodInMob: Integer): Boolean;
var
  Encerra, Chk, Med: Boolean;
  PtsNegativos, PermiFinHow: Integer;
begin
  Grl_DmkDB.AbreSQLQuery0(QrOVmItxMobCab, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovmitxmobcab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  // 19-11-02 s� estava deixando um
  'AND CodGrade=' + Grl_Geral.FF0(FCodGrade),
  'AND CodTam="' + FCodTam + '"',
  //  Fim 19-11-02
  'AND DtHrFecha < "1900-01-01" ',
  EmptyStr]);
  //Grl_Geral.MB_Info(QrOVmItxMobCab.SQL.Text);
  Encerra := QrOVmItxMobCabPecaAtual.Value >= QrOVmItxMobCabPecasItx.Value;
(*&�%
  if not Encerra then
  begin
    Chk :=
      (QrOVmItxMobCabPecaAtual.Value >= QrOVmItxMobCabLimiteChk.Value)
      and
      (QrOVmItxMobCabLimiteChk.Value > 0);
    Med :=
      (QrOVmItxMobCabPecaAtual.Value >= QrOVmItxMobCabLimiteMed.Value)
      and
      (QrOVmItxMobCabLimiteMed.Value > 0);
    //
    Encerra := Chk and Med;
  end;
*)
  //
  if Encerra then
    Encerra := QrOVmItxMobCabSubInspDone.Value = 2;
  if Encerra then
  begin
    (*&�%
    BtContinua.Visible  := False;
    BtEncerra.Enabled   := True;
    *)
    PnNome.Visible      := True;
  end else
  begin
    // pode encerrar antes?
(*
0 - Indefinido
1 - Somenteao finalizar toda inspe��o
2 - Ao atingir a pontua��o de reprova��o
3 - A qualquer momento
*)
    case  QrOVgItxGerCabPermiFinHow.Value of
      0: Encerra := False;
      1: Encerra := False;
      2:
      begin
        PtsNegativos := Grl_Geral.IMV(LaPtsNegativos.Text);
        // Ver como fazer
        Encerra := True; //PtsNegativos > QrOVcYnsARQItsPtsResalv.Value;
        (*&�%
        Encerra := PtsNegativos > QrOVcYnsARQItsPtsResalv.Value;
        *)
      end;
      3: Encerra := True;
    end;
    PnNome.Visible      := False;
    (*&�%
    BtEncerra.Enabled   := Encerra;
    BtContinua.Visible  := True;
    *)
  end;
  //
  CodInMob := QrOVmItxMobCabCodInMob.Value;
  Result := CodInMob <> 0;
  //
  if Proximo then
    CodInMob := CodInMob + 1;
  //
  AtualizaInfoGeral();
end;

procedure TFmOViTclCenSel.MostraFormOViItxAct(IDTabela: Integer);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViItxAct, FmOViItxAct, fcmOnlyCreate, True, False) then
  begin
    FmOViItxAct.FLocal        := FLocal;
    FmOViItxAct.FArtigo       := FArtigo;
    FmOViItxAct.FNrOP         := FNrOP;
    FmOViItxAct.FNrReduzidoOP := FNrReduzidoOP;
    FmOViItxAct.FProduto      := FProduto;
    FmOViItxAct.FCodGrade     := FCodGrade;
    FmOViItxAct.FNO_Local     := FNO_Local;
    FmOViItxAct.FNO_Artigo    := FNO_Artigo;
    FmOViItxAct.FCodTam       := FCodTam;
    FmOViItxAct.FOVcYnsExgCad := QrOVgItxGerCabOVcYnsExg.Value;
    FmOViItxAct.FPecaSeq      := QrPontosNegPecaSeq.Value;
    //
    //FmOViItxAct.LaLocal.Text     := NO_Local;
    FmOViItxAct.LaArtigo.Text      := FNO_Artigo;
    FmOViItxAct.LaNrOP.Text        := Grl_Geral.FF0(FNrOP);
    FmOViItxAct.LaTamCad.Text      := FCodTam;
    FmOViItxAct.LaItemAtual.Text   := Grl_Geral.FF0(QrPontosNegPecaSeq.Value);
    FmOViItxAct.LaTotalItens.Text  := Grl_Geral.FF0(QrOVmItxMobCabPecasItx.Value);
    //
    FmOViItxAct.FNO_Topico         := QrPontosNegNO_Topico.Value;
    FmOViItxAct.LaNO_Topico.Text   := QrPontosNegNO_Topico.Value;
    FmOViItxAct.FTabela            := QrPontosNegTabela.Value;
    FmOViItxAct.FIDTabela          := IDTabela;
    FmOViItxAct.FCodInMob          := QrOVmItxMobCabCodInMob.Value;
    FmOViItxAct.FCtrlInMob         := QrPontosNegCtrlInMob.Value;
    //
    FmOViItxAct.BtAltera.Enabled := QrPontosNegTabela.Value = CO_MEM_FLD_TAB_2_Faccao_LivreTexto;

    //FmOViItxAct.ReopenItem();
    //
    FMX_DmkForms.ShowModal(FmOViItxAct);
  end;
end;

procedure TFmOViTclCenSel.MostraItem(Item: Integer; SubInspDone: TSubInspDone);
// = (sidNadaPronto=0, sidMedidasFeitas=1, sidInconformidadesAvaliadas=2););
var
  SID: TSubInspDone;
begin
{
  SID := SubInspDone;
  if Item <= QrOVmItxMobCabPecasItx.Value then
  begin
    if (SID = TSubInspDOne.sidNadaPronto)
    or (SID = TSubInspDOne.sidInconformidadesAvaliadas) then
    begin
      // Medidas
      if (Item <= QrOVmItxMobCabLimiteMed.Value)
      or (QrOVmItxMobCabLimiteMed.Value = 0) then
      begin
        if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqMed, FmOViInspSeqMed, fcmOnlyCreate, True, False) then
        begin
          FmOViInspSeqMed.FLocal        := FLocal;
          FmOViInspSeqMed.FArtigo       := FArtigo;
          FmOViInspSeqMed.FNrOP         := FNrOP;
          FmOViInspSeqMed.FNrReduzidoOP := FNrReduzidoOP;
          FmOViInspSeqMed.FProduto      := FProduto;
          FmOViInspSeqMed.FCodGrade     := FCodGrade;
          FmOViInspSeqMed.FNO_Local     := FNO_Local;
          FmOViInspSeqMed.FNO_Artigo    := FNO_Artigo;
          FmOViInspSeqMed.FCodGrade     := FCodGrade;
          FmOViInspSeqMed.FCodTam       := FCodTam;
          FmOViInspSeqMed.FOVcYnsExg    := QrOVmItxMobCabOVcYnsExg.Value;
          FmOViInspSeqMed.FPecaSeq      := Item;
          //
          //FmOViInspSeqMed.LaLocal.Text  := NO_Local;
          FmOViInspSeqMed.LaArtigo.Text := FNO_Artigo;
          FmOViInspSeqMed.LaNrOP.Text   := Grl_Geral.FF0(FNrOP);
          FmOViInspSeqMed.LaTamCad.Text := FCodTam;
          FmOViInspSeqMed.LaItemAtual.Text := Grl_Geral.FF0(Item);
          FmOViInspSeqMed.LaTotalItens.Text := Grl_Geral.FF0(QrOVmItxMobCabPecasItx.Value);
          //
          FmOViInspSeqMed.FCodInMob         := QrOVmItxMobCabCodInMob.Value;
          //
          FmOViInspSeqMed.ReopenItem(Item);
          //
          FMX_DmkForms.ShowModal(FmOViInspSeqMed);
        end;
      end else
      begin
        SID := TSubInspDOne.sidMedidasFeitas;
        BtPeca.Enabled := True;
        EdPeca.Enabled := True;
      end;
    end;
    if SID = TSubInspDOne.sidMedidasFeitas then
    begin
      // Inconformidades
      if (Item <= QrOVmItxMobCabLimiteChk.Value)
      or (QrOVmItxMobCabLimiteChk.Value = 0) then
      begin
}
        if FMX_DmkForms.CriaFm_AllOS0(TFmOViTclExgAvl, FmOViTclExgAvl, fcmOnlyCreate, True, False) then
        begin
          FmOViTclExgAvl.FLocal        := FLocal;
          FmOViTclExgAvl.FArtigo       := FArtigo;
          FmOViTclExgAvl.FNrOP         := FNrOP;
          FmOViTclExgAvl.FNrReduzidoOP := FNrReduzidoOP;
          FmOViTclExgAvl.FProduto      := FProduto;
          FmOViTclExgAvl.FCodGrade     := FCodGrade;
          FmOViTclExgAvl.FNO_Local     := FNO_Local;
          FmOViTclExgAvl.FNO_Artigo    := FNO_Artigo;
          FmOViTclExgAvl.FCodGrade     := FCodGrade;
          FmOViTclExgAvl.FCodTam       := FCodTam;
          FmOViTclExgAvl.FOVcYnsExg    := QrOVmItxMobCabOVcYnsExg.Value;
          FmOViTclExgAvl.FPecaSeq      := Item;
          if (Item = QrOVmItxMobCabPecasItx.Value) then
          begin
            FmOViTclExgAvl.FFecha := True;
            FmOViTclExgAvl.BtNxtEnd.Text := 'Encerra';
          end else
          begin
            FmOViTclExgAvl.FFecha := False;
            FmOViTclExgAvl.BtNxtEnd.Text := 'Pr�ximo';
          end;
          //
          //FmOViTclExgAvl.LaLocal.Text  := NO_Local;
          FmOViTclExgAvl.LaArtigo.Text := FNO_Artigo;
          FmOViTclExgAvl.LaNrOP.Text   := Grl_Geral.FF0(FNrOP);
          FmOViTclExgAvl.LaTamCad.Text := FCodTam;
          FmOViTclExgAvl.LaItemAtual.Text := Grl_Geral.FF0(Item);
          FmOViTclExgAvl.LaTotalItens.Text := Grl_Geral.FF0(QrOVmItxMobCabPecasItx.Value);
          //
          FmOViTclExgAvl.FCodInMob         := QrOVmItxMobCabCodInMob.Value;
          //
          FmOViTclExgAvl.ReopenItem();
          //
          FMX_DmkForms.ShowModal(FmOViTclExgAvl);
        end;
{
      end else
        EncerraAntecipado();
    end;
  end;
}
end;

procedure TFmOViTclCenSel.MostraTextoQuandoPodeEncerrar(Quando: Integer);
begin
  LaPodeEncerrar.Text := ' Encerramento: ' + sCO_ENCERRA_INSPECAO[Quando];
end;

function TFmOViTclCenSel.ObtemPecasItx(var PecasItx: Integer): Boolean;
begin
  PecasItx := 1; //QrOVcYnsARQItsQtdAmostr.Value;
  Result := PecasItx > 0;
  if not Result then
  begin
    Grl_Geral.MB_Erro('Quantidade de pe�as a serem inspecionadas n�o definido!');
    FMX_DmkForms.FechaFm_AllOS0(FmOViTclCenSel);
  end;

  //QrOVgItxGerCab
  //ObtemPecasItx(PecasItx: Integer): Boolean;
end;

procedure TFmOViTclCenSel.QrOVcYnsARQItsAfterOpen(DataSet: TDataSet);
begin
  LaPtsAprovRepr.Text := '?' (*&�%$ Grl_Geral.FF0(Trunc(QrOVcYnsARQItsPtsAprova.Value)) +
  '/' + Grl_Geral.FF0(Trunc(QrOVcYnsARQItsPtsResalv.Value))*);
end;

procedure TFmOViTclCenSel.QrOVfOrdemProducaoAfterOpen(DataSet: TDataSet);
begin
  LaQtLocal.Text := Grl_Geral.FF0(Trunc(QrOVfOrdemProducaoQtLocal.Value));
end;

procedure TFmOViTclCenSel.QrOVgItxGerCabAfterScroll(DataSet: TDataSet);
begin
  MostraTextoQuandoPodeEncerrar(QrOVgItxGerCabPermiFinHow.Value);
end;

procedure TFmOViTclCenSel.QrOVgItxGerCabBeforeClose(DataSet: TDataSet);
begin
  MostraTextoQuandoPodeEncerrar(0);
end;

procedure TFmOViTclCenSel.QrOVmItxMobCabAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
  ItemAtu, LimiMed: Integer;
begin
  ItemAtu  := QrOVmItxMobCabPecaAtual.Value;
  LimiMed  := 1; (*&�%QrOVmItxMobCabLimiteMed.Value;*)
  Habilita := VAR_PERMITE_NAO_MEDIR or ((ItemAtu > LimiMed) and (LimiMed > 0));
  BtPeca.Enabled := Habilita;
  EdPeca.Enabled := Habilita;
end;

procedure TFmOViTclCenSel.QrPontosNegAfterOpen(DataSet: TDataSet);
var
  PontosTot: Integer;
begin
  PontosTot := 0;
  QrPontosNeg.DisableControls;
  try
    QrPontosNeg.First;
    while not QrPontosNeg.Eof do
    begin
      PontosTot := PontosTot + QrPontosNegPontNeg.Value;
      //
      QrPontosNeg.Next;
    end;
    LaPtsNegativos.Text := Grl_Geral.FF0(PontosTot);
  finally
    QrPontosNeg.EnableControls;
  end;
end;

procedure TFmOViTclCenSel.QrPontosNegCalcFields(DataSet: TDataSet);
begin
  QrPontosNegBRK_Topico.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(Grid1, 3, QrPontosNegNO_Topico.Value);
end;

procedure TFmOViTclCenSel.ReopenPontosNeg();
var
  CodInMob_TXT: String;
begin
  CodInMob_TXT := Grl_Geral.FF0(QrOVmItxMobCabCodInMob.Value);
  //
  //CodTab WideString
  Grl_DmkDB.AbreSQLQuery0(QrPontosNeg, Dmod.AllDB, [
  'SELECT  "3" IdTabela, "' + CO_MEM_FLD_TAB_3_Textil_Exigencia + '" Tabela,  ',
  'mmc.CtrlInMob AS "CtrlInMob::BIGINT", mmc.PecaSeq AS "PecaSeq::BIGINT", ',
  'yqt.Nome NO_Topico, mmc.PontNeg AS "PontNeg::BIGINT", ',
  'mmc.QtdFotos AS "QtdFotos::BIGINT", ',
  'mmc.Magnitude AS "Magnitude::BIGINT" ',
  'FROM ovmitxmobexg mmc ',
  'LEFT JOIN ovcynsmixtop yqt ON yqt.Codigo=mmc.Topyko ',
  'WHERE mmc.CodInMob=' + CodInMob_TXT,
  ' ',
  'UNION ',
  ' ',
  'SELECT  "4" IdTabela, "' + CO_MEM_FLD_TAB_4_Textil_LivreTexto + '" Tabela,   ',
  'mml.CtrlInMob AS "CtrlInMob::BIGINT", mml.PecaSeq AS "PecaSeq::BIGINT",  ',
  'mml.Descricao NO_Topico, mml.PontNeg AS "PontNeg::BIGINT", ',
  'mml.QtdFotos AS "QtdFotos::BIGINT", ',
  'mml.Magnitude AS "Magnitude::BIGINT" ',
  'FROM ovmitxmoblvr mml  ',
  'WHERE mml.CodInMob=' + CodInMob_TXT,
  '  ',
  'ORDER BY PecaSeq DESC, IdTabela, CtrlInMob ',
  '']);
  //Grl_Geral.MB_SQL(FmOViTclCenSel, QrPontosNeg);
end;

procedure TFmOViTclCenSel.ReopenSeFinalizadoEEnviado;
begin
  if FCodigo > 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrOVmItxMobCab, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovmitxmobcab ',
    'WHERE Codigo=' + Grl_Geral.FF0(FCodigo),
    EmptyStr]);
    if QrOVmItxMobCab.RecordCount > 0 then
    begin
      ReopenTabelasAuxiliares();
      ReopenPontosNeg()
    end;
  end;
end;

procedure TFmOViTclCenSel.ReopenTabelasAuxiliares();
var
  OVcYnsARQ_Txt, QtLocal_Txt: String;
  CodInMob: Integer;
begin
///////////////////// Configura��o de inspe��o: ovgitxgercab ///////////////////
// *&*&*&*&*&*&*&*&*&
  Grl_DmkDB.AbreSQLQuery0(QrOVgItxGerCab, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovgitxgercab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  EmptyStr]);
  //Grl_Geral.MB_SQL(FmOViTclCenSel, QrOVgItxGerCab);
  //
  ///////////// Reduzido da OP em produ��o: ovfordemproducao /////////////////
  Grl_DmkDB.AbreSQLQuery0(QrOVfOrdemProducao, Dmod.AllDB, [
  //'SELECT QtReal, QtLocal  ',
  'SELECT *  ',
  'FROM ovfordemproducao ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  'AND Produto=' + Grl_Geral.FF0(FProduto),
  //'/*  Empresa int(11) NOT NULL  DEFAULT 0, */ ',
  EmptyStr]);
  //
{
  ///////////// C�lculo de pe�as a inspecionar: OVcYnsARQIts /////////////////
  OVcYnsARQ_Txt := Grl_Geral.FF0(QrOVgItxGerCabOVcYnsARQ.Value);
  QtLocal_TXT   := Grl_Geral.FFT_Dot(QrOVfOrdemProducaoQtLocal.Value, 3, siNegativo);
  Grl_DmkDB.AbreSQLQuery0(QrOVcYnsARQIts, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovcynsarqits ',
  'WHERE Codigo=' + OVcYnsARQ_Txt,
  'AND ' + QtLocal_TXT
  + ' BETWEEN QtdPcIni AND QtdPcFim ',
  'ORDER BY QtdAmostr DESC ',
  EmptyStr]);
  //
  ///////////// C�lculo de pontos negativos em inspe��es: OVcYnsARQCad /////////////////
  OVcYnsARQ_Txt := Grl_Geral.FF0(QrOVgItxGerCabOVcYnsARQ.Value);
  QtLocal_TXT   := Grl_Geral.FFT_Dot(QrOVfOrdemProducaoQtLocal.Value, 3, siNegativo);
  Grl_DmkDB.AbreSQLQuery0(QrOVcYnsARQCad, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovcynsarqcad ',
  'WHERE Codigo=' + OVcYnsARQ_Txt,
  EmptyStr]);
  //
object QrOVcYnsARQIts: TFDQuery
  AfterOpen = QrOVcYnsARQItsAfterOpen
  Connection = Dmod.AllDB
  SQL.Strings = (
    '    SELECT *'
    '    FROM OVcYnsARQits'
    '    WHERE Codigo=1'
    '    AND 101.34 BETWEEN QtdPcIni AND QtdPcFim'
    '    ORDER BY QtdAmostr DESC')
  Left = 84
  Top = 156
  object QrOVcYnsARQItsCodigo: TIntegerField
    FieldName = 'Codigo'
    Origin = 'Codigo'
    Required = True
  end
  object QrOVcYnsARQItsControle: TIntegerField
    FieldName = 'Controle'
    Origin = 'Controle'
    ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    Required = True
  end
  object QrOVcYnsARQItsQtdPcIni: TIntegerField
    FieldName = 'QtdPcIni'
    Origin = 'QtdPcIni'
    Required = True
  end
  object QrOVcYnsARQItsQtdPcFim: TIntegerField
    FieldName = 'QtdPcFim'
    Origin = 'QtdPcFim'
    Required = True
  end
  object QrOVcYnsARQItsQtdAmostr: TIntegerField
    FieldName = 'QtdAmostr'
    Origin = 'QtdAmostr'
    Required = True
  end
  object QrOVcYnsARQItsPtsAprova: TFloatField
    FieldName = 'PtsAprova'
    Origin = 'PtsAprova'
    Required = True
  end
  object QrOVcYnsARQItsPtsResalv: TFloatField
    FieldName = 'PtsResalv'
    Origin = 'PtsResalv'
    Required = True
  end
  object QrOVcYnsARQItsPtsRejeit: TFloatField
    FieldName = 'PtsRejeit'
    Origin = 'PtsRejeit'
    Required = True
  end
end
object QrOVcYnsARQCad: TFDQuery
  Connection = Dmod.AllDB
  SQL.Strings = (
    '    SELECT *'
    '    FROM OVcYnsARQCad'
    '    WHERE Codigo=1')
  Left = 88
  Top = 104
  object QrOVcYnsARQCadCodigo: TIntegerField
    FieldName = 'Codigo'
    Origin = 'Codigo'
    ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    Required = True
  end
  object QrOVcYnsARQCadNome: TStringField
    FieldName = 'Nome'
    Origin = 'Nome'
    Size = 60
  end
  object QrOVcYnsARQCadEsquema: TStringField
    FieldName = 'Esquema'
    Origin = 'Esquema'
    Size = 15
  end
  object QrOVcYnsARQCadNivel: TStringField
    FieldName = 'Nivel'
    Origin = 'Nivel'
    Size = 15
  end
  object QrOVcYnsARQCadLMQR: TStringField
    FieldName = 'LMQR'
    Origin = 'LMQR'
    Size = 15
  end
  object QrOVcYnsARQCadNQA: TFloatField
    FieldName = 'NQA'
    Origin = 'NQA'
  end
  object QrOVcYnsARQCadPontosTole: TFloatField
    FieldName = 'PontosTole'
    Origin = 'PontosTole'
    Required = True
  end
  object QrOVcYnsARQCadPontosGrav: TFloatField
    FieldName = 'PontosGrav'
    Origin = 'PontosGrav'
    Required = True
  end
  object QrOVcYnsARQCadPontosCrit: TFloatField
    FieldName = 'PontosCrit'
    Origin = 'PontosCrit'
    Required = True
  end
end
}
end;

procedure TFmOViTclCenSel._TmContinuaTimer(Sender: TObject);
var
  Item: Integer;
begin
{
  TmContinua.Enabled := False;
  if FContinuar then
  begin
    Item := Grl_Geral.IMV(LaItemAtual.Text);
    Item := Item + 1;
    MostraItem(Item);
  end;
}
end;

procedure TFmOViTclCenSel.VerificaSeExiste();
var
  CodInMob: Integer;
begin
  ReopenTabelasAuxiliares();
///////////////////// Verifica se existe: ovmitxmobcab /////////////////////////
  if not LoacalizaAberto_QrOVmItxMobCab(False, CodInMob) then
  begin
    if FCodigo > 0 then Exit;
    //
    if IncluiNovaInspecao(CodInMob) then
      if not LoacalizaAberto_QrOVmItxMobCab(False, CodInMob) then
        FMX_DmkForms.FechaFm_AllOS0(FmOViTclCenSel);
    //
  end;
end;

function TFmOViTclCenSel.VerificaSeJaTem(): Boolean;
var
  sPergunta: String;
  Continua: Boolean;
begin
  Continua := False;
  Grl_DmkDB.AbreSQLQuery0(QrOVmItxMobCab, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovmitxmobcab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  'AND Produto=' + Grl_Geral.FF0(FProduto),
  'AND CodGrade=' + Grl_Geral.FF0(FCodGrade),
  'AND CodTam="' + FCodTam + '"',
  'AND DtHrFecha > "1900-01-01" ',
  'AND EnviadoWeb=0 ',
  EmptyStr]);
  if QrOVmItxMobCab.RecordCount > 0 then
  begin
    sPergunta := 'J� existe uma inspe��o finalizada mas n�o enviada ao servidor!' +
      sLineBreak + 'Deseja iniciar outra assim mesmo?';
    //
    MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        //MR := AResult;
        case AResult of
          mrYes: Continua := True;
          mrNo: Grl_Geral.MB_Info('Inspe��o n�o iniciada!');
          mrCancel: Grl_Geral.MB_Info('Inspe��o Abortada!');
        end;
      end);
  end else
    Continua := True;
  //
  if Continua then
    VerificaSeExiste();
  //
  Result := Continua;
end;

end.
