unit OViLocais;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums;

type
  TFmOViLocais = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    GrLocais: TGrid;
    QrOViLocais: TFDQuery;
    QrOViLocaisCodigo: TIntegerField;
    QrOViLocaisNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOViLocaisBRK_Nome: TStringField;
    procedure GrLocaisCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure QrOViLocaisCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOViLocais(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; ParteNome: String);
  end;

var
  FmOViLocais: TFmOViLocais;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnOVSM_PF,
  Module, OViIspCenSnt;

{$R *.fmx}

procedure TFmOViLocais.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViLocais.FormShow(Sender: TObject);
begin
  ReopenOViLocais(FSegmentoInsp, FSeccaoInsp, '');
end;

procedure TFmOViLocais.GrLocaisCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOViLocais.GrLocaisCellClick()';
  Artigo = 0;
  NrOP   = 0;
  NO_Artigo = '';
  NO_NrOP   = '';

begin
  // Enviados
  VAR_LAST_LOCAL_COD := QrOViLocaisCodigo.Value;
  VAR_LAST_LOCAL_NOM := QrOViLocaisNome.Value;
  //
  if (QrOViLocais.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  case VAR_OPCOES_MOB_FoLocObjIns of
    0: App_Jan.MostraFormOViArtigos(FSegmentoInsp, FSeccaoInsp,
       QrOViLocaisCodigo.Value, Artigo, QrOViLocaisNome.Value);
    1: App_Jan.MostraFormOViOPs(FSegmentoInsp, FSeccaoInsp,
       QrOViLocaisCodigo.Value, NrOP, QrOViLocaisNome.Value, NO_Artigo);
    else Grl_Geral.MB_Erro('Janela n�o implementada!' + sProcName);
  end;
end;

procedure TFmOViLocais.QrOViLocaisCalcFields(DataSet: TDataSet);
begin
  QrOViLocaisBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrLocais, 1, QrOViLocaisNome.Value);
end;

procedure TFmOViLocais.ReopenOViLocais(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
  TSeccaoInsp; ParteNome: String);
var
  TabGerCab, SQL_AND, SQL_Seccao: String;
begin
  TabGerCab := OVSM_PF.GetNomeTabSegmento_InspGerCab(FSegmentoInsp);
  SQL_AND := '';
  if Trim(ParteNome) <> '' then
    SQL_AND := 'AND loc.Nome LIKE "%' + ParteNome + '%"';
  SQL_Seccao := 'AND cll.AtribValr="' +
    Dmod.GetNomeAtribValr_SeccaoInsp(FSeccaoInsp) + '"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrOViLocais, Dmod.AllDB, [
  'SELECT loc.Codigo, loc.Nome, COUNT(loc.Codigo) AS "ROS::BIGINT" ',
  'FROM ovdlocal loc ',
  //'LEFT JOIN ovgispgercab igc ON igc.Local=loc.Codigo ',
  'LEFT JOIN ' + TabGerCab + ' igc ON igc.Local=loc.Codigo ',
  'LEFT JOIN ovdclaslocal cll ON cll.Codigo=loc.Codigo ',
  'WHERE igc.ZtatusIsp=' + Grl_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  SQL_AND,
  SQL_Seccao,
  'GROUP BY loc.Codigo ',
  'ORDER BY loc.Nome ',
  '']);
end;

procedure TFmOViLocais.SbPesquisaClick(Sender: TObject);
begin
  ReopenOViLocais(FSegmentoInsp, FSeccaoInsp, EdPesquisa.Text);
end;

end.
