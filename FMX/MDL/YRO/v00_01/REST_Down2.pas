unit REST_Down2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.Controls.Presentation, FMX.StdCtrls,
  System.JSON, FMX.ScrollBox, FMX.Memo, UnGrl_Vars, Datasnap.DBClient,
  UnDmkENums;

type
  TFmREST_Down2 = class(TForm)
    Panel1: TPanel;
    BtBaixarTudo: TButton;
    MeAvisos: TMemo;
    BtFechar: TButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    CdAux: TClientDataSet;
    CdCorda: TClientDataSet;
    BtSubirChamados: TButton;
    procedure BtBaixarTudoClick(Sender: TObject);
    procedure Teste(st: String);
    procedure BtFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure BtSubirChamadosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    sStrInfoDone, sKindInsp: String;
    FExcluiOVdLocal,
    FExcluiOVgIspGerCab,
    FExcluiOVdReferencia,
    FExcluiOVfOrdemProducao,
    FExcluiOVcYnsArqIts,
    FExcluiOVcYnsArqCad,
    FExcluiOVcYnsMedTop,
    FExcluiOVcYnsMedDim,
    FExcluiOVcYnsMedCad,
    FExcluiOVcYnsGraTop,
    FExcluiOVcYnsChkCad,
    FExcluiOVcYnsChkCtx,
    FExcluiOVcYnsChkTop,
    FExcluiOVcYnsQstCtx,
    FExcluiOVcYnsQstTop,
    FExcluiOVdProduto,
    FExcluiOVgItxGerCab,
    FExcluiOVcYnsExgTop,
    FExcluiOVcYnsExgCad,
    FExcluiOVcYnsMixTop,
    FExcluiOVcYnsMixOpc,
    FExcluiOVdClasLocal,
    FExcluiOVgTtxGerBtl,
    //
    FExcluiChmOcoCad,
    FExcluiChmOcoWho,
    FExcluiChmOcoEtp,
    FExcluiChmOcoDon,

    FExcluiChmHowCad,
    FExcluiWhoGerEnti: Boolean;
    //
    //function  DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE: String;
    //          ExcluiItensTabela: Boolean = True): Boolean;
    procedure BaixarDadosInspecoesFaccoes();
    procedure BaixarDadosInspecoesTexteis();
    procedure BaixarDadosChamadosOcorrencias();
    procedure InfoTempMemo(Texto: String; I: Integer = 0; K: Integer = 0);
    procedure InfoDoneMemo(Texto: String);
    procedure AbreQueryParaCorda(Tabela, Campo, SQL_WHERE, SQL_JOIN: String);
    procedure UploadChamadosFeitos();
  public
    { Public declarations }
    FAcaoDeInstrucaoDeNotificacaoMobile: TAcaoDeInstrucaoDeNotificacaoMobile;
    FCodigoLoc: Integer;
    //
  end;

var
  FmREST_Down2: TFmREST_Down2;

implementation

uses UnGrl_DmkDB, UnFMX_Geral, UnFMX_DmkRemoteQuery, UnOVSM_Vars, UnGrl_Geral,
  UnOVS_Consts, UnFMX_DmkUnlic, UnFMX_DmkProcFunc, UnApp_PF, UnApp_Jan,
  Principal,
  Module;

const
  NAO_EXCLUI = False;
  FMaxItems = 64;

{$R *.fmx}
{

procedure TFmREST_Down.BtBaixarTudoClick(Sender: TObject);
var
  procedure InfoTempMemo(Texto: String);
  begin
    MeAvisos.Text := Texto + '...' + sLineBreak + sStrInfoDone;
  end;
  procedure InfoDoneMemo(Texto: String);
  begin
    Application.ProcessMessages;
    sStrInfoDone := Grl_Geral.FDT(Now(), 109) + ' : ' +Texto + '!' + sLineBreak + sStrInfoDone;
    MeAvisos.Text := sStrInfoDone;
  end;
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
var
  SQL, JSON: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  BtBaixarTudo.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    sStrInfoDone := '';
    InfoTempMemo('Preparando downloads...');
    Alias := '';
    SQL_JOIN := '';
    SQL_WHERE := 'WHERE ZtatusIsp = ' +
      FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
    InfoTempMemo('Baixando configura��es de inspe��es');
    if App_PF.DownloadTabela('ovgispgercab', Alias, 'ovgispgercab', SQL_JOIN, SQL_WHERE) then
    begin
      InfoDoneMemo('Configura��es de inspe��es baixadas');
      ////////////////////////////////////////////////////////////////////////////
      ///
      /////////////////////   ovdlocal   /////////////////////////////////////////
      InfoTempMemo('Preparando baixa de locais das inspe��es ativas e finalizadas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Local ',
      'FROM ovmispmobcab ',
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT Local ',
      'FROM ovgispgercab ',
      EmptyStr]);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Local');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando locais das inspe��es ativas');
        if App_PF.DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Locais das inspe��es ativas baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      ///
      /////////////////////   ovdreferencia   ////////////////////////////////////
      InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT SeqGrupo ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'SeqGrupo');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas');
        if App_PF.DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      ///
      /////////////////////   ovfordemproducao ///////////////////////////////////
      InfoTempMemo('Preparando baixa de reduzidos de OP das inspe��es ativas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT NrReduzidoOP ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'NrReduzidoOP');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Corda + ')';
        InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas');
        if App_PF.DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////  Itens de Planos de Amostragem e Regime de Qualidade   ///////////
      InfoTempMemo('Preparando baixa itens de Planos de Amostragem e Regime de Qualidade');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsARQ ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade');
        if App_PF.DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de Planos de Amostragem e Regime de Qualidade baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////////  Planos de Amostragem e Regime de Qualidade   ////////////////
      InfoTempMemo('Preparando baixa de Cabe�alhos de Planos de Amostragem e Regime de Qualidade');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsARQ ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade');
        if App_PF.DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cabe�alhos de Planos de Amostragem e Regime de Qualidade baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ///
      /////////////////////  Itens de t�picos de Medidas   ///////////////////////
      InfoTempMemo('Preparando baixa de itens de t�picos de Medidas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsMed ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando itens de t�picos de Medidas');
        if App_PF.DownloadTabela('ovcynsmedtop', Alias, 'ovcynsmedtop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de t�picos de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ////////////////  Itens de medida de t�picos de Medidas   //////////////////
      InfoTempMemo('Preparando baixa de itens de medida de t�picos de Medidas');
  (*  J� foi no item acima!
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsMed ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando itens de medida de t�picos de Medidas');
        if App_PF.DownloadTabela('ovcynsmeddim', Alias, 'ovcynsmeddim', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de medida de t�picos de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////////////////  Cadastros de tabelas de Medidas   ///////////////////////
      InfoTempMemo('Preparando baixa de cadastros de tabelas de Medidas');
  (*  J� foi no item acima!
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsMed ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de tabelas de Medidas');
        if App_PF.DownloadTabela('ovcynsmedcad', Alias, 'ovcynsmedcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de cadastros de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      /////////////////////  Cadastros de t�picos de Medidas   ///////////////////////
      InfoTempMemo('Preparando baixa de cadastros de t�picos de Medidas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Tobiko ',
      'FROM ovcynsmedtop ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Tobiko');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de t�picos de Medidas');
        if App_PF.DownloadTabela('ovcynsgratop', Alias, 'ovcynsgratop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Itens de cadastros de Medidas baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de contextos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de contextos de inconformidades');
        if App_PF.DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de check lists de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de check lists de inconformidades');
        if App_PF.DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de check lists de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de contextos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de checklists de inconformidades');
  (*
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de contextos de de checklists inconformidades');
        if App_PF.DownloadTabela('ovcynschkctx', Alias, 'ovcynschkctx', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de contextos de checklists de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de t�picos de checklists de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de t�picos de checklists de inconformidades');
  (*
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT OVcYnsChk ',
      'FROM ovgispgercab ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
  *)
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de t�picos de checklists de inconformidades');
        if App_PF.DownloadTabela('ovcynschktop', Alias, 'ovcynschktop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de t�picos de checklists de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ///
      //////////////  Cadastros de contextos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Contexto ',
      'FROM ovcynschkctx ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Contexto');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de contextos de inconformidades');
        if App_PF.DownloadTabela('ovcynsqstctx', Alias, 'ovcynsqstctx', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      //////////////  Cadastros de t�picos de inconformidades   ////////////////
      InfoTempMemo('Preparando baixa de cadastros de t�picos de inconformidades');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Topico ',
      'FROM ovcynschktop ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Topico');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
        InfoTempMemo('Baixando cadastros de t�picos de inconformidades');
        if App_PF.DownloadTabela('ovcynsqsttop', Alias, 'ovcynsqsttop', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Cadastros de t�picos de inconformidades baixados');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      //
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      ///////////  D A D O S   B U S C A D O S   D O  ovfordemproducao ///////////
      ////////////////////////////////////////////////////////////////////////////
      ///
      ///////////////////////////   ovdproduto  //////////////////////////////////
      InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
      Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
      'SELECT DISTINCT Produto ',
      'FROM ovfordemproducao ',
      '']);
      Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Produto');
      //Grl_Geral.MB_Aviso(Corda);
      if Corda <> '' then
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := 'WHERE Controle IN (' + Corda + ')';
        InfoTempMemo('Baixando produtos usados nas inspe��es ativas');
        if App_PF.DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ///
      ///////////////////////////   ovdproduto  //////////////////////////////////
      InfoTempMemo('Preparando baixa de magnitudes de inconformidades');
      begin
        Alias := '';
        SQL_JOIN := '';
        SQL_WHERE := '';
        InfoTempMemo('Baixando magnitudes de inconformidades');
        if App_PF.DownloadTabela('ovcynsqstmag', Alias, 'ovcynsqstmag', SQL_JOIN, SQL_WHERE) then
        begin
          InfoDoneMemo('Magnitudes de inconformidades baixadas');
          //FMX_Geral.MB_Info(JSON);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////////
      ///
      ////////////////////////////////////////////////////////////////////////////
      ///////////  D A D O S   G E R A D O S   N O  O V E R S E E R  ////////////////////
      ////////////////////////////////////////////////////////////////////////////
      ///







      // No final!
      InfoDoneMemo('------------------------------------------');
      InfoDoneMemo('------------------------------------------');
      InfoDoneMemo('');
      InfoDoneMemo('Todas tabelas foram baixadas!');
      InfoDoneMemo('');
      InfoDoneMemo('');
      BtFechar.Visible := True;
      BtBaixarTudo.Visible := False;
    end;
  finally
    BtBaixarTudo.Enabled := True;
  end;
end;
}

procedure TFmREST_Down2.AbreQueryParaCorda(Tabela, Campo, SQL_WHERE, SQL_JOIN: String);
begin
  FMX_dmkRemoteQuery.QuerySelect(CdCorda, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT ' + Campo,
      'FROM ' + Lowercase(Tabela),
      SQL_WHERE,
      SQL_JOIN,
      EmptyStr
    ], VAR_AMBIENTE_APP);
end;

procedure TFmREST_Down2.BaixarDadosChamadosOcorrencias();
var
  I, K: Integer;
  Cordas, Cords2: TUMyArray_Str;
  Corda, Cord2, Alias, SQL_JOIN, SQL_WHERE: String;
  //Exclui: Boolean;
begin
  Alias := '';
  SQL_JOIN := '';
  sKindInsp := 'Ocorr�ncias';
  SQL_WHERE := 'WHERE LstFechDtH < "1900-01-01"';
  //
  InfoTempMemo('Baixando chamados de ocorr�ncias');
  AbreQueryParaCorda('chmococad', 'Codigo', SQL_WHERE, SQL_JOIN);
  Cordas := Grl_DmkDB.CordasDeQuery(CdCorda, 'Codigo', FMaxItems, Corda);
  if Corda <> '' then
  begin
    Alias := '';
    SQL_JOIN := '';
    K := Length(Cordas);
    //ExcluiOVgIspGerCab := True;
    for I := 0 to K -1 do
    begin
      SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
      InfoTempMemo('Baixando chamados de ocorr�ncias', I, K);
      if App_PF.DownloadTabela('chmococad', Alias, 'chmococad', SQL_JOIN, SQL_WHERE, FExcluiChmOcoCad, True) then
        FExcluiChmOcoCad := False;
    end;
    InfoDoneMemo('Chamados de ocorr�ncias baixadas');
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   chmocowho ///////////////////////////////////
    InfoTempMemo('Preparando baixa de designados ao chamados de ocorr�ncias ativas');
    (* Corda preparada acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Codigo ',
    'FROM chmococad ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Codigo', FMaxItems, Corda);
    *)
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*///
        SQL_WHERE := SQL_WHERE + ' AND WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER);
        //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*///
        InfoTempMemo('Baixando designados ao chamados de ocorr�ncias ativas', I, K);
        if App_PF.DownloadTabela('chmocowho', Alias, 'chmocowho', SQL_JOIN, SQL_WHERE, FExcluiChmOcoWho, True) then
          FExcluiChmOcoWho := False;
      end;
      InfoDoneMemo('Designados ao chamados de ocorr�ncias ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////   chmocodon ///////////////////////////////////
    InfoTempMemo('Preparando baixa de ocorr�ncias dos chamados ativos');
    (* Corda preparada acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Codigo ',
    'FROM chmococad ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Codigo', FMaxItems, Corda);
    *)
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Controle ',
    'FROM chmocowho ',
    'WHERE WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
    '']);
    Cords2 := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Controle', FMaxItems, Cord2);
    if (Corda <> '')
    and (Cord2 <> '') then
*)
    if (Corda <> '') then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*///
        //N�o pode por cousa do loop ! SQL_WHERE := SQL_WHERE + ' AND chmocowho IN (' + Cords2[I] + ')';
        //*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*//*///
        InfoTempMemo('Baixando ocorr�ncias dos chamados ativos', I, K);
        if App_PF.DownloadTabela('chmocodon', Alias, 'chmocodon', SQL_JOIN, SQL_WHERE, FExcluiChmOcoDon, True) then
          FExcluiChmOcoDOn := False;
      end;
      InfoDoneMemo('Ocorr�ncias dos chamados ativos baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////   chmocoetp ///////////////////////////////////
    InfoTempMemo('Preparando baixa de etapas dos chamados de ocorr�ncias ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT chmocoetp ',
    'FROM chmocodon ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'chmocoetp', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Controle IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando etapas dos chamados de ocorr�ncias ativas', I, K);
        if App_PF.DownloadTabela('chmocoetp', Alias, 'chmocoetp', SQL_JOIN, SQL_WHERE, FExcluiChmOcoEtp, True) then
          FExcluiChmOcoEtp := False;
      end;
      InfoDoneMemo('Etapas dos chamados de ocorr�ncias ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////   entidades ///////////////////////////////////
    InfoTempMemo('Preparando baixa de entidades dos chamados de ocorr�ncias ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT WhoGerEnti ',
    'FROM chmococad ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'WhoGerEnti', FMaxItems, Corda);
    //
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando entidades dos chamados de ocorr�ncias ativas', I, K);
        if App_PF.DownloadTabela('entidades', Alias, 'entidades', SQL_JOIN, SQL_WHERE, FExcluiWhoGerEnti, FALSE) then
          FExcluiWhoGerEnti := False;
      end;
      InfoDoneMemo('Entidades dos chamados de ocorr�ncias ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////   chmhowcad ///////////////////////////////////
    InfoTempMemo('Preparando baixa de t�tulos de fluxos dos chamados de ocorr�ncias ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT ChmHowCad ',
    'FROM chmococad ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'ChmHowCad', FMaxItems, Corda);
    //
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando t�tulos de fluxos dos chamados de ocorr�ncias ativas', I, K);
        if App_PF.DownloadTabela('chmhowcad', Alias, 'chmhowcad', SQL_JOIN, SQL_WHERE, FExcluiChmHowCad, True) then
          FExcluiChmHowCad := False;
      end;
      InfoDoneMemo('T�tulos de fluxos dos chamados de ocorr�ncias ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////////////   ovdlocal ///////////////////////////////////
    InfoTempMemo('Preparando baixa de locais dos chamados de ocorr�ncias ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT CodOnde ',
    'FROM chmococad ',
    'WHERE KndOnde=' + Grl_Geral.FF0(CO_KndOnde_3_COD_OVdLocal),
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'CodOnde', FMaxItems, Corda);
    //
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando locais dos chamados de ocorr�ncias ativas', I, K);
        if App_PF.DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE, FExcluiovdlocal, True) then
          FExcluiovdlocal := False;
      end;
      InfoDoneMemo('Locais de ocorr�ncias ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    ///  chmhowcad
  end;
end;

procedure TFmREST_Down2.BaixarDadosInspecoesFaccoes();
var
  I, K: Integer;
  Cordas: TUMyArray_Str;
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
  //Exclui: Boolean;
begin
  Alias := '';
  SQL_JOIN := '';
  sKindInsp := 'Fac��es';
  SQL_WHERE := 'WHERE ZtatusIsp = ' +
    FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
  //
  InfoTempMemo('Baixando configura��es de inspe��es');
  AbreQueryParaCorda('ovgispgercab', 'Codigo', SQL_WHERE, SQL_JOIN);
  Cordas := Grl_DmkDB.CordasDeQuery(CdCorda, 'Codigo', FMaxItems, Corda);
  if Corda <> '' then
  begin
    Alias := '';
    SQL_JOIN := '';
    K := Length(Cordas);
    //ExcluiOVgIspGerCab := True;
    for I := 0 to K -1 do
    begin
      SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
      InfoTempMemo('Baixando configura��es de inspe��es', I, K);
      if App_PF.DownloadTabela('ovgispgercab', Alias, 'ovgispgercab', SQL_JOIN, SQL_WHERE, FExcluiOVgIspGerCab, True) then
        FExcluiOVgIspGerCab := False;
    end;
    InfoDoneMemo('Configura��es de inspe��es ativas baixadas');
  //InfoTempMemo('Baixando configura��es de inspe��es');
  //if App_PF.DownloadTabela('ovgispgercab', Alias, 'ovgispgercab', SQL_JOIN, SQL_WHERE) then
  //begin
    //InfoDoneMemo('Configura��es de inspe��es baixadas');
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdlocal   /////////////////////////////////////////
    InfoTempMemo('Preparando baixa de locais das inspe��es ativas e finalizadas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Local ',
    'FROM ovmispmobcab ',
    ' ',
    'UNION ',
    ' ',
    'SELECT DISTINCT Local ',
    'FROM ovgispgercab ',
    EmptyStr]);
{
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Local');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando locais das inspe��es ativas');
      if App_PF.DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Locais das inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
}
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Local', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdLocal := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando locais das inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE, FExcluiOVdLocal, True) then
          FExcluiOVdLocal := False;
      end;
      InfoDoneMemo('Locais das inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdreferencia   ////////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT SeqGrupo ',
    'FROM ovgispgercab ',
    '']);
    (*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'SeqGrupo');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas');
      if App_PF.DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
    *)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'SeqGrupo', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdReferencia := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE, FExcluiOVdReferencia, True) then
          FExcluiOVdReferencia := False;
      end;
      InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;

    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovfordemproducao ///////////////////////////////////
    InfoTempMemo('Preparando baixa de reduzidos de OP das inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT NrReduzidoOP ',
    'FROM ovgispgercab ',
    '']);
{
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'NrReduzidoOP');
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Corda + ')';
      InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas');
      if App_PF.DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
}
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'NrReduzidoOP', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //ExcluiOVfOrdemProducao := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE, FExcluiOVfOrdemProducao, True) then
          FExcluiOVfOrdemProducao := False;
      end;
      InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////  Itens de Planos de Amostragem e Regime de Qualidade   ///////////
    InfoTempMemo('Preparando baixa itens de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgispgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade');
      if App_PF.DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de Planos de Amostragem e Regime de Qualidade baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsARQ', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsArqIts:= True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade', I, K);
        if App_PF.DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsArqIts, True) then
          FExcluiOVcYnsArqIts := False;
      end;
      InfoDoneMemo('Itens de planos de ae regime de qualidade baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////  Planos de Amostragem e Regime de Qualidade   ////////////////
    InfoTempMemo('Preparando baixa de Cabe�alhos de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgispgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsARQ');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade');
      if App_PF.DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cabe�alhos de Planos de Amostragem e Regime de Qualidade baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsARQ', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsArqCad := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade', I, K);
        if App_PF.DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsArqCad, True) then
          FExcluiOVcYnsArqCad := False;
      end;
      InfoDoneMemo('Cabe�alhos de planos de amostragem e regime de qualidade baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    ///
    /////////////////////  Itens de t�picos de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de itens de t�picos de Medidas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgispgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de t�picos de Medidas');
      if App_PF.DownloadTabela('ovcynsmedtop', Alias, 'ovcynsmedtop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de t�picos de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsMed', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsMedTop := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando itens de t�picos de Medidas', I, K);
        if App_PF.DownloadTabela('ovcynsmedtop', Alias, 'ovcynsmedtop', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsMedTop, True) then
          FExcluiOVcYnsMedTop := False;
      end;
      InfoDoneMemo('Itens de t�picos de medidas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    ////////////////  Itens de medida de t�picos de Medidas   //////////////////
    InfoTempMemo('Preparando baixa de itens de medida de t�picos de Medidas');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
*)
    //Grl_Geral.MB_Aviso(Corda);
(*
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de medida de t�picos de Medidas');
      if App_PF.DownloadTabela('ovcynsmeddim', Alias, 'ovcynsmeddim', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de medida de t�picos de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    //Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, ?, FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsMedDim := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando itens de medida de t�picos de Medidas', I, K);
        if App_PF.DownloadTabela('ovcynsmeddim', Alias, 'ovcynsmeddim', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsMedDim, True) then
          FExcluiOVcYnsMedDim := False;
      end;
        InfoDoneMemo('Itens de medida de t�picos de Medidas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////////////  Cadastros de tabelas de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de tabelas de medidas');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsMed ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsMed');
*)
    //Grl_Geral.MB_Aviso(Corda);
(*
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de tabelas de Medidas');
      if App_PF.DownloadTabela('ovcynsmedcad', Alias, 'ovcynsmedcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    //Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsMed', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsMedCad := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de tabelas de Medidas', I, K);
        if App_PF.DownloadTabela('ovcynsmedcad', Alias, 'ovcynsmedcad', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsMedCad, True) then
          FExcluiOVcYnsMedCad := False;
      end;
      InfoDoneMemo('Itens de cadastros de medidas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////////////  Cadastros de t�picos de Medidas   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de Medidas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Tobiko ',
    'FROM ovcynsmedtop ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Tobiko');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de Medidas');
      if App_PF.DownloadTabela('ovcynsgratop', Alias, 'ovcynsgratop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Medidas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Tobiko', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsGraTop := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de t�picos de Medidas', I, K);
        if App_PF.DownloadTabela('ovcynsgratop', Alias, 'ovcynsgratop', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsGraTop, True) then
          FExcluiOVcYnsGraTop := False;
      end;
      InfoDoneMemo('Itens de cadastros de medidas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
{   Tabela duplicada abaixo!
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de inconformidades');
      if App_PF.DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsChk', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsChkCad := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de contextos de inconformidades', I, K);
        if App_PF.DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsChkCad) then
          FExcluiOVcYnsChkCad := False;
      end;
      InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
}
    //////////////  Cadastros de check lists de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de check lists de inconformidades');
      if App_PF.DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de check lists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsChk', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsChkCad := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de check lists de inconformidades', I, K);
        if App_PF.DownloadTabela('ovcynschkcad', Alias, 'ovcynschkcad', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsChkCad, True) then
          FExcluiOVcYnsChkCad := False;
      end;
      InfoDoneMemo('Cadastros de check lists de inconformidades baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de checklists de inconformidades');
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
*)
    //Grl_Geral.MB_Aviso(Corda);
(*
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de de checklists inconformidades');
      if App_PF.DownloadTabela('ovcynschkctx', Alias, 'ovcynschkctx', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de checklists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    //Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsChk', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsChkCtx := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de contextos de de checklists inconformidades', I, K);
        if App_PF.DownloadTabela('ovcynschkctx', Alias, 'ovcynschkctx', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsChkCtx, True) then
          FExcluiOVcYnsChkCtx := False;
      end;
      InfoDoneMemo('Cadastros de contextos de checklists de inconformidades baixados');
      //FMX_Geral.MB_Info(JSON);
    end;

    ///
    //////////////  Cadastros de t�picos de checklists de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de checklists de inconformidades');
(*
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsChk ',
    'FROM ovgispgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsChk');
*)
    //Grl_Geral.MB_Aviso(Corda);
(*
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de checklists de inconformidades');
      if App_PF.DownloadTabela('ovcynschktop', Alias, 'ovcynschktop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de t�picos de checklists de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    //Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsChk', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsChkTop := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de t�picos de checklists de inconformidades', I, K);
        if App_PF.DownloadTabela('ovcynschktop', Alias, 'ovcynschktop', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsChkTop, True) then
          FExcluiOVcYnsChkTop := False;
      end;
      InfoDoneMemo('Cadastros de t�picos de checklists de inconformidades baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    ///
    //////////////  Cadastros de contextos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de contextos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Contexto ',
    'FROM ovcynschkctx ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Contexto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de contextos de inconformidades');
      if App_PF.DownloadTabela('ovcynsqstctx', Alias, 'ovcynsqstctx', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Contexto', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsQstCtx := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de contextos de inconformidades', I, K);
        if App_PF.DownloadTabela('ovcynsqstctx', Alias, 'ovcynsqstctx', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsQstCtx, True) then
          FExcluiOVcYnsQstCtx := False;
      end;
      InfoDoneMemo('Cadastros de contextos de inconformidades baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    //////////////  Cadastros de t�picos de inconformidades   ////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de inconformidades');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Topico ',
    'FROM ovcynschktop ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Topico');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de t�picos de inconformidades');
      if App_PF.DownloadTabela('ovcynsqsttop', Alias, 'ovcynsqsttop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Cadastros de t�picos de inconformidades baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Topico', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsQstTop := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de t�picos de inconformidades', I, K);
        if App_PF.DownloadTabela('ovcynsqsttop', Alias, 'ovcynsqsttop', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsQstTop, True) then
          FExcluiOVcYnsQstTop := False;
      end;
      InfoDoneMemo('Cadastros de t�picos de inconformidades baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    //
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///////////  D A D O S   B U S C A D O S   D O  ovfordemproducao ///////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ///////////////////////////   ovdproduto  //////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Produto ',
    'FROM ovfordemproducao ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Produto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Controle IN (' + Corda + ')';
      InfoTempMemo('Baixando produtos usados nas inspe��es ativas');
      if App_PF.DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Produto', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdProduto := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Controle IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando produtos usados nas inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE, FExcluiOVdProduto, True) then
          FExcluiOVdProduto := False;
      end;
      InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;
  end;
end;

procedure TFmREST_Down2.BaixarDadosInspecoesTexteis();
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
  Cordas: TUMyArray_Str;
  //Exclui: Boolean;
  I, K: Integer;
begin
  Alias := '';
  SQL_JOIN := '';
  sKindInsp := 'T�xteis';
  SQL_WHERE := 'WHERE ZtatusIsp = ' +
    FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
  //
  InfoTempMemo('Baixando configura��es de inspe��es');
  AbreQueryParaCorda('ovgitxgercab', 'Codigo', SQL_WHERE, SQL_JOIN);
  Cordas := Grl_DmkDB.CordasDeQuery(CdCorda, 'Codigo', FMaxItems, Corda);
  if Corda <> '' then
  begin
    Alias := '';
    SQL_JOIN := '';
    K := Length(Cordas);
    //FExcluiOVgItxGerCab:= True;
    for I := 0 to K -1 do
    begin
      SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
      InfoTempMemo('Baixando configura��es de inspe��es', I, K);
      if App_PF.DownloadTabela('ovgitxgercab', Alias, 'ovgitxgercab', SQL_JOIN, SQL_WHERE, FExcluiOVgItxGerCab, True) then
        FExcluiOVgItxGerCab := False;
    end;
    InfoDoneMemo('Configura��es de inspe��es ativas baixadas');
    //FMX_Geral.MB_Info(JSON);
  //if App_PF.DownloadTabela('ovgitxgercab', Alias, 'ovgitxgercab', SQL_JOIN, SQL_WHERE) then
  //begin
    //InfoDoneMemo('Configura��es de inspe��es baixadas');

    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovgitxgerbtl   /////////////////////////////////////
    InfoTempMemo('Preparando baixa de OPs de Tinturaria das inspe��es ativas'); // e finalizadas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ovgitxgercab ',
    EmptyStr]);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Codigo');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando OPs de Tinturaria das inspe��es ativas');
      if App_PF.DownloadTabela('ovgitxgerbtl', Alias, 'ovgitxgerbtl', SQL_JOIN, SQL_WHERE, NAO_EXCLUI) then
      begin
        InfoDoneMemo('OPs de Tinturaria das inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Codigo', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdLocal := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando OPs de Tinturaria das inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovgitxgerbtl', Alias, 'ovgitxgerbtl', SQL_JOIN, SQL_WHERE, FExcluiOVgTtxGerBtl, True) then
          FExcluiOVgTtxGerBtl := False;
      end;
      InfoDoneMemo('OPs de Tinturaria das inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;

    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdlocal   /////////////////////////////////////////
    InfoTempMemo('Preparando baixa de locais das inspe��es ativas e finalizadas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Local ',
    'FROM ovmitxmobcab ',
    ' ',
    'UNION ',
    ' ',
    'SELECT DISTINCT Local ',
    'FROM ovgitxgercab ',
    EmptyStr]);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Local');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando locais das inspe��es ativas');
      if App_PF.DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE, NAO_EXCLUI) then
      begin
        InfoDoneMemo('Locais das inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Local', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdLocal := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando locais das inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdlocal', Alias, 'ovdlocal', SQL_JOIN, SQL_WHERE, FExcluiOVdLocal, True) then
          FExcluiOVdLocal := False;
      end;
      InfoDoneMemo('Locais das inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovdreferencia   ////////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT SeqGrupo ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'SeqGrupo');
    //Grl_Geral.MB_Aviso(Corda);
(*
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas');
      if App_PF.DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'SeqGrupo', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdReferencia := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando refer�ncias usadas nas inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdreferencia', Alias, 'ovdreferencia', SQL_JOIN, SQL_WHERE, FExcluiOVdReferencia, True) then
          FExcluiOVdReferencia := False;
      end;
      InfoDoneMemo('Refer�ncias usadas nas inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ///
    /////////////////////   ovfordemproducao ///////////////////////////////////
    InfoTempMemo('Preparando baixa de reduzidos de OP das inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT NrReduzidoOP ',
    'FROM ovgitxgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'NrReduzidoOP');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Corda + ')';
      InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas');
      if App_PF.DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'NrReduzidoOP', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVfOrdemProducao := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE NrReduzidoOP IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando reduzidos de OP das inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovfordemproducao', Alias, 'ovfordemproducao', SQL_JOIN, SQL_WHERE, FExcluiOVfOrdemProducao, True) then
          FExcluiOVfOrdemProducao := False;
      end;
      InfoDoneMemo('Reduzidos de OP das inspe��es ativas baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
(*  N�o tem no T�xtil!!!
    /////////  Itens de Planos de Amostragem e Regime de Qualidade   ///////////
    InfoTempMemo('Preparando baixa itens de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgitxgercab ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsARQ', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      Exclui := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando itens de Planos de Amostragem e Regime de Qualidade', I, K);
        if App_PF.DownloadTabela('ovcynsarqits', Alias, 'ovcynsarqits', SQL_JOIN, SQL_WHERE, Exclui) then
          Exclui := False;
      end;
      InfoDoneMemo('Itens de Planos de Amostragem e Regime de Qualidade baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////  Planos de Amostragem e Regime de Qualidade   ////////////////
    InfoTempMemo('Preparando baixa de Cabe�alhos de Planos de Amostragem e Regime de Qualidade');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsARQ ',
    'FROM ovgitxgercab ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsARQ', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      Exclui := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando Cabe�alhos Planos de Amostragem e Regime de Qualidade', I, K);
        if App_PF.DownloadTabela('ovcynsarqcad', Alias, 'ovcynsarqcad', SQL_JOIN, SQL_WHERE, Exclui) then
          Exclui := False;
      end;
      InfoDoneMemo('Cabe�alhos de Planos de Amostragem e Regime de Qualidade baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    FIM n�o tem no T�xtil!!!
*)
    ///
    ///
    /////////////////////  Itens de t�picos de Exa��es   ///////////////////////
    InfoTempMemo('Preparando baixa de itens de t�picos de Exa��es');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsExg ',
    'FROM ovgitxgercab ',
    '']);
(*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsExg');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando itens de t�picos de Exa��es');
      if App_PF.DownloadTabela('ovcynsexgtop', Alias, 'ovcynsexgtop', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de t�picos de Exa��es baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsExg', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsExgTop:= True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando itens de t�picos de Exa��es', I, K);
        if App_PF.DownloadTabela('ovcynsexgtop', Alias, 'ovcynsexgtop', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsExgTop, True) then
          FExcluiOVcYnsExgTop := False;
      end;
      InfoDoneMemo('Itens de t�picos de exa��es baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////////////  Cadastros de tabelas de Exa��es   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de tabelas de Exa��es');
(*  J� foi no item acima!
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT OVcYnsExg ',
    'FROM ovgitxgercab ',
    '']);
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'OVcYnsExg');
*)
    //Grl_Geral.MB_Aviso(Corda);
(*
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Codigo IN (' + Corda + ')';
      InfoTempMemo('Baixando cadastros de tabelas de Exa��es');
      if App_PF.DownloadTabela('ovcynsexgcad', Alias, 'ovcynsexgcad', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Itens de cadastros de Exa��es baixados');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
*)
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'OVcYnsExg', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsExgCad := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de tabelas de Exa��es', I, K);
        if App_PF.DownloadTabela('ovcynsexgcad', Alias, 'ovcynsexgcad', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsExgCad, True) then
          FExcluiOVcYnsExgCad := False;
      end;
      InfoDoneMemo('Itens de cadastros de exa��es baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////////////  Cadastros de t�picos de Exa��es   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros de t�picos de Exa��es');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Topyko ',
    'FROM ovcynsexgtop ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Topyko', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsMixTop := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros de t�picos de Exa��es', I, K);
        if App_PF.DownloadTabela('ovcynsmixtop', Alias, 'ovcynsmixtop', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsMixTop, True) then
          FExcluiOVcYnsMixTop := False;
      end;
      InfoDoneMemo('Itens de cadastros de exa��es baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    /////////////////////  Cadastros de Avalia��es de T�picos de Exa��es   ///////////////////////
    InfoTempMemo('Preparando baixa de cadastros avalia��es de t�picos de Exa��es');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ovcynsmixtop ',
    '']);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Codigo', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVcYnsMixOpc := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando cadastros avalia��es de t�picos de exa��es', I, K);
        if App_PF.DownloadTabela('ovcynsmixopc', Alias, 'ovcynsmixopc', SQL_JOIN, SQL_WHERE, FExcluiOVcYnsMixOpc, True) then
          FExcluiOVcYnsMixOpc := False;
      end;
      InfoDoneMemo('Itens de cadastros de avalia��es de t�picos de exa��es baixados');
      //FMX_Geral.MB_Info(JSON);
    end;
    ///
    ///
    //
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///////////  D A D O S   B U S C A D O S   D O  ovfordemproducao ///////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ///////////////////////////   ovdproduto  //////////////////////////////////
    InfoTempMemo('Preparando baixa de refer�ncias usadas nas inspe��es ativas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Produto ',
    'FROM ovfordemproducao ',
    '']);
    (*
    Corda := Grl_DmkDB.CordaDeQuery(Dmod.QrAux, 'Produto');
    //Grl_Geral.MB_Aviso(Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := 'WHERE Controle IN (' + Corda + ')';
      InfoTempMemo('Baixando produtos usados nas inspe��es ativas');
      if App_PF.DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE) then
      begin
        InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;*)

    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Produto', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdProduto := False; ////////////////  C U I D A D O  ////////////////////////
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Controle IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando produtos usados nas inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdproduto', Alias, 'ovdproduto', SQL_JOIN, SQL_WHERE, FExcluiOVdProduto, True) then
          FExcluiOVdProduto := False;
      end;
      InfoDoneMemo('Produtos usados nas inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;
  end;
end;

procedure TFmREST_Down2.BtBaixarTudoClick(Sender: TObject);
var
  Corda, Alias, SQL_JOIN, SQL_WHERE: String;
  Cordas: TUMyArray_Str;
  SQL, JSON: String;
  I, K, Codigo: Integer;
  //Exclui: Boolean;
  SecConfeccao, SecTecelagem, SecTinturaria: String;
begin
  sStrInfoDone := '';
  MeAvisos.Lines.Clear;
 // MeAvisos.Lines.Clear;
  //
  FExcluiOVgIspGerCab     := True;
  FExcluiOVdLocal         := True;
  FExcluiOVdReferencia    := True;
  FExcluiOVfOrdemProducao := True;
  FExcluiOVcYnsArqIts     := True;
  FExcluiOVcYnsArqCad     := True;
  FExcluiOVcYnsMedTop     := True;
  FExcluiOVcYnsMedDim     := True;
  FExcluiOVcYnsMedCad     := True;
  FExcluiOVcYnsGraTop     := True;
  FExcluiOVcYnsChkCad     := True;
  FExcluiOVcYnsChkCtx     := True;
  FExcluiOVcYnsChkTop     := True;
  FExcluiOVcYnsQstCtx     := True;
  FExcluiOVcYnsQstTop     := True;
  FExcluiOVdProduto       := True;
  FExcluiOVgItxGerCab     := True;
  FExcluiOVcYnsExgTop     := True;
  FExcluiOVcYnsExgCad     := True;
  FExcluiOVcYnsMixTop     := True;
  FExcluiOVcYnsMixOpc     := True;
  FExcluiOVdClasLocal     := True;
  FExcluiOVgTtxGerBtl     := True;
  //
  FExcluiChmOcoCad        := True;
  FExcluiChmOcoWho        := True;
  FExcluiChmOcoEtp        := True;
  FExcluiChmOcoDon        := True;
  FExcluiChmHowCad        := True;
  FExcluiWhoGerEnti       := True;
  //
  sStrInfoDone := '';
  sKindInsp    := '';
  FMX_DmkPF.VibrarComoBotao();
  BtBaixarTudo.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;

    ////////////////////////////////////////////////////////////////////////////
    UploadChamadosFeitos();
    ////////////////////////////////////////////////////////////////////////////

    InfoTempMemo('Preparando downloads...');
    Alias := '';
    SQL_JOIN := '';
    SQL_WHERE := 'WHERE ZtatusIsp = ' +
      FMX_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////  D A D O S   P A R A   I N S P E � � O   D E   F A C � � O  ////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    if Dmod.QrOVcMobDevCadSccConfeccao.Value = 1 then
      BaixarDadosInspecoesFaccoes();
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////  D A D O S   P A R A   I N S P E � � O   D E   T � X T I L  ////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    if (Dmod.QrOVcMobDevCadSccTecelagem.Value = 1)
    or (Dmod.QrOVcMobDevCadSccTinturaria.Value = 1) then
      BaixarDadosInspecoesTexteis();
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////  D A D O S   P A R A   Q U A L Q U E R   I N S P E � � O  //////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////  ovcynsqstmag  ////////////////////////////////
    sKindInsp := 'Comum';
    InfoTempMemo('Preparando baixa de magnitudes de inconformidades');
    begin
      Alias := '';
      SQL_JOIN := '';
      SQL_WHERE := '';
      InfoTempMemo('Baixando magnitudes de inconformidades');
      if App_PF.DownloadTabela('ovcynsqstmag', Alias, 'ovcynsqstmag', SQL_JOIN, SQL_WHERE, True, False) then
      begin
        InfoDoneMemo('Magnitudes de inconformidades baixadas');
        //FMX_Geral.MB_Info(JSON);
      end;
    end;
   /////////////////////   ovdclaslocal   //////////////////////////////////////
    InfoTempMemo('Preparando baixa de atributos de locais das inspe��es ativas e finalizadas');
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ovdlocal ',
    EmptyStr]);
    Cordas := Grl_DmkDB.CordasDeQuery(Dmod.QrAux, 'Codigo', FMaxItems, Corda);
    if Corda <> '' then
    begin
      Alias := '';
      SQL_JOIN := '';
      K := Length(Cordas);
      //FExcluiOVdClasLocal := True;
      for I := 0 to K -1 do
      begin
        SQL_WHERE := 'WHERE Codigo IN (' + Cordas[I] + ')';
        InfoTempMemo('Baixando atributos de locais das inspe��es ativas', I, K);
        if App_PF.DownloadTabela('ovdclaslocal', Alias, 'ovdclaslocal', SQL_JOIN, SQL_WHERE, FExcluiOVdClasLocal, True) then
          FExcluiOVdClasLocal := False;
      end;
      InfoDoneMemo('Atributos de locais das inspe��es ativas baixadas');
      //FMX_Geral.MB_Info(JSON);
    end;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    ///////////  D A D O S   G E R A D O S   N O  O V E R S E E R  ////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///
   ////////////////////////   opcoesapp   //////////////////////////////////////
    InfoTempMemo('Preparando baixa de dados de opcoes');
    if FMX_dmkRemoteQuery.QuerySelect(CdAux, VAR_CNPJ_DEVICE_IN_SERVER,
      [
        'SELECT SecConfeccao, SecTecelagem, SecTinturaria ',
        'FROM opcoesapp',
        'WHERE Codigo=1'
      ], VAR_AMBIENTE_APP) then
    begin
      SecConfeccao  := CdAux.FieldByName('SecConfeccao').AsString;
      SecTecelagem  := CdAux.FieldByName('SecTecelagem').AsString;
      SecTinturaria := CdAux.FieldByName('SecTinturaria').AsString;
      //
(*
      if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtSQLite, stUpd, 'ovsmopcoes', False, [
      'SecConfeccao', 'SecTecelagem', 'SecTinturaria'], [
      'Codigo'], [
      SecConfeccao, SecTecelagem, SecTinturaria], [
      1], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
      False, SQLExec) then
      begin
        FMX_dmkRemoteQuery.QueryExecute(VAR_CNPJ_DEVICE_IN_SERVER, SQLExec, VAR_AMBIENTE_APP);
      end;
*)
      //if
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovsmopcoes', False, [
      'SecConfeccao', 'SecTecelagem', 'SecTinturaria'], [
      'Codigo'], [
      SecConfeccao, SecTecelagem, SecTinturaria], [
      1], False,
      TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile);
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    ////////////////////////////////////////////////////////////////////////////
    //////  D A D O S  D E  C H A M A D O S  D E  O C O R R � N C I A S  ///////
    ////////////////////////////////////////////////////////////////////////////
    ///
    if (Dmod.QrOVcMobDevCadChmOcorrencias.Value = 1) then
      BaixarDadosChamadosOcorrencias();
    end;
    InfoDoneMemo('Dados de op��es baixadas');
      // No final!
    sKindInsp := '';
    InfoDoneMemo('------------------------------------------');
    InfoDoneMemo('------------------------------------------');
    InfoDoneMemo('');
    InfoDoneMemo('Todas tabelas foram baixadas!');
    InfoDoneMemo('');
    InfoDoneMemo('');
    BtFechar.Visible := True;
    BtBaixarTudo.Visible := False;
    //
    if FAcaoDeInstrucaoDeNotificacaoMobile =
    TAcaoDeInstrucaoDeNotificacaoMobile.ainmChamadoOcorrencia then
    begin
      Codigo := FCodigoLoc;
      FCodigoLoc := 0;
      FAcaoDeInstrucaoDeNotificacaoMobile :=
        TAcaoDeInstrucaoDeNotificacaoMobile.ainmIndef;
      App_Jan.MostraFormChmOcoCab(Codigo);
      Close;
    end;
  finally
    BtBaixarTudo.Enabled := True;
  end;
end;

procedure TFmREST_Down2.BtFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFmREST_Down2.BtSubirChamadosClick(Sender: TObject);
begin
  sStrInfoDone := '';
  MeAvisos.Lines.Clear;
  FMX_DmkPF.VibrarComoBotao();
  BtSubirChamados.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    ///
    UploadChamadosFeitos();
    ///
    BtFechar.Visible := True;
  finally
    BtSubirChamados.Enabled := True;
  end;
end;

{
function TFmREST_Down.DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE:
  String): Boolean;
var
  Campos, Lit, SQL, JSON, _Alias, MyAlias: String;
begin
  Result := False;
  Campos := '';
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := FDPragma.FieldByName('Name').AsString + sLineBreak;
  SQL := MyAlias + FDPragma.FieldByName('Name').AsString;
  Lit := FDPragma.FieldByName('Name').AsString;
  FDPragma.Next;
  while not FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + FDPragma.FieldByName('Name').AsString + sLineBreak;
    SQL := SQL + ', ' + MyAlias + FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    FDPragma.Next;
  end;
  //
  //FMX_Geral.MB_Info(SQL);
  //FMX_Geral.MB_Info(Lit);
  SQL :=
    'SELECT ' + SQL + sLineBreak +
    'FROM ' + TabSrc + _Alias + sLineBreak +
    SQL_JOIN + sLineBreak +
    SQL_WHERE + '';
  //
  Lit := 'INSERT INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  //Grl_Geral.MB_Aviso(SQL);
  if FMX_DmkRemoteQuery.JsonText([SQL], JSON) then
  begin
    //FMX_Geral.MB_Info(JSON);
    Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, ['DELETE FROM ' + TabDst]);
    if JSON <> '[]' then
    begin
      Lit := Lit + FMX_DmkRemoteQuery.ParseJson2(JSON);
      //FMX_Geral.MB_Info(Lit);
      Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, [Lit]);
      //
    end;
    Result := True;
  end;
end;
}

{
function TFmREST_Down2.DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE:
  String; ExcluiItensTabela: Boolean): Boolean;
var
  Campos, Lit, SQL, JSON, _Alias, MyAlias: String;
begin
  Result := False;
  Campos := '';
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := FDPragma.FieldByName('Name').AsString + sLineBreak;
  SQL := MyAlias + FDPragma.FieldByName('Name').AsString;
  Lit := FDPragma.FieldByName('Name').AsString;
  FDPragma.Next;
  while not FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + FDPragma.FieldByName('Name').AsString + sLineBreak;
    SQL := SQL + ', ' + MyAlias + FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    FDPragma.Next;
  end;
  //
  //FMX_Geral.MB_Info(SQL);
  //FMX_Geral.MB_Info(Lit);
  SQL :=
    'SELECT ' + SQL + sLineBreak +
    'FROM ' + TabSrc + _Alias + sLineBreak +
    SQL_JOIN + sLineBreak +
    SQL_WHERE + '';
  //
  //Lit := 'INSERT INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  Lit := 'INSERT OR REPLACE INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  //Grl_Geral.MB_Aviso(SQL);
  if FMX_DmkRemoteQuery.JsonText([SQL], JSON) then
  begin
    //FMX_Geral.MB_Info(JSON);
    if ExcluiItensTabela then
      Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, ['DELETE FROM ' + TabDst]);
    if JSON <> '[]' then
    begin
      Lit := Lit + FMX_DmkRemoteQuery.ParseJson2(JSON);
      //FMX_Geral.MB_Info(Lit);
      Grl_DmkDB.ExecutaSQLQuery0(FDPragma, Dmod.AllDB, [Lit]);
      //
    end;
    Result := True;
  end;
end;
}

procedure TFmREST_Down2.FormCreate(Sender: TObject);
begin
  FAcaoDeInstrucaoDeNotificacaoMobile := TAcaoDeInstrucaoDeNotificacaoMobile.ainmIndef;
  FCodigoLoc := 0;
end;

procedure TFmREST_Down2.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmREST_Down2.FormShow(Sender: TObject);
begin
  BtFechar.Visible := False;
  //BtBaixarTudo.Visible := True;
  MeAvisos.Lines.Clear;
end;

procedure TFmREST_Down2.InfoDoneMemo(Texto: String);
begin
  Application.ProcessMessages;
  sStrInfoDone := Grl_Geral.FDT(Now(), 109) + ' : ' + sKindInsp + ' ' + Texto +
    '!' + sLineBreak + sStrInfoDone;
  MeAvisos.Text := sStrInfoDone;
end;

procedure TFmREST_Down2.InfoTempMemo(Texto: String; I: Integer; K: Integer);
begin
  Application.ProcessMessages;
  if K > 0 then
    MeAvisos.Text := sKindInsp + ' ' + IntToStr(I+1) + '/' + IntToStr(K) + ' - ' +
    Texto + '...' + sLineBreak + sStrInfoDone
  else
    MeAvisos.Text := sKindInsp + ' ' + Texto + '...' + sLineBreak + sStrInfoDone;
end;

procedure TFmREST_Down2.Teste(st: String);
//procedure ParseJSonValue;
var
  JSonValue: TJSonValue;
  Codigo, Local: String;
  Branch: String;
  I: Integer;
  //
  LRows, LElements, LItem : TJSONValue;
  LJsonObj, JSonObject: TJSONObject;
begin
   JSonObject := TJSonObject.Create;
   JsonValue := JSonObject.ParseJSONValue(st);
   FMX_Geral.MB_Info(FMX_Geral.FF0(JSonObject.Count));
(*
  LJsonObj    := TJSONObject.ParseJSONValue(st,0, True) as TJSONObject;
  //try
     LRows:=LJsonObj.Get(0).JsonValue;
     LElements:=TJSONObject(TJSONArray(LRows).Get(0)).Get(0).JsonValue;
     LItem :=TJSONObject(TJSONArray(LElements).Get(0)).Get(0).JsonValue;
     //ShowMessage(TJSONObject(LItem).Get('buying_rate').JsonValue.Value);
     FMX_Geral.MB_Info(FMX_Geral.FF0(LJsonObj.Count));
*)


// [{"Codigo":"1","Nome":null,....}....]
   //st := '{"data":{"results":[{"Branch":"ACCT590003"}]}}';
   //JsonValue := TJSonObject.ParseJSONValue(st);
   for I := 0 to 1 do
   begin
     Codigo := JsonValue.GetValue<string>('[' + FMX_Geral.FF0(I) + '].Codigo');
     Local := JsonValue.GetValue<string>('[' + FMX_Geral.FF0(I) + '].Local');
     FMX_Geral.MB_Info(Codigo + ' >> ' + Local);
   end;
   JsonValue.Free;
   JSonObject.Free;
end;

procedure TFmREST_Down2.UploadChamadosFeitos();
begin
  InfoTempMemo('Fazendo upload chamados reliazados...');
  if Dmod.UploadChamadosFeitos(MeAvisos) then
  begin
    InfoDoneMemo('Upload de chamados reliazados finalizado!');
  end;
end;

end.
