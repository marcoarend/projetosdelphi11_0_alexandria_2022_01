unit OpcoesYROM;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit,
  UnDmkEnums, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, UnGrl_Vars, FMX.DialogService;

type
  TFmOpcoesYROM = class(TForm)
    ToolBar1: TToolBar;
    TitleLabel: TLabel;
    PnScale: TPanel;
    Panel3: TPanel;
    BtOK: TButton;
    CdOVcMobDevCad: TClientDataSet;
    CdOVcMobDevCadCodigo: TIntegerField;
    CdOVcMobDevCadDeviceID: TStringField;
    CdOVcMobDevCadUserNmePdr: TStringField;
    CdOVcMobDevCadPIN: TStringField;
    CdOVcMobDevCadFoLocObjIns: TSmallintField;
    CdOVcMobDevCadScaleX: TIntegerField;
    CdOVcMobDevCadScaleY: TIntegerField;
    Button1: TButton;
    EdERPNameByCli: TEdit;
    Label3: TLabel;
    CdCtrlGeral: TClientDataSet;
    CdCtrlGeralERPNameByCli: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    LaScaleX: TLabel;
    EdScaleX: TEdit;
    LaScaleY: TLabel;
    EdScaleY: TEdit;
    Panel2: TPanel;
    Label1: TLabel;
    EdMailUser: TEdit;
    GroupBox1: TGroupBox;
    RbFoLocObjIns_0: TRadioButton;
    RbFoLocObjIns_1: TRadioButton;
    GBEstiloForms: TGroupBox;
    RBAir: TRadioButton;
    RBAmakrits: TRadioButton;
    RBAquaGraphite: TRadioButton;
    RBBlend: TRadioButton;
    RBDark: TRadioButton;
    RBGoldenGraphite: TRadioButton;
    RBLight: TRadioButton;
    RBRubyGraphite: TRadioButton;
    RBTransparent: TRadioButton;
    RBNenhum: TRadioButton;
    Panel1: TPanel;
    BtOpcoesAvancadas: TButton;
    BtSincroServer: TButton;
    Panel5: TPanel;
    BtNovaVersao: TButton;
    procedure BtOKClick(Sender: TObject);
    //
    procedure StyleRadioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure BtOpcoesAvancadasClick(Sender: TObject);
    procedure BtSincroServerClick(Sender: TObject);
    procedure BtNovaVersaoClick(Sender: TObject);
  private
    { Private declarations }
    FEstiloForms: String;
  public
    { Public declarations }
  end;

var
  FmOpcoesYROM: TFmOpcoesYROM;

implementation

uses UnYROM_Vars, UnGrl_Geral, UnGrl_DmkDB, UnFMX_dmkRemoteQuery, UnApp_PF,
  UnFMX_DmkProcFunc, UnFMX_DmkForms,
  (*&¨%$#@!"
  OpcoesAvancadas,
  *)
  UnFMX_DmkUnLic,
  Module;

{$R *.fmx}

procedure TFmOpcoesYROM.BtNovaVersaoClick(Sender: TObject);
begin
  FMX_dmkUnLic.VerificaAtualizacaoVersao();
end;

procedure TFmOpcoesYROM.BtOKClick(Sender: TObject);
var
  I, Codigo, ScaleX, ScaleY, FoLocObjIns: Integer;
  SQLType: TSQLType;
  sSQL, ERPNameByCli, EstiloForms, MailUser: String;
  Component: TComponent;
begin
  FMX_DmkPF.VibrarComoBotao();
  SQLType        := stNil;
  Codigo         := VAR_COD_DEVICE_IN_SERVER;
  ScaleX         := Grl_Geral.IMV(EdScaleX.Text);
  ScaleY         := Grl_Geral.IMV(EdScaleY.Text);
  FoLocObjIns    := 0;
  if ScaleX = 0 then
    ScaleX := 100;
  if ScaleY = 0 then
    ScaleY := 100;
  if RbFoLocObjIns_1.IsChecked then
    FoLocObjIns    := 1;
  //
  EstiloForms := FEstiloForms;
  //
  ERPNameByCli     := EdERPNameByCli.Text;
  MailUser         := EdMailUser.Text;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB,
  stUpd, 'yocmobdevatv', False, [
  'FoLocObjIns', 'ScaleX', 'ScaleY'], [
  'Codigo'], [
  FoLocObjIns, ScaleX, ScaleY], [
  Codigo], False,
  TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Dmod.ReopenYROMOpcoes();
    if Dmod.QrYROMOpcoes.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'yromopcoes', False, [
    'ERPNameByCli', 'EstiloForms', 'MailUser'], [
    'Codigo'], [
    ERPNameByCli, EstiloForms, MailUser], [
    1], False,
    TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
    begin
      SQLType := stUpd;
      if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'ovcmobdevcad', False, [
      'FoLocObjIns', 'ScaleX', 'ScaleY'], [
      'Codigo'], [
      FoLocObjIns, ScaleX, ScaleY], [
      Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
      False, sSQL) then
      begin
        if FMX_dmkRemoteQuery.SQL_Executa(sSQL) then
        begin
          Dmod.ReopenYROMOpcoes();
          Close;
        end;
      end;
    end;
  end;
end;

procedure TFmOpcoesYROM.BtOpcoesAvancadasClick(Sender: TObject);
var
  Senha: String;
begin
  (*&¨%$#@!"
  FMX_DmkPF.VibrarComoBotao();
  Senha := '';
  TDialogService.InputQuery('Opções Avançadas', ['Informe a senha de acesso'], [Senha],
  procedure(const AResult: TModalResult; const AValues: array of string)
  begin
    if AResult = mrOk then
    begin
      Senha := AValues[0];
      if Senha = '82526' then
      begin
        FMX_DmkForms.CriaFm_AllOS0(TFmOpcoesAvancadas, FmOpcoesAvancadas,
          fcmCreateTryShwM, True, False);
      end else
        Grl_Geral.MB_Aviso('Senha incorreta');
    end;
  end);
*)
end;

procedure TFmOpcoesYROM.BtSincroServerClick(Sender: TObject);
var
  //Corda,
  Alias, SQL_JOIN, SQL_WHERE: String;
  Exclui: Boolean;
begin
  Alias := '';
  SQL_JOIN := '';
  Exclui := True;
  //
  FMX_DmkPF.VibrarComoBotao();
  BtSincroServer.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    //InfoTempMemo('Preparando downloads...');
    SQL_WHERE := 'WHERE DeviceID="' + CdOVcMobDevCadDeviceID.Value + '"';
    //InfoTempMemo('Baixando configurações de inspeções', I, K);
    if App_PF.DownloadTabela('ovcmobdevcad', Alias, 'ovcmobdevcad', SQL_JOIN, SQL_WHERE, Exclui, True) then
      Exclui := False;
    //
    Dmod.ReopenOVcMobDevCad();
    Grl_Geral.MB_Info('Sincronização finalizada!');
  finally
    BtSincroServer.Enabled := True;
  end;
end;

procedure TFmOpcoesYROM.FormCreate(Sender: TObject);
begin
  LaScaleX.Visible := False;
  LaScaleY.Visible := False;
  EdScaleX.Visible := False;
  EdScaleY.Visible := False;
end;

procedure TFmOpcoesYROM.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOpcoesYROM.FormShow(Sender: TObject);
var
  I: Integer;
  RB: TRadioButton;
begin
  FMX_dmkRemoteQuery.QuerySelect(CdOVcMobDevCad, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN, ',
      // Opcooes
      'ScaleX, ScaleY, FoLocObjIns ',
      // Fim Opcoes
      'FROM ovcmobdevcad',
      'WHERE Codigo=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER) + ''
    ], VAR_AMBIENTE_APP);
  if CdOVcMobDevCad.RecordCount > 0 then
  begin
    EdScaleX.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleX.Value);
    EdScaleY.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleY.Value);
    case CdOVcMobDevCadFoLocObjIns.Value of
      0: RbFoLocObjIns_0.IsChecked := True;
      1: RbFoLocObjIns_1.IsChecked := True;
    end;
  end else
  begin
    EdScaleX.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleX.Value);
    EdScaleY.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleY.Value);
    RbFoLocObjIns_0.IsChecked := True;
  end;
  Dmod.ReopenYROMOpcoes();
  RB := TRadioButton(FmOpcoesYROM.FindComponent('RB' + Dmod.QrYROMOpcoesEstiloForms.Value));
  if RB <> nil then
    RB.IsChecked := True;
  //////////////////////////////////////////////////////////////////////////////
  ///
  FMX_dmkRemoteQuery.QuerySelect(CdCtrlGeral, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT ERPNameByCli ',
      'FROM ctrlgeral',
      'WHERE Codigo=1',
      EmptyStr
    ], VAR_AMBIENTE_APP);
  EdERPNameByCli.Text := CdCtrlGeralERPNameByCli.Value;
  EdMailUser.Text   := Dmod.QrYROMOpcoesMailUser.Value;
end;

procedure TFmOpcoesYROM.StyleRadioClick(Sender: TObject);
var
  I: Integer;
begin
  FEstiloForms := TRadioButton(Sender).Text;
  if FEstiloForms = RBNenhum.Text then
  begin
    if StyleBook <> nil then
      StyleBook := nil;
  end else
  begin
    for I := 0 to FmOpcoesYROM.ComponentCount - 1 do
    begin
      if FmOpcoesYROM.Components[I] is TStyleBook then
        if TStyleBook(FmOpcoesYROM.Components[I]).Name = 'StyleBook_' +
        FEstiloForms then
        begin
          StyleBook := TStyleBook(FmOpcoesYROM.Components[I]);
          Break;
        end;
    end;
  end;
{
  if RBNenhum.IsChecked then
  begin
    if StyleBook <> nil then
      StyleBook := nil;
  end else
  begin
    for I := 0 to FmOpcoesYROM.ComponentCount - 1 do
    begin
      if FmOpcoesYROM.Components[I] is TRadioButton then
        if (TRadioButton(FmOpcoesYROM.Components[I]).GroupName) = Lowercase('EstiloForm') then
          if TRadioButton(FmOpcoesYROM.Components[I]).IsChecked then
            EstiloForms := TRadioButton(FmOpcoesYROM.Components[I]).Text;
    end;

    for I := 0 to FmOpcoesYROM.ComponentCount - 1 do
    begin
      if FmOpcoesYROM.Components[I] is TStyleBook then
        if TStyleBook(FmOpcoesYROM.Components[I]).Name = 'StyleBook_' +
        EstiloForms then
        begin
          StyleBook := TStyleBook(FmOpcoesYROM.Components[I]);
          Break;
        end;
    end;
  end;
}
end;

end.
