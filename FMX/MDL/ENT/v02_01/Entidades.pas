unit Entidades;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.SearchBox, System.Actions, FMX.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, System.Math,
  FMX.Platform, FMX.PhoneDialer, FMX.DateTimeCtrls, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  Xml.xmldom, Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc;

type
  TPesqType = (stEMuni=0, stEPais=1, stPMuni=2, stPPais=3);
  TFmEntidades = class(TForm)
    TabControl1: TTabControl;
    TBILista: TTabItem;
    TBIVisualiza: TTabItem;
    TBIEdita: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbInclui: TSpeedButton;
    SearchBox1: TSearchBox;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    QrEntidades: TFDQuery;
    TabControl2: TTabControl;
    TabItem4: TTabItem;
    TabItem5: TTabItem;
    TabControl3: TTabControl;
    TabItem6: TTabItem;
    TabItem7: TTabItem;
    ListBox4: TListBox;
    ListBox5: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBIECodigo: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIRazaoSocial: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICNPJ: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIETe1: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBIEEmail: TListBoxItem;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBIPCodigo: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader8: TListBoxGroupHeader;
    LBICPF: TListBoxItem;
    ListBoxGroupHeader9: TListBoxGroupHeader;
    LBIPTe1: TListBoxItem;
    ListBoxGroupHeader10: TListBoxGroupHeader;
    LBIPEmail: TListBoxItem;
    Ativo: TListBoxGroupHeader;
    LBIEAtivo: TListBoxItem;
    ListBoxGroupHeader11: TListBoxGroupHeader;
    LBIPAtivo: TListBoxItem;
    QrEntidadesView: TFDQuery;
    QrEntidadesViewCodigo: TIntegerField;
    QrEntidadesViewNome: TWideStringField;
    BindingsList1: TBindingsList;
    Label2: TLabel;
    LBIFantasia: TListBoxItem;
    ListBoxGroupHeader18: TListBoxGroupHeader;
    ListBoxGroupHeader19: TListBoxGroupHeader;
    LBIIE: TListBoxItem;
    ListBoxGroupHeader20: TListBoxGroupHeader;
    LBIECel: TListBoxItem;
    ListBoxGroupHeader21: TListBoxGroupHeader;
    LBIApelido: TListBoxItem;
    ListBoxGroupHeader22: TListBoxGroupHeader;
    LBIRG: TListBoxItem;
    ListBoxGroupHeader23: TListBoxGroupHeader;
    LBIPCel: TListBoxItem;
    TBIEditaEnd: TTabItem;
    ListBox6: TListBox;
    TabControl4: TTabControl;
    TBEndJuridica: TTabItem;
    TBEndFisica: TTabItem;
    ListBoxHeader4: TListBoxHeader;
    SBEndDesiste: TSpeedButton;
    SBEndConfirma: TSpeedButton;
    ChangeTabAction4: TChangeTabAction;
    TabItem3: TTabItem;
    ListBox9: TListBox;
    ListBoxGroupHeader24: TListBoxGroupHeader;
    ListBoxGroupHeader25: TListBoxGroupHeader;
    ListBoxGroupHeader26: TListBoxGroupHeader;
    ListBoxGroupHeader27: TListBoxGroupHeader;
    ListBoxGroupHeader28: TListBoxGroupHeader;
    ListBoxGroupHeader29: TListBoxGroupHeader;
    ListBoxGroupHeader30: TListBoxGroupHeader;
    ListBoxGroupHeader31: TListBoxGroupHeader;
    ListBoxGroupHeader32: TListBoxGroupHeader;
    LBIEndCEP: TListBoxItem;
    LBIEndLograd: TListBoxItem;
    LBIEndRua: TListBoxItem;
    LBIEndNumero: TListBoxItem;
    LBIEndCompl: TListBoxItem;
    LBIEndBairro: TListBoxItem;
    LBIEndUF: TListBoxItem;
    LBIEndCodMunici: TListBoxItem;
    LBIEndCodiPais: TListBoxItem;
    QrTipoLogradE: TFDQuery;
    QrTipoLogradP: TFDQuery;
    QrTipoLogradECodigo: TIntegerField;
    QrTipoLogradENome: TStringField;
    QrTipoLogradPCodigo: TIntegerField;
    QrTipoLogradPNome: TStringField;
    QrUFsE: TFDQuery;
    QrUFsEDTB: TStringField;
    QrUFsENome: TStringField;
    QrUFsP: TFDQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    QrUFsECodigo: TIntegerField;
    QrUFsPCodigo: TIntegerField;
    LBIEENatal: TListBoxItem;
    ListBoxGroupHeader33: TListBoxGroupHeader;
    ListBoxGroupHeader34: TListBoxGroupHeader;
    LBIEPNatal: TListBoxItem;
    TabItem8: TTabItem;
    ListView1: TListView;
    QrEntiContat: TFDQuery;
    IntegerField1: TIntegerField;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TStringField;
    QrEntiContatDtaNatal: TDateField;
    Layout1: TLayout;
    LBJuridica: TListBox;
    ListBoxGroupHeader12: TListBoxGroupHeader;
    ListBoxItem8: TListBoxItem;
    EdRazaoSocial: TEdit;
    ClearEditButton1: TClearEditButton;
    ListBoxItem11: TListBoxItem;
    EdFantasia: TEdit;
    ClearEditButton9: TClearEditButton;
    ListBoxGroupHeader14: TListBoxGroupHeader;
    ListBoxItem6: TListBoxItem;
    EdETe1: TEdit;
    ClearEditButton3: TClearEditButton;
    ListBoxItem13: TListBoxItem;
    EdECel: TEdit;
    ClearEditButton11: TClearEditButton;
    ListBoxItem5: TListBoxItem;
    EdEEmail: TEdit;
    ClearEditButton4: TClearEditButton;
    ListBoxGroupHeader13: TListBoxGroupHeader;
    ListBoxItem7: TListBoxItem;
    EdCNPJ: TEdit;
    ClearEditButton2: TClearEditButton;
    ListBoxItem12: TListBoxItem;
    EdIE: TEdit;
    ClearEditButton10: TClearEditButton;
    ListBoxGroupHeader35: TListBoxGroupHeader;
    ListBoxItem40: TListBoxItem;
    DEENatal: TDateEdit;
    ListBoxItem9: TListBoxItem;
    CkEAtivo: TCheckBox;
    Layout2: TLayout;
    LBFisica: TListBox;
    ListBoxGroupHeader15: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    EdNome: TEdit;
    ClearEditButton5: TClearEditButton;
    ListBoxItem14: TListBoxItem;
    EdApelido: TEdit;
    ClearEditButton12: TClearEditButton;
    ListBoxGroupHeader17: TListBoxGroupHeader;
    ListBoxItem3: TListBoxItem;
    EdPTe1: TEdit;
    ClearEditButton7: TClearEditButton;
    ListBoxItem16: TListBoxItem;
    EdPCel: TEdit;
    ClearEditButton14: TClearEditButton;
    ListBoxItem4: TListBoxItem;
    EdPEmail: TEdit;
    ClearEditButton8: TClearEditButton;
    ListBoxGroupHeader16: TListBoxGroupHeader;
    ListBoxItem2: TListBoxItem;
    EdCPF: TEdit;
    ClearEditButton6: TClearEditButton;
    ListBoxItem15: TListBoxItem;
    EdRG: TEdit;
    ClearEditButton13: TClearEditButton;
    ListBoxGroupHeader36: TListBoxGroupHeader;
    ListBoxItem41: TListBoxItem;
    DEPNatal: TDateEdit;
    ListBoxItem10: TListBoxItem;
    CkPAtivo: TCheckBox;
    Layout3: TLayout;
    LBEndJuridica: TListBox;
    ListBoxItem18: TListBoxItem;
    SBEndECep: TSpeedButton;
    EdEndECep: TEdit;
    ClearEditButton15: TClearEditButton;
    ListBoxItem17: TListBoxItem;
    CBEndELograd: TComboBox;
    SBLimpaLograd: TSpeedButton;
    ListBoxItem19: TListBoxItem;
    EdEndERua: TEdit;
    ClearEditButton22: TClearEditButton;
    ListBoxItem20: TListBoxItem;
    EdEndENumero: TEdit;
    ClearEditButton16: TClearEditButton;
    ListBoxItem21: TListBoxItem;
    EdEndECompl: TEdit;
    ClearEditButton23: TClearEditButton;
    ListBoxItem22: TListBoxItem;
    EdEndEBairro: TEdit;
    ClearEditButton27: TClearEditButton;
    ListBoxItem23: TListBoxItem;
    CBEndEUF: TComboBox;
    SBLimpaUF: TSpeedButton;
    ListBoxItem24: TListBoxItem;
    SBEndECodMunici: TSpeedButton;
    EdEndECodMunici: TEdit;
    ClearEditButton21: TClearEditButton;
    ListBoxItem25: TListBoxItem;
    SBEndECodiPais: TSpeedButton;
    EdEndECodiPais: TEdit;
    ClearEditButton24: TClearEditButton;
    Layout4: TLayout;
    LBEndFisica: TListBox;
    ListBoxItem26: TListBoxItem;
    SBEndPCep: TSpeedButton;
    EdEndPCep: TEdit;
    ClearEditButton18: TClearEditButton;
    ListBoxItem27: TListBoxItem;
    CBEndPLograd: TComboBox;
    ListBoxItem28: TListBoxItem;
    EdEndPRua: TEdit;
    ClearEditButton28: TClearEditButton;
    ListBoxItem29: TListBoxItem;
    EdEndPNumero: TEdit;
    ClearEditButton19: TClearEditButton;
    ListBoxItem30: TListBoxItem;
    EdEndPCompl: TEdit;
    ClearEditButton29: TClearEditButton;
    ListBoxItem31: TListBoxItem;
    EdEndPBairro: TEdit;
    ClearEditButton30: TClearEditButton;
    ListBoxItem32: TListBoxItem;
    CBEndPUF: TComboBox;
    ListBoxItem33: TListBoxItem;
    SBEndPCodMunici: TSpeedButton;
    EdEndPCodMunici: TEdit;
    ClearEditButton17: TClearEditButton;
    ListBoxItem34: TListBoxItem;
    SBEndPCodiPais: TSpeedButton;
    EdEndPCodiPais: TEdit;
    ClearEditButton20: TClearEditButton;
    BindSourceDB1: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    BindSourceDB2: TBindSourceDB;
    LinkFillControlToField2: TLinkFillControlToField;
    BindSourceDB3: TBindSourceDB;
    LinkFillControlToField3: TLinkFillControlToField;
    BindSourceDB4: TBindSourceDB;
    LinkFillControlToField4: TLinkFillControlToField;
    BindSourceDB5: TBindSourceDB;
    LinkFillControlToField5: TLinkFillControlToField;
    BindSourceDB6: TBindSourceDB;
    LinkFillControlToField6: TLinkFillControlToField;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    SBContato: TSpeedButton;
    SBEndereco: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdETe1Exit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdPTe1Exit(Sender: TObject);
    procedure SBEndConfirmaClick(Sender: TObject);
    procedure SBEndDesisteClick(Sender: TObject);
    procedure SBEndECepClick(Sender: TObject);
    procedure SBEndPCodMuniciClick(Sender: TObject);
    procedure SBEndPCodiPaisClick(Sender: TObject);
    procedure SBEndECodiPaisClick(Sender: TObject);
    procedure SBEndECodMuniciClick(Sender: TObject);
    procedure ClearEditButton17Click(Sender: TObject);
    procedure ClearEditButton20Click(Sender: TObject);
    procedure ClearEditButton21Click(Sender: TObject);
    procedure ClearEditButton24Click(Sender: TObject);
    procedure SBContatoClick(Sender: TObject);
    procedure ListView1ItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure SBLimpaLogradClick(Sender: TObject);
    procedure SBLimpaUFClick(Sender: TObject);
    procedure LBIETe1Click(Sender: TObject);
    procedure LBIECelClick(Sender: TObject);
    procedure LBIEEmailClick(Sender: TObject);
    procedure LBIPTe1Click(Sender: TObject);
    procedure LBIPCelClick(Sender: TObject);
    procedure LBIPEmailClick(Sender: TObject);
    procedure EdRGExit(Sender: TObject);
    procedure EdPCelExit(Sender: TObject);
    procedure EdECelExit(Sender: TObject);
    procedure EdIEExit(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormFocusChanged(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SBEndPCepClick(Sender: TObject);
  private
    { Private declarations }
    FCodigo: Integer;
    PhoneDialerService: IFMXPhoneDialerService;
    constructor Create(AOwner: TComponent); override;
    procedure ReopenEntidades(Codigo: Integer);
    procedure ReopenEntiContat(Codigo: Integer);
    procedure ReopenEntidadesView;
    procedure ReopenTipoLograd(Query: TFDQuery);
    procedure ReopenUFs(Query: TFDQuery);
    procedure MostraPesquisa(Componente: TObject; Codigo: Integer; Pesquisa: TPesqType);
    procedure ObtemEnderecoCEP(CEP: String);
    //
    //Form Scrool Teclado Virtual
    procedure CalcContentBoundsProc(Sender: TObject; var ContentBounds: TRectF);
    procedure RestorePosition(ListBox: TListBox; Layout: TLayout);
    procedure UpdateKBBounds(ListBox: TListBox; Layout: TLayout);
  public
    { Public declarations }
  end;

var
  FmEntidades: TFmEntidades;

implementation

uses Principal, Module, UnGeral, UnAllOS_DmkProcFunc, UnAllOS_DmkDB, UnGrl_Vars,
  UnDmkEnums, MyListas, UnAllOS_EntidadesJan, UnAllOS_Hardware, UnAllOS_DmkWeb;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmEntidades.ClearEditButton17Click(Sender: TObject);
begin
  EdEndPCodMunici.Text := '';
  EdEndPCodMunici.Tag  := 0;
end;

procedure TFmEntidades.ClearEditButton20Click(Sender: TObject);
begin
  EdEndPCodiPais.Text := '';
  EdEndPCodiPais.Tag  := 0;
end;

procedure TFmEntidades.ClearEditButton21Click(Sender: TObject);
begin
  EdEndECodMunici.Text := '';
  EdEndECodMunici.Tag  := 0;
end;

procedure TFmEntidades.ClearEditButton24Click(Sender: TObject);
begin
  EdEndECodiPais.Text := '';
  EdEndECodiPais.Tag  := 0;
end;

constructor TFmEntidades.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  TPlatformServices.Current.SupportsPlatformService(IFMXPhoneDialerService, IInterface(PhoneDialerService));
end;

procedure TFmEntidades.EdCNPJExit(Sender: TObject);
begin
  EdCNPJ.Text := Geral.SoNumero_TT(EdCNPJ.Text);
end;

procedure TFmEntidades.EdCPFExit(Sender: TObject);
begin
  EdCPF.Text := Geral.SoNumero_TT(EdCPF.Text);
end;

procedure TFmEntidades.EdECelExit(Sender: TObject);
begin
  EdECel.Text := Geral.SoNumero_TT(EdECel.Text);
end;

procedure TFmEntidades.EdETe1Exit(Sender: TObject);
begin
  EdETe1.Text := Geral.SoNumero_TT(EdETe1.Text);
end;

procedure TFmEntidades.EdIEExit(Sender: TObject);
begin
  EdIE.Text := Geral.SoNumero_TT(EdIE.Text);
end;

procedure TFmEntidades.EdPCelExit(Sender: TObject);
begin
  EdPCel.Text := Geral.SoNumero_TT(EdPCel.Text);
end;

procedure TFmEntidades.EdPTe1Exit(Sender: TObject);
begin
  EdPTe1.Text := Geral.SoNumero_TT(EdPTe1.Text);
end;

procedure TFmEntidades.EdRGExit(Sender: TObject);
begin
  EdRG.Text := Geral.SoNumero_TT(EdRG.Text);
end;

procedure TFmEntidades.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TBILista;
  FCodigo                 := 0;
  //
  LBJuridica.OnCalcContentBounds    := CalcContentBoundsProc;
  LBFisica.OnCalcContentBounds      := CalcContentBoundsProc;
  LBEndJuridica.OnCalcContentBounds := CalcContentBoundsProc;
  LBEndFisica.OnCalcContentBounds   := CalcContentBoundsProc;
  //
  ReopenEntidadesView;
end;

procedure TFmEntidades.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds(LBJuridica, Layout1);
  UpdateKBBounds(LBFisica, Layout2);
  UpdateKBBounds(LBEndJuridica, Layout3);
  UpdateKBBounds(LBEndFisica, Layout4);
end;

procedure TFmEntidades.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  VAR_KBBOUNDS.Create(0, 0, 0, 0);
  VAR_NEEDOFFSET := False;
  //
  RestorePosition(LBJuridica, Layout1);
  RestorePosition(LBFisica, Layout2);
  RestorePosition(LBEndJuridica, Layout3);
  RestorePosition(LBEndFisica, Layout4);
end;

procedure TFmEntidades.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  VAR_KBBOUNDS             := TRectF.Create(Bounds);
  VAR_KBBOUNDS.TopLeft     := ScreenToClient(VAR_KBBOUNDS.TopLeft);
  VAR_KBBOUNDS.BottomRight := ScreenToClient(VAR_KBBOUNDS.BottomRight);
  //
  UpdateKBBounds(LBJuridica, Layout1);
  UpdateKBBounds(LBFisica, Layout2);
  UpdateKBBounds(LBEndJuridica, Layout3);
  UpdateKBBounds(LBEndFisica, Layout4);
end;

procedure TFmEntidades.LBIECelClick(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  AllOS_Hardware.Telefone_FazerLigacao(LBIECel.Text, PhoneDialerService, True);
  {$ENDIF}
end;

procedure TFmEntidades.LBIEEmailClick(Sender: TObject);
begin
  AllOS_Hardware.Clipboard_CopiarTexto(LBIEEmail.Text);
end;

procedure TFmEntidades.LBIETe1Click(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  AllOS_Hardware.Telefone_FazerLigacao(LBIETe1.Text, PhoneDialerService, True);
  {$ENDIF}
end;

procedure TFmEntidades.LBIPCelClick(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  AllOS_Hardware.Telefone_FazerLigacao(LBIPCel.Text, PhoneDialerService, True);
  {$ENDIF}
end;

procedure TFmEntidades.LBIPEmailClick(Sender: TObject);
begin
  AllOS_Hardware.Clipboard_CopiarTexto(LBIPEmail.Text);
end;

procedure TFmEntidades.LBIPTe1Click(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  AllOS_Hardware.Telefone_FazerLigacao(LBIPTe1.Text, PhoneDialerService, True);
  {$ENDIF}
end;

procedure TFmEntidades.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := AllOS_dmkPF.GetSelectedValue(ListBox1);
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmEntidades.ListView1ItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  Controle: Integer;
begin
  Controle := AllOS_dmkPF.GetSelectedValue(ListView1);
  //
  AllOS_EntidadesJan.MostraEntiContat(FCodigo, Controle, QrEntiContat, Dmod.MyDB);
end;

procedure TFmEntidades.MostraPesquisa(Componente: TObject; Codigo: Integer;
  Pesquisa: TPesqType);

  function ObtemUF_DTB(AObject: TObject; Query: TFDQuery): String;
  var
    UF: Integer;
  begin
    Result := '';
    //
    UF := AllOS_dmkPF.GetSelectedValue(AObject);
    //
    if UF = 0 then
    begin
      Geral.MB_Aviso('O campo UF deve ser preenchido!');
      Exit;
    end else
    begin
      if Query.Locate('Codigo', UF, []) then
        Result := Query.FieldByName('DTB').Value;
    end;
  end;

var
  UF_DTB: String;
  SQL: array of String;
begin
  case Pesquisa of
    stEMuni, stPMuni:
    begin
      if Pesquisa = stEMuni then
        UF_DTB := ObtemUF_DTB(CBEndEUF, QrUFsE)
      else
        UF_DTB := ObtemUF_DTB(CBEndPUF, QrUFsP);
      //
      if UF_DTB <> '' then
      begin
        if Dmod.MyDB.DriverName = 'SQLite' then
        begin
          SQL := [
            'SELECT Codigo, Nome ',
            'FROM dtb_munici ',
            'WHERE SUBSTR(Codigo, 1, 2) LIKE "' + UF_DTB + '" ',
            'ORDER BY Nome ',
            ''];
        end else
        begin
          SQL := [
            'SELECT Codigo, Nome ',
            'FROM dtb_munici ',
            'WHERE LEFT(Codigo, 2)=' + UF_DTB,
            'ORDER BY Nome ',
            ''];
        end;
        AllOS_dmkPF.CriaFm_PesqCod(Componente, Codigo, SQL);
      end;
    end;
    stEPais, stPPais:
    begin
      SQL := [
        'SELECT Codigo, Nome ',
        'FROM bacen_pais ',
        'ORDER BY Nome ',
        ''];
      AllOS_dmkPF.CriaFm_PesqCod(Componente, Codigo, SQL);
    end;
  end;
end;

procedure TFmEntidades.ObtemEnderecoCEP(CEP: String);
var
  Res: String;
  i: Integer;
  XMLArqRes: IXMLDocument;
begin
  if CEP <> '' then
  begin
    Res := AllOS_dmkWeb.REST_URL_Post(
      ['AplicativoSolicitante', 'IDUsuarioSolicitante', 'Token'],
      [Geral.FF0(CO_DMKID_APP), Geral.FF0(VAR_WEB_USER_ID), VAR_WEB_TOKEN_DMK],
      ['method', 'CEP'],
      ['REST_ConsultaCEP', Geral.SoNumero_TT(CEP)], CO_URL_DMKAPI);
    //
    if Res <> '' then
    begin
      XMLArqRes := TXMLDocument.Create(nil);
      XMLArqRes.Active := False;
      try
        XMLArqRes.LoadFromXML(Res);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLArqRes.DocumentElement.ChildNodes[i] do
          begin
            if Pos('ConsultaCEP', Res) > 0 then
            begin
              if TabControl4.ActiveTab = TBEndFisica then //F�sica
              begin
                CBEndPLograd.ItemIndex := AllOS_dmkDB.ObtemIndexDeTabela(
                                            QrTipoLogradP, 'Codigo',
                                            Geral.IMV(ChildNodes['codlograd'].Text));
                EdEndPRua.Text         := ChildNodes['rua'].Text;
                EdEndPNumero.Text      := '';
                EdEndPCompl.Text       := '';
                EdEndPBairro.Text      := ChildNodes['bairro'].Text;
                CBEndPUF.ItemIndex     := AllOS_dmkDB.ObtemIndexDeTabela(
                                            QrUFsP, 'Codigo',
                                            Geral.IMV(ChildNodes['coduf'].Text));
                EdEndPCodMunici.Text   := ChildNodes['cidade'].Text;
                EdEndPCodMunici.Tag    := Geral.IMV(ChildNodes['codmunici'].Text);
                EdEndPCodiPais.Text    := ChildNodes['pais'].Text;
                EdEndPCodiPais.Tag     := Geral.IMV(ChildNodes['codpais'].Text);
              end else
              begin
                CBEndELograd.ItemIndex := AllOS_dmkDB.ObtemIndexDeTabela(
                                            QrTipoLogradE, 'Codigo',
                                            Geral.IMV(ChildNodes['codlograd'].Text));
                EdEndERua.Text         := ChildNodes['rua'].Text;
                EdEndENumero.Text      := '';
                EdEndECompl.Text       := '';
                EdEndEBairro.Text      := ChildNodes['bairro'].Text;
                CBEndEUF.ItemIndex     := AllOS_dmkDB.ObtemIndexDeTabela(
                                            QrUFsE, 'Codigo',
                                            Geral.IMV(ChildNodes['coduf'].Text));
                EdEndECodMunici.Text   := ChildNodes['cidade'].Text;
                EdEndECodMunici.Tag    := Geral.IMV(ChildNodes['codmunici'].Text);
                EdEndECodiPais.Text    := ChildNodes['pais'].Text;
                EdEndECodiPais.Tag     := Geral.IMV(ChildNodes['codpais'].Text);
              end;
            end;
          end;
        end;
      finally
        ;
      end;
    end;
  end;
end;

procedure TFmEntidades.ReopenEntiContat(Codigo: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntiContat, Dmod.MyDB, [
    'SELECT * ',
    'FROM wenticonta ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntidades.ReopenEntidades(Codigo: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntidades, Dmod.MyDB, [
    'SELECT elo.Nome ELograd_TXT, plo.Nome PLograd_TXT, ',
    'euf.Nome EUF_TXT, puf.Nome PUF_TXT, ',
    'emu.Nome ECodMunici_TXT, pmu.Nome PCodMunici_TXT, ',
    'ebp.Nome ECodiPais_TXT, pbp.Nome PCodiPais_TXT, ent.* ',
    'FROM wentidades ent ',
    'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd ',
    'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd ',
    'LEFT JOIN ufs euf ON euf.Codigo=ent.EUF ',
    'LEFT JOIN ufs puf ON puf.Codigo=ent.PUF ',
    'LEFT JOIN dtb_munici emu ON emu.Codigo=ent.ECodMunici ',
    'LEFT JOIN dtb_munici pmu ON pmu.Codigo=ent.PCodMunici ',
    'LEFT JOIN bacen_pais ebp ON ebp.Codigo=ent.ECodiPais ',
    'LEFT JOIN bacen_pais pbp ON pbp.Codigo=ent.PCodiPais ',
    'WHERE ent.Codigo=' + Geral.FF0(Codigo),
    'AND ent.CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND ent.LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntidades.ReopenEntidadesView;
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntidadesView, Dmod.MyDB, [
    'SELECT Codigo, ',
    'CASE WHEN Tipo = 0 ',
    '  THEN RazaoSocial ',
    '  ELSE Nome END Nome ',
    'FROM wentidades ',
    'WHERE CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
  //
  if QrEntidadesView.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmEntidades.SbConfirmaClick(Sender: TObject);
var
  Tipo, Codigo, Ativo: Integer;
  RazaoSocial, Fantasia, CNPJ, IE, ETe1, ECel, EEmail, ENatal,
  Nome, Apelido, CPF, RG, PTe1, PCel, PEmail, PNatal: String;
  SQLTipo: TSQLType;
begin
  Codigo := FCodigo;
  Tipo   := TabControl2.TabIndex;
  //
  if Tipo = 0 then
  begin
    //Jur�dica
    RazaoSocial := EdRazaoSocial.Text;
    Fantasia    := EdFantasia.Text;
    CNPJ        := Geral.SoNumero_TT(EdCNPJ.Text);
    IE          := Geral.SoNumero_TT(EdIE.Text);
    ETe1        := Geral.SoNumero_TT(EdETe1.Text);
    ECel        := Geral.SoNumero_TT(EdECel.Text);
    EEmail      := EdEEmail.Text;
    Ativo       := Geral.BoolToInt(CkEAtivo.IsChecked);
    //
    if DEENatal.IsEmpty = True then
      ENatal := '0000-00-00'
    else
      ENatal := Geral.FDT(DEENatal.Date, 1);
    //
    Nome    := '';
    Apelido := '';
    CPF     := '';
    RG      := '';
    PTe1    := '';
    PCel    := '';
    PEmail  := '';
    PNatal  := '0000-00-00';
    //
    if AllOS_dmkPF.FIC(RazaoSocial = '', 'Defina uma descri��o!') then Exit;
  end else
  begin
    //F�sica
    Nome    := EdNome.Text;
    Apelido := EdApelido.Text;
    CPF     := Geral.SoNumero_TT(EdCPF.Text);
    RG      := Geral.SoNumero_TT(EdRG.Text);
    PTe1    := Geral.SoNumero_TT(EdPTe1.Text);
    PCel    := Geral.SoNumero_TT(EdPCel.Text);
    PEmail  := EdPEmail.Text;
    Ativo   := Geral.BoolToInt(CkPAtivo.IsChecked);
    //
    if DEPNatal.IsEmpty = True then
      PNatal := '0000-00-00'
    else
      PNatal := Geral.FDT(DEPNatal.Date, 1);
    //
    RazaoSocial := '';
    Fantasia    := '';
    CNPJ        := '';
    IE          := '';
    ETe1        := '';
    ECel        := '';
    EEmail      := '';
    ENatal      := '0000-00-00';
    //
    if AllOS_dmkPF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  end;
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := AllOS_dmkDB.ObtemCodigoInt_Sinc('wentidades', 'Codigo', Dmod.QrAux, Dmod.MyDB);
  end else
    SQLTipo := stUpd;
  //
  if Codigo <> 0 then
  begin
    AllOS_dmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo, 'wentidades', False,
      ['Tipo', 'RazaoSocial', 'Fantasia', 'CNPJ', 'IE', 'ETe1', 'ECel', 'EEmail', 'ENatal',
       'Nome', 'Apelido', 'CPF', 'RG', 'PTe1', 'PCel', 'PEmail', 'PNatal', 'Ativo'],
      ['CodUsu', 'Codigo'],
      [Tipo, RazaoSocial, Fantasia, CNPJ, IE, ETe1, ECel, EEmail, ENatal,
       Nome, Apelido, CPF, RG, PTe1, PCel, PEmail, PNatal, Ativo],
      [Codigo, Codigo], True, False, '', stMobile, True);
    //
    ReopenEntidadesView;
    //
    FCodigo := Codigo;
    //
    ExecuteAction(ChangeTabAction2);
  end else
  begin
    ExecuteAction(ChangeTabAction1);
  end;
end;

procedure TFmEntidades.SbExcluiClick(Sender: TObject);
begin
  AllOS_dmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + WLAN_CTOS,
    'WHERE Fornecedor = (' + Geral.FF0(FCodigo),
    'OR Cliente=' + Geral.FF0(FCodigo) + ') ',
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
  //
  if Dmod.QrAux.RecordCount = 0 then
  begin
  AllOS_dmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM wenticonta ',
    'WHERE Codigo=' + Geral.FF0(FCodigo),
    '']);
    if Dmod.QrAux.RecordCount = 0 then
    begin
      if AllOS_dmkDB.ExcluiRegistroInt1_Sinc('Deseja excluir este registro?',
        'wentidades', 'Codigo', FCodigo, Dmod.MyDB) = mrYes then
      begin
        ReopenEntidadesView;
        ExecuteAction(ChangeTabAction1);
      end;
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Esta entidade possui contato(s) cadastrado(s) nela!');
  end else
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Esta entidade j� foi utilizada nos lan�amentos financeiros.');
  //
  Dmod.QrAux.Close;
end;

procedure TFmEntidades.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  //
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmEntidades.SBLimpaLogradClick(Sender: TObject);
begin
  CBEndELograd.ItemIndex := -1;
end;

procedure TFmEntidades.SBLimpaUFClick(Sender: TObject);
begin
  CBEndEUF.ItemIndex := -1;
end;

procedure TFmEntidades.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmEntidades.SBEndDesisteClick(Sender: TObject);
begin
  ExecuteAction(ChangeTabAction2);
end;

procedure TFmEntidades.SBEndECepClick(Sender: TObject);
begin
  ObtemEnderecoCEP(EdEndECep.Text);
end;

procedure TFmEntidades.SBEndECodiPaisClick(Sender: TObject);
begin
  MostraPesquisa(EdEndECodiPais, EdEndECodiPais.Tag, stEPais);
end;

procedure TFmEntidades.SBEndECodMuniciClick(Sender: TObject);
begin
  MostraPesquisa(EdEndECodMunici, EdEndECodMunici.Tag, stEMuni);
end;

procedure TFmEntidades.SBEndPCepClick(Sender: TObject);
begin
  ObtemEnderecoCEP(EdEndPCep.Text);
end;

procedure TFmEntidades.SBEndPCodiPaisClick(Sender: TObject);
begin
  MostraPesquisa(EdEndPCodiPais, EdEndPCodiPais.Tag, stPPais);
end;

procedure TFmEntidades.SBEndPCodMuniciClick(Sender: TObject);
begin
  MostraPesquisa(EdEndPCodMunici, EdEndPCodMunici.Tag, stPMuni);
end;

procedure TFmEntidades.SBContatoClick(Sender: TObject);
begin
  AllOS_EntidadesJan.MostraEntiContatEdit(FCodigo, 0, QrEntiContat, Dmod.MyDB);
end;

procedure TFmEntidades.SBEndConfirmaClick(Sender: TObject);
var
  Tipo, Codigo,
  ECEP, ELograd, ENumero, EUF, ECodMunici, ECodiPais,
  PCEP, PLograd, PNumero, PUF, PCodMunici, PCodiPais: Integer;
  ERua, ECompl, EBairro,
  PRua, PCompl, PBairro: String;
begin
  Codigo := FCodigo;
  Tipo   := TabControl4.TabIndex;
  //
  if Tipo = 0 then //Jur�dica
  begin
    ECEP       := Geral.IMV(Geral.SoNumero_TT(EdEndECep.Text));
    ELograd    := AllOS_dmkPF.GetSelectedValue(CBEndELograd);
    ERua       := EdEndERua.Text;
    ENumero    := Geral.IMV(Geral.SoNumero_TT(EdEndENumero.Text));
    ECompl     := EdEndECompl.Text;
    EBairro    := EdEndEBairro.Text;
    EUF        := AllOS_dmkPF.GetSelectedValue(CBEndEUF);
    ECodMunici := EdEndECodMunici.Tag;     
    ECodiPais  := EdEndECodiPais.Tag;
    //
    PCEP       := 0;
    PLograd    := 0;
    PRua       := '';
    PNumero    := 0;
    PCompl     := '';
    PBairro    := '';
    PUF        := 0;
    PCodMunici := 0;
    PCodiPais  := 0;
  end else
  begin
    PCEP       := Geral.IMV(Geral.SoNumero_TT(EdEndPCep.Text));
    PLograd    := AllOS_dmkPF.GetSelectedValue(CBEndPLograd);
    PRua       := EdEndPRua.Text;
    PNumero    := Geral.IMV(Geral.SoNumero_TT(EdEndPNumero.Text));
    PCompl     := EdEndPCompl.Text;
    PBairro    := EdEndPBairro.Text;
    PUF        := AllOS_dmkPF.GetSelectedValue(CBEndPUF);
    PCodMunici := EdEndPCodMunici.Tag;
    PCodiPais  := EdEndPCodiPais.Tag;
    //
    ECEP       := 0;
    ELograd    := 0;
    ERua       := '';
    ENumero    := 0;
    ECompl     := '';
    EBairro    := '';
    EUF        := 0;
    ECodMunici := 0;
    ECodiPais  := 0;
  end;
  if Codigo <> 0 then
  begin
    AllOS_dmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, stUpd, 'wentidades', False,
      ['Tipo', 'ECEP', 'ELograd', 'ERua', 'ENumero', 'ECompl', 'EBairro', 'EUF',
       'ECodMunici', 'ECodiPais', 'PCEP', 'PLograd', 'PRua', 'PNumero', 'PCompl',
       'PBairro', 'PUF', 'PCodMunici', 'PCodiPais'],
      ['CodUsu', 'Codigo'],
      [Tipo, ECEP, ELograd, ERua, ENumero, ECompl, EBairro, EUF,
       ECodMunici, ECodiPais, PCEP, PLograd, PRua, PNumero, PCompl,
       PBairro, PUF, PCodMunici, PCodiPais],
      [Codigo, Codigo], True, False, '', stMobile, True);
    //
    ReopenEntidadesView;
    //
    FCodigo := Codigo;
    //
    ExecuteAction(ChangeTabAction2);
  end else
  begin
    ExecuteAction(ChangeTabAction1);
  end;
end;

procedure TFmEntidades.ReopenTipoLograd(Query: TFDQuery);
begin
  AllOS_dmkDB.AbreSQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM listalograd ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmEntidades.ReopenUFs(Query: TFDQuery);
begin
  AllOS_dmkDB.AbreSQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, DTB, Nome ',
    'FROM ufs ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmEntidades.TabControl1Change(Sender: TObject);
var
  ELograd, PLograd, EUF, PUF: Integer;
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin
      //
    end;
    1: //Visualiza
    begin
      ReopenEntidades(FCodigo);
      ReopenEntiContat(FCodigo);
      //
      if QrEntidades.FieldByName('Tipo').AsInteger = 0 then
      begin
        //Jur�dica
        TabControl3.TabIndex := 0;
        TabItem6.Visible     := True;
        TabItem7.Visible     := False;
        //
        LBIECodigo.Text     := Geral.FF0(QrEntidades.FieldByName('Codigo').AsInteger);
        LBIRazaoSocial.Text := QrEntidades.FieldByName('RazaoSocial').AsString;
        LBIFantasia.Text    := QrEntidades.FieldByName('Fantasia').AsString;
        LBICNPJ.Text        := Geral.FormataCNPJ_TT(QrEntidades.FieldByName('CNPJ').AsString);
        LBIIE.Text          := QrEntidades.FieldByName('IE').AsString;
        LBIETe1.Text        := Geral.FormataTelefone_TT_Curto(QrEntidades.FieldByName('ETe1').AsString);
        LBIECel.Text        := Geral.FormataTelefone_TT_Curto(QrEntidades.FieldByName('ECel').AsString);
        LBIEEmail.Text      := QrEntidades.FieldByName('EEmail').AsString;
        LBIEENatal.Text     := Geral.FDT(QrEntidades.FieldByName('ENatal').AsDateTime, 2, True);
        //
        //Endere�o
        LBIEndCEP.Text       := Geral.FormataCEP_TT(Geral.FF0(QrEntidades.FieldByName('ECEP').AsInteger));
        LBIEndLograd.Text    := QrEntidades.FieldByName('ELograd_TXT').AsString;
        LBIEndRua.Text       := QrEntidades.FieldByName('ERua').AsString;
        LBIEndNumero.Text    := Geral.FF0(QrEntidades.FieldByName('ENumero').AsInteger);
        LBIEndCompl.Text     := QrEntidades.FieldByName('ECompl').AsString;
        LBIEndBairro.Text    := QrEntidades.FieldByName('EBairro').AsString;
        LBIEndUF.Text        := QrEntidades.FieldByName('EUF_TXT').AsString;
        LBIEndCodMunici.Text := QrEntidades.FieldByName('ECodMunici_TXT').AsString;
        LBIEndCodiPais.Text  := QrEntidades.FieldByName('ECodiPais_TXT').AsString;
      end else
      begin
        //F�sica
        TabControl3.TabIndex := 1;
        TabItem6.Visible     := False;
        TabItem7.Visible     := True;
        //
        LBIPCodigo.Text := Geral.FF0(QrEntidades.FieldByName('Codigo').AsInteger);
        LBINome.Text    := QrEntidades.FieldByName('Nome').AsString;
        LBIApelido.Text := QrEntidades.FieldByName('Apelido').AsString;
        LBICPF.Text     := Geral.FormataCNPJ_TT(QrEntidades.FieldByName('CPF').AsString);
        LBIRG.Text      := QrEntidades.FieldByName('RG').AsString;
        LBIPTe1.Text    := Geral.FormataTelefone_TT_Curto(QrEntidades.FieldByName('PTe1').AsString);
        LBIPCel.Text    := Geral.FormataTelefone_TT_Curto(QrEntidades.FieldByName('PCel').AsString);
        LBIPEmail.Text  := QrEntidades.FieldByName('PEmail').AsString;
        LBIEPNatal.Text := Geral.FDT(QrEntidades.FieldByName('PNatal').AsDateTime, 2, True);
        //
        //Endere�o
        LBIEndCEP.Text       := Geral.FormataCEP_TT(Geral.FF0(QrEntidades.FieldByName('PCEP').AsInteger));
        LBIEndLograd.Text    := QrEntidades.FieldByName('PLograd_TXT').AsString;
        LBIEndRua.Text       := QrEntidades.FieldByName('PRua').AsString;
        LBIEndNumero.Text    := Geral.FF0(QrEntidades.FieldByName('PNumero').AsInteger);
        LBIEndCompl.Text     := QrEntidades.FieldByName('PCompl').AsString;
        LBIEndBairro.Text    := QrEntidades.FieldByName('PBairro').AsString;
        LBIEndUF.Text        := QrEntidades.FieldByName('PUF_TXT').AsString;
        LBIEndCodMunici.Text := QrEntidades.FieldByName('PCodMunici_TXT').AsString;
        LBIEndCodiPais.Text  := QrEntidades.FieldByName('PCodiPais_TXT').AsString;
      end;
      if QrEntidades.FieldByName('Ativo').AsInteger = 1 then
      begin
        LBIEAtivo.Text := 'Sim';
        LBIPAtivo.Text := 'Sim';
      end else
      begin
        LBIEAtivo.Text := 'N�o';
        LBIPAtivo.Text := 'N�o';
      end;
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenEntidades(FCodigo);
        //
        EdRazaoSocial.Text := QrEntidades.FieldByName('RazaoSocial').AsString;
        EdFantasia.Text    := QrEntidades.FieldByName('Fantasia').AsString;
        EdCNPJ.Text        := Geral.SoNumero_TT(QrEntidades.FieldByName('CNPJ').AsString);
        EdIE.Text          := Geral.SoNumero_TT(QrEntidades.FieldByName('IE').AsString);
        EdETe1.Text        := Geral.SoNumero_TT(QrEntidades.FieldByName('ETe1').AsString);
        EdECel.Text        := Geral.SoNumero_TT(QrEntidades.FieldByName('ECel').AsString);
        EdEEmail.Text      := QrEntidades.FieldByName('EEmail').AsString;
        DEENatal.Date      := QrEntidades.FieldByName('ENatal').AsDateTime;
        CkEAtivo.IsChecked := Geral.IntToBool(QrEntidades.FieldByName('Ativo').AsInteger);
        //
        if QrEntidades.FieldByName('ENatal').AsDateTime = 0 then
          DEENatal.IsEmpty := True;
        //
        EdNome.Text        := QrEntidades.FieldByName('Nome').AsString;
        EdApelido.Text     := QrEntidades.FieldByName('Apelido').AsString;
        EdCPF.Text         := Geral.SoNumero_TT(QrEntidades.FieldByName('CPF').AsString);
        EdRG.Text          := Geral.SoNumero_TT(QrEntidades.FieldByName('RG').AsString);
        EdPTe1.Text        := Geral.SoNumero_TT(QrEntidades.FieldByName('PTe1').AsString);
        EdPCel.Text        := Geral.SoNumero_TT(QrEntidades.FieldByName('PCel').AsString);
        EdPEmail.Text      := QrEntidades.FieldByName('PEmail').AsString;
        DEPNatal.Date      := QrEntidades.FieldByName('PNatal').AsDateTime;
        CkPAtivo.IsChecked := Geral.IntToBool(QrEntidades.FieldByName('Ativo').AsInteger);
        //
        if QrEntidades.FieldByName('PNatal').AsDateTime = 0 then
          DEPNatal.IsEmpty := True;
        //
        if QrEntidades.FieldByName('Tipo').AsInteger = 0 then
          TabControl2.TabIndex := 0
        else
          TabControl2.TabIndex := 1;
      end else
      begin
        EdRazaoSocial.Text := '';
        EdFantasia.Text    := '';
        EdCNPJ.Text        := '';
        EdIE.Text          := '';
        EdETe1.Text        := '';
        EdECel.Text        := '';
        EdEEmail.Text      := '';
        DEENatal.IsEmpty   := True;
        CkEAtivo.IsChecked := True;
        //
        EdNome.Text        := '';
        EdApelido.Text     := '';
        EdCPF.Text         := '';
        EdRG.Text          := '';
        EdPTe1.Text        := '';
        EdPCel.Text        := '';
        EdPEmail.Text      := '';
        DEPNatal.IsEmpty   := True;
        CkPAtivo.IsChecked := True;
        //
        TabControl2.TabIndex := 0;
      end;
    end;
    3: //Edita endere�o
    begin
      if FCodigo <> 0 then
      begin
        ReopenEntidades(FCodigo);
        ReopenTipoLograd(QrTipoLogradE);
        ReopenTipoLograd(QrTipoLogradP);
        ReopenUFs(QrUFsE);
        ReopenUFs(QrUFsP);
        //
        if QrEntidades.FieldByName('Tipo').AsInteger = 0 then
        begin
          TabControl4.TabIndex  := 0;
          TBEndJuridica.Visible := True;
          TBEndFisica.Visible   := False;
          //
          ELograd := QrEntidades.FieldByName('ELograd').AsInteger;
          EUF     := QrEntidades.FieldByName('EUF').AsInteger;
          //
          EdEndECep.Text         := Geral.FF0(QrEntidades.FieldByName('ECEP').AsInteger);
          CBEndELograd.ItemIndex := AllOS_dmkDB.ObtemIndexDeTabela(QrTipoLogradE, 'Codigo', ELograd);
          EdEndERua.Text         := QrEntidades.FieldByName('ERua').AsString;
          EdEndENumero.Text      := Geral.FF0(QrEntidades.FieldByName('ENumero').AsInteger);
          EdEndECompl.Text       := QrEntidades.FieldByName('ECompl').AsString;
          EdEndEBairro.Text      := QrEntidades.FieldByName('EBairro').AsString;
          CBEndEUF.ItemIndex     := AllOS_dmkDB.ObtemIndexDeTabela(QrUFsE, 'Codigo', EUF);
          EdEndECodMunici.Text   := QrEntidades.FieldByName('ECodMunici_TXT').AsString;
          EdEndECodMunici.Tag    := QrEntidades.FieldByName('ECodMunici').AsInteger;
          EdEndECodiPais.Text    := QrEntidades.FieldByName('ECodiPais_TXT').AsString;
          EdEndECodiPais.Tag     := QrEntidades.FieldByName('ECodiPais').AsInteger;
          //
          if EdEndECodiPais.Text = '' then
          begin
            EdEndECodiPais.Text := 'BRASIL';
            EdEndECodiPais.Tag  := 1058;
          end;
        end else
        begin
          TabControl4.TabIndex  := 1;
          TBEndJuridica.Visible := False;
          TBEndFisica.Visible   := True;
          //
          PLograd := QrEntidades.FieldByName('PLograd').AsInteger;
          PUF     := QrEntidades.FieldByName('PUF').AsInteger;
          //
          EdEndPCep.Text         := Geral.FF0(QrEntidades.FieldByName('PCEP').AsInteger);
          CBEndPLograd.ItemIndex := AllOS_dmkDB.ObtemIndexDeTabela(QrTipoLogradP, 'Codigo', PLograd);
          EdEndPRua.Text         := QrEntidades.FieldByName('PRua').AsString;
          EdEndPNumero.Text      := Geral.FF0(QrEntidades.FieldByName('PNumero').AsInteger);
          EdEndPCompl.Text       := QrEntidades.FieldByName('PCompl').AsString;
          EdEndPBairro.Text      := QrEntidades.FieldByName('PBairro').AsString;
          CBEndPUF.ItemIndex     := AllOS_dmkDB.ObtemIndexDeTabela(QrUFsP, 'Codigo', PUF);
          EdEndPCodMunici.Text   := QrEntidades.FieldByName('PCodMunici_TXT').AsString;
          EdEndPCodMunici.Tag    := QrEntidades.FieldByName('PCodMunici').AsInteger;
          EdEndPCodiPais.Text    := QrEntidades.FieldByName('PCodiPais_TXT').AsString;;
          EdEndPCodiPais.Tag     := QrEntidades.FieldByName('PCodiPais').AsInteger;
          //
          if EdEndPCodiPais.Text = '' then
          begin
            EdEndPCodiPais.Text := 'BRASIL';
            EdEndPCodiPais.Tag  := 1058;
          end;
        end;
      end;
    end;
  end;
end;

//Form Scrool Teclado Virtual
procedure TFmEntidades.CalcContentBoundsProc(Sender: TObject;
  var ContentBounds: TRectF);
begin
  if VAR_NEEDOFFSET and (VAR_KBBOUNDS.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom, 2 * ClientHeight - VAR_KBBOUNDS.Top);
  end;
end;

procedure TFmEntidades.RestorePosition(ListBox: TListBox; Layout: TLayout);
begin
  ListBox.ViewportPosition := PointF(ListBox.ViewportPosition.X, 0);
  Layout.Align             := TAlignLayout.Client;
  ListBox.RealignContent;
end;

procedure TFmEntidades.UpdateKBBounds(ListBox: TListBox; Layout: TLayout);
var
  LFocused: TControl;
  LFocusRect: TRectF;
begin
  VAR_NEEDOFFSET := False;
  //
  if Assigned(Focused) then
  begin
    LFocused   := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    //
    LFocusRect.Offset(ListBox.ViewportPosition);
    //
    if (LFocusRect.IntersectsWith(TRectF.Create(VAR_KBBOUNDS))) and
       (LFocusRect.Bottom > VAR_KBBOUNDS.Top) then
    begin
      VAR_NEEDOFFSET := True;
      Layout.Align   := TAlignLayout.Horizontal;
      //
      ListBox.RealignContent;
      Application.ProcessMessages;
      //
      ListBox.ViewportPosition := PointF(ListBox.ViewportPosition.X,
                                     LFocusRect.Bottom - VAR_KBBOUNDS.Top);
    end;
  end;
  if not VAR_NEEDOFFSET then
    RestorePosition(ListBox, Layout);
end;

end.


