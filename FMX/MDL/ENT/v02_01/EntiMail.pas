unit EntiMail;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.Layouts, FMX.ListBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.DateTimeCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TFmEntiMail = class(TForm)
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbConfirma: TSpeedButton;
    SbSaida: TSpeedButton;
    ListBoxItem1: TListBoxItem;
    EdEmail: TEdit;
    ClearEditButton1: TClearEditButton;
    QrWEntiMail: TFDQuery;
    Tipo: TListBoxItem;
    CBEntiTipCto: TComboBox;
    SBEntiTipCto: TSpeedButton;
    SBLimpar: TSpeedButton;
    QrWEntiTipCt: TFDQuery;
    QrWEntiTipCtCodigo: TIntegerField;
    QrWEntiTipCtNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    SBEntiTipCtoCad: TSpeedButton;
    Label2: TLabel;
    procedure SbSaidaClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBLimparClick(Sender: TObject);
    procedure SBEntiTipCtoClick(Sender: TObject);
    procedure SBEntiTipCtoCadClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWEntiMail();
    procedure ReopenWEntiTipCt();
  public
    { Public declarations }
    FCodigo, FControle, FConta: Integer;
    FQuery: TFDQuery;
    FDataBase: TFDConnection;
  end;

var
  FmEntiMail: TFmEntiMail;

implementation

uses Module, UnDmkEnums, UnGeral, UnAllOS_DmkProcFunc, UnAllOS_DmkDB,
  UnGrl_Vars, UnAllOS_EntidadesJan;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmEntiMail.FormShow(Sender: TObject);
begin
  ReopenWEntiTipCt();
  //
  if FConta = 0 then
  begin
    EdEmail.Text           := '';
    CBEntiTipCto.ItemIndex := -1;
  end else
  begin
    ReopenWEntiMail();
    //
    EdEmail.Text           := QrWEntiMail.FieldByName('Email').AsString;
    CBEntiTipCto.ItemIndex := QrWEntiMail.FieldByName('EntiTipCto').AsInteger;
  end;
end;

procedure TFmEntiMail.ReopenWEntiMail();
begin
  AllOS_dmkDB.AbreSQLQuery0(QrWEntiMail, Dmod.MyDB, [
    'SELECT * ',
    'FROM wentimail ',
    'WHERE Codigo=' + Geral.FF0(FCodigo),
    'AND Controle=' + Geral.FF0(FControle),
    'AND Conta=' + Geral.FF0(FConta),
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiMail.ReopenWEntiTipCt;
begin
  AllOS_dmkDB.AbreSQLQuery0(QrWEntiTipCt, Dmod.MyDB, [
    'SELECT * ',
    'FROM wentitipct ',
    'WHERE Ativo=1 ',
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiMail.SbConfirmaClick(Sender: TObject);
var
  Conta, EntiTipCto: Integer;
  Email: String;
  SQLTipo: TSQLType;
begin
  Conta      := FConta;
  Email      := EdEmail.Text;
  EntiTipCto := CBEntiTipCto.Tag;
  //
  if AllOS_dmkPF.FIC(Email = '', 'Defina um e-mail!') then Exit;
  //
  if Conta = 0 then
  begin
    SQLTipo := stIns;
    Conta   := AllOS_dmkDB.ObtemCodigoInt_Sinc('wentimail', 'Conta', Dmod.QrAux, Dmod.MyDB);
  end else
    SQLTipo := stUpd;
  //
  if (FCodigo <> 0) and (FControle <> 0) and (Conta <> 0) then
  begin
    if AllOS_dmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo, 'wentimail',
      False, ['EMail', 'EntiTipCto', 'Codigo', 'Controle'], ['Conta'],
      [Email, EntiTipCto, FCodigo, FControle], [Conta], True, False, '', stMobile, True) then
    begin
      if (FQuery <> nil) and (FDataBase <> nil) then
        AllOS_dmkDB.AbreQuery(FQuery, FDataBase);
    end;
  end;
  Close;
end;

procedure TFmEntiMail.SBEntiTipCtoCadClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := AllOS_dmkPF.GetSelectedValue(CBEntiTipCto);
  //
  AllOS_EntidadesJan.MostraEntiTipCto(Codigo, QrWEntiTipCt, Dmod.MyDB);
end;

procedure TFmEntiMail.SBEntiTipCtoClick(Sender: TObject);
var
  Codigo: Integer;
  SQL: Array of String;
begin
  Codigo := AllOS_dmkPF.GetSelectedValue(CBEntiTipCto);
  SQL    := [QrWEntiTipCt.SQL.Text];
  //
  AllOS_dmkPF.CriaFm_PesqCod(CBEntiTipCto, Codigo, SQL);
end;

procedure TFmEntiMail.SBLimparClick(Sender: TObject);
begin
  CBEntiTipCto.ItemIndex := -1;
end;

procedure TFmEntiMail.SbSaidaClick(Sender: TObject);
begin
  Close;
end;

end.
