unit EntiTipCto;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.SearchBox, System.Actions, FMX.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TFmEntiTipCto = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbInclui: TSpeedButton;
    SearchBox1: TSearchBox;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    BindingsList1: TBindingsList;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    QrEntiTipCto: TFDQuery;
    ListBoxItem1: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    CkAtivo: TCheckBox;
    EdNome: TEdit;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIAtivo: TListBoxItem;
    ClearEditButton1: TClearEditButton;
    Label2: TLabel;
    QrEntiTipCtoView: TFDQuery;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    procedure FormCreate(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure ReopenEntiTipCtoView(Codigo: Integer);
    procedure ReopenEntiTipCto;
  public
    { Public declarations }
    FCodigo: Integer;
    FQuery: TFDQuery;
    FDataBase: TFDConnection;
  end;

var
  FmEntiTipCto: TFmEntiTipCto;

implementation

uses Principal, UnAllOS_DmkProcFunc, UnAllOS_DmkDB, Module, UnDmkEnums, UnGeral,
  UnGrl_Vars, MyListas;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmEntiTipCto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (FQuery <> nil) and (FDataBase <> nil) then
    AllOS_dmkDB.AbreQuery(FQuery, FDataBase);
end;

procedure TFmEntiTipCto.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenEntiTipCto;
end;

procedure TFmEntiTipCto.FormShow(Sender: TObject);
begin
  if FCodigo <> 0 then
    ListBox1.ItemIndex := AllOS_dmkDB.ObtemIndexDeTabela(QrEntiTipCto, 'Codigo', FCodigo);
end;

procedure TFmEntiTipCto.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := AllOS_dmkPF.GetSelectedValue(ListBox1);
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmEntiTipCto.ReopenEntiTipCtoView(Codigo: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntiTipCtoView, Dmod.MyDB, [
    'SELECT * ',
    'FROM wentitipct ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiTipCto.ReopenEntiTipCto;
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntiTipCto, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM wentitipct ',
    'WHERE CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
  //
  if QrEntiTipCto.RecordCount > 0 then
    SearchBox1.Visible := True
  else
    SearchBox1.Visible := False;
end;

procedure TFmEntiTipCto.SbConfirmaClick(Sender: TObject);
var
  Codigo, Ativo: Integer;
  Nome: String;
  SQLTipo: TSQLType;
begin
  Codigo := FCodigo;
  Nome   := EdNome.Text;
  Ativo  := Geral.BoolToInt(CkAtivo.IsChecked);
  //
  if AllOS_dmkPF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := AllOS_dmkDB.ObtemCodigoInt_Sinc('wentitipct', 'Codigo', Dmod.QrAux, Dmod.MyDB);
  end else
    SQLTipo := stUpd;
  //
  if Codigo <> 0 then
  begin
    AllOS_dmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB,
      SQLTipo, 'wentitipct', False,
      ['Nome', 'CodUsu', 'Ativo'], ['Codigo'],
      [Nome, Codigo, Ativo], [Codigo],
      True, False, '', stMobile, True);
  end;
  //
  ReopenEntiTipCto;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmEntiTipCto.SbExcluiClick(Sender: TObject);
begin
  AllOS_dmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Conta ',
    'FROM wentimail ',
    'WHERE EntiTipCto=' + Geral.FF0(FCodigo),
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    ' ',
    'UNION ',
    '',
    'SELECT Conta ',
    'FROM wentitel ',
    'WHERE EntiTipCto=' + Geral.FF0(FCodigo),
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
  //
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if AllOS_dmkDB.ExcluiRegistroInt1_Sinc('Deseja excluir este registro?',
      'wentitipct', 'Codigo', FCodigo, Dmod.MyDB) = mrYes then
    begin
      ReopenEntiTipCto;
      ExecuteAction(ChangeTabAction1);
    end;
  end else
    Geral.MB_Aviso('Exclus�o abortada! Este tipo j� foi utilizado nos contatos!');
  //
  Dmod.QrAux.Close;
end;

procedure TFmEntiTipCto.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmEntiTipCto.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmEntiTipCto.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenEntiTipCtoView(FCodigo);
      //
      LBICodigo.Text := Geral.FF0(QrEntiTipCtoView.FieldByName('Codigo').AsInteger);
      LBINome.Text   := QrEntiTipCtoView.FieldByName('Nome').AsString;
      //
      if QrEntiTipCtoView.FieldByName('Ativo').AsInteger = 1 then
        LBIAtivo.Text := 'Ativo'
      else
        LBIAtivo.Text := 'Inativo';
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenEntiTipCtoView(FCodigo);
        //
        EdNome.Text       := QrEntiTipCtoView.FieldByName('Nome').AsString;
        CkAtivo.IsChecked := Geral.IntToBool(QrEntiTipCtoView.FieldByName('Ativo').AsInteger);
      end else
      begin
        EdNome.Text       := '';
        CkAtivo.IsChecked := True;
      end;
    end;
  end;
end;

end.
