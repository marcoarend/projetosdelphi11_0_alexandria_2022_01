unit EntiContatEdit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.Layouts, FMX.ListBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.DateTimeCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFmEntiContatEdit = class(TForm)
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbConfirma: TSpeedButton;
    SbSaida: TSpeedButton;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    EdNome: TEdit;
    ClearEditButton1: TClearEditButton;
    DEDtaNatal: TDateEdit;
    QrWEntiConta: TFDQuery;
    Label2: TLabel;
    procedure SbSaidaClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWEntiConta();
  public
    { Public declarations }
    FCodigo, FControle: Integer;
    FQuery: TFDQuery;
    FDataBase: TFDConnection;
  end;

var
  FmEntiContatEdit: TFmEntiContatEdit;

implementation

uses Module, UnDmkEnums, UnGeral, UnAllOS_DmkProcFunc, UnAllOS_DmkDB, UnGrl_Vars;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmEntiContatEdit.FormShow(Sender: TObject);
begin
  if FControle = 0 then
  begin
    EdNome.Text        := '';
    DEDtaNatal.IsEmpty := True;
  end else
  begin
    ReopenWEntiConta();
    //
    EdNome.Text     := QrWEntiConta.FieldByName('Nome').AsString;
    DEDtaNatal.Date := QrWEntiConta.FieldByName('DtaNatal').AsDateTime;
  end;
end;

procedure TFmEntiContatEdit.ReopenWEntiConta();
begin
  AllOS_dmkDB.AbreSQLQuery0(QrWEntiConta, Dmod.MyDB, [
    'SELECT * ',
    'FROM wenticonta ',
    'WHERE Codigo=' + Geral.FF0(FCodigo),
    'AND Controle=' + Geral.FF0(FControle),
    'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiContatEdit.SbConfirmaClick(Sender: TObject);
var
  Controle: Integer;
  Nome, DtaNatal: String;
  SQLTipo: TSQLType;
begin
  Controle := FControle;
  Nome     := EdNome.Text;
  //
  if DEDtaNatal.IsEmpty = True then
    DtaNatal := '0000-00-00'
  else
    DtaNatal := Geral.FDT(DEDtaNatal.Date, 1);
  //
  if AllOS_dmkPF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  //
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := AllOS_dmkDB.ObtemCodigoInt_Sinc('wenticonta', 'Controle', Dmod.QrAux, Dmod.MyDB);
  end else
    SQLTipo := stUpd;
  //
  if (FCodigo <> 0) and (Controle <> 0) then
  begin
    if AllOS_dmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo, 'wenticonta',
      False, ['Nome', 'DtaNatal', 'Codigo'], ['Controle'],
      [Nome, DtaNatal, FCodigo], [Controle], True, False, '', stMobile, True) then
    begin
      if (FQuery <> nil) and (FDataBase <> nil) then
        AllOS_dmkDB.AbreQuery(FQuery, FDataBase);
    end;
  end;
  Close;
end;

procedure TFmEntiContatEdit.SbSaidaClick(Sender: TObject);
begin
  Close;
end;

end.
