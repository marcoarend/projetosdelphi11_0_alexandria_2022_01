unit EntiContat;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Layouts, FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.SearchBox, System.Actions, FMX.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt, FMX.Platform,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, FMX.DateTimeCtrls,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, Data.Bind.GenData, Data.Bind.ObjectScope, FMX.PhoneDialer,
  FMX.Menus, FMX.Effects;

type
  TFmEntiContat = class(TForm)
    BindingsList1: TBindingsList;
    QrEntiContat: TFDQuery;
    Label2: TLabel;
    BindSourceDB1: TBindSourceDB;
    QrEntiContatEntNome: TWideStringField;
    QrEntiContatNome: TStringField;
    QrEntiContatCodigo: TIntegerField;
    QrEntiContatControle: TIntegerField;
    QrEntiContatDtaNatal: TDateField;
    TabControl1: TTabControl;
    TITelefone: TTabItem;
    TIEmail: TTabItem;
    QrEntiMail: TFDQuery;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TStringField;
    BindSourceDB2: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    QrEntiTel: TFDQuery;
    IntegerField1: TIntegerField;
    QrEntiTelTelefone: TStringField;
    BindSourceDB3: TBindSourceDB;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    LinkFillControlToField2: TLinkFillControlToField;
    TIContato: TTabItem;
    ListBox1: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBIEntNome: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    LBIDtaNatal: TListBoxItem;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    LBTel: TListBox;
    LinkPropertyToFieldText: TLinkPropertyToField;
    LinkPropertyToFieldText2: TLinkPropertyToField;
    LinkPropertyToFieldText3: TLinkPropertyToField;
    ListBoxHeader2: TListBoxHeader;
    SBTeInclui: TSpeedButton;
    LinkFillControlToField3: TLinkFillControlToField;
    LBEmail: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SBEmaInclui: TSpeedButton;
    LinkFillControlToField4: TLinkFillControlToField;
    QrEntiTelEntiTipCto_Txt: TStringField;
    QrEntiMailEntiTipCto_Txt: TStringField;
    LBMenuTel: TListBox;
    LBITelefonar: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ShadowEffect1: TShadowEffect;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ListBoxItem6: TListBoxItem;
    LBMenuEmail: TListBox;
    LBICopiar: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    ListBoxItem5: TListBoxItem;
    ShadowEffect2: TShadowEffect;
    procedure SbSaidaClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbEditaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SBTeIncluiClick(Sender: TObject);
    procedure SBEmaIncluiClick(Sender: TObject);
    procedure LBTelItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure LBTelClick(Sender: TObject);
    procedure LBITelefonarClick(Sender: TObject);
    procedure ListBoxItem2Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure LBEmailItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure LBEmailClick(Sender: TObject);
    procedure ListBoxItem5Click(Sender: TObject);
    procedure ListBoxItem6Click(Sender: TObject);
    procedure LBMenuTelClick(Sender: TObject);
    procedure LBMenuEmailClick(Sender: TObject);
    procedure LBICopiarClick(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
  private
    { Private declarations }
    PhoneDialerService: IFMXPhoneDialerService;
    constructor Create(AOwner: TComponent); override;
    procedure ReopenEntiContato(Codigo, Controle: Integer);
    procedure ReopenEntiMail(Codigo, Controle: Integer);
    procedure ReopenEntiTel(Codigo, Controle: Integer);
  public
    { Public declarations }
    FCodigo, FControle: Integer;
    FQuery: TFDQuery;
    FDataBase: TFDConnection;
  end;

var
  FmEntiContat: TFmEntiContat;

implementation

uses Principal, UnAllOS_DmkProcFunc, UnAllOS_DmkDB, Module, UnDmkEnums, UnGeral,
  UnGrl_Vars, UnAllOS_EntidadesJan, MyListas, UnAllOS_Hardware;

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.SmXhdpiPh.fmx ANDROID}
{$R *.iPhone4in.fmx IOS}
{$R *.iPhone.fmx IOS}
{$R *.iPhone47in.fmx IOS}
{$R *.NmXhdpiPh.fmx ANDROID}

constructor TFmEntiContat.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  TPlatformServices.Current.SupportsPlatformService(IFMXPhoneDialerService, IInterface(PhoneDialerService));
end;

procedure TFmEntiContat.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (FQuery <> nil) and (FDataBase <> nil) then
    AllOS_dmkDB.AbreQuery(FQuery, FDataBase);
end;

procedure TFmEntiContat.FormCreate(Sender: TObject);
begin
  TabControl1.TabIndex := 0;
  LBMenuTel.Visible    := False;
  LBMenuEmail.Visible  := False;
  //
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  LBITelefonar.Visible := True;
  {$ELSE}
  LBITelefonar.Visible := False;
  {$ENDIF}
end;

procedure TFmEntiContat.FormShow(Sender: TObject);
begin
  if (FCodigo <> 0) and (FControle <> 0) then
  begin
    ReopenEntiContato(FCodigo, FControle);
    ReopenEntiMail(FCodigo, FControle);
    ReopenEntiTel(FCodigo, FControle);
  end;
end;

procedure TFmEntiContat.LBTelClick(Sender: TObject);
begin
  if LBMenuTel.Visible = True then
    LBMenuTel.Visible := False;
end;

procedure TFmEntiContat.LBTelItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  AllOS_dmkPF.Configura_PopUpDeBotao_AllOS(LBMenuTel, Sender, Item);
end;

procedure TFmEntiContat.LBEmailClick(Sender: TObject);
begin
  if LBMenuEmail.Visible = True then
    LBMenuEmail.Visible := False;
end;

procedure TFmEntiContat.LBEmailItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  AllOS_dmkPF.Configura_PopUpDeBotao_AllOS(LBMenuEmail, Sender, Item);
end;

procedure TFmEntiContat.LBICopiarClick(Sender: TObject);
var
  Email: String;
begin
  Email := AllOS_dmkPF.GetSelectedText(LBEmail);
  //
  AllOS_Hardware.Clipboard_CopiarTexto(Email);
end;

procedure TFmEntiContat.LBITelefonarClick(Sender: TObject);
var
  Telefone: String;
begin
  Telefone := AllOS_dmkPF.GetSelectedText(LBTel);
  //
  if Telefone <> '' then
    AllOS_Hardware.Telefone_FazerLigacao(Telefone, PhoneDialerService, True);
end;

procedure TFmEntiContat.LBMenuEmailClick(Sender: TObject);
begin
  LBMenuEmail.Visible := False;
end;

procedure TFmEntiContat.LBMenuTelClick(Sender: TObject);
begin
  LBMenuTel.Visible := False;
end;

procedure TFmEntiContat.ListBoxItem2Click(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := AllOS_dmkPF.GetSelectedValue(LBTel);
  //
  AllOS_EntidadesJan.MostraEntiTel(FCodigo, FControle, Conta, QrEntiTel, Dmod.MyDB);
end;

procedure TFmEntiContat.ListBoxItem4Click(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := AllOS_dmkPF.GetSelectedValue(LBEmail);
  //
  AllOS_EntidadesJan.MostraEntiMail(FCodigo, FControle, Conta, QrEntiMail, Dmod.MyDB);
end;

procedure TFmEntiContat.ListBoxItem5Click(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := AllOS_dmkPF.GetSelectedValue(LBEmail);
  //
  if Conta <> 0 then
  begin
    if AllOS_dmkDB.ExcluiRegistroInt1_Sinc('Deseja excluir este registro?',
      'wentimail', 'Conta', Conta, Dmod.MyDB) = mrYes then
    begin
      AllOS_dmkDB.AbreQuery(QrEntiMail, Dmod.MyDB);
    end;
  end;
end;

procedure TFmEntiContat.ListBoxItem6Click(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := AllOS_dmkPF.GetSelectedValue(LBTel);
  //
  if Conta <> 0 then
  begin
    if AllOS_dmkDB.ExcluiRegistroInt1_Sinc('Deseja excluir este registro?',
      'wentitel', 'Conta', Conta, Dmod.MyDB) = mrYes then
    begin
      AllOS_dmkDB.AbreQuery(QrEntiTel, Dmod.MyDB);
    end;
  end;
end;

procedure TFmEntiContat.ReopenEntiContato(Codigo, Controle: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntiContat, Dmod.MyDB, [
    'SELECT wec.Codigo, wec.Controle, wec.Nome, wec.DtaNatal, ',
    'CASE WHEN ent.Tipo = 0 THEN ent.RazaoSocial ELSE ent.Nome END EntNome ',
    'FROM wenticonta wec ',
    'LEFT JOIN wentidades ent ON ent.Codigo=wec.Codigo ',
    'WHERE wec.Codigo=' + Geral.FF0(Codigo),
    'AND wec.Controle=' + Geral.FF0(Controle),
    'AND wec.CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND wec.LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiContat.ReopenEntiMail(Codigo, Controle: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntiMail, Dmod.MyDB, [
    'SELECT wem.Conta, wem.EMail, wet.Nome EntiTipCto_Txt ',
    'FROM wentimail wem ',
    'LEFT JOIN wentitipct wet ON wet.Codigo = wem.EntiTipCto ',
    'WHERE wem.Codigo=' + Geral.FF0(Codigo),
    'AND wem.Controle=' + Geral.FF0(Controle),
    'AND wem.CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND wem.LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiContat.ReopenEntiTel(Codigo, Controle: Integer);
begin
  AllOS_dmkDB.AbreSQLQuery0(QrEntiTel, Dmod.MyDB, [
    'SELECT wet.Conta, wet.Telefone, wec.Nome EntiTipCto_Txt ',
    'FROM wentitel wet ',
    'LEFT JOIN wentitipct wec ON wec.Codigo = wet.EntiTipCto ',
    'WHERE wet.Codigo=' + Geral.FF0(Codigo),
    'AND wet.Controle=' + Geral.FF0(Controle),
    'AND wet.CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
    'AND wet.LastAcao<>' + Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmEntiContat.SbEditaClick(Sender: TObject);
begin
  AllOS_EntidadesJan.MostraEntiContatEdit(FCodigo, FControle,
    QrEntiContat, Dmod.MyDB);
end;

procedure TFmEntiContat.SBEmaIncluiClick(Sender: TObject);
begin
  AllOS_EntidadesJan.MostraEntiMail(FCodigo, FControle, 0, QrEntiMail, Dmod.MyDB);
end;

procedure TFmEntiContat.SbExcluiClick(Sender: TObject);
begin
  if FControle <> 0 then
  begin
    AllOS_dmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Conta ',
      'FROM wentimail ',
      'WHERE Controle=' + Geral.FF0(FControle),
      'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
      'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
      ' ',
      'UNION ',
      '',
      'SELECT Conta ',
      'FROM wentitel ',
      'WHERE Controle=' + Geral.FF0(FControle),
      'AND CliIntSync=' + Geral.FF0(VAR_WEB_USER_USRENT),
      'AND LastAcao<>' + Geral.FF0(Integer(laDel)),
      '']);
    //
    if Dmod.QrAux.RecordCount = 0 then
    begin
      if AllOS_dmkDB.ExcluiRegistroInt1_Sinc('Deseja excluir este registro?',
        'wenticonta', 'Controle', FControle, Dmod.MyDB) = mrYes then
      begin
        if (FQuery <> nil) and (FDataBase <> nil) then
          AllOS_dmkDB.AbreQuery(FQuery, FDataBase);
        Close;
      end;
    end else
      Geral.MB_Aviso('Exclus�o abortada! Este contato possui telefone(s) e ou e-mail(s) cadastrado(s)!');
  end;
end;

procedure TFmEntiContat.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FControle;
  Close;
end;

procedure TFmEntiContat.SBTeIncluiClick(Sender: TObject);
begin
  AllOS_EntidadesJan.MostraEntiTel(FCodigo, FControle, 0, QrEntiTel, Dmod.MyDB);
end;

procedure TFmEntiContat.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := FControle;
  Close;
end;

procedure TFmEntiContat.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := FControle;
  Close;
end;

end.
