unit UnAllOS_EntidadesJan;

interface

uses System.SysUtils, System.Types, FireDAC.Comp.Client, UnDmkEnums;

type
  TUnFMX_EntidadesJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraEntidades;
    procedure MostraEntiTipCto(Codigo: Integer = 0;
              Query: TFDQuery = nil; DataBase: TFDConnection = nil);
    procedure MostraEntiTel(Codigo, Controle, Conta: Integer;
              Query: TFDQuery = nil; DataBase: TFDConnection = nil);
    procedure MostraEntiMail(Codigo, Controle, Conta: Integer;
              Query: TFDQuery = nil; DataBase: TFDConnection = nil);
    procedure MostraEntiContat(Codigo, Controle: Integer;
              Query: TFDQuery = nil; DataBase: TFDConnection = nil);
    procedure MostraEntiContatEdit(Codigo, Controle: Integer;
              Query: TFDQuery = nil; DataBase: TFDConnection = nil);
  end;
var
  FMX_EntidadesJan: TUnFMX_EntidadesJan;

implementation

uses UnAllOS_DmkDB, UnGeral, UnAllOS_DmkProcFunc, Entidades, EntiContat,
  EntiContatEdit, EntiTipCto, EntiTel, EntiMail;

{ TUnFMX_Entidades }

procedure TUnFMX_EntidadesJan.MostraEntiContat(Codigo, Controle: Integer;
  Query: TFDQuery = nil; DataBase: TFDConnection = nil);
begin
  if AllOS_dmkPF.CriaFm_AllOS2(TFmEntiContat, FmEntiContat) = True then
  begin
    FmEntiContat.FCodigo   := Codigo;
    FmEntiContat.FControle := Controle;
    FmEntiContat.FQuery    := Query;
    FmEntiContat.FDataBase := DataBase;
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmEntiContat.Show;
    {$ELSE}
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
    {$ENDIF}
  end;
end;

procedure TUnFMX_EntidadesJan.MostraEntiContatEdit(Codigo, Controle: Integer;
  Query: TFDQuery = nil; DataBase: TFDConnection = nil);
begin
  if AllOS_dmkPF.CriaFm_AllOS2(TFmEntiContatEdit, FmEntiContatEdit) = True then
  begin
    FmEntiContatEdit.FCodigo   := Codigo;
    FmEntiContatEdit.FControle := Controle;
    FmEntiContatEdit.FQuery    := Query;
    FmEntiContatEdit.FDataBase := DataBase;
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmEntiContatEdit.Show;
    {$ELSE}
    FmEntiContatEdit.ShowModal;
    FmEntiContatEdit.Destroy;
    {$ENDIF}
  end;
end;

procedure TUnFMX_EntidadesJan.MostraEntidades;
begin
  AllOS_dmkPF.CriaFm_AllOS(TFmEntidades, FmEntidades);
end;

procedure TUnFMX_EntidadesJan.MostraEntiMail(Codigo, Controle, Conta: Integer;
  Query: TFDQuery; DataBase: TFDConnection);
begin
  if AllOS_dmkPF.CriaFm_AllOS2(TFmEntiMail, FmEntiMail) = True then
  begin
    FmEntiMail.FCodigo   := Codigo;
    FmEntiMail.FControle := Controle;
    FmEntiMail.FConta    := Conta;
    FmEntiMail.FQuery    := Query;
    FmEntiMail.FDataBase := DataBase;
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmEntiMail.Show;
    {$ELSE}
    FmEntiMail.ShowModal;
    FmEntiMail.Destroy;
    {$ENDIF}
  end;
end;

procedure TUnFMX_EntidadesJan.MostraEntiTel(Codigo, Controle, Conta: Integer;
  Query: TFDQuery = nil; DataBase: TFDConnection = nil);
begin
  if AllOS_dmkPF.CriaFm_AllOS2(TFmEntiTel, FmEntiTel) = True then
  begin
    FmEntiTel.FCodigo   := Codigo;
    FmEntiTel.FControle := Controle;
    FmEntiTel.FConta    := Conta;
    FmEntiTel.FQuery    := Query;
    FmEntiTel.FDataBase := DataBase;
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmEntiTel.Show;
    {$ELSE}
    FmEntiTel.ShowModal;
    FmEntiTel.Destroy;
    {$ENDIF}
  end;
end;

procedure TUnFMX_EntidadesJan.MostraEntiTipCto(Codigo: Integer = 0;
  Query: TFDQuery = nil; DataBase: TFDConnection = nil);
begin
  if AllOS_dmkPF.CriaFm_AllOS2(TFmEntiTipCto, FmEntiTipCto) = True then
  begin
    FmEntiTipCto.FCodigo   := Codigo;
    FmEntiTipCto.FQuery    := Query;
    FmEntiTipCto.FDataBase := DataBase;
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmEntiTipCto.Show;
    {$ELSE}
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
    {$ENDIF}
  end;
end;

end.
