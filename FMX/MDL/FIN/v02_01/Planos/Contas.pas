unit Contas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Actions, System.Rtti, System.Bindings.Outputs, FMX.Types, FMX.Controls,
  FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl, FMX.Layouts, FMX.ListBox,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, FMX.SearchBox, FMX.ActnList,
  Fmx.Bind.Editors, Fmx.Bind.DBEngExt, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Data.DB, Data.Bind.EngExt, Data.Bind.Components,
  Data.Bind.DBScope, UnGrl_Vars;

type
  TFmContas = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbSaida: TSpeedButton;
    SbInclui: TSpeedButton;
    SearchBox1: TSearchBox;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    TbContas: TFDTable;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    QrContas: TFDQuery;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    CkDebito: TCheckBox;
    ListBoxItem3: TListBoxItem;
    CkCredito: TCheckBox;
    ListBoxItem4: TListBoxItem;
    CkAtivo: TCheckBox;
    EdNome: TEdit;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIDebito: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICredito: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIAtivo: TListBoxItem;
    ClearEditButton1: TClearEditButton;
    Label2: TLabel;
    TbContasCodigo: TIntegerField;
    TbContasNome: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  private
    { Private declarations }
    procedure ReopenContas(Codigo: Integer);
    procedure ReopenTbContas;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmContas: TFmContas;

implementation

uses Principal, UnFMX_DmkProcFunc, UnGrl_DmkDB, Module, UnDmkEnums, UnFMX_Geral,
  UnFMX_Grl_Vars, MyListas;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFmContas.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenTbContas;
end;

procedure TFmContas.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := FMX_dmkPF.GetSelectedValue(ListBox1);
    //
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmContas.ReopenContas(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrContas, Dmod.MyDB, [
    'SELECT * ',
    'FROM wcontas ',
    'WHERE Codigo=' + FMX_Geral.FF0(Codigo),
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmContas.ReopenTbContas;
begin
  TbContas.Close;
  TbContas.Connection := Dmod.MyDB;
  TbContas.Filtered   := True;
  TbContas.Filter     := 'CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT) +
                         ' AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel));
  TbContas.TableName  := 'wcontas';
  TbContas.Open();
  //
  if TbContas.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmContas.SbConfirmaClick(Sender: TObject);
var
  Codigo, Debito, Credito, Ativo: Integer;
  Nome: String;
  SQLTipo: TSQLType;
begin
  Codigo  := FCodigo;
  Nome    := EdNome.Text;
  Credito := FMX_Geral.BoolToInt(CkCredito.IsChecked);
  Debito  := FMX_Geral.BoolToInt(CkDebito.IsChecked);
  Ativo   := FMX_Geral.BoolToInt(CkAtivo.IsChecked);
  //
  if FMX_dmkPF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  //
  if ((Credito = 1) and (Debito = 1)) or ((Credito = 0) and (Debito = 0)) then
  begin
    FMX_Geral.MB_Aviso('Defina se a conta � cr�dito ou d�bito!');
    Exit;
  end;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := Grl_DmkDB.ObtemCodigoInt_Sinc('wcontas', 'Codigo', Dmod.QrAux,
                 Dmod.MyDB, stIns, stMobile);
  end else
    SQLTipo := stUpd;
  //
  if Codigo <> 0 then
  begin
    Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo,
      'wcontas', False, ['Nome', 'Debito', 'Credito', 'Ativo'], ['Codigo'],
      [Nome, Debito, Credito, Ativo], [Codigo], True, False, '', stMobile, True);
  end;
  //
  ReopenTbContas;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmContas.SbExcluiClick(Sender: TObject);
begin
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + WLAN_CTOS,
    'WHERE Genero=' + FMX_Geral.FF0(FCodigo),
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
  //
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite('Deseja excluir este registro?',
      'wcontas', 'Codigo', FCodigo, Dmod.MyDB) = mrYes then
    begin
      ReopenTbContas;
      ExecuteAction(ChangeTabAction1);
    end;
  end else
    FMX_Geral.MB_Aviso('Exclus�o abortada! Esta conta j� foi utilizada nos lan�amentos financeiros.');
  //
  Dmod.QrAux.Close;
end;

procedure TFmContas.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmContas.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmContas.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenContas(FCodigo);
      //
      LBICodigo.Text := FMX_Geral.FF0(QrContas.FieldByName('Codigo').AsInteger);
      LBINome.Text   := QrContas.FieldByName('Nome').AsString;
      //
      if QrContas.FieldByName('Debito').AsInteger = 1 then
        LBIDebito.Text := 'Sim'
      else
        LBIDebito.Text := 'N�o';
      //
      if QrContas.FieldByName('Credito').AsInteger = 1 then
        LBICredito.Text := 'Sim'
      else
        LBICredito.Text := 'N�o';
      //
      //
      if QrContas.FieldByName('Ativo').AsInteger = 1 then
        LBIAtivo.Text := 'Ativo'
      else
        LBIAtivo.Text := 'Inativo';
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        ReopenContas(FCodigo);
        //
        EdNome.Text         := QrContas.FieldByName('Nome').AsString;
        CkDebito.IsChecked  := FMX_Geral.IntToBool(QrContas.FieldByName('Debito').AsInteger);
        CkCredito.IsChecked := FMX_Geral.IntToBool(QrContas.FieldByName('Credito').AsInteger);
        CkAtivo.IsChecked   := FMX_Geral.IntToBool(QrContas.FieldByName('Ativo').AsInteger);
      end else
      begin
        EdNome.Text         := '';
        CkDebito.IsChecked  := False;
        CkCredito.IsChecked := False;
        CkAtivo.IsChecked   := True;
      end;
    end;
  end;
end;

end.


