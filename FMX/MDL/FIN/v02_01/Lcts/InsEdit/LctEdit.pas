unit LctEdit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Math,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.StdCtrls, FMX.ListBox, FMX.Edit, FMX.DateTimeCtrls, FMX.Layouts,
  FireDAC.Comp.Client, FMX.Controls.Presentation, UnDmkEnums, UnGrl_Vars;

type
  TFmLctEdit = class(TForm)
    Layout1: TLayout;
    ListBox3: TListBox;
    ListBoxItem1: TListBoxItem;
    SBCarteiras: TSpeedButton;
    EdCarteira: TEdit;
    SBCCarteiras: TSpeedButton;
    ListBoxItem3: TListBoxItem;
    TPData: TDateEdit;
    ListBoxItem2: TListBoxItem;
    SBContas: TSpeedButton;
    SBCConta: TSpeedButton;
    EdConta: TEdit;
    ListBoxItem4: TListBoxItem;
    SBEntidade: TSpeedButton;
    SBCEntidade: TSpeedButton;
    EdEntidade: TEdit;
    ListBoxItem6: TListBoxItem;
    EdValor: TEdit;
    ClearEditButton2: TClearEditButton;
    ListBoxItem8: TListBoxItem;
    EdDescri: TEdit;
    ClearEditButton1: TClearEditButton;
    ListBoxItem10: TListBoxItem;
    TPVencto: TDateEdit;
    ListBoxItem9: TListBoxItem;
    EdNotaFiscal: TEdit;
    ClearEditButton3: TClearEditButton;
    ListBoxItem5: TListBoxItem;
    EdSerieNF: TEdit;
    ClearEditButton4: TClearEditButton;
    ListBoxItem7: TListBoxItem;
    EdQtde: TEdit;
    ClearEditButton5: TClearEditButton;
    ListBoxHeader1: TListBoxHeader;
    ListBoxHeader2: TListBoxHeader;
    SbConfirma: TSpeedButton;
    SBQrCode: TSpeedButton;
    SbSaida: TSpeedButton;
    Label2: TLabel;
    SBJanCarteiras: TSpeedButton;
    SBJanContas: TSpeedButton;
    procedure SBCCarteirasClick(Sender: TObject);
    procedure SBCarteirasClick(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure SBCContaClick(Sender: TObject);
    procedure SBContasClick(Sender: TObject);
    procedure SBCEntidadeClick(Sender: TObject);
    procedure SBEntidadeClick(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormFocusChanged(Sender: TObject);
    procedure SBQrCodeClick(Sender: TObject);
    procedure SBJanCarteirasClick(Sender: TObject);
    procedure SBJanContasClick(Sender: TObject);
  private
    { Private declarations }
    FKBBounds: TRectF;
    FNeedOffset: Boolean;
    FEdita: Boolean;
    procedure MostraEdicao(Codigo: Integer);
    //Form Scroll
    procedure CalcContentBoundsProc(Sender: TObject; var ContentBounds: TRectF);
    procedure UpdateKBBounds;
    procedure RestorePosition;
  public
    { Public declarations }
    FCodigo, FCarteira, FConta, FEntidade, FNotaFiscal, FIDQrCode: Integer;
    FCarteiraTXT, FContaTXT, FEntidadeTXT, FDescri, FQrCode, FSerieNF: String;
    FData, FVencto: TDate;
    FValor, FQtde: Double;
    FQueryLanctos, FQueryLanctosDB: TFDQuery;
  end;

var
  FmLctEdit: TFmLctEdit;

implementation

uses UnFMX_FinanceiroJan, UnFMX_DmkProcFunc, UnFMX_Geral, UnFMX_Grl_Vars,
  UnGrl_DmkDB, Module, MyListas, UnFMX_Financeiro, UnFMX_DmkForms;

{$R *.fmx}

procedure TFmLctEdit.EdContaChange(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := EdConta.Tag;
  //
  if (Conta <> 0) and (FCodigo = 0) then
    EdDescri.Text := EdConta.Text;
end;

procedure TFmLctEdit.FormCreate(Sender: TObject);
begin
  ListBox3.OnCalcContentBounds := CalcContentBoundsProc;
  //
  FEdita          := False;
  FCodigo         := 0;
  FCarteira       := 0;
  FConta          := 0;
  FEntidade       := 0;
  FNotaFiscal     := 0;
  FCarteiraTXT    := '';
  FContaTXT       := '';
  FEntidadeTXT    := '';
  FDescri         := '';
  FQrCode         := '';
  FIDQrCode       := 0;
  FSerieNF        := '';
  FData           := Date;
  FVencto         := Date;
  FValor          := 0;
  FQtde           := 0;
  FQueryLanctos   := nil;
  FQueryLanctosDB := nil;
end;

procedure TFmLctEdit.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds;
end;

procedure TFmLctEdit.FormShow(Sender: TObject);
begin
  MostraEdicao(FCodigo);
end;

procedure TFmLctEdit.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds.Create(0, 0, 0, 0);
  FNeedOffset := False;
  RestorePosition;
end;

procedure TFmLctEdit.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds := TRectF.Create(Bounds);
  FKBBounds.TopLeft := ScreenToClient(FKBBounds.TopLeft);
  FKBBounds.BottomRight := ScreenToClient(FKBBounds.BottomRight);
  UpdateKBBounds;
end;

procedure TFmLctEdit.MostraEdicao(Codigo: Integer);
var
  Carteira, Genero, Entidade: Integer;
begin
  SBQrCode.Visible   := (FQrCode <> '') and (FIDQrCode <> 0);
  SBEntidade.Visible := False;
  //
  EdCarteira.ReadOnly := True;
  EdConta.ReadOnly    := True;
  EdEntidade.ReadOnly := True;
  //
  if Codigo <> 0 then
  begin
    ListBoxItem1.Visible := False;
    EdCarteira.Visible   := False;
    SBCCarteiras.Visible := False;
    SBCarteiras.Visible  := False;
    ListBoxItem3.Visible := False;
    TPData.Visible       := False;
    //
    Carteira := FQueryLanctosDB.FieldByName('Carteira').AsInteger;
    Genero   := FQueryLanctosDB.FieldByName('Genero').AsInteger;
    //
    EdCarteira.Text   := FQueryLanctosDB.FieldByName('CarteiraTXT').AsString;
    EdCarteira.Tag    := Carteira;
    EdConta.Text      := FQueryLanctosDB.FieldByName('ContaTXT').AsString;
    EdConta.Tag       := Genero;
    TPData.Date       := FQueryLanctosDB.FieldByName('Data').AsDateTime;
    EdDescri.Text     := FQueryLanctosDB.FieldByName('Descricao').AsString;
    TPVencto.Date     := FQueryLanctosDB.FieldByName('Vencimento').AsDateTime;
    EdNotaFiscal.Text := FMX_Geral.FF0(FQueryLanctosDB.FieldByName('NotaFiscal').AsInteger);
    EdSerieNF.Text    := FQueryLanctosDB.FieldByName('SerieNF').AsString;
    EdQtde.Text       := FMX_Geral.FFT(FQueryLanctosDB.FieldByName('Qtde').AsFloat, 2, siPositivo);
    //
    if FQueryLanctosDB.FieldByName('ContaCred').AsInteger = 1 then
    begin
      Entidade := FQueryLanctosDB.FieldByName('Cliente').AsInteger;
      //
      EdEntidade.Tag  := Entidade;
      EdEntidade.Text := FQueryLanctosDB.FieldByName('ClienteTXT').AsString;
      EdValor.Text    := FloatToStrF(FQueryLanctosDB.FieldByName('Credito').AsFloat, ffNumber, 8, 2);
    end else
    begin
      Entidade := FQueryLanctosDB.FieldByName('Fornecedor').AsInteger;

      EdEntidade.Tag  := Entidade;
      EdEntidade.Text := FQueryLanctosDB.FieldByName('FornecedorTXT').AsString;
      EdValor.Text    := FloatToStrF(FQueryLanctosDB.FieldByName('Debito').AsFloat, ffNumber, 8, 2);
    end;
  end else
  begin
    ListBoxItem1.Visible := True;
    EdCarteira.Visible   := True;
    SBCCarteiras.Visible := True;
    SBCarteiras.Visible  := True;
    ListBoxItem3.Visible := True;
    TPData.Visible       := True;
    //
    EdCarteira.Tag    := FCarteira;
    EdCarteira.Text   := FCarteiraTXT;
    EdConta.Tag       := FConta;
    EdConta.Text      := FContaTXT;
    EdEntidade.Tag    := FEntidade;
    EdEntidade.Text   := FEntidadeTXT;
    TPData.Date       := FData;
    EdValor.Text      := FloatToStrF(FValor, ffNumber, 8, 2);
    EdDescri.Text     := FDescri;
    TPVencto.Date     := FVencto;
    EdNotaFiscal.Text := FMX_Geral.FF0(FNotaFiscal);
    EdSerieNF.Text    := FSerieNF;
    EdQtde.Text       := FMX_Geral.FFT(FQtde, 2, siPositivo);
    //
    FEdita := True;
  end;
end;

procedure TFmLctEdit.SBCarteirasClick(Sender: TObject);
var
  Codigo: Integer;
  SQL: Array of String;
begin
  Codigo := EdCarteira.Tag;
  SQL    := [
            'SELECT Codigo, Nome, PagRec ',
            'FROM wcarteiras ',
            'WHERE Ativo = 1 ',
            'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
            'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
            'ORDER BY Nome '
            ];
  //
  FMX_dmkForms.CriaFm_PesqCod(EdCarteira, Codigo, SQL);
end;

procedure TFmLctEdit.SBCCarteirasClick(Sender: TObject);
begin
  EdCarteira.Tag  := 0;
  EdCarteira.Text := '';
end;

procedure TFmLctEdit.SBCContaClick(Sender: TObject);
begin
  EdConta.Tag  := 0;
  EdConta.Text := '';
end;

procedure TFmLctEdit.SBCEntidadeClick(Sender: TObject);
begin
  EdEntidade.Tag  := 0;
  EdEntidade.Text := '';
end;

procedure TFmLctEdit.SbConfirmaClick(Sender: TObject);
var
  Controle, Carteira, Conta, Entidade, Fornece, Cliente, NotaFiscal, CliInt: Integer;
  Data, Vencimento, Descricao, SerieNF: String;
  Debito, Credito, Valor, Qtde: Double;
  ContaCred: Boolean;
  SQLTipo: TSQLType;
begin
  if TPData.IsEmpty = True then
    Data := '0000-00-00'
  else
    Data := FMX_Geral.FDT(TPData.Date, 1);
  //
  Controle   := FCodigo;
  Carteira   := EdCarteira.Tag;
  Conta      := EdConta.Tag;
  Entidade   := EdEntidade.Tag;
  Valor      := FMX_Geral.DMV(EdValor.Text);
  Descricao  := EdDescri.Text;
  NotaFiscal := FMX_Geral.IMV(EdNotaFiscal.Text);
  SerieNF    := EdSerieNF.Text;
  Qtde       := FMX_Geral.DMV(EdQtde.Text);
  Data       := FMX_Geral.FDT(TPData.Date, 1);
  Vencimento := FMX_Geral.FDT(TPData.Date, 1);
  CliInt     := VAR_WEB_USR_ENT;
  //
  if FMX_dmkPF.FIC(Carteira = 0, 'Carteira n�o definida!') then Exit;
  if FMX_dmkPF.FIC(Conta = 0, 'Conta n�o definida!') then Exit;
  if FMX_dmkPF.FIC(Valor = 0, 'Valor n�o definido!') then Exit;
  if FMX_dmkPF.FIC(Descricao = '', 'Descri��o n�o definida!') then Exit;
  if FMX_dmkPF.FIC(CliInt = 0, 'Usu�rio n�o localizado!') then Exit;
  //
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT CASE WHEN Credito = 1 THEN 1 ELSE 0 END ContaCred ',
    'FROM wcontas ',
    'WHERE Codigo=' + FMX_Geral.ff0(Conta),
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
  if Dmod.QrAux.FieldByName('ContaCred').AsInteger = 1 then
    ContaCred := True
  else
    ContaCred := False;
  //
  Dmod.QrAux.Close;
  //
  if ContaCred = True then
  begin
    Cliente := Entidade;
    Credito := Valor;
    Fornece := 0;
    Debito  := 0;
  end else
  begin
    Fornece := Entidade;
    Debito  := Valor;
    Cliente := 0;
    Credito := 0;
  end;
  if Controle = 0 then
  begin
    SQLTipo  := stIns;
    Controle := Grl_DmkDB.ObtemCodigoInt_Sinc(WLAN_CTOS, 'Controle', Dmod.QrAux,
                  Dmod.MyDB, stIns, stMobile);
  end else
    SQLTipo := stUpd;
  //
  if Controle <> 0 then
  begin
    if Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo, WLAN_CTOS, False,
      ['Carteira', 'Data', 'Genero', 'Cliente', 'Fornecedor', 'Credito',
      'Debito', 'Descricao', 'NotaFiscal', 'SerieNF', 'Vencimento',
      'Qtde', 'CliInt'], ['Controle'],
      [Carteira, Data, Conta, Cliente, Fornece, Credito,
      Debito, Descricao, NotaFiscal, SerieNF, Vencimento,
      Qtde, CliInt], [Controle], True, False, '',
      stMobile, True) then
    begin
      if (FIDQrCode <> 0) then
      begin
        Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, stUpd, 'wlctqrcode', False,
          ['Lancto'], ['Codigo'], [Controle], [FIDQrCode], True, False, '',
          stMobile, True);
      end;
    end;
    //
    if FQueryLanctos <> nil then
      Grl_DmkDB.AbreQuery(FQueryLanctos, Dmod.MyDB);
    if FQueryLanctosDB <> nil then
      Grl_DmkDB.AbreQuery(FQueryLanctosDB, Dmod.MyDB);
    //
    Close;
  end else
    FMX_Geral.MB_Erro('Erro: ID n�o definido!');
end;

procedure TFmLctEdit.SBContasClick(Sender: TObject);
var
  Codigo, Carteira, PagRec: Integer;
  SQL: Array of String;
  SQLCompl: String;
begin
  Carteira := EdCarteira.Tag;
  //
  if Carteira <> 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT PagRec ',
      'FROM wcarteiras ',
      'WHERE Codigo = ' + FMX_Geral.FF0(Carteira),
      'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
      'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
      '']);

    PagRec := Dmod.QrAux.FieldByName('PagRec').AsInteger;

    Dmod.QrAux.Close;
  end else
    PagRec := -1;

  case PagRec of
    -1: //Pagar
      SQLCompl := 'AND Debito = 1 ';
    0: //Ambos
      SQLCompl := 'AND (Debito = 1 OR Credito = 1) ';
    1: //Receber
      SQLCompl := 'AND Credito = 1 ';
    else
      SQLCompl := '';
  end;

  Codigo := EdConta.Tag;
  SQL    := [
            'SELECT Codigo, Nome ',
            'FROM wcontas ',
            'WHERE Ativo = 1 ',
            'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
            'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
            SQLCompl,
            'ORDER BY Nome '
            ];
  //
  FMX_dmkForms.CriaFm_PesqCod(EdConta, Codigo, SQL);
end;

procedure TFmLctEdit.SBEntidadeClick(Sender: TObject);
(*
var
  Codigo: Integer;
  SQL: Array of String;
*)
begin
  (*
  Codigo := EdEntidade.Tag;
  SQL    := [
            'SELECT Codigo, CASE WHEN Tipo = 0 THEN RazaoSocial ELSE Nome END Nome ',
            'FROM wentidades ',
            'WHERE Ativo = 1 ',
            'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USER_USRENT),
            'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
            'ORDER BY Nome ',
            ''];
  //
  FMX_dmkPF.CriaFm_PesqCod(EdEntidade, Codigo, SQL);
  *)
end;

procedure TFmLctEdit.SBJanCarteirasClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraCarteiras;
end;

procedure TFmLctEdit.SBJanContasClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraContas;
end;

procedure TFmLctEdit.SBQrCodeClick(Sender: TObject);
begin
  if FQrCode <> '' then
    FMX_Financeiro.AbrirNFCe(FQrCode);
end;

procedure TFmLctEdit.SbSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctEdit.TPDataChange(Sender: TObject);
begin
  if (FCodigo = 0) and (FEdita = True) then
  begin
    if FMX_Geral.MB_Pergunta('Deseja alterar tamb�m o vencimento?') = mrYes then
      TPVencto.Date := TPData.Date;
  end;
end;

//Form Scroll - In�cio
procedure TFmLctEdit.CalcContentBoundsProc(Sender: TObject;
  var ContentBounds: TRectF);
begin
  if FNeedOffset and (FKBBounds.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom, 2 * ClientHeight - FKBBounds.Top);
  end;
end;

procedure TFmLctEdit.UpdateKBBounds;
var
  LFocused : TControl;
  LFocusRect: TRectF;
begin
  FNeedOffset := False;
  if Assigned(Focused) then
  begin
    LFocused := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    LFocusRect.Offset(ListBox3.ViewportPosition);
    if (LFocusRect.IntersectsWith(TRectF.Create(FKBBounds))) and
       (LFocusRect.Bottom > FKBBounds.Top) then
    begin
      FNeedOffset := True;
      Layout1.Align := TAlignLayout.Horizontal;
      ListBox3.RealignContent;
      Application.ProcessMessages;
      ListBox3.ViewportPosition := PointF(ListBox3.ViewportPosition.X,
                                     LFocusRect.Bottom - FKBBounds.Top);
    end;
  end;
  if not FNeedOffset then
    RestorePosition;
end;

procedure TFmLctEdit.RestorePosition;
begin
  ListBox3.ViewportPosition := PointF(ListBox3.ViewportPosition.X, 0);
  Layout1.Align := TAlignLayout.Client;
  ListBox3.RealignContent;
end;
//Form Scroll - Fim

end.
