unit LctGer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Rtti, System.Bindings.Outputs, System.Actions, FMX.Types, FMX.Controls,
  FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Ani, FMX.Controls.Presentation,
  FMX.Edit, FMX.ComboEdit, FMX.ListBox, FMX.StdCtrls, FMX.ExtCtrls, FMX.TabControl,
  FMX.Layouts, Fmx.Bind.Editors, Fmx.Bind.DBEngExt, FMX.EditBox, FMX.NumberBox,
  FMX.SearchBox, FMX.ActnList, FMX.DateTimeCtrls, FMX.ListView.Types, FMX.ListView,
  FMX.ListView.Appearances, Fmx.Bind.GenData, FMX.ListView.Adapters.Base,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.DB, Data.Bind.EngExt,
  Data.Bind.Components, Data.Bind.DBScope, Data.Bind.GenData, Data.Bind.ObjectScope,
  UnFMX_MultiDetailAppearanceU, UnGrl_Vars;

type
  TPesqType = (stCar=0, stCon=1, stEnt=2);
  TFmLctGer = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    ListBox2: TListBox;
    ListBoxHeader3: TListBoxHeader;
    BindingsList1: TBindingsList;
    QrCarteiras: TFDQuery;
    QrCarteirasCodigo: TIntegerField;
    BindSourceDB2: TBindSourceDB;
    LinkFillControlToField2: TLinkFillControlToField;
    QrLanctos: TFDQuery;
    QrLanctosControle: TIntegerField;
    BindSourceDB1: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ChangeTabAction4: TChangeTabAction;
    QrLanctosDB: TFDQuery;
    BindSourceDB3: TBindSourceDB;
    SbEdita: TSpeedButton;
    SbExclui: TSpeedButton;
    SbVolta: TSpeedButton;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIControle: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICarteira: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIData: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBIConta: TListBoxItem;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    LBIEntidade: TListBoxItem;
    ListBoxGroupHeader7: TListBoxGroupHeader;
    LBIValor: TListBoxItem;
    ListBoxGroupHeader8: TListBoxGroupHeader;
    LBIDescricao: TListBoxItem;
    ListBoxGroupHeader9: TListBoxGroupHeader;
    LBINotaFiscal: TListBoxItem;
    ListBoxGroupHeader10: TListBoxGroupHeader;
    LBIVencimento: TListBoxItem;
    Label2: TLabel;
    QrCarteirasNome: TStringField;
    QrLanctosDescricao: TStringField;
    QrLanctosData: TDateField;
    LinkFillControlToField4: TLinkFillControlToField;
    QrCarts: TFDQuery;
    QrCartsCodigo: TIntegerField;
    QrCartsNome: TStringField;
    QrCartsPagRec: TSmallintField;
    BindSourceDB4: TBindSourceDB;
    LinkFillControlToField5: TLinkFillControlToField;
    BindSourceDB5: TBindSourceDB;
    LinkFillControlToField6: TLinkFillControlToField;
    BindSourceDB6: TBindSourceDB;
    LinkFillControlToField7: TLinkFillControlToField;
    QrLanctosValor: TFloatField;
    QrLanctosCredito: TFloatField;
    QrLanctosDebito: TFloatField;
    ToolBar1: TToolBar;
    SbInclui: TSpeedButton;
    CBCarteiras: TComboBox;
    SbSaida: TSpeedButton;
    LinkFillControlToField9: TLinkFillControlToField;
    LinkFillControlToField8: TLinkFillControlToField;
    QrLanctosGeneroTXT: TStringField;
    ListView1: TListView;
    LinkFillControlToField3: TLinkFillControlToField;
    QrLanctosVencimento: TDateField;
    SBQrCode: TSpeedButton;
    LBIQtde: TListBoxItem;
    ListBoxGroupHeader11: TListBoxGroupHeader;
    SBDuplica: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure CBCarteirasChange(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure QrLanctosCalcFields(DataSet: TDataSet);
    procedure SelecionaLancto;
    procedure ListView1ItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure ListView1ButtonClick(const Sender: TObject;
      const AItem: TListItem; const AObject: TListItemSimpleControl);
    procedure FormShow(Sender: TObject);
    procedure SBQrCodeClick(Sender: TObject);
    procedure SbEditaClick(Sender: TObject);
    procedure SBDuplicaClick(Sender: TObject);
  private
    { Private declarations }
    FCodigo, FCarteira: Integer;
    FCarteiraTXT: String;
    procedure ReopenLanctos(Controle: Integer);
    procedure ReopenQrLanctos(Carteira: Integer);
    procedure ReopenCarteiras();
  public
    { Public declarations }
  end;

var
  FmLctGer: TFmLctGer;

implementation

{$R *.fmx}

uses Module, UnFMX_DmkProcFunc, UnGrl_DmkDB, UnDmkEnums, UnFMX_Geral,
  UnFMX_Grl_Vars, MyListas, UnFMX_Financeiro, UnFMX_FinanceiroJan;

procedure TFmLctGer.CBCarteirasChange(Sender: TObject);
var
  Carteira: Integer;
begin
  if CBCarteiras.ItemIndex > -1 then
  begin
    Carteira := FMX_dmkPF.GetSelectedValue(CBCarteiras);
    //
    if Carteira <> 0 then
    begin
      FCarteira    := Carteira;
      FCarteiraTXT := CBCarteiras.Selected.Text;
      //
      ReopenQrLanctos(Carteira)
    end else
    begin
      FCarteira    := 0;
      FCarteiraTXT := '';
      //
      QrLanctos.Close;
    end;
  end;
end;

procedure TFmLctGer.FormCreate(Sender: TObject);
begin
  TabControl1.TabIndex    := 0;
  TabControl1.TabPosition := TTabPosition.None;
  //
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SBQrCode.Visible := True;
  {$ELSE}
  SBQrCode.Visible := True;
  FmLctGer.Height  := 600;
  {$ENDIF}
  //
  TabControl1Change(Self);
end;

procedure TFmLctGer.FormShow(Sender: TObject);
begin
  FCodigo := 0;
  //
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmLctGer.ListView1ButtonClick(const Sender: TObject;
  const AItem: TListItem; const AObject: TListItemSimpleControl);
begin
  SelecionaLancto;
end;

procedure TFmLctGer.ListView1ItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  SelecionaLancto;
end;

procedure TFmLctGer.QrLanctosCalcFields(DataSet: TDataSet);
begin
  QrLanctosValor.Value := QrLanctosCredito.Value - QrLanctosDebito.Value;
end;

procedure TFmLctGer.ReopenCarteiras;
begin
  Grl_DmkDB.AbreSQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM wcarteiras ',
    'WHERE Ativo = 1 ',
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    'ORDER BY Nome ASC ',
    '']);
end;

procedure TFmLctGer.ReopenLanctos(Controle: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrLanctosDB, Dmod.MyDB, [
    'SELECT lct.*, ',
    'CASE WHEN con.Credito = 1 THEN 1 ELSE 0 END ContaCred, ',
    'con.Nome ContaTXT, car.Nome CarteiraTXT, ',
    'CASE WHEN ent.Tipo = 0 THEN ent.RazaoSocial ELSE ent.Nome END FornecedorTXT, ',
    'CASE WHEN enb.Tipo = 0 THEN enb.RazaoSocial ELSE enb.Nome END ClienteTXT ',
    'FROM ' + WLAN_CTOS + ' lct ',
    'LEFT JOIN wcontas con ON con.Codigo = lct.Genero ',
    'LEFT JOIN wcarteiras car ON car.Codigo = lct.Carteira ',
    'LEFT JOIN wentidades ent ON ent.Codigo = lct.Fornecedor ',
    'LEFT JOIN wentidades enb ON enb.Codigo = lct.Cliente ',
    'WHERE lct.Controle=' + FormatFloat('0', Controle),
    'AND lct.CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND lct.LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmLctGer.ReopenQrLanctos(Carteira: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrLanctos, Dmod.MyDB, [
    'SELECT lct.Data, lct.Controle, lct.Descricao, ',
    'lct.Credito, lct.Debito, con.Nome GeneroTXT ',
    'FROM ' + WLAN_CTOS + ' lct ',
    'LEFT JOIN wcontas con ON con.Codigo = lct.Genero ',
    'WHERE lct.Carteira=' + FMX_Geral.FF0(Carteira),
    'AND lct.CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND lct.LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    'ORDER BY lct.Data ',
    '']);
end;

procedure TFmLctGer.SBDuplicaClick(Sender: TObject);
begin
  FMX_Financeiro.DuplicaLancamento(QrLanctosDB);
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmLctGer.SbEditaClick(Sender: TObject);
begin
  FMX_Financeiro.AlteraLancamento(QrLanctosDB, tgrAltera);
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmLctGer.SbExcluiClick(Sender: TObject);
var
  Carteira: Integer;
begin
  if Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite('Deseja excluir este registro?',
    WLAN_CTOS, 'Controle', FCodigo, Dmod.MyDB) = mrYes then
  begin
    Carteira := FMX_dmkPF.GetSelectedValue(CBCarteiras);
    //
    ReopenQrLanctos(Carteira);
    //
    ExecuteAction(ChangeTabAction1);
  end;
end;

procedure TFmLctGer.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  //
  FMX_FinanceiroJan.MostraLctEdit(QrLanctos, nil, 0, FCarteira, FCarteiraTXT,
    Date, Date);
end;

procedure TFmLctGer.SBQrCodeClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraLctImpQrCode();
end;

procedure TFmLctGer.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmLctGer.SelecionaLancto;
begin
  FCodigo := FMX_dmkPF.GetSelectedValue(ListView1);
  //
  ExecuteAction(ChangeTabAction2);
end;

procedure TFmLctGer.TabControl1Change(Sender: TObject);
var
  Carteira, Genero, Entidade: Integer;
  NotaFiscal, SerieNF: String;
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin
      ReopenCarteiras;
    end;
    1: //Visualiza
    begin
      ReopenLanctos(FCodigo);
      //
      SerieNF := QrLanctosDB.FieldByName('SerieNF').AsString;
      //
      if SerieNF <> '' then
        NotaFiscal := SerieNF + '-' + FMX_Geral.FF0(QrLanctosDB.FieldByName('NotaFiscal').AsInteger)
      else
        NotaFiscal := FMX_Geral.FF0(QrLanctosDB.FieldByName('NotaFiscal').AsInteger);
      //
      LBIControle.Text   := FormatFloat('0', QrLanctosDB.FieldByName('Controle').AsInteger);
      LBICarteira.Text   := QrLanctosDB.FieldByName('CarteiraTXT').AsString;
      LBIData.Text       := QrLanctosDB.FieldByName('Data').AsString;
      LBIConta.Text      := QrLanctosDB.FieldByName('ContaTXT').AsString;
      LBIDescricao.Text  := QrLanctosDB.FieldByName('Descricao').AsString;
      LBIVencimento.Text := QrLanctosDB.FieldByName('Vencimento').AsString;
      LBINotaFiscal.Text := NotaFiscal;
      LBIQtde.Text       := FMX_Geral.FFT(QrLanctosDB.FieldByName('Qtde').AsFloat, 2, siPositivo);
      //
      if QrLanctosDB.FieldByName('ContaCred').AsInteger = 1 then
      begin
        LBIEntidade.Text := QrLanctosDB.FieldByName('ClienteTXT').AsString;
        LBIValor.Text    := FloatToStrF(QrLanctosDB.FieldByName('Credito').AsFloat, ffNumber, 8, 2);
      end else
      begin
        LBIEntidade.Text := QrLanctosDB.FieldByName('FornecedorTXT').AsString;
        LBIValor.Text    := FloatToStrF(QrLanctosDB.FieldByName('Debito').AsFloat, ffNumber, 8, 2);
      end;
    end;
  end;
end;

end.












