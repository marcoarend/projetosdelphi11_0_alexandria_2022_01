unit LctImpQrCode;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Actions,
  System.Variants, System.Rtti, System.Bindings.Outputs, System.NetEncoding,
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Android_FMX_ProcFunc, Android_BarcodeScanner,
  {$ENDIF}
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.TabControl, FMX.Layouts,
  FMX.ListBox, FMX.ScrollBox, FMX.Memo, FMX.ActnList, Fmx.Bind.Editors,
  Fmx.Bind.DBEngExt, FMX.StdActns, FMX.MediaLibrary.Actions, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet, Data.DB, Data.Bind.EngExt,
  Data.Bind.Components, Data.Bind.DBScope, UnGrl_Vars;

type
  TFmLctImpQrCode = class(TForm)
    Label1: TLabel;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    ListBoxHeader2: TListBoxHeader;
    SbSaida: TSpeedButton;
    ListBox2: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbQrCode: TSpeedButton;
    MeDados: TMemo;
    SbInclui: TSpeedButton;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    TbLctQrCode: TFDTable;
    TbLctQrCodeCodigo: TIntegerField;
    QrLctQrCode: TFDQuery;
    BSTbLctQrCode: TBindSourceDB;
    ListBox3: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBILancto: TListBoxItem;
    ListBoxHeader4: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    SBCompartilhar: TSpeedButton;
    TbLctQrCodeNome: TWideMemoField;
    ShowShareSheetAction1: TShowShareSheetAction;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    SBLimpar: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SBImportar: TSpeedButton;
    procedure SbSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbQrCodeClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure TabControl1Change(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure SBLimparClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ShowShareSheetAction1BeforeExecute(Sender: TObject);
    procedure SBImportarClick(Sender: TObject);
  private
    { Private declarations }
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    QrCode: TAndroidBarcodeScanner;
    procedure MostraBarcode(Sender: TAndroidBarcodeScanner; ABarcode: string);
    {$ENDIF}
    procedure ReopenTbLctQrCode;
    procedure ReopenLctQrCode(Codigo: Integer);
    function  VerificaQrCode(Codigo: Integer; QrCode: String): Integer;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmLctImpQrCode: TFmLctImpQrCode;

implementation

uses UnGrl_DmkDB, UnFMX_Grl_Vars, UnFMX_Geral, UnFMX_DmkProcFunc, MyListas,
  Module, UnDmkEnums, UnFMX_Financeiro;

{$R *.fmx}

procedure TFmLctImpQrCode.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  SBCompartilhar.Visible  := False;
  //
  ReopenTbLctQrCode;
  //
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SbInclui.Visible := True;
  SbQrCode.Visible := True;
  QrCode           := TAndroidBarcodeScanner.Create(true);
  QrCode.OnBarcode := MostraBarcode;
  {$ELSE}
  SbInclui.Visible := False;
  SbQrCode.Visible := False;
  {$ENDIF}
end;

{$IF DEFINED(iOS) or DEFINED(ANDROID)}
procedure TFmLctImpQrCode.MostraBarcode(Sender: TAndroidBarcodeScanner;
  ABarcode: string);
var
  Texto: String;
begin
  Texto := ABarcode;
  Texto := stringReplace(Texto, sLineBreak, ' ', [rfReplaceAll]);
  //
  MeDados.Text := Texto;
end;
{$ENDIF}

procedure TFmLctImpQrCode.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := FMX_dmkPF.GetSelectedValue(ListBox1);
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmLctImpQrCode.ReopenLctQrCode(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrLctQrCode, Dmod.MyDB, [
    'SELECT * ',
    'FROM wlctqrcode ',
    'WHERE Codigo=' + FMX_Geral.FF0(Codigo),
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmLctImpQrCode.ReopenTbLctQrCode;
begin
  TbLctQrCode.Close;
  TbLctQrCode.Connection := Dmod.MyDB;
  TbLctQrCode.Filtered   := True;
  TbLctQrCode.Filter     := 'CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT) +
                            ' AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel));
  TbLctQrCode.TableName  := 'wlctqrcode';
  TbLctQrCode.Open();
end;

procedure TFmLctImpQrCode.SbConfirmaClick(Sender: TObject);
var
  Codigo, CliInt, QrCode: Integer;
  Nome: String;
  SQLTipo: TSQLType;
begin
  Codigo := FCodigo;
  Nome   := THTMLEncoding.HTML.Encode(MeDados.Text);
  CliInt := VAR_WEB_USR_ENT;
  //
  if FMX_dmkPF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  //
  QrCode := VerificaQrCode(Codigo, MeDados.Text);
  //
  if FMX_dmkPF.FIC(QrCode <> 0, 'Este QrCode j� foi lan�ado com o ID ' +
    FMX_Geral.FF0(QrCode)) then Exit;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := Grl_DmkDB.ObtemCodigoInt_Sinc('wlctqrcode', 'Codigo', Dmod.QrAux,
                 Dmod.MyDB, stIns, stMobile);
  end else
    SQLTipo := stUpd;
  //
  if Codigo <> 0 then
  begin
    Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo,
      'wlctqrcode', False, ['CliInt', 'Nome'], ['Codigo'],
      [CliInt, Nome], [Codigo], True, False, '', stMobile, True);
  end;
  ReopenTbLctQrCode;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmLctImpQrCode.SbExcluiClick(Sender: TObject);
begin
  if Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite('Deseja excluir este registro?',
    'wlctqrcode', 'Codigo', FCodigo, Dmod.MyDB) = mrYes then
  begin
    ReopenTbLctQrCode;
    ExecuteAction(ChangeTabAction1);
  end;
end;

procedure TFmLctImpQrCode.SBImportarClick(Sender: TObject);
var
  Codigo: Integer;
  QrCode: String;
begin
  Codigo := FMX_Geral.IMV(LBICodigo.Text);
  QrCode := LBINome.Text;
  //
  if (Codigo <> 0) and (QrCode <> '') then
  begin
    FMX_Financeiro.ImportaNFCe(Codigo, QrCode);
    ExecuteAction(ChangeTabAction1);
  end;
end;

procedure TFmLctImpQrCode.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmLctImpQrCode.SBLimparClick(Sender: TObject);
begin
  MeDados.Text := '';
end;

procedure TFmLctImpQrCode.SbQrCodeClick(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  if not Android_FMX_dmkPF.AppEstaInstalado(CO_App_Name_QrCode) then
  begin
    Android_FMX_dmkPF.AbrirAppLoja(CO_App_Name_QrCode);
  end else
  begin
    QrCode.Scan;
  end;
  {$ENDIF}
end;

procedure TFmLctImpQrCode.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmLctImpQrCode.ShowShareSheetAction1BeforeExecute(Sender: TObject);
var
  URL, Msg: String;
begin
  URL := LBINome.Text;
  //
  if URL <> '' then
  begin
    URL := stringReplace(URL, ';', '&', [rfReplaceAll]);
    Msg := CO_APP_Descri + sLineBreak + 'QrCode da nota fiscal:' + sLineBreak + URL;
    //
    ShowShareSheetAction1.TextMessage := Msg;
  end;
end;

procedure TFmLctImpQrCode.SpeedButton1Click(Sender: TObject);
var
  QrCode: String;
begin
  QrCode := LBINome.Text;
  //
  if QrCode <> '' then
    FMX_Financeiro.AbrirNFCe(QrCode);
end;

procedure TFmLctImpQrCode.TabControl1Change(Sender: TObject);
var
  Codigo: Integer;
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenLctQrCode(FCodigo);
      //
      Codigo := QrLctQrCode.FieldByName('Codigo').AsInteger;
      //
      LBICodigo.Text := FMX_Geral.FF0(Codigo);
      LBINome.Text   := QrLctQrCode.FieldByName('Nome').AsString;
      LBILancto.Text := FMX_Geral.FF0(QrLctQrCode.FieldByName('Lancto').AsInteger);
      //
      SBImportar.Visible := (QrLctQrCode.FieldByName('Lancto').AsInteger = 0);
    end;
    2: //Edita
    begin
      if FCodigo <> 0 then
      begin
        MeDados.Text := QrLctQrCode.FieldByName('Nome').AsString;
      end else
      begin
        MeDados.Text := '';
      end;
    end;
  end;
end;

function TFmLctImpQrCode.VerificaQrCode(Codigo: Integer;
  QrCode: String): Integer;
var
  Nome, SQLCompl: String;
begin
  Result := 0;
  //
  Nome := stringReplace(QrCode, '&', ';', [rfReplaceAll]);
  //
  if Codigo <> 0 then
    SQLCompl := 'AND Codigo=' + FMX_Geral.FF0(Codigo)
  else
    SQLCompl := '';
  //
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM wlctqrcode ',
    'WHERE CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    'AND Nome = "'+ Nome +'"',
    SQLCompl,
    '']);
  if Dmod.QrLoc.RecordCount > 0 then
    Result := Dmod.QrLoc.FieldByName('Codigo').AsInteger;
  //
  Dmod.QrLoc.Close;
end;

end.



