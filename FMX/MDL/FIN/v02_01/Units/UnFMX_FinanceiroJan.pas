unit UnFMX_FinanceiroJan;

interface

uses System.SysUtils, System.Types, FireDAC.Comp.Client, UnDmkEnums;

type
  TUnFMX_FinanceiroJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraLctGer();
    procedure MostraLctEdit(QueryLanctos: TFDQuery; QueryLanctosDB: TFDQuery;
              Codigo, Carteira: Integer; CarteiraTXT: String; Data,
              Vencto: TDate; Conta: Integer = 0; ContaTXT: String = '';
              Entidade: Integer = 0; EntidadeTXT: String = '';
              Valor: Double = 0; Descricao: String = '';
              NotaFiscal: Integer = 0; SerieNF: String = ''; Qtde: Double = 0;
              QrCode: String = ''; IDQrCode: Integer = 0);
    procedure MostraContas;
    procedure MostraCarteiras;
    procedure MostraLctImpQrCode();
  end;
var
  FMX_FinanceiroJan: TUnFMX_FinanceiroJan;

implementation

uses UnFMX_Geral, UnFMX_DmkProcFunc, LctGer, Contas, Carteiras, LctImpQrCode,
  LctEdit, UnFMX_DmkForms;

{ TUnAllOS_Financeiro }

procedure TUnFMX_FinanceiroJan.MostraCarteiras;
begin
  FMX_dmkForms.CriaFm_AllOS(TFmCarteiras, FmCarteiras);
end;

procedure TUnFMX_FinanceiroJan.MostraContas;
begin
  FMX_dmkForms.CriaFm_AllOS(TFmContas, FmContas);
end;

procedure TUnFMX_FinanceiroJan.MostraLctEdit(QueryLanctos: TFDQuery;
  QueryLanctosDB: TFDQuery; Codigo, Carteira: Integer; CarteiraTXT: String;
  Data, Vencto: TDate; Conta: Integer = 0; ContaTXT: String = '';
  Entidade: Integer = 0; EntidadeTXT: String = ''; Valor: Double = 0;
  Descricao: String = ''; NotaFiscal: Integer = 0; SerieNF: String = '';
  Qtde: Double = 0; QrCode: String = ''; IDQrCode: Integer = 0);
begin
  if FMX_dmkForms.CriaFm_AllOS2(TFmLctEdit, FmLctEdit) = True then
  begin
    FmLctEdit.FCodigo         := Codigo;
    FmLctEdit.FCarteira       := Carteira;
    FmLctEdit.FCarteiraTXT    := CarteiraTXT;
    FmLctEdit.FData           := Data;
    FmLctEdit.FVencto         := Vencto;
    FmLctEdit.FConta          := Conta;
    FmLctEdit.FContaTXT       := ContaTXT;
    FmLctEdit.FEntidade       := Entidade;
    FmLctEdit.FEntidadeTXT    := EntidadeTXT;
    FmLctEdit.FValor          := Valor;
    FmLctEdit.FDescri         := Descricao;
    FmLctEdit.FNotaFiscal     := NotaFiscal;
    FmLctEdit.FSerieNF        := SerieNF;
    FmLctEdit.FQtde           := Qtde;
    FmLctEdit.FQueryLanctos   := QueryLanctos;
    FmLctEdit.FQueryLanctosDB := QueryLanctosDB;
    FmLctEdit.FQrCode         := QrCode;
    FmLctEdit.FIDQrCode       := IDQrCode;
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmLctEdit.Show;
    {$ELSE}
    FmLctEdit.ShowModal;
    FmLctEdit.Destroy;
    {$ENDIF}
  end;
end;

procedure TUnFMX_FinanceiroJan.MostraLctGer();
begin
  FMX_dmkForms.CriaFm_AllOS(TFmLctGer, FmLctGer);
end;

procedure TUnFMX_FinanceiroJan.MostraLctImpQrCode;
begin
  FMX_dmkForms.CriaFm_AllOS(TFmLctImpQrCode, FmLctImpQrCode);
end;

end.


