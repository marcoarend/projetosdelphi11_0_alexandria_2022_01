unit UnFMX_Financeiro;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.NetEncoding,
  FireDAC.Comp.Client, Xml.xmldom, Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc,
  UnDmkEnums, UnGrl_Vars;

type
  TGerencimantoDeRegistro = (tgrInclui, tgrAltera, tgrDuplica, tgrExclui, tgrTeste);
  TUnFMX_Financeiro = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AlteraLancamento(QueryLanctos: TFDQuery; Acao: TGerencimantoDeRegistro);
    procedure DuplicaLancamento(QueryLanctos: TFDQuery);
    function  ValidaDadosGenero(Query: TFDQuery; DB: TFDConnection;
              Valor: Integer; var Credito: Boolean): Boolean;
    // N F C E
    procedure ImportaNFCe(IDQrCode: Integer; QrCode: String);
    procedure AbrirNFCe(QrCode: String);
  end;
var
  FMX_Financeiro: TUnFMX_Financeiro;

implementation

uses UnGrl_DmkDB, UnFMX_Geral, UnFMX_DmkWeb, UnFMX_Grl_Vars, MyListas,
  UnFMX_DmkProcFunc, UnFMX_FinanceiroJan;

{ TUnFMX_Financeiro }

procedure TUnFMX_Financeiro.AbrirNFCe(QrCode: String);
var
  Conexao, Resp: Integer;
  Msg: String;
begin
  if QrCode <> '' then
  begin
    Conexao := FMX_dmkWeb.VerificaConexaoWeb(Msg);
    //
    if Conexao <= 0 then
    begin
      FMX_Geral.MB_Aviso('N�o ser� poss�vel abrir a NF!' + sLineBreak +
        'Seu dispositivo n�o est� conectado na internet!');
      Exit;
    end else
    if Conexao = 2 then
    begin
      Resp := FMX_Geral.MB_Pergunta('Seu dispositivo est� conectado na internet atrav�s de dados m�veis!' +
        sLineBreak + 'Deseja continuar?');

      if Resp <> mrYes then
        Exit;
    end;
    QrCode := stringReplace(QrCode, ';', '&', [rfReplaceAll]);
    //
    FMX_dmkPF.AbrirURLBrowser(QrCode);
  end;
end;

procedure TUnFMX_Financeiro.AlteraLancamento(QueryLanctos: TFDQuery;
  Acao: TGerencimantoDeRegistro);
var
  Controle, Carteira, Conta, Entidade, NotaFiscal: Integer;
  CarteiraTXT, ContaTXT, EntidadeTXT, Descricao, SerieNF: String;
  Data: TDate;
  Valor, Qtde: Double;
begin
  if Acao = tgrDuplica then
    Controle := 0
  else
    Controle := QueryLanctos.FieldByName('Controle').AsInteger;
  //
  Carteira    := QueryLanctos.FieldByName('Carteira').AsInteger;
  CarteiraTXT := QueryLanctos.FieldByName('CarteiraTXT').AsString;
  Data        := QueryLanctos.FieldByName('Data').AsDateTime;
  Conta       := QueryLanctos.FieldByName('Genero').AsInteger;
  ContaTXT    := QueryLanctos.FieldByName('ContaTXT').AsString;
  //
  if QueryLanctos.FieldByName('ContaCred').AsInteger = 1 then
  begin
    Entidade    := QueryLanctos.FieldByName('Cliente').AsInteger;
    EntidadeTXT := QueryLanctos.FieldByName('ClienteTXT').AsString;
    Valor       := QueryLanctos.FieldByName('Credito').AsFloat;
  end else
  begin
    Entidade    := QueryLanctos.FieldByName('Fornecedor').AsInteger;
    EntidadeTXT := QueryLanctos.FieldByName('FornecedorTXT').AsString;
    Valor       := QueryLanctos.FieldByName('Debito').AsFloat;
  end;
  Descricao   := QueryLanctos.FieldByName('Descricao').AsString;
  NotaFiscal  := QueryLanctos.FieldByName('NotaFiscal').AsInteger;
  SerieNF     := QueryLanctos.FieldByName('SerieNF').AsString;
  Qtde        := QueryLanctos.FieldByName('Qtde').AsFloat;
  //
  FMX_FinanceiroJan.MostraLctEdit(nil, QueryLanctos, Controle, Carteira,
    CarteiraTXT, Data, Data, Conta, ContaTXT, Entidade, EntidadeTXT, Valor,
    Descricao, NotaFiscal, SerieNF, Qtde);
end;

procedure TUnFMX_Financeiro.DuplicaLancamento(QueryLanctos: TFDQuery);
begin
  AlteraLancamento(QueryLanctos, tgrDuplica);
end;

procedure TUnFMX_Financeiro.ImportaNFCe(IDQrCode: Integer; QrCode: String);
var
  XMLArqRes: IXMLDocument;
  MsgCod, Conexao, Resp, i, Entidade, NF: Integer;
  Qtde, Valor: Double;
  Emissao: TDate;
  MsgTxt, Msg, Res, EntidadeTXT, Serie: String;
begin
  if QrCode <> '' then
  begin
    Conexao := FMX_dmkWeb.VerificaConexaoWeb(Msg);
    //
    if Conexao <= 0 then
    begin
      FMX_Geral.MB_Aviso('Importa��o abortada!' + sLineBreak +
        'Seu dispositivo n�o est� conectado na internet!');
      Exit;
    end else
    if Conexao = 2 then
    begin
      Resp := FMX_Geral.MB_Pergunta('Seu dispositivo est� conectado na internet atrav�s de dados m�veis!' +
        sLineBreak + 'Deseja continuar?');

      if Resp <> mrYes then
        Exit;
    end;
    Res := FMX_dmkWeb.REST_URL_Post(
      ['AplicativoSolicitante', 'IDUsuarioSolicitante', 'Token'],
      [FMX_Geral.FF0(CO_DMKID_APP), FMX_Geral.FF0(VAR_WEB_USR_ID), VAR_WEB_USR_TOKEN],
      ['id', 'method', 'DMKID_APP', 'QrCode'],
      [VAR_WEB_USR_WEBID, 'REST_dmkImporta_NFCe', FMX_Geral.FF0(CO_DMKID_APP), QrCode],
      CO_URL_DMKAPI);
    //
    if Res <> '' then
    begin
      XMLArqRes := TXMLDocument.Create(nil);
      XMLArqRes.Active := False;
      try
        XMLArqRes.LoadFromXML(Res);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLArqRes.DocumentElement.ChildNodes[i] do
          begin
            MsgCod := FMX_Geral.IMV(ChildNodes['MsgCod'].Text);
            MsgTxt := ChildNodes['MsgTxt'].Text;
            //
            if MsgCod = 101 then
            begin
              Entidade    := FMX_Geral.IMV(ChildNodes['nfce_codigo'].Text);
              EntidadeTXT := ChildNodes['nfce_nome'].Text;
              Qtde        := FMX_Geral.DMV(ChildNodes['nfce_qtde'].Text);
              Valor       := FMX_Geral.DMV(ChildNodes['nfce_valor'].Text);
              //
              if ChildNodes['nfce_nf'].Text <> '' then
                NF := FMX_Geral.IMV(ChildNodes['nfce_nf'].Text)
              else
                NF := 0;
              //
              if ChildNodes['nfce_serie'].Text <> '' then
                Serie := ChildNodes['nfce_serie'].Text
              else
                Serie := '';
              //
              if ChildNodes['nfce_emiss'].Text <> '' then
                Emissao := StrToDate(ChildNodes['nfce_emiss'].Text)
              else
                Emissao := Date;
              //
              FMX_FinanceiroJan.MostraLctEdit(nil, nil, 0, 0, '', Emissao,
                Emissao, 0, '', Entidade, EntidadeTXT, Valor, '', NF, Serie,
                Qtde, QrCode, IDQrCode);
            end else
              FMX_Geral.MB_Aviso(MsgTxt);
          end;
        end;
      finally
        ;
      end;
    end else
      FMX_Geral.MB_Erro('Falha ao executar comando REST!');
  end;
end;

function TUnFMX_Financeiro.ValidaDadosGenero(Query: TFDQuery;
  DB: TFDConnection; Valor: Integer; var Credito: Boolean): Boolean;
begin
  Credito := False;
  //
  if Valor <> 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(Query, DB, [
      'SELECT Codigo, ',
      'CASE WHEN Credito = 1 THEN 1 ELSE 0 END ContaCred ',
      'FROM contas ',
      'WHERE Codigo=' + FormatFloat('0', Valor)]);
    if Query.RecordCount > 0 then
    begin
      Credito := FMX_Geral.IntToBool(Query.FieldByName('ContaCred').AsInteger);
      Result  := True;
    end else
      Result := False;
    Query.Close;
  end else
    Result := False;
end;

end.

