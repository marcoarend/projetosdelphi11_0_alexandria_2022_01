unit Carteiras;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Actions, System.Rtti, System.Bindings.Outputs, FMX.Types, FMX.Controls,
  FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.TabControl, FMX.Layouts, FMX.ListBox, FMX.Edit, FMX.SearchBox,
  FMX.ActnList, Fmx.Bind.Editors, Fmx.Bind.DBEngExt, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.Client, FireDAC.Comp.DataSet, Data.DB, Data.Bind.EngExt,
  Data.Bind.Components, Data.Bind.DBScope, UnGrl_Vars;

type
  TCartTipo = (ctCaixa=0, ctEmissao=2);
  TFmCarteiras = class(TForm)
    Label1: TLabel;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbInclui: TSpeedButton;
    SbSaida: TSpeedButton;
    SearchBox1: TSearchBox;
    ListBox2: TListBox;
    ListBoxHeader2: TListBoxHeader;
    SbVolta: TSpeedButton;
    SbExclui: TSpeedButton;
    SbEdita: TSpeedButton;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBITipo: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIPagRec: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBIAtivo: TListBoxItem;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    ListBox3: TListBox;
    ListBoxHeader3: TListBoxHeader;
    SbDesiste: TSpeedButton;
    SbConfirma: TSpeedButton;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    EdNome: TEdit;
    CkAtivo: TCheckBox;
    QrCarteiras: TFDQuery;
    TbCarteiras: TFDTable;
    TbCarteirasCodigo: TIntegerField;
    TbCarteirasNome: TStringField;
    BSTbCarteiras: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    CBPagRec: TComboBox;
    CBTIpo: TComboBox;
    procedure SbIncluiClick(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  private
    { Private declarations }
    procedure ReopenTbCarteiras;
    procedure ReopenCarteiras(Codigo: Integer);
    procedure ConfiguraTiposDeCarteiras();
    function  ObtemTipoCarteiraStr(Tipo: Integer): String;
    function  ObtemPagRecStr(PagRec: Integer): String;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmCarteiras: TFmCarteiras;

implementation

uses Module, UnFMX_DmkProcFunc, UnGrl_DmkDB, UnDmkEnums, UnFMX_Geral,
  UnFMX_Grl_Vars, MyListas;

{$R *.fmx}

procedure TFmCarteiras.ConfiguraTiposDeCarteiras;
begin
  CBTIpo.Items.Clear;
  CBTIpo.Items.Add('Caixa');
end;

procedure TFmCarteiras.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenTbCarteiras;
end;

procedure TFmCarteiras.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := FMX_dmkPF.GetSelectedValue(ListBox1);
    ExecuteAction(ChangeTabAction2);
  end;
end;

function TFmCarteiras.ObtemPagRecStr(PagRec: Integer): String;
begin
  case PagRec of
     -1: Result := 'Pagar';
      0: Result := 'Ambos';
      1: Result := 'Receber';
    else Result := 'N�o definido';
  end;
end;

function TFmCarteiras.ObtemTipoCarteiraStr(Tipo: Integer): String;
begin
  case Tipo of
    Integer(ctCaixa):
      Result := 'Caixa';
    Integer(ctEmissao):
      Result := 'Emiss�o';
    else
      Result := 'N�o definido';
  end;
end;

procedure TFmCarteiras.ReopenCarteiras(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT * ',
    'FROM wcarteiras ',
    'WHERE Codigo=' + FMX_Geral.FF0(Codigo),
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
end;

procedure TFmCarteiras.ReopenTbCarteiras;
begin
  TbCarteiras.Close;
  TbCarteiras.Connection := Dmod.MyDB;
  TbCarteiras.Filtered   := True;
  TbCarteiras.Filter     := 'CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT) +
                            ' AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel));
  TbCarteiras.TableName  := 'wcarteiras';
  TbCarteiras.Open();
  //
  if TbCarteiras.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmCarteiras.SbConfirmaClick(Sender: TObject);
var
  Codigo, Tipo, PagRec, Ativo: Integer;
  Nome: String;
  SQLTipo: TSQLType;
begin
  Codigo := FCodigo;
  Nome   := EdNome.Text;
  Tipo   := CBTIpo.ItemIndex;
  PagRec := CBPagRec.ItemIndex - 1;
  Ativo  := FMX_Geral.BoolToInt(CkAtivo.IsChecked);
  //
  if FMX_dmkPF.FIC(Nome = '', 'Defina uma descri��o!') then Exit;
  if FMX_dmkPF.FIC(Tipo < 0, 'Defina o tipo de carteira!') then Exit;
  if FMX_dmkPF.FIC(PagRec < -1, 'Defina a caracter�stica!') then Exit;
  //
  if Tipo = 1 then
    Tipo := Integer(ctEmissao)
  else
    Tipo := Integer(ctCaixa);
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Codigo  := Grl_DmkDB.ObtemCodigoInt_Sinc('wcarteiras', 'Codigo', Dmod.QrAux,
                 Dmod.MyDB, stIns, stMobile);
  end else
    SQLTipo := stUpd;
  //
  if Codigo <> 0 then
  begin
    Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLTipo,
      'wcarteiras', False, ['Nome', 'Tipo', 'PagRec', 'Ativo'], ['Codigo'],
      [Nome, Tipo, PagRec, Ativo], [Codigo], True, False, '', stMobile, True);
  end;
  ReopenTbCarteiras;
  //
  ExecuteAction(ChangeTabAction1);
end;

procedure TFmCarteiras.SbExcluiClick(Sender: TObject);
begin
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + WLAN_CTOS,
    'WHERE Carteira=' + FMX_Geral.FF0(FCodigo),
    'AND CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
    'AND LastAcao<>' + FMX_Geral.FF0(Integer(laDel)),
    '']);
  //
  if Dmod.QrAux.RecordCount = 0 then
  begin
    if Grl_DmkDB.ExcluiRegistroInt1_Sinc_SQLite('Deseja excluir este registro?',
      'wcarteiras', 'Codigo', FCodigo, Dmod.MyDB) = mrYes then
    begin
      ReopenTbCarteiras;
      ExecuteAction(ChangeTabAction1);
    end;
  end else
    FMX_Geral.MB_Aviso('Exclus�o abortada! Esta carteira j� foi utilizada nos lan�amentos financeiros.');
  //
  Dmod.QrAux.Close;
end;

procedure TFmCarteiras.SbIncluiClick(Sender: TObject);
begin
  FCodigo := 0;
  ExecuteAction(ChangeTabAction3);
end;

procedure TFmCarteiras.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmCarteiras.TabControl1Change(Sender: TObject);
var
  Codigo: Integer;
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenCarteiras(FCodigo);
      //
      Codigo := QrCarteiras.FieldByName('Codigo').AsInteger;
      //
      LBICodigo.Text := FMX_Geral.FF0(Codigo);
      LBINome.Text   := QrCarteiras.FieldByName('Nome').AsString;
      LBITipo.Text   := ObtemTipoCarteiraStr(QrCarteiras.FieldByName('Tipo').AsInteger);
      LBIPagRec.Text := ObtemPagRecStr(QrCarteiras.FieldByName('PagRec').AsInteger);
      //
      if QrCarteiras.FieldByName('Ativo').AsInteger = 1 then
        LBIAtivo.Text := 'Ativo'
      else
        LBIAtivo.Text := 'Inativo';
    end;
    2: //Edita
    begin
      ConfiguraTiposDeCarteiras;
      //
      if FCodigo <> 0 then
      begin
        ReopenCarteiras(FCodigo);
        //
        EdNome.Text        := QrCarteiras.FieldByName('Nome').AsString;
        CBTIpo.ItemIndex   := QrCarteiras.FieldByName('Tipo').AsInteger;
        CBPagRec.ItemIndex := QrCarteiras.FieldByName('PagRec').AsInteger + 1;
        CkAtivo.IsChecked  := FMX_Geral.IntToBool(QrCarteiras.FieldByName('Ativo').AsInteger);
      end else
      begin
        EdNome.Text        := '';
        CBTIpo.ItemIndex   := 0;
        CBPagRec.ItemIndex := -1;
        CkAtivo.IsChecked  := True;
      end;
    end;
  end;
end;

end.

