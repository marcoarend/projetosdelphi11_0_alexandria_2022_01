unit OViOPs;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums;

type
  TFmOViOPs = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    GrOPs: TGrid;
    QrOViOPs: TFDQuery;
    QrOViOPsNrOP: TIntegerField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    QrOViOPsROS: TLargeintField;
    PnLocal: TPanel;
    Label1: TLabel;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaLocal: TLabel;
    LaArtigo: TLabel;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    procedure GrOPsCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FLocal, FArtigo: Integer;
    FNO_Local, FNO_Artigo: String;
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOViOPs(SegmentoInsp: TSegmentoInsp; ParteNome: String);
  end;

var
  FmOViOPs: TFmOViOPs;

implementation

uses UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan, UnGrl_Vars,
  UnFMX_DmkProcFunc, UnOVSM_PF,
  Module;

{$R *.fmx}

procedure TFmOViOPs.FormCreate(Sender: TObject);
begin
  FLocal  := 0;
  FArtigo := 0;
end;

procedure TFmOViOPs.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViOPs.FormShow(Sender: TObject);
begin
  ReopenOViOPs(FSegmentoInsp, '');
end;

procedure TFmOViOPs.GrOPsCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOViOPs.GrOPsCellClick()';
begin
  if (QrOViOPs.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  case VAR_OPCOES_MOB_FoLocObjIns of
    0: App_Jan.MostraFormOViTamanho(FSegmentoInsp, FSeccaoInsp, FLocal, FArtigo,
       QrOViOPsNrOP.Value, FNO_Local, FNO_Artigo);
    1: App_Jan.MostraFormOViArtigos(FSegmentoInsp, FSeccaoInsp, FLocal,
       QrOViOPsNrOP.Value, FNO_Local);
    else Grl_Geral.MB_Erro('Janela n�o implementada!' + sProcName);
  end;
end;

procedure TFmOViOPs.ReopenOViOPs(SegmentoInsp: TSegmentoInsp; ParteNome: String);
var
  TabGerCab, SQL_AND: String;
begin
  TabGerCab := OVSM_PF.GetNomeTabSegmento_InspGerCab(FSegmentoInsp);
  //
  if FLocal <> 0 then
    SQL_AND := SQL_AND + 'AND igc.Local=' + Grl_Geral.FF0(FLocal) + sLineBreak;
  //
  if FArtigo <> 0 then
    SQL_AND := SQL_AND + 'AND igc.SeqGrupo=' + Grl_Geral.FF0(FArtigo) + sLineBreak;
  //
  if Trim(ParteNome) <> EmptyStr then
    SQL_AND := SQL_AND + 'AND igc.NrOP LIKE "%' + ParteNome + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrOViOPs, Dmod.AllDB, [
  'SELECT igc.NrOP, COUNT(igc.NrOP) ROS ',
  //'FROM ovgispgercab igc ',
  'FROM ' + TabGerCab + ' igc ',
  'WHERE igc.ZtatusIsp=' + Grl_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  SQL_AND,
  'GROUP BY igc.NrOP ',
  'ORDER BY igc.NrOP ',
  EmptyStr]);
end;

procedure TFmOViOPs.SbPesquisaClick(Sender: TObject);
begin
  ReopenOViOPs(FSegmentoInsp, EdPesquisa.Text);
end;

end.
