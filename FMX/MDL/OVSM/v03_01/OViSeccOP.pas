unit OViSeccOP;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, FMX.Layouts;

type
  TFmOViSeccOP = class(TForm)
    GrSeccOP: TGrid;
    QrOViSeccOP: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PnLocal: TPanel;
    Label1: TLabel;
    LaLocal: TLabel;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Layout1: TLayout;
    TitleLabel: TLabel;
    PnTamanho: TPanel;
    Label4: TLabel;
    LaTamanho: TLabel;
    QrCorda: TFDQuery;
    QrOViSeccOPCodigo: TIntegerField;
    QrOViSeccOPControle: TIntegerField;
    QrOViSeccOPOrdem: TIntegerField;
    QrOViSeccOPSeccaoOP: TStringField;
    QrOViSeccOPSeccaoMaq: TStringField;
    QrOViSeccOPQtReal: TFloatField;
    procedure SbPesquisaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrOViSeccOPCalcFields(DataSet: TDataSet);
    procedure GrSeccOPCellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
    FJaClicou: Boolean;
  public
    { Public declarations }
    FLocal, FArtigo, FNrOP, FCodGrade, FNrReduzidoOP, FProduto: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOViSeccOP(SegmentoInsp: TSegmentoInsp; SeccaooInsp:
              TSeccaoInsp; ParteNome: String);
  end;

var
  FmOViSeccOP: TFmOViSeccOP;

implementation

uses UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan,
  UnFMX_DmkForms, UnFMX_DmkProcFunc, UnOVSM_PF,
  Module,
  OViLocais, OViArtigos, OViOPs, OViTamanhos, Principal;

{$R *.fmx}

procedure TFmOViSeccOP.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViSeccOP.FormShow(Sender: TObject);
begin
  FJaClicou := False;
  ReopenOViSeccOP(FSegmentoInsp, FSeccaoInsp, '');
end;

procedure TFmOViSeccOP.GrSeccOPCellClick(const Column: TColumn;
  const Row: Integer);
const
  sProcName = 'TFmOViSeccOP.GrSeccOPCellClick()';
begin
  if FJaClicou then Exit;
  //
  FJaClicou := True;
  if (QrOViSeccOP.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  //
  FMX_DmkForms.DestroiFm_AllOS0(FmOViLocais);
  FMX_DmkForms.DestroiFm_AllOS0(FmOViArtigos);
  FMX_DmkForms.DestroiFm_AllOS0(FmOViOPs);
  FMX_DmkForms.DestroiFm_AllOS0(FmOViTamanhos);
  //
  case FSeccaoInsp of
    TSeccaoInsp.sccinspTinturaria:
      App_Jan.MostraFormOViTclCenSel(
      FSegmentoInsp, FSeccaoInsp, FLocal, FArtigo, FNrOP, FNrReduzidoOP,
      FProduto, FCodGrade, FNO_Local, FNO_Artigo, FCodTam,
      QrOViSeccOPControle.Value, QrOViSeccOPSeccaoOP.Value,
      QrOViSeccOPSeccaoMaq.Value, TStatusAndamentoInspecao.saiInspAberta, 0);
    else
      Grl_Geral.MB_Erro('SeccaoInsp n�o implementado em ' + sProcName);
  end;
  //
  Close;
end;

procedure TFmOViSeccOP.QrOViSeccOPCalcFields(DataSet: TDataSet);
var
  DtHr: String;
begin
{
  DtHr := Grl_Geral.FDT(QrOViTamanhosLastDtHr.Value, 0);
  QrOViTamanhosLastDtHr_BRK.Value := Copy(DtHr, 1, 11) +
   sLineBreak +  Copy(DtHr, 12);
}
end;

procedure TFmOViSeccOP.ReopenOViSeccOP(SegmentoInsp: TSegmentoInsp;
  SeccaooInsp: TSeccaoInsp; ParteNome: String);
var
  //TabGerCab, TabMobCab, SQL_AND1, SQL_AND2,
  Corda: String;
begin
  Grl_DmkDB.AbreSQLQuery0(QrCorda, Dmod.AllDB, [
  'SELECT Codigo ',
  'FROM ovgitxgercab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  EmptyStr]);
  //Grl_Geral.MB_SQL(self, QrCorda);
  if QrCorda.RecordCount > 0 then
  begin
    Corda := Grl_DmkDB.CordaDeQuery(QrCorda, 'Codigo');
    if Corda <> EmptyStr then
    begin
      Grl_DmkDB.AbreSQLQuery0(QrOViSeccOP, Dmod.AllDB, [
      'SELECT Codigo, Controle, Ordem, SeccaoOP, SeccaoMaq, QtReal  ',
      'FROM ovgitxgerbtl  ',
      'WHERE Codigo IN (' + Corda + ')',
      'ORDER BY Ordem ',
      EmptyStr]);
      //
     if QrOViSeccOP.RecordCount = 0 then
       Grl_Geral.MB_Aviso('N�o h� batelada cadastrada para inspe��o! [B]');
    end;
  end else
  begin
    GrSeccOP.RowCount := 0;
     Grl_Geral.MB_Aviso('N�o h� batelada cadastrada para inspe��o [A]!');
  end;
  //Grl_Geral.MB_SQL(self, QrOViSeccOP);
end;

procedure TFmOViSeccOP.SbPesquisaClick(Sender: TObject);
begin
  ReopenOViSeccOP(FSegmentoInsp, FSeccaoInsp, EdPesquisa.Text);
end;

//44 e 26 s�o melhores alturas para Panel>Edit e Label!

end.
