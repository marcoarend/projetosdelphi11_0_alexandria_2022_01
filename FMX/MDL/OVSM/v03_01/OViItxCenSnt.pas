//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit OViItxCenSnt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox, FMX.TabControl, FMX.Objects,
  MultiDetailAppearanceU, FMX.MobilePreview, FMX.Controls.Presentation,
  FMX.ListView.Adapters.Base, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.ScrollBox, FMX.Memo,
  UnDmkEnums, UnProjGroupEnums, FMX.Edit, FMX.Layouts;

type
  TFmOViItxCenSnt = class(TForm)
    Layout1: TLayout;
    ToggleEditMode: TSpeedButton;
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOVmItxMobCab: TFDQuery;
    Img1024: TImage;
    Img2048: TImage;
    Img3072: TImage;
    ImgErro: TImage;
    QrOVmItxMobCabNO_Local: TStringField;
    QrOVmItxMobCabNO_Artigo: TStringField;
    QrOVmItxMobCabCodTam: TStringField;
    QrOVmItxMobCabCodInMob: TIntegerField;
    QrOVmItxMobCabDeviceSI: TIntegerField;
    QrOVmItxMobCabDeviceID: TStringField;
    QrOVmItxMobCabDeviceCU: TIntegerField;
    QrOVmItxMobCabNome: TStringField;
    QrOVmItxMobCabOVgItxGer: TIntegerField;
    QrOVmItxMobCabLocal: TIntegerField;
    QrOVmItxMobCabNrOP: TIntegerField;
    QrOVmItxMobCabSeqGrupo: TIntegerField;
    QrOVmItxMobCabNrReduzidoOP: TIntegerField;
    QrOVmItxMobCabProduto: TIntegerField;
    QrOVmItxMobCabCodGrade: TIntegerField;
    QrOVmItxMobCabCodTam_1: TStringField;
    QrOVmItxMobCabDtHrAbert: TDateTimeField;
    QrOVmItxMobCabDtHrFecha: TDateTimeField;
    QrOVmItxMobCabOVcYnExg: TIntegerField;
    QrOVmItxMobCabQtReal: TFloatField;
    QrOVmItxMobCabQtLocal: TFloatField;
    QrOVmItxMobCabPecasItx: TIntegerField;
    QrOVmItxMobCabPecaAtual: TIntegerField;
    QrOVmItxMobCabSubInspDone: TShortintField;
    QrOVmItxMobCabPontosTot: TIntegerField;
    QrOVmItxMobCabEnviadoWeb: TShortintField;
    QrOVmItxMobCabInspResul: TIntegerField;
    QrOVmItxMobCabCodigo: TIntegerField;
    QrCab: TFDQuery;
    QrCabCodInMob: TIntegerField;
    QrCabDeviceSI: TIntegerField;
    QrCabDeviceID: TStringField;
    QrCabDeviceCU: TIntegerField;
    QrCabNome: TStringField;
    QrCabOVgItxGer: TIntegerField;
    QrCabLocal: TIntegerField;
    QrCabNrOP: TIntegerField;
    QrCabSeqGrupo: TIntegerField;
    QrCabNrReduzidoOP: TIntegerField;
    QrCabProduto: TIntegerField;
    QrCabCodGrade: TIntegerField;
    QrCabCodTam: TStringField;
    QrCabDtHrAbert: TDateTimeField;
    QrCabDtHrFecha: TDateTimeField;
    QrCabOVcYnsExg: TIntegerField;
    QrCabQtReal: TFloatField;
    QrCabQtLocal: TFloatField;
    QrCabPecasItx: TIntegerField;
    QrCabPecaAtual: TIntegerField;
    QrCabSubInspDone: TShortintField;
    QrCabPontosTot: TIntegerField;
    QrCabInspResul: TIntegerField;
    QrCabEnviadoWeb: TShortintField;
    QrCabLk: TIntegerField;
    QrCabDataCad: TDateField;
    QrCabDataAlt: TDateField;
    QrCabUserCad: TIntegerField;
    QrCabUserAlt: TIntegerField;
    QrCabAlterWeb: TShortintField;
    QrCabAtivo: TShortintField;
    QrCabRandmStr: TStringField;
    QrCabEmpresa: TIntegerField;
    QrCabNO_Local: TStringField;
    QrCabNO_Artigo: TStringField;
    QrCabCodigo: TIntegerField;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    SbLimpa: TSpeedButton;
    Label1: TLabel;
    LaLocal: TLabel;
    QrCabPerExgReal: TFloatField;
    QrCabDtHrMailSnt: TFloatField;
    QrCabSegmntInsp: TIntegerField;
    QrCabSeccaoInsp: TIntegerField;
    Layout2: TLayout;
    LaTitulo: TLabel;
    QrCabBatelada: TIntegerField;
    QrCabSeccaoOP: TStringField;
    QrCabSeccaoMaq: TStringField;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure SbLimpaClick(Sender: TObject);
    procedure SbPesquisaClick(Sender: TObject);
  private
    { Private declarations }
    FStrInfoDone: String;
    //
  public
    { Public declarations }
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure CarregaOVmItxMobCabload(FechaForm: Boolean);
  end;

var
  FmOViItxCenSnt: TFmOViItxCenSnt;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars, UnREST_SQL,
  UnFMX_DmkForms, UnFMX_DmkWeb, UnREST_App, UnFMX_DmkProcFunc, UnApp_Jan,
  UnOVSM_Vars,
  Module, OVdLocal;

procedure TFmOViItxCenSnt.CarregaOVmItxMobCabload(FechaForm: Boolean);
var
  LItem: TListViewItem;
  Linha2, Linha3: String;
begin
  LaLocal.Text := VAR_LAST_LOCAL_NOM;
  //
  ListViewMultiDetail.Items.Clear;
  //
  // Code to fill TListView
  ListViewMultiDetail.BeginUpdate;
  //if Dmod.AbreOVmItxMobCabload(QrOVmItxMobCab) then
  if Dmod.ReopenOVmItxMobCab(QrOVmItxMobCab, TStatusAndamentoInspecao.saiFinalizada,
  FSeccaoInsp, VAR_LAST_LOCAL_COD) then
  begin
    QrOVmItxMobCab.First;
    while not QrOVmItxMobCab.Eof do
    begin
        LItem := ListViewMultiDetail.Items.Add;
        LItem.Tag  := QrOVmItxMobCabCodInMob.Value;
        LItem.Text := QrOVmItxMobCabNO_Local.Value;
        // Update data managed by custom appearance
(*
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := Format('%s', [QrOVmItxMobCabNO_Artigo.Value]);
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := Format('%s', ['OP ' + Grl_Geral.FF0(QrOVmItxMobCabNrOP.Value)]);
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := Format('%s', ['Tamanho: ' + QrOVmItxMobCabCodTam.Value]);
*)
        Linha2 := 'OP ' + Grl_Geral.FF0(QrOVmItxMobCabNrOP.Value) + '   ' +
                  'Tamanho: ' + QrOVmItxMobCabCodTam.Value;
        Linha3 := 'Inspe��o: ' + Grl_Geral.FF0(QrOVmItxMobCabCodigo.Value) +
                  '-' + Grl_Geral.FF0(QrOVmItxMobCabCodInMob.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 1, 11, QrOVmItxMobCabNO_Artigo.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 2, 12, Linha2);
        FMX_DmkPF.DefTextoLVMDI(LItem, 3, 13, Linha3);
        //
        case QrOVmItxMobCabInspResul.Value of
          1024: LItem.BitmapRef := Img1024.Bitmap;
          2048: LItem.BitmapRef := Img2048.Bitmap;
          3072: LItem.BitmapRef := Img3072.Bitmap;
          else
          begin

            LItem.BitmapRef := ImgErro.Bitmap;
          end;
        end;
      //
      QrOVmItxMobCab.Next;
    end;
  end else
  if FechaForm then
    FMX_DmkForms.FechaFm_AllOS0(FmOViItxCenSnt);
end;

procedure TFmOViItxCenSnt.FormCreate(Sender: TObject);
begin
  ListViewMultiDetail.Items.Clear;
end;

procedure TFmOViItxCenSnt.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViItxCenSnt.FormShow(Sender: TObject);
begin
  CarregaOVmItxMobCabload(True);
end;

procedure TFmOViItxCenSnt.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  LItem: TListViewItem;
  CodInMob, Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade, Codigo,
  Batelada: Integer;
  NO_Local, NO_Artigo, CodTam, SeccaoOP, SeccaoMaq: String;
  SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp;
begin
  FMX_DmkPF.VibrarComoBotao();
  LItem := TListViewItem(ListViewMultiDetail.Selected);
  if LItem <> nil then
  begin
    CodInMob := LItem.Tag;
    Grl_DmkDB.AbreSQLQuery0(QrCab, Dmod.AllDB, [
    'SELECT loc.Nome NO_Local, art.Nome NO_Artigo,  ',
    'opr.CodTam,  ',
    'igc.* ',
    'FROM ovmitxmobcab igc ',
    'LEFT JOIN ovdlocal loc ON loc.Codigo=igc.Local ',
    'LEFT JOIN ovdreferencia art ON art.Codigo=igc.SeqGrupo ',
    'LEFT JOIN ovdproduto opr ON opr.Controle=igc.Produto ',
    'WHERE igc.CodInMob=' + Grl_Geral.FF0(CodInMob),
    EmptyStr]);
    //
    Codigo        := QrCabCodigo.Value;
    Local         := QrCabLocal.Value;
    Artigo        := QrCabSeqGrupo.Value;
    NrOP          := QrCabNrOP.Value;
    NrReduzidoOP  := QrCabNrReduzidoOP.Value;
    Produto       := QrCabProduto.Value;
    CodGrade      := QrCabCodGrade.Value;
    CodTam        := QrCabCodTam.Value;
    NO_Local      := QrCabNO_Local.Value;
    NO_Artigo     := QrCabNO_Artigo.Value;
    Batelada      := QrCabBatelada.Value;
    SeccaoOP      := QrCabSeccaoOP.Value;
    SeccaoMaq      := QrCabSeccaoMaq.Value;
    SegmentoInsp  := TSegmentoInsp(QrCabSegmntInsp.Value);
    SeccaoInsp    := TSeccaoInsp(QrCabSeccaoInsp.Value);

    //
    App_Jan.MostraFormOViTclCenSel(SegmentoInsp, SeccaoInsp, Local, Artigo,
      NrOP, NrReduzidoOP, Produto, CodGrade, NO_Local, NO_Artigo, CodTam,
      Batelada, SeccaoOP, SeccaoMaq, TStatusAndamentoInspecao.saiFinalizada,
      Codigo);
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViItxCenSnt);
  end;
end;

procedure TFmOViItxCenSnt.SbLimpaClick(Sender: TObject);
begin
  VAR_LAST_LOCAL_COD := 0;
  VAR_LAST_LOCAL_NOM := EmptyStr;
  CarregaOVmItxMobCabload(False);
end;

procedure TFmOViItxCenSnt.SbPesquisaClick(Sender: TObject);
begin
  App_Jan.MostraFormOVdLocal(TSegmentoInsp.sgminspTextil, FSeccaoInsp);
end;

procedure TFmOViItxCenSnt.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmOViItxCenSnt.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;


end.

