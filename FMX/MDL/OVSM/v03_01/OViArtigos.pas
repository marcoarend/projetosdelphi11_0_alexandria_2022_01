unit OViArtigos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, FMX.Layouts;

type
  TFmOViArtigos = class(TForm)
    GrArtigos: TGrid;
    QrOViArtigos: TFDQuery;
    QrOViArtigosCodigo: TIntegerField;
    QrOViArtigosNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PnLocal: TPanel;
    Label1: TLabel;
    LaLocal: TLabel;
    PnNrOP: TPanel;
    Label2: TLabel;
    LaNrOP: TLabel;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOViArtigosBRK_NOme: TStringField;
    Layout1: TLayout;
    TitleLabel: TLabel;
    QrSeccOP: TFDQuery;
    QrSeccOPCodigo: TIntegerField;
    QrSeccOPControle: TIntegerField;
    QrSeccOPSeccaoOP: TStringField;
    QrSeccOPQtReal: TFloatField;
    QrSeccOPNrOP: TIntegerField;
    QrSeccOPSeqGrupo: TIntegerField;
    QrSeccOPNrReduzidoOP: TIntegerField;
    QrSeccOPNome: TStringField;
    SbSeccaoOP: TSpeedButton;
    QrSeccOPCiclo: TIntegerField;
    QrSeccOPProduto: TIntegerField;
    QrSeccOPCodGrade: TIntegerField;
    QrSeccOPCodTam: TStringField;
    QrSeccOPSeccaoMaq: TStringField;
    procedure GrArtigosCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure QrOViArtigosCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure SbSeccaoOPClick(Sender: TObject);
  private
    { Private declarations }
    FJaClicou: Boolean;
  public
    { Public declarations }
    FCampos: array of String;
    FValors: array of Variant;
    FLocal, FNrOP: Integer;
    FNO_Local: String;
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOViArtigos(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; ParteNome: String);
  end;

var
  FmOViArtigos: TFmOViArtigos;

implementation

uses UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnOVSM_PF, UnFMX_DmkForms,
  Module, OViLocais;

{$R *.fmx}

procedure TFmOViArtigos.FormCreate(Sender: TObject);
begin
  FLocal  := 0;
  FNrOP   := 0;
end;

procedure TFmOViArtigos.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViArtigos.FormShow(Sender: TObject);
begin
  FJaClicou := False;
  ReopenOViArtigos(FSegmentoInsp, FSeccaoInsp, '');
end;

procedure TFmOViArtigos.GrArtigosCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOViArtigos.GrArtigosCellClick()';
begin
  if (QrOViArtigos.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  case VAR_OPCOES_MOB_FoLocObjIns of

    0: App_Jan.MostraFormOViOPs(FSegmentoInsp, FSeccaoInsp, FLocal,
       QrOViArtigosCodigo.Value, FNO_Local, QrOViArtigosNome.Value);

    1: App_Jan.MostraFormOViTamanho(FSegmentoInsp, FSeccaoInsp, FLocal,
       QrOViArtigosCodigo.Value, FNrOP, FNO_Local, QrOViArtigosNome.Value);

    else Grl_Geral.MB_Erro('Janela n�o implementada!' + sProcName);
  end;
end;

procedure TFmOViArtigos.QrOViArtigosCalcFields(DataSet: TDataSet);
begin
  QrOViArtigosBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrArtigos, 1, QrOViArtigosNome.Value);
end;

procedure TFmOViArtigos.ReopenOViArtigos(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp; ParteNome: String);
var
  TabGerCab, SQL_AND: String;
begin
  TabGerCab := OVSM_PF.GetNomeTabSegmento_InspGerCab(FSegmentoInsp);
  //
  SQL_AND := EmptyStr;
  if FLocal <> 0 then
    SQL_AND := SQL_AND + 'AND igc.Local=' + Grl_Geral.FF0(FLocal) + sLineBreak;
  //
  if FNrOP <> 0 then
    SQL_AND := SQL_AND + 'AND igc.NrOP=' + Grl_Geral.FF0(FNrOP) + sLineBreak;
  //
  if Trim(ParteNome) <> EmptyStr then
    SQL_AND := 'AND art.Nome LIKE "%' + ParteNome + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrOViArtigos, Dmod.AllDB, [
  'SELECT art.Codigo, art.Nome, COUNT(art.Codigo) ROS ',
  'FROM ovdreferencia art ',
  //'LEFT JOIN ovgispgercab igc ON igc.SeqGrupo=art.Codigo ',
  'LEFT JOIN ' + TabGerCab + ' igc ON igc.SeqGrupo=art.Codigo ',
  'WHERE igc.ZtatusIsp=' + Grl_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  SQL_AND,
  'GROUP BY art.Codigo ',
  'ORDER BY art.Nome ',
  EmptyStr]);
  //Grl_Geral.MB_Info(QrOViArtigos.SQL.Text);
end;

procedure TFmOViArtigos.SbPesquisaClick(Sender: TObject);
begin
  ReopenOViArtigos(FSegmentoInsp, FSeccaoInsp, EdPesquisa.Text);
end;

procedure TFmOViArtigos.SbSeccaoOPClick(Sender: TObject);
const
  sProcName = 'TFmOVdLocal.SbSeccaoOPClick()';
begin
  if FJaClicou then Exit;
  //
  FJaClicou := True;
  FMX_DmkPF.VibrarComoBotao();
  if FSeccaoInsp = TSeccaoInsp.sccinspTinturaria then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrSeccOP, Dmod.AllDB, [
   'SELECT igb.Codigo, igb.Controle, igb.SeccaoOP, igb.SeccaoMaq, ',
   'igb.QtReal, igc.NrOP, igc.SeqGrupo, igc.NrReduzidoOP, art.Nome, ',
   'fop.Ciclo, fop.Produto, prd.CodGrade, prd.CodTam',
   'FROM ovgitxgerbtl igb',
   'LEFT JOIN ovgitxgercab igc ON igc.Codigo=igb.Codigo',
   'LEFT JOIN ovdreferencia art ON art.Codigo=igc.SeqGrupo',
   'LEFT JOIN ovfordemproducao fop ON fop.NrReduzidoOP=igc.NrReduzidoOP',
   'LEFT JOIN ovdproduto prd ON prd.controle=fop.Produto',
   'WHERE igc.Local=' + Grl_Geral.FF0(FLocal),
   'AND fop.Local=' + Grl_Geral.FF0(FLocal),
   'AND igb.SeccaoOP = "' + Trim(EdPesquisa.Text) +'"',
    EmptyStr]);
    //
    if QrSeccOP.RecordCount > 0 then
    begin
      //
      FMX_DmkForms.DestroiFm_AllOS0(FmOViLocais);
      //
      case FSeccaoInsp of
        TSeccaoInsp.sccinspTinturaria:
          App_Jan.MostraFormOViTclCenSel(
          FSegmentoInsp, FSeccaoInsp, FLocal, QrSeccOPSeqGrupo.Value,
          //FArtigo, FNrOP, FNrReduzidoOP,
          QrSeccOPNrOP.Value, QrSeccOPNrReduzidoOP.Value,
          //FProduto, FCodGrade, FNO_Local,
          QrSeccOPProduto.Value, QrSeccOPCodGrade.Value, FNO_Local,
          //FNO_Artigo, FCodTam,
          QrSeccOPNome.Value, QrSeccOPCodTam.Value,
          // Batelada
          QrSeccOPControle.Value, QrSeccOPSeccaoOP.Value,
          QrSeccOPSeccaoMaq.Value, TStatusAndamentoInspecao.saiInspAberta, 0);
        else
          Grl_Geral.MB_Erro('SeccaoInsp n�o implementado em ' + sProcName);
      end;
      //
      Close;
    end else
    Grl_Geral.MB_Aviso(
    'Nenhum item foi encontrado!');
  end else
    Grl_Geral.MB_Aviso(
    'Somente a tinturaria permite pesquisa de OP de fornecedor!');
end;

end.
