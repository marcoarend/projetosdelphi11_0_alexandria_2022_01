unit OVdLocal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, FMX.Layouts, FMX.Objects;

type
  TFmOVdLocal = class(TForm)
    GrLocais: TGrid;
    QrOVdLocal: TFDQuery;
    QrOVdLocalCodigo: TIntegerField;
    QrOVdLocalNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOVdLocalBRK_Nome: TStringField;
    Layout1: TLayout;
    TitleLabel: TLabel;
    procedure GrLocaisCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure QrOVdLocalCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOVdLocal(ParteNome: String);
  end;

var
  FmOVdLocal: TFmOVdLocal;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars,
  Module, OViIspCenSnt, OViItxCenSnt;

{$R *.fmx}

procedure TFmOVdLocal.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOVdLocal.FormShow(Sender: TObject);
begin
  ReopenOVdLocal('');
end;

procedure TFmOVdLocal.GrLocaisCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOVdLocal.GrLocaisCellClick(';
  FechaForm = False;
begin
  VAR_LAST_LOCAL_COD := QrOVdLocalCodigo.Value;
  VAR_LAST_LOCAL_NOM := QrOVdLocalNome.Value;
  case FSeccaoInsp of
    TSeccaoInsp.sccinspTecelagem(*1024*)  :
      if FmOViIspCenSnt <> nil then
        FmOViIspCenSnt.CarregaOVmIspMobCabLoad(FechaForm);
    TSeccaoInsp.sccinspTinturaria(*2048*) :
      if FmOViItxCenSnt <> nil then
        FmOViItxCenSnt.CarregaOVmItxMobCabLoad(FechaForm);
    TSeccaoInsp.sccinspConfeccao(*3072*)  :
      if FmOViItxCenSnt <> nil then
        FmOViItxCenSnt.CarregaOVmItxMobCabLoad(FechaForm);
    else Grl_Geral.MB_Erro('FSeccaoInsp n�o definido em ' + sProcName);
  end;
  Close;
end;

procedure TFmOVdLocal.QrOVdLocalCalcFields(DataSet: TDataSet);
begin
  QrOVdLocalBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrLocais, 1, QrOVdLocalNome.Value);
end;

procedure TFmOVdLocal.ReopenOVdLocal(ParteNome: String);
var
  SQL_AND: String;
begin
  SQL_AND := '';
  if Trim(ParteNome) <> '' then
    SQL_AND := 'AND loc.Nome LIKE "%' + ParteNome + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrOVdLocal, Dmod.AllDB, [
  'SELECT loc.Codigo, loc.Nome ',
  'FROM ovdlocal loc ',
  'WHERE loc.Codigo <>0 ',
  SQL_AND,
  'GROUP BY loc.Codigo ',
  'ORDER BY loc.Nome ',
  '']);
end;

procedure TFmOVdLocal.SbPesquisaClick(Sender: TObject);
begin
  ReopenOVdLocal(EdPesquisa.Text);
end;

end.
