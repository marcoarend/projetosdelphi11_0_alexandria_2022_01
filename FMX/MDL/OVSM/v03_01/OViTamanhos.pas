unit OViTamanhos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, FMX.Layouts;

type
  TFmOViTamanhos = class(TForm)
    GrTamanhos: TGrid;
    QrOViTamanhos: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PnLocal: TPanel;
    Label1: TLabel;
    LaLocal: TLabel;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    QrOViTamanhosControle: TIntegerField;
    QrOViTamanhosROS: TLargeintField;
    QrOViTamanhosNrReduzidoOP: TIntegerField;
    QrOViTamanhosCodGrade: TIntegerField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOViTamanhosLastDtHr: TDateTimeField;
    QrOViTamanhosLastDtHr_BRK: TStringField;
    QrOViTamanhosCodTam: TWideMemoField;
    Layout1: TLayout;
    TitleLabel: TLabel;
    procedure GrTamanhosCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrOViTamanhosCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FJaClicou: Boolean;
  public
    { Public declarations }
    FLocal, FArtigo, FNrOP: Integer;
    FNO_Local, FNO_Artigo: String;
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOViTamanhos(SegmentoInsp: TSegmentoInsp; SeccaooInsp:
              TSeccaoInsp; ParteNome: String);
  end;

var
  FmOViTamanhos: TFmOViTamanhos;

implementation

uses UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan,
  UnFMX_DmkForms, UnFMX_DmkProcFunc, UnOVSM_PF,
  Module,
  OViLocais, OViArtigos, OViOPs, Principal;

{$R *.fmx}

procedure TFmOViTamanhos.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViTamanhos.FormShow(Sender: TObject);
begin
  FJaClicou := False;
  ReopenOViTamanhos(FSegmentoInsp, FSeccaoInsp, '');
end;

procedure TFmOViTamanhos.GrTamanhosCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOViTamanhos.GrTamanhosCellClick()';
begin
  if FJaClicou then Exit;
  //
  FJaClicou := True;
  if (QrOViTamanhos.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  //
  if FSeccaoInsp <> TSeccaoInsp.sccinspTinturaria then
  begin
    FMX_DmkForms.DestroiFm_AllOS0(FmOViLocais);
    FMX_DmkForms.DestroiFm_AllOS0(FmOViArtigos);
    FMX_DmkForms.DestroiFm_AllOS0(FmOViOPs);
  end;
  //
  case FSeccaoInsp of
    TSeccaoInsp.sccinspConfeccao:
      App_Jan.MostraFormOViIspCenSel(
      FSegmentoInsp, FSeccaoInsp, FLocal, FArtigo, FNrOP,
      QrOViTamanhosNrReduzidoOP.Value, (*Produto*)QrOViTamanhosControle.Value,
      QrOViTamanhosCodGrade.Value, FNO_Local, FNO_Artigo,
      QrOViTamanhosCodTam.Value, 0(*Batelada*), TStatusAndamentoInspecao.saiInspAberta, 0);
    //
    TSeccaoInsp.sccinspTecelagem:
      App_Jan.MostraFormOViTclCenSel(
      FSegmentoInsp, FSeccaoInsp, FLocal, FArtigo, FNrOP,
      QrOViTamanhosNrReduzidoOP.Value, (*Produto*)QrOViTamanhosControle.Value,
      QrOViTamanhosCodGrade.Value, FNO_Local, FNO_Artigo,
      QrOViTamanhosCodTam.Value, 0(*Batelada*), EmptyStr(*SeccaoOP*),
      EmptyStr(*SeccaoOP*), TStatusAndamentoInspecao.saiInspAberta, 0);
    //
    TSeccaoInsp.sccinspTinturaria:
      App_Jan.MostraFormOViBatelada(FSegmentoInsp, FSeccaoInsp, FLocal, FArtigo,
      FNrOP, QrOViTamanhosNrReduzidoOP.Value,
      (*Produto*)QrOViTamanhosControle.Value, QrOViTamanhosCodGrade.Value,
      FNO_Local, FNO_Artigo, QrOViTamanhosCodTam.Value);
    //
    else
      Grl_Geral.MB_Erro('SeccaoInsp n�o implementado em ' + sProcName);
  end;
  //
  Close;
end;

procedure TFmOViTamanhos.QrOViTamanhosCalcFields(DataSet: TDataSet);
var
  DtHr: String;
begin
  DtHr := Grl_Geral.FDT(QrOViTamanhosLastDtHr.Value, 0);
  QrOViTamanhosLastDtHr_BRK.Value := Copy(DtHr, 1, 11) +
   sLineBreak +  Copy(DtHr, 12);
end;

procedure TFmOViTamanhos.ReopenOViTamanhos(SegmentoInsp: TSegmentoInsp;
  SeccaooInsp: TSeccaoInsp; ParteNome: String);
var
  TabGerCab, TabMobCab, SQL_AND1, SQL_AND2: String;
begin
  TabGerCab := OVSM_PF.GetNomeTabSegmento_InspGerCab(FSegmentoInsp);
  TabMobCab := OVSM_PF.GetNomeTabSegmento_InspMobCab(FSegmentoInsp);
//ovfordemproducao
  SQL_AND1 := EmptyStr;
  SQL_AND2 := EmptyStr;
  //
  if FLocal <> 0 then
  begin
    SQL_AND1 := SQL_AND1 + 'AND igc.Local=' + Grl_Geral.FF0(FLocal) + sLineBreak;
    SQL_AND2 := SQL_AND2 + 'AND igc.Local=' + Grl_Geral.FF0(FLocal) + sLineBreak;
  end;
  //
  if FArtigo <> 0 then
  begin
    SQL_AND1 := SQL_AND1 + 'AND igc.SeqGrupo=' + Grl_Geral.FF0(FArtigo) + sLineBreak;
    SQL_AND2 := SQL_AND2 + 'AND igc.SeqGrupo=' + Grl_Geral.FF0(FArtigo) + sLineBreak;
  end;
  //
  if FNrOP <> 0 then
  begin
    SQL_AND1 := SQL_AND1 + 'AND igc.NrOP=' + Grl_Geral.FF0(FNrOP) + sLineBreak;
    SQL_AND2 := SQL_AND2 + 'AND igc.NrOP=' + Grl_Geral.FF0(FNrOP) + sLineBreak;
  end;
  //
  if Trim(ParteNome) <> EmptyStr then
  begin
    SQL_AND1 := SQL_AND1 + 'AND opr.CodTam LIKE "%' + ParteNome + '%"';
    //SQL_AND2 := SQL_AND2 + 'AND igc.CodTam LIKE "%' + ParteNome + '%"';
  end;
  //
(*
  Grl_DmkDB.AbreSQLQuery0(QrOViTamanhos, Dmod.AllDB, [
  'SELECT opr.Controle, opr.CodTam, opr.CodGrade, ',
  'COUNT(opr.Controle) ROS, igc.NrReduzidoOP ',
  'FROM ovdproduto opr ',
  'LEFT JOIN ovfordemproducao fop ON fop.Produto=opr.Controle ',
  'LEFT JOIN ovgispgercab igc ON igc.NrReduzidoOP=fop.NrReduzidoOP ',
  'WHERE igc.ZtatusIsp=' + Grl_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  SQL_AND,
  'GROUP BY opr.Controle ',
  'ORDER BY opr.CodTam ',
  EmptyStr]);
  //Grl_Geral.MB_Info(QrOViTamanhos.SQL.Text);
*)
  Grl_DmkDB.AbreSQLQuery0(QrOViTamanhos, Dmod.AllDB, [
  'DROP TABLE IF EXISTS _LOC_OP_TAMs_DISP_; ',
  'CREATE TABLE _LOC_OP_TAMs_DISP_ (Controle INTEGER, CodTam TEXT, CodGrade INTEGER, ROS INTEGER, NrReduzidoOP INTEGER); ',
  'INSERT INTO _LOC_OP_TAMs_DISP_ ',
  'SELECT opr.Controle, opr.CodTam, opr.CodGrade,  ',
  'COUNT(opr.Controle) ROS, igc.NrReduzidoOP  ',
  'FROM ovdproduto opr  ',
  'LEFT JOIN ovfordemproducao fop ON fop.Produto=opr.Controle  ',
  //'LEFT JOIN ovgispgercab igc ON igc.NrReduzidoOP=fop.NrReduzidoOP  ',
  'LEFT JOIN ' + TabGerCab + ' igc ON igc.NrReduzidoOP=fop.NrReduzidoOP  ',
  'WHERE igc.ZtatusIsp=' + Grl_Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  SQL_AND1,
  ' ',
  'GROUP BY opr.Controle  ',
  'ORDER BY opr.CodTam ; ',
  ' ',
  ' ',
  'DROP TABLE IF EXISTS _LOC_OP_TAMs_LAST_; ',
  'CREATE TABLE _LOC_OP_TAMs_LAST_ (CodTam TEXT, DtHrFecha DateTime NOT NULL DEFAULT "0000-00-00 00:00:00"); ',
  'INSERT INTO _LOC_OP_TAMs_LAST_ ',
  'SELECT CodTam, MAX(DtHrFecha) DtHrFecha ',
  //'FROM ovmispmobcab igc ',
  'FROM ' + TabMobCab + ' igc ',
  'WHERE igc.Local>0 ',
  SQL_AND2,
  'GROUP BY CodTam; ',
  ' ',
  ' ',
  'SELECT dsp.Controle, dsp.CodTam, dsp.CodGrade, dsp.ROS AS "ROS::BIGINT", ',
  'dsp.NrReduzidoOP, DateTime(lst.DtHrFecha) AS "LastDtHr::DATETIME"  ',
  'FROM _LOC_OP_TAMs_DISP_ dsp ',
  'LEFT JOIN _LOC_OP_TAMs_LAST_ lst ON lst.CodTam=dsp.CodTam ',
  EmptyStr]);
  //Grl_Geral.MB_Info(QrOViTamanhos.SQL.Text);

(*
SELECT MAX(DtHrFecha) DtHrFecha
FROM ovmispdevcab igc
WHERE igc.Local=5841
AND igc.SeqGrupo=37199
AND igc.NrOP=777
AND igc.CodTam="04"
*)

end;

procedure TFmOViTamanhos.SbPesquisaClick(Sender: TObject);
begin
  ReopenOViTamanhos(FSegmentoInsp, FSeccaoInsp, EdPesquisa.Text);
end;

//44 e 26 s�o melhores alturas para Panel>Edit e Label!

end.
