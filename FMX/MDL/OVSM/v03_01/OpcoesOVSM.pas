unit OpcoesOVSM;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit,
  UnDmkEnums, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, UnGrl_Vars, FMX.DialogService;

type
  TFmOpcoesOVSM = class(TForm)
    ToolBar1: TToolBar;
    TitleLabel: TLabel;
    PnScale: TPanel;
    Panel3: TPanel;
    BtOK: TButton;
    CdOVcMobDevCad: TClientDataSet;
    CdOVcMobDevCadCodigo: TIntegerField;
    CdOVcMobDevCadDeviceID: TStringField;
    CdOVcMobDevCadUserNmePdr: TStringField;
    CdOVcMobDevCadPIN: TStringField;
    CdOVcMobDevCadFoLocObjIns: TSmallintField;
    CdOVcMobDevCadScaleX: TIntegerField;
    CdOVcMobDevCadScaleY: TIntegerField;
    Button1: TButton;
    EdERPNameByCli: TEdit;
    Label3: TLabel;
    CdCtrlGeral: TClientDataSet;
    CdCtrlGeralERPNameByCli: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    LaScaleX: TLabel;
    EdScaleX: TEdit;
    LaScaleY: TLabel;
    EdScaleY: TEdit;
    Panel2: TPanel;
    Label1: TLabel;
    EdMailUser: TEdit;
    Panel4: TPanel;
    Label2: TLabel;
    Label4: TLabel;
    EdStepMedMin: TEdit;
    Label5: TLabel;
    EdStepMedMax: TEdit;
    GroupBox1: TGroupBox;
    RbFoLocObjIns_0: TRadioButton;
    RbFoLocObjIns_1: TRadioButton;
    GBEstiloForms: TGroupBox;
    RBAir: TRadioButton;
    RBAmakrits: TRadioButton;
    RBAquaGraphite: TRadioButton;
    RBBlend: TRadioButton;
    RBDark: TRadioButton;
    RBGoldenGraphite: TRadioButton;
    RBLight: TRadioButton;
    RBRubyGraphite: TRadioButton;
    RBTransparent: TRadioButton;
    RBNenhum: TRadioButton;
    Label6: TLabel;
    EdDefStpCasasDecim: TEdit;
    Panel1: TPanel;
    BtOpcoesAvancadas: TButton;
    BtSincroServer: TButton;
    Panel5: TPanel;
    BtNovaVersao: TButton;
    procedure BtOKClick(Sender: TObject);
    //
    procedure StyleRadioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdStepMedMinExit(Sender: TObject);
    procedure EdDefStpCasasDecimExit(Sender: TObject);
    procedure EdStepMedMaxExit(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure BtOpcoesAvancadasClick(Sender: TObject);
    procedure BtSincroServerClick(Sender: TObject);
    procedure BtNovaVersaoClick(Sender: TObject);
  private
    { Private declarations }
    FEstiloForms: String;
  public
    { Public declarations }
  end;

var
  FmOpcoesOVSM: TFmOpcoesOVSM;

implementation

uses UnOVSM_Vars, UnGrl_Geral, UnGrl_DmkDB, UnFMX_dmkRemoteQuery, UnApp_PF,
  UnFMX_DmkProcFunc, UnFMX_DmkForms, OpcoesAvancadas, UnFMX_DmkUnLic,
  Module;

{$R *.fmx}

procedure TFmOpcoesOVSM.BtNovaVersaoClick(Sender: TObject);
begin
  FMX_dmkUnLic.VerificaAtualizacaoVersao();
end;

procedure TFmOpcoesOVSM.BtOKClick(Sender: TObject);
var
  I, Codigo, ScaleX, ScaleY, FoLocObjIns, DefStpCasasDecim: Integer;
  SQLType: TSQLType;
  sSQL, ERPNameByCli, EstiloForms, MailUser: String;
  Component: TComponent;
  StepMedMin, StepMedMax: Double;
begin
  FMX_DmkPF.VibrarComoBotao();
  SQLType        := stNil;
  Codigo         := VAR_COD_DEVICE_IN_SERVER;
  ScaleX         := Grl_Geral.IMV(EdScaleX.Text);
  ScaleY         := Grl_Geral.IMV(EdScaleY.Text);
  FoLocObjIns    := 0;
  if ScaleX = 0 then
    ScaleX := 100;
  if ScaleY = 0 then
    ScaleY := 100;
  if RbFoLocObjIns_1.IsChecked then
    FoLocObjIns    := 1;
  //
  EstiloForms := FEstiloForms;
(*
  EstiloForms :=  '[Nenhum]';
  for I := 0 to FmOpcoesOVSM.ComponentCount - 1 do
  begin
    if FmOpcoesOVSM.Components[I] is TRadioButton then
      if (TRadioButton(FmOpcoesOVSM.Components[I]).GroupName) = Lowercase('EstiloForm') then
        if TRadioButton(FmOpcoesOVSM.Components[I]).IsChecked then
          EstiloForms := TRadioButton(FmOpcoesOVSM.Components[I]).Text;
  end;
*)

  //
  ERPNameByCli     := EdERPNameByCli.Text;
  MailUser         := EdMailUser.Text;
  StepMedMin       := Grl_Geral.DMV(EdStepMedMin.Text);
  StepMedMax       := Grl_Geral.DMV(EdStepMedMax.Text);
  DefStpCasasDecim := Grl_Geral.IMV(EdDefStpCasasDecim.Text);
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB,
  stUpd, 'ovcmobdevatv', False, [
  'FoLocObjIns', 'ScaleX', 'ScaleY'], [
  'Codigo'], [
  FoLocObjIns, ScaleX, ScaleY], [
  Codigo], False,
  TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    Dmod.ReopenOVSMOpcoes();
    if Dmod.QrOVSMOpcoes.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'ovsmopcoes', False, [
    'ERPNameByCli', 'EstiloForms', 'MailUser',
    'DefStpCasasDecim', 'StepMedMin', 'StepMedMax'], [
    'Codigo'], [
    ERPNameByCli, EstiloForms, MailUser,
    DefStpCasasDecim, StepMedMin, StepMedMax], [
    1], False,
    TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
    begin
      SQLType := stUpd;
      if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'ovcmobdevcad', False, [
      'FoLocObjIns', 'ScaleX', 'ScaleY'], [
      'Codigo'], [
      FoLocObjIns, ScaleX, ScaleY], [
      Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
      False, sSQL) then
      begin
        if FMX_dmkRemoteQuery.SQL_Executa(sSQL) then
        begin
          Dmod.ReopenOVSMOpcoes();
          Close;
        end;
      end;
    end;
  end;
end;

procedure TFmOpcoesOVSM.BtOpcoesAvancadasClick(Sender: TObject);
var
  Senha: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  Senha := '';
  TDialogService.InputQuery('Opções Avançadas', ['Informe a senha de acesso'], [Senha],
  procedure(const AResult: TModalResult; const AValues: array of string)
  begin
    if AResult = mrOk then
    begin
      Senha := AValues[0];
      if Senha = '82526' then
      begin
        FMX_DmkForms.CriaFm_AllOS0(TFmOpcoesAvancadas, FmOpcoesAvancadas,
          fcmCreateTryShwM, True, False);
      end else
        Grl_Geral.MB_Aviso('Senha incorreta');
    end;
  end);
end;

procedure TFmOpcoesOVSM.BtSincroServerClick(Sender: TObject);
var
  //Corda,
  Alias, SQL_JOIN, SQL_WHERE: String;
  Exclui: Boolean;
begin
  Alias := '';
  SQL_JOIN := '';
  Exclui := True;
  //
  FMX_DmkPF.VibrarComoBotao();
  BtSincroServer.Enabled := False;
  try
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    //  Desmarcar aqui!
    if not FMX_DmkUnLic.LiberaUso(VAR_CNPJ_DEVICE_IN_SERVER) then
      Exit;
    //InfoTempMemo('Preparando downloads...');
    SQL_WHERE := 'WHERE DeviceID="' + CdOVcMobDevCadDeviceID.Value + '"';
    //InfoTempMemo('Baixando configurações de inspeções', I, K);
    if App_PF.DownloadTabela('ovcmobdevcad', Alias, 'ovcmobdevcad', SQL_JOIN, SQL_WHERE, Exclui, True) then
      Exclui := False;
    //
    Dmod.ReopenOVcMobDevCad();
    Grl_Geral.MB_Info('Sincronização finalizada!');
  finally
    BtSincroServer.Enabled := True;
  end;
end;

procedure TFmOpcoesOVSM.EdDefStpCasasDecimExit(Sender: TObject);
begin
  VAR_MEDIDAS_CASAS_DECIMAIS := Grl_Geral.IMV(EdDefStpCasasDecim.Text);
end;

procedure TFmOpcoesOVSM.EdStepMedMaxExit(Sender: TObject);
var
  Valor: Double;
begin
  Valor := Grl_Geral.DMV(EdStepMedMax.Text);
  //EdStepMedMax.Text := Grl_Geral.FFT(Valor, VAR_MEDIDAS_CASAS_DECIMAIS, siNegativo);
  EdStepMedMax.Text := Grl_Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmOpcoesOVSM.EdStepMedMinExit(Sender: TObject);
var
  Valor: Double;
begin
  Valor := Grl_Geral.DMV(EdStepMedMin.Text);
  //EdStepMedMin.Text := Grl_Geral.FFT(Valor, VAR_MEDIDAS_CASAS_DECIMAIS, siNegativo);
  EdStepMedMin.Text := Grl_Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmOpcoesOVSM.FormCreate(Sender: TObject);
begin
  LaScaleX.Visible := False;
  LaScaleY.Visible := False;
  EdScaleX.Visible := False;
  EdScaleY.Visible := False;
end;

procedure TFmOpcoesOVSM.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOpcoesOVSM.FormShow(Sender: TObject);
var
  I: Integer;
  RB: TRadioButton;
begin
  FMX_dmkRemoteQuery.QuerySelect(CdOVcMobDevCad, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN, ',
      // Opcooes
      'ScaleX, ScaleY, FoLocObjIns ',
      // Fim Opcoes
      'FROM ovcmobdevcad',
      'WHERE Codigo=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER) + ''
    ], VAR_AMBIENTE_APP);
  if CdOVcMobDevCad.RecordCount > 0 then
  begin
    EdScaleX.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleX.Value);
    EdScaleY.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleY.Value);
    case CdOVcMobDevCadFoLocObjIns.Value of
      0: RbFoLocObjIns_0.IsChecked := True;
      1: RbFoLocObjIns_1.IsChecked := True;
    end;
  end else
  begin
    EdScaleX.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleX.Value);
    EdScaleY.Text := Grl_Geral.FF0(CdOVcMobDevCadScaleY.Value);
    RbFoLocObjIns_0.IsChecked := True;
  end;
  Dmod.ReopenOVSMOpcoes();
  RB := TRadioButton(FmOpcoesOVSM.FindComponent('RB' + Dmod.QrOVSMOpcoesEstiloForms.Value));
  if RB <> nil then
    RB.IsChecked := True;
{
  for I := 0 to FmOpcoesOVSM.ComponentCount - 1 do
  begin
    if FmOpcoesOVSM.Components[I] is TRadioButton then
      if (TRadioButton(FmOpcoesOVSM.Components[I]).GroupName) = Lowercase('EstiloForm') then
        if TRadioButton(FmOpcoesOVSM.Components[I]).Text =
        Dmod.QrOVSMOpcoesEstiloForms.Value then
          TRadioButton(FmOpcoesOVSM.Components[I]).IsChecked := True;
  end;
}


  //////////////////////////////////////////////////////////////////////////////
  ///
  FMX_dmkRemoteQuery.QuerySelect(CdCtrlGeral, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT ERPNameByCli ',
      'FROM ctrlgeral',
      'WHERE Codigo=1',
      EmptyStr
    ], VAR_AMBIENTE_APP);
  EdERPNameByCli.Text := CdCtrlGeralERPNameByCli.Value;
  EdMailUser.Text   := Dmod.QrOVSMOpcoesMailUser.Value;
  EdDefStpCasasDecim.Text := Grl_Geral.FF0(Dmod.QrOVSMOpcoesDefStpCasasDecim.Value);
  EdStepMedMin.Text := Grl_Geral.FFT(Dmod.QrOVSMOpcoesStepMedMin.Value, 2, siPositivo);
  EdStepMedMax.Text := Grl_Geral.FFT(Dmod.QrOVSMOpcoesStepMedMax.Value, 2, siPositivo);
end;

procedure TFmOpcoesOVSM.StyleRadioClick(Sender: TObject);
var
  I: Integer;
begin
  FEstiloForms := TRadioButton(Sender).Text;
  if FEstiloForms = RBNenhum.Text then
  begin
    if StyleBook <> nil then
      StyleBook := nil;
  end else
  begin
    for I := 0 to FmOpcoesOVSM.ComponentCount - 1 do
    begin
      if FmOpcoesOVSM.Components[I] is TStyleBook then
        if TStyleBook(FmOpcoesOVSM.Components[I]).Name = 'StyleBook_' +
        FEstiloForms then
        begin
          StyleBook := TStyleBook(FmOpcoesOVSM.Components[I]);
          Break;
        end;
    end;
  end;
{
  if RBNenhum.IsChecked then
  begin
    if StyleBook <> nil then
      StyleBook := nil;
  end else
  begin
    for I := 0 to FmOpcoesOVSM.ComponentCount - 1 do
    begin
      if FmOpcoesOVSM.Components[I] is TRadioButton then
        if (TRadioButton(FmOpcoesOVSM.Components[I]).GroupName) = Lowercase('EstiloForm') then
          if TRadioButton(FmOpcoesOVSM.Components[I]).IsChecked then
            EstiloForms := TRadioButton(FmOpcoesOVSM.Components[I]).Text;
    end;

    for I := 0 to FmOpcoesOVSM.ComponentCount - 1 do
    begin
      if FmOpcoesOVSM.Components[I] is TStyleBook then
        if TStyleBook(FmOpcoesOVSM.Components[I]).Name = 'StyleBook_' +
        EstiloForms then
        begin
          StyleBook := TStyleBook(FmOpcoesOVSM.Components[I]);
          Break;
        end;
    end;
  end;
}
end;

end.
