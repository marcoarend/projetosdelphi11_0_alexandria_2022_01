//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit OViIspCenOpn;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox, FMX.TabControl, FMX.Objects,
  MultiDetailAppearanceU, FMX.MobilePreview, FMX.Controls.Presentation,
  FMX.ListView.Adapters.Base, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.ScrollBox, FMX.Memo,
  UnDmkEnums, UnProjGroupEnums;

type
  TFmOViIspCenOpn = class(TForm)
    ToolBar1: TToolBar;
    ToggleEditMode: TSpeedButton;
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    SpeedButtonLiveBindings: TSpeedButton;
    ToolBar2: TToolBar;
    SpeedButtonFill: TSpeedButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOVmIspMobCab: TFDQuery;
    Img000: TImage;
    Img010: TImage;
    Img020: TImage;
    ImgErro: TImage;
    MeAvisos: TMemo;
    QrOVmIspMobCabNO_Local: TStringField;
    QrOVmIspMobCabNO_Artigo: TStringField;
    QrOVmIspMobCabCodTam: TStringField;
    QrOVmIspMobCabCodInMob: TIntegerField;
    QrOVmIspMobCabDeviceSI: TIntegerField;
    QrOVmIspMobCabDeviceID: TStringField;
    QrOVmIspMobCabDeviceCU: TIntegerField;
    QrOVmIspMobCabNome: TStringField;
    QrOVmIspMobCabOVgIspGer: TIntegerField;
    QrOVmIspMobCabLocal: TIntegerField;
    QrOVmIspMobCabNrOP: TIntegerField;
    QrOVmIspMobCabSeqGrupo: TIntegerField;
    QrOVmIspMobCabNrReduzidoOP: TIntegerField;
    QrOVmIspMobCabProduto: TIntegerField;
    QrOVmIspMobCabCodGrade: TIntegerField;
    QrOVmIspMobCabCodTam_1: TStringField;
    QrOVmIspMobCabDtHrAbert: TDateTimeField;
    QrOVmIspMobCabDtHrFecha: TDateTimeField;
    QrOVmIspMobCabOVcYnsMed: TIntegerField;
    QrOVmIspMobCabOVcYnsChk: TIntegerField;
    QrOVmIspMobCabOVcYnsARQ: TIntegerField;
    QrOVmIspMobCabLimiteChk: TIntegerField;
    QrOVmIspMobCabLimiteMed: TIntegerField;
    QrOVmIspMobCabQtReal: TFloatField;
    QrOVmIspMobCabQtLocal: TFloatField;
    QrOVmIspMobCabPecasIsp: TIntegerField;
    QrOVmIspMobCabPecaAtual: TIntegerField;
    QrOVmIspMobCabSubInspDone: TShortintField;
    QrOVmIspMobCabPontosTot: TIntegerField;
    QrOVmIspMobCabEnviadoWeb: TShortintField;
    QrOVmIspMobCabInspResul: TIntegerField;
    TitleLabel: TLabel;
    QrOVmIspMobCabCodigo: TIntegerField;
    QrCab: TFDQuery;
    QrCabNO_Local: TStringField;
    QrCabNO_Artigo: TStringField;
    QrCabCodInMob: TIntegerField;
    QrCabDeviceSI: TIntegerField;
    QrCabDeviceID: TStringField;
    QrCabDeviceCU: TIntegerField;
    QrCabNome: TStringField;
    QrCabOVgIspGer: TIntegerField;
    QrCabLocal: TIntegerField;
    QrCabNrOP: TIntegerField;
    QrCabSeqGrupo: TIntegerField;
    QrCabNrReduzidoOP: TIntegerField;
    QrCabProduto: TIntegerField;
    QrCabCodGrade: TIntegerField;
    QrCabCodTam: TStringField;
    QrCabDtHrAbert: TDateTimeField;
    QrCabDtHrFecha: TDateTimeField;
    QrCabOVcYnsMed: TIntegerField;
    QrCabOVcYnsChk: TIntegerField;
    QrCabOVcYnsARQ: TIntegerField;
    QrCabLimiteChk: TIntegerField;
    QrCabLimiteMed: TIntegerField;
    QrCabQtReal: TFloatField;
    QrCabQtLocal: TFloatField;
    QrCabPecasIsp: TIntegerField;
    QrCabPecaAtual: TIntegerField;
    QrCabSubInspDone: TShortintField;
    QrCabPontosTot: TIntegerField;
    QrCabInspResul: TIntegerField;
    QrCabEnviadoWeb: TShortintField;
    QrCabLk: TIntegerField;
    QrCabDataCad: TDateField;
    QrCabDataAlt: TDateField;
    QrCabUserCad: TIntegerField;
    QrCabUserAlt: TIntegerField;
    QrCabAlterWeb: TShortintField;
    QrCabAtivo: TShortintField;
    QrCabRandmStr: TStringField;
    QrCabEmpresa: TIntegerField;
    Img030: TImage;
    Img040: TImage;
    Img050: TImage;
    Img080: TImage;
    Img100: TImage;
    Img090: TImage;
    Img070: TImage;
    Img060: TImage;
    GroupBox1: TGroupBox;
    RGContinuar: TRadioButton;
    RGExcluir: TRadioButton;
    QrCabCodigo: TIntegerField;
    QrCabPerMedReal: TFloatField;
    QrCabPerChkReal: TFloatField;
    QrCabDtHrMailSnt: TFloatField;
    QrCabSegmntInsp: TIntegerField;
    QrCabSeccaoInsp: TIntegerField;
    QrCabBatelada: TIntegerField;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FStrInfoDone: String;
    //
    procedure CarregaOVmIspMobCabload(FechaForm: Boolean);
    function  DefineStepDezPercFeitoInspecao(MaxMed, MaxChk, PcInsp, Atual:
              Double): Integer;
  public
    { Public declarations }
  end;

var
  FmOViIspCenOpn: TFmOViIspCenOpn;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars, UnREST_SQL,
  UnFMX_DmkForms, UnFMX_DmkWeb, UnREST_App, UnFMX_DmkProcFunc, UnApp_Jan,
  Module;

procedure TFmOViIspCenOpn.CarregaOVmIspMobCabload(FechaForm: Boolean);
var
  LItem: TListViewItem;
  Linha2, Linha3: String;
  StepDezPerc: Integer;
begin
  ListViewMultiDetail.Items.Clear;
  //
  // Code to fill TListView
  ListViewMultiDetail.BeginUpdate;
  //if Dmod.AbreOVmIspMobCabload(QrOVmIspMobCab) then
  if Dmod.ReopenOVmIspMobCab(QrOVmIspMobCab, TStatusAndamentoInspecao.saiInspAberta) then
  begin
    QrOVmIspMobCab.First;
    while not QrOVmIspMobCab.Eof do
    begin
        LItem := ListViewMultiDetail.Items.Add;
        LItem.Tag  := QrOVmIspMobCabCodInMob.Value;
        LItem.Text := QrOVmIspMobCabNO_Local.Value;
        // Update data managed by custom appearance
(*
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := Format('%s', [QrOVmIspMobCabNO_Artigo.Value]);
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := Format('%s', ['OP ' + Grl_Geral.FF0(QrOVmIspMobCabNrOP.Value)]);
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := Format('%s', ['Tamanho: ' + QrOVmIspMobCabCodTam.Value]);
*)
        Linha2 := 'OP ' + Grl_Geral.FF0(QrOVmIspMobCabNrOP.Value) + '   ' +
                  'Tamanho: ' + QrOVmIspMobCabCodTam.Value;
        Linha3 := 'Inspe��o: ' + Grl_Geral.FF0(QrOVmIspMobCabCodigo.Value) +
                  '-' + Grl_Geral.FF0(QrOVmIspMobCabCodInMob.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 1, 11, QrOVmIspMobCabNO_Artigo.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 2, 11, Linha2);
        FMX_DmkPF.DefTextoLVMDI(LItem, 3, 11, Linha3);
        //
        StepDezPerc := DefineStepDezPercFeitoInspecao(
          //(*QtLocal,*) QrOVmIspMobCabQtLocal.Value,
          (*MaxMed, *) QrOVmIspMobCabLimiteMed.Value,
          (*MaxChk, *) QrOVmIspMobCabLimiteChk.Value,
          (*PcInsp, *) QrOVmIspMobCabPecasIsp.Value,
          (*Atual   *) QrOVmIspMobCabPecaAtual.Value);
        case StepDezPerc of
          0: LItem.BitmapRef := Img000.Bitmap;
          1: LItem.BitmapRef := Img010.Bitmap;
          2: LItem.BitmapRef := Img020.Bitmap;
          3: LItem.BitmapRef := Img030.Bitmap;
          4: LItem.BitmapRef := Img040.Bitmap;
          5: LItem.BitmapRef := Img050.Bitmap;
          6: LItem.BitmapRef := Img060.Bitmap;
          7: LItem.BitmapRef := Img070.Bitmap;
          8: LItem.BitmapRef := Img080.Bitmap;
          9: LItem.BitmapRef := Img090.Bitmap;
          10: LItem.BitmapRef := Img100.Bitmap;
          else LItem.BitmapRef := nil;
        end;
      //
      QrOVmIspMobCab.Next;
    end;
  end else
  if FechaForm then
    FMX_DmkForms.FechaFm_AllOS0(FmOViIspCenOpn);
end;

function TFmOViIspCenOpn.DefineStepDezPercFeitoInspecao(MaxMed,
  MaxChk, PcInsp, Atual: Double): Integer;
var
  Total, Perz: Double;
begin
  Result := 0;
  if (MaxMed > 0.1) and (MaxChk > 0.1) then
  begin
    if MaxMed > MaxChk then
      Total := MaxMed
    else
      Total := MaxChk;
    if Total > PcInsp then
      Total := PcInsp;
  end else
    Total := PcInsp;
  if Total = 0 then
    Result := 0
  else
  begin
    Perz := Atual / Total * 10;
    Result := Trunc(Perz);
  end;
end;

procedure TFmOViIspCenOpn.FormCreate(Sender: TObject);
begin
  ListViewMultiDetail.Items.Clear;
end;

procedure TFmOViIspCenOpn.FormShow(Sender: TObject);
begin
  CarregaOVmIspMobCabload(True);
end;

procedure TFmOViIspCenOpn.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  LItem: TListViewItem;
  CodInMob, Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade, Batelada:
  Integer;
  NO_Local, NO_Artigo, CodTam, sPergunta, CodInMob_TXT: String;
  SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp;
  //
begin
  FMX_DmkPF.VibrarComoBotao();
  LItem := TListViewItem(ListViewMultiDetail.Selected);
  if LItem <> nil then
  begin
    if RGContinuar.IsChecked = True then
    begin
      CodInMob := LItem.Tag;
      //CodInMob_TXT := Grl_Geral.FF0(CodInMob);
      Grl_DmkDB.AbreSQLQuery0(QrCab, Dmod.AllDB, [
      'SELECT loc.Nome NO_Local, art.Nome NO_Artigo,  ',
      'opr.CodTam,  ',
      'igc.* ',
      'FROM ovmispmobcab igc ',
      'LEFT JOIN ovdlocal loc ON loc.Codigo=igc.Local ',
      'LEFT JOIN ovdreferencia art ON art.Codigo=igc.SeqGrupo ',
      'LEFT JOIN ovdproduto opr ON opr.Controle=igc.Produto ',
      'WHERE igc.CodInMob=' + Grl_Geral.FF0(CodInMob),
      EmptyStr]);
      //
      Local         := QrCabLocal.Value;
      Artigo        := QrCabSeqGrupo.Value;
      NrOP          := QrCabNrOP.Value;
      NrReduzidoOP  := QrCabNrReduzidoOP.Value;
      Produto       := QrCabProduto.Value;
      CodGrade      := QrCabCodGrade.Value;
      CodTam        := QrCabCodTam.Value;
      NO_Local      := QrCabNO_Local.Value;
      NO_Artigo     := QrCabNO_Artigo.Value;
      Batelada      := QrCabBatelada.Value;
      SegmentoInsp  := TSegmentoInsp(QrCabSegmntInsp.Value);
      SeccaoInsp    := TSeccaoInsp(QrCabSeccaoInsp.Value);
      //
      App_Jan.MostraFormOViIspCenSel(SegmentoInsp, SeccaoInsp, Local, Artigo,
        NrOP, NrReduzidoOP, Produto, CodGrade, NO_Local, NO_Artigo, CodTam,
        Batelada, TStatusAndamentoInspecao.saiInspAberta, 0);
      //
      FMX_DmkForms.FechaFm_AllOS0(FmOViIspCenOpn);
    end else
    if RGExcluir.IsChecked = True then
    begin
      sPergunta := 'Deseja realmente excluir a inspe��o selecionada?';
      MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      procedure ExcluiItensDaTabela(Tabela: String);
        begin
          Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.AllDB, [
          'DELETE FROM ' + Lowercase(Tabela),
          'WHERE CodInMob=' + CodInMob_TXT,
          EmptyStr]);
        end;
      begin
        //MR := AResult;
        case AResult of
          mrYes:
          begin
            CodInMob := LItem.Tag;
            CodInMob_TXT := Grl_Geral.FF0(CodInMob);
            //
            ExcluiItensDaTabela('OVmIspMobMed');  // Itens de medidas da inspe��o mobile
            ExcluiItensDaTabela('OVmIspMobInc');  // Itens inconformes da inspe��o mobile
            ExcluiItensDaTabela('OVmIspMobLvr');  // Itens inconformes inesperados (digita��o livre) na inspe��o mobile
            ExcluiItensDaTabela('OVmIspMobFts');  // Fotos de itens inconformes na inspe��o mobile
            // O cabe�alho exclui por �ltimo!
            ExcluiItensDaTabela('OVmIspMobCab');  // Cabe�alho da inspe��o mobile
            //
            RGContinuar.IsChecked := True;
            CarregaOVmIspMobCabload(False);
          end;
          //mrNo: Grl_Geral.MB_Erro('Defin��o n�o realizada!');
          //mrCancel: Grl_Geral.MB_Erro('Defin��o Abortada!');
        end;
      end);
    end;
  end;
end;

procedure TFmOViIspCenOpn.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmOViIspCenOpn.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;

end.
