unit LoginApp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.Controls.Presentation, FMX.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, UnGrl_Vars,
  UnFMX_Grl_Vars, FMX.Styles, FMX.Layouts,
  UnDmkEnums, FMX.ScrollBox, FMX.Memo;

type
  TFmLoginApp = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    GridPanelLayout1: TGridPanelLayout;
    Panel2: TPanel;
    Label1: TLabel;
    EdPIN: TEdit;
    BtEntrar: TButton;
    Memo1: TMemo;
    procedure BtEntrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FTentativas: Integer;
    procedure   MyIntToHex();

  public
    { Public declarations }
  end;

var
  FmLoginApp: TFmLoginApp;

implementation

uses UnGrl_DmkDB, UnOVSM_Vars, UnFMX_Geral, UnFMX_DmkProcFunc, UnApp_Jan,
  UnGrl_Geral,
  Module, Principal;

{$R *.fmx}

procedure TFmLoginApp.BtEntrarClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  if Lowercase(EdPIN.Text) = Lowercase(VAR_PIN_DEVICE_IN_SERVER) then
  begin
    Dmod.ReopenOVSMOpcoes();
    //
    FmPrincipal.LayShow.Align := TAlignLayout.Client;
    FmPrincipal.LayShow.Visible := True;
    FmPrincipal.PnStart.Visible := False;
    Dmod.ReopenOVcMobDevCad();
    { Parei Aqui! Reativar?
    case FmPrincipal.FPushNotificationsAction of
      TAcaoDeInstrucaoDeNotificacaoMobile.ainmChamadoOcorrencia:
      begin
        App_Jan.MostraFormREST_Down(
          FmPrincipal.FPushNotificationsAction,
          FmPrincipal.FPushNotificationsCodigo);
      end;
      // ...
    end;
    }
    Close;
  end else
  begin
    FTentativas := FTentativas + 1;
    if FTentativas < 3 then
      FMX_Geral.MB_Aviso('PIN inv�lido!')
    else
    begin
      Application.Terminate;
    end;
  end;
end;

procedure TFmLoginApp.FormCreate(Sender: TObject);
begin
  FTentativas := 0;
  TitleLabel.Text := VAR_ERPNameByCli;
  //MyIntToHex();
end;

procedure TFmLoginApp.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmLoginApp.FormShow(Sender: TObject);
begin
  try
    EdPIN.SetFocus;
  except
    // nada!
  end;
end;

procedure TFmLoginApp.MyIntToHex;
var
  I: Integer;
  R, G, B: String;
begin
  for I := 0 to 255 do
    Memo1.Lines.Add(Grl_Geral.FFN(I, 3) + ' = ' + IntToHex(I));
  //
  R := IntToHex(70);
  G := IntToHex(130);
  B := IntToHex(181);

  //ShowMessage('#FF4682B5 = #FF' + R + G + B);
end;


end.
