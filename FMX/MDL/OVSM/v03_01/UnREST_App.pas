unit UnREST_App;

interface

uses System.Classes, System.UITypes, System.SysUtils, System.Variants,
  REST.Types, Data.DB, FireDAC.Comp.Client, TypInfo,
  UnProjGroupEnums, UnFMX_DmkWeb;

type
  TUnREST_App = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
(*
    function EnviaEmailResultadoInspecaoAntigo(const Inspecao: Integer; const
             EmailsExtra: String; const TipoEnvioEmailInspecao:
             TTipoEnvioEmailInspecao; var Retorno: String): Boolean;
*)
    function EnviaEmailResultadoInspecaoSegmento(const SegmentoInsp:
             TSegmentoInsp; const Inspecao: Integer; const EmailsExtra: String;
             const TipoEnvioEmailInspecao: TTipoEnvioEmailInspecao;
             var Retorno: String): Boolean;
  end;

var
  REST_App: TUnREST_App;

implementation

uses UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars, UnGrl_dmkWeb;

{ TUnREST_App }

{
function TUnREST_App.EnviaEmailResultadoInspecaoAntigo(const Inspecao: Integer; const
  EmailsExtra: String; const TipoEnvioEmailInspecao: TTipoEnvioEmailInspecao;
  var Retorno: String): Boolean;
var
  StatusCode: Integer;
  Url, Res, TipoEnvio: String;
begin
  Retorno := EmptyStr;
  Url := CO_URL + 'overseer/email/envia';
  StatusCode := 0;
  TipoEnvio := GetEnumName(TypeInfo(TTipoEnvioEmailInspecao),
    Integer(TipoEnvioEmailInspecao));
  Retorno := Grl_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN],
    ['cnpj', 'ambiente', 'inspecao', 'emailExtra', 'tipoEnvio'],
    [VAR_CNPJ_DEVICE_IN_SERVER, Grl_Geral.FF0(VAR_AMBIENTE_APP),
    Grl_Geral.FF0(Inspecao), EmailsExtra, TipoEnvio], Url, StatusCode, 'json');
  //
  Result := StatusCode = 200;
end;
}


function TUnREST_App.EnviaEmailResultadoInspecaoSegmento(
  const SegmentoInsp: TSegmentoInsp; const Inspecao: Integer;
  const EmailsExtra: String;
  const TipoEnvioEmailInspecao: TTipoEnvioEmailInspecao;
  var Retorno: String): Boolean;
const
  sProcName = 'TUnREST_App.EnviaEmailResultadoInspecaoSegmento()';
var
  StatusCode: Integer;
  Url, Rota, Res, TipoEnvio: String;
begin
  Retorno := EmptyStr;

  // Antigo
  //Rota := 'overseer/email/envia';
  // Novo
  case SegmentoInsp of
    TSegmentoInsp.sgminspFaccao: Rota := 'overseer/faccao/email/envia';
    TSegmentoInsp.sgminspTextil: Rota := 'overseer/textil/email/envia';
    else
    begin
      // Antigo
      Rota := 'overseer/email/envia';
      Grl_Geral.MB_Erro('SegmentoInsp n�o implementado em ' + sProcName)
    end;
  end;
  //
  Url := VAR_URL + Rota;
  StatusCode := 0;
  TipoEnvio := GetEnumName(TypeInfo(TTipoEnvioEmailInspecao),
    Integer(TipoEnvioEmailInspecao));
  Retorno := Grl_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN],
    ['cnpj', 'ambiente', 'inspecao', 'emailExtra', 'tipoEnvio'],
    [VAR_CNPJ_DEVICE_IN_SERVER, Grl_Geral.FF0(VAR_AMBIENTE_APP),
    Grl_Geral.FF0(Inspecao), EmailsExtra, TipoEnvio], Url, StatusCode, 'json');
  //
  Result := StatusCode = 200;
end;

end.
