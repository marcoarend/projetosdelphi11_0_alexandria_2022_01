unit OViBateladas;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, FMX.Layouts;

type
  TFmOViBateladas = class(TForm)
    GrBateladas: TGrid;
    QrOViBateladas: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PnLocal: TPanel;
    Label1: TLabel;
    LaLocal: TLabel;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Layout1: TLayout;
    TitleLabel: TLabel;
    PnTamanho: TPanel;
    Label4: TLabel;
    LaTamanho: TLabel;
    QrCorda: TFDQuery;
    QrOViBateladasCodigo: TIntegerField;
    QrOViBateladasControle: TIntegerField;
    QrOViBateladasOrdem: TIntegerField;
    QrOViBateladasSeccaoOP: TStringField;
    QrOViBateladasSeccaoMaq: TStringField;
    QrOViBateladasQtReal: TFloatField;
    procedure SbPesquisaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrOViBateladasCalcFields(DataSet: TDataSet);
    procedure GrBateladasCellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
    FJaClicou: Boolean;
  public
    { Public declarations }
    FLocal, FArtigo, FNrOP, FCodGrade, FNrReduzidoOP, FProduto: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure ReopenOViBateladas(SegmentoInsp: TSegmentoInsp; SeccaooInsp:
              TSeccaoInsp; ParteNome: String);
  end;

var
  FmOViBateladas: TFmOViBateladas;

implementation

uses UnGrl_Geral, UnGrl_DmkDB, UnOVS_Consts, UnOVSM_Vars, UnApp_Jan,
  UnFMX_DmkForms, UnFMX_DmkProcFunc, UnOVSM_PF,
  Module,
  OViLocais, OViArtigos, OViOPs, OViTamanhos, Principal;

{$R *.fmx}

procedure TFmOViBateladas.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViBateladas.FormShow(Sender: TObject);
begin
  FJaClicou := False;
  ReopenOViBateladas(FSegmentoInsp, FSeccaoInsp, '');
end;

procedure TFmOViBateladas.GrBateladasCellClick(const Column: TColumn;
  const Row: Integer);
const
  sProcName = 'TFmOViBateladas.GrBateladasCellClick()';
begin
  if FJaClicou then Exit;
  //
  FJaClicou := True;
  if (QrOViBateladas.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  //
  FMX_DmkForms.DestroiFm_AllOS0(FmOViLocais);
  FMX_DmkForms.DestroiFm_AllOS0(FmOViArtigos);
  FMX_DmkForms.DestroiFm_AllOS0(FmOViOPs);
  FMX_DmkForms.DestroiFm_AllOS0(FmOViTamanhos);
  //
  case FSeccaoInsp of
    TSeccaoInsp.sccinspTinturaria:
      App_Jan.MostraFormOViTclCenSel(
      FSegmentoInsp, FSeccaoInsp, FLocal, FArtigo, FNrOP, FNrReduzidoOP,
      FProduto, FCodGrade, FNO_Local, FNO_Artigo, FCodTam,
      QrOViBateladasControle.Value, QrOViBateladasSeccaoOP.Value,
      QrOViBateladasSeccaoMaq.Value, TStatusAndamentoInspecao.saiInspAberta, 0);
    else
      Grl_Geral.MB_Erro('SeccaoInsp n�o implementado em ' + sProcName);
  end;
  //
  Close;
end;

procedure TFmOViBateladas.QrOViBateladasCalcFields(DataSet: TDataSet);
var
  DtHr: String;
begin
{
  DtHr := Grl_Geral.FDT(QrOViTamanhosLastDtHr.Value, 0);
  QrOViTamanhosLastDtHr_BRK.Value := Copy(DtHr, 1, 11) +
   sLineBreak +  Copy(DtHr, 12);
}
end;

procedure TFmOViBateladas.ReopenOViBateladas(SegmentoInsp: TSegmentoInsp;
  SeccaooInsp: TSeccaoInsp; ParteNome: String);
var
  //TabGerCab, TabMobCab, SQL_AND1, SQL_AND2,
  Corda: String;
begin
  Grl_DmkDB.AbreSQLQuery0(QrCorda, Dmod.AllDB, [
  'SELECT Codigo ',
  'FROM ovgitxgercab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  EmptyStr]);
  //Grl_Geral.MB_SQL(self, QrCorda);
  if QrCorda.RecordCount > 0 then
  begin
    Corda := Grl_DmkDB.CordaDeQuery(QrCorda, 'Codigo');
    if Corda <> EmptyStr then
    begin
      Grl_DmkDB.AbreSQLQuery0(QrOViBateladas, Dmod.AllDB, [
      'SELECT Codigo, Controle, Ordem, SeccaoOP, SeccaoMaq, QtReal  ',
      'FROM ovgitxgerbtl  ',
      'WHERE Codigo IN (' + Corda + ')',
      'ORDER BY Ordem ',
      EmptyStr]);
      //
     if QrOViBateladas.RecordCount = 0 then
       Grl_Geral.MB_Aviso('N�o h� batelada cadastrada para inspe��o! [C]');
    end;
  end else
  begin
    GrBateladas.RowCount := 0;
     Grl_Geral.MB_Aviso('N�o h� batelada cadastrada para inspe��o [D]!');
  end;
  //Grl_Geral.MB_SQL(self, QrOViBateladas);
end;

procedure TFmOViBateladas.SbPesquisaClick(Sender: TObject);
begin
  ReopenOViBateladas(FSegmentoInsp, FSeccaoInsp, EdPesquisa.Text);
end;

//44 e 26 s�o melhores alturas para Panel>Edit e Label!

end.

