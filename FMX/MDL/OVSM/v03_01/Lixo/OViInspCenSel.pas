unit OViInspCenSel;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UnDmkEnums, System.Rtti,
  FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors, UnOVS_Consts,
  FMX.Gestures, FMX.DialogService, FMX.Edit, FMX.Memo,
  UnProjGroupEnums;

type
  TSubInspDone = (sidNadaPronto=0, sidMedidasFeitas=1, sidInconformidadesAvaliadas=2);
  TFmOViInspCenSel = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnLocal: TPanel;
    Label1: TLabel;
    LaLocal: TLabel;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel1: TPanel;
    Label5: TLabel;
    LaPtsNegativos: TLabel;
    Label7: TLabel;
    LaPtsAprovRepr: TLabel;
    Panel2: TPanel;
    PnItemAtual: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    PnInspecao: TPanel;
    QrOVgIspGerCab: TFDQuery;
    QrOVmIspMobCab: TFDQuery;
    QrOVgIspGerCabCodigo: TIntegerField;
    QrOVgIspGerCabNome: TStringField;
    QrOVgIspGerCabLocal: TIntegerField;
    QrOVgIspGerCabNrOP: TIntegerField;
    QrOVgIspGerCabSeqGrupo: TIntegerField;
    QrOVgIspGerCabNrReduzidoOP: TIntegerField;
    QrOVgIspGerCabDtHrAbert: TDateTimeField;
    QrOVgIspGerCabDtHrFecha: TDateTimeField;
    QrOVgIspGerCabOVcYnsMed: TIntegerField;
    QrOVgIspGerCabOVcYnsChk: TIntegerField;
    QrOVgIspGerCabOVcYnsARQ: TIntegerField;
    QrOVgIspGerCabLimiteChk: TIntegerField;
    QrOVgIspGerCabLimiteMed: TIntegerField;
    QrOVgIspGerCabZtatusIsp: TIntegerField;
    QrOVgIspGerCabZtatusDtH: TDateTimeField;
    QrOVgIspGerCabZtatusMot: TIntegerField;
    QrOVgIspGerCabPermiFinHow: TIntegerField;
    QrOVfOrdemProducao: TFDQuery;
    QrOVfOrdemProducaoNrReduzidoOP: TIntegerField;
    QrOVfOrdemProducaoEmpresa: TIntegerField;
    QrOVfOrdemProducaoCiclo: TIntegerField;
    QrOVfOrdemProducaoNrOP: TIntegerField;
    QrOVfOrdemProducaoLocal: TIntegerField;
    QrOVfOrdemProducaoProduto: TIntegerField;
    QrOVfOrdemProducaoSeqGrupo: TIntegerField;
    QrOVfOrdemProducaoTipoOP: TIntegerField;
    QrOVfOrdemProducaoPrioridade: TIntegerField;
    QrOVfOrdemProducaoNrSituacaoOP: TIntegerField;
    QrOVfOrdemProducaoDtInclusao: TDateField;
    QrOVfOrdemProducaoDtPrevisao: TDateField;
    QrOVfOrdemProducaoTipoLocalizacao: TIntegerField;
    QrOVfOrdemProducaoDtEntrada: TDateField;
    QrOVfOrdemProducaoQtReal: TFloatField;
    QrOVfOrdemProducaoQtLocal: TFloatField;
    QrOVfOrdemProducaoCodCategoria: TIntegerField;
    QrOVfOrdemProducaoNrLote: TIntegerField;
    QrOVfOrdemProducaoNrTipoProducaoOP: TStringField;
    QrOVfOrdemProducaoDtPrevRet: TDateField;
    QrOVfOrdemProducaoCodPessoa: TIntegerField;
    QrOVfOrdemProducaoReInsrt: TIntegerField;
    QrOVfOrdemProducaoLastInsrt: TDateTimeField;
    QrOVfOrdemProducaoDestino: TStringField;
    QrOVcYnsARQIts: TFDQuery;
    QrOVcYnsARQItsCodigo: TIntegerField;
    QrOVcYnsARQItsControle: TIntegerField;
    QrOVcYnsARQItsQtdPcIni: TIntegerField;
    QrOVcYnsARQItsQtdPcFim: TIntegerField;
    QrOVcYnsARQItsQtdAmostr: TIntegerField;
    QrOVcYnsARQItsPtsAprova: TFloatField;
    QrOVcYnsARQItsPtsResalv: TFloatField;
    QrOVcYnsARQItsPtsRejeit: TFloatField;
    QrOVmIspMobCabCodInMob: TIntegerField;
    QrOVmIspMobCabDeviceSI: TIntegerField;
    QrOVmIspMobCabDeviceID: TStringField;
    QrOVmIspMobCabDeviceCU: TIntegerField;
    QrOVmIspMobCabNome: TStringField;
    QrOVmIspMobCabOVgIspGer: TIntegerField;
    QrOVmIspMobCabLocal: TIntegerField;
    QrOVmIspMobCabNrOP: TIntegerField;
    QrOVmIspMobCabSeqGrupo: TIntegerField;
    QrOVmIspMobCabNrReduzidoOP: TIntegerField;
    QrOVmIspMobCabDtHrAbert: TDateTimeField;
    QrOVmIspMobCabDtHrFecha: TDateTimeField;
    QrOVmIspMobCabOVcYnsMed: TIntegerField;
    QrOVmIspMobCabOVcYnsChk: TIntegerField;
    QrOVmIspMobCabOVcYnsARQ: TIntegerField;
    QrOVmIspMobCabLimiteChk: TIntegerField;
    QrOVmIspMobCabLimiteMed: TIntegerField;
    QrOVmIspMobCabPecasIsp: TIntegerField;
    QrOVmIspMobCabPecaAtual: TIntegerField;
    QrItem: TFDQuery;
    QrItemNome: TStringField;
    QrItemTobiko: TIntegerField;
    QrItemTolerBasCalc: TShortintField;
    QrItemTolerUnMdida: TShortintField;
    QrItemTolerRndPerc: TFloatField;
    QrItemCodigo: TIntegerField;
    QrItemControle: TIntegerField;
    QrItemConta: TIntegerField;
    QrItemCodGrade: TIntegerField;
    QrItemCodTam: TStringField;
    QrItemMedidCer: TFloatField;
    QrItemMedidMax: TFloatField;
    QrItemMedidMin: TFloatField;
    QrItemCodInMob: TIntegerField;
    QrItemCtrlInMob: TIntegerField;
    QrItemPecaSeq: TIntegerField;
    QrItemOVcYnsMedDim: TIntegerField;
    QrItemMedidFei: TFloatField;
    QrItemMedidOky: TShortintField;
    QrItemPontNeg: TIntegerField;
    QrOVcYnsARQCad: TFDQuery;
    QrOVcYnsARQCadCodigo: TIntegerField;
    QrOVcYnsARQCadNome: TStringField;
    QrOVcYnsARQCadEsquema: TStringField;
    QrOVcYnsARQCadNivel: TStringField;
    QrOVcYnsARQCadLMQR: TStringField;
    QrOVcYnsARQCadNQA: TFloatField;
    QrOVcYnsARQCadPontosTole: TFloatField;
    QrOVcYnsARQCadPontosGrav: TFloatField;
    QrOVcYnsARQCadPontosCrit: TFloatField;
    QrPontosNeg: TFDQuery;
    QrPontosNegTabela: TWideStringField;
    QrPontosNegCtrlInMob: TLargeintField;
    QrPontosNegPecaSeq: TLargeintField;
    QrPontosNegNO_Topico: TWideStringField;
    QrPontosNegPontNeg: TLargeintField;
    Grid1: TGrid;
    BtContinua: TButton;
    BLPontosNeg: TBindingsList;
    BindSourceDB1: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    _TmContinua: TTimer;
    BtEncerra: TButton;
    QrOVmIspMobCabSubInspDone: TIntegerField;
    LaSubInspDone: TLabel;
    QrPontosNegBRK_Topico: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    BtFirst: TButton;
    BtNext: TButton;
    Label8: TLabel;
    LaQtLocal: TLabel;
    BtPrior: TButton;
    BtLast: TButton;
    BtPeca: TButton;
    PnPeca: TPanel;
    BtPcOK: TButton;
    Label10: TLabel;
    EdPeca: TEdit;
    PnNome: TPanel;
    Label11: TLabel;
    MeNome: TMemo;
    Label12: TLabel;
    LaPecasIsp: TLabel;
    QrPontosNegQtdFotos: TLargeintField;
    QrMax: TFDQuery;
    LaPodeEncerrar: TLabel;
    procedure BtContinuaClick(Sender: TObject);
    procedure _TmContinuaTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure Grid1DrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const Bounds: TRectF; const Row: Integer;
      const Value: TValue; const State: TGridDrawStates);
    procedure QrPontosNegCalcFields(DataSet: TDataSet);
    procedure Grid1CellClick(const Column: TColumn; const Row: Integer);
    procedure BtNextClick(Sender: TObject);
    procedure QrOVfOrdemProducaoAfterOpen(DataSet: TDataSet);
    procedure BtPriorClick(Sender: TObject);
    procedure BtFirstClick(Sender: TObject);
    procedure BtLastClick(Sender: TObject);
    procedure BtPecaClick(Sender: TObject);
    procedure BtPcOKClick(Sender: TObject);
    procedure QrPontosNegAfterOpen(DataSet: TDataSet);
    procedure QrOVcYnsARQItsAfterOpen(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrOVmIspMobCabAfterOpen(DataSet: TDataSet);
    procedure QrOVgIspGerCabAfterScroll(DataSet: TDataSet);
    procedure QrOVgIspGerCabBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ContinuaDeOndeParou();
    procedure EncerraInspecao(PecaAtual: Integer);
    function  ObtemPecasIsp(var PecasIsp: Integer): Boolean;
    procedure MostraItem(Item: Integer; SubInspDone: TSubInspDone);
    function  GetNome_SubInspDone(SubInspDone: Integer): String;
    function  GetID_SubInsDone(SubInsDone: String): Integer;
    procedure EncerraAntecipado();
    procedure ReopenTabelasAuxiliares();

  public
    { Public declarations }
    //FContinuar: Boolean;
    FLocal, FArtigo, FNrOP, FNrReduzidoOP, FProduto, FCodGrade, FCodigo: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    FStatusAndamentoInspecao: TStatusAndamentoInspecao;
    FFinalizada: Boolean;

    //
    procedure AtualizaInfoGeral();
    procedure EditaItemPontosNeg();
    function  IncluiNovaInspecao(var OVmIspMobCab: Integer): Boolean;
    function  LoacalizaAberto_QrOVmIspMobCab(const Proximo: Boolean; var
              CodInMob: Integer): Boolean;
    procedure MostraFormOViInspSeqNcfAct(IDTabela: Integer);
    procedure MostraTextoQuandoPodeEncerrar(Quando: Integer);
    procedure ReopenSeFinalizadoEEnviado();
    procedure ReopenPontosNeg();
    procedure VerificaSeExiste();
    function  VerificaSeJaTem(): Boolean;
  end;

var
  FmOViInspCenSel: TFmOViInspCenSel;

implementation

uses UnGrl_DmkDB, UnGrl_Geral, UnOVSM_Vars, UnFMX_DmkForms, UnFMX_DmkProcFunc,
  UnGrl_Vars, UnOVSM_PF,
  Module,
  OViInspSeqMed, OViInspSeqNcfChk, OViInspSeqNcfAct,
  OViInspSeqNcfLvr, OViInspSeqNcfTop, OViInspSeqMedEdit;

{$R *.fmx}

{ TFmOViInspCenSel }

procedure TFmOViInspCenSel.AtualizaInfoGeral();
var
  LimiIsp, LimiMed, LimiChk, PecasIsp: Integer;
begin
  LaLocal.Text        := FNO_Local;
  LaArtigo.Text       := FNO_Artigo;
  LaNrOP.Text         := Grl_Geral.FF0(FNrOP);
  LaTamCad.Text       := FCodTam;
(*
  LaPtsNegativos.Text := '';
  LaPtsAprovRepr.Text := '';
  LaPecasTot.Text     := '';
*)
  LaSubInspDone.Text  := GetNome_SubInspDone(QrOVmIspMobCabSubInspDone.Value);
  LaItemAtual.Text    := Grl_Geral.FF0(QrOVmIspMobCabPecaAtual.Value);
  LaPecasIsp.Text   := Grl_Geral.FF0(QrOVmIspMobCabPecasIsp.Value);
  PecasIsp := Trunc(QrOVmIspMobCabPecasIsp.Value);
  LimiIsp := PecasIsp;
  LimiMed := Trunc(QrOVmIspMobCabLimiteMed.Value);
  LimiChk := Trunc(QrOVmIspMobCabLimiteChk.Value);
  if (LimiMed > 0) and (LimiChk > 0) then
  begin
    if LimiMed > LimiChk then
      LimiIsp := LimiMed
    else
      LimiIsp := LimiChk;
  end;
  if LimiIsp > PecasIsp then
    LimiIsp := PecasIsp;
  //
  LaTotalItens.Text := Grl_Geral.FF0(LimiIsp);
end;

procedure TFmOViInspCenSel.BtContinuaClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  ContinuaDeOndeParou();
end;

procedure TFmOViInspCenSel.BtEncerraClick(Sender: TObject);
var
  //DtHrFecha, Nome, DtHrMailSnt,
  sPergunta: String;
  //CodInMob, PontosTot, InspResul,
  ItemAtual, MaxItWrtn, MaxItNavi: Integer;
  PerMedReal, PerChkReal: Double;
begin
  FMX_DmkPF.VibrarComoBotao();
  //Nome      := MeNome.Text;
  //DtHrFecha := Grl_Geral.FDT(Now(), 109);
  //CodInMob  := QrOVmIspMobCabCodInMob.Value;
  //PontosTot := 0;
  ItemAtual := Grl_Geral.IMV(LaItemAtual.Text);
  MaxItWrtn := QrPontosNegPecaSeq.Value;
  if ItemAtual > MaxItWrtn then
    MaxItNavi := ItemAtual
  else
    MaxItNavi := MaxItWrtn;
  if MaxItNavi < QrOVmIspMobCabPecasIsp.Value then
  begin
    sPergunta := 'Deseja realmente encerrar a inspe��o no item ' + Grl_Geral.FF0(
      MaxItNavi) + '?';
    MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        case AResult of
          mrYes: EncerraInspecao(MaxItNavi);
          //mrNo: Grl_Geral.MB_Erro('Encerramento n�o realizado!');
          //mrCancel: Grl_Geral.MB_Erro('Encerramento Abortado!');
        end;
      end);
  end else
    EncerraInspecao(QrOVmIspMobCabPecasIsp.Value);
end;

procedure TFmOViInspCenSel.BtFirstClick(Sender: TObject);
begin
  LaItemAtual.Text := '1';
  LaSubInspDone.Text := 'N';
end;

procedure TFmOViInspCenSel.BtLastClick(Sender: TObject);
begin
  LaItemAtual.Text := Grl_Geral.FF0(QrOVmIspMobCabPecaAtual.Value);
  LaSubInspDone.Text := 'N';
end;

procedure TFmOViInspCenSel.BtNextClick(Sender: TObject);
var
 Proximo: Integer;
begin
  Proximo := Grl_Geral.IMV(LaItemAtual.Text) + 1;
  if Proximo <= QrOVmIspMobCabPecaAtual.Value then
  begin
    LaItemAtual.Text := Grl_Geral.FF0(Proximo);
    LaSubInspDone.Text := 'N';
  end;
end;

procedure TFmOViInspCenSel.BtPcOKClick(Sender: TObject);
var
  Peca, TotalItens: Integer;
begin
  FMX_DmkPF.VibrarComoBotao();
  FMX_DmkPF.OcutaTecladoSeExibido();
  //
  if FMX_DmkPF.FIC(EdPeca.Text = EmptyStr, EdPeca, 'Informe a pe�a!') then Exit;
  //
  Peca := Grl_Geral.IMV(EdPeca.Text);
  if FMX_DmkPF.FIC(Peca < 1, EdPeca, 'A pe�a deve ser maior que zero!') then Exit;
  TotalItens := Grl_Geral.IMV(LaTotalItens.Text);
  if FMX_DmkPF.FIC(Peca > TotalItens, EdPeca,
    'A pe�a n�o pode ser maior que o total de pe�as!') then Exit;
  //
  LaItemAtual.Text   := Grl_Geral.FF0(Peca);
  LaSubInspDone.Text := 'N';
  PnPeca.Visible := False;
end;

procedure TFmOViInspCenSel.BtPecaClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
{
// o bot�o cancel n�o funciona!
  Titulo := 'Salto de pe�a';
  Pergunta := 'Informe a pe�a:';
  //
  TDialogService.InputQuery(Titulo, Pergunta, [Peca],
  procedure(const AResult: TModalResult; const AValues: array of string)
  begin
    if AResult = mrOk then
    begin
      Grl_Geral.MB_Aviso(AValues[0]);
    end else
      Close;
  end);
}
  PnPeca.Visible := True;
  if EdPeca.Enabled and EdPeca.Visible then
    EdPeca.SetFocus;
end;

procedure TFmOViInspCenSel.BtPriorClick(Sender: TObject);
var
 Anterior: Integer;
begin
  Anterior := Grl_Geral.IMV(LaItemAtual.Text) -1;
  if Anterior > 0 then
  begin
    LaItemAtual.Text := Grl_Geral.FF0(Anterior);
    LaSubInspDone.Text := 'N';
  end;
end;

procedure TFmOViInspCenSel.ContinuaDeOndeParou();
var
  SubInspDone, Item: Integer;
begin
  Item := Grl_Geral.IMV(LaItemAtual.Text);
  SubInspDone := GetID_SubInsDone(LaSubInspDone.Text);
  if Item = 0 then
    Item := 1;
  MostraItem(Item, TSubInspDone(SubInspDone));
  //
  //BtContinua.Text := 'Pr�xima pe�a';
  BtContinua.Text := 'Continua';
end;

procedure TFmOViInspCenSel.EditaItemPontosNeg();
begin
  LaItemAtual.Text := Grl_Geral.FF0(QrPontosNegPecaSeq.Value);
  if QrPontosNegTabela.Value = CO_MEM_FLD_TAB_0_Faccao_Inconformidade then
  begin
    LaSubInspDone.Text := 'M';
    MostraFormOViInspSeqNcfAct(0);
  end
  else
  if QrPontosNegTabela.Value = CO_MEM_FLD_TAB_1_Faccao_Medida then
  begin
    LaItemAtual.Text  := Grl_Geral.FF0(QrPontosNegPecaSeq.Value);
    LaSubInspDone.Text := 'N';
    ContinuaDeOndeParou();
  end
  else
  if QrPontosNegTabela.Value = CO_MEM_FLD_TAB_2_Faccao_LivreTexto then
  begin
    LaSubInspDone.Text := 'M';
    MostraFormOViInspSeqNcfAct(2);
  end;
end;

procedure TFmOViInspCenSel.EncerraAntecipado();
var
  SubInspDone, CodInMob, _CIM: Integer;
  Proximo: Boolean;
begin
  Proximo     := False;
  SubInspDone := 2; // 2 = Inconformidades avaliadas
  CodInMob    := QrOVmIspMobCabCodInMob.Value;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
  'SubInspDone'], [
  'CodInMob'], [
  SubInspDone], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    LoacalizaAberto_QrOVmIspMobCab(Proximo, _CIM);
    //
    //FMX_DmkForms.FechaFm_AllOS0(FmOViInspSeqNcfChk);
  end;
end;

procedure TFmOViInspCenSel.EncerraInspecao(PecaAtual: Integer);
var
  DtHrFecha, Nome, DtHrMailSnt: String;
  CodInMob, PontosTot, InspResul, PecasIsp, PecaSeq, LimiteMed, PecasToMed,
  ItnsToMed, TotalMedidasFeitas, TotalMedidasAFazer: Integer;
  PerMedReal, PerChkReal: Double;
  Val: Variant;
begin
  Nome      := MeNome.Text;
  DtHrFecha := Grl_Geral.FDT(Now(), 109);
  CodInMob  := QrOVmIspMobCabCodInMob.Value;
  PontosTot := 0;
  //
  QrPontosNeg.DisableControls;
  try
    QrPontosNeg.First;
    while not QrPontosNeg.Eof do
    begin
      PontosTot := PontosTot + QrPontosNegPontNeg.Value;
      //
      QrPontosNeg.Next;
    end;
    if PontosTot <= QrOVcYnsARQItsPtsAprova.Value then
      InspResul := CO_INSP_RESUL_1024_APROVADO
    else
    if PontosTot <= QrOVcYnsARQItsPtsResalv.Value then
      InspResul := CO_INSP_RESUL_2048_APR_RESALV
    else
    //if PontosTot >= QrOVcYnsARQItsPtsRejeit.Value then
      InspResul := CO_INSP_RESUL_3072_REJEITADO;
    //
    QrPontosNeg.First;
    //
    PecasIsp := QrOVmIspMobCabPecasIsp.Value;
    if PecasIsp = 0 then
    begin
      PerMedReal  := 0;
      PerChkReal  := 0;
    end else
    begin
(*
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.AllDB, [
      'SELECT MAX(PecaSeq) PecaSeq ',
      'FROM ovmispmobmed ',
      'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
      EmptyStr]);
      PecaSeq := 0;
      if QrMax.RecordCount > 0 then
      begin
        try
          Val := QrMax.FieldByName('PecaSeq').AsVariant;
          if Val <> Null then
            PecaSeq := Integer(Val);
        except
          PecaSeq := 0;
        end;
      end;
*)
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.AllDB, [
      'SELECT * ',
      'FROM ovmispmobmed ',
      'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
      EmptyStr]);
      TotalMedidasFeitas := QrMax.RecordCount;
      Grl_DmkDB.AbreSQLQuery0(QrMax, Dmod.AllDB, [
      'SELECT Controle ',
      'FROM ovcynsmedtop ',
      'WHERE Codigo=' + Grl_Geral.FF0(QrOVmIspMobCabOVcYnsMed.Value),
      EmptyStr]);
      ItnsToMed   := QrMax.RecordCount;
      LimiteMed   := QrOVmIspMobCabLimiteMed.Value;
      if (LimiteMed > 0) and (LimiteMed < PecasIsp) then
        PecasToMed := LimiteMed
      else
        PecasToMed := PecasIsp;
      TotalMedidasAFazer := ItnsToMed * PecasToMed;
      if TotalMedidasAFazer = 0 then
        PerMedReal  :=  0
      else
        //PerMedReal  :=  PecaSeq / PecasIsp * 100; //% de pe�as medidas
        PerMedReal  := TotalMedidasFeitas / TotalMedidasAFazer * 100; //% de pe�as medidas
      PerChkReal  := PecaAtual / PecasIsp * 100; //percentual de pe�as checadas
    end;
    DtHrMailSnt := '0000-00-00 00:00:00'; //data hora envio email
    //
    if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
    'Nome',
    'DtHrFecha', 'PontosTot', 'InspResul',
    'PecaAtual', 'PerMedReal', 'PerChkReal',
    'DtHrMailSnt'], [
    'CodInMob'], [
    Nome,
    DtHrFecha, PontosTot, InspResul,
    PecaAtual, PerMedReal, PerChkReal,
    DtHrMailSnt], [
    CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
    begin
      FMX_DmkForms.FechaFm_AllOS0(FmOViInspCenSel);
    end;
  finally
    QrPontosNeg.EnableControls;
  end;
end;

procedure TFmOViInspCenSel.FormCreate(Sender: TObject);
begin
  FFinalizada         := False;
  //
  BtPeca.Enabled      := False;
  EdPeca.Enabled      := False;
  PnPeca.Visible      := False;
  BtContinua.Visible  := False;
  BtEncerra.Enabled   := False;
  //
  FCodigo             := 0;
  //
  MostraTextoQuandoPodeEncerrar(0);
end;

procedure TFmOViInspCenSel.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
{ Resolve aqui mas gera erro no form principal!
  if Key = vkHardwareBack then
  begin
    Key := vkTab;
    BtEncerra.SetFocus;
  ////////////////////////////////////////////////////////////////////////////////
    EXIT;
  ////////////////////////////////////////////////////////////////////////////////
  end;
}
  try
    FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
  except
    on E: Exception do
      Grl_Geral.MB_Erro(E.Message);
  end;
end;

function TFmOViInspCenSel.GetID_SubInsDone(SubInsDone: String): Integer;
const
  sProcName = 'TFmOViInspCenSel.GetID_SubInsDone()';
begin
  Result := -2;
  if SubInsDone = 'N' then Result := 0 else
  if SubInsDone = 'M' then Result := 1 else
  if SubInsDone = 'I' then Result := 2 else
  begin
    Result := -1;
    Grl_Geral.MB_Erro('SubInsDone "' + SubInsDone + '" indefinido em ' +
    sProcName);
  end;
end;

function TFmOViInspCenSel.GetNome_SubInspDone(SubInspDone: Integer): String;
begin
  Result := '?';
  case TSubInspDone(SubInspDone) of
    TSubInspDone.sidNadaPronto:               Result := 'N';
    TSubInspDone.sidMedidasFeitas:            Result := 'M';
    TSubInspDone.sidInconformidadesAvaliadas: Result := 'I';
  end;
end;

procedure TFmOViInspCenSel.Grid1CellClick(const Column: TColumn;
  const Row: Integer);
var
  sPergunta: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  if FFinalizada then Exit;
  //
  if Row > -1 then
  begin
    sPergunta := 'Deseja editar o item "' + QrPontosNegNO_Topico.Value + '"?';
    MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        //MR := AResult;
        case AResult of
          mrYes: EditaItemPontosNeg();
          mrNo: Grl_Geral.MB_Info('Edi��o n�o realizada!');
          mrCancel: Grl_Geral.MB_Info('Edi��o Abortada!');
        end;
      end);
  end;
end;

procedure TFmOViInspCenSel.Grid1DrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const Bounds: TRectF;
  const Row: Integer; const Value: TValue; const State: TGridDrawStates);
var
  CurX, CurY, SpacePos, Lines, LineHt : integer;
  Sentence, CurWord : string;
  EOS : boolean;
begin
(*
  Grid1.Canvas.FillRect(Rect);
  Inc(Rect.Left, 2);
  CurX := Rect.Left;
  CurY := Rect.Top;
  Lines:= 1;
  Sentence := Grid1.Cells[Col, Row];
  LineHt:= Grid1.Canvas.TextHeight(Sentence);
  EOS := FALSE;
  while (not EOS) do
  begin
    SpacePos := Pos(' ', Sentence);
    if SpacePos > 0 then
    begin
      CurWord := Copy(Sentence, 0, SpacePos);
      Sentence := Copy(Sentence, SpacePos + 1,
                       Length(Sentence) - SpacePos);
    end
    else
    begin
      EOS := TRUE;
      CurWord := Sentence;
    end;
    with Grid1.Canvas do
    begin
      {If current word won't fit in the cell then move down
       to the next line and go back to the left margin }
      if (TextWidth(CurWord) + CurX) > Rect.Right then
      begin
        CurY := CurY + LineHt;
        CurX := Rect.Left;
        Lines:= Lines+1;
      end;
      TextOut(CurX, CurY, CurWord);
      CurX := CurX + TextWidth(CurWord);
    end;
  end;
  if (Grid1.RowHeights[Row] < (StdRowHeight +
     ((Lines-1) * LineHt))) then
    Grid1.RowHeights[Row]:= StdRowHeight +
      ((Lines-1) * LineHt);
*)
end;

function TFmOViInspCenSel.IncluiNovaInspecao(var OVmIspMobCab: Integer): Boolean;
var
  DeviceID, Nome, DtHrAbert, DtHrFecha, CodTam, RandmStr: String;
  //Codigo,
  CodInMob, DeviceSI, DeviceCU, OVgIspGer, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteChk, LimiteMed, PecasIsp,
  PecaAtual, PcIsp, Produto, CodGrade, SubInspDone, Empresa: Integer;
  QtReal, QtLocal: Double;
  SQLType: TSQLType;
begin
  Result         := False;
  SQLType        := stIns;
  //Codigo         := ;
  CodInMob       := 0;
  DeviceSI       := VAR_COD_DEVICE_IN_SERVER;
  DeviceID       := VAR_IMEI_DEVICE_IN_SERVER;
  DeviceCU       := 0; // current user. Ainda n�o usado
  Nome           := ''; // No fechammento?
  OVgIspGer      := QrOVgIspGerCabCodigo.Value;
  Local          := FLocal;
  NrOP           := FNrOP;
  SeqGrupo       := FArtigo;
  NrReduzidoOP   := FNrReduzidoOP;
  Produto        := FProduto;
  CodGrade       := FCodGrade;
  CodTam         := FCodTam;
  DtHrAbert      := Grl_Geral.FDT(Now(), 109);
  DtHrFecha      := Grl_Geral.FDT(0, 109);
  OVcYnsMed      := QrOVgIspGerCabOVcYnsMed.Value;
  OVcYnsChk      := QrOVgIspGerCabOVcYnsChk.Value;
  OVcYnsARQ      := QrOVgIspGerCabOVcYnsARQ.Value;
  LimiteChk      := QrOVgIspGerCabLimiteChk.Value;
  LimiteMed      := QrOVgIspGerCabLimiteMed.Value;
  QtReal         := QrOVfOrdemProducaoQtReal.Value;
  QtLocal        := QrOVfOrdemProducaoQtLocal.Value;
  PecasIsp       := 0; // Abaixo
  PecaAtual      := 1;
  SubInspDone    := 0;
  Empresa        := QrOVfOrdemProducaoEmpresa.Value;
  //
  if not ObtemPecasIsp(PecasIsp) then
    Exit;
  //
  RandmStr       := OVSM_PF.GetRandomStr();
  //
//  CodInMob := UMyMod.BPGS1I32('ovmispmobcab', 'CodInMob', '', '', tsPosNeg?, stInsUpd?, CodInMob);
  CodInMob := Grl_DmkDB.GetNxtCodigoInt('ovmispmobcab', 'CodInMob', SQLType, CodInMob);
  //if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovmispmobcab', auto_increment?[
  if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB, stIns, 'ovmispmobcab', False, [
  (*'Codigo',*)'DeviceSI', 'DeviceID',
  'DeviceCU', 'Nome', 'OVgIspGer',
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'QtReal', 'QtLocal',
  'Produto', 'CodGrade', 'CodTam',
  'DtHrAbert', 'DtHrFecha',
  'OVcYnsMed', 'OVcYnsChk', 'OVcYnsARQ',
  'LimiteChk', 'LimiteMed', 'SubInspDone',
  'PecasIsp', 'PecaAtual', 'RandmStr',
  'Empresa'], [
  'CodInMob'], [
  (*Codigo,*) DeviceSI, DeviceID,
  DeviceCU, Nome, OVgIspGer,
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, QtReal, QtLocal,
  Produto, CodGrade, CodTam,
  DtHrAbert, DtHrFecha,
  OVcYnsMed, OVcYnsChk, OVcYnsARQ,
  LimiteChk, LimiteMed, SubInspDone,
  PecasIsp, PecaAtual, RandmStr,
  Empresa], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    OVmIspMobCab := CodInMob;
    Result := OVmIspMobCab <> 0;
  end;
end;

function TFmOViInspCenSel.LoacalizaAberto_QrOVmIspMobCab(const Proximo: Boolean;
  var CodInMob: Integer): Boolean;
var
  Encerra, Chk, Med: Boolean;
  PtsNegativos, PermiFinHow: Integer;
begin
  Grl_DmkDB.AbreSQLQuery0(QrOVmIspMobCab, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovmispmobcab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  // 19-11-02 s� estava deixando um
  'AND CodGrade=' + Grl_Geral.FF0(FCodGrade),
  'AND CodTam="' + FCodTam + '"',
  //  Fim 19-11-02
  'AND DtHrFecha < "1900-01-01" ',
  EmptyStr]);
  //Grl_Geral.MB_Info(QrOVmIspMobCab.SQL.Text);
  Encerra := QrOVmIspMobCabPecaAtual.Value >= QrOVmIspMobCabPecasIsp.Value;
  if not Encerra then
  begin
    Chk :=
      (QrOVmIspMobCabPecaAtual.Value >= QrOVmIspMobCabLimiteChk.Value)
      and
      (QrOVmIspMobCabLimiteChk.Value > 0);
    Med :=
      (QrOVmIspMobCabPecaAtual.Value >= QrOVmIspMobCabLimiteMed.Value)
      and
      (QrOVmIspMobCabLimiteMed.Value > 0);
    //
    Encerra := Chk and Med;
  end;
  //
  if Encerra then
    Encerra := QrOVmIspMobCabSubInspDone.Value = 2;
  if Encerra then
  begin
    BtContinua.Visible  := False;
    BtEncerra.Enabled   := True;
    PnNome.Visible      := True;
  end else
  begin
    // pode encerrar antes?
(*
0 - Indefinido
1 - Somenteao finalizar toda inspe��o
2 - Ao atingir a pontua��o de reprova��o
3 - A qualquer momento
*)
    case  QrOVgIspGerCabPermiFinHow.Value of
      0: Encerra := False;
      1: Encerra := False;
      2:
      begin
        PtsNegativos := Grl_Geral.IMV(LaPtsNegativos.Text);
        Encerra := PtsNegativos > QrOVcYnsARQItsPtsResalv.Value;
      end;
      3: Encerra := True;
    end;
    PnNome.Visible      := False;
    BtEncerra.Enabled   := Encerra;
    BtContinua.Visible  := True;
  end;
  //
  CodInMob := QrOVmIspMobCabCodInMob.Value;
  Result := CodInMob <> 0;
  //
  if Proximo then
    CodInMob := CodInMob + 1;
  //
  AtualizaInfoGeral();
end;

procedure TFmOViInspCenSel.MostraFormOViInspSeqNcfAct(IDTabela: Integer);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqNcfAct, FmOViInspSeqNcfAct, fcmOnlyCreate, True, False) then
  begin
    FmOViInspSeqNcfAct.FLocal        := FLocal;
    FmOViInspSeqNcfAct.FArtigo       := FArtigo;
    FmOViInspSeqNcfAct.FNrOP         := FNrOP;
    FmOViInspSeqNcfAct.FNrReduzidoOP := FNrReduzidoOP;
    FmOViInspSeqNcfAct.FProduto      := FProduto;
    FmOViInspSeqNcfAct.FCodGrade     := FCodGrade;
    FmOViInspSeqNcfAct.FNO_Local     := FNO_Local;
    FmOViInspSeqNcfAct.FNO_Artigo    := FNO_Artigo;
    FmOViInspSeqNcfAct.FCodTam       := FCodTam;
    FmOViInspSeqNcfAct.FOVcYnsChk    := QrOVgIspGerCabOVcYnsChk.Value;
    FmOViInspSeqNcfAct.FPecaSeq      := QrPontosNegPecaSeq.Value;
    //
    //FmOViInspSeqNcfAct.LaLocal.Text     := NO_Local;
    FmOViInspSeqNcfAct.LaArtigo.Text      := FNO_Artigo;
    FmOViInspSeqNcfAct.LaNrOP.Text        := Grl_Geral.FF0(FNrOP);
    FmOViInspSeqNcfAct.LaTamCad.Text      := FCodTam;
    FmOViInspSeqNcfAct.LaItemAtual.Text   := Grl_Geral.FF0(QrPontosNegPecaSeq.Value);
    FmOViInspSeqNcfAct.LaTotalItens.Text  := Grl_Geral.FF0(QrOVmIspMobCabPecasIsp.Value);
    //
    FmOViInspSeqNcfAct.FNO_Topico         := QrPontosNegNO_Topico.Value;
    FmOViInspSeqNcfAct.LaNO_Topico.Text   := QrPontosNegNO_Topico.Value;
    FmOViInspSeqNcfAct.FTabela            := QrPontosNegTabela.Value;
    FmOViInspSeqNcfAct.FIDTabela          := IDTabela;
    FmOViInspSeqNcfAct.FCodInMob          := QrOVmIspMobCabCodInMob.Value;
    FmOViInspSeqNcfAct.FCtrlInMob         := QrPontosNegCtrlInMob.Value;
    //
    FmOViInspSeqNcfAct.BtAltera.Enabled := QrPontosNegTabela.Value = CO_MEM_FLD_TAB_2_Faccao_LivreTexto;

    //FmOViInspSeqNcfAct.ReopenItem();
    //
    FMX_DmkForms.ShowModal(FmOViInspSeqNcfAct);
  end;
end;

procedure TFmOViInspCenSel.MostraItem(Item: Integer; SubInspDone: TSubInspDone);
// = (sidNadaPronto=0, sidMedidasFeitas=1, sidInconformidadesAvaliadas=2););
var
  SID: TSubInspDone;
begin
  SID := SubInspDone;
  if Item <= QrOVmIspMobCabPecasIsp.Value then
  begin
    if (SID = TSubInspDOne.sidNadaPronto)
    or (SID = TSubInspDOne.sidInconformidadesAvaliadas) then
    begin
      // Medidas
      if (Item <= QrOVmIspMobCabLimiteMed.Value)
      or (QrOVmIspMobCabLimiteMed.Value = 0) then
      begin
        if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqMed, FmOViInspSeqMed, fcmOnlyCreate, True, False) then
        begin
          FmOViInspSeqMed.FLocal        := FLocal;
          FmOViInspSeqMed.FArtigo       := FArtigo;
          FmOViInspSeqMed.FNrOP         := FNrOP;
          FmOViInspSeqMed.FNrReduzidoOP := FNrReduzidoOP;
          FmOViInspSeqMed.FProduto      := FProduto;
          FmOViInspSeqMed.FCodGrade     := FCodGrade;
          FmOViInspSeqMed.FNO_Local     := FNO_Local;
          FmOViInspSeqMed.FNO_Artigo    := FNO_Artigo;
          FmOViInspSeqMed.FCodGrade     := FCodGrade;
          FmOViInspSeqMed.FCodTam       := FCodTam;
          FmOViInspSeqMed.FOVcYnsMed    := QrOVmIspMobCabOVcYnsMed.Value;
          FmOViInspSeqMed.FPecaSeq      := Item;
          //
          //FmOViInspSeqMed.LaLocal.Text  := NO_Local;
          FmOViInspSeqMed.LaArtigo.Text := FNO_Artigo;
          FmOViInspSeqMed.LaNrOP.Text   := Grl_Geral.FF0(FNrOP);
          FmOViInspSeqMed.LaTamCad.Text := FCodTam;
          FmOViInspSeqMed.LaItemAtual.Text := Grl_Geral.FF0(Item);
          FmOViInspSeqMed.LaTotalItens.Text := Grl_Geral.FF0(QrOVmIspMobCabPecasIsp.Value);
          //
          FmOViInspSeqMed.FCodInMob         := QrOVmIspMobCabCodInMob.Value;
          //
          FmOViInspSeqMed.ReopenItem(Item);
          //
          FMX_DmkForms.ShowModal(FmOViInspSeqMed);
        end;
      end else
      begin
        SID := TSubInspDOne.sidMedidasFeitas;
        BtPeca.Enabled := True;
        EdPeca.Enabled := True;
      end;
    end;
    if SID = TSubInspDOne.sidMedidasFeitas then
    begin
      // Inconformidades
      if (Item <= QrOVmIspMobCabLimiteChk.Value)
      or (QrOVmIspMobCabLimiteChk.Value = 0) then
      begin
        if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqNcfChk, FmOViInspSeqNcfChk, fcmOnlyCreate, True, False) then
        begin
          FmOViInspSeqNcfChk.FLocal        := FLocal;
          FmOViInspSeqNcfChk.FArtigo       := FArtigo;
          FmOViInspSeqNcfChk.FNrOP         := FNrOP;
          FmOViInspSeqNcfChk.FNrReduzidoOP := FNrReduzidoOP;
          FmOViInspSeqNcfChk.FProduto      := FProduto;
          FmOViInspSeqNcfChk.FCodGrade     := FCodGrade;
          FmOViInspSeqNcfChk.FNO_Local     := FNO_Local;
          FmOViInspSeqNcfChk.FNO_Artigo    := FNO_Artigo;
          FmOViInspSeqNcfChk.FCodGrade     := FCodGrade;
          FmOViInspSeqNcfChk.FCodTam       := FCodTam;
          FmOViInspSeqNcfChk.FOVcYnsChk    := QrOVmIspMobCabOVcYnsChk.Value;
          FmOViInspSeqNcfChk.FPecaSeq      := Item;
          if (Item = QrOVmIspMobCabPecasIsp.Value) then
          begin
            FmOViInspSeqNcfChk.FFecha := True;
            FmOViInspSeqNcfChk.BtNxtEnd.Text := 'Encerra';
          end else
          begin
            FmOViInspSeqNcfChk.FFecha := False;
            FmOViInspSeqNcfChk.BtNxtEnd.Text := 'Pr�ximo';
          end;
          //
          //FmOViInspSeqNcfChk.LaLocal.Text  := NO_Local;
          FmOViInspSeqNcfChk.LaArtigo.Text := FNO_Artigo;
          FmOViInspSeqNcfChk.LaNrOP.Text   := Grl_Geral.FF0(FNrOP);
          FmOViInspSeqNcfChk.LaTamCad.Text := FCodTam;
          FmOViInspSeqNcfChk.LaItemAtual.Text := Grl_Geral.FF0(Item);
          FmOViInspSeqNcfChk.LaTotalItens.Text := Grl_Geral.FF0(QrOVmIspMobCabPecasIsp.Value);
          //
          FmOViInspSeqNcfChk.FCodInMob         := QrOVmIspMobCabCodInMob.Value;
          //
          FmOViInspSeqNcfChk.ReopenItem();
          //
          FMX_DmkForms.ShowModal(FmOViInspSeqNcfChk);
        end;
      end else
        EncerraAntecipado();
    end;
  end;
end;

procedure TFmOViInspCenSel.MostraTextoQuandoPodeEncerrar(Quando: Integer);
begin
  LaPodeEncerrar.Text := ' Encerramento: ' + sCO_ENCERRA_INSPECAO[Quando];
end;

function TFmOViInspCenSel.ObtemPecasIsp(var PecasIsp: Integer): Boolean;
begin
  PecasIsp := QrOVcYnsARQItsQtdAmostr.Value;
  Result := PecasIsp > 0;
  if not Result then
  begin
    Grl_Geral.MB_Erro('Quantidade de pe�as a serem inspecionadas n�o definido!');
    FMX_DmkForms.FechaFm_AllOS0(FmOViInspCenSel);
  end;

  //QrOVgIspGerCab
  //ObtemPecasIsp(PecasIsp: Integer): Boolean;
end;

procedure TFmOViInspCenSel.QrOVcYnsARQItsAfterOpen(DataSet: TDataSet);
begin
  LaPtsAprovRepr.Text := Grl_Geral.FF0(Trunc(QrOVcYnsARQItsPtsAprova.Value)) +
  '/' + Grl_Geral.FF0(Trunc(QrOVcYnsARQItsPtsResalv.Value));
end;

procedure TFmOViInspCenSel.QrOVfOrdemProducaoAfterOpen(DataSet: TDataSet);
begin
  LaQtLocal.Text := Grl_Geral.FF0(Trunc(QrOVfOrdemProducaoQtLocal.Value));
end;

procedure TFmOViInspCenSel.QrOVgIspGerCabAfterScroll(DataSet: TDataSet);
begin
  MostraTextoQuandoPodeEncerrar(QrOVgIspGerCabPermiFinHow.Value);
end;

procedure TFmOViInspCenSel.QrOVgIspGerCabBeforeClose(DataSet: TDataSet);
begin
  MostraTextoQuandoPodeEncerrar(0);
end;

procedure TFmOViInspCenSel.QrOVmIspMobCabAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
  ItemAtu, LimiMed: Integer;
begin
  ItemAtu  := QrOVmIspMobCabPecaAtual.Value;
  LimiMed  := QrOVmIspMobCabLimiteMed.Value;
  Habilita := VAR_PERMITE_NAO_MEDIR or ((ItemAtu > LimiMed) and (LimiMed > 0));
  BtPeca.Enabled := Habilita;
  EdPeca.Enabled := Habilita;
end;

procedure TFmOViInspCenSel.QrPontosNegAfterOpen(DataSet: TDataSet);
var
  PontosTot: Integer;
begin
  PontosTot := 0;
  QrPontosNeg.DisableControls;
  try
    QrPontosNeg.First;
    while not QrPontosNeg.Eof do
    begin
      PontosTot := PontosTot + QrPontosNegPontNeg.Value;
      //
      QrPontosNeg.Next;
    end;
    LaPtsNegativos.Text := Grl_Geral.FF0(PontosTot);
  finally
    QrPontosNeg.EnableControls;
  end;
end;

procedure TFmOViInspCenSel.QrPontosNegCalcFields(DataSet: TDataSet);
begin
  QrPontosNegBRK_Topico.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(Grid1, 3, QrPontosNegNO_Topico.Value);
end;

procedure TFmOViInspCenSel.ReopenPontosNeg();
var
  CodInMob_TXT: String;
begin
  CodInMob_TXT := Grl_Geral.FF0(QrOVmIspMobCabCodInMob.Value);
  //
  //CodTab WideString
  Grl_DmkDB.AbreSQLQuery0(QrPontosNeg, Dmod.AllDB, [
  'SELECT "0" IdTabela, "' + CO_MEM_FLD_TAB_0_Faccao_Inconformidade + '" Tabela,  ',
  'mmc.CtrlInMob AS "CtrlInMob::BIGINT", mmc.PecaSeq AS "PecaSeq::BIGINT", ',
  'yqt.Nome NO_Topico, mmc.PontNeg AS "PontNeg::BIGINT", ',
  'mmc.QtdFotos AS "QtdFotos::BIGINT" ',
  'FROM ovmispmobinc mmc ',
  'LEFT JOIN ovcynsqsttop yqt ON yqt.Codigo=mmc.Topico ',
  'WHERE mmc.CodInMob=' + CodInMob_TXT,
  ' ',
  'UNION ',
  ' ',
  'SELECT  "1" IdTabela, "' + CO_MEM_FLD_TAB_1_Faccao_Medida + '" Tabela,  ',
  'mmm.CtrlInMob AS "CtrlInMob::BIGINT", mmm.PecaSeq AS "PecaSeq::BIGINT", ',
  'ygt.Nome NO_Topico, mmm.PontNeg AS "PontNeg::BIGINT", ',
  'mmm.QtdFotos AS "QtdFotos::BIGINT" ',
  'FROM ovmispmobmed mmm ',
  'LEFT JOIN ovcynsmeddim ymd ON ymd.Conta=mmm.ovcynsmeddim ',
  'LEFT JOIN ovcynsmedtop ymt ON ymt.Controle=ymd.Controle ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE mmm.CodInMob=' + CodInMob_TXT,
  'AND mmm.MedidOky=0 ',
  ' ',
  '  ',
  'UNION  ',
  '  ',
  'SELECT  "2" IdTabela, "' + CO_MEM_FLD_TAB_2_Faccao_LivreTexto + '" Tabela,   ',
  'mml.CtrlInMob AS "CtrlInMob::BIGINT", mml.PecaSeq AS "PecaSeq::BIGINT",  ',
  'mml.Descricao NO_Topico, mml.PontNeg AS "PontNeg::BIGINT", ',
  'mml.QtdFotos AS "QtdFotos::BIGINT" ',
  'FROM ovmispmoblvr mml  ',
  'WHERE mml.CodInMob=' + CodInMob_TXT,
  '  ',
  'ORDER BY PecaSeq DESC, IdTabela, CtrlInMob ',
  '']);
  //Grl_Geral.MB_SQL(FmOViInspCenSel, QrPontosNeg);
end;

procedure TFmOViInspCenSel.ReopenSeFinalizadoEEnviado;
begin
  if FCodigo > 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrOVmIspMobCab, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovmispmobcab ',
    'WHERE Codigo=' + Grl_Geral.FF0(FCodigo),
    EmptyStr]);
    if QrOVmIspMobCab.RecordCount > 0 then
    begin
      ReopenTabelasAuxiliares();
      ReopenPontosNeg()
    end;
  end;
end;

procedure TFmOViInspCenSel.ReopenTabelasAuxiliares();
var
  OVcYnsARQ_Txt, QtLocal_Txt: String;
  CodInMob: Integer;
begin
///////////////////// Configura��o de inspe��o: ovgispgercab ///////////////////
// *&*&*&*&*&*&*&*&*&
  Grl_DmkDB.AbreSQLQuery0(QrOVgIspGerCab, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovgispgercab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  EmptyStr]);
  //Grl_Geral.MB_SQL(FmOViInspCenSel, QrOVgIspGerCab);
  //
  ///////////// Reduzido da OP em produ��o: ovfordemproducao /////////////////
  Grl_DmkDB.AbreSQLQuery0(QrOVfOrdemProducao, Dmod.AllDB, [
  //'SELECT QtReal, QtLocal  ',
  'SELECT *  ',
  'FROM ovfordemproducao ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  'AND Produto=' + Grl_Geral.FF0(FProduto),
  //'/*  Empresa int(11) NOT NULL  DEFAULT 0, */ ',
  EmptyStr]);
  //
  ///////////// C�lculo de pe�as a inspecionar: OVcYnsARQIts /////////////////
  OVcYnsARQ_Txt := Grl_Geral.FF0(QrOVgIspGerCabOVcYnsARQ.Value);
  QtLocal_TXT   := Grl_Geral.FFT_Dot(QrOVfOrdemProducaoQtLocal.Value, 3, siNegativo);
  Grl_DmkDB.AbreSQLQuery0(QrOVcYnsARQIts, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovcynsarqits ',
  'WHERE Codigo=' + OVcYnsARQ_Txt,
  'AND ' + QtLocal_TXT
  + ' BETWEEN QtdPcIni AND QtdPcFim ',
  'ORDER BY QtdAmostr DESC ',
  EmptyStr]);
  //
  ///////////// C�lculo de pontos negativos em inspe��es: OVcYnsARQCad /////////////////
  OVcYnsARQ_Txt := Grl_Geral.FF0(QrOVgIspGerCabOVcYnsARQ.Value);
  QtLocal_TXT   := Grl_Geral.FFT_Dot(QrOVfOrdemProducaoQtLocal.Value, 3, siNegativo);
  Grl_DmkDB.AbreSQLQuery0(QrOVcYnsARQCad, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovcynsarqcad ',
  'WHERE Codigo=' + OVcYnsARQ_Txt,
  EmptyStr]);
  //
end;

procedure TFmOViInspCenSel._TmContinuaTimer(Sender: TObject);
var
  Item: Integer;
begin
{
  TmContinua.Enabled := False;
  if FContinuar then
  begin
    Item := Grl_Geral.IMV(LaItemAtual.Text);
    Item := Item + 1;
    MostraItem(Item);
  end;
}
end;

procedure TFmOViInspCenSel.VerificaSeExiste();
var
  CodInMob: Integer;
begin
  ReopenTabelasAuxiliares();
///////////////////// Verifica se existe: ovmispmobcab /////////////////////////
  if not LoacalizaAberto_QrOVmIspMobCab(False, CodInMob) then
  begin
    if FCodigo > 0 then Exit;
    //
    if IncluiNovaInspecao(CodInMob) then
      if not LoacalizaAberto_QrOVmIspMobCab(False, CodInMob) then
        FMX_DmkForms.FechaFm_AllOS0(FmOViInspCenSel);
    //
  end;
end;

function TFmOViInspCenSel.VerificaSeJaTem(): Boolean;
var
  sPergunta: String;
  Continua: Boolean;
begin
  Continua := False;
  Grl_DmkDB.AbreSQLQuery0(QrOVmIspMobCab, Dmod.AllDB, [
  'SELECT * ',
  'FROM ovmispmobcab ',
  'WHERE Local=' + Grl_Geral.FF0(FLocal),
  'AND SeqGrupo=' + Grl_Geral.FF0(FArtigo),
  'AND NrOP=' + Grl_Geral.FF0(FNrOP),
  'AND NrReduzidoOP=' + Grl_Geral.FF0(FNrReduzidoOP),
  'AND Produto=' + Grl_Geral.FF0(FProduto),
  'AND CodGrade=' + Grl_Geral.FF0(FCodGrade),
  'AND CodTam="' + FCodTam + '"',
  'AND DtHrFecha > "1900-01-01" ',
  'AND EnviadoWeb=0 ',
  EmptyStr]);
  if QrOVmIspMobCab.RecordCount > 0 then
  begin
    sPergunta := 'J� existe uma inspe��o finalizada mas n�o enviada ao servidor!' +
      sLineBreak + 'Deseja iniciar outra assim mesmo?';
    //
    MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      begin
        //MR := AResult;
        case AResult of
          mrYes: Continua := True;
          mrNo: Grl_Geral.MB_Info('Inspe��o n�o iniciada!');
          mrCancel: Grl_Geral.MB_Info('Inspe��o Abortada!');
        end;
      end);
  end else
    Continua := True;
  //
  if Continua then
    VerificaSeExiste();
  //
  Result := Continua;
end;

end.
