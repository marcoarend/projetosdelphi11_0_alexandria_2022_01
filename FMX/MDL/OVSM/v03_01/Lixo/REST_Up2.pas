//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit REST_Up2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox, FMX.TabControl, FMX.Objects,
  MultiDetailAppearanceU, FMX.MobilePreview, FMX.Controls.Presentation,
  FMX.ListView.Adapters.Base, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.ScrollBox, FMX.Memo,
  UnDmkEnums, UnProjGroupEnums, FMX.Layouts, FMX.Ani;

type
  TFmREST_Up = class(TForm)
    ToolBar1: TToolBar;
    ToggleEditMode: TSpeedButton;
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrAptosUp: TFDQuery;
    Img1024: TImage;
    Img2048: TImage;
    Img3072: TImage;
    ImgErro: TImage;
    QrAptosUpNO_Local: TStringField;
    QrAptosUpNO_Artigo: TStringField;
    QrAptosUpCodTam: TStringField;
    QrAptosUpCodInMob: TIntegerField;
    QrAptosUpDeviceSI: TIntegerField;
    QrAptosUpDeviceID: TStringField;
    QrAptosUpDeviceCU: TIntegerField;
    QrAptosUpNome: TStringField;
    QrAptosUpOVgIspGer: TIntegerField;
    QrAptosUpLocal: TIntegerField;
    QrAptosUpNrOP: TIntegerField;
    QrAptosUpSeqGrupo: TIntegerField;
    QrAptosUpNrReduzidoOP: TIntegerField;
    QrAptosUpProduto: TIntegerField;
    QrAptosUpCodGrade: TIntegerField;
    QrAptosUpCodTam_1: TStringField;
    QrAptosUpDtHrAbert: TDateTimeField;
    QrAptosUpDtHrFecha: TDateTimeField;
    QrAptosUpOVcYnsMed: TIntegerField;
    QrAptosUpOVcYnsChk: TIntegerField;
    QrAptosUpOVcYnsARQ: TIntegerField;
    QrAptosUpLimiteChk: TIntegerField;
    QrAptosUpLimiteMed: TIntegerField;
    QrAptosUpQtReal: TFloatField;
    QrAptosUpQtLocal: TFloatField;
    QrAptosUpPecasIsp: TIntegerField;
    QrAptosUpPecaAtual: TIntegerField;
    QrAptosUpSubInspDone: TShortintField;
    QrAptosUpPontosTot: TIntegerField;
    QrAptosUpEnviadoWeb: TShortintField;
    QrAptosUpInspResul: TIntegerField;
    QrCab: TFDQuery;
    QrCabCodInMob: TIntegerField;
    QrCabDeviceSI: TIntegerField;
    QrCabDeviceID: TStringField;
    QrCabDeviceCU: TIntegerField;
    QrCabNome: TStringField;
    QrCabOVgIspGer: TIntegerField;
    QrCabLocal: TIntegerField;
    QrCabNrOP: TIntegerField;
    QrCabSeqGrupo: TIntegerField;
    QrCabNrReduzidoOP: TIntegerField;
    QrCabProduto: TIntegerField;
    QrCabCodGrade: TIntegerField;
    QrCabCodTam: TStringField;
    QrCabDtHrAbert: TDateTimeField;
    QrCabDtHrFecha: TDateTimeField;
    QrCabOVcYnsMed: TIntegerField;
    QrCabOVcYnsChk: TIntegerField;
    QrCabOVcYnsARQ: TIntegerField;
    QrCabLimiteChk: TIntegerField;
    QrCabLimiteMed: TIntegerField;
    QrCabQtReal: TFloatField;
    QrCabQtLocal: TFloatField;
    QrCabPecasIsp: TIntegerField;
    QrCabPecaAtual: TIntegerField;
    QrCabSubInspDone: TShortintField;
    QrCabPontosTot: TIntegerField;
    QrCabInspResul: TIntegerField;
    QrCabEnviadoWeb: TShortintField;
    QrCabLk: TIntegerField;
    QrCabDataCad: TDateField;
    QrCabDataAlt: TDateField;
    QrCabUserCad: TIntegerField;
    QrCabUserAlt: TIntegerField;
    QrCabAlterWeb: TShortintField;
    QrCabAtivo: TShortintField;
    QrCabRandmStr: TStringField;
    FDPragma: TFDQuery;
    QrIts: TFDQuery;
    QrCabEmpresa: TIntegerField;
    TitleLabel: TLabel;
    LyEnvAll: TLayout;
    AniIndicator1: TAniIndicator;
    MeAvisos: TMemo;
    Rectangle1: TRectangle;
    Label1: TLabel;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FLItem: TListViewItem;
    FStrInfoDone: String;
    FContPontos: Integer;
    //
    procedure CarregaAptosUpload(FechaForm: Boolean);
    procedure InfoTempMemo(Texto: String);
    procedure InfoDoneMemo(Texto: String);
    //procedure InsereOVmIspDevCab(Codigo, CodInMob: Integer);
    function  UploadInspecaoCab(Codigo, CodInMob: Integer): Boolean;
    function  UploadInspecaoIts(Codigo, CodInMob: Integer; TabSrc, TabDst, Msg: String): Boolean;
    procedure UploadInspecao(); //Item: TListViewItem);
    //
    procedure MostraAniIndicator();
    procedure ThreadExecMostraAniIndicator();
    procedure ThreadExecUploadInspecao();
    //
    procedure ThreadExecEmAndoid();
    procedure MostraAniIndicatorEUpload();
    //
    procedure CreateThreads();
  public
    { Public declarations }
  end;

var
  FmREST_Up: TFmREST_Up;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars, UnREST_SQL,
  UnFMX_DmkForms, UnFMX_DmkWeb, UnREST_App, UnFMX_DmkProcFunc,
  Module,
  System.Threading, System.SyncObjs;

procedure TFmREST_Up.CarregaAptosUpload(FechaForm: Boolean);
var
  LItem: TListViewItem;
begin
  ListViewMultiDetail.Items.Clear;
  //
  // Code to fill TListView
  ListViewMultiDetail.BeginUpdate;
  //if Dmod.AbreAptosUpload(QrAptosUp) then
  if Dmod.ReopenOVmIspMobCab(QrAptosUp, TStatusAndamentoInspecao.saiInspRealizada) then
  begin
    QrAptosUp.First;
    while not QrAptosUp.Eof do
    begin
        LItem := ListViewMultiDetail.Items.Add;
        LItem.Tag  := QrAptosUpCodInMob.Value;
        LItem.Text := QrAptosUpNO_Local.Value;
        // Update data managed by custom appearance
  (*
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := Format('Detail1_%d', [QrAptosUpNO_Artigo.Value]);
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := Format('Detail2_%d', ['OP ' + Grl_Geral.FF0(QrAptosUpNrOP.Value)]);
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := Format('Detail3_%d', ['Tamanho: ' + QrAptosUpCodTam.Value]);
  *)
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := Format('%s', [QrAptosUpNO_Artigo.Value]);
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := Format('%s', ['OP ' + Grl_Geral.FF0(QrAptosUpNrOP.Value)]);
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := Format('%s', ['Tamanho: ' + QrAptosUpCodTam.Value]);
        case QrAptosUpInspResul.Value of
          1024: LItem.BitmapRef := Img1024.Bitmap;
          2048: LItem.BitmapRef := Img2048.Bitmap;
          3072: LItem.BitmapRef := Img3072.Bitmap;
          else
          begin

            LItem.BitmapRef := ImgErro.Bitmap;
          end;
        end;
      //
      QrAptosUp.Next;
    end;
  end else
  if FechaForm then
    FMX_DmkForms.FechaFm_AllOS0(FmREST_Up);
end;

procedure TFmREST_Up.CreateThreads();
var
  tasks: array of ITask; //Declare your dynamic array of ITask
  value: Integer;
begin
  Setlength (tasks ,2); // Set the size of the Array (How many threads you want to use in your case this would be 5)
  value := 0;
  //This the 1st thread because it is the thread located in position 1 of the array of tasks
  tasks[0] := TTask.Create (procedure ()
  begin
    //sleep (3000); // 3 seconds
    TAnimation.AniFrameRate := 10;
    //Timer1.Enabled := True;
    Label1.Visible := True;
    LyEnvAll.Visible := True;
    Rectangle1.Visible := True;
    AniIndicator1.Visible := True;
    AniIndicator1.Enabled := True;
    //
    TInterlocked.Add (value, 3000);
  end);
  tasks[0].Start;// until .Start is called your task is not executed
  // this starts the thread that is located in the position [0] of tasks

  //And this is the 2nd thread
  tasks[1] := TTask.Create (procedure ()
  begin
    //sleep (5000); // 5 seconds
    UploadInspecao();
    TInterlocked.Add (value, 5000);
  end);
  tasks[1].Start;// and the second thread is started
  TTask.WaitForAll(tasks); {This will have the mainthread wait for all the
  {threads of tasks to finish before moving making sure that when the
  showmessage All done is displayed all the threads are done}
  ShowMessage ('All done: ' + value.ToString);
end;

procedure TFmREST_Up.FormCreate(Sender: TObject);
begin
  FLItem := nil;
  ListViewMultiDetail.Items.Clear;
  LyEnvAll.Visible := False;
  //AniIndicator1.Visible := True;
  AniIndicator1.Enabled := False;
  Rectangle1.Align := TAlignLayout.Client;
  Rectangle1.Visible := False;//
end;

procedure TFmREST_Up.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmREST_Up.FormShow(Sender: TObject);
begin
  CarregaAptosUpload(True);
end;

procedure TFmREST_Up.InfoDoneMemo(Texto: String);
begin
  Application.ProcessMessages;
  FStrInfoDone := Grl_Geral.FDT(Now(), 109) + ' : ' +Texto + '!' + sLineBreak + FStrInfoDone;
  MeAvisos.Text := FStrInfoDone;
  Application.ProcessMessages;
end;

procedure TFmREST_Up.InfoTempMemo(Texto: String);
begin
  Application.ProcessMessages;
  MeAvisos.Text := Texto + '...' + sLineBreak + FStrInfoDone;
  Application.ProcessMessages;
end;

function TFmREST_Up.UploadInspecaoCab(Codigo, CodInMob: Integer): Boolean;
var
  DeviceID, Nome, DtHrAbert, DtHrFecha, CodTam, DataCad, DataAlt, EmailsExtras,
  Retorno: String;
  DeviceSI, DeviceCU, OVgIspGer, Local, NrOP, SeqGrupo, NrReduzidoOP,
  OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteChk, LimiteMed, PecasIsp, PecaAtual,
  Produto, CodGrade, PontosTot, InspResul, SubInspDone, EnviadoWeb, Lk, UserCad,
  UserAlt, AlterWeb, Ativo, InspeSeq, Empresa: Integer;
  QtReal, QtLocal: Double;
  SQLType: TSQLType;
  wSQL, JSON, sVal, RandmStr, DtHrUpIni, DtHrUpFim, IDInspecao_TXT, Msg: String;
  TipoEnvioEmailInspecao: TTipoEnvioEmailInspecao;
begin
  IDInspecao_TXT := 'Inspe��o ' + Grl_Geral.FF0(Codigo) + '. ';
  //
  Result        := False;
  //
  CodInMob      := QrCabCodInMob.Value;
  DeviceSI      := QrCabDeviceSI.Value;
  DeviceID      := QrCabDeviceID.Value;
  DeviceCU      := QrCabDeviceCU.Value;
  Nome          := QrCabNome.Value;
  OVgIspGer     := QrCabOVgIspGer.Value;
  Local         := QrCabLocal.Value;
  NrOP          := QrCabNrOP.Value;
  SeqGrupo      := QrCabSeqGrupo.Value;
  NrReduzidoOP  := QrCabNrReduzidoOP.Value;
  Produto       := QrCabProduto.Value;
  CodGrade      := QrCabCodGrade.Value;
  CodTam        := QrCabCodTam.Value;
  DtHrAbert     := Grl_Geral.FDT(QrCabDtHrAbert.Value, 109);
  DtHrFecha     := Grl_Geral.FDT(QrCabDtHrFecha.Value, 109);
  OVcYnsMed     := QrCabOVcYnsMed.Value;
  OVcYnsChk     := QrCabOVcYnsChk.Value;
  OVcYnsARQ     := QrCabOVcYnsARQ.Value;
  LimiteChk     := QrCabLimiteChk.Value;
  LimiteMed     := QrCabLimiteMed.Value;
  QtReal        := QrCabQtReal.Value;
  QtLocal       := QrCabQtLocal.Value;
  PecasIsp      := QrCabPecasIsp.Value;
  PecaAtual     := QrCabPecaAtual.Value;
  SubInspDone   := QrCabSubInspDone.Value;
  PontosTot     := QrCabPontosTot.Value;
  InspResul     := QrCabInspResul.Value;
  EnviadoWeb    := QrCabEnviadoWeb.Value;
  Lk            := QrCabLk.Value;
  DataCad       := Grl_Geral.FDT(QrCabDataCad.Value, 1);
  DataAlt       := Grl_Geral.FDT(QrCabDataAlt.Value, 1);
  UserCad       := QrCabUserCad.Value;
  UserAlt       := QrCabUserAlt.Value;
  AlterWeb      := QrCabAlterWeb.Value;
  Ativo         := QrCabAtivo.Value;
  RandmStr      := QrCabRandmStr.Value;
  DtHrUpIni     := Grl_Geral.FDT(Now(), 109);
  DtHrUpFim     := '0000-00-00 00:00:00';
  Empresa       := QrCabEmpresa.Value;
  //
  wSQL := Grl_Geral.ATS([
  'SELECT MAX(InspeSeq) InspeSeq ',
  'FROM ovmispdevcab ',
  'WHERE Local=' + Grl_Geral.FF0(Local),
  'AND Produto=' + Grl_Geral.FF0(Produto),
  'AND NrOP=' + Grl_Geral.FF0(NrOP),
  'AND CodTam="' + CodTam + '" ',
  'AND NrReduzidoOP=' + Grl_Geral.FF0(NrReduzidoOP),
  EmptyStr]);
  //Grl_Geral.MB_Info(wSQL);
  //
  InfoTempMemo(IDInspecao_TXT + 'Obtendo o ID sequencial da inspe��o no servidor');
  InspeSeq := FMX_DmkRemoteQuery.OVUS_I([wSQL]) + 1;
  //
  SQLType := stIns;
  InfoTempMemo(IDInspecao_TXT + 'Criando o cabe�alho no servidor');
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'ovmispdevcab', False, [
  'CodInMob', 'DeviceSI', 'DeviceID',
  'DeviceCU', 'Nome', 'OVgIspGer',
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
  'OVcYnsMed', 'OVcYnsChk', 'OVcYnsARQ',
  'LimiteChk', 'LimiteMed', 'PecasIsp',
  'PecaAtual', 'QtReal', 'QtLocal',
  'Produto', 'CodGrade', 'CodTam',
  'PontosTot', 'InspResul',
  (*'EnviadoWeb',*) 'InspeSeq', 'RandmStr',
  'DtHrUpIni', 'DtHrUpFim', 'Empresa'
  ], [
  'Codigo'], [
  CodInMob, DeviceSI, DeviceID,
  DeviceCU, Nome, OVgIspGer,
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, DtHrFecha,
  OVcYnsMed, OVcYnsChk, OVcYnsARQ,
  LimiteChk, LimiteMed, PecasIsp,
  PecaAtual, QtReal, QtLocal,
  Produto, CodGrade, CodTam,
  PontosTot, InspResul,
  (*EnviadoWeb,*) InspeSeq, RandmStr,
  DtHrUpIni, DtHrUpFim, Empresa
  ], [
  Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
  False, wSQL) then
  begin
    if FMX_dmkRemoteQuery.SQL_Executa(wSQL) then
    begin
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmobmed', 'ovmispdevmed',
      IDInspecao_TXT + 'Subindo medidas ao servidor') then
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmobinc', 'ovmispdevinc',
      IDInspecao_TXT + 'Subindo inconformidades cadastradas ao servidor') then
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmoblvr', 'ovmispdevlvr',
      IDInspecao_TXT + 'Subindo inconformidades extras ao servidor') then
      begin
        InfoDoneMemo(IDInspecao_TXT + 'Enviada ao servidor');
        EnviadoWeb := Integer(TStatusAndamentoInspecao.saiEnvDadosInspSvr); // 3
        if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
        'Codigo', 'EnviadoWeb'], ['CodInMob'], [
        Codigo, EnviadoWeb], [CodInMob], True,
        TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
        begin
          InfoTempMemo(IDInspecao_TXT + 'Info servidor: EnviadoWeb = ' + Grl_Geral.FF0(EnviadoWeb));
          wSQL := Grl_Geral.ATS([
            'UPDATE ovmispdevcab ',
            'SET DtHrUpFim="' + Grl_Geral.FDT(Now(), 109) + '" ',
            'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
            EmptyStr]);
          Result := FMX_dmkRemoteQuery.SQL_Executa(wSQL);
          InfoTempMemo(IDInspecao_TXT + 'Info servidor: FimUpd = ' + Grl_Geral.FDT(Now(), 109));
          if Result then
          begin
            Dmod.ReopenOVSMOpcoes();
            EmailsExtras := Dmod.QrOVSMOpcoesMailUser.Value;
            //
            InfoTempMemo(IDInspecao_TXT + 'Enviando email');
            if EmailsExtras <> EmptyStr then
              TipoEnvioEmailInspecao := TTipoEnvioEmailInspecao.smerAll
            else
              TipoEnvioEmailInspecao := TTipoEnvioEmailInspecao.smerEnti;
            //
            if REST_App.EnviaEmailResultadoInspecao(Codigo, EmailsExtras,
            TipoEnvioEmailInspecao, Retorno) then
            begin
              //
              ////////////////////////////////////////////////////////////////////
              ////////////////////////////////////////////////////////////////////
              // Desmarcar no futuro se houver mais etapas antes de finalizar a inspe��o
              //EnviadoWeb := Integer(TStatusAndamentoInspecao.saiEnvEmailInsp);  // 4
              EnviadoWeb := Integer(TStatusAndamentoInspecao.saiFinalizada);  // 5
              ////////////////////////////////////////////////////////////////////
              ////////////////////////////////////////////////////////////////////
              if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
              'EnviadoWeb'], ['CodInMob'], [
              EnviadoWeb], [CodInMob], True,
              TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
              begin
                // Mensagem de retono de envio
                InfoDoneMemo(IDInspecao_TXT + Retorno);
              end;
            end else
            // Mensagem de retono de erro
            InfoDoneMemo(IDInspecao_TXT + Retorno);
          end;
        end;
      end;
    end else
    begin
      InfoDoneMemo(IDInspecao_TXT + 'N�o foi poss�vel criar o cabe�alho!');
    end;
  end;
end;

procedure TFmREST_Up.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  LItem: TListViewItem;
begin
  FMX_DmkPF.VibrarComoBotao();
  LItem := TListViewItem(ListViewMultiDetail.Selected);
  if LItem <> nil then
  begin
    FLItem := LItem;
(*
{$IfDef ANDROID}
    //ThreadExecEmAndoid();
    //Timer1.Enabled := True;
    MostraAniIndicatorEUpload();
{$EndIf}
{$IfDef MSWINDOWS}
    ThreadExecMostraAniIndicator();
    ThreadExecUploadInspecao();
{$EndIf}
*)
{
    ThreadExecMostraAniIndicator();
    ThreadExecUploadInspecao();
}
    CreateThreads();
    //
    //UploadInspecao(LItem);
    //
  end;
end;

procedure TFmREST_Up.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmREST_Up.ThreadExecEmAndoid();
var
  aTask: ITask;
begin
  aTask := TTask.Create(MostraAniIndicatorEUpload);
  aTask.Start;
end;

procedure TFmREST_Up.ThreadExecMostraAniIndicator();
var
  aTask: ITask;
begin
  aTask := TTask.Create(MostraAniIndicator);
  aTask.Start;
end;

procedure TFmREST_Up.ThreadExecUploadInspecao();
var
  aTask: ITask;
begin
  aTask := TTask.Create(UploadInspecao);
  aTask.Start;
end;

procedure TFmREST_Up.MostraAniIndicator();
var
  I: Integer;
begin
  TAnimation.AniFrameRate := 10;
  //Timer1.Enabled := True;
  Label1.Visible := True;
  LyEnvAll.Visible := True;
  Rectangle1.Visible := True;
  AniIndicator1.Visible := True;
  AniIndicator1.Enabled := True;
end;

procedure TFmREST_Up.MostraAniIndicatorEUpload();
begin
{
  //MostraAniIndicator();
  Timer2.Enabled := True;
  TAnimation.AniFrameRate := 10;
  //Timer1.Enabled := True;
  Label1.Visible := True;
  LyEnvAll.Visible := True;
  Rectangle1.Visible := True;
  AniIndicator1.Visible := True;
  Application.ProcessMessages;
  AniIndicator1.Enabled := True;
  //
  Application.ProcessMessages;
  //
  //UploadInspecao();
  Timer1.Enabled := True;
}
end;

procedure TFmREST_Up.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;

procedure TFmREST_Up.UploadInspecao();
var
  CodInMob, Codigo, Conexao: Integer;
  SQLType: TSQLType;
  Msg, wSQL: String;
  LItem: TListViewItem;
begin
  InfoTempMemo('Iniciando processo');
  //LItem := TListViewItem(ListViewMultiDetail.Selected);
  LItem := FLItem;
  if LItem = nil then Exit;
  try
    InfoTempMemo('Item definido. Verificando internet.');
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    InfoTempMemo('Internet verificada.');
    CodInMob := LItem.Tag;
    InfoTempMemo('Gerando ID da inspe��o no servidor');
    Grl_DmkDB.AbreSQLQuery0(QrCab, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovmispmobcab ',
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
    EmptyStr]);
    //
    // Ver se o cabe�alho j� existe!
    wSQL := Grl_Geral.ATS([
    'SELECT Codigo ',
    'FROM ovmispdevcab ',
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
    'AND RandmStr="' + QrCabRandmStr.Value + '"',
    EmptyStr]);
    Codigo  := FMX_DmkRemoteQuery.OVUS_I([wSQL]);
    //
    if Codigo = 0 then
    begin
      SQLType := TSQLType.stIns;
      Codigo  := FMX_DmkRemoteQuery.BPGS1I32W('ovmispdevcab' , 'Codigo', '', '',
        tsPos, SQLType, (*Codigo*)0);
      InfoDoneMemo('ID da inspe��o gerada no servidor: ' + Grl_Geral.FF0(Codigo));
    end else
    begin
      //if
      FMX_dmkRemoteQuery.SQL_Executa([
      'DELETE FROM ovmispdevcab ',
      'WHERE Codigo=' + Grl_Geral.FF0(Codigo)
      ]);// then
    end;
    //
    if UploadInspecaoCab(Codigo, CodInMob) then
      CarregaAptosUpload(False);
  finally
    //Timer1.Enabled := False;
    //Timer2.Enabled := False;
    //
    //LyEnvAll.Visible := False;
    //Rectangle1.Visible := False;
    //AniIndicator1.Visible := False;
    //AniIndicator1.Enabled := False;
    //Label1.Visible := False;
    //Label1.Text := '';
  end;
end;

function TFmREST_Up.UploadInspecaoIts(Codigo, CodInMob: Integer;
  TabSrc, TabDst, Msg: String): Boolean;
(*
const
  Alias = '';
  SQL_JOIN = '';
*)
var
  sCodigo,
  Campos, Lit, (*SQL, _Alias,  MyAlias,*) JSON: String;
  //
  SQL_WHERE: String;
begin
  InfoTempMemo(Msg);
  //
  Result := False;
  sCodigo := Grl_Geral.FF0(Codigo);
  SQL_WHERE := 'WHERE CodInMod=' + Grl_Geral.FF0(CodInMob);
  //
  Campos := '';
(*
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
*)
  //
  Grl_DmkDB.AbreSQLQuery0(FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := MyAlias + FDPragma.FieldByName('Name').AsString;
  Lit := FDPragma.FieldByName('Name').AsString;
  FDPragma.Next;
  while not FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + MyAlias + FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    FDPragma.Next;
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(QrIts, Dmod.AllDB, [
    'SELECT ' + Lit,
    'FROM ' + Lowercase(TabSrc),
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
  EmptyStr]);
  QrIts.First;
  if FMX_dmkRemoteQuery.SQL_Executa([
  'DELETE FROM ' + TabDst,
  'WHERE Codigo=' + sCodigo
  ]) then
    Result :=
      REST_SQL.ValuesFromFDQuery(QrIts, TabDst, 'Codigo', sCodigo, Lit, 10000);
end;

{procedure TFmREST_Up.Timer2Timer(Sender: TObject);
var
  Str: String;
begin
  Application.ProcessMessages;
  if FContPontos = 5 then
    FContPontos := 1
  else
    FContPontos := FContPontos + 1;
  case FContPontos of
    1: Str := '.';
    2: Str := '..';
    3: Str := '...';
    4: Str := '....';
    5: Str := '.....';
    else Str := '.......';
  end;
  Label1.Text := MeAvisos.Lines[0] + Str;
  Application.ProcessMessages;
end;

}

end.
