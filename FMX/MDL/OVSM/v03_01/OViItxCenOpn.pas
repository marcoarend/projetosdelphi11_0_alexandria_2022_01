//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit OViItxCenOpn;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox, FMX.TabControl, FMX.Objects,
  MultiDetailAppearanceU, FMX.MobilePreview, FMX.Controls.Presentation,
  FMX.ListView.Adapters.Base, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.ScrollBox, FMX.Memo,
  UnDmkEnums, UnProjGroupEnums;

type
  TFmOViItxCenOpn = class(TForm)
    ToolBar1: TToolBar;
    ToggleEditMode: TSpeedButton;
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    SpeedButtonLiveBindings: TSpeedButton;
    ToolBar2: TToolBar;
    SpeedButtonFill: TSpeedButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrOVmItxMobCab: TFDQuery;
    Img000: TImage;
    Img010: TImage;
    Img020: TImage;
    ImgErro: TImage;
    MeAvisos: TMemo;
    QrOVmItxMobCabNO_Local: TStringField;
    QrOVmItxMobCabNO_Artigo: TStringField;
    QrOVmItxMobCabCodTam: TStringField;
    QrOVmItxMobCabCodInMob: TIntegerField;
    QrOVmItxMobCabDeviceSI: TIntegerField;
    QrOVmItxMobCabDeviceID: TStringField;
    QrOVmItxMobCabDeviceCU: TIntegerField;
    QrOVmItxMobCabNome: TStringField;
    QrOVmItxMobCabOVgItxGer: TIntegerField;
    QrOVmItxMobCabLocal: TIntegerField;
    QrOVmItxMobCabNrOP: TIntegerField;
    QrOVmItxMobCabSeqGrupo: TIntegerField;
    QrOVmItxMobCabNrReduzidoOP: TIntegerField;
    QrOVmItxMobCabProduto: TIntegerField;
    QrOVmItxMobCabCodGrade: TIntegerField;
    QrOVmItxMobCabCodTam_1: TStringField;
    QrOVmItxMobCabDtHrAbert: TDateTimeField;
    QrOVmItxMobCabDtHrFecha: TDateTimeField;
    QrOVmItxMobCabOVcYnsExg: TIntegerField;
    QrOVmItxMobCabQtReal: TFloatField;
    QrOVmItxMobCabQtLocal: TFloatField;
    QrOVmItxMobCabPecasItx: TIntegerField;
    QrOVmItxMobCabPecaAtual: TIntegerField;
    QrOVmItxMobCabSubInspDone: TShortintField;
    QrOVmItxMobCabPontosTot: TIntegerField;
    QrOVmItxMobCabEnviadoWeb: TShortintField;
    QrOVmItxMobCabInspResul: TIntegerField;
    TitleLabel: TLabel;
    QrOVmItxMobCabCodigo: TIntegerField;
    QrCab: TFDQuery;
    QrCabNO_Local: TStringField;
    QrCabNO_Artigo: TStringField;
    QrCabCodInMob: TIntegerField;
    QrCabDeviceSI: TIntegerField;
    QrCabDeviceID: TStringField;
    QrCabDeviceCU: TIntegerField;
    QrCabNome: TStringField;
    QrCabOVgItxGer: TIntegerField;
    QrCabLocal: TIntegerField;
    QrCabNrOP: TIntegerField;
    QrCabSeqGrupo: TIntegerField;
    QrCabNrReduzidoOP: TIntegerField;
    QrCabProduto: TIntegerField;
    QrCabCodGrade: TIntegerField;
    QrCabCodTam: TStringField;
    QrCabDtHrAbert: TDateTimeField;
    QrCabDtHrFecha: TDateTimeField;
    QrCabOVcYnsExg: TIntegerField;
    QrCabQtReal: TFloatField;
    QrCabQtLocal: TFloatField;
    QrCabPecasItx: TIntegerField;
    QrCabPecaAtual: TIntegerField;
    QrCabSubInspDone: TShortintField;
    QrCabPontosTot: TIntegerField;
    QrCabInspResul: TIntegerField;
    QrCabEnviadoWeb: TShortintField;
    QrCabLk: TIntegerField;
    QrCabDataCad: TDateField;
    QrCabDataAlt: TDateField;
    QrCabUserCad: TIntegerField;
    QrCabUserAlt: TIntegerField;
    QrCabAlterWeb: TShortintField;
    QrCabAtivo: TShortintField;
    QrCabRandmStr: TStringField;
    QrCabEmpresa: TIntegerField;
    Img030: TImage;
    Img040: TImage;
    Img050: TImage;
    Img080: TImage;
    Img100: TImage;
    Img090: TImage;
    Img070: TImage;
    Img060: TImage;
    GroupBox1: TGroupBox;
    RGContinuar: TRadioButton;
    RGExcluir: TRadioButton;
    QrCabCodigo: TIntegerField;
    QrCabPerExgReal: TFloatField;
    QrCabDtHrMailSnt: TFloatField;
    QrCabSegmntInsp: TIntegerField;
    QrCabSeccaoInsp: TIntegerField;
    QrCabBatelada: TIntegerField;
    QrCabSeccaoOP: TStringField;
    QrCabSeccaoMaq: TStringField;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FStrInfoDone: String;
    //
    procedure CarregaOVmItxMobCabload(FechaForm: Boolean);
    function  DefineStepDezPercFeitoInspecao(MaxMed, MaxChk, PcInsp, Atual:
              Double): Integer;
  public
    { Public declarations }
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
  end;

var
  FmOViItxCenOpn: TFmOViItxCenOpn;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars, UnREST_SQL,
  UnFMX_DmkForms, UnFMX_DmkWeb, UnREST_App, UnFMX_DmkProcFunc, UnApp_Jan,
  Module;

procedure TFmOViItxCenOpn.CarregaOVmItxMobCabload(FechaForm: Boolean);
var
  LItem: TListViewItem;
  Linha2, Linha3: String;
  StepDezPerc: Integer;
begin
  ListViewMultiDetail.Items.Clear;
  //
  // Code to fill TListView
  ListViewMultiDetail.BeginUpdate;
  //if Dmod.AbreOVmItxMobCabload(QrOVmItxMobCab) then
  if Dmod.ReopenOVmItxMobCab(QrOVmItxMobCab,
  TStatusAndamentoInspecao.saiInspAberta, FSeccaoInsp) then
  begin
    QrOVmItxMobCab.First;
    while not QrOVmItxMobCab.Eof do
    begin
        LItem := ListViewMultiDetail.Items.Add;
        LItem.Tag  := QrOVmItxMobCabCodInMob.Value;
        LItem.Text := QrOVmItxMobCabNO_Local.Value;
        // Update data managed by custom appearance
(*
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := Format('%s', [QrOVmItxMobCabNO_Artigo.Value]);
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := Format('%s', ['OP ' + Grl_Geral.FF0(QrOVmItxMobCabNrOP.Value)]);
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := Format('%s', ['Tamanho: ' + QrOVmItxMobCabCodTam.Value]);
*)
        Linha2 := 'OP ' + Grl_Geral.FF0(QrOVmItxMobCabNrOP.Value) + '   ' +
                  'Tamanho: ' + QrOVmItxMobCabCodTam.Value;
        Linha3 := 'Inspe��o: ' + Grl_Geral.FF0(QrOVmItxMobCabCodigo.Value) +
                  '-' + Grl_Geral.FF0(QrOVmItxMobCabCodInMob.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 1, 11, QrOVmItxMobCabNO_Artigo.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 2, 11, Linha2);
        FMX_DmkPF.DefTextoLVMDI(LItem, 3, 11, Linha3);
        //
        StepDezPerc := DefineStepDezPercFeitoInspecao(
          //(*QtLocal,*) QrOVmItxMobCabQtLocal.Value,
          (*MaxMed, *) 2, //QrOVmItxMobCabLimiteMed.Value,
          (*MaxChk, *) 2, //QrOVmItxMobCabLimiteChk.Value,
          (*PcInsp, *) QrOVmItxMobCabPecasItx.Value,
          (*Atual   *) QrOVmItxMobCabPecaAtual.Value);
        case StepDezPerc of
          0: LItem.BitmapRef := Img000.Bitmap;
          1: LItem.BitmapRef := Img010.Bitmap;
          2: LItem.BitmapRef := Img020.Bitmap;
          3: LItem.BitmapRef := Img030.Bitmap;
          4: LItem.BitmapRef := Img040.Bitmap;
          5: LItem.BitmapRef := Img050.Bitmap;
          6: LItem.BitmapRef := Img060.Bitmap;
          7: LItem.BitmapRef := Img070.Bitmap;
          8: LItem.BitmapRef := Img080.Bitmap;
          9: LItem.BitmapRef := Img090.Bitmap;
          10: LItem.BitmapRef := Img100.Bitmap;
          else LItem.BitmapRef := nil;
        end;
      //
      QrOVmItxMobCab.Next;
    end;
  end else
  if FechaForm then
    FMX_DmkForms.FechaFm_AllOS0(FmOViItxCenOpn);
end;

function TFmOViItxCenOpn.DefineStepDezPercFeitoInspecao(MaxMed,
  MaxChk, PcInsp, Atual: Double): Integer;
var
  Total, Perz: Double;
begin
  Result := 0;
  if (MaxMed > 0.1) and (MaxChk > 0.1) then
  begin
    if MaxMed > MaxChk then
      Total := MaxMed
    else
      Total := MaxChk;
    if Total > PcInsp then
      Total := PcInsp;
  end else
    Total := PcInsp;
  if Total = 0 then
    Result := 0
  else
  begin
    Perz := Atual / Total * 10;
    Result := Trunc(Perz);
  end;
end;

procedure TFmOViItxCenOpn.FormCreate(Sender: TObject);
begin
  ListViewMultiDetail.Items.Clear;
end;

procedure TFmOViItxCenOpn.FormShow(Sender: TObject);
begin
  CarregaOVmItxMobCabload(True);
end;

procedure TFmOViItxCenOpn.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  LItem: TListViewItem;
  CodInMob, Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade, Batelada:
  Integer;
  NO_Local, NO_Artigo, CodTam, sPergunta, CodInMob_TXT, SeccaoOP, SeccaoMaq:
  String;
  SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp;
  //
begin
  FMX_DmkPF.VibrarComoBotao();
  LItem := TListViewItem(ListViewMultiDetail.Selected);
  if LItem <> nil then
  begin
    if RGContinuar.IsChecked = True then
    begin
      CodInMob := LItem.Tag;
      //CodInMob_TXT := Grl_Geral.FF0(CodInMob);
      Grl_DmkDB.AbreSQLQuery0(QrCab, Dmod.AllDB, [
      'SELECT loc.Nome NO_Local, art.Nome NO_Artigo,  ',
      'opr.CodTam,  ',
      'igc.* ',
      'FROM ovmitxmobcab igc ',
      'LEFT JOIN ovdlocal loc ON loc.Codigo=igc.Local ',
      'LEFT JOIN ovdreferencia art ON art.Codigo=igc.SeqGrupo ',
      'LEFT JOIN ovdproduto opr ON opr.Controle=igc.Produto ',
      'WHERE igc.CodInMob=' + Grl_Geral.FF0(CodInMob),
      EmptyStr]);
      //
      Local         := QrCabLocal.Value;
      Artigo        := QrCabSeqGrupo.Value;
      NrOP          := QrCabNrOP.Value;
      NrReduzidoOP  := QrCabNrReduzidoOP.Value;
      Produto       := QrCabProduto.Value;
      CodGrade      := QrCabCodGrade.Value;
      CodTam        := QrCabCodTam.Value;
      NO_Local      := QrCabNO_Local.Value;
      NO_Artigo     := QrCabNO_Artigo.Value;
      Batelada      := QrCabBatelada.Value;
      SeccaoOP      := QrCabSeccaoOP.Value;
      SeccaoMaq     := QrCabSeccaoMaq.Value;
      SegmentoInsp  := TSegmentoInsp(QrCabSegmntInsp.Value);
      SeccaoInsp    := TSeccaoInsp(QrCabSeccaoInsp.Value);
      //
      App_Jan.MostraFormOViTclCenSel(SegmentoInsp, SeccaoInsp, Local, Artigo,
        NrOP, NrReduzidoOP, Produto, CodGrade, NO_Local, NO_Artigo, CodTam,
        Batelada, SeccaoOP, SeccaoMaq, TStatusAndamentoInspecao.saiInspAberta,
        0);
      //
      FMX_DmkForms.FechaFm_AllOS0(FmOViItxCenOpn);
    end else
    if RGExcluir.IsChecked = True then
    begin
      sPergunta := 'Deseja realmente excluir a inspe��o selecionada?';
      MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
      [
        System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo,
        System.UITypes.TMsgDlgBtn.mbCancel
      ], 0,
      procedure(const AResult: System.UITypes.TModalResult)
      procedure ExcluiItensDaTabela(Tabela: String);
        begin
          Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrUpd, Dmod.AllDB, [
          'DELETE FROM ' + Lowercase(Tabela),
          'WHERE CodInMob=' + CodInMob_TXT,
          EmptyStr]);
        end;
      begin
        //MR := AResult;
        case AResult of
          mrYes:
          begin
            CodInMob := LItem.Tag;
            CodInMob_TXT := Grl_Geral.FF0(CodInMob);
            //
            ExcluiItensDaTabela('OVmItxMobExg');  // Itens de exa��es da inspe��o mobile
            ExcluiItensDaTabela('OVmItxMobLvr');  // Itens inconformes inesperados (digita��o livre) na inspe��o mobile
            ExcluiItensDaTabela('OVmItxMobFts');  // Fotos de itens inconformes na inspe��o mobile
            // O cabe�alho exclui por �ltimo!
            ExcluiItensDaTabela('OVmItxMobCab');  // Cabe�alho da inspe��o mobile
            //
            RGContinuar.IsChecked := True;
            CarregaOVmItxMobCabload(False);
          end;
          //mrNo: Grl_Geral.MB_Erro('Defin��o n�o realizada!');
          //mrCancel: Grl_Geral.MB_Erro('Defin��o Abortada!');
        end;
      end);
    end;
  end;
end;

procedure TFmOViItxCenOpn.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmOViItxCenOpn.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;

end.
