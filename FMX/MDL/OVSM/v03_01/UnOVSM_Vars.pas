unit UnOVSM_Vars;

interface

uses UnDmkEnums, FireDAC.Comp.Client, FMX.Memo;

type
  TUnOVSM_Vars = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
  end;
var
{
  // dados para uso no mobile - Id no server
  VAR_COD_DEVICE_IN_SERVER: Integer = 0;
  VAR_PIN_DEVICE_IN_SERVER: String = '';
  VAR_NIK_DEVICE_IN_SERVER: String = 'Usu�rio';
  VAR_CNPJ_DEVICE_IN_SERVER: String = '';
  VAR_IMEI_DEVICE_IN_SERVER: String = '';
  // dados para uso no mobile - opcoes do mobile
  VAR_OPCOES_MOB_FoLocObjIns: Integer = 0;
  //VAR_OPCOES_MOB_ScaleX: Double = 1.0;
  //VAR_OPCOES_MOB_ScaleY: Double = 1.0;
}
  VAR_MEDIDAS_CASAS_DECIMAIS: Integer = 1;
  VAR_MEDIDAS_INCREMENTO_MIN: Double = 0.1;
  VAR_MEDIDAS_INCREMENTO_MAX: Double = 0.5;
  VAR_FOTO_COD_IN_MOB: Integer = 0;
  VAR_FOTO_CTRL_IN_MOB: Integer = 0;
  VAR_FOTO_ID_TABELA: Integer = 0;
  VAR_PERMITE_NAO_MEDIR: Boolean = False;
  // TProtocoloEnvioDeImagem = (pdediIndef=0, pdediHTTP=1, pdediFTP=2, pdediREST=3);
  VAR_PROTOCOLO_ENVIO_DE_IMAGEM: TProtocoloEnvioDeImagem = pdediREST;
  //
  VAR_LAST_LOCAL_COD: Integer = 0;
  VAR_LAST_LOCAL_NOM: String = '';

implementation

end.
