﻿//---------------------------------------------------------------------------

// This software is Copyright (c) 2015 Embarcadero Technologies, Inc.
// You may only use this software if you are an authorized licensee
// of an Embarcadero developer tools product.
// This software is considered a Redistributable as defined under
// the software license agreement that comes with the Embarcadero Products
// and is subject to that software license agreement.

//---------------------------------------------------------------------------

unit REST_Up_Faccao;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.StdCtrls, FMX.ListView, Data.Bind.GenData,
  Fmx.Bind.GenData, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.ObjectScope, FMX.ListBox, FMX.TabControl, FMX.Objects,
  MultiDetailAppearanceU, FMX.MobilePreview, FMX.Controls.Presentation,
  FMX.ListView.Adapters.Base, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.ScrollBox, FMX.Memo,
  //
  System.NetEncoding, System.IOUtils, FMX.Surfaces, FMX.Graphics,
  //////////////////////////////////////////////////////////////////////////////
  //Vcl.Graphics, Só dá para usar no projeto VCL chamando units FMX !!!
  //////////////////////////////////////////////////////////////////////////////
  UnDmkEnums, UnProjGroupEnums, FMX.Layouts, FMX.Ani, Soap.EncdDecd;

type
  TFmREST_Up_Faccao = class(TForm)
    ToolBar1: TToolBar;
    ToggleEditMode: TSpeedButton;
    PrototypeBindSource1: TPrototypeBindSource;
    BindingsList1: TBindingsList;
    ListViewMultiDetail: TListView;
    LinkFillControlToField1: TLinkFillControlToField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrAptosUp: TFDQuery;
    Img1024: TImage;
    Img2048: TImage;
    Img3072: TImage;
    ImgErro: TImage;
    QrAptosUpNO_Local: TStringField;
    QrAptosUpNO_Artigo: TStringField;
    QrAptosUpCodTam: TStringField;
    QrAptosUpCodInMob: TIntegerField;
    QrAptosUpDeviceSI: TIntegerField;
    QrAptosUpDeviceID: TStringField;
    QrAptosUpDeviceCU: TIntegerField;
    QrAptosUpNome: TStringField;
    QrAptosUpOVgIspGer: TIntegerField;
    QrAptosUpLocal: TIntegerField;
    QrAptosUpNrOP: TIntegerField;
    QrAptosUpSeqGrupo: TIntegerField;
    QrAptosUpNrReduzidoOP: TIntegerField;
    QrAptosUpProduto: TIntegerField;
    QrAptosUpCodGrade: TIntegerField;
    QrAptosUpCodTam_1: TStringField;
    QrAptosUpDtHrAbert: TDateTimeField;
    QrAptosUpDtHrFecha: TDateTimeField;
    QrAptosUpOVcYnsMed: TIntegerField;
    QrAptosUpOVcYnsChk: TIntegerField;
    QrAptosUpOVcYnsARQ: TIntegerField;
    QrAptosUpLimiteChk: TIntegerField;
    QrAptosUpLimiteMed: TIntegerField;
    QrAptosUpQtReal: TFloatField;
    QrAptosUpQtLocal: TFloatField;
    QrAptosUpPecasIsp: TIntegerField;
    QrAptosUpPecaAtual: TIntegerField;
    QrAptosUpSubInspDone: TShortintField;
    QrAptosUpPontosTot: TIntegerField;
    QrAptosUpEnviadoWeb: TShortintField;
    QrAptosUpInspResul: TIntegerField;
    QrCab: TFDQuery;
    QrCabCodInMob: TIntegerField;
    QrCabDeviceSI: TIntegerField;
    QrCabDeviceID: TStringField;
    QrCabDeviceCU: TIntegerField;
    QrCabNome: TStringField;
    QrCabOVgIspGer: TIntegerField;
    QrCabLocal: TIntegerField;
    QrCabNrOP: TIntegerField;
    QrCabSeqGrupo: TIntegerField;
    QrCabNrReduzidoOP: TIntegerField;
    QrCabProduto: TIntegerField;
    QrCabCodGrade: TIntegerField;
    QrCabCodTam: TStringField;
    QrCabDtHrAbert: TDateTimeField;
    QrCabDtHrFecha: TDateTimeField;
    QrCabOVcYnsMed: TIntegerField;
    QrCabOVcYnsChk: TIntegerField;
    QrCabOVcYnsARQ: TIntegerField;
    QrCabLimiteChk: TIntegerField;
    QrCabLimiteMed: TIntegerField;
    QrCabQtReal: TFloatField;
    QrCabQtLocal: TFloatField;
    QrCabPecasIsp: TIntegerField;
    QrCabPecaAtual: TIntegerField;
    QrCabSubInspDone: TShortintField;
    QrCabPontosTot: TIntegerField;
    QrCabInspResul: TIntegerField;
    QrCabEnviadoWeb: TShortintField;
    QrCabLk: TIntegerField;
    QrCabDataCad: TDateField;
    QrCabDataAlt: TDateField;
    QrCabUserCad: TIntegerField;
    QrCabUserAlt: TIntegerField;
    QrCabAlterWeb: TShortintField;
    QrCabAtivo: TShortintField;
    QrCabRandmStr: TStringField;
    FDPragma: TFDQuery;
    QrIts: TFDQuery;
    QrCabEmpresa: TIntegerField;
    TitleLabel: TLabel;
    LyEnvAll: TLayout;
    AniIndicator1: TAniIndicator;
    MeAvisos: TMemo;
    Rectangle1: TRectangle;
    Image1: TImage;
    QrOVmIspMobFts: TFDQuery;
    QrOVmIspMobFtsCodInMob: TIntegerField;
    QrOVmIspMobFtsCtrlInMob: TIntegerField;
    QrOVmIspMobFtsDataHora: TDateTimeField;
    QrOVmIspMobFtsNomeArq: TStringField;
    QrOVmIspMobFtsIdTabela: TShortintField;
    QrCabPerMedReal: TFloatField;
    QrCabPerChkReal: TFloatField;
    QrCabCodigo: TIntegerField;
    QrCabDtHrMailSnt: TFloatField;
    QrCabSegmntInsp: TIntegerField;
    QrCabSeccaoInsp: TIntegerField;
    procedure ToggleEditModeClick(Sender: TObject);
    procedure SpeedButtonLiveBindingsClick(Sender: TObject);
    procedure ListViewMultiDetailItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FLItem: TListViewItem;
    FStrInfoDone: String;
    FContPontos: Integer;
    FContinua: Boolean;
    FShowAni, IsMSWindows: Boolean;
    //
    procedure CarregaAptosUpload(FechaForm: Boolean);
    procedure RetanguloOpaco_Esconde();
    procedure RetanguloOpaco_Mostra();
    procedure InfoTempMemo(Texto: String);
    procedure InfoDoneMemo(Texto: String);
    //procedure InsereOVmIspDevCab(Codigo, CodInMob: Integer);
    function  UploadInspecaoCab(Codigo, CodInMob: Integer): Boolean;
    function  UploadInspecaoIts(Codigo, CodInMob: Integer; TabSrc, TabDst, Msg: String): Boolean;
    procedure UploadInspecao(); //Item: TListViewItem);
    function  UploadFotos(Codigo, CodInMob: Integer): Boolean;
    //
    procedure MostraAniIndicator();
    procedure MostraSoMeAvisos();
    procedure ThreadExecMostraAniIndicator();
    procedure ThreadExecUploadInspecao();
    //
    procedure ThreadExecEmAndoid();
  //////////////////////////////////////////////////////////////////////////////
  //Vcl.Graphics, Só dá para usar no projeto VCL chamando units FMX !!!
    //function ConvertFmxBitmapToVclBitmap(b:FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
  //////////////////////////////////////////////////////////////////////////////
  public
    { Public declarations }
  end;

var
  FmREST_Up_Faccao: TFmREST_Up_Faccao;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars, UnREST_SQL,
  UnFMX_DmkForms, UnFMX_DmkWeb, UnREST_App, UnFMX_DmkProcFunc,
  Module,
  System.Threading;

procedure TFmREST_Up_Faccao.CarregaAptosUpload(FechaForm: Boolean);
var
  LItem: TListViewItem;
  Linha2, Linha3: String;
begin
  ListViewMultiDetail.Items.Clear;
  //
  // Code to fill TListView
  ListViewMultiDetail.BeginUpdate;
  //if Dmod.AbreAptosUpload(QrAptosUp) then
  if Dmod.ReopenOVmIspMobCab(QrAptosUp, TStatusAndamentoInspecao.saiInspRealizada) then
  begin
    QrAptosUp.First;
    while not QrAptosUp.Eof do
    begin
        LItem := ListViewMultiDetail.Items.Add;
        LItem.Tag  := QrAptosUpCodInMob.Value;
        LItem.Text := QrAptosUpNO_Local.Value;
        // Update data managed by custom appearance
(*
        LItem.Data[TMultiDetailAppearanceNames.Detail1] := Format('%s', [QrAptosUpNO_Artigo.Value]);
        LItem.Data[TMultiDetailAppearanceNames.Detail2] := Format('%s', ['OP ' + Grl_Geral.FF0(QrAptosUpNrOP.Value)]);
        LItem.Data[TMultiDetailAppearanceNames.Detail3] := Format('%s', ['Tamanho: ' + QrAptosUpCodTam.Value]);
*)
        Linha2 := 'OP ' + Grl_Geral.FF0(QrAptosUpNrOP.Value) + '   ' +
                 'Tamanho: ' + QrAptosUpCodTam.Value;
        Linha3 := 'Inspeção: ' + Grl_Geral.FF0(0(*QrAptosUpCodigo.Value*)) +
                  '-' + Grl_Geral.FF0(QrAptosUpCodInMob.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 1, 11, QrAptosUpNO_Artigo.Value);
        FMX_DmkPF.DefTextoLVMDI(LItem, 2, 11, Linha2);
        FMX_DmkPF.DefTextoLVMDI(LItem, 3, 11, Linha3);
        case QrAptosUpInspResul.Value of
          1024: LItem.BitmapRef := Img1024.Bitmap;
          2048: LItem.BitmapRef := Img2048.Bitmap;
          3072: LItem.BitmapRef := Img3072.Bitmap;
          else
          begin

            LItem.BitmapRef := ImgErro.Bitmap;
          end;
        end;
      //
      QrAptosUp.Next;
    end;
  end else
  if FechaForm then
    FMX_DmkForms.FechaFm_AllOS0(FmREST_Up_Faccao);
end;

{
  //////////////////////////////////////////////////////////////////////////////
  //Vcl.Graphics, Só dá para usar no projeto VCL chamando units FMX !!!
  //////////////////////////////////////////////////////////////////////////////
function TFmREST_Up.ConvertFmxBitmapToVclBitmap(
  b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor:TAlphaColor;
begin
  Result:=VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width,b.Height);
  if(b.Map(TMapAccess.Readwrite,data))then
  try
    for i := 0 to data.Height-1 do begin
      for j := 0 to data.Width-1 do begin
        AlphaColor:=data.GetPixel(i,j);
        Result.Canvas.Pixels[i,j]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B);
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;
}

procedure TFmREST_Up_Faccao.FormCreate(Sender: TObject);
begin
  {$IfDef MSWWINDOWS}
    IsMSWindows := True;
  {$Else}
    IsMSWindows := False;
  {$EndIf}
  FLItem := nil;
  FShowAni := False;
  ListViewMultiDetail.Items.Clear;
  //LyEnvAll.Visible := False;
  AniIndicator1.Visible := False;
  //AniIndicator1.Enabled := True;
  RetanguloOpaco_Esconde();
  //
end;

procedure TFmREST_Up_Faccao.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmREST_Up_Faccao.FormShow(Sender: TObject);
begin
  CarregaAptosUpload(True);
end;

procedure TFmREST_Up_Faccao.InfoDoneMemo(Texto: String);
begin
  if IsMSWindows or not FShowAni then
    Application.ProcessMessages;
  FStrInfoDone := Grl_Geral.FDT(Now(), 109) + ' : ' +Texto + '!' + sLineBreak + FStrInfoDone;
  MeAvisos.Text := FStrInfoDone;
  if IsMSWindows or not FShowAni then
    Application.ProcessMessages;
end;

procedure TFmREST_Up_Faccao.InfoTempMemo(Texto: String);
begin
  if IsMSWindows or not FShowAni then
    Application.ProcessMessages;
  MeAvisos.Text := Texto + '...' + sLineBreak + FStrInfoDone;
  if IsMSWindows or not FShowAni then
    Application.ProcessMessages;
end;

function TFmREST_Up_Faccao.UploadInspecaoCab(Codigo, CodInMob: Integer): Boolean;
var
  DeviceID, Nome, DtHrAbert, DtHrFecha, CodTam, DataCad, DataAlt, EmailsExtras,
  Retorno: String;
  DeviceSI, DeviceCU, OVgIspGer, Local, NrOP, SeqGrupo, NrReduzidoOP,
  OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteChk, LimiteMed, PecasIsp, PecaAtual,
  Produto, CodGrade, PontosTot, InspResul, SubInspDone, EnviadoWeb, Lk, UserCad,
  UserAlt, AlterWeb, Ativo, InspeSeq, Empresa: Integer;
  QtReal, QtLocal, PerMedReal, PerChkReal: Double;
  SQLType: TSQLType;
  wSQL, JSON, sVal, RandmStr, DtHrUpIni, DtHrUpFim, IDInspecao_TXT, Msg: String;
  TipoEnvioEmailInspecao: TTipoEnvioEmailInspecao;
  SegmntInsp, SeccaoInsp: Integer;
  SeccaoOP, SeccaoMaq: String;
begin
  IDInspecao_TXT := 'Inspeção ' + Grl_Geral.FF0(Codigo) + '. ';
  //
  Result        := False;
  //
  CodInMob      := QrCabCodInMob.Value;
  DeviceSI      := QrCabDeviceSI.Value;
  DeviceID      := QrCabDeviceID.Value;
  DeviceCU      := QrCabDeviceCU.Value;
  Nome          := QrCabNome.Value;
  OVgIspGer     := QrCabOVgIspGer.Value;
  Local         := QrCabLocal.Value;
  NrOP          := QrCabNrOP.Value;
  SeqGrupo      := QrCabSeqGrupo.Value;
  NrReduzidoOP  := QrCabNrReduzidoOP.Value;
  Produto       := QrCabProduto.Value;
  CodGrade      := QrCabCodGrade.Value;
  CodTam        := QrCabCodTam.Value;
  DtHrAbert     := Grl_Geral.FDT(QrCabDtHrAbert.Value, 109);
  DtHrFecha     := Grl_Geral.FDT(QrCabDtHrFecha.Value, 109);
  OVcYnsMed     := QrCabOVcYnsMed.Value;
  OVcYnsChk     := QrCabOVcYnsChk.Value;
  OVcYnsARQ     := QrCabOVcYnsARQ.Value;
  LimiteChk     := QrCabLimiteChk.Value;
  LimiteMed     := QrCabLimiteMed.Value;
  QtReal        := QrCabQtReal.Value;
  QtLocal       := QrCabQtLocal.Value;
  PecasIsp      := QrCabPecasIsp.Value;
  PecaAtual     := QrCabPecaAtual.Value;
  SubInspDone   := QrCabSubInspDone.Value;
  PontosTot     := QrCabPontosTot.Value;
  InspResul     := QrCabInspResul.Value;
  EnviadoWeb    := QrCabEnviadoWeb.Value;
  Lk            := QrCabLk.Value;
  DataCad       := Grl_Geral.FDT(QrCabDataCad.Value, 1);
  DataAlt       := Grl_Geral.FDT(QrCabDataAlt.Value, 1);
  UserCad       := QrCabUserCad.Value;
  UserAlt       := QrCabUserAlt.Value;
  AlterWeb      := QrCabAlterWeb.Value;
  Ativo         := QrCabAtivo.Value;
  RandmStr      := QrCabRandmStr.Value;
  DtHrUpIni     := Grl_Geral.FDT(Now(), 109);
  DtHrUpFim     := '0000-00-00 00:00:00';
  Empresa       := QrCabEmpresa.Value;
  PerMedReal    := QrCabPerMedReal.Value;
  PerChkReal    := QrCabPerChkReal.Value;
  SegmntInsp    := QrCabSegmntInsp.Value;
  SeccaoInsp    := QrCabSeccaoInsp.Value;
  //SeccaoOP      := QrCabSeccaoOP.Value;
  //SeccaoMaq     := QrCabSeccaoMaq.Value;
  //
  wSQL := Grl_Geral.ATS([
  'SELECT MAX(InspeSeq) InspeSeq ',
  'FROM ovmispdevcab ',
  'WHERE Local=' + Grl_Geral.FF0(Local),
  'AND Produto=' + Grl_Geral.FF0(Produto),
  'AND NrOP=' + Grl_Geral.FF0(NrOP),
  'AND CodTam="' + CodTam + '" ',
  'AND NrReduzidoOP=' + Grl_Geral.FF0(NrReduzidoOP),
  EmptyStr]);
  //Grl_Geral.MB_Info(wSQL);
  //
  InfoTempMemo(IDInspecao_TXT + 'Obtendo o ID sequencial da inspeção no servidor');
  InspeSeq := FMX_DmkRemoteQuery.OVUS_I([wSQL]) + 1;
  //
  SQLType := stIns;
  InfoTempMemo(IDInspecao_TXT + 'Criando o cabeçalho no servidor');
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'ovmispdevcab', False, [
  'CodInMob', 'DeviceSI', 'DeviceID',
  'DeviceCU', 'Nome', 'OVgIspGer',
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
  'OVcYnsMed', 'OVcYnsChk', 'OVcYnsARQ',
  'LimiteChk', 'LimiteMed', 'PecasIsp',
  'PecaAtual', 'QtReal', 'QtLocal',
  'Produto', 'CodGrade', 'CodTam',
  'PontosTot', 'InspResul',
  (*'EnviadoWeb',*) 'InspeSeq', 'RandmStr',
  'DtHrUpIni', 'DtHrUpFim', 'Empresa',
  'PerMedReal', 'PerChkReal',
  'SegmntInsp', 'SeccaoInsp'(*, 'SeccaoOP', 'SeccaoMaq'*)
  ], [
  'Codigo'], [
  CodInMob, DeviceSI, DeviceID,
  DeviceCU, Nome, OVgIspGer,
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, DtHrFecha,
  OVcYnsMed, OVcYnsChk, OVcYnsARQ,
  LimiteChk, LimiteMed, PecasIsp,
  PecaAtual, QtReal, QtLocal,
  Produto, CodGrade, CodTam,
  PontosTot, InspResul,
  (*EnviadoWeb,*) InspeSeq, RandmStr,
  DtHrUpIni, DtHrUpFim, Empresa,
  PerMedReal, PerChkReal,
  SegmntInsp, SeccaoInsp(*, SeccaoOP, SeccaoMaq*)
  ], [
  Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
  False, wSQL) then
  begin
    if FMX_dmkRemoteQuery.SQL_Executa(wSQL) then
    begin
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmobmed', 'ovmispdevmed',
      IDInspecao_TXT + 'Subindo medidas ao servidor') then
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmobinc', 'ovmispdevinc',
      IDInspecao_TXT + 'Subindo inconformidades cadastradas ao servidor') then
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmoblvr', 'ovmispdevlvr',
      IDInspecao_TXT + 'Subindo inconformidades extras ao servidor') then
      if UploadInspecaoIts(Codigo, CodInMob, 'ovmispmobfts', 'ovmispdevfts',
      IDInspecao_TXT + 'Subindo registros de fotos tiradas') then
      begin
        InfoDoneMemo(IDInspecao_TXT + 'Enviada ao servidor');
        EnviadoWeb := Integer(TStatusAndamentoInspecao.saiEnvDadosInspSvr); // 3
        if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
        'Codigo', 'EnviadoWeb'], ['CodInMob'], [
        Codigo, EnviadoWeb], [CodInMob], True,
        TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
        begin
          InfoTempMemo(IDInspecao_TXT + 'Info servidor: EnviadoWeb = ' + Grl_Geral.FF0(EnviadoWeb));
          wSQL := Grl_Geral.ATS([
            'UPDATE ovmispdevcab ',
            'SET DtHrUpFim="' + Grl_Geral.FDT(Now(), 109) + '" ',
            'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
            EmptyStr]);
          Result := FMX_dmkRemoteQuery.SQL_Executa(wSQL);
          InfoTempMemo(IDInspecao_TXT + 'Info servidor: FimUpd = ' + Grl_Geral.FDT(Now(), 109));
          if Result then
          begin
            //
(*
            Dmod.ReopenOVSMOpcoes();
            EmailsExtras := Dmod.QrOVSMOpcoesMailUser.Value;
            //
            InfoTempMemo(IDInspecao_TXT + 'Enviando email');
            if EmailsExtras <> EmptyStr then
              TipoEnvioEmailInspecao := TTipoEnvioEmailInspecao.smerAll
            else
              TipoEnvioEmailInspecao := TTipoEnvioEmailInspecao.smerEnti;
            if REST_App.EnviaEmailResultadoInspecao(Codigo, EmailsExtras,
            TipoEnvioEmailInspecao, Retorno) then
            begin
              InfoDoneMemo(IDInspecao_TXT + Retorno);
              if UploadFotos(Codigo, CodInMob) then
              begin
                InfoTempMemo(IDInspecao_TXT + 'Enviando fotos');
                //
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                // Desmarcar no futuro se houver mais etapas antes de finalizar a inspeção
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                EnviadoWeb := Integer(TStatusAndamentoInspecao.saiFinalizada);  // 9
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
                'EnviadoWeb'], ['CodInMob'], [
                EnviadoWeb], [CodInMob], True,
                TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
                begin
                  // Mensagem de retono de envio
                  InfoDoneMemo(IDInspecao_TXT + 'Envio FINALIZADO');
                end;
              end;
            end else
              // Mensagem de retono de erro
              InfoDoneMemo(IDInspecao_TXT + Retorno);
*)
            InfoTempMemo(IDInspecao_TXT + 'Enviando fotos');
            if UploadFotos(Codigo, CodInMob) then
            begin
              //
              Dmod.ReopenOVSMOpcoes();
              EmailsExtras := Dmod.QrOVSMOpcoesMailUser.Value;
              //
              InfoTempMemo(IDInspecao_TXT + 'Enviando email');
              if EmailsExtras <> EmptyStr then
                TipoEnvioEmailInspecao := TTipoEnvioEmailInspecao.smerAll
              else
                TipoEnvioEmailInspecao := TTipoEnvioEmailInspecao.smerEnti;
              //
              if REST_App.EnviaEmailResultadoInspecaoSegmento(
              TSegmentoInsp.sgminspFaccao, Codigo, EmailsExtras,
              TipoEnvioEmailInspecao, Retorno) then
              begin
                InfoDoneMemo(IDInspecao_TXT + Retorno);
                //
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                // Desmarcar no futuro se houver mais etapas antes de finalizar a inspeção
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                EnviadoWeb := Integer(TStatusAndamentoInspecao.saiFinalizada);  // 9
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
                'EnviadoWeb'], ['CodInMob'], [
                EnviadoWeb], [CodInMob], True,
                TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
                begin
                  // Mensagem de retono de envio
                  InfoDoneMemo(IDInspecao_TXT + 'Envio FINALIZADO');
                end;
              end else
                // Mensagem de retono de erro
                InfoDoneMemo(IDInspecao_TXT + Retorno);
            end;
          end;
        end;
      end;
    end else
    begin
      InfoDoneMemo(IDInspecao_TXT + 'Não foi possível criar o cabeçalho!');
    end;
  end;
end;

procedure TFmREST_Up_Faccao.ListViewMultiDetailItemClick(const Sender: TObject;
  const AItem: TListViewItem);
(*
var
  LItem: TListViewItem;
begin
  FMX_DmkPF.VibrarComoBotao();
  LItem := TListViewItem(ListViewMultiDetail.Selected);
  if LItem <> nil then
  begin
    FLItem := LItem;
{$IfDef ANDROID}
    if FShowAni then
      ThreadExecEmAndoid() else
    begin
      MostraSoMeAvisos();
      UploadInspecao();
    end;
{$EndIf}
{$IfDef MSWINDOWS}
    ThreadExecMostraAniIndicator();
    ThreadExecUploadInspecao();
{$EndIf}
    //
    //UploadInspecao(LItem);
    //
  end;
*)
var
  LItem: TListViewItem;
begin
  FMX_DmkPF.VibrarComoBotao();
  LItem := TListViewItem(ListViewMultiDetail.Selected);
  if LItem <> nil then
  begin
    FLItem := LItem;
{$IfDef ANDROID}
    if FShowAni then
      ThreadExecEmAndoid() else
    begin
      MostraSoMeAvisos();
      UploadInspecao();
    end;
{$EndIf}
{$IfDef MSWINDOWS}
      MostraSoMeAvisos();
      UploadInspecao();
{$EndIf}
    //
    //UploadInspecao(LItem);
    //
  end;
end;

procedure TFmREST_Up_Faccao.SpeedButtonLiveBindingsClick(Sender: TObject);
begin
  LinkFillControlToField1.BindList.FillList;
end;

procedure TFmREST_Up_Faccao.ThreadExecEmAndoid();
begin
  MostraAniIndicator();
  FContinua := True;
  TThread.CreateAnonymousThread
  (
    procedure ()
    //var
      //I: Integer;
    begin
      UploadInspecao();
      //Total := 0;
    end
  ).Start;
end;

procedure TFmREST_Up_Faccao.ThreadExecMostraAniIndicator();
var
  aTask: ITask;
begin
  aTask := TTask.Create(MostraAniIndicator);
  aTask.Start;
end;

procedure TFmREST_Up_Faccao.ThreadExecUploadInspecao();
var
  aTask: ITask;
begin
  aTask := TTask.Create(UploadInspecao);
  aTask.Start;
end;

procedure TFmREST_Up_Faccao.MostraAniIndicator();
begin
  TAnimation.AniFrameRate := 10;
  LyEnvAll.Visible := True;
  RetanguloOpaco_Mostra();
  AniIndicator1.Visible := True;
  AniIndicator1.Enabled := True;
end;

procedure TFmREST_Up_Faccao.MostraSoMeAvisos();
begin
  //AniIndicator1.Visible := True;
  LyEnvAll.Visible := True;
  RetanguloOpaco_Mostra();
  if IsMSWindows or not FShowAni then
    Application.ProcessMessages;
end;

procedure TFmREST_Up_Faccao.RetanguloOpaco_Mostra();
begin
  Rectangle1.Align := TAlignLayout.alClient;
  Rectangle1.Visible := True;
end;

procedure TFmREST_Up_Faccao.RetanguloOpaco_Esconde();
begin
  Rectangle1.Visible := False;
  Rectangle1.Align := TAlignLayout.alNone;
  Rectangle1.Position.X := FmRest_UP_Faccao.Height + 20;
end;

procedure TFmREST_Up_Faccao.ToggleEditModeClick(Sender: TObject);
begin
  ListViewMultiDetail.EditMode := not ListViewMultiDetail.EditMode;
end;

function TFmREST_Up_Faccao.UploadFotos(Codigo, CodInMob: Integer): Boolean;
var
  NomeArq, SohNome, sQtAll, sQtAtu: String;
  //
(*
  function Base64FromBitmap(Bitmap: TBitmap): string;
  var
    Input: TBytesStream;
    Output: TStringStream;
    Encoding: TBase64Encoding;
  begin
    Input := TBytesStream.Create;
    try
      Bitmap.SaveToStream(Input);
      Input.Position := 0;
      Output := TStringStream.Create('', TEncoding.ASCII);
      try
        Encoding := TBase64Encoding.Create(0);
        try
          Encoding.Encode(Input, Output);
          Result := Output.DataString;
        finally
          Encoding.Free;
        end;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;
*)

function Base64FromBitmap(Bitmap: TBitmap): string;
  var
    Input: TBytesStream;
    Output: TStringStream;
  begin
    Input := TBytesStream.Create;
    try
      Bitmap.SaveToStream(Input);
      Input.Position := 0;
      Output := TStringStream.Create('', TEncoding.ASCII);
      try
        Soap.EncdDecd.EncodeStream(Input, Output);
        Result := Output.DataString;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;

  //However, when using a stream instead, FMX's TBitmap.SaveToStream() saves in PNG format only, so you have to use TBitmapCodecManager.SaveToStream() to save in BMP format, eg:
  function Base64FromBitmap2(Bitmap: TBitmap): string;
    function Base64FromStream(MemStream: TMemoryStream): string;
    var
      Input: TBytesStream;
      Output: TStringStream;
      Encoding: TBase64Encoding;
    begin
      Input := TBytesStream.Create;
      try
        //Bitmap.SaveToStream(Input);
        input.LoadFromStream(MemStream);
        Input.Position := 0;
        Output := TStringStream.Create('', TEncoding.ASCII);
        try
          Encoding := TBase64Encoding.Create(0);
          try
            Encoding.Encode(Input, Output);
            Result := Output.DataString;
          finally
            Encoding.Free;
          end;
        finally
          Output.Free;
        end;
      finally
        Input.Free;
      end;
    end;
  var
    Strm : TMemoryStream;
    Surface : TBitmapSurface;
    Input: TBytesStream;
    Output: TStringStream;
  begin
    Strm := TMemoryStream.Create;
    try
      Surface := TBitmapSurface.Create;
      try
        Surface.Assign(Bitmap);
    {$IfDef MSWWINDOWS}
        TBitmapCodecManager.SaveToStream(Strm, Surface, '.bmp');
    {$Else}
        TBitmapCodecManager.SaveToStream(Strm, Surface, '.jpg');
    {$EndIf}
        Result := Base64FromStream(Strm);
      finally
        Surface.Free;
      end;
      Strm.Position := 0;
      //BMP.LoadFromStream(Strm);
    finally
      Strm.Free;
    end;
  end;

  procedure SobeImagemAosPedacos();
  const
    //TamPedaco = 65535;
    TamPedaco = 32768;
  var
    Bitmap: TBitmap;
    sBase64, DataHora: String;
    TamSubido, TamTotal, TamAtuIni: Integer;
  var
    TxtBmp, wSQL: String;
    //Codigo, CodInMob,
    IdTabela, CtrlInMob, Pedaco: Integer;
    Percentual: Double;
    SQLType: TSQLType;
  begin
    Bitmap := TBitmap.Create;
    try
      Bitmap.Width  := Image1.MultiResBitmap.Items[0].Width;
      Bitmap.Height := Image1.MultiResBitmap.Items[0].Height;
      //Bitmap.PixelFormat := pf24bit;
      Bitmap.Assign(Image1.MultiResBitmap.Items[0].Bitmap);
{$IfDef MSWINDOWS}
      sBase64 := Base64FromBitmap2(Bitmap);
{$Else}
      sBase64 := Base64FromBitmap2(Bitmap);
{$EndIf}
      //
      Pedaco     := 0;
      TamSubido  := 0;
      TamTotal   := Length(sBase64);
      //ShowMessage(sBase64);
      while TamSubido < TamTotal do
      begin
        TamAtuIni := (Pedaco * TamPedaco) + 1;
        TxtBmp    := Copy(sBase64, TamAtuIni, TamPedaco);
        //BitmapFromBase64(str1 + str2, Image2.MultiResBitmap.Items[0].Bitmap);
        SQLType        := stIns;
        //Codigo         := ;
        IdTabela       := QrOVmIspMobFtsIdTabela.Value;
        //CodInMob       := QrOVmIspMobFts.Value;
        CtrlInMob      := QrOVmIspMobFtsCtrlInMob.Value;
        DataHora       := Grl_Geral.FDT(QrOVmIspMobFtsDataHora.Value, 109);
        Pedaco         := Pedaco + 1;
        //TxtBmp         := ;

          //
        if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'ovmispdevbmp', False, [
        'CodInMob', 'TxtBmp'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'DataHora', 'Pedaco'], [
        CodInMob, TxtBmp], [
        Codigo, IdTabela, CtrlInMob, DataHora, Pedaco], True,
        TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop, False, wSQL) then
        begin
          if FMX_dmkRemoteQuery.SQL_Executa(wSQL) then
          begin
            TamSubido := Pedaco * TamPedaco;
            Percentual := TamSubido / TamTotal * 100;
            if Percentual >= 100 then
            begin
              InfoDoneMemo('Foto ' + sQtAtu + '/' + sQtAll + ' > ' + SohNome + ' enviada!')
            end else
              InfoTempMemo(Grl_Geral.FFT(Percentual, 2, siNegativo) + ' % enviado: ' + SohNome);
          end;
        end;
      end;
    finally
      //Bitmap.Free;
    end;
  end;
begin
  Result := False;
  InfoTempMemo('Preparando envio de fotos');
  if FMX_dmkRemoteQuery.SQL_Executa([
  'DELETE FROM ovmispdevbmp',
  'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
  EmptyStr]) then
  begin
    Grl_DmkDB.AbreSQLQuery0(QrOVmIspMobFts, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovmispmobfts ',
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
    EmptyStr]);
    if QrOVmIspMobFts.RecordCount = 0 then
    begin
      InfoDoneMemo('Não há fotos nesta inspeção');
      Result := True;
      Exit;
    end;
    //
    sQtAll := Grl_Geral.FF0(QrOVmIspMobFts.RecordCount);
    InfoDoneMemo('Enviando ' + sQtAll + ' fotos ao servidor');
    QrOVmIspMobFts.First;
    while not QrOVmIspMobFts.Eof do
    begin
      sQtAtu  := Grl_Geral.FF0(QrOVmIspMobFts.RecNo);
      NomeArq := QrOVmIspMobFtsNomeArq.Value;
      SohNome := ExtractFileName(NomeArq);
      InfoTempMemo('Abrindo arquivo ' + SohNome);
      if FileExists(NomeArq) then
      begin
        //System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetCameraPath, FNomeArq);
        Image1.MultiResBitmap.Items[0].Bitmap.LoadFromFile(NomeArq);
        SobeImagemAosPedacos();
      end else
      InfoDoneMemo('Arquivo não encontrado: ' + SohNome);
      //
      QrOVmIspMobFts.Next;
    end;
    Result := True;
  end;
end;

procedure TFmREST_Up_Faccao.UploadInspecao();
var
  CodInMob, Codigo, Conexao: Integer;
  SQLType: TSQLType;
  Msg, wSQL: String;
  LItem: TListViewItem;
begin
  InfoTempMemo('Iniciando processo');
  //LItem := TListViewItem(ListViewMultiDetail.Selected);
  LItem := FLItem;
  if LItem = nil then Exit;
  try
    InfoTempMemo('Item definido. Verificando internet.');
    if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
      Exit;
    InfoTempMemo('Internet verificada.');
    CodInMob := LItem.Tag;
    InfoTempMemo('Gerando ID da inspeção no servidor');
    Grl_DmkDB.AbreSQLQuery0(QrCab, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovmispmobcab ',
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
    EmptyStr]);
    //
    // Ver se o cabeçalho já existe!
    wSQL := Grl_Geral.ATS([
    'SELECT Codigo ',
    'FROM ovmispdevcab ',
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
    'AND RandmStr="' + QrCabRandmStr.Value + '"',
    EmptyStr]);
    Codigo  := FMX_DmkRemoteQuery.OVUS_I([wSQL]);
    //
    if Codigo = 0 then
    begin
      SQLType := TSQLType.stIns;
      Codigo  := FMX_DmkRemoteQuery.BPGS1I32W('ovmispdevcab' , 'Codigo', '', '',
        tsPos, SQLType, (*Codigo*)0);
      InfoDoneMemo('ID da inspeção gerada no servidor: ' + Grl_Geral.FF0(Codigo));
    end else
    begin
      //if
      FMX_dmkRemoteQuery.SQL_Executa([
      'DELETE FROM ovmispdevcab ',
      'WHERE Codigo=' + Grl_Geral.FF0(Codigo)
      ]);// then
    end;
    //
    if UploadInspecaoCab(Codigo, CodInMob) then
    begin
      RetanguloOpaco_Esconde();
      CarregaAptosUpload(False);
    end;
  finally
    FContinua := False;
    //
    RetanguloOpaco_Esconde();
    //
    AniIndicator1.Visible := False;
    AniIndicator1.Enabled := False;
  end;
end;

function TFmREST_Up_Faccao.UploadInspecaoIts(Codigo, CodInMob: Integer;
  TabSrc, TabDst, Msg: String): Boolean;
(*
const
  Alias = '';
  SQL_JOIN = '';
*)
var
  sCodigo,
  Campos, Lit, (*SQL, _Alias,  MyAlias,*) JSON: String;
  //
  SQL_WHERE: String;
begin
  InfoTempMemo(Msg);
  //
  Result := False;
  sCodigo := Grl_Geral.FF0(Codigo);
  SQL_WHERE := 'WHERE CodInMod=' + Grl_Geral.FF0(CodInMob);
  //
  Campos := '';
(*
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
*)
  //
  Grl_DmkDB.AbreSQLQuery0(FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := MyAlias + FDPragma.FieldByName('Name').AsString;
  Lit := FDPragma.FieldByName('Name').AsString;
  FDPragma.Next;
  while not FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + MyAlias + FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    FDPragma.Next;
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(QrIts, Dmod.AllDB, [
    'SELECT ' + Lit,
    'FROM ' + Lowercase(TabSrc),
    'WHERE CodInMob=' + Grl_Geral.FF0(CodInMob),
  EmptyStr]);
  QrIts.First;
  if FMX_dmkRemoteQuery.SQL_Executa([
  'DELETE FROM ' + TabDst,
  'WHERE Codigo=' + sCodigo
  ]) then
    Result :=
      REST_SQL.ValuesFromFDQuery(QrIts, TabDst, 'Codigo', sCodigo, Lit, 10000);
end;

end.
