unit OViTclExgAvl;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FMX.ScrollBox, FMX.Grid, FMX.StdCtrls,
  FMX.Controls.Presentation, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Bindings.Outputs, Data.Bind.Components, Data.Bind.Grid,
  Data.Bind.DBScope, Fmx.Bind.Grid, Fmx.Bind.Editors, UnDmkEnums,
  System.ImageList, FMX.ImgList;

type
  TFmOViTclExgAvl = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    PnArtigo: TPanel;
    Label2: TLabel;
    LaArtigo: TLabel;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaNrOP: TLabel;
    Label4: TLabel;
    LaTamCad: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    LaItemAtual: TLabel;
    Label9: TLabel;
    LaTotalItens: TLabel;
    GrContexto: TGrid;
    QrExigencia: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    BtNxtEnd: TButton;
    BtSair: TButton;
    ImageList1: TImageList;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrExigenciaBRK_Exacao: TStringField;
    QrExigenciaNO_TOPYKO: TStringField;
    QrExigenciaControle: TIntegerField;
    QrExigenciaITENS: TLargeintField;
    QrExigenciaCadTopyko: TIntegerField;
    procedure GrContextoCellClick(const Column: TColumn; const Row: Integer);
    procedure BtSairClick(Sender: TObject);
    procedure BtNxtEndClick(Sender: TObject);
    procedure QrExigenciaAfterOpen(DataSet: TDataSet);
    procedure QrExigenciaCalcFields(DataSet: TDataSet);
    procedure GrContextoGetValue(Sender: TObject; const ACol, ARow: Integer;
      var Value: TValue);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }

  public
    { Public declarations }
    FFecha: Boolean;
    FCodInMob, FPecaSeq, FLocal, FArtigo, FNrOP, FNrReduzidoOP, FProduto,
    FCodGrade, FOVcYnsExg: Integer;
    FNO_Local, FNO_Artigo, FCodTam: String;
    //
    //
    procedure ReopenItem();
  end;

var
  FmOViTclExgAvl: TFmOViTclExgAvl;

implementation

{$R *.fmx}

uses UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkForms,
  UnFMX_DmkProcFunc,
  Module,
  OViTclExgMix, OViTclCenSel;

{ TFmOViInspSeqMed }

procedure TFmOViTclExgAvl.BtNxtEndClick(Sender: TObject);
var
  SubInspDone, CodInMob, _CIM, PecaAtual, TotalPecas: Integer;
  Proximo: Boolean;
begin
  FMX_DmkPF.VibrarComoBotao();
  Proximo     := False;
  TotalPecas  := Grl_Geral.IMV(FmOViTclCenSel.LaTotalItens.Text);
  if FPecaSeq = TotalPecas then
  begin
    SubInspDone := 2; // 2 = Inconformidades avaliadas
    PecaAtual    := FPecaSeq;
  end else
  begin
    SubInspDone := 0; // 0 = Nada pronto
    PecaAtual    := FPecaSeq + 1;
  end;
  CodInMob    := FCodInMob;
  //
  if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'ovmispmobcab', False, [
  'SubInspDone', 'PecaAtual'], [
  'CodInMob'], [
  SubInspDone, PecaAtual], [
  CodInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
  begin
    FmOViTclCenSel.LoacalizaAberto_QrOVmItxMobCab(Proximo, _CIM);
    //
    FMX_DmkForms.FechaFm_AllOS0(FmOViTclExgAvl);
  end;
end;

procedure TFmOViTclExgAvl.BtSairClick(Sender: TObject);
begin
  FMX_DmkForms.FechaFm_AllOS0(FmOViTclExgAvl);
end;

procedure TFmOViTclExgAvl.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmOViTclExgAvl.GrContextoCellClick(const Column: TColumn;
  const Row: Integer);
begin
  if (QrExigencia.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViTclExgMix, FmOViTclExgMix, fcmOnlyCreate, True, False) then
  begin
    //FmOViTclExgMix.LaLocal.Text    := NO_Local;
    FmOViTclExgMix.LaArtigo.Text     := FNO_Artigo;
    FmOViTclExgMix.LaNrOP.Text       := Grl_Geral.FF0(FNrOP);
    FmOViTclExgMix.LaTamCad.Text     := FCodTam;
    FmOViTclExgMix.LaItemAtual.Text  := LaItemAtual.Text;
    FmOViTclExgMix.LaTotalItens.Text := LaTotalItens.Text;
    //
    FmOViTclExgMix.FCodInMob          := FCodInMob;
    FmOViTclExgMix.FCtrlInMob         := 0; // Criar Novo
    FmOViTclExgMix.FPecaSeq           := FPecaSeq;
    FmOViTclExgMix.FLocal             := FLocal;
    FmOViTclExgMix.FArtigo            := FArtigo;
    FmOViTclExgMix.FNrOP              := FNrOP;
    FmOViTclExgMix.FNrReduzidoOP      := FNrReduzidoOP;
    FmOViTclExgMix.FProduto           := FProduto;
    FmOViTclExgMix.FCodGrade          := FCodGrade;
    FmOViTclExgMix.FNO_Local          := FNO_Local;
    FmOViTclExgMix.FNO_Artigo         := FNO_Artigo;
    FmOViTclExgMix.FCodTam            := FCodTam;
    //
    FmOViTclExgMix.LaNO_TOPYKO.Text   := QrExigenciaNO_TOPYKO.Value;
    FmOViTclExgMix.FOVcYnsExgCad      := FOVcYnsExg; // OVcYnsExgTop.Codigo
    FmOViTclExgMix.FOVcYnsExgTop      := QrExigenciaControle.Value; // // OVcYnsExgTop.Codigo
    FmOViTclExgMix.FOVcYnsMixTop      := QrExigenciaCadTopyko.Value; // OVcYnsMixTop.Codigo
    //
    FmOViTclExgMix.ReopenItem();
    //
    FMX_DmkForms.ShowModal(FmOViTclExgMix);
  end;
end;

procedure TFmOViTclExgAvl.GrContextoGetValue(Sender: TObject; const ACol,
  ARow: Integer; var Value: TValue);
begin
  // http://monkeystyler.com/guide/TGrid
end;

procedure TFmOViTclExgAvl.QrExigenciaAfterOpen(DataSet: TDataSet);
begin
  if QrExigencia.RecordCount > 0 then
    GrContexto.Col
end;

procedure TFmOViTclExgAvl.QrExigenciaCalcFields(DataSet: TDataSet);
begin
  QrExigenciaBRK_Exacao.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrContexto, 0, QrExigenciaNO_TOPYKO.Value);
end;

procedure TFmOViTclExgAvl.ReopenItem();
begin
  Grl_DmkDB.AbreSQLQuery0(QrExigencia, Dmod.AllDB, [
(*
  'SELECT ycc.Controle, ',
  'yqt.Codigo OVcYnsMixTop, yqt.Nome NO_CONTEXTO, ',
  ' COUNT(imi.PecaSeq) ITENS ',
  'FROM ovcynsqstctx yqt ',
  'LEFT JOIN ovcynschkctx ycc ON yqt.Codigo=ycc.Contexto ',
  'LEFT JOIN ovmispmobinc imi ON imi.ovcynschkctx=ycc.Controle ',
  '  AND imi.CodInMob=' + Grl_Geral.FF0(FCodInMob),
  '  AND imi.PecaSeq=' + Grl_Geral.FF0(FPecaSeq),
  'WHERE ycc.Codigo=' + Grl_Geral.FF0(FOVcYnsExg),
  'GROUP BY yqt.Codigo ',
*)
  'SELECT imt.Nome NO_TOPYKO, imt.Codigo CadTopyko, ',
  'yet.controle, COUNT(ime.PecaSeq) ITENS ',
  'FROM ovcynsmixtop imt ',
  'LEFT JOIN ovcynsexgtop yet ON imt.Codigo=yet.Topyko ',
  ' ',
  'LEFT JOIN ovmitxmobexg ime ON ime.ovcynsexgtop=yet.Controle ',
  '  AND ime.CodInMob=' + Grl_Geral.FF0(FCodInMob),
  '  AND ime.PecaSeq=' + Grl_Geral.FF0(FPecaSeq),
  ' ',
  'WHERE yet.Codigo=' + Grl_Geral.FF0(FOVcYnsExg),
  'GROUP BY imt.Codigo ',
  EmptyStr]);
  //Grl_Geral.MB_SQL(FmOViTclExgAvl, QrExigencia);

end;

end.
