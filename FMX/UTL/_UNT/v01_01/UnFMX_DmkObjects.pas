unit UnFMX_DmkObjects;

interface

uses System.Classes, System.UITypes, FMX.Controls, FMX.Forms, FMX.ListBox,
  FMX.StdCtrls, FMX.Dialogs, FMX.Types, FMX.Layouts, System.SysUtils,
  System.Variants, System.Rtti, System.Types, System.DateUtils, FMX.Graphics,
  FMX.Edit,
  // MultiDetail
  FMX.ListView.Types, FMX.ListView.Appearances, MultiDetailAppearanceU,
  // Fim MultiDetail
  {$IF DEFINED(ANDROID)}
  Android_FMX_ProcFunc,
  {$ELSE}
  Windows_FMX_ProcFunc,
  {$ENDIF}
  Data.Bind.Components, System.Notification, System.Math, FMX.Surfaces,
  System.IOUtils, FMX.Grid, UnGrl_Vars,
  // Form KeyUp
  FMX.Platform, FMX.VirtualKeyboard, FMX.Objects,
  UnGrl_Consts;

type
  TProc = reference to procedure;
  TUnFMX_DmkObjects = class(TObject)
  private
    {Private declarations}
  public
    {Public declarations}
(*
    procedure CarregaItemAtualTextETitleEmRectangle(Form: TForm; IndicePrimario:
              Integer; Titulo: String; Texto: array of String; VSB:
              TVertScrollBox; TextClick: TProc);
*)
    procedure LabelInRectangleNameRec(Sender: TRectangle);
    function  AlturaRectRunTimeCreateInRectangle(VSB: TVertScrollBox; Rectangle:
              TRectangle; Text: TText; Linhas: array of String): Single;
    function  MenuFloatTopFromBounds(Form: TForm; RectMenuBase: TRectangle): Extended;
  end;

var
  FMX_dmkObjects: TUnFMX_DmkObjects;

implementation

uses UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums;

{ TUnFMX_DmkObjects }

function TUnFMX_DmkObjects.AlturaRectRunTimeCreateInRectangle(
  VSB: TVertScrollBox; Rectangle: TRectangle; Text: TText;
  Linhas: array of String): Single;
var
  I: Integer;
  N, W, ctW, ctH: Single;
  S: String;
begin
  W := VSB.Width - Rectangle.Margins.Left - Rectangle.Margins.Right - Text.Margins.Left - Text.Margins.Right;
  N := 0;
  S := '';
  for I := 0 to Length(Linhas) - 1 do
  begin
    ctW := Text.Canvas.TextWidth(Linhas[I]);
    N := N + Trunc(ctW / W) + 1;
    S := S + sLineBreak +  Linhas[I];
  end;
  if N = 0 then N := 1;
  //
  Text.Text := S;
  ctH := Text.Canvas.TextHeight('j|');
  //Rectangle.Height
  Result := (N * ctH) + Text.Margins.Top + Text.Margins.Bottom; // + 10;
  //
end;

{
procedure TUnFMX_DmkObjects.CarregaItemAtualTextETitleEmRectangle(Form: TForm;
  IndicePrimario: Integer; Titulo: String; Texto: array of String; VSB:
  TVertScrollBox; TextClick: TProc);
var
  CR: TRectangle;
  L: TText;
  TmpImg: TImage;
begin
  CR := TRectangle.Create(Form);
  CR.Parent := VSB;
  LabelInRectangleNameRec(CR);
  CR.Align := TAlignLayout.alTop;
  //CR.CalloutPosition := TCalloutPosition.cpLeft;
  CR.Margins.Top := 10;
  CR.Margins.Bottom := 10;
  CR.Margins.Right := 5;
  //CR.Height := 75;

  CR.Fill.Color := VAR_LaTexto_Back_Color;
  CR.Stroke.Kind := TBrushKind.None;

  CR.XRadius := 10;
  CR.YRadius := 10;
  CR.Margins.Left := 5;
  CR.Margins.Left := 5;
  CR.Margins.Top  := 10;
  CR.Margins.Left := 5;
  //CR.Stroke.Color := VAR_DMK_THEMES_COLOR_DkLigh;

  L := TText.Create(Form);
  L.Parent := CR;
  L.Align := TAlignLayout.alClient;
    //'A quick brown fox jumped over the yellow log running away from the pink dog and ran down the lane.';
  L.Tag       := IndicePrimario;
  //L.TagFloat  := QrChmOcoPuxDtHrLido.Value;
  L.Margins.Top    := 5;
  L.Margins.Bottom := 5;
  L.Margins.Left   := 20;
  L.Margins.Right  := 5;

  L.WordWrap := True;
  L.AutoSize := True;
  //L.OnPaint := FMX_DmkPF.LabelInRectanglePaint;

  L.TextSettings.FontColor := VAR_LaTexto_Font_Color;
  L.TextSettings.HorzAlign := TTextAlign.Leading;
  L.HitTest := True;
  L.OnClick := @TextClick;

  CR.Align := TAlignLayout.alBottom;
  CR.Align := TAlignLayout.alTop;
  //L.Repaint;
  //
  CR.Height        := AlturaRectRunTimeCreateInRectangle(VSB, CR, L, Texto);
  L.Text           := FMX_Geral.ATS(Texto);
end;
}

procedure TUnFMX_DmkObjects.LabelInRectangleNameRec(Sender: TRectangle);
begin
  VAR_RUN_TIME_CREATED_COUNT := VAR_RUN_TIME_CREATED_COUNT + 1;
  if VAR_RUN_TIME_CREATED_COUNT = High(Integer) - 1 then
    VAR_RUN_TIME_CREATED_COUNT := 1;
  //
  TRectangle(Sender).Name :=
    CO_RUN_TIME_CREATED_PREFIX + IntToStr(VAR_RUN_TIME_CREATED_COUNT);
end;

function TUnFMX_DmkObjects.MenuFloatTopFromBounds(Form: TForm;
  RectMenuBase: TRectangle): Extended;
var
  iBoundsH, Dif: Extended;
begin
  if VAR_VIRTUAL_KEYBOARD_BOUNDS > 0 then
    iBoundsH := VAR_VIRTUAL_KEYBOARD_BOUNDS
  else
    iBoundsH := 0;
  //
  Dif := Form.Height - RectMenuBase.Height - iBoundsH - 20;
  if Dif < 0 then
    iBoundsH := iBoundsH + Dif;
  //
  Result := iBoundsH;
end;

end.
