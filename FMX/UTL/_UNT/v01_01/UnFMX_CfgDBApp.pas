unit UnFMX_CfgDBApp;

interface

uses System.Generics.Collections, System.Classes, System.UITypes,
  System.SysUtils, System.IOUtils, System.Variants, FMX.Forms, FMX.ListBox,
  FMX.StdCtrls, FMX.Dialogs, FMX.Memo, FireDAC.Comp.Client, Data.DB,
  UnMyLinguas, UnDmkEnums, UnGrl_Vars;

type
  TUnFMX_CfgDBApp = class(TObject)
  private
    procedure ReopenOpcoesEspecificas(DataBaseAll: TFDConnection;
              QueryEspecificos, QueryUpd: TFDQuery);
  public
    procedure ReopenControle(DataBaseAll: TFDConnection;
              QueryControle: TFDQuery);
    function  ReopenUsuarios(DataBaseAll, DataBaseUsuario: TFDConnection;
              QueryUsuarios, QueryLoc: TFDQuery): Boolean;
    procedure MostraVerifyDB(Verifica: Boolean; DataBaseAll,
              DataBaseUsuario: TFDConnection; TelaCheia: Boolean = False);
    procedure ConfiguraDispositivo(LaUsuario: TLabel; DataBaseAll,
              DataBaseUsuario: TFDConnection; QueryUsuarios, QueryLoc, QueryUpd,
              QueryControle, QueryEspecificos: TFDQuery);
    procedure ConfiguraBD(DataBaseAll, DataBaseUsuario: TFDConnection;
              QueryLoc, QueryUpd, QueryUsuarios, QueryControle,
              QueryEspecificos: TFDQuery; LaUsuario: TLabel);
    //procedure MostraSincro(SincTabAux: Boolean);
    procedure VerifyDBSemMostrar(Verifica: Boolean; DataBaseAll,
              DataBaseUsuario: TFDConnection);
  end;

var
  FMX_CfgDBApp: TUnFMX_CfgDBApp;

implementation

uses UnFMX_DmkDB, UnGrl_DmkDB, UnFMX_Grl_Vars, UnGrl_Consts, UnFMX_Geral,
  UnFMX_DmkProcFunc, MyListas, VerifyDB, (*SincroDB, FMX#err*) UnFMX_DmkForms,
  Principal;

{ TUnFMX_CfgDBApp }

procedure TUnFMX_CfgDBApp.ConfiguraBD(DataBaseAll, DataBaseUsuario: TFDConnection;
  QueryLoc, QueryUpd, QueryUsuarios, QueryControle, QueryEspecificos: TFDQuery;
  LaUsuario: TLabel);
var
  Versao: Integer;
  DBUsu: TFDConnection;
begin
  FMX_dmkDB.ConfiguraDB(DataBaseAll, istSQLite, CO_DBNome, CO_RandStrBDLite);
  //
  Versao := Grl_DmkDB.ObtemVersaoAppDB(QueryLoc, DataBaseAll, istSQLite, stMobile);

  if Versao < CO_VERSAO then
  begin
    DBUsu := nil;
    //
    if Versao <> 0 then
    begin
      if ReopenUsuarios(DataBaseAll, DataBaseUsuario, QueryUsuarios, QueryLoc) then
        DBUsu := DataBaseUsuario;
    end;
   FMX_CfgDBApp.MostraVerifyDB(True, DataBaseAll, DBUsu);
  end else
  begin
    ConfiguraDispositivo(LaUsuario, DataBaseAll, DataBaseUsuario,
      QueryUsuarios, QueryLoc, QueryUpd, QueryControle, QueryEspecificos);
  end;
end;

procedure TUnFMX_CfgDBApp.ConfiguraDispositivo(LaUsuario: TLabel; DataBaseAll,
  DataBaseUsuario: TFDConnection; QueryUsuarios, QueryLoc, QueryUpd,
  QueryControle, QueryEspecificos: TFDQuery);
var
  Res: Boolean;
begin
  Res := ReopenUsuarios(DataBaseAll, DataBaseUsuario, QueryUsuarios, QueryLoc);
  //
  if not Res then
  begin
    ReopenControle(DataBaseAll, QueryControle);
    ReopenOpcoesEspecificas(DataBaseAll, QueryEspecificos, QueryUpd);
    //
    VAR_WEB_USR_ID    := 0;
    VAR_WEB_USR_TOKEN := '';
    //
    if LaUsuario <> nil then
      LaUsuario.Text := '';
  end else
  begin
    ReopenControle(DataBaseAll, QueryControle);
    ReopenOpcoesEspecificas(DataBaseAll, QueryEspecificos, QueryUpd);
    //
    FMX_Geral.DefineFormatacoes;
    //
    if LaUsuario <> nil then
      LaUsuario.Text := VAR_WEB_USR_NOME;
  end;
end;

(*
procedure TUnFMX_CfgDBApp.MostraSincro(SincTabAux: Boolean);
begin
  if FMX_dmkForms.CriaFm_AllOS2(TFmSincroDB, FmSincroDB) = True then
  begin
    FmSincroDB.CkTabelasAux.Visible := SincTabAux;
    //
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmSincroDB.Show;
    {$ELSE}
    FmSincroDB.ShowModal;
    FmSincroDB.Destroy;
    {$ENDIF}
  end;
end;
*)

procedure TUnFMX_CfgDBApp.MostraVerifyDB(Verifica: Boolean; DataBaseAll,
  DataBaseUsuario: TFDConnection; TelaCheia: Boolean = False);
var
  DBUsu: TFDConnection;
begin
  if ((VAR_WEB_USR_ID <> 0) and (VAR_WEB_USR_TOKEN <> '')) then
    DBUsu := DataBaseUsuario
  else
    DBUsu := nil;
  //
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica    := Verifica;
  FmVerifyDB.FDataBaseAll := DataBaseAll;
  FmVerifyDB.FDataBaseUsu := DBUsu;
  FmVerifyDB.FullScreen   := TelaCheia;
  FmVerifyDB.Show;
end;

procedure TUnFMX_CfgDBApp.ReopenControle(DataBaseAll: TFDConnection;
  QueryControle: TFDQuery);
begin
  Grl_DmkDB.AbreSQLQuery0(QueryControle, DataBaseAll, [
    'SELECT Versao ',
    'FROM controle ',
    '']);
end;

procedure TUnFMX_CfgDBApp.ReopenOpcoesEspecificas(DataBaseAll: TFDConnection;
  QueryEspecificos, QueryUpd: TFDQuery);
begin
  (* N�o usa habitlitar quando precisar
  Grl_DmkDB.AbreSQLQuery0(QueryEspecificos, DataBaseAll, [
    'SELECT * ',
    'FROM ' + CO_OpcTab,
    '']);
  if QrEspecificos.RecordCount = 0 then
  begin
    Grl_DmkDB.SQLInsUpd(QueryUpd, DataBaseAll, stIns, CO_OpcTab, False,
      [], ['Codigo'], [], [1], True, False, '', stMobile, False);
  end;
  *)
end;

function TUnFMX_CfgDBApp.ReopenUsuarios(DataBaseAll,
  DataBaseUsuario: TFDConnection; QueryUsuarios, QueryLoc: TFDQuery): Boolean;
begin
  Result := False;
  //
  Grl_DmkDB.AbreSQLQuery0(QueryUsuarios, DataBaseAll, [
    'SELECT name ',
    'FROM sqlite_master ',
    'WHERE name="usuarios"',
    '']);

  if QueryUsuarios.RecordCount > 0 then
  begin
    Grl_DmkDB.AbreSQLQuery0(QueryUsuarios, DataBaseAll, [
      'SELECT Codigo, PersonalName, UserName, Tipo, ',
      'UsrEnt, UsrID, UsrNum, Token, WebID ',
      'FROM Usuarios ',
      'WHERE Atual = 1 ',
      '']);
  end;
  if QueryUsuarios.RecordCount > 0 then
  begin
    VAR_USUARIO         := QueryUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USR_ID      := QueryUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USR_NOME    := QueryUsuarios.FieldByName('PersonalName').AsString;
    VAR_WEB_USR_USUARIO := QueryUsuarios.FieldByName('UserName').AsString;
    VAR_WEB_USR_ENT     := QueryUsuarios.FieldByName('UsrEnt').AsInteger;
    VAR_WEB_USR_DEVICE  := QueryUsuarios.FieldByName('UsrNum').AsInteger;
    VAR_WEB_USR_ID      := QueryUsuarios.FieldByName('UsrID').AsInteger;
    VAR_WEB_USR_TIPO    := QueryUsuarios.FieldByName('Tipo').AsInteger;
    VAR_WEB_USR_TOKEN   := QueryUsuarios.FieldByName('Token').AsString;
    VAR_WEB_USR_WEBID   := QueryUsuarios.FieldByName('WebID').AsString;
    //
    VAR_APP_DBUsuarioNome := CO_DBNome + '_' + FMX_Geral.FF0(VAR_WEB_USR_ID);
    //
    FMX_dmkDB.ConfiguraDB(DataBaseUsuario, istSQLite, VAR_APP_DBUsuarioNome, CO_RandStrBDLite);
    //
    Grl_DmkDB.AbreSQLQuery0(QueryLoc, DataBaseUsuario, [
      'SELECT name ',
      'FROM sqlite_master ',
      'WHERE type="table"',
      '']);

    if QueryLoc.RecordCount = 0 then
      MostraVerifyDB(True, nil, DataBaseUsuario);
    //
    Result := True;
  end;
end;

procedure TUnFMX_CfgDBApp.VerifyDBSemMostrar(Verifica: Boolean; DataBaseAll,
  DataBaseUsuario: TFDConnection);
var
  DBUsu: TFDConnection;
begin
  if ((VAR_WEB_USR_ID <> 0) and (VAR_WEB_USR_TOKEN <> '')) then
    DBUsu := DataBaseUsuario
  else
    DBUsu := nil;
  //
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica    := Verifica;
  FmVerifyDB.FDataBaseAll := DataBaseAll;
  FmVerifyDB.FDataBaseUsu := DBUsu;
  FmVerifyDB.Show;
end;

end.
