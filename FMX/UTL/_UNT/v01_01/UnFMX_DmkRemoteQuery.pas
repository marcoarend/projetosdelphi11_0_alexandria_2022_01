unit UnFMX_DmkRemoteQuery;

interface

uses System.Classes, System.UITypes,
  //FMX.Forms, FMX.ListBox, FMX.StdCtrls, FMX.Dialogs,
  System.SysUtils, System.Variants, IdHTTP, REST.Client, REST.Types,
  Data.DB, TypInfo,
  UnGrl_DmkWeb, UnDMkEnums;

type
  TUMyArray_Str = array of String;
  TUnFMX_DmkRemoteQuery = class(TObject)
  private
(*
    function  BuscaProximoGerlSeq1Int32_Web(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer;
              Reservados: Variant): Integer;
*)
    function  Query(const CNPJ, SQLType: String; const SQL: array of string;
              const Ambiente: Integer; var Resultado: String): Integer;
    function  ObtemValorUnicoSQL(CNPJ: String; SQL: array of string; Ambiente:
              Integer): String;
    function  BPGS1I32W_Novo(const Tabela, Campo, _WHERE, _AND: String; const
              TipoSinal: TTipoSinal; const SQLType: TSQLType; const DefUpd:
              Integer; var Codigo: Integer): Boolean;
    procedure MsgErroServer();
  public
    function  ConexaoWebREST_MySQL(): Boolean;
              //
    function  BPGS1I32W(Tabela, Campo, _WHERE, _AND: String; TipoSinal:
              TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
    function  SQL_Executa(SQL: array of string): Boolean;
    function  JsonText(const SQL: array of string; var JSON: String): Boolean;
    procedure JsonToDataset(DataSet: TDataSet; JSON: String);
    function  OVUS_I(SQL: array of string): Integer;
    function  OVUS_X(SQL: array of string): String;
    //
    function  QueryExecute(CNPJ: String; SQL: array of string; Ambiente:
              Integer): Boolean;
    function  QuerySelect(DataSet: TDataSet; CNPJ: String; SQL: array of string;
              Ambiente: Integer): Boolean;
    //
    //
    function  ParseJson0(StrJson: String): String; // Campo �nico, valor Unico
    procedure ParseJson1(StrJson: String); // s� teste. N�o usado
    function  ParseJson2(StrJson: String): String;
    function  ParseJson3(const StrJson: String; const MaxItens: Integer; var
              StrCordas: String): TUMyArray_Str;
    //
    function  CordasDeQrREST_1(const Tabela, Campo: String;
              const MaxItens: Integer; var StrCordas: String): TUMyArray_Str;

  end;

var
  FMX_dmkRemoteQuery: TUnFMX_DmkRemoteQuery;
const
  CO_SQLTYPE_SELECT = 'select';
  CO_SQLTYPE_EXECUTE = 'execute';

implementation

uses UnFMX_DmkWeb, REST.Response.Adapter, System.JSON, UnGrl_Geral, UnGrl_Vars,
  (*UnFMX_Grl_Vars,*) UnGrl_Consts;

{ TUnFMX_DmkRemoteQuery }

function TUnFMX_DmkRemoteQuery.BPGS1I32W(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
var
  Codigo: Integer;
begin
  FMX_DmkRemoteQuery.BPGS1I32W_Novo(Tabela, Campo, _WHERE,
  _AND, TipoSinal, SQLType, DefUpd, Codigo);
  //
  Result := Codigo;
(*
  Result := BuscaProximoGerlSeq1Int32_Web(Tabela, Campo, _WHERE, _AND,
    TipoSinal, SQLType, DefUpd, Null);
*)
end;

function TUnFMX_DmkRemoteQuery.BPGS1I32W_Novo(const Tabela, Campo, _WHERE,
  _AND: String; const TipoSinal: TTipoSinal; const SQLType: TSQLType;
  const DefUpd: Integer; var Codigo: Integer): Boolean;
var
  URL, TS, ST, NextCod: String;
  StatusCode: Integer;
begin
  Codigo := 0;
  URL := (*CO_URL*)VAR_URL + 'buscaProximoGerlSeq1Int32';
  TS  := GetEnumName(TypeInfo(TTipoSinal), Integer(TipoSinal));
  ST  := GetEnumName(TypeInfo(TSQLType), Integer(SQLType));
  //
  NextCod := FMX_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN], ['cnpj', 'ambiente',
  'tabela', 'campo', 'where', 'and', 'tipoSinal', 'sqlType', 'defUpd',
  'reservados'], [VAR_CNPJ_DEVICE_IN_SERVER, Grl_Geral.FF0(VAR_AMBIENTE_APP),
  Tabela, Campo, _WHERE, _AND, TS, ST, Grl_Geral.FF0(DefUpd), ''], URL, StatusCode,
  'json');
  //
  if StatusCode = HTTP_200_OK then
    Codigo := Grl_Geral.IMV(NextCod)
  else
  begin
    if StatusCode = -1 then
      MsgErroServer();
    Grl_Geral.MB_Erro('N�o foi poss�vel obter o pr�ximo ID ' + Tabela + '.' +
    Campo + sLineBreak + sLineBreak + NextCod);
  end;
  //
  Result := Codigo <> 0;
end;

{
function TUnFMX_DmkRemoteQuery.BuscaProximoGerlSeq1Int32_Web(Tabela, Campo,
  _WHERE, _AND: String; TipoSinal: TTipoSinal; SQLType: TSQLType;
  DefUpd: Integer; Reservados: Variant): Integer;
const
  txtNeg = 'BigIntNeg';
  txtPos = 'BigIntPos';
  txtErr = 'BigInt???';
var
  Tab, Fld, MiM: String;
  Atual, P: Integer;
  Forma: TSQLType;
  Sinal: TTipoSinal;
  //DataBase: TmySQLDatabase;
  wSQL, JSON, sVal: String;
begin
  Result := 0;
  case SQLType of
    stUpd: Result := DefUpd;
    stIns:
    begin
      Tab := LowerCase(Tabela);
      P := pos('.', Tab);
      if P > 0 then
        Tab := Copy(Tab, P + 1);
      //
      Fld := txtErr;
      case TipoSinal of
        tsPos: Fld := TxtPos;
        tsNeg: Fld := txtNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Fld := txtNeg
          else
            Fld := txtPos;
        end;
      end;
(*
      if DB = nil then
        DataBase := DMod.MyDB
      else
        DataBase := DB;
      //
      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;');
      QvUpdY.ExecSQL;
      //
      QvLivreY.Close;
      QvLivreY.Database := Database;
      UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
      'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
      'WHERE Tabela="' + LowerCase(Tab) + '" ',
      'AND Campo="' + LowerCase(Campo) + '" ',
      'AND _WHERE="' + LowerCase(_WHERE) + '" ',
      'AND _AND="' + LowerCase(_AND) + '" ',
      '']);
      Atual := QvLivreY.FieldByName('Codigo').AsInteger;
*)
      wSQL := Grl_Geral.ATS([
      'UPDATE gerlseq1 ',
      'SET ' + Fld + '=' + Fld + ' + 1',
      'WHERE Tabela="' + LowerCase(Tab) + '" ',
      'AND Campo="' + LowerCase(Campo) + '" ',
      'AND _WHERE="' + LowerCase(_WHERE) + '" ',
      'AND _AND="' + LowerCase(_AND) + '"; ',
      EmptyStr]);
      if QueryExecute(VAR_CNPJ_DEVICE_IN_SERVER, [wSQL], VAR_AMBIENTE_APP) then
      begin
        wSQL := Grl_Geral.ATS([
        //'LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;',
        'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
        'WHERE Tabela="' + LowerCase(Tab) + '" ',
        'AND Campo="' + LowerCase(Campo) + '" ',
        'AND _WHERE="' + LowerCase(_WHERE) + '" ',
        'AND _AND="' + LowerCase(_AND) + '"; ',
        //'UNLOCK TABLES;',
        EmptyStr]);
        //
        Result := OVUS_I([wSQL]);
(*        if FMX_DmkRemoteQuery.JsonText(wSQL], JSON) then
        begin
          sVal := ParseJson0(JSON);
          Result := Grl_Geral.IMV(sVal);
*)
        end;
      end;
(*
      // Verificar se j� existe
      UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
      'SELECT ' + Campo + ' Codigo FROM ' + Tab,
      'WHERE ' + Campo + '=' + Geral.FF0(Atual),
      Geral.ATS_if(_WHERE<>'', ['AND ' + _WHERE]),
      Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
      '']);

      if (Atual = 0) then
        Forma := stIns
      else
        Forma := stUpd;
      // Evitar duplicidade buscando na tabela de inser��o:
      if (Atual = 0) or (QvLivreY.RecordCount > 0) then
      begin
        case TipoSinal of
          tsPos: MiM := 'MAX(';
          tsNeg: MiM := 'MIN(';
          tsDef:
          begin
            if VAR_ForcaBigIntNeg then
              MiM := 'MIN('
            else
              MiM := 'MAX(';
          end;
          else MiM := 'MINMAX?(';
        end;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
        'SELECT ' + MiM + Campo + ') Codigo FROM ' + Tab,
        Geral.ATS_if(_WHERE<>'', ['WHERE ' + _WHERE]),
        Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
        '']);
        //
        Atual := QvLivreY.FieldByName('Codigo').AsInteger;
      end;
      // Fim evitar duplicidade!
      //
      case TipoSinal of
        tsPos: Sinal := tsPos;
        tsNeg: Sinal := tsNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Sinal := tsNeg
          else
            Sinal := tsPos;
        end;
        else Sinal := tsPos;
      end;

      case Sinal of
        tsPos:
        begin
          if Atual < 1 then
            Result := 1 // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
          else
            Result := Atual + 1;
        end;
        tsNeg:
        begin
          if Atual > -1 then
            Result := -1
          else
            Result := Atual - 1;
        end;
      end;
      //
      // 2013-04-06
      if Reservados <> Null then
      begin
        if Result <= Reservados then
          Result := Reservados + 1;
      end;
      // fim 2013-04-06
      QvLivreY.Close;
      //
      SQLInsUpd(QvUpdY, Forma, 'gerlseq1', False, [
      Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
      Result], [Tab, Campo, _WHERE, _AND], True);
      //
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UNLOCK TABLES;');
      QvUpdY.ExecSQL;
    end;
    else Grl_Geral.MB_Erro(
    '"SQLType" n�o implementado no "BuscaProximoGerlSeq1Int32"');
*)
  end;
  //
end;
}

function TUnFMX_DmkRemoteQuery.ConexaoWebREST_MySQL: Boolean;
var
  Msg: String;
  Ping: Byte;
begin
  Result := False;
  Ping := 0;
  Msg := '';
  try
    if FMX_DmkWeb.VerificaConexaoWeb(Msg) > 0 then
    begin
      Ping := OVUS_I(['SELECT 1']);
      Result := Ping = 1;
    end
    else
      Grl_Geral.MB_Erro('Servidor WEB inacess�vel!');
  except
    Grl_Geral.MB_Erro('Servidor de dados inacess�vel!' + sLineBreak + Msg);
  end;
end;

function TUnFMX_DmkRemoteQuery.CordasDeQrREST_1(const Tabela, Campo: String;
  const MaxItens: Integer; var StrCordas: String): TUMyArray_Str;
const
  Virgula = ', ';
var
  I, N, Conta: Integer;
  Corda: String;
var
  SQL, JSON, Item: String;
begin
  Corda     := EmptyStr;
  StrCordas := EmptyStr;
  I := 0;
  N := 0;
  SetLength(Result, N);
  //
  SQL := 'SELECT ' + Campo + ' FROM ' + Tabela;
  if FMX_DmkRemoteQuery.JsonText([SQL], JSON) then
  begin
    //FMX_Geral.MB_Info(JSON);
    if JSON <> '[]' then
    begin
      Result := ParseJson3(JSON, MaxItens, StrCordas);
      //
    end;
    //Result := True;
  end;
end;

function TUnFMX_DmkRemoteQuery.SQL_Executa(SQL: array of string): Boolean;
begin
  Result := QueryExecute(VAR_CNPJ_DEVICE_IN_SERVER, SQL, VAR_AMBIENTE_APP);
end;

function TUnFMX_DmkRemoteQuery.JsonText(const SQL: array of string; var JSON:
  String): Boolean;
var
 StatusCode: Integer;
begin
  StatusCode := Query(VAR_CNPJ_DEVICE_IN_SERVER, CO_SQLTYPE_SELECT, SQL,
    VAR_AMBIENTE_APP, JSON);
  Result := StatusCode = 200;
  Result :=  Result and (JSON <> EmptyStr);
end;

procedure TUnFMX_DmkRemoteQuery.JsonToDataset(DataSet: TDataSet; JSON: String);
var
  JsonObj: TJSONArray;
  JsonDS: TCustomJSONDataSetAdapter;
begin
  if JSON = EmptyStr then
  begin
    Exit;
  end;

  JsonObj := TJSONObject.ParseJSONValue(JSON) as TJSONArray;
  JsonDS  := TCustomJSONDataSetAdapter.Create(Nil);

  try
    JsonDS.Dataset := DataSet;
    JsonDS.UpdateDataSet(JsonObj);
  finally
    JsonDS.Free;
    JsonObj.Free;
  end;
end;

procedure TUnFMX_DmkRemoteQuery.MsgErroServer();
begin
  Grl_Geral.MB_Erro('N�o foi poss�vel acessar o servidor! ' + sLineBreak +
  'Verifique se o wifi ativo possui internet ou utiliza os dados m�veis!');
end;

function TUnFMX_DmkRemoteQuery.ObtemValorUnicoSQL(CNPJ: String; SQL:
  array of string; Ambiente: Integer): String;
var
  JSON: String;
begin
  if FMX_DmkRemoteQuery.JsonText(SQL, JSON) then
    Result := ParseJson0(JSON);
  if Result = 'null' then
    Result := '';
end;

function TUnFMX_DmkRemoteQuery.OVUS_I(SQL: array of string): Integer;
var
  sVal: String;
begin
  sVal   := ObtemValorUnicoSQL(VAR_CNPJ_DEVICE_IN_SERVER, SQL, VAR_AMBIENTE_APP);
  if sVal = 'null' then
    Result := 0
  else
    Result := Grl_Geral.IMV(sVal);
end;

function TUnFMX_DmkRemoteQuery.OVUS_X(SQL: array of string): String;
begin
  Result := ObtemValorUnicoSQL(VAR_CNPJ_DEVICE_IN_SERVER, SQL, VAR_AMBIENTE_APP);
end;

function TUnFMX_DmkRemoteQuery.ParseJson0(StrJson: String): String;
const
   Registro = 0;
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
begin
  Result := '';
{$IfDef ANDROID}
  LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(StrJson),0) as TJSONArray;
{$Else}
  LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(StrJson),0) as TJSONArray;
{$EndIf}
{
  for LJsonValue in LJsonArr do
  begin
    for LItem in TJSONArray(LJsonValue) do
      FMX_Geral.MB_Info('Registro ' + FMX_Geral.FF0(Registro) + ' > ' +
      Format('%s : %s',[TJSONPair(LItem).JsonString.Value, TJSONPair(LItem).JsonValue.Value]));
   Registro := Registro + 1;
  end;
}
  for LJsonValue in LJsonArr do
  begin
    for LItem in TJSONArray(LJsonValue) do
    begin
      Result := TJSONPair(LItem).JsonValue.Value;
      Exit;
    end;
  end;
end;

procedure TUnFMX_DmkRemoteQuery.ParseJson1(StrJson: String);
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  Registro: Integer;
begin
// s� teste. Ainda n�o usado!!!!
   Registro := 1;
{$IfDef ANDROID}
   LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(StrJson),0) as TJSONArray;
// s� teste. Ainda n�o usado!!!!
{$Else}
   LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(StrJson),0) as TJSONArray;
// s� teste. Ainda n�o usado!!!!
{$EndIf}
   for LJsonValue in LJsonArr do
   begin
// s� teste. Ainda n�o usado!!!!
      for LItem in TJSONArray(LJsonValue) do
// s� teste. Ainda n�o usado!!!!
        Grl_Geral.MB_Aviso('Json > Registro ' + Grl_Geral.FF0(Registro) + ' > ' +
// s� teste. Ainda n�o usado!!!!
        Format('%s : %s',[TJSONPair(LItem).JsonString.Value, TJSONPair(LItem).JsonValue.Value]));
// s� teste. Ainda n�o usado!!!!
     Registro := Registro + 1;
   end;
end;

function TUnFMX_DmkRemoteQuery.ParseJson2(StrJson: String): String;
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  I, J, N: Integer;
  Valor: String;
begin
  Result := '';
{$IfDef ANDROID}
  LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(StrJson),0) as TJSONArray;
{$Else}
  LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(StrJson),0) as TJSONArray;
{$EndIf}
  N := TJSONArray(LJsonArr).Count - 1;
  J := 0;
  for LJsonValue in LJsonArr do
  begin
    //Itens := TJSONArray(LJsonValue).Count;
    I := 0;
    for LItem in TJSONArray(LJsonValue) do
    begin
      if TJSONPair(LItem).JsonValue.Value = 'null' then
        Valor := '' // null
      else
      begin
        Valor := TJSONPair(LItem).JsonValue.Value;
        //if Pos('''', Valor) > 0 then
          //Valor := StringReplace(Valor, '''', '''''', [rfReplaceAll, rfIgnoreCase]);
        if Pos('"', Valor) > 0 then
          Valor := StringReplace(Valor, '"', '""', [rfReplaceAll, rfIgnoreCase]);
        if Pos('!', Valor) > 0 then
          Valor := StringReplace(Valor, '!', '!!', [rfReplaceAll, rfIgnoreCase]);
{
        if Pos('#', Valor) > 0 then
          Valor := StringReplace(Valor, '#', '##', [rfReplaceAll, rfIgnoreCase]);
        if Pos('/', Valor) > 0 then
          Valor := StringReplace(Valor, '/', '//', [rfReplaceAll, rfIgnoreCase]);
        if Pos('\', Valor) > 0 then
          Valor := StringReplace(Valor, '\', '\\', [rfReplaceAll, rfIgnoreCase]);
}
      end;
      //
      //FMX_Geral.MB_Info('Registro ' + FMX_Geral.FF0(Registro) + ' > ' +
      //Format('%s : %s',[TJSONPair(LItem).JsonString.Value, TJSONPair(LItem).JsonValue.Value]));
      if I > 0 then
        Result := Result + ',"' + Valor + '"'
      else
        Result := Result + '("' + Valor + '"';
      I := I +1;
    end;
    if J = N then
      Result := Result + ');' + sLineBreak
    else
      Result := Result + '),' + sLineBreak;
    J := J + 1;
  end;
  //Result := 'VALUES ' + sLineBreak + Result + sLineBreak + ';';
  Result := 'VALUES ' + sLineBreak + Result + sLineBreak;
end;

function TUnFMX_DmkRemoteQuery.ParseJson3(const StrJson: String; const MaxItens: Integer; var StrCordas: String): TUMyArray_Str;
const
  Virgula = ', ';
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  I, J, N, K, L: Integer;
  Valor: String;
  Corda: String;
(*
var
  SQL, JSON: String;
*)
begin
  Corda     := EmptyStr;
  StrCordas := EmptyStr;
  I := 0;
  N := 0;
  K := 0;
  L := 0;
  SetLength(Result, N);
  //
{$IfDef ANDROID}
  LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(StrJson),0) as TJSONArray;
{$Else}
  LJsonArr    := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(StrJson),0) as TJSONArray;
{$EndIf}
  N := TJSONArray(LJsonArr).Count - 1;
  J := 0;
  for LJsonValue in LJsonArr do
  begin
    //Itens := TJSONArray(LJsonValue).Count;
    I := 0;
    for LItem in TJSONArray(LJsonValue) do
    begin
      if TJSONPair(LItem).JsonValue.Value = 'null' then
        Valor := '' // null
      else
      begin
        Valor := TJSONPair(LItem).JsonValue.Value;
{
        //if Pos('''', Valor) > 0 then
          //Valor := StringReplace(Valor, '''', '''''', [rfReplaceAll, rfIgnoreCase]);
        if Pos('"', Valor) > 0 then
          Valor := StringReplace(Valor, '"', '""', [rfReplaceAll, rfIgnoreCase]);
        if Pos('!', Valor) > 0 then
          Valor := StringReplace(Valor, '!', '!!', [rfReplaceAll, rfIgnoreCase]);
(*
        if Pos('#', Valor) > 0 then
          Valor := StringReplace(Valor, '#', '##', [rfReplaceAll, rfIgnoreCase]);
        if Pos('/', Valor) > 0 then
          Valor := StringReplace(Valor, '/', '//', [rfReplaceAll, rfIgnoreCase]);
        if Pos('\', Valor) > 0 then
          Valor := StringReplace(Valor, '\', '\\', [rfReplaceAll, rfIgnoreCase]);
*)
}
      end;
      //
{
      //FMX_Geral.MB_Info('Registro ' + FMX_Geral.FF0(Registro) + ' > ' +
      //Format('%s : %s',[TJSONPair(LItem).JsonString.Value, TJSONPair(LItem).JsonValue.Value]));
      if I > 0 then
        Result := Result + ',"' + Valor + '"'
      else
        Result := Result + '("' + Valor + '"';
      I := I +1;
}
      if I > 0 then
        Grl_Geral.MB_Info('Registro ' + IntToStr(J) + ' com mais de um valor! >> "' + Valor + '"');
      I := I + 1;
    end;
    if K > 0 then
      Corda := Corda + Virgula + Valor
    else
      Corda := Valor;
    J := J + 1;
    K := K + 1;
    if K = MaxItens then
    begin
      SetLength(Result, L + 1);
      Result[L] := Corda;
      if StrCordas <> EmptyStr then
        StrCordas := StrCordas + Virgula;
      StrCordas := StrCordas + Corda;
      Corda     := EmptyStr;
      L := L + 1;
      K := 0;
    end;
  end;
  if Corda <> EmptyStr then
  begin
    SetLength(Result, L + 1);
    Result[L] := Corda;
    if StrCordas <> EmptyStr then
      StrCordas := StrCordas + Virgula;
    StrCordas := StrCordas + Corda;
  end;
end;

function TUnFMX_DmkRemoteQuery.Query(const CNPJ, SQLType: String; const SQL:
  array of string; const Ambiente: Integer; var Resultado: String): Integer;
const
  Recurso = 'sql/executa';
var
  //Url,
  SQLStr: String;
  I, StatusCode: Integer;
begin
  Result    := 0;
  Resultado := '';

  if CNPJ = EmptyStr then
  begin
    Grl_Geral.MB_Aviso('O par�metro CNPJ n�o foi informado!');
    Exit;
  end;

  if Length(SQL) = 0 then
  begin
    Grl_Geral.MB_Aviso('O par�metro SQL n�o foi informado!');
    Exit;
  end;

  SQLStr := '';

  for I := Low(SQL) to High(SQL) do
  begin
    SQLStr := SQLStr + ' ' + SQL[I];
  end;

  //Url := (*CO_URL*)VAR_URL + 'sql/executa';
  //Url := (*CO_URL*)VAR_URL + 'sql/executa/v2';

  Resultado := FMX_dmkWeb.REST_URL_Post_2(
  //['token'], [CO_TOKEN], ['cnpj', 'sql', 'ambiente'], [CNPJ, SQLStr, Grl_Geral.FF0(Ambiente)],

    VAR_URL, Recurso, CO_TOKEN, CNPJ, IntToStr(Ambiente), Grl_Geral.ATS(SQL),

    StatusCode);
  Result := StatusCode;
end;

function TUnFMX_DmkRemoteQuery.QueryExecute(CNPJ: String; SQL: array of string;
  Ambiente: Integer): Boolean;
var
  Res, StrErr: String;
  StatusCode: Integer;
begin
  Result := False;
  StatusCode := Query(CNPJ, CO_SQLTYPE_EXECUTE, SQL, Ambiente, Res);
  //
  if StatusCode > 0 then
  begin
    if StatusCode = HTTP_200_OK then
    begin
      if (LowerCase(Res) = 'true')
      or (
        (Grl_Geral.SoNumero_TT(Res) = Res)
        and
        (Grl_Geral.IMV(Res)> 0)
      )
      then
        Result := True
      else
        Grl_Geral.MB_Erro('Retorno 200 OK '+ sLineBreak +
        'Res = ' + Res + sLineBreak +
        'Erro de SQL?: ' + sLineBreak + Grl_Geral.ATS(SQL));
    end else
    begin
      StrErr := Grl_Geral.FF0(StatusCode) + ' - ' +
        Grl_DmkWeb.GetMeaningHTTP_CodeResponse(StatusCode) + sLineBreak + Res;
      if StatusCode = HTTP_422_ERRO_DE_SEMANTICA_DO_LADO_CLIENTE then
      begin
        StrErr := StrErr + sLineBreak + Grl_Geral.ATS(SQL);
      end;
      Grl_Geral.MB_Erro(StrErr);
    end;
  end;
end;

function TUnFMX_DmkRemoteQuery.QuerySelect(DataSet: TDataSet; CNPJ: String;
  SQL: array of string; Ambiente: Integer): Boolean;
var
  JSON, StrErr: String;
  StatusCode: Integer;
begin
  DataSet.Close;
  Result := False;
  StatusCode := Query(CNPJ, CO_SQLTYPE_SELECT, SQL, Ambiente, JSON);
  if StatusCode > 0 then
  begin
    if StatusCode = HTTP_200_OK then
    begin
      if JSON <> EmptyStr then
      begin
        JsonToDataset(DataSet, JSON);
      end;
      Result := True;
    end else
    begin
      StrErr := Grl_Geral.FF0(StatusCode) + ' - ' +
        Grl_DmkWeb.GetMeaningHTTP_CodeResponse(StatusCode) + sLineBreak + JSON;
      if StatusCode = HTTP_422_ERRO_DE_SEMANTICA_DO_LADO_CLIENTE then
      begin
        StrErr := StrErr + sLineBreak + Grl_Geral.ATS(SQL);
      end;
      Grl_Geral.MB_Erro(StrErr);
    end;
(*
    if StatusCode = HTTP_400_BAD_REQUEST then
    begin
      Grl_Geral.MB_Erro(JSON);
    end else
    Grl_Geral.MB_Erro('Ocorreu um erro de SQL:' + sLineBreak +
      Grl_Geral.ATS(SQL));
*)
  end;
end;

end.

