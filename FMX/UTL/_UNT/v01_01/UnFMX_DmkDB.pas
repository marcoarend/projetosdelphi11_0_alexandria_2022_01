unit UnFMX_DmkDB;

interface

uses System.Generics.Collections, System.Classes, System.UITypes,
  System.SysUtils, FMX.Forms, FMX.ListBox, FMX.StdCtrls, FMX.Dialogs,
  FireDAC.Comp.Client, System.IOUtils, FMX.Memo, UnDmkEnums, System.Variants,
  Data.DB, UnGrl_DmkDB, UnGrl_Vars, UnMyLinguas;

type
  TUnFMX_DmkDB = class(TObject)
  private

  public
    function  ConfiguraDB(DataBase: TFDConnection; DriverNome: TDriverName;
              NomeDataBase: String; PassEncrypt: String = ''): Boolean;
    (*
    Movido para UnGrl_DmkDB
    function  ObtemVersaoAppDB(Query: TFDQuery; DataBase: TFDConnection;
              DriverNome: TDriverName): Integer;
    function  AbreQuery(Query: TFDQuery; DataBase: TFDConnection): Boolean;
    function  AbreSQLQuery0(Query: TFDQuery; DataBase: TFDConnection;
              SQL: array of String): Boolean;
    function  EfetuaVerificacoes(DBName: String; Memo: TMemo; Estrutura,
              EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
              LBIAvisoP1, LBIAvisoP2, LBIAviso1R, LBIAviso2R, LBIAviso1B,
              LBIAviso2B, LBIAviso1G, LBIAviso2G, LBITempoI, LBITempoF,
              LBITempoT: TListBoxItem; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DriverNome: TDriverName;
              DataBase: TFDConnection; VersaoAtual: Integer): Boolean;
    function  ExecutaSQLQuery0(Query: TFDQuery; DataBase: TFDConnection;
              SQL: array of String): Boolean;
    function  CarregaSQLInsUpd(QrUpd: TFDQuery; DataBase: TFDConnection;
              Tipo: TSQLType; Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String; ValCampos,
              ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String; Device: TDeviceType;
              Sincro: Boolean = False): Boolean;
    function  ObtemCodigoInt(Tabela, Campo: String; Query: TFDQuery;
              DataBase: TFDConnection): Integer;
    function  ObtemCodigoInt_Sinc(Tabela, Campo: String; Query: TFDQuery;
              DataBase: TFDConnection; SQLTipo: TSQLType): Integer;
    function  ValidaDadosCampo(Query: TFDQuery; DB: TFDConnection; Campo,
              Tabela: String; Valor: Integer; PemiteZero: Boolean = False): Boolean;
    function  ObtemIndexDeTabela(Query: TFDQuery; CampoCodigo: String; Valor: Integer): Integer;
    procedure GravaAviso(Aviso: String; Memo: TMemo);
    procedure MostraSQL(Query: TFDQuery; Memo: TMemo; Titulo: String);
    procedure AtualizaVersaoTBControle(DataBase: TFDConnection; Versao: Integer);
    procedure RemoveUsuarios(DataBase: TFDConnection; DriverNome: TDriverName);
    *)
    //S I N C R O N I S M O  W E B
    (*
    Movido para UnGrl_DmkDB
    function  ObtemDataSincro(DataBase: TFDConnection; DriverNome: TDriverName;
              Tabela: String): TDateTime;
    procedure AtualizaSincroDB(DataBase: TFDConnection; DriverNome: TDriverName;
              Tabela, DataHora: String);
    *)
    //S Q L I T E
    (*
    Movido para UnGrl_DmkDB
    function  EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo; Estrutura,
              EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
              LBIAvisoP1, LBIAvisoP2, LBIAviso1R, LBIAviso2R, LBIAviso1B,
              LBIAviso2B, LBIAviso1G, LBIAviso2G, LBITempoI, LBITempoF,
              LBITempoT: TListBoxItem; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DataBase: TFDConnection): Boolean;
    function  VerificaCampos_SQLite(DBName: String; TabelaNome,
              TabelaBase: String; Memo: TMemo; RecriaRegObrig,
              Tem_Del, Tem_Sync: Boolean; LBIAviso1B, LBIAviso2B, LBIAviso1G,
              LBIAviso2G: TListBoxItem; MeFldsNoNeed: TMemo;
              DataBase: TFDConnection): TResultVerify;
    function  TabelaExiste_SQLite(Tabela: String; QrTabs: TFDQuery): Boolean;
    function  CriaTabela_SQLite(DBName: String; TabelaNome, TabelaBase: String;
              Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean; LBIAviso1B,
              LBIAviso2B, LBIAviso1G, LBIAviso2G: TListBoxItem;
              DataBase: TFDConnection; Tem_Sync: Boolean = False): Boolean;
    function  CriaIndice_SQLite(Tabela, IdxNome, Aviso: String; Memo: TMemo;
              DataBase: TFDConnection): TResultVerify;
    function  VerificaEstrutura_SQLite(DBName: String; Memo: TMemo;
              RecriaRegObrig: Boolean; LBIAviso1R, LBIAviso2R, LBIAviso1B,
              LBIAviso2B, LBIAviso1G, LBIAviso2G, LBITempoI, LBITempoF,
              LBITempoT: TListBoxItem; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DataBase: TFDConnection): Boolean;
    function  IndiceExiste_SQLite(Tabela, Indice: String;
              Memo: TMemo; DataBase: TFDConnection): Integer;
    function  RecriaIndice_SQLite(Tabela, IdxNome, Aviso: String;
              Memo: TMemo; DataBase: TFDConnection): TResultVerify;
    function  ExcluiIndice_SQLite(DBName: String; Tabela, IdxNome,
              Aviso: String; Motivo: TMyMotivo; Memo: TMemo;
              DataBase: TFDConnection): TResultVerify;
    function  CampoExiste_SQLite(Tabela, Campo: String; Memo: TMemo;
              QrNTV: TFDQuery): Integer;
    function  CampoAtivo_SQLite(DBName: String; TabelaNome, TabelaBase,
              Campo: String; Memo: TMemo; QrNTV: TFDQuery;
              DataBase: TFDConnection): Integer;
    function  ExcluiRegistroInt1(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TFDConnection): Integer;
    function  ExcluiRegistroInt1_Sinc(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TFDConnection): Integer;
    procedure AdicionaDefault_SQLite(var Opcoes: String; Tipo, Padrao: String);
    procedure AdicionaFldNoNeed_SQLite(MeFldsNoNeed: TMemo; Tabela, Campo: String);
    function  ObtemListaDeColunasDeTabela(DataBase: TFDConnection;
              DriverNome: TDriverName; Tabela: String; TipoColuna: TTipoColuna;
              var Colunas: TStringList): Boolean;
    *)
  end;

var
  FMX_dmkDB: TUnFMX_DmkDB;

implementation

uses UnFMX_DmkProcFunc, MyListas, UnFMX_Geral, UnFMX_Grl_Vars, UnGrl_Consts;

{ TUnFMX_DmkDB }

function TUnFMX_DmkDB.ConfiguraDB(DataBase: TFDConnection;
  DriverNome: TDriverName; NomeDataBase: String; PassEncrypt: String = ''): Boolean;
var
  DBNome, NomeDriver: String;
begin
  Result := True;
  if DataBase.Connected then
    DataBase.Connected := False;
  //
  case DriverNome of
    istMySQL:
    begin
      FMX_Geral.MB_Aviso('N�o implementado!');
      Result := False;
    end;
    istSQLite:
    begin
      NomeDriver := 'SQLite';
      //
      try
        {$IF DEFINED(iOS) or DEFINED(ANDROID)}
        DBNome := TPath.Combine(TPath.GetDocumentsPath, NomeDataBase + '.s3db');
        {$ELSE}
        DBNome := ExtractFilePath(ParamStr(0)) + NomeDataBase + '.s3db';
        {$ENDIF}
        DataBase.Params.Values['Database'] := DBNome;
        //
        if PassEncrypt <> '' then
        begin
          DataBase.Params.Values['Encrypt']  := 'aes-256';
          DataBase.Params.Values['Password'] := PassEncrypt;
        end else
        begin
          DataBase.Params.Values['Encrypt'] := 'No';
        end;
        //
        DataBase.DriverName  := NomeDriver;
        DataBase.LoginPrompt := False;
        DataBase.Connected   := True;
        Result               := True;
      except
        Result := False;
      end;
    end;
  end;
end;

(*
Movido para UnGrl_DmkDB
function TUnFMX_DmkDB.ObtemVersaoAppDB(Query: TFDQuery;
  DataBase: TFDConnection; DriverNome: TDriverName): Integer;
var
  Versao: Integer;
begin
  //True  = Tabelaas j� criadas
  //False = Tabelas n�o criadas
  Versao := 0;

  case DriverNome of
    istMySQL:
      begin
        FMX_Geral.MB_Aviso('N�o implementado');
      end;
    istSQLite:
      begin
        DataBase.ExecSQL('CREATE TABLE IF NOT EXISTS controle (Codigo INT(11) PRIMARY KEY NOT NULL DEFAULT 0, Versao BIGINT(20) NOT NULL DEFAULT 0);');

        AbreSQLQuery0(Query, DataBase, [
          'SELECT Versao ',
          'FROM controle ',
          '']);

        if Query.RecordCount > 0 then
        begin
          Versao := Query.FieldByName('Versao').AsInteger
        end else
        begin
          CarregaSQLInsUpd(Query, DataBase, stIns, 'controle', False,
            ['Versao'], ['Codigo'], [0], [1], False, False, '', stMobile);
        end;
        Query.Close;
      end;
  end;
  Result := Versao;
end;

function TUnFMX_DmkDB.AbreQuery(Query: TFDQuery;
  DataBase: TFDConnection): Boolean;
begin
  Result := False;
  try
    Query.Close;
    Query.Connection := DataBase;
    Query.Open();
    //
    Result := True;
  finally
    ;
  end;
end;

function TUnFMX_DmkDB.AbreSQLQuery0(Query: TFDQuery; DataBase: TFDConnection;
  SQL: array of String): Boolean;
var
  I: Integer;
begin
  Result := False;
  Query.Close;
  Query.Connection := DataBase;
  //
  try
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      FMX_Geral.MB_Erro('Texto da SQL indefinido no SQLQuery!' + sLineBreak +
        'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.Open;
      Result := True;
    end;
  except
    on e: Exception do
    begin
      FMX_Geral.MB_Erro('Erro ao tentar abrir uma SQL no SQLQuery!' +
        sLineBreak + TComponent(Query.Owner).Name + '.' + Query.Name +
        sLineBreak + e.Message + sLineBreak + Query.SQL.Text);
    end;
  end;
end;

function TUnFMX_DmkDB.EfetuaVerificacoes(DBName: String;
  Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
  RecriaRegObrig: Boolean; LBIAvisoP1, LBIAvisoP2, LBIAviso1R, LBIAviso2R,
  LBIAviso1B, LBIAviso2B, LBIAviso1G, LBIAviso2G, LBITempoI, LBITempoF,
  LBITempoT: TListBoxItem; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; DriverNome: TDriverName; DataBase: TFDConnection;
  VersaoAtual: Integer): Boolean;
begin
  Result := False;
  //
  case DriverNome of
    istMySQL:
    begin
      FMX_Geral.MB_Aviso('N�o implementado!');
      Result := False;
    end;
    istSQLite:
    begin
      Result := EfetuaVerificacoes_SQLite(DBName, Memo, Estrutura,
                  EstrutLoc, Controle, Pergunta, RecriaRegObrig, LBIAvisoP1,
                  LBIAvisoP2, LBIAviso1R, LBIAviso2R, LBIAviso1B, LBIAviso2B,
                  LBIAviso1G, LBIAviso2G, LBITempoI, LBITempoF, LBITempoT,
                  MeTabsNoNeed, PB, MeFldsNoNeed, DataBase);
      if Result then
      begin
        if DBName = CO_DBNome then
          AtualizaVersaoTBControle(DataBase, VersaoAtual);
      end;
    end;
  end;
end;

function TUnFMX_DmkDB.ExecutaSQLQuery0(Query: TFDQuery; DataBase: TFDConnection;
  SQL: array of String): Boolean;
var
  I: Integer;
begin
  Result := False;
  try
    Query.Close;

    Query.Connection := DataBase;

    if High(SQL) > -1 then
    begin
      Query.SQL.Clear;

      for I := Low(SQL) to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;

    if Query.SQL.Text = '' then
    begin
      FMX_Geral.MB_Erro('Texto da SQL indefinido no "ExecutaSQLQuery0"!' + sLineBreak +
      'Avise a DERMATEK!');

      Result := False;
    end else
    begin
      Query.ExecSQL;

      Result := True;
    end;
  except
    on E: Exception do
    begin
      FMX_Geral.MB_Erro('Erro ao tentar executar uma SQL no "ExecutaSQLQuery0"!' +
      sLinebreak + E.Message + sLineBreak + Query.SQL.Text);
    end;
  end;
end;

function TUnFMX_DmkDB.CarregaSQLInsUpd(QrUpd: TFDQuery;
  DataBase: TFDConnection; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean; SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String; Device: TDeviceType; Sincro: Boolean = False): Boolean;
var
  QueryLoc: TFDQuery;
  LastAcao: TLastAcao;
  AlterWeb: TAlterWeb;
  i, j, k, CliIntSync: Integer;
  LastModifi: TDateTime;
  Valor, Liga, Data, LastModifi_Txt, Tab, Campos, Valores, SQLWhere: String;
begin
  Result := False;
  i      := High(SQLCampos);
  j      := High(ValCampos);

  if i <> j then
  begin
    FMX_Geral.MB_Erro('ERRO! Existem ' + FMX_Geral.FF0(i+1) + ' campos e ' +  FMX_Geral.FF0(j+1) +
      ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  i := High(SQLIndex);
  j := High(ValIndex);

  if i <> j then
  begin
    FMX_Geral.MB_Erro('ERRO! Existem ' + FMX_Geral.FF0(i+1) + ' �ndices e ' +
      FMX_Geral.FF0(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  if (i >= 0) and Auto_increment then
  begin
    FMX_Geral.MB_Erro('AVISO! Existem ' + FMX_Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  Tab      := LowerCase(Tabela);
  AlterWeb := alDsk;

  case Device of
    stDesktop:
      AlterWeb := alDsk;
    stWeb:
      AlterWeb := alWeb;
    stMobile:
      AlterWeb := alMob;
  end;

  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    FMX_Geral.MB_Erro('AVISO: O status da a��o est� definida como ' +
      '"' + Grl_DmkDB.NomeTipoSQL(Tipo) + '"');
  end;

  Data := #39 + FMX_Geral.FDT(Date, 1) + #39;

  QrUpd.SQL.Clear;
  QrUpd.Connection := DataBase;

  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' (')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' (');
  end else
  begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');

    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb='+ FMX_Geral.FF0(Integer(AlterWeb)) +', ');
  end;

  Campos  := '';
  Valores := '';
  j       := System.High(SQLCampos);

  for i := System.Low(SQLCampos) to j do
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', ' + SQLCampos[i];
      Valor   := FMX_Geral.VariavelToString(ValCampos[i]);
      Valores := Valores + ', ' + Valor;
    end else
    begin
      if SQLCampos[i] = CO_JOKE_SQL then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
          QrUpd.SQL.Add(ValCampos[i] + ', ')
        else
          QrUpd.SQL.Add(ValCampos[i]);
      end else
      begin
        Valor := FMX_Geral.VariavelToString(ValCampos[i]);

        if (i < j) or UserDataAlterweb then
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
        else
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
      end;
    end;
  end;

  if Sincro then
  begin
    LastModifi     := FMX_dmkPF.DateTime_MyTimeZoneToUTC(Now);
    LastModifi_Txt := #39 + FMX_Geral.FDT(LastModifi, 109) + #39 ;
    CliIntSync     := VAR_WEB_USER_USRENT;

    if Tipo = stIns then
    begin
      LastAcao := laIns; //Inclus�o
    end else
    begin
      //Se n�o faz sincronismo, marter o "lastacao" = "inclus�o" por causa do sincronismo
      LastAcao := laUpd;//Altera��o
      SQLWhere := '';

      for k := Low(SQLIndex) to High(SQLIndex) do
      begin
        SQLWhere := SQLWhere + ' AND ' + SQLIndex[k] + '="'+ FMX_dmkPF.VariantToString(ValIndex[k]) +'"';
      end;
      QueryLoc := TFDQuery.Create(DataBase);
      try
        AbreSQLQuery0(QueryLoc, DataBase, [
          'SELECT LastAcao ',
          'FROM ' + Tabela,
          'WHERE AlterWeb=2 ',
          SQLWhere,
          '']);
        if QueryLoc.RecordCount > 0 then
        begin
          if QueryLoc.FieldByName('LastAcao').AsInteger = 0 then //Inclus�o
            LastAcao := laIns;
        end;
      finally
        QueryLoc.Free;
      end;
    end;

    if Tipo = stIns then
    begin
      Campos  := Campos + ', LastModifi, LastAcao, CliIntSync';
      Valores := Valores + ', ' + LastModifi_Txt + ', ' + FMX_Geral.FF0(Integer(LastAcao)) +
                   ', ' + FMX_Geral.FF0(CliIntSync);
    end else
      QrUpd.SQL.Add('LastModifi=' + LastModifi_Txt + ', LastAcao=' +
        FMX_Geral.FF0(Integer(LastAcao)) + ', CliIntSync=' + FMX_Geral.FF0(CliIntSync) + ', ');
  end;

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', DataCad, UserCad, AlterWeb';
      Valores := Valores + ', ' + Data + ', ' + FMX_Geral.FF0(VAR_USUARIO) + ', ' + FMX_Geral.FF0(Integer(AlterWeb));
    end else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + FMX_Geral.FF0(VAR_USUARIO));
  end;

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      FMX_Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
  end else
  begin
    for i := System.Low(SQLIndex) to System.High(SQLIndex) do
    begin
      if Tipo = stIns then
      begin
        Liga := ', ';
      end else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;

      if SQLIndex[i] = CO_JOKE_SQL then
      begin
        QrUpd.SQL.Add(Liga + ValIndex[i]);
      end else
      begin
        if Tipo = stIns then
        begin
          Campos  := Campos + ', ' + SQLIndex[i];
          Valor   := FMX_Geral.VariavelToString(ValIndex[i]);
          Valores := Valores + ', ' + Valor;
        end else
        begin
          Valor := FMX_Geral.VariavelToString(ValIndex[i]);

          QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;

  if Tipo = stIns then
  begin
    Campos  := Campos.Substring(2) + ') VALUES (';
    Valores := Valores.Substring(2) + ')';

    QrUpd.SQL.Add(Campos);
    QrUpd.SQL.Add(Valores);
  end;

  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;

  QrUpd.SQL.Add(';');
  QrUpd.ExecSQL();
  Result := True;
end;

function TUnFMX_DmkDB.ObtemCodigoInt(Tabela, Campo: String; Query: TFDQuery;
  DataBase: TFDConnection): Integer;
var
  Atual: Integer;
begin
  AbreSQLQuery0(Query, DataBase, [
    'SELECT MAX(' + Campo + ') ' + Campo,
    'FROM ' + Tabela]);

  if Query.FieldByName(Campo).AsVariant <> Null then
    Atual := Query.FieldByName(Campo).AsVariant
  else
    Atual := 0;

  Result := Atual + 1;
end;

function TUnFMX_DmkDB.ObtemCodigoInt_Sinc(Tabela, Campo: String;
  Query: TFDQuery; DataBase: TFDConnection; SQLTipo: TSQLType): Integer;
const
  CO_MAX: Integer = 3; //Por enquanto o m�ximo de dispositivos ser� 3 casas
var
  MaxCasas, MaxCodigo, MinCodigo, Device, Codigo: Integer;
begin
  Result := 0;
  Device := VAR_WEB_USER_USRNUM;

  if SQLTipo = stUpd then
  begin
    Exit;
  end;

  if Device = 0 then
  begin
    FMX_Geral.MB_Aviso('Dispositivo n�o registrado!');
    Exit;
  end;

  if Length(FMX_Geral.FF0(Device)) > CO_MAX then
  begin
    FMX_Geral.MB_Aviso('Limite de dispositivos atingido!' + sLineBreak +
      'Avise a Dermatek!');
    Exit;
  end;

  MaxCasas  := Length(FMX_Geral.FF0(High(Integer))) - CO_MAX;
  MaxCodigo := FMX_Geral.IMV('1' + FMX_Geral.FFN(0, MaxCasas)) - 1;
  MaxCodigo := FMX_Geral.IMV(FMX_Geral.FF0(VAR_WEB_USER_USRNUM) + FMX_Geral.FF0(MaxCodigo));
  MinCodigo := FMX_Geral.IMV(FMX_Geral.FF0(VAR_WEB_USER_USRNUM) + FMX_Geral.FFN(0, MaxCasas));

  AbreSQLQuery0(Query, DataBase, [
    'SELECT MAX(' + Campo + ') Campo ',
    'FROM ' + Tabela,
    'WHERE ' + Campo + ' >= ' + FMX_Geral.FF0(MinCodigo),
    'AND ' + Campo + ' < ' + FMX_Geral.FF0(MaxCodigo),
    '']);

  if Query.RecordCount > 0 then
  begin
    if Query.FieldByName('Campo').AsVariant <> Null then
    begin
      Codigo := Query.FieldByName(Campo).AsVariant + 1;

      if Codigo <= MaxCodigo then
      begin
        Result := Codigo;
      end else
      begin
        FMX_Geral.MB_Aviso('Limite de ID atingido para a tabela '+ Tabela +'!' +
          sLineBreak + 'Avise a Dermatek!');
        //
        Result := 0;
      end;
    end else
    begin
      Result := MinCodigo;
    end;
  end else
  begin
    Result := MinCodigo;
  end;
  Query.Close;
end;

procedure TUnFMX_DmkDB.GravaAviso(Aviso: String; Memo: TMemo);
begin
  if Memo <> nil then
    Memo.Lines.Add(Aviso);
end;

procedure TUnFMX_DmkDB.MostraSQL(Query: TFDQuery; Memo: TMemo;
  Titulo: String);
var
  I: Integer;
begin
  Memo.Lines.Add('===== ' + Titulo + ' =====');

  for I := 0 to Query.SQL.Count -1 do
    Memo.Lines.Add(Query.SQL[I]);
end;

procedure TUnFMX_DmkDB.AtualizaVersaoTBControle(DataBase: TFDConnection;
  Versao: Integer);
var
  QueryUpd: TFDQuery;
begin
  QueryUpd := TFDQuery.Create(DataBase);
  try
    CarregaSQLInsUpd(QueryUpd, DataBase, stUpd, 'controle', False,
      ['Versao'], ['Codigo'], [Versao], [1], False, False, '', stMobile);
  finally
    if QueryUpd <> nil then
      QueryUpd.Free;
  end;
end;

function TUnFMX_DmkDB.EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo;
  Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LBIAvisoP1,
  LBIAvisoP2, LBIAviso1R, LBIAviso2R, LBIAviso1B, LBIAviso2B, LBIAviso1G,
  LBIAviso2G, LBITempoI, LBITempoF, LBITempoT: TListBoxItem;
  MeTabsNoNeed: TMemo; PB: TProgressBar; MeFldsNoNeed: TMemo;
  DataBase: TFDConnection): Boolean;
var
  Qry: TFDQuery;
  Res: Integer;
begin
  Result := False;
  Qry := TFDQuery.Create(DataBase);
  try
    MeTabsNoNeed.Lines.Clear;

    Res := FMX_Geral.MB_Pergunta('Ser� realizada uma verifica��o na base de dados: ' +
              sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
              'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
              'perante restaura��o de backup.');

    if Res <> mrYes then
      Exit;

    Application.ProcessMessages;

    Result    := True;
    FPergunta := Pergunta;
    try
      // verifica se a vers�o atual � mais nova
      // se n�o � impede a verifica��o
      if DBName <> '' then
      begin
        AbreSQLQuery0(Qry, DataBase, [
        'SELECT name FROM sqlite_master ',
        'WHERE type=''table''',
        'AND name LIKE ''Controle''',
        'ORDER BY name;',
        '']);
         if Qry.RecordCount > 0 then
        begin
          AbreSQLQuery0(Qry, DataBase, [
          'PRAGMA table_info(controle);',
          '']);
          if Qry.RecordCount > 0 then
          begin
            AbreSQLQuery0(Qry, DataBase, [
            'SELECT Versao FROM controle',
            '']);
            if CO_VERSAO < Qry.FieldByName('Versao').AsLargeInt then
            begin
              FMX_Geral.MB_Aviso('Verifica��o de banco de dados cancelada! '+
              sLineBreak + ' A vers�o deste arquivo � inferior a cadastrada ' +
              'no banco de dados.');
              Exit;
            end;
          end;
        end;
      end else
        FMX_Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
      //
      VerificaEstrutura_SQLite(DBName, Memo, RecriaRegObrig, LBIAviso1R,
        LBIAviso2R, LBIAviso1B, LBIAviso2B, LBIAviso1G, LBIAviso2G, LBITempoI,
        LBITempoF, LBITempoT, MeTabsNoNeed, PB, MeFldsNoNeed, DataBase);
    except
      raise;
    end;
    FMX_Geral.MB_Aviso('Verifica��o de banco de dados finalizada!');
  finally
    Qry.Free;
  end;
end;

function TUnFMX_DmkDB.VerificaCampos_SQLite(DBName, TabelaNome,
  TabelaBase: String; Memo: TMemo; RecriaRegObrig, Tem_Del, Tem_Sync: Boolean;
  LBIAviso1B, LBIAviso2B, LBIAviso1G, LBIAviso2G: TListBoxItem;
  MeFldsNoNeed: TMemo; DataBase: TFDConnection): TResultVerify;
var
  i, k, Resp, EhIdx: Integer;
  Opcoes, Item, Nome, IdxNome: String;
  Indices: TStringList;
  IdxExiste: Boolean;
  TemControle: TTemControle;
  QrNTV, QrUpd, QrIdx: TFDQuery;
  SQL: String;
begin
  Result := rvOK;
  QrNTV := TFDQuery.Create(DataBase);
  QrUpd := TFDQuery.Create(DataBase);
  QrIdx := TFDQuery.Create(DataBase);
  try
    AbreSQLQuery0(QrNTV, DataBase, [
      'PRAGMA table_info(' + Lowercase(TabelaNome) + ');',
      '']);
    try
      FLCampos  := TList<TCampos>.Create;
      FLIndices := TList<TIndices>.Create;
      try
        FCriarAtivoAlterWeb := True;
        TemControle := [];
        MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
        //
        if (tctrlLok in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Lk';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataCad';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataAlt';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserCad';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserAlt';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlWeb in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'AlterWeb';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAti in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Ativo';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if Tem_Del then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'DataDel';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'MotvDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        if Tem_Sync then
        begin
          New(FRCampos);
          FRCampos.Field      := 'LastModifi';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
          FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
          FRCampos.Null       := '';           //2 => Exclus�o
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'CliIntSync';
          FRCampos.Tipo       := 'int';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
        while not QrNTV.Eof do
        begin
          Nome := QrNTV.FieldByName('name').AsString;
          Item := Format(ivCampo_Nome, [TabelaNome, Nome]);

          case CampoAtivo_SQLite(DBName, TabelaNome, TabelaBase, Nome,
            Memo, QrNTV, DataBase) of
            1:
            begin
              try
                Opcoes := FRCampos.Tipo;
                if FRCampos.Null <> 'YES' then
                  Opcoes := Opcoes + ' NOT NULL ';
                if FRCampos.Default <> myco_ then
                  AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);
                //
                if FRCampos.Extra <> myco_ then
                begin
                  if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) > 0 then
                    Opcoes := Opcoes + ' auto_increment ';
                end;
                FMX_Geral.MB_Aviso('O campo ' + Nome +
                  ' da tabela ' + TabelaNome +
                  ' difere do esperado e o RDBM SQLite n�o permite regulariz�-lo.');
              except
                GravaAviso(Item + ivMsgERROAlterar, Memo);
                raise;
              end;
            end;
            9:
            begin
              AdicionaFldNoNeed_SQLite(MeFldsNoNeed, TabelaNome, Nome);
                GravaAviso('O campo "' + TabelaNome + '.' + Nome +
                '" deveria ser exclu�do!', Memo);
            end;
          end;
          QrNTV.Next;
        end;
        for i:= 0 to FLCampos.Count - 1 do
        begin
          FRCampos := FLCampos[i];
          if CampoExiste_SQLite(TabelaNome, FRCampos.Field, Memo, QrNTV) = 2 then
          begin
            Item := Format(ivCampo_Nome, [TabelaNome, FRCampos.Field]);
            try
              Opcoes := FRCampos.Tipo;
              if FRCampos.Null <> 'YES' then
                Opcoes := Opcoes + ' NOT NULL ';
              if FRCampos.Default <> myco_ then
                AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);
              //
              SQL := mycoALTERTABLE + TabelaNome + mycoADD + FRCampos.Field +
                mycoEspaco + Opcoes;
              if not FPergunta then Resp := mrYes else
              Resp := FMX_Geral.MB_Pergunta('O campo ' + FRCampos.Field +
              ' n�o existe na tabela ' + TabelaNome + ' e ser� criado.' +
              sLineBreak + 'Confirma a cria��o do campo?');
              if Resp = mrYes then
              begin
                ExecutaSQLQuery0(QrUpd, DataBase, [
                  SQL,
                  '']);
                GravaAviso(Item + ivcriado, Memo);
              end else begin
                GravaAviso(Item + ivAbortInclUser, Memo);
                if Resp = mrCancel then
                begin
                  Result := rvAbort;
                  GravaAviso(Item + ivAbortInclUser, Memo);
                  //
                  Exit;
                end;
              end;
            except
              GravaAviso(Item + ivMsgERROCriar, Memo);
              raise;
            end;
          end;
        end;
        Indices := TStringList.Create;
        Indices.Sorted := True;
        Indices.Duplicates := (dupIgnore);
        try
          for k := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[k];
            Indices.Add(FRIndices.Key_name);
          end;
          ExecutaSQLQuery0(QrIdx, DataBase, [
          'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
          '']);

          while not QrIdx.Eof do
          begin
            IdxNome := QrIdx.FieldByName('name').AsString;
            begin
              IdxExiste := False;
              Item := Format(ivIndice_Nome, [TabelaNome, IdxNome]);
              for k := 0 to Indices.Count -1 do
              begin
                if UpperCase(Indices[k]) = 'PRIMARY' then
                begin
                  if UpperCase(IdxNome) =
                    Uppercase('idx_' + TabelaNome + '_primary') then
                      IdxExiste := True
                  else
                  if UpperCase(IdxNome) =
                    Uppercase('sqlite_autoindex_' + TabelaNome + '_1') then
                      IdxExiste := True;
                end else
                if Indices[k] = IdxNome then
                  IdxExiste := True;
              end;
              if not IdxExiste then
              begin
                if not FPergunta then Resp := mrYes else
                Resp := FMX_Geral.MB_Pergunta(Format(ivExclIndice,
                  [Application.Title, IdxNome, TabelaNome]));
                if Resp = mrYes then
                begin
                  if ExcluiIndice_SQLite(DBName, TabelaNome,
                    IdxNome, Item, mymotSemRef, Memo, DataBase) <> rvOK
                  then
                    Result := rvAbort;
                end else
                begin
                  GravaAviso(Item + ivAbortExclUser, Memo);

                  if Resp = mrCancel then
                  begin
                    Result := rvAbort;
                    Exit;
                  end;
                end;
              end;
            end;
            QrIdx.Next;
          end;
          QrIdx.Close;
          for k := 0 to Indices.Count -1 do
          begin
            EhIdx := IndiceExiste_SQLite(TabelaNome, Indices[k], Memo, DataBase);

            if EhIdx in ([2,3]) then
              if RecriaIndice_SQLite(TabelaNome, Indices[k], Item, Memo, DataBase) <> rvOK
              then begin
                Result := rvAbort;
                Exit;
              end;
            if EhIdx in ([0]) then
            begin
              if CriaIndice_SQLite(TabelaNome, Indices[k], Item, Memo,
                DataBase) <> rvOK then
              begin
                Result := rvAbort;
              end;
            end;
          end;
        finally
          Indices.Free;
        end;
        ////
      finally
        FLCampos.Free;
        FLIndices.Free;
      end;
    finally
      QrNTV.Close;
    end;
  finally
    if QrNTV <> nil then
      QrNTV.Free;
    if QrUpd <> nil then
      QrUpd.Free;
    if QrIdx <> nil then
      QrIdx.Free;
  end;
end;

function TUnFMX_DmkDB.TabelaExiste_SQLite(Tabela: String;
  QrTabs: TFDQuery): Boolean;
var
  Tab: String;
  P: Integer;
begin
  Result := False;
  try
    QrTabs.First;
    P := Tabela.IndexOf('.');
    if P > 0 then
      Tab := Tabela.Substring(P + 1)
    else
      Tab := Tabela;
    while not QrTabs.Eof do
    begin
      if Uppercase(Tab) = Uppercase(QrTabs.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      QrTabs.Next;
    end;
  except
    raise;
  end;
end;

function TUnFMX_DmkDB.CriaTabela_SQLite(DBName, TabelaNome,
  TabelaBase: String; Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean;
  LBIAviso1B, LBIAviso2B, LBIAviso1G, LBIAviso2G: TListBoxItem;
  DataBase: TFDConnection; Tem_Sync: Boolean = False): Boolean;
var
  i, j, k, u, Primarios: Integer;
  Item, Opcoes, Campos: String;
  Indices, Conta: TStringList;
  Texto: TStringList;
  TemControle: TTemControle;
  //
  QrNTI: TFDQuery;
begin
  Result := True;
  FLCampos  := TList<TCampos>.Create;
  FLIndices := TList<TIndices>.Create;
  Texto     := TStringList.Create;
  QrNTI     := TFDQuery.Create(DataBase);
  try
    TemControle := [];
    MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
    //
    if (tctrlLok in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Lk';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlCad in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DataCad';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAlt in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DataAlt';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlCad in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAlt in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserAlt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlWeb in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'AlterWeb';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if (tctrlAti in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Ativo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end;
    if Tem_Del then
    begin
      New(FRCampos);
      FRCampos.Field      := 'UserDel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataDel';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MotvDel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o info
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
    if Tem_Sync then
    begin
      New(FRCampos);
      FRCampos.Field      := 'LastModifi';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
      FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
      FRCampos.Null       := '';           //2 => Exclus�o
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliIntSync';
      FRCampos.Tipo       := 'int';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o info
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
    MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
    try
      Texto.Add('CREATE TABLE ' + Lowercase(TabelaNome) + ' (');
      //
      Primarios := 0;

      for i:= 0 to FLCampos.Count - 1 do
      begin
        FRCampos := FLCampos[i];

        if FRCampos.Key = 'PRI' then
          Primarios := Primarios + 1;
      end;

      Item := Format(ivTabela_Nome, [TabelaNome]);

      for i:= 0 to FLCampos.Count - 1 do
      begin
        Opcoes   := myco_;
        FRCampos := FLCampos[i];
        Opcoes   := FRCampos.Tipo;

        if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) > 0 then
          Opcoes := Opcoes + ' AUTOINCREMENT ';

        if FRCampos.Null <> 'YES' then
          Opcoes := Opcoes + ' NOT NULL ';

        if FRCampos.Default <> myco_ then
          AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);

        Opcoes := mycoEspacos2 + FRCampos.Field + mycoEspaco + Opcoes;

        if i < (FLCampos.Count-1) then
          Opcoes := Opcoes + mycoVirgula;

        Texto.Add(Opcoes);
      end;

      Indices            := TStringList.Create;
      Indices.Sorted     := True;
      Indices.Duplicates := (dupIgnore);

      try
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];

          Indices.Add(FRIndices.Key_name);
        end;
        u := Texto.Count-1;

        if Indices.Count > 0 then
          Texto[u] := Texto[u] + mycoVirgula;

        for k := 0 to Indices.Count -1 do
        begin
          Conta  := TStringList.Create;
          Campos := myco_;

          for i := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[i];

            if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
          end;

          for j := 0 to FMX_Geral.IMV(Conta[Conta.Count-1])-1 do
          begin
            for i := 0 to FLIndices.Count -1 do
            begin
              FRIndices := FLIndices.Items[i];
              if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
              begin
                if Campos <> myco_ then Campos := Campos + ',';
                Campos := Campos + FRIndices.Column_name;
              end;
            end;
          end;

          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Uppercase(Indices[k]) = Uppercase('PRIMARY') then
            begin
              if (Primarios > 0) then
                Texto.Add('  CONSTRAINT INDICE_PRIMARIO PRIMARY KEY (' + Campos + ')')
            end else
              Texto.Add('  CONSTRAINT '+ Indices[k] + ' UNIQUE ('+Campos+')');

            if k < (Indices.Count-1) then
            begin
             u        := Texto.Count-1;
             Texto[u] := Texto[u] + mycoVirgula;
            end;
          end;
          Conta.Free;
        end;
      finally
        Indices.Free;
      end;
      Texto.Add(')');
      MostraSQL(QrNTI, Memo, '==== CRIAR TABELA ====');

      if Acao = actCreate then
      begin
        GravaAviso(Item + sLineBreak + Texto.Text, Memo);
        ExecutaSQLQuery0(QrNTI, DataBase, [
          Texto.Text,
          '']);
        GravaAviso(Item + ivCriado, Memo);
      end;
    except
      GravaAviso(Item+ivMsgERROCriar, Memo);
    end;
  finally
    FLCampos.Free;
    FLIndices.Free;
    Texto.Free;
    QrNTI.Free;
  end;
end;

function TUnFMX_DmkDB.CriaIndice_SQLite(Tabela, IdxNome, Aviso: String;
  Memo: TMemo; DataBase: TFDConnection): TResultVerify;
var
  I, J, K: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
  QrIts: TFDQuery;
  UNQ, IDX: String;
begin
  Result := rvOK;
  if not FPergunta then
    Resp := mrYes
  else
    Resp := FMX_Geral.MB_Pergunta('Confirma a cria��o do �ndice ' + IdxNome +
      ' da tabela ' + Tabela + '?');

  if Resp = mrYes then
  begin
    Conta  := TStringList.Create;
    Campos := myco_;

    for I := 0 to FLIndices.Count -1 do
    begin
      FRIndices := FLIndices.Items[I];

      if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
        Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
    end;

    K := FMX_Geral.IMV(Conta[Conta.Count-1])-1;

    if K = -1 then
      GravaAviso('FRIndices.Seq_in_index inv�lido (0)!' +
        'ERRO na cria��o do �ndice ' + IdxNome + ' na tabela '+ Tabela, Memo);

    for J := 0 to K do
    begin
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];

        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
        begin
          if StrToInt(Conta[J]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
    end;
    QrIts := TFDQuery.Create(DataBase);
    try
      try
        AbreSQLQuery0(QrIts, DataBase, [
          'SELECT COUNT{*} _ITENS_ FROM Controle ', //Trocar {} por ()
          'GROUP BY Codigo ',
          'ORDER BY _ITENS_ DESC ',
          '']);

        if (QrIts.RecordCount > 0) and (QrIts.FieldByName('_ITENS_').AsFloat > 1) then
        begin
          GravaAviso('O �ndice "' + IdxNome + '" n�o foi inclu�do na tabela "' +
            Tabela + '" pois j� seria criado com viola��o!', Memo);
        end else
        begin
          QrIts.Close;
          QrIts.SQL.Clear;

          if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
            or (Uppercase(IdxNome.Substring(1, 6)) = 'UNIQUE')
          then
            UNQ := ' UNIQUE '
          else
            UNQ := ' ';

          if LowerCase(IdxNome) = 'primary' then
            IDX := 'idx_' + Tabela + '_primary'
          else
            IDX := IdxNome;
          //
          ExecutaSQLQuery0(QrIts, DataBase, [
            'CREATE' + UNQ + 'INDEX IF NOT EXISTS ' + IDX,
            'ON ' + Lowercase(Tabela) + ' (' + Campos +  ')',
            '']);

          Result := rvOK;

          GravaAviso(Aviso + ivCriado, Memo);
        end;
      except;
        MostraSQL(QrIts, Memo,
          'ERRO na cria��o de �ndice na tabela ' + Tabela);
        Conta.Free;
        GravaAviso(Aviso + ivMsgERROCriar, Memo);
        raise;
      end;
    finally
      QrIts.Free;
    end;
  end else begin
    GravaAviso(Aviso + ivAbortInclUser, Memo);
    if Resp = mrCancel then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TUnFMX_DmkDB.VerificaEstrutura_SQLite(DBName: String; Memo: TMemo;
  RecriaRegObrig: Boolean; LBIAviso1R, LBIAviso2R, LBIAviso1B, LBIAviso2B,
  LBIAviso1G, LBIAviso2G, LBITempoI, LBITempoF, LBITempoT: TListBoxItem;
  MeTabsNoNeed: TMemo; PB: TProgressBar; MeFldsNoNeed: TMemo;
  DataBase: TFDConnection): Boolean;
var
  i: Integer;
  TabNome, TabBase, Item: String;
  TempoI, TempoF, TempoT: TTime;
  DefTabela: TTabelas;
  Tem_Del, Tem_Sync: Boolean;
  QrTabs: TFDQuery;
  Texto: String;
begin
  QrTabs := TFDQuery.Create(DataBase);
  try
    Tem_Del      := False;
    Tem_Sync     := False;
    TempoI       := Time();
    FViuControle := 0;
    Result       := True;

    FMX_dmkPF.Informa2(LBIAviso1R, LBIAviso2R, False, Format(ivMsgEstruturaBD, [DBName]));
    FMX_dmkPF.Informa(LBITempoI, False, FormatDateTime('hh:nn:ss:zzz', TempoI));

    try
      FTabelas := TList<TTabelas>.Create;

      MyList.CriaListaTabelas(DBName, FTabelas);

      try
        if LBIAviso1R <> nil then
          Texto := LBIAviso1R.Text
        else
          Texto := '';
        FMX_
        dmkPF.Informa2(LBIAviso1R, LBIAviso2R, True, Texto + '(' +
          FMX_Geral.FF0(FTabelas.Count) + ' tabelas)');

        FMX_dmkPF.Informa2(LBIAviso1B, LBIAviso2B, True, 'Verificando estrutura de tabelas j� criadas');

        AbreSQLQuery0(QrTabs, DataBase, [
          'SELECT name FROM sqlite_master ',
          'WHERE type=''table'' ',
          'ORDER BY name; ',
          '']);

        if PB <> nil then
        begin
          PB.Max := QrTabs.RecordCount;
          PB.Value := 0;
        end;

        QrTabs.First;
        while not QrTabs.Eof do
        begin
          if PB <> nil then
            PB.Value := PB.Value + 1;
          // Fazer Primeiro controle !!!
          if FViuControle = 0 then
          begin
            TabNome      := 'controle';
            FViuControle := 1;
          end else
          if FViuControle = 2 then
          begin
            QrTabs.Prior;
            FViuControle   := 3;
            TabNome        := QrTabs.Fields[0].AsString;
          end else
            TabNome := QrTabs.Fields[0].AsString;

          TabBase := '';

          for i := 0 to FTabelas.Count -1 do
          begin
            DefTabela := FTabelas[i];

            if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
            begin
              TabBase  := DefTabela.TabBase;
              Tem_Del  := DefTabela.Tem_Del;
              Tem_Sync := DefTabela.Tem_Sync;
              Break;
            end;
          end;

          if TabBase = '' then
            TabBase := TabNome;

          FMX_dmkPF.Informa2(LBIAviso1B, LBIAviso2B, True, 'Verificando estrutura de tabelas j� criadas');
          FMX_dmkPF.Informa2(LBIAviso1G, LBIAviso2G, True, 'Verificando tabela "' + TabNome + '"');

          if not Grl_DmkDB.TabelaAtiva(TabNome) then
          begin
            Item := Format(ivTabela_Nome, [TabNome]);

            if Lowercase(TabNome.Substring(1, 4)) <> 'lct0' then
              MeTabsNoNeed.Lines.Add(TabNome);
          end else
          begin
            if VerificaCampos_SQLite(DBName, TabNome, TabBase, Memo,
              RecriaRegObrig, Tem_Del, Tem_Sync, LBIAviso1B, LBIAviso2B,
              LBIAviso1G, LBIAviso2G, MeFldsNoNeed, DataBase) <> rvOK then
            begin
              GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
              Exit;
            end;
          end;
          QrTabs.Next;
        end;

        FMX_dmkPF.Informa2(LBIAviso1B, LBIAviso2B, True, 'Verificando tabelas a serem criadas');

        if PB <> nil then
        begin
          PB.Max   := FTabelas.Count;
          PB.Value := 0;
        end;

        for i := 0 to FTabelas.Count -1 do
        begin
          FMX_dmkPF.Informa2(LBIAviso1G, LBIAviso2G, True, '"' + TabNome + '" verificando se existe');

          if PB <> nil then
            PB.Value := PB.Value + 1;

          DefTabela := FTabelas[i];
          TabNome   := DefTabela.TabCria;
          TabBase   := DefTabela.TabBase;
          Tem_Del   := DefTabela.Tem_Del;
          Tem_Sync  := DefTabela.Tem_Sync;

          if TabBase = '' then
            TabBase := TabNome;

          if not TabelaExiste_SQLite(TabNome, QrTabs) then
          begin
              FMX_dmkPF.Informa2(LBIAviso1G, LBIAviso2G, True, '"' + TabNome + '" criando tabela ');
              CriaTabela_SQLite(DBName, TabNome, TabBase, Memo, actCreate,
                Tem_Del, LBIAviso1B, LBIAviso2B, LBIAviso1G, LBIAviso2G,
                DataBase, Tem_Sync);
          end;

        end;
        FMX_dmkPF.Informa2(LBIAviso1G, LBIAviso2G, True, '');
        QrTabs.Close;
      finally
        FTabelas.Free;
      end;
      FMX_dmkPF.Informa2(LBIAviso1G, LBIAviso2G, False, '...');
      FMX_dmkPF.Informa2(LBIAviso1B, LBIAviso2B, False, '...');
      FMX_dmkPF.Informa2(LBIAviso1R, LBIAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));

      TempoF := Time();

      FMX_dmkPF.Informa(LBITempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));

      TempoT := TempoF - TempoI;

      if TempoT < 0 then
        TempoT := TempoT + 1;

      FMX_dmkPF.Informa(LBITempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
    except
      FMX_dmkPF.Informa2(LBIAviso1R, LBIAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));
      raise;
    end;
  finally
    QrTabs.Free;
  end;
end;

function TUnFMX_DmkDB.IndiceExiste_SQLite(Tabela, Indice: String;
  Memo: TMemo; DataBase: TFDConnection): Integer;
var
  i: Integer;
  Need, Find, Have: Integer;
  Item: String;
  QrIts: TFDQuery;
  MyIDXa0, MyIDXb1, MyIDXb2, MyIDXc1: String;
  isPRI, Continua: Boolean;
begin
  Item   := Format(ivCampo_Nome, [Tabela, Indice]) + mycoEspaco + 'N�o encontrado:';
  Result := 0;
  Need   := 0;
  Find   := 0;
  Have   := 0;
  QrIts  := TFDQuery.Create(DataBase);

  try
    AbreSQLQuery0(QrIts, DataBase, [
      'SELECT name FROM sqlite_master ',
      'WHERE type=''index''',
      'AND Name LIKE ''%_' + LowerCase(Tabela) + '_%''',
      'ORDER BY name; ',
      '']);
    QrIts.First;
    while not QrIts.Eof do
    begin
      QrIts.Next;
    end;

    while not QrIts.Eof do
    begin
      if Uppercase(Indice) = 'PRIMARY' then
      begin
        if Uppercase(QrIts.FieldByName('name').AsString) = Uppercase('idx_' +
        Tabela + '_primary') then
          Have := Have + 1
        else
        if Uppercase(QrIts.FieldByName('name').AsString) =
          Uppercase('sqlite_autoindex_' + Tabela + '_1') then
            Have := Have + 1;
      end else
      if Uppercase(QrIts.FieldByName('name').AsString) =
      Uppercase(Indice) then
        Have := Have + 1;
      QrIts.Next;
    end;

    try
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(Indice) then
        begin
          QrIts.First;

          Need := Need + 1;

          while not QrIts.Eof do
          begin
            MyIDXa0 := Uppercase(QrIts.FieldByName('name').AsString);
            MyIDXb1 := 'idx_' + Tabela + '_primary';
            MyIDXb2 := 'sqlite_autoindex_' + Tabela + '_1';
            MyIDXc1 := FRIndices.Key_name;
            isPRI   := Uppercase(FRIndices.Key_name) = 'PRIMARY';

            Continua :=
            not (
              (
                isPRI
                and (
                  (MyIDXa0 = MyIDXb1)
                  or
                  (MyIDXa0 = MyIDXb2)
                )
              )
              or
              (MyIDXa0 = MyIDXc1)
            );
            if Continua then
            begin
              if Result = 0 then
                Result := 1;
              Find := Find + 1;
            end;
            QrIts.Next;
          end;
          if Find < Need then
          begin
            if Find > 0 then
            begin
              GravaAviso('A coluna ' + FRIndices.Column_name +
                ' n�o foi encontrada no �ndice ' + Indice + '.', Memo);

              Result := 2;
            end else
              GravaAviso('O �ndice ' + Indice + ' n�o existe.', Memo);
          end;
        end;
      end;
    except
      QrIts.Close;
      raise;
    end;
    if Have > Find then
    begin
      GravaAviso('H� excesso de colunas no �ndice ' + Indice + ' da tabela ' + Tabela +  '.', Memo);
      Result := 3;
    end;
  finally
    QrIts.Free;
  end;
end;

function TUnFMX_DmkDB.RecriaIndice_SQLite(Tabela, IdxNome,
  Aviso: String; Memo: TMemo; DataBase: TFDConnection): TResultVerify;
var
  I, J: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
  Qry: TFDQuery;
  UNQ: String;
begin
  Result := rvOK;
  if not FPergunta then
    Resp := mrYes
  else
    Resp := FMX_Geral.MB_Pergunta('Confirma a alter��o do �ndice ' + IdxNome +
    ' da tabela ' + Tabela + '?)');
  //
  if Resp = mrYes then
  begin
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for I := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[I];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      for J := 0 to FMX_Geral.IMV(Conta[Conta.Count-1])-1 do
      begin
        for I := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[I];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[J]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      Qry := TFDQuery.Create(DataBase);
      try
        ExecutaSQLQuery0(Qry, DataBase, [
        'DROP INDEX IF EXISTS ' + IdxNome + '''',
        '']);
        //
        if (Uppercase(IdxNome) = Uppercase('PRIMARY'))
        or (Uppercase(IdxNome.Substring(1, 6)) = 'UNIQUE') then
          UNQ := ' UNIQUE '
        else
          UNQ := ' ';
        //
        ExecutaSQLQuery0(Qry, DataBase, [
        'CREATE' + UNQ + 'INDEX IF NOT EXISTS ' + LowerCase(IdxNome),
        'ON ' + Lowercase(Tabela) + ' (' + Campos +  ')',
        '']);
        MostraSQL(Qry, Memo, 'CRIAR INDICE');
      finally
        Qry.Free;
      end;
      //
      Result := rvOK;
      GravaAviso(Aviso + ivAlterado, Memo);
    except;
      Conta.Free;
      GravaAviso(Aviso + ivMsgERROAlterar, Memo);
      raise;
    end;
  end else begin
    GravaAviso(Aviso + ivAbortAlteUser, Memo);
    if Resp = mrCancel then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TUnFMX_DmkDB.ExcluiIndice_SQLite(DBName, Tabela, IdxNome,
  Aviso: String; Motivo: TMyMotivo; Memo: TMemo;
  DataBase: TFDConnection): TResultVerify;
var
  Resp: Integer;
  Txt: String;
  Qry: TFDQuery;
begin
  if True then
  begin
    case Motivo of
      mymotDifere: Txt := Format('O �ndice %s da Tabela %s difere do esperado e ' +
                            'deve ser excluido.', [IdxNome, Tabela]);
      mymotSemRef: Txt := Format('N�o h� refer�ncia ao �ndice %s da Tabela %s, ' +
                            'que deve ser excluido.', [IdxNome, Tabela]);
    end;
    if not FPergunta then
      Resp := mrNone
    else
      Resp := FMX_Geral.MB_Pergunta(Txt + sLineBreak +
                'Confirma a exclus�o do �ndice ?');

    if Resp = mrYes then
    begin
      try
        Qry := TFDQuery.Create(DataBase);

        try
          ExecutaSQLQuery0(Qry, DataBase, [
          'DROP INDEX IF EXISTS ' + IdxNome + '''',
          '']);
          MostraSQL(Qry, Memo, '+++EXCLUIR INDICE+++');
        finally
          Qry.Free;
        end;
        //
        GravaAviso(Aviso + ivMsgExcluido, Memo);
      except;
        GravaAviso(Aviso + ivMsgERROExcluir, Memo);
        raise;
      end;
    end else begin
      GravaAviso(Aviso + ivAbortExclUser, Memo);
      if Resp = mrCancel then
      begin
        Result := rvAbort;
        Exit;
      end;
    end;
    Result := rvOK;
    GravaAviso('O �ndice "' + Tabela + '.' + IdxNome + ' deveria ser exclu�do!', Memo);
    //
  end else
    Result := rvOK;
end;

function TUnFMX_DmkDB.CampoExiste_SQLite(Tabela, Campo: String; Memo: TMemo;
  QrNTV: TFDQuery): Integer;
var
  Item: String;
begin
  Item := Format(ivCampo_Nome, [Tabela, Campo]) + mycoEspaco + ivMsgNaoExiste;
  Result := 2;
  try
    QrNTV.First;
    while not QrNTV.Eof do
    begin
      if Uppercase(QrNTV.FieldByName('name').AsString) =
         Uppercase(FRCampos.Field) then Result := 0;
      if Result < 2 then Exit;
      QrNTV.Next;
    end;
    if Result = 2 then
      GravaAviso(Item, Memo);
  except
    raise;
  end;
end;

function TUnFMX_DmkDB.CampoAtivo_SQLite(DBName, TabelaNome, TabelaBase,
  Campo: String; Memo: TMemo; QrNTV: TFDQuery; DataBase: TFDConnection): Integer;

  function NuloStrToInt(Txt: String): Integer;
  begin
    if Txt = 'YES' then
      Result := 0
    else
      Result := 1;
  end;

var
  i: Integer;
  Item: String;
  QrIdx, QrPII: TFDQuery;
begin
  Item := Format(ivCampo_Nome, [TabelaNome, Campo]) + mycoEspaco + ivMsgDiferenca;
  Result := 9;
  try
    for i := 0 to FLCampos.Count -1 do
    begin
      FRCampos := FLCampos.Items[i];
      if Uppercase(FRCampos.Field) = Uppercase(Campo) then
      begin
        Result := 0;
        if Uppercase(FRCampos.Tipo)  <>
           Uppercase(QrNTV.FieldByName('Type').AsString) then
        begin
          Result := 1;
          GravaAviso(Item + mycoTypeDataType, Memo);
        end;
        if NuloStrToInt(Uppercase(FRCampos.Null))  <>
           QrNTV.FieldByName('notnull').AsInteger then
        begin
          Result := 1;
          GravaAviso(Item+'"NULL"', Memo);
        end;
        if (Uppercase(FRCampos.Default) <>
           Uppercase(QrNTV.FieldByName('dflt_value').AsString)) then
        if (Uppercase(#39 + FRCampos.Default + #39) <>
           Uppercase(QrNTV.FieldByName('dflt_value').AsString)) then
        begin
          Result := 1;
          GravaAviso(Item + '"DEFAULT"' + sLineBreak +
          'Lista dmk: ' + FRCampos.Default + sLineBreak + 'Obtido do BD: ' +
          QrNTV.FieldByName('dflt_value').AsString, Memo);
        end;
        if (FRCampos.Key  <> '') and (
           QrNTV.FieldByName('pk').AsInteger = 0) then
        begin
          if (FRCampos.Key = 'UNI')
          or (FRCampos.Key = 'MUL') then
          begin
            // SQLite n�o informa
            // ver pelo �ndice:
            QrIdx := TFDQuery.Create(DataBase);
            QrPII := TFDQuery.Create(DataBase);
            try

              AbreSQLQuery0(QrIdx, DataBase, [
              'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
              '']);
              QrIdx.First;
              while not QrIdx.Eof do
              begin
                AbreSQLQuery0(QrPII, DataBase, [
                'PRAGMA INDEX_INFO(''' + QrIdx.FieldByName('name').AsString + ''')',
                '']);
                while not QrPII.Eof do
                begin
                  if LowerCase(QrPII.FieldByName('name').AsString) = Lowercase(Campo) then
                  begin
                    Exit;
                  end;
                  //
                  QrPII.Next;
                end;
                //
                QrIdx.Next;
              end;
            finally
              if QrIdx <> nil then
                QrIdx.Free;
              if QrPII <> nil then
                QrPII.Free;
            end;
          end;
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Result = 9 then Result := 3;
            GravaAviso(Item+'"KEY"', Memo);
          end;
        end;
      end;
      if Result < 9 then Exit;
    end;
  except
    raise;
  end;
end;

procedure TUnFMX_DmkDB.AdicionaDefault_SQLite(var Opcoes: String; Tipo,
  Padrao: String);
begin
  if Lowercase(Tipo).IndexOf('int') > 0 then
    Opcoes := Opcoes + ' DEFAULT ' + Padrao
  else
    Opcoes := Opcoes + ' DEFAULT ' + #39 + Padrao + #39;
end;

procedure TUnFMX_DmkDB.AdicionaFldNoNeed_SQLite(MeFldsNoNeed: TMemo; Tabela,
  Campo: String);
var
  Texto: String;
  I, N: Integer;
begin
  Texto := Tabela + '.' + Campo;
  N := -1;
  //
  for I := 0 to MeFldsNoNeed.Lines.Count - 1 do
  begin
    if MeFldsNoNeed.Lines[I] = Texto then
    begin
      N := I;
      Break;
    end;
  end;
  //
  if N = -1 then
    MeFldsNoNeed.Lines.Add(Texto);
end;

function TUnFMX_DmkDB.ValidaDadosCampo(Query: TFDQuery; DB: TFDConnection;
  Campo, Tabela: String; Valor: Integer; PemiteZero: Boolean): Boolean;
begin
  if Valor <> 0 then
  begin
    AbreSQLQuery0(Query, DB, [
      'SELECT ' + Campo,
      'FROM ' + Tabela,
      'WHERE ' + Campo + '=' + FMX_Geral.FF0(Valor)]);
    if Query.RecordCount > 0 then
      Result := True
    else
      Result := False;
    Query.Close;
  end else
  begin
    if PemiteZero then
      Result := True
    else
      Result := False;
  end;
end;

function TUnFMX_DmkDB.ObtemIndexDeTabela(Query: TFDQuery; CampoCodigo: String;
  Valor: Integer): Integer;
var
  Res: Integer;
{
  Codigo, i: Integer;
}
begin
  {
  i := 0;
  //
  Query.First;
  while not Query.Eof do
  begin
    Codigo := Query.FieldByName(CampoCodigo).AsInteger;
    //
    if Codigo = Valor then
      Break;
    i := i + 1;
    //
    Query.Next;
  end;
  Result := i;
  }
  Res := -1;
  //
  if ((Query.State <> dsInactive) and (Query.RecordCount > 0) and
    (Query.Locate(CampoCodigo, Valor, [])))
  then
    Res := Query.RecNo - 1;
  //
  Result := Res;
end;

procedure TUnFMX_DmkDB.RemoveUsuarios(DataBase: TFDConnection;
  DriverNome: TDriverName);
var
  Qry: TFDQuery;
begin
  case DriverNome of
    istMySQL:
    begin
      FMX_Geral.MB_Aviso('N�o implementado!');
    end;
    istSQLite:
    begin
      Qry := TFDQuery.Create(DataBase);
      try
        ExecutaSQLQuery0(Qry, DataBase, [
          'DELETE FROM usuarios ',
          '']);
      finally
        Qry.Free;
      end;
    end;
  end;
end;

procedure TUnFMX_DmkDB.AtualizaSincroDB(DataBase: TFDConnection;
  DriverNome: TDriverName; Tabela, DataHora: String);
var
  QueryLoc, QueryUpd: TFDQuery;
  Codigo: Integer;
  SQLTipo: TSQLType;
begin
  case DriverNome of
    istMySQL:
    begin
      FMX_Geral.MB_Aviso('N�o implementado!');
    end;
    istSQLite:
    begin
      QueryLoc := TFDQuery.Create(DataBase);
      QueryUpd := TFDQuery.Create(DataBase);
      try
        AbreSQLQuery0(QueryLoc, DataBase, [
          'SELECT Codigo ',
          'FROM sincrodb ',
          'WHERE Tabela="' + Tabela + '"',
          'AND Usuario=' + FMX_Geral.FF0(VAR_WEB_USER_ID),
          '']);
        if QueryLoc.RecordCount > 0 then
        begin
          Codigo  := QueryLoc.FieldByName('Codigo').AsInteger;
          SQLTipo := stUpd;
        end else
        begin
          Codigo  := ObtemCodigoInt('sincrodb', 'Codigo', QueryUpd, DataBase);
          SQLTipo := stIns;
        end;
        CarregaSQLInsUpd(QueryUpd, DataBase, SQLTipo, 'sincrodb', False,
          ['Tabela', 'DtaSincroUTC'], ['Usuario', 'Codigo'],
          [Tabela, DataHora], [VAR_WEB_USER_ID, Codigo],
          True, False, '', stMobile);
      finally
        QueryLoc.Free;
        QueryUpd.Free;
      end;
    end;
  end;
end;

function TUnFMX_DmkDB.ObtemDataSincro(DataBase: TFDConnection;
  DriverNome: TDriverName; Tabela: String): TDateTime;
const
  CO_Tempo = 30; //30 minutos antes do �ltimo sincro para evitar erros de hora
var
  Query: TFDQuery;
begin
  Result := 0;

  case DriverNome of
    istMySQL:
    begin
      FMX_Geral.MB_Aviso('N�o implementado!');
    end;
    istSQLite:
    begin
      Query := TFDQuery.Create(DataBase);
      try
        AbreSQLQuery0(Query, DataBase, [
          'SELECT DtaSincroUTC, Tabela ',
          'FROM sincrodb ',
          'WHERE Tabela="' + Tabela + '"',
          'AND Usuario=' + FMX_Geral.FF0(VAR_WEB_USER_ID),
          '']);
        if Query.RecordCount > 0 then
        begin
          Result := Query.FieldByName('DtaSincroUTC').AsDateTime - CO_Tempo;
        end;
      finally
        Query.Free;
      end;
    end;
  end;
end;

function TUnFMX_DmkDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo: String;
              Inteiro1: Integer; DataBase: TFDConnection): Integer;
var
  Qry: TFDQuery;
begin
  Qry:= TFDQuery.Create(DataBase);
  try
    Qry.Connection := DataBase;
    //
    Result := mrNo;
    //
    if Pergunta = '' then
      Result := mrYes
    else
      Result := FMX_Geral.MB_Pergunta(Pergunta);
    //
    if Result = mrYes then
    begin
      ExecutaSQLQuery0(Qry, DataBase,
        [DELETE_FROM + lowercase(Tabela),
        'WHERE ' + Campo + '=' + FMX_Geral.FF0(Inteiro1)]);
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;

function TUnFMX_DmkDB.ExcluiRegistroInt1_Sinc(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DataBase: TFDConnection): Integer;
var
  LastModifi: String;
  Qry: TFDQuery;
begin
  Qry:= TFDQuery.Create(DataBase);
  try
    Qry.Connection := DataBase;
    //
    Result := mrNo;
    //
    if Pergunta = '' then
      Result := mrYes
    else
      Result := FMX_Geral.MB_Pergunta(Pergunta);
    if Result = mrYes then
    begin
      LastModifi := FMX_Geral.FDT(FMX_dmkPF.DateTime_MyTimeZoneToUTC(Now), 109);
      //
      ExecutaSQLQuery0(Qry, DataBase,
        ['UPDATE ' + LowerCase(Tabela) +
        ' SET LastAcao = ' + FMX_Geral.FF0(Integer(laDel)) + ', ',
        ' LastModifi = "' + LastModifi + '"',
        ' WHERE '+ Campo +' = ' + FMX_Geral.FF0(Inteiro1)]);
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;

function TUnFMX_DmkDB.ObtemListaDeColunasDeTabela(DataBase: TFDConnection;
  DriverNome: TDriverName; Tabela: String; TipoColuna: TTipoColuna;
  var Colunas: TStringList): Boolean;
var
  Query: TFDQuery;
  Coluna: String;
  Chave: Integer;
begin
  Result := False;

  Colunas.Clear;

  case DriverNome of
    istMySQL:
    begin
      FMX_Geral.MB_Aviso('N�o implementado!');
     end;
    istSQLite:
    begin
      Query := TFDQuery.Create(DataBase);
      try
        AbreSQLQuery0(Query, DataBase, [
          'PRAGMA table_info('+ Tabela +') ',
          '']);
        if Query.RecordCount > 0 then
        begin
          Query.First;

          while not Query.Eof do
          begin
            Chave  := Query.FieldByName('pk').AsInteger;
            Coluna := Query.FieldByName('name').AsString;

            case TipoColuna of
              stPrimarias:
              begin
                if Chave = 1 then
                  Colunas.Add(Coluna);
              end;
              stNaoPrimarias:
              begin
                if Chave <> 1 then
                  Colunas.Add(Coluna);
              end;
              stTodas:
              begin
                Colunas.Add(Coluna);
              end;
            end;
            Query.Next;
          end;
          Result := True;
        end;
      finally
        if Query <> nil then
          Query.Free;
      end;
    end;
  end;
end;
*)

end.
