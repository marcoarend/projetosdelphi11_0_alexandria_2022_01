unit UnFMX_PushNotifications;


interface

uses System.Classes, System.UITypes,
  System.SysUtils, System.Variants,
  //IdHTTP, REST.Client, REST.Types, Data.DB, TypInfo,
  IPPeerClient, REST.Backend.PushTypes, System.JSON,
  REST.Backend.EMSPushDevice, System.PushNotification, REST.Backend.EMSProvider,
  Data.Bind.Components, Data.Bind.ObjectScope, REST.Backend.BindSource,
  REST.Backend.PushDevice(*,
  UnGrl_DmkWeb, UnDMkEnums*),
  //FMX.PushNotification.Android,
  FMX.Memo, FMX.Dialogs, System.Threading, UnFMX_Grl_Vars, UnDmkEnums;

type
  TUnFMX_PushNotifications = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //
    procedure DoReceiveNotificationEvent(Sender: TObject; const ServiceNotification: TPushServiceNotification);
    procedure DoServiceConnectionChange(Sender: TObject; PushChanges: TPushService.TChanges);
    //
    procedure PushNotifications_FormActivate(Sender: TObject);
    procedure PushNotifications_FormCreate(CodigoProjetoGoogle: String;
              PushEvents: TPushEvents; EMSProvider: TEMSProvider);
    procedure PushNotifications_FormShow(Sender: TObject);
    //
    procedure ObtemTodasNotificacoesStartapp(MemoLog: TMemo);
  end;

var
  FMX_PushNotifications: TUnFMX_PushNotifications;

implementation

uses UnApp_PF, UnGrl_Geral;
(*UnFMX_DmkWeb, REST.Response.Adapter, System.JSON, UnGrl_Geral, UnGrl_Vars,
  (*UnFMX_Grl_Vars, UnGrl_Consts;*)

{ TUnFMX_PushNotifications }

const
  CO_Informa_Testes = False; //True;

procedure TUnFMX_PushNotifications.PushNotifications_FormCreate(
  CodigoProjetoGoogle: String; PushEvents: TPushEvents; EMSProvider: TEMSProvider);
begin
  try
    VAR_PushNotifications_CodigoProjeto := CodigoProjetoGoogle;
    VAR_PushNotifications_PushEvents    := PushEvents;
    VAR_PushNotifications_EMSProvider   := EMSProvider;
    PushEvents.AutoActivate := True;    //  Delphi Rio >> False?
    PushEvents.AutoRegisterDevice := False;
    //
    PushEvents.Provider := EMSProvider;
    /////////////////////////  O v e r S e e r M o b  //////////////////////////////////////////////////////////////
    // https://console.developers.google.com/apis/dashboard?folder=&organizationId=&project=ovspush-263502        //
    // Chave de API:                                                                                              //
    // AIzaSyAr5MIKvNheeVOxyBydu4-Zjd32CIFRL5Q                                                                    //
    // N�mero do projeto:                                                                                         //
    // 2 0 5 8 0 7 9 6 8 2 8 2                                                                                               //
    //                                                                                                            //
    // ---------------------------------------------------------------------------------------|                   //
    // |                                                                                      |                   //
    // | Project > Options > Version Info > All Configurations > Android Platform > API Key:  |                   //
    // | AIzaSyAr5MIKvNheeVOxyBydu4-Zjd32CIFRL5Q                                              |                   //
    // |-------------------------------------------------------------------------------------------------------|  //
    // |                                                                                                       |  //
    // | Project > Options >EntitledList > All Configurations > Android Platform > Receive Push Notifications: |  //
    // | True                                                                                                  |  //
    // |-------------------------------------------------------------------------------------------------------|  //
    //                                                                                                            //
    // C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\FMX\APP\CLI\OVSM\v03_01\Projeto\                                  //
    // C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\FMX\APP\CLI\OVSM\v03_01\Projeto\AndroidManifest.template.xml      //
    // Editar o arquivo AndroidManifest.template.xml:                                                             //
    // ---------------------------------------------------------------------------------------|                   //
    // |                                                                                      |                   //
    // |      ...                                                                             |                   //
    // |      </activity>                                                                     |                   //
    // |      <%activity%>                                                                    |                   //
    // |                                                                                      |                   //
    // |       // Inserir esta linha! � para receber as notofoca��es!                         |                   //
    // |      <service android:name="com.embarcadero.gcm.notifications.GCMIntentService" />   |                   //
    // |      // Fim da inser��o!                                                             |                   //
    // |                                                                                      |                   //
    // |                                                                                      |                   //
    // |      <%receivers%>                                                                   |                   //
    // |      ....                                                                            |                   //
    // |--------------------------------------------------------------------------------------|                   //
    //                                                                                                            //
    ///                                                                                                           //
    ///  C�digo do projeto O v e r S e e r M o b                                                                            //
    EMSProvider.AndroidPush.GCMAppID := VAR_PushNotifications_CodigoProjeto; // O v e r S e e rMob = '2 0 5 8 0 7 9 6 8 2 8 2'      //
    //                                                                                                            //
    //                                                                                                            //
    //////////////////////  FIM - O v e r S e e r M o b  ///////////////////////////////////////////////////////////
  except
    on E: Exception do
      if CO_Informa_Testes then
        Grl_Geral.MB_Erro('Erro capturado em FormCreate:' + sLineBreak + E.Message);
  end;
end;

procedure TUnFMX_PushNotifications.DoReceiveNotificationEvent(Sender: TObject;
  const ServiceNotification: TPushServiceNotification);
var
  MessageText: string;
  x: Integer;
  obj : TJSONObject;
  MyJSONPair : TJSONPair;
  AllText: String;
  DadosNotif: TArr1NivJSON;
(*
'{"google.delivered_priority":"normal","google.sent_time":"1578082534975","google.ttl":"2419200","google.original_priority":"normal","id":"2 0 5 8 0 7 9 6 8 2 8 2","from":"2 0 5 8 0 7 9 6 8 2 8 2","google.message_id":"0:1578082534996093%eef72599f9fd7ecd","message":"Suspendisse bibendum elementum enim maximus sagittis ante tristique nec. Suspendisse bibendum elemen","DmkXtra001":"chmococad","DmkXtra002":"1","DmkXtra003":"AURORA BOREAL EXT IND. MONT. EQUIP. PAUL","DmkXtra004":"2019-12-26 00:00:00","DmkXtra005":"2020-02-22 00:00:00"}'#012'=' <repeats 33 times>,
#012#012'google.delivered_priority =>> normal'#012'google.sent_time =>> 1578082534975'#012'google.ttl =>> 2419200'#012'google.original_priority =>> normal'#012'id =>> 2 0 5 8 0 7 9 6 8 2 8 2'#012'from =>> 2 0 5 8 0 7 9 6 8 2 8 2'#012'google.message_id =>> 0:1578082534996093%eef72599f9fd7ecd'#012'message =>> Suspendisse bibendum elementum enim maximus sagittis ante tristique nec. Suspendisse bibendum elemen'#012'DmkXtra001 =>> chmococad'#012'DmkXtra002 =>> 1'#012'DmkXtra003 =>> AURORA BOREAL EXT IND. MONT. EQUIP. PAUL'#012'DmkXtra004 =>> 2019-12-26 00:00:00'#012'DmkXtra005 =>> 2020-02-22 00:00:00'
*)
begin
  //Grl_Geral.MB_Info(ServiceNotification.DataKey);
//  Grl_Geral.MB_Info(ServiceNotification.Json.Value);
  AllText := ServiceNotification.DataObject.ToJSON + sLineBreak + '=================================' + sLineBreak;
          //App_PF.PushNotifications_ReceiveDestrincha(notif);
  //SetLength(DadosNotif, 0);
  //N := 0;
  //
  //MessageText := ServiceNotification.DataObject.GetValue('message').Value;
  try
    for x := 0 to ServiceNotification.DataObject.Count - 1 do
    begin
      // iOS...
      if ServiceNotification.DataKey = 'aps' then
      begin
        SetLength(DadosNotif, X + 1);
        DadosNotif[x, 0] := ServiceNotification.DataObject.Pairs[x].JsonString.Value;
        DadosNotif[x, 1] := ServiceNotification.DataObject.Pairs[x].JsonValue.Value;

        if ServiceNotification.DataObject.Pairs[x].JsonString.Value = 'alert' then
          MessageText := ServiceNotification.DataObject.Pairs[x].JsonValue.Value;
      end;

      // Android...
      if ServiceNotification.DataKey = 'gcm' then
      begin
        SetLength(DadosNotif, X + 1);
        DadosNotif[x, 0] := ServiceNotification.DataObject.Pairs[x].JsonString.Value;
        DadosNotif[x, 1] := ServiceNotification.DataObject.Pairs[x].JsonValue.Value;
        //
        AllText := AllText + sLineBreak +
          ServiceNotification.DataObject.Pairs[x].JsonString.Value
          + ' =>> ' +
          ServiceNotification.DataObject.Pairs[x].JsonValue.Value;
        //
        if ServiceNotification.DataObject.Pairs[x].JsonString.Value = 'message' then
          MessageText := ServiceNotification.DataObject.Pairs[x].JsonValue.Value;
      end;

    end;
    App_PF.PushNotifications_ReceiveDestrincha2(DadosNotif);
  except
    on E: Exception do
      if CO_Informa_Testes then
        Grl_Geral.MB_Erro('Erro capturado em DoReceiveNotificationEvent:' + sLineBreak + E.Message);
  end;
  try
    if VAR_PushNotifications_Memo <> nil then
      VAR_PushNotifications_Memo.Lines.Add(MessageText);
  except
    on E: Exception do
      if CO_Informa_Testes then
        Grl_Geral.MB_Erro('Erro capturado em Memo.Lines.Add(MessageText):' + sLineBreak + E.Message);
  end;
end;

procedure TUnFMX_PushNotifications.DoServiceConnectionChange(Sender: TObject;
  PushChanges: TPushService.TChanges);
begin
  try
    if TPushService.TChange.DeviceToken in PushChanges then
      VAR_PushNotifications_DeviceToken := VAR_PushService.DeviceTokenValue[TPushService.TDeviceTokenNames.DeviceToken];
    //if VAR_PushNotifications_Memo <> nil then
      //VAR_PushNotifications_Memo.Lines.Add(VAR_PushNotifications_DeviceToken);
    //
    App_PF.PushNotifications_RegistraDevice();
  except
    on E: Exception do
      if CO_Informa_Testes then
        Grl_Geral.MB_Erro('Erro capturado em DoServiceConnectionChange:' + sLineBreak + E.Message);
  end;
end;

procedure TUnFMX_PushNotifications.ObtemTodasNotificacoesStartapp(MemoLog: TMemo);
var
  //PushService: TPushService;
  //ServiceConnection: TPushServiceConnection;
  Notifications: TArray<TPushServiceNotification>;
  //
  FDeviceId: String;
begin
(*
  VAR_PushService :=
    TPushServiceManager.Instance.GetServiceByName(TPushService.TServiceNames.GCM);
  VAR_ServiceConnection := TPushServiceConnection.Create(PushService);
*)
  VAR_ServiceConnection.Active := True;
  VAR_ServiceConnection.OnChange := DoServiceConnectionChange; //OnServiceConnectionChange;
  VAR_ServiceConnection.OnReceiveNotification := DoReceiveNotificationEvent; //OnReceiveNotificationEvent;
  FDeviceId :=
    VAR_PushService.DeviceIDValue[TPushService.TDeviceIDNames.DeviceId];
  MemoLog.Lines.Add('DeviceID: ' + FDeviceId);
  MemoLog.Lines.Add('Ready to receive!');
   // Checks notification on startup, if application was launched fromcold start
  // by tapping on Notification in Notification Center
  Notifications := VAR_PushService.StartupNotifications;
  if Length(Notifications) > 0 then
  begin
      MemoLog.Lines.Add('-----------------------------------------');
      MemoLog.Lines.Add('DataKey = ' + Notifications[0].DataKey);
      MemoLog.Lines.Add('Json = ' + Notifications[0].Json.ToString);
      MemoLog.Lines.Add('DataObject = ' +
        Notifications[0].DataObject.ToString);
      MemoLog.Lines.Add('-----------------------------------------');
  end;
end;

procedure TUnFMX_PushNotifications.PushNotifications_FormActivate(
  Sender: TObject);
var
  Notif:  TPushData;
  I, N: integer;
  //ArrNotifMobile: TArrNotifMobile;
  //  Teste
   Notifications: TArray<TPushServiceNotification>;
  //
  DadosNotif: TArr1NivJSON;
begin
  try
    if VAR_PushNotifications_PushEvents <> nil then
    begin
      notif := VAR_PushNotifications_PushEvents.StartupNotification; // notificaoes que abriram meu app...
      //

{
      // Inicio teste
      Notifications := VAR_PushService.StartupNotifications;
      N := Length(Notifications);
      if N > 0 then
      begin
        for I := 0 to N - 1 do
        begin
          if VAR_PushNotifications_Memo <> nil then
          begin
            VAR_PushNotifications_Memo.Lines.Add('');
            VAR_PushNotifications_Memo.Lines.Add('-----------------------------------------');
            VAR_PushNotifications_Memo.Lines.Add('      Notifica��o ' + Grl_Geral.FF0(I));
            VAR_PushNotifications_Memo.Lines.Add('-----------------------------------------');
            VAR_PushNotifications_Memo.Lines.Add('DataKey = '    + Notifications[I].DataKey);
            VAR_PushNotifications_Memo.Lines.Add('Json = '       + Notifications[I].Json.ToString);
            VAR_PushNotifications_Memo.Lines.Add('DataObject = ' + Notifications[I].DataObject.ToString);
            VAR_PushNotifications_Memo.Lines.Add('-----------------------------------------');
            VAR_PushNotifications_Memo.Lines.Add('');
          end;
        end;
      end;
      //Fim teste
}

      try
      // descomentar ap�s teste
(**)
        if Assigned(notif) then
        begin
         //App_PF.PushNotifications_ReceiveDestrincha(notif);
          //
          //if CO_Informa_Testes then
            //Grl_Geral.MB_Info('Form activate: ' + sLineBreak + notif.Message);
          // Android...
          if VAR_PushNotifications_PushEvents.StartupNotification.GCM.Message <> '' then
          begin
            for I := 0 to notif.Extras.Count - 1 do
            begin
              SetLength(DadosNotif, I + 1);
              DadosNotif[I, 0] := notif.Extras[I].Key;
              DadosNotif[I, 1] := notif.Extras[I].Value;
              //
              if VAR_PushNotifications_Memo <> nil then
                VAR_PushNotifications_Memo.lines.Add(notif.Extras[I].Key + ' = ' + notif.Extras[I].Value);
            end;
            I := Length(DadosNotif);
            SetLength(DadosNotif, I + 1);
            DadosNotif[I, 0] := 'message';
            DadosNotif[I, 1] := notif.Message;
          end;

          // iOS...
          if VAR_PushNotifications_PushEvents.StartupNotification.APS.Alert <> '' then
          begin
            for I := 0 to notif.Extras.Count - 1 do
            begin
              SetLength(DadosNotif, I + 1);
              DadosNotif[I, 0] := notif.Extras[I].Key;
              DadosNotif[I, 1] := notif.Extras[I].Value;
              //
              if VAR_PushNotifications_Memo <> nil then
                VAR_PushNotifications_Memo.lines.Add(notif.Extras[I].Key + ' = ' + notif.Extras[I].Value);
            end;
            I := Length(DadosNotif);
            SetLength(DadosNotif, I + 1);
            DadosNotif[I, 0] := 'alert';
            DadosNotif[I, 1] := notif.Message;
          end;
          //
          App_PF.PushNotifications_ReceiveDestrincha2(DadosNotif);
          //
        end;
        //
// Fim Descomentar ap�s teste!
(**)
      finally
        if notif <> nil then
          DisposeOf;
      end;
      //
    end;
  except
    on E: Exception do
      if CO_Informa_Testes then
        Grl_Geral.MB_Erro('Erro capturado em FormActivate:' + sLineBreak +
        //'Posi��o: ' + Grl_Geral.FF0(P) + sLineBreak +
        E.Message);
  end;
end;

procedure TUnFMX_PushNotifications.PushNotifications_FormShow(Sender: TObject);
begin
// Notifica��es Push...
  try
    {$IFDEF IOS}
      VAR_PushService := TPushServiceManager.Instance.GetServiceByName(TPushService.TServiceNames.APS);
      VAR_ServiceConnection := TPushServiceConnection.Create(PushService);
      VAR_ServiceConnection.OnChange := DoServiceConnectionChange;
      VAR_ServiceConnection.OnReceiveNotification := DoReceiveNotificationEvent;
      TTask.run(procedure
      begin
        VAR_ServiceConnection.Active := True;
      end);
    {$ELSE}
      VAR_PushService := TPushServiceManager.Instance.GetServiceByName(TPushService.TServiceNames.GCM);
      VAR_PushService.AppProps[TPushService.TAppPropNames.GCMAppID] := VAR_PushNotifications_CodigoProjeto; //'2 0 5 8 0 7 9 6 8 2 8 2'; // Troque aqui e no componente EMSProvider...
      VAR_ServiceConnection := TPushServiceConnection.Create(VAR_PushService);
      VAR_ServiceConnection.OnChange := DoServiceConnectionChange;
      VAR_ServiceConnection.OnReceiveNotification := DoReceiveNotificationEvent;
      //
      TTask.run(procedure
      begin
        VAR_ServiceConnection.Active := True;
      end);
      //
    {$ENDIF}
     //-------------------
  except
    on E: Exception do
      if CO_Informa_Testes then
        Grl_Geral.MB_Erro('Erro capturado em FormShow:' + sLineBreak + E.Message);
  end;
end;

end.

