unit UnFMX_DmkWeb;

interface

uses System.Classes, System.UITypes,
  System.SysUtils, System.Variants, IdURI, IdHTTP, REST.Client,
  REST.Types, Net.HttpClient, System.JSON;

type
  TUnFMX_DmkWeb = class(TObject)
  private
    procedure ResetRESTComponentsToDefaults(RESTRequest: TRESTRequest;
              RESTResponse: TRESTResponse; RESTClient: TRESTClient);
    function  REST_URL_Post_0(HeaderNam, HeaderVal, ParamsNam,
              ParamsVal: array of string; URL: String; var StatusCode: Integer;
              ContentType: String = 'XML'): String;
    function  REST_URL_Post_1(HeaderNam, HeaderVal, ParamsNam,
              ParamsVal: array of string; URL: String; var StatusCode: Integer;
              ContentType: String = 'XML'): String;

  public
    function  REST_URL_Post(*_Antigo*)(HeaderNam, HeaderVal, ParamsNam,
              ParamsVal: array of string; URL: String; var StatusCode: Integer;
              ContentType: String): String;
    function  REST_URL_Post_2(URL, Recurso, Token, CNPJ, Ambiente, SQL: String;
              var StatusCode: Integer; ContentType: String = 'XML'): String;

    function  REST_XML_PUT(HeaderNam, HeaderVal: array of string; ParamVal,
              URL: String; var StatusCode: Integer;
              ContentType: String = 'XML'): String;
    function  VerificaConexaoWeb(var Msg: String): Integer;
    function  URLPut(URL: String; VarValue, Resultado: TStringStream;
              HeaderNome, HeaderValor: array of String): String;
    function  URLPost(URL: String; VarsValues: array of String;
              Resultado: TStringStream): Boolean;
  end;

var
  FMX_dmkWeb: TUnFMX_DmkWeb;
  VAR_URL: String;
const
  //CO_URL_DMKAPI     = 'http://localhost:3571/dmkREST.php';
  CO_URL_DMKAPI = 'http://dmk.dermatek.net.br/dmkREST.php';
  CO_WEBID = 'dmk';

implementation

uses
{$IF DEFINED(iOS) or DEFINED(ANDROID)}
Android_FMX_NetworkState,
{$ELSE}
WiniNet, Windows,
{$ENDIF}
UnGrl_Geral, UnGrl_Consts;

{ TUnFMX_DmkWeb }

function TUnFMX_DmkWeb.VerificaConexaoWeb(var Msg: String): Integer;
var
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  NS: TUnFMX_NetworkState;
  {$ELSE}
  Conected: Boolean;
  Flags: dword;
  {$ENDIF}
  Res: Integer;
begin
  //-1 => Falha ao verificar
  // 0 => N�o conectado
  // 1 => Conectado via WIFI
  // 2 => Conectado via WWAN
  // 3 => Conectado via Proxy
  // 4 => Conectado via Modem Busy
  // Para Android permiss�o => Access Wifi State
  Msg := '';
  Res := 0;
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    NS := TUnFMX_NetworkState.Create;
    try
      if not NS.IsConnected then
        Res := 0
      else if NS.IsWifiConnected then
        Res := 1
      else if NS.IsMobileConnected then
        Res := 2
      else
        Res := -1;
      Msg := NS.CurrentSSID;
    finally
      NS.Free;
    end;
  {$ELSE}
    Conected := internetgetconnectedstate(@flags, 0);
    if Conected then
    begin
      if (flags and internet_connection_modem) = internet_connection_modem then
        Res := 2
      else if (flags and internet_connection_lan) = internet_connection_lan then
        Res := 1
      else if (flags and internet_connection_proxy) = internet_connection_proxy then
        Res := 3
      else if (flags and internet_connection_modem_busy) = internet_connection_modem_busy then
        Res := 4
      else
        Res := -1;
    end;
  {$ENDIF}
  Result := Res;
end;

function TUnFMX_DmkWeb.URLPut(URL: String; VarValue, Resultado: TStringStream;
  HeaderNome, HeaderValor: array of String): String;
var
  HTTP: TIdHTTP;
  i: Integer;
  Res: String;
begin
  Res  := '';
  //
  if Length(HeaderNome) <> Length(HeaderValor) then
  begin
    Grl_Geral.MB_Aviso('O header deve possuir a mesma quantidade de itens para: Nome e Valor!');
    Exit;
  end;
  //
  HTTP := TIdHTTP.Create;
  //
  try
    if HTTP.Connected then
      HTTP.Disconnect;
    //
    HTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    //
    if Length(HeaderNome) > 0 then
    begin
      for i := 0 to Length(HeaderNome) do
      begin
        HTTP.Request.CustomHeaders.AddValue(HeaderNome[i], HeaderValor[i]);
      end; 
    end;
    //
    Res := HTTP.Put(URL, VarValue);
    //
    Result := Res;
  finally
    HTTP.Free;
  end;
end;

function TUnFMX_DmkWeb.REST_URL_Post_0(HeaderNam, HeaderVal, ParamsNam,
  ParamsVal: array of string; URL: String; var StatusCode: Integer;
  ContentType: String): String;
begin

end;

function TUnFMX_DmkWeb.REST_URL_Post_1(HeaderNam, HeaderVal, ParamsNam,
  ParamsVal: array of string; URL: String; var StatusCode: Integer;
  ContentType: String = 'XML'): String;
var
  Conexao, Resp, i: Integer;
  Msg: String;
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  //Param: TRESTRequestParameter;
begin
  StatusCode := 0;
  Result     := '';
  //
  if Length(ParamsNam) <> Length(ParamsVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros e de valores de par�metros devem ser os mesmos!');
    Exit;
  end;
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
  Conexao := VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end;
  RESTRequest  := TRESTRequest.Create(nil);
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL     := URL;
    //RESTRequest.ContentType := ctAPPLICATION_JSON;
    RESTClient.ContentType := ContentType;
    RESTRequest.Method     := TRESTRequestMethod.rmPOST;
    RESTRequest.Client     := RESTClient;
    RESTRequest.Response   := RESTResponse;
    //

    RESTRequest.Resource := ''; //'application/json';



      if Length(HeaderNam) > 0 then
    begin
      for i := Low(HeaderNam) to High(HeaderNam) do
      begin
        RESTRequest.Params.AddHeader(HeaderNam[i], HeaderVal[i]);
      end;
    end;
    //
    if Length(ParamsNam) > 0 then
    begin
      for i := Low(ParamsNam) to High(ParamsNam) do
      begin
(*
        Param       := RESTRequest.Params.AddItem;
        Param.name  := UTF8Encode(ParamsNam[i]);
        Param.Value := UTF8Encode(ParamsVal[i]);
        Param.Kind  := TRESTRequestParameterKind.pkGETorPOST;
*)
        RESTRequest.AddParameter(UTF8Encode(ParamsNam[i]), UTF8Encode(ParamsVal[i]), TRESTRequestParameterKind.pkGETorPOST);

      end;
    end;
    //
    // Marco
    try
      RESTRequest.Execute;
    except
      //on E: ENetHTTPClientException do
      on E: Exception do
      begin
        StatusCode := -1;
        Result     := E.Message;
        Grl_Geral.MB_Erro('FMX-DmkWeb-RUP' + sLineBreak +
        'N�o foi poss�vel acessar o servidor!' + sLineBreak +
        'URL: ' + URL + sLineBreak +
        'HeaderNam ' + Grl_Geral.ATS(HeaderNam) + sLineBreak +
        //'HeaderVal ' + Grl_Geral.ATS(HeaderVal) + sLineBreak +  Cuidado! pode trer dados sigilosos como senha ou token!
        'ParamsNam ' + Grl_Geral.ATS(ParamsNam) + sLineBreak +
        //'ParamsVal ' + Grl_Geral.ATS(ParamsVal) + sLineBreak +  Cuidado! pode trer dados sigilosos como senha ou token!
        'Caso esteja usando wifi sem acesso � internet, tente habilitar somente os dados m�veis!'
        + sLineBreak +
        'Resposta: ' + RESTResponse.Content);
      end;
    end;
    if StatusCode = -1 then
      Exit;
    //
    StatusCode := RESTResponse.StatusCode;
    //
    Result := RESTResponse.Content;
    //
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;

function TUnFMX_DmkWeb.REST_URL_Post_2(URL, Recurso, Token, CNPJ, Ambiente, SQL:
  String; var StatusCode: Integer; ContentType: String): String;
(*
var
  Url: String;
  Client: TRESTClient;
  Request: TRESTRequest;
  Json: TJSONValue;
begin
  Url := 'http://dermatek.com.br/api/';
*)
var
  Conexao, Resp, i: Integer;
  Msg: String;
  Request: TRESTRequest;
  Client: TRESTClient;
  //RESTResponse: TRESTResponse;
  //Param: TRESTRequestParameter;
   Json: TJSONValue;
begin
  StatusCode := 0;
  Result     := '';
  //
(*
  if Length(ParamsNam) <> Length(ParamsVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros e de valores de par�metros devem ser os mesmos!');
    Exit;
  end;
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
*)
  Conexao := VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end;


  Client := TRESTClient.Create(URL);

  Request := TRESTRequest.Create(nil);
  Request.Client := Client;
  Request.Method := rmPOST;
  Request.Resource := Recurso; //'sql/executa/v2';

  // 'ws00ok0g0cwggcw8oscko4ks4k4oko8csk48kc40'
  Request.AddParameter('token', Token, TRESTRequestParameterKind.pkHTTPHEADER);
  // '96734892000123'
  Request.AddParameter('cnpj', Cnpj, TRESTRequestParameterKind.pkREQUESTBODY);
  //'production'
  Request.AddParameter('ambiente', Ambiente, TRESTRequestParameterKind.pkREQUESTBODY);
  //Request.AddParameter('sql', 'SELECT * FROM entidades LIMIT 1', TRESTRequestParameterKind.pkREQUESTBODY);
  Request.AddParameter('sql', SQL, TRESTRequestParameterKind.pkREQUESTBODY);

  Request.Execute;
  Result := Request.Response.Content;
  //ShowMessage(Result);

  Json := Request.Response.JSONValue; //Devolve o resultado em um json

  StatusCode := Request.Response.StatusCode;
  //ShowMessage(IntToStr(Request.Response.StatusCode));

  Request := nil;


end;

function TUnFMX_DmkWeb.REST_URL_Post(*_Antigo*)(HeaderNam, HeaderVal, ParamsNam,
  ParamsVal: array of string; URL: String; var StatusCode: Integer;
  ContentType: String): String;
var
  Conexao, Resp, i: Integer;
  Msg: String;
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
begin
  StatusCode := 0;
  Result     := '';
  //
  if Length(ParamsNam) <> Length(ParamsVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros e de valores de par�metros devem ser os mesmos!');
    Exit;
  end;
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
  Conexao := VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end;
  RESTRequest  := TRESTRequest.Create(nil);
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL     := URL;
    //RESTRequest.ContentType := ctAPPLICATION_JSON;
    RESTClient.ContentType := ContentType;
    RESTRequest.Method     := TRESTRequestMethod.rmPOST;
    RESTRequest.Client     := RESTClient;
    RESTRequest.Response   := RESTResponse;
    //
    if Length(HeaderNam) > 0 then
    begin
      for i := Low(HeaderNam) to High(HeaderNam) do
      begin
        RESTRequest.Params.AddHeader(HeaderNam[i], HeaderVal[i]);
      end;
    end;
    //
    if Length(ParamsNam) > 0 then
    begin
      for i := Low(ParamsNam) to High(ParamsNam) do
      begin
        Param       := RESTRequest.Params.AddItem;
        Param.name  := UTF8Encode(ParamsNam[i]);
        Param.Value := UTF8Encode(ParamsVal[i]);
        Param.Kind  := TRESTRequestParameterKind.pkGETorPOST;
      end;
    end;
    //
    // Marco
    try
      RESTRequest.Execute;
    except
      //on E: ENetHTTPClientException do
      on E: Exception do
      begin
        StatusCode := -1;
        Result     := E.Message;
        Grl_Geral.MB_Erro('FMX-DmkWeb-RUP' + sLineBreak +
        'N�o foi poss�vel acessar o servidor!' + sLineBreak +
        'URL: ' + URL + sLineBreak +
        'HeaderNam ' + Grl_Geral.ATS(HeaderNam) + sLineBreak +
        //'HeaderVal ' + Grl_Geral.ATS(HeaderVal) + sLineBreak +  Cuidado! pode trer dados sigilosos como senha ou token!
        'ParamsNam ' + Grl_Geral.ATS(ParamsNam) + sLineBreak +
        //'ParamsVal ' + Grl_Geral.ATS(ParamsVal) + sLineBreak +  Cuidado! pode trer dados sigilosos como senha ou token!
        'Caso esteja usando wifi sem acesso � internet, tente habilitar somente os dados m�veis!'
        + sLineBreak +
        'Resposta: ' + RESTResponse.Content);
      end;
    end;
    if StatusCode = -1 then
      Exit;
    //
    StatusCode := RESTResponse.StatusCode;
    //
    Result := RESTResponse.Content;
    //
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;

function TUnFMX_DmkWeb.REST_XML_PUT(HeaderNam, HeaderVal: array of string;
  ParamVal, URL: String; var StatusCode: Integer;
  ContentType: String = 'XML'): String;
var
  Conexao, Resp, i: Integer;
  Msg: String;
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
begin
  StatusCode := 0;
  Result     := '';
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
  Conexao := VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end;
  RESTRequest  := TRESTRequest.Create(nil);
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL     := URL;
    RESTClient.ContentType := ContentType;
    RESTRequest.Method     := TRESTRequestMethod.rmPUT;
    RESTRequest.Client     := RESTClient;
    RESTRequest.Response   := RESTResponse;
    //
    if Length(HeaderNam) > 0 then
    begin
      for i := Low(HeaderNam) to High(HeaderNam) do
      begin
        RESTRequest.Params.AddHeader(HeaderNam[i], HeaderVal[i]);
      end;
    end;
    //
    if Length(ParamVal) > 0 then
    begin
      Param             := RESTRequest.Params.AddItem;
      Param.ContentType := TRESTContentType.ctAPPLICATION_ATOM_XML;
      Param.Kind        := TRESTRequestParameterKind.pkREQUESTBODY;
      Param.Value       := ParamVal;
    end;
    //
    StatusCode := RESTResponse.StatusCode;
    //
    RESTRequest.Execute;
    //
    Result := RESTResponse.Content;
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;

procedure TUnFMX_DmkWeb.ResetRESTComponentsToDefaults(RESTRequest: TRESTRequest;
  RESTResponse: TRESTResponse; RESTClient: TRESTClient);
begin
  RESTRequest.ResetToDefaults;
  RESTResponse.ResetToDefaults;
  RESTClient.ResetToDefaults;
end;

function TUnFMX_DmkWeb.URLPost(URL: String; VarsValues: array of String;
  Resultado: TStringStream): Boolean;
var
  HTTP: TIdHTTP;
  Vars: TStringList;
  i: Integer;
  Valor: String;
begin
  Vars := TStringList.Create;
  HTTP := TIdHTTP.Create;
  try
    for i := 0 to Length(VarsValues) - 1 do
    begin
      Valor := VarsValues[i];
      if Valor <> '' then
        Vars.Add(Valor);
    end;
    //
    HTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    HTTP.Post(URL, Vars, Resultado);
    //
    Result := True;
  finally
    Vars.Free;
    HTTP.Free;
  end;
end;

end.

