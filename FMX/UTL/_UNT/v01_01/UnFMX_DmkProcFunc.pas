unit UnFMX_DmkProcFunc;

interface

uses System.Classes, System.UITypes, FMX.Controls, FMX.Forms, FMX.ListBox,
  FMX.StdCtrls, FMX.Dialogs, FMX.Types, FMX.Layouts, System.SysUtils,
  System.Variants, System.Rtti, System.Types, System.DateUtils, FMX.Graphics,
  FMX.Edit,
  // MultiDetail
  FMX.ListView.Types, FMX.ListView.Appearances, MultiDetailAppearanceU,
  // Fim MultiDetail
  {$IF DEFINED(ANDROID)}
  Android_FMX_ProcFunc,
  {$ELSE}
  Windows_FMX_ProcFunc,
  {$ENDIF}
  Data.Bind.Components, System.Notification, System.Math, FMX.Surfaces,
  System.IOUtils, FMX.Grid, UnGrl_Vars,
  // Form KeyUp
  FMX.Platform, FMX.VirtualKeyboard, FMX.Objects,
  UnGrl_Consts;

type
  TUnFMX_DmkProcFunc = class(TObject)
  private

  public
    procedure Informa(Item1: TListBoxItem; Aguarde: Boolean; Texto: String);
    function  DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
    function  Informa2(Item1, Item2: TListBoxItem; Aguarde: Boolean;
              Texto: String): Boolean;
    // S T R I N G  /  T E X T O
    function  QuebraTextoEmGrid(Grid: TGrid; Coluna: Integer; Texto: String): String;
    function  GetSelectedText(AObject: TObject): String;
    function  GetSelectedValue(AObject: TObject): Integer;
    function  VariantToString(Variant: Variant): String;
    function  FIC(FaltaInfoCompo: Boolean; Compo: TComponent; Mensagem: String;
              ExibeMsg: Boolean = True): Boolean;
    // I M A G E N S
    function  CriarFotoArredondada(const Imagem: TBitmap): TBitmap;
    // J A N E L A S
    procedure  ManterTelaLigada(Ligada: Boolean);
    procedure Configura_PopUpDeBotao_AllOS(Menu: TListBox; Objeto: TObject;
              Objeto2: TObject = nil);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
              Shift: TShiftState);
    procedure OcultaFormPrincipal(Sender: TObject; var Key: Word; var KeyChar:
              Char; Shift: TShiftState);
    // N O T I F I C A � � E S
    function  VerificaConexaoWeb(var Msg: String): Integer;
    procedure GeraNotificacao(NotificationCenter: TNotificationCenter;
              Nome, Titulo, Texto: String);
    // W E B
    procedure AbrirURLBrowser(const URL: string);
    // O U T R O S
    procedure VibrarComoBotao();
    procedure DefTextoLVMDI(LItem: TListViewItem; Item, FontSize: Integer;
              Texto: String);
    procedure OcutaTecladoSeExibido();

    // COMPONENTES
    procedure CorrigeCorLabel(LaX: TLabel);
    procedure LabelInRectanglePaint(Sender: TObject; Canvas: TCanvas; const
              ARect: TRectF);
    function  IsLabelRunTimeCreateInRectangle(Sender: TRectangle): Boolean;
    procedure AlturaRectRunTimeCreateInRectangle1(VSB: TVertScrollBox; Rectangle:
              TRectangle; Text: TText; Linhas: array of String);

  end;

var
  FMX_dmkPF: TUnFMX_DmkProcFunc;

implementation

uses UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums;

{ TUnFMX_DmkProcFunc }

procedure TUnFMX_DmkProcFunc.AlturaRectRunTimeCreateInRectangle1(VSB:
  TVertScrollBox; Rectangle: TRectangle; Text: TText; Linhas: array of String);
var
  I: Integer;
  N, W, ctW, ctH: Single;
  S: String;
begin
  W := VSB.Width - Rectangle.Margins.Left - Rectangle.Margins.Right - Text.Margins.Left - Text.Margins.Right;
  N := 0;
  S := '';
  for I := 0 to Length(Linhas) - 1 do
  begin
    ctW := Text.Canvas.TextWidth(Linhas[I]);
    N := N + Trunc(ctW / W) + 1;
    S := S + sLineBreak +  Linhas[I];
  end;
  if N = 0 then N := 1;
  //
  Text.Text := S;
  ctH := Text.Canvas.TextHeight('j|');
  Rectangle.Height := (N * ctH) + Text.Margins.Top + Text.Margins.Bottom; // + 10;
  //
end;

procedure TUnFMX_DmkProcFunc.Configura_PopUpDeBotao_AllOS(Menu: TListBox;
  Objeto: TObject; Objeto2: TObject = nil);
var
  FP1, FP2: TPointF;
begin
  FP1.X := 0;
  FP1.Y := 0;
  //
  if Objeto is TSpeedButton then
  begin
    FP1 := TSpeedButton(Objeto).LocalToAbsolute(FP1);
    //FP1 := TSpeedButton(Objeto).ClientToScreen(FP1);
  end else
  if Objeto is TButton then
  begin
    FP1 := TButton(Objeto).LocalToAbsolute(FP1);
  end else
  if (Objeto is TCustomListBox) and ((Objeto2 <> nil) and (Objeto2 is TListBoxItem)) then
  begin
    FP2.X := 0;
    FP2.Y := 0;
    //
    FP2 := TCustomListBox(Objeto).LocalToAbsolute(FP2);
    FP1 := TListBoxItem(Objeto2).LocalToAbsolute(FP1);
    //
    FP1.X := FP1.X - FP2.X;
    FP1.Y := FP1.Y - FP2.Y;
  end;
  Menu.ClearSelection;
  Menu.Visible    := not Menu.Visible;
  Menu.Position.X := FP1.X;
  Menu.Position.Y := FP1.Y;
  //
  if Menu.Visible then
  begin
    Menu.ApplyStyleLookup;
    Menu.RealignContent;
  end;
end;

procedure TUnFMX_DmkProcFunc.CorrigeCorLabel(LaX: TLabel);
begin
  //LaX.StyledSettings := LaX.StyledSettings - [TStyledSetting.ssFontColor];
  LaX.StyledSettings := LaX.StyledSettings - [TStyledSetting.FontColor];
  LaX.FontColor := VAR_DMK_COLOR_TEXT;
end;

function TUnFMX_DmkProcFunc.VariantToString(Variant: Variant): String;
var
  DataHora: TDateTime;
begin
  case TVarData(Variant).VType of
    varDate:
    begin
      DataHora := VarToDateTime(Variant);
      Result   := FMX_Geral.FDT(DataHora, 109);
    end;
    varDouble:
      Result := FMX_Geral.FFT_Dot(Variant, 2, siNegativo);
    else
      Result := VarToStr(Variant);
  end;
end;

function TUnFMX_DmkProcFunc.VerificaConexaoWeb(var Msg: String): Integer;
begin
  {$IF DEFINED(ANDROID)}
  Result := Android_FMX_dmkPF.VerificaConexaoWeb(Msg);
  {$ELSE}
  Result := Windows_FMX_dmkPF.VerificaConexaoWeb(Msg);
  {$ENDIF}
end;

procedure TUnFMX_DmkProcFunc.VibrarComoBotao;
begin
  Application.ProcessMessages;
  {$IF DEFINED(ANDROID)}
  Android_FMX_dmkPF.VibrarComoBotao();
  {$ELSE}
  Windows_FMX_dmkPF.VibrarComoBotao();
  {$ENDIF}
end;

procedure TUnFMX_DmkProcFunc.AbrirURLBrowser(const URL: string);
begin
  {$IF DEFINED(ANDROID)}
  Android_FMX_dmkPF.AbrirURLBrowser(URL);
  {$ELSE}
  Windows_FMX_dmkPF.AbrirURLBrowser(URL);
  {$ENDIF}
end;

function TUnFMX_DmkProcFunc.CriarFotoArredondada(const Imagem: TBitmap): TBitmap;
var
  Tam: Integer;
begin
  Result := TBitmap.Create;
  Tam    := Min(Imagem.Height, Imagem.Width);
  //
  Result.SetSize(Tam, Tam);
  Result.Clear(TAlphaColorRec.Null);
  //
  if Result.Canvas.BeginScene then
  begin
    try
      Result.Canvas.Fill.Bitmap.Bitmap.Assign(Imagem);
      Result.Canvas.Fill.Kind := TBrushKind.Bitmap;
      Result.Canvas.FillEllipse(TRectF.Create(0, 0, Tam, Tam), 1);
    finally
      Result.Canvas.EndScene;
    end;
  end;
end;

procedure TUnFMX_DmkProcFunc.GeraNotificacao(NotificationCenter: TNotificationCenter;
  Nome, Titulo, Texto: String);
var
  MyNotification: TNotification;
begin
  MyNotification := NotificationCenter.CreateNotification;
  try
    if MyNotification <> nil then
    begin
      MyNotification.Name      := Nome;
      MyNotification.Title     := Titulo;
      MyNotification.AlertBody := Texto;
      MyNotification.FireDate  := Now;

      NotificationCenter.ApplicationIconBadgeNumber := 1;
      NotificationCenter.PresentNotification(MyNotification);
    end else
      FMX_Geral.MB_Aviso(Texto);
  finally
    MyNotification.DisposeOf;
  end;
end;

function TUnFMX_DmkProcFunc.GetSelectedText(AObject: TObject): String;
var
  LEditor: IBindListEditorCommon;
begin
  LEditor := GetBindEditor(AObject, IBindListEditorCommon) as IBindListEditorCommon;
  //
  if LEditor <> nil then
    Result := LEditor.SelectedText
  else
    Result := '';
end;

function TUnFMX_DmkProcFunc.GetSelectedValue(AObject: TObject): Integer;
var
  ValStr: String;
  Sel: TValue;
  LEditor: IBindListEditorCommon;
begin
  LEditor := GetBindEditor(AObject, IBindListEditorCommon) as IBindListEditorCommon;
  //
  if LEditor <> nil then
  begin
    Sel     := LEditor.SelectedValue;
    ValStr  := Sel.ToString;

    if (ValStr = '(empty)') or (ValStr = '') then
      Result := 0
    else
      Result := ValStr.ToInteger();
  end else
    Result := 0;
end;

procedure TUnFMX_DmkProcFunc.Informa(Item1: TListBoxItem; Aguarde: Boolean;
  Texto: String);
begin
  if Item1 <> nil then
  begin
    if Aguarde then
      Item1.Text := 'Aguarde... ' + Texto + '...'
    else
      Item1.Text := Texto;
    Application.ProcessMessages;
  end;
end;

function TUnFMX_DmkProcFunc.Informa2(Item1, Item2: TListBoxItem; Aguarde: Boolean;
  Texto: String): Boolean;
begin
  Result := False;
  try
    if Item1 <> nil then
    begin
      if Aguarde then
        Item1.Text := 'Aguarde... ' + Texto + '...'
      else
        Item1.Text := Texto;
    end;
    if Item2 <> nil then
    begin
      if Aguarde then
        Item2.Text := 'Aguarde... ' + Texto + '...'
      else
        Item2.Text := Texto;
    end;
    Application.ProcessMessages;
    Result := True;
  except
    // nada
  end;
end;

function TUnFMX_DmkProcFunc.IsLabelRunTimeCreateInRectangle(
  Sender: TRectangle): Boolean;
begin
  Result :=
    Copy(Sender.Name, 1, CO_RUN_TIME_CREATED_LENGHT) = CO_RUN_TIME_CREATED_PREFIX;
end;

procedure TUnFMX_DmkProcFunc.LabelInRectanglePaint(Sender: TObject;
  Canvas: TCanvas; const ARect: TRectF);
begin
  TRectangle(TText(Sender).Parent).Height := TText(Sender).Height + 10;
end;

procedure TUnFMX_DmkProcFunc.ManterTelaLigada(Ligada: Boolean);
begin
  {$IF DEFINED(ANDROID)}
  Android_FMX_dmkPF.ManterTelaLigada(Ligada);
  {$ELSE}
  Windows_FMX_dmkPF.ManterTelaLigada(Ligada);
  {$ENDIF}
end;

procedure TUnFMX_DmkProcFunc.OcultaFormPrincipal(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  {$IF DEFINED(ANDROID)}
  Android_FMX_dmkPF.OcultaFormPrincipal(Sender, Key, KeyChar, Shift);
  {$ELSE}
  Windows_FMX_dmkPF.OcultaFormPrincipal(Sender, Key, KeyChar, Shift);
  {$ENDIF}
end;

procedure TUnFMX_DmkProcFunc.OcutaTecladoSeExibido();
var
  FService : IFMXVirtualKeyboardService;
begin
  TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
  //if (FService <> nil) and (TVirtualKeyboardState.vksVisible in FService.VirtualKeyBoardState) then
  if (FService <> nil) and (TVirtualKeyboardState.Visible in FService.VirtualKeyBoardState) then
  begin
    // Back button pressed, keyboard visible, so do nothing...
///////////////////////////////////////////////////////////////////////////////////
    // ... mas as vezez gera erro de acesso � mem�ria!. (Delphi Segmentation fault (11))
    // Obejetos destruidos/inexistentes referenciados?????
    // Ent�o � melhor for�ar o ocultamento do teclado virtual e retornar Key=0!!!!
     FService.HideVirtualKeyboard;
///////////////////////////////////////////////////////////////////////////////////
  end;
end;

function TUnFMX_DmkProcFunc.QuebraTextoEmGrid(Grid: TGrid; Coluna: Integer;
  Texto: String): String;
var
  I, SpacePos: Integer;
  TxtShow, Teste, Linha, Sentence, Parte: String;
  //
  procedure CopiaParteAtual();
  begin
    Teste := Linha + Parte;
    //
    if Grid.Canvas.TextWidth(Teste) > Grid.Columns[Coluna].Width then
    begin
      TxtShow := TxtShow + Linha + sLineBreak;
      Linha := Parte;
    end else
    begin
      Linha := Linha + Parte;
    end;
  end;
begin
  Linha    := '';
  TxtShow  := '';
  Sentence := Texto;
  while Sentence <> '' do
  begin
    SpacePos := Pos(' ', Sentence);
    if SpacePos > 0 then
    begin
      Parte := Copy(Sentence, 0, SpacePos);
      Sentence := Copy(Sentence, SpacePos + 1, Length(Sentence) - SpacePos);
      CopiaParteAtual();
    end else
    begin
      Parte    := Sentence;
      Sentence := '';
      CopiaParteAtual();
    end;
  end;
  if Trim(Linha) <> '' then
    TxtShow := TxtShow + Linha;
  Result := TxtShow;
end;

function TUnFMX_DmkProcFunc.DateTime_MyTimeZoneToUTC(DataHora: TDateTime): TDateTime;
begin
  Result := TTimeZone.Local.ToUniversalTime(DataHora);
end;

procedure TUnFMX_DmkProcFunc.DefTextoLVMDI(LItem: TListViewItem; Item, FontSize: Integer;
  Texto: String);
const
  sProcFunc = 'TUnFMX_DmkProcFunc.DefTextoLVMDI()';
var
  sItem: String;
begin
   case Item of
    1: sItem := TMultiDetailAppearanceNames.Detail1;
    2: sItem := TMultiDetailAppearanceNames.Detail2;
    3: sItem := TMultiDetailAppearanceNames.Detail3;
    else
    begin
      sItem := '';
      FMX_Geral.MB_Erro('Item ' + FMX_Geral.FF0(Item) + ' indefinido em ' +
      sProcFunc);
    end;
  end;
  TListItemText(LItem.Objects.FindObjectT<TListItemText>(sItem)).Font.Size := FontSize;
  LItem.Data[sItem] := Format('%s', [Texto]);
end;

function TUnFMX_DmkProcFunc.FIC(FaltaInfoCompo: Boolean; Compo: TComponent;
  Mensagem: String; ExibeMsg: Boolean): Boolean;
begin
  if FaltaInfoCompo then
  begin
    Result := True;
    if (Mensagem <> '') and ExibeMsg then
      FMX_Geral.MB_Aviso(Mensagem);
    if Compo <> nil then
    begin
      if Compo is TEdit then
        TEdit(Compo).SetFocus;
    end;
  end else
    Result := False;
end;

procedure TUnFMX_DmkProcFunc.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  FService : IFMXVirtualKeyboardService;
begin
  try
    if Key = vkHardwareBack then
    begin
      TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
      //if (FService <> nil) and (TVirtualKeyboardState.vksVisible in FService.VirtualKeyBoardState) then
      if (FService <> nil) and (TVirtualKeyboardState.Visible in FService.VirtualKeyBoardState) then
      begin
        // Back button pressed, keyboard visible, so do nothing...
///////////////////////////////////////////////////////////////////////////////////
        // ... mas as vezez gera erro de acesso � mem�ria!. (Delphi Segmentation fault (11))
        // Obejetos destruidos/inexistentes referenciados?????
        // Ent�o � melhor for�ar o ocultamento do teclado virtual e retornar Key=0!!!!
         FService.HideVirtualKeyboard;
         Key := 0;
///////////////////////////////////////////////////////////////////////////////////
      end else
      begin
        // Back button pressed, keyboard not visible or not supported on this platform, lets exit the app...
  (*    N�o � mais permitido a partir do Delphi Berlin/Tokyo
        if MessageDlg('Exit Application?', TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbOK, TMsgDlgBtn.mbCancel], -1) = mrOK then
        begin
          // Exit application here...
        end else
  *)
        begin
          if Sender is TForm then
          begin
            {$If DEFINED(iOS) or DEFINED(ANDROID)}
              TForm(Sender).Hide;
            {$Else}
              TForm(Sender).Close;
            {$EndIf}
          end;
          // They changed their mind, so ignore the Back button press...
          Key := 0;
        end;
      end;
    end
  except
    on E: Exception do
      FMX_Geral.MB_Erro(E.Message);
  end;
end;

end.
