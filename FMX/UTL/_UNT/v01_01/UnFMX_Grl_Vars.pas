unit UnFMX_Grl_Vars;

interface

uses UnDmkEnums, System.Classes, FireDAC.Comp.Client, FMX.Memo, FMX.Styles, FMX.Controls,
  IPPeerClient, REST.Backend.PushTypes, System.JSON, System.UITypes, REST.Backend.EMSPushDevice,
  System.PushNotification, REST.Backend.EMSProvider,
  Data.Bind.Components, Data.Bind.ObjectScope, REST.Backend.BindSource,
  REST.Backend.PushDevice, FMX.Dialogs, System.Threading;

type
  TProcedure = procedure;
  TNotifyEvent = procedure(Sender: TObject) of object;
  TUnFMX_Grl_Vars = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AddTODO(Acao: String);
    procedure ConfiguraCoresJanelas(Thema: Integer);
  end;
var
  FMX_Grl_Vars: TUnFMX_Grl_Vars;
  //
  // FMX
(*
  VAR_RUN_TIME_CREATED_COUNT: Integer = 0;
  VAR_Form_Fill_Color: TAlphaColor = TAlphaColors.Red; //$FFF0F0FA;
  //Form_Fill.Kind  := TBrushKind.Solid;
  VAR_ToolBarTitle_TintColor: TAlphaColor = TAlphaColors.Yellow; //$FFF0F4FA;
  VAR_LinGrey_Stroke_Color: TAlphaColor = TAlphaColors.Blue; //TAlphaColors.Lightblue;
  VAR_LinBlue_Stroke_Color: TAlphaColor = TAlphaColors.Purple; //TAlphaColors.PowderBlue;
  VAR_LaTitle_Color: TAlphaColor = TAlphaColors.Green; //$FF5C5C5C;
  VAR_LaTexto_Font_Color: TAlphaColor = TAlphaColors.Fuchsia; //$FF898989;
  VAR_LaTexto_Back_Color: TAlphaColor = TAlphaColors.Black; //$FFFFFFFF;
  VAR_LaTitle_Font_Color: TAlphaColor = TAlphaColors.Fuchsia; //TAlphaColors.Skyblue;
  VAR_LaTitle_Back_Color: TAlphaColor = TAlphaColors.Black; //$FFFFFFFF;
*)
  VAR_VIRTUAL_KEYBOARD_BOUNDS: Integer = 200;
  //
  VAR_RUN_TIME_CREATED_COUNT: Integer = 0;
  VAR_Form_Fill_Color: TAlphaColor = $FFF0F0FA;
  //Form_Fill.Kind  := TBrushKind.Solid;
  VAR_ToolBarTitle_TintColor: TAlphaColor = $FFF0F4FA;
  VAR_LinGrey_Stroke_Color: TAlphaColor = TAlphaColors.Lightblue;
  VAR_LinBlue_Stroke_Color: TAlphaColor = TAlphaColors.PowderBlue;
  //VAR_LaTitle_Color: TAlphaColor = $FF5C5C5C;
  VAR_LaFormTitle_Font_Color: TAlphaColor = $FF5C5C5C;
  VAR_LaItemTexto_Font_Color: TAlphaColor = $FF898989;
  VAR_LaItemTexto_Back_Color: TAlphaColor = $FFFFFFFF;
  VAR_LaItemTitle_Font_Color: TAlphaColor = TAlphaColors.Skyblue;
  VAR_LaItemTitle_Back_Color: TAlphaColor = $FFFFFFFF;
  // Float Menu
  VAR_RectMenuBase_Stroke_Color: TAlphaColor = TAlphaColors.Black; // TAlphaColors.Red
  VAR_RectMenuBase_Fill_Color: TAlphaColor = TAlphaColors.Bisque ; // = $FF434343;
  VAR_LaMenuClickNao_FontColor: TAlphaColor = TAlphaColors.Red ; // = $FF434343;
  VAR_LaMenuClickSim_FontColor: TAlphaColor = TAlphaColors.Blue ; // = $FF434343;
  VAR_LaMenuLine1_Stroke_Color: TAlphaColor = TAlphaColors.Yellow ; // = $FFDDDDDD;
  VAR_LaMenuLine2_Stroke_Color: TAlphaColor = TAlphaColors.Fuchsia ; // = $FFDDDDDD;
  VAR_LaMenuTexto_FontColor: TAlphaColor = TAlphaColors.Green ; //    = $FF434343;
  VAR_LaMenuTitulo_FontColor: TAlphaColor = TAlphaColors.Orange ; //   = $FF1C1C1C;
  VAR_LMenuSubTitulo_FontColor: TAlphaColor = TAlphaColors.Aqua ; // = $FF9B9B9B;

  // Q U A L Q U E R   A P P
  //vResourceStream : TResourceStream;
  VAR_CHAMOU_APP_EXTERNO: Boolean = False;

  // D B
  VAR_APP_DB_TYPE: TDmkDBMSs;
  VAR_APP_DBUsuarioNome: String;
  //VAR_APP_DB_NAME_ONLY,
  TMeuDB: String;
  VAR_APP_DB_NAME_E_EXT: String;
  VAR_APP_DB_FULLPATH: String;
  VAR_RDBMS_DriverID: String;
  VAR_RDBMS_ODBCDriver: String;
  VAR_RDBMS_SERVER: String;
  VAR_RDBMS_PORT: String;
  VAR_RDBMS_PWD: String;
  VAR_RDBMS_USER: String;
  VAR_LOGIN: String;
  VAR_SENHA: String;
  VAR_SQLITE_DB_LOCAL_SERVER: TFDConnection;
  VAR_SQLITE_DB_LOCAL_USER: TFDConnection;
  VAR_SQLITE_DB_WEB_SERVER: TFDConnection;
  VAR_SQLITE_DB_WEB_USER: TFDConnection;
  VAR_PORQUE_VERIFICA: String;
  VAR_ForcaBigIntNeg: Boolean = False;
  // From DB
  VAR_LIB_FILIAIS: String;
  VAR_LIB_EMPRESAS: String;
  VAR_LIB_EMPRESA_SEL: Integer;
  VAR_BOSS: String;
  VAR_SENHATXT: String;
  VAR_NOMEEMPRESA: String;
  VAR_CNPJEMPRESA: String;
  VAR_BOSSLOGIN: String;
  VAR_BOSSSENHA: String;
  VAR_ADMIN: String;
  VAR_FUNCILOGIN: Integer;
  VAR_SKINCONTA: Integer;
  VAR_TODO_IMPLEMENT: String = 'EnterToTab' + sLineBreak;
  //


  // P u s h   N o t i f i c a t i o n s
  VAR_PushNotifications_DeviceToken: String = '';
  VAR_PushNotifications_CodigoProjeto: String = '';
  VAR_PushNotifications_Memo: TMemo = nil;
  VAR_PushNotifications_PushEvents: TPushEvents = nil;
  VAR_PushNotifications_EMSProvider: TEMSProvider = nil;
  VAR_PushService: TPushService = nil;
  VAR_ServiceConnection: TPushServiceConnection = nil;
  // Fim Push Notifications


  // F O R M S
  VAR_LIBERA_TODOS_FORMS: Boolean;
  VAR_CADASTRO: Integer = 0;
  VAR_FORM_ID: String;
  VAR_APP_MAIN_STYLE: String;
  // acesso a forms
  FM_MASTER : String = '';
  //
  // G E O   L O C A L I Z A � � O
  VAR_LOC_LATI_LONGI: Boolean = False;
  VAR_LATITUDE: Double = 0.0000000000;
  VAR_LONGITUDE: Double = 0.0000000000;
  VAR_LOC_DATA_HORA: TDateTime = 0;
  //
  // M I S C E L A N E A
  VAR_SOMAIUSCULAS: Boolean;
  VAR_DEVICE_ID: String = '';
  //

  VAR_API_ID_ENTIDADE: Integer = 0;
  VAR_API_ID_USUARIO: Integer = 0;

implementation

{ TUnFMX_Grl_Vars }

procedure TUnFMX_Grl_Vars.AddTODO(Acao: String);
begin
  VAR_TODO_IMPLEMENT := VAR_TODO_IMPLEMENT + Acao + sLineBreak;
end;

procedure TUnFMX_Grl_Vars.ConfiguraCoresJanelas(Thema: Integer);
begin
  case Thema of
    -1: // Teste
    begin
        VAR_Form_Fill_Color           := TAlphaColors.Red; //$FFF0F0FA;
        //Form_Fill.Kind              := TBrushKind.Solid;
        VAR_ToolBarTitle_TintColor    := TAlphaColors.Yellow; //$FFF0F4FA;
        VAR_LinGrey_Stroke_Color      := TAlphaColors.Blue; //TAlphaColors.Lightblue;
        VAR_LinBlue_Stroke_Color      := TAlphaColors.Purple; //TAlphaColors.PowderBlue;
        // Form - Geral
        VAR_LaFormTitle_Font_Color    := TAlphaColors.Green; //$FF5C5C5C;
        VAR_LaItemTexto_Font_Color    := TAlphaColors.Maroon; //$FF898989;
        VAR_LaItemTexto_Back_Color    := TAlphaColors.Black; //$FFFFFFFF;
        VAR_LaItemTitle_Font_Color    := TAlphaColors.Fuchsia; //TAlphaColors.Skyblue;
        VAR_LaItemTitle_Back_Color    := TAlphaColors.Black; //$FFFFFFFF;
        // Float Menu
        VAR_RectMenuBase_Stroke_Color := TAlphaColors.Red;
        VAR_RectMenuBase_Fill_Color   := TAlphaColors.Bisque ; // = $FF434343;
        VAR_LaMenuClickNao_FontColor  := TAlphaColors.Red ; // = $FF434343;
        VAR_LaMenuClickSim_FontColor  := TAlphaColors.Blue ; // = $FF434343;
        VAR_LaMenuLine1_Stroke_Color  := TAlphaColors.Yellow ; // = $FFDDDDDD;
        VAR_LaMenuLine2_Stroke_Color  := TAlphaColors.Fuchsia ; // = $FFDDDDDD;
        VAR_LaMenuTexto_FontColor     := TAlphaColors.Green ; //    = $FF434343;
        VAR_LaMenuTitulo_FontColor    := TAlphaColors.Orange ; //   = $FF1C1C1C;
        VAR_LMenuSubTitulo_FontColor  := TAlphaColors.Aqua ; // = $FF9B9B9B;
    end;
    else
    begin
      // volta �s default
        VAR_Form_Fill_Color           := $FFF0F0FA;
        //Form_Fill.Kind              := TBrushKind.Solid;
        VAR_ToolBarTitle_TintColor    := $FFF0F4FA;
        VAR_LinGrey_Stroke_Color      := TAlphaColors.Lightblue;
        VAR_LinBlue_Stroke_Color      := TAlphaColors.PowderBlue;
        // Form - Geral
        VAR_LaFormTitle_Font_Color    := $FF5C5C5C;
        VAR_LaItemTexto_Font_Color    := $FF9B9B9B; //$FF434343;
        VAR_LaItemTexto_Back_Color    := $FFFFFFFF;
        VAR_LaItemTitle_Font_Color    := TAlphaColors.Skyblue;
        VAR_LaItemTitle_Back_Color    := $FFFFFFFF;
        // Float Menu
        VAR_RectMenuBase_Stroke_Color := TAlphaColors.Black;
        VAR_RectMenuBase_Fill_Color   := $FFFFFFFF;
        VAR_LaMenuClickNao_FontColor  := TAlphaColors.Indianred; //$FF9B9B9B;
        VAR_LaMenuClickSim_FontColor  := TAlphaColors.Lightseagreen; // $FF9B9B9B;
        VAR_LaMenuLine1_Stroke_Color  := $FFDDDDDD;
        VAR_LaMenuLine2_Stroke_Color  := $FFDDDDDD;
        VAR_LaMenuTexto_FontColor     := $FF9B9B9B;
        VAR_LaMenuTitulo_FontColor    := $FF434343;
        VAR_LMenuSubTitulo_FontColor  := $FF9B9B9B;

    end;
  end;
end;

end.


