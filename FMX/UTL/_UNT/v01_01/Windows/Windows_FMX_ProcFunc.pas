unit Windows_FMX_ProcFunc;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.TabControl, FMX.Edit,
  System.IOUtils, Winapi.ShellAPI, Winapi.UrlMon, Winapi.WinInet, Winapi.Windows;

type
  TWindows_FMX_ProcFunc = class(TObject)
  private
  public
    // T E L A
    procedure  ManterTelaLigada(Ligada: Boolean);
    procedure OcultaFormPrincipal(Sender: TObject; var Key: Word; var KeyChar:
              Char; Shift: TShiftState);
    // W E B
    function  VerificaConexaoWeb(var Msg: String): Integer;
    procedure AbrirURLBrowser(const URL: string);
    // O U T R O S
    procedure VibrarComoBotao();

  end;

var
  Windows_FMX_dmkPF: TWindows_FMX_ProcFunc;

implementation

{ TWindows_FMX_ProcFunc }

procedure TWindows_FMX_ProcFunc.AbrirURLBrowser(const URL: string);
begin
  HlinkNavigateString(nil, Pchar(URL));
end;

procedure TWindows_FMX_ProcFunc.ManterTelaLigada(Ligada: Boolean);
begin
  ShowMessage('A procedure "TWindows_FMX_ProcFunc.ManterTelaLigada"' +
    sLineBreak + 'n�o est� implementada para a plataforma Windows!');
end;

procedure TWindows_FMX_ProcFunc.OcultaFormPrincipal(Sender: TObject;
  var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  Key := Key; // Mudar se precisar!
end;

function TWindows_FMX_ProcFunc.VerificaConexaoWeb(var Msg: String): Integer;
var
  Conected: Boolean;
  Flags: DWORD;
  Res: Integer;
begin
  //-1 => Falha ao verificar
  // 0 => N�o conectado
  // 1 => Conectado via WIFI
  // 2 => Conectado via WWAN
  // 3 => Conectado via Proxy
  // 4 => Conectado via Modem Busy
  // Para Android permiss�o => Access Wifi State
  Msg := '';
  Res := 0;
  //
  Conected := InternetGetConnectedState(@flags, 0);
  if Conected then
  begin
    if (Flags and INTERNET_CONNECTION_MODEM) = INTERNET_CONNECTION_MODEM then
      Res := 2
    else if (Flags and INTERNET_CONNECTION_LAN) = INTERNET_CONNECTION_LAN then
      Res := 1
    else if (Flags and INTERNET_CONNECTION_PROXY) = INTERNET_CONNECTION_PROXY then
      Res := 3
    else if (Flags and INTERNET_CONNECTION_MODEM_BUSY) = INTERNET_CONNECTION_MODEM_BUSY then
      Res := 4
    else
      Res := -1;
  end;
  Result := Res;
end;

procedure TWindows_FMX_ProcFunc.VibrarComoBotao();
begin
  // nada?
end;

end.
