unit UnFMX_Geral;

interface

uses
{$IFDEF MSWINDOWS}
  Winapi.Windows,
{$ENDIF}
  System.SysUtils, System.UITypes, System.Variants, System.IOUtils,
  System.Classes, FMX.Forms, FMX.Dialogs, FMX.Edit, System.MaskUtils,
  Data.DB,
  System.Types, UnDmkEnums,
  TypInfo, UnFMX_Ref;

var
  VAR_FORMATDATE, VAR_FORMATDATE2, VAR_FORMATDATE3, VAR_FORMATDATE4,
  VAR_FORMATDATE5, VAR_FORMATTIME, VAR_FORMATTIME2, VAR_FORMATTIME3,
  VAR_FORMATTIME4, VAR_FORMATTIME5,
  VAR_FORMATDATE6, VAR_FORMATDATE0, VAR_FORMATTIMESTAMP4,
  VAR_FORMATDATETIME, VAR_FORMATDATETIME1, VAR_FORMATDATETIME2,
  VAR_FORMATDATETIME3, VAR_FORMATDATETIME4,
  VAR_FORMATDATEi, VAR_FORMATDATE7, VAR_FORMATDATE8,
  VAR_FORMATDATE9, VAR_FORMATDATE10, VAR_FORMATDATE11, VAR_FORMATDATE12,
  VAR_FORMATDATE13, VAR_FORMATDATE14, VAR_FORMATDATE15, VAR_FORMATDATE16,
  VAR_FORMATDATE17, VAR_FORMATDATE18, VAR_FORMATDATE19, VAR_FORMATDATE20,
  VAR_FORMATDATE21, VAR_FORMATDATE22, VAR_FORMATDATE23, VAR_FORMATDATE24,
  VAR_FORMATDATE25, VAR_FORMATDATE26,
  VAR_FORMATDATECB4: String;
  VAR_GETDATAI, VAR_GETDATAF: TDateTime;
  //
  VAR_FORMATFLOAT0, VAR_FORMATFLOAT1, VAR_FORMATFLOAT2, VAR_FORMATFLOAT3,
  VAR_FORMATFLOAT4, VAR_FORMATFLOAT5, VAR_FORMATFLOAT6, VAR_FORMATFLOAT7,
  VAR_FORMATFLOAT8, VAR_FORMATFLOAT9, VAR_FORMATFLOAT10: String;
  VAR_UNIDADEPADRAO: Integer;
  VAR_NEEDOFFSET: Boolean;
  VAR_KBBOUNDS: TRectF;

  //
{
  VAR_SOMAIUSCULAS, VAR_MAIUSC_MINUSC: Boolean;
  VAR_SAVE_CFG_DBGRID_MENU: TPopupMenu;
  VAR_SAVE_CFG_DBGRID_GRID: TCustomDBGrid;
  VAR_SAVE_CFG_DBGRID_DMKV: TComponent;
}
  //
  VAR_USUARIO: Integer = 0;

const
  CO_JOKE_SQL = '$#'; // Texto livre no S Q L I n s U p d
  CO_SEQ_Numeros = '0123456789';

type
  TUnFMX_Geral = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // F O R M A T A � � E S
    procedure DefineFormatacoes();
    function  FDT(Data: TDateTime; Tipo: Integer; Nulo: Boolean = False): String;
    function  FFN(Numero, Digitos: Integer): String;
    function  FF0(Numero: Integer): String;
    function  FormataCBO2002(CBO2002: String): String;
    function  FormataCEP_TT(CEP: String; Separador: String = '-';
              Default: String = ''): String;
    function  FormataChaveNFe(ChaveNFe: String; Como: TComoFmtChNFe): String;
    function  FormataCNPJ_TFT(Texto: String): String;
    function  FormataCNPJ_TT(Texto: String): String;
    function  Formata_IE(IE: String; UF: Variant; LinkMsk: String;
              Tipo: Integer = 0): String;
    function  FormataNCM(NCM: String): String;
    function  FormataNumeroDeRua(Rua: String; Numero: Variant; Nulo: Boolean): String;
    function  FormataTelefone_TT_Curto(Telefone : String): String;
    function  FormataTelefone_TT(Telefone : String): String;
    function  FormataTelefone_TT_NFe(Telefone : String): String;
    function  FormataVersao(Versao: Variant): String;
    function  FormataUF(UF: Variant): String;
    // N � M E R O S
    function  IMV(Texto: String): Integer;
    function  BoolToInt(Verdade: Boolean): Integer;
    function  IntToBool(Verdade: Integer): Boolean;
    function  MTD(Data: String; Obrigatorio: Boolean): TDateTime; // Mensal To DAte
    function  TFD(Texto: String; Digitos: Integer; Sinal: TSinal) : String;
    function  TFT(Texto: String; Casas: Integer; Sinal: TSinal): String;
    function  TFT_MinMax(Texto: String; Min, Max: Extended; Casas: Integer;
              Sinal: Boolean) : String;
    function  TFT_Min(Texto: String; Min: Double; Casas: Integer; Sinal:
              Boolean): String;
    function  TFT_Max(Texto: String; Max: Double; Casas: Integer; Sinal:
              Boolean): String;
    function  SoNumero_TT(Texto : String) : String;
    function  SoNumeroELetra_TT(Texto : String) : String;
    function  SoNumeroESinal_TT(Texto : String) : String;
    function  SoNumeroESinalEVirgula_TT(Texto : String) : String;
    function  CNPJ_Valida(const EdCNPJ: TCustomEdit; var CNPJ: String): Boolean;
    function  CalculaCNPJCPF(Nume: String) : String;
    function  PareceTelefoneBR(Telefone: String): Boolean;
    function  GetCodigoUF_da_SiglaUF(SiglaUF: String): Integer;
    function  GetSiglaUF_do_CodigoUF(CodigoUF: Integer): String;
    function  VersaoTxt2006(Versao: Integer): String;
    function  SoNumeroEVirgula_TT(Texto: String) : String;

    // D A T A  /  H O R A
    function  EhDataBR(): Boolean;
    function  ValidaDataBR(Texto: String; PermiteZero: Boolean;
              ForceNextYear: Boolean): TDateTime;
    function  StringToTime(const S: string): TDateTime;

    // N U M E R O S
    function  DMV(Texto: String): Double;
    function  IntInArray(Inteiro: Integer; ArrayInteiros: array of Integer): Boolean;
    function  FFT(Numero: Double; Casas: Integer; Sinal: TSinal): String;
    function  FFT_Dot(Numero: Double; Casas: Integer; Sinal: TSinal) : String;
    function  FI64(Numero: Int64): String;
    function  FFI(Numero: Double): String;
    function  GetNumIntsFmt(Digitos: Integer): String;
    function  I64(Texto: String): Int64;
    function  LimpaNumeroTexto(Texto: String): String;
    function  ToFormStr(AProp: string; AVal: Single): string;
    function  ToFormStrB(AProp: string; AVal: Boolean): string;
    function  ToFormStrS(AProp, AVal: string): string;
    function  TrimAllNoDouble(Texto: String): String;
    function  TrimAllNoInteger(Texto: String): String;

    // M E N S A G E N S
    function  MB_Erro(Texto: String): TModalResult;
    function  MB_Aviso(Texto: String): TModalResult;
    function  MB_Info(Texto: String): TModalResult;
    function  MB_Pergunta(Texto: String): TModalResult;
    function  MB_SQL(Form: TForm; Qry: TDataSet): Integer;
    function  FIC(FaltaInfoCompo: Boolean; Mensagem: String;
              ExibeMsg: Boolean = True): Boolean;

    // M O D U L O S
    function  Modulo11_2a9_Back(Numero: String; Tipo: Integer): String;

    // S T R I N G S
    function  NomeTipoSQL(Tipo: TSQLType): String;
    function  Substitui(Txt, ant, new: string) : string;
    function  VariantToString(AVar: OleVariant): string;
    function  VariavelToStringAS(Variavel: Variant): String;
    function  VariavelToStringAD(Variavel: Variant): String;
    function  WideStringToSQLString(Texto: (*Wide*)String): (*Wide*)String;
    function  ATS(ListaSQL: array of string): String;
    function  ATS_if(Condicao: Boolean; ListaSQL: array of string): String;
    function  Explode(Str, Separador: String; TamMin: Integer): TStringList;

    // O S   /   P L A T F O R M
    function  DirAppAllOS(Arquivo: String): String;
    procedure ProcessaMsgs();
  end;


var
  FMX_Geral: TUnFMX_Geral;
  APP_VERSAO: String;

implementation

{ TUnDmkGeral }

uses UnFMX_ValidaIE, UnFMX_DmkForms, DmkDialog;

const
  cND = 'N�o definido';
  cForm = '  %s =' + sLineBreak + '%30s        %3.5f ' + sLineBreak;
  cFormS = '  %s =' + sLineBreak + '%30s        %s ' + sLineBreak;

function TUnFMX_Geral.ATS(ListaSQL: array of string): String;
var
  I: Integer;
begin
  Result := '';
  for I := Low(ListaSQL) to High(ListaSQL) do
    Result := Result + ListaSQL[I] + sLineBreak;
end;

function TUnFMX_Geral.ATS_if(Condicao: Boolean; ListaSQL: array of string): String;
var
  I: Integer;
begin
  Result := '';
  if Condicao then
  begin
    for I := Low(ListaSQL) to High(ListaSQL) do
      Result := Result + ListaSQL[I] + sLineBreak;
  end;
end;

function TUnFMX_Geral.BoolToInt(Verdade: Boolean): Integer;
begin
  if Verdade then Result := 1 else Result := 0;
end;

function TUnFMX_Geral.FIC(FaltaInfoCompo: Boolean; Mensagem: String;
  ExibeMsg: Boolean = True): Boolean;
begin
  if FaltaInfoCompo then
  begin
    Result := True;
    //
    if (Mensagem <> '') and ExibeMsg then
      MB_Aviso(Mensagem);
  end else
    Result := False;
end;

function TUnFMX_Geral.CalculaCNPJCPF(Nume: String): String;
var
  l : Integer;
  Numero, Res: String;
  i,j,k : Integer;
  Soma : Integer;
  Digito : Integer;
  CNPJ : Boolean;
begin
  if Nume <> '' then
  begin
  Numero := '';
  //for l := 1 to (Length(Nume)-2) do
  for l := 0 to (Length(Nume)-3) do
    if CharInSet(Nume[l], ['0'..'9']) then
      Numero := Numero + Chr(Ord(Nume[l]));
  case length(Numero) of
     9 : CNPJ := False;
    12 : CNPJ := True;
    13 : CNPJ := True;
    else Exit;
  end;
  Res := Numero;
  //Calcula 2 vezes
  for j := 1 to 2 do begin
    k := 2;
    Soma := 0;
    //for i := Length(Res) downto 1 do begin
    for i := Length(Res) - 1 downto 0 do begin
      //Converte o d�gito para num�rico, multiplica e soma
      Soma := Soma + (Ord(Res[i]) - Ord('0')) * k;
      Inc(k);
      if (k > 9) and CNPJ then
        k := 2;
    end;
    Digito := 11 - Soma mod 11;
    if Digito >= 10 then
      Digito := 0;
    //adiciona o d�gito ao Resado
      Res := Res + Chr(Digito + Ord('0'));
    end;
  end;
  Result := String(Res);
end;

function TUnFMX_Geral.CNPJ_Valida(const EdCNPJ: TCustomEdit;
  var CNPJ: String): Boolean;
var
  Num, CNPJ1, CNPJ2: String;
begin
  Result := False;
  CNPJ1 := SoNumero_TT(String(CNPJ));
  if CNPJ1 <> '' then
  begin
    Num := CalculaCNPJCPF(CNPJ1);
    if FormataCNPJ_TFT(CNPJ1) <> Num then
    begin
      ShowMessage('Valor inv�lido para CNPJ / CPF');
      if EdCNPJ <> nil then
      try
        if TCustomEdit(EdCNPJ).Enabled
        and TCustomEdit(EdCNPJ).Visible
        and (TCustomEdit(EdCNPJ).ReadOnly = False)
        then
          TCustomEdit(EdCNPJ).SetFocus;
      except
        ;//
      end;
    end else begin
      CNPJ2 := FormataCNPJ_TT(CNPJ1);
      CNPJ := String(CNPJ2);
      Result := True;
    end;
  end;
end;

procedure TUnFMX_Geral.DefineFormatacoes();
begin
  if FormatSettings.ShortDateFormat = 'M/d/yyyy' then
    VAR_FORMATDATEi := 'mm/dd/yy'
  else
    VAR_FORMATDATEi := 'dd/mm/yy';
  VAR_FORMATDATE0 := 'yy/mm/dd';
  VAR_FORMATDATE  := 'yyyy-mm-dd'; // mudei 2011-05-21
  VAR_FORMATDATE2 := 'dd/mm/yyyy';
  VAR_FORMATDATE3 := 'dd/mm/yy'; // para labels, n�o usar para c�lculo!
  VAR_FORMATDATE5 := 'mmm/yy'; // para labels, n�o usar para c�lculo!
  VAR_FORMATDATE4 := 'yyyymmdd';
  VAR_FORMATDATE6 := 'dd "de" mmmm "de" yyyy';
  VAR_FORMATDATE7 := 'mmmm "de" yyyy';
  VAR_FORMATDATE8 := 'dd "de" mmmm "de" yyyy "�s" hh:nn:ss';
  VAR_FORMATDATE9 := 'yyyy/mm/dd hh:nn:ss';
  VAR_FORMATDATE10 := 'mm/dd/YYYY';
  VAR_FORMATDATE11 := 'dd/mm/yyyy hh:nn';
  VAR_FORMATDATE12 := 'dd/mm';
  VAR_FORMATDATE13 := 'yymm'; // Mez
  VAR_FORMATDATE14 := 'mm/yyyy'; // Mes
  VAR_FORMATDATE15 := 'dddd, dd "de" mmmm "de" yyyy'; // Mes
  VAR_FORMATDATE16 := 'dd';
  VAR_FORMATDATE17 := 'mmmm/yy';
  VAR_FORMATDATE18 := 'mmm/yy';
  VAR_FORMATDATE19 := 'mm/yy';
  VAR_FORMATDATE20 := 'yyyymm';
  VAR_FORMATDATE21 := 'yy';
  VAR_FORMATDATE22 := 'mm';
  VAR_FORMATDATE23 := 'ddmmyyyy';
  VAR_FORMATDATE24 := 'mmyyyy';
  VAR_FORMATDATE25 := 'yyyy';
  VAR_FORMATDATE26 := 'yymmdd_hhnnss';

  VAR_FORMATTIMESTAMP4 := 'mm/yy';
  VAR_FORMATTIME := 'hh:nn:ss';
  VAR_FORMATTIME2 := 'hh:nn';
  VAR_FORMATTIME3 := 'hhnn';
  VAR_FORMATTIME4 := 'hh:nn:ss:zzz';
  VAR_FORMATTIME5 := 'hh"h "nn"min "ss "seg"';
  VAR_FORMATDATE15 := 'dddd, dd "de" mmmm "de" yyyy'; // Mes
  VAR_FORMATDATETIME   := 'dd/mm/yyyy hh:nn:ss';
  VAR_FORMATDATETIME1  := 'dd/mm/yy hh:nn:ss';
  VAR_FORMATDATETIME2  := 'yyyy/mm/dd hh:nn:ss';
  VAR_FORMATDATETIME3  := 'dd/mm/yy hh:nn';
  VAR_FORMATDATETIME4  := 'yyyy-mm-dd hh:nn:ss';
  VAR_FORMATDATECB4    := 'YYYYMMDDhh:nn:ss';
  //
  VAR_FORMATFLOAT0 := '#,###,###,##0';
  VAR_FORMATFLOAT1 := '#,###,###,##0.0';
  VAR_FORMATFLOAT2 := '#,###,###,##0.00';
  VAR_FORMATFLOAT3 := '#,###,###,##0.000';
  VAR_FORMATFLOAT4 := '#,###,###,##0.0000';
  VAR_FORMATFLOAT5 := '#,###,###,##0.00000';
  VAR_FORMATFLOAT6 := '#,###,###,##0.000000';
  VAR_FORMATFLOAT7 := '#,###,###,##0.0000000';
  VAR_FORMATFLOAT8 := '#,###,###,##0.00000000';
  VAR_FORMATFLOAT9 := '#,###,###,##0.000000000';
  VAR_FORMATFLOAT10 := '#,###,###,##0.0000000000';
  //
end;

function TUnFMX_Geral.DirAppAllOS(Arquivo: String): String;
begin
  {$IF DEFINED(IOS) or DEFINED(ANDROID)}
  Result :=   TPath.GetDocumentsPath + System.SysUtils.PathDelim  + Arquivo;
  {$ELSE}
  Result := 'C:' + System.SysUtils.PathDelim  + 'Dermatek' +
    System.SysUtils.PathDelim  + 'AllOS' + System.SysUtils.PathDelim  + Arquivo;
  {$ENDIF}
end;

function TUnFMX_Geral.DMV(Texto: String): Double;
var
  Numero : String;
begin
  Numero := LimpaNumeroTexto(Texto);
  if Numero = '' then Result := 0
  else
    Result := StrToFloat(Numero);
end;

function TUnFMX_Geral.EhDataBR: Boolean;
begin
  Result := Uppercase(FormatSettings.ShortDateFormat[1]) = 'D';
end;

function TUnFMX_Geral.Explode(Str, Separador: String; TamMin: Integer): TStringList;
var
  i: Integer;
begin
  Result := TStringList.Create;
  //
  i := Pos(Separador, Str);
  //
  while (i > 0) do
  begin
    if (((i - 1) > TamMin) and (TamMin <> 0)) or (TamMin = 0) then
      Result.Add(Copy(Str, 1, i-1));
    Delete(Str, 1, i + Length(Separador) - 1);
    //
    i := Pos(Separador, Str);
  end;
  if (Str <> '') then
    Result.Add(Str);
end;

function TUnFMX_Geral.FDT(Data: TDateTime; Tipo: Integer; Nulo: Boolean): String;
var
  Res: String;
begin
  Result := '';
  Res := '';
  if Nulo and (Data< (1/86400)) then Exit; // menos que um segundo
  //Res := FormatDateTime('yyyy/mm/yy hh:nn:ss:zzz', Data);
  case Tipo of
    00: Res := FormatDateTime(VAR_FORMATDATETIME, Data);
    01: Res := FormatDateTime(VAR_FORMATDATE, Data);
    02: Res := FormatDateTime(VAR_FORMATDATE2, Data);
    03: Res := FormatDateTime(VAR_FORMATDATE3, Data);
    04: Res := FormatDateTime(VAR_FORMATDATE4, Data);
    05: Res := FormatDateTime(VAR_FORMATDATE5, Data);
    06: Res := FormatDateTime(VAR_FORMATDATE6, Data);
    07: Res := FormatDateTime(VAR_FORMATDATE7, Data);
    08: Res := FormatDateTime(VAR_FORMATDATE8, Data);
    09: Res := FormatDateTime(VAR_FORMATDATE9, Data);
    10: Res := FormatDateTime(VAR_FORMATDATE10, Data);
    11: Res := FormatDateTime(VAR_FORMATDATE11, Data);
    12: Res := FormatDateTime(VAR_FORMATDATE12, Data);
    13: Res := FormatDateTime(VAR_FORMATDATE13, Data);
    14: Res := FormatDateTime(VAR_FORMATDATE14, Data);
    15: Res := FormatDateTime(VAR_FORMATDATE15, Data);
    16: Res := FormatDateTime(VAR_FORMATDATE16, Data);
    17: Res := FormatDateTime(VAR_FORMATDATE17, Data);
    18: Res := FormatDateTime(VAR_FORMATDATE18, Data);
    19: Res := FormatDateTime(VAR_FORMATDATE19, Data);
    20: Res := FormatDateTime(VAR_FORMATDATE20, Data);
    21: Res := FormatDateTime(VAR_FORMATDATE21, Data);
    22: Res := FormatDateTime(VAR_FORMATDATE22, Data);
    23: Res := FormatDateTime(VAR_FORMATDATE23, Data);
    24: Res := FormatDateTime(VAR_FORMATDATE24, Data);
    25: Res := FormatDateTime(VAR_FORMATDATE25, Data); // 'YYYY'
    26: Res := FormatDateTime(VAR_FORMATDATE26, Data); // 'yymmdd_hhnnss'
    //
   100: Res := FormatDateTime(VAR_FORMATTIME,   Data);
   102: Res := FormatDateTime(VAR_FORMATTIME2,  Data);
   103: Res := FormatDateTime(VAR_FORMATTIME3,  Data);
   104: Res := FormatDateTime(VAR_FORMATTIMESTAMP4, Data);
   105: Res := FormatDateTime(VAR_FORMATDATETIME2,  Data);
   106: Res := FormatDateTime(VAR_FORMATDATETIME1,  Data);
   107: Res := FormatDateTime(VAR_FORMATDATETIME3,  Data);
   108: Res := FormatDateTime(VAR_FORMATTIME4,  Data);
   109: Res := FormatDateTime(VAR_FORMATDATETIME4,  Data);
   110: Res := FormatDateTime(VAR_FORMATTIME5,  Data);
    else Res := '* ERRO Res *'
  end;
  if Data = 0 then
  begin
    // data no formato de sistema
    // N�O MEXER AQUI - estraga SQLs
    case Tipo of
      1: Res := '0000-00-00';
      109: Res := '0000-00-00 00:00:00';
      else Res := '';
    end;
  end;
  // 2012-02-18
  //if Data < 0 then
    //Res := '-' + Res;
  // fim 2012-02-18
  Result := Res;
end;

function TUnFMX_Geral.FF0(Numero: Integer): String;
begin
  Result := FormatFloat('0', Numero);
end;

function TUnFMX_Geral.FFI(Numero: Double): String;
begin
  Result := IntToStr(Round(Numero));   // Round = Int64  ::  Trunc = Integer
end;

function TUnFMX_Geral.FFN(Numero, Digitos: Integer): String;
var
  Fmt: String;
begin
  Fmt := GetNumIntsFmt(Digitos);
  if Fmt = '' then
    Result := ''
  else
    Result := FormatFloat(Fmt, Numero);
end;

function TUnFMX_Geral.FFT(Numero: Double; Casas: Integer; Sinal: TSinal): String;
var
  Fmt: String;
begin
  if Casas <> 0 then
    Fmt := '0.' + (*FormatSettings.DecimalSeparator +*) GetNumIntsFmt(Casas)
  else
    Fmt := '0';
  Result := FormatFloat(Fmt, Numero);
  if (Sinal = siPositivo) and (Result.Substring(0, 1) = '-') then
    Result := Result.Substring(1);
end;

function TUnFMX_Geral.FFT_Dot(Numero: Double; Casas: Integer;
  Sinal: TSinal): String;
var
  Texto: String;
  I: Integer;
begin
  Result := '';
  Texto  := TFT(FormatFloat('0.00000000000000000000', Numero), Casas, Sinal);
  //
  for I := Low(Texto) to High(Texto) do
  begin
    if Texto[I] <> '.' then
    begin
      if Texto[I] = ',' then
        Result := Result + '.'
      else
        Result := Result + Texto[I];
    end;
  end;
end;

function TUnFMX_Geral.FI64(Numero: Int64): String;
begin
  Result := IntToStr(Numero);
end;

function TUnFMX_Geral.FormataCBO2002(CBO2002: String): String;
var
  Txt: String;
begin
  Txt := SoNumero_TT(CBO2002);
  Txt := FormatFloat('000000', IMV(Txt));
  Insert('-', Txt, 5);
  Result := Txt;
end;

function TUnFMX_Geral.FormataCEP_TT(CEP, Separador, Default: String): String;
var
  Numeros: String;
begin
  if (CEP = '0') or (CEP = '') or (CEP = '00000-000')then Result := Default else
  begin
    Numeros := SoNumero_TT(CEP);
    if Length(Numeros) <= 5 then Numeros := Numeros + '000';
    while Length(Numeros) < 8 do Numeros := '0' + Numeros;
    Result := Copy(Numeros, 1, 5) + Separador + Copy(Numeros, 6, 3);
  end;
end;

function TUnFMX_Geral.FormataChaveNFe(ChaveNFe: String;
  Como: TComoFmtChNFe): String;
var
  chave, cDV, Separador: String;
  n: Integer;
begin
  Result := SoNumero_TT(Trim(ChaveNFe));
  if Result = '' then
    Exit;
  n := Length(Result);
  if n <> 44 then
  begin
    MB_Aviso('A chave da NF-e "' + ChaveNFe + '" � inv�lida!' + slineBreak +
      'A chave est� com ' + IntToStr(n) + ' caracteres quando deveria ter 44.');
    Exit;
  end;
  chave := Copy(Result, 01, 43);
  cDV   := Copy(Result, 44, 01);
  //
  if cDV <> Modulo11_2a9_Back(chave, 0) then
  begin
    MB_Aviso('A chave da NF-e "' + ChaveNFe + '" � inv�lida!' + slineBreak +
      'Seu d�gito verificador n�o confere!.');
    Exit;
  end;
  case Como of
    cfcnDANFE:
    begin
      Separador := ' ';
      Result :=
        Copy(Result, 01, 04) + Separador +
        Copy(Result, 05, 04) + Separador +
        Copy(Result, 09, 04) + Separador +
        Copy(Result, 13, 04) + Separador +
        Copy(Result, 17, 04) + Separador +
        Copy(Result, 21, 04) + Separador +
        Copy(Result, 25, 04) + Separador +
        Copy(Result, 29, 04) + Separador +
        Copy(Result, 33, 04) + Separador +
        Copy(Result, 37, 04) + Separador +
        Copy(Result, 41, 04);
    end;
    cfcnFrendly:
    //15-1203-04.333.952/0001-88-55-009-000.000.202-176.112.641-4
    begin
      Result :=
      Copy(Result, 01, 02) + '-' +
      Copy(Result, 03, 04) + '-' +
      Copy(Result, 07, 02) + '.' +
      Copy(Result, 09, 03) + '.' +
      Copy(Result, 11, 03) + '/' +
      Copy(Result, 14, 04) + '-' +
      Copy(Result, 18, 02) + '-' +
      Copy(Result, 20, 02) + '-' +
      Copy(Result, 22, 03) + '-' +
      Copy(Result, 25, 03) + '.' +
      Copy(Result, 28, 03) + '.' +
      Copy(Result, 31, 03) + '-' +
      Copy(Result, 34, 03) + '.' +
      Copy(Result, 37, 03) + '.' +
      Copy(Result, 41, 03) + '-' +
      Copy(Result, 44);
    end;
  end;

end;

function TUnFMX_Geral.FormataCNPJ_TFT(Texto: String): String;
var
  i: Integer;
begin
  Result := '';
  if Texto = '' then Exit;
  try
    for I := Low(Texto) to High(Texto) do
      if CharInSet(Texto[i], ['0'..'9']) then Result := Result + Texto[i];
  except
    MB_Erro('CNPJ / CPF inv�lido.' + sLineBreak + 'Geral.FormataCNPJ_TFT()');
  end;
end;

function TUnFMX_Geral.FormataCNPJ_TT(Texto: String): String;
  procedure MensagemErroCPFJ(ContNum: Integer);
  begin
    if ContNum < 14 then
      MB_Erro('CPF inv�lido: ' + String(Texto) + sLineBreak + 'Geral.FormataCNPJ_TT()')
    else
      MB_Erro('CNPJ inv�lido: ' + String(Texto) + sLineBreak + 'Geral.FormataCNPJ_TT()');
  end;
var
  Txt: String;
  //Num,
  Res: String;
  Tam: Integer;
begin
  Txt := SoNumero_TT(String(Texto));
  //
  if (Txt = '') and (String(Texto) <> Txt) then
  begin
    Res := String(Texto);
    Exit;
  end;
  Res := String(Txt);
  if Txt = '' then Exit;
  Tam := Length(Txt);
  try
    case Tam of
    11:
      {$IF DEFINED(iOS) or DEFINED(ANDROID)}
      Res := Txt[0]+Txt[1]+Txt[2]+'.'+
              Txt[3]+Txt[4]+Txt[5]+'.'+
              Txt[6]+Txt[7]+Txt[8]+'-'+
              Txt[9]+Txt[10];
      {$ELSE}
      Res := Txt[1]+Txt[2]+Txt[3]+'.'+
              Txt[4]+Txt[5]+Txt[6]+'.'+
              Txt[7]+Txt[8]+Txt[9]+'-'+
              Txt[10]+Txt[11];
      {$ENDIF}
    14:
      {$IF DEFINED(iOS) or DEFINED(ANDROID)}
      Res := Txt[0]+Txt[1]+'.'+
                Txt[2]+Txt[3]+Txt[4]+'.'+
                Txt[5]+Txt[6]+Txt[7]+'/'+
                Txt[8]+Txt[9]+Txt[10]+Txt[11]+'-'+
                Txt[12]+Txt[13];
      {$ELSE}
      Res := Txt[1]+Txt[2]+'.'+
                Txt[3]+Txt[4]+Txt[5]+'.'+
                Txt[6]+Txt[7]+Txt[8]+'/'+
                Txt[9]+Txt[10]+Txt[11]+Txt[12]+'-'+
                Txt[13]+Txt[14];
      {$ENDIF}
    15:
      {$IF DEFINED(iOS) or DEFINED(ANDROID)}
      Res := Txt[0]+Txt[1]+Txt[2]+'.'+
                  Txt[3]+Txt[4]+Txt[5]+'.'+
                  Txt[6]+Txt[7]+Txt[8]+'/'+
                  Txt[9]+Txt[10]+Txt[11]+Txt[12]+'-'+
                  Txt[13]+Txt[14];
      {$ELSE}
      Res := Txt[1]+Txt[2]+Txt[3]+'.'+
                  Txt[4]+Txt[5]+Txt[6]+'.'+
                  Txt[7]+Txt[8]+Txt[9]+'/'+
                  Txt[10]+Txt[11]+Txt[12]+Txt[13]+'-'+
                  Txt[14]+Txt[15];
      {$ENDIF}
      else
      begin
        MensagemErroCPFJ(Tam);
        Exit;
      end;
    end;
  except
    MensagemErroCPFJ(Tam);
  end;
  Result := Res;
end;

function TUnFMX_Geral.FormataNCM(NCM: String): String;
var
  Txt, Capitulo, Posicao, SubPosicao, Item, SubItem: String;
begin
  Txt := SoNumero_TT(NCM);
  Capitulo   := Copy(Txt, 1, 2);
  Posicao    := Copy(Txt, 3, 2);
  SubPosicao := Copy(Txt, 5, 2);
  Item       := Copy(Txt, 7, 1);
  SubItem    := Copy(Txt, 8, 1);
  //
  Txt := Capitulo + Posicao;
  if Subposicao <> '' then
  begin
    Txt := Txt + '.' + SubPosicao;
    if Item <> '' then
      Txt := Txt + '.' + Item + SubItem;
  end;
  Result := Txt;
end;

function TUnFMX_Geral.FormataNumeroDeRua(Rua: String; Numero: Variant;
  Nulo: Boolean): String;
var
  Num: String;
begin
// 2012/05/17 > Rua: String
  if Trim(String(Rua)) = '' then
    Result := ''
  else begin
// FIM 2012/05/17
    if VarType(Numero)  <> vtInteger then
      Num := SoNumero_TT(Numero)
    else
      Num := SoNumero_TT(IntToStr(Numero));
    if (Num = '0') or (Num = '') then
    begin
      if Nulo then Result := '' else Result := 'S/N';
    end else Result := Num;
  end;
end;

function TUnFMX_Geral.FormataTelefone_TT(Telefone: String): String;
var
  T: String;
  Tam, i: Integer;
  Res: String;
begin
  Res := String(Telefone);
  if PareceTelefoneBR(Res) then
  begin
    if Length(Telefone) > 0 then
    if Telefone[1] <> '+' then
    begin
      T := '';
      for I := Low(Telefone) to High(Telefone) do
        if CharInSet(Telefone[i], (['0'..'9'])) then T := T + String(Telefone[i]);
      Tam := Length(T);
      if (Tam = 13) then
        Res := T[1]+' xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      //
{     2012-08-31 telefone S�o Paulo
      else if (Tam = 12) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
}
      else if (Tam = 12) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      // Fim 2012-08-31
      else if (Tam = 12) and (T[1] <> '0')then
        Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      //
      else if (Tam = 11) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
{     2012-08-31 telefone S�o Paulo
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
}
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      // Fim 2012-08-31
      else if (Tam = 10) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 09) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      else if (Tam = 09) and (T[1] = '0')then
        Res := '0 xx (xx) '+T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 08) and (T[1] <> '0')then
        Res := '0 xx (xx) '+T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 08) and (T[1] = '0')then
        Res := '0 xx (xx) '+T[2]+T[3]+T[4]+'- '+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 07) and (T[1] <> '0')then
        Res := '0 xx (xx) '+T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7];
    end;
  end;
  Result := Res;
end;

function TUnFMX_Geral.FormataTelefone_TT_Curto(Telefone: String): String;
var
  T, Res: String;
  Tam, i: Integer;
begin
  Res := String(Telefone);
  if PareceTelefoneBR(Res) then
  begin
    if Length(Telefone) > 0 then
    if Telefone[1] <> '+' then
    begin
      T := '';
      for I := Low(Telefone) to High(Telefone) do
        //if Telefone[i] in (['0'..'9']) then T := T + Telefone[i];
        if CharInSet(Telefone[i], (['0'..'9'])) then T := T + String(Telefone[i]);
      Tam := Length(T);
      {$IF DEFINED(iOS) or DEFINED(ANDROID)}
      if (Tam = 13) then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      //
      else if (Tam = 12) and (T[0] = '0')then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 12) and (T[0] <> '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      //
      else if (Tam = 11) and (T[0] = '0') and (T[2] = '0') and (T[3] = '0') then
        Res := T[0]+T[1]+T[2]+T[3]+' '+T[4]+T[5]+T[6]+' '+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 11) and (T[0] = '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 11) and (T[0] <> '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 10) and (T[0] = '0') and (T[2] = '0') and (T[3] = '0') then
        Res := T[0]+T[1]+T[2]+T[3]+' '+T[4]+T[5]+T[6]+' '+T[7]+T[8]+T[9]
      else if (Tam = 10) and (T[0] = '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      else if (Tam = 10) and (T[0] <> '0')then
        Res := '('+T[0]+T[1]+') '+T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 09) and (T[0] <> '0')then
        Res := '('+T[0]+T[1]+') '+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 09) and (T[0] = '0')then
        Res := T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 08) and (T[0] <> '0')then
        Res := T[0]+T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7]
      else if (Tam = 08) and (T[0] = '0')then
        Res := T[1]+T[2]+T[3]+'- '+T[4]+T[5]+T[6]+T[7]
      //
      else if (Tam = 07) and (T[0] <> '0')then
        Res := T[0]+T[1]+T[2]+'-'+T[3]+T[4]+T[5]+T[6];
      {$ELSE}
      if (Tam = 13) then
        Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      //
      else if (Tam = 12) and (T[1] = '0')then
        Res := '('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      else if (Tam = 12) and (T[1] <> '0')then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      //
      else if (Tam = 11) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      //
      else if (Tam = 10) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] = '0')then
        Res := '('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] <> '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 09) and (T[1] <> '0')then
        Res := '('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      else if (Tam = 09) and (T[1] = '0')then
        Res := T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 08) and (T[1] <> '0')then
        Res := T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 08) and (T[1] = '0')then
        Res := T[2]+T[3]+T[4]+'- '+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 07) and (T[1] <> '0')then
        Res := T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7];
      {$ENDIF}
    end;
  end;
  Result := Res;
end;

function TUnFMX_Geral.FormataTelefone_TT_NFe(Telefone: String): String;
var
  T: String;
  Tam, i: Integer;
  Res: String;
begin
  Res := String(Telefone);
  if PareceTelefoneBR(Res) then
  begin
    if Length(Telefone) > 0 then
    if Telefone[1] <> '+' then
    begin
      T := '';
      for I := Low(Telefone) to High(Telefone) do
        //if Telefone[i] in (['0'..'9']) then T := T + Telefone[i];
        if CharInSet(Telefone[i], (['0'..'9'])) then T := T + String(Telefone[i]);
      Tam := Length(T);
      if (Tam = 13) then
        Res := T[1]+' xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+T[9]+'-'+T[10]+T[11]+T[12]+T[13]
      //
{     2012-08-31 telefone S�o Paulo
      else if (Tam = 12) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[4]+T[5]+') '+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
}
      else if (Tam = 12) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      // Fim 2012-08-31
      else if (Tam = 12) and (T[1] <> '0')then
        Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+T[8]+'-'+T[9]+T[10]+T[11]+T[12]
      //
      else if (Tam = 11) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]+T[11]
      else if (Tam = 11) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
{     2012-08-31 telefone S�o Paulo
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '0 xx ('+T[3]+T[4]+') '+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
}
      else if (Tam = 11) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+T[7]+'-'+T[8]+T[9]+T[10]+T[11]
      // Fim 2012-08-31
      else if (Tam = 10) and (T[1] = '0') and (T[3] = '0') and (T[4] = '0') then
        Res := T[1]+T[2]+T[3]+T[4]+' '+T[5]+T[6]+T[7]+' '+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] = '0')then
        Res := T[1]+' xx ('+T[2]+T[3]+') '+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      else if (Tam = 10) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+T[6]+'-'+T[7]+T[8]+T[9]+T[10]
      //
      else if (Tam = 09) and (T[1] <> '0')then
        Res := '0 xx ('+T[1]+T[2]+') '+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      else if (Tam = 09) and (T[1] = '0')then
        Res := '0 xx (xx) '+T[2]+T[3]+T[4]+T[5]+'-'+T[6]+T[7]+T[8]+T[9]
      //
      else if (Tam = 08) and (T[1] <> '0')then
        Res := '0 xx (xx) '+T[1]+T[2]+T[3]+T[4]+'-'+T[5]+T[6]+T[7]+T[8]
      else if (Tam = 08) and (T[1] = '0')then
        Res := '0 xx (xx) '+T[2]+T[3]+T[4]+'- '+T[5]+T[6]+T[7]+T[8]
      //
      else if (Tam = 07) and (T[1] <> '0')then
        Res := '0 xx (xx) '+T[1]+T[2]+T[3]+'-'+T[4]+T[5]+T[6]+T[7];
    end;
  end;
  Result := Res;
end;

function TUnFMX_Geral.FormataUF(UF: Variant): String;
var
  Estado: Integer;
begin
  if VarType(UF) <> vtInteger then
    Estado := GetCodigoUF_da_SiglaUF(UF)
  else
    Estado := UF;
  //
  Result := GetSiglaUF_do_CodigoUF(Estado);
end;

function TUnFMX_Geral.FormataVersao(Versao: Variant): String;
begin
  case VarType(Versao) of
    vtInt64, vtInteger:
      Result := VersaoTxt2006(Versao);
    vtString:
      Result := VersaoTxt2006(IMV(Versao));
    else
      MB_Erro('"VarType" desconhecido na fun��o ' + sLineBreak +
        '"Geral.FormataVersao"');
  end;
end;

function TUnFMX_Geral.Formata_IE(IE: String; UF: Variant; LinkMsk: String;
  Tipo: Integer): String;
const
  Isento = 'ISENTO';
var
  Mascara: String;
  Estado: Integer;
begin
  // n�o � IE, � RG!
  if Tipo = 1 then
  begin
    Result := IE;
    Exit;
  end;
  //
  if VarType(UF)  <> vtInteger then
    Estado := GetCodigoUF_da_SiglaUF(UF)
  else
    Estado := UF;
  if Trim(IE) <> '' then
  begin
    if Uppercase(IE) = Isento then
    begin
      Result := Isento;
    end else
    begin
      if FMX_ValidaIE.Valida_IE(IE, Estado, LinkMsk) then
      begin
        case Estado of
          (*TO*)-27:
          begin
            if Length(IE) = 9 then
              Mascara := '00.000.000-0;0;*'
            else
              Mascara := '00.00.000000-0;0;*';
          end;
          (*SP*)-26:
          begin
            if Length(IE) > 0 then
            if Uppercase(IE[1]) = 'P' then
              Mascara := 'L-000000000.0/000;0;*'
            else Mascara := '000.000.000.000;0;*';
          end;
          (*SE*)-25: Mascara := '00000000-0;0;*';
          (*SC*)-24: Mascara := '000.000.000;0;*';
          (*RS*)-23: Mascara := '000/0000000;0;*';
          (*RR*)-22: Mascara := '00000000-0;0;*';
          (*RO*)-21: Mascara := '000000000;0;*';
          (*RN*)-20: Mascara := '00.000.000-0;0;*';
          (*RJ*)-19: Mascara := '00.000.00-0;0;*';
          (*PR*)-18: Mascara := '000.00000-00;0;*';
          (*PI*)-17: Mascara := '00000000-0;0;*';
          (*PE*)-16:
          begin
            if Length(IE) = 14 then
              Mascara := '00.0.000.0000000-0;0;*'
            else
              Mascara := '0000000-00;0;*';
          end;
          (*PB*)-15: Mascara := '00.000.000-0;0;*';
          (*PA*)-14: Mascara := '00000000-0;0;*';
          (*MT*)-13: Mascara := '0000000000-0;0;*';
          (*MS*)-12: Mascara := '00.000.000-0;0;*';
          (*MG*)-11: Mascara := '000.000.000/0000;0;*';
          (*MA*)-10: Mascara := '000000000;0;*';
          (*GO*)-09: Mascara := '00.000.000-0;0;*';
          (*ES*)-08: Mascara := '00000000-0;0;*';
          (*DF*)-07: Mascara := '000.00000.000-00;0;*';
          (*CE*)-06: Mascara := '00000000-0;0;*';
          (*BA*)-05: Mascara := '000000-00;0;*';
          (*AP*)-04: Mascara := '000000000;0;*';
          (*AM*)-03: Mascara := '00.000.000-0;0;*';
          (*AL*)-02: Mascara := '000000000;0;*';
          (*AC*)-01: Mascara := '00.00.0000-0;0;*';
          else Mascara := '';
        end;
        //Numero := StrToFloat(SoNumero_TT(IE));
        if Mascara = '' then Result := IE else
        Result := FormatMaskText(Mascara, SoNumero_TT(IE));
      end;
    end;// else Result := IE;
  end else Result := IE;
end;

function TUnFMX_Geral.GetCodigoUF_da_SiglaUF(SiglaUF: String): Integer;
var
  Codigo: Integer;
begin
  if SoNumeroESinal_TT(SiglaUF) = SiglaUF then
    Codigo := IMV(SiglaUF)
  else
    Codigo := 0;
  case Codigo of
    -99,-27..-1: Result := Codigo;
    0:
    begin
      if SiglaUF = 'EX' then Result := -99 else
      if SiglaUF = 'AN' then Result := -91 else
      if SiglaUF = 'TO' then Result := -27 else
      if SiglaUF = 'SP' then Result := -26 else
      if SiglaUF = 'SE' then Result := -25 else
      if SiglaUF = 'SC' then Result := -24 else
      if SiglaUF = 'RS' then Result := -23 else
      if SiglaUF = 'RR' then Result := -22 else
      if SiglaUF = 'RO' then Result := -21 else
      if SiglaUF = 'RN' then Result := -20 else
      if SiglaUF = 'RJ' then Result := -19 else
      if SiglaUF = 'PR' then Result := -18 else
      if SiglaUF = 'PI' then Result := -17 else
      if SiglaUF = 'PE' then Result := -16 else
      if SiglaUF = 'PB' then Result := -15 else
      if SiglaUF = 'PA' then Result := -14 else
      if SiglaUF = 'MT' then Result := -13 else
      if SiglaUF = 'MS' then Result := -12 else
      if SiglaUF = 'MG' then Result := -11 else
      if SiglaUF = 'MA' then Result := -10 else
      if SiglaUF = 'GO' then Result := -09 else
      if SiglaUF = 'ES' then Result := -08 else
      if SiglaUF = 'DF' then Result := -07 else
      if SiglaUF = 'CE' then Result := -06 else
      if SiglaUF = 'BA' then Result := -05 else
      if SiglaUF = 'AP' then Result := -04 else
      if SiglaUF = 'AM' then Result := -03 else
      if SiglaUF = 'AL' then Result := -02 else
      if SiglaUF = 'AC' then Result := -01 else
      Result := 0;
    end;
    1..27,99: Result := - Codigo;
    else Result := 0;
  end;
end;

function TUnFMX_Geral.GetNumIntsFmt(Digitos: Integer): String;
var
  I: Integer;
  Zeros: String;
begin
  case Digitos of
    00: Result := '';
    01: Result := '0';
    02: Result := '00';
    03: Result := '000';
    04: Result := '0000';
    05: Result := '00000';
    06: Result := '000000';
    07: Result := '0000000';
    08: Result := '00000000';
    09: Result := '000000000';
    10: Result := '0000000000';
    11: Result := '00000000000';
    12: Result := '000000000000';
    13: Result := '0000000000000';
    14: Result := '00000000000000';
    15: Result := '000000000000000';
    16: Result := '0000000000000000';
    17: Result := '00000000000000000';
    18: Result := '000000000000000000';
    19: Result := '0000000000000000000';
    20: Result := '00000000000000000000';
    else
    begin
      Zeros := '00000000000000000000';
      for I := 21 to Digitos do
        Zeros := Zeros + '0';
      Result := Zeros;
    end;
  end;
end;

function TUnFMX_Geral.GetSiglaUF_do_CodigoUF(CodigoUF: Integer): String;
begin
  case CodigoUF of
   -100: Result := 'SVAN';
   -143: Result := 'SVRS';
    -91: Result := 'AN';
    -99: Result := 'EX';
    -27: Result := 'TO';
    -26: Result := 'SP';
    -25: Result := 'SE';
    -24: Result := 'SC';
    -23: Result := 'RS';
    -22: Result := 'RR';
    -21: Result := 'RO';
    -20: Result := 'RN';
    -19: Result := 'RJ';
    -18: Result := 'PR';
    -17: Result := 'PI';
    -16: Result := 'PE';
    -15: Result := 'PB';
    -14: Result := 'PA';
    -13: Result := 'MT';
    -12: Result := 'MS';
    -11: Result := 'MG';
    -10: Result := 'MA';
    -09: Result := 'GO';
    -08: Result := 'ES';
    -07: Result := 'DF';
    -06: Result := 'CE';
    -05: Result := 'BA';
    -04: Result := 'AP';
    -03: Result := 'AM';
    -02: Result := 'AL';
    -01: Result := 'AC';

     00: Result := '';

     01: Result := 'AC';
     02: Result := 'AL';
     03: Result := 'AM';
     04: Result := 'AP';
     05: Result := 'BA';
     06: Result := 'CE';
     07: Result := 'DF';
     08: Result := 'ES';
     09: Result := 'GO';
     10: Result := 'MA';
     11: Result := 'MG';
     12: Result := 'MS';
     13: Result := 'MT';
     14: Result := 'PA';
     15: Result := 'PB';
     16: Result := 'PE';
     17: Result := 'PI';
     18: Result := 'PR';
     19: Result := 'RJ';
     20: Result := 'RN';
     21: Result := 'RO';
     22: Result := 'RR';
     23: Result := 'RS';
     24: Result := 'SC';
     25: Result := 'SE';
     26: Result := 'SP';
     27: Result := 'TO';
     91: Result := 'AN';
     99: Result := 'EX';
    100: Result := 'SVAN';
    143: Result := 'SVRS';
    else Result := IntToStr(CodigoUF);
  end;
end;

function TUnFMX_Geral.I64(Texto: String): Int64;
var
  Num: String;
  i: Integer;
begin
  try
    if Trim(Texto) = '' then Texto := '0';
    Num := '';
    for I := Low(Texto) to High(Texto) do
    begin
      if (Texto[i] = '-') and (i = 1) then Num := '-';
      if CharInSet(Texto[i], ['0'..'9']) then Num := Num + Texto[i];
    end;
    if (Texto = '') or (Texto = '-') then
      Result := StrToInt64('0')
    else
      Result := StrToInt64(Num);
  except
    raise;
  end;
end;

function TUnFMX_Geral.IMV(Texto: String): Integer;
var
  Num: String;
  K, I: Integer;
begin
  try
    if Trim(Texto) = '' then
      Texto := '0';
    if Texto[Low(Texto)] = '-' then
    begin
      Num := '-';
      K := Low(Texto) + 1;
    end else
    begin
      Num := '';
      K := Low(Texto);
    end;
    for I := K to High(Texto) do
    begin
      if CO_SEQ_Numeros.IndexOf(Texto[I]) > -1 then
        Num := Num + Texto[I]
      else
        MB_Erro('"' + Texto[I] + '" n�o � um n�mero!');
    end;
    if (Texto = '') or (Texto = '-') then
      Result := 0
    else
      Result := StrToInt(Num);
  except
    raise;
    Result := 0;
  end;
end;

function TUnFMX_Geral.IntInArray(Inteiro: Integer;
  ArrayInteiros: array of Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := Low(ArrayInteiros) to High(ArrayInteiros) do
  begin
    if Inteiro = ArrayInteiros[I] then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TUnFMX_Geral.IntToBool(Verdade: Integer): Boolean;
begin
  if Verdade = 0 then Result := False else Result := True;
end;

function TUnFMX_Geral.LimpaNumeroTexto(Texto: String): String;
var
  i: Integer;
  c: Boolean;
  Numero: String;
begin
  c := False;
  Numero := '';
  for i := High(Texto) downto Low(Texto) do
  begin
    //if CharInSet(Texto[i], ['0'..'9']) then
    if CO_SEQ_Numeros.IndexOf(Texto[I]) > - 1 then
      Numero := Texto[i] + Numero
    else if Texto[i] = FormatSettings.ThousandSeparator then
    begin
      // apenas limpa
    end else if Texto[I] = FormatSettings.DecimalSeparator then
    begin
      if (i < Length(Texto)) and (c=False) then
      begin
        Numero := FormatSettings.DecimalSeparator + Numero;
        c := True;
      end;
    end else if (I = System.Low(Texto)) and (Texto[I] = '-') then
    begin
      if Length(Numero) > 0 then
        //if CharInSet(Numero[i], ['0'..'9']) then
        if CO_SEQ_Numeros.IndexOf(Numero[I]) > - 1 then
          Numero := '-' + Numero;
    end else begin
      Numero := '0';
      Break;
    end;
  end;
  Result := Numero;
end;

function TUnFMX_Geral.MB_Aviso(Texto: String): TModalResult;
begin
  ShowMessage(Texto);
  //
  Result := mrOk;
  //Dialog n�o funciona no android
  //MessageDlg(sPergunta, TMsgDlgType.mtWarning, [TMsgDlgBtn.MbOk], 0);
end;

function TUnFMX_Geral.MB_Erro(Texto: String): TModalResult;
begin
  ShowMessage(Texto);
  //
  Result := mrOk;
  //Dialog n�o funciona no android
  //MessageDlg(sPergunta, TMsgDlgType.mtWarning, [TMsgDlgBtn.MbOk], 0);
end;

function TUnFMX_Geral.MB_Info(Texto: String): TModalResult;
begin
{///////////////////////////////////////////////////////////////////////////////
  ShowMessage(Texto);
  //
  Result := mrOk;
  //Dialog n�o funciona no android
  //MessageDlg(sPergunta, TMsgDlgType.mtInformation, [TMsgDlgBtn.MbOk], 0);
///////////////////////////////////////////////////////////////////////////////}
  {$If DEFINED(iOS) or DEFINED(ANDROID)}
    ShowMessage(Texto);
  {$Else}
    if FMX_DmkForms.CriaFm_AllOS0(TFmDmkDialog, FmDmkDialog, fcmOnlyCreate, True, False) then
    begin
      FmDmkDialog.MeTexto.Text := Texto;
      Application.ProcessMessages;
      FmDmkDialog.BtOK.Visible := True;
      //
      FMX_DmkForms.ShowModal(FmDmkDialog);
    end;
  {$EndIf}
end;

function TUnFMX_Geral.MB_Pergunta(Texto: String): TModalResult;
(*
var
  MR: TModalResult;
*)
begin
  (*
  MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0,
    procedure(const AResult: System.UITypes.TModalResult)
    begin
      //MR := AResult;
      case AResult of
        mrYes: ?;
        mrNo: Grl_Geral.MB_Erro('Defin��o n�o realizada!');
        mrCancel: Grl_Geral.MB_Erro('Defin��o Abortada!');
      end;
    end);
  while MR = mrNone do
    Application.ProcessMessages;
  Result := MR;
  *)
  (*
  Atualizado em: 27/09/2019
  Usar apenas em casos bem espec�ficos pois no Android o c�digo � executado de tr�s para frente

  Result := MessageDlg(sPergunta, System.UITypes.TMsgDlgType.mtConfirmation,
    [
      System.UITypes.TMsgDlgBtn.mbYes,
      System.UITypes.TMsgDlgBtn.mbNo,
      System.UITypes.TMsgDlgBtn.mbCancel
    ], 0, System.UITypes.TMsgDlgBtn.mbYes);
  *)
  MB_Aviso('TUnFMX_Geral.MB_Pergunta => Desativado!');
end;

function TUnFMX_Geral.MB_SQL(Form: TForm; Qry: TDataSet): Integer;
{
  procedure GetPropertyValues(AObj: TObject; AValues: TStrings);
  var
    count:    integer;
    data:     PTypeData;
    default:  string;
    i:        integer;
    info:     PTypeInfo;
    propList: PPropList;
    propInfo: PPropInfo;
    propName: string;
    value:    variant;
    //
  begin
    info := AObj.ClassInfo;
    data := GetTypeData(info);
    GetMem(propList, data^.PropCount * SizeOf(PPropInfo));
    try
      count := GetPropList(info, tkAny,  propList);
      for i := 0 to count - 1 do
      begin
        propName := propList^[i]^.Name;
        propInfo := GetPropInfo(info, propName);
        if propInfo <> nil then begin
          case propInfo^.PropType^.Kind of
            tkClass,
            tkMethod:      value := '$' + IntToHex(GetOrdProp(AObj, propInfo), 8);
            tkFloat:       value := GetFloatProp(AObj, propInfo);
            tkInteger:     value := GetOrdProp(AObj, propInfo);
            tkString,
            tkLString,
            tkWString:     value := GetStrProp(AObj, propInfo);
            tkEnumeration: value := GetEnumProp(AObj, propInfo);
            else           value := '???';
          end;
          if propInfo.default = longint($80000000) then
            default := 'none'
          else
            default := IntToStr(propInfo.default);
          AValues.Add(Format('%s: %s [default: %s]',
                             [propName, value, default]));
          // $80000000 apparently indicates "no default".
        end;
      end;
    finally
      FreeMem(propList, data^.PropCount * SizeOf(PPropInfo));
    end;
  end;
  function ObtemSQLText(Objeto: TObject): String;
  var
    ListaProp: TStringList;
  begin
    ListaProp := TStringList.Create;
    try
      GetPropertyValues(Objeto, ListaProp);
      FmX_Geral.MB_Info(ListaProp.Text);
    finally
      ListaProp.Free;
    end;

  end;
}
  function GetQueryText(DataSet: TDataSet): String;
  var
    PropInfo: PPropInfo;
    SQLObj: TStrings;
  begin
    Result := '';
    try
      PropInfo := GetPropInfo(DataSet, 'SQL', [tkClass]);
      if not Assigned(PropInfo) then Exit;
      SQLObj := TStrings(GetObjectProp(DataSet, PropInfo, TStrings));
      if not Assigned(SQLObj) then Exit;
      (*
      DataSet.Close;
      SQLObj.Text := SQL;
      DataSet.Open;
      Result := True;
      *)
      Result := SQLObj.Text;
    except
    //
    end;
  end;
var
  FSQL: Variant;
  //sSQL: TStrings;
  Texto: String;
begin
  if FMX_Ref.GET_Compo_Val(Form, Qry.Name, 'SQL', TDataSet, FSQL) then
  begin
    //Texto := ObtemSQLText(TObject(FSQL));
    //FMX_Geral.MB_Info(Texto);
    //
    //sSQL := TStrings(FSQL);
    Texto := GetQueryText(Qry);
    //
    Result := MB_Info('Texto SQL' + sLineBreak + Form.Caption + sLineBreak +
    Form.Name + '.' + Qry.Name + sLineBreak + sLineBreak + Texto);
  end;
end;

function TUnFMX_Geral.Modulo11_2a9_Back(Numero: String; Tipo: Integer): String;
var
  nMul, i, Soma: Integer;
begin
  nMul := 1;
  Soma := 0;
  for i := Length(Numero) downto 1 do
  begin
    nMul := nMul + 1;
    if nMul > 9 then nMul := 2;
    Soma := Soma + (nMul * StrToInt(Numero[i]));
  end;
  Soma := Soma mod 11;
  //
  Soma := 11 - Soma;
  //
  case Tipo of
    // CampoLivre
    0: if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
    // 1: Banco do Brasil
    1: if Soma > 9 then Result := 'X' else Result := IntToStr(Soma);
    2: if (Soma > 9) or (Soma < 1) then Result := '1' else Result := IntToStr(Soma);
    else if Soma > 9 then Result := '0' else Result := IntToStr(Soma);
    //
  end;
  if (Tipo = 1) and (Result = '0') then Result := '1';
end;

function TUnFMX_Geral.MTD(Data: String; Obrigatorio: Boolean): TDateTime;
var
  Fat, i, Separa, Tam: Integer;
  M, A, X0, X1, X2: String;
begin
  Result := 0;
  try
    Tam := Length(Data);
    if (Tam = 0) then
    begin
      if Obrigatorio then
        MB_Erro('Data obrigat�ria!');
      Exit;
    end;
    if (StrRScan(PChar(Data),'/') = Nil) then
    begin
      if Uppercase(FormatSettings.ShortDateFormat[1]) = 'M' then Fat := -1 else Fat := 1;
      case (Tam * Fat) of
        -8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
        -6:
        begin
          if IMV(Data[1]+Data[2])> 12 then
            Data := Data[3]+Data[4]+'/'+Data[1]+Data[2]+'/'+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        -5: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3]+Data[4]+Data[5];
        -4: Data := Data[1]+Data[2]+'/01/'+Data[3]+Data[4];
        -3: Data := '0'+Data[1]+'/01/'+Data[2]+Data[3];
        ////////////////////////////////////////////////////////////////////////
        3: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3];
        4: Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4];
        5: Data := '01/0'+Data[1]+'/'+Data[2]+Data[3]+Data[4]+Data[5];
        6:
        begin
          if IMV(Data[3]+Data[4])> 12 then
            Data := '01/'+Data[1]+Data[2]+'/'+Data[3]+Data[4]+Data[5]+Data[6]
          else
            Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6];
        end;
        8: Data := Data[1]+Data[2]+'/'+Data[3]+Data[4]+'/'+Data[5]+Data[6]+Data[7]+Data[8];
      end;
    end else begin
      Separa := 0;
      for I := Low(Tam) to High(Tam) do
      begin
        if Data[i] = '/' then Separa := Separa + 1;
        //if Data[i] in ['0'..'9'] then
        if CharInSet(Data[i], ['0'..'9']) then
        begin
          case Separa of
            0: X0 := X0 + Data[i];
            1: X1 := X1 + Data[i];
            2: X2 := X2 + Data[i];
          end;
        end;
      end;
      if Separa = 2 then
      begin
        M := X1;
        A := X2;
      end else begin
        M := X0;
        A := X1;
      end;
      if Length(M) = 1 then M := '0' + M;
      if Uppercase(FormatSettings.ShortDateFormat[1]) = 'M' then Data := M+'/01/'+A else
         Data := '01/'+M+'/'+A;
    end;
    Result := StrToDate(Data);
  except
    on EConvertError do
    begin
      Result := Null;
    end;
  end;
end;

function TUnFMX_Geral.NomeTipoSQL(Tipo: TSQLType): String;
begin
  case Tipo of
    stCpy: Result := 'C�pia';
    stDel: Result := 'Exclus�o';
    stIns: Result := 'Inclus�o';
    stLok: Result := 'Travado';
    stUpd: Result := 'Altera��o';
    stUnd: Result := 'Desiste';
    stPsq: Result := 'Pesquisa';
    else   Result := '???????';
  end;
end;

function TUnFMX_Geral.PareceTelefoneBR(Telefone: String): Boolean;
var
  I: Integer;
  TxtA, TxtB: String;
begin
  TxtA := Trim(Telefone);
  TxtB := '';
  //
  for I := Low(TxtA) to High(TxtA) do
  begin
    if not (CharInSet(TxtA[i], (['0'..'9',')','(','-',' ','x','X']))) then
    TxtB := TxtB + TxtA[I];
  end;
  Result := Length(TxtB) = 0;
end;

procedure TUnFMX_Geral.ProcessaMsgs();
{$IFDEF MSWINDOWS}
var
  Msg: TMsg;
  Pegou: Boolean;
{$ENDIF}
begin
{$IFDEF MSWINDOWS}
  Pegou := False;
  while not Pegou do
  //while not (AllTasksFinished) do
  begin
    Sleep(1);
    if GetMessage(Msg, 0, 0, 0) then
    begin
      TranslateMessage(Msg);
      DispatchMessage(Msg);
      //
      Pegou := True;
    end;
  end;
{$ENDIF}
  Application.ProcessMessages;
end;

function TUnFMX_Geral.SoNumeroELetra_TT(Texto: String): String;
var
  i:Integer;
begin
  Result := '';
  for I := Low(Texto) to High(Texto) do
  begin
    if CharInSet(Texto[i], (['0'..'9'])) then Result := Result + Texto[i];
    if CharInSet(Texto[i], (['A'..'Z'])) then Result := Result + Texto[i];
    if CharInSet(Texto[i], (['a'..'z'])) then Result := Result + Texto[i];
  end;
end;

function TUnFMX_Geral.SoNumeroESinalEVirgula_TT(Texto: String): String;
var
  I: Integer;
begin
  Result := '';
  if Length(Texto) > 0 then
  begin
    for I := Low(Texto) to High(Texto) do
      if CharInSet(Texto[i], (['0'..'9', ',', '-'])) then Result := Result + Texto[i];
  end;
end;

function TUnFMX_Geral.SoNumeroESinal_TT(Texto: String): String;
var
  i:Integer;
begin
  if Trim(Texto) = '' then Result := '' else
  begin
    if Texto[1] = '-' then
      Result := '-'
    else
      Result := '';
    for I := Low(Texto) to High(Texto) do
      if CharInSet(Texto[i], ['0'..'9']) then Result := Result + Texto[i];
  end;
end;

function TUnFMX_Geral.SoNumeroEVirgula_TT(Texto: String): String;
var
  I: Integer;
begin
  Result := '';
  //
  if Length(Texto) > 0 then
  begin
    for I := Low(Texto) to High(Texto) do
      if CharInSet(Texto[i], (['0'..'9', ','])) then Result := Result + Texto[i];
  end;
end;

function TUnFMX_Geral.SoNumero_TT(Texto: String): String;
var
  I: Integer;
begin
  Result := '';
  //
  if Length(Texto) > 0 then
  begin
    for I := Low(Texto) to High(Texto) do
      if CharInSet(Texto[I], (['0'..'9'])) then Result := Result + Texto[I];
  end;
end;

function TUnFMX_Geral.StringToTime(const S: string): TDateTime;
begin
  if not TryStrToTime(S, Result) then
    MB_Erro('Data inv�lida: ' + S);
end;

function TUnFMX_Geral.Substitui(Txt, ant, new: string): string;
(*
var
  TamT, TamA, TamN, PosA: Integer;
*)
begin
  Result := Txt.Replace(ant, new);
{
  if ant = new then
  begin
    Result := Txt;
    Exit;
  end;
  TamT := Txt.Length;
  TamA := ant.Length;
  PosA := Txt.IndexOf(ant);
  while PosA > -1 do
  begin
    if PosA = 0 then
      Txt := new + Txt.Substring(TamA + 1, TamT - TamA)
    else if PosA +  TamA -1 = TamT then
      Txt := Txt.Substring(0, PosA - 1) + new
    else
      Txt := Txt.Substring(0, PosA - 1) + new + Txt.Substring(PosA + TamA);
    PosA := Txt.IndexOf(ant);
  end;
  Result := Txt;
}
end;

function TUnFMX_Geral.TFD(Texto: String; Digitos: Integer; Sinal: TSinal): String;
var
  Val, FmtTxt, FmtNul: String;
  i: integer;
begin
  Val := SoNumeroESinal_TT(TFT(Texto, 0, Sinal));
  //
  for I := Low(Digitos) to High(Digitos) do
    FmtTxT := FmtTxT + '0';
  //
  if Texto = '' then
    FmtNul := ' '
  else
    FmtNul := FmtTxt;
  //
  FmtTxT := FmtTxT +';-'+FmtTxT+';' + FmtNul;
  Result := Trim(FormatFloat(FmtTxt, StrToFloat(Val)));
end;

function TUnFMX_Geral.TFT(Texto: String; Casas: Integer; Sinal: TSinal): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Double;
begin
  TxtForma := '#,###,##0';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    //
    //for I := Low(Casas) to High(Casas) do
    for I := 0 to Casas - 1 do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  Resultado := FormatFloat(TxtForma, Flutuante);
  if (Sinal = siPositivo) and (Resultado[1] = '-') then
  begin
    Resultado := Copy(Resultado, 2, Length(Resultado)-1);
    if Resultado = '' then Resultado := '0';
  end;

  Result := Resultado;
end;

function TUnFMX_Geral.TFT_Max(Texto: String; Max: Double; Casas: Integer;
  Sinal: Boolean): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Double;
begin
  TxtForma := '#,###,##0';
  Numero := '';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    //
    for I := Low(Casas) to High(Casas) do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  if Flutuante > Max then Flutuante := Max;
  Resultado := FormatFloat(TxtForma, Flutuante);
  Result := Resultado;
end;

function TUnFMX_Geral.TFT_Min(Texto: String; Min: Double; Casas: Integer;
  Sinal: Boolean): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Double;
begin
  TxtForma := '#,###,##0';
  Numero := '';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    //
    for I := Low(Casas) to High(Casas) do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  if Flutuante < Min then Flutuante := Min;
  Resultado := FormatFloat(TxtForma, Flutuante);
  Result := Resultado;
end;

function TUnFMX_Geral.TFT_MinMax(Texto: String; Min, Max: Extended; Casas: Integer;
  Sinal: Boolean): String;
var
  Numero, TxtForma, Resultado : String;
  i: Integer;
  Flutuante : Extended;
begin
  if Texto = '' then
  begin
    Result := '';
    Exit;
  end;
  if Min > Max then
  begin
    Exception.Create(PChar('M�nimo maior que m�ximo na function ' +
    'TFT_MinMax'));
    Result := Texto;
    Exit;
  end;
  TxtForma := '#,###,##0';
  Numero := '';
  Numero := LimpaNumeroTexto(Texto);
  if Casas > 0 then
  begin
    TxtForma := TxtForma + '.';
    //
    for I := Low(Casas) to High(Casas) do
    begin
      TxtForma := TxtForma + '0';
    end;
  end;
  if Numero = '-' then
   Numero := ('-0');
  if (Numero = '') then Flutuante := 0
  else
    Flutuante := StrToFloat(Numero);
  if Flutuante < Min then Flutuante := Min;
  if Flutuante > Max then Flutuante := Max;
  Resultado := FormatFloat(TxtForma, Flutuante);
  Result := Resultado;
end;

function TUnFMX_Geral.ToFormStr(AProp: string; AVal: Single): string;
begin
  Result := Format(cForm,[AProp,'', AVal]);
end;

function TUnFMX_Geral.ToFormStrB(AProp: string;
  AVal: Boolean): string;
begin
  if AVal then
    ToFormStrS(AProp, 'True')
  else
    ToFormStrS(AProp, 'False');
end;

function TUnFMX_Geral.ToFormStrS(AProp, AVal: string): string;
begin
  Result := Format(cFormS,[AProp,'', AVal]);
end;

function TUnFMX_Geral.TrimAllNoDouble(Texto: String): String;
var
  I: Integer;
  s: String;
begin
  s := Texto;
  while Pos(' ', s) > 0 do
    Delete (s, Pos(' ', s), 1);
  while Pos(FormatSettings.ThousandSeparator, s) > 0 do
    Delete (s, Pos(FormatSettings.ThousandSeparator, s), 1);
  i := Pos(FormatSettings.CurrencyString, s);
  if i > 0 then
    Delete(S, I, Length(FormatSettings.CurrencyString));
  Result := s;
end;

function TUnFMX_Geral.TrimAllNoInteger(Texto: String): String;
var
  I: Integer;
  s: String;
begin
  s := Texto;
  while Pos(' ', s) > 0 do
    Delete (s, Pos(' ', s), 1);
  while Pos(FormatSettings.ThousandSeparator, s) > 0 do
    Delete (s, Pos(FormatSettings.ThousandSeparator, s), 1);

  i := Pos(FormatSettings.DecimalSeparator, s);
  if i = 0 then i := Length(s);
  Result := Copy(s, 1, i);
end;

function TUnFMX_Geral.ValidaDataBR(Texto: String; PermiteZero,
  ForceNextYear: Boolean): TDateTime;
var
  Data: TDateTime;
  Dia, Mes, Ano : Word;
  ADia, AMes, AAno : Word;
  TextChar : PChar;
  i, a, b: Integer;
  Compara: String;
begin
  if Texto = '0,00' then Texto := FormatDateTime('dd/mm/yyyy', Date);
  //Result := 0;
  try
    AAno := 0;
    Compara := '';
    if PermiteZero then
    begin
      for I := Low(Texto) to High(Texto) do
        Compara := Compara + '0';
      if Texto = Compara then
      begin
        Result := 0;
        Exit;
      end;
    end;
    TextChar := PChar(Texto);
    a := Pos('/', Texto);
    b := Length(Texto);
    if (a = 3) and (b = 5) then
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      Dia := IMV(Copy(Texto, 1, 2));
      Mes := IMV(Copy(Texto, 4, 2));
      Data := EncodeDate(Ano, Mes, Dia);
      if (int(Data) < Int(Date)) and ForceNextYear then
        Data := EncodeDate(Ano+1, Mes, Dia);
      Result := Data;
      //Texto := FormatDateTime('ddmmyyyy', Data);
    end else if a > 0 then
    begin
      Delete(Texto, a, 1);
      Dia := IMV(Copy(Texto, 1, a-1));
      b := pos('/', Texto);
      if b > 0 then Ano := StrToInt(Copy(Texto, b+1, Length(Texto)- b)) else
      begin
        DecodeDate(Date, AAno, AMes, ADia);
        Ano := AAno;
      end;
      if Ano < 100 then
        if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
      if b = 0 then b := Length(Texto) + 1;
      Mes := StrToInt(Copy(Texto, a, b-a));
      if (Ano=0) or (Mes=0) or (Dia=0) then
        Result := 0
      else begin
        Data := EncodeDate(Ano, Mes, Dia);
        if AAno > 0 then
        begin
          if (Int(Data) < Int(Date)) and ForceNextYear then
            Data := EncodeDate(Ano + 1, Mes, Dia);
        end;
        Result := Data;
      end;
    end else
    if (StrRScan(TextChar,'/') = nil) and
    ((Length(Texto) = 4) or
    (Length(Texto) = 6) or
    (Length(Texto) = 8)) then
    begin
      Dia := StrToInt(Texto[1]+Texto[2]);
      Mes := StrToInt(Texto[3]+Texto[4]);
      if (CharInSet(Texto[5], ['0'..'9'])) and
         (CharInSet(Texto[6], ['0'..'9'])) and
         (CharInSet(Texto[7], ['0'..'9'])) and
         (CharInSet(Texto[8], ['0'..'9'])) then
        Ano := StrToInt(Texto[5]+Texto[6]+Texto[7]+Texto[8])
      else
      if (CharInSet(Texto[5], ['0'..'9'])) and
         (CharInSet(Texto[6], ['0'..'9'])) then
        Ano := StrToInt(Texto[5]+Texto[6])
      else
      begin
        DecodeDate(Date, AAno, AMes, ADia);
        Ano := AAno;
      end;
      if Ano < 100 then
        if Ano > 49 then Ano := Ano + 1900 else Ano := Ano + 2000;
      if (Ano=0) or (Mes=0) or (Dia=0) then
        Result := 0

      else begin
        if not TryEncodedate(Ano, Mes, Dia, Data) then
          MB_Erro('Data inv�lida: ' + Texto + sLineBreak + 'Geral.ValidaDataBR()');
        //
        if AAno > 0 then
        begin
          if (Int(Data) < Int(Date)) and ForceNextYear then
            Data := EncodeDate(Ano + 1, Mes, Dia);
        end;
        Result := Data;
      end;
    end else
      Result := StrToDate(Texto);
  except
    raise;
  end;
end;

function TUnFMX_Geral.VariantToString(AVar: OleVariant): string;
var
  i: integer;
  V: olevariant;
begin
  if AVar = null then
  begin
    Result := '';
  end else begin
//In the BeforeNavigate2 event of TWebBrowser, you receive the PostData
//and Header data as OleVariant.  If you simply assign the OleVariant type
//to a string, you may get part of the data or garbage.

//You can convert the OleVariant to String using this function:
    Result := '';
    if System.Variants.VarType(AVar) = (varVariant or varByRef) then
       V := Variant(TVarData(AVar).VPointer^)
    else V := AVar;

    if VarType(V) = (varByte or varArray) then
        try
          for i:=VarArrayLowBound(V,1) to VarArrayHighBound(V,1) do
             Result := Result + Chr(Byte(V[i]));
        except;
        end
      else Result := V;
//A nice FAQ site on TWebBrowser is:
//http://members.home.com/hfournier/
  end;
end;

function TUnFMX_Geral.VariavelToStringAS(Variavel: Variant): String;
  function Inteiro(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := IntToStr(Integer(Variavel));
  end;
{
  function Intei64(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := String(Variavel);
  end;
}
begin
  // para ver a variavel sem aspas use
  // MLAGeral.VariantToString
  try
    case VarType(Variavel) of
      (*
      varEmpty    {= $0000}: Result := '';
      varNull     {= $0001}: Result := 'Null';
      varSmallint {= $0002}: Result := Inteiro(Variavel);
      varInteger  {= $0003}: Result := Inteiro(Variavel);
      varSingle   {= $0004}: Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005}: Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      varCurrency {= $0006}: Result := FormatFloat('0.000000000000000', Variavel);
      varDate     {= $0007}: Result := #39 + FormatDateTime('yyyy-mm-dd', Variavel) + #39;

      varBoolean  {= $000B}: Result := IntToStr(BoolToInt(Variavel));

      varString   {= $0100}: Result := #39 + Variavel + #39;
      *)
      varEmpty    {= $0000;} { vt_empty        0 } : Result := '';
      varNull     {= $0001;} { vt_null         1 } : Result := 'Null';
      varSmallint {= $0002;} { vt_i2           2 } : Result := Inteiro(Variavel);
      varInteger  {= $0003;} { vt_i4           3 } : Result := Inteiro(Variavel);
      varSingle   {= $0004;} { vt_r4           4 } : Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005;} { vt_r8           5 } : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      varCurrency {= $0006;} { vt_cy           6 } : Result := FormatFloat('0.000000000000000', Variavel);
      //varDate     {= $0007;} { vt_date         7 } : Result := #39 + FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', Variavel) + #39;
      varDate     {= $0007;} { vt_date         7 } : Result := #39 + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + #39;
      varOleStr   {= $0008;} { vt_bstr         8 } : Result := #39 + WideStringToSQLString(Variavel) + #39;
      varDispatch {= $0009;} { vt_dispatch     9 } : Result := #39 + Variavel + #39;
      varError    {= $000A;} { vt_error       10 } : Result := #39 + Variavel + #39;
      varBoolean  {= $000B;} { vt_bool        11 } : Result := IntToStr(BoolToInt(Variavel));
      varVariant  {= $000C;} { vt_variant     12 } : Result := #39 + Variavel + #39;
      varUnknown  {= $000D;} { vt_unknown     13 } : Result := #39 + Variavel + #39;
    //varDecimal  {= $000E;} { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
    //varUndef0F  {= $000F;} { undefined      15 } {UNSUPPORTED per Microsoft}
      varShortInt {= $0010;} { vt_i1          16 } : Result := Inteiro(Variavel);
      varByte     {= $0011;} { vt_ui1         17 } : Result := Inteiro(Variavel);
      varWord     {= $0012;} { vt_ui2         18 } : Result := Inteiro(Variavel);
      varLongWord {= $0013;} { vt_ui4         19 } : Result := Inteiro(Variavel);
      varInt64    {= $0014;} { vt_i8          20 } : Result := (*Intei64*)Inteiro(Variavel);
    //varWord64   {= $0015;} { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
    {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

      varStrArg   {= $0048;} { vt_clsid       72 } : Result := #39 + Variavel + #39;
      varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Result := #39 + WideStringToSQLString(Variavel) + #39;
      varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Result := #39 + Variavel + #39;
      varUString  {= $0102;} { Unicode string 258 } {not OLE compatible }: Result := #39 + WideStringToSQLString(Variavel) + #39;
      //271 {FMTBcdVariantType = 271}                : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      // custom types range from $110 (272) to $7FF (2047)
      //Tipo de vari�vel: 258 => UnicodeString
      else begin
        if (VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType') then
          Result := FloatToStrF(Variavel, ffFixed, 15, 15)
        else
        if (VarTypeAsText(VarType(Variavel)) = 'SQLTimeStampVariantType') then
          //Result := #39 + FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', Variavel) + #39
          Result := #39 + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + #39
        else begin
          //raise EAbort.Create('Vari�vel n�o definida!');
          MB_Erro('Vari�vel n�o definida!' + sLineBreak +
          'Tipo de vari�vel: ' + IntToStr(VarType(Variavel)) + ' => ' +
          VarTypeAsText(VarType(Variavel)));
          Result := #39 + VariantToString(Variavel) + #39;
        end;
      end;
    end;
    if (VarType(Variavel) in ([varSingle, varDouble, varCurrency]))
    or ((VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType')) then
    begin
        if Result.IndexOf(',.') > -1 then
          Result := Result.Replace(',.', '.');
        if Result.IndexOf(',') > -1 then
          Result := Result.Replace(',', '.');
          //Result[pos(',', Result)] := '.';
        // Evitar erro em n�meros muito grandes!
        //if Result.IndexOf(',') > 0 then Result[pos(',', Result)] := '.';
        // Evitar erro em n�meros muito grandes!


        if Result.Length > 21 then Result := Result.Substring(1, 21);
    end;
  except
    MB_Erro('Erro na function "VariavelToString".' + sLineBreak +
    'Tipo de vari�vel: ' + VarTypeAsText(VarType(Variavel)));
  end;
  (*varOleStr   = $0008;
  varDispatch = $0009;
  varError    = $000A;
  varBoolean  = $000B;
  varVariant  = $000C;
  varUnknown  = $000D;
  varByte     = $0011;

  varStrArg   = $0048;

  varString   = $0100;
  varAny      = $0101;
  varTypeMask = $0FFF;
  varArray    = $2000;

  varByRef    = $4000;

The lower twelve bits of a Variant type code (the bits defined by the varTypeMask bit mask) define the type of the Variant. The varArray bit is set if the Variant is an array of the given type. The varByRef bit is set if the Variant is a reference to a value of the given type as opposed to an actual value.

The following table describes the meaning of each of the Variant type codes.

VarType	Contents of Variant

varEmpty	The Variant is Unassigned.
Null	The Variant is Null.
varSmallint	16-bit signed integer (type Smallint).
varInteger	32-bit signed integer (type Integer).
varSingle	Single-precision floating-point value (type Single).
varDouble	Double-precision floating-point value (type Double).
varCurrency	Currency floating-point value (type Currency).
varDate	Date and time value (type TDateTime).
varOleStr	Reference to a dynamically allocated UNICODE string.

varDispatch	Reference to an Automation object (an IDispatch interface pointer).
varError	Operating system error code.
varBoolean	16-bit boolean (type WordBool).
varVariant	A Variant.
varUnknown	Reference to an unknown OLE object (an IUnknown interface pointer).
varByte	A Byte
varStrArg	COM-compatible string.
varString	Reference to a dynamically allocated string. (not COM compatible)
varAny	A CORBA Any value. *)
end;

function TUnFMX_Geral.VariavelToStringAD(Variavel: Variant): String;
  function Inteiro(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := IntToStr(Integer(Variavel));
  end;
  function Intei64(Variavel: Variant): String;
  begin
    if Variavel = Null then
      Result := 'Null'
    else
      Result := String(Variavel);
  end;
begin
  // para ver a variavel sem aspas use
  //  M L A G e r a l.VariantToString
  try
    case VarType(Variavel) of
      (*
      varEmpty    {= $0000}: Result := '';
      varNull     {= $0001}: Result := 'Null';
      varSmallint {= $0002}: Result := Inteiro(Variavel);
      varInteger  {= $0003}: Result := Inteiro(Variavel);
      varSingle   {= $0004}: Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005}: Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      varCurrency {= $0006}: Result := FormatFloat('0.000000000000000', Variavel);
      varDate     {= $0007}: Result := '"' + FormatDateTime('yyyy-mm-dd', Variavel) + '"';

      varBoolean  {= $000B}: Result := IntToStr(BoolToInt(Variavel));

      varString   {= $0100}: Result := '"' + Variavel + '"';
      *)
      varEmpty    {= $0000;} { vt_empty        0 } : Result := '';
      varNull     {= $0001;} { vt_null         1 } : Result := 'Null';
      varSmallint {= $0002;} { vt_i2           2 } : Result := Inteiro(Variavel);
      varInteger  {= $0003;} { vt_i4           3 } : Result := Inteiro(Variavel);
      varSingle   {= $0004;} { vt_r4           4 } : Result := FormatFloat('0.000000000000000', Variavel);
      //varDouble   {= $0005;} { vt_r8           5 } : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      //varDouble   {= $0005;} { vt_r8           5 } : Result := FormatFloat('0.000000000000000', Variavel);
      varDouble   {= $0005;} { vt_r8           5 } : Result := FormatFloat('0.000000000000000', Variavel);
      varCurrency {= $0006;} { vt_cy           6 } : Result := FormatFloat('0.000000000000000', Variavel);
      //varDate     {= $0007;} { vt_date         7 } : Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', Variavel) + '"';
      varDate     {= $0007;} { vt_date         7 } : Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + '"';
      varOleStr   {= $0008;} { vt_bstr         8 } : Result := '"' + WideStringToSQLString(Variavel) + '"';
      varDispatch {= $0009;} { vt_dispatch     9 } : Result := '"' + Variavel + '"';
      varError    {= $000A;} { vt_error       10 } : Result := '"' + Variavel + '"';
      varBoolean  {= $000B;} { vt_bool        11 } : Result := IntToStr(BoolToInt(Variavel));
      varVariant  {= $000C;} { vt_variant     12 } : Result := '"' + Variavel + '"';
      varUnknown  {= $000D;} { vt_unknown     13 } : Result := '"' + Variavel + '"';
    //varDecimal  {= $000E;} { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
    //varUndef0F  {= $000F;} { undefined      15 } {UNSUPPORTED per Microsoft}
      varShortInt {= $0010;} { vt_i1          16 } : Result := Inteiro(Variavel);
      varByte     {= $0011;} { vt_ui1         17 } : Result := Inteiro(Variavel);
      varWord     {= $0012;} { vt_ui2         18 } : Result := Inteiro(Variavel);
      varLongWord {= $0013;} { vt_ui4         19 } : Result := Inteiro(Variavel);
      varInt64    {= $0014;} { vt_i8          20 } : Result := Intei64(Variavel);
    //varWord64   {= $0015;} { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
    {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

      varStrArg   {= $0048;} { vt_clsid       72 } : Result := '"' + Variavel + '"';
      varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Result := '"' + WideStringToSQLString(Variavel) + '"';
      varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Result := '"' + Variavel + '"';
      varUString  {= $0102;} { Unicode string 258 } {not OLE compatible }: Result := '"' + WideStringToSQLString(Variavel) + '"';
      //271 {FMTBcdVariantType = 271}                : Result := FloatToStrF(Variavel, ffFixed, 15, 15);
      // custom types range from $110 (272) to $7FF (2047)
      //Tipo de vari�vel: 258 => UnicodeString
      else begin
        if (VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType') then
          Result := FloatToStrF(Variavel, ffFixed, 15, 15)
        else
        if (VarTypeAsText(VarType(Variavel)) = 'SQLTimeStampVariantType') then
          //Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', Variavel) + '"'
          Result := '"' + FormatDateTime('yyyy-mm-dd hh:nn:ss', Variavel) + '"'
        else begin
          //raise EAbort.Create('Vari�vel n�o definida!');
          MB_Erro('Vari�vel n�o definida!' + sLineBreak +
          'Tipo de vari�vel: ' + IntToStr(VarType(Variavel)) + ' => ' +
          VarTypeAsText(VarType(Variavel)));
          Result := '"' + VariantToString(Variavel) + '"';
        end;
      end;
    end;
    if (VarType(Variavel) in ([varSingle, varDouble, varCurrency]))
    or ((VarTypeAsText(VarType(Variavel)) = 'FMTBcdVariantType')) then
    begin
        //if pos(',', Result) > 0 then Result[pos(',', Result)] := '.';
        if Result.IndexOf(',.') > -1 then
          Result := Result.Replace(',.', '.');
        if Result.IndexOf(',') > -1 then
          Result := Result.Replace(',', '.');
        // Evitar erro em n�meros muito grandes!
        if Length(Result) > 21 then Result := Copy(Result, 1, 21);
    end;
  except
    FMX_Geral.MB_Erro('Erro na function "VariavelToString".' + sLineBreak +
    'Tipo de vari�vel: ' + VarTypeAsText(VarType(Variavel)));
  end;
  (*varOleStr   = $0008;
  varDispatch = $0009;
  varError    = $000A;
  varBoolean  = $000B;
  varVariant  = $000C;
  varUnknown  = $000D;
  varByte     = $0011;

  varStrArg   = $0048;

  varString   = $0100;
  varAny      = $0101;
  varTypeMask = $0FFF;
  varArray    = $2000;

  varByRef    = $4000;

The lower twelve bits of a Variant type code (the bits defined by the varTypeMask bit mask) define the type of the Variant. The varArray bit is set if the Variant is an array of the given type. The varByRef bit is set if the Variant is a reference to a value of the given type as opposed to an actual value.

The following table describes the meaning of each of the Variant type codes.

VarType	Contents of Variant

varEmpty	The Variant is Unassigned.
Null	The Variant is Null.
varSmallint	16-bit signed integer (type Smallint).
varInteger	32-bit signed integer (type Integer).
varSingle	Single-precision floating-point value (type Single).
varDouble	Double-precision floating-point value (type Double).
varCurrency	Currency floating-point value (type Currency).
varDate	Date and time value (type TDateTime).
varOleStr	Reference to a dynamically allocated UNICODE string.

varDispatch	Reference to an Automation object (an IDispatch interface pointer).
varError	Operating system error code.
varBoolean	16-bit boolean (type WordBool).
varVariant	A Variant.
varUnknown	Reference to an unknown OLE object (an IUnknown interface pointer).
varByte	A Byte
varStrArg	COM-compatible string.
varString	Reference to a dynamically allocated string. (not COM compatible)
varAny	A CORBA Any value. *)
end;

function TUnFMX_Geral.VersaoTxt2006(Versao: Integer): String;
var
 Ano, Mes, Dia, Com : Integer;
begin
  Ano := Versao Div 100000000;
  Mes := (Versao Mod 100000000) Div 1000000;
  Dia := (Versao Mod 1000000) DIV 10000;
  Com := Versao Mod 10000;
  Result := IntToStr(Ano)+'.'+IntToStr(Mes)+'.'+
            IntToStr(Dia)+'.'+IntToStr(Com);
  APP_VERSAO := Result;
end;

function TUnFMX_Geral.WideStringToSQLString(Texto: (*Wide*)String): (*Wide*)String;
const
  SP = '\\';
var
  i: integer;
  ServerPath: Boolean;
begin
  Result := Texto;
  Result := '';
  if Texto = '' then exit;
  ServerPath := Texto.IndexOf(SP) = 1;
  for i := Low(Texto) to High(Texto) do
  begin
    if Texto[i] = '\' then Result := Result + '\\' else
    //if Texto[i] = '/' then Result := Result + '//' else
    if Texto[i] = #39 then
    begin
      if (Texto.Length = i) or (Texto[i+1] <> #39) then
        Result := Result + #39#39;
    end
    {
    ... colocar aqui outros empecilhos
    else if ... then ...
    }
    else
    if (i > 1) and (Texto[i] = '-') and (Texto[i-1] = '-') then
      Result := Result + ' '
    else
      Result := Result + Texto[i];
  end;

  Result := Substitui(Result, '\\\\', '\\');
  Result := Substitui(Result, '\\\', '\\');
  Result := Substitui(Result, #39#39#39#39, #39#39);
  Result := Substitui(Result, #39#39#39, #39#39);
  //
  if ServerPath then
    Result := SP + Result;
end;

end.

