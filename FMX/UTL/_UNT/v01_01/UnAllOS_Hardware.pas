unit UnAllOS_Hardware;

interface

uses System.Classes, System.UITypes, FMX.Forms, FMX.ListBox, FMX.StdCtrls,
  FMX.Dialogs, FMX.PhoneDialer, FMX.Platform, System.Rtti, System.SysUtils,
  System.Variants;

type
  TUnAllOS_Hardware = class(TObject)
  private

  public
    function  Clipboard_ColarTexto(): String;
    procedure Clipboard_CopiarTexto(Texto: String);
    procedure Telefone_FazerLigacao(Telefone: String;
              PhoneDialerService: IFMXPhoneDialerService;
              Edita: Boolean = False);
  end;

var
  AllOS_Hardware: TUnAllOS_Hardware;
const
  CO_MSG_PLATAFORMA: String = 'Fun��o n�o implementada para a plataforma atual!';

implementation

uses UnGeral;

{ TUnAllOS_Hardware }

procedure TUnAllOS_Hardware.Telefone_FazerLigacao(Telefone: String;
  PhoneDialerService: IFMXPhoneDialerService; Edita: Boolean = False);
var
  Tel: String;
begin
  if Telefone <> '' then
  begin
    //{$IF DEFINED(iOS) or DEFINED(ANDROID)}
    Tel := Telefone;
    //
    if Edita = True then
    begin
      InputQuery('Telefone', ['N�mero'], [Tel],
        procedure(const AResult: TModalResult; const AValues: array of string)
        begin
          if (AResult = mrOk) and (Tel <> '') then
          begin
            PhoneDialerService.Call(Tel);
          end;
        end);
    end else
      PhoneDialerService.Call(Tel);
  end;
end;

function TUnAllOS_Hardware.Clipboard_ColarTexto(): String;
var
  Svc: IFMXClipboardService;
  Value: TValue;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, Svc) then
  begin
    Value := Svc.GetClipboard;
    //
    if not Value.IsEmpty then
    begin
      if Value.IsType<string> then
        Result := Value.ToString;
    end;
  end else
    Result := '';
end;

procedure TUnAllOS_Hardware.Clipboard_CopiarTexto(Texto: String);
var
  Svc: IFMXClipboardService;
begin
  if Texto <> '' then
  begin
    if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, Svc) then
    begin
      Svc.SetClipboard(Texto);
      //
      Geral.MB_Aviso('Texto copiado para a �rea de transfer�ncia!');
    end else
      Geral.MB_Aviso(CO_MSG_PLATAFORMA);
  end;
end;

end.
