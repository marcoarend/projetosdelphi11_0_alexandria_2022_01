unit UnFMX_DmkForms;

interface

uses System.Classes, System.UITypes, FMX.Controls, FMX.Forms, FMX.ListBox,
  FMX.StdCtrls, FMX.Dialogs, FMX.Types, FMX.Layouts, System.SysUtils,
  System.Variants, System.Rtti, System.Types, System.DateUtils, FMX.Graphics,
  Data.Bind.Components, System.Notification, System.Math, FMX.Surfaces,
  System.IOUtils, UnGrl_Vars, FMX.Styles,
  UnDmkENums;

type
  TUnFMX_DmkForms = class(TObject)
  private
    procedure AplicaStyle(var Reference);
  public
    // J A N E L A S
    procedure ConfiguraTextoBotoes(RemoveText: Boolean;
              const InstanceClass: TComponentClass; var Reference);
    procedure CriaFm_AllOS(const InstanceClass: TComponentClass; var Reference;
              const Liberado: Boolean = False; const TelaCheia: Boolean = False);
    function  CriaFm_AllOS2(const InstanceClass: TComponentClass; var Reference;
              const Liberado: Boolean = False; const TelaCheia: Boolean = False):
              Boolean;
    function  CriaFm_AllOS0(const InstanceClass: TComponentClass; var Reference;
              Modo: TFormCreateModo; const Liberado: Boolean = False; const
              TelaCheia: Boolean = False; const AplyStyle: Boolean = True):
              Boolean;
    procedure CriaFm_PesqCod(Componente: TObject; Codigo: Integer; SQL: array of String;
              const Liberado: Boolean = False; const TelaCheia: Boolean = False);
    procedure RegistraDevice();
    function  FechaFm_AllOS0(var Reference): Boolean;
    function  DestroiFm_AllOS0(var Reference): Boolean;
    procedure ShowModal(var Reference);
  end;

var
  FMX_dmkForms: TUnFMX_DmkForms;

implementation

uses UnFMX_Geral, UnFMX_Grl_Vars, PesqCod, (*DmkLogin,*) Principal;

{ TUnFMX_DmkForms }

procedure TUnFMX_DmkForms.AplicaStyle(var Reference);
var
  vResourceStream : TResourceStream;
  SB: TStyleBook;
  EstiloForms: String;
begin
  if VAR_EstiloForms = '' then
    EstiloForms := 'AquaGraphite'
  else
  if VAR_EstiloForms <> '[Nenhum]' then
    EstiloForms := VAR_EstiloForms
  else
    EstiloForms := '';
  //
  if EstiloForms <> '' then
  begin
(*
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
*)
    SB := TStyleBook(TForm(Reference).FindComponent('StyleBook_' + EstiloForms));
    //SB := TStyleBook.Create(TForm(Reference));
    if SB <> nil then
    begin
  {
      vResourceStream := TResourceStream.Create( HInstance, 'AQUAGRAPHITE_Style', RT_RCDATA );
    //  vResourceStream := TResourceStream.Create( HInstance, 'AIR_Style', RT_RCDATA );
    //  vResourceStream := TResourceStream.Create( HInstance, 'LIGHT_Style', RT_RCDATA );
      try
        SB.LoadFromStream( vResourceStream );
      finally
        vResourceStream.Free;
      end;
  }
      TForm(Reference).StyleBook := SB;
    end;
  end;
   //StyleBook1.Style.FindStyleResource('
end;

procedure TUnFMX_DmkForms.ConfiguraTextoBotoes(RemoveText: Boolean;
  const InstanceClass: TComponentClass; var Reference);
var
  Botao: TSpeedButton;
  i: Integer;
begin
  //BUG do Delphi = N�o mostra desenho em alguns bot�es na Windows somente Android
  for i := 0 to TForm(Reference).ComponentCount - 1 do
  begin
    if TForm(Reference).Components[i] is TSpeedButton then
    begin
      Botao := TSpeedButton(TForm(Reference).Components[i]);

      if Botao <> nil then
      begin
        if (RemoveText = True) and (Botao.StyleLookup <> 'toolbutton') then
          Botao.Text := ''
        else
          Botao.Text := Botao.Text;
      end;
    end;
  end;
end;

procedure TUnFMX_DmkForms.CriaFm_AllOS(const InstanceClass: TComponentClass;
  var Reference; const Liberado, TelaCheia: Boolean);
  //
begin
  if ((VAR_WEB_USR_ID <> 0) and (VAR_WEB_USR_TOKEN <> '')) or (Liberado) then
  begin
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    Application.CreateForm(InstanceClass, Reference);
    AplicaStyle(Reference);
    //
    ConfiguraTextoBotoes(True, InstanceClass, Reference);
    //
    TForm(Reference).FullScreen := TelaCheia;
    TForm(Reference).Show;
    {$ELSE}
    Application.CreateForm(InstanceClass, Reference);
    AplicaStyle(Reference);
    //
    ConfiguraTextoBotoes(False, InstanceClass, Reference);
    //
    TForm(Reference).FullScreen := TelaCheia;
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    {$ENDIF}
  end else
    RegistraDevice;
end;

function TUnFMX_DmkForms.CriaFm_AllOS2(const InstanceClass: TComponentClass;
  var Reference; const Liberado, TelaCheia: Boolean): Boolean;
begin
  Result := False;
  //
  if ((VAR_WEB_USR_ID <> 0) and (VAR_WEB_USR_TOKEN <> '')) or (Liberado) then
  begin
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    Application.CreateForm(InstanceClass, Reference);
    AplicaStyle(Reference);
    //
    ConfiguraTextoBotoes(True, InstanceClass, Reference);
    //
    TForm(Reference).FullScreen := TelaCheia;
    {$ELSE}
    Application.CreateForm(InstanceClass, Reference);
    AplicaStyle(Reference);
    //
    ConfiguraTextoBotoes(False, InstanceClass, Reference);
    //
    TForm(Reference).FullScreen := TelaCheia;
    {$ENDIF}
    Result := True;
  end else
    RegistraDevice;
end;

function TUnFMX_DmkForms.CriaFm_AllOS0(const InstanceClass: TComponentClass; var
  Reference; Modo: TFormCreateModo; const Liberado: Boolean = False; const
  TelaCheia: Boolean = False; const AplyStyle: Boolean = True): Boolean;
const
  sProcName = 'TUnFMX_DmkForms.CriaFm_AllOS0';
begin
  Result   := False;
  //JaExiste := False;
  if ((VAR_WEB_USR_ID <> 0) and (VAR_WEB_USR_TOKEN <> '')) or (Liberado) then
  begin
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
(*
      if DestroiAntes and (TForm(Reference) <> nil) then
         TForm(Reference).DisposeOf;  // Erro
*)      //
      if TForm(Reference) = nil then
      begin
        Application.CreateForm(InstanceClass, Reference);
        if AplyStyle then
          AplicaStyle(Reference);
        //
        ConfiguraTextoBotoes(True, InstanceClass, Reference);
        //
        TForm(Reference).FullScreen := TelaCheia;
        //FMX_Geral.MB_Info(TForm(Reference).Name + ' criado!');
      end; // else JaExiste := True;
      case Modo of
        //TFormCreateModo.fcmNenhum: ; // nada, informa erro
        TFormCreateModo.fcmOnlyCreate: ; // nada, apenas cria
        TFormCreateModo.fcmCreateTryShwM: TForm(Reference).Show;
        else FMX_Geral.MB_Erro('[Android/iOS] "TFormCreateModo" indefinido em ' +
        sProcName + ' para ' + TForm(Reference).Name);
      end;
    {$ELSE}
      Application.CreateForm(InstanceClass, Reference);
      if AplyStyle then
        AplicaStyle(Reference);
      //
      ConfiguraTextoBotoes(False, InstanceClass, Reference);
      //
      TForm(Reference).FullScreen := TelaCheia;
      //
      case Modo of
        //TFormCreateModo.fcmNenhum: ; // nada, informa erro
        TFormCreateModo.fcmOnlyCreate: ; // nada, apenas cria
        TFormCreateModo.fcmCreateTryShwM:
        begin
          TForm(Reference).ShowModal;
          TForm(Reference).Destroy;
        end;
        else FMX_Geral.MB_Erro('[MS Windows] "TFormCreateModo" indefinido em ' +
        sProcName + ' para ' + TForm(Reference).Name);
      end;
    {$ENDIF}
  end else
    RegistraDevice;
  //
  Result := True;
end;

procedure TUnFMX_DmkForms.CriaFm_PesqCod(Componente: TObject;
  Codigo: Integer; SQL: array of String; const Liberado: Boolean = False;
  const TelaCheia: Boolean = False);

  function ObtemSQL(const SQL: array of string): TStringList;
  var
    i: Integer;
  begin
    Result := TStringList.Create;
    for i := low(SQL) to high(SQL) do
      Result.Add(SQL[i]);
  end;

begin
  if CriaFm_AllOS0(TFmPesqCod, FmPesqCod, fcmOnlyCreate) = True then
  begin
    FmPesqCod.FCompo     := Componente;
    FmPesqCod.FCodigo    := Codigo;
    FmPesqCod.QrPesq.SQL := ObtemSQL(SQL);
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmPesqCod.Show;
    {$ELSE}
    FmPesqCod.ShowModal;
    FmPesqCod.Destroy;
    {$ENDIF}
  end else
    RegistraDevice;
end;

function TUnFMX_DmkForms.DestroiFm_AllOS0(var Reference): Boolean;
begin
  {$If DEFINED(iOS) or DEFINED(ANDROID)}
    TForm(Reference).Close;
  {$Else}
    TForm(Reference).Close;
  {$EndIf}
end;

function TUnFMX_DmkForms.FechaFm_AllOS0(var Reference): Boolean;
begin
  {$If DEFINED(iOS) or DEFINED(ANDROID)}
    TForm(Reference).Hide;
  {$Else}
    TForm(Reference).Close;
  {$EndIf}
end;

procedure TUnFMX_DmkForms.RegistraDevice;
begin
  //CriaFm_AllOS0(TFmDmkLogin, FmDmkLogin, fcmCreateTryShwM, True);
end;

procedure TUnFMX_DmkForms.ShowModal(var Reference);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  TForm(Reference).Show;
  {$ELSE}
  TForm(Reference).ShowModal;
  TForm(Reference).Destroy;
  {$ENDIF}
end;

end.

