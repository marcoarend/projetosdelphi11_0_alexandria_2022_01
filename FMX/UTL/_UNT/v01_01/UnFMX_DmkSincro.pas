unit UnFMX_DmkSincro;

interface

uses System.Classes, System.UITypes, System.Rtti, System.SysUtils,
  {$IfDef MSWINDOWS} System.WideStrUtils, {$EndIf}
  System.Variants, System.Threading, FMX.Forms, FMX.ListBox, FMX.StdCtrls,
  FMX.Dialogs, FMX.PhoneDialer, FMX.Platform, FireDAC.Comp.Client, Xml.XMLIntf,
  Xml.XMLDoc, Xml.adomxmldom, UnGrl_DmkDB, UnDmkEnums, UnGrl_Vars;

type
  TUnFMX_DmkSincro = class(TObject)
  private
    function  ExecutSincronismo_Upload(DataBase: TFDConnection; QueryUpd,
              QueryAux: TFDQuery; Tabela: String; Acao: TLastAcao;
              SQLIndex: array of string; var Msg, Log: String): Boolean;
    function  ExecutaSincronismo_Download(DataBase: TFDConnection;
              QueryUpd: TFDQuery; Tabela: String; DataHoraUTC: TDateTime;
              Acao: TLastAcao; var Msg, Log: String): Boolean;
  public
    function  SincronizaTabela(DataBase: TFDConnection; QueryUpd,
              QueryAux: TFDQuery; Tabela: String; CamposIndex: array of string;
              DataHora: TDateTime; var Msg: String): Boolean;
  end;

var
  FMX_DmkSincro: TUnFMX_DmkSincro;

implementation

uses UnFMX_Geral, UnFMX_DmkWeb, UnFMX_Grl_Vars, UnGrl_Geral, MyListas,
  UnFMX_DmkProcFunc;

{ TUnFMX_DmkSincro }

function TUnFMX_DmkSincro.ExecutaSincronismo_Download(DataBase: TFDConnection;
  QueryUpd: TFDQuery; Tabela: String; DataHoraUTC: TDateTime; Acao: TLastAcao;
  var Msg, Log: String): Boolean;
var
  DtaTime, DtaTimeTb, Res, SQL, SQLWhere, SQLCampos, SQLValores, MethodVal: String;
  i, j, MsgCod: Integer;
  XMLArqRes: IXMLDocument;
  ListaCampos, ListaValores: TStringList;
begin
  if Acao = laUpd then
  begin
    FMX_Geral.MB_Aviso('A��o n�o implementada');
    Exit;
  end;
  //
  Result    := False;
  DtaTime   := FMX_Geral.FDT(DataHoraUTC, 109);
  DtaTimeTb := FMX_Geral.FDT(Grl_DmkDB.ObtemDataSincro(DataBase, Tabela), 109);
  //
  Log := Log + sLineBreak + sLineBreak + 'Data hora UTC = ' + DtaTime + sLineBreak +
           'Data �ltimo sincronismo = ' + DtaTimeTb;
  //
  if Acao = laIns then
    MethodVal := 'REST_dmkSinc_Download'
  else
    MethodVal := 'REST_dmkSinc_Download_Del';
  //
  Log := Log + sLineBreak + sLineBreak + 'M�todo = ' + MethodVal;
  //
  Res := FMX_dmkWeb.REST_URL_Post(
    ['AplicativoSolicitante', 'IDUsuarioSolicitante', 'Token'],
    [FMX_Geral.FF0(CO_DMKID_APP), FMX_Geral.FF0(VAR_WEB_USR_ID), VAR_WEB_USR_TOKEN],
    ['id', 'method', 'Tabela', 'DataHoraUTC'],
    [VAR_WEB_USR_WEBID, MethodVal, Tabela, DtaTimeTb], CO_URL_DMKAPI);
  //
  if Res <> '' then
  begin
    XMLArqRes := TXMLDocument.Create(nil);
    XMLArqRes.Active := False;
    try
      Log := Log + sLineBreak + sLineBreak + 'Resposta = ' + Res;
      //
      if Pos('MSGCOD', UpperCase(Res)) > 0 then
      begin
        XMLArqRes.LoadFromXML(Res);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        i := 0;
        //
        for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLArqRes.DocumentElement.ChildNodes[i] do
          begin
            MsgCod := FMX_Geral.IMV(ChildNodes['MsgCod'].Text);

            if MsgCod <> 202 then //Sem registros para retornar
              Msg := Msg + sLineBreak + ChildNodes['MsgTxt'].Text;
          end;
        end;
      end else
      if Pos('XML', UpperCase(Res)) > 0 then
      begin
        XMLArqRes.LoadFromXML(Res);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        ListaCampos  := TStringList.Create;
        ListaValores := TStringList.Create;
        try
          i := 0;
          //
          for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
          begin
            with XMLArqRes.DocumentElement.ChildNodes[i] do
            begin
              if Acao = laIns then
              begin
                if i = 0 then
                begin
                  SQLCampos  := '';
                  //
                  j := 0;
                  //
                  for j := 0 to ChildNodes.Count - 1 do
                  begin
                    if j = ChildNodes.Count - 1 then
                      SQLCampos := SQLCampos + ChildNodes[j].Text
                    else
                      SQLCampos := SQLCampos + ChildNodes[j].Text + ',';
                  end;
                end else
                begin
                  if SQLCampos <> '' then
                  begin
                    SQLValores := '';
                    //
                    j := 0;
                    //
                    for j := 0 to ChildNodes.Count - 1 do
                    begin
                      if j = ChildNodes.Count - 1 then
                        SQLValores := SQLValores + ChildNodes[j].Text
                      else
                        SQLValores := SQLValores + ChildNodes[j].Text + ',';
                    end;
                    {$IfDef MSWINDOWS}
                    if IsUTF8String(SQLValores) then
                      SQLValores := Utf8ToAnsi(SQLValores);
                    {$EndIf}
                    //
                    SQL := 'INSERT OR REPLACE INTO ' + Tabela + ' (' + SQLCampos + ') VALUES (' + SQLValores + ')';
                    //
                    if Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [SQL]) then
                    begin
                      if i = XMLArqRes.DocumentElement.ChildNodes.Count - 1 then
                        Grl_DmkDB.AtualizaSincroDB(DataBase, stMobile, Tabela, DtaTime);
                    end;
                  end else
                  begin
                    Msg := Msg + sLineBreak + 'O XML retornado � inv�lido!';
                    Exit;
                  end;
                end;
              end else
              begin
                if i = 0 then
                begin
                  j := 0;
                  //
                  for j := 0 to ChildNodes.Count - 1 do
                    ListaCampos.Add(ChildNodes[j].Text);
                end else
                begin
                  if ListaCampos.Count > 0 then
                  begin
                    ListaValores.Clear;
                    //
                    j := 0;
                    //
                    for j := 0 to ChildNodes.Count - 1 do
                      ListaValores.Add(ChildNodes[j].Text);

                    SQL      := 'DELETE FROM ' + Tabela;
                    SQLWhere := '';

                    if ListaCampos.Count = ListaValores.Count then
                    begin
                      j := 0;
                      //
                      for j := 0 to ListaCampos.Count - 1 do
                      begin
                        if j = 0 then
                          SQLWhere := ' WHERE ' + ListaCampos[j] + ' = ' + ListaValores[j]
                        else
                          SQLWhere := SQLWhere + ' AND ' + ListaCampos[j] + ' = ' + ListaValores[j];
                      end;
                    end else
                    begin
                      Msg := Msg + sLineBreak + 'Falha ao excluir registros!';
                      Exit;
                    end;
                    //
                    if Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [SQL, SQLWhere]) then
                    begin
                      if i = XMLArqRes.DocumentElement.ChildNodes.Count - 1 then
                        Grl_DmkDB.AtualizaSincroDB(DataBase, stMobile, Tabela, DtaTime);
                    end;
                  end else
                  begin
                    Msg := Msg + sLineBreak + 'O XML retornado � inv�lido!';
                    Exit;
                  end;
                end;
              end;
            end;
          end;
        finally
          ListaCampos.Free;
          ListaValores.Free;
        end;
      end else
        Msg := Msg + sLineBreak + 'Falha ao fazer requisi��o!';
    finally
      ;
    end;
  end;
  if Msg = '' then
  begin
    Result := True;
  end;
end;

function TUnFMX_DmkSincro.ExecutSincronismo_Upload(DataBase: TFDConnection;
  QueryUpd, QueryAux: TFDQuery; Tabela: String; Acao: TLastAcao;
  SQLIndex: array of string; var Msg, Log: String): Boolean;

  function EnviaXMLToWeb(XMLArq: IXMLDocument): Boolean;
  var
    i, MsgCod: Integer;
    MsgTxt, Res, XMLEnv_Txt, URL: String;
    XMLRes, XMLEnv: TStringStream;
    XMLArqRes: IXMLDocument;
  begin
    Result := False;
    XMLRes := TStringStream.Create('');
    XMLEnv := TStringStream.Create('');
    //
    (*
    XMLArq.SaveToFile('C:\_Compilers\RAD_PHP_XE2\Projetos\Dermatek\Teste.xml');
    Exit;
    *)
    //
    XMLArq.SaveToXML(XMLEnv_Txt);
    //
    XMLEnv_Txt := StringReplace(XMLEnv_Txt, #13, '', [rfReplaceAll]);
    XMLEnv_Txt := StringReplace(XMLEnv_Txt, #10, '', [rfReplaceAll]);
    //
    try
      case Acao of
        laIns: URL := CO_URL_DMKAPI + '?id='+ VAR_WEB_USR_WEBID + '&method=REST_dmkSinc_Upload_Ins&Tabela=' + Tabela;
        laUpd: URL := CO_URL_DMKAPI + '?id='+ VAR_WEB_USR_WEBID + '&method=REST_dmkSinc_Upload_Upd&Tabela=' + Tabela;
        laDel: URL := CO_URL_DMKAPI + '?id='+ VAR_WEB_USR_WEBID + '&method=REST_dmkSinc_Upload_Del&Tabela=' + Tabela;
      end;
      Log := Log + sLineBreak + sLineBreak + 'URL = ' + URL + sLineBreak + 'XML enviado = ' + XMLEnv_Txt;
      //
      Res := FMX_dmkWeb.REST_XML_PUT(
        ['AplicativoSolicitante', 'IDUsuarioSolicitante', 'Token'],
        [FMX_Geral.FF0(CO_DMKID_APP), FMX_Geral.FF0(VAR_WEB_USR_ID), VAR_WEB_USR_TOKEN],
        XMLEnv_Txt, URL);
      //
      if Res <> '' then
      begin
        Log := Log + sLineBreak + sLineBreak + 'Resposta = ' + Res;
        //
        if Pos('XML', UpperCase(Res)) > 0 then
        begin
          XMLArqRes := TXMLDocument.Create(nil);
          XMLArqRes.Active := False;
          try
            XMLArqRes.LoadFromXML(Res);
            //
            i := 0;
            //
            for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
            begin
              with XMLArqRes.DocumentElement.ChildNodes[i] do
              begin
                MsgCod := FMX_Geral.IMV(ChildNodes['MsgCod'].Text);
                MsgTxt := ChildNodes['MsgTxt'].Text;
                //
                if MsgCod = 104 then
                begin
                  Grl_DmkDB.ExecutaSQLQuery0(QueryUpd, DataBase, [
                    'DELETE FROM ' + Tabela,
                    'WHERE AlterWeb=' + FMX_Geral.FF0(Integer(alMob)),
                    'AND LastAcao=' + FMX_Geral.FF0(Integer(Acao)),
                    '']);
                  //
                  Result := True;
                end else
                  Msg := Msg + sLineBreak + 'Falha ao sincronizar dados!' + sLineBreak + 'Motivo: ' + MsgTxt;
              end;
            end;
          finally
            ;
          end;
        end else
          Msg := Msg + sLineBreak + 'Falha ao fazer requisi��o!';
      end;
      XMLRes.Free;
      XMLEnv.Free;
    except
      XMLRes.Free;
      XMLEnv.Free;
    end;
  end;

  function LocalizaValorArray(ArrayFilds: array of string; Valor: String): Boolean;
  var
    k: Integer;
  begin
    Result := False;
    //
    for k := Low(ArrayFilds) to High(ArrayFilds) do
    begin
      if ArrayFilds[k] = Valor then
      begin
        Result := True;
        Exit;
      end;
    end;
  end;

const
  MAX_REGS: Integer = 50;
var
  XMLArq: IXMLDocument;
  NodeReg0, NodeVal0, NodeReg1, NodeVal1, NodeReg, NodeVal, NodeKReg, NodeKVal: IXMLNode;
  i, j: Integer;
  Campo, Valor, SQLCompl: String;
begin
  Result := False;
  //
  XMLArq := TXMLDocument.Create(nil);
  XMLArq.Active := False;
  //
  try
    if Acao = laDel then
      SQLCompl := ''
    else
      SQLCompl := 'AND AlterWeb=' + FMX_Geral.FF0(Integer(alMob));
    //
    Grl_DmkDB.AbreSQLQuery0(QueryAux, DataBase, [
      'SELECT * ',
      'FROM ' + Tabela,
      'WHERE CliIntSync=' + FMX_Geral.FF0(VAR_WEB_USR_ENT),
      SQLCompl,
      'AND LastAcao=' + FMX_Geral.FF0(Integer(Acao)),
      '']);
    //
    Log := Log + sLineBreak + sLineBreak + QueryAux.SQL.Text;
    //
    if QueryAux.RecordCount > 0 then
    begin
      i := 0;
      //
      while not QueryAux.Eof do
      begin
        if i = 0 then
        begin
          XMLArq.Active          := True;
          XMLArq.Version         := '1.0';
          XMLArq.Encoding        := 'UTF-8';
          XMLArq.DocumentElement := XMLArq.CreateNode(Tabela, ntElement, '');
          //
          NodeReg0 := XMLArq.DocumentElement.AddChild('fld', -1);
          //
          if Acao = laUpd then
            NodeReg1 := XMLArq.DocumentElement.AddChild('key', -1);
        end;
        j := 0;
        //
        for j := 0 to QueryAux.Fields.Count -1 do
        begin
          Valor := AnsiToUtf8(FMX_Geral.VariavelToString(QueryAux.Fields[j].Value));
          Campo := QueryAux.Fields[j].FieldName;
          //
          if i = 0 then
          begin
            NodeVal0      := NodeReg0.AddChild('nam', -1);
            NodeVal0.Text := Campo;
          end;
          if j = 0 then
          begin
            NodeReg := XMLArq.DocumentElement.AddChild('reg' + FMX_Geral.FF0(i + 1), -1);
            //
            if Acao = laUpd then
              NodeKReg := XMLArq.DocumentElement.AddChild('key' + FMX_Geral.FF0(i + 1), -1);
          end;
          NodeVal      := NodeReg.AddChild('val', -1);
          NodeVal.Text := Valor;
        end;
        if ((i + 1) = MAX_REGS) or (QueryAux.RecordCount = (i + 1)) then
        begin
          Log := Log + sLineBreak + sLineBreak + XMLArq.XML.Text;
          //
          if not EnviaXMLToWeb(XMLArq) then
          begin
            Msg := Msg + sLineBreak + 'Fun��o: "ExecutSincronismo_Upload.SalvaXMLESincroniza"';
            Log := Log + sLineBreak + sLineBreak + Msg;
            Exit;
          end else
            Result := True;
          //
          i := 0;
        end else
          i := i + 1;
        //
        QueryAux.Next;
      end;
    end else
      Result := True;
  finally
    ;
  end;
end;

function TUnFMX_DmkSincro.SincronizaTabela(DataBase: TFDConnection;
  QueryUpd, QueryAux: TFDQuery; Tabela: String; CamposIndex: array of string;
  DataHora: TDateTime; var Msg: String): Boolean;

  procedure SalvaLog(Log: String);
  begin
    Grl_Geral.SalvaArquivoEmDocumentos(Log, 'LogSincro_' + Tabela, 'txt');
  end;

var
  Log: string;
begin
  Result := False;
  //No Asus procurar por sincro no gerenciador de arquivos para achar os arquivos de log
  //
  Log := Tabela + sLineBreak + FMX_Geral.FDT(DataHora, 2) + sLineBreak;
  //
  if not ExecutSincronismo_Upload(DataBase, QueryUpd, QueryAux, Tabela, laIns, CamposIndex, Msg, Log) then
  begin
    SalvaLog(Log);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak + '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutSincronismo_Upload(DataBase, QueryUpd, QueryAux, Tabela, laUpd, CamposIndex, Msg, Log) then
  begin
    SalvaLog(Log);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak + '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutSincronismo_Upload(DataBase, QueryUpd, QueryAux, Tabela, laDel, CamposIndex, Msg, Log) then
  begin
    SalvaLog(Log);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak + '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutaSincronismo_Download(DataBase, QueryUpd, Tabela, DataHora, laIns, Msg, Log) then
  begin
    SalvaLog(Log);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak + '-----------------------------------' + sLineBreak + sLineBreak;
  //
  if not ExecutaSincronismo_Download(DataBase, QueryUpd, Tabela, DataHora, laDel, Msg, Log) then
  begin
    SalvaLog(Log);
    Exit;
  end else
    Log := Log + sLineBreak + sLineBreak + '-----------------------------------' + sLineBreak + sLineBreak;
  //
  Result := True;
end;

end.
