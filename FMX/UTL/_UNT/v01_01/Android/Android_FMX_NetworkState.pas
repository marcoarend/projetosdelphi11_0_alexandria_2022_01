unit Android_FMX_NetworkState;

interface

type
  TCustomNetworkState = class(TObject)
    function CurrentSSID: string; virtual; abstract;
    function IsConnected: Boolean; virtual; abstract;
    function IsWifiConnected: Boolean; virtual; abstract;
    function IsMobileConnected: Boolean; virtual; abstract;
  end;

  TUnFMX_NetworkState = class(TCustomNetworkState)
  private
    FPlatformNetworkState: TCustomNetworkState;
  public
    constructor Create;
    destructor Destroy; override;
    function CurrentSSID: string;
    function IsConnected: Boolean; override;
    function IsWifiConnected: Boolean; override;
    function IsMobileConnected: Boolean; override;
  end;

implementation

uses
  Android_NetworkState;

{ TNetworkState }

constructor TUnFMX_NetworkState.Create;
begin
  inherited;
  FPlatformNetworkState := TPlatformNetworkState.Create;
end;

destructor TUnFMX_NetworkState.Destroy;
begin
  FPlatformNetworkState.Free;
  inherited;
end;

function TUnFMX_NetworkState.CurrentSSID: string;
begin
  Result := FPlatformNetworkState.CurrentSSID;
end;

function TUnFMX_NetworkState.IsConnected: Boolean;
begin
  Result := FPlatformNetworkState.IsConnected;
end;

function TUnFMX_NetworkState.IsMobileConnected: Boolean;
begin
  Result := FPlatformNetworkState.IsMobileConnected;
end;

function TUnFMX_NetworkState.IsWifiConnected: Boolean;
begin
  Result := FPlatformNetworkState.IsWifiConnected;
end;

end.
