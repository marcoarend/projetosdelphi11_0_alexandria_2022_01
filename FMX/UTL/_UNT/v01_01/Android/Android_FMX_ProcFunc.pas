unit Android_FMX_ProcFunc;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.TabControl, FMX.Edit,

  IdURI,
  Android_FMX_NetworkState,
  Androidapi.JNI.GraphicsContentViewText,
  Androidapi.JNI.App,
  Androidapi.Helpers,
  FMX.Platform.Android,
  Androidapi.JNI.JavaTypes,
  Androidapi.JNI.Net,
  Androidapi.JNI.Os,
  Android_BarcodeScanner,
  Androidapi.JNI.PowerManager,
  FMX.ScrollBox, FMX.Memo, System.IOUtils,
  // Vibrar
  Androidapi.JNIBridge,
  // Form KeyUp
  FMX.Platform, FMX.VirtualKeyboard;

type
  TAndroid_FMX_ProcFunc = class(TObject)
  private

  public
    // J A N E L A S
    procedure ManterTelaLigada(Ligada: Boolean);
    procedure OcultaFormPrincipal(Sender: TObject; var Key: Word; var KeyChar:
              Char; Shift: TShiftState);
    // L O J A
    procedure AbrirAppLoja(const AppNome: string);
    // W E B
    function  VerificaConexaoWeb(var Msg: String): Integer;
    procedure AbrirURLBrowser(const URL: string);
    // S I S T E M A
    function  AppEstaInstalado(const AppNome: string): Boolean;
    // O U T R O S
    procedure VibrarComoBotao();
  end;

var
  Android_FMX_dmkPF: TAndroid_FMX_ProcFunc;
const
  CO_App_Name_QrCode = 'com.google.zxing.client.android';

implementation

uses UnGrl_Consts;

{ TAndroid_FMX_ProcFunc }

procedure TAndroid_FMX_ProcFunc.ManterTelaLigada(Ligada: Boolean);
begin
  if Ligada = True then
    AcquireWakeLock
  else
    ReleaseWakeLock;
end;

procedure TAndroid_FMX_ProcFunc.OcultaFormPrincipal(Sender: TObject; var Key:
  Word; var KeyChar: Char; Shift: TShiftState);
var
  Intent: JIntent;
  FService: IFMXVirtualKeyboardService;
begin
  if Key = vkHardwareBack then
  begin
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
    if (FService <> nil) and (TVirtualKeyboardState.vksVisible in FService.VirtualKeyBoardState) then
    begin
      // Back button pressed, keyboard visible, so do nothing...
    end else
    begin
      // Back button pressed, keyboard not visible or not supported on this platform, lets exit the app...
      Intent := TJIntent.Create;
      Intent.setAction(TJIntent.JavaClass.ACTION_MAIN);
      Intent.addCategory(TJIntent.JavaClass.CATEGORY_HOME);
      TAndroidHelper.Activity.startActivity(Intent);
      //
      Key := 0;
    end;
  end;
(*
var
  Intent: JIntent;
begin
  if Key = vkHardwareBack then
  begin
  /
    //MainActivity.movetasktoback(true);
    //Second possibility is:
    Intent := TJIntent.Create;
    Intent.setAction(TJIntent.JavaClass.ACTION_MAIN);
    Intent.addCategory(TJIntent.JavaClass.CATEGORY_HOME);
    TAndroidHelper.Activity.startActivity(Intent);
    //
    Key := 0;
  end;
*)
end;

function TAndroid_FMX_ProcFunc.VerificaConexaoWeb(var Msg: String): Integer;
var
  NS: TUnFMX_NetworkState;
  Res: Integer;
begin
  //-1 => Falha ao verificar
  // 0 => N�o conectado
  // 1 => Conectado via WIFI
  // 2 => Conectado via WWAN
  // 3 => Conectado via Proxy
  // 4 => Conectado via Modem Busy
  // Para Android permiss�o => Access Wifi State
  Msg := '';
  Res := 0;
  NS := TUnFMX_NetworkState.Create;
  try
    if not NS.IsConnected then
      Res := 0
    else if NS.IsWifiConnected then
      Res := 1
    else if NS.IsMobileConnected then
      Res := 2
    else
      Res := -1;
    Msg := NS.CurrentSSID;
  finally
    NS.Free;
  end;
  Result := Res;
end;

procedure TAndroid_FMX_ProcFunc.VibrarComoBotao();
var
  Vibrator: JVibrator;
begin
 Vibrator := TJVibrator.Wrap((SharedActivityContext.getSystemService(TJContext.JavaClass.VIBRATOR_SERVICE) as ILocalObject).GetObjectID);
// Vibrate for x milliseconds
 Vibrator.vibrate(CO_TEMPO_VIBRA_COMO_BOTAO);
end;

procedure TAndroid_FMX_ProcFunc.AbrirAppLoja(const AppNome: string);
var
  Comando: String;
  Intent: JIntent;
begin
  Comando:= 'market://details?id='+ AppNome;
  Intent := TJIntent.Create;
  Intent.setAction(TJIntent.JavaClass.ACTION_VIEW);
  Intent.setData(TJnet_Uri.JavaClass.parse(StringToJString(Comando)));
  MainActivity.startActivity(Intent);
end;

function TAndroid_FMX_ProcFunc.AppEstaInstalado(const AppNome: string): Boolean;
var
  PackageManager: JPackageManager;
begin
  PackageManager := SharedActivity.getPackageManager;
  try
    PackageManager.getPackageInfo(StringToJString(AppNome), TJPackageManager.JavaClass.GET_ACTIVITIES);
    Result := True;
  except
    on Ex: Exception do
      Result := False;
  end;
end;

procedure TAndroid_FMX_ProcFunc.AbrirURLBrowser(const URL: string);
var
  Intent: JIntent;
begin
  if URL <> '' then
  begin
    Intent := TJIntent.Create;
    Intent.setAction(TJIntent.JavaClass.ACTION_VIEW);
    Intent.setData(StrToJURI(URL));
    SharedActivity.startActivity(Intent);
  end;
end;

end.
