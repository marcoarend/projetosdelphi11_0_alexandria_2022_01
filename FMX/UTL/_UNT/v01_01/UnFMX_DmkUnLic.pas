unit UnFMX_DmkUnLic;

interface

uses System.Classes, System.UITypes, FMX.Forms, FMX.ListBox, FMX.StdCtrls,
  FMX.Dialogs, System.SysUtils, System.Variants, IdHTTP, REST.Client, REST.Types,
  Data.DB, System.JSON;

type
  TUnFMX_DmkUnLic = class(TObject)
  private
    function ObtemStatusTexto(Status: Integer): String;
  public
    function  LiberaUso(clienteCNPJ: String): Boolean;
    procedure VerificaAtualizacaoVersao(ForcaBaixa: Boolean = False);
  end;

var
  FMX_dmkUnLic: TUnFMX_DmkUnLic;

implementation

uses UnFMX_DmkWeb, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_AllOS, MyListas,
  UnGrl_OS, UnFMX_DmkProcFunc, UnGrl_Consts;

{ TUnFMX_DmkUnLic }

function TUnFMX_DmkUnLic.LiberaUso(clienteCNPJ: String): Boolean;
var
  DeviceID, DeviceName, DvcScreenH, DvcScreenW, OSName, OSNickName,
  OSVersion, CodApp, Versao, Url,
  Res, Vencimento, AtualizadoEm, StatusTexto: String;
  StatusID, LimiteCliInt, DeviceNum, StatusCode: Integer;
  JsonObj: TJSONObject;
begin
  Result := False;

  if clienteCNPJ = EmptyStr then
  begin
    Grl_Geral.MB_Aviso('O par�metro CNPJ n�o foi informado!');
    Exit;
  end;

  Grl_AllOS.ConfiguraDadosDispositivo(DeviceID, DeviceName,
    DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion);

  CodApp := Grl_Geral.FF0(CO_DMKID_APP);
  Versao := Grl_Geral.FF0(CO_VERSAO);
  Url    := (*CO_URL*)VAR_URL + 'valida/licenca/mobile';

  Res := FMX_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN],
          [
            'cnpj', 'cod_aplic', 'versao_atu', 'device_id',
            'device_name', 'dvc_screen_h', 'dvc_screen_w', 'os_name',
            'os_nickname', 'os_version', 'cnpj_cliente'
          ],
          [
            CO_DMK_CNPJ, CodApp, Versao, DeviceID,
            DeviceName, DvcScreenH, DvcScreenW, OSName,
            OSNickName, OSVersion, clienteCNPJ
          ], Url, StatusCode, 'json');

  if StatusCode = 200 then //HTTP_OK
  begin
    JsonObj := TJSONObject.ParseJSONValue(Res) as TJSONObject;

    Vencimento   := JsonObj.Values['vencimento'].Value;
    AtualizadoEm := JsonObj.Values['atualizado_em'].Value;
    StatusID     := Grl_Geral.IMV(JsonObj.Values['status'].Value);
    StatusTexto  := ObtemStatusTexto(StatusID);
    LimiteCliInt := Grl_Geral.IMV(JsonObj.Values['limite_cliint'].Value);
    DeviceNum    := Grl_Geral.IMV(JsonObj.Values['device_num'].Value);

    //Grl_Geral.MB_Aviso('Teste OK');

    Result := True;
  end else
  begin
    if Res <> EmptyStr then
      Grl_Geral.MB_Aviso(Res);
  end;
end;

function TUnFMX_DmkUnLic.ObtemStatusTexto(Status: Integer): String;
begin
  case Status of
      0: Result := 'Bloqueado';
      1: Result := 'Monitorado';
      2: Result := 'Vencido';
      3: Result := 'Alugado';
      4: Result := 'Liberado';
    else Result := 'N�o catalogado';
  end;
end;

procedure TUnFMX_DmkUnLic.VerificaAtualizacaoVersao(ForcaBaixa: Boolean = False);
var
  Msg, DeviceID, Url, Res,
  MessageValue, VersaoSite, Arquivo, ArquivoTxt: String;
  Conexao, CodApp, Versao, StatusCode, FB,
  MessageCode: Integer;
  JsonObj: TJSONObject;
begin
  Conexao := FMX_dmkWeb.VerificaConexaoWeb(Msg);
  //
  if Conexao > 0 then //Tem internet
  begin
    CodApp   := CO_DMKID_APP;
    Versao   := CO_VERSAO;
    DeviceID := Grl_OS.ObtemIMEI_MD5;
    FB       := Grl_Geral.BoolToInt(ForcaBaixa);

    Url := VAR_URL + 'aplicativo/versao/mobile';

    Res := FMX_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN],
            [
              'cnpj', 'cod_aplic', 'versao_atu',
              'device_id', 'forca_baixa'
            ],
            [
              CO_DMK_CNPJ, Grl_Geral.FF0(CodApp), Grl_Geral.FF0(Versao),
              DeviceID, Grl_Geral.FF0(FB)
            ], Url, StatusCode, 'json');

    if StatusCode = 200 then //HTTP_OK
    begin
      JsonObj := TJSONObject.ParseJSONValue(Res) as TJSONObject;

      MessageCode  := Grl_Geral.IMV(JsonObj.Values['messageCode'].Value);
      MessageValue := JsonObj.Values['messageValue'].Value;
      VersaoSite   := JsonObj.Values['versaoSite'].Value;
      Arquivo      := JsonObj.Values['arquivo'].Value;
      ArquivoTxt   := JsonObj.Values['arquivoTxt'].Value;

      if MessageCode = 105 then
      begin
        MessageDlg('A vers�o ' + VersaoSite + ' est� dispon�vel.' +
          sLineBreak + 'Deseja atualizar?', System.UITypes.TMsgDlgType.mtConfirmation,
          [
            System.UITypes.TMsgDlgBtn.mbYes,
            System.UITypes.TMsgDlgBtn.mbNo,
            System.UITypes.TMsgDlgBtn.mbCancel
          ], 0,
          procedure(const AResult: System.UITypes.TModalResult)
          begin
            if AResult = mrYes then
            begin
               FMX_dmkPF.AbrirURLBrowser(Arquivo);
            end;
          end);
      end else
      begin
        if MessageValue <> EmptyStr then
          Grl_Geral.MB_Aviso(MessageValue);
      end;
    end else
    begin
      if Res <> EmptyStr then
        Grl_Geral.MB_Aviso(Res);
    end;
  end;
end;

end.

