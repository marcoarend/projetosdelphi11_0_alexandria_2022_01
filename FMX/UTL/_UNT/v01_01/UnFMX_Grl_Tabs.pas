unit UnFMX_Grl_Tabs;

{ Colocar no MyListas:

Uses PerfJan_Tabs;


//
function TMyListas.CriaListaTabelas(:
      UnFMX_Grl_Tabs.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnFMX_Grl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnFMX_Grl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnFMX_Grl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnFMX_Grl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnFMX_Grl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnFMX_Grl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses System.Classes, System.SysUtils, Generics.Collections, UnMyLinguas,
  UnDmkEnums;

type
  TUnFMX_Grl_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CarregaListaFRCampos(Tabela: String; FRCampos: TCampos; FLCampos:
              TList<TCampos>; var TemControle: TTemControle): Boolean;
    function  CarregaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>): Boolean;
    function  CompletaListaFRCampos(Tabela: String; FRCampos:
              TCampos; FLCampos: TList<TCampos>): Boolean;
    function  CarregaListaFRIndices(TabelaBase: String; FRIndices:
              TIndices; FLIndices: TList<TIndices>): Boolean;
  end;

var
  FMX_Grl_Tabs: TUnFMX_Grl_Tabs;

implementation

uses MyListas;


{ TUnFMX_Grl_Tabs }

function TUnFMX_Grl_Tabs.CarregaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('Controle'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnFMX_Grl_Tabs.CarregaListaFRIndices(TabelaBase: String;
  FRIndices: TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('Controle') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnFMX_Grl_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Vers�o do aplicativo';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnFMX_Grl_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
  except
    raise;
    Result := False;
  end;
end;

end.
