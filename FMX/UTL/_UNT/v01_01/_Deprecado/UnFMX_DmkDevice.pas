unit UnFMX_DmkDevice;

interface

uses FMX.Forms, FMX.ListBox, System.SysUtils, System.Classes, FMX.StdCtrls
{$IFDEF IOS}
  ,
  iOSapi.UIKit;
{$ENDIF}
{$IFDEF ANDROID}
  ,
  androidapi.Helpers,
  androidapi.JNI.JavaTypes,
  androidapi.JNI.Telephony,
  Androidapi.JNI.Provider,
  Androidapi.JNIBridge,
  Androidapi.JNI.GraphicsContentViewText,
  androidapi.JNI.Os;
{$ENDIF}
{$IFDEF MSWINDOWS}
  ,
  Winapi.Windows, UnitMD5;
{$ENDIF}

type
  {$IFDEF ANDROID}
  TAndroidVersion = (
    UNKNOWN,
    BASE,
    BASE_1_1,
    CUPCAKE,
    CUR_DEVELOPMENT,
    DONUT,
    ECLAIR,
    ECLAIR_0_1,
    ECLAIR_MR1,
    FROYO,
    GINGERBREAD,
    GINGERBREAD_MR1,
    HONEYCOMB,
    HONEYCOMB_MR1,
    HONEYCOMB_MR2,
    ICE_CREAM_SANDWICH,
    ICE_CREAM_SANDWICH_MR1,
    JELLY_BEAN,
    JELLY_BEAN_MR1,
    JELLY_BEAN_MR2,
    KITKAT,
    KITKAT_WATCH,
    LOLLIPOP,
    LOLLIPOP_MR1
  );
  {$ENDIF}
  TUnFMX_DmkDevice = class(TObject)
  private
    function  ObtemIMEI_MD5(): String;
    procedure ObtemDadosDispositivo(var OSName, OSNickName, OSVersion, TipoDispositivo: String);
  public
    procedure ConfiguraDadosDispositivo(var DeviceID, DeviceName, DvcScreenH,
              DvcScreenW, OSName, OSNickName, OSVersion: String);
  end;

var
  FMX_dmkDevice: TUnFMX_DmkDevice;
const
  CO_Msg_NImplementado = 'N�o implementado';

implementation

uses UnFMX_DmkProcFunc, UnFMX_Geral;

{ TUnAllOS_DmkProcFunc }

procedure TUnFMX_DmkDevice.ConfiguraDadosDispositivo(var DeviceID, DeviceName,
  DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion: String);
begin
  DeviceID   := ObtemIMEI_MD5;
  DvcScreenH := FMX_Geral.FF0(Screen.Size.Height);
  DvcScreenW := FMX_Geral.FF0(Screen.Size.Width);
  //
  ObtemDadosDispositivo(OSName, OSNickName, OSVersion, DeviceName);
end;

{$IFDEF MSWINDOWS}
procedure TUnFMX_DmkDevice.ObtemDadosDispositivo(var OSName, OSNickName,
  OSVersion, TipoDispositivo: String);

  function IndyComputerName(): string;
  var
    i: LongWord;
  begin
    SetLength(Result, MAX_COMPUTERNAME_LENGTH + 1);
    i := Length(Result);
    if GetComputerName(@Result[1], i) then begin
      SetLength(Result, i);
    end;
  end;

begin
  OSName          := TOSVersion.Name;
  OSNickName      := TOSVersion.ToString;
  OSVersion       := FMX_Geral.FF0(TOSVersion.Major) + '.' + FMX_Geral.FF0(TOSVersion.Minor);
  TipoDispositivo := IndyComputerName;
end;
{$ENDIF}

{$IFDEF IOS}
procedure TUnFMX_DmkDevice.ObtemDadosDispositivo(var OSName, OSNickName,
  OSVersion, TipoDispositivo: String);
var
  Device : UIDevice;
begin
  Device := TUIDevice.Wrap(TUIDevice.OCClass.currentDevice);

  OSName          := TOSVersion.Name;
  OSNickName      := Device.systemName.UTF8String;
  OSVersion       := Device.systemVersion.UTF8String;
  TipoDispositivo := Device.model.UTF8String;
end;
{$ENDIF}

{$IFDEF ANDROID}
procedure TUnFMX_DmkDevice.ObtemDadosDispositivo(var OSName, OSNickName,
  OSVersion, TipoDispositivo: String);
(*
const
  Teste: array[TOSVersion.TPlatform] of string = ('Windows', 'MacOS', 'iOS', 'Android', 'WinRT', 'Linux');
*)
var
  codename: string;
  version: TAndroidVersion;
begin
  codename        := 'Unknown';
  version         := TAndroidVersion(TJBuild_VERSION.JavaClass.SDK_INT);
  TipoDispositivo := JStringToString(TJBuild.JavaClass.MODEL);

  case version of
    UNKNOWN:
      codename := 'Unknown';
    BASE, BASE_1_1:
      codename := 'Base';
    CUPCAKE:
      codename := 'Cupcake';
    CUR_DEVELOPMENT:
      codename := 'Curent Development';
    DONUT:
      codename := 'Donut';
    ECLAIR, ECLAIR_0_1, ECLAIR_MR1:
      codename := 'Eclair';
    FROYO:
      codename := 'Froyo';
    GINGERBREAD, GINGERBREAD_MR1:
      codename := 'Gingerbread';
    HONEYCOMB, HONEYCOMB_MR1, HONEYCOMB_MR2:
      codename := 'Honeycomb';
    ICE_CREAM_SANDWICH, ICE_CREAM_SANDWICH_MR1:
      codename := 'Ice Cream Sandwich';
    JELLY_BEAN, JELLY_BEAN_MR1, JELLY_BEAN_MR2:
      codename := 'Jelly Bean';
    KITKAT, KITKAT_WATCH:
      codename := 'KitKat';
    LOLLIPOP, LOLLIPOP_MR1:
      codename := 'Lollipop';
  end;
  OSName     := TOSVersion.Name;
  OSNickName := codename;
  OSVersion  := JStringToString(TJBuild_VERSION.JavaClass.RELEASE);
end;
{$ENDIF}

{$IFDEF ANDROID}
function TUnFMX_DmkDevice.ObtemIMEI_MD5(): String;
var
  Obj: JObject;
  TM: JTelephonyManager;
  Identifier: String;
begin
  Obj := SharedActivityContext.getSystemService(TJContext.JavaClass.TELEPHONY_SERVICE);
  if Obj <> nil then
  begin
    TM := TJTelephonyManager.Wrap( (Obj as ILocalObject).GetObjectID );
    if TM <> nil then
      Identifier := JStringToString(TM.getDeviceId);
  end;
  if Identifier = '' then
    Identifier := JStringToString(TJSettings_Secure.JavaClass.getString(
      SharedActivity.getContentResolver, TJSettings_Secure.JavaClass.ANDROID_ID));
  //
  Result := Identifier;
end;
{$ENDIF}

{$IF DEFINED (MSWINDOWS)}
function TUnFMX_DmkDevice.ObtemIMEI_MD5(): String;
  function CalculaValSerialKey2(DataVerifi: String; Web: Boolean): String;
    function SerialNum(FDrive:String): String;
    var
      nVNameSer: PDWORD;
      pVolName: PChar;
      FSSysFlags, maxCmpLen: DWord;
      pFSBuf: PChar;
      drv: String;
    begin
      try
        drv := FDrive + '\';

        GetMem(pVolName, MAX_PATH);
        GetMem(pFSBuf, MAX_PATH);
        GetMem(nVNameSer, MAX_PATH);

        GetVolumeInformation(PChar(drv), pVolName, MAX_PATH, nVNameSer,
          maxCmpLen, FSSysFlags, pFSBuf, MAX_PATH);

        Result := FMX_Geral.FF0(nVNameSer^);

        FreeMem(pVolName, MAX_PATH);
        FreeMem(pFSBuf, MAX_PATH);
        FreeMem(nVNameSer, MAX_PATH);
      except
        Result := '';
      end;
    end;

    function MacAddress2: String;
    var
      a,b,c,d: LongWord;
      CPUID: String;
    begin
      asm
        push EAX
        push EBX
        push ECX
        push EDX

        mov eax, 1
        db $0F, $A2
        mov a, EAX
        mov b, EBX
        mov c, ECX
        mov d, EDX

        pop EDX
        pop ECX
        pop EBX
        pop EAX

        {
        mov eax, 1
        db $0F, $A2
        mov a, EAX
        mov b, EBX
        mov c, ECX
        mov d, EDX
        }
      end;
      CPUID  := IntToHex(a,8);// + inttohex(b,8) + inttohex(c,8) + inttohex(d,8);
      Result := CPUID;
    end;
  var
    Unidade, HD, MAC: String;
  begin
    //SerialKey = N�mero do HD + N�mero do CPU + Data da �ltima verifica��o
    //
    Unidade := ExtractFileDrive(ParamStr(0));//ExtractFileDrive(Application.ExeName);
    HD      := SerialNum(Unidade);
    MAC     := MacAddress2;
    //
    if Web then
      Result := UnMD5.StrMD5(HD + MAC)
    else
      Result := UnMD5.StrMD5(HD + MAC + DataVerifi);
  end;
begin
  Result := CalculaValSerialKey2('', True);
end;
{$ENDIF}

{$IFDEF IOS}
function TUnFMX_DmkDevice.ObtemIMEI_MD5: String;
begin
  Result := CO_Msg_NImplementado;
end;
{$ENDIF}

end.
