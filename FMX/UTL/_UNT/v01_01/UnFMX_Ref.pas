unit UnFMX_Ref;

interface

uses
{$IFDEF MSWINDOWS}
  Winapi.Windows,
{$ENDIF}
  System.SysUtils, System.UITypes, System.Variants, System.IOUtils,
  System.Classes, FMX.Forms, FMX.Dialogs, FMX.Edit, System.MaskUtils,
  Data.DB,
  System.Types, UnDmkEnums, TypInfo,
  UnGrl_Geral;
{
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, Variants,
  UnInternalConsts2, ComCtrls, Registry, dmkEdit, dmkEditCB, dmkGeral,
  dmkEditDateTimePicker, dmkDBLookupCombobox, dmkCheckBox, dmkRadioGroup,
  dmkDBEdit, dmkValUsu, dmkCheckGroup, dmkPopOutFntCBox, dmkMemo, dmkVariable,
  dmkCompoStore, dmkRichEdit, UnDmkProcFunc, UnDmkEnums, dmkImage, Grids,
  DBGrids, DmkDBGridZTO, DmkDBGrid;
}

type
  TUnFMX_Ref = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  GET_Compo_Val(var Form: TForm; const Nome, Propriedade: String;
              const Classe: TClass; var Valor: Variant): Boolean;
  end;

var
  FMX_Ref: TUnFMX_Ref;

  implementation

{ TUnFMX_Ref }

{$IFDEF ANDROID}
uses
  UnFMX_Geral;
{$ENDIF}


{ TUnFMX_Ref }

function TUnFMX_Ref.GET_Compo_Val(var Form: TForm; const Nome,
  Propriedade: String; const Classe: TClass; var Valor: Variant): Boolean;
var
  Objeto: TObject;
  PropInfo: PPropInfo;
begin
  Result := False;
  if (Form.FindComponent(Nome) as Classe) <> nil then
    Objeto := Form.FindComponent(Nome) as Classe
  else begin
    Grl_Geral.MB_Erro('O componente ' + Nome +
    ' n�o existe ou n�o pode ser setado!' + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  if Objeto <> nil then
  begin
    PropInfo := GetPropInfo(Objeto.ClassInfo, Propriedade);
    if PropInfo <> nil then
    begin
      try
        Valor := GetPropValue(Objeto, Propriedade, Valor);
        Result := True;
      except
        Grl_Geral.MB_Erro('A propriedade ' + Propriedade +
        ' n�o pode ser setada no componente ' + TComponent(Objeto).Name + '.' +
        sLineBreak + 'AVISE A DERMATEK!');
      end;
    end else
      Grl_Geral.MB_Erro('O componente ' + TComponent(Objeto).Name +
      ' n�o possui a propriedade ' + Propriedade + '.' + sLineBreak +
      'AVISE A DERMATEK!');
  end;
end;

end.
