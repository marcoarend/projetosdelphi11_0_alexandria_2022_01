unit UnFMX_MyObjects;

interface

uses System.Classes, System.UITypes, FMX.Controls, FMX.Forms, FMX.ListBox,
  FMX.StdCtrls, FMX.Dialogs, FMX.Types, FMX.Layouts, System.SysUtils,
  System.Variants, System.Rtti, System.Types, System.DateUtils, FMX.Graphics,
  FMX.Edit,
  // MultiDetail
  FMX.ListView.Types, FMX.ListView.Appearances, MultiDetailAppearanceU,
  // Fim MultiDetail
  {$IF DEFINED(ANDROID)}
  Android_FMX_ProcFunc,
  {$ELSE}
  Windows_FMX_ProcFunc,
  {$ENDIF}
  Data.Bind.Components, System.Notification, System.Math, FMX.Surfaces,
  System.IOUtils, FMX.Grid, UnGrl_Vars,
  // Form KeyUp
  FMX.Platform, FMX.VirtualKeyboard, FMX.Objects,
  UnGrl_Consts;

type
  TUnFMX_DmkProcFunc = class(TObject)
  private
    {Private declarations}
  public
    {Public declarations}
  end;

var
  FMX_dmkPF: TUnFMX_DmkProcFunc;

implementation

uses UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums;

end.
