unit Upload;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView;

type
  TFmUpload = class(TForm)
    ToolBar1: TToolBar;
    SBCamera: TSpeedButton;
    SBSaida: TSpeedButton;
    LVItems: TListView;
    procedure SBSaidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBCameraClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetItems();
    function  UploadFile(Name, Base64: String): String;
  public
    { Public declarations }
    FDirectory: String;
  end;

var
  FmUpload: TFmUpload;

implementation

uses UnGrl_Geral, UnGrl_IO, UnGrl_DmkWeb, UnGrl_Components;

{$R *.fmx}

procedure TFmUpload.FormShow(Sender: TObject);
begin
  SetItems();
end;

procedure TFmUpload.SBCameraClick(Sender: TObject);
var
  I: Integer;
  Count: Double;
  Item: TListViewItem;
  Nome, Base64, Res: String;
begin
  Count := Grl_Components.ListViewCount(LVItems);
  //
  for I := 0 to Trunc(Count) - 1 do
  begin
    Item := LVItems.Items[I];
    //
    Nome   := Item.Text;
    Base64 := Grl_IO.FileToBase64(Nome);
    //
    Res := UploadFile(ExtractFileName(Nome), Base64);
    //
    if Res <> '' then
      Item.Detail := Res
    else
      Grl_Components.ListViewRemove(LVItems, I);
  end;
  //
  Grl_Geral.MB_Aviso('Upload finalizado com sucesso!');
end;

procedure TFmUpload.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUpload.SetItems;
var
  I: Integer;
  Path: String;
  List: TStringList;
begin
  Path := Grl_IO.GetApplicationPath(Application.Title, FDirectory);
  //
  List := Grl_IO.GetFilesFromPath(Path);
  //
  Grl_Components.ListViewClear(LVItems);
  //
  if List.Count > 0 then
  begin
    for I := 0 to List.Count - 1 do
      Grl_Components.ListViewAdd(LVItems, List.Strings[I]);
  end;
end;

function TFmUpload.UploadFile(Name, Base64: String): String;
var
  Url, Res: String;
  StatusCode: Integer;
begin
  Url := CO_URL + 'overseer/upload/arquivo';
  Res := Grl_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN],
          [
            'cnpj', 'ambiente', 'nome', 'base64'
          ],
          [
            '02582267000160', '1', Name, Base64
          ], Url, StatusCode, 'json');

  if StatusCode = 200 then //HTTP_OK
    Result := ''
  else
    Result := Res;
end;

end.
