unit DmkDialog2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo;

type
  TFmDmkDialog2 = class(TForm)
    Panel1: TPanel;
    BtYES: TButton;
    BtNO: TButton;
    MePergunta: TMemo;
    procedure BtNOClick(Sender: TObject);
    procedure BtYESClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FResult: TModalResult;
  end;

var
  FmDmkDialog2: TFmDmkDialog2;

implementation

uses UnGrl_Vars;

{$R *.fmx}

procedure TFmDmkDialog2.BtNOClick(Sender: TObject);
begin
  FResult := System.UITypes.mrNo;
  VAR_MODAL_RESULT := System.UITypes.mrNo;
  Close;
end;

procedure TFmDmkDialog2.BtYESClick(Sender: TObject);
begin
  FResult := System.UITypes.mrYes;
  VAR_MODAL_RESULT := System.UITypes.mrYes;
  Close;
end;

procedure TFmDmkDialog2.FormCreate(Sender: TObject);
begin
  MePergunta.Text := VAR_MODAL_MESSAGE;
end;

end.
