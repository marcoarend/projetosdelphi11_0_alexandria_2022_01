unit DmkDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo;

type
  TFmDmkDialog = class(TForm)
    Panel1: TPanel;
    BtYES: TButton;
    BtNO: TButton;
    MeTexto: TMemo;
    BtOK: TButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    procedure BtNOClick(Sender: TObject);
    procedure BtYESClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FResult: TModalResult;
  end;

var
  FmDmkDialog: TFmDmkDialog;

implementation

uses UnGrl_Vars;

{$R *.fmx}

procedure TFmDmkDialog.BtNOClick(Sender: TObject);
begin
  FResult := System.UITypes.mrNo;
  VAR_MODAL_RESULT := System.UITypes.mrNo;
  Close;
end;

procedure TFmDmkDialog.BtYESClick(Sender: TObject);
begin
  FResult := System.UITypes.mrYes;
  VAR_MODAL_RESULT := System.UITypes.mrYes;
  Close;
end;

procedure TFmDmkDialog.FormCreate(Sender: TObject);
begin
  BtYes.Visible := False;
  BtNo.Visible := False;
  BtOK.Visible := False;
end;

end.
