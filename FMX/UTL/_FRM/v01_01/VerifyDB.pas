unit VerifyDB;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  {$IFDEF ANDROID} androidapi.Helpers, {$ENDIF}
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.ListBox,
  FMX.ScrollBox, FMX.Memo, FireDAC.Comp.Client,
  System.Generics.Collections, UnDmkEnums, UnMyLinguas;

type
  TFmVerifyDB = class(TForm)
    ListBox1: TListBox;
    ToolBar1: TToolBar;
    SBVerifica: TSpeedButton;
    SBSaida: TSpeedButton;
    LBIAvisoR1: TListBoxItem;
    LBIAvisoG1: TListBoxItem;
    LBIAvisoB1: TListBoxItem;
    LBIAvisoP1: TListBoxItem;
    LBITempoI: TListBoxItem;
    LBITempoF: TListBoxItem;
    LBITempoT: TListBoxItem;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    Memo1: TMemo;
    MeFldsNoNeed: TMemo;
    MeTabsNoNeed: TMemo;
    PB1: TProgressBar;
    Label1: TLabel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    ListBoxItem4: TListBoxItem;
    LBTabToRecriar: TListBox;
    PnTabToRecriar: TPanel;
    BtTabToRecriar: TButton;
    procedure SBSaidaClick(Sender: TObject);
    procedure SBVerificaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LBTabToRecriarClick(Sender: TObject);
    procedure LBTabToRecriarExit(Sender: TObject);
    procedure BtTabToRecriarClick(Sender: TObject);
  private
    { Private declarations }
    FVerificou: Boolean;
    function  CopiaTabelaSQLite(TabSrc, TabDst: String): Boolean;
    procedure VerificaBD();
  public
    { Public declarations }
    FVerifica: Boolean;
    FDataBaseAll, FDataBaseUsu: TFDConnection;
    //
    procedure VerificaBDSemMostrar();
  end;

var
  FmVerifyDB: TFmVerifyDB;

implementation

uses UnGrl_DmkDB, Module, MyListas, UnFMX_DmkProcFunc, UnFMX_Geral, Principal,
  UnFMX_CfgDBApp, UnFMX_Grl_Vars, UnGrl_Geral;

{$R *.fmx}

procedure TFmVerifyDB.BtTabToRecriarClick(Sender: TObject);
var
  FTabelas: TList<TTabelas>;
  DefTabela: TTabelas;
  BaseDB, DBNome, TabNome, TabBase(*, Item*): String;
  Tem_Del, Tem_Sync: Boolean;
  Achow, Continua: Boolean;
  //
  function RecriaTabelaCamposAlterados(DBtoAlt: TFDConnection): Boolean;
  var
    NewTabNome: String;
  begin
    if Lowercase(DBtoAlt.Name) = Lowercase('MyDB') then
      DBNome := VAR_APP_DBUsuarioNome
    else
      DBNome := CO_DBNome;
    //
    NewTabNome := '_TabelaEmReestruturacao_' + TabNome;
    Grl_DmkDB.CriaTabela_SQLite(DBNome, (*TabNome*)NewTabNome, TabBase, Memo1,
    actCreate, Tem_Del,
    //LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
    LBIAvisoB1, nil, LBIAvisoG1, nil,
    DBtoAlt, Tem_Sync);
    //
    if CopiaTabelaSQLite(TabNome, NewTabNome) then
    begin
      DBtoAlt.ExecSQL(Grl_Geral.ATS([
      'BEGIN TRANSACTION; ',
      (*
      'CREATE TABLE t1_new ( ',
      '  foo  TEXT PRIMARY KEY, ',
      '  bar  TEXT, ',
      '  baz  INTEGER ',
      '); ',
      ' ',
      'INSERT INTO t1_new SELECT foo, bar, baz FROM t1; ',
      *)
      'DROP TABLE ' + TabNome + '; ',
      'ALTER TABLE ' + NewTabNome + ' RENAME TO ' + TabNome + '; ',
      'COMMIT; ',
      EmptyStr]));
    //
{ Exemplo de exclus�o de campo:
BEGIN TRANSACTION;
CREATE TABLE t1_new (
  foo  TEXT PRIMARY KEY,
  bar  TEXT,
  baz  INTEGER
);

INSERT INTO t1_new SELECT foo, bar, baz FROM t1;
DROP TABLE t1;
ALTER TABLE t1_new RENAME TO t1;
COMMIT;
}
    end;
  end;
var
    I: Integer;
begin
  BtTabToRecriar.Enabled := False;
  FTabelas := TList<TTabelas>.Create;
      {$IfDef DMK_FMX}
  MyList.CriaListaTabelas(FTabelas);
      {$Else}
  MyList.CriaListaTabelas(DBName, FTabelas);
      {$EndIf}
  //
  TabNome := LBTabToRecriar.Items[LBTabToRecriar.ItemIndex];
  TabBase := '';
  Achow   := False;
  for I := 0 to FTabelas.Count -1 do
  begin
    DefTabela := FTabelas[i];
    //
    if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
    begin
      TabBase  := DefTabela.TabBase;
      Tem_Del  := DefTabela.Tem_Del;
      Tem_Sync := DefTabela.Tem_Sync;
      Achow    := True;
      Break;
    end;
  end;
  //
  if Achow then
  begin
    if TabBase = EmptyStr then
      TabBase := TabNome;
    if FDataBaseAll <> nil then
    begin
      Continua := RecriaTabelaCamposAlterados(FDataBaseAll);
    end;

    if FDataBaseUsu <> nil then
    begin
      Continua :=RecriaTabelaCamposAlterados(FDataBaseUsu);
    end;
  end;
end;

function TFmVerifyDB.CopiaTabelaSQLite(TabSrc, TabDst: String): Boolean;
var
  //Campos, SQL, JSON, _Alias, MyAlias
  Lit: String;
begin
  Result := False;
  //Campos := '';
{
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
}
  //
  Grl_DmkDB.AbreSQLQuery0(Dmod.FDPragma, Dmod.AllDB, [
    //'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    'PRAGMA table_info(' + Lowercase(TabDst) + ')',
    ''
  ]);
  if Dmod.FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := Dmod.FDPragma.FieldByName('Name').AsString + sLineBreak;
  //SQL := MyAlias + Dmod.FDPragma.FieldByName('Name').AsString;
  Lit := Dmod.FDPragma.FieldByName('Name').AsString;
  Dmod.FDPragma.Next;
  while not Dmod.FDPragma.Eof do
  begin
    //SQL := SQL + ', ' + MyAlias + Dmod.FDPragma.FieldByName('Name').AsString;
    Lit := Lit + ', ' + Dmod.FDPragma.FieldByName('Name').AsString;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    Dmod.FDPragma.Next;
  end;
  //
  //FMX_Geral.MB_Info(SQL);
  //FMX_Geral.MB_Info(Lit);
{
  SQL :=
    'SELECT ' + SQL + sLineBreak +
    'FROM ' + TabSrc + _Alias + sLineBreak +
    SQL_JOIN + sLineBreak +
    SQL_WHERE + '';
}
  //
  //Lit := INSERT INTO t1_new SELECT foo, bar, baz FROM t1;
  Lit := 'INSERT INTO ' + TabDst + ' SELECT ' + Lit + ' FROM ' + TabSrc + ';';
  Grl_Geral.MB_Aviso(Lit);
  Grl_DmkDB.ExecutaSQLQuery0(Dmod.FDPragma, Dmod.AllDB, [Lit]);
  Result := True;
end;

procedure TFmVerifyDB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (not FVerificou) and (Dmod.QrControle.Active = False) then
    Application.Terminate;
end;

procedure TFmVerifyDB.FormCreate(Sender: TObject);
begin
  BringToFront;
  //
  FDataBaseUsu := nil;
  FDataBaseAll := nil;
  FVerifica    := False;
  FVerificou   := False;
  //
  LBIAvisoR1.Text := '...';
  LBIAvisoG1.Text := '...';
  LBIAvisoB1.Text := '...';
  LBIAvisoP1.Text := '...';
  LBITempoI.Text  := '...';
  LBITempoF.Text  := '...';
  LBITempoT.Text  := '...';
  //
  Memo1.Text        := '';
  MeFldsNoNeed.Text := '';
  MeTabsNoNeed.Text := '';
end;

procedure TFmVerifyDB.FormShow(Sender: TObject);
begin
  if FVerifica then
    VerificaBD;
end;

procedure TFmVerifyDB.LBTabToRecriarClick(Sender: TObject);
begin
  if LBTabToRecriar.Selected <> nil then
    BtTabToRecriar.Enabled := True;
end;

procedure TFmVerifyDB.LBTabToRecriarExit(Sender: TObject);
begin
  //BtTabToRecriar.Enabled := False;
end;

procedure TFmVerifyDB.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifyDB.SBVerificaClick(Sender: TObject);
begin
  VerificaBD();
end;

procedure TFmVerifyDB.VerificaBD;

  function VerificaDB(DataBase: TFDConnection): Boolean;
  var
    DBNome: String;
  begin
    if DataBase.Name = 'MyDB' then
      DBNome := VAR_APP_DBUsuarioNome
    else
      DBNome := CO_DBNome;
    //
    Result := Grl_DmkDB.EfetuaVerificacoes(DBNome, Memo1,
                True(*CkEstrutura.Checked*), True(*CkEstrutLoc.Checked*), True,
                False(*CkPergunta.Checked*), True(*CkRegObrigat.Checked*),
                LBIAvisoP1, nil, LBIAvisoR1, nil, LBIAvisoB1, nil, LBIAvisoG1, nil,
                LBITempoI, LBITempoF, LBITempoT, MeTabsNoNeed, PB1, MeFldsNoNeed,
                stMobile, istSQLite, DataBase, CO_VERSAO, LBTabToRecriar);
  end;

var
  Versao, Resp: Integer;
  Continua, Verifica: Boolean;
begin
  Continua := False;
  if (FDataBaseAll = nil) and (FDataBaseUsu = nil) then
  begin
    FMX_Geral.MB_Erro('Banco de dados n�o definido!');
    Exit;
  end;

  Memo1.Lines.Clear;
  LBTabToRecriar.Items.Clear;

  Verifica := True;
  Versao   := Grl_DmkDB.ObtemVersaoAppDB(Dmod.QrLoc, Dmod.AllDB, istSQLite, stMobile);

  if Versao <= CO_VERSAO then
  begin
    Verifica := True;
  end else
  begin
    Resp := FMX_Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
              'banco de dados atual. N�o � recomendado executar a verifica��o com '+
              'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
              'Confirma assim mesmo a verifica��o?');
    if Resp = mrYes then
    begin
      Verifica := True;
    end else
    begin
      Verifica := False;
    end;
  end;

  if Verifica then
  begin
    Application.ProcessMessages;

    if FDataBaseAll <> nil then
    begin
      Continua := VerificaDB(FDataBaseAll);
    end;

    if FDataBaseUsu <> nil then
    begin
      Continua := VerificaDB(FDataBaseUsu);
    end;
    if Continua = True then
    begin
      FMX_CfgDBApp.ConfiguraDispositivo(FmPrincipal.LaUsuario, Dmod.AllDB,
        Dmod.MyDB, Dmod.QrUsuarios, Dmod.QrLoc, Dmod.QrUpd, Dmod.QrControle,
        Dmod.QrEspecificos);
      //
      FVerificou := True;
      //
      //Close;
    end;
  end;
  //Application.Terminate;
end;

procedure TFmVerifyDB.VerificaBDSemMostrar();
begin
  VerificaBD();
end;

end.

