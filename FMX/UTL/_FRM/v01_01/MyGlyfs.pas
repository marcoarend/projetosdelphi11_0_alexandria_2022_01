unit MyGlyfs;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.ImageList, FMX.ImgList, FMX.Objects, FMX.Surfaces;

type
  TFmMyGlyfs = class(TForm)
    Img42X42: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CarregaImagemUsuario(ImgUsuario: TImage);
  end;

var
  FmMyGlyfs: TFmMyGlyfs;

implementation

uses UnFMX_DmkProcFunc;

{$R *.fmx}

{ TFmMyGlyfs }

procedure TFmMyGlyfs.CarregaImagemUsuario(ImgUsuario: TImage);
var
  Img: TBitmap;
  Tam: TSizeF;
begin
  Tam.cx := ImgUsuario.Width;
  Tam.cy := ImgUsuario.Height;
  //
  Img := TBitmap.Create;
  try
    Img := Img42X42.Bitmap(Tam, 0);
    //
    if Img <> nil then
      ImgUsuario.Bitmap := FMX_dmkPF.CriarFotoArredondada(Img);
  finally
    ;
  end;
end;

end.
