unit PesqCod;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.DateUtils, System.Rtti, System.Bindings.Outputs, FMX.Types, FMX.Controls,
  FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.ScrollBox, FMX.Memo, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListBox, FMX.Layouts,
  FMX.ListView, Fmx.Bind.Editors, FireDAC.Comp.Client, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Stan.StorageXML, Data.DB, Data.Bind.Components,
  Data.Bind.ObjectScope, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.DBScope;

type
  TFmPesqCod = class(TForm)
    Label1: TLabel;
    ToolBar2: TToolBar;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    LVPesq: TListView;
    QrPesq: TFDQuery;
    QrPesqCodigo: TIntegerField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    QrPesqNome: TStringField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    procedure SpeedButton1Click(Sender: TObject);
    procedure LVPesqItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCod, FIdx: Integer;
    FTxt: String;
    procedure ReopenPesq();
  public
    { Public declarations }
    FSQL: array of String;
    FCompo: TObject;
    FCodigo: Integer;
  end;

var
  FmPesqCod: TFmPesqCod;

implementation

uses UnFMX_DmkWeb, UnGrl_DmkDB, UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums,
{CE
, Module
fim CE}
  UnFMX_DmkProcFunc, MyListas;

{$R *.fmx}

procedure TFmPesqCod.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (FCompo <> nil) and (FCompo is TEdit) then
  begin
    TEdit(FCompo).Text := FTxt;
    TEdit(FCompo).Tag  := FCod;
  end else
  if (FCompo <> nil) and (FCompo is TComboBox) then
  begin
    TComboBox(FCompo).ItemIndex := FIdx;
  end;
end;

procedure TFmPesqCod.FormShow(Sender: TObject);
begin
  ReopenPesq();
  //
  FCod := FCodigo;
  FIdx := -1;
  FTxt := '';
  //
  if FCod <> 0 then
    LVPesq.ItemIndex := Grl_DmkDB.ObtemIndexDeTabela(QrPesq, 'Codigo', FCod);
end;

procedure TFmPesqCod.LVPesqItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  FCod := FMX_dmkPF.GetSelectedValue(LVPesq);
  FTxt := AItem.Text;
  FIdx := LVPesq.ItemIndex;
  //
  Close;
end;

procedure TFmPesqCod.ReopenPesq;
begin
{CE
  Grl_DmkDB.AbreSQLQuery0(QrPesq, Dmod.MyDB, FSQL);
  //
  if QrPesq.RecordCount = 0 then
  begin
    QrPesq.Close;
    FMX_Geral.MB_Aviso('N�o foi localizado nenhum registro!');
  end;
fim CE}
end;

procedure TFmPesqCod.SpeedButton1Click(Sender: TObject);
begin
  Close;
end;

end.









