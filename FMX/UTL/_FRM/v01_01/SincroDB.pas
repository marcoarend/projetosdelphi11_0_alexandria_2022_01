unit SincroDB;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Notification, System.DateUtils, FMX.Types, FMX.Controls, FMX.Forms,
  FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.ScrollBox, FMX.Memo, FMX.ListBox, FMX.Layouts, FireDAC.Comp.Client,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.Comp.DataSet, Data.DB, Data.Bind.Components,
  Data.Bind.ObjectScope, Web.HTTPApp, Datasnap.DBClient, IdURI, IdHTTP,
  IPPeerClient, REST.Client, REST.Types, Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc,
  Xml.omnixmldom, UnGrl_Vars, UnGrl_DmkDB;

type
  TFmSincroDB = class(TForm)
    Label1: TLabel;
    ToolBar1: TToolBar;
    SBSincro: TSpeedButton;
    SBSaida: TSpeedButton;
    PnDados: TPanel;
    PB1: TProgressBar;
    QrSinc: TFDQuery;
    QrUpd: TFDQuery;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    RESTClient1: TRESTClient;
    LaAviso: TLabel;
    Panel1: TPanel;
    CkTabelasAux: TCheckBox;
    AI1: TAniIndicator;
    NotificationCenter1: TNotificationCenter;
    procedure SBSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBSincroClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure NotifyComplete(Msg: String);
    procedure ResetRESTComponentsToDefaults;
    //Tabelas Aux
    function  ExecutaSincronismo_TabelasAux(Tabela: String; var Msg: String): Boolean;
    function  VerificaTabelasAuxEstaVazio(): Boolean;
    procedure LimpaTabelasAux();
  public
    { Public declarations }
  end;

var
  FmSincroDB: TFmSincroDB;

implementation

uses UnFMX_DmkWeb, MyListas, UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums, Module,
  UnFMX_DmkProcFunc, UnFMX_DmkSincro;

{$R *.fmx}

procedure TFmSincroDB.LimpaTabelasAux;

  procedure ExcluiItensTabela(Tabela: String);
  begin
    Grl_DmkDB.ExecutaSQLQuery0(QrUpd, Dmod.MyDB, [
      'DELETE FROM ' + Tabela,
      '']);
  end;

begin
  ExcluiItensTabela('bacen_pais');
  ExcluiItensTabela('dtb_munici');
  ExcluiItensTabela('listalograd');
  ExcluiItensTabela('ufs');
end;

function TFmSincroDB.ExecutaSincronismo_TabelasAux(Tabela: String;
  var Msg: String): Boolean;
var
  Res, SQL, SQLWhere, SQLCampos, SQLValores: String;
  i, j, MsgCod: Integer;
  Param: TRESTRequestParameter;
  XMLArqRes: IXMLDocument;
  ListaCampos, ListaValores: TStringList;
begin
  Result := False;
  Msg    := '';
  //
  Res := FMX_dmkWeb.REST_URL_Post(
    ['AplicativoSolicitante', 'IDUsuarioSolicitante', 'Token'],
    [FMX_Geral.FF0(CO_DMKID_APP), FMX_Geral.FF0(VAR_WEB_USR_ID), VAR_WEB_USR_TOKEN],
    ['method', 'SQL'],
    ['REST_dmkSinc_ExecutaSelect', 'SELECT * FROM ' + Tabela], CO_URL_DMKAPI);
  //
  if Res <> '' then
  begin
    XMLArqRes := TXMLDocument.Create(nil);
    XMLArqRes.Active := False;
    try
      if Pos('MSGCOD', UpperCase(Res)) > 0 then
      begin
        XMLArqRes.LoadFromXML(Res);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLArqRes.DocumentElement.ChildNodes[i] do
          begin
            MsgCod := FMX_Geral.IMV(ChildNodes['MsgCod'].Text);

            if MsgCod <> 202 then //Sem registros para retornar
              Msg := ChildNodes['MsgTxt'].Text;
          end;
        end;
      end else
      if Pos('XML', UpperCase(Res)) > 0 then
      begin
        XMLArqRes.LoadFromXML(Res);
        XMLArqRes.Encoding := 'ISO-10646-UCS-2';
        //
        for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLArqRes.DocumentElement.ChildNodes[i] do
          begin
            if i = 0 then
            begin
              SQLCampos  := '';
              //
              for j := 0 to ChildNodes.Count - 1 do
              begin
                if j = ChildNodes.Count - 1 then
                  SQLCampos := SQLCampos + ChildNodes[j].Text
                else
                  SQLCampos := SQLCampos + ChildNodes[j].Text + ',';
              end;
            end else
            begin
              if SQLCampos <> '' then
              begin
                SQLValores := '';
                //
                for j := 0 to ChildNodes.Count - 1 do
                begin
                  if j = ChildNodes.Count - 1 then
                    SQLValores := SQLValores + ChildNodes[j].Text
                  else
                    SQLValores := SQLValores + ChildNodes[j].Text + ',';
                end;
                //SQLValores := Utf8ToAnsi(SQLValores);
                SQL        := 'INSERT OR REPLACE INTO ' + Tabela + ' (' + SQLCampos + ') VALUES (' + SQLValores + ')';
                //
                Grl_DmkDB.ExecutaSQLQuery0(QrUpd, Dmod.MyDB, [SQL]);
              end else
              begin
                Msg := 'O XML retornado � inv�lido!';
                Exit;
              end;
            end;
          end;
        end;
      end else
        Msg := 'Falha ao fazer requisi��o!';
    finally
      ;
    end;
  end;
  if Msg = '' then
  begin
    Result := True;
  end;
end;

procedure TFmSincroDB.FormCreate(Sender: TObject);
begin
  PB1.Visible     := False;
  AI1.Visible     := False;
  AI1.Enabled     := False;
  LaAviso.Visible := False;
end;

procedure TFmSincroDB.FormShow(Sender: TObject);
begin
  if CkTabelasAux.Visible then
    CkTabelasAux.IsChecked := VerificaTabelasAuxEstaVazio()
  else
    CkTabelasAux.IsChecked := False;
end;

procedure TFmSincroDB.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSincroDB.SBSincroClick(Sender: TObject);
var
  Conexao, Resp: Integer;
  Msg: String;
begin
  Conexao := FMX_dmkPF.VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    FMX_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end else
  if Conexao = 2 then
  begin
    Resp := FMX_Geral.MB_Pergunta('Seu dispositivo est� conectado na internet atrav�s de dados m�veis!' +
      sLineBreak + 'Deseja continuar?');

    if Resp <> mrYes then
      Exit;
  end;
  case CO_DMKID_APP of
    41: //InfrajobMobile
    begin
      TThread.CreateAnonymousThread(procedure ()
        const
          CO_TabelasAux: Integer = 4;
          CO_Tabelas: Integer = 8;
          CO_Processos: Integer = 5; 
        var
          DataHora: TDateTime;
          Msg: String;
        begin
          try
            TThread.Synchronize (TThread.CurrentThread,
            procedure ()
            begin
              DataHora := FMX_dmkPF.DateTime_MyTimeZoneToUTC(Now);
              //
              PB1.Max     := CO_Tabelas;
              PB1.Value   := 0;
              PB1.Visible := True;
              //
              if CkTabelasAux.IsChecked = True then
                PB1.Max := PB1.Max + CO_TabelasAux;
              //
              AI1.Enabled     := True;
              AI1.Visible     := True;
              LaAviso.Text    := 'Aguarde...';
              LaAviso.Visible := True;
            end);
            TThread.Synchronize (TThread.CurrentThread,
            procedure ()
            begin
              if CkTabelasAux.IsChecked = True then
              begin
                LimpaTabelasAux();
                //
                if not ExecutaSincronismo_TabelasAux('bacen_pais', Msg) then Exit;
                LaAviso.Text := 'Sincronizando tabela bacen_pais';
                PB1.Value := PB1.Value + 1;
                Application.ProcessMessages;
                AI1.UpdateEffects;
                //
                if not ExecutaSincronismo_TabelasAux('dtb_munici', Msg) then Exit;
                LaAviso.Text := 'Sincronizando tabela dtb_munici';
                PB1.Value := PB1.Value + 1;
                Application.ProcessMessages;
                AI1.UpdateEffects;
                //
                if not ExecutaSincronismo_TabelasAux('listalograd', Msg) then Exit;
                LaAviso.Text := 'Sincronizando tabela listalograd';
                PB1.Value := PB1.Value + 1;
                Application.ProcessMessages;
                AI1.UpdateEffects;
                //
                if not ExecutaSincronismo_TabelasAux('ufs', Msg) then Exit;
                LaAviso.Text := 'Sincronizando tabela ufs';
                PB1.Value := PB1.Value + 1;
                Application.ProcessMessages;
                AI1.UpdateEffects;
              end;


              if FMX_DmkSincro.SincronizaTabela(Dmod.MyDB, Dmod.QrUpd,
                Dmod.QrAux, 'wentidades', ['Codigo', 'CodUsu'], DataHora, Msg) = False then
              begin
                Exit;
              end;
              LaAviso.Text := 'Sincronizando tabela wentidades';
              PB1.Value := PB1.Value + 1;
              Application.ProcessMessages;
              AI1.UpdateEffects;


              if not FMX_DmkSincro.SincronizaTabela(Dmod.MyDB, Dmod.QrUpd,
                Dmod.QrAux, 'wcarteiras', ['Codigo'], DataHora, Msg)
              then
                Exit;
              LaAviso.Text := 'Sincronizando tabela wcarteiras';
              PB1.Value := PB1.Value + 1;
              Application.ProcessMessages;
              AI1.UpdateEffects;


              if not FMX_DmkSincro.SincronizaTabela(Dmod.MyDB, Dmod.QrUpd,
                Dmod.QrAux, 'wcontas', ['Codigo'], DataHora, Msg)
              then
                Exit;
              LaAviso.Text := 'Sincronizando tabela wcontas';
              PB1.Value    := PB1.Value + 1;
              Application.ProcessMessages;
              AI1.UpdateEffects;


              if not FMX_DmkSincro.SincronizaTabela(Dmod.MyDB, Dmod.QrUpd,
                Dmod.QrAux, WLAN_CTOS, ['Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], DataHora, Msg)
              then
                Exit;
              LaAviso.Text := 'Sincronizando tabela ' + WLAN_CTOS;
              PB1.Value    := PB1.Value + 1;
              Application.ProcessMessages;
              AI1.UpdateEffects;


              if not FMX_DmkSincro.SincronizaTabela(Dmod.MyDB, Dmod.QrUpd,
                Dmod.QrAux, 'wlctqrcode', ['Codigo'], DataHora, Msg)
              then
                Exit;
              LaAviso.Text := 'Sincronizando tabela wlctqrcode';
              PB1.Value    := PB1.Value + 1;
              Application.ProcessMessages;
              AI1.UpdateEffects;
            end);
            TThread.Synchronize (TThread.CurrentThread,
            procedure ()
            begin
              if Msg = '' then
                Msg := 'Sincronismo finalizado!';
            end);
          finally
            TThread.Synchronize (TThread.CurrentThread,
            procedure ()
            begin
              PB1.Visible := False;
              //
              AI1.Enabled := False;
              AI1.Visible := False;
            end);
          end;
          TThread.Synchronize (TThread.CurrentThread,
          procedure ()
          begin
            if Msg <> '' then
            begin
              LaAviso.Text := Msg;
              //
              FMX_dmkPF.GeraNotificacao(NotificationCenter1, CO_APP_Nome,
                CO_APP_Descri, Msg);
            end;
          end);
        end).Start;
    end;
    else
      FMX_Geral.MB_Aviso('Sincronismo n�o implementado para este aplicativo!');
  end;
end;

procedure TFmSincroDB.NotifyComplete(Msg: String);
var
  MyNotification: TNotification;
begin
  MyNotification := NotificationCenter1.CreateNotification;
  try
    if MyNotification <> nil then
    begin
      MyNotification.Name        := 'InfrajobMobile';
      MyNotification.Title       := 'Infrajob Mobile';
      MyNotification.AlertBody   := Msg;
      MyNotification.FireDate    := Now;

      NotificationCenter1.ApplicationIconBadgeNumber := 1;
      NotificationCenter1.PresentNotification(MyNotification);
    end else
      FMX_Geral.MB_Aviso(Msg);
  finally
    MyNotification.DisposeOf;
  end;
end;

function TFmSincroDB.VerificaTabelasAuxEstaVazio: Boolean;
begin
  //Tebelas aux
  Grl_DmkDB.AbreSQLQuery0(QrSinc, Dmod.MyDB, [
    'SELECT * ',
    'FROM bacen_pais ',
    'LIMIT 1 ',
    '']);
  if QrSinc.RecordCount > 0 then
    Result := False
  else
    Result := True;
end;

procedure TFmSincroDB.ResetRESTComponentsToDefaults;
begin
  RESTRequest1.ResetToDefaults;
  RESTClient1.ResetToDefaults;
  RESTResponse1.ResetToDefaults;
end;

end.





