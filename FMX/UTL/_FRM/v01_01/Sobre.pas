unit Sobre;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.ListBox, FMX.Layouts, UnGrl_Vars;

type
  TFmSobre = class(TForm)
    ToolBar1: TToolBar;
    SBSaida: TSpeedButton;
    ListBox1: TListBox;
    LBIOSName: TListBoxItem;
    LBIDeviceName: TListBoxItem;
    LBIOSVersion: TListBoxItem;
    LBIDvcScreenW: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBIDvcScreenH: TListBoxItem;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    LBIAppNome: TListBoxItem;
    LBIVersao: TListBoxItem;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBIDeviceID: TListBoxItem;
    Label1: TLabel;
    LBIOSNickName: TListBoxItem;
    LBIDeviceNum: TListBoxItem;
    procedure SBSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSobre: TFmSobre;

implementation

uses UnGrl_AllOS, MyListas, UnFMX_Geral, UnFMX_DmkProcFunc, UnFMX_Grl_Vars;

{$R *.fmx}

procedure TFmSobre.FormCreate(Sender: TObject);
var
  AppNome, Versao, DeviceID, DeviceName, DeviceNum, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion: String;
begin
  AppNome   := Application.Title;
  Versao    := FMX_Geral.FF0(CO_VERSAO);
  DeviceNum := FMX_Geral.FF0(VAR_WEB_USR_DEVICE);
  //
  Grl_AllOS.ConfiguraDadosDispositivo(DeviceID, DeviceName, DvcScreenH,
    DvcScreenW, OSName, OSNickName, OSVersion);
  //
  LBIAppNome.Text    := LBIAppNome.Text    + ' ' + AppNome;
  LBIVersao.Text     := LBIVersao.Text     + ' ' + Versao;
  LBIDeviceName.Text := LBIDeviceName.Text + ' ' + DeviceName;
  LBIDeviceNum.Text  := LBIDeviceNum.Text  + ' ' + DeviceNum;
  LBIDeviceID.Text   := LBIDeviceID.Text   + ' ' + DeviceID;
  LBIOSName.Text     := LBIOSName.Text     + ' ' + OSName;
  LBIOSNickName.Text := LBIOSNickName.Text + ' ' + OSNickName;
  LBIOSVersion.Text  := LBIOSVersion.Text  + ' ' + OSVersion;
  LBIDvcScreenW.Text := LBIDvcScreenW.Text + ' ' + DvcScreenW;
  LBIDvcScreenH.Text := LBIDvcScreenH.Text + ' ' + DvcScreenH;
end;

procedure TFmSobre.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

end.
