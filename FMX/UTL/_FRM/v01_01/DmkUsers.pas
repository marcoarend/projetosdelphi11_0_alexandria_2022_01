unit DmkUsers;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.ImageList, System.Actions, System.Rtti, System.Bindings.Outputs,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.ImgList, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  Data.DB, FMX.ListBox, FMX.Edit, FMX.SearchBox, FMX.StdCtrls, FMX.ActnList,
  FMX.Controls.Presentation, FMX.Layouts, FMX.TabControl, Fmx.Bind.Editors,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.EngExt, Data.Bind.DBScope;

type
  TFmDmkUsers = class(TForm)
    TbUsuarios: TFDTable;
    TbUsuariosCodigo: TIntegerField;
    TbUsuariosPersonalName2: TStringField;
    TbUsuariosUserName2: TStringField;
    TbUsuariosPassWord: TStringField;
    TbUsuariosAtual: TShortintField;
    QrUsuarios: TFDQuery;
    ImageList1: TImageList;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    ListBox1: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbInclui: TSpeedButton;
    SbSaida: TSpeedButton;
    SBReabre: TSpeedButton;
    SearchBox1: TSearchBox;
    TabItem2: TTabItem;
    ListBox2: TListBox;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    LBICodigo: TListBoxItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBINome: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    LBIUserName: TListBoxItem;
    ListBoxHeader2: TListBoxHeader;
    SbExclui: TSpeedButton;
    SbVolta: TSpeedButton;
    SBSeleciona: TSpeedButton;
    Label2: TLabel;
    ActionList1: TActionList;
    ChangeTabAction1: TChangeTabAction;
    ChangeTabAction2: TChangeTabAction;
    ChangeTabAction3: TChangeTabAction;
    BSTbUsuarios: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    procedure SBReabreClick(Sender: TObject);
    procedure SbIncluiClick(Sender: TObject);
    procedure SbSaidaClick(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure SBSelecionaClick(Sender: TObject);
    procedure SbExcluiClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTbUsuarios;
    procedure ReopenUsuarios(Codigo: Integer);
    function  SelecionaUsuario(Codigo: Integer): Boolean;
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmDmkUsers: TFmDmkUsers;

implementation

{$R *.fmx}

uses Principal, Module, UnFMX_DmkProcFunc, UnGrl_DmkDB, UnDmkEnums, UnFMX_Geral,
  UnFMX_Grl_Vars, MyListas, DmkLogin, UnFMX_CfgDBApp, UnFMX_DmkForms;

procedure TFmDmkUsers.FormCreate(Sender: TObject);
begin
  TabControl1.TabPosition := TTabPosition.None;
  TabControl1.ActiveTab   := TabItem1;
  //
  ReopenTbUsuarios;
end;

procedure TFmDmkUsers.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if Item.IsSelected then
  begin
    FCodigo := FMX_dmkPF.GetSelectedValue(ListBox1);
    ExecuteAction(ChangeTabAction2);
  end;
end;

procedure TFmDmkUsers.ReopenTbUsuarios;
begin
  TbUsuarios.Close;
  TbUsuarios.Connection := Dmod.AllDB;
  TbUsuarios.Filtered   := False;
  TbUsuarios.TableName  := 'usuarios';
  TbUsuarios.Open();
  //
  if TbUsuarios.RecordCount > 0 then
    SearchBox1.Enabled := True
  else
    SearchBox1.Enabled := False;
end;

procedure TFmDmkUsers.ReopenUsuarios(Codigo: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrUsuarios, Dmod.AllDB, [
    'SELECT * ',
    'FROM usuarios ',
    'WHERE Codigo=' + FMX_Geral.FF0(Codigo),
    '']);
end;

procedure TFmDmkUsers.SbExcluiClick(Sender: TObject);
begin
  if Grl_DmkDB.ExcluiRegistroInt1_SQLite('ATEN��O:' + sLineBreak +
    'Ao remover este usu�rio voc� n�o ter� mais acesso aos dados atrav�s deste dispositivo!' +
    sLineBreak + 'Deseja excluir este registro?',
    'usuarios', 'Codigo', FCodigo, Dmod.AllDB) = mrYes then
  begin
    ReopenTbUsuarios;
    //
    if TbUsuarios.RecordCount > 0 then
    begin
      if SelecionaUsuario(TbUsuariosCodigo.Value) then
        ExecuteAction(ChangeTabAction1);
    end else
      Application.Terminate;
  end;
end;

procedure TFmDmkUsers.SbIncluiClick(Sender: TObject);
begin
  FMX_dmkForms.CriaFm_AllOS(TFmDmkLogin, FmDmkLogin, True);
end;

procedure TFmDmkUsers.SBReabreClick(Sender: TObject);
begin
  ReopenTbUsuarios;
end;

procedure TFmDmkUsers.SbSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := FCodigo;
  Close;
end;

procedure TFmDmkUsers.SBSelecionaClick(Sender: TObject);
var
  UsrCod: Integer;
begin
  UsrCod := FCodigo;
  //
  if Grl_DmkDB.CarregaSQLInsUpd(Dmod.QrLoc, Dmod.AllDB, stUpd, 'usuarios', False,
    ['Atual'], ['Codigo'], [1], [UsrCod], True, False, '', stWeb) then
  begin
    if SelecionaUsuario(UsrCod) then
    begin
      ReopenTbUsuarios;
      ExecuteAction(ChangeTabAction1);
    end else
      FMX_Geral.MB_Erro('Falha ao selecionar usu�rio!');
  end;
end;

function TFmDmkUsers.SelecionaUsuario(Codigo: Integer): Boolean;
begin
  Result := False;
  //
  if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrLoc, Dmod.AllDB,
    ['UPDATE usuarios SET Atual="1" ', 'WHERE Codigo = "'+
    FMX_Geral.FF0(Codigo) +'"']) then
  begin
    if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrLoc, Dmod.AllDB,
      ['UPDATE usuarios SET Atual="0" ', 'WHERE Codigo <> "'+
      FMX_Geral.FF0(Codigo) +'"']) then
    begin
      FMX_CfgDBApp.ConfiguraDispositivo(FmPrincipal.LaUsuario, Dmod.AllDB,
        Dmod.MyDB, Dmod.QrUsuarios, Dmod.QrLoc, Dmod.QrUpd, Dmod.QrControle,
        Dmod.QrEspecificos);
      //
      Result := True;
    end;
  end;
end;

procedure TFmDmkUsers.TabControl1Change(Sender: TObject);
var
  Codigo: Integer;
begin
  case TabControl1.TabIndex of
    0: //Itens
    begin

    end;
    1: //Visualiza
    begin
      ReopenUsuarios(FCodigo);
      //
      Codigo := QrUsuarios.FieldByName('Codigo').AsInteger;
      //
      LBICodigo.Text   := FMX_Geral.FF0(Codigo);
      LBINome.Text     := QrUsuarios.FieldByName('PersonalName').AsString;
      LBIUserName.Text := QrUsuarios.FieldByName('UserName').AsString;
    end;
  end;
end;

end.
