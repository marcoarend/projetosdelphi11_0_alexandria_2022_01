unit DmkLogin;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  {$IFDEF ANDROID}
  androidapi.Helpers,
  {$ENDIF}
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.ListBox, FMX.Layouts, FMX.Edit, Xml.xmldom,
  Xml.XMLIntf, Xml.adomxmldom, Xml.XMLDoc, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope;

type
  TFmDmkLogin = class(TForm)
    ListBox1: TListBox;
    ListBoxItem1: TListBoxItem;
    EdUsuario: TEdit;
    ClearEditButton1: TClearEditButton;
    ListBoxItem2: TListBoxItem;
    EdSenha: TEdit;
    ToolBar1: TToolBar;
    Label1: TLabel;
    ClearEditButton2: TClearEditButton;
    PasswordEditButton1: TPasswordEditButton;
    SbConfirma: TSpeedButton;
    SbDesiste: TSpeedButton;
    procedure SbDesisteClick(Sender: TObject);
    procedure SbConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmDmkLogin: TFmDmkLogin;

implementation

uses  UnGrl_AllOS, UnFMX_DmkWeb, UnFMX_Geral, MyListas, UnGrl_DmkDB,
  Module, UnDmkEnums, Principal, UnFMX_DmkProcFunc, UnFMX_CfgDBApp;

{$R *.fmx}


procedure TFmDmkLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Dmod.QrControle.Active = False then
  begin
    {$IF DEFINED(ANDROID)}
    SharedActivity.finish;
    {$ENDIF}
    {$IFDEF MSWINDOWS}
    Application.Terminate;
    {$ENDIF}
  end else
    Close;
end;

procedure TFmDmkLogin.SbConfirmaClick(Sender: TObject);
const
  CoMsg        = 'Falha ao autenticar dispositivo!';
  CoMsgErrCons = 'Falha ao executar comando REST!';
var
  Usuario, Senha, Res, Res2, Msg, MsgTxt, DeviceID, DeviceName, DvcScreenH,
  DvcScreenW, OSName, OSNickName, OSVersion, NomeUsr, UsrTip, UsrEnt, Token,
  WebID, DataF, DataV: String;
  i, j, MsgCod, UsrCod, UsrID, UsrNum, Status, StatusCode: Integer;
  Continua: Boolean;
  XMLArqRes, XMLArqRes2: IXMLDocument;
  SQLTipo: TSQLType;
begin
  Continua := False;
  Usuario  := EdUsuario.Text;
  Senha    := EdSenha.Text;
  //
  if FMX_dmkPF.FIC(Usuario = '', EdUsuario, 'Usu�rio n�o definido!') then Exit;
  if FMX_dmkPF.FIC(Senha = '', EdSenha, 'Senha n�o definida!') then Exit;
  //
  Grl_AllOS.ConfiguraDadosDispositivo(DeviceID, DeviceName, DvcScreenH,
    DvcScreenW, OSName, OSNickName, OSVersion);
  //
  Res := FMX_dmkWeb.REST_URL_Post([], [], ['id', 'method', 'Usuario', 'Senha'],
    [CO_WEBID, 'REST_LoginUser', Usuario, Senha], CO_URL_DMKAPI, StatusCode);
  //
  if Res <> '' then
  begin
    XMLArqRes := TXMLDocument.Create(nil);
    XMLArqRes.Active := False;
    try
      XMLArqRes.LoadFromXML(Res);
      XMLArqRes.Encoding := 'ISO-10646-UCS-2';
      //
      for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLArqRes.DocumentElement.ChildNodes[i] do
        begin
          MsgCod := FMX_Geral.IMV(ChildNodes['MsgCod'].Text);

          if MsgCod <> 100 then //Falha ao conectar
          begin
            Msg := ChildNodes['MsgTxt'].Text;
            //
            FMX_Geral.MB_Erro('Erro: ' + FMX_Geral.FF0(MsgCod) + sLineBreak + Msg);
            Exit;
          end else
          begin
            UsrCod  := FMX_Geral.IMV(ChildNodes['UsrCod'].Text);
            NomeUsr := ChildNodes['NomeUsr'].Text;
            UsrTip  := ChildNodes['UsrTip'].Text;
            UsrEnt  := ChildNodes['UsrEnt'].Text; //Deixar assim pois pode ser negativo
            UsrID   := FMX_Geral.IMV(ChildNodes['UsrID'].Text);
            Token   := ChildNodes['Token'].Text;
            //
            Res2 := FMX_dmkWeb.REST_URL_Post([], [],
              ['id', 'method', 'CodAplic',
              'VersaoAtu', 'DeviceID', 'DeviceName',
              'DvcScreenH', 'DvcScreenW', 'OSName',
              'OSNickName', 'OSVersion', 'UsrID',
              'UsrEnt'],
              [CO_WEBID, 'REST_ValidaLicencaMobileDmk', FMX_Geral.FF0(CO_DMKID_APP),
              FMX_Geral.FF0(CO_VERSAO), DeviceID, DeviceName,
              DvcScreenH, DvcScreenW, OSName,
              OSNickName, OSVersion, FMX_Geral.FF0(UsrCod),
              UsrEnt], CO_URL_DMKAPI, StatusCode);
            //
            if Res2 <> '' then
            begin
              XMLArqRes2 := TXMLDocument.Create(nil);
              XMLArqRes2.Active := False;
              try
                XMLArqRes2.LoadFromXML(Res2);
                XMLArqRes2.Encoding := 'ISO-10646-UCS-2';
                //
                for j := 0 to XMLArqRes2.DocumentElement.ChildNodes.Count - 1 do
                begin
                  with XMLArqRes2.DocumentElement.ChildNodes[j] do
                  begin
                    MsgCod := FMX_Geral.IMV(ChildNodes['MsgCod'].Text);
                    MsgTxt := ChildNodes['MsgTxt'].Text;
                    UsrNum := FMX_Geral.IMV(ChildNodes['UsrNum'].Text);
                    DataF  := ChildNodes['DataFim'].Text;
                    DataV  := ChildNodes['VerificadoEm'].Text;
                    Status := FMX_Geral.IMV(ChildNodes['Status'].Text);
                    WebID  := ChildNodes['WebID'].Text;
                    //
                    if MsgCod = 100 then
                    begin
                      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB,
                        stUpd, 'controle', False, ['Versao'], ['Codigo'],
                        [FMX_Geral.FF0(CO_VERSAO)], [1], False,
                        TdmkSQLInsert.dmksqlinsInsOnly, '', stWeb) then
                      begin
                        Grl_DmkDB.AbreSQLQuery0(Dmod.QrLoc, Dmod.AllDB, [
                          'SELECT Codigo ',
                          'FROM usuarios ',
                          'WHERE Codigo=' + FMX_Geral.FF0(UsrCod),
                          '']);
                        if Dmod.QrLoc.RecordCount > 0 then
                          SQLTipo := stUpd
                        else
                          SQLTipo := stIns;
                        //
                        if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB,
                          SQLTipo, 'usuarios', False,
                          ['PersonalName', 'UserName', 'PassWord',
                          'UsrEnt', 'UsrID', 'UsrNum',
                          'Token', 'WebID', 'Tipo', 'Atual',
                          'DataF', 'DataV', 'Status'], ['Codigo'],
                          [NomeUsr, Usuario, '',
                          UsrEnt, UsrID, UsrNum,
                          Token, WebID, UsrTip, 1,
                          DataF, DataV, Status], [UsrCod], True,
                          TdmkSQLInsert.dmksqlinsInsOnly, '', stWeb) then
                        begin
                          if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrLoc, Dmod.AllDB,
                            ['UPDATE usuarios SET Atual="0" ',
                            'WHERE Codigo <> "'+ FMX_Geral.FF0(UsrCod) +'"'])
                          then
                            Continua := True;
                        end else
                          FMX_Geral.MB_Aviso(CoMsg);
                      end else
                        FMX_Geral.MB_Aviso(CoMsg);
                    end else
                      FMX_Geral.MB_Aviso(MsgTxt);
                  end;
                end;
              finally
                ;
              end;
            end else
              FMX_Geral.MB_Erro(CoMsgErrCons);
          end;
        end;
      end;
    finally
      ;
    end;
  end else
    FMX_Geral.MB_Erro(CoMsgErrCons);
  //
  if Continua then
  begin
    FMX_CfgDBApp.ConfiguraDispositivo(FmPrincipal.LaUsuario, Dmod.AllDB,
      Dmod.MyDB, Dmod.QrUsuarios, Dmod.QrLoc, Dmod.QrUpd, Dmod.QrControle,
      Dmod.QrEspecificos);
    Close;
  end;
end;

procedure TFmDmkLogin.SbDesisteClick(Sender: TObject);
begin
  Close;
end;

end.




