unit OAuth;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.WebBrowser;

type
  TOAuth2WebFormRedirectEvent = procedure(const AURL: string; var DoCloseWebView : Boolean) of object;

  TFmOAuth = class(TForm)
    Browser: TWebBrowser;
    procedure BrowserDidFinishLoad(ASender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FOnAfterRedirect: TOAuth2WebFormRedirectEvent;

    FLastURL: string;
  public
    { Public declarations }
    procedure ShowWithURL(const AURL: string);

    property LastURL: string read FLastURL;

    property OnAfterRedirect: TOAuth2WebFormRedirectEvent read FOnAfterRedirect write FOnAfterRedirect;
  end;

var
  FmOAuth: TFmOAuth;

implementation

{$R *.fmx}

procedure TFmOAuth.BrowserDidFinishLoad(ASender: TObject);
var
  LDoCloseForm : boolean;
begin
  FLastURL := Browser.URL;

  if Assigned(FOnAfterRedirect) then
  begin
    LDoCloseForm:= FALSE;

    FOnAfterRedirect(FLastURL, LDoCloseForm);

    if LDoCloseForm then
      self.Close;
  end;
end;

procedure TFmOAuth.FormCreate(Sender: TObject);
begin
  FOnAfterRedirect := nil;

  FLastURL := '';
end;

procedure TFmOAuth.ShowWithURL(const AURL: string);
begin
  Browser.Navigate(AURL);
  Self.ShowModal;
end;

end.
