unit UnFMX_DmkHTML2;

interface

uses System.Classes, REST.Client, REST.Types, UnDmkEnums;

type
  TUnFMX_DmkHTML2 = class(TObject)
  private
    procedure ResetRESTComponentsToDefaults(RESTRequest: TRESTRequest;
              RESTResponse: TRESTResponse; RESTClient: TRESTClient);
  public
    // M � T O D O S
    function  HttpGetURL(URL: String; HeaderParams: array of THttpHeader;
              Parametros: array of THttpParam; var Resultado: String;
              TimeOut: Integer = 0): Integer;
    function  HttpPostURL(URL: String; HeaderParams: array of THttpHeader;
              Parametros: array of THttpParam; var Resultado: String;
              TimeOut: Integer = 0): Integer;
    function  HttpPutURL(URL: String; HeaderParams: array of THttpHeader;
              ParamVal: TStringStream; var Resultado: String;
              TimeOut: Integer = 0): Integer;
  end;

var
  FMX_dmkHTML2: TUnFMX_DmkHTML2;

implementation

uses UnFMX_Geral;

{ TUnFMX_DmkHTML2 }

function TUnFMX_DmkHTML2.HttpGetURL(URL: String;
  HeaderParams: array of THttpHeader; Parametros: array of THttpParam;
  var Resultado: String; TimeOut: Integer = 0): Integer;
var
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  I: Integer;
  HeaderStr, HeaderVal, ParamNome, ParamValor, Params: String;
begin
  Result := 0;
  //
  //Adiciona par�metros
  if Length(Parametros) > 0 then
  begin
    for I := Low(Parametros) to High(Parametros) do
    begin
      Params := '?';
      //
      ParamNome  := UTF8Encode(Parametros[I].Nome);
      ParamValor := URIEncode(UTF8Encode(Parametros[I].Valor));
      //
      if (I = 1) then
        Params := Params + ParamNome + '=' + ParamValor
      else
        Params := Params + '&' + ParamNome + '=' + ParamValor;
    end;
  end else
    Params := '';
  //
  RESTRequest := TRESTRequest.Create(nil);
  //
  if TimeOut > 0 then
    RESTRequest.TimeOut = TimeOut;
  //
  RESTClient   := TRESTClient.Create(URL + Params);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL   := URL + Params;
    RESTRequest.Method   := TRESTRequestMethod.rmGET;
    RESTRequest.Client   := RESTClient;
    RESTRequest.Response := RESTResponse;
    //
    //Adiciona header
    if Length(HeaderParams) > 0 then
    begin
      for I := Low(HeaderParams) to High(HeaderParams) do
      begin
        HeaderStr := HeaderParams[I].Nome;
        HeaderVal := HeaderParams[I].Valor;
        //
        RESTRequest.Params.AddHeader(HeaderStr, HeaderVal);
      end;
    end;
    //
    try
      RESTRequest.Execute;
      //
      Resultado := RESTResponse.Content;
      Result    := RESTRequest.Response.StatusCode;
    except
      Result := 0;
    end;
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;

function TUnFMX_DmkHTML2.HttpPostURL(URL: String; HeaderParams: array of THttpHeader;
  Parametros: array of THttpParam; var Resultado: String; TimeOut: Integer = 0): Integer;
var
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
  I: Integer;
  HeaderStr, HeaderVal, ParamNome, ParamValor: String;
begin
  Result := 0;
  //
  RESTRequest := TRESTRequest.Create(nil);
  //
  if TimeOut > 0 then
    RESTRequest.TimeOut = TimeOut;
  //
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL   := URL;
    RESTRequest.Method   := TRESTRequestMethod.rmPOST;
    RESTRequest.Client   := RESTClient;
    RESTRequest.Response := RESTResponse;
    //
    //Adiciona header
    if Length(HeaderParams) > 0 then
    begin
      for I := Low(HeaderParams) to High(HeaderParams) do
      begin
        HeaderStr := HeaderParams[I].Nome;
        HeaderVal := HeaderParams[I].Valor;
        //
        RESTRequest.Params.AddHeader(HeaderStr, HeaderVal);
      end;
    end;
    //Adiciona par�metros
    if Length(Parametros) > 0 then
    begin
      for I := Low(Parametros) to High(Parametros) do
      begin
        ParamNome  := UTF8Encode(Parametros[I].Nome);
        ParamValor := UTF8Encode(Parametros[I].Valor);
        //
        Param       := RESTRequest.Params.AddItem;
        Param.name  := ParamNome;
        Param.Value := ParamValor;
        Param.Kind  := TRESTRequestParameterKind.pkGETorPOST;
      end;
    end;
    try
      RESTRequest.Execute;
      //
      Resultado := RESTResponse.Content;
      Result    := RESTRequest.Response.StatusCode;
    except
      Result := 0;
    end;
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;

function TUnFMX_DmkHTML2.HttpPutURL(URL: String; HeaderParams: array of THttpHeader;
  ParamVal: TStringStream; var Resultado: String; TimeOut: Integer = 0): Integer;
var
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
  ParVal, HeaderStr, HeaderVal: String;
  I: Integer;
begin
  Result := 0;
  //
  if Length(HeaderStr) <> Length(HeaderVal) then
  begin
    FMX_Geral.MB_Aviso('A quantidade de itens dos par�metros: "HeaderStr" e "HeaderVal" deve ser a mesma!');
    Exit;
  end;
  RESTRequest := TRESTRequest.Create(nil);
  //
  if TimeOut > 0 then
    RESTRequest.TimeOut = TimeOut;
  //
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL   := URL;
    RESTRequest.Method   := TRESTRequestMethod.rmPOST;
    RESTRequest.Client   := RESTClient;
    RESTRequest.Response := RESTResponse;
    //
    //Adiciona header
    if Length(HeaderParams) > 0 then
    begin
      for I := Low(HeaderParams) to High(HeaderParams) do
      begin
        HeaderStr := HeaderParams[I].Nome;
        HeaderVal := HeaderParams[I].Valor;
        //
        RESTRequest.Params.AddHeader(HeaderStr, HeaderVal);
      end;
    end;
    //Adiciona par�metros
    ParVal := ParamVal.DataString;
    //
    if Length(ParVal) > 0 then
    begin
      Param             := RESTRequest.Params.AddItem;
      Param.ContentType := TRESTContentType.ctAPPLICATION_ATOM_XML;
      Param.Kind        := TRESTRequestParameterKind.pkREQUESTBODY;
      Param.Value       := ParVal;
    end;
    try
      RESTRequest.Execute;
      //
      Resultado := RESTResponse.Content;
      Result    := RESTRequest.Response.StatusCode;
    except
      Result := 0;
    end;
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;

procedure TUnFMX_DmkHTML2.ResetRESTComponentsToDefaults(
  RESTRequest: TRESTRequest; RESTResponse: TRESTResponse;
  RESTClient: TRESTClient);
begin
  RESTRequest.ResetToDefaults;
  RESTResponse.ResetToDefaults;
  RESTClient.ResetToDefaults;
end;

end.

