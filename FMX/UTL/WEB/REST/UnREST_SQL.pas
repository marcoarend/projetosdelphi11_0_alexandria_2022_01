unit UnREST_SQL;

interface

uses System.Classes, System.UITypes, System.SysUtils, System.Variants,
  REST.Types, Data.DB, FireDAC.Comp.Client;

type
  TUnREST_SQL = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ValuesFromFDQuery(Query: TFDQuery; Tabela, IDSrvNam, IDSrvVal,
             OthFields: String; MaxLenStat: Integer): Boolean;
  end;

var
  REST_SQL: TUnREST_SQL;

implementation

uses UnGrl_Geral, UnFMX_DmkRemoteQuery, UnGrl_Vars;

{ TUnREST_SQL }

function TUnREST_SQL.ValuesFromFDQuery(Query: TFDQuery; Tabela, IDSrvNam,
  IDSrvVal, OthFields: String; MaxLenStat: Integer): Boolean;
var
  I, RecLen, StaLen, CabLen: Integer;
  RecStr, StaStr, CabStr, wSQL: String;
begin
  if Query.RecordCount = 0 then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  //
  RecStr := EmptyStr;
  //
  CabStr := 'INSERT INTO ' + Tabela + ' (';
  if IDSrvNam <> EmptyStr then
    CabStr := CabStr + IDSrvNam + ',';
  CabStr := CabStr + OthFields + ') VALUES ';
  //
  CabLen := Length(CabStr);
  StaStr := EmptyStr; //CabStr;
  StaLen := CabLen;
  Query.First;
  while not Query.Eof do
  begin
    RecStr := ' (';
    if IDSrvNam <> EmptyStr then
      RecStr := RecStr + (*IDSrvNam + '=' +*) IDSrvVal + ',';
    for I := 0 to Query.Fields.Count - 1 do
    begin
      RecStr := RecStr + (*Query.Fields[I].Name + '=' +*)
        Grl_Geral.VariavelToStringAD(Query.Fields[I].Value);
      if I < Query.Fields.Count - 1 then
        RecStr := RecStr + ','
      else
        RecStr := RecStr + ') '
    end;
    StaStr := StaStr + RecStr;
    StaLen := StaLen + Length(RecStr);
    //
    if StaLen >= MaxLenStat then
    begin
      wSQL := CabStr + StaStr + ';';
      //Grl_Geral.MB_Info(wSQL);
      Result := FMX_DmkRemoteQuery.SQL_Executa(wSQL);
      if Result then
      begin
        StaStr := EmptyStr;
        StaLen := CabLen;
      end else
        Exit;
////////////////////////////////////////////////////////////////////////////////
    end else
    if Query.RecNo < Query.RecordCount then
      StaStr := StaStr + ', ';
    //
    Query.Next;
  end;
  if StaStr <> EmptyStr then
  begin
    wSQL := CabStr + StaStr + ';';
   Result := FMX_DmkRemoteQuery.SQL_Executa(wSQL);
  end;

end;

end.
