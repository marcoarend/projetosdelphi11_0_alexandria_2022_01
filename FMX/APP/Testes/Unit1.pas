unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Consts,
  FMX.DialogService.Async,
  System.NetEncoding, System.IOUtils,
  FMX.Edit, FMX.Objects, System.Actions, FMX.ActnList, FMX.Layouts,
  FMX.StdActns, FMX.MediaLibrary.Actions;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Image1: TImage;
    ActionList1: TActionList;
    ToolBar1: TToolBar;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Layout1: TLayout;
    Button1: TButton;
    Button2: TButton;
    TakePhotoFromCameraAction1: TTakePhotoFromCameraAction;
    TakePhotoFromLibraryAction1: TTakePhotoFromLibraryAction;
    ShowShareSheetAction1: TShowShareSheetAction;
    Image2: TImage;
    Button6: TButton;
    procedure Button2Click(Sender: TObject);
    procedure TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
    procedure TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
    procedure ShowShareSheetAction1BeforeExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
    function  Base64FromBitmap(Bitmap: TBitmap): string;
    procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
    procedure DuplicaImagem();
    // Pergunta
    function MB_Pergunta(Pergunta: String; Proc: TProc): Boolean;
    procedure Ret_OK();
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure SomeProc;
begin
  Form1.Ret_OK();
end;


function TForm1.Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
  Encoding: TBase64Encoding;
begin
  Input := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Encode(Input, Output);
        Result := Output.DataString;
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TForm1.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Input := TStringStream.Create(Base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  DuplicaImagem();
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  F: procedure;
begin
  F := SomeProc;
  MB_Pergunta('Pergunta', F);
//
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  getNewName: String;
begin
  getNewName := 'teste.jpg';
  getNewName := System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetCameraPath, getNewName);
  Image1.Bitmap.SaveToFile(getNewName);
  Image2.Bitmap.LoadFromFile(getNewName);
end;

procedure TForm1.DuplicaImagem();
var
  Bitmap: TBitmap;
  sBase64, Str1, str2: String;
begin
  Bitmap := TBitmap.Create;
  try
    BitMap := Image1.MultiResBitmap.Items[0].Bitmap;
    sBase64 := Base64FromBitmap(Bitmap);
    ShowMessage(IntToStr(Length(sBase64)));
//    ShowMessage(sBase64);
    Str1 := Copy(sBase64, 1, 1000);
    Str2 := Copy(sBase64, 1001);
    BitmapFromBase64(str1 + str2, Image2.MultiResBitmap.Items[0].Bitmap);
  finally
    //Bitmap.Free;
  end;
end;

function TForm1.MB_Pergunta(Pergunta: String; Proc: TProc): Boolean;
begin
  TDialogServiceAsync.MessageDialog(
  Pergunta,
  TMsgDlgType.mtConfirmation,
  [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],
  TMsgDlgBtn.mbNo,
  0,
  procedure(const AResult: TModalResult)
  begin
    case AResult of
      mrYes:
      begin
        if Assigned(Proc) then
          Proc;
      end;
      else ShowMessage('Button pressed:' + IntToStr(ModalResult));
    end;
    // use AResult here as needed...
    // remember, this event will be called ASYNCHRONOUSLY!
    // MessageDialog() will have already exited and the calling
    // code moved on to other things by the time this handler runs...
  end
  );
  // remember, code here after MessageDialog() exits will
  // run WHILE the dialog is still waiting for user input. You
  // cannot wait here for the dialog to close first. Whatever
  // code that needs the dialog result MUST be triggered by
  // the ACloseDialogProc handler above, not called here...
end;

procedure TForm1.Ret_OK();
begin
  ShowMessage('OK pressionado: ' + Button2.Text);
end;

procedure TForm1.ShowShareSheetAction1BeforeExecute(Sender: TObject);
begin
  ShowShareSheetAction1.Bitmap.Assign(Image1.Bitmap);
end;

procedure TForm1.TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
begin
  //Configurado no Object Inspector em design time:
  //TakePhotoFromCameraAction1.NeedSaveToAlbum := True;
  //TakePhotoFromCameraAction1.Editable := True;
  //
  // ActionList ser� executado!
  //
  Image1.Bitmap.Assign(Image);
end;

procedure TForm1.TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
begin
  //Configurado no Object Inspector em design time:
  //TakePhotoFromLibraryAction1.Editable := True;
  //
  // ActionList ser� executado!
  //
  Image1.Bitmap.Assign(Image);
end;
// Usado exemplo de:
//http://docwiki.embarcadero.com/RADStudio/Rio/en/Taking_and_Sharing_Pictures_and_Text_Using_Action_Lists
//http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Taking_and_Sharing_Pictures_and_Text_Using_Action_Lists

end.
