program DmkRControl;

uses
  System.StartUpCopy,
  FMX.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnFMX_Geral in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Geral.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\AllOS\Units\UnDmkEnums.pas',
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\Mobile\Geral\Lixo\UnGrl_Consts.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnFMX_Grl_Vars in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Vars.pas',
  UnFMX_ValidaIE in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_ValidaIE.pas',
  UnFMX_DmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkProcFunc.pas' {$R *.res},
  Android_FMX_NetworkState in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_NetworkState.pas',
  Android_NetworkState in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_NetworkState.pas',
  Android_BarcodeScanner in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_BarcodeScanner.pas',
  Androidapi.JNI.PowerManager in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Androidapi.JNI.PowerManager.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.FormFactor.Orientations := [TFormOrientation.Portrait];
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
