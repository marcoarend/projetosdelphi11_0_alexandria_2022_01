unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Layouts, FMX.ListBox,
  IPPeerClient, IPPeerServer, System.Tether.Manager, System.Tether.AppProfile,
  FMX.Effects;

type
  TFmPrincipal = class(TForm)
    TCPrincipal: TTabControl;
    TIConexao: TTabItem;
    TICRemoto: TTabItem;
    ToolBarTop: TToolBar;
    Label2: TLabel;
    BtRefresh: TButton;
    LBServidores: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    LaVolume: TLabel;
    BtVolDown: TButton;
    BtMudo: TButton;
    BtVolUp: TButton;
    Panel4: TPanel;
    BtF5: TButton;
    BtEsc: TButton;
    Panel5: TPanel;
    BtBaixo: TButton;
    BtDir: TButton;
    BtCima: TButton;
    BtEsq: TButton;
    ToolBar1: TToolBar;
    SBDesconectar: TSpeedButton;
    StyleBook1: TStyleBook;
    Panel6: TPanel;
    BtStop: TButton;
    BtPlay: TButton;
    BtPrev: TButton;
    BtNext: TButton;
    BtMenu: TButton;
    LBMenu: TListBox;
    LBIBranco: TListBoxItem;
    ShadowEffect1: TShadowEffect;
    ShadowEffect2: TShadowEffect;
    LBIPreto: TListBoxItem;
    LBIAltEnter: TListBoxItem;
    Label1: TLabel;
    LBIAltTab: TListBoxItem;
    LBIProjetar: TListBoxItem;
    TAPRControlCli: TTetheringAppProfile;
    TMRControlCli: TTetheringManager;
    procedure FormCreate(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LBServidoresItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure BtCimaClick(Sender: TObject);
    procedure BtBaixoClick(Sender: TObject);
    procedure BtDirClick(Sender: TObject);
    procedure BtEsqClick(Sender: TObject);
    procedure BtEscClick(Sender: TObject);
    procedure BtF5Click(Sender: TObject);
    procedure BtMudoClick(Sender: TObject);
    procedure BtVolDownClick(Sender: TObject);
    procedure BtVolUpClick(Sender: TObject);
    procedure SBDesconectarClick(Sender: TObject);
    procedure BtPlayClick(Sender: TObject);
    procedure BtStopClick(Sender: TObject);
    procedure BtPrevClick(Sender: TObject);
    procedure BtNextClick(Sender: TObject);
    procedure LBIAltEnterClick(Sender: TObject);
    procedure LBIPretoClick(Sender: TObject);
    procedure LBIBrancoClick(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure LBIAltTabClick(Sender: TObject);
    procedure LBIProjetarClick(Sender: TObject);
    procedure TMRControlCliEndManagersDiscovery(const Sender: TObject;
      const ARemoteManagers: TTetheringManagerInfoList);
    procedure TMRControlCliEndProfilesDiscovery(const Sender: TObject;
      const ARemoteProfiles: TTetheringProfileInfoList);
    procedure TMRControlCliRemoteManagerShutdown(const Sender: TObject;
      const AManagerIdentifier: string);
    procedure TMRControlCliRequestManagerPassword(const Sender: TObject;
      const ARemoteIdentifier: string; var Password: string);
    procedure TAPRControlCliDisconnect(const Sender: TObject;
      const AProfileInfo: TTetheringProfileInfo);
  private
    { Private declarations }
    FConnected: Boolean;
    procedure LocalizaServidor();
    procedure AtualizaLista;
    procedure ExecutaTecla(Tecla: String);
    function  VerificaConexaoServidor: Boolean;
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses UnGrl_Consts, UnFMX_Geral, UnFMX_DmkProcFunc;

{$R *.fmx}
{$R *.XLgXhdpiTb.fmx ANDROID}
{$R *.LgXhdpiTb.fmx ANDROID}

procedure TFmPrincipal.AtualizaLista;
var
  I: Integer;
begin
  LBServidores.Clear;
  for I := 0 to TMRControlCli.RemoteProfiles.Count - 1 do
    //if (TMRControlCli.RemoteProfiles[I].ProfileText = 'MediaReceiverApp') or (TMRControlCli.RemoteProfiles[I].ProfileText = 'VCLMediaReceiverApp') then
      LBServidores.Items.Add(TMRControlCli.RemoteProfiles[I].ProfileText);
  if LBServidores.Count > 0 then
  begin
    LBServidores.ItemIndex := 0;
    FConnected := TAPRControlCli.Connect(TMRControlCli.RemoteProfiles[0]);
  end;
end;

procedure TFmPrincipal.BtBaixoClick(Sender: TObject);
begin
  ExecutaTecla('VK_DOWN');
end;

procedure TFmPrincipal.BtCimaClick(Sender: TObject);
begin
  ExecutaTecla('VK_UP');
end;

procedure TFmPrincipal.BtDirClick(Sender: TObject);
begin
  ExecutaTecla('VK_RIGHT');
end;

procedure TFmPrincipal.BtEscClick(Sender: TObject);
begin
  ExecutaTecla('VK_ESCAPE');
end;

procedure TFmPrincipal.BtEsqClick(Sender: TObject);
begin
  ExecutaTecla('VK_LEFT');
end;

procedure TFmPrincipal.BtF5Click(Sender: TObject);
begin
  ExecutaTecla('VK_F5');
end;

procedure TFmPrincipal.BtMenuClick(Sender: TObject);
begin
  FMX_dmkPF.Configura_PopUpDeBotao_AllOS(LBMenu, Sender);
end;

procedure TFmPrincipal.BtMudoClick(Sender: TObject);
begin
  ExecutaTecla('VK_VOLUME_MUTE');
end;

procedure TFmPrincipal.BtNextClick(Sender: TObject);
begin
  ExecutaTecla('VK_MEDIA_NEXT_TRACK');
end;

procedure TFmPrincipal.BtStopClick(Sender: TObject);
begin
  ExecutaTecla('VK_MEDIA_STOP');
end;

procedure TFmPrincipal.BtPlayClick(Sender: TObject);
begin
  ExecutaTecla('VK_MEDIA_PLAY_PAUSE');
end;

procedure TFmPrincipal.BtPrevClick(Sender: TObject);
begin
  ExecutaTecla('VK_MEDIA_PREV_TRACK');
end;

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  LocalizaServidor;
end;

procedure TFmPrincipal.BtVolDownClick(Sender: TObject);
begin
  ExecutaTecla('VK_VOLUME_DOWN');
end;

procedure TFmPrincipal.BtVolUpClick(Sender: TObject);
begin
  ExecutaTecla('VK_VOLUME_UP');
end;

procedure TFmPrincipal.ExecutaTecla(Tecla: String);
var
  Descri, Comando: String;
begin
  if FConnected then
  begin
    Descri  := Tecla;
    Comando := Tecla;
    //
    TAPRControlCli.SendString(TMRControlCli.RemoteProfiles[LBServidores.ItemIndex], Descri, Comando);
  end else
    FMX_Geral.MB_Aviso('Nenhum dispositivo conectado!');
  (*
  Teclas com suporte
  VK_LWIN             => Menu do windows
  VK_LEFT             => Seta esquerda
  VK_RIGHT            => Seta direita
  VK_UP               => Seta para cima
  VK_DOWN             => Seta para baixo
  VK_ESCAPE           => Esc
  VK_F5               => F5
  VK_VOLUME_UP        => Aumentar volume
  VK_VOLUME_DOWN      => Diminuir volume
  VK_VOLUME_MUTE      => Volume Mudo
  VK_MEDIA_PLAY_PAUSE => Play / Pause
  VK_MEDIA_STOP       => Stop
  VK_MEDIA_PREV_TRACK => Prev
  VK_MEDIA_NEXT_TRACK => Next
  VK_MENU+VK_RETURN   => Alt + Enter
  VK_OEM_COMMA        => , (Slide branco)
  VK_OEM_PERIOD       => . (Slide preto)
  VK_MENU+VK_TAB      => Teclas Alt + Tab
  VK_LWIN+vkP         => Tecla (Projetar (monitor))
  *)
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  TCPrincipal.TabPosition := TTabPosition.None;
  TCPrincipal.ActiveTab   := TIConexao;
  TMRControlCli.Password  := CO_SecureStr_001;
  //
  LBMenu.Visible := False;
  //
  {$IF DEFINED(ANDROID)}
  FMX_dmkPF.ManterTelaLigada(True);
  {$ENDIF}
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  LocalizaServidor;
end;

procedure TFmPrincipal.LBIAltEnterClick(Sender: TObject);
begin
  ExecutaTecla('VK_MENU+VK_RETURN');
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBIAltTabClick(Sender: TObject);
begin
  ExecutaTecla('VK_MENU+VK_TAB');
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBIBrancoClick(Sender: TObject);
begin
  ExecutaTecla('VK_OEM_COMMA');
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBIPretoClick(Sender: TObject);
begin
  ExecutaTecla('VK_OEM_PERIOD');
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBIProjetarClick(Sender: TObject);
begin
  ExecutaTecla('VK_LWIN+vkP');
  LBMenu.Visible := False;
end;

procedure TFmPrincipal.LBServidoresItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  if VerificaConexaoServidor then
  begin
    FConnected := TAPRControlCli.Connect(TMRControlCli.RemoteProfiles[LBServidores.ItemIndex]);
    //
    if FConnected = True then
      TCPrincipal.ActiveTab := TICRemoto
    else
      FMX_Geral.MB_Aviso('Falha ao conectar no servidor!');
  end;
end;

procedure TFmPrincipal.LocalizaServidor;
var
  I: Integer;
begin
  LBServidores.Clear;
  //
  for I := TMRControlCli.PairedManagers.Count - 1 downto 0 do
    TMRControlCli.UnPairManager(TMRControlCli.PairedManagers[I]);
  //
  TMRControlCli.DiscoverManagers;
end;

procedure TFmPrincipal.SBDesconectarClick(Sender: TObject);
begin
  if FConnected then
  begin
    TAPRControlCli.Disconnect(TMRControlCli.RemoteProfiles[0]);
    FConnected := False;
    TCPrincipal.ActiveTab := TIConexao;
  end;
end;

procedure TFmPrincipal.TAPRControlCliDisconnect(const Sender: TObject;
  const AProfileInfo: TTetheringProfileInfo);
begin
  TCPrincipal.ActiveTab := TIConexao;
end;

procedure TFmPrincipal.TMRControlCliEndManagersDiscovery(
  const Sender: TObject; const ARemoteManagers: TTetheringManagerInfoList);
var
  I: Integer;
begin
  for I := 0 to ARemoteManagers.Count - 1 do
    if (ARemoteManagers[I].ManagerText = 'MediaReceiverManager') or (ARemoteManagers[I].ManagerText = 'VCLMediaReceiver') then
      TMRControlCli.PairManager(ARemoteManagers[I]);
end;

procedure TFmPrincipal.TMRControlCliEndProfilesDiscovery(
  const Sender: TObject; const ARemoteProfiles: TTetheringProfileInfoList);
begin
  AtualizaLista;
end;

procedure TFmPrincipal.TMRControlCliRemoteManagerShutdown(
  const Sender: TObject; const AManagerIdentifier: string);
begin
  AtualizaLista;
end;

procedure TFmPrincipal.TMRControlCliRequestManagerPassword(
  const Sender: TObject; const ARemoteIdentifier: string; var Password: string);
begin
  Password := CO_SecureStr_001;
end;

function TFmPrincipal.VerificaConexaoServidor: Boolean;
begin
  if LBServidores.ItemIndex >= 0 then
    Result := True
  else
  begin
    Result := False;
    //
    FMX_Geral.MB_Aviso('O aplicativo n�o est� conectado no servidor!');
  end;
end;

end.
