unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.TabControl, FMX.Edit,
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  IdURI,
  Androidapi.JNI.GraphicsContentViewText,
  Androidapi.JNI.App,
  Androidapi.Helpers,
  FMX.Platform.Android,
  Androidapi.JNI.JavaTypes,
  Androidapi.JNI.Net,
  Androidapi.JNI.Os,
  Android.BarcodeScanner,
  {$ENDIF}
  FMX.ScrollBox, FMX.Memo, System.IOUtils;

type
  TFmPrincipal = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    ListBox1: TListBox;
    ListBox2: TListBox;
    ListBoxHeader1: TListBoxHeader;
    SbQrCode: TSpeedButton;
    ListBoxItem1: TListBoxItem;
    EdNome: TEdit;
    ClearEditButton1: TClearEditButton;
    Label1: TLabel;
    ListBoxHeader2: TListBoxHeader;
    SBSalvar: TSpeedButton;
    MeDados: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure SbQrCodeClick(Sender: TObject);
    procedure SBSalvarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    QrCode: TAndroidBarcodeScanner;
    function  AppEstaInstalado(const AppNome: string): Boolean;
    procedure MostraBarcode(Sender: TAndroidBarcodeScanner; ABarcode: string);
    procedure AbrirAppLoja(const AppNome: string);
    {$ENDIF}
    function  QrCodeDecode(const QrCode, Chave: String): String;
    function  Criptografia(mStr, mChave: string): string;
    function  CarregaTexto(): String;
    procedure SalvaTexto(Texto: String);
    procedure MostraEdicao();
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;
const
  CO_App_Name = 'com.google.zxing.client.android';
  CO_App = 'BDermMob';

implementation

{$R *.fmx}
function TFmPrincipal.Criptografia(mStr, mChave: string): string;
var
  i, j, TamanhoString, pos, PosLetra, TamanhoChave: Integer;
begin
  {$IFDEF MSWINDOWS}
  j := 0;
  {$ELSE}
  j := -1;
  {$ENDIF}
  Result := mStr;
  TamanhoString := Length(mStr);
  TamanhoChave := Length(mChave);
  //
  for i := 1 + j to TamanhoString + j do
  begin
    pos := (i mod TamanhoChave);
    if pos = j then
      pos := TamanhoChave;

    posLetra := ord(Result[i]) xor ord(mChave[pos]);
    if posLetra = j then
      posLetra := ord(Result[i]);

    Result[i] := chr(posLetra);
  end;
end;

procedure TFmPrincipal.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  (* N�o est� funcionando
  CanClose := False;
  MessageDlg('Deseja realmente fechar a aplica��o?',
    System.UITypes.TMsgDlgType.mtInformation,
    [System.UITypes.TMsgDlgBtn.mbYes, System.UITypes.TMsgDlgBtn.mbNo], 0,
    procedure(const BotaoPressionado: TModalResult)
    begin
      case BotaoPressionado of
        mrYes:
        begin
          SharedActivity.Finish;
        end;
        mrNo:
        begin
          ShowMessage('Voc� respondeu n�o');
        end;
      end;
    end
  );
  *)
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SbQrCode.Visible := True;
  QrCode           := TAndroidBarcodeScanner.Create(true);
  QrCode.OnBarcode := MostraBarcode;
  {$ELSE}
  SbQrCode.Visible := False;
  {$ENDIF}
  //
  MostraEdicao();
end;

procedure TFmPrincipal.MostraEdicao;
var
  Codigo: String;
begin
  Codigo := CarregaTexto;
  //
  EdNome.Text := Codigo;
  //
  if Codigo <> '' then
    TabControl1.ActiveTab := TabItem2
  else
    TabControl1.ActiveTab := TabItem1;
end;

{$IF DEFINED(iOS) or DEFINED(ANDROID)}
procedure TFmPrincipal.MostraBarcode(Sender: TAndroidBarcodeScanner;
  ABarcode: string);
var
  Texto: String;
begin
  Texto := QrCodeDecode(ABarcode, EdNome.Text);
  //
  MeDados.Text := Texto;
end;
{$ENDIF}

{$IF DEFINED(iOS) or DEFINED(ANDROID)}
procedure TFmPrincipal.AbrirAppLoja(const AppNome: string);
var
  Comando: String;
  Intent: JIntent;
begin
  Comando:= 'market://details?id='+ AppNome;
  Intent := TJIntent.Create;
  Intent.setAction(TJIntent.JavaClass.ACTION_VIEW);
  Intent.setData(TJnet_Uri.JavaClass.parse(StringToJString(Comando)));
  MainActivity.startActivity(Intent);
end;
{$ENDIF}

{$IF DEFINED(iOS) or DEFINED(ANDROID)}
function TFmPrincipal.AppEstaInstalado(const AppNome: string): Boolean;
var
  PackageManager: JPackageManager;
begin
  PackageManager := SharedActivity.getPackageManager;
  try
    PackageManager.getPackageInfo(StringToJString(AppNome), TJPackageManager.JavaClass.GET_ACTIVITIES);
    Result := True;
  except
    on Ex: Exception do
      Result := False;
  end;
end;
{$ENDIF}

function TFmPrincipal.QrCodeDecode(const QrCode, Chave: String): String;
var
  I, n: Integer;
  Txt, Txt2, Txt3: String;
begin
  Txt  := QrCode;
  Txt3 := '';

  for I := 1 to (Length(Txt) div 2) do
  begin
    n := I * 2 -1;
    Txt2 := Copy(Txt, n, 2);

    Txt3 := Txt3 + Char(StrToInt('$' + Txt2));
  end;

  Txt2 := Criptografia(Txt3, Chave);

  Result := Txt2;
end;

function TFmPrincipal.CarregaTexto(): String;
var
  TextFile: TStringList;
  FileName: string;
begin
  Result   := '';
  TextFile := TStringList.Create;
  try
    FileName := TPath.Combine(TPath.GetHomePath, CO_App + '.txt');
    if FileExists(FileName) then
    begin
      TextFile.LoadFromFile(FileName);
      Result := stringReplace(TextFile.Text, sLineBreak, '', [rfReplaceAll]);
    end;
  finally
    TextFile.Free;
  end;
end;

procedure TFmPrincipal.SalvaTexto(Texto: String);
var
  TextFile: TStringList;
  Arquivo: string;
begin
  TextFile := TStringList.Create;
  try
    Arquivo := TPath.Combine(TPath.GetHomePath, CO_App + '.txt');
    TextFile.Text := Trim(Texto);
    TextFile.SaveToFile(Arquivo);
  finally
    TextFile.Free;
  end;
end;

procedure TFmPrincipal.SbQrCodeClick(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  if not AppEstaInstalado(CO_App_Name) then
  begin
    AbrirAppLoja(CO_App_Name);
  end else
  begin
    QrCode.Scan;
  end;
  {$ENDIF}
end;

procedure TFmPrincipal.SBSalvarClick(Sender: TObject);
var
  Codigo: String;
begin
  Codigo := EdNome.Text;
  //
  if Codigo = '' then
  begin
    ShowMessage('C�digo n�o definido!');
    Exit;
  end;
  SalvaTexto(Codigo);
  //
  TabControl1.ActiveTab := TabItem2;
end;

end.
