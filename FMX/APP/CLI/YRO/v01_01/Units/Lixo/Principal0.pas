unit Principal0;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Gestures,
  UnFMX_Geral, FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls,
  UnTemp, FMX.Layouts, FMX.Edit, Datasnap.DBClient, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.DialogService,
  FMX.Styles, FMX.Consts,
  UnProjGroupEnums, IPPeerClient, REST.Backend.PushTypes, System.JSON,
  REST.Backend.EMSPushDevice, System.PushNotification, REST.Backend.EMSProvider,
  Data.Bind.Components, Data.Bind.ObjectScope, REST.Backend.BindSource,
  REST.Backend.PushDevice,
  // temp
  FMX.Platform, FMX.VirtualKeyboard,
  // fim Temp
  UnAPP_PF,
  UnDmkEnums, FMX.Media, UnFMX_Grl_Vars;

type
  TFmPrincipal0 = class(TForm)
    GestureManager1: TGestureManager;
    TimerLogin: TTimer;
    PnShow: TPanel;
    GridPanelLayout1: TGridPanelLayout;
    LaUsuario: TLabel;
    PnStart: TPanel;
    ImgShow: TImage;
    ImgConfeccao: TImage;
    Image7: TImage;
    ImgTecelagem: TImage;
    ImgTinturaria: TImage;
    ImgConfeccaoReOpn: TImage;
    ImgTecelagemReOpn: TImage;
    ImgTinturariaReOpn: TImage;
    ImgConfeccaoSnt: TImage;
    ImgTecelagemSnt: TImage;
    ImgTinturariaSnt: TImage;
    ImgUploadFaccao: TImage;
    ImgDownloadAll: TImage;
    ImgUploadTextil: TImage;
    ImgVerifiDB: TImage;
    ImgOpcoes: TImage;
    PushEvents1: TPushEvents;
    EMSProvider1: TEMSProvider;
    Panel7: TPanel;
    MePushNotifications: TMemo;
    Panel8: TPanel;
    Button1: TButton;
    LayNotif: TLayout;
    LayImgChmOcoCab: TLayout;
    ImgChmOcoCab: TImage;
    LayChmOcoCad: TLayout;
    CirChmOcoCab_: TCircle;
    LaChmOcoCab: TLabel;
    Button2: TButton;
    CdTeste: TClientDataSet;
    CdTesteCodigo: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure ImgOvelhaClick(Sender: TObject);
    procedure TimerLoginTimer(Sender: TObject);
    procedure ImgDownloadAllClick(Sender: TObject);
    procedure ImgOpcoesClick(Sender: TObject);
    procedure ImgUploadFaccaoClick(Sender: TObject);
    procedure ImgTecelagemReOpnClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure ImgConfeccaoReOpnClick(Sender: TObject);
    procedure ImgTecelagemClick(Sender: TObject);
    procedure ImgUploadTextilClick(Sender: TObject);
    procedure ImgConfeccaoClick(Sender: TObject);
    procedure ImgVerifiDBClick(Sender: TObject);
    procedure ImgTinturariaReOpnClick(Sender: TObject);
    procedure ImgConfeccaoSntClick(Sender: TObject);
    procedure ImgTecelagemSntClick(Sender: TObject);
    procedure ImgTinturariaSntClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImgTinturariaClick(Sender: TObject);
    procedure ImgChmOcoCabClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure LaChmOcoCabClick(Sender: TObject);
    procedure CirChmOcoCab_Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    FTentativas: Integer;
    //
    procedure AddToMemo(Texto: String);
  public
    { Public declarations }
    FPushNotificationsCodigo: Integer;
    FPushNotificationsNoLocl: String;
    FPushNotificationsAction: TAcaoDeInstrucaoDeNotificacaoMobile;
  end;

var
  FmPrincipal0: TFmPrincipal0;
  //

implementation

uses UnFMX_CfgDBApp, Module, UnGrl_DmkDB, MyListas, UnFMX_DmkRemoteQuery,
  // Teste
  ChmOcoPux,
  // fim teste
  UnFMX_DmkForms, UnApp_Jan, UnGrl_Vars, UnFMX_DmkProcFunc,
  UnFMX_PushNotifications, UnOVSM_PF,
  LoginApp, Upload,
  UnMyLinguas, UnGrl_OS, UnOVSM_Vars, UnGrl_Geral,
  UnREST_App, UnGrl_AllOS;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.iPhone55in.fmx IOS}
{$R *.Windows.fmx MSWINDOWS}
{$R *.Surface.fmx MSWINDOWS}

procedure TFmPrincipal0.AddToMemo(Texto: String);
begin
{
  Memo1.Text := Texto + sLineBreak + Memo1.Text;
  Application.ProcessMessages;
}
end;

procedure TFmPrincipal0.Button1Click(Sender: TObject);
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmChmOcoPux, FmChmOcoPux,
  fcmCreateTryShwM, True, False);
end;

procedure TFmPrincipal0.Button2Click(Sender: TObject);
begin
  FMX_dmkRemoteQuery.QuerySelect(CdTeste, VAR_CNPJ_DEVICE_IN_SERVER,
    [
      'SELECT Codigo ',
      'FROM entidades ',
      'WHERE Codigo=999999999 ',
      EmptyStr
    ], VAR_AMBIENTE_APP);
  Grl_Geral.MB_Teste(Grl_Geral.FF0(CdTesteCodigo.Value));
end;

{ KeyBoard
var
  KeyBoard: IFMXVirtualKeyboardService;
begin
 KeyBoard := TPlatformServices.Current.GetPlatformService(IFMXVirtualKeyboardService) as IFMXVirtualKeyboardService;
 KeyBoard.ShowVirtualKeyboard(MePushNotifications);
end;
}

{ Push Teste 99 msg)
var
  Instrucoes: TArr1NivJSON;
begin
  SetLength(Instrucoes, 13);
  Instrucoes[0][0] := 'google.delivered_priority';
  Instrucoes[0][1] := 'normal';

  Instrucoes[01][0] := 'google.sent_time';
  Instrucoes[01][1] := '1578082534975';

  Instrucoes[02][0] := 'google.ttl';
  Instrucoes[02][1] := '2419200';

  Instrucoes[03][0] := 'google.original_priority';
  Instrucoes[03][1] := 'normal';

  Instrucoes[04][0] := 'id';
  Instrucoes[04][1] := '205807968282';

  Instrucoes[05][0] := 'from';
  Instrucoes[05][1] := '205807968282';

  Instrucoes[06][0] := 'google.message_id';
  Instrucoes[06][1] := '0:1578082534996093%eef72599f9fd7ecd';

  Instrucoes[07][0] := 'message';
  Instrucoes[07][1] := 'Suspendisse bibendum elementum enim maximus sagittis ante tristique nec. Suspendisse bibendum elemen';

  Instrucoes[08][0] := 'DmkXtra001';
  Instrucoes[08][1] := 'chmococad';

  Instrucoes[09][0] := 'DmkXtra002';
  Instrucoes[09][1] := '1';

  Instrucoes[10][0] := 'DmkXtra003';
  Instrucoes[10][1] := 'AURORA BOREAL EXT IND. MONT. EQUIP. PAUL';

  Instrucoes[11][0] := 'DmkXtra004';
  Instrucoes[11][1] := '2019-12-26 00:00:00';

  Instrucoes[12][0] := 'DmkXtra005';
  Instrucoes[12][1] := '2020-02-22 00:00:00';

  App_PF.PushNotifications_ReceiveDestrincha2(Instrucoes);

    FmPrincipal.LaChmOcoCab.HitTest := True; // caso coloque false sem querer
    FmPrincipal.LaChmOcoCab.Text := '99';//Grl_Geral.FF0(QrChmOcoPux.RecordCount);
    FmPrincipal.LayChmOcoCad.Visible := True;
end;
}

procedure TFmPrincipal0.CirChmOcoCab_Click(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormChmOcoPux();
end;

procedure TFmPrincipal0.FormActivate(Sender: TObject);
begin
{$IfDef MSWINDOWS}
  // nada...
{$Else}
  FMX_PushNotifications.PushNotifications_FormActivate(Sender);
{$EndIf}
end;

procedure TFmPrincipal0.FormCreate(Sender: TObject);
  function LarguraTela(): Integer;
  var
    ScreenSize: TSize;
  begin
    ScreenSize := Screen.Size;
    //Caption := IntToStr(ScreenSize.Width) + '*' + IntToStr(ScreenSize.Height);
    Result := ScreenSize.Width;
    //Grl_Geral.MB_Info('Altura = ' + IntToStr(ScreenSize.Height));
  end;
begin
  FMX_Grl_Vars.ConfiguraCoresJanelas(0); //-1 teste de cor!); //0 Padr�o Ambiente de produ��o);
{$IfDef MSWINDOWS}
{$Else}
  VAR_PushNotifications_CodigoProjeto := '205807968282';
  VAR_PushNotifications_Memo := MePushNotifications;
  FMX_PushNotifications.PushNotifications_FormCreate(
    (*CodigoProjetoGoogle = '205807968282'*)VAR_PushNotifications_CodigoProjeto,
    PushEvents1, EMSProvider1);
  //FMX_PushNotifications.ObtemTodasNotificacoesStartapp(MePushNotifications);
{$EndIf}
  //
  VAR_AMBIENTE_APP  := 0; // Produ��o
  VAR_ERPNameByCli  := Application.Title;
  //VAR_SCALE_X       := Screen.Width / 360;
  VAR_SCALE_X       := LarguraTela() / 360;
  VAR_SCALE_Y       := VAR_SCALE_X;
  //vResourceStream := TResourceStream.Create( HInstance, 'LIGHT_Style', RT_RCDATA );
  //
  MyLinguas.GetInternationalStrings(myliPortuguesBR);
  //
  VAR_IMEI_DEVICE_IN_SERVER := Grl_OS.ObtemIMEI_MD5();
  //
  FTentativas := 0;
  //
  //LaChmOcoCab.Visible  := False;
  //CirChmOcoCab.Visible := False;
  LayChmOcoCad.Visible := False;
  LayChmOcoCad.Position.Y := 0;
  LayChmOcoCad.Position.X := LayNotif.Width - LayChmOcoCad.Width; // 96 - 40 = 56
  PnShow.Visible       := False;
  PnShow.Align         := TAlignLayout.Client;
  PnStart.Align        := TAlignLayout.alClient; // 2019-11-08
  PnStart.Visible      := True;
  //
  //
  Application.CreateForm(TDmod, Dmod);
  FMX_CfgDBApp.ConfiguraBD(Dmod.AllDB, Dmod.MyDB, Dmod.QrLoc, Dmod.QrUpd,
    Dmod.QrUsuarios, Dmod.QrControle, Dmod.QrEspecificos, LaUsuario);
  //
  TimerLogin.Enabled := True;
  //
  //TStyleManager.SetStyleFromFile('Air.style');
end;

procedure TFmPrincipal0.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.OcultaFormPrincipal(Sender, Key, KeyChar, Shift);
{
var
  Intent: JIntent;
begin
  if Key = vkHardwareBack then
  begin
    //MainActivity.movetasktoback(true);
    //Second possibility is:
    Intent := TJIntent.Create;
    Intent.setAction(TJIntent.JavaClass.ACTION_MAIN);
    Intent.addCategory(TJIntent.JavaClass.CATEGORY_HOME);
    TAndroidHelper.Activity.startActivity(Intent);
    //
    Key := 0;
  end;
}
end;

procedure TFmPrincipal0.FormShow(Sender: TObject);
begin
  GridPanelLayout1.Enabled := True;
{$IfDef MSWINDOWS}
  // nada...
{$Else}
  FMX_PushNotifications.PushNotifications_FormShow(Sender);
{$EndIf}
  Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, EmptyStr);
end;

procedure TFmPrincipal0.ImgDownloadAllClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormREST_Down(TAcaoDeInstrucaoDeNotificacaoMobile.ainmIndef, 0);
  //App_Jan.MostraFormREST_Down(TAcaoDeInstrucaoDeNotificacaoMobile.ainmChamadoOcorrencia, 2);
end;

procedure TFmPrincipal0.ImgChmOcoCabClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormChmOcoCab(0);
end;

procedure TFmPrincipal0.ImgConfeccaoClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormOViLocais(
    TSegmentoInsp.sgminspFaccao,
    TSeccaoInsp.sccinspConfeccao
  );
end;

procedure TFmPrincipal0.ImgConfeccaoReOpnClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmIspMobCab(Qry, TStatusAndamentoInspecao.saiInspAberta) then
      App_Jan.MostraFormOViIspCenOpn()
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es de confec��o em andamento!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgConfeccaoSntClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmIspMobCab(Qry, TStatusAndamentoInspecao.saiFinalizada) then
      App_Jan.MostraFormOViIspCenSnt()
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es finalizadas!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgOpcoesClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormOpcoesOVSM();
end;

procedure TFmPrincipal0.ImgTecelagemReOpnClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiInspAberta,
    TSeccaoInsp.sccinspTecelagem) then
      App_Jan.MostraFormOViItxCenOpn(
        TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTecelagem)
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es de tecelagem em andamento!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgTecelagemSntClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiFinalizada,
    TSeccaoInsp.sccinspTecelagem) then
      App_Jan.MostraFormOViItxCenSnt(
        TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTecelagem)
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es finalizadas!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgOvelhaClick(Sender: TObject);
{
const
  Inspecao = 107;
var
  TipoEnvio: TTipoEnvioEmailInspecao;
  Res: String;
  EmailsExtra: String;
begin
  FMX_DmkPF.VibrarComoBotao();
  EmailsExtra := '';
  if EmailsExtra <> EmptyStr then
    TipoEnvio := TTipoEnvioEmailInspecao.smerAll
  else
    TipoEnvio := TTipoEnvioEmailInspecao.smerEnti;
  //
  Res := REST_App.EnviaEmailResultadoInspecao(Inspecao, EmailsExtra,
  TTipoEnvioEmailInspecao.smerEnti);
  //
  Grl_Geral.MB_Info(Res);
(*
  'marco@dermatek.com.br;arendemilly@gmail.com');
  //'marco@dermatek.com.br;arendemilly@gmail.com');
  //'marco@dermatek.com.br');
  //'');
*)
}
{
var
  I, N: Integer;
  Form: TForm;
}
begin
{
  AddToMemo('Application.MainForm ' + Application.MainForm.Name);
  //if Key = vkHardwareBack then
  begin
    I := 0;
    //Fechou := True;
    //while Fechou = True do
    begin
      I := I + 1;
      //Fechou := False;
      for N := 0 to Application.ComponentCount-1 do
      begin
        if Application.Components[N] is TForm then
        begin
          AddToMemo('Form ' + TForm(Application.Components[N]).Name);
          Form := TForm(Application.Components[N]);
          if (Form <> Application.MainForm)  // FmPrincipal_LgXhdpiPh
          and (Form <> FmLoginApp) then   // FmPrincipal_LgXhdpiPh
          begin
            AddToMemo('Fechando janela ' + Grl_Geral.FF0(I) + ': ' + Form.Name);
            FMX_DmkForms.DestroiFm_AllOS0(Form);
            //Fechou := True;
          end;
        end
        else
          AddToMemo('????? ' + TComponent(Application.Components[N]).Name);
      end;
    end;
    //AddToMemo('Destruindo m�dulo de dados.');
    //DMod.Destroy;
    //AddToMemo('Destruindo janela principal.');
    //Application.MainForm.DisposeOf;
    Grl_AllOS.EncerraApplicacao();
    //FMX.TApplication.Terminate();
    //Application.MainForm.DisposeOf;
    //FreeAndNil(Application);
  end;
}
{
  Application.CreateForm(TFmUpload, FmUpload);
  FmUpload.FDirectory  := 'proforma';
  FmUpload.Show;
}
end;

procedure TFmPrincipal0.ImgTecelagemClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormOViLocais(
    TSegmentoInsp.sgminspTextil,
    TSeccaoInsp.sccinspTecelagem
    );
end;

procedure TFmPrincipal0.ImgTinturariaClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormOViLocais(
    TSegmentoInsp.sgminspTextil,
    TSeccaoInsp.sccinspTinturaria
    );
end;

procedure TFmPrincipal0.ImgTinturariaReOpnClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiInspAberta,
    TSeccaoInsp.sccinspTinturaria) then
      App_Jan.MostraFormOViItxCenOpn(
        TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTinturaria)
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es de tinturaria em andamento!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgTinturariaSntClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiFinalizada,
    TSeccaoInsp.sccinspTinturaria) then
      App_Jan.MostraFormOViItxCenSnt(
        TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTinturaria)
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es finalizadas!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgUploadFaccaoClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  Qry := TFDQuery.Create(Dmod);
  try
    if Dmod.ReopenOVmIspMobCab(Qry, TStatusAndamentoInspecao.saiInspRealizada) then
      App_Jan.MostraFormREST_UP_Faccao()
     else
      Grl_Geral.MB_Aviso('N�o h� inspe��es FAC��O aptas ao upload!');
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal0.ImgVerifiDBClick(Sender: TObject);
const
  DBUsu = nil;
begin
  FMX_DmkPF.VibrarComoBotao();
  FMX_CfgDBApp.MostraVerifyDB(True, Dmod.AllDB, DBUsu);
end;

procedure TFmPrincipal0.LaChmOcoCabClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormChmOcoPux();
end;

procedure TFmPrincipal0.TimerLoginTimer(Sender: TObject);
begin
  TimerLogin.Enabled := False;
  //
  OVSM_PF.Login();
end;

//Fazer observa��o na inspe��o ao finalizar!

// Floating Action Button no Delphi / Firemonkey
// https://www.youtube.com/watch?v=R-HHRZnPrQE

// Envio de mensagem whatsapp
//https://www.landersongomes.com.br/embarcadero/delphi/integrando-aplicativo-delphi-xe7-android-com-whatsapp

end.
