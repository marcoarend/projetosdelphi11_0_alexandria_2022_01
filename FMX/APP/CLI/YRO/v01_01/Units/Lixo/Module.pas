unit Module;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.FMXUI.Wait, FireDAC.Comp.UI, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.Controls.Presentation,
  FMX.StdCtrls;

type
  TDmod = class(TForm)
    MyDB: TFDConnection;
    QrLoc: TFDQuery;
    QrAux: TFDQuery;
    QrUpd: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    QrUsuarios: TFDQuery;
    QrControle: TFDQuery;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConfiguraDispositivo(LaUsuario: TLabel);
    procedure MostraVerifyDB(Verifica: Boolean);
    procedure ReopenControle();
    procedure ReopenUsuarios();
    procedure ConfiguraBD(LaUsuario: TLabel);
  end;

var
  Dmod: TDmod;

implementation

uses (*UnAllOS_DmkDB,*)
MyListas, VerifyDB, UnGeral, UnGrl_Vars, UnAllOS_DmkProcFunc,
  DmkLogin;


{$R *.fmx}

{ TForm1 }

procedure TDmod.ConfiguraBD(LaUsuario: TLabel);
var
  Versao: Integer;
begin
  AllOS_dmkDB.ConfiguraDB(MyDB, istSQLite, CO_DBNome);

  Versao := AllOS_dmkDB.ObtemVersaoAppDB(QrLoc, MyDB, istSQLite);

  if Versao < CO_VERSAO then
  begin
    MostraVerifyDB(False);
  end else
  begin
    ConfiguraDispositivo(LaUsuario);
  end;
end;

procedure TDmod.ConfiguraDispositivo(LaUsuario: TLabel);
var
  Res: Boolean;
begin
  ReopenUsuarios;
  ReopenControle;
  //
  Geral.DefineFormatacoes;
  //
  if (QrControle.RecordCount > 0) and (QrUsuarios.RecordCount > 0) then
    Res := True
  else
    Res := False;
  //
  if not Res then
  begin
    VAR_WEB_USER_ID   := 0;
    VAR_WEB_TOKEN_DMK := '';
    LaUsuario.Text    := '';
    //
    AllOS_dmkPF.CriaFm_AllOS(TFmDmkLogin, FmDmkLogin, True);
  end else
    LaUsuario.Text := QrUsuarios.FieldByName('UserName').AsString;
end;

procedure TDmod.MostraVerifyDB(Verifica: Boolean);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FullScreen := True;
  FmVerifyDB.Show;
  {$ELSE}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FullScreen := False;
  FmVerifyDB.ShowModal;
  FmVerifyDB.Destroy;
  {$ENDIF}
end;

procedure TDmod.ReopenControle;
begin
  AllOS_dmkDB.AbreSQLQuery0(QrControle, MyDB, [
    'SELECT Device, Versao ',
    'FROM controle ',
    '']);
  if QrControle.RecordCount > 0 then
    VAR_WEB_DEVICE_ID   := QrControle.FieldByName('Device').AsInteger;
end;

procedure TDmod.ReopenUsuarios;
begin
  AllOS_dmkDB.AbreSQLQuery0(QrUsuarios, MyDB, [
    'SELECT Codigo, UserName, Tipo, UsrEnt, UsrID, Token ',
    'FROM Usuarios ',
    'WHERE Atual = 1 ',
    '']);
  if QrUsuarios.RecordCount > 0 then
  begin
    VAR_USUARIO         := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USER_ID     := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USER_USRENT := QrUsuarios.FieldByName('UsrEnt').AsInteger;
    VAR_WEB_USER_USRID  := QrUsuarios.FieldByName('UsrID').AsInteger;
    VAR_WEB_USER_TIPO   := QrUsuarios.FieldByName('Tipo').AsInteger;
    VAR_WEB_TOKEN_DMK   := QrUsuarios.FieldByName('Token').AsString;
  end;
end;

end.
