unit UnOVSM_PF;

interface

uses System.SysUtils, System.JSON, Variants, UnProjGroupEnums;

type
  TUnOVSM_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
{
    function  DefineNomeArquivoFotoInconformidade(CodInMob, CtrlInMob:
              Integer; Data: Variant): String;
    function  GetNomeTabSegmento_InspGerCab(SegmentoInsp: TSegmentoInsp): String;
    function  GetNomeTabSegmento_InspMobCab(SegmentoInsp: TSegmentoInsp): String;
    function  GetRandomStr(): String;
    procedure JsonTeste();
    procedure Login();
    function  ObtemCodigoDeAceiteNoServidor(): Boolean;
}

  end;
var
  OVSM_PF: TUnOVSM_PF;

implementation

uses Module, UnGrl_DmkDB, UnOVSM_Vars, UnApp_Jan, UnFMX_Geral, UnFMX_CfgDBApp,
  UnGrl_Vars, UnGrl_Geral, UnApp_PF;

{ TUnOVSM_PF }

(*
function TUnOVSM_PF.DefineNomeArquivoFotoInconformidade(CodInMob, CtrlInMob:
  Integer; Data: Variant): String;
var
  DataHora: String;
begin
  Result := 'OVS_' +
            Grl_Geral.FFN(CodInMob, 10) + '_' +
            Grl_Geral.FFN(CtrlInMob, 10);
  if Data <> null then
  begin
    DataHora := FormatDateTime('_YYYYMMDD_HHNNSS', Data);
    Result := Result + DataHora + '.bmp';
  end;
end;

function TUnOVSM_PF.GetNomeTabSegmento_InspGerCab(
  SegmentoInsp: TSegmentoInsp): String;
const
  sProcName = 'TUnOVSM_PF.GetNomeTabSegmento_InspGerCab()';
begin
  case SegmentoInsp of
    //sgminspNone=0,
    sgminspFaccao{1024}: Result := 'ovgispgercab';
    sgminspTextil{2048}: Result := 'ovgitxgercab';
    else
    begin
      Result := 'ovg???gercab';
      Grl_Geral.MB_Erro('SegmentoInsp n�o implementado em ' + sProcName);
    end;
  end;
end;

function TUnOVSM_PF.GetNomeTabSegmento_InspMobCab(
  SegmentoInsp: TSegmentoInsp): String;
const
  sProcName = 'TUnOVSM_PF.GetNomeTabSegmento_InspMobCab()';
begin
  case SegmentoInsp of
    //sgminspNone=0,
    sgminspFaccao{1024}: Result := 'ovmispmobcab';
    sgminspTextil{20418}: Result := 'ovmitxmobcab';
    else
    begin
      Result := 'ovg???mobcab';
      Grl_Geral.MB_Erro('SegmentoInsp n�o implementado em ' + sProcName);
    end;
  end;
end;

function TUnOVSM_PF.GetRandomStr(): String;
{
  function Randomico(): Integer;
  var
    tc: Int64;
  begin
    tc := GetTickCount();
    Result := tc mod 62;
  end;
}
  function Randomico(): Integer;
  begin
    {$IfDef MSWINDOWS}
      Result := Random(62) + 1;
    {$Else}
      Result := Random(62);
    {$EndIF}
  end;
const
  StrLen = 32;
var
  Str, sAnt, sAtu: String;
begin
  Str := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  Result := '';
  sAnt := '';
  Randomize;
  repeat
    //Randomize;
    //sAtu := Str[Random(Length(Str)) + 1];
    sAtu := Str[Randomico()];
    if sAtu = sAnt then
    begin
      while sAtu = sAnt do
      begin
        Randomize;
        //sAtu := Str[Random(Length(Str)) + 1];
        sAtu := Str[Randomico()];
      end;
    end;
    Result := Result + sAtu;
    sAnt := sAtu;
  until (Length(Result) = StrLen);
end;

procedure TUnOVSM_PF.JsonTeste();
var
  jsonObj, jSubObj: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  i: Integer;
begin
{
  jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Memo1.Text), 0) as TJSONObject;

  jv := jsonObj.Get('ATIVIDADES_SECUNDARIAS').JsonValue;
  ja := jv as TJSONArray;

  ListBox1.Clear;

  for i := 0 to ja.Size - 1 do
  begin
    jSubObj := (ja.Get(i) as TJSONObject);
    jv := jSubObj.Get(0).JsonValue;
    ListBox1.Items.Add(jv.Value);
  end;
}
end;

procedure TUnOVSM_PF.Login();
begin
  if Grl_DmkDB.TabelaExiste_SQLite2('ovcmobdevatv', Dmod.QrAux, Dmod.AllDB) then
  begin
    if OVSM_PF.ObtemCodigoDeAceiteNoServidor() then
        App_Jan.MostraFormLoginApp()
    else
      ;//FMX_Geral.MB_Erro('Falha ao obter dados do usu�rio (B)!');
  end else
  begin
    FMX_CfgDBApp.VerifyDBSemMostrar(True, Dmod.AllDB, {DBUsu} nil);
    //
    if Grl_DmkDB.TabelaExiste_SQLite2('ovcmobdevatv', Dmod.QrAux, Dmod.AllDB) then
    begin
      if OVSM_PF.ObtemCodigoDeAceiteNoServidor() then
        App_Jan.MostraFormLoginApp()
      else
        FMX_Geral.MB_Erro('Falha ao obter dados do usu�rio (C)!');
    end else
  end;
end;

function TUnOVSM_PF.ObtemCodigoDeAceiteNoServidor(): Boolean;
begin
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
  'SELECT Codigo, PIN, UserNmePdr, CNPJ, ',
  // Opcoes OVSM
  'ScaleX, ScaleY, FoLocObjIns ',
  // Fim Opcoes OVSM
  'FROM ovcmobdevatv ',
  'ORDER BY Codigo DESC',
  '']);
  //
  if Dmod.QrAux.RecordCount > 0 then
  begin
    VAR_COD_DEVICE_IN_SERVER := Dmod.QrAux.FieldByName('Codigo').AsInteger;
    VAR_PIN_DEVICE_IN_SERVER := Dmod.QrAux.FieldByName('PIN').AsString;
    VAR_NIK_DEVICE_IN_SERVER := Dmod.QrAux.FieldByName('UserNmePdr').AsString;
    VAR_CNPJ_DEVICE_IN_SERVER := Dmod.QrAux.FieldByName('CNPJ').AsString;
    Result := True;
    VAR_OPCOES_MOB_FoLocObjIns := Dmod.QrAux.FieldByName('FoLocObjIns').AsInteger;
    //VAR_OPCOES_MOB_ScaleX      := Dmod.QrAux.FieldByName('ScaleX').AsInteger / 100;
    //VAR_OPCOES_MOB_ScaleY      := Dmod.QrAux.FieldByName('ScaleY').AsInteger / 100;
  end else
  begin
    VAR_COD_DEVICE_IN_SERVER := 0;
    VAR_PIN_DEVICE_IN_SERVER := '';
    //VAR_NIK_DEVICE_IN_SERVER := 'Usu�rio';
    VAR_CNPJ_DEVICE_IN_SERVER := '';
    //
    VAR_OPCOES_MOB_FoLocObjIns := 0;
    //VAR_OPCOES_MOB_ScaleX      := 1.0;
    //VAR_OPCOES_MOB_ScaleY      := 1.0;
    //
    App_Jan.MostraFormLoginSvr();
    //
    Result := VAR_COD_DEVICE_IN_SERVER <> 0;
  end;
  if Result then
    App_PF.PushNotifications_RegistraDevice();

end;
*)

end.
