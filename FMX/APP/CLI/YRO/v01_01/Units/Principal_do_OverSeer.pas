unit Principal_do_OverSeer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Layouts, FMX.Ani,
  System.Math.Vectors, FMX.Controls3D, FMX.Layers3D, IPPeerClient,
  REST.Backend.PushTypes, System.JSON, REST.Backend.EMSPushDevice,
  System.PushNotification, REST.Backend.EMSProvider, Data.Bind.Components,
  Data.Bind.ObjectScope, REST.Backend.BindSource, REST.Backend.PushDevice,
  //dmk
  UnProjGroupEnums, UnDmkEnums,
  // Fim dmk
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFmPrincipal = class(TForm)
    Circle2: TCircle;
    ImgMenu: TImage;
    LayMenu: TLayout;
    RectOpacity: TRectangle;
    ImgUploadChamado: TImage;
    ImgVerifiDB: TImage;
    ImgOpcoes: TImage;
    AnimaMenu: TFloatAnimation;
    LayPrincipal: TLayout;
    LayInspecao: TLayout;
    Layout19: TLayout;
    Layout20: TLayout;
    Layout21: TLayout;
    Layout22: TLayout;
    ImgInspecao: TImage;
    Layout23: TLayout;
    ImgInspecaoSnt: TImage;
    ImgRetomarInspecao: TImage;
    Layout25: TLayout;
    Layout26: TLayout;
    Layout27: TLayout;
    ImgUpload: TImage;
    GridPanelLayout1: TGridPanelLayout;
    Layout4: TLayout;
    Layout16: TLayout;
    ImgDownloadAll: TImage;
    Layout6: TLayout;
    Layout10: TLayout;
    ImgConfeccao: TImage;
    Layout11: TLayout;
    ImgTecelagem: TImage;
    Layout12: TLayout;
    ImgTinturaria: TImage;
    Layout17: TLayout;
    LayNotif: TLayout;
    LayImgChmOcoCab: TLayout;
    ImgChmOcoCab: TImage;
    LayChmOcoCad: TLayout;
    CirChmOcoCab_: TCircle;
    LaChmOcoCab: TLabel;
    PnStart: TPanel;
    ImgShow: TImage;
    TimerLogin: TTimer;
    PushEvents1: TPushEvents;
    EMSProvider1: TEMSProvider;
    LayShow: TLayout;
    LaUsuario: TLabel;
    ImgCorDmk: TImage;
    procedure ImgOpcoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AnimaMenuFinish(Sender: TObject);
    procedure ImgMenuClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure TimerLoginTimer(Sender: TObject);
    procedure ImgInspecaoClick(Sender: TObject);
    procedure ImgRetomarInspecaoClick(Sender: TObject);
    procedure ImgConfeccaoClick(Sender: TObject);
    procedure ImgTecelagemClick(Sender: TObject);
    procedure ImgTinturariaClick(Sender: TObject);
    procedure ImgUploadClick(Sender: TObject);
    procedure ImgInspecaoSntClick(Sender: TObject);
    procedure ImgUploadChamadoClick(Sender: TObject);
    procedure ImgVerifiDBClick(Sender: TObject);
    procedure LaChmOcoCabClick(Sender: TObject);
    procedure ImgChmOcoCabClick(Sender: TObject);
    procedure ImgDownloadAllClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CirChmOcoCab_Click(Sender: TObject);
  private
    { Private declarations }
    FSegmentoInsp: TSegmentoInsp;
    FSeccaoInsp: TSeccaoInsp;
    //
    procedure AnimaOMenu();
    procedure MostraBotoesCircle(Visivel: Boolean);
    procedure MostraLayMenu();
    procedure OcultaAnimaMenu();

  public
    { Public declarations }
    FPushNotificationsCodigo: Integer;
    FPushNotificationsNoLocl: String;
    FPushNotificationsAction: TAcaoDeInstrucaoDeNotificacaoMobile;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  UnGrl_Geral, UnGrl_Vars, UnGrl_OS, UnMyLinguas,
  UnFMX_PushNotifications, UnFMX_Grl_Vars, UnFMX_CfgDBApp, UnFMX_DmkProcFunc,
  UnApp_Jan, UnOVSM_PF,
  Module;

{$R *.fmx}

procedure TFmPrincipal.AnimaOMenu();
begin
  LayMenu.Position.Y := FmPrincipal.Height + 20;
  LayMenu.Visible := True;

  AnimaMenu.Inverse := False;
  AnimaMenu.StartValue := FmPrincipal.Height + 20;
  AnimaMenu.StopValue := 0;
  AnimaMenu.Start;
end;

procedure TFmPrincipal.CirChmOcoCab_Click(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormChmOcoPux();
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
{$IfDef MSWINDOWS}
  // nada...
{$Else}
  FMX_PushNotifications.PushNotifications_FormActivate(Sender);
{$EndIf}
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
  function LarguraTela(): Integer;
  var
    ScreenSize: TSize;
  begin
    ScreenSize := Screen.Size;
    //Caption := IntToStr(ScreenSize.Width) + '*' + IntToStr(ScreenSize.Height);
    Result := ScreenSize.Width;
    //Grl_Geral.MB_Info('Altura = ' + IntToStr(ScreenSize.Height));
  end;
begin
// Da vers�o Float :
  LayMenu.Visible      := False;
  RectOpacity.Visible  := True;
  LayPrincipal.Visible := True;
/// Da Vers�o Inicial
  FMX_Grl_Vars.ConfiguraCoresJanelas(0); //-1 teste de cor!); //0 Padr�o Ambiente de produ��o);
{$IfDef MSWINDOWS}
{$Else}
  VAR_PushNotifications_CodigoProjeto := '205807968282';
  VAR_PushNotifications_Memo := nil; //MePushNotifications;
  FMX_PushNotifications.PushNotifications_FormCreate(
    (*CodigoProjetoGoogle = '205807968282'*)VAR_PushNotifications_CodigoProjeto,
    PushEvents1, EMSProvider1);
  //FMX_PushNotifications.ObtemTodasNotificacoesStartapp(MePushNotifications);
{$EndIf}
  //
  VAR_AMBIENTE_APP  := 0; // Produ��o
  VAR_ERPNameByCli  := Application.Title;
  //VAR_SCALE_X       := Screen.Width / 360;
  VAR_SCALE_X       := LarguraTela() / 360;
  VAR_SCALE_Y       := VAR_SCALE_X;
  //vResourceStream := TResourceStream.Create( HInstance, 'LIGHT_Style', RT_RCDATA );
  //
  MyLinguas.GetInternationalStrings(myliPortuguesBR);
  //
  VAR_IMEI_DEVICE_IN_SERVER := Grl_OS.ObtemIMEI_MD5();
  //
  //FTentativas := 0;
  //
  //LaChmOcoCab.Visible  := False;
  //CirChmOcoCab.Visible := False;
  LayChmOcoCad.Visible := False;
  LayChmOcoCad.Position.Y := 0;
  LayChmOcoCad.Position.X := LayNotif.Width - LayChmOcoCad.Width; // 96 - 40 = 56
  LayShow.Visible       := False;
  LayShow.Align         := TAlignLayout.Client;
  PnStart.Align        := TAlignLayout.alClient; // 2019-11-08
  PnStart.Visible      := True;
  //
  //
  Application.CreateForm(TDmod, Dmod);
  FMX_CfgDBApp.ConfiguraBD(Dmod.AllDB, Dmod.MyDB, Dmod.QrLoc, Dmod.QrUpd,
    Dmod.QrUsuarios, Dmod.QrControle, Dmod.QrEspecificos, LaUsuario);
  //
  TimerLogin.Enabled := True;
  //
  //TStyleManager.SetStyleFromFile('Air.style');
end;

procedure TFmPrincipal.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.OcultaFormPrincipal(Sender, Key, KeyChar, Shift);
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  GridPanelLayout1.Enabled := True;
{$IfDef MSWINDOWS}
  // nada...
{$Else}
  FMX_PushNotifications.PushNotifications_FormShow(Sender);
{$EndIf}
  Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, EmptyStr);
end;

procedure TFmPrincipal.ImgChmOcoCabClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormChmOcoCab(0);
end;

procedure TFmPrincipal.ImgConfeccaoClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  FSegmentoInsp := TSegmentoInsp.sgminspFaccao;
  FSeccaoInsp   := TSeccaoInsp.sccinspConfeccao;
  //
  MostraLayMenu();
end;

procedure TFmPrincipal.ImgDownloadAllClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormREST_Down(TAcaoDeInstrucaoDeNotificacaoMobile.ainmIndef, 0,
  (*DownLoadAll*)True, (*UploadChamados*)False);
end;

procedure TFmPrincipal.ImgInspecaoClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  //
  case FSegmentoInsp of
    TSegmentoInsp.sgminspFaccao:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspConfeccao:
        begin
          App_Jan.MostraFormOViLocais(
            TSegmentoInsp.sgminspFaccao,
            TSeccaoInsp.sccinspConfeccao
          );
        end;
      end;
    end;
    TSegmentoInsp.sgminspTextil:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspTecelagem:
        begin
          App_Jan.MostraFormOViLocais(
            TSegmentoInsp.sgminspTextil,
            TSeccaoInsp.sccinspTecelagem
            );
        end;
        TSeccaoInsp.sccinspTinturaria:
        begin
          App_Jan.MostraFormOViLocais(
            TSegmentoInsp.sgminspTextil,
            TSeccaoInsp.sccinspTinturaria
            );
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.ImgInspecaoSntClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  //
  case FSegmentoInsp of
    TSegmentoInsp.sgminspFaccao:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspConfeccao:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmIspMobCab(Qry, TStatusAndamentoInspecao.saiFinalizada) then
              App_Jan.MostraFormOViIspCenSnt()
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es finalizadas!');
          finally
            Qry.Free;
          end;
        end;
      end;
    end;
    TSegmentoInsp.sgminspTextil:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspTecelagem:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiFinalizada,
            TSeccaoInsp.sccinspTecelagem) then
              App_Jan.MostraFormOViItxCenSnt(
                TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTecelagem)
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es finalizadas!');
          finally
            Qry.Free;
          end;
        end;
        TSeccaoInsp.sccinspTinturaria:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiFinalizada,
            TSeccaoInsp.sccinspTinturaria) then
              App_Jan.MostraFormOViItxCenSnt(
                TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTinturaria)
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es finalizadas!');
          finally
            Qry.Free;
          end;
        end;
      end;
    end;
  end;

end;

procedure TFmPrincipal.ImgOpcoesClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  App_Jan.MostraFormOpcoesOVSM();
end;


procedure TFmPrincipal.ImgRetomarInspecaoClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  //
  case FSegmentoInsp of
    TSegmentoInsp.sgminspFaccao:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspConfeccao:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmIspMobCab(Qry, TStatusAndamentoInspecao.saiInspAberta) then
              App_Jan.MostraFormOViIspCenOpn()
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es de confec��o em andamento!');
          finally
            Qry.Free;
          end;
        end;
      end;
    end;
    TSegmentoInsp.sgminspTextil:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspTecelagem:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiInspAberta,
            TSeccaoInsp.sccinspTecelagem) then
              App_Jan.MostraFormOViItxCenOpn(
                TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTecelagem)
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es de tecelagem em andamento!');
          finally
            Qry.Free;
          end;
        end;
        TSeccaoInsp.sccinspTinturaria:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiInspAberta,
            TSeccaoInsp.sccinspTinturaria) then
              App_Jan.MostraFormOViItxCenOpn(
                TSegmentoInsp.sgminspTextil, TSeccaoInsp.sccinspTinturaria)
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es de tinturaria em andamento!');
          finally
            Qry.Free;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.ImgTecelagemClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  FSegmentoInsp := TSegmentoInsp.sgminspTextil;
  FSeccaoInsp   := TSeccaoInsp.sccinspTecelagem;
  //
  MostraLayMenu();
end;

procedure TFmPrincipal.ImgTinturariaClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  FSegmentoInsp := TSegmentoInsp.sgminspTextil;
  FSeccaoInsp   := TSeccaoInsp.sccinspTinturaria;
  //
  MostraLayMenu();
end;

procedure TFmPrincipal.ImgUploadChamadoClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  App_Jan.MostraFormREST_Down(TAcaoDeInstrucaoDeNotificacaoMobile.ainmIndef, 0,
  (*DownLoadAll*)False, (*UploadChamados*)True);
end;

procedure TFmPrincipal.ImgUploadClick(Sender: TObject);
var
  Qry: TFDQuery;
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  //
  case FSegmentoInsp of
    TSegmentoInsp.sgminspFaccao:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspConfeccao:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmIspMobCab(Qry, TStatusAndamentoInspecao.saiInspRealizada) then
              App_Jan.MostraFormREST_UP_Faccao()
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es FAC��O aptas ao upload!');
          finally
            Qry.Free;
          end;
        end;
      end;
    end;
    TSegmentoInsp.sgminspTextil:
    begin
      case FSeccaoInsp of
        TSeccaoInsp.sccinspTecelagem,
        TSeccaoInsp.sccinspTinturaria:
        begin
          Qry := TFDQuery.Create(Dmod);
          try
            if Dmod.ReopenOVmItxMobCab(Qry, TStatusAndamentoInspecao.saiInspRealizada,
            TSeccaoInsp.sccinspNone) then
              App_Jan.MostraFormREST_UP_Textil()
             else
              Grl_Geral.MB_Aviso('N�o h� inspe��es T�XTIL aptas ao upload!');
          finally
            Qry.Free;
          end;
        end;
        (*
        TSeccaoInsp.sccinspTinturaria:
        begin
         /
        end;
        *)
      end;
    end;
  end;
end;

procedure TFmPrincipal.ImgVerifiDBClick(Sender: TObject);
const
  DBUsu = nil;
begin
  FMX_DmkPF.VibrarComoBotao();
  OcultaAnimaMenu();
  FMX_CfgDBApp.MostraVerifyDB(True, Dmod.AllDB, DBUsu);
end;

procedure TFmPrincipal.LaChmOcoCabClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  App_Jan.MostraFormChmOcoPux();
end;

procedure TFmPrincipal.MostraBotoesCircle(Visivel: Boolean);
begin
  LayInspecao.Visible      := not Visivel;
  //LayPrincipal.Visible     := not Visivel;
  //
  ImgUploadChamado.Visible := Visivel;
  ImgOpcoes.Visible        := Visivel;
  ImgVerifiDB.Visible      := Visivel;
end;

procedure TFmPrincipal.MostraLayMenu();
begin
  MostraBotoesCircle(False);
  AnimaOMenu();
end;

procedure TFmPrincipal.OcultaAnimaMenu();
begin
  AnimaMenu.Inverse := True;
  AnimaMenu.Start;
end;

procedure TFmPrincipal.TimerLoginTimer(Sender: TObject);
begin
  TimerLogin.Enabled := False;
  //
  OVSM_PF.Login();
end;

procedure TFmPrincipal.ImgMenuClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  if LayMenu.Visible then
  begin
    //MostraBotoesCircle(False);
    OcultaAnimaMenu();
  end else
  begin
    MostraBotoesCircle(True);
    //
    AnimaOMenu();
  end;
end;

procedure TFmPrincipal.AnimaMenuFinish(Sender: TObject);
begin
  if AnimaMenu.Inverse = True then
    LayMenu.Visible := False;
end;

end.
