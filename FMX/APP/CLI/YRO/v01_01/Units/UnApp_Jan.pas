unit UnApp_Jan;

interface

uses System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics,
  FMX.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.FMXUI.Wait, FireDAC.Comp.UI, UnDmkEnums,
  UnProjGroupEnums,

  FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, MultiDetailAppearanceU,

  UnFMX_Grl_Vars;

type
  THackListView = class(TListView);
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormLoginApp();
    procedure MostraFormLoginSvr();
    procedure MostraFormOpcoesYROM();
{
    procedure MostraFormOVdLocal(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp);
    procedure MostraFormOViLocais(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp);
    procedure MostraFormOViArtigos(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; Local, NrOP: Integer; NO_Local: String);
    procedure MostraFormOViOPs(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; Local, Artigo: Integer; NO_Local, NO_Artigo: String);
    procedure MostraFormOViTamanho(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; Local, Artigo, NrOP: Integer; NO_Local, NO_Artigo:
              String);
    procedure MostraFormOViBatelada(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade:
              Integer; NO_Local, NO_Artigo, CodTam: String);
    procedure MostraFormREST_Down(Acao: TAcaoDeInstrucaoDeNotificacaoMobile;
              Codigo: Integer; DownLoadAll, UploadChamados: Boolean);
    procedure MostraFormREST_Up_Faccao();
    procedure MostraFormREST_Up_Textil();
    procedure MostraFormOViIspCenSel(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade:
              Integer; NO_Local, NO_Artigo, CodTam: String; Batelada: Integer;
              StatusAndamentoInspecao: TStatusAndamentoInspecao; Codigo:
              Integer);
    procedure MostraFormOViIspCenOpn();
    procedure MostraFormOViIspCenSnt();
    procedure MostraOViInspSeqNcfLvr(CIM: Integer; ItemAtual_Text,
              TotalItens_Text: String; CodInMob, CtrlInMob, PecaSeq, Local,
              Artigo, NrOP, NrReduzidoOP, Produto, CodGrade: Integer; NO_Local,
              NO_Artigo, CodTam, NO_Contexto_Text: String; OVcYnsChk,
              OVcYnsChkCtx, CadContexto: Integer; ReabreOViInspSeqNcfChk:
              Boolean);
    procedure MostraFormOViTclCenSel(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp; Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade:
              Integer; NO_Local, NO_Artigo, CodTam: String; Batelada: Integer;
              SeccaoOP, SeccaoMaq: String; StatusAndamentoInspecao:
              TStatusAndamentoInspecao; Codigo: Integer);
    procedure MostraFormOViItxCenOpn(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp);
    procedure MostraFormOViItxCenSnt(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
              TSeccaoInsp);
    procedure MostraOViItxLvr(CIM: Integer; ItemAtual_Text,
              TotalItens_Text: String; CodInMob, CtrlInMob, PecaSeq, Local,
              Artigo, NrOP, NrReduzidoOP, Produto, CodGrade: Integer; NO_Local,
              NO_Artigo, CodTam, NO_Contexto_Text: String; OVcYnsExgCad,
              OVcYnsExgTop, OVcYnsMixTop: Integer; ReabreOViItxLvr: Boolean);
    procedure MostraFormChmOcoCab(Codigo: Integer);
    procedure MostraFormChmOcoCad(
              // C�digo do Chamado,
              QrChmOcoCabCodigo: Integer;
              // T�tulo
              QrChmOcoCabNome,
              // Quem Chamou
              QrChmOcoCabNO_WhoGerEnti,
              // Descri��o do que fazer
              QrChmOcoCabDescricao,
              // Motivo
              QrChmOcoCabPorqueDescr,
              // T�tulo Procedimento (Fluxo)
              QrChmOcoCabNO_ChmHowCad: String;
              // Per�odo de execu��o
              QrChmOcoCabQuandoIni,
              QrChmOcoCabQuandoFim: TDateTime;
              // Tem itens
              QrChmOcoCabChmHowCad: Integer;
              // Finalizado?
              DoneDtHr: TDateTime;
              // Query a ser reaberta ap�s finaliza��o de chamado
              QrCab: TFDQuery
              );
    procedure MostraFormChmOcoDon(Codigo, DON_Controle, ETP_Controle,
              WHO_Controle: Integer; Etp_Nome, Cab_NO_WhoGerEnti,
              Etp_Descricao: String; Etp_QuandoIni, Etp_QuandoFim: TDateTime;
              DON_Observacao: String);
    procedure MostraFormChmOcoPux();
}

  end;

var
  App_Jan: TUnApp_Jan;

implementation


uses UnFMX_DmkForms, UnFMX_dmkRemoteQuery, UnGrl_DmkDB,
  Module,
  LoginApp, LoginSvr, OpcoesYROM{, REST_Down2,
  REST_Up_Faccao, REST_Up_Textil, UnGrl_Geral,
   UnYROM_Vars, UnGrl_Vars,
  OViLocais, OViArtigos, OViOPs, OViTamanhos, OViBateladas, OViIspCenSel,
  OViInspSeqNcfLvr, OVdLocal, OViIspCenOpn, OViIspCenSnt, OViTclCenSel,
  OViItxLvr, OViItxCenOpn, OViItxCenSnt,
  ChmOcoCab, ChmOcoCad, ChmOcoDon, ChmOcoPux};


{ TUnApp_Jan }

{
procedure TUnApp_Jan.MostraFormChmOcoCab(Codigo: Integer);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmChmOcoCab, FmChmOcoCab, fcmOnlyCreate, True,
  False) then
  begin
    FmChmOcoCab.FCodigo := Codigo;
    //
    //if VAR_EstiloForms = '[Nenhum]' then
    begin
      FmChmOcoCab.Fill.Color             := VAR_Form_Fill_Color;
      FmChmOcoCab.Fill.Kind              := TBrushKind.Solid;
      //FmChmOcoCab.ToolBarTitle.TintColor := VAR_ToolBarTitle_TintColor;
      FmChmOcoCab.LaTitle.TextSettings.FontColor := VAR_LaFormTitle_Font_Color;
      //FmChmOcoCab.LaTexto.TextSettings.FontColor := VAR_LaTexto_Color;
      FmChmOcoCab.RecTitle.Fill.Color    := VAR_ToolBarTitle_TintColor;
      FmChmOcoCab.LinGrey.Stroke.Color   := VAR_LinGrey_Stroke_Color;
      FmChmOcoCab.LinBlue.Stroke.Color   := VAR_LinBlue_Stroke_Color;
      //
      FmChmOcoCab.LVChmOcoCab.ItemAppearanceObjects.HeaderObjects.Text.TextColor := VAR_LaItemTitle_Font_Color;
      FmChmOcoCab.LVChmOcoCab.ItemAppearanceObjects.ItemObjects.Text.TextColor   := VAR_LaItemTexto_Font_Color;
      //
    end;
    FMX_DmkForms.ShowModal(FmChmOcoCab);
  end;
end;

procedure TUnApp_Jan.MostraFormChmOcoCad(QrChmOcoCabCodigo: Integer; QrChmOcoCabNome,
  QrChmOcoCabNO_WhoGerEnti, QrChmOcoCabDescricao, QrChmOcoCabPorqueDescr,
  QrChmOcoCabNO_ChmHowCad: String; QrChmOcoCabQuandoIni, QrChmOcoCabQuandoFim:
  TDateTime; QrChmOcoCabChmHowCad: Integer; DoneDtHr: TDateTime; QrCab: TFDQuery);
const
  sProcName = 'TUnApp_Jan.MostraFormChmOcoCad()';
var
  NaoExiste: Boolean;
begin
  try
    NaoExiste := FmChmOcoCad = nil;
    //
    if FMX_DmkForms.CriaFm_AllOS0(TFmChmOcoCad, FmChmOcoCad, fcmOnlyCreate, True,
    False) then
    begin
      if NaoExiste then
      begin
        FmChmOcoCad.LaEncerrarChamado.TextSettings.FontColor := VAR_LaMenuClickSim_FontColor;
        //FmChmOcoCad.LaEnccerra.TextSettings.FontColor := VAR_LaMenuClickSim_FontColor;

        FmChmOcoCad.Fill.Color             := VAR_Form_Fill_Color;
        FmChmOcoCad.Fill.Kind              := TBrushKind.Solid;
        //FmChmOcoCad.ToolBarTitle.TintColor := VAR_ToolBarTitle_TintColor;
        FmChmOcoCad.LaTitle.TextSettings.FontColor := VAR_LaFormTitle_Font_Color;
        //FmChmOcoCad.LaTexto.TextSettings.FontColor := VAR_LaTexto_Color;
        FmChmOcoCad.RecTitle.Fill.Color    := VAR_ToolBarTitle_TintColor;
        FmChmOcoCad.LinGrey.Stroke.Color   := VAR_LinGrey_Stroke_Color;
        FmChmOcoCad.LinBlue.Stroke.Color   := VAR_LinBlue_Stroke_Color;

        FmChmOcoCad.LVChmOcoCad.ItemAppearanceObjects.HeaderObjects.Text.TextColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LVChmOcoCad.ItemAppearanceObjects.ItemObjects.Text.TextColor   := VAR_LaItemTexto_Font_Color;

        FmChmOcoCad.LaTitNome.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LaTitNO_WhoGerEnti.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LaTitDescricao.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LaTitPorqueDescr.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LaTitPeriodo.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LaConcatPeriodo.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        FmChmOcoCad.LaTitNO_ChmHowCad.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
        //
        FmChmOcoCad.LaNome.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaNO_WhoGerEnti.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaDescricao.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaPorqueDescr.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaNO_ChmHowCad.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaDataIni.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaDataFim.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
        FmChmOcoCad.LaNO_ChmHowCad.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;

        FmChmOcoCad.RecDataIni.Fill.Color := VAR_LaItemTexto_Back_Color;
        FmChmOcoCad.RecDataFim.Fill.Color := VAR_LaItemTexto_Back_Color;

        FmChmOcoCad.RectMenuBase.Width := FmChmOcoCad.Width - (FmChmOcoCad.RectMenuBase.Position.X * 2);
        FmChmOcoCad.RectMenuBase.Stroke.Color              := VAR_RectMenuBase_Stroke_Color;
        FmChmOcoCad.RectMenuBase.Fill.Color                := VAR_RectMenuBase_Fill_Color;
        FmChmOcoCad.LaMenuClickNao.TextSettings.FontColor  := VAR_LaMenuClickNao_FontColor;
        FmChmOcoCad.LaMenuClickSim.TextSettings.FontColor  := VAR_LaMenuClickSim_FontColor;
        FmChmOcoCad.LaMenuLine1.Stroke.Color               := VAR_LaMenuLine1_Stroke_Color;
        FmChmOcoCad.LaMenuLine2.Stroke.Color               := VAR_LaMenuLine2_Stroke_Color;
        FmChmOcoCad.LaMenuTexto.TextSettings.FontColor     := VAR_LaMenuTexto_FontColor;
        FmChmOcoCad.LaMenuTitulo.TextSettings.FontColor    := VAR_LaMenuTitulo_FontColor;
        FmChmOcoCad.LaMenuSubTitulo.TextSettings.FontColor := VAR_LMenuSubTitulo_FontColor;
        //
        //FmChmOcoCad.LayControle.Align := TAlignLayout.Bottom;  Erro de desenho! Fica por cima so ListView!
        FmChmOcoCad.RBRealizou_Sim.FontColor               := VAR_LaMenuClickSim_FontColor;
        FmChmOcoCad.RBRealizou_Nao.FontColor               := VAR_LaMenuClickNao_FontColor;

      end;


      ////////////////////////////////////////////////

      FmChmOcoCad.FChmOcoCad := QrChmOcoCabCodigo;
      //FmChmOcoCad.FChmHowCad := QrChmOcoCabChmHowCad;
      FmChmOcoCad.FDoneDtHr  := DoneDtHr;
      //
      FmChmOcoCad.LaNome.Text := QrChmOcoCabNome;
      FmChmOcoCad.LaNO_WhoGerEnti.Text := QrChmOcoCabNO_WhoGerEnti;
      FmChmOcoCad.LaDescricao.Text := QrChmOcoCabDescricao;
      FmChmOcoCad.LaPorqueDescr.Text := QrChmOcoCabPorqueDescr;
      FmChmOcoCad.LaNO_ChmHowCad.Text := QrChmOcoCabNO_ChmHowCad;
      //
      FmChmOcoCad.LaDataIni.Text := FormatDateTime('dd/mm/yyyy hh:nn', QrChmOcoCabQuandoIni); // QrChmOcoCabQuandoIni;
      FmChmOcoCad.LaDataFim.Text := FormatDateTime('dd/mm/yyyy hh:nn', QrChmOcoCabQuandoFim); // QrChmOcoCabQuandoFim;
      FmChmOcoCad.LaNO_ChmHowCad.Text := QrChmOcoCabNO_ChmHowCad;
      //
      FmChmOcoCad.LayMenu.Align := TAlignLayout.Contents;
      FmChmOcoCad.LayMenu.Visible := False;
      //
      FmChmOcoCad.ReopenChmOcoEtp(0);
      //
      //
      FmChmOcoCad.FQrCab := QrCab;

      FMX_DmkForms.ShowModal(FmChmOcoCad);
    end;
  except
    on E: Exception do
      Grl_Geral.MB_Erro(sProcName + E.Message); // + sLineBreak + 'Linha: ' + Grl_Geral.FF0(I));
  end;
end;

procedure TUnApp_Jan.MostraFormChmOcoDon(Codigo, DON_Controle, ETP_Controle,
  WHO_Controle: Integer; Etp_Nome, Cab_NO_WhoGerEnti, Etp_Descricao: String;
  Etp_QuandoIni, Etp_QuandoFim: TDateTime; DON_Observacao: String);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmChmOcoDon, FmChmOcoDon, fcmOnlyCreate, True,
  False) then
  begin
    FmChmOcoDOn.Fill.Color             := VAR_Form_Fill_Color;
    FmChmOcoDOn.Fill.Kind              := TBrushKind.Solid;
    //FmChmOcoDOn.ToolBarTitle.TintColor := VAR_ToolBarTitle_TintColor;
    FmChmOcoDOn.LaTitle.TextSettings.FontColor := VAR_LaFormTitle_Font_Color;
    //FmChmOcoDOn.LaTexto.TextSettings.FontColor := VAR_LaTexto_Color;
    FmChmOcoDOn.RecTitle.Fill.Color    := VAR_ToolBarTitle_TintColor;
    FmChmOcoDOn.LinGrey.Stroke.Color   := VAR_LinGrey_Stroke_Color;
    FmChmOcoDOn.LinBlue.Stroke.Color   := VAR_LinBlue_Stroke_Color;

    FmChmOcoDOn.LaTitNome.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
    FmChmOcoDOn.LaTitNO_WhoGerEnti.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
    FmChmOcoDOn.LaTitDescricao.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
    FmChmOcoDOn.LaTitPeriodo.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
    FmChmOcoDOn.LaConcatPeriodo.TextSettings.FontColor := VAR_LaItemTitle_Font_Color;
    //
    FmChmOcoDOn.LaNome.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
    FmChmOcoDOn.LaDescricao.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
    FmChmOcoDOn.LaDataIni.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
    FmChmOcoDOn.LaDataFim.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;
    FmChmOcoDOn.LaNO_WhoGerEnti.TextSettings.FontColor := VAR_LaItemTexto_Font_Color;

    //fazer Rectangles!

    FmChmOcoDOn.RecDataIni.Fill.Color := VAR_LaItemTexto_Back_Color;
    FmChmOcoDOn.RecDataFim.Fill.Color := VAR_LaItemTexto_Back_Color;

    FmChmOcoDOn.RectMenuBase.Width := FmChmOcoDOn.Width - (FmChmOcoDOn.RectMenuBase.Position.X * 2);
    FmChmOcoDOn.RectMenuBase.Stroke.Color             := VAR_RectMenuBase_Stroke_Color;
    FmChmOcoDOn.RectMenuBase.Fill.Color               := VAR_RectMenuBase_Fill_Color;
    FmChmOcoDOn.LaMenuClickNao.TextSettings.FontColor := VAR_LaMenuClickNao_FontColor;
    FmChmOcoDOn.LaMenuClickSim.TextSettings.FontColor := VAR_LaMenuClickSim_FontColor;
    FmChmOcoDOn.LaMenuLine1.Stroke.Color              := VAR_LaMenuLine1_Stroke_Color;
    FmChmOcoDOn.LaMenuLine2.Stroke.Color              := VAR_LaMenuLine2_Stroke_Color;
    FmChmOcoDOn.MeObservacao.TextSettings.FontColor   := VAR_LaMenuTexto_FontColor;
    FmChmOcoDOn.LaMenuTitulo.TextSettings.FontColor   := VAR_LaMenuTitulo_FontColor;
    FmChmOcoDOn.LMenuSubTitulo.TextSettings.FontColor := VAR_LMenuSubTitulo_FontColor;

    FmChmOcoDOn.LaEncerrarEvento.TextSettings.FontColor := VAR_LaMenuClickSim_FontColor;

      ////////////////////////////////////////////////

    FmChmOcoDon.FSQLType := stUpd;
    //
    FmChmOcoDon.FCodigo         := Codigo;
    FmChmOcoDon.FDON_Controle   := DON_Controle;
    FmChmOcoDon.FETP_Controle   := ETP_Controle;
    FmChmOcoDon.FWHO_Controle   := WHO_Controle;
    FmChmOcoDon.FDON_Observacao := DON_Observacao;
    //
    FmChmOcoDon.LaNome.Text := Etp_Nome;
    FmChmOcoDon.LaNO_WhoGerEnti.Text := Cab_NO_WhoGerEnti;
    FmChmOcoDon.LaDescricao.Text := Etp_Descricao;
    FmChmOcoDon.LaDataIni.Text := FormatDateTime('dd/mm/yyyy hh:nn', Etp_QuandoIni);
    FmChmOcoDon.LaDataFim.Text := FormatDateTime('dd/mm/yyyy hh:nn', Etp_QuandoFim);
    //
    //FmChmOcoDon.ReopenChmOcoEtp(0);
    //
    FmChmOcoDon.LayMenu.Align   := TAlignLayout.Contents;
    FmChmOcoDon.LayMenu.Visible := False;
    FMX_DmkForms.ShowModal(FmChmOcoDon);
  end;
end;

procedure TUnApp_Jan.MostraFormChmOcoPux();
const
  DestriAntes = False;
begin
  Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, EmptyStr);
  //
  if Dmod.QrChmOcoPux.RecordCount > 0 then
  begin
    //if FmChmOcoPux <> nil then
      //FmChmOcoPux.DisposeOf
    //
    if FMX_DmkForms.CriaFm_AllOS0(TFmChmOcoPux, FmChmOcoPux,
    fcmOnlyCreate, True, False, DestriAntes) then
    begin
      (*
      FmChmOcoPux.Fill.Color             := $FFF0F0FA;
      FmChmOcoPux.Fill.Kind              := TBrushKind.Solid;
      FmChmOcoPux.ToolBarTitle.TintColor := $FFF0F4FA;
      FmChmOcoPux.LinGrey.Stroke.Color   := TAlphaColors.Lightblue;
      FmChmOcoPux.LinBlue.Stroke.Color   := TAlphaColors.PowderBlue;
      *)
      FmChmOcoPux.Fill.Color             := VAR_Form_Fill_Color;
      FmChmOcoPux.Fill.Kind              := TBrushKind.Solid;
      FmChmOcoPux.RecTitle.Fill.Color     := VAR_ToolBarTitle_TintColor;
      //FmChmOcoPux.RecTitle.Fill.Color    := VAR_ToolBarTitle_TintColor;
      FmChmOcoPux.LaTitle.TextSettings.FontColor := VAR_LaFormTitle_Font_Color;
      //FmChmOcoPux.LaTexto.TextSettings.FontColor := VAR_LaTexto_Color;
      FmChmOcoPux.LinGrey.Stroke.Color   := VAR_LinGrey_Stroke_Color;
      FmChmOcoPux.LinBlue.Stroke.Color   := VAR_LinBlue_Stroke_Color;
      //
      FmChmOcoPux.RectMenuBase.Width := FmChmOcoPux.Width - (FmChmOcoPux.RectMenuBase.Position.X * 2);
      FmChmOcoPux.RectMenuBase.Stroke.Color                := VAR_RectMenuBase_Stroke_Color;
      FmChmOcoPux.RectMenuBase.Fill.Color                  := VAR_RectMenuBase_Fill_Color;
      FmChmOcoPux.LaMenuDesiste.TextSettings.FontColor     := VAR_LaMenuClickNao_FontColor;
      FmChmOcoPux.LaMenuExcluir.TextSettings.FontColor     := VAR_LaMenuClickSim_FontColor;
      FmChmOcoPux.LaMenuVisualizar.TextSettings.FontColor  := VAR_LaMenuClickSim_FontColor;
      FmChmOcoPux.LaMenuLine1.Stroke.Color                 := VAR_LaMenuLine1_Stroke_Color;
      FmChmOcoPux.LaMenuLine2.Stroke.Color                 := VAR_LaMenuLine2_Stroke_Color;
      FmChmOcoPux.LaMenuTitulo.TextSettings.FontColor      := VAR_LaMenuTitulo_FontColor;
      FmChmOcoPux.LaMenuSubTitulo.TextSettings.FontColor   := VAR_LMenuSubTitulo_FontColor;
      //

      FmChmOcoPux.LayMenu.Align   := TAlignLayout.Contents;
      FmChmOcoPux.LayMenu.Visible := False;
      FmChmOcoPux.FCodigo         := 0;
      FmChmOcoPux.FDtHrLido       := 0;
      //
      FmChmOcoPux.CarregaItensExistentes();
      //
      FMX_DmkForms.ShowModal(FmChmOcoPux);
    end;
  end else
    Grl_Geral.MB_Info('N�o h� notifica��es cadastradas!');
end;
}

procedure TUnApp_Jan.MostraFormLoginApp();
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmLoginApp, FmLoginApp, fcmCreateTryShwM, True, False);
end;

procedure TUnApp_Jan.MostraFormLoginSvr();
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmLoginSvr, FmLoginSvr, fcmCreateTryShwM, True, False);
end;

procedure TUnApp_Jan.MostraFormOpcoesYROM();
begin
  if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
    Exit;
  FMX_DmkForms.CriaFm_AllOS0(TFmOpcoesYROM, FmOpcoesYROM, fcmCreateTryShwM, True, False);
end;

{
procedure TUnApp_Jan.MostraFormOVdLocal(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOVdLocal, FmOVdLocal, fcmOnlyCreate, True,
  False) then
  begin
    FmOVdLocal.FSegmentoInsp := SegmentoInsp;
    FmOVdLocal.FSeccaoInsp   := SeccaoInsp;
    //
    FMX_DmkForms.ShowModal(FmOVdLocal);
  end;
end;

procedure TUnApp_Jan.MostraFormOViArtigos(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp; Local, NrOP: Integer; NO_Local: String);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViArtigos, FmOViArtigos, fcmOnlyCreate, True, False) then
  begin
    FmOViArtigos.FSegmentoInsp := SegmentoInsp;
    FmOViArtigos.FSeccaoInsp   := SeccaoInsp;
    FmOViArtigos.FLocal        := Local;
    FmOViArtigos.FNrOP         := NrOP;
    FmOViArtigos.FNO_Local     := NO_Local;
    //
    FmOViArtigos.LaLocal.Text  := NO_Local;
    if NrOP <> 0 then
      FmOViArtigos.LaNrOP.Text := Grl_Geral.FF0(NrOP)
    else
      FmOViArtigos.PnNrOP.Visible := False;
    //
    FmOViArtigos.ReopenOViArtigos(SegmentoInsp, SeccaoInsp, EmptyStr);
    //
    FMX_DmkForms.ShowModal(FmOViArtigos);
  end;
end;

procedure TUnApp_Jan.MostraFormOViBatelada(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp; Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade:
  Integer; NO_Local, NO_Artigo, CodTam: String);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViBateladas, FmOViBateladas, fcmOnlyCreate, True, False) then
  begin
    FmOViBateladas.FSegmentoInsp := SegmentoInsp;
    FmOViBateladas.FSeccaoInsp   := SeccaoInsp;
    FmOViBateladas.FLocal        := Local;
    FmOViBateladas.FArtigo       := Artigo;
    FmOViBateladas.FNrOP         := NrOP;
    FmOViBateladas.FNO_Local     := NO_Local;
    FmOViBateladas.FNO_Artigo    := NO_Artigo;
    FmOViBateladas.FCodGrade     := CodGrade;
    FmOViBateladas.FCodTam       := CodTam;
    FmOViBateladas.FNrReduzidoOP := NrReduzidoOP;
    FmOViBateladas.FProduto      := Produto;
    //
    FmOViBateladas.LaLocal.Text  := NO_Local;
    if Artigo <> 0 then
      FmOViBateladas.LaArtigo.Text := NO_Artigo
    else
      FmOViBateladas.PnArtigo.Visible := False;
    //
    if NrOP <> 0 then
      FmOViBateladas.LaNrOP.Text := Grl_Geral.FF0(NrOP)
    else
      FmOViBateladas.PnNrOP.Visible := False;
    //
    if CodTam <> EmptyStr then
      FmOViBateladas.LaTamanho.Text := CodTam
    else
      FmOViBateladas.PnTamanho.Visible := False;
    //
    //FmOViBateladas.ReopenOViBateladas(SegmentoInsp, SeccaoInsp, '');
    //
    FMX_DmkForms.ShowModal(FmOViBateladas);
  end;
end;

procedure TUnApp_Jan.MostraFormOViIspCenOpn();
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmOViIspCenOpn, FmOViIspCenOpn, fcmCreateTryShwM, True, False);
end;

procedure TUnApp_Jan.MostraFormOViItxCenOpn(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViItxCenOpn, FmOViItxCenOpn, fcmOnlyCreate,
  True, False) then
  begin
    FmOViItxCenOpn.FSegmentoInsp := SegmentoInsp;
    FmOViItxCenOpn.FSeccaoInsp   := SeccaoInsp;
    //FmOViItxCenOpn.Reopen(SegmentoInsp, SeccaoInsp, EmptyStr);
    //
    FMX_DmkForms.ShowModal(FmOViItxCenOpn);
  end;
end;

procedure TUnApp_Jan.MostraFormOViItxCenSnt(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp);
var
  Compl: String;
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViItxCenSnt, FmOViItxCenSnt,
  fcmOnlyCreate, True, False) then
  begin
    FmOViItxCenSnt.FSegmentoInsp := SegmentoInsp;
    FmOViItxCenSnt.FSeccaoInsp   := SeccaoInsp;
    Compl := ProjGroupEnums.ObtemNomeSeccaoInsp(SeccaoInsp);
    FmOViItxCenSnt.LaTitulo.Text := 'Inspe��es Enviadas ' + Compl;
    FmOViItxCenSnt.CarregaOVmItxMobCabload(False);
    //
    FMX_DmkForms.ShowModal(FmOViItxCenSnt);
  end;
end;

procedure TUnApp_Jan.MostraFormOViIspCenSel(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp; Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade:
  Integer; NO_Local, NO_Artigo, CodTam: String; Batelada: Integer;
  StatusAndamentoInspecao: TStatusAndamentoInspecao; Codigo: Integer);
var
  NaoFinalizada: Boolean;
  Compl: String;
begin
  NaoFinalizada := StatusAndamentoInspecao <> saiFinalizada;
  //
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViIspCenSel, FmOViIspCenSel,
  fcmOnlyCreate, True, False) then
  begin
    FmOViIspCenSel.FSegmentoInsp := SegmentoInsp;
    FmOViIspCenSel.FSeccaoInsp   := SeccaoInsp;
    Compl := ProjGroupEnums.ObtemNomeSeccaoInsp(SeccaoInsp);
    FmOViIspCenSel.LaTitulo.Text := 'Inspe��o ' + Compl;
    FmOViIspCenSel.FStatusAndamentoInspecao := StatusAndamentoInspecao;
    // do OnCrete...
    FmOViIspCenSel. BtPeca.Enabled      := False;
    FmOViIspCenSel. EdPeca.Enabled      := False;
    FmOViIspCenSel. PnPeca.Visible      := False;
    FmOViIspCenSel. BtContinua.Visible  := False;
    FmOViIspCenSel. BtEncerra.Enabled   := False;
    if NaoFinalizada then
      FmOViIspCenSel.MostraTextoQuandoPodeEncerrar(0)
    else
      FmOViIspCenSel.LaPodeEncerrar.Text := ' Encerramento: J� realizado';
    // ... fim OnCreate
    FmOViIspCenSel.FCodigo                  := Codigo;
    FmOViIspCenSel.FFinalizada              := NaoFinalizada = False;
    FmOViIspCenSel.PnInspecao.Visible       := NaoFinalizada;
    FmOViIspCenSel.PnItemAtual.Visible      := NaoFinalizada;
    FmOViIspCenSel.BtFirst.Visible          := NaoFinalizada;
    FmOViIspCenSel.BtPrior.Visible          := NaoFinalizada;
    FmOViIspCenSel.BtNext.Visible           := NaoFinalizada;
    FmOViIspCenSel.BtLast.Visible           := NaoFinalizada;
    //
    FmOViIspCenSel.FLocal        := Local;
    FmOViIspCenSel.FArtigo       := Artigo;
    FmOViIspCenSel.FNrOP         := NrOP;
    FmOViIspCenSel.FNrReduzidoOP := NrReduzidoOP;
    FmOViIspCenSel.FProduto      := Produto;
    FmOViIspCenSel.FNO_Local     := NO_Local;
    FmOViIspCenSel.FNO_Artigo    := NO_Artigo;
    FmOViIspCenSel.FCodGrade     := CodGrade;
    FmOViIspCenSel.FCodTam       := CodTam;
    //
    //FmOViIspCenSel.VerificaSeExiste();
    if Codigo > 0 then
    begin
      FmOViIspCenSel.PnNome.Visible := False;
      FmOViIspCenSel.ReopenSeFinalizadoEEnviado();
      //FmOViIspCenSel.VerificaSeExiste();
      FmOViIspCenSel.AtualizaInfoGeral();
      //FmOViIspCenSel.ReopenPontosNeg();
      FMX_DmkForms.ShowModal(FmOViIspCenSel);
    end else
    begin
      if FmOViIspCenSel.VerificaSeJaTem() then
      begin
        FmOViIspCenSel.VerificaSeExiste();
        FmOViIspCenSel.ReopenPontosNeg();
        //
        FMX_DmkForms.ShowModal(FmOViIspCenSel);
      end
      else
        //FmOViIspCenSel.Destroy;
        FMX_DmkForms.FechaFm_AllOS0(FmOViIspCenSel);
    end;
  end;
end;

procedure TUnApp_Jan.MostraFormOViIspCenSnt();
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViIspCenSnt, FmOViIspCenSnt,
  fcmOnlyCreate, True, False) then
  begin
    FmOViIspCenSnt.CarregaOVmIspMobCabload(False);
    //
    FMX_DmkForms.ShowModal(FmOViIspCenSnt);
  end;
end;

procedure TUnApp_Jan.MostraFormOViLocais(SegmentoInsp: TSegmentoInsp;
SeccaoInsp: TSeccaoInsp);
begin
  //FMX_DmkForms.CriaFm_AllOS0(TFmOViLocais, FmOViLocais, fcmCreateTryShwM, True, False);
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViLocais, FmOViLocais, fcmOnlyCreate, True, False) then
  begin
    FmOViLocais.FSegmentoInsp := SegmentoInsp;
    FmOViLocais.FSeccaoInsp := SeccaoInsp;
    //
    FMX_DmkForms.ShowModal(FmOViLocais);
  end;
end;

procedure TUnApp_Jan.MostraFormOViOPs(SegmentoInsp: TSegmentoInsp; SeccaoInsp:
  TSeccaoInsp; Local, Artigo: Integer; NO_Local, NO_Artigo: String);
var
  I: Integer;
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViOPs, FmOViOPs, fcmOnlyCreate, True, False) then
  begin
    FmOViOPs.FSegmentoInsp := SegmentoInsp;
    FmOViOPs.FSeccaoInsp   := SeccaoInsp;
    FmOViOPs.FLocal        := Local;
    FmOViOPs.FArtigo       := Artigo;
    FmOViOPs.FNO_Local     := NO_Local;
    FmOViOPs.FNO_Artigo    := NO_Artigo;
    //
    FmOViOPs.LaLocal.Text  := NO_Local;
    if Artigo <> 0 then
      FmOViOPs.LaArtigo.Text := NO_Artigo
    else
      FmOViOPs.PnArtigo.Visible := False;
    //
    FmOViOPs.ReopenOViOPs(SegmentoInsp, '');
    //
    FMX_DmkForms.ShowModal(FmOViOPs);
  end;
end;

procedure TUnApp_Jan.MostraFormOViTamanho(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp; Local, Artigo, NrOP: Integer; NO_Local, NO_Artigo:
  String);
begin
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViTamanhos, FmOViTamanhos, fcmOnlyCreate, True, False) then
  begin
    FmOViTamanhos.FSegmentoInsp := SegmentoInsp;
    FmOViTamanhos.FSeccaoInsp   := SeccaoInsp;
    FmOViTamanhos.FLocal        := Local;
    FmOViTamanhos.FArtigo       := Artigo;
    FmOViTamanhos.FNrOP         := NrOP;
    FmOViTamanhos.FNO_Local     := NO_Local;
    FmOViTamanhos.FNO_Artigo    := NO_Artigo;
    //
    FmOViTamanhos.LaLocal.Text  := NO_Local;
    if Artigo <> 0 then
      FmOViTamanhos.LaArtigo.Text := NO_Artigo
    else
      FmOViTamanhos.PnArtigo.Visible := False;
    //
    if NrOP <> 0 then
      FmOViTamanhos.LaNrOP.Text := Grl_Geral.FF0(NrOP)
    else
      FmOViTamanhos.PnNrOP.Visible := False;
    //
    FmOViTamanhos.ReopenOViTamanhos(SegmentoInsp, SeccaoInsp, '');
    //
    FMX_DmkForms.ShowModal(FmOViTamanhos);
  end;
end;

procedure TUnApp_Jan.MostraFormOViTclCenSel(SegmentoInsp: TSegmentoInsp;
  SeccaoInsp: TSeccaoInsp; Local, Artigo, NrOP, NrReduzidoOP, Produto, CodGrade:
  Integer; NO_Local, NO_Artigo, CodTam: String; Batelada: Integer; SeccaoOP,
  SeccaoMaq: String; StatusAndamentoInspecao: TStatusAndamentoInspecao; Codigo:
  Integer);
var
  NaoFinalizada: Boolean;
  Compl: String;
begin
  NaoFinalizada := StatusAndamentoInspecao <> saiFinalizada;
  //
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViTclCenSel, FmOViTclCenSel,
  fcmOnlyCreate, True, False) then
  begin
    FmOViTclCenSel.FSegmentoInsp := SegmentoInsp;
    FmOViTclCenSel.FSeccaoInsp   := SeccaoInsp;
    Compl := ProjGroupEnums.ObtemNomeSeccaoInsp(SeccaoInsp);
    FmOViTclCenSel.LaTitulo.Text := 'Inspe��o ' + Compl;
    FmOViTclCenSel.FStatusAndamentoInspecao := StatusAndamentoInspecao;
    // do OnCrete...
    FmOViTclCenSel. BtPeca.Enabled      := False;
    FmOViTclCenSel. EdPeca.Enabled      := False;
    FmOViTclCenSel. PnPeca.Visible      := False;
(*&�% Desmarcar?
    FmOViTclCenSel. BtContinua.Visible  := False;
    FmOViTclCenSel. BtEncerra.Enabled   := False;
*)
    if NaoFinalizada then
      FmOViTclCenSel.MostraTextoQuandoPodeEncerrar(0)
    else
      FmOViTclCenSel.LaPodeEncerrar.Text := ' Encerramento: J� realizado';
    // ... fim OnCreate
    FmOViTclCenSel.FCodigo                  := Codigo;
    FmOViTclCenSel.FFinalizada              := NaoFinalizada = False;
    FmOViTclCenSel.PnInspecao.Visible       := NaoFinalizada;
    FmOViTclCenSel.PnItemAtual.Visible      := NaoFinalizada;
    FmOViTclCenSel.BtFirst.Visible          := NaoFinalizada;
    FmOViTclCenSel.BtPrior.Visible          := NaoFinalizada;
    FmOViTclCenSel.BtNext.Visible           := NaoFinalizada;
    FmOViTclCenSel.BtLast.Visible           := NaoFinalizada;
    //FmOViTclCenSel.PnSeccao.Enabled         := NaoFinalizada;
    //
    FmOViTclCenSel.FLocal        := Local;
    FmOViTclCenSel.FArtigo       := Artigo;
    FmOViTclCenSel.FNrOP         := NrOP;
    FmOViTclCenSel.FNrReduzidoOP := NrReduzidoOP;
    FmOViTclCenSel.FProduto      := Produto;
    FmOViTclCenSel.FNO_Local     := NO_Local;
    FmOViTclCenSel.FNO_Artigo    := NO_Artigo;
    FmOViTclCenSel.FCodGrade     := CodGrade;
    FmOViTclCenSel.FCodTam       := CodTam;
    //
    FmOViTclCenSel.FBatelada     := Batelada;
    FmOViTclCenSel.FSeccaoOP     := SeccaoOP;
    FmOViTclCenSel.FSeccaoMaq    := SeccaoMaq;
    //FmOViTclCenSel.VerificaSeExiste();
    if Codigo > 0 then
    begin
      FmOViTclCenSel.PnNome.Visible := False;
      FmOViTclCenSel.ReopenSeFinalizadoEEnviado();
      //FmOViTclCenSel.VerificaSeExiste();
      FmOViTclCenSel.AtualizaInfoGeral();
      //FmOViTclCenSel.ReopenPontosNeg();
      FMX_DmkForms.ShowModal(FmOViTclCenSel);
    end else
    begin
      if FmOViTclCenSel.VerificaSeJaTem() then
      begin
        FmOViTclCenSel.VerificaSeExiste();
        FmOViTclCenSel.ReopenPontosNeg();
        //
        FMX_DmkForms.ShowModal(FmOViTclCenSel);
      end
      else
        //FmOViTclCenSel.Destroy;
        FMX_DmkForms.FechaFm_AllOS0(FmOViTclCenSel);
    end;
  end;
end;

procedure TUnApp_Jan.MostraFormREST_Down(Acao: TAcaoDeInstrucaoDeNotificacaoMobile;
  Codigo: Integer; DownLoadAll, UploadChamados: Boolean);
begin
  //FMX_DmkForms.CriaFm_AllOS0(TFmREST_Down, FmREST_Down, fcmCreateTryShwM, True, False);
  if FMX_DmkForms.CriaFm_AllOS0(TFmREST_Down2, FmREST_Down2, fcmOnlyCreate,
  True, False) then
  begin
    FmREST_Down2.FAcaoDeInstrucaoDeNotificacaoMobile := Acao;
    FmREST_Down2.FCodigoLoc := Codigo;
    FmREST_Down2.BtBaixarTudo.Visible := DownLoadAll;
    FmREST_Down2.BtSubirChamados.Visible := UploadChamados;
    //
    FMX_DmkForms.ShowModal(FmREST_Down2);
  end;
end;

procedure TUnApp_Jan.MostraFormREST_Up_Faccao();
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmREST_Up_Faccao, FmREST_Up_Faccao, fcmCreateTryShwM, True, False);
  //FMX_DmkForms.CriaFm_AllOS0(TFmREST_Up_FGX, FmREST_Up_FGX, fcmCreateTryShwM, True, False);
end;

procedure TUnApp_Jan.MostraFormREST_Up_Textil();
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmREST_Up_Textil, FmREST_Up_Textil, fcmCreateTryShwM, True, False);
end;

procedure TUnApp_Jan.MostraOViInspSeqNcfLvr(CIM: Integer;
  ItemAtual_Text, TotalItens_Text: String; CodInMob, CtrlInMob, PecaSeq, Local,
  Artigo, NrOP, NrReduzidoOP, Produto, CodGrade: Integer; NO_Local, NO_Artigo,
  CodTam, NO_Contexto_Text: String; OVcYnsChk, OVcYnsChkCtx, CadContexto: Integer;
  ReabreOViInspSeqNcfChk: Boolean);
var
  Qry: TFDQuery;
  Descricao: String;
  Magnitude: Integer;
begin
  Descricao := '';
  Magnitude := 0;
  if CtrlInMob <> 0 then
  begin
    Qry := TFDQuery.Create(Dmod);
    try
      Grl_DmkDB.AbreSQLQuery0(Qry, Dmod.AllDB, [
      'SELECT * ',
      'FROM ovmispmoblvr ',
      'WHERE CtrlInMob=' + Grl_Geral.FF0(CtrlInMob),
      EmptyStr]);
      Descricao := Qry.FieldByName('Descricao').AsString;
      Magnitude := Qry.FieldByName('Magnitude').AsInteger;
    finally
      Qry.Free;
    end;
  end;
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViInspSeqNcfLvr, FmOViInspSeqNcfLvr, fcmOnlyCreate, True, False) then
  begin
    FmOViInspSeqNcfLvr.FReabreOViInspSeqNcfChk := ReabreOViInspSeqNcfChk;
    //FmOViInspSeqNcfLvr.LaLocal.Text     := NO_Local;
    FmOViInspSeqNcfLvr.LaArtigo.Text      := NO_Artigo;
    FmOViInspSeqNcfLvr.LaNrOP.Text        := Grl_Geral.FF0(NrOP);
    FmOViInspSeqNcfLvr.LaTamCad.Text      := CodTam;
    FmOViInspSeqNcfLvr.LaItemAtual.Text   := ItemAtual_Text;
    FmOViInspSeqNcfLvr.LaTotalItens.Text  := TotalItens_Text;
    //
    FmOViInspSeqNcfLvr.FCodInMob          := CodInMob;
    FmOViInspSeqNcfLvr.FCtrlInMob         := CtrlInMob;
    FmOViInspSeqNcfLvr.FPecaSeq           := PecaSeq;
    FmOViInspSeqNcfLvr.FLocal             := Local;
    FmOViInspSeqNcfLvr.FArtigo            := Artigo;
    FmOViInspSeqNcfLvr.FNrOP              := NrOP;
    FmOViInspSeqNcfLvr.FNrReduzidoOP      := NrReduzidoOP;
    FmOViInspSeqNcfLvr.FProduto           := Produto;
    FmOViInspSeqNcfLvr.FCodGrade          := CodGrade;
    FmOViInspSeqNcfLvr.FNO_Local          := NO_Local;
    FmOViInspSeqNcfLvr.FNO_Artigo         := NO_Artigo;
    FmOViInspSeqNcfLvr.FCodTam            := CodTam;
    //
    FmOViInspSeqNcfLvr.LaNO_Contexto.Text  := NO_Contexto_Text;
    FmOViInspSeqNcfLvr.FOVcYnsChk          := OVcYnsChk;
    FmOViInspSeqNcfLvr.FOVcYnsChkCtx       := OVcYnsChkCtx;
    FmOViInspSeqNcfLvr.FCadContexto        := CadContexto;
    //
    FmOViInspSeqNcfLvr.EdDescricao.Text      := Descricao;
    FmOViInspSeqNcfLvr.CBMagnitude.ItemIndex := (Magnitude div 1024) - 1;
    //
    FMX_DmkForms.ShowModal(FmOViInspSeqNcfLvr);
    //
  end;
end;

procedure TUnApp_Jan.MostraOViItxLvr(CIM: Integer; ItemAtual_Text,
  TotalItens_Text: String; CodInMob, CtrlInMob, PecaSeq, Local, Artigo, NrOP,
  NrReduzidoOP, Produto, CodGrade: Integer; NO_Local, NO_Artigo, CodTam,
  NO_Contexto_Text: String; OVcYnsExgCad, OVcYnsExgTop,
  OVcYnsMixTop: Integer; ReabreOViItxLvr: Boolean);
var
  Qry: TFDQuery;
  Descricao: String;
  Magnitude: Integer;
begin
  Descricao := '';
  Magnitude := 0;
  if CtrlInMob <> 0 then
  begin
    Qry := TFDQuery.Create(Dmod);
    try
      Grl_DmkDB.AbreSQLQuery0(Qry, Dmod.AllDB, [
      'SELECT * ',
      'FROM ovmispmoblvr ',
      'WHERE CtrlInMob=' + Grl_Geral.FF0(CtrlInMob),
      EmptyStr]);
      Descricao := Qry.FieldByName('Descricao').AsString;
      Magnitude := Qry.FieldByName('Magnitude').AsInteger;
    finally
      Qry.Free;
    end;
  end;
  if FMX_DmkForms.CriaFm_AllOS0(TFmOViItxLvr, FmOViItxLvr, fcmOnlyCreate, True, False) then
  begin
    FmOViItxLvr.FReabreOViItxLvr   := ReabreOViItxLvr;
    //FmOViItxLvr.LaLocal.Text     := NO_Local;
    FmOViItxLvr.LaArtigo.Text      := NO_Artigo;
    FmOViItxLvr.LaNrOP.Text        := Grl_Geral.FF0(NrOP);
    FmOViItxLvr.LaTamCad.Text      := CodTam;
    FmOViItxLvr.LaItemAtual.Text   := ItemAtual_Text;
    FmOViItxLvr.LaTotalItens.Text  := TotalItens_Text;
    //
    FmOViItxLvr.FCodInMob          := CodInMob;
    FmOViItxLvr.FCtrlInMob         := CtrlInMob;
    FmOViItxLvr.FPecaSeq           := PecaSeq;
    FmOViItxLvr.FLocal             := Local;
    FmOViItxLvr.FArtigo            := Artigo;
    FmOViItxLvr.FNrOP              := NrOP;
    FmOViItxLvr.FNrReduzidoOP      := NrReduzidoOP;
    FmOViItxLvr.FProduto           := Produto;
    FmOViItxLvr.FCodGrade          := CodGrade;
    FmOViItxLvr.FNO_Local          := NO_Local;
    FmOViItxLvr.FNO_Artigo         := NO_Artigo;
    FmOViItxLvr.FCodTam            := CodTam;
    //
    FmOViItxLvr.LaNO_Contexto.Text  := NO_Contexto_Text;
    FmOViItxLvr.FOVcYnsExgTop       := OVcYnsExgTop;
    FmOViItxLvr.FOVcYnsExgCad       := OVcYnsExgCad;
    FmOViItxLvr.FOVcYnsMixTop       := OVcYnsMixTop;
    //
    FmOViItxLvr.EdDescricao.Text      := Descricao;
    FmOViItxLvr.CBMagnitude.ItemIndex := (Magnitude div 1024) - 1;
    //
    FMX_DmkForms.ShowModal(FmOViItxLvr);
    //
  end;
end;
}

end.
