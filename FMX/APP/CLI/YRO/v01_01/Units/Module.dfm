object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 364
  Width = 557
  object QrLoc: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 31
    Top = 21
  end
  object QrControle: TFDQuery
    SQL.Strings = (
      'SELECT Versao FROM controle ')
    Left = 31
    Top = 84
  end
  object QrUsuarios: TFDQuery
    SQL.Strings = (
      'SELECT Versao FROM controle ')
    Left = 87
    Top = 84
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 160
    Top = 84
  end
  object QrAux: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 71
    Top = 21
  end
  object QrUpd: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 119
    Top = 21
  end
  object QrEspecificos: TFDQuery
    SQL.Strings = (
      '')
    Left = 31
    Top = 140
  end
  object AllDB: TFDConnection
    Params.Strings = (
      'Database=C:\Executaveis\19.0\OverSeerMob\overseermob.s3db'
      'DriverID=SQLite')
    FetchOptions.AssignedValues = [evLiveWindowParanoic, evLiveWindowFastFirst]
    FetchOptions.LiveWindowFastFirst = True
    FormatOptions.AssignedValues = [fvSE2Null]
    LoginPrompt = False
    Left = 488
    Top = 5
  end
  object MyDB: TFDConnection
    Params.Strings = (
      'DriverID=SQLite')
    FetchOptions.AssignedValues = [evLiveWindowFastFirst]
    FetchOptions.LiveWindowFastFirst = True
    LoginPrompt = False
    Left = 448
    Top = 5
  end
  object QrYROMOpcoes: TFDQuery
    AfterOpen = QrYROMOpcoesAfterOpen
    Connection = MyDB
    SQL.Strings = (
      'SELECT * FROM yromopcoes')
    Left = 32
    Top = 192
    object QrYROMOpcoesERPNameByCli: TStringField
      FieldName = 'ERPNameByCli'
      Size = 60
    end
    object QrYROMOpcoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrYROMOpcoesScaleX: TIntegerField
      FieldName = 'ScaleX'
    end
    object QrYROMOpcoesScaleY: TIntegerField
      FieldName = 'ScaleY'
    end
    object QrYROMOpcoesFoLocObjIns: TSmallintField
      FieldName = 'FoLocObjIns'
    end
    object QrYROMOpcoesEstiloForms: TStringField
      FieldName = 'EstiloForms'
      Size = 60
    end
    object QrYROMOpcoesMailUser: TStringField
      FieldName = 'MailUser'
      Size = 255
    end
    object QrYROMOpcoesAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrYROMOpcoesUsarHTTPS: TSmallintField
      FieldName = 'UsarHTTPS'
    end
    object QrYROMOpcoesUsarApiDev: TSmallintField
      FieldName = 'UsarApiDev'
    end
  end
  object FDPragma: TFDQuery
    Connection = AllDB
    Left = 256
    Top = 74
  end
  object QrOVcMobDevCad: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT * '
      'FROM ovcmobdevcad')
    Left = 256
    Top = 124
    object QrOVcMobDevCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrOVcMobDevCadNome: TStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Size = 60
    end
    object QrOVcMobDevCadDeviceID: TStringField
      FieldName = 'DeviceID'
      Origin = 'DeviceID'
      Size = 60
    end
    object QrOVcMobDevCadUserNmePdr: TStringField
      FieldName = 'UserNmePdr'
      Origin = 'UserNmePdr'
      Size = 60
    end
    object QrOVcMobDevCadDeviceName: TStringField
      FieldName = 'DeviceName'
      Origin = 'DeviceName'
      Size = 60
    end
    object QrOVcMobDevCadDvcScreenH: TIntegerField
      FieldName = 'DvcScreenH'
      Origin = 'DvcScreenH'
    end
    object QrOVcMobDevCadDvcScreenW: TIntegerField
      FieldName = 'DvcScreenW'
      Origin = 'DvcScreenW'
    end
    object QrOVcMobDevCadOSName: TStringField
      FieldName = 'OSName'
      Origin = 'OSName'
      Size = 60
    end
    object QrOVcMobDevCadOSNickName: TStringField
      FieldName = 'OSNickName'
      Origin = 'OSNickName'
      Size = 60
    end
    object QrOVcMobDevCadOSVersion: TStringField
      FieldName = 'OSVersion'
      Origin = 'OSVersion'
      Size = 60
    end
    object QrOVcMobDevCadDtaHabIni: TDateTimeField
      FieldName = 'DtaHabIni'
      Origin = 'DtaHabIni'
      Required = True
    end
    object QrOVcMobDevCadDtaHabFim: TDateTimeField
      FieldName = 'DtaHabFim'
      Origin = 'DtaHabFim'
      Required = True
    end
    object QrOVcMobDevCadAllowed: TShortintField
      FieldName = 'Allowed'
      Origin = 'Allowed'
    end
    object QrOVcMobDevCadLastSetAlw: TIntegerField
      FieldName = 'LastSetAlw'
      Origin = 'LastSetAlw'
    end
    object QrOVcMobDevCadPIN: TStringField
      FieldName = 'PIN'
      Origin = 'PIN'
      Required = True
      Size = 60
    end
    object QrOVcMobDevCadScaleX: TIntegerField
      FieldName = 'ScaleX'
      Origin = 'ScaleX'
      Required = True
    end
    object QrOVcMobDevCadScaleY: TIntegerField
      FieldName = 'ScaleY'
      Origin = 'ScaleY'
      Required = True
    end
    object QrOVcMobDevCadFoLocObjIns: TShortintField
      FieldName = 'FoLocObjIns'
      Origin = 'FoLocObjIns'
      Required = True
    end
    object QrOVcMobDevCadLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'Lk'
    end
    object QrOVcMobDevCadDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'DataCad'
    end
    object QrOVcMobDevCadDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'DataAlt'
    end
    object QrOVcMobDevCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'UserCad'
    end
    object QrOVcMobDevCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'UserAlt'
    end
    object QrOVcMobDevCadAlterWeb: TShortintField
      FieldName = 'AlterWeb'
      Origin = 'AlterWeb'
      Required = True
    end
    object QrOVcMobDevCadAtivo: TShortintField
      FieldName = 'Ativo'
      Origin = 'Ativo'
      Required = True
    end
    object QrOVcMobDevCadSccConfeccao: TShortintField
      FieldName = 'SccConfeccao'
      Origin = 'SccConfeccao'
      Required = True
    end
    object QrOVcMobDevCadSccTecelagem: TShortintField
      FieldName = 'SccTecelagem'
      Origin = 'SccTecelagem'
      Required = True
    end
    object QrOVcMobDevCadSccTinturaria: TShortintField
      FieldName = 'SccTinturaria'
      Origin = 'SccTinturaria'
      Required = True
    end
    object QrOVcMobDevCadChmOcorrencias: TSmallintField
      FieldName = 'ChmOcorrencias'
    end
  end
  object QrChmOcoDon: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT * '
      'FROM chmocodon'
      'WHERE Controle=0')
    Left = 352
    Top = 56
    object QrChmOcoDonCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object QrChmOcoDonControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'Controle'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrChmOcoDonChmOcoEtp: TIntegerField
      FieldName = 'ChmOcoEtp'
      Origin = 'ChmOcoEtp'
      Required = True
    end
    object QrChmOcoDonChmOcoWho: TIntegerField
      FieldName = 'ChmOcoWho'
      Origin = 'ChmOcoWho'
      Required = True
    end
    object QrChmOcoDonObservacao: TStringField
      FieldName = 'Observacao'
      Origin = 'Observacao'
      Size = 255
    end
    object QrChmOcoDonDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Origin = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoDonCloseUser: TIntegerField
      FieldName = 'CloseUser'
      Origin = 'CloseUser'
      Required = True
    end
    object QrChmOcoDonCloseDtHr: TDateTimeField
      FieldName = 'CloseDtHr'
      Origin = 'CloseDtHr'
      Required = True
    end
    object QrChmOcoDonMobUpWeb: TShortintField
      FieldName = 'MobUpWeb'
      Origin = 'MobUpWeb'
      Required = True
    end
  end
  object QrChmOcoPux: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT * '
      'FROM chmocopux '
      'WHERE DtHrLido<="1900-01-01"')
    Left = 352
    Top = 8
    object QrChmOcoPuxCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrChmOcoPuxNome: TStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Size = 100
    end
    object QrChmOcoPuxTxtLocal: TStringField
      FieldName = 'TxtLocal'
      Origin = 'TxtLocal'
      Size = 100
    end
    object QrChmOcoPuxDtHrPush: TDateTimeField
      FieldName = 'DtHrPush'
      Origin = 'DtHrPush'
      Required = True
    end
    object QrChmOcoPuxDtHrLido: TDateTimeField
      FieldName = 'DtHrLido'
      Origin = 'DtHrLido'
      Required = True
    end
    object QrChmOcoPuxDtHrCatx: TDateTimeField
      FieldName = 'DtHrCatx'
    end
    object QrChmOcoPuxQuandoIni: TDateTimeField
      FieldName = 'QuandoIni'
      Origin = 'QuandoIni'
      Required = True
    end
    object QrChmOcoPuxQuandoFim: TDateTimeField
      FieldName = 'QuandoFim'
      Origin = 'QuandoFim'
      Required = True
    end
  end
  object QrEpoch: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      
        'SELECT datetime(1578009128645/1000, '#39'unixepoch'#39', '#39'localtime'#39') AS' +
        ' DataHora;')
    Left = 148
    Top = 264
    object QrEpochDataHora: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'DataHora'
      Origin = 'DataHora'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
  end
  object QrChmOcoWho: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT who.* '
      'FROM chmocowho who '
      'WHERE who.MobUpWeb=0 '
      'AND who.WhoDoMobile>0')
    Left = 352
    Top = 104
    object QrChmOcoWhoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object QrChmOcoWhoControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'Controle'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrChmOcoWhoOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'Ordem'
      Required = True
    end
    object QrChmOcoWhoKndWhoPrtnc: TIntegerField
      FieldName = 'KndWhoPrtnc'
      Origin = 'KndWhoPrtnc'
      Required = True
    end
    object QrChmOcoWhoWhoDoEnti: TIntegerField
      FieldName = 'WhoDoEnti'
      Origin = 'WhoDoEnti'
      Required = True
    end
    object QrChmOcoWhoWhoDoMobile: TIntegerField
      FieldName = 'WhoDoMobile'
      Origin = 'WhoDoMobile'
      Required = True
    end
    object QrChmOcoWhoDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Origin = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoWhoSntNotif: TDateTimeField
      FieldName = 'SntNotif'
      Origin = 'SntNotif'
      Required = True
    end
    object QrChmOcoWhoCfmRcbNotif: TDateTimeField
      FieldName = 'CfmRcbNotif'
      Origin = 'CfmRcbNotif'
      Required = True
    end
    object QrChmOcoWhoCfmDtHrLido: TDateTimeField
      FieldName = 'CfmDtHrLido'
      Origin = 'CfmDtHrLido'
      Required = True
    end
    object QrChmOcoWhoMobUpWeb: TShortintField
      FieldName = 'MobUpWeb'
      Origin = 'MobUpWeb'
      Required = True
    end
    object QrChmOcoWhoRealizado: TShortintField
      FieldName = 'Realizado'
      Origin = 'Realizado'
      Required = True
    end
    object QrChmOcoWhoObsEncerr: TStringField
      FieldName = 'ObsEncerr'
      Origin = 'ObsEncerr'
      Size = 255
    end
  end
end
