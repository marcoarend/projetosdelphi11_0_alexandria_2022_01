unit UnTemp;

interface

uses UnDmkEnums, FMX.Memo, System.UITypes;

type
  TUnTemp = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InfoMemoStep(Texto: String);
  end;
var
  MeSTEP_CODE: TMemo = nil;
  Temp: TUnTemp;


implementation

{ TUnTemp }

procedure TUnTemp.InfoMemoStep(Texto: String);
begin
  if MeSTEP_CODE <> nil then
    MeSTEP_CODE.Text := Texto + sLineBreak + MeSTEP_CODE.Text;
end;

end.
