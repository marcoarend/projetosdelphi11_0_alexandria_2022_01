unit UnProjGroupEnums;

interface

uses  System.SysUtils, System.Types;

//const
  //CO_ShowKindList_0_RealIndx = 0;

type
  TStatusAndamentoInspecao = (saiIndefinido=0, saiInspAberta=1,
    saiInspRealizada=2, saiEnvDadosInspSvr=3, saiEnvFotosInspSvr=4,
    saiEnvEmailInsp=5,

    //... Livres:
    saiLivre6=6,
    saiLivre7=7,
    saiLivre8=8,
    //... Fim
    saiFinalizada=9);
  //TStatusEnvioInspecao = (seiIndefinido=0, seiEnvioDadosServer=1, seiEnvioEmail=2);
  TTipoEnvioEmailInspecao = (smerAll=0, smerXtra=1, smerEnti=2);
  TSegmentoInsp = (sgminspNone=0, sgminspFaccao=1024, sgminspTextil=2048);
  TSeccaoInsp = (sccinspNone=0, sccinspTecelagem=1024, sccinspTinturaria=2048,
                 sccinspConfeccao=3072);
  TUnProjGroupEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ObtemNomeSeccaoInsp(SeccaoInsp: TSeccaoInsp): String;
  end;
var
  ProjGroupEnums: TUnProjGroupEnums;


implementation

{ TUnProjGroupEnums }

function TUnProjGroupEnums.ObtemNomeSeccaoInsp(SeccaoInsp: TSeccaoInsp): String;
begin
  case SeccaoInsp of
    sccinspNone(*0*)          : Result := '*Indefinido*';
    sccinspTecelagem(*1024*)  : Result := 'Tecelagem';
    sccinspTinturaria(*2048*) : Result := 'Tinturaria';
    sccinspConfeccao(*3072*)  : Result := 'Confec��o';
    else Result := '?s?e?c?a?o?';
  end;
end;

end.
