program Exes;

uses
  {$IF DEFINED(ANDROID)}
  UnGrl_OS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Android\UnGrl_OS.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_FMX_ProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_ProcFunc.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_NetworkState in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_NetworkState.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_BarcodeScanner in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_BarcodeScanner.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Androidapi.JNI.PowerManager in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Androidapi.JNI.PowerManager.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_FMX_NetworkState in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_NetworkState.pas',
  {$ENDIF }
  {$IFDEF MSWINDOWS}
  UnGrl_OS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Windows\UnGrl_OS.pas',
  {$ENDIF }
  {$IFDEF MSWINDOWS}
  Windows_FMX_ProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  {$ENDIF }
  {$IFDEF MSWINDOWS}
  UnitMD5 in '..\..\..\..\..\..\VCL\UTL\Encrypt\v01_01\UnitMD5.pas',
  {$ENDIF }
  {$IF DEFINED(iOS)}
  UnGrl_OS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\iOS\UnGrl_OS.pas',
  {$ENDIF }
  System.StartUpCopy,
  FMX.Forms,
  UnFMX_DmkDB in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkDB.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnFMX_Geral in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Geral.pas',
  UnFMX_ValidaIE in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_ValidaIE.pas',
  UnFMX_DmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkProcFunc.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnFMX_Grl_Vars in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Vars.pas',
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_Tabs in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Tabs.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  Module in '..\Units\Module.pas' {Dmod},
  MyListas in '..\Units\MyListas.pas',
  UnFMX_CfgDBApp in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_CfgDBApp.pas',
  VerifyDB in '..\..\..\..\..\UTL\_FRM\v01_01\VerifyDB.pas' {FmVerifyDB},
  UnFMX_DmkForms in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkForms.pas',
  PesqCod in '..\..\..\..\..\UTL\_FRM\v01_01\PesqCod.pas' {FmPesqCod},
  UnFMX_DmkWeb in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkWeb.pas',
  UnGrl_AllOS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_AllOS.pas',
  UnFMX_DmkRemoteQuery in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkRemoteQuery.pas',
  DmkDialog in '..\..\..\..\..\UTL\_FRM\v03_01\DmkDialog.pas' {FmDmkDialog},
  UnApp_Jan in '..\Units\UnApp_Jan.pas',
  UnFMX_DmkUnLic in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkUnLic.pas',
  UnFMX_Ref in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Ref.pas',
  MultiDetailAppearanceU in '..\..\..\..\..\MDL\ListViewMultiDetailAppearance\MultiDetailAppearanceU.pas',
  UnREST_SQL in '..\..\..\..\..\UTL\WEB\REST\UnREST_SQL.pas',
  UnProjGroupEnums in 'UnProjGroupEnums.pas',
  TiraFoto in '..\..\..\..\..\MDL\Foto\TiraFoto.pas' {FmTiraFoto},
  UnFMX_Fotos in '..\..\..\..\..\MDL\Foto\UnFMX_Fotos.pas',
  UnApp_PF in '..\Units\UnApp_PF.pas',
  UnGrl_Components in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Components.pas',
  UnGrl_IO in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_IO.pas',
  Chamados_Tabs in '..\..\..\..\..\..\VCL\MDL\CHAM\v01_01\Chamados_Tabs.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnGrl_DmkWeb in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkWeb.pas',
  UnGrlTemp in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrlTemp.pas',
  UnExes_Tabs in '..\..\..\..\..\..\VCL\MDL\Exes\v01_01\UnExes_Tabs.pas',
  UnExes_Consts in '..\..\..\..\..\..\VCL\MDL\Exes\v01_01\UnExes_Consts.pas',
  UnProjGroup_Consts in '..\..\..\..\..\..\VCL\MDL\Exes\v01_01\UnProjGroup_Consts.pas',
  UnExesM_PF in '..\Units\UnExesM_PF.pas',
  UnExesM_Vars in '..\..\..\..\..\MDL\Exes\v01_01\UnExesM_Vars.pas',
  OpcoesExesM in '..\..\..\..\..\MDL\Exes\v01_01\OpcoesExesM.pas' {FmOpcoesExesM},
  LoginApp in '..\..\..\..\..\MDL\Exes\v01_01\LoginApp.pas' {FmLoginApp},
  LoginSvr in '..\..\..\..\..\MDL\Exes\v01_01\LoginSvr.pas' {FmLoginSvr},
  OpcoesSeqJan in '..\..\..\..\..\MDL\Exes\v01_01\OpcoesSeqJan.pas' {FmOpcoesSeqJan},
  OpcoesAvancadas in '..\..\..\..\..\MDL\Exes\v01_01\OpcoesAvancadas.pas' {FmOpcoesAvancadas},
  REST_Down in '..\Forms\REST_Down.pas' {FmREST_Down},
  EXiMdoPag in '..\Units\EXiMdoPag.pas' {FmEXiMdoPag},
  EXiBasico in '..\Units\EXiBasico.pas' {FmEXiBasico},
  EXmDspLctCab in '..\Units\EXmDspLctCab.pas' {FmEXmDspLctCab},
  EXiStCmprv in '..\Units\EXiStCmprv.pas' {FmEXiStCmprv},
  EXiCtaPag2 in '..\Units\EXiCtaPag2.pas' {FmEXiCtaPag2},
  EXiCtaPag in '..\Units\EXiCtaPag.pas' {FmEXiCtaPag},
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas';

{$R *.res}

begin
(*
  UnTemp in 'UnTemp.pas',
  LoginApp in '..\..\..\..\..\MDL\YRO\v00_00\LoginApp.pas' {FmLoginApp};
  LoginSvr in '..\..\..\..\..\MDL\YRO\v00_00\LoginSvr.pas' {FmLoginSvr};
  ChmOcoCab in '..\..\..\..\..\MDL\CHM\v01_01\ChmOcoCab.pas' {FmChmOcoCab},
  ChmOcoCad in '..\..\..\..\..\MDL\CHM\v01_01\ChmOcoCad.pas' {FmChmOcoCad},
  UnFMX_PushNotifications in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_PushNotifications.pas',
  ChmOcoPux in '..\..\..\..\..\MDL\CHM\v01_01\ChmOcoPux.pas' {FmChmOcoPux},
  UnFMX_DmkObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkObjects.pas',
  ChmOcoDon in '..\..\..\..\..\MDL\CHM\v01_01\ChmOcoDon.pas' {FmChmOcoDon},
*)

(*
  REST_Down2 in '..\..\..\..\..\MDL\YRO\v00_01\REST_Down2.pas' {FmREST_Down2},
  OVdLocal in '..\..\..\..\..\MDL\YRO\v00_01\OVdLocal.pas' {FmOVdLocal},
  OViLocais in '..\..\..\..\..\MDL\YRO\v00_01\OViLocais.pas' {FmOViLocais},
  OViOPs in '..\..\..\..\..\MDL\YRO\v00_01\OViOPs.pas' {FmOViOPs},
  OViArtigos in '..\..\..\..\..\MDL\YRO\v00_01\OViArtigos.pas' {FmOViArtigos},
  OViTamanhos in '..\..\..\..\..\MDL\YRO\v00_01\OViTamanhos.pas' {FmOViTamanhos},
  OViBateladas in '..\..\..\..\..\MDL\YRO\v00_01\OViBateladas.pas' {FmOViBateladas},
  OViSeccOP in '..\..\..\..\..\MDL\YRO\v00_01\OViSeccOP.pas' {FmOViSeccOP},
  OViIspCenSel in '..\..\..\..\..\MDL\YRO\v00_01\OViIspCenSel.pas' {FmOViIspCenSel},
  OViIspCenOpn in '..\..\..\..\..\MDL\YRO\v00_01\OViIspCenOpn.pas' {FmOViIspCenOpn},
  OViIspCenSnt in '..\..\..\..\..\MDL\YRO\v00_01\OViIspCenSnt.pas' {FmOViIspCenSnt},
  OViInspSeqNcfChk in '..\..\..\..\..\MDL\YRO\v00_01\OViInspSeqNcfChk.pas' {FmOViInspSeqNcfChk},
  OViInspSeqNcfTop in '..\..\..\..\..\MDL\YRO\v00_01\OViInspSeqNcfTop.pas' {FmOViInspSeqNcfTop},
  OViInspSeqNcfLvr in '..\..\..\..\..\MDL\YRO\v00_01\OViInspSeqNcfLvr.pas' {FmOViInspSeqNcfLvr},
  OViInspSeqNcfAct in '..\..\..\..\..\MDL\YRO\v00_01\OViInspSeqNcfAct.pas' {FmOViInspSeqNcfAct},
  OViInspSeqMed in '..\..\..\..\..\MDL\YRO\v00_01\OViInspSeqMed.pas' {FmOViInspSeqMed},
  OViInspSeqMedEdit in '..\..\..\..\..\MDL\YRO\v00_01\OViInspSeqMedEdit.pas' {FmOViInspSeqMedEdit},
  UnREST_App in '..\..\..\..\..\MDL\YRO\v00_01\UnREST_App.pas',
  OpcoesAvancadas in '..\..\..\..\..\MDL\YRO\v00_01\OpcoesAvancadas.pas' {FmOpcoesAvancadas},
  OViItxAct in '..\..\..\..\..\MDL\YRO\v00_01\OViItxAct.pas' {FmOViItxAct},
  OViItxLvr in '..\..\..\..\..\MDL\YRO\v00_01\OViItxLvr.pas' {FmOViItxLvr},
  OViItxCenOpn in '..\..\..\..\..\MDL\YRO\v00_01\OViItxCenOpn.pas' {FmOViItxCenOpn},
  OViItxCenSnt in '..\..\..\..\..\MDL\YRO\v00_01\OViItxCenSnt.pas' {FmOViItxCenSnt},
  OViTclExgAvl in '..\..\..\..\..\MDL\YRO\v00_01\OViTclExgAvl.pas' {FmOViTclExgAvl},
  OViTclExgMix in '..\..\..\..\..\MDL\YRO\v00_01\OViTclExgMix.pas' {FmOViTclExgMix},
  OViTclCenSel in '..\..\..\..\..\MDL\YRO\v00_01\OViTclCenSel.pas' {FmOViTclCenSel},
  REST_Up_Faccao in '..\..\..\..\..\MDL\YRO\v00_01\REST_Up_Faccao.pas' {FmREST_Up_Faccao},
  REST_Up_Textil in '..\..\..\..\..\MDL\YRO\v00_01\REST_Up_Textil.pas' {FmREST_Up_Textil};
*)

(*
  Original:
    Project > Options > Version Info > Debug Configuration - Android Platform > Label =  $(ModuleName)
    Project > Options > Version Info > Debug Configuration - Android Platform > ApiKey =  AIzaSyAuoY0fIOMlbozcYpX6EUQXWtthRRCnRoI
    Project > Options > Application Debug Configuration - Android Platform > Launcher Icon xxxxxxx =
      C:\_Sincro\O v e r S e e r\Mob\�cone\Original\O v e r S e e r XXXXXXXXXX.png


  Nayr:
    Project > Options > Version Info > Debug Configuration - Android Platform > Label =  Nayr SGQ
    Project > Options > Version Info > Debug Configuration - Android Platform > ApiKey =  AIzaSyAuoY0fIOMlbozcYpX6EUQXWtthRRCnRoI
    Project > Options > Application Debug Configuration - Android Platform > Launcher Icon xxxxxxx =
      C:\_Sincro\O v e r S e e r\Mob\�cone\Original\O v e r S e e r XXXXXXXXXX.png
*)

  Application.Initialize;
  Application.Title := 'YRO';
  Application.FormFactor.Orientations := [TFormOrientation.Portrait];
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;

end.
