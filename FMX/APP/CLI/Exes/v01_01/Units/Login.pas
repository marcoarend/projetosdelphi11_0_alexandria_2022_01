unit LoginApp;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TFmLogin = class(TForm)
    Label1: TLabel;
    EdPIN: TEdit;
    BtEntrar: TButton;
    procedure BtEntrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTentativas: Integer;
  public
    { Public declarations }
  end;

var
  FmLogin: TFmLogin;

implementation

uses UnFMX_Grl_Vars, UnFMX_Geral, Principal;

{$R *.fmx}

procedure TFmLogin.BtEntrarClick(Sender: TObject);
begin
  if Lowercase(EdPIN.Text) = Lowercase(VAR_PIN_DEVICE_IN_SERVER) then
  begin
    FmPrincipal.PnShow.Align := TAlignLayout.Client;
    FmPrincipal.PnShow.Visible := True;
    FmPrincipal.ImgShow.Visible := False;
    Close;
  end else
  begin
    FTentativas := FTentativas + 1;
    if FTentativas < 3 then
      FMX_Geral.MB_Aviso('PIN inv�lido!')
    else
    begin
      Application.Terminate;
    end;
  end;
end;

procedure TFmLogin.FormCreate(Sender: TObject);
begin
  FTentativas := 0;
end;

end.
