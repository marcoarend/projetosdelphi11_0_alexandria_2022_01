unit EXiStCmprv;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, UnDmkEnums, UnApp_PF, UnGrl_Consts;

type
  TFmEXiStCmprv = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    GrStCmprv: TGrid;
    QrEXcStCmprv: TFDQuery;
    QrEXcStCmprvCodigo: TIntegerField;
    QrEXcStCmprvNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrEXcStCmprvBRK_Nome: TStringField;
    procedure SbPesquisaClick(Sender: TObject);
    procedure QrEXcStCmprvCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure GrStCmprvCellClick(const Column: TColumn; const Row: Integer);
  private
    { Private declarations }
    procedure TiraFotoParaEnviarViaREST();
    procedure TiraFotoParaEnviarViaFTP();
  public
    { Public declarations }
    //FFormaEdicao: TEXFormaEdicao;
    //FEXSeqJan: TEXSeqJan;
    //FSequenciaJan, FEXcIdFunc, FEXcMdoPag,
    FEXcStCmprv(*, FEXcCtaPag*): Integer;
    //FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT: String;
    //FData, FHora: TDateTime;
    FValor: Double;
    //FANoMes, FObserva: String;
    //FAno, FMes: Integer;
    FCodInMob(*, FCtrlInMob*): Integer;
    FSQLType: TSQLType;
    //
    procedure ReopenEXcStCmprv(ParteNome: String);
  end;

var
  FmEXiStCmprv: TFmEXiStCmprv;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnExes_Consts, UnExesM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnExesM_PF,
  Module, UnFMX_Fotos;

{$R *.fmx}

procedure TFmEXiStCmprv.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmEXiStCmprv.FormShow(Sender: TObject);
begin
  //Dmod.ReopenExesMOpcoes();
  Dmod.ReopenEXcMobDevCad();
  //EdEXcIdFunc.Text := Grl_Geral.FF0(Dmod.QrExesMOpcoesEXcIdFunc.Value);
  ReopenEXcStCmprv('');
end;

procedure TFmEXiStCmprv.GrStCmprvCellClick(const Column: TColumn;
  const Row: Integer);
const
  sProcFunc = 'TFmEXiStCmprv.GrStCmprvCellClick()';
begin
  // TProtocoloEnvioDeImagem = (pdediIndef=0, pdediHTTP=1, pdediFTP=2, pdediREST=3);
  case VAR_PROTOCOLO_ENVIO_DE_IMAGEM of
    TProtocoloEnvioDeImagem.pdediFTP: TiraFotoParaEnviarViaFTP();
    TProtocoloEnvioDeImagem.pdediREST: TiraFotoParaEnviarViaREST();
    else Grl_Geral.MB_Aviso('"PROTOCOLO_ENVIO_DE_IMAGEM" indefinido em ' + sProcFunc);
  end;
end;

procedure TFmEXiStCmprv.QrEXcStCmprvCalcFields(DataSet: TDataSet);
begin
  QrEXcStCmprvBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrStCmprv, 1, QrEXcStCmprvNome.Value);
end;

procedure TFmEXiStCmprv.ReopenEXcStCmprv(ParteNome: String);
var
  SQL_AND: String;
begin
  SQL_AND := '';
  if Trim(ParteNome) <> '' then
    SQL_AND := 'AND scp.Nome LIKE "%' + ParteNome + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrEXcStCmprv, Dmod.AllDB, [
  'SELECT scp.Codigo, scp.Nome',
  'FROM excstcmprv scp ',
  'WHERE Ativo=1 ',
  SQL_AND,
  'GROUP BY scp.Codigo ',
  'ORDER BY scp.Nome ',
  '']);
end;

procedure TFmEXiStCmprv.SbPesquisaClick(Sender: TObject);
begin
  ReopenEXcStCmprv(EdPesquisa.Text);
end;

procedure TFmEXiStCmprv.TiraFotoParaEnviarViaFTP;
begin
{Application.CreateForm(TFmCamera, FmCamera);
  FmCamera.FTipoFile   := ttfPng;
  FmCamera.FFilePrefix := 'OVS_1_1_1_';
  FmCamera.FDirectory  := 'proforma';
  FmCamera.Show;
}
end;

procedure TFmEXiStCmprv.TiraFotoParaEnviarViaREST();
const
  AcrescentaDataNoNome = True;
  FIdTabela = 0;
  FCtrlInMob = 0;
var
  IDFoto, NomeArq, SQLExec: String;
begin
(*
  IDFoto := 'OVS_' +
            Grl_Geral.FFN(FCodInMob, 10) + '_' +
            Grl_Geral.FFN(FCtrlInMob, 10);
*)
  IDFoto := App_PF.DefineNomeArquivoFotoDespesa(FCodInMob, FValor);
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtSQLite, stIns, 'exmdspmobfts', False, [
  'EXcStCmprv', 'NomeArq'], [
  'CodInMob', 'DataHora'], [
  FEXcStCmprv, CO_JOKE_FOTO_ARQNOME], [
  FCodInMob, CO_JOKE_FOTO_DATAHORA], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
  False, SQLExec) then
  begin
    if FMX_Fotos.MostraFormTiraFoto(IDFoto, AcrescentaDataNoNome, SQLExec, NomeArq,
    FIdTabela) then
    begin
      VAR_FOTO_COD_IN_MOB  := FCodInMob;
      VAR_FOTO_CTRL_IN_MOB := FCtrlInMob;
      VAR_FOTO_ID_TABELA   := FIdTabela;
      //
      App_PF.AcoesFinaisDeTirarFoto()
    end;
  end;
end;

end.
