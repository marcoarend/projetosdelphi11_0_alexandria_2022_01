unit Testes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, FMX.Gestures, FMX.Controls.Presentation, UnGrl_Geral,
  FMX.ScrollBox, FMX.Memo, UnTemp, UnFMX_Geral, UnGrl_AllOS, FMX.Edit,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, Data.DB,
{$IfDef MSWINDOWS}
  ZAbstractRODataset, ZAbstractDataset, ZDataset,
{$EndIf}
  Datasnap.DBClient, FMX.Grid.Style, Fmx.Bind.Grid, Fmx.Bind.Editors, FMX.Grid,
  UnGrl_Vars;

type
  TFmTestes = class(TForm)
    HeaderToolBar: TToolBar;
    ToolBarLabel: TLabel;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    GestureManager1: TGestureManager;
    LaUsuario: TLabel;
    Panel1: TPanel;
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Panel2: TPanel;
    Label1: TLabel;
    EdDeviceName: TEdit;
    Label2: TLabel;
    EdDvcScreenH: TEdit;
    Label3: TLabel;
    EdDvcScreenW: TEdit;
    Label4: TLabel;
    EdOSName: TEdit;
    Label5: TLabel;
    EdOSNickName: TEdit;
    Label6: TLabel;
    EdOSVersion: TEdit;
    Label7: TLabel;
    EdDeviceID: TEdit;
    Panel3: TPanel;
    Panel4: TPanel;
    BtExecute: TButton;
    BtSelect: TButton;
    ClientDataSet1: TClientDataSet;
    Grid1: TGrid;
    ClientDataSet1Codigo: TIntegerField;
    ClientDataSet1Nome: TStringField;
    BindSourceDB1: TBindSourceDB;
    Panel5: TPanel;
    Panel6: TPanel;
    Button5: TButton;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    TabItem5: TTabItem;
    SGTabelas: TStringGrid;
    StringColumn1: TStringColumn;
    Panel7: TPanel;
    Button6: TButton;
    MeJASON: TMemo;
    Label8: TLabel;
    EdUserNmePdr: TEdit;
    CdOVcMobDevCad: TClientDataSet;
    CdOVcMobDevCadCodigo: TIntegerField;
    CdOVcMobDevCadDeviceID: TStringField;
    CdOVcMobDevCadUserNmePdr: TStringField;
    CdOVcMobDevCadPIN: TStringField;
    Label9: TLabel;
    EdCNPJEmpresa: TEdit;
    Label10: TLabel;
    EdPIN: TEdit;
    Button4: TButton;
    BtBuscaDados: TButton;
    CdAux: TClientDataSet;
    IntegerField1: TIntegerField;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    procedure FormCreate(Sender: TObject);
    procedure FormGesture(Sender: TObject; const EventInfo: TGestureEventInfo;
      var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure BtSelectClick(Sender: TObject);
    procedure BtExecuteClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure BtBuscaDadosClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FTentativas: Integer;
    //
    function  InsereDadosAtivacaoNoServidor(): Boolean;
    function  AtivarDevice(Codigo: Integer; Nome, DeviceID, UserNmePdr, PIN:
              String): Boolean;
  public
    { Public declarations }
  end;

var
  FmTestes: TFmTestes;

implementation

uses UnFMX_CfgDBApp, Module, UnGrl_DmkDB, UnDmkEnums, MyListas,
  UnFMX_DmkRemoteQuery, UnFMX_DmkProcFunc, UnOVSM_PF, UnOVSM_Vars;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.iPhone55in.fmx IOS}
{$R *.Windows.fmx MSWINDOWS}
{$R *.Surface.fmx MSWINDOWS}

function TFmTestes.AtivarDevice(Codigo: Integer; Nome, DeviceID, UserNmePdr,
  PIN: String): Boolean;
var
  //DeviceID, UserNmePdr, Nome: String;
  //Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  //Codigo         := ;
  //Nome           := '';
  //DeviceID       := ;
  //UserNmePdr     := ;
  if Lowercase(PIN) = Lowercase(CdOVcMobDevCadPIN.Value) then
  begin

    if Grl_DmkDB.ExecutaSQLQuery0(Dmod.QrLoc, Dmod.AllDB,
      [
        'DELETE FROM ovcmobdevatv WHERE DeviceID="' + DeviceID + '"',
        ''
      ]) then
    begin
      if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB,
      stIns, 'ovcmobdevatv', False, ['Nome', 'DeviceID', 'UserNmePdr', 'PIN'], ['Codigo'],
      [Nome, DeviceID, UserNmePdr, PIN], [Codigo], False,
      TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
      begin
        if OVSM_PF.ObtemCodigoDeAceiteNoServidor() then
        begin
          FMX_Geral.MB_Info('Ol� ' + VAR_NIK_DEVICE_IN_SERVER + '!' + sLineBreak +
          'Este dispositivo foi aceito na empresa!');
        end else
          Grl_Geral.MB_Erro('Falha ao obter dados do usu�rio! (A)');
      end else
        Grl_Geral.MB_Erro('Falha ao ativar usu�rio (B)!');
    end else
      Grl_Geral.MB_Erro('Falha ao ativar usu�rio (A)!');
  end else
  begin
    FTentativas := FTentativas + 1;
    if FTentativas < 3 then
      Grl_Geral.MB_Aviso('PIN n�o confere!')
    else
      Application.Terminate;
  end;
end;

procedure TFmTestes.BtBuscaDadosClick(Sender: TObject);
(*
var
  Nome, DeviceID, UserNmePdr, DeviceName, OSName, OSNickName, OSVersion, DtaHabIni, DtaHabFim, PIN: String;
  Codigo, DvcScreenH, DvcScreenW, Allowed, LastSetAlw: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Nome           := ;
  DeviceID       := ;
  UserNmePdr     := ;
  DeviceName     := ;
  DvcScreenH     := ;
  DvcScreenW     := ;
  OSName         := ;
  OSNickName     := ;
  OSVersion      := ;
  DtaHabIni      := ;
  DtaHabFim      := ;
  Allowed        := ;
  LastSetAlw     := ;
  PIN            := ;
*)
var
  Nome, DeviceID, UserNmePdr, PIN: String;
  Codigo: Integer;
  SQLType: TSQLType;
  //
  CNPJEmpresa, Texto: String;
begin
  //Result         := False;
  //
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := 'Dispositivo de ' + EdUserNmePdr.Text;
  DeviceID       := EdDeviceID.Text;
  UserNmePdr     := EdUserNmePdr.Text;
  CNPJEmpresa    := EdCNPJEmpresa.Text;
  PIN            := EdPIN.Text;
  //
  if FMX_DmkPF.FIC(CNPJEmpresa = '', EdCNPJEmpresa, 'Informe o CNPJ da Empresa!') then Exit;
  if FMX_DmkPF.FIC(UserNmePdr = '', EdUserNmePdr, 'Informe o nome do respons�vel pelo aparelho!') then Exit;
  if FMX_DmkPF.FIC(PIN = '', EdUserNmePdr, 'Informe o PIN de login no aparelho!') then Exit;
  //
  FMX_dmkRemoteQuery.QuerySelect(CdOVcMobDevCad, CNPJEmpresa, //'02582267000160',
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN ',
      'FROM ovcmobdevcad',
      'WHERE DeviceID="' + EdDeviceID.Text + '"'
    ], VAR_AMBIENTE_APP);
  if CdOVcMobDevCad.RecordCount > 0 then
  begin
    if CdOVcMobDevCadCodigo.Value < 0 then
    begin
      FMX_Geral.MB_Aviso(
      'Este dispositivo j� enviou seus dados para ativa��o, mas ainda n�o foi aceito!');
    end else
    begin
      Codigo := CdOVcMobDevCadCodigo.Value;
      AtivarDevice(Codigo, Nome, DeviceID, UserNmePdr, PIN);
    end;
  end else
  begin
    FMX_Geral.MB_Info(
    'Este dispositivo ainda n�o enviou seus dados para ativa��o na empresa!' + sLineBreak +
    '    1. Clique em "Enviar dados para ativa��o" para que o servidor receba seu IMEI.' + sLineBreak +
    '    2. Contate o administrador e solicite a habilita��o do dispositivo no sistema.' + sLineBreak +
    '    3. Somente o�s a s etapas acima clique em "Buscar dados de ativa��o" para efetivar a habilita��o neste dispositivo.');
  end;
end;

procedure TFmTestes.BtExecuteClick(Sender: TObject);
begin
  if FMX_dmkRemoteQuery.QueryExecute('02582267000160',
    [
      'DROP TABLE IF EXISTS teste; ',
      'CREATE TABLE teste ( ',
      ' Codigo int(11) NOT NULL DEFAULT "0", ',
      ' Nome varchar(50) NOT NULL DEFAULT "(INDEFINIDO)", ',
      '  PRIMARY KEY (Codigo) ',
      ') ENGINE=InnoDB DEFAULT CHARSET=latin1 '
    ], VAR_AMBIENTE_APP) then
    Grl_Geral.MB_Aviso('Tabela criada com sucesso!')
  else
    Grl_Geral.MB_Erro('Falha ao criar tabela!');
end;

procedure TFmTestes.BtSelectClick(Sender: TObject);
begin
  FMX_dmkRemoteQuery.QuerySelect(ClientDataSet1, '02582267000160',
    [
      'SELECT *',
      'FROM carteiras'
    ], VAR_AMBIENTE_APP);
end;

procedure TFmTestes.Button1Click(Sender: TObject);
begin
  MeSTEP_CODE := Memo1;
  FMX_CfgDBApp.ConfiguraBD(Dmod.AllDB, Dmod.MyDB, Dmod.QrLoc, Dmod.QrUpd,
    Dmod.QrUsuarios, Dmod.QrControle, Dmod.QrEspecificos, LaUsuario);
  //FMX_CfgDBApp.MostraVerifyDB(False, Dmod.AllDB, Dmod.MyDB);
end;

procedure TFmTestes.Button2Click(Sender: TObject);
var
  Versao: Integer;
begin
  Grl_DmkDB.AtualizaVersaoTBControle(Dmod.AllDB, TDeviceType.stMobile, CO_VERSAO);
  Versao := Grl_DmkDB.ObtemVersaoAppDB(DMod.QrAux, Dmod.AllDB, istSQLite, stMobile);
  Grl_Geral.MB_Aviso('Vars�o atualizada para: ' + Grl_Geral.FF0(Versao));
end;

procedure TFmTestes.Button3Click(Sender: TObject);
var
  Nome: String;
  I, Codigo: Integer;
  SQLType: TSQLType;
begin
  for I := 1 to 3 do
  begin
    Codigo := 0;
    Codigo := Grl_DmkDB.GetNxtCodigoInt('entidades', 'Codigo', SQLType, Codigo);
    Nome := 'Nome ' + FMX_Geral.FF0(Codigo);
    if Grl_DmkDB.SQLInsUpd(Dmod.QrLoc, Dmod.AllDB,
    stIns, 'entidades', False, ['Nome'], ['Codigo'], [Nome], [Codigo], False,
    TdmkSQLInsert.dmksqlinsInsOnly, '', stWeb) then ;
  end;
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrLoc, Dmod.AllDB, [
    'SELECT Codigo, Nome ',
    'FROM entidades ',
    '']);
  while not Dmod.QrLoc.Eof do
  begin
    FMX_Geral.MB_Info(
      FMX_Geral.FF0(Dmod.QrLoc.FieldByName('Codigo').AsInteger) + ' >> ' +
      Dmod.QrLoc.FieldByName('Nome').AsString);
    Dmod.QrLoc.Next;
  end;
end;

procedure TFmTestes.Button4Click(Sender: TObject);
var
  CNPJEmpresa: String;
begin
  CNPJEmpresa    := EdCNPJEmpresa.Text;
  if FMX_DmkPF.FIC(CNPJEmpresa = '', EdCNPJEmpresa, 'Informe o CNPJ da Empresa!') then Exit;
  //
  FMX_dmkRemoteQuery.QuerySelect(CdOVcMobDevCad, CNPJEmpresa, //'02582267000160',
    [
      'SELECT Codigo, DeviceID, UserNmePdr, PIN ',
      'FROM ovcmobdevcad',
      'WHERE DeviceID="' + EdDeviceID.Text + '"'
    ], VAR_AMBIENTE_APP);
  if CdOVcMobDevCad.RecordCount > 0 then
  begin
    if CdOVcMobDevCadCodigo.Value < 0 then
    begin
      FMX_Geral.MB_Aviso(
      'Este dispositivo j� enviou seus dados para ativa��o.' + sLineBreak +
      'Clique em "Buscar dados de ativa��o" para verificar se o dispositivo foi aceito.');
    end else
    begin
      FMX_Geral.MB_Info(
      'Este dispositivo j� est� ativo na empresa!.');
    end;
  end else
  begin
    if InsereDadosAtivacaoNoServidor() then
    begin
      FMX_Geral.MB_Info(
      'Dados enviados com sucesso para a empresa!' + sLineBreak +
      '    1. Contate o administrador e solicite a habilita��o do dispositivo no sistema.' + sLineBreak +
      '    2. Clique em "Buscar dados de ativa��o" para efetivar a habilita��o neste dispositivo.');
    end else
    begin
      FMX_Geral.MB_Erro('N�o foi poss�vel executar a SQL no servidor!');
    end;
  end;
end;

procedure TFmTestes.Button5Click(Sender: TObject);
begin
(*
  Dmod.QrLoc.Connection := DMod.AllDB;
  FMX_dmkRemoteQuery.QuerySelect(Dmod.QrLoc, '02582267000160',
    [
      'SELECT name FROM sqlite_master ',
      'WHERE type=''table''',
      'AND name LIKE ''Controle''',
      'ORDER BY name;'
    ]);
*)
  //precisa configurar DB antes!
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrLoc, DMod.AllDB, [
    'SELECT name FROM sqlite_master ',
    'WHERE type=''table''',
    'ORDER BY name;',
    '']);
   SGTabelas.RowCount := Dmod.QrLoc.RecordCount + 1;
   Dmod.QrLoc.First;
   while not Dmod.QrLoc.Eof do
   begin
     SGTabelas.Cells[0,  Dmod.QrLoc.RecNo -1] := Dmod.QrLoc.Fields[0].AsString;
     //FMX_Geral.MB_Info(Dmod.QrLoc.Fields[0].AsString);
     Dmod.QrLoc.Next;
   end;
end;

procedure TFmTestes.Button6Click(Sender: TObject);
var
  Texto: String;
begin
{
  if FMX_dmkRemoteQuery.JsonText('02582267000160',
    [
      'SELECT *',
      'FROM ovdproduto'(*,
      'LIMIT 0, 10000'*)
    ], Texto, VAR_AMBIENTE_APP) then
  MeJASON.Text := Texto;
}
end;

procedure TFmTestes.FormCreate(Sender: TObject);
var
  DeviceID, DeviceName, DvcScreenH, DvcScreenW, OSName, OSNickName, OSVersion:
  String;
begin
  { This defines the default active tab at runtime }
  FTentativas := 0;
  TabControl1.ActiveTab := TabItem1;
  //
  // Dados para ativa��o do dispositivo no servidor
  Grl_AllOS.ConfiguraDadosDispositivo(DeviceID, DeviceName, DvcScreenH,
    DvcScreenW, OSName, OSNickName, OSVersion);
  EdDeviceID.Text     := DeviceID;
  EdDeviceName.Text   := DeviceName;
  EdDvcScreenH.Text   := DvcScreenH;
  EdDvcScreenW.Text   := DvcScreenW;
  EdOSName.Text       := OSName;
  EdOSNickName.Text   := OSNickName;
  EdOSVersion.Text    := OSVersion;
end;

procedure TFmTestes.FormGesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
{$IFDEF ANDROID}
  case EventInfo.GestureID of
    sgiLeft:
    begin
      if TabControl1.ActiveTab <> TabControl1.Tabs[TabControl1.TabCount-1] then
        TabControl1.ActiveTab := TabControl1.Tabs[TabControl1.TabIndex+1];
      Handled := True;
    end;

    sgiRight:
    begin
      if TabControl1.ActiveTab <> TabControl1.Tabs[0] then
        TabControl1.ActiveTab := TabControl1.Tabs[TabControl1.TabIndex-1];
      Handled := True;
    end;
  end;
{$ENDIF}
end;

procedure TFmTestes.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

function TFmTestes.InsereDadosAtivacaoNoServidor(): Boolean;
var
  //DtaHabIni, DtaHabFim,
  Nome, DeviceID, UserNmePdr, DeviceName, OSName, OSNickName, OSVersion, PIN: String;
  // Allowed, LastSetAlw
  Codigo, DvcScreenH, DvcScreenW: Integer;
  SQLType: TSQLType;
  //
  CNPJEmpresa, Texto: String;
begin
  Result         := False;
  //
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := 'Dispositivo de ' + EdUserNmePdr.Text;
  DeviceID       := EdDeviceID.Text;
  UserNmePdr     := EdUserNmePdr.Text;
  DeviceName     := EdDeviceName.Text;
  DvcScreenH     := FMX_Geral.IMV(EdDvcScreenH.Text);
  DvcScreenW     := FMX_Geral.IMV(EdDvcScreenW.Text);
  OSName         := EdOSName.Text;
  OSNickName     := EdOSNickName.Text;
  OSVersion      := EdOSVersion.Text;
  //DtaHabIni      := '0000-00-00 00:00:00';
  //DtaHabFim      := '0000-00-00 00:00:00';
  //Allowed        := 0; // N�o
  //LastSetAlw     := 0; //  OVcMobDevAlw.Codigo
  CNPJEmpresa    := EdCNPJEmpresa.Text;
  PIN            := EdPIN.Text;
  //
  if FMX_DmkPF.FIC(CNPJEmpresa = '', EdCNPJEmpresa, 'Informe o CNPJ da Empresa!') then Exit;
  if FMX_DmkPF.FIC(UserNmePdr = '', EdUserNmePdr, 'Informe o nome do respons�vel pelo aparelho!') then Exit;
  if FMX_DmkPF.FIC(PIN = '', EdUserNmePdr, 'Informe PIN a ser usado no aparelho!') then Exit;
  //
  FMX_dmkRemoteQuery.QuerySelect(CdAux, CNPJEmpresa,
    [
      'SELECT MIN(Codigo) Codigo ',
      'FROM ovcmobdevcad',
      ''
    ], VAR_AMBIENTE_APP);
  Codigo := CdAux.FieldByName('Codigo').AsInteger;
  Codigo := Codigo -1;
  //
  //Codigo := UMyMod.BPGS1I32('ovcmobdevcad', 'Codigo', '', '', tsPosNeg?, stInsUpd?, Codigo);
{
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, stIns, 'ovcmobdevcad', False, [
  FMX_dmkRemoteQuery.QuerySelect(CdAux, CNPJEmpresa,
    [
      'INSERT INTO ovcmobdevcad SET ',
      '  Nome="'       + Nome + '",',
      '  DeviceID="'   + DeviceID + '",',
      '  UserNmePdr="' + UserNmePdr + '",',
      '  DeviceName="' + DeviceName + '",',
      '  DvcScreenH="' + DvcScreenH + '",',
      '  DvcScreenW="' + DvcScreenW + '",',
      '  OSName="'     + OSName + '"',
      '  OSNickName="' + OSNickName + '"',
      '  OSVersion="' + Nome + '"',
//      '  DtaHabIni',
//      '  DtaHabFim',
      '  Allowed=0',
      //'  LastSetAlw'], [
      '  Codigo=' + FMX_Geral.FF0(Codigo),
      ''
    ]);
}
  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, stIns, 'ovcmobdevcad', False, [
  'Nome', 'DeviceID', 'UserNmePdr',
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',
  (*'DtaHabIni', 'DtaHabFim', 'Allowed',
  'LastSetAlw'*)'PIN'], [
  'Codigo'], [
  Nome, DeviceID, UserNmePdr,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,
  (*DtaHabIni, DtaHabFim, Allowed,
  LastSetAlw*)PIN], [
  Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
  False, Texto) then
  begin
    Result := FMX_dmkRemoteQuery.QueryExecute(CNPJEmpresa, Texto, VAR_AMBIENTE_APP);
  end;
end;

end.
