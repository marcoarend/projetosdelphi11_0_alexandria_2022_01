unit EXiBasico;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, FMX.DateTimeCtrls, Math, UnDmkEnums, FMX.Memo;

type
  TFmEXiBasico = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    Panel2: TPanel;
    Label11: TLabel;
    Panel1: TPanel;
    Label1: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    Panel4: TPanel;
    Label3: TLabel;
    EdEXcIdFunc: TEdit;
    TeHora: TTimeEdit;
    Panel5: TPanel;
    Label4: TLabel;
    EdValor: TEdit;
    Panel6: TPanel;
    BtConfirma: TButton;
    DeData: TDateEdit;
    Panel7: TPanel;
    BtAnoMesDec: TButton;
    EdAnoMes: TEdit;
    BtAnoMesInc: TButton;
    MeObserva: TMemo;
    procedure GrLocaisCellClick(const Column: TColumn; const Row: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure EdValorEnter(Sender: TObject);
    procedure EdValorKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtAnoMesDecClick(Sender: TObject);
    procedure BtAnoMesIncClick(Sender: TObject);
  private
    { Private declarations }
    FTypedStrValor: String;
    FTypingOKValor: Boolean;
    //
  public
    { Public declarations }
    FFormaEdicao: TEXFormaEdicao;
    FEXSeqJan: TEXSeqJan;
    FSequenciaJan, FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag: Integer;
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT: String;
    FData, FHora: TDateTime;
    FValor: Double;
    FANoMes, FObserva: String;
    FAno, FMes: Integer;
    FCodInMob, FCtrlInMob: Integer;
    FSQLType: TSQLType;
    //
  end;

var
  FmEXiBasico1: TFmEXiBasico;
  FmEXiBasico2: TFmEXiBasico;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnExes_Consts, UnExesM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnExesM_PF, UnApp_PF,
  Module(*, OViIspCenSnt*);

{$R *.fmx}

procedure TFmEXiBasico.BtAnoMesIncClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  EdAnoMes.Text := App_Pf.IncrementaAnoMes(EdAnoMes.Text, +1);
end;

procedure TFmEXiBasico.BtAnoMesDecClick(Sender: TObject);
begin
  FMX_DmkPF.VibrarComoBotao();
  //
  EdAnoMes.Text := App_Pf.IncrementaAnoMes(EdAnoMes.Text, -1);
end;

procedure TFmEXiBasico.BtConfirmaClick(Sender: TObject);
var
  sEXcIdFunc: String;
  FEXcIdFunc: Integer;
  IdFunc: Integer;
begin
  FMX_DmkPF.VibrarComoBotao();
  //Grl_Geral.MB_Info('EdEXcIdFunc.KeyBoardType.DecimalNumberPad??   ou NumberPad');
  sEXcIdFunc := Grl_Geral.SoNumero_TT(EdEXcIdFunc.Text);
  if FMX_dmkPF.FIC(sEXcIdFunc <> Trim(EdEXcIdFunc.Text), EdEXcIdFunc,
  'Valor inv�lido para ID Funcional! S� digite n�meros!') then
  begin
    EdEXcIdFunc.SetFocus;
    Exit;
  end;
  FEXcIdFunc := Grl_Geral.IMV(sEXcIdFunc);
  if FMX_dmkPF.FIC(FEXcIdFunc < 0.001, EdEXcIdFunc,
  'Valor inv�lido para ID Funcional! Valor deve ser diferente de zero!') then
  begin
    EdEXcIdFunc.SetFocus;
    Exit;
  end;
  //
  if Grl_Geral.FIC(FEXcIdFunc < 1, EdEXcIdFunc, 'Informe a Identidade funcional!') then
    Exit;
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT Codigo ',
    'FROM excidfunc ',
    'WHERE Codigo=' + Grl_Geral.FF0(FEXcIdFunc),
    EmptyStr]);
  IdFunc := 0;
  if Dmod.QrAux.RecordCount > 0 then
    IdFunc := Dmod.QrAux.FieldByName('Codigo').AsInteger;
  if Grl_Geral.FIC(FEXcIdFunc <> IdFunc, EdEXcIdFunc, 'Identidade funcional inv�lida!') then
    Exit;

  FData  := DeData.Date;
  FHora  := TeHora.Time;
  FValor := Grl_Geral.DMV(EdValor.Text);
  if Grl_Geral.FIC(FValor < 0.01, EdValor, 'Informe um valor maior que zero!!') then
    Exit;
  FAnoMes := EdAnoMes.Text;
  App_PF.AnoEMesDeAnoMes(FAnoMes, FAno, FMes);
  FObserva := MeObserva.Text;
  //InsAltAtual(MedidFei);
  //
  App_Jan.MostraFormEXi_Sequencial(FSQLType, FFormaEdicao, FEXSeqJan, FSequenciaJan,
    FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag,
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT,
    FData, FHora, FValor, FANoMes, FAno, FMes, FObserva,
    FCodInMob, FCtrlInMob);
  //Close;
end;

procedure TFmEXiBasico.EdValorEnter(Sender: TObject);
begin
  FTypedStrValor := '';
  FTypingOKValor := True;
  EdValor.CaretPosition := Length(EdValor.Text);
end;

procedure TFmEXiBasico.EdValorKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  Valor, Divisor: Double;
  Casas: Integer;
begin
  if FTypingOKValor then
  begin
    if CharInSet(KeyChar, ['0'..'9']) then
    begin
      FTypedStrValor := FTypedStrValor + KeyChar;
      Casas := CO_VALOR_CASAS_DECIMAIS;
      Valor := Grl_Geral.DMV(FTypedStrValor);
      Divisor := Power(10, Casas);
      Valor := Valor / Divisor;
      EdValor.Text := Grl_Geral.FFT(Valor, Casas, siNegativo);
      EdValor.CaretPosition := Length(EdValor.Text);
    end else
      FTypingOKValor := False;
  end;
end;

procedure TFmEXiBasico.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmEXiBasico.FormShow(Sender: TObject);
var
  Agora, Data, Hora: TDateTime;
  AnoMes: String;
  Ano, Mes: Integer;
begin
  Agora := Now();
  App_PF.DefineAnoMesDeData(Agora, AnoMes, Ano, Mes, Data, Hora);
  //Dmod.ReopenExesMOpcoes();
  Dmod.ReopenEXcMobDevCad();
  //EdEXcIdFunc.Text := Grl_Geral.FF0(Dmod.QrExesMOpcoesEXcIdFunc.Value);
  EdEXcIdFunc.Text := Grl_Geral.FF0(Dmod.QrEXcMobDevCadEXcIdFunc.Value);
  EdAnoMes.Text    := AnoMes;
  //FEXcMdoPag
  //FEXcCtaPag
  DeData.Date      := Data;
  TeHora.Time      := Hora;
  //EdValor.Text     := Grl_Geral.FFT(100.123, 2, siPositivo);
  EdValor.Text     := FloatToStrF(0, ffNumber, 10, 2) ;
  //
  try
    EdValor.SetFocus;
  except
    // nada!
  end;
end;

procedure TFmEXiBasico.GrLocaisCellClick(const Column: TColumn; const Row: Integer);
begin
(*
  ????
  App_Jan.MostraFormEXi_Sequencial(FEXSeqJan, FSequenciaJan,
    FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag,
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT);
*)
end;

end.
