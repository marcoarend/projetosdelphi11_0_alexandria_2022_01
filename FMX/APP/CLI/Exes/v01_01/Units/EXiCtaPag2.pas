unit EXiCtaPag2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, UnDmkEnums;

type
  TFmEXiCtaPag2 = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    GrLocais: TGrid;
    QrEXcCtaPag: TFDQuery;
    QrEXcCtaPagCodigo: TIntegerField;
    QrEXcCtaPagNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrEXcCtaPagBRK_Nome: TStringField;
    procedure GrLocaisCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure QrEXcCtaPagCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FFormaEdicao: TEXFormaEdicao;
    FEXSeqJan: TEXSeqJan;
    FSequenciaJan, FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag: Integer;
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT: String;
    FData, FHora: TDateTime;
    FValor: Double;
    FANoMes, FObserva: String;
    FAno, FMes: Integer;
    FCodInMob, FCtrlInMob: Integer;
    FSQLType: TSQLType;
    //
    procedure ReopenEXcCtaPag(ParteNome: String);
  end;

var
  FmEXiCtaPag2: TFmEXiCtaPag2;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnExes_Consts, UnExesM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnExesM_PF,
  Module(*, OViIspCenSnt*);

{$R *.fmx}

procedure TFmEXiCtaPag2.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmEXiCtaPag2.FormShow(Sender: TObject);
begin
  ReopenEXcCtaPag('');
end;

procedure TFmEXiCtaPag2.GrLocaisCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOViLocais.GrLocaisCellClick()';
  Artigo = 0;
  NrOP   = 0;
  NO_Artigo = '';
  NO_NrOP   = '';
begin
  // Enviados
  //VAR_LAST_MDOPAG_COD := QrEXcCtaPagCodigo.Value;
  //VAR_LAST_MDOPAG_NOM := QrEXcCtaPagNome.Value;
  //
  if (QrEXcCtaPag.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
(*
  case VAR_OPCOES_MOB_FoLocObjIns of
    0: App_Jan.MostraFormOViArtigos(FSegmentoInsp, FSeccaoInsp,
       QrOViLocaisCodigo.Value, Artigo, QrOViLocaisNome.Value);
    1: App_Jan.MostraFormOViOPs(FSegmentoInsp, FSeccaoInsp,
       QrOViLocaisCodigo.Value, NrOP, QrOViLocaisNome.Value, NO_Artigo);
    else Grl_Geral.MB_Erro('Janela n�o implementada!' + sProcName);
  end;
*)
  FEXcCtaPag     := QrEXcCtaPagCodigo.Value;
  FEXcCtaPag_TXT := QrEXcCtaPagNome.Value;
  //
  App_Jan.MostraFormEXi_Sequencial(FSQLType, FFormaEdicao, FEXSeqJan, FSequenciaJan,
    FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag,
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT,
    FData, FHora, FValor, FANoMes, FAno, FMes, FObserva,
    FCodInMob, FCtrlInMob);
  //Close;
  Close;
end;

procedure TFmEXiCtaPag2.QrEXcCtaPagCalcFields(DataSet: TDataSet);
begin
  QrEXcCtaPagBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrLocais, 1, QrEXcCtaPagNome.Value);
end;

procedure TFmEXiCtaPag2.ReopenEXcCtaPag(ParteNome: String);
var
  SQL_AND: String;
begin
  SQL_AND := '';
  if Trim(ParteNome) <> '' then
    SQL_AND := 'AND mpg.Nome LIKE "%' + ParteNome + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrEXcCtaPag, Dmod.AllDB, [
  'SELECT mpg.Codigo, mpg.Nome',
  'FROM excctapag mpg ',
  'WHERE Ativo=1 ',
  SQL_AND,
  'GROUP BY mpg.Codigo ',
  'ORDER BY mpg.Nome ',
  '']);
end;

procedure TFmEXiCtaPag2.SbPesquisaClick(Sender: TObject);
begin
  ReopenEXcCtaPag(EdPesquisa.Text);
end;

end.
