unit EXmDspLctCab;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, UnDmkENums, FMX.Memo;

type
  TFmEXmDspLctCab = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    Panel1: TPanel;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    PnNrOP: TPanel;
    Label3: TLabel;
    LaEXcIdFunc: TLabel;
    Label4: TLabel;
    LaValor: TLabel;
    MeObserva: TMemo;
    QrEXmDspLctCab: TFDQuery;
    QrEXmDspLctCabCodInMob: TIntegerField;
    QrEXmDspLctCabObserva: TStringField;
    QrEXmDspLctCabValor: TFloatField;
    Panel2: TPanel;
    Label1: TLabel;
    LaCodInMob: TLabel;
    Label5: TLabel;
    LaData: TLabel;
    QrEXmDspLctCabAnoMes: TStringField;
    QrEXmDspLctCabAno: TIntegerField;
    QrEXmDspLctCabMes: TIntegerField;
    QrEXmDspLctCabEXcIdFunc: TIntegerField;
    QrEXmDspLctCabData: TDateField;
    QrEXmDspLctCabHora: TTimeField;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    QrEXmDspLctIts: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    GrLocais: TGrid;
    QrEXmDspLctItsCodInMob: TIntegerField;
    QrEXmDspLctItsValor: TFloatField;
    QrEXmDspLctItsAnoMes: TStringField;
    QrEXmDspLctItsAno: TIntegerField;
    QrEXmDspLctItsMes: TIntegerField;
    QrEXmDspLctItsEXcIdFunc: TIntegerField;
    QrEXmDspLctItsData: TDateField;
    QrEXmDspLctItsHora: TTimeField;
    QrEXmDspLctItsCtrlInMob: TIntegerField;
    QrEXmDspLctItsEXcCtaPag: TIntegerField;
    QrEXmDspLctItsEXcMdoPag: TIntegerField;
    QrEXmDspLctItsNO_EXcCtaPag: TStringField;
    QrEXmDspLctItsNO_EXcMdoPag: TStringField;
    Panel3: TPanel;
    BtMaisUmItem: TButton;
    BtAnoMesInc: TButton;
    procedure QrEXmDspLctCabCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure QrEXmDspLctCabAfterOpen(DataSet: TDataSet);
    procedure BtMaisUmItemClick(Sender: TObject);
    procedure BtAnoMesIncClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FFormaEdicao: TEXFormaEdicao;
    FEXSeqJan: TEXSeqJan;
    FSequenciaJan, FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag: Integer;
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT: String;
    FData, FHora: TDateTime;
    FValor: Double;
    FANoMes: String;
    FAno, FMes: Integer;
    FCodInMob, FCtrlInMob: Integer;
    FSQLType: TSQLType;
    //
    procedure ReopenEXmDspLctCab(CodInMob: Integer);
    procedure ReopenEXmDspLctIts();

  end;

var
  FmEXmDspLctCab: TFmEXmDspLctCab;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnExes_Consts, UnExesM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnExesM_PF,
  Module(*, OViIspCenSnt*), UnFMX_Geral;

{$R *.fmx}

procedure TFmEXmDspLctCab.BtAnoMesIncClick(Sender: TObject);
const
  SQLTYpe = stIns;
var
  CodInMob: Integer;
  Valor: Double;
begin
  //
  CodInMob := QrEXmDspLctCabCodInMob.Value;
  Valor    := QrEXmDspLctCabValor.Value;
  //
  App_Jan.MostraFormEXiStCmprv(SQLType, (*FormaEdicao:
  TEXFormaEdicao; EXSeqJan: TEXSeqJan; SequenciaJan,
  EXcIdFunc, EXcMdoPag, EXcStCmprv, EXcCtaPag: Integer;
  EXcMdoPag_TXT, EXcStCmprv_TXT, EXcCtaPag_TXT: String;
  Data, Hora: TDateTime;*) Valor,(*: Double;
  AnoMes: String; Ano, Mes: Integer; Observa: String;*)
  FCodInMob(*, CtrlInMob: Integer*));
end;

procedure TFmEXmDspLctCab.BtMaisUmItemClick(Sender: TObject);
var
  SequenciaJan: Integer;
  EXSeqJan: TEXSeqJan;
begin
  SequenciaJan := 1;
  EXSeqJan     := exsj_06_MaisUmItem;
  //
  App_Jan.MostraFormEXi_Sequencial(stIns, TEXFormaEdicao.exfe_01_MaisInc,
  (*F*)EXSeqJan, (*F*)SequenciaJan,
  FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag,
  FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT, FData, FHora, FValor,
  FAnoMes, FAno, FMes, MeObserva.Text,
  FCodInMob, (*FCtrlInMob*)0);
  //
  //Close;
(*
  App_Jan.MostraFormEXiCtaPag2(TEXFormaEdicao.exfe_01_MaisInc,
  FEXSeqJan, FSequenciaJan, FEXcIdFunc, FEXcMdoPag, FEXcStCmprv,
  FEXcCtaPag, FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT,
  FData, FHora, FValor, FAnoMes, FAno, FMes,
  MeObserva.Text, FCodInMob, FCtrlInMob);
  //
*)
end;

procedure TFmEXmDspLctCab.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmEXmDspLctCab.FormShow(Sender: TObject);
begin
  ReopenEXmDspLctCab(FCodInMob);
end;

procedure TFmEXmDspLctCab.QrEXmDspLctCabAfterOpen(DataSet: TDataSet);
begin
  ReopenEXmDspLctIts();
end;

procedure TFmEXmDspLctCab.QrEXmDspLctCabCalcFields(DataSet: TDataSet);
begin
(*
  QrEXcMdoPagBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrLocais, 1, QrEXcMdoPagNome.Value);
*)
end;

procedure TFmEXmDspLctCab.ReopenEXmDspLctCab(CodInMob: Integer);
begin
  Grl_DmkDB.AbreSQLQuery0(QrEXmDspLctCab, Dmod.AllDB, [
  'SELECT dlc.*',
  'FROM exmdsplctcab dlc ',
  'WHERE dlc.CodInMob=' + Grl_Geral.FF0(FCodInMob),
  '']);
  LaCodInMob.Text  := Grl_Geral.FF0(QrEXmDspLctCabCodInMob.Value);
  LaValor.Text     := Grl_Geral.FFT(QrEXmDspLctCabValor.Value, 2, siNegativo);
  LaEXcIDFunc.Text := Grl_Geral.FF0(QrEXmDspLctCabEXcIdFunc.Value);
  LaData.Text      := Grl_Geral.FDT(QrEXmDspLctCabData.Value, 1);
  MeObserva.Text   := QrEXmDspLctCabObserva.Value;
end;

procedure TFmEXmDspLctCab.ReopenEXmDspLctIts;
begin
  Grl_DmkDB.AbreSQLQuery0(QrEXmDspLctIts, Dmod.AllDB, [
  'SELECT pag.Nome NO_EXcCtaPag, ',
  'mdo.Nome NO_EXcMdoPag, dli.* ',
  'FROM exmdsplctits dli ',
  'LEFT JOIN excctapag pag ON pag.Codigo=dli.EXcCtaPag ',
  'LEFT JOIN excmdopag mdo ON mdo.Codigo=dli.EXcMdoPag ',
  'WHERE dli.CodInMob=' + Grl_Geral.FF0(QrEXmDspLctCabCodInMob.Value),
  '']);
end;

end.
