object Dmod: TDmod
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 364
  Width = 557
  PixelsPerInch = 96
  object QrLoc: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 31
    Top = 21
  end
  object QrControle: TFDQuery
    SQL.Strings = (
      'SELECT Versao FROM controle ')
    Left = 31
    Top = 84
  end
  object QrUsuarios: TFDQuery
    SQL.Strings = (
      'SELECT Versao FROM controle ')
    Left = 87
    Top = 84
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 160
    Top = 84
  end
  object QrAux: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 71
    Top = 21
  end
  object QrUpd: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 119
    Top = 21
  end
  object QrEspecificos: TFDQuery
    SQL.Strings = (
      '')
    Left = 31
    Top = 140
  end
  object AllDB: TFDConnection
    Params.Strings = (
      'Database=C:\Executaveis\19.0\OverSeerMob\overseermob.s3db'
      'DriverID=SQLite')
    FetchOptions.AssignedValues = [evLiveWindowParanoic, evLiveWindowFastFirst]
    FetchOptions.LiveWindowFastFirst = True
    FormatOptions.AssignedValues = [fvSE2Null]
    LoginPrompt = False
    Left = 488
    Top = 5
  end
  object MyDB: TFDConnection
    Params.Strings = (
      'DriverID=SQLite')
    FetchOptions.AssignedValues = [evLiveWindowFastFirst]
    FetchOptions.LiveWindowFastFirst = True
    LoginPrompt = False
    Left = 448
    Top = 5
  end
  object QrExesMOpcoes: TFDQuery
    AfterOpen = QrExesMOpcoesAfterOpen
    Connection = MyDB
    SQL.Strings = (
      'SELECT * FROM exesmopcoes')
    Left = 32
    Top = 192
    object QrExesMOpcoesERPNameByCli: TStringField
      FieldName = 'ERPNameByCli'
      Size = 60
    end
    object QrExesMOpcoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExesMOpcoesScaleX: TIntegerField
      FieldName = 'ScaleX'
    end
    object QrExesMOpcoesScaleY: TIntegerField
      FieldName = 'ScaleY'
    end
    object QrExesMOpcoesEstiloForms: TStringField
      FieldName = 'EstiloForms'
      Size = 60
    end
    object QrExesMOpcoesMailUser: TStringField
      FieldName = 'MailUser'
      Size = 255
    end
    object QrExesMOpcoesAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrExesMOpcoesUsarHTTPS: TSmallintField
      FieldName = 'UsarHTTPS'
    end
    object QrExesMOpcoesUsarApiDev: TSmallintField
      FieldName = 'UsarApiDev'
    end
    object QrExesMOpcoesEXcIdFunc: TIntegerField
      FieldName = 'EXcIdFunc'
    end
    object QrExesMOpcoesEXSeqJan: TSmallintField
      FieldName = 'EXSeqJan'
    end
  end
  object FDPragma: TFDQuery
    Connection = AllDB
    Left = 256
    Top = 74
  end
  object QrEXcMobDevCad: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT * '
      'FROM ovcmobdevcad')
    Left = 256
    Top = 124
    object QrEXcMobDevCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrEXcMobDevCadNome: TStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Size = 60
    end
    object QrEXcMobDevCadDeviceID: TStringField
      FieldName = 'DeviceID'
      Origin = 'DeviceID'
      Size = 60
    end
    object QrEXcMobDevCadUserNmePdr: TStringField
      FieldName = 'UserNmePdr'
      Origin = 'UserNmePdr'
      Size = 60
    end
    object QrEXcMobDevCadDeviceName: TStringField
      FieldName = 'DeviceName'
      Origin = 'DeviceName'
      Size = 60
    end
    object QrEXcMobDevCadDvcScreenH: TIntegerField
      FieldName = 'DvcScreenH'
      Origin = 'DvcScreenH'
    end
    object QrEXcMobDevCadDvcScreenW: TIntegerField
      FieldName = 'DvcScreenW'
      Origin = 'DvcScreenW'
    end
    object QrEXcMobDevCadOSName: TStringField
      FieldName = 'OSName'
      Origin = 'OSName'
      Size = 60
    end
    object QrEXcMobDevCadOSNickName: TStringField
      FieldName = 'OSNickName'
      Origin = 'OSNickName'
      Size = 60
    end
    object QrEXcMobDevCadOSVersion: TStringField
      FieldName = 'OSVersion'
      Origin = 'OSVersion'
      Size = 60
    end
    object QrEXcMobDevCadDtaHabIni: TDateTimeField
      FieldName = 'DtaHabIni'
      Origin = 'DtaHabIni'
      Required = True
    end
    object QrEXcMobDevCadDtaHabFim: TDateTimeField
      FieldName = 'DtaHabFim'
      Origin = 'DtaHabFim'
      Required = True
    end
    object QrEXcMobDevCadAllowed: TShortintField
      FieldName = 'Allowed'
      Origin = 'Allowed'
    end
    object QrEXcMobDevCadLastSetAlw: TIntegerField
      FieldName = 'LastSetAlw'
      Origin = 'LastSetAlw'
    end
    object QrEXcMobDevCadPIN: TStringField
      FieldName = 'PIN'
      Origin = 'PIN'
      Required = True
      Size = 60
    end
    object QrEXcMobDevCadScaleX: TIntegerField
      FieldName = 'ScaleX'
      Origin = 'ScaleX'
      Required = True
    end
    object QrEXcMobDevCadScaleY: TIntegerField
      FieldName = 'ScaleY'
      Origin = 'ScaleY'
      Required = True
    end
    object QrEXcMobDevCadChmOcorrencias: TSmallintField
      FieldName = 'ChmOcorrencias'
    end
    object QrEXcMobDevCadEXcIdFunc: TIntegerField
      FieldName = 'EXcIdFunc'
    end
    object QrEXcMobDevCadEXSeqJan: TSmallintField
      FieldName = 'EXSeqJan'
    end
  end
  object QrChmOcoDon: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT * '
      'FROM chmocodon'
      'WHERE Controle=0')
    Left = 352
    Top = 56
    object QrChmOcoDonCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object QrChmOcoDonControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'Controle'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrChmOcoDonChmOcoEtp: TIntegerField
      FieldName = 'ChmOcoEtp'
      Origin = 'ChmOcoEtp'
      Required = True
    end
    object QrChmOcoDonChmOcoWho: TIntegerField
      FieldName = 'ChmOcoWho'
      Origin = 'ChmOcoWho'
      Required = True
    end
    object QrChmOcoDonObservacao: TStringField
      FieldName = 'Observacao'
      Origin = 'Observacao'
      Size = 255
    end
    object QrChmOcoDonDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Origin = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoDonCloseUser: TIntegerField
      FieldName = 'CloseUser'
      Origin = 'CloseUser'
      Required = True
    end
    object QrChmOcoDonCloseDtHr: TDateTimeField
      FieldName = 'CloseDtHr'
      Origin = 'CloseDtHr'
      Required = True
    end
    object QrChmOcoDonMobUpWeb: TShortintField
      FieldName = 'MobUpWeb'
      Origin = 'MobUpWeb'
      Required = True
    end
  end
  object QrChmOcoPux: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT * '
      'FROM chmocopux '
      'WHERE DtHrLido<="1900-01-01"')
    Left = 352
    Top = 8
    object QrChmOcoPuxCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrChmOcoPuxNome: TStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Size = 100
    end
    object QrChmOcoPuxTxtLocal: TStringField
      FieldName = 'TxtLocal'
      Origin = 'TxtLocal'
      Size = 100
    end
    object QrChmOcoPuxDtHrPush: TDateTimeField
      FieldName = 'DtHrPush'
      Origin = 'DtHrPush'
      Required = True
    end
    object QrChmOcoPuxDtHrLido: TDateTimeField
      FieldName = 'DtHrLido'
      Origin = 'DtHrLido'
      Required = True
    end
    object QrChmOcoPuxDtHrCatx: TDateTimeField
      FieldName = 'DtHrCatx'
    end
    object QrChmOcoPuxQuandoIni: TDateTimeField
      FieldName = 'QuandoIni'
      Origin = 'QuandoIni'
      Required = True
    end
    object QrChmOcoPuxQuandoFim: TDateTimeField
      FieldName = 'QuandoFim'
      Origin = 'QuandoFim'
      Required = True
    end
  end
  object QrEpoch: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      
        'SELECT datetime(1578009128645/1000, '#39'unixepoch'#39', '#39'localtime'#39') AS' +
        ' DataHora;')
    Left = 148
    Top = 264
    object QrEpochDataHora: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'DataHora'
      Origin = 'DataHora'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
  end
  object QrChmOcoWho: TFDQuery
    Connection = AllDB
    SQL.Strings = (
      'SELECT who.* '
      'FROM chmocowho who '
      'WHERE who.MobUpWeb=0 '
      'AND who.WhoDoMobile>0')
    Left = 352
    Top = 104
    object QrChmOcoWhoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object QrChmOcoWhoControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'Controle'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QrChmOcoWhoOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'Ordem'
      Required = True
    end
    object QrChmOcoWhoKndWhoPrtnc: TIntegerField
      FieldName = 'KndWhoPrtnc'
      Origin = 'KndWhoPrtnc'
      Required = True
    end
    object QrChmOcoWhoWhoDoEnti: TIntegerField
      FieldName = 'WhoDoEnti'
      Origin = 'WhoDoEnti'
      Required = True
    end
    object QrChmOcoWhoWhoDoMobile: TIntegerField
      FieldName = 'WhoDoMobile'
      Origin = 'WhoDoMobile'
      Required = True
    end
    object QrChmOcoWhoDoneDtHr: TDateTimeField
      FieldName = 'DoneDtHr'
      Origin = 'DoneDtHr'
      Required = True
    end
    object QrChmOcoWhoSntNotif: TDateTimeField
      FieldName = 'SntNotif'
      Origin = 'SntNotif'
      Required = True
    end
    object QrChmOcoWhoCfmRcbNotif: TDateTimeField
      FieldName = 'CfmRcbNotif'
      Origin = 'CfmRcbNotif'
      Required = True
    end
    object QrChmOcoWhoCfmDtHrLido: TDateTimeField
      FieldName = 'CfmDtHrLido'
      Origin = 'CfmDtHrLido'
      Required = True
    end
    object QrChmOcoWhoMobUpWeb: TShortintField
      FieldName = 'MobUpWeb'
      Origin = 'MobUpWeb'
      Required = True
    end
    object QrChmOcoWhoRealizado: TShortintField
      FieldName = 'Realizado'
      Origin = 'Realizado'
      Required = True
    end
    object QrChmOcoWhoObsEncerr: TStringField
      FieldName = 'ObsEncerr'
      Origin = 'ObsEncerr'
      Size = 255
    end
  end
end
