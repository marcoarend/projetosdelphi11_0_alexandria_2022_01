unit EXiMdoPag;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.StdCtrls,
  FMX.Controls.Presentation, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.Edit,
  UnProjGroupEnums, UnDmkEnums;

type
  TFmEXiMdoPag = class(TForm)
    TopToolbar: TToolBar;
    TitleLabel: TLabel;
    DoneButton: TSpeedButton;
    GrLocais: TGrid;
    QrEXcMdoPag: TFDQuery;
    QrEXcMdoPagCodigo: TIntegerField;
    QrEXcMdoPagNome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    Panel1: TPanel;
    SbPesquisa: TSpeedButton;
    EdPesquisa: TEdit;
    StyleBook_Air: TStyleBook;
    StyleBook_Amakrits: TStyleBook;
    StyleBook_AquaGraphite: TStyleBook;
    StyleBook_Blend: TStyleBook;
    StyleBook_Dark: TStyleBook;
    StyleBook_GoldenGraphite: TStyleBook;
    StyleBook_Light: TStyleBook;
    StyleBook_RubyGraphite: TStyleBook;
    StyleBook_Transparent: TStyleBook;
    QrEXcMdoPagBRK_Nome: TStringField;
    procedure GrLocaisCellClick(const Column: TColumn; const Row: Integer);
    procedure SbPesquisaClick(Sender: TObject);
    procedure QrEXcMdoPagCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }

  public
    { Public declarations }
    FFormaEdicao: TEXFormaEdicao;
    FEXSeqJan: TEXSeqJan;
    FSequenciaJan, FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag: Integer;
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT: String;
    FData, FHora: TDateTime;
    FValor: Double;
    FANoMes, FObserva: String;
    FAno, FMes: Integer;
    FCodInMob, FCtrlInMob: Integer;
    FSQLType: TSQLType;
    //
    procedure ReopenEXcMdoPag(ParteNome: String);
  end;

var
  FmEXiMdoPag: TFmEXiMdoPag;

implementation

uses
  UnGrl_Geral, UnGrl_DmkDB, UnExes_Consts, UnExesM_Vars, UnApp_Jan,
  UnFMX_DmkProcFunc, UnGrl_Vars, UnExesM_PF,
  Module(*, OViIspCenSnt*), UnFMX_Geral;

{$R *.fmx}

procedure TFmEXiMdoPag.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  FMX_DmkPF.FormKeyUp(Sender, Key, KeyChar, Shift);
end;

procedure TFmEXiMdoPag.FormShow(Sender: TObject);
begin
  ReopenEXcMdoPag('');
end;

procedure TFmEXiMdoPag.GrLocaisCellClick(const Column: TColumn; const Row: Integer);
const
  sProcName = 'TFmOViLocais.GrLocaisCellClick()';
  Artigo = 0;
  NrOP   = 0;
  NO_Artigo = '';
  NO_NrOP   = '';
(*
var
  IdFunc: Integer;
*)
begin
  // Enviados
  VAR_LAST_MDOPAG_COD := QrEXcMdoPagCodigo.Value;
  VAR_LAST_MDOPAG_NOM := QrEXcMdoPagNome.Value;
  //
  if (QrEXcMdoPag.RecordCount > 0)  then
    FMX_DmkPF.VibrarComoBotao();
(*
  case VAR_OPCOES_MOB_FoLocObjIns of
    0: App_Jan.MostraFormOViArtigos(FSegmentoInsp, FSeccaoInsp,
       QrOViLocaisCodigo.Value, Artigo, QrOViLocaisNome.Value);
    1: App_Jan.MostraFormOViOPs(FSegmentoInsp, FSeccaoInsp,
       QrOViLocaisCodigo.Value, NrOP, QrOViLocaisNome.Value, NO_Artigo);
    else Grl_Geral.MB_Erro('Janela n�o implementada!' + sProcName);
  end;
*)


{
  //FEXcIdFunc      := Grl_Geral.IMV(EdEXcIdFunc.Text);
  FEXcMdoPag      := QrEXcMdoPagCodigo.Value;
  FEXcMdoPag_TXT  := QrEXcMdoPagNome.Value;
  //
  if FMX_Geral.FIC(FEXcIdFunc < 1, 'Informe a Identidade funcional!') then
    Exit;
  if FMX_Geral.FIC(FEXcMdoPag < 1, 'Informe o Modo de Pagamento!') then
    Exit;
  Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT Codigo ',
    'FROM excidfunc ',
    'WHERE Codigo=' + Grl_Geral.FF0(FEXcIdFunc),
    EmptyStr]);
  IdFunc := 0;
  if Dmod.QrAux.RecordCount > 0 then
    IdFunc := Dmod.QrAux.FieldByName('Codigo').AsInteger;
  if FMX_Geral.FIC(FEXcIdFunc <> IdFunc, 'Identidade funcional inv�lida!') then
    Exit;
  //
}
(*
  App_Jan.MostraFormEXiStCmprv(FSequenciaJan,
    FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag,
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT);
*)
  FEXcMdoPag      := QrEXcMdoPagCodigo.Value;
  FEXcMdoPag_TXT  := QrEXcMdoPagNome.Value;
  //
  App_Jan.MostraFormEXi_Sequencial(FSQLType, FFormaEdicao, FEXSeqJan, FSequenciaJan,
    FEXcIdFunc, FEXcMdoPag, FEXcStCmprv, FEXcCtaPag,
    FEXcMdoPag_TXT, FEXcStCmprv_TXT, FEXcCtaPag_TXT,
    FData, FHora, FValor, FANoMes, FAno, FMes, FObserva,
    FCodInMob, FCtrlInMob);
  //Close;
end;

procedure TFmEXiMdoPag.QrEXcMdoPagCalcFields(DataSet: TDataSet);
begin
  QrEXcMdoPagBRK_Nome.Value :=
    FMX_DmkPF.QuebraTextoEmGrid(GrLocais, 1, QrEXcMdoPagNome.Value);
end;

procedure TFmEXiMdoPag.ReopenEXcMdoPag(ParteNome: String);
var
  SQL_AND: String;
begin
  SQL_AND := '';
  if Trim(ParteNome) <> '' then
    SQL_AND := 'AND mpg.Nome LIKE "%' + ParteNome + '%"';
  //
  Grl_DmkDB.AbreSQLQuery0(QrEXcMdoPag, Dmod.AllDB, [
  'SELECT mpg.Codigo, mpg.Nome',
  'FROM excmdopag mpg ',
  'WHERE Ativo=1 ',
  SQL_AND,
  'GROUP BY mpg.Codigo ',
  'ORDER BY mpg.Nome ',
  '']);
end;

procedure TFmEXiMdoPag.SbPesquisaClick(Sender: TObject);
begin
  ReopenEXcMdoPag(EdPesquisa.Text);
end;

(*

object Panel2: TPanel
  Align = Top
  Position.Y = 48.000000000000000000
  Size.Width = 360.000000000000000000
  Size.Height = 53.000000000000000000
  Size.PlatformDefault = False
  TabOrder = 5
  object Label11: TLabel
    Position.X = 12.000000000000000000
    Position.Y = 18.000000000000000000
    Size.Width = 150.000000000000000000
    Size.Height = 23.000000000000000000
    Size.PlatformDefault = False
    Text = 'ID Funcional:'
    TabOrder = 16
  end
  object EdEXcIdFunc: TEdit
    Touch.InteractiveGestures = [LongTap, DoubleTap]
    TabOrder = 1
    KeyboardType = NumberPad
    Position.X = 188.000000000000000000
    Position.Y = 13.000000000000000000
    Size.Width = 150.000000000000000000
    Size.Height = 32.000000000000000000
    Size.PlatformDefault = False
  end
end

*)

end.
