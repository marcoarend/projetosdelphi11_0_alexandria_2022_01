unit UnApp_PF;

interface

uses System.JSON, System.SysUtils,

  //IPPeerClient,
  REST.Backend.PushTypes, System.PushNotification,
  //REST.Backend.EMSPushDevice,  REST.Backend.EMSProvider,
  //Data.Bind.Components, Data.Bind.ObjectScope, REST.Backend.BindSource,
  //REST.Backend.PushDevice,

  UnGrl_Vars, UnFMX_Grl_Vars, UnProjGroup_Consts, UnDmkEnums;
type
  TArr1NivJSON = array of array[0..1] of String;
  //
  TUnApp_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AcoesFinaisDeTirarFoto(): String;
    function  DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN, SQL_WHERE: String;
(*
              ExcluiItensTabela: Boolean = True;
              ComLastFlds: Boolean = False): Boolean;
*)
              ExcluiItensTabela: Boolean;
              ComLastFlds: Boolean): Boolean;
    procedure InsAltPushRecebido(SQLType: TSQLType; Codigo: Integer; Nome,
              TxtLocal, unixepoch_X_1000, QuandoIni, QuandoFim: String);
    procedure PushNotifications_RegistraDevice();
    procedure PushNotifications_ReceiveDestrincha(Instrucoes: TPushData);
    procedure PushNotifications_ReceiveDestrincha2(Instrucoes: TArr1NivJSON);

  end;
var
  App_PF: TUnApp_PF;
  //
  VAR_DeviceToken_REGISTERED: Boolean = False;

implementation

uses UnOVSM_Vars, UnGrl_DmkDB, UnGrl_Geral, UnFMX_DmkRemoteQuery, UnApp_Jan,
  Module, OViInspSeqNcfAct, OViIspCenSel,
    Principal;

{ TUnApp_PF }

function TUnApp_PF.AcoesFinaisDeTirarFoto(): String;
var
  Tabela: String;
  QtdFotos, CtrlInMob: Integer;
begin
  if (VAR_FOTO_COD_IN_MOB <> 0) and (VAR_FOTO_CTRL_IN_MOB <> 0) then
  begin
    case VAR_FOTO_ID_TABELA of
      0: Tabela := Lowercase('OVmIspMobInc');
      1: Tabela := Lowercase('OVmIspMobMed');
      2: Tabela := Lowercase('OVmIspMobLvr');
      else Tabela := Lowercase('OVmIspMob???');
    end;
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT CtrlInMob ',
    'FROM ovmispmobfts ',
    'WHERE IdTabela=' + Grl_Geral.FF0(VAR_FOTO_ID_TABELA),
    'AND CodInMob=' + Grl_Geral.FF0(VAR_FOTO_COD_IN_MOB),
    'AND CtrlInMob=' + Grl_Geral.FF0(VAR_FOTO_CTRL_IN_MOB),
    EmptyStr]);
    //
    QtdFotos  := Dmod.QrAux.RecordCount;
    CtrlInMob := VAR_FOTO_CTRL_IN_MOB;
    //
    if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, Tabela, False, [
    'QtdFotos'], ['CtrlInMob'], [
    QtdFotos], [CtrlInMob], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
    begin
      case VAR_FOTO_ID_TABELA of
        0: FmOViInspSeqNcfAct.ReopenOVmIspMobFts();
        1: ;//
        2: FmOViInspSeqNcfAct.ReopenOVmIspMobFts();
      end;
    end;
    FmOViIspCenSel.ReopenPontosNeg();
  end;
end;

function TUnApp_PF.DownloadTabela(TabSrc, Alias, TabDst, SQL_JOIN,
  SQL_WHERE: String; ExcluiItensTabela: Boolean;
  ComLastFlds: Boolean): Boolean;
var
  Campo, Campos, Lit, SQL, JSON, _Alias, MyAlias: String;
begin
  Result := False;
  Campos := '';
  _Alias := Trim(Alias);
  if _Alias <> '' then
  begin
    MyAlias := _Alias + '.';
    _Alias  := ' ' + _Alias + ' ';
  end else
  begin
    MyAlias := '';
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(Dmod.FDPragma, Dmod.AllDB, [
    'PRAGMA table_info(' + Lowercase(TabSrc) + ')',
    ''
  ]);
  if Dmod.FDPragma.RecordCount = 0 then Exit;
  //
// Cid - Name      - Type    - notnull - dflt_value  -  pk
//   0   Codigo      int(11)     0/1        null        0/1

  //SQL := Dmod.FDPragma.FieldByName('Name').AsString + sLineBreak;
  SQL := MyAlias + Dmod.FDPragma.FieldByName('Name').AsString;
  Lit := Dmod.FDPragma.FieldByName('Name').AsString;
  Dmod.FDPragma.Next;
  while not Dmod.FDPragma.Eof do
  begin
    Campo := MyAlias + Dmod.FDPragma.FieldByName('Name').AsString;
    //SQL := SQL + ', ' + Dmod.FDPragma.FieldByName('Name').AsString + sLineBreak;
    if ComLastFlds then
    begin
      SQL := SQL + ', ' + Campo;
      Lit := Lit + ', ' + Campo;
    end else
    begin
      if (Campo = 'LastModifi') or (Campo = 'LastAcao') then
      begin
        // nada
      end else
      begin
        SQL := SQL + ', ' + Campo;
        Lit := Lit + ', ' + Campo;
      end;
    end;
(*
INSERT INTO table (column1,column2 ,..)
VALUES( value1,    value2 ,...);
*)
    Dmod.FDPragma.Next;
  end;
  //
  //FMX_Geral.MB_Info(SQL);
  //FMX_Geral.MB_Info(Lit);
  SQL :=
    'SELECT ' + SQL + sLineBreak +
    'FROM ' + TabSrc + _Alias + sLineBreak +
    SQL_JOIN + sLineBreak +
    SQL_WHERE + '';
  //
  //Lit := 'INSERT INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  Lit := 'INSERT OR REPLACE INTO ' + TabDst + ' (' + Lit + ')' + sLineBreak;
  //Grl_Geral.MB_Aviso(SQL);
  if FMX_DmkRemoteQuery.JsonText([SQL], JSON) then
  begin
    //FMX_Geral.MB_Info(JSON);
    if ExcluiItensTabela then
      Grl_DmkDB.ExecutaSQLQuery0(Dmod.FDPragma, Dmod.AllDB, ['DELETE FROM ' + TabDst]);
    if JSON <> '[]' then
    begin
      Lit := Lit + FMX_DmkRemoteQuery.ParseJson2(JSON);
      //FMX_Geral.MB_Info(Lit);
      Grl_DmkDB.ExecutaSQLQuery0(Dmod.FDPragma, Dmod.AllDB, [Lit]);
      //
    end;
    Result := True;
  end;
end;

procedure TUnApp_PF.InsAltPushRecebido(SQLType: TSQLType; Codigo: Integer; Nome,
  TxtLocal, unixepoch_X_1000, QuandoIni, QuandoFim: String);
var
  DtHrLido, DtHrCatx, DtHrPush: String;
begin
//google.sent_time=1578009128645
  DtHrLido := '0000-00-00 00:00:00';
  DtHrPush := '0000-00-00 00:00:00';
  //if Grl_Geral.IMV(unixepoch_X_1000) > 86400000 then
  if Length(unixepoch_X_1000) > 8 then //86400000
  begin
    DtHrCatx := Dmod.ObtemDataHoraDeEpochTime_X_1000(unixepoch_X_1000);
  end else
    DtHrCatx := '0000-00-00 00:00:00';
  //
  Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, 'WHERE Codigo=' + Grl_Geral.FF0(Codigo));
  if Dmod.QrChmOcoPux.RecordCount = 0 then
  begin
    Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocopux', False, [
    'Nome', 'TxtLocal', 'DtHrLido',
    'DtHrPush', 'DtHrCatx', 'QuandoIni',
    'QuandoFim'], ['Codigo'], [
    Nome, TxtLocal, DtHrLido,
    DtHrPush, DtHrCatx, QuandoIni,
    QuandoFim], [Codigo], True(*False*),
    TdmkSQLInsert.dmksqlinsInsIgnore, '', stMobile); // then
  end;
end;

procedure TUnApp_PF.PushNotifications_ReceiveDestrincha(Instrucoes: TPushData);
const
  ProcName = 'TUnApp_PF.PushNotifications_ReceiveDestrincha()';
var
  I: Integer;
  Acao: TAcaoDeInstrucaoDeNotificacaoMobile;
  MyPushExtras: TMyPushExtras;
  //
  Codigo: Integer;
  Nome, TxtLocal, unixepoch_X_1000, QuandoIni, QuandoFim: String;
begin

{
      // iOS...
      if ServiceNotification.DataKey = 'aps' then
      begin
        if ServiceNotification.DataObject.Pairs[x].JsonString.Value = 'alert' then
          MessageText := ServiceNotification.DataObject.Pairs[x].JsonValue.Value;
      end;

      // Android...
      if ServiceNotification.DataKey = 'gcm' then
      begin
        if ServiceNotification.DataObject.Pairs[x].JsonString.Value = 'message' then
          MessageText := ServiceNotification.DataObject.Pairs[x].JsonValue.Value;
      end;
}
  //
  QuandoIni := '0000-00-00 00:00:00';
  QuandoFim := '0000-00-00 00:00:00';
  unixepoch_X_1000 := '';
  for I := 0 to Instrucoes.Extras.Count - 1 do
  begin
    if Instrucoes.Extras[I].Key = INSTRUCAO_PUSH_NOTIFICATION_EXTRA_001 then
      MyPushExtras[0] := Instrucoes.Extras[I].Value
    else
    if Instrucoes.Extras[I].Key = INSTRUCAO_PUSH_NOTIFICATION_EXTRA_002 then
      MyPushExtras[1] := Instrucoes.Extras[I].Value
    else
    if Instrucoes.Extras[I].Key = INSTRUCAO_PUSH_NOTIFICATION_EXTRA_003 then
      MyPushExtras[2] := Instrucoes.Extras[I].Value
    else
    if Instrucoes.Extras[I].Key = INSTRUCAO_PUSH_NOTIFICATION_EXTRA_004 then
      MyPushExtras[3] := Instrucoes.Extras[I].Value
    else
    if Instrucoes.Extras[I].Key = INSTRUCAO_PUSH_NOTIFICATION_EXTRA_005 then
      MyPushExtras[4] := Instrucoes.Extras[I].Value
    else
    ;
    if Instrucoes.Extras[I].Key = 'google.sent_time' then
    begin
      unixepoch_X_1000 := Instrucoes.Extras[I].Value;
    end;
    ///////////  Temporario ////////////////////////////////////////////////////
    if VAR_PushNotifications_Memo <> nil then
    begin
      VAR_PushNotifications_Memo.lines.Add(Instrucoes.Extras[I].Key + ' = ' + Instrucoes.Extras[I].Value);
    end;
  end;
  if MyPushExtras[0] = CO_TAB_ChmOcoCad then
  begin
    FmPrincipal.FPushNotificationsAction := TAcaoDeInstrucaoDeNotificacaoMobile.ainmChamadoOcorrencia;
    FmPrincipal.FPushNotificationsCodigo := Grl_Geral.IMV('0' + MyPushExtras[1]);
    FmPrincipal.FPushNotificationsNoLocl := MyPushExtras[2];
    //
    // J� inserir na tabela!
    Nome     := Instrucoes.Message;
    Codigo   := Grl_Geral.IMV('0' + MyPushExtras[1]);
    TxtLocal := MyPushExtras[2];
    InsAltPushRecebido(stIns, Codigo, Nome, TxtLocal, QuandoIni, QuandoFim, unixepoch_X_1000);
    Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, 'WHERE DtHrLido="0000-00-00 00:00:00" ');
    //
  end else

    // Evitar mostrar mensagem ao usuario?
    //Geral.MB_Erro('Tipo de notifica��o n�o implementado: ' + );

  //

end;

procedure TUnApp_PF.PushNotifications_ReceiveDestrincha2(
  Instrucoes: TArr1NivJSON);
var
  I: Integer;
  Codigo: Integer;
  Tabela, Nome, TxtLocal, DtHrPush, DtHrCatx, QuandoIni, QuandoFim: String;
  GST: Extended;
  Dta: TDateTime;
begin
(*
google.delivered_priority =>> normal'#012'
google.sent_time =>> 1578082534975'#012'
google.ttl =>> 2419200'#012'
google.original_priority =>> normal'#012'
id =>> 205807968282'#012'
from =>> 205807968282'#012'
google.message_id =>> 0:1578082534996093%eef72599f9fd7ecd'#012'
message =>> Suspendisse bibendum elementum enim maximus sagittis ante tristique nec. Suspendisse bibendum elemen'#012'
DmkXtra001 =>> chmococad'#012'
DmkXtra002 =>> 1'#012'
DmkXtra003 =>> AURORA BOREAL EXT IND. MONT. EQUIP. PAUL'#012'
DmkXtra004 =>> 2019-12-26 00:00:00'#012'
DmkXtra005 =>> 2020-02-22 00:00:00'
*)
  Codigo    := 0;
  Nome      := '';
  TxtLocal  := '';
  DtHrPush  := '0000-00-00 00:00:00';
  DtHrCatx  := Grl_Geral.FDT(Now(), 109);
  QuandoIni := '0000-00-00 00:00:00';
  QuandoFim := '0000-00-00 00:00:00';
  //
  Dta := 0;
  DtHrPush := '0000-00-00 00:00:00';
  for I := 0 to Length(Instrucoes) - 1 do
  begin
//google.delivered_priority =>> normal
    // Nada por enquanto!!!
//google.sent_time =>> 1578082534975
    if Lowercase(Instrucoes[I][0]) = 'google.sent_time' then
    begin
      GST := StrToFloat(Instrucoes[I][1]) / 1000;
      Dta := (GST / 86400) + 25569;
      DtHrPush := Grl_Geral.FDT(Dta, 109);
    end;
//google.ttl =>> 2419200
    // Nada por enquanto!!!
//google.original_priority =>> normal
    // Nada por enquanto!!!
//id =>> 205807968282
    // Nada por enquanto!!!
//from =>> 205807968282
    // Nada por enquanto!!!
//google.message_id =>> 0:1578082534996093%eef72599f9fd7ecd
    // Nada por enquanto!!!
//message =>> Suspendisse bibendum elementum enim maximus sagittis ante tristique nec. Suspendisse bibendum elemen
    if Lowercase(Instrucoes[I][0]) = Lowercase('message') then
      Nome := Instrucoes[I][1];
//DmkXtra001 =>> chmococad
    if Lowercase(Instrucoes[I][0]) = Lowercase(INSTRUCAO_PUSH_NOTIFICATION_EXTRA_001) then
      Tabela := Instrucoes[I][1];
//DmkXtra002 =>> 1
    if Lowercase(Instrucoes[I][0]) = Lowercase(INSTRUCAO_PUSH_NOTIFICATION_EXTRA_002) then
      Codigo := Grl_Geral.IMV(Instrucoes[I][1]);
//DmkXtra003 =>> AURORA BOREAL EXT IND. MONT. EQUIP. PAUL
    if Lowercase(Instrucoes[I][0]) = Lowercase(INSTRUCAO_PUSH_NOTIFICATION_EXTRA_003) then
      TxtLocal := Instrucoes[I][1];
//DmkXtra004 =>> 2019-12-26 00:00:00
    if Lowercase(Instrucoes[I][0]) = Lowercase(INSTRUCAO_PUSH_NOTIFICATION_EXTRA_004) then
      QuandoIni := Instrucoes[I][1];
//DmkXtra005 =>> 2020-02-22 00:00:00
    if Lowercase(Instrucoes[I][0]) = Lowercase(INSTRUCAO_PUSH_NOTIFICATION_EXTRA_005) then
      QuandoFim := Instrucoes[I][1];
  end;
  if Lowercase(Tabela) = Lowercase(CO_TAB_ChmOcoCad) then
  begin
    Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, 'WHERE Codigo=' + Grl_Geral.FF0(Codigo));
    if Dmod.QrChmOcoPux.RecordCount = 0 then
    begin
      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stIns, 'chmocopux', False, [
      'Nome', 'TxtLocal', //'DtHrLido',
      'DtHrPush', 'DtHrCatx', 'QuandoIni',
      'QuandoFim'], ['Codigo'], [
      Nome, TxtLocal, //DtHrLido,
      DtHrPush, DtHrCatx, QuandoIni,
      QuandoFim], [Codigo], True(*False*),
      TdmkSQLInsert.dmksqlinsInsIgnore, '', stMobile) then
        Dmod.ReopenChmOcoPux(Dmod.QrChmOcoPux, EmptyStr);
    end;
  end;
end;

procedure TUnApp_PF.PushNotifications_RegistraDevice();
  function InsereTabelaLocalETentaNaWeb(): Boolean;
  var
    sSQL: String;
    Nome, DeviceID, PushToken, DtaAcsIni: String;
    Codigo, CodInMob, MobDevCad: Integer;
    SQLType: TSQLType;
  begin
    Result         := False;
    //
    SQLType        := stIns;
    Codigo         := 0;
    CodInMob       := 0;
    Nome           := '';
    MobDevCad      := VAR_COD_DEVICE_IN_SERVER;
    DeviceID       := VAR_IMEI_DEVICE_IN_SERVER;
    PushToken      := VAR_PushNotifications_DeviceToken;
    DtaAcsIni      := Grl_Geral.FDT(Now(), 109);
    // verifica se j� n�o existe
    Grl_DmkDB.AbreSQLQuery0(Dmod.QrAux, Dmod.AllDB, [
    'SELECT PushToken ',
    'FROM chmdevacs ',
    'WHERE PushToken="' + VAR_PushNotifications_DeviceToken + '"',
    EmptyStr]);
    if Dmod.QrAux.RecordCount = 0 then
    begin
      // caso n�o exista, cria logs e altera cadastro no server
      CodInMob := Grl_DmkDB.GetNxtCodigoInt('chmdevacs', 'CodInMob', SQLType, CodInMob);
      //
      // Log do device no localhost
      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmdevacs', False, [
      'Nome', 'MobDevCad', 'DeviceID',
      'PushToken', 'DtaAcsIni', 'Codigo'], [
      'CodInMob'], [
      Nome, MobDevCad, DeviceID,
      PushToken, DtaAcsIni, Codigo], [
      CodInMob], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
      begin
        Result := True;
        VAR_DeviceToken_REGISTERED := True;
        //
        if FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
        begin
          // Obter codigo Web  (ID do log do device no server)
          Codigo  := FMX_DmkRemoteQuery.BPGS1I32W('chmdevacs' , 'Codigo', '', '',
            tsPos, SQLType, (*Codigo*)0);
          if Codigo > 0 then
          begin
            // Log do device no server
            if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'chmdevacs', False, [
            'Nome', 'MobDevCad', 'DeviceID',
            'PushToken', 'DtaAcsIni', 'CodInMob'], [
            'Codigo'], [
            Nome, MobDevCad, DeviceID,
            PushToken, DtaAcsIni, CodInMob], [
            Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
            False, sSQL) then
            begin
              if FMX_dmkRemoteQuery.SQL_Executa(sSQL) then
              begin
                // informa no log localhost o ID do log do device no server
                if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmdevacs', False, [
                'Codigo'], ['CodInMob'], [Codigo], [CodInMob], True,
                TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
                begin
                  // Atualiza token do cadastro do device no server
                  if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, stUpd, 'ovcmobdevcad', False, [
                  'PushNotifID'], ['Codigo'], [PushToken], [MobDevCad], True,
                  TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop, False,
                  sSQL) then
                    if FMX_dmkRemoteQuery.SQL_Executa(sSQL) then
                    begin
                      // nada
                    end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  //
begin
  if (VAR_PushNotifications_DeviceToken <> '')
  and (VAR_COD_DEVICE_IN_SERVER <> 0)
  and (VAR_DeviceToken_REGISTERED = False) then
  begin
    //
    InsereTabelaLocalETentaNaWeb();
    //...
    VAR_DeviceToken_REGISTERED := True;
  end;
end;

end.
