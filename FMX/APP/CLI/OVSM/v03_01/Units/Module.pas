unit Module;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FMX.Forms, FireDAC.FMXUI.Wait, FireDAC.Comp.UI,
  System.UITypes, FMX.StdCtrls, System.TypInfo,
  UnGrl_Vars, UnProjGroupEnums, FMX.Memo, FireDAC.Phys.SQLiteWrapper.Stat;

type
  TDmod = class(TDataModule)
    QrLoc: TFDQuery;
    QrControle: TFDQuery;
    QrUsuarios: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    QrAux: TFDQuery;
    QrUpd: TFDQuery;
    QrEspecificos: TFDQuery;
    AllDB: TFDConnection;
    MyDB: TFDConnection;
    QrOVSMOpcoes: TFDQuery;
    QrOVSMOpcoesERPNameByCli: TStringField;
    QrOVSMOpcoesCodigo: TIntegerField;
    QrOVSMOpcoesScaleX: TIntegerField;
    QrOVSMOpcoesScaleY: TIntegerField;
    QrOVSMOpcoesFoLocObjIns: TSmallintField;
    QrOVSMOpcoesEstiloForms: TStringField;
    QrOVSMOpcoesMailUser: TStringField;
    QrOVSMOpcoesDefStpCasasDecim: TSmallintField;
    QrOVSMOpcoesStepMedMin: TFloatField;
    QrOVSMOpcoesStepMedMax: TFloatField;
    QrOVSMOpcoesAmbiente: TSmallintField;
    QrOVSMOpcoesAllowNoMed: TSmallintField;
    QrOVSMOpcoesUsarHTTPS: TSmallintField;
    QrOVSMOpcoesUsarApiDev: TSmallintField;
    QrOVSMOpcoesSecConfeccao: TStringField;
    QrOVSMOpcoesSecTecelagem: TStringField;
    QrOVSMOpcoesSecTinturaria: TStringField;
    FDPragma: TFDQuery;
    QrOVcMobDevCad: TFDQuery;
    QrOVcMobDevCadCodigo: TIntegerField;
    QrOVcMobDevCadNome: TStringField;
    QrOVcMobDevCadDeviceID: TStringField;
    QrOVcMobDevCadUserNmePdr: TStringField;
    QrOVcMobDevCadDeviceName: TStringField;
    QrOVcMobDevCadDvcScreenH: TIntegerField;
    QrOVcMobDevCadDvcScreenW: TIntegerField;
    QrOVcMobDevCadOSName: TStringField;
    QrOVcMobDevCadOSNickName: TStringField;
    QrOVcMobDevCadOSVersion: TStringField;
    QrOVcMobDevCadDtaHabIni: TDateTimeField;
    QrOVcMobDevCadDtaHabFim: TDateTimeField;
    QrOVcMobDevCadAllowed: TShortintField;
    QrOVcMobDevCadLastSetAlw: TIntegerField;
    QrOVcMobDevCadPIN: TStringField;
    QrOVcMobDevCadScaleX: TIntegerField;
    QrOVcMobDevCadScaleY: TIntegerField;
    QrOVcMobDevCadFoLocObjIns: TShortintField;
    QrOVcMobDevCadLk: TIntegerField;
    QrOVcMobDevCadDataCad: TDateField;
    QrOVcMobDevCadDataAlt: TDateField;
    QrOVcMobDevCadUserCad: TIntegerField;
    QrOVcMobDevCadUserAlt: TIntegerField;
    QrOVcMobDevCadAlterWeb: TShortintField;
    QrOVcMobDevCadAtivo: TShortintField;
    QrOVcMobDevCadSccConfeccao: TShortintField;
    QrOVcMobDevCadSccTecelagem: TShortintField;
    QrOVcMobDevCadSccTinturaria: TShortintField;
    QrOVcMobDevCadChmOcorrencias: TSmallintField;
    QrChmOcoDon: TFDQuery;
    QrChmOcoDonCodigo: TIntegerField;
    QrChmOcoDonControle: TIntegerField;
    QrChmOcoDonChmOcoEtp: TIntegerField;
    QrChmOcoDonChmOcoWho: TIntegerField;
    QrChmOcoDonObservacao: TStringField;
    QrChmOcoDonDoneDtHr: TDateTimeField;
    QrChmOcoDonCloseUser: TIntegerField;
    QrChmOcoDonCloseDtHr: TDateTimeField;
    QrChmOcoDonMobUpWeb: TShortintField;
    QrChmOcoPux: TFDQuery;
    QrChmOcoPuxCodigo: TIntegerField;
    QrChmOcoPuxNome: TStringField;
    QrChmOcoPuxTxtLocal: TStringField;
    QrChmOcoPuxDtHrPush: TDateTimeField;
    QrChmOcoPuxDtHrLido: TDateTimeField;
    QrEpoch: TFDQuery;
    QrEpochDataHora: TWideStringField;
    QrChmOcoPuxDtHrCatx: TDateTimeField;
    QrChmOcoPuxQuandoIni: TDateTimeField;
    QrChmOcoPuxQuandoFim: TDateTimeField;
    QrChmOcoWho: TFDQuery;
    QrChmOcoWhoCodigo: TIntegerField;
    QrChmOcoWhoControle: TIntegerField;
    QrChmOcoWhoOrdem: TIntegerField;
    QrChmOcoWhoKndWhoPrtnc: TIntegerField;
    QrChmOcoWhoWhoDoEnti: TIntegerField;
    QrChmOcoWhoWhoDoMobile: TIntegerField;
    QrChmOcoWhoDoneDtHr: TDateTimeField;
    QrChmOcoWhoSntNotif: TDateTimeField;
    QrChmOcoWhoCfmRcbNotif: TDateTimeField;
    QrChmOcoWhoCfmDtHrLido: TDateTimeField;
    QrChmOcoWhoMobUpWeb: TShortintField;
    QrChmOcoWhoRealizado: TShortintField;
    QrChmOcoWhoObsEncerr: TStringField;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrOVSMOpcoesAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    IsMSWindows, FShowAni: Boolean;
    FStrInfoDone: String;
  public
    { Public declarations }
    procedure DefineCoresTema();
    function  GetNomeAtribValr_SeccaoInsp(SeccaoInsp: TSeccaoInsp): String;
    procedure InfoTempMemo(Memo: TMemo; Texto: String);
    procedure InformaLeituraNotificacao(Codigo: Integer; EstaLido: TDateTime);
    function  ReopenOVmIspMobCab(Qry: TFDQuery; StatusAndamentoInspecao:
              TStatusAndamentoInspecao; Local: Integer = 0): Boolean;
    function  ReopenOVmItxMobCab(Qry: TFDQuery; StatusAndamentoInspecao:
              TStatusAndamentoInspecao; SeccaoInsp: TSeccaoInsp;
              Local: Integer = 0): Boolean;
    function  ReopenOVSMOpcoes(Avisa: Boolean = True): Boolean;
    procedure ReopenOVcMobDevCad();
    procedure ReopenChmOcoCad(_QrChmOcoCab_: TFDQuery; SQL_AND: String);
    procedure ReopenChmOcoPux(Query: TFDQuery; SQL_WHERE: String);
    function  UploadChamadosFeitos(Memo: TMemo): Boolean;
    function  ObtemDataHoraDeEpochTime_X_1000(Epoch: String): String;
  end;

var
  Dmod: TDmod;

implementation

uses UnFMX_CfgDBApp, MyListas, UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums,
  UnFMX_DmkProcFunc, UnGrl_Consts, UnGrl_Geral, UnGrl_DmkDB, UnOVSM_Vars,
  UnFMX_DmkWeb, UnApp_Jan, UnFMX_DmkRemoteQuery, UnOVS_Consts,
  Principal;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDmod.ReopenChmOcoCad(_QrChmOcoCab_: TFDQuery; SQL_AND: String);
begin
  Grl_DmkDB.AbreSQLQuery0(_QrChmOcoCab_, Dmod.AllDB, [
  'SELECT cow.DoneDtHr,',
  // Erro quando oca.ChmHowCad = zero. Retorna null e n�o transforma em string!
  'CASE(oca.ChmHowCad) ',
  '  WHEN 0 THEN "" ',
  '  ELSE chc.Nome ',
  '  END NO_ChmHowCad, ',
  // Fim Erro null
  '/**sen.Login NO_UserLog,**/ ',
  '/**IF(wge.Tipo=0, wge.RazaoSocial, wge.Nome) NO_WhoGerEnti,**/   ',
  'CASE (wge.Tipo) ',
  '  WHEN 0 THEN wge.RazaoSocial ',
  '  WHEN 1 THEN wge.Nome ',
  '  ELSE "????" ',
  '  END NO_WhoGerEnti, ',
  ' ',
  '/**IF(oca.chmtitoco<>0, tit.Nome, oca.Nome) NO_Titulo,**/ ',
  'CASE KndOnde ',
  '  WHEN 1 THEN oca.OndeDescr ',
  '  /*WHEN 2 THEN scc.Nome*/ ',
  '  WHEN 3 THEN ovl.Nome ',
  '  ELSE "????" ',
  'END NO_ONDE, ',
  ' ',
  '/**IF(oca.WhoGerEnti<>0, tit.Nome, oca.Nome) NO_chmocowho**/ ',
  ' ',
  'strftime("%d/%m/%Y %H:%M:%S", datetime(oca.QuandoIni)) QuandoIni_TXT, ',
  'strftime("%d/%m/%Y %H:%M:%S", datetime(oca.QuandoFIm)) QuandoFim_TXT, ',
  'strftime("%d/%m/%Y %H:%M:%S", datetime(oca.QuandoIni)) || " a " || strftime("%d/%m/%Y %H:%M:%S", datetime(oca.QuandoFim)) AS Periodo_TXT, ',
  //'REPLACE(Descricao, CHAR(13)+CHAR(10), " ") AS DESCRI_NO_LINEBREAK, ',
  'REPLACE(Descricao,  ''' + sLineBreak + ''', " ") AS DESCRI_NO_LINEBREAK, ',
  'oca.* ',
  ' ',
  'FROM chmococad oca ',
  '/**LEFT JOIN chmtitoco tit ON tit.Codigo=oca.chmtitoco**/ ',
  'LEFT JOIN entidades    wge ON wge.Codigo=oca.WhoGerEnti ',
  '/*LEFT JOIN stqcencad  scc ON scc.Codigo=oca.CodOnde*/ ',
  'LEFT JOIN ovdlocal     ovl ON ovl.Codigo=oca.CodOnde ',
  'LEFT JOIN chmhowcad    chc ON chc.Codigo=oca.ChmHowCad ',
  '/**LEFT JOIN senhas    sen ON sen.Numero=oca.Usercad**/ ',
  ' ',
  'LEFT JOIN chmocowho cow ON cow.Codigo=oca.Codigo ',
  ' ',
  'WHERE (cow.WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
  'OR oca.KndIsWhoDo=' + Grl_Geral.FF0(CO_KndIsWhoDo_4_COD_Todos),
  'OR cow.ReopenDtHr > cow.DoneDtHr ',
  ') ',
  SQL_AND,
  EmptyStr]);
end;

procedure TDmod.ReopenChmOcoPux(Query: TFDQuery; SQL_WHERE: String);
var
  Texto: String;
begin
  Grl_DmkDB.AbreSQLQuery0(Query, Dmod.AllDB, [
  'SELECT * ',
  'FROM chmocopux ',
  //'WHERE DtHrLido<="1900-01-01" ',
  //'WHERE DtHrLido="0000-00-00 00:00:00" ',
  SQL_WHERE,
  EmptyStr]);
  if (SQL_WHERE = EmptyStr) then
  begin
    if(QrChmOcoPux.RecordCount) > 0 then
    begin
      if QrChmOcoPux.RecordCount > 99 then
        Texto := '99+'
      else
        Texto := Grl_Geral.FF0(QrChmOcoPux.RecordCount);
      //
      FmPrincipal.LaChmOcoCab.HitTest := True; // caso coloque false sem querer
      FmPrincipal.LaChmOcoCab.Text := Texto;
      FmPrincipal.LayChmOcoCad.Visible := True;
    end else
    begin
      FmPrincipal.LayChmOcoCad.Visible := False;
      //FmPrincipal.LaChmOcoCab.HitTest := True;
      FmPrincipal.LaChmOcoCab.Text := '0';
    end;
  end;
end;

procedure TDmod.ReopenOVcMobDevCad();
var
  HabConfeccao, HabTecelagem, HabTinturaria, HabChmOcorr: Boolean;
begin
  //
  //Result := False;
  try
    Grl_DmkDB.AbreSQLQuery0(QrOVcMobDevCad, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovcmobdevcad',
    'WHERE Codigo=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER) + '',
    EmptyStr]);
    //Result := True;
  except
    Grl_Geral.MB_Erro('N�o foi poss�vel abrir a tabela "ovcmobdevcad');
  end;
  HabConfeccao  := False;
  HabTecelagem  := False;
  HabTinturaria := False;
  HabChmOcorr   := False;
  if QrOVcMobDevCad.RecordCount > 0 then
  begin
    //HabConfeccao  := True;
    HabConfeccao  := QrOVcMobDevCadSccConfeccao.Value = 1;
    HabTecelagem  := QrOVcMobDevCadSccTecelagem.Value = 1;
    HabTinturaria := QrOVcMobDevCadSccTinturaria.Value = 1;
    //
    HabChmOcorr   := QrOVcMobDevCadChmOcorrencias.Value = 1
  end;

  //

  FmPrincipal.ImgDownloadAll.Enabled := HabConfeccao or HabTecelagem or HabTinturaria;
  //
  FmPrincipal.ImgConfeccao.Enabled := HabConfeccao;
  //FmPrincipal.ImgConfeccao.Enabled := True;
  FmPrincipal.ImgTecelagem.Enabled := HabTecelagem;
  FmPrincipal.ImgTinturaria.Enabled := HabTinturaria;
  //
  FmPrincipal.ImgChmOcoCab.Enabled := HabChmOcorr;
  //
end;

function TDmod.ReopenOVmIspMobCab(Qry: TFDQuery; StatusAndamentoInspecao:
              TStatusAndamentoInspecao; Local: Integer): Boolean;
const
  sProcFunc = 'Dmod.ReopenOVmIspMobCab()';
var
  SQL_WHERE, SQL_ORDER, SQL_Local: String;
begin
  Result := False;
  SQL_ORDER := '';
  SQL_Local := '';
  //
  case StatusAndamentoInspecao of
    (*1*)TStatusAndamentoInspecao.saiInspAberta:
      SQL_WHERE := Grl_Geral.ATS([
                  'WHERE DtHrFecha <= "1900-01-01" ',
                  'AND EnviadoWeb=0 ']);
    (*2*)TStatusAndamentoInspecao.saiInspRealizada,
    (*3*)TStatusAndamentoInspecao.saiEnvDadosInspSvr,
    (*4*)TStatusAndamentoInspecao.saiEnvEmailInsp,
    (*5*)TStatusAndamentoInspecao.saiEnvFotosInspSvr:
      SQL_WHERE := Grl_Geral.ATS([
                  'WHERE DtHrFecha > "1900-01-01" ',
                  'AND EnviadoWeb < ' + Grl_Geral.FF0(Integer(saiFinalizada))]);
    (*9*)TStatusAndamentoInspecao.saiFinalizada:
    begin
      SQL_ORDER := 'ORDER BY DtHrFecha DESC, CodInMob DESC';
      SQL_WHERE := Grl_Geral.ATS([
                  'WHERE DtHrFecha > "1900-01-01" ',
                  'AND EnviadoWeb = ' + Grl_Geral.FF0(Integer(saiFinalizada))]);
    end;
    (*0*)TStatusAndamentoInspecao.saiIndefinido:
    begin
      Grl_Geral.MB_Erro('Status "' + GetEnumName(TypeInfo(
      TStatusAndamentoInspecao), Integer(StatusAndamentoInspecao)) +
      ' n�o implementado em ' + sProcFunc);
      SQL_WHERE := '#### ERRO ####';
    end;
  end;
  if Local <> 0 then
    SQL_Local := 'AND igc.Local=' + Grl_Geral.FF0(Local);
  //
  Grl_DmkDB.AbreSQLQuery0(Qry, Dmod.AllDB, [
  'SELECT loc.Nome NO_Local, art.Nome NO_Artigo,  ',
  'opr.CodTam,  ',
  'igc.* ',
  'FROM ovmispmobcab igc ',
  'LEFT JOIN ovdlocal loc ON loc.Codigo=igc.Local ',
  'LEFT JOIN ovdreferencia art ON art.Codigo=igc.SeqGrupo ',
  'LEFT JOIN ovdproduto opr ON opr.Controle=igc.Produto ',
  SQL_WHERE,
  SQL_Local,
  SQL_ORDER,
  EmptyStr]);
  //
  Result := Qry.RecordCount > 0;
end;

function TDmod.ReopenOVmItxMobCab(Qry: TFDQuery;
  StatusAndamentoInspecao: TStatusAndamentoInspecao; SeccaoInsp: TSeccaoInsp;
  Local: Integer): Boolean;
const
  sProcFunc = 'Dmod.ReopenOVmItxMobCab()';
var
  SQL_WHERE, SQL_ORDER, SQL_Local, SQL_SeccaoInsp: String;
begin
  Result := False;
  SQL_ORDER := '';
  SQL_Local := '';
  //
  case StatusAndamentoInspecao of
    (*1*)TStatusAndamentoInspecao.saiInspAberta:
      SQL_WHERE := Grl_Geral.ATS([
                  'WHERE DtHrFecha <= "1900-01-01" ',
                  'AND EnviadoWeb=0 ']);
    (*2*)TStatusAndamentoInspecao.saiInspRealizada,
    (*3*)TStatusAndamentoInspecao.saiEnvDadosInspSvr,
    (*4*)TStatusAndamentoInspecao.saiEnvEmailInsp,
    (*5*)TStatusAndamentoInspecao.saiEnvFotosInspSvr:
      SQL_WHERE := Grl_Geral.ATS([
                  'WHERE DtHrFecha > "1900-01-01" ',
                  'AND EnviadoWeb < ' + Grl_Geral.FF0(Integer(saiFinalizada))]);
    (*9*)TStatusAndamentoInspecao.saiFinalizada:
    begin
      SQL_ORDER := 'ORDER BY DtHrFecha DESC, CodInMob DESC';
      SQL_WHERE := Grl_Geral.ATS([
                  'WHERE DtHrFecha > "1900-01-01" ',
                  'AND EnviadoWeb = ' + Grl_Geral.FF0(Integer(saiFinalizada))]);
    end;
    (*0*)TStatusAndamentoInspecao.saiIndefinido:
    begin
      Grl_Geral.MB_Erro('Status "' + GetEnumName(TypeInfo(
      TStatusAndamentoInspecao), Integer(StatusAndamentoInspecao)) +
      ' n�o implementado em ' + sProcFunc);
      SQL_WHERE := '#### ERRO ####';
    end;
  end;
  if Local <> 0 then
    SQL_Local := 'AND igc.Local=' + Grl_Geral.FF0(Local);
  //
  case SeccaoInsp of
    TSeccaoInsp.sccinspNone: SQL_SeccaoInsp := '';
    else SQL_SeccaoInsp := 'AND igc.SeccaoInsp=' + Grl_Geral.FF0(Integer(SeccaoInsp));
  end;
  //
  Grl_DmkDB.AbreSQLQuery0(Qry, Dmod.AllDB, [
  'SELECT loc.Nome NO_Local, art.Nome NO_Artigo,  ',
  'opr.CodTam,  ',
  'igc.* ',
  'FROM ovmitxmobcab igc ',
  'LEFT JOIN ovdlocal loc ON loc.Codigo=igc.Local ',
  'LEFT JOIN ovdreferencia art ON art.Codigo=igc.SeqGrupo ',
  'LEFT JOIN ovdproduto opr ON opr.Controle=igc.Produto ',
  SQL_WHERE,
  SQL_Local,
  SQL_SeccaoInsp,
  SQL_ORDER,
  EmptyStr]);
  //
  //Grl_Geral.MB_Info(Qry.SQL.Text);
  Result := Qry.RecordCount > 0;
end;

procedure TDmod.DataModuleCreate(Sender: TObject);
const
  Codigo = 1;
  ScaleX = 100;
  ScaleY = 100;
  FoLocObjIns = 0;
const
  DBUsu = nil;
var
  ERPNameByCli, EstiloForms, MailUser: String;
  StepMedMin, StepMedMax: Double;
  DefStpCasasDecim, Ambiente: Integer;
begin
  {$IfDef MSWWINDOWS}
    IsMSWindows := True;
  {$Else}
    IsMSWindows := False;
  {$EndIf}
  FShowAni := False;
  //
  if AllDB.Connected then
  begin
    Grl_Geral.MB_Aviso('"AllDB" conectado antes da configura��o!');
    AllDB.Connected := False;
  end;
  //
  FMX_Geral.DefineFormatacoes();
  VAR_LOCAL_DB_COMPO_DATASET  := QrAux;
  VAR_LOCAL_DB_COMPO_DATABASE := AllDB;
  //
  FMX_CfgDBApp.ConfiguraBD(AllDB, MyDB, QrLoc, QrUpd, QrUsuarios, QrControle,
    QrEspecificos, FmPrincipal.LaUsuario);
  //try
    //ReopenOVSMOpcoes();
  //except
    Grl_DmkDB.AbreSQLQuery0(QrAux, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovsmopcoes ',
    'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
    EmptyStr]);
    if QrAux.RecordCount = 0 then
    begin
      ERPNameByCli     := Application.Title;
      EstiloForms      := 'AquaGraphite';
      MailUser         := '';
      StepMedMin       := 0.1;
      StepMedMax       := 0.5;
      DefStpCasasDecim := 1;
      Ambiente         := 0;
      //
      Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stIns, 'ovsmopcoes', False, [
      'ScaleX', 'ScaleY', 'FoLocObjIns',
      'ERPNameByCli', 'EstiloForms', 'MailUser',
      'StepMedMin', 'StepMedMax', 'DefStpCasasDecim',
      'Ambiente'], [
      'Codigo'],[
      ScaleX, ScaleY, FoLocObjIns,
      ERPNameByCli, EstiloForms, MailUser,
      StepMedMin, StepMedMax, DefStpCasasDecim,
      Ambiente], [
      Codigo], True, TdmkSQLInsert.dmksqlinsInsOnly, '', stWeb);
      //
      QrAux.Close;
    end;
  //end;
  if QrOVSMOpcoes.State = dsInactive then
  begin
    if not ReopenOVSMOpcoes(False) then
    begin
      FMX_CfgDBApp.MostraVerifyDB(True, Dmod.AllDB, DBUsu);
      Grl_Geral.MB_Info('Feche o aplicativo e r�nicie novamente!');
    end;
  end;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  if AllDB.Connected then
    AllDB.Connected := False;
  if MyDB.Connected then
    MyDB.Connected := False;
end;

procedure TDmod.DefineCoresTema();
begin
  if (VAR_EstiloForms = '[Nenhum]') then
  begin
    //[Nenhum]
    VAR_DMK_THEMES_COLOR_Form   := $FFE6E6E6; //230, 230, 230));
    VAR_DMK_THEMES_COLOR_Panel  := $FFE1E1E1;
    VAR_DMK_THEMES_COLOR_Button := $FFD6D6D6;
    VAR_DMK_THEMES_COLOR_Edit   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_Text   := $FF000000;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF3E3E3E;
    VAR_DMK_THEMES_COLOR_DkLigh := $FFEDEDED;
    VAR_DMK_THEMES_COLOR_DisBak := $FFEFEFEF;
    VAR_DMK_THEMES_COLOR_DisTxt := $FF565656;
  end else
  if (VAR_EstiloForms = 'Air') then
  begin
    //Air
    VAR_DMK_THEMES_COLOR_Form   := $FF343434;
    VAR_DMK_THEMES_COLOR_Panel  := $FF484848;
    VAR_DMK_THEMES_COLOR_Button := $FF575757;
    VAR_DMK_THEMES_COLOR_Edit   := $FF191919;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF4B7A85;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF415C63;
    VAR_DMK_THEMES_COLOR_DisBak := $FF656565;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFB2B2B2;
  end else
  if (VAR_EstiloForms = 'Amakrits') then
  begin
    //Amakrits
    VAR_DMK_THEMES_COLOR_Form   := $FF272727;
    VAR_DMK_THEMES_COLOR_Panel  := $FF313131;
    VAR_DMK_THEMES_COLOR_Button := $FF303030;
    VAR_DMK_THEMES_COLOR_Edit   := $FF313131;
    VAR_DMK_THEMES_COLOR_Text   := $FF000000;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF66F8E3;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF418076;
    VAR_DMK_THEMES_COLOR_DisBak := $FF2C2C2C;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFAD8989;
  end else
  //if (VAR_EstiloForms := $FF'AquaGraphite')
  if (VAR_EstiloForms = 'AquaGraphite') then
  begin
    //AquaGraphite
    VAR_DMK_THEMES_COLOR_Form   := $FF252525;
    VAR_DMK_THEMES_COLOR_Panel  := $FF252525;
    VAR_DMK_THEMES_COLOR_Button := $FF006BBB;
    VAR_DMK_THEMES_COLOR_Edit   := $FF000000;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF009DFA;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF0065B1;
    VAR_DMK_THEMES_COLOR_DisBak := $FF252525;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFFFFFFF;
  end else
  if (VAR_EstiloForms = 'Blend') then
  begin
    //Blend
    VAR_DMK_THEMES_COLOR_Form   := $FF505050;
    VAR_DMK_THEMES_COLOR_Panel  := $FF404040;
    VAR_DMK_THEMES_COLOR_Button := $FF626262;
    VAR_DMK_THEMES_COLOR_Edit   := $FF333333;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF8C6C43;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF756147;
    VAR_DMK_THEMES_COLOR_DisBak := $FF3B3B3B;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFA4A4A4;
  end else
  if (VAR_EstiloForms = 'Dark') then
  begin
    //Dark
    VAR_DMK_THEMES_COLOR_Form   := $FF505050;
    VAR_DMK_THEMES_COLOR_Panel  := $FF404040;
    VAR_DMK_THEMES_COLOR_Button := $FF484848;
    VAR_DMK_THEMES_COLOR_Edit   := $FF404040;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FFF5AB28;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF926F42;
    VAR_DMK_THEMES_COLOR_DisBak := $FF3B3B3B;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFA3A3A3;
  end else
  if (VAR_EstiloForms = 'GoldenGraphite') then
  begin
    //GoldenGraphite
    VAR_DMK_THEMES_COLOR_Form   := $FF252525;
    VAR_DMK_THEMES_COLOR_Panel  := $FF252525;
    VAR_DMK_THEMES_COLOR_Button := $FFDF990C;
    VAR_DMK_THEMES_COLOR_Edit   := $FF000000;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FFFAA800;
    VAR_DMK_THEMES_COLOR_DkLigh := $FFB18100;
    VAR_DMK_THEMES_COLOR_DisBak := $FF0F0F0F;
    VAR_DMK_THEMES_COLOR_DisTxt := $FF9F9F9F;
  end else
  if (VAR_EstiloForms = 'Light') then
  begin
    //Light
    VAR_DMK_THEMES_COLOR_Form   := $FFF0F0F0;
    VAR_DMK_THEMES_COLOR_Panel  := $FFF0F0F0;
    VAR_DMK_THEMES_COLOR_Button := $FFE8E8E8;
    VAR_DMK_THEMES_COLOR_Edit   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_Text   := $FF898989;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF3DC9ED;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF8DDFF4;
    VAR_DMK_THEMES_COLOR_DisBak := $FFFBFBFB;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFB6B6B6;
  end else
  if (VAR_EstiloForms = 'RubyGraphite') then
  begin
    //RubyGraphite
    VAR_DMK_THEMES_COLOR_Form   := $FF252525;
    VAR_DMK_THEMES_COLOR_Panel  := $FF252525;
    VAR_DMK_THEMES_COLOR_Button := $FFC20C00;
    VAR_DMK_THEMES_COLOR_Edit   := $FF000000;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FFFA0300;
    VAR_DMK_THEMES_COLOR_DkLigh := $FFB10C00;
    VAR_DMK_THEMES_COLOR_DisBak := $FF0F0F0F;
    VAR_DMK_THEMES_COLOR_DisTxt := $FF9F9F9F;
  end else
  if (VAR_EstiloForms = 'Transparent') then
  begin
    //Transparent
    VAR_DMK_THEMES_COLOR_Form   := $FF03080A;
    VAR_DMK_THEMES_COLOR_Panel  := $FF051113;
    VAR_DMK_THEMES_COLOR_Button := $FF1E353F;
    VAR_DMK_THEMES_COLOR_Edit   := $FF21353D;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FF6BD2EF;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF327186;
    VAR_DMK_THEMES_COLOR_DisBak := $FF186530;
  end else
  //if (VAR_EstiloForms := 'AquaGraphite')
  begin
    //AquaGraphite
    VAR_DMK_THEMES_COLOR_Form   := $FF252525;
    VAR_DMK_THEMES_COLOR_Panel  := $FF252525;
    VAR_DMK_THEMES_COLOR_Button := $FF006BBB;
    VAR_DMK_THEMES_COLOR_Edit   := $FF000000;
    VAR_DMK_THEMES_COLOR_Text   := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_HiLigh := $FFFFFFFF;
    VAR_DMK_THEMES_COLOR_DkLigh := $FF00ABB1;
    VAR_DMK_THEMES_COLOR_DisBak := $FF252525;
    VAR_DMK_THEMES_COLOR_DisTxt := $FFFFFFFF;
  end;
  //

{
//[Nenhum]
  if (VAR_EstiloForms = '[Nenhum]') then
  begin
    VAR_DMK_THEMES_COLOR_Form   := $FFE6E6E6; //230, 230, 230));
    VAR_DMK_THEMES_COLOR_Panel  = 225, 225, 225
    VAR_DMK_THEMES_COLOR_Button = 214, 214, 214
    VAR_DMK_THEMES_COLOR_Edit   = 255, 255, 255
    VAR_DMK_THEMES_COLOR_Text   = 000, 000, 000
    VAR_DMK_THEMES_COLOR_HiLigh = 062, 062, 062
    VAR_DMK_THEMES_COLOR_DkLigh = 237, 237, 237
    VAR_DMK_THEMES_COLOR_DisBak = 239, 239, 239
    VAR_DMK_THEMES_COLOR_DisTxt = 086, 086, 086

    TAlphaColorRec(FC).R := VC and $FF;

  end else
  if (VAR_EstiloForms = 'Air')
  if (VAR_EstiloForms = 'Amakrits')
  //if (VAR_EstiloForms = 'AquaGraphite')
  if (VAR_EstiloForms = 'AquaGraphite')
  if (VAR_EstiloForms = 'Blend')
  if (VAR_EstiloForms = 'Dark')
  if (VAR_EstiloForms = 'GoldenGraphite')
  if (VAR_EstiloForms = 'Light')
  if (VAR_EstiloForms = 'RubyGraphite')
  if (VAR_EstiloForms = 'Transparent')
  //


  Air
  VAR_DMK_THEMES_COLOR_Form   = 052, 052, 052
  VAR_DMK_THEMES_COLOR_Panel  = 072, 072, 072
  VAR_DMK_THEMES_COLOR_Button = 087, 087, 087
  VAR_DMK_THEMES_COLOR_Edit   = 025, 025, 025
  VAR_DMK_THEMES_COLOR_Text   = 255, 255, 255
  VAR_DMK_THEMES_COLOR_HiLigh = 075, 122, 133
  VAR_DMK_THEMES_COLOR_DkLigh = 065, 092, 099
  VAR_DMK_THEMES_COLOR_DisBak = 041, 041, 041
  VAR_DMK_THEMES_COLOR_DisTxt = 178, 178, 178

  Amakrits
  Form   = 039, 039, 039
  Panel  = 049, 049, 049
  Button = 048, 048, 048
  Edit   = 049, 049, 049
  Text   = 000, 000, 000
  HiLigh = 102, 248, 227
  DkLigh = 065, 128, 118
  DisBak = 044, 044, 044
  DisTxt = 173, 137, 137

  AquaGraphite
  Form   = 037, 037, 037
  Panel  = 037, 037, 037
  Button = 000, 107, 187
  Edit   = 000, 000, 000
  Text   = 255, 255, 255
  HiLigh = 000, 157, 250
  DkLigh = 000, 101, 177
  DisBak = 037, 037, 037
  DisTxt = 255, 255, 255

  Blend
  Form   = 080, 080, 080
  Panel  = 064, 064, 064
  Button = 098, 098, 098
  Edit   = 051, 051, 051
  Text   = 255, 255, 255
  HiLigh = 140, 108, 067
  DkLigh = 117, 097, 071
  DisBak = 059, 059, 059
  DisTxt = 164, 164, 164

  Dark
  Form   = 080, ,
  Panel  = 064, ,
  Button = 072, ,
  Edit   = 064, ,
  Text   = 255, ,
  HiLigh = 245, 171, 040
  DkLigh = 146, 111, 066
  DisBak = 059, ,
  DisTxt = 163, ,

  GoldenGraphite
  Form   = 037, ,
  Panel  = 037, ,
  Button = 223, 153, 012
  Edit   = 000, ,
  Text   = 255, ,
  HiLigh = 250, 168, 000
  DkLigh = 177, 129, 000
  DisBak = 015, ,
  DisTxt = 159, ,

  Light
  Form   = 240, ,
  Panel  = 240, ,
  Button = 232, ,
  Edit   = 255, ,
  Text   = 137, ,
  HiLigh = 061, 201, 237
  DkLigh = 141, 223, 244
  DisBak = 251, ,
  DisTxt = 182, ,

  RubyGraphite
  Form   = 037, ,
  Panel  = 037, ,
  Button = 194, 012, 000
  Edit   = 000, ,
  Text   = 255, ,
  HiLigh = 250, 003, 000
  DkLigh = 177, 012, 000
  DisBak = 015, ,
  DisTxt = 159, ,

  Transparent
  Form   = 003, 008, 010
  Panel  = 005, 017, 019
  Button = 030, 053, 063
  Edit   = 033, 053, 061
  Text   = 255, 255, 255
  HiLigh = 107, 210, 239
  DkLigh = 050, 113, 134
  DisBak = 024, 041, 048
}











{
000 = 00
001 = 01
002 = 02
003 = 03
004 = 04
005 = 05
006 = 06
007 = 07
008 = 08
009 = 09
010 = 0A
011 = 0B
012 = 0C
013 = 0D
014 = 0E
015 = 0F
016 = 10
017 = 11
018 = 12
019 = 13
020 = 14
021 = 15
022 = 16
023 = 17
024 = 18
025 = 19
026 = 1A
027 = 1B
028 = 1C
029 = 1D
030 = 1E
031 = 1F
032 = 20
033 = 21
034 = 22
035 = 23
036 = 24
037 = 25
038 = 26
039 = 27
040 = 28
041 = 29
042 = 2A
043 = 2B
044 = 2C
045 = 2D
046 = 2E
047 = 2F
048 = 30
049 = 31
050 = 32
051 = 33
052 = 34
053 = 35
054 = 36
055 = 37
056 = 38
057 = 39
058 = 3A
059 = 3B
060 = 3C
061 = 3D
062 = 3E
063 = 3F
064 = 40
065 = 41
066 = 42
067 = 43
068 = 44
069 = 45
070 = 46
071 = 47
072 = 48
073 = 49
074 = 4A
075 = 4B
076 = 4C
077 = 4D
078 = 4E
079 = 4F
080 = 50
081 = 51
082 = 52
083 = 53
084 = 54
085 = 55
086 = 56
087 = 57
088 = 58
089 = 59
090 = 5A
091 = 5B
092 = 5C
093 = 5D
094 = 5E
095 = 5F
096 = 60
097 = 61
098 = 62
099 = 63
100 = 64
101 = 65
102 = 66
103 = 67
104 = 68
105 = 69
106 = 6A
107 = 6B
108 = 6C
109 = 6D
110 = 6E
111 = 6F
112 = 70
113 = 71
114 = 72
115 = 73
116 = 74
117 = 75
118 = 76
119 = 77
120 = 78
121 = 79
122 = 7A
123 = 7B
124 = 7C
125 = 7D
126 = 7E
127 = 7F
128 = 80
129 = 81
130 = 82
131 = 83
132 = 84
133 = 85
134 = 86
135 = 87
136 = 88
137 = 89
138 = 8A
139 = 8B
140 = 8C
141 = 8D
142 = 8E
143 = 8F
144 = 90
145 = 91
146 = 92
147 = 93
148 = 94
149 = 95
150 = 96
151 = 97
152 = 98
153 = 99
154 = 9A
155 = 9B
156 = 9C
157 = 9D
158 = 9E
159 = 9F
160 = A0
161 = A1
162 = A2
163 = A3
164 = A4
165 = A5
166 = A6
167 = A7
168 = A8
169 = A9
170 = AA
171 = AB
172 = AC
173 = AD
174 = AE
175 = AF
176 = B0
177 = B1
178 = B2
179 = B3
180 = B4
181 = B5
182 = B6
183 = B7
184 = B8
185 = B9
186 = BA
187 = BB
188 = BC
189 = BD
190 = BE
191 = BF
192 = C0
193 = C1
194 = C2
195 = C3
196 = C4
197 = C5
198 = C6
199 = C7
200 = C8
201 = C9
202 = CA
203 = CB
204 = CC
205 = CD
206 = CE
207 = CF
208 = D0
209 = D1
210 = D2
211 = D3
212 = D4
213 = D5
214 = D6
215 = D7
216 = D8
217 = D9
218 = DA
219 = DB
220 = DC
221 = DD
222 = DE
223 = DF
224 = E0
225 = E1
226 = E2
227 = E3
228 = E4
229 = E5
230 = E6
231 = E7
232 = E8
233 = E9
234 = EA
235 = EB
236 = EC
237 = ED
238 = EE
239 = EF
240 = F0
241 = F1
242 = F2
243 = F3
244 = F4
245 = F5
246 = F6
247 = F7
248 = F8
249 = F9
250 = FA
251 = FB
252 = FC
253 = FD
254 = FE
255 = FF
}

end;

function TDmod.GetNomeAtribValr_SeccaoInsp(SeccaoInsp: TSeccaoInsp): String;
const
  sProcName = 'TDmod.GetNomeAtribValr_SeccaoInsp()';
begin
  ReopenOVSMOpcoes();
  case SeccaoInsp of
    TSeccaoInsp.sccinspTecelagem  : Result := QrOVSMOpcoesSecTecelagem.Value;
    TSeccaoInsp.sccinspTinturaria : Result := QrOVSMOpcoesSecTinturaria.Value;
    TSeccaoInsp.sccinspConfeccao  : Result := QrOVSMOpcoesSecConfeccao.Value;
    else
    begin
      Grl_Geral.MB_Erro('SeccaoInsp n�o implementado em ' + sProcName);
      Result := '?SeccaoInsp?'
    end;
  end;
end;

procedure TDmod.InformaLeituraNotificacao(Codigo: Integer; EstaLido: TDateTime);
var
  Agora, FoiLido: TDateTime;
  DtHrLido, DtHrPush, CfmRcbNotif, CfmDtHrLido, SQLExec: String;
  Controle: Integer;
begin
  if EstaLido < 2 then
  begin
    Agora := Now();
    Grl_DmkDB.AbreSQLQuery0(QrAux, Dmod.AllDB, [
    'SELECT DtHrLido, DtHrPush ',
    'FROM chmocopux ',
    'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
    EmptyStr]);
    //
    FoiLido := QrAux.FieldByName('DtHrLido').AsDateTime;
    if FoiLido < 2 then
    begin
      DtHrLido := Grl_Geral.FDT(Agora, 109);
      DtHrPush := Grl_Geral.FDT(QrAux.FieldByName('DtHrPush').AsDateTime, 109);
      //
      if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocopux', False, [
      'DtHrLido'], [
      'Codigo'], [
      DtHrLido], [
      Codigo], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
      begin
        if not FMX_DmkRemoteQuery.ConexaoWebREST_MySQL() then
          Exit;
         //
        //chmocowho.Controle
        Controle := FMX_DmkRemoteQuery.OVUS_I(Grl_Geral.ATS([
          'SELECT Controle ',
          'FROM chmocowho ',
          'WHERE Codigo=' + Grl_Geral.FF0(Codigo),
          'AND WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
          EmptyStr]));
        if Controle > 0 then
        begin
          CfmRcbNotif := DtHrLido;
          CfmDtHrLido := DtHrPush;
          //
          SQLExec := Grl_Geral.ATS([
            'UPDATE chmocowho ',
            'SET CfmRcbNotif="' + CfmRcbNotif + '" ',
            'WHERE Controle=' + Grl_Geral.FF0(Controle),
            'AND CfmRcbNotif <= "1900-01-01" ',
            '; ',
            'UPDATE chmocowho ',
            'SET CfmDtHrLido="' + CfmDtHrLido + '" ',
            'WHERE Controle=' + Grl_Geral.FF0(Controle),
            'AND CfmDtHrLido <= "1900-01-01" ',
            '; ',
            EmptyStr]);
          if FMX_dmkRemoteQuery.SQL_Executa(SQLExec) then
              ;
          //
        end;
      end;
    end;
  end;
end;

procedure TDmod.InfoTempMemo(Memo: TMemo; Texto: String);
begin
  if Memo <> nil then
  begin
    if IsMSWindows or not FShowAni then
      Application.ProcessMessages;
    Memo.Text := Texto + '...' + sLineBreak + FStrInfoDone;
    if IsMSWindows or not FShowAni then
      Application.ProcessMessages;
  end;
end;

function TDmod.ObtemDataHoraDeEpochTime_X_1000(Epoch: String): String;
begin
  Result := '0000-00-00 00:00:00';
  if Grl_Geral.SoNumero_TT(Epoch) = Epoch then
  begin
    //
    try
      Grl_DmkDB.AbreSQLQuery0(QrEpoch, Dmod.AllDB, [
      'SELECT datetime(' + Epoch + '/1000, "unixepoch", "localtime") AS DataHora; ',
      EmptyStr]);
      //
      Result := QrEpochDataHora.Value;
    except
      // nada
      Result := '0000-00-00 00:00:00';
    end;
  end;
end;

procedure TDmod.QrOVSMOpcoesAfterOpen(DataSet: TDataSet);
  function DefineVAR_URL(UsaHTTPS, UsarApiDev: Integer): String;
  var
    TipoUrl: Integer;
  begin
    TipoUrl := (UsaHTTPS * 10) + (UsarApiDev);
    case TipoURL of
      00: Result := CO_URL_PRD;       // = 'http://dermatek.com.br/api/';
      01: Result := CO_URL_DEV;       // = 'http://dev.dermatek.com.br/api/';
      10: Result := CO_URL_SECUR_PRD; // = 'https://dermatek.com.br/api/';
      11: Result := CO_URL_SECUR_DEV; // = 'https://dev.dermatek.com.br/api/';
      else Result := 'URL???';
    end;
  end;
begin
  if QrOVSMOpcoes.RecordCount > 0 then
  begin
    VAR_URL := DefineVAR_URL(QrOVSMOpcoesUsarHTTPS.Value, QrOVSMOpcoesUsarApiDev.Value);
   //
    VAR_ERPNameByCli := QrOVSMOpcoesERPNameByCli.Value;
    if VAR_ERPNameByCli = EmptyStr then
      VAR_ERPNameByCli := Application.Title;
    VAR_EstiloForms := QrOVSMOpcoesEstiloForms.Value;
    //
    VAR_MEDIDAS_CASAS_DECIMAIS := QrOVSMOpcoesDefStpCasasDecim.Value;
    VAR_MEDIDAS_INCREMENTO_MIN := QrOVSMOpcoesStepMedMin.Value;
    VAR_MEDIDAS_INCREMENTO_MAX := QrOVSMOpcoesStepMedMax.Value;
    VAR_AMBIENTE_APP           := QrOVSMOpcoesAmbiente.Value;
    VAR_PERMITE_NAO_MEDIR      := QrOVSMOpcoesAllowNoMed.Value = 1;
  end;
  //if (VAR_EstiloForms = ''(*AquaGraphite*)) then VAR_DMK_COLOR_TEXT := TAlphaColors.RoyalBlue else
  if (VAR_EstiloForms = ''(*AquaGraphite*)) then VAR_DMK_COLOR_TEXT := TAlphaColors.SteelBlue else
  if (VAR_EstiloForms = '[Nenhum]')         then VAR_DMK_COLOR_TEXT := TAlphaColors.Black else
  if (VAR_EstiloForms = 'Air')              then VAR_DMK_COLOR_TEXT := TAlphaColors.Steelblue else
  if (VAR_EstiloForms = 'Amakrits')         then VAR_DMK_COLOR_TEXT := TAlphaColors.Skyblue  else
  //if (VAR_EstiloForms = 'AquaGraphite')     then VAR_DMK_COLOR_TEXT := TAlphaColors.RoyalBlue else
  if (VAR_EstiloForms = 'AquaGraphite')     then VAR_DMK_COLOR_TEXT := TAlphaColors.SteelBlue else
  if (VAR_EstiloForms = 'Blend')            then VAR_DMK_COLOR_TEXT := TAlphaColors.Wheat  else
  if (VAR_EstiloForms = 'Dark')             then VAR_DMK_COLOR_TEXT := TAlphaColors.Darkorange else
  if (VAR_EstiloForms = 'GoldenGraphite')   then VAR_DMK_COLOR_TEXT := TAlphaColors.Orange else
  if (VAR_EstiloForms = 'Light')            then VAR_DMK_COLOR_TEXT := TAlphaColors.Dodgerblue else
  if (VAR_EstiloForms = 'RubyGraphite')     then VAR_DMK_COLOR_TEXT := TAlphaColors.Orangered(*Red?*) else
  if (VAR_EstiloForms = 'Transparent')      then VAR_DMK_COLOR_TEXT := TAlphaColors.Skyblue else
                                                 VAR_DMK_COLOR_TEXT := TAlphaColors.Grey;
  //
  DefineCoresTema();
end;

function TDmod.ReopenOVSMOpcoes(Avisa: Boolean): Boolean;
begin
  Result := False;
  try
    Grl_DmkDB.AbreSQLQuery0(QrOVSMOpcoes, Dmod.AllDB, [
    'SELECT * ',
    'FROM ovsmopcoes ',
    'WHERE Codigo=1',
    EmptyStr]);
    Result := True;
  except
    if Avisa then
      Grl_Geral.MB_Erro('N�o foi poss�vel abrir a tabela "ovsmopcoes');
  end;
end;

function TDmod.UploadChamadosFeitos(Memo: TMemo): Boolean;
var
  wSQL: String;
var
  DoneDtHr, Observacao, ObsEncerr: String;
  Controle, MobUpWeb, Realizado: Integer;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  FStrInfoDone := EmptyStr;
  Result := False;
  try
////////////////////////////////////////////////////////////////////////////////
/// Itens de chamados
////////////////////////////////////////////////////////////////////////////////
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoDon, Dmod.AllDB, [
    'SELECT don.* ',
    'FROM chmocodon don ',
    'LEFT JOIN chmocowho who ON who.Controle=don.ChmOcoWho ',
    'WHERE don.DoneDtHr > "1900-01-01" ',
    'AND don.MobUpWeb=0 ',
    'AND who.WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
    EMptyStr]);
    //
    if QrChmOcoDon.RecordCount > 0 then
    begin
      while not QrChmOcoDon.Eof do
      begin
        InfoTempMemo(Memo, 'Atualizando item no servidor');
        //
        Controle       := QrChmOcoDonControle.Value;
        DoneDtHr       := Grl_Geral.FDT(QrChmOcoDonDoneDtHr.Value, 109);
        Observacao     := QrChmOcoDonObservacao.Value;
        MobUpWeb       := 1; // Sim
        if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'chmocodon', False, [
        'DoneDtHr', 'Observacao', 'MobUpWeb'], [
        'Controle'], [
        DoneDtHr, Observacao, MobUpWeb], [
        Controle], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
        False, wSQL) then
        begin
          if FMX_dmkRemoteQuery.SQL_Executa(wSQL) then
          begin
            if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocodon', False, [
            'MobUpWeb'], ['Controle'], [
            MobUpWeb], [Controle], True(*False*),
            TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
          end;
        end;
        //
        QrChmOcoDon.Next;
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
/// Chamados (cabe�alhos)
////////////////////////////////////////////////////////////////////////////////
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoWho, Dmod.AllDB, [
    'SELECT who.* ',
    'FROM chmocowho who ',
    'WHERE who.MobUpWeb=0 ',
    'AND who.WhoDoMobile=' + Grl_Geral.FF0(VAR_COD_DEVICE_IN_SERVER),
    EMptyStr]);
    //
    if QrChmOcoWho.RecordCount > 0 then
    begin
      while not QrChmOcoWho.Eof do
      begin
        InfoTempMemo(Memo, 'Atualizando item no servidor');
        //
        Controle       := QrChmOcoWhoControle.Value;
        DoneDtHr       := Grl_Geral.FDT(QrChmOcoWhoDoneDtHr.Value, 109);
        ObsEncerr      := QrChmOcoWhoObsEncerr.Value;
        Realizado      := QrChmOcoWhoRealizado.Value;
        MobUpWeb       := 1; // Sim
        if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'chmocowho', False, [
        'DoneDtHr', 'MobUpWeb',
        'ObsEncerr', 'Realizado'], [
        'Controle'], [
        DoneDtHr, MobUpWeb,
        ObsEncerr, Realizado], [
        Controle], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
        False, wSQL) then
        begin
          if FMX_dmkRemoteQuery.SQL_Executa(wSQL) then
          begin
            if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, SQLType, 'chmocowho', False, [
            'MobUpWeb'], ['Controle'], [
            MobUpWeb], [Controle], True(*False*),
            TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
          end;
        end;
        //
        QrChmOcoWho.Next;
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////
    Result := True;
  except
    //if Avisa then
      Grl_Geral.MB_Erro('N�o foi poss�vel subir todos dados!');
  end;
end;

{
function TDmod.UploadChamadosFeitos(Memo: TMemo): Boolean;
var
  wSQL: String;
  DoneDtHr, CloseDtHr, Observacao: String;
  Codigo, CtrlInMob, Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
  SQLType: TSQLType;
  Continua: Boolean;
begin
  FStrInfoDone := EmptyStr;
  Result := False;
  try
    Grl_DmkDB.AbreSQLQuery0(QrAux, Dmod.AllDB, [
    'SELECT * ',
    'FROM chmocodon ',
    'WHERE Controle=0 ',
    EmptyStr]);
    Grl_DmkDB.AbreSQLQuery0(QrChmOcoDon, Dmod.AllDB, [
    'SELECT * ',
    'FROM chmocodon ',
    'WHERE Controle=0 ',
    EmptyStr]);
    if QrAux.Fields.Count > QrChmOcoDon.Fields.Count then
    begin
      Grl_Geral.MB_Erro('"QrChmOcoDon.Fields.Count" difere do esperado!' +
      sLineBreak + 'Avise a dermatek');
      //Exit;
    end;
    if QrChmOcoDon.RecordCount > 0 then
    begin
      while not QrChmOcoDon.Eof do
      begin
        Continua := False;
        InfoTempMemo(Memo, 'Obtendo o ID sequencial de encerramento de item no servidor');
        wSQL := Grl_Geral.ATS([
        'SELECT Controle ',
        'FROM chmocodon ',
        'WHERE Codigo=' + Grl_Geral.FF0(QrChmOcoDonCodigo.Value),
        'AND CtrlInMob=' + Grl_Geral.FF0(QrChmOcoDonCtrlInMob.Value),
        EmptyStr]);
        //Grl_Geral.MB_Info(wSQL);
        //
        Controle := FMX_DmkRemoteQuery.OVUS_I([wSQL]);
        //
        if Controle = 0 then
        begin
          SQLType := stIns;
           Controle := FMX_DmkRemoteQuery.BPGS1I32W('chmocodon' , 'Controle', '', '',
             tsPos, SQLType, (*Codigo*)0);
          if Controle = 0 then
            Controle := 1;
          //
          InfoTempMemo(Memo, 'Criando o registro no servidor');
          Codigo         := QrChmOcoDonCodigo.Value;
          CtrlInMob      := QrChmOcoDonCtrlInMob.Value;
          //Controle       := QrChmOcoDonControle.Value;
          ChmOcoEtp      := QrChmOcoDonChmOcoEtp.Value;
          ChmOcoWho      := QrChmOcoDonChmOcoWho.Value;
          DoneDtHr       := Grl_Geral.FDT(QrChmOcoDonDoneDtHr.Value, 109);
          CloseUser      := QrChmOcoDonCloseUser.Value;
          CloseDtHr      := Grl_Geral.FDT(QrChmOcoDonCloseDtHr.Value, 109);
          Observacao     := QrChmOcoDonObservacao.Value;
          //
          if Grl_DmkDB.TextoSQLInsUpd(TDBType.dbtMySQL, SQLType, 'chmocodon', False, [
          'Codigo', 'CtrlInMob', 'ChmOcoEtp',
          'ChmOcoWho', 'DoneDtHr', 'CloseUser',
          'CloseDtHr', 'Observacao'], [
          'Controle'], [
          Codigo, CtrlInMob, ChmOcoEtp,
          ChmOcoWho, DoneDtHr, CloseUser,
          CloseDtHr, Observacao], [
          Controle], True, TdmkSQLInsert.dmksqlinsInsOnly, '', TDeviceType.stDesktop,
          False, wSQL) then
          begin
            Continua := FMX_dmkRemoteQuery.SQL_Executa(wSQL);
          end;
        end else
        begin
          CtrlInMob := QrChmOcoDonCtrlInMob.Value;
          Continua  := True;
        end;
        if Continua then
        begin
          if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.AllDB, stUpd, 'chmocodon', False, [
          'Controle'], [
          'CtrlInMob'], [
          Controle], [
          CtrlInMob], True(*False*), TdmkSQLInsert.dmksqlinsInsOnly, '', stMobile) then
            ;
        end;
        //
        QrChmOcoDon.Next;
      end;
    end else
      Grl_Geral.MB_Info('N�o h� chamados de ocorr�ncias para subir!');
    //
    // ...
    //
    Result := True;
  except
    //if Avisa then
      Grl_Geral.MB_Erro('N�o foi poss�vel subir todos dados!');
  end;
end;
}

end.
