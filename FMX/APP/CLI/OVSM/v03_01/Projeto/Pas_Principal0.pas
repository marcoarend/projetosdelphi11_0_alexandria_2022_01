unit Pas_Principal0;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Gestures,
  UnFMX_Geral, FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls,
  UnTemp, FMX.Layouts, FMX.Edit, Datasnap.DBClient, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.DialogService,
  FMX.Styles, FMX.Consts,
  UnProjGroupEnums, IPPeerClient, REST.Backend.PushTypes, System.JSON,
  REST.Backend.EMSPushDevice, System.PushNotification, REST.Backend.EMSProvider,
  Data.Bind.Components, Data.Bind.ObjectScope, REST.Backend.BindSource,
  REST.Backend.PushDevice,
  // temp
  FMX.Platform, FMX.VirtualKeyboard,
  // fim Temp
  UnAPP_PF,
  UnDmkEnums, FMX.Media, UnFMX_Grl_Vars;

type
  TFmPrincipal0 = class(TForm)
    LayShow: TPanel;
    GridPanelLayout1: TGridPanelLayout;
    PnStart: TPanel;
    ImgShow: TImage;
    Image7: TImage;
    ImgTinturaria: TImage;
    ImgConfeccaoSnt: TImage;
    ImgTecelagemSnt: TImage;
    ImgTinturariaSnt: TImage;
    ImgUploadFaccao: TImage;
    ImgDownloadAll: TImage;
    ImgUploadTextil: TImage;
    ImgVerifiDB: TImage;
    ImgOpcoes: TImage;
    PushEvents1: TPushEvents;
    EMSProvider1: TEMSProvider;
    Panel7: TPanel;
    MePushNotifications: TMemo;
    Panel8: TPanel;
    Button1: TButton;
    LayNotif: TLayout;
    LayImgChmOcoCab: TLayout;
    ImgChmOcoCab: TImage;
    LayChmOcoCad: TLayout;
    CirChmOcoCab_: TCircle;
    LaChmOcoCab: TLabel;
    Button2: TButton;
    procedure ImgDownloadAllClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    //
  public
    { Public declarations }
  end;

var
  FmPrincipal0: TFmPrincipal0;
  //

implementation

uses UnFMX_CfgDBApp, Module, UnGrl_DmkDB, MyListas, UnFMX_DmkRemoteQuery,
  // Teste
  ChmOcoPux,
  // fim teste
  UnFMX_DmkForms, UnApp_Jan, UnGrl_Vars, UnFMX_DmkProcFunc,
  UnFMX_PushNotifications, UnOVSM_PF,
  LoginApp, Upload,
  UnMyLinguas, UnGrl_OS, UnOVSM_Vars, UnGrl_Geral,
  UnREST_App, UnGrl_AllOS;

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.iPhone55in.fmx IOS}
{$R *.Windows.fmx MSWINDOWS}
{$R *.Surface.fmx MSWINDOWS}

procedure TFmPrincipal0.Button1Click(Sender: TObject);
begin
  FMX_DmkForms.CriaFm_AllOS0(TFmChmOcoPux, FmChmOcoPux,
  fcmCreateTryShwM, True, False);
end;

{ KeyBoard
var
  KeyBoard: IFMXVirtualKeyboardService;
begin
 KeyBoard := TPlatformServices.Current.GetPlatformService(IFMXVirtualKeyboardService) as IFMXVirtualKeyboardService;
 KeyBoard.ShowVirtualKeyboard(MePushNotifications);
end;
}

{ Push Teste 99 msg)
var
  Instrucoes: TArr1NivJSON;
begin
  SetLength(Instrucoes, 13);
  Instrucoes[0][0] := 'google.delivered_priority';
  Instrucoes[0][1] := 'normal';

  Instrucoes[01][0] := 'google.sent_time';
  Instrucoes[01][1] := '1578082534975';

  Instrucoes[02][0] := 'google.ttl';
  Instrucoes[02][1] := '2419200';

  Instrucoes[03][0] := 'google.original_priority';
  Instrucoes[03][1] := 'normal';

  Instrucoes[04][0] := 'id';
  Instrucoes[04][1] := '205807968282';

  Instrucoes[05][0] := 'from';
  Instrucoes[05][1] := '205807968282';

  Instrucoes[06][0] := 'google.message_id';
  Instrucoes[06][1] := '0:1578082534996093%eef72599f9fd7ecd';

  Instrucoes[07][0] := 'message';
  Instrucoes[07][1] := 'Suspendisse bibendum elementum enim maximus sagittis ante tristique nec. Suspendisse bibendum elemen';

  Instrucoes[08][0] := 'DmkXtra001';
  Instrucoes[08][1] := 'chmococad';

  Instrucoes[09][0] := 'DmkXtra002';
  Instrucoes[09][1] := '1';

  Instrucoes[10][0] := 'DmkXtra003';
  Instrucoes[10][1] := 'AURORA BOREAL EXT IND. MONT. EQUIP. PAUL';

  Instrucoes[11][0] := 'DmkXtra004';
  Instrucoes[11][1] := '2019-12-26 00:00:00';

  Instrucoes[12][0] := 'DmkXtra005';
  Instrucoes[12][1] := '2020-02-22 00:00:00';

  App_PF.PushNotifications_ReceiveDestrincha2(Instrucoes);

    FmPrincipal.LaChmOcoCab.HitTest := True; // caso coloque false sem querer
    FmPrincipal.LaChmOcoCab.Text := '99';//Grl_Geral.FF0(QrChmOcoPux.RecordCount);
    FmPrincipal.LayChmOcoCad.Visible := True;
end;
}



//Fazer observa��o na inspe��o ao finalizar!

// Floating Action Button no Delphi / Firemonkey
// https://www.youtube.com/watch?v=R-HHRZnPrQE

// Envio de mensagem whatsapp
//https://www.landersongomes.com.br/embarcadero/delphi/integrando-aplicativo-delphi-xe7-android-com-whatsapp

end.