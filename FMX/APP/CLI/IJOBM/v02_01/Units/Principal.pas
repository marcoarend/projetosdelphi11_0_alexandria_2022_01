unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.MultiView, FMX.Layouts,
  FMX.ListBox, FMX.Objects, System.IOUtils, FMX.ScrollBox, FMX.Memo, FMX.Edit;

type
  TFmPrincipal = class(TForm)
    Label2: TLabel;
    LayoutContent: TLayout;
    Image1: TImage;
    ToolBar2: TToolBar;
    SBOcultar: TSpeedButton;
    MultiView1: TMultiView;
    Layout1: TLayout;
    SBSincro: TSpeedButton;
    SBVerifyDB: TSpeedButton;
    SBSobre: TSpeedButton;
    ScrollBox1: TScrollBox;
    SBLctGer: TSpeedButton;
    SBContas: TSpeedButton;
    SBCarteiras: TSpeedButton;
    SBQrCode: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Layout2: TLayout;
    ImgUsuario: TImage;
    SBConta: TSpeedButton;
    LaUsuario: TLabel;
    Button1: TButton;
    Edit1: TEdit;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure SBContaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBVerifyDBClick(Sender: TObject);
    procedure SBSobreClick(Sender: TObject);
    procedure SBSincroClick(Sender: TObject);
    procedure MultiView1Click(Sender: TObject);
    procedure SBLctGerClick(Sender: TObject);
    procedure SBContasClick(Sender: TObject);
    procedure SBCarteirasClick(Sender: TObject);
    procedure SBQrCodeClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraSincro();
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses Module, UnFMX_DmkProcFunc, DmkUsers, Sobre, UnDmkEnums, MyListas,
  UnFMX_FinanceiroJan, UnFMX_DmkSincro, UnFMX_CfgDBApp, DmkLogin, UnFMX_Geral,
  UnFMX_dmkWeb, MyGlyfs, UnFMX_DmkForms, UnGrl_REST;

{$R *.fmx}

procedure TFmPrincipal.Button1Click(Sender: TObject);
var
  ResCod: Integer;
  Token: String;
  Res: String;
begin
  Token := Edit1.Text;
  //
  if Token <> '' then
  begin
    ResCod := Grl_REST.Rest_Dermatek_Post('token', 'token_gerar_chaveapi', Token,
                trtXML, [], Res);
    //
    if ResCod = 200 then
      Memo1.Text := Res;
  end;
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
var
  Token: String;
begin
  Token := Grl_REST.OAuth2_Dermatek_EfetuaLogin(Self);
  //
  Edit1.Text := Token;
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
var
  ResCod: Integer;
  Token, Res: String;
  ParamTabela, ParamDataHoraUtc: THttpParam;
begin
  Token := 'skcokgc404owsk8w0ossocw0sows8g48';
  //
  ParamTabela.Nome  := 'tabela';
  ParamTabela.Valor := 'wlanctos';
  //
  ParamDataHoraUtc.Nome  := 'datahorautc';
  ParamDataHoraUtc.Valor := '2001-01-01 00:00:00';
  //
  ResCod := Grl_REST.Rest_Dermatek_Post('sincro', 'download', Token,
            trtXML, [ParamTabela, ParamDataHoraUtc], Res);
  //
  if ResCod <> 0 then
    Memo1.Text := Res;
end;

procedure TFmPrincipal.Button4Click(Sender: TObject);
var
  aUTF8Str, Res: RawByteString;
  aUnicodeStr: UnicodeString;
begin
  aUTF8Str := UTF8Encode('Instalação de aplicativos Desktop');
  SetCodePage(aUTF8Str, 0, False);
  aUnicodeStr := UnicodeString(aUTF8Str);
  Res := aUnicodeStr;
  //
  ShowMessage(Res);
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  FmMyGlyfs.CarregaImagemUsuario(ImgUsuario);
  //
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  MultiView1.MasterButton := SBOcultar;
  SBOcultar.Visible       := True;
  {$ELSE}
  MultiView1.MasterButton := nil;
  SBOcultar.Visible       := False;
  {$ENDIF}
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  FMX_CfgDBApp.ConfiguraBD(Dmod.AllDB, Dmod.MyDB, Dmod.QrLoc, Dmod.QrUpd,
    Dmod.QrUsuarios, Dmod.QrControle, Dmod.QrEspecificos, LaUsuario);
end;

procedure TFmPrincipal.MostraSincro;
begin
  FMX_CfgDBApp.MostraSincro(False);
end;

procedure TFmPrincipal.MultiView1Click(Sender: TObject);
begin
  MultiView1.HideMaster;
end;

procedure TFmPrincipal.SBContaClick(Sender: TObject);
begin
  FMX_dmkForms.CriaFm_AllOS(TFmDmkUsers, FmDmkUsers, True);
end;

procedure TFmPrincipal.SBSincroClick(Sender: TObject);
begin
  MostraSincro;
end;

procedure TFmPrincipal.SBSobreClick(Sender: TObject);
begin
  FMX_dmkForms.CriaFm_AllOS(TFmSobre, FmSobre);
end;

procedure TFmPrincipal.SBVerifyDBClick(Sender: TObject);
begin
  FMX_CfgDBApp.MostraVerifyDB(False, Dmod.AllDB, Dmod.MyDB);
end;

procedure TFmPrincipal.SpeedButton1Click(Sender: TObject);
begin
  MostraSincro;
end;

procedure TFmPrincipal.SpeedButton2Click(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraLctEdit(nil, nil, 0, 0, '', Date, Date);
end;

procedure TFmPrincipal.SBCarteirasClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraCarteiras;
end;

procedure TFmPrincipal.SBContasClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraContas;
end;

procedure TFmPrincipal.SBLctGerClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraLctGer();
end;

procedure TFmPrincipal.SBQrCodeClick(Sender: TObject);
begin
  FMX_FinanceiroJan.MostraLctImpQrCode();
end;

end.
