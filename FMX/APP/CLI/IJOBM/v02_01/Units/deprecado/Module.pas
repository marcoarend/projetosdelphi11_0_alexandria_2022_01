unit Module;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FMX.Forms, FireDAC.FMXUI.Wait, FireDAC.Comp.UI,
  System.UITypes, FMX.StdCtrls;

type
  TDmod = class(TDataModule)
    MyDB: TFDConnection;
    QrLoc: TFDQuery;
    QrControle: TFDQuery;
    QrUsuarios: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    QrAux: TFDQuery;
    QrUpd: TFDQuery;
    QrEspecificos: TFDQuery;
    AllDB: TFDConnection;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraVerifyDB(Verifica: Boolean);
    procedure MostraSincro(Tabelas, SQLIndex: TStringList);
    procedure ConfiguraDispositivo(LaUsuario: TLabel);
    procedure ReopenOpcoesEspecificas();
    procedure ReopenControle();
    procedure ReopenUsuarios();
    procedure ConfiguraBD(LaUsuario: TLabel);
  end;

var
  Dmod: TDmod;

implementation

uses UnFMX_DmkDB, MyListas, VerifyDB, UnFMX_Geral, UnFMX_Grl_Vars, UnFMX_DmkProcFunc,
  DmkLogin, UnDmkEnums, SincroDB, UnGrl_Consts;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDmod.ConfiguraBD(LaUsuario: TLabel);
var
  Versao: Integer;
begin
  FMX_dmkDB.ConfiguraDB(MyDB, istSQLite, CO_DBNome, CO_RandStrWeb01);

  Versao := FMX_dmkDB.ObtemVersaoAppDB(QrLoc, MyDB, istSQLite);

  if Versao < CO_VERSAO then
  begin
    MostraVerifyDB(False);
  end else
  begin
    ConfiguraDispositivo(LaUsuario);
  end;
end;

procedure TDmod.ConfiguraDispositivo(LaUsuario: TLabel);
var
  Res: Boolean;
begin
  ReopenUsuarios;
  ReopenControle;
  ReopenOpcoesEspecificas;
  //
  FMX_Geral.DefineFormatacoes;
  //
  if (QrControle.RecordCount > 0) and (QrUsuarios.RecordCount > 0) then
    Res := True
  else
    Res := False;
  //
  if not Res then
  begin
    VAR_WEB_USER_ID   := 0;
    VAR_WEB_TOKEN_DMK := '';
    //
    if LaUsuario <> nil then
      LaUsuario.Text := '';
    //
    FMX_dmkPF.CriaFm_AllOS(TFmDmkLogin, FmDmkLogin, True);
  end else
  begin
    if LaUsuario <> nil then
      LaUsuario.Text := VAR_WEB_NOMEUSR;
  end;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  if MyDB.Connected then
    MyDB.Connected := False;
end;

procedure TDmod.MostraSincro(Tabelas, SQLIndex: TStringList);
begin
  if FMX_dmkPF.CriaFm_AllOS2(TFmSincroDB, FmSincroDB) = True then
  begin
    {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    FmSincroDB.Show;
    {$ELSE}
    FmSincroDB.ShowModal;
    FmSincroDB.Destroy;
    {$ENDIF}
  end;
end;

procedure TDmod.MostraVerifyDB(Verifica: Boolean);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FullScreen := True;
  FmVerifyDB.Show;
  {$ELSE}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FullScreen := False;
  FmVerifyDB.ShowModal;
  FmVerifyDB.Destroy;
  {$ENDIF}
end;

procedure TDmod.ReopenControle;
begin
  FMX_dmkDB.AbreSQLQuery0(QrControle, MyDB, [
    'SELECT Device, Versao ',
    'FROM controle ',
    '']);
  if QrControle.RecordCount > 0 then
    VAR_WEB_DEVICE_ID   := QrControle.FieldByName('Device').AsInteger;
end;

procedure TDmod.ReopenOpcoesEspecificas;
begin
  (* N�o usa habitlitar quando precisar
  FMX_dmkDB.AbreSQLQuery0(QrEspecificos, MyDB, [
    'SELECT * ',
    'FROM opcijobm ',
    '']);
  if QrEspecificos.RecordCount = 0 then
  begin
    FMX_dmkDB.CarregaSQLInsUpd(QrUpd, MyDB, stIns, 'opcijobm', False,
      [], ['Codigo'], [], [1], True, False, '', stMobile, False);
  end;
  *)
end;

procedure TDmod.ReopenUsuarios;
begin
  FMX_dmkDB.AbreSQLQuery0(QrUsuarios, MyDB, [
    'SELECT Codigo, PersonalName, UserName, Tipo, UsrEnt, UsrID, UsrNum, Token ',
    'FROM Usuarios ',
    'WHERE Atual = 1 ',
    '']);
  if QrUsuarios.RecordCount > 0 then
  begin
    VAR_USUARIO         := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USER_ID     := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_NOMEUSR     := QrUsuarios.FieldByName('PersonalName').AsString;
    VAR_WEB_USER_USRENT := QrUsuarios.FieldByName('UsrEnt').AsInteger;
    VAR_WEB_USER_USRNUM := QrUsuarios.FieldByName('UsrNum').AsInteger;
    VAR_WEB_USER_USRID  := QrUsuarios.FieldByName('UsrID').AsInteger;
    VAR_WEB_USER_TIPO   := QrUsuarios.FieldByName('Tipo').AsInteger;
    VAR_WEB_TOKEN_DMK   := QrUsuarios.FieldByName('Token').AsString;
  end;
end;

end.
