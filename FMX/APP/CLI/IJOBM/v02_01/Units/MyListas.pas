unit MyListas;

interface

uses System.Classes, System.SysUtils, Generics.Collections, UnDmkEnums,
  UnMyLinguas;

type
  TMyListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
              var TemControle: TTemControle): Boolean;
    function  CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices:
              TList<TIndices>): Boolean;
    function  CriaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>): Boolean;
    function  CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function  CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
  end;

const
  CO_VERSAO = 1606171151;
  CO_APP_Nome = 'InfrajobMob';
  CO_APP_Descri = 'Infrajob Mobile';
  CO_DMKID_APP = 41;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2
  CO_DBNome = 'infrajobmob';
  LAN_CTOS = 'lanctos';
  WLAN_CTOS = 'wlanctos';

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;

implementation

uses UnGrl_Tabs, UnFMX_Grl_Tabs, UnFinance_Tabs, UnEnti_Tabs, UnUsuarios_Tabs;

{ TMyListas }

function TMyListas.CriaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    if DatabaseName <> CO_DBNome then
    begin
      FMX_Grl_Tabs.CarregaListaTabelas(DatabaseName, Lista);
      Enti_Tabs.CarregaListaTabelas(Lista);
      Finance_Tabs.CarregaListaTabelas(Lista);
      Usuarios_Tabs.CarregaListaTabelas(Lista);
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('opcoes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentidades'), 'entidades', True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wcarteiras'), 'carteiras', True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wcontas'),    'contas',    True);
      MyLinguas.AdTbLst(Lista, False, Lowercase('wlctqrcode'), 'lctqrcode', True);
      MyLinguas.AdTbLst(Lista, False, LowerCase(WLAN_CTOS),    LAN_CTOS,    True);
    end else
    begin
      FMX_Grl_Tabs.CarregaListaTabelas(DatabaseName, Lista);
      Grl_Tabs.CarregaListaTabelas(Lista);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('opcoes') then
    begin
      New(FRIndices);
      FRIndices.Non_unique   := 0;
      FRIndices.Key_name     := 'PRIMARY';
      FRIndices.Seq_in_index := 1;
      FRIndices.Column_name  := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      FMX_Grl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Grl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLindices);
      Usuarios_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //FMX_Grl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;

function TMyListas.CriaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    //FMX_Grl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String;
  FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    //FMX_Grl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('opcoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := 'Chave prim�ria U';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    begin
      FMX_Grl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Grl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Usuarios_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    FMX_Grl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Usuarios_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

end.
