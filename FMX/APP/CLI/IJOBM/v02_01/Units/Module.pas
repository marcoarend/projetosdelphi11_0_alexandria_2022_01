unit Module;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FMX.Forms, FireDAC.FMXUI.Wait, FireDAC.Comp.UI,
  System.UITypes, FMX.StdCtrls;

type
  TDmod = class(TDataModule)
    QrLoc: TFDQuery;
    QrControle: TFDQuery;
    QrUsuarios: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    QrAux: TFDQuery;
    QrUpd: TFDQuery;
    QrEspecificos: TFDQuery;
    AllDB: TFDConnection;
    MyDB: TFDConnection;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dmod: TDmod;

implementation

uses UnFMX_CfgDBApp, MyListas, UnFMX_Geral, UnFMX_Grl_Vars, UnDmkEnums,
  UnFMX_DmkProcFunc, UnGrl_Consts;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  if AllDB.Connected then
    AllDB.Connected := False;
  if MyDB.Connected then
    MyDB.Connected := False;
end;

end.
