program InfrajobMob;

uses
  System.StartUpCopy,
  FMX.Forms,
  MyListas in '..\Units\MyListas.pas',
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  UnFinance_Tabs in '..\..\..\..\..\..\VCL\MDL\FIN\v01_01\Units\UnFinance_Tabs.pas',
  UnitMD5 in '..\..\..\..\..\..\VCL\UTL\Encrypt\v01_01\UnitMD5.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnFMX_DmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkProcFunc.pas',
  UnFMX_Geral in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Geral.pas',
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnFMX_Grl_Vars in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Vars.pas',
  UnFMX_Grl_Tabs in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Tabs.pas',
  UnFMX_ValidaIE in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_ValidaIE.pas',
  PesqCod in '..\..\..\..\..\UTL\_FRM\v01_01\PesqCod.pas' {FmPesqCod},
  UnFMX_DmkWeb in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkWeb.pas',
  Sobre in '..\..\..\..\..\UTL\_FRM\v01_01\Sobre.pas' {FmSobre},
  UnFMX_Financeiro in '..\..\..\..\..\MDL\FIN\v02_01\Units\UnFMX_Financeiro.pas',
  UnFMX_FinanceiroJan in '..\..\..\..\..\MDL\FIN\v02_01\Units\UnFMX_FinanceiroJan.pas',
  UnFMX_MultiDetailAppearanceU in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_MultiDetailAppearanceU.pas',
  Contas in '..\..\..\..\..\MDL\FIN\v02_01\Planos\Contas.pas' {FmContas},
  SincroDB in '..\..\..\..\..\UTL\_FRM\v01_01\SincroDB.pas' {FmSincroDB},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  VerifyDB in '..\..\..\..\..\UTL\_FRM\v01_01\VerifyDB.pas' {FmVerifyDB},
  UnFMX_DmkSincro in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkSincro.pas',
  UnFMX_CfgDBApp in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_CfgDBApp.pas',
  UnEnti_Tabs in '..\..\..\..\..\..\VCL\MDL\ENT\v02_01\UnEnti_Tabs.pas',
  LctGer in '..\..\..\..\..\MDL\FIN\v02_01\Lcts\Gerencia\LctGer.pas' {FmLctGer},
  Carteiras in '..\..\..\..\..\MDL\FIN\v02_01\Forms\Carteiras.pas' {FmCarteiras},
  DmkLogin in '..\..\..\..\..\UTL\_FRM\v01_01\DmkLogin.pas' {FmDmkLogin},
  DmkUsers in '..\..\..\..\..\UTL\_FRM\v01_01\DmkUsers.pas' {FmDmkUsers},
  LctImpQrCode in '..\..\..\..\..\MDL\FIN\v02_01\Lcts\Gerencia\LctImpQrCode.pas' {FmLctImpQrCode},
  LctEdit in '..\..\..\..\..\MDL\FIN\v02_01\Lcts\InsEdit\LctEdit.pas' {FmLctEdit},
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnFMX_DmkDB in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkDB.pas',
  UnFMX_DmkForms in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkForms.pas' {$R *.res},
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnGrl_REST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_REST.pas',
  OAuth in '..\..\..\..\..\UTL\WEB\HTML\v01_01\OAuth.pas' {FmOAuth},
  UnFMX_DmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnFMX_DmkHTML2.pas',
  UnGrl_Sincro in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_Sincro.pas',
  UnGrl_Tabs in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Tabs.pas',
  UnUsuarios_Tabs in '..\..\..\..\..\..\MultiOS\MDL\USR\v02_01\UnUsuarios_Tabs.pas',
  UnGrl_AllOS in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_AllOS.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
