unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.MultiView, FMX.Layouts,
  FMX.ListBox, FMX.Objects;

type
  TFmPrincipal = class(TForm)
    Label2: TLabel;
    MultiView1: TMultiView;
    ToolBar1: TToolBar;
    LaUsuario: TLabel;
    SBConta: TSpeedButton;
    LayoutContent: TLayout;
    Image1: TImage;
    ToolBar2: TToolBar;
    SBOcultar: TSpeedButton;
    Layout1: TLayout;
    SBVerifyDB: TSpeedButton;
    SBSobre: TSpeedButton;
    ScrollBox1: TScrollBox;
    SBPallets: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBVerifyDBClick(Sender: TObject);
    procedure SBSobreClick(Sender: TObject);
    procedure MultiView1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses Module, DmkUsers, Sobre, MyListas, UnFMX_DmkProcFunc;

{$R *.fmx}

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SBOcultar.Visible := True;
  {$ELSE}
  SBOcultar.Visible := False;
  {$ENDIF}
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  Dmod.ConfiguraBD(LaUsuario);
end;

procedure TFmPrincipal.MultiView1Click(Sender: TObject);
begin
  MultiView1.HideMaster;
end;

procedure TFmPrincipal.SBSobreClick(Sender: TObject);
begin
  FMX_dmkPF.CriaFm_AllOS(TFmSobre, FmSobre);
end;

procedure TFmPrincipal.SBVerifyDBClick(Sender: TObject);
begin
  //Dmod.MostraVerifyDB(False);
end;

end.
