unit Module;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FMX.Forms, FireDAC.FMXUI.Wait, FireDAC.Comp.UI,
  System.UITypes, FMX.StdCtrls;

type
  TDmod = class(TDataModule)
    MyDB: TFDConnection;
    QrLoc: TFDQuery;
    QrControle: TFDQuery;
    QrUsuarios: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    QrAux: TFDQuery;
    QrUpd: TFDQuery;
    QrEspecificos: TFDQuery;
    AllDB: TFDConnection;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraVerifyDB(Verifica: Boolean; DataBase: TFDConnection);
    procedure ConfiguraDispositivo(LaUsuario: TLabel);
    procedure ReopenOpcoesEspecificas();
    procedure ReopenControle();
    procedure ReopenUsuarios();
    procedure ConfiguraBD(LaUsuario: TLabel);
  end;

var
  Dmod: TDmod;

implementation

uses UnGrl_DmkDB, UnFMX_DmkDB, MyListas, VerifyDB, UnFMX_Geral, UnFMX_Grl_Vars,
  UnDmkEnums, UnFMX_DmkProcFunc, DmkLogin, UnGrl_Consts, UnGrl_Vars;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDmod.ConfiguraBD(LaUsuario: TLabel);
var
  Versao: Integer;
begin
  FMX_dmkDB.ConfiguraDB(AllDB, istSQLite, CO_DBNome, CO_RandStrWeb01);
  //
  ConfiguraDispositivo(LaUsuario);
end;

procedure TDmod.ConfiguraDispositivo(LaUsuario: TLabel);
var
  Res: Boolean;
begin
  ReopenUsuarios;
  //
  if (QrUsuarios.RecordCount > 0) then
    Res := True
  else
    Res := False;
  //
  if not Res then
  begin
    VAR_WEB_USER_ID   := 0;
    VAR_WEB_TOKEN_DMK := '';
    //
    if LaUsuario <> nil then
      LaUsuario.Text := '';
    //
    FMX_dmkPF.CriaFm_AllOS(TFmDmkLogin, FmDmkLogin, True);
  end else
  begin
    ReopenControle;
    ReopenOpcoesEspecificas;
    //
    FMX_Geral.DefineFormatacoes;
    //
    if LaUsuario <> nil then
      LaUsuario.Text := VAR_WEB_NOMEUSR;
  end;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  if MyDB.Connected then
    MyDB.Connected := False;
end;

procedure TDmod.MostraVerifyDB(Verifica: Boolean; DataBase: TFDConnection);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FDataBase  := DataBase;
  FmVerifyDB.FullScreen := True;
  FmVerifyDB.Show;
  {$ELSE}
  Application.CreateForm(TFmVerifyDB, FmVerifyDB);
  FmVerifyDB.FVerifica  := Verifica;
  FmVerifyDB.FDataBase  := DataBase;
  FmVerifyDB.FullScreen := False;
  FmVerifyDB.ShowModal;
  FmVerifyDB.Destroy;
  {$ENDIF}
end;

procedure TDmod.ReopenControle;
begin
  FMX_dmkDB.AbreSQLQuery0(QrControle, AllDB, [
    'SELECT Device, Versao ',
    'FROM controle ',
    '']);
  if QrControle.RecordCount > 0 then
    VAR_WEB_DEVICE_ID   := QrControle.FieldByName('Device').AsInteger;
end;

procedure TDmod.ReopenOpcoesEspecificas;
begin
  (* N�o usa habitlitar quando precisar
  AllOS_dmkDB.AbreSQLQuery0(QrEspecificos, AllDB, [
    'SELECT * ',
    'FROM opcijobm ',
    '']);
  if QrEspecificos.RecordCount = 0 then
  begin
    AllOS_dmkDB.CarregaSQLInsUpd(QrUpd, MyDB, stIns, 'opcijobm', False,
      [], ['Codigo'], [], [1], True, False, '', stMobile, False);
  end;
  *)
end;

procedure TDmod.ReopenUsuarios;
begin
  FMX_dmkDB.AbreSQLQuery0(QrUsuarios, AllDB, [
    'SELECT name ',
    'FROM sqlite_master ',
    'WHERE name="Usuarios"',
    '']);
  if QrUsuarios.RecordCount > 0 then
  begin
    FMX_dmkDB.AbreSQLQuery0(QrUsuarios, AllDB, [
      'SELECT Codigo, PersonalName, UserName, Tipo, UsrEnt, UsrID, UsrNum, Token ',
      'FROM Usuarios ',
      'WHERE Atual = 1 ',
      '']);
  end;
  if QrUsuarios.RecordCount > 0 then
  begin
    VAR_USUARIO         := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_USER_ID     := QrUsuarios.FieldByName('Codigo').AsInteger;
    VAR_WEB_NOMEUSR     := QrUsuarios.FieldByName('PersonalName').AsString;
    VAR_WEB_USER_USRENT := QrUsuarios.FieldByName('UsrEnt').AsInteger;
    VAR_WEB_USER_USRNUM := QrUsuarios.FieldByName('UsrNum').AsInteger;
    VAR_WEB_USER_USRID  := QrUsuarios.FieldByName('UsrID').AsInteger;
    VAR_WEB_USER_TIPO   := QrUsuarios.FieldByName('Tipo').AsInteger;
    VAR_WEB_TOKEN_DMK   := QrUsuarios.FieldByName('Token').AsString;
    //
    VAR_APP_DBUsuarioNome := CO_DBNome + '_' + FMX_Geral.FF0(VAR_WEB_USER_ID);
    //
    FMX_dmkDB.ConfiguraDB(MyDB, istSQLite, VAR_APP_DBUsuarioNome, CO_RandStrWeb01);
  end;
end;

end.
