program BDermMob;

uses
  System.StartUpCopy,
  FMX.Forms,
  MyListas in '..\Units\MyListas.pas',
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\AllOS\Units\UnDmkEnums.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnFMX_DmkDB in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkDB.pas',
  UnFMX_Grl_Tabs in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Tabs.pas',
  UnFMX_DmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkProcFunc.pas',
  UnFMX_Geral in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Geral.pas',
  UnFMX_ValidaIE in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_ValidaIE.pas',
  UnFMX_Grl_Vars in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Vars.pas',
  PesqCod in '..\..\..\..\..\UTL\_FRM\v01_01\PesqCod.pas' {FmPesqCod},
  UnFMX_DmkWeb in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkWeb.pas',
  VerifyDB in '..\..\..\..\..\UTL\_FRM\v01_01\VerifyDB.pas' {FmVerifyDB},
  DmkUsers in '..\..\..\..\..\UTL\_FRM\v01_01\DmkUsers.pas' {FmDmkUsers},
  DmkLogin in '..\..\..\..\..\UTL\_FRM\v01_01\DmkLogin.pas' {FmDmkLogin},
  UnFMX_DmkDevice in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkDevice.pas',
  UnitMD5 in '..\..\..\..\..\..\VCL\UTL\Encrypt\v01_01\UnitMD5.pas',
  Sobre in '..\..\..\..\..\UTL\_FRM\v01_01\Sobre.pas' {FmSobre},
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\AllOS\Units\UnGrl_Consts.pas',
  {$If DEFINED(ANDROID)}
  Android_BarcodeScanner in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_BarcodeScanner.pas',
  Android_FMX_NetworkState in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_NetworkState.pas',
  Android_NetworkState in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_NetworkState.pas',
  Android_FMX_ProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_ProcFunc.pas',
  {$Else}
  Windows_FMX_ProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  {$EndIf}
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\AllOS\Units\UnGrl_DmkDB.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\AllOS\Units\UnGrl_Vars.pas',
  UnFMX_CfgDBApp in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_CfgDBApp.pas',
  SincroDB in '..\..\..\..\..\UTL\_FRM\v01_01\SincroDB.pas' {FmSincroDB},
  UnFMX_DmkSincro in '..\..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkSincro.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
