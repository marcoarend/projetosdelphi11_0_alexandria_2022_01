program InfrajobMob;

uses
  System.StartUpCopy,
  FMX.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnAllOS_DmkProcFunc in '..\..\..\..\..\AllOS\Units\UnAllOS_DmkProcFunc.pas',
  Sobre in '..\..\..\..\..\AllOS\Forms\v_02_00\Sobre.pas' {FmSobre},
  UnGeral in '..\..\..\..\..\AllOS\Units\UnGeral.pas',
  UnDmkEnums in '..\..\..\..\..\AllOS\Units\UnDmkEnums.pas',
  UnValidaIE in '..\..\..\..\..\AllOS\Units\UnValidaIE.pas',
  UnGrl_Vars in '..\..\..\..\..\AllOS\Units\UnGrl_Vars.pas',
  UnAllOS_DmkDevice in '..\..\..\..\..\AllOS\Units\UnAllOS_DmkDevice.pas',
  MyListas in '..\Units\MyListas.pas',
  UnMyLinguas in '..\..\..\..\..\AllOS\Listas\UnMyLinguas.pas',
  UnGrl_Tabs in '..\..\..\..\..\AllOS\Units\UnGrl_Tabs.pas',
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  VerifyDB in '..\..\..\..\..\AllOS\Forms\v_02_00\VerifyDB.pas' {FmVerifyDB},
  UnAllOS_DmkDB in '..\..\..\..\..\AllOS\Units\UnAllOS_DmkDB.pas',
  DmkLogin in '..\..\..\..\..\AllOS\Forms\v_02_00\DmkLogin.pas' {FmDmkLogin},
  UnAllOS_DmkWeb in '..\..\..\..\..\AllOS\Units\UnAllOS_DmkWeb.pas',
  DmkUsers in '..\..\..\..\..\AllOS\Forms\v_02_00\DmkUsers.pas' {FmDmkUsers},
  Android.NetworkState in '..\..\..\..\..\AllOS\Units\Android\Android.NetworkState.pas',
  UnAllOS_NetworkState in '..\..\..\..\..\AllOS\Units\UnAllOS_NetworkState.pas',
  Entidades in '..\..\..\..\MDL\ENT\v02_01\Entidades.pas' {FmEntidades};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDmod, Dmod);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
