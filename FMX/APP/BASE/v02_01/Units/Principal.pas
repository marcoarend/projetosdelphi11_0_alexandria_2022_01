unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, FMX.ListBox, FMX.Layouts,
  FMX.TreeView, FMX.MultiView, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, FMX.Objects;

type
  TFmPrincipal = class(TForm)
    MasterToolbar: TToolBar;
    Menu: TTabItem;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    LBISobre: TListBoxItem;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    SBConta: TSpeedButton;
    QrUsuariosAtual: TShortintField;
    BindingsList1: TBindingsList;
    LinkFillControlToField1: TLinkFillControlToField;
    Layout1: TLayout;
    Image1: TImage;
    DetailToolbar: TToolBar;
    MasterButton: TSpeedButton;
    SBOcultar: TSpeedButton;
    Label1: TLabel;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    LBICarteiras: TListBoxItem;
    LBIContas: TListBoxItem;
    procedure LBISobreClick(Sender: TObject);
    procedure LBIVerifyDBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1ItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure FormShow(Sender: TObject);
    procedure SBContaClick(Sender: TObject);
    procedure CBUsuarioClick(Sender: TObject);
    procedure LBIEntidadesClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenUsuarios;
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses Module, UnAllOS_DmkProcFunc, Sobre, DmkUsers, UnAllOS_DmkDB, Entidades;

{$R *.fmx}
{$R *.XLgXhdpiTb.fmx ANDROID}
{$R *.Surface.fmx MSWINDOWS}
{$R *.SmXhdpiPh.fmx ANDROID}
{$R *.iPhone55in.fmx IOS}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TFmPrincipal.CBUsuarioClick(Sender: TObject);
begin
  if QrUsuarios.Active = False then

end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  {$IF DEFINED(iOS) or DEFINED(ANDROID)}
  SBOcultar.Visible := True;
  {$ELSE}
  SBOcultar.Visible := False;
  {$ENDIF}
  FullScreen := False;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  Dmod.ConfiguraBD(LaUsuario);
end;

procedure TFmPrincipal.LBIEntidadesClick(Sender: TObject);
begin
  AllOS_dmkPF.CriaFm_AllOS(TFmEntidades, FmEntidades);
end;

procedure TFmPrincipal.LBISobreClick(Sender: TObject);
begin
  AllOS_dmkPF.CriaFm_AllOS(TFmSobre, FmSobre);
end;

procedure TFmPrincipal.LBIVerifyDBClick(Sender: TObject);
begin
  Dmod.MostraVerifyDB(False);
end;

procedure TFmPrincipal.ListBox1ItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  MultiView1.HideMaster;
end;

procedure TFmPrincipal.ReopenUsuarios;
begin
  AllOS_dmkDB.AbreSQLQuery0(QrUsuarios, Dmod.MyDB, [
    'SELECT Codigo, UserName, Atual ',
    'FROM usuarios ',
    'ORDER BY UserName ',
    '']);
end;

procedure TFmPrincipal.SBContaClick(Sender: TObject);
begin
  AllOS_dmkPF.CriaFm_AllOS(TFmDmkUsers, FmDmkUsers);
end;

end.

