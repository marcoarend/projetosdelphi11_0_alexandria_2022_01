unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, FMX.Gestures, FMX.Controls.Presentation, UnGrl_Geral,
  FMX.ScrollBox, FMX.Memo, FMX.DialogService, System.Rtti, FMX.Grid.Style,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.DBScope, Data.DB,
  Datasnap.DBClient, Data.Bind.Grid, FMX.Grid, FMX.Edit;

type
  TFmPrincipal = class(TForm)
    HeaderToolBar: TToolBar;
    ToolBarLabel: TLabel;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    GestureManager1: TGestureManager;
    LaUsuario: TLabel;
    Button1: TButton;
    Panel5: TPanel;
    Button5: TButton;
    ClientDataSet1: TClientDataSet;
    ClientDataSet1Codigo: TIntegerField;
    ClientDataSet1Nome: TStringField;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormGesture(Sender: TObject; const EventInfo: TGestureEventInfo;
      var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
    procedure pergunta();
    procedure resposta();
    procedure posPergunta();
  public
    { Public declarations }
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses  UnFMX_CfgDBApp, Module, UnFMX_DmkUnLic;

{$R *.fmx}

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  FMX_CfgDBApp.ConfiguraBD(Dmod.AllDB, Dmod.MyDB, Dmod.QrLoc, Dmod.QrUpd,
    Dmod.QrUsuarios, Dmod.QrControle, Dmod.QrEspecificos, LaUsuario);
  FMX_CfgDBApp.MostraVerifyDB(False, Dmod.AllDB, Dmod.MyDB);
end;

procedure TFmPrincipal.Button5Click(Sender: TObject);
begin
  FMX_dmkUnLic.LiberaUso('02582267000160');
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  { This defines the default active tab at runtime }
  TabControl1.ActiveTab := TabItem1;
end;

procedure TFmPrincipal.FormGesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
{$IFDEF ANDROID}
  case EventInfo.GestureID of
    sgiLeft:
    begin
      if TabControl1.ActiveTab <> TabControl1.Tabs[TabControl1.TabCount-1] then
        TabControl1.ActiveTab := TabControl1.Tabs[TabControl1.TabIndex+1];
      Handled := True;
    end;

    sgiRight:
    begin
      if TabControl1.ActiveTab <> TabControl1.Tabs[0] then
        TabControl1.ActiveTab := TabControl1.Tabs[TabControl1.TabIndex-1];
      Handled := True;
    end;
  end;
{$ENDIF}
end;

procedure TFmPrincipal.pergunta;
begin
  TDialogService.MessageDialog('Choose a button:', System.UITypes.TMsgDlgType.mtInformation,
    [System.UITypes.TMsgDlgBtn.mbYes, System.UITypes.TMsgDlgBtn.mbNo, System.UITypes.TMsgDlgBtn.mbCancel],
    System.UITypes.TMsgDlgBtn.mbYes, 0,

    // Use an anonymous method to make sure the acknowledgment appears as expected.
    procedure(const AResult: TModalResult)
    begin
      resposta();
    end);
end;

procedure TFmPrincipal.posPergunta;
begin
  ShowMessage('TFmPrincipal.posPergunta');
end;

procedure TFmPrincipal.resposta;
begin
  ShowMessage('TFmPrincipal.resposta');
end;

end.
