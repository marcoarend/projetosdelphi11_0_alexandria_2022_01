program TabbedApplication;

uses
  System.StartUpCopy,
  {$IF DEFINED(ANDROID)}
  UnGrl_OS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Android\UnGrl_OS.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_FMX_ProcFunc in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_ProcFunc.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_FMX_NetworkState in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_NetworkState.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_NetworkState in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_NetworkState.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Android_BarcodeScanner in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_BarcodeScanner.pas',
  {$ENDIF }
  {$IF DEFINED(ANDROID)}
  Androidapi.JNI.PowerManager in '..\..\..\..\UTL\_UNT\v01_01\Android\Androidapi.JNI.PowerManager.pas',
  {$ENDIF }
  {$IFDEF MSWINDOWS}
  UnGrl_OS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Windows\UnGrl_OS.pas',
  {$ENDIF }
  {$IFDEF MSWINDOWS}
  Windows_FMX_ProcFunc in '..\..\..\..\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  UnitMD5 in '..\..\..\..\..\VCL\UTL\Encrypt\v01_01\UnitMD5.pas',
  {$ENDIF }
  {$IF DEFINED(iOS)}
  UnGrl_OS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\iOS\UnGrl_OS.pas',
  {$ENDIF }
  FMX.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnFMX_DmkDB in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkDB.pas',
  UnDmkEnums in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnGrl_DmkDB in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnFMX_Geral in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_Geral.pas',
  UnFMX_ValidaIE in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_ValidaIE.pas',
  UnFMX_DmkProcFunc in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkProcFunc.pas',
  UnGrl_Vars in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnFMX_Grl_Vars in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Vars.pas',
  UnGrl_Consts in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_Tabs in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Tabs.pas',
  UnGrl_Geral in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  Module in '..\Units\Module.pas' {Dmod},
  MyListas in '..\Units\MyListas.pas',
  UnFMX_CfgDBApp in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_CfgDBApp.pas',
  VerifyDB in '..\..\..\..\UTL\_FRM\v01_01\VerifyDB.pas' {FmVerifyDB},
  UnFMX_DmkForms in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkForms.pas',
  PesqCod in '..\..\..\..\UTL\_FRM\v01_01\PesqCod.pas' {FmPesqCod},
  UnFMX_DmkWeb in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkWeb.pas',
  DmkLogin in '..\..\..\..\UTL\_FRM\v01_01\DmkLogin.pas' {FmDmkLogin},
  UnGrl_AllOS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_AllOS.pas',
  UnTemp in 'UnTemp.pas',
  UnFMX_DmkRemoteQuery in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkRemoteQuery.pas',
  DmkDialog in '..\..\..\..\UTL\_FRM\v03_01\DmkDialog.pas' {FmDmkDialog},
  UnFMX_DmkUnLic in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkUnLic.pas',
  UnFMX_Ref in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_Ref.pas',
  Camera in '..\Units\Camera.pas' {FmCamera},
  UnGrl_IO in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_IO.pas',
  UnGrl_DmkWeb in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkWeb.pas',
  Upload in '..\Units\Upload.pas' {FmUpload},
  UnGrl_Components in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Components.pas',
  MultiDetailAppearanceU in 'C:\Users\Public\Documents\Embarcadero\Studio\19.0\Samples\Object Pascal\Multi-Device Samples\User Interface\ListView\ListViewMultiDetailAppearance\MultiDetailAppearanceU.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TDmod, Dmod);
  Application.Run;



(*
  System.StartUpCopy,
  {$IF DEFINED(ANDROID)}
  UnGrl_OS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Android\UnGrl_OS.pas',
  Android_FMX_ProcFunc in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_ProcFunc.pas',
  Android_FMX_NetworkState in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_FMX_NetworkState.pas',
  Android_NetworkState in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_NetworkState.pas',
  Android_BarcodeScanner in '..\..\..\..\UTL\_UNT\v01_01\Android\Android_BarcodeScanner.pas',
  Androidapi.JNI.PowerManager in '..\..\..\..\UTL\_UNT\v01_01\Android\Androidapi.JNI.PowerManager.pas',
  {$ENDIF }
  {$IFDEF MSWINDOWS}
  UnGrl_OS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\Windows\UnGrl_OS.pas',
  {$ENDIF }
  {$IF DEFINED(iOS)}
  UnGrl_OS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\iOS\UnGrl_OS.pas',
  {$ENDIF }
  FMX.Forms,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnFMX_DmkDB in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkDB.pas',
  UnDmkEnums in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnGrl_DmkDB in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnFMX_Geral in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_Geral.pas',
  UnFMX_ValidaIE in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_ValidaIE.pas',
  UnFMX_DmkProcFunc in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkProcFunc.pas',
  UnGrl_Vars in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnFMX_Grl_Vars in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_Grl_Vars.pas',
  UnGrl_Consts in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_Tabs in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Tabs.pas',
  UnGrl_Geral in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  Module in '..\Units\Module.pas' {Dmod},
  MyListas in '..\Units\MyListas.pas',
  UnFMX_CfgDBApp in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_CfgDBApp.pas',
  VerifyDB in '..\..\..\..\UTL\_FRM\v01_01\VerifyDB.pas' {FmVerifyDB},
  UnFMX_DmkForms in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkForms.pas',
  PesqCod in '..\..\..\..\UTL\_FRM\v01_01\PesqCod.pas' {FmPesqCod},
  UnFMX_DmkWeb in '..\..\..\..\UTL\_UNT\v01_01\UnFMX_DmkWeb.pas',
  DmkLogin in '..\..\..\..\UTL\_FRM\v01_01\DmkLogin.pas' {FmDmkLogin},
  UnGrl_AllOS in '..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_AllOS.pas';
*)

 end.
