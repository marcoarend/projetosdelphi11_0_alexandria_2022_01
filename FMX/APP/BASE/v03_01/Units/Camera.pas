unit Camera;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.NetEncoding,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, System.Actions,
  FMX.ActnList, FMX.MediaLibrary, FMX.Platform, System.Messaging, FMX.StdActns,
  FMX.MediaLibrary.Actions, FMX.ScrollBox, FMX.Memo,
  System.IOUtils;

type
  TTipFile = (ttfJpg, ttfPng);
  TFmCamera = class(TForm)
    ToolBar1: TToolBar;
    SBCamera: TSpeedButton;
    ActionList1: TActionList;
    SBLibrary: TSpeedButton;
    TakePhotoFromCameraAction1: TTakePhotoFromCameraAction;
    TakePhotoFromLibraryAction1: TTakePhotoFromLibraryAction;
    SBShare: TSpeedButton;
    ShowShareSheetAction1: TShowShareSheetAction;
    SBSaida: TSpeedButton;
    Image1: TImage;
    SBSave: TSpeedButton;
    procedure TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
    procedure ShowShareSheetAction1BeforeExecute(Sender: TObject);
    procedure SBSaidaClick(Sender: TObject);
    procedure TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
    procedure SBSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTipoFile: TTipFile;
    FFilePrefix: String;
    FDirectory: String;
  end;

var
  FmCamera: TFmCamera;

implementation

uses FMX.DialogService, UnGrl_Geral, UnGrl_IO;

{$R *.fmx}

{ TFmCamera }

procedure TFmCamera.SBSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCamera.SBSaveClick(Sender: TObject);
var
  Path, Name: String;
begin
  Path := Grl_IO.GetApplicationPath(Application.Title, FDirectory);
  Name := FFilePrefix + Grl_Geral.FDT(Now, 26);
  //
  case FTipoFile of
    ttfJpg:
      Grl_IO.SaveImageInJpegFile(Image1, Path, Name);
    ttfPng:
      Grl_IO.SaveImageInPngFile(Image1, Path, Name);
  end;
  //
  Grl_Geral.MB_Aviso('Salvo com sucesso!');
end;

procedure TFmCamera.ShowShareSheetAction1BeforeExecute(Sender: TObject);
begin
  ShowShareSheetAction1.Bitmap.Assign(Image1.Bitmap);
end;

procedure TFmCamera.TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
begin
  Image1.Bitmap.Assign(Image);
end;

procedure TFmCamera.TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
begin
  Image1.Bitmap.Assign(Image);
end;

end.
