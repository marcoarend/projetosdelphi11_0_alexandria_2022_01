object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 282
  Width = 355
  object QrLoc: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 31
    Top = 21
  end
  object QrControle: TFDQuery
    SQL.Strings = (
      'SELECT Versao FROM controle ')
    Left = 31
    Top = 84
  end
  object QrUsuarios: TFDQuery
    SQL.Strings = (
      'SELECT Versao FROM controle ')
    Left = 87
    Top = 84
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 160
    Top = 84
  end
  object QrAux: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 71
    Top = 21
  end
  object QrUpd: TFDQuery
    SQL.Strings = (
      'PRAGMA table_info(weventoslct)')
    Left = 119
    Top = 21
  end
  object QrEspecificos: TFDQuery
    SQL.Strings = (
      '')
    Left = 31
    Top = 140
  end
  object AllDB: TFDConnection
    Params.Strings = (
      
        'Database=C:\Executaveis\MultiOS\InfrajobMob\Win32\infrajobmob.db' +
        '3'
      'DriverID=SQLite'
      'Encrypt=No'
      'LockingMode=Exclusive')
    FetchOptions.AssignedValues = [evLiveWindowParanoic, evLiveWindowFastFirst]
    FetchOptions.LiveWindowFastFirst = True
    LoginPrompt = False
    Left = 296
    Top = 21
  end
  object MyDB: TFDConnection
    Params.Strings = (
      'DriverID=SQLite')
    FetchOptions.AssignedValues = [evLiveWindowFastFirst]
    FetchOptions.LiveWindowFastFirst = True
    LoginPrompt = False
    Left = 256
    Top = 21
  end
  object FDPragma: TFDQuery
    Connection = AllDB
    Left = 256
    Top = 74
  end
end
