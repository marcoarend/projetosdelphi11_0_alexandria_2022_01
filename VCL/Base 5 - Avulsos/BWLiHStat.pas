unit BWLiHStat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmBWLiHStat = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    Ed_Sel_: TdmkEditCB;
    CB_Sel_: TdmkDBLookupComboBox;
    QrBWStatusAnt: TMySQLQuery;
    QrBWStatusAntCodigo: TIntegerField;
    QrBWStatusAntNome: TWideStringField;
    DsBWStatusAnt: TDataSource;
    QrBWStatusAtu: TMySQLQuery;
    QrBWStatusAtuCodigo: TIntegerField;
    QrBWStatusAtuNome: TWideStringField;
    DsBWStatusAtu: TDataSource;
    Label9: TLabel;
    EdBWStatus: TdmkEditCB;
    CBBWStatus: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    FCodigo, FStatusAnt: Integer;
  end;

  var
  FmBWLiHStat: TFmBWLiHStat;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmBWLiHStat.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
(*
  MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle? := EdControle.ValueVariant;
  Controle? := UMyMod.BuscaEmLivreY_Def('cadastro_com_itens_its', 'Controle', ImgTipo.SQLType, Controle);
ou > ? := UMyMod.BPGS1I32('cadastro_com_itens_its', 'Controle', '', '', tsPosNeg?, ImgTipo.SQLType, Controle);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'cadastro_com_itens_its', auto_increment?[
capos?], [
'Controle'], [
valores?], [
Controle], UserDataAlterweb?, IGNORE?
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
*)
end;

procedure TFmBWLiHStat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBWLiHStat.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmBWLiHStat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrBWStatusAnt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBWStatusAtu, Dmod.MyDB);
end;

procedure TFmBWLiHStat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBWLiHStat.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
end;

end.
