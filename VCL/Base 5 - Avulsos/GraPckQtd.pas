unit GraPckQtd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.ComCtrls, mySQLDirectQuery;

type
  TFmGraPckQtd = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PCGrades: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GradeC: TStringGrid;
    GradeA: TStringGrid;
    DqAux: TMySQLDirectQuery;
    TabSheet4: TTabSheet;
    GradeX: TStringGrid;
    GradeK: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure GradeKClick(Sender: TObject);
    procedure GradeKDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
  private
    { Private declarations }
    function  ObtemCtrlPckQtdDeGraGruX(GraGruX: Integer): Integer;
    function  ObtemQtde(var Qtde: Double): Boolean;
    function  ObtemPerc(var Perc: Double): Boolean;
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    function  SetaItemCorTam(GridA, GridC, GridX, GridK: TStringGrid): Boolean;
    function  SetaVariosItensCorTam(GridA, GridC, GridX, GridK: TStringGrid): Boolean;
    function  InsAltAtual(GraGruX: Integer; Qtde: Double): Boolean;
  public
    { Public declarations }
    //FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FGraPckCad, FGraTamCad, FNivel1: Integer;
    //
    procedure PreparaGrades();
  end;

  var
  FmGraPckQtd: TFmGraPckQtd;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModProd, GetValor, GraGruY, UMySQLDB, ModuleGeral;

{$R *.DFM}

procedure TFmGraPckQtd.BtOKClick(Sender: TObject);
var
  Codigo, GraGru1, Controle: Integer;
begin
(*
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle? := EdControle.ValueVariant;
  Controle? := UMyMod.BuscaEmLivreY_Def('cadastro_com_itens_its', 'Controle', ImgTipo.SQLType, Controle);
ou > ? := UMyMod.BPGS1I32('cadastro_com_itens_its', 'Controle', '', '', tsPosNeg?, ImgTipo.SQLType, Controle);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'cadastro_com_itens_its', auto_increment?[
capos?], [
'Controle'], [
valores?], [
Controle], UserDataAlterweb?, IGNORE?
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
*)
end;

procedure TFmGraPckQtd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraPckQtd.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  DBEdit1.DataSource := FDsCab;
  DBEdit2.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmGraPckQtd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCGrades.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 192;
  GradeC.ColWidths[0] := 192;
  GradeX.ColWidths[0] := 192;
  GradeK.ColWidths[0] := 192;
end;

procedure TFmGraPckQtd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraPckQtd.GradeKClick(Sender: TObject);
begin
  if (GradeK.Col = 0) or (GradeK.Row = 0) then
    SetaVariosItensCorTam(GradeA, GradeC, GradeX, GradeK)
  else
    SetaItemCorTam(GradeA, GradeC, GradeX, GradeK);

  //

  DmProd.AtualizaGradesPack1(FNivel1, FGraPckCad, GradeK);
end;

procedure TFmGraPckQtd.GradeKDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Qtde: Double;
  Txto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel2.Color
  else if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
(*
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack,
      Cor, taLeftJustify,
      GradeK.Cells[Acol, ARow], 0, 0, False)
*)
  else
  begin
    Qtde := Geral.DMV(GradeK.Cells[Acol, ARow]);
    if Qtde > 0 then
      Txto := FormatFloat(Dmod.FStrFmtQtd, Qtde)
    else
      Txto := '';
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack, Cor,
      taRightJustify, Txto, 0, 0, False);
  end;
end;

function TFmGraPckQtd.InsAltAtual(GraGruX: Integer; Qtde: Double): Boolean;
var
  Codigo, Controle: Integer;
  SQLType: TSQLType;
begin
  Result         := False;
  //SQLType        := ImgTipo.SQLType;
  Codigo         := FGraPckCad;
  Controle       := ObtemCtrlPckQtdDeGraGruX(GraGruX);
  //GraGruX        := ;
  //Qtde           := ;
  //
  if Controle = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  Controle := UMyMod.BPGS1I32('grapckqtd', 'Controle', '', '', tsPos, SQLType, Controle);
  Result   := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'grapckqtd', False, [
  'Codigo', 'GraGruX', 'Qtde'], [
  'Controle'], [
  Codigo, GraGruX, Qtde], [
  Controle], True);
end;

function TFmGraPckQtd.ObtemCtrlPckQtdDeGraGruX(GraGruX: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLDIrectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT Controle ',
  'FROM grapckqtd ',
  'WHERE Codigo=' + Geral.FF0(FGraPckCad),
  'AND GraGruX=' + Geral.FF0(GraGruX),
  '']);
  //
  Result := USQLDB.Int(DqAux, 'Controle');
end;

function TFmGraPckQtd.ObtemPerc(var Perc: Double): Boolean;
var
  ResVar: Variant;
begin
  //Perc := 100;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Perc, 3, 0, '', '', True, '200% = 2X, 50% = 1/2',
  'Informe o percentual de ajuste: ',
  0, ResVar) then
  begin
    Perc := Geral.DMV(ResVar);
    if (Perc >= 0) then
      Result := True
    else
      Geral.MB_Aviso('Valor inv�lido! N�o pode ser negativo!');
  end;
end;

function TFmGraPckQtd.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Qtde, 3, 0, '', '', True, 'Quantidade', 'Informe a quantidade: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    if (Qtde >= 0) then
      Result := True
    else
      Geral.MB_Aviso('Valor inv�lido! N�o pode ser negativo!');
  end;
end;

procedure TFmGraPckQtd.PreparaGrades();
begin
  DmProd.ConfigGradesPack1(FNivel1, FGraTamCad, FGraPckCad, GradeA, GradeC, GradeX, GradeK);
end;

procedure TFmGraPckQtd.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
(*
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database?);
    //
    if Controle? <> 0 then
      FQrIts.Locate('Controle?, Controle?, []);
  end;
*)
end;

function TFmGraPckQtd.SetaItemCorTam(GridA, GridC, GridX, GridK: TStringGrid): Boolean;
var
  Ativo, Coluna, Linha,c, l, Controle: Integer;
  Qtde: Double;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridK.Col;
  Linha  := GridK.Row;
  Ativo := Geral.IMV(GridA.Cells[Coluna, Linha]);
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  Controle := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) then Exit;
  if (Ativo <> 0) and (Controle <> 0) then
  begin
    //Ativo := 1;
    if not ObtemQtde(Qtde) then Exit;
    InsAltAtual(Controle, Qtde);
  end;
  //
  DmProd.AtualizaGradesPack1(FNivel1, FGraPckCad, GradeK);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmGraPckQtd.SetaVariosItensCorTam(GridA, GridC, GridX, GridK:
  TStringGrid): Boolean;
var
  Coluna, Linha, Ativo, c, l, Controle(*, Ativos*): Integer;
  ColI, ColF, RowI, RowF, CountC, CountL: Integer;
  ItensTxt: String;
  Qtde, Perc: Double;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  //Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridK.Col;
  Linha  := GridK.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
(*
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  //ShowMessage('Ativos: ' + IntToStr(Ativos));
  if Ativos > 0 then
    Ativo := 0
  else
    Ativo := 1;
*)

  //
  if (Coluna = 0) and (Linha = 0) then
  begin
    ItensTxt := 'todo grupo';
  end else if Coluna = 0 then
  begin
    ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
  end else if Linha = 0 then
  begin
    ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
  end;
  if Geral.MB_Pergunta('Confirma a defini��o de quantidade de ' +
  ItensTxt + '?') <> ID_YES then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Perc := 100;
  Qtde := 0;
  if (Coluna = 0) and (Linha <> 0) then
  begin
    if not ObtemPerc(Perc) then Exit;
  end else
  begin
    if not ObtemQtde(Qtde) then Exit;
  end;
  for c := ColI to ColF do
  begin
    for l := RowI to RowF do
    begin
       Ativo := Geral.IMV(GridA.Cells[c, l]);
       Controle := Geral.IMV(GridC.Cells[c, l]);
      if (Ativo = 1) and (Controle <> 0) then
      begin
        if (Coluna = 0) and (Linha <> 0) then
          Qtde := Geral.DMV(GridK.Cells[c, l]);
        InsAltAtual(Controle, Qtde * Perc / 100);
      end;
    end;
  end;
  //
  DmProd.AtualizaGradesPack1(FNivel1, FGraPckCad, GradeK);
  Result := True;
  Screen.Cursor := crDefault;
end;

(*
function TFmGraGruAtiCorTam.StatusItemCorTam(GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Ativo, c, l, Controle, Coluna, Linha, GraGruY: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  Ativo := Geral.IMV(GridA.Cells[Coluna, Linha]);
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  Controle := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) then Exit;
  if (Ativo = 0) and (Controle = 0) then
  begin
    Ativo := 1;

    //GraGruY := DmProd.ObtemGraGruYDeGraGru1(FNivel1);
    if FGraGruY = 0 then
      GraGruY := DmProd.ObtemGraGruYDeGraGru1(FNivel1)
    else
      GraGruY := FGraGruY;
    //

    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'GraGruX', 'GraGruX', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET Ativo=:P0, GraGruC=:P1, ');
    Dmod.QrUpd.SQL.Add('GraGru1=:P2, GraTamI=:P3, GraGruY=:P4, Controle=:P5');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := l;
    Dmod.QrUpd.Params[02].AsInteger := FNivel1;
    Dmod.QrUpd.Params[03].AsInteger := C;
    Dmod.QrUpd.Params[04].AsInteger := GraGruY;
    Dmod.QrUpd.Params[05].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end else begin
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE gragrux SET Ativo=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end;
  DmProd.AtualizaGradesAtivos1(FNivel1, GridA, GridC);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmGraGruAtiCorTam.StatusVariosItensCorTam(GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, Ativo, c, l, Controle, Ativos: Integer;
  ColI, ColF, RowI, RowF, CountC, CountL: Integer;
  AtivTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  //ShowMessage('Ativos: ' + IntToStr(Ativos));
  if Ativos > 0 then
    Ativo := 0
  else
    Ativo := 1;
  if Ativo = 0 then
    AtivTxt := ' desativa��o '
  else
    AtivTxt := 'ativa��o';
  //
  if (Coluna = 0) and (Linha = 0) then
  begin
    ItensTxt := 'todo grupo';
  end else if Coluna = 0 then
  begin
    ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
  end else if Linha = 0 then
  begin
    ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
  end;
  if Geral.MB_Pergunta('Confirma a ' + AtivTxt + ' de ' +
  ItensTxt + '?') <> ID_YES then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    Controle := Geral.IMV(GridC.Cells[c, l]);
    if (Ativo = 1) and (Controle = 0) then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'GraGruX', 'GraGruX', 'Controle');
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO gragrux SET Ativo=:P0, GraGruC=:P1, ');
      Dmod.QrUpd.SQL.Add('GraGru1=:P2, GraTamI=:P3, Controle=:P4');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(GridX.Cells[0, l]);
      Dmod.QrUpd.Params[02].AsInteger := FNivel1;
      Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(GridX.Cells[c, 0]);
      Dmod.QrUpd.Params[04].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE gragrux SET Ativo=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  DmProd.AtualizaGradesAtivos1(FNivel1, GridA, GridC);
  Result := True;
  Screen.Cursor := crDefault;
end;
*)
end.
