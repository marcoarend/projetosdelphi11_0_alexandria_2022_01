unit Cadastro_Com_Itens_Nv2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCadastro_Com_Itens_Nv2 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBTopico: TdmkDBLookupComboBox;
    EdTopico: TdmkEditCB;
    Label1: TLabel;
    Sb_XYZ: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Qr_XYZ: TMySQLQuery;
    Qr_XYZCodigo: TIntegerField;
    Qr_XYZNome: TWideStringField;
    Ds_XYZ: TDataSource;
    Label2: TLabel;
    DBEdControle: TdmkDBEdit;
    DBEdCtrlNome: TDBEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Sb_XYZClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTop(Conta: Integer);
    procedure ReopenOVcYnsQstMag();
  public
    { Public declarations }
    FQrTop: TmySQLQuery;
    FDsCad, FDsCtx: TDataSource;
    FContexRef: Integer;
  end;

  var
  FmCadastro_Com_Itens_Nv2: TFmCadastro_Com_Itens_Nv2;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmCadastro_Com_Itens_Nv2.BtOKClick(Sender: TObject);
begin
{
  begin
    ReopenTop(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdTopico.ValueVariant     := 0;
      CBTopico.KeyValue         := Null;
      EdNome.ValueVariant       := '';
      //EdNome.SetFocus;
      EdTopico.SetFocus;
    end else Close;
  end;
}
end;

procedure TFmCadastro_Com_Itens_Nv2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCadastro_Com_Itens_Nv2.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCad;
  DBEdNome.DataSource     := FDsCad;
  //
  DBEdControle.DataSource := FDsCtx;
  DBEdCtrlNome.DataSource := FDsCtx;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmCadastro_Com_Itens_Nv2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenOVcYnsQstMag();
end;

procedure TFmCadastro_Com_Itens_Nv2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadastro_Com_Itens_Nv2.ReopenTop(Conta: Integer);
begin
  if FQrTop <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrTop, FQrTop.Database);
    //
    if Conta <> 0 then
      FQrTop.Locate('Conta', Conta, []);
  end;
end;

procedure TFmCadastro_Com_Itens_Nv2.ReopenOVcYnsQstMag();
begin
  UnDMkDAC_PF.AbreQuery(QrOVcYnsQstMag, DMod.MyDB);
end;

procedure TFmCadastro_Com_Itens_Nv2.ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
begin
end;

procedure TFmCadastro_Com_Itens_Nv2.Sb_XYZClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstTop();
  UMyMod.SetaCodigoPesquisado(EdTopico, CBTopico, QrOVcYnsQstTop, VAR_CADASTRO);
end;

end.
