object FmEntCliCad: TFmEntCliCad
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ????????????? ??????????? ????????????'
  ClientHeight = 461
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 347
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 347
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 347
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 620
          Height = 90
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 92
            Top = 8
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object Label4: TLabel
            Left = 8
            Top = 8
            Width = 68
            Height = 13
            Caption = 'Codigo SAP'#174':'
          end
          object Label2: TLabel
            Left = 8
            Top = 48
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label3: TLabel
            Left = 240
            Top = 48
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label5: TLabel
            Left = 272
            Top = 48
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object EdCliNome: TdmkEdit
            Left = 92
            Top = 24
            Width = 313
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdCliNomeChange
          end
          object EdCliCodi: TdmkEdit
            Left = 8
            Top = 24
            Width = 80
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdCliCodiChange
          end
          object EdCidade: TdmkEdit
            Left = 8
            Top = 64
            Width = 229
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdUF: TdmkEdit
            Left = 240
            Top = 64
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            MaxLength = 2
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdPais: TdmkEdit
            Left = 272
            Top = 64
            Width = 133
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Brasil'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Brasil'
          end
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 105
          Width = 620
          Height = 240
          Align = alClient
          DataSource = DsEntCliCad
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = dmkDBGridZTO1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'CliCodi'
              Title.Caption = 'Codigo SAP'#174
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliNome'
              Title.Caption = 'Nome'
              Width = 281
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cidade'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pais'
              Title.Caption = 'Pa'#237's'
              Width = 90
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 347
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 391
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'I&gnora'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Seleciona'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtInsUpd: TBitBtn
        Tag = 10
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Insere'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtInsUpdClick
      end
    end
  end
  object QrEntCliCad: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 136
    Top = 252
    object QrEntCliCadversao: TFloatField
      FieldName = 'versao'
    end
    object QrEntCliCadCliCodi: TWideStringField
      FieldName = 'CliCodi'
      Size = 60
    end
    object QrEntCliCadCliNome: TWideStringField
      FieldName = 'CliNome'
      Size = 255
    end
    object QrEntCliCadNomeUTF: TWideStringField
      FieldName = 'NomeUTF'
      Size = 255
    end
    object QrEntCliCadCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object QrEntCliCadUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrEntCliCadPais: TWideStringField
      FieldName = 'Pais'
      Size = 255
    end
    object QrEntCliCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEntCliCad: TDataSource
    DataSet = QrEntCliCad
    Left = 136
    Top = 300
  end
end
