unit TesteCores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  Vcl.Menus, UnDmkProcFunc,
  Math;   // Unit containing the min command

type
  TFmTesteCores = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PMOK: TPopupMenu;
    CMYKW1: TMenuItem;
    CMY1: TMenuItem;
    N1: TMenuItem;
    Importarcor1: TMenuItem;
    ColorDialog1: TColorDialog;
    Base1: TMenuItem;
    Corante11: TMenuItem;
    Corante21: TMenuItem;
    Corante31: TMenuItem;
    Corante41: TMenuItem;
    Amostra1: TMenuItem;
    Button1: TButton;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel2: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel6: TPanel;
    ShapeB: TShape;
    Label20: TLabel;
    LaBaseR: TLabel;
    Label22: TLabel;
    LaBaseG: TLabel;
    Label24: TLabel;
    LaBaseB: TLabel;
    Label27: TLabel;
    ShFundo: TShape;
    Label50: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    LaBaseHslL: TLabel;
    LaBaseHslS: TLabel;
    LaBaseHslH: TLabel;
    EdBasePerc: TdmkEdit;
    EdBaseC: TdmkEdit;
    EdBaseM: TdmkEdit;
    EdBaseY: TdmkEdit;
    EdBaseK: TdmkEdit;
    EdBaseW: TdmkEdit;
    EdMascC: TdmkEdit;
    EdMascM: TdmkEdit;
    EdMascY: TdmkEdit;
    EdMascK: TdmkEdit;
    EdMascW: TdmkEdit;
    Panel7: TPanel;
    Shape1: TShape;
    Label9: TLabel;
    LaCor1R: TLabel;
    Label10: TLabel;
    LaCor1G: TLabel;
    Label11: TLabel;
    LaCor1B: TLabel;
    Label29: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    ShapeMix1: TShape;
    Label44: TLabel;
    LaMix1R: TLabel;
    LaMix1G: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    LaMix1B: TLabel;
    EdCor1Perc: TdmkEdit;
    EdCor1C: TdmkEdit;
    EdCor1M: TdmkEdit;
    EdCor1Y: TdmkEdit;
    EdCor1K: TdmkEdit;
    EdCor1W: TdmkEdit;
    EdCor1Satu: TdmkEdit;
    EdMix1C: TdmkEdit;
    EdMix1M: TdmkEdit;
    EdMix1Y: TdmkEdit;
    EdMix1K: TdmkEdit;
    EdMix1W: TdmkEdit;
    Panel8: TPanel;
    Shape2: TShape;
    Label12: TLabel;
    LaCor2R: TLabel;
    Label13: TLabel;
    LaCor2G: TLabel;
    Label14: TLabel;
    LaCor2B: TLabel;
    Label31: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    ShapeMix2: TShape;
    Label45: TLabel;
    LaMix2R: TLabel;
    Label49: TLabel;
    LaMix2G: TLabel;
    Label51: TLabel;
    LaMix2B: TLabel;
    EdCor2Perc: TdmkEdit;
    EdCor2C: TdmkEdit;
    EdCor2M: TdmkEdit;
    EdCor2Y: TdmkEdit;
    EdCor2K: TdmkEdit;
    EdCor2W: TdmkEdit;
    EdCor2Satu: TdmkEdit;
    EdMix2C: TdmkEdit;
    EdMix2M: TdmkEdit;
    EdMix2Y: TdmkEdit;
    EdMix2K: TdmkEdit;
    EdMix2W: TdmkEdit;
    Panel9: TPanel;
    Shape3: TShape;
    Label15: TLabel;
    LaCor3R: TLabel;
    Label16: TLabel;
    LaCor3G: TLabel;
    Label17: TLabel;
    LaCor3B: TLabel;
    Label32: TLabel;
    Label39: TLabel;
    Label41: TLabel;
    EdCor3Perc: TdmkEdit;
    EdCor3C: TdmkEdit;
    EdCor3M: TdmkEdit;
    EdCor3Y: TdmkEdit;
    EdCor3K: TdmkEdit;
    EdCor3W: TdmkEdit;
    EdCor3Satu: TdmkEdit;
    Panel10: TPanel;
    Shape4: TShape;
    Label26: TLabel;
    LaCor4R: TLabel;
    Label28: TLabel;
    LaCor4G: TLabel;
    Label30: TLabel;
    LaCor4B: TLabel;
    Label33: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EdCor4Perc: TdmkEdit;
    EdCor4C: TdmkEdit;
    EdCor4M: TdmkEdit;
    EdCor4Y: TdmkEdit;
    EdCor4K: TdmkEdit;
    EdCor4W: TdmkEdit;
    EdCor4Satu: TdmkEdit;
    Panel11: TPanel;
    ShapeS: TShape;
    Label34: TLabel;
    LaSmplR: TLabel;
    Label36: TLabel;
    LaSmplG: TLabel;
    Label38: TLabel;
    LaSmplB: TLabel;
    Label40: TLabel;
    EdSmplPerc: TdmkEdit;
    EdSmplC: TdmkEdit;
    EdSmplM: TdmkEdit;
    EdSmplY: TdmkEdit;
    EdSmplK: TdmkEdit;
    EdSmplW: TdmkEdit;
    Panel13: TPanel;
    ShapeIOri: TShape;
    Label63: TLabel;
    ShapeICln: TShape;
    dmkEdit2: TdmkEdit;
    SGIOri: TStringGrid;
    SGICln: TStringGrid;
    Panel12: TPanel;
    LaMixB: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    LaMixG: TLabel;
    LaMixR: TLabel;
    Label6: TLabel;
    ShapeM: TShape;
    Label56: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label46: TLabel;
    Label21: TLabel;
    LaCor1Satu: TLabel;
    LaCor2Satu: TLabel;
    Label60: TLabel;
    LaCor1Redu: TLabel;
    LaCor2Redu: TLabel;
    LaTotaSatu: TLabel;
    LaTotaRedu: TLabel;
    ShLoadMid: TShape;
    ShLoadCln: TShape;
    ShLoadDrk: TShape;
    Label54: TLabel;
    Shape5: TShape;
    BtMatiz: TBitBtn;
    EdMixW: TdmkEdit;
    EdMixK: TdmkEdit;
    EdMixY: TdmkEdit;
    EdMixM: TdmkEdit;
    EdMixC: TdmkEdit;
    EdCor0Perc: TdmkEdit;
    EdCor1Matz: TdmkEdit;
    EdCor2Matz: TdmkEdit;
    EdCorTMatz: TdmkEdit;
    EdCorBMatiz: TdmkEdit;
    dmkEdit1: TdmkEdit;
    SGHSL: TStringGrid;
    SpeedButton1: TSpeedButton;
    Label55: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    LaRYBb: TLabel;
    LaRYBy: TLabel;
    LaRYBr: TLabel;
    Label59: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    LaRGBb: TLabel;
    LaRGBg: TLabel;
    LaRGBr: TLabel;
    BtMtzRYB: TBitBtn;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    LaMixRYB1B: TLabel;
    LaMixRYB1Y: TLabel;
    LaMixRYB1R: TLabel;
    Label70: TLabel;
    LaMixRYB2R: TLabel;
    Label72: TLabel;
    LaMixRYB2Y: TLabel;
    Label74: TLabel;
    LaMixRYB2B: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CMYKW1Click(Sender: TObject);
    procedure CMY1Click(Sender: TObject);
    procedure BtMatizClick(Sender: TObject);
    procedure Shape1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Shape2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Shape3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Base1Click(Sender: TObject);
    procedure Corante11Click(Sender: TObject);
    procedure Corante21Click(Sender: TObject);
    procedure Corante31Click(Sender: TObject);
    procedure Corante41Click(Sender: TObject);
    procedure Amostra1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdCor1MatzRedefinido(Sender: TObject);
    procedure EdCor2MatzRedefinido(Sender: TObject);
    procedure ShapeBMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Shape4MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ShapeSMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure ShapeMMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtMtzRYBClick(Sender: TObject);
    //
  private
    { Private declarations }
    FReadCor: String;
    FpPixel, FtPixel, FsPixel: Integer;
    //
    procedure DefineCorShape(Sender: TObject; LaCorR, LaCorG, LaCorB: TLabel);
    procedure DefineCorCMYK(Cor: TColor; EdCorC, EdCorM, EdCorY, EdCorK, EdCorW: TdmkEdit);
    function  GetMaxValFluOf3(A, B, C: Double): Double;
    function  GetRValue(rgb: DWORD): Byte;
    function  GetGValue(rgb: DWORD): Byte;
    function  GetBValue(rgb: DWORD): Byte;
    procedure ImportaCorDeBitmap(Shape: TShape; LaR, LaG, LaB: TLabel);
    procedure ImportaCorDeBitmap1(Shape: TShape; LaR, LaG, LaB: TLabel);
    procedure ImportaCorDeBitmap2((*Shape: TShape; LaRGB_R, LaRGB_G, LaRGB_B,
              LaCMYKW_C, LaCMYKW_M, LaCMYKW_Y, LaCMYKW_K, LaCMYKW_W,
              LaHSL_H, LaHSL_S, LaHSL_L: TLabel*));
    procedure RGB_to_CMY(const R, G, B: Double; var C, M, Y, K, W: Double);
    procedure RGB_to_CMYK(const R, G, B: Double; var C, M, Y, K, W: Double);
    procedure CMYK_to_RGB_Byte(const C, M, Y, K, W: Double; var R, G, B: Byte);
    procedure CMYK_to_RGB_Flut(const C, M, Y, K, W: Double; var R, G, B: Double);

    procedure RemoveBaseCor(EdBaseC, EdBaseM, EdBaseY, EdBaseK, EdBaseW,
              EdCor_C, EdCor_M, EdCor_Y, EdCor_K, EdCor_W,
              EdMix_C, EdMix_M, EdMix_Y, EdMix_K, EdMix_W: TDmkEdit;
              ShapeMix: TShape; LaR, LaG, LaB: TLabel;
              EdCorSatu, EdCorPerc: TdmkEdit);
    procedure CalculaReducaoMascara();
    procedure CalculaSaturacaoCor(EdCorSatu, EdCorPerc, EdCorMatz: TdmkEdit;
              LaCorSatu: TLabel);
    procedure CalculaTotalCorantes();
    procedure DiminuiValorNaoNegativo(EdCorX, EdMixX, EdMasc: TdmkEdit);
    procedure DefineRYB(LabelRGBr, LabelRGBg, LabelRGBb, LabelRYBr, LabelRYBy,
              LabelRYBb: TLabel);
  public
    { Public declarations }
  end;

  var
  FmTesteCores: TFmTesteCores;

implementation

uses UnMyObjects, ModuleGeral, AppCreate, Principal, UnDmkColor;

{$R *.DFM}

const
  cRowR = 0;
  cRowG = 1;
  cRowB = 2;

  cRowC = 3;
  cRowM = 4;
  cRowY = 5;
  cRowK = 6;
  cRowW = 7;

  cRowH = 8;
  cRowS = 9;
  cRowL = 10;

  cRovH = 11;
  cRovS = 12;
  cRovV = 13;

  cRovR = 14;
  cRovY = 15;
  cRovB = 16;

  cRoHd = 17;
  cRoHm = 18;
  cRoSd = 19;
  cRoSm = 20;
  cRoLd = 21;
  cRoLm = 22;

procedure TFmTesteCores.Amostra1Click(Sender: TObject);
begin
  ImportaCorDeBitmap(ShapeS, LaSmplR, LaSmplG, LaSmplB);
  DefineCorCMYK(ShapeS.Brush.Color, EdSmplC, EdSmplM, EdSmplY, EdSmplK, EdSmplW);
end;

procedure TFmTesteCores.Base1Click(Sender: TObject);
begin
  ImportaCorDeBitmap(ShapeB, LaBaseR, LaBaseG, LaBaseB);
  DefineCorCMYK(ShapeB.Brush.Color, EdBaseC, EdBaseM, EdBaseY, EdBaseK, EdBaseW);
end;

procedure TFmTesteCores.BtMatizClick(Sender: TObject);
var
  Matiz1, Matiz2: Double;
  MixC, MixM, MixY, MixK, MixW: Double;
  Mix1C, Mix1M, Mix1Y, Mix1K, Mix1W: Double;
  Mix2C, Mix2M, Mix2Y, Mix2K, Mix2W: Double;
  MixR, MixG, MixB: Byte;
  Mix0: TColor;
  H1, S1, L1, R, G, B: Integer;
  H2, S2, L2, R2, G2, B2: Double;
  RGBTriple: TRGBTriple;
begin
  DmkColor.RGBtoHSLRange(ShapeB.Brush.Color, H1, S1, L1);
  LaBaseHslH.Caption := Geral.FFT(H1, 2, siNegativo);
  LaBaseHslS.Caption := Geral.FFT(S1, 2, siNegativo);
  LaBaseHslL.Caption := Geral.FFT(L1, 2, siNegativo);
  //
  DmkColor.RGBtoHSLRange2(
    Geral.DMV(LaBaseR.Caption),
    Geral.DMV(LaBaseG.Caption),
    Geral.DMV(LaBaseB.Caption),
    H2, S2, L2);
  LaBaseHslH.Caption := Geral.FFT(H2, 2, siNegativo);
  LaBaseHslS.Caption := Geral.FFT(S2, 2, siNegativo);
  LaBaseHslL.Caption := Geral.FFT(L2, 2, siNegativo);
{  N�o gostei desta maneira
  //RGBTriple  := DmkColor.HSLToRGBTriple(H1, S1, L1);
  //Shape5.Brush.Color  := RGB(RGBTriple.rgbtRed, RGBTriple.rgbtGreen, RGBTriple.rgbtBlue);
}
  DmkColor.HSLtoRGB2(H2/239*360, S2 / 240 * 100, L2 / 240 * 100, R2, G2, B2);
  Shape5.Brush.Color := RGB(Round(R2), Round(G2), Round(B2));
  //
  CalculaReducaoMascara();
  //
{
  RemoveBaseCor(
    EdBaseC, EdBaseM, EdBaseY, EdBaseK, EdBaseW,
    EdCor1C, EdCor1M, EdCor1Y, EdCor1K, EdCor1W,
    EdMix1C, EdMix1M, EdMix1Y, EdMix1K, EdMix1W,
    ShapeMix1, LaCor1R, LaCor1G, LaCor1B, EdCor1Satu, EdCor1Perc);

  // Remover melhor!!
  RemoveBaseCor(EdBaseC, EdBaseM, EdBaseY, EdBaseK,
  EdBaseW, EdCor2C, EdCor2M, EdCor2Y, EdCor2K, EdCor2W, EdMix2C, EdMix2M,
  EdMix2Y, EdMix2K, EdMix2W, ShapeMix2, LaCor2R, LaCor2G, LaCor2B,
  EdCor2Satu, EdCor2Perc);

  // colocar base de volta!

  //MatizB := EdCorBMatiz.ValueVariant / EdCorBPerc.ValueVariant;
}
  Matiz1 := EdCor1Matz.ValueVariant / EdCor1Perc.ValueVariant;
  Matiz2 := EdCor2Matz.ValueVariant / EdCor2Perc.ValueVariant;
  //
  Mix1C := EdMix1C.ValueVariant;
  Mix1M := EdMix1M.ValueVariant;
  Mix1Y := EdMix1Y.ValueVariant;
  Mix1K := EdMix1K.ValueVariant;
  Mix1W := EdMix1W.ValueVariant;

  Mix2C := EdMix2C.ValueVariant;
  Mix2M := EdMix2M.ValueVariant;
  Mix2Y := EdMix2Y.ValueVariant;
  Mix2K := EdMix2K.ValueVariant;
  Mix2W := EdMix2W.ValueVariant;

  MixC := (Mix1C * Matiz1) + (Mix2C * Matiz2);
  MixM := (Mix1M * Matiz1) + (Mix2M * Matiz2);
  MixY := (Mix1Y * Matiz1) + (Mix2Y * Matiz2);
  MixK := (Mix1K * Matiz1) + (Mix2K * Matiz2);
  MixW := (Mix1W * Matiz1) + (Mix2K * Matiz2);
  //
  MixC := MixC + EdMascC.ValueVariant;
  MixM := MixM + EdMascM.ValueVariant;
  MixY := MixY + EdMascY.ValueVariant;
  MixK := MixK + EdMascK.ValueVariant;
  MixW := MixW + EdMascW.ValueVariant;
  //
  CMYK_to_RGB_Byte(MixC, MixM, MixY, MixK, MixW, MixR, MixG, MixB);
  Mix0 := RGB(MixR, MixG, MixB);
  //
  ShapeM.Brush.Color := Mix0;

  LaMixR.Caption := Geral.FFT(MixR, 2, siNegativo);
  LaMixG.Caption := Geral.FFT(MixG, 2, siNegativo);
  LaMixB.Caption := Geral.FFT(MixB, 2, siNegativo);

  EdMixC.ValueVariant := MixC;
  EdMixM.ValueVariant := MixM;
  EdMixY.ValueVariant := MixY;
  EdMixK.ValueVariant := MixK;
  EdMixW.ValueVariant := MixW;
  //
end;

procedure TFmTesteCores.BtMtzRYBClick(Sender: TObject);
var
  RGBr1, RGBg1, RGBb1, RGBr2, RGBg2, RGBb2,
  Perc1, Perc2,
  Frca1, Frca2,
  RGBr, RGBg, RGBb: Double;
begin
  RGBr1 := Geral.DMV(LaCor1R.Caption);
  RGBg1 := Geral.DMV(LaCor1G.Caption);
  RGBb1 := Geral.DMV(LaCor1B.Caption);
  RGBr2 := Geral.DMV(LaCor2R.Caption);
  RGBg2 := Geral.DMV(LaCor2G.Caption);
  RGBb2 := Geral.DMV(LaCor2B.Caption);
  Perc1 := EdCor1Matz.ValueVariant;
  Perc2 := EdCor1Matz.ValueVariant;
  Frca1 := 1;
  Frca2 := 1;
  //
  RGBr := 255.00;
  RGBg := 255.00;
  RGBb := 255.00;
  DmkColor.Matiz_Mistura2CoresRGB(
  RGBr1, RGBg1, RGBb1, RGBr2, RGBg2, RGBb2,
  Perc1, Perc2,
  Frca1, Frca2,
  RGBr, RGBg, RGBb);
  //
end;

procedure TFmTesteCores.BtOKClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOK, BtOK);
end;

procedure TFmTesteCores.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTesteCores.Button1Click(Sender: TObject);
var
  DPP: Double;
  Itens: ItensArray;
  Valrs: ValrsArray;
  RangeMin, RangeMid, RangeMax: Double;
begin
  DPP := dmkPF.DesvioPadraoPopulacionalLista([2, 4, 4, 4, 5, 5, 7, 9],
  RangeMin, RangeMid, RangeMax);
  Geral.MB_Info('Desvio padr�o Lista: ' + FloatToStr(DPP) + sLineBreak +
  'RangeMin:' + FloatToStr(RangeMin) + sLineBreak +
  'RangeMid:' + FloatToStr(RangeMid) + sLineBreak +
  'RangeMax:' + FloatToStr(RangeMax) + sLineBreak);
  SetLength(Itens, 5);
  SetLength(Valrs, 5);
  Itens[00] := 2;
  Itens[01] := 4;
  Itens[02] := 5;
  Itens[03] := 7;
  Itens[04] := 9;
  Valrs[00] := 1;
  Valrs[01] := 3;
  Valrs[02] := 2;
  Valrs[03] := 1;
  Valrs[04] := 1;
  DPP := dmkPF.DesvioPadraoPopulacionalArrIntDbl(
    Itens, Valrs, RangeMin, RangeMid, RangeMax);
  Geral.MB_Info('Desvio padr�o Array: ' + FloatToStr(DPP) + sLineBreak +
  'RangeMin:' + FloatToStr(RangeMin) + sLineBreak +
  'RangeMid:' + FloatToStr(RangeMid) + sLineBreak +
  'RangeMax:' + FloatToStr(RangeMax) + sLineBreak);
end;

procedure TFmTesteCores.CalculaReducaoMascara();
var
  Satu, Perc, Fator: Double;
  C, M, Y, K, W: Double;
  R, G, B: Double;
  H2, S2, L2, SatuMatz, SatuBase, R2, G2, B2: Double;
begin
  CalculaSaturacaoCor(EdCor1Satu, EdCor1Perc, EdCor1Matz, LaCor1Satu);
  CalculaSaturacaoCor(EdCor2Satu, EdCor2Perc, EdCor2Matz, LaCor2Satu);
  //
  H2 := Geral.DMV(LaBaseHslH.Caption);
  S2 := Geral.DMV(LaBaseHslS.Caption);
  L2 := Geral.DMV(LaBaseHslL.Caption);
  //
  SatuMatz := Geral.DMV(LaTotaSatu.Caption);
  if SatuMatz > 100 then
    SatuMatz := 100;
//  SatuBase := (100 - SatuMatz) * (240 - L2) / 100;
  SatuBase := (SatuMatz) * (240 - L2) / 100;
  L2 := L2 + SatuBase;
  if L2 > 240 then
    L2 := 240;
  DmkColor.HSLtoRGB2(H2/239*360, S2 / 240 * 100, L2 / 240 * 100, R2, G2, B2);
  ShFundo.Brush.Color := RGB(Round(R2), Round(G2), Round(B2));
  //
  DefineCorCMYK(ShFundo.Brush.Color, EdMascC, EdMascM, EdMascY, EdMascK, EdMascW);
  //
  DiminuiValorNaoNegativo(EdCor1C, EdMix1C, EdMascC);
  DiminuiValorNaoNegativo(EdCor1M, EdMix1M, EdMascM);
  DiminuiValorNaoNegativo(EdCor1Y, EdMix1Y, EdMascY);
  DiminuiValorNaoNegativo(EdCor1K, EdMix1K, EdMascK);
  DiminuiValorNaoNegativo(EdCor1W, EdMix1W, EdMascW);
  // Mostra Cor Corrigida
  CMYK_to_RGB_Flut(
    EdCor1C.ValueVariant, EdCor1M.ValueVariant, EdCor1Y.ValueVariant,
    EdCor1K.ValueVariant, EdCor1W.ValueVariant, R, G, B);
  ShapeMix1.Brush.Color := RGB(Round(R), Round(G), Round(B));
  LaMix1R.Caption := Geral.FFT(R, 2, siNegativo);
  LaMix1G.Caption := Geral.FFT(G, 2, siNegativo);
  LaMix1B.Caption := Geral.FFT(B, 2, siNegativo);
  //
  //
  DiminuiValorNaoNegativo(EdCor2C, EdMix2C, EdMascC);
  DiminuiValorNaoNegativo(EdCor2M, EdMix2M, EdMascM);
  DiminuiValorNaoNegativo(EdCor2Y, EdMix2Y, EdMascY);
  DiminuiValorNaoNegativo(EdCor2K, EdMix2K, EdMascK);
  DiminuiValorNaoNegativo(EdCor2W, EdMix2W, EdMascW);
  CMYK_to_RGB_Flut(
    EdCor2C.ValueVariant, EdCor2M.ValueVariant, EdCor2Y.ValueVariant,
    EdCor2K.ValueVariant, EdCor2W.ValueVariant, R, G, B);
  ShapeMix2.Brush.Color := RGB(Round(R), Round(G), Round(B));
  LaMix2R.Caption := Geral.FFT(R, 2, siNegativo);
  LaMix2G.Caption := Geral.FFT(G, 2, siNegativo);
  LaMix2B.Caption := Geral.FFT(B, 2, siNegativo);
  //
  //
end;

procedure TFmTesteCores.CalculaSaturacaoCor(EdCorSatu, EdCorPerc,
  EdCorMatz: TdmkEdit; LaCorSatu: TLabel);
var
  Satu, Perc, Matz, Valr, Tota: Double;
begin
  Satu := EdCorSatu.ValueVariant;
  Perc := EdCorPerc.ValueVariant;
  Matz := EdCorMatz.ValueVariant;
  Valr := 0;
  //
  if (Satu > 0) (*and (Perc > 0)*) and (Matz > 0) then
  begin
    Valr := Matz / Satu * 100;
  end;
  LaCorSatu.Caption := Geral.FFT(Valr, 2, siNegativo);
  //
  Tota :=
    Geral.DMV(LaCor1Satu.Caption) +
    Geral.DMV(LaCor2Satu.Caption);
  LaTotaSatu.Caption := Geral.FFT(Tota, 2, siNegativo);
end;

procedure TFmTesteCores.CalculaTotalCorantes();
begin
  EdCorTMatz.ValueVariant :=
  EdCor1Matz.ValueVariant +
  EdCor2Matz.ValueVariant;
end;

procedure TFmTesteCores.CMY1Click(Sender: TObject);
var
  C, M, Y, K, W: Integer;
  RGB: TRGBTriple;
begin
  K := 0;
  W := 0;

  RGB := DmkColor.ColorToRGBTriple(Shape1.Brush.Color);
  DmkColor.RGBTripleToCMYK(RGB, C, M, Y, K);
  EdCor1C.ValueVariant := C;
  EdCor1M.ValueVariant := M;
  EdCor1Y.ValueVariant := Y;
  EdCor1K.ValueVariant := K;
  EdCor1W.ValueVariant := W;
  //
  RGB := DmkColor.ColorToRGBTriple(Shape2.Brush.Color);
  DmkColor.RGBTripleToCMYK(RGB, C, M, Y, K);
  EdCor2C.ValueVariant := C;
  EdCor2M.ValueVariant := M;
  EdCor2Y.ValueVariant := Y;
  EdCor2K.ValueVariant := K;
  EdCor2W.ValueVariant := W;
  //
  RGB := DmkColor.ColorToRGBTriple(Shape3.Brush.Color);
  DmkColor.RGBTripleToCMYK(RGB, C, M, Y, K);
  EdCor3C.ValueVariant := C;
  EdCor3M.ValueVariant := M;
  EdCor3Y.ValueVariant := Y;
  EdCor3K.ValueVariant := K;
  EdCor3W.ValueVariant := W;
  //

end;

procedure TFmTesteCores.CMYKW1Click(Sender: TObject);
var
  C, M, Y, K, W: Double;
  RGB: TRGBTriple;
begin
  RGB := DmkColor.ColorToRGBTriple(Shape1.Brush.Color);
  DmkColor.RGBTripleToCMYKW_4(RGB, C, M, Y, K, W);
  EdCor1C.ValueVariant := C;
  EdCor1M.ValueVariant := M;
  EdCor1Y.ValueVariant := Y;
  EdCor1K.ValueVariant := K;
  EdCor1W.ValueVariant := W;
  //
  RGB := DmkColor.ColorToRGBTriple(Shape2.Brush.Color);
  DmkColor.RGBTripleToCMYKW_4(RGB, C, M, Y, K, W);
  EdCor2C.ValueVariant := C;
  EdCor2M.ValueVariant := M;
  EdCor2Y.ValueVariant := Y;
  EdCor2K.ValueVariant := K;
  EdCor2W.ValueVariant := W;
  //
  RGB := DmkColor.ColorToRGBTriple(Shape3.Brush.Color);
  DmkColor.RGBTripleToCMYKW_4(RGB, C, M, Y, K, W);
  EdCor3C.ValueVariant := C;
  EdCor3M.ValueVariant := M;
  EdCor3Y.ValueVariant := Y;
  EdCor3K.ValueVariant := K;
  EdCor3W.ValueVariant := W;
  //
end;

procedure TFmTesteCores.CMYK_to_RGB_Byte(const C, M, Y, K, W: Double; var R, G,
  B: Byte);
begin
   if (C + K) < 1 then
     R := 255 - Trunc(((C + K) * 255) + 0.5) else
     R := 0;
   if (M + K) < 1 then
     G := 255 - Trunc(((M + K) * 255) + 0.5) else
     G := 0;
   if (Y + K) < 1 then
     B := 255 - Trunc(((Y + K) * 255) + 0.5) else
     B := 0;
{
var
  Ci, Mi, Yi, Ki: Byte;
begin
  Ci := Trunc((C * 255) + 0.5);
  Mi := Trunc((M * 255) + 0.5);
  Yi := Trunc((Y * 255) + 0.5);
  Ki := Trunc((K * 255) + 0.5);
  MLAGeral.CMYKTORGB(Ci, Mi, Yi, Ki, R, G, B);
}
end;

procedure TFmTesteCores.CMYK_to_RGB_Flut(const C, M, Y, K, W: Double; var R, G,
  B: Double);
begin
   if (C + K) < 1 then
     R := 255 - ((C + K) * 255) else
     R := 0;
   if (M + K) < 1 then
     G := 255 - ((M + K) * 255) else
     G := 0;
   if (Y + K) < 1 then
     B := 255 - ((Y + K) * 255) else
     B := 0;
end;

procedure TFmTesteCores.Corante11Click(Sender: TObject);
begin
  ImportaCorDeBitmap(Shape1, LaCor1R, LaCor1G, LaCor1B);
  DefineCorCMYK(Shape1.Brush.Color, EdCor1C, EdCor1M, EdCor1Y, EdCor1K, EdCor1W);
end;

procedure TFmTesteCores.Corante21Click(Sender: TObject);
begin
  ImportaCorDeBitmap(Shape2, LaCor2R, LaCor2G, LaCor2B);
  DefineCorCMYK(Shape2.Brush.Color, EdCor2C, EdCor2M, EdCor2Y, EdCor2K, EdCor2W);
end;

procedure TFmTesteCores.Corante31Click(Sender: TObject);
begin
  ImportaCorDeBitmap(Shape3, LaCor3R, LaCor3G, LaCor3B);
  DefineCorCMYK(Shape3.Brush.Color, EdCor3C, EdCor3M, EdCor3Y, EdCor3K, EdCor3W);
end;

procedure TFmTesteCores.Corante41Click(Sender: TObject);
begin
  ImportaCorDeBitmap(Shape4, LaCor4R, LaCor4G, LaCor4B);
  DefineCorCMYK(Shape4.Brush.Color, EdCor4C, EdCor4M, EdCor4Y, EdCor4K, EdCor4W);
end;

procedure TFmTesteCores.DefineCorCMYK(Cor: TColor; EdCorC, EdCorM, EdCorY, EdCorK, EdCorW: TdmkEdit);
var
  C, M, Y, K, W: Double;
  RGB: TRGBTriple;
begin
  RGB := DmkColor.ColorToRGBTriple(Cor);
  //
  RGB_to_CMYK(RGB.rgbtRed, RGB.rgbtGreen, RGB.rgbtBlue, C, M, Y, K, W);
  //
  EdCorC.ValueVariant := C;
  EdCorM.ValueVariant := M;
  EdCorY.ValueVariant := Y;
  EdCorK.ValueVariant := K;
  EdCorW.ValueVariant := W;
end;

procedure TFmTesteCores.DefineCorShape(Sender: TObject; LaCorR, LaCorG, LaCorB: TLabel);
var
  Cor: TColor;
begin
  ColorDialog1.Color := TShape(Sender).Brush.Color;
  if ColorDialog1.Execute then
  begin
    Cor := ColorDialog1.Color;
    TShape(Sender).Brush.Color := Cor;
    LaCorR.Caption := Geral.FF0(GetRValue(Cor));
    LaCorG.Caption := Geral.FF0(GetGValue(Cor));
    LaCorB.Caption := Geral.FF0(GetBValue(Cor));
  end;
end;

procedure TFmTesteCores.DefineRYB(LabelRGBr, LabelRGBg, LabelRGBb, LabelRYBr,
  LabelRYBy, LabelRYBb: TLabel);
var
  RGBr, RGBg, RGBb, RYBr, RYBy, RYBb: Double;
begin
  RGBr := Geral.DMV(LabelRGBr.Caption);
  RGBg := Geral.DMV(LabelRGBg.Caption);
  RGBb := Geral.DMV(LabelRGBb.Caption);
  DmkColor.RGB2RYB(RGBr, RGBg, RGBb, RYBr, RYBy, RYBb);
  LabelRYBr.Caption := Geral.FFT(RYBr, 2, siNegativo);
  LabelRYBy.Caption := Geral.FFT(RYBy, 2, siNegativo);
  LabelRYBb.Caption := Geral.FFT(RYBb, 2, siNegativo);
end;

procedure TFmTesteCores.DiminuiValorNaoNegativo(EdCorX, EdMixX, EdMasc: TdmkEdit);
var
  CorX, MixX, Masc: Double;
begin
  CorX := EdCorX.ValueVariant;
  Masc := EdMasc.ValueVariant;
  //
  MixX := CorX - Masc;
  if MixX < 0 then
    MixX := 0;
  //
  EdMixX.ValueVariant := MixX;
end;

procedure TFmTesteCores.EdCor1MatzRedefinido(Sender: TObject);
begin
  CalculaTotalCorantes();
end;

procedure TFmTesteCores.EdCor2MatzRedefinido(Sender: TObject);
begin
  CalculaTotalCorantes();
end;

procedure TFmTesteCores.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTesteCores.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  SGHSL.Cells[0, 0] := 'Valor';
  SGHSL.Cells[1, 0] := 'Hue (Matiz)';
  SGHSL.Cells[2, 0] := 'Staura��o';
  SGHSL.Cells[3, 0] := 'Luminosidade';
  for I := 1 to 241 do
      SGHSL.Cells[0, I] := Geral.FF0(I-1);
  //
  SGICln.Cells[0, cRowR] := 'R';
  SGICln.Cells[0, cRowG] := 'G';
  SGICln.Cells[0, cRowB] := 'B';

  SGICln.Cells[0, cRowC] := 'C';
  SGICln.Cells[0, cRowM] := 'M';
  SGICln.Cells[0, cRowY] := 'Y';
  SGICln.Cells[0, cRowK] := 'K';
  SGICln.Cells[0, cRowW] := 'W';

  SGICln.Cells[0, cRowH] := 'H';
  SGICln.Cells[0, cRowS] := 'S';
  SGICln.Cells[0, cRowL] := 'L';

  SGICln.Cells[0, cRovH] := 'H';
  SGICln.Cells[0, cRovS] := 'S';
  SGICln.Cells[0, cRovV] := 'V';

  SGICln.Cells[0, cRovR] := 'R';
  SGICln.Cells[0, cRovY] := 'Y';
  SGICln.Cells[0, cRovB] := 'B';

{
  SGICln.Cells[0, cRoHd] := 'Hd';
  SGICln.Cells[0, cRoHm] := 'Hm';
  SGICln.Cells[0, cRoSd] := 'Sd';
  SGICln.Cells[0, cRoSm] := 'Sm';
  SGICln.Cells[0, cRoLd] := 'Ld';
  SGICln.Cells[0, cRoLm] := 'Lm';
}
  //

  SGIOri.Cells[0, cRowR] := 'R';
  SGIOri.Cells[0, cRowG] := 'G';
  SGIOri.Cells[0, cRowB] := 'B';

  SGIOri.Cells[0, cRowC] := 'C';
  SGIOri.Cells[0, cRowM] := 'M';
  SGIOri.Cells[0, cRowY] := 'Y';
  SGIOri.Cells[0, cRowK] := 'K';
  SGIOri.Cells[0, cRowW] := 'W';

  SGIOri.Cells[0, cRowH] := 'H';
  SGIOri.Cells[0, cRowS] := 'S';
  SGIOri.Cells[0, cRowL] := 'L';

  SGIOri.Cells[0, cRovH] := 'H';
  SGIOri.Cells[0, cRovS] := 'S';
  SGIOri.Cells[0, cRovV] := 'V';

  SGIOri.Cells[0, cRovR] := 'R';
  SGIOri.Cells[0, cRovY] := 'Y';
  SGIOri.Cells[0, cRovB] := 'B';

  SGIOri.Cells[0, cRoHd] := 'Hd';
  SGIOri.Cells[0, cRoHm] := 'Hm';
  SGIOri.Cells[0, cRoSd] := 'Sd';
  SGIOri.Cells[0, cRoSm] := 'Sm';
  SGIOri.Cells[0, cRoLd] := 'Ld';
  SGIOri.Cells[0, cRoLm] := 'Lm';
  //
end;

procedure TFmTesteCores.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTesteCores.FormShow(Sender: TObject);
begin
  DefineCorCMYK(ShapeS.Brush.Color, EdSmplC, EdSmplM, EdSmplY, EdSmplK, EdSmplW);
  DefineCorCMYK(ShapeB.Brush.Color, EdBaseC, EdBaseM, EdBaseY, EdBaseK, EdBaseW);
  DefineCorCMYK(Shape1.Brush.Color, EdCor1C, EdCor1M, EdCor1Y, EdCor1K, EdCor1W);
  DefineCorCMYK(Shape2.Brush.Color, EdCor2C, EdCor2M, EdCor2Y, EdCor2K, EdCor2W);
  DefineCorCMYK(Shape3.Brush.Color, EdCor3C, EdCor3M, EdCor3Y, EdCor3K, EdCor3W);
  DefineCorCMYK(Shape4.Brush.Color, EdCor4C, EdCor4M, EdCor4Y, EdCor4K, EdCor4W);
  //
  DefineRYB(LaCor1R, LaCor1G, LaCor1B, LaMixRYB1R, LaMixRYB1Y, LaMixRYB1B);
  DefineRYB(LaCor2R, LaCor2G, LaCor2B, LaMixRYB2R, LaMixRYB2Y, LaMixRYB2B);
end;

function TFmTesteCores.GetBValue(rgb: DWORD): Byte;
begin
  Result := Byte(rgb shr 16);
end;

function TFmTesteCores.GetGValue(rgb: DWORD): Byte;
begin
  Result := Byte(rgb shr 8);
end;

function TFmTesteCores.GetMaxValFluOf3(A, B, C: Double): Double;
begin
  Result := A;
  if B > Result then
    Result := B;
  if C > Result then
    Result := C;
end;

function TFmTesteCores.GetRValue(rgb: DWORD): Byte;
begin
  Result := Byte(rgb);
end;

procedure TFmTesteCores.ImportaCorDeBitmap(Shape: TShape; LaR, LaG, LaB: TLabel);
var
  R2, G2, B2: Double;
begin
  ImportaCorDeBitmap2();
end;

procedure TFmTesteCores.ImportaCorDeBitmap1(Shape: TShape; LaR, LaG,
  LaB: TLabel);
var
  Arquivo: String;
  Filter: String;
  Bitmap: TBitmap;
  Rs, Gs, Bs: Int64;
  Rm, Gm, Bm,
  C, M, Y, K, W: Double;
  //
  A, B: Integer;
  Cor: TColor;
begin
  Filter  := '';
  MyObjects.DefineArquivoImagem(Arquivo, Filter);
  //
  Screen.Cursor := crHourGLass;
  try
    Rs := 0;
    Gs := 0;
    Bs := 0;
    //
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromFile(Arquivo);
    //
    FtPixel := Bitmap.Width * Bitmap.Height;
    FpPixel := 0;
    for A := 0 to Bitmap.Width-1 do
    begin
      for B := 0 to Bitmap.Height-1 do
      begin
        FpPixel := FpPixel + 1;
        //
        Cor := Bitmap.Canvas.Pixels[A, B];
        Rs := Rs + GetRValue(Cor);
        Gs := Gs + GetGValue(Cor);
        Bs := Bs + GetBValue(Cor);
        //
        if (FpPixel / 100000) = (FpPixel div 100000) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lido '  +
          Geral.FF0(FpPixel) + ' de ' + Geral.FF0(FtPixel) + ' (' +
          Geral.FFT(FpPixel / FtPixel * 100, 2, siPositivo) + '%)');
        end;
      end;
    end;
    Rm := Rs / FtPixel;
    Gm := Gs / FtPixel;
    Bm := Bs / FtPixel;
    //
    Shape.Brush.Color := RGB(Trunc(Rm + 0.5), Trunc(Gm + 0.5), Trunc(Bm + 0.5));
    LaR.Caption := Geral.FFT(Rm, 2, siNegativo);
    LaG.Caption := Geral.FFT(Gm, 2, siNegativo);
    LaB.Caption := Geral.FFT(Bm, 2, siNegativo);
    //
    RGB_to_CMYK(Rm, Gm, Bm, C, M, Y, K, W);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTesteCores.ImportaCorDeBitmap2((*Shape: TShape; LaRGB_R, LaRGB_G,
  LaRGB_B, LaCMYKW_C, LaCMYKW_M, LaCMYKW_Y, LaCMYKW_K, LaCMYKW_W,
  LaHSL_H, LaHSL_S, LaHSL_L: TLabel*));
var
  Arquivo: String;
  Filter: String;
  Bitmap: TBitmap;
  I, R, G, B, H1, S1, L1: Integer;
  Rs, Gs, Bs: Int64;
  Rm, Gm, Bm,
  R2, G2, B2,
  C2, M2, Y2, K2, W2, DPPh, DPPs, DPPl: Double;
  //
  AxX, AxY, N: Integer;
  CorPxl, CorClr: TColor;
  Hue: array[0..239] of Integer;
  Sat, Lum: array[0..240] of Integer;
  H2, S2, L2: Double;
  ArrHueItens, ArrSatItens, ArrLumItens: ItensArray;
  ArrHueValrs, ArrSatValrs, ArrLumValrs: ValrsArray;
  RangeMinH, RangeMidH, RangeMaxH,
  RangeMinS, RangeMidS, RangeMaxS,
  RangeMinL, RangeMidL, RangeMaxL: Double;
  SumH2, SumS2, SumL2: Extended;
  MyBMP: TBitmap;
  HSV: THSV;
  (*RGBr, RGBg, RGBb,*) RYBr, RYBy, RYBb: Double;
begin
  //FReadCor :=
    //UnAppCreate.RecriaTempTableNovo(ntrtt_ReadCor, DModG.QrUpdPID1, False);
  //
  Filter  := '';
  if not MyObjects.DefineArquivoImagem(Arquivo, Filter) then Exit;
  //
  Screen.Cursor := crHourGLass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lendo Arquivo');
    Rs := 0;
    Gs := 0;
    Bs := 0;
    for I := Low(Hue) to High(Hue) do
    begin
      Hue[I] := 0;
      Sat[I] := 0;
      Lum[I] := 0;
    end;
    Sat[240] := 0;
    Lum[240] := 0;
    //
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromFile(Arquivo);
    //
    FtPixel := Bitmap.Width * Bitmap.Height;
    FpPixel := 0;
    //
    for AxX := 0 to Bitmap.Width-1 do
    begin
      for AxY := 0 to Bitmap.Height-1 do
      begin
        FpPixel := FpPixel + 1;
        //
        CorPxl := Bitmap.Canvas.Pixels[AxX, AxY];
        R := GetRValue(CorPxl);
        G := GetGValue(CorPxl);
        B := GetBValue(CorPxl);
        //
        Rs := Rs + R;
        Gs := Gs + G;
        Bs := Bs + B;
        //
        if (FpPixel / 100000) = (FpPixel div 100000) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lido '  +
          Geral.FF0(FpPixel) + ' de ' + Geral.FF0(FtPixel) + ' (' +
          Geral.FFT(FpPixel / FtPixel * 100, 2, siPositivo) + '%)');
        end;
        //
        DmkColor.RGBtoHSLRange2(R, G, B, H2, S2, L2);
        H1 := Round(H2);
        S1 := Round(S2);
        L1 := Round(L2);
        if H1 = 240 then
          H1 := 0;
        Hue[H1] := Hue[H1] + 1;
        Sat[S1] := Sat[S1] + 1;
        Lum[L1] := Lum[L1] + 1;
      end;
    end;
    Rm := Rs / FtPixel;
    Gm := Gs / FtPixel;
    Bm := Bs / FtPixel;
    //
    ShapeIOri.Brush.Color := RGB(Trunc(Rm + 0.5), Trunc(Gm + 0.5), Trunc(Bm + 0.5));
    {
    LaR.Caption := Geral.FFT(Rm, 2, siNegativo);
    LaG.Caption := Geral.FFT(Gm, 2, siNegativo);
    LaB.Caption := Geral.FFT(Bm, 2, siNegativo);
    }
    RGB_to_CMYK(Rm, Gm, Bm, C2, M2, Y2, K2, W2);
    DmkColor.RGBtoHSLRange2(Rm, Gm, Bm, H2, S2, L2);
    HSV := DmkColor.RGB2HSV(Round(Rm), Round(Gm), Round(Bm));
    DmkColor.RGB2RYB(Rm, Gm, Bm, RYBr, RYBy, RYBb);
    //
    SGIOri.Cells[1, cRowR] := Geral.FFT(Rm, 2, siNegativo);
    SGIOri.Cells[1, cRowG] := Geral.FFT(Gm, 2, siNegativo);
    SGIOri.Cells[1, cRowB] := Geral.FFT(Bm, 2, siNegativo);

    SGIOri.Cells[1, cRowC] := Geral.FFT(C2, 4, siNegativo);
    SGIOri.Cells[1, cRowM] := Geral.FFT(M2, 4, siNegativo);
    SGIOri.Cells[1, cRowY] := Geral.FFT(Y2, 4, siNegativo);
    SGIOri.Cells[1, cRowK] := Geral.FFT(K2, 4, siNegativo);
    SGIOri.Cells[1, cRowW] := Geral.FFT(W2, 4, siNegativo);

    SGIOri.Cells[1, cRowH] := Geral.FFT(H2, 2, siNegativo);
    SGIOri.Cells[1, cRowS] := Geral.FFT(S2, 2, siNegativo);
    SGIOri.Cells[1, cRowL] := Geral.FFT(L2, 2, siNegativo);

    SGIOri.Cells[1, cRovH] := Geral.FFT(HSV.Hue, 2, siNegativo);
    SGIOri.Cells[1, cRovS] := Geral.FFT(HSV.Sat, 2, siNegativo);
    SGIOri.Cells[1, cRovV] := Geral.FFT(HSV.Val, 2, siNegativo);

    SGIOri.Cells[1, cRovR] := Geral.FFT(RYBr, 2, siNegativo);
    SGIOri.Cells[1, cRovY] := Geral.FFT(RYBy, 2, siNegativo);
    SGIOri.Cells[1, cRovB] := Geral.FFT(RYBb, 2, siNegativo);

    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo range do Hue (Matiz)');
    N := 0;
    SetLength(ArrHueItens, N);
    SetLength(ArrHueValrs, N);
    for I := Low(Hue) to High(Hue) do
    begin
      if Hue[I] > 0 then
      begin
        N := N + 1;
        SetLength(ArrHueItens, N);
        SetLength(ArrHueValrs, N);
        ArrHueItens[N - 1] := I;
        ArrHueValrs[N - 1] := Hue[I];
      end;
    end;
    DPPh := dmkPF.DesvioPadraoPopulacionalArrIntDbl(ArrHueItens, ArrHueValrs,
    RangeMinH, RangeMidH, RangeMaxH);
{
    Geral.MB_Info('Desvio padr�o Hue (Matiz): ' + FloatToStr(DPPh) + sLineBreak +
      'RangeMin:' + FloatToStr(RangeMinH) + sLineBreak +
      'RangeMid:' + FloatToStr(RangeMidH) + sLineBreak +
      'RangeMax:' + FloatToStr(RangeMaxH) + sLineBreak);
}
    for I := Low(ArrHueItens) to High(ArrHueItens) do
      SGHSL.Cells[1, ArrHueItens[I] + 1] := Geral.FFT(ArrHueValrs[I], 2, siNegativo);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo range da Satura��o');
    N := 0;
    SetLength(ArrSatItens, N);
    SetLength(ArrSatValrs, N);
    for I := Low(Sat) to High(Sat) do
    begin
      if Sat[I] > 0 then
      begin
        N := N + 1;
        SetLength(ArrSatItens, N);
        SetLength(ArrSatValrs, N);
        ArrSatItens[N - 1] := I;
        ArrSatValrs[N - 1] := Sat[I];
      end;
    end;
    DPPs := dmkPF.DesvioPadraoPopulacionalArrIntDbl(ArrSatItens, ArrSatValrs,
    RangeMinS, RangeMidS, RangeMaxS);
{
    Geral.MB_Info('Desvio padr�o Satura��o: ' + FloatToStr(DPPs) + sLineBreak +
      'RangeMin:' + FloatToStr(RangeMinS) + sLineBreak +
      'RangeMid:' + FloatToStr(RangeMidS) + sLineBreak +
      'RangeMax:' + FloatToStr(RangeMaxS) + sLineBreak);
}
    for I := Low(ArrSatItens) to High(ArrSatItens) do
      SGHSL.Cells[2, ArrSatItens[I] + 1] := Geral.FFT(ArrSatValrs[I], 2, siNegativo);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo range do Luminosidade');
    N := 0;
    SetLength(ArrLumItens, N);
    SetLength(ArrLumValrs, N);
    for I := Low(Lum) to High(Lum) do
    begin
      if Lum[I] > 0 then
      begin
        N := N + 1;
        SetLength(ArrLumItens, N);
        SetLength(ArrLumValrs, N);
        ArrLumItens[N - 1] := I;
        ArrLumValrs[N - 1] := Lum[I];
      end;
    end;
    DPPl := dmkPF.DesvioPadraoPopulacionalArrIntDbl(ArrLumItens, ArrLumValrs,
    RangeMinL, RangeMidL, RangeMaxL);
{
    Geral.MB_Info('Desvio padr�o da Luminosidade: ' + FloatToStr(DPPl) + sLineBreak +
      'RangeMin:' + FloatToStr(RangeMinL) + sLineBreak +
      'RangeMid:' + FloatToStr(RangeMidL) + sLineBreak +
      'RangeMax:' + FloatToStr(RangeMaxL) + sLineBreak);
}
    for I := Low(ArrLumItens) to High(ArrLumItens) do
      SGHSL.Cells[3, ArrLumItens[I] + 1] := Geral.FFT(ArrLumValrs[I], 2, siNegativo);
    //
    SGIOri.Cells[1, cRoHd] := Geral.FFT(DPPh, 2, siNegativo);
    SGIOri.Cells[1, cRoHm] := Geral.FFT(RangeMidH, 2, siNegativo);
    SGIOri.Cells[1, cRoSd] := Geral.FFT(DPPs, 2, siNegativo);
    SGIOri.Cells[1, cRoSm] := Geral.FFT(RangeMidS, 2, siNegativo);
    SGIOri.Cells[1, cRoLd] := Geral.FFT(DPPl, 2, siNegativo);
    SGIOri.Cells[1, cRoLm] := Geral.FFT(RangeMidL, 2, siNegativo);
    //
////////////////////////////////////////////////////////////////////////////////
    //
    Rs := 0;
    Gs := 0;
    Bs := 0;
    FsPixel := 0;
    FpPixel := 0;
    for AxX := 0 to Bitmap.Width-1 do
    begin
      for AxY := 0 to Bitmap.Height-1 do
      begin
        FpPixel := FpPixel + 1;
        //
        CorPxl := Bitmap.Canvas.Pixels[AxX, AxY];
        R := GetRValue(CorPxl);
        G := GetGValue(CorPxl);
        B := GetBValue(CorPxl);
        //
        DmkColor.RGBtoHSLRange2(R, G, B, H2, S2, L2);
        if  (H2 >= RangeMinH) and (H2 <= RangeMaxH)
        and (S2 >= RangeMinS) and (S2 <= RangeMaxS)
        and (L2 >= RangeMinL) and (L2 <= RangeMaxL) then
        begin
          FsPixel := FsPixel + 1;
          SumH2 := SumH2 + H2;
          SumS2 := SumS2 + S2;
          SumL2 := SumL2 + L2;
        end;
        //
        if (FpPixel / 100000) = (FpPixel div 100000) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lido '  +
          Geral.FF0(FpPixel) + ' de ' + Geral.FF0(FtPixel) + ' (' +
          Geral.FFT(FpPixel / FtPixel * 100, 2, siPositivo) + '%)');
        end;
      end;
    end;
    H2 := SumH2 / FsPixel;
    S2 := SumS2 / FsPixel;
    L2 := SumL2 / FsPixel;
    //
    DmkColor.HSLtoRGB2(H2/239*360, S2 / 240 * 100, L2 / 240 * 100, R2, G2, B2);
    CorClr := RGB(Trunc(R2 + 0.5), Trunc(G2 + 0.5), Trunc(B2 + 0.5));
    ShapeICln.Brush.Color := CorClr;
    RGB_to_CMYK(R2, G2, B2, C2, M2, Y2, K2, W2);
    HSV := DmkColor.RGB2HSV(Round(R2), Round(G2), Round(B2));
    DmkColor.RGB2RYB(R2, G2, B2, RYBr, RYBy, RYBb);
    //
    SGICln.Cells[1, cRowR] := Geral.FFT(R2, 2, siNegativo);
    SGICln.Cells[1, cRowG] := Geral.FFT(G2, 2, siNegativo);
    SGICln.Cells[1, cRowB] := Geral.FFT(B2, 2, siNegativo);

    SGICln.Cells[1, cRowC] := Geral.FFT(C2, 4, siNegativo);
    SGICln.Cells[1, cRowM] := Geral.FFT(M2, 4, siNegativo);
    SGICln.Cells[1, cRowY] := Geral.FFT(Y2, 4, siNegativo);
    SGICln.Cells[1, cRowK] := Geral.FFT(K2, 4, siNegativo);
    SGICln.Cells[1, cRowW] := Geral.FFT(W2, 4, siNegativo);

    SGICln.Cells[1, cRowH] := Geral.FFT(H2, 2, siNegativo);
    SGICln.Cells[1, cRowS] := Geral.FFT(S2, 2, siNegativo);
    SGICln.Cells[1, cRowL] := Geral.FFT(L2, 2, siNegativo);

    SGICln.Cells[1, cRovH] := Geral.FFT(HSV.Hue, 2, siNegativo);
    SGICln.Cells[1, cRovS] := Geral.FFT(HSV.Sat, 2, siNegativo);
    SGICln.Cells[1, cRovV] := Geral.FFT(HSV.Val, 2, siNegativo);

    SGICln.Cells[1, cRovR] := Geral.FFT(RYBr, 2, siNegativo);
    SGICln.Cells[1, cRovY] := Geral.FFT(RYBy, 2, siNegativo);
    SGICln.Cells[1, cRovB] := Geral.FFT(RYBb, 2, siNegativo);
{
    SGICln.Cells[1, cRoHd] := Geral.FFT(DPPh, 2, siNegativo);
    SGICln.Cells[1, cRoHm] := Geral.FFT(RangeMidH, 2, siNegativo);
    SGICln.Cells[1, cRoSd] := Geral.FFT(DPPs, 2, siNegativo);
    SGICln.Cells[1, cRoSm] := Geral.FFT(RangeMidS, 2, siNegativo);
    SGICln.Cells[1, cRoLd] := Geral.FFT(DPPl, 2, siNegativo);
    SGICln.Cells[1, cRoLm] := Geral.FFT(RangeMidL, 2, siNegativo);
}
    //
////////////////////////////////////////////////////////////////////////////////
    //
    MyBMP := TBitmap.Create;
    MyBMP.Height := Bitmap.Height;
    MyBMP.Width := Bitmap.Width;
    FpPixel := 0;
    for AxX := 0 to Bitmap.Width-1 do
    begin
      for AxY := 0 to Bitmap.Height-1 do
      begin
        FpPixel := FpPixel + 1;
        CorPxl := Bitmap.Canvas.Pixels[AxX, AxY];
        R := GetRValue(CorPxl);
        G := GetGValue(CorPxl);
        B := GetBValue(CorPxl);
        //
        DmkColor.RGBtoHSLRange2(R, G, B, H2, S2, L2);
        if (H2 < RangeMinH) or (H2 > RangeMaxH)
        or (S2 < RangeMinS) or (S2 > RangeMaxS)
        or (L2 < RangeMinL) or (L2 > RangeMaxL) then
          MyBMP.Canvas.Pixels[AxX, AxY] := CorClr
        else
          MyBMP.Canvas.Pixels[AxX, AxY] := CorPxl;
        //
        if (FpPixel / 100000) = (FpPixel div 100000) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lido '  +
          Geral.FF0(FpPixel) + ' de ' + Geral.FF0(FtPixel) + ' (' +
          Geral.FFT(FpPixel / FtPixel * 100, 2, siPositivo) + '%)');
        end;
      end;
    end;
    FmPrincipal.Image1.Picture.Bitmap.Assign(MyBMP);
    //
////////////////////////////////////////////////////////////////////////////////
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTesteCores.RemoveBaseCor(EdBaseC, EdBaseM, EdBaseY, EdBaseK,
  EdBaseW, EdCor_C, EdCor_M, EdCor_Y, EdCor_K, EdCor_W, EdMix_C, EdMix_M,
  EdMix_Y, EdMix_K, EdMix_W: TDmkEdit; ShapeMix: TShape; LaR, LaG, LaB: TLabel;
  EdCorSatu, EdCorPerc: TdmkEdit);
var
  Satu, Perc, Fator: Double;
  C, M, Y, K, W: Double;
  R, G, B: Double;
begin
  Satu := EdCorSatu.ValueVariant;
  Perc := EdCorPerc.ValueVariant;
  //
  if (Satu > 0) and (Perc < Satu) then
    Fator := (Satu - Perc) / Satu;
  //
  C := EdCor_C.ValueVariant - (EdBaseC.ValueVariant * Fator);
  M := EdCor_M.ValueVariant - (EdBaseM.ValueVariant * Fator);
  Y := EdCor_Y.ValueVariant - (EdBaseY.ValueVariant * Fator);
  K := EdCor_K.ValueVariant - (EdBaseK.ValueVariant * Fator);
  W := EdCor_W.ValueVariant - (EdBaseW.ValueVariant * Fator);
  //
  EdMix_C.ValueVariant := C;
  EdMix_M.ValueVariant := M;
  EdMix_Y.ValueVariant := Y;
  EdMix_K.ValueVariant := K;
  EdMix_W.ValueVariant := W;
  //
  CMYK_to_RGB_Flut(C, M, Y, K, W, R, G, B);
  //
  LaR.Caption := Geral.FFT(R, 2, siNegativo);
  LaG.Caption := Geral.FFT(G, 2, siNegativo);
  LaB.Caption := Geral.FFT(B, 2, siNegativo);
  //
  ShapeMix.Brush.Color := RGB(Trunc(R + 0.5), Trunc(G + 0.5), Trunc(B + 0.5));
end;

procedure TFmTesteCores.RGB_to_CMY(const R, G, B: Double; var C, M, Y,
  K, W: Double);
var
  Rn, Gn, Bn: Double;
begin
  Rn := R / 255;
  Gn := G / 255;
  Bn := B / 255;
  //
  //Black   = Minimum(1-Red,1-Green,1-Blue)
  //K  := 1 - GetMaxValFluOf3(Rn, Gn, Bn);
  K := 0;
  //Cyan    = (1-Red-Black)/(1-Black)
  C  := (1 - Rn - K) / (1 - K);
  //Magenta = (1-Green-Black)/(1-Black)
  M  := (1 - Gn - K) / (1 - K);
  //Yellow  = (1-Blue-Black)/(1-Black)
  Y  := (1 - Bn - K) / (1 - K);
  //
  W := 0;
end;

procedure TFmTesteCores.RGB_to_CMYK(const R, G, B: Double; var C, M, Y,
  K, W: Double);
var
  Rn, Gn, Bn: Double;
begin
  Rn := R / 255;
  Gn := G / 255;
  Bn := B / 255;
  //
  //Black   = Minimum(1-Red,1-Green,1-Blue)
  K  := 1 - GetMaxValFluOf3(Rn, Gn, Bn);
  //Cyan    = (1-Red-Black)/(1-Black)
  C  := (1 - Rn - K) / (1 - K);
  //Magenta = (1-Green-Black)/(1-Black)
  M  := (1 - Gn - K) / (1 - K);
  //Yellow  = (1-Blue-Black)/(1-Black)
  Y  := (1 - Bn - K) / (1 - K);
  //
  W := 0;
end;

procedure TFmTesteCores.Shape1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaCor1R, LaCor1G, LaCor1B);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdCor1C, EdCor1M, EdCor1Y, EdCor1K, EdCor1W);
  DefineRYB(LaCor1R, LaCor1G, LaCor1B, LaMixRYB1R, LaMixRYB1Y, LaMixRYB1B);
end;

procedure TFmTesteCores.Shape2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaCor2R, LaCor2G, LaCor2B);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdCor2C, EdCor2M, EdCor2Y, EdCor2K, EdCor2W);
  DefineRYB(LaCor2R, LaCor2G, LaCor2B, LaMixRYB2R, LaMixRYB2Y, LaMixRYB2B);
end;

procedure TFmTesteCores.Shape3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaCor3R, LaCor3G, LaCor3B);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdCor3C, EdCor3M, EdCor3Y, EdCor3K, EdCor3W);
end;

procedure TFmTesteCores.Shape4MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaCor4R, LaCor4G, LaCor4B);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdCor4C, EdCor4M, EdCor4Y, EdCor4K, EdCor4W);
end;

procedure TFmTesteCores.ShapeBMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaBaseR, LaBaseG, LaBaseB);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdBaseC, EdBaseM, EdBaseY, EdBaseK, EdBaseW);
end;

procedure TFmTesteCores.ShapeMMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaMixR, LaMixG, LaMixB);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdMixC, EdMixM, EdMixY, EdMixK, EdMixW);
end;

procedure TFmTesteCores.ShapeSMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DefineCorShape(Sender, LaSmplR, LaSmplG, LaSmplB);
  DefineCorCMYK(TShape(Sender).Brush.Color,
    EdSmplC, EdSmplM, EdSmplY, EdSmplK, EdSmplW);
end;

procedure TFmTesteCores.SpeedButton1Click(Sender: TObject);
var
  Cor: TColor;
  RGBr, RGBg, RGBb,
  RYBr, RYBy, RYBb: Double;
begin
  Cor := Shape5.Brush.Color;
  RGBr := GetRValue(Cor);
  RGBg := GetGValue(Cor);
  RGBb := GetBValue(Cor);
  //
  DmkColor.RGB2RYB(RGBr, RGBg, RGBb, RYBr, RYBy, RYBb);
  //
  LaRYBr.Caption := Geral.FFT(RYBr, 2, siNegativo);
  LaRYBy.Caption := Geral.FFT(RYBy, 2, siNegativo);
  LaRYBb.Caption := Geral.FFT(RYBb, 2, siNegativo);
  //
  DmkColor.RYB2RGB(RYBr, RYBy, RYBb, RGBr, RGBg, RGBb);
  //
  LaRGBr.Caption := Geral.FFT(RGBr, 2, siNegativo);
  LaRGBg.Caption := Geral.FFT(RGBg, 2, siNegativo);
  LaRGBb.Caption := Geral.FFT(RGBb, 2, siNegativo);
  //
end;

end.
