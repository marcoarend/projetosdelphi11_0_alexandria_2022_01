object FmStringGrid_UserDraw: TFmStringGrid_UserDraw
  Left = 339
  Top = 185
  Caption = 'PRD-FICSM-002 :: Ficha de Consumo - Itens N'#227'o B'#225'sicos'
  ClientHeight = 624
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnBase: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 462
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 29
      Align = alTop
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 8
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label3: TLabel
        Left = 112
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdGG1Dest: TdmkEdit
        Left = 28
        Top = 4
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNomeGG1Dest: TdmkEdit
        Left = 168
        Top = 4
        Width = 829
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object PnLeft: TPanel
      Left = 0
      Top = 29
      Width = 377
      Height = 433
      Align = alLeft
      TabOrder = 1
      object PCOrigens: TPageControl
        Left = 1
        Top = 1
        Width = 375
        Height = 431
        ActivePage = TabSheet6
        Align = alClient
        TabOrder = 0
        object TabSheet5: TTabSheet
          Caption = ' Adicionados '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Splitter1: TSplitter
            Left = 0
            Top = 297
            Width = 367
            Height = 5
            Cursor = crVSplit
            Align = alTop
          end
          object DBGSrcArt: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 367
            Height = 297
            Align = alTop
            DataSource = DsSrcArt
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDblClick = DBGSrcArtDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Nivel1'
                Title.Caption = 'Produto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GG1_Sorc'
                Title.Caption = 'Descri'#231#227'o do produto'
                Width = 236
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ITENS'
                Title.Caption = 'Itens'
                Width = 28
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 0
            Top = 302
            Width = 367
            Height = 101
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            ExplicitLeft = 32
            ExplicitTop = 320
            ExplicitWidth = 185
            ExplicitHeight = 41
            object Splitter2: TSplitter
              Left = 121
              Top = 0
              Height = 101
              ExplicitLeft = 148
              ExplicitTop = 44
              ExplicitHeight = 100
            end
            object DBGSrcTam: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 121
              Height = 101
              Align = alLeft
              DataSource = DsSrcTam
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDblClick = DBGSrcTamDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TAM_Sorc'
                  Title.Caption = 'Tamanho'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ITENS'
                  Title.Caption = 'Itens'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Width = 68
                  Visible = True
                end>
            end
            object DBGSrcCor: TdmkDBGridZTO
              Left = 124
              Top = 0
              Width = 243
              Height = 101
              Align = alClient
              DataSource = DsSrcCor
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnDblClick = DBGSrcCorDblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_COR_Sorc'
                  Title.Caption = 'Descri'#231#227'o da cor'
                  Width = 169
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ITENS'
                  Title.Caption = 'Itens'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' Adicionar '
          ImageIndex = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 367
            Height = 403
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 367
              Height = 81
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label1: TLabel
                Left = 4
                Top = 0
                Width = 40
                Height = 13
                Caption = 'Produto:'
              end
              object Label2: TLabel
                Left = 4
                Top = 40
                Width = 48
                Height = 13
                Caption = 'Tamanho:'
              end
              object EdGraGru1: TdmkEditCB
                Left = 4
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdGraGru1Redefinido
                DBLookupComboBox = CBGraGru1
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraGru1: TdmkDBLookupComboBox
                Left = 63
                Top = 16
                Width = 300
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsGraGru1
                TabOrder = 1
                dmkEditCB = EdGraGru1
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdGraTamIts: TdmkEditCB
                Left = 4
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdGraTamItsRedefinido
                DBLookupComboBox = CBGraTamIts
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraTamIts: TdmkDBLookupComboBox
                Left = 63
                Top = 56
                Width = 300
                Height = 21
                KeyField = 'Controle'
                ListField = 'Nome'
                ListSource = DsGraTamIts
                TabOrder = 3
                dmkEditCB = EdGraTamIts
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object Panel3: TPanel
              Left = 0
              Top = 81
              Width = 367
              Height = 322
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object PnVV: TPanel
                Left = 0
                Top = 0
                Width = 312
                Height = 322
                Align = alClient
                TabOrder = 0
                object DBGCores: TdmkDBGridZTO
                  Left = 1
                  Top = 1
                  Width = 310
                  Height = 320
                  Align = alClient
                  DataSource = DsCores
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  OnCellClick = DBGCoresCellClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruC'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Title.Caption = 'Nome da cor de origem'
                      Visible = True
                    end>
                end
              end
              object Panel5: TPanel
                Left = 312
                Top = 0
                Width = 55
                Height = 322
                Align = alRight
                TabOrder = 1
                object Sb_NtoN: TSpeedButton
                  Left = 4
                  Top = 20
                  Width = 45
                  Height = 25
                  Caption = 'N >> N'
                end
                object Sb_DelN: TSpeedButton
                  Left = 4
                  Top = 136
                  Width = 45
                  Height = 25
                  Caption = '0 << N'
                end
                object Sb_1toN: TSpeedButton
                  Left = 4
                  Top = 48
                  Width = 45
                  Height = 25
                  Caption = '1 > N'
                end
                object Sb_Del1: TSpeedButton
                  Left = 4
                  Top = 164
                  Width = 45
                  Height = 25
                  Caption = '0 < 1'
                end
                object Sb_1to1: TSpeedButton
                  Left = 4
                  Top = 76
                  Width = 45
                  Height = 25
                  Caption = '1 > 1'
                  OnClick = Sb_1to1Click
                end
              end
            end
          end
        end
      end
    end
    object PnDest: TPanel
      Left = 377
      Top = 29
      Width = 631
      Height = 433
      Align = alClient
      TabOrder = 2
      object PCGrades: TPageControl
        Left = 1
        Top = 1
        Width = 629
        Height = 431
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = ' Quantidades '
          object GradeK: TStringGrid
            Left = 0
            Top = 0
            Width = 621
            Height = 403
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            DoubleBuffered = True
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Options = [goFixedHorzLine, goHorzLine, goRangeSelect, goFixedColClick, goFixedRowClick]
            ParentDoubleBuffered = False
            TabOrder = 0
            OnClick = GradeKClick
            OnDblClick = GradeKDblClick
            OnDrawCell = GradeKDrawCell
            OnFixedCellClick = GradeKFixedCellClick
            OnMouseDown = GradeKMouseDown
            OnMouseUp = GradeKMouseUp
            OnTopLeftChanged = GradeKTopLeftChanged
            RowHeights = (
              18
              18)
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Ativos '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeA: TStringGrid
            Left = 0
            Top = 0
            Width = 621
            Height = 403
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            RowHeights = (
              18
              18)
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Reduzidos '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 286
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            RowHeights = (
              18
              18)
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' X '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 286
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            RowHeights = (
              18
              18)
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 483
        Height = 32
        Caption = ' Ficha de Consumo - Itens N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 32
        Caption = ' Ficha de Consumo - Itens N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 483
        Height = 32
        Caption = ' Ficha de Consumo - Itens N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 510
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 554
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 180
        Top = 2
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label6: TLabel
        Left = 180
        Top = 24
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label7: TLabel
        Left = 364
        Top = 2
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label8: TLabel
        Left = 364
        Top = 24
        Width = 9
        Height = 13
        Caption = '...'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = FmPrincipal.MySQLDatabase1
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome, GraTamCad, GG1Subst'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 780
    Top = 120
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGru1GG1Subst: TIntegerField
      FieldName = 'GG1Subst'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 780
    Top = 168
  end
  object QrGraTamIts: TMySQLQuery
    Database = FmPrincipal.MySQLDatabase1
    SQL.Strings = (
      'SELECT Controle, Nome'
      'FROM gratamits'
      'WHERE Codigo=1'
      'ORDER BY Controle')
    Left = 780
    Top = 220
    object QrGraTamItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraTamItsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 5
    end
  end
  object DsGraTamIts: TDataSource
    DataSet = QrGraTamIts
    Left = 780
    Top = 264
  end
  object QrCores: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrCoresBeforeClose
    AfterScroll = QrCoresAfterScroll
    Left = 77
    Top = 235
    object QrCoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrCoresGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
  end
  object DsCores: TDataSource
    DataSet = QrCores
    Left = 77
    Top = 283
  end
  object DqAux: TMySQLDirectQuery
    Database = FmTempDB.DBTeste
    Left = 205
    Top = 275
  end
  object QrSrcArt: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrSrcArtBeforeClose
    AfterScroll = QrSrcArtAfterScroll
    SQL.Strings = (
      'SELECT gg11.Nivel1, gg11.Nome NO_GG1_Sorc, '
      'COUNT(fcs.Controle) ITENS  '
      'FROM ficonsspc fcs '
      'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc '
      'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  '
      'WHERE fcs.GG1Dest=25'
      'GROUP BY gg11.Nivel1'
      'ORDER BY gg11.Nome ')
    Left = 418
    Top = 242
    object QrSrcArtNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSrcArtNO_GG1_Sorc: TWideStringField
      FieldName = 'NO_GG1_Sorc'
      Size = 120
    end
    object QrSrcArtITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSrcArt: TDataSource
    DataSet = QrSrcArt
    Left = 418
    Top = 290
  end
  object QrSrcTam: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrSrcTamBeforeClose
    AfterScroll = QrSrcTamAfterScroll
    SQL.Strings = (
      'SELECT gti1.Controle, gti1.Nome NO_TAM_Sorc, '
      'COUNT(fcs.Controle) ITENS  '
      'FROM ficonsspc fcs '
      'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc '
      'LEFT JOIN gratamits  gti1 ON gti1.Controle=ggx1.GraTamI  '
      'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  '
      'WHERE fcs.GG1Dest=25'
      'AND gg11.Nivel1>0'
      'GROUP BY gti1.Controle'
      'ORDER BY gti1.Nome')
    Left = 478
    Top = 242
    object QrSrcTamControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrcTamNO_TAM_Sorc: TWideStringField
      FieldName = 'NO_TAM_Sorc'
      Size = 5
    end
    object QrSrcTamITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSrcTam: TDataSource
    DataSet = QrSrcTam
    Left = 478
    Top = 290
  end
  object QrSrcCor: TMySQLQuery
    Database = FmTempDB.DBTeste
    AfterScroll = QrSrcCorAfterScroll
    SQL.Strings = (
      'SELECT gcc1.Codigo, gcc1.Nome NO_COR_Sorc, '
      'COUNT(fcs.Controle) ITENS  '
      'FROM ficonsspc fcs '
      'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc '
      'LEFT JOIN gragruc    ggc1 ON ggc1.Controle=ggx1.GraGruC  '
      'LEFT JOIN gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraCorCad  '
      'LEFT JOIN gratamits  gti1 ON gti1.Controle=ggx1.GraTamI  '
      'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  '
      'WHERE fcs.GG1Dest=25'
      'AND gg11.Nivel1>0'
      'AND gti1.Controle>0'
      'GROUP BY gcc1.Codigo'
      'ORDER BY gcc1.Nome')
    Left = 542
    Top = 242
    object QrSrcCorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSrcCorNO_COR_Sorc: TWideStringField
      FieldName = 'NO_COR_Sorc'
      Size = 30
    end
    object QrSrcCorITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSrcCor: TDataSource
    DataSet = QrSrcCor
    Left = 542
    Top = 290
  end
end
