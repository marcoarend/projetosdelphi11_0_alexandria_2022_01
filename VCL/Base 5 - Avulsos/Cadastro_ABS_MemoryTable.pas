unit Cadastro_ABS_MemoryTable;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBGridZTO,
  ABSMain, dmkRadioGroup;

type
  TFmEntCliCad = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    Panel5: TPanel;
    Label1: TLabel;
    EdCliNome: TdmkEdit;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrEntCliCad: TABSQuery;
    DsEntCliCad: TDataSource;
    BtInsUpd: TBitBtn;
    Label4: TLabel;
    EdCliCodi: TdmkEdit;
    QrEntCliCadversao: TFloatField;
    QrEntCliCadCliCodi: TWideStringField;
    QrEntCliCadCliNome: TWideStringField;
    QrEntCliCadAtivo: TSmallintField;
    QrEntCliCadNomeUTF: TWideStringField;
    Label2: TLabel;
    EdCidade: TdmkEdit;
    Label3: TLabel;
    EdUF: TdmkEdit;
    Label5: TLabel;
    EdPais: TdmkEdit;
    QrEntCliCadCidade: TWideStringField;
    QrEntCliCadUF: TWideStringField;
    QrEntCliCadPais: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtInsUpdClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
    procedure EdCliNomeChange(Sender: TObject);
    procedure EdCliCodiChange(Sender: TObject);
  private
    { Private declarations }
    procedure DefinedadosPorSelecao();
    procedure ReabrePesquisa();
  public
    { Public declarations }
    //
    FConfirmado: Boolean;
    FCliCodi, FCliNome: String;
    //
  end;

  var
  FmEntCliCad: TFmEntCliCad;

implementation

uses UnMyObjects, Module, UMemModule;

{$R *.DFM}

procedure TFmEntCliCad.BtInsUpdClick(Sender: TObject);
const
  Ativo = '1';
var
  CliCodi, CliNome, Cidade, UF, Pais: String;
begin
  CliCodi := EdCliCodi.Text;
  CliNome := EdCliNome.Text;
  Cidade  := EdCidade.Text;
  UF      := EdUF.Text;
  Pais    := EdPais.Text;
  //
  if MyObjects.FIC(Trim(CliCodi) = '', EdCliCodi, 'Defina o c�digo do cliente!') then
    Exit;
  if MyObjects.FIC(Trim(CliNome) = '', EdCliNome, 'Defina o nome do cliente!') then
    Exit;
  if Dmod.InserirRegistroEntCliCad(CliCodi, CliNome, Cidade, UF, Pais, Ativo) then
  begin
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_XML);
    Dmod.SalvaXML_EntCliCod(False);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    FConfirmado := True;
    FCliCodi    := CliCodi;
    FCliNome    := CliNome;
    //
    Dmod.AbreEntCliCad(Dmod.TbEntCliCad);
    UMemMod.AbreABSTable1(Dmod.TbEntCliCad);
    //
    EdCliCodi.Text := '';
    EdCliNome.Text := '';
    EdCidade.Text  := '';
    EdUF.Text      := '';
    //EdPais.Text    := '';
    //
    ReabrePesquisa();
    QrEntCliCad.Locate('CliCodi', CliCodi, []);
  end;
end;

procedure TFmEntCliCad.BtOKClick(Sender: TObject);
begin
  DefinedadosPorSelecao();
end;

procedure TFmEntCliCad.BtSaidaClick(Sender: TObject);
begin
  FConfirmado := True;
  FCliCodi    := EdCliCodi.Text;
  FCliNome    := EdCliNome.Text;
  //
  Close;
end;

procedure TFmEntCliCad.DefinedadosPorSelecao();
begin
  FConfirmado := QrEntCliCad.RecordCount > 0;
  FCliCodi    := QrEntCliCadCliCodi.Value;
  FCliNome    := QrEntCliCadCliNome.Value;
  //
  Close;
end;

procedure TFmEntCliCad.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  //DefinedadosPorSelecao();
  EdCliCodi.Text := QrEntCliCadCliCodi.Value;
  EdCliNome.Text := QrEntCliCadCliNome.Value;
  EdCidade.Text  := QrEntCliCadCidade.Value;
  EdUF.Text      := QrEntCliCadUF.Value;
  EdPais.Text    := QrEntCliCadPais.Value;
  //
end;

procedure TFmEntCliCad.EdCliCodiChange(Sender: TObject);
begin
  MyObjects.AlteraCaptionBtInsUpd(BtInsUpd,
    Dmod.LocalizaEntCliCad(EdCliCodi.Text));
end;

procedure TFmEntCliCad.EdCliNomeChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmEntCliCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntCliCad.FormCreate(Sender: TObject);
begin
  FConfirmado := False;
  FCliCodi    := '';
  FCliNome    := '';
  //
  ReabrePesquisa();
end;

procedure TFmEntCliCad.ReabrePesquisa();
var
  Texto: String;
begin
  Texto := Geral.MyUTF(EdCliNome.Text);
  QrEntCliCad.Close;
  UMemMod.AbreABSQuery1(QrEntCliCad, [
  'SELECT ecc.* ',
  'FROM entclicad ecc ',
  'WHERE ecc.NomeUTF LIKE "%' + Texto + '%"',
  'ORDER BY ecc.NomeUTF ',
  '']);
end;

end.
