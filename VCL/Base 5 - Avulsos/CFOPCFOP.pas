unit CFOPCFOP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCFOPCFOP = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label2: TLabel;
    EdCFOP_OUT: TdmkEditCB;
    CBCFOP_OUT: TdmkDBLookupComboBox;
    SbCFOP_OUT: TSpeedButton;
    QrInn: TmySQLQuery;
    QrInnCodigo: TWideStringField;
    QrInnNome: TWideStringField;
    DsInn: TDataSource;
    QrOut: TmySQLQuery;
    QrOutCodigo: TWideStringField;
    QrOutNome: TWideStringField;
    DsOut: TDataSource;
    EdCFOP_INN: TdmkEditCB;
    CBCFOP_INN: TdmkDBLookupComboBox;
    SbCFOP_INN: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCFOP_OUTClick(Sender: TObject);
    procedure SbCFOP_INNClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure CadShowCFOP(Query: TmySQLQuery; Ed: TdmkEditCB; CB:
              TdmkDBLookupComboBox);
  public
    { Public declarations }
    FQrCab, FQrInn, FQrOut: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCFOPCFOP: TFmCFOPCFOP;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnGrade_Jan;

{$R *.DFM}

procedure TFmCFOPCFOP.BtOKClick(Sender: TObject);
var
  Nome, CFOP_INN, CFOP_OUT: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  CFOP_INN       := EdCFOP_INN.ValueVariant;
  CFOP_OUT       := EdCFOP_OUT.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  if MyObjects.FIC(Trim(CFOP_OUT) = '', EdCFOP_OUT, 'Informe o CFOP de sa�da!') then
    Exit;
  if MyObjects.FIC(Trim(CFOP_INN) = '', EdCFOP_INN, 'Informe o CFOP de entrada!') then
    Exit;
  if MyObjects.FIC(Trim(CFOP_INN) = Trim(CFOP_OUT), nil, 'O CFOP de entrada e sa�da n�o pode ser o mesmo!') then
    Exit;
  Codigo := UMyMod.BPGS1I32('cfopcfop', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cfopcfop', False, [
  'CFOP_INN', 'CFOP_OUT', 'Nome'], [
  'Codigo'], [
  CFOP_INN, CFOP_OUT, Nome], [
  Codigo], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmCFOPCFOP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCFOPCFOP.CadShowCFOP(Query: TmySQLQuery; Ed: TdmkEditCB; CB:
  TdmkDBLookupComboBox);
begin
  VAR_CADTEXTO := '';
  //
  Grade_Jan.MostraFormCFOP2003();
  //
  if VAR_CADTEXTO <> '' then
  begin
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    Ed.ValueVariant := VAR_CADTEXTO;
    CB.KeyValue     := VAR_CADTEXTO;
  end;
end;

procedure TFmCFOPCFOP.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmCFOPCFOP.FormCreate(Sender: TObject);
  procedure AbreCFOP(Query: TmySQLQuery);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM CFOP2003 ',
    'ORDER BY Nome ',
    '']);
  end;
begin
  ImgTipo.SQLType := stLok;
  //
  AbreCFOP(QrInn);
  AbreCFOP(QrOut);
end;

procedure TFmCFOPCFOP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCFOPCFOP.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrInn <> nil then
    UnDmkDAC_PF.AbreQuery(FQrInn, Dmod.MyDB);
    //
  if FQrOut <> nil then
    UnDmkDAC_PF.AbreQuery(FQrOut, Dmod.MyDB);
    //
end;

procedure TFmCFOPCFOP.SbCFOP_INNClick(Sender: TObject);
begin
  CadShowCFOP(QrInn, EdCFOP_INN, CBCFOP_INN);
end;

procedure TFmCFOPCFOP.SbCFOP_OUTClick(Sender: TObject);
begin
  CadShowCFOP(QrOut, EdCFOP_OUT, CBCFOP_OUT);
end;

end.
