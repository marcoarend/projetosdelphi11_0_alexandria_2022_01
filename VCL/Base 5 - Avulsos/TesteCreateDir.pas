unit TesteCreateDir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts,
  // CreateDir
  AccCtrl, AclApi, UnDmkEnums, dmkEdit,
  // Distance
  math;

type
  PTrusteeW = ^TTrusteeW;
  TTrusteeW = record
    pMultipleTrustee: PTrusteeW;
    MultipleTrusteeOperation: DWORD;  { MULTIPLE_TRUSTEE_OPERATION }
    TrusteeForm: DWORD;  { TRUSTEE_FORM }
    TrusteeType: DWORD;  { TRUSTEE_TYPE }
    ptstrName: PWideChar;
  end;
  TExplicitAccessW = record
    grfAccessPermissions: DWORD;
    grfAccessMode: DWORD;  { ACCESS_MODE }
    grfInheritance: DWORD;
    Trustee: TTrusteeW;
  end;
  TFmTesteCreateDir = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Verde: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Shape1: TShape;
    Label5: TLabel;
    Label6: TLabel;
    EdGG: TdmkEdit;
    EdGB: TdmkEdit;
    EdGR: TdmkEdit;
    EdRR: TdmkEdit;
    EdRB: TdmkEdit;
    EdVal: TdmkEdit;
    EdRM: TdmkEdit;
    ScrollBar1: TScrollBar;
    Panel5: TPanel;
    EdLat0: TdmkEdit;
    EdLon0: TdmkEdit;
    EdLat1: TdmkEdit;
    EdLon1: TdmkEdit;
    EdRes1: TdmkEdit;
    EdLat2: TdmkEdit;
    EdLon2: TdmkEdit;
    EdRes2: TdmkEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    CreateDir: TTabSheet;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdRBChange(Sender: TObject);
    procedure ScrollBar1Change(Sender: TObject);
    procedure EdValChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalculaDistancia();
    procedure CriaDirFullAcess();
  end;

  var
  FmTesteCreateDir: TFmTesteCreateDir;

implementation

uses UnMyObjects, Module, UnDmkProcFunc;

{$R *.DFM}

function CreateDirectoryFullAccess(NewDirectory:String) :Boolean;
var
    SecurityAttributes  : TSecurityAttributes;
    SecurityDescriptor  : PSecurityDescriptor;
    ExplicitAccess      : array[0..0] of TExplicitAccessW;
    //easize              : integer;
    pACL                : Windows.PACL;
begin
    ExplicitAccess[0].grfAccessPermissions:= STANDARD_RIGHTS_ALL or SPECIFIC_RIGHTS_ALL;
    ExplicitAccess[0].grfAccessMode:=Ord(SET_ACCESS);
    ExplicitAccess[0].grfInheritance:=SUB_CONTAINERS_AND_OBJECTS_INHERIT;
    ExplicitAccess[0].Trustee.TrusteeForm:=Ord(TRUSTEE_IS_NAME);
    ExplicitAccess[0].Trustee.TrusteeType:=Ord(TRUSTEE_IS_USER);
    ExplicitAccess[0].Trustee.ptstrName:='Usu�rios';//Access for all users
    //ExplicitAccess[0].Trustee.ptstrName:='Everyone';//Access for all users
    SetEntriesinAclW(1,@ExplicitAccess,nil,pACL);//creates a new access control list

    SecurityDescriptor:= AllocMem(sizeof(SECURITY_DESCRIPTOR_MIN_LENGTH));
    InitializeSecurityDescriptor(SecurityDescriptor,SECURITY_DESCRIPTOR_REVISION);
    SetSecurityDescriptorDacl(SecurityDescriptor,true,pacl,false);//sets information in a discretionary access control list (DACL).

    FillChar(SecurityAttributes,sizeof(SECURITY_ATTRIBUTES),#0);
    SecurityAttributes.nLength:=sizeof(SECURITY_ATTRIBUTES);
    SecurityAttributes.lpSecurityDescriptor:=SecurityDescriptor;
    SecurityAttributes.bInheritHandle:=false;
    CreateDirectory(PChar(NewDirectory),@SecurityAttributes);
    Result:=GetLastError=0;// if all ok, GetLastError = 0
end;

procedure TFmTesteCreateDir.BtOKClick(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: CalculaDistancia();
    2: CriaDirFullAcess();
  end;
end;

procedure TFmTesteCreateDir.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTesteCreateDir.CalculaDistancia();
  function Distance(Latitude1, Longitude1, Latitude2, Longitude2: Double): Double;
  begin
    Result :=
      ( ArcCos(
              cos( DegToRad(Latitude1) ) *
              cos( DegToRad( Latitude2 ) ) *
              cos( DegToRad( Longitude2 ) -
              DegToRad(Longitude1) ) +
              sin( DegToRad(Latitude1) ) *
              sin( DegToRad( Latitude2 ) )
          )
      ) * 6371;
  end;
begin
  EdRes1.ValueVariant := Distance(
    EdLat0.ValueVariant, EdLon0.ValueVariant,
    EdLat1.ValueVariant, EdLon1.ValueVariant);
  EdRes2.ValueVariant := Distance(
    EdLat0.ValueVariant, EdLon0.ValueVariant,
    EdLat2.ValueVariant, EdLon2.ValueVariant);
end;

procedure TFmTesteCreateDir.CriaDirFullAcess;
begin
  if CreateDirectoryFullAccess('C:\MyNewDir') then
   ShowMessage('Ok')
  else
   ShowMessage('Failed');
end;

procedure TFmTesteCreateDir.EdRBChange(Sender: TObject);
begin
  ScrollBar1.Max := Trunc(EdRB.ValueVariant * 1.5) + 1;
end;

procedure TFmTesteCreateDir.EdValChange(Sender: TObject);
var
  Cor: TColor;
begin
  Cor := dmkPF.CalculaCor(
    EdVal.ValueVariant,
    EdGG.ValueVariant,
    EdGB.ValueVariant,
    EdGR.ValueVariant,
    EdRR.ValueVariant,
    EdRM.ValueVariant,
    EdRB.ValueVariant);
  //
  Shape1.Brush.Color := Cor;
end;

procedure TFmTesteCreateDir.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTesteCreateDir.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ScrollBar1.Max := Trunc(EdRB.ValueVariant * 1.5) + 1;
  EdValChange(Self);
end;

procedure TFmTesteCreateDir.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTesteCreateDir.ScrollBar1Change(Sender: TObject);
begin
  EdVal.ValueVariant := ScrollBar1.Position;
end;

{
SELECT *
FROM siaptercad
WHERE Codigo IN (
    SELECT DISTINCT SiapTerCad
    FROM oscab
    WHERE Codigo IN (
        SELECT ID_Cod1
        FROM protpakits
        WHERE Controle=309
        )
    )


}

end.
