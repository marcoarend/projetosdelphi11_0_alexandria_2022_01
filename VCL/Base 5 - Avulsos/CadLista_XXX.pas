unit CadLista_XXX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO;

type
  TFmCadLista_XXX = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    TbCadLista_XXX: TmySQLTable;
    DsCadLista_XXX: TDataSource;
    TbCadLista_XXXCodigo: TIntegerField;
    TbCadLista_XXXNome: TWideStringField;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    LbItensMD: TListBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbCadLista_XXXBeforePost(DataSet: TDataSet);
    procedure TbCadLista_XXXDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbCadLista_XXXAfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbCadLista_XXXAfterInsert(DataSet: TDataSet);
    procedure TbCadLista_XXXNewRecord(DataSet: TDataSet);
    procedure TbCadLista_XXXBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    FModoInclusao: TdmkModoInclusao;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    FFldIndices: array of String;
    FValIndices: array of Variant;
*)
    //
    procedure ReabrePesquisa();
  end;

  var
  FmCadLista_XXX: TFmCadLista_XXX;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista;

{$R *.DFM}

procedure TFmCadLista_XXX.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, TmySQLQuery(TbCadLista_XXX), [PnDados],
  [PnEdita], EdNome, ImgTipo, TbCadLista_XXX.TableName);
end;

procedure TFmCadLista_XXX.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32(TbCadLista_XXX.TableName, FFldID, '', '',
    tsPos, ImgTipo.SQLType, TbCadLista_XXXCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    TbCadLista_XXX.TableName, Codigo, Dmod.QrUpd, [PnEdita], [PnDados],
    ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
*)
    TbCadLista_XXX.Refresh;
    TbCadLista_XXX.Locate(FFldID, Codigo, []);
  end;
end;

procedure TFmCadLista_XXX.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmCadLista_XXX.BtIncluiClick(Sender: TObject);
begin
(*
  UnCfgCadLista.IncluiNovoRegistro(FModoInclusao, Self, PnEdita, [PnDados],
  [PnEdita], EdNome, ImgTipo, FReservados, TbCadLista_XXX, FFldID);
*)
end;

procedure TFmCadLista_XXX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCadLista_XXX.DBGrid2DblClick(Sender: TObject);
begin
  UnCfgCadLista.LocalizaPesquisadoInt1(TbCadLista_XXX, QrPesq, FFldID, True);
end;

procedure TFmCadLista_XXX.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadLista_XXX.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
  BtInclui.Visible := FReservados <> Null;
end;

procedure TFmCadLista_XXX.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align := alClient;
  FNovoCodigo := nc??;
  FReservados := Null ou zero?;
  FSoReserva := False?;
  FPermiteExcluir := False?;
  FModoInclusao := dmkmi???;
  FFldID := CO_CODIGO?;
  FFldNome := CO_NOME?;
  //
  TbEstatusOSs.TableName := 'CadLista_XXX?;
end;

procedure TFmCadLista_XXX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadLista_XXX.FormShow(Sender: TObject);
begin
  TbCadLista_XXX.Open;
end;

procedure TFmCadLista_XXX.LbItensMDDblClick(Sender: TObject);
begin
  UnCfgCadLista.AddVariavelEmTexto(LbItensMD, TbCadLista_XXX, FFldNome, DBGrid1);
end;

procedure TFmCadLista_XXX.ReabrePesquisa();
begin
  UnCfgCadLista.ReabrePesquisa(
    QrPesq, TbCadLista_XXX, EdPesq, TBTam, FFldID, FFldNome);
end;

procedure TFmCadLista_XXX.TbCadLista_XXXAfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TBCadLista_XXX.Cancel;
end;

procedure TFmCadLista_XXX.TbCadLista_XXXAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TBCadLista_XXXCodigo.Value;
end;

procedure TFmCadLista_XXX.TbCadLista_XXXBeforeOpen(DataSet: TDataSet);
(*
var
  I: Integer;
  Txt: String;
*)
begin
(* NUDQU*) - Nunca usado! Desmarcar quando precisar usar
  I := High(FFldIndices);
  TBCadLista_XXX.Filter := '';
  TBCadLista_XXX.Close;  // Ter certeza ??
  TBCadLista_XXX.Filtered := False;
  for I := Low(FFldIndices) to High(FFldIndices) do
  begin
    Txt := 'and ' + FFldIndices[I] + Geral.VariavelToString(FFldIndices[I]);
  end;
  if Txt <> '' then
  begin
    Txt := Copy(Txt, 4);
    TBCadLista_XXX.Filter := Txt;
    TBCadLista_XXX.Filtered := True;
  end;
*)
end;

procedure TFmCadLista_XXX.TbCadLista_XXXBeforePost(DataSet: TDataSet);
begin
  UnCfgCadLista.TbBeforePost(TbCadLista_XXX, FNovoCodigo, FFldID, FReservados);
  //
(**) NUDQU - Nunca usado! Desmarcar quando precisar usar
    for I := Low(FFldIndices) to High(FFldIndices) do
      TBCadLista_XXX.FieldByName(FFldIndices[I]).AsVariant := FFldIndices[I];
*)
  end;
end;

procedure TFmCadLista_XXX.TbCadLista_XXXDeleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmCadLista_XXX.TbCadLista_XXXNewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TBCadLista_XXX.Cancel;
end;

procedure TFmCadLista_XXX.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
