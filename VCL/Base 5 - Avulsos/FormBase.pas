unit FormBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmFormBase = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPqx: TMySQLQuery;
    DsSPqx: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    EdEmpresa: TdmkEdit;
    Label1: TLabel;
    EdCliInt: TdmkEdit;
    Label2: TLabel;
    EdInsumo: TdmkEdit;
    Label3: TLabel;
    EdNO_Empresa: TdmkEdit;
    EdNO_CliInt: TdmkEdit;
    EdNO_Insumo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenStqaLocIts(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmFormBase: TFmFormBase;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmFormBase.BtOKClick(Sender: TObject);
var
  DtHrAbert, DtHrFecha, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType?;
{
  Codigo         := ;
  Local          := ;
  NrOP           := ;
  SeqGrupo       := ;
  NrReduzidoOP   := ;
  DtHrAbert      := ;
  DtHrFecha      := ;
  OVcYnsMed      := ;
  OVcYnsChk      := ;
  LimiteChk      := ;
  LimiteMed      := ;
  ZtatusIsp      := ;
  ZtatusDtH      := ;
  ZtatusMot      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('ovgispgercab', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovgispgercab', auto_increment?[
'Local', 'NrOP', 'SeqGrupo',
'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
'ZtatusMot'], [
'Codigo'], [
Local, NrOP, SeqGrupo,
NrReduzidoOP, DtHrAbert, DtHrFecha,
OVcYnsMed, OVcYnsChk, LimiteChk,
LimiteMed, ZtatusIsp, ZtatusDtH,
ZtatusMot], [
Codigo], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmFormBase.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormBase.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormBase.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenStqaLocIts(0);
end;

procedure TFmFormBase.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormBase.ReopenStqaLocIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqaLocIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqalocits ',
  '']);
end;

end.
