unit StringGrid_UserDraw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.ComCtrls, Vcl.DBGrids,
  dmkDBGridZTO, mySQLDirectQuery;

type
  //THackDBGrid = class(TDBGrid);
  TFmStringGrid_UserDraw = class(TForm)
    PnBase: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Label5: TLabel;
    EdGG1Dest: TdmkEdit;
    Label3: TLabel;
    EdNomeGG1Dest: TdmkEdit;
    PnLeft: TPanel;
    PnDest: TPanel;
    PCGrades: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GradeA: TStringGrid;
    TabSheet3: TTabSheet;
    GradeC: TStringGrid;
    TabSheet4: TTabSheet;
    GradeX: TStringGrid;
    PCOrigens: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel3: TPanel;
    PnVV: TPanel;
    Panel5: TPanel;
    QrGraGru1: TMySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    DsGraGru1: TDataSource;
    Label1: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdGraTamIts: TdmkEditCB;
    CBGraTamIts: TdmkDBLookupComboBox;
    QrGraTamIts: TMySQLQuery;
    DsGraTamIts: TDataSource;
    QrGraTamItsControle: TIntegerField;
    QrGraTamItsNome: TWideStringField;
    QrGraGru1GraTamCad: TIntegerField;
    GradeK: TStringGrid;
    QrCores: TMySQLQuery;
    QrCoresCodigo: TIntegerField;
    QrCoresNome: TWideStringField;
    QrCoresGraGruC: TIntegerField;
    DBGCores: TdmkDBGridZTO;
    DsCores: TDataSource;
    Sb_NtoN: TSpeedButton;
    Sb_DelN: TSpeedButton;
    Sb_1toN: TSpeedButton;
    Sb_Del1: TSpeedButton;
    Sb_1to1: TSpeedButton;
    DqAux: TMySQLDirectQuery;
    QrGraGru1GG1Subst: TIntegerField;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBGSrcArt: TdmkDBGridZTO;
    Splitter1: TSplitter;
    Panel8: TPanel;
    DBGSrcTam: TdmkDBGridZTO;
    DBGSrcCor: TdmkDBGridZTO;
    QrSrcArt: TMySQLQuery;
    DsSrcArt: TDataSource;
    QrSrcTam: TMySQLQuery;
    DsSrcTam: TDataSource;
    QrSrcCor: TMySQLQuery;
    DsSrcCor: TDataSource;
    QrSrcArtNivel1: TIntegerField;
    QrSrcArtNO_GG1_Sorc: TWideStringField;
    QrSrcArtITENS: TLargeintField;
    QrSrcTamControle: TIntegerField;
    QrSrcTamNO_TAM_Sorc: TWideStringField;
    QrSrcTamITENS: TLargeintField;
    QrSrcCorCodigo: TIntegerField;
    QrSrcCorNO_COR_Sorc: TWideStringField;
    QrSrcCorITENS: TLargeintField;
    Splitter2: TSplitter;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure GradeKDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure EdGraGru1Redefinido(Sender: TObject);
    procedure GradeKClick(Sender: TObject);
    procedure GradeKTopLeftChanged(Sender: TObject);
    procedure EdGraTamItsRedefinido(Sender: TObject);
    procedure Sb_1to1Click(Sender: TObject);
    procedure DBGCoresCellClick(Column: TColumn);
    procedure QrCoresAfterScroll(DataSet: TDataSet);
    procedure QrCoresBeforeClose(DataSet: TDataSet);
    procedure GradeKMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GradeKMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GradeKDblClick(Sender: TObject);
    procedure GradeKFixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure QrSrcArtAfterScroll(DataSet: TDataSet);
    procedure QrSrcArtBeforeClose(DataSet: TDataSet);
    procedure QrSrcTamAfterScroll(DataSet: TDataSet);
    procedure QrSrcTamBeforeClose(DataSet: TDataSet);
    procedure QrSrcCorAfterScroll(DataSet: TDataSet);
    procedure DBGSrcArtDblClick(Sender: TObject);
    procedure DBGSrcTamDblClick(Sender: TObject);
    procedure DBGSrcCorDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure ReopenGraTamItsOrigem(GraGru1, GraTamCad: Integer);
    procedure ReopenCoresOrigem(GraGru1, GraTamIts: Integer);
    procedure ReopenSrcTam();
    procedure ReopenSrcCor();
    procedure InsereItem(GG1Dest, GGXDest, GGXSorc, GG1Subst: Integer; QtdUso:
              Double);
    function  ObtemQuantidade(var q: Double): Boolean;
    procedure DesenhaLinha();
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FGG1Dest, FGraTamCadDest,
    FOriGraGru1, FOriGraTamI: Integer;
    FDBGClicked: Boolean;
    //
    procedure PreparaGrades();
    procedure ReopenSrcArt();

  end;

  var
  FmStringGrid_UserDraw: TFmStringGrid_UserDraw;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnGrade_Jan, UnTX_PF, UnGraTX_Jan, ModProd, ModuleGeral, UMySQLDB,
  GetValor;

{$R *.DFM}

const
  iColsPerItem = 3;
  iFixedCols   = 1;
  iFixedRows   = 2;

procedure TFmStringGrid_UserDraw.BtOKClick(Sender: TObject);
(*
var
  GG1Dest, Controle, GGXSorc, GGXSubs, Parte, Obrigatorio: Integer;
  QtdUso: Double;
  SQLType: TSQLType;
*)
begin
(*
  SQLType        := ImgTipo.SQLType;
  GG1Dest        := EdGG1Dest.ValueVariant;
  Controle       := EdControle.ValueVariant;
  GGXSorc        := EdGGXSorc.ValueVariant;
  GGXSubs        := EdGGXSubs.ValueVariant;
  Parte          := EdParte.ValueVariant;
  QtdUso         := EdQtdUso.ValueVariant;
  Obrigatorio    := Geral.BoolToInt(CkObrigatorio.Checked);
  //
  if MyObjects.FIC(GGXSorc = 0, EdGGXSorc, 'Informe o reduzido preferencial!') then
    Exit;
  if MyObjects.FIC(QtdUso < 0.001, EdGGXSorc, 'Informe a quantidade!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ficonsbas', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsbas', False, [
  'GG1Dest', 'GGXSorc', 'GGXSubs',
  'Parte', 'QtdUso', 'Obrigatorio'], [
  'Controle'], [
  GG1Dest, GGXSorc, GGXSubs,
  Parte, QtdUso, Obrigatorio], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdGGXSorc.ValueVariant   := 0;
      CBGGXSubs.KeyValue       := Null;
      EdGGXSubs.ValueVariant   := 0;
      CBGGXSubs.KeyValue       := Null;
      EdParte.ValueVariant     := 0;
      CBParte.KeyValue         := Null;
      EdQtdUso.ValueVariant    := 0;
      //
      EdGGXSorc.SetFocus;
    end else Close;
  end;
*)
end;

procedure TFmStringGrid_UserDraw.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStringGrid_UserDraw.DBGCoresCellClick(Column: TColumn);
begin
  FDBGClicked := True;
end;

procedure TFmStringGrid_UserDraw.DBGSrcArtDblClick(Sender: TObject);
begin
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  PCOrigens.ActivePageIndex := 1;
end;

procedure TFmStringGrid_UserDraw.DBGSrcCorDblClick(Sender: TObject);
begin
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  EdGraTamIts.ValueVariant := QrSrcTamControle.Value;
  CBGraTamIts.KeyValue     := QrSrcTamControle.Value;
  //
  QrCores.Locate('Codigo', QrSrcCorCodigo.Value, []);
  //
  PCOrigens.ActivePageIndex := 1;
end;

procedure TFmStringGrid_UserDraw.DBGSrcTamDblClick(Sender: TObject);
begin
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  EdGraTamIts.ValueVariant := QrSrcTamControle.Value;
  CBGraTamIts.KeyValue     := QrSrcTamControle.Value;
  //
  PCOrigens.ActivePageIndex := 1;
end;

procedure TFmStringGrid_UserDraw.DesenhaLinha();
(*
var
  Vertices: array of TPoint;
*)
begin
(*
  SetLength(Vertices, 5);
    vertices[0] := Point(0, 0);
    vertices[1] := Point(50, 0);
    vertices[2] := Point(75, 50);
    vertices[3] := Point(25, 50);
    vertices[4] := Point(0, 0);
    Shape1.Picture.Canvas.Polyline(vertices);
*)
end;

procedure TFmStringGrid_UserDraw.EdGraGru1Redefinido(Sender: TObject);
var
  CodUsu, GraTamCad: Integer;
begin
  CodUsu := EdGraGru1.ValueVariant;
  if CodUsu <> 0 then
  begin
    FOriGraGru1 := QrGraGru1Nivel1.Value;
    GraTamCad   := QrGraGru1GraTamCad.Value;
  end else
  begin
    FOriGraGRu1 := 0;
    GraTamCad   := 0;
  end;
  DmProd.AtualizaGradesFiConsSpcTripleCol(FGG1Dest, FOriGraGru1,
  (*Tam*)0, (*Cor*)0, GradeK, iFixedCols, iFixedRows);
  //
  ReopenGraTamItsOrigem(FOriGraGru1, GraTamCad);
end;

procedure TFmStringGrid_UserDraw.EdGraTamItsRedefinido(Sender: TObject);
var
  CodUsu: Integer;
begin
  CodUsu        := EdGraGru1.ValueVariant;
  FOriGraTamI := EdGraTamIts.ValueVariant;
  if (CodUsu <> 0) and CBGraTamIts.KeyValue <> Null then
  begin
    FOriGraGru1 := QrGraGru1Nivel1.Value;
    ReopenCoresOrigem(FOriGraGru1, FOriGraTamI)
  end else
    QrCores.Close;
end;

procedure TFmStringGrid_UserDraw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStringGrid_UserDraw.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FDBGClicked            := False;
  FOriGraGru1            := 0;
  FOriGraTamI            := 0;
  PCGrades.ActivePageIndex := 0;
  //
  GradeA.ColWidths[0] := 192;
  GradeC.ColWidths[0] := 192;
  GradeX.ColWidths[0] := 192;
  GradeK.ColWidths[0] := 192;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrGraCorCad, Dmod.MyDB);
  //UnDmkDAC_PF.AbreQuery(QrGraTamCad, Dmod.MyDB);
  //
end;

procedure TFmStringGrid_UserDraw.FormResize(Sender: TObject);
begin
  GradeK.Invalidate;
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStringGrid_UserDraw.GradeKClick(Sender: TObject);
begin
  //GradeK.Invalidate;
end;

procedure TFmStringGrid_UserDraw.GradeKDblClick(Sender: TObject);
begin
//
end;

procedure TFmStringGrid_UserDraw.GradeKDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
{
var
  Cor: Integer;
  Qtde: Double;
  Txto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel2.Color
  else if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
(*
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack,
      Cor, taLeftJustify,
      GradeK.Cells[Acol, ARow], 0, 0, False)
*)
  else
  begin
    Qtde := Geral.DMV(GradeK.Cells[Acol, ARow]);
    if Qtde > 0 then
      Txto := FormatFloat(Dmod.FStrFmtQtd, Qtde)
    else
      Txto := '';
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack, Cor,
      taRightJustify, Txto, 0, 0, False);
  end;
}
var
  CellText: String;
  c3lin, c3cor, c3TxtA, c3TxtB, Col_, ColA, ColB, ColC, cIni, cFim, rCol, l,
  SubCol: Integer;
  //IsPar: Boolean;
  Alinhamento: TAlignment;
begin
  Rect := GradeK.CellRect(ACol, ARow);

  if ARow = 0 then
  begin
    c3lin := (ACol mod iColsPerItem) + 0;
    //==========================================================================
    //---------- Borda do retangulo 3C1L na primeira linha ---------------------
    // caso n�o aparecem as 3 colunas no grid n�o desenha a linha oculta!
    //case ACol of
    case c3lin of
      //1, 4, 7: begin - Primeira coluna do Triple column
      1: begin
        Rect.Right := Rect.Right + GradeK.GridLineWidth;
      end;
      //2, 5, 8: begin - Segunda coluna...
      2: begin
        Rect.Left := Rect.Left - GradeK.GridLineWidth;
        Rect.Right := Rect.Right + GradeK.GridLineWidth;
      end;
      //3, 6, 9: begin -  Terceira coluna
      3: begin
        Rect.Left := Rect.Left - GradeK.GridLineWidth;
      end;
    end;
    //==========================================================================
    // -------------- Cor do retangulo 3C1L na primeira linha ------------------
(*
    case ACol of
      0, 4..6: begin
        GradeK.Canvas.Brush.Color := clWindow;
      end;
      1..3, 7..9: begin
        GradeK.Canvas.Brush.Color := clWebLinen;
      end;
    end;
*)
    c3cor := ((ACol + (iColsPerItem - iFixedCols)) div iColsPerItem) + 0;
    if (c3cor div 2) = (c3cor / 2) then  // 2 >> Cor sim / cor n�o
      GradeK.Canvas.Brush.Color := clWindow
    else
      GradeK.Canvas.Brush.Color := clWebLinen;
  end else
  begin
    //==========================================================================
    // ----------- Cor das linhas a partir da segunda (subtitulo) --------------
    if (State * [gdSelected, gdRowSelected]) <> [] then
      GradeK.Canvas.Brush.Color := clHighlight
    else
      GradeK.Canvas.Brush.Color := clWindow;
  end;

  //============================================================================
  // ------------  ---------------
  GradeK.Canvas.Brush.Style := bsSolid;
  GradeK.Canvas.Pen.Style := psClear;
  GradeK.Canvas.FillRect(Rect);

  GradeK.Canvas.Brush.Style := bsClear;
  GradeK.Canvas.Pen.Style := psSolid;
  //GradeK.Canvas.Pen.Color := clWindowText;
  GradeK.Canvas.Pen.Color := clSilver;

  if ARow = 0 then
  begin
    GradeK.Canvas.MoveTo(Rect.Left, Rect.Top);
    GradeK.Canvas.LineTo(Rect.Right, Rect.Top);

    //case ACol of
    //  0, 1, 4, 7: begin
    case c3lin of
      1: begin
        GradeK.Canvas.MoveTo(Rect.Left, Rect.Top);
        GradeK.Canvas.LineTo(Rect.Left, Rect.Bottom);
      end;
    end;

    //if ACol = 9 then
    if ACol = GradeK.ColCount - 1 then
    begin
      GradeK.Canvas.MoveTo(Rect.Right-1, Rect.Top);
      GradeK.Canvas.LineTo(Rect.Right-1, Rect.Bottom);
    end;

    GradeK.Canvas.MoveTo(Rect.Left, Rect.Bottom);
    GradeK.Canvas.LineTo(Rect.Right, Rect.Bottom);
  end
  else if ARow = 1 then
  begin
    GradeK.Canvas.MoveTo(Rect.Left, Rect.Top);
    GradeK.Canvas.LineTo(Rect.Right, Rect.Top);

    //case ACol of
      //1..9: begin
    if (ACol > 0) and (ACol < GradeK.ColCount) then begin
        GradeK.Canvas.MoveTo(Rect.Left, Rect.Top);
        GradeK.Canvas.LineTo(Rect.Left, Rect.Bottom);
      //end;
    end;

    //if ACol = 9 then
    if ACol = GradeK.ColCount - 1 then
    begin
      GradeK.Canvas.MoveTo(Rect.Right-1, Rect.Top);
      GradeK.Canvas.LineTo(Rect.Right-1, Rect.Bottom);
    end;

    GradeK.Canvas.MoveTo(Rect.Left, Rect.Bottom-1);
    GradeK.Canvas.LineTo(Rect.Right, Rect.Bottom-1);
  end
  else begin
    //case ACol of
      //1..9: begin
    if (ACol > 0) and (ACol < GradeK.ColCount) then begin
        GradeK.Canvas.MoveTo(Rect.Left, Rect.Top);
        GradeK.Canvas.LineTo(Rect.Left, Rect.Bottom);
      //end;
    end;

    //if ACol = 9 then
    if ACol = GradeK.ColCount - 1 then
    begin
      GradeK.Canvas.MoveTo(Rect.Right-1, Rect.Top);
      GradeK.Canvas.LineTo(Rect.Right-1, Rect.Bottom);
    end;
  end;

  if (State * [gdSelected, gdRowSelected]) <> [] then
  begin
    GradeK.Canvas.Brush.Color := clHighlight;
    GradeK.Canvas.Font.Color := clHighlightText;
  end else
  begin
    GradeK.Canvas.Brush.Color := clWindow;
    GradeK.Canvas.Font.Color := clWindowText;
  end;
  GradeK.Canvas.Brush.Style := bsClear;

  //if ARow = 0 then
  if (ARow = 0) and (ACol = 0) then
  begin
    CellText := GradeK.Cells[ACol, ARow];
    Rect.Inflate(-2, -2);
    DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_WORDBREAK or DT_LEFT or DT_VCENTER or DT_END_ELLIPSIS)
  end else
  if (ARow = 0) and (ACol > 0) then
  begin
    // Textos dos subtitulos!
(*
    case ACol of
      1..3: begin
        Rect.TopLeft := GradeK.CellRect(1, 0).TopLeft;
        Rect.BottomRight := GradeK.CellRect(3, 0).BottomRight;
        CellText := GradeK.Cells[1, 0];
      end;
      4..6: begin
        Rect.TopLeft := GradeK.CellRect(4, 0).TopLeft;
        Rect.BottomRight := GradeK.CellRect(6, 0).BottomRight;
        CellText := GradeK.Cells[4, 0];
      end;
      7..9: begin
        Rect.TopLeft := GradeK.CellRect(7, 0).TopLeft;
        Rect.BottomRight := GradeK.CellRect(9, 0).BottomRight;
        CellText := GradeK.Cells[7, 0];
      end;
    end;
*)
    begin
      //Memo1.Lines.Text := IntToStr(GradeK.LeftCol) + ' - ' + IntToStr(GradeK.VisibleColCount) + sLineBreak + Memo1.Lines.Text;
      c3TxtA := (ACol - iFixedCols) div iColsPerItem;
      c3TxtB := ACol mod iColsPerItem;
      Col_ := c3TxtA;
      ColA := (Col_ * iColsPerItem) + 1;
      ColB := (c3TxtA + 1) * iColsPerItem;
      begin
       // caso n�o aparecem as 3 colunas no grid ajusta o rect para centralizar
       // o texto nas colunas visiveis!
        if ColA < GradeK.LeftCol then
          cIni := GradeK.LeftCol
        else
          cIni := ColA;
        rCol := GradeK.LeftCol + GradeK.VisibleColCount;
        if ColB > rCol then
          cFim := rCol
        else
          cFim := ColB;
        // Fim ajuste colunas visiveis (cIni e cFim)
        Rect.TopLeft := GradeK.CellRect(cIni(*1,4,7...*), 0).TopLeft;
        Rect.BottomRight := GradeK.CellRect(cFim(*3,6,9...*), 0).BottomRight;
        CellText := GradeK.Cells[ColA(*1,4,7...*), 0];
(*
        Memo1.Lines.Text := IntToStr(GradeK.LeftCol) + ' - ' + IntToStr(GradeK.VisibleColCount)  +
        ' A=' + IntToStr(ColA) + ' B=' + IntToStr(ColB) + ' s=' + CellText + sLineBreak + Memo1.Lines.Text;
*)
      end;
    end;
    //
    Rect.Inflate(-2, -2);
    DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_CENTER or DT_VCENTER or DT_END_ELLIPSIS);
  end
  else if ARow = 1 then
  begin
    CellText := GradeK.Cells[ACol, ARow];
    Rect.Inflate(-2, -2);
    if ACol = 0 then
      DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_WORDBREAK or DT_LEFT or DT_VCENTER or DT_END_ELLIPSIS)
    else
      DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_CENTER or DT_BOTTOM or DT_END_ELLIPSIS);
  end
  else begin
    CellText := GradeK.Cells[ACol, ARow];
    Rect.Inflate(-2, -2);
(*
    case ACol of
      0..1, 4, 7: begin
        DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_LEFT or DT_VCENTER or DT_END_ELLIPSIS);
      end;
      2..3, 5..6, 8..9: begin
        DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_RIGHT or DT_VCENTER or DT_END_ELLIPSIS);
      end;
    end;
*)
    if ACol = 0 then
      Alinhamento := TAlignment.taLeftJustify
    else
    begin
      SubCol := ACol mod iColsPerItem;
      case SubCol of
        //Qtde
        0: Alinhamento := TAlignment.taRightJustify;
        //NO_COR, NO_TAM
        else
           Alinhamento := TAlignment.taLeftJustify
      end;
    end;
    case Alinhamento of
      TAlignment.taRightJustify:
        DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_RIGHT or DT_VCENTER or DT_END_ELLIPSIS);
      TAlignment.taLeftJustify:
        DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_LEFT or DT_VCENTER or DT_END_ELLIPSIS);
      TAlignment.taCenter:
        DrawText(GradeK.Canvas.Handle, PChar(CellText), Length(CellText), Rect, DT_SINGLELINE or DT_CENTER or DT_VCENTER or DT_END_ELLIPSIS);
    end;

  end;
  DesenhaLinha();
end;

procedure TFmStringGrid_UserDraw.GradeKFixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
var
  GR: TGridRect;
begin
  if (ACol = 0) and (ARow > 1) then
  begin
    GR.Left := 2;
    GR.Top := ARow;
    GR.Right := GradeK.ColCount - 1;;
    GR.Bottom := ARow;
    //
    GradeK.Selection := GR;
  end;
end;

procedure TFmStringGrid_UserDraw.GradeKMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //Label4.Caption := Geral.FF0(GradeK.Col) + '.' + Geral.FF0(GradeK.Row);
end;

procedure TFmStringGrid_UserDraw.GradeKMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  //
  procedure Selecao();
  var
    Col, Lin, I:Integer;
  begin
    I := 0;
    for Col := GradeK.Selection.Left to GradeK.Selection.Right do
      for Lin := GradeK.Selection.Top to GradeK.Selection.Bottom do
      begin
        I := I + 1;
        GradeK.Cells[Col, Lin] := Geral.FF0(I);
      end;
  end;
var
  Col, Row, ColI, ColF: Integer;
  GR: TGridRect;
begin
(*
  GradeK.MouseToCell(X, Y, Col, Row);
  Label6.Caption := Geral.FF0(Col) + '.' + Geral.FF0(Row);
  Label7.Caption :=
    Geral.FF0(GradeK.Selection.Left) + '.' + Geral.FF0(GradeK.Selection.Top) +
    ' >> ' +
    Geral.FF0(GradeK.Selection.Right) + '.' + Geral.FF0(GradeK.Selection.Bottom);
  //
*)
  Col := (GradeK.Selection.Left - 1) mod iColsPerItem;
  ColI := GradeK.Selection.Left - Col;
  Col := GradeK.Selection.Right mod iColsPerItem;
  if Col = 0 then
    Col := iColsPerItem;
  ColF := GradeK.Selection.Right + iColsPerItem - Col;
  //
  GR.Left := ColI;
  GR.Top := GradeK.Selection.Top;
  GR.Right := ColF;
  GR.Bottom := GradeK.Selection.Bottom;
  //
  GradeK.Selection := GR;
  //
(*
  Label8.Caption :=
    Geral.FF0(GradeK.Selection.Left) + '.' + Geral.FF0(GradeK.Selection.Top) +
    ' >> ' +
    Geral.FF0(GradeK.Selection.Right) + '.' + Geral.FF0(GradeK.Selection.Bottom);
  //
  Selecao();
*)
end;

procedure TFmStringGrid_UserDraw.GradeKTopLeftChanged(Sender: TObject);
begin
  GradeK.Invalidate;
end;

procedure TFmStringGrid_UserDraw.InsereItem(GG1Dest, GGXDest, GGXSorc, GG1Subst: Integer;
  QtdUso: Double);
var
  //GG1Dest, GGXDest, GGXSorc,
  Controle, GGXSubs, Parte, Obrigatorio: Integer;
  //QtdUso: Double;
  SQLType: TSQLType;
  IsOK: Boolean;
begin
  //SQLType        :=
  //GG1Dest        := ;
  //GGXDest        := ;
  //GGXSorc        := ;
  // Ver se j� existe
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT Controle',
  'FROM ficonsspc',
  'WHERE GG1Dest=' + Geral.FF0(GG1Dest),
  'AND GGXDest=' + Geral.FF0(GGXDest),
  EmptyStr]);
  Controle       := USQLDB.Int(DqAux, 'Controle');
  //
  if Controle <> 0 then
  begin
    SQLType := stUpd;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsspc', False, [
    'GGXSorc', 'QtdUso'], ['Controle'], [GGXSorc, QtdUso], [Controle], True);
  end else
  begin
    SQLType := stIns;
    //
    GGXSubs := 0;
    if GG1Subst <> 0 then
    begin
      GGXSubs := Dmod.MyDB.SelectInteger(
      ' SELECT ggx.Controle ' +
      ' FROM gragrux ggx ' +
      ' LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC' +
      ' WHERE ggx.GraGru1=' + Geral.FF0(GG1Subst) +
      ' AND ggx.GraTamI=' + Geral.FF0(FOriGraTamI) +
      ' AND ggc.GraCorCad=' + Geral.FF0(QrCoresCodigo.Value) +
      EmptyStr, IsOK, 0);
      if (IsOK <> True) then
        GGXSubs := 0;
    end;
    Parte          := 0;
    //QtdUso         := 0.000;
    Obrigatorio    := 1;

    //
    Controle := UMyMod.BPGS1I32('ficonsspc', 'Controle', '', '', tsPos, SQLType, Controle);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsspc', False, [
    'GG1Dest', 'GGXDest', 'GGXSorc',
    'GGXSubs', 'Parte', 'QtdUso',
    'Obrigatorio'], [
    'Controle'], [
    GG1Dest, GGXDest, GGXSorc,
    GGXSubs, Parte, QtdUso,
    Obrigatorio], [
    Controle], True);
  end;
end;

function TFmStringGrid_UserDraw.ObtemQuantidade(var q: Double): Boolean;
const
  FormCaption  = 'XXX-XXXXX-001 :: Quantidade';
  ValCaption   = 'Informe a quantidade:';
  WidthCaption = Length(ValCaption) * 7;
var
  ValVar: Variant;
begin
  Result := MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  q, 3, 0, '0,000', '', True, FormCaption, ValCaption, WidthCaption,
  ValVar);
  //
  if Result then
    q := Geral.DMV(ValVar);
end;

procedure TFmStringGrid_UserDraw.PreparaGrades();
var
  GR: TGridRect;
begin
  DmProd.ConfigGradesFiConsSpcTripleCol(FGG1Dest, FGraTamCadDest,
  GradeA, GradeC, GradeX, GradeK, iFixedCols, iFixedRows);
(*
  GradeK.MouseToCell(X, Y, Col, Row);
  Label6.Caption := Geral.FF0(Col) + '.' + Geral.FF0(Row);
  Label7.Caption :=
    Geral.FF0(GradeK.Selection.Left) + '.' + Geral.FF0(GradeK.Selection.Top) +
    ' >> ' +
    Geral.FF0(GradeK.Selection.Right) + '.' + Geral.FF0(GradeK.Selection.Bottom);
  //
*)
  // Primeiro item:
  GR.Left   := 1;
  GR.Top    := 2;
  GR.Right  := 3;
  GR.Bottom := 2;
  //
  GradeK.Selection := GR;
end;

procedure TFmStringGrid_UserDraw.QrCoresAfterScroll(DataSet: TDataSet);
begin
  FDBGClicked := False;
end;

procedure TFmStringGrid_UserDraw.QrCoresBeforeClose(DataSet: TDataSet);
begin
  FDBGClicked := False;
end;

procedure TFmStringGrid_UserDraw.QrSrcArtAfterScroll(DataSet: TDataSet);
begin
  ReopenSrcTam();
end;

procedure TFmStringGrid_UserDraw.QrSrcArtBeforeClose(DataSet: TDataSet);
begin
  QrSrcTam.Close;
end;

procedure TFmStringGrid_UserDraw.QrSrcCorAfterScroll(DataSet: TDataSet);
begin
  DmProd.AtualizaGradesFiConsSpcTripleCol(FGG1Dest, QrSrcArtNivel1.Value,
  QrSrcTamControle.Value, QrSrcCorCodigo.Value, GradeK, iFixedCols, iFixedRows);
  //
  GradeK.RowHeights[0] := GradeK.RowHeights[1] * 2;
  GradeK.Cells[0,0] := 'Teste de v�rias linhas '#13#10'Linha 2';
end;

procedure TFmStringGrid_UserDraw.QrSrcTamAfterScroll(DataSet: TDataSet);
begin
  ReopenSrcCor();
end;

procedure TFmStringGrid_UserDraw.QrSrcTamBeforeClose(DataSet: TDataSet);
begin
  QrSrcCor.Close;
end;

procedure TFmStringGrid_UserDraw.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmStringGrid_UserDraw.ReopenCoresOrigem(GraGru1, GraTamIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCores, Dmod.MyDB, [
  'SELECT gcc.Codigo, gcc.Nome, ggx.GraGruC ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
  'WHERE ggx.GraGru1=' + Geral.FF0(GraGru1),
  'AND ggx.GraTamI=' + Geral.FF0(GraTamIts),
  'ORDER BY gcc.Nome',
  EmptyStr]);

(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesCors, Dmod.MyDB, [
  'SELECT gc.Controle Cor, cc.Nome NOMECOR ',
  'FROM gragruc gc ',
  'LEFT JOIN gracorcad cc ON cc.Codigo=gc.GraCorCad ',
  'WHERE gc.Nivel1=' + Geral.FF0(Nivel1),
  'ORDER BY NOMECOR ',
  EmptyStr]);
*)  //

(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTams, Dmod.MyDB, [
  'SELECT Controle Tam, Nome NOMETAM ',
  'FROM gratamits ',
  'WHERE Codigo=' + Geral.FF0(Grade),
  EmptyStr]);
*)

end;

procedure TFmStringGrid_UserDraw.ReopenGraTamItsOrigem(GraGru1, GraTamCad: Integer);
var
  GraTamIts: Integer;
begin
  if GraGru1 <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraTamIts, Dmod.MyDB, [
    'SELECT Controle, Nome',
    'FROM gratamits',
    'WHERE Codigo=' + Geral.FF0(GraTamCad),
    'ORDER BY Controle',
    EmptyStr]);
    //
    if QrGraTamIts.RecordCount = 1 then
    begin
      EdGraTamIts.ValueVariant := QrGraTamItsControle.Value;
      CBGraTamIts.KeyValue     := QrGraTamItsControle.Value;
    end else
    begin
      GraTamIts := EdGraTamIts.ValueVariant;
      if GraTamIts <> 0 then
      begin
        if not QrGraTamIts.Locate('Controle', GraTamIts, []) then
        begin
          EdGraTamIts.ValueVariant := 0;
          CBGraTamIts.KeyValue     := Null;
        end;
      end;
    end;
  end else
  begin
    EdGraTamIts.ValueVariant := 0;
    CBGraTamIts.KeyValue     := Null;
    //
    QrGraTamIts.Close;
  end;
end;

procedure TFmStringGrid_UserDraw.ReopenSrcArt();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrcArt, Dmod.MyDB, [
  'SELECT gg11.Nivel1, gg11.Nome NO_GG1_Sorc, ',
  'COUNT(fcs.Controle) ITENS  ',
  'FROM ficonsspc fcs ',
  'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc ',
  'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  ',
  'WHERE fcs.GG1Dest=' + Geral.FF0(FGG1Dest),
  'GROUP BY gg11.Nivel1',
  'ORDER BY gg11.Nome ',
  EmptyStr]);
end;

procedure TFmStringGrid_UserDraw.ReopenSrcCor();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrcCor, Dmod.MyDB, [
  'SELECT gcc1.Codigo, gcc1.Nome NO_COR_Sorc, ',
  'COUNT(fcs.Controle) ITENS  ',
  'FROM ficonsspc fcs ',
  'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc ',
  'LEFT JOIN gragruc    ggc1 ON ggc1.Controle=ggx1.GraGruC  ',
  'LEFT JOIN gracorcad  gcc1 ON gcc1.Codigo=ggc1.GraCorCad  ',
  'LEFT JOIN gratamits  gti1 ON gti1.Controle=ggx1.GraTamI  ',
  'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  ',
  'WHERE fcs.GG1Dest=' + Geral.FF0(FGG1Dest),
  'AND gg11.Nivel1=' + Geral.FF0(QrSrcArtNivel1.Value),
  'AND gti1.Controle=' + Geral.FF0(QrSrcTamControle.Value),
  'GROUP BY gcc1.Codigo',
  'ORDER BY gcc1.Nome',
  EmptyStr]);
end;

procedure TFmStringGrid_UserDraw.ReopenSrcTam();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrcTam, Dmod.MyDB, [
  'SELECT gti1.Controle, gti1.Nome NO_TAM_Sorc, ',
  'COUNT(fcs.Controle) ITENS  ',
  'FROM ficonsspc fcs ',
  'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc ',
  'LEFT JOIN gratamits  gti1 ON gti1.Controle=ggx1.GraTamI  ',
  'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  ',
  'WHERE fcs.GG1Dest=' + Geral.FF0(FGG1Dest),
  'AND gg11.Nivel1=' + Geral.FF0(QrSrcArtNivel1.Value),
  'GROUP BY gti1.Controle',
  'ORDER BY gti1.Nome ',
  EmptyStr]);
end;

procedure TFmStringGrid_UserDraw.Sb_1to1Click(Sender: TObject);
var
  CellSel: Boolean;
  Col, Row, Ativo, GGXDest, GGXSorc, DBGc, DBGl, ColVal: Integer;
  IsOK: Boolean;
  QtdUso: Double;
begin
  (*
  DBGc := THackDBGrid(DBGCores).Col;
  DBGl := THackDBGrid(DBGCores).Row;
  if MyObjects.FIC((DBGc < 1) or (DBGl < 2), DBGCores,
  'Selecione a cor de origem!') then Exit;
  *)
  (*
  if MyObjects.FIC(DBGCores.SelectedRows.Count <> 1, DBGCores,
  'Selecione a cor de origem!') then Exit;
  *)
  if MyObjects.FIC(CBGraGru1.KeyValue = Null, EdGraGru1,
  'Selecione o produto de origem!') then Exit;
  //
  if MyObjects.FIC(CBGraTamIts.KeyValue = Null, EdGraTamIts,
  'Selecione o tamanho de origem!') then Exit;
  //
  if MyObjects.FIC(QrCores.RecordCount = 0, nil,
  'Selecione uma cor de origem v�lida!') then Exit;
  //
  if MyObjects.FIC(FDBGClicked = False, nil,
  'Selecione a cor de origem!') then Exit;
  //

  CellSel := (GradeK.Col > 0) and (GradeK.Row > 1);
  if MyObjects.FIC(CellSel = False, GradeK,
  'Selecione o reduzido de desino!') then Exit;
  //
  Col := ((GradeK.Col - iFixedCols) div iColsPerItem) + 1;
  Row := GradeK.Row; // - iFixedRows + 1;
  //
  Ativo   := Geral.IMV(GradeA.Cells[Col, Row]);
  GGXDest := Geral.IMV(GradeC.Cells[Col, Row]);
  //
  Geral.MB_Teste('Ativo = ' + Geral.FF0(Ativo) + sLineBreak +
  'Reduzido = ' + Geral.FF0(GGXDest));
  //
  // Fazer aqui select se existe
  //
  if (Ativo = 1) and (GGXDest <> 0) then
  begin
    //
    GGXSorc := Dmod.MyDB.SelectInteger(
    ' SELECT ggx.Controle ' +
    ' FROM gragrux ggx ' +
    ' WHERE ggx.GraGru1=' + Geral.FF0(FOriGraGru1) +
    ' AND ggx.GraTamI=' + Geral.FF0(FOriGraTamI) +
    ' AND ggx.GraGruC=' + Geral.FF0(QrCoresGraGruC.Value) +
    EmptyStr, IsOK, 0);
    if (IsOK = False) or (GGXSorc = 0) then
      Geral.MB_Aviso('Reduzido de origem n�o encontrado!')
    else
    begin
      ColVal := ((GradeK.Col  + (iColsPerItem -1)) div iColsPerItem) * iColsPerItem;
      QtdUso := Geral.DMV(GradeK.Cells[ColVal, GradeK.Row]);
      if ObtemQuantidade(QtdUso) then
      begin
        InsereItem(FGG1Dest, GGXDest, GGXSorc, QrGraGru1GG1Subst.Value, QtdUso);
        //
        DmProd.AtualizaGradesFiConsSpcTripleCol(FGG1Dest, FOriGraGru1,
        FOriGraTamI, QrCoresGraGruC.Value, GradeK, iFixedCols, iFixedRows);
      end;
    end;
  end else
  begin
    if GGXDest = 0 then
      Geral.MB_Aviso('Reduzido de destino n�o existe!')
    else
    if Ativo <> 1 then
      Geral.MB_Aviso('Reduzido de destino n�o est� ativo!')
    else
  end;
end;

end.
