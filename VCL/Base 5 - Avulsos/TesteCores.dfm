object FmTesteCores: TFmTesteCores
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-000 :: Form Base'
  ClientHeight = 691
  ClientWidth = 1109
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1109
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1061
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 1013
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 133
        Height = 32
        Caption = 'Form Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 133
        Height = 32
        Caption = 'Form Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 133
        Height = 32
        Caption = 'Form Base'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 1109
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 963
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 961
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Button1: TButton
        Left = 136
        Top = 12
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 1
        OnClick = Button1Click
      end
      object GBAvisos1: TGroupBox
        Left = 220
        Top = -3
        Width = 645
        Height = 44
        Caption = ' Avisos: '
        TabOrder = 2
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 641
          Height = 27
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1109
    Height = 573
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 31
      Height = 573
      Align = alLeft
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 164
        Width = 10
        Height = 13
        Caption = 'C:'
      end
      object Label2: TLabel
        Left = 8
        Top = 188
        Width = 12
        Height = 13
        Caption = 'M:'
      end
      object Label3: TLabel
        Left = 8
        Top = 212
        Width = 10
        Height = 13
        Caption = 'Y:'
      end
      object Label4: TLabel
        Left = 8
        Top = 236
        Width = 10
        Height = 13
        Caption = 'K:'
      end
      object Label5: TLabel
        Left = 8
        Top = 260
        Width = 14
        Height = 13
        Caption = 'W:'
      end
    end
    object Panel6: TPanel
      Left = 31
      Top = 0
      Width = 96
      Height = 573
      Align = alLeft
      TabOrder = 1
      object ShapeB: TShape
        Left = 4
        Top = 88
        Width = 29
        Height = 65
        Brush.Color = 14148589
        Pen.Color = clGray
        OnMouseUp = ShapeBMouseUp
      end
      object Label20: TLabel
        Left = 40
        Top = 88
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaBaseR: TLabel
        Left = 56
        Top = 88
        Width = 33
        Height = 13
        Caption = '237,28'
      end
      object Label22: TLabel
        Left = 40
        Top = 100
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaBaseG: TLabel
        Left = 56
        Top = 100
        Width = 33
        Height = 13
        Caption = '227,24'
      end
      object Label24: TLabel
        Left = 40
        Top = 112
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaBaseB: TLabel
        Left = 56
        Top = 112
        Width = 33
        Height = 13
        Caption = '214,50'
      end
      object Label27: TLabel
        Left = 24
        Top = 12
        Width = 29
        Height = 13
        Caption = 'Base'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ShFundo: TShape
        Left = 4
        Top = 284
        Width = 29
        Height = 65
        Brush.Color = 14148589
        Pen.Color = clGray
        OnMouseUp = Shape1MouseUp
      end
      object Label50: TLabel
        Left = 40
        Top = 284
        Width = 11
        Height = 13
        Caption = 'H:'
      end
      object Label52: TLabel
        Left = 40
        Top = 296
        Width = 10
        Height = 13
        Caption = 'S:'
      end
      object Label53: TLabel
        Left = 40
        Top = 308
        Width = 9
        Height = 13
        Caption = 'L:'
      end
      object LaBaseHslL: TLabel
        Left = 56
        Top = 308
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object LaBaseHslS: TLabel
        Left = 56
        Top = 296
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object LaBaseHslH: TLabel
        Left = 56
        Top = 284
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object EdBasePerc: TdmkEdit
        Left = 4
        Top = 60
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
      end
      object EdBaseC: TdmkEdit
        Left = 4
        Top = 160
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBaseM: TdmkEdit
        Left = 4
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBaseY: TdmkEdit
        Left = 4
        Top = 208
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBaseK: TdmkEdit
        Left = 4
        Top = 232
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBaseW: TdmkEdit
        Left = 4
        Top = 256
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMascC: TdmkEdit
        Left = 4
        Top = 356
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMascM: TdmkEdit
        Left = 4
        Top = 380
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMascY: TdmkEdit
        Left = 4
        Top = 404
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMascK: TdmkEdit
        Left = 4
        Top = 428
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMascW: TdmkEdit
        Left = 4
        Top = 452
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel7: TPanel
      Left = 127
      Top = 0
      Width = 96
      Height = 573
      Align = alLeft
      TabOrder = 2
      object Shape1: TShape
        Left = 4
        Top = 88
        Width = 29
        Height = 65
        Brush.Color = 7458038
        Pen.Color = clGray
        OnMouseUp = Shape1MouseUp
      end
      object Label9: TLabel
        Left = 40
        Top = 92
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaCor1R: TLabel
        Left = 56
        Top = 92
        Width = 33
        Height = 13
        Caption = '245,92'
      end
      object Label10: TLabel
        Left = 40
        Top = 114
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaCor1G: TLabel
        Left = 56
        Top = 114
        Width = 33
        Height = 13
        Caption = '203,57'
      end
      object Label11: TLabel
        Left = 40
        Top = 136
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaCor1B: TLabel
        Left = 56
        Top = 136
        Width = 33
        Height = 13
        Caption = '112,55'
      end
      object Label29: TLabel
        Left = 24
        Top = 12
        Width = 56
        Height = 13
        Caption = 'Corante 1'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label23: TLabel
        Left = 4
        Top = 40
        Width = 33
        Height = 13
        Caption = '% Sat.:'
      end
      object Label25: TLabel
        Left = 4
        Top = 64
        Width = 33
        Height = 13
        Caption = '% Uso:'
      end
      object ShapeMix1: TShape
        Left = 4
        Top = 284
        Width = 29
        Height = 65
        Brush.Color = 6868735
        Pen.Color = clGray
        OnMouseUp = Shape1MouseUp
      end
      object Label44: TLabel
        Left = 40
        Top = 288
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaMix1R: TLabel
        Left = 56
        Top = 288
        Width = 33
        Height = 13
        Caption = '245,92'
      end
      object LaMix1G: TLabel
        Left = 56
        Top = 310
        Width = 33
        Height = 13
        Caption = '203,57'
      end
      object Label47: TLabel
        Left = 40
        Top = 310
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object Label48: TLabel
        Left = 40
        Top = 332
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaMix1B: TLabel
        Left = 56
        Top = 332
        Width = 33
        Height = 13
        Caption = '112,55'
      end
      object Label64: TLabel
        Left = 8
        Top = 480
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object Label65: TLabel
        Left = 8
        Top = 502
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object Label66: TLabel
        Left = 8
        Top = 524
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaMixRYB1B: TLabel
        Left = 24
        Top = 524
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object LaMixRYB1Y: TLabel
        Left = 24
        Top = 502
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object LaMixRYB1R: TLabel
        Left = 24
        Top = 480
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object EdCor1Perc: TdmkEdit
        Left = 40
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
      end
      object EdCor1C: TdmkEdit
        Left = 4
        Top = 160
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor1M: TdmkEdit
        Left = 4
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor1Y: TdmkEdit
        Left = 4
        Top = 208
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor1K: TdmkEdit
        Left = 4
        Top = 232
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor1W: TdmkEdit
        Left = 4
        Top = 256
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor1Satu: TdmkEdit
        Left = 40
        Top = 36
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '3,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 3.000000000000000000
        ValWarn = False
      end
      object EdMix1C: TdmkEdit
        Left = 4
        Top = 356
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix1M: TdmkEdit
        Left = 4
        Top = 380
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix1Y: TdmkEdit
        Left = 4
        Top = 404
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix1K: TdmkEdit
        Left = 4
        Top = 428
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix1W: TdmkEdit
        Left = 4
        Top = 452
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel8: TPanel
      Left = 223
      Top = 0
      Width = 96
      Height = 573
      Align = alLeft
      TabOrder = 3
      object Shape2: TShape
        Left = 4
        Top = 88
        Width = 29
        Height = 65
        Brush.Color = 15846582
        Pen.Color = clGray
        OnMouseUp = Shape2MouseUp
      end
      object Label12: TLabel
        Left = 40
        Top = 92
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaCor2R: TLabel
        Left = 56
        Top = 92
        Width = 33
        Height = 13
        Caption = '181,69'
      end
      object Label13: TLabel
        Left = 40
        Top = 114
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaCor2G: TLabel
        Left = 56
        Top = 114
        Width = 33
        Height = 13
        Caption = '203,61'
      end
      object Label14: TLabel
        Left = 40
        Top = 136
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaCor2B: TLabel
        Left = 56
        Top = 136
        Width = 33
        Height = 13
        Caption = '241,03'
      end
      object Label31: TLabel
        Left = 24
        Top = 12
        Width = 56
        Height = 13
        Caption = 'Corante 2'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label35: TLabel
        Left = 4
        Top = 40
        Width = 33
        Height = 13
        Caption = '% Sat.:'
      end
      object Label37: TLabel
        Left = 4
        Top = 64
        Width = 33
        Height = 13
        Caption = '% Uso:'
      end
      object ShapeMix2: TShape
        Left = 4
        Top = 284
        Width = 29
        Height = 65
        Brush.Color = 16763304
        Pen.Color = clGray
        OnMouseUp = Shape2MouseUp
      end
      object Label45: TLabel
        Left = 40
        Top = 288
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaMix2R: TLabel
        Left = 56
        Top = 288
        Width = 33
        Height = 13
        Caption = '181,69'
      end
      object Label49: TLabel
        Left = 40
        Top = 310
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaMix2G: TLabel
        Left = 56
        Top = 310
        Width = 33
        Height = 13
        Caption = '203,61'
      end
      object Label51: TLabel
        Left = 40
        Top = 332
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaMix2B: TLabel
        Left = 56
        Top = 332
        Width = 33
        Height = 13
        Caption = '241,03'
      end
      object Label70: TLabel
        Left = 8
        Top = 480
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaMixRYB2R: TLabel
        Left = 24
        Top = 480
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object Label72: TLabel
        Left = 8
        Top = 502
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaMixRYB2Y: TLabel
        Left = 24
        Top = 502
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object Label74: TLabel
        Left = 8
        Top = 524
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaMixRYB2B: TLabel
        Left = 24
        Top = 524
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object EdCor2Perc: TdmkEdit
        Left = 40
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
      end
      object EdCor2C: TdmkEdit
        Left = 4
        Top = 160
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor2M: TdmkEdit
        Left = 4
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor2Y: TdmkEdit
        Left = 4
        Top = 208
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor2K: TdmkEdit
        Left = 4
        Top = 232
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor2W: TdmkEdit
        Left = 4
        Top = 256
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor2Satu: TdmkEdit
        Left = 40
        Top = 36
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '3,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 3.000000000000000000
        ValWarn = False
      end
      object EdMix2C: TdmkEdit
        Left = 4
        Top = 356
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix2M: TdmkEdit
        Left = 4
        Top = 380
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix2Y: TdmkEdit
        Left = 4
        Top = 404
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix2K: TdmkEdit
        Left = 4
        Top = 428
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMix2W: TdmkEdit
        Left = 4
        Top = 452
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel9: TPanel
      Left = 319
      Top = 0
      Width = 96
      Height = 573
      Align = alLeft
      TabOrder = 4
      object Shape3: TShape
        Left = 4
        Top = 88
        Width = 29
        Height = 65
        Pen.Color = clGray
        OnMouseUp = Shape3MouseUp
      end
      object Label15: TLabel
        Left = 40
        Top = 92
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaCor3R: TLabel
        Left = 56
        Top = 92
        Width = 33
        Height = 13
        Caption = '255,00'
      end
      object Label16: TLabel
        Left = 40
        Top = 114
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaCor3G: TLabel
        Left = 56
        Top = 114
        Width = 33
        Height = 13
        Caption = '255,00'
      end
      object Label17: TLabel
        Left = 40
        Top = 136
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaCor3B: TLabel
        Left = 56
        Top = 136
        Width = 33
        Height = 13
        Caption = '255,00'
      end
      object Label32: TLabel
        Left = 24
        Top = 12
        Width = 56
        Height = 13
        Caption = 'Corante 3'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label39: TLabel
        Left = 4
        Top = 40
        Width = 33
        Height = 13
        Caption = '% Sat.:'
      end
      object Label41: TLabel
        Left = 4
        Top = 64
        Width = 33
        Height = 13
        Caption = '% Uso:'
      end
      object EdCor3Perc: TdmkEdit
        Left = 40
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor3C: TdmkEdit
        Left = 4
        Top = 160
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor3M: TdmkEdit
        Left = 4
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor3Y: TdmkEdit
        Left = 4
        Top = 208
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor3K: TdmkEdit
        Left = 4
        Top = 232
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor3W: TdmkEdit
        Left = 4
        Top = 256
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor3Satu: TdmkEdit
        Left = 40
        Top = 36
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel10: TPanel
      Left = 415
      Top = 0
      Width = 96
      Height = 573
      Align = alLeft
      TabOrder = 5
      object Shape4: TShape
        Left = 4
        Top = 88
        Width = 29
        Height = 65
        Pen.Color = clGray
        OnMouseUp = Shape4MouseUp
      end
      object Label26: TLabel
        Left = 40
        Top = 92
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaCor4R: TLabel
        Left = 56
        Top = 92
        Width = 33
        Height = 13
        Caption = '255,00'
      end
      object Label28: TLabel
        Left = 40
        Top = 114
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaCor4G: TLabel
        Left = 56
        Top = 114
        Width = 33
        Height = 13
        Caption = '255,00'
      end
      object Label30: TLabel
        Left = 40
        Top = 136
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaCor4B: TLabel
        Left = 56
        Top = 136
        Width = 33
        Height = 13
        Caption = '255,00'
      end
      object Label33: TLabel
        Left = 24
        Top = 12
        Width = 56
        Height = 13
        Caption = 'Corante 4'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label42: TLabel
        Left = 4
        Top = 40
        Width = 33
        Height = 13
        Caption = '% Sat.:'
      end
      object Label43: TLabel
        Left = 4
        Top = 64
        Width = 33
        Height = 13
        Caption = '% Uso:'
      end
      object EdCor4Perc: TdmkEdit
        Left = 40
        Top = 60
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor4C: TdmkEdit
        Left = 4
        Top = 160
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor4M: TdmkEdit
        Left = 4
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor4Y: TdmkEdit
        Left = 4
        Top = 208
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor4K: TdmkEdit
        Left = 4
        Top = 232
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor4W: TdmkEdit
        Left = 4
        Top = 256
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor4Satu: TdmkEdit
        Left = 40
        Top = 36
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel11: TPanel
      Left = 511
      Top = 0
      Width = 96
      Height = 573
      Align = alLeft
      TabOrder = 6
      object ShapeS: TShape
        Left = 4
        Top = 88
        Width = 29
        Height = 65
        Brush.Color = 11199726
        Pen.Color = clGray
        OnMouseUp = ShapeSMouseUp
      end
      object Label34: TLabel
        Left = 40
        Top = 92
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object LaSmplR: TLabel
        Left = 56
        Top = 92
        Width = 33
        Height = 13
        Caption = '237,73'
      end
      object Label36: TLabel
        Left = 40
        Top = 114
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaSmplG: TLabel
        Left = 56
        Top = 114
        Width = 33
        Height = 13
        Caption = '228,20'
      end
      object Label38: TLabel
        Left = 40
        Top = 136
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object LaSmplB: TLabel
        Left = 56
        Top = 136
        Width = 33
        Height = 13
        Caption = '170,26'
      end
      object Label40: TLabel
        Left = 20
        Top = 12
        Width = 46
        Height = 13
        Caption = 'Amostra'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdSmplPerc: TdmkEdit
        Left = 4
        Top = 60
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
      end
      object EdSmplC: TdmkEdit
        Left = 4
        Top = 160
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSmplM: TdmkEdit
        Left = 4
        Top = 184
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSmplY: TdmkEdit
        Left = 4
        Top = 208
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSmplK: TdmkEdit
        Left = 4
        Top = 232
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSmplW: TdmkEdit
        Left = 4
        Top = 256
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel13: TPanel
      Left = 607
      Top = 0
      Width = 182
      Height = 573
      Align = alLeft
      TabOrder = 7
      object ShapeIOri: TShape
        Left = 2
        Top = 56
        Width = 84
        Height = 29
        Pen.Color = clWhite
        OnMouseUp = ShapeSMouseUp
      end
      object Label63: TLabel
        Left = 20
        Top = 12
        Width = 66
        Height = 13
        Caption = 'Importa Cor'
        Font.Charset = ANSI_CHARSET
        Font.Color = 13762560
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ShapeICln: TShape
        Left = 90
        Top = 56
        Width = 84
        Height = 29
        Pen.Color = clWhite
        OnMouseUp = ShapeSMouseUp
      end
      object dmkEdit2: TdmkEdit
        Left = 4
        Top = 32
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
      end
      object SGIOri: TStringGrid
        Left = 2
        Top = 88
        Width = 84
        Height = 373
        ColCount = 2
        DefaultColWidth = 24
        DefaultRowHeight = 15
        RowCount = 23
        FixedRows = 0
        TabOrder = 1
        ColWidths = (
          24
          51)
      end
      object SGICln: TStringGrid
        Left = 90
        Top = 88
        Width = 84
        Height = 277
        ColCount = 2
        DefaultColWidth = 24
        DefaultRowHeight = 15
        RowCount = 17
        FixedRows = 0
        TabOrder = 2
        ColWidths = (
          24
          51)
      end
      object BtMtzRYB: TBitBtn
        Left = 36
        Top = 516
        Width = 75
        Height = 25
        Caption = 'Matiz RYB'
        TabOrder = 3
        OnClick = BtMtzRYBClick
      end
    end
    object Panel12: TPanel
      Left = 789
      Top = 0
      Width = 320
      Height = 573
      Align = alClient
      TabOrder = 8
      object LaMixB: TLabel
        Left = 284
        Top = 96
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object Label8: TLabel
        Left = 268
        Top = 96
        Width = 10
        Height = 13
        Caption = 'B:'
      end
      object Label7: TLabel
        Left = 268
        Top = 74
        Width = 11
        Height = 13
        Caption = 'G:'
      end
      object LaMixG: TLabel
        Left = 284
        Top = 74
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object LaMixR: TLabel
        Left = 284
        Top = 52
        Width = 21
        Height = 13
        Caption = '0,00'
      end
      object Label6: TLabel
        Left = 268
        Top = 52
        Width = 11
        Height = 13
        Caption = 'R:'
      end
      object ShapeM: TShape
        Left = 232
        Top = 52
        Width = 29
        Height = 65
        OnMouseUp = ShapeMMouseUp
      end
      object Label56: TLabel
        Left = 128
        Top = 36
        Width = 33
        Height = 13
        Caption = '% Sat.:'
        Enabled = False
      end
      object Label18: TLabel
        Left = 12
        Top = 64
        Width = 28
        Height = 13
        Caption = 'Cor 1:'
      end
      object Label19: TLabel
        Left = 12
        Top = 88
        Width = 28
        Height = 13
        Caption = 'Cor 2:'
      end
      object Label46: TLabel
        Left = 12
        Top = 112
        Width = 27
        Height = 13
        Caption = 'Total:'
        Enabled = False
      end
      object Label21: TLabel
        Left = 12
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Base:'
        Enabled = False
      end
      object LaCor1Satu: TLabel
        Left = 136
        Top = 64
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaCor2Satu: TLabel
        Left = 136
        Top = 88
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label60: TLabel
        Left = 176
        Top = 36
        Width = 37
        Height = 13
        Caption = '% Red.:'
        Enabled = False
      end
      object LaCor1Redu: TLabel
        Left = 188
        Top = 64
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaCor2Redu: TLabel
        Left = 188
        Top = 88
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaTotaSatu: TLabel
        Left = 136
        Top = 112
        Width = 9
        Height = 13
        Caption = '...'
      end
      object LaTotaRedu: TLabel
        Left = 188
        Top = 112
        Width = 9
        Height = 13
        Caption = '...'
      end
      object ShLoadMid: TShape
        Left = 48
        Top = 160
        Width = 29
        Height = 65
        Pen.Color = clGray
        OnMouseUp = Shape3MouseUp
      end
      object ShLoadCln: TShape
        Left = 76
        Top = 160
        Width = 29
        Height = 65
        Pen.Color = clGray
        OnMouseUp = Shape3MouseUp
      end
      object ShLoadDrk: TShape
        Left = 104
        Top = 160
        Width = 29
        Height = 65
        Pen.Color = clGray
        OnMouseUp = Shape3MouseUp
      end
      object Label54: TLabel
        Left = 12
        Top = 232
        Width = 11
        Height = 13
        Caption = '%:'
      end
      object Shape5: TShape
        Left = 184
        Top = 148
        Width = 29
        Height = 65
        Pen.Color = clGray
        OnMouseUp = Shape3MouseUp
      end
      object SpeedButton1: TSpeedButton
        Left = 188
        Top = 220
        Width = 23
        Height = 22
        OnClick = SpeedButton1Click
      end
      object Label55: TLabel
        Left = 188
        Top = 252
        Width = 14
        Height = 13
        Caption = 'R:'
        Font.Charset = ANSI_CHARSET
        Font.Color = 50372
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label57: TLabel
        Left = 188
        Top = 274
        Width = 13
        Height = 13
        Caption = 'Y:'
        Font.Charset = ANSI_CHARSET
        Font.Color = 50372
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label58: TLabel
        Left = 188
        Top = 296
        Width = 13
        Height = 13
        Caption = 'B:'
        Font.Charset = ANSI_CHARSET
        Font.Color = 50372
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaRYBb: TLabel
        Left = 204
        Top = 296
        Width = 40
        Height = 13
        Caption = '255,00'
        Font.Charset = ANSI_CHARSET
        Font.Color = 50372
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaRYBy: TLabel
        Left = 204
        Top = 274
        Width = 40
        Height = 13
        Caption = '255,00'
        Font.Charset = ANSI_CHARSET
        Font.Color = 50372
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaRYBr: TLabel
        Left = 204
        Top = 252
        Width = 40
        Height = 13
        Caption = '255,00'
        Font.Charset = ANSI_CHARSET
        Font.Color = 50372
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label59: TLabel
        Left = 256
        Top = 252
        Width = 14
        Height = 13
        Caption = 'R:'
        Color = clAqua
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label61: TLabel
        Left = 256
        Top = 274
        Width = 14
        Height = 13
        Caption = 'G:'
        Color = clAqua
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label62: TLabel
        Left = 256
        Top = 296
        Width = 13
        Height = 13
        Caption = 'B:'
        Color = clAqua
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LaRGBb: TLabel
        Left = 272
        Top = 296
        Width = 40
        Height = 13
        Caption = '255,00'
        Color = clAqua
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LaRGBg: TLabel
        Left = 272
        Top = 274
        Width = 40
        Height = 13
        Caption = '255,00'
        Color = clAqua
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object LaRGBr: TLabel
        Left = 272
        Top = 252
        Width = 40
        Height = 13
        Caption = '255,00'
        Color = clAqua
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object BtMatiz: TBitBtn
        Left = 12
        Top = 28
        Width = 75
        Height = 25
        Caption = 'Matiz'
        TabOrder = 0
        OnClick = BtMatizClick
      end
      object EdMixW: TdmkEdit
        Left = 232
        Top = 220
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMixK: TdmkEdit
        Left = 232
        Top = 196
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMixY: TdmkEdit
        Left = 232
        Top = 172
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMixM: TdmkEdit
        Left = 232
        Top = 148
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMixC: TdmkEdit
        Left = 232
        Top = 124
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor0Perc: TdmkEdit
        Left = 232
        Top = 28
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCor1Matz: TdmkEdit
        Left = 48
        Top = 60
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,500000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.500000000000000000
        ValWarn = False
        OnRedefinido = EdCor1MatzRedefinido
      end
      object EdCor2Matz: TdmkEdit
        Left = 48
        Top = 84
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,500000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.500000000000000000
        ValWarn = False
        OnRedefinido = EdCor2MatzRedefinido
      end
      object EdCorTMatz: TdmkEdit
        Left = 48
        Top = 108
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2.000000000000000000
        ValWarn = False
      end
      object EdCorBMatiz: TdmkEdit
        Left = 48
        Top = 132
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object dmkEdit1: TdmkEdit
        Left = 48
        Top = 228
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,500000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.500000000000000000
        ValWarn = False
        OnRedefinido = EdCor1MatzRedefinido
      end
      object SGHSL: TStringGrid
        Left = 1
        Top = 317
        Width = 318
        Height = 255
        Align = alBottom
        ColCount = 4
        DefaultRowHeight = 21
        RowCount = 242
        TabOrder = 12
        RowHeights = (
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21
          21)
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 296
    Top = 65531
  end
  object PMOK: TPopupMenu
    Left = 392
    Top = 420
    object CMYKW1: TMenuItem
      Caption = 'CMYKW4'
      OnClick = CMYKW1Click
    end
    object CMY1: TMenuItem
      Caption = 'CMY'
      OnClick = CMY1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Importarcor1: TMenuItem
      Caption = 'Importar cor'
      object Base1: TMenuItem
        Caption = 'Base'
        OnClick = Base1Click
      end
      object Corante11: TMenuItem
        Caption = 'Corante 1'
        OnClick = Corante11Click
      end
      object Corante21: TMenuItem
        Caption = 'Corante 2'
        OnClick = Corante21Click
      end
      object Corante31: TMenuItem
        Caption = 'Corante 3'
        OnClick = Corante31Click
      end
      object Corante41: TMenuItem
        Caption = 'Corante 4'
        OnClick = Corante41Click
      end
      object Amostra1: TMenuItem
        Caption = 'Amostra'
        OnClick = Amostra1Click
      end
    end
  end
  object ColorDialog1: TColorDialog
    Left = 760
    Top = 36
  end
end
