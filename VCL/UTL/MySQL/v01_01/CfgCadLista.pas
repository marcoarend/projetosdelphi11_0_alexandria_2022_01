unit CfgCadLista;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Variants, dmkEdit,
  dmkGeral, mySQLDbTables, DB,(* DbTables,*) DBGrids, mySQLExceptions, ComCtrls,
  dmkDBGridZTO, UndmkProcFunc, UnDmkEnums;

type
  TUnCfgCadLista = class(TObject)
  private
    { Private declarations }
    (*
    procedure MostraCadRegistro(DB: TmySQLDataBase; Tabela: String;
      SizeNome: Integer; NovoCodigo: TNovoCodigo; Titulo: String);
    *)
  public
    { Public declarations }
    procedure MostraCadLista(DB: TmySQLDataBase; Tabela: String; SizeNome:
              Integer; NovoCodigo: TNovoCodigo; Titulo: String; ListaVars:
              array of String; SoReservados: Boolean; Reservados: Variant;
              FldIndices: array of String; ValIndices: array of Variant;
              PermiteExcluir: Boolean; Codigo: Integer = 0;
              SQLInsExtra: String = '');
    procedure MostraCadListaOrdem((*InstanceClass: TComponentClass; var Reference;*)
              DB: TmySQLDataBase; Tabela: String; SizeNome:
              Integer; NovoCodigo: TNovoCodigo; Titulo: String;
              ListaVars: array of String;
              SoReservados: Boolean; Reservados: Variant;
              FldIndices: array of String; ValIndices: array of Variant;
              PermiteExcluir: Boolean; Codigo: Integer = 0);
    procedure MostraCadRegistroSimples(DB: TmySQLDataBase; Tabela: TmySqlTable;
              DataSource: TDataSource; NovoCodigo: TNovoCodigo; Titulo: String;
              Codigo: Integer = 0);
    procedure MostraCadItemGrupo(DB: TmySQLDataBase; Tabela: String;
              Lista: TAppGrupLst; NovoCodigo: TNovoCodigo; Codigo: Integer = 0);
    procedure InsAltItemLstEmRegTab(DestTabela, DestFldItem, DestFldID,
              DestFldValr, SrcTabela, SrcFldCodi, SrcFldNome, _WHERE_: String;
              DefaultItem, DefaultValr: Variant; TipoItem, TipoValr: TAllFormat;
              Tamanho1, Tamanho2: Integer; SQLType: TSQLType; Titulo,
              PromptItem, PromptValr: String; DestNovoCodigo, SrcNovoCodigo:
              TNovoCodigo; SQLIndex: array of String;
              ValIndex: array of Variant; Qry: TmySQLQuery);
    function  LocalizaPesquisadoInt1(Tb: TmySQLTable; Qr: TmySQLQuery; FldID:
              String; Avisa: Boolean): Boolean;
    procedure AddVariavelEmTexto(ListBox: TListBox; Table: TmySQLTable;
              FldNome: String; DBGrid: TdmkDBGridZTO);
    procedure ReabrePesquisa(Qr: TmySQLQuery; Tb: TmySQLTable;
              EdPesq: TEdit; TrackBar: TTrackBar; FldID, FldNome: String);
    function  IncluiCodigoUser(const Reservados: Variant; const Tb: TmySQLTable;
              const FldID: String; var ID: Integer): Boolean;
    function  IncluiNovoRegistro(ModoInclusao: TdmkModoInclusao;
              Form: TForm; Panel: TWinControl; WinCtrlsToHide,
              WinCtrlsToShow: array of TWinControl; CompoToFocus: TWinControl;
              CompoTipo: TControl; Reservados: Variant; Tb: TmySQLTable;
              FldID: String): Boolean;
    procedure TbBeforePost(Tb: TmySQLTable; NovoCodigo: TNovoCodigo;
              FldID: String; Reservados: Variant);
    function  PoeTbEmEdicao(Tb: TmySQLTable; SQLType: TSQLType): Boolean;
  end;

var
  UnCfgCadLista: TUnCfgCadLista;

implementation

uses CadLista, CadItemGrupo, CadRegistroSimples, MyDBCheck, SelCod,
  UMySQLModule, Module, DmkDAC_PF, GetValor, UnMyObjects, CadListaOrdem;

{ TUnCfgCadLista }

procedure TUnCfgCadLista.AddVariavelEmTexto(ListBox: TListBox; Table:
TmySQLTable; FldNome: String; DBGrid: TdmkDBGridZTO);
var
  Texto: String;
  Ate: Integer;
begin
  Texto := ListBox.Items[ListBox.ItemIndex];
  if Texto <> '' then
  begin
    Ate := Pos(']', Texto);
    Texto := Copy(Texto, 1, Ate);
    Table.Edit;
    Table.FieldByName(FldNome).Value := Table.FieldByName(FldNome).Value + Texto;
    Table.Post;
  end;
  DBGrid.SetFocus;
end;

function TUnCfgCadLista.IncluiCodigoUser(const Reservados: Variant;
const Tb: TmySQLTable; const FldID: String; var ID: Integer): Boolean;
var
  Codigo: Variant;
  Reserv: Integer;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  ID := 0;
  Codigo := 0;
  Reserv := 0;
  if Reservados <> Null then
    Reserv := Reservados;
  //
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Codigo, 0, 0,
  '', Geral.FF0(Reserv), True, 'C�digo', 'Informe o C�digo: ', 0, Codigo) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      Qry.Database := Dmod.MyDB;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + FldID,
      'FROM ' + Tb.TableName,
      'WHERE ' + FldID + '=' + Geral.FF0(Codigo),
      '']);
      if Qry.RecordCount > 0 then
      begin
        Geral.MB_Aviso('O c�digo ' + String(Codigo) +
        ' j� est� sendo usado em outro registro!');
        Exit;
      end else
      begin
        if UMyMod.SQLInsUpd(Qry, stIns, TB.TableName, False, [
        ], [
        'Codigo'], [
        ], [
        Codigo], True) then
        begin
          ID := Codigo;
          Result := True;
          Tb.Refresh;
          Tb.Locate(FldID, Codigo, []);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnCfgCadLista.IncluiNovoRegistro(ModoInclusao: TdmkModoInclusao;
Form: TForm; Panel: TWinControl; WinCtrlsToHide,
WinCtrlsToShow: array of TWinControl; CompoToFocus: TWinControl;
CompoTipo: TControl; Reservados: Variant; Tb: TmySQLTable; FldID: String): Boolean;
var
  Codigo: Integer;
begin
  Result := False;
  case ModoInclusao of
    dmkmiAutomatico:
    begin
      Result := UMyMod.ConfigPanelInsUpd(stIns, Form, Panel, TmySQLQuery(Tb),
        WinCtrlsToHide, WinCtrlsToShow, CompoToFocus, CompoTipo, Tb.TableName);
    end;
    dmkmiUserDefine:
    begin
      Result := UnCfgCadLista.IncluiCodigoUser(Reservados, Tb, FldID, Codigo);
      //
      if Result then
      UMyMod.ConfigPanelInsUpd(stUpd, Form, Panel, TmySQLQuery(Tb),
        WinCtrlsToHide, WinCtrlsToShow, CompoToFocus, CompoTipo, Tb.TableName);
    end
    else Geral.MB_Aviso('Modo de inclus�o n�o definido!' + sLineBreak +
    'CfgCadLista.IncluiNovoRegistro()');
  end;
  //
end;

procedure TUnCfgCadLista.InsAltItemLstEmRegTab(DestTabela, DestFldItem, DestFldID,
  DestFldValr, SrcTabela, SrcFldCodi, SrcFldNome, _WHERE_: String;
  DefaultItem, DefaultValr: Variant; TipoItem, TipoValr: TAllFormat;
  Tamanho1, Tamanho2: Integer; SQLType: TSQLType; Titulo,
  PromptItem, PromptValr: String; DestNovoCodigo, SrcNovoCodigo:
  TNovoCodigo; SQLIndex: array of String;
  ValIndex: array of Variant; Qry: TmySQLQuery);
var
  Idx: Integer;
  UsaValr: Boolean;
  //
  procedure InsAlt(Item, Valr: Variant);
  {
  var
    Codigo, Controle, Caracteris: Integer;
  }
  begin
    Idx := 0;
    if SQLType = stIns then
    begin
      if TipoItem = dmktfInteger then
      begin
        case DestNovoCodigo of
          ncControle: Idx := UMyMod.BuscaNovoCodigo_Int(
            Dmod.QrAux, DestTabela, DestFldID, [], [], stIns, 0, siPositivo, nil);
          ncGerlSeq1: Idx := UMyMod.BPGS1I32(
            DestTabela, DestFldID, '', '', tsDef, stIns, 0);
          else
          begin
            Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido! [2]');
            Halt(0);
            Exit;
          end;
        end;
      end;
      ValIndex[High(ValIndex)] := Idx;
    end;
    if UsaValr then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, DestTabela, False, [
      DestFldItem, DestFldValr], SQLIndex, [
      Item, Valr], ValIndex, True);
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, DestTabela, False, [
      DestFldItem(*, DestFldValr*)], SQLIndex, [
      Item(*, Valr*)], ValIndex, True);
    end;
    if Qry <> nil then
    begin
      Qry.Close;
      UnDmkDAC_PF.AbreQueryApenas(Qry);
      Qry.Locate(SQLIndex[High(SQLIndex)], ValIndex[High(ValIndex)], []);
    end;
  end;
  procedure MostraForm(var ContinuaMostrando: Boolean);
  begin
    Application.CreateForm(TFmSelCod, FmSelCod);
    FmSelCod.ImgTipo.SQLType := SQLType;
    FmSelCod.Caption := FmSelCod.FIDThis + ' :: ' + Titulo;
    FmSelCod.LaPrompt.Caption := PromptItem;
    FmSelCod.QrSel.Close;
    FmSelCod.QrSel.Database := Dmod.MyDB;
    FmSelCod.QrSel.SQL.Clear;
    FmSelCod.QrSel.SQL.Add('SELECT ' + SrcFldNome + ' Descricao, ' + SrcFldCodi +  ' Codigo');
    FmSelCod.QrSel.SQL.Add('FROM ' + SrcTabela);
    FmSelCod.QrSel.SQL.Add(_WHERE_);
    FmSelCod.QrSel.SQL.Add('ORDER BY ' + SrcFldNome);
    UnDmkDAC_PF.AbreQueryApenas(FmSelCod.QrSel);
    //
    FmSelCod.CBSel.LocF7TableName := SrcTabela;
    FmSelCod.CBSel.LocF7CodiFldName := SrcFldCodi;
    FmSelCod.CBSel.LocF7NameFldName := SrcFldNome;
    //
    FmSelCod.FTabela := SrcTabela;
    FmSelCod.FNovoCodigo := SrcNovoCodigo;
    FmSelCod.SBCadastro.Visible := True;
    //
    if DefaultItem <> Null then
    begin
      FmSelCod.EdSel.ValueVariant := DefaultItem;
      FmSelCod.CBSel.KeyValue     := DefaultItem;
    end;
    UsaValr := TipoValr <> dmktfNone;
    if UsaValr then
    begin
      FmSelCod.LaOrdem.Visible := True;
      FmSelCod.LaOrdem.Caption := PromptValr;
      FmSelCod.dmkEdOrdem.Visible := True;
      FmSelCod.dmkEdOrdem.FormatType := TipoValr;
      case TipoValr of
        //dmktfNone: ;
        dmktfString: ;
        dmktfDouble: FmSelCod.dmkEdOrdem.DecimalSize := Tamanho2;
        dmktfInteger: FmSelCod.dmkEdOrdem.LeftZeros := Tamanho2;
        {
        dmktfLongint: ;
        dmktfInt64: ;
        dmktfDate: ;
        dmktfTime: ;
        dmktfDateTime: ;
        dmktfMesAno: ;
        dmktf_AAAAMM: ;
        dmktfUnknown: ;
        }
      end;
      if DefaultValr <> Null then
        FmSelCod.dmkEdOrdem.ValueVariant := DefaultValr;
    end else begin
      FmSelCod.LaOrdem.Visible := False;
      FmSelCod.dmkEdOrdem.Visible := False;
    end;
    FmSelCod.CkContinua.Visible := True;
    FmSelCod.CkContinua.Checked := True;
    FmSelCod.ShowModal;
    if FmSelCod.FSelected then
    begin
      InsAlt(FmSelCod.EdSel.ValueVariant,
        FmSelCod.dmkEdOrdem.ValueVariant);
    end;
    ContinuaMostrando := FmSelCod.FContinua;
    FmSelCod.Destroy;
  end;
var
  Mostra: Boolean;
begin
  Mostra := True;
  while Mostra do
    MostraForm(Mostra);
end;

procedure TUnCfgCadLista.MostraCadLista((*InstanceClass: TComponentClass;
  var Reference;*) DB: TmySQLDataBase; Tabela: String;
  SizeNome: Integer; NovoCodigo: TNovoCodigo; Titulo: String;
  ListaVars: array of String; SoReservados: Boolean; Reservados: Variant;
  FldIndices: array of String; ValIndices: array of Variant;
  PermiteExcluir: Boolean; Codigo: Integer = 0; SQLInsExtra: String = '');
var
  I, N: Integer;
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCadLista, FmCadLista, afmoNegarComAviso) then
  begin
    FmCadLista.FPermiteExcluir := PermiteExcluir;
    FmCadLista.FReservados := Reservados;
    FmCadLista.FSoReserva  := SoReservados;
    //FmCadLista.BtInclui.Visible := Reservados <> Null;
    FmCadLista.FDBGInsert := SoReservados = False;
    FmCadLista.FTabela := Lowercase(Tabela);
    FmCadLista.FSQLInsExtra := SQLInsExtra;
    FmCadLista.Caption := 'CAD-LISTA-001 :: ' + Titulo;
    FmCadLista.TbCad.DataBase := DB;
    FmCadLista.TbCad.TableName := Lowercase(Tabela);
    FmCadLista.FNovoCodigo := NovoCodigo;
    if SizeNome > 0 then
    begin
      FmCadLista.TbCadNome.DisplayWidth := SizeNome;
      FmCadLista.TbCadNome.Size := SizeNome;
    end;
    UnDmkDAC_PF.AbreTable(FmCadLista.TbCad, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FmCadLista.TbCad.Locate('Codigo', Codigo, []);
    //
    N := High(ListaVars);
    if N >= 0 then
    begin
      FmCadLista.LbItensMD.Items.Clear;
      for I := Low(ListaVars) to N do
        FmCadLista.LbItensMD.Items.Add(ListaVars[I]);
      FmCadLista.LbItensMD.Visible := True;
    end;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    N := High(FldIndices);
    if N >= 0 then
    begin
      SetLength(FmCadLista.FFldIndices, N + 1);
      if N >= 0 then
      begin
        for I := Low(FldIndices) to N do
          FmCadLista.FFldIndices[I] := FldIndices[I];
      end;
      //
      N := High(ValIndices);
      SetLength(FmCadLista.FValIndices, N + 1);
      if N >= 0 then
      begin
        for I := Low(ValIndices) to N do
          FmCadLista.FValIndices[I] := ValIndices[I];
      end;
    end;
*)
    //
    FmCadLista.ShowModal;
    FmCadLista.Destroy;
    //
  end;
end;

procedure TUnCfgCadLista.MostraCadListaOrdem(DB: TmySQLDataBase; Tabela: String;
  SizeNome: Integer; NovoCodigo: TNovoCodigo; Titulo: String;
  ListaVars: array of String; SoReservados: Boolean; Reservados: Variant;
  FldIndices: array of String; ValIndices: array of Variant;
  PermiteExcluir: Boolean; Codigo: Integer);
var
  I, N: Integer;
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCadListaOrdem, FmCadListaOrdem, afmoNegarComAviso) then
  begin
    FmCadListaOrdem.FPermiteExcluir := PermiteExcluir;
    FmCadListaOrdem.FReservados := Reservados;
    FmCadListaOrdem.FSoReserva  := SoReservados;
    //FmCadListaOrdem.BtInclui.Visible := Reservados <> Null;
    FmCadListaOrdem.FDBGInsert := SoReservados = False;
    FmCadListaOrdem.Caption := 'CAD-LISTA-001 :: ' + Titulo;
    FmCadListaOrdem.TbCad.DataBase := DB;
    FmCadListaOrdem.TbCad.TableName := Lowercase(Tabela);
    FmCadListaOrdem.FNovoCodigo := NovoCodigo;
    if SizeNome > 0 then
    begin
      FmCadListaOrdem.TbCadNome.DisplayWidth := SizeNome;
      FmCadListaOrdem.TbCadNome.Size := SizeNome;
    end;
    UnDmkDAC_PF.AbreTable(FmCadListaOrdem.TbCad, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FmCadListaOrdem.TbCad.Locate('Codigo', Codigo, []);
    //
    N := High(ListaVars);
    if N >= 0 then
    begin
      FmCadListaOrdem.LbItensMD.Items.Clear;
      for I := Low(ListaVars) to N do
        FmCadListaOrdem.LbItensMD.Items.Add(ListaVars[I]);
      FmCadListaOrdem.LbItensMD.Visible := True;
    end;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    N := High(FldIndices);
    if N >= 0 then
    begin
      SetLength(FmCadListaOrdem.FFldIndices, N + 1);
      if N >= 0 then
      begin
        for I := Low(FldIndices) to N do
          FmCadListaOrdem.FFldIndices[I] := FldIndices[I];
      end;
      //
      N := High(ValIndices);
      SetLength(FmCadListaOrdem.FValIndices, N + 1);
      if N >= 0 then
      begin
        for I := Low(ValIndices) to N do
          FmCadListaOrdem.FValIndices[I] := ValIndices[I];
      end;
    end;
*)
    //
    FmCadListaOrdem.ShowModal;
    FmCadListaOrdem.Destroy;
    //
  end;
end;

{
procedure TUnCfgCadLista.MostraCadRegistro(DB: TmySQLDataBase; Tabela: String;
  SizeNome: Integer; NovoCodigo: TNovoCodigo; Titulo: String);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCadLista, FmCadLista, afmoNegarComAviso) then
  begin
    FmCadLista.Caption := 'CAD-LISTA-001 :: ' + Titulo;
    FmCadLista.TbCad.DataBase := DB;
    FmCadLista.TbCad.TableName := Tabela;
    FmCadLista.FNovoCodigo := NovoCodigo;
    if SizeNome > 0 then
    begin
      FmCadLista.TbCadNome.DisplayWidth := SizeNome;
      FmCadLista.TbCadNome.Size := SizeNome;
    end;
    UnDmkDAC_PF.AbreQueryApenas(FmCadLista.TbCad);
    //
    FmCadLista.ShowModal;
    FmCadLista.Destroy;
    //
  end;
end;
}

procedure TUnCfgCadLista.MostraCadRegistroSimples(DB: TmySQLDataBase;
  Tabela: TmySQLTable; DataSource: TDataSource; NovoCodigo: TNovoCodigo;
  Titulo: String; Codigo: Integer = 0);
var
  I, N: Integer;
  Fator: Double;
begin
  Tabela.Database := DB;
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCadRegistroSimples, FmCadRegistroSimples, afmoNegarComAviso) then
  begin
    FmCadRegistroSimples.Caption     := 'CAD-LISTA-003 :: ' + Titulo;
    FmCadRegistroSimples.FTbCad      := Tabela;
    FmCadRegistroSimples.FNovoCodigo := NovoCodigo;
    FmCadRegistroSimples.FCodigo     := Codigo;
    FmCadRegistroSimples.FTbCad.AfterPost := FmCadRegistroSimples.TbCadAfterPost;
    FmCadRegistroSimples.FTbCad.OnDeleting := FmCadRegistroSimples.TbCadDeleting;
    FmCadRegistroSimples.FTbCad.BeforePost := FmCadRegistroSimples.TbCadBeforePost;
    UnDmkDAC_PF.AbreTable(FmCadRegistroSimples.FTbCad, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FmCadRegistroSimples.FTbCad.Locate('Codigo', Codigo, []);
    //
    FmCadRegistroSimples.DBGrid1.DataSource := DataSource;
    FmCadRegistroSimples.frxDsCad.DataSet := Tabela;
    //
    N := 0;
    for I := 0 to FmCadRegistroSimples.DBGrid1.Columns.Count - 1 do
    begin
      if FmCadRegistroSimples.DBGrid1.Columns[I].Width > 400 then
        FmCadRegistroSimples.DBGrid1.Columns[I].Width := 400;
      N := N + FmCadRegistroSimples.DBGrid1.Columns[I].Width;
    end;
    Fator := (FmCadRegistroSimples.DBGrid1.Width - 40) / N;
    if Fator < 1 then
    begin
      for I := 0 to FmCadRegistroSimples.DBGrid1.Columns.Count - 1 do
      begin
        FmCadRegistroSimples.DBGrid1.Columns[I].Width := Trunc(
          Fator * FmCadRegistroSimples.DBGrid1.Columns[I].Width);
      end;
    end;
    //
    FmCadRegistroSimples.ShowModal;
    FmCadRegistroSimples.Destroy;
    //
  end;
end;

function TUnCfgCadLista.PoeTbEmEdicao(Tb: TmySQLTable; SQLType: TSQLType): Boolean;
begin
  Result := False;
  case SQLType of
    stIns: TB.Insert;
    stUpd: TB.Edit;
    else
    begin
      Geral.MB_Aviso(
      '"SQLType" indefinido em "UnCfgCadLista.PoeTbEmEdicao()"');
      Exit;
    end;
  end;
  Result := True;
end;

procedure TUnCfgCadLista.ReabrePesquisa(Qr: TmySQLQuery; Tb: TmySQLTable;
EdPesq: TEdit; TrackBar: TTrackBar; FldID, FldNome: String);
var
  Texto, SQL: String;
  K, N, I: Integer;
begin
  Qr.Close;
  Texto := EdPesq.Text;
  K := Length(Texto);
  N := TrackBar.Position;
  if N < 3 then
    N := 3;
  if K >= 3 then
  begin
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (' + FldNome + ' LIKE "%' + Copy(Texto, I, N) + '%")' + sLineBreak;
    UnDmkDAC_PF.AbreMySQLQuery0(Qr, Dmod.MyDB, [
    'SELECT ' + FldID + ', ' + FldNome,
    'FROM ' + Tb.TableName,
    'WHERE (' + FldNome + ' LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
  end;
end;

procedure TUnCfgCadLista.TbBeforePost(Tb: TmySQLTable; NovoCodigo: TNovoCodigo;
  FldID: String; Reservados: Variant);
begin
  if Tb.State = dsInsert then
  begin
    case NovoCodigo of
      ncControle: Tb.FieldByName(FldID).AsInteger := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, Tb.TableName, FldID, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: Tb.FieldByName(FldID).AsInteger := UMyMod.BuscaProximoGerlSeq1Int32(
        Tb.TableName, FldID, '', '', tsDef, stIns, 0, Reservados);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
        Halt(0);
        Exit;
      end;
    end;
  end;
end;

function TUnCfgCadLista.LocalizaPesquisadoInt1(Tb: TmySQLTable; Qr: TmySQLQuery;
  FldID: String; Avisa: Boolean): Boolean;
var
  Codigo: Integer;
begin
  Result := False;
  if (Qr.State <> dsInactive) and (Qr.RecordCount > 0) then
  begin
    Codigo := Qr.FieldByName('Codigo').AsInteger;
    Result := Tb.Locate(FldID, Codigo, []);
    if not Result then
      Geral.MB_Aviso('N�o foi poss�vel localizar o registro n� ' +
      Geral.FF0(Codigo));
  end else
    Geral.MB_Aviso('N�o h� resultados na pesquisa para localizar o registro!');
end;

procedure TUnCfgCadLista.MostraCadItemGrupo(DB: TmySQLDataBase; Tabela: String;
              Lista: TAppGrupLst; NovoCodigo: TNovoCodigo; Codigo: Integer = 0);
var
  //J, 
  I, K, Siz: Integer;
  Tab, Tit: String;
  Form: TForm;
begin
{
  for I := Low(Lista) to High(Lista) do
  begin
    for J := Low(Lista[I]) to High(Lista[I]) do
    begin
      ShowMessage(Lista[I][J]);
    end;
  end;
}
  K := -1;
  for I := Low(Lista) to High(Lista) do
  begin
    if LowerCase(Lista[I][0]) = LowerCase(Tabela) then
    begin
      K := I;
      Break;
    end;
  end;
  if K > -1 then
  begin
    VAR_CADASTRO := 0;
    if DBCheck.CriaFm(TFmCadItemGrupo, FmCadItemGrupo, afmoNegarComAviso) then
    begin
      Tab := LowerCase(Lista[K][0]);
      Siz := Geral.IMV(Lista[K][1]);
      Tit := Lista[K][2];

      if Siz > 0 then
      begin
        FmCadItemGrupo.QrCadNome.DisplayWidth := Siz;
        FmCadItemGrupo.QrCadNome.Size         := Siz;
      end;

      FmCadItemGrupo.Caption        := 'CAD-LISTA-002 :: ' + Tit;
      FmCadItemGrupo.FDataBase      := DB;
      FmCadItemGrupo.FListaNiveis   := Lista;
      FmCadItemGrupo.QrCad.DataBase := DB;
      FmCadItemGrupo.FNomTabCad     := Tab;
      FmCadItemGrupo.FNovoCodigo    := NovoCodigo;
      //
      if K > 0 then
      begin
        K   := K -1;
        Tab := LowerCase(Lista[K][0]);
        Siz := Geral.IMV(Lista[K][1]);
        Tit := Lista[K][2];

        if Siz > 0 then
        begin
          FmCadItemGrupo.QrGruNome.DisplayWidth := Siz;
          FmCadItemGrupo.QrGruNome.Size := Siz;
        end;

        FmCadItemGrupo.FNomTabGru := Tab;
        FmCadItemGrupo.ReopenGru(0);
      end else
      begin
        FmCadItemGrupo.FNomTabGru := '';
        FmCadItemGrupo.LaNivSup.Enabled := False;
        FmCadItemGrupo.EdNivSup.Enabled := False;
        FmCadItemGrupo.CBNivSup.Enabled := False;
      end;
      //
      Form := FmCadItemGrupo.CriaOForm();
      //FmCadItemGrupo.ShowModal;
      if Codigo <> 0 then
        FmCadItemGrupo.LocCod(Codigo, Codigo);
      //
      FmCadItemGrupo.EdOrdem.ReadOnly := False;

      Form.ShowModal;
      //FmCadItemGrupo.Destroy;
      Form.Destroy;
      //
    end;
  end else Geral.MB_Erro('A tabela "' + Tabela +
  '" n�o existe na lista de n�veis selecionada!' + sLineBreak +
  'CfgCadLista.MostraCadItemGrupo()');
end;

end.
