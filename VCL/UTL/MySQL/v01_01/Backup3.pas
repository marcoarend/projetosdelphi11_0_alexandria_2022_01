unit Backup3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, CheckLst,
  Menus, ComCtrls, Variants, dmkGeral, dmkEdit, mySQLDump, dmkImage,
  UnDmkProcFunc, UnDMkEnums, UnProjGroup_Consts;

type
  TFmBackup3 = class(TForm)
    QrDatabases1: TmySQLQuery;
    QrDatabases1Database: TWideStringField;
    DsDatabases1: TDataSource;
    QrTabs: TmySQLQuery;
    DsTabs: TDataSource;
    DbAll: TmySQLDatabase;
    PMTabelas: TPopupMenu;
    Todas1: TMenuItem;
    Nenhuma1: TMenuItem;
    Prdefinidas1: TMenuItem;
    PadresCNAB1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    MeBase: TMemo;
    MeCmd: TMemo;
    MeResult_: TMemo;
    CBTables: TCheckListBox;
    Panel5: TPanel;
    BtTudo: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    CBDB1: TDBLookupComboBox;
    Panel2: TPanel;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    Label7: TLabel;
    CBDB2: TDBLookupComboBox;
    DsDatabases2: TDataSource;
    QrDatabases2: TmySQLQuery;
    StringField1: TWideStringField;
    QrSel: TmySQLQuery;
    Basesdefolhadepagamento1: TMenuItem;
    Panel6: TPanel;
    RGExporta: TRadioGroup;
    Memo1: TMemo;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    TabSheet4: TTabSheet;
    Panel8: TPanel;
    Label5: TLabel;
    EdPathMySQL: TdmkEdit;
    SpeedButton3: TSpeedButton;
    LaAllow: TLabel;
    EdAllow: TdmkEdit;
    EdBuffer: TdmkEdit;
    LaBuffer: TLabel;
    LaCharacter: TLabel;
    EdCharacter: TdmkEdit;
    Label4: TLabel;
    EdSenha: TEdit;
    CkCriaDB: TCheckBox;
    GroupBox1: TGroupBox;
    LaCharSet: TLabel;
    EdCharSet: TdmkEdit;
    Panel9: TPanel;
    Label6: TLabel;
    EdArquivo: TdmkEdit;
    CkWinZip: TCheckBox;
    CkExclui: TCheckBox;
    Label8: TLabel;
    CkDelNaoCompact: TCheckBox;
    CkExportaXML: TCheckBox;
    EdZipPath: TEdit;
{$WARNINGS OFF}
    SaveDialog1: TFileSaveDialog;
{$WARNINGS ON}
    Memo3: TMemo;
    MeResult: TRichEdit;
    Panel11: TPanel;
    ExportaodeSPEDEFD1: TMenuItem;
    N1: TMenuItem;
    DTBNFe1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel13: TPanel;
    BtOK: TBitBtn;
    Button1: TButton;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    N2: TMenuItem;
    NFeLayout1: TMenuItem;
    NFeWebServices1: TMenuItem;
    GBDestino1: TGroupBox;
    SpeedButton1: TSpeedButton;
    LaAvisoDest1: TLabel;
    EdDestino1: TdmkEdit;
    PBAvisoDest1: TProgressBar;
    GroupBox2: TGroupBox;
    LaAvisoDest2: TLabel;
    PBAvisoDest2: TProgressBar;
    EdDestino2: TdmkEdit;
    SpeedButton2: TSpeedButton;
    N3: TMenuItem;
    DControl1: TMenuItem;
    mySQLDump1: TmySQLDump;
    Planodecontas1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure CBDB1Click(Sender: TObject);
    procedure CBDB1Exit(Sender: TObject);
    procedure QrDatabases1AfterScroll(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure Todas1Click(Sender: TObject);
    procedure Nenhuma1Click(Sender: TObject);
    procedure PadresCNAB1Click(Sender: TObject);
    procedure CBDB2Click(Sender: TObject);
    procedure Basesdefolhadepagamento1Click(Sender: TObject);
    procedure CkWinZipClick(Sender: TObject);
    //procedure MeResult_KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure mySQLDump1Process(Sender: TObject; Table: string;
      Percent: Integer);
    procedure mySQLDump1DataProcess(Sender: TObject; Percent: Integer);
    procedure mySQLDump1Finish(Sender: TObject);
    procedure ExportaodeSPEDEFD1Click(Sender: TObject);
    procedure DTBNFe1Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure MeResultKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDestino2Change(Sender: TObject);
    procedure EdDestino1Change(Sender: TObject);
    procedure NFeLayout1Click(Sender: TObject);
    procedure NFeWebServices1Click(Sender: TObject);
    procedure DControl1Click(Sender: TObject);
    procedure Planodecontas1Click(Sender: TObject);
  private
    { Private declarations }
    FInfos, FLastI: Integer;
    FTempo, FLastT: TDateTime;
    FLastX: String;
    FParar: Boolean;
    //WMLA_TbCount, WMLA_DuCount,
    //WMLA_ZPCount: Integer;
    procedure ObtemInformacoesDoDiretorio(ProgressBar: TProgressBar;
              LabelTxt: TLabel; Diretorio: String);
    function  CriaNomeArquivo(Diretorio: String; NomeCurto: Boolean): String;
    function  TabelasADumpear(var Tabelas: String): Integer;
    //procedure ExecutaCmd(cmd: String);
    procedure Backup();
    procedure Exporta;
    procedure ExportaGruposDeTabelas(Grupo: Integer);
    procedure AddTextMemo2(Cor: TColor; Texto: String);
    procedure SelecionaTabelas(MaxTab: Integer; Tabelas: Array of String;
              CriaDB: Boolean = False);
    //procedure CompactaArquivo(Arquivo, Origem, Destino: String);
    procedure InformaTempoBackup();
    procedure DescreveTexto((*Fonte: Integer; Texto: String*));
    procedure ConfiguraDataBase(DataBase: String);
    function  LimpaTextoZIP(Texto: AnsiString): AnsiString;
    (*
    function  ObtemMySQLBinDir(): String;
    *)
  public
    { Public declarations }
    FFinalizado: Boolean;
  end;

  var
  FmBackup3: TFmBackup3;
  const
  // IN�CIO BANCOS
  FldBancosMax = 18; // 19 -1
  FldBancosStr: array[0..FldBancosMax] of String = ('Codigo', 'Nome', 'Site',
    'DVCC', 'DVB', 'ID_400i', 'ID_400t', 'XLSLinha', 'XLSData', 'XLSHist',
    'XLSDocu', 'XLSHiDo', 'XLSCred', 'XLSDebi', 'XLSCrDb', 'XLSDouC',
    'XLSComp', 'XLSCPMF', 'XLSSldo');
  //
  FldBanCNAB_RMax = 09; // 10 -1
  FldBanCNAB_RStr: array[0..FldBanCNAB_RMax] of String = ('Codigo', 'Registro',
    'Seq1ini', 'Seq1Tam', 'Seq1Val', 'Seq2ini', 'Seq2Tam', 'Seq2Val',
    'Envio', 'TamLinha');
  //
  FldCNAB_FldMax = 1; // 2 -1
  FldCNAB_FldStr: array[0..FldCNAB_FldMax] of String = ('Codigo', 'Nome');
  //
  FldCNAB_CaGMax = 2; // 3 -1
  FldCNAB_CaGStr: array[0..FldCNAB_CaGMax] of String = ('Codigo', 'ID', 'Nome');
  //
  FldCNAB_CaDMax = 17; // 18 -1
  FldCNAB_CaDStr: array[0..FldCNAB_CaDMax] of String = ('BcoOrig', 'ID', 'Nome',
    'Signific', 'PadrIni', 'PadrTam', 'PadrVal', 'AlfaNum', 'FmtInv', 'Ajuda',
    'Registro', 'Envio', 'Campo', 'T240', 'T400', 'SecoVal', 'Formato', 'Casas');
  // FIM BANCOS

  // INICIO FOLHA DE PAGAMENTO
  FldFPAcumuMax = 5; // 6 -1
  FldFPAcumuStr: array[0..FldFPAcumuMax] of String = ('Codigo', 'Descricao',
    'Ativo', 'Chave', 'Visivel', 'RegimeCxa');
  //
  FldFPEventMax = 10; // 11 -1
  FldFPEventStr: array[0..FldFPEventMax] of String = ('Codigo', 'Descricao',
    'Ativo', 'Natureza', 'Base', 'Auto', 'Percentual', 'Prioridade', 'Visivel',
    'Informacao', 'Salvar');
  //
  FldFPEventAcuMax = 03; // 4 -1
  FldFPEventAcuStr: array[0..FldFPEventAcuMax] of String = ('Event', 'Acumu',
    'Ativo', 'Cod_ID');
  //
  FldFPEventTipMax = 03; // 4 -1
  FldFPEventTipStr: array[0..FldFPEventTipMax] of String = ('Event', 'Tipo',
    'Ativo', 'Cod_ID');
  //
  FldFPEventVarMax = 03; // 4 -1
  FldFPEventVarStr: array[0..FldFPEventVarMax] of String = ('Event', 'Varia',
    'Ativo', 'Cod_ID');
  // FIM FOLHA DE PAGAMENTO

implementation

uses UnMyObjects, Module, UnInternalConsts, ZForge, DmkDAC_PF, UMySQLDB, MyListas;

{$R *.DFM}

procedure TFmBackup3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBackup3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBackup3.InformaTempoBackup();
var
  Tempo: TTime;
  Segundos: Integer;
begin
  Tempo := Now() - FTempo;
  Segundos := Trunc(Tempo * 86400);
  //
  if Segundos > PB1.Position then
  begin
    if Segundos <= PB1.Max then
      PB1.Position := Segundos;
  end;
  //  
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Tempo: ' + FormatDateTime('hh:nn:ss', Tempo) + FLastX);
end;

function TFmBackup3.LimpaTextoZIP(Texto: AnsiString): AnsiString;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Texto) do
  begin
    if Ord(Texto[I]) > 31 then
    begin
      if (Trim(String(Result)) = '') and (Texto[I] = '.') then (*nada*) else
      Result := Result + Texto[I];
    end;
  end;
  Result := AnsiString(Trim(String(Result)));
  if Result <> '' then
    AddTextMemo2(clBlack, String(Result));
end;

procedure TFmBackup3.MeResultKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ch: string[1];
begin
  CharToOem(@Key, @ch[1]);
  Key := Ord(ch[1]);
end;

{
procedure TFmBackup3.MeResult_KeyPress(Sender: TObject; var Key: AnsiChar);
var
  ch: string[1];
begin
  CharToOem(@Key, @ch[1]);
  Key := ch[1];
end;
}

procedure TFmBackup3.mySQLDump1DataProcess(Sender: TObject; Percent: Integer);
begin
  //dmkEdit2.ValueVariant := Percent;
end;

procedure TFmBackup3.mySQLDump1Finish(Sender: TObject);
begin
  ShowMessage('Backup terminou!');
end;

procedure TFmBackup3.mySQLDump1Process(Sender: TObject; Table: string;
  Percent: Integer);
begin
{
  Label9.Caption := Table;
  dmkEdit1.ValueVariant := Percent;
}
end;

procedure TFmBackup3.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDestino1);
  //
  if EdDestino1.ValueVariant <> '' then
  begin
    Geral.WriteAppKeyLM2('BackUpDestino1', 'Dermatek', EdDestino1.Text, ktString);
    //
    ObtemInformacoesDoDiretorio(PBAvisoDest1, LaAvisoDest1, EdDestino1.Text);
  end;
end;

procedure TFmBackup3.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDestino2);
  //
  if EdDestino2.ValueVariant <> '' then
  begin
    Geral.WriteAppKeyLM2('BackUpDestino2', 'Dermatek', EdDestino2.Text, ktString);
    //
    ObtemInformacoesDoDiretorio(PBAvisoDest2, LaAvisoDest2, EdDestino2.Text);
  end;
end;

procedure TFmBackup3.FormCreate(Sender: TObject);
var
  //IP, Nome: String;
  BinDir: String;
begin
  ImgTipo.SQLType := stLok;
  //
  if DbAll.Connected then
    Geral.MB_Erro('"DbAll" conectado antes da cria��o!');
  DbAll.Connected := False;
  DbAll.Host := '*erro*';
  FParar := False;
  FFinalizado := False;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  CBTables.Clear;
  {if VAR_IPBACKUP <> '' then IP := VAR_IPBACKUP else IP := 'localhost';
  if VAR_BACKUPDIR  = '' then VAR_BACKUPDIR  := CO_DIR_RAIZ_DMK + '\BackUps';
  if VAR_RESTOREDIR = '' then VAR_RESTOREDIR := CO_DIR_RAIZ_DMK + '\Backups\Restore';
  //
  EdPath.Text := Geral.ReadAppKeyLM('BackUpPath', 'Dermatek', ktString, '');
  with DbAll do
  begin
    Connected    := False;
    Host         := IP;
    DatabaseName := TMeuDB;
    UserName     := VAR_MYSQLUSER;
    UserPassword := VAR_BDSENHA;
    Connected    := True;
  end;}
  //
  MeResult_.Lines.Clear;
  MeResult.Lines.Clear;
  //
{
  EdDestino1.Text := Geral.ReadAppKeyLM('BackUpDestino1', 'Dermatek', ktString, '');
  EdDestino2.Text := Geral.ReadAppKeyLM('BackUpDestino2', 'Dermatek', ktString, '');
  //
}
  //
{
  QrDatabases1. O p e n ;
  if QrDatabases1.Locate('Database', TMeuDB, [loCaseInsensitive]) then
  begin
    CBDB1.KeyValue := QrDatabases1Database.Value;
    EdArquivo.Text := CBDB1.Text +  M L A G e r a l.ObtemDataStr(Now(), False)+'.sql';
  end;
  //
  QrDatabases2. O p e n ;
}
  (*
  BinDir := Geral.ReadAppKeyLM('PathMySQL', 'Dermatek', ktString, '');
  //
  if (BinDir = '') or (not DirectoryExists(BinDir)) then
  begin
    BinDir := ObtemMySQLBinDir;
    //
    Geral.WriteAppKeyLM('PathMySQL', 'Dermatek', BinDir, ktString);
  end;
  *)
  EdPathMySQL.Text := USQLDB.ObtemPathMySQLBin(DbAll);
  //
  MeResult_.Font.Handle := GetStockObject(OEM_FIXED_FONT);
  //
  N3.Visible        := CO_DMKID_APP = 17;
  DControl1.Visible := CO_DMKID_APP = 17;
end;

procedure TFmBackup3.EdDestino1Change(Sender: TObject);
begin
  if EdDestino1.Text <> '' then
  begin
    if EdDestino1.Text[Length(EdDestino1.Text)] <> '\' then
      EdDestino1.Text := EdDestino1.Text + '\';
    //EdDestino1.Text := M L A G e r a l.TiraBarrasDuplicadas(EdDestino1.Text);
  end;
end;

procedure TFmBackup3.EdDestino2Change(Sender: TObject);
begin
  if EdDestino2.Text <> '' then
  begin
    if EdDestino2.Text[Length(EdDestino2.Text)] <> '\' then
      EdDestino2.Text := EdDestino2.Text + '\';
    //EdDestino2.Text := M L A G e r a l.TiraBarrasDuplicadas(EdDestino2.Text);
  end;
end;

{
procedure TFmBackup3.ExecutaCmd(cmd: String);
var
  buf: array[0..4095] of byte;
  si: STARTUPINFO;
  sa: SECURITY_ATTRIBUTES;
  sd: SECURITY_DESCRIPTOR;
  pi: PROCESS_INFORMATION;
  newstdin,newstdout,read_stdout,write_stdin: THandle;
  exitcod,bread,avail: Cardinal;
  //
begin
  if dmkPF.IsWinNT  then
    begin
      InitializeSecurityDescriptor(@sd,SECURITY_DESCRIPTOR_REVISION);
      SetSecurityDescriptorDacl(@sd, true, nil, false);
      sa.lpSecurityDescriptor := @sd;
    end else sa.lpSecurityDescriptor := nil;
  sa.nLength := sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle := true;
  // Tuberias
  if CreatePipe(newstdin,write_stdin,@sa,0) then
  begin
    if CreatePipe(read_stdout,newstdout,@sa,0) then
    begin
      GetStartupInfo(si);
      with si do
      begin
        dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
        wShowWindow := SW_HIDE;
        hStdOutput  := newstdout;
        hStdError   := newstdout;
        hStdInput   := newstdin;
      end;
      fillchar(buf,sizeof(buf),0);
      GetEnvironmentVariable('COMSPEC',@buf,sizeof(buf)-1);
      strcat(@buf, PChar(' /c '+cmd));
      if CreateProcess(nil,@buf,nil,nil,TRUE,CREATE_NEW_CONSOLE,nil,nil,si,pi) then
      begin
        GetExitCodeProcess(pi.hProcess,exitcod);
        PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
        while (exitcod = STILL_ACTIVE) or (bread > 0) do
        begin
          Application.ProcessMessages;
          if (bread > 0) then
          begin
            fillchar(buf,sizeof(buf),0);
            if (avail > sizeof(buf)-1) then
              while (bread >= sizeof(buf)-1)  do
              begin
                ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                // function StrPas(const Str: PAnsiChar): AnsiString;
                LimpaTextoZIP(StrPas(PAnsiChar(@buf)));
                DescreveTexto((*1, StrPas(PAnsiChar(@buf)*));
                fillchar(buf,sizeof(buf),0);
              end else
              begin
                ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                LimpaTextoZIP(StrPas(PAnsiChar(@buf)));
                DescreveTexto((*2, StrPas(@buf)*));
              end;
            end;
            GetExitCodeProcess(pi.hProcess,exitcod);
            PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
            InformaTempoBackup();
          end;
        end;
       CloseHandle(read_stdout);
       CloseHandle(newstdout);
    end;
    CloseHandle(newstdin);
    CloseHandle(write_stdin);
  end;
end;
}

procedure TFmBackup3.SpeedButton3Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdPathMySQL);
end;

function TFmBackup3.CriaNomeArquivo(Diretorio: String; NomeCurto: Boolean): String;
var
  Pasta: String;
begin
  Pasta := Trim(Diretorio);
  if Length(Pasta) > 0 then
  begin
    if Diretorio[Length(Diretorio)] <> '\' then Diretorio := Diretorio + '\';
    if NomeCurto then
      Result := String(dmkPF.NomeLongoParaCurto(AnsiString(Diretorio))) + EdArquivo.Text
    else
      Result := Diretorio + EdArquivo.Text;
  end else Result := '';    
end;

procedure TFmBackup3.DControl1Click(Sender: TObject);
const
  MaxTab = 36;
  Tabelas: array [0..MaxTab]of String = (
    'helpcab',
    'helpopc',
    'helptopic',
    'helpfaq',
    'helprestr',
    'helprestr',
    'helprestip',
    'arreits',
    'aplicativos',
    'aplicmodul',
    'aplicmodul',
    'cliaplic',
    'cliaplicmo',
    'clientes',
    'enticliint',
    'feriados',
    'entidades',
    'enticontat',
    'enticonent',
    'entimail',
    'entitel',
    'carteiras',
    'emailconta',
    'cnab_cfg',
    'mysqlconf',
    'listalograd',
    'modulos',
    'nfsenfscab',
    'bloopcoes',
    'prev',
    'preemail',
    'preemmsg',
    'protocolos',
    'protpakits',
    'srvtarifa',
    'entitipcto',
    'ufs'
    );
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.DescreveTexto((*Fonte: Integer; Texto: String*));
var
  Posi: Integer;
begin
  FInfos := FInfos + 1;
  Posi   := PB2.Position + 1;
  //
  if Posi <= PB2.Max then
    PB2.Position := Posi;
  {
  for I := 1 to Length(Texto) do
  begin
    Memo3.Lines.Add('T' + FormatFloat('0', Fonte) + ' L' + FormatFloat('0',
    FInfos) + ' ' + Texto[I] + ' ' + FormatFloat('000', Ord(Texto[I])));
  end;
  }
end;

procedure TFmBackup3.DTBNFe1Click(Sender: TObject);
const
  MaxTab = 13;
  Tabelas: array [0..MaxTab]of String = (
  // SPED
  'spedefdicmsipiblcs', 'spedefdicmsipiflds',
  // DTB
  'dissemdtb', 'bacen_pais', 'dtb_munici', 'dtb_distri', 'dtb_micro', 'dtb_meso',
  'munsemdtb', 'dtb_paises', 'dtb_ufs', 'dtb_subdis',
  // NFe
  'nfelayc', 'nfelayi');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.SelecionaTabelas(MaxTab: Integer; Tabelas: Array of String;
  CriaDB: Boolean = False);
var
  i, j, k: Integer;
begin
  CKCriaDB.Checked := CriaDB;
  //
  k := 0;
  for i := 0 to CBTables.Items.Count -1 do
  begin
    CBTables.Checked[i] := False;
    for j := 0 to MaxTab do
    begin
      if Uppercase(Tabelas[j]) = Uppercase(CBTables.Items[i]) then
      begin
        CBTables.Checked[i] := True;
        Inc(k, 1);
      end;
    end;
  end;
  if (k < MaxTab) or ((k = 0) and (MaxTab = 0)) then
    Geral.MB_Erro(Geral.FF0(MaxTab - k) + ' das ' + Geral.FF0(MaxTab) +
      ' tabelas n�o foram localizadas na lista ' + 'do banco de dados "' +
      CBDB1.Text + '"');
end;

procedure TFmBackup3.CBDB1Click(Sender: TObject);
begin
  EdArquivo.Text  := USQLDB.Backup_CriaNomeArquivo(CBDB1.Text, Now());
  EdDestino1.Text := Geral.ReadAppKeyLM('Destino1_' + CBDB1.Text, 'Dermatek', ktString, EdDestino1.Text);
  EdDestino2.Text := Geral.ReadAppKeyLM('Destino2_' + CBDB1.Text, 'Dermatek', ktString, EdDestino2.Text);
  //
  ObtemInformacoesDoDiretorio(PBAvisoDest1, LaAvisoDest1, EdDestino1.Text);
  ObtemInformacoesDoDiretorio(PBAvisoDest2, LaAvisoDest2, EdDestino2.Text);
end;

procedure TFmBackup3.CBDB1Exit(Sender: TObject);
begin
  EdArquivo.Text := USQLDB.Backup_CriaNomeArquivo(CBDB1.Text, Now());
end;

procedure TFmBackup3.QrDatabases1AfterScroll(DataSet: TDataSet);
var
  Nome: String;
begin
  CBTables.Clear;
  if CBDB1.KeyValue <> Null then
  begin
    (*DbAll.Disconnect;
    DbAll.DatabaseName := CBDB.Text;*)
    QrTabs.Close;
    QrTabs.SQL.Clear;
    QrTabs.SQL.Add('SHOW TABLES FROM '+qrdatabases1.fields[0].asstring);
    UnDmkDAC_PF.AbreQueryApenas(QrTabs);
    while not QrTabs.Eof do
    begin
      Nome := QrTabs.Fields[0].AsString;
      if Uppercase(Nome) <> 'MASTER' then CBTables.Items.Add(Nome);
      QrTabs.Next;
    end;
    QrTabs.Close;
    Todas1Click(Self);
  end;  
end;

procedure TFmBackup3.BtTudoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTabelas, BtTudo);
end;

procedure TFmBackup3.Button1Click(Sender: TObject);
begin
{
  Screen.Cursor := crHourGlass;
  DumpDB.Connected := False;
  DumpDB.Params := DBAll.Params;
  DumpDB.DatabaseName := CBDB1.Text;
  TabelasADumpear2();
  mySQLDump1.Execute;
  Screen.Cursor := crDefault;
}
end;

function TFmBackup3.TabelasADumpear(var Tabelas: String): Integer;
var
  i, k: Integer;
  Txt: String;
begin
  // n�o fazer: -B '+CBDB.Text + '
  // para n�o copiar a tabela Master
  k := 0;
  Txt := '';
  for i := 0 to CBTables.Items.Count -1 do
  begin
    if CBTables.Checked[i] then
    begin
      k := k + 1;
      Txt := Txt + ' ' + CBTables.Items[i];
    end;
  end;
  //
  if k > 0 then
  begin
    //Txt := ' --databases ' + CBDB1.Text + ' --tables ' + Copy(Txt, 2, Length(Txt));
    if k = CBTables.Items.Count then
      Txt := ' --databases ' + CBDB1.Text //+ ' --tables ' + Copy(Txt, 2, Length(Txt));
    else begin
      Txt := ' --databases ' + CBDB1.Text + ' --tables ' + Copy(Txt, 2, Length(Txt));
      Geral.MB_Aviso(
        'A sele��o de v�rias tabelas pode provocar erro no backup!' + sLineBreak +
        'Tamanho da string = ' + IntToStr(Length(Txt)));
    end;
    Tabelas := Txt;
  end else
  begin
    Tabelas := '';
    Geral.MB_Erro('Nenhuma tabela foi selecionada!');
  end;
  Result := k;
end;

procedure TFmBackup3.Todas1Click(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CBTables.Items.Count -1 do
      CBTables.Checked[i] := True;
end;

procedure TFmBackup3.ObtemInformacoesDoDiretorio(ProgressBar: TProgressBar;
  LabelTxt: TLabel; Diretorio: String);
var
  Total, Livre: Int64;
  Texto, NomL, NomT: String;
  TamL, TamT: Double;
  NivL, NivT, Max, Posi: Integer;
begin
  if DirectoryExists(Diretorio) then
  begin
    GetDiskFreeSpaceEx(PChar(Diretorio), Livre, Total, nil);
    //
    if Total > 0 then
    begin
      Geral.AdequaMedida(grandezaBytes, Total, 0, TamT, NivT, NomT);
      Geral.AdequaMedida(grandezaBytes, Livre, 0, TamL, NivL, NomL);
      //
      Texto := 'Total: ' + Geral.FFT(TamT, NivT, siPositivo) + ' ' + NomT + ' / ';
      Texto := Texto + 'Dispon�vel: ' + Geral.FFT(TamL, NivL, siPositivo) + ' ' + NomL;
      //
      Max  := Trunc(Total / 1000);
      Posi := Trunc(Livre / 1000);
      Posi := Max - Posi;
      //
      LabelTxt.Caption := Texto;
      //
      if (Max > 0) and (Posi > 0) then
      begin
        ProgressBar.Max      := Max;
        ProgressBar.Position := Posi;
        ProgressBar.Visible  := True;
      end else
        ProgressBar.Visible  := False;
    end else
    begin
      LabelTxt.Caption    := '';
      ProgressBar.Visible := False;
    end;
  end else
  begin
    LabelTxt.Caption    := 'Diret�rio n�o localizado!';
    ProgressBar.Visible := False;
  end;
end;

procedure TFmBackup3.Nenhuma1Click(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CBTables.Items.Count -1 do
      CBTables.Checked[i] := False;
end;

procedure TFmBackup3.NFeLayout1Click(Sender: TObject);
const
  MaxTab = 1;
  Tabelas: array [0..MaxTab]of String = ('nfelayc', 'nfelayi');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.NFeWebServices1Click(Sender: TObject);
const
  MaxTab = 0;
  Tabelas: array [0..MaxTab]of String = ('nfe_ws');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.PadresCNAB1Click(Sender: TObject);
const
  MaxTab = 6;
  Tabelas: array [0..MaxTab]of String = (
    'BanCNAB_R', 'Bancos', 'CNAB_CaD', 'CNAB_CaG', 'CNAB_Fld',
    'CNAB_Esp', 'CNAB_Ins');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.Planodecontas1Click(Sender: TObject);
const
  MaxTab = 4;
  Tabelas: array [0..MaxTab]of String = ('plano', 'conjuntos', 'grupos', 'subgrupos', 'contas');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.BtOKClick(Sender: TObject);
var
  Tempo: TTime;
begin
  try
    FInfos := 0;
    FLastT := Geral.ReadAppKeyLM('TempoBachup_' + CBDB1.Text, 'Dermatek', ktTime,  0);
    FLastI := Geral.ReadAppKeyLM('CountZip_' + CBDB1.Text, 'Dermatek', ktInteger, 0);
    //
    if FLastT > 0 then
      FLastX := ' (o �ltimo backup demorou ' + FormatDateTime('hh:nn:ss', FLastT) + ')'
    else
      FLastX := '';
    //
    if FLastT = 0 then
      FLastT := 1;
    //
    if FLastI = 0 then
      FLastI := 1;
    //
    PB1.Position := 0;
    PB1.Max      := Trunc(FLastT * 86400) + 3;
    PB2.Position := 0;
    PB2.Max      := FLastI + 3;
    FTempo       := Now();
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '');
    //
    LaAviso1.Visible := True;
    LaAviso2.Visible := True;
    //dmkEdit1.ValueVariant := 0;
    //
    MeResult.Lines.Clear;
    Memo3.Lines.Clear;
    //
    case PageControl1.ActivePageIndex of
      0: Backup();
      1: Exporta();
      else Geral.MB_Aviso('A��o n�o implementada!');
    end;
    //
    if FFinalizado then
    begin
      Tempo := Now() - FTempo;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Tempo total: ' +
        FormatDateTime('hh:nn:ss', Tempo) + ' - Backup finalizado!');
      //
      Geral.WriteAppKeyLM2('TempoBachup_' + CBDB1.Text, 'Dermatek', Tempo, ktTime);
      Geral.WriteAppKeyLM2('CountZip_' + CBDB1.Text, 'Dermatek', FInfos, ktInteger);
      Geral.WriteAppKeyLM2('Destino1_' + CBDB1.Text, 'Dermatek', EdDestino1.Text, ktString);
      Geral.WriteAppKeyLM2('Destino2_' + CBDB1.Text, 'Dermatek', EdDestino2.Text, ktString);
      //
      Geral.WriteAppKeyLM('PathMySQL', 'Dermatek', EdPathMySQL.Text, ktString);
      //
      PB1.Position := 0;
      PB2.Position := 0;
      //
      Geral.MB_Aviso('Backup finalizado!');
    end;
  except
    on E: Exception do
      Geral.MB_Erro('Falha ao realizar backup!' + sLineBreak + E.Message);
  end;
end;

procedure TFmBackup3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBackup3.AddTextMemo2(Cor: TColor; Texto: String);
begin
{
      RichEdit.SelAttributes.Color := clBlue;
      RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(lowercase(TabDest)) + Texto + sLineBreak + RichEdit.Text;
      RichEdit.Update;
      Application.ProcessMessages;
}
  MeResult.SelAttributes.Color := Cor;
  MeResult.Lines.Add(Geral.NowTxt() + Texto);
  MeResult.SelStart := Length(MeResult.Text);
  //Memo2.Text := Geral.NowTxt() + Texto + sLineBreak + Memo2.Text;
  MeResult.Update;
  Application.ProcessMessages;
end;

procedure TFmBackup3.Backup();
const
  noCreateDb = '--no-create-db';
var
  Senha, (*Texto2,*) CriaDB, Tabelas, Host, User, Arq1, Arq2: String;
  Port, q, k: Integer;
  //
  Tam: Double;
  Ori, Niv: Integer;
  //Pasta, 
  Nom, ArqZ, XML, PathMySQL: String;
  Compactado: Boolean;
  Texto1: WideString;
begin
  Screen.Cursor := crHourGlass;
  Update;
  Application.ProcessMessages;
  try
    Compactado := False;
    Host  := Dmod.MyDB.Host;
    Senha := Dmod.MyDB.UserPassword;
    User  := Dmod.MyDB.UserName;
    Port  := DMod.MyDB.Port;
    if CkCriaDB.Checked then CriaDB := '' else CriaDB := noCreateDB;
    //if Trim(EdSenha.Text) <> '' then Senha := EdSenha.Text;
    q := 0;
    if Trim(CBDB1.Text) = '' then
    begin
      Geral.MB_Erro('Defina o banco de dados!');
      Exit;
    end;
    if (Trim(EdPathMySQL.Text) = '') or (not DirectoryExists(EdPathMySQL.Text)) then
    begin
      Geral.MB_Erro('Defina o caminho do MySQL!');
      Exit;
    end;
    if Trim(EdDestino1.Text) <> '' then
    begin
      if not DirectoryExists(EdDestino1.Text) then
        ForceDirectories(EdDestino1.Text);
      if DirectoryExists(EdDestino1.Text) then
      begin
        q := q + 1;
        k := TabelasADumpear(Tabelas);
        if CkExportaXML.Checked then
          XML := ' --xml '
        else
          XML := '';
        if k > 0 then
        begin
          PathMySQL := dmkPF.ValidaDiretorio(EdPathMySQL.Text, True);
          //
          Texto1 := String(dmkPF.NomeLongoParaCurto(AnsiString(PathMySQL))) + 'mysqldump ' +
          LaAllow.Caption + Geral.SoNumero_TT(EdAllow.Text) + ' ' +
          LaBuffer.Caption + Geral.SoNumero_TT(EdBuffer.Text) + ' ' +
          LaCharacter.Caption + EdCharacter.Text + ' ' +
          //LaCharSet.Caption + EdCharSet.Text +
          //' --debug ' + > n�o funciona
          CriaDB + XML +
          //' -h localhost -u root -p' + CO_USERSPNOW + '  -B '+CBDB.Text + ' > '+
          // ini 2021-02-06
          //' -P' + Geral.FF0(Port) + ' -h ' + Host + ' -u ' + User + ' -p' + Senha + ' '+ Tabelas + ' > '+
          ' -P' + Geral.FF0(Port) + ' -h ' + Host + ' -u ' + User + ' -p"' + Senha + '" '+ Tabelas + ' > '+
          // fim 2021-02-06
          CriaNomeArquivo(EdDestino1.Text, True);
        end;
        MeCmd.Lines.Add(Texto1);
      end  else
        Geral.MB_Erro('O diret�rio "' + EdDestino1.Text + '" n�o foi localizado!');
    end else
    begin
      Geral.MB_Aviso('Defina o campo: "Destino 1 (Onde ser� feito o backup)"!');
      Exit;
    end;
    //;
    (*if Trim(EdDestino2.Text) <> '' then
    begin
      if not DirectoryExists(EdDestino2.Text) then
        ForceDirectories(EdDestino2.Text);
      if DirectoryExists(EdDestino2.Text) then
      begin
        q := q + 1;
        Texto1 := dmkPF.NomeLongoParaCurto(EdPathMySQL.Text)+'mysqldump '+
        LaAllow.Caption+Geral.SoNumero_TT(EdAllow.Text)+' '+
        LaBuffer.Caption+Geral.SoNumero_TT(EdBuffer.Text)+
        ' -h localhost -u root -p' + CO_USERSPNOW + '  -B '+CBDB.Text + ' > '+
        CriaNomeArquivo(EdDestino2.Text, True);
        MeCmd.Lines.Add(Texto2);
      end  else Geral.MB_('O diret�rio "'+EdDestino2.Text+
      '" n�o foi localizado!'));
    end;*)
    //;
    if q <> 0 then
    begin
      if Texto1 <> '' then
      begin
        AddTextMemo2(clBlack, 'Destino1: Backup sendo realizado...');
        //ExecutaCmd(Texto1);
        MyObjects.ExecutaCmd(Texto1, MeResult_);
        Arq1 := CriaNomeArquivo(EdDestino1.Text, False);
        Arq2 := CriaNomeArquivo(EdDestino2.Text, False);
        if FileExists(Arq1) then
        begin
          Ori := Geral.FileSize(Arq1);
          if Ori > 0 then
          begin
            FFinalizado := True;
            Geral.AdequaMedida(grandezaBytes, Ori, 0, Tam, Niv, Nom);
            AddTextMemo2(clBlue, 'Destino1: Tamanho do arquivo: ' +
              FormatFloat('0.00', Tam) + ' ' + Nom);
            AddTextMemo2(clBlue, 'Destino1: Backup realizado com sucesso!');


            if CkWinZip.Checked then
            begin
              AddTextMemo2(clBlack, 'Destino1: Compactando backup!');
              //
              Application.CreateForm(TFmZForge, FmZForge);
              FmZForge.Show;
              ArqZ := FmZForge.ZipaArquivo(zftArquivo, ExtractFileDir(Arq1), Arq1,
                StringReplace(ExtractFileName(Arq1), ExtractFileExt(Arq1),
                '', [rfReplaceAll, rfIgnoreCase]), '', False, False);
              FmZForge.Destroy;
              //
              if Length(ArqZ) > 0 then
              begin
                AddTextMemo2(clBlue, 'Destino1: backup compactado!');
                Compactado := True;
                if (CkDelNaoCompact.Checked) and (Trim(EdDestino2.Text) = '') then
                  DeleteFile(Arq1);
              end else
              begin
                Geral.MB_Erro('Falha ao zipar arquivo!');
                Exit;
              end;
            end;

            //  Z I P A R
            //
            {
            if CkWinZip.Checked then
            begin
              AddTextMemo2(clBlack, 'Destino1: Compactando backup!');
              //
              ArqZ := dmkPF.MudaExtensaoDeArquivo(EdArquivo.Text, 'zip');
              Pasta := EdDestino1.Text;
              CompactaArquivo(ArqZ, Arq1, Pasta);
              AddTextMemo2(clBlue, 'Destino1: backup compactado!');
              Compactado := True;
              if (CkDelNaoCompact.Checked) and (Trim(EdDestino2.Text) = '') then
                DeleteFile(Arq1);
            end;
            }
            {
            AddTextMemo2(clBlue, IntToStr(WMLA_TbCount) + ' tabela(s) inserida(s).');
            AddTextMemo2(clBlue, IntToStr(WMLA_DuCount) + ' dump criado(s).');
            }
            //
            //  Fim Z I P A R
            //
            //
            if Trim(EdDestino2.Text) <> '' then
            begin
              AddTextMemo2(clBlack, 'Destino2: verificando diret�rio...');
              if DirectoryExists(Copy(EdDestino2.Text, 1, 2)) then
              begin
                ForceDirectories(EdDestino2.Text);
                AddTextMemo2(clBlack, 'Destino2: Copiando arquivo do destino 1...');
                if Compactado then
                begin
                  {
                  Pasta := EdDestino2.Text;
                  CompactaArquivo(ArqZ, Arq1, Pasta);
                  }
                  Arq2 := StringReplace(Arq2, ExtractFileExt(Arq2), '.zip',
                            [rfReplaceAll, rfIgnoreCase]);
                  if CopyFile(pChar(ArqZ), pChar(Arq2), True) then
                    if (CkDelNaoCompact.Checked) then
                      DeleteFile(Arq1);
                end else
                  dmkPF.CopiaArq(Arq1, Arq2);
                //
                if FileExists(Arq2) then
                begin
                  Ori := Geral.FileSize(Arq2);
                  if Ori > 0 then
                  begin
                    AddTextMemo2(clBlue, 'Destino2: Backup realizado com sucesso!');
                    {
                    //Tamanho do arquivo desatualizado! Aparece com o tamanho original mesmo zipado.
                    AddTextMemo2(clBlue, 'Destino2: Tamanho do arquivo: ' +
                      FormatFloat('0.00', Tam) + ' ' + Nom);
                    }
                  end else AddTextMemo2(clred, 'Destino2: ERRO. Arquivo vazio!');
                end;
              end else
                AddTextMemo2(clred, 'O drive "' + Copy(EdDestino2.Text, 1, 2) + '" n�o existe ou est� inacess�vel!');

            end;
          end else AddTextMemo2(clred, 'Destino1: ERRO. Arquivo vazio!');
        end else
          AddTextMemo2(clRed, 'Destino1: ERRO ao fazer backup!');
      end;
      {
      if Texto2 <> '' then
      begin
        ExecutaCmd(Texto2);
        Geral.MB_('Destino2: Backup realizado com sucesso!',
        'Backup Finalizado', MB_OK+MB_ICONINFORMATION);
      end;
      }
    end else
      Geral.MB_Erro('Nenhum backup foi realizado!');
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBackup3.Exporta;
begin
  if not DBAll.Connected then
  begin
    Geral.MB_Aviso('Selecione a base de dados!');
    Exit;
  end;
  case RGExporta.ItemIndex of
    -1: Geral.MB_Aviso('Defina o tipo de exporta��o!');
    0..1: ExportaGruposDeTabelas(RGExporta.ItemIndex);
    else Geral.MB_Aviso('Exporta��o n�o implementada!');
  end;
end;

procedure TFmBackup3.CBDB2Click(Sender: TObject);
begin
  (*
  DbAll.Disconnect;
  DbAll.DatabaseName := CBDB2.Text;
  DbAll.Connect;
  *)
  //
  ConfiguraDataBase(CBDB2.Text);
end;

procedure TFmBackup3.CkWinZipClick(Sender: TObject);
begin
  CkDelNaoCompact.Enabled := CkWinZip.Checked;
end;

procedure TFmBackup3.ConfiguraDataBase(DataBase: String);
begin
  UnDmkDAC_PF.ConectaMyDB_DAC(DbAll, DataBase, VAR_IP, VAR_PORTA, 'root',
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
end;

(*
function TFmBackup3.ObtemMySQLBinDir: String;
var
  Query: TmySQLQuery;
  BaseDir, Diretorio: String;
begin
  try
    if not DbAll.Connected then
      ConfiguraDataBase('mysql');
    //
    Query := TmySQLQuery.Create(Dmod.MyDB);
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DbAll, [
      'SHOW VARIABLES LIKE "basedir"',
      '']);
    //
    BaseDir := Query.FieldByName('Value').AsString;
  finally
    Query.Free;
  end;
  //
  if Pos('/', BaseDir) > 0 then
    Diretorio := dmkPF.InverteBarras(BaseDir) + 'bin\'
  else
    Diretorio := BaseDir + 'bin\';
  //
  Result := Diretorio;
end;
*)

{
Substitu�da pelo ZForge para n�o precisar instalar o WinZip
procedure TFmBackup3.CompactaArquivo(Arquivo, Origem, Destino: String);
var
  ExecDOS : PChar;
  i, Verificacoes: Integer;
  ZipPath, Options: String;
begin
  if CkWinZip.Checked then
  begin
    try
      // Zipa arquivos criados no dir A: e C:
      if Destino[1] = 'A' then Options := Options + '&';
      if CkExclui.Checked then Options := Options + 'w';
      if Options <> '' then Options := ' -' + Options;
      Verificacoes := 500;
      /////////////////////////////////////////////////////////////////////////////
      // -yp deixa DOS Aberto
      // -& (Multiplos discos)
      /////////////////////////////////////////////////////////////////////////////
      //ZipPath := FmWetBlueMLA_BK.EdZipPath.Text;
      ZipPath := EdZipPath.Text;
      if not FileExists(ZipPath+'\wzzip.exe') then
      begin
        Memo1.Lines.Add('O arquivo "'+ZipPath+'\wzzip.exe" n�o foi encontrado!');
        raise EAbort.Create('');
      end else begin
        ZipPath := dmkPF.NomeLongoParaCurto(ZipPath);
        ExecDOS := PChar(PChar(ZipPath+'\wzzip'+Options+' '+
          ExtractShortPathName(Destino)+Arquivo+' '+Origem));
        MeResult.Lines.Add('');
        AddTextMemo2(clGreen, '*** Linha de comando: ***');
        MeResult.Lines.Add(ExecDOS);
        MeResult.Lines.Add('*** Fim linha de comando ***');
        MeResult.Lines.Add('');
        (*
        Geral.MB_(ExecDos, 'Exec DOS', MB_OK+MB_ICONWARNING);
        Exit;
        *)
        try
          //WinExec(ExecDOS, SW_SHOW);
          ExecutaCmd(ExecDOS);
          //dmkPF.WinExecAndWaitOrNot(ExecDOS, SW_SHOW, nil, True);
          // Erro!!!!!!!!!!!
          //M L A G e r a l.WinExecAndWait32(ExecDOS, SW_HIDE, Memo2);
          //M L A G e r a l.LeSaidaDOS(ExecDOS, Memo2);
        except
          AddTextMemo2(clRed, '*** Compacta��o falhou! ***');
        end;
      end;
      for i := 1 to Verificacoes do
      begin
        Application.ProcessMessages;
        if FParar then
        begin
          FParar := False;
          Screen.Cursor := crDefault;
          AddTextMemo2(clFuchsia, 'Compacta��o interrompida pelo usu�rio!');
          Break;
        end;
        Sleep(1000);
        if FileExists(Destino + Arquivo) then
        begin
          WMLA_ZPCount := WMLA_ZPCount + 1;
          Break;
        end;
      end;
  (*    ExecDOS := PChar(CO_DIR_RAIZ_DMK + '\wzzip ' + CO_DIR_RAIZ_DMK + '\Backups\' + ArqNome +
                       ' ' + CO_DIR_RAIZ_DMK '\' + Arquivo);
      WinExec(ExecDOS, SW_HIDE);*)
      /////////////////////////////////////////////////////////////////////////////
    except
      Memo1.Lines.Add('Erro na compacta��o com Win Zip!');
    end;
  end else begin
    //CopyFile(PAnsiChar(Origem), PAnsiChar(Destino+Arquivo+'.sql'), True);
  end;
end;
}

procedure TFmBackup3.ExportaGruposDeTabelas(Grupo: Integer);
  function ReabreTabela(Tabela, Registros, Extra, Ordem: String): Boolean;
  begin
    Result := False;
    //
    QrSel.Close;
    QrSel.SQL.Clear;
    QrSel.SQL.Add('SELECT ' + Registros + ' FROM ' + Lowercase(Tabela));
    QrSel.SQL.Add(Extra);
    if Ordem <> '' then
      QrSel.SQL.Add('ORDER BY ' + Ordem);
    try
      UnDmkDAC_PF.AbreQueryApenas(QrSel);
      Result := True;
    except
      Geral.MB_Erro('N�o foi poss�vel abrir a tabela de "' +
      Tabela + '" da base de dados "' + CBDB2.Text + '"!');
    end;
  end;
  procedure ExportaTabela(Tabela, Ordem, Extra: String; FldMax: Integer;
    FldStr: array of String);
  var
    i: integer;
    Registros: (*array of*) String;
  begin
    Registros := FldStr[0];
    for i := 1 to FldMax do
      Registros := Registros + ', ' + FldStr[i];
    if ReabreTabela(Tabela, Registros, Extra, Ordem) then
    begin
      Memo1.Lines.Add('<' + Tabela + '>');
      QrSel.First;
      while not QrSel.Eof do
      begin
        Memo1.Lines.Add('[' + FormatFloat('000000', QrSel.RecNo) + ']');
        //
        for i := 0 to QrSel.FieldCount -1 do
        begin
          Memo1.Lines.Add(QrSel.Fields[i].FieldName + '=' +
            dmkPF.DataTypeToString(QrSel.Fields[i]));
        end;
        QrSel.Next;
      end;
    end;
  end;
  procedure ExportaGrupo(Grupo: Integer; VersaoArq: String);
  const
    X1 = 'WHERE Codigo <= 500';
  begin
    case Grupo of
      0:
      begin
        Memo1.Lines.Add('Bancos=' + VersaoArq);
        ExportaTabela('Bancos',    'Codigo',           '', FldBancosMax,    FldBancosStr);
        ExportaTabela('BanCNAB_R', 'Codigo, Registro', '', FldBanCNAB_RMax, FldBanCNAB_RStr);
        ExportaTabela('CNAB_Fld',  'Codigo',           '', FldCNAB_FldMax,  FldCNAB_FldStr);
        ExportaTabela('CNAB_CaG',  'Codigo',           '', FldCNAB_CaGMax,  FldCNAB_CaGStr);
        ExportaTabela('CNAB_CaD',  'BcoOrig, ID',      '', FldCNAB_CaDMax,  FldCNAB_CaDStr);
      end;
      1:
      begin
        Memo1.Lines.Add('FolhaDePagamento=' + VersaoArq);
        ExportaTabela('FPAcumu',   'Codigo',           '', FldFPAcumuMax,   FldFPAcumuStr);
        ExportaTabela('FPEvent',   'Codigo',           X1, FldFPEventMax,   FldFPEventStr);
        ExportaTabela('FPEventAcu','Cod_ID',           '', FldFPEventAcuMax,FldFPEventAcuStr);
        ExportaTabela('FPEventTip','Cod_ID',           '', FldFPEventTipMax,FldFPEventTipStr);
        ExportaTabela('FPEventVar','Cod_ID',           '', FldFPEventVarMax,FldFPEventVarStr);
      end;
    end;
  end;
var
  VerTxt: String;
begin
  Memo1.Lines.Clear;
  VerTxt := Copy(Geral.SoNumero_TT(dmkPF.ObtemDataStr(Now, False)), 1, 10);
  if not InputQuery('Vers�o do Arquivo',
  'Informe o n�mero de vers�o do aquivo:', VerTxt) then
    Exit;
  //
  VerTxt := IntToStr(Geral.IMV(VerTxt));
  ExportaGrupo(Grupo, VerTxt);
{$WARNINGS OFF}
  if SaveDialog1.Execute then
    MyObjects.ExportaMemoToFile(Memo1, SaveDialog1.FileName, {Exclui} True,
    {EscreveLinhasVazias} False, {SemAcentos} False);
{$WARNINGS ON}
end;

procedure TFmBackup3.ExportaodeSPEDEFD1Click(Sender: TObject);
const
  MaxTab = 1;
  Tabelas: array [0..MaxTab]of String = ('spedefdicmsipiblcs', 'spedefdicmsipiflds');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

procedure TFmBackup3.Basesdefolhadepagamento1Click(Sender: TObject);
const
  MaxTab = 23;
  Tabelas: array [0..MaxTab]of String = ('FPAcumu', 'FPAdmis', 'FPCateg',
    'FPCBO02', 'FPCBO94', 'FPCivil', 'FPDeslg', 'FPEvent', 'FPEventAcu',
    'FPEventTip', 'FPEventVar', 'FPFeriaFal', 'FPGrauI', 'FPMunic',
    'FPMunicCEP', 'FPNacio', 'FPParam', 'FPRacas', 'FPSitua', 'FPTpCal',
    'FPTpCalPer', 'FPTpSal', 'FPVaria', 'FPVincu');
begin
  SelecionaTabelas(MaxTab, Tabelas);
end;

end.

