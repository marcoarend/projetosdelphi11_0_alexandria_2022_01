unit ServicoManager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkGeral, DB, mySQLDbTables, dmkImage,
  UnDmkEnums, UnGrl_Consts;

type
  TFmServicoManager = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    EdServico: TEdit;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    Label2: TLabel;
    Edit1: TEdit;
    DB: TmySQLDatabase;
    Qr: TmySQLQuery;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtStatus: TBitBtn;
    BtAtivar: TBitBtn;
    BtDesativar: TBitBtn;
    BtConfig: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    RGServico: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtAtivarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtStatusClick(Sender: TObject);
    procedure BtDesativarClick(Sender: TObject);
    procedure BtConfigClick(Sender: TObject);
    procedure RGServicoClick(Sender: TObject);
  private
    { Private declarations }
    function StatusServico: Integer;
    function DescobreServicoMySQL(): String;
  public
    { Public declarations }
    FStatusServico: Integer;
    //
    FCB4_DB: String;
    
  end;

  var
  FmServicoManager: TFmServicoManager;

implementation

{$R *.DFM}

uses UnMyObjects, {$IFNDEF NO_USE_MYSQLMODULE}MyDBCheck, {$ENDIF} ServiceManager;

procedure TFmServicoManager.BtSaidaClick(Sender: TObject);
begin
  FStatusServico := StatusServico;
  Close;
end;

procedure TFmServicoManager.BtConfigClick(Sender: TObject);
begin
  {$IFNDEF NO_USE_MYSQLMODULE}
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  {$ENDIF}
  Screen.Cursor := crHourGlass;
  if RGServico.Itemindex = 1 then
  begin
    DB.Connect;
    Qr.Close;
    Qr.SQL.Clear;
    Qr.SQL.Add('INSERT INTO user SET Password=PASSWORD("' + CO_USERSPNOW + '"),');
    Qr.SQL.Add('Select_priv="Y", Insert_priv="Y",');
    Qr.SQL.Add('Update_priv="Y", Delete_priv="Y", Create_priv="Y",');
    Qr.SQL.Add('Drop_priv="Y", Reload_priv="Y", Shutdown_priv="Y",');
    Qr.SQL.Add('Process_priv="Y", File_priv="Y", Grant_priv="Y",');
    Qr.SQL.Add('References_priv="Y", Index_priv="Y", Alter_priv="Y",');
    Qr.SQL.Add('Show_db_priv="Y", Super_priv="Y", Create_tmp_table_priv="Y",');
    Qr.SQL.Add('Lock_tables_priv="Y", Execute_priv="Y", Repl_slave_priv="Y",');
    Qr.SQL.Add('Repl_client_priv="Y", Host="%", User="root";');
    Qr.ExecSQL;
    //
    Qr.SQL.Clear;
    Qr.SQL.Add('INSERT INTO user SET Password=PASSWORD("' + CO_USERSPNOW + '"),');
    Qr.SQL.Add('Select_priv="Y", Insert_priv="Y",');
    Qr.SQL.Add('Update_priv="Y", Delete_priv="Y", Create_priv="Y",');
    Qr.SQL.Add('Drop_priv="Y", Reload_priv="Y", Shutdown_priv="Y",');
    Qr.SQL.Add('Process_priv="Y", File_priv="Y", Grant_priv="Y",');
    Qr.SQL.Add('References_priv="Y", Index_priv="Y", Alter_priv="Y",');
    Qr.SQL.Add('Show_db_priv="Y", Super_priv="Y", Create_tmp_table_priv="Y",');
    Qr.SQL.Add('Lock_tables_priv="Y", Execute_priv="Y", Repl_slave_priv="Y",');
    Qr.SQL.Add('Repl_client_priv="Y", Host="localhost", User="root";');
    Qr.ExecSQL;
    //
    Qr.SQL.Clear;
    Qr.SQL.Add(DELETE_FROM + ' user');
    Qr.SQL.Add('WHERE Password = ""');
    Qr.ExecSQL;
    //
    Qr.SQL.Clear;
    Qr.SQL.Add('FLUSH PRIVILEGES;');
    Qr.ExecSQL;
    //
  end;
  Screen.Cursor := crDefault;
  Application.Terminate;
end;

procedure TFmServicoManager.BtAtivarClick(Sender: TObject);
var
  Servico: TServiceManager;
  Arquivo, NewName: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if RGServico.ItemIndex = 2 then
    begin
      if Geral.MB_Pergunta(
      'Deseja renomear o arquivo de log antes de ativar o servi�o?') = ID_YES then
      begin
        Arquivo := FCB4_DB[1] + ':\codebase\server\S4SERVER.log';
        if not FileExists(Arquivo) then
          Geral.MB_Aviso(
          'Arquivo n�o localizado para renomear:' + sLineBreak + Arquivo)
        else begin
          NewName := ExtractFilePath(Arquivo) + 'S4SERVER' +
            FormatDateTime('_YYYYMMDD_HHNNSS', Now()) + '.log';
          RenameFile(Arquivo, NewName);
        end;
      end;
    end;
    ProgressBar1.Position := 0;
    ProgressBar1.Max := 70;
    Servico := TServiceManager.Create;
    try
      if Servico.Connect  then
        if Servico.OpenServiceConnection(PChar(EdServico.Text)) then
        begin
          Servico.StartService;
          while Servico.GetStatus = 2 do
          begin
            ProgressBar1.Position := ProgressBar1.Position + 1;
            ProgressBar1.Refresh;
            Sleep(300);
          end;
          ProgressBar1.Position := 70;
          Edit1.Text := Servico.GetStatusText(Servico.GetStatus);
          if Servico.ServiceRunning then
          begin
            if RGServico.ItemIndex = 1 then
              Geral.WriteAppKeyLM2('MySQL_Service', Application.Title, EdServico.Text, ktString);
          end;
        end;
    finally
      Servico.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmServicoManager.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmServicoManager.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmServicoManager.RGServicoClick(Sender: TObject);
begin
  BtStatus.Enabled := False;
  BtAtivar.Enabled := False;
  BtDesativar.Enabled := False;
  BtConfig.Enabled := False;
  EdServico.Text := '';
  case RGServico.ItemIndex of
    1: // MySQL
    begin
      BtStatus.Enabled := True;
      BtAtivar.Enabled := True;
      BtDesativar.Enabled := True;
      BtConfig.Enabled := True;
      EdServico.Text := DescobreServicoMySQL();
    end;
    2: // CodeBase
    begin
      BtStatus.Enabled := True;
      BtAtivar.Enabled := True;
      BtDesativar.Enabled := True;
      BtConfig.Enabled := False;
      EdServico.Text := 's4service.exe';
    end;
  end;
end;

procedure TFmServicoManager.FormCreate(Sender: TObject);
//var
  //Servico: String;
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmServicoManager.BtStatusClick(Sender: TObject);
var
  Servico: TServiceManager;
begin
  FStatusServico := StatusServico;
  Servico := TServiceManager.Create;
  try
    if Servico.Connect  then
      if Servico.OpenServiceConnection(PChar(EdServico.Text)) then
        Edit1.Text := Servico.GetStatusText(Servico.GetStatus);
  finally
    Servico.Free;
  end;
end;

function TFmServicoManager.DescobreServicoMySQL: String;
var
  Servico: String;
begin
  //
  Servico := Geral.ReadAppKeyLM('MySQL_Service', Application.Title, ktString, '');
  //
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL41\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL41';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL4\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL4';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL51\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL51';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL5\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL5';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL';
  EdServico.Text := Servico;
end;

procedure TFmServicoManager.BtDesativarClick(Sender: TObject);
var
  Servico: TServiceManager;
begin
  Screen.Cursor := crHourGlass;
  try
    ProgressBar1.Position := 0;
    ProgressBar1.Max := 70;
    Servico := TServiceManager.Create;
    try
      if Servico.Connect  then
        if Servico.OpenServiceConnection(PChar(EdServico.Text)) then
        begin
          Servico.StopService;
          while Servico.GetStatus = 3 do
          begin
            ProgressBar1.Position := ProgressBar1.Position + 1;
            ProgressBar1.Refresh;
            Sleep(300);
          end;
          ProgressBar1.Position := 70;
          Edit1.Text := Servico.GetStatusText(Servico.GetStatus);
        end;
    finally
      Servico.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmServicoManager.StatusServico: Integer;
var
  Servico: TServiceManager;
begin
  Result := -1;
  Servico := TServiceManager.Create;
  try
    if Servico.Connect  then
      if Servico.OpenServiceConnection(PChar(EdServico.Text)) then
      begin
        Result := Servico.GetStatus;
        Edit1.Text := Servico.GetStatusText(Result);
      end;
  finally
    Servico.Free;
  end;
end;

end.


