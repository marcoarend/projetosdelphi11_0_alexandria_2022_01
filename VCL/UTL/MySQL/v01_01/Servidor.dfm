object FmServidor: TFmServidor
  Left = 281
  Top = 271
  Caption = 'XXX-XXXXX-006 :: Tipo de Conex'#227'o'
  ClientHeight = 299
  ClientWidth = 481
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object LaApp: TLabel
    Left = 140
    Top = 216
    Width = 337
    Height = 40
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object RGTipo: TRadioGroup
    Left = 8
    Top = 8
    Width = 465
    Height = 125
    Caption = ' Tipo de m'#225'quina: '
    Columns = 2
    Items.Strings = (
      'Nenhum'
      'Cliente'
      'Servidor'
      'Ambos')
    TabOrder = 0
  end
  object BtConfirma: TBitBtn
    Tag = 14
    Left = 7
    Top = 140
    Width = 120
    Height = 40
    Cursor = crHandPoint
    Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
    Caption = '&OK'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    NumGlyphs = 2
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = BtConfirmaClick
  end
end
