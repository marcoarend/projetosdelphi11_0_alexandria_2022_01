object FmVerifiDB: TFmVerifiDB
  Left = 382
  Top = 202
  Caption = 'FER-VRFBD-001 :: Verifica'#231#227'o de Banco de Dados Servidor'
  ClientHeight = 671
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 553
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 538
      Width = 1008
      Height = 15
      Align = alBottom
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 60
        Align = alClient
        Caption = ' Verifica'#231#245'es: '
        TabOrder = 0
        object CkEstrutura: TCheckBox
          Left = 172
          Top = 16
          Width = 161
          Height = 17
          Caption = 'Estrutura do banco de dados.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkRegObrigat: TCheckBox
          Left = 525
          Top = 16
          Width = 157
          Height = 17
          Caption = 'Recria registros obrigat'#243'rios.'
          TabOrder = 1
        end
        object CkPergunta: TCheckBox
          Left = 12
          Top = 16
          Width = 155
          Height = 17
          Caption = 'Pergunta antes de executar.'
          TabOrder = 2
        end
        object CkEstrutLoc: TCheckBox
          Left = 336
          Top = 16
          Width = 187
          Height = 17
          Caption = 'Estrutura do banco de dados local.'
          TabOrder = 3
        end
        object CkEntidade0: TCheckBox
          Left = 12
          Top = 36
          Width = 129
          Height = 17
          Caption = 'Exclui entidade zero.'
          TabOrder = 4
        end
        object CkQeiLnk: TCheckBox
          Left = 172
          Top = 36
          Width = 61
          Height = 17
          Caption = 'QeiLnk.'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object CkVAR_VERIFICA_BD_FIELD_DEFAULT_VALUE: TCheckBox
          Left = 336
          Top = 36
          Width = 345
          Height = 17
          Caption = 'Verifica valores padr'#227'o de preenchimento de campos.'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
        object CkJanelas: TCheckBox
          Left = 256
          Top = 36
          Width = 61
          Height = 17
          Caption = 'Janelas.'
          Checked = True
          State = cbChecked
          TabOrder = 7
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 182
      Width = 773
      Height = 356
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Avisos'
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 765
          Height = 328
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 60
      Width = 1008
      Height = 101
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object LaAvisoP1: TLabel
        Left = 13
        Top = 80
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoP2: TLabel
        Left = 12
        Top = 80
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 77
        Align = alTop
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 60
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 828
            Height = 60
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAvisoG1: TLabel
              Left = 13
              Top = 34
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoB1: TLabel
              Left = 13
              Top = 18
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoG2: TLabel
              Left = 12
              Top = 33
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoB2: TLabel
              Left = 12
              Top = 17
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoR1: TLabel
              Left = 13
              Top = 2
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoR2: TLabel
              Left = 12
              Top = 1
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
          object Panel14: TPanel
            Left = 990
            Top = 0
            Width = 14
            Height = 60
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
          end
          object Panel6: TPanel
            Left = 888
            Top = 0
            Width = 102
            Height = 60
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object LaTempoI: TLabel
              Left = 0
              Top = 0
              Width = 102
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitWidth = 68
            end
            object LaTempoF: TLabel
              Left = 0
              Top = 14
              Width = 102
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitWidth = 68
            end
            object LaTempoT: TLabel
              Left = 0
              Top = 28
              Width = 102
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              Transparent = True
              ExplicitWidth = 68
            end
          end
          object Panel8: TPanel
            Left = 828
            Top = 0
            Width = 60
            Height = 60
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object Label4: TLabel
              Left = 0
              Top = 0
              Width = 60
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'In'#237'cio:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 44
            end
            object Label5: TLabel
              Left = 0
              Top = 14
              Width = 60
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'T'#233'rmino:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 44
            end
            object Label6: TLabel
              Left = 0
              Top = 28
              Width = 60
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'Tempo:'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentColor = False
              ParentFont = False
              Transparent = True
              ExplicitWidth = 44
            end
          end
        end
      end
    end
    object EdPqVeri: TdmkEdit
      Left = 0
      Top = 161
      Width = 1008
      Height = 21
      Align = alTop
      TabOrder = 4
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object PageControl2: TPageControl
      Left = 773
      Top = 182
      Width = 235
      Height = 356
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      ActivePage = TabSheet5
      Align = alRight
      TabOrder = 5
      object TabSheet2: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Tabelas desnecess'#225'rias'
        object ClTabsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 227
          Height = 292
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel10: TPanel
          Left = 0
          Top = 292
          Width = 227
          Height = 36
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtTabsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
            OnClick = BtTabsNoNeedClick
          end
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Campos desnecess'#225'rios'
        ImageIndex = 1
        object ClFldsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 227
          Height = 291
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel9: TPanel
          Left = 0
          Top = 291
          Width = 227
          Height = 37
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtClFldsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
            OnClick = BtClFldsNoNeedClick
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = #205'ndices desnecess'#225'rios'
        ImageIndex = 2
        object ClIdxsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 227
          Height = 292
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel16: TPanel
          Left = 0
          Top = 292
          Width = 227
          Height = 36
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtClIdxsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
            OnClick = BtClIdxsNoNeedClick
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'A'#231#245'es extras'
        ImageIndex = 3
        object Button1: TButton
          Left = 4
          Top = 8
          Width = 133
          Height = 25
          Caption = 'ERPNameByCli'
          TabOrder = 0
          OnClick = Button1Click
        end
        object MeAcoesExtras: TMemo
          Left = 0
          Top = 239
          Width = 227
          Height = 89
          Align = alBottom
          TabOrder = 1
        end
        object Button2: TButton
          Left = 4
          Top = 36
          Width = 133
          Height = 25
          Caption = 'Limpa DreCfgTp1'
          TabOrder = 2
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 4
          Top = 64
          Width = 133
          Height = 25
          Caption = 'Limpa DreCfgTp2'
          TabOrder = 3
          OnClick = Button3Click
        end
        object Button4: TButton
          Left = 4
          Top = 92
          Width = 133
          Height = 25
          Caption = 'Limpa VarFatID'
          TabOrder = 4
          OnClick = Button4Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 488
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados Servidor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 488
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados Servidor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 488
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados Servidor'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 601
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 8
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sair'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 168
        Top = 16
        Width = 86
        Height = 13
        Caption = 'A partir da tabela: '
      end
      object BtConfirma: TBitBtn
        Tag = 125
        Left = 10
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Atualiza'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object EdTabIni: TdmkEdit
        Left = 256
        Top = 12
        Width = 205
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 8
    Top = 16
  end
  object PMClFldsNoNeed: TPopupMenu
    OnPopup = PMClFldsNoNeedPopup
    Left = 524
    Top = 416
    object Excluicampos1: TMenuItem
      Caption = '&Exclui campo(s)'
      OnClick = Excluicampos1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Marcartodos1: TMenuItem
      Caption = '&Marcar todos'
      OnClick = Marcartodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
  end
  object PMTabsNoNeed: TPopupMenu
    OnPopup = PMTabsNoNeedPopup
    Left = 544
    Top = 272
    object Excluitabelas1: TMenuItem
      Caption = '&Exclui tabela(s)'
      OnClick = Excluitabelas1Click
    end
    object Listatabelas1: TMenuItem
      Caption = '&Lista tabela(s)'
      OnClick = Listatabelas1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Marcartodos2: TMenuItem
      Caption = '&Marcar todos'
      OnClick = Marcartodos2Click
    end
    object Desmarcartodos2: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = Desmarcartodos2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object CorrigeCodUsu1: TMenuItem
      Caption = '&Corrige Cod. Usu.'
      OnClick = CorrigeCodUsu1Click
    end
  end
  object PMClIdxsNoNeed: TPopupMenu
    OnPopup = PMClIdxsNoNeedPopup
    Left = 540
    Top = 528
    object ExcluirIndices1: TMenuItem
      Caption = '&Exclui '#237'ndice(s)'
      OnClick = ExcluirIndices1Click
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MarcarTodos3: TMenuItem
      Caption = '&Marcar todos'
      OnClick = MarcarTodos3Click
    end
    object DesmarcarTodos3: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = DesmarcarTodos3Click
    end
  end
end
