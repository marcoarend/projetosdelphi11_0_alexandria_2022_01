unit MeuDBUses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, DBGrids, Buttons, StdCtrls, Menus, DB, dmkGeral,
  mySQLDbTables, frxClass, frxDBSet, frxCross, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkEditF7, TypInfo, dmkDBEdit, UnDmkProcFunc, UnDmkEnums,
  dmkEditEdIdx;

type
  //TDBGrid = class(Vcl.DBGrids.TDBGrid);
  TFmMeuDBUses = class(TForm)
    PnImprime: TPanel;
    Grade: TStringGrid;
    DBGrid1: TDBGrid;
    PnConfirma: TPanel;
    Button1: TButton;
    BtImprime: TBitBtn;
    Button2: TButton;
    PMImpressao: TPopupMenu;
    Visualizaimpresso1: TMenuItem;
    DesignerModificarelatrioantesdeimprimir1: TMenuItem;
    ImprimeImprimesemvizualizar1: TMenuItem;
    frxReport1: TfrxReport;
    frxReport2: TfrxReport;
    frxDsPG: TfrxDBDataset;
    DsPG: TDataSource;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PnInativo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TEdit;
    EdCPF: TEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    dmkDBLookupComboBox1: TdmkDBLookupComboBox;
    dmkEditCB1: TdmkEditCB;
    dmkEditF71: TdmkEditF7;
    QrPG: TmySQLQuery;
    QrUpd: TmySQLQuery;
    SbImprime: TSpeedButton;
    PnVisivel: TPanel;
    EdFonteTam: TdmkEditCB;
    Label3: TLabel;
    RGOrientacao: TRadioGroup;
    RGOQueFazer: TRadioGroup;
    SbHide: TSpeedButton;
    Label4: TLabel;
    EdColIni: TdmkEditCB;
    EdColFim: TdmkEditCB;
    procedure Button2Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Visualizaimpresso1Click(Sender: TObject);
    procedure DesignerModificarelatrioantesdeimprimir1Click(Sender: TObject);
    procedure ImprimeImprimesemvizualizar1Click(Sender: TObject);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbHideClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FPrintGrid: String;
    FContaCol: Integer;
    procedure Imprime_frx2(Mostra: Byte);
    function  DefineDBdeDmkEditEdIdx(const dmkIdx: TdmkEditEdIdx; var MySQLDB:
              TmySQLDataBase): Boolean;
  public
    { Public declarations }
    FGrade: TStringGrid;
    FTipoGrade: Integer;
    FTitulo: String;
    FDBGrid, FOriDBGrid: TDBGrid;
    //function  ConectaDBTest(): Boolean;
    procedure ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
    procedure ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
    procedure ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
    procedure PesquisaNome(Tabela, CampoNome, CampoCodigo: String;
              DefinedSQL: String = '');
    procedure PesquisaNomeSQLeJoke(SQLJoke_SQL, SQLJoke_Jok: String);
    procedure VirtualKey_F7(Texto: String);
    procedure VirtualKey_F5();
    procedure ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor: Variant;
              var UpdType: TUpdType);
    procedure PesquisaPeloTexto_Nome(Controle: TControl; dmkEdCB: TdmkEditCB;
              dmkCBox: TdmkDBLookupComboBox; dmkEdIdx: TdmkEditEdIdx;
              LS: TDataSource; Qr: TmySQLQuery; DB: TmySQLDataBase; SQLBase,
              NomeTabela, NomeCampoNome, NomeCampoCodigo, Texto: String);
    procedure PesquisaPrdTamCor(Controle: TControl; dmkEdCB: TdmkEditCB;
              dmkCBox: TdmkDBLookupComboBox; DmkEdIdx: TdmkEditEdIdx;
              LS: TDataSource; Qr: TmySQLQuery; DB: TmySQLDataBase; SQLBase,
              NomeTabela, NomeCampoNome, NomeCampoCodigo: String);
    procedure DefineResultadoF7(Controle: TControl; dmkEdCB: TdmkEditCB;
              dmkCBox: TdmkDBLookupComboBox; dmkEdIdx: TdmkEditEdIdx);
    procedure LocalizaESetaEdDBEdit(x: String);
    procedure Imprime_frx3(Mostra: Byte);
  end;
  //
var
  FmMeuDBUses: TFmMeuDBUses;
  PID_DB_TEST: TmySQLDatabase;
  AID_DB_TEST: TmySQLDatabase;

implementation

uses UnMyObjects, UCreateEDrop, Module, UnInternalConsts, UnMsgInt,
{$IfNDef SemGrade} PesqPrdTamCor, {$EndIf}
Printers, ModuleGeral, DmkDAC_PF, PesqNome;

{$R *.dfm}

{ TFmMeuDBUses }

procedure TFmMeuDBUses.BtImprimeClick(Sender: TObject);
begin
  Imprime_frx2(1);
end;

procedure TFmMeuDBUses.Button1Click(Sender: TObject);
begin
  Hide;
  PnImprime.Visible := False;
  PnInativo.Visible := True;
end;

procedure TFmMeuDBUses.Button2Click(Sender: TObject);
begin
  //FmPrincipal.SalvaArquivo(EdNome, EdCPF, Grade, Date, 'Dermatek.ini', True);
end;

function TFmMeuDBUses.DefineDBdeDmkEditEdIdx(const dmkIdx: TdmkEditEdIdx; var
  MySQLDB: TmySQLDataBase): Boolean;
const
  sProcName = 'DefineDBdeDmkEditEdIdx()';
begin
  Result := False;
  MySQLDB := nil;
  if dmkIdx <> nil then
  begin
    case dmkIdx.LocF7DmkDataBase of
      TDmkDBLookup.ddluNone: ; //MySQLDB := nil;
      TDmkDBLookup.ddluMyDB : MySQLDB := Dmod.MyDB;
      TDmkDBLookup.ddluMyPID_DB : MySQLDB := DModG.MyPID_DB;
      TDmkDBLookup.ddluAll_DB : MySQLDB := DModG.AllID_DB;
      else
      begin
        Geral.MB_Erro('Database n�o implementado em ' + sProcName);
        Exit;
      end;
    end;
    Result :=  MySQLDB <> nil;
  end;
end;

procedure TFmMeuDBUses.DefineResultadoF7(Controle: TControl; dmkEdCB: TdmkEditCB;
  dmkCBox: TdmkDBLookupComboBox; dmkEdIdx: TdmkEditEdIdx);
begin
  if VAR_CADASTRO <> 0 then
  begin
    if dmkCBox <> nil then
    begin
      if dmkCBox.ListField = dmkCBox.KeyField then
        dmkCBox.KeyValue := VAR_CAD_NOME
      else
        dmkCBox.KeyValue := VAR_CADASTRO;
      dmkEdCB := TdmkEditCB(dmkCBox.dmkEditCB);
      if dmkEdCB <> nil then
      begin
        dmkEdCB.ValueVariant := VAR_CADASTRO;
        dmkEdCB.OnFim;
      end;
    end else
    if dmkEdIdx <> nil then
    begin
      dmkEdIdx.ValueVariant := VAR_CADASTRO;
    end;
(*
  end else
  begin
    TDBLookupComboBox(Controle).KeyValue := VAR_CADASTRO;
    LocalizaESetaEdDBEdit(Copy(TDBLookupComboBox(Controle).Name, 3));
  end;
    end
    else
    if FmPesqNome.FF7 <> nil then
    begin
      TdmkEdCBitF7(Controle).Text := VAR_CAD_NOME;
      if TdmkEdCBitF7(Controle).dmkEdCBitCod <> nil then
      begin
        //Show Message(TdmkEdCBitF7(Controle).Name);
        //Show Message(TdmkEdCBit(TdmkEdCBitF7(Controle).dmkEdCBitCod).Name);
        TdmkEdCBit(TdmkEdCBitF7(Controle).dmkEdCBitCod).ValueVariant := VAR_CADASTRO;
      end;
    end;
  end;
*)
  end;
  VAR_CaptionFormOrigem := '';
  VAR_NomeCompoF7 := '';
{
  if VAR_CADASTRO <> 0 then
  begin
    if FmPesqNome.FCB <> nil then
    begin
      if Controle is TdmkDBLookupComboBox then
      begin
        dmkCBox := TdmkDBLookupComboBox(Controle);
        if dmkCBox <> nil then
        begin
          if dmkCBox.ListField = dmkCBox.KeyField then
            dmkCBox.KeyValue := VAR_CAD_NOME
          else
            dmkCBox.KeyValue := VAR_CADASTRO;
          dmkEdCB := TdmkEdCBitCB(dmkCBox.dmkEdCBitCB);
          if dmkEdCB <> nil then
          begin
            dmkEdCB.ValueVariant := VAR_CADASTRO;
            dmkEdCB.OnFim;
          end;
        end;
      end else
      begin
        TDBLookupComboBox(Controle).KeyValue := VAR_CADASTRO;
        LocalizaESetaEdDBEdit(Copy(TDBLookupComboBox(Controle).Name, 3));
      end;
    end
    else
    if FmPesqNome.FF7 <> nil then
    begin
      TdmkEdCBitF7(Controle).Text := VAR_CAD_NOME;
      if TdmkEdCBitF7(Controle).dmkEdCBitCod <> nil then
      begin
        //Show Message(TdmkEdCBitF7(Controle).Name);
        //Show Message(TdmkEdCBit(TdmkEdCBitF7(Controle).dmkEdCBitCod).Name);
        TdmkEdCBit(TdmkEdCBitF7(Controle).dmkEdCBitCod).ValueVariant := VAR_CADASTRO;
      end;
    end;
  end;
  VAR_CaptionFormOrigem := '';
  VAR_NomeCompoF7 := '';
  FmPesqNome.Destroy
}
end;

procedure TFmMeuDBUses.DesignerModificarelatrioantesdeimprimir1Click(
  Sender: TObject);
begin
  Imprime_Frx2(1);
end;

procedure TFmMeuDBUses.FormCreate(Sender: TObject);
begin
  frxDsPG.DataSet := QrPg;
end;

procedure TFmMeuDBUses.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    if FTipoGrade = 0 then
    begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[j - 1, i - 1]]);
    end else begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[i - 1, j - 1]]);
    end;
  end;
end;

procedure TFmMeuDBUses.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'Titulo' then Value := FTitulo;
end;

procedure TFmMeuDBUses.ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
var
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery(DBGrid.DataSource.DataSet);
  frxDsPG.DataSet := Query;
  //
  FTipoGrade := 1; // Grade com origem de DataAware. DBGrid ...
  FTitulo := Titulo;
  FDBGrid := DBGrid;
  FGrade := TStringGrid(DBGrid);
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.ImprimeImprimesemvizualizar1Click(Sender: TObject);
begin
  Imprime_Frx2(2);
end;

procedure TFmMeuDBUses.ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
var
  i, j, c, r: Integer;
  Xt: array of String;
  Xv: array of String;
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  c := StringGrid.ColCount;
  r := StringGrid.RowCount;
  // 2012-02-24
  //ConectaDBTest();
  //QrUpd.Database := DBTest;
  QrUpd.Database := DmodG.MyPID_DB;
  FPrintGrid := UCriaEDrop.RecriaDropedTab(ntrtt_PrintGrid, QrUpd, c);
  SetLength(Xt, StringGrid.ColCount);
  SetLength(Xv, StringGrid.ColCount);
  for i := 0 to c - 1 do
    Xt[i] := 'Col' + FormatFloat('000', i);
  for i := 0 to r - 1 do
  begin
    for j := 0 to c - 1 do
      Xv[j] := StringGrid.Cells[j,i];
    //
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('INSERT INTO ' + FPrintGrid + ' SET ');
    for j := 0 to c - 2 do
      QrUpd.SQL.Add(Xt[j] + '="' + dmkPF.DuplicaAspasDuplas(Xv[j]) + '",');
    QrUpd.SQL.Add(Xt[c-1] + '="' + dmkPF.DuplicaAspasDuplas(Xv[c-1]) + '"');

    QrUpd.ExecSQL;
  end;
  FGrade  := StringGrid;
  FDBGrid := nil;
  FTitulo := Titulo;
  // 2012-02-24
  //QrPg.Database   := DBTest;
  //frxDsPG.DataSet := QrPG;
  QrPg.Close;
  QrPg.Database   := DModG.MyPID_DB;
  frxDsPG.DataSet := QrPG;
  QrPG.SQL.Clear;
  QrPG.SQL.Add('SELECT * ');
  QrPG.SQL.Add('FROM ' + FPrintGrid);
  QrPG.SQL.Add(';');
  QrPG.SQL.Add('DROP TABLE ' + FPrintGrid);
  QrPG.SQL.Add(';');
  UnDmkDAC_PF.AbreQuery(QrPG, DModG.MyPID_DB);
  //  fim 2012-02-24
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  FGrade := StringGrid;
  FTitulo := Titulo;
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.Imprime_frx2(Mostra: Byte);
const
  PagePagHei = 279;

  PageBanWid = 0;
  PagePagTop = 117; // >(=1,01) - (1,91 = 200)
  PagePagFot = 100;

  PageBanLef = 100;
  PageBanTop = 0;
  //PageBanWid = 0;
  PageBanHei = 50;
  PageMemHei = 50;
  PageColWid = 210;

var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  Foot: TfrxPageFooter;
  BANLEF, BANTOP, BANWID, BANHEI,
  //BARLEF, BARTOP, BARWID, BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  I, J,
  L, T, W, H, (*k, *)TopoIni: Integer;
  //Fator: Double;
  //sFonte,
  Campo, MyFmtStr: String;
  Menos: Byte;
  Orientacao: Integer;
  Font_Size: Integer;
  Fator, FtrMemHi: Double;
begin
  FtrMemHi := 1.15;
(*
  sFonte := '6';
  if InputQuery('Fonte', 'Tamanho da fonte', sfonte) then
    Font_Size := Geral.IMV(sFonte);
  if Font_Size < 4 then
    Font_Size := 4;
*)
  Font_Size := EdFonteTam.ValueVariant;
  Fator := Font_Size / 10;
{
  Orientacao := MyObjects.SelRadioGroup('Orienta��o de Impress�o',
  'Selecione a orienta��o', ['Retrato', 'Paisagem'], 1);
  if not (Orientacao in ([0,1])) then
    Exit;
}
  Orientacao := RGOrientacao.ItemIndex;
  Menos := 0;
  //k := -1;
  // Configura p�gina
  while frxReport2.PagesCount > 0 do
    frxReport2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxReport2);
  Page.CreateUniqueName;
  Page.LeftMargin   := 10; //20
  Page.RightMargin  := 10;
  Page.TopMargin    := 10;
  Page.BottomMargin := 10;
  Page.Height       := Trunc(PagePagHei / VAR_DOTIMP);
  case Orientacao of 
    0: Page.Orientation := poPortrait;
    1: Page.Orientation := poLandscape;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(PageBanWid / VAR_DOTIMP);
  H := Trunc(PagePagTop / VAR_DOTIMP * FtrMemHi);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  TITULO DO RELAT�RIO
  FContaCol := 0;
  //TopoIni    := 0;
  MEMTOP := 0;
  MEMHEI := Trunc(24 * Fator * FtrMemHi);//Trunc(PageMemHei / VAR_DOTIMP); 1,59 = 60 0,59=
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  case Orientacao of
    0: MEMWID := Trunc(1800 / VAR_DOTIMP);
    1: MEMWID := Trunc(2700 / VAR_DOTIMP);
  end;
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := Trunc(12 * Fator);
  //
  Memo.Memo.Text := FTitulo;
  Memo.HAlign    := haLeft;

  ///
  ///  DATA/HORA
  FContaCol := 0;
  //TopoIni    := 0;
  MEMTOP := 0;
  MEMHEI := Trunc(24 * Fator * FtrMemHi);//Trunc(PageMemHei / VAR_DOTIMP); 1,59 = 60 0,59=
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  case Orientacao of
    0: MEMWID := Trunc(1800 / VAR_DOTIMP);
    1: MEMWID := Trunc(2700 / VAR_DOTIMP);
  end;
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := Trunc(12 * Fator);
  //
  Memo.Memo.Text := FormatDateTime('dd/mm/yyyy hh:nn', Now());
  Memo.HAlign    := haRight;
  Memo.DisplayFormat.Kind := fkText;
  //Memo.DisplayFormat.FormatStr := 'dd/mm/yyyy hh:mm';
  Memo.DisplayFormat.FormatStr := '';
  {
  if CkGrade.Checked then
  begin
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.TopLine.Width := 0.1;
    Memo.Frame.LeftLine.Width := 0.1;
    Memo.Frame.RightLine.Width := 0.1;
    Memo.Frame.BottomLine.Width := 0.1;
  end;
}
  ///  T�tulos dos campos
  FContaCol := 0;
  TopoIni    := 0;
  //MEMHEI := Trunc(PageMemHei / VAR_DOTIMP) ;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP * FtrMemHi);
  MEMLEF := 0;//Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    //  TITULOS DAS COLUNAS
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1] * Fator);
    //
    //inc(k, 1);
    //MEMTOP := TopoIni + 24;// + (k * MEMHEI);
    MEMTOP := TopoIni + MEMHEI;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := Font_Size; // 10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := 'Teste 1'
    else begin
      Memo.Memo.Text := TDBGrid(FDBGrid).Columns[i].Title.Caption;
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  // Configura Banda de
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(PageBanTop / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  //BANHEI := Trunc(PageBanHei / VAR_DOTIMP);
  BANHEI := Trunc(PageBanHei / VAR_DOTIMP * Fator * FtrMemHi);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.DataSet := frxDsPG;
  {
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  }
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  TopoIni    := 0;
  //MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP * Fator * FtrMemHi);
  MEMLEF := 0;//Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  //for i := 0 to FDBGrid).ColCount - Menos do
  for i := 0 to TDBGrid(FDBGrid).Columns.Count - Menos + 1 do
  begin
    MEMLEF := MEMLEF + MEMWID;
    //MEMWID := Trunc(FDBGrid.ColWidths[i + Menos - 1] * Fator);
    //MEMWID := Trunc(TDBGrid(FDBGrid).Columns.Items[i + Menos - 1].Width * Fator);
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1] * Fator);
    //
    //inc(k, 1);
    MEMTOP := TopoIni;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := Font_Size; //10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := '[frxDsPG."Col' + FormatFloat('000', i) + '"]'
    else begin
      Campo := FDBGrid.Columns[i].FieldName;
      case FDBGrid.Columns[I].Alignment of
        taRightJustify: Memo.HAlign := haRight;
        taLeftJustify:  Memo.HAlign := haLeft;
        else            Memo.HAlign := haCenter;
      end;
      for J := 0 to FDBGrid.DataSource.DataSet.FieldCount - 1 do
      begin
        if FDBGrid.DataSource.DataSet.Fields[J].FieldName = Campo then
        begin
          if FDBGrid.DataSource.DataSet.Fields[J].DataType = ftInteger then
          begin
            MyFmtStr := TIntegerField(FDBGrid.DataSource.DataSet.Fields[J]).DisplayFormat;
            if MyFmtStr <> EmptyStr then
              Memo.DisplayFormat.Kind := fkNumeric
            else
              Memo.DisplayFormat.Kind := fkText;
            Memo.DisplayFormat.FormatStr := MyFmtStr;
          end else
          if FDBGrid.DataSource.DataSet.Fields[J].DataType = ftFloat then
          begin
            MyFmtStr := TFloatField(FDBGrid.DataSource.DataSet.Fields[J]).DisplayFormat;
            if MyFmtStr <> EmptyStr then
              Memo.DisplayFormat.Kind := fkNumeric
            else
              Memo.DisplayFormat.Kind := fkText;
            Memo.DisplayFormat.FormatStr := MyFmtStr;
          end else
          if FDBGrid.DataSource.DataSet.Fields[J].DataType = ftTime then
          begin
            MyFmtStr := TTimeField(FDBGrid.DataSource.DataSet.Fields[J]).DisplayFormat;
            if MyFmtStr <> EmptyStr then
              Memo.DisplayFormat.Kind := fkDateTime
            else
              Memo.DisplayFormat.Kind := fkText;
            Memo.DisplayFormat.FormatStr := MyFmtStr;
          end else
          if FDBGrid.DataSource.DataSet.Fields[J].DataType = ftDate then
          begin
            MyFmtStr := TDateField(FDBGrid.DataSource.DataSet.Fields[J]).DisplayFormat;
            if MyFmtStr <> EmptyStr then
              Memo.DisplayFormat.Kind := fkDateTime
            else
              Memo.DisplayFormat.Kind := fkText;
            Memo.DisplayFormat.FormatStr := MyFmtStr;
          end else
          if FDBGrid.DataSource.DataSet.Fields[J].DataType = ftDateTime then
          begin
            MyFmtStr := TDateTimeField(FDBGrid.DataSource.DataSet.Fields[J]).DisplayFormat;
            if MyFmtStr <> EmptyStr then
              Memo.DisplayFormat.Kind := fkDateTime
            else
              Memo.DisplayFormat.Kind := fkText;
            Memo.DisplayFormat.FormatStr := MyFmtStr;
          end else
            ;
        end;
      end;
      if Campo <> EmptyStr then
        Memo.Memo.Text := '[frxDsPG."' + Campo + '"]'
      else
        Memo.Memo.Text := '';
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  //////////////////////////////////////////////////////////////////////////////
  // Configura Foota
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(269 / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PagePagFot / VAR_DOTIMP * FtrMemHi);
  Foot := TfrxPageFooter.Create(Page);
  Foot.CreateUniqueName;
  Foot.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  frxDsPG.DataSet.DisableControls;
  try
{$IFNDEF NAO_USA_FRX}
  case Mostra of
    0: MyObjects.frxMostra(frxReport2, FTitulo);
    1: frxReport2.DesignReport();
    2: MyObjects.frxImprime(frxReport2, FTitulo);
  end;
{$ELSE}
  Geral.MB_Info('"FRX" sem "DEFINE"!');
{$ENDIF}
  finally
    frxDsPG.DataSet.EnableControls;
  end;
end;

procedure TFmMeuDBUses.Imprime_frx3(Mostra: Byte);
const
  PagePagHei = 279;

  PageBanWid = 0;
  PagePagTop = 117; // >(=1,01) - (1,91 = 200)
  PagePagFot = 100;

  PageBanLef = 100;
  PageBanTop = 0;
  //PageBanWid = 0;
  PageBanHei = 50;
  PageMemHei = 50;
  PageColWid = 210;

var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  Foot: TfrxPageFooter;
  BANLEF, BANTOP, BANWID, BANHEI,
  //BARLEF, BARTOP, BARWID, BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  i,
  L, T, W, H, (*k, *)TopoIni: Integer;
  //Fator: Double;
  Campo: String;
  Menos: Byte;
  Orientacao: Integer;

const
  Font_Size = 6;
begin
  Orientacao := MyObjects.SelRadioGroup('Orienta��o de Impress�o',
  'Selecione a orienta��o', ['Retrato', 'Paisagem'], 1);
  if not (Orientacao in ([0,1])) then
    Exit;
  Menos := 0;
  //k := -1;
  // Configura p�gina
  while frxReport2.PagesCount > 0 do
    frxReport2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxReport2);
  Page.CreateUniqueName;
  Page.LeftMargin   := 20;
  Page.RightMargin  := 10;
  Page.TopMargin    := 10;
  Page.BottomMargin := 10;
  Page.Height       := Trunc(PagePagHei / VAR_DOTIMP);
  case Orientacao of
    0: Page.Orientation := poPortrait;
    1: Page.Orientation := poLandscape;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(PageBanWid / VAR_DOTIMP);
  H := Trunc(PagePagTop / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  TITULO DO RELAT�RIO
  FContaCol := 0;
  //TopoIni    := 0;
  MEMTOP := 0;
  MEMHEI := 24;//Trunc(PageMemHei / VAR_DOTIMP); 1,59 = 60 0,59=
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 1800;
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := Trunc(Font_Size * 1.4);
  //
  Memo.Memo.Text := FTitulo;
  ///  T�tulos dos campos
  FContaCol := 0;
  TopoIni   := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    //  TITULOS DAS COLUNAS
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni + 24;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := Font_Size * 2;  // 10
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := 'Teste 1'
    else begin
      Memo.Memo.Text := FDBGrid.Columns[i].Title.Caption;
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  // Configura Banda de
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(PageBanTop / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PageBanHei / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.DataSet := frxDsPG;
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i + Menos - 1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := Font_Size * 1; //10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := '[frxDsPG."Col' + FormatFloat('000', i) + '"]'
    else begin
      Campo := FDBGrid.Columns[i].FieldName;
      Memo.Memo.Text := '[frxDsPG."' + Campo + '"]';
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  // Configura Foota
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(269 / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PagePagFot / VAR_DOTIMP);
  Foot := TfrxPageFooter.Create(Page);
  Foot.CreateUniqueName;
  Foot.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  frxDsPG.DataSet.DisableControls;
  try
{$IFNDEF NAO_USA_FRX}
  case Mostra of
    0: MyObjects.frxMostra(frxReport2, FTitulo);
    1: frxReport2.DesignReport();
    2: MyObjects.frxImprime(frxReport2, FTitulo);
  end;
{$ELSE}
  Geral.MB_Info('"FRX" sem "DEFINE"!');
{$ENDIF}
  finally
    frxDsPG.DataSet.EnableControls;
  end;
end;

procedure TFmMeuDBUses.LocalizaESetaEdDBEdit(x: String);
var
  Form: TForm;
  DBEdit: TDBEdit;
  Nome1, Nome2, FldName: String;
  DS: TDataSource;
  Tb: TmySQLTable;
begin
  Nome1 := 'EdDB' + x;
  Nome2 := 'DBEd' + x;
  DBEdit := nil;
  Form := TForm(Screen.Activeform);
  if (Form.FindComponent(Nome1) as TDBEdit) <> nil then
    DBEDit := Form.FindComponent(Nome1) as TDBEdit
  else
  if (Form.FindComponent(Nome2) as TDBEdit) <> nil then
    DBEDit := Form.FindComponent(Nome2) as TDBEdit;
  //
  if DBEdit <> nil then
  begin
    DS := TDataSource(DBEdit.DataSource);
    if DS <> nil then
    begin
      Tb := TmySQLTable(DS.DataSet);
      if Tb <> nil then
      begin
        FldName := DBEdit.DataField;
        Tb.FieldByName(FldName).AsInteger := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmMeuDBUses.MenuItem1Click(Sender: TObject);
begin
  Visualizaimpresso1Click(Self);
end;

procedure TFmMeuDBUses.MenuItem2Click(Sender: TObject);
begin
  DesignerModificarelatrioantesdeimprimir1Click(Self);
end;

procedure TFmMeuDBUses.MenuItem3Click(Sender: TObject);
begin
  ImprimeImprimesemvizualizar1Click(Self);
end;

procedure TFmMeuDBUses.ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor:
  Variant; var UpdType: TUpdType);
var
  Campo: String;
begin
  Campo := GetPropValue(Objeto, 'UpdCampo');
  try
  Valor := TmySQLDataSet(TDataSource(TdmkDBEdit(Objeto).DataSource).
    DataSet).FieldByName(Campo).Value;
  UpdType := TdmkDBEdit(Objeto).UpdType;
  except
    Geral.MB_Erro('N�o foi poss�vel obter o "Tabela.Campo.Valor" do TDBEdit! ' +
    sLineBreak + 'TDBEdit: ' + TdmkDBEdit(Objeto).Name +
    sLineBreak + 'Campo: ' + TdmkDBEdit(Objeto).DataField);
    //
    raise;
  end;
end;

procedure TFmMeuDBUses.PesquisaNome(Tabela, CampoNome, CampoCodigo, DefinedSQL: String);
begin
  VAR_CADASTRO := 0;
  //
  MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
  //
  FmPesqNome.LaDoc.Visible := False;
  FmPesqNome.EdDoc.Visible := False;
  //
  FmPesqNome.FND              := True;
  FmPesqNome.FNomeTabela      := Tabela;
  FmPesqNome.FNomeCampoNome   := CampoNome;
  FmPesqNome.FNomeCampoCodigo := CampoCodigo;
  FmPesqNome.FDefinedSQL      := DefinedSQL;
  FmPesqNome.ShowModal;
  FmPesqNome.Destroy;
end;

procedure TFmMeuDBUses.PesquisaNomeSQLeJoke(SQLJoke_SQL, SQLJoke_Jok: String);
begin
  VAR_CADASTRO := 0;
  //
  MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
  //
  FmPesqNome.LaDoc.Visible := False;
  FmPesqNome.EdDoc.Visible := False;
  //
  FmPesqNome.FND              := True;
  FmPesqNome.FNomeTabela      := ''; //Tabela;
  FmPesqNome.FNomeCampoNome   := ''; //CampoNome;
  FmPesqNome.FNomeCampoCodigo := ''; //CampoCodigo;
  FmPesqNome.FSQLJoke_SQL     := SQLJoke_SQL;
  FmPesqNome.FSQLJoke_Jok     := SQLJoke_Jok;
  FmPesqNome.ShowModal;
  FmPesqNome.Destroy;
end;

procedure TFmMeuDBUses.PesquisaPeloTexto_Nome(Controle: TControl; dmkEdCB:
  TdmkEditCB; dmkCBox: TdmkDBLookupComboBox; dmkEdIdx: TdmkEditEdIdx;
  LS: TDataSource; Qr: TmySQLQuery; DB:
  TmySQLDataBase; SQLBase, NomeTabela, NomeCampoNome, NomeCampoCodigo, Texto: String);
begin
  MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
  if (Controle is TdmkEditCB) then
    FmPesqNome.EdTxtPesq.Text := TdmkEditCB(Controle).TxtPesq;
  FmPesqNome.FTexto := Texto;
  if Controle is TdmkEditEdIdx then
  begin
    FmPesqNome.FF7  := dmkEdIdx;
    FmPesqNome.FCB  := nil;
    VAR_NomeCompoF7 := TdmkEdit(Controle).Name;
    FmPesqNome.FDB  := DB;
    FmPesqNome.FNomeTabela      := dmkEdIdx.LocF7TableName;
    FmPesqNome.FNomeCampoCodigo := dmkEdIdx.LocF7CodiFldName;
    FmPesqNome.FNomeCampoNome   := dmkEdIdx.LocF7NameFldName;
    //FLocF7SQLText: TStrings;
    //FLocF7SQLMasc: String;
  end else
  if Controle is TdmkEditF7 then
  begin
    FmPesqNome.FF7 := dmkEdCB;
    FmPesqNome.FCB := nil;
    VAR_NomeCompoF7 := TdmkEdit(Controle).Name;
  end else
  begin
    if (Controle is TdmkEditCB) then
      Controle := TdmkEditCB(Controle).DBLookupComboBox;
    FmPesqNome.FCB := TDBLookupComboBox(Controle);
    FmPesqNome.FF7 := TdmkEdit(Controle);
    VAR_NomeCompoF7 := TDBLookupComboBox(Controle).Name;
  end;
  FmPesqNome.FDB := nil;
  if (FmPesqNome.FCB <> nil) and (FmPesqNome.FCB.ListSource <> nil) then
  begin
    LS := TDataSource(FmPesqNome.FCB.ListSource);
    if (LS <> nil) and (LS.Dataset <> nil) then
    begin
      Qr := TmySQLQuery(LS.DataSet);
      if (Qr <> nil) and (Qr.Database <> nil) then
        FmPesqNome.FDB := Qr.Database;
    end;
  end;
  if FmPesqNome.FDB = nil then
    FmPesqNome.FDB := Dmod.MyDB;
////////////////  ini 2019-01-26  //////////////////////////////////////////////
  // �ltima op��o
  if FmPesqNome.FCB <>nil then
  begin
    if Controle is TDBLookupComboBox then
      FmPesqNome.FDS := TDBLookupComboBox(Controle).ListSource;
  end;
////////////////  fim 2019-01-26  //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  FmPesqNome.ShowModal;
  //
  DefineResultadoF7(Controle, dmkEdCB, dmkCBox, dmkEdIdx);
  //
  FmPesqNome.Destroy;
end;

procedure TFmMeuDBUses.PesquisaPrdTamCor(Controle: TControl; dmkEdCB: TdmkEditCB;
  dmkCBox: TdmkDBLookupComboBox; DmkEdIdx: TdmkEditEdIdx; LS: TDataSource;
  Qr: TmySQLQuery; DB: TmySQLDataBase; SQLBase, NomeTabela, NomeCampoNome,
  NomeCampoCodigo: String);
begin
{$IfNDef SemGrade}
  VAR_CADASTRO := 0;
  //
  MyObjects.CriaForm_AcessoTotal(TFmPesqPrdTamCor, FmPesqPrdTamCor);
  //
  FmPesqPrdTamCor.FCB              := dmkCBox;
  FmPesqPrdTamCor.FF7              := dmkEdCB;
  FmPesqPrdTamCor.FControle        := Controle;
  FmPesqPrdTamCor.FLS              := LS;
  FmPesqPrdTamCor.FQr              := Qr;
  FmPesqPrdTamCor.FDB              := DB;
  FmPesqPrdTamCor.FSQLBase         := SQLBase;
  FmPesqPrdTamCor.FNomeTabela      := NomeTabela;
  FmPesqPrdTamCor.FNomeCampoNome   := NomeCampoNome;
  FmPesqPrdTamCor.FNomeCampoCodigo := NomeCampoCodigo;
  //
  FmPesqPrdTamCor.QrPesq.Database  := DB;
{
  FmPesqPrdTamCor.FND              := True;
  FmPesqPrdTamCor.FNomeTabela      := ''; //Tabela;
  FmPesqPrdTamCor.FNomeCampoNome   := ''; //CampoNome;
  FmPesqPrdTamCor.FNomeCampoCodigo := ''; //CampoCodigo;
  FmPesqPrdTamCor.FSQLJoke_SQL     := SQLJoke_SQL;
  FmPesqPrdTamCor.FSQLJoke_Jok     := SQLJoke_Jok;
}
  FmPesqPrdTamCor.ShowModal;
  DefineResultadoF7(Controle, dmkEdCB, dmkCBox, dmkEdIdx);
  FmPesqPrdTamCor.Destroy;
{$EndIf
}end;

procedure TFmMeuDBUses.SbHideClick(Sender: TObject);
begin
  Hide;
end;

procedure TFmMeuDBUses.SbImprimeClick(Sender: TObject);
var
  Query: TmySQLQuery;
  I, K: Integer;
begin
  DBGrid1.Columns.Clear;
  for I := EdColIni.ValueVariant to EdColFim.ValueVariant do
  begin
    K := I-1;
    DBGrid1.Columns.Add;
    DBGrid1.Columns[K].Title := FOriDBGrid.Columns[K].Title;
    DBGrid1.Columns[K].Width := FOriDBGrid.Columns[K].Width;
    DBGrid1.Columns[K].FieldName := FOriDBGrid.Columns[K].FieldName;
    //
  end;
  DBGrid1.DataSource := FOriDBGrid.DataSource;

  Query := TmySQLQuery(FOriDBGrid.DataSource.DataSet);
  frxDsPG.DataSet := Query;
  //
  FTipoGrade := 1; // Grade com origem de DataAware. DBGrid ...
  //FTitulo := FTitulo;
  FDBGrid := DBGrid1;
  FGrade := TStringGrid(DBGrid1);
  //MyObjects.MostraPopupDeBotao(PMImpressao, SbImprime);
  Imprime_Frx2(RGOQueFazer.ItemIndex);
end;

procedure TFmMeuDBUses.Visualizaimpresso1Click(Sender: TObject);
begin
  Imprime_Frx2(0);
end;

procedure TFmMeuDBUses.VirtualKey_F5();
var
  Controle: TControl;
  OK: Boolean;
  DBLookupComboBox: TDBLookupComboBox;
  DataSource: TDataSource;
  DataSet: TDataSet;
begin
  if Screen.ActiveForm = nil then Exit;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB)(* or
    (Controle is TdmkEditF7)*);
    if OK then
    begin
      if Controle is TDBLookupComboBox then
        DBLookupComboBox := TDBLookupComboBox(Controle)
      else
      if Controle is TdmkDBLookupComboBox then
        DBLookupComboBox := TdmkDBLookupComboBox(Controle)
      else
      if Controle is TdmkEditCB then
        DBLookupComboBox := TdmkDBLookupComboBox(TdmkEditCB(Controle).DBLookupComboBox)
      else
      //if Controle is TdmkEditF7 then
        DBLookupComboBox := nil;
      //
      if DBLookupComboBox <> nil then
      begin
        DataSource := TDataSource(DBLookupComboBox.ListSource);
        if DataSource <> nil then
        begin
          DataSet := TDataSet(DataSource.DataSet);
          if DataSet <> nil then
          begin
            if DataSet is TmySQLDataSet then
            begin
              // Evitar quebra de seguran�a!
              // S� abrir se j� estiver aberto!
              if TdataSet(DataSet).State <> dsInactive then
              begin
                TdataSet(DataSet).Close;
                TdataSet(DataSet). Open ;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmMeuDBUses.VirtualKey_F7(Texto: String);
var
  Controle: TControl;
  OK: Boolean;
  LS: TDataSource;
  Qr: TmySQLQuery;
  DB: TmySQLDataBase;
  dmkCBox: TDmkDBLookupComboBox;
  dmkEdCB: TdmkEditCB;
  dmkEdIdx: TdmkEditEdIdx;
  dmkF7PreDefProc: TdmkF7PreDefProc; // = (f7pNone=0, f7pEntidades=1, f7pPrdTamCor=2);
  SQLBase, NomeTabela, NomeCampoNome, NomeCampoCodigo: String;
  //
begin
  if Screen.ActiveForm = nil then Exit;
  dmkEdIdx := nil;
  dmkEdCB  := nil;
  dmkCBox  := nil;
  VAR_CaptionFormOrigem := TForm(Screen.Activeform).Caption;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB) or
    (Controle is TdmkEditF7) or
    (Controle is TdmkEditEdIdx);
    if OK then
    begin
      SQLBase         := EmptyStr;
      NomeTabela      := EmptyStr;
      NomeCampoNome   := EmptyStr;
      NomeCampoCodigo := EmptyStr;
      VAR_CADASTRO    := 0;
      dmkF7PreDefProc := TdmkF7PreDefProc.f7pNone; //=0, f7pEntidades=1, f7pPrdTamCor=2);
      //
      if Controle is TdmkDBLookupComboBox then
        dmkCBox := TdmkDBLookupComboBox(Controle)
      else
      if (Controle is TdmkEditCB) then
      begin
        dmkEdCB := TdmkEditCB(Controle);
        dmkCBox := TdmkDBLookupComboBox(TdmkEditCB(Controle).DBLookupComboBox);
      end;
      //
      if dmkCBox <> nil then
      begin
        dmkEdCB := TdmkEditCB(dmkCBox.dmkEditCB);
        if TdmkDBLookupComboBox(dmkCBox).LocF7PreDefProc <> null then
          dmkF7PreDefProc := dmkCBox.LocF7PreDefProc;
        if (dmkCBox.ListSource <> nil) then
        begin
          LS := TDataSource(dmkCBox.ListSource);
          if (LS <> nil) and (LS.Dataset <> nil) then
          begin
            Qr := TmySQLQuery(LS.DataSet);
            if (Qr <> nil) and (Qr.Database <> nil) then
            begin
              DB := Qr.Database;
            end;
          end;
        end;
        //Geral.MB_Teste(dmkCBox.Name);
        SQLBase         := dmkCBox.LocF7SQLText.Text;
        NomeTabela      := dmkCBox.LocF7TableName;
        NomeCampoNome   := dmkCBox.LocF7NameFldName;
        NomeCampoCodigo := dmkCBox.LocF7CodiFldName;
      end else
      if Controle is TdmkEditEdIdx then
      begin
        dmkEdIdx        := TdmkEditEdIdx(Controle);
        SQLBase         := dmkEdIdx.LocF7SQLText.Text;
        NomeTabela      := dmkEdIdx.LocF7TableName;
        NomeCampoNome   := dmkEdIdx.LocF7NameFldName;
        NomeCampoCodigo := dmkEdIdx.LocF7CodiFldName;
        if not DefineDBdeDmkEditEdIdx(dmkEdIdx, DB) then Exit;
      end;
      case dmkF7PreDefProc of
        //(*0*)TdmkF7PreDefProc.f7pNone:
        //(*1*)TdmkF7PreDefProc.f7pEntidades:
        (*2*)TdmkF7PreDefProc.f7pPrdTamCor: PesquisaPrdTamCor(Controle, dmkEdCB,
          dmkCBox, dmkEdIdx, LS, Qr, DB, SQLBase, NomeTabela, NomeCampoNome,
          NomeCampoCodigo);
        else
          PesquisaPeloTexto_Nome(Controle, dmkEdCB, dmkCBox, dmkEdIdx,
          LS, Qr, DB, SQLBase, NomeTabela, NomeCampoNome, NomeCampoCodigo,
          Texto);
      end;
    end;
  end;
end;

end.
