unit CadLista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums, frxClass,
  frxDBSet, Vcl.Menus;

type
  THackDBGrid = class(TDBGrid);
  TFmCadLista = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    TbCad: TmySQLTable;
    DsCad: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    TbCadCodigo: TIntegerField;
    TbCadNome: TWideStringField;
    Panel5: TPanel;
    QrPesq: TMySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    LbItensMD: TListBox;
    BtInclui: TBitBtn;
    frxCAD_LISTA_001: TfrxReport;
    frxDsCad: TfrxDBDataset;
    SbImprime: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtLoadCSV: TBitBtn;
    PnLoad: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    EdLoadCSVArq: TdmkEdit;
    Grade: TStringGrid;
    Panel8: TPanel;
    BtCarregaB: TBitBtn;
    BtInsereB: TBitBtn;
    MeAvisos: TMemo;
    PB1: TProgressBar;
    QrPsq2: TMySQLQuery;
    QrPsq2Codigo: TIntegerField;
    QrPsq2Nome: TWideStringField;
    PMInsereB: TPopupMenu;
    Umitemporclula1: TMenuItem;
    Vriositensporclula1: TMenuItem;
    BtSelecionaB: TBitBtn;
    FabricantesxMarcas1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbCadBeforePost(DataSet: TDataSet);
    procedure TbCadDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbCadAfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure DBGrid1Exit(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbCadAfterInsert(DataSet: TDataSet);
    procedure TbCadNewRecord(DataSet: TDataSet);
    procedure TbCadBeforeOpen(DataSet: TDataSet);
    procedure frxCAD_LISTA_001GetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure TbCadBeforeEdit(DataSet: TDataSet);
    procedure BtInsereBClick(Sender: TObject);
    procedure BtLoadCSVClick(Sender: TObject);
    procedure Umitemporclula1Click(Sender: TObject);
    procedure Vriositensporclula1Click(Sender: TObject);
    procedure BtSelecionaBClick(Sender: TObject);
    procedure BtCarregaBClick(Sender: TObject);
    procedure FabricantesxMarcas1Click(Sender: TObject);
  private
    { Private declarations }
    //FCharPos, FGridRow: Integer;
    function CarregaAtual_SG(Texto: String): Boolean;
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    FTabela, FSQLInsExtra: String;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    FFldIndices: array of String;
    FValIndices: array of Variant;
*)
    //
    procedure ReabrePesquisa();
  end;

  var
  FmCadLista: TFmCadLista;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, GetValor, MyDBCheck;

{$R *.DFM}

procedure TFmCadLista.BtAlteraClick(Sender: TObject);
begin
  if (TbCad.State <> dsInactive) and (TbCad.RecordCount > 0) then
  begin
    TbCad.Edit;
    DBGrid1.SetFocus;
    DBGrid1.SelectedField := TbCadNome;
  end;
end;

procedure TFmCadLista.BtCarregaBClick(Sender: TObject);
var
  Arquivo: String;
begin
  MeAvisos.Lines.Clear;
  //
  Arquivo := EdLoadCSVArq.Text;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    MyObjects.Xls_To_StringGrid_Faster(Grade, Arquivo, '', PB1,
    LaAviso1, LaAviso2, MeAvisos, 1, 0);
    //
    BtInsereB.Enabled := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Arquivo carregado!');
  end;
end;

procedure TFmCadLista.BtExcluiClick(Sender: TObject);
begin
  if (TbCad.State <> dsInactive) and (TbCad.RecordCount > 0) then
    TbCad.Delete;
end;

procedure TFmCadLista.BtIncluiClick(Sender: TObject);
var
  Codigo: Variant;
  Qry: TmySQLQuery;
begin
  if FReservados <> Null then
  begin
    Codigo := 0;
    //
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Codigo, 0, 0,
    '', Geral.FF0(FReservados), True, 'C�digo', 'Informe o C�digo: ', 0, Codigo) then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        Qry.Database := Dmod.MyDB;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT ' + FFldID,
        'FROM ' + TbCad.TableName,
        'WHERE ' + FFldID + '=' + Geral.FF0(Codigo),
        '']);
        if Qry.RecordCount > 0 then
        begin
          Geral.MB_Aviso('O c�digo ' + String(Codigo) +
          ' j� est� sendo usado em outro registro!');
          Exit;
        end else
        begin
          if UMyMod.SQLInsUpd(Qry, stIns, TBCad.TableName, False, [
          ], [
          'Codigo'], [
          ], [
          Codigo], True) then
          begin
            TbCad.Refresh;
            TbCad.Locate(FFldID, Codigo, []);
          end;
        end;
      finally
        Qry.Free;
      end;
    end;
  end else
  begin
    TbCad.Insert;
    if TbCad.State in ([dsInsert, dsEdit]) then
    begin
      DBGrid1.SetFocus;
      DBGrid1.SelectedField := TbCadNome;
    end else
      Geral.MB_Aviso('Verifique se esta tabela pode ser editada!');
  end;
end;

procedure TFmCadLista.BtInsereBClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMInsereB, BtInsereB);
  //
end;

procedure TFmCadLista.BtLoadCSVClick(Sender: TObject);
begin
  PnLoad.Visible := True;
end;

procedure TFmCadLista.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := TbCadCodigo.Value;
  Close;
end;

procedure TFmCadLista.BtSelecionaBClick(Sender: TObject);
var
  Pasta, Arquivo: String;
begin
  Pasta   := 'C:\Dermatek\CSV\';
  Arquivo := 'Cadastros.csv';
  //
  MyObjects.DefineArquivo2(Self, EdLoadCSVArq, Pasta, Arquivo,
    TPriorityPath.ppAtual_Edit);
end;

function TFmCadLista.CarregaAtual_SG(Texto: String): Boolean;
const
  FixCol = 1;
var
  //
  SQLType: TSQLType;
  //
  I, O, Codigo: Integer;
  Nome: String;
  Campos: array of String;
  Valores: array of String;
begin
  Result  := False;
  SQLType := stIns;
  Nome    := EmptyStr;
  for I := 1 to Length(Texto) do
  begin
    O := Ord(Texto[I]);
    if (O > 31) and (O < 255) then
      Nome := Nome + Texto[I];
  end;
  if Nome = EmptyStr then
  begin
    Result := True;
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq2, Dmod.MyDB, [
  'SELECT ' + FFldID + ', ' + FFldNome,
  'FROM ' + TbCad.TableName,
  'WHERE ' + FFldNome + ' ="' + Nome + '"',
  '']);
  if QrPsq2.RecordCount = 0 then
  begin
    case FNovoCodigo of
      ncControle: Codigo := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, TbCad.TableName, FFldID, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: Codigo := UMyMod.BPGS1I32(
        TbCad.TableName, FFldID, '', '', tsPos, stIns, 0);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
        Halt(0);
        Exit;
      end;
    end;

    (*
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabela, False, [
    'Nome', FCamposExtras], [
    'Codigo'], [
    Nome, FValorsExtras], [
    Codigo], True);
    *)
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
      'INSERT INTO ' + FTabela,
      'SET Codigo=' + Geral.FF0(Codigo),
      ', Nome="' + Nome + '"',
      FSQLInsExtra,
      '']));
    Result := True;
  end else
    Result := True;
end;

procedure TFmCadLista.DBGrid1Exit(Sender: TObject);
begin
  //FGridRow := THackDBGrid(DBGrid1).Row;
  //FCharPos := THackDBGrid(DBGrid1).InplaceEditor.SelStart;
end;

procedure TFmCadLista.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadLista.FabricantesxMarcas1Click(Sender: TObject);
var
  Col, Row, I, Codigo, Controle, GraFabMCd: Integer;
  sGraFabCad, sGraFabMCd, Nome: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if Geral.MB_Pergunta('Deseja excluir TODOS cadastros existentes?') = ID_YES then
  begin
    if Geral.MB_Pergunta('Confirma que realmente deseja excluir TODOS cadastros existentes?') = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM grafabmar');
    end;
  end;
  //
  Nome := '';
  PB1.Position := 0;
  PB1.Max := Grade.RowCount - 1;
  for I := 0 to Grade.RowCount - 1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    sGraFabCad := Trim(Grade.Cells[1, I]);
    sGraFabMCd := Trim(Grade.Cells[2, I]);
    //
    if (sGraFabCad <> EmptyStr) and (sGraFabMCd <> EmptyStr) and (sGraFabCad <> '0') then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
      'SELECT Codigo, Nome  ',
      'FROM grafabcad ',
      'WHERE Nome = "' + sGraFabCad + '"',
      '']);
      Codigo := QrPesq.FieldByName('Codigo').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsq2, Dmod.MyDB, [
      'SELECT Codigo, Nome  ',
      'FROM grafabmcd ',
      'WHERE Nome = "' + sGraFabMCd + '"',
      '']);
      GraFabMCd := QrPsq2.FieldByName('Codigo').AsInteger;
      //
      Controle := UMyMod.BuscaEmLivreY_Def('grafabmar', 'Controle', stIns, Controle);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'grafabmar', False, [
      'Codigo', 'Nome', 'GraFabMCd'], [
      'Controle'], [
      Codigo, Nome, GraFabMCd], [
      Controle], True) then ;
      //
    end;
  end;
  PnLoad.Visible := False;
  UnDmkDAC_PF.AbreTable(TbCad, Dmod.MyDB);
end;

procedure TFmCadLista.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmCadLista.FormCreate(Sender: TObject);
begin
  FNovoCodigo := ncIdefinido;
  FReservados := Null;
  FSoReserva := False;
  FPermiteExcluir := False;
  FFldID := 'Codigo';
  FFldNome := 'Nome';
  FTabela := '';
  FSQLInsExtra := '';
end;

procedure TFmCadLista.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadLista.frxCAD_LISTA_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
    Value := LaTitulo1A.Caption
  else
  if VarName = 'VARF_CAMPO_A' then
  begin
    if TbCad.Fields.Count = 3 then
      Value := TbCad.Fields[2].FieldName
    else
      Value := '';
  end
  else
  if VarName = 'VARF_VALOR_A' then
  begin
    if TbCad.Fields.Count = 3 then
      Value := TbCad.Fields[2].AsString
    else
      Value := '';
  end
  else
end;

procedure TFmCadLista.LbItensMDDblClick(Sender: TObject);
var
  Texto: String;
  Ate: Integer;
begin
  Texto := LbItensMD.Items[LbItensMD.ItemIndex];
  if Texto <> '' then
  begin
    Ate := Pos(']', Texto);
    Texto := Copy(Texto, 1, Ate);
    (*
    Clipboard.AsText := Texto;
    THackDBGrid(DBGrid1).InplaceEditor.PasteFromClipboard;
    *)
    TBCad.Edit;
    TBCadNome.Value := TBCadNome.Value + Texto;
    TBCad.Post;
  end;
  DBGrid1.SetFocus;
end;

procedure TFmCadLista.ReabrePesquisa();
var
  Texto, SQL: String;
  K, N, I: Integer;
begin
  QrPesq.Close;
  Texto := EdPesq.Text;
  K := Length(Texto);
  N := TBTam.Position;
  if N < 3 then
    N := 3;
  if K >= 3 then
  begin
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (' + FFldNome + ' LIKE "%' + Copy(Texto, I, N) + '%")' + sLineBreak;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT ' + FFldID + ', ' + FFldNome,
    'FROM ' + TbCad.TableName,
    'WHERE (' + FFldNome + ' LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
  end;
end;

procedure TFmCadLista.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxCAD_LISTA_001, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxCAD_LISTA_001, LaTitulo1A.Caption);
end;

procedure TFmCadLista.TbCadAfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TbCad.Cancel;
end;

procedure TFmCadLista.TbCadAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TbCadCodigo.Value;
end;

procedure TFmCadLista.TbCadBeforeEdit(DataSet: TDataSet);
begin
  if TbCadCodigo.Value = 0 then
  begin
    TbCad.Cancel;
    //TbCad.Append; // Erro
  end;
end;

procedure TFmCadLista.TbCadBeforeOpen(DataSet: TDataSet);
(*
var
  I: Integer;
  Txt: String;
*)
begin
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
  I := High(FFldIndices);
  TbCad.Filter := '';
  TbCad.Close;  // Ter certeza ??
  TbCad.Filtered := False;
  for I := Low(FFldIndices) to High(FFldIndices) do
  begin
    Txt := 'and ' + FFldIndices[I] + Geral.VariavelToString(FFldIndices[I]);
  end;
  if Txt <> '' then
  begin
    Txt := Copy(Txt, 4);
    TbCad.Filter := Txt;
    TbCad.Filtered := True;
  end;
*)
end;

procedure TFmCadLista.TbCadBeforePost(DataSet: TDataSet);
begin
  if Tbcad.State = dsInsert then
  begin
    case FNovoCodigo of
      ncControle: TbCadCodigo.Value := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, TbCad.TableName, FFldID, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: TbCadCodigo.Value := UMyMod.BuscaProximoGerlSeq1Int32(
        TbCad.TableName, FFldID, '', '', tsDef, stIns, 0, FReservados);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
        Halt(0);
        Exit;
      end;
    end;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    for I := Low(FFldIndices) to High(FFldIndices) do
      TbCad.FieldByName(FFldIndices[I]).AsVariant := FFldIndices[I];
*)
  end
  else if Tbcad.State = dsEdit then
  begin
    if TbCadCodigo.Value = 0 then
      TbCad.Cancel;
  end;
end;

procedure TFmCadLista.TbCadDeleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmCadLista.TbCadNewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TbCad.Cancel;
end;

procedure TFmCadLista.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadLista.Umitemporclula1Click(Sender: TObject);
var
  Col, Row, I: Integer;
  Texto: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if Geral.MB_Pergunta('Deseja excluir TODOS cadastros existentes?') = ID_YES then
  begin
    if Geral.MB_Pergunta('Confirma que realmente deseja excluir TODOS cadastros existentes?') = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + FTabela);
    end;
  end;
  //
  PB1.Position := 0;
  PB1.Max := Grade.RowCount - 1;
  for I := 0 to Grade.RowCount - 1 do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    //
    Texto   := Trim(Grade.Cells[1, I]);
    if CarregaAtual_SG(Texto) = False then
      Exit;
  end;
  PnLoad.Visible := False;
  UnDmkDAC_PF.AbreTable(TbCad, Dmod.MyDB);
end;

procedure TFmCadLista.Vriositensporclula1Click(Sender: TObject);
var
  LstLin1: TStringList;
  I, N: Integer;
  Linha, Divisor: String;
  //
  Texto, Nome: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if Geral.MB_Pergunta('Deseja excluir TODOS cadastros existentes?') = ID_YES then
  begin
    if Geral.MB_Pergunta('Confirma que realmente deseja excluir TODOS cadastros existentes?') = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + FTabela);
    end;
  end;
  //
  Divisor := '/';
  //
  if InputQuery('Separador de cadastros', 'Informe o separador:', Divisor) then
  begin
    if Divisor <> EmptyStr then
    begin
      LstLin1 := TStringList.Create;
      try
        PB1.Position := 0;
        PB1.Max := Grade.RowCount - 1;
        for I := 0 to Grade.RowCount - 1 do
        begin
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //
          Linha := Divisor + Grade.Cells[1, I] + Divisor;
          //
          LstLin1 := Geral.Explode3(Linha, Divisor);
          //
          Texto := EmptyStr;
          for N := 0 to LstLin1.Count - 1 do
          begin
            Nome  := Trim(LstLin1[N]);
            if Nome <> EmptyStr then
              //Texto := Texto + sLineBreak + Nome;
            if CarregaAtual_SG(Nome) = False then
              Exit;
          end;
          //Geral.MB_Teste(Texto);
        end;
      finally
        LstLin1.Free;
      end;
      //
      PnLoad.Visible := False;
      UnDmkDAC_PF.AbreTable(TbCad, Dmod.MyDB);
      //
    end;
  end;
end;

end.
