unit VerifiDBCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,   
     ComCtrls,
  StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts, UnMLAGeral,
  mySQLDbTables, dmkGeral;

Type
  TFmVerifiDBCliente = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    BtSair: TBitBtn;
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    CkEstrutLoc: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    CkRegObrigat: TCheckBox;
    procedure BtSairClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FDBExtra: TmySQLDataBase;
  end;

var
  FmVerifiDBCliente: TFmVerifiDBCliente;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal, ModuleGeral;

{$R *.DFM}

procedure TFmVerifiDBCliente.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDBCliente.BtConfirmaClick(Sender: TObject);
var
  Versao, Resp: Integer;
  Verifica: Boolean;
begin
  Verifica := False;
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  if Versao <= CO_VERSAO then Verifica := True
  else begin
    Resp := Application.MessageBox('A vers�o do execut�vel � inferior a do '+
    'banco de dados atual. N�o � recomendado executar a verifica��o com '+
    'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
    'Confirma assim mesmo a verifica��o?', 'Vers�o Antiga em Execu��o',
    MB_YESNO+MB_ICONWARNING);
    if Resp = ID_YES then Verifica := True;
  end;
  if Verifica then
  begin
    DBCheck.EfetuaVerificacoes(DmodG.MyPID_DB, nil, Memo1,
    CkEstrutura.Checked, CkEstrutLoc.Checked, True,
    CkPergunta.Checked, CkRegObrigat.Checked);
  end;
end;

procedure TFmVerifiDBCliente.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDBCliente.FormCreate(Sender: TObject);
var
  BD: String;
  Resp: Word;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW DATABASES');
  Dmod.QrAux.Open;
  BD := CO_VAZIO;
  while not Dmod.QrAux.Eof do
  begin
    if Uppercase(Dmod.QrAux.FieldByName('Database').AsString)=Uppercase(VAR_MyPID_DB_NOME) then
      BD := VAR_MyPID_DB_NOME;
    Dmod.QrAux.Next;
  end;
  DModG.MyPID_DB.Close;
  DModG.MyPID_DB.DataBaseName := BD;
  if DModG.MyPID_DB.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados '+VAR_MyPID_DB_NOME+
      ' n�o existe e deve ser criado. Confirma a cria��o?'), 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('CREATE DATABASE '+VAR_MyPID_DB_NOME);
      Dmod.QrAux.ExecSQL;
      DModG.MyPID_DB.DatabaseName := VAR_MyPID_DB_NOME;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
end;

procedure TFmVerifiDBCliente.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 31);
end;

procedure TFmVerifiDBCliente.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  DBCheck.EfetuaVerificacoes(DmodG.MyPID_DB, nil, Memo1,
  CkEstrutura.Checked, False, True, False, CkRegObrigat.Checked);
  BtSair.Enabled := True;
  CkPergunta.Checked  := False;
  CkEstrutLoc.Checked := False;
end;

procedure TFmVerifiDBCliente.FormShow(Sender: TObject);
begin
  FmPrincipal.Hide;
end;

procedure TFmVerifiDBCliente.FormHide(Sender: TObject);
begin
  FmPrincipal.Show;
end;

end.
