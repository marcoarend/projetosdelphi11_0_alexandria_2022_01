unit CreateGeral;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTableGerl = (ntrtt_AgendaEnt, ntrtt_AgendaImp,
    ntrtt_Empresas, ntrtt_Niveis, ntrtt_ItensPorCod);
  TAcaoCreateGer = (acDrop, acCreate, acFind);
  TCreateGeral = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttAgendaEnt(Qry: TmySQLQuery);
    procedure Cria_ntrttAgendaImp(Qry: TmySQLQuery);
    procedure Cria_ntrttEmpresas(Qry: TmySQLQuery);
    procedure Cria_ntrttItensPorCod(Qry: TmySQLQuery);
    procedure Cria_ntrttNiveis(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableGerl;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateGeral: TCreateGeral;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TCreateGeral.Cria_ntrttAgendaEnt(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  QuestaoTyp                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Entidade                       int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateGeral.Cria_ntrttAgendaImp(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CtrlParti                      int(11)                ,'); // Pode ser nulo!
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  QuestaoTyp                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  QuestaoCod                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  QuestaoExe                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Inicio                         datetime     NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  Termino                        datetime     NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  EmprEnti                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Local                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cor                            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cption                         tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                           varchar(255)                               ,');
  Qry.SQL.Add('  Notas                          varchar(255)                               ,');
  Qry.SQL.Add('  FatoGeradr                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FatoGeradr                  varchar(60)                                ,');
  Qry.SQL.Add('  NO_QuestaoExe                  varchar(60)                                ,');
  Qry.SQL.Add('  NO_Lugar                       varchar(100)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateGeral.Cria_ntrttEmpresas(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CodCliInt            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CodEnti              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CodFilial            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(100)                               ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateGeral.Cria_ntrttItensPorCod(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(100)                               ,');
  Qry.SQL.Add('  Itens                bigint(20)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateGeral.Cria_ntrttNiveis(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  oNiv5                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cNiv5                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nNiv5                varchar(100)                               ,');
  Qry.SQL.Add('  aNiv5                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  oNiv4                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cNiv4                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nNiv4                varchar(100)                               ,');
  Qry.SQL.Add('  aNiv4                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  oNiv3                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cNiv3                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nNiv3                varchar(60)                                ,');
  Qry.SQL.Add('  aNiv3                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  oNiv2                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cNiv2                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nNiv2                varchar(60)                                ,');
  Qry.SQL.Add('  aNiv2                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  oNiv1                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cNiv1                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nNiv1                varchar(60)                                ,');
  Qry.SQL.Add('  aNiv1                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Tabela               tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TCreateGeral.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableGerl; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_AgendaEnt:        Nome := Lowercase('_AgendaEnt_');
      ntrtt_AgendaImp:        Nome := Lowercase('_AgendaImp_');
      ntrtt_Empresas:         Nome := Lowercase('_Empresas_');
      ntrtt_ItensPorCod:      Nome := Lowercase('_ItensPorCod_');
      ntrtt_Niveis:           Nome := Lowercase('_Niveis_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro('Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_AgendaEnt:         Cria_ntrttAgendaEnt(Qry);
    ntrtt_AgendaImp:         Cria_ntrttAgendaImp(Qry);
    ntrtt_Empresas:          Cria_ntrttEmpresas(Qry);
    ntrtt_ItensPorCod:       Cria_ntrttItensPorCod(Qry);
    ntrtt_Niveis:            Cria_ntrttNiveis(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

end.

