object FmInstallMySQL41: TFmInstallMySQL41
  Left = 369
  Top = 188
  Caption = 'Inicializador de Banco de Dados'
  ClientHeight = 288
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 468
    Height = 240
    Align = alClient
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 240
    Width = 468
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BtSair: TBitBtn
      Tag = 13
      Left = 367
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSairClick
    end
  end
  object BD: TmySQLDatabase
    DatabaseName = 'mysql'
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=mysql'
      'UID=root'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 32
    Top = 16
  end
  object QrIns: TmySQLQuery
    Database = BD
    SQL.Strings = (
      
        'INSERT INTO user (Host, User, Password, Select_priv, Insert_priv' +
        ', Update_priv, Delete_priv, Create_priv, Drop_priv, Reload_priv,' +
        ' Shutdown_priv, Process_priv, File_priv, Grant_priv, References_' +
        'priv, Index_priv, Alter_priv) VALUES (:P0, '#39'root'#39', PASSWORD(:P1)' +
        ', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y'#39', '#39'Y' +
        #39', '#39'Y'#39')')
    Left = 76
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrDel: TmySQLQuery
    Database = BD
    SQL.Strings = (
      'DELETE FROM user ')
    Left = 108
    Top = 16
  end
  object QrUser: TmySQLQuery
    Database = BD
    SQL.Strings = (
      'SELECT * FROM user')
    Left = 208
    Top = 220
    object QrUserHost: TWideStringField
      DisplayWidth = 12
      FieldName = 'Host'
      Required = True
      Size = 60
    end
    object QrUserUser: TWideStringField
      DisplayWidth = 19
      FieldName = 'User'
      Required = True
      Size = 16
    end
    object QrUserPassword: TWideStringField
      DisplayWidth = 19
      FieldName = 'Password'
      Required = True
      Size = 16
    end
    object QrUserSelect_priv: TBooleanField
      DisplayWidth = 11
      FieldName = 'Select_priv'
    end
    object QrUserInsert_priv: TBooleanField
      DisplayWidth = 10
      FieldName = 'Insert_priv'
    end
    object QrUserUpdate_priv: TBooleanField
      DisplayWidth = 12
      FieldName = 'Update_priv'
    end
    object QrUserDelete_priv: TBooleanField
      DisplayWidth = 11
      FieldName = 'Delete_priv'
    end
    object QrUserCreate_priv: TBooleanField
      DisplayWidth = 11
      FieldName = 'Create_priv'
    end
    object QrUserDrop_priv: TBooleanField
      DisplayWidth = 9
      FieldName = 'Drop_priv'
    end
    object QrUserReload_priv: TBooleanField
      DisplayWidth = 12
      FieldName = 'Reload_priv'
    end
    object QrUserShutdown_priv: TBooleanField
      DisplayWidth = 14
      FieldName = 'Shutdown_priv'
    end
    object QrUserProcess_priv: TBooleanField
      DisplayWidth = 12
      FieldName = 'Process_priv'
    end
    object QrUserFile_priv: TBooleanField
      DisplayWidth = 8
      FieldName = 'File_priv'
    end
    object QrUserGrant_priv: TBooleanField
      DisplayWidth = 10
      FieldName = 'Grant_priv'
    end
    object QrUserReferences_priv: TBooleanField
      DisplayWidth = 16
      FieldName = 'References_priv'
    end
    object QrUserIndex_priv: TBooleanField
      DisplayWidth = 10
      FieldName = 'Index_priv'
    end
    object QrUserAlter_priv: TBooleanField
      DisplayWidth = 9
      FieldName = 'Alter_priv'
    end
  end
  object DsUser: TDataSource
    DataSet = QrUser
    Left = 236
    Top = 220
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 188
    Top = 96
  end
end
