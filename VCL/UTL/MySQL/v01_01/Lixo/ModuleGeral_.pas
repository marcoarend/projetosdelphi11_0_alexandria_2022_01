unit ModuleGeral_;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, comctrls, frxClass, frxDBSet, stdctrls, dmkGeral, dmkLabel,
  dmkEdit, Variants, ABSMain, dmkEditCB, dmkDBLookupComboBox, Menus, UnMsgInt,
  UnInternalConsts, UnInternalConsts2, ExtCtrls, (*IdBaseComponent, IdComponent,
  IdRawBase, IdRawClient, IdIcmpClient,*) Registry,
  {$IfDef cAdvToolx} //Berlin
  AdvToolBar, AdvGlowButton,
  {$EndIf} //Berlin
  TypInfo, UnDmkProcFunc, DmkDAC_PF, dmkDBGrid, dmkImage, dmkDBGridZTO, UMySQLDB,
  dmkVariable, UnDmkEnums, Grids, DBGrids, ShellApi, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, UnProjGroup_Vars, dmkPageControl,


  //System.ImageList, Vcl.ImgList,
  Vcl.Buttons, Vcl.ToolWin;

type
  THackDBGrid = class(TDBGrid);
  THackStringGrid = class(TStringGrid);
  TDModG = class(TDataModule)
    QrSCE: TMySQLQuery;
    QrSCECODIGOCONTA: TIntegerField;
    QrSCENOMECONTA: TWideStringField;
    QrSCECODIGOSUBGRUPO: TIntegerField;
    QrSCENOMESUBGRUPO: TWideStringField;
    QrSCECODIGOGRUPO: TIntegerField;
    QrSCENOMEGRUPO: TWideStringField;
    QrSCECODIGOCONJUNTO: TIntegerField;
    QrSCENOMECONJUNTO: TWideStringField;
    QrSCECODIGOplano: TIntegerField;
    QrSCENOMEplano: TWideStringField;
    QrSCEPL_ATU: TFloatField;
    QrSCEPL_FUT: TFloatField;
    QrSCECJ_ATU: TFloatField;
    QrSCECJ_FUT: TFloatField;
    QrSCEGR_ATU: TFloatField;
    QrSCEGR_FUT: TFloatField;
    QrSCESG_ATU: TFloatField;
    QrSCESG_FUT: TFloatField;
    QrSCECO_ATU: TFloatField;
    QrSCECO_FUT: TFloatField;
    QrSCEPL_CS: TSmallintField;
    QrSCECJ_CS: TSmallintField;
    QrSCEGR_CS: TSmallintField;
    QrSCESG_CS: TSmallintField;
    QrSCECO_CS: TSmallintField;
    QrSCEPL_ATU_TXT: TWideStringField;
    QrSCEPL_FUT_TXT: TWideStringField;
    QrSCECJ_ATU_TXT: TWideStringField;
    QrSCECJ_FUT_TXT: TWideStringField;
    QrSCEGR_ATU_TXT: TWideStringField;
    QrSCEGR_FUT_TXT: TWideStringField;
    QrSCESG_ATU_TXT: TWideStringField;
    QrSCESG_FUT_TXT: TWideStringField;
    QrSCECO_ATU_TXT: TWideStringField;
    QrSCECO_FUT_TXT: TWideStringField;
    QrSCEPL_PEA: TFloatField;
    QrSCEPL_PED: TFloatField;
    QrSCECJ_PEA: TFloatField;
    QrSCECJ_PED: TFloatField;
    QrSCEGR_PEA: TFloatField;
    QrSCEGR_PED: TFloatField;
    QrSCESG_PEA: TFloatField;
    QrSCESG_PED: TFloatField;
    QrSCECO_PEA: TFloatField;
    QrSCECO_PED: TFloatField;
    QrSCEPL_PEA_TXT: TWideStringField;
    QrSCEPL_PED_TXT: TWideStringField;
    QrSCECJ_PEA_TXT: TWideStringField;
    QrSCECJ_PED_TXT: TWideStringField;
    QrSCEGR_PEA_TXT: TWideStringField;
    QrSCEGR_PED_TXT: TWideStringField;
    QrSCESG_PEA_TXT: TWideStringField;
    QrSCESG_PED_TXT: TWideStringField;
    QrSCECO_PEA_TXT: TWideStringField;
    QrSCECO_PED_TXT: TWideStringField;
    frxDsSCE: TfrxDBDataset;
    QrCST: TMySQLQuery;
    QrCSTSdoAtu: TFloatField;
    QrCSTSdoFut: TFloatField;
    QrCSTPerAnt: TFloatField;
    QrCSTPerAtu: TFloatField;
    frxDsCST: TfrxDBDataset;
    QrMaster: TMySQLQuery;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterSolicitaSenha: TSmallintField;
    QrMasterEm: TWideStringField;
    QrMasterUsaAccMngr: TSmallintField;
    QrMasterMasSenha: TWideStringField;
    QrMasterMasLogin: TWideStringField;
    QrMasterMonitorar: TSmallintField;
    QrMasterLicenca: TWideStringField;
    QrMasterDistorcao: TIntegerField;
    QrMasterDataI: TDateField;
    QrMasterDataF: TDateField;
    QrMasterHoje: TDateField;
    QrMasterHora: TTimeField;
    QrMasterMasAtivar: TWideStringField;
    QrMasterLimite: TSmallintField;
    QrMasterSitSenha: TSmallintField;
    QrMasterChekF: TWideStringField;
    QrMasterDiasExtra: TIntegerField;
    QrMasterChekMonit: TWideStringField;
    QrMasterLicTel: TWideStringField;
    QrMasterLicMail: TWideStringField;
    QrMasterLicInfo: TWideStringField;
    QrMasterAlterWeb: TSmallintField;
    QrMasterAtivo: TSmallintField;
    QrMasterHabilModulos: TWideMemoField;
    frxDsMaster: TfrxDBDataset;
    frxDsDono: TfrxDBDataset;
    QrDepto: TMySQLQuery;
    QrDeptoNOMEDEPTO: TWideStringField;
    QrEndereco: TMySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOME_ENT: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoCEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrEnderecoRespons1: TWideStringField;
    QrEnderecoNO_2_ENT: TWideStringField;
    QrEnderecoNO_TIPO_DOC: TWideStringField;
    QrEnderecoENumero: TIntegerField;
    QrEnderecoPNumero: TIntegerField;
    QrEnderecoNUMERO: TIntegerField;
    QrEnderecoELograd: TSmallintField;
    QrEnderecoPLograd: TSmallintField;
    QrEnderecoLOGRAD: TSmallintField;
    QrEnderecoECEP: TIntegerField;
    QrEnderecoPCEP: TIntegerField;
    QrEnderecoCEP: TIntegerField;
    QrEnderecoE_MIN: TWideStringField;
    QrEnderecoEMAIL: TWideStringField;
    QrEnderecoRG: TWideStringField;
    QrEnderecoSSP: TWideStringField;
    QrEnderecoDataRG: TDateField;
    QrEnderecoTRATO: TWideStringField;
    QrEnderecoENDEREF: TWideStringField;
    QrEnderecoCODMUNICI: TFloatField;
    QrEnderecoCODPAIS: TFloatField;
    QrEnderecoSITE: TWideStringField;
    QrEnderecoRespons2: TWideStringField;
    frxDsEndereco: TfrxDBDataset;
    QrPerf: TMySQLQuery;
    QrPerfLibera: TSmallintField;
    QrPerfOlha: TSmallintField;
    QrPerfInclui: TSmallintField;
    QrPerfAltera: TSmallintField;
    QrPerfExclui: TSmallintField;
    Qrperfjanela: TWideStringField;
    Qrperfjan: TMySQLQuery;
    QrperfjanJanela: TWideStringField;
    QrperfjanNome: TWideStringField;
    QrperfjanDescricao: TWideStringField;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    QrSBCodUsu: TIntegerField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    QrSB2CodUsu: TIntegerField;
    DsSB2: TDataSource;
    DsSB3: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    QrSB3CodUsu: TIntegerField;
    QrSB4: TMySQLQuery;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrSB4CodUsu: TWideStringField;
    DsSB4: TDataSource;
    QrEndereco2: TMySQLQuery;
    QrEndereco2Codigo: TIntegerField;
    QrEndereco2Cadastro: TDateField;
    QrEndereco2NOME_ENT: TWideStringField;
    QrEndereco2CNPJ_CPF: TWideStringField;
    QrEndereco2IE_RG: TWideStringField;
    QrEndereco2NIRE_: TWideStringField;
    QrEndereco2RUA: TWideStringField;
    QrEndereco2COMPL: TWideStringField;
    QrEndereco2BAIRRO: TWideStringField;
    QrEndereco2CIDADE: TWideStringField;
    QrEndereco2NOMELOGRAD: TWideStringField;
    QrEndereco2NOMEUF: TWideStringField;
    QrEndereco2Pais: TWideStringField;
    QrEndereco2Tipo: TSmallintField;
    QrEndereco2TE1: TWideStringField;
    QrEndereco2FAX: TWideStringField;
    QrEndereco2ENatal: TDateField;
    QrEndereco2PNatal: TDateField;
    QrEndereco2Respons1: TWideStringField;
    QrEndereco2CEP_TXT: TWideStringField;
    QrEndereco2NUMERO_TXT: TWideStringField;
    QrEndereco2E_ALL: TWideStringField;
    QrEndereco2CNPJ_TXT: TWideStringField;
    QrEndereco2FAX_TXT: TWideStringField;
    QrEndereco2TE1_TXT: TWideStringField;
    QrEndereco2NATAL_TXT: TWideStringField;
    QrEndereco2NO_TIPO_DOC: TWideStringField;
    QrEndereco2ENumero: TIntegerField;
    QrEndereco2PNumero: TIntegerField;
    QrEndereco2NUMERO: TIntegerField;
    QrEndereco2ELograd: TSmallintField;
    QrEndereco2PLograd: TSmallintField;
    QrEndereco2LOGRAD: TSmallintField;
    QrEndereco2ECEP: TIntegerField;
    QrEndereco2PCEP: TIntegerField;
    QrEndereco2CEP: TIntegerField;
    QrEndereco2SITE: TWideStringField;
    QrEmpresas: TMySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasFilial: TIntegerField;
    QrEmpresasNOMEFILIAL: TWideStringField;
    QrEmpresasCNPJ_CPF: TWideStringField;
    QrEmpresasIE: TWideStringField;
    QrEmpresasNIRE: TWideStringField;
    DsEmpresas: TDataSource;
    QrDuplCodUsu: TMySQLQuery;
    QrDuplCodUsuNome: TWideStringField;
    QrUsuCot: TMySQLQuery;
    QrTemCot: TMySQLQuery;
    QrEntrega: TMySQLQuery;
    QrEntregaL_Ativo: TSmallintField;
    QrEntregaCodigo: TIntegerField;
    QrEntregaCadastro: TDateField;
    QrEntregaENatal: TDateField;
    QrEntregaPNatal: TDateField;
    QrEntregaTipo: TSmallintField;
    QrEntregaRespons1: TWideStringField;
    QrEntregaNOME_ENT: TWideStringField;
    QrEntregaCNPJ_CPF: TWideStringField;
    QrEntregaIE_RG: TWideStringField;
    QrEntregaNIRE_: TWideStringField;
    QrEntregaRUA: TWideStringField;
    QrEntregaNUMERO: TIntegerField;
    QrEntregaCOMPL: TWideStringField;
    QrEntregaBAIRRO: TWideStringField;
    QrEntregaCIDADE: TWideStringField;
    QrEntregaNOMELOGRAD: TWideStringField;
    QrEntregaNOMEUF: TWideStringField;
    QrEntregaPais: TWideStringField;
    QrEntregaLograd: TSmallintField;
    QrEntregaTE1: TWideStringField;
    QrEntregaCEP: TIntegerField;
    QrEntregaFAX: TWideStringField;
    QrEntregaCEP_TXT: TWideStringField;
    QrEntregaNUMERO_TXT: TWideStringField;
    QrEntregaE_ALL: TWideStringField;
    QrEntregaCNPJ_TXT: TWideStringField;
    QrEntregaFAX_TXT: TWideStringField;
    QrEntregaTE1_TXT: TWideStringField;
    QrEntregaNATAL_TXT: TWideStringField;
    QrEntAtrIts: TMySQLQuery;
    QrEntAtrItsNOMEATRIBCAD: TWideStringField;
    QrEntAtrItscontrole: TIntegerField;
    QrEntAtrItsCodigo: TIntegerField;
    QrEntAtrItsCodUsu: TIntegerField;
    QrEntAtrItsNOMEATRIBITS: TWideStringField;
    QrEntAtrDef: TMySQLQuery;
    QrEntAtrDefcontrole: TIntegerField;
    QrNexInt: TMySQLQuery;
    QrNexIntCodigo: TIntegerField;
    QrAgora: TMySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrGetUFX: TMySQLQuery;
    QrGetUFXNome: TWideStringField;
    QrAux: TMySQLQuery;
    QrFiliLog: TMySQLQuery;
    QrFiliLogCodigo: TIntegerField;
    QrFiliLogFilial: TIntegerField;
    QrFiliLogNomeEmp: TWideStringField;
    QrParamsEmp: TMySQLQuery;
    QrParamsEmpCNPJ_CPF: TWideStringField;
    QrParamsEmpCodigo: TIntegerField;
    QrParamsEmpMoeda: TIntegerField;
    QrParamsEmpSituacao: TIntegerField;
    QrParamsEmpRegrFiscal: TIntegerField;
    QrParamsEmpFretePor: TSmallintField;
    QrParamsEmpTipMediaDD: TSmallintField;
    QrParamsEmpFatSemPrcL: TSmallintField;
    QrParamsEmpBalQtdItem: TFloatField;
    QrParamsEmpAssociada: TIntegerField;
    QrParamsEmpFatSemEstq: TSmallintField;
    QrParamsEmpAssocModNF: TIntegerField;
    QrParamsEmpCtaProdVen: TIntegerField;
    QrParamsEmpfaturasep: TWideStringField;
    QrParamsEmpfaturaseq: TSmallintField;
    QrParamsEmpFaturaDta: TSmallintField;
    QrParamsEmpTxtProdVen: TWideStringField;
    QrParamsEmpLogo3x1: TWideStringField;
    QrParamsEmpTipCalcJuro: TSmallintField;
    QrParamsEmpSimplesFed: TSmallintField;
    QrParamsEmpSimplesEst: TSmallintField;
    QrParamsEmpCerDigital: TWideMemoField;
    QrParamsEmpDirNFeGer: TWideStringField;
    QrParamsEmpDirNFeAss: TWideStringField;
    QrParamsEmpDirEnvLot: TWideStringField;
    QrParamsEmpDirRec: TWideStringField;
    QrParamsEmpDirPedRec: TWideStringField;
    QrParamsEmpDirProRec: TWideStringField;
    QrParamsEmpDirDen: TWideStringField;
    QrParamsEmpDirPedCan: TWideStringField;
    QrParamsEmpDirCan: TWideStringField;
    QrParamsEmpDirPedInu: TWideStringField;
    QrParamsEmpDirInu: TWideStringField;
    QrParamsEmpDirPedSit: TWideStringField;
    QrParamsEmpDirSit: TWideStringField;
    QrParamsEmpDirPedSta: TWideStringField;
    QrParamsEmpDirSta: TWideStringField;
    QrParamsEmpUF_WebServ: TWideStringField;
    QrParamsEmpSiglaCustm: TWideStringField;
    QrParamsEmpInfoPerCuz: TSmallintField;
    QrParamsEmpPathLogoNF: TWideStringField;
    QrParamsEmpCtaServico: TIntegerField;
    QrParamsEmpTxtServico: TWideStringField;
    QrParamsEmpDupServico: TWideStringField;
    QrParamsEmpDupProdVen: TWideStringField;
    QrParamsEmpPedVdaMudLista: TSmallintField;
    QrParamsEmpPedVdaMudPrazo: TSmallintField;
    QrParamsEmpNFeSerNum: TWideStringField;
    QrParamsEmpUF_DistDFeInt: TWideStringField;
    QrParamsEmpUF_Servico: TWideStringField;
    QrParamsEmpFisFax: TWideStringField;
    QrParamsEmpFisContNom: TWideStringField;
    QrParamsEmpFisContTel: TWideStringField;
    QrParamsEmpReg10: TSmallintField;
    QrParamsEmpReg11: TSmallintField;
    QrParamsEmpReg50: TSmallintField;
    QrParamsEmpReg51: TSmallintField;
    QrParamsEmpReg53: TSmallintField;
    QrParamsEmpReg54: TSmallintField;
    QrParamsEmpReg55: TSmallintField;
    QrParamsEmpReg56: TSmallintField;
    QrParamsEmpReg57: TSmallintField;
    QrParamsEmpReg60M: TSmallintField;
    QrParamsEmpReg60A: TSmallintField;
    QrParamsEmpReg60D: TSmallintField;
    QrParamsEmpReg60I: TSmallintField;
    QrParamsEmpReg60R: TSmallintField;
    QrParamsEmpReg61: TSmallintField;
    QrParamsEmpReg61R: TSmallintField;
    QrParamsEmpReg70: TSmallintField;
    QrParamsEmpReg71: TSmallintField;
    QrParamsEmpReg74: TSmallintField;
    QrParamsEmpReg75: TSmallintField;
    QrParamsEmpReg76: TSmallintField;
    QrParamsEmpReg77: TSmallintField;
    QrParamsEmpReg85: TSmallintField;
    QrParamsEmpReg86: TSmallintField;
    QrParamsEmpReg90: TSmallintField;
    QrParamsEmpSINTEGRA_Path: TWideStringField;
    QrParamsEmpDirDANFEs: TWideStringField;
    QrParamsEmpDirNFeProt: TWideStringField;
    QrParamsEmpPreMailAut: TIntegerField;
    QrParamsEmpPreMailCan: TIntegerField;
    QrParamsEmpversao: TFloatField;
    QrParamsEmpFaturaNum: TSmallintField;
    QrParamsEmpNFeSerVal: TDateField;
    QrParamsEmpNFeSerAvi: TSmallintField;
    QrParamsEmpide_mod: TSmallintField;
    QrParamsEmpide_tpImp: TSmallintField;
    QrParamsEmpide_tpAmb: TSmallintField;
    QrParamsEmpAppCode: TSmallintField;
    QrParamsEmpAssDigMode: TSmallintField;
    QrParamsEmpEntiTipCto: TIntegerField;
    QrParamsEmpMyEmailNFe: TWideStringField;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmpDirSchema: TWideStringField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmpUsaReferen: TSmallintField;
    QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField;
    QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField;
    QrParamsEmpSPED_EFD_CadContador: TIntegerField;
    QrParamsEmpSPED_EFD_CRCContador: TWideStringField;
    QrParamsEmpSPED_EFD_EscriContab: TIntegerField;
    QrParamsEmpSPED_EFD_EnderContab: TSmallintField;
    QrParamsEmpSPED_EFD_Path: TWideStringField;
    QrParamsEmpNFeItsLin: TSmallintField;
    QrParamsEmpNFeFTRazao: TSmallintField;
    QrParamsEmpNFeFTEnder: TSmallintField;
    QrParamsEmpNFeFTFones: TSmallintField;
    QrParamsEmpNFeMaiusc: TSmallintField;
    QrParamsEmpNFeShowURL: TWideStringField;
    QrParamsEmpNFetpEmis: TSmallintField;
    QrParamsEmpSCAN_Ser: TIntegerField;
    QrParamsEmpSCAN_nNF: TIntegerField;
    QrParamsEmpDirNFeRWeb: TWideStringField;
    QrParamsEmpNFSeMetodo: TIntegerField;
    QrParamsEmpNFSeVersao: TFloatField;
    QrParamsEmpNFSeSerieRps: TWideStringField;
    QrParamsEmpDpsNumero: TIntegerField;
    QrParamsEmpNFSeWSProducao: TWideStringField;
    QrParamsEmpNFSeWSHomologa: TWideStringField;
    QrParamsEmpNFSeAmbiente: TSmallintField;
    QrParamsEmpNFSeTipCtoMail: TIntegerField;
    QrParamsEmpPreMailEveCCe: TIntegerField;
    QrParamsEmpEntiTipCt1: TIntegerField;
    QrParamsEmpDirEveEnvLot: TWideStringField;
    QrParamsEmpDirEveRetLot: TWideStringField;
    QrParamsEmpDirEvePedCCe: TWideStringField;
    QrParamsEmpDirEveRetCCe: TWideStringField;
    QrParamsEmpDirEveProcCCe: TWideStringField;
    QrParamsEmpDirEvePedCan: TWideStringField;
    QrParamsEmpDirEveRetCan: TWideStringField;
    QrParamsEmpEstq0UsoCons: TSmallintField;
    QrParamsEmpDirNFSeDPSGer: TWideStringField;
    QrParamsEmpDirNFSeDPSAss: TWideStringField;
    QrParamsEmpNFSeCertDigital: TWideStringField;
    QrParamsEmpNFSeCertValidad: TDateField;
    QrParamsEmpNFSeCertAviExpi: TSmallintField;
    QrParamsEmpNFeVerStaSer: TFloatField;
    QrParamsEmpNFeVerEnvLot: TFloatField;
    QrParamsEmpNFeVerConLot: TFloatField;
    QrParamsEmpNFeVerCanNFe: TFloatField;
    QrParamsEmpNFeVerInuNum: TFloatField;
    QrParamsEmpNFeVerConNFe: TFloatField;
    QrParamsEmpNFeVerLotEve: TFloatField;
    QrParamsEmpNFeVerConDes: TFloatField;
    QrParamsEmpNFeVerConsCad: TFloatField;
    QrParamsEmpNFeVerDistDFeInt: TFloatField;
    QrParamsEmpNFeVerDowNFe: TFloatField;
    QrParamsEmpUF_MDeMDe: TWideStringField;
    QrParamsEmpUF_MDeDes: TWideStringField;
    QrParamsEmpUF_MDeNFe: TWideStringField;
    QrParamsEmpNFeNT2013_003LTT: TSmallintField;
    QrParamsEmpPediVdaNElertas: TIntegerField;
    QrParamsEmpTZD_UTC: TFloatField;
    QrParamsEmpTZD_UTC_Str: TWideStringField;
    QrParamsEmpNFe_indFinalCpl: TSmallintField;
    QrParamsEmpNFSe_indFinalCpl: TSmallintField;
    QrParamsEmpCartEmisHonFun: TIntegerField;
    QrParamsEmpCtaServicoPg: TIntegerField;
    QrParamsEmpCtaProdCom: TIntegerField;
    QrParamsEmpCTeversao: TFloatField;
    QrParamsEmpNFSeCodMunici: TIntegerField;
    QrParamsEmpNoDANFEMail: TSmallintField;
    QrParamsEmpSPED_EFD_Peri_E100: TSmallintField;
    QrParamsEmpSPED_EFD_Peri_E500: TSmallintField;
    QrParamsEmpSPED_EFD_Peri_K100: TSmallintField;
    QrParamsEmpRetImpost: TSmallintField;
    QrParamsEmpFreteRpICMS: TFloatField;
    QrParamsEmpFreteRpPIS: TFloatField;
    QrParamsEmpFreteRpCOFINS: TFloatField;
    QrParamsEmpNfse_WhatsApp_Msg: TWideMemoField;
    QrParamsEmpNfse_WhatsApp_EntiTipCto: TIntegerField;
    QrParamsEmpSPED_EFD_ICMS_IPI_VersaoGuia: TWideStringField;
    QrParamsEmpinfRespTec_Usa: TSmallintField;
    QrParamsEmpinfRespTec_CNPJ: TWideStringField;
    QrParamsEmpinfRespTec_xContato: TWideStringField;
    QrParamsEmpinfRespTec_email: TWideStringField;
    QrParamsEmpinfRespTec_fone: TWideStringField;
    QrParamsEmpinfRespTec_idCSRT: TWideStringField;
    QrParamsEmpinfRespTec_CSRT: TWideStringField;
    QrParamsEmpSPED_EFD_MovSubPrd: TSmallintField;
    QrParamsEmpNT2018_05v120: TSmallintField;
    QrThread: TMySQLQuery;
    QrThreadID: TLargeintField;
    MyPID_DB: TMySQLDatabase;
    QrUpdPID1: TMySQLQuery;
    QrSenhas: TMySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSenhasIP_Default: TWideStringField;
    QrSenhasPerfil_TXT: TWideStringField;
    MyLocDB: TMySQLDatabase;
    QrUpdL: TMySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCad: TMySQLQuery;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrAux_: TMySQLQuery;
    DsFiliLog: TDataSource;
    QrFiliaisSP: TMySQLQuery;
    QrFiliaisSPEntidade: TIntegerField;
    QrFiliaisSPEmpresa: TIntegerField;
    QrCambioMda: TMySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DsCambioMda: TDataSource;
    QrSituacao: TABSQuery;
    QrSituacaoCodigo: TIntegerField;
    QrSituacaoNome: TWideStringField;
    DsSituacao: TDataSource;
    QrCliIntLog: TMySQLQuery;
    QrCliIntLogCodEnti: TIntegerField;
    DsCliIntLog: TDataSource;
    QrCliIntUni: TMySQLQuery;
    QrCliIntUniCodigo: TIntegerField;
    DsCliIntUni: TDataSource;
    QrLocCI: TMySQLQuery;
    QrLocCICodigo: TIntegerField;
    QrLocCIPercJuros: TFloatField;
    QrLocCIPercMulta: TFloatField;
    QrLocCIVTCBBNITAR: TFloatField;
    QrUpdPID2: TMySQLQuery;
    QrLocLogr: TMySQLQuery;
    QrLocLogrCodigo: TIntegerField;
    QrPEM_T: TMySQLQuery;
    QrPEM_TTexto: TWideMemoField;
    QrPEM_TDescricao: TWideStringField;
    QrPEM_TControle: TIntegerField;
    QrPEM_TOrdem: TIntegerField;
    QrPEM_TTipo: TIntegerField;
    QrPEM_TCidID_Img: TWideStringField;
    QrPEM_2: TMySQLQuery;
    QrPEM_2Texto: TWideMemoField;
    QrPEM_2Descricao: TWideStringField;
    QrPEM_2Controle: TIntegerField;
    QrPEM_2CidID_Img: TWideStringField;
    QrPEM_1: TMySQLQuery;
    QrPEM_1Texto: TWideMemoField;
    QrPEM_1Descricao: TWideStringField;
    QrPEM_0: TMySQLQuery;
    QrPEM_0Texto: TWideMemoField;
    QrPEM_0Descricao: TWideStringField;
    DsPreEmail: TDataSource;
    QrPreEmail: TMySQLQuery;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    QrPreEmailSMTPServer: TWideStringField;
    QrPreEmailSend_Name: TWideStringField;
    QrPreEmailSend_Mail: TWideStringField;
    QrPreEmailPass_Mail: TWideStringField;
    QrPreEmailLogi_Name: TWideStringField;
    QrPreEmailLogi_Pass: TWideStringField;
    QrPreEmailLogi_Auth: TSmallintField;
    QrPreEmailMail_Titu: TWideStringField;
    QrPreEmailSaudacao: TWideStringField;
    QrPreEmailNaoEnvBloq: TSmallintField;
    QrPreEmailPorta_mail: TIntegerField;
    QrPreEmailLogi_SSL: TSmallintField;
    QrPreEmailEmailConta: TIntegerField;
    QrPreEmMsg: TMySQLQuery;
    QrPreEmMsgNOMETIPO: TWideStringField;
    QrPreEmMsgCodigo: TIntegerField;
    QrPreEmMsgControle: TIntegerField;
    QrPreEmMsgOrdem: TIntegerField;
    QrPreEmMsgTipo: TIntegerField;
    QrPreEmMsgTexto: TWideMemoField;
    QrPreEmMsgLk: TIntegerField;
    QrPreEmMsgDataCad: TDateField;
    QrPreEmMsgDataAlt: TDateField;
    QrPreEmMsgUserCad: TIntegerField;
    QrPreEmMsgUserAlt: TIntegerField;
    QrPreEmMsgAlterWeb: TSmallintField;
    QrPreEmMsgAtivo: TSmallintField;
    QrPreEmMsgDescricao: TWideStringField;
    DsPreEmMsg: TDataSource;
    QrLancto: TMySQLQuery;
    QrLanctoDebito: TFloatField;
    QrLanctoCredito: TFloatField;
    QrLanctoFornecedor: TIntegerField;
    QrLanctoCliente: TIntegerField;
    QrLanctoNOMECLIENTE: TWideStringField;
    QrLanctoNOMEFORNECEDOR: TWideStringField;
    QrLanctoData: TDateField;
    QrLanctoVencimento: TDateField;
    QrLanctoBanco1: TIntegerField;
    QrPPI: TMySQLQuery;
    QrPPIDataE: TDateField;
    QrPPINO_RETORNA: TWideStringField;
    QrPPINO_Motivo: TWideStringField;
    QrPPILimiteSai: TDateField;
    QrPPISaiu: TIntegerField;
    QrPPIDataSai: TDateTimeField;
    QrPPILimiteRem: TDateField;
    QrPPIRecebeu: TIntegerField;
    QrPPIDataRec: TDateTimeField;
    QrPPILimiteRet: TDateField;
    QrPPIRetornou: TIntegerField;
    QrPPIDataRet: TDateTimeField;
    QrPPIDataSai_TXT: TWideStringField;
    QrPPIDataRec_TXT: TWideStringField;
    QrPPIDataRet_TXT: TWideStringField;
    QrPPIConta: TIntegerField;
    QrPPIRetorna: TSmallintField;
    QrPPITexto: TWideStringField;
    QrPPILancto: TIntegerField;
    QrPPIDocum: TFloatField;
    QrPTK: TMySQLQuery;
    QrPTKNome: TWideStringField;
    QrPTKLote: TIntegerField;
    QrPTKMez: TIntegerField;
    QrPTKMes_TXT: TWideStringField;
    DsPTK: TDataSource;
    DsPPI: TDataSource;
    QrMaius: TMySQLQuery;
    QrMaiusCodigo: TIntegerField;
    QrMaiusRazaoSocial: TWideStringField;
    QrMaiusNome: TWideStringField;
    QrPKA: TMySQLQuery;
    QrPKANome: TWideStringField;
    QrPKALote: TIntegerField;
    QrPKAMez: TIntegerField;
    QrPKADataE: TDateField;
    QrPKAConta: TIntegerField;
    QrPKARetorna: TSmallintField;
    QrPKANO_RETORNA: TWideStringField;
    QrPKANO_Motivo: TWideStringField;
    QrPKALimiteSai: TDateField;
    QrPKASaiu: TIntegerField;
    QrPKADataSai: TDateTimeField;
    QrPKALimiteRem: TDateField;
    QrPKARecebeu: TIntegerField;
    QrPKADataRec: TDateTimeField;
    QrPKALimiteRet: TDateField;
    QrPKARetornou: TIntegerField;
    QrPKADataRet: TDateTimeField;
    QrPKADataSai_TXT: TWideStringField;
    QrPKADataRec_TXT: TWideStringField;
    QrPKADataRet_TXT: TWideStringField;
    DsPKA: TDataSource;
    QrUEFE: TMySQLQuery;
    QrUEFEData: TDateField;
    QrMeses: TMySQLQuery;
    QrMesesIni: TFloatField;
    QrMesesMeio: TFloatField;
    QrMesesFim: TFloatField;
    QrSMIN: TMySQLQuery;
    QrSMINQtde: TFloatField;
    QrSMINCustoAll: TFloatField;
    QrSMIX: TMySQLQuery;
    QrSMIXGraGruX: TIntegerField;
    QrSMIXQTDE: TFloatField;
    QrSMIXGraGru1: TIntegerField;
    QrX: TMySQLQuery;
    QrXGraGruX: TIntegerField;
    QrGraGruVal: TMySQLQuery;
    QrGraGruValControle: TIntegerField;
    QrGraGruX: TMySQLQuery;
    QrGraGruXNivel1: TIntegerField;
    QrGraGruXNO_PRD: TWideStringField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXNO_PGT: TWideStringField;
    QrGraGruXSIGLA: TWideStringField;
    QrGraGruXNO_TAM: TWideStringField;
    QrGraGruXNO_COR: TWideStringField;
    QrGraGruXGraCorCad: TIntegerField;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    QrGraGruXNivel2: TIntegerField;
    QrGraGruXNivel3: TIntegerField;
    QrGraGruXNivel4: TIntegerField;
    QrGraGruXNivel5: TIntegerField;
    QrLmz: TMySQLQuery;
    QrLmzMez: TIntegerField;
    QrLmzControle: TIntegerField;
    QrLmzID_Pgto: TIntegerField;
    QrLmzData: TDateField;
    QrUmz: TMySQLQuery;
    QrUmzMez: TIntegerField;
    PMEnti: TPopupMenu;
    ModeloA1: TMenuItem;
    ModeloB1: TMenuItem;
    QrCerrado: TMySQLQuery;
    QrCerradoData: TDateField;
    QrProtoMail: TMySQLQuery;
    QrProtoMailDepto_Cod: TIntegerField;
    QrProtoMailDepto_Txt: TWideStringField;
    QrProtoMailEntid_Cod: TIntegerField;
    QrProtoMailEntid_Txt: TWideStringField;
    QrProtoMailTaref_Cod: TIntegerField;
    QrProtoMailTaref_Txt: TWideStringField;
    QrProtoMailDeliv_Cod: TIntegerField;
    QrProtoMailDeliv_Txt: TWideStringField;
    QrProtoMailDataE: TDateField;
    QrProtoMailDataE_Txt: TWideStringField;
    QrProtoMailDataD: TDateField;
    QrProtoMailDataD_Txt: TWideStringField;
    QrProtoMailProtoLote: TIntegerField;
    QrProtoMailNivelEmail: TSmallintField;
    QrProtoMailItem: TIntegerField;
    QrProtoMailNivelDescr: TWideStringField;
    QrProtoMailProtocolo: TIntegerField;
    QrProtoMailRecipEmeio: TWideStringField;
    QrProtoMailRecipNome: TWideStringField;
    QrProtoMailRecipProno: TWideStringField;
    QrProtoMailRecipItem: TIntegerField;
    QrProtoMailBloqueto: TFloatField;
    QrProtoMailVencimento: TDateField;
    QrProtoMailValor: TFloatField;
    QrProtoMailPreEmeio: TIntegerField;
    QrProtoMailCondominio: TWideStringField;
    QrProtoMailIDEmeio: TIntegerField;
    QrEntiMail: TMySQLQuery;
    QrEntiMailEMail: TWideStringField;
    QrAuxPID1: TMySQLQuery;
    DBDmk: TMySQLDatabase;
    QrCtrlGeral: TMySQLQuery;
    QrCtrlGeralDmkNetPath: TWideStringField;
    QrCtrlGeralDuplicLct: TSmallintField;
    QrCtrlGeralVctAutDefU: TSmallintField;
    QrCtrlGeralVctAutLast: TDateTimeField;
    QrCtrlGeralAbaIniApp: TIntegerField;
    QrCtrlGeralMyPathsFrx: TSmallintField;
    QrCtrlGeralFixBugsApp: TLargeintField;
    QrCtrlGeralFixBugsGrl: TLargeintField;
    QrCliAplic: TMySQLQuery;
    QrCliAplicControle: TIntegerField;
    QrCliAplicFimChek: TWideStringField;
    QrCliAplicFimData: TDateField;
    QrEntDmk: TMySQLQuery;
    QrEntDmkCodigo: TIntegerField;
    QrEntDmkNO_ENT: TWideStringField;
    QrEntDmkCPFJ: TWideStringField;
    QrEntDmkLicTel: TWideStringField;
    QrEntDmkLicMail: TWideStringField;
    QrCliAplicLi: TMySQLQuery;
    QrCliAplicLiConta: TAutoIncField;
    QrCliAplicLiNomePC: TWideStringField;
    QrCliAplicLiDataCad: TDateField;
    QrCliAplicLiDataAlt: TDateField;
    QrCliAplicLiUserCad: TIntegerField;
    QrCliAplicLiUserAlt: TIntegerField;
    QrCliAplicLiSerialKey: TWideStringField;
    QrCliAplicLiSerialNum: TWideStringField;
    QrCliAplicLiCadastro: TDateTimeField;
    QrCliAplicLiConsultaDH: TDateTimeField;
    QrCliAplicLiConsultaNV: TIntegerField;
    QrTermiServ: TMySQLQuery;
    QrTermiServSerialKey: TWideStringField;
    QrTermiServSerialNum: TWideStringField;
    QrTerminal: TMySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrTerminalLicenca: TWideStringField;
    QrTerminalSerialKey: TWideStringField;
    QrTerminalSerialNum: TWideStringField;
    QrTerminalServidor: TSmallintField;
    QrTerminalNomePC: TWideStringField;
    QrTerminalDescriPC: TWideStringField;
    QrTerminalVersaoMin: TWideStringField;
    QrTerminalVersaoMax: TWideStringField;
    QrWAgora: TMySQLQuery;
    QrWAgoraANO: TLargeintField;
    QrWAgoraMES: TLargeintField;
    QrWAgoraDIA: TLargeintField;
    QrWAgoraHORA: TLargeintField;
    QrWAgoraMINUTO: TLargeintField;
    QrWAgoraSEGUNDO: TLargeintField;
    QrWAgoraAGORA: TDateTimeField;
    QrDefEnti: TMySQLQuery;
    QrDefEntiCodigo: TIntegerField;
    QrDefEntiFilial: TIntegerField;
    QrDefEntiCliInt: TIntegerField;
    QrDefEntiNO_ENT: TWideStringField;
    DsLastMorto: TDataSource;
    QrLastMorto: TMySQLQuery;
    QrLastMortoData: TDateField;
    QrLastMortoSaldoIni: TFloatField;
    QrLastMortoCreditos: TFloatField;
    QrLastMortoDebitos: TFloatField;
    QrLastMortoSaldoFim: TFloatField;
    QrLastMortoTabLct: TWideStringField;
    QrLastMortoAM: TWideStringField;
    DsLastEncer: TDataSource;
    QrLastEncer: TMySQLQuery;
    QrLastEncerData: TDateField;
    QrLastEncerSaldoIni: TFloatField;
    QrLastEncerCreditos: TFloatField;
    QrLastEncerDebitos: TFloatField;
    QrLastEncerSaldoFim: TFloatField;
    QrLastEncerTabLct: TWideStringField;
    QrLastEncerAM: TWideStringField;
    QrParOrfRep: TMySQLQuery;
    QrParOrfRepData: TDateField;
    QrParOrfRepCarteira: TIntegerField;
    QrParOrfRepControle: TIntegerField;
    QrParOrfRepDescricao: TWideStringField;
    QrParOrfRepCredito: TFloatField;
    QrParOrfRepVencimento: TDateField;
    QrParOrfRepCompensado: TDateField;
    QrParOrfRepFatNum: TFloatField;
    QrParOrfRepFatParcela: TIntegerField;
    QrParOrfRepMez: TIntegerField;
    QrParOrfRepCliente: TIntegerField;
    QrParOrfRepCliInt: TIntegerField;
    QrParOrfRepForneceI: TIntegerField;
    QrParOrfRepDepto: TIntegerField;
    QrParOrfRepAtrelado: TIntegerField;
    DsParOrfRep: TDataSource;
    QrCheque: TMySQLQuery;
    QrChequeBanco1: TIntegerField;
    QrChequeDebito: TFloatField;
    QrChequeCredito: TFloatField;
    QrChequeFornecedor: TIntegerField;
    QrChequeCliente: TIntegerField;
    QrChequeNOMECLIENTE: TWideStringField;
    QrChequeNOMEFORNECEDOR: TWideStringField;
    QrChequeData: TDateField;
    QrChequeVencimento: TDateField;
    QrFavoritos3: TMySQLQuery;
    QrFavoritos3Nivel3: TIntegerField;
    QrFavoritos3Titulo: TWideStringField;
    QrFavoritos2: TMySQLQuery;
    QrFavoritos2Nivel2: TIntegerField;
    QrFavoritos2Titulo: TWideStringField;
    QrFavoritos1: TMySQLQuery;
    QrFavoritos1Descri1: TWideStringField;
    QrFavoritos1Descri2: TWideStringField;
    QrFavoritos1Descri3: TWideStringField;
    QrFavoritos1Nome: TWideStringField;
    QrFavoritos1Classe: TWideStringField;
    Tb_Empresas: TMySQLTable;
    Tb_EmpresasCodCliInt: TIntegerField;
    Tb_EmpresasCodEnti: TIntegerField;
    Tb_EmpresasCodFilial: TIntegerField;
    Tb_EmpresasNome: TWideStringField;
    Tb_EmpresasAtivo: TSmallintField;
    Ds_Empresas: TDataSource;
    Tb_Indi_Pags: TMySQLTable;
    Tb_Indi_PagsCodigo: TIntegerField;
    Tb_Indi_PagsNome: TWideStringField;
    Tb_Indi_PagsAtivo: TSmallintField;
    Ds_Indi_Pags: TDataSource;
    QrEnti: TMySQLQuery;
    QrEntiTipo: TSmallintField;
    QrEntiIE: TWideStringField;
    QrEntiCNPJ: TWideStringField;
    QrEntiCPF: TWideStringField;
    QrEntiUF_Cod: TFloatField;
    QrEntiNO_ENTI: TWideStringField;
    QrEntiCodMunici: TFloatField;
    QrEntiCodiPais: TFloatField;
    QrEntiCliInt: TMySQLQuery;
    QrControle: TMySQLQuery;
    QrUsTx: TMySQLQuery;
    QrUsTxTxtMeu: TWideStringField;
    QrPerfis: TMySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrSSit: TMySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrBoss: TMySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TMySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrUTC: TMySQLQuery;
    QrUTCDATA_LOC: TDateTimeField;
    QrUTCDATA_UTC: TDateTimeField;
    QrUTCUTC_Dif: TFloatField;
    QrOpcoesGerl: TMySQLQuery;
    AllID_DB: TMySQLDatabase;
    QrAllUpd: TMySQLQuery;
    QrAllAux: TMySQLQuery;
    QrEntNFS: TMySQLQuery;
    QrEntNFSTipo: TSmallintField;
    QrEntNFSIE: TWideStringField;
    QrEntNFSCNPJ: TWideStringField;
    QrEntNFSCPF: TWideStringField;
    QrEntNFSNO_ENT: TWideStringField;
    QrEntNFSRua: TWideStringField;
    QrEntNFSNumero: TFloatField;
    QrEntNFSCompl: TWideStringField;
    QrEntNFSBairro: TWideStringField;
    QrEntNFSUF_Cod: TFloatField;
    QrEntNFSCodiPais: TFloatField;
    QrEntNFSNIRE: TWideStringField;
    QrEntNFSNO_Lograd: TWideStringField;
    QrEntNFSTE1: TWideStringField;
    QrEntNFSLograd: TFloatField;
    QrEntNFSCEP: TFloatField;
    QrEntNFSCodMunici: TFloatField;
    QrLocFilial: TMySQLQuery;
    QrLocFilialCodEnti: TIntegerField;
    QrLocFilialCodFilial: TIntegerField;
    QrCidade: TMySQLQuery;
    QrCidadeNome: TWideStringField;
    QrSelCods: TMySQLQuery;
    QrSelCodsNivel1: TIntegerField;
    QrSelCodsNivel2: TIntegerField;
    QrSelCodsNivel3: TIntegerField;
    QrSelCodsNivel4: TIntegerField;
    QrSelCodsNivel5: TIntegerField;
    QrSelCodsNome: TWideStringField;
    QrSelCodsAtivo: TSmallintField;
    DsSelCods: TDataSource;
    QrFiliaisOF: TMySQLQuery;
    QrFiliaisOFEntidade: TIntegerField;
    QrFiliaisOFEmpresa: TIntegerField;
    QrCartToPrn: TMySQLQuery;
    QrCartToPrnITENS: TLargeintField;
    QrProvisorio1: TMySQLQuery;
    QrProvisorio1NULO: TIntegerField;
    QrProvisorio1Codigo: TIntegerField;
    QrProvisorio1Controle: TIntegerField;
    PMSaveCfgMenu: TPopupMenu;
    SalvarOrdenaodascolunasdagrade2: TMenuItem;
    CarregarOrdenaopadrodascolunasdagrade1: TMenuItem;
    N1: TMenuItem;
    esteXLS1: TMenuItem;
    dmkVarCfgMenu: TdmkVariable;
    QrMD5: TMySQLQuery;
    QrMD5Texto: TWideStringField;
    QrNomes: TMySQLQuery;
    QrNomesTEXTO: TWideStringField;
    frxDsEndereco2: TfrxDBDataset;
    QrSB5: TMySQLQuery;
    QrSB5Codigo: TLargeintField;
    QrSB5CodUsu: TLargeintField;
    QrSB5Nome: TWideStringField;
    DsSB5: TDataSource;
    QrCambios: TMySQLQuery;
    QrCambiosCodigo: TIntegerField;
    QrCambiosValor: TFloatField;
    QrCambiosDataC: TDateField;
    DsCambios: TDataSource;
    QrWebParams: TMySQLQuery;
    MySyncDB: TMySQLDatabase;
    QrUpdSync: TMySQLQuery;
    MyPID_CompressDB: TMySQLDatabase;
    MyCompressDB: TMySQLDatabase;
    QrDono: TMySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoTipo: TSmallintField;
    QrDonoTE1: TWideStringField;
    QrDonoTE2: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoTE2_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrDonoCEP: TIntegerField;
    QrDonoECEP: TIntegerField;
    QrDonoPCEP: TIntegerField;
    QrDonoEUF: TSmallintField;
    QrDonoPUF: TSmallintField;
    QrDonoUF: TSmallintField;
    QrDonoFONES: TWideStringField;
    QrDonoCODMUNICI: TFloatField;
    procedure QrSCECalcFields(DataSet: TDataSet);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrEndereco2CalcFields(DataSet: TDataSet);
    procedure QrEntregaCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrFiliLogAfterOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrCliIntLogAfterOpen(DataSet: TDataSet);
    procedure QrCliIntUniAfterOpen(DataSet: TDataSet);
    procedure QrPTKBeforeClose(DataSet: TDataSet);
    procedure QrPTKAfterScroll(DataSet: TDataSet);
    procedure QrPTKCalcFields(DataSet: TDataSet);
    procedure QrPPICalcFields(DataSet: TDataSet);
    procedure QrPKACalcFields(DataSet: TDataSet);
    procedure QrDonoCalcFields(DataSet: TDataSet);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure SalvarOrdenaodascolunasdagrade2Click(Sender: TObject);
    procedure CarregarOrdenaopadrodascolunasdagrade1Click(Sender: TObject);
    procedure dmkVarCfgMenuChange(Sender: TObject);
    procedure esteXLS1Click(Sender: TObject);
    procedure QrParamsEmpCalcFields(DataSet: TDataSet);
    procedure QrCtrlGeralAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    {$IfNDef SemEntidade}FEntiToCad: Integer;{$EndIf}
    // para novo LCT
    FCliInt, FEntidade, FFilial: Integer;
    FEntNome: String;
    // fim para novo LCT
    procedure VerificaVarsToConsts();
    procedure AtualizaCambioMda();
    procedure ConfiguraMaxTimeIdle();
  public
    { Public declarations }
    FCriouDB: Boolean;
    FLaAviso: TLabel;
    FPTK_Def_Client, FPTK_ID_Cod1, FPTK_ID_Cod2, FPTK_ID_Cod3, FPTK_ID_Cod4, FPKA_Next: Integer;
    // Mudei 2010-10-27
    //FPTK_Abertos: Boolean;
    FFiltroPPI: Integer;
    // fim 2010-10-27
    FSerialKey, FSerialNum: String;

    // P R O V I S � R I O
    procedure ConsertaCST_PIS_COFINS();
    procedure VerificaIEsIsentos();
    // FIM PROVIS�RIO


    //function BuscaProximoInteiro(Tabela, Campo, CampoTipo: String; Tipo: Integer): Integer;
    procedure ReopenSenhas();
    procedure HabilitaEdCBEmpresa(EdEmpresa: TdmkEditCB; CBEmpresa:
              TdmkDBLookupComboBox; SQLType: TSQLType; WinControl: TWinControl);
    function ObterDBVersao(): String;
    function BuscaProximoInteiro(Tabela, Campo: String; CampoTipo: String = '';
             Tipo: Integer = 0; Campo2: String = ''; Valr2: Integer = 0;
             Campo3: String = ''; Valr3: Integer = 0): Integer;
    function BuscaProximoInteiro_A(Tabela, Campo: String): Integer;
    function BuscaProximoInteiro_Negativo(DB: TmySQLDataBase;
             Tabela, Campo, CampoTipo: String; Tipo: Integer): Integer;
    function BuscaProximoCodigoInt(Tabela, Campo, ComplSQL: String;
             PreDefinido: Integer; Limite: Integer = High(Integer);
             Aviso: String = ''): Integer;
    function BuscaProximoCodigoInt_Novo(const Tabela, Campo, ComplSQL: String;
             const PreDefinido: Integer; const Aviso: String; var ProximoCodigoInt: Integer;
             const Limite: Integer = High(Integer)): Boolean;
    function BuscaProximoCodigoInt_Negativo(const Tabela, CampoIdx,
             ComplSQL: String; const PreDefinido: Integer; const Aviso: String;
             var ProximoCodigoInt: Integer; const Limite: Integer = -High(Integer)): Boolean;
    function ObtemAgora(UTC: Boolean = False): TDateTime;
    function ObtemDataHora(var Hoje: TDateTime; var Agora: TTime): Boolean;
    function ObtemNomeFantasiaEmpresa(): String;
    function ObtemNomeDepto(Depto: Integer): String;
    function ObtemEnderecoEtiqueta3Linhas(const Entidade: Integer;
             var Linha1, linha2, Linha3: String): String;
    function ObtemEnderecoEntrega3Linhas(const Entidade: Integer;
             var Linha1, linha2, Linha3: String): String;
    function ObtemUFX(UF: Integer): String;
    function ObtemNomeUFDeEntidade(Entidade: Integer): String;
    function ObtemTipoDeLogradouroERuaDeLogradouro(const xLogr: String;
             var Logr_I: Integer; var Rua: String): Boolean;
    function VerificaDuplicidadeCodUsu(Tabela, CampoNome: String;
             CamposKey: array of String; EdNome: TdmkEdit; KeyValues: array of Integer;
             ImgTipo: TdmkImage; Avisa: Boolean): Boolean;
    function ObtemMunicipioDeEntidade(Entidade: Integer): Integer;

    function ReopenEndereco(Entidade: Integer): Boolean;
    function ReopenEndereco2(Entidade: Integer): Boolean;
    procedure ReopenEndereco3(Query: TmySQLQuery; Entidade: Integer = 0);
    procedure ReopenEndereco4(Query: TmySQLQuery; Entidade: Integer);
    function ReopenEntrega(Entidade: Integer): Boolean;
    function MyThreadID(): Longint;
    function AllID_DB_Cria(): Boolean;
    function MyPID_DB_Cria(): Boolean;
    function MyPID_DB_Excl(Nome: String): Boolean;
    function MySyncDB_Cria(): Boolean;

    procedure ReopenStqCenCad(Codigo: Integer);
    procedure ReopenParamsEmp(Empresa: Integer);
    procedure ReopenDono();
    //procedure ReopenFilial(Filial: Integer);
    procedure ReopenEmpresas(Usuario, Empresa: Integer; EdEmpresa:
              TdmkEditCB = nil; CBEmpresa: TdmkDBLookupComboBox = nil;
              FiltroSQL: String = ''; Inverte: Boolean = False;
              QueryEmp: TmySQLQuery = nil);
    procedure ReopenQrEntAtrIts(SomenteSelecionados: Boolean;
              Entidade: Integer);
    procedure AtualizaCotacoes();
    //
    procedure ConectaBDLocalViaServidor();
    function SoUmaEmpresaLogada(Avisa: Boolean): Boolean;
    function EmpresaLogada(Empresa: Integer): Boolean;
    function SelecionarCliInt(var CliInt: Integer; const Avisar: Boolean): Boolean;
    //       ObtemCodigoDeCNPJ
    function DefineEntidade(const CNPJCPF: String; const AvisaErros: Boolean;
             var Entidade: Integer; var Nome: String): Boolean;
    function ObtemEntidadeDeCNPJCFP(DOC: String; var Entidade:Integer): Boolean;
    function ObtemEntidadeDeNO_ENT(Nome: String; var Entidade:Integer): Boolean;
    function ObtemEntidadeDeCOD_PART(COD_PART: String; var Entidade: Integer): Boolean;
    function ObtemEntidadeNomeDeCodigo(const Entidade: Integer; var NO_ENT: String): Boolean;
    function LocalizaClienteInterno(ClienteInterno: Integer; Aviso: String): Boolean;
    //
    function DefineFretePor(const PreDef: Integer; var FretePor, modFrete: Integer): Boolean;
    function ReopenPreEmail(PreEmail: Integer): Boolean;
    procedure ReopenPTK();
    procedure TransformaCadastrosDeEntidadesEmMaiusculas();
    function OBtemDataUltimoEncerramentoFinanceiro(FEntInt: Integer): TDateTime;
    function ObtemReduzidoDeNivel1(Nivel1: Integer): Integer;
    function ObtemAgoraTxt(): String;
    function MesesEntreDatas(DataIni, DataFim: TDateTime(*; DataInteira: Boolean*)): Double;
    function MediaMesesReparcelamento2(DataIni: TDateTime; Parcelas: Integer;
             var ArrDatas: Array of TDateTime): Double;
    function MediaMesesReparcelamento3(DataIni: TDateTime; Parcelas: Integer;
             var ArrDatas: Array of TDateTime): Double;
    procedure AtualizaPrecosGraGruVal2_All(PB: TProgressBar; Entidade: Integer);
    //procedure AtualizaPrecosGraGruVal2(GraGruX: Integer);
    function AtualizaPrecosGraGruVal2(GraGruX, Entidade: Integer): MyArrayD2;
    procedure CorrigeMesDeCompetenciaDeExtratos(PB: TProgressBar;
              TabLctA: String);
    //function VersaoNFe(Empresa: Integer): Integer;
    function VersaoNFSe(Empresa: Integer): Integer;
    //function SchemaNFe(Empresa: Integer): String;
    procedure DefineDataMinima(Entidade: Integer);
    function ObtemPrimeiroEMail_NFe(Empresa, Entidade: Integer): String;
    function ObtemPrimeiroEMail_NFSe(Empresa, Entidade: Integer): String;
    procedure CorrigeDuplicacaoDeLctControle(TabLctA: String; LaAviso: TLabel;
              PB1: TProgressBar);
    procedure ReopenTerminal(LiberaUso5: Boolean = False);
    procedure DefineServidorEIP();
    function  EhOServidor(): Boolean;
    // Novo LCT
    function  NomeTab(DBName: String; Tab: TNomeTab; SetaVAR_LCT: Boolean;
              Letra: TTipoTab = ttA; Empresa: Integer = 0): String;
    procedure All_ABD(DBName: String; var Entidade, CliInt: Integer;
              var DtEncer, DtMorto: TDateTime;
              var TabLctA, TabLctB, TabLctD: String);
    function  Def_EM_ABD(DBName: String; const Entidade, CliInt: Integer;
              var DtEncer, DtMorto: TDateTime;
              var TabLctA, TabLctB, TabLctD: String): Boolean;
    function  Def_EM_ABD_Limpa(var DtEncer, DtMorto: TDateTime;
              var TabLctA, TabLctB, TabLctD: String): Boolean;
    function  Def_EM_xID(const TabLctA, TabLctB, TabLctD, PreID: String;
              var TabID_A, TabID_B, TabID_D: String): Boolean;
    function  EmpresaAtual_ObtemNome(): String;
    function  EmpresaAtual_ObtemCodigo(TipoCod: TTipoEntiCod): Integer;
    function  EmpresaAtual_SetaCodigos(Codigo: Integer; TipoCod: TTipoEntiCod;
              MostraMsg: Boolean): Boolean;
    procedure ReopenDtEncerMorto(Entidade: Integer);
    function  MezLastEncerr(): Integer;
    // fim Novo LCT
    procedure ReopenPPI(Filtro: Integer);
    procedure MostraBackup3(MostraMsgPermis: Boolean = True);
    function  ListaDeItensDeBalancete(TipoCfgRel: TTipoCfgRel;
              AddNenhum: Boolean; Lista: TStrings): Boolean;
  {$IfDef cAdvToolx} //Berlin
    function  CriaFavoritos(Pager: TAdvToolBarPager; LaAviso0,
              LaAviso1: TLabel; Base: TAdvGlowButton; var Reference): Boolean; overload;
  {$Else}
    function  CriaFavoritos(PageControl: TdmkPageControl; LaAviso0,
              LaAviso1: TLabel; Base: TBitBtn; var Reference): Boolean; overload;
  {$EndIf} //Berlin
    procedure AdvGlowButtonClick(Sender: TObject);
    procedure ObtemCNPJ_CPF_IE_DeEntidade(const Entidade: Integer;
              var emit_CNPJ, emit_CPF, emit_IE: String);
    function  ObtemLctA_Para_Web(TabLctA: String): String;
    function  ReopenEntiCliInt(EntiCliInt: Integer): Boolean;
    function  ReopenControle(): Boolean;
    procedure ReopenMaster();
    //procedure ReopenMaster 2();
    function  ReCaptionTexto(Texto: String): String;
    procedure CorrigeEmpresaDeMenosUmParaMenosOnze(); // -11 (mudar de -1 para -11)
    procedure SelecionaEmpresaSeUnica(EdEmpresa: TdmkEditCB; CBEmpresa:
              TdmkDBLookupComboBox; Qry: TmySQLQuery = nil;
              Campo: String = 'Filial');
    function  Privilegios(Usuario: Integer; FechaQuery: Boolean = True): Boolean;
    function  NaoPermiteExclusao(Tabela, Form: String;
              Msg: Integer): Boolean;
    function  UTC_ObtemDiferenca(): TTime;
    function  DefineEmpresaSelecionada(const EdEmpresa: TdmkEditCB;
              var Empresa: Integer): Boolean;
    function  ObtemEmpresaSelecionada(const EdEmpresa: TdmkEditCB;
              var Empresa: Integer): Boolean;
{$IfNDef SemEntidade}
    function  CadastroDeEntidade(Entidade: Integer; TipoForm,
              Padrao: TFormCadEnti; EditingByMatriz: Boolean = False;
              AbrirEmAba: Boolean = False; InOwner: TWincontrol = nil;
              Pager: TWinControl = nil;
              LiberMultiEmpresas: Boolean = False; EntTipo: TUnEntTipo = uetNenhum;
              Aba: Integer = 0; EntiContat: Integer = 0): Boolean;
    function  CadastroESelecaoDeEntidade(Entidade: Integer; QrEntidade:
              TmySQLQuery; EdEntidade: TdmkEditCB; CBEntidade:
              TdmkDBLookupComboBox): Boolean;
{$EndIf} // SemEntidade
    function  ObtemCodigoDeEntidadePeloAntigo(Antigo: String): Integer;
    procedure ReopenWebParams(Empresa: Integer);
    procedure ReopenOpcoesGerl();
    procedure ReopenEnti(Entidade: Integer);
    procedure ReopenEntNFS(Entidade: Integer);
    function  ObtemFilialDeEntidade(Entidade: Integer): Integer;
    function  ObtemEntidadeDeFilial(Filial: Integer): Integer;
    // DBGRID
    //procedure CarregaPosicaoColunasDmkDBGrid(NomeForm: String; Usuario: Integer;
    //          dmkDBGrid: TdmkDBGrid);
    procedure CarregaPosicaoColunasDmkDBGrid(Usuario: Integer);
    procedure SalvaPosicaoColunasDmkDBGrid(Usuario: Integer;
              RestauraPadrao: Boolean);
    function  DefineTextoTipoRPS(Codigo: Integer): String;
    function  ObtemNomeCidade(CodigoMunicipio: Integer): String;
    function  ObtemNomeCidade_e_UF(CodigoMunicipio: Integer): String;
    //EntiImagens Mover para UnEnti!*)
    //procedure MostraEntiImagens(Codigo, CodEnti, TipImg: Integer; MostraImagem: Boolean);
    function  ObtemCaminhoImagem(TipImg, CodEnti: Integer): String;
    procedure VerificaCartasAImprimir(GB: TGroupBox; LaAvisoA, LaAvisoB,
              LaAvisoC: TLabel);
    function DadosRetDeEntidade(const Entidade: Integer; var CliInt: Integer;
             const CNAB_Cfg: Integer = 0): Boolean;
    procedure AtualizaEntiConEnt();
{$IfDef DEFINE_VARLCT}
{$IfNDef NO_Financeiro}
    function  SelecionaEmpresa(const SelectLetraLct: TSelectLetraLct;
              const FinanceiroNovo: Boolean = True): Boolean;
{$EndIf}
{$EndIf}
    function  ContatoJaAtrelado(Entidade, Contato: Integer; Avisa: Boolean): Boolean;
    procedure ReopenEntiContat(Query: TmySQLQuery; Entidade: Variant;
              ReopenZero: Boolean);
{$IFDEF UsaWSuport}
    (* Mover para DmkWeb ???*)
    procedure ImportaBancos(Memo3: TMemo);
{$ENDIF}
    function  MD5_por_SQL(Texto: String): String;
    function  ConcatenaPrimeirosNomes(Tabela, FldID, FldAge: String;
              Codigo: Integer): String;
    //Atualiza��es
    procedure CorrigeCorETamInexistentes();
    // Fim Atualiza��es
    function  ObtemVersaoMySQLServer(): String;
    procedure VerificaHorVerao();
    function  EstahNoHorarioDeVerao_e_TZD_JahCorrigido(const Data: TDateTime;
              var TZD_UTC: Double): Boolean;
    function  ObtemHorarioTZD_JahCorrigido(Empresa: Integer): Double;
    function  ObtemFusoHorarioServidor(): Double;
    function  ObtemFusoHorarioLocal(): Double;
    procedure ReopenCambio();
    function  ValidaQtdCliInt(CliInt, CliIntAtuAtivo: Integer): Boolean;
    procedure ReopenMunici(Query: TmySQLQuery; UF: String);
    procedure AjustaFiliaisCliInt();
    procedure SetaFilialdeEntidade(Entidade: Integer; Ed: TdmkEditCB; CB:
              TdmkDBLookupComboBox);
    procedure ExecutaPing(FmMLA: TForm; DataBase: array of TmySQLDatabase;
              Memo1: TMemo = nil);
    {$IfNDef SemCtrlGeral}
    procedure ReopenCtrlGeral();
    {$EndIf}
    function  SalvaRegistroExcluido(SQLType: TSQLType; Tabela:
              String; Niveis: Integer; Nivel01: Extended;
              Nivel02: Extended = 0; Nivel03: Extended = 0;
              Nivel04: Extended = 0; Nivel05: Extended = 0;
              Nivel06: Extended = 0; Nivel07: Extended = 0;
              Nivel08: Extended = 0; Nivel09: Extended = 0;
              Nivel10: Extended = 0): Boolean;
    function  ConectaMySyncDB(NomeDB: String): Boolean;
    procedure ReopenPrestador(Qry: TmySQLQuery);
    function  SiglaDuplicada(Sigla, FldSigla, FldCodigo, FldNome, Tabela: String;
              Codigo: Variant): Boolean;
end;

var
  DModG: TDModG;

implementation

uses Module, UMySQLModule, MyDBCheck, UnALL_Jan, UnGrl_Consts, UnGrl_Vars,
  {$IfNDef ServicoDoWindows} UnMyObjects, {$EndIF}
  {$IfNDef SemCotacoes}CambioCot,{$EndIf}
  {$IfNDef SemDBLocal}VerifiDBLocal, {$EndIf}
  {$IfNDef SemEntidade}Entidades, Entidade2, Entidade3, HorVerao, {$EndIf}
  {$IfNDef SemGrade} ModProd, {$EndIf}
  {$IfNDef SemNFe_0000} ModuleNFE_0000, NFe_PF, {$EndIf}
  {$IfDef DEFINE_VARLCT}
    {$IfNDef NO_FINANCEIRO}
     EmpresaSel, ModuleFin,
    {$EndIf}
  {$EndIf}
  {$IFDEF UsaWSuport} UnDmkWeb, Restaura2, {$ENDIF}
  {$IFNDEF NO_USE_EMAILDMK} MailCfg, {$ENDIF}

//  {$IFNDEF NAO_USA_UnitMD5} UnitMD5, {$ENDIF}
//  UnLock_MLA,
  MyListas, ZCF2, UnGOTOy, Terminais,
  (*EntiImagens,*) (*Restaura2*)
  VerifiDB, UnLic_Dmk, Servidor, Backup3, Principal, MeuDBUses, UnDmkABS_PF;

{$R *.DFM}

procedure TDModG.QrCliIntLogAfterOpen(DataSet: TDataSet);
begin
  VAR_LIB_ARRAY_EMPRESAS_CONTA := QrCliIntLog.RecordCount;
  SetLength(VAR_LIB_ARRAY_EMPRESAS_LISTA, VAR_LIB_ARRAY_EMPRESAS_CONTA);
  QrCliIntLog.First;
  while not QrCliIntLog.Eof do
  begin
    VAR_LIB_ARRAY_EMPRESAS_LISTA[QrCliIntLog.RecNo-1] := QrCliIntLogCodEnti.Value;
    QrCliIntLog.Next;
  end;
end;

function TDModG.ValidaQtdCliInt(CliInt, CliIntAtuAtivo: Integer): Boolean;
var
  Disponiveis, Total: Integer;
  Msg: String;
begin
  Result := True;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT QtdCliInt ',
    'FROM master ',
    '']);
  if QrAux.RecordCount > 0 then
  begin
    Disponiveis := QrAux.FieldByName('QtdCliInt').AsInteger;
    //
    if Disponiveis > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
        'SELECT COUNT(CodCliInt) TotalCliInt ',
        'FROM enticliint ',
        'WHERE CodCliInt <> ' + Geral.FF0(CliInt),
        'AND Ativo IN (0, 1) ',
        '']);
      if QrAux.RecordCount > 0 then
      begin
        if CliIntAtuAtivo in [0, 1] then
          Total := QrAux.FieldByName('TotalCliInt').AsInteger + 1
        else
          Total := QrAux.FieldByName('TotalCliInt').AsInteger;
        //
        if Disponiveis < Total  then
        begin
          if VAR_KIND_DEPTO = kdUH then
            Msg := 'condom�nios'
          else
            Msg := 'empresas';
          //
          Geral.MB_Aviso('Voc� excedeu o limite de ' + Msg + ' dispon�veis!' +
            sLineBreak + 'Solicite a libera��o de mais ' +  Msg + ' junto a Dermatek!');
          //
          Result := False;
        end;
      end;
    end;
  end;
end;

procedure TDModG.QrCliIntUniAfterOpen(DataSet: TDataSet);
begin
  VAR_LIB_ARRAY_EMPRESAS_CONTA := QrCliIntUni.RecordCount;
  SetLength(VAR_LIB_ARRAY_EMPRESAS_LISTA, VAR_LIB_ARRAY_EMPRESAS_CONTA);
  QrCliIntUni.First;
  while not QrCliIntUni.Eof do
  begin
    VAR_LIB_ARRAY_EMPRESAS_LISTA[QrCliIntUni.RecNo-1] := QrCliIntUniCodigo.Value;
    QrCliIntUni.Next;
  end;
end;

procedure TDModG.QrCtrlGeralAfterOpen(DataSet: TDataSet);
begin
  VAR_CtrlGeralMyPathsFrx := QrCtrlGeralMyPathsFrx.Value;
end;

procedure TDModG.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  //  erro mysql 5.1.x
  if QrDonoTipo.Value = 0 then
  begin
    QrDonoUF.Value  := QrDonoEUF.Value;
    QrDonoCEP.Value := QrDonoECEP.Value;
  end else begin
    QrDonoUF.Value  := QrDonoPUF.Value;
    QrDonoCEP.Value := QrDonoPCEP.Value;
  end;
  //  fim erro mysql 5.1.x
  QrDonoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrDonoRua.Value, Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTE2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
end;

procedure TDModG.QrEndereco2CalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  if QrEndereco2Tipo.Value = 0 then
  begin
    QrEndereco2NUMERO.Value := QrEndereco2ENUMERO.Value;
    QrEndereco2LOGRAD.Value := QrEndereco2ELOGRAD.Value;
    QrEndereco2CEP.Value := QrEndereco2ECEP.Value;
  end else begin
    QrEndereco2NUMERO.Value := QrEndereco2PNUMERO.Value;
    QrEndereco2LOGRAD.Value := QrEndereco2PLOGRAD.Value;
    QrEndereco2CEP.Value := QrEndereco2PCEP.Value;
  end;
  QrEndereco2TE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEndereco2Te1.Value);
  QrEndereco2FAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEndereco2Fax.Value);
  QrEndereco2CNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEndereco2CNPJ_CPF.Value);
  //
  QrEndereco2NUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEndereco2Rua.Value, QrEndereco2Numero.Value, False);
  QrEndereco2E_ALL.Value := QrEndereco2NOMELOGRAD.Value;
  if Trim(QrEndereco2E_ALL.Value) <> '' then QrEndereco2E_ALL.Value :=
    QrEndereco2E_ALL.Value + ' ';
  QrEndereco2E_ALL.Value := QrEndereco2E_ALL.Value + QrEndereco2Rua.Value;
  if Trim(QrEndereco2Rua.Value) <> '' then QrEndereco2E_ALL.Value :=
    QrEndereco2E_ALL.Value + ', ' + QrEndereco2NUMERO_TXT.Value;
  if Trim(QrEndereco2Compl.Value) <>  '' then QrEndereco2E_ALL.Value :=
    QrEndereco2E_ALL.Value + ' ' + QrEndereco2Compl.Value;
  if Trim(QrEndereco2Bairro.Value) <>  '' then QrEndereco2E_ALL.Value :=
    QrEndereco2E_ALL.Value + ' - ' + QrEndereco2Bairro.Value;
  if QrEndereco2CEP.Value > 0 then QrEndereco2E_ALL.Value :=
    QrEndereco2E_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEndereco2CEP.Value);
  if Trim(QrEndereco2Cidade.Value) <>  '' then QrEndereco2E_ALL.Value :=
    QrEndereco2E_ALL.Value + ' - ' + QrEndereco2Cidade.Value;
  //
  QrEndereco2CEP_TXT.Value :=Geral.FormataCEP_NT(QrEndereco2CEP.Value);
  //
  if QrEndereco2Tipo.Value = 0 then
  begin
    Natal := QrEndereco2ENatal.Value;
    QrEndereco2NO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEndereco2PNatal.Value;
    QrEndereco2NO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEndereco2NATAL_TXT.Value := ''
  else QrEndereco2NATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDModG.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  if QrEnderecoTipo.Value = 0 then
  begin
    QrEnderecoNUMERO.Value := QrEnderecoENUMERO.Value;
    QrEnderecoLOGRAD.Value := QrEnderecoELOGRAD.Value;
    QrEnderecoCEP.Value := QrEnderecoECEP.Value;
  end else begin
    QrEnderecoNUMERO.Value := QrEnderecoPNUMERO.Value;
    QrEnderecoLOGRAD.Value := QrEnderecoPLOGRAD.Value;
    QrEnderecoCEP.Value := QrEnderecoPCEP.Value;
  end;
  QrEnderecoTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEnderecoRua.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  //
  QrEnderecoE_MIN.Value := QrEnderecoE_ALL.Value;
  //
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  if Trim(QrEnderecoNOMEUF.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoNOMEUF.Value;
  //
  QrEnderecoCEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  //
  if QrEnderecoTipo.Value = 0 then Natal := QrEnderecoENatal.Value
  else Natal := QrEnderecoPNatal.Value;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDModG.QrEntregaCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEntregaTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaTe1.Value);
  QrEntregaFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaFax.Value);
  QrEntregaCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCNPJ_CPF.Value);
  //
  QrEntregaNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaRua.Value, QrEntregaNumero.Value, False);
  QrEntregaE_ALL.Value := QrEntregaNOMELOGRAD.Value;
  if Trim(QrEntregaE_ALL.Value) <> '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' ';
  QrEntregaE_ALL.Value := QrEntregaE_ALL.Value + QrEntregaRua.Value;
  if Trim(QrEntregaRua.Value) <> '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ', ' + QrEntregaNUMERO_TXT.Value;
  if Trim(QrEntregaCompl.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' ' + QrEntregaCompl.Value;
  if Trim(QrEntregaBairro.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + QrEntregaBairro.Value;
  if QrEntregaCEP.Value > 0 then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCEP.Value);
  if Trim(QrEntregaCidade.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + QrEntregaCidade.Value;
  if Trim(QrEntregaNOMEUF.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + QrEntregaNOMEUF.Value;
  //
  QrEntregaCEP_TXT.Value := Geral.FormataCEP_NT(QrEntregaCEP.Value);
  //
  if QrEntregaTipo.Value = 0 then Natal := QrEntregaENatal.Value
  else Natal := QrEntregaPNatal.Value;
  if Natal < 2 then QrEntregaNATAL_TXT.Value := ''
  else QrEntregaNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDModG.QrFiliLogAfterOpen(DataSet: TDataSet);
begin
  VAR_LIB_ARRAY_EMPRESAS_CONTA := QrFiliLog.RecordCount;
  //
  SetLength(VAR_LIB_ARRAY_EMPRESAS_LISTA, VAR_LIB_ARRAY_EMPRESAS_CONTA);
  //
  ReopenWebParams(DModG.QrFiliLogCodigo.Value);
  //
  QrFiliLog.First;
  while not QrFiliLog.Eof do
  begin
    VAR_LIB_ARRAY_EMPRESAS_LISTA[QrFiliLog.RecNo-1] := QrFililogCodigo.Value;
    QrFiliLog.Next;
  end;
end;

procedure TDModG.QrMasterAfterOpen(DataSet: TDataSet);
begin
  (*
  Desativado em: 03/02/2014
    => Motivo: Se n�o tiver o CNPJ cadastrado dar� a mensagem ao abrir o Dmod.ReopenMaster na incializa��o do aplicativo

  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
    GOTOy.AvisoIndef(3);
  *)
  //
  //
  FmPrincipal.Caption := Application.Title+'  ::  ' + QrMasterEm.Value + ' # ' +
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  //
  //if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
end;

procedure TDModG.QrParamsEmpCalcFields(DataSet: TDataSet);
begin
  QrParamsEmpTZD_UTC_Str.Value := dmkPF.TZD_UTC_FloatToSignedStr(QrParamsEmpTZD_UTC.Value);
end;

procedure TDModG.QrPKACalcFields(DataSet: TDataSet);
begin
  QrPKADataSai_TXT.Value := dmkPF.FDT_NULO(QrPKADataSai.Value, 3);
  QrPKADataRec_TXT.Value := dmkPF.FDT_NULO(QrPKADataRec.Value, 3);
  QrPKADataRet_TXT.Value := dmkPF.FDT_NULO(QrPKADataRet.Value, 3);
end;

procedure TDModG.QrPPICalcFields(DataSet: TDataSet);
begin
  QrPPIDataSai_TXT.Value := dmkPF.FDT_NULO(QrPPIDataSai.Value, 3);
  QrPPIDataRec_TXT.Value := dmkPF.FDT_NULO(QrPPIDataRec.Value, 3);
  QrPPIDataRet_TXT.Value := dmkPF.FDT_NULO(QrPPIDataRet.Value, 3);
end;

procedure TDModG.QrPTKAfterScroll(DataSet: TDataSet);
begin
  ReopenPPI(FFiltroPPI);
end;

procedure TDModG.ReopenPPI(Filtro: Integer);
begin
  QrPPI.Close;
  QrPPI.SQL.Clear;
  QrPPI.SQL.Add('SELECT ppi.Texto, ppi.DataE, ppi.Conta, ppi.Retorna,');
  QrPPI.SQL.Add('IF(ppi.Retorna=0,"N","S") NO_RETORNA,');
  QrPPI.SQL.Add('IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,');
  QrPPI.SQL.Add('ppi.LimiteSai, ppi.Saiu, ppi.DataSai,');
  QrPPI.SQL.Add('ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,');
  QrPPI.SQL.Add('ppi.LimiteRet, ppi.Retornou, ppi.DataRet, ');
  QrPPI.SQL.Add('ppi.Texto, ppi.Lancto, ppi.Docum');
  QrPPI.SQL.Add('FROM protpakits ppi');
  QrPPI.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo');
  QrPPI.SQL.Add('LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle');
  QrPPI.SQL.Add('LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual');
  QrPPI.SQL.Add('WHERE (');
  //Mudei 2010-10-27
  //if not FPTK_Abertos then
  case Filtro of
    0: // Antigo false
    begin
      QrPPI.SQL.Add('  (Saiu=0 AND LimiteSai <= SYSDATE())');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Recebeu=0 AND LimiteRem <= SYSDATE())');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE())');
    end;
    1: // antigo true
    begin
      QrPPI.SQL.Add('  (Saiu=0)');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Recebeu=0)');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Retorna=1 AND Retornou=0)');
    end;
    2,3: // Tudo
    begin
      QrPPI.SQL.Add('Conta <> 0');
    end;
  end;
  QrPPI.SQL.Add(')');
  if Filtro = 3 then
    QrPPI.SQL.Add('AND ppi.Cancelado=1')
  else
    QrPPI.SQL.Add('AND ppi.Cancelado=0');
  //
  if FPTK_Def_Client <> 0 then
    QrPPI.SQL.Add('AND ptc.Def_Client=' + FormatFloat('0', FPTK_Def_Client));
  if FPTK_ID_Cod1 <> 0 then
    QrPPI.SQL.Add('AND ppi.ID_Cod1=' + FormatFloat('0', FPTK_ID_Cod1));
  if FPTK_ID_Cod2 <> 0 then
    QrPPI.SQL.Add('AND ppi.ID_Cod2=' + FormatFloat('0', FPTK_ID_Cod2));
  if FPTK_ID_Cod3 <> 0 then
    QrPPI.SQL.Add('AND ppi.ID_Cod3=' + FormatFloat('0', FPTK_ID_Cod3));
  if FPTK_ID_Cod4 <> 0 then
    QrPPI.SQL.Add('AND ppi.ID_Cod4=' + FormatFloat('0', FPTK_ID_Cod4));
  //
  QrPPI.SQL.Add('AND ptk.Controle=' + FormatFloat('0', QrPTKLote.Value));
  QrPPI.SQL.Add('ORDER BY ppi.DataD, ppi.Conta');
  UnDmkDAC_PF.AbreQueryApenas(QrPPI);
end;

procedure TDModG.QrPTKBeforeClose(DataSet: TDataSet);
begin
  QrPPI.Close;
end;

procedure TDModG.QrPTKCalcFields(DataSet: TDataSet);
begin
  QrPTKMes_TXT.Value := dmkPF.MezToMesEAnoCurto(QrPTKMez.Value);
end;

procedure TDModG.QrSCECalcFields(DataSet: TDataSet);
  function DFS(Status: Integer; ValVerdade: Double): String;
  begin
    Result := dmkPF.EscolhaDe2Str(Status=0, 'n/i',
      Geral.FFT(ValVerdade, 2, siNegativo));
  end;
  {function SFS(Status: Integer; ValVerdade: Double): String;
  begin
    Result := dmkPF.EscolhaDe2(Status=0, 'n/i',
      Geral.FFT(ValVerdade + QrSCE, 2, siNegativo));
  end;}
begin
  QrSCEPL_ATU_TXT.Value := DFS(QrSCEPL_CS.Value, QrSCEPL_ATU.Value);
  QrSCEPL_FUT_TXT.Value := DFS(QrSCEPL_CS.Value, QrSCEPL_FUT.Value);
  //
  QrSCECJ_ATU_TXT.Value := DFS(QrSCECJ_CS.Value, QrSCECJ_ATU.Value);
  QrSCECJ_FUT_TXT.Value := DFS(QrSCECJ_CS.Value, QrSCECJ_FUT.Value);
  //
  QrSCEGR_ATU_TXT.Value := DFS(QrSCEGR_CS.Value, QrSCEGR_ATU.Value);
  QrSCEGR_FUT_TXT.Value := DFS(QrSCEGR_CS.Value, QrSCEGR_FUT.Value);
  //
  QrSCESG_ATU_TXT.Value := DFS(QrSCESG_CS.Value, QrSCESG_ATU.Value);
  QrSCESG_FUT_TXT.Value := DFS(QrSCESG_CS.Value, QrSCESG_FUT.Value);
  //
  QrSCECO_ATU_TXT.Value := DFS(QrSCECO_CS.Value, QrSCECO_ATU.Value);
  QrSCECO_FUT_TXT.Value := DFS(QrSCECO_CS.Value, QrSCECO_FUT.Value);
  //
  //
  //
  QrSCEPL_PEA_TXT.Value := DFS(QrSCEPL_CS.Value, QrSCEPL_PEA.Value);
  QrSCEPL_PED_TXT.Value := DFS(QrSCEPL_CS.Value, QrSCEPL_PED.Value);
  //
  QrSCECJ_PEA_TXT.Value := DFS(QrSCECJ_CS.Value, QrSCECJ_PEA.Value);
  QrSCECJ_PED_TXT.Value := DFS(QrSCECJ_CS.Value, QrSCECJ_PED.Value);
  //
  QrSCEGR_PEA_TXT.Value := DFS(QrSCEGR_CS.Value, QrSCEGR_PEA.Value);
  QrSCEGR_PED_TXT.Value := DFS(QrSCEGR_CS.Value, QrSCEGR_PED.Value);
  //
  QrSCESG_PEA_TXT.Value := DFS(QrSCESG_CS.Value, QrSCESG_PEA.Value);
  QrSCESG_PED_TXT.Value := DFS(QrSCESG_CS.Value, QrSCESG_PED.Value);
  //
  QrSCECO_PEA_TXT.Value := DFS(QrSCECO_CS.Value, QrSCECO_PEA.Value);
  QrSCECO_PED_TXT.Value := DFS(QrSCECO_CS.Value, QrSCECO_PED.Value);
  //
end;

function TDModG.ObtemNomeFantasiaEmpresa(): String;
begin
  ReopenDono;
  //
  Result := QrDonoAPELIDO.Value;
  if Trim(Result) = '' then
    Result := QrDonoNOMEDONO.Value;
end;

function TDModG.ObtemNomeUFDeEntidade(Entidade: Integer): String;
var
  Qry: TmySQLQuery;
  Empresa: Integer;
begin
  Result := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NomeUF ',
      'FROM entidades ent ',
      'LEFT JOIN ufs ufe ON ufe.Codigo=ent.EUF ',
      'LEFT JOIN ufs ufp ON ufp.Codigo=ent.PUF ',
      'WHERE ent.Codigo=' + Geral.FF0(Entidade),
      '']);
    //
    Result := Qry.FieldByName('NomeUF').AsString;
  finally
    Qry.Free;
  end;
end;

function TDModG.ObtemPrimeiroEMail_NFe(Empresa, Entidade: Integer): String;
begin
  ReopenParamsEmp(Empresa);
  //
  QrEntiMail.Close;
  QrEntiMail.Params[00].AsInteger := Entidade;
  QrEntiMail.Params[01].AsInteger := DModG.QrParamsEmpEntiTipCto.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrEntiMail);
  //
  Result := QrEntiMailEMail.Value;
end;

function TDModG.ObtemPrimeiroEMail_NFSe(Empresa, Entidade: Integer): String;
begin
  ReopenParamsEmp(Empresa);
  //
  QrEntiMail.Close;
  QrEntiMail.Params[00].AsInteger := Entidade;
  QrEntiMail.Params[01].AsInteger := DModG.QrParamsEmpNFSeTipCtoMail.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrEntiMail);
  //
  Result := QrEntiMailEMail.Value;
end;

function TDModG.ObtemReduzidoDeNivel1(Nivel1: Integer): Integer;
begin
  Result := 0;
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT Controle');
  QrAux.SQL.Add('FROM gragrux');
  QrAux.SQL.Add('WHERE GraGru1=:P0');
  QrAux.Params[0].AsInteger := Nivel1;
  UnDmkDAC_PF.AbreQueryApenas(QrAux);
  //
  case QrAux.RecordCount of
    0: Geral.MB_Aviso(
    'Nenhum reduzido foi encontrado para o produto selecionado!');
    1: Result := QrAux.FieldByName('Controle').AsInteger;
    2: Geral.MB_Aviso('Foi encontrado mais de um reduzido para o ' + sLineBreak +
    'produto selecionado e nenhum foi considerado!');
  end;  
end;

function TDModG.ObtemTipoDeLogradouroERuaDeLogradouro(const xLogr: String;
  var Logr_I: Integer; var Rua: String): Boolean;
var
  P: Integer;
  Logr_T: String;
begin
  P := pos(' ', xLogr);
  Logr_T := Copy(xLogr, 1, P - 1);
  //
  QrLocLogr.Close;
  QrLocLogr.Params[0].AsString := Logr_T;
  UnDmkDAC_PF.AbreQueryApenas(QrLocLogr);
  //
  Logr_I := QrLocLogrCodigo.Value;
  if Logr_I <> 0 then
    Rua := Copy(xLogr, P + 1)
  else
    Rua := xLogr;
  Result := True;  
end;

function TDModG.ObtemUFX(UF: Integer): String;
begin
  QrGetUFX.Close;
  QrGetUFX.Params[0].AsInteger := UF;
  UnDmkDAC_PF.AbreQueryApenas(QrGetUFX);
  //
  Result := QrGetUFXNome.Value;
end;

function TDModG.ObtemVersaoMySQLServer(): String;
var
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery.Create(Dmod);
  try
    Query.Close;
    Query.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Version() ',
    '']);
    //
    Result := Query.Fields[0].AsString;
  finally
    Query.Free;
  end;
end;

function TDModG.ObterDBVersao: String;
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, ['SELECT Version() Versao']);
    //
    Result := QrAux.FieldByName('Versao').AsString;
  except
    Result := '';
  end;
end;

function TDModG.Privilegios(Usuario: Integer; FechaQuery: Boolean = True): Boolean;
begin
  //Result := False;
  FM_MASTER := 'F';
  if VAR_DERMA_AD = 2 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPerfis, Dmod.MyDB, [
    'SELECT pit.Libera, pit.Janela ',
    'FROM perfits pit ',
    'WHERE pit.Codigo=' + Geral.FF0(Usuario),
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPerfis, Dmod.MyDB, [
    'SELECT pip.Libera, pit.Janela ',
    'FROM perfisits pit ',
    'LEFT JOIN perfisitsperf pip ',
    '  ON pip.Janela=pit.Janela AND pip.Codigo=' + Geral.FF0(Usuario),
    '']);
  end;
  Result := QrPerfis.RecordCount > 0;
  //
  if FechaQuery then
    QrPerfis.Close;
end;

function TDModG.ObtemNomeCidade(CodigoMunicipio: Integer): String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCidade, DmodG.AllID_DB, [
  'SELECT Nome ',
  'FROM dtb_munici ',
  'WHERE Codigo=' + Geral.FF0(CodigoMunicipio),
  '']);
  Result := QrCidadeNome.Value;
end;

function TDModG.ObtemNomeCidade_e_UF(CodigoMunicipio: Integer): String;
var
  CodUF: String;
begin
  CodUF := Copy(Geral.FFN(CodigoMunicipio, 7), 1, 2);
  Result := ObtemNomeCidade(CodigoMunicipio) + '/' +
    Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Geral.IMV(CodUF));
end;

function TDModG.ObtemNomeDepto(Depto: Integer): String;
begin
  if (Uppercase(Application.Title) = 'SYNDIC')
  or (Uppercase(Application.Title) = 'SYNKER') then
  begin
    QrDepto.Close;
    QrDepto.SQL.Clear;
    QrDepto.SQL.Add('SELECT Unidade NOMEDEPTO');
    QrDepto.SQL.Add('FROM condimov');
    QrDepto.SQL.Add('WHERE Conta=:P0');
    QrDepto.Params[0].AsInteger := Depto;
    UnDmkDAC_PF.AbreQueryApenas(QrDepto);
    Result := QrDeptoNOMEDEPTO.Value;
  end else Result := '';
end;

function TDModG.ReopenEndereco(Entidade: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndereco, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    '/* ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    '*/ ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    '/* ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    '*/ ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    '']);
  Result := QrEndereco.RecordCount > 0;
end;

function TDModG.ReopenEndereco2(Entidade: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndereco2, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    '/* ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    '*/ ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    '/* ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    '*/ ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    '']);
  Result := QrEndereco2.RecordCount > 0;
end;

procedure TDModG.ReopenEndereco3(Query: TmySQLQuery; Entidade: Integer);
var
  SQLEnti: String;
begin
  if Entidade <> 0 then
    SQLEnti := 'WHERE en.Codigo=' + Geral.FF0(Entidade)
  else
    SQLEnti := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) + 0.000 NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) + 0.000 CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG, IE ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    SQLEnti,
    'ORDER BY NOME_ENT',
  '']);
end;

procedure TDModG.ReopenEndereco4(Query: TmySQLQuery; Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT    ',
  ' en.L_CNPJ, ',
  ' en.L_CPF, ',
  ' en.L_Nome, ',
  ' en.LLograd, ',
  ' llo.Nome NO_LLograd, ',
  ' en.LRua, ',
  ' en.LNumero, ',
  ' en.LCompl, ',
  ' en.LBairro, ',
  ' en.LCodMunici, ',
  ' mun.Nome xMunicipio, ',
  ' en.LUF, ',
  ' ufl.Nome NO_LUF, ',
  ' en.LCEP, ',
  ' en.LCodiPais, ',
  ' pai.Nome xPais, ',
  ' en.LTel, ',
  ' en.LEmail, ',
  ' en.L_IE,',
  ' en.L_Ativo  ',
  'FROM entidades en ',
  'LEFT JOIN listalograd llo ON llo.Codigo=en.LLograd ',
  'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Entidade),
  '']);
end;

procedure TDModG.ReopenEnti(Entidade: Integer);
begin
  QrEnti.Close;
  QrEnti.Params[0].AsInteger  := Entidade;
  UMyMod.AbreQuery(QrEnti, DMod.MyDB);
end;

function TDModG.ReopenEntiCliInt(EntiCliInt: Integer): Boolean;
begin
  Result := False;
  if UnDmkDAC_PF.AbreMySQLQuery0(QrEntiCliInt, Dmod.MyDB, [
  'SELECT * ',
  'FROM enticliint ',
  'WHERE CodEnti=' + FormatFloat('0', EntiCliInt),
  '']) then
    Result := QrEntiCliInt.RecordCount > 0;
end;

procedure TDModG.ReopenEntiContat(Query: TmySQLQuery; Entidade: Variant;
ReopenZero: Boolean);
var
  Cliente: Integer;
  SQL_WHERE: String;
begin
  Query.Close;
  if Entidade = null then
    Cliente := 0
  else
    Cliente := Entidade;
  //
  if ReopenZero or (Cliente <> 0) then
  begin
    if Cliente <> 0 then
      SQL_WHERE := 'WHERE eco.Codigo=' + Geral.FF0(Cliente)
    else
      SQL_WHERE := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT DISTINCT eco.Controle, eco.Nome ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    SQL_WHERE,
    'ORDER BY Nome ',
    '']);
  end;  
end;

procedure TDModG.ReopenEntNFS(Entidade: Integer);
begin
  QrEntNFS.Close;
  QrEntNFS.Params[0].AsInteger  := Entidade;
  UMyMod.AbreQuery(QrEntNFS, DMod.MyDB);
end;

function TDModG.ReopenEntrega(Entidade: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntrega, Dmod.MyDB, [
    'SELECT en.Codigo, en.L_Ativo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOME_ENT, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CPF, ',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, ',
    'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, ',
    'en.LRua RUA, ',
    'en.LNumero NUMERO, ',
    'en.LCompl  COMPL, ',
    'en.LBairro BAIRRO, ',
    'mun.Nome CIDADE, ',
    'llo.Nome   NOMELOGRAD, ',
    'ufs.Nome   NOMEUF, ',
    'en.LPais   Pais, ',
    'en.LLograd Lograd, ',
    'en.LTel    TE1, ',
    'en.LCEP    CEP, ',
    'en.LFax     FAX ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufs ON ufs.Codigo=en.LUF ',
    'LEFT JOIN listalograd llo ON llo.Codigo=en.LLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = en.LCodMunici ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    '']);
  Result := QrEntrega.RecordCount > 0;
end;

procedure TDModG.ReopenMaster();
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrMaster, Dmod.MyDB, [
    'SELECT cm.Dono, cm.Versao, ma.* ',
    'FROM controle cm ',
    'LEFT JOIN entidades te ON te.Codigo=cm.Dono ',
    //'LEFT JOIN master ma ON ma.CNPJ=te.CNPJ ', Corrigido em 13/08/2012
    'LEFT JOIN master ma ON ma.CNPJ=IF(te.Tipo = 0, te.CNPJ, te.CPF)', 
    '']);
  except
    Geral.MB_Erro('Erro ao abrir a tabela master!');
    //
    VAR_PORQUE_VERIFICA := VAR_PORQUE_VERIFICA + ';ModuleGeral:1777';
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
end;

procedure TDModG.ReopenMunici(Query: TmySQLQuery; UF: String);
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, AllID_DB, [
      'SELECT mun.Codigo, mun.Nome ',
      'FROM dtb_munici mun ',
      Geral.ATS_If(Length(UF) <> 0, ['WHERE LEFT(mun.Codigo, 2)="' +
        Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF)) + '"']),
      'ORDER BY mun.Nome ',
      '']);
  except
    DBCheck.AvisaDBTerceiros();
  end;
end;

procedure TDModG.ReopenWebParams(Empresa: Integer);
begin
  if USQLDB.TabelaExiste('web_params', Dmod.MyDB) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrWebParams, Dmod.MyDB, [
      'SELECT *, ',
      'AES_DECRYPT(Web_Pwd, "' + CO_RandStrWeb01 + '") Web_Pwd_STR, ',
      'AES_DECRYPT(Web_FTPs, "' + CO_RandStrWeb01 + '") Web_FTPs_STR ',
      'FROM web_params ',
      'WHERE Empresa=' + Geral.FF0(Empresa),
      '']);
    //
    VAR_WEB_ID := QrWebParams.FieldByName('Web_Id').AsString;
    VAR_DBWEB  := QrWebParams.FieldByName('Web_DB').AsString;
  end else
    QrWebParams.Close;
end;

procedure TDModG.ReopenOpcoesGerl;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesGerl, Dmod.MyDB, [
    'SELECT * ',
    'FROM opcoesgerl ',
    '']);
  //
  VAR_SLOGAN_FOOTER := QrOpcoesGerl.FieldByName('SloganFoot').AsString;
end;

{
procedure TDModG.ReopenMaster 2();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMaster2, Dmod.MyDB, [
  'SELECT * FROM master ',
  '']);
end;
}

{
   N�o usar mais! agora pode ser mais de uma!
procedure TDModG.ReopenFilial(Filial: Integer);
begin
  QrFilial.Close;
  QrFilial.Params[0].AsInteger := Filial;
  UnDmkDAC_PF.AbreQueryApenas(QrFilial);
end;
  }

procedure TDModG.ReopenParamsEmp(Empresa: Integer);
begin
(*
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := Empresa;
  QrParamsEmp. O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrParamsEmp, Dmod.MyDB, [
    'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, emp.* ',
    'FROM paramsemp emp ',
    'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo ',
    'WHERE emp.Codigo=' + Geral.FF0(Empresa),
    '']);
  //
  VAR_NT2018_05v120 := QrParamsEmpNT2018_05v120.Value > 0;
  Dmod.ReopenParamsEspecificos(Empresa);
  //
end;

function TDModG.ReopenPreEmail(PreEmail: Integer): Boolean;
begin
  Result := False;
  //
  DModG.QrPreEmail.Close;
  DModG.QrPreEmMsg.Close;
  DModG.QrPEM_0.Close;
  DModG.QrPEM_1.Close;
  DModG.QrPEM_2.Close;
  DModG.QrPEM_T.Close;
  //
  if PreEmail <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrPreEmail, Dmod.MyDB, [
      'SELECT pre.Codigo, pre.Nome, con.SMTPServer, ',
      'con.Send_Name, con.Usuario Send_Mail, ',
      'AES_DECRYPT(con.Password, "' + CO_RandStrWeb01 + '")  Pass_Mail, ',
      'con.Logi_Name, AES_DECRYPT(con.Logi_Pass, "' + CO_RandStrWeb01 +'")  Logi_Pass, ',
      'con.Logi_Auth, pre.Mail_Titu, pre.Saudacao, pre.NaoEnvBloq, ',
      'con.Porta_mail, con.Logi_SSL, pre.EmailConta ',
      'FROM preemail pre ',
      'LEFT JOIN  emailconta con ON con.Codigo = pre.EmailConta ',
      'WHERE pre.Codigo=' + Geral.FF0(PreEmail),
      '']);
    //
    DModG.QrPreEmMsg.Params[0].AsInteger := PreEmail;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrPreEmMsg);
    // Texto plano
    DModG.QrPEM_0.Params[0].AsInteger := PreEmail;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrPEM_0);
    // HTML
    DModG.QrPEM_1.Params[0].AsInteger := PreEmail;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrPEM_1);
    // Imagens
    DModG.QrPEM_2.Params[0].AsInteger := PreEmail;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrPEM_2);
    // Tudo
    DModG.QrPEM_T.Params[0].AsInteger := PreEmail;
    UnDmkDAC_PF.AbreQueryApenas(DModG.QrPEM_T);
    //
    Result := True;
  end;
end;

procedure TDModG.ReopenPrestador(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE ',
  'FROM entidades ent ',
  'WHERE ent.' + VAR_FLD_ENT_PRESTADOR + '="V" ',
  'OR Codigo <= -11',
  'ORDER BY NOMEENTIDADE ',
  '']);

end;

procedure TDModG.ReopenPTK();
var
  Lote: Integer;
begin
  if QrPTK.State <> dsInactive then
    Lote := QrPTKLote.Value
  else
    Lote := 0;
  QrPTK.Close;
  QrPKA.Close;
  if FPTK_Def_Client <> 0 then
  begin
    QrPTK.SQL.Clear;
    QrPTK.SQL.Add('SELECT DISTINCT ptc.Nome, ptk.Controle Lote, ptk.Mez');
    QrPTK.SQL.Add('FROM protpakits ppi');
    QrPTK.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo');
    QrPTK.SQL.Add('LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle');
    QrPTK.SQL.Add('WHERE (');
  {
    if not FPTK_Abertos then
    begin
      QrPTK.SQL.Add('  (Saiu=0 AND LimiteSai <= SYSDATE())');
      QrPTK.SQL.Add('   OR');
      QrPTK.SQL.Add('  (Recebeu=0 AND LimiteRem <= SYSDATE())');
      QrPTK.SQL.Add('   OR');
      QrPTK.SQL.Add('  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE())');
    end else begin
      QrPTK.SQL.Add('  (Saiu=0)');
      QrPTK.SQL.Add('   OR');
      QrPTK.SQL.Add('  (Recebeu=0)');
      QrPTK.SQL.Add('   OR');
      QrPTK.SQL.Add('  (Retorna=1 AND Retornou=0)');
    end;
    QrPTK.SQL.Add(')');
    QrPTK.SQL.Add('AND ppi.Cancelado=0');
  }
    //Mudei 2010-10-27
    //if not FPTK_Abertos then
    case FFiltroPPI of
      0: // Antigo false
      begin
        QrPTK.SQL.Add('  (Saiu=0 AND LimiteSai <= SYSDATE())');
        QrPTK.SQL.Add('   OR');
        QrPTK.SQL.Add('  (Recebeu=0 AND LimiteRem <= SYSDATE())');
        QrPTK.SQL.Add('   OR');
        QrPTK.SQL.Add('  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE())');
      end;
      1: // antigo true
      begin
        QrPTK.SQL.Add('  (Saiu=0)');
        QrPTK.SQL.Add('   OR');
        QrPTK.SQL.Add('  (Recebeu=0)');
        QrPTK.SQL.Add('   OR');
        QrPTK.SQL.Add('  (Retorna=1 AND Retornou=0)');
      end;
      2,3: // Tudo
      begin
        QrPTK.SQL.Add('Conta <> 0');
      end;
    end;
    QrPTK.SQL.Add(')');
    if FFiltroPPI = 3 then
      QrPTK.SQL.Add('AND ppi.Cancelado=1')
    else
      QrPTK.SQL.Add('AND ppi.Cancelado=0');
    //
    // 2011-09-24
    //if FPTK_Def_Client <> 0 then
      QrPTK.SQL.Add('AND ptc.Def_Client=' + FormatFloat('0', FPTK_Def_Client));
    // fim 2011-09-24
    if FPTK_ID_Cod1 <> 0 then
      QrPTK.SQL.Add('AND ppi.ID_Cod1=' + FormatFloat('0', FPTK_ID_Cod1));
    if FPTK_ID_Cod2 <> 0 then
      QrPTK.SQL.Add('AND ppi.ID_Cod2=' + FormatFloat('0', FPTK_ID_Cod2));
    if FPTK_ID_Cod3 <> 0 then
      QrPTK.SQL.Add('AND ppi.ID_Cod3=' + FormatFloat('0', FPTK_ID_Cod3));
    if FPTK_ID_Cod4 <> 0 then
      QrPTK.SQL.Add('AND ppi.ID_Cod4=' + FormatFloat('0', FPTK_ID_Cod4));
    //
    QrPTK.SQL.Add('ORDER BY ptk.Mez, ptk.Controle');
    // Parei aqui!
    //colocar aviso de entidade errada! Def_Client
    //Geral.M B _ S Q L (nil, QrPTK);
    UnDmkDAC_PF.AbreQueryApenas(QrPTK);
    if Lote <> 0 then
      QrPTK.Locate('Lote', Lote, []);
    //
    UnDmkDAC_PF.AbreQueryApenas(QrPKA);
    QrPKA.Locate('Conta', FPKA_Next, []);
  end;
end;

function TDModG.ObtemEnderecoEtiqueta3Linhas(const Entidade: Integer;
var Linha1, linha2, Linha3: String): String;
begin
  Linha1 := '';
  Linha2 := '';
  Linha3 := '';
  if ReopenEndereco(Entidade) then
  begin
    //  Linha 1
    if Trim(QrEnderecoNOMELOGRAD.Value) <> '' then
      Linha1 := QrEnderecoNOMELOGRAD.Value + ' ';
    if Trim(QrEnderecoRUA.Value) <> '' then
      Linha1 := Linha1 + QrEnderecoRUA.Value + ', '
    else
      Linha1 := Linha1 + QrEnderecoRUA.Value + '?, ';
    if Trim(QrEnderecoNUMERO_TXT.Value) <> '' then
      Linha1 := Linha1 + QrEnderecoNUMERO_TXT.Value
    else
      Linha1 := Linha1 + QrEnderecoRUA.Value + 'S/N� ';
    if Trim(QrEnderecoCOMPL.Value) <> '' then
      Linha1 := Linha1 + ' - ' + QrEnderecoCOMPL.Value;
    //  Linha 2
    if Trim(QrEnderecoBAIRRO.Value) <> '' then
      Linha2 := 'BAIRRO ' + QrEnderecoBAIRRO.Value
    else
      Linha2 := '';
    //  Linha 3
    if Trim(QrEnderecoCEP_TXT.Value) <> '' then
      Linha3 := QrEnderecoCEP_TXT.Value + ' ';
    Linha3 := Linha3 + QrEnderecoCIDADE.Value;
    if Trim(QrEnderecoNOMEUF.Value) <> '' then
      Linha3 := Linha3 + ' - ' + QrEnderecoNOMEUF.Value;

    //

    Result := '';
    if Linha1 <> '' then
      Result := Result + Uppercase(linha1) + sLineBreak;
    if Linha2 <> '' then
      Result := Result + Uppercase(linha2) + sLineBreak;
    if Linha3 <> '' then
      Result := Result + Uppercase(linha3);// + sLineBreak;

  end else Result := '';
end;

function TDModG.ObtemEmpresaSelecionada(const EdEmpresa: TdmkEditCB;
  var Empresa: Integer): Boolean;
begin
  if EdEmpresa.ValueVariant <> 0 then
    Empresa := QrEmpresasCodigo.Value
  else
    EMpresa := 0;
  Result := not MyObjects.FIC(Empresa=0, nil, 'Defina a Empresa');
end;

function TDModG.ObtemEnderecoEntrega3Linhas(const Entidade: Integer;
var Linha1, linha2, Linha3: String): String;
begin
  Linha1 := '';
  Linha2 := '';
  Linha3 := '';
  if ReopenEntrega(Entidade) then
  begin
    if Trim(QrEntregaRUA.Value) = '' then
      Result := ''
    else begin
      //  Linha 1
      if Trim(QrEntregaNOMELOGRAD.Value) <> '' then
        Linha1 := QrEntregaNOMELOGRAD.Value + ' ';
      if Trim(QrEntregaRUA.Value) <> '' then
        Linha1 := Linha1 + QrEntregaRUA.Value + ', '
      else
        Linha1 := Linha1 + QrEntregaRUA.Value + '?, ';
      if Trim(QrEntregaNUMERO_TXT.Value) <> '' then
        Linha1 := Linha1 + QrEntregaNUMERO_TXT.Value
      else
        Linha1 := Linha1 + QrEntregaRUA.Value + 'S/N� ';
      if Trim(QrEntregaCOMPL.Value) <> '' then
        Linha1 := Linha1 + ' - ' + QrEntregaCOMPL.Value;
      //  Linha 2
      if Trim(QrEntregaBAIRRO.Value) <> '' then
        Linha2 := 'BAIRRO ' + QrEntregaBAIRRO.Value
      else
        Linha2 := '';
      //  Linha 3
      if Trim(QrEntregaCEP_TXT.Value) <> '' then
        Linha3 := QrEntregaCEP_TXT.Value + ' ';
      Linha3 := Linha3 + QrEntregaCIDADE.Value;
      if Trim(QrEntregaNOMEUF.Value) <> '' then
        Linha3 := Linha3 + ' - ' + QrEntregaNOMEUF.Value;

      //

      Result := '';
      if Linha1 <> '' then
        Result := Result + Uppercase(linha1) + sLineBreak;
      if Linha2 <> '' then
        Result := Result + Uppercase(linha2) + sLineBreak;
      if Linha3 <> '' then
        Result := Result + Uppercase(linha3);// + sLineBreak;
    end;
  end else Result := '';
end;

procedure TDModG.ReopenSenhas;
begin
  QrSenhas.Close;
  QrSenhas.Database := Dmod.MyDB;
  QrSenhas.Params[0].AsInteger := VAR_USUARIO;
  UMyMod.AbreQuery(QrSenhas, Dmod.MyDB);
end;

procedure TDModG.ReopenStqCenCad(Codigo: Integer);
begin
  QrStqCenCad.Close;
  UnDmkDAC_PF.AbreQueryApenas(QrStqCenCad);
  //
  if Codigo > 0 then
    QrStqCenCad.Locate('Codigo', Codigo, []);
end;

procedure TDModG.ReopenTerminal(LiberaUso5: Boolean = False);
var
  SerialKey, SerialNum: String;
begin
  if LiberaUso5 then
  begin
    SerialKey := Geral.ReadAppKeyCU(TMeuDB + '_SerialKey', Application.Title, ktString, '');
    SerialNum := Geral.ReadAppKeyCU(TMeuDB + '_SerialNum', Application.Title, ktString, '');
  end else
  begin
    SerialKey := FSerialKey;
    SerialNum := FSerialNum;
  end;
  //
  QrTerminal.Close;
  QrTerminal.Params[00].AsString := SerialKey;
  QrTerminal.Params[01].AsString := SerialNum;
  UnDmkDAC_PF.AbreQueryApenas(QrTerminal);
end;

{$IFDEF DEFINE_VARLCT}
{$IfNDef NO_Financeiro}
function TDModG.SelecionaEmpresa(const SelectLetraLct: TSelectLetraLct;
  const FinanceiroNovo: Boolean): Boolean;
var
  Entidade: Integer;
  FinNovo: Boolean;
begin
  EmpresaAtual_SetaCodigos(0, tecEntidade, False);
  //
  ReopenEmpresas(VAR_USUARIO, 0);

  if DModG.QrEmpresas.RecordCount = 1 then
  begin
    EmpresaAtual_SetaCodigos(DModG.QrEmpresasCodigo.Value, tecEntidade, True);
    VAR_LETRA_LCT := 'a';
    // N�o pode!
    //FmPrincipal.DefineVarsCliInt(FEmpresa);
    DModG.NomeTab(TMeuDB, ntLct, True);
  end else
  begin
    if DBCheck.CriaFm(TFmEmpresaSel, FmEmpresaSel, afmoLiberado) then
    begin
      case SelectLetraLct of
        sllNenhum:
        begin
          FmEmpresaSel.RGLetraLct.Visible := False;
        end;
        sllLivre: ; // Nada
        //
        sllSelecA: FmEmpresaSel.RGLetraLct.ItemIndex := 0;
        sllSelecB: FmEmpresaSel.RGLetraLct.ItemIndex := 1;
        sllSelecC: FmEmpresaSel.RGLetraLct.ItemIndex := 2;
        sllSelecD: FmEmpresaSel.RGLetraLct.ItemIndex := 3;
        sllSelecE: FmEmpresaSel.RGLetraLct.ItemIndex := 4;
        sllSelecF: FmEmpresaSel.RGLetraLct.ItemIndex := 5;
        //
        sllForcaA: FmEmpresaSel.RGLetraLct.ItemIndex := 0;
        sllForcaB: FmEmpresaSel.RGLetraLct.ItemIndex := 1;
        sllForcaC: FmEmpresaSel.RGLetraLct.ItemIndex := 2;
        sllForcaD: FmEmpresaSel.RGLetraLct.ItemIndex := 3;
        sllForcaE: FmEmpresaSel.RGLetraLct.ItemIndex := 4;
        sllForcaF: FmEmpresaSel.RGLetraLct.ItemIndex := 5;
      end;
      FmEmpresaSel.RGLetraLct.Enabled := SelectLetraLct < sllForcaA;
      if DModG.QrEmpresas.RecordCount = 0 then
        Geral.MB_Aviso('N�o h� empresa cadastrada!')
      else begin
        if DModG.QrEmpresas.RecordCount = 1 then
        begin
          {
          Precisa saber a tabela de lan ctos
          FEmpresa  := DModG.QrEmpresasFilial.Value;
          DefineVarsEmpresa(FEmpresa);
          }
          FmEmpresaSel.EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
          FmEmpresaSel.CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
        end;
        DmodG.ReopenEmpresas(VAR_USUARIO, 0, nil, nil, '', False);
        FmEmpresaSel.ShowModal;
        // DModG.FEmpresa  := FmEmpresaSel.FEmpresa;
        // DModG.FEntidade := FmEmpresaSel.FEntidade;
        // Erro quando n�o seleciona
        // al�m do mais j� selecionou no FmEmpresaSel
        //DefineVarsEmpresa(FEmpresa);
      end;
      FmEmpresaSel.Destroy;
      Application.ProcessMessages;
    end;
  end;
  Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  Result := Entidade <> 0;
  if Result then
  begin
    FinNovo := DModFin.EntidadeHabilitadadaParaFinanceiroNovo(Entidade,
      FinanceiroNovo, FinanceiroNovo) = sfnNovoFin;
    if FinanceiroNovo then
      Result := FinNovo
    else
      Result := not FinNovo;
  end;
end;
{$EndIf}
{$EndIf}

procedure TDModG.SelecionaEmpresaSeUnica(EdEmpresa: TdmkEditCB;
  CBEmpresa: TdmkDBLookupComboBox; Qry: TmySQLQuery = nil;
  Campo: String = 'Filial');
begin
(*
  if QrEmpresas.State <> dsInactive then
  begin
    if EdEmpresa.ValueVariant = 0 then
    begin
      if QrEmpresas.RecordCount = 1 then
      begin
        EdEmpresa.ValueVariant := QrEmpresasFilial.Value;
        CBEmpresa.KeyValue := QrEmpresasFilial.Value;
      end;
    end;
  end;
*)
  if Qry = nil then Qry := QrEmpresas;
  //
  if Qry.State <> dsInactive then
  begin
    if EdEmpresa.ValueVariant = 0 then
    begin
      if Qry.RecordCount = 1 then
      begin
        EdEmpresa.ValueVariant := Qry.FieldByName(Campo).AsInteger;
        CBEmpresa.KeyValue := Qry.FieldByName(Campo).AsInteger;
      end;
    end;
  end;
end;

function TDModG.SelecionarCliInt(var CliInt: Integer; const Avisar: Boolean): Boolean;
const
  Aviso  = '...';
  Titulo = 'Sele��o de Empresa';
  Prompt = 'Informe a empresa:';
  Campo  = 'NomeEmp';
begin
  CliInt := 0;
  case VAR_TYPE_LOG of
    ttlNone: Geral.MB_Info(
      'Aplicativo sem cliente interno. Para ativ�-lo contate a Dermatek!');
    ttlFiliLog:
    begin
      if QrFililog.RecordCount = 1 then
        CliInt := QrFililogCodigo.Value
      else
        CliInt := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil,
          DsFililog, Campo, 0, [], Dmod.MyDB, False);
    end;
    ttlCliIntLog:
      CliInt := QrCliIntLogCodEnti.Value;
    ttlCliIntUni:
      // mudei 2011-11-04
      //CliInt := -1;
      CliInt := -11;
    else (*ttlUnknow*) Geral.MB_Erro(
    'Aplicativo sem sele��o de cliente interno. AVISE A DERMATEK!');
  end;
  Result := CliInt <> 0;
end;

procedure TDModG.SetaFilialdeEntidade(Entidade: Integer; Ed: TdmkEditCB;
  CB: TdmkDBLookupComboBox);
var
  Filial: Integer;
begin
  Filial := ObtemFilialDeEntidade(Entidade);
  Ed.ValueVariant := Filial;
  CB.KeyValue := Filial;
end;

function TDModG.SiglaDuplicada(Sigla, FldSigla, FldCodigo, FldNome, Tabela: String;
  Codigo: Variant): Boolean;
var
  SQL_CodInUpd: String;
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    SQL_CodInUpd := '';
    if Codigo <> null then
    try
      SQL_CodInUpd := 'AND ' + FldCodigo + '<>' + FloatToStr(Extended(Codigo));
    except
      //
    end;
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
    'SELECT ' + FldCodigo + ', ' + FldNome + ', ' + FldSigla,
    'FROM ' + Tabela,
    'WHERE UPPER(' + FldSigla + ')="' + AnsiUppercase(Sigla) + '"',
    SQL_CodInUpd,
    'ORDER BY ' + FldCodigo,
    '']);
    //
    Result := Qry.RecordCount > 0;
    if Result then Geral.MB_Aviso('Sigla j� cadastrada sob o c�digo ' +
    Geral.FF0(Qry.FieldByName(FldCodigo).AsInteger) + sLineBreak + 'Descri��o: "' +
    Qry.FieldByName(FldSigla).AsString + '"');
  finally
    Qry.Free;
  end;
end;

function TDModG.SoUmaEmpresaLogada(Avisa: Boolean): Boolean;
begin
  if QrFiliLog.State = dsInactive then
  begin
    Result := False;
      Geral.MB_Aviso('A��o abortada! N�o h� nenhuma empresa logada!');
  end else
  begin
    Result := QrFiliLog.RecordCount = 1;
    //
    if not Result and Avisa then
      Geral.MB_Aviso('A��o abortada! H� mais de uma empresa logada!');
  end;
end;

function TDModG.NaoPermiteExclusao(Tabela, Form: String; Msg: Integer): Boolean;
var
  Loc: Boolean;
begin
  if Msg = -2 then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER))
    or ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    then Result := False else
    Geral.MB_Aviso('Acesso Restrito');
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if Msg = 0 then
    begin
      Geral.MB_Erro('Tabela de perfis n�o definida ao verificar acesso.');
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((VAR_LOGIN = 'ADMIN') and (Uppercase(VAR_SENHA) = VAR_ADMIN) and (VAR_ADMIN <> ''))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  then Result := False
  else begin
    if DmodG.QrPerfis.State <> dsBrowse then
    begin
      Result := False;
      if (Copy(Uppercase(Application.Title), 1, 5) <> 'EXULT')
      and (Copy(Uppercase(Application.Title), 1, 5) <> 'EXUL2') then
      Geral.MB_Erro('Tabela de perfis n�o est� aberta!. Avise a Dermatek');
      Exit;
    end;
    UMyMod.AbreQuery(DmodG.QrPerfis, Dmod.MyDB);
    if not DmodG.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('INSERT INTO perfisits SET Janela=:P0');
        Dmod.QrAux.Params[0].AsString  := Form;
        Dmod.QrAux.ExecSQL;
        DmodG.Privilegios(-1000);
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (DmodG.QrPerfisLibera.Value = 0) or (Loc=False) then
    //if Form = 'F' then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MB_Aviso(
        'Somente a senha gerencial (BOSS) tem acesso a este formul�rio!')
      else begin
        case Msg of
         -1: ;// nada;
          0: Geral.MB_Aviso(VAR_ACESSONEGADO+sLineBreak+'['+
            Form+']');
          1: Geral.MB_Aviso('Login com acesso parcial!');
          2: Geral.MB_Aviso(VAR_ACESSONEGADO+sLineBreak+'['+
            Form+']');
          3: Geral.MB_Aviso('Login com acesso parcial, sem acesso a edi��o!');
        end;
      end;
    end else Result := False;
  end;
end;

function TDModG.NomeTab(DBName: String; Tab: TNomeTab; SetaVAR_LCT: Boolean;
  Letra: TTipoTab = ttA; Empresa: Integer = 0): String;
var
  N1, N2, N3, Nome: String;
  CliInt: Integer;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta fun��o mudar na WEB em:                        ///
  ///  helpers -> dmk_financeiro_helper.php -> nome_tab                     ///
  /////////////////////////////////////////////////////////////////////////////

  if Empresa = 0 then
    CliInt := EmpresaAtual_ObtemCodigo(tecCliInt)
  else CliInt := Empresa;
  //
  if CliInt = 0 then
  begin
    if VAR_FilialUnica <> 0 then
    begin
      CliInt := VAR_FilialUnica
    end else
    begin
      {
      Geral.MB_Erro('ERRO! Tabela de lan�amentos ficaria indefinida!' +
      sLineBreak + 'function TDModG.NomeTab()' + sLineBreak
      + 'O aplicativo ser� encerrado!');
      }
      raise Exception.Create(
        'ERRO! Tabela de lan�amentos ficou indefinida!' +
        sLineBreak + 'function TDModG.NomeTab()');
      Halt;
    end;
  end;
  if CliInt < 0 then
    N2 := '_' + FormatFloat('000', - CliInt)
  else
    N2 := FormatFloat('0000', CliInt);
  //

  case Letra of
    tt_: N3 := Lowercase(VAR_LETRA_LCT);
    ttA: N3 := 'a';
    ttB: N3 := 'b';
    ttD: N3 := 'd';
  end;
  //
  if not (N3[1] in (['a','b','c','d','e','f'])) then
  begin
    Geral.MB_Erro('Tipo de tabela de lan�amentos n�o definida!');
  end;
  if N3 = 'c' then
    N1 := VAR_MyPID_DB_NOME
  else
    N1 := DBName;
  if N1 <> '' then
  begin
    if N1[Length(N1)] <> '.' then
      N1 := N1 + '.';
  end;
  //
  case Tab of
    ntLct: Nome := 'lct';
    ntAri: Nome := 'ari';
    ntCns: Nome := 'cns';
    ntPri: Nome := 'pri';
    ntPrv: Nome := 'prv';
    else   Nome := 'err';
  end;
  Result := Lowercase(N1 + Nome + N2 + N3);
  //
  if SetaVAR_LCT then
  begin
    {$IFDEF DEFINE_VARLCT}
    if (CO_DMKID_APP = 19) //Academy
    or (CO_DMKID_APP = 18) //Exult
    or (CO_DMKID_APP = 6) //Planning
    or (CO_DMKID_APP = 22) //Credito2
    or (CO_DMKID_APP = 4) //Syndi2
    or (CO_DMKID_APP = 43) //Syndi3
    or (CO_DMKID_APP = 7) //LeSew
    or (CO_DMKID_APP = 2) //Bluederm
    then
      VAR_LCT := '!ERRO!'
    else
    begin
      VAR_LCT := Result;
      VAR_TAB_LCT_SEL := Result;
    end;
    {$ENDIF}
  end;
end;

procedure TDModG.TransformaCadastrosDeEntidadesEmMaiusculas();
var
 Nome, RazaoSocial: String;
begin
 Screen.Cursor := crHourGlass;
 try
   QrMaius.Close;
   UnDmkDAC_PF.AbreQueryApenas(QrMaius);
   DMod.QrUpd.SQL.Clear;
   DMod.QrUpd.SQL.Add('UPDATE entidades SET');
   DMod.QrUpd.SQL.Add('RazaoSocial=:P0, Nome=:P1 ');
   DMod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
   DMod.QrUpd.SQL.Add('');
   while not QrMaius.Eof do
   begin
     Nome := Geral.Maiusculas(QrMaiusNome.Value, False);
     RazaoSocial := Geral.Maiusculas(QrMaiusRazaoSocial.Value, False);
     //
     DMod.QrUpd.Params[00].AsString  := RazaoSocial;
     DMod.QrUpd.Params[01].AsString  := Nome;
     DMod.QrUpd.Params[02].AsInteger := QrMaiusCodigo.Value;
     DMod.QrUpd.ExecSQL;
     //
     QrMaius.Next;
   end;
   Geral.MB_Info('Processo terminado com sucesso!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDModG.UTC_ObtemDiferenca(): TTime;
begin
  QrUTC.Close;
  //QrUTC.DataBase := Dmod.MyDB;
  UnDmkDAC_PF.AbreMySQLQuery0(QrUTC, Dmod.MyDB, [
  'SELECT SYSDATE() DATA_LOC, ',
  'UTC_TIMESTAMP() DATA_UTC, ',
  'SYSDATE()- UTC_TIMESTAMP() UTC_Dif ',
  '']);
  //
  Result := QrUTCDATA_LOC.Value - QrUTCDATA_UTC.Value;
end;

procedure TDModG.ReopenEmpresas(Usuario, Empresa: Integer; EdEmpresa:
  TdmkEditCB = nil; CBEmpresa: TdmkDBLookupComboBox = nil; FiltroSQL: String = '';
  Inverte: Boolean = False; QueryEmp: TmySQLQuery = nil);
var
  //  Liga: String;
  UsuTxt: String;
  Query: TmySQLQuery;
begin
  //2015-03-17 => Feito assim por causa dos forms em aba
  //
  Query := TmySQLQuery.Create(Self);
  //
  if QueryEmp = nil then
  begin
    Query := QrEmpresas;
  end else
    Query := QueryEmp;
  //
  //Liga := ' WHERE ';
  Query.Close;
  if (VAR_LIB_EMPRESAS <> '') or (VAR_KIND_DEPTO in [kdUH, kdDpto]) then
  begin
    Query.SQL.Clear;
    Query.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
    Query.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,');
    Query.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE, ');
    Query.SQL.Add('IF(ent.Tipo = 0, ent.EUF, ent.PUF) UF, ');
    Query.SQL.Add('IF(ent.Tipo = 0, ent.ECodMunici, ent.PCodMunici) CodMunici, ');
    Query.SQL.Add('IF(ent.Tipo = 0, ent.ECodiPais, ent.PCodiPais) CodiPais ');
    Query.SQL.Add('FROM entidades ent');
    Query.SQL.Add('LEFT JOIN senhasits sei ON ent.Codigo=sei.Empresa');
    //if (Uppercase(Application.Title) = 'SYNDIC') or (Uppercase(Application.Title) = 'SYNKER') then
    if (VAR_KIND_DEPTO in [kdUH, kdDpto]) then
    begin
      Query.SQL.Add('LEFT JOIN enticliint eci ON eci.CodEnti=ent.Codigo');
      Query.SQL.Add('LEFT JOIN senhas snh ON snh.Funcionario=eci.AccManager');
      Query.SQL.Add('WHERE ent.CliInt <> 0');
      { Desabilitei em 2010-07-27
      if Trim(VAR_LIB_EMPRESAS) <> '' then
      begin
        Query.SQL.Add('AND ent.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
        //Liga := ' AND ';
      end;
      if Usuario > 0 then
      begin
        Query.SQL.Add('AND sei.Numero=:P0');
        Query.Params[0].AsInteger := Usuario;
      end;
      }
      if (VAR_TIPO_TAB_LCT > 0) then
      begin
        if Inverte then
          Query.SQL.Add('AND eci.TipoTabLct=0')
        else
          Query.SQL.Add('AND eci.TipoTabLct>0');
      end;
      //
      if DmodG.QrMasterUsaAccMngr.Value = 1 then
      begin
        if Usuario > 0 then
        begin
          // 2011-09-20
          //usar em todos forms este Query!!
          UsuTxt := FormatFloat('0', Usuario);
          Query.SQL.Add('AND (snh.Numero=' + UsuTxt +
            ' OR sei.Numero=' + UsuTxt + ')');
          (*
          Query.SQL.Add('AND sei.Numero=:P0');
          Query.Params[0].AsInteger := Usuario;
          *)
        end;
      end;
    end else begin
      Query.SQL.Add('WHERE ent.Codigo < -10');
      Query.SQL.Add('AND ent.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
      if Usuario > 0 then
      begin
        Query.SQL.Add('AND sei.Numero=:P0');
        Query.Params[0].AsInteger := Usuario;
      end;
    end;
    if FiltroSQL <> '' then
      Query.SQL.Add(FiltroSQL);
    //
    Query.SQL.Add('ORDER BY NOMEFILIAL');
    UMyMod.AbreQuery(Query, Query.DataBase, 'DModG.ReopenEmpresas()');
    //
    if Empresa <> 0 then
      Query.Locate('Codigo', Empresa, []);
    if (Query.RecordCount = 1) or (Empresa <> 0) then
    begin
      if EdEmpresa <> nil then
        EdEmpresa.ValueVariant := Query.FieldByName('Filial').AsInteger;
      if CBEmpresa <> nil then
        CBEmpresa.KeyValue := Query.FieldByName('Filial').AsInteger;
    end;
  end;
end;

procedure TDModG.VerificaCartasAImprimir(GB: TGroupBox; LaAvisoA, LaAvisoB,
              LaAvisoC: TLabel);
const
  //q = DEFAULT_QUALITY;
  //q = DRAFT_QUALITY;
  //q = PROOF_QUALITY;
  //q = NONANTIALIASED_QUALITY;
  //q = ANTIALIASED_QUALITY;
  //q = 5 (*CLEARTYPE_QUALITY*);
  q = PROOF_QUALITY;
var
  Texto: String;
begin
  UnDmkDAC_PF.AbreQuery(QrCartToPrn, Dmod.MyDB);
  if QrCartToPrnITENS.Value > 0 then
  begin
    if QrCartToPrnITENS.Value = 1 then
      Texto := 'H� uma carta de cobran�a a ser impressa!'
    else
      Texto := 'H� ' + Geral.FF0(QrCartToPrnITENS.Value) +
      ' cartas de cobran�a a serem impressas!';
    GB.Visible := True;
    GB.Color := $007C746A;
    {
    MyObjects.Entitula(GB, [LaAvisoA, LaAvisoB, LaAvisoC],
    [], Texto, True, taLeftJustify, 2, 10, 20);
    }
    LaAvisoA.Caption := Texto;
    LaAvisoB.Caption := Texto;
    LaAvisoC.Caption := Texto;
    //
    MyObjects.AjustaFonte(LaAvisoA, q);
    MyObjects.AjustaFonte(LaAvisoB, q);
    MyObjects.AjustaFonte(LaAvisoC, q);
  end
  else
    GB.Visible := False;
end;

function TDModG.VerificaDuplicidadeCodUsu(Tabela, CampoNome: String;
CamposKey: array of String; EdNome: TdmkEdit; KeyValues: array of Integer;
ImgTipo: TdmkImage; Avisa: Boolean): Boolean;
var
  i: Integer;
  Liga: String;
begin
  Result := False;
  if ImgTipo.SQLType <> stIns then
    Exit;
  //
  QrDuplCodUsu.Close;
  QrDuplCodUsu.SQL.Clear;
  QrDuplCodUsu.SQL.Add('SELECT ' + CampoNome + ' Nome FROM ' + Lowercase(Tabela));
  Liga := 'WHERE';
  for i := Low(CamposKey) to High(CamposKey) do
  begin
    QrDuplCodUsu.SQL.Add(
      Liga + ' ' + CamposKey[i] + '=' + FormatFloat('0', KeyValues[i]));
    Liga := 'AND';
  end;
  UMyMod.AbreQuery(QrDuplCodUsu, QrDuplCodUsu.DataBase, 'TDModG.VerificaDuplicidadeCodUsu()');
  if QrDuplCodUsu.RecordCount > 0 then
  begin
    Result := True;
    if Avisa then
      Geral.MB_Aviso('ATEN��O: O c�digo informado j� foi ' +
        'cadastrado anteriormente!' + sLineBreak + 'Descri��o: ' +
        QrDuplCodUsuNome.Value)
    else
      EdNome.Text := QrDuplCodUsuNome.Value;
  end;
end;

procedure TDModG.VerificaHorVerao();
var
  Qry: TmySQLQuery;
  Codigo: Integer;
  Continua: Boolean;
begin
  //Somente empresa logada
  //
  if not DModG.SoUmaEmpresaLogada(False) then Exit;
  //
  {$IfNDef SemNFe_0000}
  Continua := UnNFe_PF.VersaoNFeEmUso() > 200;
  {$Else}
  Continua := True;
  {$EndIf}
  if Continua then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT SYSDATE() Agora, emp.hVeraoAsk, emp.TZD_UTC_Auto, emp.Codigo ',
        'FROM paramsemp emp ',
        'WHERE Codigo=' + VAR_LIB_Empresas,
        '']);
      if Qry.FieldByName('TZD_UTC_Auto').AsInteger = 1 then //Autom�tico
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        //
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False,
          ['TZD_UTC'], ['Codigo'],
          [ObtemFusoHorarioServidor], [Codigo], True)
        then
          Geral.MB_Erro('Falha ao atualizar fuso hor�rio!');
      end else
      begin
        if Qry.FieldByName('Agora').AsdateTime >
          Qry.FieldByName('hVeraoAsk').AsdateTime then
        begin
{$IfNDef SemEntidade}
          if DBCheck.CriaFm(TFmHorVerao, FmHorVerao, afmoNegarComAviso) then
          begin
            FmHorVerao.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
            //
            FmHorVerao.ShowModal;
            FmHorVerao.Destroy;
          end;
{$EndIf}
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TDModG.VerificaIEsIsentos();
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, indIEDest, IE  ',
    'FROM entidades ',
    'WHERE IE <> ""',
    'AND IE<>"ISENTO" ',
    'AND indIEDest=0 ',
    'ORDER BY IE ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' cadastros de entidades com o I.E. definido mas sem defini��o do "Indicador de I.E.".'
      + sLineBreak + 'Deseja alterar seu cadastro para "Contribuinte ICMS"?'
      ) = ID_YES then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE entidades SET ',
        'indIEDest=1 ',
        'WHERE IE <> ""',
        'AND IE<>"ISENTO" ',
        'AND indIEDest=0 ',
        '']);
      end;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, indIEDest, IE  ',
    'FROM entidades ',
    'WHERE IE="ISENTO" ',
    'AND indIEDest=0 ',
    'ORDER BY IE ',
    '']);
    //
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' cadastros de entidades com o I.E. "ISENTO" mas sem defini��o do "Indicador de I.E.".'
      + sLineBreak + 'Deseja alterar seu cadastro para "Contribuinte isento de Inscri��o no cadastro de Contribuintes do ICMS"?'
      ) = ID_YES then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE entidades SET ',
        'indIEDest=2 ',
        'WHERE IE="ISENTO" ',
        'AND indIEDest=0 ',
        '']);
      end;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, indIEDest, IE ',
      'FROM entidades ',
      'WHERE Tipo=1 ', //CPF
      'AND (indIEDest=0 OR indIEDest=2)',
      'ORDER BY IE ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
        ' cadastros de entidades tipo pessoa f�sica sem defini��o do "Indicador de I.E.".'
        + sLineBreak + 'Deseja alterar seu cadastro para "Contribuinte isento de Inscri��o no cadastro de Contribuintes do ICMS"?'
        ) = ID_YES then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
          'UPDATE entidades SET ',
          'indIEDest=9 ',
          'WHERE Tipo=1 ',
          'AND (indIEDest=0 OR indIEDest=2)',
          '']);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDModG.VerificaVarsToConsts();
var
  I, Erros: Integer;
  //
  procedure IncErr(Erro: Boolean);
  begin
    if Erro then
      Erros := Erros + 1;
  end;
begin
  Erros := 0;
  //
  for I := 0 to Integer(High(TAgendaQuestao)) do
  begin
    case I of
      0: IncErr(I <> Integer(qagIndefinido));
      1: IncErr(I <> Integer(qagAvulso));
      2: IncErr(I <> Integer(qagOSBgstrl));
      else IncErr(True);
    end;
  end;
  //
  for I := 0 to Integer(High(TTypTabAtr)) do
  begin
    case I of
      0: IncErr(I <> Integer(ttaNenhum));
      1: IncErr(I <> Integer(ttaPreDef));
      2: IncErr(I <> Integer(ttaTxtFree));
      else IncErr(True);
    end;
  end;
  //
  for I := 0 to Integer(High(TAgendaAfazer)) do
  begin
    case I of
      0: IncErr(I <> Integer(aagIndefinido));
      1: IncErr(I <> Integer(aagVistoria));
      2: IncErr(I <> Integer(aagExecucao));
      3: IncErr(I <> Integer(aagEscalonamento));
      // ...
     (* deixar aagDesconhecido por �ltimo!!!! >>>>>*)
      4: IncErr(I <> Integer(aagDesconhecido));
      else IncErr(True);
    end;
  end;
  //
  for I := 0 to Integer(High(TIDFinalidadeLct)) do
  begin
    case I of
      0: IncErr(I <> Integer(idflIndefinido));
      1: IncErr(I <> Integer(idflProprios));
      2: IncErr(I <> Integer(idflCondominios));
      3: IncErr(I <> Integer(idflCursosCiclicos));
      else IncErr(True);
    end;
  end;
  //
  for I := 0 to Integer(High(TTipoReceitas)) do
  begin
    case I of
      0: IncErr(I <> Integer(dmktfrmSetrEmi_INDEFIN));
      1: IncErr(I <> Integer(dmktfrmSetrEmi_MOLHADO));
      2: IncErr(I <> Integer(dmktfrmSetrEmi_ACABATO));
      else IncErr(True);
    end;
  end;
  //
  for I := 0 to Integer(High(TTipoSourceMP)) do
  begin
    case I of
      0: IncErr(I <> Integer(dmktfrmSourcMP_INDEFIN));
      1: IncErr(I <> Integer(dmktfrmSourcMP_Ribeira)); // Controle por kg
      2: IncErr(I <> Integer(dmktfrmSourcMP_WetSome)); // Controle por m2
      3: IncErr(I <> Integer(dmktfrmSourcMP_VS_BH));   // Controle por kg do VS
      4: IncErr(I <> Integer(dmktfrmSourcMP_VS_WE));   // Controle por m2 do VS
      5: IncErr(I <> Integer(dmktfrmSourcMP_VS_FI));   // Controle por m2 do VS
      else IncErr(True);
    end;
  end;
  //
  // ...
  //
  if Erros > 0 then
  begin
    Geral.MB_ERRO('Erro fatal em "DModG.VerificaVarsToConsts()"');
    Halt(0);
  end;
end;

(*
function TDModG.VersaoNFe(Empresa: Integer): Integer;
begin
  ReopenParamsEmp(Empresa);
  Result := Trunc((QrParamsEmpversao.Value *100) + 0.5);
end;
*)

procedure TDModG.SalvaPosicaoColunasDmkDBGrid(Usuario: Integer;
  RestauraPadrao: Boolean);
var
  FormName, GradeName, FieldName: String;
  I, Codigo, Tamanho: Integer;
  DBGrid: TdmkDBGridZTO;
begin
  if MyObjects.FIC(VAR_SAVE_CFG_DBGRID_GRID = nil, nil, 'Grade n�o definida!') then Exit;
  //FormName  := NomeForm;
  FormName  := TComponent(VAR_SAVE_CFG_DBGRID_GRID.Owner).Name;
  DBGrid := TdmkDBGridZTO(VAR_SAVE_CFG_DBGRID_GRID);
  GradeName := DBGrid.Name;
  //
  if MyObjects.FIC(Length(FormName) = 0, nil, 'Formul�rio n�o definido!') then Exit;
  if MyObjects.FIC(Length(GradeName)= 0, nil, 'Grade n�o definida!') then Exit;
  if MyObjects.FIC(Usuario = 0, nil, 'Usu�rio n�o definido!') then Exit;
  //
  //Exclui antigos
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add(DELETE_FROM + ' poscolgri');
  QrAux.SQL.Add('WHERE FormName=:P0');
  QrAux.SQL.Add('AND GradeName=:P1');
  QrAux.SQL.Add('AND Usuario=:P2');
  QrAux.Params[0].AsString  := FormName;
  QrAux.Params[1].AsString  := GradeName;
  QrAux.Params[2].AsInteger := Usuario;
  QrAux.ExecSQL;
  //
  if not RestauraPadrao then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      for I := 0 to DBGrid.Columns.Count - 1 do
      begin
        Codigo    := UMyMod.BuscaEmLivreY_Def_Geral('PosColGri', 'Codigo', stIns, 0, nil);
        Tamanho   := DBGrid.Columns[I].Width;
        FieldName := DBGrid.Columns[I].FieldName;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'PosColGri', False, [
          'FormName', 'GradeName', 'Tamanho', 'FieldName', 'Usuario'
        ], ['Codigo'],
        [
          FormName, GradeName, Tamanho, FieldName, Usuario
        ], [Codigo], True) then
        begin
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

function TDModG.SalvaRegistroExcluido(SQLType: TSQLType; Tabela:
             String; Niveis: Integer; Nivel01: Extended;
             Nivel02: Extended = 0; Nivel03: Extended = 0;
             Nivel04: Extended = 0; Nivel05: Extended = 0;
             Nivel06: Extended = 0; Nivel07: Extended = 0;
             Nivel08: Extended = 0; Nivel09: Extended = 0;
             Nivel10: Extended = 0): Boolean;
begin
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctrlexcltb', False, [], [
  'Tabela', 'Niveis', 'Nivel01',
  'Nivel02', 'Nivel03', 'Nivel04',
  'Nivel05', 'Nivel06', 'Nivel07',
  'Nivel08', 'Nivel09', 'Nivel10'], [],
  [
  Tabela, Niveis, Nivel01,
  Nivel02, Nivel03, Nivel04,
  Nivel05, Nivel06, Nivel07,
  Nivel08, Nivel09, Nivel10], //[],
  True);
end;

procedure TDModG.SalvarOrdenaodascolunasdagrade2Click(Sender: TObject);
begin
  SalvaPosicaoColunasDmkDBGrid(VAR_USUARIO, False);
end;

function TDModG.VersaoNFSe(Empresa: Integer): Integer;
begin
  ReopenParamsEmp(Empresa);
  Result := Trunc((QrParamsEmpNFSeVersao.Value *100) + 0.5);
end;

procedure TDModG.AdvGlowButtonClick(Sender: TObject);
var
  Texto: WideString;
  I: Integer;
  PropList: TPropList;
begin
  Texto := '';
  I := 0;
  GetPropInfos(Sender.ClassInfo, @PropList);
  while (PropList[I] <> nil) and (I < High(PropList)) do
  begin
    try
      Texto := Texto + PropList[I].Name + ': ' + PropList[I].PropType^.Name + sLineBreak;
      Inc(I);
    except
      Geral.MB_Info(Texto);
    end;
  end;
  Geral.MB_Info(Texto);
end;

procedure TDModG.AjustaFiliaisCliInt;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=Filial');
  Dmod.QrUpd.SQL.Add('WHERE Codigo < -10');
  Dmod.QrUpd.ExecSQL;
end;

function TDModG.AllID_DB_Cria(): Boolean;
var
  Nome: String;
  {
  P, I, K: Integer;
  Fonte: String;
  }
begin
  try
    //  Configurar no IP certo para poder criar o database pessoal!
    AllID_DB.Disconnect;
    AllID_DB.Host         := Dmod.MyDB.Host;
    AllID_DB.UserName     := Dmod.MyDB.UserName;
    AllID_DB.UserPassword := Dmod.MyDB.UserPassword;
    AllID_DB.DatabaseName := Dmod.MyDB.DatabaseName;
    AllID_DB.UserName     := Dmod.MyDB.UserName;
    AllID_DB.Port         := VAR_PORTA;
    try
      Screen.Cursor := crHourGlass;
      AllID_DB.Connect;
    finally
      Screen.Cursor := crDefault;
    end;
    //
    if Trim(VAR_AllID_DB_NOME) = '' then
    begin
      {$IFNDEf SemDBLocal}
      if TLocDB <> '' then
        Nome := TLocDB
      else
        Nome := TMeuDB;
      {$Else}
        Nome := TMeuDB;
      {$EndIf}
      VAR_AllID_DB_NOME := Lowercase(Nome + 'all');
    end;
    QrUpdPID1.Close;
    QrUpdPID1.SQL.Clear;
    QrUpdPID1.SQL.Add('SHOW DATABASES LIKE "' + VAR_AllID_DB_NOME + '"');
    UnDmkDAC_PF.AbreQuery(QrUpdPID1, AllID_DB);
    //
    if QrUpdPID1.RecordCount = 0 then
    begin
      // Para garantir !!!
      //AllID_DB_Excl(VAR_AllID_DB_NOME);
      //
      QrUpdPID1.Close;
      QrUpdPID1.SQL.Clear;
      QrUpdPID1.SQL.Add('CREATE DATABASE ' + VAR_AllID_DB_NOME);
      QrUpdPID1.ExecSQL;
      //
      {$IfNDef SemBDTErceiros}
      ALL_Jan.MostraFormVerifiDBTerceiros(True);
      {$EndIf}
    end;
    //
    AllID_DB.Disconnect;
    AllID_DB.Host         := Dmod.MyDB.Host;
    AllID_DB.UserName     := Dmod.MyDB.UserName;
    AllID_DB.UserPassword := Dmod.MyDB.UserPassword;
    AllID_DB.DatabaseName := VAR_AllID_DB_NOME;
    AllID_DB.Port         := VAR_PORTA;
    AllID_DB.Connect;
    Result := True;
    //
    AID_DB_TEST := AllID_DB;
  finally
    QrUpdPID1.Close;
    QrUpdPID1.DataBase := MyPID_DB;
  end;
end;

procedure TDModG.All_ABD(DBName: String; var Entidade, CliInt: Integer;
  var DtEncer, DtMorto: TDateTime;
  var TabLctA, TabLctB, TabLctD: String);
begin
  Entidade := DModG.QrEmpresasCodigo.Value;
  if Entidade <> 0 then
  begin
    CliInt := DModG.QrEmpresasFilial.Value;
    //
    Def_EM_ABD(DBName, Entidade, CliInt, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  end else
  begin
    CliInt := 0;
    Def_EM_ABD_Limpa(DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  end;
end;

procedure TDModG.AtualizaCotacoes();
begin
  {$IfNDef SemCotacoes}
  if VAR_USUARIO > 0 then
  begin
    QrUsuCot.Close;
    QrUsuCot.Params[0].AsInteger := VAR_USUARIO;
    UnDmkDAC_PF.AbreQueryApenas(QrUsuCot);
    //
    if QrUsuCot.RecordCount > 0 then
    begin
      QrTemCot.Close;
      QrTemCot.Params[0].AsString := Geral.FDT(Date, 1);
      UnDmkDAC_PF.AbreQueryApenas(QrTemCot);
      //
      if QrTemCot.RecordCount = 0 then
      begin
        if DBCheck.CriaFm(TFmCambioCot, FmCambioCot, afmoNegarComAviso) then
        begin
          FmCambioCot.ShowModal;
          FmCambioCot.Destroy;
        end;
      end;
    end;
  end;
  {$EndIf}
end;

procedure TDModG.AtualizaEntiConEnt();
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrProvisorio1, Dmod.MyDB, [
    'SELECT ece.Codigo NULO, ect.Codigo, ect.Controle  ',
    'FROM enticontat ect ',
    'LEFT JOIN enticonent ece ON  ',
    '  ece.Controle=ect.Controle ',
    '  AND ece.Codigo=ect.Codigo ',
    'WHERE ece.Codigo IS NULL ',
    ' ']);
    //
    QrProvisorio1.First;
    while not QrProvisorio1.Eof do
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticonent', False, [], [
      'Codigo', 'Controle'], [], [
      QrProvisorio1Codigo.Value, QrProvisorio1Controle.Value], True);
      //
      QrProvisorio1.Next;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TDModG.BuscaProximoInteiro(Tabela, Campo: String;
  CampoTipo: String = ''; Tipo: Integer = 0;
  Campo2: String = ''; Valr2: Integer = 0;
  Campo3: String = ''; Valr3: Integer = 0): Integer;
begin
  QrNexInt.Close;
  //
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('LOCK TABLES ' + lowercase('livres') + ' WRITE, ' +
    lowercase('controle') + ' WRITE, '+lowercase(Tabela)+' WRITE;');
  QrNexInt.ExecSQL;
  //
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('SELECT MAX(' + Campo + ') Codigo');
  QrNexInt.SQL.Add('FROM ' + Lowercase(Tabela));
  if Trim(CampoTipo) <> '' then
  begin
    QrNexInt.SQL.Add('WHERE ' + CampoTipo + '=' + FormatFloat('0', Tipo));
    if Campo2 <> '' then
      QrNexInt.SQL.Add(' AND ' + Campo2 + '=' + FormatFloat('0', Valr2));
    if Campo3 <> '' then
      QrNexInt.SQL.Add(' AND ' + Campo3 + '=' + FormatFloat('0', Valr3));
  end;
  UMyMod.AbreQuery(QrNexInt, QrNexInt.Database, 'TDModG.BuscaProximoInteiro()');
  //
  Result := QrNexIntCodigo.Value + 1;
  //
  QrNexInt.Close;
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('UNLOCK TABLES;');
  QrNexInt.ExecSQL;
  //
end;

// Procura em livres antes!
function TDModG.BuscaProximoInteiro_A(Tabela, Campo: String): Integer;
const
  TabLivre = 'livres';
begin
  Result := 0;
  QrNexInt.Close;
  //
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    Lowercase('controle') + ' WRITE, ' + Lowercase(Tabela)+' WRITE;');
  QrNexInt.ExecSQL;
  //
  //QrNexInt.Close;
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('SELECT Codigo FROM ' + Lowercase(TabLivre) + ' WHERE Tabela=:Table');
  QrNexInt.SQL.Add('ORDER BY Codigo');
  QrNexInt.Params[0].AsString := Lowercase(Tabela);
  UnDmkDAC_PF.AbreQueryApenas(QrNexInt);
  if QrNexInt.FieldByName('Codigo').AsInteger > 0 then
  begin
    Result := QrNexInt.FieldByName('Codigo').AsInteger;
    QrNexInt.Close;
    QrNexInt.SQL.Clear;
    QrNexInt.SQL.Add(DELETE_FROM + Lowercase(TabLivre)+' WHERE Tabela=:P0');
    QrNexInt.SQL.Add('AND Codigo=:P1');
    QrNexInt.Params[0].AsString := Lowercase(Tabela);
    QrNexInt.Params[1].AsInteger := Result;
    QrNexInt.ExecSQL;
    //Novo// Evitar duplica��o
    QrNexInt.SQL.Clear;
    QrNexInt.SQL.Add('SELECT ' + Campo + ' Codigo FROM '+ Lowercase(Tabela)+'');
    QrNexInt.SQL.Add('WHERE ' + Campo + '=:P0');
    QrNexInt.Params[0].AsInteger := Result;
    UnDmkDAC_PF.AbreQueryApenas(QrNexInt);
    //
    if QrNexInt.RecordCount > 0 then
      Result := 0;
  end;
  QrNexInt.Close;
  if Result = 0 then
  begin
    QrNexInt.SQL.Clear;
    QrNexInt.SQL.Add('SELECT MAX(' + Campo + ') Codigo');
    QrNexInt.SQL.Add('FROM ' + Lowercase(Tabela));
    {
    if Trim(CampoTipo) <> '' then
    begin
      QrNexInt.SQL.Add('WHERE ' + CampoTipo + '=' + FormatFloat('0', Tipo));
      if Campo2 <> '' then
        QrNexInt.SQL.Add(' AND ' + Campo2 + '=' + FormatFloat('0', Valr2));
      if Campo3 <> '' then
        QrNexInt.SQL.Add(' AND ' + Campo3 + '=' + FormatFloat('0', Valr3));
    end;
    }
    UMyMod.AbreQuery(QrNexInt, QrNexInt.DataBase, 'TDModG.BuscaProximoInteiro()');
    //
    Result := QrNexIntCodigo.Value + 1;
  end;
  //
  QrNexInt.Close;
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('UNLOCK TABLES;');
  QrNexInt.ExecSQL;
end;

function TDModG.BuscaProximoInteiro_Negativo(DB: TmySQLDataBase; Tabela, Campo, CampoTipo: String;
  Tipo: Integer): Integer;
begin
  QrNexInt.Close;
  QrNexInt.Database := DB;
  QrNexInt.SQL.Clear;
  QrNexInt.SQL.Add('SELECT MIN(' + Campo + ') Codigo');
  QrNexInt.SQL.Add('FROM ' + Lowercase(Tabela));
  if Trim(CampoTipo) <> '' then
    QrNexInt.SQL.Add('WHERE ' + CampoTipo + '=' + FormatFloat('0', Tipo));
  UnDmkDAC_PF.AbreQueryApenas(QrNexInt);
  //
  if QrNexIntCodigo.Value >= 1 then
    Result := -1
  else
    Result := QrNexIntCodigo.Value - 1;
end;

function TDModG.DadosRetDeEntidade(const Entidade: Integer; var CliInt: Integer;
  const CNAB_Cfg: Integer = 0): Boolean;
begin
  QrLocCI.Close;
  QrLocCI.SQL.Clear;
  (* Foi migrado para o cnab_cfg => 01/02/2016
  if VAR_KIND_DEPTO = kdUH then
  begin
    QrLocCI.SQL.Add('SELECT Codigo, PercJuros, PercMulta, VTCBBNITAR');
    QrLocCI.SQL.Add('FROM cond');
    QrLocCI.SQL.Add('WHERE Cliente=:P0');
    QrLocCI.Params[0].AsInteger := Entidade;
  end else
  *)
  if (CO_DMKID_APP = 3) or (CO_DMKID_APP = 22) then //Creditor ou Credito2
  begin
    QrLocCI.SQL.Add('SELECT Codigo, JuroSacado PercJuros,  MultaPerc ');
    QrLocCI.SQL.Add('PercMulta, 0.00 VTCBBNITAR');
    QrLocCI.SQL.Add('FROM entidades ');
    QrLocCI.SQL.Add('WHERE Codigo=:P0');
    QrLocCI.Params[0].AsInteger := Entidade;
  end else
  begin
    QrLocCI.SQL.Add('SELECT cin.CodFilial Codigo, cfg.JurosPerc PercJuros, ');
    QrLocCI.SQL.Add('cfg.MultaPerc PercMulta, VTCBBNITAR');
    QrLocCI.SQL.Add('FROM cnab_cfg cfg');
    QrLocCI.SQL.Add('LEFT JOIN enticliint cin ON cin.CodEnti = cfg.Cedente');
    QrLocCI.SQL.Add('WHERE cfg.Cedente=:P0');
    QrLocCI.SQL.Add('AND cfg.Codigo=:P1');
    QrLocCI.SQL.Add('');
    QrLocCI.SQL.Add('UNION');
    QrLocCI.SQL.Add('');
    QrLocCI.SQL.Add('SELECT cin.CodFilial Codigo, cfg.JurosPerc PercJuros,');
    QrLocCI.SQL.Add('cfg.MultaPerc PercMulta, VTCBBNITAR');
    QrLocCI.SQL.Add('FROM cnab_cfg cfg');
    QrLocCI.SQL.Add('LEFT JOIN enticliint cin ON cin.CodEnti = cfg.SacadAvali');
    QrLocCI.SQL.Add('WHERE cfg.SacadAvali=:P2');
    QrLocCI.SQL.Add('AND cfg.Codigo=:P3');
    QrLocCI.Params[0].AsInteger := Entidade;
    QrLocCI.Params[1].AsInteger := CNAB_Cfg;
    QrLocCI.Params[2].AsInteger := Entidade;
    QrLocCI.Params[3].AsInteger := CNAB_Cfg;
  end;
  UMyMod.AbreQuery(QrLocCI, QrLocCI.Database, 'TDModG.DadosRetDeEntidade()');
  if QrLocCI.RecordCount = 1 then
  begin
    CliInt := QrLocCICodigo.Value;
    Result := True;
  end else begin
    Result := False;
    CliInt := 0;
    Geral.MB_Erro('ERRO! Foram encontrados ' +
      Geral.FF0(QrLocCI.RecordCount) +
      ' clientes internos para a entidade n� ' + Geral.FF0(Entidade) + '!');
  end;
  // A T E N � � O  ! ! !
  // N�o Fechar por cousa de juros e multa
end;

procedure TDModG.DataModuleCreate(Sender: TObject);
var
  Campos: String;
  Compo: TComponent;
  Query: TmySQLQuery;
  I(*, N*): Integer;
//  Mensagem: WideString;
begin
  VAR_SAVE_CFG_DBGRID_MENU := PMSaveCfgMenu;
  VAR_SAVE_CFG_DBGRID_DMKV := dmkVarCfgMenu;
  {
  if not VAR_DIRETIVAS_COMPILER_VERSION_DEFINIDAS then
    Geral.MB_ERRO('DIRETIVAS DE COMPILA��O N�O DEFINIDAS!' + sLineBreak +
    'Implementar dmkPF.AcoesAntesDeIniciarApp_dmk() no FmPrincipal!');
  }
  //
  FCriouDB := USQLDB.TabelaExiste('controle', (*Dmod.QrAux,*) Dmod.MyDB);
  //
  VAR_SEQ_ADV_COMPOS := 0;
  //N := 0;
  //
  if DBDmk.Connected then
    Geral.MB_Erro('DBDmk est� connectado antes da configura��o!');

    //

    //
  if MyPID_DB.Connected then
    Geral.MB_Aviso('MyPID_DB est� connectado antes da configura��o!');
    //
  if MyPID_CompressDB.Connected then
    Geral.MB_Aviso('MyPID_CompressDB est� connectado antes da configura��o!');
    //
  if MyCompressDB.Connected then
    Geral.MB_Aviso('MyCompressDB est� connectado antes da configura��o!');
    //
  if MySyncDB.Connected then
    Geral.MB_Aviso('MySincDB est� connectado antes da configura��o!');

    //

  if MyPID_DB.DatabaseName <> 'mysql' then Geral.MB_Aviso(
    'MyPID_DB est� com DatabaseName default diferente do obrigat�rio!');

  if MySyncDB.DatabaseName <> 'mysql' then Geral.MB_Aviso(
    'MySyncDB est� com DatabaseName default diferente do obrigat�rio!');

    //

  MyLocDB.LoginPrompt  := False;
  DBDmk.LoginPrompt    := False;
  MyPID_DB.LoginPrompt := False;
  MySyncDB.LoginPrompt := False;
  AllID_DB.LoginPrompt := False;

  if Dmod = nil then
  begin
    Geral.MB_Erro('� necess�rio criar o "DMod" antes do "ModuleGeral"');
    Halt(0);
  end;
  if VAR_TYPE_LOG = ttlUnknow then
    Geral.MB_Erro(
    '� necess�rio definir o tipo de sele��o de cliente interno. AVISE A DERMATEK!');
  //
  MyLocDB.Connected := False;
  MyLocDB.DatabaseName := '';
  //
  MyPID_DB.Connected := False;
  MyPID_DB.DatabaseName := '';
  //
  MyCompressDB.Connected := False;
  MyCompressDB.DatabaseName := '';
  //
  MyPID_CompressDB.Connected := False;
  MyPID_CompressDB.DatabaseName := '';
  //
  MySyncDB.Connected := False;
  MySyncDB.DatabaseName := '';
  //
  FSerialKey := Lic_Dmk.CalculaValSerialKey();
  FSerialNum := Lic_Dmk.CalculaValSerialNum();
  //
  // Evitar desconfigura��o espont�nea da IDE do Delphi
  for I := 0 to DModG.ComponentCount - 1 do
  begin
    Compo := DModG.Components[I];
    if Compo is TmySQLQuery then
    begin
      Query := TmySQLQuery(Compo);
      // All
      if Query.Name = 'QrAllUpd' then
        Query.Database := AllID_DB
      else
      if Query.Name = 'QrAllAux' then
        Query.Database := AllID_DB
      else
      //  User
      if Query.Name = 'QrUpdPID1' then
        Query.Database := MyPID_DB
      else
      if Query.Name = 'QrUpdPID2' then
        Query.Database := MyPID_DB
      else
      if Query.Name = 'QrProtoMail' then
        Query.Database := MyPID_DB
      else
      if Query.Name = 'QrSelCods' then
        Query.Database := MyPID_DB
      else
      if Query.Name = 'QrUpdL' then
        Query.Database := MyLocDB
      else
      if Query.Name = 'QrAux_' then
        Query.Database := MyLocDB
      //
      // Site dermatek
      else
      if Query.Name = 'QrCliAplic' then
        Query.Database := DBDmk
      else
      if Query.Name = 'QrCliAplicLi' then
        Query.Database := DBDmk
      else
      if Query.Name = 'QrEntDmk' then
        Query.Database := DBDmk
      else
      if Query.Name = 'QrWAgora' then
        Query.Database := DBDmk
      // Fim site dermatek
      //
      else
        Query.Database := Dmod.MyDB;

    end;
  end;
  //
  QrUpdSync.Database := DModG.MySyncDB;
  /////////////////// S I T U A � � O //////////////////////////////////////////
  QrSituacao.Close;
  QrSituacao.SQL.Add('DROP TABLE situacao; ');
  QrSituacao.SQL.Add('CREATE TABLE situacao (');
  QrSituacao.SQL.Add('  Codigo integer     ,');
  QrSituacao.SQL.Add('  Nome   varchar(30) ');
  QrSituacao.SQL.Add(');');
  //
  Campos := 'INSERT INTO situacao (Codigo,Nome) VALUES (';
  QrSituacao.SQL.Add(Campos + '1,"Bloqueado");');
  QrSituacao.SQL.Add(Campos + '2,"Liberado");');
  QrSituacao.SQL.Add(Campos + '3,"Faturado total");');
  QrSituacao.SQL.Add(Campos + '4,"Faturado parcial");');
  QrSituacao.SQL.Add(Campos + '5,"Em an�lise");');
  QrSituacao.SQL.Add(Campos + '6,"Cancelado");');
  //
  QrSituacao.SQL.Add('SELECT * FROM situacao ORDER BY Nome;');
  DmkABS_PF.AbreQuery(QrSituacao);
  //
  QrFiliaisSP.Database := Dmod.MyDB;
  QrCambioMda.Database := Dmod.MyDB;
  QrFiliLog.Database   := Dmod.MyDB;
  QrCliIntLog.Database := Dmod.MyDB;
  QrPTK.Database       := Dmod.MyDB;
  QrPPI.Database       := Dmod.MyDB;
  //
  FPTK_ID_Cod1 := 0;
  FPTK_ID_Cod2 := 0;
  FPTK_ID_Cod3 := 0;
  FPTK_ID_Cod4 := 0;
  FFiltroPPI   := 0;
  //
  DMod.ZZDB.Connected    := False;
  DMod.ZZDB.DatabaseName := DMod.MyDB.DatabaseName;
  DMod.ZZDB.Host         := DMod.MyDB.Host;
  DMod.ZZDB.Port         := VAR_PORTA;
  DMod.ZZDB.UserName     := DMod.MyDB.UserName;
  DMod.ZZDB.UserPassword := DMod.MyDB.UserPassword;
  try
    DMod.ZZDB.Connected    := True;
  except
    Geral.MB_Aviso(
    'N�o foi poss�vel conectar o "ZZDB" pela configura��o do "MyDB":' +
    sLineBreak + sLineBreak+
    'Database: ' + DMod.MyDB.DatabaseName + sLineBreak +
    'Host: ' + DMod.MyDB.Host + sLineBreak +
    'Porta: ' + FormatFloat('0', VAR_PORTA));
  end;
  AllID_DB_Cria();
  //
  if FCriouDB then
  begin
    ReopenDono();
    {$IfNDef SemCtrlGeral}
    ReopenCtrlGeral();
    {$EndIf}
    AtualizaCambioMda();
    //
    ReopenCambio();
  end;
  VerificaVarsToConsts();
  ConfiguraMaxTimeIdle();
end;

procedure TDModG.ConfiguraMaxTimeIdle;
var
  IdleMinutos, IdlePingMin: Integer;
begin
  if ReopenControle() then
  begin
    //
    IdleMinutos := QrControle.FieldByName('IdleMinutos').AsInteger;
    IdlePingMin := Geral.ReadAppKeyLM('IdlePingMin', Application.Title, ktInteger, 10);
    //
    if IdleMinutos <> 0 then
    begin
      VAR_LOG_TIMEIDLE := True;
      VAR_MAX_TIMEIDLE := IdleMinutos;
    end else
    begin
      VAR_LOG_TIMEIDLE := False;
      VAR_MAX_TIMEIDLE := IdlePingMin;
    end;
    //
    if VAR_MAX_TIMEIDLE <> 0 then
      VAR_MAX_TIMEIDLE := VAR_MAX_TIMEIDLE * 60;
  end;
end;

{$IfNDef SemCtrlGeral}
procedure TDModG.ReopenCtrlGeral();
begin
  QrCtrlGeral.Close;
  UnDmkDAC_PF.AbreQueryApenas(QrCtrlGeral);
end;
{$EndIf}

procedure TDModG.DataModuleDestroy(Sender: TObject);
begin
  // N�o funciona !!!
  {
  if VAR_MyPID_DB_NOME <> '' then
    MyPID_DB_Excl(VAR_MyPID_DB_NOME);
  }
end;

procedure TDModG.DefineDataMinima(Entidade: Integer);
begin
  QrCerrado.Close;
  QrCerrado.Params[0].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQueryApenas(QrCerrado);
  //
  VAR_DATA_MINIMA := Trunc(QrCerradoData.Value) + 1;
end;

function TDModG.DefineFretePor(const PreDef: Integer; var FretePor,
  modFrete: Integer): Boolean;
begin
  FretePor := PreDef;
  modFrete := FretePor;
  Result := FretePor in ([0,1,2,9]);
  if not Result then
    Geral.MB_Aviso('Modo de frete (frete por) n�o definido!');
end;

procedure TDModG.DefineServidorEIP();
begin
  // Servidor
  if VAR_SERVIDOR = 0 then  // se ainda n�o estiver setado
    VAR_SERVIDOR := Geral.ReadAppKeyLM('Server', Application.Title, ktInteger, 0);
  if not VAR_SERVIDOR3 in [1,2,3] then VAR_SERVIDOR3 := 2;
  if not (VAR_SERVIDOR in [1,2,VAR_SERVIDOR3]) then
    MyObjects.FormShow(TFmServidor, FmServidor);

  // IP
  if VAR_IP = CO_VAZIO then  // se ainda n�o estiver setado
    VAR_IP := Geral.ReadAppKeyLM('IPServer', Application.Title, ktString, CO_VAZIO);
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKeyLM2('IPServer', Application.Title, VAR_IP, ktString);
      end else begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
end;

function TDModG.DefineTextoTipoRPS(Codigo: Integer): String;
begin
  case Codigo of
    0: Result := '';
    1: Result := 'RPS';
    2: Result := 'Nota Fiscal Conjugada (Mista)';
    3: Result := 'Cupom';
    else Result := '? ? ?'
  end;
end;

// Define tabelas lct e datas de encerramentos!
function TDModG.Def_EM_ABD(DBName: String; const Entidade, CliInt: Integer;
var DtEncer, DtMorto: TDateTime;
var TabLctA, TabLctB, TabLctD: String): Boolean;
begin
  ReopenDtEncerMorto(Entidade);
  DtEncer  := QrLastEncerData.Value;
  DtMorto  := QrLastMortoData.Value;
  TabLctA  := NomeTab(DBName, ntLct, False, ttA, CliInt);
  TabLctB  := NomeTab(DBName, ntLct, False, ttB, CliInt);
  TabLctD  := NomeTab(DBName, ntLct, False, ttD, CliInt);
  //
  Result := True;
end;

// Define tabelas lct e datas de encerramentos!
function TDModG.Def_EM_ABD_Limpa(var DtEncer, DtMorto:
TDateTime; var TabLctA, TabLctB, TabLctD: String): Boolean;
begin
  DtEncer  := 0;
  DtMorto  := 0;
  TabLctA  := '';
  TabLctB  := '';
  TabLctD  := '';
  //
  Result := True;
end;

// Define tabelas FatID
function TDModG.Def_EM_xID(const TabLctA, TabLctB, TabLctD, PreID: String;
var TabID_A, TabID_B, TabID_D: String): Boolean;
begin
  if Length(TabLctA) > 3 then
    TabID_A := Geral.Substitui(TabLctA, 'lct', PreID)
  else
    TabID_A := '';
  //
  if Length(TabLctB) > 3 then
    TabID_B := Geral.Substitui(TabLctB, 'lct', PreID)
  else
    TabID_B := '';
  //
  if Length(TabLctB) > 3 then
    TabID_D := Geral.Substitui(TabLctD, 'lct', PreID)
  else
    TabID_D := '';
  //
  Result := True;
end;

procedure TDModG.dmkVarCfgMenuChange(Sender: TObject);
begin
  DModG.CarregaPosicaoColunasDmkDBGrid(VAR_USUARIO);
end;

function TDModG.EhOServidor(): Boolean;
begin
  DefineServidorEIP();
  //
  Result := (VAR_SERVIDOR = 2) or
  ( VAR_IP = '127.0.0.1') or
  ( VAR_IP = 'localhost') or
  (VAR_IP = Geral.ObtemIP(1));
end;

function TDModG.EmpresaAtual_ObtemCodigo(TipoCod: TTipoEntiCod): Integer;
begin
  case TipoCod of
    tecEntidade : Result := FEntidade;
    tecCliInt   : Result := FCliInt;
    tecFilial   : Result := FFilial;
    else          Result := 0;
  end;
end;

function TDModG.EmpresaAtual_ObtemNome(): String;
begin
  Result := FEntNome;
end;

function TDModG.EmpresaAtual_SetaCodigos(Codigo: Integer;
  TipoCod: TTipoEntiCod; MostraMsg: Boolean): Boolean;
var
  SQL: String;
begin
  Result := False;
  case TipoCod of
    tecEntidade: SQL := 'WHERE Codigo=';
    tecCliInt:   SQL := 'WHERE CliInt=';
    tecFilial:   SQL := 'WHERE Codigo <-10 AND Filial='
  end;
  SQL := SQL + FormatFloat('0', Codigo);
  //
  if Codigo <> 0 then
  begin
    QrDefEnti.Close;
    QrDefEnti.SQL.Clear;
    QrDefEnti.SQL.Add('SELECT Codigo, Filial, CliInt,');
    QrDefEnti.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NO_ENT');
    QrDefEnti.SQL.Add('FROM entidades');
    QrDefEnti.SQL.Add(SQL);
    UnDmkDAC_PF.AbreQueryApenas(QrDefEnti);
    //
    FEntidade := QrDefEntiCodigo.Value;
    FCliInt   := QrDefEntiCliInt.Value;
    FFilial   := QrDefEntiFilial.Value;
    FEntNome  := QrDefEntiNO_ENT.Value;
    Result := True;
  end else begin
    FEntidade := 0;
    FCliInt   := 0;
    FFilial   := 0;
    FEntNome  := '? ? ?';
    //
    if MostraMsg then Geral.MB_Erro(
    'N�o foi poss�vel definir o cliente interno (empresa ou filial)' + sLineBreak +
    'function TDModG.EmpresaAtual_SetaCodigos()');
  end;
end;

function TDModG.EmpresaLogada(Empresa: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to VAR_LIB_ARRAY_EMPRESAS_CONTA do
  begin
    if Empresa = VAR_LIB_ARRAY_EMPRESAS_LISTA[I] then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TDModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(const Data: TDateTime;
  var TZD_UTC: Double): Boolean;
var
  Qry: TmySQLQuery;
  //Hoj,
  Ini, Fim: TDateTime;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SYSDATE() Agora, emp.hVeraoIni, ',
    'emp.hVeraoFim, emp.TZD_UTC ',
    'FROM paramsemp emp ',
    'WHERE Codigo=' + VAR_LIB_Empresas,
    '']);
(*
    Hoj := Qry.FieldByName('Agora').AsdateTime;
    Ini := Qry.FieldByName('hVeraoIni').AsdateTime;
    Fim := Qry.FieldByName('hVeraoFim').AsdateTime;
*)
    Ini := Qry.FieldByName('hVeraoIni').AsdateTime;
    Fim := Qry.FieldByName('hVeraoFim').AsdateTime;
    //
    Result :=
      (  (Data > Ini) and (Data < Fim + 1)  ) or
      (  (Data > Ini) and (Ini > Fim + 1)  );
    //
    TZD_UTC := Qry.FieldByName('TZD_UTC').AsFloat;
    if Result then
      TZD_UTC := TZD_UTC + (1 / 24);  // 1 hora
  finally
    Qry.Free;
  end;
end;

procedure TDModG.esteXLS1Click(Sender: TObject);
const
  Dir = 'C:\Dermatek\Temp\Grid\';
  Arq = 'GradeZTO.xls';
  Exclui = True;
var
  Texto, FormName, GradeName, FieldName: String;
  I, Codigo, Tamanho: Integer;
  DBGrid: TdmkDBGridZTO;
begin
  if MyObjects.FIC(VAR_SAVE_CFG_DBGRID_GRID = nil, nil, 'Grade n�o definida!') then Exit;
  //FormName  := NomeForm;
  FormName  := TComponent(VAR_SAVE_CFG_DBGRID_GRID.Owner).Name;
  DBGrid := TdmkDBGridZTO(VAR_SAVE_CFG_DBGRID_GRID);
  GradeName := DBGrid.Name;
  //
  if MyObjects.FIC(Length(FormName) = 0, nil, 'Formul�rio n�o definido!') then Exit;
  if MyObjects.FIC(Length(GradeName)= 0, nil, 'Grade n�o definida!') then Exit;
  //if MyObjects.FIC(Usuario = 0, nil, 'Usu�rio n�o definido!') then Exit;
  //
  Texto := '';
  ForceDirectories(Dir);
  for I := 0 to DBGrid.Columns.Count - 1 do
  begin
    if Assigned(DBGrid.Columns[I]) then
      Texto := Texto + DBGrid.Columns[I].Title.Caption + #9;
  end;
  Texto := Texto + sLineBreak;
  //
  DBGrid.DataSource.DataSet.First;
  while not DBGrid.DataSource.DataSet.Eof do
  begin
    for I := 0 to DBGrid.FieldCount - 1 do
    begin
      Texto := Texto + DBGrid.Fields[i].asString + #9;
    end;
    //
    Texto := Texto + sLineBreak;
    DBGrid.DataSource.DataSet.Next;
  end;
  //
  Geral.SalvaTextoEmArquivo(Dir + Arq, Texto, Exclui);
  ShellExecute(Application.Handle, PChar('open'), PChar(Dir + Arq),
    PChar(''), nil, SW_NORMAL);
end;

procedure TDModG.ExecutaPing(FmMLA: TForm; DataBase: array of TmySQLDatabase;
  Memo1: TMemo = nil);
begin
  if VAR_MAX_TIMEIDLE <> 0 then
  begin
    VAR_TIMEIDLE := dmkPF.ObtemTempoInativoDoAplicativo;
    //
    if Memo1 <> nil then
      Memo1.Text := Geral.FFI(VAR_TIMEIDLE);
    //
    if VAR_TIMEIDLE = VAR_MAX_TIMEIDLE then
    begin
      if VAR_LOG_TIMEIDLE = True then
      begin
        ALL_Jan.MostraFormLogOff(FmMLA, DataBase);
      end else
      begin
        try
          USQLDB.ExecutaPingServidor(DataBase);
        except
          ;
        end;
      end;
    end;
  end;
end;

procedure TDModG.HabilitaEdCBEmpresa(EdEmpresa: TdmkEditCB;
  CBEmpresa: TdmkDBLookupComboBox; SQLType: TSQLType; WinControl: TWinControl);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
  if SQLType = stIns then
  begin
    SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
    if WinControl <> nil then
      if EdEmpresa.ValueVariant > 0 then
      begin
        if WinControl.CanFocus and WinControl.Enabled then
        try
          WinControl.SetFocus;
        except
          ;
        end;
      end;
  end;
end;

{$IFDEF UsaWSuport}
procedure TDModG.ImportaBancos(Memo3: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Integer;
begin
  Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_BANCOS, 'Bancos',
    Geral.SoNumero_TT(QrMasterCNPJ.Value), Dmod.QrControle.FieldByName('VerBcoTabs').AsInteger, 0,
    ObtemAgora(), Memo3, dtSQLs, Versao, ArqNome, False);
  if Res then
  begin
    Application.CreateForm(TFmRestaura2, FmRestaura2);
    FmRestaura2.Show;
    FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
    FmRestaura2.FDB := DMod.MyDB;
    FmRestaura2.BtDiretoClick(Self);
    FmRestaura2.Close;
    //
    //Atualiza vers�o no banco de dados
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE controle SET VerBcoTabs=' + Geral.FF0(Versao),
      '']);
    //
    Dmod.ReopenControle;
  end;                  
end;
{$ENDIF}

function TDModG.ListaDeItensDeBalancete(TipoCfgRel: TTipoCfgRel;
  AddNenhum: Boolean; Lista: TStrings): Boolean;
begin
  Result := False;
  Lista.Clear;
  case TipoCfgRel of
    tcrBalanceteLoc:
    begin
      if AddNenhum then
        Lista.Add('[Nenhum]');
{01}      Lista.Add('Capa');
{02}      Lista.Add('Parecer dos revisores');
{03}      Lista.Add('EACUE� pelo m�s (data) da entrada');
{04}      Lista.Add('EACUE� pelo m�s de compet�ncia');
{05}      Lista.Add('Extrato das contas correntes no per�odo');
{06}      Lista.Add('Saldos das contas do plano de contas com saldo controlado');
{07}      Lista.Add('Saldos das contas do plano de contas');
{08}      Lista.Add('Demonstrativo de receitas e despesas no per�odo');
{09}      Lista.Add('Mensalidades pr�-estipuladas');
{10}      Lista.Add('Resultados mensais');
{11}      Lista.Add('Composi��o da provis�o efetuada para a arrecada��o');
{12}      Lista.Add('Processamento de bloquetos de cobran�a');
{13}      Lista.Add('Pend�ncias de integrantes do cliente');
{14}      Lista.Add('Reparcelamentos de integrantes no per�odo');
{15}      Lista.Add('Reparcelamentos de integrantes em aberto');
{16}      Lista.Add('Documentos compensados no per�odo');
      //
      Result := True;
    end;
  end;
end;

function TDModG.LocalizaClienteInterno(ClienteInterno: Integer;
  Aviso: String): Boolean;
begin
  Result := False;
  case VAR_TYPE_LOG of
    ttlFiliLog: Result := QrFiliLog.Locate('Codigo', ClienteInterno, []);
    ttlCliIntLog: Result := QrCliIntLog.Locate('CodEnti', ClienteInterno, []);
    ttlCliIntUni: Result := True; // ???
  end;
  if not Result and (Aviso <> '') then
    Geral.MB_Aviso(Aviso);
end;

function TDModG.MesesEntreDatas(DataIni, DataFim: TDateTime(*; n�o tem como!
  DataInteira: Boolean*)): Double;
var
  DataI, DataF: String;
  MesI, MesF: String;
begin
  (*if DataInteira then
  begin*)
    DataI := FormatDateTime('yyyy-mm-dd', DataIni);
    DataF := FormatDateTime('yyyy-mm-dd', DataFim);
  (*end else begin
    DataI := FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', DataIni);
    DataF := FormatDateTime('yyyy-mm-dd hh:nn:ss:zzz', DataFim);
  end;*)
  MesI := FormatDateTime('yyyymm', DataIni);
  MesF := FormatDateTime('yyyymm', DataFim);
  QrMeses.Close;
  QrMeses.SQL.Clear;
  QrMeses.SQL.Add('SELECT');
  QrMeses.SQL.Add('((DAY(LAST_DAY("' + DataI + '")) + 0.0000)  -');
  QrMeses.SQL.Add('(DAY("' + DataI + '") + 0.0000)) /');
  QrMeses.SQL.Add('((DAY(LAST_DAY("' + DataI + '"))+0.0000)) Ini,');
  QrMeses.SQL.Add('PERIOD_DIFF(' + MesF + ',' + MesI + ')-1 + 0.0000 Meio,');
  QrMeses.SQL.Add('(DAY("' + DataF + '") + 0.0000)/');
  QrMeses.SQL.Add('(DAY(LAST_DAY("' + DataF + '")) + 0.0000) Fim');
  UnDmkDAC_PF.AbreQueryApenas(QrMeses);
  //
  Result := QrMesesIni.Value + QrMesesMeio.Value + QrMesesFim.Value;
end;

function TDModG.MezLastEncerr(): Integer;
var
  Mez: String;
begin
  Mez := Geral.DataToMez(DModG.QrLastEncerData.Value);
  if Mez = '' then
    Result := 0
  else
    Result := StrToInt(Mez);
end;

procedure TDModG.MostraBackup3(MostraMsgPermis: Boolean = True);
var
  DBName: String;
  AcesForm: TAcessFmModo;
begin
  if VAR_LOGIN <> '' then
  begin
    if MostraMsgPermis then
      AcesForm := afmoNegarComAviso
    else
      AcesForm := afmoNegarSemAviso;
    //
    if DBCheck.CriaFm(TFmBackUp3, FmBackUp3, AcesForm) then
    begin
      with FmBackUp3.DbAll do
      begin
        Connected    := False;
        Host         := DMod.MyDB.Host;
        DatabaseName := DMod.MyDB.DatabaseName;
        UserName     := DMod.MyDB.UserName;
        UserPassword := DMod.MyDB.UserPassword;
        Port         := DMod.MyDB.Port;
        Connected    := True;
      end;
      //
      FmBackup3.QrDatabases1.Close;
      UnDmkDAC_PF.AbreQueryApenas(FmBackup3.QrDatabases1);
      if TMeuDB <> '' then
      begin
        FmBackup3.QrDatabases1.First;
        while not FmBackup3.QrDatabases1.Eof do
        begin
          DBName := Lowercase(TMeuDB);
          if Lowercase(FmBackup3.QrDatabases1.Fields[0].AsString) = DBName then
          begin
            FmBackup3.CBDB1.KeyValue := FmBackup3.QrDatabases1.Fields[0].AsString;
            FmBackup3.CBDB1Click(Self);
            Break;
          end;
          //
          FmBackup3.QrDatabases1.Next;
        end;
      end;
      {
      FmBackup3.EdSenha.Text := FSenha;
      FmBackup3.EdZipPath.Text := EdZipPath.Text;
      for i := 0 to FmBackup3.ComponentCount - 1 do
      begin
        if FmBackup3.Components[i] is TmySQLQuery then
          with TButton(Components[i]) do
            case ModalResult of
              mrYes: Caption := 'Cont�nua';
              mrOK: Caption := 'Program.';
              mrNo: Caption := 'N�o cobra';
              mrCancel: Caption := 'Cancela';
            end;

        if LBDatabase.ItemIndex > -1 then
        begin
          if FmBackup3.QrDatabases1.Locate('Database', LBDatabase.Items[LBDatabase.ItemIndex], []) then
          begin
            FmBackup3.CBDB1.KeyValue := LBDatabase.Items[LBDatabase.ItemIndex];
            FmBackup3.CBDB1Click(Self);
          end;
        end;
      end;
      }
      FmBackUp3.ShowModal;
      FmBackUp3.Destroy;
    end;
  end;
end;

(* Mover (Movido?) Para UnEnti !!!!
procedure TDModG.MostraEntiImagens(Codigo, CodEnti, TipImg: Integer; MostraImagem: Boolean);
begin
  if DBCheck.CriaFm(TFmEntiImagens, FmEntiImagens, afmoNegarComAviso) then
  begin
    FmEntiImagens.ReopenEntidades(Codigo, '');
    if MostraImagem then
    begin
      FmEntiImagens.ReopenEntiImgs(Codigo, CodEnti, TipImg);
      FmEntiImagens.VisualizaImagem();
    end;
    FmEntiImagens.ShowModal;
    FmEntiImagens.Destroy;
  end;
end;
*)

function TDModG.MyPID_DB_Cria(): Boolean;
var
  P, I, K: Integer;
  Fonte, Nome: String;
begin
  try
    // Aproveita aqui para ver as fontes!
    Result := False;
    K      := 0;
    //
    for I := 0 to Screen.Fonts.Count - 1 do
    begin
      Fonte := Lowercase(Screen.Fonts[I]);
      if pos('univers', Fonte) > 0 then
        K := K + 1;
      if pos('cmc7', Fonte) > 0 then
        K := K + 1;
    end;
    K := 5 - K;
    if K > 0 then
    begin
      Geral.MB_Aviso('O aplicativo detectou a falta de ' + Geral.FF0(K) +
        ' Fontes. ' + sLineBreak +
        'Reinicie o computador e caso esta mensagem apare�a novamente AVISE A DERMATEK!');
    end;
    //
    if ZZTerminate then
      Exit;

    if (Dmod = nil) or (Dmod.MyDB.Connected = False) then
      Abort;
    //  Configurar no IP certo para poder criar o database pessoal!
    MyPID_DB.Disconnect;
    MyPID_DB.Host         := Dmod.MyDB.Host;
    MYPID_DB.UserName     := Dmod.MyDB.UserName;
    MyPID_DB.UserPassword := Dmod.MyDB.UserPassword;
    MyPID_DB.DatabaseName := Dmod.MyDB.DatabaseName;//'mysql';
    MyPID_DB.Port         := VAR_PORTA;
    try
      Screen.Cursor := crHourGlass;
      MyPID_DB.Connect;
    finally
      Screen.Cursor := crDefault;
    end;
    //
{
    //  Configurar no IP certo para poder criar o database pessoal para mySQLDirectQuery!
    MyPID_CompressDB.Disconnect;
    MyPID_CompressDB.Host         := Dmod.MyDB.Host;
    MyPID_CompressDB.UserName     := Dmod.MyDB.UserName;
    MyPID_CompressDB.UserPassword := Dmod.MyDB.UserPassword;
    MyPID_CompressDB.DatabaseName := Dmod.MyDB.DatabaseName;//'mysql';
    MyPID_CompressDB.Port         := VAR_PORTA;
    try
      Screen.Cursor := crHourGlass;
      MyPID_CompressDB.Connect;
    finally
      Screen.Cursor := crDefault;
    end;
    //
}
    if Trim(VAR_MyPID_DB_NOME) = '' then
    begin
      Nome := Geral.ReadAppKeyCU('Database', Application.Title, ktString, '');
      //
      if Nome <> '' then
      begin
        Nome := 'loc' + Nome;
      end else
      begin
        {$IFNDEf SemDBLocal}
        if TLocDB <> '' then
          Nome := TLocDB
        else
          Nome := TMeuDB;
        {$Else}
          Nome := TMeuDB;
        {$EndIf}
      end;
      if VAR_NAO_PERMITE_EXCLUIR_PID_DB then
        VAR_MyPID_DB_NOME := Lowercase(Nome + '_' +
          FormatFloat('00', VAR_USUARIO))
      else
        VAR_MyPID_DB_NOME := Lowercase(Nome + '_' +
          FormatFloat('0', VAR_USUARIO));
      //  caso for Master ou Admin (n�meros negativos)
      P := pos('-', VAR_MyPID_DB_NOME);
      if P > 0 then
        VAR_MyPID_DB_NOME[P] := '_';
    end;
    QrUpdPID1.Close;
    QrUpdPID1.SQL.Clear;
    QrUpdPID1.SQL.Add('SHOW DATABASES LIKE "' + VAR_MyPID_DB_NOME + '"');
    UnDmkDAC_PF.AbreQueryApenas(QrUpdPID1);
    //
    if QrUpdPID1.RecordCount = 0 then
    begin
      // Para garantir !!!
      if not VAR_NAO_PERMITE_EXCLUIR_PID_DB then
      begin
        MyPID_DB_Excl(VAR_MyPID_DB_NOME);
        //
        QrUpdPID1.Close;
        QrUpdPID1.SQL.Clear;
        QrUpdPID1.SQL.Add('CREATE DATABASE ' + VAR_MyPID_DB_NOME);
        QrUpdPID1.ExecSQL;
      end;
    end;
    //
    MyCompressDB.Disconnect;
    MyCompressDB.Host         := Dmod.MyDB.Host;
    MyCompressDB.UserName     := Dmod.MyDB.UserName;
    MyCompressDB.UserPassword := Dmod.MyDB.UserPassword;
    MyCompressDB.DatabaseName := Dmod.MyDB.DatabaseName;
    MyCompressDB.UserName     := Dmod.MyDB.UserName;
    MyCompressDB.Port         := Dmod.MyDB.Port;
    MyCompressDB.Connect;
    //
    MyPID_DB.Disconnect;
    MyPID_DB.Host         := Dmod.MyDB.Host;
    MYPID_DB.UserName     := Dmod.MyDB.UserName;
    MyPID_DB.UserPassword := Dmod.MyDB.UserPassword;
    MyPID_DB.DatabaseName := VAR_MyPID_DB_NOME;
    MyPID_DB.UserName     := Dmod.MyDB.UserName;
    MyPID_DB.Port         := VAR_PORTA;
    MyPID_DB.Connect;
    //
    MyPID_CompressDB.Disconnect;
    MyPID_CompressDB.Host         := Dmod.MyDB.Host;
    MyPID_CompressDB.UserName     := Dmod.MyDB.UserName;
    MyPID_CompressDB.UserPassword := Dmod.MyDB.UserPassword;
    MyPID_CompressDB.DatabaseName := VAR_MyPID_DB_NOME;
    MyPID_CompressDB.UserName     := Dmod.MyDB.UserName;
    MyPID_CompressDB.Port         := VAR_PORTA;
    MyPID_CompressDB.Connect;
    //
    Result := True;
    //
    ReopenControle();
    // 2011-09-19 - Evitar mais de um c�digo
    if QrControle.RecordCount > 1 then
    begin
      if QrControle.Locate('Codigo', 1, []) then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add(DELETE_FROM + ' Controle WHERE Codigo <> 1');
        Dmod.QrUpd.ExecSQL;
      end;
    end;
    (*
    Modificado em: 19/09/2016 => Motivo: Foi movido para o OnCreate do ModulegGeral por causa do ReopenDono
    AllID_DB_Cria();
    *)
    PID_DB_TEST := MyPID_DB;
    // Aproveita aqui para aplicativos com Grade!
    ReopenOpcoesGerl();
  {$IfNDef SemGrade}
    DmProd.ReopenOpcoesGrad();
  {$EndIf}
  {$IfNDef SemNFe_0000}
    DmNFe_0000.QrCNAE21cad.Database := AllID_DB;
  {$EndIf}
  // Tempor�rio. Eliminar quando desativar NFe 2.00 ?????
  VerificaIEsIsentos();
  finally
    ;
  end;
end;

function TDModG.MyPID_DB_Excl(Nome: String): Boolean;
var
  Continua: Boolean;
  I: Integer;
begin
  Result := False;
  Continua := False;
  if pos('_', Nome) > 0 then
  begin
    I := pos('_', Nome);
    if Geral.SoNumero_TT(Copy(Nome, I)) <> '' then
      Continua := True;
  end;
  if Continua then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrAux, DModG.MyPID_DB, [
    'DROP DATABASE IF EXISTS ' + Nome,
    '']);
    //
    Result := True;
  end else Geral.MB_Aviso('N�o � permitido excluir a base de ' +
  'dados "' + Nome + '". Avise a DERMATEK!');
end;

function TDModG.MySyncDB_Cria(): Boolean;
var
  P, I, K: Integer;
  Fonte, Nome: String;
begin
  try
    Result := False;
    //
    if ZZTerminate then
      Exit;

    if (Dmod = nil) or (Dmod.MyDB.Connected = False) then
      Abort;
    if Trim(VAR_MySyncDB_NOME) = '' then
      Exit;
    //  Configurar no IP certo
    ConectaMySyncDB(VAR_MySyncDB_NOME);
    //
  finally
    ;
  end;
end;

function TDModG.MyThreadID(): Longint;
begin
  QrThread.Close;
  UnDmkDAC_PF.AbreQueryApenas(QrThread);
  Result := QrThreadID.Value;
end;

function TDModG.ReCaptionTexto(Texto: String): String;
begin
  QrUsTx.Close;
  QrUsTx.Params[0].AsString := Texto;
  UnDmkDAC_PF.AbreQueryApenas(QrUsTx);
  //
  if QrUsTx.RecordCount > 0 then
    Result := QrUsTxTxtMeu.Value
  else
    Result := Texto;
end;

procedure TDModG.ReopenCambio;
var
  Agora: TDateTime;
begin
  {$IfNDef SemCotacoes}
  Agora := DModG.ObtemAgora();
  UnDmkDAC_PF.AbreMySQLQuery0(QrCambios, Dmod.MyDB, [
  'SELECT DataC, Codigo, Valor ',
  'FROM cambiocot cot ',
  'WHERE cot.DataC<="' + Geral.FDT(Agora, 1) + '"',
  'ORDER BY cot.DataC DESC ',
  'LIMIT 1 ']);
  VAR_CAMBIO_DATA := QrCambiosDataC.Value;
  VAR_CAMBIO_USD := 0;
  VAR_CAMBIO_EUR := 0;
  VAR_CAMBIO_IDX := 1;
  //
  if VAR_CAMBIO_DATA > 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCambios, Dmod.MyDB, [
    'SELECT DataC, Codigo, Valor ',
    'FROM cambiocot cot ',
    'WHERE cot.DataC="' + Geral.FDT(VAR_CAMBIO_DATA, 1) + '"',
    ' ']);
    QrCambios.First;
    while not QrCambios.Eof do
    begin
      case TdmkCambios(QrCambiosCodigo.Value) of
        cambiosUSD: VAR_CAMBIO_USD := QrCambiosValor.Value;
        cambiosEUR: VAR_CAMBIO_EUR := QrCambiosValor.Value;
        cambiosIDX: VAR_CAMBIO_IDX := QrCambiosValor.Value;
      end;
      //
      QrCambios.Next;
    end;
  end;
  {$EndIf}
end;

function TDModG.ReopenControle(): Boolean;
begin
  Result := False;
  if FCriouDB then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrControle, Dmod.MyDB, [
      'SELECT * ',
      'FROM controle',
      '']);
    Result := True;
  end;
end;

procedure TDModG.ReopenDono();
begin
  {$IfNDef SemEntidade}
  (*
  Mudado em 19/09/2016 => Motivo para buscar a cidade da tabela de munic�pios

  QrDono.Close;
  UnDmkDAC_PF.AbreQueryApenas(QrDono);
  *)
  UnDmkDAC_PF.AbreMySQLQuery0(QrDono, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, ECel, ',
    'Respons1, Respons2, ECEP, PCEP, EUF, PUF, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDONO, ',
    'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELIDO, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CPF, ',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, ',
    'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, ',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, ',
    'CASE WHEN en.Tipo=0 THEN en.ENumero + 0.000 ELSE en.PNumero + 0.000 END NUMERO, ',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL, ',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIRRO, ',
    'IF(mu.Nome <> "", mu.Nome, CASE WHEN en.Tipo=0 THEN en.ECidade ELSE en.PCidade END) CIDADE, ',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOMELOGRAD, ',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOMEUF, ',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais, ',
    '/*CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lograd, ',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,*/ ',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1, ',
    'CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2, ',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX, ',
    'IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) + 0.000 CODMUNICI ',
    'FROM controle co ',
    'LEFT JOIN entidades en ON en.Codigo=co.Dono ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    '']);
  {$EndIf}
end;

procedure TDModG.ReopenDtEncerMorto(Entidade: Integer);
begin
  QrLastEncer.Close;
  QrLastEncer.Params[00].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQueryApenas(QrLastEncer);
 //
  QrLastMorto.Close;
  QrLastMorto.Params[00].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQueryApenas(QrLastMorto);
end;

procedure TDModG.ReopenQrEntAtrIts(SomenteSelecionados: Boolean;
Entidade: Integer);
begin
  QrEntAtrIts.Close;
  QrEntAtrIts.SQL.Clear;
  QrEntAtrIts.SQL.Add('SELECT eac.Nome NOMEATRIBCAD, eai.Controle,');
  QrEntAtrIts.SQL.Add('eai.Codigo, eai.CodUsu, eai.Nome NOMEATRIBITS');
  QrEntAtrIts.SQL.Add('FROM entatrits eai');
  QrEntAtrIts.SQL.Add('LEFT JOIN entatrcad eac ON eac.Codigo=eai.Codigo');
  if SomenteSelecionados then
  begin
    QrEntAtrIts.SQL.Add('WHERE eac.Codigo IN (');
    QrEntAtrIts.SQL.Add('  SELECT eda.EntAtrCad');
    QrEntAtrIts.SQL.Add('  FROM entdefatr eda');
    QrEntAtrIts.SQL.Add('  WHERE eda.Entidade=' + FormatFloat('0', Entidade));
    QrEntAtrIts.SQL.Add(')');
    //
  end;
  QrEntAtrIts.SQL.Add('ORDER BY NOMEATRIBCAD, NOMEATRIBITS, eai.Codigo, eai.Controle');
  UnDmkDAC_PF.AbreQueryApenas(QrEntAtrIts);
end;

function TDModG.ObtemAgora(UTC: Boolean = False): TDateTime;
///////////////////////////////////
// ver tamb�m > ObtemDataHora    //
///////////////////////////////////
var
  Hoje: TDateTime;
  Agora: TTime;
begin
  if UTC = True then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAgora, Dmod.MyDB, [
      'SELECT YEAR(UTC_TIMESTAMP()) ANO, MONTH(UTC_TIMESTAMP()) MES, ',
      'DAYOFMONTH(UTC_TIMESTAMP()) DIA, ',
      'HOUR(UTC_TIMESTAMP()) HORA, MINUTE(UTC_TIMESTAMP()) MINUTO, ',
      'SECOND(UTC_TIMESTAMP()) SEGUNDO, UTC_TIMESTAMP() AGORA ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAgora, Dmod.MyDB, [
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES, ',
      'DAYOFMONTH(NOW()) DIA, ',
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO, ',
      'SECOND(NOW()) SEGUNDO, NOW() AGORA ',
      '']);
  end;
  Hoje   := EncodeDate(QrAgoraAno.Value, QrAgoraMes.Value, QrAgoraDia.Value);
  Agora  := EncodeTime(QrAgoraHora.Value, QrAgoraMINUTO.Value, QrAgoraSEGUNDO.Value, 0);
  Result := Hoje + Agora;
end;

function TDModG.ObtemAgoraTxt(): String;
begin
  Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', ObtemAgora());
end;

function TDModG.ObtemCaminhoImagem(TipImg, CodEnti: Integer): String;
const
  ComplCaminho = 'Imagens\Entidades\';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT Nome ',
  'FROM entiimgs ',
  'WHERE TipImg=' + Geral.FF0(TipImg),
  'AND CodEnti=' + Geral.FF0(CodEnti),
  '']);
  if QrAux.RecordCount > 0 then
    Result := DModG.QrCtrlGeralDmkNetPath.Value + ComplCaminho +
                QrAux.FieldByName('Nome').AsString
  else
    Result := DModG.QrCtrlGeralDmkNetPath.Value + ComplCaminho + 'avatar.jpg';
end;

procedure TDModG.ObtemCNPJ_CPF_IE_DeEntidade(const Entidade: Integer;
  var emit_CNPJ, emit_CPF, emit_IE: String);
begin
  ReopenEnti(Entidade);
  //
  if QrEntiTipo.Value = 0 then
  begin
    emit_CNPJ := QrEntiCNPJ.Value;
    emit_CPF  := '';
  end else begin
    emit_CNPJ := '';
    emit_CPF  := QrEntiCPF.Value;
  end;
  emit_IE := QrEntiIE.Value;
end;

function TDModG.ObtemCodigoDeEntidadePeloAntigo(Antigo: String): Integer;
var
  QrLoc: TmySQLQuery;
begin
  QrLoc := TmySQLQuery.Create(Dmod);
  try
    QrLoc.Close;
    QrLoc.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE Antigo="' + Antigo + '" ',
    'ORDER BY Codigo ',
    '']);
    //
    Result := QrLoc.FieldByName('Codigo').AsInteger;
  finally
    QrLoc.Free;
  end;
end;

function TDModG.ObtemDataHora(var Hoje: TDateTime; var Agora: TTime): Boolean;
begin
  //Result := False;
////////////////////////////////
// ver tamb�m > ObtemAgora    //
////////////////////////////////
  Hoje  := 0;
  Agora := 0;
  try
    QrAgora.Close;
    UnDmkDAC_PF.AbreQueryApenas(QrAgora);
    Hoje  := EncodeDate(QrAgoraAno.Value, QrAgoraMes.Value, QrAgoraDia.Value);
    Agora := EncodeTime(QrAgoraHora.Value, QrAgoraMINUTO.Value, QrAgoraSEGUNDO.Value, 0);
    Result := True;
  except
    raise;
  end;
end;

function TDModG.OBtemDataUltimoEncerramentoFinanceiro(
  FEntInt: Integer): TDateTime;
begin
{FAZER DESABILITADO PELO MARCELO
  QrUEFE.Close;
  QrUEFE.Params[0].AsInteger := FEntInt;
  UnDmkDAC_PF.AbreQueryApenas(QrUEFE);
  //
  Result := QrUEFEData.value;}
  Result := 0;
end;

function TDModG.BuscaProximoCodigoInt(Tabela, Campo, ComplSQL: String;
PreDefinido: Integer; Limite: Integer = High(Integer); Aviso: String = ''): Integer;
var
  TabNome: String;
begin
  if (PreDefinido <> 0) then Result := PreDefinido
  else begin
    TabNome := Lowercase(Tabela);
    //
    if Limite < High(Integer) then
    begin
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT ' + Campo + ' FROM ' + Lowercase(TabNome));
      Dmod.QrAux.SQL.Add(ComplSQL);
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      if Limite <= Dmod.QrAux.FieldByName(Campo).AsInteger then
      begin
        Result := 0;
        Geral.MB_Aviso(
        'AVISO! Foi atingido o limite permitido de numera��o para a tabela "'
        + Tabela + '"!' + sLineBreak + 'Limite permitido = ' +
        Geral.FFT(Limite, 0, siNegativo) + sLineBreak +
        '�ltima numera��o usada = ' + Geral.FFT(
        Dmod.QrAux.FieldByName(Campo).AsInteger, 0, siNegativo) + sLineBreak +
        'Identifica��o:' + sLineBreak + Aviso);
        Exit;
      end;
    end;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('LOCK TABLES ' + Lowercase(TabNome) + ' WRITE');
    Dmod.QrAux.ExecSQL;
    //
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('UPDATE ' + Lowercase(TabNome) + ' SET ' + Campo + '=' + Campo + ' + 1');
    Dmod.QrAux.SQL.Add(ComplSQL);
    Dmod.QrAux.ExecSQL;
    //
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT ' + Campo + ' FROM ' + Lowercase(TabNome));
    Dmod.QrAux.SQL.Add(ComplSQL);
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    Result := Dmod.QrAux.FieldByName(Campo).AsInteger;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('UNLOCK TABLES ');
    Dmod.QrAux.ExecSQL;
    //
  end;
end;

function TDModG.BuscaProximoCodigoInt_Novo(const Tabela, Campo, ComplSQL: String;
const PreDefinido: Integer; const Aviso: String; var ProximoCodigoInt: Integer;
const Limite: Integer = High(Integer)): Boolean;
var
  TabNome: String;
begin
  if (PreDefinido <> 0) then
  begin
    ProximoCodigoInt := PreDefinido;
    Result := True;
  end else begin
    TabNome := Lowercase(Tabela);
    //
    if Limite < High(Integer) then
    begin
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT ' + Campo + ' FROM ' + Lowercase(TabNome));
      Dmod.QrAux.SQL.Add(ComplSQL);
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      //
      if Limite <= Dmod.QrAux.FieldByName(Campo).AsInteger then
      begin
        ProximoCodigoInt := 0;
        Result := False;
        Geral.MB_Aviso(
        'AVISO! Foi atingido o limite permitido de numera��o para a tabela "'
        + Tabela + '"!' + sLineBreak + 'Limite permitido = ' +
        Geral.FFT(Limite, 0, siNegativo)  + sLineBreak +
        '�ltima numera��o usada = ' + Geral.FFT(
        Dmod.QrAux.FieldByName(Campo).AsInteger, 0, siNegativo)  + sLineBreak +
        'Identifica��o:' + sLineBreak +  Aviso);
        Exit;
      end;
    end;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('LOCK TABLES ' + Lowercase(TabNome) + ' WRITE');
    Dmod.QrAux.ExecSQL;
    //
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('UPDATE ' + Lowercase(TabNome) + ' SET ' + Campo + '=' + Campo + ' + 1');
    Dmod.QrAux.SQL.Add(ComplSQL);
    Dmod.QrAux.ExecSQL;
    //
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT ' + Campo + ' FROM ' + Lowercase(TabNome));
    Dmod.QrAux.SQL.Add(ComplSQL);
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    ProximoCodigoInt := Dmod.QrAux.FieldByName(Campo).AsInteger;
    Result := ProximoCodigoInt > 0;
    if not Result then
    Geral.MB_Aviso('AVISO! N�o foi poss�vel definir o pr�ximo n�mero ' +
    'da tabela "' + Tabela + '"!' + sLineBreak + 'Limite permitido = ' +
    Geral.FFT(Limite, 0, siNegativo) + sLineBreak +
    '�ltima numera��o usada = ' + Geral.FFT(
    Dmod.QrAux.FieldByName(Campo).AsInteger, 0, siNegativo) + sLineBreak +
    'Identifica��o:' + sLineBreak + Aviso);
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('UNLOCK TABLES ');
    Dmod.QrAux.ExecSQL;
    //
  end;
end;

function TDModG.BuscaProximoCodigoInt_Negativo(const Tabela, CampoIdx,
ComplSQL: String; const PreDefinido: Integer; const Aviso: String;
var ProximoCodigoInt: Integer; const Limite: Integer = -High(Integer)): Boolean;
var
  TabNome: String;
begin
  if (PreDefinido <> 0) then
  begin
    ProximoCodigoInt := PreDefinido;
    Result := True;
  end else begin
    TabNome := Lowercase(Tabela);
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('LOCK TABLES ' + Lowercase(TabNome) + ' WRITE');
    Dmod.QrAux.ExecSQL;
    //
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MIN(' + CampoIdx + ') ' + CampoIdx + ' FROM ' + Lowercase(TabNome));
    Dmod.QrAux.SQL.Add(ComplSQL);
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    //
    if Limite >= Dmod.QrAux.FieldByName(CampoIdx).AsInteger then
    begin
      ProximoCodigoInt := 0;
      Result := False;
      Geral.MB_Aviso(
      'AVISO! Foi atingido o limite permitido de numera��o para a tabela "' +
      TabNome + '"!' + sLineBreak + 'Limite permitido = ' +
      Geral.FFT(Limite, 0, siNegativo)  + sLineBreak +
      '�ltima numera��o usada = ' + Geral.FFT(
      Dmod.QrAux.FieldByName(CampoIdx).AsInteger, 0, siNegativo)  + sLineBreak +
      'Identifica��o:' + sLineBreak +  Aviso);
      Exit;
    end;
    //
{
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('UPDATE ' + Lowercase(TabNome) + ' SET ' + CampoTabCtrl + '=' + CampoTabCtrl + ' - 1');
    Dmod.QrAux.SQL.Add(ComplSQL);
    Dmod.QrAux.ExecSQL;
    //
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT ' + CampoTabCtrl + ' FROM ' + Lowercase(TabNome));
    Dmod.QrAux.SQL.Add(ComplSQL);
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
}
    //
    ProximoCodigoInt := Dmod.QrAux.FieldByName(CampoIdx).AsInteger - 1;
    Result := ProximoCodigoInt <> 0;
    if not Result then
    Geral.MB_Aviso(PChar('AVISO! N�o foi poss�vel definir o pr�ximo n�mero ' +
    'da tabela "' + TabNome + '"!' + sLineBreak + 'Limite permitido = ' +
    Geral.FFT(Limite, 0, siNegativo) + sLineBreak +
    '�ltima numera��o usada = ' + Geral.FFT(
    Dmod.QrAux.FieldByName(CampoIdx).AsInteger, 0, siNegativo) + sLineBreak +
    'Identifica��o:' + sLineBreak + Aviso));
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('UNLOCK TABLES ');
    Dmod.QrAux.ExecSQL;
    //
  end;
end;

{$IfNDef SemEntidade}
function TDModG.CadastroDeEntidade(Entidade: Integer; TipoForm,
  Padrao: TFormCadEnti; EditingByMatriz: Boolean = False;
  AbrirEmAba: Boolean = False; InOwner: TWincontrol = nil;
  Pager: TWinControl = nil; LiberMultiEmpresas: Boolean = False;
  EntTipo: TUnEntTipo = uetNenhum; Aba: Integer = 0; EntiContat: Integer = 0): Boolean;
var
  Modelo: Integer;
  Form: TForm;
  LiberaMultiEmp: Boolean;
begin
  VAR_CADASTRO   := 0;
  Result         := False;
  FEntiToCad     := Entidade;
  LiberaMultiEmp := LiberMultiEmpresas;
  //
  if CO_DMKID_APP in [3, 4, 7, 11, 12, 43] then //Creditor, Syndi2, LeSew, DermaAD, DermaAD2, Syndi3
    LiberaMultiEmp := True;
  //
  if LiberaMultiEmp = False then
    if not SoUmaEmpresaLogada(True) then Exit;
  //
  if GERAL_MODELO_FORM_ENTIDADES <> fmcadSelecionar then
    Modelo := Integer(GERAL_MODELO_FORM_ENTIDADES) -1
  else
  begin
    case TipoForm of
      fmcadSelecionar:
      begin
        Modelo := MyObjects.SelRadioGroup('Selecione o modelo', '', ['Modelo A', 'Modelo B', 'Modelo C'], 2);
        if not Modelo in [0, 1] then
          Exit;
      end;
      //
      fmcadEntidades: Modelo := 0;
      fmcadEntidade2: Modelo := 1;
      fmcadEntidade3: Modelo := 2;
      else Modelo := -1;
    end;
  end;
  case Modelo of
    0:
    begin
      if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
      begin
        // Deve ser antes
        FmEntidades.FEditingByMatriz := EditingByMatriz;
        // somente depois...
        if FEntiToCad <> 0 then
          FmEntidades.LocCod(FEntiToCad, FEntiToCad);
        if EntTipo <> uetNenhum then
          FmEntidades.FEntTipo := EntTipo;
        FmEntidades.PageControl1.ActivePageIndex := Aba;
        FmEntidades.ShowModal;
        FmEntidades.Destroy;
        Result := True;
      end;
    end;
    1:
    begin
      if AbrirEmAba then
      begin
        if FmEntidade2 = nil then
        begin
          Form := MyObjects.FormTDICria(TFmEntidade2, InOwner, Pager, True, True);
          //
          if EntTipo <> uetNenhum then
            TFmEntidade2(Form).FEntTipo := EntTipo;
          //
          if EntiContat <> 0 then
            TFmEntidade2(Form).FEntiContat := EntiContat;
          //
          TFmEntidade2(Form).FEditingByMatriz          := EditingByMatriz;
          TFmEntidade2(Form).PCShowGer.ActivePageIndex := Aba;
          //
          if FEntiToCad <> 0 then
            TFmEntidade2(Form).LocCod(FEntiToCad, FEntiToCad);
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
        begin
          // Deve ser antes
          FmEntidade2.FEditingByMatriz          := EditingByMatriz;
          FmEntidade2.PCShowGer.ActivePageIndex := Aba;
          //
          if EntiContat <> 0 then
            FmEntidade2.FEntiContat := EntiContat;
          //
          if EntTipo <> uetNenhum then
            FmEntidade2.FEntTipo := EntTipo;
          // somente depois...
          if FEntiToCad <> 0 then
            FmEntidade2.LocCod(FEntiToCad, FEntiToCad)
          else if Length(Geral.SoNumero_TT(VAR_CNPJ_A_CADASTRAR)) > 10 then
          begin
            FmEntidade2.F_CNPJ_A_CADASTRAR := Geral.SoNumero_TT(VAR_CNPJ_A_CADASTRAR);
            VAR_CNPJ_A_CADASTRAR := '';
          end;
          FmEntidade2.ShowModal;
          FmEntidade2.Destroy;
          //
          FmEntidade2 := nil;
          //
          Result := True;
        end;
      end;
    end;
    2:
    begin
      if AbrirEmAba then
      begin
        if FmEntidade3 = nil then
        begin
          Form := MyObjects.FormTDICria(TFmEntidade3, InOwner, Pager, True, True);
          //
          (*
          if EntTipo <> uetNenhum then
            TFmEntidade3(Form).FEntTipo := EntTipo;
          //
          TFmEntidade3(Form).FEditingByMatriz := EditingByMatriz;
          //
          if FEntiToCad <> 0 then
            TFmEntidade3(Form).LocCod(FEntiToCad, FEntiToCad);
          *)
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmEntidade3, FmEntidade3, afmoNegarComAviso) then
        begin
          (*
          // Deve ser antes
          FmEntidade3.FEditingByMatriz := EditingByMatriz;
          //
          if EntTipo <> uetNenhum then
            FmEntidade3.FEntTipo := EntTipo;
          // somente depois...
          if FEntiToCad <> 0 then
            FmEntidade3.LocCod(FEntiToCad, FEntiToCad)
          else if Length(Geral.SoNumero_TT(VAR_CNPJ_A_CADASTRAR)) > 10 then
          begin
            FmEntidade3.F_CNPJ_A_CADASTRAR := Geral.SoNumero_TT(VAR_CNPJ_A_CADASTRAR);
            VAR_CNPJ_A_CADASTRAR := '';
          end;
          *)
          FmEntidade3.ShowModal;
          FmEntidade3.Destroy;
          //
          FmEntidade3 := nil;
          //
          Result := True;
        end;
      end;
    end
    else Geral.MB_Erro('Modelo de cadastro de entidade n�o implementado!');
  end;
end;

function TDModG.CadastroESelecaoDeEntidade(Entidade: Integer; QrEntidade:
TmySQLQuery; EdEntidade: TdmkEditCB; CBEntidade: TdmkDBLookupComboBox): Boolean;
begin
  CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrEntidade, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidade, VAR_CADASTRO, 'Codigo');
    EdEntidade.SetFocus;
  end;
end;

{$EndIf}

function TDModG.MD5_por_SQL(Texto: String): String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMD5, Dmod.MyDB, [
  'SELECT MD5("' + Texto + '") Texto ',
  '']);
  //
  Result := QrMD5Texto.Value;
end;

{
procedure TDModG.CarregaPosicaoColunasDmkDBGrid(NomeForm: String;
  Usuario: Integer; dmkDBGrid: TdmkDBGrid);
var
  I: Integer;
begin
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT * ');
  QrAux.SQL.Add('FROM poscolgri');
  QrAux.SQL.Add('WHERE FormName=:P0');
  QrAux.SQL.Add('AND GradeName=:P1');
  QrAux.SQL.Add('AND Usuario=:P2');
  QrAux.Params[0].AsString  := NomeForm;
  QrAux.Params[1].AsString  := dmkDBGrid.Name;
  QrAux.Params[2].AsInteger := Usuario;
  UnDmkDAC_PF.AbreQueryApenas(QrAux);
  if QrAux.RecordCount > 0 then
  begin
    while not QrAux.Eof do
    begin
      for I := 0 to dmkDBGrid.Columns.Count - 1 do
      begin
        if QrAux.FieldByName('FieldName').AsString = dmkDBGrid.Columns[I].FieldName then
        begin
          dmkDBGrid.Columns[I].Index := QrAux.RecNo - 1;
          dmkDBGrid.Columns[I].Width := QrAux.FieldByName('Tamanho').AsInteger;
        end;
      end;
      //
      QrAux.Next;
    end;
  end;
end;
}

procedure TDModG.CarregaPosicaoColunasDmkDBGrid(Usuario: Integer);
var
  I, N: Integer;
  FormName, GradeName: String;
  DBGrid: TdmkDBGridZTO;
begin
  if VAR_SAVE_CFG_DBGRID_GRID = nil then
    Exit;
  // 2014-08-27
  //FormName  := TComponent(VAR_SAVE_CFG_DBGRID_GRID.Owner).Name;
  FormName  := TComponent(VAR_SAVE_CFG_DBGRID_GRID.Owner).ClassName;
  FormName := Copy(FormName, 2);
  // FIM 2014-08-27
  //
  DBGrid := TdmkDBGridZTO(VAR_SAVE_CFG_DBGRID_GRID);
  GradeName := DBGrid.Name;
  //
  if MyObjects.FIC(Length(FormName) = 0, nil, 'Formul�rio n�o definido!') then Exit;
  if MyObjects.FIC(Length(GradeName)= 0, nil, 'Grade n�o definida!') then Exit;
  if MyObjects.FIC(Usuario = 0, nil, 'Usu�rio n�o definido!') then Exit;
  //
  // 2017-12-17
(*
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT * ');
  QrAux.SQL.Add('FROM poscolgri');
  QrAux.SQL.Add('WHERE FormName=:P0');
  QrAux.SQL.Add('AND GradeName=:P1');
  QrAux.SQL.Add('AND Usuario=:P2');
  QrAux.Params[0].AsString  := FormName;
  QrAux.Params[1].AsString  := GradeName;
  QrAux.Params[2].AsInteger := Usuario;
  UnDmkDAC_PF.AbreQueryApenas(QrAux);
*)
  if USQLDB.TabelaExiste('poscolgri', Dmod.MyDB) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM poscolgri ',
    'WHERE FormName="' + FormName + '"',
    'AND GradeName="' + GradeName + '"',
    'AND Usuario=' + Geral.FF0(Usuario),
    '']);
    // fim 2017-12-17
    if QrAux.RecordCount > 0 then
    begin
      // 2014-08-27
      while DBGrid.Columns.Count - 1 < QrAux.RecordCount + 10 do
        DBGrid.Columns.Add;
      // FIM 2014-08-27
      while not QrAux.Eof do
      begin
        for I := 0 to DBGrid.Columns.Count - 1 do
        begin
          if QrAux.FieldByName('FieldName').AsString = DBGrid.Columns[I].FieldName then
          begin
  (*        2013-07-20
            DBGrid.Columns[I].Index := QrAux.RecNo - 1;
            DBGrid.Columns[I].Width := QrAux.FieldByName('Tamanho').AsInteger;
  *)
            DBGrid.Columns[I].Width := QrAux.FieldByName('Tamanho').AsInteger;
            // 2014-08-26
            N := QrAux.RecNo - 1;
            if N > DBGrid.Columns.Count - 1 then
              N := DBGrid.Columns.Count - 1;
            try
              DBGrid.Columns[I].Index := N;
            except
              ;
            end;
            // FIM 2013-07-20
          end;
        end;
        //
        QrAux.Next;
      end;
    end;
  end else
  begin

  end;
end;

procedure TDModG.CarregarOrdenaopadrodascolunasdagrade1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja carregar a ordena��o padr�o das colunas da grade?'
    + sLineBreak + 'Ao fazer isso a configura��o atual ser� alterada.' + sLineBreak +
    'Deseja continuar?') = ID_YES then
  begin
    SalvaPosicaoColunasDmkDBGrid(VAR_USUARIO, True);
  end;
end;

function TDModG.ConcatenaPrimeirosNomes(Tabela, FldID, FldAge: String;
  Codigo: Integer): String;
var
  Nome, Texto: String;
  p: Integer;
begin
  Result := '';
  Texto := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNomes, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) TEXTO ',
  'FROM entidades ent ',
  'LEFT JOIN ' + Tabela + ' lft ON lft.' + FldAge + '=ent.Codigo ',
  'WHERE lft.' + FldID + '=' + Geral.FF0(Codigo),
  'ORDER BY lft.' + FldAge,
  '']);
  QrNomes.First;
  while not QrNomes.Eof do
  begin
    Nome := Trim(QrNomesTEXTO.Value);
    p := pos(' ', Nome);
    if p > 0 then
      Nome := Copy(Nome, 1, p-1);
    Texto := Texto + ', ' + Nome;
    //
    QrNomes.Next;
  end;
  //
  if Texto <> '' then
    Texto := Copy(Texto, 3);
  //
  Result := Texto;
end;

procedure TDModG.ConectaBDLocalViaServidor();
var
  IP_Default: String;
begin
  if VAR_USUARIO < 0 then
  begin
    IP_Default := '127.0.0.1';
    //InputQuery('Verifica��o de Banco de Dados Local', 'Informe o IP:', IP_Default);
  end else begin
    ReopenSenhas;
    //
    IP_Default := QrSenhasIP_Default.Value;
  end;
  //
  if MyLocDB.Connected then
    Geral.MB_Aviso('DModG.MyLocDB est� connectado antes da configura��o!');
  if MyPID_DB.Connected then
    Geral.MB_Aviso('DModG.MyPID_DB est� connectado antes da configura��o!');
  if MyCompressDB.Connected then
    Geral.MB_Aviso('DModG.MyCompressDB est� connectado antes da configura��o!');
  if MyPID_CompressDB.Connected then
    Geral.MB_Aviso('DModG.MyPID_CompressDB est� connectado antes da configura��o!');
  if MySyncDB.Connected then
    Geral.MB_Aviso('DModG.MySyncDB est� connectado antes da configura��o!');
  {$IfNDef SemDBLocal}
  MyLocDB.Close;
  MyLocDB.UserPassword := VAR_BDSENHA;
  MyLocDB.DatabaseName := Dmod.MyLocDatabase.DatabaseName;
  MyLocDB.Host         := IP_Default;
  MyLocDB.Port         := VAR_PORTA;
  if VAR_VLOCAL and CO_VLOCAL then
  begin
    if Trim(IP_Default) = '' then
    begin
      Geral.MB_Erro(
      'Verifica��o do banco de dados local cancelada! IP n�o informado!');
      Exit;
    end;
    if not VAR_VERIFI_DB_CANCEL then
    begin
      Application.CreateForm(TFmVerifiDBLocal, FmVerifiDBLocal);
      with FmVerifiDBLocal do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
    end;
  end;
  {$EndIf}
  //
end;

function TDModG.ConectaMySyncDB(NomeDB: String): Boolean;
begin
  Result := False;
  MySyncDB.Disconnect;
  MySyncDB.Host         := Dmod.MyDB.Host;
  MySyncDB.UserName     := Dmod.MyDB.UserName;
  MySyncDB.UserPassword := Dmod.MyDB.UserPassword;
  MySyncDB.DatabaseName := NomeDB;
  MySyncDB.Port         := VAR_PORTA;
  try
    Screen.Cursor := crHourGlass;
    MySyncDB.Connect;
  finally
    Screen.Cursor := crDefault;
  end;
  Result := MySyncDB.Connected;
end;

procedure TDModG.ConsertaCST_PIS_COFINS();
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE gragru1 SET PIS_CST = 1 WHERE PIS_CST=0;');
  Dmod.QrUpd.SQL.Add('UPDATE gragru1 SET COFINS_CST = 1 WHERE COFINS_CST=0;');
  Dmod.QrUpd.ExecSQL;
end;

function TDModG.ContatoJaAtrelado(Entidade, Contato: Integer;
  Avisa: Boolean): Boolean;
var
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery.Create(Dmod);
  try
    Query.Close;
    Query.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Controle ',
    'FROM enticonent ',
    'WHERE Codigo=' + Geral.FF0(Entidade),
    'AND Controle=' + Geral.FF0(Contato),
    '']);
    //
    Result := Query.RecordCount > 0;
    if Result and Avisa then
      Geral.MB_Aviso('O contato ' + Geral.FF0(Contato) +
      ' j� est� atrelado � entidade ' + Geral.FF0(Entidade));
  finally
    Query.Free;
  end;
end;

procedure TDModG.CorrigeCorETamInexistentes();
var
  Qry: TmySQLQuery;
  Nao, Nivel1, GraCorCad, Controle: Integer;
begin
  Nao := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.GraGru1, ggx.GraGruC, ggx.GraTAmI, ',
    'ggc.Controle CTRL_GGC, gti.Controle CTRL_GTI ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'WHERE ggc.Controle IS NULL ',
    'OR gti.Controle IS NULL ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' cadastros de grade sem Cor ou Tamanho e devem ser corrigidos!' +
      sLineBreak + 'Deseja corrig�-los agora?') = ID_YES then
      begin
        Qry.First;
        while not Qry.Eof do
        begin
          if Qry.FieldByName('CTRL_GGC').AsInteger = 0 then
          begin
            if Qry.FieldByName('GraGruC').AsInteger <> 0 then
            begin
              Nivel1    := Qry.FieldByName('GraGru1').AsInteger;
              GraCorCad := 1;
              Controle  := Qry.FieldByName('GraGruC').AsInteger;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
              'Nivel1', 'GraCorCad'], [
              'Controle'], [
              Nivel1, GraCorCad], [
              Controle], True);
            end else Nao := Nao + 1;
          end else Nao := Nao + 1;
          //
          Qry.Next;
        end;
        UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
        if Nao > 0 then
          Geral.MB_Erro(Geral.FF0(Nao) +
          ' itens n�o foram corrigidos por falta de implementa��o!');
        if Qry.RecordCount > 0 then
          Geral.MB_Erro('No total ' + Geral.FF0(Qry.RecordCount) +
          ' itens n�o foram corrigidos!');
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDModG.CorrigeDuplicacaoDeLctControle(TabLctA: String; LaAviso: TLabel;
  PB1: TProgressBar);
var
  Controle, Itens: Integer;
  Texto1, Texto2: String;
begin
  Screen.Cursor := crHourGlass;
  Itens := 0;
  MyObjects.Informa(LaAviso, True, 'Verificando se h� duplicidade');
  QrAuxPID1.Close;
  QrAuxPID1.SQL.Clear;
  QrAuxPID1.SQL.Add('DROP TABLE IF EXISTS _corrige_ctrl_lan_;');
  QrAuxPID1.SQL.Add('CREATE TABLE _corrige_ctrl_lan_');
  QrAuxPID1.SQL.Add('SELECT COUNT(Controle) Itens, Controle, Sub');
  QrAuxPID1.SQL.Add('FROM ' + TMeuDB + '.' + TabLctA);
  QrAuxPID1.SQL.Add('GROUP BY Controle, Sub;');
  QrAuxPID1.SQL.Add(DELETE_FROM + ' _corrige_ctrl_lan_ WHERE Itens < 2;');
  QrAuxPID1.SQL.Add('SELECT lan.Data, lan.Tipo, lan.Carteira, lan.Controle, lan.Sub');
  QrAuxPID1.SQL.Add('FROM _corrige_ctrl_lan_ tes, ' + TMeuDB + '.' + TabLctA + ' lan');
  QrAuxPID1.SQL.Add('WHERE lan.Controle=tes.Controle');
  QrAuxPID1.SQL.Add('ORDER BY Controle;');
  UnDmkDAC_PF.AbreQueryApenas(QrAuxPID1);
  if QrAuxPID1.RecordCount > 0 then
  begin
    if PB1 <> nil then
      PB1.Max := QrAuxPID1.RecordCount;
    QrAuxPID1.First;
    while not QrAuxPID1.Eof do
    begin
      if PB1 <> nil then
        PB1.Position := PB1.Position + 1;
      Texto1 := 'Corrigindo controle ' + FormatFloat('0',
        QrAuxPID1.FieldByName('Controle').AsFloat) + ' ( ' + FormatFloat('0',
        QrAuxPID1.RecNo) + ' de ' + FormatFloat('0', QrAuxPID1.RecordCount) + ')';
      MyObjects.Informa(LaAviso, True, Texto1);
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub');
      Dmod.QrAux.SQL.Add('FROM ' + TabLctA);
      Dmod.QrAux.SQL.Add('WHERE Controle=:P0');
      Dmod.QrAux.SQL.Add('AND Sub=:P1');
      Dmod.QrAux.Params[00].AsFloat   := QrAuxPID1.FieldByName('Controle').AsFloat;
      Dmod.QrAux.Params[01].AsInteger := QrAuxPID1.FieldByName('Sub').AsInteger;
      UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
      while not Dmod.QrAux.Eof do
      begin
        Texto2 := Texto1 + '. Item ' + FormatFloat('0', Dmod.QrAux.RecNo) + ' de '
        + FormatFloat('0', Dmod.QrAux.RecordCount);
        MyObjects.Informa(LaAviso, True, Texto2);
        //
        Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE ' + TabLctA + ' SET Controle=:P0, ErrCtrl=:P1 ');
        Dmod.QrUpd.SQL.Add('WHERE Data=:P2');
        Dmod.QrUpd.SQL.Add('AND Tipo=:P3');
        Dmod.QrUpd.SQL.Add('AND Carteira=:P4');
        Dmod.QrUpd.SQL.Add('AND Controle=:P5');
        Dmod.QrUpd.SQL.Add('AND Sub=:P6');
        Dmod.QrUpd.Params[00].AsInteger := Controle;
        Dmod.QrUpd.Params[01].AsInteger := Dmod.QrAux.FieldByName('Controle').AsInteger;
        Dmod.QrUpd.Params[02].AsString  := Geral.FDT(Dmod.QrAux.FieldByName('Data').AsDateTime, 1);
        Dmod.QrUpd.Params[03].AsInteger := Dmod.QrAux.FieldByName('Tipo').AsInteger;
        Dmod.QrUpd.Params[04].AsInteger := Dmod.QrAux.FieldByName('Carteira').AsInteger;
        Dmod.QrUpd.Params[05].AsInteger := Dmod.QrAux.FieldByName('Controle').AsInteger;
        Dmod.QrUpd.Params[06].AsInteger := Dmod.QrAux.FieldByName('Sub').AsInteger;
        Dmod.QrUpd.ExecSQL;
        //
        Itens := Itens + 1;
        //
        Dmod.QrAux.Next;
       end;
      //
      QrAuxPID1.Next;
    end;
  end ;
  MyObjects.Informa(LaAviso, False, '');
  Screen.Cursor := crDefault;
  Geral.MB_Info('Foram modiificados ' + FormatFloat('0', Itens) +
  'lan�amentos!');
end;

procedure TDModG.CorrigeEmpresaDeMenosUmParaMenosOnze();
var
{
  Adm, CNPJ_N, CNPJ_A: String;
  Cod: Integer;
}
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    // E N T I D A D E
    // verifica se existe o -1
    DMod.QrAux.Close;
    DMod.QrAux.SQL.Clear;
    DMod.QrAux.SQL.Add('SELECT * FROM entidades WHERE Codigo=-1');
    UnDmkDAC_PF.AbreQueryApenas(DMod.QrAux);
    // se existir o -1 verifica se existe o -11 tamb�m
    if DMod.QrAux.RecordCount > 0 then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT * FROM entidades WHERE Codigo=-11');
      UnDmkDAC_PF.AbreQueryApenas(DMod.QrAux);
      //
      // se o -11 existir, exclui o -1
      if DMod.QrAux.RecordCount > 0 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add(DELETE_FROM + ' entidades WHERE Codigo=-1');
        DMod.QrUpd.ExecSQL;
      end
      else
      // caso contr�rio muda o -1 para -11
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('UPDATE entidades SET Codigo=-11 WHERE Codigo=-1');
        DMod.QrUpd.ExecSQL;
      end;
    end;
    //
    // E N T I C L I I N T
    // verifica se existe o -1
    DMod.QrAux.Close;
    DMod.QrAux.SQL.Clear;
    DMod.QrAux.SQL.Add('SELECT * FROM enticliint WHERE CodEnti=-1');
    UnDmkDAC_PF.AbreQueryApenas(DMod.QrAux);
    // se existir o -1 verifica se existe o -11 tamb�m
    if DMod.QrAux.RecordCount > 0 then
    begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT * FROM enticliint WHERE CodEnti=-11');
      UnDmkDAC_PF.AbreQueryApenas(DMod.QrAux);
      //
      // se o -11 existir, exclui o -1
      if DMod.QrAux.RecordCount > 0 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add(DELETE_FROM + ' enticliint WHERE CodEnti=-1');
        DMod.QrUpd.ExecSQL;
      end
      else
      // caso contr�rio muda o -1 para -11
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('UPDATE enticliint SET CodEnti=-11 WHERE CodEnti=-1');
        DMod.QrUpd.ExecSQL;
      end;
    end else begin
      DMod.QrAux.Close;
      DMod.QrAux.SQL.Clear;
      DMod.QrAux.SQL.Add('SELECT * FROM enticliint WHERE CodEnti=-11');
      UnDmkDAC_PF.AbreQueryApenas(DMod.QrAux);
      //
      if DMod.QrAux.RecordCount = 0 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('INSERT INTO enticliint SET CodEnti=-11, ');
        DMod.QrUpd.SQL.Add('CodFilial=1, TipoTabLct=0, CodCliInt=1');
        DMod.QrUpd.ExecSQL;
      end;
    end;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE controle SET Dono=-11');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=1, Filial=1, Cliente1="V" WHERE Codigo=-11');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE enticliint SET CodCliInt=1, CodFilial=1 WHERE CodEnti=-11');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE carteiras SET ForneceI=-11 WHERE ForneceI IN (0,-1)');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE lct0001a SET CliInt=-11 WHERE CliInt IN (0,-1)');
    DMod.QrUpd.ExecSQL;
    //
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TDModG.CorrigeMesDeCompetenciaDeExtratos(PB: TProgressBar;
TabLctA: String);
begin
  Screen.Cursor := crHourGlass;
  PB.Position := 0;
  QrLmz.Close;
  QrLmz.SQL.Clear;
  QrLmz.SQL.Add('SELECT Mez, Controle, ID_Pgto,  Data');
  QrLmz.SQL.Add('FROM ' + TabLctA);
  QrLmz.SQL.Add('WHERE Mez=1');
  QrLmz.SQL.Add('AND ID_Pgto<>0');
  QrLmz.SQL.Add('ORDER BY Data');
  QrLmz.SQL.Add('');
  UnDmkDAC_PF.AbreQueryApenas(QrLmz);
  PB.Max := QrLmz.RecordCount;
  while not QrLMZ.Eof do
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
    Application.ProcessMessages;
    //
    QrUmz.Close;
    QrUmz.SQL.Clear;
    QrUmz.SQL.Add('SELECT Mez');
    QrUmz.SQL.Add('FROM ' + TabLctA);
    QrUmz.SQL.Add('WHERE Controle=:P0');
    QrUmz.Params[0].AsInteger := QrLmzID_Pgto.Value;
    UnDmkDAC_PF.AbreQueryApenas(QrUmz);
    //
    Dmod.MyDB.Execute('UPDATE LOW_PRIORITY ' + TabLctA + ' SET Mez=' +
    FormatFloat('0', QrUmzMez.Value) + ' WHERE Controle=' +
    FormatFloat('0', QrLmzControle.Value));
    //
    QrLmz.Next;
  end;
  Screen.Cursor := crDefault;
end;

 {$IfDef cAdvToolx} //Berlin
function TDModG.CriaFavoritos(Pager: TAdvToolBarPager; LaAviso0,
  LaAviso1: TLabel; Base: TAdvGlowButton; var Reference): Boolean;
const
  FExcluir = '* E X C L U I R *';
var
  AdvPage: TAdvPage;
  AdvToolBar: TAdvToolBar;
  Colunas, W, T, L, I, N, AtbW: Integer;
  //Objeto: TObject;
  Compo, NewCompo, OldCompo: TComponent;
  Nome, X: String;
  //
  AdvGlowButton: TAdvGlowButton;
  DBAdvGlowButton: TDBAdvGlowButton;
  AdvGlowMenuButton: TAdvGlowMenuButton;
  ProCustomGlowButton: TProCustomGlowButton;
  WindowState: TWindowState;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso0, LaAviso1, False, 'Destruindo as barras de ferramentas favoritas j� criadas');
  with TForm(Reference) do
  begin
    // Botoes
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      if Compo is TAdvCustomGlowButton then
      begin
        Nome := TAdvCustomGlowButton(Compo).Name;
        x := VAR_NomeAdvGlowBtn_TempoExec;
        if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
        begin
          //TAdvCustomGlowButton(Compo).Destroy;
          TAdvCustomGlowButton(Compo).Free;
          I := 0;
        end;
      end;
      //
      I := I + 1;
    end;
    // ToolBar
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      if Compo is TAdvToolBar then
      begin
        Nome := TAdvToolBar(Compo).Name;
        x := VAR_NomeToolBar_TempoExec;
        if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
        begin
          TAdvToolBar(Compo).Free;
          I := 0;
        end;
      end;
      //
      I := I + 1;
    end;
    // Page
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      if Compo is TAdvPage then
      begin
        Nome := TAdvPage(Compo).Name;
        //Geral.MB_Aviso(Nome);
        x := VAR_NomeAdvPage_TempoExec;
        if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
        begin
          TAdvPage(Compo).Free;
          I := 0;
        end;
      end;
      //
      I := I + 1;
    end;
    // Teste todos
{
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      Nome := TComponent(Compo).Name;
      x := VAR_NomeAdvGlowBtn_TempoExec;
      if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
      begin
        TComponent(Compo).Destroy;
        I := I - 1;
      end;
        x := VAR_NomeToolBar_TempoExec;
      if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
      begin
        TComponent(Compo).Destroy;
        I := I - 1;
      end;
      x := VAR_NomeAdvPage_TempoExec;
      if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
      begin
        TComponent(Compo).Destroy;
        I := I - 1;
      end;
      //
      I := I + 1;
    end;
}
  end;
  //
  //
  MyObjects.Informa2(LaAviso0, LaAviso1, False, 'Criando Barras de ferramentas favoritas');
  QrFavoritos3.Close;
  QrFavoritos3.Params[0].AsInteger := VAR_USUARIO;
  UnDmkDAC_PF.AbreQueryApenas(QrFavoritos3);
  //
  Result := QrFavoritos3.RecordCount <> 0;
  //
  while not QrFavoritos3.Eof do
  begin
    // Cria e configura Guia (AdvPage)
    AdvPage := TAdvPage.Create(TForm(Reference));
    //AdvPage.Parent := Base.Parent;
    AdvPage.ParentWindow := Base.ParentWindow;
    VAR_SEQ_ADV_COMPOS := VAR_SEQ_ADV_COMPOS + 1;
    AdvPage.Name := VAR_NomeAdvPage_TempoExec + FormatFloat('0', VAR_SEQ_ADV_COMPOS);
    //AdvPage.Name := VAR_NomeAdvPage_TempoExec + FormatFloat('0', QrFavoritos3.RecNo);
    AdvPage.Caption := QrFavoritos3Titulo.Value;
    AdvPage.PageIndex := QrFavoritos3.RecNo - 1;
    //
    // Cria e configura P�gina (AdvToolBar)
    QrFavoritos2.Close;
    QrFavoritos2.Params[0].AsInteger := QrFavoritos3Nivel3.Value;
    UnDmkDAC_PF.AbreQueryApenas(QrFavoritos2);
    //
    while not QrFavoritos2.Eof do
    begin
      //
      QrFavoritos1.Close;
      QrFavoritos1.Params[0].AsInteger := QrFavoritos2Nivel2.Value;
      UnDmkDAC_PF.AbreQueryApenas(QrFavoritos1);
      Colunas := QrFavoritos1.RecordCount div 3;
      if QrFavoritos1.RecordCount mod 3 <> 0 then
        Colunas := Colunas + 1;
      W := (Colunas * 116) + 4;
      if W < 120 then
        W := 120;
      //
      AdvToolBar := TAdvToolBar.Create(TForm(Reference));
      AdvToolBar.ShowOptionIndicator := False;
      AdvToolBar.Parent := TForm(Reference);
      AdvPage.AddAdvToolBar(AdvToolBar);
      AdvToolBar.AutoSize := False;
      AdvToolBar.AllowFloating := False;
      AdvToolBar.AutoArrangeButtons := False;
      AdvToolBar.AutoPositionControls := False;
      AdvToolBar.Width := W;
      VAR_SEQ_ADV_COMPOS := VAR_SEQ_ADV_COMPOS + 1;
      AdvToolBar.Name := VAR_NomeToolBar_TempoExec + FormatFloat('0', VAR_SEQ_ADV_COMPOS);
      //AdvToolBar.Name := VAR_NomeToolBar_TempoExec + FormatFloat('0', QrFavoritos2.RecNo);
      AdvToolBar.Caption := QrFavoritos2Titulo.Value;
      AdvToolBar.ToolBarIndex := QrFavoritos2.RecNo - 1;
      //
      // Cria e configura Bot�es
      while not QrFavoritos1.Eof do
      begin
        //NewCompo := nil;
        OldCompo := nil;
        with TForm(Reference) do
        begin
          for I := 0 to ComponentCount -1 do
          begin
            Compo := Components[i];
            //
            if Compo is TAdvCustomGlowButton then
            begin
              if Trim(TAdvCustomGlowButton(Compo).Name) = Trim(QrFavoritos1Nome.Value) then
              begin
                OldCompo := Compo;
//                AdvGlowButton.Picture := TAdvCustomGlowButton(Compo).Picture;
//                AdvGlowButton.OnClick := TAdvCustomGlowButton(Compo).OnClick;
              end;
            end;
{
  TAdvGlowButton       = class(TAdvCustomGlowButton)
  TDBAdvGlowButton     = class(TAdvCustomGlowButton)
  TProCustomGlowButton = class(TAdvCustomGlowButton);
  TAdvGlowMenuButton   = class(TAdvCustomGlowButton)
  TProCustomGlowButton = class(TAdvCustomGlowButton);
}

          end;
        end;
        //
        if OldCompo <> nil then
        begin
          if Uppercase(QrFavoritos1Classe.Value) = Uppercase('TDBAdvGlowButton') then
          begin
            DBAdvGlowButton := TDBAdvGlowButton.Create(TForm(Reference));
            //
            TAdvGlowButton(NewCompo).PopupMenu        := TAdvGlowMenuButton(OldCompo).PopupMenu;
            TAdvGlowButton(NewCompo).DropDownButton   := TAdvGlowMenuButton(OldCompo).DropDownButton;
            TAdvGlowButton(NewCompo).DropDownPosition := TAdvGlowMenuButton(OldCompo).DropDownPosition;
            TAdvGlowButton(NewCompo).DropDownMenu     := TAdvGlowMenuButton(OldCompo).DropDownMenu;
            TAdvGlowButton(NewCompo).DropDownSplit    := TAdvGlowMenuButton(OldCompo).DropDownSplit;
            //
            DBAdvGlowButton.Layout := blGlyphLeftAdjusted;
            NewCompo := DBAdvGlowButton;
          end else
          if Uppercase(QrFavoritos1Classe.Value) = Uppercase('TAdvGlowMenuButton') then
          begin
            AdvGlowMenuButton := TAdvGlowMenuButton.Create(TForm(Reference));
            AdvGlowMenuButton.Layout := blGlyphLeftAdjusted;
            NewCompo := AdvGlowMenuButton;
            TAdvGlowMenuButton(NewCompo).PopupMenu := TAdvGlowMenuButton(OldCompo).PopupMenu;
            TAdvGlowMenuButton(NewCompo).DropDownButton := TAdvGlowMenuButton(OldCompo).DropDownButton;
            TAdvGlowMenuButton(NewCompo).DropDownPosition := TAdvGlowMenuButton(OldCompo).DropDownPosition;
            TAdvGlowMenuButton(NewCompo).DropDownMenu := TAdvGlowMenuButton(OldCompo).DropDownMenu;
            TAdvGlowMenuButton(NewCompo).DropDownSplit := TAdvGlowMenuButton(OldCompo).DropDownSplit;
          end else
          if Uppercase(QrFavoritos1Classe.Value) = Uppercase('TProCustomGlowButton') then
          begin
            ProCustomGlowButton := TProCustomGlowButton.Create(TForm(Reference));
            NewCompo := ProCustomGlowButton;
          end else
          begin
            AdvGlowButton := TAdvGlowButton.Create(TForm(Reference));
            AdvGlowButton.Layout := blGlyphLeftAdjusted;
            NewCompo := AdvGlowButton;
          end;
          //
          if TAdvGlowMenuButton(OldCompo).Height < 46 then
            TAdvCustomGlowButton(NewCompo).Height := 46
          else
            TAdvCustomGlowButton(NewCompo).Height := TAdvGlowMenuButton(OldCompo).Height;
          //
          if TAdvGlowMenuButton(OldCompo).Width < 112 then
            TAdvCustomGlowButton(NewCompo).Width := 112
          else
            TAdvCustomGlowButton(NewCompo).Width := TAdvGlowMenuButton(OldCompo).Width;
          //
          AtbW := (Colunas * (TAdvCustomGlowButton(NewCompo).Width + 4)) + 4;
          if AtbW < 120 then
            AtbW := 120;
          //
          AdvToolBar.Width := AtbW;
          //
          AdvToolBar.AddToolBarControl(TControl(NewCompo));
          //
          L := (((QrFavoritos1.RecNo -1) div 3) * (TAdvCustomGlowButton(NewCompo).Width + 4)) + 4;
          T := (((QrFavoritos1.RecNo -1) mod 3) * (TAdvCustomGlowButton(NewCompo).Height + 4)) + 4;
          //
          TAdvCustomGlowButton(NewCompo).Top  := T;
          TAdvCustomGlowButton(NewCompo).Left := L;
          //AdvGlowButton.OnClick := AdvGlowButtonClick;
          //
          VAR_SEQ_ADV_COMPOS := VAR_SEQ_ADV_COMPOS + 1;
          //N := N + 1;
          TAdvCustomGlowButton(NewCompo).Name := VAR_NomeAdvGlowBtn_TempoExec + FormatFloat('000000000', VAR_SEQ_ADV_COMPOS);
          //TAdvCustomGlowButton(NewCompo).Name := VAR_NomeAdvGlowBtn_TempoExec + FormatFloat('000000000', N);
          TAdvCustomGlowButton(NewCompo).Caption := QrFavoritos1Descri1.Value + sLineBreak +
            QrFavoritos1Descri2.Value + sLineBreak + QrFavoritos1Descri3.Value;
          //
          TAdvCustomGlowButton(NewCompo).Picture := TAdvCustomGlowButton(OldCompo).Picture;
          TAdvCustomGlowButton(NewCompo).OnClick := TAdvCustomGlowButton(OldCompo).OnClick;
        end;
        QrFavoritos1.Next;
      end;
      //
      QrFavoritos2.Next;
    end;
    //
    QrFavoritos3.Next;
  end;
  Pager.ActivePageIndex := 0;
  Application.ProcessMessages;
  WindowState := TForm(Reference).WindowState;
  TForm(Reference).WindowState := wsNormal;
  //W := TForm(Reference).Height;
  //L := TForm(Reference).Width;
  //TForm(Reference).Height := 1;
  TForm(Reference).Width  := TForm(Reference).Width + 10;
  TForm(Reference).Invalidate;
  TForm(Reference).Repaint;
  Application.ProcessMessages;
  TForm(Reference).Width  := TForm(Reference).Width - 10;
  TForm(Reference).Invalidate;
  TForm(Reference).Repaint;
  Application.ProcessMessages;
  TForm(Reference).WindowState := WindowState;
  {
  TForm(Reference).Height := W;
  TForm(Reference).Width  := L;
  }
  {
  WindowState := TForm(Reference).WindowState;
  TForm(Reference).WindowState := wsMinimized;
  Application.ProcessMessages;
  TForm(Reference).WindowState := WindowState;
  }
  MyObjects.Informa2(LaAviso0, LaAviso1, False, '...');
end;
{$Else} //Berlin

function TDModG.CriaFavoritos(PageControl: TdmkPageControl; LaAviso0,
              LaAviso1: TLabel; Base: TBitBtn; var Reference): Boolean;
const
  FExcluir = '* E X C L U I R *';
var
  TabSheet: TTabSheet;
  Panel, SubPanel: TPanel;
  Colunas, W, T, L, I, N, AtbW: Integer;
  //Objeto: TObject;
  Compo, NewCompo, OldCompo: TComponent;
  Nome, X: String;
  //
  BitBtn: TBitBtn;
  DBBitBtn: TBitBtn;
  //ProCustomGlowButton: TProCustomGlowButton;
  WindowState: TWindowState;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso0, LaAviso1, False, 'Destruindo as barras de ferramentas favoritas j� criadas');
  with TForm(Reference) do
  begin
    // Botoes
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      if Compo is TBitBtn then
      begin
        Nome := TBitBtn(Compo).Name;
        x := VAR_NomeAdvGlowBtn_TempoExec;
        if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
        begin
          //TBitBtn(Compo).Destroy;
          TBitBtn(Compo).Free;
          I := 0;
        end;
      end;
      //
      I := I + 1;
    end;
    // ToolBar
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      if Compo is TPanel then
      begin
        Nome := TPanel(Compo).Name;
        x := VAR_NomeToolBar_TempoExec;
        if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
        begin
          TPanel(Compo).Free;
          I := 0;
        end;
      end;
      //
      I := I + 1;
    end;
    // Page
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      if Compo is TTabSheet then
      begin
        Nome := TTabSheet(Compo).Name;
        //Geral.MB_Aviso(Nome);
        x := VAR_NomeTabSheet_TempoExec;
        if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
        begin
          TTabSheet(Compo).Free;
          I := 0;
        end;
      end;
      //
      I := I + 1;
    end;
    // Teste todos
{
    I := 0;
    while I < ComponentCount -1 do
    begin
      Compo := Components[I];
      //
      Nome := TComponent(Compo).Name;
      x := VAR_NomeAdvGlowBtn_TempoExec;
      if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
      begin
        TComponent(Compo).Destroy;
        I := I - 1;
      end;
        x := VAR_NomeToolBar_TempoExec;
      if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
      begin
        TComponent(Compo).Destroy;
        I := I - 1;
      end;
      x := VAR_NomeTabSheet_TempoExec;
      if (Uppercase(Copy(Nome, 1, Length(x))) = Uppercase(x)) then
      begin
        TComponent(Compo).Destroy;
        I := I - 1;
      end;
      //
      I := I + 1;
    end;
}
  end;
  //
  //
  MyObjects.Informa2(LaAviso0, LaAviso1, False, 'Criando Barras de ferramentas favoritas');
  QrFavoritos3.Close;
  QrFavoritos3.Params[0].AsInteger := VAR_USUARIO;
  UnDmkDAC_PF.AbreQueryApenas(QrFavoritos3);
  //
  Result := QrFavoritos3.RecordCount <> 0;
  //
  while not QrFavoritos3.Eof do
  begin
    // Cria e configura Guia (TabSheet)
    //TabSheet := TTabSheet.Create(TForm(Reference));
    TabSheet := TTabSheet.Create(PageControl);
    TabSheet.PageControl := PageControl;
       //TabSheet.Parent := Base.Parent;
    //TabSheet.ParentWindow := Base.ParentWindow;
    VAR_SEQ_ADV_COMPOS := VAR_SEQ_ADV_COMPOS + 1;
    TabSheet.Name := VAR_NomeTabSheet_TempoExec + FormatFloat('0', VAR_SEQ_ADV_COMPOS);
    //TabSheet.Name := VAR_NomeTabSheet_TempoExec + FormatFloat('0', QrFavoritos3.RecNo);
    TabSheet.Caption := QrFavoritos3Titulo.Value;
    ShowMessage('PageControl.AddTabSheet(TabSheet);');
    TabSheet.Parent := PageControl;
    //PageControl.AddTabSheet(TabSheet);
    TabSheet.PageIndex := QrFavoritos3.RecNo - 1;
    //
    // Cria e configura P�gina (Panel)
    QrFavoritos2.Close;
    QrFavoritos2.Params[0].AsInteger := QrFavoritos3Nivel3.Value;
    UnDmkDAC_PF.AbreQueryApenas(QrFavoritos2);
    //
    while not QrFavoritos2.Eof do
    begin
      //
      QrFavoritos1.Close;
      QrFavoritos1.Params[0].AsInteger := QrFavoritos2Nivel2.Value;
      UnDmkDAC_PF.AbreQueryApenas(QrFavoritos1);
      Colunas := QrFavoritos1.RecordCount div 3;
      if QrFavoritos1.RecordCount mod 3 <> 0 then
        Colunas := Colunas + 1;
      W := (Colunas * 116) + 4;
      if W < 120 then
        W := 120;
      //
      //Panel := TPanel.Create(TForm(Reference));
      Panel := TPanel.Create(TabSheet);
      //Panel.Parent := TForm(Reference);
      Panel.Parent := TabSheet;
      //Panel.ShowOptionIndicator := False;
      //TabSheet.AddPanel(Panel);
      Panel.AutoSize := False;
      //Panel.AllowFloating := False;
      //Panel.AutoArrangeButtons := False;
      //Panel.AutoPositionControls := False;
      Panel.Width := W;
      VAR_SEQ_ADV_COMPOS := VAR_SEQ_ADV_COMPOS + 1;
      Panel.Name := VAR_NomeToolBar_TempoExec + FormatFloat('0', VAR_SEQ_ADV_COMPOS);
      //Panel.Name := VAR_NomeToolBar_TempoExec + FormatFloat('0', QrFavoritos2.RecNo);
      //Panel.Caption := QrFavoritos2Titulo.Value;
      Panel.Caption := '';
      //Panel.ToolBarIndex := QrFavoritos2.RecNo - 1;
      //
      Panel.Align := alLeft;



      SubPanel := TPanel.Create(Panel);
      SubPanel.Parent := Panel;
      SubPanel.Name := VAR_NomeToolBar_TempoExec + '_Titulo_' + FormatFloat('0', VAR_SEQ_ADV_COMPOS);
      //Panel.Name := VAR_NomeToolBar_TempoExec + FormatFloat('0', QrFavoritos2.RecNo);
      SubPanel.Caption := QrFavoritos2Titulo.Value;
      SubPanel.Height := 24;
      SubPanel.Align := alBottom;

      // Cria e configura Bot�es
      while not QrFavoritos1.Eof do
      begin
        //NewCompo := nil;
        OldCompo := nil;
        with TForm(Reference) do
        begin
          for I := 0 to ComponentCount -1 do
          begin
            Compo := Components[i];
            //
            if Compo is TBitBtn then
            begin
              if Trim(TBitBtn(Compo).Name) = Trim(QrFavoritos1Nome.Value) then
              begin
                OldCompo := Compo;
//                BitBtn.Picture := TBitBtn(Compo).Picture;
//                BitBtn.OnClick := TBitBtn(Compo).OnClick;
              end;
            end;
(*
  TBitBtn       = class(TBitBtn)
  TDBBitBtn     = class(TBitBtn)
  TProCustomGlowButton = class(TBitBtn);
  TBitBtn   = class(TBitBtn)
  TProCustomGlowButton = class(TBitBtn);
*)

          end;
        end;
        //
        if OldCompo <> nil then
        begin
(*
          if Uppercase(QrFavoritos1Classe.Value) = Uppercase('TDBBitBtn') then
          begin
            DBBitBtn := TDBBitBtn.Create(TForm(Reference));
            //
            TBitBtn(NewCompo).PopupMenu        := TBitBtn(OldCompo).PopupMenu;
            TBitBtn(NewCompo).DropDownButton   := TBitBtn(OldCompo).DropDownButton;
            TBitBtn(NewCompo).DropDownPosition := TBitBtn(OldCompo).DropDownPosition;
            TBitBtn(NewCompo).DropDownMenu     := TBitBtn(OldCompo).DropDownMenu;
            TBitBtn(NewCompo).DropDownSplit    := TBitBtn(OldCompo).DropDownSplit;
            //
            DBBitBtn.Layout := blGlyphLeftAdjusted;
            NewCompo := DBBitBtn;
          end else
*)
          if Uppercase(QrFavoritos1Classe.Value) = Uppercase('TBitBtn') then
          begin
            //BitBtn := TBitBtn.Create(TForm(Reference));
            BitBtn := TBitBtn.Create(Panel);
            BitBtn.Parent := Panel;

            BitBtn.Layout := blGlyphLeft;
            NewCompo := BitBtn;
            TBitBtn(NewCompo).PopupMenu := TBitBtn(OldCompo).PopupMenu;
            //TBitBtn(NewCompo).DropDownButton := TBitBtn(OldCompo).DropDownButton;
            //TBitBtn(NewCompo).DropDownPosition := TBitBtn(OldCompo).DropDownPosition;
            //TBitBtn(NewCompo).DropDownMenu := TBitBtn(OldCompo).DropDownMenu;
            //TBitBtn(NewCompo).DropDownSplit := TBitBtn(OldCompo).DropDownSplit;
          end else
(*
          if Uppercase(QrFavoritos1Classe.Value) = Uppercase('TProCustomGlowButton') then
          begin
            ProCustomGlowButton := TProCustomGlowButton.Create(TForm(Reference));
            NewCompo := ProCustomGlowButton;
          end else
*)
          begin
            //BitBtn := TBitBtn.Create(TForm(Reference));
            BitBtn := TBitBtn.Create(Panel);
            BitBtn.Parent := Panel;
            BitBtn.Layout := blGlyphLeft;
            NewCompo := BitBtn;
          end;
          //
          if TBitBtn(OldCompo).Height < 46 then
            TBitBtn(NewCompo).Height := 46
          else
            TBitBtn(NewCompo).Height := TBitBtn(OldCompo).Height;
          //
          if TBitBtn(OldCompo).Width < 112 then
            TBitBtn(NewCompo).Width := 112
          else
            TBitBtn(NewCompo).Width := TBitBtn(OldCompo).Width;
          //
          AtbW := (Colunas * (TBitBtn(NewCompo).Width + 4)) + 4;
          if AtbW < 120 then
            AtbW := 120;
          //
          Panel.Width := AtbW;
          //
          //Panel.AddToolBarControl(TControl(NewCompo));
          //
          L := (((QrFavoritos1.RecNo -1) div 3) * (TBitBtn(NewCompo).Width + 4)) + 4;
          T := (((QrFavoritos1.RecNo -1) mod 3) * (TBitBtn(NewCompo).Height + 4)) + 4;
          //
          TBitBtn(NewCompo).Top  := T;
          TBitBtn(NewCompo).Left := L;
          //BitBtn.OnClick := BitBtnClick;
          //
          VAR_SEQ_ADV_COMPOS := VAR_SEQ_ADV_COMPOS + 1;
          //N := N + 1;
          TBitBtn(NewCompo).Name := VAR_NomeAdvGlowBtn_TempoExec + FormatFloat('000000000', VAR_SEQ_ADV_COMPOS);
          //TBitBtn(NewCompo).Name := VAR_NomeAdvGlowBtn_TempoExec + FormatFloat('000000000', N);
          TBitBtn(NewCompo).Caption := QrFavoritos1Descri1.Value + sLineBreak +
            QrFavoritos1Descri2.Value + sLineBreak + QrFavoritos1Descri3.Value;
          //
          TBitBtn(NewCompo).Glyph := TBitBtn(OldCompo).Glyph;
          TBitBtn(NewCompo).OnClick := TBitBtn(OldCompo).OnClick;
        end;
        QrFavoritos1.Next;
      end;





      //
      QrFavoritos2.Next;
    end;
    //
    QrFavoritos3.Next;
  end;
  PageControl.ActivePageIndex := 0;
  Application.ProcessMessages;
  WindowState := TForm(Reference).WindowState;
  TForm(Reference).WindowState := wsNormal;
  //W := TForm(Reference).Height;
  //L := TForm(Reference).Width;
  //TForm(Reference).Height := 1;
  TForm(Reference).Width  := TForm(Reference).Width + 10;
  TForm(Reference).Invalidate;
  TForm(Reference).Repaint;
  Application.ProcessMessages;
  TForm(Reference).Width  := TForm(Reference).Width - 10;
  TForm(Reference).Invalidate;
  TForm(Reference).Repaint;
  Application.ProcessMessages;
  TForm(Reference).WindowState := WindowState;
  {
  TForm(Reference).Height := W;
  TForm(Reference).Width  := L;
  }
  {
  WindowState := TForm(Reference).WindowState;
  TForm(Reference).WindowState := wsMinimized;
  Application.ProcessMessages;
  TForm(Reference).WindowState := WindowState;
  }
  MyObjects.Informa2(LaAviso0, LaAviso1, False, '...');
end;
{$EndIf} //Berlin

function TDModG.DefineEmpresaSelecionada(const EdEmpresa: TdmkEditCB;
  var Empresa: Integer): Boolean;
var
  Filial: Integer;
begin
  Filial := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a empresa') then
  begin
    Empresa := 0;
    Result := False;
    EdEmpresa.SetFocus;
  end else
  begin
    Empresa := QrEmpresasCodigo.Value;
    Result := True;
  end;
end;

function TDModG.DefineEntidade(const CNPJCPF: String;
  const AvisaErros: Boolean; var Entidade: Integer; var Nome: String): Boolean;
var
  Doc: String;
begin
  Result := False;
  Entidade := 0;
  if (Length(CNPJCPF) = 14) and (Copy(CNPJCPF, 1, 3) = '000') then
    Doc := Copy(CNPJCPF, 4)
  else
    Doc := CNPJCPF;
  ObtemEntidadeDeCNPJCFP(Doc, Entidade);
  if Entidade <> 0 then
  begin
    if ReopenEndereco(Entidade) then
      Nome := QrEnderecoNOME_ENT.Value;
    Result := Nome <> '';
  end else begin
    if AvisaErros then
      Geral.MB_Aviso('Entidade n�o definida para o CNPJ/CPF:' +
      sLineBreak + CNPJCPF);
  end;
end;

function TDModG.ObtemEntidadeDeCNPJCFP(DOC: String; var Entidade: Integer): Boolean;
var
  Txt:String;
  T: Integer;
begin
  Result   := False;
  //Entidade := 0;
  Txt := Geral.SoNumero_TT(DOC);
  T := Length(Txt);
  if T in ([11,14]) then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades');
    case T of
      11: Dmod.QrAux.SQL.Add('WHERE CPF="' + Txt + '"');
      14: Dmod.QrAux.SQL.Add('WHERE CNPJ="' + Txt + '"');
    end;
    Dmod.QrAux.SQL.Add('ORDER BY Codigo'); // para pegar o primeiro cadastro casa haja mais de um!
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    if Dmod.QrAux.Recordcount > 0 then
    begin
      Entidade := Dmod.QrAux.FieldByName('Codigo').AsInteger;
      Result := True;
    end;
  end;
end;

function TDModG.ObtemEntidadeDeCOD_PART(COD_PART: String; var Entidade: Integer): Boolean;
begin
  Result   := False;
  //Entidade := 0;
  if Length(Trim(COD_PART)) > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE COD_PART="' + COD_PART + '"');
    Dmod.QrAux.SQL.Add('ORDER BY Codigo'); // para pegar o primeiro cadastro casa haja mais de um!
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
    if Dmod.QrAux.Recordcount > 0 then
    begin
      Entidade := Dmod.QrAux.FieldByName('Codigo').AsInteger;
      Result := True;
    end;
  end;
end;

function TDModG.ObtemEntidadeDeFilial(Filial: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocFilial, Dmod.MyDB, [
  'SELECT CodEnti, CodFilial ',
  'FROM enticliint ',
  'WHERE CodFilial=' + Geral.FF0(Filial),
  '']);
  //
  Result := QrLocFilialCodEnti.Value;
end;

function TDModG.ObtemEntidadeDeNO_ENT(Nome: String;
  var Entidade: Integer): Boolean;
var
  Txt:String;
  T: Integer;
  //
  Qry: TmySQLQuery;
begin
  Result   := False;
  //Entidade := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    Txt := Trim(Nome);
    T := Length(Txt);
    if T > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo FROM entidades ',
      'WHERE RazaoSocial = "' + Txt + '" ',
      'OR Nome = "' + Txt + '" ', // para pegar o primeiro cadastro casa haja mais de um!
      'ORDER BY Codigo',
      '']);
      if Qry.Recordcount > 0 then
      begin
        Entidade := Qry.FieldByName('Codigo').AsInteger;
        Result := True;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TDModG.ObtemEntidadeNomeDeCodigo(const Entidade: Integer;
  var NO_ENT: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IF(Tipo=0, RazaoSocial, Nome) NO_ENT ',
    'FROM entidades ',
    'WHERE Codigo=' + Geral.FF0(Entidade),
    '']);
    NO_ENT := Qry.FieldByName('NO_ENT').AsString;
    Result := Qry.RecordCount > 0;
  finally
    Qry.Free;
  end;
end;

function TDModG.ObtemFilialDeEntidade(Entidade: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocFilial, Dmod.MyDB, [
  'SELECT CodEnti, CodFilial ',
  'FROM enticliint ',
  'WHERE CodEnti=' + Geral.FF0(Entidade),
  '']);
  Result := QrLocFilialCodFilial.Value;
end;

function TDModG.ObtemFusoHorarioLocal: Double;
var
  TimeZone: TTimeZoneInformation;
begin
  Result := 0;
  //
  GetTimeZoneInformation(TimeZone);
  Result := (((TimeZone.Bias + TimeZone.DaylightBias) / 60) / 24) * -1;
end;

function TDModG.ObtemFusoHorarioServidor: Double;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT ((TIME_TO_SEC(UTC_TIME()) - TIME_TO_SEC(CURTIME())) / 60) * -1 TimeZone ',
    '']);
  Result := ((Dmod.QrAux.FieldByName('TimeZone').AsFloat / 60) / 24);
end;

function TDModG.ObtemHorarioTZD_JahCorrigido(Empresa: Integer): Double;
var
  HorVerao, TZD_UTC: Double;
  Data: TDateTime;
var
  Qry: TmySQLQuery;
  //Hoj,
  Ini, Fim: TDateTime;
begin
  TZD_UTC := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SYSDATE() Agora, emp.hVeraoIni, ',
      'emp.hVeraoFim, emp.TZD_UTC, emp.TZD_UTC_Auto ',
      'FROM paramsemp emp ',
      'WHERE Codigo=' + Geral.FF0(Empresa),
      '']);
    //
    Ini := Qry.FieldByName('hVeraoIni').AsdateTime;
    Fim := Qry.FieldByName('hVeraoFim').AsdateTime;
    //
    Data := ObtemAgora();
    if
      (
        Qry.FieldByName('TZD_UTC_Auto').AsInteger = 0
      )
      and
      (
      (  (Data > Ini) and (Data < Fim + 1)  ) or
      (  (Data > Ini) and (Ini > Fim + 1)  )
      )
    then
      HorVerao := 1 / 24
    else
      HorVerao := 0;
    //
    TZD_UTC := Qry.FieldByName('TZD_UTC').AsFloat;
    //
  finally
    Qry.Free;
  end;
  //
  //Result := ObtemAgora() + TZD_UTC() + HorVerao;
  Result := TZD_UTC + HorVerao;
end;

function TDModG.ObtemLctA_Para_Web(TabLctA: String): String;
var
  P: Integer;
begin
  Result := Trim(TabLctA);
  if Result <> '' then
  begin
    P := pos('.', TabLctA);
    if P > 0 then
      Result := Copy(TabLctA, P + 1); 
  end;
end;


function TDModG.ObtemMunicipioDeEntidade(Entidade: Integer): Integer;
begin
  ReopenEnti(Entidade);
  Result := Trunc(QrEntiCodMunici.Value);
end;

function TDModG.MediaMesesReparcelamento2(DataIni: TDateTime; Parcelas: Integer;
var ArrDatas: array of TDateTime): Double;
(*function MediaMesesReparcelamento2($DiasPrimeira, $MesesAtrazo, $Parcelas)
{
  $query = new Query();
  $query->Database =  GetDBModule()->Database1;
  $query->LimitStart = '-1';
  $query->LimitCount = '-1';
  //
  $rtn2 = array($Parcelas + 1);
  //
  //$Ini = floor(floor($DataPrimeira/86400) * 86400);
  $Ini = $DiasPrimeira;
  //
  $rtn = 0;
  //$rtn2[] = 0;
  for( $i = 1; $i <= $Parcelas; $i++ )
  {
    $query->Close();
    $xyz = $i-1 . ' MONTH)) Dias';
    $query->SQL =
      'SELECT TO_DAYS(DATE_ADD(FROM_DAYS(' . $DiasPrimeira . '), INTERVAL ' . $xyz ;
    $query->open();
    //echo($query->SQL);
    $Datay = CalculaDataDeposito2($query->Dias);
    $rtn += $Datay;
    $rtn2[] = $Datay;
    //$rtn .= date('d/m/Y', $Datax) . ' - ' . date('d/m/Y', $Datay) . '<br/>';
  }
  $rtn2[0] = ((($rtn / $Parcelas) - $Ini) / 30) + $MesesAtrazo;
  return $rtn2;
}
*)
var
  NewData, DepData: TDateTime;
  (*Meses, *)Dias, I, D: Integer;
begin
  Dias := 0;
  ArrDatas[0] := DataIni;
  for I := 1 to Parcelas - 1 do
  begin
    NewData := IncMonth(DataIni, I);
    DepData := UMyMod.CalculaDataDeposito(NewData);
    D := Trunc(DepData - NewData);
    Dias := Dias + D;
    //
    ArrDatas[I] := IncMonth(DataIni, I) + D;
  end;
  Result := ((((Parcelas - 1) / 2) * 30) + Dias) / 30;
  //Show Message(FloatToStr(Result) + ' - ' + Geral.FF0(Dias) + '(Dias)');
end;

function TDModG.MediaMesesReparcelamento3(DataIni: TDateTime; Parcelas: Integer;
var ArrDatas: array of TDateTime): Double;
var
  DataI, NewData, DepData: TDateTime;
  (*Dias,*) I, D: Integer;
  //
  Meses: Double;
begin
  //Dias := 0;
  DataI := Int(DataIni);
  ArrDatas[0] := DataI;
  Meses := MesesEntreDatas(0, DataI);
  for I := 1 to Parcelas - 1 do
  begin
    NewData := IncMonth(DataI, I);
    DepData := UMyMod.CalculaDataDeposito(NewData);
    D := Trunc(DepData - NewData);
    //Dias := Dias + D;
    //
    ArrDatas[I] := IncMonth(DataI, I) + D;
    Meses := Meses + MesesEntreDatas(0, ArrDatas[I]);
  end;
  //Result := ((((Parcelas - 1) / 2) * 30) + Dias) / 30;
  Result := Meses / Parcelas;
end;

function TDModG.AtualizaPrecosGraGruVal2(GraGruX, Entidade: Integer): MyArrayD2;
var
  GraGru1, GraCusPrc, Controle: Integer;
  CustoPreco: Double;
  SQLType: TSQLType;
  //
  Qtde, Valr, Parc, UltimaCompra, PrecoMedio: Double;
begin
  Result[1] := 0;
  Result[2] := 0;
  //
  //Obtem �ltima compra
  QrSMIN.Close;
  QrSMIN.Params[00].AsInteger := GraGruX;
  QrSMIN.Params[01].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQueryApenas(QrSMIN);
  //
  Qtde := QrSMINQtde.Value;
  Valr := QrSMINCustoAll.Value;
  //
  if Qtde = 0 then
    UltimaCompra := 0
  else
    UltimaCompra := Valr / Qtde;
  //
  //Obtem soma da quantidade compra efetivada
  QrSMIX.Close;
  QrSMIX.Params[00].AsInteger := GraGruX;
  QrSMIX.Params[01].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQueryApenas(QrSMIX);
  //
  GraGru1 := QrSMIXGraGru1.Value;
  //
  if Qtde < QrSMIXQTDE.Value then
  begin
    QrSMIN.Next;
    while not QrSMIN.Eof do
    begin
      if Qtde + QrSMINQtde.Value > QrSMIXQTDE.Value then
      begin
        Parc := (QrSMINCustoAll.Value / QrSMINQtde.Value) * (QrSMIXQTDE.Value - Qtde);
        Valr := Valr + Parc;
        Qtde := QrSMIXQTDE.Value;
        Break;
      end else begin
        Qtde := Qtde + QrSMINQtde.Value;
        Valr := Valr + QrSMINCustoAll.Value;
      end;
      QrSMIN.Next;
    end;
  end;
  if Qtde = 0 then
    PrecoMedio := 0
  else
    PrecoMedio := Valr / Qtde;
  //
  // Atualizar �ltima compra
  if UltimaCompra > 0 then
  begin
    Result[1] := UltimaCompra;
    CustoPreco := UltimaCompra;
    GraCusPrc  := 4; // �ltima compra
    QrGraGruVal.Close;
    QrGraGruVal.Params[00].AsInteger := GraGruX;
    QrGraGruVal.Params[01].AsInteger := GraCusPrc;
    QrGraGruVal.Params[02].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQueryApenas(QrGraGruVal);
    //
    Controle := QrGraGruValControle.Value;
    if Controle = 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY_Def('gragruval', 'Controle', stIns, 0);
      SQLType  := stIns;
    end else begin
      SQLType  := stUpd;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
    'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
    GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [Controle], True) then ;
  end;
  //
  // Atualizar Pre�o m�dio
  if PrecoMedio > 0 then
  begin
    Result[2] := PrecoMedio;
    CustoPreco := PrecoMedio;
    GraCusPrc  := 5; // Pre�o m�dio
    QrGraGruVal.Close;
    QrGraGruVal.Params[00].AsInteger := GraGruX;
    QrGraGruVal.Params[01].AsInteger := GraCusPrc;
    QrGraGruVal.Params[02].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQueryApenas(QrGraGruVal);
    //
    Controle := QrGraGruValControle.Value;
    if Controle = 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY_Def('gragruval', 'Controle', stIns, 0);
      SQLType  := stIns;
    end else begin
      SQLType  := stUpd;
    end;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
      'GraGruX', 'GraGru1', 'GraCusPrc', 'CustoPreco', 'Entidade'], ['Controle'], [
      GraGruX, GraGru1, GraCusPrc, CustoPreco, Entidade], [Controle], True)
    then
      ;
  end;
end;

procedure TDModG.AtualizaPrecosGraGruVal2_All(PB: TProgressBar; Entidade: Integer);
begin
  Screen.Cursor := crHourGlass;
  QrX.Close;
  UnDmkDAC_PF.AbreQueryApenas(QrX);
  PB.Position := 0;
  PB.Max := QrX.RecordCount;
  PB.Update;
  Application.ProcessMessages;
  while not QrX.Eof do
  begin
    PB.Position := PB.Position + 1;
    PB.Update;
    Application.ProcessMessages;
    //
    AtualizaPrecosGraGruVal2(QrXGraGruX.Value, Entidade);
    QrX.Next;
  end;
  Screen.Cursor := crDefault;
  Geral.MB_Aviso('Atualiza��o finalizada!');
  PB.Position := 0;
end;

procedure TDModG.AtualizaCambioMda;
var
  Query: TmySQLQuery;
  Codigo, CambioMdaAtz: Integer;
  CampoExiste: Boolean;
begin
  if Dmod.MyDB.Connected then
  begin
    try
      CampoExiste    := False;
      Query          := TmySQLQuery.Create(Dmod);
      Query.Database := Dmod.MyDB;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SHOW COLUMNS ',
        'FROM controle ',
        'LIKE "CambioMdaAtz" ',
        '']);
      if Query.RecordCount > 0 then
        CampoExiste := True;
      //
      if CampoExiste then
      begin
        ReopenControle();
        //
        CambioMdaAtz := QrControle.FieldByName('CambioMdaAtz').AsInteger;
        //
        if CambioMdaAtz = 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
            'SELECT Codigo ',
            'FROM cambiomda ',
            'WHERE Sigla = "R$" ',
            'AND Cod_BC = 0 ',
            '']);
          if Query.RecordCount > 0 then
          begin
            Codigo := Query.FieldByName('Codigo').AsInteger;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cambiomda', False,
              ['Cod_BC', 'Simbolo'], ['Codigo'],
              ['790', 'BRL'], [Codigo], False) then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False,
                ['CambioMdaAtz'], ['Codigo'], ['1'], [1], False);
            end;
          end;
        end;
      end;
    finally
      Query.Free;
    end;
  end;
end;

{

procedure TForm1.CriarAtalho(ANomeArquivo, AParametros, ADiretorioInicial,

  ANomedoAtalho, APastaDoAtalho: string);

var

  MeuObjeto: IUnknown;

  MeuSLink: IShellLink;

  MeuPFile: IPersistFile;

  Diretorio: string;

  wNomeArquivo: WideString;

  MeuRegistro: TRegIniFile;
 
begin
 
  //Cria e instancia os objetos usados para criar o atalho
 
  MeuObjeto := CreateComObject(CLSID_ShellLink);
 
  MeuSLink := MeuObjeto as IShellLink;
 
  MeuPFile := MeuObjeto as IPersistFile;
 
  with MeuSLink do
 
  begin
 
    SetArguments(PChar(AParametros));
 
    SetPath(PChar(ANomeArquivo));
 
    SetWorkingDirectory(PChar(ADiretorioInicial));
 
  end;
 
  //Pega endere�o da pasta Desktop do Windows
 
  MeuRegistro :=
 
    TRegIniFile.Create('Software\MicroSoft\Windows\CurrentVersion\Explorer');
 
  Diretorio := MeuRegistro.ReadString('Shell Folders', 'Desktop', '');
 
  wNomeArquivo := Diretorio + '\' + ANomedoAtalho + '.lnk';
 
  //Cria de fato o atalho na tela
 
  MeuPFile.Save(PWChar(wNomeArquivo), False);
 
  MeuRegistro.Free;
 
end;


Leia mais em: Criando e excluindo atalhos do desktop http://www.devmedia.com.br/criando-e-excluindo-atalhos-do-desktop/980#ixzz2PuITETBt
procedure TForm1.AlterarIcone(Atalho: string; NovoIcone: PAnsiChar);
var
  MeuObjeto: IUnknown;
  MeuSLink: IShellLink;
  MeuPFile: IPersistFile;
  wNomeArquivo: WideString;
begin
  MeuObjeto := CreateComObject(CLSID_ShellLink);
  MeuSLink := MeuObjeto as IShellLink;
  MeuPFile := MeuObjeto as IPersistFile;
  MeuSLink.SetIconLocation(PChar(edEndIcone.Text), 0);
  wNomeArquivo := edArquivo.Text;
  MeuPFile.Save(PWChar(wNomeArquivo), False);
end;

Leia mais em: Mudando o �cone de um atalho http://www.devmedia.com.br/mudando-o-icone-de-um-atalho/2068#ixzz2PuHuI5Kl








procedure TForm1.ApagarAtalho(FileName: string);
 
var
 
  Directory: string;
 
  WFileName: WideString;
 
  MyReg: TRegIniFile;
 
begin
 
  MyReg :=
 
    TRegIniFile.Create('Software\MicroSoft\Windows\CurrentVersion\Explorer');
 
  Directory := MyReg.ReadString('Shell Folders', 'Desktop', '');
 
  if FileExists(Directory + '\' + FileName) then
 
  begin
 
    if MessageDlg('Confirma a exclus�o do atalho: ' + sLineBreak +
 
      Directory + '\' + FileName, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
 
      DeleteFile(Directory + '\' + FileName);
 
  end
 
  else
 
    MessageDlg('O atalho: ' + sLineBreak + Directory + '\' + FileName + sLineBreak +
 
      'n�o existe ou j� foi exclu�do.',
 
      mtInformation, [mbOK], 0);
 
  MyReg.Free;
 
end;
 
 

No evento onClick do bot�o Apagar o atalho basta chamar o procedimento informando o par�metro necess�rio: Nome do atalho.

Conclus�es

O uso de interfaces facilita a cria��o de deteminadas tarefas que seu sistema deve desenvolver.




Adriano Santos
� especialista Delphi, ex-editor das revistas ClubeDelphi e WebMobile, atualmente � Product Manager na SPK Tecnologia - empresa distribuidora de diversas solu��es tecnol�gicas � e especialista em Microsoft Team Foundation Server.






    4 COMENT�RIOS












Juan
Link quebrado os GIF � encontrados

[h� +1 ano] - Responder


 
 
 






Juan 
Link quebrado os GIF � encontrados

[h� +1 ano] - Responder


 

 






Side Controll 
Aparece somente o Atalho em branco no Desktop nao chama o EXE. Poderia me dizer oq estou a fazer de errado. A baixo a chamada da Funcao CriarAtalho(''DesktopAtalho.exe'','''',''C:\Demo\Delphi\Criando e excluindo atalhos do desktop\'', ''Atalho Desktop Teste'',''C:\Demo\Delphi\Criando e excluindo atalhos do desktop\''); Obrigado, juan 

[h� +1 ano] - Responder


 
 





Alysson Gon�alves

respondendo a pergunta do amigo ae...
o metodo correto para chamar a procedure �:

criaratalho(application.exename, '''''''', extractfilepath(application.exename), ''''atalho teste'''', '''''''');

obs. 1:
o parametro "apastadoatalho" servia para criar atalho no menu iniciar (recurso q o autor retirou, ou alguma outra pessoa retirou, sei la), mas resumindo; nele vc informava em qual pasta do menu iniciar vc quer add o atalho. ex:  ''''acess�rios/'''' dai a procedure colocaria o atalho na pasta Acess�rios, ou ent�o criaria uma pasta com o nome q vc colocasse ali.
link de uma procedure q cria atalho no menu iniciar e no desktop (retirado de http://www.webmundi.com/delphi/dfuncaof.asp?SubTipo=Arquivos).

link direto: http://geocities.yahoo.com.br/admsqlbr/arquivos/CShortcut.zip

obs. 2:
em nenhum dos dois exemplos ensina colocar um comentario no atalho (q aparece como um hint, quando colocamos o mouse sobre o atalho.
mas fazer isso � bem simples:

adicione um parametro na procedura para receber o comentario. ex:

procedure TForm1.CriarAtalho(anomearquivo, adesc, aparametros, adiretorioinicial,

  anomedoatalho, apastadoatalho: string);
(nao esqueca de acertar la onde � declarada a procedure...^^)

depois, em:

  with meuslink do

  begin

    setarguments(pchar(aparametros));

    setpath(pchar(anomearquivo));

    setworkingdirectory(pchar(adiretorioinicial));

  end;

adicione:

  with meuslink do

  begin

    setarguments(pchar(aparametros));

    setpath(pchar(anomearquivo));
 
    setDescription(PChar(adesc));

 
    setworkingdirectory(pchar(adiretorioinicial));
 
  end;
pronto ae agora � so chamar a procedure corretamente q fica tudo certo.

tambem � possivel atribuir hotkeys e varios outras funcionalidades...
que podem ser encontradas no help do windows "Win32 Programmer''''s Reference"
que vem com o delph. (pelo menos no 7), que por padr�o e adicionada no 
"Menu Iniciar\Programas\Borland Delphi 7\Help\MS SDK Help Files\Win32 Programmer''''s Reference"
(isso se vc tiver adicionado atalhos no menu, :p)

ae, a resposta fico meio grandinha mas e que eu demorei tanto para achar q agora tenho obriga��o de passar pra frente^^

 
[h� +1 ano] - Responder


 
 


cursos relacionados


�ltimos do autor






object QrParamsEmpCNPJ_CPF: TWideStringField
  FieldName = 'CNPJ_CPF'
  Size = 18
end
object QrParamsEmpCodigo: TIntegerField
  FieldName = 'Codigo'
  Origin = 'paramsemp.Codigo'
end
object QrParamsEmpMoeda: TIntegerField
  FieldName = 'Moeda'
  Origin = 'paramsemp.Moeda'
end
object QrParamsEmpSituacao: TIntegerField
  FieldName = 'Situacao'
  Origin = 'paramsemp.Situacao'
end
object QrParamsEmpRegrFiscal: TIntegerField
  FieldName = 'RegrFiscal'
  Origin = 'paramsemp.RegrFiscal'
end
object QrParamsEmpFretePor: TSmallintField
  FieldName = 'FretePor'
  Origin = 'paramsemp.FretePor'
end
object QrParamsEmpTipMediaDD: TSmallintField
  FieldName = 'TipMediaDD'
  Origin = 'paramsemp.TipMediaDD'
end
object QrParamsEmpFatSemPrcL: TSmallintField
  FieldName = 'FatSemPrcL'
  Origin = 'paramsemp.FatSemPrcL'
end
object QrParamsEmpBalQtdItem: TFloatField
  FieldName = 'BalQtdItem'
  Origin = 'paramsemp.BalQtdItem'
end
object QrParamsEmpAssociada: TIntegerField
  FieldName = 'Associada'
  Origin = 'paramsemp.Associada'
end
object QrParamsEmpFatSemEstq: TSmallintField
  FieldName = 'FatSemEstq'
  Origin = 'paramsemp.FatSemEstq'
end
object QrParamsEmpAssocModNF: TIntegerField
  FieldName = 'AssocModNF'
  Origin = 'paramsemp.AssocModNF'
end
object QrParamsEmpCtaProdVen: TIntegerField
  FieldName = 'CtaProdVen'
  Origin = 'paramsemp.CtaProdVen'
end
object QrParamsEmpfaturasep: TWideStringField
  FieldName = 'faturasep'
  Origin = 'paramsemp.FaturaSep'
  Size = 1
end
object QrParamsEmpfaturaseq: TSmallintField
  FieldName = 'faturaseq'
  Origin = 'paramsemp.FaturaSeq'
end
object QrParamsEmpFaturaDta: TSmallintField
  FieldName = 'FaturaDta'
  Origin = 'paramsemp.FaturaDta'
end
object QrParamsEmpTxtProdVen: TWideStringField
  FieldName = 'TxtProdVen'
  Origin = 'paramsemp.TxtProdVen'
  Size = 100
end
object QrParamsEmpLogo3x1: TWideStringField
  FieldName = 'Logo3x1'
  Origin = 'paramsemp.Logo3x1'
  Size = 255
end
object QrParamsEmpTipCalcJuro: TSmallintField
  FieldName = 'TipCalcJuro'
  Origin = 'paramsemp.TipCalcJuro'
end
object QrParamsEmpSimplesFed: TSmallintField
  FieldName = 'SimplesFed'
  Origin = 'paramsemp.SimplesFed'
end
object QrParamsEmpSimplesEst: TSmallintField
  FieldName = 'SimplesEst'
  Origin = 'paramsemp.SimplesEst'
end
object QrParamsEmpCerDigital: TWideMemoField
  FieldName = 'CerDigital'
  Origin = 'paramsemp.CerDigital'
  BlobType = ftMemo
  Size = 4
end
object QrParamsEmpDirNFeGer: TWideStringField
  FieldName = 'DirNFeGer'
  Origin = 'paramsemp.DirNFeGer'
  Size = 255
end
object QrParamsEmpDirNFeAss: TWideStringField
  FieldName = 'DirNFeAss'
  Origin = 'paramsemp.DirNFeAss'
  Size = 255
end
object QrParamsEmpDirEnvLot: TWideStringField
  FieldName = 'DirEnvLot'
  Origin = 'paramsemp.DirEnvLot'
  Size = 255
end
object QrParamsEmpDirRec: TWideStringField
  FieldName = 'DirRec'
  Origin = 'paramsemp.DirRec'
  Size = 255
end
object QrParamsEmpDirPedRec: TWideStringField
  FieldName = 'DirPedRec'
  Origin = 'paramsemp.DirPedRec'
  Size = 255
end
object QrParamsEmpDirProRec: TWideStringField
  FieldName = 'DirProRec'
  Origin = 'paramsemp.DirProRec'
  Size = 255
end
object QrParamsEmpDirDen: TWideStringField
  FieldName = 'DirDen'
  Origin = 'paramsemp.DirDen'
  Size = 255
end
object QrParamsEmpDirPedCan: TWideStringField
  FieldName = 'DirPedCan'
  Origin = 'paramsemp.DirPedCan'
  Size = 255
end
object QrParamsEmpDirCan: TWideStringField
  FieldName = 'DirCan'
  Origin = 'paramsemp.DirCan'
  Size = 255
end
object QrParamsEmpDirPedInu: TWideStringField
  FieldName = 'DirPedInu'
  Origin = 'paramsemp.DirPedInu'
  Size = 255
end
object QrParamsEmpDirInu: TWideStringField
  FieldName = 'DirInu'
  Origin = 'paramsemp.DirInu'
  Size = 255
end
object QrParamsEmpDirPedSit: TWideStringField
  FieldName = 'DirPedSit'
  Origin = 'paramsemp.DirPedSit'
  Size = 255
end
object QrParamsEmpDirSit: TWideStringField
  FieldName = 'DirSit'
  Origin = 'paramsemp.DirSit'
  Size = 255
end
object QrParamsEmpDirPedSta: TWideStringField
  FieldName = 'DirPedSta'
  Origin = 'paramsemp.DirPedSta'
  Size = 255
end
object QrParamsEmpDirSta: TWideStringField
  FieldName = 'DirSta'
  Origin = 'paramsemp.DirSta'
  Size = 255
end
object QrParamsEmpUF_WebServ: TWideStringField
  FieldName = 'UF_WebServ'
  Origin = 'paramsemp.UF_WebServ'
  Size = 2
end
object QrParamsEmpSiglaCustm: TWideStringField
  FieldName = 'SiglaCustm'
  Origin = 'paramsemp.SiglaCustm'
  Size = 15
end
object QrParamsEmpInfoPerCuz: TSmallintField
  FieldName = 'InfoPerCuz'
  Origin = 'paramsemp.InfoPerCuz'
end
object QrParamsEmpPathLogoNF: TWideStringField
  FieldName = 'PathLogoNF'
  Origin = 'paramsemp.PathLogoNF'
  Size = 255
end
object QrParamsEmpCtaServico: TIntegerField
  FieldName = 'CtaServico'
  Origin = 'paramsemp.CtaServico'
end
object QrParamsEmpTxtServico: TWideStringField
  FieldName = 'TxtServico'
  Origin = 'paramsemp.TxtServico'
  Size = 100
end
object QrParamsEmpDupServico: TWideStringField
  FieldName = 'DupServico'
  Origin = 'paramsemp.DupServico'
  Size = 3
end
object QrParamsEmpDupProdVen: TWideStringField
  FieldName = 'DupProdVen'
  Origin = 'paramsemp.DupProdVen'
  Size = 3
end
object QrParamsEmpPedVdaMudLista: TSmallintField
  FieldName = 'PedVdaMudLista'
  Origin = 'paramsemp.PedVdaMudLista'
end
object QrParamsEmpPedVdaMudPrazo: TSmallintField
  FieldName = 'PedVdaMudPrazo'
  Origin = 'paramsemp.PedVdaMudPrazo'
end
object QrParamsEmpNFeSerNum: TWideStringField
  FieldName = 'NFeSerNum'
  Origin = 'paramsemp.NFeSerNum'
  Size = 255
end
object QrParamsEmpUF_DistDFeInt: TWideStringField
  DisplayWidth = 10
  FieldName = 'UF_DistDFeInt'
  Size = 10
end
object QrParamsEmpUF_Servico: TWideStringField
  DisplayWidth = 10
  FieldName = 'UF_Servico'
  Origin = 'paramsemp.UF_Servico'
  Size = 10
end
object QrParamsEmpFisFax: TWideStringField
  FieldName = 'FisFax'
  Origin = 'paramsemp.FisFax'
  Size = 30
end
object QrParamsEmpFisContNom: TWideStringField
  FieldName = 'FisContNom'
  Origin = 'paramsemp.FisContNom'
  Size = 28
end
object QrParamsEmpFisContTel: TWideStringField
  FieldName = 'FisContTel'
  Origin = 'paramsemp.FisContTel'
  Size = 30
end
object QrParamsEmpReg10: TSmallintField
  FieldName = 'Reg10'
  Origin = 'paramsemp.Reg10'
end
object QrParamsEmpReg11: TSmallintField
  FieldName = 'Reg11'
  Origin = 'paramsemp.Reg11'
end
object QrParamsEmpReg50: TSmallintField
  FieldName = 'Reg50'
  Origin = 'paramsemp.Reg50'
end
object QrParamsEmpReg51: TSmallintField
  FieldName = 'Reg51'
  Origin = 'paramsemp.Reg51'
end
object QrParamsEmpReg53: TSmallintField
  FieldName = 'Reg53'
  Origin = 'paramsemp.Reg53'
end
object QrParamsEmpReg54: TSmallintField
  FieldName = 'Reg54'
  Origin = 'paramsemp.Reg54'
end
object QrParamsEmpReg55: TSmallintField
  FieldName = 'Reg55'
  Origin = 'paramsemp.Reg55'
end
object QrParamsEmpReg56: TSmallintField
  FieldName = 'Reg56'
  Origin = 'paramsemp.Reg56'
end
object QrParamsEmpReg57: TSmallintField
  FieldName = 'Reg57'
  Origin = 'paramsemp.Reg57'
end
object QrParamsEmpReg60M: TSmallintField
  FieldName = 'Reg60M'
  Origin = 'paramsemp.Reg60M'
end
object QrParamsEmpReg60A: TSmallintField
  FieldName = 'Reg60A'
  Origin = 'paramsemp.Reg60A'
end
object QrParamsEmpReg60D: TSmallintField
  FieldName = 'Reg60D'
  Origin = 'paramsemp.Reg60D'
end
object QrParamsEmpReg60I: TSmallintField
  FieldName = 'Reg60I'
  Origin = 'paramsemp.Reg60I'
end
object QrParamsEmpReg60R: TSmallintField
  FieldName = 'Reg60R'
  Origin = 'paramsemp.Reg60R'
end
object QrParamsEmpReg61: TSmallintField
  FieldName = 'Reg61'
  Origin = 'paramsemp.Reg61'
end
object QrParamsEmpReg61R: TSmallintField
  FieldName = 'Reg61R'
  Origin = 'paramsemp.Reg61R'
end
object QrParamsEmpReg70: TSmallintField
  FieldName = 'Reg70'
  Origin = 'paramsemp.Reg70'
end
object QrParamsEmpReg71: TSmallintField
  FieldName = 'Reg71'
  Origin = 'paramsemp.Reg71'
end
object QrParamsEmpReg74: TSmallintField
  FieldName = 'Reg74'
  Origin = 'paramsemp.Reg74'
end
object QrParamsEmpReg75: TSmallintField
  FieldName = 'Reg75'
  Origin = 'paramsemp.Reg75'
end
object QrParamsEmpReg76: TSmallintField
  FieldName = 'Reg76'
  Origin = 'paramsemp.Reg76'
end
object QrParamsEmpReg77: TSmallintField
  FieldName = 'Reg77'
  Origin = 'paramsemp.Reg77'
end
object QrParamsEmpReg85: TSmallintField
  FieldName = 'Reg85'
  Origin = 'paramsemp.Reg85'
end
object QrParamsEmpReg86: TSmallintField
  FieldName = 'Reg86'
  Origin = 'paramsemp.Reg86'
end
object QrParamsEmpReg90: TSmallintField
  FieldName = 'Reg90'
  Origin = 'paramsemp.Reg90'
end
object QrParamsEmpSINTEGRA_Path: TWideStringField
  FieldName = 'SINTEGRA_Path'
  Origin = 'paramsemp.SINTEGRA_Path'
  Size = 255
end
object QrParamsEmpDirDANFEs: TWideStringField
  FieldName = 'DirDANFEs'
  Origin = 'paramsemp.DirDANFEs'
  Size = 255
end
object QrParamsEmpDirNFeProt: TWideStringField
  FieldName = 'DirNFeProt'
  Origin = 'paramsemp.DirNFeProt'
  Size = 255
end
object QrParamsEmpPreMailAut: TIntegerField
  FieldName = 'PreMailAut'
  Origin = 'paramsemp.PreMailAut'
end
object QrParamsEmpPreMailCan: TIntegerField
  FieldName = 'PreMailCan'
  Origin = 'paramsemp.PreMailCan'
end
object QrParamsEmpversao: TFloatField
  FieldName = 'versao'
  Origin = 'paramsemp.versao'
end
object QrParamsEmpfaturasep: TWideStringField
  FieldName = 'faturasep'
  Origin = 'paramsemp.FaturaSep'
  Size = 1
end
object QrParamsEmpfaturaseq: TSmallintField
  FieldName = 'faturaseq'
  Origin = 'paramsemp.FaturaSeq'
end
object QrParamsEmpFaturaNum: TSmallintField
  FieldName = 'FaturaNum'
  Origin = 'paramsemp.FaturaNum'
end
object QrParamsEmpNFeSerVal: TDateField
  FieldName = 'NFeSerVal'
  Origin = 'paramsemp.NFeSerVal'
end
object QrParamsEmpNFeSerAvi: TSmallintField
  FieldName = 'NFeSerAvi'
  Origin = 'paramsemp.NFeSerAvi'
end
object QrParamsEmpide_mod: TSmallintField
  FieldName = 'ide_mod'
  Origin = 'paramsemp.ide_mod'
end
object QrParamsEmpide_tpImp: TSmallintField
  FieldName = 'ide_tpImp'
  Origin = 'paramsemp.ide_tpImp'
end
object QrParamsEmpide_tpAmb: TSmallintField
  FieldName = 'ide_tpAmb'
  Origin = 'paramsemp.ide_tpAmb'
end
object QrParamsEmpAppCode: TSmallintField
  FieldName = 'AppCode'
  Origin = 'paramsemp.AppCode'
end
object QrParamsEmpAssDigMode: TSmallintField
  FieldName = 'AssDigMode'
  Origin = 'paramsemp.AssDigMode'
end
object QrParamsEmpEntiTipCto: TIntegerField
  FieldName = 'EntiTipCto'
  Origin = 'paramsemp.EntiTipCto'
end
object QrParamsEmpMyEmailNFe: TWideStringField
  FieldName = 'MyEmailNFe'
  Origin = 'paramsemp.MyEmailNFe'
  Size = 255
end
object QrParamsEmpCRT: TSmallintField
  FieldName = 'CRT'
  Origin = 'paramsemp.CRT'
end
object QrParamsEmpCSOSN: TIntegerField
  FieldName = 'CSOSN'
  Origin = 'paramsemp.CSOSN'
end
object QrParamsEmpDirSchema: TWideStringField
  FieldName = 'DirSchema'
  Origin = 'paramsemp.DirSchema'
  Size = 255
end
object QrParamsEmppCredSNAlq: TFloatField
  FieldName = 'pCredSNAlq'
  Origin = 'paramsemp.pCredSNAlq'
end
object QrParamsEmppCredSNMez: TIntegerField
  FieldName = 'pCredSNMez'
  Origin = 'paramsemp.pCredSNMez'
end
object QrParamsEmpUsaReferen: TSmallintField
  FieldName = 'UsaReferen'
  Origin = 'paramsemp.UsaReferen'
end
object QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField
  FieldName = 'SPED_EFD_IND_PERFIL'
  Origin = 'paramsemp.SPED_EFD_IND_PERFIL'
  Size = 1
end
object QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField
  FieldName = 'SPED_EFD_IND_ATIV'
  Origin = 'paramsemp.SPED_EFD_IND_ATIV'
end
object QrParamsEmpSPED_EFD_CadContador: TIntegerField
  FieldName = 'SPED_EFD_CadContador'
  Origin = 'paramsemp.SPED_EFD_CadContador'
end
object QrParamsEmpSPED_EFD_CRCContador: TWideStringField
  FieldName = 'SPED_EFD_CRCContador'
  Origin = 'paramsemp.SPED_EFD_CRCContador'
  Size = 15
end
object QrParamsEmpSPED_EFD_EscriContab: TIntegerField
  FieldName = 'SPED_EFD_EscriContab'
  Origin = 'paramsemp.SPED_EFD_EscriContab'
end
object QrParamsEmpSPED_EFD_EnderContab: TSmallintField
  FieldName = 'SPED_EFD_EnderContab'
  Origin = 'paramsemp.SPED_EFD_EnderContab'
end
object QrParamsEmpSPED_EFD_Path: TWideStringField
  FieldName = 'SPED_EFD_Path'
  Origin = 'paramsemp.SPED_EFD_Path'
  Size = 255
end
object QrParamsEmpNFeItsLin: TSmallintField
  FieldName = 'NFeItsLin'
  Origin = 'paramsemp.NFeItsLin'
end
object QrParamsEmpNFeFTRazao: TSmallintField
  FieldName = 'NFeFTRazao'
  Origin = 'paramsemp.NFeFTRazao'
end
object QrParamsEmpNFeFTEnder: TSmallintField
  FieldName = 'NFeFTEnder'
  Origin = 'paramsemp.NFeFTEnder'
end
object QrParamsEmpNFeFTFones: TSmallintField
  FieldName = 'NFeFTFones'
  Origin = 'paramsemp.NFeFTFones'
end
object QrParamsEmpNFeMaiusc: TSmallintField
  FieldName = 'NFeMaiusc'
end
object QrParamsEmpNFeShowURL: TWideStringField
  FieldName = 'NFeShowURL'
  Size = 255
end
object QrParamsEmpNFetpEmis: TSmallintField
  FieldName = 'NFetpEmis'
end
object QrParamsEmpSCAN_Ser: TIntegerField
  FieldName = 'SCAN_Ser'
end
object QrParamsEmpSCAN_nNF: TIntegerField
  FieldName = 'SCAN_nNF'
end
object QrParamsEmpDirNFeRWeb: TWideStringField
  FieldName = 'DirNFeRWeb'
  Size = 255
end
object QrParamsEmpNFSeMetodo: TIntegerField
  FieldName = 'NFSeMetodo'
end
object QrParamsEmpNFSeVersao: TFloatField
  FieldName = 'NFSeVersao'
end
object QrParamsEmpNFSeSerieRps: TWideStringField
  FieldName = 'NFSeSerieRps'
  Size = 5
end
object QrParamsEmpDpsNumero: TIntegerField
  FieldName = 'DpsNumero'
end
object QrParamsEmpNFSeWSProducao: TWideStringField
  FieldName = 'NFSeWSProducao'
  Size = 255
end
object QrParamsEmpNFSeWSHomologa: TWideStringField
  FieldName = 'NFSeWSHomologa'
  Size = 255
end
object QrParamsEmpNFSeAmbiente: TSmallintField
  FieldName = 'NFSeAmbiente'
end
object QrParamsEmpNFSeTipCtoMail: TIntegerField
  FieldName = 'NFSeTipCtoMail'
end
object QrParamsEmpPreMailEveCCe: TIntegerField
  FieldName = 'PreMailEveCCe'
end
object QrParamsEmpEntiTipCt1: TIntegerField
  FieldName = 'EntiTipCt1'
end
object QrParamsEmpDirEveEnvLot: TWideStringField
  FieldName = 'DirEveEnvLot'
  Size = 255
end
object QrParamsEmpDirEveRetLot: TWideStringField
  FieldName = 'DirEveRetLot'
  Size = 255
end
object QrParamsEmpDirEvePedCCe: TWideStringField
  FieldName = 'DirEvePedCCe'
  Size = 255
end
object QrParamsEmpDirEveRetCCe: TWideStringField
  FieldName = 'DirEveRetCCe'
  Size = 255
end
object QrParamsEmpDirEveProcCCe: TWideStringField
  FieldName = 'DirEveProcCCe'
  Size = 255
end
object QrParamsEmpDirEvePedCan: TWideStringField
  FieldName = 'DirEvePedCan'
  Size = 255
end
object QrParamsEmpDirEveRetCan: TWideStringField
  FieldName = 'DirEveRetCan'
  Size = 255
end
object QrParamsEmpEstq0UsoCons: TSmallintField
  FieldName = 'Estq0UsoCons'
end
object QrParamsEmpDirNFSeDPSGer: TWideStringField
  FieldName = 'DirNFSeDPSGer'
  Size = 255
end
object QrParamsEmpDirNFSeDPSAss: TWideStringField
  FieldName = 'DirNFSeDPSAss'
  Size = 255
end
object QrParamsEmpNFSeCertDigital: TWideStringField
  FieldName = 'NFSeCertDigital'
  Size = 255
end
object QrParamsEmpNFSeCertValidad: TDateField
  FieldName = 'NFSeCertValidad'
end
object QrParamsEmpNFSeCertAviExpi: TSmallintField
  FieldName = 'NFSeCertAviExpi'
end
object QrParamsEmpNFeVerStaSer: TFloatField
  FieldName = 'NFeVerStaSer'
end
object QrParamsEmpNFeVerEnvLot: TFloatField
  FieldName = 'NFeVerEnvLot'
end
object QrParamsEmpNFeVerConLot: TFloatField
  FieldName = 'NFeVerConLot'
end
object QrParamsEmpNFeVerCanNFe: TFloatField
  FieldName = 'NFeVerCanNFe'
end
object QrParamsEmpNFeVerInuNum: TFloatField
  FieldName = 'NFeVerInuNum'
end
object QrParamsEmpNFeVerConNFe: TFloatField
  FieldName = 'NFeVerConNFe'
end
object QrParamsEmpNFeVerLotEve: TFloatField
  FieldName = 'NFeVerLotEve'
end
object QrParamsEmpNFeVerConDes: TFloatField
  FieldName = 'NFeVerConDes'
end
object QrParamsEmpNFeVerConsCad: TFloatField
  FieldName = 'NFeVerConsCad'
end
object QrParamsEmpNFeVerDistDFeInt: TFloatField
  FieldName = 'NFeVerDistDFeInt'
end
object QrParamsEmpNFeVerDowNFe: TFloatField
  FieldName = 'NFeVerDowNFe'
end
object QrParamsEmpUF_MDeMDe: TWideStringField
  DisplayWidth = 10
  FieldName = 'UF_MDeMDe'
  Size = 10
end
object QrParamsEmpUF_MDeDes: TWideStringField
  DisplayWidth = 10
  FieldName = 'UF_MDeDes'
  Size = 10
end
object QrParamsEmpUF_MDeNFe: TWideStringField
  DisplayWidth = 10
  FieldName = 'UF_MDeNFe'
  Size = 10
end
object QrParamsEmpNFeNT2013_003LTT: TSmallintField
  FieldName = 'NFeNT2013_003LTT'
end
object QrParamsEmpPediVdaNElertas: TIntegerField
  FieldName = 'PediVdaNElertas'
end
object QrParamsEmpTZD_UTC: TFloatField
  FieldName = 'TZD_UTC'
end
object QrParamsEmpTZD_UTC_Str: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TZD_UTC_Str'
  Size = 6
  Calculated = True
end
object QrParamsEmpNFe_indFinalCpl: TSmallintField
  FieldName = 'NFe_indFinalCpl'
end
object QrParamsEmpNFSe_indFinalCpl: TSmallintField
  FieldName = 'NFSe_indFinalCpl'
end
object QrParamsEmpCartEmisHonFun: TIntegerField
  FieldName = 'CartEmisHonFun'
end
object QrParamsEmpCtaServicoPg: TIntegerField
  FieldName = 'CtaServicoPg'
end
object QrParamsEmpCtaProdCom: TIntegerField
  FieldName = 'CtaProdCom'
end

}
end.

