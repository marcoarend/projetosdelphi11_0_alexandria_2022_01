unit UMySQLDB;

interface

uses System.SysUtils, Vcl.Forms, Vcl.StdCtrls, Vcl.ComCtrls, vcl.Dialogs, Classes,
  Winapi.Windows, mySQLDbTables, System.Variants, UnDmkEnums, dmkGeral, DmkDAC_PF,
  UnInternalConsts, {$IfNDef NAO_USA_DB_GERAL} UnAll_Jan, {$EndIf}
  mySQLDirectQuery, UnDmkProcFunc, UnGrl_Vars, Data.DB, System.UITypes;

type
  TStringArray = array of string;
  TNoDropCreateTable = (ndctDropAndCreate=0, ndctNoCreateInsAll=1,
                       ndctNoCreateAndInsIgnore=2, ndctNoCreateAndReplace=3,
                       ndctNoCreateAndReplaceCompleateIns=4);
  TUSQLDB = class(TObject)
  private
    { Private declarations }
    procedure ConfiguraDataBase(DataBase: TmySQLDatabase; DataBaseName: String);
    function  ObtemSQL_DataHora(UTC: Boolean): TStringArray;
    function  ObtemMySQLBinDir(DataBase: TmySQLDatabase): String;
    function  SenhaDesconhecida(DB: TmySQLDatabase; QrUpd: TmySQLQuery): Boolean;
    function  MudaSenhaBD(DB: TmySQLDatabase; QrUpd: TmySQLQuery; Senha,
              Usuario: String; MsgErr: Boolean): Boolean;
  public
    { Public declarations }
    // D M o d . C r e a t e
    procedure VerificaDmod_1_Create_DBs(DBs: array of TmySQLDatabase);
    function  VerificaDmod_2_DefineVarsDBConnect((*var ConnType: Integer; var Host:
              String; var Port: Integer; var DBName, UserName, Senha: String*)): String;
    procedure VerificaDmod_3_RedefineSenhaSeNecessario(ZZDB: TmySQLDatabase; QrUpd: TmySQLQuery);
    procedure VerificaDmod_4_PreparaParaConectar();
    procedure VerificaDmod_5_DefineBDPrincipal(MyDB: TmySQLDatabase; QrAux:
              tmySQLQuery);
    procedure VerificaDmod_6_AtualizaBDSeNecessario(const MyDB: TmySQLDatabase;
              const QrAux: tmySQLQuery; var VerificaDBTerc: Boolean);
    procedure VerificaDmod_7_CriaDataModule(InstanceClass: TComponentClass; var Reference);
    // B A C K U P   /   S I N C R O N I S M O
    function  Backup_CriaNome(Prefixo: String; Agora: TDateTime): String;
    // D A T A   /   H O R A
    function  ObtemAgora(DB: TmySQLDatabase; UTC: Boolean = False): TDateTime;
    function  ObtemDataHora(DB: TmySQLDatabase; var Hoje: TDateTime;
              var Agora: TTime; UTC: Boolean = False): Boolean;
    // M Y S Q L
    function  ObtemPathMySQLBin(DataBase: TmySQLDatabase): String;
    function  TestarConexaoMySQL(Form: TForm; Host, User, Pass,
              BancoDados: String; Port: Integer; Avisa: Boolean): Boolean;
    function  TabelaExiste(Tabela: String; Database: TmySQLDatabase): Boolean;
    function  CampoExiste(Tabela, Campo: String; Database: TmySQLDatabase): Boolean;
    procedure ExecutaPingServidor(DataBase: array of TmySQLDatabase);
    procedure MsgNaoConectaDB(DB: TmySQLDatabase; ProcName: String; Erro: Exception);
    procedure MsgBeforeConectaDB(Sender: TObject);
    // E X C L U S � E S
    function  ImpedeExclusaoPeloNomeDaTabela(Tabela: String): Boolean;
    function  ExcluiRegistroInt1(Pergunta, Tabela, Campo: String; Inteiro1:
              Integer; DB: TmySQLDatabase): Integer;
    {$IfNDef NAO_USA_DB_GERAL}
    function  ExcluiRegistroBig1(Pergunta, Tabela, Campo: String; BigInt1:
              LongInt; DB: TmySQLDatabase): Integer;
    {$EndIf}
    // S Q L S
    function  ObtemValorCampo(DB: TmySQLDatabase; Valor: Variant; ValCampo,
              Campo, Tabela: String): String;
    function  SQL_DB_I_U(const DB: TmySQLDatabase; const Tipo: TSQLType; const
              Tabela: String; const Auto_increment: Boolean; const SQLCampos,
              SQLIndex: array of String; const ValCampos, ValIndex:
              array of Variant; const UserDataAlterweb, IGNORE: Boolean;
              var RecordsAffected: Int64; const ComplUpd: String = '';
              const InfoSQLOnError: Boolean = True; InfoErro: Boolean = True):
              Boolean;
    function  SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  SQLInsUpdAlterIdx(QrUpd: TmySQLQuery; Tipo: TSQLType;
              Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex,
              SQLAlterIdx: array of String; ValCampos, ValIndex,
              ValAlterIdx: array of Variant; UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;

    function  CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String): Boolean;
    function  CarregaSQLInsUpdAlterIdx(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex, SQLAlterIdx: array of String;
              ValCampos, ValIndex, ValAlterIdx: array of Variant;
              UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String): Boolean;
    function  CarregaSQLDB_I_U(Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String; var SQL: String): Boolean;
    {$IfNDef NAO_USA_SEL_RADIO_GROUP}
    function  ObtemVersaoAppDB(DataBase: TmySQLDatabase;
              // Ini 2019-01-25
              //Empresa: Integer; AppWeb: Boolean = False): Integer;
                App: TVerApp = verappAppFile;(*verappIndef=0, verappAppFile=1, verappAppApp=2, verappWebFile=3*)
                Empresa: Integer = 0):
                //ini 2022-01-11
                //Integer;
                Int64;
                // fim 2022-01-11
              // Fim 2019-01-25
    {$EndIf}
    procedure AtualizaCampoAtivo_Unico(QrUpd: TmySQLQuery; Tabela, Campo_Ativo,
              Campo_Primario: String; Valor_Ativo, Valor_Primario: Integer;
              Acao: TSetaCampoAtivo);
    procedure AtualizaCampoAtivo_Todos(QrUpd: TmySQLQuery; Tabela: String;
              QrToSet: TmySQLQuery; Database: TmySQLDatabase; Campo_Ativo, Campo_Primario: String;
              Acao: TSetaCampoAtivo);
    // B A C K U P   /   S I N C R O N I S M O
    function  Sincro_AtualizaDados(DB: TmySQLDatabase; AgoraUTC: TDateTime;
              Empresa: Integer; SincroTotal: Boolean; Termino: Boolean): Boolean;
    function  Sincro_ValidaDBUnidirecional(DataBase: TmySQLDatabase;
              QueryAux: TmySQLQuery; NomeDB: String; var Msg: String): Boolean;
    {$IfNDef NAO_USA_SEL_RADIO_GROUP}
    function  Sincro_ExecutaUnidirecional(DBOri, DBDes: TmySQLDataBase;
              Empresa: Integer; PathMySQL, Destino, ArqNome, Ori_Host, Ori_DB,
              Ori_User, Ori_Pass: String; Ori_Porta: Integer; Des_Host, Des_DB,
              Des_User, Des_Pass: String; Des_Porta: Integer; Tabs: String;
              SincTotal: Boolean; AgoraUTC: TDateTime; MemoResul: TMemo;
              var Msg: String; LabelAviso: TLabel = nil): Boolean;
    {$EndIf}
    function  Backup_CriaNomeArquivo(DB: String; Agora: TDateTime): String;
    function  Backup_ConfiguraDestinoArquivo(Diretorio, ArqNome: String;
              NomeCurto: Boolean): String;
    {$IfNDef NAO_USA_SEL_RADIO_GROUP}
    function  Backup_Restaura(DataBase: TmySQLDatabase; Arquivo: String;
              SelecionaDB: Boolean; var Msg: String; LabelStatus: TStaticText = nil;
              Progress: TProgressBar = nil): Boolean;
    {$EndIf}
    function  Backup_Executa(DataBase: TmySQLDatabase; PathMySQL, Destino,
              ArqNome, Host, DbNome, Usuario, Senha: String; Porta: Integer;
              MemoResul: TMemo; DbCria, DbXML: Boolean;
              NoDropCreateTable: TNoDropCreateTable; var Msg: String;
              LabelAviso: TLabel = nil; Tabelas: String = '';
              max_allowed_packet: String = '2097152';
              net_buffer_length: String = '32768';
              default_character_set: String = 'latin1'): String;
    function  Backup_ObtemTipoTxt(Tipo: Integer): String;
    function  Backup_ConfiguraTipos(): TStringList;
    procedure Backups_ExcluiAntigos(UltExe: TDateTime; Dias: Integer; Diretorio,
              Nome: String);
    // O B T E R   V A L O R E S   D E  C A M P O S
    function  GetFlu(DB: TmySQLDatabase; Tabela, Campo, SQL_Condition: String): Double;
    // A C E S S O   A   V A L O R E S   D E  C A M P O S
    function  DtH(mySQLDirectQuery: TmySQLDirectQuery; Campo: String): TDateTime;
    function  Flu(mySQLDirectQuery: TmySQLDirectQuery; Campo: String): Double;
    function  Int(mySQLDirectQuery: TmySQLDirectQuery; Campo: String): Integer;
    function  Str(mySQLDirectQuery: TmySQLDirectQuery; Campo: String): String;
    //
    function  DtHIdx(mySQLDirectQuery: TmySQLDirectQuery; Indice: Integer): TDateTime;
    function  FluIdx(mySQLDirectQuery: TmySQLDirectQuery; Indice: Integer): Double;
    function  IntIdx(mySQLDirectQuery: TmySQLDirectQuery; Indice: Integer): Integer;
    function  StrIdx(mySQLDirectQuery: TmySQLDirectQuery; Indice: Integer): String;
    //function  Str(Campo: Nome): String;
    function  v_i(Qr: TmySQLQuery; Campo: String): Integer;
    function  v_f(Qr: TmySQLQuery; Campo: String): Double;
    function  v_t(Qr: TmySQLQuery; Campo: String): TDateTime;
    function  v_x(Qr: TmySQLQuery; Campo: String): String;
    //
    function  ConectaDB(DB: TmySQLDatabase; MyCodLoc: String): Boolean;
    procedure ConfiguraDB(ForcaConfiguracao: Boolean);
    function  TabelasExistem(Tabelas: array of String; Database: TmySQLDatabase): Boolean;

end;

const
  coMySQLDumpName = 'mysqldump.exe';
var
  USQLDB: TUSQLDB;

implementation

uses {$IfNDef NAO_USA_DB_GERAL} ModuleGeral, {$EndIf} UnGrl_Consts,
{$IfDef sVeriConex}
//
{$Else}
  VerificaConexoes,
{$EndIf}
  UnGrl_Geral, UnMyObjects, Senha, Module;

{ TUSQLDB }

procedure TUSQLDB.AtualizaCampoAtivo_Todos(QrUpd: TmySQLQuery; Tabela: String;
  QrToSet: TmySQLQuery; Database: TmySQLDatabase; Campo_Ativo, Campo_Primario: String; Acao: TSetaCampoAtivo);
const
  sProcName = 'TUSQLDB.AtualizaCampoAtivo(_Unico)';
var
  PrimaIni, Valor_Ativo, Valor_Primario: Integer;
  Novo_Valor: Integer;
begin
  if QrToSet.State = dsInactive then
  begin
    Geral.MB_Aviso('A tabela ' + QrToSet.Name + ' est� fechada!');
    Exit;
  end;
  //
  PrimaIni := QrToSet.FieldByName(Campo_Primario).AsInteger;
  //
  Screen.Cursor := crHourGlass;
  QrToSet.DisableControls;
  try
    QrToSet.First;
    while not QrToSet.Eof do
    begin
      Valor_Primario := QrToSet.FieldByName(Campo_Primario).AsInteger;
      Valor_Ativo    := QrToSet.FieldByName(Campo_Ativo).AsInteger;
      Novo_Valor := -1;
      case Acao of
        TSetaCampoAtivo.scaDesativa: Novo_Valor := 0;
        TSetaCampoAtivo.scaAtiva: Novo_Valor := 1;
        TSetaCampoAtivo.scaInverte:
        begin
          if Valor_Ativo = 0 then
            Novo_Valor := 1
          else
            Novo_Valor := 0;
        end;
        else
        begin
          Geral.MB_Erro('Valor para o campo "Ativo" n�o definido em ' + sProcName);
          Exit;
        end;
      end;
      //
      SQLInsUpd(QrUpd, stUpd, Tabela, False,
        [Campo_Ativo], [Campo_Primario],
        //[Valor_Ativo], [Valor_Primario], True);
        [Novo_Valor], [Valor_Primario], False);
      //
      QrToSet.Next;
    end;
    //
    UnDmkDAC_PF.AbreQuery(QrToSet, Database);
    QrToSet.Locate(Campo_Primario, PrimaIni, []);
    //
  finally
    QrToSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TUSQLDB.AtualizaCampoAtivo_Unico(QrUpd: TmySQLQuery; Tabela, Campo_Ativo,
  Campo_Primario: String; Valor_Ativo, Valor_Primario: Integer; Acao:
  TSetaCampoAtivo);
const
  sProcName = 'TUSQLDB.AtualizaCampoAtivo(_Unico)';
var
  Novo_Valor: Integer;
begin
  Novo_Valor := -1;
  case Acao of
    TSetaCampoAtivo.scaDesativa: Novo_Valor := 0;
    TSetaCampoAtivo.scaAtiva: Novo_Valor := 1;
    TSetaCampoAtivo.scaInverte:
    begin
      if Valor_Ativo = 0 then
        Novo_Valor := 1
      else
        Novo_Valor := 0;
    end;
    else
    begin
      Geral.MB_Erro('Valor para o campo "Ativo" n�o definido em ' + sProcName);
      Exit;
    end;
  end;
  //
  SQLInsUpd(QrUpd, stUpd, Tabela, False,
    [Campo_Ativo], [Campo_Primario],
    //[Valor_Ativo], [Valor_Primario], True);
    [Novo_Valor], [Valor_Primario], False);
end;

function TUSQLDB.TabelaExiste(Tabela: String; Database: TmySQLDatabase): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SHOW TABLES LIKE "' + Tabela + '"',
      '']);
    if Qry.RecordCount > 0 then
      Result := True;
  finally
    Qry.Free;
  end;
end;

function TUSQLDB.TabelasExistem(Tabelas: array of String; Database: TmySQLDatabase): Boolean;
var
  Qry: TmySQLQuery;
  QtdAll, QtdeOK: Integer;
  I: Integer;
begin
  Result := False;
  QtdAll := Length(Tabelas);
  QtdeOK := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    for I := Low(Tabelas)to High(Tabelas) do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
        'SHOW TABLES LIKE "' + Tabelas[I] + '"',
        '']);
      if Qry.RecordCount = 0 then
        Exit;
    end;
    //
    Result := True;
  finally
    Qry.Free;
  end;
end;

function TUSQLDB.CampoExiste(Tabela, Campo: String; Database: TmySQLDatabase): Boolean;
var
  Query: TmySQLQuery;
begin
  Result := False;
  if Database.Connected then
  begin
    if TabelaExiste(Tabela, Database) then
    begin
      try
        Query := TmySQLQuery.Create(TDataModule(Database.Owner));
        Query.Database := Database;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
          'SHOW COLUMNS ',
          'FROM ' + Tabela,
          'LIKE "' + Campo + '" ',
          '']);
        //
        Result := Query.RecordCount > 0;
      finally
        Query.Free;
      end;
    end;
  end else
    Geral.MB_Aviso('Database n�o conectado: ' + Database.Name)
end;

function TUSQLDB.ObtemAgora(DB: TmySQLDatabase; UTC: Boolean = False): TDateTime;
var
  Ano, Mes, Dia, Hora, Minuto, Segundo: Word;
  Hoje: TDateTime;
  Agora: TTime;
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DB.Owner);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, ObtemSQL_DataHora(UTC));
    //
    Ano     := Qry.FieldByName('Ano').AsInteger;
    Mes     := Qry.FieldByName('Mes').AsInteger;
    Dia     := Qry.FieldByName('Dia').AsInteger;
    Hora    := Qry.FieldByName('Hora').AsInteger;
    Minuto  := Qry.FieldByName('Minuto').AsInteger;
    Segundo := Qry.FieldByName('Segundo').AsInteger;
    Hoje    := EncodeDate(Ano, Mes, Dia);
    Agora   := EncodeTime(Hora, Minuto, Segundo, 0);
    Result  := Hoje + Agora;
  finally
    Qry.Free;
  end;
end;

function TUSQLDB.ObtemDataHora(DB: TmySQLDatabase; var Hoje: TDateTime;
  var Agora: TTime; UTC: Boolean = False): Boolean;
var
  Ano, Mes, Dia, Hora, Minuto, Segundo: Word;
  Qry: TmySQLQuery;
begin
  Hoje  := 0;
  Agora := 0;
  Qry   := TmySQLQuery.Create(DB.Owner);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, ObtemSQL_DataHora(UTC));
    //
    Ano     := Qry.FieldByName('Ano').AsInteger;
    Mes     := Qry.FieldByName('Mes').AsInteger;
    Dia     := Qry.FieldByName('Dia').AsInteger;
    Hora    := Qry.FieldByName('Hora').AsInteger;
    Minuto  := Qry.FieldByName('Minuto').AsInteger;
    Segundo := Qry.FieldByName('Segundo').AsInteger;
    //
    Hoje  := EncodeDate(Ano, Mes, Dia);
    Agora := EncodeTime(Hora, Minuto, Segundo, 0);
    //
    Result := True;
    //
    Qry.Free;
  except
    Qry.Free;
    raise;
  end;
end;

function TUSQLDB.Backup_CriaNome(Prefixo: String; Agora: TDateTime): String;
begin
  Result := Prefixo + '_' + dmkPF.ObtemDataStr(Agora, False);
end;

function TUSQLDB.Backup_CriaNomeArquivo(DB: String; Agora: TDateTime): String;
begin
  Result := DB + '_' + dmkPF.ObtemDataStr(Agora, False) + '.sql';
end;

procedure TUSQLDB.Backups_ExcluiAntigos(UltExe: TDateTime; Dias: Integer;
  Diretorio, Nome: String);
var
  Arq, Ano, NomeArq, Arqiv: String;
  Agora, DtaMin, DtaArq: TDateTime;
  BuscaResul: TSearchRec;
begin
  if (Dias <> 0) and (UltExe < Now) then //Se dias 0 n�o faz e verificar se o rel�gio n�o est� errado para n�o excluir todos
  begin
    if SetCurrentDir(Diretorio) then
    begin
      Agora  := Now;
      Ano    := Geral.FDT(Agora, 21);
      DtaMin := Agora - Dias;
      //
      if findfirst(Nome + '_' + Ano + '*', faAnyFile, BuscaResul) = 0 then
      begin
        repeat
          NomeArq := BuscaResul.Name;
          DtaArq  := BuscaResul.TimeStamp;
          //
          if DtaArq < DtaMin  then
          begin
            Arqiv := Diretorio + '\' + NomeArq;
            //
            if FileExists(Arq) then
              System.SysUtils.DeleteFile(Arq)
            else if DirectoryExists(Arq) then
              dmkPF.RemoveArquivosDeDiretorio(Arq);
          end;
        until
          FindNext(BuscaResul) <> 0;
        //
        System.SysUtils.FindClose(BuscaResul);
      end;
    end;
  end;
end;

function TUSQLDB.Backup_ConfiguraTipos: TStringList;
var
  Tipos: TStringList;
begin
  Tipos := TStringList.Create;
  try
    Tipos.Add('Backup');
    Tipos.Add('Sincronismo');
  finally
    Result := Tipos;
  end;
end;

function TUSQLDB.Backup_ObtemTipoTxt(Tipo: Integer): String;
var
  Lst: TStringList;
begin
  Lst := TStringList.Create;
  try
    Lst := Backup_ConfiguraTipos();
    //
    case Tipo of
        0: Result := Lst[0];
        1: Result := Lst[1];
      else Result := 'Tipo n�o implementado';
    end;
  finally
    Lst.Free;
  end;
end;

{$IfNDef NAO_USA_SEL_RADIO_GROUP}
function TUSQLDB.Backup_Restaura(DataBase: TmySQLDatabase; Arquivo: String;
  SelecionaDB: Boolean; var Msg: String; LabelStatus: TStaticText = nil;
  Progress: TProgressBar = nil): Boolean;
var
  Exec: String;

  procedure ExecutaSQL(S: String);
  var
    X: String;
  begin
    if Length(S) > 1 then X := S[1]+S[2] else x := '';
    if x <> '--' then
    if x <> '/*' then
    begin
      if S <> '' then
      begin
        Exec := Exec + sLineBreak + S;
        if Exec[Length(Exec)] = ';' then
        begin
          if Exec <> '' then
          begin
            DataBase.Execute(Exec);
            Exec := '';
          end;
        end;
      end;
    end else begin
      if Exec <> '' then
      begin
        DataBase.Execute(Exec);
        Exec := '';
      end;
    end;
  end;

const
  TxtAchouDB = 'USE `';
var
  AchouDB, Continua: Boolean;
  Faltando, Corrido: TDateTime;
  F: TextFile;
  S, DBName, StrUsaDB: String;
  Next, Step, I, TotDecl, Declara, Tam, Resul: Integer;
  DBs: array[0..1] of String;
begin
  Result := False;
  try
    Msg      := '';
    Exec     := '';
    AchouDB  := False;
    Step     := 1000;
    Next     := Step;
    StrUsaDB := '';
    //
    if FileExists(Arquivo) then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      TotDecl := 0;
      //
      if LabelStatus <> nil then
      begin
        LabelStatus.Caption := 'Calculando quantidade de TotDecl��es...';
        LabelStatus.Refresh;
        LabelStatus.Update;
      end;
      //
      while not Eof(F) do
      begin
        TotDecl := TotDecl + 1;
        //
        if TotDecl = Next then
        begin
          if LabelStatus <> nil then
            LabelStatus.Caption := 'Calculando quantidade de TotDecl��es...' + Geral.FF0(TotDecl);
          Next := Next + Step;
        end;
        //
        Readln(F, S);
        //
        if pos (TxtAchouDB, s) > 0 then
        begin
          DBName  := Copy(S, Length(TxtAchouDB));
          DBName  := Geral.Substitui(DBName, Char(96), '');
          DBName  := Geral.Substitui(DBName, ';', '');
          AchouDB := True;
          //
          if DataBase.DatabaseName <> DBName then
          begin
            if SelecionaDB then
            begin
              DBs[0] := DataBase.DatabaseName;
              DBs[1] := DBName;
              //
              Resul := MyObjects.SelRadioGroup('Sele��o de Base de dados',
                         'Selecione a Base de Dados', DBs, 0, 0);
              //
              if Resul >= 0 then
              begin
                try
                  UnDmkDAC_PF.ConectaMyDB_DAC(DataBase, DBs[Resul], DataBase.Host,
                    DataBase.Port,  VAR_SQLUSER, DataBase.UserPassword,
                    (*Desconecta*)True, (*Configura*)True, (*Conecta*)True, True);
                  //
                  Continua := DataBase.Connected;
                except
                  Continua := False;
                end;
                if Continua = False then
                begin
                  Msg := 'ERRO: Falha ao conectar no banco de dados!';
                  Exit;
                end;
              end else
              begin
                Msg := 'Banco de dados n�o definido!';
                Exit;
              end;
            end else
            begin
              Msg := 'Banco de dados n�o definido!';
              Exit;
            end;
          end;
        end;
      end;
      //
      Reset(F);
      //
      Declara  := 0;
      Faltando := 0;
      //
      if Progress <> nil then
      begin
        Progress.Position := 0;
        Progress.Max      := TotDecl;
      end;
      //
      while not Eof(F) do
      begin
        Declara := Declara + 1;
        Corrido := Now() - Corrido;
        //
        if TotDecl > 0 then
          Faltando := Corrido * TotDecl / Declara;
        //
        if Progress <> nil then
        begin
          Progress.Position := Progress.Position + 1;
          Progress.Refresh;
        end;
        //
        if LabelStatus <> nil then
        begin
          LabelStatus.Caption := 'Executando declara��o n� ' + Geral.FF0(Declara) +
                                  ' de ' + Geral.FF0(TotDecl) + '  -  ' +
                                  FormatFloat('0.00', Declara / TotDecl * 100) +
                                  '%  -  Tempo corrido: ' +
                                  FormatDateTime(VAR_FORMATTIME, Corrido) +
                                  '  Tempo restante: ' +
                                  FormatDateTime(VAR_FORMATTIME, Faltando - Corrido) +
                                  ' (aprox.)  Tempo total: ' +
                                  FormatDateTime(VAR_FORMATTIME, Faltando) +
                                  ' (aprox.)';
          LabelStatus.Refresh;
          LabelStatus.Update;
        end;
        Readln(F, S);
        //
        if Declara <> 1 then
          ExecutaSQL(S);
        //
        Application.ProcessMessages;
      end;
      if LabelStatus <> nil then
      begin
        LabelStatus.Caption := 'Executadas ' + Geral.FF0(Declara) +
                               ' declara��es de ' + Geral.FF0(TotDecl) + '  =  ' +
                               FormatFloat('0.0000', Declara / TotDecl * 100) +
                               '%  -  Tempo Total: ' + FormatDateTime(VAR_FORMATTIME, Corrido);
      end;
      CloseFile(F);
      //
      Msg    := 'Backup restaurado!';
      Result := True;
    end else
      Msg := 'O arquivo informado n�o foi localizado ou est� inacess�vel!';
  finally
    if Progress <> nil then
      Progress.Position := 0;
  end;
end;
{$EndIf}

function TUSQLDB.CarregaSQLDB_I_U(Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean; SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String; var SQL: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  //
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' campos e ' +
      Geral.FF0(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' �ndices e ' +
      Geral.FF0(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('Aviso! Existem ' + Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Aviso('AVISO: O status da a��o est� definida como ' +
    '"' + DmkEnums.NomeTipoSQL(Tipo) + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  //QrUpd.SQL.Clear;
  SQL := '';
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      //QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ')
      SQL := SQL + 'INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ' + sLineBreak
    else
      //QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ');
      SQL := SQL + 'INSERT INTO ' + Lowercase(Tab) + ' SET ' + sLineBreak ;
  end else begin
    //QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    SQL := SQL + 'UPDATE ' + Lowercase(Tab) + ' SET ' + sLineBreak;
    if UserDataAlterweb then
      //QrUpd.SQL.Add('AlterWeb=1, ');
      SQL := SQL + 'AlterWeb=1, ' + sLineBreak;
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    // 2011-12-10
    if SQLCampos[i] = CO_JOKE_SQL then
    begin
      if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
        //QrUpd.SQL.Add(ValCampos[i] + ', ')
        SQL := SQL + ValCampos[i] + ', ' + sLineBreak
      else
        //QrUpd.SQL.Add(ValCampos[i]);
        SQL := SQL + ValCampos[i] + sLineBreak;
    end else
    // fim 2011-12-10
    begin
      if Pos('AES_ENCRYPT', ValCampos[i]) > 0 then
        Valor := ValCampos[i]
      else
        Valor := Geral.VariavelToString(ValCampos[i]);
      //
      if (i < j) or UserDataAlterweb then
        //QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
        SQL := SQL + SQLCampos[i] + '=' + Valor + ', ' + sLineBreak
      else
        //QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
        SQL := SQL + SQLCampos[i] + '=' + Valor + sLineBreak
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    //QrUpd.SQL.Add('AWServerID=' + VAR_TXT_AWServerID + ', AWStatSinc=' + Geral.FF0(Integer(Tipo)) + ', ');
    SQL := SQL + 'AWServerID=' + VAR_TXT_AWServerID + ', AWStatSinc=' + Geral.FF0(Integer(Tipo)) + ', ' + sLineBreak;
    if Tipo = stIns then
      //QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
      SQL := SQL + 'DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO) + sLineBreak
    else
      //QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
      SQL := SQL + 'DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO) + sLineBreak;
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else
  begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        //QrUpd.SQL.Add(Liga + ValIndex[i])
        SQL := SQL + Liga + ValIndex[i] + sLineBreak
      else
      // fim 2011-12-02
      begin
        Valor := Geral.VariavelToString(ValIndex[i]);
        //QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        SQL := SQL + Liga + SQLIndex[i] + '=' + Valor + sLineBreak;
      end;
    end;
  end;
  if Trim(ComplUpd) <> '' then
  begin
    //QrUpd.SQL.Add(ComplUpd);
    SQL := SQL + ComplUpd + sLineBreak;
  end;
  //QrUpd.SQL.Add(';');
  SQL := SQL + ';' + sLineBreak;
  Result := True;
  //
  //Geral.MB_Info(SQL);
end;

function TUSQLDB.CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
  NotNull: Boolean;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  //
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' campos e ' +
      Geral.FF0(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' �ndices e ' +
      Geral.FF0(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('Aviso! Existem ' + Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Aviso('AVISO: O status da a��o est� definida como ' +
    '"' + DmkEnums.NomeTipoSQL(Tipo) + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ');
  end else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    //if Uppercase(SQLCampos[i]) = Uppercase('infAdic_infCpl') then  2019-11-29
      //Geral.MB_Info(SQLCampos[i]);
    // 2011-12-10
    // ini 2020-11-24
     NotNull := ValCampos[i] <> null;
    // fim 2020-11-24
    if NotNull and (SQLCampos[i] = CO_JOKE_SQL) then
    begin
      if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
        QrUpd.SQL.Add(ValCampos[i] + ', ')
      else
        QrUpd.SQL.Add(ValCampos[i]);
    end else
    // fim 2011-12-10
    begin
      if NotNull and (Pos('AES_ENCRYPT', ValCampos[i]) > 0) then
        Valor := ValCampos[i]
      else
        Valor := Geral.VariavelToString(ValCampos[i]);
      //
      if (i < j) or UserDataAlterweb then
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
      else
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    QrUpd.SQL.Add('AWServerID=' + VAR_TXT_AWServerID + ', AWStatSinc=' + Geral.FF0(Integer(Tipo)) + ', ');
    if Tipo = stIns then
      QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
    else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else
  begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        QrUpd.SQL.Add(Liga + ValIndex[i])
      else
      // fim 2011-12-02
      begin
        Valor := Geral.VariavelToString(ValIndex[i]);
        QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
      end;
    end;
  end;
  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;
  QrUpd.SQL.Add(';');
  Result := True;
  //
  //Geral.MB_SQL(?,QrUpd);
end;

function TUSQLDB.CarregaSQLInsUpdAlterIdx(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex,
  SQLAlterIdx: array of String; ValCampos, ValIndex,
  ValAlterIdx: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String): Boolean;
const
  ProcName = 'CarregaSQLInsUpdAlterIdx';
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
  NotNull: Boolean;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  //
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' campos e ' +
      Geral.FF0(j+1) + ' valores para estes campos em "' + ProcName + '"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' �ndices e ' +
      Geral.FF0(j+1) + ' valores para estes �ndices em "' + ProcName + '"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  i := High(SQLAlterIdx);
  j := High(ValAlterIdx);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' �ndices alter�veis e ' +
      Geral.FF0(j+1) + ' valores alter�veis para estes �ndices alter�veis em "' + ProcName + '"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('Aviso! Existem ' + Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "' + ProcName + '"!' +
      sLineBreak + 'Tabela: "' + Tabela + '"');
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Aviso('AVISO: O status da a��o est� definida como ' +
    '"' + DmkEnums.NomeTipoSQL(Tipo) + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ');
  end else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    //if Uppercase(SQLCampos[i]) = Uppercase('infAdic_infCpl') then  2019-11-29
      //Geral.MB_Info(SQLCampos[i]);
    // 2011-12-10
    // ini 2020-11-24
     NotNull := ValCampos[i] <> null;
    // fim 2020-11-24
    if NotNull and (SQLCampos[i] = CO_JOKE_SQL) then
    begin
      if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
        QrUpd.SQL.Add(ValCampos[i] + ', ')
      else
        QrUpd.SQL.Add(ValCampos[i]);
    end else
    // fim 2011-12-10
    begin
      if NotNull and (Pos('AES_ENCRYPT', ValCampos[i]) > 0) then
        Valor := ValCampos[i]
      else
        Valor := Geral.VariavelToString(ValCampos[i]);
      //
      if (i < j) or UserDataAlterweb then
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
      else
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
    end;
  end;
  //

  j := High(SQLAlterIdx);
  if (Tipo = stUpd) and (j > -1) then
  begin
    j := High(SQLIndex);
    for i := Low(SQLIndex) to j do
    begin
       NotNull := ValIndex[i] <> null;
      // fim 2020-11-24
      if NotNull and (SQLIndex[i] = CO_JOKE_SQL) then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValIndex[i]) <> '') then
          QrUpd.SQL.Add(ValIndex[i] + ', ')
        else
          QrUpd.SQL.Add(ValIndex[i]);
      end else
      begin
        if NotNull and (Pos('AES_ENCRYPT', ValIndex[i]) > 0) then
          Valor := ValIndex[i]
        else
          Valor := Geral.VariavelToString(ValIndex[i]);
        //
        if (i < j) or UserDataAlterweb then
          QrUpd.SQL.Add(SQLIndex[i] + '=' + Valor + ', ')
        else
          QrUpd.SQL.Add(SQLIndex[i] + '=' + Valor);
      end;
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    QrUpd.SQL.Add('AWServerID=' + VAR_TXT_AWServerID + ', AWStatSinc=' + Geral.FF0(Integer(Tipo)) + ', ');
    if Tipo = stIns then
      QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
    else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
    // N�o faz nada
  end else
  begin
    j := High(SQLAlterIdx);
    if (Tipo = stUpd) and (j > -1) then
    begin
      for i := Low(SQLAlterIdx) to High(SQLAlterIdx) do
      begin
        (*
        if Tipo = stIns then
          Liga := ', '
        else
        *)
        begin
          if i = 0 then
            Liga := 'WHERE '
          else
            Liga := 'AND ';
        end;
        //
        if SQLAlterIdx[i] = CO_JOKE_SQL then
          QrUpd.SQL.Add(Liga + ValAlterIdx[i])
        else
        begin
          Valor := Geral.VariavelToString(ValAlterIdx[i]);
          QrUpd.SQL.Add(Liga + SQLAlterIdx[i] + '=' + Valor);
        end;
      end;
    end else
    begin
      for i := Low(SQLIndex) to High(SQLIndex) do
      begin
        if Tipo = stIns then
          Liga := ', '
        else
        begin
          if i = 0 then
            Liga := 'WHERE '
          else
            Liga := 'AND ';
        end;
        //
        // 2011-12-02
        if SQLIndex[i] = CO_JOKE_SQL then
          QrUpd.SQL.Add(Liga + ValIndex[i])
        else
        // fim 2011-12-02
        begin
          Valor := Geral.VariavelToString(ValIndex[i]);
          QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;
  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;
  QrUpd.SQL.Add(';');
  Result := True;
  //
  //Geral.MB_Teste(QrUpd.SQL.Text);
end;

{$IfNDef NAO_USA_DB_GERAL}
function TUSQLDB.ExcluiRegistroBig1(Pergunta, Tabela, Campo: String;
  BigInt1: LongInt; DB: TmySQLDatabase): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DB.Owner);
  try
    Qry.Database := DB;
    Result := ID_NO;
    if ImpedeExclusaoPeloNomeDaTabela(Tabela) then
      Exit;
    //
    if Pergunta = '' then
      Result := ID_YES
    else
      Result := Geral.MB_Pergunta(Pergunta);
    if Result = ID_YES then
    begin
      Qry.SQL.Clear;
      Qry.SQL.Add(DELETE_FROM + lowercase(tabela)+
      ' WHERE '+Campo+'='+IntToStr(BigInt1));
      Qry.ExecSQL;
      if VAR_SALVA_REGISTROS_EXCLUIDOS then
      begin
        DModG.SalvaRegistroExcluido(stIns, Tabela, (*Niveis*)1, BigInt1);
      end;
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;
{$EndIf}

// ExcluiInteiro , ExcluiInteger
// DeleteInteiro , DeleteInteger
function TUSQLDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DB: TmySQLDatabase): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(DB.Owner);
  try
    Qry.Database := DB;
    Result := ID_NO;
    if ImpedeExclusaoPeloNomeDaTabela(Tabela) then
      Exit;
    //
    if Pergunta = '' then
      Result := ID_YES
    else
      Result := Geral.MB_Pergunta(Pergunta);
    if Result = ID_YES then
    begin
      Qry.SQL.Clear;
      Qry.SQL.Add(DELETE_FROM + lowercase(tabela)+
      ' WHERE '+Campo+'='+IntToStr(Inteiro1));
      Qry.ExecSQL;
      // 2018-03-02
      {$IfNDef NAO_USA_DB_GERAL}
      if VAR_SALVA_REGISTROS_EXCLUIDOS then
      begin
        DModG.SalvaRegistroExcluido(stIns, Tabela, (*Niveis*)1, Inteiro1);
      end;
      {$EndIf}
      // Fim 2018-03-02
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;

function TUSQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela: String): Boolean;
begin
  if Lowercase(Tabela) = Lowercase(VAR_LCT) then
  begin
    Result := True;
    //
    Geral.MB_Aviso('Exclus�o cancelada! A tabela "' + Tabela +
      '" n�o pode ter lan�amentos exclu�dos nesta rotina! Avise a Dermatek!');
  end else
    Result := False;
end;

function TUSQLDB.Int(mySQLDirectQuery: TmySQLDirectQuery; Campo: String): Integer;
begin
  if mySQLDirectQuery.RecordCount > 0 then
    Result := Geral.IMV(mySQLDirectQuery.FieldValueByFieldName(Campo))
  else
    Result := 0;
end;

function TUSQLDB.IntIdx(mySQLDirectQuery: TmySQLDirectQuery;
  Indice: Integer): Integer;
var
  s: String;
begin
  if mySQLDirectQuery.RecordCount > 0 then
  begin
    s := mySQLDirectQuery.FieldValues[Indice];
    if s <> '' then
      Result := StrToInt(s)
    else
      Result := 0;
  end else
    Result := 0;
end;

procedure TUSQLDB.MsgBeforeConectaDB(Sender: TObject);
var
 DB: TmySQLDataBase;
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  DB := TMySQLDatabase(Sender);
  //
  Geral.MB_Info('Ser� realizada uma conex�o � base de dados:' + sLineBreak +
  'Componente: '+ DB.Name + sLineBreak +
  'Host: '+ DB.Host + sLineBreak +
  'Port: '+ Geral.FF0(DB.Port) + sLineBreak +
  'DB: '+ DB.DataBaseName + sLineBreak +
  'User: '+ DB.UserName + sLineBreak);
end;

procedure TUSQLDB.MsgNaoConectaDB(DB: TmySQLDatabase; ProcName: String;
  Erro: Exception);
begin
  Exit;
  //
  Geral.MB_Erro('N�o foi poss�vel conectar na base de dados!' + sLineBreak +
  'Componente: '+ DB.Name + sLineBreak +
  'Host: '+ DB.Host + sLineBreak +
  'Port: '+ Geral.FF0(DB.Port) + sLineBreak +
  'DB: '+ DB.DataBaseName + sLineBreak +
  'User: '+ DB.UserName + sLineBreak +
  'Procedimento: '+ ProcName + sLineBreak +
  'Erro: ' + Erro.Message);
end;

function TUSQLDB.MudaSenhaBD(DB: TmySQLDatabase; QrUpd: TmySQLQuery; Senha,
  Usuario: String; MsgErr: Boolean): Boolean;
begin
  Result := False;
  DB.UserPassword := Senha;
  DB.UserName := Usuario;
  try
    DB.Connected := True;
    try
      DB.Connected := True;
      QrUpd.Database := DB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add(DELETE_FROM + ' user ');//WHERE User="root" OR User=""');
      QrUpd.ExecSQL;
      //
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('INSERT INTO user SET User="root", ');
      QrUpd.SQL.Add('Password=PASSWORD("'+VAR_BDSENHA+'"), ');
      QrUpd.SQL.Add('Host="localhost",');
      QrUpd.SQL.Add('Select_priv="Y",');
      QrUpd.SQL.Add('Insert_priv="Y",');
      QrUpd.SQL.Add('Update_priv="Y",');
      QrUpd.SQL.Add('Delete_priv="Y",');
      QrUpd.SQL.Add('Create_priv="Y",');
      QrUpd.SQL.Add('Drop_priv="Y",');
      QrUpd.SQL.Add('Reload_priv="Y",');
      QrUpd.SQL.Add('Shutdown_priv="Y",');
      QrUpd.SQL.Add('Process_priv="Y",');
      QrUpd.SQL.Add('File_priv="Y",');
      QrUpd.SQL.Add('Grant_priv="Y",');
      QrUpd.SQL.Add('References_priv="Y",');
      QrUpd.SQL.Add('Index_priv="Y",');
      QrUpd.SQL.Add('Alter_priv="Y",');
      QrUpd.SQL.Add('Show_db_priv="Y",');
      QrUpd.SQL.Add('Super_priv="Y",');
      QrUpd.SQL.Add('Create_tmp_table_priv="Y",');
      QrUpd.SQL.Add('Lock_tables_priv="Y",');
      QrUpd.SQL.Add('Execute_priv="Y",');
      QrUpd.SQL.Add('Repl_slave_priv="Y",');
      QrUpd.SQL.Add('Repl_client_priv="Y"');
      QrUpd.ExecSQL;
      //
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('INSERT INTO user SET User="root", ');
      QrUpd.SQL.Add('Password=PASSWORD("'+VAR_BDSENHA+'"), ');
      QrUpd.SQL.Add('Host="%",');
      QrUpd.SQL.Add('Select_priv="Y",');
      QrUpd.SQL.Add('Insert_priv="Y",');
      QrUpd.SQL.Add('Update_priv="Y",');
      QrUpd.SQL.Add('Delete_priv="Y",');
      QrUpd.SQL.Add('Create_priv="Y",');
      QrUpd.SQL.Add('Drop_priv="Y",');
      QrUpd.SQL.Add('Reload_priv="Y",');
      QrUpd.SQL.Add('Shutdown_priv="Y",');
      QrUpd.SQL.Add('Process_priv="Y",');
      QrUpd.SQL.Add('File_priv="Y",');
      QrUpd.SQL.Add('Grant_priv="Y",');
      QrUpd.SQL.Add('References_priv="Y",');
      QrUpd.SQL.Add('Index_priv="Y",');
      QrUpd.SQL.Add('Alter_priv="Y",');
      QrUpd.SQL.Add('Show_db_priv="Y",');
      QrUpd.SQL.Add('Super_priv="Y",');
      QrUpd.SQL.Add('Create_tmp_table_priv="Y",');
      QrUpd.SQL.Add('Lock_tables_priv="Y",');
      QrUpd.SQL.Add('Execute_priv="Y",');
      QrUpd.SQL.Add('Repl_slave_priv="Y",');
      QrUpd.SQL.Add('Repl_client_priv="Y"');
      QrUpd.ExecSQL;
      //
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.Database := DB;
      ///////////
      Result := True;
      ///////////
    except
      if MsgErr then
        Geral.MB_Erro('N�o foi poss�vel reconfigurar o banco de dados!');
    end;
  except
    //Geral.MB_Erro('Senha e/ou usu�rio invalidos!');
  end;
end;

function TUSQLDB.ConectaDB(DB: TmySQLDatabase; MyCodLoc: String): Boolean;
begin
  try
    DB.Connect;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(
      'N�o foi poss�vel conectar o "' + DB.Name + '"!' + sLineBreak +
      'Configura��o:' + sLineBreak + sLineBreak+
      'Database: ' + DB.DatabaseName + sLineBreak +
      'Host: ' + DB.Host + sLineBreak +
      'Porta: ' + FormatFloat('0', DB.Port) + sLineBreak +
      'User: ' + DB.UserName + sLineBreak + sLineBreak +
      'Mensagem de origem: ' + MyCodLoc);
      //
    end;
  end;
end;

procedure TUSQLDB.ConfiguraDataBase(DataBase: TmySQLDatabase; DataBaseName: String);
begin
  UnDmkDAC_PF.ConectaMyDB_DAC(DataBase, DataBaseName, VAR_IP, VAR_PORTA, 'root',
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
end;

procedure TUSQLDB.ConfiguraDB(ForcaConfiguracao: Boolean);
begin
{$IfDef sVeriConex}
//
{$Else}
  Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
  FmVerificaConexoes.FForcaConfiguracao := True;
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
{$EndIf}
end;

function TUSQLDB.DtH(mySQLDirectQuery: TmySQLDirectQuery;
  Campo: String): TDateTime;
begin
  // DateSeparator := '-';
  if mySQLDirectQuery.RecordCount > 0 then
    Result := Geral.ValidaDataHoraSQL(mySQLDirectQuery.FieldValueByFieldName(Campo))
  else
    Result := 0;
end;

function TUSQLDB.DtHIdx(mySQLDirectQuery: TmySQLDirectQuery;
  Indice: Integer): TDateTime;
begin
  if mySQLDirectQuery.RecordCount > 0 then
    Result := Geral.ValidaDataHoraSQL(mySQLDirectQuery.FieldValues[Indice])
  else
    Result := 0;
end;

function TUSQLDB.Backup_ConfiguraDestinoArquivo(Diretorio, ArqNome: String;
  NomeCurto: Boolean): String;
var
  Pasta: String;
begin
  Pasta := Trim(Diretorio);
  //
  if Length(Pasta) > 0 then
  begin
    if Diretorio[Length(Diretorio)] <> '\' then
      Diretorio := Diretorio + '\';
    //
    Result := ArqNome;
    //
    if NomeCurto then
      Result := String(dmkPF.NomeLongoParaCurto(AnsiString(Diretorio))) + Result
    else
      Result := Diretorio + Result;
  end else
    Result := '';
end;

function TUSQLDB.ObtemMySQLBinDir(DataBase: TmySQLDatabase): String;
const
  dumpName = 'mysqldump.exe';
var
  Query: TmySQLQuery;
  BaseDir, Diretorio: String;
begin
  try
    if not DataBase.Connected then
      ConfiguraDataBase(DataBase, 'mysql');
    //
    Query := TmySQLQuery.Create(DataBase);
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SHOW VARIABLES LIKE "basedir"',
      '']);
    //
    BaseDir := Query.FieldByName('Value').AsString;
  finally
    Query.Free;
  end;
  //
  Diretorio := dmkPF.AdicionaNoDiretorio(BaseDir, 'bin\');
  Result    := Diretorio;
end;

function TUSQLDB.ObtemPathMySQLBin(DataBase: TmySQLDatabase): String;
var
  BinDir, MySQLDump: String;
begin
  Result := '';
  //
  BinDir    := Geral.ReadAppKeyLM('PathMySQL', 'Dermatek', ktString, '');
  MySQLDump := dmkPF.AdicionaNoDiretorio(BinDir, coMySQLDumpName);
  //
  if (BinDir = '') or (not DirectoryExists(BinDir)) or (not FileExists(MySQLDump)) then
  begin
    Result := ObtemMySQLBinDir(DataBase);
    //
    Geral.WriteAppKeyLM('PathMySQL', 'Dermatek', Result, ktString);
  end else
    Result := BinDir;
end;

function TUSQLDB.SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean; SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant; UserDataAlterweb: Boolean; ComplUpd: String;
  InfoSQLOnError, InfoErro: Boolean): Boolean;
begin
  Result := CarregaSQLInsUpd(QrUpd, Tipo, lowercase(Tabela), Auto_increment,
              SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb,
              False, ComplUpd);
  //Geral.MB_Teste(QrUpd.SQL.Text);
  if Result then
  begin
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      QrUpd.ExecSQL;
      //
      Result := True;
    except
      if InfoSQLOnError then
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      if InfoErro then
        raise;
    end;
  end else;
end;

function TUSQLDB.SQLInsUpdAlterIdx(QrUpd: TmySQLQuery; Tipo: TSQLType;
              Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex,
              SQLAlterIdx: array of String; ValCampos, ValIndex,
              ValAlterIdx: array of Variant; UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
begin
  Result := CarregaSQLInsUpdAlterIdx(QrUpd, Tipo, lowercase(Tabela),
    Auto_increment, SQLCampos, SQLIndex, SQLAlterIdx, ValCampos, ValIndex,
    ValAlterIdx, UserDataAlterweb, False, ComplUpd);
  if Result then
  begin
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      QrUpd.ExecSQL;
      //
      Result := True;
    except
      if InfoSQLOnError then
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      if InfoErro then
        raise;
    end;
  end else;
end;

function TUSQLDB.SQL_DB_I_U(const DB: TmySQLDatabase; const Tipo: TSQLType;
  const Tabela: String; const Auto_increment: Boolean; const SQLCampos,
  SQLIndex: array of String; const ValCampos, ValIndex: array of Variant;
  const UserDataAlterweb, IGNORE: Boolean; var RecordsAffected: Int64;
  const ComplUpd: String = ''; const InfoSQLOnError: Boolean = True; InfoErro:
  Boolean = True): Boolean;
var
  SQL: String;
begin
  RecordsAffected := 0;
  Result := CarregaSQLDB_I_U(Tipo, lowercase(Tabela), Auto_increment, SQLCampos,
  SQLIndex, ValCampos, ValIndex, UserDataAlterweb, IGNORE, ComplUpd, SQL);
  if Result then
  begin
    try
      RecordsAffected := DB.Execute(SQL);
      //
      Result := True;
    except
      if InfoSQLOnError then
        Geral.MB_Erro('TUSQLDB.SQL_DB_I_U()' + sLineBreak + SQL);
      if InfoErro then
        raise;
    end;
  end else;
end;

function TUSQLDB.Str(mySQLDirectQuery: TmySQLDirectQuery;
  Campo: String): String;
begin
  if mySQLDirectQuery.RecordCount > 0 then
    Result := mySQLDirectQuery.FieldValueByFieldName(Campo)
  else
    Result := '';
end;

function TUSQLDB.StrIdx(mySQLDirectQuery: TmySQLDirectQuery;
  Indice: Integer): String;
begin
  if mySQLDirectQuery.RecordCount > 0 then
    Result := mySQLDirectQuery.FieldValues[Indice]
  else
    Result := '';
end;

function TUSQLDB.TestarConexaoMySQL(Form: TForm; Host, User, Pass,
  BancoDados: String; Port: Integer; Avisa: Boolean): Boolean;
var
  DB: TmySQLDataBase;
begin
  DB := TmySQLDataBase.Create(Form);
  try
    try
      if DB.Connected then
        DB.Disconnect;
      //
      DB.Host         := Host;
      DB.Port         := Port;
      DB.UserName     := User;
      DB.UserPassword := Pass;
      DB.DatabaseName := BancoDados;
      //DB.Connect;
      ConectaDB(DB, 'USQLDB.1087');
    finally
      Result := DB.Connected;
      if Avisa then
      begin
        if Result then
          Geral.MB_Aviso(
            'Conex�o com o serivdor ' + Host + ' estabelecida com �xito!')
        else
          Geral.MB_Aviso('N�o foi poss�vel abrir conex�o com o servidor ' +
            Host + '!');
      end;
      DB.Disconnect;
    end;
  finally
    DB.Free;
  end;
end;

procedure TUSQLDB.VerificaDmod_1_Create_DBs(DBs: array of TmySQLDatabase);
var
  I: Integer;
begin
  for I := Low(DBs) to High(DBs) do
  begin
    if DBs[I] <> nil then
    begin
      if DBs[I].Connected then
      begin
        Geral.MB_Erro(DBs[I].Name + ' est� connectado antes da configura��o!');
        DBs[I].Disconnect;
      end;
      DBs[I].LoginPrompt := False;
    end;
  end;
end;

function TUSQLDB.VerificaDmod_2_DefineVarsDBConnect(): String;
var
  ConnType: Integer;
  Host: String;
  Port: Integer;
  DBName, UserName, Senha: String;
begin
  Geral.DefineFormatacoes;
  //
  Senha := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if Senha = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(Senha, CO_LLM_SecureStr);
  //
  Port := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  VAR_PORTA := Port;
  //Server := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  ConnType     := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  VAR_SERVIDOR := ConnType;
  //
(*
  case Server of
    1:   VAR_SERVIDOR := 0;
    2:   VAR_SERVIDOR := 1;
    3:   VAR_SERVIDOR := 2;
    else VAR_SERVIDOR := -1;
  end;
*)
  Host         := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  VAR_IP       := Host;
  DBName       := Geral.ReadAppKeyCU('Database', Application.Title, ktString, 'mysql');
  UserName     := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, 'root');
  VAR_SQLUSER  := UserName;
  if VAR_BDSENHA = '' then
    VAR_BDSENHA := CO_USERSPNOW;
  if VAR_SQLUSER = '' then
    VAR_SQLUSER := 'root';
  //
  Result := DBName;
end;

procedure TUSQLDB.VerificaDmod_3_RedefineSenhaSeNecessario(ZZDB: TmySQLDatabase; QrUpd: TmySQLQuery);
begin
  /////////////////////////////////////////
  if SenhaDesconhecida(ZZDB, QrUpd) then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  UnDmkDAC_PF.ConectaMyDB_DAC(ZZDB, 'mysql', VAR_IP, VAR_PORTA, VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      Halt(0);
      Exit;
    except
      if VAR_SERVIDOR = 2 then
        Geral.MB_Erro('Banco de dados teste n�o se conecta!');
    end;
  end;
end;

procedure TUSQLDB.VerificaDmod_4_PreparaParaConectar();
begin
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MB_Erro('M�quina cliente sem rede.');
        //Application.Terminate;
      end;
    end;
 //FmPrincipal.FLinModErr := 2264;
 except
    raise;
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
//FmPrincipal.FLinModErr := 2272;
    Application.Terminate;
    Exit;
  end;
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
//FmPrincipal.FLinModErr := 2280;
  // ini 2020-09-17
(*
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
*)
{$IfDef sVeriConex}
//
{$Else}
    Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
    FmVerificaConexoes.ShowModal;
    FmVerificaConexoes.Destroy;
{$EndIf}
  // fim 2020-09-17
  end;
  if VAR_IP = CO_VAZIO then
  begin
//FmPrincipal.FLinModErr := 2294;
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
end;

procedure TUSQLDB.VerificaDmod_5_DefineBDPrincipal(MyDB: TmySQLDatabase; QrAux:
  tmySQLQuery);
var
  BD: String;
  Resp: Integer;
begin
  QrAux.Close;
  QrAux.Database := MyDB;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES LIKE "%' + TMeuDB + '%"');
  UnDmkDAC_PF.AbreQueryApenas(QrAux);
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    //if Uppercase(QrAux.FieldByName('Database').AsWideString)=Uppercase(TMeuDB) then
    if Uppercase(QrAux.Fields[0].AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  //FmPrincipal.FLinModErr := 2249;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      //
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else //if Resp = ID_CANCEL then
    begin
      // ini 2022-02-22
      //Geral.MB_Aviso('O aplicativo ser� encerrado!');
      //Halt(0);
      ConfiguraDB(True);
      // ini 2022-02-22
      //Application.Terminate;
      Exit;
    end;
  end;
end;

procedure TUSQLDB.VerificaDmod_6_AtualizaBDSeNecessario(const MyDB: TmySQLDatabase;
  const QrAux: tmySQLQuery; var VerificaDBTerc: Boolean);
var
  Versao: Int64;
  Verifica: Boolean;
  Resp: Integer;
begin
  //FmPrincipal.FLinModErr := 2419;
  Versao   := USQLDB.ObtemVersaoAppDB(MyDB);
  Verifica := False;
  if Versao < CO_VERSAO then
    Verifica := True
  else
(*
  //if Versao = CO_VERSAO then
  begin
    VerNew   := USQLDB.ObtemVersaoAppDB(MyDB, TVerApp.verappAppApp);
    if VerNew < CO_VERNEW then
      Verifica := True;
  end;
*)  //
  if Versao = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, '+
              'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
              'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
              'banco de dados. Confirma a Verifica��o?');
    //
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  //FmPrincipal.VerificaTerminal;
  // M L A G e r a l .ConfiguracoesIniciais(1, MyDB.DatabaseName);
  //FmPrincipal.FLinModErr := 2451;
  //MyList.ConfiguracoesIniciais(1);
  //FmPrincipal.FLinModErr := 2453;
  //UnDmkDAC_PF.AbreQuery(QrCambios, MyDB);
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  if Verifica then
{$IfDef sVeriConex}
//
{$Else}
    ALL_Jan.MostraFormVerifiDB(True);
{$EndIf}
  //FmPrincipal.FLinModErr := 2458;
  //
  try
    (*
    UnDmkDAC_PF.AbreQuery(QrMaster, MyDB, '', True);
    VAR_EMPRESANOME := QrMasterEm.Value;
    *)
  except
    try
  //FmPrincipal.FLinModErr := 2465;
{$IfDef sVeriConex}
//
{$Else}
      ALL_Jan.MostraFormVerifiDB(True);
{$EndIf}
      //
      VerificaDBTerc := True;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;

{
  //FmPrincipal.FLinModErr := 2475;
  try
    Application.CreateForm(TDmodG, DmodG);
  //FmPrincipal.FLinModErr := 2478;
  except
    on e: Exception do
    begin
      Geral.MB_Aviso('Imposs�vel criar Modulo de dados Geral' + sLineBreak +
      'FmPrincipal.FLinModGeralErr = ' + Geral.FF0(FmPrincipal.FLinModErr) + sLineBreak +
      e.Message);
    end;
      //Application.Terminate;
      //Exit;
  end;
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MB_Aviso('Imposs�vel criar Modulo Financeiro');
    Application.Terminate;
    Exit;
  end;
  //
  if VerificaDBTerc then
  begin
    All_Jan.MostraFormVerifiDBTerceiros(True);
    //
    VerificaDBTerc := False;
  end;
  //
FmPrincipal.FLinModErr := 2503;
}
end;

procedure TUSQLDB.VerificaDmod_7_CriaDataModule(InstanceClass: TComponentClass;
  var Reference);
begin
  try
    Application.CreateForm(InstanceClass, Reference);
  //FmPrincipal.FLinModErr := 2478;
  except
    on e: Exception do
    begin
      Geral.MB_Aviso('Imposs�vel criar Modulo de Dados ' +
      TDataModule(InstanceClass).Name + sLineBreak +
      //'FmPrincipal.FLinModGeralErr = ' + Geral.FF0(FmPrincipal.FLinModErr) + sLineBreak +
      e.Message);
    end;
      //Application.Terminate;
      //Exit;
  end;
end;

function TUSQLDB.v_f(Qr: TmySQLQuery; Campo: String): Double;
begin
  Result := Qr.FieldByName(Campo).AsFloat;
end;

function TUSQLDB.v_i(Qr: TmySQLQuery; Campo: String): Integer;
begin
  Result := Qr.FieldByName(Campo).AsInteger;
end;

function TUSQLDB.v_t(Qr: TmySQLQuery; Campo: String): TDateTime;
begin
  Result := Qr.FieldByName(Campo).AsDateTime;
end;

function TUSQLDB.v_x(Qr: TmySQLQuery; Campo: String): String;
begin
  Result := Qr.FieldByName(Campo).AsString;
end;

{$IfNDef NAO_USA_SEL_RADIO_GROUP}
function TUSQLDB.ObtemVersaoAppDB(DataBase: TmySQLDatabase;
  // Ini 2019-01-25
  //Empresa: Integer; AppWeb: Boolean = False): Integer;
    App: TVerApp;(*verappIndef=0, verappAppFile=1, verappAppApp=2, verappWebFile=3*)
    Empresa: Integer):
                //ini 2022-01-11
                //Integer;
                Int64;
                // fim 2022-01-11
  // Fim 2019-01-25
const
  sMsg = 'N�o foi poss�vel obter a vers�o do aplicativo salva no registro do Windows!';
var
  Campo: String;
begin
(*
  if AppWeb = False then
  begin
    if DataBase.Connected then
    begin
      try
        Result := Geral.IMV(ObtemValorCampo(DataBase, 1, 'Codigo', 'Versao', 'controle'))
      except
        {$IfNDef NAO_USA_DB_GERAL}
        ALL_Jan.MostraFormVerifiDB(False);
        {$EndIf}
      end;
    end else
      Result := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
  end else
  begin
    if DataBase.Connected then
      Result := Geral.IMV(ObtemValorCampo(DataBase, Empresa, 'Empresa', 'Web_DBVersao', 'web_params'))
    else
      Result := 0;
  end;
*)
  Result := 0;
  //if AppWeb = False then
  case App of
    TVerApp.verappAppFile,
    TVerApp.verappAppApp:
    begin
      if DataBase.Connected then
      begin
        try
          case App of
          {$IFDEF VER320} // Tokyo
            TVerApp.verappAppFile: Campo := 'Versao';
            TVerApp.verappAppApp : Campo := 'VerMrg';
          {$Else}
            {$IfDef VER350} // Alexandria
              TVerApp.verappAppFile: Campo := 'Vers28';
              TVerApp.verappAppApp : Campo := 'VMrg28';
            {$Else}
              TVerApp.verappAppFile: Campo := 'Versao';
              TVerApp.verappAppApp : Campo := 'VerMrg';
              else Campo := '?versao?';
            {$EndIf}
          {$EndIf}
          end;
          //Result := Geral.IMV(ObtemValorCampo(DataBase, 1, 'Codigo', 'Versao', 'controle'))
                //ini 2022-01-11
          //Result := Geral.IMV(ObtemValorCampo(DataBase, 1, 'Codigo', Campo, 'controle'))
          if USQLDB.TabelaExiste('controle', (*Dmod.QrAux,*) Dmod.MyDB) = False then
            Result := 0
          else
            Result := Geral.I64(ObtemValorCampo(DataBase, 1, 'Codigo', Campo, 'controle'))
                //fim 2022-01-11
        except
{$IfDef sVeriConex}
//
{$Else}
          DModG.VerificaCampoVersaoControle('controle', Campo);
{$EndIf}
          {$IfNDef NAO_USA_DB_GERAL}
          ALL_Jan.MostraFormVerifiDB(False);
          {$EndIf}
        end;
      end else
      begin
        //ini 2022-01-11
        //Result := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
        {$IFDEF VER320} // Tokyo
          Result := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
        {$Else}
          {$IfDef VER350} // Alexandria
            Result := Geral.I64(Geral.ReadAppKey('Versao', Application.Title, ktString, 0, HKEY_LOCAL_MACHINE));
          {$Else}
            try
              Result := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
            except
              on E: exception do Geral.ErroVersaoDoCompilador(sMsg, E.Message);
            end;
          {$EndIf}
        {$EndIf}
        //FIm 2022-01-11
      end;
    end;
    TVerApp.verappWebFile:
    begin
      if DataBase.Connected then
      begin
                //ini 2022-01-11
        try
          Result := Geral.I64(ObtemValorCampo(DataBase, Empresa, 'Empresa', 'Web_DBVersao', 'web_params'))
        except
          Result := Geral.IMV(ObtemValorCampo(DataBase, Empresa, 'Empresa', 'Web_DBVersao', 'web_params'))
        end;
                //fim 2022-01-11
      end else
        Result := 0;
    end;
    else
      Geral.MB_Erro('Tipo de vers�o indefinida em busca de ves�o!');
  end;
end;
{$EndIf}

procedure TUSQLDB.ExecutaPingServidor(DataBase: array of TmySQLDatabase);
var
  DB: TmySQLDatabase;
  Qry: TmySQLQuery;
  I: Integer;
begin
  for I := Low(DataBase) to High(DataBase) do
  begin
    DB := DataBase[I];
    //
    if DB.Connected then
    begin
      Qry := TmySQLQuery.Create(DB.Owner);
      try
        try
          Qry.Database := DB;
          Qry.SQL.Clear;
          Qry.SQL.Add('SELECT 1 Um');
          UnDmkDAC_PF.AbreQuery(Qry, DB); // 2022-02-20 - Antigo . O p e n ;
        except
          ;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

function TUSQLDB.Flu(mySQLDirectQuery: TmySQLDirectQuery;
  Campo: String): Double;
var
  s: String;
begin
  if mySQLDirectQuery.RecordCount > 0 then
  begin
    s := mySQLDirectQuery.FieldValueByFieldName(Campo);
    s := StringReplace(s, '.', ',', [rfReplaceAll]);
    if s <> '' then
      Result := StrToFloat(s)
    else
      Result := 0;
  end else
    Result := 0;
end;

function TUSQLDB.FluIdx(mySQLDirectQuery: TmySQLDirectQuery;
  Indice: Integer): Double;
var
  s: String;
begin
  if mySQLDirectQuery.RecordCount > 0 then
  begin
    s := mySQLDirectQuery.FieldValues[Indice];
    s := StringReplace(s, '.', ',', [rfReplaceAll]);
    if s <> '' then
      Result := StrToFloat(s)
    else
      Result := 0;
  end else
    Result := 0;
end;

function TUSQLDB.GetFlu(DB: TmySQLDatabase; Tabela, Campo, SQL_Condition: String): Double;
const
 sProcName = 'TUSQLDB.GetFlu()';
var
  Dq: TMySQLDirectQuery;
begin
  Result := 0.0;
  Dq := TMySQLDirectQuery.Create(Dmod);
  try
    Dq.Database := DB;
    Dq.SQL.Text := 'SELECT ' + Campo + ' FROM ' + Tabela + sLineBreak + SQL_Condition;
    try
      Dq.Open;
      Result := Flu(Dq, Campo);
    except
      on E: Exception do
        Geral.MB_Erro(E.Message + sLineBreak + Dq.SQL.Text + sLineBreak +
          sProcName);
    end;
  finally
    Dq.Free;
  end;
end;

function TUSQLDB.Backup_Executa(DataBase: TmySQLDatabase; PathMySQL, Destino,
  ArqNome, Host, DbNome, Usuario, Senha: String; Porta: Integer; MemoResul:
  TMemo; DbCria, DbXML: Boolean;
  NoDropCreateTable: TNoDropCreateTable; var Msg: String; LabelAviso:
  TLabel = nil; Tabelas: String = ''; max_allowed_packet: String = '2097152';
  net_buffer_length: String = '32768'; default_character_set:
  String = 'latin1'): String;
const
  sProcName = 'USQLDB.Backup_Executa()';
var
  Continua: Boolean;
  Cmd, Db, Arq, CriarDB, XML, DirMySQL, sNoDropCreateTable: String;
begin
  Result   := '';
  DirMySQL := dmkPF.ValidaDiretorio(PathMySQL, True);
  //
  if not FileExists(dmkPF.NomeLongoParaCurto(AnsiString(DirMySQL)) + 'mysqldump.exe') then
  begin
    Msg := 'O MySQLDump n�o foi localizado!';
    Exit;
  end;
  //
  if not DirectoryExists(Destino) then
    ForceDirectories(Destino);
  //
  if not DirectoryExists(Destino) then
  begin
    Msg := 'O diret�rio destino n�o existe ou est� inacess�vel!';
    Exit;
  end;
  //
  if (Host <> '') and (Porta > 0) and (Usuario <> '') and (Senha <> '') and
    (DbNome <> '') and (Destino <> '') then
  begin
    try
      UnDmkDAC_PF.ConectaMyDB_DAC(DataBase, DbNome, Host, Porta, Usuario,
        Senha, (*Desconecta*)True, (*Configura*)True, (*Conecta*)True, True);
      //
      Continua := DataBase.Connected;
    except
      Continua := False;
    end;
    //
    if Continua = False then
    begin
      Msg := 'ERRO: Falha ao conectar no banco de dados!';
      Exit;
    end;
    //
    if DbCria then
      CriarDB := ''
    else
      CriarDB := '--no-create-db';
    //
    if DbXML then
      XML := ' --xml '
    else
      XML := '';
    //
    case NoDropCreateTable of
      (*0*)ndctDropAndCreate: sNoDropCreateTable := '';
      (*1*)ndctNoCreateInsAll: sNoDropCreateTable := '--no-create-info ';
      (*2*)ndctNoCreateAndInsIgnore: sNoDropCreateTable := '--no-create-info --insert-ignore ';
      (*3*)ndctNoCreateAndReplace: sNoDropCreateTable := '--no-create-info --replace ';
      (*4*)ndctNoCreateAndReplaceCompleateIns: sNoDropCreateTable := '--complete-insert --no-create-info --replace ';
      else
      begin
        sNoDropCreateTable := '-- �#$%�&';
        Geral.MB_Erro('"NoDropCreateTable" n�o implementado em ' + sProcName);
      end;
    end;
    //
    Db  := '--databases ' + DbNome;
    Arq := Backup_ConfiguraDestinoArquivo(Destino, ArqNome, True);
    Cmd := String(dmkPF.NomeLongoParaCurto(AnsiString(DirMySQL))) + 'mysqldump ' +
            '--max_allowed_packet=' + max_allowed_packet + ' ' +
            '--net_buffer_length=' + net_buffer_length + ' ' +
            '--default-character-set=' + default_character_set + ' ' +
            //'--skip-add-drop-table --no-create-info ' + ' ' +
            sNoDropCreateTable +
            CriarDB + XML + ' -P ' + Geral.FF0(Porta) + ' -h ' + Host + ' -u ' +
            Usuario + ' -p' + Senha + ' ' + Db + Tabelas + ' > ' + Arq;
    //
    MyObjects.ExecutaCmd(Cmd, MemoResul);
    //
    if LabelAviso <> nil then
      LabelAviso.Caption := 'Atualizando dados de backups...';
    //
    if FileExists(Arq) then
    begin
      Msg    := 'Backup do banco de dados ' + String(DbNome) + ' finalizado!';
      Result := Arq;
    end else
      Msg := 'Falha ao realizar o backup!';
  end else
    Msg := 'Dados insuficientes para realizar o backup!';
end;

function TUSQLDB.SenhaDesconhecida(DB: TmySQLDatabase; QrUpd: TmySQLQuery): Boolean;

  procedure TentaAlterarSenha(NomeBD: String);
  begin
    if not MudaSenhaBD(Db, QrUpd, '', 'root', False) then
    if not MudaSenhaBD(Db, QrUpd, CO_SPDESCONHECIDA, 'root', False) then
    begin
      VAR_LOGIN := '';
      VAR_SENHA := '';
      MyObjects.Senha(TFmSenha, FmSenha, 0, '', '', '', '', '', True, '', 0, '');
      if not MudaSenhaBD(Db, QrUpd, VAR_SENHA, VAR_LOGIN, True) then
      begin
        Geral.MB_Erro('O banco de dados possui senha desconhecida '+
        'e a aplica��o n�o poder� ser iniciada!');
        Halt(0);
        Exit;
      end;
    end else Result := False;
    DB.Connected := False;
    DB.DatabaseName := NomeBD;
  end;

var
  NomeBD: String;
begin
  Result := True;
  if (VAR_BDSENHA = '') or (VAR_BDSENHA = CO_SPDESCONHECIDA) then
  begin
    try
      DB.Connected := False;
      NomeBD := DB.DatabaseName;
      DB.Host         := VAR_SQLHOST;
      DB.DatabaseName := 'mysql';
      DB.UserName     := 'root';
      DB.UserPassword := VAR_BDSENHA;
      DB.Port         := VAR_PORTA;
      DB.ConnectionTimeout := 10;
      try
        //DB.Connect;
        USQLDB.ConectaDB(DB, 'UnGOTOy.2980');
      except
        Geral.MB_Erro(
        'N�o foi poss�vel conectar o "ZZDB" para configura��o do mysql:' +
        sLineBreak + sLineBreak+
        'Database: ' + DB.DatabaseName + sLineBreak +
        'Host: ' + DB.Host + sLineBreak +
        'Porta: ' + FormatFloat('0', VAR_PORTA));
      end;
      if DB.Connected then
      begin
        Result := False;
        DB.Connected := False;
        DB.DatabaseName := NomeBD
      end else begin
        TentaAlterarSenha(NomeBD);
      end;
    except
      TentaAlterarSenha(NomeBD);
    end;
  end else
    Result := False;
end;

function TUSQLDB.Sincro_AtualizaDados(DB: TmySQLDatabase; AgoraUTC: TDateTime;
  Empresa: Integer; SincroTotal: Boolean; Termino: Boolean): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(DB.Owner);
  try
    if SincroTotal then
    begin
      if Termino = True then
      begin
        if TabelaExiste('web_params', DB) then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DB, [
            'UPDATE web_params SET ',
            'DtaSincro="' + Geral.FDT(AgoraUTC, 9) + '", ',
            'Empresa=' + Geral.FF0(Empresa),
            '']);
        end;
        // Atualiza tabela web
        if TabelaExiste('web_opcoes', DB) then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DB, [
            'UPDATE web_opcoes SET SincroEmExec=0, ',
            'DtaSincro="' + Geral.FDT(AgoraUTC, 9) + '",',
            'Empresa=' + Geral.FF0(Empresa),
            '']);
        end;
      end else
      begin
        if TabelaExiste('web_opcoes', DB) then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DB, [
            'UPDATE web_opcoes SET SincroEmExec=1, ',
            'Empresa=' + Geral.FF0(Empresa),
            '']);
        end;
      end;
    end else
    begin
      if TabelaExiste('web_opcoes', DB) then
      begin
        if Termino = True then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DB, [
            'UPDATE web_opcoes SET SincroEmExec=0, ',
            'Empresa=' + Geral.FF0(Empresa),
            '']);
        end else
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DB, [
            'UPDATE web_opcoes SET SincroEmExec=1, ',
            'Empresa=' + Geral.FF0(Empresa),
            '']);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

{$IFNDEF NAO_USA_SEL_RADIO_GROUP}
function TUSQLDB.Sincro_ExecutaUnidirecional(DBOri, DBDes: TmySQLDataBase;
  Empresa: Integer; PathMySQL, Destino, ArqNome, Ori_Host, Ori_DB, Ori_User,
  Ori_Pass: String; Ori_Porta: Integer; Des_Host, Des_DB, Des_User,
  Des_Pass: String; Des_Porta: Integer; Tabs: String; SincTotal: Boolean;
  AgoraUTC: TDateTime; MemoResul: TMemo; var Msg: String;
  LabelAviso: TLabel = nil): Boolean;
var
  Continua: Boolean;
  ArqBkp: String;
begin
  Result := False;
  //
  //Executa backup
  Continua := False;
  ArqBkp   := Backup_Executa(DBOri, PathMySQL, Destino, ArqNome,
                Ori_Host, Ori_DB, Ori_User, Ori_Pass, Ori_Porta,
                MemoResul, False, False, ndctDropAndCreate, Msg,
                LabelAviso, Tabs);
  //
  if ArqBkp <> '' then
  begin
    //Executa restaura
    try
      UnDmkDAC_PF.ConectaMyDB_DAC(DBDes, Des_DB, Des_Host,
        Des_Porta, Des_User, Des_Pass, True, True, True, True);
      //
      Continua := DBDes.Connected;
    except
      Continua := False;
    end;
    //
    if Continua = False then
    begin
      System.SysUtils.DeleteFile(ArqBkp);
      //
      Msg := 'ERRO: Falha ao conectar no banco de dados!';
      Exit;
    end;
    //
    Result := Backup_Restaura(DBDes, ArqBkp, False, Msg, TStaticText(LabelAviso));
    //
    System.SysUtils.DeleteFile(ArqBkp);
    //
    Sincro_AtualizaDados(DBDes, AgoraUTC, Empresa, SincTotal, False);
    //
    if Result = True then
    begin
      Msg := 'Dados sincronisados com sucesso!';
    end else
    begin
      Exit;
    end;
  end else
  begin
    Msg := 'Falha ao realizar o backup para o sincronismo!';
    Exit;
  end;
end;
{$EndIf}

function TUSQLDB.Sincro_ValidaDBUnidirecional(DataBase: TmySQLDatabase;
  QueryAux: TmySQLQuery; NomeDB: String; var Msg: String): Boolean;
var
  AppId: Integer;
  HabilModulos: String;
begin
  Msg    := '';
  Result := False;
  //
  if TabelaExiste(CO_SINCRO_TAB, DataBase) then
  begin
    if TabelaExiste('master', DataBase) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, DataBase, [
        'SELECT HabilModulos ',
        'FROM master ',
        '']);
      HabilModulos := QueryAux.FieldByName('HabilModulos').AsString;
      //
      if Pos('dcontrol', LowerCase(NomeDB)) > 0 then
        AppId := 17
      else
        AppId := 0;
      //
      if Grl_Geral.LiberaModulo(AppId, Grl_Geral.ObtemSiglaModulo(mdlappWEB),
             HabilModulos)
      then
        Result := True
      else
        Msg := 'O banco de dados selecionado n�o tem permiss�o para utilizar o m�dulo WEB!' +
                 sLineBreak + 'Solicite sua libera��o junto a Dermatek!';
    end else
      Msg := 'O banco de dados informado n�o possui suporte a sincronismo!';
  end else
    Msg := 'O banco de dados informado n�o possui suporte a sincronismo!';
end;

function TUSQLDB.ObtemSQL_DataHora(UTC: Boolean): TStringArray;
begin
  if UTC = True then
  begin
    SetLength(Result, 4);
    //
    Result[0] := 'SELECT YEAR(UTC_TIMESTAMP()) ANO, MONTH(UTC_TIMESTAMP()) MES, ';
    Result[1] := 'DAYOFMONTH(UTC_TIMESTAMP()) DIA, ';
    Result[2] := 'HOUR(UTC_TIMESTAMP()) HORA, MINUTE(UTC_TIMESTAMP()) MINUTO, ';
    Result[3] := 'SECOND(UTC_TIMESTAMP()) SEGUNDO, UTC_TIMESTAMP() AGORA ';
  end else
  begin
    SetLength(Result, 4);
    //
    Result[0] := 'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES, ';
    Result[1] := 'DAYOFMONTH(NOW()) DIA, ';
    Result[2] := 'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO, ';
    Result[3] := 'SECOND(NOW()) SEGUNDO, NOW() AGORA ';
  end;
end;

function TUSQLDB.ObtemValorCampo(DB: TmySQLDatabase; Valor: Variant; ValCampo,
  Campo, Tabela: String): String;
var
  Qry: TMySQLQuery;
begin
  Result := '';
  Qry    := TmySQLQuery.Create(DB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT ' + Campo,
      'FROM ' + Tabela,
      'WHERE ' + ValCampo + '="' + VarToStr(Valor) + '"',
      '']);
    if Qry.RecordCount > 0 then
      Result := VarToStr(Qry.FieldByName(Campo).AsVariant);
  finally
    Qry.Free;
  end;
end;

end.
