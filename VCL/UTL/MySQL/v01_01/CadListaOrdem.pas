unit CadListaOrdem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums, frxClass,
  frxDBSet;

type
  THackDBGrid = class(TDBGrid);
  TFmCadListaOrdem = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    TbCad: TmySQLTable;
    DsCad: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    TbCadCodigo: TIntegerField;
    TbCadNome: TWideStringField;
    Panel5: TPanel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    LbItensMD: TListBox;
    BtInclui: TBitBtn;
    frxCAD_LISTA_001: TfrxReport;
    frxDsCad: TfrxDBDataset;
    SbImprime: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    TbCadOrdem: TIntegerField;
    TbCadAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbCadBeforePost(DataSet: TDataSet);
    procedure TbCadDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbCadAfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure DBGrid1Exit(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbCadAfterInsert(DataSet: TDataSet);
    procedure TbCadNewRecord(DataSet: TDataSet);
    procedure TbCadBeforeOpen(DataSet: TDataSet);
    procedure frxCAD_LISTA_001GetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure TbCadBeforeEdit(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
  private
    { Private declarations }
    //FCharPos, FGridRow: Integer;
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    FFldIndices: array of String;
    FValIndices: array of Variant;
*)
    //
    procedure ReabrePesquisa();
  end;

  var
  FmCadListaOrdem: TFmCadListaOrdem;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, GetValor;

{$R *.DFM}

procedure TFmCadListaOrdem.BtAlteraClick(Sender: TObject);
begin
  if (TbCad.State <> dsInactive) and (TbCad.RecordCount > 0) then
  begin
    TbCad.Edit;
    DBGrid1.SetFocus;
    DBGrid1.SelectedField := TbCadNome;
  end;
end;

procedure TFmCadListaOrdem.BtExcluiClick(Sender: TObject);
begin
  if (TbCad.State <> dsInactive) and (TbCad.RecordCount > 0) then
    TbCad.Delete;
end;

procedure TFmCadListaOrdem.BtIncluiClick(Sender: TObject);
var
  Codigo: Variant;
  Qry: TmySQLQuery;
begin
  if FReservados <> Null then
  begin
    Codigo := 0;
    //
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Codigo, 0, 0,
    '', Geral.FF0(FReservados), True, 'C�digo', 'Informe o C�digo: ', 0, Codigo) then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        Qry.Database := Dmod.MyDB;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT ' + FFldID,
        'FROM ' + TbCad.TableName,
        'WHERE ' + FFldID + '=' + Geral.FF0(Codigo),
        '']);
        if Qry.RecordCount > 0 then
        begin
          Geral.MB_Aviso('O c�digo ' + String(Codigo) +
          ' j� est� sendo usado em outro registro!');
          Exit;
        end else
        begin
          if UMyMod.SQLInsUpd(Qry, stIns, TBCad.TableName, False, [
          ], [
          'Codigo'], [
          ], [
          Codigo], True) then
          begin
            TbCad.Refresh;
            TbCad.Locate(FFldID, Codigo, []);
          end;
        end;
      finally
        Qry.Free;
      end;
    end;
  end else
  begin
    TbCad.Insert;
    if TbCad.State in ([dsInsert, dsEdit]) then
    begin
      DBGrid1.SetFocus;
      DBGrid1.SelectedField := TbCadNome;
    end else
      Geral.MB_Aviso('Verifique se esta tabela pode ser editada!');
  end;
end;

procedure TFmCadListaOrdem.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := TbCadCodigo.Value;
  Close;
end;

procedure TFmCadListaOrdem.DBGrid1CellClick(Column: TColumn);
var
  Ativo, IDInt: Integer;
begin
  if Uppercase(Column.FieldName) = Uppercase('Ativo') then
  begin
    TbCad.Edit;
    if TbCadAtivo.Value = 0 then
      TbCadAtivo.Value := 1
    else
      TbCadAtivo.Value := 0;
    //
    TbCad.Post;
    TbCad.Refresh;
  end;
end;

procedure TFmCadListaOrdem.DBGrid1ColEnter(Sender: TObject);
begin
  if THackDBGrid(DBGrid1).Col = 1 then
    DBGrid1.Options := DBGrid1.Options - [dgEditing]
  else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmCadListaOrdem.DBGrid1Exit(Sender: TObject);
begin
  //FGridRow := THackDBGrid(DBGrid1).Row;
  //FCharPos := THackDBGrid(DBGrid1).InplaceEditor.SelStart;
end;

procedure TFmCadListaOrdem.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadListaOrdem.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmCadListaOrdem.FormCreate(Sender: TObject);
begin
  FNovoCodigo := ncIdefinido;
  FReservados := Null;
  FSoReserva := False;
  FPermiteExcluir := False;
  FFldID := 'Codigo';
  FFldNome := 'Nome';
end;

procedure TFmCadListaOrdem.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadListaOrdem.frxCAD_LISTA_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := Dmod.QrMasterEm.Value
  else
  if VarName = 'VARF_TITULO' then
    Value := LaTitulo1A.Caption
  else
  if VarName = 'VARF_CAMPO_A' then
  begin
    if TbCad.Fields.Count = 3 then
      Value := TbCad.Fields[2].FieldName
    else
      Value := '';
  end
  else
  if VarName = 'VARF_VALOR_A' then
  begin
    if TbCad.Fields.Count = 3 then
      Value := TbCad.Fields[2].AsString
    else
      Value := '';
  end
  else
end;

procedure TFmCadListaOrdem.LbItensMDDblClick(Sender: TObject);
var
  Texto: String;
  Ate: Integer;
begin
  Texto := LbItensMD.Items[LbItensMD.ItemIndex];
  if Texto <> '' then
  begin
    Ate := Pos(']', Texto);
    Texto := Copy(Texto, 1, Ate);
    (*
    Clipboard.AsText := Texto;
    THackDBGrid(DBGrid1).InplaceEditor.PasteFromClipboard;
    *)
    TBCad.Edit;
    TBCadNome.Value := TBCadNome.Value + Texto;
    TBCad.Post;
  end;
  DBGrid1.SetFocus;
end;

procedure TFmCadListaOrdem.ReabrePesquisa();
var
  Texto, SQL: String;
  K, N, I: Integer;
begin
  QrPesq.Close;
  Texto := EdPesq.Text;
  K := Length(Texto);
  N := TBTam.Position;
  if N < 3 then
    N := 3;
  if K >= 3 then
  begin
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (' + FFldNome + ' LIKE "%' + Copy(Texto, I, N) + '%")' + sLineBreak;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT ' + FFldID + ', ' + FFldNome,
    'FROM ' + TbCad.TableName,
    'WHERE (' + FFldNome + ' LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
  end;
end;

procedure TFmCadListaOrdem.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxCAD_LISTA_001, [
  DModG.frxDsDono,
  frxDsCad]);
  MyObjects.frxMostra(frxCAD_LISTA_001, LaTitulo1A.Caption);
end;

procedure TFmCadListaOrdem.TbCadAfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TbCad.Cancel;
end;

procedure TFmCadListaOrdem.TbCadAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TbCadCodigo.Value;
end;

procedure TFmCadListaOrdem.TbCadBeforeEdit(DataSet: TDataSet);
begin
  if TbCadCodigo.Value = 0 then
  begin
    TbCad.Cancel;
    //TbCad.Append; // Erro
  end;
end;

procedure TFmCadListaOrdem.TbCadBeforeOpen(DataSet: TDataSet);
(*
var
  I: Integer;
  Txt: String;
*)
begin
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
  I := High(FFldIndices);
  TbCad.Filter := '';
  TbCad.Close;  // Ter certeza ??
  TbCad.Filtered := False;
  for I := Low(FFldIndices) to High(FFldIndices) do
  begin
    Txt := 'and ' + FFldIndices[I] + Geral.VariavelToString(FFldIndices[I]);
  end;
  if Txt <> '' then
  begin
    Txt := Copy(Txt, 4);
    TbCad.Filter := Txt;
    TbCad.Filtered := True;
  end;
*)
end;

procedure TFmCadListaOrdem.TbCadBeforePost(DataSet: TDataSet);
begin
  if Tbcad.State = dsInsert then
  begin
    case FNovoCodigo of
      ncControle: TbCadCodigo.Value := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, TbCad.TableName, FFldID, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: TbCadCodigo.Value := UMyMod.BuscaProximoGerlSeq1Int32(
        TbCad.TableName, FFldID, '', '', tsDef, stIns, 0, FReservados);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
        Halt(0);
        Exit;
      end;
    end;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    for I := Low(FFldIndices) to High(FFldIndices) do
      TbCad.FieldByName(FFldIndices[I]).AsVariant := FFldIndices[I];
*)
  end
  else if Tbcad.State = dsEdit then
  begin
    if TbCadCodigo.Value = 0 then
      TbCad.Cancel;
  end;
end;

procedure TFmCadListaOrdem.TbCadDeleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmCadListaOrdem.TbCadNewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TbCad.Cancel;
end;

procedure TFmCadListaOrdem.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
