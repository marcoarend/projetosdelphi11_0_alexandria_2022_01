object FmBackup3: TFmBackup3
  Left = 380
  Top = 170
  Caption = 'FER-BACKP-003 :: Backup 3 (Beta)'
  ClientHeight = 660
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 454
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Backup'
      object PainelDados: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 426
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object MeBase: TMemo
          Left = 0
          Top = 320
          Width = 1000
          Height = 46
          Align = alBottom
          Lines.Strings = (
            
              '//C:\ARQUIV~1\MySQL\MYSQLS~1.1\bin\mysqldump --max_allowed_packe' +
              't=2097152 '
            
              '--net_buffer_length=65536 -h localhost -u root -pwkljweryhvbirt ' +
              ' -B seven > '
            'C:\Seven0606121728.sql'
            '')
          TabOrder = 0
          Visible = False
        end
        object MeCmd: TMemo
          Left = 0
          Top = 366
          Width = 1000
          Height = 60
          Align = alBottom
          TabOrder = 1
          Visible = False
        end
        object MeResult_: TMemo
          Left = 767
          Top = 0
          Width = 109
          Height = 320
          Align = alRight
          Color = clBlack
          Font.Charset = ANSI_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Lines.Strings = (
            'Teste 1'
            'C:\Meus '
            'Documentos')
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          Visible = False
        end
        object TPanel
          Left = 0
          Top = 0
          Width = 201
          Height = 320
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 3
          object CBTables: TCheckListBox
            Left = 0
            Top = 97
            Width = 201
            Height = 223
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ItemHeight = 13
            ParentFont = False
            TabOrder = 0
          end
          object Panel5: TPanel
            Left = 0
            Top = 48
            Width = 201
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object BtTudo: TBitBtn
              Tag = 127
              Left = 54
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Tabelas'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtTudoClick
            end
          end
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 201
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object Label1: TLabel
              Left = 12
              Top = 4
              Width = 74
              Height = 13
              Caption = 'Base de dados:'
            end
            object CBDB1: TDBLookupComboBox
              Left = 12
              Top = 20
              Width = 181
              Height = 21
              KeyField = 'Database'
              ListField = 'Database'
              ListSource = DsDatabases1
              TabOrder = 0
              OnClick = CBDB1Click
              OnExit = CBDB1Exit
            end
          end
        end
        object Panel2: TPanel
          Left = 201
          Top = 0
          Width = 380
          Height = 320
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 4
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 380
            Height = 320
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 0
            object TabSheet3: TTabSheet
              Caption = ' Cofigura'#231#245'es de sa'#237'da '
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 372
                Height = 292
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel9: TPanel
                  Left = 0
                  Top = 0
                  Width = 372
                  Height = 265
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label6: TLabel
                    Left = 10
                    Top = 160
                    Width = 84
                    Height = 13
                    Caption = 'Nome do arquivo:'
                  end
                  object EdArquivo: TdmkEdit
                    Left = 10
                    Top = 176
                    Width = 355
                    Height = 21
                    TabOrder = 2
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object CkWinZip: TCheckBox
                    Left = 10
                    Top = 202
                    Width = 149
                    Height = 17
                    Caption = 'Campactar com Win Zip'
                    Checked = True
                    State = cbChecked
                    TabOrder = 3
                    OnClick = CkWinZipClick
                  end
                  object CkExclui: TCheckBox
                    Left = 10
                    Top = 220
                    Width = 327
                    Height = 17
                    Caption = 'Excluir o arquivo do local do destino caso j'#225' exista.'
                    TabOrder = 5
                  end
                  object CkDelNaoCompact: TCheckBox
                    Left = 166
                    Top = 202
                    Width = 183
                    Height = 17
                    Caption = 'Exclui arquivo n'#227'o compactado.'
                    Checked = True
                    State = cbChecked
                    TabOrder = 4
                  end
                  object GBDestino1: TGroupBox
                    Left = 10
                    Top = 10
                    Width = 355
                    Height = 70
                    Caption = 'Destino 1: (Onde ser'#225' feito o backup)'
                    TabOrder = 0
                    object SpeedButton1: TSpeedButton
                      Left = 330
                      Top = 20
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton1Click
                    end
                    object LaAvisoDest1: TLabel
                      Left = 241
                      Top = 50
                      Width = 110
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Total: 0 / Dispon'#237'vel: 0'
                    end
                    object EdDestino1: TdmkEdit
                      Left = 5
                      Top = 20
                      Width = 320
                      Height = 21
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdDestino1Change
                    end
                    object PBAvisoDest1: TProgressBar
                      Left = 5
                      Top = 46
                      Width = 100
                      Height = 17
                      MarqueeInterval = 1
                      Step = 1
                      TabOrder = 1
                    end
                  end
                  object GroupBox2: TGroupBox
                    Left = 10
                    Top = 88
                    Width = 355
                    Height = 70
                    Caption = 'Destino 2: (ser'#225' copiado o arquivo do destino 1)'
                    TabOrder = 1
                    object LaAvisoDest2: TLabel
                      Left = 241
                      Top = 50
                      Width = 110
                      Height = 13
                      Alignment = taRightJustify
                      Caption = 'Total: 0 / Dispon'#237'vel: 0'
                    end
                    object SpeedButton2: TSpeedButton
                      Left = 330
                      Top = 20
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton2Click
                    end
                    object PBAvisoDest2: TProgressBar
                      Left = 5
                      Top = 46
                      Width = 100
                      Height = 17
                      MarqueeInterval = 1
                      Step = 1
                      TabOrder = 0
                    end
                    object EdDestino2: TdmkEdit
                      Left = 5
                      Top = 20
                      Width = 320
                      Height = 21
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdDestino2Change
                    end
                  end
                end
                object Panel11: TPanel
                  Left = 0
                  Top = 265
                  Width = 372
                  Height = 27
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                end
              end
            end
            object TabSheet4: TTabSheet
              Caption = ' Configura'#231#245'es avan'#231'adas '
              ImageIndex = 1
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 374
                Height = 297
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label5: TLabel
                  Left = 8
                  Top = 4
                  Width = 95
                  Height = 13
                  Caption = 'Diret'#243'rio do MySQL:'
                end
                object SpeedButton3: TSpeedButton
                  Left = 332
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton3Click
                end
                object LaAllow: TLabel
                  Left = 8
                  Top = 48
                  Width = 112
                  Height = 13
                  Caption = '--max_allowed_packet='
                end
                object LaBuffer: TLabel
                  Left = 128
                  Top = 48
                  Width = 95
                  Height = 13
                  Caption = '--net_buffer_length='
                  Visible = False
                end
                object LaCharacter: TLabel
                  Left = 236
                  Top = 48
                  Width = 109
                  Height = 13
                  Caption = '--default-character-set='
                end
                object Label4: TLabel
                  Left = 8
                  Top = 110
                  Width = 135
                  Height = 13
                  Caption = 'Senha alternativa (opcional):'
                  Visible = False
                end
                object Label8: TLabel
                  Left = 8
                  Top = 150
                  Width = 159
                  Height = 13
                  Caption = 'Caminho do Zip Command Line'#169':'
                  Visible = False
                end
                object EdPathMySQL: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 323
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'C:\Program Files\MySQL\MySQL Server 4.1\bin'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'C:\Program Files\MySQL\MySQL Server 4.1\bin'
                  ValWarn = False
                end
                object EdAllow: TdmkEdit
                  Left = 8
                  Top = 64
                  Width = 117
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1048576'
                  ValMax = '33554432'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2097152'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2097152
                  ValWarn = False
                end
                object EdBuffer: TdmkEdit
                  Left = 128
                  Top = 64
                  Width = 101
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '8192'
                  ValMax = '1048576'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '32768'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 32768
                  ValWarn = False
                end
                object EdCharacter: TdmkEdit
                  Left = 232
                  Top = 64
                  Width = 121
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1048576'
                  ValMax = '33554432'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'latin1'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'latin1'
                  ValWarn = False
                end
                object EdSenha: TEdit
                  Left = 8
                  Top = 126
                  Width = 345
                  Height = 24
                  PasswordChar = '*'
                  TabOrder = 4
                  Visible = False
                end
                object CkCriaDB: TCheckBox
                  Left = 8
                  Top = 90
                  Width = 95
                  Height = 17
                  Caption = 'Cria database.'
                  TabOrder = 5
                end
                object GroupBox1: TGroupBox
                  Left = 4
                  Top = 239
                  Width = 345
                  Height = 57
                  Caption = ' N'#227'o usa!!!!! '
                  TabOrder = 7
                  Visible = False
                  object LaCharSet: TLabel
                    Left = 8
                    Top = 12
                    Width = 64
                    Height = 13
                    Caption = '--set-charset='
                  end
                  object EdCharSet: TdmkEdit
                    Left = 12
                    Top = 28
                    Width = 133
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '1048576'
                    ValMax = '33554432'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = 'latin1'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 'latin1'
                    ValWarn = False
                  end
                end
                object CkExportaXML: TCheckBox
                  Left = 110
                  Top = 90
                  Width = 157
                  Height = 17
                  Caption = 'Exportar em formato XML.'
                  TabOrder = 8
                end
                object EdZipPath: TEdit
                  Left = 8
                  Top = 166
                  Width = 345
                  Height = 24
                  TabOrder = 6
                  Text = 'C:\Arquivos de Programas\WinZip'
                  Visible = False
                end
              end
            end
          end
        end
        object Memo3: TMemo
          Left = 876
          Top = 0
          Width = 124
          Height = 320
          Align = alRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          Visible = False
          WordWrap = False
        end
        object MeResult: TRichEdit
          Left = 581
          Top = 0
          Width = 186
          Height = 320
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 6
          OnKeyDown = MeResultKeyDown
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Exporta'#231#227'o'
      ImageIndex = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1002
        object Label7: TLabel
          Left = 12
          Top = 4
          Width = 74
          Height = 13
          Caption = 'Base de dados:'
        end
        object CBDB2: TDBLookupComboBox
          Left = 12
          Top = 20
          Width = 181
          Height = 21
          KeyField = 'Database'
          ListField = 'Database'
          ListSource = DsDatabases2
          TabOrder = 0
          OnClick = CBDB2Click
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 48
        Width = 1000
        Height = 378
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        ExplicitWidth = 1002
        ExplicitHeight = 383
        object RGExporta: TRadioGroup
          Left = 12
          Top = 52
          Width = 185
          Height = 105
          Caption = ' Configura'#231#227'o: '
          Items.Strings = (
            'Bancos e CNAB'
            'Folha de pagamento')
          TabOrder = 0
        end
        object Memo1: TMemo
          Left = 422
          Top = 0
          Width = 580
          Height = 381
          Align = alRight
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 196
        Height = 32
        Caption = 'Backup 3 (Beta)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 196
        Height = 32
        Caption = 'Backup 3 (Beta)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 196
        Height = 32
        Caption = 'Backup 3 (Beta)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 502
    Width = 1008
    Height = 88
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 71
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 8
        Top = 4
        Width = 52
        Height = 18
        Caption = 'Tempo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object LaAviso2: TLabel
        Left = 9
        Top = 5
        Width = 52
        Height = 18
        Caption = 'Tempo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object PB1: TProgressBar
        Left = 12
        Top = 24
        Width = 981
        Height = 17
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 12
        Top = 44
        Width = 981
        Height = 17
        TabOrder = 1
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 590
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Button1: TButton
        Left = 631
        Top = 12
        Width = 225
        Height = 25
        Caption = 'Teste mySQLDump 2011-06-17'
        TabOrder = 1
        Visible = False
        OnClick = Button1Click
      end
    end
  end
  object QrDatabases1: TMySQLQuery
    Database = DbAll
    AfterScroll = QrDatabases1AfterScroll
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 40
    Top = 196
    object QrDatabases1Database: TWideStringField
      FieldName = 'Database'
      Required = True
      Size = 64
    end
  end
  object DsDatabases1: TDataSource
    DataSet = QrDatabases1
    Left = 68
    Top = 196
  end
  object QrTabs: TMySQLQuery
    Database = DbAll
    SQL.Strings = (
      'SHOW TABLES FROM :p0')
    Left = 96
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsTabs: TDataSource
    DataSet = QrTabs
    Left = 124
    Top = 196
  end
  object DbAll: TMySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 96
    Top = 224
  end
  object PMTabelas: TPopupMenu
    Left = 126
    Top = 134
    object Todas1: TMenuItem
      Caption = '&Todas'
      OnClick = Todas1Click
    end
    object Nenhuma1: TMenuItem
      Caption = '&Nenhuma'
      OnClick = Nenhuma1Click
    end
    object Prdefinidas1: TMenuItem
      Caption = '-'
    end
    object PadresCNAB1: TMenuItem
      Caption = 'Padr'#245'es &CNAB'
      OnClick = PadresCNAB1Click
    end
    object Basesdefolhadepagamento1: TMenuItem
      Caption = 'Bases de &Folha de pagamento'
      OnClick = Basesdefolhadepagamento1Click
    end
    object ExportaodeSPEDEFD1: TMenuItem
      Caption = 'Exporta'#231#227'o de &SPED EFD'
      OnClick = ExportaodeSPEDEFD1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DTBNFe1: TMenuItem
      Caption = 'EFD + &DTB + NFe'
      OnClick = DTBNFe1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object NFeLayout1: TMenuItem
      Caption = 'NF-e - &Layout'
      OnClick = NFeLayout1Click
    end
    object NFeWebServices1: TMenuItem
      Caption = 'NF-e - &Web Services'
      OnClick = NFeWebServices1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object DControl1: TMenuItem
      Caption = '&DControl'
      OnClick = DControl1Click
    end
    object TMenuItem
      Caption = '-'
    end
    object Planodecontas1: TMenuItem
      Caption = 'Plano de contas'
      OnClick = Planodecontas1Click
    end
  end
  object DsDatabases2: TDataSource
    DataSet = QrDatabases1
    Left = 68
    Top = 224
  end
  object QrDatabases2: TMySQLQuery
    Database = DbAll
    AfterScroll = QrDatabases1AfterScroll
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 40
    Top = 224
    object StringField1: TWideStringField
      FieldName = 'Database'
      Required = True
      Size = 64
    end
  end
  object QrSel: TMySQLQuery
    Database = DbAll
    Left = 124
    Top = 224
  end
  object SaveDialog1: TFileSaveDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = []
    Left = 84
    Top = 280
  end
  object mySQLDump1: TMySQLDump
    Database = DbAll
    Delimiter = ';'
    LineComment = '-- '
    Left = 488
    Top = 24
  end
end
