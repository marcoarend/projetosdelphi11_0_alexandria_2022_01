unit UCreate;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnGrl_Geral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables, dmkGeral, UnDmkEnums;

type
  TNomeTabRecriaTempTable = (ntrtt_CodNom, ntrtt_CodAnt, ntrtt_CodQtd, ntrtt_Test000001,
    // Financeiro
    ntrttConfPgtos, ntrttIOF, ntrttLct, (*ntrttSaldos,*) ntrttSimulaPg,
    // Geral
    ntrttFavoritosPage, ntrttFavoritosTBar, ntrttFavoritosBtns,
    ntrttSelCods,
    //
    ntrttInfoSeq,
    //
    ntrttresliqfac,
    //
    ntrttEnt9Dig,
    //
    ntrttCfgRel,
    ntrttSdoNivCtas,
    ntrttCtasNiv,
    ntrttExtratoCC2,
    ntrttPagRec1,
    ntrttPagRec2,
    ntrttSdoNiveis,
    //
    ntrttArre, ntrttArreBaA, ntrttArreBaAUni, ntrttPri,
    ntrttBoletos, ntrttBoletosC, ntrttBoletosDR, ntrttBoletosDI, ntrttBoletosR,
    ntrttBoletosSR, ntrttBoletosValor, ntrttBoletosCarne,
    ntrttCNABBcoArq, ntrttCNABBcoDir, ntrttCNABBcoReg,
    ntrttCondParent,
    ntrttCopiaCH1, ntrttCopiaCH2,
    ntrttEmeios,
    ntrttHinMus,
    ntrttMPInRat,
    ntrttImpBMZ,
    ntrttImprimir1,
    ntrttLReA,
    ntrttMPInClasMul, ntrttMPInSubPMul,
    ntrttMU6MM, ntrttMU6MT,
    ntrttProtoMail,
    ntrttUnidCond,
    ntrttBloArreTmp,
    {
    ntrttNFeArqs, ntrttNFePR01, ntrttNFeCP01, ntrttNFeCR01, ntrttNFeDP01,
    ntrttNFeDR01, ntrttNFeXR01, ntrttNFeYR01, ntrttNFeZR01,
    ntrttNFeA, ntrttNFeB, ntrttNFeB12a, ntrttNFeC, ntrttNFeD, ntrttNFeE,
    ntrttNFeF, ntrttNFeG, ntrttNFeI, ntrttNFeIDi, ntrttNFeIDiA, ntrttNFeN,
    ntrttNFeO, ntrttNFeP, ntrttNFeQ, ntrttNFeR, ntrttNFeS, ntrttNFeT, ntrttNFeU,
    ntrttNFeV, ntrttNFeW02, ntrttNFeW17, ntrttNFeW23, ntrttNFeX22, ntrttNFeX01,
    ntrttNFeX26, ntrttNFeX33, ntrttNFeY01, ntrttNFeY07, ntrttNFeZ01, ntrttNFeZ04,
    ntrttNFeZ07, ntrttNFeZ10, ntrttNFeZA01, ntrttNFeZB01,
    //ntrttNFeAP01,
    }
    ntrttPrevBAB,
    ntrttPrevLcts,
    ntrttPrevVeri,
    ntrttConsLei,
    ntrttSemQuita,
    ntrttGrafico,
    ntrttWOrdSerTmp,
    {
    ntrttSintegra10, ntrttSintegra11, ntrttSintegra50, ntrttSintegra51,
    ntrttSintegra53, ntrttSintegra54, ntrttSintegra70, ntrttSintegra71,
    ntrttSintegra74, ntrttSintegra75, ntrttSintegra85, ntrttSintegra86,
    ntrttSintegra88, ntrttSintegra90, ntrttSintegraPM, ntrttSintegraEC,
    ntrttSintegraEr,
    ntrttS50Load, ntrttS54Load, ntrttS70Load, ntrttS75Load,
    }
    ntrttPesqESel,
    ntrttSmiaMPIn,
    ntrttLctoEdit,
    ntrttFatPedFis
    {ntrttStqMovValX}
    );
  TAcaoCreate = (acDrop, acCreate, acFind);
  TUCreate = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttCodQtd(Qry: TmySQLQuery);
    procedure Cria_ntrttProtoMail(Qry: TmySQLQuery);
    procedure Cria_ntrttUnidCond(Qry: TmySQLQuery);
    procedure Cria_ntrttBloArreTmp(Qry: TmySQLQuery);
    procedure Cria_ntrttImpBMZ(Qry: TmySQLQuery);
    procedure Cria_ntrttMPInRat(Qry: TmySQLQuery);
    procedure Cria_ntrttImprimir1(Qry: TmySQLQuery);
    procedure Cria_ntrttPesqESel(Qry: TmySQLQuery);
    procedure Cria_ntrttSmiaMPIn(Qry: TmySQLQuery);
    procedure Cria_ntrttLctoEdit(Qry: TmySQLQuery);
    procedure Cria_ntrttFatPedFis(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletos(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletosC(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletosDR(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletosDI(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletosR(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletosSR(Qry: TmySQLQuery);
    procedure Cria_ntrttBoletosValor(Qry: TmySQLQuery);
    procedure Cria_ntrttConfPgtos(Qry: TmySQLQuery);
    procedure Cria_ntrttIOF(Qry: TmySQLQuery);
    procedure Cria_ntrttLct(Qry: TmySQLQuery);
    //procedure Cria_ntrttSaldos(Qry: TmySQLQuery);
    procedure Cria_ntrttSelCods(Qry: TmySQLQuery);
    procedure Cria_ntrttSimulaPg(Qry: TmySQLQuery);
    procedure Cria_ntrttFavoritosPage(Qry: TmySQLQuery);
    procedure Cria_ntrttFavoritosTBar(Qry: TmySQLQuery);
    procedure Cria_ntrttFavoritosBtns(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttInfoSeq(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttresliqfac(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttEnt9Dig(Qry: TmySQLQuery);
    //
    procedure Cria_ntrttCfgRel(Qry: TmySQLQuery);
    procedure Cria_ntrttSdoNivCtas(Qry: TmySQLQuery);
    procedure Cria_ntrttCtasNiv(Qry: TmySQLQuery);
    procedure Cria_ntrttExtratoCC2(Qry: TmySQLQuery);
    procedure Cria_ntrttPagRec1(Qry: TmySQLQuery);
    procedure Cria_ntrttPagRec2(Qry: TmySQLQuery);
    procedure Cria_ntrttSdoNiveis(Qry: TmySQLQuery);
    //
    function ContinuaEmOutraProcedure(TabNo: String; Qry: TmySQLQuery; Nome: String): String;
//{$ifNDef SemDBLocal}
    //function ContinuaRecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
//{$EndIf}

  public
    { Public declarations }
//{$ifNDef SemDBLocal}
{
    function ReabreQueryLocal(Query: TMySQLQuery; Tab: String; Avisa:
             Boolean): Boolean;
    function AtualizaTabelaControleLocal(CampoInt, CampoStr, CampoFloat: String;
             ValInt: Integer; ValStr: String; ValFloAt: Double): Boolean;
    function GerenciaTabelaLocal(Tabela: String; Acao: TAcaoCreate): Boolean;
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
}
//{$EndIf}
    // Procedures e functions da base de dados por usuario
    function GerenciaTempTable(Tabela: String; Acao: TAcaoCreate;
             DataModule: TDataModule; DataBase: TmySQLDatabase; Qry: TmySQLQuery;
             UniqueTableName: Boolean; Repeticoes: Integer = 1;
             NomeTab: String = ''): String;
    function RecriaTempTable(Tabela: String; Qry: TmySQLQuery;
             UniqueTableName: Boolean; Repeticoes: Integer = 1;
             NomeTab: String = ''): String;
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
    function InsereRegistrosCodNom(QrUpd: TmySQLQuery; NomeTab: String;
             Nomes: array of String): Boolean;
    function  RecriaListaSimplesSelMany(DB: TmySQLDatabase; TabToCopy: String):
              String;
    procedure MsgDBLocalDesabilitado();
  end;

var
  UCriar: TUCreate;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

procedure TUCreate.Cria_ntrttBloArreTmp(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle       integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  Seq            integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  Periodo        integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  Conta          integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  BloArre        integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  Blo'+TAB_ARI+' integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  CartEmiss      integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  CNAB_Cfg       integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  Fatur_Cfg      integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  NFSeSrvCad     integer(11)  DEFAULT 0,');
  Qry.SQL.Add('  Valor          double(15,2) DEFAULT "0.00",');
  Qry.SQL.Add('  Texto          varchar(100) DEFAULT NULL,');
  Qry.SQL.Add('  DiaVencto      integer(2)   DEFAULT 0,');
  Qry.SQL.Add('  Vencto         date         DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  Adiciona       tinyint(1)   DEFAULT 0,');
  Qry.SQL.Add('  Entidade       int(11)      DEFAULT 0,');
  Qry.SQL.Add('  IDArre         int(11)      DEFAULT 0,');
  Qry.SQL.Add('  NomeEnti       varchar(100) DEFAULT ""');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttBoletos(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  FracaoIdeal          double(15,6) NOT NULL                      ,');
  Qry.SQL.Add('  Andar                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Boleto               double(15,0) NOT NULL                      ,');
  Qry.SQL.Add('  Apto                 int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Propriet             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Vencto               date         NOT NULL                      ,');
  Qry.SQL.Add('  Unidade              varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  NOMEPROPRIET         varchar(100) NOT NULL                      ,');
  Qry.SQL.Add('  BOLAPTO              varchar(65)  NOT NULL                      ,');
  Qry.SQL.Add('  cdb_IDExpota         varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  cdi_IDExpota         varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  KGT                  bigint(20)   NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttBoletosC(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 char(1)      NOT NULL                      ,');
  Qry.SQL.Add('  CNPJ                 char(14)     NOT NULL                      ,');
  Qry.SQL.Add('  Ano                  int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Mes                  int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Vencto               date         NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
Detalhe de Taxas Individuais
Fld  Posi��o Tamanho Tipo      Descri��o
[01] 1       2       Fixo      Preencher com as letras DI
[02] 3       3       Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6       15      Texto     Complemento
[04] 16(21)  6       Texto     Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
[05] 27      4       Texto     Unidade (ver tabela 1)
[06] 31      1       Num�rico  Tipo de Medida utilizada (ver tabela 2)
[07] 32      7       Num�rico  Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s e �gua.
[08] 39      7       Num�rico  Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s.
[09] 46      7       Num�rico  Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas e G�s.
[10] 53      9       Num�rico  Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
[11] 62      9       Num�rico  Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500

Exemplo: DI006               A     04  1000011900001230000400000002000000000238
}
procedure TUCreate.Cria_ntrttBoletosDI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  CodTaxa              int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  Compl                char(15)     NOT NULL                      ,');
  Qry.SQL.Add('  Bloco                varchar(6)   NOT NULL                      ,');
  Qry.SQL.Add('  Unidade              varchar(4)   NOT NULL                      ,');
  Qry.SQL.Add('  TMU                  tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  LeituAnt             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  LeituAtu             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Preco                double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Valor                double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
Detalhe de Taxas para     Rateio
Fld  Posi��o  Tamanho  Tipo      Descri��o
[01] 1        2        Fixo      Preencher com as letras DR
[02] 3        3        Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6        15       Texto     Complemento
[04] 16(21)   6        Texto     Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
[05] 27       9        Num�rico  Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500

 Exemplo: DR001TESTE                000015000
}
procedure TUCreate.Cria_ntrttBoletosDR(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  CodTaxa              int(3)       NOT NULL                      ,');
  Qry.SQL.Add('  Compl                char(15)     NOT NULL                      ,');
  Qry.SQL.Add('  Bloco                varchar(6)   NOT NULL                      ,');
  Qry.SQL.Add('  Valor                double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
Rodap�
Posi��o Tamanho Tipo      Descri��o
1       1       Fixo      Preencher com a letra R
2       6       Num�rico  Total de linhas do arquivo formatado com zeros a esquerda, exemplo 000099

Exemplo: R000099
}
procedure TUCreate.Cria_ntrttBoletosR(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  Registros            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttBoletosSR(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ITENS                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Valor                double(15,2) NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttBoletosValor(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Lancto               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  CliInt               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  ValorLct             double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  ValorPeri            double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttCodQtd(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  QtdeA                double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  QtdeB                double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  QtdeC                double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttProtoMail(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Bloqueto     double(20,0)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Depto_Cod    integer(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Depto_Txt    varchar(100), ');
  Qry.SQL.Add('  Entid_Cod    integer(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Entid_Txt    varchar(100), ');
  Qry.SQL.Add('  Taref_Cod    integer(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Taref_Txt    varchar(100), ');
  Qry.SQL.Add('  Deliv_Cod    integer(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Deliv_Txt    varchar(100), ');
  Qry.SQL.Add('  DataE        Date, ');
  Qry.SQL.Add('  DataE_Txt    varchar(30), ');
  Qry.SQL.Add('  DataD        Date, ');
  Qry.SQL.Add('  DataD_Txt    varchar(30), ');
  Qry.SQL.Add('  Protocolo    integer(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  ProtoLote    integer(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  NivelEmail   tinyint(1)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  NivelDescr   varchar(50), ');
  Qry.SQL.Add('  RecipItem    int(11), ');
  Qry.SQL.Add('  RecipEmeio   varchar(100), ');
  Qry.SQL.Add('  RecipNome    varchar(100), ');
  Qry.SQL.Add('  RecipProno   varchar(20), ');
  Qry.SQL.Add('  Vencimento   date  NOT NULL DEFAULT "0000-00-00", ');
  Qry.SQL.Add('  Valor        double(15,2)  NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  Condominio   varchar(255)  NOT NULL DEFAULT "??", ');
  Qry.SQL.Add('  PreEmeio     int(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  IDEmeio      int(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Item         int(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  PrevCod      int(11)  NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Periodo      int(11)  NOT NULL DEFAULT "0"  ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttresliqfac(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NOMECLIENTE  varchar(100),                         ');
  Qry.SQL.Add('  NOMEMES      varchar(20),                          ');
  Qry.SQL.Add('  Mes          integer(11)  DEFAULT 0   ,            ');
  Qry.SQL.Add('  Data         date,                                 ');
  Qry.SQL.Add('  Lote         integer(11)  NOT NULL DEFAULT "0",    ');
  Qry.SQL.Add('  Codigo       integer(11)  NOT NULL DEFAULT "0",    ');
  Qry.SQL.Add('  PROPRIO      double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  TXCOMPRAT    double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  VALVALOREMT  double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  MARGEM_TODOS double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  REPAS_V      double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  MARGEM_BRUTA double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  NREP_T       double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  PRO_COL      double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  MARGEM_COL   double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  PIS_T_VAL    double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  ISS_VAL      double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  COFINS_T_VAL double(15,2) NOT NULL DEFAULT "0.00", ');
  Qry.SQL.Add('  TOTAL        double(15,2) NOT NULL DEFAULT "0.00"  ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci   ');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttEnt9Dig(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo    integer(11)  NOT NULL DEFAULT "0"    ,');
  Qry.SQL.Add('  Conta     integer(11)           DEFAULT "0"    ,');
  Qry.SQL.Add('  Tipo      tinyint(11)           DEFAULT "0"    ,');
  Qry.SQL.Add('  Nome      varchar(100)                         ,');
  Qry.SQL.Add('  Telefone  varchar(20)                          ,');
  Qry.SQL.Add('  TelefoneN varchar(20)                          ,');
  Qry.SQL.Add('  EhCelular tinyint(11)           DEFAULT "0"    ,');
  Qry.SQL.Add('  AtzCadas  tinyint(11)           DEFAULT "0"     ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttCfgRel(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"        ,');
  Qry.SQL.Add('  RelTitu              varchar(100) NOT NULL  DEFAULT "0"        ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL  DEFAULT "0"        ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "1"        ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttSdoNivCtas(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NomeNivel    varchar(20),                                    ');
  Qry.SQL.Add('  Nivel        int(11)       NOT NULL DEFAULT 0,               ');
  Qry.SQL.Add('  NomeGenero   varchar(100),                                   ');
  Qry.SQL.Add('  Genero       int(11)       NOT NULL DEFAULT 0,               ');
  Qry.SQL.Add('  SdoIni       double(15,2)  NOT NULL DEFAULT "0.00",          ');
  Qry.SQL.Add('  MovCre       double(15,2)  NOT NULL DEFAULT "0.00",          ');
  Qry.SQL.Add('  MovDeb       double(15,2)  NOT NULL DEFAULT "0.00",          ');
  Qry.SQL.Add('  SdoFim       double(15,2)  NOT NULL DEFAULT "0.00",          ');
  Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 0');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttCtasNiv(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NOMENIVEL            varchar(20)                             ,');
  Qry.SQL.Add('  NOMEGENERO           varchar(50)                             ,');
  Qry.SQL.Add('  Nivel                int(11)      NOT NULL  DEFAULT "0"      ,');
  Qry.SQL.Add('  Genero               int(11)      NOT NULL  DEFAULT "0"      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "0"       ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttExtratoCC2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataE      date             , '); // Emissao
  Qry.SQL.Add('  DataV      date             , '); // Vencto
  Qry.SQL.Add('  DataQ      date             , '); // Quita��o
  Qry.SQL.Add('  DataX      date             , '); // Data considerada
  Qry.SQL.Add('  Texto      varchar(255)     , ');
  Qry.SQL.Add('  Docum      varchar(30)      , ');
  Qry.SQL.Add('  NotaF      varchar(30)      , ');
  Qry.SQL.Add('  Credi      double(15,2)     , ');
  Qry.SQL.Add('  Debit      double(15,2)     , ');
  Qry.SQL.Add('  SdoCr      double(15,2)     , '); // Saldo a receber
  Qry.SQL.Add('  SdoDb      double(15,2)     , '); // Saldo a Pagar
  Qry.SQL.Add('  Saldo      double(15,2)     , ');
  Qry.SQL.Add('  Ctrle      int(11)          , ');
  Qry.SQL.Add('  CtSub      int(11)          , ');
  Qry.SQL.Add('  ID_Pg      int(11)          , ');
  Qry.SQL.Add('  CartC      int(11)          , ');
  Qry.SQL.Add('  CartN      varchar(100)     , ');
  Qry.SQL.Add('  TipoI      int(11)          , ');
  Qry.SQL.Add('  DataCad    date             , ');
  Qry.SQL.Add('  UserCad    int(11)          , ');
  Qry.SQL.Add('  DataAlt    date             , ');
  Qry.SQL.Add('  UserAlt    int(11)          , ');
  Qry.SQL.Add('  Codig      int(11)          , ');
  Qry.SQL.Add('  tCrt       int(11)          , ');
  Qry.SQL.Add('  Sit        int(11)            ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttPagRec1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data          date, ');
  Qry.SQL.Add('  DataDoc       date, ');
  Qry.SQL.Add('  Vencimento    date, ');
  Qry.SQL.Add('  TERCEIRO      double DEFAULT "0", ');
  Qry.SQL.Add('  NOME_TERCEIRO varchar(255), ');
  Qry.SQL.Add('  NOMEVENCIDO   varchar(255), ');
  Qry.SQL.Add('  NOMECONTA     varchar(255), ');
  Qry.SQL.Add('  NOMECARTEIRA  varchar(255), ');
  Qry.SQL.Add('  Qtde          double(15,2), ');
  Qry.SQL.Add('  Descricao     varchar(255), ');
  Qry.SQL.Add('  Duplicata     varchar( 15), ');
  Qry.SQL.Add('  CtrlPai       double, ');
  Qry.SQL.Add('  Tipo          double, ');
  Qry.SQL.Add('  ID_Pgto       double, ');
  Qry.SQL.Add('  CtrlIni       double, ');
  Qry.SQL.Add('  Documento     double, ');
  Qry.SQL.Add('  NotaFiscal    double, ');
  Qry.SQL.Add('  Controle      double, ');
  Qry.SQL.Add('  Valor         double(15,2), ');
  Qry.SQL.Add('  MoraDia       double(15,2), ');
  Qry.SQL.Add('  PAGO_REAL     double(15,2), ');
  Qry.SQL.Add('  PENDENTE      double(15,2), ');
  Qry.SQL.Add('  ATUALIZADO    double(15,2), ');
  Qry.SQL.Add('  MULTA_REAL    double(15,2), ');
  Qry.SQL.Add('  ATRAZODD      double, ');
  Qry.SQL.Add('  NO_UH         varchar(10) ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttPagRec2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data  date, ');
  Qry.SQL.Add('  NOMEVENCIDO  varchar(255), ');
  Qry.SQL.Add('  NOMECONTA    varchar(255), ');
  Qry.SQL.Add('  NOMECARTEIRA varchar(255), ');
  Qry.SQL.Add('  Descricao    varchar(255), ');
  Qry.SQL.Add('  Documento    double, ');
  Qry.SQL.Add('  NotaFiscal   double, ');
  Qry.SQL.Add('  Controle     double NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  ID_Pgto      double NOT NULL DEFAULT "0", ');
  Qry.SQL.Add('  Valor        double(15,2), ');
  Qry.SQL.Add('  MoraDia      double(15,2), ');
  Qry.SQL.Add('  PAGO_REAL    double(15,2), ');
  Qry.SQL.Add('  ATUALIZADO   double(15,2), ');
  Qry.SQL.Add('  MULTA_REAL   double(15,2), ');
  Qry.SQL.Add('  ATRAZODD     double, ');
  Qry.SQL.Add('  PRIMARY KEY (ID_Pgto,Controle)');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttSdoNiveis(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Nivel        int(11)                                            ,');
  Qry.SQL.Add('  CodPla       int(11)                                            ,');
  Qry.SQL.Add('  CodCjt       int(11)                                            ,');
  Qry.SQL.Add('  CodGru       int(11)                                            ,');
  Qry.SQL.Add('  CodSgr       int(11)                                            ,');
  Qry.SQL.Add('  Genero       int(11)                                            ,');
  Qry.SQL.Add('  Ordena       varchar(100)                                       ,');
  Qry.SQL.Add('  NomeGe       varchar(100)                                       ,');
  Qry.SQL.Add('  NomeNi       varchar(20)                                        ,');
  Qry.SQL.Add('  SumMov       double(15,2)                                       ,');
  Qry.SQL.Add('  SdoAnt       double(15,2)                                       ,');
  Qry.SQL.Add('  SumCre       double(15,2)                                       ,');
  Qry.SQL.Add('  SumDeb       double(15,2)                                       ,');
  Qry.SQL.Add('  SdoFim       double(15,2)                                       ,');
  // usado para cadastrar
  Qry.SQL.Add('  Ctrla        tinyint(1)                                         ,');
  Qry.SQL.Add('  Seleci       tinyint(1)                                         ,');
  // usado para obter saldo do modo antigo para o novo
  Qry.SQL.Add('  IniOld       double(15,2)                                       ,');
  Qry.SQL.Add('  PRIMARY KEY (Nivel,Genero)');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttConfPgtos(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle   integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  Empresa    integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  Entidade   integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  NomeEnti   varchar(100) NOT NULL DEFAULT "? ? ?"                ,');
  Qry.SQL.Add('  Genero     integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  NomeCta    varchar(100) NOT NULL DEFAULT "? ? ?"                ,');
  Qry.SQL.Add('  Descricao  varchar(255) NOT NULL DEFAULT "? ? ?"                ,');
  Qry.SQL.Add('  QtdeMin    integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  QtdeMax    integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  QtdeExe    integer      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  ValrMin    double(15,2) NOT NULL DEFAULT "0.00"                 ,');
  Qry.SQL.Add('  ValrMax    double(15,2) NOT NULL DEFAULT "0.00"                 ,');
  Qry.SQL.Add('  ValrExe    double(15,2) NOT NULL DEFAULT "0.00"                 ,');
  Qry.SQL.Add('  DiaAlert   tinyint      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  DiaVence   tinyint      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  Alertar    tinyint      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  Status     integer      NOT NULL DEFAULT "0"                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttFavoritosBtns(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Seq                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Classe               varchar(255)                               ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  DescrPage            varchar(255)                               ,');
  Qry.SQL.Add('  DescrTBar            varchar(255)                               ,');
  Qry.SQL.Add('  DescrBtn             varchar(255)                               ,');
  Qry.SQL.Add('  PRIMARY KEY (Seq)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttInfoSeq(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha int(11), ');
  Qry.SQL.Add('  Item  int(11), ');
  Qry.SQL.Add('  Local varchar(255), ');
  Qry.SQL.Add('  Acao varchar(255) ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttFavoritosPage(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttFavoritosTBar(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo, Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttImpBMZ(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha       int(11),     ');
  Qry.SQL.Add('  Etapa       int(11),     ');
  Qry.SQL.Add('  SubEtapa    int(11),     ');
  Qry.SQL.Add('  GraGruX     int(11),     ');
  Qry.SQL.Add('  Insumo      int(11),     ');
  Qry.SQL.Add('  NomeEtapa   varchar(255),');
  Qry.SQL.Add('  TempoP      int(11),     ');
  Qry.SQL.Add('  TempoR      int(11),     ');
  Qry.SQL.Add('  pH          double(15,2),');
  Qry.SQL.Add('  pH_Min      double(15,2),');
  Qry.SQL.Add('  pH_Max      double(15,2),');
  Qry.SQL.Add('  Be          double(15,2),');
  Qry.SQL.Add('  Be_Min      double(15,2),');
  Qry.SQL.Add('  Be_Max      double(15,2),');
  Qry.SQL.Add('  GC_Min      double,      ');
  Qry.SQL.Add('  GC_Max      double,      ');
  Qry.SQL.Add('  Diluicao    double(15,2),');
  Qry.SQL.Add('  Graus       double(15,2),');
  Qry.SQL.Add('  Observacos  varchar(255),');
  Qry.SQL.Add('  ObserProce  varchar(255),');
  Qry.SQL.Add('  Perc        double(15,3),');
  Qry.SQL.Add('  Peso        double(15,3),');
  Qry.SQL.Add('  Descricao   varchar(255),');
  Qry.SQL.Add('  Porcent     double(15,3),');
  Qry.SQL.Add('  CustoKg     double(15,4),');
  Qry.SQL.Add('  CustoIt     double(15,3),');
  Qry.SQL.Add('  NxLotes     varchar(255),');
  Qry.SQL.Add('  SiglaIQ     varchar(6)');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttImprimir1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Campo        int(11),     ');
  Qry.SQL.Add('  Valor        varchar(255),');
  Qry.SQL.Add('  Prefixo      varchar(255),');
  Qry.SQL.Add('  Sufixo       varchar(255),');
  Qry.SQL.Add('  Substituicao varchar(255),');
  Qry.SQL.Add('  Substitui    int(1),      ');
  Qry.SQL.Add('  Nulo         int(1),      ');
  Qry.SQL.Add('  Forma        int(11),     ');
  Qry.SQL.Add('  Conta        int(11),     ');
  Qry.SQL.Add('  DBand        int(11),     ');
  Qry.SQL.Add('  DTopo        int(11),     ');
  Qry.SQL.Add('  DMEsq        int(11),     ');
  Qry.SQL.Add('  DComp        int(11),     ');
  Qry.SQL.Add('  DAltu        int(11),     ');
  Qry.SQL.Add('  Set_N        int(11),     ');
  Qry.SQL.Add('  Set_I        int(11),     ');
  Qry.SQL.Add('  Set_U        int(11),     ');
  Qry.SQL.Add('  Set_T        int(11),     ');
  Qry.SQL.Add('  Set_A        int(11),     ');
  Qry.SQL.Add('  Linha        int(11),     ');
  Qry.SQL.Add('  Colun        int(11),     ');
  Qry.SQL.Add('  Pagin        int(11),     ');
  Qry.SQL.Add('  Tipo         int(11)      ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttIOF(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Parcela              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  BaseCalc             double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  Vencimento           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  DiasParcel           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DiasCorrid           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  MesesPerio           double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  FatorJuros           double(15,6)           DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ValCorrigi           double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  ValPrincip           double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  ValJuros             double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  IOF_Percen           double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  IOF_Valor            double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Prestacao            double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  Ativo                int(11)      NOT NULL  DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttLct(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Tipo                 tinyint(4)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Carteira             mediumint(9) NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Genero               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Descricao            varchar(100)                               ,');
  Qry.SQL.Add('  NotaFiscal           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Credito              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
  Qry.SQL.Add('  Documento            double(20,0)           DEFAULT "0"         ,');
  Qry.SQL.Add('  Sit                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Vencimento           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  FatID                int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  FatID_Sub            int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  FatNum               double(20,0)           DEFAULT "0"         ,');
  Qry.SQL.Add('  FatParcela           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Mez                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Fornecedor           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Cliente              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliInt               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  ForneceI             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  MoraDia              double(13,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Multa                double(13,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  DataDoc              date                                       ,');
  Qry.SQL.Add('  Vendedor             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Account              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Duplicata            varchar(13)                                ,');
  Qry.SQL.Add('  Depto                int(11)                                    ,');
  Qry.SQL.Add('  Unidade              int(11)      NOT NULL  DEFAULT "0"         ,');
  // 2011-08-21 (Blue Derm)
  Qry.SQL.Add('  Atrelado             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerieNF              char(3)                                    ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                int(11)      NOT NULL  DEFAULT "0"         ');
  // fim 2011-08-21
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttMPInRat(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle   int(11)      NOT NULL AUTO_INCREMENT ,');
  Qry.SQL.Add('  ProcedeCod int(11)      DEFAULT 0               ,');
  Qry.SQL.Add('  ProcedeNom varchar(100) DEFAULT "?"             ,');
  Qry.SQL.Add('  Pecas      double(15,1) DEFAULT 0               ,');
  Qry.SQL.Add('  PecasNF    double(15,1) DEFAULT 0               ,');
  Qry.SQL.Add('  PNF        double(15,3) DEFAULT 0               ,');
  Qry.SQL.Add('  PNF_Fat    double(15,3) DEFAULT 0               ,');
  Qry.SQL.Add('  PLE        double(15,3) DEFAULT 0               ,');
  Qry.SQL.Add('  PDA        double(15,3) DEFAULT 0               ,');
  Qry.SQL.Add('  Preco      double(15,6) DEFAULT 0               , ');
  Qry.SQL.Add('  Custo      double(15,2) DEFAULT 0               ,');
  Qry.SQL.Add('  Frete      double(15,2) DEFAULT 0               ,');
  Qry.SQL.Add('  NF         int(11)      DEFAULT 0               ,');
  Qry.SQL.Add('  Conheci    int(11)      DEFAULT 0               ,');
  Qry.SQL.Add('  Emissao    date                                 ,');
  Qry.SQL.Add('  Vencto     date                                 ,');
  Qry.SQL.Add('  EmisFrete  date                                 ,');
  Qry.SQL.Add('  VctoFrete  date                                 ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttPesqESel(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

(*
procedure TUCreate.Cria_ntrttSaldos(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CartCodi             int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  CartTipo             int(11)      NOT NULL DEFAULT -1           ,');
  Qry.SQL.Add('  CartNiv2             int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Nome                 varchar(50)  NOT NULL DEFAULT "???"        ,');
  Qry.SQL.Add('  Saldo                double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Tipo                 int(11)      NOT NULL DEFAULT 0             ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
*)

procedure TUCreate.Cria_ntrttSelCods(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Nivel1               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nivel2               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nivel3               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nivel4               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nivel5               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  PRIMARY KEY (Nivel1, Nivel2, Nivel3, Nivel4, Nivel5)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttSimulaPg(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Tipo                 int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Parcela              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  DataPg               date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  PerJuro              double(15,6) NOT NULL DEFAULT 0.000000     ,');
  Qry.SQL.Add('  ValParc              double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  ValOrig              double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  ValPrin              double(15,4) NOT NULL DEFAULT 0.0000       ,');
  Qry.SQL.Add('  ValJuro              double(15,4) NOT NULL DEFAULT 0.0000       ,');
  Qry.SQL.Add('  PercIOF              double(15,6) NOT NULL DEFAULT 0.000000     ,');
  Qry.SQL.Add('  ValrIOF              double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Dias                 int(11)      NOT NULL DEFAULT 0             ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttSmiaMPIn(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataHora             datetime     NOT NULL                      ,');
  Qry.SQL.Add('  IDCtrl               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  OriCodi              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  OriCtrl              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  MP_Qtde              double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  MP_Pecas             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  MP_Peso              double(15,3) NOT NULL                      ,');

  Qry.SQL.Add('  Bx_Qtde              double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Bx_Pecas             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Bx_Peso              double(15,3) NOT NULL                      ,');

  Qry.SQL.Add('  Er_Qtde              double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Er_Pecas             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Er_Peso              double(15,3) NOT NULL                      ,');

{
  Qry.SQL.Add('  WB_Qtde              double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  WB_Pecas             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  WB_Peso              double(15,3) NOT NULL                      ,');

  Qry.SQL.Add('  SB_Qtde              double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  SB_Pecas             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  SB_Peso              double(15,3) NOT NULL                      ,');

  Qry.SQL.Add('  Ou_Qtde              double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Ou_Pecas             double(15,3) NOT NULL                      ,');
  Qry.SQL.Add('  Ou_Peso              double(15,3) NOT NULL                      ,');
}

  Qry.SQL.Add('  ParCodi              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  PRIMARY KEY (IDCtrl)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttLctoEdit(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Sub                  int(11)                                    ,');
  Qry.SQL.Add('  Carteira             int(11)                                    ,');
  Qry.SQL.Add('  Descricao            varchar(255)                               ,');
  Qry.SQL.Add('  Data                 date                                       ,');
  Qry.SQL.Add('  Vencimento           date                                       ,');
  //Qry.SQL.Add('  Compensado           date                                       ,');
  Qry.SQL.Add('  MultaVal             double(15,2)                               ,');
  Qry.SQL.Add('  JurosVal             double(15,2)                               ,');
  Qry.SQL.Add('  ValorOri             double(15,2)                               ,');
  Qry.SQL.Add('  ValorPgt             double(15,2)                                ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttFatPedFis(Qry: TMySQLQuery);
begin
  Qry.SQL.Add('  OriCodi            int(11) NOT NULL          ,');
  Qry.SQL.Add('  OriCnta            int(11) NOT NULL          ,');
  Qry.SQL.Add('  Gragru1            int(11) NOT NULL          ,');
  Qry.SQL.Add('  CFOP               varchar(5)                ,');
  Qry.SQL.Add('  CST_B              char(2)                   ,');
  Qry.SQL.Add('  modBC              tinyint(1)                ,');
  Qry.SQl.Add('  pRedBC             double(5,2)               ,');
  Qry.SQL.Add('  UFEmit             char(2)                   ,');
  Qry.SQL.Add('  UFDest             char(2)                   ,');
  Qry.SQL.Add('  ICMSAliq           double(15,2)              ,');
  Qry.SQL.Add('  pICMSUFDest        double(6,4)               ,');
  Qry.SQL.Add('  pICMSInter         double(6,4)               ,');
  Qry.SQL.Add('  pFCPUFDest         double(6,4)               ,');
  Qry.SQL.Add('  pBCUFDest          double(15,10)             ,');
  Qry.SQL.Add('  pICMSInterPart     double(6,4)               ,');
  Qry.SQL.Add('  UsaInterPartLei    tinyint(1)                 ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreate.Cria_ntrttUnidCond(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Andar        int(11),      ');
  Qry.SQL.Add('  Apto         int(11),      ');
  Qry.SQL.Add('  Entidad      int(11),      ');
  Qry.SQL.Add('  Protoco      int(11),      ');
  Qry.SQL.Add('  Bloquet      int(11),      ');
  Qry.SQL.Add('  Unidade      varchar(10),  ');
  Qry.SQL.Add('  Proprie      varchar(100), ');
  Qry.SQL.Add('  Valor        double(15,2), ');
  Qry.SQL.Add('  Data1        date,         ');
  Qry.SQL.Add('  Data2        date,         ');
  Qry.SQL.Add('  Texto        varchar(100), ');
  Qry.SQL.Add('  RecipItem    int(11),      '); // ID
  Qry.SQL.Add('  RecipNome    varchar(100), ');
  Qry.SQL.Add('  RecipEmeio   varchar(100), ');
  Qry.SQL.Add('  RecipProno   varchar(20),  ');
  //
  Qry.SQL.Add('  Vencimento   date, ');
  Qry.SQL.Add('  Condominio   varchar(255), ');
  Qry.SQL.Add('  PreEmeio     int(11),');
  Qry.SQL.Add('  IDEmeio      int(11),');
  //
  Qry.SQL.Add('  Ordem        int(11),      ');
  Qry.SQL.Add('  Selecio      tinyint(1)    ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TUCreate.GerenciaTempTable(Tabela: String; Acao: TAcaoCreate;
DataModule: TDataModule; DataBase: TmySQLDatabase; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1;
NomeTab: String = ''): String;
var
  QtTemp1, QtTemp2: TmySQLQuery;
  Encontrado: Boolean;
begin
  Result := '';
  Encontrado := False;
  if Geral.ReadAppKeyLM(Tabela, Application.Title+'\Recriar', ktInteger, 0) = 1 then
  begin;
    Result := RecriaTempTable(Tabela, Qry, UniqueTableName, Repeticoes, NomeTab);
    Exit;
  end;
  QtTemp1 := TmySQLQuery.Create(DataModule);
  QtTemp1.DataBase := DataBase;
  QtTemp2 := TmySQLQuery.Create(DataModule);
  QtTemp2.DataBase := DataBase;
  QtTemp1.SQL.Add('SHOW TABLES');
  UnDmkDAC_PF.AbreQuery(QtTemp1, Database);
  while not QtTemp1.Eof do
  begin
    if Lowercase(QtTemp1.Fields[0].AsString) = Lowercase(Tabela) then
    begin
      Encontrado := True;
      if (Acao in ([acFind, acCreate])) then Result := Tabela
      else if (Acao = acDrop) then
      begin
        QtTemp2.SQL.Add('DROP TABLE IF EXISTS ' + Tabela);
        QtTemp2.ExecSQL;
        Result := '';
      end;
    end;
    QtTemp1.Next;
  end;
  if (Acao = acCreate) and (Encontrado = False) then
    Result := RecriaTempTable(Tabela, Qry, UniqueTableName, Repeticoes, NomeTab);
  QtTemp1.Free;
  QtTemp2.Free;
end;

function TUCreate.InsereRegistrosCodNom(QrUpd: TmySQLQuery; NomeTab: String;
Nomes: array of String): Boolean;
var
  i, j: Integer;
  Tab: String;
begin
  j := High(Nomes);
  Tab := LowerCase(NomeTab);
  QrUpd.SQL.Clear;
  for i := Low(Nomes) to j do
  begin
    QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET Codigo=' +
    FormatFloat('0', I) + ', Nome="' + Nomes[I] + '";');
  end;
  QrUpd.ExecSQL;
  Result := True;
end;

procedure TUCreate.MsgDBLocalDesabilitado();
begin
    Grl_Geral.InfoSemModulo(mdlappLocalDB);
end;

function TUCreate.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_CodNom:         Nome := Lowercase('_Cod_Nom_');
      ntrtt_CodAnt:         Nome := Lowercase('_Cod_Ant_');
      ntrtt_CodQtd:         Nome := Lowercase('_Cod_Qtd_');
      ntrttProtoMail:       Nome := Lowercase('_protomail_');
      ntrttUnidCond:        Nome := LowerCase('_unidcond_');
      ntrttBloArreTmp:      Nome := LowerCase('_bloarretmp_');
      ntrtt_Test000001:     Nome := Lowercase('Test000001');
      //
      ntrttConfPgtos:       Nome := Lowercase('_Conf_Pgtos_');
      ntrttIOF:             Nome := Lowercase('_iof_');
      ntrttLct:             Nome := Lowercase('_lct_');
      //ntrttSaldos:          Nome := Lowercase('Saldos');
      ntrttSelCods:         Nome := Lowercase('_SelCods_');
      ntrttSimulaPg:        Nome := Lowercase('SimulaPg');
      ntrttFavoritosPage:   Nome := Lowercase('_Fav_Page_');
      ntrttFavoritosTBar:   Nome := Lowercase('_Fav_TBar_');
      ntrttFavoritosBtns:   Nome := Lowercase('_Fav_Btns_');
      //
      ntrttEnt9Dig:         Nome := LowerCase('_Ent9Dig_');
      //
      ntrttCfgRel:          Nome := LowerCase('_CfgRel_');
      ntrttSdoNivCtas:      Nome := LowerCase('_SdoNivCtas_');
      ntrttCtasNiv:         Nome := LowerCase('_CtasNiv_');
      ntrttExtratoCC2:      Nome := LowerCase('_ExtratoCC2_');
      ntrttPagRec1:         Nome := LowerCase('_PagRec1_');
      ntrttPagRec2:         Nome := LowerCase('_PagRec2_');
      ntrttSdoNiveis:       Nome := LowerCase('_SdoNiveis_');
      //
      ntrttArre:            Nome := Lowercase('Arre');
      ntrttArreBaA:         Nome := Lowercase('ArreBaA');
      ntrttArreBaAUni:      Nome := Lowercase('ArreBaAUni');
      //
      ntrttPri:             Nome := Lowercase('_pri_');
      //
      ntrttBoletos:         Nome := Lowercase('_boletos_');
      ntrttBoletosC:        Nome := Lowercase('_boletos_C_');
      ntrttBoletosDR:       Nome := Lowercase('_boletos_DR_');
      ntrttBoletosDI:       Nome := Lowercase('_boletos_DI_');
      ntrttBoletosR:        Nome := Lowercase('_boletos_R_');
      ntrttBoletosSR:       Nome := Lowercase('_boletos_SR_');
      ntrttBoletosValor:    Nome := Lowercase('_boletos_Valor_');
      ntrttBoletosCarne:    Nome := LowerCase('_boletoscarne_');
      //
      ntrttCNABBcoArq:      Nome := Lowercase('CNABBcoArq');
      ntrttCNABBcoDir:      Nome := Lowercase('CNABBcoDir');
      ntrttCNABBcoReg:      Nome := Lowercase('CNABBcoReg');
      //
      ntrttCondParent:      Nome := Lowercase('CondParent');
      //
      ntrttCopiaCH1:        Nome := Lowercase('CopiaCH1');
      ntrttCopiaCH2:        Nome := Lowercase('CopiaCH2');
      //
      ntrttEmeios:          Nome := Lowercase('Emeios');
      //
      //ntrttFatDivRef:       Nome := Lowercase('FatDivRef');
      //ntrttFatDivSum:       Nome := Lowercase('FatDivSum');
      //
      ntrttLReA:            Nome := LowerCase('LReA');
      //
      ntrttMPInClasMul:     Nome := Lowercase('MPInClasMul');
      ntrttMPInSubPMul:     Nome := Lowercase('MPInSubPMul');
      //
      ntrttMU6MM:           Nome := Lowercase('_MOD_COND_MU6MM_');
      ntrttMU6MT:           Nome := Lowercase('_MOD_COND_MU6MT_');
      //
      {
      ntrttNFeArqs:         Nome := Lowercase('_NFe_Arqs_');
      ntrttNFeCP01:         Nome := Lowercase('_NFe_CP01_');
      ntrttNFeCR01:         Nome := Lowercase('_NFe_CR01_');
      ntrttNFeDP01:         Nome := Lowercase('_NFe_DP01_');
      ntrttNFeDR01:         Nome := Lowercase('_NFe_DR01_');
      ntrttNFePR01:         Nome := Lowercase('_NFe_PR01_');
      ntrttNFeXR01:         Nome := Lowercase('_NFe_XR01_');
      ntrttNFeYR01:         Nome := Lowercase('_NFe_YR01_');
      ntrttNFeZR01:         Nome := Lowercase('_NFe_ZR01_');
      ntrttNFeA:            Nome := Lowercase('_NFe_A_');
      ntrttNFeB:            Nome := Lowercase('_NFe_B_');
      ntrttNFeB12a:         Nome := Lowercase('_NFe_B_12a');
      ntrttNFeC:            Nome := Lowercase('_NFe_C_');
      ntrttNFeD:            Nome := Lowercase('_NFe_D_');
      ntrttNFeE:            Nome := Lowercase('_NFe_E_');
      ntrttNFeF:            Nome := Lowercase('_NFe_F_');
      ntrttNFeG:            Nome := Lowercase('_NFe_G_');
      ntrttNFeI:            Nome := Lowercase('_NFe_I_');
      ntrttNFeIDi:          Nome := Lowercase('_NFe_I_Di');
      ntrttNFeIDiA:         Nome := Lowercase('_NFe_I_DiA');
      ntrttNFeN:            Nome := Lowercase('_NFe_N_');
      ntrttNFeO:            Nome := Lowercase('_NFe_O_');
      ntrttNFeP:            Nome := Lowercase('_NFe_P_');
      ntrttNFeQ:            Nome := Lowercase('_NFe_Q_');
      ntrttNFeR:            Nome := Lowercase('_NFe_R_');
      ntrttNFeS:            Nome := Lowercase('_NFe_S_');
      ntrttNFeT:            Nome := Lowercase('_NFe_T_');
      ntrttNFeU:            Nome := Lowercase('_NFe_U_');
      ntrttNFeV:            Nome := Lowercase('_NFe_V_');
      ntrttNFeW02:          Nome := Lowercase('_NFe_W_02');
      ntrttNFeW17:          Nome := Lowercase('_NFe_W_17');
      ntrttNFeW23:          Nome := Lowercase('_NFe_W_23');
      ntrttNFeX01:          Nome := Lowercase('_NFe_X_01');
      ntrttNFeX22:          Nome := Lowercase('_NFe_X_22');
      ntrttNFeX26:          Nome := Lowercase('_NFe_X_26');
      ntrttNFeX33:          Nome := Lowercase('_NFe_X_33');
      ntrttNFeY01:          Nome := Lowercase('_NFe_Y_01');
      ntrttNFeY07:          Nome := Lowercase('_NFe_Y_07');
      ntrttNFeZ01:          Nome := Lowercase('_NFe_Z_01');
      ntrttNFeZ04:          Nome := Lowercase('_NFe_Z_04');
      ntrttNFeZ07:          Nome := Lowercase('_NFe_Z_07');
      ntrttNFeZ10:          Nome := Lowercase('_NFe_Z_10');
      ntrttNFeZA01:         Nome := Lowercase('_NFe_ZA_01');
      ntrttNFeZB01:         Nome := Lowercase('_NFe_ZB_01');
      //
      }
      ntrttHinMus:          Nome := Lowercase('_Hin_Mus_');
      //
      ntrttImpBMZ:          Nome := Lowercase('ImpBMZ');
      //
      ntrttMPInRat:          Nome := Lowercase('MPInRat');
      //
      ntrttImprimir1:       Nome := Lowercase('Imprimir1');
      //
      ntrttPrevBAB:         Nome := Lowercase('PrevBAB');
      //
      ntrttPrevLcts:        Nome := Lowercase('PrevLcts');
      //
      ntrttPrevVeri:        Nome := Lowercase('PrevVeri');
      //
      ntrttConsLei:         Nome := Lowercase('ConsLei');
      //
      ntrttSemQuita:        Nome := Lowercase('_sem_quita_');
      //
      ntrttGrafico:         Nome := LowerCase('_Grafico_');
      //
      ntrttWOrdSerTmp:      Nome := LowerCase('_wordsertmp_');
      {
      ntrttSintegra10:      Nome := Lowercase('Sintegra10');
      ntrttSintegra11:      Nome := Lowercase('Sintegra11');
      ntrttSintegra50:      Nome := Lowercase('Sintegra50');
      ntrttSintegra51:      Nome := Lowercase('Sintegra51');
      ntrttSintegra53:      Nome := Lowercase('Sintegra53');
      ntrttSintegra54:      Nome := Lowercase('Sintegra54');
      ntrttSintegra70:      Nome := Lowercase('Sintegra70');
      ntrttSintegra71:      Nome := Lowercase('Sintegra71');
      ntrttSintegra74:      Nome := Lowercase('Sintegra74');
      ntrttSintegra75:      Nome := Lowercase('Sintegra75');
      ntrttSintegra85:      Nome := Lowercase('Sintegra85');
      ntrttSintegra86:      Nome := Lowercase('Sintegra86');
      ntrttSintegra88:      Nome := Lowercase('Sintegra88');
      ntrttSintegra90:      Nome := Lowercase('Sintegra90');
      ntrttSintegraPM:      Nome := Lowercase('SintegraPM');
      ntrttSintegraEC:      Nome := Lowercase('SintegraEC');
      ntrttSintegraEr:      Nome := Lowercase('SintegraEr');
      //
      ntrttS50Load:         Nome := Lowercase('Load_s50');
      ntrttS54Load:         Nome := Lowercase('Load_s54');
      ntrttS70Load:         Nome := Lowercase('Load_s70');
      ntrttS75Load:         Nome := Lowercase('Load_s75');
      }
      //
      ntrttPesqESel:        Nome := Lowercase('PesqESel');
      ntrttSmiaMPIn:        Nome := Lowercase('SmiaMPIn');
      ntrttLctoEdit:        Nome := Lowercase('LctoEdit');
      //
      ntrttFatPedFis:       Nome := Lowercase('_FatPedFis_');
      //
      ntrttInfoSeq:         Nome := LowerCase('InfoSeq');
      //
      ntrttresliqfac:       Nome := LowerCase('ResLiqFac');
      //
      //ntrttStqMovValX:      Nome := Lowercase('StqMovValX');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_CodNom:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Nome                 varchar(255)                               ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  PRIMARY KEY (Codigo)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrtt_CodAnt:
    begin
      Qry.SQL.Add('  Grupo                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Nome                 varchar(255)                               ,');
      Qry.SQL.Add('  Inicial              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Codigo)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrtt_CodQtd: Cria_ntrttCodQtd(Qry);
    ntrttProtoMail: Cria_ntrttProtoMail(Qry);
    ntrttUnidCond: Cria_ntrttUnidCond(Qry);
    ntrttBloArreTmp: Cria_ntrttBloArreTmp(Qry);
    //
    ntrtt_Test000001:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Nome                 varchar(255)                               ,');
      Qry.SQL.Add('  Base                 double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  Casas                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  ValorC               double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  Gap                  double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  ValorG               double(15,6) NOT NULL DEFAULT "0.000000"   ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Codigo)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    // Geral

    ntrttConfPgtos: Cria_ntrttConfPgtos(Qry);
    ntrttIOF: Cria_ntrttIOF(Qry);
    ntrttLct: Cria_ntrttLct(Qry);
    //ntrttSaldos: Cria_ntrttSaldos(Qry);
    ntrttSelCods: Cria_ntrttSelCods(Qry);
    ntrttSimulaPg: Cria_ntrttSimulaPg(Qry);
    ntrttFavoritosPage: Cria_ntrttFavoritosPage(Qry);
    ntrttFavoritosTBar: Cria_ntrttFavoritosTBar(Qry);
    ntrttFavoritosBtns: Cria_ntrttFavoritosBtns(Qry);
    //
    ntrttInfoSeq: Cria_ntrttInfoSeq(Qry);
    //
    ntrttresliqfac: Cria_ntrttresliqfac(Qry);
    //
    ntrttEnt9Dig: Cria_ntrttEnt9Dig(Qry);
    //
    ntrttCfgRel: Cria_ntrttCfgRel(Qry);
    ntrttSdoNivCtas: Cria_ntrttSdoNivCtas(Qry);
    ntrttCtasNiv: Cria_ntrttCtasNiv(Qry);
    ntrttExtratoCC2: Cria_ntrttExtratoCC2(Qry);
    ntrttPagRec1: Cria_ntrttPagRec1(Qry);
    ntrttPagRec2: Cria_ntrttPagRec2(Qry);
    ntrttSdoNiveis: Cria_ntrttSdoNiveis(Qry);
    //
    ntrttArre:
    begin
      Qry.SQL.Add('  Cliente      int(11)                      ,');
      Qry.SQL.Add('  NO_ENT       varchar(100)                 ,');
      Qry.SQL.Add('  Valr01       double(15,2)                 ,');
      Qry.SQL.Add('  Valr02       double(15,2)                 ,');
      Qry.SQL.Add('  Valr03       double(15,2)                 ,');
      Qry.SQL.Add('  Valr04       double(15,2)                 ,');
      Qry.SQL.Add('  Valr05       double(15,2)                 ,');
      Qry.SQL.Add('  Valr06       double(15,2)                 ,');
      Qry.SQL.Add('  Valr07       double(15,2)                 ,');
      Qry.SQL.Add('  Valr08       double(15,2)                 ,');
      Qry.SQL.Add('  Valr09       double(15,2)                 ,');
      Qry.SQL.Add('  Valr10       double(15,2)                 ,');
      Qry.SQL.Add('  Valr11       double(15,2)                 ,');
      Qry.SQL.Add('  Valr12       double(15,2)                 ,');
      Qry.SQL.Add('  ValrA1       double(15,2)                 ,');
      Qry.SQL.Add('  ValrM1       double(15,2)                 ,');
      Qry.SQL.Add('  ValrA0       double(15,2)                 ,');
      Qry.SQL.Add('  ValrM0       double(15,2)                 ,');
      Qry.SQL.Add('  Ativo        tinyint(1)                    ');
      Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttArreBAA:
    begin
      Qry.SQL.Add('  Seq        integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  Conta      integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  ArreBaI    integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  ArreBaC    integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  Valor      double(15,2) DEFAULT "0.00",');
      Qry.SQL.Add('  Texto      varchar(40)  DEFAULT NULL,');
      Qry.SQL.Add('  CNAB_Cfg   integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  Fatur_Cfg  integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  NFSeSrvCad integer(11)  DEFAULT 0,');
      Qry.SQL.Add('  Adiciona   tinyint(1)   DEFAULT 0');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttArreBaAUni:
    begin
      Qry.SQL.Add('  Seq          int(11) DEFAULT 0,');
      Qry.SQL.Add('  Apto         int(11) DEFAULT 0,');
      Qry.SQL.Add('  Propriet     int(11) DEFAULT 0,');
      Qry.SQL.Add('  NaoArreSobre int(11) DEFAULT 0,');
      Qry.SQL.Add('  NaoArreRisco int(11) DEFAULT 0,');
      Qry.SQL.Add('  Unidade      varchar(10) DEFAULT "",');
      Qry.SQL.Add('  NomePropriet varchar(100) DEFAULT "",');
      Qry.SQL.Add('  ValCalc      double(15,4)  DEFAULT "0.0000",');
      Qry.SQL.Add('  Valor        double(15,2)  DEFAULT "0.00",');
      Qry.SQL.Add('  Calculo      tinyint(1)  DEFAULT 0,');
      Qry.SQL.Add('  Cotas        double(15,6)  DEFAULT "0.000000",');
      Qry.SQL.Add('  DescriCota   varchar(10)  DEFAULT "",');
      Qry.SQL.Add('  TextoCota    varchar(30)  DEFAULT "",');
      Qry.SQL.Add('  ArreBaC      int(11) DEFAULT 0,');
      Qry.SQL.Add('  ArreBaI      int(11) DEFAULT 0,');
      Qry.SQL.Add('  Adiciona     tinyint(1) DEFAULT 0');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttPri:
    begin
      Qry.SQL.Add('  Codigo       int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Controle     int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Conta        int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Valor        double(15,2) DEFAULT "0.00",');
      Qry.SQL.Add('  PrevBaC      int(11)      DEFAULT 0,');
      Qry.SQL.Add('  PrevBaI      int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Texto        varchar(50)  DEFAULT "",');
      Qry.SQL.Add('  EmitVal      double(15,2) DEFAULT "0.00",');
      Qry.SQL.Add('  EmitSit      int(11)      DEFAULT 0,');
      Qry.SQL.Add('  NomeConta    varchar(50)  DEFAULT "",');
      Qry.SQL.Add('  NomeSubGrupo varchar(50)  DEFAULT "",');
      Qry.SQL.Add('  Periodo      int(11)      DEFAULT 0,');
      Qry.SQL.Add('  CliInt       int(11)      DEFAULT 0');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttBoletos:      Cria_ntrttBoletos(Qry);
    ntrttBoletosC:     Cria_ntrttBoletosC(Qry);
    ntrttBoletosDR:    Cria_ntrttBoletosDR(Qry);
    ntrttBoletosDI:    Cria_ntrttBoletosDI(Qry);
    ntrttBoletosR:     Cria_ntrttBoletosR(Qry);
    ntrttBoletosSR:    Cria_ntrttBoletosSR(Qry);
    ntrttBoletosValor: Cria_ntrttBoletosValor(Qry);
    //
    ntrttBoletosCarne:
    begin
      Qry.SQL.Add('  Prev                 int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Cond                 int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FracaoIdeal          double(15,6) NOT NULL                      ,');
      Qry.SQL.Add('  Andar                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Boleto               double(20,0) NOT NULL                      ,');
      Qry.SQL.Add('  Apto                 int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Propriet             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Vencto               date         NOT NULL                      ,');
      Qry.SQL.Add('  Unidade              varchar(10)  NOT NULL                      ,');
      Qry.SQL.Add('  NOMEPROPRIET         varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  Periodo              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  KGT                  int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  CNAB_Cfg             int(11)      NOT NULL                       ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttCNABBcoArq:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Arquivo              varchar(255)                               ,');
      Qry.SQL.Add('  Entidade             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  NO_Entid             varchar(255)                               ,');
      Qry.SQL.Add('  CNPJ_CPF             varchar(20)                                ,');
      Qry.SQL.Add('  Cedente              varchar(30)                                ,');
      Qry.SQL.Add('  Lotes                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Itens                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Creditos             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  BcoArq               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  CNAB                 int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttCNABBcoDir:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Nome                 varchar(255)                               ,');
      Qry.SQL.Add('  CNABDirMul           varchar(255)                               ,');
      Qry.SQL.Add('  DirExiste            char(3)      NOT NULL DEFAULT "N�o"        ,');
      Qry.SQL.Add('  Arquivos             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Codigo)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttCNABBcoReg:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
      (* [00] := Seq *)
      Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Conta                int(11)      NOT NULL AUTO_INCREMENT       ,');
      Qry.SQL.Add('  CNPJ_CPF             varchar(14)  NOT NULL DEFAULT "SEM_DOC"    ,');
      Qry.SQL.Add('  EntCliente           int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  NO_EntClie           varchar(100)                               ,');
      Qry.SQL.Add('  Cedente              varchar(30)                                ,');
      Qry.SQL.Add('  CartCod              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  CartTxt              varchar(100)                               ,');
      (* [01] := Nosso n�m. *)
      Qry.SQL.Add('  NossoNum             varchar(30)                                ,');
      (* [02] := Ocorr�ncia *)
      Qry.SQL.Add('  OcorrCod             int(11)      NOT NULL DEFAULT "0"          ,');
      (* [03] := Descri��o da ocorr�ncia *)
      Qry.SQL.Add('  OcorrTxt             varchar(255)                               ,');
      (* [04] := Data ocor. *)
      Qry.SQL.Add('  OcorrDta             date NOT NULL DEFAULT "0000-00-00"         ,');
      (* [05] := Seu n�mero *)
      Qry.SQL.Add('  SeuNumer             double(20,0) NOT NULL DEFAULT "0"          ,');
      // Documento (Texto do n�mero do documento no t�tulo)
      Qry.SQL.Add('  NumDocum             varchar(60)                                ,');
      (* [06] := Val. t�tulo *)
      Qry.SQL.Add('  ValTitul             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [07] := Abatimento *)
      Qry.SQL.Add('  ValAbati             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [08] := Desconto *)
      Qry.SQL.Add('  ValDesco             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [09] := Val. receb. *)
      Qry.SQL.Add('  ValEPago             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [10] := Juros mora *)
      Qry.SQL.Add('  ValJuros             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [11] := Multa *)
      Qry.SQL.Add('  ValMulta             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [12] := Outros + *)
      Qry.SQL.Add('  ValOutrC             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [13] := Jur + Mul. *)
      Qry.SQL.Add('  ValJuMul             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [14] := Tarifa *)
      Qry.SQL.Add('  ValTarif             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [15] := ERRO *)
      Qry.SQL.Add('  ValErro              double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [16] := M.O. *)
      Qry.SQL.Add('  Motivos              varchar(30)                                ,');
      (* [17] := Motivos ocorr�ncia *)
      Qry.SQL.Add('  MotivTxt             varchar(255)                               ,');
      (* [18] := Dt.lanc.c/c *)
      Qry.SQL.Add('  PagtoDta             date NOT NULL DEFAULT "0000-00-00"         ,');
      (* [19] := Dt.D�b.tarifa *)
      Qry.SQL.Add('  DtaTarif             date NOT NULL DEFAULT "0000-00-00"         ,');
      (* [20] := Entidade *)
      Qry.SQL.Add('  Entidade             int(11)      NOT NULL DEFAULT "0"          ,');
      (* [21] := ID Link *)
      Qry.SQL.Add('  ID_Link              varchar(30)                                ,');
      (* [22] := Dir *)
      Qry.SQL.Add('  SeqDir               int(11)      NOT NULL DEFAULT "0"          ,');
      (* [23] := Arq *)
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL DEFAULT "0"          ,');
      (* [24] := Item *)
      Qry.SQL.Add('  SeqReg               int(11)      NOT NULL DEFAULT "0"          ,');
      (* [25] := Bco *)
      Qry.SQL.Add('  Banco                int(11)      NOT NULL DEFAULT "0"          ,');
      (* [26] := Documento *)
      (* [27] := Val. Bruto *)
      Qry.SQL.Add('  ValCBrut             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [28] := Outros - *)
      Qry.SQL.Add('  ValOutrD             double(15,2) NOT NULL DEFAULT "0.00"       ,');
      (* [29] := Vencto *)
      Qry.SQL.Add('  VenctDta             date NOT NULL DEFAULT "0000-00-00"         ,');
      //
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Conta)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttCondParent:
    begin
      Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL DEFAULT 0            ,');
      Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT 0            ,');
      Qry.SQL.Add('  NOUTMOR              varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  DataNasc             varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  Parentesco           varchar(100) NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttCopiaCH1:
    begin
      Qry.SQL.Add('  Banco1               mediumint(3)           DEFAULT "0"         ,');
      Qry.SQL.Add('  Conta1               varchar(15)                                ,');
      Qry.SQL.Add('  Agencia1             mediumint(4)           DEFAULT "0"         ,');
      Qry.SQL.Add('  SerieCH              varchar(20)                                ,');
      Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
      Qry.SQL.Add('  Documento            double(20,0)           DEFAULT "0"         ,');
      Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
      Qry.SQL.Add('  NOMEFORNECE          varchar(100)                               ,');
      Qry.SQL.Add('  Descricao            varchar(100)                               ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  ID_Pgto              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  NOMEBANCO            varchar(100)           DEFAULT "?"         ,');
      Qry.SQL.Add('  TipoDoc              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Tipo                 int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  ID_Copia             int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  NOMEEMPRESA          varchar(100)                               ,');
      Qry.SQL.Add('  NotaFiscal           int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Duplicata            varchar(50)                                ,');
      Qry.SQL.Add('  Genero               int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Protocolo            int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  EANTem               tinyint(1)   NOT NULL  DEFAULT "0"         ');
      //Qry.SQL.Add('  EAN128I              varchar(30)                                ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttCopiaCH2:
    begin
      Qry.SQL.Add('  Banco1               mediumint(3)           DEFAULT "0"         ,');
      Qry.SQL.Add('  Conta1               varchar(15)                                ,');
      Qry.SQL.Add('  Agencia1             mediumint(4)           DEFAULT "0"         ,');
      Qry.SQL.Add('  SerieCH              varchar(20)                                ,');
      Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
      Qry.SQL.Add('  Documento            double(20,0)           DEFAULT "0"         ,');
      Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
      Qry.SQL.Add('  NOMEFORNECE          text                                       ,');
      Qry.SQL.Add('  Descricao            text                                       ,');
      Qry.SQL.Add('  Controles            text                                       ,');
      Qry.SQL.Add('  ID_Pgto              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  NOMEBANCO            varchar(100)           DEFAULT "?"         ,');
      Qry.SQL.Add('  TipoDoc              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Tipo                 int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  ID_Copia             int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  QtdDocs              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  NOMEEMPRESA          varchar(100)                               ,');
      Qry.SQL.Add('  NotaFiscal           int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Duplicata            varchar(50)                                ,');
      Qry.SQL.Add('  Genero               int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Protocolo            int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  EANTem               tinyint(1)   NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  EAN128I              varchar(30)                                ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttEmeios:
    begin
{
        'EntiCod', 'TipoStr', 'ExtrCod',
        'Emeio', 'EntiNom', 'ExtrNom',
        'ExtrInt1', 'ExtrInt2'
      ], ['Item'], [
}
      Qry.SQL.Add('  Item                 int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  EntiCod              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  ExtrCod              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  ExtrInt1             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  ExtrInt2             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  TipoStr              varchar(30)                                ,');
      Qry.SQL.Add('  Emeio                varchar(255)                               ,');
      Qry.SQL.Add('  EntiNom              varchar(100)                               ,');
      Qry.SQL.Add('  ExtrNom              varchar(100)                               ,');
      Qry.SQL.Add('  Ativo                int(11)      NOT NULL DEFAULT "1"          ,');
      Qry.SQL.Add('  PRIMARY KEY (Item)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttHinMus:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Numero               int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Nome                 varchar(255)                               ,');
      Qry.SQL.Add('  PRIMARY KEY (Codigo)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    ntrttImpBMZ: Cria_ntrttImpBMZ(Qry);
    //
    ntrttMPInRat: Cria_ntrttMPInRat(Qry);
    //
    ntrttImprimir1: Cria_ntrttImprimir1(Qry);
    //
    ntrttLReA:
    begin
      Qry.SQL.Add('  Codigo     integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  Data       date                     ,');
      Qry.SQL.Add('  NomeCli    varchar(100) DEFAULT "?" ,');
      Qry.SQL.Add('  Lote       integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  NomeMes    varchar(30)  DEFAULT "?" ,');
      Qry.SQL.Add('  Mes        integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  NF         integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  Total      double(15,2) DEFAULT 0   ,');
      Qry.SQL.Add('  Dias       double(15,6) DEFAULT 0   ,');
      Qry.SQL.Add('  TC0        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TC1        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TC2        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TC3        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TC4        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TC5        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TC6        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV0        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV1        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV2        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV3        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV4        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV5        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  VV6        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ETC        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  EVV        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TTC        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TVV        double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TTC_T      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TTC_J      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TRC_T      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  TRC_J      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ALL_T      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ALL_J      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ddVal      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ddLiq      double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  QtdeDocs   int(11)      DEFAULT 0   ,');
      Qry.SQL.Add('  MediaDoc   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ISS_Val    double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  PIS_Val    double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  COFINS_Val double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  Impostos   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  IOF_TOTAL  double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  Tarifas    double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  PgOcorren  double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  CHDevPg    double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  DUDevPg    double(15,4) DEFAULT 0   ,');
      //
      Qry.SQL.Add('  ColCod01   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod02   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod03   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod04   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod05   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod06   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod07   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod08   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod09   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod10   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod11   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColCod12   integer(11)  DEFAULT 0   ,');
      Qry.SQL.Add('  ColNom01   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom02   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom03   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom04   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom05   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom06   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom07   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom08   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom09   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom10   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom11   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColNom12   varchar(50)  DEFAULT ""  ,');
      Qry.SQL.Add('  ColVal01   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal02   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal03   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal04   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal05   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal06   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal07   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal08   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal09   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal10   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal11   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColVal12   double(15,4) DEFAULT 0   ,');
      Qry.SQL.Add('  ColTip01   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip02   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip03   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip04   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip05   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip06   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip07   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip08   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip09   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip10   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip11   tinyint(1)   NOT NULL    ,');
      Qry.SQL.Add('  ColTip12   tinyint(1)   NOT NULL     ');
      //
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttMPInClasMul:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
      Qry.SQL.Add('  ClienteI             int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Procedencia          int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Transportadora       int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Marca                varchar(20)  NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Lote                 varchar(11)                                ,');
      Qry.SQL.Add('  Pecas                double(15,1) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  PLE                  double(15,3) NOT NULL  DEFAULT "0.000"     ,');
      Qry.SQL.Add('  PecasOut             double(15,1) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  PecasSal             double(15,1) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  Estq_Pecas           double(15,1) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  Estq_M2              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Estq_P2              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Estq_Peso            double(15,3) NOT NULL  DEFAULT "0.000"     ,');
      Qry.SQL.Add('  StqMovIts            int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  DebCtrl              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  DebStqCenCad         int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  DebGraGruX           int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  GerBxaEstq           int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Clas_Qtde            double(15,3) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  Clas_Pecas           double(15,1) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  Clas_M2              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Clas_P2              double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Clas_Peso            double(15,3) NOT NULL  DEFAULT "0.000"     ,');
      Qry.SQL.Add('  Bxa_Qtde             double(15,3) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  Bxa_Pecas            double(15,1) NOT NULL  DEFAULT "0.0"       ,');
      Qry.SQL.Add('  Bxa_M2               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Bxa_P2               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
      Qry.SQL.Add('  Bxa_Peso             double(15,3) NOT NULL  DEFAULT "0.000"     ,');
      Qry.SQL.Add('  DataHora             datetime DEFAULT "0000-00-00 00:00:00"     ,');
      Qry.SQL.Add('  Tipificacao          int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Animal               int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  PRIMARY KEY (Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttMPInSubPMul:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Conta                int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  GGX_Ori              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  GGX_Ger              int(11)      NOT NULL  DEFAULT "0"         ,');
      Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL                      ,');
      Qry.SQL.Add('  Pecas                double(15,3) NOT NULL                      ,');
      Qry.SQL.Add('  Peso                 double(15,3) NOT NULL                      ,');
      Qry.SQL.Add('  AreaM2               double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  AreaP2               double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  CustoAll             double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ValorAll             double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Conta)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttMU6MM:
    begin
      Qry.SQL.Add('  MEZ                  int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  SUM_DEB              double(19,2) NOT NULL DEFAULT "0.00"        ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttMU6MT:
    begin
      Qry.SQL.Add('  SUM_DEB              double(19,2) NOT NULL DEFAULT "0.00"        ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    {
    ntrttNFeArqs: // Lista de arquivos XML carregados
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  Versao               double(15,1) NOT NULL                      ,'); // 1.0 conforme http://www.w3.org/TR/xml/#NT-VersionNum
      Qry.SQL.Add('  Codificacao          varchar(30)  NOT NULL                      ,'); // UTF-8
      Qry.SQL.Add('  LocalName            varchar(50)  NOT NULL                      ,');
      Qry.SQL.Add('  Prefix               varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  NodeName             varchar(100) NOT NULL                      ,'); // Elemento Raiz
      Qry.SQL.Add('  NamespaceURI         varchar(255) NOT NULL                      ,'); // http://www.portalfiscal.inf.br/nfe
      Qry.SQL.Add('  ArqDir               varchar(255) NOT NULL                      ,');
      Qry.SQL.Add('  ArqName              varchar(255) NOT NULL                      ,');
      Qry.SQL.Add('  xTipoDoc             varchar(255) NOT NULL                      ,');
      Qry.SQL.Add('  Assinado             tinyint(1)   NOT NULL DEFAULT 0            ,');
     
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeCP01: // Pedido de Cancelamento de NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  cancNFe_versao       double(4,2)  NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_Id           varchar(46)  NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_tpAmb        tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  infCanc_xServ        varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_chNFe        varchar(44)  NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_nProt        bigint(15)   NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_xJust        varchar(255) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeCR01: // Resultado da Solicita��o de Cancelamento de NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  retCanc_versao       double(4,2)  NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  infCanc_Id           varchar(60)  NOT NULL                      ,'); // tamanho indefinido!
      Qry.SQL.Add('  infCanc_tpAmb        tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  infCanc_verAplic     varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_cStat        mediumint(3) NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_xMotivo      varchar(255) NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_cUF          tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  infCanc_xUF          char(2)      NOT NULL DEFAULT "??"         ,');
      Qry.SQL.Add('  infCanc_chNFe        varchar(44)                                ,');
      Qry.SQL.Add('  infCanc_dhRecbto     datetime                                   ,');
      Qry.SQL.Add('  infCanc_nProt        bigint(15)                                 ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeDP01: // Solicita��o de Inutiliza��o de Numera��o de NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  inutNFe_versao       double(4,2)  NOT NULL                      ,');
      Qry.SQL.Add('  inutNFe_Id           varchar(43)  NOT NULL                      ,');
      Qry.SQL.Add('  inutNFe_tpAmb        tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  inutNFe_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  inutNFe_xServ        varchar(10)  NOT NULL                      ,');
      Qry.SQL.Add('  inutNFe_cUF          tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  inutNFe_xUF          char(2)                                    ,');
      Qry.SQL.Add('  inutNFe_ano          tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  inutNFe_CNPJ         varchar(14)                                ,');
      Qry.SQL.Add('  inutNFe_mod          tinyint(2)   NOT NULL DEFAULT "55"         ,');
      Qry.SQL.Add('  inutNFe_serie        mediumint(3) NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  inutNFe_nNFIni       int(9)       NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  inutNFe_nNFFin       int(9)       NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  inutNFe_xJust        varchar(255) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeDR01: // Resultado da Solicita��o de Inutiliza��o de Numera��o de NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  retInutNFe_versao    double(4,2)  NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  retInutNFe_Id        varchar(17)  NOT NULL                      ,'); // tamanho indefinido!
      Qry.SQL.Add('  retInutNFe_tpAmb     tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  retInutNFe_xAmb      varchar(11)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  retInutNFe_verAplic  varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  retInutNFe_cStat     mediumint(3) NOT NULL                      ,');
      Qry.SQL.Add('  retInutNFe_xMotivo   varchar(255) NOT NULL                      ,');
      Qry.SQL.Add('  retInutNFe_cUF       tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  retInutNFe_xUF       char(2)      NOT NULL DEFAULT "??"         ,');

      Qry.SQL.Add('  retInutNFe_ano       tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  retInutNFe_CNPJ      varchar(14)                                ,');
      Qry.SQL.Add('  retInutNFe_mod       tinyint(2)   NOT NULL DEFAULT "55"         ,');
      Qry.SQL.Add('  retInutNFe_serie     mediumint(3) NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  retInutNFe_nNFIni    int(9)       NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  retInutNFe_nNFFin    int(9)       NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  retInutNFe_dhRecbto  datetime                                   ,');
      Qry.SQL.Add('  retInutNFe_nProt     bigint(15)                                 ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFePR01: // Protocolo de Recebimento da NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  protNFe_versao       double(4,2)  NOT NULL                      ,');
      Qry.SQL.Add('  infProt_Id           varchar(46)  NOT NULL                      ,');
      Qry.SQL.Add('  infProt_tpAmb        tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  infProt_xAmb         varchar(11)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  infProt_verAplic     varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  infProt_chNFe        varchar(44)                                ,');
      Qry.SQL.Add('  infProt_dhRecbto     datetime                                   ,');
      Qry.SQL.Add('  infProt_nProt        bigint(15)                                 ,');
      Qry.SQL.Add('  infProt_digVal       varchar(28)                                ,');
      Qry.SQL.Add('  infProt_cStat        mediumint(3) NOT NULL                      ,');
      Qry.SQL.Add('  infProt_xMotivo      varchar(255) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeXR01: // Leiaute da Distribui��o: NF-e (Autorizada)
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  nfeProcNFe_versao    double(4,2)  NOT NULL DEFAULT "0.00"       ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeYR01: // Leiaute de Distribui��o: Cancelamento de NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  procCancNFe_versao   double(4,2)  NOT NULL DEFAULT "0.00"       ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZR01: // Leiaute de Distribui��o: Inutiliza��o de Numera��o de NF-e
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  procInutNFe_versao   double(4,2)  NOT NULL DEFAULT "0.00"       ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeA:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  IDCtrl               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  LoteEnv              bigint(15)   NOT NULL                      ,');
      Qry.SQL.Add('  versao               double(4,2)  NOT NULL                      ,');
      Qry.SQL.Add('  Id                   varchar(44)  NOT NULL                      ,');

      Qry.SQL.Add('  ErrEmit              tinyint(1)   NOT NULL DEFAULT -1           ,');
      Qry.SQL.Add('  ErrDest              tinyint(1)   NOT NULL DEFAULT -1           ,');
      Qry.SQL.Add('  ErrTrsp              tinyint(1)   NOT NULL DEFAULT -1           ,');
      Qry.SQL.Add('  ErrProd              tinyint(1)   NOT NULL DEFAULT -1           ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeB:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  ide_cUF              tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xUF              char(2)      NOT NULL DEFAULT "??"         ,');
      Qry.SQL.Add('  ide_cNF              int(9)       NOT NULL                      ,');
      Qry.SQL.Add('  ide_natOp            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_indPag           tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xIndPag          varchar(30)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_mod              tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_serie            int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  ide_nNF              int(9)       NOT NULL                      ,');
      Qry.SQL.Add('  ide_dEmi             date         NOT NULL                      ,');
      Qry.SQL.Add('  ide_dSaiEnt          date         NOT NULL                      ,');
      Qry.SQL.Add('  ide_hSaiEnt          time         NOT NULL                      ,');
      Qry.SQL.Add('  ide_tpNF             tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xTpNF            varchar(10)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_cMunFG           int(7)       NOT NULL                      ,');
      Qry.SQL.Add('  ide_xMunFG           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_tpImp            tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xTpImp           varchar(10)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_tpEmis           tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xTpEmis          varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  ide_cDV              tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_tpAmb            tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xAmb             varchar(11)  NOT NULL DEFAULT "?"          ,');
      Qry.SQL.Add('  ide_finNFe           tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xFInNFe          varchar(30)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_procEmi          tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ide_xProcEmi         varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  ide_verProc          varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  ide_dhCont           datetime     NOT NULL                      ,');
      Qry.SQL.Add('  ide_xJust            varchar(255) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeB12a:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  refNFe               varchar(44)  NOT NULL                      ,');
      Qry.SQL.Add('  refNF_cUF            tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  refNF_xUF            char(2)      NOT NULL DEFAULT "??"         ,');
      Qry.SQL.Add('  refNF_AAMM           int(4)       NOT NULL                      ,');
      Qry.SQL.Add('  refNF_CNPJ           varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  refNF_mod            tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  refNF_serie          int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  refNF_nNF            int(9)       NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_cUF           tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_xUF           char(2)      NOT NULL DEFAULT "??"         ,');
      Qry.SQL.Add('  refNFP_AAMM          smallint(4)  NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_CNPJ          varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_CPF           varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_IE            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_mod           tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_serie         smallint(3)  NOT NULL                      ,');
      Qry.SQL.Add('  refNFP_nNF           int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  refCTe               varchar(44)  NOT NULL                      ,');
      Qry.SQL.Add('  refECF_mod           char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  refECF_nECF          smallint(3)  NOT NULL                      ,');
      Qry.SQL.Add('  refECF_nCOO          mediumint(6) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeC:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  emit_CNPJ            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_CPF             varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_xNome           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_xFant           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_xLgr            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_nro             varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_xCpl            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_xBairro         varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_cMun            int(7)       NOT NULL                      ,');
      Qry.SQL.Add('  emit_xMun            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_UF              char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  emit_CEP             int(8)       NOT NULL                      ,');
      Qry.SQL.Add('  emit_cPais           int(4)       NOT NULL                      ,');
      Qry.SQL.Add('  emit_xPais           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_fone            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_IE              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_IEST            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_IM              varchar(15)  NOT NULL                      ,');
      Qry.SQL.Add('  emit_CNAE            varchar(7)   NOT NULL                      ,');
      Qry.SQL.Add('  emit_CRT             tinyint(1)   NOT NULL                      ,');

      Qry.SQL.Add('  emit_xCNAE           varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  emit_xCRT            varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  CodInfoEmit          int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  NomeEmit             varchar(100) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeD:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  avulsa_CNPJ          varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_xOrgao        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_matr          varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_xAgente       varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_fone          varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_UF            char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_nDAR          varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_dEmi          date         NOT NULL DEFAULT "0000-00-00" ,');
      Qry.SQL.Add('  avulsa_vDAR          double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  avulsa_repEmi        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  avulsa_dPag          date         NOT NULL DEFAULT "0000-00-00" ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeE:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  dest_CNPJ            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_CPF             varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_xNome           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_xLgr            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_nro             varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_xCpl            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_xBairro         varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_cMun            int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  dest_xMun            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_UF              char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  dest_CEP             varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  dest_cPais           int(4)       NOT NULL                      ,');
      Qry.SQL.Add('  dest_xPais           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_fone            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_IE              varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  dest_ISUF            varchar(9)   NOT NULL                      ,');
      Qry.SQL.Add('  dest_email           varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  CodInfoDest          int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  NomeDest             varchar(100) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeF:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  retirada_CNPJ        varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_CPF         varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_xLgr        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_nro         varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_xCpl        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_xBairro     varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_cMun        int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  retirada_xMun        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  retirada_UF          char(2)      NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeG:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  entrega_CNPJ         varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_CPF          varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_xLgr         varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_nro          varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_xCpl         varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_xBairro      varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_cMun         int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  entrega_xMun         varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  entrega_UF           char(2)      NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeI:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  prod_cProd           varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  prod_cEAN            varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  prod_xProd           varchar(120) NOT NULL                      ,');
      Qry.SQL.Add('  prod_NCM             varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  prod_EXTIPI          char(3)      NOT NULL                      ,');
      //Qry.SQL.Add('  prod_genero          tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  prod_CFOP            int(4)       NOT NULL                      ,');
      Qry.SQL.Add('  prod_uCom            varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  prod_qCom            double(12,4) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vUnCom          double(16,10) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vProd           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  prod_cEANTrib        varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  prod_uTrib           varchar(6)   NOT NULL                      ,');
      Qry.SQL.Add('  prod_qTrib           double(12,4) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vUnTrib         double(16,10) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vFrete          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vSeg            double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vDesc           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  prod_vOutro          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  prod_indTot          tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  prod_xPed            varchar(15)  NOT NULL                      ,');
      Qry.SQL.Add('  prod_nItemPed        int(6)       NOT NULL                      ,');
(*
      Qry.SQL.Add('  Tem_IPI              tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  _Ativo_              tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  InfAdCuztm           int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  EhServico            int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  ICMSRec_pRedBC       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSRec_vBC          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSRec_pAliq        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSRec_vICMS        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  IPIRec_pRedBC        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  IPIRec_vBC           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  IPIRec_pAliq         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  IPIRec_vIPI          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PISRec_pRedBC        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PISRec_vBC           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PISRec_pAliq         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PISRec_vPIS          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSRec_pRedBC     double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSRec_vBC        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSRec_pAliq      double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSRec_vCOFINS    double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  MeuID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Lk                   int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  DataCad              date         NOT NULL                      ,');
      Qry.SQL.Add('  DataAlt              date         NOT NULL                      ,');
      Qry.SQL.Add('  UserCad              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  UserAlt              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  AlterWeb             tinyint(1)   NOT NULL                      ,');
*)


      Qry.SQL.Add('  GraGruX              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  NomeGGX              varchar(100) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeIDi:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  DI_nDI               varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  DI_dDI               date         NOT NULL                      ,');
      Qry.SQL.Add('  DI_xLocDesemb        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  DI_UFDesemb          char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  DI_dDesemb           date         NOT NULL                      ,');
      Qry.SQL.Add('  DI_cExportador       varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeIDiA:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  DI_nDI               varchar(12)  NOT NULL                      ,');
      Qry.SQL.Add('  Adi_nAdicao          int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  Adi_nSeqAdic         int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  Adi_cFabricante      varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  Adi_vDescDI          double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem, Controle, Conta)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeN:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_Orig            tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_CST             tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_modBC           tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pRedBC          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vBC             double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pICMS           double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vICMS           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_modBCST         tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pMVAST          double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pRedBCST        double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vBCST           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pICMSST         double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vICMSST         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_CSOSN           mediumint(3) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_UFST            char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pBCOp           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vBCSTRet        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vICMSSTRet      double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_motDesICMS      tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_pCredSN         double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ICMS_vCredICMSSN     double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeO:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  IPI_clEnq            varchar(5)   NOT NULL                      ,');
      Qry.SQL.Add('  IPI_CNPJProd         varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  IPI_cSelo            varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  IPI_qSelo            double(12,0) NOT NULL                      ,');
      Qry.SQL.Add('  IPI_cEnq             char(3)      NOT NULL                      ,');
      Qry.SQL.Add('  IPI_CST              tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  IPI_vBC              double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  IPI_qUnid            double(16,4) NOT NULL                      ,');
      Qry.SQL.Add('  IPI_vUnid            double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  IPI_pIPI             double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  IPI_vIPI             double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeP:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  II_vBC               double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  II_vDespAdu          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  II_vII               double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  II_vIOF              double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeQ:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  PIS_CST              tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  PIS_vBC              double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PIS_pPIS             double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  PIS_vPIS             double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PIS_qBCProd          double(16,4) NOT NULL                      ,');
      Qry.SQL.Add('  PIS_vAliqProd        double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  PIS_fatorBC          double(6,4)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeR:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  PISST_vBC            double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PISST_pPIS           double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  PISST_qBCProd        double(16,4) NOT NULL                      ,');
      Qry.SQL.Add('  PISST_vAliqProd      double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  PISST_vPIS           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  PISST_fatorBCST      double(6,4)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeS:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_CST           tinyint(2)   NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_vBC           double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_pCOFINS       double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_qBCProd       double(16,4) NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_vAliqProd     double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_vCOFINS       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINS_fatorBC       double(6,4)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeT:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  COFINSST_vBC         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSST_pCOFINS     double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  COFINSST_qBCProd     double(16,4) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSST_vAliqProd   double(15,4) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSST_vCOFINS     double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  COFINSST_fatorBCST   double(6,4)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeU:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  ISSQN_vBC            double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ISSQN_vAliq          double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  ISSQN_vISSQN         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ISSQN_cMunFG         int(7)       NOT NULL                      ,');
      Qry.SQL.Add('  ISSQN_cListServ      int(4)       NOT NULL                      ,');
      Qry.SQL.Add('  ISSQN_cSitTrib       char(1)      NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeV:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nItem                int(3)       NOT NULL                      ,');
      Qry.SQL.Add('  InfAdProd            text         NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, nItem)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeW02:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  ICMSTot_vBC          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vICMS        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vBCST        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vST          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vProd        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vFrete       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vSeg         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vDesc        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vII          double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vIPI         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vPIS         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vCOFINS      double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vOutro       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ICMSTot_vNF          double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeW17:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  ISSQNtot_vServ       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ISSQNtot_vBC         double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ISSQNtot_vISS        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ISSQNtot_vPIS        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  ISSQNtot_vCOFINS     double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeW23:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  RetTrib_vRetPIS      double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTrib_vRetCOFINS   double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTrib_vRetCSLL     double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTrib_vBCIRRF      double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTrib_vIRRF        double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTrib_vBCRetPrev   double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTrib_vRetPrev     double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeX01:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  ModFrete             tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  xModFrete            varchar(50)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_CNPJ      varchar(14)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_CPF       varchar(11)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_XNome     varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_IE        varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_XEnder    varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_XMun      varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  Transporta_UF        char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_vServ      double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_vBCRet     double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_PICMSRet   double(5,2)  NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_vICMSRet   double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_CFOP       varchar(4)   NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_CMunFG     varchar(7)   NOT NULL                      ,');
      Qry.SQL.Add('  RetTransp_xMunFG     varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  VeicTransp_Placa     varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  VeicTransp_UF        char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  VeicTransp_RNTRC     varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  Vagao                varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  Balsa                varchar(20)  NOT NULL                      ,');

      Qry.SQL.Add('  CodInfoTrsp          int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  NomeTrsp             varchar(100) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeX22:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  placa                varchar(8)   NOT NULL                      ,');
      Qry.SQL.Add('  UF                   char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  RNTRC                varchar(20)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeX26:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  qVol                 double(15,0) NOT NULL                      ,');
      Qry.SQL.Add('  esp                  varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  marca                varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  nVol                 varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  pesoL                double(15,3) NOT NULL                      ,');
      Qry.SQL.Add('  pesoB                double(15,3) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeX33:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nLacre               varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle, Conta)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeY01:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  Cobr_Fat_nFat        varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  Cobr_Fat_vOrig       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Cobr_Fat_vDesc       double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Cobr_Fat_vLiq        double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeY07:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nDup                 varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  dVenc                date         NOT NULL                      ,');
      Qry.SQL.Add('  vDup                 double(15,2) NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZ01:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  InfAdic_InfAdFisco   text         NOT NULL                      ,');
      Qry.SQL.Add('  InfAdic_InfCpl       text         NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZ04:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  xCampo               varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  xTexto               varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZ07:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  xCampo               varchar(20)  NOT NULL                      ,');
      Qry.SQL.Add('  xTexto               varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZ10:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  nProc                varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  indProc              tinyint(1)   NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe, Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZA01:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  Exporta_UFEmbarq     char(2)      NOT NULL                      ,');
      Qry.SQL.Add('  Exporta_XLocEmbarq   varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttNFeZB01:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');

      Qry.SQL.Add('  Compra_XNEmp         varchar(17)  NOT NULL                      ,');
      Qry.SQL.Add('  Compra_XPed          varchar(60)  NOT NULL                      ,');
      Qry.SQL.Add('  Compra_XCont         varchar(60)  NOT NULL                      ,');

      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
(*
    //
    ntrttNFe?:
    begin
      Qry.SQL.Add('  SeqArq               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SeqNFe               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatID                int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  FatNum               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');


      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (SeqArq, SeqNFe)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
*)
}
    ntrttPrevBAB:
    begin
      Qry.SQL.Add('  Conta                int(11)       DEFAULT 0,');
      Qry.SQL.Add('  NOMECONTA            varchar(100)  DEFAULT "",');
      Qry.SQL.Add('  PrevBaI              int(11)       DEFAULT 0,');
      Qry.SQL.Add('  PrevBaC              int(11)       DEFAULT 0,');
      Qry.SQL.Add('  Valor                double(15,2)  DEFAULT "0.00",');
      Qry.SQL.Add('  LastM                varchar(8)    DEFAULT "",');
      Qry.SQL.Add('  Last1                double(15,2)  DEFAULT "0.00",');
      Qry.SQL.Add('  Last6                double(15,2)  DEFAULT "0.00",');
      Qry.SQL.Add('  Texto                varchar(40)   DEFAULT NULL,');
      Qry.SQL.Add('  Adiciona             tinyint(1)    DEFAULT 0');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttPrevLcts:
    begin
      Qry.SQL.Add('  PrevBaI              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
      Qry.SQL.Add('  Tipo                 tinyint(4)   NOT NULL                      ,');
      Qry.SQL.Add('  Carteira             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  Genero               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL                      ,');
      Qry.SQL.Add('  Descricao            varchar(100)                               ,');
      Qry.SQL.Add('  NotaFiscal           int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  VALOR                double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Compensado           date         NOT NULL                      ,');
      Qry.SQL.Add('  Documento            double(20,0) NOT NULL                      ,');
      Qry.SQL.Add('  Vencimento           date         NOT NULL                      ,');
      Qry.SQL.Add('  Mez                  int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  TERCEIRO             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  CliInt               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Protocolo            int(11)      NOT NULL                      ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttPrevVeri:
    begin
      Qry.SQL.Add('  ProvRat              tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
      Qry.SQL.Add('  Valor                double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Vencimento           date         NOT NULL                      ,');
      Qry.SQL.Add('  Documento            double(20,0) NOT NULL                      ,');
      Qry.SQL.Add('  Compensado           date         NOT NULL                      ,');
      Qry.SQL.Add('  NotaFiscal           int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  TERCEIRO             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  DC                   varchar(10)                                ,');
      Qry.SQL.Add('  NO_TERCEIRO          varchar(100)                               ,');
      Qry.SQL.Add('  PrevBaC_Cod          int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  PrevBaC_Txt          varchar(40)                                ,');
      Qry.SQL.Add('  Descricao            varchar(100)                               ,');
      Qry.SQL.Add('  Motivo               tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  SitCobr              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Cond                 int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Texto                varchar(40)                                ,');
      Qry.SQL.Add('  Parcelas             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  ParcPerI             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  ParcPerF             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  InfoParc             int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  PrevCod              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Genero               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  NO_CONTA             varchar(50)                                ,');
      Qry.SQL.Add('  ID_Pgto              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttConsLei:
    begin
      Qry.SQL.Add('  ID_Rand              varchar(15)  DEFAULT "",');
      Qry.SQL.Add('  Propriet             int(11)      DEFAULT 0,');
      Qry.SQL.Add('  OrdBlc               int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Andar                int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Apto_ID              int(11)      DEFAULT 0,');
      Qry.SQL.Add('  Apto_Un              varchar(10)  DEFAULT "?",');
      Qry.SQL.Add('  MedAnt               double(15,6) DEFAULT "0.000000",');
      Qry.SQL.Add('  MedAtu               double(15,6) DEFAULT "0.000000",');
      Qry.SQL.Add('  Carencia             double(15,6) DEFAULT "0.000000",');
      Qry.SQL.Add('  QtdFator             double(15,6) DEFAULT "0.000000",');
      Qry.SQL.Add('  ValUnita             double(15,6) DEFAULT "0.000000",');
      Qry.SQL.Add('  NaoImpLei            tinyint(1)   DEFAULT 0,');
      Qry.SQL.Add('  DifCaren             tinyint(1)   DEFAULT 0,');
      Qry.SQL.Add('  Adiciona             tinyint(1)   DEFAULT 0,');
      Qry.SQL.Add('  PRIMARY KEY (ID_Rand, Apto_ID)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttSemQuita:
    begin
      Qry.SQL.Add('  Carteira             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Data                 date         NOT NULL DEFAULT "0000-00-00" ,');
      Qry.SQL.Add('  Descricao            varchar(255)                               ,');
      Qry.SQL.Add('  Credito              double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  Debito               double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  PRIMARY KEY (Controle)');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttGrafico:
    begin
      Qry.SQL.Add('  Linha                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  TextoInt             int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Texto                varchar(50)                                ,');
      Qry.SQL.Add('  Val01                double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  Val02                double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  Val03                double(15,2) NOT NULL DEFAULT "0.00"       ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttWOrdSerTmp:
    begin
      Qry.SQL.Add('  WebUser              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Aplic                int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Versao               varchar(20)                                ,');
      Qry.SQL.Add('  Assunto              int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Prioridade           int(11)      NOT NULL DEFAULT "0"          ,');
      Qry.SQL.Add('  Janela               varchar(13)                                ,');
      Qry.SQL.Add('  JanelaRel            varchar(13)                                ,');
      Qry.SQL.Add('  CompoRel             varchar(13)                                ,');
      Qry.SQL.Add('  Sistema              varchar(100)                               ,');
      Qry.SQL.Add('  Impacto              varchar(100)                               ,');
      Qry.SQL.Add('  Descri               text                                        ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
      Qry.ExecSQL;
    end;
    //
    ntrttPesqESel: Cria_ntrttPesqESel(Qry);
    //
    ntrttSmiaMPIn: Cria_ntrttSmiaMPIn(Qry);
    //
    ntrttLctoEdit: Cria_ntrttLctoEdit(Qry);
    //
    ntrttFatPedFis: Cria_ntrttFatPedFis(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

function TUCreate.RecriaListaSimplesSelMany(DB: TmySQLDatabase; TabToCopy:
  String): String;
var
  Tab: String;
begin
  Tab := '_' + Lowercase(TabToCopy) + '_';
  DB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS ' + Tab + '; ',
  'CREATE TABLE ' + Tab,
  'SELECT Codigo, Nome, Ativo ',
  'FROM ' + TMeuDB + '.' + Lowercase(TabToCopy) + '; ',
  ' ',
  '']));
  Result := Tab;
end;

function TUCreate.RecriaTempTable(Tabela: String; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  Result := '';
  TabNo := '';
  if NomeTab = '' then
    Nome := Lowercase(Tabela)
  else
    Nome := Lowercase(NomeTab);
  //
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  Nome := Lowercase(Tabela);
  Result := Nome;
  // Cadastro gen�rico
  if Nome = Lowercase('Cad_0') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(255)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('AgeLct') then
  begin
    Qry.SQL.Add('  Controle   int          ,');
    Qry.SQL.Add('  SerieDoc   varchar(30)  ,');
    Qry.SQL.Add('  Descricao  varchar(100) ,');
    Qry.SQL.Add('  NomeCliFor varchar(100) ,');
    Qry.SQL.Add('  Debito     double(15,2) ,');
    Qry.SQL.Add('  NF         int           ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('BloArreTmp') then
  begin
    Qry.SQL.Add('  Controle     integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Seq          integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Periodo      integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Conta        integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  BloArre      integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Blo'+TAB_ARI+'   integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  CartEmiss    integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  CNAB_Cfg     integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Valor        double(15,2) DEFAULT "0.00",');
    Qry.SQL.Add('  Texto        varchar(40)  DEFAULT NULL,');
    Qry.SQL.Add('  DiaVencto    integer(2)   DEFAULT 0,');
    Qry.SQL.Add('  Adiciona     tinyint(1)   DEFAULT 0,');
    Qry.SQL.Add('  Entidade     int(11)      DEFAULT 0,');
    Qry.SQL.Add('  IDArre       int(11)      DEFAULT 0,');
    Qry.SQL.Add('  NomeEnti     varchar(100) DEFAULT ""');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  // Carn�
  if Nome = Lowercase('Carne0') then
  begin
    Qry.SQL.Add('  Pagina               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Banda                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Carne                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Folha                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencto               date                   DEFAULT "0000-00-00",');
    Qry.SQL.Add('  SUB_TOT              double(15,2) NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Boleto               double(15,0) NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Unidade              varchar(10)  NOT NULL  DEFAULT "???"       ,');
    Qry.SQL.Add('  E_ALL                varchar(255)                               ,');
    Qry.SQL.Add('  PROPRI_E_MORADOR     varchar(255)                               ,');
    Qry.SQL.Add('  PWD_WEB              varchar(255)                               ,');
    Qry.SQL.Add('  CODIGOBARRAS         varchar(60)                                ,');
    Qry.SQL.Add('  NossoNumero          varchar(60) NOT NULL  DEFAULT "0"          ,');
    Qry.SQL.Add('  LinhaDigitavel       varchar(60) NOT NULL  DEFAULT "0"          ,');
    Qry.SQL.Add('  Imprimir             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  PRIMARY KEY (Pagina,Banda)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('cfgrel') then
  begin
    Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  RelTitu              varchar(100) NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Ordem                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "1"         ,');
    Qry.SQL.Add('  PRIMARY KEY (Controle)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Chamada1') then begin
    Qry.SQL.Add('  NOMEPROFESSOR  varchar(100) DEFAULT "???", ');
    Qry.SQL.Add('  T_I  int(11) NOT NULL Default "0", ');
    Qry.SQL.Add('  D01  date, ');
    Qry.SQL.Add('  D02  date, ');
    Qry.SQL.Add('  D03  date, ');
    Qry.SQL.Add('  D04  date, ');
    Qry.SQL.Add('  D05  date, ');
    Qry.SQL.Add('  D06  date, ');
    Qry.SQL.Add('  D07  date, ');
    Qry.SQL.Add('  D08  date, ');
    Qry.SQL.Add('  D09  date, ');
    Qry.SQL.Add('  D10  date, ');
    Qry.SQL.Add('  D11  date, ');
    Qry.SQL.Add('  D12  date, ');
    Qry.SQL.Add('  D13  date, ');
    Qry.SQL.Add('  D14  date, ');
    Qry.SQL.Add('  D15  date, ');
    Qry.SQL.Add('  D16  date, ');
    Qry.SQL.Add('  D17  date, ');
    Qry.SQL.Add('  D18  date, ');
    Qry.SQL.Add('  D19  date, ');
    Qry.SQL.Add('  D20  date, ');
    Qry.SQL.Add('  D21  date, ');
    Qry.SQL.Add('  D22  date, ');
    Qry.SQL.Add('  D23  date, ');
    Qry.SQL.Add('  D24  date, ');
    Qry.SQL.Add('  D25  date, ');
    Qry.SQL.Add('  D26  date, ');
    Qry.SQL.Add('  D27  date, ');
    Qry.SQL.Add('  D28  date, ');
    Qry.SQL.Add('  D29  date, ');
    Qry.SQL.Add('  D30  date, ');
    Qry.SQL.Add('  D31  date, ');
    Qry.SQL.Add('  W01  varchar(8), ');
    Qry.SQL.Add('  W02  varchar(8), ');
    Qry.SQL.Add('  W03  varchar(8), ');
    Qry.SQL.Add('  W04  varchar(8), ');
    Qry.SQL.Add('  W05  varchar(8), ');
    Qry.SQL.Add('  W06  varchar(8), ');
    Qry.SQL.Add('  W07  varchar(8), ');
    Qry.SQL.Add('  H01  varchar(50), ');
    Qry.SQL.Add('  H02  varchar(50), ');
    Qry.SQL.Add('  H03  varchar(50), ');
    Qry.SQL.Add('  H04  varchar(50), ');
    Qry.SQL.Add('  H05  varchar(50), ');
    Qry.SQL.Add('  H06  varchar(50), ');
    Qry.SQL.Add('  H07  varchar(50), ');
    Qry.SQL.Add('  Ref  varchar(50), ');
    Qry.SQL.Add('  Tur  varchar(50), ');
    Qry.SQL.Add('  PRIMARY KEY (T_I)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Chamada2') then begin
    Qry.SQL.Add('  T_I  int(11) NOT NULL default "0", ');
    Qry.SQL.Add('  Alu  int(11) NOT NULL default "0", ');
    Qry.SQL.Add('  Nom  varchar(100), ');
    Qry.SQL.Add('  Tel  varchar(40), ');
    Qry.SQL.Add('  Nas  varchar(10), ');
    Qry.SQL.Add('  Ani  varchar(30), ');
    Qry.SQL.Add('  Ren  varchar(30), ');
    Qry.SQL.Add('  S01  char(1), ');
    Qry.SQL.Add('  S02  char(1), ');
    Qry.SQL.Add('  S03  char(1), ');
    Qry.SQL.Add('  S04  char(1), ');
    Qry.SQL.Add('  S05  char(1), ');
    Qry.SQL.Add('  S06  char(1), ');
    Qry.SQL.Add('  S07  char(1), ');
    Qry.SQL.Add('  S08  char(1), ');
    Qry.SQL.Add('  S09  char(1), ');
    Qry.SQL.Add('  S10  char(1), ');
    Qry.SQL.Add('  S11  char(1), ');
    Qry.SQL.Add('  S12  char(1), ');
    Qry.SQL.Add('  S13  char(1), ');
    Qry.SQL.Add('  S14  char(1), ');
    Qry.SQL.Add('  S15  char(1), ');
    Qry.SQL.Add('  S16  char(1), ');
    Qry.SQL.Add('  S17  char(1), ');
    Qry.SQL.Add('  S18  char(1), ');
    Qry.SQL.Add('  S19  char(1), ');
    Qry.SQL.Add('  S20  char(1), ');
    Qry.SQL.Add('  S21  char(1), ');
    Qry.SQL.Add('  S22  char(1), ');
    Qry.SQL.Add('  S23  char(1), ');
    Qry.SQL.Add('  S24  char(1), ');
    Qry.SQL.Add('  S25  char(1), ');
    Qry.SQL.Add('  S26  char(1), ');
    Qry.SQL.Add('  S27  char(1), ');
    Qry.SQL.Add('  S28  char(1), ');
    Qry.SQL.Add('  S29  char(1), ');
    Qry.SQL.Add('  S30  char(1), ');
    Qry.SQL.Add('  S31  char(1), ');
    Qry.SQL.Add('  PRIMARY KEY (T_I, Nom, Alu)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ChequesImp') then
  begin
    Qry.SQL.Add('  Controle   int(11),');
    Qry.SQL.Add('  Sub        int(11),');
    Qry.SQL.Add('  ChNumero   double(20,0),');
    Qry.SQL.Add('  ChSerie    varchar(10) ,');
    Qry.SQL.Add('  ChGrava    tinyint(1)  ,');
    Qry.SQL.Add('  Valor      varchar(255),');
    Qry.SQL.Add('  Extenso1   varchar(255),');
    Qry.SQL.Add('  Extenso2   varchar(255),');
    Qry.SQL.Add('  Favorecido varchar(255),');
    Qry.SQL.Add('  Cidade     varchar(255),');
    Qry.SQL.Add('  Dia        varchar(255),');
    Qry.SQL.Add('  Mes        varchar(255),');
    Qry.SQL.Add('  Ano        varchar(255),');
    Qry.SQL.Add('  BomPara    varchar(255),');
    Qry.SQL.Add('  Observacao varchar(255),');
    Qry.SQL.Add('  Seq        int(11),');
    Qry.SQL.Add('  Codigo     int(11),');
    Qry.SQL.Add('  CPI        int(11),');
    Qry.SQL.Add('  TopoIni    int(11),');
    Qry.SQL.Add('  Altura     int(11),');
    Qry.SQL.Add('  MEsq       int(11)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ChequesLct') then
  begin
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL              ,');
    Qry.SQL.Add('  Data                 date         NOT NULL              ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL              ,');
    Qry.SQL.Add('  Genero               int(11)      NOT NULL              ,');
    Qry.SQL.Add('  NO_CONTA             varchar(50)  NOT NULL              ,');
    Qry.SQL.Add('  Descricao            varchar(100) NOT NULL              ,');
    Qry.SQL.Add('  Debito               double(15,2) NOT NULL              ,');
    Qry.SQL.Add('  Vencimento           date         NOT NULL              ,');
    Qry.SQL.Add('  Fornecedor           int(11)      NOT NULL              ,');
    Qry.SQL.Add('  NO_ENT               varchar(100) NOT NULL              ,');
    Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL              ,');
    Qry.SQL.Add('  Documento            double(15,0) NOT NULL              ,');
    Qry.SQL.Add('  SerieCH              varchar(10)  NOT NULL              ,');
    Qry.SQL.Add('  Agrupar              int(11)      NOT NULL               ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('CNAB_Rem') then
  begin
    Qry.SQL.Add('  Item      int(11)    AUTO_INCREMENT, ');
    Qry.SQL.Add('  Linha     int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  Codigo    int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  TipoDado  tinyint(3) NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  DadoI     int(11)             DEFAULT NULL, ');
    Qry.SQL.Add('  DadoF     double              DEFAULT NULL, ');
    Qry.SQL.Add('  DadoD     date                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoH     time                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoT     varchar(255)        DEFAULT "", ');
    Qry.SQL.Add('  PRIMARY KEY (Item)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Concilia') then
  begin
    Qry.SQL.Add('  Linha    int(11) DEFAULT 0,');
    Qry.SQL.Add('  Nivel    int(11) DEFAULT 0,');
    Qry.SQL.Add('  LinIt    int(11) DEFAULT 0,');
    Qry.SQL.Add('  DataM    date DEFAULT "0000-00-00",');
    Qry.SQL.Add('  Texto    varchar(50) DEFAULT "?",');
    Qry.SQL.Add('  Docum    varchar(20) DEFAULT "?",');
    Qry.SQL.Add('  Valor    double(15,2)  DEFAULT "0.00",');
    Qry.SQL.Add('  Saldo    double(15,2)  DEFAULT "0.00",');
    Qry.SQL.Add('  Acao     int(11) DEFAULT 0, ');
    Qry.SQL.Add('  Conta    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  Clien    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  Forne    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  Depto    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  Perio    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  CtrlE    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  CartT    int(11) DEFAULT -1000, ');
    Qry.SQL.Add('  CtaLk    int(11) DEFAULT 0, ');
    Qry.SQL.Add('  DifEr    double(15,2)  DEFAULT "0.00",');
    Qry.SQL.Add('  NOMEDEPTO varchar(100),');
    Qry.SQL.Add('  NOMECONTA varchar(100),');
    Qry.SQL.Add('  NOMETERCE varchar(100),');
    Qry.SQL.Add('  NOMECARTE varchar(100),');
    Qry.SQL.Add('  TIPOCARTE int(11),');
    Qry.SQL.Add('  PRIMARY KEY (Linha,Nivel,LinIt)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('condimov0') then
  begin
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Propriet             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Andar                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Unidade              varchar(10)  NOT NULL  DEFAULT "?"         ,');
    Qry.SQL.Add('  Status               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  SitImv               int(11)      NOT NULL  DEFAULT "1"         ,');
    Qry.SQL.Add('  Usuario              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "1"         ,');
    Qry.SQL.Add('  Protocolo            int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Moradores            int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  FracaoIdeal          double(15,6) NOT NULL  DEFAULT "1.000000"  ,');
    Qry.SQL.Add('  ModelBloq            tinyint(2)   NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  ConfigBol            int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  BloqEndTip           tinyint(1)   NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  BloqEndEnt           int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  EnderLin1            varchar(100)                               ,');
    Qry.SQL.Add('  EnderLin2            varchar(100)                               ,');
    Qry.SQL.Add('  EnderNome            varchar(100)                               ,');
    Qry.SQL.Add('  Procurador           int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Ordem                int(11)      NOT NULL  DEFAULT "0"         ,');
    //
    Qry.SQL.Add('  PRIMARY KEY (Conta)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Cons') then
  begin
    Qry.SQL.Add('  Codigo   integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Nome     varchar(50)  DEFAULT NULL,');
    Qry.SQL.Add('  Preco    double(15,4) DEFAULT "0.0000",');
    Qry.SQL.Add('  Casas    tinyint(1)   DEFAULT 0,');
    Qry.SQL.Add('  UnidImp  varchar(5)   DEFAULT NULL,');
    Qry.SQL.Add('  UnidLei  varchar(10)  DEFAULT NULL,');
    Qry.SQL.Add('  UnidFat  double(15,6) DEFAULT "0.000000",');
    Qry.SQL.Add('  Ativo    tinyint(1)   DEFAULT 0');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
(*
  // parei aqui
  // tirar quando syndic 1 for abolido
  if Nome = Lowercase('ConfPgtos') then
  begin
    Qry.SQL.Add('  Codigo     integer      ,');
    Qry.SQL.Add('  NomeCta    varchar(100) ,');
    Qry.SQL.Add('  QtdeMin    integer      ,');
    Qry.SQL.Add('  QtdeMax    integer      ,');
    Qry.SQL.Add('  QtdeExe    integer      ,');
    Qry.SQL.Add('  ValrMin    Float        ,');
    Qry.SQL.Add('  ValrMax    Float        ,');
    Qry.SQL.Add('  ValrExe    float        ,');
    Qry.SQL.Add('  Status     integer       ');
    //
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
*)
  if Nome = Lowercase('ConsLei') then
  begin
    Qry.SQL.Add('  ID_Rand    varchar(15)   DEFAULT "",');
    Qry.SQL.Add('  Propriet   int(11)       DEFAULT 0,');
    Qry.SQL.Add('  OrdBlc     int(11)       DEFAULT 0,');
    Qry.SQL.Add('  Andar      int(11)       DEFAULT 0,');
    Qry.SQL.Add('  Apto_ID    int(11)       DEFAULT 0,');
    Qry.SQL.Add('  Apto_Un    varchar(10)   DEFAULT "?",');
    Qry.SQL.Add('  MedAnt     double(15,6)  DEFAULT "0.000000",');
    Qry.SQL.Add('  MedAtu     double(15,6)  DEFAULT "0.000000",');
    Qry.SQL.Add('  Carencia   double(15,6)  DEFAULT "0.000000",');
    Qry.SQL.Add('  QtdFator   double(15,6)  DEFAULT "0.000000",');
    Qry.SQL.Add('  ValUnita   double(15,6)  DEFAULT "0.000000",');
    Qry.SQL.Add('  NaoImpLei  tinyint(1)    DEFAULT 0,');
    Qry.SQL.Add('  DifCaren   tinyint(1)    DEFAULT 0,');
    Qry.SQL.Add('  Adiciona   tinyint(1)    DEFAULT 0,');
    Qry.SQL.Add('  PRIMARY KEY (ID_Rand, Apto_ID)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
(*
  if Nome = Lowercase('ConsMes') then
  begin
    Qry.SQL.Add('  Codigo       int(11)  DEFAULT 0,');
    Qry.SQL.Add('  Nome         varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00"');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
*)
  if Nome = Lowercase('CopDocCtrl') then
  begin
    Qry.SQL.Add('  Controle     int(11)      NOT NULL DEFAULT "0"            ,');
    Qry.SQL.Add('  Data         date         NOT NULL DEFAULT "0000-00-00"   ,');
    Qry.SQL.Add('  Tipo         tinyint(1)   NOT NULL DEFAULT "0"            ,');
    Qry.SQL.Add('  Carteira     int(11)      NOT NULL DEFAULT "0"            ,');
    Qry.SQL.Add('  Documento    int(11)      NOT NULL DEFAULT "0"            ,');
    Qry.SQL.Add('  SerieCH      varchar(10)  NOT NULL DEFAULT NULL           ,');
    Qry.SQL.Add('  TipoDoc      int(11)      NOT NULL DEFAULT "0"            ,');
    Qry.SQL.Add('  ID_Difer     int(11)      NOT NULL DEFAULT "0"            ,');
    Qry.SQL.Add('  Ativo        tinyint(1)   NOT NULL DEFAULT "0"             ');
    //Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira,Documento,SerieCH)         ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('CtasItsCtas') then
  begin
    Qry.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Nome  varchar(255) ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('CtasNiv') then begin
    Qry.SQL.Add('  NOMENIVEL            varchar(20)                                ,');
    Qry.SQL.Add('  NOMEGENERO           varchar(50)                                ,');
    Qry.SQL.Add('  Nivel                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Genero               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "0"          ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
(*
  if Nome = Lowercase('CtasResMes') then
  begin
    Qry.SQL.Add('  SeqImp               int(11)                DEFAULT 1           ,');
    Qry.SQL.Add('  Conta                int(11)                DEFAULT 0           ,');
    Qry.SQL.Add('  Nome                 varchar(100)                               ,');
    Qry.SQL.Add('  Periodo              int(11)                DEFAULT 0           ,');
    Qry.SQL.Add('  Tipo                 int(11)                DEFAULT 0           ,');
    Qry.SQL.Add('  Fator                double(15,6)           DEFAULT 0.000000    ,');
    Qry.SQL.Add('  ValFator             double(15,6)           DEFAULT 0.00        ,');
    Qry.SQL.Add('  Devido               double(15,2)           DEFAULT 0.00        ,');
    Qry.SQL.Add('  Pago                 double(15,2)           DEFAULT 0.00        ,');
    Qry.SQL.Add('  Diferenca            double(15,2)           DEFAULT 0.00        ,');
    Qry.SQL.Add('  Acumulado            double(15,2)           DEFAULT 0.00         ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
*)
  if Nome = Lowercase('Enderecos') then
  begin
    Qry.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Linha1   varchar(255) DEFAULT NULL,');
    Qry.SQL.Add('  Linha2   varchar(255) DEFAULT NULL,');
    Qry.SQL.Add('  Linha3   varchar(255) DEFAULT NULL,');
    Qry.SQL.Add('  Linha4   varchar(255) DEFAULT NULL,');
    Qry.SQL.Add('  Linha5   varchar(255) DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('EntiLoad03') then
  begin
    Qry.SQL.Add('  Codigo   integer(11)  DEFAULT 0,');    // 01
    Qry.SQL.Add('  _A_      varchar(5)   DEFAULT NULL,'); // 02
    Qry.SQL.Add('  Nome     varchar(100) DEFAULT NULL,'); // 03
    Qry.SQL.Add('  Fantasia varchar(50)  DEFAULT NULL,'); // 04
    Qry.SQL.Add('  _B_      varchar(5)   DEFAULT NULL,'); // 05
    Qry.SQL.Add('  _C_      varchar(5)   DEFAULT NULL,'); // 06
    Qry.SQL.Add('  CNPJ     varchar(20)  DEFAULT NULL,'); // 07
    Qry.SQL.Add('  _D_      varchar(5)   DEFAULT NULL,'); // 08
    Qry.SQL.Add('  Endereco varchar(100) DEFAULT NULL,'); // 09
    Qry.SQL.Add('  Contato  varchar(30)  DEFAULT NULL,'); // 10
    Qry.SQL.Add('  ContaCtb varchar(20)  DEFAULT NULL,'); // 11
    Qry.SQL.Add('  CPF      varchar(18)  DEFAULT NULL,'); // 12
    Qry.SQL.Add('  _E_      varchar(5)   DEFAULT NULL,'); // 13
    Qry.SQL.Add('  Bairro   varchar(30)  DEFAULT NULL,'); // 14
    Qry.SQL.Add('  Fax      varchar(20)  DEFAULT NULL,'); // 15
    Qry.SQL.Add('  Telex    varchar(20)  DEFAULT NULL,'); // 16
    Qry.SQL.Add('  IM       varchar(20)  DEFAULT NULL,'); // 17
    Qry.SQL.Add('  CEP      varchar(12)  DEFAULT NULL,'); // 18
    Qry.SQL.Add('  _F_      varchar(5)   DEFAULT NULL,'); // 19
    Qry.SQL.Add('  CidUF    varchar(50)  DEFAULT NULL,'); // 20
    Qry.SQL.Add('  Tel1     varchar(20)  DEFAULT NULL,'); // 21
    Qry.SQL.Add('  _G_      varchar(5)   DEFAULT NULL,'); // 22
    Qry.SQL.Add('  Tel2     varchar(20)  DEFAULT NULL,'); // 23
    Qry.SQL.Add('  _H_      varchar(5)   DEFAULT NULL,'); // 24
    Qry.SQL.Add('  _I_      varchar(5)   DEFAULT NULL,'); // 25
    Qry.SQL.Add('  _J_      varchar(5)   DEFAULT NULL,'); // 26
    Qry.SQL.Add('  Categor  varchar(11)  DEFAULT NULL,');  // 27
    Qry.SQL.Add('  nLogr    int(11)      DEFAULT NULL,');  // Meu
    Qry.SQL.Add('  xLogr    varchar(20)  DEFAULT NULL,');  // Meu
    Qry.SQL.Add('  Rua      varchar(30)  DEFAULT NULL,');  // Meu
    Qry.SQL.Add('  Numero   int(11)      DEFAULT NULL,');  // Meu
    Qry.SQL.Add('  Compl    varchar(30)  DEFAULT NULL,');  // Meu
    Qry.SQL.Add('  Cidade   varchar(30)  DEFAULT NULL,');  // Meu
    Qry.SQL.Add('  UF       int(11)      DEFAULT NULL');  // Meu
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('EstqPto') then
  begin
    Qry.SQL.Add('  CodUsu   integer(11)  NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Grade    integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  GraGruX  integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Cor      integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Tam      integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  No_Grade varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  No_Cor   varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  No_Tam   varchar(10)  DEFAULT NULL,');
    Qry.SQL.Add('  QtdeOld  integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  QtdeNew  integer(11)  DEFAULT 0');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ExtratoCC') then
  begin
    Qry.SQL.Add('  Linha        int(11) AUTO_INCREMENT        ,');
    Qry.SQL.Add('  DataM        date                          ,');
    Qry.SQL.Add('  Texto        varchar(255)                  ,');
    Qry.SQL.Add('  Docum        varchar(30)                   ,');
    Qry.SQL.Add('  NotaF        int(11)                       ,');
    Qry.SQL.Add('  Credi        double(15,2)                  ,');
    Qry.SQL.Add('  Debit        double(15,2)                  ,');
    Qry.SQL.Add('  Saldo        double(15,2)                  ,');
    Qry.SQL.Add('  CartO        int(11)                       ,');
    Qry.SQL.Add('  CartC        int(11)                       ,');
    Qry.SQL.Add('  CartN        varchar(100)                  ,');
    Qry.SQL.Add('  SdIni        int(11)                       ,');
    Qry.SQL.Add('  TipoI        int(11)                       ,');
    Qry.SQL.Add('  Unida        varchar(20)                   ,');
    Qry.SQL.Add('  CTipN        varchar(30)                   ,');
    Qry.SQL.Add('  DebCr        char(1)                       ,');
    Qry.SQL.Add('  PRIMARY KEY (Linha)                         ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ExtratoCC2') then
  begin
    Qry.SQL.Add('  DataE      date             , '); // Emissao
    Qry.SQL.Add('  DataV      date             , '); // Vencto
    Qry.SQL.Add('  DataQ      date             , '); // Quita��o
    Qry.SQL.Add('  DataX      date             , '); // Data considerada
    Qry.SQL.Add('  Texto      varchar(255)     , ');
    Qry.SQL.Add('  Docum      varchar(30)      , ');
    Qry.SQL.Add('  NotaF      varchar(30)      , ');
    Qry.SQL.Add('  Credi      double(15,2)     , ');
    Qry.SQL.Add('  Debit      double(15,2)     , ');
    Qry.SQL.Add('  SdoCr      double(15,2)     , '); // Saldo a receber
    Qry.SQL.Add('  SdoDb      double(15,2)     , '); // Saldo a Pagar
    Qry.SQL.Add('  Saldo      double(15,2)     , ');
    Qry.SQL.Add('  Ctrle      int(11)          , ');
    Qry.SQL.Add('  CtSub      int(11)          , ');
    Qry.SQL.Add('  ID_Pg      int(11)          , ');
    Qry.SQL.Add('  CartC      int(11)          , ');
    Qry.SQL.Add('  CartN      varchar(100)     , ');
    Qry.SQL.Add('  TipoI      int(11)          , ');
    Qry.SQL.Add('  DataCad    date             , ');
    Qry.SQL.Add('  UserCad    int(11)          , ');
    Qry.SQL.Add('  DataAlt    date             , ');
    Qry.SQL.Add('  UserAlt    int(11)          , ');
    Qry.SQL.Add('  Codig      int(11)          , ');
    Qry.SQL.Add('  tCrt       int(11)          , ');
    Qry.SQL.Add('  Sit        int(11)            ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Lowercase(Tabela) = Lowercase('Fiadores') then begin
    Qry.SQL.Add('  Codigo     int(11)     , ');
    Qry.SQL.Add('  Tipo       int(11)       ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
{ Alterado e movido para Grade_Create
  if Nome = Lowercase('GGX_SCC_Stq') then
  begin
    Qry.SQL.Add('  Nivel1               int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  NO_PRD               varchar(511)            DEFAULT "? ? ?"     ,');
    Qry.SQL.Add('  PrdGrupTip           int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  UnidMed              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  NO_PGT               varchar(30)            DEFAULT "? ? ?"     ,');
    Qry.SQL.Add('  SIGLA                varchar(6)             DEFAULT "??"        ,');
    Qry.SQL.Add('  NO_TAM               varchar(5)             DEFAULT "??"        ,');
    Qry.SQL.Add('  NO_COR               varchar(30)            DEFAULT "? ? ?"     ,');
    Qry.SQL.Add('  GraCorCad            int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  GraGruC              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  GraGru1              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  GraTamI              int(11)                DEFAULT "0"         ,');

    Qry.SQL.Add('  QTDE                 double(15,3)                               ,');
    Qry.SQL.Add('  PECAS                double(15,3)                               ,');
    Qry.SQL.Add('  PESO                 double(15,3)                               ,');
    Qry.SQL.Add('  AREAM2               double(15,2)                               ,');
    Qry.SQL.Add('  AREAP2               double(15,2)                               ,');
    Qry.SQL.Add('  PrcCusUni            double(15,4)                               ,');
    Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  StqCenCad            int(11)      NOT NULL  DEFAULT "0"         ,');
    //Qry.SQL.Add('  Difere               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  NCM                  varchar(10)                                ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
}
  if Nome = Lowercase('Horarios0') then begin
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  DSem                 int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NOMEDSEM             varchar(13)  NOT NULL                      ,');
    Qry.SQL.Add('  HIni                 time         NOT NULL                      ,');
    Qry.SQL.Add('  HFim                 time         NOT NULL                      ,');
    Qry.SQL.Add('  Nome                 varchar(50)  NOT NULL                      ,');
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Horarios1') then
  begin
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Item                 int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  SubItem              int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TURMA                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TURMAITS             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Curso                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NOMECURSO            varchar(50)  NOT NULL                      ,');
    Qry.SQL.Add('  REFTURMAITS          varchar(30)  NOT NULL                      ,');
    Qry.SQL.Add('  NOMETURMA            varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  IDTURMA              varchar(30)  NOT NULL                      ,');
    Qry.SQL.Add('  Periodo              int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nome                 varchar(50)  NOT NULL                      ,');
    Qry.SQL.Add('  DSem                 int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  HIni                 time         NOT NULL                      ,');
    Qry.SQL.Add('  HFim                 time         NOT NULL                      ,');
    Qry.SQL.Add('  NOMEDSEM             varchar(7)   NOT NULL                      ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ImportSal1') then
  begin
    Qry.SQL.Add('  Entidade   int(11)       DEFAULT NULL,');
    Qry.SQL.Add('  Valor      double(15,2)  DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Ind01') then
  begin
    Qry.SQL.Add('  DEBITO       double(15,2) ,');
    Qry.SQL.Add('  CREDITO      double(15,2) ,');
    Qry.SQL.Add('  NOMECONTA    varchar(100) ,');
    Qry.SQL.Add('  NOMECONTA2   varchar(100) ,');
    Qry.SQL.Add('  NOMESUBGRUPO varchar(100) ,');
    Qry.SQL.Add('  NOMEGRUPO    varchar(100) ,');
    Qry.SQL.Add('  NOMECONJUNTO varchar(100) ,');
    Qry.SQL.Add('  NOMECONTA3   varchar(100) ,');
    Qry.SQL.Add('  Data         date         ,');
    Qry.SQL.Add('  Documento    float        ,');
    Qry.SQL.Add('  NotaFiscal   int(11)      ,');
    Qry.SQL.Add('  Subgrupo     int(11)      ,');
    Qry.SQL.Add('  Grupo        int(11)      ,');
    Qry.SQL.Add('  Conjunto     int(11)      ,');
    Qry.SQL.Add('  cjOrdemLista int(11)      ,');
    Qry.SQL.Add('  grOrdemLista int(11)      ,');
    Qry.SQL.Add('  sgOrdemLista int(11)      ,');
    Qry.SQL.Add('  coOrdemLista int(11)       ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
    Result := ContinuaEmOutraProcedure(TabNo, Qry, Nome);
end;

function TUCreate.ContinuaEmOutraProcedure(TabNo: String; Qry: TmySQLQuery; Nome: String): String;
begin
  if Nome = Lowercase('InfoSeq') then
  begin
    Qry.SQL.Add('  Linha int(11), ');
    Qry.SQL.Add('  Item  int(11), ');
    Qry.SQL.Add('  Local varchar(255), ');
    Qry.SQL.Add('  Acao varchar(255) ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Janelas') then
  begin
    Qry.SQL.Add('  Codigo    int(11), ');
    Qry.SQL.Add('  Item      int(11), ');
    Qry.SQL.Add('  WndID     bigint(11), ');
    Qry.SQL.Add('  Texto     varchar(100), ');
    Qry.SQL.Add('  cbSize    int(11) UNSIGNED, ');
    //
    Qry.SQL.Add('  rcWindowT int(11), ');
    Qry.SQL.Add('  rcWindowE int(11), ');
    Qry.SQL.Add('  rcWindowB int(11), ');
    Qry.SQL.Add('  rcWindowD int(11), ');
    //
    Qry.SQL.Add('  rcClientT int(11), ');
    Qry.SQL.Add('  rcClientE int(11), ');
    Qry.SQL.Add('  rcClientB int(11), ');
    Qry.SQL.Add('  rcClientD int(11), ');
    //
    Qry.SQL.Add('  dwStyle         int(11) UNSIGNED, ');
    Qry.SQL.Add('  dwExStyle       int(11) UNSIGNED, ');
    Qry.SQL.Add('  dwWindowStatus  int(11) UNSIGNED, ');
    //
    Qry.SQL.Add('  cxWindowBorders int(11), ');
    Qry.SQL.Add('  cyWindowBorders int(11), ');
    //
    Qry.SQL.Add('  atomWindowType  int(11) , ');
    //
    Qry.SQL.Add('  PRIMARY KEY (Codigo,Item)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('LctoEdit') then
  begin
    Qry.SQL.Add('  Controle   int(11) NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  Sub        int(11), ');
    Qry.SQL.Add('  Carteira   int(11), ');
    Qry.SQL.Add('  Descricao  varchar(255), ');
    Qry.SQL.Add('  Data       date, ');
    Qry.SQL.Add('  Vencimento date, ');
    //Qry.SQL.Add('  Compensado date, ');
    Qry.SQL.Add('  MultaVal   double(15,2), ');
    Qry.SQL.Add('  JurosVal   double(15,2), ');
    Qry.SQL.Add('  ValorOri   double(15,2), ');
    Qry.SQL.Add('  ValorPgt   double(15,2) ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Layout001') then
  begin
    Qry.SQL.Add('  DataInvent varchar(008)  DEFAULT NULL,');
    Qry.SQL.Add('  MesAnoInic varchar(004)  DEFAULT NULL,');
    Qry.SQL.Add('  MesAnoFina varchar(004)  DEFAULT NULL,');
    Qry.SQL.Add('  CodigoProd varchar(020)  DEFAULT NULL,');
    Qry.SQL.Add('  SituacProd    char(001)  DEFAULT NULL,');
    Qry.SQL.Add('  CNPJTercei varchar(014)  DEFAULT NULL,');
    Qry.SQL.Add('  IETerceiro varchar(020)  DEFAULT NULL,');
    Qry.SQL.Add('  UFTerceiro varchar(002)  DEFAULT NULL,');
    Qry.SQL.Add('  Quantidade varchar(021)  DEFAULT NULL,');
    Qry.SQL.Add('  ValorUnita varchar(017)  DEFAULT NULL,');
    Qry.SQL.Add('  ValorTotal varchar(017)  DEFAULT NULL,');
    Qry.SQL.Add('  ICMSRecupe varchar(017)  DEFAULT NULL,');
    Qry.SQL.Add('  Observacao varchar(060)  DEFAULT NULL,');
    Qry.SQL.Add('  DescriProd varchar(080)  DEFAULT NULL,');
    Qry.SQL.Add('  GrupoProdu varchar(004)  DEFAULT NULL,');
    Qry.SQL.Add('  ClassifNCM varchar(010)  DEFAULT NULL,');
    Qry.SQL.Add('  RESERVADOS varchar(030)  DEFAULT NULL,');
    Qry.SQL.Add('  UnidMedida varchar(003)  DEFAULT NULL,');
    Qry.SQL.Add('  DescrGrupo varchar(030)  DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Lista1') then
  begin
    Qry.SQL.Add('  Codigo      int(11), ');
    Qry.SQL.Add('  Nome   varchar(255), ');
    Qry.SQL.Add('  Tipo        int(11), ');
    Qry.SQL.Add('  CliInt      int(11), ');
    Qry.SQL.Add('  Depto       int(11)  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ListErr1') then
  begin
    Qry.SQL.Add('  My_Idx integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Codigo varchar(20)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto1 varchar(255) DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('MinhaEtiq') then
  begin
    Qry.SQL.Add('  Nome   varchar(100) DEFAULT NULL,');
    Qry.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Texto1 varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto2 varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto3 varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto4 varchar(100)  DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('MovV') then
  begin
    Qry.SQL.Add('  CodiGra     integer(11)  DEFAULT 0   ,');
    Qry.SQL.Add('  CodiCor     integer(11)  DEFAULT 0   ,');
    Qry.SQL.Add('  CodiTam     integer(11)  DEFAULT 0   ,');
    Qry.SQL.Add('  NomeGra     varchar(30) DEFAULT NULL,');
    Qry.SQL.Add('  NomeCor     varchar(30) DEFAULT NULL,');
    Qry.SQL.Add('  NomeTam     varchar(30) DEFAULT NULL,');
    Qry.SQL.Add('  LevaQtd    double(15,2) DEFAULT NULL,');
    //Qry.SQL.Add('  LevaVal    double(15,2) DEFAULT NULL,');
    Qry.SQL.Add('  VendQtd    double(15,2) DEFAULT NULL,');
    Qry.SQL.Add('  VendVal    double(15,2) DEFAULT NULL,');
    Qry.SQL.Add('  CmisVal    double(15,2) DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('MinhaEtiq') then
  begin
    Qry.SQL.Add('  Nome   varchar(100) DEFAULT NULL,');
    Qry.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
    Qry.SQL.Add('  Texto1 varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto2 varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto3 varchar(100)  DEFAULT NULL,');
    Qry.SQL.Add('  Texto4 varchar(100)  DEFAULT NULL');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('NF1Fat') then begin
    Qry.SQL.Add('  Controle   integer(11)  NOT NULL AUTO_INCREMENT,');
    Qry.SQL.Add('  NumeroA    varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  DataA      varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  ValorA     double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  NumeroB    varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  DataB      varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  ValorB     double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  NumeroC    varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  DataC      varchar(30)  DEFAULT NULL,');
    Qry.SQL.Add('  ValorC     double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('PRIMARY KEY  (Controle)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('NF1Pro') then begin
    Qry.SQL.Add('  Controle   integer(11)  NOT NULL AUTO_INCREMENT,');
    Qry.SQL.Add('  Codigo     varchar( 30) DEFAULT NULL,');
    Qry.SQL.Add('  Produto    varchar(100) DEFAULT NULL,');
    Qry.SQL.Add('  ClasFisc   varchar(  8) DEFAULT NULL,');
    Qry.SQL.Add('  SitTrib    varchar(  3) DEFAULT NULL,');
    Qry.SQL.Add('  Unidade    varchar(  6) DEFAULT NULL,');
    Qry.SQL.Add('  Qtde       double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  Preco      double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  Desc_Per   double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  Valor      double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  ICMS_Per   double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  ICMS_Val   double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  IPI_Per    double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('  IPI_Val    double(15,2) DEFAULT NULL, ');
    Qry.SQL.Add('PRIMARY KEY  (Controle)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
(*
  Desativado em 2014-10-17 - Alguem usa?
  end else
  if Nome = Lowercase('NFeMPAdd') then
  begin
    Qry.SQL.Add('  Conta      int(11),');
    Qry.SQL.Add('  Emissao    date,');
    Qry.SQL.Add('  Fornece    int(11),');
    Qry.SQL.Add('  Caminhao   int(11),');
    Qry.SQL.Add('  Pecas      double(15,1),');
    Qry.SQL.Add('  PLE        double(15,3),');
    Qry.SQL.Add('  M2         double(15,2),');
    Qry.SQL.Add('  CMPValor   double(15,2),');
    Qry.SQL.Add('  NO_FORNECE varchar(100),');
    Qry.SQL.Add('  NO_CLIINT  varchar(100),');
    Qry.SQL.Add('  Ativo      tinyint(1) NOT NULL DEFAULT 0');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
*)
  end else
  if Nome = Lowercase('NFeYIts') then
  begin
    Qry.SQL.Add('  Seq1    integer(11) ,');
    Qry.SQL.Add('  nDup1   varchar(60) ,');
    Qry.SQL.Add('  dVenc1         date ,');
    Qry.SQL.Add('  vDup1  double(15,2) ,');

    Qry.SQL.Add('  Seq2    integer(11) ,');
    Qry.SQL.Add('  nDup2   varchar(60) ,');
    Qry.SQL.Add('  dVenc2         date ,');
    Qry.SQL.Add('  vDup2  double(15,2) ,');

    Qry.SQL.Add('  Seq3    integer(11) ,');
    Qry.SQL.Add('  nDup3   varchar(60) ,');
    Qry.SQL.Add('  dVenc3         date ,');
    Qry.SQL.Add('  vDup3  double(15,2) ,');

    Qry.SQL.Add('  Seq4    integer(11) ,');
    Qry.SQL.Add('  nDup4   varchar(60) ,');
    Qry.SQL.Add('  dVenc4         date ,');
    Qry.SQL.Add('  vDup4  double(15,2) ,');

    Qry.SQL.Add('  Seq5    integer(11) ,');
    Qry.SQL.Add('  nDup5   varchar(60) ,');
    Qry.SQL.Add('  dVenc5         date ,');
    Qry.SQL.Add('  vDup5  double(15,2) ,');

    Qry.SQL.Add('  Linha      int(11) NOT NULL DEFAULT 1');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('OperaAll') then
  begin
    Qry.SQL.Add('  Cliente    varchar(100),');
    Qry.SQL.Add('  Emitente   varchar(100),');
    Qry.SQL.Add('  TD         varchar(002),');
    Qry.SQL.Add('  Descri     varchar(100),');
    Qry.SQL.Add('  Ocorrencia varchar(100),');
    Qry.SQL.Add('  Data       date, ');
    Qry.SQL.Add('  Valor      double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Juro       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Taxa       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Desconto   double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Pago       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Saldo      double(15,2) NOT NULL DEFAULT "0.00"');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('OSAcess') then
  begin
    Qry.SQL.Add('  Adiciona   tinyint(1) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Componente int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Qtde       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Status     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Observ     varchar(50)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('OSCustos') then
  begin
    Qry.SQL.Add('  IDCtrl   int(11),');
    Qry.SQL.Add('  DataHora datetime,');
    Qry.SQL.Add('  Reduzido int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Produto  varchar(50),');
    Qry.SQL.Add('  Cor      varchar(50),');
    Qry.SQL.Add('  Tam      varchar(50),');
    Qry.SQL.Add('  Sigla    char(5),');
    Qry.SQL.Add('  Qtde     double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Custo    double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Fornece  varchar(100)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('OSItens') then
  begin
    Qry.SQL.Add('  Codigo     varchar(25),');
    Qry.SQL.Add('  Nome       varchar(80),');
    Qry.SQL.Add('  Qtde       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Valor      double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Desconto   double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Segurad    int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Status     tinyint(1) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Item       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Tipo       tinyint(1) NOT NULL DEFAULT 0'); // 1 = Servi�o 2 = Pe�a
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PagRec1') then
  begin
    Qry.SQL.Add('  Data          date, ');
    Qry.SQL.Add('  DataDoc       date, ');
    Qry.SQL.Add('  Vencimento    date, ');
    Qry.SQL.Add('  TERCEIRO      double DEFAULT "0", ');
    Qry.SQL.Add('  NOME_TERCEIRO varchar(255), ');
    Qry.SQL.Add('  NOMEVENCIDO   varchar(255), ');
    Qry.SQL.Add('  NOMECONTA     varchar(255), ');
    Qry.SQL.Add('  NOMECARTEIRA  varchar(255), ');
    Qry.SQL.Add('  Qtde          double(15,3), ');
    Qry.SQL.Add('  Descricao     varchar(255), ');
    Qry.SQL.Add('  Duplicata     varchar( 15), ');
    Qry.SQL.Add('  CtrlPai       double, ');
    Qry.SQL.Add('  Tipo          double, ');
    Qry.SQL.Add('  ID_Pgto       double, ');
    Qry.SQL.Add('  CtrlIni       double, ');
    Qry.SQL.Add('  Documento     double, ');
    Qry.SQL.Add('  NotaFiscal    double, ');
    Qry.SQL.Add('  Controle      double, ');
    Qry.SQL.Add('  Valor         double(15,2), ');
    Qry.SQL.Add('  MoraDia       double(15,2), ');
    Qry.SQL.Add('  PAGO_REAL     double(15,2), ');
    Qry.SQL.Add('  PENDENTE      double(15,2), ');
    Qry.SQL.Add('  ATUALIZADO    double(15,2), ');
    Qry.SQL.Add('  MULTA_REAL    double(15,2), ');
    Qry.SQL.Add('  ATRAZODD      double, ');
    Qry.SQL.Add('  NO_UH         varchar(10), ');
    Qry.SQL.Add('  Qtd2          double(15,3) ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PagRec2') then
  begin
    Qry.SQL.Add('  Data  date, ');
    Qry.SQL.Add('  NOMEVENCIDO  varchar(255), ');
    Qry.SQL.Add('  NOMECONTA    varchar(255), ');
    Qry.SQL.Add('  NOMECARTEIRA varchar(255), ');
    Qry.SQL.Add('  Descricao    varchar(255), ');
    Qry.SQL.Add('  Documento    double, ');
    Qry.SQL.Add('  NotaFiscal   double, ');
    Qry.SQL.Add('  Controle     double NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  ID_Pgto      double NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Valor        double(15,2), ');
    Qry.SQL.Add('  MoraDia      double(15,2), ');
    Qry.SQL.Add('  PAGO_REAL    double(15,2), ');
    Qry.SQL.Add('  ATUALIZADO   double(15,2), ');
    Qry.SQL.Add('  MULTA_REAL   double(15,2), ');
    Qry.SQL.Add('  ATRAZODD     double, ');
    Qry.SQL.Add('  PRIMARY KEY (ID_Pgto,Controle)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ParcPagtos') then
  begin
    Qry.SQL.Add('  Parcela  int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  SerieCH  varchar(10),');
    Qry.SQL.Add('  Doc      bigint(20) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Data     date,');
    Qry.SQL.Add('  Credito  double(15,2) NOT NULL DEFAULT "0",');
    Qry.SQL.Add('  Debito   double(15,2) NOT NULL DEFAULT "0",');
    Qry.SQL.Add('  Mora     double(15,2) NOT NULL DEFAULT "0",');
    Qry.SQL.Add('  Multa    double(15,2) NOT NULL DEFAULT "0",');
    Qry.SQL.Add('  ICMS_V   double(15,2) NOT NULL DEFAULT "0",');
    Qry.SQL.Add('  Duplicata varchar(30) ,');
    Qry.SQL.Add('  Descricao varchar(100),');
    Qry.SQL.Add('  PRIMARY KEY (Parcela)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('pci1') then
  begin
    Qry.SQL.Add('  GraGruC      int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  GraGru1      int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  GraTamI      int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Controle     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Estq         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Pedv         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Nec1         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Comp         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Prdz         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Nec2         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Sobr         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 1');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PedItsAnul')  then
  begin
    Qry.SQL.Add('  IDCtrl       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 1');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PQ010')  then
  begin
    Qry.SQL.Add('  Data         date,');
    Qry.SQL.Add('  OrigemCodi   int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  OrigemCtrl   int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Tipo         int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Insumo       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Nome         varchar(100),');
    Qry.SQL.Add('  Peso         double(15,3) NOT NULL DEFAULT "0.000",');
    Qry.SQL.Add('  Valor        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
(*
  end else
  if Nome = Lowercase('PQBIts') then begin
    Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  PQ                   int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  CI                   int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  CustoPadrao          double(15,4)           DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  Peso                 double(15,3) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  Valor                double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  Custo                double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  AjPeso               double(15,3) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  AjValor              double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  AjCusto              double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  NO_PQ                varchar(50)                                ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "1"         ,');
    Qry.SQL.Add('  PRIMARY KEY (Controle)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
*)
  end else
  if Nome = Lowercase('PrdMov')  then
  begin
    Qry.SQL.Add('  Nivel1       int(11),');
    Qry.SQL.Add('  NO_PRD       varchar(511),');
    Qry.SQL.Add('  PrdGrupTip   int(11),');
    Qry.SQL.Add('  UnidMed      int(11),');
    Qry.SQL.Add('  NO_PGT       varchar(30),');
    Qry.SQL.Add('  SIGLA        varchar(6),');
    Qry.SQL.Add('  NO_TAM       varchar(5),');
    Qry.SQL.Add('  NO_COR       varchar(30),');
    Qry.SQL.Add('  GraCorCad    int(11),');
    Qry.SQL.Add('  GraGruC      int(11),');
    Qry.SQL.Add('  GraGru1      int(11),');
    Qry.SQL.Add('  GraTamI      int(11),');
    Qry.SQL.Add('  DataHora     datetime,');
    Qry.SQL.Add('  IDCtrl       int(11),');
    Qry.SQL.Add('  Tipo         int(11),');
    Qry.SQL.Add('  OriCodi      int(11),');
    Qry.SQL.Add('  OriCtrl      int(11),');
    Qry.SQL.Add('  OriCnta      int(11),');
    Qry.SQL.Add('  OriPart      int(11),');
    Qry.SQL.Add('  Empresa      int(11),');
    Qry.SQL.Add('  StqCenCad    int(11),');
    Qry.SQL.Add('  StqCenLoc    int(11),');
    Qry.SQL.Add('  NO_LOC_CEN   varchar(120),');
    Qry.SQL.Add('  GraGruX      int(11),');
    Qry.SQL.Add('  Pecas        double(15,3),');
    Qry.SQL.Add('  Peso         double(15,3),');
    Qry.SQL.Add('  AreaM2       double(15,3),');
    Qry.SQL.Add('  AreaP2       double(15,2),');
    Qry.SQL.Add('  Qtde         double(15,2),');
    Qry.SQL.Add('  FatorClas    double(15,3),');
    Qry.SQL.Add('  SdoPecas     double(15,3),');
    Qry.SQL.Add('  SdoPeso      double(15,3),');
    Qry.SQL.Add('  SdoAreaM2    double(15,3),');
    Qry.SQL.Add('  SdoAreaP2    double(15,2),');
    Qry.SQL.Add('  SdoQtde      double(15,2),');
    Qry.SQL.Add('  LoteMP       varchar(20),'); 
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 1');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PrevBAB') then
  begin
    Qry.SQL.Add('  Conta     int(11) DEFAULT 0,');
    Qry.SQL.Add('  NOMECONTA varchar(100) DEFAULT "",');
    Qry.SQL.Add('  PrevBaI   int(11) DEFAULT 0,');
    Qry.SQL.Add('  PrevBaC   int(11) DEFAULT 0,');
    Qry.SQL.Add('  Valor     double(15,2)  DEFAULT "0.00",');
    Qry.SQL.Add('  LastM     varchar(8)  DEFAULT "",');
    Qry.SQL.Add('  Last1     double(15,2)  DEFAULT "0.00",');
    Qry.SQL.Add('  Last6     double(15,2)  DEFAULT "0.00",');
    Qry.SQL.Add('  Texto     varchar(40) DEFAULT NULL,');
    Qry.SQL.Add('  Adiciona  tinyint(1) DEFAULT 0');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PrevLct1') then
  begin
    Qry.SQL.Add('  Entidade             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Carteira             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Genero               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Descricao            varchar(100)                               ,');
    Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  Credito              double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  SubGrupo             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Grupo                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Conjunto             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Plano                int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "1"         ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PromCom') then
  begin
    Qry.SQL.Add('  Codigo    int(11)      NOT NULL DEFAULT 0           ,');
    Qry.SQL.Add('  Controle  int(11)      NOT NULL DEFAULT 0           ,');
    Qry.SQL.Add('  CICCTRL   int(11)      NOT NULL DEFAULT 0           ,');
    Qry.SQL.Add('  Professor int(11)      NOT NULL DEFAULT 0           ,');
    Qry.SQL.Add('  RolComis  int(11)      NOT NULL DEFAULT 0           ,');
    Qry.SQL.Add('  Data      date         NOT NULL DEFAULT "0000-00-00",');
    Qry.SQL.Add('  Cidade    varchar(100)                              ,');
    Qry.SQL.Add('  UF        char(2)                                   ,');
    Qry.SQL.Add('  Promotor  varchar(100)                              ,');
    Qry.SQL.Add('  Inscritos int(11)                DEFAULT "0"        ,');
    Qry.SQL.Add('  Presentes int(11)                DEFAULT "0"        ,');
    Qry.SQL.Add('  Comissao  double(15,6) NOT NULL  DEFAULT "0.000000" ,');
    Qry.SQL.Add('  DPrmAluno double(15,2) NOT NULL  DEFAULT "0.00"      ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PromWeb') then
  begin
    Qry.SQL.Add('  Codigo    int(11)      NOT NULL DEFAULT 0           ,');
    Qry.SQL.Add('  Nome      varchar(100)                              ,');
    Qry.SQL.Add('  CPF       varchar(18)                                ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ProtoMail') then
  begin
    Qry.SQL.Add('  Bloqueto     double(20,0)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Depto_Cod    integer(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Depto_Txt    varchar(100), ');
    Qry.SQL.Add('  Entid_Cod    integer(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Entid_Txt    varchar(100), ');
    Qry.SQL.Add('  Taref_Cod    integer(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Taref_Txt    varchar(100), ');
    Qry.SQL.Add('  Deliv_Cod    integer(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Deliv_Txt    varchar(100), ');
    Qry.SQL.Add('  DataE        Date, ');
    Qry.SQL.Add('  DataE_Txt    varchar(30), ');
    Qry.SQL.Add('  DataD        Date, ');
    Qry.SQL.Add('  DataD_Txt    varchar(30), ');
    Qry.SQL.Add('  Protocolo    integer(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  ProtoLote    integer(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  NivelEmail   tinyint(1)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  NivelDescr   varchar(50), ');
    Qry.SQL.Add('  RecipItem    int(11), ');
    Qry.SQL.Add('  RecipEmeio   varchar(100), ');
    Qry.SQL.Add('  RecipNome    varchar(100), ');
    Qry.SQL.Add('  RecipProno   varchar(20), ');
    Qry.SQL.Add('  Vencimento   date  NOT NULL DEFAULT "0000-00-00", ');
    Qry.SQL.Add('  Valor        double(15,2)  NOT NULL DEFAULT "0.00", ');
    Qry.SQL.Add('  Condominio   varchar(255)  NOT NULL DEFAULT "??", ');
    Qry.SQL.Add('  PreEmeio     int(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  IDEmeio      int(11)  NOT NULL DEFAULT "0", ');
    Qry.SQL.Add('  Item         int(11)  NOT NULL DEFAULT "0"  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ProtPakI') then
  begin
    Qry.SQL.Add('  Manual               int(11)      NOT NULL DEFAULT "0"          ,');
    //Qry.SQL.Add('  NO_Manual            varchar(50)                                ,');
    Qry.SQL.Add('  DataE                date         NOT NULL DEFAULT "0000-00-00" ,');
    Qry.SQL.Add('  MesAno               date         NOT NULL DEFAULT "0000-00-00" ,');
    Qry.SQL.Add('  Depto                int(11)      NOT NULL DEFAULT "0"          ,');
    Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
    Qry.SQL.Add('  Docum                double(20,0) NOT NULL DEFAULT "0"          ,');
    Qry.SQL.Add('  Valor                double(15,2) NOT NULL DEFAULT "0.00"       ,');
    Qry.SQL.Add('  Vencto               date         NOT NULL DEFAULT "0000-00-00" ,');
    Qry.SQL.Add('  Retorna              tinyint(1)   NOT NULL DEFAULT "0"          ,');
    Qry.SQL.Add('  Texto                varchar(30)                                ,');
    Qry.SQL.Add('  LimiteSai            date         NOT NULL DEFAULT "0000-00-00" ,');
    Qry.SQL.Add('  LimiteRem            date         NOT NULL DEFAULT "0000-00-00" ,');
    Qry.SQL.Add('  LimiteRet            date         NOT NULL DEFAULT "0000-00-00"  ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('PVICli')  then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Nome         varchar(120),');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 1');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ReparcI') then begin
    Qry.SQL.Add('  Credito              double(15,2)                               ,');
    Qry.SQL.Add('  Multa                double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  MoraDia              double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  Cliente              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  Depto                int(11)                                    ,');
    Qry.SQL.Add('  Mez                  int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencimento           date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  DataCad              date                                       ,');
    Qry.SQL.Add('  FatNum               double(20,0)           DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencto               date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  TxaJur               double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  TxaMul               double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ValBol               double(15,2)                               ,');
    Qry.SQL.Add('  ValJur               double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  ValMul               double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  ValPgt               double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  PRIMARY KEY (FatNum,Controle)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ReparcP') then begin
    Qry.SQL.Add('  TotParc              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  NumParc              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencto               date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  Valor                double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  PRIMARY KEY (TotParc,NumParc)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('ReparcI2') then
  begin
    Qry.SQL.Add('  Credito              double(15,2)                               ,');
    Qry.SQL.Add('  Multa                double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  MoraDia              double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  Cliente              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  Depto                int(11)                                    ,');
    Qry.SQL.Add('  Mez                  int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencimento           date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  DataCad              date                                       ,');
    Qry.SQL.Add('  FatNum               double(20,0)           DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencto               date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  TxaJur               double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  TxaMul               double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ValBol               double(15,2)                               ,');
    Qry.SQL.Add('  ValJur               double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  ValMul               double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  ValPgt               double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  PRIMARY KEY (FatNum,Controle)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
(*
  if Nome = Lowercase('ResMes') then
  begin
    Qry.SQL.Add('  SeqID        int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Exclusivo    char(1) NOT NULL DEFAULT "F",');
    Qry.SQL.Add('  TipoAgrupa   int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  OrdemLista   int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  SubOrdem     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  SubGrupo     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeSubGrupo varchar(255) NOT NULL DEFAULT "?",');
    Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeConta    varchar(255) NOT NULL DEFAULT "?",');
    Qry.SQL.Add('  AnoAn        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Pendencias   double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  AnoAt        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  LimChrtLin   int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  PRIMARY KEY (SeqID)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
*)
(*
  if Nome = Lowercase('ResPenM') then
  begin
    Qry.SQL.Add('  Grupo        int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeConta    varchar(255) NOT NULL DEFAULT "?",');
    Qry.SQL.Add('  AnoAn        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Pendencias   double(15,2) NOT NULL DEFAULT "0.00"');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
*)
  if Nome = Lowercase('ReparcP2') then
  begin
    Qry.SQL.Add('  TotParc              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  NumParc              int(11)                DEFAULT "0"         ,');
    Qry.SQL.Add('  Vencto               date         NOT NULL  DEFAULT "0000-00-00",');
    Qry.SQL.Add('  Valor                double(15,2) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  PRIMARY KEY (TotParc,NumParc)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SaldoFim') then
  begin
    Qry.SQL.Add('  Carteira    int(11)     , ');
    Qry.SQL.Add('  Saldo       double(15,2)  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SaldosNiv') then
  begin
    Qry.SQL.Add('  Nivel        int(11)                      ,');
    // C�digo
    Qry.SQL.Add('  CO_Cta       int(11)                      ,');
    Qry.SQL.Add('  CO_SGr       int(11)                      ,');
    Qry.SQL.Add('  CO_Gru       int(11)                      ,');
    Qry.SQL.Add('  CO_Cjt       int(11)                      ,');
    Qry.SQL.Add('  CO_Pla       int(11)                      ,');
    // Nome
    Qry.SQL.Add('  NO_Cta       varchar(100)                 ,');
    Qry.SQL.Add('  NO_SGr       varchar(100)                 ,');
    Qry.SQL.Add('  NO_Gru       varchar(100)                 ,');
    Qry.SQL.Add('  NO_Cjt       varchar(100)                 ,');
    Qry.SQL.Add('  NO_Pla       varchar(100)                 ,');
    // Ordem Lista
    Qry.SQL.Add('  OL_Cta       int(11)                      ,');
    Qry.SQL.Add('  OL_SGr       int(11)                      ,');
    Qry.SQL.Add('  OL_Gru       int(11)                      ,');
    Qry.SQL.Add('  OL_Cjt       int(11)                      ,');
    Qry.SQL.Add('  OL_Pla       int(11)                      ,');
    // Valores
    Qry.SQL.Add('  SdoAnt       double(15,2)                 ,');
    Qry.SQL.Add('  Credito      double(15,2)                 ,');
    Qry.SQL.Add('  Debito       double(15,2)                 ,');
    Qry.SQL.Add('  Movim        double(15,2)                 ,');
    // Extra (C�pia do c�digo)
    Qry.SQL.Add('  EX_Cta       int(11)                      ,');
    Qry.SQL.Add('  EX_SGr       int(11)                      ,');
    Qry.SQL.Add('  EX_Gru       int(11)                      ,');
    Qry.SQL.Add('  EX_Cjt       int(11)                      ,');
    Qry.SQL.Add('  EX_Pla       int(11)                      ,');
    //
    Qry.SQL.Add('  ID_SEQ       int(11)                      ,');
    Qry.SQL.Add('  Ativo        tinyint(1)                    ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SdoNiveis') then
  begin                                                                            
    Qry.SQL.Add('  Nivel        int(11)                                            ,');
    Qry.SQL.Add('  CodPla       int(11)                                            ,');
    Qry.SQL.Add('  CodCjt       int(11)                                            ,');
    Qry.SQL.Add('  CodGru       int(11)                                            ,');
    Qry.SQL.Add('  CodSgr       int(11)                                            ,');
    Qry.SQL.Add('  Genero       int(11)                                            ,');
    Qry.SQL.Add('  Ordena       varchar(100)                                       ,');
    Qry.SQL.Add('  NomeGe       varchar(100)                                       ,');
    Qry.SQL.Add('  NomeNi       varchar(20)                                        ,');
    Qry.SQL.Add('  SumMov       double(15,2)                                       ,');
    Qry.SQL.Add('  SdoAnt       double(15,2)                                       ,');
    Qry.SQL.Add('  SumCre       double(15,2)                                       ,');
    Qry.SQL.Add('  SumDeb       double(15,2)                                       ,');
    Qry.SQL.Add('  SdoFim       double(15,2)                                       ,');
    // usado para cadastrar
    Qry.SQL.Add('  Ctrla        tinyint(1)                                         ,');
    Qry.SQL.Add('  Seleci       tinyint(1)                                         ,');
    // usado para obter saldo do modo antigo para o novo
    Qry.SQL.Add('  IniOld       double(15,2)                                       ,');
    Qry.SQL.Add('  PRIMARY KEY (Nivel,Genero)');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SdoNivCtas') then
  begin
    Qry.SQL.Add('  NomeNivel    varchar(20),');
    Qry.SQL.Add('  Nivel        int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeGenero   varchar(100),');
    Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  SdoIni       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  MovCre       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  MovDeb       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  SdoFim       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 0');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SdoOldNew') then
  begin
    Qry.SQL.Add('  NomePlano    varchar(100),');
    Qry.SQL.Add('  Plano        int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeConjunto varchar(100),');
    Qry.SQL.Add('  Conjunto     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeGrupo    varchar(100),');
    Qry.SQL.Add('  Grupo        int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeSubGrupo varchar(100),');
    Qry.SQL.Add('  SubGrupo     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  NomeConta    varchar(100),');
    Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  SdoIni       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Movto        double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  MovNew       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Diferenca    double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 0');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('StqInnNFe') then
  begin
    Qry.SQL.Add('  NeedPc               int(11)      NOT NULL  DEFAULT 0           ,');
    Qry.SQL.Add('  NeedM2               int(11)      NOT NULL  DEFAULT 0           ,');
    Qry.SQL.Add('  NeedKg               int(11)      NOT NULL  DEFAULT 0           ,');
    Qry.SQL.Add('  nItem                int(11)      NOT NULL  DEFAULT 0           ,');
    Qry.SQL.Add('  prod_cProd           varchar(60)                                ,');
    Qry.SQL.Add('  prod_xProd           varchar(120)                               ,');
    Qry.SQL.Add('  prod_uCom            varchar(6)                                 ,');
    Qry.SQL.Add('  prod_qCom            double(15,4)           DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  prod_vUnCom          double(21,10)          DEFAULT "0.0000000000",');
    Qry.SQL.Add('  prod_vProd           double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  Nivel1               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  HowBxaEstq           int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Pecas                double(12,4)           DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  Peso                 double(12,4)           DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  M2                   double(12,4)           DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  P2                   double(12,4)           DEFAULT "0.0000"    ,');
    Qry.SQL.Add('  prod_vFrete          double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  prod_vSeg            double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  prod_vDesc           double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  prod_vOutro          double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  PRIMARY KEY (nItem)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('StqInnOpt') then
  begin
    Qry.SQL.Add('  nItem                int(11)      NOT NULL  DEFAULT 0           ,');
    Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  NO_Cor               varchar(30)                                ,');
    Qry.SQL.Add('  NO_Tam               varchar(5)                                 ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('stqmovitsc') then
  begin
    Qry.SQL.Add('  DataHora             datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
    Qry.SQL.Add('  IDCtrl               int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Tipo                 int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  OriCodi              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  OriCtrl              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  OriCnta              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  OriPart              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Empresa              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  StqCenCad            int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  GraGruX              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Qtde                 double(15,3) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  Pecas                double(15,3) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  Peso                 double(15,3) NOT NULL  DEFAULT "0.000"     ,');
    Qry.SQL.Add('  AlterWeb             tinyint(1)   NOT NULL  DEFAULT "1"         ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL  DEFAULT "1"         ,');
    Qry.SQL.Add('  AreaM2               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
    Qry.SQL.Add('  AreaP2               double(15,2) NOT NULL  DEFAULT "0.00"      ,');
    Qry.SQL.Add('  FatorClas            double(15,3) NOT NULL  DEFAULT "1.000"     ,');
    Qry.SQL.Add('  QuemUsou             int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  Retorno              tinyint(1)             DEFAULT "0"         ,');
    Qry.SQL.Add('  ParTipo              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  ParCodi              int(11)      NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  DebCtrl              int(11)      NOT NULL  DEFAULT "-1"        ,');
    Qry.SQL.Add('  SMIMultIns           int(11)      NOT NULL  DEFAULT "-1"        ,');
    Qry.SQL.Add('  CustoAll             double(15,2) NOT NULL  DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ValorAll             double(15,2) NOT NULL  DEFAULT "0.00"      ,');
    Qry.SQL.Add('  GrupoBal             int(11)      NOT NULL  DEFAULT "-1"        ,');
    Qry.SQL.Add('  PRIMARY KEY (IDCtrl)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPClie') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPRepr') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPRegi') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPRegr') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPProd') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPSitu') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
    //
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('INSERT INTO svpsitu VALUES (1,1,"Bloqueado",0), (2,2,' +
    '"Liberado",0), (3,3,"Faturado total",0), (4,4,"Faturado parcial",0), ' +
    '(5,5,"Em an�lise",0), (6,6,"Cancelado",0)');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('SVPAtrProd') then
  begin
    Qry.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Controle     int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  CodUsu       int(11) NOT NULL DEFAULT 0   , ');
    Qry.SQL.Add('  Nome         varchar(100)                 , ');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0  ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('Testemunha') then begin
    Qry.SQL.Add('  Nome  varchar(100), ');
    Qry.SQL.Add('  CNPJ  varchar(30), ');
    Qry.SQL.Add('  Tipo  int(11)       ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('TrfOldNew') then
  begin
    Qry.SQL.Add('  Data         date,');
    Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Carteira     int(11) NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  Credito      double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Debito       double(15,2) NOT NULL DEFAULT "0.00",');
    Qry.SQL.Add('  Descri       varchar(50),');
    Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 0');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('UFsAtoB') then
  begin
    Qry.SQL.Add('  Nome         char(2),');
    Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 0');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('UnidCond') then
  begin
    Qry.SQL.Add('  Andar        int(11),      ');
    Qry.SQL.Add('  Apto         int(11),      ');
    Qry.SQL.Add('  Entidad      int(11),      ');
    Qry.SQL.Add('  Protoco      int(11),      ');
    Qry.SQL.Add('  Bloquet      int(11),      ');
    Qry.SQL.Add('  Unidade      varchar(10),  ');
    Qry.SQL.Add('  Proprie      varchar(100), ');
    Qry.SQL.Add('  Valor        double(15,2), ');
    Qry.SQL.Add('  Data1        date,         ');
    Qry.SQL.Add('  Data2        date,         ');
    Qry.SQL.Add('  Texto        varchar(100), ');
    Qry.SQL.Add('  RecipItem    int(11),      '); // ID
    Qry.SQL.Add('  RecipNome    varchar(100), ');
    Qry.SQL.Add('  RecipEmeio   varchar(100), ');
    Qry.SQL.Add('  RecipProno   varchar(20),  ');
    //
    Qry.SQL.Add('  Vencimento   date, ');
    Qry.SQL.Add('  Condominio   varchar(255), ');
    Qry.SQL.Add('  PreEmeio     int(11),');
    Qry.SQL.Add('  IDEmeio      int(11),');
    //
    Qry.SQL.Add('  Ordem        int(11),      ');
    Qry.SQL.Add('  Selecio      tinyint(1)    ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('UnidCon2') then
  begin
    Qry.SQL.Add('  LeitCod      int(11),      ');
    Qry.SQL.Add('  BlocOrd      int(11),      ');
    Qry.SQL.Add('  Andar        int(11),      ');
    Qry.SQL.Add('  Apto         int(11),      ');
    Qry.SQL.Add('  Entidad      int(11),      ');
    Qry.SQL.Add('  Casas        tinyint(1),   ');
    Qry.SQL.Add('  LeitNom      varchar(50),  ');
    Qry.SQL.Add('  BlocNom      varchar(100), ');
    Qry.SQL.Add('  Unidade      varchar(10),  ');
    Qry.SQL.Add('  Proprie      varchar(100), ');
    Qry.SQL.Add('  UnidImp      varchar(10),  ');
    Qry.SQL.Add('  Quant01      double(15,3), ');
    Qry.SQL.Add('  Quant02      double(15,3), ');
    Qry.SQL.Add('  Quant03      double(15,3), ');
    Qry.SQL.Add('  Quant04      double(15,3), ');
    Qry.SQL.Add('  Quant05      double(15,3), ');
    Qry.SQL.Add('  Quant06      double(15,3), ');
    Qry.SQL.Add('  Quant07      double(15,3), ');
    Qry.SQL.Add('  Quant08      double(15,3), ');
    Qry.SQL.Add('  Quant09      double(15,3), ');
    Qry.SQL.Add('  Quant10      double(15,3), ');
    Qry.SQL.Add('  Quant11      double(15,3), ');
    Qry.SQL.Add('  Quant12      double(15,3), ');
    Qry.SQL.Add('  Ativo        tinyint(1)    ');
    Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
(*
  end else
  if Nome = Lowercase('_rlg_culto_002_estrf') then begin
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  ItemTxt              varchar(20)  NOT NULL                      ,');
    Qry.SQL.Add('  Nome                 varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NumEstrf             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('_rlg_culto_002_linha') then begin
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel2               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  ID                   int(11)      NOT NULL AUTO_INCREMENT       ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Item                 int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TxtLin               text         NOT NULL                      ,');
    Qry.SQL.Add('  Leitor               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TipoPredef           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TipoSeleci           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NumLinha             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NO_LEITOR            varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  Sigla                char(3)      NOT NULL                      ,');
    Qry.SQL.Add('  Clareza              tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  NO_TIPOPREDEF        varchar(50)  NOT NULL                      ,');
    Qry.SQL.Add('  LiturHinar           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  LiturCabec           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  MusicHinar           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  MusicCabec           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  MusicNumer           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  LivroI               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  LivroF               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  CapitI               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  CapitF               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  VersoI               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  VersoF               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TraduCod             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TraduTxt             varchar(255) NOT NULL                      ,');
    Qry.SQL.Add('  CredLivCod           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  CredLivTxt           varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  CredCabCod           int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  CredCabTxt           varchar(255) NOT NULL                      ,');
    Qry.SQL.Add('  Passagem             varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  Texto                Text         NOT NULL                      ,');
    Qry.SQL.Add('  PRIMARY KEY (ID)');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('_rlg_culto_002_linha_itens') then begin
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel2               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel3               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  ID                   int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('_rlg_culto_004_estrf') then begin
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel2               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel3               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel4               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  IDLinha              int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  ItemTxt              varchar(30)  NOT NULL                      ,');
    Qry.SQL.Add('  Nome                 varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NumEstrf             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('_rlg_culto_004_linha') then begin
    Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel1               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel2               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel3               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel4               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Nivel5               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  IDLinha              int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Conta                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Item                 int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Ordem                int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  TxtLin               text         NOT NULL                      ,');
    Qry.SQL.Add('  Leitor               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NumLinha             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  NO_LEITOR            varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  Sigla                char(3)      NOT NULL                      ,');
    Qry.SQL.Add('  Clareza              tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  Leitura              tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  Texto                text         NOT NULL                      ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
*)
  end else
  if Nome = LowerCase('relvdatmp') then begin
    Qry.SQL.Add('  Codigo               int(11)                                    ,');
    Qry.SQL.Add('  DiaHora              varchar(30)                                ,');
    Qry.SQL.Add('  Cliente              varchar(100)                               ,');
    Qry.SQL.Add('  Vendedor             varchar(100)                               ,');
    Qry.SQL.Add('  Status               varchar(100)                               ,');
    Qry.SQL.Add('  Valor                double(15,3)  DEFAULT "1.000"              ,');
    Qry.SQL.Add('  Desconto             double(15,3)  DEFAULT "1.000"              ,');
    Qry.SQL.Add('  Frete                double(15,3)  DEFAULT "1.000"              ,');
    Qry.SQL.Add('  Total                double(15,3)  DEFAULT "1.000"              ,');
    Qry.SQL.Add('  CustoMedio           double(15,3)  DEFAULT "1.000"               ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else
  if Nome = Lowercase('bloqinadcli') then begin
    Qry.SQL.Add('  Cliente              int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
    Qry.SQL.Add('  CliInt               int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  CREDITO              double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  PAGO                 double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  Juros                double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  Multa                double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  TOTAL                double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  SALDO                double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  PEND_VAL             double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  Mez                  int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  MEZ_TXT              varchar(7)   NOT NULL                      ,');
    Qry.SQL.Add('  Vencimento           date         NOT NULL                      ,');
    Qry.SQL.Add('  Compensado           date         NOT NULL                      ,');
    Qry.SQL.Add('  Controle             int(11)      NOT NULL                      ,');
    Qry.SQL.Add('  FatNum               double(20,0) NOT NULL                      ,');
    Qry.SQL.Add('  VCTO_TXT             varchar(8)   NOT NULL                      ,');
    Qry.SQL.Add('  NOMEEMP              varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  NOMEENT              varchar(100) NOT NULL                      ,');
    Qry.SQL.Add('  NOME_Cfg             varchar(50) NOT NULL                       ,');
    Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                      ,');
    Qry.SQL.Add('  NewVencto            date         NOT NULL                      ,');
    Qry.SQL.Add('  Boleto               double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  Tipo                 double(19,2) NOT NULL                      ,');
    Qry.SQL.Add('  Config               double(19,2) NOT NULL                       ');
    Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
    Qry.ExecSQL;
  end else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
  Nome + '" por falta de implementa��o!');
  Result := Nome;
end;


{$ifNDef SemDBLocal}
{
function TUCreate.GerenciaTabelaLocal(Tabela: String; Acao: TAcaoCreate): Boolean;
var
  QtTemp1, QtTemp2: TmySQLQuery;
  Encontrado: Boolean;
begin
  Result := False;
  Encontrado := False;
  if Geral.ReadAppKeyLM(Tabela, Application.Title+'\Recriar', ktInteger, 0) = 1 then
  begin;
    Result := RecriaTabelaLocal(Tabela, 1);
    Exit;
  end;
  QtTemp1 := TmySQLQuery.Create(Dmod);
  QtTemp1.DataBase := Dmod.MyLocDataBase;
  QtTemp2 := TmySQLQuery.Create(Dmod);
  QtTemp2.DataBase := Dmod.MyLocDataBase;
  QtTemp1.SQL.Add('SHOW TABLES');
  UnDmkDAC_PF.AbreQuery(QtTemp1, Dmod.MyLocDatabase);
  while not QtTemp1.Eof do
  begin
    if Lowercase(QtTemp1.Fields[0].AsString) = Lowercase(Tabela) then
    begin
      Encontrado := True;
      if (Acao = acFind) then Result := True
      else if (Acao = acDrop) then
      begin
        QtTemp2.SQL.Add('DROP TABLE IF EXISTS '+Tabela);
        QtTemp2.ExecSQL;
        Result := True;
      end;
    end;
    QtTemp1.Next;
  end;
  if (Acao = acCreate) and Encontrado=False then
  begin
    RecriaTabelaLocal(Tabela, 1);
    Result := True;
  end;
  QtTemp1.Free;
  QtTemp2.Free;
end;
}
{
function TUCreate.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Tabela := Lowercase(Tabela);
  Result := False;
  if Tabela = '*' then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o e cria��o de todas'+
    ' tabelas locais?')=ID_YES
    then begin
      Screen.Cursor := crHourGlass;
      try
        Dmod.QlLocal.ExecSql;
      except
        raise;
      end;
      Screen.Cursor := crDefault;
    end;
  end else begin
    try
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('DROP TABLE IF EXISTS '+Tabela);
      Dmod.QrUpdL.ExecSQL;
      if Acao > 0 then
      begin
        Dmod.QrUpdL.SQL.Clear;
        if Trim(Tabela) = '' then begin
          Geral.MB_Erro('Tabela temporaria n�o definida!');
        end else if Lowercase(Tabela) = Lowercase('Aniversarios') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Aniversarios (');
          Dmod.QrUpdL.SQL.Add('  Codi  int(11), ');
          Dmod.QrUpdL.SQL.Add('  Tipo  int(11), ');
          Dmod.QrUpdL.SQL.Add('  TiCa  int(11), ');
          Dmod.QrUpdL.SQL.Add('  Nome  varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  DCad  date, ');
          Dmod.QrUpdL.SQL.Add('  CEP   int(11), ');
          Dmod.QrUpdL.SQL.Add('  Bair  Varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Comp  Varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Cida  Varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Pais  Varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  Nume  integer(5), ');
          Dmod.QrUpdL.SQL.Add('  Rua   varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Sexo  varchar(1), ');
          Dmod.QrUpdL.SQL.Add('  UF    varchar(2), ');
          Dmod.QrUpdL.SQL.Add('  Nasc  date ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ArreBAA') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ArreBAA (');
          Dmod.QrUpdL.SQL.Add('  Seq      int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Conta    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ArreBaI  int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ArreBaC  int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Valor    double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Texto    varchar(40) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Adiciona tinyint(1) DEFAULT 0');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ArreBAAUni') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ArreBAAUni (');
          Dmod.QrUpdL.SQL.Add('  Seq          int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Apto         int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Propriet     int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NaoArreSobre int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NaoArreRisco int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Unidade      varchar(10) DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  NomePropriet varchar(100) DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  Valor        double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Calculo      tinyint(1)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Cotas        double(15,6)  DEFAULT "0.000000",');
          Dmod.QrUpdL.SQL.Add('  DescriCota   varchar(10)  DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  TextoCota    varchar(30)  DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  ArreBaC      int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ArreBaI      int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Adiciona     tinyint(1) DEFAULT 0');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Bal') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Bal (');
          Dmod.QrUpdL.SQL.Add('  Produto        Varchar(8)   NOT NULL default "00000000",');
          Dmod.QrUpdL.SQL.Add('  Descricao      Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  NomeForecedor  Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Fornecedor     int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQAnt       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVAnt       double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQEnt       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVEnt       double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQVen       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVVen       double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQDev       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVDev       double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQRec       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVRec       double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQNow       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVNow       double(15,4) NOT NULL default "0"');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('BarCodes') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE BarCodes (');
          Dmod.QrUpdL.SQL.Add('  Conta  int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Nome   varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  EAN13  varchar(13)  DEFAULT "0000000000000",');
          Dmod.QrUpdL.SQL.Add('  Preco  varchar(50)  DEFAULT " ",');
          Dmod.QrUpdL.SQL.Add('  Obs    varchar(255) DEFAULT " "');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('BolConIts') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE BolConIts (');
          Dmod.QrUpdL.SQL.Add('  Tipo      int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Controle  int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ContaN    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ContaC    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ContaI    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Texto     varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Preco     double(15,4)  DEFAULT 0.0000,');
          Dmod.QrUpdL.SQL.Add('  Valor     double(15,2)  DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  Apto      int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Propriet  int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Boleto    double(20,0)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  MedAnt    double(15,6)  DEFAULT 0.000000,');
          Dmod.QrUpdL.SQL.Add('  MedAtu    double(15,6)  DEFAULT 0.000000,');
          Dmod.QrUpdL.SQL.Add('  Fator     double(15,6)  DEFAULT 0.000000,');
          Dmod.QrUpdL.SQL.Add('  Consumo1  double(15,6)  DEFAULT 0.000000,');
          Dmod.QrUpdL.SQL.Add('  Consumo2  double(15,6)  DEFAULT 0.000000');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Certificado') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Certificado (');
          Dmod.QrUpdL.SQL.Add('  Controle    int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Valor    varchar(255)      ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Chamada1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Chamada1 (');
          Dmod.QrUpdL.SQL.Add('  NOMEPROFESSOR  varchar(100) DEFAULT "???", ');
          Dmod.QrUpdL.SQL.Add('  T_I  int(11) NOT NULL Default "0", ');
          Dmod.QrUpdL.SQL.Add('  D01  date, ');
          Dmod.QrUpdL.SQL.Add('  D02  date, ');
          Dmod.QrUpdL.SQL.Add('  D03  date, ');
          Dmod.QrUpdL.SQL.Add('  D04  date, ');
          Dmod.QrUpdL.SQL.Add('  D05  date, ');
          Dmod.QrUpdL.SQL.Add('  D06  date, ');
          Dmod.QrUpdL.SQL.Add('  D07  date, ');
          Dmod.QrUpdL.SQL.Add('  D08  date, ');
          Dmod.QrUpdL.SQL.Add('  D09  date, ');
          Dmod.QrUpdL.SQL.Add('  D10  date, ');
          Dmod.QrUpdL.SQL.Add('  D11  date, ');
          Dmod.QrUpdL.SQL.Add('  D12  date, ');
          Dmod.QrUpdL.SQL.Add('  D13  date, ');
          Dmod.QrUpdL.SQL.Add('  D14  date, ');
          Dmod.QrUpdL.SQL.Add('  D15  date, ');
          Dmod.QrUpdL.SQL.Add('  D16  date, ');
          Dmod.QrUpdL.SQL.Add('  D17  date, ');
          Dmod.QrUpdL.SQL.Add('  D18  date, ');
          Dmod.QrUpdL.SQL.Add('  D19  date, ');
          Dmod.QrUpdL.SQL.Add('  D20  date, ');
          Dmod.QrUpdL.SQL.Add('  D21  date, ');
          Dmod.QrUpdL.SQL.Add('  D22  date, ');
          Dmod.QrUpdL.SQL.Add('  D23  date, ');
          Dmod.QrUpdL.SQL.Add('  D24  date, ');
          Dmod.QrUpdL.SQL.Add('  D25  date, ');
          Dmod.QrUpdL.SQL.Add('  D26  date, ');
          Dmod.QrUpdL.SQL.Add('  D27  date, ');
          Dmod.QrUpdL.SQL.Add('  D28  date, ');
          Dmod.QrUpdL.SQL.Add('  D29  date, ');
          Dmod.QrUpdL.SQL.Add('  D30  date, ');
          Dmod.QrUpdL.SQL.Add('  D31  date, ');
          Dmod.QrUpdL.SQL.Add('  W01  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  W02  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  W03  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  W04  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  W05  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  W06  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  W07  varchar(8), ');
          Dmod.QrUpdL.SQL.Add('  H01  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  H02  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  H03  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  H04  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  H05  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  H06  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  H07  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Ref  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Tur  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (T_I)');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Chamada2') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Chamada2 (');
          Dmod.QrUpdL.SQL.Add('  T_I  int(11) NOT NULL default "0", ');
          Dmod.QrUpdL.SQL.Add('  Alu  int(11) NOT NULL default "0", ');
          Dmod.QrUpdL.SQL.Add('  Nom  varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Nas  varchar(10), ');
          Dmod.QrUpdL.SQL.Add('  S01  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S02  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S03  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S04  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S05  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S06  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S07  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S08  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S09  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S10  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S11  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S12  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S13  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S14  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S15  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S16  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S17  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S18  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S19  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S20  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S21  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S22  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S23  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S24  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S25  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S26  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S27  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S28  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S29  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S30  char(1), ');
          Dmod.QrUpdL.SQL.Add('  S31  char(1), ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (T_I, Nom, Alu)');
          //Dmod.QrUpdL.SQL.Add(',  KEY IdxNom (Nom)');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Cheques') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Cheques (');
          Dmod.QrUpdL.SQL.Add('  Conta     int(11),');
          Dmod.QrUpdL.SQL.Add('  Cheque    double,');
          Dmod.QrUpdL.SQL.Add('  ValorNU   double(15,2),');
          Dmod.QrUpdL.SQL.Add('  Cliente   varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Data      date,');
          Dmod.QrUpdL.SQL.Add('  Vencto    date,');
          Dmod.QrUpdL.SQL.Add('  Descricao varchar(255)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Chamada3') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Chamada3 (');
          Dmod.QrUpdL.SQL.Add('  Turma     int(11),');
          Dmod.QrUpdL.SQL.Add('  Curso     int(11),');
          Dmod.QrUpdL.SQL.Add('  Materia   int(11),');
          Dmod.QrUpdL.SQL.Add('  Sala      int(11),');
          Dmod.QrUpdL.SQL.Add('  Professor int(11),');
          Dmod.QrUpdL.SQL.Add('  Carga     int(11),');
          Dmod.QrUpdL.SQL.Add('  ReTurma   double(15,2),');
          Dmod.QrUpdL.SQL.Add('  ReCurso   double(15,2),');
          Dmod.QrUpdL.SQL.Add('  ReMateria double(15,2),');
          Dmod.QrUpdL.SQL.Add('  CuSala    double(15,2),');
          Dmod.QrUpdL.SQL.Add('  CuProfess double(15,2),');
          Dmod.QrUpdL.SQL.Add('  ToSala    double(15,2),');
          Dmod.QrUpdL.SQL.Add('  ToProfess double(15,2),');
          Dmod.QrUpdL.SQL.Add('  ToGeral   double(15,2),');
          Dmod.QrUpdL.SQL.Add('  SaTurma   double(15,2),');
          Dmod.QrUpdL.SQL.Add('  SaCurso   double(15,2),');
          Dmod.QrUpdL.SQL.Add('  SaMateria double(15,2),');
          Dmod.QrUpdL.SQL.Add('  SaSala    double(15,2),');
          Dmod.QrUpdL.SQL.Add('  SaProfess double(15,2)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ChequesImp') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ChequesImp (');
          Dmod.QrUpdL.SQL.Add('  Valor      varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Extenso1   varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Extenso2   varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Favorecido varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Cidade     varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Dia        varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Mes        varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Ano        varchar(255),');
          Dmod.QrUpdL.SQL.Add('  BomPara    varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Observacao varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Seq            int(11),');
          Dmod.QrUpdL.SQL.Add('  Codigo         int(11),');
          Dmod.QrUpdL.SQL.Add('  CPI            int(11),');
          Dmod.QrUpdL.SQL.Add('  TopoIni        int(11),');
          Dmod.QrUpdL.SQL.Add('  Altura         int(11),');
          Dmod.QrUpdL.SQL.Add('  MEsq           int(11)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('CNAB_1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE CNAB_1 (');
          Dmod.QrUpdL.SQL.Add('  Boleto    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Instante  double(15,8) DEFAULT 0.00000000, ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Boleto)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('CNAB_Rem') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE CNAB_Rem (');
          Dmod.QrUpdL.SQL.Add('  Item      int(11)    AUTO_INCREMENT, ');
          Dmod.QrUpdL.SQL.Add('  Linha     int(11)    NOT NULL DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Codigo    int(11)    NOT NULL DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  TipoDado  tinyint(3) NOT NULL DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  DadoI     int(11)             DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  DadoF     double              DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  DadoD     date                DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  DadoH     time                DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  DadoT     varchar(255)        DEFAULT "", ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Item)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else
        if Lowercase(Tabela) = Lowercase('Concilia') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Concilia (');
          Dmod.QrUpdL.SQL.Add('  Linha    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  DataM    date DEFAULT "0000-00-00",');
          Dmod.QrUpdL.SQL.Add('  Texto    varchar(50) DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Docum    varchar(20) DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Valor    double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Saldo    double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Acao     int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Conta    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Clien    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Forne    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Depto    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Perio    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  CtrlE    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  CartT    int(11) DEFAULT -1000, ');
          Dmod.QrUpdL.SQL.Add('  CtaLk    int(11) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  DifEr    double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  NOMEDEPTO varchar(100),');
          Dmod.QrUpdL.SQL.Add('  NOMECONTA varchar(100),');
          Dmod.QrUpdL.SQL.Add('  NOMETERCE varchar(100),');
          Dmod.QrUpdL.SQL.Add('  NOMECARTE varchar(100),');
          Dmod.QrUpdL.SQL.Add('  TIPOCARTE int(11),');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Linha)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else
        if Lowercase(Tabela) = Lowercase('ConsLei') then
        begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ConsLei (');
          Dmod.QrUpdL.SQL.Add('  ID_Rand  varchar(15) DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  Propriet int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  OrdBlc   int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Andar    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Apto_ID  int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Apto_Un  varchar(10) DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  MedAnt   double(15,6)  DEFAULT "0.000000",');
          Dmod.QrUpdL.SQL.Add('  MedAtu   double(15,6)  DEFAULT "0.000000",');
          Dmod.QrUpdL.SQL.Add('  Carencia double(15,6)  DEFAULT "0.000000",');
          Dmod.QrUpdL.SQL.Add('  QtdFator double(15,6)  DEFAULT "0.000000",');
          Dmod.QrUpdL.SQL.Add('  ValUnita double(15,6)  DEFAULT "0.000000",');
          Dmod.QrUpdL.SQL.Add('  NaoImpLei tinyint(1) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  DifCaren tinyint(1) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Adiciona tinyint(1) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (ID_Rand, Apto_ID)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
(*
        end else if Lowercase(Tabela) = Lowercase('ConsMes') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ConsMes (');
          Dmod.QrUpdL.SQL.Add('  Codigo       int(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Nome         varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00"');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
*)
        end else if Lowercase(Tabela) = Lowercase('CoresConta') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE CoresConta (');
          Dmod.QrUpdL.SQL.Add('  Cor       int(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Qtd       int(11) NOT NULL DEFAULT 0');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('CtasItsCtas') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE CtasItsCtas (');
          Dmod.QrUpdL.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Nome  varchar(255) ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
(*
        end else if Lowercase(Tabela) = Lowercase('CtasResMes') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE CtasResMes (');
          Dmod.QrUpdL.SQL.Add('  SeqImp     integer(11)  DEFAULT 1,');
          Dmod.QrUpdL.SQL.Add('  Conta      integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Nome       varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Periodo    integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Tipo       integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Fator      double(15,6)  DEFAULT 0.000000,');
          Dmod.QrUpdL.SQL.Add('  ValFator   double(15,6)  DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  Devido     double(15,2)  DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  Pago       double(15,2)  DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  Diferenca  double(15,2)  DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  Acumulado  double(15,2)  DEFAULT 0.00');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
*)
        end else if Lowercase(Tabela) = Lowercase('Emeios') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Emeios (');
          Dmod.QrUpdL.SQL.Add('  Item     int(11)      DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  EntiCod  int(11)      DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ExtrCod  int(11)      DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ExtrInt1 int(11)      DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ExtrInt2 int(11)      DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Ativo    tinyint(1)   DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Emeio    varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  EntiNom  varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ExtrNom  varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  TipoStr  varchar(30) DEFAULT NULL');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else
        if Lowercase(Tabela) = Lowercase('Enderecos') then
        begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Enderecos (');
          Dmod.QrUpdL.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Linha1   varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Linha2   varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Linha3   varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Linha4   varchar(255) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Linha5   varchar(255) DEFAULT NULL');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else
        if Lowercase(Tabela) = Lowercase('Estq') then
        begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Estq (');
          Dmod.QrUpdL.SQL.Add('  Produto        Varchar(8)   NOT NULL default "00000000",');
          Dmod.QrUpdL.SQL.Add('  Descricao      Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  NomeForecedor  Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Fornecedor     int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  DocID          double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQ          double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqV          double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  AcumQ          double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  AcumV          double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Data           date,');
          Dmod.QrUpdL.SQL.Add('  Tipo           int(11) NOT NULL default "0"');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('EstqPerNG') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE EstqPerNG (');
          Dmod.QrUpdL.SQL.Add('  Controle       int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Produto        Varchar(8)   NOT NULL default "00000000",');
          Dmod.QrUpdL.SQL.Add('  Descricao      Varchar(100) NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  NomeMarca      Varchar(100) NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Marca          int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  DocID          double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQN         double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVN         double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  AcumQN         double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  AcumVN         double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqQG         double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EstqVG         double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  AcumQG         double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  AcumVG         double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  DataM          date,');
          Dmod.QrUpdL.SQL.Add('  DataN          date,');
          Dmod.QrUpdL.SQL.Add('  HoraM          varchar(5),');
          Dmod.QrUpdL.SQL.Add('  HoraN          varchar(5),');
          Dmod.QrUpdL.SQL.Add('  Tipo           int(11) NOT NULL default "0"');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('EstqEm') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE EstqEm (');
          Dmod.QrUpdL.SQL.Add('  Mercadoria     Varchar(8)   NOT NULL default "00000000",');
          Dmod.QrUpdL.SQL.Add('  NomeMercadoria Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  NomeFornecedor Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Fornecedor     int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  NomeGestor     Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Gestor         int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  PEntrada       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  VEntrada       double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  PVenda         double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  VVenda         double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  PDevolucao     double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  VDevolucao     double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  PRecompra      double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  VRecompra      double(15,4) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  PInicial       double(15,3) NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  VInicial       double(15,4) NOT NULL default "0"');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('EstqEmPQ') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE EstqEmPQ (');
          Dmod.QrUpdL.SQL.Add('  PQ             int(11)   NOT NULL default 0,');
          Dmod.QrUpdL.SQL.Add('  NomePQ         Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  CliInt         int(11)   NOT NULL default 0,');
          Dmod.QrUpdL.SQL.Add('  NomeCliInt     Varchar(50)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Fornecedor     int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  NomeFornecedor Varchar(100)  NOT NULL default "?",');
          Dmod.QrUpdL.SQL.Add('  Setor          int(11)      NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  NomeSetor      Varchar(30)  NOT NULL default "?",');

          Dmod.QrUpdL.SQL.Add('  QtdAnt         double(15,3) NOT NULL default "0.000",');
          Dmod.QrUpdL.SQL.Add('  ValAnt         double(15,2) NOT NULL default "0.00",');
          Dmod.QrUpdL.SQL.Add('  PrcAnt         double(15,4) NOT NULL default "0.0000",');

          Dmod.QrUpdL.SQL.Add('  QtdEnt         double(15,3) NOT NULL default "0.000",');
          Dmod.QrUpdL.SQL.Add('  ValEnt         double(15,2) NOT NULL default "0.00",');
          Dmod.QrUpdL.SQL.Add('  PrcEnt         double(15,4) NOT NULL default "0.0000",');

          Dmod.QrUpdL.SQL.Add('  QtdSai         double(15,3) NOT NULL default "0.000",');
          Dmod.QrUpdL.SQL.Add('  ValSai         double(15,2) NOT NULL default "0.00",');
          Dmod.QrUpdL.SQL.Add('  PrcSai         double(15,4) NOT NULL default "0.0000",');

          Dmod.QrUpdL.SQL.Add('  QtdFim         double(15,3) NOT NULL default "0.000",');
          Dmod.QrUpdL.SQL.Add('  ValFim         double(15,2) NOT NULL default "0.00",');
          Dmod.QrUpdL.SQL.Add('  PrcFim         double(15,4) NOT NULL default "0.0000",');

          Dmod.QrUpdL.SQL.Add('  ID_Rel         varchar(32) default "", ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (PQ, CliInt)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ExtratoCC') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ExtratoCC (');
          Dmod.QrUpdL.SQL.Add('  Linha      int(11) AUTO_INCREMENT , ');
          Dmod.QrUpdL.SQL.Add('  DataM      date             , ');
          Dmod.QrUpdL.SQL.Add('  Texto      varchar(100)     , ');
          Dmod.QrUpdL.SQL.Add('  Docum      varchar(30)      , ');
          Dmod.QrUpdL.SQL.Add('  Credi      double(15,2)     , ');
          Dmod.QrUpdL.SQL.Add('  Debit      double(15,2)     , ');
          Dmod.QrUpdL.SQL.Add('  Saldo      double(15,2)     , ');
          Dmod.QrUpdL.SQL.Add('  CartO      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  CartC      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  CartN      varchar(100)     , ');
          Dmod.QrUpdL.SQL.Add('  SdIni      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  TipoI      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  Unida      varchar(20)      , ');
          Dmod.QrUpdL.SQL.Add('  DebCr      char(1)          , ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Linha)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ExtratoCC2') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ExtratoCC2 (');
          Dmod.QrUpdL.SQL.Add('  DataE      date             , '); // Emissao
          Dmod.QrUpdL.SQL.Add('  DataV      date             , '); // Vencto
          Dmod.QrUpdL.SQL.Add('  DataQ      date             , '); // Quita��o
          Dmod.QrUpdL.SQL.Add('  DataX      date             , '); // Data considerada
          Dmod.QrUpdL.SQL.Add('  Texto      varchar(255)     , ');
          Dmod.QrUpdL.SQL.Add('  Docum      varchar(30)      , ');
          Dmod.QrUpdL.SQL.Add('  NotaF      varchar(30)      , ');
          Dmod.QrUpdL.SQL.Add('  Credi      double(15,2)     , ');
          Dmod.QrUpdL.SQL.Add('  Debit      double(15,2)     , ');
          Dmod.QrUpdL.SQL.Add('  Saldo      double(15,2)     , ');
          Dmod.QrUpdL.SQL.Add('  Ctrle      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  CtSub      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  ID_Pg      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  CartC      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  CartN      varchar(100)     , ');
          Dmod.QrUpdL.SQL.Add('  TipoI      int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  Sit        int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  DataCad    date             , ');
          Dmod.QrUpdL.SQL.Add('  UserCad    int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  DataAlt    date             , ');
          Dmod.QrUpdL.SQL.Add('  UserAlt    int(11)          , ');
          Dmod.QrUpdL.SQL.Add('  Codig      int(11)            ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Fiadores') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Fiadores (');
          Dmod.QrUpdL.SQL.Add('  Codigo     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Tipo       int(11)       ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('FPCalc') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPCalc (');
          Dmod.QrUpdL.SQL.Add('  FPFolhaCal int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  NomeEnti   varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  CodFunci   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Calcula    tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  FPFunciFer int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  MediaHE    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MediaCo    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MediaAd    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Salvou     tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('FPCalcAcum') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPCalcAcum (');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  Evento     int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  Acumulador int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Entidade, Evento, Acumulador)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('FPCalcBase') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPCalcBase (');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  Evento     int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  Eve_ValPer double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Eve_Meses  tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Eve_Incid  tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Eve_BasCod int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Eve_BasPer double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Conta      int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Cta_IncPer double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Cta_MesesR tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Natureza   char(1)     , ');
          Dmod.QrUpdL.SQL.Add('  Referencia double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Percentual double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Valor      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Incidencia tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Alteracao  tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Informacao tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Salvar     tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Visivel    tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Prioridade int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Fonte      tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Auto       tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  ProporciHT tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Unidade    varchar(15) , ');
          Dmod.QrUpdL.SQL.Add('  Eve_Ciclo  tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Eve_DtCicl Date        , ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Entidade, Evento)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('FPCalcEven') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPCalcEven (');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  Evento     int(11) DEFAULT "0" , ');
          Dmod.QrUpdL.SQL.Add('  Referencia double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Unidade    varchar(15) , ');
          Dmod.QrUpdL.SQL.Add('  Valor      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Alteracao  tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Info       tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Salva      tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Mostra     tinyint(1)  , ');
          //Dmod.QrUpdL.SQL.Add('  Acumula    tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Prioridade int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Auto       tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Entidade, Evento)');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('FPCalcFeri') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPCalcFeri (');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Item       int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Tipo       int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Auto       int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Referencia double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Valor      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        (*end else if Lowercase(Tabela) = Lowercase('FPCalcUser') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPCalcUser (');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Evento     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Referencia double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Valor      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Alteracao  tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Info       tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Salva      tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Mostra     tinyint(1)  , ');
          //Dmod.QrUpdL.SQL.Add('  Acumula    tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  Prioridade int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;*)
        end else if Lowercase(Tabela) = Lowercase('FPImpCab') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPImpCab (');
          Dmod.QrUpdL.SQL.Add('  FolhaCal   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Controle   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  NumFolha   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Empresa    int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  CodFunci   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Entidade   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Registro   varchar(10) , ');
          Dmod.QrUpdL.SQL.Add('  CodINSS    varchar(20) , ');
          Dmod.QrUpdL.SQL.Add('  DataAdm    Date        , ');
          Dmod.QrUpdL.SQL.Add('  CBO2002    varchar(8)  , ');
          Dmod.QrUpdL.SQL.Add('  NomeEnt    varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Depto      varchar(20) , ');
          Dmod.QrUpdL.SQL.Add('  NomeDepto  varchar(50) , ');
          Dmod.QrUpdL.SQL.Add('  Funcao     varchar(20) , ');
          Dmod.QrUpdL.SQL.Add('  NomeFuncao varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Periodo    int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  UltimFolha tinyint(1)  , ');
          Dmod.QrUpdL.SQL.Add('  DtIniPA    date        , ');
          Dmod.QrUpdL.SQL.Add('  DtFimPA    date        , ');
          Dmod.QrUpdL.SQL.Add('  DtIniPG    date        , ');
          Dmod.QrUpdL.SQL.Add('  DtFimPG    date        , ');
          Dmod.QrUpdL.SQL.Add('  DtIniPP    date        , ');
          Dmod.QrUpdL.SQL.Add('  DtFimPP    date        , ');
          Dmod.QrUpdL.SQL.Add('  DiasPG     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  DiasPP     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  DiasFI     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  MediaHE    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MediaCo    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MediaAd    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  SalFixo    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  BaseCal    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  DtRecPgto  date        , ');
           //
          Dmod.QrUpdL.SQL.Add('  Foto       varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('FPImpIts') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPImpIts (');
          Dmod.QrUpdL.SQL.Add('  Controle   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Linha      int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  CodiEvento int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  NomeEvento varchar(50) , ');
          Dmod.QrUpdL.SQL.Add('  Referencia double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ValorP     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ValorD     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else
          Result := ContinuaRecriaTabelaLocal(Tabela,Acao);
      end;
    except
      Geral.MB_Erro('Erro ao criar tabela local: '+Tabela+'!');
      raise;
    end;
  end;
end;
}

{
function TUCreate.ContinuaRecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
        if Lowercase(Tabela) = Lowercase('FPImpRPe') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE FPImpRPe (');
          Dmod.QrUpdL.SQL.Add('  Controle   int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  TotalP     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  TotalD     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  TotalL     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Base91     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Base93     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Base95     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Base96     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Base98     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Base99     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acum03     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acum17     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acum25     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acum26     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acum27     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acum28     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Ativo      tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Grafico') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Grafico (');
          Dmod.QrUpdL.SQL.Add('  Linha int(11), ');
          Dmod.QrUpdL.SQL.Add('  Texto varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Val01 double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Val02 double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Val03 double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Ativo tinyint(1) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('HistCliServ1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE HistCliServ1 (');
          Dmod.QrUpdL.SQL.Add('  Cliente     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  OSCliente   varchar(15) , ');
          Dmod.QrUpdL.SQL.Add('  NomeCliente varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  DataCad     date        , ');
          Dmod.QrUpdL.SQL.Add('  Placa       varchar(10) , ');
          Dmod.QrUpdL.SQL.Add('  Modelo      varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  HoraE       varchar(4)  , ');
          Dmod.QrUpdL.SQL.Add('  HoraS       varchar(4)  , ');
          Dmod.QrUpdL.SQL.Add('  kmE         double(15,1), ');
          Dmod.QrUpdL.SQL.Add('  kmS         double(15,1), ');
          Dmod.QrUpdL.SQL.Add('  Atendimento int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Historico   varchar(30) , ');
          Dmod.QrUpdL.SQL.Add('  Descricao   varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Preco       double(15,4), ');
          Dmod.QrUpdL.SQL.Add('  Qtde        double(15,3), ');
          Dmod.QrUpdL.SQL.Add('  Desconto    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Acrescimo   double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Debito      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Credito     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Saldo       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Conta       integer(11) , ');
          Dmod.QrUpdL.SQL.Add('  Controle    integer(11)   ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('HistComiss1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE HistComiss1 (');
          Dmod.QrUpdL.SQL.Add('  Cliente     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  NomeCliente varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  DataCad     date        , ');
          Dmod.QrUpdL.SQL.Add('  Placa       varchar(10) , ');
          Dmod.QrUpdL.SQL.Add('  Atendimento int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Genero      int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  Historico   varchar(30) , ');
          Dmod.QrUpdL.SQL.Add('  Descricao   varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Debito      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Credito     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Saldo       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Conta       integer(11) , ');
          Dmod.QrUpdL.SQL.Add('  Controle    integer(11) , ');
          Dmod.QrUpdL.SQL.Add('  Funci       integer(11) , ');
          Dmod.QrUpdL.SQL.Add('  NomeFunci   varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Modelo      varchar(100)  ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ImpBMZ') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ImpBMZ (');
          Dmod.QrUpdL.SQL.Add('  Linha       int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Etapa       int(11),     ');
          Dmod.QrUpdL.SQL.Add('  SubEtapa    int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Insumo      int(11),     ');
          Dmod.QrUpdL.SQL.Add('  NomeEtapa   varchar(255),');
          Dmod.QrUpdL.SQL.Add('  TempoP      int(11), ');
          Dmod.QrUpdL.SQL.Add('  TempoR      int(11), ');
          Dmod.QrUpdL.SQL.Add('  pH          double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  pH_Min      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  pH_Max      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Be          double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Be_Min      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Be_Max      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  GC_Min      double, ');
          Dmod.QrUpdL.SQL.Add('  GC_Max      double, ');
          Dmod.QrUpdL.SQL.Add('  Diluicao    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Graus       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Observacos  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  ObserProce  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Perc        double(15,3), ');
          Dmod.QrUpdL.SQL.Add('  Peso        double(15,3), ');
          Dmod.QrUpdL.SQL.Add('  Descricao   varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Porcent     double(15,3), ');
          Dmod.QrUpdL.SQL.Add('  CustoKg     double(15,4), ');
          Dmod.QrUpdL.SQL.Add('  CustoIt     double(15,3)');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ImpEquipe') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ImpEquipe (');
          Dmod.QrUpdL.SQL.Add('  Equipe      int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Imprime     tinyint(1)   ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ImpEquMes') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ImpEquMes (');
          Dmod.QrUpdL.SQL.Add('  Equipe      varchar(255),');
          Dmod.QrUpdL.SQL.Add('  Funci       varchar(255),');
          Dmod.QrUpdL.SQL.Add('  dd01        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd02        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd03        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd04        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd05        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd06        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd07        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd08        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd09        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd10        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd11        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd12        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd13        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd14        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd15        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd16        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd17        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd18        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd19        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd20        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd21        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd22        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd23        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd24        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd25        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd26        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd27        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd28        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd29        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd30        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  dd31        int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Periodo     varchar(100) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ImportLote') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ImportLote (');
          Dmod.QrUpdL.SQL.Add('  Tipo        int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Ordem       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Sit         int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Duplicado   int(11),      ');
          Dmod.QrUpdL.SQL.Add('  DEmiss      date,         ');
          Dmod.QrUpdL.SQL.Add('  DCompra     date,         ');
          Dmod.QrUpdL.SQL.Add('  DVence      date,         ');
          Dmod.QrUpdL.SQL.Add('  Deposito    date,         ');
          Dmod.QrUpdL.SQL.Add('  Comp        mediumint(3), ');
          Dmod.QrUpdL.SQL.Add('  Praca       mediumint(3), ');
          Dmod.QrUpdL.SQL.Add('  Banco       mediumint(3), ');
          Dmod.QrUpdL.SQL.Add('  Agencia     mediumint(4), ');
          Dmod.QrUpdL.SQL.Add('  Conta       varchar(20),  ');
          Dmod.QrUpdL.SQL.Add('  BAC         varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  CPF         varchar(15),  ');
          Dmod.QrUpdL.SQL.Add('  Emitente    varchar(50),  ');
          Dmod.QrUpdL.SQL.Add('  Cheque      int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Duplicata   varchar(12),  ');
          Dmod.QrUpdL.SQL.Add('  Valor       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Aberto      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  AberCH      int(11),      ');
          Dmod.QrUpdL.SQL.Add('  ValAcum     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Disponiv    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  OcorrA      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  OcorrT      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  QtdeCH      int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Rua         varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  Numero      int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Compl       varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  Birro       varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  Cidade      varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  UF          varchar(02),  ');
          Dmod.QrUpdL.SQL.Add('  CEP         int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Tel1        varchar(20),  ');
          Dmod.QrUpdL.SQL.Add('  Email       varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  IE          varchar(25),  ');
          Dmod.QrUpdL.SQL.Add('  Desco       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Bruto       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  CPF_2       varchar(15),  ');
          Dmod.QrUpdL.SQL.Add('  Nome_2      varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  RISCOEM     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  RISCOSA     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Rua_2       varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  Numero_2    int(5),       ');
          Dmod.QrUpdL.SQL.Add('  Compl_2     varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  IE_2        varchar(20),  ');
          Dmod.QrUpdL.SQL.Add('  Bairro_2    varchar(30),  ');
          Dmod.QrUpdL.SQL.Add('  Cidade_2    varchar(25),  ');
          Dmod.QrUpdL.SQL.Add('  UF_2        varchar(2),   ');
          Dmod.QrUpdL.SQL.Add('  CEP_2       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  TEL1_2      varchar(20),  ');
          Dmod.QrUpdL.SQL.Add('  Email_2     varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  StatusSPC   tinyint(1)    ');
          //Dmod.QrUpdL.SQL.Add('  DescriSPC   varchar(30)  ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ImportSGS') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ImportSGS (');
          Dmod.QrUpdL.SQL.Add('  Ordem       int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Status      int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Grade       int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Cor         int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Tam         int(11),     ');
          Dmod.QrUpdL.SQL.Add('  Qtde        double(15,2),');
          Dmod.QrUpdL.SQL.Add('  Preco       double(15,2),');
          Dmod.QrUpdL.SQL.Add('  PrOri       double(15,2),');
          Dmod.QrUpdL.SQL.Add('  PrDef       double(15,2),');
          Dmod.QrUpdL.SQL.Add('  Valor       double(15,2),');
          Dmod.QrUpdL.SQL.Add('  VlOri       double(15,2),');
          Dmod.QrUpdL.SQL.Add('  VlDef       double(15,2) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Imprimir1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Imprimir1 (');
          Dmod.QrUpdL.SQL.Add('  Campo  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Valor  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Prefixo  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Sufixo  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Substituicao varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Substitui  tinyint(1), ');
          Dmod.QrUpdL.SQL.Add('  Nulo  tinyint(1), ');
          Dmod.QrUpdL.SQL.Add('  Forma  integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Conta  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DBand  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DTopo  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DMEsq  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DComp  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DAltu  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_N  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_I  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_U  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_T  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_A  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Linha  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Colun  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Pagin  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Tipo  int(11) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Imprimir2') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Imprimir2 (');
          Dmod.QrUpdL.SQL.Add('  Controle int(11), ');
          Dmod.QrUpdL.SQL.Add('  Band int(11), ');
          //
          Dmod.QrUpdL.SQL.Add('  Codigo varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  Refere varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Descri varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  CFiscal varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  SitTrib varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Unidade varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  Quanti double(15,3), ');
          Dmod.QrUpdL.SQL.Add('  ValUni double(15,4), ');
          Dmod.QrUpdL.SQL.Add('  Total double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ICMS double(4,2), ');
          Dmod.QrUpdL.SQL.Add('  IPI double(4,2), ');
          Dmod.QrUpdL.SQL.Add('  ValIPI double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  CFOP varchar(20), ');
          //
          Dmod.QrUpdL.SQL.Add('  Prefixo varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Sufixo varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Substituicao varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Substitui  tinyint(1), ');
          Dmod.QrUpdL.SQL.Add('  Nulo  tinyint(1), ');
          Dmod.QrUpdL.SQL.Add('  Conta  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Forma  integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Tipo   integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DBand  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DTopo  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DMEsq  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DComp  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  DAltu  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_N  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_I  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_U  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_T  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Set_A  Integer(11), ');
          Dmod.QrUpdL.SQL.Add('  Pagina  int(11) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Imprimir3') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Imprimir3 (');
          Dmod.QrUpdL.SQL.Add('  Controle int(11), ');
          Dmod.QrUpdL.SQL.Add('  Band int(11), ');
          Dmod.QrUpdL.SQL.Add('  Parcela varchar(4), ');
          Dmod.QrUpdL.SQL.Add('  Prefixo varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Sufixo varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Valor  double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  Vencto date, ');
          Dmod.QrUpdL.SQL.Add('  Pagina  int(11) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('InfoSeq') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE InfoSeq (');
          Dmod.QrUpdL.SQL.Add('  Linha int(11), ');
          Dmod.QrUpdL.SQL.Add('  Item  int(11), ');
          Dmod.QrUpdL.SQL.Add('  Local varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Acao varchar(255) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('LctoEdit') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE LctoEdit (');
          Dmod.QrUpdL.SQL.Add('  Controle   int(11) NOT NULL DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Sub        int(11), ');
          Dmod.QrUpdL.SQL.Add('  Carteira   int(11), ');
          Dmod.QrUpdL.SQL.Add('  Descricao  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Data       date, ');
          Dmod.QrUpdL.SQL.Add('  Vencimento date, ');
          //Dmod.QrUpdL.SQL.Add('  Compensado date, ');
          Dmod.QrUpdL.SQL.Add('  MultaVal   double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  JurosVal   double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ValorOri   double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ValorPgt   double(15,2) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Lista1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Lista1 (');
          Dmod.QrUpdL.SQL.Add('  Nome  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Tipo  int(11) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Lista2') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Lista2 (');
          Dmod.QrUpdL.SQL.Add('  Codigo   int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Subitm   int(11) DEFAULT 0');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('LocCtrl') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE LocCtrl (');
          Dmod.QrUpdL.SQL.Add('  CartConcilia  int(11) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('LReA') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE LReA (');
          Dmod.QrUpdL.SQL.Add('  Codigo   integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Data     date,');
          Dmod.QrUpdL.SQL.Add('  NomeCli  varchar(100) DEFAULT "?" ,');
          Dmod.QrUpdL.SQL.Add('  Lote     integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  NomeMes  varchar(30)  DEFAULT "?" ,');
          Dmod.QrUpdL.SQL.Add('  Mes      integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  NF       integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Total    double(15,2) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Dias     double(15,6) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC0      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC1      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC2      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC3      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC4      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC5      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TC6      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV0      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV1      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV2      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV3      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV4      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV5      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VV6      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  ETC      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  EVV      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TTC      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TVV      double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TTC_T    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TTC_J    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TRC_T    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  TRC_J    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  ALL_T    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  ALL_J    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  ddVal    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  ddLiq    double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  QtdeDocs int(11)      DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  MediaDoc double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  ISS_Val  double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  PIS_Val  double(15,4) DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  COFINS_Val double(15,4) DEFAULT 0 ,');
          Dmod.QrUpdL.SQL.Add('  Impostos double(15,4) DEFAULT 0  ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('MercEstq') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE MercEstq (');
          Dmod.QrUpdL.SQL.Add('  Mercadoria     Varchar(14)   NOT NULL default "00000000000000",');
          Dmod.QrUpdL.SQL.Add('  Fornecedor     Double        NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Gestor         Double        NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Tipo           Varchar(30)   NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  NomeMercadoria Varchar(100)  NOT NULL default " ",');
          Dmod.QrUpdL.SQL.Add('  NomeFornecedor Varchar(100)  NOT NULL default " ",');
          Dmod.QrUpdL.SQL.Add('  NomeGestor     Varchar(100)  NOT NULL default " ",');
          Dmod.QrUpdL.SQL.Add('  EQuant         double(15,3)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EValor         double(15,4)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  SQuant         double(15,3)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  SValor         double(15,4)  NOT NULL default "0"');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('MercMovs') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE MercMovs (');
          Dmod.QrUpdL.SQL.Add('  Tipo           Varchar(30)   NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Lancto         double        NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Controle       double        NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EQuant         double(15,3)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  EValor         double(15,4)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  SQuant         double(15,3)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  SValor         double(15,4)  NOT NULL default "0",');
          Dmod.QrUpdL.SQL.Add('  Data           Date');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
(*
        end else if Lowercase(Tabela) = Lowercase('MoviPQ') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE MoviPQ (');
          Dmod.QrUpdL.SQL.Add('  Insumo integer(11)   DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomePQ varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  NomeFO varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  NomeCI varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  NomeSE varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  AntPes double(15,3) DEFAULT 0.000,');
          Dmod.QrUpdL.SQL.Add('  AntVal double(15,2) DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  InnPes double(15,3) DEFAULT 0.000,');
          Dmod.QrUpdL.SQL.Add('  InnVal double(15,2) DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  OutPes double(15,3) DEFAULT 0.000,');
          Dmod.QrUpdL.SQL.Add('  OutVal double(15,2) DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  BalPes double(15,3) DEFAULT 0.000,');
          Dmod.QrUpdL.SQL.Add('  BalVal double(15,2) DEFAULT 0.00,');
          Dmod.QrUpdL.SQL.Add('  FimPes double(15,3) DEFAULT 0.000,');
          Dmod.QrUpdL.SQL.Add('  FimVal double(15,2) DEFAULT 0.00');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
*)
        end else if Lowercase(Tabela) = Lowercase('MinhaEtiq') then
        begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE MinhaEtiq (');
          Dmod.QrUpdL.SQL.Add('  Nome   varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Conta  integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Texto1 varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Texto2 varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Texto3 varchar(100)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Texto4 varchar(100)  DEFAULT NULL');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end
        else if Lowercase(Tabela) = Lowercase('Movireco') then
        begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Movireco (');
          Dmod.QrUpdL.SQL.Add('  EhTotal    int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomeForI   varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ForneceI   integer(11)  DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Gastos     double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  PesoQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  PesoVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Entradas   double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  SdoAnt     double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  SdoDep     double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ColAQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ColAVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  TranQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  TranVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  MoviQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  MoviVal    double(15,2) DEFAULT NULL');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Movireco1') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Movireco1 (');
          Dmod.QrUpdL.SQL.Add('  NomeForI   varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ForneceI   integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  NomeProd   varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Produto    integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  PesoQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  PesoVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ColAQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ColAVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  TranQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  TranVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  MoviQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  MoviVal    double(15,2) DEFAULT NULL ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Movireco2') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Movireco2 (');
          Dmod.QrUpdL.SQL.Add('  NomeProd   varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Produto    integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  VendQtd    double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  VendVal    double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  VendCus    double(15,2) DEFAULT NULL');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('MPInRat') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE MPInRat (');
          Dmod.QrUpdL.SQL.Add('  Controle   int(11)      AUTO_INCREMENT,');
          Dmod.QrUpdL.SQL.Add('  ProcedeCod int(11)      DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ProcedeNom varchar(100) DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Pecas      double(15,1) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PecasNF    double(15,1) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PNF        double(15,3) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PNF_Fat    double(15,3) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PLE        double(15,3) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PDA        double(15,3) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Preco      double(15,6) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Custo      double(15,2) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Frete      double(15,2) DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  NF         int(11)      DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Conheci    int(11)      DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Emissao    date         DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  Vencto     date         DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  EmisFrete  date         DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('  VctoFrete  date         DEFAULT 0, ');
          Dmod.QrUpdL.SQL.Add('PRIMARY KEY  (Controle)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('MPInRes') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE MPInRes (');
          Dmod.QrUpdL.SQL.Add('  Marca      varchar(30) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Tipo       integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Tipific    integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Procede    varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  MPIn       integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Pesagem    integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Pecas      double(15,1) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  PLE        double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  PNF        double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  M2         double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Data       Date                     ,');
          Dmod.QrUpdL.SQL.Add('  Setor      integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Valor      double(15,2) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  PesoT      double(15,3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Fulao      varchar(15)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Custo      double(15,6) DEFAULT NULL ');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('NF1Fat') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE NF1Fat (');
          Dmod.QrUpdL.SQL.Add('  Controle   integer(11)  NOT NULL AUTO_INCREMENT,');
          Dmod.QrUpdL.SQL.Add('  NumeroA    varchar(30)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  DataA      varchar(30)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ValorA     double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  NumeroB    varchar(30)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  DataB      varchar(30)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ValorB     double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  NumeroC    varchar(30)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  DataC      varchar(30)  DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ValorC     double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('PRIMARY KEY  (Controle)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('NF1Pro') then begin
          Dmod.QrUpdL.SQL.Clear;
          Dmod.QrUpdL.SQL.Add('CREATE TABLE NF1Pro (');
          Dmod.QrUpdL.SQL.Add('  Controle   integer(11)  NOT NULL AUTO_INCREMENT,');
          Dmod.QrUpdL.SQL.Add('  Codigo     varchar( 30) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Produto    varchar(100) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  ClasFisc   varchar(  8) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  SitTrib    varchar(  3) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Unidade    varchar(  6) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Qtde       double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  Preco      double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  Desc_Per   double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  Valor      double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  ICMS_Per   double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  ICMS_Val   double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  IPI_Per    double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('  IPI_Val    double(15,2) DEFAULT NULL, ');
          Dmod.QrUpdL.SQL.Add('PRIMARY KEY  (Controle)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('PagRec1') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE PagRec1 (');
          Dmod.QrUpdL.SQL.Add('  Data          date, ');
          Dmod.QrUpdL.SQL.Add('  DataDoc       date, ');
          Dmod.QrUpdL.SQL.Add('  Vencimento    date, ');
          Dmod.QrUpdL.SQL.Add('  TERCEIRO      double DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  NOME_TERCEIRO varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  NOMEVENCIDO   varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  NOMECONTA     varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Descricao     varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Duplicata     varchar( 15), ');
          Dmod.QrUpdL.SQL.Add('  CtrlPai       double, ');
          Dmod.QrUpdL.SQL.Add('  Tipo          double, ');
          Dmod.QrUpdL.SQL.Add('  ID_Pgto       double, ');
          Dmod.QrUpdL.SQL.Add('  CtrlIni       double, ');
          Dmod.QrUpdL.SQL.Add('  Documento     double, ');
          Dmod.QrUpdL.SQL.Add('  NotaFiscal    double, ');
          Dmod.QrUpdL.SQL.Add('  Controle      double, ');
          Dmod.QrUpdL.SQL.Add('  Valor         double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MoraDia       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  PAGO_REAL     double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  PENDENTE      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ATUALIZADO    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MULTA_REAL    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ATRAZODD      double ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('PagRec2') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE PagRec2 (');
          Dmod.QrUpdL.SQL.Add('  Data  date, ');
          Dmod.QrUpdL.SQL.Add('  NOMEVENCIDO  varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  NOMECONTA    varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Descricao    varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Documento    double, ');
          Dmod.QrUpdL.SQL.Add('  NotaFiscal   double, ');
          Dmod.QrUpdL.SQL.Add('  Controle     double NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  ID_Pgto      double NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Valor        double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MoraDia      double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  PAGO_REAL    double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ATUALIZADO   double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  MULTA_REAL   double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  ATRAZODD     double, ');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (ID_Pgto,Controle)');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Palet') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Palet (');
          Dmod.QrUpdL.SQL.Add('  Palet          int(11), ');
          Dmod.QrUpdL.SQL.Add('  Data           date, ');
          Dmod.QrUpdL.SQL.Add('  OS             int(11), ');
          Dmod.QrUpdL.SQL.Add('  CliInt         int(11), ');
          Dmod.QrUpdL.SQL.Add('  Procede        int(11), ');
          Dmod.QrUpdL.SQL.Add('  Lote           varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  Marca          varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  NOMECLIINT     varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  NOMEPROCEDE    varchar(255), ');
          Dmod.QrUpdL.SQL.Add('  Pecas          double, ');
          Dmod.QrUpdL.SQL.Add('  Saida1         double, ');
          Dmod.QrUpdL.SQL.Add('  Saida2         double, ');
          Dmod.QrUpdL.SQL.Add('  Saldo          double ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Parcpagtos') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Parcpagtos (');
          Dmod.QrUpdL.SQL.Add('  Parcela  int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Doc      bigint(20) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Data     date,');
          Dmod.QrUpdL.SQL.Add('  Credito  double(15,2) NOT NULL DEFAULT "0",');
          Dmod.QrUpdL.SQL.Add('  Debito   double(15,2) NOT NULL DEFAULT "0",');
          Dmod.QrUpdL.SQL.Add('  Mora     double(15,2) NOT NULL DEFAULT "0",');
          Dmod.QrUpdL.SQL.Add('  Multa    double(15,2) NOT NULL DEFAULT "0",');
          Dmod.QrUpdL.SQL.Add('  ICMS_V   double(15,2) NOT NULL DEFAULT "0",');
          Dmod.QrUpdL.SQL.Add('  Duplicata varchar(30) ,');
          Dmod.QrUpdL.SQL.Add('  Descricao varchar(100),');
          Dmod.QrUpdL.SQL.Add('  PRIMARY KEY (Parcela)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('PagtosEnti') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE PagtosEnti (');
          Dmod.QrUpdL.SQL.Add('  Tipo     int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Entidade int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Carga    int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Valor    double(15,2) NOT NULL DEFAULT "0.00"');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('PrevBAB') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE PrevBAB (');
          Dmod.QrUpdL.SQL.Add('  Conta     int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NOMECONTA varchar(100) DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  PrevBaI   int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  PrevBaC   int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Valor     double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  LastM     varchar(8)  DEFAULT "",');
          Dmod.QrUpdL.SQL.Add('  Last1     double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Last6     double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Texto     varchar(40) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  Adiciona  tinyint(1) DEFAULT 0');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('PrevBAN') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE PrevBAN (');
          Dmod.QrUpdL.SQL.Add('  Codigo   int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Valor    double(15,2)  DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Texto    varchar(40) DEFAULT NULL,');
          Dmod.QrUpdL.SQL.Add('  SitCobr  int(11) DEFAULT 1,');
          Dmod.QrUpdL.SQL.Add('  Parcelas int(11) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ParcPerI int(11) DEFAULT 1,');
          Dmod.QrUpdL.SQL.Add('  ParcPerF int(11) DEFAULT 12,');
          Dmod.QrUpdL.SQL.Add('  InfoParc tinyint(1) DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Adiciona tinyint(1) DEFAULT 0');
          Dmod.QrUpdL.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ProtoMail') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ProtoMail (');
          Dmod.QrUpdL.SQL.Add('  Bloqueto     double(20,0)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Depto_Cod    integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Depto_Txt    varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Entid_Cod    integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Entid_Txt    varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Taref_Cod    integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Taref_Txt    varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Deliv_Cod    integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Deliv_Txt    varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  DataE        Date, ');
          Dmod.QrUpdL.SQL.Add('  DataE_Txt    varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  DataD        Date, ');
          Dmod.QrUpdL.SQL.Add('  DataD_Txt    varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Protocolo    integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  ProtoLote    integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  NivelEmail   tinyint(1)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  NivelDescr   varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  RecipItem    int(11), ');
          Dmod.QrUpdL.SQL.Add('  RecipEmeio   varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  RecipNome    varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  RecipProno   varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  Vencimento   date  NOT NULL DEFAULT "0000-00-00", ');
          Dmod.QrUpdL.SQL.Add('  Valor        double(15,2)  NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Condominio   varchar(255)  NOT NULL DEFAULT "??", ');
          Dmod.QrUpdL.SQL.Add('  PreEmeio     int(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  IDEmeio      int(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Item         int(11)  NOT NULL DEFAULT "0" ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Recibos') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Recibos (');
          Dmod.QrUpdL.SQL.Add('  Emitente     integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Beneficiario integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Valor        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  NumRecibo    varchar(30), ');
          Dmod.QrUpdL.SQL.Add('  Referente    Text, ');
          Dmod.QrUpdL.SQL.Add('  Data         Date');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ResLiqFac') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ResLiqFac (');
          Dmod.QrUpdL.SQL.Add('  NOMECLIENTE  varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  NOMEMES      varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  Mes          integer(11)  DEFAULT 0   ,');
          Dmod.QrUpdL.SQL.Add('  Data         date, ');
          Dmod.QrUpdL.SQL.Add('  Lote         integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Codigo       integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  PROPRIO      double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  TXCOMPRAT    double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  VALVALOREMT  double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  MARGEM_TODOS double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  REPAS_V      double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  MARGEM_BRUTA double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  NREP_T       double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  PRO_COL      double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  MARGEM_COL   double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  PIS_T_VAL    double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  ISS_VAL      double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  COFINS_T_VAL double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  TOTAL        double(15,2) NOT NULL DEFAULT "0.00" ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
(*
        end else if Lowercase(Tabela) = Lowercase('ResMes') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ResMes (');
          Dmod.QrUpdL.SQL.Add('  Exclusivo    char(1) NOT NULL DEFAULT "F",');
          Dmod.QrUpdL.SQL.Add('  TipoAgrupa   int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  OrdemLista   int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  SubOrdem     int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  SubGrupo     int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomeSubGrupo varchar(255) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomeConta    varchar(255) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  AnoAn        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Pendencias   double(15,2) NOT NULL DEFAULT "0.00"');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
*)
(*
        end else if Lowercase(Tabela) = Lowercase('ResPenM') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ResPenM (');
          Dmod.QrUpdL.SQL.Add('  Grupo        int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomeConta    varchar(255) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  AnoAn        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00",');
          Dmod.QrUpdL.SQL.Add('  Pendencias   double(15,2) NOT NULL DEFAULT "0.00"');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
*)
        end else if Lowercase(Tabela) = Lowercase('ResMesTeach') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ResMesTeach (');
          Dmod.QrUpdL.SQL.Add('  Tipo         varchar(10) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Codigo       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Sala         int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Professor    int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CargaDesp    int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CuSala       double(15,10) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CuProfessor  double(15,10) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ToSala       double(15,4 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ToProfessor  double(15,4 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ReceitaBase  double(15,4 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CargaRec     int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  ToReceita    double(15,4 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  SALDO        double(15,4 ) NOT NULL DEFAULT 0');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('ResWB') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE ResWB (');
          Dmod.QrUpdL.SQL.Add('  Tipifi       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomTip       varchar(20) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Ordena       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CodCli       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomCli       varchar(100) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  CodGen       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  NomGen       varchar(100) NOT NULL DEFAULT "?",');
          Dmod.QrUpdL.SQL.Add('  Credit       double(15,2 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Impost       double(15,2 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  SaldoT       double(15,2 ) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  SaldoA       double(15,2 ) NOT NULL DEFAULT 0');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('RGB') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE RGB (');
          Dmod.QrUpdL.SQL.Add('  Pixel       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Color       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CorR        int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CorG        int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  CorB        int(11) NOT NULL DEFAULT 0');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('Risco') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Risco (');
          Dmod.QrUpdL.SQL.Add('  Cliente      integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Controle     integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Tipo         integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Banco        integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Agencia      integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Conta        varchar(30)  ,');
          Dmod.QrUpdL.SQL.Add('  Duplicata    varchar(50)  ,');
          Dmod.QrUpdL.SQL.Add('  Cheque       integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  DEmissao     date, ');
          Dmod.QrUpdL.SQL.Add('  DCompra      date, ');
          Dmod.QrUpdL.SQL.Add('  DVence       date, ');
          Dmod.QrUpdL.SQL.Add('  DDeposito    date, ');
          Dmod.QrUpdL.SQL.Add('  DDevol1      date, ');
          Dmod.QrUpdL.SQL.Add('  DDevol2      date, ');
          Dmod.QrUpdL.SQL.Add('  UltPagto     date, ');
          Dmod.QrUpdL.SQL.Add('  Valor        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Taxas        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Saldo        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Juros        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Desco        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Pago         double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Atual        double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Devolvido    double(15,2) NOT NULL DEFAULT "0.00", ');
          Dmod.QrUpdL.SQL.Add('  Emitente     varchar(100)  ,');
          Dmod.QrUpdL.SQL.Add('  CPF          varchar(30)  ,');
          Dmod.QrUpdL.SQL.Add('  Alinea1      integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Alinea2      integer(11)  NOT NULL DEFAULT "0", ');
          Dmod.QrUpdL.SQL.Add('  Status       varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Historico    varchar(100) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        (*end else if Lowercase(Tabela) = Lowercase('Rubricas') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE Rubricas (');
          Dmod.QrUpdL.SQL.Add('  Linha       int(11) NOT NULL DEFAULT 0,');
          Dmod.QrUpdL.SQL.Add('  Nome1       varchar(255) ,');
          Dmod.QrUpdL.SQL.Add('  CPF1        varchar(30) ,');
          Dmod.QrUpdL.SQL.Add('  Nome2       varchar(255) ,');
          Dmod.QrUpdL.SQL.Add('  CPF2        varchar(30)');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;*)
        end else if Lowercase(Tabela) = Lowercase('SaldoCli') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE SaldoCli (');
          Dmod.QrUpdL.SQL.Add('  Cliente     int(11)     , ');
          Dmod.QrUpdL.SQL.Add('  NomeCliente varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Tel1 varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Tel2 varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Tel3 varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Cel  varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  Valor       double(15,2) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
(*
        end else if Lowercase(Tabela) = Lowercase('SetSetores') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE SetSetores (');
          Dmod.QrUpdL.SQL.Add('  Codigo       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Setado       int(11)       ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
*)
        end else if Lowercase(Tabela) = Lowercase('SdoNiveis') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE SdoNiveis (');
          Dmod.QrUpdL.SQL.Add('  Nivel        int(11),      ');
          Dmod.QrUpdL.SQL.Add('  CodPla       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  CodCjt       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  CodGru       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  CodSgr       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Genero       int(11),      ');
          Dmod.QrUpdL.SQL.Add('  Ordena       varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  NomeGe       varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  NomeNi       varchar(20), ');
          Dmod.QrUpdL.SQL.Add('  SumMov       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  SdoAnt       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  SumCre       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  SumDeb       double(15,2), ');
          Dmod.QrUpdL.SQL.Add('  SdoFim       double(15,2), ');
          // usado para cadastrar
          Dmod.QrUpdL.SQL.Add('  Ctrla        tinyint(1),    ');
          Dmod.QrUpdL.SQL.Add('  Seleci       tinyint(1)    ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else
        if Lowercase(Tabela) = Lowercase('TxaAdmVal') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE TxaAdmVal (');
          Dmod.QrUpdL.SQL.Add('  Nivel         int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Codigo        int(11),       ');
          Dmod.QrUpdL.SQL.Add('  TaxaAdm       double(15,4),  ');
          Dmod.QrUpdL.SQL.Add('  BaseVal       double(15,2),  ');
          Dmod.QrUpdL.SQL.Add('  TaxaVal       double(15,2),  ');
          Dmod.QrUpdL.SQL.Add('  Texto         varchar(100),  ');
          Dmod.QrUpdL.SQL.Add('  Ordem         int(11),        ');
          Dmod.QrUpdL.SQL.Add('  Ativo         int(11) NOT NULL DEFAULT "0"');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else if Lowercase(Tabela) = Lowercase('TurmasT') then begin
          Dmod.QrUpdL.SQL.Add('CREATE TABLE TurmasT (');
          Dmod.QrUpdL.SQL.Add('  Controle      int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Ano           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Refere        varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  NomeGrupo     varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  NomeTurma     varchar(50), ');
          Dmod.QrUpdL.SQL.Add('  NomeInstrutor varchar(100), ');
          Dmod.QrUpdL.SQL.Add('  Lotacao       int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Dom           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Seg           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Ter           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Qua           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Qui           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Sex           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Sab           int(11),       ');
          Dmod.QrUpdL.SQL.Add('  Situacao      varchar(20) ');
          Dmod.QrUpdL.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
          Dmod.QrUpdL.ExecSQL;
        end else raise
          EAbort.Create('A cria��o da tabela local "' +
            Tabela + '" n�o foi implementada!');
        Result := True;
end;
}

{
function TUCreate.ReabreQueryLocal(Query: TMySQLQuery; Tab: String; Avisa:
Boolean): Boolean;
var
  QtTemp: TmySQLQuery;
  Encontrado: Boolean;
begin
  Encontrado := False;
  QtTemp := TmySQLQuery.Create(Dmod);
  QtTemp.DataBase := Dmod.MyLocDataBase;
  QtTemp.SQL.Add('SHOW TABLES');
  UnDmkDAC_PF.AbreQuery(QtTemp, Dmod.MyLocDataBase);
  while not QtTemp.Eof do
  begin
    if Lowercase(QtTemp.Fields[0].AsString) = Lowercase(Tab) then
    begin
      Encontrado := True;
      Query.Close;
      UnDmkDAC_PF.AbreQueryApenas(Query);
      Break;
    end;
    QtTemp.Next;
  end;
  if not Encontrado and Avisa then
    Geral.MB_Erro('Tabela ' + Tab + ' n�o existe!');
  QtTemp.Close;
  QtTemp.Free;
  Result := Encontrado;
end;

function TUCreate.AtualizaTabelaControleLocal(CampoInt, CampoStr, CampoFloat:
  String; ValInt: Integer; ValStr: String; ValFloAt: Double): Boolean;
var
  Linha1, LinhaI, LinhaS, LinhaD: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrAuxL.SQL.Clear;
    Dmod.QrAuxL.SQL.Add('SELECT * FROM locctrl ');
    UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAuxL);
    //
    if Dmod.QrAuxL.RecordCount > 0 then
      Linha1 := 'UPDATE locctrl SET '
    else
      Linha1 := 'INSERT INTO locctrl SET ';
    //
    if Trim(CampoInt) = '' then LinhaI := '' else
    LinhaI := CampoInt + '='+IntToStr(ValInt);
    //
    if Trim(CampoStr) = '' then LinhaS := '' else
    LinhaS := CampoStr + '='+ValStr;
    //
    if Trim(CampoFloat) = '' then LinhaD := '' else
      LinhaD := CampoFloat + '=:P0';
    //
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add(Linha1);
    Dmod.QrUpdL.SQL.Add(LinhaI);
    Dmod.QrUpdL.SQL.Add(LinhaS);
    Dmod.QrUpdL.SQL.Add(LinhaD);
    if (Trim(CampoFloat) <> '') then
      Dmod.QrUpdL.Params[0].AsFloat := ValFloat;
    Dmod.QrUpdL.ExecSQL;
    Result := True;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;
}
{$Else}

{
function TUCreate.ReabreQueryLocal(Query: TMySQLQuery; Tab: String; Avisa:
           Boolean): Boolean;
begin

end;


function TUCreate.AtualizaTabelaControleLocal(CampoInt, CampoStr, CampoFloat: String;
           ValInt: Integer; ValStr: String; ValFloAt: Double): Boolean;
begin

end;

function TUCreate.GerenciaTabelaLocal(Tabela: String; Acao: TAcaoCreate): Boolean;
begin

end;
}

{$EndIf}

end.

