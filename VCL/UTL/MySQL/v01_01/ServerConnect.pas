unit ServerConnect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkGeral, dmkEdit, UnDmkProcFunc,
  dmkImage, UnDmkEnums;

type
  TFmServerConnect = class(TForm)
    Panel1: TPanel;
    RGServidor: TRadioGroup;
    EdIP: TdmkEdit;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdPesq: TEdit;
    ProgressBar1: TProgressBar;
    Edit1: TEdit;
    Label3: TLabel;
    Memo1: TRichEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdIPExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    function StatusServico: Integer;
    procedure AtivarServico;
    procedure DesativarServico;
    function  LeMyIni: String;
    function  EscreveMyIni(Texto: String): Boolean;
    procedure MostraFormMyInis();
  public
    { Public declarations }
  end;

  var
  FmServerConnect: TFmServerConnect;

implementation

uses UnMyObjects, UnInternalConsts, ServiceManager, inifiles, MyInis, MyDBCheck,
  ZCF2;

{$R *.DFM}

procedure TFmServerConnect.BtOKClick(Sender: TObject);
var
  Status: Integer;
  Drive, IniAtivo, IniNovo1, IniNovo2, IniNovo0: String;
  n: Double;
begin
  Memo1.Lines.Clear;
  if RGServidor.ItemIndex = -1 then
  begin
    Memo1.SelAttributes.Color := clRed;
    Memo1.Lines.Add('Tipo de conex�o n�o informado!');
    Geral.MB_Aviso('Informe o tipo de conex�o!');
    Exit;
  end;
  VAR_SERVIDOR := RGServidor.ItemIndex + 1;
  case VAR_SERVIDOR of
    1: VAR_IP := EdIP.Text;
    2: VAR_IP := '127.0.0.1';
    else VAR_IP := '127.0.0.1';
  end;
  Memo1.SelAttributes.Color := clBlue;
  Memo1.Lines.Add('IP para conex�o: '+VAR_IP);
  //
  Geral.WriteAppKeyLM2('IPServer', Application.Title, EdIP.Text, ktString);
  //
  Status   := StatusServico;
  IniNovo1 := Geral.ReadAppKeyLM('MySQLData1', Application.Title, ktString, '');
  IniNovo2 := Geral.ReadAppKeyLM('MySQLData2', Application.Title, ktString, '');
  //
  if (IniNovo1 = '') or (IniNovo2 = '') then
  begin
    MostraFormMyInis();
    Exit;
  end;
  case RGServidor.ItemIndex of
    0: IniNovo0 := dmkPF.InverteBarras(IniNovo2);
    1: IniNovo0 := dmkPF.InverteBarras(IniNovo1);
  end;
  Memo1.SelAttributes.Color := clBlue;
  Memo1.Lines.Add('Caminho do MySQL: '+IniNovo0);
  if Length(IniNovo0) < 2 then
  begin
    Memo1.SelAttributes.Color := clRed;
    Memo1.Lines.Add('Caminho do MySQL inv�lido!');
    Exit;
  end;
  Drive := Copy(IniNovo0, 1, 1);
  n := dmkPF.GetDiskSerialNumber(Drive);
  if n < 2 then
  begin
    Memo1.SelAttributes.Color := clRed;
    Memo1.Lines.Add('O drive indicado para o caminho do MySQL n�o foi reconhecido!');
    Memo1.Lines.Add('Verifique se o drive "'+ Drive+':\" est� instalado!');
    Exit;
  end;
  if not DirectoryExists(IniNovo0) then
  begin
    Memo1.SelAttributes.Color := clRed;
    Memo1.Lines.Add('O caminho do MySQL n�o foi encontrado!');
    Memo1.Lines.Add('Verifique se o diret�rio "'+ IniNovo0+'" realmente existe!');
    Exit;
  end;
  //////////////////////////////////////////////////////////////////////////////
  if Status = 4 then
  begin
    IniAtivo := LeMyIni;
    if IniAtivo <> IniNovo0 then DesativarServico;
  end;
  EscreveMyIni(IniNovo0);
  AtivarServico;
  if StatusServico = 4 then Close;
end;

procedure TFmServerConnect.BtSaidaClick(Sender: TObject);
begin
  VAR_SERVIDOR := 0;
  ZZTerminate := True;
  Application.Terminate;
  Close;
end;

procedure TFmServerConnect.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmServerConnect.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmServerConnect.EdIPExit(Sender: TObject);
begin
  if not dmkPF.VerificaIPSintaxe(EdIP.Text, True) then EdIP.SetFocus;
end;

procedure TFmServerConnect.FormCreate(Sender: TObject);
var
  Servico: String;
begin
  ImgTipo.SQLType := stLok;
  //
  Servico := Geral.ReadAppKeyLM('MySQL_Service', Application.Title, ktString, '');
  //
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL41\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL41';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL4\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL4';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL51\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL51';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL5\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL5';
  if servico = '' then
    if Geral.ReadKey('ImagePath', 'SYSTEM\CurrentControlSet\Services\MySQL\',
      ktString, '', HKEY_LOCAL_MACHINE) <> '' then Servico := 'MySQL';
  EdPesq.Text := Servico;
  //
  EdIP.Text := VAR_IP;
end;

function TFmServerConnect.StatusServico: Integer;
var
  Servico: TServiceManager;
begin
  Result := -1;
  Servico := TServiceManager.Create;
  try
    if Servico.Connect  then
      if Servico.OpenServiceConnection(PChar(EdPesq.Text)) then
      begin
        Result := Servico.GetStatus;
        Edit1.Text := Servico.GetStatusText(Result);
      end;
  finally
    Servico.Free;
  end;
end;

procedure TFmServerConnect.AtivarServico;
var
  Servico: TServiceManager;
begin
  ProgressBar1.Position := 0;
  ProgressBar1.Max := 70;
  Servico := TServiceManager.Create;
  try
    if Servico.Connect  then
      if Servico.OpenServiceConnection(PChar(EdPesq.Text)) then
      begin
        Servico.StartService;
        while Servico.GetStatus = 2 do
        begin
          ProgressBar1.Position := ProgressBar1.Position + 1;
          ProgressBar1.Refresh;
          Sleep(300);
        end;
        ProgressBar1.Position := 70;
        Edit1.Text := Servico.GetStatusText(Servico.GetStatus);
        if Servico.ServiceRunning then
        begin
          Geral.WriteAppKeyLM2('MySQL_Service', Application.Title, EdPesq.Text, ktString);
        end;
      end;
  finally
    Servico.Free;
  end;
end;

procedure TFmServerConnect.DesativarServico;
var
  Servico: TServiceManager;
begin
  ProgressBar1.Position := 0;
  ProgressBar1.Max := 70;
  Servico := TServiceManager.Create;
  try
    if Servico.Connect  then
      if Servico.OpenServiceConnection(PChar(EdPesq.Text)) then
      begin
        Servico.StopService;
        while Servico.GetStatus = 3 do
        begin
          ProgressBar1.Position := ProgressBar1.Position + 1;
          ProgressBar1.Refresh;
          Sleep(300);
        end;
        ProgressBar1.Position := 70;
        Edit1.Text := Servico.GetStatusText(Servico.GetStatus);
      end;
  finally
    Servico.Free;
  end;
end;

function TFmServerConnect.LeMyIni: String;
var
  MyIni: TIniFile;
  MyIniPath: String;
begin
  Result    := '';
  MyIniPath := Geral.ReadAppKeyLM('MyIniPath', Application.Title, ktString, '');
  //
  if MyIniPath = '' then
  begin
    MostraFormMyInis();
    Exit;
  end;
  MyIni := TIniFile.Create(MyIniPath+'My.ini');
  with MyIni do
  begin
    Result := MyIni.ReadString('mysqld', 'datadir', '');
    //Edit2.Text := Result;
  end;
  MyIni.Free;
end;

procedure TFmServerConnect.MostraFormMyInis();
begin
  if DBCheck.CriaFm(TFmMyInis, FmMyInis, afmoSoBoss) then
  begin
    FmMyInis.ShowModal;
    FmMyInis.Destroy;
  end;
  ZZTerminate := True;
  Application.Terminate;
end;

function TFmServerConnect.EscreveMyIni(Texto: String): Boolean;
var
  MyIni: TIniFile;
  MyIniPath: String;
begin
  Result    := True;
  MyIniPath := Geral.ReadAppKeyLM('MyIniPath', Application.Title, ktString, '');
  //
  if MyIniPath = '' then
  begin
    MostraFormMyInis();
    Exit;
  end;
  MyIni := TIniFile.Create(MyIniPath+'My.ini');
  MyIni.WriteString('mysqld', 'datadir', Texto);
  if not (LeMyIni = Texto) then Result := False;
  MyIni.Free;
end;

end.
