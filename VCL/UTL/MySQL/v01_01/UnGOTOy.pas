unit UnGOTOy;
{
  VAR_LIB_EMPRESA_SEL: Empresa logada > Ex: -11
}

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls,
  UnInternalConsts2, Mask, ComCtrls, (*DBTables,*) ResIntStrings, DmkDAC_PF,
  Registry, ZCF2, buttons, UnInternalConsts3, UnDmkProcFunc,
  mySQLDbTables, Grids, DBGrids, TypInfo, Winsock, IniFiles, Variants,
  dmkGeral, UnDmkEnums, UMySQLDB, UnGrl_consts, UnDmkPing,
  UnProjGroup_Consts;

type
  TUnGOTOy = class(TObject)
  private
    { Private declarations }
    function LocalizaCodigo(Atual, Codigo: Integer): Boolean;
    function LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
    function LocalizaTxtUsu(Atual, Codigo: String): Boolean;
    function Vai(Para: TVaiPara; Atual: Integer; Sort: String) : String;
    //function Registros(Tabela: TmySQLQuery): Integer;
  public
    { Public declarations }
    function TxtLaRegistro(RecCount, Increm : Integer) : String;
    function Go(Para: TVaiPara; Atual: Integer; Sort: String) : String;
    function GoNew(Para: TVaiPara; Atual: Integer; GBMyReg: TGroupBox;
             LaMyRegs: array of TLabel): Boolean;
    function VaiNumTxt(Para: TVaiPara; Atual, Sort: String) : String;
    function VaiTxt(Para: TVaiPara; Atual, Sort: String) : String;
    function VaiData(Para: TVaiPara; Atual: TDate; Sort: String) : String;
    function VaiInt(Para: TVaiPara; Qry: TmySQLQuery; Tabela, Campo, Extra:
             String; Negativo: Boolean): Boolean;
    function LC(Atual, Codigo: Integer): Boolean;
    function LocalizaNumTxt(Atual, Codigo: String): Boolean;
    function LocalizaTxt(Atual, Texto: String): Boolean;
    function LocalizaPriorNextNumTxt(Tabela, Campo, Atual: String;
             DataBase: TmySQLDataBase): String;
    function LocalizaPriorNextIntQr(Tabela: TmySQLQuery;
             Campo: TIntegerField; Atual: Integer): Integer; overload;
    function LocalizaPriorNextIntQr(Tabela: TmySQLQuery;
             Campo: TLargeIntField; Atual: LargeInt): Integer; overload;
    function LocalizaPriorNextTxtQr(Tabela: TmySQLQuery;
             Campo: TWideStringField; Atual: String): String;
    function LocalizaPriorNextInt_SQL(Tabela, Campo: String; Atual: Integer;
             DataBase: TmySQLDataBase): Integer;
    function LocalizaData(Atual, Data: TDate): Boolean;
    function BtEnabled(Codigo: Integer; PodeNeg: Boolean): Boolean;
    function BtEnabledNumTxt(Codigo: String; PodeNeg: Boolean): Boolean;
    function Nome(Padrao: String): String;
    function NomeNew(GBMyReg: TGroupBox; LaMyRegs: array of TLabel): Boolean;
    function NomeDireto(Padrao: String): String;
    function NomeRelacionado(Padrao: String): String;
    function CodigoTxt(Padrao: String): String;
    function Codigo(Atual: Integer; Padrao: String): String;
    function CodigoNew(Atual: Integer; GBMyReg: TGroupBox;
             LaMyRegs: array of TLabel): Boolean;
    function CodUsu(Atual: Integer; Padrao: String): String;
    function TxtUsu(Atual, Padrao: String): String;
    function NumTxt(Atual, Padrao: String): String;
    function Registros(Tabela: TmySQLQuery): Integer;
    function MyLocate(DataBase: TmySQLDataBase; Tabela: TmySQLQuery;
             Campo: String; Valor: Variant; Grid: TDBGrid): Boolean;
    //
{$IfNDef sLock}
    function  CriaFmUnLock_MLA: Boolean;
    procedure VerificaLiberacao;
    procedure LiberaUso;
    function  Destrava(DataFinal: TDateTime): Boolean;
{$EndIf}
    //procedure LiberaUso2;
    //
    function  VerificaTipoDaCarteira(Carteira: Integer): Integer;
    ////////////////////////////////////////////////////////////////////////////
    procedure LimpaVAR_SQL;
    procedure BotoesSb(LaTipo: String); overload;
    procedure BotoesSb(SQLType: TSQLType); overload;
    procedure BotoesSb(SQLType: TSQLType; Form: TForm); overload;
    function  CriaLocalizador(Codigo: Integer): Boolean;
    function  CriaLocalizadorCodUsu(Codigo: Integer): Boolean;
    function  CriaLocalizadorTxtUsu(Codigo: String): Boolean;
    procedure CriaLocalNumTxt(Codigo: String);
    procedure CriaLocalTxt(Texto: String);
    procedure CriaLocalizadorData(Data: TDate);
    procedure VerificaTerminal;
    function  ConectaAoServidor(IP: String; HostPort, CancelTimeMs: Word): Boolean;
    procedure DefineSenhaBoss();
    procedure DefineSenhaAdmin();
    function  DefineSenhaUsuario(Login: String): String;
    procedure AnaliseSenha(EdLogin, EdSenha: TEdit; FmMLA, FmMain: TForm;
              Componente: TComponent; ForcaSenha: Boolean;
              LaAviso1, LaAviso2: TLabel; QrGeraSenha: TmySQLQuery);
    procedure AnaliseSenhaMultiEmpresas(EdLogin, EdSenha: TEdit; FmMLA, FmMain: TForm;
              Componente: TComponent; ForcaSenha: Boolean;
              LaAviso1, LaAviso2: TLabel; QrGeraSenha: TmySQLQuery);
    procedure AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit; FmMLA, FmMain: TForm;
              Componente: TComponent; ForcaSenha: Boolean; LaAviso1, LaAviso2: TLabel;
              QrGeraSenha, DmodFinQrCarts: TmySQLQuery; ShowModal: Boolean = False);
{
function TUnGOTOy.OMySQLEstaInstalado(StaticText: TStaticText; ProgessBar:
             TProgressBar): Boolean;
             mudei 2011-10-30}
    function  OMySQLEstaInstalado(LaAviso1, LaAviso2: TLabel; ProgessBar:
              TProgressBar; BtEntra, BtConecta: TBitBtn): Boolean;//VerificaInstalacaoDoMySQL;
    function  DefineSenhaDia(Numero: Integer; Forcar: Boolean; Query:
              TmySQLQuery): Boolean;
{$IFNDEF NAO_USA_RECIBO}
    procedure EmiteRecibo(Codigo, Emitente, Beneficiario: Integer; Valor, ValorP,
              ValorE: Double; NumRecibo, Referente, ReferenteP, ReferenteE:
              String; Data: TDateTime; Sit: Integer);// Sit >> 2010-03-13
    procedure EmiteReciboMany(Tabela: String);
(*
    procedure CadastraImpressaoDeRecibo_ReciboImp(const Recibo: String; const
              _DtRecibo, _DtPrnted: TDateTime; const CNPJCPF: String; const
              Emitente, Beneficiario: Integer; const Valor: Double; var Codigo:
              Integer);
*)
    procedure CadastraImpressaoDeRecibo_ReciboImpCab(Codigo: Integer; Recibo: String);
    procedure ImpressaoDeRecibo_AltDtPrnted(Codigo: Integer; DtPrnted: String);
    procedure ImpressaoDeRecibo_ConfOuDesconf(Codigo, Controle: Integer; DtConfrmad: String);
{$ENDIF}
    function  SenhaDesconhecida(): Boolean;
    function  MudaSenhaBD(Senha, Usuario: String; MsgErr: Boolean): Boolean;
    //  Movido para DmkDAC_PF
    //function  TabelaExiste(Tabela: String; Database: TmySQLDatabase): Boolean;
    function  EnderecoDeEntidade(Entidade, TipoDeEndereco: Integer): String;
    function  EnderecoDeEntidade1(Entidade, TipoDeEndereco: Integer): String;
    function  EnderecoDeEntidade2(Entidade, TipoDeEndereco: Integer): String;
    function  ItemDeEndereco(Item, Valor: String): String;
    function  DefinePathMySQL: Boolean;
    function  DefineBaseDados: TmySQLDataBase;
    function  Fechar(Caption: String) : TCloseAction; overload;
    function  Fechar(SQLType: TSQLType) : TCloseAction; overload;
    function  FiltroNeg(): String;
    procedure CriaChavedeAcesso();
    procedure AvisoIndef(ID: Integer);
  end;

var
  GOTOy: TUnGOTOy;
  VAR_FILME: String[8];
const
  CO_BENEFICIARIO_0 = 0;
//  ArqBDF, ListPrint: TStringList;

implementation


uses
{$IfNDef sLock} UnLock_MLA, {$EndIf}
UnMyObjects, Module, UMySQLModule, SenhaBoss, (*InstallMySQL41, *)
{$IfNDef SemPontoFunci}PontoFunci, {$EndIf} NomeX,
{$IFNDEF NAO_USA_RECIBO} Recibo, Recibos, ReciboMany, {$ENDIF} Principal,
{$IfNDef sVerifiDB}Servidor, ServiceManager, ServicoManager, ServerConnect,{$EndIf}
Senha, Terminais,
GModule, ModuleGeral;

////////////////////////INICIO CONTROLE GERAL BD \\\\\\\\\\\\\\\\\\\\\\\\\

function TUnGOTOy.TxtLaRegistro(RecCount, Increm : Integer) : String;
var
  Total, p: Integer;
  CountTxt: String;
begin
  QvCountY.Close;
  QvCountY.Database := DefineBaseDados;

  QvCountY.SQL.Clear;
  // 2012-08-13
  //'gragrux ggx, gragru1 gg1'
  p := Pos(',', VAR_GOTOTABELA);
  if p > 0 then
    QvCountY.SQL.Add('SELECT COUNT(*) Record From '+Lowercase(Copy(VAR_GOTOTABELA, 1, p-1))+'')
  else
  // FIM 2012-08-13
  QvCountY.SQL.Add('SELECT COUNT(*) Record From '+Lowercase(VAR_GOTOTABELA)+'');
  UMyMod.AbreQuery(QvCountY, QvCountY.Database);
  Total := QvCountY.FieldByName('Record').AsInteger;
  QvCountY.Close;
  case Increm of
    -2 : VAR_RECNO := 0;
     0 : if Total > 0 then VAR_RECNO := 1 else VAR_RECNO := 0;
     2 : VAR_RECNO := Total;
     else VAR_RECNO := VAR_RECNO + Increm;
  end;
  if VAR_RECNO > RecCount then VAR_RECNO := RecCount;
  if VAR_RECNO < 0 then VAR_RECNO := 0;
  if (RecCount > 0) and (VAR_RECNO = 0) then VAR_RECNO := 1;
  if RecCount = -1 then CountTxt := '?'
  else CountTxt := IntToStr(RecCount);
  Result := (*IntToStr(VAR_RECNO)+CO_DE+*)CountTxt+CO_DE+IntToStr(Total);
end;

function TUnGOTOy.Go(Para: TVaiPara; Atual: Integer; Sort: String): String;
begin
  Result := Vai(Para, Atual, Sort);
end;

function TUnGOTOy.GoNew(Para: TVaiPara; Atual: Integer; GBMyReg: TGroupBox;
  LaMyRegs: array of TLabel): Boolean;
var
  Texto: String;
  I, K: Integer;
begin
  Texto := Vai(Para, Atual, GBMyReg.Caption[3]);
  K := pos(':', Texto);
  GBMyReg.Caption := ' ' + Copy(Texto, 1, K) + ' ';
  Texto := Copy(Texto, K + 2);
  for I := Low(LaMyRegs) to High(LaMyRegs) do
    LaMyRegs[I].Caption := Texto;
  Result := True;
end;

function TUnGOTOy.Vai(Para: TVaiPara; Atual: Integer; Sort: String): String;
const
  Mostra = False; //True;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOMySQLTABLE.First else
    if Para = vpPrior then VAR_GOTOMySQLTABLE.Prior else
    if Para = vpNext then VAR_GOTOMySQLTABLE.Next else
    if Para = vpLast then VAR_GOTOMySQLTABLE.Last else
    if Para = vpThis then
      if not VAR_GOTOMySQLTABLE.Locate('Periodo', Geral.Periodo2000(Date), []) then
        Geral.MB_Aviso('Per�odo atual n�o localizado!');
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    QvLocY.Database := DefineBaseDados;
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      }
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocY.SQL.Add('AND ')
        else
          QvLocY.SQL.Add('WHERE ');
        QvLocY.SQL.Add(VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
      end;
      }
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsInteger := Atual;
      Increm := -1;
      Quando := True;
    end else
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
      end;
      }
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsInteger := Atual;
      Increm := 1;
      Quando := True;
    end else
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      }
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocY.SQL.Add('AND ')
        else
          QvLocY.SQL.Add('WHERE ');
        QvLocY.SQL.Add(VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := 2;
    end else
    if Para = vpThis then
    begin
      QvLocY.SQL.Add('SELECT '+VAR_GOTOCAMPO+' Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+'='+IntToStr(Geral.Periodo2000(Date)));
      Quando := True;
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      Increm := 0;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    //
    //QvLocY. O p e n ;
    UMyMod.AbreQuery(QvLocY, QvLocY.Database, 'TUnGOTOy.Vai()');
    //
    if Mostra then
      dmkPF.LeMeuTexto(QvLocY.SQL.Text);
    QvLocY.First;
    if (QvLocY.RecordCount > 0) then
    begin
      if (QvLocY.FieldByName('Record').AsString <> '') then
        if (QvLocY.FieldByName('Record').AsInteger <> null) then
          LocalizaCodigo(Atual, QvLocY.FieldByName('Record').AsInteger);
    end else
    begin
      if Para = vpThis then
      begin
        // Colocar a mensagem no localizador de per�odo
        //Geral.MB_Aviso(VAR_PERIODO_NAO_LOC + ': Atual'));
        Result := VAR_PERIODO_NAO_LOC;
        QvLocY.Close;
        Exit;
      end;
    end;
    QvLocY.Close;
  end;
  if VAR_GOTOMySQLTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOMySQLTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOy.VaiNumTxt(Para: TVaiPara; Atual, Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOMySQLTABLE.First;
    if Para = vpPrior then VAR_GOTOMySQLTABLE.Prior;
    if Para = vpNext then VAR_GOTOMySQLTABLE.Next;
    if Para = vpLast then VAR_GOTOMySQLTABLE.Last;
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    QvLocY.Database := DefineBasedados;
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if VAR_GOTONEG <> gotoAll then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsString := Atual;
      Increm := -1;
      Quando := True;
    end;
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsString := Atual;
      Increm := 1;
      Quando := True;
    end;
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if VAR_GOTONEG <> gotoAll then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := 2;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    UMyMod.AbreQuery(QvLocY, QvLocY.Database);
    if QvLocY.RecordCount > 0 then
      if QvLocY.FieldByName('Record').AsString <> '' then
        LocalizaNumTxt(Atual, QvLocY.FieldByName('Record').AsString);
    QvLocY.Close;
  end;
  if VAR_GOTOMySQLTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOMySQLTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOy.VaiTxt(Para: TVaiPara; Atual, Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Limit: Integer;
  Sentido: String;
begin
  Limit := 0;
  Qtd := 0;
  increm := 0;
  Sentido := '';
  if Sort = 'A' then
  begin
    case Para of
      vpFirst: VAR_GOTOMySQLTABLE.First;
      vpPrior: VAR_GOTOMySQLTABLE.Prior;
      vpNext : VAR_GOTOMySQLTABLE.Next;
      vpLast : VAR_GOTOMySQLTABLE.Last;
    end;
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    QvLocY.Database := DefineBaseDados;
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <= "'+Atual+'"');
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <= "'+Atual+'"');
      Increm := -1;
      Limit := 2;
      Sentido := 'DESC';
    end;
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >= "'+Atual+'"');
      Increm := 1;
      Limit := 2;
    end;
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >= "'+Atual+'"');
      Increm := 2;
    end;
    if VAR_GOTOVAR > 0 then QvLocY.SQL.Add('AND '+VAR_GOTOVAR1);
    QvLocY.SQL.Add('ORDER BY Record '+Sentido);
    if Limit > 0 then QvLocY.SQL.Add('LIMIT '+IntToStr(Limit));
    UMyMod.AbreQuery(QvLocY, QvLocY.Database);
    case Para of
      vpFirst: QvLocY.First;
      vpPrior: QvLocY.Next; // DESC
      vpNext : QvLocY.Next;
      vpLast : QvLocY.Last;
    end;
    if QvLocY.RecordCount > 0 then
      if QvLocY.FieldByName('Record').AsString <> '' then
        LocalizaTxt(Atual, QvLocY.FieldByName('Record').AsString);
    QvLocY.Close;
  end;
  if VAR_GOTOMySQLTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOMySQLTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOy.VaiData(Para: TVaiPara; Atual: TDate; Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOMySQLTABLE.First;
    if Para = vpPrior then VAR_GOTOMySQLTABLE.Prior;
    if Para = vpNext then VAR_GOTOMySQLTABLE.Next;
    if Para = vpLast then VAR_GOTOMySQLTABLE.Last;
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[J..D]: ';
    QvLocY.Close;
    QvLocY.Database := DefineBaseDados;
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if VAR_GOTONEG <> gotoAll then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, Atual);
      Increm := -1;
      Quando := True;
    end;
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocY.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, Atual);
      Increm := 1;
      Quando := True;
    end;
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if VAR_GOTONEG <> gotoAll then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := 2;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    UMyMod.AbreQuery(QvLocY, QvLocY.Database);
    if QvLocY.RecordCount > 0 then
       LocalizaData(Atual, QvLocY.FieldByName('Record').AsDateTime);
    QvLocY.Close;
  end;
  if VAR_GOTOMySQLTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOMySQLTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOy.VaiInt(Para: TVaiPara; Qry: TmySQLQuery; Tabela, Campo, Extra:
String; Negativo: Boolean): Boolean;
var
  //Seq: String;
  //Increm, Qtd,
  Atual: Integer;
  Quando: Boolean;
begin
  Result := False;
  Quando := False;
  //Qtd := 0;
  //increm := 0;
  QvLocY.Close;
  QvLocY.Database := Qry.Database;
  QvLocY.SQL.Clear;
  Atual := Qry.FieldByName(Campo).AsInteger;
  if Para = vpFirst then
  begin
    QvLocY.SQL.Add('SELECT MIN('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    if not Negativo then
    begin
      QvLocY.SQL.Add('WHERE '+Campo+' > 0');
      Quando := True;
    end;
    //Increm := -2;
  end;
  if Para = vpPrior then
  begin
    QvLocY.SQL.Add('SELECT MAX('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    QvLocY.SQL.Add('WHERE '+Campo+' <:P0');
    if not Negativo then QvLocY.SQL.Add('AND '+Campo+' > 0');
    QvLocY.Params[0].AsInteger := Atual;
    //Increm := -1;
    Quando := True;
  end else
  if Para = vpNext then
  begin
    QvLocY.SQL.Add('SELECT MIN('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    QvLocY.SQL.Add('WHERE '+Campo+' >:P0');
    if not Negativo then QvLocY.SQL.Add('AND '+Campo+' > 0');
    QvLocY.Params[0].AsInteger := Atual;
    //Increm := 1;
    Quando := True;
  end else
  if Para = vpLast then
  begin
    QvLocY.SQL.Add('SELECT MAX('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    if not Negativo then
    begin
      QvLocY.SQL.Add('WHERE '+Campo+' > 0');
      Quando := True;
    end;
    //Increm := 2;
  end else
  if Para = vpThis then
  begin
    QvLocY.SQL.Add('SELECT '+Campo+' Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    QvLocY.SQL.Add('WHERE '+Campo+'='+IntToStr(Geral.Periodo2000(Date)));
    Quando := True;
    if not Negativo then
      QvLocY.SQL.Add('AND '+Campo+' > 0');
    //Increm := 0;
  end;
  if Trim(Extra) <> '' then
  begin
    if Quando then
      QvLocY.SQL.Add('AND ' + Extra)
    else
      QvLocY.SQL.Add('WHERE ' + Extra);
  end;
  UMyMod.AbreQuery(QvLocY, QvLocY.Database);
  QvLocY.First;
  if (QvLocY.RecordCount > 0) then
  begin
    if (QvLocY.FieldByName('Record').AsString <> '') then
      if (QvLocY.FieldByName('Record').AsInteger <> null) then
      begin
        Qry.Close;
        Qry.Params[00].AsInteger := QvLocY.FieldByName('Record').AsInteger;
        UMyMod.AbreQuery(Qry, Qry.Database);
        //
        Result := True;
      end;
  end;
end;

function TUnGOTOy.CriaLocalizador(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOMySQLTABLE.Database <> DefineBaseDados) then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizador". Tabela: '+VAR_GOTOMySQLTABLE.Name);
  if GetPropInfo(VAR_GOTOMySQLTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizador". Tabela: '+VAR_GOTOMySQLTABLE.Name)
  else begin
    VAR_GOTOMySQLTABLE.Close;
    try
      VAR_GOTOMySQLTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
      for i := 0 to VAR_SQL1.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL1[i]);
      // Cuidado erro frequente AND na SQL sem WHERE !!!! 
      VAR_GOTOMySQLTABLE.Params[0].AsInteger := Codigo;
      //UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
      UnDmkDAC_PF.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLDBNAME);
      //Geral.MB_Info(VAR_GOTOMySQLTABLE.SQL.Text);
      // 2020-09-26
      //if Registros(VAR_GOTOMySQLTABLE) > 0 then Result := True;
      Result := VAR_GOTOMySQLTABLE.RecordCount > 0;
    except
      on E: Exception do
      begin
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(VAR_GOTOMySQLTABLE, '', nil, True, True);
        Geral.MB_Erro('Erro no procedimento de reabertura de '+
        'tabela "CriaLocalizador". Tabela: ' +
        VAR_GOTOMySQLTABLE.Name + sLineBreak + E.Message);
      end;
    end;
  end;
end;

function TUnGOTOy.CriaLocalizadorCodUsu(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOMySQLTABLE.Database <> DefineBaseDados) then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOMySQLTABLE.Name);
  if GetPropInfo(VAR_GOTOMySQLTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOMySQLTABLE.Name)
  else begin
    VAR_GOTOMySQLTABLE.Close;
    try
      VAR_GOTOMySQLTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
      if VAR_SQL2 <> nil then
      for i := 0 to VAR_SQL2.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL2[i])
      else
        Geral.MB_Aviso('"VAR_SQL2" n�o configurado!  AVISE A DERMATEK!');
      VAR_GOTOMySQLTABLE.Params[0].AsInteger := Codigo;
      UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
      if Registros(VAR_GOTOMySQLTABLE) > 0 then Result := True;
    except;
      Geral.MB_Erro('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOMySQLTABLE.Name);
      raise;
    end;
  end;
end;

function TUnGOTOy.CriaLocalizadorTxtUsu(Codigo: String): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOMySQLTABLE.Database <> DefineBaseDados) then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOMySQLTABLE.Name);
  if GetPropInfo(VAR_GOTOMySQLTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOMySQLTABLE.Name)
  else begin
    VAR_GOTOMySQLTABLE.Close;
    try
      VAR_GOTOMySQLTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
      if VAR_SQL2 <> nil then
      for i := 0 to VAR_SQL2.Count -1 do
        VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL2[i])
      else
        Geral.MB_Aviso('"VAR_SQL2" n�o configurado!  AVISE A DERMATEK!');
      VAR_GOTOMySQLTABLE.Params[0].AsString := Codigo;
      UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
      if Registros(VAR_GOTOMySQLTABLE) > 0 then Result := True;
    except;
      Geral.MB_Erro('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOMySQLTABLE.Name);
      raise;
    end;
  end;
end;

procedure TUnGOTOy.CriaLocalNumTxt(Codigo: String);
var
  i: Integer;
begin
  VAR_GOTOMySQLTABLE.Close;
  VAR_GOTOMySQLTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do
    VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do
    //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQL1.Count -1 do
    VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL1[i]);
  VAR_GOTOMySQLTABLE.Params[0].AsString := Codigo;
  UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
  //ShowMessage(TmySQLQuery(VAR_GOTOMySQLTABLE).FieldByName('CodTxt').AsString);
end;

procedure TUnGOTOy.CriaLocalTxt(Texto: String);
var
  i: Integer;
begin
  VAR_GOTOMySQLTABLE.Close;
  VAR_GOTOMySQLTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do
    VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do
    //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQL1.Count -1 do
    VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL1[i]);
  VAR_GOTOMySQLTABLE.Params[0].AsString := Texto;
  UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
end;

procedure TUnGOTOy.CriaLocalizadorData(Data: TDate);
var
  i: Integer;
begin
  VAR_GOTOMySQLTABLE.Close;
  VAR_GOTOMySQLTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do
    VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do
    //VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQL1.Count -1 do
    VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL1[i]);
  VAR_GOTOMySQLTABLE.Params[0].AsDate := Data;
  UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
end;

function TUnGOTOy.LC(Atual, Codigo: Integer): Boolean;
begin
  Result := LocalizaCodigo(Atual, Codigo);
end;

function TUnGOTOy.LocalizaCodigo(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    try
      Result := CriaLocalizador(Codigo);
    except
       raise;
    end;
  finally
     if not Result then
     begin
       EAbort.Create('Erro na fun��o "CriaLocalizador".');
     end;
  end;
  try
    if (VAR_GOTOMySQLTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizador(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodigo".');
    raise;
  end;
end;

function TUnGOTOy.LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorCodUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorCodUsu".');
  end;
  try
    if (Registros(VAR_GOTOMySQLTABLE) = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizadorCodUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodUsu".');
    raise;
  end;
end;

function TUnGOTOy.LocalizaTxtUsu(Atual, Codigo: String): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorTxtUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorTxtUsu".');
  end;
  try
    if (Registros(VAR_GOTOMySQLTABLE) = 0) (*and ((Atual <> '') or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizadorTxtUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaTxtUsu".');
    raise;
  end;
end;

function TUnGOTOy.LocalizaPriorNextIntQr(Tabela: TmySQLQuery;
 Campo: TIntegerField; Atual: Integer): Integer;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;

function TUnGOTOy.LocalizaPriorNextTxtQr(Tabela: TmySQLQuery;
 Campo: TWideStringField; Atual: String): String;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;

function TUnGOTOy.LocalizaPriorNextIntQr(Tabela: TmySQLQuery;
  Campo: TLargeIntField; Atual: LargeInt): Integer;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;

function TUnGOTOy.LocalizaPriorNextInt_SQL(Tabela, Campo: String; Atual: Integer;
 DataBase: TmySQLDataBase): Integer;
begin
  //Result := 0;
  Dmod.QrPriorNext.Close;
  Dmod.QrPriorNext.Database := Dmod.MyDB; //2022-02-19
  Dmod.QrPriorNext.SQL.Clear;
  Dmod.QrPriorNext.SQL.Add('SELECT MAX('+Campo+') Codigo');
  Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
  Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' < '+IntToStr(Atual));
  UMyMod.AbreQuery(Dmod.QrPriorNext, Dmod.QrPriorNext.Database);
  if Dmod.QrPriorNext.RecordCount > 0 then Result :=
  Dmod.QrPriorNext.FieldByName(Campo).AsInteger else
  begin
    Dmod.QrPriorNext.Close;
    Dmod.QrPriorNext.SQL.Clear;
    Dmod.QrPriorNext.SQL.Add('SELECT MIN('+Campo+') Codigo');
    Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
    Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' > "'+IntToStr(Atual));
    UMyMod.AbreQuery(Dmod.QrPriorNext, Dmod.QrPriorNext.Database);
    if Dmod.QrPriorNext.RecordCount > 0 then Result :=
    Dmod.QrPriorNext.FieldByName(Campo).AsInteger else Result := 0;
  end;
end;

function TUnGOTOy.MyLocate(DataBase: TmySQLDataBase; Tabela: TmySQLQuery;
Campo: String; Valor: Variant; Grid: TDBGrid): Boolean;
begin
  Result := False;
  //if Tabela.Locate
end;

function TUnGOTOy.LocalizaPriorNextNumTxt(Tabela, Campo, Atual: String;
 DataBase: TmySQLDataBase): String;
begin
  Result := '';
  Dmod.QrPriorNext.Close;
  Dmod.QrPriorNext.Database := Dmod.MyDB; //2022-02-19
  Dmod.QrPriorNext.SQL.Clear;
  Dmod.QrPriorNext.SQL.Add('SELECT MAX('+Campo+') Codigo');
  Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
  Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' < "'+Atual+'"');
  UMyMod.AbreQuery(Dmod.QrPriorNext, Dmod.QrPriorNext.Database);
  if Dmod.QrPriorNext.RecordCount > 0 then Result :=
  Dmod.QrPriorNext.FieldByName('Codigo').AsString else
  begin
    Dmod.QrPriorNext.Close;
    Dmod.QrPriorNext.SQL.Clear;
    Dmod.QrPriorNext.SQL.Add('SELECT MIN('+Campo+') Codigo');
    Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
    Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' > "'+Atual+'"');
    UMyMod.AbreQuery(Dmod.QrPriorNext, Dmod.QrPriorNext.Database);
    if Dmod.QrPriorNext.RecordCount > 0 then Result :=
    Dmod.QrPriorNext.FieldByName('Codigo').AsString else Result := '';
  end;
end;

function TUnGOTOy.LocalizaNumTxt(Atual, Codigo: String): Boolean;
begin
  Result := False;
  CriaLocalNumTxt(Codigo);
  //
  if (Registros(VAR_GOTOMySQLTABLE) = 0) (*and ((Atual <> '0000000000000') or (VAR_GOTONEG = True))*) then
  begin
    CriaLocalNumTxt(Atual);
    Result := True;
  end;
end;

function TUnGOTOy.LocalizaTxt(Atual, Texto: String): Boolean;
begin
  Result := False;
  CriaLocalTxt(Texto);
  //
  if (Registros(VAR_GOTOMySQLTABLE) = 0) (*and ((Atual <> '') or (VAR_GOTONEG = True))*) then
  begin
    CriaLocalTxt(Atual);
    Result := True;
  end;
end;

function TUnGOTOy.LocalizaData(Atual, Data: TDate): Boolean;
begin
  Result := False;
  CriaLocalizadorData(Data);
  if (VAR_GOTOMySQLTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
  begin
    CriaLocalizadorData(Data);
    Result := True;
  end;
end;

procedure TUnGOTOy.BotoesSb(LaTipo: String);
var
  Acao: Boolean;
  i: Integer;
begin
  // Evitar erro
  if Screen.ActiveForm = nil then Exit;
  if LaTipo = CO_TRAVADO then Acao := True else Acao := False;
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Screen.ActiveForm.Components[i].Name = 'SbNovo')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNumero')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNome')
    or  (Screen.ActiveForm.Components[i].Name = 'SbQuery'))
    then begin
      if (Screen.ActiveForm.Components[i] is TBitBtn) then
        TBitBtn(Screen.ActiveForm.Components[i]).Enabled := Acao;
      if (Screen.ActiveForm.Components[i] is TSpeedButton) then
        TSpeedButton(Screen.ActiveForm.Components[i]).Enabled := Acao;
    end;
  end;
end;

{
procedure TUnGOTOy.BotoesSb2(LaTipo: String; Form: TForm);
var
  Acao: Boolean;
  i: Integer;
begin
  if Form =
  if LaTipo = CO_TRAVADO then Acao := True else Acao := False;
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Form.Components[i].Name = 'SbNovo')
    or  (Form.Components[i].Name = 'SbNumero')
    or  (Form.Components[i].Name = 'SbNome')
    or  (Form.Components[i].Name = 'SbQuery'))
    then begin
      if (Form.Components[i] is TBitBtn) then
        TBitBtn(Form.Components[i]).Enabled := Acao;
      if (Form.Components[i] is TSpeedButton) then
        TSpeedButton(Form.Components[i]).Enabled := Acao;
    end;
  end;
end;
}

procedure TUnGOTOy.BotoesSb(SQLType: TSQLType);
var
  Acao: Boolean;
  i, j: Integer;
begin
  if Screen.ActiveForm = nil then
    Exit;
  if SQLType = stLok then
    Acao := True
  else
    Acao := False;
  //
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Screen.ActiveForm.Components[i].Name = 'SbNovo')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNumero')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNome')
    or  (Screen.ActiveForm.Components[i].Name = 'SbQuery'))
    then begin
      if (Screen.ActiveForm.Components[i] is TBitBtn) then
        TBitBtn(Screen.ActiveForm.Components[i]).Enabled := Acao;
      if (Screen.ActiveForm.Components[i] is TSpeedButton) then
        TSpeedButton(Screen.ActiveForm.Components[i]).Enabled := Acao;
    end;
  end;
end;

function TUnGOTOy.BtEnabled(Codigo: Integer; PodeNeg: Boolean): Boolean;
begin
  if (Codigo = 0) or ((Codigo < 0) and (PodeNeg = False)) then
    Result := False
  else
    Result := True;
end;

function TUnGOTOy.BtEnabledNumTxt(Codigo: String; PodeNeg: Boolean): Boolean;
begin
  if (Length(Trim(Codigo)) = 0) and (PodeNeg = False) then
    Result := False
  else
    Result := True;
end;

{$IFNDEF NAO_USA_RECIBO}
procedure TUnGOTOy.CadastraImpressaoDeRecibo_ReciboImpCab(Codigo: Integer;
  Recibo: String);
var
  DtPrnted, DtConfrmad: String;
  Controle: Integer;
begin
  DtPrnted       := Geral.FDT(DModG.ObtemAgora(), 109);
  ImpressaoDeRecibo_AltDtPrnted(Codigo, DtPrnted);
  //
  Controle := UMyMod.BPGS1I32('reciboimpimp', 'Controle', '', '', tsPos, stIns, Controle);
  //
  Dmod.QrUpd.Database := Dmod.MyDB; //2022-02-19
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'reciboimpimp', False, [
  'Codigo', 'Recibo', 'DtPrnted'], ['Controle'], [
  Codigo, Recibo, DtPrnted], [Controle], True) then
  begin
    if Geral.MB_Pergunta('O recibo foi impresso?') = ID_YES then
    begin
      DtConfrmad := Geral.FDT(DModG.ObtemAgora(), 109);
      ImpressaoDeRecibo_ConfOuDesconf(Codigo, Controle, DtConfrmad);
    end;
  end;
end;
{$EndIf}

function TUnGOTOy.Codigo(Atual: Integer; Padrao: String): String;
var
  Num : String;
  Cod : Integer;
  Continua, Passa: Boolean;
begin
  Passa := False;
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA, 'Digite o c�digo.', '' );
  try
    Cod := Geral.IMV(Num);
    Continua := False;
    case VAR_GOTONEG of
      gotoNeg: Continua := Cod < 0;
      gotoNiZ: Continua := Cod <= 0;
      gotoAll: Continua := True;
      gotoNiP: Continua := Cod <> 0;
      gotoPiZ: Continua := Cod >= 0;
      gotoPos: Continua := Cod > 0;
    end;
    //if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not Continua then
    begin
      if (LowerCase(VAR_GOTOTABELA) = 'entidades') and (Cod = -2) then
        Passa := True
      else
      if (LowerCase(VAR_GOTOTABELA) = 'cunscad cun') and (Cod = -2) then
        Passa := True
      else
      begin
        Geral.MB_Aviso('C�digo n�o acess�vel nesta janela!');
        Exit;
      end;
    end;
    if (Cod > 0) or Passa or
    (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not GOTOy.LocalizaCodigo(Atual, Cod) then
      Geral.MB_Erro('O registro n� ' + Num +
      ' n�o foi localizado.')
    else
    begin
      if VAR_GOTOMySQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOMySQLTABLE.DisableControls;
        VAR_GOTOMySQLTABLE.Last;
        VAR_GOTOMySQLTABLE.EnableControls;
        VAR_GOTOMySQLTABLE.First;
      end;
      Result := '[1...]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
end;

function TUnGOTOy.CodigoNew(Atual: Integer; GBMyReg: TGroupBox;
  LaMyRegs: array of TLabel): Boolean;
var
  Num : String;
  Cod : Integer;
begin
  Num := InputBox(VAR_GOTOTABELA,'Digite o c�digo.', '' );
  try
    Cod := Geral.IMV(Num);
    if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not GOTOy.LocalizaCodigo(Atual, Cod) then
      Geral.MB_Erro('O registro n� ' + Num +
      ' n�o foi localizado.')
    else
    begin
      if VAR_GOTOMySQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOMySQLTABLE.DisableControls;
        VAR_GOTOMySQLTABLE.Last;
        VAR_GOTOMySQLTABLE.EnableControls;
        VAR_GOTOMySQLTABLE.First;
      end;
      GBMyReg.Caption := ' [1...]: ';
      Num := TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
      for Cod := Low(LaMyRegs) to High(LaMyRegs) do
        LaMyRegs[Cod].Caption := Num;
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
  Result := True;
end;

function TUnGOTOy.CodUsu(Atual: Integer; Padrao: String): String;
var
  Num : String;
  Cod : Integer;
  Continua: Boolean;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite a refer�ncia.', '' );
  try
    Cod := Geral.IMV(Num);
    Continua := False;
    case VAR_GOTONEG of
      gotoNeg: Continua := Cod < 0;
      gotoNiZ: Continua := Cod <= 0;
      gotoAll: Continua := True;
      gotoNiP: Continua := Cod <> 0;
      gotoPiZ: Continua := Cod >= 0;
      gotoPos: Continua := Cod > 0;
    end;
    //if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not Continua then
    begin
      Geral.MB_Aviso('C�digo n�o acess�vel nesta janela!');
      Exit;
    end;
    if not GOTOy.LocalizaCodUsu(Atual, Cod) then
      Geral.MB_Erro('A refer�ncia n� ' + Num + ' n�o foi localizada.')
    else
    begin
      if VAR_GOTOMySQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOMySQLTABLE.DisableControls;
        VAR_GOTOMySQLTABLE.Last;
        VAR_GOTOMySQLTABLE.EnableControls;
        VAR_GOTOMySQLTABLE.First;
      end;
      Result := '[R...]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
end;

function TUnGOTOy.TxtUsu(Atual, Padrao: String): String;
var
  Num : String;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite a refer�ncia.', '' );
  try
    if (Num <> '') or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not GOTOy.LocalizaTxtUsu(Atual, Num) then
      Geral.MB_Erro('A refer�ncia n� ' + Num +
      ' n�o foi localizada.')
    else
    begin
      if VAR_GOTOMySQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOMySQLTABLE.DisableControls;
        VAR_GOTOMySQLTABLE.Last;
        VAR_GOTOMySQLTABLE.EnableControls;
        VAR_GOTOMySQLTABLE.First;
      end;
      Result := '[R...]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
end;

function TUnGOTOy.NumTxt(Atual, Padrao: String): String;
var
  Num : String;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite o c�digo.', '' );
  try
    if GOTOy.LocalizaNumTxt(Atual, Num) then
      Geral.MB_Erro('O registro n� ' + Num +
      ' n�o foi localizado.')
    else
    begin
      if VAR_GOTOMySQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOMySQLTABLE.DisableControls;
        VAR_GOTOMySQLTABLE.Last;
        VAR_GOTOMySQLTABLE.EnableControls;
        VAR_GOTOMySQLTABLE.First;
      end;
      Result := '[1...]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
end;

function TUnGOTOy.Nome(Padrao: String): String;
var
  X : String;
  i: Integer;
  Continua: Boolean;
begin
  Result := Padrao;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura Pelo Nome';
  FmNomeX.ShowModal;
  X := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  if Continua = True then
  begin
    VAR_GOTOMySQLTABLE.Close;
    VAR_GOTOMySQLTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLa[i]);
    VAR_GOTOMySQLTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOMySQLTABLE.Params[0].AsString := X;
    if VAR_GOTOMySQLTABLE.Params.Count > 1 then
    VAR_GOTOMySQLTABLE.Params[1].AsString := X;
    UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
    if VAR_GOTOMySQLTABLE.RecordCount = -1 then
    begin
      VAR_GOTOMySQLTABLE.DisableControls;
      VAR_GOTOMySQLTABLE.Last;
      VAR_GOTOMySQLTABLE.EnableControls;
      VAR_GOTOMySQLTABLE.First;
    end;
    Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
  end;
end;

function TUnGOTOy.NomeDireto(Padrao: String): String;
var
  Nome : String;
  i: Integer;
begin
  Nome := '%'+Padrao+'%';
  VAR_GOTOMySQLTABLE.Close;
  VAR_GOTOMySQLTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLa[i]);
  VAR_GOTOMySQLTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
  VAR_GOTOMySQLTABLE.Params[0].AsString := Nome;
  if VAR_GOTOMySQLTABLE.Params.Count > 1 then
  VAR_GOTOMySQLTABLE.Params[1].AsString := Nome;
  UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
  if VAR_GOTOMySQLTABLE.RecordCount = -1 then
  begin
    VAR_GOTOMySQLTABLE.DisableControls;
    VAR_GOTOMySQLTABLE.Last;
    VAR_GOTOMySQLTABLE.EnableControls;
    VAR_GOTOMySQLTABLE.First;
  end;
  Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
end;

function TUnGOTOy.NomeNew(GBMyReg: TGroupBox;
  LaMyRegs: array of TLabel): Boolean;
var
  X : String;
  i: Integer;
  Continua: Boolean;
begin
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura Pelo Nome';
  FmNomeX.ShowModal;
  X := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  if Continua = True then
  begin
    VAR_GOTOMySQLTABLE.Close;
    VAR_GOTOMySQLTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLa[i]);
    VAR_GOTOMySQLTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOMySQLTABLE.Params[0].AsString := X;
    if VAR_GOTOMySQLTABLE.Params.Count > 1 then
    VAR_GOTOMySQLTABLE.Params[1].AsString := X;
    UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
    if VAR_GOTOMySQLTABLE.RecordCount = -1 then
    begin
      VAR_GOTOMySQLTABLE.DisableControls;
      VAR_GOTOMySQLTABLE.Last;
      VAR_GOTOMySQLTABLE.EnableControls;
      VAR_GOTOMySQLTABLE.First;
    end;
    GBMyReg.Caption := ' [A..Z]: ';
    X := TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
    for I := Low(LaMyRegs) to High(LaMyRegs) do
      LaMyRegs[I].Caption := X;
  end;
  Result := True;
end;

function TUnGOTOy.NomeRelacionado(Padrao: String): String;
var
  X : String;
  i: Integer;
  Continua: Boolean;
begin
  Result := Padrao;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura Pelo Nome';
  FmNomeX.ShowModal;
  X := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  if Continua = True then
  begin
    VAR_GOTOMySQLTABLE.Close;
    VAR_GOTOMySQLTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLa[i]);
    VAR_GOTOMySQLTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOMySQLTABLE.Params[0].AsString := X;
    if VAR_GOTOMySQLTABLE.Params.Count > 1 then
    VAR_GOTOMySQLTABLE.Params[1].AsString := X;
    //Geral.MB_Aviso(VAR_GOTOMySQLTABLE.SQL.Text);
    UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);//Open;
    if VAR_GOTOMySQLTABLE.RecordCount = -1 then
    begin
      VAR_GOTOMySQLTABLE.DisableControls;
      VAR_GOTOMySQLTABLE.Last;
      VAR_GOTOMySQLTABLE.EnableControls;
      VAR_GOTOMySQLTABLE.First;
    end;
    Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
  end;
end;

function TUnGOTOy.CodigoTxt(Padrao: String): String;
var
  Codigo : String;
  i: Integer;
begin
  Result := Padrao;
  Codigo := InputBox(VAR_GOTOTABELA, 'Digite parte do c�digo', '%%' );
  if Codigo <> CO_VAZIO then
  begin
    VAR_GOTOMySQLTABLE.Close;
    VAR_GOTOMySQLTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQL1.Count -1 do VAR_GOTOMySQLTABLE.SQL.Add(VAR_SQL1[i]);
    VAR_GOTOMySQLTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOMySQLTABLE.Params[0].AsString := Codigo;
    UMyMod.AbreQuery(VAR_GOTOMySQLTABLE, VAR_GOTOMySQLTABLE.Database);
    if VAR_GOTOMySQLTABLE.RecordCount = -1 then
    begin
      VAR_GOTOMySQLTABLE.DisableControls;
      VAR_GOTOMySQLTABLE.Last;
      VAR_GOTOMySQLTABLE.EnableControls;
      VAR_GOTOMySQLTABLE.First;
    end;
    Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
  end;
end;

function TUnGOTOy.Registros(Tabela: TmySQLQuery): Integer;
begin
  if Tabela.State = dsInactive then Result := 0
  else begin
    if Tabela.RecordCount = -1 then
    begin
      Tabela.Last;
      Tabela.First;
    end;
    Result := Tabela.RecordCount;
  end;
end;

procedure TUnGOTOy.LimpaVAR_SQL;
begin
  //VAR_SQLy.Clear;
  VAR_SQLx.Clear;
  VAR_SQL1.Clear;
  VAR_SQL2.Clear;
  VAR_SQLa.Clear;
end;

{$IfNDef sLock}
function TUnGOTOy.Destrava(DataFinal: TDateTime): Boolean;
var
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje : TDateTime;
  Agora: TTime;
  Licenca: int64;
  Chave: Double;
  Serial: Integer;
  CNPJ, Enti: String;
begin
  Result := False;
  try
    CriaChavedeAcesso;
    CriaChavedeAcesso;
    Chave  := Geral.DMV(UMyMod.LeChavedeAcesso);
    Serial := dmkPF.GetDiskSerialNumber('C');
    Licenca := Trunc((Serial * Chave) - Serial - Chave);
    //
    DmodG.ReopenMaster('TUnGOTOy.1573');
    //
    USQLDB.ObtemDataHora(Dmod.MyDB, Hoje, Agora);
    //
    Dmod.QrAux.Database := Dmod.MyDB; //2022-02-19
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.QrAux.Database);
    //
    Dmod.QrUpd.Database := Dmod.MyDB;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
    else
      Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
    Dmod.QrUpd.Params[0].AsInteger := -11;
    Dmod.QrUpd.Params[1].AsString  := FmUnLock_MLA.FCNPJ;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    // Pode ainda n�o haver dono!!!
    DmodG.ReopenMaster('TUnGOTOy.1594');
    if DmodG.QrMaster.RecordCount > 0 then
    begin
      Dmod.QrUpd.SQL.Add('UPDATE master SET ');
    end else
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO master SET ');
    end;
    Dmod.QrUpd.SQL.Add('Monitorar=1, Distorcao=0, Limite=0, DiasExtra=0, ');
    Dmod.QrUpd.SQL.Add('Hoje="' + FormatDateTime(VAR_FORMATDATE,  Hoje) + '", ');
    Dmod.QrUpd.SQL.Add('Hora="'+FormatDateTime(VAR_FORMATTIME2, Agora)+'", ');
    Dmod.QrUpd.SQL.Add('DataF="'+FormatDateTime(VAR_FORMATDATE,  DataFinal)+'",');
    Dmod.QrUpd.SQL.Add('Em="'+FmUnLock_MLA.FRazaoSocial+'", ');
    Dmod.QrUpd.SQL.Add('CNPJ="'+FmUnLock_MLA.FCNPJ+'" ');
    if VAR_SERVIDOR = 2 then
      Dmod.QrUpd.SQL.Add(', Licenca=' + FormatFloat('0', Licenca));
    if (DmodG.QrMasterMasSenha.Value = '') and (DmodG.QrMasterMasLogin.Value = '') then
    begin
      Dmod.QrUpd.SQL.Add(', SitSenha=1, MasLogin=''geren'', MasSenha=AES_ENCRYPT(''geren'', ''' +
      CO_USERSPNOW + ''')');
    end;
    Dmod.QrUpd.ExecSQL;
    //
    VAR_ESTEIP := Geral.ObtemIP(1);
    Dmod.QrTerminal.Database := Dmod.MyDB; //2022-02-19
    Dmod.QrTerminal.Close;
    Dmod.QrTerminal.Params[0].AsString := VAR_IP_LICENCA;
    UMyMod.AbreQuery(Dmod.QrTerminal, Dmod.QrTerminal.Database);
    //
    if Dmod.QrTerminal.RecordCount = 0 then
    begin
      VAR_IP_LICENCA := '';
      MyObjects.FormShow(TFmTerminais, FmTerminais);
      //
      Geral.WriteAppKeyLM2('IPClient', Application.Title, VAR_IP_LICENCA, ktString);
    end;
    if VAR_IP_LICENCA <> '' then
    begin
      Dmod.QrUpd.Database := Dmod.MyDB;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0 WHERE IP=:P1 ');
      Dmod.QrUpd.Params[0].AsFloat  := Licenca;
      Dmod.QrUpd.Params[1].AsString := VAR_IP_LICENCA;
      Dmod.QrUpd.ExecSQL;
      //
    end else begin
      Geral.MB_Aviso('IP sem Terminal definido!'+
      sLineBreak+'IP: '+VAR_IP_LICENCA);
    end;
    //
    Dmod.QrAux.Database := Dmod.MyDB; // 2022-02-19
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Tipo FROM entidades WHERE Codigo in (-1,-11)');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.QrAux.Database);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, ')
    else Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=0, ');
    if Dmod.QrAux.FieldByName('Tipo').AsInteger = 1 then Enti := 'Nome' else Enti := 'RazaoSocial';
    if Dmod.QrAux.FieldByName('Tipo').AsInteger = 1 then CNPJ := 'CPF' else CNPJ := 'CNPJ';
    Dmod.QrUpd.SQL.Add(Enti+'=:P0, '+CNPJ+'=:P1');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('WHERE Codigo in (-1,-11)')
    else Dmod.QrUpd.SQL.Add(', Filial=1, Codigo=-11');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_MLA.FRazaoSocial;
    Dmod.QrUpd.Params[1].AsString := FmUnLock_MLA.FCNPJ;
    Dmod.QrUpd.ExecSQL;
  except;
    raise;
  end;
end;
{$EndIf}

procedure TUnGOTOy.CriaChavedeAcesso();
begin
  Geral.WriteAppKeyLM('Dmk001', TMeuDB, Random(100000), ktInteger);
end;

{$IfNDef sLock}
procedure TUnGOTOy.VerificaLiberacao();
var
  Dias, Tempo : Double;
  Distorcao, Monitorar: Integer;
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje, Data: TDateTime;
  Agora: TTime;
  Exec: String;
begin
  Exec := Uppercase(ExtractFileName(Application.ExeName));
  //if Exec = Uppercase('Syndic.exe') then Exit;
  DmodG.ReopenMaster('TUnGOTOy.1684');
  if DmodG.QrMasterMonitorar.Value = 0 then  // travado ap�s per�odo de avalia��o
  begin
    CriaFmUnLock_MLA;
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end else begin
    if DmodG.QrMasterMonitorar.Value in [1, 3] then // sob avalia��o
    begin
      Monitorar := DmodG.QrMasterMonitorar.Value;
      Distorcao := DmodG.QrMasterDistorcao.Value;
      //
      USQLDB.ObtemDataHora(DMod.MyDB, Hoje, Agora);
      //
      Dias := Trunc(Hoje) - Trunc(DmodG.QrMasterHoje.Value);
      Tempo := Agora - DmodG.QrMasterHora.Value;
      if Dias < 0 then inc(Distorcao, 1)
      else if (Dias = 0) and (Tempo < 0) then inc(Distorcao, 1)
      else if Trunc(Agora) < DmodG.QrMasterHoje.Value then
         Agora := DmodG.QrMasterHoje.Value + 1 + Round(SQRT(Distorcao));
      if (Hoje + Distorcao ) > DmodG.QrMasterDataF.Value then inc(Monitorar, -1);

      Dmod.QrUpdU.Database := Dmod.MyDB;
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=:P0, Distorcao=:P1, ');
      Dmod.QrUpdU.SQL.Add('Hoje=:P2, Hora=:P3');
      Dmod.QrUpdU.Params[0].AsInteger := Monitorar;
      Dmod.QrUpdU.Params[1].AsInteger := Distorcao;
      Dmod.QrUpdU.Params[2].AsString := FormatDateTime(VAR_FORMATDATE,  Hoje);
      Dmod.QrUpdU.Params[3].AsString := FormatDateTime(VAR_FORMATTIME2, Agora);
      Dmod.QrUpdU.ExecSQL;
    end
    else
  //          Close;
  end;
  if DmodG.QrMasterMonitorar.Value = 2  then  // aluguel vencido
  begin
    GOTOy.CriaFmUnLock_MLA;
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end;
  if DmodG.QrMasterMonitorar.Value = 4  then  // Liberado eternamente
  begin
    //
  end;
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DmodG.QrMasterDistorcao.Value;
    Data := DmodG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data)+' ['+
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
end;

procedure TUnGOTOy.LiberaUso;
var
  Licenca: String;
  Dias, Chave: Double;
  Serial: Integer;
  Data: TDateTime;
begin
  if VAR_STDATABASES <> nil then
  begin
    {$IfNDef SemDBLocal}
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' + Dmod.MyLocDataBase.DataBaseName + VAR_BDsEXTRAS;
    {$Else}
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' + VAR_BDsEXTRAS;
    {$EndIf}
    VAR_STDATABASES.Width := Length(VAR_STDATABASES.Text) * 7 + 8;
  end;
  DmodG.ReopenMaster('TUnGOTOy.1757');
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := DmodG.QrMasterDistorcao.Value;
    Data := DmodG.QrMasterDataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
  Licenca := DmodG.QrMasterLicenca.Value;
  Chave   := Geral.DMV(UMyMod.LeChavedeAcesso);
  Serial  := dmkPF.GetDiskSerialNumber('C');
  if Licenca <> FloatToStr(Trunc(((Chave*Serial)-Chave-Serial))) then
  begin
    DmodG.ReopenMaster('TUnGOTOy.1771');
    Dmod.QrUpdU.Database := Dmod.MyDB;
    Dmod.QrUpdU.SQL.Clear;
    if DmodG.QrMaster.RecordCount=0 then
      Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Monitorar=2')
    else
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=2');
    Dmod.QrUpdU.ExecSQL;
    DmodG.ReopenMaster('TUnGOTOy.1778');
    VerificaLiberacao;
  end else if
    DmodG.QrMasterMonitorar.Value = 2 then
      VerificaLiberacao;
end;

function TUnGOTOy.CriaFmUnLock_MLA: Boolean;
begin
  Result := False;
  DmodG.ReopenMaster('TUnGOTOy.1788');
  if DmodG.QrMasterLimite.Value < 30 then
  begin
    Dmod.ReopenControle();
    MyObjects.FormCria(TFmUnLock_MLA, FmUnLock_MLA);
    FmUnLock_MLA.FContraSenha := Dmod.QrControle.FieldByName('ContraSenha').AsString;
    if (Trim(DmodG.QrMasterEm.Value) <> '') and (DmodG.QrMasterCNPJ.Value <> '') then
    begin
      FmUnLock_MLA.EdRazao.Text := DmodG.QrMasterEm.Value;
      FmUnLock_MLA.EdCNPJ.ValueVariant := DmodG.QrMasterCNPJ.Value;
    end else begin
      Dmod.QrAux.Database := Dmod.MyDB; // 2022-02-19
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NO_ENT,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,CNPJ,CPF) CNPJ_CPF');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.Params[0].AsInteger := Dmod.QrControle.FieldByName('Dono').AsInteger;
      UMyMod.AbreQuery(Dmod.QrAux, Dmod.QrAux.Database);
      FmUnLock_MLA.EdRazao.Text := Dmod.QrAux.FieldByName('NO_ENT').AsString;
      FmUnLock_MLA.EdCNPJ.ValueVariant := Dmod.QrAux.FieldByName('CNPJ_CPF').AsString;
    end;
    FmUnLock_MLA.ShowModal;
    Dmod.QrUpd.Database := Dmod.MyDB;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha=:P0');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_MLA.FContraSenha;
    Dmod.QrUpd.ExecSQL;
    //
    DmodG.ReopenMaster('TUnGOTOy.1816');
    if FmUnlock_MLA.FCrypt then Destrava(FmUnLock_MLA.MCNova.Date)
    else begin
      Dmod.QrUpdU.Database := Dmod.MyDB;
      Dmod.QrUpdU.SQL.Clear;
      if DmodG.QrMaster.RecordCount = 0 then
        Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Limite=1')
      else Dmod.QrUpdU.SQL.Add('UPDATE master SET Limite=Limite+1');
      Dmod.QrUpdU.ExecSQL;
      Geral.MB_Erro('Senha inv�lida.'+sLineBreak+
      'H� possibilidade de mais '+IntToStr(29-DmodG.QrMasterLimite.Value)+
      ' tentativas de reativa��o do sistema. Ap�s, '+
      'o sistema travar� em definitivo.');
    end;
    FmUnLock_MLA.Destroy;
    //
    Result := True;
  end else
    Geral.MB_Erro('Tentativas de reativa��o do sistema esgotadas.' +
      sLineBreak + ' Sistema Travado');
end;
{$EndIf}

{
procedure TUnGOTOy.LiberaUso2;
var
  Achou: Boolean;
  MinhaLi: String;
  Dias, Chave: Double;
  Data: TDateTime;
  Licenca: String;
  Serial: Integer;
begin
  Achou := False;
  if VAR_STDATABASES <> nil then
  begin
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' +
    Dmod.MyLocDataBase.DataBaseName + VAR_BDsEXTRAS;
    VAR_STDATABASES.Width := Length(VAR_STDATABASES.Text) * 7 + 8;
  end;
  DmodG.ReopenMaster();
  Dias := DmodG.QrMasterDistorcao.Value;
  Data := DmodG.QrMasterDataF.Value;
  if VAR_STDATALICENCA <> nil then
  begin
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
  Licenca  := DmodG.QrMasterLicenca.Value;
  Chave    := Geral.DMV(UMyMod.LeChavedeAcesso);
  Serial   := dmkPF.GetDiskSerialNumber('C');
  MinhaLi := FloatToStr(Trunc(((Chave*Serial)-Chave-Serial)));
  if (Licenca  <> FloatToStr(Trunc(((Chave*Serial)-Chave-Serial))))
  or (Data - Dias - Date < 1) then
  begin
    Dmod.QrTerminais.Close;
    UMyMod.AbreQuery(Dmod.QrTerminais);
    while not Dmod.QrTerminais.Eof do
    begin
      Licenca  := Dmod.QrTerminaisLicenca.Value;
      if Licenca = MinhaLi then
      begin
        Achou := True;
        Break;
        Exit;
      end;
      Dmod.QrTerminais.Next;
    end;
    if not Achou then
    begin
      VAR_ESTEIP := Geral.ObtemIP(1);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0');
      Dmod.QrUpd.SQL.Add('WHERE IP=:P1');
      Dmod.QrUpd.Params[00].AsString := MinhaLi;
      Dmod.QrUpd.Params[01].AsString := VAR_ESTEIP;
      Dmod.QrUpd.ExecSQL;
      //
      DmodG.ReopenMaster();
      Dmod.QrUpdU.SQL.Clear;
      if DmodG.QrMaster.RecordCount=0 then
        Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Monitorar=2')
      else
        Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=2');
      Dmod.QrUpdU.ExecSQL;
      DmodG.ReopenMaster();
      VerificaLiberacao;
    end;
  end else if
    DmodG.QrMasterMonitorar.Value = 2 then
      VerificaLiberacao;
  //VerificaLiberacao;
end;
}

function TUnGOTOy.VerificaTipoDaCarteira(Carteira: Integer): Integer;
begin
  Dmod.QrAux.Database := Dmod.MyDB; // 2022-02-19
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Tipo FROM carteiras');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.Params[0].AsInteger := Carteira;
  UMyMod.AbreQuery(Dmod.QrAux, Dmod.QrAux.Database);
  Result := Dmod.QrAux.FieldByName('Tipo').AsInteger;
  Dmod.QrAux.Close;
end;

procedure TUnGOTOy.VerificaTerminal();
var
  wVersionRequested: Word;
  wsaData: TWSAData;
  IP: String;
begin
  wVersionRequested := MAKEWORD(1,1);
  WSAStartup(wVersionRequested, wsaData);
  //if VAR_SERVIDOR = 1 then
  //begin
    VAR_ESTEIP := Geral.ObtemIP(1);
    if VAR_IP_LICENCA <> '' then IP := VAR_IP_LICENCA else IP := VAR_ESTEIP;
    Dmod.QrTerminal.Database := Dmod.MyDB; //2022-02-19
    Dmod.QrTerminal.Close;
    Dmod.QrTerminal.Params[0].AsString := IP;
    UMyMod.AbreQuery(Dmod.QrTerminal, Dmod.QrTerminal.Database);
    if Dmod.QrTerminal.RecordCount = 0 then
    begin
      if VAR_STAVISOS <> nil then
      VAR_STAVISOS.Text := ' M�quina cliente sem terminal definido!';
    end else
      VAR_TERMINAL := Dmod.QrTerminalTerminal.Value;
  //end else VAR_TERMINAL := 0;
  if VAR_STTERMINAL <> nil then
    VAR_STTERMINAL.Text := IntToStr(VAR_TERMINAL);
  (*if (VAR_SERVIDOR = 1)
  and ((VAR_IP='127.0.0.1') or (VAR_IP='localhost') or (VAR_IP = ''))
  then begin
    Geral.MB_Erro('IP inv�lido para m�quina cliente!');
    ZZTerminate := True;
    Application.Terminate;
  end;*)
end;

procedure TUnGOTOy.DefineSenhaBoss();
var
  Texto, a: String;
begin
  ///////////////////////////////////////////
  // Excluir QrBoss e QrSenha de Fm???_MLA //
  ///////////////////////////////////////////
  DmodG.QrBSit.Close;
  DmodG.QrBSit.Database := Dmod.MyDB; // 2022-02-19
  UMyMod.AbreQuery(DmodG.QrBSit, DmodG.QrBSit.Database);
  case DmodG.QrBSitSitSenha.Value of
    1: Texto := CO_USERSP001;
    else Texto := 'xxx';
  end;
  if DmodG.QrBSitSitSenha.Value <> CO_USERSPSEQ then
  begin
    DmodG.QrBoss.Close;
    DmodG.QrBoss.Database := Dmod.MyDB; // 2022-02-19
    DmodG.QrBoss.Params[0].AsString := Texto;
    UMyMod.AbreQuery(DmodG.QrBoss, DmodG.QrBoss.Database);
    if DmodG.QrBSitSitSenha.Value = 0 then a := DmodG.QrBossMasZero.Value
    else a := DmodG.QrBossMasSenha.Value;
    Dmod.QrUpd.Database := Dmod.MyDB;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE master SET SitSenha=:P0, ');
    Dmod.QrUpd.SQL.Add('MasSenha=AES_ENCRYPT(:P1, :P2) ');
    Dmod.QrUpd.Params[00].AsInteger := CO_USERSPSEQ;
    Dmod.QrUpd.Params[01].AsString  := a;
    Dmod.QrUpd.Params[02].AsString  := CO_USERSPNOW;
    Dmod.QrUpd.ExecSQL;
  end;
  DmodG.QrBSit.Close;
  DmodG.QrBoss.Close;
  DmodG.QrBoss.Database := Dmod.MyDB; // 2022-02-19
  DmodG.QrBoss.Params[0].AsString := CO_USERSPNOW;
  UMyMod.AbreQuery(DmodG.QrBoss, DmodG.QrBoss.Database);
  if DmodG.QrBossMasSenha.Value <> '' then
    VAR_BOSS := Uppercase(DmodG.QrBossMasSenha.Value)
  else
    VAR_BOSS := CO_MASTER;
  VAR_BOSSLOGIN := DmodG.QrBossMasLogin.Value;
  VAR_BOSSSENHA := Uppercase(DmodG.QrBossMasSenha.Value);
  if VAR_SENHA = CO_MASTER then
    VAR_USUARIO := -1;
  if Uppercase(VAR_SENHA) = Uppercase(DmodG.QrBossMasSenha.Value) then
    VAR_USUARIO := -2;
  if ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
    VAR_USUARIO := -3;
end;

procedure TUnGOTOy.DefineSenhaAdmin();
const
  NaoAvisaErro = False;
var
  Texto, a: String;
begin
  try
    //if DmodG.QrBSitSitSenha.Value <> CO_USERSPSEQ then
    begin
      UnDMkDAC_PF.AbreMySQLQuery0(DModG.QrAdmin, Dmod.MyDB, [
      'SELECT ',
      'AES_DECRYPT(SenhaAdmin, "' + CO_USERSP001 + '") SenhaAdmin ',
      'FROM master ',
      ''], NaoAvisaErro);
    end;
    if DModG.QrAdminSenhaAdmin.Value <> EmptyStr then
      VAR_ADMIN := DModG.QrAdminSenhaAdmin.Value
    else
      VAR_ADMIN := CO_ADMIN;
    //
    if (VAR_ADMIN <> '') and (Uppercase(VAR_LOGIN) = Uppercase('ADMIN')) then
    begin
      if (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) then
        VAR_USUARIO := -3
      else
      if (Uppercase(VAR_SENHA) = Uppercase(CO_ADMIN)) then
        VAR_USUARIO := -3;
    end;
  except
    Geral.MB_Erro('Admin n�o definido!');
  end;
end;

function TUnGOTOy.DefineSenhaUsuario(Login: String): String;
var
  Texto, a: String;
begin
  DmodG.QrSSit.Close;
  DmodG.QrSSit.Database := Dmod.MyDB; // 2022-02-19
  DmodG.QrSSit.Params[0].AsString := Login;
  UMyMod.AbreQuery(DmodG.QrSSit, DmodG.QrSSit.Database);
  if DmodG.QrSSit.RecordCount = 0 then
  begin
    Result := '';
    // Somente abrir para eviat exception
    DmodG.QrSenhas.Close;
    DmodG.QrSenhas.Database := Dmod.MyDB; // 2022-02-19
    DmodG.QrSenhas.Params[00].AsString := CO_USERSPNOW;
    DmodG.QrSenhas.Params[01].AsString := Login;
    UMyMod.AbreQuery(DmodG.QrSenhas, DmodG.QrSenhas.Database);
    Exit;
  end;
  case DmodG.QrSSitSitSenha.Value of
    1: Texto := CO_USERSP001;
    else Texto := 'xxx';
  end;
  if DmodG.QrSSitSitSenha.Value <> CO_USERSPSEQ then
  begin
    DmodG.QrSenhas.Close;
    DmodG.QrSenhas.Database := Dmod.MyDB; // 2022-02-19
    DmodG.QrSenhas.Params[00].AsString := Texto;
    DmodG.QrSenhas.Params[01].AsString := Login;
    UMyMod.AbreQuery(DmodG.QrSenhas, DmodG.QrSenhas.Database);
    if DmodG.QrSSitSitSenha.Value = 0 then a := DmodG.QrSenhasSenhaOld.Value
    else a := DmodG.QrSenhasSenhaNew.Value;
    Dmod.QrUpd.Database := Dmod.MyDB;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE senhas SET SitSenha=:P0, ');
    Dmod.QrUpd.SQL.Add('Senha=AES_ENCRYPT(:P1, :P2) ');
    Dmod.QrUpd.SQL.Add('WHERE Numero=:P3 ');
    Dmod.QrUpd.Params[00].AsInteger := CO_USERSPSEQ;
    Dmod.QrUpd.Params[01].AsString  := a;
    Dmod.QrUpd.Params[02].AsString  := CO_USERSPNOW;
    Dmod.QrUpd.Params[03].AsInteger := DmodG.QrSenhasNumero.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  DmodG.QrSSit.Close;
  DmodG.QrSenhas.Close;
  DmodG.QrSenhas.Database := Dmod.MyDB; // 2022-02-19
  DmodG.QrSenhas.Params[00].AsString := CO_USERSPNOW;
  DmodG.QrSenhas.Params[01].AsString := Login;
  UMyMod.AbreQuery(DmodG.QrSenhas, DmodG.QrSenhas.Database);
  Result := DmodG.QrSenhasSenhaNew.Value;
end;

procedure TUnGOTOy.AnaliseSenha(EdLogin, EdSenha: TEdit; FmMLA, FmMain:
  TForm; Componente: TComponent; ForcaSenha: Boolean;
  LaAviso1, LaAviso2: TLabel; QrGeraSenha: TmySQLQuery);
var
  Mostra: Boolean;
  SenhaUsuario: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando usu�rio e senha');
  //
  try
    VAR_USUARIO := 0;
    VAR_PERFIL  := 0;
    DModG.ReopenMaster('TUnGOTOy.2099');
    // 2011-09-21 evitar erro no Logoff
    DmodG.MyPID_DB.Connected := False;
    // Fim 2011-09-21
    //
    VAR_LOGIN := Uppercase(EdLogin.Text);
    VAR_SENHA := Uppercase(EdSenha.Text);
    //VAR_FIRMA := EdEmpresa.ValueVariant;
    DefineSenhaAdmin;
    DefineSenhaBoss;
    if (DmodG.QrMasterSolicitaSenha.Value = 0)
    and (DmodG.QrMaster.RecordCount > 0)
    and (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
    begin
      VAR_LOGIN := '{?}';
      VAR_SENHA := '*';
    end;
    if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
    begin
      if (Trim(DmodG.QrBossMasSenha.Value) = '')
      or (Trim(DmodG.QrBossMasLogin.Value) = '') then
      begin
        if Geral.MB_Pergunta('N�o h� cadastro da senha administradora '+
        'do aplicativo. Deseja cadastr�-la agora?')= ID_YES then begin
          VAR_BOSS        := '';
          VAR_SENHATXT    := '';
          VAR_NOMEEMPRESA := DmodG.QrBossEm.Value;
          VAR_CNPJEMPRESA := DmodG.QrBossCNPJ.Value;
          MyObjects.FormShow(TFmSenhaBoss, FmSenhaBoss);
          Dmod.QrUpd.Database := Dmod.MyDB;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE master SET MasSenha=AES_ENCRYPT(:P0, :P1), ');
          Dmod.QrUpd.SQL.Add('MasLogin=:P2, Em=:P3, CNPJ=:P4, SitSenha=:P5');
          Dmod.QrUpd.SQL.Add('');
          Dmod.QrUpd.Params[00].AsString := VAR_SENHATXT;
          Dmod.QrUpd.Params[01].AsString := CO_USERSPNOW;
          Dmod.QrUpd.Params[02].AsString := VAR_BOSS;
          Dmod.QrUpd.Params[03].AsString := VAR_NOMEEMPRESA;
          Dmod.QrUpd.Params[04].AsString := VAR_CNPJEMPRESA;
          Dmod.QrUpd.Params[05].AsInteger := CO_USERSPSEQ;
          Dmod.QrUpd.ExecSQL;
          //
          ZZTerminate := True;
          Application.Terminate;
        end;
      end;
      if Componente <> nil then
        if Componente.Name = 'BtConfirma' then
        begin
          if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
          begin
            Geral.MB_Erro('Informe um Login e uma senha!');
            EdLogin.SetFocus;
          end else if (Trim(VAR_LOGIN) = '') then
          begin
            Geral.MB_Erro('Informe um Login!');
            EdLogin.SetFocus;
          end else begin
            Geral.MB_Erro('Informe uma senha!');
            EdSenha.SetFocus;
          end;
        end;
      Exit;
    end else if Trim(VAR_LOGIN) = '' then
    begin
      EdLogin.SetFocus;
      Exit;
    end else if Trim(VAR_SENHA) = '' then
    begin
      EdSenha.SetFocus;
      Exit;
    end;
    VAR_BOSSLOGIN := DmodG.QrBossMasLogin.Value;
    VAR_BOSSSENHA := Uppercase(DmodG.QrBossMasSenha.Value);
    if VAR_SENHA = CO_MASTER then
      VAR_USUARIO := -1;
    if Uppercase(VAR_SENHA) = Uppercase(DmodG.QrBossMasSenha.Value) then
      VAR_USUARIO := -2;
    if ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
      VAR_USUARIO := -3;
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then FM_MASTER := 'V';
    //
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
    or (DmodG.QrMasterSolicitaSenha.Value = 0) then
    begin
      if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
      if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := ' '+VAR_LOGIN;
      if VAR_LOGOFFDE <> nil then VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
      FmMain.Enabled := True;
      Application.ProcessMessages;
      FmMLA.Hide;
      if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
      inc(VAR_FMPRINCIPALLIBERADO, 1);
      Exit;
    end;
    SenhaUsuario := DefineSenhaUsuario(EdLogin.Text);
    if DmodG.QrSenhas.RecordCount > 0 then
    begin
      VAR_USUARIO    := DmodG.QrSenhasNumero.Value;
      VAR_PERFIL     := DmodG.QrSenhasPerfil.Value;
      VAR_FUNCILOGIN := DmodG.QrSenhasFuncionario.Value;
      //
      if Uppercase(SenhaUsuario) = Uppercase(VAR_SENHA) then
      begin
        if VAR_LOGOFFDE <> nil then
        VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
        if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := DmodG.QrSenhasLogin.Value;
        if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
        if (DmodG.Privilegios(DmodG.QrSenhasPerfil.Value) = True) then
        begin
          //if CkRegerar.Checked then Forcar := True else Forcar := False;
          if QrGeraSenha <> nil then
            Mostra := DefineSenhaDia(VAR_USUARIO, ForcaSenha, QrGeraSenha)
          else Mostra := True;
          if Mostra then
          begin
            FmMLA.Hide;
            if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
            FmMain.Enabled := True;
            inc(VAR_FMPRINCIPALLIBERADO, 1);
          end else begin
            if (QrGeraSenha <> nil) then
            begin
              if LaAviso1 <> nil then
                LaAviso1.Caption := 'Erro ao definir senha do dia.';
              if LaAviso2 <> nil then
                LaAviso2.Caption := 'Erro ao definir senha do dia.';
            end;
            Exit;
          end;
        end else begin
          Geral.MB_Erro('Erro. Perfil inexistente. [1]');
          ZZTerminate := True;
          Application.Terminate;
          Exit;
        end;
      end else begin
        AvisoIndef(1);
        //
        VAR_SKINCONTA := VAR_SKINCONTA + 1;
        if VAR_SKINCONTA <= 2 then EdSenha.SetFocus
        else begin
          ZZTerminate := True;
          Application.Terminate;
        end;  
      end;
    end else begin
      Geral.MB_Aviso('Login n�o cadastrado.');
      EdLogin.SetFocus;
      Exit;
    end;
  finally
    DmodG.QrSenhas.Close;
    DmodG.QrBoss.Close;
  end;
end;

procedure TUnGOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit; FmMLA,
  FmMain: TForm; Componente: TComponent; ForcaSenha: Boolean; LaAviso1,
  LaAviso2: TLabel; QrGeraSenha, DmodFinQrCarts: TmySQLQuery;
  ShowModal: Boolean = False);
var
  Mostra: Boolean;
  SenhaUsuario, Filiais: String;
  I: Integer;
  NomeQr, Texto: String;
begin
  VAR_PERFIL := 0;
  //
  if EdLogin.Text = '' then
  begin
    EdLogin.SetFocus;
    //Exit;
  end;
  if EdSenha.Text = '' then
  begin
    EdSenha.SetFocus;
    //Exit;
  end;
  if EdEmpresa.Text = '' then
  begin
    EdEmpresa.SetFocus;
    //Exit;
  end;
  if DModG = nil then
  begin
    Geral.MB_Aviso('N�o foi poss�vel verificar o login e senha!' + sLineBreak +
    'Verifique se existe conex�o com o servidor!' + sLineBreak +
    'Module Geral n�o criado ainda!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    DModG.ReopenMaster('TUnGOTOy.2294');
    try
      // impedir usu�rio n�o habilitado a ver lan�amentos

      // TIREI DAQUI PARA N�O CHAMAR A PARTE FINANCEIRA NOS
      // APLICATIVOS QUE N�O USAM!!!!
      //DmodFinQrCarts.Close;
      if DmodFinQrCarts <> nil then
        DmodFinQrCarts.Close;
    except
    end;
    try
      // Compatibilidade
      // impedir usu�rio n�o habilitado a ver lan�amentos
      for I := 0 to Dmod.ComponentCount - 1 do
      begin
        if (Dmod.Components[i] is TmySQLQuery) then
        begin
          NomeQr := Uppercase(TmySQLQuery(Dmod.Components[i]).Name);
          if (NomeQr = 'QRCARTEIRAS') or (NomeQr = 'QRCARTS') then
            TmySQLQuery(Dmod.Components[i]).Close;
        end;
      end;
    except
    end;
    try
      // Compatibilidade
      // impedir usu�rio n�o habilitado a ver lan�amentos
      for I := 0 to FmPrincipal.ComponentCount - 1 do
      begin
        if (FmPrincipal.Components[i] is TmySQLQuery) then
        begin
          NomeQr := Uppercase(TmySQLQuery(FmPrincipal.Components[i]).Name);
          if (NomeQr = 'QRCARTEIRAS') or (NomeQr = 'QRCARTS') then
            TmySQLQuery(FmPrincipal.Components[i]).Close;
        end;
      end;
    except
    end;
    //
    VAR_LOGIN := Uppercase(EdLogin.Text);
    VAR_SENHA := Uppercase(EdSenha.Text);
    //VAR_FIRMA := EdEmpresa.ValueVariant;
    Filiais   := Trim(EdEmpresa.Text);
    VAR_LIB_FILIAIS := Geral.SoNumeroESinalEVirgula_TT(Filiais);
    if (VAR_LIB_FILIAIS <> Filiais) or (Filiais = '') then
    begin
      Screen.Cursor := crDefault;
      if Filiais <> '' then
        Geral.MB_Aviso('Empresa desconhecida!');
      EdEmpresa.SetFocus;
      Exit;
    end;
    //
    // Parei aqui
    //VAR_EMPRESA := DmodG.QrFiliaisCodigo.Value;
    //if VAR_FIRMA <> 0 then VAR_FIRMA := - VAR_FIRMA;
    DmodG.QrFiliLog.Close;
    DmodG.QrFiliLog.Database := Dmod.MyDB; // 2022-02-19
    DmodG.QrFiliLog.SQL.Clear;
    DmodG.QrFiliLog.SQL.Add('SELECT Codigo, Filial, ');
    // compatibilidade com DBCheck.EscolheCodigo
    DmodG.QrFiliLog.SQL.Add('IF(Tipo=0,RazaoSocial,Nome) NomeEmp ');
    //
    DmodG.QrFiliLog.SQL.Add('FROM entidades');
    DmodG.QrFiliLog.SQL.Add('WHERE Codigo < -10');
    DmodG.QrFiliLog.SQL.Add('AND Filial in (' + VAR_LIB_FILIAIS + ')');
    UMyMod.AbreQuery(DmodG.QrFiliLog, DmodG.QrFiliLog.Database);
    //  no after open � criado o array
    DmodG.QrFiliLog.First;
    //
    Filiais := VAR_LIB_FILIAIS;
    VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrFiliLogCodigo.Value);
    VAR_LIB_FILIAIS  := FormatFloat('0', DmodG.QrFiliLogFilial.Value);
    VAR_LIB_EMPRESAS_NOME := DmodG.QrFiliLogNomeEmp.Value;
    if DmodG.QrFiliLog.RecordCount > 1 then
    begin
      DmodG.QrFiliLog.Next;
      while not DmodG.QrFiliLog.Eof do
      begin
        VAR_LIB_EMPRESAS := VAR_LIB_EMPRESAS + ',' + FormatFloat('0', DmodG.QrFiliLogCodigo.Value);
        VAR_LIB_FILIAIS  := VAR_LIB_FILIAIS  + ',' + FormatFloat('0', DmodG.QrFiliLogFilial.Value);
        VAR_LIB_EMPRESAS_NOME := VAR_LIB_EMPRESAS_NOME + ', ' + DmodG.QrFiliLogNomeEmp.Value;
        DmodG.QrFiliLog.Next;
      end;
      VAR_LIB_EMPRESA_SEL := 0;
      VAR_LIB_EMPRESA_SEL_TXT := Geral.FF0(DmodG.QrFiliLog.RecordCount) + ' empresas logadas!';
    end else
    begin
      VAR_LIB_EMPRESA_SEL := DModG.QrFiliLogCodigo.Value;
      VAR_LIB_EMPRESA_SEL_TXT := DModG.QrFiliLogNomeEmp.Value;
    end;
    if (VAR_LIB_FILIAIS <> Filiais) or (VAR_LIB_FILIAIS = '') then
    begin
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Empresa n�o reconhecida!');
      EdEmpresa.SetFocus;
      Exit;
    end;

    //
    DefineSenhaAdmin;
    DefineSenhaBoss;
    if (DmodG.QrMasterSolicitaSenha.Value = 0)
    and (DmodG.QrMaster.RecordCount > 0)
    and (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
    begin
      VAR_LOGIN := '{?}';
      VAR_SENHA := '*';
    end;
    if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') {and (Filiais = '')} then
    begin
      if (Trim(DmodG.QrBossMasSenha.Value) = '')
      or (Trim(DmodG.QrBossMasLogin.Value) = '') then
      begin
        if Geral.MB_Pergunta('N�o h� cadastro da senha administradora '+
        'do aplicativo. Deseja cadastr�-la agora?')= ID_YES then begin
          VAR_BOSS        := '';
          VAR_SENHATXT    := '';
          VAR_NOMEEMPRESA := DmodG.QrBossEm.Value;
          VAR_CNPJEMPRESA := DmodG.QrBossCNPJ.Value;
          MyObjects.FormShow(TFmSenhaBoss, FmSenhaBoss);
          Dmod.QrUpd.Database := Dmod.MyDB;
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE master SET MasSenha=AES_ENCRYPT(:P0, :P1), ');
          Dmod.QrUpd.SQL.Add('MasLogin=:P2, Em=:P3, CNPJ=:P4, SitSenha=:P5');
          Dmod.QrUpd.SQL.Add('');
          Dmod.QrUpd.Params[00].AsString := VAR_SENHATXT;
          Dmod.QrUpd.Params[01].AsString := CO_USERSPNOW;
          Dmod.QrUpd.Params[02].AsString := VAR_BOSS;
          Dmod.QrUpd.Params[03].AsString := VAR_NOMEEMPRESA;
          Dmod.QrUpd.Params[04].AsString := VAR_CNPJEMPRESA;
          Dmod.QrUpd.Params[05].AsInteger := CO_USERSPSEQ;
          Dmod.QrUpd.ExecSQL;
          //
          ZZTerminate := True;
          Application.Terminate;
        end;
      end;
      if Componente <> nil then
        if Componente.Name = 'BtConfirma' then
        begin
          if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') and (Filiais = '') then
          begin
            Geral.MB_Erro('Informe um Login, uma senha e uma empresa!');
            EdLogin.SetFocus;
          end else if (Trim(VAR_LOGIN) = '') then
          begin
            Geral.MB_Erro('Informe um Login!');
            EdLogin.SetFocus;
          end else if (Trim(VAR_SENHA) = '') then
          begin
            Geral.MB_Erro('Informe uma senha!');
            EdSenha.SetFocus;
          end else
          begin
            Geral.MB_Erro('Informe uma empresa!');
            EdSenha.SetFocus;
          end;
        end;
      Exit;
    end else if Trim(VAR_LOGIN) = '' then
    begin
      EdLogin.SetFocus;
      Exit;
    end else if Trim(VAR_SENHA) = '' then
    begin
      EdSenha.SetFocus;
      Exit;
    end else if Filiais = '' then
    begin
      EdEmpresa.SetFocus;
      Exit;
    end;
    VAR_BOSSLOGIN := DmodG.QrBossMasLogin.Value;
    VAR_BOSSSENHA := Uppercase(DmodG.QrBossMasSenha.Value);
    if VAR_SENHA = CO_MASTER then
      VAR_USUARIO := -1;
    if VAR_SENHA = Uppercase(DmodG.QrBossMasSenha.Value) then
      VAR_USUARIO := -2;
    if ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
      VAR_USUARIO := -3;
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then FM_MASTER := 'V';
    //
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
    or (DmodG.QrMasterSolicitaSenha.Value = 0) then
    begin
      if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
      if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := ' '+VAR_LOGIN;
      if VAR_LOGOFFDE <> nil then VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
      FmMain.Enabled := True;
      Application.ProcessMessages;
      //
      if ShowModal = False then
        FmMLA.Hide
      else
        FmMLA.Close;
      //
      if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
      inc(VAR_FMPRINCIPALLIBERADO, 1);
      Exit;
    end;
    SenhaUsuario := DefineSenhaUsuario(EdLogin.Text);
    if DmodG.QrSenhas.RecordCount > 0 then
    begin
      VAR_USUARIO    := DmodG.QrSenhasNumero.Value;
      VAR_PERFIL     := DmodG.QrSenhasPerfil.Value;
      VAR_FUNCILOGIN := DmodG.QrSenhasFuncionario.Value;
      if Uppercase(SenhaUsuario) = Uppercase(VAR_SENHA) then
      begin
        (*
        Todos tem que passar por aqui

        if Uppercase(Application.Title) = 'PLANNING' then
        begin
        *)
          Dmod.QrUpdU.Database := Dmod.MyDB; // 2022-02-19
          Dmod.QrUpdU.SQL.Clear;
          Dmod.QrUpdU.SQL.Add('SELECT *');
          Dmod.QrUpdU.SQL.Add('FROM senhasits si');
          Dmod.QrUpdU.SQL.Add('LEFT JOIN entidades en ');
          Dmod.QrUpdU.SQL.Add('  ON si.Empresa=en.Codigo AND en.Codigo < -10');
          Dmod.QrUpdU.SQL.Add('WHERE en.Filial in (' + VAR_LIB_FILIAIS + ')');
          Dmod.QrUpdU.SQL.Add('AND si.Numero=:P0');
          Dmod.QrUpdU.Params[0].AsInteger := VAR_USUARIO;
          UMyMod.AbreQuery(Dmod.QrUpdU, Dmod.QrUpdU.Database);
          //
          if Dmod.QrUpdU.RecordCount = 0 then
          begin
            Geral.MB_Aviso('Empresa inv�lida!');
            EdEmpresa.SetFocus;
            Exit;
          end;
        //end;
        if VAR_LOGOFFDE <> nil then
        VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
        if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
        if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := DmodG.QrSenhasLogin.Value;
        if (DmodG.Privilegios(DmodG.QrSenhasPerfil.Value) = True) then
        begin
          //if CkRegerar.Checked then Forcar := True else Forcar := False;
          if QrGeraSenha <> nil then
            Mostra := DefineSenhaDia(VAR_USUARIO, ForcaSenha, QrGeraSenha)
          else Mostra := True;
          if Mostra then
          begin
            if ShowModal = False then
              FmMLA.Hide
            else
              FmMLA.Close;
            //
            if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
            FmMain.Enabled := True;
            Inc(VAR_FMPRINCIPALLIBERADO, 1);
          end else begin
            if (QrGeraSenha <> nil) then
            begin
              Texto := 'Erro ao definir senha do dia.';
              if LaAviso1 <> nil then
                LaAviso1.Caption := Texto;
              if LaAviso2 <> nil then
                LaAviso2.Caption := Texto;
            end;
            Exit;
          end;
        end else begin
          Geral.MB_Erro('Erro. Perfil inexistente. [2]');
          ZZTerminate := True;
          Application.Terminate;
          Exit;
        end;
      end else begin
        AvisoIndef(2);
        VAR_SKINCONTA := VAR_SKINCONTA + 1;
        if VAR_SKINCONTA <= 2 then EdSenha.SetFocus
        else begin
          ZZTerminate := True;
          Application.Terminate;
        end;
      end;
    end else begin
      Geral.MB_Aviso('Login n�o cadastrado.');
      EdLogin.SetFocus;
      Exit;
    end;
  finally
    Screen.Cursor := crDefault;
    DmodG.QrSenhas.Close;
    DmodG.QrBoss.Close;
  end;
  //
  MyObjects.Informa2(VAR_LA_PRINCIPAL2, VAR_LA_PRINCIPAL1, False,
    Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT);
end;

procedure TUnGOTOy.AnaliseSenhaMultiEmpresas(EdLogin, EdSenha: TEdit; FmMLA,
  FmMain: TForm; Componente: TComponent; ForcaSenha: Boolean;
  LaAviso1, LaAviso2: TLabel; QrGeraSenha: TmySQLQuery);
begin
  //DModG.ReopenMaster();
  AnaliseSenha(EdLogin, EdSenha, FmMLA, FmMain, Componente,
    ForcaSenha, LaAviso1, LaAviso2, QrGeraSenha);
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  DmodG.QrEmpresas.First;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrEmpresasCodigo.Value);
  VAR_LIB_FILIAIS  := FormatFloat('0', DmodG.QrEmpresasFilial.Value);
  DmodG.QrEmpresas.Next;
  while not DmodG.QrEmpresas.Eof do
  begin
    VAR_LIB_EMPRESAS := VAR_LIB_EMPRESAS + ',' + FormatFloat('0', DmodG.QrEmpresasCodigo.Value);
    VAR_LIB_FILIAIS  := VAR_LIB_FILIAIS  + ',' + FormatFloat('0', DmodG.QrEmpresasFilial.Value);
    DmodG.QrEmpresas.Next;
  end;
  VAR_LIB_EMPRESA_SEL := 0;
end;

procedure TUnGOTOy.AvisoIndef(ID: Integer);
begin
  Geral.MB_Aviso('CNPJ n�o definido ou Empresa n�o definida. ' + sLineBreak +
    'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.' + sLineBreak +
    'C�digo = ' + Geral.FF0(ID));
end;

function TUnGOTOy.DefineSenhaDia(Numero: Integer; Forcar: Boolean;
  Query: TMySQLQuery): Boolean;
var
  Nova: Double;
  Senha: String;
begin
  try
    Query.Close;
    Query.Database := Dmod.MyDB; // 2022-02-19
    Query.Params[0].AsInteger := Numero;
    UMyMod.AbreQuery(Query, Query.Database);
    Nova := 0;
    if Forcar or (int(Query.FieldByName('DataSenha').AsDateTime) < int(Date)) then
    begin
      Randomize;
      while (Nova = 0) or (Nova=Geral.IMV(Query.FieldByName('SenhaDia').AsString)) do
      begin
        Nova := Random(10000);
        Nova := int(Nova);
        if Nova > 9999 then Nova := 0;
      end;
      if Nova < 1000 then Nova := Nova * 10;
      Senha := FormatFloat('0000', Nova);
      Dmod.QrUpd.Database := Dmod.MyDB;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE senhas SET');
      Dmod.QrUpd.SQL.Add('DataSenha=:P0,');
      Dmod.QrUpd.SQL.Add('SenhaDia=:P1');
      Dmod.QrUpd.SQL.Add('WHERE Numero=:P2');
      Dmod.QrUpd.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, int(Date));
      Dmod.QrUpd.Params[1].AsString  := Senha;
      Dmod.QrUpd.Params[2].AsInteger := Numero;
      Dmod.QrUpd.ExecSQL;
      Geral.MB_Aviso('Sua senha do dia �:' + sLineBreak + sLineBreak +
        Senha + sLineBreak);
      if VAR_USACARTAOPONTO > 0 then
      begin
        {$IfNDef SemPontoFunci}
        MyObjects.FormCria(TFmPontoFunci, FmPontoFunci);
        with FmPontoFunci do
        begin
          EdFuncionario.Text := IntToStr(Query.FieldByName('Funcionario').AsInteger);
          CBFuncionario.KeyValue := Query.FieldByName('Funcionario').AsInteger;
          EdLogin.Text  := Query.FieldByName('Login').AsString;
          EdSenhaA.Text := Senha;
          EdSenhaP.Text := Query.FieldByName('Senha').AsString;
          MostraPonto;
          Destroy;
        end;
        {$EndIf}
      end;
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnGOTOy.OMySQLEstaInstalado(LaAviso1, LaAviso2: TLabel; ProgessBar:
  TProgressBar; BtEntra, BtConecta: TBitBtn): Boolean;
{
function TUnGOTOy.OMySQLEstaInstalado(StaticText: TStaticText; ProgessBar:
             TProgressBar): Boolean;
}
var
  (*CamExec: String;
  Servico: TServiceManager;*)
  StatusServico: Integer;
  Texto: String;
begin
  //Result := True;

  if (DModG.EhOServidor) or (VAR_SERVIDOR = 3) then // 3 = Ambos
  begin
    if not dmkPF.ExecutavelEstaRodando('mysqld-nt.exe') then
    if not dmkPF.ExecutavelEstaRodando('mysqld.exe') then
    if not dmkPF.ExecutavelEstaRodando('mysqld-opt.exe') then
    begin
      //Result := False;
      //
      if not dmkPF.ExecutavelEstaInstalado('MySQL Server') then
      begin
        (*
        if Geral.MB_Pergunta('O ' + Application.Title + ' n�o detectou a '+
        'instala��o do MySQL Server! Deseja instal�-lo agora?')=ID_YES then
          MyObjects.FormShow(TFmInstallMySQL41, FmInstallMySQL41);
        *)
        if Geral.MB_Pergunta('O ' + Application.Title + ' n�o detectou a ' +
        'instala��o do MySQL Server!' + sLineBreak +
        'Instale o MySQL Server e tente novamente!' + sLineBreak +
        'Este computador n�o � o servidor, mas um cliente?') = ID_YES then
        begin
          USQLDB.ConfiguraDB(True);
          Halt(0);
          Exit;
        end else
        begin
          Halt(0);
          Exit;
        end;

      end else begin
{$IfNDef sVerifiDB}
        MyObjects.FormCria(TFmServicoManager, FmServicoManager);
        FmServicoManager.ShowModal;
        StatusServico := FmServicoManager.FStatusServico;
        FmServicoManager.Destroy;
        //
        if StatusServico <> 2 then
          MyObjects.FormShow(TFmServerConnect, FmServerConnect);
{$EndIf}
      end;
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  if VAR_SERVIDOR = 0 then
    DModG.DefineServidorEIP();
////////////////////////////////////////////////////////////////////////////////
  if ProgessBar <> nil then VAR_CONNECTIPPROGRESS := ProgessBar;
  if LaAviso1 <> nil then VAR_CONNECTIPLABEL1TX := LaAviso1;
  if LaAviso2 <> nil then VAR_CONNECTIPLABEL2TX := LaAviso2;
  //VAR_PORTA := 3306; j� setado no Dmod
  // 21000 � o m�ximo. Mais que isso a conex�o falha automaticamente
  Result := ConectaAoServidor(VAR_IP, VAR_PORTA, 21);
  if Result then
  begin
    // Inicio Marco Arend
    Texto := 'Conex�o a porta '+ IntToStr(VAR_PORTA) +
      ' do IP ' + VAR_IP + ' realizada com sucesso!!';
    //
    if VAR_CONNECTIPLABEL1TX <> nil then VAR_CONNECTIPLABEL1TX.Caption := Texto;
    if VAR_CONNECTIPLABEL2TX <> nil then VAR_CONNECTIPLABEL2TX.Caption := Texto;
    if VAR_CONNECTIPLABEL1TX <> nil then VAR_CONNECTIPLABEL1TX.Update;
    if VAR_CONNECTIPLABEL2TX <> nil then VAR_CONNECTIPLABEL2TX.Update;
    //
    if VAR_CONNECTIPPROGRESS <> nil then
    begin
      VAR_CONNECTIPPROGRESS.Position  := 0;
      VAR_CONNECTIPPROGRESS.Update;
    end;
  end else
  begin
    Result := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    if BtEntra <> nil then
      BtEntra.Visible := False;
    if BtConecta <> nil then
      BtConecta.Visible := True;
  end;
end;

{$IFNDEF NAO_USA_RECIBO}
procedure TUnGOTOy.EmiteRecibo(Codigo, Emitente, Beneficiario: Integer; Valor, ValorP,
  ValorE: Double; NumRecibo, Referente, ReferenteP, ReferenteE: String;
  Data: TDateTime; Sit: Integer);
var
  Responsavel, LocalEData: String;
begin
  EnderecoDeEntidade(Emitente, -1);
  //
  if (Valor <> 0) and (ValorP <> 0) then
  begin
    Geral.MB_Aviso('Informados "Valor" e "ValorP". "ValorP" '+
    'ser� o considerado!');
  end;
  if Trim(DmodG.QrEnderecoRespons1.Value) <> CO_VAZIO then
    Responsavel := DmodG.QrEnderecoRespons1.Value
  else
    Responsavel := CO_VAZIO;
  //
  if Trim(DmodG.QrEnderecoRespons2.Value) <> CO_VAZIO then
  begin
    if Responsavel <> CO_VAZIO then
      Responsavel := Responsavel + ' ou '+ DmodG.QrEnderecoRespons2.Value
    else
      Responsavel := DmodG.QrEnderecoRespons2.Value;
  end;
  if Responsavel = CO_VAZIO then
    Responsavel := 'Respons�vel';
  //
  if (Valor <> 0) or (Sit = 4) then
  begin
    MyObjects.FormCria(TFmRecibo, FmRecibo);
    //
    with FmRecibo do
    begin
      FCodigo       := Codigo;
      FEmitente     := EMitente;
      FBeneficiario := Beneficiario;
      FDtRecibo     := Data;
      FDtPrnted     := DModG.ObtemAgora();

      // Mudei 2011-02-13 de Dmod para DModG
      EdNumero.Text   := NumRecibo;
      EdValor.Text    := Geral.FFT(Valor, 2, siPositivo);
      EdEmitente.Text := DmodG.QrEnderecoNOME_ENT.Value;
      EdECNPJ.Text    := ItemDeEndereco(DmodG.QrEnderecoNO_TIPO_DOC.Value + ': ', DmodG.QrEnderecoCNPJ_TXT.Value);
      EdERua.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
      EdENumero.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
      EdECompl.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
      EdEBairro.Text  := ItemDeEndereco('Bairro: ', DmodG.QrEnderecoBAIRRO.Value);
      EdECidade.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
      EdEUF.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
      EdECEP.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
      EdEPais.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
      EdETe1.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      Texto.Text      := Referente+'.';
      //
      EnderecoDeEntidade(Beneficiario, -1);
      //
      EdBeneficiario.Text := DmodG.QrEnderecoNOME_ENT.Value;
      EdBCNPJ.Text        := ItemDeEndereco(DmodG.QrEnderecoNO_TIPO_DOC.Value + ': ', DmodG.QrEnderecoCNPJ_TXT.Value);
      EdBRua.Text         := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
      EdBNumero.Text      := ItemDeEndereco('N�:', DmodG.QrEnderecoNUMERO_TXT.Value);
      EdBCompl.Text       := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
      EdBBairro.Text      := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
      EdBCidade.Text      := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
      EdBUF.Text          := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
      EdBCEP.Text         := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
      EdBPais.Text        := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
      EdBTe1.Text         := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      //
      FLocalEData_Atual := Geral.Maiusculas(FormatDateTime('dddd, dd" de "mmmm" de "yyyy', DModG.ObtemAgora),
                           Geral.EhMinusculas(DmodG.QrEnderecoCIDADE.Value, False));
      LocalEData        := Geral.Maiusculas(FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Data),
                           Geral.EhMinusculas(DmodG.QrEnderecoCIDADE.Value, False));
      //
      if DmodG.QrEnderecoCIDADE.Value <> '' then
      begin
        FLocalEData_Atual := DmodG.QrEnderecoCIDADE.Value + ', ' + FLocalEData_Atual;
        LocalEData        := DmodG.QrEnderecoCIDADE.Value + ', ' + LocalEData;
      end;
      //
      FLocalEData_Atual  := FLocalEData_Atual + '.';
      EdLocalData.Text   := LocalEData + '.';
      EdResponsavel.Text := Responsavel;
      // Fim 2011-02-13 de Dmod para DModG
      //
      ShowModal;
      Destroy;
    end;
  end else
  begin
    MyObjects.FormCria(TFmRecibos, FmRecibos);
    with FmRecibos do
    begin
      FCodigo       := Codigo;
      FEmitente     := Emitente;
      FBeneficiario := Beneficiario;
      FDtRecibo     := Data;
      FDtPrnted     := DModG.ObtemAgora();

      ///////
      // PARTE PESSOAL
      //////
      if Valor <> 0 then
      begin
        EnderecoDeEntidade(Beneficiario, -1);
        //
        if DmodG.QrEnderecoTipo.Value = 1 then
        begin
          EdNumeroP.Text  := NumRecibo;
          EdValorP.Text   := Geral.FFT(Valor, 2, siPositivo);
          EdPPagador.Text := DmodG.QrEnderecoNOME_ENT.Value;
          EdPCNPJC.Text   := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
          EdPRua.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
          EdPNumero.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
          EdPCompl.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
          EdPBairro.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
          EdPCidade.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
          EdPUF.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
          EdPCEP.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
          EdPPais.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
          EdPTe1.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
          TextoP.Text     := Referente;
        end else
        begin
          EdNumeroE.Text  := NumRecibo;
          EdValorE.Text   := Geral.FFT(Valor, 2, siPositivo);
          EdEPagador.Text := DmodG.QrEnderecoNOME_ENT.Value;
          EdECNPJC.Text   := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
          EdERuaC.Text    := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
          EdENumeroC.Text := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
          EdEComplC.Text  := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
          EdEBairroC.Text := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
          EdECidadeC.Text := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
          EdeUFC.Text     := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
          EdeCEPC.Text    := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
          EdePaisC.Text   := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
          EdeTe1C.Text    := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
          TextoE.Text     := Referente;
        end;
      end;
      // Emitente
      EnderecoDeEntidade(Emitente, -1);
      //
      if DmodG.QrEnderecoTipo.Value = 1 then
      begin
        EdEmitenteP.Text := DmodG.QrEnderecoNOME_ENT.Value;
        EdCNPJP.Text     := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
        EdPRuaE.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
        EdPNumeroE.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
        EdPComplE.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
        EdPBairroE.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
        EdPCidadeE.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
        EdPUFE.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
        EdPCEPE.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
        EdPPaisE.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
        EdPTe1E.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      end else
      begin
        EdEmitenteE.Text := DmodG.QrEnderecoNOME_ENT.Value;
        EdECNPJE.Text     := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
        EdERuaE.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
        EdENumeroE.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
        EdEComplE.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
        EdEBairroE.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
        EdECidadeE.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
        EdEUFE.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
        EdECEPE.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
        EdEPaisE.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
        EdETe1E.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      end;
      //
      if ValorP <> 0 then
      begin
        EnderecoDeEntidade(Beneficiario, -1);
        //
        EdNumeroP.Text  := NumRecibo + '-P';
        EdValorP.Text   := Geral.FFT(ValorP, 2, siPositivo);
        EdPPagador.Text := DmodG.QrEnderecoNOME_ENT.Value;
        EdPCNPJC.Text   := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
        EdPRua.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
        EdPNumero.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
        EdPCompl.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
        EdPBairro.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
        EdPCidade.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
        EdPUF.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
        EdPCEP.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
        EdPPais.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
        EdPTe1.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
        TextoP.Text     := ReferenteP;
      end;
      // Emitente
      EnderecoDeEntidade(Emitente, -1);
      //
      EdEmitenteP.Text := DmodG.QrEnderecoNOME_ENT.Value;
      EdCNPJP.Text     := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
      EdPRuaE.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
      EdPNumeroE.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
      EdPComplE.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
      EdPBairroE.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
      EdPCidadeE.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
      EdPUFE.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
      EdPCEPE.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
      EdPPaisE.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
      EdPTe1E.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      ///////
      //  PARTE EMPRESA
      //////
      if ValorE <> 0 then
      begin
        EnderecoDeEntidade(Beneficiario, -1);
        //
        EdNumeroE.Text  := NumRecibo + '-E';
        EdValorE.Text   := Geral.FFT(ValorE, 2, siPositivo);
        EdEPagador.Text := DmodG.QrEnderecoNOME_ENT.Value;
        EdECNPJC.Text   := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
        EdERuaC.Text    := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
        EdENumeroC.Text := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
        EdEComplC.Text  := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
        EdEBairroC.Text := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
        EdECidadeC.Text := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
        EdEUFC.Text     := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
        EdECEPC.Text    := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
        EdEPaisC.Text   := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
        EdETe1C.Text    := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
        TextoE.Text     := ReferenteE;
      end;
      // Emitente
      EnderecoDeEntidade(Emitente, -1);
      //
      EdEmitenteE.Text := DmodG.QrEnderecoNOME_ENT.Value;
      EdECNPJE.Text    := Geral.FormataCNPJ_TT(DModG.QrEnderecoCNPJ_CPF.Value);
      EdERuaE.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
      EdENumeroE.Text  := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
      EdEComplE.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
      EdEBairroE.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
      EdECidadeE.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
      EdEUFE.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
      EdECEPE.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
      EdEPaisE.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
      EdETe1E.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      //
      LocalEData := Geral.Maiusculas(FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Data),
                      Geral.EhMinusculas(DmodG.QrEnderecoCIDADE.Value, False));
      //
      if DmodG.QrEnderecoCIDADE.Value <> '' then
        LocalEData := DmodG.QrEnderecoCIDADE.Value + ', ' + LocalEData;
      //
      EdPLocalData.Text   := LocalEData + '.';
      EdELocalData.Text   := LocalEData + '.';
      EdPResponsavel.Text := Responsavel;
      EdEResponsavel.Text := Responsavel;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;
procedure TUnGOTOy.EmiteReciboMany(Tabela: String);
begin
  MyObjects.FormCria(TFmReciboMany, FmReciboMany);
  FmReciboMany.FRecibos := Tabela;
  FmReciboMany.ImprimeRecibos();
  FmReciboMany.Destroy;
end;

procedure TUnGOTOy.ImpressaoDeRecibo_AltDtPrnted(Codigo: Integer;
  DtPrnted: String);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'reciboimpcab', False, [
  'DtPrnted'], ['Codigo'], [
  DtPrnted], [Codigo], True);
end;

procedure TUnGOTOy.ImpressaoDeRecibo_ConfOuDesconf(Codigo, Controle: Integer; DtConfrmad: String);
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'reciboimpcab', False, [
  'DtConfrmad'], ['Codigo'], [
  DtConfrmad], [Codigo], True) then
  begin
    if Controle <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'reciboimpimp', False, [
      'DtConfrmad'], ['Controle'], [
      DtConfrmad], [Controle], True);
    end;
  end;
end;

(*
procedure TUnGOTOy.CadastraImpressaoDeRecibo_ReciboImp(const Recibo: String;
  const _DtRecibo, _DtPrnted: TDateTime; const CNPJCPF: String; const Emitente,
  Beneficiario: Integer; const Valor: Double; var Codigo: Integer);
var
  DtRecibo, DtPrnted, DtConfrmad: String;
  SQLType: TSQLType;
begin
  Codigo         := 0;
  DtRecibo       := Geral.FDT(_DtRecibo, 109);
  DtPrnted       := Geral.FDT(_DtPrnted, 109);
  //
  SQLType        := TSQLType.stIns;
  //
  Codigo := UMyMod.BPGS1I32('reciboimp', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'reciboimp', False, [
  'Recibo', 'DtRecibo', 'DtPrnted',
  'Emitente', 'Beneficiario', 'CNPJCPF',
  'Valor'], [
  'Codigo'], [
  Recibo, DtRecibo, DtPrnted,
  Emitente, Beneficiario, CNPJCPF,
  Valor], [
  Codigo], True) then
  begin
    if Geral.MB_Pergunta('O recibo foi impresso?') = ID_YES then
    begin
      DtConfrmad := Geral.FDT(DModG.ObtemAgora(), 109);
      ImpressaoDeRecibo_ConfOuDesconf(Codigo, DtConfrmad);
    end;
  end;
end;
*)

{$ENDIF}

function TUnGOTOy.SenhaDesconhecida(): Boolean;
  procedure TentaAlterarSenha(NomeBD: String);
  begin
    if not MudaSenhaBD('', 'root', False) then
    if not MudaSenhaBD(CO_SPDESCONHECIDA, 'root', False) then
    begin
      VAR_LOGIN := '';
      VAR_SENHA := '';
      MyObjects.Senha(TFmSenha, FmSenha, 0, '', '', '', '', '', True, '', 0, '');
      if not MudaSenhaBD(VAR_SENHA, VAR_LOGIN, True) then
      begin
        Geral.MB_Erro('O banco de dados possui senha desconhecida '+
        'e a aplica��o n�o poder� ser iniciada!');
        FmPrincipal.Close;
        Application.Terminate;
        Exit;
      end;
    end else Result := False;
    Dmod.ZZDB.Connected := False;
    Dmod.ZZDB.DatabaseName := NomeBD;
  end;
var
  NomeBD: String;
begin
  Result := True;
  if (VAR_BDSENHA = '') or (VAR_BDSENHA = CO_SPDESCONHECIDA) then
  begin
    try
      Dmod.ZZDB.Connected := False;
      NomeBD := Dmod.ZZDB.DatabaseName;
      Dmod.ZZDB.Host         := VAR_SQLHOST;
      Dmod.ZZDB.DatabaseName := 'mysql';
      Dmod.ZZDB.UserName     := 'root';
      Dmod.ZZDB.UserPassword := VAR_BDSENHA;
      Dmod.ZZDB.Port         := VAR_PORTA;
      Dmod.ZZDB.ConnectionTimeout := 10;
      try
        //Dmod.ZZDB.Connect;
        USQLDB.ConectaDB(Dmod.ZZDB, 'UnGOTOy.2980');
      except
        Geral.MB_Erro(
        'N�o foi poss�vel conectar o "ZZDB" para configura��o do mysql:' +
        sLineBreak + sLineBreak+
        'Database: ' + DMod.ZZDB.DatabaseName + sLineBreak +
        'Host: ' + DMod.ZZDB.Host + sLineBreak +
        'Porta: ' + FormatFloat('0', VAR_PORTA));
      end;
      if Dmod.ZZDB.Connected then
      begin
        Result := False;
        Dmod.ZZDB.Connected := False;
        Dmod.ZZDB.DatabaseName := NomeBD
      end else begin
        TentaAlterarSenha(NomeBD);
      end;
    except
      TentaAlterarSenha(NomeBD);
    end;
  end else
    Result := False;
end;

function TUnGOTOy.MudaSenhaBD(Senha, Usuario: String; MsgErr: Boolean): Boolean;
begin
  Result := False;
  Dmod.ZZDB.UserPassword := Senha;
  Dmod.ZZDB.UserName := Usuario;
  try
    Dmod.ZZDB.Connected := True;
    try
      Dmod.ZZDB.Connected := True;
      Dmod.QrUpd.Database := Dmod.ZZDB;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(DELETE_FROM + ' user ');//WHERE User="root" OR User=""');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO user SET User="root", ');
      Dmod.QrUpd.SQL.Add('Password=PASSWORD("'+VAR_BDSENHA+'"), ');
      Dmod.QrUpd.SQL.Add('Host="localhost",');
      Dmod.QrUpd.SQL.Add('Select_priv="Y",');
      Dmod.QrUpd.SQL.Add('Insert_priv="Y",');
      Dmod.QrUpd.SQL.Add('Update_priv="Y",');
      Dmod.QrUpd.SQL.Add('Delete_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_priv="Y",');
      Dmod.QrUpd.SQL.Add('Drop_priv="Y",');
      Dmod.QrUpd.SQL.Add('Reload_priv="Y",');
      Dmod.QrUpd.SQL.Add('Shutdown_priv="Y",');
      Dmod.QrUpd.SQL.Add('Process_priv="Y",');
      Dmod.QrUpd.SQL.Add('File_priv="Y",');
      Dmod.QrUpd.SQL.Add('Grant_priv="Y",');
      Dmod.QrUpd.SQL.Add('References_priv="Y",');
      Dmod.QrUpd.SQL.Add('Index_priv="Y",');
      Dmod.QrUpd.SQL.Add('Alter_priv="Y",');
      Dmod.QrUpd.SQL.Add('Show_db_priv="Y",');
      Dmod.QrUpd.SQL.Add('Super_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_tmp_table_priv="Y",');
      Dmod.QrUpd.SQL.Add('Lock_tables_priv="Y",');
      Dmod.QrUpd.SQL.Add('Execute_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_slave_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_client_priv="Y"');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO user SET User="root", ');
      Dmod.QrUpd.SQL.Add('Password=PASSWORD("'+VAR_BDSENHA+'"), ');
      Dmod.QrUpd.SQL.Add('Host="%",');
      Dmod.QrUpd.SQL.Add('Select_priv="Y",');
      Dmod.QrUpd.SQL.Add('Insert_priv="Y",');
      Dmod.QrUpd.SQL.Add('Update_priv="Y",');
      Dmod.QrUpd.SQL.Add('Delete_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_priv="Y",');
      Dmod.QrUpd.SQL.Add('Drop_priv="Y",');
      Dmod.QrUpd.SQL.Add('Reload_priv="Y",');
      Dmod.QrUpd.SQL.Add('Shutdown_priv="Y",');
      Dmod.QrUpd.SQL.Add('Process_priv="Y",');
      Dmod.QrUpd.SQL.Add('File_priv="Y",');
      Dmod.QrUpd.SQL.Add('Grant_priv="Y",');
      Dmod.QrUpd.SQL.Add('References_priv="Y",');
      Dmod.QrUpd.SQL.Add('Index_priv="Y",');
      Dmod.QrUpd.SQL.Add('Alter_priv="Y",');
      Dmod.QrUpd.SQL.Add('Show_db_priv="Y",');
      Dmod.QrUpd.SQL.Add('Super_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_tmp_table_priv="Y",');
      Dmod.QrUpd.SQL.Add('Lock_tables_priv="Y",');
      Dmod.QrUpd.SQL.Add('Execute_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_slave_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_client_priv="Y"');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('FLUSH PRIVILEGES');
      Dmod.QrUpd.ExecSQL;
      ///////////
      Dmod.QrUpd.Database := Dmod.MyDB;
      ///////////
      Result := True;
      ///////////
    except
      if MsgErr then
        Geral.MB_Erro('N�o foi poss�vel reconfigurar o banco de dados!');
    end;
  except
    //Geral.MB_Erro('Senha e/ou usu�rio invalidos!');
  end;
end;

function TUnGOTOy.ConectaAoServidor(IP: String; HostPort, CancelTimeMs: Word):
 Boolean;
var
  Servidor: Integer;
  Conectado, Continua: Boolean;
  //PError: array[0..255] of Char;
  Texto: String;
begin
  Continua := False;
  //
  Texto := 'Conectando a porta '+ IntToStr(HostPort) + ' do IP ' + IP + '...';
  if VAR_CONNECTIPLABEL1TX <> nil then VAR_CONNECTIPLABEL1TX.Caption := Texto;
  if VAR_CONNECTIPLABEL2TX <> nil then VAR_CONNECTIPLABEL2TX.Caption := Texto;
  if VAR_CONNECTIPLABEL1TX <> nil then VAR_CONNECTIPLABEL1TX.Update;
  if VAR_CONNECTIPLABEL2TX <> nil then VAR_CONNECTIPLABEL2TX.Update;
  //
  if VAR_CONNECTIPPROGRESS <> nil then
  begin
    VAR_CONNECTIPPROGRESS.Max       := CancelTimeMs;
    VAR_CONNECTIPPROGRESS.Position  := CancelTimeMs;
    VAR_CONNECTIPPROGRESS.Update;
  end;
  //
  Application.ProcessMessages;

(*
{$IFDEF VER230} //XE2
  Servidor := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
                HKEY_LOCAL_MACHINE);
  //
  if Servidor = 2 then
    Continua := True
  else
    Continua := MyObjects.Ping(IP);
  //
  if Continua then
  begin
    //N�o funciona com host => if not dmkPF.TestaPortaPorIP(HostPort, IP, nil) then
    if not dmkPF.TestaPortaPorHost(HostPort, IP) then
    begin
      Geral.MB_Erro('N�o foi poss�vel a conex�o com a porta: [' +
        Geral.FF0(HostPort) + ']' + sLineBreak + 'Erro retornado: "' +
        PError + '"' + sLineBreak + 'function TUnGOTOy.ConectaAoServidor()');
      Result := False;
      Exit;
    end else
      Conectado := True;
  end else
  begin
    Geral.MB_Erro('N�o foi poss�vel a conex�o ao IP: [' +IP+']' + sLineBreak +
      'Erro retornado: "' + PError + '"' + sLineBreak +
      'function TUnGOTOy.ConectaAoServidor()');
    Result := False;
    Exit;
  end;
{$ELSE}
*)
  // ini Delphi 28 Alexandria
  // N�o funciona
  Conectado := MyObjects.Ping(VAR_IP);
  //
  if not Conectado then
    Conectado := DmkPing.PingHost(VAR_IP, 10000);
  if not Conectado then
    Geral.MB_Info('N�o foi poss�vel pingar o IP ' + VAR_IP);
{
  //Loop infinito no Delphi 28 Alexandria
  Conectado := dmkPF.IsConnectedToNet(IP, HostPort, CancelTimeMs, 0, PError);
}

//{$ENDIF}
  Result := Conectado;
end;

function TUnGOTOy.EnderecoDeEntidade(Entidade, TipoDeEndereco: Integer): String;
begin
  // 2011-02-13 Mudei Dmod para DModG
  DModG.ReopenEndereco(Entidade);
  if DmodG.QrEndereco.RecordCount > 0 then
  begin
    case TipoDeEndereco of
      0: Result := DmodG.QrEnderecoE_ALL.Value;
      else Result := '************';
    end;
  end else Result := '';
  // fim 2011-02-13
  // ATEN��O:
  // N�o fechar
  //DmodG.QrEndereco.Close;
end;

function TUnGOTOy.EnderecoDeEntidade1(Entidade, TipoDeEndereco: Integer): String;
begin
  DModG.ReopenEndereco(Entidade);
  if DmodG.QrEndereco.RecordCount > 0 then
  begin
    case TipoDeEndereco of
      0: Result := DmodG.QrEnderecoE_ALL.Value;
      else Result := '************';
    end;
  end else Result := '';
  // N�o fechar
  //Dmod.QrEndereco.Close;
end;

function TUnGOTOy.EnderecoDeEntidade2(Entidade, TipoDeEndereco: Integer): String;
begin
  DmodG.QrEndereco2.Close;
  DModG.QrEndereco2.Database := Dmod.MyDB; // 2022-02-19
  DmodG.QrEndereco2.Params[0].AsInteger := Entidade;
  UMyMod.AbreQuery(DmodG.QrEndereco2, DmodG.QrEndereco2.Database);
  if DmodG.QrEndereco2.RecordCount > 0 then
  begin
    case TipoDeEndereco of
      0: Result := DmodG.QrEndereco2E_ALL.Value;
      else Result := '************';
    end;
  end else Result := '';
  // N�o fechar
  //Dmod.QrEndereco.Close;
end;

function TUnGOTOy.ItemDeEndereco(Item, Valor: String): String;
begin
  if Item <> '' then Result := Item + ' ' + Valor
  else Result := Valor;
end;

function TUnGOTOy.DefinePathMySQL: Boolean;
begin
  Result := False;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.Database := Dmod.MyDB; // 2022-02-19
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW VARIABLES');
  UMyMod.AbreQuery(Dmod.QrAux, Dmod.QrAux.Database);
  //
  VAR_MYSQLBASEDIR := '';
  VAR_MYSQLDATADIR := '';
  //
  while not Dmod.QrAux.Eof do
  begin
    if Dmod.QrAux.FieldByName('Variable_name').AsString = 'basedir' then
      VAR_MYSQLBASEDIR := Dmod.QrAux.FieldByName('Value').AsString;
    if Dmod.QrAux.FieldByName('Variable_name').AsString = 'datadir' then
      VAR_MYSQLDATADIR := Dmod.QrAux.FieldByName('Value').AsString;
    Dmod.QrAux.Next;
    if (VAR_MYSQLBASEDIR <> '') and  (VAR_MYSQLDATADIR <> '') then
    begin
      Result := True;
      Break;
    end;
  end;
  //if not Result then
end;

function TUnGOTOy.DefineBaseDados: TmySQLDataBase;
begin
  if VAR_GOTOMySQLDBNAME = nil then Result := VAR_GOTOMySQLDBNAME2
  else result := VAR_GOTOMySQLDBNAME;
end;

function TUnGOTOy.Fechar(Caption: String) : TCloseAction;
var
  TextoMB : PChar;
begin
  Result := caFree;
  if Caption <> CO_TRAVADO then
  begin
    if Caption = CO_DESISTE then
    begin
      Geral.MB_Aviso('Feche o formul�rio pelo bot�o "Desiste" ou "Sair"!');
      Result := caNone;
    end else begin
      TextoMB := PChar(SMLA_AVISOFORMCLOSE+Caption+'.');
      Geral.MB_Aviso(TextoMB);
      Result := caNone;
    end;
  end;
end;

function TUnGOTOy.Fechar(SQLType: TSQLType): TCloseAction;
var
  TextoMB : PChar;
begin
  Result := caFree;
  if SQLType <> stLok then
  begin
    if SQLType = stUnd then // Desiste
    begin
      Geral.MB_Aviso('Feche o formul�rio pelo bot�o "Desiste" ou "Sair"!');
      Result := caNone;
    end else begin
      TextoMB := PChar(SMLA_AVISOFORMCLOSE + DmkEnums.NomeTipoSQL(SQLType) + '.');
      Geral.MB_Aviso(TextoMB);
      Result := caNone;
    end;
  end;
end;

function TUnGOTOy.FiltroNeg(): String;
begin
  case VAR_GOTONEG of
    gotoNeg: Result := ' < 0 ';
    gotoNiZ: Result := ' <= 0 ';
    gotoAll: Result := ' ';
    gotoNiP: Result := ' <> 0 ';
    gotoPiZ: Result := ' >= 0 ';
    gotoPos: Result := ' > 0 ';
  end;
end;

procedure TUnGOTOy.BotoesSb(SQLType: TSQLType; Form: TForm);
var
  Acao: Boolean;
  i: Integer;
begin
  if SQLType = stLok then Acao := True else Acao := False;
  for i := 0 to Form.ComponentCount -1 do
  begin
    if  ((Form.Components[i].Name = 'SbNovo')
    or  (Form.Components[i].Name = 'SbNumero')
    or  (Form.Components[i].Name = 'SbNome')
    or  (Form.Components[i].Name = 'SbQuery'))
    then begin
      if (Form.Components[i] is TBitBtn) then
        TBitBtn(Form.Components[i]).Enabled := Acao;
      if (Form.Components[i] is TSpeedButton) then
        TSpeedButton(Form.Components[i]).Enabled := Acao;
    end;
  end;
end;

end.



