unit MyInis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkEdit, dmkImage, UnDmkEnums,
  UnDmkProcFunc;

type
  TFmMyInis = class(TForm)
    Panel1: TPanel;
    EdMySQLData1: TdmkEdit;
    Label4: TLabel;
    Label1: TLabel;
    EdMySQLData2: TdmkEdit;
    Label2: TLabel;
    EdMyIniPath: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraDiretoriosMySQL(var MySQLData, MyIniPath: String);
  public
    { Public declarations }
  end;

  var
  FmMyInis: TFmMyInis;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmMyInis.BtOKClick(Sender: TObject);
begin
  Geral.WriteAppKeyLM2('MySQLData1', Application.Title, EdMySQLData1.Text, ktString);
  Geral.WriteAppKeyLM2('MySQLData2', Application.Title, EdMySQLData2.Text, ktString);
  Geral.WriteAppKeyLM2('MyIniPath', Application.Title, EdMyIniPath.Text, ktString);
  Close;
end;

procedure TFmMyInis.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMyInis.ConfiguraDiretoriosMySQL(var MySQLData, MyIniPath: String);
begin
  MySQLData := '';
  MyIniPath := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SHOW VARIABLES LIKE "datadir"',
    '']);
  MySQLData := Dmod.QrAux.FieldByName('Value').AsString;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SHOW VARIABLES LIKE "basedir"',
    '']);
  MyIniPath := Dmod.QrAux.FieldByName('Value').AsString;
  //
  if Pos('/', MySQLData) > 0 then
    MySQLData := dmkPF.InverteBarras(MySQLData);
  if Pos('/', MyIniPath) > 0 then
    MyIniPath := dmkPF.InverteBarras(MyIniPath);
  //
  Dmod.MyDB.Disconnect;
end;

procedure TFmMyInis.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMyInis.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMyInis.FormCreate(Sender: TObject);
var
  MyIniPath, MySQLData: String;
begin
  ImgTipo.SQLType := stLok;
  //
  (*
  if Dmod.MyDB.Connected then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW VARIABLES');
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    while not Dmod.QrAux.Eof do
    begin
      if (AnsiCompareText(Dmod.QrAux.FieldByName('Variable_name').AsString, 'datadir') = 0) then
        MySQLData := Dmod.QrAux.FieldByName('Value').AsVariant;
      if (AnsiCompareText(Dmod.QrAux.FieldByName('Variable_name').AsString, 'basedir') = 0) then
        MyIniPath := Dmod.QrAux.FieldByName('Value').AsVariant;
      Dmod.QrAux.Next;
    end;
  end;
  *)
  ConfiguraDiretoriosMySQL(MySQLData, MyIniPath);
  //
  EdMySQLData1.Text := Geral.ReadAppKeyLM('MySQLData1', Application.Title, ktString, MySQLData);
  EdMySQLData2.Text := Geral.ReadAppKeyLM('MySQLData2', Application.Title, ktString, MySQLData);
  EdMyIniPath.Text  := Geral.ReadAppKeyLM('MyIniPath', Application.Title, ktString, MyIniPath);
end;

procedure TFmMyInis.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdMySQLData1);
end;

procedure TFmMyInis.SpeedButton2Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdMySQLData2);
end;

procedure TFmMyInis.SpeedButton3Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdMyIniPath);
end;

end.
