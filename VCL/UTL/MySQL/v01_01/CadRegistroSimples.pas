unit CadRegistroSimples;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, UnDmkEnums, frxClass, frxDBSet;

type
  TFmCadRegistroSimples = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Panel5: TPanel;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    DBGrid1: TdmkDBGrid;
    frxDsCad: TfrxDBDataset;
    frxCAD_LISTA_003: TfrxReport;
    SbImprime: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbCadBeforePost(DataSet: TDataSet);
    procedure TbCadDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbCadAfterPost(DataSet: TDataSet);
    procedure frxCAD_LISTA_003GetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
    FNovoCodigo: TNovoCodigo;
    FTbCad: TmySQLTable;
    //
    procedure ReabrePesquisa();
  end;

  var
  FmCadRegistroSimples: TFmCadRegistroSimples;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmCadRegistroSimples.BtSaidaClick(Sender: TObject);
begin
  case FNovoCodigo of
    ncControle:
      VAR_CADASTRO := FTbCad.FieldByName('Codigo').AsInteger;
    ncGerlSeq1:
      VAR_CADASTRO := FTbCad.FieldByName('Codigo').AsInteger;
  end;
  Close;
end;

procedure TFmCadRegistroSimples.DBGrid1DblClick(Sender: TObject);
begin
  FTbCad.Edit;
end;

procedure TFmCadRegistroSimples.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmCadRegistroSimples.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmCadRegistroSimples.FormCreate(Sender: TObject);
begin
  FCodigo     := 0;
  FNovoCodigo := ncIdefinido;
end;

procedure TFmCadRegistroSimples.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadRegistroSimples.frxCAD_LISTA_003GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
    Value := LaTitulo1A.Caption
  else
  if VarName = 'VARF_CAMPO_A' then
  begin
    if FTbCad.Fields.Count = 3 then
      Value := FTbCad.Fields[2].FieldName
    else
      Value := '';
  end
  else
  if VarName = 'VARF_VALOR_A' then
  begin
    if FTbCad.Fields.Count = 3 then
      Value := FTbCad.Fields[2].AsString
    else
      Value := '';
  end
  else
end;

procedure TFmCadRegistroSimples.ReabrePesquisa();
var
  Texto, SQL: String;
  K, N, I: Integer;
begin
  QrPesq.Close;
  Texto := EdPesq.Text;
  K := Length(Texto);
  N := TBTam.Position;
  if N < 3 then
    N := 3;
  if K >= 3 then
  begin
    SQL := '';
    for I := 2 to K - N + 1 do
      SQL := SQL + 'OR (Nome LIKE "%' + Copy(Texto, I, N) + '%")' + sLineBreak;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM ' + FTbCad.TableName,
    'WHERE (Nome LIKE "%' + Copy(Texto, 1, N) + '%")',
    SQL,
    '']);
    if FCodigo <> 0 then
      QrPesq.Locate('Codigo', FCodigo, []);
  end;
end;

procedure TFmCadRegistroSimples.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxCAD_LISTA_003, LaTitulo1A.Caption);
end;

procedure TFmCadRegistroSimples.TbCadAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := FTbCad.FieldByName('Codigo').AsInteger;
end;

procedure TFmCadRegistroSimples.TbCadBeforePost(DataSet: TDataSet);
begin
  if FTbCad.State = dsInsert then
  begin
    case FNovoCodigo of
      ncControle: FTbCad.FieldByName('Codigo').AsInteger := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, FTbCad.TableName, 'Codigo', [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: FTbCad.FieldByName('Codigo').AsInteger := UMyMod.BPGS1I32(
        FTbCad.TableName, 'Codigo', '', '', tsDef, stIns, 0);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
        Halt(0);
        Exit;
      end;
    end;
  end;
end;

procedure TFmCadRegistroSimples.TbCadDeleting(Sender: TObject; var Allow: Boolean);
begin
  Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmCadRegistroSimples.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
