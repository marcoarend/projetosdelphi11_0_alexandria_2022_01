unit GModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) mySQLDbTables;

type
  TGMod = class(TDataModule)
    mySQLQuery1: TmySQLQuery;
    mySQLDatabase1: TmySQLDatabase;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
    procedure CriaQv_GOTOy;

  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo

  public
    { Public declarations }
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
  end;

var
  GMod: TGMod;
  QvAgora     : TmySQLQuery;
  QvLocX      : TmySQLQuery;
  QvSelX      : TmySQLQuery;
  QvCountX    : TmySQLQuery;
  QvLivreX    : TmySQLQuery;
  QvDuplicStrX: TmySQLQuery;
  QvDuplicIntX: TmySQLQuery;
  QvInsLogX   : TmySQLQuery;
  QvDelLogX   : TmySQLQuery;
  QvUpdX      : TmySQLQuery;
  //
  QvAllY      : TmySQLQuery;
  QvUpdY      : TmySQLQuery;
  QvLivreY    : TmySQLQuery;
  QvUser      : TmySQLQuery;
  QvLocY      : TmySQLQuery;
  QvCountY    : TmySQLQuery;
  QvRecCountY : TmySQLQuery;
  QvSelY      : TmySQLQuery;
  QvDuplicStrY: TmySQLQuery;
  QvDuplicIntY: TmySQLQuery;
  QvInsLogY   : TmySQLQuery;
  QvDelLogY   : TmySQLQuery;

implementation

uses UnInternalConsts;

{$R *.DFM}

procedure TGMod.CriaQv_GOTOy;
begin
  QvAgora := TmySQLQuery.Create(Gmod);
  QvAgora.DataBase := VAR_GOTOMySQLDBNAME;
  QvAgora.SQL.Add('SELECT NOW() Agora');
  //
  QvLocX := TmySQLQuery.Create(Gmod);
  QvLocX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvUpdX := TmySQLQuery.Create(Gmod);
  QvUpdX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvSelX := TmySQLQuery.Create(Gmod);
  QvSelX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvCountX := TmySQLQuery.Create(Gmod);
  QvCountX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvDuplicStrX := TmySQLQuery.Create(Gmod);
  QvDuplicStrX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvDuplicIntX := TmySQLQuery.Create(Gmod);
  QvDuplicIntX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvInsLogX := TmySQLQuery.Create(Gmod);
  QvInsLogX.DataBase := VAR_GOTOMySQLDBNAME;
  QvInsLogX.SQL.Add('INSERT INTO logs SET Data=NOW(), Tipo=:P0, ');
  QvInsLogX.SQL.Add('Usuario=:P1, ID=:P2');
  //
  QvDelLogX := TmySQLQuery.Create(Gmod);
  QvDelLogX.DataBase := VAR_GOTOMySQLDBNAME;
  QvDelLogX.SQL.Add(DELETE_FROM + ' logs WHERE Tipo=:P0');
  QvDelLogX.SQL.Add('AND Usuario=:P1 AND ID=:P2');
  //
  QvInsLogY := TMySQLQuery.Create(Gmod);
  QvInsLogY.DataBase := VAR_GOTOMySQLDBNAME;
  QvInsLogY.SQL.Add('INSERT INTO logs SET Data=NOW(), Tipo=:P0, ');
  QvInsLogY.SQL.Add('Usuario=:P1, ID=:P2');
  //
  QvDelLogY := TMySQLQuery.Create(Gmod);
  QvDelLogY.DataBase := VAR_GOTOMySQLDBNAME;
  QvDelLogY.SQL.Add(DELETE_FROM + ' logs WHERE Tipo=:P0');
  QvDelLogY.SQL.Add('AND Usuario=:P1 AND ID=:P2');
  //
  QvLivreX := TmySQLQuery.Create(Gmod);
  QvLivreX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvLocY := TmySQLQuery.Create(Gmod);
  QvLocY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvUser := TmySQLQuery.Create(Gmod);
  QvUser.DataBase := VAR_GOTOMySQLDBNAME;
  QvUser.SQL.Add('SELECT Login FROM senhas');
  QvUser.SQL.Add('WHERE Numero=:Usuario');
  //
  QvCountY := TmySQLQuery.Create(Gmod);
  QvCountY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvUpdY := TmySQLQuery.Create(Gmod);
  QvUpdY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvLivreY := TmySQLQuery.Create(Gmod);
  QvLivreY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvRecCountY := TmySQLQuery.Create(Gmod);
  QvRecCountY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvSelY := TmySQLQuery.Create(Gmod);
  QvSelY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvDuplicStrY := TmySQLQuery.Create(Gmod);
  QvDuplicStrY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvDuplicIntY := TmySQLQuery.Create(Gmod);
  QvDuplicIntY.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvAllY := TmySQLQuery.Create(Self);
  QvAllY.DataBase := VAR_GOTOMySQLDBNAME;
  //
end;

procedure TGMod.DataModuleCreate(Sender: TObject);
begin
  CriaQv_GOTOy;
end;


procedure TGMod.DefineProperties(Filer: TFiler);  // Alexandria -> Tokyo
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TGMod.ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TGMod.WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.

