unit DBReplicaParte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmDBReplicaParte = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    QrSorc: TmySQLQuery;
    DBSorc: TmySQLDatabase;
    QrDest: TmySQLQuery;
    DBDest: TmySQLDatabase;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    EdSorcDatabaseName: TdmkEdit;
    Label2: TLabel;
    EdSorcHost: TdmkEdit;
    Label3: TLabel;
    EdSorcPorta: TdmkEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdDestDatabaseName: TdmkEdit;
    EdDestHost: TdmkEdit;
    EdDestPorta: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FArqReplic1, FArqReplic2: String;
    //
    function ImportaTodaTabela(TabSorc, TabDest: String): String;
  public
    { Public declarations }
  end;

  var
  FmDBReplicaParte: TFmDBReplicaParte;

implementation

uses UnMyObjects, Module, UMySQLModule, UnDmkProcFunc, MyListas, UnMyLinguas;

{$R *.DFM}

procedure TFmDBReplicaParte.BtOKClick(Sender: TObject);
var
  SQL, TabNome, TabBase: String;
  Tabelas: TList;
  DefTabela: TTabelas;
  I: Integer;
begin
  DMod.MyDB.Connected := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Conectando base de dados origem');
  DBSorc.Connected := False;
  DBSorc.DatabaseName := EdSorcDatabaseName.Text;
  DBSorc.Host := EdSorcHost.Text;
  DBSorc.Port := EdSorcPorta.ValueVariant;
  DBSorc.UserName := DMod.MyDB.UserName;
  DBSorc.UserPassword := DMod.MyDB.UserPassword;
  DBSorc.Connect;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Conectando base de dados destino');
  DBDest.Connected := False;
  DBDest.DatabaseName := EdDestDatabaseName.Text;
  DBDest.Host := EdDestHost.Text;
  DBDest.Port := EdDestPorta.ValueVariant;
  DBDest.UserName := DMod.MyDB.UserName;
  DBDest.UserPassword := DMod.MyDB.UserPassword;
  DBDest.Connect;
  //
  QrSorc.Close;
  QrSorc.Database := DBSorc;
  //
  QrDest.Close;
  QrDest.Database := DBDest;
  //
  {
  QrDIns.Close;
  QrDIns.Database := DBDest;
  }
  //
  Tabelas := TList.Create;
  MyList.CriaListaReplicacoes(DMod.MyDB, Tabelas);
  try
    PB1.Max := Tabelas.Count;
    PB1.Position := 0;
    for I := 0 to Tabelas.Count -1 do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Recriando tabela "' + TabNome + '"');
      DefTabela := Tabelas[i];
      TabNome := DefTabela.TabCria;
      TabBase := DefTabela.TabBase;
      //Tem_Del := DefTabela.Tem_Del;
      if TabBase = '' then
        TabBase := TabNome;
      //
      ImportaTodaTabela(TabBase, TabNome);
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Replicação finalizada!');
  finally
    Tabelas.Free;
  end;
  //
  DMod.MyDB.Connected := True;
end;

procedure TFmDBReplicaParte.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDBReplicaParte.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDBReplicaParte.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FArqReplic1 := 'C:\Dermatek\Web\' + Application.Title + '\Conf';
  FArqReplic2 := 'C:/Dermatek/Web/' + Application.Title + '/Data';
  ForceDirectories(FArqReplic1);
  ForceDirectories(FArqReplic2);
  FArqReplic1 := FArqReplic1 + '\SQL_%s.%s';
  FArqReplic2 := FArqReplic2 + '/SQL_%s.%s';
end;

procedure TFmDBReplicaParte.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmDBReplicaParte.ImportaTodaTabela(TabSorc, TabDest: String): String;
  function EhTabelaDeLct(NomeTb: String): Boolean;
  var
    nTbI, nTbM: String;
    nTbF: Char;
  begin
    nTbI := Copy(NomeTb, 1, 3);
    nTbM := Copy(NomeTb, 4, Length(NomeTb)-4);
    nTbF := NomeTb[Length(NomeTb)];
    //
    Result := (nTbI = 'lct') and
    (nTbF in ['a','b','d']) and
    (nTbM = Geral.SoNumero_TT(nTbM));
  end;
var
  Arq: String;
  Campos, Prefix: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando tabela "' + TabSorc + '"');
  QrSorc.Close;
  Campos := '';
  Prefix := '';
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    Uppercase(TabDest) + ' - Obtendo campos da tabela destino...');
  Campos := UMyMod.ObtemCamposDeTabelaIdentica(DBDest, TabDest, Prefix);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    Uppercase(TabDest) + ' - Exportando para arquivo temporário...');
  Arq := Format(FArqReplic2, [TabDest, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  QrSorc.SQL.Clear;
  QrSorc.SQL.Add('SELECT '+Campos);
  QrSorc.SQL.Add('FROM '+ Lowercase(TabSorc));
  (*
  if CkLct.Checked then
  begin
    if (Lowercase(TabSorc) = LAN_CTOS) or EhTabelaDeLct(Lowercase(TabSorc)) then
      QrSorc.SQL.Add('WHERE Sit < 2 AND Tipo = 2'); // Somente abertos
  end;
  *)
  QrSorc.SQL.Add('INTO OUTFILE "' + Arq + '"');
  QrSorc.ExecSQL;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    Uppercase(TabDest) + ' - Excluindo registros duplicados na web...');
  QrDest.SQL.Clear;
  QrDest.SQL.Add(DELETE_FROM + Lowercase(TabDest));
  Application.ProcessMessages;
  QrDest.ExecSQL;
  //
  //Arq := Format(FArqReplic1, [TabDest, 'txt']);
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    Uppercase(TabDest) + ' - Importando de arquivo temporário...');
  QrDest.SQL.Clear;
  QrDest.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  QrDest.SQL.Add('INTO Table ' + TabDest);
  //
  QrDest.ExecSQL;
  //
(*
  QrSorc.SQL.Clear;
  QrSorc.SQL.Add('UPDATE '+lowercase(TabSorc)+' SET AlterWeb=0');
  QrSorc.ExecSQL;
*)
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    Uppercase(TabDest) + ' - Replicada!');
end;

end.
