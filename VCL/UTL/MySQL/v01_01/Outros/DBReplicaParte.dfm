object FmDBReplicaParte: TFmDBReplicaParte
  Left = 339
  Top = 185
  Caption = 'REP-MySQL-001 :: Replicar Parcialmente Base de Dados'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 460
        Height = 32
        Caption = 'Replicar Parcialmente Base de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 460
        Height = 32
        Caption = 'Replicar Parcialmente Base de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 460
        Height = 32
        Caption = 'Replicar Parcialmente Base de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 308
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 308
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 308
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 8
          Top = 4
          Width = 277
          Height = 97
          Caption = ' Base de dados de origem: '
          TabOrder = 0
          object Label1: TLabel
            Left = 24
            Top = 36
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object Label2: TLabel
            Left = 24
            Top = 60
            Width = 25
            Height = 13
            Caption = 'Host:'
          end
          object Label3: TLabel
            Left = 188
            Top = 60
            Width = 28
            Height = 13
            Caption = 'Porta:'
          end
          object EdSorcDatabaseName: TdmkEdit
            Left = 60
            Top = 32
            Width = 205
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdSorcHost: TdmkEdit
            Left = 60
            Top = 56
            Width = 125
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdSorcPorta: TdmkEdit
            Left = 224
            Top = 56
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object GroupBox3: TGroupBox
          Left = 292
          Top = 4
          Width = 277
          Height = 97
          Caption = ' Base de dados de destino: '
          TabOrder = 1
          object Label4: TLabel
            Left = 24
            Top = 36
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object Label5: TLabel
            Left = 24
            Top = 60
            Width = 25
            Height = 13
            Caption = 'Host:'
          end
          object Label6: TLabel
            Left = 188
            Top = 60
            Width = 28
            Height = 13
            Caption = 'Porta:'
          end
          object EdDestDatabaseName: TdmkEdit
            Left = 60
            Top = 32
            Width = 205
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDestHost: TdmkEdit
            Left = 60
            Top = 56
            Width = 125
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDestPorta: TdmkEdit
            Left = 224
            Top = 56
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 356
    Width = 784
    Height = 66
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 49
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 32
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrSorc: TmySQLQuery
    Database = DBSorc
    Left = 588
    Top = 12
  end
  object DBSorc: TmySQLDatabase
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    Left = 560
    Top = 12
  end
  object QrDest: TmySQLQuery
    Database = DBDest
    Left = 588
    Top = 40
  end
  object DBDest: TmySQLDatabase
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    Left = 560
    Top = 40
  end
end
