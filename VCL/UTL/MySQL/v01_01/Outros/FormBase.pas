unit BancosDados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, Registry, UnInternalConsts, UnDmkEnums, dmkEdit, UnMLAGeral;

type
  TFmBancoDados = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    BtOK: TBitBtn;
    BtExclui: TBitBtn;
    Panel5: TPanel;
    Grade: TStringGrid;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    BtAbrir: TBitBtn;
    PainelAguarde: TPanel;
    Label1: TLabel;
    Progress: TProgressBar;
    Timer1: TTimer;
    EdPathMyDB: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure GradeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FMaxTempo, FContaTempo: Integer;
    FCaminhoDB: String;
    FMeusDBs: TMeusDBs;
    procedure LimpaEmpresas;
    procedure SalvaLista;
    procedure CriaArquivoSym(FolderDB, NameDB: String);
  public
    { Public declarations }
  end;

  var
  FmBancoDados: TFmBancoDados;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, DmkDAC_PF, CreditoConsts;

{$R *.DFM}

procedure TFmBancoDados.BtAbrirClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdPathMyDB);
end;

procedure TFmBancoDados.BtExcluiClick(Sender: TObject);
begin
  MyObjects.ExcluiLinhaStringGrid(Grade);
  Grade.SetFocus;
  //Grade.Col := 1;
end;

procedure TFmBancoDados.BtOKClick(Sender: TObject);
var
  Caminho: String;
  i: Integer;
  Existe: Boolean;
begin
  Existe  := False;
  Caminho := EdPathMyDB.Text;
  //
  if Caminho = '' then
    Caminho := '[NENHUM]';
  //
  Geral.WriteAppKey('PathMyDB', Application.Title, Caminho, ktString, HKEY_LOCAL_MACHINE);
  //
  if Caminho = '[NENHUM]' then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW DATABASES');
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, DMod.MyDB);
    //
    while not Dmod.QrAux.Eof do
    begin
      if Uppercase(Dmod.QrAux.FieldByName('Database').AsString ) =
      Uppercase(TMeuDB) then
      begin
        Existe := True;
        Break;
      end else Dmod.QrAux.Next;
    end;
    //
    if not Existe then
    begin
      UMyMod.ExecutaMySQLQuery1(Dmod.QrAux, [
        'CREATE DATABASE ' + TMeuDB,
        '']);
    end;
    Dmod.MyDB.DatabaseName := TMeuDB;
  end else
  begin
    CriaArquivoSym(EdPathMyDB.Text, TMeuDB);
  end;
  //
  FmPrincipal.FReiniciarAppDB := True;
  SalvaLista;
  //
  for i := 0 to FMeusDBs.MaxDBs -1 do
  begin
    if FMeusDBs.DriverExist[i] = '1' then
    begin
      if FMeusDBs.FolderExist[i] = '0' then
      begin
        CriaArquivoSym(FMeusDBs.BDFolder[i], FMeusDBs.BDName[i]);
      end;
    end;
  end;
  Close;
end;

procedure TFmBancoDados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBancoDados.CriaArquivoSym(FolderDB, NameDB: String);
var
  Text: TextFile;
  FolderMySQL: String;
  Conectou: Boolean;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW VARIABLES');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, DMod.MyDB);
  //
  while not Dmod.QrAux.Eof do
  begin
    if (AnsiCompareText(Dmod.QrAux.Fields[0].AsString, 'datadir') = 0) then
      FolderMySQL := Dmod.QrAux.Fields[1].AsString;
    //
    Dmod.QrAux.Next;
  end;
  if FolderMySQL = '' then
    Geral.MB_Erro('Erro. O diret�rio' + ' de instala��o do MySQL n�o foi localizado!')
  else
  begin
    if FolderDB[Length(FolderDB)] <> '\' then
      FolderDB := FolderDB + '\';
    //
    if not DirectoryExists(FolderDB) then
    begin
      if not CreateDir(FolderDB) then
      begin
        raise Exception.Create('Imposs�vel criar o diret�rio "' + FolderDB + '"');
        Halt(0);
      end;
    end;
    if not DirectoryExists(FolderDB+NameDB) then
    begin
      if not CreateDir(FolderDB + NameDB) then
      begin
        raise Exception.Create('Imposs�vel criar o diret�rio "' + FolderDB + NameDB + '"');
        Halt(0);
      end;
    end;
    //
    if Uppercase(FolderDB) <> Uppercase(FolderMySQL) then
    begin
      AssignFile(Text, FolderMySQL+NameDB + '.sym');
      ReWrite(Text);
      WriteLn(Text, FolderDB + NameDB + '\');
      CloseFile(Text);
    end;
    //
    FMaxTempo             := 30;
    FContaTempo           := 0;
    Progress.Position     := 0;
    Progress.Max          := FMaxTempo;
    PainelAguarde.Visible := True;
    Timer1.Enabled        := True;
    Conectou              := False;
    //
    while FContaTempo < FMaxTempo do
    begin
      Dmod.ConectaMeuDB;
      if Dmod.MyDB.DataBaseName <> CO_VAZIO then
      begin
        Conectou := True;
        Break;
      end;
    end;
    if not Conectou then
    begin
      if Geral.MB_Pergunta('N�o foi poss�vel a conex�o direta ao ' +
        'banco de dados recem criado "' + TMeuDB + '".' + sLineBreak +
         'Ser� necess�rio reiniciar o computador!' + sLineBreak +
         'Deseja reinici�-lo agora?') = ID_YES
      then
        MLAGeral.ReiniciaMaquina;
    end;
  end;
  //
end;

procedure TFmBancoDados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBancoDados.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FMeusDBs             := TMeusDBs.Create;
  FMeusDBs.RegistryKey := HKEY_LOCAL_MACHINE;
  FMeusDBs.StringGrid  := Grade;
  FMeusDBs.KeyPath     := SExtraBDsCaminho;
  FMeusDBs.Reload;
  //
  FCaminhoDB      := Geral.ReadAppKey('PathMyDB', Application.Title, ktString,
                       '', HKEY_LOCAL_MACHINE);
  EdPathMyDB.Text := FCaminhoDB;
  //
  if FCaminhoDB <> '' then
  begin
    Label11.Enabled    := False;
    EdPathMyDB.Enabled := False;
  end;
end;

procedure TFmBancoDados.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBancoDados.GradeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Texto, PastaSel: String;
  i: Integer;
begin
  if (key=VK_F4) then
  begin
    PastaSel := MyObjects.DefineDiretorio(Self, nil);
    //
    if PastaSel <> '' then
    begin
      Grade.Cells[Grade.Col, Grade.Row] := PastaSel;
    end;
  end else if (key=VK_DOWN) then
  begin
    if Grade.RowCount - 1 = Grade.Row then
    begin
      if Grade.RowCount > CRD_MAX_EMPRESAS then
      begin
        Application.MessageBox('M�ximo de empresas j� cadastradas!', 'Erro',
          MB_OK+MB_ICONERROR);
      end else begin
        Texto := '';
        for i := 1 to Grade.ColCount -1 do Texto := Texto + Grade.Cells[i, Grade.Row];
        if Texto <> '' then
        begin
          Grade.RowCount := Grade.RowCount + 1;
          Grade.Cells[0, Grade.RowCount -1] := IntToStr(Grade.RowCount -1);
        end;
      end;
    end;
  //////////////////////////////////////////////////////////////////////////////
  end else if (key=VK_UP) then
  begin
    if Grade.RowCount - 1 = Grade.Row then
    begin
      Texto := '';
      for i := 1 to Grade.ColCount -1 do Texto := Texto + Grade.Cells[i, Grade.Row];
      if Texto = '' then
      begin
        Sleep(10);
        if Grade.RowCount > 2 then Grade.RowCount := Grade.RowCount - 1;
      end;
    end;
  end;
end;

procedure TFmBancoDados.LimpaEmpresas;
var
  Reg: TRegistry;
  Val:TStringList;
  i: Integer;
begin
  Reg := TRegistry.Create;
  try
    Val := TStringList.Create;
    try
      Reg.RootKey := HKEY_LOCAL_MACHINE;
      //
      if Reg.OpenKey(SExtraBDsCaminho, False) then
      begin
        Reg.GetValueNames(Val);
        //
        for i:= 0 to Val.Count-1 do
          Reg.DeleteValue(Val.Strings[i]);
        //
        Reg.DeleteKey(SExtraBDsCaminho);
      end;
    finally
      Val.Free;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TFmBancoDados.SalvaLista;
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    LimpaEmpresas;
    for i := 1 to Grade.RowCount -1 do
    begin
      Geral.WriteAppKey(Grade.Cells[1,i], Application.Title+'\BDsExtras',
        Grade.Cells[2,i], ktString, HKEY_LOCAL_MACHINE);
      Geral.WriteAppKey(Grade.Cells[1,i], Application.Title+'\Empresas',
        Grade.Cells[3,i], ktString, HKEY_LOCAL_MACHINE);
    end;
    FMeusDBs.Reload;
    Grade.SetFocus;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Grade.SetFocus;
    raise;
  end;
end;

procedure TFmBancoDados.Timer1Timer(Sender: TObject);
begin
  FContaTempo       := FContaTempo + 1;
  Progress.Position := Progress.Position + 1;
  Progress.Refresh;
  Progress.Update;
end;

end.
