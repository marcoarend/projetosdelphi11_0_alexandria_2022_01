unit VerifiDBy;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts,
  UnMLAGeral, mySQLDbTables, dmkGeral, CheckLst, dmkImage, UnDmkEnums,
  Vcl.Menus, UnGrl_Vars;

Type
  TFmVerifiDBy = class(TForm)
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    CkRegObrigat: TCheckBox;
    Panel4: TPanel;
    Panel9: TPanel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoB2: TLabel;
    LaAvisoG1: TLabel;
    LaAvisoG2: TLabel;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    LaAvisoP1: TLabel;
    LaAvisoP2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Panel16: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel17: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSair: TBitBtn;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel15: TPanel;
    BtExclTab: TButton;
    ClTabsNoNeed: TCheckListBox;
    TabSheet3: TTabSheet;
    ClFldsNoNeed: TCheckListBox;
    Panel10: TPanel;
    BtExclFld: TButton;
    TabSheet4: TTabSheet;
    ClIdxsNoNeed: TCheckListBox;
    Panel1: TPanel;
    BtClIdxsNoNeed: TButton;
    PMClIdxsNoNeed: TPopupMenu;
    ExcluirIndices1: TMenuItem;
    MenuItem2: TMenuItem;
    MarcarTodos3: TMenuItem;
    DesmarcarTodos3: TMenuItem;
    procedure BtSairClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure ClTabsNoNeedClick(Sender: TObject);
    procedure BtExclTabClick(Sender: TObject);
    procedure ClFldsNoNeedClick(Sender: TObject);
    procedure BtExclFldClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtClIdxsNoNeedClick(Sender: TObject);
    procedure PMClIdxsNoNeedPopup(Sender: TObject);
    procedure ExcluirIndices1Click(Sender: TObject);
    procedure MarcarTodos3Click(Sender: TObject);
    procedure DesmarcarTodos3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMySQLDB: TmySQLDataBase;
  end;

var
  FmVerifiDBy: TFmVerifiDBy;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal;

{$R *.DFM}

procedure TFmVerifiDBy.BtExclFldClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropCamposTabelas(ClFldsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDBy.BtExclTabClick(Sender: TObject);
var
  //P, N,
  I: Integer;
  Tabela: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MensagemBox('Deseja realmente excluir as tabelas selecionadas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    try
      //N := 0;
      for I := ClTabsNoNeed.Items.Count - 1 downto 0 do
      begin
        if ClTabsNoNeed.Checked[I] then
        begin
          Tabela := ClTabsNoNeed.Items[I];
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DROP TABLE ' + Tabela);
          Dmod.QrUpd.ExecSQL;
          //
          ClTabsNoNeed.Items.Delete(I);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVerifiDBy.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDBy.ClFldsNoNeedClick(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClFldsNoNeed.Count - 1 do
  begin
    if ClFldsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  BtExclFld.Enabled := N > 0;
end;

procedure TFmVerifiDBy.ClTabsNoNeedClick(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClTabsNoNeed.Count - 1 do
  begin
    if ClTabsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  BtExclTab.Enabled := N > 0;
end;

procedure TFmVerifiDBy.DesmarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbUnchecked, true, false);
end;

procedure TFmVerifiDBy.ExcluirIndices1Click(Sender: TObject);
const
  Aviso = '';
var
  Tabela, IdxNome: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropIndicesTabelas(ClIdxsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDBy.BtClIdxsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClIdxsNoNeed, BtClIdxsNoNeed);
end;

procedure TFmVerifiDBy.BtConfirmaClick(Sender: TObject);
var
  Versao, Resp: Integer;
  Verifica: Boolean;
begin
  Verifica := False;
  Versao   := Geral.ReadAppKeyLM('Versao', Application.Title, ktInteger, CO_VERSAO);
  //
  if Versao <= CO_VERSAO then
    Verifica := True
  else
  begin
    Resp := Application.MessageBox('A vers�o do execut�vel � inferior a do '+
      'banco de dados atual. N�o � recomendado executar a verifica��o com '+
      'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
      'Confirma assim mesmo a verifica��o?', 'Vers�o Antiga em Execu��o',
      MB_YESNO+MB_ICONWARNING);
    if Resp = ID_YES then
      Verifica := True;
  end;
  if Verifica then
  begin
    DBCheck.EfetuaVerificacoes(Dmod.MyDB, Dmod.MyLocDatabase, Memo1,
      CkEstrutura.Checked, False, False, CkPergunta.Checked,
      CkRegObrigat.Checked, LaAvisoP1, LaAvisoP2, LaAvisoR1, LaAvisoR2,
      LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2, LaTempoI, LaTempoF, LaTempoT,
      ClTabsNoNeed, ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed);
  end;
end;

procedure TFmVerifiDBy.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDBy.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmVerifiDBy.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDBy.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  DBCheck.EfetuaVerificacoes(FMySQLDB, Dmod.MyLocDatabase, Memo1,
    CkEstrutura.Checked, False, False, CkPergunta.Checked, CkRegObrigat.Checked,
    LaAvisoP1, LaAvisoP2, LaAvisoR1, LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1,
    LaAvisoG2, LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, ProgressBar1,
    ClFldsNoNeed, ClIdxsNoNeed);
  //
  BtSair.Enabled := True;
  CkPergunta.Checked  := False;
end;

procedure TFmVerifiDBy.FormShow(Sender: TObject);
begin
  FmPrincipal.Hide;
end;

procedure TFmVerifiDBy.MarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBy.PMClIdxsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClIdxsNoNeed.Count - 1 do
  begin
    if ClIdxsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  ExcluirIndices1.Enabled := N > 0;
end;

procedure TFmVerifiDBy.FormHide(Sender: TObject);
begin
  FmPrincipal.Show;
end;

end.
