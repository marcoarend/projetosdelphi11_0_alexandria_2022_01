unit VerifiDBTerceiros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts,
  mySQLDbTables, dmkGeral, CheckLst, dmkImage, UnDmkEnums, UnDmkProcFunc,
  Vcl.Menus, UnGrl_Vars;

Type
  TFmVerifiDBTerceiros = class(TForm)
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkRegObrigat: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    CkEstrutLoc: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    CkEntidade0: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    LaAviso1B: TLabel;
    LaAviso1G: TLabel;
    LaAviso1R: TLabel;
    LaAviso2B: TLabel;
    LaAviso2G: TLabel;
    LaAviso2R: TLabel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel4: TPanel;
    BtSair: TBitBtn;
    BtConfirma: TBitBtn;
    LaAvisoP1: TLabel;
    LaAvisoP2: TLabel;
    TabSheet2: TTabSheet;
    Panel15: TPanel;
    GBNFSe: TGroupBox;
    BtLC116_03: TBitBtn;
    BtTsErrosAlertas: TBitBtn;
    BitBtn1: TBitBtn;
    QrMover: TmySQLQuery;
    TabSheet3: TTabSheet;
    Button1: TButton;
    GroupBox4: TGroupBox;
    GBCTe: TGroupBox;
    BtCTeLayout: TBitBtn;
    BtNFeLoadTabs: TBitBtn;
    BtCNAE: TBitBtn;
    Memo3: TMemo;
    BtBancos: TBitBtn;
    BtIBPTax: TBitBtn;
    GBNFe: TGroupBox;
    BtLayout: TBitBtn;
    BtWebServices: TBitBtn;
    PMTabsNoNeed: TPopupMenu;
    Excluitabelas1: TMenuItem;
    Listatabelas1: TMenuItem;
    N2: TMenuItem;
    Marcartodos2: TMenuItem;
    Desmarcartodos2: TMenuItem;
    PMClFldsNoNeed: TPopupMenu;
    Excluicampos1: TMenuItem;
    N1: TMenuItem;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    GBSped: TGroupBox;
    BtSpedLayout: TBitBtn;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    ClTabsNoNeed: TCheckListBox;
    Panel10: TPanel;
    BtTabsNoNeed: TButton;
    ClFldsNoNeed: TCheckListBox;
    Panel9: TPanel;
    BtClFldsNoNeed: TButton;
    ClIdxsNoNeed: TCheckListBox;
    Panel17: TPanel;
    BtClIdxsNoNeed: TButton;
    GroupBox2: TGroupBox;
    BtRestaura: TBitBtn;
    procedure BtSairClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure LBTabsData(Control: TWinControl; Index: Integer;
      var Data: string);
    procedure FormCreate(Sender: TObject);
    procedure BtTsErrosAlertasClick(Sender: TObject);
    procedure BtLC116_03Click(Sender: TObject);
    procedure BtCNAEClick(Sender: TObject);
    procedure BtNFeLoadTabsClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtBancosClick(Sender: TObject);
    procedure BtIBPTaxClick(Sender: TObject);
    procedure BtWebServicesClick(Sender: TObject);
    procedure BtLayoutClick(Sender: TObject);
    procedure BtCTeLayoutClick(Sender: TObject);
    procedure Marcartodos2Click(Sender: TObject);
    procedure Desmarcartodos2Click(Sender: TObject);
    procedure Listatabelas1Click(Sender: TObject);
    procedure Excluitabelas1Click(Sender: TObject);
    procedure BtTabsNoNeedClick(Sender: TObject);
    procedure PMTabsNoNeedPopup(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure Excluicampos1Click(Sender: TObject);
    procedure PMClFldsNoNeedPopup(Sender: TObject);
    procedure BtClFldsNoNeedClick(Sender: TObject);
    procedure BtSpedLayoutClick(Sender: TObject);
    procedure BtRestauraClick(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaDB();
  public
    { Public declarations }
    FVerifi: Boolean;
    FDBExtra: TmySQLDataBase;
  end;

var
  FmVerifiDBTerceiros: TFmVerifiDBTerceiros;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal, UMySQLModule,
{$IfNDef sNFSe}
  IBPTaxcad,
{$Else}
  {$IfNDef SemNFe_0000}
    IBPTaxcad,
  {$EndIf}
{$EndIf}
{$IfNDef sNFSe} NFSe_PF_0000, {$EndIf}
{$IfNDef SemNFe_0000} ModPediVda, {$EndIf}
{$IfDef ComCTe_0000} UnCTe_PF, {$EndIf}
{$IfDef ComSPED_0000} UnEfdIcmsIpi_PF, {$EndIf}
ModuleGeral, dmkDAC_PF,
{$IfNDef NoEntiAux}
IBGE_LoadTabs, CNAE21Cad,
{$EndIf}
UMySQLDB;

{$R *.DFM}

procedure TFmVerifiDBTerceiros.BtIBPTaxClick(Sender: TObject);
begin
{$IfNDef sNFSe}
  if DBCheck.CriaFm(TFmIBPTaxCad, FmIBPTaxCad, afmoLiberado) then
  begin
    FmIBPTaxCad.ShowModal;
    FmIBPTaxCad.Destroy;
  end;
{$EndIf}
{$IfNDef SemNFe_0000}
  if DBCheck.CriaFm(TFmIBPTaxCad, FmIBPTaxCad, afmoLiberado) then
  begin
    FmIBPTaxCad.ShowModal;
    FmIBPTaxCad.Destroy;
  end;
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtLayoutClick(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  DmPediVda.BaixaLayoutNFe(Memo3);
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtLC116_03Click(Sender: TObject);
begin
{$IfNDef sNFSe}
  UnNFSe_PF_0000.MostraFormListServ(0);
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtNFeLoadTabsClick(Sender: TObject);
begin
{$IfNDef NoEntiAux}
  //Fazer assim porque aparece a mensagem dizendo
  //que a janela n�o est� cadastrada para quem n�o tem NFe
  Application.CreateForm(TFmIBGE_LoadTabs, FmIBGE_LoadTabs);
  FmIBGE_LoadTabs.ShowModal;
  FmIBGE_LoadTabs.Destroy;
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtRestauraClick(Sender: TObject);
const
  Arquivo = '';
begin
  DModG.MostraFormRestaura2(Dmod.MyDB, Arquivo);
end;

procedure TFmVerifiDBTerceiros.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDBTerceiros.BtSpedLayoutClick(Sender: TObject);
begin
{$IfDef ComSPED_0000}
  EfdIcmsIpi_PF.BaixaLayoutSPED(Memo3);
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtTabsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTabsNoNeed, BtTabsNoNeed);
end;

procedure TFmVerifiDBTerceiros.BtTsErrosAlertasClick(Sender: TObject);
begin
{$IfNDef sNFSe}
  UnNFSe_PF_0000.MostraFormTsErrosAlertasCab();
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtWebServicesClick(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  DmPediVda.BaixaWebServicesNFe(Memo3);
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.Button1Click(Sender: TObject);
const
  IniDir = 'C:\Dermatek\Nfse\';
  Arquivo = '';
  Titulo = 'Informe o aqruivo XML';
  Filtro = '*.xml|*.*';
var
 Res: String;
 Texto: WideString;
begin
  if MyObjects.FileOpenDialog(self, inidir, arquivo, Titulo, filtro, [], Res) then
  begin
    Texto := dmkPF.LoadFileToText(Res);
    Texto := Geral.Substitui(Texto, '><', '>' + sLineBreak + '<');
    Geral.MB_Info(Texto);
  end;
end;

procedure TFmVerifiDBTerceiros.Desmarcartodos1Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbUnchecked, true, false);;
end;

procedure TFmVerifiDBTerceiros.Desmarcartodos2Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBTerceiros.Excluicampos1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropCamposTabelas(ClFldsNoNeed, DmodG.AllID_DB, Memo1);
end;

procedure TFmVerifiDBTerceiros.Excluitabelas1Click(Sender: TObject);
var
  I: Integer;
  Tabela: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MB_Pergunta(
  'Deseja realmente excluir as tabelas selecionadas?') = ID_YES then
  begin
    try
      //N := 0;
      for I := ClTabsNoNeed.Items.Count - 1 downto 0 do
      begin
        if ClTabsNoNeed.Checked[I] then
        begin
          Tabela := ClTabsNoNeed.Items[I];
          DModG.QrAllUpd.SQL.Clear;
          DModG.QrAllUpd.SQL.Add('DROP TABLE ' + Tabela);
          DModG.QrAllUpd.ExecSQL;
          //
          ClTabsNoNeed.Items.Delete(I);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVerifiDBTerceiros.BitBtn1Click(Sender: TObject);
  procedure MoveTabela(Tabela: String);
  begin
    if USQLDB.TabelaExiste(Tabela, (*Dmod.QrUpd,*) Dmod.MyDB) then
    begin
      UMyMod.ExecutaMySQLQuery1(DModG.QrAllUpd, [
      'DROP TABLE IF EXISTS ' + Tabela + '; ',
      'CREATE TABLE ' + Tabela,
      'SELECT * FROM ' + TMeuDB + '.' + Tabela + '; ',
      '']);
      if USQLDB.TabelaExiste(Tabela, (*DModG.QrAllUpd,*) DModG.AllID_DB) then
        Geral.MB_Aviso('Tabela "' + Tabela + '" movida com sucesso');
    end;
  end;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  MoveTabela(Lowercase('DisSemDTB'));
  MoveTabela(Lowercase('MunSemDTB'));
  MoveTabela(Lowercase('DTB_Paises'));
  MoveTabela(Lowercase('DTB_UFs'));
  MoveTabela(Lowercase('DTB_Meso'));
  MoveTabela(Lowercase('DTB_Micro'));
  MoveTabela(Lowercase('DTB_Munici'));
  MoveTabela(Lowercase('DTB_Distri'));
  MoveTabela(Lowercase('DTB_SubDis'));
  MoveTabela(Lowercase('BACEN_Pais'));
end;

procedure TFmVerifiDBTerceiros.BtBancosClick(Sender: TObject);
begin
{$IFDEF UsaWSuport}
  DModG.ImportaBancos(Memo3);
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtClFldsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClFldsNoNeed, BtClFldsNoNeed);
end;

procedure TFmVerifiDBTerceiros.BtCNAEClick(Sender: TObject);
begin
{$IfNDef NoEntiAux}
  if DBCheck.CriaFm(TFmCNAE21Cad, FmCNAE21Cad, afmoLiberado) then
  begin
    FmCNAE21Cad.ShowModal;
    FmCNAE21Cad.Destroy;
  end;
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.BtConfirmaClick(Sender: TObject);
begin
  VerificaDB();
end;

procedure TFmVerifiDBTerceiros.VerificaDB();
var
  Versao, Resp: Integer;
  Verifica: Boolean;
begin
  PageControl1.ActivePageIndex := 0;
  //
  Verifica := False;
  Versao   := USQLDB.ObtemVersaoAppDB(Dmod.MyDB);
  //
  if Versao <= CO_VERSAO then
    Verifica := True
  else
  begin
    Resp := Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
      'banco de dados atual. N�o � recomendado executar a verifica��o com '+
      'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
      'Confirma assim mesmo a verifica��o?');
    if Resp = ID_YES then
      Verifica := True;
  end;
  if Verifica then
  begin
    // Para ter certeza que existe!
    DModG.AllID_DB_Cria();
    //
    DBCheck.EfetuaVerificacoes(DmodG.AllID_DB, nil, Memo1, CkEstrutura.Checked,
      CkEstrutLoc.Checked, True, CkPergunta.Checked, CkRegObrigat.Checked,
      LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B,
      LaAviso1G, LaAviso2G, LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed,
      ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed);
    //
    if Memo1.Text = '' then
      PageControl1.ActivePageIndex := 1;
   end;
end;

procedure TFmVerifiDBTerceiros.BtCTeLayoutClick(Sender: TObject);
begin
{$IfDef ComCTe_0000}
  CTe_PF.BaixaLayoutCTe(Memo3);
{$EndIf}
end;

procedure TFmVerifiDBTerceiros.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDBTerceiros.FormCreate(Sender: TObject);
var
  BD: String;
  Resp: Word;
begin
  ImgTipo.SQLType := stNil;
  PageControl1.ActivePageIndex := 0;
  //
  TabSheet3.TabVisible := False;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW DATABASES');
  UnDmkDAC_PF.AbreQueryApenas(Dmod.QrAux);
  BD := CO_VAZIO;
  while not Dmod.QrAux.Eof do
  begin
    if Uppercase(Dmod.QrAux.FieldByName('Database').AsString)=Uppercase(VAR_AllID_DB_NOME) then
      BD := VAR_AllID_DB_NOME;
    Dmod.QrAux.Next;
  end;
  DModG.AllID_DB.Close;
  DModG.AllID_DB.DataBaseName := BD;
  if DModG.AllID_DB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados ' + VAR_AllID_DB_NOME +
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('CREATE DATABASE '+VAR_AllID_DB_NOME);
      Dmod.QrAux.ExecSQL;
      DModG.AllID_DB.DatabaseName := VAR_AllID_DB_NOME;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Info('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  {$IfNDef SemNFe_0000}
    GBNFe.Visible := True;
  {$Else}
    GBNFe.Visible := False;
  {$EndIf}
  {$IfNDef sNFSe}
    GBNFSe.Visible := True;
  {$Else}
    GBNFSe.Visible := False;
  {$EndIf}
  {$IfDef ComCTe_0000}
    GBCTe.Visible := True;
  {$Else}
    GBCTe.Visible := False;
  {$EndIf}
  {$IfDef ComSPED_0000}
    GBSped.Visible := True;
  {$Else}
    GBSped.Visible := False;
  {$EndIf}
  //
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmVerifiDBTerceiros.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1R, LaAviso2R], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDBTerceiros.Timer1Timer(Sender: TObject);
var
  DBLoc: TmySQLDatabase;
begin
  {$IfNDef SemDBLocal}
  DBLoc := Dmod.MyLocDatabase;
  {$Else}
  DBLoc := nil;
  {$EndIf}
  //
  Timer1.Enabled := False;
  //
  VerificaDB();
  //
  BtSair.Enabled      := True;
  CkPergunta.Checked  := False;
  CkEstrutLoc.Checked := False;
end;

procedure TFmVerifiDBTerceiros.FormShow(Sender: TObject);
begin
  //FmPrincipal.Hide;
end;

procedure TFmVerifiDBTerceiros.LBTabsData(Control: TWinControl; Index: Integer;
  var Data: string);
begin
  ShowMessage('Data');
end;

procedure TFmVerifiDBTerceiros.Listatabelas1Click(Sender: TObject);
begin
  Geral.MB_Info(ClTabsNoNeed.Items.Text);
end;

procedure TFmVerifiDBTerceiros.Marcartodos1Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBTerceiros.Marcartodos2Click(Sender: TObject);
begin
  ClTabsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBTerceiros.PMClFldsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClFldsNoNeed.Count - 1 do
  begin
    if ClFldsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  Excluicampos1.Enabled := N > 0;
end;

procedure TFmVerifiDBTerceiros.PMTabsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClTabsNoNeed.Count - 1 do
  begin
    if ClTabsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  Excluitabelas1.Enabled := N > 0;
  Listatabelas1.Enabled  := N > 0;
end;

procedure TFmVerifiDBTerceiros.FormHide(Sender: TObject);
begin
  //FmPrincipal.Show;
end;

end.



