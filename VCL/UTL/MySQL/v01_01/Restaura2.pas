unit Restaura2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, CheckLst, UnInternalConsts, ComCtrls,
  Db, mySQLDbTables, MySQLDump, MySQLBatch, mySQLDirectQuery, dmkGeral, dmkEdit,
  UnMyObjects, dmkImage, UnDmkEnums;

type
  TFmRestaura2 = class(TForm)
    Panel2: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    LaStatus: TStaticText;
    Panel8: TPanel;
    EdBackFile: TdmkEdit;
    BtAbrir: TBitBtn;
    Label1: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    Splitter1: TSplitter;
    BeRestore_: TMySQLBatchExecute;
    Memo3: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    Progress1: TProgressBar;
    BtDireto: TBitBtn;
    CkMostra: TCheckBox;
    BtParar: TBitBtn;
    BtSaida: TBitBtn;
    Splitter2: TSplitter;
    RGDBs: TRadioGroup;
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
    procedure MySQLDumpFinish(Sender: TObject);
    procedure BeRestore_AfterExecute(Sender: TObject);
    procedure BeRestore_AfterStatement(Sender: TObject; SQLText: String;
      const StatementNo, RowsAffected: Integer; const Success: Boolean);
    procedure BeRestore_Process(Sender: TObject; SQLText: String;
      const StatementNo: Integer);
    procedure BtDiretoClick(Sender: TObject);
    procedure EdBackFileChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure RGDBsClick(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    FExecute: String;
    FContaD, FContaE, FContaS, FContaP, FContaL: Integer;
    FInicio, FCorrido, FFaltando: TDateTime;
    FNumDecl: Integer;
    //function LeArquivo(Arquivo: String): String;
    //function NumeroDeDeclaracoes: Integer;
    //function NumeroDeLetras: Integer;
    //procedure ProcessoEmDOS(LinhaComando : PChar);
    function ExecutaDirect(Arquivo: String): String;
    procedure CarregarPrimeirasLinhas(Arquivo: String);

  public
    { Public declarations }
    FZipPath: String;
    FDB: TmySQLDataBase;
    FDBs: array of TmySQLDataBase;
  end;

var
  FmRestaura2: TFmRestaura2;

implementation

{$R *.DFM}

var
  (*WMLA_TbCount, WMLA_ZPCount,*) WMLA_DuCount: Integer;

procedure TFmRestaura2.FormActivate(Sender: TObject);
begin
  MyObjects.CorInicomponente;
end;

procedure TFmRestaura2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmRestaura2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRestaura2.CarregarPrimeirasLinhas(Arquivo: String);
const
  MaxDecl = 100;
var
  F: TextFile;
  S: String;
  TotDecl, Declara: Integer;
begin
  Screen.Cursor := crHourGlass;
  //FNumDecl := 0;
  if FileExists(Arquivo) then
  begin
    AssignFile(F, Arquivo);
    Reset(F);
    TotDecl := 0;
    LaStatus.Caption := 'Calculando quantidade de TotDecl��es...';
    LaStatus.Refresh;
    LaStatus.Update;
    while not Eof(F) do
    begin
      TotDecl := TotDecl + 1;
      Readln(F, S);
      if TotDecl <= MaxDecl then
        Memo2.Lines.Add(S);
    end;
    //
    Reset(F);
    CloseFile(F);
  end;
  if TotDecl > MaxDecl then
    Geral.MB_Info('Foram mostradas apenas as ' + Geral.FF0(MaxDecl) + ' declara��es de ' + Geral.FF0(TotDecl) + '!');
  Progress1.Position := 0;
  LaStatus.Caption := 'Total de declara��es: ' + Geral.FF0(TotDecl);
  Memo2.SelStart := 0; {first character could be any}
  Memo2.SelLength := 1; {move display cursor to this position}
  Screen.Cursor := crDefault;
end;

procedure TFmRestaura2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRestaura2.BtPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmRestaura2.MySQLDumpFinish(Sender: TObject);
begin
  WMLA_DuCount := WMLA_DuCount +1;
  LaStatus.Caption := 'Terminado!';
  LaStatus.Refresh;
  LaStatus.Update;
  Progress1.Position := 0;
  LaStatus.Caption := 'Dump terminado!';
end;

procedure TFmRestaura2.RGDBsClick(Sender: TObject);
begin
  if RGDBs.ItemIndex > -1 then
    FDB := FDBs[RGDBs.ItemIndex];
end;

{ Desabilitado porque n�o usa
function TFmRestaura2.LeArquivo(Arquivo: String): String;
  procedure AddLinhaSQL(S: String);
  var
    X: String;
  begin
    if Length(S) > 1 then X := S[1]+S[2] else x := '';
    if x <> '--' then
    if x <> '/*' then
    (*****BeRestore.SQL.Add(S);*)
  end;
var
  F: TextFile;
  S: String;
  Declara: Integer;
begin
  Screen.Cursor := crHourGlass;
  FNumDecl := 0;
  (*****BeRestore.SQL.Clear;*)
  if FileExists(Arquivo) then
  begin
    AssignFile(F, Arquivo);
    Reset(F);
    Declara := 0;
    while not Eof(F) do
    begin
      Declara := Declara + 1;
      LaStatus.Caption := 'Carregando linha n� '+IntToStr(Declara);
      LaStatus.Refresh;
      LaStatus.Update;
      Readln(F, S);
      if Declara <> 1 then AddLinhaSQL(S);
      Application.ProcessMessages;
    end;
    CloseFile(F);
  end;
  FNumDecl := NumeroDeDeclaracoes;
  if FNumDecl > 0 then
  begin
    LaStatus.Caption := 'SQL Carregado';
    Memo1.Lines.Add('Arquivo SQL com '+IntToStr(FNumDecl)+' declara��es.');
  end else begin
    LaStatus.Caption := 'N�o foi carregado nenhuma SQL!';
  end;
  FContaD := NumeroDeLetras;
  FNumDecl := NumeroDeDeclaracoes;
  Progress1.Position := 0;
  Progress1.Max := FContaD;
  Memo1.Lines.Add('Arquivo SQL com '+IntToStr(FNumDecl)+' declara��es.');
  if FNumDecl > 0 then
    LaStatus.Caption := 'SQL Carregado'
  else
    LaStatus.Caption := 'N�o foi carregado nenhuma SQL!';
  Screen.Cursor := crDefault;
end;

function TFmRestaura2.NumeroDeDeclaracoes: Integer;
var
  i, Conta: Integer;
begin
  Conta := 0;
  for i := 0 to BeRestore_.SQL.Count - 1 do
  begin
    if Pos (';', BeRestore_.SQL[i]) > 1 then Conta := Conta +1;
  end;
  Result := Conta;
end;

function TFmRestaura2.NumeroDeLetras: Integer;
var
  i, Conta: Integer;
begin
  Conta := 0;
  for i := 0 to BeRestore_.SQL.Count - 1 do
  begin
    Conta := Conta + Length(BeRestore_.SQL[i]);
  end;
  Result := Conta;
end;
}

procedure TFmRestaura2.BeRestore_AfterExecute(Sender: TObject);
begin
  FContaE := FContaE + 1;
  Update;
  //
  Memo1.Lines.Add('Declara��es executadas = '+IntToStr(FContaP));
end;

procedure TFmRestaura2.BeRestore_AfterStatement(Sender: TObject;
  SQLText: String; const StatementNo, RowsAffected: Integer;
  const Success: Boolean);
begin
  FContaL := FContaL + Length(SQLText);
  FContaS := FContaS + 1;
  if CkMostra.Checked then Memo2.Text := SQLText;
  Application.ProcessMessages;
  FCorrido  := Now() - FInicio;
  FFaltando := FCorrido * FContaD / FContaL;
  Progress1.Position := FContaL;
  LaStatus.Caption := 'Restaurado '+
    FormatFloat('0.00', Progress1.Position/Progress1.Max*100)+' % ('+
  IntToStr(StatementNo)+' registros) Tempo corrido: '+
  FormatDateTime(VAR_FORMATTIME, FCorrido)+'  Tempo restante: '+
  FormatDateTime(VAR_FORMATTIME, FFaltando-FCorrido)+' (aproximado)  Tempo total: '+
  FormatDateTime(VAR_FORMATTIME, FFaltando)+' (aproximado)';
  LaStatus.Update;
end;

procedure TFmRestaura2.BeRestore_Process(Sender: TObject; SQLText: String;
  const StatementNo: Integer);
begin
  FContaP := FContaP + 1;
end;

{ Desabilitado porque n�o usa
procedure TFmRestaura2.ProcessoEmDOS(LinhaComando : PChar);
var
  Startupinfo : TStartupinfo;
  ProcessInformation : TProcessInformation;
  rc : Boolean;
begin
  Fillchar(Startupinfo, SizeOf(Startupinfo), #0);
  with Startupinfo do
    cb := SizeOf(Startupinfo);
  rc := CreateProcess(NIL, LinhaComando, NIL, NIL, FALSE, NORMAL_PRIORITY_CLASS,
                      NIL, NIL, Startupinfo, ProcessInformation);
  if rc then
    WaitForSingleObject(ProcessInformation.hProcess, INFINITE);
end;
}

procedure TFmRestaura2.EdBackFileChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := FileExists(EdBackFile.Text);
  BtDireto.Enabled := Habilita;
end;

procedure TFmRestaura2.BtAbrirClick(Sender: TObject);
  procedure AbreArquivo();
  var
    Extensao: String;
  begin
    FInicio := Now();
    Memo1.Lines.Clear;
    Memo2.Lines.Clear;
    Memo3.Lines.Clear;
    (*****BeRestore.SQL.Clear;*)
    if FileExists(EdBackFile.Text) then
    begin
      Extensao := ExtractFileExt(EdBackFile.Text);
      if AnsiCompareText(Extensao, '.sql') = 0 then
      begin
        //ExecutaDirect(EdBackFile.Text)
        //Memo2.Lines.LoadFromFile(EdBackFile.Text);
        CarregarPrimeirasLinhas(EdBackFile.Text);
        BtDireto.Visible := True;
        BtDireto.Enabled := True;
        BtParar.Visible := True;
        BtParar.Enabled := True;
      end else
        Geral.MB_Aviso('Extens�o inv�lida: "' + Extensao + '"');
    end else
      Geral.MB_Aviso('Arquivo n�o encontrado: "' + EdBackFile.Text + '"');
  end;
begin
  MyObjects.DefineArquivo1(Self, EdBackFile);
  //
  AbreArquivo();
end;

procedure TFmRestaura2.BtDiretoClick(Sender: TObject);
var
  Extensao: String;
begin
  FInicio := Now();
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  Memo3.Lines.Clear;
  (*****BeRestore.SQL.Clear;*)
  if FileExists(EdBackFile.Text) then
  begin
    Extensao := ExtractFileExt(EdBackFile.Text);
    if AnsiCompareText(Extensao, '.sql') = 0 then ExecutaDirect(EdBackFile.Text)
    else ShowMessage('Extens�o inv�lida: "'+Extensao+'"');
  end else ShowMessage('Arquivo n�o encontrado: "'+EdBackFile.Text+'"');
end;

function TFmRestaura2.ExecutaDirect(Arquivo: String): String;
  procedure ExecutaSQL(S: String);
  var
    X: String;
  begin
    if Length(S) > 1 then X := S[1]+S[2] else x := '';
    if x <> '--' then
    if x <> '/*' then
    begin
      if S <> '' then
      begin
        FExecute := FExecute + sLineBreak+ S;
        if FExecute[Length(FExecute)] = ';' then
        begin
          if FExecute <> '' then
          begin
            FDB.Execute(FExecute);
            FExecute := '';
          end;
        end;
      end;
    end else begin
      if FExecute <> '' then
      begin
        FDB.Execute(FExecute);
        FExecute := '';
      end;
    end;
  end;
var
  F: TextFile;
  S: String;
  TotDecl, Declara: Integer;
begin
  Screen.Cursor := crHourGlass;
  FExecute := '';
  FNumDecl := 0;
  if FileExists(Arquivo) then
  begin
    AssignFile(F, Arquivo);
    Reset(F);
    TotDecl := 0;
    LaStatus.Caption := 'Calculando quantidade de TotDecl��es...';
    LaStatus.Refresh;
    LaStatus.Update;
    while not Eof(F) do
    begin
      TotDecl := TotDecl + 1;
      Readln(F, S);
    end;
    //
    Reset(F);
    Declara := 0;
    FFaltando := 0;
    Progress1.Position := 0;
    Progress1.Max := TotDecl;
    while not Eof(F) do
    begin
      Declara := Declara + 1;
      FCorrido  := Now() - FInicio;
      if TotDecl > 0 then
        FFaltando := FCorrido * TotDecl / Declara;
      Progress1.Position := Progress1.Position + 1;
      Progress1.Refresh;
      LaStatus.Caption := 'Executando declara��o n� '+IntToStr(Declara) +
        ' de ' + IntToStr(TotDecl) + '  -  ' +
        FormatFloat('0.00', Declara / TotDecl * 100) + '%  -  Tempo corrido: '+
        FormatDateTime(VAR_FORMATTIME, FCorrido)+'  Tempo restante: '+
        FormatDateTime(VAR_FORMATTIME, FFaltando-FCorrido)+' (aprox.)  Tempo total: '+
        FormatDateTime(VAR_FORMATTIME, FFaltando)+' (aprox.)';
      LaStatus.Refresh;
      LaStatus.Update;
      Readln(F, S);
      if Declara <> 1 then ExecutaSQL(S);
      Application.ProcessMessages;
    end;
    LaStatus.Caption := 'Executadas '+IntToStr(Declara) + ' declara��es de ' +
      IntToStr(TotDecl) + '  =  ' + FormatFloat('0.0000', Declara / TotDecl * 100) +
      '%  -  Tempo Total: ' + FormatDateTime(VAR_FORMATTIME, FCorrido);
    CloseFile(F);
  end;
  Geral.MB_Info('Backup restaurado!');
  Progress1.Position := 0;
  Screen.Cursor := crDefault;
end;

end.
