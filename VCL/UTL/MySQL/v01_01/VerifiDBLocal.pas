unit VerifiDBLocal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts,
  mySQLDbTables, dmkGeral, CheckLst, dmkImage, UnDmkEnums, UnGrl_Vars;

Type
  TFmVerifiDBLocal = class(TForm)
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    CkEstrutLoc: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    CkRegObrigat: TCheckBox;
    Panel4: TPanel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LaAvisoP1: TLabel;
    LaAvisoP2: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoB2: TLabel;
    LaAvisoG1: TLabel;
    LaAvisoG2: TLabel;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    Panel10: TPanel;
    BtConfirma: TBitBtn;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    ClFldsNoNeed: TCheckListBox;
    Panel9: TPanel;
    BtExclFld: TButton;
    TabSheet4: TTabSheet;
    ClTabsNoNeed: TCheckListBox;
    ClIdxsNoNeed: TCheckListBox;
    Panel16: TPanel;
    BtClIdxsNoNeed: TButton;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure BtExclFldClick(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FDBExtra: TmySQLDataBase;
  end;

var
  FmVerifiDBLocal: TFmVerifiDBLocal;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal, ModuleGeral, UMySQLDB;

{$R *.DFM}

procedure TFmVerifiDBLocal.BtExclFldClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropCamposTabelas(ClFldsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDBLocal.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDBLocal.BtConfirmaClick(Sender: TObject);
var
  Versao, Resp: Integer;
  Verifica: Boolean;
begin
  Verifica := False;
  Versao   := USQLDB.ObtemVersaoAppDB(Dmod.MyDB);
  //
  if Versao <= CO_VERSAO then
    Verifica := True
  else
  begin
    Resp := Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
      'banco de dados atual. N�o � recomendado executar a verifica��o com '+
      'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
      'Confirma assim mesmo a verifica��o?');
    if Resp = ID_YES then
      Verifica := True;
  end;
  if Verifica then
  begin
    DModG.MyLocDB.UserName     := 'root';
    DModG.MyLocDB.UserPassword := CO_USERSPNOW;
    if not DModG.MyLocDB.Connected then
      DModG.MyLocDB.Connect;
    //
    DBCheck.EfetuaVerificacoes(DModG.MyLocDB, nil, Memo1,
      CkEstrutura.Checked, CkEstrutLoc.Checked, True,
      CkPergunta.Checked, CkRegObrigat.Checked, LaAvisoP1, LaAvisoP2,
      LaAvisoR1, LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1,
      LaAvisoG2, LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, ProgressBar1,
      ClFldsNoNeed, ClIdxsNoNeed);
    //
    BtSair.Enabled      := True;
    CkPergunta.Checked  := False;
    CkEstrutLoc.Checked := False;
  end;
end;

procedure TFmVerifiDBLocal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDBLocal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmVerifiDBLocal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDBLocal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  DBCheck.EfetuaVerificacoes(DModG.MyLocDB, nil, Memo1, CkEstrutura.Checked,
    False, True, False, CkRegObrigat.Checked, LaAvisoP1, LaAvisoP2,
    LaAvisoR1, LaAvisoR2, LaAvisoG1, LaAvisoB1, LaAvisoB2,
    LaAvisoG2, LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed,
    ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed);
  //
  BtSair.Enabled      := True;
  CkPergunta.Checked  := False;
  CkEstrutLoc.Checked := False;
end;

procedure TFmVerifiDBLocal.FormShow(Sender: TObject);
begin
  FmPrincipal.Hide;
end;

procedure TFmVerifiDBLocal.FormHide(Sender: TObject);
begin
  FmPrincipal.Show;
end;

end.
