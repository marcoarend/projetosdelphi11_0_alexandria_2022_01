unit VerifiDBUsuario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts,
  UnMLAGeral, mySQLDbTables, dmkGeral, dmkImage, CheckLst, UnDmkEnums, Vcl.Menus;

Type
  TFmVerifiDBUsuario = class(TForm)
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkRegObrigat: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    CkEstrutLoc: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    CkEntidade0: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    LaAviso1B: TLabel;
    LaAviso1G: TLabel;
    LaAviso1R: TLabel;
    LaAviso2B: TLabel;
    LaAviso2G: TLabel;
    LaAviso2R: TLabel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel4: TPanel;
    BtSair: TBitBtn;
    BtConfirma: TBitBtn;
    LaAvisoP1: TLabel;
    LaAvisoP2: TLabel;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel10: TPanel;
    BtExclTab: TButton;
    ClTabsNoNeed: TCheckListBox;
    TabSheet3: TTabSheet;
    ClFldsNoNeed: TCheckListBox;
    Panel9: TPanel;
    BtExclFld: TButton;
    TabSheet4: TTabSheet;
    ClIdxsNoNeed: TCheckListBox;
    Panel16: TPanel;
    BtClIdxsNoNeed: TButton;
    PMClIdxsNoNeed: TPopupMenu;
    ExcluirIndices1: TMenuItem;
    MenuItem2: TMenuItem;
    MarcarTodos3: TMenuItem;
    DesmarcarTodos3: TMenuItem;
    procedure BtSairClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure LBTabsData(Control: TWinControl; Index: Integer;
      var Data: string);
    procedure FormCreate(Sender: TObject);
    procedure BtExclTabClick(Sender: TObject);
    procedure ClTabsNoNeedClick(Sender: TObject);
    procedure PMClIdxsNoNeedPopup(Sender: TObject);
    procedure BtClIdxsNoNeedClick(Sender: TObject);
    procedure ExcluirIndices1Click(Sender: TObject);
    procedure MarcarTodos3Click(Sender: TObject);
    procedure DesmarcarTodos3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FDBExtra: TmySQLDataBase;
  end;

var
  FmVerifiDBUsuario: TFmVerifiDBUsuario;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal, UMySQLModule, 
  ModuleGeral;

{$R *.DFM}

procedure TFmVerifiDBUsuario.BtExclTabClick(Sender: TObject);
var
  I, P, N: Integer;
  Tabela: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MensagemBox('Deseja realmente excluir as tabelas selecionadas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    try
      N := 0;
      for I := ClTabsNoNeed.Items.Count - 1 downto 0 do
      begin
        if ClTabsNoNeed.Checked[I] then
        begin
          Tabela := ClTabsNoNeed.Items[I];
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DROP TABLE ' + Tabela);
          Dmod.QrUpd.ExecSQL;
          //
          ClTabsNoNeed.Items.Delete(I);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVerifiDBUsuario.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDBUsuario.ClTabsNoNeedClick(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClTabsNoNeed.Count - 1 do
  begin
    if ClTabsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  BtExclTab.Enabled := N > 0;
end;

procedure TFmVerifiDBUsuario.DesmarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbUnchecked, true, false);
end;

procedure TFmVerifiDBUsuario.ExcluirIndices1Click(Sender: TObject);
const
  Aviso = '';
var
  Tabela, IdxNome: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropIndicesTabelas(ClIdxsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDBUsuario.BtClIdxsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClIdxsNoNeed, BtClIdxsNoNeed);
end;

procedure TFmVerifiDBUsuario.BtConfirmaClick(Sender: TObject);
var
  Versao, Resp: Integer;
  Verifica: Boolean;
begin
  Verifica := False;
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  if Versao <= CO_VERSAO then
    Verifica := True
  else begin
    Resp := Geral.MB_Aviso('A vers�o do execut�vel � inferior a do ' +
              'banco de dados atual. N�o � recomendado executar a verifica��o com ' +
              'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.' +
              'Confirma assim mesmo a verifica��o?');
    //
    if Resp = ID_YES then
      Verifica := True;
  end;
  if Verifica then
  begin
    DBCheck.EfetuaVerificacoes(DmodG.MyPID_DB, nil, Memo1, CkEstrutura.Checked,
      CkEstrutLoc.Checked, True, CkPergunta.Checked, CkRegObrigat.Checked,
      LaAvisoP1, LaAvisoP2, LaAviso1R,
      LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
      LaTempoF, LaTempoT,
      ClTabsNoNeed, ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed);
  end;
end;

procedure TFmVerifiDBUsuario.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDBUsuario.FormCreate(Sender: TObject);
var
  BD: String;
  Resp: Word;
begin
  ImgTipo.SQLType := stNil;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW DATABASES');
  Dmod.QrAux.Open;
  BD := CO_VAZIO;
  while not Dmod.QrAux.Eof do
  begin
    if Uppercase(Dmod.QrAux.FieldByName('Database').AsString)=Uppercase(VAR_MyPID_DB_NOME) then
      BD := VAR_MyPID_DB_NOME;
    Dmod.QrAux.Next;
  end;
  DModG.MyPID_DB.Close;
  DModG.MyPID_DB.DataBaseName := BD;
  if DModG.MyPID_DB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados ' + VAR_MyPID_DB_NOME +
              ' n�o existe e deve ser criado. Confirma a cria��o?');
    //
    if Resp = ID_YES then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('CREATE DATABASE '+VAR_MyPID_DB_NOME);
      Dmod.QrAux.ExecSQL;
      DModG.MyPID_DB.DatabaseName := VAR_MyPID_DB_NOME;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  //
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmVerifiDBUsuario.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1R, LaAviso2R], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDBUsuario.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  DBCheck.EfetuaVerificacoes(Dmod.MyDB, Dmod.MyLocDatabase, Memo1,
  CkEstrutura.Checked, False, True, False, CkRegObrigat.Checked,
    LaAvisoP1, LaAvisoP2, LaAviso1R,
    LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
    LaTempoF, LaTempoT,
    ClTabsNoNeed, ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed);
  BtSair.Enabled := True;
  CkPergunta.Checked  := False;
  CkEstrutLoc.Checked := False;
end;

procedure TFmVerifiDBUsuario.FormShow(Sender: TObject);
begin
  FmPrincipal.Hide;
end;

procedure TFmVerifiDBUsuario.LBTabsData(Control: TWinControl; Index: Integer;
  var Data: string);
begin
  ShowMessage('Data');
end;

procedure TFmVerifiDBUsuario.MarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBUsuario.PMClIdxsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClIdxsNoNeed.Count - 1 do
  begin
    if ClIdxsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  ExcluirIndices1.Enabled := N > 0;
end;

procedure TFmVerifiDBUsuario.FormHide(Sender: TObject);
begin
  FmPrincipal.Show;
end;

end.
