object FmVerifiDBLocal: TFmVerifiDBLocal
  Left = 382
  Top = 202
  Caption = 'FER-VRFBD-003 :: Verifica'#231#227'o de Banco de Dados Local'
  ClientHeight = 586
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 688
    Height = 424
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 408
      Width = 688
      Height = 16
      Align = alBottom
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 688
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 688
        Height = 60
        Align = alClient
        Caption = ' Verifica'#231#245'es: '
        TabOrder = 0
        object CkEstrutura: TCheckBox
          Left = 172
          Top = 16
          Width = 161
          Height = 17
          Caption = 'Estrutura do banco de dados.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkPergunta: TCheckBox
          Left = 12
          Top = 16
          Width = 155
          Height = 17
          Caption = 'Pergunta antes de executar.'
          TabOrder = 1
        end
        object CkEstrutLoc: TCheckBox
          Left = 336
          Top = 16
          Width = 185
          Height = 17
          Caption = 'Estrutura do banco de dados local.'
          TabOrder = 2
        end
        object CkRegObrigat: TCheckBox
          Left = 524
          Top = 16
          Width = 157
          Height = 17
          Caption = 'Recria registros obrigat'#243'rios.'
          Enabled = False
          TabOrder = 3
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 164
      Width = 453
      Height = 244
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Avisos'
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 446
          Height = 219
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 60
      Width = 688
      Height = 104
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object LaAvisoP1: TLabel
        Left = 13
        Top = 80
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoP2: TLabel
        Left = 12
        Top = 80
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 0
        Width = 688
        Height = 73
        Align = alTop
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 685
          Height = 57
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 559
            Height = 57
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAvisoB1: TLabel
              Left = 13
              Top = 18
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoB2: TLabel
              Left = 12
              Top = 17
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoG1: TLabel
              Left = 13
              Top = 34
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoG2: TLabel
              Left = 12
              Top = 33
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoR1: TLabel
              Left = 13
              Top = 2
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoR2: TLabel
              Left = 12
              Top = 1
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
          object Panel14: TPanel
            Left = 671
            Top = 0
            Width = 14
            Height = 57
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
          end
          object Panel6: TPanel
            Left = 603
            Top = 0
            Width = 68
            Height = 57
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object LaTempoI: TLabel
              Left = 0
              Top = 0
              Width = 68
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTempoF: TLabel
              Left = 0
              Top = 14
              Width = 68
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTempoT: TLabel
              Left = 0
              Top = 28
              Width = 68
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
          end
          object Panel8: TPanel
            Left = 559
            Top = 0
            Width = 44
            Height = 57
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object Label4: TLabel
              Left = 0
              Top = 0
              Width = 44
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'In'#237'cio:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label5: TLabel
              Left = 0
              Top = 14
              Width = 44
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'T'#233'rmino:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label6: TLabel
              Left = 0
              Top = 28
              Width = 44
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'Tempo:'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 453
      Top = 164
      Width = 235
      Height = 244
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      ActivePage = TabSheet2
      Align = alRight
      TabOrder = 4
      object TabSheet2: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Tabelas desnecess'#225'rias'
        object ClTabsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 229
          Height = 219
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Campos desnecess'#225'rios'
        ImageIndex = 1
        object ClFldsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 229
          Height = 182
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object Panel9: TPanel
          Left = 0
          Top = 179
          Width = 227
          Height = 37
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 182
          ExplicitWidth = 229
          object BtExclFld: TButton
            Left = 5
            Top = 6
            Width = 172
            Height = 25
            Caption = 'Exclui campo(s)'
            Enabled = False
            TabOrder = 0
            OnClick = BtExclFldClick
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = #205'ndices desnecess'#225'rios'
        ImageIndex = 2
        object ClIdxsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 229
          Height = 182
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object Panel16: TPanel
          Left = 0
          Top = 179
          Width = 227
          Height = 37
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 182
          ExplicitWidth = 229
          object BtClIdxsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 640
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 592
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 60
        Height = 32
        Caption = '????'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 60
        Height = 32
        Caption = '????'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 60
        Height = 32
        Caption = '????'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 472
    Width = 688
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 685
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 516
    Width = 688
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 543
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 541
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 125
        Left = 10
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Atualiza'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 140
    Top = 120
  end
end
