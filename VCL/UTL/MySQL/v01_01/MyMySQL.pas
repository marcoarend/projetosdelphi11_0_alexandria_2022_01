unit MyMySQL;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls, dmkGeral,
  UnInternalConsts2, Mask, ComCtrls, (*DBTables,*) ResIntStrings, mySQLDbTables,
  (*DBIProcs,*) Registry, ZCF2, Buttons,  (*UnInternalConsts3,*) TypInfo, dbGrids,
  dmkValUsu, DmkDAC_PF, dmkLabel, dmkEdit, dmkDBLookupCombobox, dmkEditCB,
  dmkEditDateTimePicker, dmkCheckGroup, dmkMemo, dmkRadioGroup, dmkCheckBox,
  dmkDBEdit, (*dmkPopOutFntCBox,*) Variants,  dmkImage, UnMyLinguas,
  UnDmkProcFunc, UnDmkEnums;

const
  TMaxConfWinControl = 255;

type
  TUnMyMySQL = class(TObject)

  private
    { Private declarations }

  public
    // CRIA��O E MANUTEN��O DE TABELAS
    function  AdicionaCampo(LCampos: TList; Field, Tipo, Default: String;
              Nulo: Boolean = False; Key: String = '' ; Extra: String = ''):
              Boolean;
    function  CriaTabela(QrExec: TmySQLQuery; TabelaNome, TabelaBase:
              String; Memo: TMemo; Acao: TAcaoCriaTabela;
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel;
              FLCampos, FLIndices: TList): Boolean;
    function  BuscaNovoCodigo_Int(QrAux: TmySQLQuery; Tabela, Campo: String;
              CamposExtras: array of String; ValExtras: array of Variant;
              SQLType: TSQLType; Padrao: Integer; Sinal: TSinal;
              dmkEdit: TdmkEdit): Integer;
    function  BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType;
              Atual: Integer; EdControle: TdmkEdit = nil; Forca_1:
              Boolean = True): Integer;
    function  BuscaEmLivreY(Database: TmySQLDatabase;
              TabLivre, TabControle, Table, FieldControl, FieldTable : String;
              Forca_1: Boolean = True) : Integer;
    function BPGS1I32(Tabela, Campo, _WHERE, _AND: String;
             TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;


    // QUERIES EM GERAL
    function  AbreMySQLQuery1(Query: TMySQLQuery; SQL: array of String): Boolean;
    function  ExecutaMySQLQuery1(Query: TMySQLQuery; SQL: array of String): Boolean;
    function  CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean; ComplUpd: String): Boolean;
    function  ExecutaQuery(Query: TmySQLQuery): Boolean;
    function  SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    // AUXILIARES
    procedure GravaAviso(Aviso: String; Memo: TMemo);
    // Novos
    function  ObtemCamposDeTabelaIdentica(DataBase: TmySQLDatabase;
              Tabela, Prefix: String): String;// Prefix -> 'la.' = ref tabela
  end;

var
  UnMyMySQL: TUnMyMySQL;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;

implementation

uses GModule, Module;

function TUnMyMySQL.AbreMySQLQuery1(Query: TMySQLQuery;
  SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
begin
  //Result := False;
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MensagemBox('Texto da SQL indefinido no MySQLQuery!' + sLineBreak +
      'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
      Result := False;
    end
    else begin
      Query.Open;
      Result := True;
    end;
    Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    Geral.MensagemBox('Erro ao tentar abrir uma SQL no MySQLQuery!' + sLineBreak +
    'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
    raise;
  end;
end;


function TUnMyMySQL.AdicionaCampo(LCampos: TList; Field, Tipo, Default: String;
  Nulo: Boolean; Key, Extra: String): Boolean;
begin
  New(FRCampos);
  FRCampos.Field      := Field;
  FRCampos.Tipo       := Tipo;
  if Nulo then
    FRCampos.Null       := 'YES'
  else
    FRCampos.Null       := '';
  FRCampos.Key        := Key;
  FRCampos.Default    := Default;
  FRCampos.Extra      := Extra;
  //
  LCampos.Add(FRCampos);
  //
  Result := True;
end;

function TUnMyMySQL.BuscaEmLivreY(Database: TmySQLDatabase; TabLivre,
  TabControle, Table, FieldControl, FieldTable: String;
  Forca_1: Boolean): Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
  QvLivreY.SQL.Add('ORDER BY Codigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  QvLivreY.Open;
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
    QvUpdY.SQL.Add('AND Codigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsInteger := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
    QvLivreY.Params[0].AsInteger := Codigo;
    QvLivreY.Open;
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      QvLivreY.Open;
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
      QvLivreY.Open;
      if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
      //
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsInteger := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    QvLivreY.Open;
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
    QvLivreY.Open;
    if (QvLivreY.FieldByName('Codigo').AsString = '') and Forca_1 then
    begin
      if Codigo = 0 then
        Codigo := 1;
    end else if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
    //
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsInteger := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUnMyMySQL.BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit; Forca_1: Boolean): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUnMyMySQL.BuscaNovoCodigo_Int(QrAux: TmySQLQuery; Tabela,
  Campo: String; CamposExtras: array of String; ValExtras: array of Variant;
  SQLType: TSQLType; Padrao: Integer; Sinal: TSinal;
  dmkEdit: TdmkEdit): Integer;
var
  i, j: Integer;
  Liga, Valor: String;
begin
  if SQLType = stUpd then
  begin
    Result := Padrao;
    Exit;
  end;
  QrAux.Close;
  QrAux.SQL.Clear;
  if Sinal = siPositivo then
    QrAux.SQL.Add('SELECT MAX(' + Campo + ') Campo FROM ' + lowercase(Tabela))
  else
    QrAux.SQL.Add('SELECT MIN(' + Campo + ') Campo FROM ' + lowercase(Tabela));

  //

  Liga := 'WHERE';
  j := High(CamposExtras);
  for i := Low(CamposExtras) to j do
  begin
    Valor := Geral.VariavelToString(ValExtras[i]);
    {
    if (i < j) then
      QrAux.SQL.Add(Liga + ' ' + CamposExtras[i] + '=' + Valor + ', ')
    else
    }
      QrAux.SQL.Add(Liga + ' ' + CamposExtras[i] + '=' + Valor);
    Liga := 'AND';
  end;
  //
  try
    //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrAux, '', nil, True, True);
    QrAux.Open;
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrAux, '', nil, True, True);
  end;
  if Sinal = siPositivo then
  begin
    Result := QrAux.Fields[0].AsInteger + 1;
    if Result = 0 then Result := 1;
  end else begin
    Result := QrAux.Fields[0].AsInteger - 1;
    if Result = 0 then Result := -1;
  end;
  //
  if dmkEdit <> nil then
    dmkEdit.ValueVariant := Result;
end;

function TUnMyMySQL.BPGS1I32(Tabela, Campo, _WHERE,
  _AND: String; TipoSinal: TTipoSinal; SQLType: TSQLType;
  DefUpd: Integer): Integer;
var
  Tab, Fld, MiM: String;
  Atual, P: Integer;
  Forma: TSQLType;
begin
  Result := 0;
  case SQLType of
    stUpd: Result := DefUpd;
    stIns:
    begin
      Tab := LowerCase(Tabela);
      P := pos('.', Tab);
      if P > 0 then
        Tab := Copy(Tab, P + 1);
      //
      case TipoSinal of
        tsPos: Fld := 'BigIntPos';
        tsNeg: Fld := 'BigIntNeg';
        else Fld := 'BigInt???';
      end;
      QvUpdY.Close;
      QvUpdY.Database := DMod.MyDB;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;');
      QvUpdY.ExecSQL;
      //
      QvLivreY.Close;
      QvLivreY.Database := Dmod.MyDB;
      AbreMySQLQuery1(QvLivreY, [
      'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
      'WHERE Tabela="' + LowerCase(Tab) + '" ',
      'AND Campo="' + LowerCase(Campo) + '" ',
      'AND _WHERE="' + LowerCase(_WHERE) + '" ',
      'AND _AND="' + LowerCase(_AND) + '" ',
      '']);
      Atual := QvLivreY.FieldByName('Codigo').AsInteger;

      // Verificar se j� existe
      AbreMySQLQuery1(QvLivreY, [
      'SELECT ' + Campo + ' Codigo FROM ' + Tab,
      'WHERE ' + Campo + '=' + Geral.FF0(Atual),
      Geral.ATS_if(_WHERE<>'', ['AND ' + _WHERE]),
      Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
      '']);

      if (Atual = 0) then
        Forma := stIns
      else
        Forma := stUpd;
      // Evitar duplicidade buscando na tabela de inser��o:
      if (Atual = 0) or (QvLivreY.RecordCount > 0) then
      begin
        case TipoSinal of
          tsPos: MiM := 'MAX(';
          tsNeg: MiM := 'MIN(';
          else MiM := 'MINMAX?(';
        end;
        //
        AbreMySQLQuery1(QvLivreY, [
        'SELECT ' + MiM + Campo + ') Codigo FROM ' + Tab,
        Geral.ATS_if(_WHERE<>'', ['WHERE ' + _WHERE]),
        Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
        '']);
        //
        Atual := QvLivreY.FieldByName('Codigo').AsInteger;
      end;
      // Fim evitar duplicidade!
      //
      Result := Atual + 1;
      QvLivreY.Close;
      //


      SQLInsUpd(QvUpdY, Forma, 'gerlseq1', False, [
      Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
      Result], [Tab, Campo, _WHERE, _AND], True);
      //
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UNLOCK TABLES;');
      QvUpdY.ExecSQL;
    end;
    else Geral.MensagemBox(
    '"SQLType" n�o implementado no "BPGS1I32"',
    'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
end;

function TUnMyMySQL.CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
  ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if (i >= 0) and Auto_increment then
  begin
    Geral.MensagemBox('AVISO! Existem ' + IntToStr(i+1) + ' �ndices informados ' +
    'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MensagemBox('AVISO: O status da a��o est� definida como ' +
    '"' +
(*20200919
    Geral.LaTipo(Tipo) +
*)
    '"', 'AVISO IMPORTANTE', MB_OK+MB_ICONWARNING);
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' SET ')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ');
  end else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    // 2011-12-10
    if SQLCampos[i] = CO_JOKE_SQL then
    begin
      if (i < j) or UserDataAlterweb then
        QrUpd.SQL.Add(ValCampos[i] + ', ')
      else
        QrUpd.SQL.Add(ValCampos[i]);
    end else
    // fim 2011-12-10
    begin
      Valor := Geral.VariavelToString(ValCampos[i]);
      if (i < j) or UserDataAlterweb then
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
      else
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
      QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
    else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MensagemBox('SQL com Auto Increment e Indice ao mesmo tempo!',
      'ERRO', MB_OK+MB_ICONERROR);
    // N�o faz nada
  end else begin

    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      if Tipo = stIns then
        Liga := ', '
      else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;
      //
      // 2011-12-02
      if SQLIndex[i] = CO_JOKE_SQL then
        QrUpd.SQL.Add(Liga + ValIndex[i])
      else
      // fim 2011-12-02
      begin
        Valor := Geral.VariavelToString(ValIndex[i]);
        QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
      end;
    end;
  end;
  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;
  QrUpd.SQL.Add(';');
  Result := True;
end;

function TUnMyMySQL.CriaTabela(QrExec: TmySQLQuery; TabelaNome,
  TabelaBase: String; Memo: TMemo; Acao: TAcaoCriaTabela;
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel;
  FLCampos, FLIndices: TList): Boolean;
var
  i, j, k, u: Integer;
  Item, Opcoes, Campos: String;
  Indices, Conta: TStringList;
  Texto: TStringList;
  //TemControle: TTemControle;
begin
  Result := True;
{
  FLCampos  := TList.Create;
  FLIndices := TList.Create;
}
  Texto     := TStringList.Create;
  try
    //MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
    try
      QrExec.Close;
      QrExec.SQL.Clear;
      QrExec.SQL.Add('CREATE TABLE '+Lowercase(TabelaNome)+' (');
      //
      Item := Format(ivTabela_Nome, [TabelaNome]);
      for i:= 0 to FLCampos.Count - 1 do
      begin
        Opcoes := myco_;
        FRCampos := FLCampos[i];
        //ShowMensagem(FRCampos.Field);
        Opcoes := FRCampos.Tipo;
        if FRCampos.Null <> 'YES' then
          Opcoes := Opcoes + ' NOT NULL ';
        if FRCampos.Default <> myco_ then
          Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
          FRCampos.Default + mycoAspasDuplas;
        if FRCampos.Extra <> myco_ then
        begin
          if Uppercase(FRCampos.Extra) = Uppercase('auto_increment')
          then Opcoes := Opcoes + FRCampos.Extra;
        end;
        Opcoes := mycoEspacos2+FRCampos.Field+mycoEspaco+Opcoes;
        if i < (FLCampos.Count-1) then Opcoes := Opcoes + mycoVirgula;
        Texto.Add(Opcoes);
        //Texto[Texto.Count] := Opcoes;
      end;
      Indices := TStringList.Create;
      Indices.Sorted := True;
      Indices.Duplicates := (dupIgnore);
      try
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          Indices.Add(FRIndices.Key_name);
        end;
        u := Texto.Count-1;
        if Indices.Count > 0 then Texto[u] := Texto[u] + mycoVirgula;
        for k := 0 to Indices.Count -1 do
        begin
          Conta := TStringList.Create;
          Campos := myco_;
          for i := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[i];
            if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
          end;
          for j := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
          begin
            for i := 0 to FLIndices.Count -1 do
            begin
              FRIndices := FLIndices.Items[i];
              if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
              begin
                if Campos <> myco_ then Campos := Campos + ',';
                Campos := Campos + FRIndices.Column_name;
              end;
            end;
          end;
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Uppercase(Indices[k]) = Uppercase('PRIMARY') then
              Texto.Add('  PRIMARY KEY (' + Campos + ')')
            else if Uppercase(Copy(Indices[k], 1, 6)) = Uppercase('UNIQUE') then
              Texto.Add('  UNIQUE KEY '+ Indices[k] + ' ('+Campos+')')
            else
              Texto.Add('  INDEX '     + Indices[k] + ' ('+Campos+')');
            if k < (Indices.Count-1) then
            begin
             u := Texto.Count-1;
             Texto[u] := Texto[u]+mycoVirgula;
            end;
          end;
          Conta.Free;
        end;
      finally
        Indices.Free;
      end;
      Texto.Add(')');
      Texto.Add('CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
      for i := 0 to Texto.Count-1 do
      QrExec.SQL.Add(Texto[i]);
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', Memo, False, False);
      if Acao = actCreate then
      begin
        QrExec.ExecSQL;
        GravaAviso(Item+ivCriado, Memo);
      end;
    except
      on E: Exception do
        GravaAviso(Item + E.Message + sLineBreak + ivMsgERROCriar, Memo);
    end;
  finally
    FLCampos.Free;
    FLIndices.Free;
    Texto.Free;
  end;
end;

function TUnMyMySQL.ExecutaMySQLQuery1(Query: TMySQLQuery;
  SQL: array of String): Boolean;
var
  I: Integer;
begin
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) > 0 then
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MensagemBox('Texto da SQL indefinido no MySQLQuery!' + sLineBreak +
      'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
      Result := False;
    end
    else begin
      Query.ExecSQL;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MensagemBox('Erro ao tentar executar uma SQL no MySQLQuery!' + sLineBreak +
    'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
    raise;
  end;
end;

function TUnMyMySQL.ExecutaQuery(Query: TmySQLQuery): Boolean;
  procedure MostraQuery(Query: TmySQLQuery);
  var
    i: Integer;
    Texto: WideString;
  begin
    Texto := Query.SQL.Text;
    //
    for i := 1 to Query.ParamCount do
    begin
      Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
        TParam(Query.Params[i-1]).AsString + '*/';
    end;
    //
    Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
    //
    Geral.MensagemBox(Texto, 'ERRO SQL', MB_OK+MB_ICONERROR);
  end;
begin
  try
    Query.ExecSQL;
    Result := True;
  except
    Geral.MensagemBox(Query.SQL.Text, 'Erro de Execu��o de Query', MB_OK+MB_ICONERROR);
    MostraQuery(Query);
    raise;
  end;
end;

procedure TUnMyMySQL.GravaAviso(Aviso: String; Memo: TMemo);
begin
  if Memo <> nil then
  begin
    Memo.Lines.Add(Aviso);
    //if ToFile then
      //WriteLn(FText, Aviso);
  end;
end;

function TUnMyMySQL.ObtemCamposDeTabelaIdentica(DataBase: TmySQLDatabase;
  Tabela, Prefix: String): String;
var
  J: Integer;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT * FROM '+lowercase(tabela));
  QvSelY.SQL.Add('LIMIT 1');
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
  //////////////////////////////////////////////////////////////////////////////
  // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
  Result := ' ' + Prefix + QvSelY.Fields[0].FieldName + ' ' + sLineBreak;
  for J := 1 to QvSelY.Fields.Count-1 do
    Result := Result + ', ' + Prefix+QvSelY.Fields[J].FieldName + ' ' + sLineBreak;
  //////////////////////////////////////////////////////////////////////////////
  // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //
end;

function TUnMyMySQL.SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant; UserDataAlterweb: Boolean;
  ComplUpd: String; InfoSQLOnError, InfoErro: Boolean): Boolean;
begin
  Result := CarregaSQLInsUpd(QrUpd, Tipo, lowercase(Tabela), Auto_increment,
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, False, ComplUpd);
  if Result then
  begin
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      QrUpd.ExecSQL;
      Result := True;
    except
      if InfoSQLOnError then
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      if InfoErro then
        raise;
    end;
  end else;
end;

end.
