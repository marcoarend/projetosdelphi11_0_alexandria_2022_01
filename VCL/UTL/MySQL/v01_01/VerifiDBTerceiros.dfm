object FmVerifiDBTerceiros: TFmVerifiDBTerceiros
  Left = 382
  Top = 202
  Caption = 'FER-VRFBD-002 :: Verifica'#231#227'o de Banco de Dados Usu'#225'rio'
  ClientHeight = 586
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 468
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 453
      Width = 1008
      Height = 15
      Align = alBottom
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 60
        Align = alClient
        Caption = ' Verifica'#231#245'es: '
        TabOrder = 0
        object CkEstrutura: TCheckBox
          Left = 172
          Top = 16
          Width = 161
          Height = 17
          Caption = 'Estrutura do banco de dados.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkRegObrigat: TCheckBox
          Left = 524
          Top = 16
          Width = 157
          Height = 17
          Caption = 'Recria registros obrigat'#243'rios.'
          TabOrder = 1
        end
        object CkPergunta: TCheckBox
          Left = 12
          Top = 16
          Width = 155
          Height = 17
          Caption = 'Pergunta antes de executar.'
          TabOrder = 2
        end
        object CkEstrutLoc: TCheckBox
          Left = 336
          Top = 16
          Width = 185
          Height = 17
          Caption = 'Estrutura do banco de dados local.'
          TabOrder = 3
        end
        object CkEntidade0: TCheckBox
          Left = 12
          Top = 36
          Width = 129
          Height = 17
          Caption = 'Exclui entidade zero.'
          TabOrder = 4
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 161
      Width = 773
      Height = 292
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Avisos'
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 445
          Height = 264
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Importa'#231#245'es'
        ImageIndex = 1
        object Panel15: TPanel
          Left = 0
          Top = 0
          Width = 765
          Height = 264
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object GBNFSe: TGroupBox
            Left = 273
            Top = 1
            Width = 136
            Height = 262
            Align = alLeft
            Caption = ' NFS-e: '
            TabOrder = 0
            object BtLC116_03: TBitBtn
              Tag = 530
              Left = 12
              Top = 20
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = '&LC 116/03'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtLC116_03Click
            end
            object BtTsErrosAlertas: TBitBtn
              Tag = 531
              Left = 12
              Top = 64
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Erros e'#13#10'Alertas'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtTsErrosAlertasClick
            end
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 1
            Width = 136
            Height = 262
            Align = alLeft
            Caption = ' Geral: '
            TabOrder = 1
            object BtNFeLoadTabs: TBitBtn
              Tag = 532
              Left = 12
              Top = 20
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Cidades / Pa'#237'ses'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtNFeLoadTabsClick
            end
            object BtCNAE: TBitBtn
              Tag = 507
              Left = 12
              Top = 64
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'CNAE'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtCNAEClick
            end
            object BtBancos: TBitBtn
              Tag = 426
              Left = 12
              Top = 110
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Bancos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtBancosClick
            end
            object BtIBPTax: TBitBtn
              Left = 12
              Top = 154
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'IBPTax'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BtIBPTaxClick
            end
          end
          object GBCTe: TGroupBox
            Left = 409
            Top = 1
            Width = 136
            Height = 262
            Align = alLeft
            Caption = ' CT-e: '
            TabOrder = 2
            object BtCTeLayout: TBitBtn
              Tag = 614
              Left = 12
              Top = 20
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Layout'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtCTeLayoutClick
            end
          end
          object GBNFe: TGroupBox
            Left = 137
            Top = 1
            Width = 136
            Height = 262
            Align = alLeft
            Caption = ' NF-e: '
            TabOrder = 3
            object BtLayout: TBitBtn
              Tag = 394
              Left = 12
              Top = 20
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Layout'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtLayoutClick
            end
            object BtWebServices: TBitBtn
              Tag = 394
              Left = 12
              Top = 64
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Web Services'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtWebServicesClick
            end
          end
          object GBSped: TGroupBox
            Left = 545
            Top = 1
            Width = 136
            Height = 262
            Align = alLeft
            Caption = ' Sped: '
            TabOrder = 4
            object BtSpedLayout: TBitBtn
              Tag = 617
              Left = 12
              Top = 20
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Layout'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSpedLayoutClick
            end
          end
          object GroupBox2: TGroupBox
            Left = 681
            Top = 1
            Width = 136
            Height = 262
            Align = alLeft
            Caption = ' Outros: '
            TabOrder = 5
            object BtRestaura: TBitBtn
              Tag = 11
              Left = 12
              Top = 20
              Width = 112
              Height = 40
              Cursor = crHandPoint
              Caption = 'Restaura'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtRestauraClick
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Utilit'#225'rios'
        ImageIndex = 2
        object Button1: TButton
          Left = 4
          Top = 12
          Width = 145
          Height = 25
          Caption = 'Quebar xml em linhas'
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 60
      Width = 1008
      Height = 101
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object LaAvisoP1: TLabel
        Left = 13
        Top = 80
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoP2: TLabel
        Left = 12
        Top = 80
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 73
        Align = alTop
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 56
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 878
            Height = 56
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAviso1B: TLabel
              Left = 13
              Top = 34
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso1G: TLabel
              Left = 13
              Top = 18
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso1R: TLabel
              Left = 13
              Top = 2
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso2B: TLabel
              Left = 12
              Top = 33
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso2G: TLabel
              Left = 12
              Top = 17
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso2R: TLabel
              Left = 12
              Top = 1
              Width = 120
              Height = 17
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object Memo3: TMemo
              Left = 320
              Top = 8
              Width = 185
              Height = 40
              ReadOnly = True
              TabOrder = 0
              Visible = False
            end
          end
          object Panel14: TPanel
            Left = 990
            Top = 0
            Width = 14
            Height = 56
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
          end
          object Panel6: TPanel
            Left = 922
            Top = 0
            Width = 68
            Height = 56
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object LaTempoI: TLabel
              Left = 0
              Top = 0
              Width = 68
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTempoF: TLabel
              Left = 0
              Top = 14
              Width = 68
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTempoT: TLabel
              Left = 0
              Top = 28
              Width = 68
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
          end
          object Panel8: TPanel
            Left = 878
            Top = 0
            Width = 44
            Height = 56
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object Label4: TLabel
              Left = 0
              Top = 0
              Width = 44
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'In'#237'cio:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label5: TLabel
              Left = 0
              Top = 14
              Width = 44
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'T'#233'rmino:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label6: TLabel
              Left = 0
              Top = 28
              Width = 44
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'Tempo:'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 773
      Top = 161
      Width = 235
      Height = 292
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      ActivePage = TabSheet5
      Align = alRight
      TabOrder = 4
      object TabSheet4: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Tabelas desnecess'#225'rias'
        object ClTabsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 227
          Height = 226
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel10: TPanel
          Left = 0
          Top = 226
          Width = 227
          Height = 38
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtTabsNoNeed: TButton
            Left = 5
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
            OnClick = BtTabsNoNeedClick
          end
        end
      end
      object TabSheet5: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Campos desnecess'#225'rios'
        ImageIndex = 1
        object ClFldsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 227
          Height = 225
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel9: TPanel
          Left = 0
          Top = 225
          Width = 227
          Height = 39
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtClFldsNoNeed: TButton
            Left = 5
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
            OnClick = BtClFldsNoNeedClick
          end
        end
      end
      object TabSheet6: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = #205'ndices desnecess'#225'rios'
        ImageIndex = 2
        object ClIdxsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 227
          Height = 227
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel17: TPanel
          Left = 0
          Top = 227
          Width = 227
          Height = 37
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtClIdxsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 25
            Height = 25
            Caption = '...'
            TabOrder = 0
            OnClick = BtClFldsNoNeedClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 481
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 481
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 481
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 516
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 8
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sair'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 125
        Left = 10
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Atualiza'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BitBtn1: TBitBtn
        Tag = 532
        Left = 226
        Top = 5
        Width = 231
        Height = 40
        Cursor = crHandPoint
        Caption = 'Mover tabelas Cidades / Pa'#237'ses'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = False
        OnClick = BitBtn1Click
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 8
    Top = 16
  end
  object QrMover: TMySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 80
  end
  object PMTabsNoNeed: TPopupMenu
    OnPopup = PMTabsNoNeedPopup
    Left = 560
    Top = 264
    object Excluitabelas1: TMenuItem
      Caption = '&Exclui tabela(s)'
      OnClick = Excluitabelas1Click
    end
    object Listatabelas1: TMenuItem
      Caption = '&Lista tabela(s)'
      OnClick = Listatabelas1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Marcartodos2: TMenuItem
      Caption = '&Marcar todos'
      OnClick = Marcartodos2Click
    end
    object Desmarcartodos2: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = Desmarcartodos2Click
    end
  end
  object PMClFldsNoNeed: TPopupMenu
    OnPopup = PMClFldsNoNeedPopup
    Left = 552
    Top = 392
    object Excluicampos1: TMenuItem
      Caption = '&Exclui campo(s)'
      OnClick = Excluicampos1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Marcartodos1: TMenuItem
      Caption = '&Marcar todos'
      OnClick = Marcartodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
  end
end
