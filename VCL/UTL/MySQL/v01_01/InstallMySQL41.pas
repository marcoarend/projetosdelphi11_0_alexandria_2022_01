unit InstallMySQL41;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, StdCtrls, Buttons, ExtCtrls, UnInternalConsts, dmkGeral,
  Grids, DBGrids, ShellApi, ComObj;

type
  TFmInstallMySQL41 = class(TForm)
    BD: TmySQLDatabase;
    QrIns: TmySQLQuery;
    QrDel: TmySQLQuery;
    Memo1: TMemo;
    QrUser: TmySQLQuery;
    QrUserHost: TWideStringField;
    QrUserUser: TWideStringField;
    QrUserPassword: TWideStringField;
    QrUserSelect_priv: TBooleanField;
    QrUserInsert_priv: TBooleanField;
    QrUserUpdate_priv: TBooleanField;
    QrUserDelete_priv: TBooleanField;
    QrUserCreate_priv: TBooleanField;
    QrUserDrop_priv: TBooleanField;
    QrUserReload_priv: TBooleanField;
    QrUserShutdown_priv: TBooleanField;
    QrUserProcess_priv: TBooleanField;
    QrUserFile_priv: TBooleanField;
    QrUserGrant_priv: TBooleanField;
    QrUserReferences_priv: TBooleanField;
    QrUserIndex_priv: TBooleanField;
    QrUserAlter_priv: TBooleanField;
    DsUser: TDataSource;
    Panel1: TPanel;
    BtSair: TBitBtn;
    Timer1: TTimer;
    procedure BtSairClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FHabilitado: Boolean;
    function InstalaAplicativo: Boolean;
    //function ConfiguraMySQL: Boolean;
    //function ConectaMySQL: Boolean;
    //function AlteraSenha: Boolean;
    //function ReConectaMySQL: Boolean;
    //procedure Aguarda1Segundo;
    //procedure ReiniciaMaquina;
    //procedure DesligaMaquina;
  public
    { Public declarations }
    FSenha, FLogin: String;
    function MyExitWindows(RebootParam: Longword): Boolean;
  end;

var
  FmInstallMySQL41: TFmInstallMySQL41;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmInstallMySQL41.BtSairClick(Sender: TObject);
begin
  Geral.MB_Aviso('O aplicativo ser� finalizado!');
  Application.Terminate;
end;

procedure TFmInstallMySQL41.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FHabilitado then
  begin
    FHabilitado  := True;
    Timer1.Enabled := True;
  end;
end;

function TFmInstallMySQL41.InstalaAplicativo: Boolean;
const
  //MySQLInstall = 'C:\Dermatek\MySQL\Install\mysql41.msi';
  MySQLInstall = 'C:\Dermatek\MySQL\Install\MySQL41.exe';
  MySQLInstal2 = 'C:\Dermatek\MySQL\Install\mysql-essential-4.1.13a-win32.msi';
var
  //StartupInfo: TStartupInfo;
  //ProcessInformation: TProcessInformation;
  //rc : Boolean;
  //
  //Instalado, Localizado: Boolean;
  //i: Integer;
  IE: Variant;
  URL: String;
begin
  Result := False;
  Memo1.Lines.Add('Aguarde...');
  Memo1.Lines.Add('');
  if FileExists(MySQLInstal2) then
  begin
    //WinExec(MySQLInstall, SW_SHOWNORMAL);
    ShellExecute(Application.Handle, PChar('open'), PChar(MySQLInstal2),
      PChar(''), nil, SW_NORMAL);
  end else begin
    if Geral.MB_Pergunta('O ' + Application.Title +
    ' n�o localizou o instalador do MySQL em "' + MySQLInstal2 +
    '" desej� baix�-lo da internet?')= ID_YES then
    begin
      URL := 'ftp://ftp.pucpr.br/mysql/Downloads/MySQL-4.1/mysql-essential-4.1.13a-win32.msi';
      IE := CreateOleObject('InternetExplorer.Application');
      IE.Visible := True;
      IE.Navigate(URL)
      (*if  M L A G e r a l.DownloadFile(
      //'http://dev.mysql.com/get/Downloads/MySQL-4.1/mysql-essential-4.1.13a-win32.msi/from/pick',
      MySQLInstall) then
      ShellExecute(Application.Handle, PChar('open'), PChar(MySQLInstal2),
        PChar(''), nil, SW_NORMAL)
      else Geral.MB_('Download falhou!', 'Erro', MB_OK+MB_ICONERROR);*)
    end;
  end;
  Application.Terminate;
  (*if  M L A G e r a l.WinExecAndWait32(MeuTxt, 0) = WAIT_FAILED then
  begin
    Geral.MB_('Execu��o de "'+MeuTxt+'" Falhou'),
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
    Application.Terminate;
  end;*)
  ////////////////////////////////////////////////////////////////////////////
  (*for i := 120 downto 0 do
  begin
    Sleep(1000);
    if  M L A G e r a l .ExecutavelEstaInstalado('MySQL Server') then
    begin
      Localizado := True;
      Sleep(5000);
      Break;
    end;
    Memo1.Text := Memo1.Text +'.';
  end;
  if Localizado then Memo1.Lines.Add('MySQL instalado.')
  else Memo1.Lines.Add('Arquivo "'+MySQLInstall+'" n�o encontrado.');
  Result := True;
  Memo1.Lines.Clear;*)
end;

(*function TFmInstallMySQL41.ConfiguraMySQL: Boolean;
const
  //MySQLExec1 = 'C:\MySQL\bin\mysqladmin -password ';
  //MySQLExec2 = 'C:\MySQL\bin\mysqld.exe';
  MySQLExec3 = 'C:\MySQL\bin\winmysqladmin.exe';
var
  Text: String;
begin
  Result := False;
  //Text := MySQLExec1;
  try
    if FileExists(MySQLExec3) then
    begin
      Memo1.Lines.Add('O '+Application.Title+' vai iniciar a ativa��o do MySQL');
      Memo1.Lines.Add('Aguarde...');
      Memo1.Lines.Add('');
      Memo1.Refresh;
      Update;
      //////////////////////////////////////////////////////////////////////////
      //winexec(MySQLExec1+CO_BDSENHA, SW_SHOWMINIMIZED);
      //for i := 1 to 5 do Aguarda1segundo;
      //Text := MySQLExec2;
      //winexec(MySQLExec2, SW_SHOWMINIMIZED);
      //for i := 1 to 5 do Aguarda1segundo;
      Text := MySQLExec3;
      winexec(MySQLExec3, SW_SHOWMAXIMIZED);
      Geral.WriteAppKeyLM('mysqld', '\Software\Microsoft\Windows\'+
        'CurrentVersion\Run', 'C:\mysql\bin\winmysqladmin.exe', ktString);
      Memo1.Lines.Clear;
    end else begin
      Memo1.Lines.Add('Arquivo "'+Text+'" n�o encontrado.');
    end;
  except
    Memo1.Lines.Add('Erro ao ativar MySQLAdmin.')
  end;
end;*)

(*function TFmInstallMySQL41.AlteraSenha: Boolean;
begin
  try
    QrDel.ExecSQL;
    QrIns.Params[0].AsString := '%';
    QrIns.Params[1].AsString := VAR_BDSENHA;
    QrIns.ExecSQL;
    QrIns.Params[0].AsString := 'localhost';
    QrIns.Params[1].AsString := VAR_BDSENHA;
    QrIns.ExecSQL;
    QrIns.Params[0].AsString := '127.0.0.1';
    QrIns.Params[1].AsString := VAR_BDSENHA;
    QrIns.ExecSQL;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;*)

function TFmInstallMySQL41.MyExitWindows(RebootParam: Longword): Boolean;
var
  TTokenHd: THandle;
  TTokenPvg: TTokenPrivileges;
  cbtpPrevious: DWORD;
  rTTokenPvg: TTokenPrivileges;
  pcbtpPreviousRequired: DWORD;
  tpResult: Boolean;
const
  SE_SHUTDOWN_NAME = 'SeShutdownPrivilege';
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    tpResult := OpenProcessToken(GetCurrentProcess(),
      TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY,
      TTokenHd);
    if tpResult then
    begin
      tpResult := LookupPrivilegeValue(nil,
                                       SE_SHUTDOWN_NAME,
                                       TTokenPvg.Privileges[0].Luid);
      TTokenPvg.PrivilegeCount := 1;
      TTokenPvg.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
      cbtpPrevious := SizeOf(rTTokenPvg);
      pcbtpPreviousRequired := 0;
      if tpResult then
        Windows.AdjustTokenPrivileges(TTokenHd,
                                      False,
                                      TTokenPvg,
                                      cbtpPrevious,
                                      rTTokenPvg,
                                      pcbtpPreviousRequired);
    end;
  end;
  Result := ExitWindowsEx(RebootParam, 0);
end;

(*procedure TFmInstallMySQL41.ReiniciaMaquina;
begin
  MyExitWindows(EWX_REBOOT or EWX_FORCE);
end;*)

(*procedure TFmInstallMySQL41.DesligaMaquina;
begin
  MyExitWindows(EWX_REBOOT or EWX_FORCE);
end;*)

(*
 Parameters for MyExitWindows()


{************************************************************************}

{2. Console Shutdown Demo}

program Shutdown;
{$APPTYPE CONSOLE}

uses
  SysUtils,
  Windows;

// Shutdown Program
// (c) 2000 NeuralAbyss Software
// www.neuralabyss.com

var
  logoff: Boolean = False;
  reboot: Boolean = False;
  warn: Boolean = False;
  downQuick: Boolean = False;
  cancelShutdown: Boolean = False;
  powerOff: Boolean = False;
  timeDelay: Integer = 0;

function HasParam(Opt: Char): Boolean;
var
  x: Integer;
begin
  Result := False;
  for x := 1 to ParamCount do
    if (ParamStr(x) = '-' + opt) or (ParamStr(x) = '/' + opt) then Result := True;
end;

function GetErrorstring: string;
var
  lz: Cardinal;
  err: array[0..512] of Char;
begin
  lz := GetLastError;
  FormatMessage(FORMAT_MESSAGE_FROM _system, nil, lz, 0, @err, 512, nil);
  Result := string(err);
end;

procedure DoShutdown;
var
  rl, flgs: Cardinal;
  hToken: Cardinal;
  tkp: TOKEN_PRIVILEGES;
begin
  flgs := 0;
  if downQuick then flgs := flgs or EWX_FORCE;
  if not reboot then flgs := flgs or EWX_SHUTDOWN;
  if reboot then flgs := flgs or EWX_REBOOT;
  if poweroff and (not reboot) then flgs := flgs or EWX_POWEROFF;
  if logoff then flgs := (flgs and (not (EWX_REBOOT or EWX_SHUTDOWN or EWX_POWEROFF))) or
      EWX_LOGOFF;
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    if not OpenProcessToken(GetCurrentProcess, TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY,
      hToken) then
      Writeln('Cannot open process token. [' + GetErrorstring + ']')
    else
    begin
      if LookupPrivilegeValue(nil, 'SeShutdownPrivilege', tkp.Privileges[0].Luid) then
      begin
        tkp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
        tkp.PrivilegeCount           := 1;
        AdjustTokenPrivileges(hToken, False, tkp, 0, nil, rl);
        if GetLastError <> ERROR_SUCCESS then
          Writeln('Error adjusting process privileges.');
      end
      else
        Writeln('Cannot find privilege value. [' + GetErrorstring + ']');
    end;
    {   if CancelShutdown then
          if AbortSystemShutdown(nil) = False then
            Writeln(\'Cannot abort. [\' + GetErrorstring + \']\')
          else
           Writeln(\'Cancelled.\')
       else
       begin
         if InitiateSystemShutdown(nil, nil, timeDelay, downQuick, Reboot) = False then
            Writeln(\'Cannot go down. [\' + GetErrorstring + \']\')
         else
            Writeln(\'Shutting down!\');
       end;
    }
  end;
  //     else begin
  ExitWindowsEx(flgs, 0);
  //     end;
end;

begin
  Writeln('Shutdown v0.3 for Win32 (similar to the Linux version)');
  Writeln('(c) 2000 NeuralAbyss Software. All Rights Reserved.');
  if HasParam('?') or (ParamCount = 0) then
  begin
    Writeln('Usage:    shutdown [-akrhfnc] [-t secs]');
    Writeln('                  -k:      don''t really shutdown, only warn.');
    Writeln('                  -r:      reboot after shutdown.');
    Writeln('                  -h:      halt after shutdown.');
    Writeln('                  -p:      power off after shutdown');
    Writeln('                  -l:      log off only');
    Writeln('                  -n:      kill apps that don''t want to die.');
    Writeln('                  -c:      cancel a running shutdown.');
  end
  else
  begin
    if HasParam('k') then warn := True;
    if HasParam('r') then reboot := True;
    if HasParam('h') and reboot then
    begin
      Writeln('Error: Cannot specify -r and -h parameters together!');
      Exit;
    end;
    if HasParam('h') then reboot := False;
    if HasParam('n') then downQuick := True;
    if HasParam('c') then cancelShutdown := True;
    if HasParam('p') then powerOff := True;
    if HasParam('l') then logoff := True;
    DoShutdown;
  end;
end.





// Parameters for MyExitWindows()


EWX_LOGOFF

Shuts down all processes running in the security context of the process that called the
ExitWindowsEx function. Then it logs the user off.

Alle Prozesse des Benutzers werden beendet, danach wird der Benutzer abgemeldet.

EWX_POWEROFF

Shuts down the system and turns off the power.
The system must support the power-off feature.
Windows NT/2000/XP:
The calling process must have the SE_SHUTDOWN_NAME privilege.

F�hrt Windows herunter und setzt den Computer in den StandBy-Modus,
sofern von der Hardware unterst�tzt.

EWX_REBOOT

Shuts down the system and then restarts the system.
Windows NT/2000/XP: The calling process must have the SE_SHUTDOWN_NAME privilege.

F�hrt Windows herunter und startet es neu.

EWX_SHUTDOWN

Shuts down the system to a point at which it is safe to turn off the power.
All file buffers have been flushed to disk, and all running processes have stopped.
If the system supports the power-off feature, the power is also turned off.
Windows NT/2000/XP: The calling process must have the SE_SHUTDOWN_NAME privilege.

F�hrt Windows herunter.


EWX_FORCE

Forces processes to terminate. When this flag is set,
the system does not send the WM_QUERYENDSESSION and WM_ENDSESSION messages.
This can cause the applications to lose data.
Therefore, you should only use this flag in an emergency.

Die aktiven Prozesse werden zwangsweise und ohne R�ckfrage beendet.

EWX_FORCEIFHUNG

Windows 2000/XP: Forces processes to terminate if they do not respond to the
WM_QUERYENDSESSION or WM_ENDSESSION message. This flag is ignored if EWX_FORCE is used.

Windows 2000/XP: Die aktiven Prozesse werden aufgefordert, sich selbst zu beenden und
m�ssen dies best�tigen. Reagieren sie nicht, werden sie zwangsweise beendet.






More Links to this topic
http://www.swissdelphicenter.ch/de/showcode.php?id=1232


*)
procedure TFmInstallMySQL41.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  InstalaAplicativo;
end;

end.


