unit MyDBCheck;

interface

uses System.Generics.Collections, System.StrUtils,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls,  ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnGOTOy, dbgrids, dmkPermissoes, dmkCheckGroup,
  Variants, TypInfo, dmkGeral, DmkDAC_PF, UndmkProcFunc, dmkEditCB, CheckLst,
  dmkDBLookupComboBox, UnDmkEnums, UMySQLDB, UnProjGroup_Consts;

type
  TSelCodsExecution = (sceNone, sceExecA);
  THackCheckGroup = class(TCustomCheckGroup);
  TAcessFormModo = (afmSoMaster, afmSoBoss, afmNegarSemAviso, afmNegarComAviso,
                    afmParcial, afmParcialSemEdicao, afmAcessoTotal);
  TResultVerify = (rvOK, rvErr, rvAbort);
  TMyMotivo = (mymotDifere=0, mymotSemRef=1, mymotUserDel=2);
  TTabStepCod = (tscEntidades, tscEtqPrinCad);
  TMyDBCheck = class(TObject)
  private
    { Private declarations }
    procedure AdicionaFldNoNeed(ClFldsNoNeed: TCheckListBox;
              Tabela, Campo: String);
    procedure AdicionaIdxNoNeed(ClIdxsNoNeed: TCheckListBox;
              Tabela, Campo: String);
    procedure CarregaIndicesCDR(FLIndices: TList<TIndices>);
    function  VerificaEstrutura(DataBase: TmySQLDatabase; Memo: TMemo;
              RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G,
              LaTempoI, LaTempoF, LaTempoT: TLabel; ClTabsNoNeed: TCheckListBox;
              PB: TProgressBar; ClFldsNoNeed, ClIdxsNoNeed: TCheckListBox;
              QeiLnk: Boolean = False; Janelas: Boolean = False): Boolean;
{$IFNDEF NAO_CRIA_USER_DB}
    function  VerificaEstrutLoc(DataBase: TmySQLDatabase; Memo: TMemo;
              LaAviso1R, LaAviso2R, LaTempoF, LaTempoT: TLabel): Boolean;
{$ENDIF}
    function  TabelaAtiva(Tabela: String): Boolean;
    function  IndiceExiste(Database: TmySQLDatabase; Tabela, Indice: String;
              Memo: TMemo): Integer;
    function  CampoAtivo(Database: TmySQLDatabase; TabelaNome, TabelaBase,
              Campo: String; Tem_Del: Boolean; Memo: TMemo): Integer;
    function  ExcluiIndice(Database: TMySQLDatabase; Tabela, IdxNome,
              Aviso: String; Motivo: TMyMotivo; Memo: TMemo; ClIdxsNoNeed:
              TCheckListBox): TResultVerify;
    function  CriaIndice(Database: TmySQLDatabase; Tabela, IdxNome, Aviso:
              String; Memo: TMemo): TResultVerify;
    function  RecriaIndice(Database: TmySQLDatabase; Tabela, IdxNome, Aviso:
              String; Memo: TMemo): TResultVerify;
    //function  VerificaRegistrosDePerfis(DataBase: TmySQLDatabase;
              //Memo: TMemo; Avisa: Boolean): TResultVerify;
    {$IfNDef SemCashier}
    (* 23/02/2015 => Aparentemente n�o usa
    function  VerificaRegistrosDeUserSets(DataBase: TmySQLDatabase;
              Memo: TMemo; Avisa: Boolean): TResultVerify;
    function  VerificaRegistrosDeImpDOS(DataBase: TmySQLDatabase;
              Memo: TMemo; Avisa: Boolean): TResultVerify;
    function  VerificaRegistrosDeCheque(DataBase: TmySQLDatabase;
              Memo: TMemo; Avisa: Boolean): TResultVerify;
    *)
    {$EndIf}
    function  VerificaRegistrosDeJanelas(DataBase: TmySQLDatabase;
              Memo: TMemo; Avisa: Boolean; LaAviso1G, LaAviso2G: TLabel;
              PB: TProgressBar): TResultVerify;
    //
    procedure VerificaControle(Memo: TMemo);
    procedure VerificaOutrosDepois(Database: TmySQLDataBase; Memo: TMemo);
    function  VerificaRegistrosDeArquivo(Database: TMySQLDatabase; TabelaNome,
              TabelaBase, NomeTabItemDel: String; Memo: TMemo; Avisa: Boolean):
              TResultVerify;
    function  VerificaRegistrosDBMQeiLnk(Database: TMySQLDatabase; TabelaNome,
              TabelaBase: String; Tem_Del: Boolean; QeiLnkOn:
              TCRCTableManage; NomeTabItemDel: String; Memo: TMemo): Boolean;

    procedure VerificaVariaveis(Memo: TMemo);
    //
    // T E M P O R A R I O
    procedure ConsertaTabela(Tabela: String);
  public
    { Public declarations }
    FProgress: TProgressBar;
    //
    function  CRCTableManageToQeiLnkOn(CRCTableManage: TCRCTableManage): Boolean;
    function  ItemTuplePurposeToDBMQeiLnk(ItemTuplePurpose: TItemTuplePurpose;
              TabelaNome, CampoNome: String): Boolean;
    function  TabelaExiste(Tabela: String): Boolean;
    function  ImpedeVerificacao(): Boolean;
    procedure GravaAviso(Aviso: String; Memo: TMemo; ToFile: Boolean = False);
    function  EfetuaVerificacoes(DataBase, DataBaseLoc: TmySQLDataBase;
              Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
              RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2, LaAviso1R,
              LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
              LaTempoF, LaTempoT: TLabel; ClTabsNoNeed: TCheckListBox;
              (* 2011-10-10*)PB: TProgressBar; ClFldsNoNeed, ClIdxsNoNeed:
              TCheckListBox; QeiLnk: Boolean = False; Janelas: Boolean = False):
              Boolean;
    procedure DropCamposTabelas(ClFldsNoNeed: TCheckListBox;
              DataBase: TmySQLDataBase; Memo: TMemo);
    function  ConfiguraOrdemCamposTabela(CamposTxt_Opc, CamposFields_Opc: array of String;
              CamposTxt_Def, CamposFields_Def, Separador: String;
              var CamposTxt_Sel, CamposFields_Sel: String): Boolean;
    function  CriaDatabase(Query: TmySQLQuery; Database: String): Boolean;
    function  CriaTabela(Database: TmySQLDatabase; TabelaNome, TabelaBase: String;
              Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean;
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel;
              Tem_Sync: Boolean = False; Idx_CDR: Boolean = False;
              QeiLnkOn: TCRCTableManage = TCRCTableManage.crctmStandBy;
              NomeTabItemDel: String = ''): Boolean;
    function  VerificaCampos(Database: TmySQLDatabase; TabelaNome, TabelaBase:
              String; Memo: TMemo; RecriaRegObrig, Tem_Del, Tem_Sync: Boolean;
              Idx_CDR: Boolean; QeiLnkOn: TCRCTableManage; LaAviso1B, LaAviso2B,
              LaAviso1G, LaAviso2G: TLabel; ClFldsNoNeed, ClIdxsNoNeed:
              TCheckListBox; QeiLnk: Boolean = False; NomeTabItemDel:
              String = ''): TResultVerify;
    function  CampoExiste(Tabela, Campo: String; Memo: TMemo): Integer;
    function  VerificaRegistrosObrigatorios_Inclui(DataBase: TmySQLDatabase;
              TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean;
              LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): TResultVerify;
    function  VerificaRegistrosObrigatorios_Corrige(DataBase: TmySQLDatabase;
              TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean): TResultVerify;
    (* 23/02/2015 => Aparentemente n�o usa
    function  AcessoNegadoAoForm_2(Tabela, Form : String; ModoAcesso:
              TAcessFormModo; LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): Boolean;
    *)
    function  AcessoNegadoAoForm_3(Tabela, Form, Descricao: String; ModoAcesso:
              TAcessFormModo; LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): Boolean;
    function  AcessoNegadoAoForm_4(ModoAcesso: TAcessFmModo; Form: String): Boolean;
    function  AcessoAoForm(Form: String; ModoAcesso: TAcessFmModo): Integer;
    function  CriaFormAntigo(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFormModo): Boolean;
    function  CriaFm(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; Titulo: String = ''): Boolean;
    //function  Account(DataBase, Tabela, CampoA, CampoS: String; Procura: TBDProcura):
    //          TFoundInBD;
    //function  QuaisItens: TSelType;
    function  Quais_Selecionou(const Query: TmySQLQuery; DBGrid:
              TDBGrid; var Quais: TSelType): Boolean;
    function  Quais_Selecionou_ExecProc(const Query: TmySQLQuery; DBGrid:
              TDBGrid; var Quais: TSelType; Procedure1: TProcedure): Boolean;
    function  ExcluiRegistro(QrExecSQL, QrDados: TmySQLQuery; TabelaTarget: String;
              CamposSource, CamposTarget: array of String; ReopenQrDados: Boolean;
              Pergunta: String = ''): Boolean;
              // QuaisExclui Quais exclui Quais_Exclui
    function  QuaisItens_Exclui(QrExecSQL, QrDados: TmySQLQuery; DBGrid: TDBGrid;
              TabelaTarget: String; CamposSource, CamposTarget: array of String;
              Quais: TSelType; SQL_AfterDel: String; NaoReabre: Boolean = False;
              Pergunta: Boolean = True): Integer;
    function  QuaisItens_Altera(QrExecSQL, QrDados: TmySQLQuery;
              DBGrid: TDBGrid; Tabela: String;
              SQLCamposAlt, SQLIndexAlt, SQLIndexSource: array of String;
              SQLValores: array of Variant; Quais: TSelType;
              Pergunta, AvisaAlterados: Boolean): Boolean;
    function  ObtemData(const DtDefault: TDateTime; var DtSelect:
              TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
              const HabilitaHora: Boolean = False; Titulo: String = ''): Boolean;
              //ObtemDataEUsuario , GetDataEUser , GetDataEUsuario
    function  ObtemHora(const HrDefault: TTime; var HrSelect: TTime; Titulo:
              String = ''): Boolean;
      function  ObtemDataEUser(const UserDefault: Integer; var UserSelect: Integer;
              const DtDefault: TDateTime; var DtSelect:
              TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
              const HabilitaHora: Boolean = False): Boolean;
    function  ObtemData_Juros_Multa(const MinData, DtDefault: TDateTime;
              var DtSelect: TDateTime; const DefJuros, DefMulta: Double;
              var Juros, Multa: Double): Boolean;
    function  LiberaPelaSenhaBoss(Aviso: String = ''; SenhaExtra: String = ''): Boolean;
    function  LiberaPelaSenhaMaster(Aviso: String = ''): Boolean;
    function  LiberaPelaSenhaAdmin(Aviso: String = ''): Boolean;
    function  LiberaPelaSenhaUsuario(Usuario: Integer; Aviso: String = ''): Boolean;
    function  LiberaPelaSenhaPwdLibFunc(Aviso: String = ''): Boolean;
    function  EscolheCodigoUnico(Aviso, Titulo, Prompt: String;
              DataSource: TmySQLQuery; ListSource: TDataSource;
              ListField: String; Default: Variant; SQL: array of String;
              DataBase: TmySQLDatabase; PermiteSelZero: Boolean;
              MostraSbCadastro: Boolean = False; Tabela: String = '';
              NovoCodigo: TNovoCodigo = TNovoCodigo.ncIdefinido): Variant;
    function  Escolhe2CodigoUnicos(var Resultado1, Resultado2: Variant;
              const Aviso, Titulo, Prompt1, Prompt2: String;
              const DataSource1, DataSource2: TmySQLQuery; const ListSource1, ListSource2: TDataSource;
              const ListField1, ListField2: String; const Default1, Default2: Variant; const SQL1, SQL2: array of String;
              const DataBase: TmySQLDatabase; const PermiteSelZero: Boolean;
              const MostraSbCadastro: Boolean = False; const Tabela: String = '';
              const NovoCodigo: TNovoCodigo = TNovoCodigo.ncIdefinido): Boolean;
    function  EscolheCodigoUniGrid(Aviso, Titulo, Prompt: String;
              DataSource: TDataSource(*; ListSource: TDataSource;
              ListField: String;* Default: Variant; SQL: array of String;
              DataBase: TmySQLDatabase; PermiteSelZero: Boolean*);
              MultiSelect, Destroi: Boolean;
              BtOKVisible: Boolean = True; BtTodosVisible: Boolean = True;
              BtNenhumVisible: Boolean = True): Boolean;
    function  EscolheCodigosMultiplos_0(Aviso, Titulo, Prompt: String;
              DataSource: TDataSource; FieldAtivo, FieldNivel,
              FieldNome: String; SQLExec, SQLOpen: array of String;
              QrExecSQL: TmySQLQuery): Boolean;
(*
    function  EscolheCodigosMultiplos_A(Aviso, Titulo, Prompt: String;
              DataSource: TDataSource; FieldAtivo, FieldNivel, FieldNome: String; SQLExec,
              SQLOpen: array of String; DestTab, DestMaster, DestDetail, DestSelec1: String;
              ValrMaster: Integer; QrDetail: TmySQLQuery; FieldDetail: String;
              ExcluiAnteriores: Boolean; QrExecSQL: TmySQLQuery): Boolean;
*)
    function  EscolheCodigosMultiplos_A(Aviso, Titulo, Prompt: String;
              DataSource: TDataSource; FieldAtivo, FieldNivel,
              FieldNome: String; SQLExec, SQLOpen: array of String;
              DestTab: String; DestMaster: array of String;
              DestDetail, DestSelec1: String;
              ValrMaster: array of Variant;
              QrDetail: TmySQLQuery; FieldDetail: String;
              ExcluiAnteriores: Boolean; QrExecSQL: TmySQLQuery): Boolean;
    function  EscolheTextoUnico(Aviso, Titulo, Prompt: String;
              DataSource: TmySQLQuery; ListSource: TDataSource;
              ListField: String; Default: Variant; SQL: array of String;
              DataBase: TmySQLDatabase; PermiteVazio: Boolean; LocF7CodiFldName,
              LocF7NameFldName: String): Variant;
    function  ObtemCodUsuZStepCodUni(Tabela: TTabStepCod): Integer;
    function  Obtem_verProc: String;
    (* 2017-10-14 => Movido para o UnGrl_Geral
    function  LiberaModulo(Aplicativo: Integer; Modulo, HabilModulo: String;
              MostraMsg: Boolean = False): Boolean;
    *)
    procedure VerificaEntiCliInt();
    procedure PreencheCheckGroupClieForn(CGClie, CGForn: TCustomCheckGroup);
    procedure CadastraItemESetaCodigo(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; dmkEdCB: TdmkEditCB; dmkCB:
              TdmkDBLookupComboBox; Qry: TmySQLQuery);
    procedure Define_Form_ID(Form_ID: String);
    procedure AvisaDBTerceiros();
    procedure AtivaCodigos(Aviso, Titulo, Tabela, FldCodi, FldNome, FldAtiv:
              String; SQL: array of String; DB: TmySQLDataBase; DefLoc: Integer);
{$IFNDEF NAO_CRIA_USER_DB}
    procedure MostraInfoSeq(Janela, Descricao: array of string;
              Coluna1: String = ''; Coluna2: String = '';
              TamColuna1: Integer = 120; TamColuna2: Integer = 350);
{$ENDIF}
    function  DropIndicesTabelas(ClIdxsNoNeed: TCheckListBox; Database:
              TMySQLDatabase; Memo: TMemo): TResultVerify;
    function  ObtemVersao(var VerZero, Versao: Int64): Boolean;
  end;

var
  DBCheck: TMyDBCheck;
  FToFile    : Boolean = False;
  FText      : TextFile;
  FTabelas   : TList<TTabelas>;
  FTabLoc    : TList<TTabelas>;
  FIncrement : TStringList;
  FCheque    : TStringList;
  FLCampos   : TList<TCampos>;
  FLIndices  : TList<TIndices>;
  FLJanelas  : TList<TJanelas>;
  FLQeiLnk   : TList<TQeiLnk>;
  FRCampos   : TCampos;
  FRIndices  : TIndices;
  FRJanelas  : TJanelas;
  FRQeiLnk   : TQeiLnk;
  FPergunta  : Boolean;
  FListaSQL  : TStringList;
  FPerfis    : TStringList;
  FUserSets  : TStringList;
  FImpDOS    : TStringList;
  FViuControle: Integer;
  FCriarAtivoAlterWeb: Boolean;

const
  CO_TAB_INDX_CB4_MAX_INDX = 4;
  CO_TAB_INDX_CB4_DB_NAMES: array[0..CO_TAB_INDX_CB4_MAX_INDX] of String = ('',    '',        'FD',           '',        '');
  CO_TAB_INDX_CB4_DESCREVE: array[0..CO_TAB_INDX_CB4_MAX_INDX] of String = ('N/D', 'N�o tem', 'Foto digital', 'Scanner', 'XML');
  //
  CO_TAB_INDX_CB4_INDEFINI = 0;
  CO_TAB_INDX_CB4_SEM_DOCU = 1;
  //
  CO_TAB_NAME_CB4_FOTODOCU = 'FD';
  CO_TAB_INDX_CB4_FOTODOCU = 2;
  //
  CO_TAB_INDX_CB4_SCANDOCU = 3;
  //
  CO_TAB_INDX_CB4_ARQU_XML = 4;
  //

implementation

// � chamado por FmVerifiDB
uses UnMyObjects, Module, UnMsgInt, QuaisItens, GetData, GetHora, GetDataEUser,
{$IFNDEF NAO_CRIA_USER_DB}UCreate, InfoSeq, {$ENDIF}
{$IFDEF UsaWSuport}WebBrowser, WSuporte, {$ENDIF}
{$IFNdef SemBDTErceiros}VerifiDBTerceiros, {$EndIf}
ModuleGeral, Senha, SelCod, Sel2Cod, SelCods, SelStr, OrderFieldsImp, ZStepCodUni,
AtivaCod, UMySQLModule, UnGrl_Consts, UnGrl_Vars, GerlShowGrid, MyListas;

function TMyDBCheck.EfetuaVerificacoes(DataBase, DataBaseLoc: TmySQLDatabase;
  Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
  LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
  LaTempoI, LaTempoF, LaTempoT: TLabel;
  ClTabsNoNeed: TCheckListBox; (* 2012-09-07*)PB: TProgressBar; ClFldsNoNeed,
  ClIdxsNoNeed: TCheckListBox; QeiLnk: Boolean; Janelas: Boolean): Boolean;
var
  WinPath : array[0..144] of Char;
  Name: String;
  //Cursor: TCursor;
  VerMrg: Integer;
  //
  i: Integer;
  TabNome, TabBase, Item, TabItDel: String;
  DefTabela: TTabelas;
  Tem_Del, Tem_Sync, Idx_CDR: Boolean;
  QeiLnkOn: TCRCTableManage;
  Versao: Int64;
begin
  Tem_Del  := False;
  Tem_Sync := False;
  Idx_CDR  := False;
  QeiLnkOn := TCRCTableManage.crctmIndef;
  TabItDel := '';
  //
  VAR_CONTA_FIELD_IN_DB := 0;
  VAR_CONTA_ERRO_NULL_DEFAULT := 0;
  ClTabsNoNeed.Items.Clear;
  LaTempoI.Caption := '...';
  LaTempoF.Caption := '...';
  LaTempoT.Caption := '...';
  Result := False;
  if ImpedeVerificacao() then
  begin
    Exit;
  end;
  Result := False;
  //
  // ini 2023-08-08 - evitar n�o cri��o da tabela controle!
  try
    Versao := USQLDB.ObtemVersaoAppDB(Dmod.MyDB);
  except
    Versao := 0;
  end;
  // fim 2023-08-08
  if Geral.MB_Pergunta('Ser� realizada uma verifica��o na base de dados: ' +
    'Vers�o atual do BD: ' + Geral.FF0(Versao) +
    sLineBreak + 'Nova vers�o: ' +  Geral.FF0(CO_VERSAO) +
    sLineBreak + 'Servidor: ' + Database.Host + sLineBreak + 'Banco de dados: '
    + Database.DatabaseName + sLineBreak +
    'A altera��es realizados nesta verifica��o somente ser�o revers�veis perante restaura��o de backup.'
    + sLineBreak + 'Para fazer um backup clique em "n�o" e feche esta janela.'
    + sLineBreak + ' Na janela principal h� um bot�o de backup!' + sLineBreak
    + 'Confirma assim mesmo a verifica��o?') <> ID_YES
  then
    Exit;
  Memo.Update;
  Application.ProcessMessages;
  Result := True;
  FPergunta := Pergunta;
  //Cursor := Screen.ActiveForm.Cursor;
  Screen.Cursor := crHourGlass;
  try
    // verifica se a vers�o atual � mais nova
    // se n�o � impede a verifica��o
    if DataBase.DatabaseName <> '' then
    begin
      Dmod.QrMas.Close;
      Dmod.QrMas.Database := DataBase;
      Dmod.QrMas.SQL.Clear;
      Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + DataBase.DatabaseName);
      Dmod.QrMas.SQL.Add('LIKE "Controle"');
      UnDmkDAC_PF.AbreQuery(Dmod.QrMas, DataBase);
      if Dmod.QrMas.RecordCount > 0 then
      begin
        Dmod.QrMas.Close;
        Dmod.QrMas.Database := DataBase;
        Dmod.QrMas.SQL.Clear;
        Dmod.QrMas.SQL.Add('SHOW COLUMNS FROM controle LIKE "versao"');
        UnDmkDAC_PF.AbreQuery(Dmod.QrMas, DataBase);
        if Dmod.QrMas.RecordCount > 0 then
        begin
          Dmod.QrMas.Close;
          Dmod.QrMas.Database := DataBase;
          Dmod.QrMas.SQL.Clear;
          Dmod.QrMas.SQL.Add('SELECT Versao FROM controle');
          UnDmkDAC_PF.AbreQuery(Dmod.QrMas, DataBase);
          //
          if CO_VERSAO < Dmod.QrMas.FieldByName('Versao').AsInteger then
          begin
            if Geral.MB_Aviso('Verifica��o de banco de dados cancelada! '+
            sLineBreak + ' A vers�o deste arquivo � inferior a cadastrada ' +
            'no banco de dados.' + sLineBreak + 'Deseja continuar?') = ID_YES then
            begin
              if not DBCheck.LiberaPelaSenhaBoss then
              begin
                Screen.Cursor := crDefault;
                Exit;
              end;
            end;
          end;
        end;
      end;
    end else Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
    //
    GetWindowsDirectory(WinPath,144);
    //Name := StrPas(WinPath)+'C:\Dermatek';
    Name := 'C:\Dermatek';
    try
    if not DirectoryExists(Name) then ForceDirectories(Name);
    except
      Geral.MB_ERRO('N�o foi poss�vel criar o diret�rio:' + Name);
    end;
    Name := Name+'\LogFile.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(FText, Name);
    ReWrite(FText);
    FToFile := True;
    try
      MyList.VerificaOutrosAntes(DataBase, Memo);
      VerificaVariaveis(Memo);
      if Memo <> nil then Memo.Lines.Clear;
      Memo.Refresh;
      Memo.Update;
      // Primeiro VAR_MYARQDB para definir tabelas no final como VAR_GOTOMySQLDBNAME
      if VAR_MYARQDB <> nil then
        VerificaEstrutura(VAR_MYARQDB, Memo, RecriaRegObrig,
          LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
          LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, PB, ClFldsNoNeed,
          ClIdxsNoNeed, QeiLnk, Janelas)
      else if Memo <> nil then
        //Memo.Lines.Add('Informa��o: "VAR_MYARQDB" n�o definido.');
        MyObjects.Informa2(LaAvisoP1, LaAvisoP2, False, '"VAR_MYARQDB" n�o definido.');
      if Estrutura then
        VerificaEstrutura(Database, Memo, RecriaRegObrig,
          LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
          LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, PB, ClFldsNoNeed,
          ClIdxsNoNeed, QeiLnk, Janelas)
      else begin
        if QeiLnk then
        begin
          Dmod.QrMas.Close;
          Dmod.QrMas.Database := DataBase;
          Dmod.QrMas.SQL.Clear;
          Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + DataBase.DatabaseName);
          UnDmkDAC_PF.AbreQuery(Dmod.QrMas, DataBase);
          if PB <> nil then
          begin
            PB.Max := Dmod.QrMas.RecordCount;
            PB.Position := 0;
          end;
          while not Dmod.QrMas.Eof do
          begin
            if PB <> nil then
              PB.Position := PB.Position + 1;
            FTabelas := TList<TTabelas>.Create;
            MyList.CriaListaTabelas(DataBase, FTabelas);
            for i := 0 to FTabelas.Count -1 do
            begin
              DefTabela := FTabelas[i];
              if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
              begin
                TabBase  := DefTabela.TabBase;
                Tem_Del  := DefTabela.Tem_Del;
                Tem_Sync := DefTabela.Tem_Sync;
                Idx_CDR  := DefTabela.Idx_CDR;
                QeiLnkOn := TCRCTableManage(DefTabela.QeiLnkOn);
                TabItDel := DefTabela.TabItDel;
                //
                Break;
              end;
            end;
            if QeiLnkOn <> TCRCTableManage.crctmIndef then
              if CRCTableManageToQeiLnkOn(TCRCTableManage(QeiLnkOn)) then
                VerificaRegistrosDBMQeiLnk(Database, TabNome, TabBase,
                Tem_Del, QeiLnkOn, TabItDel, Memo);
            //
            Dmod.QrMas.Next;
          end;
        end;
        if Janelas then
        begin
          MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando registros "Janelas"');
          VerificaRegistrosDeJanelas(DataBase, Memo, True, LaAviso1G, LaAviso2G, PB);
          MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando "Outros Depois"');
        end;
        Dmod.QrMas.Close;
        Dmod.QrMas.Database := Database;//VAR_GOTOMySQLDBNAME;
        Dmod.QrSQL.Close;
        Dmod.QrSQL.Database := Database;//VAR_GOTOMySQLDBNAME;
        Dmod.QrIdx.Close;
        Dmod.QrIdx.Database := Database;//VAR_GOTOMySQLDBNAME;
      end;
{$IFNDEF NAO_CRIA_USER_DB}
      if EstrutLoc then
        VerificaEstrutLoc(DataBaseLoc, Memo,
      LaAviso1R, LaAviso2R, LaTempoF, LaTempoT);
      if Controle then
        VerificaControle(Memo);
{$ENDIF}
      VerificaOutrosDepois(DataBase, Memo);
      //
      if CO_VERMCW < CO_VERMLA then
        VerMrg := CO_VERMCW
      else
        VerMrg := CO_VERMLA;
      //  MyDBCheck
      {$ifDef VER320} // Delphi 25 Tokyo
        //fim 2022-01-26
        //Geral.WriteAppKeyLM2('Versao', Application.Title, CO_VERSAO, ktString);
        //Geral.WriteAppKeyLM2('VerMrg', Application.Title,    VerMrg, ktString);
        // fim 2022-01/10
        Geral.WriteAppKeyLM2('Versao', Application.Title, Integer(CO_VERSAO), ktInteger);
        Geral.WriteAppKeyLM2('VerMrg', Application.Title,    Integer(VerMrg), ktInteger);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, ['Versao', 'VerMrg'], [], [CO_VERSAO, VerMrg], [], False);
  (*
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE controle SET Versao =:P0');
        Dmod.QrUpd.Params[0].AsInteger := CO_VERSAO;
        Dmod.QrUpd.ExecSQL;
  *)
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, ['Versao', 'VerMrg'], [], [Integer(CO_VERSAO), Integer(VerMrg)], [], False);
      {$Else}
        {$ifDef VER350} // Delphi 28 Alexandria
          Geral.WriteAppKeyCU('Vers28', Application.Title, Geral.FF0(CO_VERSAO), ktString);
          Geral.WriteAppKeyCU('VMrg28', Application.Title,    Geral.FF0(VerMrg), ktString);
          try
            Geral.WriteAppKeyLM2('Vers28', Application.Title, Geral.FF0(CO_VERSAO), ktString);
            Geral.WriteAppKeyLM2('VMrg28', Application.Title,    Geral.FF0(VerMrg), ktString);
          finally
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, ['Vers28', 'VerMrg'], [], [CO_VERSAO, VerMrg], [], False);
          end;
        {$EndIf}
      {$EndIf}
      CloseFile(FText);
    except
      CloseFile(FText);
      raise;
    end;
    FToFile := False;
    Screen.Cursor := crDefault;
  except
    //Result := False;
    Screen.Cursor := crDefault;
    raise;
  end;
  //
  if VAR_CONTA_ERRO_NULL_DEFAULT > 0 then
  begin
    Memo.Lines.Add('');
    Memo.Lines.Add('');
    Memo.Lines.Add('===========================================================');
    Memo.Lines.Add('===========================================================');
    Memo.Lines.Add('AVISO! >> ' + Geral.FF0(VAR_CONTA_ERRO_NULL_DEFAULT) +
                   ' campos com NULL e DEFAULT de um total de ' +
                   Geral.FF0(VAR_CONTA_FIELD_IN_DB));
    Memo.Lines.Add('===========================================================');
    Memo.Lines.Add('===========================================================');
  end;
end;

function TMyDBCheck.VerificaEstrutura(DataBase: TmySQLDatabase;
  Memo: TMemo; RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B,
  LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF, LaTempoT: TLabel;
  ClTabsNoNeed: TCheckListBox; PB: TProgressBar; ClFldsNoNeed, ClIdxsNoNeed:
  TCheckListBox; QeiLnk, Janelas: Boolean): Boolean;
var
  //p,
  i, Resp: Integer;
  //TabA, TabB,
  TabNome, TabBase, Item, TabItDel: String;
  TempoI, TempoF, TempoT: TTime;
  DefTabela: TTabelas;
  Tem_Del, Tem_Sync, Idx_CDR: Boolean;
  QeiLnkOn: TCRCTableManage;
  SQLManual: String;
  JaValendoFlds: Boolean;
begin
  Tem_Del  := False;
  Tem_Sync := False;
  Idx_CDR  := False;
  QeiLnkOn := TCRCTableManage.crctmIndef;
  TempoI   := Time();
  TabItDel := '';
  JaValendoFlds := VAR_CONTA_TABLE_START = '';
  //
  //
  if DataBase = Dmod.MyDB then FViuControle := 0 else FViuControle := 4;
  Result := True;
  //GravaAviso(Format(ivMsgEstruturaBD, [DataBase.DatabaseName]), Memo);
  MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgEstruturaBD, [DataBase.DatabaseName]));
  //GravaAviso(FormatDateTime(ivFormatIni, Now), Memo);
  MyObjects.Informa(LaTempoI, False, FormatDateTime('hh:nn:ss:zzz', TempoI));
  try
    FTabelas := TList<TTabelas>.Create;
    MyList.CriaListaTabelas(DataBase, FTabelas);


(*
        for i := 0 to FTabelas.Count -1 do
        begin
          if Lowercase(FTabelas[i].TabCria) = 'plaallcad' then
            Geral.MB_Info('plaallcad');
        end;
*)
    try
      MyObjects.Informa2(LaAviso1R, LaAviso2R, True, LaAviso1R.Caption + '(' +
        Geral.FF0(FTabelas.Count) + ' tabelas)');
(* 2018-04-08*)
      MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas prim�rias');
      UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrMas, DataBase, [
      'SHOW TABLES FROM ' + DataBase.DatabaseName,
      'LIKE "dbmqeilnk" ',
      '']);
      if Dmod.QrMas.RecordCount = 0 then
      begin
        Memo.Lines.Add('Criar DBMQeiLnk aqui!');
        SQLManual := Geral.ATS([
        '  CREATE TABLE dbmqeilnk ( ',
        '    KLAskrTab varchar(60) NOT NULL  DEFAULT "?????", ',
        '    KLAskrCol varchar(60) NOT NULL  DEFAULT "?????", ',
        '    KLRplyTab varchar(60) NOT NULL  DEFAULT "?????", ',
        '    KLRplyCol varchar(60) NOT NULL  DEFAULT "?????", ',
        '    KLMTbsCol varchar(60) NOT NULL  DEFAULT "?????", ',
        '    KLMTabDel varchar(60) NOT NULL  DEFAULT "?????", ',
(*
        '    AlterWeb tinyint(1) NOT NULL  DEFAULT "1", ',
        '    AWServerID int(11) NOT NULL  DEFAULT "0", ',
        '    AWStatSinc tinyint(1) NOT NULL  DEFAULT "0", ',
        '    Ativo tinyint(1) NOT NULL  DEFAULT "1", ',
*)
        '    KLPurpose tinyint(1)  NOT NULL  DEFAULT "0", ',
        '    TbPurpose tinyint(1)  NOT NULL  DEFAULT "0", ',
        '    TbManage tinyint(1)   NOT NULL  DEFAULT "0", ',
        '    PRIMARY KEY (KLAskrTab,KLAskrCol,KLRplyTab,KLRplyCol) ',
        '  ) ',
        '  CHARACTER SET latin1 COLLATE latin1_swedish_ci; ',
        '']);
        DataBase.Execute(SQLManual);
        Memo.Lines.Add(SQLManual);
      end;
      //
      MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas j� criadas');

      //Session.GetTableNames(mycoSkinTanX, mycoAsterisco, True, True, FArquivos);
      Dmod.QrMas.Close;
      Dmod.QrMas.Database := DataBase;
      Dmod.QrMas.SQL.Clear;
      Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + DataBase.DatabaseName);
      UnDmkDAC_PF.AbreQuery(Dmod.QrMas, DataBase);
      if PB <> nil then
      begin
        PB.Max := Dmod.QrMas.RecordCount;
        PB.Position := 0;
      end;
      while not Dmod.QrMas.Eof do
      begin
        if PB <> nil then
          PB.Position := PB.Position + 1;
        // Fazer Primeiro controle !!!
        if FViuControle = 0 then
        begin
          TabNome := 'controle';
          FViuControle := 1;
          try
            Dmod.ReopenControle();
          except
            TabNome := Dmod.QrMas.Fields[0].AsString;
          end;
        end else
        // Fazer depois ctrlgeral !!!
        {
        if FViuControle = 1 then
        begin
          TabNome := 'ctrlgeral';
          FViuControle := 2;
          try
            Dmod.ReopenControle();
          except
            TabNome := Dmod.QrMas.Fields[0].AsString;
          end;
        end else
        }
        if FViuControle = 2 then
        begin
          Dmod.QrMas.Prior;
          FViuControle := 3;
          TabNome := Dmod.QrMas.Fields[0].AsString;
        end else
          TabNome := Dmod.QrMas.Fields[0].AsString;


        TabBase := '';
{
        for i := 0 to FTabelas.Count -1 do
        begin
          p := pos(';', FTabelas[i]);
          if p > 0 then
          begin
            TabA := Uppercase(Copy(FTabelas[i], 1, p - 1));
            TabB := Uppercase(TabNome);
            if TabA = TabB then
            begin
              TabBase := Copy(FTabelas[i], p + 1);
              Break;
            end;
          end;
        end;
}
(*
        if (Lowercase(TabNome) = CO_???_TAB_VMI) then
          Geral.MB_Info(TabNome);
*)
        //
(*
        if Lowercase(TabNome) = 'plaallcad' then
          Geral.MB_Info('plaallcad');
*)
        for i := 0 to FTabelas.Count -1 do
        begin
          DefTabela := FTabelas[i];
          if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
          begin
            TabBase  := DefTabela.TabBase;
            Tem_Del  := DefTabela.Tem_Del;
            Tem_Sync := DefTabela.Tem_Sync;
            Idx_CDR  := DefTabela.Idx_CDR;
            QeiLnkOn := TCRCTableManage(DefTabela.QeiLnkOn);
            TabItDel := DefTabela.TabItDel;
            //
            Break;
          end;
        end;
        if TabBase = '' then
          TabBase := TabNome;

        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas j� criadas');
        MyObjects.Informa2(LaAviso1G, LaAviso2G, True, 'Verificando tabela "' + TabNome + '"');

        //ChangeFileExt(FArquivos[i], myco_);
        if not TabelaAtiva(TabNome) then
        begin
          Item := Format(ivTabela_Nome, [TabNome]);
          if MyList.ExcluiTab then
          begin
            if not FPergunta then Resp := ID_YES else
            Resp := Geral.MB_Pergunta(Format(ivExclTabela,
            [Application.Title, TabNome]));
            if Resp = ID_YES then
            begin
              try
                // Ver lct
                if Lowercase(Copy(TabNome, 1, 4)) <> 'lct0' then
                  //GravaAviso(Item+ivMsgExcluidaSeria, Memo);
                  ClTabsNoNeed.Items.Add(TabNome);
              except
                GravaAviso(Item+ivMsgERROExcluir, Memo);
              end;
            end else begin
              GravaAviso(Item+ivAbortExclUser, Memo);
              if Resp = ID_CANCEL then
              begin
                GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
                Screen.Cursor := crDefault;
                Exit;
              end;
            end;
          end;
        end else
        begin
          if not JaValendoFlds then
            JaValendoFlds := Uppercase(VAR_CONTA_TABLE_START) = Uppercase(TabNome);
          if JaValendoFlds then
          begin
            Screen.Cursor := crHourGlass;
            if VerificaCampos(DataBase, TabNome, TabBase, Memo, RecriaRegObrig,
            Tem_Del, Tem_Sync, Idx_CDR, QeiLnkOn, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
            ClFldsNoNeed, ClIdxsNoNeed, QeiLnk, TabItDel) <> rvOK then
            begin
              GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
              Screen.Cursor := crDefault;
              Exit;
            end;
          end;
        end;
        Dmod.QrMas.Next;
      end;
{
      for i := 0 to FTabelas.Count -1 do
      begin
        p := pos(';', FTabelas[i]);
        if p = 0 then
        begin
          TabNome := FTabelas[i];
          TabBase := FTabelas[i];
        end else begin
          TabNome := Copy(FTabelas[i], 1, p - 1);
          TabBase := Copy(FTabelas[i], p + 1);
        end;
}
      MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando tabelas a serem criadas');
      if PB <> nil then
      begin
        PB.Max := FTabelas.Count;
        PB.Position := 0;
      end;
      for i := 0 to FTabelas.Count -1 do
      begin
        MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"' + TabNome + '" verificando se existe');
        if PB <> nil then
          PB.Position := PB.Position + 1;
        //
        DefTabela := FTabelas[i];
(*
        if Lowercase(FTabelas[i].TabCria) = Lowercase('plaallcad') then
          Geral.MB_Info('PlaAllCad');
*)
{        if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
        begin
}
          TabNome  := DefTabela.TabCria;
          TabBase  := DefTabela.TabBase;
          TabItDel := DefTabela.TabItDel;
          Tem_Del  := DefTabela.Tem_Del;
          Tem_Sync := DefTabela.Tem_Sync;
          Idx_CDR  := DefTabela.Idx_CDR;
          QeiLnkOn := TCRCTableManage(DefTabela.QeiLnkOn);

          if TabBase = '' then
            TabBase := TabNome;
{
          Break;
        end;
}

        if not TabelaExiste(TabNome) then
        begin
          Screen.Cursor := crHourGlass;
          if pos('"' + LowerCase(TabNome) + '"', Dmod.TabelasQueNaoQueroCriar()) > 0 then
            ClTabsNoNeed.Items.Add('A tabela "' + TabNome + '" n�o ser� criada!')
          else begin
            MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"' + TabNome + '" criando tabela ');
            CriaTabela(Database, TabNome, TabBase, Memo, actCreate, Tem_Del,
            LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, Tem_Sync, Idx_CDR,
            QeiLnkOn, TabItDel);
          end;
        end;
      end;
      MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '');
      Dmod.QrMas.Close;
    finally
      FTabelas.Free;
    end;
    if DataBase = Dmod.MyDB then
    begin
      // Na Web N�o D�!
      if TMeuDB <> VAR_DBWEB then
      begin
        {$IfNDef SemCashier}
        (* 23/02/2015 => Aparentemente n�o usa
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando registros "UserSets"');
        VerificaRegistrosDeUserSets(Dmod.MyLocDatabase, Memo, True);
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando registros "ImpDOS"');
        VerificaRegistrosDeImpDOS(DataBase, Memo, True);
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando registros "Cheque"');
        VerificaRegistrosDeCheque(DataBase, Memo, True);
        *)
        {$EndIf}
      end;
      if Janelas then
      begin
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando registros "Janelas"');
        VerificaRegistrosDeJanelas(DataBase, Memo, True, LaAviso1G, LaAviso2G, PB);
        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando "Outros Depois"');
      end;
      MyList.VerificaOutrosDepois(DataBase, Memo);
    end;
    //GravaAviso(Format(ivMsgFimAnalBD, [Database.DatabaseName]), Memo);
    MyObjects.Informa2(LaAviso1G, LaAviso2G, False, '...');
    MyObjects.Informa2(LaAviso1B, LaAviso2B, False, '...');
    MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [Database.DatabaseName]));
    //GravaAviso(FormatDateTime(ivFormatFim, Now), Memo);
    TempoF := Time();
    MyObjects.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));
    TempoT := TempoF - TempoI;
    if TempoT < 0 then
      TempoT := TempoT + 1;
    MyObjects.Informa(LaTempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
  except
    MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [Database.DatabaseName]));
    raise;
  end;
end;

procedure TMyDBCheck.VerificaOutrosDepois(Database: TmySQLDataBase;
  Memo: TMemo);
begin
  if DataBase.DatabaseName = TMeuDB then
  begin
    // Verifica tabela de Clientes Internos
    VerificaEntiCliInt();
  end;
end;

procedure TMyDBCheck.VerificaEntiCliInt();
var
  CodEnti, CodFilial, CodCliInt, TipoTabLct: Integer;
begin
  {$IFDEF DEFINE_VARLCT}
    TipoTabLct := 1;
  {$ELSE}
    TipoTabLct := 0;
  {$ENDIF}

  Dmod.QrAux.Close;
  Dmod.QrAux.Database := Dmod.MyDB;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW TABLES FROM ' + DMod.MyDB.DatabaseName);
  Dmod.QrAux.SQL.Add('LIKE "enticliint"');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT ent.Codigo, ent.CliInt, ent.Filial,');
    Dmod.QrAux.SQL.Add('eci.CodCliInt, eci.CodEnti, eci.CodFilial,');
    Dmod.QrAux.SQL.Add('IF(eci.CodCliInt IS NULL, 0, 1) Existe');
    Dmod.QrAux.SQL.Add('FROM entidades ent');
    Dmod.QrAux.SQL.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=ent.CliInt');
    Dmod.QrAux.SQL.Add('WHERE ent.CliInt <> 0');
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.First;
      while not Dmod.QrAux.Eof do
      begin
        CodEnti   := Dmod.QrAux.FieldByName('Codigo').AsInteger;
        CodCliInt := Dmod.QrAux.FieldByName('CliInt').AsInteger;
        CodFilial := Dmod.QrAux.FieldByName('Filial').AsInteger;
        if Dmod.QrAux.FieldByName('Existe').AsInteger = 0 then
        begin
          // Criar pois n�o existe
          try
            // 2013-01-02 - Evitar erro de CliInt = -11 no  s y n d i c
{
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticliint', False, [
            'CodEnti', 'CodFilial', 'TipoTabLct'], ['CodCliInt'], [
            CodEnti, CodFilial, TipoTabLct], [CodCliInt], True);
}
            UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'enticliint', False,
            ['CodEnti', 'CodFilial', 'TipoTabLct'],
            ['CodCliInt'],
            ['Ativo'],
            [CodEnti, CodFilial, TipoTabLct],
            [CodCliInt],
            [1], True);
          except
            ;// Nada
          end;
        end else begin
          if Dmod.QrAux.FieldByName('Filial').AsInteger
          <> Dmod.QrAux.FieldByName('CodFilial').AsInteger then
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
            'CodFilial', 'TipoTabLct'], ['CodCliInt'], [
            CodFilial, TipoTabLct], [CodCliInt], True);
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
end;

{$IFNDEF NAO_CRIA_USER_DB}
function TMyDBCheck.VerificaEstrutLoc(DataBase: TmySQLDatabase; Memo: TMemo;
LaAviso1R, LaAviso2R, LaTempoF, LaTempoT: TLabel): Boolean;
var
  i: Integer;
  Cria, termina: Boolean;
  TempoF: TTime;
  DefTabela: TTabelas;
  NomeTab: String;
begin
  Result := True;
  try
    FTabLoc := TList<TTabelas>.Create;
    MyList.CriaListaTabelasLocais(FTabLoc);
    try
      for i := 0 to FTabLoc.Count -1 do
      begin
        DefTabela := FTabLoc[i];
        NomeTab := DefTabela.TabCria;
        Cria    := False;
        Termina := False;
        if FPergunta then
        begin
          case Geral.MB_Pergunta('Deseja excluir e recriar a tabela '+
          'local "'+ NomeTab +'"?') of
            ID_YES: Cria := True;
            ID_Cancel: Termina := True;
          end;
          if Termina then Exit;
        end else Cria := True;
        if Cria then
        begin
          GravaAviso(Format(ivMsgRecriandoTabLoc, [NomeTab]), Memo);
{$IFNDEF NAO_CRIA_USER_DB}
          Ucriar.RecriaTempTable(NomeTab, DModG.QrUpdPID1, False);
{$ifNDef SemDBLocal}
          Ucriar.RecriaTabelaLocal(NomeTab, 1);
          {$EndIf}
          {$EndIf}
        end;
      end;
    finally
      FTabLoc.Free;
    end;
    GravaAviso(ivMsgFimAnalBDLoc, Memo);
    TempoF := Time();
    MyObjects.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));
  except
    MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [Database.DatabaseName]));
    raise;
  end;
end;
{$ENDIF}

function TMyDBCheck.VerificaCampos(Database: TmySQLDatabase; TabelaNome,
  TabelaBase: String; Memo: TMemo; RecriaRegObrig, Tem_Del, Tem_Sync: Boolean;
  Idx_CDR: Boolean; QeiLnkOn: TCRCTableManage; LaAviso1B, LaAviso2B, LaAviso1G,
  LaAviso2G: TLabel; ClFldsNoNeed, ClIdxsNoNeed: TCheckListBox;
  QeiLnk: Boolean; NomeTabItemDel: String): TResultVerify;
var
  I, K, Resp, EhIdx: Integer;
  Opcoes, Item, Nome, IdxNome: String;
  Indices: TStringList;
  IdxExiste: Boolean;
  TemControle: TTemControle;
begin
  Result := rvOK;
  try
    Dmod.QrNTV.Close;
    Dmod.QrNTV.Database := Database;
    Dmod.QrNTV.SQL.Clear;
    Dmod.QrNTV.SQL.Add('SHOW FIELDS FROM ' + lowercase(TabelaNome));
    UMyMod.AbreQuery(Dmod.QrNTV, Database, 'TMyDBCheck.VerificaCampos() > Dmod.QrNTV');
    try
      FLCampos  := TList<TCampos>.Create;
      FLIndices := TList<TIndices>.Create;
      FLQeiLnk  := TList<TQeiLnk>.Create;
      try
        FCriarAtivoAlterWeb := True;
        TemControle := [];
        MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
        MyList.CriaListaQeiLnk(TabelaNome, FLQeiLnk);
        if QeiLnk then
        begin
          if CRCTableManageToQeiLnkOn(TCRCTableManage(QeiLnkOn)) then
            VerificaRegistrosDBMQeiLnk(Database, TabelaNome, TabelaBase,
            Tem_Del, QeiLnkOn, NomeTabItemDel, Memo);
          //Exit;
        end;
        //
        //
        if (tctrlNil in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Ativo';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FRCampos.Coments    := '';
          FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
          FLCampos.Add(FRCampos);
        end else
        begin
          if (tctrlLok in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'Lk';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES';
            FRCampos.Key        := '';
            FRCampos.Default    := '0';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if (tctrlCad in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'DataCad';
            FRCampos.Tipo       := 'date';
            FRCampos.Null       := 'YES';
            FRCampos.Key        := '';
            FRCampos.Default    := '';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if (tctrlAlt in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'DataAlt';
            FRCampos.Tipo       := 'date';
            FRCampos.Null       := 'YES';
            FRCampos.Key        := '';
            FRCampos.Default    := '';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if (tctrlCad in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'UserCad';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES';
            FRCampos.Key        := '';
            FRCampos.Default    := '0';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if (tctrlAlt in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'UserAlt';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES';
            FRCampos.Key        := '';
            FRCampos.Default    := '0';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if (tctrlWeb in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'AlterWeb';
            FRCampos.Tipo       := 'tinyint(1)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '1';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if (tctrlAWSI in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'AWServerID';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            if Idx_CDR then
              FRCampos.Key        := 'PRI'
            else
              FRCampos.Key        := '';
            FRCampos.Default    := '0';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpERPSync;
            FLCampos.Add(FRCampos);
            //
            New(FRCampos);
            FRCampos.Field      := 'AWStatSinc';
            FRCampos.Tipo       := 'tinyint(1)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '0'; // SQLType
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpERPSync;
            FLCampos.Add(FRCampos);
            //
          end;
          if (tctrlAti in TemControle) then
          begin
            New(FRCampos);
            FRCampos.Field      := 'Ativo';
            FRCampos.Tipo       := 'tinyint(1)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '1';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
          end;
          if Tem_Del then
          begin
            New(FRCampos);
            FRCampos.Field      := 'UserDel';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '0';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
            //
            New(FRCampos);
            FRCampos.Field      := 'DataDel';
            FRCampos.Tipo       := 'datetime';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '0000-00-00 00:00:00';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
            //
            New(FRCampos);
            FRCampos.Field      := 'MotvDel';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '0'; // 0=N�o info
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
            //
          end else
          if Tem_Sync then
          begin
            New(FRCampos);
            FRCampos.Field      := 'LastModifi';
            FRCampos.Tipo       := 'datetime';
            FRCampos.Null       := 'YES';
            FRCampos.Key        := '';
            FRCampos.Default    := '0000-00-00 00:00:00';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
            //
            New(FRCampos);
            FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
            FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR           //2 => Exclus�o
            FRCampos.Key        := '';
            FRCampos.Default    := '0';
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
            //
            New(FRCampos);
            FRCampos.Field      := 'CliIntSync';
            FRCampos.Tipo       := 'int(11)';
            FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
            FRCampos.Key        := '';
            FRCampos.Default    := '0'; // 0=N�o info
            FRCampos.Extra      := '';
            FRCampos.Coments    := '';
            FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
            FLCampos.Add(FRCampos);
            //
          end;
        end;
        //if TabelaNome = 'lct 0001a' then
          //Geral.MB_Aviso('Aqui');
        if Tem_Del = False then
        begin
          MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
          if Idx_CDR then
            CarregaIndicesCDR(FLIndices);
        end else
        begin
          MyList.CriaListaIndices(TabelaNome, TabelaNome, FLIndices);
          if Idx_CDR then
            CarregaIndicesCDR(FLIndices);
        end;
        while not Dmod.QrNTV.Eof do
        begin
        //
          Nome := Dmod.QrNTV.FieldByName('Field').AsString;
          //try
            Item := Format(ivCampo_Nome, [TabelaNome, Nome]);
          (*except
            on E: Exception do
              Geral.MB_Erro(E.Message);
          end;*)
          //if Lowercase(TabelaNome) = 'stqmovitsb' then
            //ShowMessage(TabelaNome);
          case CampoAtivo(DataBase, TabelaNome, TabelaBase, Nome, Tem_Del, Memo) of
            1:
            begin
              try
                Opcoes := FRCampos.Tipo;
                // N�o � aqui! fica junto com o Tipo! Ex: "tinyint(3) unsigned"
                //if pos(Uppercase('unsigned'), Uppercase(FRCampos.Extra)) > 0 then
                  // Opcoes := Opcoes + ' unsigned ';
                // inicio BERLIN
                //if (FRCampos.Null <> 'YES') then
                if (FRCampos.Null = 'NO') or (FRCampos.Default <> myco_ ) then
                  Opcoes := Opcoes + ' NOT NULL '
                // inicio BERLIN
                else
                  Opcoes := Opcoes + ' NULL ';
                // Final Berlin
                if FRCampos.Default <> myco_ then
                  Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                    FRCampos.Default + mycoAspasDuplas;
                if FRCampos.Extra <> myco_ then
                begin
                  //if Uppercase(FRCampos.Extra) = Uppercase('auto_increment')
                  //then Opcoes := Opcoes + FRCampos.Extra;
                  if pos(Uppercase('auto_increment'), Uppercase(FRCampos.Extra)) > 0 then
                    Opcoes := Opcoes + ' auto_increment ';
                end;
                Dmod.QrSQL.Close;
                Dmod.QrSQL.DataBase := DataBase;
                Dmod.QrSQL.SQL.Clear;
                Dmod.QrSQL.SQL.Add(mycoALTERTABLE+TabelaNome+mycoCHANGE+
                  Nome+mycoEspaco+Nome+mycoEspaco+Opcoes);
                if ((not FPergunta) and (MyList.ExcluiReg))
                then Resp := ID_YES else
                Resp := Geral.MB_Pergunta('O campo '+Nome+
                  ' da tabela '+TabelaNome+' difere do esperado e ser� regularizado.'
                  +sLineBreak+'Confirma a altera��o?');
                if Resp = ID_YES then
                begin
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  GravaAviso(Item+ivAlterado, Memo);
                end else begin
                  GravaAviso(Item+ivAbortAlteUser, Memo);
                  if Resp = ID_CANCEL then
                  begin
                    Result := rvAbort;
                    Exit;
                  end;
                end;
              except
                GravaAviso(Item+ivMsgERROAlterar, Memo);
                raise;
              end;
            end;
            9:
            begin
              { 2012-05-30 Desabilitado por causa do
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MB_Aviso(Format(ivExclCampo,
                [Application.Title, Nome, TabelaNome]));
              if Resp = ID_YES then
              begin
                try
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.DataBase := DataBase;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add(mycoALTERTABLE+TabelaNome+mycoDROP+
                    Nome);
                  if MyList.ExcluiReg then
                  begin
                    if not FPergunta then Resp := ID_YES else
                    Resp := Geral.MB_Pergunta('Confirma a EXCLUS�O do campo '+
                    Nome+'?');
                    if Resp = ID_YES then
                    begin
                      Dmod.QrSQL.ExecSQL;
                      GravaAviso(Item+ivMsgExcluida, Memo);
                    end else begin
                      GravaAviso(Item+ivAbortExclUser, Memo);
                      if Resp = ID_CANCEL then
                      begin
                        Result := rvAbort;
                        Exit;
                      end;
                    end;
                  end;
                except
                  GravaAviso(Item+ivMsgERROexcluir, Memo);
                  raise;
                end;
              end else begin
                GravaAviso(Item+ivAbortExclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  Result := rvAbort;
                  Exit;
                end;
              end;
              }
              //
              AdicionaFldNoNeed(ClFldsNoNeed, TabelaNome, Nome);
              //ClFldsNoNeed.Items.Add(TabelaNome + '.' + Nome);
              GravaAviso('O campo "' + TabelaNome + '.' + Nome +
                '" deveria ser exclu�do!', Memo);
            end;
          end;
          Dmod.QrNTV.Next;
        end;
        for i:= 0 to FLCampos.Count - 1 do
        begin
          FRCampos := FLCampos[i];
          if CampoExiste(TabelaNome, FRCampos.Field, Memo) = 2 then
          begin
            Item := Format(ivCampo_Nome, [TabelaNome, FRCampos.Field]);
            try
              Opcoes := FRCampos.Tipo;
              // inicio BERLIN
              //if (FRCampos.Null <> 'YES') then
              if (FRCampos.Null = 'NO') or (FRCampos.Default <> myco_ ) then
                Opcoes := Opcoes + ' NOT NULL '
              // inicio BERLIN
              else
                Opcoes := Opcoes + ' NULL ';
              // final BERLIN
              if FRCampos.Default <> myco_ then
              begin
                if UpperCase(FRCampos.Default) = 'CURRENT_TIMESTAMP' then
                  Opcoes := Opcoes + ' DEFAULT ' + FRCampos.Default
                else
                  Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                              FRCampos.Default + mycoAspasDuplas;
              end;
              if FRCampos.Extra <> myco_ then
                Opcoes := Opcoes + ' ' + FRCampos.Extra;
              //
              Dmod.QrSQL.Close;
              Dmod.QrSQL.DataBase := DataBase;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add(mycoALTERTABLE+TabelaNome+mycoADD+
              FRCampos.Field+mycoEspaco+Opcoes);
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MB_Pergunta('O campo '+FRCampos.Field+
              ' n�o existe na tabela ' + TabelaNome + ' e ser� criado.' +
              slineBreak + 'Confirma a cria��o do campo?');
              if Resp = ID_YES then
              begin
                UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                Dmod.QrSQL.ExecSQL;
                GravaAviso(Item+ivcriado, Memo);
              end else begin
                GravaAviso(Item+ivAbortInclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  Result := rvAbort;
                  GravaAviso(Item+ivAbortInclUser, Memo);
                  //
                  Exit;
                end;
              end;
            except
              GravaAviso(Item+ivMsgERROCriar, Memo);
              raise;
            end;
          end;
        end;
        Indices := TStringList.Create;
        Indices.Sorted := True;
        Indices.Duplicates := (dupIgnore);
        try
          for k := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[k];
            Indices.Add(FRIndices.Key_name);
          end;
          Dmod.QrIdx.Close;
          Dmod.QrIdx.Database := DataBase;
          Dmod.QrIdx.SQL.Clear;
          Dmod.QrIdx.SQL.Add('SHOW KEYS FROM ' + LowerCase(TabelaNome));
          UMyMod.AbreQuery(Dmod.QrIdx, Database, 'TMyDBCheck.VerificaCampos()');
          while not Dmod.QrIdx.Eof do
          begin
            IdxExiste := False;
            IdxNome := Dmod.QrIdx.FieldByName('Key_name').AsString;
            //if (Uppercase(Tabela) =  'ENTIDADES') and (Uppercase(IdxNome) =  'UNIQUE1') then
              //ShowMessage('ENTIDADES.UNIQUE1');
            Item := Format(ivIndice_Nome, [TabelaNome, IdxNome]);
            for k := 0 to Indices.Count -1 do
              if Indices[k] = IdxNome then
              IdxExiste := True;
            if not IdxExiste then
            begin
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MB_Pergunta(Format(ivExclIndice,
                [Application.Title, IdxNome, TabelaNome]));
              if Resp = ID_YES then
              begin
                if ExcluiIndice(Database, TabelaNome, IdxNome, Item,
                mymotSemRef, Memo, ClIdxsNoNeed) <>
                   rvOK then Result := rvAbort;
              end else begin
                GravaAviso(Item+ivAbortExclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  Result := rvAbort;
                  Exit;
                end;
              end;
            end;
            Dmod.QrIdx.Next;
          end;
          Dmod.QrIdx.Close;
          for k := 0 to Indices.Count -1 do
          begin
            EhIdx := IndiceExiste(Database, TabelaNome, Indices[k], Memo);
            if EhIdx in ([2,3]) then
              if RecriaIndice(Database, TabelaNome, Indices[k], Item, Memo) <> rvOK
              then begin
                Result := rvAbort;
                //Exit;
              end;
            if EhIdx in ([0]) then
            begin
              if CriaIndice(Database, TabelaNome, Indices[k], Item, Memo) <> rvOK then
              begin
                Result := rvAbort;
                //Exit;
              end;
            end;
          end;
        finally
          Indices.Free;
        end;
        ////
        ///
        //if Uppercase(TabelaNome) = 'OSPRVSTA' then
          //Geral.MB_Info(TabelaNome);
        VerificaRegistrosObrigatorios_Inclui(Database, TabelaNome, TabelaBase,
          Memo, True, RecriaRegObrig, LaAviso1B, LaAviso2B, LaAviso1G,
          LaAviso2G);
        VerificaRegistrosDeArquivo(Database, TabelaNome, TabelaBase,
          NometabItemDel, Memo, RecriaRegObrig);
        if CRCTableManageToQeiLnkOn(TCRCTableManage(QeiLnkOn)) then
          VerificaRegistrosDBMQeiLnk(Database, TabelaNome, TabelaBase,
          Tem_Del, QeiLnkOn, NomeTabItemDel, Memo);
      finally
        FLCampos.Free;
        FLIndices.Free;
      end;
    finally
      Dmod.QrNTV.Close;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosObrigatorios_Inclui(DataBase: TmySQLDatabase;
  TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean;
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): TResultVerify;
var
  i, j, n, OK, z: Integer;
  ListaCampos, ListaValores: TStringList;
  Linha, Campo, Valor, Item, Virgula, LisSQL, ValSel, ValFld: String;
  Criar: Boolean;
begin
  Result := rvOK;
  try
    FListaSQL := TStringList.Create;
    try
      MyList.CriaListaSQL(TabelaNome, FListaSQL);
      MyList.CompletaListaSQL(TabelaNome, FListaSQL);
      //2015-03-16
      (*
      if TabelaNome <> TabelaBase then
        MyList.CompletaListaSQL(TabelaNome, FListaSQL);
      *)
//      if TabelaNome = 'cfop2003' then
        //Geral.MB_Info(TabelaNome);
      if TabelaNome <> TabelaBase then
        MyList.CompletaListaSQL(TabelaBase, FListaSQL);
      // FIM 2015-03-16
      if FListaSQL.Count > 1 then
      begin
        ListaCampos := TStringList.Create;
        try
          j := 0;
          Linha :=  FListaSQL[0];
          while j < (Length(Linha)) do
          begin
            OK := 0;
            Campo := '';
            while OK < 1 do
            begin
              j := j + 1;
              if j> Length(Linha) then Break;
              if Linha[j] = CO_APOSTROFO then OK := OK - 1
              //else if Linha[j] = ',' then OK := OK + 1
              else if Linha[j] = Char(124) (* separador = "|" *) then
                OK := OK + 1
              else
                Campo := Campo + Linha[j];
            end;
            ListaCampos.Add(Campo);
          end;
          //
          ListaValores := TStringList.Create;
          try
            // 2012-02-02
            ValSel := '';
            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) (* separador = "|" *) then
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              ValSel := ValSel + ',' + ListaValores[0];
            end;
            ValSel := Copy(ValSel, 2);
            Dmod.QrNTV.Close;
            Dmod.QrNTV.Database := Database;
            Dmod.QrNTV.SQL.Clear;
            Dmod.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
            Dmod.QrNTV.SQL.Add('WHERE ' + ListaCampos[0] + ' IN (' + ValSel + ')');
            try
              UMyMod.AbreQuery(Dmod.QrNTV, Database, 'TMyDBCheck.VerificaRegistrosObrigatorios_Inclui()');
            except
              GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo);
              raise;
            end;

            // Fim 2012-02-02

            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) (* separador = "|" *) then
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              Campo := ListaCampos[0];
              Valor := ListaValores[0];
              MyObjects.Informa2(LaAviso1G, LaAviso2G, True,
                'Verificando tabela "' + TabelaNome +
                '" Verificando registro obrigat�rio "' + Campo + '" > ' + Valor);
              Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);

              // 2012-02-02
              {
              Dmod.QrNTV.Close;
              Dmod.QrNTV.Database := Database;
              Dmod.QrNTV.SQL.Clear;
              Dmod.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
              Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
              try
                UMyMod.AbreQuery(Dmod.QrNTV, 'TMyDBCheck.VerificaRegistrosObrigatorios_Inclui()');
              except
                GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo);
                raise;
              end;
              //Criar := False;
              /
              if GOTOy.Registros(Dmod.QrNTV) <> 0 then
              }
              if Valor[1] = '"' then
                ValFld := Copy(Valor, 2, Length(Valor) - 2)
              else
                ValFld := Valor;
              if ValFld = '0000-00-00' then
                ValFld := '0';
              // ini 2020-10-13
              if (Dmod.QrNTV.RecordCount > 0) and
              // fim 2020-10-13
              (Dmod.QrNTV.Locate(Campo, Trim(ValFld), [loCaseInsensitive])) then
              // Fim 2012-02-02
              begin
                if (Recria and
                // Evitar destruir dados pois tabela tem
                // registro obrigatorio modificado p�s inclu�o
                (Uppercase(TabelaNome) <> Uppercase('controle')) and
                (Uppercase(TabelaNome) <> Uppercase('master')) and
                (Uppercase(TabelaNome) <> Uppercase('entidades')) and
                (Uppercase(TabelaNome) <> Uppercase('senhas')))
                //or ( (Uppercase(TabelaNome) = Uppercase('entidades')) and (Valor = '0'))
                then
                begin
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrNTV.Close;
                  Dmod.QrNTV.Database := Database;
                  Dmod.QrNTV.SQL.Clear;
                  Dmod.QrNTV.SQL.Add(DELETE_FROM + Lowercase(TabelaNome));
                  Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
                  Dmod.QrNTV.ExecSQL;
                  Criar := True;
                  GravaAviso(Item+'foi exclu�do para ser recriado.', Memo);
                end else
                  Criar := False;
              end else
                Criar := True;
              if Criar then
              begin
                if Avisa and (GOTOy.Registros(Dmod.QrNTV) = 0) then
                  GravaAviso(Item+'n�o existe.', Memo);
                try
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.Database := Database;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrSQL.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  if avisa then GravaAviso(Item+'inclu�do.', Memo);
                except
                  LisSQL := 'Lista de valores:' + sLineBreak;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + sLineBreak;
                  GravaAviso(LisSQL + Dmod.QrSQL.SQL.Text + sLineBreak + Item+ivMsgERROIncluir, Memo);
                  //raise;
                end;
              end;
            end;
          finally
            ListaValores.Free;
          end;
        finally
          ListaCampos.Free;
        end;
      end;
    finally
      FListaSQL.Free;
      MyObjects.Informa2(LaAviso1B, LaAviso2B, True, '');
      MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '');
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosObrigatorios_Corrige(DataBase: TmySQLDatabase;
  TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean): TResultVerify;
var
  i, j, n, OK, z: Integer;
  ListaCampos, ListaValores: TStringList;
  Linha, Campo, Valor, Item, Virgula, LisSQL: String;
  Criar: Boolean;
begin
  Result := rvOK;
  try
    FListaSQL := TStringList.Create;
    try
      MyList.CriaListaSQL(TabelaBase, FListaSQL);
      MyList.CompletaListaSQL(TabelaBase, FListaSQL);
      if FListaSQL.Count > 1 then
      begin
        ListaCampos := TStringList.Create;
        try
          j := 0;
          Linha :=  FListaSQL[0];
          while j < (Length(Linha)) do
          begin
            OK := 0;
            Campo := '';
            while OK < 1 do
            begin
              j := j + 1;
              if j> Length(Linha) then Break;
              if Linha[j] = CO_APOSTROFO then OK := OK - 1
              //else if Linha[j] = ',' then OK := OK + 1
              else if Linha[j] = Char(124) (* separador = "|" *) then
                OK := OK + 1
              else
                Campo := Campo + Linha[j];
            end;
            ListaCampos.Add(Campo);
          end;
          ListaValores := TStringList.Create;
          try
            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) (* separador = "|" *) then
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              Campo := ListaCampos[0];
              Valor := ListaValores[0];
              Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);
              Dmod.QrNTV.Close;
              Dmod.QrNTV.Database := Database;
              Dmod.QrNTV.SQL.Clear;
              Dmod.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
              Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
              try
                UMyMod.AbreQuery(Dmod.QrNTV, Database, 'TMyDBCheck.VerificaRegistrosObrigatorios_Corrige()');
              except
                GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo, False);
                raise;
              end;
              //Criar := False;
              if GOTOy.Registros(Dmod.QrNTV) <> 0 then
              begin
                if (Recria and
                // Evitar destruir dados pois tabela tem
                // registro obrigatorio modificado p�s inclu�o
                (Uppercase(TabelaNome) <> Uppercase('controle')) and
                (Uppercase(TabelaNome) <> Uppercase('master')) and
                (Uppercase(TabelaNome) <> Uppercase('entidades')) and
                (Uppercase(TabelaNome) <> Uppercase('senhas')))
                //or ( (Uppercase(TabelaNome) = Uppercase('entidades')) and (Valor = '0'))
                then
                begin
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrNTV.Close;
                  Dmod.QrNTV.Database := Database;
                  Dmod.QrNTV.SQL.Clear;
                  Dmod.QrNTV.SQL.Add(DELETE_FROM + Lowercase(TabelaNome));
                  Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
                  Dmod.QrNTV.ExecSQL;
                  Criar := True;
                  GravaAviso(Item+'foi exclu�do para ser recriado.', Memo, False);
                end else Criar := False;
              end else Criar := True;
              if Criar then
              begin
                if Avisa and (GOTOy.Registros(Dmod.QrNTV) = 0) then
                  GravaAviso(Item+'n�o existe.', Memo, False);
                try
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.Database := Database;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrSQL.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  if avisa then GravaAviso(Item+'inclu�do.', Memo, False);
                except
                  LisSQL := 'Lista de valores:' + sLineBreak;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + sLineBreak;
                  GravaAviso(LisSQL + Dmod.QrSQL.SQL.Text + sLineBreak + Item+ivMsgERROIncluir, Memo, False);
                  //raise;
                end;
              end else
              begin
                if Avisa and (GOTOy.Registros(Dmod.QrNTV) = 0) then
                  GravaAviso(Item+'ser� corrigido.', Memo, False);
                try
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.Database := Database;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add('UPDATE ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 1 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrSQL.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
                  Dmod.QrSQL.SQL.Add(sLineBreak + 'WHERE ' + ListaCampos[0]+'='+ListaValores[0]);
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  if avisa then GravaAviso(Item+'alterado.', Memo, False);
                except
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', nil, True, True);
                  LisSQL := 'Lista de valores:' + sLineBreak;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + sLineBreak;
                  GravaAviso(LisSQL + Dmod.QrSQL.SQL.Text + sLineBreak + Item+ivMsgERROIncluir, Memo, False);
                  //raise;
                end;
              end;
            end;
          finally
            ListaValores.Free;
          end;
        finally
          ListaCampos.Free;
        end;
      end;
    finally
      FListaSQL.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

{$IfNDef SemCashier}
(* 23/02/2015 => Aparentemente n�o usa
function TMyDBCheck.VerificaRegistrosDeUserSets(DataBase: TmySQLDatabase;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i, Posi: Integer;
  Item: String;
  Nome, Descricao: String;
begin
  Result := rvOK;
  try
    FUserSets := TStringList.Create;
    try
      MyList.CriaListaUserSets(FUserSets);
      if FUserSets.Count > 1 then
      begin
        Dmod.QrSQL.Close;
        Dmod.QrSQL.Database := Database;
        Dmod.QrSQL.SQL.Clear;
        Dmod.QrSQL.SQL.Add('UPDATE usersets SET Ativo=0');
        Dmod.QrSQL.ExecSQL;
        for i := 0 to FUserSets.Count-1 do
        begin
          //
          {
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM usersets');
          Dmod.QrNTV.SQL.Add('WHERE NomeForm=:P0 AND Definicao=:P1');
          }
          Posi := pos(';', FUserSets[i]);
          if Posi > 0 then
          begin
            Nome := Copy(FUserSets[i], 1, Posi-1);
            Descricao := Copy(FUserSets[i], Posi+1, Length(FUserSets[i])-Posi);
          end else begin
            Nome := FUserSets[i];
            Descricao := '';
          end;
          {
          Dmod.QrNTV.Params[0].AsString := Nome;
          Dmod.QrNTV.Params[1].AsString := Descricao;
          }
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrNTV, Database, [
          'SELECT * FROM usersets ',
          'WHERE NomeForm="' + Nome + '" ',
          'AND Definicao="' + Descricao + '" ',
          '']);
          try
            UnDmkDAC_PF.AbreQuery(Dmod.QrNTV, Database);
          except
            GravaAviso('N�o � poss�vel verificar se existe o campo "' + Nome +
            '" na tabela "UserSets"', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+
            '" n�o existe na tabela "UserSets".', Memo);
            try
              Dmod.QrSQL.Close;
              Dmod.QrSQL.Database := Database;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO usersets SET');
              Dmod.QrSQL.SQL.Add('NomeForm = :P0, Definicao=:P1');
              Dmod.QrSQL.Params[0].AsString := Nome;
              Dmod.QrSQL.Params[1].AsString := Descricao;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+'" foi inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end else begin
            Dmod.QrSQL.Close;
            Dmod.QrSQL.Database := Database;
            Dmod.QrSQL.SQL.Clear;
            Dmod.QrSQL.SQL.Add('UPDATE usersets SET Ativo=1');
            Dmod.QrSQL.SQL.Add('WHERE NomeForm = :P0 AND Definicao=:P1');
            Dmod.QrSQL.Params[0].AsString := Nome;
            Dmod.QrSQL.Params[1].AsString := Descricao;
            Dmod.QrSQL.ExecSQL;
          end;
        end;
        Dmod.QrSQL.Close;
        Dmod.QrSQL.Database := Database;
        Dmod.QrSQL.SQL.Clear;
        Dmod.QrSQL.SQL.Add(DELETE_FROM + ' usersets WHERE Ativo=0');
        Dmod.QrSQL.ExecSQL;
      end;
    finally
      FUserSets.Free;
    end;
  except
    raise;
  end;
end;
*)

(* 23/02/2015 => Aparentemente n�o usa
function TMyDBCheck.VerificaRegistrosDeCheque(DataBase: TmySQLDatabase;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
  function CriaListaCheque(FCheque: TStringList): Boolean;
  begin
    Result := True;
    try
      FCheque.Add('01;Valor');
      FCheque.Add('02;Extenso 1');
      FCheque.Add('03;Extenso 2');
      FCheque.Add('04;Favorecido');
      FCheque.Add('05;Dia');
      FCheque.Add('06;M�s');
      FCheque.Add('07;Ano');
      FCheque.Add('08;Bom Para');
      FCheque.Add('09;Observa��o');
      FCheque.Add('10;Cidade');
      //
    except
      raise;
      Result := False;
    end;
  end;
var
  i, Posi: Integer;
  Item: String;
  Nome, Descricao: String;
begin
  Result := rvOK;
  try
    FCheque := TStringList.Create;
    try
      CriaListaCheque(FCheque);
      if FCheque.Count > 1 then
      begin
        for i := 0 to FCheque.Count-1 do
        begin
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM chconfits');
          Dmod.QrNTV.SQL.Add('WHERE Campo=:P0 AND Nome=:P1');
          Posi := pos(';', FCheque[i]);
          if Posi > 0 then
          begin
            Nome := Copy(FCheque[i], 1, Posi-1);
            Descricao := Copy(FCheque[i], Posi+1, Length(FCheque[i])-Posi);
          end else begin
            Nome := FCheque[i];
            Descricao := '';
          end;
          Dmod.QrNTV.Params[0].AsString := Nome;
          Dmod.QrNTV.Params[1].AsString := Descricao;
          try
            UnDmkDAC_PF.AbreQuery(Dmod.QrNTV, Database);
          except
            GravaAviso('N�o � poss�vel verificar se existe o campo '+Nome+
            ' na tabela "chconfits"', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+
            '" n�o existe na tabela "chconfits".', Memo);
            try
              Dmod.QrSQL.Close;
              Dmod.QrSQL.Database := Database;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO chconfits SET');
              Dmod.QrSQL.SQL.Add('Campo = :P0, Nome=:P1');
              Dmod.QrSQL.Params[0].AsString := Nome;
              Dmod.QrSQL.Params[1].AsString := Descricao;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+'" foi inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end;
        end;
      end;
    finally
      FCheque.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;
*)

(* 23/02/2015 => Aparentemente n�o usa
function TMyDBCheck.VerificaRegistrosDeImpDOS(DataBase: TmySQLDatabase;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i, j, k, Tam: Integer;
  Item: String;
  //Posi: MyArrayI02;
  Codigo, Tabela, Descricao: String;
begin
  Result := rvOK;
  try
    FImpDOS := TStringList.Create;
    try
      MyList.CriaListaImpDOS(FImpDOS);
      if FImpDOS.Count > 1 then
      begin
        for i := 0 to FImpDOS.Count-1 do
        begin
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM impdosits');
          Dmod.QrNTV.SQL.Add('WHERE Codigo=:P0 AND Tabela=:P1 AND Descricao=:P2');
          //Posi1 := pos(';', FImpDOS[i]);
          Tam := Length(FImpDOS[i]);
          k := 1;
          Codigo    := '';
          Tabela    := '';
          Descricao := '';
          for j := 1 to Tam do
          begin
            if FImpDos[i][j] = ';' then k := k + 1
            else begin
              case k of
                1: Codigo    := Codigo    + FImpDos[i][j];
                2: Tabela    := Tabela    + FImpDos[i][j];
                3: Descricao := Descricao + FImpDos[i][j];
              end;
            end;
          end;
          Dmod.QrNTV.Params[0].AsString := Codigo;
          Dmod.QrNTV.Params[1].AsString := Tabela;
          Dmod.QrNTV.Params[2].AsString := Descricao;
          try
            UnDmkDAC_PF.AbreQuery(Dmod.QrNTV, Database);
          except
            GravaAviso('N�o � poss�vel verificar se existe o campo "'+Descricao+
            '" na tabela "impdosits"', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso('O campo "'+Descricao+
            '" n�o existe na tabela "ImpDOS".', Memo);
            try
              Dmod.QrSQL.Close;
              Dmod.QrSQL.Database := Database;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add(DELETE_FROM + ' impdosits ');
              Dmod.QrSQL.SQL.Add('WHERE Codigo = :P0');
              Dmod.QrSQL.Params[0].AsString := Codigo;
              Dmod.QrSQL.ExecSQL;
              //
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO impdosits SET');
              Dmod.QrSQL.SQL.Add('Codigo = :P0, Tabela=:P1, Descricao=:P2');
              Dmod.QrSQL.Params[0].AsString := Codigo;
              Dmod.QrSQL.Params[1].AsString := Tabela;
              Dmod.QrSQL.Params[2].AsString := Descricao;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso('O campo "'+Descricao+'" foi inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end;
        end;
      end;
    finally
      FImpDOS.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;
*)
{$EndIf}

function TMyDBCheck.VerificaRegistrosDBMQeiLnk(Database: TMySQLDatabase;
  TabelaNome, TabelaBase: String; Tem_Del: Boolean; QeiLnkOn: TCRCTableManage;
  NomeTabItemDel: String; Memo: TMemo): Boolean;
const
  sProcName = 'MyDBCheck.VerificaRegistrosDBMQeiLnk()';
var
  Qry1, Qry2: TmySQLQuery;
  //
  function RegistrosDBMQeiLnk(): Boolean;
  var
    KLAskrTab, KLAskrCol, KLRplyTab, KLRplyCol, KLMTbsCol, KLMTabDel: String;
    KLPurpose, TbPurpose, TbManage, AWServerID, AWStatSinc: Integer;
    SQLType: TSQLType;
  begin
    Result         := False;
    //
    SQLType        := stIns;
    KLAskrTab      := TabelaNome;
    KLAskrCol      := FRQeiLnk.AskrCol;
    KLRplyTab      := FRQeiLnk.RplyTab;
    KLRplyCol      := FRQeiLnk.RplyCol;
    KLPurpose      := Integer(FRQeiLnk.Purpose);
    KLMTbsCol      := FRQeiLnk.MTbsCol;
    KLMTabDel      := NomeTabItemDel;
    TbPurpose      := Integer(FRCampos.Purpose);
    TbManage       := Integer(QeiLnkOn);
    //
    if (FRCampos.Purpose in  CO_PrimaryIncremenPurpose) and (FRCampos.Key <> 'PRI') then
        GravaAviso(sProcName + ' ' + TabelaNome + '.' + FRCampos.Field + ' > ' +
        GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose)) +
        ' Purpose em CO_PrimaryIncremenPurpose mas FRCampos.Key <> "PRI"', Memo);
    //
    if (not (FRCampos.Purpose in  CO_PrimaryNoAndIncPurpose)) and (FRCampos.Key = 'PRI') then
        GravaAviso(sProcName + ' ' + TabelaNome + '.' + FRCampos.Field + ' > ' +
        GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose)) +
        ' FRCampos.Key = "PRI" mas Purpose n�o est� em CO_PrimaryNoAndIncPurpose', Memo);
    //

    if (FRCampos.Purpose <> FRQeiLnk.Purpose) then
    begin
      if Tem_Del
      and (Lowercase(TabelaNome) <> Lowercase(TabelaBase))
      and ((FRQeiLnk.Purpose = itpCDRDeleteSync) and (FRCampos.Purpose = itpCDRIncremSync))then
      begin
       // Tabela de exclus�o. Ex.: vsmovitz
      end else
      begin
        GravaAviso(sProcName + ' ' + TabelaNome + '.' + FRCampos.Field + ' > ' +
        GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose)) +
        ' Purpose difere de ' +
        GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRQeiLnk.Purpose)), Memo);
      end;
    end;
    //
    SQLType := stNil;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Database, [
    'SELECT KLPurpose, TbPurpose, TbManage, KLMTbsCol, KLMTabDel ',
    'FROM dbmqeilnk ',
    'WHERE KLAskrTab="' + KLAskrTab + '" ',
    'AND  KLAskrCol="' + KLAskrCol + '" ',
    'AND  KLRplyTab="' + KLRplyTab + '" ',
    'AND  KLRplyCol="' + KLRplyCol + '" ',
    '']);
    if Qry1.RecordCount = 0 then
      SQLType := stIns
    else
    if Qry1.RecordCount = 1 then
    begin
      if (Qry1.FieldByName('KLPurpose').AsInteger <> KLPurpose)
      or (Qry1.FieldByName('TbPurpose').AsInteger <> TbPurpose)
      or (Qry1.FieldByName('TbManage').AsInteger <> TbManage)
      or (Qry1.FieldByName('KLMTbsCol').AsString <> KLMTbsCol)
      or (Qry1.FieldByName('KLMTabDel').AsString <> KLMTabDel) then
        SQLType := stUpd
      else
        Result := True;
    end;
    //
    if SQLType <> stNil then
    begin
      // ? := UMyMod.BPGS1I32('dbmqeilnk', 'KLAskrTab', 'KLAskrCol', 'KLRplyTab', 'KLRplyCol', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'dbmqeilnk', False, [
      'KLMTbsCol', 'KLPurpose', 'TbPurpose',
      'TbManage', 'KLMTabDel'], [
      'KLAskrTab', 'KLAskrCol', 'KLRplyTab', 'KLRplyCol'], [
      KLMTbsCol, KLPurpose, TbPurpose,
      TbManage, KLMTabDel], [
      KLAskrTab, KLAskrCol, KLRplyTab, KLRplyCol], False);
      if Result then
        GravaAviso(sProcName + ' ' + TabelaNome + '.' + FRCampos.Field + ' > ' +
        GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose)) +
        ' inserido!', Memo);
    end;
  end;
var
  I, J, N: Integer;
  CampoAsk: String;
  PodeCriar, Feito, Excluir, Verificado: Boolean;
begin
  if FLQeiLnk.Count = 0 then
  begin
    if CO_SIGLA_APP = 'PLAN' then
      // 2019-01-14 desmarquei provisoriamente por causa do tx... no Planning!
    else
      GravaAviso(sProcName + ' ' + TabelaNome + ' Nenhum campo definido!', Memo);
    //
    Exit;
  end;
    //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
  Qry2 := TmySQLQuery.Create(Dmod);
  try
    //Geral.MB_Info(sProcName + sLineBreak + 'Tabela: ' + TabelaNome);
    //if LowerCase(TabelaNome) = 'vsmovitz' then
      //Geral.MB_Info(TabelaNome);
    N := 0;
    for I := 0 to FLCampos.Count -1 do
    begin
      Feito := False;
      Verificado := False;
      //
      FRCampos := FLCampos[I];
      PodeCriar := ItemTuplePurposeToDBMQeiLnk(FRCampos.Purpose, TabelaNome, FRCampos.Field);
      if PodeCriar then
      begin
        for J := 0 to FLQeiLnk.Count -1 do
        begin
          FRQeiLnk := FLQeiLnk[J];
          if (LowerCase(FRQeiLnk.AskrTab) = LowerCase(TabelaNome))
          and (LowerCase(FRQeiLnk.AskrCol) = LowerCase(FRCampos.Field)) then
          begin
            //Geral.MB_Info(FRQeiLnk.RplyTab + '.' + FRQeiLnk.RplyCol);
            Feito := RegistrosDBMQeiLnk();
            Verificado := True;
          end;
        end;
        if not Feito then
        begin
          if not Tem_Del then
            GravaAviso(sProcName + ' ' + TabelaNome + '.' + FRCampos.Field +
            ' > ' +
            GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose))
            + ' N�o inserido!', Memo)
          else Verificado := True;
        end;
      end else
      begin
        Feito := True;
        Verificado := True;
      end;
      if Feito then N := N + 1;
      if not Verificado then
        GravaAviso(sProcName + ' ' + TabelaNome + '.' + FRCampos.Field + ' > ' +
        GetEnumName(TypeInfo(TItemTuplePurpose), Integer(FRCampos.Purpose))
        + ' Campo n�o implementado!' + sLineBreak + TabelaNome, Memo);
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Database, [
    'SELECT KLAskrCol ',
    'FROM dbmqeilnk ',
    'WHERE KLAskrTab="' + TabelaNome + '" ',
    '']);
    Qry2.First;
    while not Qry2.Eof do
    begin
      Excluir := True;
      for I := 0 to FLCampos.Count -1 do
      begin
        FRCampos := FLCampos[I];
        CampoAsk := Qry2.FieldByName('KLAskrCol').AsString;
        if LowerCase(FRCampos.Field) = LowerCase(CampoAsk) then
        begin
          PodeCriar := ItemTuplePurposeToDBMQeiLnk(FRCampos.Purpose, TabelaNome, FRCampos.Field);
          Excluir := PodeCriar = False;
        end;
      end;
      if Excluir then
         GravaAviso('O registro ' + TabelaNome + '.' + CampoAsk +
         ' da tabela "DBMQeiLnk" devia ser exclu�do!', Memo);
      // Parei aqui
      //
      Qry2.Next;
    end;
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
  end;
end;

function TMyDBCheck.VerificaRegistrosDeArquivo(Database: TMySQLDatabase;
 TabelaNome, TabelaBase, NomeTabItemDel: String; Memo: TMemo; Avisa: Boolean):
 TResultVerify;
var
  i, j, n: Integer;
  ListaCampos, ListaValores: TStringList;
  Campo, Valor, Item, Virgula: String;
  F: TextFile;
  S: string;
  Name: String;
  Lista: TStringList;
  Campos, Registros: Integer;
begin
  Result := rvOK;
  Name := 'C:\Dermatek\Listas\'+TabelaNome+'.myl';
  if not FileExists(Name) then Exit;
  AssignFile(F, Name);
  Reset(F);
  Lista := TStringList.Create;
  while not Eof(F) do
  begin
    Readln(F, S);
    Lista.Add(S);
  end;
  CloseFile(F);
  if Uppercase(Lista[0]) <> '>>LISTA_TABELA_DERMATEK' then Exit;
  try
    Campos := 0;
    i := 1;
    ListaCampos := TStringList.Create;
    try
      ListaValores := TStringList.Create;
      try
        while not (Copy(Lista[i], 1,2) = '<<') do
        begin
          if Uppercase(Copy(Lista[i], 1,5)) = 'CAMPO' then
          begin
            Campos := Campos+1;
            ListaCampos.Add(Copy(Lista[i], 7, Length(Lista[i])-6));
          end;
          i := i + 1;
        end;
        for i := i+1 to Lista.Count-1 do ListaValores.Add(Lista[i]);
        Registros := Trunc(ListaValores.Count / Campos);
        //
        for j := 0 to Registros-1 do
        begin
          Campo := ListaCampos[0];
          Valor := ListaValores[j*Campos];
          Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM ' + lowercase(TabelaNome));
          Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ="'+Valor+'"');
          try
            UnDmkDAC_PF.AbreQuery(Dmod.QrNTV, Database);
          except
            GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso(Item+'n�o existe.', Memo);
            try
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
              for n := 0 to ListaCampos.Count-1 do
              begin
                if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                Dmod.QrSQL.SQL.Add(ListaCampos[n]+'="'+ListaValores[(j*Campos)+n]+ '"'+Virgula);
              end;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso(Item+'inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end;
        end;
        //
      finally
        ListaValores.Free;
      end;
    finally
      ListaCampos.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;
{ 2012-01-30

function TMyDBCheck.VerificaRegistrosDeJanelas(DataBase: TmySQLDatabase;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i: Integer;
begin
  try
    Result := rvErr;
    FLJanelas := TList<TJanelas>.Create;
    try
      MyList.CriaListaJanelas(FLJanelas);
      CashTb.CompletaListaFRJanelasCashier(FRJanelas, FLJanelas);
      for i:= 0 to FLJanelas.Count - 1 do
      begin
        FRJanelas := FLJanelas[i];

        Dmod.QrNTV.Close;
        Dmod.QrNTV.Database := Database;
        Dmod.QrNTV.SQL.Clear;
        Dmod.QrNTV.SQL.Add('SELECT Janela, Nome, Descricao');
        Dmod.QrNTV.SQL.Add('FROM perfjan');
        Dmod.QrNTV.SQL.Add('WHERE Janela=:P0');
        Dmod.QrNTV.Params[0].AsString := FRJanelas.ID;
        UnDmkDAC_PF.AbreQuery(Dmod.QrNTV, Database);
        if Dmod.QrNTV.RecordCount = 0 then
        begin
          GravaAviso('Ser� adicionado o campo "' +
            FRJanelas.ID + '" na tabela perfjan.', Memo);
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO perfjan SET ');
          Dmod.QrUpd.SQL.Add('Janela=:P0, Nome=:P1, Descricao=:P2');
          Dmod.QrUpd.Params[00].AsString := FRJanelas.ID;
          Dmod.QrUpd.Params[01].AsString := FRJanelas.Nome;
          Dmod.QrUpd.Params[02].AsString := FRJanelas.Descricao;
          Dmod.QrUpd.ExecSQL;
          GravaAviso('Registro "' + FRJanelas.ID + '" adicionado com sucesso.', Memo);
          Result := rvOk;
        end;
      end;
    finally
      FLJanelas.Free;
    end;
  except
    raise;
  end;
end;
}
function TMyDBCheck.VerificaRegistrosDeJanelas(DataBase: TmySQLDatabase;
  Memo: TMemo; Avisa: Boolean; LaAviso1G, LaAviso2G: TLabel;
  PB: TProgressBar): TResultVerify;
var
  i: Integer;
  Lista: WideString;
begin
  try
    Result := rvErr;
    FLJanelas := TList<TJanelas>.Create;
    try
      MyList.CriaListaJanelas(FLJanelas);
      // Mudar para MyListas >> Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas); !!!!!!!!
      //CashTb.CompletaListaFRJanelasCashier(FRJanelas, FLJanelas);
      Dmod.QrNTV.Close;
      Dmod.QrNTV.Database := Database;
      Dmod.QrNTV.SQL.Clear;
      Dmod.QrNTV.SQL.Add('SELECT Janela, Nome, Descricao, Modulo');
      Dmod.QrNTV.SQL.Add('FROM perfjan');
      //Dmod.QrNTV.SQL.Add('WHERE Janela=:P0');
      //Dmod.QrNTV.Params[0].AsString := FRJanelas.ID;
      UnDmkDAC_PF.AbreQuery(Dmod.QrNTV, Database);
      PB.Position := 0;
      PB.Max := FLJanelas.Count;
      for i:= 0 to FLJanelas.Count - 1 do
      begin
        FRJanelas := FLJanelas[i];
        //
        MyObjects.Informa2EUpdPB(PB, LaAviso1G, LaAviso2G, True, '"Janela" ' + FRJanelas.ID + ' > Verificando');
        // ini 2020-10-13
        if (Dmod.QrNTV.RecordCount = 0) or
        // fim 2020-10-13
        (not Dmod.QrNTV.Locate('Janela', FRJanelas.ID, [])) then
        begin
          MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"Janela" ' + FRJanelas.ID + ' > Incluindo');
          GravaAviso('Ser� adicionado o campo "' +
            FRJanelas.ID + '" na tabela perfjan.', Memo);
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO perfjan SET ');
          Dmod.QrUpd.SQL.Add('Janela=:P0, Nome=:P1, Descricao=:P2, Modulo=:P3');
          Dmod.QrUpd.Params[00].AsString := FRJanelas.ID;
          Dmod.QrUpd.Params[01].AsString := FRJanelas.Nome;
          Dmod.QrUpd.Params[02].AsString := FRJanelas.Descricao;
          Dmod.QrUpd.Params[03].AsString := FRJanelas.Modulo;
          Dmod.QrUpd.ExecSQL;
          GravaAviso('Registro "' + FRJanelas.ID + '" adicionado com sucesso.', Memo);
          Result := rvOk;
        end else
        begin
          if (FRJanelas.Modulo <> '') and
            (Dmod.QrNTV.FieldByName('Modulo').AsString = '') then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE perfjan SET ');
            Dmod.QrUpd.SQL.Add('Modulo=:P0 WHERE Janela=:P1');
            Dmod.QrUpd.Params[00].AsString := FRJanelas.Modulo;
            Dmod.QrUpd.Params[01].AsString := FRJanelas.ID;
            Dmod.QrUpd.ExecSQL;
            GravaAviso('Registro "' + FRJanelas.ID + '" atualizado com sucesso.', Memo);
            Result := rvOk;
          end;
        end;
        if i = 0 then
          Lista := '"' + FRJanelas.ID + '"'
        else
          Lista := Lista + ', ' + '"' + FRJanelas.ID + '"';
      end;
      if Lista <> '' then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add(DELETE_FROM + ' perfjan WHERE Janela NOT IN (' + Lista + ')');
        Dmod.QrUpd.ExecSQL;
      end;
    finally
      FLJanelas.Free;
    end;
  except
    raise;
  end;
end;
// Fim 2012-01-30

function TMyDBCheck.ExcluiIndice(Database: TMySQLDatabase; Tabela, IdxNome,
  Aviso: String; Motivo: TMyMotivo; Memo: TMemo; ClIdxsNoNeed: TCheckListBox):
  TResultVerify;
var
  Resp: Integer;
  Txt: String;
begin
  //Result := rvErr;
  if MyList.ExcluiIdx then
  begin
    case Motivo of
      mymotDifere: Txt := Format('O �ndice %s da Tabela %s difere do esperado e deve ser excluido.', [IdxNome, Tabela]);
      mymotSemRef: Txt := Format('N�o h� refer�ncia ao �ndice %s da Tabela %s, que deve ser excluido.', [IdxNome, Tabela]);
      mymotUserDel: Txt := Format('Exclus�o do �ndice %s da Tabela %s, por escolha do usu�rio.', [IdxNome, Tabela]);
    end;
{
    if not FPergunta then Resp := ID_YES else
}
    if not FPergunta then Resp := ID_NO else
    Resp := Geral.MB_Pergunta(Txt+sLineBreak+
            'Confirma a exclus�o do �ndice ?');
    if Resp = ID_YES then
    begin
      try
        Dmod.QrSQL.Close;
        Dmod.QrSQL.DataBase := DataBase;
        Dmod.QrSQL.SQL.Clear;
        if Uppercase(IdxNome) = Uppercase('PRIMARY') then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY')
        else
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome);
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
        Dmod.QrSQL.ExecSQL;
//        Result := rvOK;
        GravaAviso(Aviso+ivMsgExcluido, Memo);
      except;
        //Result := rvErr;
        GravaAviso(Aviso+ivMsgERROExcluir, Memo);
        raise;
      end;
    end else begin
      GravaAviso(Aviso+ivAbortExclUser, Memo);
      if Resp = ID_CANCEL then
      begin
        Result := rvAbort;
        Exit;
      end;
    end;
    Result := rvOK;
    GravaAviso('O �ndice "' + Tabela + '.' + IdxNome + ' deveria ser exclu�do!', Memo);
    AdicionaIdxNoNeed(ClIdxsNoNeed, Tabela, IdxNome);
    //
  end else Result := rvOK;
end;

function TMyDBCheck.ExcluiRegistro(QrExecSQL, QrDados: TmySQLQuery;
TabelaTarget: String; CamposSource, CamposTarget: array of String;
ReopenQrDados: Boolean; Pergunta: String): Boolean;
var
  Continua, i, j: Integer;
  Valor: String;
begin
  Result := False;
  if USQLDB.ImpedeExclusaoPeloNomeDaTabela(TabelaTarget) then
    Exit;
  if Trim(Pergunta) = '' then
    Continua := ID_YES
  else
    Continua := Geral.MB_Pergunta(Pergunta);
  //
  if Continua = ID_YES then
  begin
    QrExecSQL.SQL.Clear;
    QrExecSQL.SQL.Add(DELETE_FROM  + Lowercase(TabelaTarget) + ' WHERE ');
    //
    j := High(CamposSource);
    for i := Low(CamposSource) to j do
    begin
      Valor := Geral.VariavelToString(QrDados.FieldByName(CamposSource[i]).Value);
      if i < j then
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor + ' AND ')
      else
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor);
    end;
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
      QrExecSQL.ExecSQL;
      Result := True;
      if ReopenQrDados then
      begin
        QrDados.Close;
        UnDmkDAC_PF.AbreQuery(QrDados, Dmod.MyDB);
      end;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
    end;
  end;
end;

function TMyDBCheck.CriaIndice(Database: TmySQLDatabase; Tabela, IdxNome, Aviso: String; Memo: TMemo):
  TResultVerify;
var
  i, j, K: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
begin
  Result := rvOK;
  if not FPergunta then Resp := ID_YES else
  Resp := Geral.MB_Pergunta('Confirma a cria��o do �ndice '+IdxNome+
  ' da tabela '+Tabela+'?');
  if Resp = ID_YES then
  begin
    //
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for i := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[i];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      K := Geral.IMV(Conta[Conta.Count-1])-1;
      if K = -1 then
        GravaAviso('FRIndices.Seq_in_index inv�lido (0)!' +
        'ERRO na cria��o do �ndice ' + IdxNome + ' na tabela '+ Tabela, Memo);
      for j := 0 to K do
      begin
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      Dmod.QrSQL.Close;
      Dmod.QrSQL.Database := DataBase;
      Dmod.QrSQL.SQL.Clear;
      // Verificar se n�o haveria viola��o de �ndice!!!
      Dmod.QrSQL.Close;
      Dmod.QrSQL.SQL.Clear;
      Dmod.QrSQL.SQL.Add('SELECT COUNT(*) _ITENS_ FROM ' + lowercase(Tabela));
      Dmod.QrSQL.SQL.Add('GROUP BY ' + Campos);
      Dmod.QrSQL.SQL.Add('ORDER BY _ITENS_ DESC');
      UnDmkDAC_PF.AbreQuery(Dmod.QrSQL, DataBase);
      if (Dmod.QrSQL.RecordCount > 0) and
      (Dmod.QrSQL.FieldByName('_ITENS_').AsFloat > 1) then
      begin
        GravaAviso('O �ndice "' + IdxNome + '" n�o foi inclu�do na tabela "' +
        Tabela + '" pois j� seria criado com viola��o!', Memo);
        {
        Geral.MB_Aviso('O �ndice "' + IdxNome +
        '" n�o foi inclu�do na tabela "' + Tabela +
        '" pois j� seria criado com viola��o!');
        }
      end else begin
        Dmod.QrSQL.Close;
        Dmod.QrSQL.SQL.Clear;
        //
        if Uppercase(IdxNome) = Uppercase('PRIMARY') then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' ADD PRIMARY KEY ('+Campos+')')
        else
        if Uppercase(Copy(IdxNome, 1, 6)) = 'UNIQUE' then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' ADD UNIQUE '+IdxNome+' ('+Campos+')')
        else
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' Add INDEX '+IdxNome+' ('+Campos+')');
         UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
        Dmod.QrSQL.ExecSQL;
        Result := rvOK;
        GravaAviso(Aviso+ivCriado, Memo);
      end;
      Dmod.QrSQL.Close;
    except;
      UMyMod.MostraSQL(Dmod.QrSQL, Memo, 'ERRO na cria��o de �ndice na tabela '+Tabela);
      Conta.Free;
      //Result := rvErr;
      GravaAviso(Aviso+ivMsgERROCriar, Memo);
      raise;
    end;
  end else begin
    GravaAviso(Aviso+ivAbortInclUser, Memo);
    if Resp = ID_CANCEL then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TMyDBCheck.RecriaIndice(Database: TmySQLDatabase; Tabela, IdxNome,
 Aviso: String; Memo: TMemo): TResultVerify;
var
  i, j: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
begin
  Result := rvOK;
  if not FPergunta then Resp := ID_YES else
  Resp := Geral.MB_Pergunta('Confirma a alter��o do �ndice '+IdxNome+
  ' da tabela '+Tabela+'?');
  if Resp = ID_YES then
  begin
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for i := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[i];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      for j := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
      begin
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      Dmod.QrSQL.Close;
      Dmod.QrSQL.Database := DataBase;
      Dmod.QrSQL.SQL.Clear;
      if Uppercase(IdxNome) = Uppercase('PRIMARY') then
      begin
        //Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY, ADD PRIMARY KEY ('+Campos+')')
        // ini 2022 21 05
        if (Lowercase(Tabela) = 'fisregcfop')
        or (Lowercase(Tabela) = 'fisregufs')
        or (Lowercase(Tabela) = 'fisregufx')
        then
        begin
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY');
          try
            Dmod.QrSQL.ExecSQL;
          except
            GravaAviso(Aviso + sLineBreak + Dmod.QrSQL.SQL.Text, Memo);
          end;
          Dmod.QrSQL.SQL.Clear;
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' ADD PRIMARY KEY ('+Campos+')');
        end else
        begin
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY, ADD PRIMARY KEY ('+Campos+')')
        end;
        // fim 2022 21 05
      end
      else
      if Uppercase(Copy(IdxNome, 1, 6)) = Uppercase('UNIQUE') then
        Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome+', ADD UNIQUE '+IdxNome+' ('+Campos+')')
      else
        Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome+', ADD INDEX '+IdxNome+' ('+Campos+')');
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
      try
        Dmod.QrSQL.ExecSQL;
      except
        GravaAviso(Aviso + sLineBreak + Dmod.QrSQL.SQL.Text, Memo);
      end;
      Result := rvOK;
      GravaAviso(Aviso+ivAlterado, Memo);
    except;
      ConsertaTabela(Tabela);
      Conta.Free;
      //Result := rvErr;
      GravaAviso(Aviso+ivMsgERROAlterar, Memo);
      raise;
    end;
  end else begin
    GravaAviso(Aviso+ivAbortAlteUser, Memo);
    if Resp = ID_CANCEL then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TMyDBCheck.TabelaAtiva(Tabela: String): Boolean;
var
  //p,
  i: Integer;
  //Tab: String;
  DefTabela: TTabelas;
begin
  Result := False;
  try
{
    for i := 0 to FTabelas.Count -1 do
    begin
      //Tab := FTabelas[i];
      p := pos(';', FTabelas[i]);
      if p = 0 then
        Tab := FTabelas[i]
      else
        Tab := Copy(FTabelas[i], 1, p - 1);
      if Uppercase(Tab) = Uppercase(Tabela) then
      begin
        //if Uppercase(Tabela) = 'CONTROLE' then ShowMessage('Achou');
        Result := True;
        Exit;
      end;
    end;
}
    for i := 0 to FTabelas.Count -1 do
    begin
      DefTabela := FTabelas[i];
      if Uppercase(DefTabela.TabCria) = Uppercase(Tabela) then
      begin
        Result := True;
        Exit;
      end;
    end;
  except
    raise;
  end;
end;

function TMyDBCheck.TabelaExiste(Tabela: String): Boolean;
var
  Tab: String;
  P: Integer;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta fun��o mudar na WEB em:                        ///
  ///  helpers -> dmk_db_helper.php -> tabela_existe                        ///
  /////////////////////////////////////////////////////////////////////////////

  Result := False;
  try
    Dmod.QrMas.First;
    P := pos('.', Tabela);
    if P > 0 then
      Tab := Copy(Tabela, P + 1)
    else
      Tab := Tabela;
    while not Dmod.QrMas.Eof do
    begin
      if Uppercase(Tab) = Uppercase(Dmod.QrMas.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      Dmod.QrMas.Next;
    end;
  except
    raise;
  end;
end;

function TMyDBCheck.ImpedeVerificacao(): Boolean;
begin
  {$WARNINGS OFF}
  //////////////////////////////////////////////////////////////////////////////
  // Impedir Verifica��o em vers�es novas                                     //
  //////////////////////////////////////////////////////////////////////////////
  if CO_VERSAO mod 10000 = 9999 then
  begin
    VAR_VERIFI_DB_CANCEL := True;
    Result := True;
    Geral.MB_Aviso('Esta � uma vers�o provis�ria.' + sLineBreak +
    'N�o ser� feita nenhuma verifica��o no banco de dados.' + sLineBreak +
    'Favor reportar qualquer tipo de erro que venha a ocorrer imediatamente ' +
    '� DERMATEK, para ser solucionado antes da vers�o est�vel.');
    Exit;
  end else Result := False;
  //////////////////////////////////////////////////////////////////////////////
  // FIM  Impedir Verifica��o em vers�es novas                                //
  //////////////////////////////////////////////////////////////////////////////
  {$WARNINGS ON}
end;

function TMyDBCheck.IndiceExiste(Database: TmySQLDatabase; Tabela, Indice: String;
 Memo: TMemo): Integer;
var
  i: Integer;
  Need, Find, Have: Integer;
  Item: String;
begin
  Item := Format(ivCampo_Nome, [Tabela, Indice])+mycoEspaco + 'N�o encontrado:';
  Result := 0;
  Need := 0;
  Find := 0;
  Have := 0;
  Dmod.QrIdx.Close;
  Dmod.QrIdx.Database := DataBase;
  Dmod.QrIdx.SQL.Clear;
  Dmod.QrIdx.SQL.Add('SHOW KEYS FROM ' + Lowercase(Tabela));
  UnDmkDAC_PF.AbreQuery(Dmod.QrIdx, DataBase);
  while not Dmod.QrIdx.Eof do
  begin
    if Uppercase(Dmod.QrIdx.FieldByName('Key_name').AsString) =
    Uppercase(Indice) then Have := Have + 1;
    Dmod.QrIdx.Next;
  end;
  try
    for i := 0 to FLIndices.Count -1 do
    begin
      FRIndices := FLIndices.Items[i];
      if Uppercase(FRIndices.Key_name) = Uppercase(Indice) then
      begin
        Dmod.QrIdx.First;
        Need := Need + 1;
        while not Dmod.QrIdx.Eof do
        begin
          if (Uppercase(FRIndices.Key_name)  =
              Uppercase(Dmod.QrIdx.FieldByName('Key_name').AsString)) then
          begin
            if Result = 0 then Result := 1;
            if (FRIndices.Non_unique  =
               Dmod.QrIdx.FieldByName('Non_unique').AsInteger)
            and (FRIndices.Seq_in_index  =
               Dmod.QrIdx.FieldByName('Seq_in_index').AsInteger)
            and (Uppercase(FRIndices.Column_name)  =
               Uppercase(Dmod.QrIdx.FieldByName('Column_name').AsString))
            then Find := Find + 1;
          end;
          Dmod.QrIdx.Next;
        end;
        if Find < Need then
        begin
          if Find > 0 then
          begin
            GravaAviso('A coluna '+FRIndices.Column_name+
            ' n�o foi encontrada no �ndice '+Indice+'.', Memo);
            Result := 2;
          end else GravaAviso('O �ndice '+Indice+' n�o existe.', Memo);
        end;
      end;
      //if Result = 2 then Exit;
    end;
  except
    Dmod.QrIdx.Close;
    raise;
  end;
  if Have > Find then
  begin
    GravaAviso('H� excesso de colunas no �ndice '+Indice+' da tabela '+Tabela+
    '.', Memo);
    MyList.ModificaDadosDeIndicesAlterados(Indice, Tabela, Database, Memo);
    Result := 3;
  end;
end;

function TMyDBCheck.ItemTuplePurposeToDBMQeiLnk(
  ItemTuplePurpose: TItemTuplePurpose; TabelaNome, CampoNome: String): Boolean;
const
  sProcName = 'MyDBCheck.ItemTuplePurposeToDBMQeiLnk()';
begin
  Result := False;
  case ItemTuplePurpose of
    //(*0*)TItemTuplePurpose.itpIndef
    (*01*)TItemTuplePurpose.itpCDRIncremSync,
    (*04*)TItemTuplePurpose.itpCDRRelatnSync,
    (*08*)TItemTuplePurpose.itpAllSrvrIncUniq,
    (*09*)TItemTuplePurpose.itpERPSrvrIncOver,
    (*10*)TItemTuplePurpose.itpCDRRelSncOrfao,
    (*14*)TItemTuplePurpose.itpCDRRelSncMulTab,
    (*16*)TItemTuplePurpose.itpERPPriNoIncRelatSync,
    (*17*)TItemTuplePurpose.itpERPPriNoIncNoRelSync:
    begin
      Result := True;
    end;
    //
    (*02*)TItemTuplePurpose.itpERPRelatnSync,
    (*03*)TItemTuplePurpose.itpSysRelatnPrDf,
    (*05*)TItemTuplePurpose.itpUsrPrimtivData,
    (*06*)TItemTuplePurpose.itpSysOrCalcData,
    (*07*)TItemTuplePurpose.itpERPSync,
    (*11*)TItemTuplePurpose.itpUsrOrErpPrimtiv,
    (*12*)TItemTuplePurpose.itpDeprecado,
    (*13*)TItemTuplePurpose.itpInutilizado:
    begin
      //Nada!!
    end;
    //
    else
      Geral.MB_Erro(sProcName + slineBreak +
      GetEnumName(TypeInfo(TItemTuplePurpose), Integer(ItemTuplePurpose)) +
      ' N�o implementado!' + slineBreak + 'Tabela: ' + TabelaNome + sLineBreak +
      'Campo: ' + CampoNome);
  end;
end;

procedure TMyDBCheck.CadastraItemESetaCodigo(InstanceClass: TComponentClass;
  var Reference; ModoAcesso: TAcessFmModo; dmkEdCB: TdmkEditCB; dmkCB:
  TdmkDBLookupComboBox; Qry: TmySQLQuery);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso) then
  begin
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(dmkEdCB, dmkCB, Qry, VAR_CADASTRO);
  end;
end;

function TMyDBCheck.CampoAtivo(Database: TmySQLDatabase; TabelaNome, TabelaBase,
  Campo: String; Tem_Del: Boolean; Memo: TMemo): Integer;
var
  i: Integer;
  //Change: Boolean;
  Item: String;
  Continua: Boolean;
  FRCampos_Null, NTV_Null: String;
  // 2022-03-01
  SemBarrasDuplas, DefaultNoField: String;
begin
  Item := Format(ivCampo_Nome, [TabelaNome, Campo])+mycoEspaco + ivMsgDiferenca;
  Result := 9;
  //Change := False;
  try
    for i := 0 to FLCampos.Count -1 do
    begin
      VAR_CONTA_FIELD_IN_DB := VAR_CONTA_FIELD_IN_DB + 1;
      //
      FRCampos := FLCampos.Items[i];
      if Uppercase(FRCampos.Field) = Uppercase(Campo) then
      begin
        //if Uppercase(Campo) = Uppercase('Geral_PathSchemas') then
          //Geral.MB_Teste(Uppercase(FRCampos.Field));
        Result := 0;
        if Uppercase(FRCampos.Tipo)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Type').AsString) then
        begin
          Result := 1;
          GravaAviso(Item+mycoTypeDataType, Memo);
        end;
        // ini 2022-01-21
        (*if Uppercase(FRCampos.Null)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Null').AsString) then*)
        Continua := True;
        //try
          FRCampos_Null := Uppercase(FRCampos.Null);
          NTV_Null      := Uppercase(Dmod.QrNTV.FieldByName('Null').AsString);
        (*except
          //on E: Exception do
            //Geral.MB_Erro(E.Message);
        end;*)
        if FRCampos_Null <> NTV_Null then
        // fim 2022-01-21
        begin
          // mudou no DELPHI BERLIN
          if (FRCampos.Default <> '') and (FRCampos.Null <> 'NO') then
          begin
            //GravaAviso(Item + 'Campo "NULL" com "DEFAULT" <> ""', Memo);
            VAR_CONTA_ERRO_NULL_DEFAULT := VAR_CONTA_ERRO_NULL_DEFAULT + 1;
          end else
          begin
            Result := 1;
            GravaAviso(Item+'"NULL"', Memo);
          end;
 (*  //      DELPHI BERLIN
          if (Uppercase(FRCampos.Null) = '') and
          (Uppercase(Dmod.QrNTV.FieldByName('Null').AsString) = 'NO') then
          begin
            // N�o faz nada
            // mudou no mySQL 5.0 de '' para 'NO'
            //
          end else begin
            Result := 1;
            GravaAviso(Item+'"NULL"', Memo);
          end;
//
          if (Uppercase(FRCampos.Null) = '') and
          (Uppercase(Dmod.QrNTV.FieldByName('Null').AsString) = 'YES') then
          begin
            // mudou no DELPHI BERLIN
            Result := 1;
            GravaAviso(Item+'"NULL"', Memo);
            //
          end else begin
            // mudou no DELPHI BERLIN
          //
          end;
        end;
//        Fim DELPHI BERLIN
*)
        end else
        begin
          //GravaAviso(Item+'"NULL" >>>>>>>> OK !!!', Memo);
        end;
////////////////////////////////////////////////////////////////////////////////
///     Remover!???
        if VAR_VERIFICA_BD_FIELD_DEFAULT_VALUE then
        begin
          try
            // ini 2022-02-21
            if Dmod.QrNTV.FieldByName('Default').AsVariant <> null then
            begin
            // fim 2022-02-21
              if Uppercase(FRCampos.Default) <>
                 Uppercase(String(Dmod.QrNTV.FieldByName('Default').AsVariant)) then
              begin
                // ini 2022-03-01
                SemBarrasDuplas := DmkPF.SimplificaBarras(Uppercase(FRCampos.Default));
                DefaultNoField := Uppercase(String(Dmod.QrNTV.FieldByName('Default').AsVariant));
                if SemBarrasDuplas <> DefaultNoField then
                begin
                // fim 2022-03-01
                  //Geral.MB_Teste('"' + SemBarrasDuplas + '"' + sLineBreak + sLineBreak +'� diferente de: ' + sLineBreak + sLineBreak + '"' + DefaultNoField + '"');
                  Result := 1;
                  GravaAviso(Item+'"DEFAULT"', Memo);
                end; // 2022-03-01
              end;
            // ini 2022-02-21
            end;
            // fim 2022-02-21
          except
            on E: Exception do
            begin
              GravaAviso(Item+'"DEFAULT"  >> ERRO: ' + TabelaNome + '>> ' + TabelaBase + '.' + Campo, Memo);
              Dmod.QrNTV.SQL.Text;
              GravaAviso(Item+'"ERRO" >> ' + Dmod.QrNTV.FieldByName('Default').AsString, Memo);
              GravaAviso(Item+'"ERRO" >> ' + FRCampos.Default, Memo);
              Geral.MB_ERRO(E.Message);
            end;
          end;
        end;
////////////////////////////////////////////////////////////////////////////////

{        try
            //GravaAviso(Item + '"DEFAULT"  >> OK: ' + TabelaNome + '>> ' + TabelaBase + '.' + Campo, Memo);
          if Uppercase(FRCampos.Default) <>
             Uppercase(Dmod.QrNTV.FieldByName('Default').AsString) then
          begin
            Result := 1;
            GravaAviso(Item+'"DEFAULT"', Memo);
          end;
        except
          on E: Exception do
          begin
            GravaAviso(Item+'"DEFAULT"  >> ERRO: ' + TabelaNome + '>> ' + TabelaBase + '.' + Campo, Memo);
            Dmod.QrNTV.SQL.Text;
            GravaAviso(Item+'"ERRO" >> ' + Dmod.QrNTV.FieldByName('Default').AsString, Memo);
            GravaAviso(Item+'"ERRO" >> ' + FRCampos.Default, Memo);
            Geral.MB_ERRO(E.Message);
          end;
        end;
}        if Uppercase(FRCampos.Extra)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Extra').AsString) then
        begin
          Result := 1;
          GravaAviso(Item+'"EXTRA"', Memo);
        end;
        if Uppercase(FRCampos.Key)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Key').AsString) then
        begin
          //if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          if Tem_Del = False then
          begin
            if Result = 9 then Result := 3;
            GravaAviso(Item+'"KEY"', Memo);
          end;
        end;
      end;
      if Result < 9 then Exit;
    end;
  except
    raise;
  end;
end;

function TMyDBCheck.CampoExiste(Tabela, Campo: String; Memo: TMemo): Integer;
var
  Item: String;
begin
  Item := Format(ivCampo_Nome, [Tabela, Campo])+mycoEspaco+ivMsgNaoExiste;
  Result := 2;
  try
    Dmod.QrNTV.First;
    while not Dmod.QrNTV.Eof do
    begin
      if Uppercase(Dmod.QrNTV.FieldByName('Field').AsString) =
         Uppercase(FRCampos.Field) then Result := 0;
      if Result < 2 then Exit;
      Dmod.QrNTV.Next;
    end;
    if Result = 2 then
      GravaAviso(Item, Memo);
  except
    raise;
  end;
end;

procedure TMyDBCheck.CarregaIndicesCDR(FLIndices: TList<TIndices>);
var
  I, P, U1, U2: Integer;
begin
  //if Idx_CDR then
  begin
    P := 0;
    U1 := 0;
    U2 := 0;
    for I := 0 to FLIndices.Count - 1 do
    begin
      FRIndices := FLIndices.Items[I];
      if Uppercase(FRIndices.Key_name) = 'PRIMARY' then
        P := P + 1
      else
      if Uppercase(FRIndices.Key_name) = 'UNIQUE1' then
        U1 := U1 + 1
      else
      if Uppercase(FRIndices.Key_name) = 'UNIQUE2' then
        U2 := U2 + 1
    end;
    if P > 0 then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := P + 1;
      FRIndices.Column_name   := 'AWServerID';
      FLIndices.Add(FRIndices);
    end;
    if U1 > 0 then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := U1 + 1;
      FRIndices.Column_name   := 'AWServerID';
      FLIndices.Add(FRIndices);
    end;
    if U2 > 0 then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE2';
      FRIndices.Seq_in_index  := U2 + 1;
      FRIndices.Column_name   := 'AWServerID';
      FLIndices.Add(FRIndices);
    end;
  end;
end;

function TMyDBCheck.ConfiguraOrdemCamposTabela(CamposTxt_Opc,
  CamposFields_Opc: array of String; CamposTxt_Def, CamposFields_Def,
  Separador: String; var CamposTxt_Sel, CamposFields_Sel: String): Boolean;
var
  i: Integer;
  DescriSel, TableSel, DescriSelNew, TableSelNew: TStringList;
  CamSel_Descri, CamSel_Fields, CamSel_DescriNew, CamSel_FieldsNew, Texto, TxtDesc, TxtAsc: String;
begin
  Result := False;
  //
  if Length(CamposTxt_Opc) <> Length(CamposFields_Opc) then
  begin
    Geral.MB_Aviso('A quantidade de campos e de descri��es deve ser a mesama!');
    Exit;
  end;
  if MyObjects.FIC(CamposTxt_Def = '', nil, 'Valor padr�o para ordena��o n�o definido!') then Exit;
  if MyObjects.FIC(CamposFields_Def = '', nil, 'Valor padr�o para ordena��o n�o definido!') then Exit;
  //
  try
    CamSel_Descri := CamposTxt_Sel;
    CamSel_Fields := CamposFields_Sel;
    //
    if CamSel_Descri = '' then
      CamSel_Descri := CamposTxt_Def;
    if CamSel_Fields = '' then
      CamSel_Fields := CamposFields_Def;
    //
    CamSel_FieldsNew := StringReplace(CamSel_Fields, 'DESC', '', [rfReplaceAll, rfIgnoreCase]);
    CamSel_FieldsNew := StringReplace(CamSel_FieldsNew, 'ASC', '', [rfReplaceAll, rfIgnoreCase]);
    CamSel_FieldsNew := Trim(CamSel_FieldsNew);
    //
    CamSel_DescriNew := StringReplace(CamSel_Descri, '(Decrescente)', '', [rfReplaceAll, rfIgnoreCase]);
    CamSel_DescriNew := StringReplace(CamSel_DescriNew, '(Crescente)', '', [rfReplaceAll, rfIgnoreCase]);
    CamSel_DescriNew := Trim(CamSel_DescriNew);
    //
    DescriSel    := TStringList.Create;
    TableSel     := TStringList.Create;
    DescriSelNew := TStringList.Create;
    TableSelNew  := TStringList.Create;
    //
    DescriSel    := Geral.Explode(CamSel_Descri, Separador, 1);
    TableSel     := Geral.Explode(CamSel_Fields, Separador, 1);
    DescriSelNew := Geral.Explode(CamSel_DescriNew, Separador, 1);
    TableSelNew  := Geral.Explode(CamSel_FieldsNew, Separador, 1);
    //
    Application.CreateForm(TFmOrderFieldsImp, FmOrderFieldsImp);
    //
    FmOrderFieldsImp.LBFieldsDisTab.Items.Clear;
    for i := Low(CamposFields_Opc) to High(CamposFields_Opc) do
    begin
      Texto := CamposFields_Opc[i];
      //
      if TableSelNew.IndexOf(Texto) = -1 then
        FmOrderFieldsImp.LBFieldsDisTab.Items.Add(Texto)
    end;

    FmOrderFieldsImp.LBFieldsDisTxt.Items.Clear;
    for i := Low(CamposTxt_Opc) to High(CamposTxt_Opc) do
    begin
      Texto := CamposTxt_Opc[i];
      //
      if DescriSelNew.IndexOf(Texto) = -1 then
        FmOrderFieldsImp.LBFieldsDisTxt.Items.Add(Texto)
    end;

    FmOrderFieldsImp.LBFieldsOrdTab.Items.Clear;
    for i := 0 to TableSel.Count - 1 do
      FmOrderFieldsImp.LBFieldsOrdTab.Items.Add(TableSel[i]);

    FmOrderFieldsImp.LBFieldsOrdTxt.Items.Clear;
    for i := 0 to DescriSel.Count - 1 do
      FmOrderFieldsImp.LBFieldsOrdTxt.Items.Add(DescriSel[i]);

    FmOrderFieldsImp.FSeparador := Separador;

    FmOrderFieldsImp.ShowModal;
    //
    CamposTxt_Sel    := FmOrderFieldsImp.FOrdTxt_Sel;
    CamposFields_Sel := FmOrderFieldsImp.FOrdFld_Sel;
    //
    FmOrderFieldsImp.Destroy;
  finally
    DescriSel.Free;
    TableSel.Free;
  end;
  //
  Result := (CamposTxt_Sel <> '') and (CamposFields_Sel <> '');
end;

procedure TMyDBCheck.ConsertaTabela(Tabela: String);
var
  ID: Integer;
  TbUper: String;
begin
  TbUper := Uppercase(Tabela);
  if ((TbUper = 'STQMOVITSA') or (TbUper = 'STQMOVITSB')) then
  begin
    DmodG.QrAux.Close;
    DmodG.QrAux.SQL.Clear;
    DmodG.QrAux.SQL.Add('SELECT Tipo, OriCodi, OriCtrl');
    DmodG.QrAux.SQL.Add('FROM ' + Lowercase(Tabela));
    DmodG.QrAux.SQL.Add('WHERE IDCtrl=0');
    UnDmkDAC_PF.AbreQuery(DmodG.QrAux, Dmod.MyDB);
    if DmodG.QrAux.RecordCount > 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + Lowercase(Tabela) + ' SET ');
      Dmod.QrUpd.SQL.Add('IDCtrl=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P1 ');
      Dmod.QrUpd.SQL.Add('AND OriCodi=:P2');
      Dmod.QrUpd.SQL.Add('AND OriCtrl=:P3 ');
      while not DmodG.QrAux.Eof do
      begin
        ID := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
        Dmod.QrUpd.Params[00].AsInteger := ID;
        Dmod.QrUpd.Params[01].AsInteger := DmodG.QrAux.FieldByName('Tipo').AsInteger;
        Dmod.QrUpd.Params[02].AsInteger := DmodG.QrAux.FieldByName('OriCodi').AsInteger;
        Dmod.QrUpd.Params[03].AsInteger := DmodG.QrAux.FieldByName('OriCtrl').AsInteger;
        Dmod.QrUpd.ExecSQL;
        //
        DmodG.QrAux.Next;
      end;
    end;
  end;  
end;

function TMyDBCheck.CRCTableManageToQeiLnkOn(
  CRCTableManage: TCRCTableManage): Boolean;
const
  sProcName = 'MyDBCheck.CRCTableManageToQeiLnkOn()';
begin
  Result := False;
  case CRCTableManage of
    (*1*)TCRCTableManage.crctmStandBy,
    (*2*)TCRCTableManage.crctmERPUpload,
    (*4*)TCRCTableManage.crctmCRCUpNotSyncToward:
    begin
      // Nada! >> Result := False;
    end;
    (*3*)TCRCTableManage.crctmCRCUpAndSyncToward,
    (*5*)TCRCTableManage.crctmAllUpAndSyncToward,
    (*6*)TCRCTableManage.crctmCRCUpAndSyncDelSelf,
    (*7*)TCRCTableManage.crctmCRCUpAndSyncDelGnrc,
    (*8*)TCRCTableManage.crctmAllUpAndSyncOnlyIns:
    begin
      Result := True;
    end;
    //(*0*)TCRCTableManage.crctmIndef
    else
      Geral.MB_Erro(
      '"CRCTableManage (QeiLnkOn) " n�o implementado em ' +
      sProcName);
  end;
end;

function TMyDBCheck.CriaTabela(Database: TmySQLDatabase; TabelaNome, TabelaBase:
  String; Memo: TMemo; Acao: TAcaoCriaTabela; Tem_Del: Boolean; LaAviso1B,
  LaAviso2B, LaAviso1G, LaAviso2G: TLabel; Tem_Sync: Boolean = False;
  Idx_CDR: Boolean = False; QeiLnkOn: TCRCTableManage =
  TCRCTableManage.crctmStandBy; NomeTabItemDel: String = ''): Boolean;
var
  i, j, k, u: Integer;
  Item, Opcoes, Campos: String;
  Indices, Conta: TStringList;
  Texto: TStringList;
  TemControle: TTemControle;
begin
  Result := True;
  FLCampos  := TList<TCampos>.Create;
  FLIndices := TList<TIndices>.Create;
  Texto     := TStringList.Create;
  try
    TemControle := [];
    MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
    //
    if (tctrlNil in TemControle) then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Ativo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
    end else
    begin
      if (tctrlLok in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'Lk';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if (tctrlCad in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'DataCad';
        FRCampos.Tipo       := 'date';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if (tctrlAlt in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'DataAlt';
        FRCampos.Tipo       := 'date';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if (tctrlCad in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'UserCad';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if (tctrlAlt in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'UserAlt';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if (tctrlWeb in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'AlterWeb';
        FRCampos.Tipo       := 'tinyint(1)';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '1';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if (tctrlAWSI in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'AWServerID';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpERPSync;
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'AWStatSinc';
        FRCampos.Tipo       := 'tinyint(1)';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '0'; // SQLType
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpERPSync;
        FLCampos.Add(FRCampos);
        //
      end;
      if (tctrlAti in TemControle) then
      begin
        New(FRCampos);
        FRCampos.Field      := 'Ativo';
        FRCampos.Tipo       := 'tinyint(1)';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '1';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
      end;
      if Tem_Del then
      begin
        New(FRCampos);
        FRCampos.Field      := 'UserDel';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'DataDel';
        FRCampos.Tipo       := 'datetime';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '0000-00-00 00:00:00';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MotvDel';
        FRCampos.Tipo       := 'int';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '0'; // 0=N�o info
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
        //
      end;
      if Tem_Sync then
      begin
        New(FRCampos);
        FRCampos.Field      := 'LastModifi';
        FRCampos.Tipo       := 'datetime';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '0000-00-00 00:00:00';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
        FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR           //2 => Exclus�o
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'CliIntSync';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
        FRCampos.Key        := '';
        FRCampos.Default    := '0'; // 0=N�o info
        FRCampos.Extra      := '';
        FRCampos.Coments    := '';
        FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
        FLCampos.Add(FRCampos);
        //
      end;
    end;

{
    New(FRCampos);
    FRCampos.Field      := mycoCampoAlterWeb;
    FRCampos.Tipo       := 'tinyint(1)';
    FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
    FRCampos.Key        := '';
    FRCampos.Default    := '1';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
    New(FRCampos);
    FRCampos.Field      := mycoCampoAtivo;
    FRCampos.Tipo       := 'tinyint(1)';
    FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
    FRCampos.Key        := '';
    FRCampos.Default    := '1';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
}
    if Tem_Del = False then
    begin
      MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
      if Idx_CDR then
        CarregaIndicesCDR(FLIndices);
    end;
    try
      Dmod.QrNTI.Close;
      Dmod.QrNTI.SQL.Clear;
      Dmod.QrNTI.Database := DataBase;
      Dmod.QrNTI.SQL.Clear;
      Dmod.QrNTI.SQL.Add('CREATE TABLE '+Lowercase(TabelaNome)+' (');
      //
      Item := Format(ivTabela_Nome, [TabelaNome]);
      for i:= 0 to FLCampos.Count - 1 do
      begin
        Opcoes := myco_;
        FRCampos := FLCampos[i];
        //ShowMensagem(FRCampos.Field);
        Opcoes := FRCampos.Tipo;
        // N�o � aqui! fica junto com o Tipo! Ex: "tinyint(3) unsigned"
        //if pos(Uppercase('unsigned'), Uppercase(FRCampos.Extra)) > 0 then
          //Opcoes := Opcoes + ' unsigned ';
         // inicio BERLIN
        //if (FRCampos.Null <> 'YES') then
        if (FRCampos.Null <> 'YES') or (FRCampos.Default <> myco_ ) then
          Opcoes := Opcoes + ' NOT NULL '
        // inicio BERLIN
        else
          Opcoes := Opcoes + ' NULL ';
        // final BERLIN
        if FRCampos.Default <> myco_ then
        begin
          if UpperCase(FRCampos.Default) = 'CURRENT_TIMESTAMP' then
            Opcoes := Opcoes + ' DEFAULT ' + FRCampos.Default
          else
            Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                        FRCampos.Default + mycoAspasDuplas;
        end;
        if FRCampos.Extra <> myco_ then
        begin
          //if Uppercase(FRCampos.Extra) = Uppercase('auto_increment')
          //then Opcoes := Opcoes + FRCampos.Extra;
          if pos(Uppercase('auto_increment'), Uppercase(FRCampos.Extra)) > 0 then
            Opcoes := Opcoes + ' auto_increment '
          else
            Opcoes := Opcoes + ' ' + FRCampos.Extra;
        end;
        Opcoes := mycoEspacos2+FRCampos.Field+mycoEspaco+Opcoes;
        if i < (FLCampos.Count-1) then
          Opcoes := Opcoes + mycoVirgula;
        Texto.Add(Opcoes);
        //Texto[Texto.Count] := Opcoes;
      end;
      Indices := TStringList.Create;
      Indices.Sorted := True;
      Indices.Duplicates := (dupIgnore);
      try
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          Indices.Add(FRIndices.Key_name);
        end;
        u := Texto.Count-1;
        if Indices.Count > 0 then Texto[u] := Texto[u] + mycoVirgula;
        for k := 0 to Indices.Count -1 do
        begin
          Conta := TStringList.Create;
          Campos := myco_;
          for i := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[i];
            if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
          end;
          for j := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
          begin
            for i := 0 to FLIndices.Count -1 do
            begin
              FRIndices := FLIndices.Items[i];
              if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
              begin
                if Campos <> myco_ then
                  Campos := Campos + ',';
                Campos := Campos + FRIndices.Column_name;
              end;
            end;
          end;
          //if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          if Tem_Del = False then
          begin
            if Uppercase(Indices[k]) = Uppercase('PRIMARY') then
              Texto.Add('  PRIMARY KEY (' + Campos + ')')
            else if Uppercase(Copy(Indices[k], 1, 6)) = Uppercase('UNIQUE') then
              Texto.Add('  UNIQUE KEY '+ Indices[k] + ' ('+Campos+')')
            else
              Texto.Add('  INDEX '     + Indices[k] + ' ('+Campos+')');
            if k < (Indices.Count-1) then
            begin
             u := Texto.Count-1;
             Texto[u] := Texto[u]+mycoVirgula;
            end;
          end;
          Conta.Free;
        end;
      finally
        Indices.Free;
      end;
      Texto.Add(')');
      // 15-06-2009
      //Texto.Add('ENGINE = InnoDB1');
      Texto.Add('CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
      // fim 15-06-2009
      for i := 0 to Texto.Count-1 do
      Dmod.QrNTI.SQL.Add(Texto[i]);
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrNTI, '', Memo, False, False);
      if Acao = actCreate then
      begin
        Dmod.QrNTI.ExecSQL;
        GravaAviso(Item+ivCriado, Memo);
        VerificaRegistrosObrigatorios_Inclui(Database, TabelaNome, TabelaBase,
          Memo, False, True, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
        VerificaRegistrosDeArquivo(Database, TabelaNome, TabelaBase, NomeTabItemDel, Memo, False);
        if CRCTableManageToQeiLnkOn(TCRCTableManage(QeiLnkOn)) then
          VerificaRegistrosDBMQeiLnk(Database, TabelaNome, TabelaBase,
          Tem_Del, QeiLnkOn, NomeTabItemDel, Memo);
      end;
    except
      //GravaAviso(Item+ivMsgERROCriar, Memo);
      on E: Exception do
        GravaAviso(Item+E.Message+sLineBreak+ivMsgERROCriar, Memo);
    end;
  finally
    FLCampos.Free;
    FLIndices.Free;
    Texto.Free;
  end;
end;

procedure TMyDBCheck.Define_Form_ID(Form_ID: String);
begin
  if Copy(Uppercase(Form_ID), 1, 12) <> 'FRX-PRINT-00' (*2'*) then
    VAR_FORM_ID := Form_ID;
end;

procedure TMyDBCheck.DropCamposTabelas(ClFldsNoNeed: TCheckListBox;
  DataBase: TmySQLDataBase; Memo: TMemo);
var
  I, P, N: Integer;
  Item, Tabela, Campo: String;
begin
  N := 0;
  for I := ClFldsNoNeed.Items.Count - 1 downto 0 do
  begin
    if ClFldsNoNeed.Checked[I] then
    begin
      try
        Item := ClFldsNoNeed.Items[I];
        P := pos('.', Item);
        Tabela := Copy(ClFldsNoNeed.Items[I], 1, P - 1);
        Campo  := Copy(ClFldsNoNeed.Items[I], P + 1);
        //
        Dmod.QrSQL.Close;
        Dmod.QrSQL.DataBase := DataBase;
        Dmod.QrSQL.SQL.Clear;
        Dmod.QrSQL.SQL.Add(mycoALTERTABLE + Tabela + mycoDROP + Campo);
        if MyList.ExcluiReg then
        begin
          {
          if not FPergunta then Resp := ID_YES else
          Resp := Geral.MB_Pergunta('Confirma a EXCLUS�O do campo '+
          Nome+'?');
          if Resp = ID_YES then
          begin
          }
            Dmod.QrSQL.ExecSQL;
            N := N + 1;
          {
            GravaAviso(Item + ivMsgExcluida, Memo);
          end else begin
            GravaAviso(Item+ivAbortExclUser, Memo);
            if Resp = ID_CANCEL then
            begin
              Result := rvAbort;
              Exit;
            end;
          end;
          }
        end;
        ClFldsNoNeed.Items.Delete(I);
      except
        GravaAviso(Item + ivMsgERROexcluir, Memo);
        raise;
      end;
    end;
  end;
  Geral.MB_Info(Geral.FF0(N) + ' campos foram exclu�dos de tabelas!');
end;

function TMyDBCheck.DropIndicesTabelas(ClIdxsNoNeed: TCheckListBox; Database:
  TMySQLDatabase; Memo: TMemo): TResultVerify;
var
  I, P, N: Integer;
  Item, Tabela, IdxNome: String;
begin
  N := 0;
  for I := ClIdxsNoNeed.Items.Count - 1 downto 0 do
  begin
    if ClIdxsNoNeed.Checked[I] then
    begin
      try
        Item := ClIdxsNoNeed.Items[I];
        P := pos('.', Item);
        Tabela  := Copy(ClIdxsNoNeed.Items[I], 1, P - 1);
        IdxNome := Copy(ClIdxsNoNeed.Items[I], P + 1);
        //

        Dmod.QrSQL.Close;
        Dmod.QrSQL.DataBase := DataBase;
        Dmod.QrSQL.SQL.Clear;
        if Uppercase(IdxNome) = Uppercase('PRIMARY') then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY')
        else
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome);
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
        Dmod.QrSQL.ExecSQL;
        ClIdxsNoNeed.Items.Delete(I);
      except
        GravaAviso(Item + ivMsgERROexcluir, Memo);
        raise;
      end;
    end;
  end;
  Geral.MB_Info(Geral.FF0(N) + ' �ndices foram exclu�dos de tabelas!');
end;

procedure TMyDBCheck.VerificaVariaveis(Memo: TMemo);
const
  Texto= 'Variavel ''%s'' n�o definida';
begin
  if VAR_CAMPOTRANSPORTADORA = '' then
  Memo.Lines.Add(Format(Texto, ['VAR_CAMPOTRANSPORTADORA']));
end;

procedure TMyDBCheck.GravaAviso(Aviso: String; Memo: TMemo; ToFile: Boolean);
begin
  if Memo <> nil then
  begin
    Memo.Lines.Add(Aviso);
    if FToFile or ToFile then // 2022-07-23
      WriteLn(FText, Aviso);
  end;  
end;

procedure TMyDBCheck.VerificaControle(Memo: TMemo);
begin
  // N�o necess�rio??
end;

(* 23/02/2015 => Aparentemente n�o usa
function TMyDBCheck.AcessoNegadoAoForm_2(Tabela, Form : String; ModoAcesso:
  TAcessFormModo; LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): Boolean;

  function Privilegios(Usuario: Integer): Boolean;
  begin
    FM_MASTER := 'F';
    DmodG.QrPerfis.Close;
    // adicionado em 2008.03.17
    DmodG.QrPerfis.SQL.Clear;
    DmodG.QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    DmodG.QrPerfis.SQL.Add('FROM perfisits pit');
    DmodG.QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    DmodG.QrPerfis.SQL.Add('LEFT JOIN senhas sen ON sen.Perfil=pip.Codigo');
    DmodG.QrPerfis.SQL.Add('WHERE sen.Numero=:P0');
    DmodG.QrPerfis.SQL.Add('');
    // fim adicionado em 2008.03.17
    DmodG.QrPerfis.Params[0].AsInteger := Usuario;
    UnDmkDAC_PF.AbreQuery(DmodG.QrPerfis, Dmod.MyDB);
    Result := DmodG.QrPerfis.RecordCount > 0;
    if not Result then Geral.MB_Aviso('N�o h� privil�gios para o usu�rio ' +
      IntToStr(VAR_USUARIO) + ' - ' + VAR_LOGIN + '!');
    // N�o pode fechar!!
    //DmodG.QrPerfis.Close;
  end;
const
  Tfm = 'TFM';
var
  Loc: Boolean;
begin
  if ModoAcesso = afmAcessoTotal then
  begin
    Result := False;
    Exit;
  end;
  if Uppercase(Copy(Form, 1, 3)) = Tfm then Form := Copy(Form, 4, Length(Form)-3);
  if ModoAcesso = afmSoMaster then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then Result := False
    else Geral.MB_Aviso('Acesso Restrito');
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if ModoAcesso = afmNegarComAviso then
    begin
      Geral.MB_Erro('Tabela de perfis n�o definida ao verificar acesso.');
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  or (DmodG.QrMasterSolicitaSenha.Value = 0)
  then Result := False
  else begin
    Privilegios(VAR_USUARIO);
    if not DmodG.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
        Dmod.QrAux.Params[0].AsString  := Form;
        UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
        if Dmod.QrAux.RecordCount = 0 then
        begin
          DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'perfisits', 'perfisits', nil,
            True, True, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
          Dmod.QrAux.Params[0].AsString  := Form;
          UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          if Dmod.QrAux.RecordCount = 0 then
          begin
            Dmod.QrAux.Close;
            Dmod.QrAux.SQL.Clear;
            Dmod.QrAux.SQL.Add('INSERT INTO perfisits SET Janela=:P0');
            Dmod.QrAux.Params[0].AsString  := Form;
            Dmod.QrAux.ExecSQL;
            DmodG.Privilegios(-1000);
          end;
        end;
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (DmodG.QrPerfisLibera.Value = 0) or (Loc=False) then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MB_Aviso('Somente o administrador tem acesso a este formul�rio!')
      else begin
        case ModoAcesso of
          afmNegarSemAviso: ;// nada;  Msg=-1
          afmNegarComAviso: //  Msg = 0 ou 2
            Geral.MB_Aviso(VAR_ACESSONEGADO + sLineBreak + '[' +  Form + ']');
          afmParcial:  // Msg = 1
            Geral.MB_Aviso('Login com acesso parcial!');
          afmParcialSemEdicao:   // Msg = 3
            Geral.MB_Aviso('Login com acesso parcial, sem acesso a edi��o!');
          end;
      end;
    end else Result := False;
  end;
end;
*)

function TMyDBCheck.AcessoNegadoAoForm_3(Tabela, Form, Descricao: String;
ModoAcesso: TAcessFormModo; LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel): Boolean;

  (* Usar DModG.Privilegios
  function Privilegios(Usuario: Integer): Boolean;
  begin
    FM_MASTER := 'F';
    DmodG.QrPerfis.Close;
    // adicionado em 2008.03.17
    DmodG.QrPerfis.SQL.Clear;
    DmodG.QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    DmodG.QrPerfis.SQL.Add('FROM perfisits pit');
    DmodG.QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    DmodG.QrPerfis.SQL.Add('LEFT JOIN senhas sen ON sen.Perfil=pip.Codigo');
    DmodG.QrPerfis.SQL.Add('WHERE sen.Numero=:P0');
    DmodG.QrPerfis.SQL.Add('');
    // fim adicionado em 2008.03.17
    DmodG.QrPerfis.Params[0].AsInteger := Usuario;
    UnDmkDAC_PF.AbreQuery(DmodG.QrPerfis, Dmod.MyDB);
    Result := DmodG.QrPerfis.RecordCount > 0;
    if not Result then
    Geral.MB_Aviso('N�o h� privil�gios para o usu�rio ' +
    IntToStr(VAR_USUARIO) + ' - ' + VAR_LOGIN + '!');
    // N�o pode fechar!!
    //DmodG.QrPerfis.Close;
  end;
  *)
const
  Tfm = 'TFM';
var
  Loc: Boolean;
begin
  if ModoAcesso = afmAcessoTotal then
  begin
    Result := False;
    Exit;
  end;
  if Uppercase(Copy(Form, 1, 3)) = Tfm then Form := Copy(Form, 4, Length(Form)-3);
  if ModoAcesso = afmSoMaster then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then Result := False
    else Geral.MB_Aviso('Acesso Restrito (Master)');
    Exit;
  end;
  if ModoAcesso = afmSoBoss then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then Result := False
    else Geral.MB_Aviso('Acesso Restrito (Administrador)');
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if ModoAcesso = afmNegarComAviso then
    begin
      Geral.MB_Erro('Tabela de perfis n�o definida ao verificar acesso.');
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  or (DmodG.QrMasterSolicitaSenha.Value = 0)
  then Result := False
  else begin
    DmodG.Privilegios(VAR_USUARIO, False);
    if not DmodG.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        if VAR_DERMA_AD = 2 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
            'SELECT * ',
            'FROM perfits ',
            'WHERE Janela="' + Form + '"',
            '']);
          if Dmod.QrAux.RecordCount = 0 then
          begin
            DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'perfits', 'perfits', nil,
              True, True, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
            //
            UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
              'SELECT * ',
              'FROM perfits ',
              'WHERE Janela="' + Form + '"',
              '']);
            if Dmod.QrAux.RecordCount = 0 then
            begin
              Dmod.QrAux.Close;
              Dmod.QrAux.SQL.Clear;
              Dmod.QrAux.SQL.Add('INSERT INTO perfits ');
              Dmod.QrAux.SQL.Add('SET Janela=:P0 ');
              Dmod.QrAux.Params[0].AsString  := Form;
              Dmod.QrAux.ExecSQL;
              DmodG.Privilegios(-1000);
            end;
          end;
        end else
        begin
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
          Dmod.QrAux.Params[0].AsString  := Form;
          UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          if Dmod.QrAux.RecordCount = 0 then
          begin
            DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'perfisits', 'perfisits', nil,
              True, True, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G);
            Dmod.QrAux.Close;
            Dmod.QrAux.SQL.Clear;
            Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
            Dmod.QrAux.Params[0].AsString  := Form;
            UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
            if Dmod.QrAux.RecordCount = 0 then
            begin
              Dmod.QrAux.Close;
              Dmod.QrAux.SQL.Clear;
              Dmod.QrAux.SQL.Add('INSERT INTO perfisits ');
              Dmod.QrAux.SQL.Add('SET Janela=:P0, Descricao=:P1');
              Dmod.QrAux.Params[0].AsString  := Form;
              Dmod.QrAux.Params[1].AsString  := Descricao;
              Dmod.QrAux.ExecSQL;
              DmodG.Privilegios(-1000);
            end;
          end;
        end;
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (DmodG.QrPerfisLibera.Value = 0) or (Loc=False) then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MB_Aviso('Somente o administrador tem acesso a este formul�rio!')
      else begin
        case ModoAcesso of
          afmNegarSemAviso: ;// nada;  Msg=-1
          afmNegarComAviso: //  Msg = 0 ou 2
            Geral.MB_Aviso(VAR_ACESSONEGADO+sLineBreak+'['+
            Form+']');
          afmParcial:  // Msg = 1
            Geral.MB_Aviso('Login com acesso parcial!');
          afmParcialSemEdicao:   // Msg = 3
            Geral.MB_Aviso('Login com acesso parcial, sem acesso a edi��o!');
        end;
      end;
    end else Result := False;
  end;
end;

procedure TMyDBCheck.AdicionaFldNoNeed(ClFldsNoNeed: TCheckListBox;
  Tabela, Campo: String);
var
  Texto: String;
  I, N: Integer;
begin
  Texto := Tabela + '.' + Campo;
  N := -1;
  //
  for I := 0 to ClFldsNoNeed.Items.Count - 1 do
  begin
    if ClFldsNoNeed.Items[I] = Texto then
    begin
      N := I;
      Break;
    end;
  end;
  //
  if N = -1 then
    ClFldsNoNeed.Items.Add(Texto);
end;

procedure TMyDBCheck.AdicionaIdxNoNeed(ClIdxsNoNeed: TCheckListBox;
  Tabela, Campo: String);
var
  Texto: String;
  I, N: Integer;
begin
  Texto := Tabela + '.' + Campo;
  N := -1;
  //
  for I := 0 to ClIdxsNoNeed.Items.Count - 1 do
  begin
    if ClIdxsNoNeed.Items[I] = Texto then
    begin
      N := I;
      Break;
    end;
  end;
  //
  if N = -1 then
    ClIdxsNoNeed.Items.Add(Texto);
end;

procedure TMyDBCheck.AtivaCodigos(Aviso, Titulo, Tabela, FldCodi, FldNome,
  FldAtiv: String; SQL: array of String; DB: TmySQLDataBase; DefLoc: Integer);
begin
  Application.CreateForm(TFmAtivaCod, FmAtivaCod);
(*
  FmAtivaCod.DBGAtiva.Columns[0].FieldName := FldAtiv;
  FmAtivaCod.DBGAtiva.Columns[1].FieldName := FldCodi;
  FmAtivaCod.DBGAtiva.Columns[2].FieldName := FldNome;
*)
  FmAtivaCod.FTitulo  := Aviso;
  FmAtivaCod.FDB      := DB;
  FmAtivaCod.FSQL     := Geral.ATS(SQL);
  FmAtivaCod.FTabNome := Tabela;
  FmAtivaCod.FFldCodi := FldCodi;
  FmAtivaCod.FFldNome := FldNome;
  FmAtivaCod.FFldAtiv := FldAtiv;
  FmAtivaCod.ReopenAtivo(DefLoc);
  FmAtivaCod.FormResize(Self);
  MyObjects.Informa2(FmAtivaCod.LaAviso1, FmAtivaCod.LaAviso2, False, Aviso);
  //
  FmAtivaCod.ShowModal;
  FmAtivaCod.Destroy;
end;

{$IFNDEF NAO_CRIA_USER_DB}
procedure TMyDBCheck.MostraInfoSeq(Janela, Descricao: array of string;
  Coluna1: String = ''; Coluna2: String = ''; TamColuna1: Integer = 120;
  TamColuna2: Integer = 350);
var
  I, Linha, Item: Integer;
  InfoSeqTab, Jan, Descri, Col1, Col2: String;
begin
{$IFNDEF NAO_CRIA_USER_DB}
  InfoSeqTab := UCriar.RecriaTempTableNovo(ntrttInfoSeq, DmodG.QrUpdPID1, False);
{$EndIf}
  //
  if Coluna1 = '' then
    Col1 := 'Janela: Local'
  else
    Col1 := Coluna1;
  if Coluna2 = '' then
    Col2 := 'A��o / informa��o'
  else
    Col2 := Coluna2;
  //
  if Length(Janela) <> Length(Descricao) then
  begin
    Geral.MB_Aviso('A quantidade de itens da vari�vel Janela n�o confere com a quantidade de itens da vari�vel Descricao!');
    Exit;
  end;
  for I := Low(Janela) to High(Janela) do
  begin
    Linha  := UMyMod.BuscaNovoCodigo_Int(DmodG.QrUpdPID1, InfoSeqTab, 'Linha',
               [], [], stIns, 0, siPositivo, nil);
    Item   := UMyMod.BuscaNovoCodigo_Int(DmodG.QrUpdPID1, InfoSeqTab, 'Item',
               [], [], stIns, 0, siPositivo, nil);
    Descri := Descricao[I];
    Jan    := Janela[I];
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, InfoSeqTab, False, ['Linha',
      'Item', 'Local', 'Acao'], [], [Linha, Item, Jan, Descri], [], False);
  end;
  if DBCheck.CriaFm(TFmInfoSeq, FmInfoSeq, afmoLiberado) then
  begin
    FmInfoSeq.DBGrid1.Columns[1].Title.Caption := Col1;
    FmInfoSeq.DBGrid1.Columns[2].Title.Caption := Col2;
    FmInfoSeq.DBGrid1.Columns[1].Width         := TamColuna1;
    FmInfoSeq.DBGrid1.Columns[2].Width         := TamColuna2;
    FmInfoSeq.FTmpTable                        := InfoSeqTab;
    FmInfoSeq.BtMostra.Visible                 := False;
    //
    FmInfoSeq.ShowModal;
    FmInfoSeq.Destroy;
  end;
end;
{$ENDIF}

procedure TMyDBCheck.AvisaDBTerceiros();
begin
  if Geral.MB_Pergunta('N�o foi poss�vel abrir uma ou mais tabelas de terceiros!'
  + sLineBreak + 'Deseja verificar a estrutura do banco de dados de terceiros?') = ID_YES then
  begin
    {$IFNdef SemBDTErceiros}
    try
      if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
      begin
        FmVerifiDBTerceiros.ShowModal;
        FmVerifiDBTerceiros.Destroy;
      end;
    except
      if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoLiberado) then
      begin
        FmVerifiDBTerceiros.ShowModal;
        FmVerifiDBTerceiros.Destroy;
      end;
    end;
    {$Else}
    Geral.MB_Aviso('N�o foi poss�vel abrir uma ou mais tabelas de terceiros!'
    + sLineBreak + 'Avise a Dermatek');
    {$EndIf}
  end;
end;

function TMyDBCheck.CriaFormAntigo(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFormModo): Boolean;
var
  Instance: TComponent;
  s, Form: String;
  MyCursor: TCursor;
  k: Integer;
begin
  Result := False;
  Form := InstanceClass.ClassName;
  if Pos('TFm', Form) = 1 then
    Form := Copy(Form, 4, Length(Form)-3);
  //ShowMessage(Form);
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      k := 13;
      s := Copy(TForm(Instance).Caption, 1, k);
      Define_Form_ID(s);
      if VAR_LIBERA_TODOS_FORMS = False then
      begin
        //ShowMessage(TForm(Instance).Caption);
        if AcessoNegadoAoForm_3('Perfis', Form, TForm(Instance).Caption,
        ModoAcesso, nil, nil, nil, nil) then
        begin
          TForm(Instance).Destroy;
          Exit;
        end else Result := True;
      end else Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TMyDBCheck.CriaDatabase(Query: TmySQLQuery; Database: String): Boolean;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('CREATE DATABASE ' + (*Lowercase(*)Database(*)*));
  Query.SQL.Add('DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
  Query.ExecSQL;
  Result := True;
end;

function TMyDBCheck.CriaFm(InstanceClass: TComponentClass; var Reference;
  ModoAcesso: TAcessFmModo; Titulo: String): Boolean;

  procedure Habilita(Compo: TComponent; Visivel: Boolean);
  var
    PropInfo: PPropInfo;
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, 'Visible');
      if PropInfo <> nil then
        SetPropValue(Compo, 'Visible', Visivel);
    end;
  end;

var
  Instance: TComponent;
  Form, Nome, Form_ID, NovoTitulo, NomeForm: String;
  MyCursor: TCursor;
  k, i, Acesso: Integer;
  Inclui, Altera, Exclui: Boolean;
  ModoAntigo: TAcessFormModo;
  Objeto: TObject;
  PropInfo: PPropInfo;
  Continua: Boolean;
begin
  if VAR_APLICATION_TERMINATE then
    Exit;

  {
  if  (UpperCase(Application.Title) = CO_APP_TITLE_003)//CRED ITOR
  or  (UpperCase(Application.Title) = 'SYNDIC') then
  }

  {$IFDEF UsaWSuport}
  (*
  if InstanceClass <> TFmWebBrowser then
  begin
    if FmWebBrowser <> nil then
      FmWebBrowser.Close;
  end else
  *)
  if InstanceClass <> TFmWSuporte then
  begin
    if FmWSuporte <> nil then
      FmWSuporte.Close;
  end;
  {$ENDIF}

  if (UpperCase(Application.Title) = CO_APP_TITLE_003) then //CRED ITOR
  begin
    case ModoAcesso of
      afmoSoMaster:      ModoAntigo := afmSoMaster;
      afmoSoAdmin:       ModoAntigo := afmSoMaster;
      afmoSoBoss:        ModoAntigo := afmSoBoss;
      afmoNegarSemAviso: ModoAntigo := afmNegarSemAviso;
      afmoNegarComAviso: ModoAntigo := afmNegarComAviso;
      afmoLiberado(*afmoProvisorio*)   : ModoAntigo := afmAcessoTotal;
      afmoSemVerificar  : ModoAntigo := afmAcessoTotal;
      //
      else ModoAntigo := afmNegarComAviso;
    end;
    Result := CriaFormAntigo(InstanceClass, Reference, ModoAntigo);
    Exit;
  end;
  Result := False;
  Form := InstanceClass.ClassName;
  //
  if Pos('TFm', Form) = 1 then
    Form := Copy(Form, 4, Length(Form)-3);
  //ShowMessage(Form);
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      //try
        Instance.Create(Application);
      (*
      except
        on E: exception do
        begin
          if FindWindow(PWideChar(TComponent(InstanceClass).ClassName), nil) <> 0 then
          begin
            TComponent(InstanceClass).Destroy;
          end
          else Geral.MB_Erro(E.Message);

        end;
      end;
      *)
{
      // 2011-09-23
      TForm(Reference).AlphaBlendValue := 0;
      TForm(Reference).AlphaBlend := True;
      // Fim 2011-09-23
}
      ///
      if VAR_SOMAIUSCULAS then
      begin
        with TForm(Reference) do
        begin
          for i := 0 to ComponentCount -1 do
          begin
            Objeto := Components[i];
            PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'CharCase');
            if PropInfo <> nil then
            begin
              Nome := Uppercase(TComponent(Objeto).Name);

              if (Nome <> 'EDEMAIL') and (Nome <> 'CBEMAIL') and
              (Nome <> 'EDPEMAIL') and (Nome <> 'EDEEMAIL') then
              try
                if (GetPropValue(TComponent(Objeto), 'CharCase') = 'ecNormal') then
                begin
                  Continua := True;
                  PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'NoForceUppercase');
                  //
                  if PropInfo <> nil then
                  begin
                    if GetPropValue(TComponent(Objeto), 'NoForceUppercase') = 'True' then
                      Continua := False;
                  end;
                  //
                  if Continua then
                    SetPropValue(Objeto, 'CharCase', 'ecUpperCase');
                end;
              except
                //
              end;
            end;
          end;
        end;
      end;
      {
      if  (UpperCase(Application.Title) = CO_APP_TITLE_003) //CRED ITOR
      or  (UpperCase(Application.Title) = 'SYNDIC') then
      }
      if (UpperCase(Application.Title) = CO_APP_TITLE_003) then//CRED ITOR
      begin
        Screen.Cursor := MyCursor;
        Result := True;
        Exit;
      end;

      ///
      //k := pos('::', TForm(Instance).Caption);
      //if k = 0 then
      k := 13;
      if Titulo <> '' then
        TForm(Instance).Caption := Titulo
////// inicio 2019-01-18
      else
      begin
        if pos('icmsipi', LowerCase(TForm(Instance).Name)) > 0 then
        begin
          NomeForm   := LowerCase(TForm(Instance).Name);
          NovoTitulo := TForm(Instance).Caption;
          if pos('_v03_0_1', NomeForm) > 0 then
            TForm(Instance).Caption := NovoTitulo + ' v 3.0.1'
          else
          //if pos('_v03_0_2_a', NovoTitulo)) > 0 then
          if pos('_v03_0_2', NomeForm) > 0 then
            TForm(Instance).Caption := NovoTitulo + ' v 3.0.beta'
        end;
      end;
////// fim 2019-01-18
      //
      Form_ID := Copy(TForm(Instance).Caption, 1, k);
      Define_Form_ID(Form_ID);
      // Verifica se a janela existe
      DModG.QrPerfJan.Close;
      DModG.QrPerfJan.Params[0].AsString := Form_ID;
      UnDmkDAC_PF.AbreQuery(DModG.QrPerfJan, Dmod.MyDB);
      if DModG.QrPerfJan.RecordCount = 0 then
      begin
        if (Form_ID <> 'MAS-CADAS-000') and (ModoAcesso <> afmoSemVerificar) then
        begin
          Geral.MB_Aviso('A janela ' + Form_ID +
          ' n�o est� cadastrada para ser consultada!' + sLineBreak +
          'Informe a DERMATEK!');
          if (VAR_USUARIO >=0) or (Form_ID <> 'FER-VRFBD-001') then
          begin
            TForm(Instance).Destroy;
            Screen.Cursor := MyCursor;
            Exit;
          end;
        end;
      end;
      if VAR_LIBERA_TODOS_FORMS = False then
      begin
        //ShowMessage(TForm(Instance).Caption);
        Acesso := AcessoAoForm(Form_ID, ModoAcesso);
        if Acesso = 0 then
        begin
          TForm(Instance).Destroy;
          Screen.Cursor := MyCursor;
          Exit;
        end else begin
          Result := True;
          if Acesso < 8 then
          begin
            Inclui := DmodG.QrPerfInclui.Value = 1;
            Altera := DmodG.QrPerfAltera.Value = 1;
            Exclui := DmodG.QrPerfExclui.Value = 1;
            for i := 0 to TComponent(Reference).ComponentCount - 1 do
            begin
              if TComponent(Reference).Components[i] is TdmkPermissoes then
              begin
                if TdmkPermissoes(TComponent(Reference)) <> nil then
                begin
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns01, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns02, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns03, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns04, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns05, Inclui);
                  //
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd01, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd02, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd03, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd04, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd05, Altera);
                  //
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel01, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel02, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel03, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel04, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel05, Exclui);
                  //
                end;
              end;
            end;
          end;
        end;
      end;
    except
      //VOSTRO_1320
      raise;
      TComponent(Reference) := nil;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TMyDBCheck.AcessoNegadoAoForm_4(ModoAcesso: TAcessFmModo; Form: String): Boolean;
var
  MyCursor: TCursor;
  Form_ID: String;
  Acesso: Integer;
  Continua: Boolean;
begin
  Result := True;
  //
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Form_ID := Form;
    //
    Define_Form_ID(Form_ID);
    // Verifica se a janela existe
    DModG.QrPerfJan.Close;
    DModG.QrPerfJan.Params[0].AsString := Form_ID;
    UnDmkDAC_PF.AbreQuery(DModG.QrPerfJan, Dmod.MyDB);
    //
    if DModG.QrPerfJan.RecordCount = 0 then
    begin
      if (Form_ID <> 'MAS-CADAS-000') and (ModoAcesso <> afmoSemVerificar) then
      begin
        Geral.MB_Aviso('A janela ' + Form_ID +
        ' n�o est� cadastrada para ser consultada!' + sLineBreak +
        'Informe a DERMATEK!');
        if (VAR_USUARIO >=0) or (Form_ID <> 'FER-VRFBD-001') then
        begin
          Screen.Cursor := MyCursor;
          Exit;
        end;
      end;
    end;
    if VAR_LIBERA_TODOS_FORMS = False then
    begin
      //ShowMessage(TForm(Instance).Caption);
      Acesso := AcessoAoForm(Form_ID, ModoAcesso);
      if Acesso = 0 then
      begin
        Screen.Cursor := MyCursor;
        Exit;
      end else begin
        Result := False;
      end;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

// escolher ( selecionar ) quais exclui
function TMyDBCheck.QuaisItens_Exclui(QrExecSQL, QrDados: TmySQLQuery; DBGrid: TDBGrid;
             TabelaTarget: String; CamposSource, CamposTarget: array of String;
             Quais: TSelType; SQL_AfterDel: String; NaoReabre: Boolean = False;
             Pergunta: Boolean = True): Integer;

  function ExecutaSQL: Boolean;
  var
    i, j: Integer;
    Valor: String;
  begin
    Result := False;
    if USQLDB.ImpedeExclusaoPeloNomeDaTabela(TabelaTarget) then
      Exit;
    QrExecSQL.SQL.Clear;
    QrExecSQL.SQL.Add(DELETE_FROM + Lowercase(TabelaTarget) + ' WHERE ');
    //
    j := High(CamposSource);
    for i := Low(CamposSource) to j do
    begin
      Valor := Geral.VariavelToString(QrDados.FieldByName(CamposSource[i]).Value);
      if i < j then
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor + ' AND ')
      else
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor);
    end;
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
      QrExecSQL.ExecSQL;
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
    end;
    //
  end;

var
  n, m, Continua: integer;
  q: TSelType;
begin
  q := istNenhum;
  Result := 0;
  if (QrDados.State <> dsBrowse) or (QrDados.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� item a ser exclu�do!');
    Exit;
  end;
  if Quais = istPergunta then
  begin
    if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
    begin
      with FmQuaisItens do
      begin
        ShowModal;
        if not FSelecionou then
        begin
          Geral.MB_Aviso('Exclus�o cancelada pelo usu�rio!');
          q := istDesiste;
        end else q := FEscolha;
        Destroy;
      end;
    end;
  end
  else q := Quais;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;
  m := 0;
  if (q = istSelecionados) and (DBGrid.SelectedRows.Count < 2) then
    q := istAtual;
  case q of
    istAtual:
    begin
      if Pergunta = True then
        Continua := Geral.MB_Pergunta('Confirma a exclus�o do item selecionado?')
      else
        Continua := ID_YES;
      //
      if Continua = ID_YES then
        if ExecutaSQL() then m := 1;
    end;
    istSelecionados:
    begin
      if Pergunta = True then
        Continua := Geral.MB_Pergunta('Confirma a exclus�o dos ' +
                      Geral.FF0(DBGrid.SelectedRows.Count) + ' itens selecionados?')
      else
        Continua := ID_YES;
      //
      if Continua = ID_YES then
      begin
        with DBGrid.DataSource.DataSet do
        for n := 0 to DBGrid.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGrid.SelectedRows.Items[n]);
          GotoBookmark(DBGrid.SelectedRows.Items[n]);
          if ExecutaSQL() then inc(m, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Pergunta = True then
        Continua := Geral.MB_Pergunta('Confirma a exclus�o de todos os ' +
                      Geral.FF0(QrDados.RecordCount) + ' itens pesquisados?')
      else
        Continua := ID_YES;
      //
      if Continua = ID_YES then
      begin
        QrDados.First;
        while not QrDados.Eof do
        begin
          if ExecutaSQL() then inc(m, 1);
          QrDados.Next;
        end;
      end;
    end;
  end;
  if m > 0 then
  begin
    Result := m;
    if Trim(SQL_AfterDel) <> '' then
    begin
      QrExecSQL.SQL.Clear;
      QrExecSQL.SQL.Add(SQL_AfterDel);
      QrExecSQL.ExecSQL;
    end;
    if NaoReabre = False then
    begin
      QrDados.Close;
      UnDmkDAC_PF.AbreQueryApenas(QrDados);
    end;
    if Pergunta = True then
    begin
      if m = 1 then
        Geral.MB_Info('Um item foi exclu�do!')
      else
        Geral.MB_Info(IntToStr(m) + ' itens foram exclu�dos!');
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TMyDBCheck.Quais_Selecionou(const Query: TmySQLQuery; DBGrid:
TDBGrid; var Quais: TSelType): Boolean;
begin
  Result := True;
  Quais := istNenhum;
  if (Query <> nil) and (Query.State <> dsBrowse) and (Query.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� item a ser selecionado!');
    Exit;
  end;
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Geral.MB_Aviso('Sele��o cancelada pelo usu�rio!');
        Quais := istDesiste;
      end else Quais := FEscolha;
      Destroy;
    end;
  end;
  if (Quais = istSelecionados) and (DBGrid <> nil)
  and (DBGrid.SelectedRows.Count < 2) then
    Quais := istAtual;
  //
  Result := not (Quais in ([istNenhum, istPergunta, istDesiste]));
end;

function TMyDBCheck.Quais_Selecionou_ExecProc(const Query: TmySQLQuery; DBGrid:
TDBGrid; var Quais: TSelType; Procedure1: TProcedure): Boolean;
var
  q: TSelType;
  n: Integer;
begin
  Result := Quais_Selecionou(Query, DBGrid, q);
  if not Result then Exit;
  //
  case q of
    istAtual:
    begin
      Procedure1;
      //procedure2;
    end;
    istSelecionados:
    begin
      with DBGrid.DataSource.DataSet do
      for n := 0 to DBGrid.SelectedRows.Count-1 do
      begin
        //GotoBookmark(DBGrid.SelectedRows.Items[n]);
        GotoBookmark(DBGrid.SelectedRows.Items[n]);
        Procedure1;
        //Procedure2;
      end;
    end;
    istTodos:
    begin
      {
      if Geral.MB_Pergunta('Confirma a exclus�o de todos os ' +
      IntToStr(QrStqMovIts.RecordCount) + ' itens pesquisados?') = ID_YES then
      begin
      }
        Query.First;
        while not Query.Eof do
        begin
          Procedure1;
          //procedure2;
          Query.Next;
        end;
      //end;
    end;
  end;
end;

function TMyDBCheck.QuaisItens_Altera(QrExecSQL, QrDados: TmySQLQuery;
DBGrid: TDBGrid; Tabela: String;
SQLCamposAlt, SQLIndexAlt, SQLIndexSource: array of String;
SQLValores: array of Variant; Quais: TSelType;
Pergunta, AvisaAlterados: Boolean): Boolean;
  function ExecutaSQL(): Boolean;
  var
    i, j: Integer;
    Valor: String;
  begin
    Result := False;
    QrExecSQL.SQL.Clear;
    QrExecSQL.SQL.Add('UPDATE ' + Lowercase(Tabela) + ' SET ');
    //
    j := High(SQLCamposAlt);
    for i := Low(SQLCamposAlt) to j do
    begin
      Valor := Geral.VariavelToString(SQLValores[i]);
      if i < j then
        QrExecSQL.SQL.Add(SQLCamposAlt[i] + '=' + Valor + ', ')
      else
        QrExecSQL.SQL.Add(SQLCamposAlt[i] + '=' + Valor);
    end;
    //
    j := High(SQLIndexAlt);
    for i := Low(SQLIndexAlt) to j do
    begin
      Valor := Geral.VariavelToString(QrDados.FieldByName(SQLIndexSource[i]).Value);
      if i = 0 then
        QrExecSQL.SQL.Add(' WHERE ' + SQLIndexAlt[i] + '=' + Valor + ' ')
      else if i < j then
        QrExecSQL.SQL.Add(' AND ' + SQLIndexAlt[i] + '=' + Valor + ' ');
      end;
    try
      QrExecSQL.ExecSQL;
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
    end;
    //
  end;
  var
    n, Afetados: integer;
    q: TSelType;
    Executa: Boolean;
begin
  q := istNenhum;
  Afetados := 0;
  //
  Result := False;
  if (QrDados.State <> dsBrowse) or (QrDados.RecordCount = 0) then
  begin
    if AvisaAlterados then
      Geral.MB_Aviso('N�o h� item a ser alterado!');
    Exit;
  end;
  if Quais = istPergunta then
  begin
    if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
    begin
      with FmQuaisItens do
      begin
        ShowModal;
        if not FSelecionou then
        begin
          if AvisaAlterados then
            Geral.MB_Aviso('Exclus�o cancelada pelo usu�rio!');
          q := istDesiste;
        end else q := FEscolha;
        Destroy;
      end;
    end;
  end else q := Quais;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;

  if (q = istSelecionados) and (DBGrid.SelectedRows.Count < 2) then
     q := istAtual;
  case q of
    istAtual:
    begin
      if Pergunta then
      begin
        Executa := Geral.MB_Pergunta('Confirma a altera��o do item selecionado?') = ID_YES
      end else Executa := True;
      if Executa then
        if ExecutaSQL() then Afetados := 1;
    end;
    istSelecionados:
    begin
      if Pergunta then
        Executa := Geral.MB_Pergunta('Confirma a altera��o dos ' +
        IntToStr(DBGrid.SelectedRows.Count) + ' itens selecionados?') = ID_YES
      else
        Executa := True;
      if Executa then
      begin
        with DBGrid.DataSource.DataSet do
        for n := 0 to DBGrid.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGrid.SelectedRows.Items[n]);
          GotoBookmark(DBGrid.SelectedRows.Items[n]);
          if ExecutaSQL() then inc(Afetados, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Pergunta then Executa :=
        Geral.MB_Pergunta('Confirma a altera��o de todos os ' +
        IntToStr(QrDados.RecordCount) + ' itens pesquisados?') = ID_YES
      else
        Executa := True;
      if Executa then
      begin
        QrDados.First;
        while not QrDados.Eof do
        begin
          if ExecutaSQL() then inc(Afetados, 1);
          QrDados.Next;
        end;
      end;
    end;
  end;
  if Afetados > 0 then
  begin
    Result := True;
    QrDados.Close;
    UnDmkDAC_PF.AbreQuery(QrDados, Dmod.MyDB);
    if AvisaAlterados then
    begin
      if Afetados = 1 then Geral.MB_Info('Um item foi alterado!') else
      Geral.MB_Info(IntToStr(Afetados) + ' itens foram alterados!');
    end;
  end;
  Screen.Cursor := crDefault;
end;

// GetData, GetDataHora, ObtemDataHora
function TMyDBCheck.ObtemData(const DtDefault: TDateTime; var DtSelect:
  TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
  const HabilitaHora: Boolean = False; Titulo: String = ''): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData) then
  begin
    with FmGetData do
    begin
      FMinData        := MinData;
      // ini 2024-01-04
      //TPData.Date     := DtDefault;
      TPData.MinDate     := MinData;
      try
        TPData.Date     := DtDefault;
      except
        TPData.Date     := MinData;
      end;
      // fim 2024-01-04
      LaMulta.Visible := False;
      EdMulta.Visible := False;
      LaJuros.Visible := False;
      EdJuros.Visible := False;
      // 2011-05-21
      EdHora.ValueVariant := HrDefault;
      EdHora.Enabled := HabilitaHora;
      LaHora.Enabled := HabilitaHora;
      // Fim 2011-05-21
      // 2014-11-26
      FTitulo := Titulo;
      // FIM 2014-11-26
      ShowModal;
      if FSelecionou then
      begin
        DtSelect := Int(TPData.Date) + EdHora.ValueVariant;
        Result := True;
      end;
      Destroy;
    end;
  end;
end;

function TMyDBCheck.ObtemHora(const HrDefault: TTime; var HrSelect:
  TTime; Titulo: String = ''): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetHora, FmGetHora) then
  begin
    VAR_GET_SO_HORA := HrDefault;
    with FmGetHora do
    begin
      ShowModal;
      if FSelecionou then
      begin
        HrSelect := EdHora.ValueVariant;
        Result := True;
      end;
      Destroy;
    end;
  end;
end;

function TMyDBCheck.ObtemVersao(var VerZero, Versao: Int64): Boolean;
const
  sMsg = 'N�o foi poss�vel obter a vers�o do aplicativo salva no registro do Windows!';
begin
  Result := False;
  try
    // ini 2022-01-10
    // Em 2022 foi atingido o limete m�ximo do DataType Integer (2147483647),
    // ent�o mudei a vers�o de de Integer para String,
    //VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
    //Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger, CO_VERSAO, HKEY_LOCAL_MACHINE);
    {
    try
      VerZero := StrToInt64(Geral.ReadAppKey('Versao', Application.Title, ktString,
        0, HKEY_LOCAL_MACHINE));
    except}
      //VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
      {$ifDef VER350} // Delphi 28 Alexandria
        VerZero := Geral.I64(Geral.ReadAppKey('Vers28', Application.Title, ktString, '0', HKEY_LOCAL_MACHINE));
      {$Else}
        try
          VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0, HKEY_LOCAL_MACHINE);
        except
          on E: exception do Geral.ErroVersaoDoCompilador(sMsg, E.Message);
        end;
      {$EndIf}
    //end;
    {try
      Versao := StrToInt64(Geral.ReadAppKey('Versao', Application.Title, ktString,
        CO_VERSAO, HKEY_LOCAL_MACHINE));
    except}
      //Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger, CO_VERSAO, HKEY_LOCAL_MACHINE);
      {$ifDef VER350} // Delphi 28 Alexandria
        Versao := Geral.I64(Geral.ReadAppKey('Versao', Application.Title, ktString, CO_VERSAO, HKEY_LOCAL_MACHINE));
      {$Else}
        try
          Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger, CO_VERSAO, HKEY_LOCAL_MACHINE);
        except
          on E: exception do Geral.ErroVersaoDoCompilador(sMsg, E.Message);
        end;
      {$EndIf}
    //end;
    Result := True;
  finally
    if Result = False then
      Geral.MB_Aviso('N�o foi poss�vel obter a vers�o do registro do Windows!');
  end;
end;

function TMyDBCheck.ObtemDataEUser(const UserDefault: Integer;
  var UserSelect: Integer; const DtDefault: TDateTime; var DtSelect:
  TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
  const HabilitaHora: Boolean = False): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetDataEUser, FmGetDataEUSer) then
  begin
    with FmGetDataEUser do
    begin
      FMinData        := MinData;
      if TPData.MinDate < FMinData then
        TPData.MinDate := FMinData;
      //  
      if DtDefault >= TPData.MinDate then
        TPData.Date     := DtDefault
      else
      if Date >= TPData.MinDate then
        TPData.Date     := Date
      else
        TPData.Date     := TPData.MinDate;
      //
      EdUsuario.ValueVariant := UserDefault;
      CBUsuario.KeyValue     := UserDefault;
      //
      EdHora.ValueVariant := HrDefault;
      EdHora.Enabled := HabilitaHora;
      LaHora.Enabled := HabilitaHora;
      ShowModal;
      if FSelecionou then
      begin
        DtSelect := Int(TPData.Date) + EdHora.ValueVariant;
        UserSelect := EdUsuario.ValueVariant;
        Result := True;
      end;
      Destroy;
    end;
  end;
end;

function TMyDBCheck.ObtemData_Juros_Multa(const MinData, DtDefault: TDateTime;
var DtSelect: TDateTime; const DefJuros, DefMulta: Double;
var Juros, Multa: Double): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData) then
  begin
    with FmGetData do
    begin
      FMinData             := MinData;
      if DtDefault >= MinData then
        TPData.Date        := DtDefault
      else
        TPData.Date        := MinData;
      LaMulta.Visible      := True;
      EdMulta.Visible      := True;
      LaJuros.Visible      := True;
      EdJuros.Visible      := True;
      EdMulta.ValueVariant := DefMulta;
      EdJuros.ValueVariant := DefJuros;
      ShowModal;
      if FSelecionou then
      begin
        DtSelect := TPData.Date;
        Juros    := EdJuros.ValueVariant;
        Multa    := EdMulta.ValueVariant;
        Result   := True;
      end;
      Destroy;
    end;
  end;
end;

function TMyDBCheck.AcessoAoForm(Form: String; ModoAcesso: TAcessFmModo): Integer;

  function ValidaModulo(Janela: String): Boolean;
  const
    ListaModulos: Array[0..2] of String = ('UMED', 'PRAZ', 'SPED');
  var
    Modulo: String;
  begin
{
    if CO_DMKID_APP <> 17 then
    begin
      DModG.QrMaster.Close;
      UnDmkDAC_PF.AbreQuery(DModG.QrMaster, Dmod.MyDB);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT Modulo ',
      'FROM perfjan ',
      'WHERE UPPER(Janela) = "'+ UpperCase(Janela) +'" ',
      '']);
      if DModG.QrAux.RecordCount > 0 then
      begin
        Modulo := DModG.QrAux.FieldByName('Modulo').AsString;
        //
        if not AnsiMatchStr(UpperCase(Modulo), ListaModulos) then
        begin
          if (Modulo = '') or (Pos(UpperCase(Modulo), UpperCase(DModG.QrMasterHabilModulos.Value)) > 0) then
            Result := True
          else
            Result := False;
        end else
          Result := True;
      end else
        Result := True;
      if not Result then
      begin
        Geral.MB_Aviso('Esta janela pertence ao m�dulo ' + Modulo + '.' +
          sLineBreak + 'E este m�dulo n�o est� habilitado!' + sLineBreak +
          'Caso deseje habilit�-lo solicite junto a Dermatek!');
      end;
    end else
}
      Result := True;
  end;

  function Privilegios(Usuario: Integer; ModoAcesso: TAcessFmModo): Boolean;
  begin
    FM_MASTER := 'F';
    DmodG.QrPerf.Close;
    DmodG.QrPerf.Params[0].AsInteger := Usuario;
    UnDmkDAC_PF.AbreQuery(DmodG.QrPerf, Dmod.MyDB);
    Result := DmodG.QrPerf.RecordCount > 0;
    if not Result and (ModoAcesso = afmoNegarComAviso) then
      Geral.MB_Aviso('N�o h� privil�gios para o usu�rio ' +
      IntToStr(VAR_USUARIO) + ' - ' + VAR_LOGIN + '!');
    // N�o pode fechar!!
    //DmodG.QrPerf.Close;
  end;
var
  Loc: Boolean;
begin
  if pos('_EXULT', Uppercase(TMeuDB)) > 0 then
  begin
    Result := 9;
    Exit;
  end;
  if ModoAcesso = afmoSoMaster then
  begin
    Result := 0;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then Result := 9
    else Geral.MB_Aviso('Acesso Restrito. Fale com a DERMATEK');
    Exit;
  end;
  if ModoAcesso = afmoSoAdmin then
  begin
    Result := 0;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) or
    ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    then Result := 9
    else Geral.MB_Aviso('Acesso Restrito. Fale com o administrador!');
    Exit;
  end;
  if ModoAcesso = afmoSoBoss then
  begin
    Result := 0;
    if ((VAR_SENHA = Uppercase(VAR_BOSSSENHA)) and (VAR_LOGIN =
      UpperCase(VAR_BOSSLOGIN))) or (VAR_SENHA = CO_MASTER) or
      ((VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    then
      Result := 9
    else
      Geral.MB_Aviso('Acesso Restrito. Fale com o respons�vel pela senha BOSS!');
      Exit;
  end;
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) or
    ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and
    (VAR_ADMIN <> '')) or (DmodG.QrMasterSolicitaSenha.Value = 0)
  then
    Result := 9
  else if ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN)) and
    (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then
  begin
    if (ValidaModulo(Form) = False) then
      Result := 0
    else
      Result := 8;
  end
  else begin
    Privilegios(VAR_USUARIO, ModoAcesso);
    Loc := DmodG.QrPerf.Locate('Janela', Form, [loCaseInsensitive]);
    if (DmodG.QrPerfOlha.Value = 0) or (Loc = False) then
    begin
      Result := 0;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MB_Aviso('Somente o administrador tem acesso a este formul�rio!')
      else begin
        case ModoAcesso of
          afmoNegarSemAviso: ;// nada;  Msg=-1
          afmoNegarComAviso: //  Msg = 0 ou 2
            Geral.MB_Aviso(VAR_ACESSONEGADO+sLineBreak+'['+
            Form+']');
          afmoSoBoss:
            Geral.MB_Aviso('Acesso permitido somente a gestor e administrador!');
          afmoSemVerificar: Result := 1;
          {
          afmoParcial:  // Msg = 1
            Geral.MB_('Login com acesso parcial!');
          afmoParcialSemEdicao:   // Msg = 3
            Geral.MB_('Login com acesso parcial, sem acesso a edi��o!');
          }
        end;
      end;
    end
    else if (ValidaModulo(Form) = False) then
      Result := 0
    else
      Result := 1;
  end;
end;

function TMyDBCheck.LiberaPelaSenhaBoss(Aviso: String = ''; SenhaExtra: String = ''): Boolean;
begin
  Result := MyObjects.LiberaPelaSenhaBoss(TFmSenha, FmSenha, Aviso, SenhaExtra);
end;

function TMyDBCheck.LiberaPelaSenhaMaster(Aviso: String): Boolean;
begin
  VAR_FSENHA := 1;
  if MyObjects.CriaForm_AcessoTotal(TFmSenha, FmSenha) then
  begin
    if Aviso <> '' then
      MyObjects.Informa2(FmSenha.LaAviso1, FmSenha.LaAviso2, False, Aviso);
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
end;

function TMyDBCheck.LiberaPelaSenhaPwdLibFunc(Aviso: String): Boolean;
begin
  VAR_FSENHA := 9;
  if MyObjects.CriaForm_AcessoTotal(TFmSenha, FmSenha) then
  begin
    if Aviso <> '' then
      MyObjects.Informa2(FmSenha.LaAviso1, FmSenha.LaAviso2, False, Aviso);
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
end;

function TMyDBCheck.LiberaPelaSenhaUsuario(Usuario: Integer;
  Aviso: String): Boolean;
begin
  Result := VAR_USUARIO = Usuario;
  if not Result then
    Result := LiberaPelaSenhaBoss(Aviso);
end;

function TMyDBCheck.LiberaPelaSenhaAdmin(Aviso: String = ''): Boolean;
begin
  VAR_FSENHA := 3;
  if MyObjects.CriaForm_AcessoTotal(TFmSenha, FmSenha) then
  begin
    if Aviso <> '' then
      MyObjects.Informa2(FmSenha.LaAviso1, FmSenha.LaAviso2, False, Aviso);
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
end;

function TMyDBCheck.Escolhe2CodigoUnicos(var Resultado1, Resultado2: Variant;
              const Aviso, Titulo, Prompt1, Prompt2: String;
              const DataSource1, DataSource2: TmySQLQuery; const ListSource1, ListSource2: TDataSource;
              const ListField1, ListField2: String; const Default1, Default2: Variant; const SQL1, SQL2: array of String;
              const DataBase: TmySQLDatabase; const PermiteSelZero: Boolean;
              const MostraSbCadastro: Boolean = False; const Tabela: String = '';
              const NovoCodigo: TNovoCodigo = TNovoCodigo.ncIdefinido): Boolean;
// ObtemCodigo
var
  Qry1, Qry2: TmySQLQuery;
  SQLSearch1, SQLSearch2, SQLs, FldCodi, FldNome, Liga: String;
  P, P1: Integer;
begin
  // Result := False;
  Result := False;
  //
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  Application.CreateForm(TFmSel2Cod, FmSel2Cod);
  FmSel2Cod.Informa(Aviso);
  FmSel2Cod.Caption             := Titulo;
  FmSel2Cod.LaPrompt1.Caption   := Prompt1;
  FmSel2Cod.LaPrompt2.Caption   := Prompt2;
  FmSel2Cod.FPermiteSelZero     := PermiteSelZero;
  FmSel2Cod.SbCadastro1.Visible := MostraSbCadastro;
  FmSel2Cod.SbCadastro2.Visible := MostraSbCadastro;
  FmSel2Cod.FTabela             := Tabela;
  FmSel2Cod.FNovoCodigo         := NovoCodigo;
  //
  if DataSource1 <> nil then
  begin
    Qry1 := DataSource1;
    Qry1.DataBase := DataBase;
    SQLSearch1 := DataSource1.SQL.Text;
    //
    Qry2 := DataSource2;
    Qry2.DataBase := DataBase;
    SQLSearch2 := DataSource2.SQL.Text;
  end else
  begin
    Qry1 := FmSel2Cod.QrSel1;
    SQLSearch1 := Geral.ATS(SQL1);
    //
    Qry2 := FmSel2Cod.QrSel2;
    SQLSearch2 := Geral.ATS(SQL2);
  end;
  //
  if DataBase <> nil then
  begin
    Qry1.Database := DataBase;
    Qry2.Database := DataBase;
  end;
  //
  if Length(SQL1) > 0 then
  begin
    Qry1.Close;
    Qry1.SQL.Text := Geral.ATS(SQL1);
    UnDmkDAC_PF.AbreQuery(Qry1, Qry1.Database);
  end;
  if Length(SQL2) > 0 then
  begin
    Qry2.Close;
    Qry2.SQL.Text := Geral.ATS(SQL2);
    UnDmkDAC_PF.AbreQuery(Qry2, Qry2.Database);
  end;
  //precisa ser antes
  if ListField1 <> '' then
    FmSel2Cod.CBSel1.ListField  := ListField1;
  if ListSource1 <> nil then
    FmSel2Cod.CBSel1.ListSource := ListSource1;
  //
  if ListField2 <> '' then
    FmSel2Cod.CBSel1.ListField  := ListField2;
  if ListSource2 <> nil then
    FmSel2Cod.CBSel2.ListSource := ListSource2;
  //
  if Default1 <> Null then
  begin
    FmSel2Cod.EdSel1.ValueVariant := Default1;
    FmSel2Cod.CBSel1.KeyValue     := Default1;
  end;
  if Default2 <> Null then
  begin
    FmSel2Cod.EdSel2.ValueVariant := Default2;
    FmSel2Cod.CBSel2.KeyValue     := Default2;
  end;
  // 2014-04-26
  SQLs := SQLSearch1;
  P := pos(Uppercase('ORDER BY'), Uppercase(SQLs));
  if P > 0 then
    SQLs := Copy(SQLs, 1, P -1);
  P  := pos(Uppercase('SELECT '), Uppercase(SQLs)) + Length('SELECT');
  P1 := pos(Uppercase('Descricao'), Uppercase(SQLs));
  FldCodi := Trim(Copy(SQLs, P, P1 - P));
  P  := pos(Uppercase('Codigo'), Uppercase(SQLs)) + Length('Codigo');
  P1 := pos(Uppercase('Descricao'), Uppercase(SQLs));
  FldNome := Trim(Copy(SQLs, P, P1 - P));
  P  := pos(Uppercase(','), FldNome);
  if P > 0 then
    FldNome := Trim(Copy(FldNome, P + 1));
  P  := pos(Uppercase('WHERE'), Uppercase(SQLs));
  if P > 0 then
    Liga := 'AND'
  else
    Liga := 'WHERE';
  SQLSearch1 := SQLs + sLineBreak + Liga + ' ' + FldNome + ' LIKE "%$#%" ';
  FmSel2Cod.CBSel1.LocF7SQLMasc := '$#';
  FmSel2Cod.CBSel1.LocF7SQLText.Text := SQLSearch1;
  FmSel2Cod.CBSel2.LocF7SQLMasc := '$#';
  FmSel2Cod.CBSel2.LocF7SQLText.Text := SQLSearch1;
  //Geral.MB_Info(SQLSearch);
  // FIM 2014-04-26
  FmSel2Cod.ShowModal;
  if FmSel2Cod.FSelected then
  begin
    Resultado1 := FmSel2Cod.EdSel1.ValueVariant;
    Resultado2 := FmSel2Cod.EdSel2.ValueVariant;
  end;
  FmSel2Cod.Destroy;
  VAR_CaptionFormOrigem := '';
  Result := FmSel2Cod.FSelected;
end;

function TMyDBCheck.EscolheCodigosMultiplos_0(Aviso, Titulo, Prompt: String;
  DataSource: TDataSource; FieldAtivo, FieldNivel, FieldNome: String; SQLExec,
  SQLOpen: array of String; QrExecSQL: TmySQLQuery): Boolean;
begin
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  Application.CreateForm(TFmSelCods, FmSelCods);
  MyObjects.Informa2(FmSelCods.LaAviso1, FmSelCods.LaAviso2, False, Aviso);
  FmSelCods.FQrExecSQL       := QrExecSQL;
  FmSelCods.Caption          := Titulo;
  FmSelCods.GBPrompt.Caption := Prompt;
  //
  if Length(SQLExec) > 0 then
  begin
{$IFNDEF NAO_CRIA_USER_DB}
    FmSelCods.FTempTab := UCriar.RecriaTempTableNovo(ntrttSelCods, DModG.QrUpdPID1, False);
          {$EndIf}
    DModG.QrSelCods.Close;
    //DModG.QrSelCods.SQL.Text := Geral.ATS(SQLExec);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrSelCods, DModG.MyPID_DB, SQLExec);
  end;
  if Length(SQLOpen) > 0 then
  begin
    DModG.QrSelCods.Close;
    DModG.QrSelCods.SQL.Text := Geral.ATS(SQLOpen);
    UnDmkDAC_PF.AbreQuery(DModG.QrSelCods, DModG.MyPID_DB);
  end;
  //precisa ser antes
  if FieldAtivo <> '' then
  begin
    FmSelCods.DBGSel.Columns[0].FieldName  := FieldAtivo;
    FmSelCods.FFldAtivo := FieldAtivo;
  end else
    FmSelCods.FFldAtivo := 'Ativo';

  FmSelCods.FFldUnique := FieldNivel;
  if FieldNivel <> '' then
    FmSelCods.DBGSel.Columns[1].FieldName  := FieldNivel;
  if FieldNome <> '' then
    FmSelCods.DBGSel.Columns[2].FieldName  := FieldNome;
  //
  if DataSource <> nil then
    FmSelCods.DBGSel.DataSource := DataSource;
  //
  FmSelCods.ShowModal;
  VAR_CaptionFormOrigem := '';
  Result := FmSelCods.FSelected;
  //
  FmSelCods.Destroy;
//
end;

(*
function TMyDBCheck.EscolheCodigosMultiplos_A(Aviso, Titulo, Prompt: String;
  DataSource: TDataSource; FieldAtivo, FieldNivel, FieldNome: String; SQLExec,
  SQLOpen: array of String; DestTab, DestMaster, DestDetail, DestSelec1: String;
  ValrMaster: Integer; QrDetail: TmySQLQuery; FieldDetail: String;
  ExcluiAnteriores: Boolean; QrExecSQL: TmySQLQuery): Boolean;
const
  Ativo = 1;
begin
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  Application.CreateForm(TFmSelCods, FmSelCods);
  MyObjects.Informa2(FmSelCods.LaAviso1, FmSelCods.LaAviso2, False, Aviso);
  FmSelCods.FQrExecSQL        := QrExecSQL;
  FmSelCods.FSelCodsExecution := sceExecA;
  FmSelCods.Caption           := Titulo;
  FmSelCods.GBPrompt.Caption  := Prompt;
  FmSelCods.FDestTab          := DestTab;
  FmSelCods.FValrMaster       := ValrMaster;
  FmSelCods.FDestMaster       := DestMaster;
  FmSelCods.FDestDetail       := DestDetail;
  FmSelCods.FDestSelec1       := DestSelec1;
  FmSelCods.FQrDetail         := QrDetail;
  FmSelCods.FExcluiAnteriores := ExcluiAnteriores;
  //
  if Length(SQLExec) > 0 then
  begin
    FmSelCods.FTempTab := UCriar.RecriaTempTableNovo(ntrttSelCods, DModG.QrUpdPID1, False);
    DModG.QrSelCods.Close;
    //DModG.QrSelCods.SQL.Text := Geral.ATS(SQLExec);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrSelCods, DModG.MyPID_DB, SQLExec);
    //

    if (QrDetail <> nil) and (QrDetail.State <> dsInactive) then
    begin
      QrDetail.First;
      while not QrDetail.Eof do
      begin
        if ExcluiAnteriores then
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_selcods_', False, [
          FieldAtivo], [
          FieldNivel], [
          Ativo], [QrDetail.FieldByName(FieldDetail).AsInteger], False);
        end else
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
          DELETE_FROM + ' _selcods_ ',
          'WHERE ' + FieldNivel + '=' + Geral.FF0(
          QrDetail.FieldByName(FieldDetail).AsInteger),
          '']);
        end;
        //
        QrDetail.Next;
      end;
    end;
  end;
  if Length(SQLOpen) > 0 then
  begin
    DModG.QrSelCods.Close;
    DModG.QrSelCods.SQL.Text := Geral.ATS(SQLOpen);
    UnDmkDAC_PF.AbreQuery(DModG.QrSelCods, DModG.MyPID_DB);
  end;
  //precisa ser antes
  if FieldAtivo <> '' then
  begin
    FmSelCods.DBGSel.Columns[0].FieldName  := FieldAtivo;
    FmSelCods.FFldAtivo := FieldAtivo;
  end else
    FmSelCods.FFldAtivo := 'Ativo';

  FmSelCods.FFldUnique := FieldNivel;
  if FieldNivel <> '' then
    FmSelCods.DBGSel.Columns[1].FieldName  := FieldNivel;
  if FieldNome <> '' then
    FmSelCods.DBGSel.Columns[2].FieldName  := FieldNome;
  //
  if DataSource <> nil then
    FmSelCods.DBGSel.DataSource := DataSource;
  //
  FmSelCods.ShowModal;
  VAR_CaptionFormOrigem := '';
  Result := FmSelCods.FSelected;
  //
  FmSelCods.Destroy;
//
end;
*)

function TMyDBCheck.EscolheCodigosMultiplos_A(Aviso, Titulo, Prompt: String;
  DataSource: TDataSource; FieldAtivo, FieldNivel, FieldNome: String; SQLExec,
  SQLOpen: array of String; DestTab: String; DestMaster: array of String;
  DestDetail, DestSelec1: String;
  ValrMaster: array of Variant;
  QrDetail: TmySQLQuery; FieldDetail: String;
  ExcluiAnteriores: Boolean; QrExecSQL: TmySQLQuery): Boolean;
const
  Ativo = 1;
var
  I: Integer;
begin
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  if Length(ValrMaster) <> Length(DestMaster) then
  begin
    Geral.MB_Aviso(
    'Quantidade de campos "Master" difere da quantidade de seus valores!' +
    sLineBreak + '"TMyDBCheck.EscolheCodigosMultiplos_A()"');
    Exit;
  end;
  //
  Application.CreateForm(TFmSelCods, FmSelCods);
  MyObjects.Informa2(FmSelCods.LaAviso1, FmSelCods.LaAviso2, False, Aviso);
  FmSelCods.FQrExecSQL        := QrExecSQL;
  FmSelCods.FSelCodsExecution := sceExecA;
  FmSelCods.Caption           := Titulo;
  FmSelCods.GBPrompt.Caption  := Prompt;
  FmSelCods.FDestTab          := DestTab;
  //
  SetLength(FmSelCods.FValrMaster, Length(ValrMaster));
  SetLength(FmSelCods.FDestMaster, Length(DestMaster));
  for I := 0 to Length(ValrMaster) -1 do
  begin
    FmSelCods.FValrMaster[I]  := ValrMaster[I];
    FmSelCods.FDestMaster[I]  := DestMaster[I];
  end;
  FmSelCods.FDestDetail       := DestDetail;
  FmSelCods.FDestSelec1       := DestSelec1;
  FmSelCods.FQrDetail         := QrDetail;
  FmSelCods.FExcluiAnteriores := ExcluiAnteriores;
  //
  if Length(SQLExec) > 0 then
  begin
 {$IFNDEF NAO_CRIA_USER_DB}
   FmSelCods.FTempTab := UCriar.RecriaTempTableNovo(ntrttSelCods, DModG.QrUpdPID1, False);
{$EndIf}
    DModG.QrSelCods.Close;
    //DModG.QrSelCods.SQL.Text := Geral.ATS(SQLExec);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrSelCods, DModG.MyPID_DB, SQLExec);
    //

    if (QrDetail <> nil) and (QrDetail.State <> dsInactive) then
    begin
      QrDetail.First;
      while not QrDetail.Eof do
      begin
        if ExcluiAnteriores then
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_selcods_', False, [
          FieldAtivo], [
          FieldNivel], [
          Ativo], [QrDetail.FieldByName(FieldDetail).AsInteger], False);
        end else
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
          DELETE_FROM + ' _selcods_ ',
          'WHERE ' + FieldNivel + '=' + Geral.FF0(
          QrDetail.FieldByName(FieldDetail).AsInteger),
          '']);
        end;
        //
        QrDetail.Next;
      end;
    end;
  end;
  if Length(SQLOpen) > 0 then
  begin
    DModG.QrSelCods.Close;
    DModG.QrSelCods.SQL.Text := Geral.ATS(SQLOpen);
    UnDmkDAC_PF.AbreQuery(DModG.QrSelCods, DModG.MyPID_DB);
  end;
  //precisa ser antes
  if FieldAtivo <> '' then
  begin
    FmSelCods.DBGSel.Columns[0].FieldName  := FieldAtivo;
    FmSelCods.FFldAtivo := FieldAtivo;
  end else
    FmSelCods.FFldAtivo := 'Ativo';

  FmSelCods.FFldUnique := FieldNivel;
  if FieldNivel <> '' then
    FmSelCods.DBGSel.Columns[1].FieldName  := FieldNivel;
  if FieldNome <> '' then
    FmSelCods.DBGSel.Columns[2].FieldName  := FieldNome;
  //
  if DataSource <> nil then
    FmSelCods.DBGSel.DataSource := DataSource;
  //
  FmSelCods.ShowModal;
  VAR_CaptionFormOrigem := '';
  Result := FmSelCods.FSelected;
  //
  FmSelCods.Destroy;
//
end;

// SelecionaCodigo Seleciona Codigo GetCodigo
function TMyDBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt: String;
              DataSource: TmySQLQuery; ListSource: TDataSource;
              ListField: String; Default: Variant; SQL: array of String;
              DataBase: TmySQLDatabase; PermiteSelZero: Boolean;
              MostraSbCadastro: Boolean = False; Tabela: String = '';
              NovoCodigo: TNovoCodigo = TNovoCodigo.ncIdefinido): Variant;
// ObtemCodigo
var
  Qry: TmySQLQuery;
  SQLSearch, SQLs, FldCodi, FldNome, Liga: String;
  P, P1: Integer;
begin
  // Result := False;
  Result := Null;
  //
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  Application.CreateForm(TFmSelCod, FmSelCod);
  FmSelCod.Informa(Aviso);
  FmSelCod.Caption            := Titulo;
  FmSelCod.LaPrompt.Caption   := Prompt;
  FmSelCod.FPermiteSelZero    := PermiteSelZero;
  FmSelCod.SbCadastro.Visible := MostraSbCadastro;
  FmSelCod.FTabela            := Tabela;
  FmSelCod.FNovoCodigo        := NovoCodigo;
  //
  if DataSource <> nil then
  begin
    Qry := DataSource;
    Qry.DataBase := DataBase;
    SQLSearch := DataSource.SQL.Text;
  end else
  begin
    Qry := FmSelCod.QrSel;
    SQLSearch := Geral.ATS(SQL);
  end;
  //
  if DataBase <> nil then
    Qry.Database := DataBase;
  //
  if Length(SQL) > 0 then
  begin
    Qry.Close;
    Qry.SQL.Text := Geral.ATS(SQL);
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  end;
  //precisa ser antes
  if ListField <> '' then
    FmSelCod.CBSel.ListField  := ListField;
  if ListSource <> nil then
    FmSelCod.CBSel.ListSource := ListSource;
  //
  if Default <> Null then
  begin
    FmSelCod.EdSel.ValueVariant := Default;
    FmSelCod.CBSel.KeyValue     := Default;
  end;
  // 2014-04-26
  SQLs := SQLSearch;
  P := pos(Uppercase('ORDER BY'), Uppercase(SQLs));
  if P > 0 then
    SQLs := Copy(SQLs, 1, P -1);
  P  := pos(Uppercase('SELECT '), Uppercase(SQLs)) + Length('SELECT');
  P1 := pos(Uppercase('Descricao'), Uppercase(SQLs));
  FldCodi := Trim(Copy(SQLs, P, P1 - P));
  P  := pos(Uppercase('Codigo'), Uppercase(SQLs)) + Length('Codigo');
  P1 := pos(Uppercase('Descricao'), Uppercase(SQLs));
  FldNome := Trim(Copy(SQLs, P, P1 - P));
  P  := pos(Uppercase(','), FldNome);
  if P > 0 then
    FldNome := Trim(Copy(FldNome, P + 1));
  P  := pos(Uppercase('WHERE'), Uppercase(SQLs));
  if P > 0 then
    Liga := 'AND'
  else
    Liga := 'WHERE';
  SQLSearch := SQLs + sLineBreak + Liga + ' ' + FldNome + ' LIKE "%$#%" ';
  FmSelCod.CBSel.LocF7SQLMasc := '$#';
  FmSelCod.CBSel.LocF7SQLText.Text := SQLSearch;
  //Geral.MB_Info(SQLSearch);
  // FIM 2014-04-26
  FmSelCod.ShowModal;
  if FmSelCod.FSelected then
    Result := FmSelCod.EdSel.ValueVariant;
  FmSelCod.Destroy;
  VAR_CaptionFormOrigem := '';
end;

function TMyDBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt: String;
  DataSource: TDataSource(*; ListSource: TDataSource; ListField: String;
  Default: Variant; SQL: array of String; DataBase: TmySQLDatabase;
  PermiteSelZero: Boolean*); MultiSelect, Destroi: Boolean;
  BtOKVisible: Boolean = True; BtTodosVisible: Boolean = True;
  BtNenhumVisible: Boolean = True): Boolean;
var
  Qry: TmySQLQuery;
  SQLSearch, SQLs, FldCodi, FldNome, Liga: String;
  P, P1: Integer;
begin
  // Result := False;
  Result := False;
  //
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
  FmGerlShowGrid.Informa(Aviso);
  FmGerlShowGrid.FTitulo          := 'XXX-XXXXX-014 :: ' + Titulo;
  FmGerlShowGrid.Caption          := FmGerlShowGrid.FTitulo;
  FmGerlShowGrid.LaAviso1.Caption := Prompt;
  FmGerlShowGrid.LaAviso2.Caption := Prompt;
  //FmGerlShowGrid.LaPrompt.Caption := Prompt;
  FmGerlShowGrid.BtOK.Visible := BtOKVisible;
  FmGerlShowGrid.BtTodos.Visible := BtTodosVisible;
  FmGerlShowGrid.BtNenhum.Visible := BtNenhumVisible;
  FmGerlShowGrid.MeAvisos.Text := Aviso;

(*
  FmGerlShowGrid.FPermiteSelZero  := PermiteSelZero;
  //
  //
  if DataBase <> nil then
    Qry.Database := DataBase;
  //
  if Length(SQL) > 0 then
  begin
    Qry.Close;
    Qry.SQL.Text := Geral.ATS(SQL);
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  end;
  //precisa ser antes
  if ListField <> '' then
    FmGerlShowGrid.CBSel.ListField  := ListField;
*)
  if DataSource <> nil then
    FmGerlShowGrid.DBGSel.DataSource := DataSource;
  //
(*
  if Default <> Null then
  begin
    FmGerlShowGrid.EdSel.ValueVariant := Default;
    FmGerlShowGrid.CBSel.KeyValue     := Default;
  end;
  SQLs := SQLSearch;
  P := pos(Uppercase('ORDER BY'), Uppercase(SQLs));
  if P > 0 then
    SQLs := Copy(SQLs, 1, P -1);
  P  := pos(Uppercase('SELECT '), Uppercase(SQLs)) + Length('SELECT');
  P1 := pos(Uppercase('Descricao'), Uppercase(SQLs));
  FldCodi := Trim(Copy(SQLs, P, P1 - P));
  P  := pos(Uppercase('Codigo'), Uppercase(SQLs)) + Length('Codigo');
  P1 := pos(Uppercase('Descricao'), Uppercase(SQLs));
  FldNome := Trim(Copy(SQLs, P, P1 - P));
  P  := pos(Uppercase(','), FldNome);
  if P > 0 then
    FldNome := Trim(Copy(FldNome, P + 1));
  P  := pos(Uppercase('WHERE'), Uppercase(SQLs));
  if P > 0 then
    Liga := 'AND'
  else
    Liga := 'WHERE';
  SQLSearch := SQLs + sLineBreak + Liga + ' ' + FldNome + ' LIKE "%$#%" ';
*)
(*
  FmGerlShowGrid.CBSel.LocF7SQLMasc := '$#';
  FmGerlShowGrid.CBSel.LocF7SQLText.Text := SQLSearch;
*)
  if MultiSelect then
    FmGerlShowGrid.DBGSel.Options := FmGerlShowGrid.DBGSel.Options + [dgMultiSelect];
  //
  FmGerlShowGrid.FormResize(FmGerlShowGrid);
  FmGerlShowGrid.MeAvisosChange(Self);
  FmGerlShowGrid.ShowModal;
(*
  if FmGerlShowGrid.FSelected then
    Result := FmGerlShowGrid.EdSel.ValueVariant;
*)
  Result := FmGerlShowGrid.FSelecionou;
  if Destroi then
    FmGerlShowGrid.Destroy;
  VAR_CaptionFormOrigem := '';
end;

// SelecionaTexto Seleciona Texto DefineTexto ObtemTexto
// SelecionaString Seleciona String DefineString ObtemString
function TMyDBCheck.EscolheTextoUnico(Aviso, Titulo, Prompt: String;
  DataSource: TmySQLQuery; ListSource: TDataSource; ListField: String;
  Default: Variant; SQL: array of String; DataBase: TmySQLDatabase;
  PermiteVazio: Boolean; LocF7CodiFldName, LocF7NameFldName: String): Variant;
var
  Qry: TmySQLQuery;
begin
  Result := Null;
  //
  if Screen.ActiveForm <> nil then
    VAR_CaptionFormOrigem := TForm(Screen.ActiveForm).Caption;
  //
  Application.CreateForm(TFmSelStr, FmSelStr);
  FmSelStr.Informa(Aviso);
  FmSelStr.Caption          := Titulo;
  FmSelStr.LaPrompt.Caption := Prompt;
  FmSelStr.FPermiteVazio  := PermiteVazio;
  //
  if DataSource <> nil then
  begin
    Qry := DataSource;
    Qry.DataBase := DataBase;
  end else
    Qry := FmSelStr.QrSel;
  //
  if DataBase <> nil then
    Qry.Database := DataBase;
  //
  if Length(SQL) > 0 then
  begin
    Qry.Close;
    Qry.SQL.Text := Geral.ATS(SQL);
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  end;
  //precisa ser antes
  if ListField <> '' then
    FmSelStr.CBSel.ListField  := ListField;
  if ListSource <> nil then
    FmSelStr.CBSel.ListSource := ListSource;
  //
  if Default <> Null then
  begin
    FmSelStr.CBSel.KeyValue     := Default;
  end;
  FmSelStr.CBSel.LocF7CodiFldName := LocF7CodiFldName; //'CodTxt';
  FmSelStr.CBSel.LocF7NameFldName := LocF7NameFldName; //'Nome';

  FmSelStr.ShowModal;
  if FmSelStr.FSelected then
    Result := FmSelStr.CBSel.KeyValue;
  FmSelStr.Destroy;
  VAR_CaptionFormOrigem := '';
end;

function TMyDBCheck.ObtemCodUsuZStepCodUni(Tabela: TTabStepCod): Integer;
var
  NomeTab, DescriTab: String;
  //Mostra: Boolean;
begin
  Result := 0;
  //Mostra := true;
  case Tabela of
    tscEntidades:
    begin
      NomeTab := 'entidades';
      DescriTab := 'Cadastro de Pessoas F�sicas e Jur�dicas';
    end;
    tscEtqPrinCad:
    begin
      NomeTab := 'etqprincad';
      DescriTab := 'Cadastro Impressoras de Etiquetas';
    end;
    {
    N�o precisa
    else begin
      Mostra := false;

    end;
    }
  end;
  if DBCheck.CriaFm(TFmZStepCodUni, FmZStepCodUni, afmoNegarComAviso) then
  begin
    FmZStepCodUni.EdTabela.Text    := NomeTab;
    FmZStepCodUni.EdTabDescri.Text := DescriTab;
    FmZStepCodUni.ReopenZStepCod;
    FmZStepCodUni.ShowModal;
    Result := FmZStepCodUni.FResult;
    FmZStepCodUni.Destroy;
  end;
end;

function TMyDBCheck.Obtem_verProc: String;
begin
  Result := CO_SIGLA_APP + '_' + FormatFloat('0', CO_VERSAO)
end;


procedure TMyDBCheck.PreencheCheckGroupClieForn(CGClie, CGForn: TCustomCheckGroup);
var
  I: Integer;
begin
{$IfNDef SemEntidade}
  // Clientes
  if CGClie <> nil then
  begin
    THackCheckGroup(CGClie).Items.Clear;
    for I := 1 to _MaxClieSets do
      THackCheckGroup(CGClie).Items.Add(Geral.FFN(I, 2) + '. ' + _ArrClieSets[I]);
    I := (_MaxClieSets div 5) + 1;
    if I > 6 then I := 6;
    THackCheckGroup(CGClie).Columns := I;
  end;
  // Fornecedores
  if CGForn <> nil then
  begin
    THackCheckGroup(CGForn).Items.Clear;
    for I := 1 to _MaxFornSets do
      THackCheckGroup(CGForn).Items.Add(Geral.FFN(I, 2) + '. ' + _ArrFornSets[I]);
    I := (_MaxFornSets div 5) + 1;
    if I > 6 then I := 6;
    THackCheckGroup(CGForn).Columns := I;
  end;
{$EndIf}
end;

{
   Substitui��es:

   ATEN��O: Colocar var antes de "TemControle: TTemControle"
   TemControle := True  -> TemControle := TemControle + cTemControleSim;
   TemControle := False -> TemControle := TemControle + cTemControleNao;

}

end.

