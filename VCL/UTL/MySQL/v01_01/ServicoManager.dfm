object FmServicoManager: TFmServicoManager
  Left = 362
  Top = 201
  Caption = 'XXX-XXXXX-999 :: Gerenciador de Servi'#231'os  [Windows]'
  ClientHeight = 341
  ClientWidth = 648
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 648
    Height = 179
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 100
    object Panel1: TPanel
      Left = 0
      Top = 64
      Width = 648
      Height = 115
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Servi'#231'o:'
      end
      object Label2: TLabel
        Left = 8
        Top = 52
        Width = 85
        Height = 13
        Caption = 'Status do servi'#231'o:'
      end
      object EdServico: TEdit
        Left = 8
        Top = 20
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object ProgressBar1: TProgressBar
        Left = 136
        Top = 22
        Width = 509
        Height = 17
        TabOrder = 1
      end
      object Edit1: TEdit
        Left = 8
        Top = 68
        Width = 637
        Height = 21
        TabStop = False
        Color = clBtnFace
        Enabled = False
        TabOrder = 2
      end
    end
    object RGServico: TRadioGroup
      Left = 0
      Top = 0
      Width = 648
      Height = 53
      Align = alTop
      Caption = 'Servi '#231'o: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Nenhum'
        'MySQL (4.x, 5.x, etc)'
        'CodeBase (Database server)')
      TabOrder = 1
      OnClick = RGServicoClick
      ExplicitLeft = 4
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 227
    Width = 648
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 148
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 271
    Width = 648
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 192
    object PnSaiDesis: TPanel
      Left = 520
      Top = 15
      Width = 126
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 0
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 518
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtStatus: TBitBtn
        Tag = 131
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Status'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtStatusClick
      end
      object BtAtivar: TBitBtn
        Tag = 129
        Left = 127
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Ativar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAtivarClick
      end
      object BtDesativar: TBitBtn
        Tag = 130
        Left = 250
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Desativar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtDesativarClick
      end
      object BtConfig: TBitBtn
        Tag = 130
        Left = 374
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Configurar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtConfigClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 648
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 600
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 552
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 443
        Height = 32
        Caption = 'Gerenciador de Servi'#231'os  [Windows]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 443
        Height = 32
        Caption = 'Gerenciador de Servi'#231'os  [Windows]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 443
        Height = 32
        Caption = 'Gerenciador de Servi'#231'os  [Windows]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DB: TMySQLDatabase
    DatabaseName = 'mysql'
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=mysql'
      'UID=root'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 256
    Top = 72
  end
  object Qr: TMySQLQuery
    Database = DB
    SQL.Strings = (
      'INSERT INTO user SET Password=PASSWORD('#39'wkljweryhvbirt'#39'), '
      'Select_priv='#39'Y'#39', Insert_priv='#39'Y'#39', '
      'Update_priv='#39'Y'#39', Delete_priv='#39'Y'#39', Create_priv='#39'Y'#39', '
      'Drop_priv='#39'Y'#39', Reload_priv='#39'Y'#39', Shutdown_priv='#39'Y'#39', '
      'Process_priv='#39'Y'#39', File_priv='#39'Y'#39', Grant_priv='#39'Y'#39', '
      'References_priv='#39'Y'#39', Index_priv='#39'Y'#39', Alter_priv='#39'Y'#39', '
      'Show_db_priv='#39'Y'#39', Super_priv='#39'Y'#39', Create_tmp_table_priv='#39'Y'#39', '
      'Lock_tables_priv='#39'Y'#39', Execute_priv='#39'Y'#39', Repl_slave_priv='#39'Y'#39', '
      'Repl_client_priv='#39'Y'#39';')
    Left = 284
    Top = 72
  end
end
