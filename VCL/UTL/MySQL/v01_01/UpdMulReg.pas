unit UpdMulReg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, mySQLDbTables, dmkDBGridZTO;

type
  TFmUpdMulReg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GBSel: TGroupBox;
    LaSel: TLabel;
    CBSel: TdmkDBLookupComboBox;
    EdSel: TdmkEditCB;
    QrSel: TmySQLQuery;
    QrSelCodigo: TIntegerField;
    QrSelNome: TWideStringField;
    DsSel: TDataSource;
    QrSelCodUsu: TIntegerField;
    DBGPsq: TdmkDBGridZTO;
    MeHelp: TMemo;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdSelChange(Sender: TObject);
    procedure DBGPsqAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure BtOKClick(Sender: TObject);
    procedure QrPsqAfterOpen(DataSet: TDataSet);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
  private
    { Private declarations }
    procedure StatusBotao();
  public
    { Public declarations }
    FTabela, FFldUpdNom, FFldIdxNom: String;
    FUserDataAlterweb: Boolean;
  end;

  var
  FmUpdMulReg: TFmUpdMulReg;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmUpdMulReg.BtNenhumClick(Sender: TObject);
begin
  DBGPsq.SelectedRows.Clear;
  StatusBotao();
end;

procedure TFmUpdMulReg.BtOKClick(Sender: TObject);
const
  MaxN = 100;
var
  NewVal: Variant;
  IdxVal: Variant;
  I, N: Integer;
  IdxValTxt, NewValTxt: String;
  //
  procedure ExecutaEmLote();
  begin
    (*UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabela, False, [
    FFldUpdNom], [FFldIdxNom], [NewVal], [IdxVal], FUserDataAlterweb);
    *)
    IdxValTxt := Copy(IdxValTxt, 2);
    Dmod.MyDB.Execute('UPDATE ' + FTabela + sLineBreak +
    'SET ' + FFldUpdNom + '=' + NewValTxt + slineBreak +
    'WHERE ' + FFldIdxNom + ' IN (' + IdxValTxt + ')');
    //
    N := 0;
    IdxValTxt := '';
  end;
begin
  if Geral.MB_Pergunta(
  'Confirma a atualização dos registros selecionados?') = ID_YES then
  begin
    PB1.Max := DBGPsq.SelectedRows.Count;
    NewVal := EdSel.ValueVariant;
    if DBGPsq.SelectedRows.Count < 10 then
    begin
      with DBGPsq.DataSource.DataSet do
      for I := 0 to DBGPsq.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGPsq.SelectedRows.Items[I]));
        GotoBookmark(DBGPsq.SelectedRows.Items[I]);
        //
        IdxVal := QrPsq.FieldByName(FFldIdxNom).AsExtended;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabela, False, [
        FFldUpdNom], [FFldIdxNom], [NewVal], [IdxVal], FUserDataAlterweb);
        //
        MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      end;
    end else
    begin
      QrPsq.DisableControls;
      //
      try
        NewValTxt := Geral.FF0(NewVal);
        IdxValTxt := '';
        N         := 0;
        //
        with DBGPsq.DataSource.DataSet do
        for I := 0 to DBGPsq.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGPsq.SelectedRows.Items[I]));
          GotoBookmark(DBGPsq.SelectedRows.Items[I]);
          //
          N := N + 1;
          IdxValTxt := IdxValTxt +  ',' + QrPsq.FieldByName(FFldIdxNom).AsString + sLineBreak;
          //
          if N >= MaxN then
            ExecutaEmLote();
          //
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
        end;
        if N > 0 then
          ExecutaEmLote();
      finally
        QrPsq.EnableControls;
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrPsq, QrPsq.DataBase);
  end;
end;

procedure TFmUpdMulReg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUpdMulReg.BtTudoClick(Sender: TObject);
begin
  MyObjects.DBGridSelectAll(TDBGrid(DBGPsq));
  StatusBotao();
end;

procedure TFmUpdMulReg.DBGPsqAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  StatusBotao();
end;

procedure TFmUpdMulReg.EdSelChange(Sender: TObject);
begin
  StatusBotao();
end;

procedure TFmUpdMulReg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUpdMulReg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmUpdMulReg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUpdMulReg.QrPsqAfterOpen(DataSet: TDataSet);
var
  I: Integer;
begin
  for I := 1 to DBGPsq.Columns.Count -1 do
  begin
    if DBGPsq.Columns[I].Width > 140 then
      DBGPsq.Columns[I].Width := 140;
  end;
end;

procedure TFmUpdMulReg.StatusBotao();
begin
  BtOK.Enabled := (EdSel.ValueVariant <> 0) and (DBGPsq.SelectedRows.Count > 0);
end;

end.
