object FmRestaura2: TFmRestaura2
  Left = 342
  Top = 183
  Caption = 'FER-BACKP-102 :: Restaura Banco de Dados (2)'
  ClientHeight = 614
  ClientWidth = 780
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 780
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 323
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 780
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 323
      object Splitter1: TSplitter
        Left = 0
        Top = 225
        Width = 780
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 157
      end
      object Splitter2: TSplitter
        Left = 0
        Top = 295
        Width = 780
        Height = 5
        Cursor = crVSplit
        Align = alBottom
        ExplicitLeft = 12
        ExplicitTop = 228
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 780
        Height = 121
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 118
          Height = 13
          Caption = 'Arquivo a ser restaurado:'
        end
        object EdBackFile: TdmkEdit
          Left = 7
          Top = 20
          Width = 671
          Height = 20
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdBackFileChange
        end
        object BtAbrir: TBitBtn
          Left = 682
          Top = 4
          Width = 88
          Height = 39
          Hint = 'Confirma a senha digitada'
          Caption = '&Arquivo'
          NumGlyphs = 2
          TabOrder = 1
          Visible = False
          OnClick = BtAbrirClick
        end
        object RGDBs: TRadioGroup
          Left = 8
          Top = 44
          Width = 761
          Height = 73
          Caption = '  Banco de dados conectados: '
          Items.Strings = (
            'Pr'#233'-definido')
          TabOrder = 2
          OnClick = RGDBsClick
        end
      end
      object Memo1: TMemo
        Left = 0
        Top = 121
        Width = 780
        Height = 104
        Align = alTop
        TabOrder = 1
        ExplicitTop = 93
      end
      object Memo2: TMemo
        Left = 0
        Top = 230
        Width = 780
        Height = 65
        Align = alClient
        TabOrder = 2
        ExplicitTop = 200
        ExplicitHeight = 0
      end
      object Memo3: TMemo
        Left = 0
        Top = 300
        Width = 780
        Height = 103
        Align = alBottom
        OEMConvert = True
        TabOrder = 3
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 450
    Width = 780
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 370
    object LaStatus: TStaticText
      Left = 0
      Top = 0
      Width = 780
      Height = 47
      Align = alClient
      Caption = 'Pronto.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitTop = 20
      ExplicitHeight = 27
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 780
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 733
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 686
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 340
        Height = 31
        Caption = 'Restaura Banco de Dados (2)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 340
        Height = 31
        Caption = 'Restaura Banco de Dados (2)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 340
        Height = 31
        Caption = 'Restaura Banco de Dados (2)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 497
    Width = 780
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 417
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Progress1: TProgressBar
        Left = 0
        Top = 21
        Width = 776
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 552
    Width = 780
    Height = 62
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 472
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 45
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 634
        Top = 0
        Width = 142
        Height = 45
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Left = 10
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtDireto: TBitBtn
        Left = 8
        Top = 4
        Width = 118
        Height = 39
        Caption = '&OK'
        Enabled = False
        TabOrder = 1
        Visible = False
        OnClick = BtDiretoClick
      end
      object CkMostra: TCheckBox
        Left = 138
        Top = 16
        Width = 131
        Height = 17
        Caption = 'Mostra declara'#231#245'es.'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 2
        Visible = False
      end
      object BtParar: TBitBtn
        Left = 453
        Top = 4
        Width = 118
        Height = 39
        Hint = 'Confirma a senha digitada'
        Caption = '&Parar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
        OnClick = BtPararClick
      end
    end
  end
  object BeRestore_: TMySQLBatchExecute
    Action = baIgnore
    Database = Dmod.MyDB
    Delimiter = ';'
    OnAfterExecute = BeRestore_AfterExecute
    OnAfterStatement = BeRestore_AfterStatement
    OnProcess = BeRestore_Process
    Left = 69
    Top = 292
  end
end
