// ini Delphi 28 Alexandria
//{$I dmk.inc}
// fim Delphi 28 Alexandria
unit DmkDAC_PF;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, ShellAPI, TypInfo,
  comobj, ShlObj, Winsock, Math, About, UnMsgInt, mySQLDbTables, DB,
  Vcl.DBCtrls, System.Rtti, dmkEdit, dmkEditCB,
  mySQLExceptions, dmkGeral, UnInternalConsts, dmkDBGrid, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars, mySQLDirectQuery;

type
  TUnDmkDAC_PF = class(TObject)
  private
    {private declaration}
    // Usar AbreMySQLQuery0 qundo poss�vel!
    procedure MostraDirectQuery(Query: TmySQLDirectQuery; Aviso: String = '');
    procedure MostraQuery(Query: TmySQLQuery; Aviso: String = '');
    function  AbreMySQLQuery1(Query: TMySQLQuery;
              SQL: array of String; MostraConteudoErro: Boolean = True): Boolean;
    function  AbreMySQLDirectQuery1(Query: TmySQLDirectQuery;
              SQL: array of String): Boolean;
  public
    {public declaration}
    // ACOES EM TmySQLQuery e TmySQLTable
    procedure LeMeuSQL_Fixo_y(Query: TmySQLQuery; Arquivo: String; Memo: TMemo;
               WinArq, AskShow: Boolean);
    procedure LeMeuSQLForce_y(Query: TmySQLQuery; Arquivo: String; Memo: TMemo;
              WinArq, AskShow, Force: Boolean);
    procedure LeMeuSQLDirectForce_y(Query: TmySQLDirectQuery; Arquivo: String; Memo: TMemo;
              WinArq, AskShow, Force: Boolean);
    function  AbreQuery(Query: TmySQLQuery; DB: TmySQLDatabase;
              Aviso: String = ''; Silent: Boolean = False): Boolean;
    function  AbreQueryApenas(Query: TmySQLQuery; Aviso: String = ''): Boolean;
    function  AbreTable(Table: TmySQLTable; DB: TmySQLDatabase; Aviso: String = ''): Boolean;
    function  AbreMySQLDirectQuery0(Query: TmySQLDirectQuery; DB: TmySQLDatabase;
              SQL: array of String): Boolean;
    function  AbreMySQLQuery0(Query: TMySQLQuery; DB: TmySQLDatabase;
              SQL: array of String; MostraConteudoErro: Boolean = True): Boolean;
    function  AbreMySQLQuery2(Query: TMySQLQuery; DB: TmySQLDatabase;
              SQL: array of String; LaAviso1, LaAviso2: TLabel): Boolean;
    procedure ConectaMyDB_DAC(DB: TmySQLDataBase; DatabaseName, Host: String;
              Port: Integer; UserName, UserPassword: String; Desconecta,
              Configura, Conecta: Boolean; Silencioso: Boolean = False);
    procedure ConfiguracaoDB(DB: TmySQLDatabase);
    function  ExecutaMySQLQuery0(Query: TMySQLQuery; DB: TMySQLDatabase;
              SQL: array of String): Boolean;
    function  ExecutaQuery(Query: TmySQLQuery; DB: TmySQLDatabase;
              Aviso: String = ''): Boolean;
    function  ExecutaDB(DB: TmySQLDatabase; SQL: String): Boolean;
    // COMPONENTES
    procedure DesfazOrdenacaoDmkDBGrid(Grid: TDmkDBGrid; FldAtu: String);
    {$IfNDef sUSQLDB}
    function  TestarConexaoMySQL(Form: TForm; Host, User, Pass, BancoDados: String;
              Port: Integer; Avisa: Boolean): Boolean;
    {$EndIf}
    // QUERIES GENERICAS
    function  CodigosSeparadosPorVirgula_DeQuery(Qry: TmySQLQuery; Campo:
              String): String;
    function  CriaDataModule(const Owner: TComponent; const InstanceClass: TComponentClass; var Reference): Boolean;
    procedure ExcluiArquivosDeCampos();
    function  GetQrVal1Str(const Form: TForm; const Database: TmySQLDatabase;
              const SQL: array of String; var Valor: String): Integer;
    function  InsereRegistrosPorCopia(
              QrUpd: TmySQLQuery; Tabela, OriDB: String;
              FldSel: array of String; ValSel: array of Variant;
              FldAlt: array of String; ValAlt: array of Variant; FldAutoInc: String;
              UserDataAlterweb: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
    procedure MostraSQLSequncial(Qry: TmySQLQuery);
    procedure MostraSQLDirectSequncial(Qry: TmySQLDirectQuery);
    function  ObtemProximaOrdem(DB: TmySQLDataBase; Tabela, Campo: String;
              Para: TVaiPara; TipoDado: TAllFormat; Incremento: Variant;
              Base: Variant; FldIdx: array of String; ValIdx: array of Variant): Variant;
    function  ObtemNomeDeCodigo(Database: TmySQLDatabase; Tabela:
              String; Indice: Integer;
              CampoCod: String = 'Codigo'; CampoNome: String = 'Nome'): String;
    function  TextoJaCadastrado(Texto, Campo, Tabela: String; DB:
              TmySQLDataBase): Boolean;
    //Movido para UMySQLDB function  TabelaExiste(Tabela: String; Database: TmySQLDatabase): Boolean;
    procedure ReabrirTabelasFormAtivo(Sender: TObject);
    // Memory Table TABSQuery
    function AbreDataSetQuery(Query: TDataSet; SQL: array of String): Boolean;
    function  AtivaDesativaRegistroIdx1(Database: TmySQLDatabase; Tabela,
              CampoIdx, CampoAtivo: String; Codigo: Integer; Valor: Boolean):
              Boolean;
  end;

var
  UnDmkDAC_PF: TUnDmkDAC_PF;

implementation

uses
{$IfNDef sUSQLDB} UMySQLDB, {$EndIf}
  UnMyObjects;

{ TDmkDAC_PF }

function TUnDmkDAC_PF.AbreDataSetQuery(Query: TDataSet;
  SQL: array of String): Boolean;
var
  I: Integer;
  Texto: String;
  p: TRttiProperty;
  c: TRttiContext;
  t: TRttiType;
  OK: Boolean;
begin
  //Result := False;
  OK := False;
  Texto := '';
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) > 0 then
    begin
      for I := 0 to High(SQL) do
        Texto := Texto + SQL[I] + sLineBreak;
      try
        c := TRttiContext.Create;
        t := c.GetType(Query.ClassInfo);
        p := t.GetProperty('SQL');
        if p <> nil then
        begin
          if p.PropertyType.ToString = 'TStrings' then
          begin
            p.GetValue(Query).AsType<TStrings>.Text := Texto;
            OK := True;
          end;
        end;
      except
        EAbort.Create('Erro de RTTI ao setar SQL em DataSet!');
      end;
    end;
    if OK then
    begin
      Query. Open; // 2022-02-20 - Antigo . O p e n ;
      //
      Result := True;
    end else
      Geral.MB_Erro('A SQL n�o pode ser setada no DataSet!');
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MB_Erro('Erro ao tentar abrir uma DataSet.SQL!');
    dmkPF.LeMeuTexto(Texto);
    raise;
  end;
end;

function TUnDmkDAC_PF.AbreMySQLDirectQuery0(Query: TMySQLDirectQuery;
  DB: TmySQLDatabase; SQL: array of String): Boolean;
begin
  Query.Close;
  Query.Database := DB;
  //
  Result := AbreMySQLDirectQuery1(Query, SQL);
  //
  VAR_ABETURAS_QUERY_DIRECT := VAR_ABETURAS_QUERY_DIRECT + 1;
end;

function TUnDmkDAC_PF.AbreMySQLDirectQuery1(Query: TMySQLDirectQuery;
  SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
  Err: String;
begin
  //Result := False;
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no MySQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.Open();
      Query.First();
      Result := True;
    end;
    Screen.Cursor := MyCursor;
    MostraSQLDirectSequncial(Query);
  except
    Screen.Cursor := MyCursor;
    if pos('Compress', TmySQLDatabase(Query.Database).Name) < 0 then
      Err := sLineBreak + 'Database para "TMySQLDirectQuery" deve ser "Compress"'
    else
      Err := '';
    Geral.MB_Erro('Erro ao tentar abrir uma SQL no MySQLQuery!' + Err +
    sLineBreak + TComponent(Query.Owner).Name + '.' + Query.Name);
    UnDmkDAC_PF.LeMeuSQLDirectForce_y(Query, '', nil, True, True, True);
    raise;
  end;
end;

function TUnDmkDAC_PF.AbreMySQLQuery0(Query: TMySQLQuery; DB: TmySQLDatabase;
  SQL: array of String; MostraConteudoErro: Boolean = True): Boolean;
begin
  Query.Close;
  Query.Database := DB;
  //
  Result := AbreMySQLQuery1(Query, SQL, MostraConteudoErro);
  //
  VAR_ABETURAS_QUERY_NORMAL := VAR_ABETURAS_QUERY_NORMAL + 1;
end;

function TUnDmkDAC_PF.AbreMySQLQuery1(Query: TMySQLQuery;
  SQL: array of String; MostraConteudoErro: Boolean = True): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
begin
  //Result := False;
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no MySQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query. Open ;
      Result := True;
    end;
    Screen.Cursor := MyCursor;
    MostraSQLSequncial(Query);
  except
    Screen.Cursor := MyCursor;
    Geral.MB_Erro('Erro ao tentar abrir uma SQL no MySQLQuery!' + sLineBreak +
      TComponent(Query.Owner).Name + '.' + Query.Name);
    if MostraConteudoErro then
    begin
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
      raise;
    end;
  end;
end;

function TUnDmkDAC_PF.AbreMySQLQuery2(Query: TMySQLQuery; DB: TmySQLDatabase;
  SQL: array of String; LaAviso1, LaAviso2: TLabel): Boolean;
const
  sProcName = 'DmkDAC_PF.AbreMySQLQuery2';
var
  I: Integer;
  MyCursor: TCursor;
  Texto: String;
begin
  //Result := False;
  // 2021-01-22 criado para ver se evira erro estranho que congela o executavel
  // ao carregar array de linhas uma po uma na query!
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    Texto := EmptyStr;
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sProcName + '.Gerando texto do array');
////////////////////////////////////////////////////////////////////////////////
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      for I := 0 to High(SQL) do
      begin
        if SQL[I] <> '' then
          Texto := Texto + SQL[I] + sLineBreak;
      end;
    end;
    if Texto = EmptyStr then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no MySQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sProcName + '.Setando datbase');
////////////////////////////////////////////////////////////////////////////////
      Query.Database := DB;
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sProcName + '.Setando texto');
////////////////////////////////////////////////////////////////////////////////
      Query.SQL.Text := Texto;
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sProcName + '.Abrindo query');
////////////////////////////////////////////////////////////////////////////////
      Query. Open ;
      Result := True;
    end;
    Screen.Cursor := MyCursor;
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sProcName + '.MostraSQLSequncial');
////////////////////////////////////////////////////////////////////////////////
    MostraSQLSequncial(Query);
  except
    Screen.Cursor := MyCursor;
    Geral.MB_Erro('Erro ao tentar abrir uma SQL no MySQLQuery!' + sLineBreak +
    TComponent(Query.Owner).Name + '.' + Query.Name);
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
    raise;
  end;
end;

function TUnDmkDAC_PF.AbreQuery(Query: TmySQLQuery; DB: TmySQLDatabase;
  Aviso: String = ''; Silent: Boolean = False): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
var
  Texto: String;
begin
  Result := False;
  try
    Query.Close;
    Query.Database := DB;
    Query. Open ;
    Result := True;
  except
    on E: Exception do
    begin
      if not Silent then
      begin
        Texto := E.Message + sLineBreak;
        MostraQuery(Query, Texto + Aviso);
      end;
      raise;
    end else
    begin
      if not Silent then
      begin
        MostraQuery(Query, Aviso);
      end;
      raise;
    end;
  end;
end;

function TUnDmkDAC_PF.AbreQueryApenas(Query: TmySQLQuery;  Aviso: String): Boolean;
// . O p e n ;  ApenasAbre
begin
  try
    Query.Close;
    //Query.Database := DB;
    Query. Open ;
    Result := True;
  except
    MostraQuery(Query, Aviso);
    raise;
  end;
end;

function TUnDmkDAC_PF.AbreTable(Table: TmySQLTable; DB: TmySQLDatabase;
  Aviso: String): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
begin
  try
    Table.Close;
    Table.Database := DB;
    Table. Open ;
    Result := True;
  except
    Geral.MB_Erro('N�o foi poss�vel abrir a TABLE: ' + Table.Name + sLineBreak +
    'Tabela: "' + Table.TableName + '"');
    raise;
  end;
end;

function TUnDmkDAC_PF.AtivaDesativaRegistroIdx1(Database: TmySQLDatabase;
  Tabela, CampoIdx, CampoAtivo: String; Codigo: Integer; Valor: Boolean):
  Boolean;
begin
  Result := ExecutaDB(Database,
    ' UPDATE ' + Tabela +
    ' SET ' + CampoAtivo + '=' + Geral.FF0(Geral.BoolToInt(Valor)) +
    ' WHERE ' + CampoIdx + '=' + Geral.FF0(Codigo));

end;

function TUnDmkDAC_PF.CodigosSeparadosPorVirgula_DeQuery(Qry: TmySQLQuery;
  Campo: String): String;
begin
  Result := '';
  Qry.First;
  while not Qry.Eof do
  begin
    Result := Result + ',' + Geral.FF0(Qry.FieldByName(Campo).AsInteger);
    //
    Qry.Next;
  end;
  if Length(Result) > 0 then
    Result := Copy(Result, 2);
end;

procedure TUnDmkDAC_PF.ConectaMyDB_DAC(DB: TmySQLDataBase; DatabaseName,
  Host: String; Port: Integer; UserName, UserPassword: String; Desconecta,
  Configura, Conecta: Boolean; Silencioso: Boolean = False);
begin
  if Desconecta then
    DB.Connected := False;
  if Configura then
    ConfiguracaoDB(DB);
  //
  if Desconecta then
    DB.Connected := False;
  //
  DB.DatabaseName   := DatabaseName;
  DB.Host           := Host;
  DB.Port           := Port;
  DB.UserName       := UserName;
  DB.UserPassword   := UserPassword;
  DB.KeepConnection := True;
  //
  if Conecta then
  begin
    try
      DB.Connected := True;
    except
      if Silencioso = False then
      begin
        Geral.MB_Aviso(
          'N�o foi poss�vel conectar o "' + DB.Name + '"!' + sLineBreak +
          'Configura��o:' + sLineBreak + sLineBreak+
          'Database: ' + DatabaseName + sLineBreak +
          'Host: ' + Host + sLineBreak +
          'Porta: ' + FormatFloat('0', Port));
      end;
    end;
  end;
end;

procedure TUnDmkDAC_PF.ConfiguracaoDB(DB: TmySQLDatabase);
begin
  DB.LoginPrompt := False;
  //
{$IfDef VER310_UP}
    //DB.ConnectionCharacterSet      := 'latin1';
    //DB.ConnectionCollation         := 'latin1_swedish_ci';
  DB.ConnectionCharacterSet      := '';
  DB.ConnectionCollation         := '';
{$ELSE}
  Geral.MB_Info('ConnectionCharacterSet = "latin1"' + sLineBreak + 'Verificar String -> WideString');
  {$IFDEF DELPHI16_UP}
    DB.ConnectionCharacterSet      := 'latin1';
    DB.ConnectionCollation         := 'latin1_swedish_ci';
  {$ELSE}
    DB.ConnectionCharacterSet      := '';
    DB.ConnectionCollation         := '';
  {$ENDIF}
{$EndIf}
end;

function TUnDmkDAC_PF.CriaDataModule(const Owner: TComponent; const InstanceClass: TComponentClass; var Reference): Boolean;
var
  Instance: TComponent;
  MyCursor: TCursor;
begin
  Result := False;
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;
    try
      Instance := TComponent(InstanceClass.NewInstance);
      TComponent(Reference) := Instance;
      try
        Instance.Create(nil); // Owner
        Result := True;
      except
        TComponent(Reference) := nil;
        raise;
      end;
      Screen.Cursor := MyCursor;
    finally
      Screen.Cursor := MyCursor;
    end;
  except
    Geral.MB_Erro('N�o foi poss�vel criar o m�dulo de dados "' +
    InstanceClass.ClassName + '"');
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TUnDmkDAC_PF.DesfazOrdenacaoDmkDBGrid(Grid: TDmkDBGrid;
FldAtu: String);
var
  Query: TmySQLQuery;
  Valor: Variant;
begin
  if Grid.DataSource.DataSet is TmySQLQuery then
    Query := TmySQLQuery(Grid.DataSource.DataSet)
  else
    Query := nil;
  if Query <> nil then
  begin
    Query.SortFieldNames := '';
    if Query.State <> dsInactive then
    begin
      if FldAtu <> '' then
      begin
        Valor := Query.FieldByName(FldAtu).AsVariant;
        Query.Close;
        Query. Open ;
        Query.Locate(FldAtu, Valor, []);
      end;
    end;
  end;
end;

procedure TUnDmkDAC_PF.ExcluiArquivosDeCampos();
var
  SearchRec: TSearchRec;
  Result: Integer;
  Name: String;
begin
  Name := CO_DIR_RAIZ_DMK + '\Web\' + Application.Title + '\';
  Result := FindFirst(Name+'*.*', faAnyFile, SearchRec);
  while Result = 0 do
  begin
    DeleteFile(Name+SearchRec.Name);
    Result := FindNext(SearchRec);
  end;
  Geral.MB_Aviso('Todos arquivos configura��es de upload foram ' +
  'exclu�dos para serem renovados! Tente fazer o upload novamente!');
end;

function TUnDmkDAC_PF.ExecutaDB(DB: TmySQLDatabase; SQL: String): Boolean;
  procedure MostraSQL();
  var
    Texto: String;
  begin
    Texto := SQL + sLineBreak + '/*' + DB.Name + '*/';
    //
    Geral.MB_Erro(Texto);
  end;
begin
  try
    DB.Execute(SQL);
    Result := True;
  except
    MostraSQL();
    raise;
  end;
end;

function TUnDmkDAC_PF.ExecutaMySQLQuery0(Query: TMySQLQuery; DB: TMySQLDatabase;
  SQL: array of String): Boolean;
var
  I: Integer;
  Qry, QrExec: TmySQLQuery;
begin
  try
    Screen.Cursor := crSQLWait;
    if Query = nil then
    begin
      Qry := TmySQLQuery.Create(DB.Owner);
      QrExec := Qry;
    end else
      QrExec := Query;
    try
      QrExec.Close;
      QrExec.Database := DB;
      //if High(SQL) > 0 then
      if (Length(SQL) > 1) or
      ((Length(SQL) = 1) and (Trim(SQL[0]) <> '')) then
      begin
        QrExec.SQL.Clear;
        for I := 0 to High(SQL) do
          QrExec.SQL.Add(SQL[I]);
      end;
      if QrExec.SQL.Text = '' then
      begin
        Geral.MB_Erro('Texto da SQL indefinido no MySQLQrExec!' + sLineBreak +
        'Avise a DERMATEK!');
        Result := False;
      end
      else begin
        QrExec.ExecSQL;
        //Geral.MB_(QrExec.SQL.Text, 'SQL', MB_OK+MB_ICONINFORMATION);
        MostraSQLSequncial(QrExec);
        Result := True;
      end;
    finally
      if QrExec = nil then
        if Qry <> nil then
          Qry.Free;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MB_Erro('Erro ao tentar executar uma SQL no MySQLQrExec!' + sLineBreak +
    'Avise a DERMATEK!');
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
    raise;
  end;
end;

function TUnDmkDAC_PF.ExecutaQuery(Query: TmySQLQuery; DB: TmySQLDatabase;
  Aviso: String): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
begin
  Result := False;
  try
    Query.Close;
    Query.Database := DB;
    Query.ExecSQL;
    Result := True;
  except
    MostraQuery(Query, Aviso);
    raise;
  end;
end;

function TUnDmkDAC_PF.GetQrVal1Str(const Form: TForm;
  const Database: TmySQLDatabase; const SQL: array of String;
  var Valor: String): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := -1;
  Qry    := TmySQLQuery.Create(Database.Owner);
  try
    AbreMySQLQuery0(Qry, Database, SQL);
    Result := Qry.RecordCount;
    if Result > 0 then
      Valor := Qry.Fields[0].AsString
    else
      Valor := '';
  finally
    Qry.Free;
  end;
end;

function TUnDmkDAC_PF.InsereRegistrosPorCopia(
QrUpd: TmySQLQuery; Tabela, OriDB: String;
FldSel: array of String; ValSel: array of Variant;
FldAlt: array of String; ValAlt: array of Variant; FldAutoInc: String;
UserDataAlterweb: Boolean; LaAviso1, LaAviso2: TLabel): Boolean;
var
  I, J: Integer;
  Data, Valor, SQLAtz, TmpTab, SQLSel, TmpDB, Liga: String;
begin
  if (LaAviso1 <> nil) and (LaAviso2 <> nil) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Copiando registro. Tabela: "' +
      Tabela + '". Registro: ' + Geral.VariavelToString(ValAlt[High(ValAlt)]));
  end;
  //FldAutoInc: String; > ver o que fazer com campos auto increment!!
  Result := False;
  TmpTab := '_copia_' + Tabela;
  if OriDB <> '' then
    TmpDB := OriDB + '.'
  else
    TmpDB := '';
  //
  I := High(FldAlt);
  j := High(ValAlt);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) +
    ' valores para estes campos a serem alterados em "InsereRegistrosPorCopia"!'
    + sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  //
  I := High(FldSel);
  j := High(ValSel);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos a serem copiados em "InsereRegistrosPorCopia"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  //
  SQLSel := '';
  for I := Low(FldSel) to High(FldSel) do
  begin
    if I = 0 then
      Liga := 'WHERE '
    else
      Liga := 'AND ';
    if FldSel[i] = CO_JOKE_SQL then
      SQLSel := SQLSel + Liga + ValSel[i] + sLineBreak
    else
    begin
      Valor := Geral.VariavelToString(ValSel[i]);
      SQLSel := SQLSel + Liga + FldSel[i] + '=' + Valor + sLineBreak;
    end;
  end;  
  //
  SQLAtz := '';
  j := High(FldAlt);
  for i := Low(FldAlt) to j do
  begin
    if FldAlt[i] = CO_JOKE_SQL then
    begin
      if ((i < j) or UserDataAlterweb) and (Trim(ValAlt[i]) <> '') then
        SQLAtz := SQLAtz + ValAlt[i] + ', ' + sLineBreak
      else
        SQLAtz := SQLAtz + ValAlt[i] + sLineBreak;
    end else
    begin
      Valor := Geral.VariavelToString(ValAlt[i]);
      if (i < j) or UserDataAlterweb then
        SQLAtz := SQLAtz + FldAlt[i] + '=' + Valor + ', ' + sLineBreak
      else
        SQLAtz := SQLAtz + FldAlt[i] + '=' + Valor + sLineBreak;
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    SQLAtz := SQLAtz + 'DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO)
    + ',' + sLineBreak + 'DataAlt="0000-00-00", UserAlt=0' + sLineBreak;
  end;
  //
  try
    ExecutaMySQLQuery0(QrUpd, QrUpd.DataBase, [
      'DROP TABLE IF EXISTS ' + TmpTab + '; ',
      'CREATE TABLE ' + TmpTab,
      'SELECT * FROM ' + TmpDB + Tabela,
      SQLSel,
      '; ',
      'UPDATE ' + TmpTab + ' SET ',
      SQLAtz,
      '; ',
      'INSERT INTO ' + TmpDB + Tabela,
      'SELECT * FROM ' + TmpTab + '; ',
      'DROP TABLE  ' + TmpTab + '; ',
      '']);
    //
    if (LaAviso1 <> nil) and (LaAviso2 <> nil) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    Result := True;
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
    raise;
  end;
end;

procedure TUnDmkDAC_PF.LeMeuSQLDirectForce_y(Query: TmySQLDirectQuery;
  Arquivo: String; Memo: TMemo; WinArq, AskShow, Force: Boolean);
var
  i: Integer;
  Text1, Text2: TextFile;
  Name, Diretorio: String;
  Other: Boolean;
  Texto1, Texto2, Texto3, Texto4: String;
begin
  //Texto1 := '/*'+TmySQLDatabase(Query.Database).DatabaseName+'.'+Query.Name+'*/';
  Texto1 := '/*'+TComponent(Query.Owner).Name + '.' + Query.Name + '*/';
  Texto2 := ''; //'/*********** SQL ***********/';
  Texto3 := ''; //'/********* FIM SQL *********/';
  Texto4 := sLineBreak;
  if WinArq then
  begin
    Diretorio := CO_DERMATEK_SISTEMA;
    if not DirectoryExists(Diretorio) then ForceDirectories(Diretorio);
    Name := Diretorio + '\SQL.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(Text1, Name);
    ReWrite(Text1);
    WriteLn(Text1, Texto1);
  end;
  if Trim(Arquivo) <> '' then
  begin
    Other := True;
    if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text2, Arquivo);
    ReWrite(Text2);
    WriteLn(Text2, Texto1);
  end else Other := False;
  if Memo <> nil then
  begin
    if TmySQLDatabase(Query.Database) <> nil then
    begin
      Memo.Lines.Add(Texto1);
      Memo.Lines.Add(Texto2);
    end;
  end;
  for i := 0 to Query.SQL.Count -1 do
  begin
    if WinArq then WriteLn(Text1, Query.SQL[i]);
    if Other  then WriteLn(Text2, Query.SQL[i]);
    if Memo <> nil then Memo.Lines.Add(Query.SQL[i]);
  end;
  //
(*
  if Query.Params.Count > 0 then
  begin
    Texto4 := Texto4 + '/***** Parametros *******/' + sLineBreak;
    for i := 0 to Query.Params.Count -1 do
    begin
      Texto4 := Texto4 + '/***** ' + Query.Params[i].Name + ' = ' +
        Geral.VariantToString(Query.Params[i].Value) + ' ******/'  + sLineBreak;
    end;
    Texto4 := Texto4 + '/***** FIM Parametros *******/' + sLineBreak;
  end else begin
    Texto4 := Texto4 + '/*****Query sem parametros*******/' + sLineBreak;
  end;
*)
  if WinArq then WriteLn(Text1, Texto4);
  if Other  then WriteLn(Text2, Texto4);
  if Memo <> nil then Memo.Lines.Add(Texto4);
  //
  if WinArq then CloseFile(Text1);
  if Other  then CloseFile(Text2);
  if TmySQLDatabase(Query.Database) <> nil then
    if Memo <> nil then Memo.Lines.Add(Texto3);
  if AskShow then
  begin
    if (FM_MASTER = 'V') or (Uppercase(VAR_LOGIN) = 'A') or Force then
    begin
      Geral.MB_Aviso(Texto1 + sLineBreak + Texto2 + sLineBreak + Query.SQL.Text +
      Texto3 + sLineBreak + Texto4);
    end;
  end;
end;

procedure TUnDmkDAC_PF.LeMeuSQLForce_y(Query: TmySQLQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow, Force: Boolean);
var
  i: Integer;
  Text1, Text2: TextFile;
  Name, Diretorio: String;
  Other: Boolean;
  Texto1, Texto2, Texto3, Texto4: String;
begin
  //Texto1 := '/*'+TmySQLDatabase(Query.Database).DatabaseName+'.'+Query.Name+'*/';
  Texto1 := '/*'+TComponent(Query.Owner).Name + '.' + Query.Name + '*/';
  Texto2 := '/*********** SQL ***********/';
  Texto3 := '/********* FIM SQL *********/';
  Texto4 := sLineBreak;
  if WinArq then
  begin
    Diretorio := CO_DERMATEK_SISTEMA;
    if not DirectoryExists(Diretorio) then ForceDirectories(Diretorio);
    Name := Diretorio + '\SQL.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(Text1, Name);
    ReWrite(Text1);
    WriteLn(Text1, Texto1);
  end;
  if Trim(Arquivo) <> '' then
  begin
    Other := True;
    if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text2, Arquivo);
    ReWrite(Text2);
    WriteLn(Text2, Texto1);
  end else Other := False;
  if Memo <> nil then
  begin
    if TmySQLDatabase(Query.Database) <> nil then
    begin
      Memo.Lines.Add(Texto1);
      Memo.Lines.Add(Texto2);
    end;
  end;
  for i := 0 to Query.SQL.Count -1 do
  begin
    if WinArq then WriteLn(Text1, Query.SQL[i]);
    if Other  then WriteLn(Text2, Query.SQL[i]);
    if Memo <> nil then Memo.Lines.Add(Query.SQL[i]);
  end;
  //
  if Query.Params.Count > 0 then
  begin
    Texto4 := Texto4 + '/***** Parametros *******/' + sLineBreak;
    for i := 0 to Query.Params.Count -1 do
    begin
      Texto4 := Texto4 + '/***** ' + Query.Params[i].Name + ' = ' +
        Geral.VariantToString(Query.Params[i].Value) + ' ******/'  + sLineBreak;
    end;
    Texto4 := Texto4 + '/***** FIM Parametros *******/' + sLineBreak;
  end else begin
    Texto4 := Texto4 + '/*****Query sem parametros*******/' + sLineBreak;
  end;
  if WinArq then WriteLn(Text1, Texto4);
  if Other  then WriteLn(Text2, Texto4);
  if Memo <> nil then Memo.Lines.Add(Texto4);
  //
  if WinArq then CloseFile(Text1);
  if Other  then CloseFile(Text2);
  if TmySQLDatabase(Query.Database) <> nil then
    if Memo <> nil then Memo.Lines.Add(Texto3);
  if AskShow then
  begin
    if (FM_MASTER = 'V') or (Uppercase(VAR_LOGIN) = 'A') or Force then
    begin
      Geral.MB_Aviso(Texto1 + sLineBreak + Texto2 + sLineBreak + Query.SQL.Text +
      Texto3 + sLineBreak + Texto4);
    end;
  end;
end;

procedure TUnDmkDAC_PF.LeMeuSQL_Fixo_y(Query: TmySQLQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow: Boolean);
begin
  LeMeuSQLForce_y(Query, Arquivo, Memo, WinArq, AskShow, True);
end;

procedure TUnDmkDAC_PF.MostraDirectQuery(Query: TmySQLDirectQuery;
  Aviso: String);
var
  i: Integer;
  NomeParente: String;
  Texto: WideString;
begin
  if Query.Owner <> nil then
    NomeParente := 'Owner: ' + TComponent(Query.Owner).Name
  else
    NomeParente := 'SEM OWNER!';
  Texto := '/* ' + Aviso +
    sLineBreak + NomeParente +
    sLineBreak + ' */' +
    sLineBreak + Query.SQL.Text;
  //
{
  for i := 1 to Query.ParamCount do
  begin
    Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
      TParam(Query.Params[i-1]).AsString + '*/';
  end;
}
  Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
  Geral.MB_Erro(Texto);
end;

procedure TUnDmkDAC_PF.MostraQuery(Query: TmySQLQuery; Aviso: String);
var
  i: Integer;
  NomeParente: String;
  Texto: WideString;
begin
  if Query.Owner <> nil then
    NomeParente := 'Owner: ' + TComponent(Query.Owner).Name
  else
    NomeParente := 'SEM OWNER!';
  Texto := '/* ' + Aviso +
    sLineBreak + NomeParente +
    sLineBreak + ' */' +
    sLineBreak + Query.SQL.Text;
  //
  for i := 1 to Query.ParamCount do
  begin
    Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
      TParam(Query.Params[i-1]).AsString + '*/';
  end;
  Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
  Geral.MB_Erro(Texto);
end;

procedure TUnDmkDAC_PF.MostraSQLDirectSequncial(Qry: TmySQLDirectQuery);
var
  Continua: Boolean;
begin
  if VAR_SHOW_ALL_SQL_TEXT > 0 then
  begin
    Continua := True;
    if VAR_MASK_ALL_SQL_TEXT <> '' then
      Continua := pos(VAR_MASK_ALL_SQL_TEXT, Qry.SQL.Text) > 0;
    if Continua then
    begin
      MostraDirectQuery(Qry, '"VAR_SHOW_ALL_SQL_TEXT" = ' + Geral.FF0(VAR_SHOW_ALL_SQL_TEXT));
      VAR_SHOW_ALL_SQL_TEXT := VAR_SHOW_ALL_SQL_TEXT + 1;
    end;
  end;
end;

procedure TUnDmkDAC_PF.MostraSQLSequncial(Qry: TmySQLQuery);
var
  Continua: Boolean;
begin
  if VAR_SHOW_ALL_SQL_TEXT > 0 then
  begin
    Continua := True;
    if VAR_MASK_ALL_SQL_TEXT <> '' then
      Continua := pos(VAR_MASK_ALL_SQL_TEXT, Qry.SQL.Text) > 0;
    if Continua then
    begin
      MostraQuery(Qry, '"VAR_SHOW_ALL_SQL_TEXT" = ' + Geral.FF0(VAR_SHOW_ALL_SQL_TEXT));
      VAR_SHOW_ALL_SQL_TEXT := VAR_SHOW_ALL_SQL_TEXT + 1;
    end;
  end;
end;

function TUnDmkDAC_PF.ObtemNomeDeCodigo(Database: TmySQLDatabase; Tabela: String; Indice: Integer;
  CampoCod, CampoNome: String): String;
var
  Qry: TmySQLQuery;
begin
  Result := '';
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
    'SELECT ' + CampoNome,
    'FROM ' + Tabela,
    'WHERE ' + CampoCod + '=' + Geral.FF0(Indice),
    '']);
    //
    Result := Qry.FieldByName(CampoNome).AsString;
  finally
    Qry.Free;
  end;
end;

function TUnDmkDAC_PF.ObtemProximaOrdem(DB: TmySQLDataBase; Tabela,
  Campo: String; Para: TVaiPara; TipoDado: TAllFormat; Incremento: Variant;
  Base: Variant; FldIdx: array of String; ValIdx: array of Variant): Variant;
var
  Qry: TmySQLQuery;
  I, J, Ordem: Integer;
  SQLCampo, SQLFiltro, Liga, Como, Extra, Valor: String;
begin
  Result    := Null;
  SQLFiltro := '';
  case Para of
    (*vpFirst: Como := 'MIN';
    vpPrior: Como := 'MAX';
    vpNext:  Como := '';
    *)
    vpLast:  Como := 'MAX';
    else     Como := '(* Como? *)';
  end;
  case Para of
    (*
    vpFirst: Liga := '';
    vpPrior: Liga := '';
    vpNext:  Liga := '';
    *)
    vpLast:  Liga := '';
    else     Liga := '';
  end;
  //
  case Para of
    (*
    vpFirst: Extra := '';
    vpPrior: Extra := '';
    vpNext:  Extra := '';
    *)
    vpLast:  Extra := '';
    else     Extra := '';
  end;
  //
  SQLCampo := 'SELECT ' + Como + '(' + Campo + ') Ordem ' + Liga + Extra;
  I := High(FldIdx);
  J := High(ValIdx);
  if I <> J then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(I + 1) + ' campos e ' +
    IntToStr(J + 1) + ' valores para estes campos em "ObtemProximaOrdem"!' +
    sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  for I := Low(FldIdx) to High(FldIdx) do
  begin
    if (I = 0) and (Liga = '') then
      Liga := 'WHERE '
    else
      Liga := 'AND ';
    if FldIdx[I] = CO_JOKE_SQL then
      SQLFiltro := SQLFiltro + Liga + ValIdx[I] + sLineBreak
    else
    begin
      Valor := Geral.VariavelToString(ValIdx[I]);
      SQLFiltro := SQLFiltro + Liga + FldIdx[I] + '=' + Valor + sLineBreak;
    end;
  end;
  Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
  try
    AbreMySQLQuery0(Qry, DB, [
    SQLCampo,
    'FROM ' + LowerCase(Tabela),
    SQLFiltro,
    '']);
    case TipoDado of
      dmktfString: Result := Qry.FieldByName('Ordem').AsString + String(Incremento);
      dmktfDouble: Result := Qry.FieldByName('Ordem').AsFloat + Double(Incremento);
      dmktfInteger, dmktfLongint:
        Result := Qry.FieldByName('Ordem').AsInteger + Integer(Incremento);
      dmktfInt64: Result := Qry.FieldByName('Ordem').AsVariant + Incremento;
      dmktfDate, dmktfTime, dmktfDateTime:
        Result := Qry.FieldByName('Ordem').AsDateTime + TDateTime(Incremento);
      else (*dmktfNone:*)
      begin
        Result := Qry.FieldByName('Ordem').AsVariant + Incremento;
        Geral.MB_Erro('Formato "AllFormat" n�o implementado no "ObtemProximaOrdem"!');
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnDmkDAC_PF.ReabrirTabelasFormAtivo(Sender: TObject);
var
  Count, i, j: Integer;
  Nome, QrTxt: String;
  Combo: TDBLookupComboBox;
  Query: TmySQLQuery;
  DataSource: TDataSource;
  Mensagem: String;
begin
  Count := 0;
  if Screen.ActiveForm <> nil then
  begin
    for i := 0 to Screen.ActiveForm.ComponentCount -1 do
    begin
      if Screen.ActiveForm.Components[i] is TdmkEdit then
      begin
        Nome := TdmkEdit(Screen.ActiveForm.Components[i]).Name;
        Nome[1] := 'C';
        Nome[2] := 'B';
        //
        Combo := TDBLookupComboBox(Screen.ActiveForm.FindComponent(Nome));
        if Combo <> nil then
        begin
          DataSource := Combo.ListSource;
          if DataSource <> nil then
          begin
            QrTxt := DataSource.Name;
            QrTxt[1] := 'Q';
            QrTxt[2] := 'r';
            Query := TmySQLQuery(Screen.ActiveForm.FindComponent(QrTxt));
            if Query <> nil then
            begin
              Query.Close;
              Query. Open ;
              Count := Count + 1;
            end;
          end;
        end;
      end else
      if Screen.ActiveForm.Components[i] is TdmkEditCB then
      begin
        Nome := TdmkEditCB(Screen.ActiveForm.Components[i]).Name;
        Nome[1] := 'C';
        Nome[2] := 'B';
        //
        Combo := TDBLookupComboBox(Screen.ActiveForm.FindComponent(Nome));
        if Combo <> nil then
        begin
          DataSource := Combo.ListSource;
          if DataSource <> nil then
          begin
            QrTxt := DataSource.Name;
            QrTxt[1] := 'Q';
            QrTxt[2] := 'r';
            Query := TmySQLQuery(Screen.ActiveForm.FindComponent(QrTxt));
            if Query <> nil then
            begin
              Query.Close;
              Query. Open ;
              Count := Count + 1;
            end;
          end;
        end;
      end else
      if Screen.ActiveForm.Components[i] is TMySQLTable then
      begin
        for j := 0 to TMySQLTable(Screen.ActiveForm.Components[i]).FieldCount -1 do
        begin
          if TMySQLTable(Screen.ActiveForm.Components[i]).Fields[j].FieldKind = fkLookup then
          begin
            try
              Query := TMySQLQuery(TMySQLTable(
                Screen.ActiveForm.Components[i]).Fields[j].LookupDataSet);
              if Query <> nil then
              begin
                Query.Close;
                Query. Open ;
                Count := Count + 1;
              end;
            except
              ;
            end;
          end;
        end;
      end;
    end;
    case Count of
     0: Mensagem := 'Nenhuma tabela foi reaberta.';
     1: Mensagem := 'Uma tabela foi reaberta.';
      else Mensagem := IntToStr(Count)+' tabelas foram reabertas.';
    end;
    Geral.MB_Info(Mensagem);
  end else Geral.MB_Erro('Janela n�o definida!');
end;

{$IfNDef sUSQLDB}
function TUnDmkDAC_PF.TestarConexaoMySQL(Form: TForm; Host, User, Pass,
  BancoDados: String; Port: Integer; Avisa: Boolean): Boolean;
begin
  Result := USQLDB.TestarConexaoMySQL(Form, Host, User, Pass, BancoDados, Port, Avisa);
end;
{$EndIf}

function TUnDmkDAC_PF.TextoJaCadastrado(Texto, Campo, Tabela: String; DB:
  TmySQLDataBase): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(DB.Owner);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
    'SELECT LOWER(' + Campo + ') ' + Campo,
    'FROM ' + Lowercase(Tabela),
    'WHERE ' + Campo + '="' + Lowercase(Texto) + '"',
    '']);
    Result := Qry.RecordCount > 0;
    //
    if MyObjects.FIC(Result, nil, 'O registro "' + Texto +
    '" j� est� cadastrado!') then ; // nada
  finally
    Qry.Free;
  end;
end;


end.
