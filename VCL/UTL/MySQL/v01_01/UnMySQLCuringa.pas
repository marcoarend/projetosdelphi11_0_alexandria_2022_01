unit UnMySQLCuringa;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, Mask,
  ComCtrls, UnInternalConsts2, mySQLDbTables, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, Variants, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TUnCuringa = class(TObject)
  private
    { Private declarations }
    {$IFNDEF NAO_USA_DB_GERAL}
    function  NomeDoCampoCodUsu(Database: TmySQLDataBase; NomeDaTabela,
              NomeDoCampoCodigo: String): String;
    {$ENDIF}
  public
    { Public declarations }
    ////////////////////////////////////////////////////////////////////////////

    {$IFNDEF NAO_USA_DB_GERAL}
    function  ExecutouPesq01(TextoAPesquisar, NomeDoCampoCodigo,
              NomeDoCampoNome, NomeDaTabela: String; DataBase: TmySQLDatabase;
              Extra, LEFT_JOIN: string; var Erro: String): Boolean;

    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function  ExecutouPesq05(NomeDoCampoCodigo, NomeDoCampoNome1,
              NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4,
              NomeDoCampoNome5, NomeDoCampoNome6, NomeDoCampoNome7,
              NomeDoCampoNome8, NomeDaTabela, NomeDoCampoTipo:
              String; Tipo1: Integer; DataBase: TmySQLDatabase; Extra: string;
              SohNum: Boolean; var Erro: String): Boolean;
    function  ExecutouPesq06((*NomeDoCampoCodigo, NomeDoCampoNome1,
              NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4,
              NomeDoCampoNome5, NomeDoCampoNome6,
              NomeDaTabela, NomeDoCampoTipo:
              String; Tipo1: Integer;*) DataBase: TmySQLDatabase(*; Extra: string;
              SohNum: Boolean; var Erro: String*)): Boolean;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function  ExecutouPesqLivre(TextoAPesquisar, SQL_Livre: String; DataBase:
              TmySQLDatabase; var Erro: String): Boolean;

    {$ENDIF}

    ////////////////////////////////////////////////////////////////////////////
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaFormEnti(NomeDoCampoCodigo, NomeDoCampoNome1, NomeDoCampoNome2,
             NomeDoCampoNome3, NomeDoCampoNome4, NomeDoCampoNome5,
             NomeDoCampoNome6, NomeDoCampoNome7, NomeDoCampoNome8, NomeDaTabela,
             NomeDoCampoTipo: String; Tipo1: Integer; DataBase:
             TmySQLDatabase; Extra: string; SohNum: Boolean = False): Integer;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaFormENome(NomeDoCampoCodigo, NomeDoCampoNome1,
             NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4,
             NomeDaTabela, NomeDoCampoTipo: String; Tipo1: Integer;
             DataBase: TmySQLDatabase; Extra, Nome: string): Integer;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaForm0(NomeDoCampoCodigo, NomeDoCampoNomeA,
             NomeDoCampoNomeB, NomeDaTabela: String; DataBase: TmySQLDatabase;
             Extra: string; ReturnCodUsu: Boolean = False): Integer;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaForm(NomeDoCampoCodigo, NomeDoCampoNome, NomeDaTabela: String;
             DataBase: TmySQLDatabase; Extra: string;
             ReturnCodUsu: Boolean = False; LEFT_JOIN: String = '';
             TextoIni: String = ''): Integer;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaForm2(NomeDoCampoFlutuante, NomeDoCampoNome, NomeDaTabela:
             String; DataBase: TmySQLDatabase; Extra: string): Double;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaForm3(NomeDoCampoData, NomeDoCampoNome, NomeDaTabela1,
             NomeDaTabela2: String; DataBase: TmySQLDatabase; Extra: string):
             TDate;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaForm4(NomeDoCampoCodigo, NomeDoCampoNome, NomeDaTabela: String;
             DataBase: TmySQLDatabase; Extra: string): String;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function CriaForm5(NomeDoCampoCodigo, NomeDoCampoNome, NomeDaTabela: String;
             DataBase: TmySQLDatabase; Extra: string): Longint;
    {$ENDIF}
    {$IFNDEF NAO_USA_DB_GERAL}
    function Pesquisa(NomeDoCampoCodigo, NomeDoCampoNome,
             NomeDaTabela: String; DataBase: TmySQLDatabase; Extra: string;
             Edit: TdmkEdit; DBCB: TdmkDBLookupComboBox;
             DataType: TAllFormat; AbreQuery: Boolean = False): Boolean;
    function Pesquisa2(const NomeDoCampoCodigo, NomeDoCampoNome,
             NomeDaTabela: String; const DataBase: TmySQLDatabase; const Extra: string;
             var Retorno: Variant;
             const DataType: TAllFormat; AbreQuery: Boolean = False): Boolean;
    function CriaFormLivre(SQL: String; Database: TMySQLDatabase): Integer;
    {$ENDIF}
    function ObtemSQLPesquisaText(Mascara: Integer; Texto: String): String;
  end;

var
  CuringaLoc: TUnCuringa;

implementation

uses UnMyObjects, Module,
{$IfNDef NAO_USA_DB_GERAL} ModuleGeral, Curinga, PesqDescri, UmySQLModule, {$EndIf}
NomeX,  DmkDAC_PF;

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaFormEnti(NomeDoCampoCodigo, NomeDoCampoNome1,
  NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4, NomeDoCampoNome5,
  NomeDoCampoNome6, NomeDoCampoNome7, NomeDoCampoNome8, NomeDaTabela,
  NomeDoCampoTipo: String; Tipo1: Integer;
  DataBase: TmySQLDatabase; Extra: string; SohNum: Boolean = False): Integer;
var
  Continua: Boolean;
  Erro, TxtOriginal: String;
begin
  Result := 0;
  try
    MyObjects.FormCria(TFmNomeX, FmNomeX);
    FmNomeX.Caption := 'Procura e Lista';
    FmNomeX.ShowModal;
    VAR_NOME_S := FmNomeX.FTexto;
    TxtOriginal := FmNomeX.FOrigi;
    Continua := FmNomeX.FContinua;
    FmNomeX.Destroy;
    VAR_CODIGO := 0;
    if Continua = True then
    begin
      // ini 2021-11-24
{
      if ExecutouPesq05(NomeDoCampoCodigo, NomeDoCampoNome1, NomeDoCampoNome2,
      NomeDoCampoNome3, NomeDoCampoNome4, NomeDoCampoNome5, NomeDoCampoNome6,
      NomeDoCampoNome7, NomeDoCampoNome8, NomeDaTabela, NomeDoCampoTipo, Tipo1, DataBase,
      Extra, SohNum, Erro) then
}
      if ExecutouPesq06((*NomeDoCampoCodigo, NomeDoCampoNome1, NomeDoCampoNome2,
      NomeDoCampoNome3, NomeDoCampoNome4, NomeDoCampoNome5, NomeDoCampoNome6,
      NomeDoCampoNome7, NomeDoCampoNome8, NomeDaTabela, NomeDoCampoTipo, Tipo1,*)
       DataBase
      (*, Extra, SohNum, Erro*)) then
      // fim 2021-11-24
      begin
        MyObjects.FormCria(TFmCuringa, FmCuringa);
        FmCuringa.EdPesq.Text := TxtOriginal;

        FmCuringa.FTextoAPesquisar   := VAR_NOME_S;
        FmCuringa.FNomeDoCampoCodigo := NomeDoCampoCodigo;
        FmCuringa.FNomeDoCampoNome1  := NomeDoCampoNome1;
        FmCuringa.FNomeDoCampoNome2  := NomeDoCampoNome2;
        FmCuringa.FNomeDoCampoNome3  := NomeDoCampoNome3;
        FmCuringa.FNomeDoCampoNome4  := NomeDoCampoNome4;
        FmCuringa.FNomeDoCampoNome5  := NomeDoCampoNome5;
        FmCuringa.FNomeDoCampoNome6  := NomeDoCampoNome6;
        FmCuringa.FNomeDoCampoNome7  := NomeDoCampoNome7;
        FmCuringa.FNomeDoCampoNome8  := NomeDoCampoNome8;
        FmCuringa.FNomeDaTabela      := NomeDaTabela;
        FmCuringa.FDataBase          := DataBase;
        FmCuringa.FExtra             := Extra;
        FmCuringa.FLEFT_JOIN         := '';
        FmCuringa.FNomeDoCampoTipo   := NomeDoCampoTipo;
        FmCuringa.FTipo1             := Tipo1;
        FmCuringa.FSohNum            := SohNum;

        //Deve ser depois!
        FmCuringa.FOrigemPesquisa := fpc2Nomes;
        FmCuringa.FPodeReabrirPesquisa := True;
        FmCuringa.PnPesquisa.Visible := True;

        MyObjects.Informa2(FmCuringa.LaAviso1, FmCuringa.LaAviso2, False, Erro);

        FmCuringa.ShowModal;
        FmCuringa.Destroy;
      end;

      Result := VAR_CODIGO;
    end;
  finally
    DmodG.QrSB.Close;
  end;
end;

function TUnCuringa.CriaFormLivre(SQL: String; Database: TMySQLDatabase): Integer;
var
  Continua: Boolean;
  Erro, TxtOriginal, Texto: String;
begin
  Result := 0;
  try
    MyObjects.FormCria(TFmNomeX, FmNomeX);
    FmNomeX.Caption := 'Procura e Lista';
    FmNomeX.ShowModal;
    VAR_NOME_S := FmNomeX.FTexto;
    TxtOriginal := FmNomeX.FOrigi;
    Continua := FmNomeX.FContinua;
    FmNomeX.Destroy;
    VAR_CODIGO := 0;
    if Continua = True then
    begin
      if ExecutouPesqLivre(VAR_NOME_S, SQL, DataBase, Erro) then
      begin
        MyObjects.FormCria(TFmCuringa, FmCuringa);
        FmCuringa.EdPesq.Text := TxtOriginal;
        FmCuringa.FSQL_Livre := SQL;
        FmCuringa.FDatabase := DataBase;

        //Deve ser depois!
        FmCuringa.FOrigemPesquisa := fpcSQL_Livre;
        FmCuringa.FPodeReabrirPesquisa := True;
        FmCuringa.PnPesquisa.Visible := True;

        MyObjects.Informa2(FmCuringa.LaAviso1, FmCuringa.LaAviso2, False, Erro);

        FmCuringa.ShowModal;
        FmCuringa.Destroy;
      end;

      Result := VAR_CODIGO;
    end;
  finally
    //
  end;
end;

{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaFormENome(NomeDoCampoCodigo, NomeDoCampoNome1,
  NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4, NomeDaTabela,
  NomeDoCampoTipo: String; Tipo1: Integer;
  DataBase: TmySQLDatabase; Extra, Nome: string): Integer;
var
  Continua: Boolean;
begin
  Result := 0;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura e Lista';
  FmNomeX.ShowModal;
  Nome := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  VAR_CODIGO := 0;
  if Continua = True then
  begin
    DmodG.QrSB.Close;
    DmodG.QrSB.SQL.Clear;
    DmodG.QrSB.Database := DataBase;
    DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, ');
    DmodG.QrSB.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
    DmodG.QrSB.SQL.Add('CASE WHEN '+NomeDoCampoTipo+'='+IntToStr(Tipo1));
    DmodG.QrSB.SQL.Add('THEN '+NomeDoCampoNome1+' ELSE '+NomeDoCampoNome2);
    DmodG.QrSB.SQL.Add('END NOME FROM '+lowercase(nomedatabela));
    DmodG.QrSB.SQL.Add('WHERE (CASE WHEN '+NomeDoCampoTipo+'='+IntToStr(Tipo1));
    DmodG.QrSB.SQL.Add('THEN '+NomeDoCampoNome1+' ELSE '+NomeDoCampoNome2);
    DmodG.QrSB.SQL.Add('END) LIKE :P0');
    case VAR_GOTONEG of
      gotoNeg : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
      gotoNiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
      gotoAll : ;//
      gotoNiP : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
      gotoPiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
      gotoPos : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
    end;
    DmodG.QrSB.SQL.Add(Extra);
    DmodG.QrSB.SQL.Add(' ORDER BY NOME');
    DmodG.QrSB.Params[0].AsString := Nome;
    UMyMod.AbreQuery(DmodG.QrSB, DataBase, 'TUnCuringa.CriaFormENome');

    if DmodG.QrSB.RecordCount = 0 then
      Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
    else
      MyObjects.FormShow(TFmCuringa, FmCuringa);
    Result := VAR_CODIGO;
    DmodG.QrSB.Close;
  end;
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.ExecutouPesq01(TextoAPesquisar, NomeDoCampoCodigo,
  NomeDoCampoNome, NomeDaTabela: String; DataBase: TmySQLDatabase;
  Extra, LEFT_JOIN: string; var Erro: String): Boolean;
var
  SQLNeg: String;
begin
  Erro := '';
  //Result := False;
  case VAR_GOTONEG of
    gotoNeg : SQLNeg := ' AND '+NomeDoCampoCodigo+'<0';
    gotoNiZ : SQLNeg := ' AND '+NomeDoCampoCodigo+'<=0';
    gotoNiP : SQLNeg := ' AND '+NomeDoCampoCodigo+'<>0';
    gotoPiZ : SQLNeg := ' AND '+NomeDoCampoCodigo+'>=0';
    gotoPos : SQLNeg := ' AND '+NomeDoCampoCodigo+'>0';
    else (*gotoAll :*) SQLNeg := '';
  end;
  try
    Result := UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrSB, DataBase, [
    'SELECT ' + NomeDoCampoCodigo + ' Codigo, ',
    NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ',
    NomeDoCampoNome + ' Nome FROM ' + lowercase(NomeDaTabela) + ' ' + LEFT_JOIN,
    ' WHERE '+NomeDoCampoNome+' LIKE "' + TextoAPesquisar + '"',
    SQLNeg,
    Extra,
    ' ORDER BY ' + NomeDoCampoNome,
    ' ']);
    //Geral.MB_Info(DmodG.QrSB.SQL.Text)
  except
    Geral.MB_ERRO('N�o foi poss�vel a localiza��o pelo nome!' + sLineBreak +
    sLineBreak + 'SQL:' + sLineBreak + DmodG.QrSB.SQL.Text);
    //
    raise;
  end;
  if (DmodG.QrSB.RecordCount = 0) then
    Erro := 'N�o foi encontrado nenhum registro!';
  //
{
  DmodG.QrSB.Close;
  DmodG.QrSB.Database := DataBase;
  DmodG.QrSB.SQL.Clear;
  DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, ');
  DmodG.QrSB.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
  DmodG.QrSB.SQL.Add(NomeDoCampoNome + ' Nome FROM ' + lowercase(NomeDaTabela) + LEFT_JOIN);
  DmodG.QrSB.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
  case VAR_GOTONEG of
    gotoNeg : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
    gotoNiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
    gotoAll : ;//
    gotoNiP : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
    gotoPiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
    gotoPos : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
  end;
  DmodG.QrSB.SQL.Add(Extra);
  DmodG.QrSB.SQL.Add(' ORDER BY '+NomeDoCampoNome);
  DmodG.QrSB.Params[0].AsString := TextoAPesquisar;
  try
    UMyMod.AbreQuery(DmodG.QrSB, DataBase, 'TUnCuringa.CriaForm');
  except
    Geral.MB_Aviso('N�o foi poss�vel a localiza��o pelo nome!' +
    sLineBreaksLineBreak'SQL:'sLineBreak + DmodG.QrSB.SQL.Text);
  end;
  if DmodG.QrSB.RecordCount = 0 then
    Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
}
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.ExecutouPesq05(NomeDoCampoCodigo, NomeDoCampoNome1,
  NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4, NomeDoCampoNome5,
  NomeDoCampoNome6, NomeDoCampoNome7, NomeDoCampoNome8, NomeDaTabela,
  NomeDoCampoTipo: String; Tipo1: Integer; DataBase:
  TmySQLDatabase; Extra: string; SohNum: Boolean; var Erro: String): Boolean;
  //
  function GetCNPJ(): String;
  begin
    if NomeDoCampoNome5 <> EmptyStr then
    begin
      Result := ', " CNPJ: " + CNPJ'
    end else
      Result := ''
  end;
var
  SQL1, SQL2, SQL3, SQL4, (*SQL5, SQL6,*) SQLNeg, WHR1, WHR2, WHR3, WHR4, WHR5, WHR6, SQLx: String;
begin
  Erro := '';
  case VAR_GOTONEG of
    gotoNeg : SQLNeg := ' AND ' + NomeDoCampoCodigo + '<0';
    gotoNiZ : SQLNeg := ' AND ' + NomeDoCampoCodigo + '<=0';
    gotoNiP : SQLNeg := ' AND ' + NomeDoCampoCodigo + '<>0';
    gotoPiZ : SQLNeg := ' AND ' + NomeDoCampoCodigo + '>=0';
    gotoPos : SQLNeg := ' AND ' + NomeDoCampoCodigo + '>0';
    else (*gotoAll :*) SQLNeg := '';
  end;
  try
    if SohNum then
      VAR_NOME_S := '%' + Geral.SoNumero_TT(VAR_NOME_S) + '%';
    //
    if NomeDoCampoNome1 = '' then SQL1 := '' else SQL1 :=
    //'IF(' + NomeDoCampoNome1 + '<>"", CONCAT(" [R] ", ' + NomeDoCampoNome1 + '), ""), ';
    //'CASE WHEN ' + NomeDoCampoNome1 + '<>"" THEN CONCAT(" [R] ", ' + NomeDoCampoNome1 + ') ELSE "" END, ';
    'CASE WHEN ' + NomeDoCampoNome1 + '<>"" THEN CONCAT(" [R] ", ' + NomeDoCampoNome1 + GetCNPJ() + ') ELSE "" END, ';
    if NomeDoCampoNome2 = '' then SQL2 := '' else SQL2 :=
    //'IF(' + NomeDoCampoNome2 + '<>"", CONCAT(" [N] ", ' + NomeDoCampoNome2 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome2 + '<>"" THEN CONCAT(" [N] ", ' + NomeDoCampoNome2 + ') ELSE "" END, ';
    if NomeDoCampoNome3 = '' then SQL3 := '' else SQL3 :=
    //'IF(' + NomeDoCampoNome3 + '<>"", CONCAT(" [F] ", ' + NomeDoCampoNome3 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome3 + '<>"" THEN CONCAT(" [F] ", ' + NomeDoCampoNome3 + ') ELSE "" END, ';
    if NomeDoCampoNome4 = '' then SQL4 := '' else SQL4 :=
    //'IF(' + NomeDoCampoNome4 + '<>"", CONCAT(" [A] ", ' + NomeDoCampoNome4 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome4 + '<>"" THEN CONCAT(" [A] ", ' + NomeDoCampoNome4 + ') ELSE "" END, ';
    //
    SQLx := SQL1 + SQL2 + SQL3 + SQL4;
    SQLx := Copy(SQLx, 1, Length(SQLx) - 2);
    //
    WHR1 := 'WHERE (' + NomeDoCampoNome1 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome2 = '' then WHR2 := '' else
      WHR2 := 'OR ' + NomeDoCampoNome2 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome3 = '' then WHR3 := '' else
      WHR3 := 'OR ' + NomeDoCampoNome3 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome4 = '' then WHR4 := '' else
      WHR4 := 'OR ' + NomeDoCampoNome4 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome5 = '' then WHR5 := '' else
      WHR5 := 'OR ' + NomeDoCampoNome5 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome6 = '' then WHR6 := '' else
      WHR6 := 'OR ' + NomeDoCampoNome6 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    //
    Result := UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrSB, DataBase, [
{
    'SELECT ' + NomeDoCampoCodigo + ' Codigo, ',
    NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ',
    'CASE WHEN ' + NomeDoCampoTipo + '=' + IntToStr(Tipo1),
    'THEN UCASE(' + NomeDoCampoNome1 + ') ELSE UCASE('+NomeDoCampoNome2,
    ') END NOME FROM ' + lowerCase(nomedatabela),
    'WHERE (CASE WHEN ' + NomeDoCampoTipo + '=' + IntToStr(Tipo1),
    'THEN UCASE(' + NomeDoCampoNome1 + ') ELSE UCASE(' + NomeDoCampoNome2,
    ') END) LIKE UCASE("' + VAR_NOME_S + '")',
}
    'SELECT ' + NomeDoCampoCodigo + ' Codigo,  ',
    NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu,  ',
    'CONCAT(',
    SQLx,
    //'"") NOME FROM ' + LowerCase(NomeDaTabela),
    ') NOME FROM ' + LowerCase(NomeDaTabela),
    WHR1,
    WHR2,
    WHR3,
    WHR4,
    WHR5,
    WHR6,
    ')',
    Extra,
    SQLNeg,
    ' ORDER BY NOME',
    '']);
    //
    //Geral.MB_Teste(DmodG.QrSB.SQL.Text);
  except
    Geral.MB_ERRO('N�o foi poss�vel a localiza��o pelo nome!' + sLineBreak +
    sLineBreak + 'SQL:' + sLineBreak + DmodG.QrSB.SQL.Text);
    //
    raise;
  end;
  if (DmodG.QrSB.RecordCount = 0) then
    Erro := 'N�o foi encontrado nenhum registro!';
{
  // Marco 2012-06-07
  if SohNum then
    VAR_NOME_S := '%' + Geral.SoNumero_TT(VAR_NOME_S) + '%';
  // Marco 2012-06-07
  DmodG.QrSB.Close;
  DmodG.QrSB.SQL.Clear;
  DmodG.QrSB.Database := DataBase;
  DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, ');
  DmodG.QrSB.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
  DmodG.QrSB.SQL.Add('CASE WHEN '+NomeDoCampoTipo+'='+IntToStr(Tipo1));
  DmodG.QrSB.SQL.Add('THEN UCASE('+NomeDoCampoNome1+') ELSE UCASE('+NomeDoCampoNome2);
  DmodG.QrSB.SQL.Add(') END NOME FROM '+lowercase(nomedatabela));
  DmodG.QrSB.SQL.Add('WHERE (CASE WHEN '+NomeDoCampoTipo+'='+IntToStr(Tipo1));
  DmodG.QrSB.SQL.Add('THEN UCASE('+NomeDoCampoNome1+') ELSE UCASE('+NomeDoCampoNome2);
  DmodG.QrSB.SQL.Add(') END) LIKE UCASE("'+ VAR_NOME_S +'")');
  case VAR_GOTONEG of
    gotoNeg : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
    gotoNiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
    gotoAll : ;//
    gotoNiP : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
    gotoPiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
    gotoPos : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
  end;
  DmodG.QrSB.SQL.Add(Extra);
  DmodG.QrSB.SQL.Add(' ORDER BY NOME');
  UMyMod.AbreQuery(DmodG.QrSB, DataBase, 'TUnCuringa.CriaFormE');
  //
  if DmodG.QrSB.RecordCount = 0 then
    Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
  else
}
end;
function TUnCuringa.ExecutouPesq06((*NomeDoCampoCodigo, NomeDoCampoNome1,
  NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4, NomeDoCampoNome5,
  NomeDoCampoNome6, NomeDaTabela, NomeDoCampoTipo: String; Tipo1: Integer;*)
  DataBase: TmySQLDatabase(*; Extra: string; SohNum: Boolean;
  var Erro: String*)): Boolean;
begin
  Result := UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrSB, DataBase, [
  'SELECT ent.Codigo Codigo,   ',
  'ent.CodUsu CodUsu,   ',
  'CONCAT( ',
  'CASE WHEN ent.RazaoSocial<>"" THEN CONCAT(" [R] ", ent.RazaoSocial) ELSE "" END,  ',
  'CASE WHEN ent.Nome<>"" THEN CONCAT(" [N] ", ent.Nome) ELSE "" END,  ',
  '" [", ',
  'CASE WHEN ent.Tipo=0 THEN uf0.Nome ELSE uf1.Nome END,  ',
  '" ", ',
  'CASE WHEN ent.Tipo=0 THEN  ',
  '  CONCAT( ',
  '    SUBSTR(CNPJ, 1, 2), ".",  ',
  '    SUBSTR(CNPJ, 3, 3), ".",  ',
  '    SUBSTR(CNPJ, 6, 3), "/",  ',
  '    SUBSTR(CNPJ, 9, 4), "-",  ',
  '    SUBSTR(CNPJ, 11, 2))  ',
  '  ELSE  ',
  '  CONCAT( ',
  '    SUBSTR(CPF, 1, 3), ".",  ',
  '    SUBSTR(CPF, 4, 3), ".",  ',
  '    SUBSTR(CPF, 7, 3), "-",  ',
  '    SUBSTR(CPF, 9, 2)) ',
  ' END, ',
  '"] ",  ',
  'CASE WHEN ent.Fantasia<>"" THEN CONCAT(" [F] ", ent.Fantasia) ELSE "" END,  ',
  'CASE WHEN ent.Apelido<>"" THEN CONCAT(" [A] ", ent.Apelido) ELSE "" END ',
  ') NOME  ',
  'FROM entidades ent ',
  'LEFT JOIN ufs uf0 ON ent.EUF = uf0.Codigo ',
  'LEFT JOIN ufs uf1 ON ent.PUF = uf1.Codigo ',
  'WHERE (ent.RazaoSocial LIKE UCASE("' + VAR_NOME_S + '")  ',
  'OR ent.Nome LIKE UCASE("' + VAR_NOME_S + '")  ',
  'OR ent.Fantasia LIKE UCASE("' + VAR_NOME_S + '")  ',
  'OR ent.Apelido LIKE UCASE("' + VAR_NOME_S + '")  ',
  'OR ent.CNPJ LIKE UCASE("' + VAR_NOME_S + '")  ',
  'OR ent.CPF LIKE UCASE("' + VAR_NOME_S + '")  ',
  ') ',
  'AND ent.Codigo>=0 ',
  'ORDER BY NOME ',
  '']);
end;

function TUnCuringa.ExecutouPesqLivre(TextoAPesquisar, SQL_Livre: String;
  DataBase: TmySQLDatabase; var Erro: String): Boolean;
var
  SQL, x: String;
begin
  //x := '%' + TextoAPesquisar + '%';
  x := TextoAPesquisar;
  SQL := StringReplace(SQL_Livre, CO_JOKE_SQL, x, [rfReplaceAll, rfIgnoreCase]);
  //
  Result := UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrSB, DataBase, [
  SQL]);
end;

{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.NomeDoCampoCodUsu(Database: TmySQLDataBase; NomeDaTabela,
  NomeDoCampoCodigo: String): String;
var
  P: Integer;
  NomeTab: String;
begin
  P := Pos(' ', NomeDaTabela); 
  if P > 0 then
    NomeTab := Copy(NomeDaTabela, 1, P - 1)
  else
    NomeTab := NomeDaTabela;
  //
  DmodG.QrAux_.Close;
  DmodG.QrAux_.Database := DataBase;
  DmodG.QrAux_.SQL.Clear;
  DmodG.QrAux_.SQL.Add('DESCRIBE ' + lowercase(NomeTab) +' "CodUsu" ');
  UMyMod.AbreQuery(DmodG.QrAux_, DataBase, 'TUnCuringa.NomeDoCampoCodUsu');
  if DmodG.QrAux_.RecordCount > 0 then
    Result := 'CodUsu'
  else
    Result := NomeDoCampoCodigo;
end;
{$ENDIF}

function TUnCuringa.ObtemSQLPesquisaText(Mascara: Integer; Texto: String): String;
var
  Txt, Res: String;
begin
  Txt := Texto;
  //
  if Mascara in ([0, 3]) then
    Txt := StringReplace(Txt, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Res := '';
  //
  if Mascara in ([0, 1, 2]) then
    Res := '%';
  //
  Res := Res + Txt;
  //
  if Mascara in ([0, 1, 4]) then
    Res := Res + '%';
  //
  Result := Res;
end;

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.Pesquisa(NomeDoCampoCodigo, NomeDoCampoNome,
  NomeDaTabela: String; DataBase: TmySQLDatabase; Extra: string;
  Edit: TdmkEdit; DBCB: TdmkDBLookupComboBox; DataType: TAllFormat;
  AbreQuery: Boolean = False): Boolean;
begin
  Result := False;
  MyObjects.CriaForm_AcessoTotal(TFmPesqDescri, FmPesqDescri);
  FmPesqDescri.FDataType := DataType;
  case DataType of
    dmktfInteger:
    begin
      DmodG.QrSB.Close;
      DmodG.QrSB.SQL.Clear;
      DmodG.QrSB.Database := DataBase;
      DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, ');
      DmodG.QrSB.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
      DmodG.QrSB.SQL.Add(NomeDoCampoNome+' Nome FROM '+lowercase(nomedatabela));
      DmodG.QrSB.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
      case VAR_GOTONEG of
        gotoNeg : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
        gotoNiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
        gotoAll : ;//
        gotoNiP : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
        gotoPiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
        gotoPos : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
      end;
      DmodG.QrSB.SQL.Add(Extra);
      DmodG.QrSB.SQL.Add(' ORDER BY '+NomeDoCampoNome);
      //
      FmPesqDescri.DBGrid1.DataSource := DmodG.DsSB;
      //
      if AbreQuery then
      begin
        DmodG.QrSB.Params[0].AsString := '%%';
        UMyMod.AbreQuery(DmodG.QrSB, DataBase);
      end;
    end
    else begin
      Geral.MB_Erro(
      '"DataType" n�o informado na function "UnCuringa.Pesquisa"! AVISE A DERMATEK');
      Exit;
    end;
  end;
  FmPesqDescri.ShowModal;
  if FmPesqDescri.FValor <> Null then
  begin
    if Edit <> nil then
      Edit.ValueVariant := FmPesqDescri.FValor;
    if DBCB <> nil then
      DBCB.KeyValue     := FmPesqDescri.FValor;
  end;
  FmPesqDescri.Destroy;
  Result := True;
end;

function TUnCuringa.Pesquisa2(const NomeDoCampoCodigo, NomeDoCampoNome,
  NomeDaTabela: String; const DataBase: TmySQLDatabase; const Extra: string;
  var Retorno: Variant; const DataType: TAllFormat;
  AbreQuery: Boolean): Boolean;
begin
  Result := False;
  MyObjects.CriaForm_AcessoTotal(TFmPesqDescri, FmPesqDescri);
  FmPesqDescri.FDataType := DataType;
  case DataType of
    dmktfInteger:
    begin
      DmodG.QrSB.Close;
      DmodG.QrSB.SQL.Clear;
      DmodG.QrSB.Database := DataBase;
      DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, ');
      DmodG.QrSB.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
      DmodG.QrSB.SQL.Add(NomeDoCampoNome+' Nome FROM '+lowercase(nomedatabela));
      DmodG.QrSB.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
      case VAR_GOTONEG of
        gotoNeg : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
        gotoNiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
        gotoAll : ;//
        gotoNiP : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
        gotoPiZ : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
        gotoPos : DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
      end;
      DmodG.QrSB.SQL.Add(Extra);
      DmodG.QrSB.SQL.Add(' ORDER BY '+NomeDoCampoNome);
      //
      FmPesqDescri.DBGrid1.DataSource := DmodG.DsSB;
      //
      if AbreQuery then
      begin
        DmodG.QrSB.Params[0].AsString := '%%';
        UMyMod.AbreQuery(DmodG.QrSB, DataBase);
      end;
    end
    else begin
      Geral.MB_Erro(
      '"DataType" n�o informado na function "UnCuringa.Pesquisa"! AVISE A DERMATEK');
      Exit;
    end;
  end;
  FmPesqDescri.ShowModal;
  Retorno := FmPesqDescri.FValor;
  Result := Retorno <> Null;
  FmPesqDescri.Destroy;
end;

{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaForm(NomeDoCampoCodigo, NomeDoCampoNome,
  NomeDaTabela: String; DataBase: TmySQLDatabase; Extra: string;
  ReturnCodUsu: Boolean = False; LEFT_JOIN: String = '';
  TextoIni: String = ''): Integer;
var
  TextoAPesquisar, Erro, TxtOriginal: String;
  Continua: Boolean;
begin
  try
    VAR_CORDA_CODIGOS := '';
    Result := 0;
    MyObjects.FormCria(TFmNomeX, FmNomeX);
    FmNomeX.Caption := 'Procura e Lista';
    FmNomeX.EdTxt.Text := TextoIni;
    FmNomeX.ShowModal;
    TextoAPesquisar := FmNomeX.FTexto;
    TxtOriginal := FmNomeX.FOrigi;
    Continua := FmNomeX.FContinua;
    FmNomeX.Destroy;
    VAR_CODIGO := 0;
    if Continua = True then
    begin
      if ExecutouPesq01(TextoAPesquisar, NomeDoCampoCodigo, NomeDoCampoNome, NomeDaTabela,
      DataBase, Extra, LEFT_JOIN, Erro) then
      begin
        MyObjects.FormCria(TFmCuringa, FmCuringa);
        FmCuringa.EdPesq.Text := TxtOriginal;

        FmCuringa.FTextoAPesquisar   := TextoAPesquisar;
        FmCuringa.FNomeDoCampoCodigo := NomeDoCampoCodigo;
        FmCuringa.FNomeDoCampoNome1  := NomeDoCampoNome;
        FmCuringa.FNomeDoCampoNome2  := '?';
        FmCuringa.FNomeDoCampoNome3  := '?';
        FmCuringa.FNomeDoCampoNome4  := '?';
        FmCuringa.FNomeDaTabela      := NomeDaTabela;
        FmCuringa.FDataBase          := DataBase;
        FmCuringa.FExtra             := Extra;
        FmCuringa.FLEFT_JOIN         := LEFT_JOIN;
        FmCuringa.FNomeDoCampoTipo   := '';
        FmCuringa.FTipo1             := 0;
        FmCuringa.FSohNum            := False;

        //Deve ser depois!
        FmCuringa.FOrigemPesquisa      := fpcTexto;
        FmCuringa.FPodeReabrirPesquisa := True;
        FmCuringa.PnPesquisa.Visible   := True;

        MyObjects.Informa2(FmCuringa.LaAviso1, FmCuringa.LaAviso2, False, Erro);

        FmCuringa.ShowModal;
        FmCuringa.Destroy;
      end;
      //
      if ReturnCodUsu then
        Result := VAR_CODUSU
      else
        Result := VAR_CODIGO;
    end;
  finally
    DmodG.QrSB.Close;
  end;
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaForm0(NomeDoCampoCodigo, NomeDoCampoNomeA,
  NomeDoCampoNomeB, NomeDaTabela: String; DataBase: TmySQLDatabase;
  Extra: string; ReturnCodUsu: Boolean = False): Integer;
var
  Nome: String;
var
  Continua: Boolean;
begin
  Result := 0;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura e Lista';
  FmNomeX.ShowModal;
  Nome := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  VAR_CODIGO := 0;
  if Continua = True then
  begin
    DmodG.QrSB.Close;
    DmodG.QrSB.Database := DataBase;
    DmodG.QrSB.SQL.Clear;
    //
    DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, CodUsu, ');
    DmodG.QrSB.SQL.Add('IF(' + NomeDoCampoNomeA + ' LIKE "%' +
    Nome + '%", ' + NomeDoCampoNomeA + ', ' + NomeDoCampoNomeB + ') Nome ');
    DmodG.QrSB.SQL.Add('FROM ' + NomeDaTabela);
    DmodG.QrSB.SQL.Add('WHERE ');
    DmodG.QrSB.SQL.Add('(');
    DmodG.QrSB.SQL.Add('  ' + NomeDoCampoNomeA + ' LIKE "%' + Nome + '%"');
    DmodG.QrSB.SQL.Add('  OR');
    DmodG.QrSB.SQL.Add('  ' + NomeDoCampoNomeB + ' LIKE "%' + Nome + '%"');
    DmodG.QrSB.SQL.Add(')');
    DmodG.QrSB.SQL.Add('AND ' + NomeDoCampoCodigo + '>0');
    DmodG.QrSB.SQL.Add('ORDER BY NOME');
    {
    DmodG.QrSB.SQL.Add('SELECT ' + NomeDoCampoCodigo + ' Codigo, ');
    DmodG.QrSB.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
    DmodG.QrSB.SQL.Add(NomeDoCampoNome + ' Nome FROM ' + lowercase(NomeDaTabela));
    DmodG.QrSB.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
    if not VAR_GOTONEG then DmodG.QrSB.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
    DmodG.QrSB.SQL.Add(Extra);
    DmodG.QrSB.SQL.Add(' ORDER BY '+NomeDoCampoNome);
    DmodG.QrSB.Params[0].AsString := Nome;
    }
    try
      UMyMod.AbreQuery(DmodG.QrSB, DataBase, 'TUnCuringa.CriaForm0');
    except
      Geral.MB_Aviso('N�o foi poss�vel a localiza��o pelo nome!' +
      sLineBreak + sLineBreak + 'SQL:' + sLineBreak + DmodG.QrSB.SQL.Text);
    end;
    if DmodG.QrSB.RecordCount = 0 then
      Geral.MB_Erro('N�o foi encontrado nenhum registro!')
    else
      MyObjects.FormShow(TFmCuringa, FmCuringa);
    //
    if ReturnCodUsu then
      Result := VAR_CODUSU
    else
      Result := VAR_CODIGO;
    DmodG.QrSB.Close;
  end;
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaForm2(NomeDoCampoFlutuante, NomeDoCampoNome,
  NomeDaTabela: String; DataBase: TmySQLDatabase; Extra: string): Double;
var
  Nome : String;
  Continua: Boolean;
begin
  Result := 0;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura e Lista';
  FmNomeX.ShowModal;
  Nome := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  VAR_CODIGO2 := 0;
  if Continua = True then
  begin
    DmodG.QrSB2.Close;
    DmodG.QrSB2.SQL.Clear;
    DmodG.QrSB2.Database := DataBase;
    DmodG.QrSB2.SQL.Add('SELECT '+NomeDoCampoFlutuante+' Codigo, ');
    DmodG.QrSB2.SQL.Add(NomeDoCampoNome+' Nome FROM '+lowercase(nomedatabela));
    DmodG.QrSB2.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
    case VAR_GOTONEG of
      gotoNeg : DmodG.QrSB2.SQL.Add(' AND '+NomeDoCampoFlutuante+'<0');
      gotoNiZ : DmodG.QrSB2.SQL.Add(' AND '+NomeDoCampoFlutuante+'<=0');
      gotoAll : ;//
      gotoNiP : DmodG.QrSB2.SQL.Add(' AND '+NomeDoCampoFlutuante+'<>0');
      gotoPiZ : DmodG.QrSB2.SQL.Add(' AND '+NomeDoCampoFlutuante+'>=0');
      gotoPos : DmodG.QrSB2.SQL.Add(' AND '+NomeDoCampoFlutuante+'>0');
    end;
    DmodG.QrSB2.SQL.Add(Extra);
    DmodG.QrSB2.SQL.Add(' ORDER BY '+NomeDoCampoNome);
    DmodG.QrSB2.Params[0].AsString := Nome;
    UMyMod.AbreQuery(DmodG.QrSB2, DataBase, 'TUnCuringa.CriaForm2');

    if DmodG.QrSB2.RecordCount = 0 then
      Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
    else
    begin
      MyObjects.FormCria(TFmCuringa, FmCuringa);
      FmCuringa.DBGrid1.DataSource := DmodG.DsSB2;
      FmCuringa.ShowModal;
      FmCuringa.Destroy;
    end;
    Result := VAR_CODIGO2;
    DmodG.QrSB2.Close;
  end;
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaForm3(NomeDoCampoData, NomeDoCampoNome, NomeDaTabela1,
  NomeDaTabela2: String; DataBase: TmySQLDatabase; Extra: string): TDate;
var
  Continua: Boolean;
begin
  Result := 0;
  Continua := True;
  VAR_CODIGO3 := 0;
  if Continua = True then
  begin
    DmodG.QrSB3.Close;
    DmodG.QrSB3.SQL.Clear;
    DmodG.QrSB3.Database := DataBase;
    DmodG.QrSB3.SQL.Add('SELECT t1.'+NomeDoCampoData+' Codigo, ');
    DmodG.QrSB3.SQL.Add('t2.'+NomeDoCampoNome+' Nome FROM '+lowercase(nomedatabela1)+' t1, ');
    DmodG.QrSB3.SQL.Add(Lowercase(NomeDaTabela2)+' t2 ');
    case VAR_GOTONEG of
      gotoNeg : DmodG.QrSB3.SQL.Add(' WHERE '+NomeDoCampoData+'<0');
      gotoNiZ : DmodG.QrSB3.SQL.Add(' WHERE '+NomeDoCampoData+'<=0');
      gotoAll : ;//
      gotoNiP : DmodG.QrSB3.SQL.Add(' WHERE '+NomeDoCampoData+'<>0');
      gotoPiZ : DmodG.QrSB3.SQL.Add(' WHERE '+NomeDoCampoData+'>=0');
      gotoPos : DmodG.QrSB3.SQL.Add(' WHERE '+NomeDoCampoData+'>0');
    end;
    if VAR_GOTONEG = gotoAll then
      DmodG.QrSB3.SQL.Add('WHERE '+Extra)
    else
      DmodG.QrSB3.SQL.Add('AND '+Extra);
    //  
    DmodG.QrSB3.SQL.Add(' ORDER BY t1.'+NomeDoCampoData);
    UMyMod.AbreQuery(DmodG.QrSB3, DataBase, 'TUnCuringa.CriaForm3');

    if DmodG.QrSB3.RecordCount = 0 then
      Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
    else
    begin
      MyObjects.FormCria(TFmCuringa, FmCuringa);
      FmCuringa.DBGrid1.DataSource := DmodG.DsSB3;
      FmCuringa.ShowModal;
      FmCuringa.Destroy;
    end;
    Result := VAR_CODIGO3;
    DmodG.QrSB3.Close;
  end;
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaForm4(NomeDoCampoCodigo, NomeDoCampoNome,
  NomeDaTabela: String; DataBase: TmySQLDatabase; Extra: string): String;
var
  Nome : String;
var
  Continua: Boolean;
begin
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura e Lista';
  FmNomeX.ShowModal;
  Nome := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  VAR_CODIGO4 := '';
  if Continua = True then
  begin
    DmodG.QrSB4.Close;
    DmodG.QrSB4.SQL.Clear;
    DmodG.QrSB4.Database := DataBase;
    DmodG.QrSB4.SQL.Add('SELECT '+NomeDoCampoCodigo+' Codigo, ');
    DmodG.QrSB4.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
    DmodG.QrSB4.SQL.Add(NomeDoCampoNome+' Nome FROM '+lowercase(nomedatabela));
    DmodG.QrSB4.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
    case VAR_GOTONEG of
      gotoNeg : DmodG.QrSB4.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
      gotoNiZ : DmodG.QrSB4.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
      gotoAll : ;//
      gotoNiP : DmodG.QrSB4.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
      gotoPiZ : DmodG.QrSB4.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
      gotoPos : DmodG.QrSB4.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
    end;
    DmodG.QrSB4.SQL.Add(Extra);
    DmodG.QrSB4.SQL.Add(' ORDER BY '+NomeDoCampoNome);
    DmodG.QrSB4.Params[0].AsString := Nome;
    UMyMod.AbreQuery(DmodG.QrSB4, DataBase, 'TUnCuringa.CriaForm4');

    if DmodG.QrSB4.RecordCount = 0 then
      Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
    else
    begin
      MyObjects.FormCria(TFmCuringa, FmCuringa);
      FmCuringa.DBGrid1.DataSource := DmodG.DsSB4;
      FmCuringa.ShowModal;
      FmCuringa.Destroy;
    end;
    Result := VAR_CODIGO4;
    DmodG.QrSB4.Close;
  end;
end;
{$ENDIF}

{$IFNDEF NAO_USA_DB_GERAL}
function TUnCuringa.CriaForm5(NomeDoCampoCodigo, NomeDoCampoNome,
  NomeDaTabela: String; DataBase: TmySQLDatabase; Extra: string): Longint;
var
  Nome : String;
var
  Continua: Boolean;
begin
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura e Lista';
  FmNomeX.ShowModal;
  Nome := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  VAR_CODIGO5 := 0;
  if Continua = True then
  begin
    DmodG.QrSB5.Close;
    DmodG.QrSB5.SQL.Clear;
    DmodG.QrSB5.Database := DataBase;
    DmodG.QrSB5.SQL.Add('SELECT '+NomeDoCampoCodigo+' Codigo, ');
    DmodG.QrSB5.SQL.Add(NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ');
    DmodG.QrSB5.SQL.Add(NomeDoCampoNome+' Nome FROM '+lowercase(nomedatabela));
    DmodG.QrSB5.SQL.Add(' WHERE '+NomeDoCampoNome+' LIKE :P0');
    case VAR_GOTONEG of
      gotoNeg : DmodG.QrSB5.SQL.Add(' AND '+NomeDoCampoCodigo+'<0');
      gotoNiZ : DmodG.QrSB5.SQL.Add(' AND '+NomeDoCampoCodigo+'<=0');
      gotoAll : ;//
      gotoNiP : DmodG.QrSB5.SQL.Add(' AND '+NomeDoCampoCodigo+'<>0');
      gotoPiZ : DmodG.QrSB5.SQL.Add(' AND '+NomeDoCampoCodigo+'>=0');
      gotoPos : DmodG.QrSB5.SQL.Add(' AND '+NomeDoCampoCodigo+'>0');
    end;
    DmodG.QrSB5.SQL.Add(Extra);
    DmodG.QrSB5.SQL.Add(' ORDER BY '+NomeDoCampoNome);
    DmodG.QrSB5.Params[0].AsString := Nome;
    UMyMod.AbreQuery(DmodG.QrSB5, DataBase, 'TUnCuringa.CriaForm5');

    if DmodG.QrSB5.RecordCount = 0 then
      Geral.MB_Aviso('N�o foi encontrado nenhum registro!')
    else
    begin
      MyObjects.FormCria(TFmCuringa, FmCuringa);
      FmCuringa.DBGrid1.DataSource := DmodG.DsSB5;
      FmCuringa.ShowModal;
      FmCuringa.Destroy;
    end;
    Result := VAR_CODIGO5;
    DmodG.QrSB5.Close;
  end;
end;
{$ENDIF}

end.

