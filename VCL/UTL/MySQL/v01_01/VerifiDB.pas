unit VerifiDB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts,
  mySQLDbTables, dmkGeral, dmkImage, CheckLst, dmkEdit, UnDMkEnums, Vcl.Menus,
  UnGrl_Vars;

Type
  TFmVerifiDB = class(TForm)
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkRegObrigat: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    CkEstrutLoc: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    CkEntidade0: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    LaAvisoG1: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoG2: TLabel;
    LaAvisoB2: TLabel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel4: TPanel;
    BtSair: TBitBtn;
    BtConfirma: TBitBtn;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    LaAvisoP1: TLabel;
    LaAvisoP2: TLabel;
    EdPqVeri: TdmkEdit;
    PMClFldsNoNeed: TPopupMenu;
    Excluicampos1: TMenuItem;
    N1: TMenuItem;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    PMTabsNoNeed: TPopupMenu;
    N2: TMenuItem;
    Excluitabelas1: TMenuItem;
    Listatabelas1: TMenuItem;
    Marcartodos2: TMenuItem;
    Desmarcartodos2: TMenuItem;
    N3: TMenuItem;
    CorrigeCodUsu1: TMenuItem;
    PMClIdxsNoNeed: TPopupMenu;
    MenuItem2: TMenuItem;
    MarcarTodos3: TMenuItem;
    DesmarcarTodos3: TMenuItem;
    ExcluirIndices1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    ClTabsNoNeed: TCheckListBox;
    Panel10: TPanel;
    BtTabsNoNeed: TButton;
    TabSheet3: TTabSheet;
    ClFldsNoNeed: TCheckListBox;
    Panel9: TPanel;
    BtClFldsNoNeed: TButton;
    TabSheet4: TTabSheet;
    ClIdxsNoNeed: TCheckListBox;
    Panel16: TPanel;
    BtClIdxsNoNeed: TButton;
    CkQeiLnk: TCheckBox;
    CkVAR_VERIFICA_BD_FIELD_DEFAULT_VALUE: TCheckBox;
    Label1: TLabel;
    EdTabIni: TdmkEdit;
    TabSheet5: TTabSheet;
    Button1: TButton;
    MeAcoesExtras: TMemo;
    Button2: TButton;
    Button3: TButton;
    CkJanelas: TCheckBox;
    Button4: TButton;
    procedure BtSairClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Excluicampos1Click(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure BtClFldsNoNeedClick(Sender: TObject);
    procedure Excluitabelas1Click(Sender: TObject);
    procedure Listatabelas1Click(Sender: TObject);
    procedure Marcartodos2Click(Sender: TObject);
    procedure Desmarcartodos2Click(Sender: TObject);
    procedure BtTabsNoNeedClick(Sender: TObject);
    procedure CorrigeCodUsu1Click(Sender: TObject);
    procedure PMTabsNoNeedPopup(Sender: TObject);
    procedure PMClFldsNoNeedPopup(Sender: TObject);
    procedure ExcluirIndices1Click(Sender: TObject);
    procedure MarcarTodos3Click(Sender: TObject);
    procedure DesmarcarTodos3Click(Sender: TObject);
    procedure PMClIdxsNoNeedPopup(Sender: TObject);
    procedure BtClIdxsNoNeedClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FVerifi: Boolean;
    FDBExtra: TmySQLDataBase;
  end;

var
  FmVerifiDB: TFmVerifiDB;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal, UMySQLModule, UMySQLDB;

{$R *.DFM}

procedure TFmVerifiDB.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDB.BtTabsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTabsNoNeed, BtTabsNoNeed);
end;

procedure TFmVerifiDB.Button1Click(Sender: TObject);
begin
  MeAcoesExtras.Lines.Clear;
  //
  if USQLDB.CampoExiste('ctrlgeral', 'ERPNameByCli', DMod.MyDB) then
  begin
    Dmod.MyDB.Execute('UPDATE ctrlgeral SET ERPNameByCli="0" WHERE ERPNameByCli IS NULL')
  end;
end;

procedure TFmVerifiDB.Button2Click(Sender: TObject);
const
  Avisa = True;
  RecriaRegObrig = True;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.MyDB.Execute('DELETE FROM drecfgtp1');
    DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'drecfgtp1', 'drecfgtp1',
    MeAcoesExtras, Avisa, RecriaRegObrig, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVerifiDB.Button3Click(Sender: TObject);
const
  Avisa = True;
  RecriaRegObrig = True;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.MyDB.Execute('DELETE FROM drecfgtp2');
    DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'drecfgtp2', 'drecfgtp2',
    MeAcoesExtras, Avisa, RecriaRegObrig, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVerifiDB.Button4Click(Sender: TObject);
const
  Avisa = True;
  RecriaRegObrig = True;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.MyDB.Execute('DELETE FROM varfatid');
    DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'varfatid', 'varfatid',
    MeAcoesExtras, Avisa, RecriaRegObrig, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVerifiDB.CorrigeCodUsu1Click(Sender: TObject);
var
  Tabela: String;
begin
  FormStyle := fsNormal;
  // n�o precisa, j� pergunta no processo
  //if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  Tabela := 'entidades';
  if InputQuery('Corre��o de Tabela', 'Tabela a ser corrigida:', Tabela) then
    UMyMod.AtualizaEntidadesCodigoToCodUsu(Tabela);
  FormStyle := fsStayOnTop;
end;

procedure TFmVerifiDB.Desmarcartodos1Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbUnchecked, true, false);
end;

procedure TFmVerifiDB.Desmarcartodos2Click(Sender: TObject);
begin
  ClTabsNoNeed.CheckAll(cbUnchecked, true, false);;
end;

procedure TFmVerifiDB.Excluicampos1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropCamposTabelas(ClFldsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDB.ExcluirIndices1Click(Sender: TObject);
const
  Aviso = '';
var
  Tabela, IdxNome: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropIndicesTabelas(ClIdxsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDB.Excluitabelas1Click(Sender: TObject);
var
  //P, N,
  I: Integer;
  Tabela: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MB_Pergunta(
  'Deseja realmente excluir as tabelas selecionadas?') = ID_YES then
  begin
    try
      //N := 0;
      for I := ClTabsNoNeed.Items.Count - 1 downto 0 do
      begin
        if ClTabsNoNeed.Checked[I] then
        begin
          Tabela := ClTabsNoNeed.Items[I];
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DROP TABLE ' + Tabela);
          Dmod.QrUpd.ExecSQL;
          //
          ClTabsNoNeed.Items.Delete(I);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVerifiDB.BtClFldsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClFldsNoNeed, BtClFldsNoNeed);
end;

procedure TFmVerifiDB.BtClIdxsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClIdxsNoNeed, BtClIdxsNoNeed);
end;

procedure TFmVerifiDB.BtConfirmaClick(Sender: TObject);
var
  Versao, Resp: Integer;
  Verifica, EstrutLoc: Boolean;
  DBLoc: TmySQLDatabase;
begin
  VAR_VERIFICA_BD_FIELD_DEFAULT_VALUE := CkVAR_VERIFICA_BD_FIELD_DEFAULT_VALUE.Checked;
  VAR_CONTA_TABLE_START := Trim(EdTabIni.Text);

  {$IfNDef SemDBLocal}
  DBLoc     := Dmod.MyLocDatabase;
  EstrutLoc := CkEstrutLoc.Checked;
  {$Else}
  DBLoc     := nil;
  EstrutLoc := False;
  {$EndIf}
  //
  //
  Verifica := False;
  Versao   := USQLDB.ObtemVersaoAppDB(Dmod.MyDB);
  //
  if Versao <= CO_VERSAO then
    Verifica := True
  else
  begin
    Resp := Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
      'banco de dados atual. N�o � recomendado executar a verifica��o com '+
      'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
      'Confirma assim mesmo a verifica��o?');
    if Resp = ID_YES then
      Verifica := True;
  end;
  Update;
  Application.ProcessMessages;
  if Verifica then
  begin
    if CkEntidade0.Checked then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(DELETE_FROM + ' entidades');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=0');
      Dmod.QrUpd.ExecSQL;
    end;
    BtConfirma.Enabled := False;
    try
      DBCheck.EfetuaVerificacoes(Dmod.MyDB, DBLoc, Memo1,
        CkEstrutura.Checked, EstrutLoc, True,
        CkPergunta.Checked, CkRegObrigat.Checked, LaAvisoP1, LaAvisoP2,
        LaAvisoR1, LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2,
        LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, ProgressBar1,
        ClFldsNoNeed, ClIdxsNoNeed, CkQeiLnk.Checked, CkJanelas.Checked);
    finally
      BtConfirma.Enabled := True;
    end;
    //
    BtSair.Enabled      := True;
    CkPergunta.Checked  := False;
    CkEstrutLoc.Checked := False;
  end;
end;

procedure TFmVerifiDB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDB.FormClose(Sender: TObject; var Action: TCloseAction);
  (*
  function TemCampoNaTabela(Campo, Tabela: String): Boolean;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW FIELDS');
    Dmod.QrAux.SQL.Add('FROM ' + LowerCase(Tabela));
    Dmod.QrAux.SQL.Add('LIKE "' + Campo + '"');
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    Result := Dmod.QrAux.RecordCount > 0;
    Dmod.QrAux.Close;
  end;
  *)
begin
  (* Este procedimento s� era necess�rio na transi��o da NF-e 1.0 para a 2.0
  if TemCampoNaTabela('CorrGrad01', 'Controle') then
  begin
    if Geral.MB_Pergunta('� necess�rio reiniciar o aplicativo!' + sLineBreak +
    'Deseja reinici�-lo agora?') = ID_YES then
      Halt(0);
  end;
  *)
end;

procedure TFmVerifiDB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  EdPqVeri.Text       := VAR_PORQUE_VERIFICA;
  VAR_PORQUE_VERIFICA := '';
  //
  {$IfNDef SemDBLocal}
  CkEstrutLoc.Visible := True;
  {$Else}
  CkEstrutLoc.Visible := False;
  {$EndIf}
  //
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmVerifiDB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAvisoR1, LaAvisoR2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDB.Timer1Timer(Sender: TObject);
var
  DBLoc: TmySQLDatabase;
begin
  {$IfNDef SemDBLocal}
  DBLoc := Dmod.MyLocDatabase;
  {$Else}
  DBLoc := nil;
  {$EndIf}
  Timer1.Enabled := False;
  BtConfirma.Enabled := False;
  //
  try
    DBCheck.EfetuaVerificacoes(Dmod.MyDB, DBLoc, Memo1,
      CkEstrutura.Checked, False, True, False, CkRegObrigat.Checked,
      LaAvisoP1, LaAvisoP2, LaAvisoR1, LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1,
      LaAvisoG2, LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed,
      ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed, CkQeiLnk.Checked,
      CkJanelas.Checked);
  finally
    BtConfirma.Enabled := True;
  end;
  //
  BtSair.Enabled      := True;
  CkPergunta.Checked  := False;
  CkEstrutLoc.Checked := False;
end;

procedure TFmVerifiDB.FormShow(Sender: TObject);
begin
  // Est� tirando o icone da barra de tarefas!
  //FmPrincipal.Hide;
end;

procedure TFmVerifiDB.Listatabelas1Click(Sender: TObject);
begin
  Geral.MB_Info(ClTabsNoNeed.Items.Text);
end;

procedure TFmVerifiDB.Marcartodos1Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDB.Marcartodos2Click(Sender: TObject);
begin
  ClTabsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDB.MarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDB.DesmarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbUnchecked, true, false);
end;

procedure TFmVerifiDB.PMClFldsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClFldsNoNeed.Count - 1 do
  begin
    if ClFldsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  Excluicampos1.Enabled := N > 0;
end;

procedure TFmVerifiDB.PMClIdxsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClIdxsNoNeed.Count - 1 do
  begin
    if ClIdxsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  ExcluirIndices1.Enabled := N > 0;
end;

procedure TFmVerifiDB.PMTabsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClTabsNoNeed.Count - 1 do
  begin
    if ClTabsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  Excluitabelas1.Enabled := N > 0;
  Listatabelas1.Enabled  := N > 0;
end;

procedure TFmVerifiDB.FormHide(Sender: TObject);
begin
  //FmPrincipal.Show;
end;

end.

