
// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria
(*
12.1.1. BatchModify
Insert, update or delete records without refetching data from database after every operation.
Syntax:
property BatchModify: Boolean;
Description:
If you want implement batch Insert, Delete or Update operations, we suggest you to use
BatchModify property.
This property allows you increase operation speed. When you set BatchModify to True, DAC for
MySQL doesn't fetch data from database after each Insert or Update operations.
Use TMySQLQuery for batch Insert, Update or Delete operations.
TMySQLTable 214
� 1999-2015, Microolap Technologies
Example:
procedure TForm1.Button1Click(Sender: TObject);
var
I : Integer;
begin
MySQLTable1.BatchModify := True;
MySQLTable1.DisableControls;
for I := 1 to 1000 do
begin
MySQLTable1.Insert;
MySQLTable1.FieldByName('ID').AsInteger := I;
MySQLTable1.FieldByName('Name').AsString := 'name'+IntToStr(I);
MySQLTable1.Post;
end;
MySQLTable1.BatchModify := False;
MySQLTable1.EnableControls;
end;
*)
unit UMySQLModule;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls,
  UnInternalConsts2, Mask, ComCtrls, ResIntStrings, mySQLDbTables,
  Registry, ZCF2, Buttons, UnInternalConsts3, TypInfo, ModuleGeral,
  dbGrids, dmkGeral, MyDBCheck, dmkValUsu, DmkDAC_PF, dmkLabel, dmkEdit,
  dmkDBLookupCombobox, dmkEditCB, dmkEditDateTimePicker, dmkCheckGroup, dmkMemo,
  dmkRadioGroup, dmkCheckBox, dmkDBEdit, dmkPopOutFntCBox, Variants, GModule,
  dmkImage, UnMyLinguas, UnDmkProcFunc, dmkRichEdit, UnDmkEnums, UMySQLDB,
  UnGrl_Vars;

const
  TMaxConfWinControl = 255;

type
  TUMyArray_Str = array of String;
  TUMyArray_Var = array of Variant;
  TTipoExcluiTab = (tetNaoExclui, tetExcuiSoAntes, tetExcluiAntesEDepois);
  TBELY = (belyCodigo, belyControle, belyNumero);
  TConfWinControl = array[0..TMaxConfWinControl] of TControl;
  TUModule = class(TObject)

  private
    { Private declarations }
    (* N�o usa
    function  ValorNuloEVazio1(Query: TmySQLQuery; Campo: String): Boolean;
    *)
    function  ValorNuloEVazio2(Calsse: TClass; Objeto: TObject;
              Query: TmySQLQuery; Campo: String): Boolean;

    // L O G   D E   T A B E L A S
    function  SaveTmpLogTable(DataBase: TmySQLDatabase; TabelaOri,
              TabelaDes: String; LstKeyVal: TStringList): Boolean;
  public
    { T E M P O R A R I O S }
    procedure AtualizaEntidadesCodigoToCodUsu(Tabela: String);

    { Public declarations }
    //       // Delete registros SQLDelete

    function  ExcluiRegistro_EnviaArquivoMorto(TabMorto, Tab: String; Motivo: Integer;
              SQLIndex: array of String; ValIndex: array of Variant;
              DataBase: TmySQLDatabase; MostraMsg: Boolean = True): Boolean;
    function  ExcluiRegistrosDeTodaTabela(Qry: TmySQLQuery; Pergunta,
              Tabela: String): Integer;
    function  ExcluiRegistros(Pergunta: String; QrUpd: TmySQLQuery; Tabela:
              String; SQLCampos, Compare: array of String;
              ValCampos: array of Variant; ComplSQL: String): Boolean;
    function  GeraCondicaoDeSQL(var TextoSQL: String; const Tabela: String;
              const SQLCampos, Compare: array of String; const ValCampos:
              array of Variant; const FirstCondition, ComplSQL: String):
              Boolean;
    function  CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String): Boolean;
    function  CarregaSQLIns_ON_DUPLICATE_KEY(QrUpd: TmySQLQuery; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex, SQLUpdate: array of String;
              ValCampos, ValIndex, ValUpdate: array of Variant;
              UserDataAlterweb: Boolean;
              CampoIncrementa: String = '';
              Incremento: Integer = 1): Boolean;
    function  CarregaSQLReplace(QrUpd: TmySQLQuery; Tabela: String;
              //Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean): Boolean;
    function  SQLInsUpd_Old(QrUpd: TmySQLQuery; Tipo: String; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant): Boolean;
    function  LctUpd(QrUpd: TmySQLQuery; Tabela: String;
              SQLCampos: array of String; ValCampos: array of Variant;
              Data: TDateTime; Tipo, Carteira: Integer; Controle: Int64;
              Sub: Integer): Boolean;
    function  SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  SQLInsUpdAlterIdx(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex, SQLAlterIdx: array of String;
              ValCampos, ValIndex, ValAlterIdx: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
{   Fazer no UnFinanceiro!
    function  SQLInsUpd_Lct(QrUpd: TmySQLQuery; Tipo: TSQLType;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String): Boolean;
}
    function  SQLIns_ON_DUPLICATE_KEY(
              QrUpd: TmySQLQuery; Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex, SQLUpdate: array of String;
              ValCampos, ValIndex, ValUpdate: array of Variant;
              UserDataAlterweb: Boolean;
              CampoIncrementa: String = '';
              Incremento: Integer = 1): Boolean;
    function  SQLInsUpd_IGNORE(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = ''): Boolean;
    function  SQLInsUpd_UpdExtra(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex, SQLSoUpd: array of String;
              ValCampos, ValIndex, ValSoUpd: array of Variant;
              UserDataAlterWeb: Boolean): Boolean;
              //Novo CriaFormInsUpd = FormInsUpd_Show
    function  SQLReplace(QrUpd: TmySQLQuery; Tabela: String;
              //Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean): Boolean;
    function  FormInsUpd_Show(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; Query: TmySQLQuery;
              Acao: TSQLType): Boolean;
              // Multiplas queries
    function  FormInsUpd_Mul_Show(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; Queries: array of TmySQLQuery; Acao: TSQLType): Boolean;
             //para cria e fazer ShowModal manualmente
    function  FormInsUpd_Cria(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; Query: TmySQLQuery;
              Acao: TSQLType; Tabela: String = ''): Boolean;
              //
    function  FormInsUpd_Mul_Cria(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; Queries: array of TmySQLQuery;
              Acao: TSQLType): Boolean;
    function  ConfigPanelInsUpd(Acao: TSQLType; Form: TForm; Panel: TWinControl;
              Query: TmySQLQuery; WinCtrlsToHide, WinCtrlsToShow: array of TWinControl;
              CompoToFocus: TWinControl; CompoTipo: TControl; Tabela: String):
              Boolean;
    function  TravaFmEmPanelInsUpd(PanelsToHide, PanelsToShow: array of TPanel;
              ImgTipo: TdmkImage): Boolean;
    function  ExecSQLInsUpdFm(Form: TForm; Acao: TSQLType; Tabela:
              String; NewItem: Variant; QrExec: TmySQLQuery): Boolean;
    function  BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType; Atual: Integer;
              EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
    function  BuscaEmLivreY_Def_Geral(Table, Field: String; Acao: TSQLType; Atual: Integer;
              EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
    function  BuscaEmLivreY_Def_SPED_EFD(Table, Field: String; Acao: TSQLType; Atual: Integer;
              EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
    function  BuscaEmLivreY_Def_Old(Table, Field: String; Acao: String; Atual: Integer): Integer;
    function  BuscaProximaFatParcelaDeFatID(FatID: Integer; Tabela: String): Integer;
    //function  BuscaProximoCtrlGeral(Campo: String): Integer;
    function  BuscaProximoGerlSeq1Int32(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer;
              Reservados: Variant; DB: TmySQLDatabase = nil; Quantidade:
              Integer = 1): Integer;
    // mesmo que BuscaProximoGerlSeq1Int32!
    function  BPGS1I32(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer;
              DB: TmySQLDatabase = nil): Integer;
    function  BPGS1I32_Varios(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer;
              DB: TmySQLDatabase = nil; Quantidade: Integer = 1): Integer;
    function  BPGS1I32_Reaproveita(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
    function  BPGS1I64(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64;
              DB: TmySQLDatabase = nil): Int64;
    function  BPGS1I64_Reaproveita(Tabela, Campo, _WHERE, _AND: String;
              TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64;
              DB: TmySQLDatabase = nil): Int64;
    //
    function  SenhaEspecial: Boolean;
    function  AtualizaLaRegistro(Database: TmySQLDatabase; Tabela : String;
              RecCount, Increm : Integer) : String;
    function  IncrementaControle(Database:TmySQLDatabase; TabControle,
              FieldControl: String) : Integer;
    function  BuscaPrimeiroCodigoLivre(Tabela, Campo: String): Integer;
    function  BuscaIntSafe(Database: TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Integer;
    function  BuscaEmLivreY(Database:TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String; Forca_1: Boolean = True) : Integer;
    function  BuscaEmLivreY_MinMax(Database: TmySQLDatabase; TabLivre,
              TabControle, Table, FieldControl, FieldTable : String;
              Minimo, Maximo: Integer) : Integer;
    function  BuscaEmLivreY_Double(Database: TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Double;
    function  BuscaEmLivreInt64Y(Database: TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Int64;
    function  SelLockY(Registro : Integer; Database: TmySQLDatabase; Table, Field : String) : Boolean;
    function  SelLockY_Double(Registro : Double; Database: TmySQLDatabase; Table, Field : String) : Boolean;
    function  SelLockInt64Y(Registro : Int64; Database: TmySQLDatabase; Table, Field : String) : Boolean;
    function  SelLockY_Text(Registro : String; Database: TmySQLDatabase; Table, Field : String) : Boolean;
    function  SelLockQualquerY(Database: TmySQLDatabase; Table, NomeTable : String) : Boolean;
    function  SelLockA(Registro : String; Database: TmySQLDatabase; Table, Field : String) : Boolean;
    function  VerificaDuplicadoStr(Database: TmySQLDatabase; Tabela, CampoDescri, CampoCod, Descricao,
              CampoAnterior: String; Codigo : Integer) : Boolean;
    function  VerificaDuplicadoInt(Database: TmySQLDatabase; Tabela, CampoInteiro1, CampoInteiro2,
              CampoCod: String; Inteiro1, Inteiro2, Codigo : Integer) : Boolean;
    function  VerificaDuplicado1(Database: TmySQLDatabase; Tabela: String;
              CamposFind: array of String; ValoresFind: array of Variant;
              CampoOmit: String; ValorOmit: Variant): Boolean;
    function  VerificaDuplicadoNovo(SQLType: TSQLType; QrPsq: TMySQLQuery;
              Database: TmySQLDatabase; Tabela, MasterFld, PriKeyFld: String;
              MasterVal, PriKeyVal: Variant; ValorFld: String; ValorVal:
              Variant; ShowMsgDupl: Boolean): Boolean;
    function  NegaInclusaoY(Database: TmySQLDatabase; Tabela, Campo, TabLivre:
              String; Maximo: Integer) : Boolean;
    //function  AcessoNegadoAoForm(Tabela, Form : String; Msg: Integer) : Boolean;
    function  DiasUteis(DataI, DataF: TDateTime): Integer;
    function  DiasUteis2(DataI, DataF: TDateTime; DiasUteis: Integer): Integer;
    //function  FormShow(InstanceClass: TComponentClass;
              //Reference: TComponent; Form: TForm; ModoAcesso: TAcessFormMode): Boolean;
    //function  FormShow(InstanceClass: TComponentClass;
              //var Reference; ModoAcesso: TAcessFormModo): Boolean;
    {function  FormCria(ComponentClass: TComponentClass;
              Reference: TComponent; ModoAcesso: TAcessFormMode): Boolean;}
    //function  FormCria(InstanceClass: TComponentClass; var Reference;
              //ModoAcesso: TAcessFormModo): Boolean;
    //function  FormPode(InstanceClass: TComponentClass; var Reference;
      //        ModoAcesso: TAcessFormModo): Boolean;
    //
    function  DiaInutil(Data: TDateTime): Integer;
    procedure ProximoDiaUtil(const Data: TDateTime; const Dom, Seg, Ter,
              Qua, Qui, Sex, Sab, Feriado: Word;
              var Res: TDateTime; var DiaSemTxt: String);
    function  CalculaDataDeposito(Vencimento: TDateTime): TDateTime;
    function  CalculaDiasArray_Pula(Data, Vcto: TDateTime; DMai, Comp: Integer): MyArrayI1k;
    function  CalculaDiasArray_Normal(Data, Vcto: TDateTime; DMai, Comp: Integer): MyArrayI1k;
    function  CalculaDiasArray_NaoPula(Data, Vcto: TDateTime; DMai,
              Comp:Integer): MyArrayI1k;
    function  CalculaDias_Pula(Data, Vcto: TDateTime; DMai, Comp: Integer): Integer;
    function  CalculaDias_Normal(Data, Vcto: TDateTime; DMai, Comp: Integer): Integer;
    function  CalculaDias_NaoPula(Data, Vcto: TDateTime; DMai,
              Comp:Integer): Integer;
    function  CalculaDiasArray(Data, Vcto: TDateTime; DMai, Comp, Pula: Integer):
              MyArrayI1k;
    function  CalculaDias(Data, Vcto: TDateTime; DMai, Comp, Pula, CBE: Integer): Integer;
    function  DiaUtilMes(DataIni: TDateTime; DiaIni, QtdUteis, MaxDia: Integer): Integer;
    //
    function  ProximoRegistro(Query: TmySQLQuery; Campo: String; Atual:
              Integer): Integer;
    //Exclui todos registros
    function  ExcluiTodosRegistros(Pergunta, Tabela: String): Integer;
    //ExcluiRegistroI1
    function  ExcluiRegistroInt1(Pergunta, Tabela, Campo: String; Inteiro1:
              Integer; DB: TmySQLDatabase): Integer;
    function  ExcluiRegistroBig1(Pergunta, Tabela, Campo: String; BigInt1:
              LongInt; DB: TmySQLDatabase): Integer;
    //ExcluiRegistroI2
    function  ExcluiRegistroInt2(Pergunta, Tabela, Campo1, Campo2: String;
              Inteiro1, Inteiro2: Integer; DataBase: TmySQLDatabase = nil): Integer;
    function  ExcluiRegistroIntArr(Pergunta, Tabela: String; Campos: array of String;
              Inteiros: array of Integer): Integer;
    function  ExcluiRegistroTxt1(Pergunta, Tabela, Campo: String;
              Texto1, ComplementoSQL: String; DB: TMySQLDataBase): Integer;
    function  ObtemCamposDeTabelaIdentica(const DataBase: TmySQLDatabase;
              const Tabela, Prefix: String; var QtdFlds: Integer): String;// Prefix -> 'la.' = ref tabela
    {
    function  ObtemCamposDeTabelaParecida(DataBase: TmySQLDatabase;
              Origem, Destino, Prefix: String): String;// Prefix -> 'la.' = ref tabela
    }
    function  ArrayDeTabelaIdentica_Campos(DataBase: TmySQLDatabase;
              Tabela: String): TUMyArray_Str;
    function  ArrayDeTabelaIdentica_Values(Tabela: TmySQLQuery): TUMyArray_Var;
    function  ArrayDeTabelaIdentica_Values2(Tabela: TmySQLQuery): TUMyArray_Var;
    function  ConfigJanela(Janela: String; Lista: TConfWinControl): Boolean;
    function  ConfigJanela10(Janela: String;
              c0, c1, c2, c3, c4, c5, c6, c7, c8, c9:TControl): Boolean;
    function  ConfigJanela20(Janela: String;
              c0, c1, c2, c3, c4, c5, c6, c7, c8, c9,
              c10, c11, c12, c13, c14, c15, c16, c17, c18, c19:TControl): Boolean;

    procedure LogDel(Database: TMySQLDataBase; Tipo, ID: Integer);
    procedure LogIns(Database: TMySQLDataBase; Tipo, ID: Integer);
    procedure UpdateControleData(Database: TmySQLDatabase; TabControle, Field : String; Data : TDate);
    procedure UpdLockY(Registro : Integer; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdLockY_Text(Registro : String; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdLockY_Double(Registro : Double; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdLockA(Registro : String; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdLockInt64Y(Registro : Int64; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdUnlockY(Registro : Integer; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdUnlockY_Text(Registro : String; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdUnlockY_Double(Registro : Double; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdUnlockInt64Y(Registro : Int64; Database: TmySQLDatabase; Table, Field : String);
    procedure UpdLockTudoY(Database: TmySQLDatabase; Table : String);
    procedure UpdUnlockTudoW(Database: TmySQLDatabase; Table : String);
    procedure UpdUnlockA(Registro : String; Database: TmySQLDatabase; Table, Field : String);
    procedure PoeEmLivreY(Database: TmySQLDatabase; TabLivre, Table : String; Codigo : Integer);
    //procedure PoeEmLivreY_Text(Database: TmySQLDatabase; TabLivre, Table : String; Codigo : String);
    procedure PoeEmLivreY_Double(Database: TmySQLDatabase; TabLivre, Table : String; Codigo : Double);
    //function SituacaoLancamento(QrLct: TmySQLQuery): String; Usar TUnFinanceiro.NomeSitLancto(...
    // TmySQLTable
    function  VerificaCamposObrigatorios(Tabela: TMySQLTable): Boolean;
    procedure DadosAutomaticosTb(Tabela: TMySQLTable);
    procedure MostraEdicaoTb(Form: TForm);
    procedure OcultaEdicaoTb(Form: TForm);
    procedure AlteraRegistroTb(Form: TForm; Tabela: TmySQLTable;
              ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
    procedure IncluiRegistroTb(Form: TForm; Tabela: TmySQLTable;
              ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
    //procedure DesisteRegistroTb(Form: TForm; Tabela: TmySQLTable);
    procedure DesisteRegistroTb(Form: TForm; Tabela: TmySQLTable;
              NomeTabela, NomeCampoCodigo: String;
              ImgTipo: TdmkImage; CodigoALiberar: Integer);
    procedure ConfirmaRegistroTb_Codigo(Form: TForm; Tabela: TmySQLTable);
    procedure ConfirmaRegistroTb_Numero(Form: TForm; Tabela: TmySQLTable;
              ImgTipo: TdmkImage;
              PainelDados, PainelConfirma, PainelControle: TPanel);
    procedure BELY_Tb(Tabela: TmySQLTable; Campo: TBELY);
    procedure VerificaFeriadosFuturos(ComponentClass: TComponentClass;
              Reference: TComponent; SohSeNaoTem: Boolean = True);
    //
    function  SQLInsUpdL(QrUpd: TmySQLQuery; Tipo, Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant): Boolean;
    function  SQLDel1(QrExec, QrData: TmySQLQuery; Tabela, Campo: String;
              Valor: Variant; PerguntaSeExclui: Boolean;
              PerguntaAlternativa: String; ImpedeExclusao: Boolean): Boolean;
    function  SQLDel2(QrExec, QrData: TmySQLQuery; Tabela, CampoLoc: String;
              Campos: array of String; Valores: array of Variant;
              PerguntaSeExclui: Boolean; PerguntaAlternativa: String): Boolean;
    function  SQLLoc1(Query: TmySQLQuery; Tabela, Campo: String;
              Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
    //

    procedure ExportaRegistrosEntreDBs_Antigo(TabOrig, TabDest, CondicaoSQL: String;
              BaseOrig, BaseDest: TmySQLDatabase; RichEdit: TRichEdit);
    procedure ExportaRegistrosEntreDBs_Novo(TabOrig, TabDest, CondicaoSQL: String;
              BaseOrig, BaseDest: TmySQLDatabase; RichEdit: TRichEdit);
    function  EspelhaRegistroEntreTabelas(RichEdit: TRichEdit;
              //SQLCampos: array of String; ValCampos: array of Variant;
              TabOrig, TabDest: String; BaseOrig, BaseDest: TmySQLDatabase;
              WHERE_Condition: String): Boolean;
    // OpenQuery - QueryOpen          
    function  AbreQuery(Query: TmySQLQuery; DB: TmySQLDatabase;
              Aviso: String = ''): Boolean;
    function  AbreTable(Table: TmySQLTable; DB: TmySQLDatabase;
              Aviso: String = ''): Boolean;
    function  ReabreQuery(Query: TmySQLQuery; DB: TmySQLDatabase;
              Valores: array of Variant; Aviso: String): Boolean;
              //ExecutaSQL
    function  ExecutaQuery(Query: TmySQLQuery): Boolean;
    function  ExecutaDB(DB: TmySQLDatabase; SQL: String): Boolean;

    function  HabilitaMenuItemInt(MenuItens: array of TMenuItem;
              Query: TmySQLQuery; Campo: String; MinRec, MinVal: Integer): Boolean;
{  N�o usar mais! Usar ExcluiLct_?
    function ExcluiLct(QrAux, QrUpd: TmySQLQuery; Campos: array of String;
             Valores: array of Variant; TabLctA: String): Boolean;
}
    procedure AtzEntiAltDtPlaCt(QrUpd: TmySQLQuery; CliInt: Integer;
              Data: TDateTime);
    function  ExecSQLInsUpdPanel(Acao: TSQLType; Form: TForm; Panel: TWinControl;
              Tabela: String; NewItem: Variant; QrExec: TmySQLQuery;
              PanelsToHide, PanelsToShow: array of TWinControl; CompoTipo: TControl;
              UserDataAlterweb: Boolean; ShowSQLText: Boolean = False;
              TabelaLog: String = ''; CamposLog: String = ''): Boolean;
    function  CancelSQLInsUpdPanel(Acao: TSQLType; Form: TForm; Panel: TPanel;
              Tabela: String; NewItem: Variant; QrExec: TmySQLQuery;
              PanelsToHide, PanelsToShow: array of TWinControl; ImgTipo:
              TdmkImage; Campo: String): Boolean;
    //Mudou: negativos e padr�o para Upd (Novo) 2010-06-15
    // 2011-07-02 -> Sinal de positivo para TSinal e dmkEdit
    function  BuscaNovoCodigo_Int(QrAux: TmySQLQuery; Tabela, Campo: String;
              CamposExtras: array of String; ValExtras: array of Variant;
              SQLType: TSQLType; Padrao: Integer; Sinal: TSinal;
              dmkEdit: TdmkEdit = nil): Integer;
(*
    function  AbreBDEQuery(Query: TQuery; DataBaseName: String = '';
              Aviso: String = ''): Boolean;
*)
    function  AbreBDEQuery(Query: TDataSet; DataBaseName: String = '';
              Aviso: String = ''): Boolean;
    //
    procedure StartTransaction(Qry: TmySQLQuery);
    procedure Commit(Qry: TmySQLQuery);
    procedure RollBack(Qry: TmySQLQuery);
    function  ObtemCodigoDeCodUsu(EdCodUsu: TdmkEditCB; var Codigo: Integer;
              Aviso: String; Campo: String = 'Codigo'; CodUsu: String =
              'CodUsu'): Boolean;
    function  ObtemValorDoCampoXDeIndex_Int(Index: Integer;
              CampoX, CampoIndex: String; Query: TmySQLQuery;
              dmkEditCB: TdmkEditCB; dmkDBlookupComboBox: TdmkDBlookupComboBox;
              var Valor: Integer): Boolean;
    function  SetaCodUsuDeCodigo(EdCodUsu: TdmkEditCB; CBCodUsu:
              TdmkDBLookupComboBox; Qry: TmySQLQuery; Codigo: Integer;
              Campo: String = 'Codigo'; CodUsu: String =
              'CodUsu'): Boolean;
    function  SetaCodigoPesquisado(EdCodigo: TdmkEditCB; CBCodigo:
              TdmkDBLookupComboBox; Qry: TmySQLQuery; Codigo: Integer;
              Campo: String = 'Codigo'): Boolean;
    function  SetaCodTxtPesquisado(EdCodTxt: TdmkEditCB; CBCodTxt:
              TdmkDBLookupComboBox; Qry: TmySQLQuery; CodTxt: String;
              Campo: String = 'CodTxt'): Boolean;
    function  Busca_IDCtrl_NFe(Acao: TSQLType; Atual: Integer): Integer;
    function  Busca_IDCtrl_CTe(Acao: TSQLType; Atual: Integer): Integer;
    function  Busca_IDCtrl_MDFe(Acao: TSQLType; Atual: Integer): Integer;
    function  ObtemProximoCHTalao(Carteira: Integer; EdSerieCH, EdDoc: TdmkEdit): Boolean;
    function  ObtemQtdeCHTaloes(Carteira: Integer): Integer;
    function  MudaOrdemRegistroAtual(Tabela, FldIndice, FldOrdem: String;
              QueryAReordenar, QrUpd: TmySQLQuery): Boolean;
    function  AcertaOrdemTodosRegistros(Tabela, FldIndice, FldOrdem: String;
              QueryAReordenar, QrUpd: TmySQLQuery): Boolean;
    function  DefineDataBase(var MeuDB: String; const ExeName: String): Boolean;
    function  RegistrosNaTabela(Tabela, ComplSQL: String; Database: TmySQLDatabase): Integer;
    function  MontaOrdemSQL(Pertence: array of Boolean; Campo: array of String):
              String;
              // OpenQuery AbreQuery QueryAbre QueryOpen
    function  NaoPermiteExcluirDeTable(): Boolean;
              // AbreMySQLQuery2 : mudei 2011-11-15
    function  AbreMySQLQueryA(Query: TMySQLQuery; Database: TmySQLDatabase;
              SQLPre: array of String;
              Condicoes: array of Boolean; SQLCompl: array of String;
              SQLPos: array of String): Boolean;
              // ExecutaQuery ExecQuery QueryExecuta
    function  InsereRegistrosPreDefinidos(Query: TMySQLQuery; Tabela: String;
              SQL: array of String): Boolean;
    function  TextMySQLQuery1(Query: TMySQLQuery; SQL: array of String): Boolean;
    function  ExecutaMySQLQuery1(Query: TMySQLQuery; SQL: array of String): Boolean;
    procedure PesquisaESeleciona(Tabela, CampoNome, CampoCodigo: String);
    function  AbreSQL_ABD(Qry: TmySQLQuery; NomeTabACriar, TabLctA,
              TabLctB, TabLctD, TabID_A, TabID_B, TabID_D: String; DataI, DataF,
              DtEncer, DtMorto: TDateTime; SQL_ABD, SQLExtra: array of String;
              ExcluiTab: TTipoExcluiTab; Aviso: String): Boolean;
    function  MoveRegistroEntreTabelas(QrUpd: TmySQLQuery; TabOrig, TabDest:
              String; IdxFldDel, Operador: array of String; IdxValDel:
              array of Variant; Acao: TAcaoMoverRegistro): Boolean;
    function  LeChavedeAcesso(): String;
    function  ObtemIndicePrimario_Int(const NovoCodigo: TNovoCodigo;
              Qry: TmySQLQuery; const Tabela, Campo: String; var Idx: Integer):
              Boolean;
    function  DuplicaRegistro_IdxInt1(QrExec: TmySQLQuery; Tabela: String;
              II1Cod: Integer; II1Fld: String; NovoCodigo: TNovoCodigo;
              var Idx: Integer): Boolean;
    procedure UpdMulReg01(Help, Titulo, Prompt: String; DataSource: TmySQLQuery;
              ListSource: TDataSource; ListField: String; Default: Variant;
              SQLSorc: String; DataBase: TmySQLDatabase; SQLDest, Tabela,
              FldUpdNom, FldIdxNom: String; UserDataAlterweb: Boolean);

    // M E M O
    procedure MostraSQL(Query: TmySQLQuery; Memo:TMemo; Titulo: String);
    //
    //
    function  ObtemNomeMunici(CodMunici: Integer; Query: TmySQLQuery): String;
    function  SelecionaDB(DBs: array of String): String;
    // L O G   D E   T A B E L A S
    function  SaveLogTable(DataBase: TmySQLDatabase; TabelaOri,
              TabelaDes: String; LstKeyVal: TStringList;
              CamposLog: String): Boolean;
  end;

var
  UMyMod: TUModule;

implementation


uses
UnMyObjects, Module, SenhaEspecial, ConfJanela, Principal, SelRadioGroup,
{$IFDEF sUPD} UpdMulReg, {$ENDIF}
UnMyVCLRef, PesqESel;

//var
  //TabInLst: TTabelas;

function TUModule.SQLInsUpd_Old(QrUpd: TmySQLQuery; Tipo: String; Tabela: String;
         Auto_increment: Boolean;
         SQLCampos, SQLIndex: array of String;
         ValCampos, ValIndex: array of Variant): Boolean;
var
  SQLType: TSQLType;

begin
  if Tipo = CO_INCLUSAO then SQLTYpe := stIns else
  if Tipo = CO_ALTERACAO then SQLTYpe := stUpd else
  SQLTYpe := stLok;
  Result := SQLInsUpd(QrUpd, SQLType, lowercase(Tabela), Auto_increment,
    SQLCampos, SQLIndex, ValCampos, ValIndex, True);
end;

function TUModule.CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb, IGNORE: Boolean; ComplUpd: String): Boolean;
begin
  Result := USQLDB.CarregaSQLInsUpd(QrUpd, Tipo, Tabela, Auto_increment,
              SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb,
              IGNORE, ComplUpd);
end;

function TUModule.CarregaSQLReplace(QrUpd: TmySQLQuery; Tabela: String;
  //Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  Tab := LowerCase(Tabela);
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('REPLACE INTO ' + Lowercase(Tab) + ' SET ');
  if UserDataAlterweb then
    QrUpd.SQL.Add('AlterWeb=1, ');
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //
  if UserDataAlterweb then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO));
  //
  {
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_('SQL com Auto Increment e Indice ao mesmo tempo!',
      );
    // N�o faz nada
  end else begin
  }
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      //if Tipo = stIns then
      Liga := ', '
      {
      else begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end};
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  //end;
  QrUpd.SQL.Add(';');
  Result := True;
end;

function TUModule.LctUpd(QrUpd: TmySQLQuery; Tabela: String;
 SQLCampos: array of String; ValCampos: array of Variant;
 Data: TDateTime; Tipo, Carteira: Integer; Controle: Int64;
 Sub: Integer): Boolean;
begin
  Result := CarregaSQLInsUpd(QrUpd, stUpd, lowercase(Tabela), False,
  SQLCampos, ['Data', 'Tipo', 'Carteira', 'Controle', 'Sub'],
  ValCampos, [Geral.FDT(Data, 1), Tipo, Carteira, Controle, Sub], True, False, '');
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      raise;
    end;
  end else;
end;

function TUModule.LeChavedeAcesso(): String;
begin
  Result := Geral.ReadAppKeyLM('Dmk001', TMeuDB, ktInteger, 0);
end;

function TUModule.SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; ComplUpd: String = '';
  InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
begin
  Result := USQLDB.SQLInsUpd(QrUpd, Tipo, Tabela, Auto_increment, SQLCampos,
              SQLIndex, ValCampos, ValIndex, UserDataAlterweb, ComplUpd,
              InfoSQLOnError, InfoErro);
end;

function TUModule.SQLInsUpdAlterIdx(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Tabela: String; Auto_increment: Boolean; SQLCampos, SQLIndex,
  SQLAlterIdx: array of String; ValCampos, ValIndex,
  ValAlterIdx: array of Variant; UserDataAlterweb: Boolean; ComplUpd: String;
  InfoSQLOnError, InfoErro: Boolean): Boolean;
begin
  Result := USQLDB.SQLInsUpdAlterIdx(QrUpd, Tipo, Tabela, Auto_increment,
  SQLCampos, SQLIndex, SQLAlterIdx, ValCampos, ValIndex, ValAlterIdx,
  UserDataAlterweb, ComplUpd, InfoSQLOnError, InfoErro);
end;

function TUModule.ExcluiRegistros(Pergunta: String; QrUpd: TmySQLQuery; Tabela:
             String; SQLCampos, Compare: array of String;
             ValCampos: array of Variant; ComplSQL: String): Boolean;
var
  Continua: Boolean;
  TextoSQL: String;
begin
  Result := False;
  if Trim(Pergunta) = '' then Continua := True else
    Continua := Geral.MB_Pergunta(Pergunta) = ID_YES;
  if Continua then
  begin
    if GeraCondicaoDeSQL(TextoSQL, Tabela, SQLCampos, Compare, ValCampos,
    'WHERE', ComplSQL) then
    begin
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add(DELETE_FROM + ' ' + LowerCase(Tabela) + TextoSQL);
      Result := ExecutaQuery(QrUpd);
    end;
  end;
end;

// CUIDADO Replace exclui a chave antes de inserir
// para n�o excluir use ON DUPLICATE KEY (ON_DUPLICATE_KEY ONDUPLICATEKEY)
function TUModule.SQLReplace(QrUpd: TmySQLQuery; Tabela: String;
  //Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean): Boolean;
begin
  Result := CarregaSQLReplace(QrUpd, lowercase(Tabela),
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb);
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      raise;
    end;
  end else;
end;

// ON DUPLICATE KEY
function TUModule.SQLIns_ON_DUPLICATE_KEY(
  QrUpd: TmySQLQuery; Tabela: String; Auto_increment: Boolean;
  SQLCampos, SQLIndex, SQLUpdate: array of String;
  ValCampos, ValIndex, ValUpdate: array of Variant;
  UserDataAlterweb: Boolean;
  CampoIncrementa: String = '';
  Incremento: Integer = 1): Boolean;
begin
  Result := CarregaSQLIns_ON_DUPLICATE_KEY(QrUpd, lowercase(Tabela), Auto_increment,
  SQLCampos, SQLIndex, SQLUpdate, ValCampos, ValIndex, ValUpdate,
  UserDataAlterweb, CampoIncrementa, Incremento);
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      raise;
    end;
  end else;
end;

function TUModule.CarregaSQLIns_ON_DUPLICATE_KEY(QrUpd: TmySQLQuery; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex, SQLUpdate: array of String;
  ValCampos, ValIndex, ValUpdate: array of Variant;
  UserDataAlterweb: Boolean;
  CampoIncrementa: String = '';
  Incremento: Integer = 1): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd_ON_DUPLICATE_KEY"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j) + ' valores para estes �ndices em "CarregaSQLInsUpd_ON_DUPLICATE_KEY"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  i := High(SQLUpdate);
  j := High(ValUpdate);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos de update e ' +
    IntToStr(j) + ' valores para estes campos de update em "CarregaSQLInsUpd_ON_DUPLICATE_KEY"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  Tab := LowerCase(Tabela);
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ');
  if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //

  if UserDataAlterweb then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO));
    //QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  //
  if Auto_increment then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      Liga := ', ';
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  end;

  // ON DUPLICATE KEY

  Liga := '';
  QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE ');
  j := High(SQLUpdate);
  for i := Low(SQLUpdate) to j do
  begin
    Valor := Geral.VariavelToString(ValUpdate[i]);
    if (i < j) or (UserDataAlterweb and (CampoIncrementa = '')) then
      QrUpd.SQL.Add(SQLUpdate[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLUpdate[i] + '=' + Valor);
    Liga := ', ';
  end;
  if Trim(CampoIncrementa) <> '' then
  begin
    QrUpd.SQL.Add(Liga + CampoIncrementa + '=' + CampoIncrementa + ' + ' +
    FormatFloat('0', Incremento));
    if UserDataAlterweb then
      QrUpd.SQL.Add(', ');
  end;
  if UserDataAlterweb then
    QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  //
  Result := True;
end;

function TUModule.SQLInsUpd_IGNORE(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; ComplUpd: String = ''): Boolean;
{
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
}
begin
  Result := CarregaSQLInsUpd(QrUpd, Tipo, lowercase(Tabela), Auto_increment,
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, True, ComplUpd);
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
      raise;
    end;
  end;
end;

function TUModule.SQLInsUpd_UpdExtra(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex, SQLSoUpd: array of String;
  ValCampos, ValIndex, ValSoUpd: array of Variant;
  UserDataAlterWeb: Boolean): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  //Result := False;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Aviso('AVISO: O status da a��o est� definida como ' +
    '"' + DmkEnums.NomeTipoSQL(Tipo) + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = stIns then
    QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ')
  else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
      QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
    else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      if Tipo = stIns then Liga := ', ' else
      begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end;
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
    //
    if Tipo = stUpd then
    begin
      for i := Low(SQLSoUpd) to High(SQLSoUpd) do
      begin
        Valor := Geral.VariavelToString(ValSoUpd[i]);
        QrUpd.SQL.Add('AND ' + SQLSoUpd[i] + '=' + Valor);
      end;
    end;
    //
  end;
  //
  try
    QrUpd.ExecSQL;
    Result := True;
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
    raise;
  end;
end;

function TUModule.FormInsUpd_Show(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Query: TmySQLQuery; Acao: TSQLType): Boolean;
begin
  Result := False;
  if FormInsUpd_Cria(InstanceClass, Reference, ModoAcesso, Query, Acao) then
  begin
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    Result := True;
  end;
end;

function TUModule.GeraCondicaoDeSQL(var TextoSQL: String; const Tabela: String;
  const SQLCampos, Compare: array of String; const ValCampos:
  array of Variant; const FirstCondition, ComplSQL: String):
  Boolean;
var
  i, j: Integer;
  Valor: String;
begin
  Result := False;
  TextoSQL := sLineBreak + FirstCondition + ' ';
  //
  i := High(SQLCampos);
  j := High(Compare);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' comparadores "GeraCondicaoDeSQL"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "GeraCondicaoDeSQL"!' + sLineBreak +
    'Tabela: "' + Tabela + '"');
    Exit;
  end;
  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i = 0) then
      TextoSQL := TextoSQL + SQLCampos[i] + Compare[i] + Valor + sLineBreak
    else
      TextoSQL := TextoSQL + 'AND ' + SQLCampos[i] + Compare[i] + Valor + sLineBreak;
  end;
  //
  if Trim(ComplSQL) <> '' then
  begin
    TextoSQL := TextoSQL + ComplSQL + sLineBreak;
  end;
  TextoSQL := TextoSQL + ';';
  Result := True;
end;

function TUModule.FormInsUpd_Mul_Show(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Queries: array of TmySQLQuery; Acao: TSQLType): Boolean;
begin
  Result := False;
  if FormInsUpd_Mul_Cria(InstanceClass, Reference, ModoAcesso, Queries, Acao) then
  begin
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    Result := True;
  end;
end;

function TUModule.FormInsUpd_Cria(InstanceClass: TComponentClass; var Reference;
  ModoAcesso: TAcessFmModo; Query: TmySQLQuery; Acao: TSQLType;
  Tabela: String = ''): Boolean;

  function ObtemValorDefaultCampo(Qry: TmySQLQuery; Campo: String): Variant;
  begin
    Result := Null;
    //
    if Qry.Locate('Field', Campo, []) then
      Result := Qry.FieldByName('Default').AsVariant;
  end;

  procedure ReopenQueryCampos(Qry: TmySQLQuery);
  begin
    if (Tabela <> '') and (Query <> nil) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Query.DataBase, [
        'SHOW COLUMNS ',
        'FROM ' + Tabela,
        '']);
    end;
  end;

  function ExecutaCamposDefault(): Boolean;
  begin
    Result := (Query <> nil) and (Tabela <> '') and (Acao = stIns);
  end;

  procedure Mensagem(Campo: String; Objeto: TObject; Erro: String = '');
  var
    Msg: String;
  begin
    Msg := 'N�o foi poss�vel definir o valor do campo "' + Campo +
      '" no componente "' + TComponent(Objeto).Name + '"';
    if Erro <> '' then
      Msg := Erro + sLinebreak + Msg;
    Geral.MB_Erro(Msg);
  end;

var
  Objeto: TObject;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PropInfo: PPropInfo;
  i: Integer;
  Campo, Val: String;
  Valor: Variant;
  MySou: TDataSource;
  MySet: TDataSet;
  //MyQry: TmySQLQuery;
  MyFld: String;
  //PropVal: Variant;
  Achou: Boolean;
  Qry: TmySQLQuery;
begin
  Result := DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso);
  //
  if ExecutaCamposDefault() then
  begin
    Qry := TmySQLQuery.Create(Query.Database);
    //
    ReopenQueryCampos(Qry);
  end;
  try
    if not Result then Exit;
    with TForm(Reference) do
    begin
      Achou := False;
      Campo := 'LaTipo';
      if (FindComponent(Campo) as TdmkLabel) <> nil then
      begin
        TdmkLabel(FindComponent(Campo) as TLabel).SQLType := Acao;
        if TdmkLabel(FindComponent(Campo) as TLabel).SQLType <> Acao then
        Geral.MB_Erro('N�o foi poss�vel definir o tipo de a��o ' +
        'na janela ' + TForm(Reference).Name + '!')
        else Achou := True;
      end;
      Campo := 'ImgTipo';
      if (FindComponent(Campo) as TdmkImage) <> nil then
      begin
        TdmkImage(FindComponent(Campo) as TImage).SQLType := Acao;
        if TdmkImage(FindComponent(Campo) as TImage).SQLType <> Acao then
        Geral.MB_Erro('N�o foi poss�vel definir o tipo de a��o ' +
        'na janela ' + TForm(Reference).Name + '!')
        else Achou := True;
      end;
      if not Achou then Geral.MB_Erro('O formul�rio ' +
      TForm(Reference).Name + ' n�o possui "label" para definir o tipo de a��o!');
      //
      with TForm(Reference) do
      begin
        for i := 0 to ComponentCount -1 do
        begin
          Objeto := Components[i];
          //
          if Acao = stUpd then
          begin
            PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
            PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
            PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
            PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
            if PI_OldValor <> nil then
            begin
              if (PI_DataField <> nil) and (PI_DataSource <> nil) then
              begin
                //
                //PropVal := GetPropValue(Objeto, 'DataSource');
                //MySet   := TDataSource(PropVal);
                //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
                MySou := TDataSource(TDBEdit(Objeto).DataSource);
                // 2012-02-05 FmEntiContat, FmEntiMail, ...
                if MySou <> nil then
                // Fim 2012-02-05
                begin
                  MySet := TDataSource(MySou).DataSet;
                  //MyQry := TMySQLQuery(TDataSource(MySet));
                  MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
                  SetPropValue(Objeto, 'OldValor',
                    TDataSet(MySet).FieldByName(MyFld).AsVariant);
                end;
              end else if PI_QryCampo <> nil then
              begin
                Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
                if Campo <> '' then
                begin
                  PropInfo := GetPropInfo(Objeto, 'OldValor');
                  if PropInfo <> nil then
                  try
{
                    if (Objeto is TdmkRadioGroup) and
                    ((VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varNull) or
                    ((VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varString) and
                    (TmySQLQuery(Query).FieldByName(Campo).AsVariant = ''))) then
                      SetPropValue(Objeto, 'OldValor', -1)
}
                    //if (Objeto is TdmkRadioGroup) and  ValorNuloEVazio1(Query, Campo) then
                    if ValorNuloEVazio2(TdmkRadioGroup, Objeto, Query, Campo) then
                        SetPropValue(Objeto, 'OldValor', -1)
                    else
{
                    if (Objeto is TdmkEditDateTimePicker) and
                    ((VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varNull) or
                    ((VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varString) and
                    (TmySQLQuery(Query).FieldByName(Campo).AsVariant = ''))) then
}
                    if ValorNuloEVazio2(TdmkEditDateTimePicker, Objeto, Query, Campo) then
                      SetPropValue(Objeto, 'OldValor', 0)
                    else
                      SetPropValue(Objeto, 'OldValor',
                        TmySQLQuery(Query).FieldByName(Campo).AsVariant);
                  except
                    Geral.MB_Erro('N�o foi poss�vel definir o "OldValor" = "'
                    + Geral.VariantTostring(
                    TmySQLQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                    Campo + '" no objeto "' + TComponent(Objeto).Name + '"');
                  end;
                end;
              end;
            end;
          end;
          //fim OldValor
          //
          PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
          if PropInfo <> nil then
          begin
            Campo := GetStrProp(TComponent(Objeto), PropInfo);
            if Campo <> '' then
            begin
              if Acao = stIns then
              begin
                if ExecutaCamposDefault() then
                  Valor := ObtemValorDefaultCampo(Qry, Campo)
                else
                  Valor := Null
              end else
              try
                Valor := TmySQLQuery(Query).FieldByName(Campo).AsVariant;
              except
                on E: Exception do
                  Geral.MB_Erro(E.Message + sLineBreak +
                  TComponent(Objeto).Name)
              end;
              try
                if (Objeto is TdmkEdit) then
                begin
                  if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                  begin
                    Valor := Geral.MesEAnoDoMez(Valor);
                    TdmkEdit(Objeto).Texto := Valor;
                  end else
                  if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                  begin
                    Valor := Geral.AnoEMesDoMez(Valor);
                    TdmkEdit(Objeto).Texto := Valor;
                  end else
                  TdmkEdit(Objeto).ValueVariant := Valor;
                end
                //
                else if (Objeto is TdmkDBLookupCombobox) then
                  TdmkDBLookupCombobox(Objeto).KeyValue := Valor
                //
                else if (Objeto is TdmkEditCB) then
                  TdmkEditCB(Objeto).ValueVariant := Valor
                //
                else if (Objeto is TdmkEditDateTimePicker) then
                begin
                  if Valor <> Null then
                  begin
                    Val := VarToStr(Valor);
                    //
                    if (Val = '00:00:00') or (Val = '00:00') or
                      (Val = '0000-00-00 00:00:00') or (Val = '0000-00-00') then
                      TdmkEditDateTimePicker(Objeto).Date := 0
                    else
                      TdmkEditDateTimePicker(Objeto).Date := Valor;
                  end;
                end
                //
                else if (Objeto is TdmkMemo) then
                begin
                  if Valor = Null then Valor := '';
                  TdmkMemo(Objeto).Text := Valor;
                end
                //
                else if (Objeto is TdmkRadioGroup) then
                begin
                  {
                  if (Valor = Null) or (Valor = '') then Valor := -1;
                  TdmkRadioGroup(Objeto).ItemIndex := Valor;
                  }
                  if (Valor = Null) then Valor := -1;
                  if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
                  or (VarType(Valor) = varUString)
{$ENDIF}
                  then
                  begin
                    if (Valor = '') then
                      Valor := -1
                  end;
                  TdmkRadioGroup(Objeto).ItemIndex := Valor;
                end
                //
                else if (Objeto is TdmkCheckBox) then
                begin
                  if Valor = Null then Valor := False;
                  //
                  if Valor = TdmkCheckBox(Objeto).ValCheck then
                    TdmkCheckBox(Objeto).Checked := True
                  else
                    TdmkCheckBox(Objeto).Checked := False;
                end
                //
                else if (Objeto is TdmkCheckGroup) then
                begin
                  if Valor = Null then Valor := 0;
                  TdmkCheckGroup(Objeto).Value := Valor;
                end
                //
                else if (Objeto is TdmkPopOutFntCBox) then
                  TdmkPopOutFntCBox(Objeto).FonteNome := Valor
                //
                else if (Objeto is TdmkRichEdit) then
                  MyObjects.DefineTextoRichEdit(TdmkRichEdit(Objeto), Valor)
                //
                else if (Objeto is TdmkValUsu) then
                  TdmkValUsu(Objeto).ValueVariant := Valor
                //

                //

                else if (Objeto is TdmkDBEdit) then begin end //nada
                //
                else Mensagem(Campo, Objeto);
              except
                on E: Exception do
                begin
                  Mensagem(Campo, Objeto, E.Message);
                end;
              end;
            end;
          end;
        end;
      end;
      //
      {
      TForm(Reference).ShowModal;
      TForm(Reference).Destroy;
      }
    end;
  finally
    if ExecutaCamposDefault() then
      Qry.Free;
  end;
end;

function TUModule.FormInsUpd_Mul_Cria(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Queries: array of TmySQLQuery; Acao: TSQLType): Boolean;
  procedure Mensagem(Campo: String; Objeto: TObject);
  begin
    Geral.MB_Erro('N�o foi poss�vel definir o valor ' +
    'do campo "' + Campo + '" no componente "' +
    TComponent(Objeto).Name + '"');
  end;
var
  Objeto: TObject;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PI_QryNome: PPropInfo;
  i, j: Integer;
  NomeQry: String;
  Campo: String;
  Valor: Variant;
  MySou: TDataSource;
  MySet: TDataSet;
  Query: TmySQLQuery;
  MyFld: String;
  OldValor: String;
  //PropVal: Variant;
begin
  Result := DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso);
  if not Result then Exit;
  with TForm(Reference) do
  begin
    Campo := 'ImgTipo';
    if (FindComponent(Campo) as TdmkImage) <> nil then
    begin
      TdmkImage(FindComponent(Campo) as TdmkImage).SQLType := Acao;
      if TdmkImage(FindComponent(Campo) as TdmkImage).SQLType <> Acao then
      Geral.MB_Erro('N�o foi poss�vel definir o tipo de a��o ' +
      'na janela ' + TForm(Reference).Name + '!');
    end else Geral.MB_Erro('O formul�rio ' +
    TForm(Reference).Name + ' n�o possui "label" para definir o tipo de a��o!');
    //
    with TForm(Reference) do
    begin
      Query := nil;
      for i := 0 to ComponentCount -1 do
      begin
        Objeto := Components[i];
        //
        if Acao = stUpd then
        begin
          PI_QryNome := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryName');
          if PI_QryNome <> nil then
          begin
            NomeQry := GetStrProp(TComponent(Objeto), PI_QryNome);
            for j := Low(Queries) to High(Queries) do
            begin
              if TmySQLQuery(Queries[j]).Name = NomeQry then
              begin
                Query := Queries[j];
                Break;
              end;
            end;
          end;
          // else Geral.MB_Erro(
            //'N�o foi poss�vel definir a query pelo "QryName"', 'Aviso',
            //MB_OK+MB_ICONERROR);
          //
          PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
          PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
          PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
          PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
          if PI_OldValor <> nil then
          begin
            if (PI_DataField <> nil) and (PI_DataSource <> nil) then
            begin
              //
              //PropVal := GetPropValue(Objeto, 'DataSource');
              //MySet   := TDataSource(PropVal);
              //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
              MySou := TDataSource(TDBEdit(Objeto).DataSource);
              MySet := TDataSource(MySou).DataSet;
              //MyQry := TMySQLQuery(TDataSource(MySet));
              MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
              SetPropValue(Objeto, 'OldValor',
                TDataSet(MySet).FieldByName(MyFld).AsVariant);

            end else if PI_QryCampo <> nil then
            begin
              Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
              if Campo <> '' then
              begin
                PI_OldValor := GetPropInfo(Objeto, 'OldValor');
                if PI_OldValor <> nil then
                try
                  if TmySQLQuery(Query).State <> dsInactive then // 2021-03-13
                  begin
                    if (TmySQLQuery(Query).FieldByName(Campo).AsVariant <> Null) then
                    begin
                      OldValor := Geral.VariantToString(
                        TmySQLQuery(Query).FieldByName(Campo).AsVariant);
                      SetPropValue(Objeto, 'OldValor', OldValor);
                    end;
                  end;
                except
                  Geral.MB_Erro('N�o foi poss�vel definir o "OldValor" = "'
                  + Geral.VariantTostring(
                  TmySQLQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                  Query.Name + '.' + Campo + '" no objeto "' +
                  TComponent(Objeto).Name + '"');
                end;
              end;
            end;
          end;
        end;
        //fim OldValor
        //
        PI_QryCampo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
        if PI_QryCampo <> nil then
        begin
          Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
          if Campo <> '' then
          begin
            if Acao = stIns then Valor := Null else Valor :=
              TmySQLQuery(Query).FieldByName(Campo).AsVariant;
            try
              if (Objeto is TdmkEdit) then
              begin
                try
                if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                  Valor := Geral.MesEAnoDoMez(Valor)
                else
                if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                  Valor := Geral.AnoEMesDoMez(Valor);
                //
                TdmkEdit(Objeto).ValueVariant := Valor;
                except
                  Geral.MB_Erro('Erro ao setar valor do componente:' +
                  sLineBreak + TdmkEdit(Objeto).Name);
                end;
              end
              //
              else if (Objeto is TdmkDBLookupCombobox) then
                TdmkDBLookupCombobox(Objeto).KeyValue := Valor
              //
              else if (Objeto is TdmkEditCB) then
                TdmkEditCB(Objeto).ValueVariant := Valor
              //
              else if (Objeto is TdmkEditDateTimePicker) then
              begin
                if Valor <> Null then
                  //TdmkEditDateTimePicker(Objeto).Date := Date
                //else
                  TdmkEditDateTimePicker(Objeto).Date := Valor;
              end
              //
              else if (Objeto is TdmkMemo) then
              begin
                if Valor = Null then Valor := '';
                TdmkMemo(Objeto).Text := Valor;
              end
              //
              else if (Objeto is TdmkRadioGroup) then
              begin
                {
                if (Valor = Null) or (Valor = '') then Valor := -1;
                TdmkRadioGroup(Objeto).ItemIndex := Valor;
                }
                if (Valor = Null) then Valor := -1;
                if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
                or (VarType(Valor) = varUString)
{$ENDIF}
                then
                begin
                  if (Valor = '') then
                    Valor := -1
                end;
                if String(VAlor) = EmptyStr then
                  Valor := -1;
                TdmkRadioGroup(Objeto).ItemIndex := Valor;
              end
              //
              else if (Objeto is TdmkCheckBox) then
              begin
                if Valor = Null then Valor := False;
                //
                if TdmkCheckBox(Objeto).ValCheck = Valor then
                  TdmkCheckBox(Objeto).Checked := True
                else
                  TdmkCheckBox(Objeto).Checked := False;
              end
              //
              else if (Objeto is TdmkCheckGroup) then
              begin
                if Valor = Null then Valor := 0;
                TdmkCheckGroup(Objeto).Value := Valor;
              end
              //
              else if (Objeto is TdmkPopOutFntCBox) then
                TdmkPopOutFntCBox(Objeto).FonteNome := Valor
              //
              else if (Objeto is TdmkRichEdit) then
                MyObjects.DefineTextoRichEdit(TdmkRichEdit(Objeto), Valor)
              //
              else if (Objeto is TdmkValUsu) then
                TdmkValUsu(Objeto).ValueVariant := Valor
              //

              //

              else if (Objeto is TdmkDBEdit) then begin end //nada
              //
              else Mensagem(Campo, Objeto);
            except
              Mensagem(Campo, Objeto);
            end;
          end;
        end;
      end;
    end;
    //
    {
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    }
  end;
end;

function TUModule.ExecSQLInsUpdFm(Form: TForm; Acao: TSQLType; Tabela:
String; NewItem: Variant; QrExec: TmySQLQuery): Boolean;
var
  SQL: String;
  Cursor: TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Result := False;
    if Acao in ([stIns, stUpd]) then
    begin
      SQL := MyVCLref.CriaSQLdmkForm(Form, Acao, lowercase(Tabela), NewItem);
      //
      QrExec.Close;
      QrExec.SQL.Text := SQL;
      try
        //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
        QrExec.ExecSQL;
        Result := True;
      finally
        if not Result then
        begin
          if Acao = stIns then
            PoeEmLivreY(QrExec.Database, 'livres', lowercase(Tabela), NewItem);
          UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
        end;
      end;
    end else Geral.MB_Erro('A��o n�o implementada: "' + DmkEnums.NomeTipoSQL(Acao) +
    '"' + sLineBreak + 'function TUModule.ExecSQLInsUpdFm()');
  finally
    Screen.Cursor := Cursor;
  end;
end;

function TUModule.CancelSQLInsUpdPanel(Acao: TSQLType; Form: TForm;
Panel: TPanel; Tabela: String; NewItem: Variant; QrExec: TmySQLQuery;
PanelsToHide, PanelsToShow: array of TWinControl; ImgTipo: TdmkImage;
Campo: String): Boolean;
var
  i, j, k: Integer;
begin
  //
  i := Low(PanelsToShow);
  j := High(PanelsToShow);
  for k := i to j do PanelsToShow[i].Visible := True;
  //
  i := Low(PanelsToHide);
  j := High(PanelsToHide);
  for k := i to j do PanelsToHide[i].Visible := False;
  //
  //if Acao = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', lowercase(Tabela), NewItem);
  UMyMod.UpdUnlockY(NewItem, QrExec.Database, lowercase(Tabela), Campo);
  Result := True;
end;

function TUModule.SaveLogTable(DataBase: TmySQLDatabase; TabelaOri,
  TabelaDes: String; LstKeyVal: TStringList; CamposLog: String): Boolean;
const
  MotivoTmp = 998;
  Motivo = 999;

  function ObtemSQLWhere: String;
  var
    I: Integer;
    Prefix: String;
  begin
    Result := '';
    //
    for I := 0 to LstKeyVal.Count - 1 do
    begin
      if I = 0 then
        Prefix := 'WHERE '
      else
        Prefix := 'AND ';
      //
      Result := Result + Prefix + LstKeyVal[I] + ' ';
    end;
  end;

  procedure AtzLogTable(QueryOri, QueryDes, QueryUpd: TmySQLQuery;
    LstCampos: TStringList);
  var
    I, UserDel, MotvDel: Integer;
    Campo, DataDel: String;
    Deleta: Boolean;
  begin
    Deleta := True;
    //
    for I := 0 to LstCampos.Count - 1 do
    begin
      Campo := Trim(LstCampos[I]);
      //
      if QueryOri.FieldByName(Campo).AsVariant <>
        QueryDes.FieldByName(Campo).AsVariant then
      begin
        Deleta := False;
        Break;
      end;
    end;
    DataDel := Geral.FDT(QueryDes.FieldByName('DataDel').AsDateTime, 109);
    UserDel := QueryDes.FieldByName('UserDel').AsInteger;
    MotvDel := QueryDes.FieldByName('MotvDel').AsInteger;
    //
    if Deleta = True then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Dmod.MyDB, [
        DELETE_FROM + ' ' + TabelaDes,
        ObtemSQLWhere,
        'AND DataDel="' + DataDel + '"',
        'AND UserDel=' + Geral.FF0(UserDel),
        'AND MotvDel=' + Geral.FF0(MotvDel),
        '']);
    end else
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Dmod.MyDB, [
        'UPDATE ' + TabelaDes + ' SET ',
        'MotvDel=' + Geral.FF0(Motivo),
        ObtemSQLWhere,
        'AND DataDel="' + DataDel + '"',
        'AND UserDel=' + Geral.FF0(UserDel),
        'AND MotvDel=' + Geral.FF0(MotvDel),
        '']);
    end;
  end;

var
  TbCol: String;
  ListaCampos: TStringList;
  QryOri, QryDes, QryUpd: TmySQLQuery;
  QtdFlds: Integer;
begin
  Result      := False;
  QryOri      := TmySQLQuery.Create(DMod);
  QryDes      := TmySQLQuery.Create(DMod);
  QryUpd      := TmySQLQuery.Create(DMod);
  ListaCampos := TStringList.Create;
  //
  if LstKeyVal.Count = 0 then
  begin
    Geral.MB_Erro('Nenhuma chave de tabela foi informada!' + sLineBreak +
      'Fun��o: TUModule.SaveTmpLogTable');
    Exit;
  end;
  //
  try
    TbCol := UMyMod.ObtemCamposDeTabelaIdentica(DataBase, TabelaDes, '', QtdFlds);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QryDes, Dmod.MyDB, [
      'SELECT ' + TbCol,
      'FROM ' + TabelaDes,
      ObtemSQLWhere,
      'AND MotvDel=' + Geral.FF0(MotivoTmp),
      '']);
    if QryDes.RecordCount > 0 then
    begin
      TbCol := Geral.Substitui(TbCol, ', DataDel', '');
      TbCol := Geral.Substitui(TbCol, ', UserDel', '');
      TbCol := Geral.Substitui(TbCol, ', MotvDel', '');
      //
      if CamposLog <> '' then
        ListaCampos := Geral.Explode(CamposLog, ',')
      else
        ListaCampos := Geral.Explode(TbCol, ', ');
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QryOri, Dmod.MyDB, [
        'SELECT ' + TbCol,
        'FROM ' + TabelaOri,
        ObtemSQLWhere,
        '']);
      if QryOri.RecordCount > 0 then
      begin
        QryDes.First;
        while not QryDes.Eof do
        begin
          AtzLogTable(QryOri, QryDes, QryUpd, ListaCampos);
          //
          QryDes.Next;
        end;
      end;
    end else
      Result := True;
  finally
    QryOri.Free;
    QryDes.Free;
    QryUpd.Free;
    ListaCampos.Free;
  end;
end;

function TUModule.SaveTmpLogTable(DataBase: TmySQLDatabase; TabelaOri,
  TabelaDes: String; LstKeyVal: TStringList): Boolean;

  function ObtemSQLWhere: String;
  var
    I: Integer;
    Prefix: String;
  begin
    Result := '';
    //
    for I := 0 to LstKeyVal.Count - 1 do
    begin
      if I = 0 then
        Prefix := 'WHERE '
      else
        Prefix := 'AND ';
      //
      Result := Result + Prefix + LstKeyVal[I];
      //
      if I <> LstKeyVal.Count - 1 then
        Result := Result + ', ';
    end;
  end;

const
  Motivo = 998;
var
  TbCol, Dta: String;
  Qry: TmySQLQuery;
  QtdFlds: Integer;
begin
  Result := False;
  Dta    := Geral.FDT(DModG.ObtemAgora(), 105);
  TbCol  := UMyMod.ObtemCamposDeTabelaIdentica(DataBase, TabelaDes, '', QtdFlds);
  //
  if LstKeyVal.Count = 0 then
  begin
    Geral.MB_Erro('Nenhuma chave de tabela foi informada!' + sLineBreak +
      'Fun��o: TUModule.SaveTmpLogTable');
    Exit;
  end;
  //
  TbCol := Geral.Substitui(TbCol, ', DataDel', ', "' + Dta + '" DataDel');
  TbCol := Geral.Substitui(TbCol, ', UserDel', ', ' + Geral.FF0(VAR_USUARIO) + ' UserDel');
  TbCol := Geral.Substitui(TbCol, ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
  //
  TbCol := 'INSERT INTO ' + TabelaDes + ' SELECT ' + sLineBreak + TbCol +
           sLineBreak + 'FROM ' + TabelaOri + sLineBreak + ObtemSQLWhere + '; ';
  //
  Qry := TmySQLQuery.Create(DMod);
  try
    Qry.SQL.Text := TbCol;
    //
    if Qry.Database <> DataBase then
      Qry.Database := DataBase;
    //
    Result := UMyMod.ExecutaQuery(Qry);
  finally
    Qry.Free;
  end;
end;

function TUModule.ExecSQLInsUpdPanel(Acao: TSQLType; Form: TForm;
  Panel: TWinControl; Tabela: String; NewItem: Variant; QrExec: TmySQLQuery;
  PanelsToHide, PanelsToShow: array of TWinControl; CompoTipo: TControl;
  UserDataAlterweb: Boolean; ShowSQLText: Boolean = False;
  TabelaLog: String = ''; CamposLog: String = ''): Boolean;
var
  SQL, SQL2: String;
  i, j, k: Integer;
  Compo: TComponent;
  Lista: TStringList;
begin
  ////////////// AVISO /////////////////////////////////////////////////////////
  //                                                                          //
  // Para salvar o log da tabela basta informar o nome da tabela destino na   //
  // vari�vel "TabelaLog".                                                    //
  // Para controlar o log de apenas alguns campos informar a vari�vel         //
  // "TabelaLog" e na vari�vel "CamposLog" informar os campos a serem         //
  // controlados separados por ",".                                           //
  //                                                                          //
  // OBS.: A vari�vel "CamposLog" foi declarada como string pois o            //
  // array of string n�o permite valores default                              //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  Result := False;
  //
  if (Acao = stUpd) and (TabelaLog <> '') then
  begin
    Lista := MyVCLref.ObtemChaveEValordmkPanel(Form, Panel);
    SaveTmpLogTable(QrExec.Database, Tabela, TabelaLog, Lista);
  end;
  //
  SQL := MyVCLref.CriaSQLdmkPanel_Lins(Form, Panel, Acao, lowercase(Tabela),
         NewItem, UserDataAlterweb);
  //
  QrExec.Close;
  QrExec.SQL.Text := SQL;
  //
  try
    ExecutaQuery(QrExec);
    Result := True;
    //
    if (Acao = stUpd) and (TabelaLog <> '') then
      SaveLogTable(QrExec.Database, Tabela, TabelaLog, Lista, CamposLog);
    //
    i := Low(PanelsToShow);
    j := High(PanelsToShow);
    for k := i to j do PanelsToShow[i].Visible := True;
    //
    i := Low(PanelsToHide);
    j := High(PanelsToHide);
    for k := i to j do PanelsToHide[i].Visible := False;
    //
    if CompoTipo <> nil then
    begin
      if CompoTipo is TdmkLabel then
        TdmkLabel(CompoTipo).SQLType := stLok
      else
      if CompoTipo is TdmkImage then
        TdmkImage(CompoTipo).SQLType := stLok
      else
        Geral.MB_Erro(
        'Tipo de componente n�o implementado na fun��o "ExecSQLInsUpdPanel"');
    end;
    for i := 0 to Form.ComponentCount - 1 do
    begin
      Compo := Form.Components[i];
      if  (Compo.Name = 'SbNovo')
      or  (Compo.Name = 'SbNumero')
      or  (Compo.Name = 'SbNome')
      or  (Compo.Name = 'SbQuery')
      then begin
        if (Compo is TBitBtn) then
          TBitBtn(Compo).Enabled := True
        else
        if (Compo is TSpeedButton) then
          TSpeedButton(Compo).Enabled := True;
      end;
    end;
    Result := True;
    if ShowSQLText then
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
  finally
    if not Result then
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
  end;
  //Geral.MB_Teste(QrExec.SQL.Text)
end;

function TUModule.BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUModule.BuscaEmLivreY_Def_Geral(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'ctrlgeral',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUModule.BuscaEmLivreY_Def_SPED_EFD(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'spedefdicmsipictrl',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUModule.BuscaEmLivreY_Def_Old(Table, Field: String; Acao: String;
  Atual: Integer): Integer;
begin
  if Acao = CO_ALTERACAO then Result := Atual else
  if Acao = CO_INCLUSAO then Result :=
    BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle', lowercase(Table), lowercase(Table), Field)
  else Result := 0;
end;

{
function TUModule.SituacaoLancamento(QrLct: TmySQLQuery): String;
begin
  UFinanceiro.NomeSitLancto(LctSit, LanctoTipo, CarteiraPrazo:
         Integer; LanctoVencto: TDateTime; Reparcelamento: Integer;
         ConverteCompensadoEmQuitado: Boolean = False): String;
  //if QrLct.FieldByName('Sit.Value = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if QrLct.FieldByName('Sit').AsInteger = -1 then
     Result := CO_IMPORTACAO
  else
    if QrLct.FieldByName('Sit').AsInteger = 1 then
     Result := CO_PAGTOPARCIAL
  else
    if QrLct.FieldByName('Sit').AsInteger = 2 then
       Result := CO_QUITADA
  else if QrLct.FieldByName('Sit').AsInteger = 3 then
  begin
    // Caixa com prazo
    if (QrLct.FieldByName('Carteira').AsInteger = 0) and
       (QrLct.FieldByName('Tipo').AsInteger = 0) then
    begin
      if QrLct.FieldByName('Vencimento').AsDateTime < Date then
      Result := CO_QUIT_AUTOM
      else
      Result := CO_PREDATADO;
    end else Result := CO_COMPENSADA;
  end else
    if QrLct.FieldByName('Vencimento').AsDateTime < Date then
       Result := CO_VENCIDA
  else
       Result := CO_EMABERTO;
end;
}

procedure TUModule.UpdUnlockA(Registro : String; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdX.Close;
  QvUpdX.DataBase := VAR_GOTOMySQLDBNAME;
  QvUpdX.SQL.Clear;
  QvUpdX.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdX.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdX.Params[0].AsString := Registro;
  QvUpdX.ExecSQL;
end;

procedure TUModule.UpdLockA(Registro : String; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdX.Close;
  QvUpdX.DataBase := VAR_GOTOMySQLDBNAME;
  QvUpdX.SQL.Clear;
  QvUpdX.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdX.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdX.Params[0].AsInteger := VAR_USUARIO;
  QvUpdX.Params[1].AsString := Registro;
  QvUpdX.ExecSQL;
end;

function TUModule.SelecionaDB(DBs: array of String): String;
var
  Item: Integer;
begin
  Item := MyObjects.SelRadioGroup('Selecione o modelo', '', DBs, 2);
  if Item < 0 then
    Item := 0;
  Result := DBs[Item];
  if Result = '' then
    Result := Application.Name;
end;

function TUModule.SelLockA(Registro : String; Database: TmySQLDatabase; Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelX.Close;
  QvSelX.DataBase := VAR_GOTOMySQLDBNAME;
  QvSelX.SQL.Clear;
  QvSelX.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelX.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelX.Params[0].AsString := Registro;
  UnDmkDAC_PF.AbreQuery(QvSelX,VAR_GOTOMySQLDBNAME);
  if (QvSelX.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOMySQLDBNAME;
    QvUser.Params[0].AsInteger := QvSelX.FieldByName('Lk').AsInteger;
    UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;

    if (QvSelX.FieldByName('Lk').AsInteger = VAR_USUARIO) then
    begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_21+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end else if (VAR_SENHA = VAR_BOSS) then
    begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_1+Usuario+CO_PONTO+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end else
    begin
      Geral.MB_Aviso(FIN_MSG_REGISTROTRAVADO1 + sLineBreak +
      FIN_MSG_REGISTROTRAVADO3_1 + sLineBreak + Usuario+CO_PONTO);
      QvUser.Close;
    end;
  end
  else
    Result := False;
  QvSelX.Close;
end;

function TUModule.IncrementaControle(Database:TmySQLDatabase; TabControle,
 FieldControl: String) : Integer;
begin
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.Close;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT '+FieldControl+' Codigo FROM '+lowercase(tabcontrole));
  UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
  Result := QvLivreY.FieldByName('Codigo').AsInteger + 1;
 //
  QvUpdY.Close;
  QvUpdY.Database := Database;
  UnDmkDAC_PF.AbreQuery(QvUpdY, Database);
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FieldControl+'=:P0');
  QvUpdY.Params[0].AsInteger := Result;
  QvUpdY.ExecSQL;
end;

function TUModule.InsereRegistrosPreDefinidos(Query: TMySQLQuery; Tabela: String;
  SQL: array of String): Boolean;
var
  I: Integer;
  MyCursor: TCursor;
  Linha, Txt, Campos: String;
  SQLa, SQLb: WideString;
begin
  Result := False;
  MyCursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;

    Linha := SQL[0];
    SQLa := 'INSERT INTO ' + Lowercase(Tabela) + ' (' + sLineBreak;
    while Linha <> '' do
    begin
      if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Linha, Txt, Linha) then
        Campos := Campos + ', ' + Txt;
    end;
    //
    if High(SQL) >= 1 then // 0 = 1 linha
    begin
      for I := 1 to High(SQL) do
      begin
        SQLb := '';
        Linha := SQL[I];
        while Linha <> '' do
        begin
          if Geral.SeparaPrimeiraOcorrenciaDeTexto('|', Linha, Txt, Linha) then
            SQLB := SQLB + ', "' + Txt + '"';
        end;
        //
        if SQLb <> '' then
        begin
          ExecutaMySQLQuery1(Query, [
          SQLa,
          Copy(Campos, 3),
          ') VALUES (',
          Copy(SQLb, 3),
          ')']);
        end;
      end;
    end;
    Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    Geral.MB_Erro('Erro ao tentar executar uma SQL no InsereRegistrosPreDefinidos!' + sLineBreak +
    'Avise a DERMATEK!');
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
    raise;
  end;
end;

function TUModule.BuscaEmLivreY(Database: TmySQLDatabase;
  TabLivre, TabControle, Table, FieldControl, FieldTable : String;
  Forca_1: Boolean = True) : Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
  QvLivreY.SQL.Add('ORDER BY Codigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
    QvUpdY.SQL.Add('AND Codigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsInteger := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
    QvLivreY.Params[0].AsInteger := Codigo;
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
      //
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsInteger := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if (QvLivreY.FieldByName('Codigo').AsString = '') and Forca_1 then
    begin
      if Codigo = 0 then
        Codigo := 1;
    end else if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
    //
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsInteger := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUModule.BuscaEmLivreY_MinMax(Database: TmySQLDatabase;
  TabLivre, TabControle, Table, FieldControl, FieldTable : String; Minimo,
  Maximo: Integer) : Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  Codigo := 0;
  QvUpdY.Close;
  QvUpdY.Database := Database;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;
  //
  if Minimo = 0 then
  begin
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
    QvUpdY.ExecSQL;
    QvLivreY.Close;
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
    QvLivreY.SQL.Add('ORDER BY Codigo');
    QvLivreY.Params[0].AsString  := lowercase(Table);
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if (QvLivreY.FieldByName('Codigo').AsInteger > 0) and (Minimo = 0) then
    begin
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
      QvUpdY.SQL.Add('AND Codigo=:P1');
      QvUpdY.Params[0].AsString := lowercase(Table);
      QvUpdY.Params[1].AsInteger := Codigo;
      QvUpdY.ExecSQL;
      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
      QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
      QvLivreY.Params[0].AsInteger := Codigo;
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      if QvLivreY.RecordCount > 0 then
      begin
        QvLivreY.Close;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
        UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

        //Novo// Evitar duplica��o
        QvLivreY.Database := Database;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
        UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
        if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
          Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
        //
        QvUpdY.Close;
        QvUpdY.SQL.Clear;
        QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
        QvUpdY.Params[0].AsInteger := Codigo;
        QvUpdY.ExecSQL;
      end;
    end;
  end
  else
  begin
    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add('WHERE '+FieldTable+' BETWEEN :MINIMO AND :MAXIMO');
    QvLivreY.Params[0].AsInteger := Minimo;
    QvLivreY.Params[1].AsInteger := Maximo-1;
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if QvLivreY.FieldByName('Codigo').AsString = '' then Codigo := Minimo
    else if QvLivreY.FieldByName('Codigo').AsInteger >= Minimo then
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
    if Codigo > Maximo then Codigo := Maximo;
    //
  end;
  QvLivreY.Close;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUModule.BuscaIntSafe(Database: TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  //Novo// Evitar duas chamadas conjuntas
  QvLivreY.Close;
  QvLivreY.Database := Database;

  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  (*QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('LOCK TABLES '+lowercase(TabControle) + ' READ, ' + lowercase(TabLivre) + ' READ');
  QvLivreY.ExecSQL;*)
  try
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
    QvUpdY.ExecSQL;
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
    QvLivreY.SQL.Add('ORDER BY Codigo');
    QvLivreY.Params[0].AsString := lowercase(Table);
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
    begin
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
      QvUpdY.SQL.Add('AND Codigo=:P1');
      QvUpdY.Params[0].AsString := lowercase(Table);
      QvUpdY.Params[1].AsInteger := Codigo;
      QvUpdY.ExecSQL;
      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
      QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
      QvLivreY.Params[0].AsInteger := Codigo;
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      if QvLivreY.RecordCount > 0 then
      begin
        //
        QvLivreY.Close;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
        UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

        //Evitar duplica��o
        QvLivreY.Database := Database;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
        UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
        if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
          Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
        //
        QvUpdY.Close;
        QvUpdY.SQL.Clear;
        QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
        QvUpdY.Params[0].AsInteger := Codigo;
        QvUpdY.ExecSQL;
      end;
    end
    else
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      if QvLivreY.FieldByName('Codigo').AsString = '' then Codigo := 1
      else if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
      //
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsInteger := Codigo;
      QvUpdY.ExecSQL;
    end;
    QvLivreY.Close;
  finally
    //Novo// Evitar duas chamadas conjuntas
    (*QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('UNLOCK TABLES');
    QvLivreY.ExecSQL;*)
  end;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUModule.AtualizaLaRegistro(Database: TmySQLDatabase; Tabela : String;
 RecCount, Increm : Integer) : String;
var
  Total : Integer;
begin
  QvRecCountY.SQL.Clear;
  QvRecCountY.Database := Database;
  QvRecCountY.SQL.Add('SELECT COUNT(*) Record From '+lowercase(Tabela)+'');
  UnDmkDAC_PF.AbreQuery(QvRecCountY, Database);
  Total := QvRecCountY.FieldByName('Record').AsInteger;
  QvRecCountY.Close;
  case Increm of
    -2 : VAR_RECNO := 0;
     0 : if Total > 0 then VAR_RECNO := 1 else VAR_RECNO := 0;
     2 : VAR_RECNO := Total;
     else VAR_RECNO := VAR_RECNO + Increm;
  end;
  if VAR_RECNO > RecCount then VAR_RECNO := RecCount;
  if VAR_RECNO < 0 then VAR_RECNO := 0;
  if (RecCount > 0) and (VAR_RECNO = 0) then VAR_RECNO := 1;
  Result := IntToStr(VAR_RECNO)+CO_DE+IntToStr(RecCount)+CO_DE+IntToStr(Total);
end;

procedure TUModule.UpdLockY(Registro : Integer; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsInteger := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdLockY_Text(Registro : String; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsString := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdMulReg01(Help, Titulo, Prompt: String; DataSource:
  TmySQLQuery; ListSource: TDataSource; ListField: String; Default: Variant;
  SQLSorc: String; DataBase: TmySQLDatabase; SQLDest, Tabela, FldUpdNom,
  FldIdxNom: String; UserDataAlterweb: Boolean);
var
  Qry: TmySQLQuery;
begin
{$IFDEF sUPD}
  Application.CreateForm(TFmUpdMulReg, FmUpdMulReg);
  FmUpdMulReg.MeHelp.Text := Help;
  FmUpdMulReg.GBSel.Caption := Titulo;
  FmUpdMulReg.LaSel.Caption := Prompt;
  if DataSource <> nil then
  begin
    Qry := DataSource;
    Qry.DataBase := DataBase;
  end else
  begin
    Qry := FmUpdMulReg.QrSel;
  end;
  //
  if DataBase <> nil then
    Qry.Database := DataBase;
  //
  if Length(SQLSorc) > 0 then
  begin
    Qry.Close;
    Qry.SQL.Text := Geral.ATS(SQLSorc);
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  end;
  FmUpdMulReg.QrPsq.SQL.Text := SQLDest;
  UnDmkDAC_PF.AbreQuery(FmUpdMulReg.QrPsq, Qry.Database);
  //precisa ser antes
  if ListField <> '' then
    FmUpdMulReg.CBSel.ListField  := ListField;
  if ListSource <> nil then
    FmUpdMulReg.CBSel.ListSource := ListSource;
  //
  if Default <> Null then
  begin
    FmUpdMulReg.EdSel.ValueVariant := Default;
    FmUpdMulReg.CBSel.KeyValue     := Default;
  end;
  //
  FmUpdMulReg.FTabela              := Tabela;
  FmUpdMulReg.FFldUpdNom           := FldUpdNom;
  FmUpdMulReg.FFldIdxNom           := FldIdxNom;
  //
  FmUpdMulReg.ShowModal;
  FmUpdMulReg.Destroy;
{$ELSE}
  Geral.MB_Aviso('Janela UpdMulReg n�o habilitada para este app! (sUPD)');
{$ENDIF}
end;

procedure TUModule.UpdLockY_Double(Registro : Double; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdLockInt64Y(Registro : Int64; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdUnlockY(Registro : Integer; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdUnlockY_Text(Registro : String; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsString := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdUnlockY_Double(Registro : Double; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdUnlockInt64Y(Registro : Int64; Database: TmySQLDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdLockTudoY(Database: TmySQLDatabase; Table : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.ExecSQL;
end;

procedure TUModule.UpdUnlockTudoW(Database: TmySQLDatabase; Table : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.ExecSQL;
end;

procedure TUModule.PesquisaESeleciona(Tabela, CampoNome, CampoCodigo: String);
begin
  VAR_CADASTRO := 0;
  MyObjects.CriaForm_AcessoTotal(TFmPesqESel, FmPesqESel);
  FmPesqESel.FND := True;
  FmPesqESel.FNomeTabela := Tabela;
  FmPesqESel.FNomeCampoNome := CampoNome;
  FmPesqESel.FNomeCampoCodigo := CampoCodigo;
  FmPesqESel.ShowModal;
  FmPesqESel.Destroy;
end;

procedure TUModule.PoeEmLivreY(Database: TmySQLDatabase; TabLivre, Table : String; Codigo : Integer);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('INSERT INTO '+lowercase(tablivre)+' SET Codigo=:P0, Tabela=:P1');
  QvUpdY.Params[0].AsInteger := Codigo;
  QvUpdY.Params[1].AsString := lowercase(Table);
  QvUpdY.ExecSQL;
end;

(*procedure TUModule.PoeEmLivreY_Text(Database: TmySQLDatabase; TabLivre, Table : String; Codigo : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('INSERT INTO '+lowercase(tablivre)+' SET Codigo=:P0, Tabela=:P1');
  QvUpdY.Params[0].AsString := Codigo;
  QvUpdY.Params[1].AsString := lowercase(Table);
  QvUpdY.ExecSQL;
end;*)

function TUModule.SelLockY(Registro : Integer; Database: TmySQLDatabase; Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsInteger := Registro;
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOMySQLDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;

    if (QvSelY.FieldByName('Lk').AsInteger = VAR_USUARIO) then
    begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_21+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end else begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_1+Usuario+CO_PONTO+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
      // ini 2021-06-28
      if Geral.MB_Pergunta('Deseja editar assim mesmo com a senha BOSS?') = ID_YES then
      begin
        if DBCheck.LiberaPelaSenhaBoss() then
          Result := False;
      end;
      // fim 2021-06-28
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUModule.SelLockY_Text(Registro : String; Database: TmySQLDatabase; Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsString := Registro;
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOMySQLDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;

    if (QvSelY.FieldByName('Lk').AsInteger = VAR_USUARIO) then
    begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_21+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end else begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_1+Usuario+CO_PONTO+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUModule.SelLockY_Double(Registro : Double; Database: TmySQLDatabase;
  Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsFloat := Registro;
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOMySQLDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;
    if (QvSelY.FieldByName('Lk').AsInteger <> VAR_USUARIO) then
    begin
      Geral.MB_Erro(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_1+sLineBreak+Usuario+CO_PONTO);
      QvUser.Close;
    end
    else
    begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_21+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUModule.SelLockInt64Y(Registro : Int64; Database: TmySQLDatabase;
  Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsFloat := Registro;
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOMySQLDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;
    if (QvSelY.FieldByName('Lk').AsInteger <> VAR_USUARIO) then
    begin
      Geral.MB_Erro(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_1+sLineBreak+Usuario+CO_PONTO);
      QvUser.Close;
      // ini 2021-06-28
      if Geral.MB_Pergunta('Deseja editar assim mesmo com a senha BOSS?') = ID_YES then
      begin
        if DBCheck.LiberaPelaSenhaBoss() then
          Result := False;
      end;
      // fim 2021-06-28
    end
    else
    begin
      if Geral.MB_Pergunta(FIN_MSG_REGISTROTRAVADO1+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_21+sLineBreak+
      FIN_MSG_REGISTROTRAVADO3_22) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUModule.SelLockQualquerY(Database: TmySQLDatabase; Table, NomeTable : String) : Boolean;
var
  Usuario : String;
  Usuario2 : Integer;
  Mensagem: PChar;
begin
  Mensagem := PChar('???');
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE Lk <> 0');
  QvSelY.SQL.Add('ORDER BY Lk');
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  if (QvSelY.RecordCount > 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    if (QvSelY.RecordCount = 1) then
    begin
      QvUser.Close;
      QvUser.DataBase := VAR_GOTOMySQLDBNAME;
      QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
      UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
      if QvUser.RecordCount > 0 then
         Usuario := QvUser.FieldByName('Login').AsString;
      Geral.MB_Erro(FIN_MSG_REGISTROTRAVADO4_1+NomeTable+
      sLineBreak + FIN_MSG_REGISTROTRAVADO4_2+Usuario+CO_PONTO);
    end
    else
    begin
      Mensagem := PChar(FIN_MSG_REGISTROTRAVADO5_1+
      IntToStr(QvSelY.RecordCount)+
      FIN_MSG_REGISTROTRAVADO5_2+NomeTable+sLineBreak+
      FIN_MSG_REGISTROTRAVADO5_3);
      QvSelY.First;
      Usuario2 := 0;
      while not QvSelY.Eof do
      begin
        if QvSelY.FieldByName('Lk').AsInteger <> Usuario2 then
        begin
          Usuario2 := QvSelY.FieldByName('Lk').AsInteger;
          QvUser.Close;
          QvUser.DataBase := VAR_GOTOMySQLDBNAME;
          QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
          UnDmkDAC_PF.AbreQuery(QvUser,VAR_GOTOMySQLDBNAME);
          if QvUser.RecordCount > 0 then
          Mensagem := PChar(Mensagem+
          sLineBreak+QvUser.FieldByName('Login').AsString);
        end;
        QvSelY.Next;
      end;
    end;
    Geral.MB_Erro(Mensagem);
    QvUser.Close;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUModule.BuscaEmLivreY_Double(Database: TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Double;
var
  Codigo : Double;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE (Codigo < 1) and (BigCodigo < 1)');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT BigCodigo Codigo FROM '+lowercase(tablivre)+' WHERE BigTabela=:Table');
  QvLivreY.SQL.Add('ORDER BY BigCodigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE BigTabela=:P0');
    QvUpdY.SQL.Add('AND BigCodigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsFloat := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
//    QvLivreM.Params[0].AsString := lowercase(Table);
    QvLivreY.Params[0].AsFloat := Codigo;
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsFloat := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsFloat := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUModule.BuscaEmLivreInt64Y(Database: TmySQLDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Int64;
var
  Codigo : Int64;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE (Codigo < 1) and (BigCodigo < 1)');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT BigCodigo Codigo FROM '+lowercase(tablivre)+' WHERE BigTabela=:Table');
  QvLivreY.SQL.Add('ORDER BY BigCodigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE BigTabela=:P0');
    QvUpdY.SQL.Add('AND BigCodigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsFloat := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
//    QvLivreM.Params[0].AsString := lowercase(Table);
    QvLivreY.Params[0].AsFloat := Codigo;
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsFloat := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsFloat := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;
  //
  Result := Codigo;
end;

procedure TUModule.PoeEmLivreY_Double(Database: TmySQLDatabase; TabLivre, Table : String; Codigo : Double);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('INSERT INTO '+lowercase(tablivre)+' (BigCodigo, BigTabela) VALUES (:P0, :P1)');
  QvUpdY.Params[0].AsFloat := Codigo;
  QvUpdY.Params[1].AsString := lowercase(Table);
  QvUpdY.ExecSQL;
end;

function TUModule.VerificaDuplicadoStr(Database: TmySQLDatabase; Tabela, CampoDescri, CampoCod, Descricao,
 CampoAnterior: String; Codigo : Integer) : Boolean;
var
  Duplicado : Boolean;
begin
  Duplicado := False;
  QvDuplicStrY.Close;
  QvDuplicStrY.Database := Database;
  QvDuplicStrY.SQL.Clear;
  QvDuplicStrY.SQL.Add('SELECT '+CampoDescri+' NOME, '+CampoCod+
  ' CODIGO, '+CampoAnterior+' ANTERIOR FROM '+lowercase(tabela)+'');
  QvDuplicStrY.SQL.Add('WHERE '+CampoDescri+'=:Nome');
  QvDuplicStrY.Params[0].AsString := Descricao;
  UnDmkDAC_PF.AbreQuery(QvDuplicStrY, Database);
  if QvDuplicStrY.RecordCount > 1 then Duplicado := True;
  if (QvDuplicStrY.RecordCount = 1) and
     (QvDuplicStrY.FieldByName('Codigo').AsInteger <> Codigo) then Duplicado := True;
  if Duplicado then
      Geral.MB_Erro(VAR_MSG_ITEMDUPLICADO);
  if Duplicado then
  begin
    VAR_DUPLICCOD := QvDuplicStrY.FieldByName('Codigo').AsInteger;
    VAR_DUPLICANT := QvDuplicStrY.FieldByName('ANTERIOR').AsInteger;
  end;
  Result := Duplicado;
  QvDuplicStrY.Close;
end;

function TUModule.VerificaDuplicado1(Database: TmySQLDatabase; Tabela: String;
  CamposFind: array of String; ValoresFind: array of Variant; CampoOmit: String;
  ValorOmit: Variant): Boolean;
var
  Valor: String;
  SQL: array of String;
  I, J: Integer;
begin
  Result := False;
  I := High(CamposFind);
  J := High(ValoresFind);
  if I <> J then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(I + 1) + ' campos e ' +
    IntToStr(J + 1) + ' valores para estes campos em "VerificaDuplicado1"!' +
    sLineBreak + 'Tabela: "' + Tabela + '"');
    Exit;
  end;
  SetLength(SQL, I + 3);
  //
  SQL[0] := 'SELECT * FROM ' + Tabela;
  for I := Low(CamposFind) to J do
  begin
    Valor := Geral.VariavelToString(ValoresFind[I]);
    if (I = Low(CamposFind)) then
      SQL[I + 1] := 'WHERE ' + CamposFind[I] + '=' + Valor
    else
      SQL[I + 1] := 'AND ' + CamposFind[I] + '=' + Valor
  end;
  if CampoOmit <> '' then
    SQL[J + 2] := 'AND ' + CampoOmit + '<>' + Geral.VariavelToString(ValorOmit);
  QvDuplicIntY.Close;
  if DataBase <> nil then
    QvDuplicIntY.Database := Database;
  UnDmkDAC_PF.AbreMySQLQuery0(QvDuplicIntY, Database, SQL);
  Result := QvDuplicIntY.RecordCount > 0;
  if Result then
    Geral.MB_Aviso(VAR_MSG_ITEMDUPLICADO);
  QvDuplicIntY.Close;
end;

function TUModule.VerificaDuplicadoInt(Database: TmySQLDatabase; Tabela, CampoInteiro1, CampoInteiro2,
 CampoCod: String; Inteiro1, Inteiro2, Codigo : Integer) : Boolean;
var
  Duplicado : Boolean;
begin
  Duplicado := False;
  QvDuplicIntY.Close;
  QvDuplicIntY.Database := Database;
  QvDuplicIntY.SQL.Clear;
  QvDuplicIntY.SQL.Add('SELECT '+CampoInteiro1+' INTEIRO1, '+CampoInteiro2+
  ' INTEIRO2, '+CampoCod+' CODIGO FROM '+lowercase(tabela)+'');
  QvDuplicIntY.SQL.Add('WHERE '+CampoInteiro1+'=:P0');
  QvDuplicIntY.SQL.Add('AND '+CampoInteiro2+'=:P1');
  QvDuplicIntY.Params[0].AsInteger := Inteiro1;
  QvDuplicIntY.Params[1].AsInteger := Inteiro2;
  UnDmkDAC_PF.AbreQuery(QvDuplicIntY, Database);
  if QvDuplicIntY.RecordCount > 1 then Duplicado := True;
  if (QvDuplicIntY.RecordCount = 1) and
     (QvDuplicIntY.FieldByName('Codigo').AsInteger <> Codigo) then Duplicado := True;
  if Duplicado then
    Geral.MB_Aviso(VAR_MSG_ITEMDUPLICADO);
  if Duplicado = True then
  begin
    VAR_DUPLICCOD := QvDuplicIntY.FieldByName('Codigo').AsInteger;
//    VAR_DUPLICANT := QvDuplicIntYANTERIOR.Value;
  end;
  Result := Duplicado;
  QvDuplicIntY.Close;
end;

function TUModule.VerificaDuplicadoNovo(SQLType: TSQLType; QrPsq: TMySQLQuery;
  Database: TmySQLDatabase; Tabela, MasterFld, PriKeyFld: String; MasterVal,
  PriKeyVal: Variant; ValorFld: String; ValorVal: Variant; ShowMsgDupl:
  Boolean): Boolean;
var
  SQL_AND, sInsAlt: String;
begin
  if SQLType = stUpd then
  begin
    SQL_AND := 'AND ' + PriKeyFld + '<>' + Geral.VariavelToString(PriKeyVal);
    sInsAlt := 'Altera��o';
  end else
  begin
    SQL_AND := '';
    sInsAlt := 'Inclus�o';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQUery0(QrPsq, Database, [
  'SELECT ' + PriKeyFld,
  'FROM ' + LowerCase(Tabela),
  'WHERE ' + MasterFld + '=' + Geral.VariavelToString(MasterVal),
  'AND ' + ValorFld + '=' + Geral.VariavelToString(ValorVal),
  SQL_AND,
  EmptyStr]);
  //Geral.MB_SQL(nil, QrPsq);
  Result := QrPsq.RecordCount > 0;
  if Result then
  begin
    if ShowMsgDupl then
      Geral.MB_Aviso(sInsAlt + ' abortada!' + sLineBreak +
      'Registro j� existe sob o ID > "' + PriKeyFld + '" = ' +
      QrPsq.FieldByName(PriKeyFld).AsString);
  end;
end;

procedure TUModule.UpdateControleData(Database: TmySQLDatabase; TabControle, Field : String; Data : TDate);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+Field+'=:P0');
  QvUpdY.Params[0].AsDate := Data;
  QvUpdY.ExecSQL;
end;

function TUModule.NaoPermiteExcluirDeTable(): Boolean;
begin
  Result := False;
  Geral.MB_Aviso('Exclus�o n�o permitida!');
end;

function TUModule.NegaInclusaoY(Database: TmySQLDatabase; Tabela, Campo, TabLivre: String;
 Maximo: Integer) : Boolean;
begin
  Result := True;
  //
  QvRecCountY.Close;
  QvRecCountY.SQL.Clear;
  QvRecCountY.Database := Database;
  QvRecCountY.SQL.Add('SELECT '+Campo+' Record FROM '+lowercase(tabela));
  UnDmkDAC_PF.AbreQuery(QvRecCountY, Database);
  if QvRecCountY.FieldByName('Record').AsInteger < Maximo then
  begin
    Result := False;
    QvRecCountY.Close;
    Exit;
  end;
  //
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela="'+Campo+'"');
  QvLivreY.SQL.Add('ORDER BY Codigo');
  UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then Result := False;
  QvLivreY.Close;
end;

procedure TUModule.LogIns(Database: TMySQLDataBase; Tipo, ID: Integer);

(*Tipo:
  1 - Pesagem de Insumos qu�micos
  2 - Altera��o de pesagem de PQ
  3 - Transferencia em Money
*)
begin
  QvInsLogY.DataBase := Database;
  QvInsLogY.Params[0].AsInteger := Tipo;
  QvInsLogY.Params[1].AsInteger := VAR_USUARIO;
  QvInsLogY.Params[2].AsInteger := ID;
  QvInsLogY.ExecSQL;
  //dbiSaveChanges(QvInsLogY.Handle);
end;

procedure TUModule.LogDel(Database: TMySQLDataBase; Tipo, ID: Integer);
begin
  QvDelLogY.DataBase := DataBase;
  QvDelLogY.Params[0].AsInteger := Tipo;
  QvDelLogY.Params[1].AsInteger := VAR_USUARIO;
  QvDelLogY.Params[2].AsInteger := ID;
  QvDelLogY.ExecSQL;
  //dbiSaveChanges(QvDelLogY.Handle);
end;

procedure TUModule.OcultaEdicaoTb(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := True else
    if Form.Components[i] is TPanel then
    begin
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
        TPanel(Form.Components[i]).Enabled := False else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
        TPanel(Form.Components[i]).Visible := False else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
        TPanel(Form.Components[i]).Visible := True;
    end;
  end;
end;

function TUModule.MontaOrdemSQL(Pertence: array of Boolean;
  Campo: array of String): String;
var
  I, J: Integer;
begin
  Result := '';
  I := High(Pertence);
  J := High(Campo);
  if I <> I then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(J+1) + ' campos e ' +
    IntToStr(I+1) + ' indica��es de ordena��o em  "MontaOrdemSQL"!');
    Exit;
  end;
  for I := 0 to J do
  begin
    if Pertence[I] = True then
      Result := Result + Campo[I] + ', ';
  end;
  J := Length(Result);
  if J > 0 then
    Result := 'ORDER BY ' + Copy(Result, 1, J - 2);
end;

procedure TUModule.MostraEdicaoTb(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := False else
    if Form.Components[i] is TPanel then
    begin
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
        TPanel(Form.Components[i]).Enabled := True else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
        TPanel(Form.Components[i]).Visible := True else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
        TPanel(Form.Components[i]).Visible := False;
    end;
  end;
end;

procedure TUModule.MostraSQL(Query: TmySQLQuery; Memo: TMemo; Titulo: String);
var
  i: Integer;
begin
  Memo.Lines.Add('===== '+Titulo+' =====');
  for i := 0 to Query.SQL.Count -1 do Memo.Lines.Add(Query.SQL[i]);
  Memo.Lines.Add('=== FIM '+Titulo+' ===');
end;

function TUModule.MoveRegistroEntreTabelas(QrUpd: TmySQLQuery; TabOrig, TabDest:
  String; IdxFldDel, Operador: array of String; IdxValDel: array of Variant;
  Acao: TAcaoMoverRegistro): Boolean;
var
  Dta, CamposDel, Condicoes: String;
  QtdFlds: Integer;
begin
  Result := False;
  if Acao = amrNothing then
  begin
    Result := True;
    Exit;
  end;
  //
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  CamposDel := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabDest, '', QtdFlds);
  CamposDel := Geral.Substitui(CamposDel,
    ', DataDel', ', "' + Dta + '" DataDel');
  CamposDel := Geral.Substitui(CamposDel,
    ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
  //
  QrUpd.SQL.Clear;
  if GeraCondicaoDeSQL(Condicoes, TabDest, IdxFldDel, Operador, IdxValDel,
  'WHERE', '') then
  begin
    if Acao in ([amrOnlyIns, amrInsAndDel]) then
    begin
      QrUpd.SQL.Add('INSERT INTO ' + TabDest + ' SELECT ' + sLineBreak +
      CamposDel + sLineBreak + 'FROM ' + TabOrig + sLineBreak + Condicoes);
    end;
    if Acao in ([amrOnlyDel, amrInsAndDel]) then
      QrUpd.SQL.Add(DELETE_FROM + TabOrig + sLineBreak + Condicoes);
    UMyMod.ExecutaQuery(QrUpd);
    Result := True;
  end;
end;

function TUModule.MudaOrdemRegistroAtual(Tabela, FldIndice, FldOrdem: String;
  QueryAReordenar, QrUpd: TmySQLQuery): Boolean;
  procedure AtualizaOrdemDoItem(Ordem, Indice: Integer);
  begin
    UMyMod.SQLInsUpd(QrUpd, stUpd, Lowercase(Tabela), False, [
    FldOrdem], [FldIndice], [Ordem], [Indice], False);
  end;
var
  Ordem: Double;
  I, N, K, Indice: Integer;
begin
  Result := False;
  Indice := QueryAReordenar.FieldByName(FldIndice).AsInteger;
  if dmkPF.ObtemValorDouble(Ordem, 0) then
  begin
    N := Trunc(Ordem);
    if (N < 1) or (N > QueryAReordenar.RecordCount) then
    begin
      Geral.MB_Aviso('A ordem ' + IntToStr(N) + ' n�o existe!');
      Exit;
    end;
    I := 0;
    K := QueryAReordenar.FieldByName(FldIndice).AsInteger;
    //
    AtualizaOrdemDoItem(N, K);
    //
    QueryAReordenar.First;
    while not QueryAReordenar.Eof do
    begin
      if K <> QueryAReordenar.FieldByName(FldIndice).AsInteger then
      begin
        I := I + 1;
        if (I = N) then
          I := I + 1;
        //
        AtualizaOrdemDoItem(I, QueryAReordenar.FieldByName(FldIndice).AsInteger);
      end;
      QueryAReordenar.Next;
    end;
    //
    QueryAReordenar.Close;
    UnDmkDAC_PF.AbreQuery(QueryAReordenar, QueryAReordenar.DataBase);

    if QueryAReordenar.Locate(FldIndice, Indice, []) then
      Result := QueryAReordenar.FieldByName(FldOrdem).AsInteger = N;
  end;
end;

procedure TUModule.IncluiRegistroTb(Form: TForm; Tabela: TmySQLTable;
  ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
begin
  Screen.Cursor := crHourGlass;
  MostraEdicaoTb(Form);
  Tabela.Insert;
  if ComponentToFocus <> nil then
  try
    ComponentToFocus.SetFocus;
  except
  end;
  if ImgTipo <> nil then ImgTipo.SQLType := stIns;
  Screen.Cursor := crDefault;
end;

procedure TUModule.AlteraRegistroTb(Form: TForm; Tabela: TmySQLTable;
  ComponentToFocus: TWinControl; ImgTipo: TdmkImage);
begin
  Screen.Cursor := crHourGlass;
  try
    MostraEdicaoTb(Form);
    if ImgTipo <> nil then ImgTipo.SQLType := stUpd;
    Tabela.Edit;
    if ComponentToFocus <> nil then
      ComponentToFocus.SetFocus;
  finally
    Screen.Cursor := crDefault;
  end;
end;

(* N�o usa
function TUModule.ValorNuloEVazio1(Query: TmySQLQuery; Campo: String): Boolean;
begin
  Result :=
  (VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varNull) or
  (
    (
      (VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varString)
{$IFDEF DELPHI12_UP}
      or
      (VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varUString)
{$ENDIF}
    )
    and
    (TmySQLQuery(Query).FieldByName(Campo).AsVariant = '')
  );
end;
*)

function TUModule.ValorNuloEVazio2(Calsse: TClass; Objeto: TObject;
  Query: TmySQLQuery; Campo: String): Boolean;
begin
  Result :=
  (VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varNull) or
  (
    (
      (VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varString)
{$IFDEF DELPHI12_UP}
      or
      (VarType(TmySQLQuery(Query).FieldByName(Campo).AsVariant) = varUString)
{$ENDIF}
    )
    and
    (TmySQLQuery(Query).FieldByName(Campo).AsVariant = '')
  );
end;

function TUModule.VerificaCamposObrigatorios(Tabela: TMySQLTable): Boolean;
var
  i: integer;
begin
  Result := True;
  if not (Tabela.State in ([dsInsert, dsEdit])) then exit;
  for i := 0 to Tabela.FieldCount - 1 do
  begin
    if Uppercase(Tabela.Fields[i].FieldName) = 'NOME'
      then if trim(Tabela.FieldByName('Nome').AsString)= '' then
      begin
        Geral.MB_Erro('Informa um nome!');
        Screen.Cursor := crDefault;
        Result := False;
      end;
  end;
end;

procedure TUModule.DadosAutomaticosTb(Tabela: TMySQLTable);
begin
  if Tabela.State = dsInsert then
  begin
    Tabela.FieldByName('DataCad').AsDateTime := Date;//FormatDateTime(VAR_FORMATDATE, Date);
    Tabela.FieldByName('UserCad').AsInteger  := VAR_USUARIO;
  end else if Tabela.State = dsEdit then
  begin
    Tabela.FieldByName('DataAlt').AsDateTime := Date;//FormatDateTime(VAR_FORMATDATE, Date);
    Tabela.FieldByName('UserAlt').AsInteger  := VAR_USUARIO;
  end;
end;

procedure TUModule.ConfirmaRegistroTb_Codigo(Form: TForm; Tabela: TmySQLTable);
begin
  Screen.Cursor := crHourGlass;
  if not VerificaCamposObrigatorios(Tabela) then Exit;
  DadosAutomaticosTb(Tabela);
  if Tabela.State = dsInsert then
  begin
    Tabela.FieldByName('Codigo').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres',
    'controle', Tabela.TableName, Tabela.TableName, 'Codigo');
  end;
  OcultaEdicaoTb(Form);
  Tabela.Post;
  Screen.Cursor := crDefault;
end;

procedure TUModule.ConfirmaRegistroTb_Numero(Form: TForm; Tabela: TmySQLTable;
ImgTipo: TdmkImage; PainelDados, PainelConfirma, PainelControle: TPanel);
var
  i: integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if not (Tabela.State in ([dsInsert, dsEdit])) then
      Tabela.Edit;
    if not VerificaCamposObrigatorios(Tabela) then
      Exit;
    DadosAutomaticosTb(Tabela);
    if Tabela.State = dsInsert then
    begin
      if Tabela.FieldByName('Numero').AsInteger = 0 then
      Tabela.FieldByName('Numero').AsInteger :=
        BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres',
        'controle', Tabela.TableName, Tabela.TableName, 'Numero');
    end;
    Tabela.Post;
    if ImgTipo <> nil then
      ImgTipo.SQLType := stLok;
    if PainelDados <> nil then
      PainelDados.Enabled := False;
    if PainelConfirma <> nil then
      PainelConfirma.Visible := False;
    if PainelControle <> nil then
      PainelControle.Visible := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUModule.DefineDataBase(var MeuDB: String; const ExeName: String): Boolean;
(*
var
  U, I, Conta: Integer;
  BDs: array of String;
begin
  Result := False;
  Conta := Geral.ReadAppKeyCU('Count', Application.Title+'\Databases', ktInteger, 0);
  if Conta > 0 then
  begin
    SetLength(BDs, Conta);
    for I := 0 to Conta -1 do
      BDs[I] := Geral.ReadAppKeyCU(FormatFloat('0', I),
      Application.Title+'\Databases', ktString, MeuDB);
    U := Geral.ReadAppKeyCU('Last', Application.Title + '\' + ExeName, ktInteger, -1);
    I := MyObjects.SelRadioGroup('Sele��o da Base de Dados',
    'Lista de databases cadastradas', BDs, 2, U);
    if I > -1 then
    begin
      MeuDB := BDs[I];
      Geral.WriteAppKeyCU('Last', Application.Title + '\' + ExeName, I, ktInteger);
    end;
      //
    Result := MeuDB <> '';
    if not Result then
      Halt(0);
  end;
  // else TMeuDB := MeuDB;
*)
var
  Lista: TStrings;
  U, I, J, N, IDApp, Conta: Integer;
  Codigo, Controle: Integer;
  Nome: String;
  Reg: TRegistry;
  Caminho, CamFlds, DB: String;
  //
  BDs: array of String;
begin
  Result := False;
  IDApp  := -1;
  N      := 1;
  //
  Reg         := TRegistry.Create(KEY_READ);
  Reg.RootKey := HKEY_CURRENT_USER;
  try
    Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDACab');
    //
    Reg.OpenKeyReadOnly('Software\' + Caminho);
    //
    Lista := TStringList.Create;
    try
      Reg.GetValueNames(Lista);
      //
      Conta := Lista.Count;
      //
      if Conta > 0 then
      begin
        SetLength(BDs, Conta);
        //
        for I := 0 to Conta - 1 do
        begin
          Nome := Reg.ReadString(Lista[I]);
          //
          if UpperCase(Nome) = Uppercase(TMeuDB) then
          begin
            IDApp := Geral.IMV(Lista[I]);
          end;
        end;
      end;
    finally
      Lista.Free;
    end;
  finally
    Reg.Free;
  end;
    //
  if IDApp <> 0 then
  begin
    Reg         := TRegistry.Create(KEY_READ);
    Reg.RootKey := HKEY_CURRENT_USER;
    try
      Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDAIts');
      Reg.OpenKeyReadOnly('Software\' + Caminho);
      Lista := TStringList.Create;
      try
        Reg.GetValueNames(Lista);
        Conta := Lista.Count;
        if Conta > 0 then
        begin
          for I := 0 to Conta - 1 do
          begin
            Controle := Geral.IMV(Lista[I]);
            Nome     := Reg.ReadString(Lista[I]);
            CamFlds  := MyObjects.DBsDA_CaminhoRegistro(Caminho, Controle);
            Codigo   := Geral.ReadAppKeyCU('Codigo', CamFlds, ktInteger, 0);
            //
            if Codigo = IDApp then
            begin
              N := N + 1;
              SetLength(BDs, N);
              BDs[N - 1] := Nome;
            end;
          end;
          if N > 1 then
          begin
            BDs[0] := TMeuDB;
            Caminho := MyObjects.DBsDA_CaminhoTabela('DBsDAIts', False);
            U := Geral.ReadAppKeyCU('LastSel', Caminho, ktInteger, -1);
            I := MyObjects.SelRadioGroup('Sele��o da Base de Dados',
            'Lista de databases cadastradas', BDs, 2, U);
            if I > -1 then
            begin
              MeuDB := BDs[I];
              Geral.WriteAppKeyCU('LastSel', Caminho, I, ktInteger);
              Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', MeuDB, ktString);
              // 2018-02-28
              // Collect Data Server Repository
              if Pos(lowercase(Application.Name) + 'cdr', MeuDB) > 0 then
              begin
                J := MyObjects.SelRadioGroup('Objetivo da conex�o',
                'Objetivo da conex�o', ['Coletar dados',
                'Servidor de dados'], 1, -1);
                if J > -1 then
                begin
                  VAR_IS_CollectDataServerRepository := J = 1;
                end else
                begin
                  VAR_TERMINATE := True;
                  Application.Terminate;
                end;
              // Fim 2018-02-28
              end;
            end else
            begin
              VAR_TERMINATE := True;
              Application.Terminate;
            end;
              //
            Result := MeuDB <> '';
            if not Result then
              Halt(0);
          end;
        end;
      finally
        Lista.Free;
      end;
    finally
      Reg.Free;
    end;
  end;
  if Result = False then
  begin
    DB := Geral.ReadAppKeyCU('Database', Application.Title, ktString, '');
    //
    if DB <> '' then
    begin
      Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', MeuDB, ktString);
      //
      MeuDB  := DB;
      Result := True;
    end;
  end;
  if VAR_STDATABASES <> nil then
    VAR_STDATABASES.Text := MeuDB;
end;

procedure TUModule.DesisteRegistroTb(Form: TForm; Tabela: TmySQLTable;
  NomeTabela, NomeCampoCodigo: String;
  ImgTipo: TdmkImage; CodigoALiberar: Integer);
  // SQLType: TSQLType
begin
  Screen.Cursor := crHourGlass;
  OcultaEdicaoTb(Form);
  Tabela.Cancel;
  if ImgTipo.SQLType = stIns then
    PoeEmLivreY(Dmod.MyDB, 'livres', NomeTabela, CodigoALiberar)
  else if ImgTipo.SQLType = stUpd then
    UpdUnlockY(CodigoALiberar, Dmod.MyDB, NomeTabela, NomeCampoCodigo);
  //
  ImgTipo.SQLType := stLok;
  Screen.Cursor := crDefault;
end;

procedure TUModule.BELY_Tb(Tabela: TmySQLTable; Campo: TBELY);
var
  Atual: Double;
begin
  Screen.Cursor := crHourGlass;
  case Campo of
    belyCodigo   : Atual := Tabela.FieldByName('Codigo').AsInteger;
    belyControle : Atual := Tabela.FieldByName('controle').AsInteger;
    belyNumero   : Atual := Tabela.FieldByName('Numero').AsInteger;
    else Atual   := 0;
  end;
  if Atual < 0.1 then
  begin
    case Campo of
      belyCodigo   : Tabela.FieldByName('Codigo').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'Codigo');
      belyControle : Tabela.FieldByName('controle').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'controle');
      belyNumero   : Tabela.FieldByName('Numero').AsInteger := BuscaEmLivreY(VAR_GOTOMySQLDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'Numero');
      else begin
        Geral.MB_Erro('ERRO. Campo desconhecido na procedure "BELY"');
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TUModule.BPGS1I32(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer;
  DB: TmySQLDatabase): Integer;
begin
  Result := BuscaProximoGerlSeq1Int32(Tabela, Campo, _WHERE, _AND, TipoSinal,
    SQLType, DefUpd, Null, DB);
end;

function TUModule.BPGS1I32_Reaproveita(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer): Integer;
const
  TabLivre = 'livre2';
var
  Codigo : Integer;
  //FldCtrl: String;
  Continua: Boolean;
  Database: TmySQLDatabase;
begin
  Codigo := 0;
  Continua := True;
  Database := Dmod.MyDB;
  //
  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    Lowercase(Tabela) + ' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;

  while Continua do
  begin
    QvLivreY.Close;
    QvLivreY.Database := Database;
    UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
    'SELECT Codigo ',
    'FROM ' + TabLivre,
    'WHERE Tabela="' + Lowercase(Tabela) + '" ',
    'AND Campo="' + Campo + '" ',
    'ORDER BY Codigo ',
    'LIMIT 1',
    '']);
    //
    if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
    begin
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre));
      QvUpdY.SQL.Add('WHERE Tabela="' + Lowercase(Tabela) + '"');
      QvUpdY.SQL.Add('AND Campo="' + Campo + '"');
      QvUpdY.SQL.Add('AND Codigo=' + Geral.FF0(Codigo));
      QvUpdY.ExecSQL;
      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT ' + Campo + ' Codigo FROM ' + Lowercase(Tabela));
      QvLivreY.SQL.Add('WHERE ' + Campo + '=' + Geral.FF0(Codigo));
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      if QvLivreY.RecordCount > 0 then
      begin
        Codigo := 0;
        Continua := True;
      end else Continua := False;
    end else Continua := False;
  end;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  //
  
  if Codigo = 0 then
  begin
    Result := BuscaProximoGerlSeq1Int32(
      //Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType, Null, DefUpd);
      Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType, 0, DefUpd);
  end else Result := Codigo;
end;

function TUModule.BPGS1I32_Varios(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer; DB: TmySQLDatabase;
  Quantidade: Integer): Integer;
begin
  Result := BuscaProximoGerlSeq1Int32(Tabela, Campo, _WHERE, _AND, TipoSinal,
    SQLType, DefUpd, Null, DB, Quantidade);
end;

function TUModule.DiaInutil(Data : TDateTime): Integer;
var
  DiaSemana : Integer;
  Data_Txt, DiaAno_Txt: String;
  Util: Integer;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta fun��o mudar na WEB em:                        ///
  ///  libraries -> Dmk_datetime.php -> dia_inutil                          ///
  /////////////////////////////////////////////////////////////////////////////

  DiaSemana := DayOfWeek(Data);
  //
  case DiaSemana of
      1: Util := 201;// Domingo
      7: Util := 207;// S�bado
    else Util := 0;
  end;
  if Util = 0 then
  begin
    Data_Txt   := Geral.FDT(Data, 1);
    DiaAno_Txt := Geral.FDT(Data, 12);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM feriados ',
      'WHERE Data="' + Data_Txt + '" ',
      'OR ',
      '( ',
      'Anual = 1 ',
      'AND DATE_FORMAT(Data, "%d/%m") LIKE "' + DiaAno_Txt + '" ',
      ') ',
      '']);
    if Dmod.QrAux.RecordCount > 0 then
      Util := 208; // Feriado
  end;
  Result := Util;
  {
  Resultados:
  0: Dia �til
  201: Domingo
  207: S�bado
  208: Feriado cadastrado na tabela mySQL Feriados
  }
end;

function TUModule.CalculaDiasArray_Pula(Data, Vcto: TDateTime; DMai, Comp:Integer):
  MyArrayI1k;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  Mudou: Boolean;
begin
  for i := 0 to HIGH(Result) do
    Result[i] := 0;
  Przo := Trunc(Vcto - Data);
  for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  Mudou := False;
  if (Comp>0) or (DMai>0) then
  begin
    while Util <> 0 do
    begin
      Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      Mudou := True;
    end;
    if Mudou then Przo := Przo - 1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      Result[Przo] := 101;
    end;
    //
    for i := 1 to DMai do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      Result[Przo] := 102;
    end;
  end;
  //
  //Mudou := False;
  while Util <> 0 do
  begin
    Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
    //Mudou := True;
  end;
  //if Mudou then Przo := Przo - 1;
  Result[0] := Przo;
end;

function TUModule.CalculaDiasArray_Normal(Data, Vcto: TDateTime; DMai, Comp:Integer):
  MyArrayI1k;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  for i := 0 to HIGH(Result) do
    Result[i] := 0;
  Przo := Trunc(Vcto - Data);
  for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  if (Comp>0) or (DMai>0) then
  begin
    while Util <> 0 do
    begin
      Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      //Mudou := True;
    end;
    //if Mudou then Przo := Przo -1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      Result[Przo] := 101;
    end;
  end;
  while Util <> 0 do
  begin
    Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result[0] := Przo+DMai;
end;

function TUModule.CalculaDiasArray_NaoPula(Data, Vcto: TDateTime; DMai, Comp:Integer):
  MyArrayI1k;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  for i := 0 to HIGH(Result) do
    Result[i] := 0;
  Przo := Trunc(Vcto - Data) + Comp + DMai;
  for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  while Util <> 0 do
  begin
    Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result[0] := Przo;
end;

function TUModule.CalculaDias_Pula(Data, Vcto: TDateTime; DMai, Comp:Integer): Integer;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  //for i := 0 to HIGH(Result) do
    //Result(*[i]*) := 0;
  Przo := Trunc(Vcto - Data);
  //for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  if (Comp>0) or (DMai>0) then
  begin
    while Util <> 0 do
    begin
      //Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      //Mudou := True;
    end;
    //if Mudou then Przo := Przo - 1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        //Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      //Result[Przo] := 101;
    end;
    //
    for i := 1 to DMai do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        //Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      //Result[Przo] := 102;
    end;
  end;
  //
  //Mudou := False;
  while Util <> 0 do
  begin
    //Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
    //Mudou := True;
  end;
  //if Mudou then Przo := Przo - 1;
  Result(*[0]*) := Przo;
end;

function TUModule.CalculaDataDeposito(Vencimento: TDateTime): TDateTime;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  Util: integer;
  //Mudou: Boolean;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta fun��o mudar na WEB em:                        ///
  ///  libraries -> Dmk_datetime.php -> calcula_data_deposito               ///
  /////////////////////////////////////////////////////////////////////////////

  Real := Vencimento;
  Util := DiaInutil(Real);
  while Util <> 0 do
  begin
    Real := Real+1;
    Util := DiaInutil(Real);
  end;
  Result := Real;
end;

function TUModule.CalculaDias_Normal(Data, Vcto: TDateTime; DMai, Comp:Integer): Integer;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  //for i := 0 to HIGH(Result) do
    //Result(*[i]*) := 0;
  Przo := Trunc(Vcto - Data);
  //for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  if (Comp>0) then
  begin
    while Util <> 0 do
    begin
      //Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      //Mudou := True;
    end;
    //if Mudou then Przo := Przo - 1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
    end;
  end;
  //
  while Util <> 0 do
  begin
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result := Przo+DMai;
end;

function TUModule.CalculaDias_NaoPula(Data, Vcto: TDateTime; DMai,
  Comp:Integer): Integer;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  Przo, Util: integer;
  //Mudou: Boolean;
begin
  //Result := 0;
  Przo := Trunc(Vcto - Data) + Comp + Dmai;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  while Util <> 0 do
  begin
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result := Przo;
end;

function TUModule.CalculaDias(Data, Vcto: TDateTime; DMai,
  Comp, Pula, CBE: Integer): Integer;
begin
  case Pula of
    0: Result := CalculaDias_Normal(Data, Vcto, DMai, Comp);
    1: Result := CalculaDias_NaoPula(Data, Vcto, DMai, Comp);
    2: Result := CalculaDias_Pula(Data, Vcto, DMai, Comp);
    else Result := 0;
  end;
  if Result < CBE then Result := CBE;
end;

function TUModule.CalculaDiasArray(Data, Vcto: TDateTime; DMai,
  Comp, Pula:Integer): MyArrayI1k;
begin
  if Pula = 0 then Result := CalculaDiasArray_Normal(Data, Vcto, DMai, Comp);
  if Pula = 1 then Result := CalculaDiasArray_NaoPula(Data, Vcto, DMai, Comp);
  if Pula = 2 then Result := CalculaDiasArray_Pula(Data, Vcto, DMai, Comp);
end;

procedure TUModule.ProximoDiaUtil(const Data: TDateTime; const Dom, Seg, Ter,
  Qua, Qui, Sex, Sab, Feriado: Word;
  var Res: TDateTime; var DiaSemTxt: String);
var
  DiaSemana, IdS: Word;
  Util: Boolean;
  Qry: TmySQLQuery;
  Dif: Boolean;
  Data_Txt, DiaAno_Txt: String;
begin
  Res := Data;
  DiaSemTxt := '???';
  Qry := TmySQLQuery.Create(Dmod);
  try
    DiaSemana := DayOfWeek(Data);
    case DiaSemana of
      1: Res := Res + Dom;
      2: Res := Res + Seg;
      3: Res := Res + Ter;
      4: Res := Res + Qua;
      5: Res := Res + Qui;
      6: Res := Res + Sex;
      7: Res := Res + Sab;
    end;
    case DiaSemana of
      1: DiaSemTxt := 'Dom';
      2: DiaSemTxt := 'Seg';
      3: DiaSemTxt := 'Ter';
      4: DiaSemTxt := 'Qua';
      5: DiaSemTxt := 'Qui';
      6: DiaSemTxt := 'Sex';
      7: DiaSemTxt := 'Sab';
    end;
    if Feriado <> 0 then
    begin
      Util := False;
      while Util = False do
      begin
        Data_Txt   := Geral.FDT(Res, 1);
        DiaAno_Txt := Geral.FDT(Res, 12);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT * FROM feriados ',
          'WHERE Data="' + Data_Txt + '" ',
          'OR ',
          '( ',
          'Anual = 1 ',
          'AND DATE_FORMAT(Data, "%d/%m") LIKE "' + DiaAno_Txt + '" ',
          ') ',
          '']);
        //
        Util := Qry.RecordCount = 0;
        //
        if not Util then
        begin
          DiaSemTxt := 'FER';
          Res := Res + Feriado;
          //
          Dif := True;
          while Dif do
          begin
            DiaSemana := DayOfWeek(Res);
            case DiaSemana of
              1: Dif := Dom <> 0;
              2: Dif := Seg <> 0;
              3: Dif := Ter <> 0;
              4: Dif := Qua <> 0;
              5: Dif := Qui <> 0;
              6: Dif := Sex <> 0;
              7: Dif := Sab <> 0;
            end;
            if Dif then
            begin
              if Feriado < 0 then
                Res := Res - 1
              else
                Res := Res + 1;
            end;
          end;
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUModule.ProximoRegistro(Query: TmySQLQuery; Campo: String; Atual:
Integer): Integer;
begin
  Query.Next;
  if Query.FieldByName(Campo).AsInteger = Atual then Query.Prior;
  Result := Query.FieldByName(Campo).AsInteger;
end;

function TUModule.ReabreQuery(Query: TmySQLQuery; DB: TmySQLDatabase; Valores: array of Variant;
  Aviso: String): Boolean;
var
  I, J: Integer;
begin
  J := High(Valores);
  Result := True;
  for I := Low(Valores) to j do
  begin
    case VarType(Valores[I]) of
      varEmpty    {= $0000;} { vt_empty        0 } : Query.Params[I].AsString   := '';
      varNull     {= $0001;} { vt_null         1 } : Query.Params[I].AsString   := 'NULL';
      varSmallint {= $0002;} { vt_i2           2 } : Query.Params[I].AsInteger  := Valores[I];
      varInteger  {= $0003;} { vt_i4           3 } : Query.Params[I].AsInteger  := Valores[I];
      varSingle   {= $0004;} { vt_r4           4 } : Query.Params[I].AsFloat    := Valores[I];
      varDouble   {= $0005;} { vt_r8           5 } : Query.Params[I].AsFloat    := Valores[I];
      varCurrency {= $0006;} { vt_cy           6 } : Query.Params[I].AsFloat    := Valores[I];
      varDate     {= $0007;} { vt_date         7 } : Query.Params[I].AsDateTime := Valores[I];
      //varOleStr   {= $0008;} { vt_bstr         8 } : Query.Params[I].AsString   := Valores[I];
      //varDispatch {= $0009;} { vt_dispatch     9 } : Result := '"' + Variavel + '"';
      //varError    {= $000A;} { vt_error       10 } : Result := '"' + Variavel + '"';
      //varBoolean  {= $000B;} { vt_bool        11 } : Result := IntToStr(BoolToInt(Variavel));
      //varVariant  {= $000C;} { vt_variant     12 } : Result := '"' + Variavel + '"';
      //varUnknown  {= $000D;} { vt_unknown     13 } : Result := '"' + Variavel + '"';
    //varDecimal  {= $000E;} { vt_decimal     14 } {UNSUPPORTED as of v6.x code base}
    //varUndef0F  {= $000F;} { undefined      15 } {UNSUPPORTED per Microsoft}
      varShortInt {= $0010;} { vt_i1          16 } : Query.Params[I].AsInteger  := Valores[I];
      varByte     {= $0011;} { vt_ui1         17 } : Query.Params[I].AsInteger  := Valores[I];
      varWord     {= $0012;} { vt_ui2         18 } : Query.Params[I].AsInteger  := Valores[I];
      varLongWord {= $0013;} { vt_ui4         19 } : Query.Params[I].AsInteger  := Valores[I];
      varInt64    {= $0014;} { vt_i8          20 } : Query.Params[I].AsInteger  := Valores[I];
    //varWord64   {= $0015;} { vt_ui8         21 } {UNSUPPORTED as of v6.x code base}
    {  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap }

      //varStrArg   {= $0048;} { vt_clsid       72 } : Result := '"' + Variavel + '"';
      varString   {= $0100;} { Pascal string 256 } {not OLE compatible } : Query.Params[I].AsString := Valores[I];
      //varAny      {= $0101;} { Corba any     257 } {not OLE compatible } : Result := '"' + Variavel + '"';
{$IFDEF DELPHI12_UP} // nao tenho certeza em qual delphi comecou, mas acho que eh no 2009
      varUString   {= $0102;} { Pascal string 258 } {not OLE compatible } : Query.Params[I].AsString := Valores[I];
{$ENDIF}
      // custom types range from $110 (272) to $7FF (2047)
      else begin
        //raise EAbort.Create('Vari�vel n�o definida!');
        Geral.MB_Aviso('Vari�vel n�o definida!' + sLineBreak + Aviso);
        Result := False;
      end;
    end;
  end;
  //
  if Result then
    Result := AbreQuery(Query, DB, Aviso);
end;

function TUModule.RegistrosNaTabela(Tabela, ComplSQL: String;
  Database: TmySQLDatabase): Integer;
const
  Campo = 'Registros';
var
  Qry: TmySQLQuery;
begin
  //Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.DataBase := Database;
    //
    Qry.SQL.Add('SELECT COUNT(*) ' + Campo);
    Qry.SQL.Add('FROM ' + LowerCase(Tabela));
    Qry.SQL.Add(ComplSQL);
    UnDmkDAC_PF.AbreQuery(Qry, Database);
    //
    Result := Qry.FieldByName(Campo).AsInteger;
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TUModule.RollBack(Qry: TmySQLQuery);
begin
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('ROLLBACK;');
  Qry.ExecSQL;
end;

function TUModule.ExcluiTodosRegistros(Pergunta, Tabela: String): Integer;
begin
  Result := ID_NO;
  if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MB_Pergunta(Pergunta);
  if Result = ID_YES then
  begin
    if Pergunta = '' then
      Result := ID_YES
    else
      Result := Geral.MB_Pergunta('TODOS REGISTROS da tabela "' + Tabela +
      '" ser�o exclu�dos! Confirma a exclus�o?');
    if Result = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(DELETE_FROM + lowercase(tabela));
      Dmod.QrUpd.ExecSQL;
    end;
  end;
end;

function TUModule.ExcluiRegistroBig1(Pergunta, Tabela, Campo: String;
  BigInt1: LongInt; DB: TmySQLDatabase): Integer;
begin
    {$IfNDef NAO_USA_DB_GERAL}
  Result := USQLDB.ExcluiRegistroBig1(Pergunta, Tabela, Campo, BigInt1, DB);
    {$EndIf}
end;

function TUModule.ExcluiRegistroInt1(Pergunta, Tabela, Campo: String; Inteiro1:
Integer; DB: TmySQLDatabase): Integer;
begin
  Result := USQLDB.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro1, DB);
end;

function TUModule.ExcluiRegistrosDeTodaTabela(Qry: TmySQLQuery; Pergunta,
Tabela: String): Integer;
begin
  Result := ID_NO;
  if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MB_Pergunta(Pergunta);
  if Result = ID_YES then
  begin
    Qry.SQL.Clear;
    Qry.SQL.Add(DELETE_FROM + lowercase(tabela));
    Qry.ExecSQL;
  end;
end;

function TUModule.ExcluiRegistro_EnviaArquivoMorto(TabMorto, Tab: String;
  Motivo: Integer; SQLIndex: array of String; ValIndex: array of Variant;
  DataBase: TmySQLDatabase; MostraMsg: Boolean = True): Boolean;
var
  i, j: Integer;
  Liga, Valor, CamposTabz, CamposWhere, Dta, SQL: String;
  Qry: TmySQLQuery;
  QtdFlds: Integer;
begin
  Result := False;
  //
  i := High(SQLIndex);
  j := High(ValIndex);
  //
  if i <> j then
  begin
    if MostraMsg then
      Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i + 1) + ' �ndices e ' +
        Geral.FF0(j + 1) + ' valores para estes �ndices em "ExcluiRegistro_EnviaArquivoMorto"!' +
        sLineBreak + 'Tabela: "' + Tab + '"');
    Exit;
  end;
  //
  if TabMorto = '' then
  begin
    if MostraMsg then
      Geral.MB_Erro('Tabela destino n�o informada!');
    Exit;
  end;
  //
  if Tab = '' then
  begin
    if MostraMsg then
      Geral.MB_Erro('Tabela origem n�o informada!');
    Exit;
  end;
  //
  Qry := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    Dta := Geral.FDT(DModG.ObtemAgora(), 105);
    //
    CamposTabz := UMyMod.ObtemCamposDeTabelaIdentica(DataBase, TabMorto, '', QtdFlds);
    CamposTabz := Geral.Substitui(CamposTabz, ', DataDel', ', "' + Dta +
                    '" DataDel');
    CamposTabz := Geral.Substitui(CamposTabz, ', UserDel', ', ' +
                    Geral.FF0(VAR_USUARIO) + ' UserDel');
    CamposTabz := Geral.Substitui(CamposTabz, ', MotvDel', ', ' +
                    Geral.FF0(Motivo) + ' MotvDel');
    //
    CamposTabz := 'INSERT INTO ' + TabMorto + ' SELECT ' + sLineBreak +
                    CamposTabz + sLineBreak + 'FROM ' + Tab + sLineBreak;
    //
    CamposWhere := '';
    //
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      if i = 0 then
        Liga := 'WHERE '
      else
        Liga := 'AND ';
      //
      if SQLIndex[i] = CO_JOKE_SQL then
      begin
        CamposWhere := CamposWhere + Liga + ValIndex[i] + sLineBreak;
      end else
      begin
        Valor := Geral.VariavelToString(ValIndex[i]);
        //
        CamposWhere := CamposWhere + Liga + SQLIndex[i] + '=' + Valor + sLineBreak;
      end;
    end;
    SQL := CamposTabz + CamposWhere;
    //
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [SQL]);
    //
    if (Result = False) and (MostraMsg = True) then
      Geral.MB_Erro('Falha ao mover registro para arquivo morto!' + sLineBreak +
        'Tabela origem: ' + Tab + sLineBreak + 'Tabela destino: ' + TabMorto);
  finally
    Qry.Free;
  end;
end;

function TUModule.ExcluiRegistroInt2(Pergunta, Tabela, Campo1, Campo2: String;
  Inteiro1, Inteiro2: Integer; DataBase: TmySQLDatabase): Integer;
var
  DB: TmySQLDataBase;
begin
  Result := ID_NO;
  if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MB_Pergunta(Pergunta);
  if Result = ID_YES then
  begin
    if DataBase <> nil then
      DB := DataBase
    else
      DB := Dmod.MyDB;
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + lowercase(tabela)+
    ' WHERE '+Campo1+'='+IntToStr(Inteiro1) +
    ' AND '+Campo2+'='+IntToStr(Inteiro2));
    Dmod.QrUpd.ExecSQL;
*)
    DB.Execute(DELETE_FROM + lowercase(tabela)+
    ' WHERE '+Campo1+'='+IntToStr(Inteiro1) +
    ' AND '+Campo2+'='+IntToStr(Inteiro2));
  end;
end;

function TUModule.ExcluiRegistroIntArr(Pergunta, Tabela: String;
  Campos: array of String; Inteiros: array of Integer): Integer;
var
  Linhas: String;
  I: Integer;
begin
  Linhas := '';
  Result := ID_NO;
  if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MB_Pergunta(Pergunta);
  if Result = ID_YES then
  begin
    for I := 1 to High(Campos) do
      Linhas := Linhas + 'AND ' + Campos[I] + '=' +
      FormatFloat('0', Inteiros[I]) + sLineBreak;
    if not ExecutaMySQLQuery1(Dmod.QrUpd, [
      DELETE_FROM + LowerCase(Tabela),
      'WHERE ' + Campos[0] + '=' + IntToStr(Inteiros[0]),
      Linhas]) then
    Result := ID_NO;
  end;
end;

function TUModule.ExcluiRegistroTxt1(Pergunta, Tabela, Campo: String;
Texto1, ComplementoSQL: String; DB: TMySQLDataBase): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry:= TmySQLQuery.Create(Dmod);
  try
    Qry.Database := DB;
    Result := ID_NO;
    if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
      Exit;
    //
    if Pergunta = '' then
      Result := ID_YES
    else
      Result := Geral.MB_Pergunta(Pergunta);
    if Result = ID_YES then
    begin
      Qry.SQL.Clear;
      Qry.SQL.Add(DELETE_FROM + lowercase(tabela)+
      ' WHERE '+Campo+'="'+Texto1 + '" ' + ComplementoSQL);
      Qry.ExecSQL;
    end;
  finally
    if Qry <> nil then
      Qry.Free;
  end;
end;

function TUModule.ObtemCamposDeTabelaIdentica(const DataBase: TmySQLDatabase;
  const Tabela, Prefix: String; var QtdFlds: Integer): String;// Prefix -> 'la.' = ref tabela
var
  J: Integer;
begin
  QtdFlds := 0;
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT * FROM '+lowercase(tabela));
  QvSelY.SQL.Add('LIMIT 1');
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  QtdFlds := QvSelY.Fields.Count;
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
  //////////////////////////////////////////////////////////////////////////////
  // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
  Result := ' ' + Prefix + QvSelY.Fields[0].FieldName + ' ' + sLineBreak;
  for J := 1 to QtdFlds - 1 do
    Result := Result + ', ' + Prefix+QvSelY.Fields[J].FieldName + ' ' + sLineBreak;
  //////////////////////////////////////////////////////////////////////////////
  // N�o mexer aqui!! � usado para substituir campo por VALOR + Campo!!!!
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //
end;

{ a inclus�o de dados n�o funciona!
function TUModule.ObtemCamposDeTabelaParecida(DataBase: TmySQLDatabase;
  Origem, Destino, Prefix: String): String;// Prefix -> 'la.' = ref tabela
var
  j: Integer;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT * FROM '+lowercase(Origem));
  QvSelY.SQL.Add('LIMIT 1');
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  //
  Result := ' '+Prefix+QvSelY.Fields[0].FieldName + sLineBreak;
  for j := 1 to QvSelY.Fields.Count-1 do
  begin
    QvSelX.Close;
    QvSelX.DataBase := Database;
    QvSelX.SQL.Clear;
    QvSelX.SQL.Add('DESCRIBE ' + Destino + ' ' + QvSelY.Fields[j].FieldName);
    UnDmkDAC_PF.AbreQuery(QvSelX, Database);
    if QvSelX.RecordCount = 1 then
      Result := Result + ', ' + Prefix + QvSelY.Fields[j].FieldName + sLineBreak
    else
      Result := Result + '/*, ' + Prefix + QvSelY.Fields[j].FieldName + '*/ + 'sLineBreak;
  end;
end;
}

function TUModule.ArrayDeTabelaIdentica_Campos(DataBase: TmySQLDatabase;
  Tabela: String): TUMyArray_Str;
var
  j: Integer;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT * FROM '+lowercase(tabela));
  QvSelY.SQL.Add('LIMIT 1');
  UnDmkDAC_PF.AbreQuery(QvSelY, Database);
  //
  SetLength(Result, QvSelY.Fields.Count);
  //

  for j := 0 to QvSelY.Fields.Count-1 do
    Result[j] := QvSelY.Fields[j].FieldName;
end;

function TUModule.ArrayDeTabelaIdentica_Values(Tabela: TmySQLQuery): TUMyArray_Var;
var
  j: Integer;
begin
  //
  SetLength(Result, Tabela.Fields.Count);
  //
  for j := 0 to Tabela.Fields.Count-1 do
    Result[j] := Tabela.Fields[j].Value;
end;

function TUModule.ArrayDeTabelaIdentica_Values2(
  Tabela: TmySQLQuery): TUMyArray_Var;
var
  j: Integer;
begin
  //
  SetLength(Result, Tabela.Fields.Count);
  //
  for j := 0 to Tabela.Fields.Count-1 do
  begin
    if Tabela.Fields[j].Value = null then
      Result[j] := EmptyStr
    else
      Result[j] := Tabela.Fields[j].Value;
  end;
end;

function TUModule.ObtemValorDoCampoXDeIndex_Int(Index: Integer;
CampoX, CampoIndex: String; Query: TmySQLQuery;
dmkEditCB: TdmkEditCB; dmkDBlookupComboBox: TdmkDBlookupComboBox;
var Valor: Integer): Boolean;
begin
  Result := False;
  if Query.Locate(CampoIndex, Index, []) then
  begin
    Valor := Query.FieldByName(CampoX).AsInteger;
    if dmkEditCB <> nil then
      dmkEditCB.ValueVariant := Valor;
    if dmkDBlookupComboBox <> nil then
      dmkDBlookupComboBox.KeyValue := Valor;
    Result := True;
  end else
    Valor := 0;
end;

function TUModule.ObtemCodigoDeCodUsu(EdCodUsu: TdmkEditCB;
  var Codigo: Integer; Aviso: String; Campo: String = 'Codigo'; CodUsu: String =
  'CodUsu'): Boolean;
var
  Query: TmySQLQuery;
  DB: TDBLookupComboBox;
  DS: TDataSource;
  CU: Integer;
begin
  Codigo := 0;
  Result := False;
  if EdCodUsu.ValueVariant <> Null then
  begin
    if EdCodUsu.DBLookupComboBox <> nil then
    begin
      DB := EdCodUsu.DBLookupComboBox;
      if DB <> nil then
        DS := DB.ListSource
      else
        Ds := nil;
      if DS <> nil then
      begin
        Query := TMySQLQuery(DS.DataSet);
        if Query <> nil then
        begin
          CU := EdCodUsu.ValueVariant;
          if Query.Locate(CodUsu, CU, []) then
          begin
            Codigo := Query.FieldByName(Campo).AsInteger;
            Result := True;
          end;
        end;
      end;
    end;
  end;
  if not Result and (Aviso <> '') then
    Geral.MB_Aviso(Aviso);
end;

function TUModule.ObtemIndicePrimario_Int(const NovoCodigo: TNovoCodigo;
  Qry: TmySQLQuery; const Tabela, Campo: String; var Idx: Integer): Boolean;
begin
  case NovoCodigo of
    ncControle: Idx := UMyMod.BuscaNovoCodigo_Int(
      Qry, Tabela, Campo, [], [], stIns, 0, siPositivo, nil);
    ncGerlSeq1: Idx := UMyMod.BuscaProximoGerlSeq1Int32(
      Tabela, Campo, '', '', tsDef, stIns, 0, Null);
    else (*ncIdefinido: CtrlGeral: ;*)
    begin
      Result := False;
      Geral.MB_Erro('Tipo de obten��o de novo c�digo indefinido!');
      Exit;
    end;
  end;
  Result := True;
end;

function TUModule.ObtemNomeMunici(CodMunici: Integer;
  Query: TmySQLQuery): String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.AllID_DB, [
  'SELECT Nome ',
  'FROM dtb_munici ',
  'WHERE Codigo=' + Geral.FF0(CodMunici),
  '']);
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('Nome').AsString
  else
    Result := '';
end;

function TUModule.ObtemProximoCHTalao(Carteira: Integer; EdSerieCH,
  EdDoc: TdmkEdit): Boolean;
var
  Qry: TmySQLQuery;
  //NumFim,
  Controle, NumIni,NumAtu, Taloes: Integer;
  Serie: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  Qry.DataBase := DMod.MyDB;
  //
  Qry.SQL.Add('LOCK TABLES carttalch WRITE;');
  Qry.ExecSQL;
  Qry.SQL.Add('SELECT *');
  Qry.SQL.Add('FROM carttalch');
  Qry.SQL.Add('WHERE Codigo=' + FormatFloat('0', Carteira));
  Qry.SQL.Add('AND NumAtu < NumFim');
  Qry.SQL.Add('ORDER BY Controle');
  UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  Taloes := Qry.RecordCount;
  if Qry.RecordCount > 0 then
  begin
    Controle := Qry.FieldByName('Controle').AsInteger;
    NumIni   := Qry.FieldByName('NumIni').AsInteger;
    //NumFim   := Qry.FieldByName('NumFim').AsInteger;
    NumAtu   := Qry.FieldByName('NumAtu').AsInteger;
    Serie    := Qry.FieldByName('Serie').AsString;
    if NumAtu = 0 then
      NumAtu := NumIni
    else
      NumAtu := NumAtu + 1;
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('UPDATE carttalch SET NumAtu=' + FormatFloat('0', NumAtu));
    Qry.SQL.Add('WHERE controle=' + FormatFloat('0', Controle));
    Qry.ExecSQL;
    //
    EdSerieCH.ValueVariant := Serie;
    EdDoc.ValueVariant     := NumAtu;
    //
    Result := True;
  end else Geral.MB_Aviso('N�o h� mais numera��o de cheque dispon�vel!');
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('UNLOCK TABLES;');
  Qry.ExecSQL;
  Qry.Free;
  //
  if Taloes < 2 then Geral.MB_Aviso('� necess�rio solicitar mais tal�es ' +
  'de cheque para esta carteira!');
end;

function TUModule.ObtemQtdeCHTaloes(Carteira: Integer): Integer;
var
  Qry: TmySQLQuery;
  //Controle,
  NumIni, NumFim, NumAtu: Integer;
  //Serie: String;
  //Cheques: Integer;
begin
  Result  := 0;
  Qry := TmySQLQuery.Create(Dmod);
  Qry.DataBase := DMod.MyDB;
  //
  Qry.SQL.Add('SELECT *');
  Qry.SQL.Add('FROM carttalch');
  Qry.SQL.Add('WHERE Codigo=' + FormatFloat('0', Carteira));
  Qry.SQL.Add('AND NumAtu < NumFim');
  Qry.SQL.Add('ORDER BY Controle');
  UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
  if Qry.RecordCount > 0 then
  begin
    Qry.First;
    while not Qry.Eof do
    begin
      NumIni   := Qry.FieldByName('NumIni').AsInteger;
      NumFim   := Qry.FieldByName('NumFim').AsInteger;
      NumAtu   := Qry.FieldByName('NumAtu').AsInteger;
      if NumAtu = 0 then
        Result := Result + NumFim - NumIni + 1
      else
        Result := Result + NumFim - NumAtu;
      //
      Qry.Next;  
    end;
  end;
  Qry.Free;
end;

function TUModule.SenhaEspecial: Boolean;
begin
  MyObjects.FormCria(TFmSenhaEspecial, FmSenhaEspecial);
  FmSenhaEspecial.ShowModal;
  Result := FmSenhaEspecial.FStatusAcesso;
  FmSenhaEspecial.Destroy;
  //
end;

  // GetCodUsuDeCodigo
  // ObtemCodUsuDeCodigo
  // � "Seta" e n�o "Obtem" porque seta o Edit e o DBLookupComboBox
function TUModule.SetaCodUsuDeCodigo(EdCodUsu: TdmkEditCB;
  CBCodUsu: TdmkDBLookupComboBox; Qry: TmySQLQuery; Codigo: Integer;
  Campo, CodUsu: String): Boolean;
var
  Loc: Integer;
begin
  Result := False;
  Qry.Close;
  UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  if Codigo <> 0 then
  begin
    if Qry.Locate(Campo, Codigo, []) then
    begin
      Loc := Qry.FieldByName(CodUsu).AsInteger;
      if EdCodUsu <> nil then
        EdCodUsu.ValueVariant := Loc;
      if CBCodUsu <> nil then
        CBCodUsu.KeyValue := Loc;
      //
      Result := True;
    end else
    begin
      if EdCodUsu <> nil then
        EdCodUsu.ValueVariant := 0;
      if CBCodUsu <> nil then
        CBCodUsu.KeyValue := Null;
    end;
  end;
end;

// SetaCodigoSelecionado
function TUModule.SetaCodigoPesquisado(EdCodigo: TdmkEditCB; CBCodigo:
  TdmkDBLookupComboBox; Qry: TmySQLQuery; Codigo: Integer; Campo: String): Boolean;
begin
  Result := False;
  if Qry <> nil then
  begin
    Qry.Close;
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
    if Codigo <> 0 then
    begin
      if Qry.Locate(Campo, Codigo, []) then
      begin
        if EdCodigo <> nil then
          EdCodigo.ValueVariant := Qry.FieldByName(Campo).AsInteger;
        if CBCodigo <> nil then
          CBCodigo.KeyValue := Qry.FieldByName(Campo).AsInteger;
        //
        Result := True;
      end;
    end;
  end;
end;

function TUModule.SetaCodTxtPesquisado(EdCodTxt: TdmkEditCB;
  CBCodTxt: TdmkDBLookupComboBox; Qry: TmySQLQuery; CodTxt,
  Campo: String): Boolean;
begin
  Result := False;
  Qry.Close;
  UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  if CodTxt <> '' then
  begin
    if Qry.Locate(Campo, CodTxt, []) then
    begin
      if EdCodTxt <> nil then
        EdCodTxt.ValueVariant := Qry.FieldByName(Campo).AsInteger;
      if CBCodTxt <> nil then
        CBCodTxt.KeyValue := Qry.FieldByName(Campo).AsInteger;
      //
      Result := True;
    end;
  end;
end;

procedure TUModule.Commit(Qry: TmySQLQuery);
begin
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('COMMIT;');
  Qry.ExecSQL;
end;

function TUModule.ConfigJanela(Janela: String; Lista: TConfWinControl): Boolean;
begin
  MyObjects.FormCria(TFmConfJanela, FmConfJanela);
  with FmConfJanela do
  begin
    FJanela := Janela;
    FListaCWC := Lista;
    ReopenConfJanela('');
    ShowModal;
    Destroy;
  end;
  Result := True;
end;

function TUModule.ConfigJanela10(Janela: String;
  c0, c1, c2, c3, c4, c5, c6, c7, c8, c9:TControl): Boolean;
var
  Lista: TConfWinControl;
  i: Integer;
begin
  Lista[00] := c0;
  Lista[01] := c1;
  Lista[02] := c2;
  Lista[03] := c3;
  Lista[04] := c4;
  Lista[05] := c5;
  Lista[06] := c6;
  Lista[07] := c7;
  Lista[08] := c8;
  Lista[09] := c9;
  for i := 10 to TMaxConfWinControl do Lista[i] := nil;
  Result := ConfigJanela(Janela, Lista);
end;

function TUModule.ConfigJanela20(Janela: String;
  c0, c1, c2, c3, c4, c5, c6, c7, c8, c9,
  c10, c11, c12, c13, c14, c15, c16, c17, c18, c19:TControl): Boolean;
var
  Lista: TConfWinControl;
  i: Integer;
begin
  Lista[00] := c0;
  Lista[01] := c1;
  Lista[02] := c2;
  Lista[03] := c3;
  Lista[04] := c4;
  Lista[05] := c5;
  Lista[06] := c6;
  Lista[07] := c7;
  Lista[08] := c8;
  Lista[09] := c9;
  Lista[10] := c10;
  Lista[11] := c11;
  Lista[12] := c12;
  Lista[13] := c13;
  Lista[14] := c14;
  Lista[15] := c15;
  Lista[16] := c16;
  Lista[17] := c17;
  Lista[18] := c18;
  Lista[19] := c19;
  for i := 20 to TMaxConfWinControl do Lista[i] := nil;
  Result := ConfigJanela(Janela, Lista);
end;

function TUModule.TextMySQLQuery1(Query: TMySQLQuery;
  SQL: array of String): Boolean;
var
  I: Integer;
begin
  //Result := False;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;
    // n�o abre, apenas executa
    //Query. O p e n;
    Result := True;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MB_Erro('Erro ao tentar definir o texto de uma SQL no MySQLQuery!'
    + sLineBreak + 'Avise a DERMATEK!');
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
    raise;
  end;
end;

function TUModule.TravaFmEmPanelInsUpd(PanelsToHide, PanelsToShow: array of TPanel;
  ImgTipo: TdmkImage): Boolean;
var
  i, j, k: Integer;
begin
  i := Low(PanelsToHide);
  j := High(PanelsToHide);
  for k := i to j do PanelsToHide[i].Visible := False;
  //
  i := Low(PanelsToShow);
  j := High(PanelsToShow);
  for k := i to j do PanelsToShow[i].Visible := True;
  //
  if ImgTipo <> nil then
    ImgTipo.SQLType := stLok;
  Result := True;
end;

function TUModule.ConfigPanelInsUpd(Acao: TSQLType; Form: TForm; Panel: TWinControl;
Query: TmySQLQuery; WinCtrlsToHide, WinCtrlsToShow: array of TWinControl;
CompoToFocus: TWinControl; CompoTipo: TControl; Tabela: String): Boolean;
  function ValorPadrao(Qry: TmySQLQuery; Campo: String): Variant;
  var
    Tipo, DefVal: String;
    e, k: Integer;
    d: Double;
  begin
    Result := Null;
    if Qry.Locate('Field', Campo, []) then
    begin
      //try
        if VarType(Qry.FieldByName('Default').AsVariant) = varNull then
          DefVal := ''
        else
          DefVal := Qry.FieldByName('Default').AsVariant;
      //except
        //DefVal := Trim(Qry.FieldByName('Default').AsWideString);
      //end;
      if String(DefVal) <> '' then
      begin
        Tipo := Uppercase(Qry.FieldByName('Type').AsString);
        k := pos('(', Tipo);
        if k > 0 then
          Tipo := Copy(Tipo, 1, k-1);
        if (Tipo = 'TINYINT') or (Tipo = 'SMALLINT') or (Tipo = 'MEDIUMINT')
        or (Tipo = 'INT') or (Tipo = 'INTEGER') or (Tipo = 'BIGINT') then
          Result := Qry.FieldByName('Default').AsVariant
        else
        if (Tipo = 'FLOAT') or (Tipo = 'DOUBLE') or (Tipo = 'DOUBLEPRECISION')
        or (Tipo = 'REAL') or (Tipo = 'DECIMAL') or (Tipo = 'NUMERIC') then
        begin
          Val(Qry.FieldByName('Default').AsVariant, d, e);
          Result := d;
        end else
        if (Tipo = 'CHAR') or (Tipo = 'VARCHAR') or (Tipo = 'TINYBLOB')
        or (Tipo = 'BLOB') or (Tipo = 'MEDIUMBLOB') or (Tipo = 'LONGBLOB')
        or (Tipo = 'TINYTEXT') or (Tipo = 'TEXT') or (Tipo = 'MEDIUMTEXT')
        or (Tipo = 'LONGTEXT') or (Tipo = 'ENUM') or (Tipo = 'SET')
        then
          Result := Qry.FieldByName('Default').AsVariant
        else
        if (Tipo = 'DATE') or (Tipo = 'DATETIME') or (Tipo = 'TIMESTAMP')
        or (Tipo = 'TIME') or (Tipo = 'YEAR') then
          // Precisa modificar??
          Result := Qry.FieldByName('Default').AsString
        else
          Geral.MB_Aviso('O campo do tipo "' + Tipo +
          '" ainda n�o implementado na function "ValorPadrao". ' +
          '   AVISE A DERMATEK!   Isto n�o � uma notofica��o de erro!');
      end;
    end;
  end;
  procedure Mensagem(Campo: String; Objeto: TObject);
  begin
    Geral.MB_Erro('N�o foi poss�vel definir o valor ' +
    'do campo "' + Campo + '" no componente "' +
    TComponent(Objeto).Name + '"');
  end;
var
  i, j, k: Integer;
  c, Objeto: TComponent;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PropInfo: PPropInfo;
  Campo, s: String;
  Valor: Variant;
  IsOk: Boolean;
  Qry: TmySQLQuery;
  MySou: TDataSource;
  MySet: TDataSet;
  //MyQry: TmySQLQuery;
  MyFld: String;
  Data: TDateTime;
begin
  //IncluiRegistro;
  Screen.Cursor := crHourGlass;
try
try
  if Acao = stIns then
  begin
    Qry:= TmySQLQuery.Create(Dmod);
    Qry.Database := Query.Database;
    if Qry.Database = nil then
      Qry.Database := Dmod.MyDB;
    Qry.SQL.Add('SHOW FIELDS FROM ' + lowercase(Tabela));
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
  end else Qry := nil;
  for i := 0 to Form.ComponentCount - 1 do
  begin
    IsOk := False;
    Objeto := Form.Components[i];
    if  (Objeto.Name = 'SbNovo')
    or  (Objeto.Name = 'SbNumero')
    or  (Objeto.Name = 'SbNome')
    or  (Objeto.Name = 'SbQuery')
    then begin
      if (Objeto is TBitBtn) then
        TBitBtn(Objeto).Enabled := False
      else
      if (Objeto is TSpeedButton) then
        TSpeedButton(Objeto).Enabled := False;
    end
    else if (Objeto is TdmkValUsu) then
    begin
      if TdmkValUsu(Objeto).Panel <> nil then
      begin
        if TdmkValUsu(Objeto).Panel.Name = Panel.Name then
          IsOK := True
        else
          IsOK := False;
      end else Geral.MB_Erro('TdmkValUsu sem panel cadastrado!');
    end else begin
      IsOK := False;
      c := TComponent(Form.Components[i]).GetParentComponent;
      while c <> nil do
      begin
        if TComponent(c).Name = Panel.Name then
        begin
          IsOk := True;
          Break;
        end
        else c := TComponent(c).GetParentComponent;
      end;
    end;
    if IsOk then
    begin
      Objeto := Form.Components[i];
      //
      if Acao = stUpd then
      begin
        PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
        PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
        PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
        PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
        if PI_OldValor <> nil then
        begin
          if (PI_DataField <> nil) and (PI_DataSource <> nil) then
          begin
            //
            //PropVal := GetPropValue(Objeto, 'DataSource');
            //MySet   := TDataSource(PropVal);
            //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
            MySou := TDataSource(TDBEdit(Objeto).DataSource);
            MySet := TDataSource(MySou).DataSet;
            //MyQry := TMySQLQuery(TDataSource(MySet));
            MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
            SetPropValue(Objeto, 'OldValor',
              TDataSet(MySet).FieldByName(MyFld).AsVariant);

          end else if PI_QryCampo <> nil then
          begin
            Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
            if Campo <> '' then
            begin
              PropInfo := GetPropInfo(Objeto, 'OldValor');
              if PropInfo <> nil then
              try
                if (Objeto is TdmkRadioGroup)
                and (TmySQLQuery(Query).FieldByName(Campo).AsVariant = Null) then
                // nada
                else
                // Erro Aqui Berlin!
                SetPropValue(Objeto, 'OldValor',
                  TmySQLQuery(Query).FieldByName(Campo).AsVariant);
              except
                if TmySQLQuery(Query).FieldByName(Campo).AsVariant <> Null then
                  Geral.MB_Erro('N�o foi poss�vel definir o "OldValor" = "'
                  + Geral.VariantTostring(
                  TmySQLQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                  Campo + '" no objeto "' + TComponent(Objeto).Name + '"');
              end;
            end;
          end;
        end;
      end;
      //fim OldValor
      //
      PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
      if PropInfo <> nil then
      begin
        Campo := GetStrProp(TComponent(Objeto), PropInfo);
        if Campo <> '' then
        begin
          if Acao = stIns then
            Valor := ValorPadrao(Qry, Campo)
          else
            try
              Valor := TmySQLQuery(Query).FieldByName(Campo).AsVariant;
            except
              Geral.MB_Erro('N�o foi poss�vel definir o valor "' +
              Geral.VariavelToString(Valor) + '" para o objeto "' +
              TComponent(Objeto).Name + '"');
              //
            end;
          try
            if (Objeto is TdmkEdit) then
            begin
              if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                Valor := Geral.MesEAnoDoMez(Valor)
              else
              if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                Valor := Geral.AnoEMesDoMez(Valor);
              // 2012-06-01 0000-00-00 00:00:00
              if (
                 (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
              or (VarType(Valor) = varUString)
{$ENDIF}
              )
              and (TdmkEdit(Objeto).FormatType = dmktfTime)
              and (Length(Valor) = 19) then
              begin
                Valor := Copy(Valor, 12);
              end;
              // FIM 2012-06-01
              TdmkEdit(Objeto).ValueVariant := Valor;
            end
            //
            else if (Objeto is TdmkDBLookupCombobox) then
              TdmkDBLookupCombobox(Objeto).KeyValue := Valor
            //
            else if (Objeto is TdmkEditCB) then
              TdmkEditCB(Objeto).ValueVariant := Valor
            //
            else if (Objeto is TdmkEditDateTimePicker) then
            begin
              if Valor <> Null then
              begin
                if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
                or (VarType(Valor) = varUString)
{$ENDIF}
                then
                begin
                  // 2012-06-01 > 0000-00-00 00:00:00
                  if Length(Valor) = 19 then
                    Valor := Copy(Valor, 1, 10);
                  // Fim 2012-06-01
                  Data := Geral.ValidaDataSimples(Valor, False);
                  if (Data < TdmkEditDateTimePicker(Objeto).MinDate)
                  and (Acao = stIns) then
                    TdmkEditDateTimePicker(Objeto).Date := TdmkEditDateTimePicker(Objeto).MinDate
                  else
                    TdmkEditDateTimePicker(Objeto).Date := Data;
                end else
                  TdmkEditDateTimePicker(Objeto).Date := Valor;
              end;
            end
            //
            else if (Objeto is TdmkMemo) then
            begin
              if Valor = Null then Valor := '';
              TdmkMemo(Objeto).Text := Valor;
            end
            //
            else if (Objeto is TdmkRadioGroup) then
            begin
              // -1 complica inclus�o !!
              //if (Valor = Null) or (Valor = '') then Valor := -1;
              if (Valor = Null) then Valor := -1;
              if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
              or (VarType(Valor) = varUString)
{$ENDIF}
              then
              begin
                if (Valor = '') then
                  Valor := -1
              end;
              TdmkRadioGroup(Objeto).ItemIndex := Valor;
            end
            //
            else if (Objeto is TdmkCheckBox) then
            begin
              if Valor = Null then Valor := False;
              if (VarType(Valor) = varString)
{$IFDEF DELPHI12_UP}
              or (VarType(Valor) = varUString)
{$ENDIF}
              then
              begin
                s := Uppercase(Valor);
                if (s = 'V') or (s = 'S') or (s = '1') then
                  TdmkCheckBox(Objeto).Checked := True
                else
                if (s = 'F') or (s = 'N') or (s = '') or (s = '0') then
                  TdmkCheckBox(Objeto).Checked := False
                else
                  Geral.MB_Erro('Valor indefinido para "TdmkCheckBox"!');
              end else
                TdmkCheckBox(Objeto).Checked := Geral.IntToBool(Valor);
            end
            //
            else if (Objeto is TdmkCheckGroup) then
            begin
              if Valor = Null then Valor := False;
              TdmkCheckGroup(Objeto).Value := Valor;
            end
            //
            else if (Objeto is TdmkPopOutFntCBox) then
            begin
              if TdmkPopOutFntCBox(Objeto).Items.Count = 0 then
                TdmkPopOutFntCBox(Objeto).Items.Assign(Screen.Fonts);
              TdmkPopOutFntCBox(Objeto).FonteNome := Valor;
            end
            //
            else if (Objeto is TdmkRichEdit) then
            begin
              if Valor = Null then
                Valor := '';
              MyObjects.DefineTextoRichEdit(TdmkRichEdit(Objeto), Valor)
            end
            //
            else if (Objeto is TdmkValUsu) then
              TdmkValUsu(Objeto).ValueVariant := Valor
            //

            //

            else if (Objeto is TdmkDBEdit) then begin end //nada
            //
            else Mensagem(Campo, Objeto);
          except
            Mensagem(Campo, Objeto);
          end;
        end;
      end;
    end;
  end;
  Panel.Visible := True;
  //
  i := Low(WinCtrlsToHide);
  j := High(WinCtrlsToHide);
  for k := i to j do WinCtrlsToHide[i].Visible := False;
  //
  i := Low(WinCtrlsToShow);
  j := High(WinCtrlsToShow);
  for k := i to j do WinCtrlsToShow[i].Visible := True;
  //
  if CompoToFocus <> nil then
  try
    if Form.Visible then
      TWinControl(CompoToFocus).SetFocus;
  except
    on E: Exception do
    begin
      if VAR_USUARIO = -1 then
        Geral.MB_Erro(E.Message + sLineBreak +
        'Componente: ' + TWinControl(CompoToFocus).Name);
    end;
  end;
  if CompoTipo is TdmkLabel then
    TdmkLabel(CompoTipo).SQLType := Acao
  else
  if CompoTipo is TdmkImage then
    TdmkImage(CompoTipo).SQLType := Acao
  else
    Geral.MB_Erro(
    'Tipo de componente n�o implementado na fun��o "ConfigPanelInsUpd"');

  Result := True;
  if (Acao = stIns) and (Qry <> nil) then
    Qry.Free;
except
  on E: Exception do
  begin
    Geral.MB_Erro('Form: ' + Form.Name + sLineBreak +
    'Panel: ' + Panel.Name + sLineBreak +
    'Componente: ' + TComponent(Objeto).Name + sLineBreak +
    'TUModule.ConfigPanelInsUpd()' + sLineBreak +
    E.Message + sLineBreak +
    'Query: ' + Query.SQL.Text + sLineBreak);
  end;
end;
(*
function TUModule.ConfigPanelInsUpd(Acao: TSQLType; Form: TForm; Panel: TWinControl;
Query: TmySQLQuery; WinCtrlsToHide, WinCtrlsToShow: array of TWinControl;
CompoToFocus: TWinControl; CompoTipo: TControl; Tabela: String): Boolean;
*)
finally
  Screen.Cursor := crDefault;
end;

end;

procedure TUModule.VerificaFeriadosFuturos(ComponentClass: TComponentClass;
  Reference: TComponent; SohSeNaoTem: Boolean);
var
  Query: TmySQLQuery;
  Data_Txt, DiaAno_Txt: String;
  Agora: TDateTime;
begin
  if SohSeNaoTem then
  begin
    Query := TmySQLQuery.Create(Dmod);
    try
      Agora      := DmodG.ObtemAgora();
      Data_Txt   := Geral.FDT(Agora, 1);
      DiaAno_Txt := Geral.FDT(Agora, 22) + Geral.FDT(Agora, 16);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT * ',
        'FROM feriados ',
        'WHERE Data > "' + Data_Txt + '" ',
        'OR ',
        '( ',
        'Anual = 1 ',
        'AND DATE_FORMAT(Data, "%m%d") > "' + DiaAno_Txt + '" ',
        ') ',
        '']);
      //
      if Query.RecordCount = 0 then
      begin
        // Ini 2019-01-26
        UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT * ',
        'FROM feriados ',
        '']);
        //
        if Query.RecordCount > 0 then
        // Fim 2019-01-26
        begin
            if Geral.MB_Pergunta('N�o h� nenhum feriado futuro. ' + sLineBreak +
              'Deseja cadastrar agora?') = ID_YES
            then
              MyObjects.FormShow(ComponentClass, Reference);
        end;
      end;
    finally
      Query.Free;
    end;
  end else
    MyObjects.FormShow(ComponentClass, Reference);
end;

function TUModule.DiasUteis(DataI, DataF: TDateTime): Integer;
var
  Query: TmySQLQuery;
  i, k, ini, fim, Dias: Integer;
  DataI_Txt, DataF_Txt, DiaAnoI_Txt, DiaAnoF_Txt: String;
begin
  Query := TmySQLQuery.Create(Dmod);
  try
    DataI_Txt   := Geral.FDT(DataI, 1);
    DataF_Txt   := Geral.FDT(DataF, 1);
    DiaAnoI_Txt := Geral.FDT(DataI, 12);
    DiaAnoF_Txt := Geral.FDT(DataF, 12);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT *, DATE_FORMAT(Data, "%d/%m") DiaMes ',
      'FROM feriados ',
      'WHERE WEEKDAY(Data) < 5 ',
      'AND Data BETWEEN "' + DataI_Txt + '" AND "' + DataF_Txt + '" ',
      'OR ',
      '( ',
      'Anual = 1 ',
      'AND DATE_FORMAT(Data, "%d/%m") BETWEEN "' + DiaAnoI_Txt + '" AND "' + DiaAnoF_Txt + '" ',
      ') ',
      'GROUP BY DiaMes ',
      '']);
    ini  := Trunc(Int(DataI));
    fim  := Trunc(Int(DataF));
    Dias := fim - ini + 1 - Query.RecordCount;
    //
    if Dias > 0 then
    begin
      Dias := 0;
      //
      for i := ini to fim do
      begin
        k := DayOfWeek(i);
        if (k > 1) and (k < 7) then
          inc(Dias, 1);
      end;
      Dias := Dias - Query.RecordCount;
    end;
    if Dias < 0 then
      Dias := 0;
    Result := Dias;
  finally
    Query.Free;
  end;
end;

function TUModule.DiasUteis2(DataI, DataF: TDateTime; DiasUteis: Integer): Integer;
var
  Query: TmySQLQuery;
  i, k, p, ini, fim, Dias: Integer;
  DataI_Txt, DataF_Txt, DiaAnoI_Txt, DiaAnoF_Txt: String;
  IniDiaSem, FimDiaSem: Integer;
  //Texto: String;
begin
  //Texto := '';
  Query := TmySQLQuery.Create(Dmod);
  try
    DataI_Txt   := Geral.FDT(DataI, 1);
    DataF_Txt   := Geral.FDT(DataF, 1);
    DiaAnoI_Txt := Geral.FDT(DataI, 12);
    DiaAnoF_Txt := Geral.FDT(DataF, 12);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT *, DATE_FORMAT(Data, "%d/%m") DiaMes ',
      'FROM feriados ',
      'WHERE WEEKDAY(Data) < 5 ',
      'AND Data BETWEEN "' + DataI_Txt + '" AND "' + DataF_Txt + '" ',
      'OR ',
      '( ',
      'Anual = 1 ',
      'AND DATE_FORMAT(Data, "%d/%m") BETWEEN "' + DiaAnoI_Txt + '" AND "' + DiaAnoF_Txt + '" ',
      ') ',
      'GROUP BY DiaMes ',
      '']);
    ini  := Trunc(Int(DataI));
    fim  := Trunc(Int(DataF));
    Dias := fim - ini + 1 - Query.RecordCount;
    //
    if Dias > 0 then
    begin
      Dias := 0;
      //
      for i := ini to fim do
      begin
        k := DayOfWeek(i);
        p := Trunc(power(2, (k-1)));
        //if (k > 1) and (k < 7) then
        if dmkPF.IntInConjunto2(p, DiasUteis) then
        begin
          inc(Dias, 1);
{
          Texto := Texto + Geral.FDT(i, 1) + ' [' + IntToStr(k) + '] = �til' + sLineBreak;
        end else
        begin
          Texto := Texto + Geral.FDT(i, 1) + ' [' + IntToStr(k) + '] = IN�TIL' + sLineBreak;
}
        end;
      end;
      Dias := Dias - Query.RecordCount;
    end;
    if Dias < 0 then
      Dias := 0;
    Result := Dias;
  finally
    Query.Free;
  end;
  //Geral.MB_Info(Texto);
end;

{
function TUModule.SQLInsUpd_Old(QrUpd: TmySQLQuery; Tipo, Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  //Result := False;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> CO_INCLUSAO) and (Tipo <> CO_ALTERACAO) ) then
  begin
    Geral.MB_Erro('AVISO: O status do formul�rio est� ' +
    'definido como "' + Tipo + '"'), 'AVISO IMPORTANTE', MB_OK+MB_ICONWARNING);
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = CO_INCLUSAO then
    QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ')
  else
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET AlterWeb=1, ');

  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    //if i < j then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ');
    //else
      //QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //

  if Tipo = CO_INCLUSAO then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
  else
    QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));

  //
  if Auto_increment and (Tipo = CO_INCLUSAO) then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      if Tipo = CO_INCLUSAO then Liga := ', ' else
      begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end;
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  end;
  //
  try
    QrUpd.ExecSQL;
    Result := True;
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
    raise;
  end;
end;
}
function TUModule.SQLInsUpdL(QrUpd: TmySQLQuery; Tipo, Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  //Result := False;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> CO_INCLUSAO) and (Tipo <> CO_ALTERACAO) ) then
  begin
    Geral.MB_Aviso('AVISO: O status do formul�rio est� ' +
    'definido como "' + Tipo + '"');
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = CO_INCLUSAO then
    QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ')
  else
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');

  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j-1 do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    //if i < j then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ');
    //else
      //QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  Valor := Geral.VariavelToString(ValCampos[j]);
  QrUpd.SQL.Add(SQLCampos[j] + '=' + Valor);
  //

  {
  if Tipo = CO_INCLUSAO then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
  else
    QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  }
  //
  if Auto_increment and (Tipo = CO_INCLUSAO) then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      if Tipo = CO_INCLUSAO then Liga := ', ' else
      begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end;
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  end;
  //
  try
    QrUpd.ExecSQL;
    Result := True;
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
    raise;
  end;
end;

function TUModule.SQLDel1(QrExec, QrData: TmySQLQuery; Tabela, Campo: String;
  Valor: Variant; PerguntaSeExclui: Boolean; PerguntaAlternativa: String;
  ImpedeExclusao: Boolean): Boolean;
var
  Txt, Perg: String;
  Prox: Variant;
  Exclui: Boolean;
begin
  Result := False;
  if ImpedeExclusao then
    if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
      Exit;
  //
  Txt := Geral.VariavelToString(Valor);
  if PerguntaSeExclui then
  begin
    if Trim(PerguntaAlternativa) <> '' then Perg := PerguntaAlternativa else Perg :=
    'Confirma a exclus�o do registro selecionado?';
    Exclui := Geral.MB_Pergunta(Perg) = ID_YES;
  end else Exclui := True;
  if Exclui then
  begin
    QrExec.SQL.Clear;
    QrExec.SQL.Add(DELETE_FROM  + lowercase(Tabela));
    QrExec.SQL.Add('WHERE ' + Campo + '="' + String(Valor) +'"');
    try
      QrExec.ExecSQL;
      //
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
    end;
    if QrData <> nil then
    begin
      try
        QrData.Close;
        UnDmkDAC_PF.AbreQuery(QrData, QrData.Database);
        Prox := ProximoRegistro(QrData, Campo, 0);
        QrData.Locate(Campo, Prox, []);
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUModule.SQLDel2(QrExec, QrData: TmySQLQuery; Tabela, CampoLoc: String;
  Campos: array of String; Valores: array of Variant; PerguntaSeExclui: Boolean;
  PerguntaAlternativa: String): Boolean;
var
  Valor, Perg, Liga: String;
  Prox: Variant;
  Exclui: Boolean;
  i, j: Integer;
begin
  Result := False;
  if USQLDB.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  if PerguntaSeExclui then
  begin
    if Trim(PerguntaAlternativa) <> '' then Perg := PerguntaAlternativa else Perg :=
    'Confirma a exclus�o do registro selecionado?';
    Exclui := Geral.MB_Pergunta(Perg) = ID_YES;
  end else Exclui := True;
  if Exclui then
  begin
    Liga := 'WHERE ';
    QrExec.SQL.Clear;
    QrExec.SQL.Add(DELETE_FROM + lowercase(Tabela));
    //
    j := High(Campos);
    for i := Low(Campos) to j do
    begin
      Valor := Geral.VariavelToString(Valores[i]);
      QrExec.SQL.Add(Liga + Campos[i] + '=' + Valor + ' ');
      Liga := 'AND ';
    end;
    //
    try
      QrExec.ExecSQL;
      //
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExec, '', nil, True, True);
    end;
    if QrData <> nil then
    begin
      try
        QrData.Close;
        UnDmkDAC_PF.AbreQuery(QrData, QrData.Database);
        if CampoLoc <> '' then
        begin
          Prox := ProximoRegistro(QrData, CampoLoc, 0);
          QrData.Locate(CampoLoc, Prox, []);
        end;  
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUModule.SQLLoc1(Query: TmySQLQuery; Tabela, Campo: String;
  Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
var
  Txt: String;
begin
  Txt := Geral.VariavelToString(Valor);
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + '=' + Txt);
  UnDmkDAC_PF.AbreQuery(Query, Query.Database);
  //
  Result := Query.RecordCount;
  //
  if (Result = 0) and (Trim(MsgNaoLoc) <> '') then
    Geral.MB_Aviso(MsgNaoLoc);
  if (Result > 0) and (Trim(MsgLoc) <> '') then
    Geral.MB_Aviso(MsgLoc + sLineBreak + 'Itens = ' +
    IntToStr(Result));
end;

procedure TUModule.StartTransaction(Qry: TmySQLQuery);
begin
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('START TRANSACTION;');
  Qry.ExecSQL;
end;

function TUModule.DiaUtilMes(DataIni: TDateTime; DiaIni, QtdUteis,
  MaxDia: Integer): Integer;
var
  Ano1, Mes1, Dia1: Word;
  Ano2, Mes2, Dia2: Word;
  DiaA, Conta, Uteis: Integer;
  DataA: TDateTime;
begin
  //Result := DiaIni;
  DecodeDate(DataIni, Ano1, Mes1, Dia1);
  DataA := EncodeDate(Ano1, Mes1, DiaIni);
  //
  Uteis := 0;
  Conta := 0;
  DiaA  := DiaIni;
  while Uteis < QtdUteis do
  begin
    if DiaInutil(EncodeDate(Ano1, Mes1, DiaA)) = 0 then
      Uteis := Uteis + 1;
    inc(DiaA, 1);
    DecodeDate(DataA+Conta, Ano2, Mes2, Dia2);
    if Mes2 <> Mes1 then
    begin
      Result := DiaA -1;
      Exit;
    end;
    if DiaA >= MaxDia then
    begin
      Result := MaxDia;
      Exit;
    end;
  end;
  if DiaA > 1 then inc(DiaA, -1);
  Result := DiaA;
end;

function TUModule.DuplicaRegistro_IdxInt1(QrExec: TmySQLQuery; Tabela: String;
  II1Cod: Integer; II1Fld: String; NovoCodigo: TNovoCodigo;
  var Idx: Integer): Boolean;
var
  Fld, Valores, Campos: String;
begin
  Idx := 0;
  Result := False;
  //
  Valores := '';
  QrExec.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrExec, QrExec.Database, [
  'SHOW FIELDS FROM ' + Tabela,
  '']);
  //
  QrExec.First;
  while not QrExec.Eof do
  begin
    Fld := QrExec.FieldByName('Field').AsString;
    if (Uppercase(Fld) <> Uppercase(II1Fld)) and
    (Uppercase(Fld) <> Uppercase('UserAlt')) and
    (Uppercase(Fld) <> Uppercase('DataAlt')) then
      Valores := Valores + ', ' + Fld;
    //
    QrExec.Next;
  end;
  QrExec.Close;
  //
  if not ObtemIndicePrimario_Int(NovoCodigo, QrExec, Tabela, II1Fld, Idx) then
    Exit;
  if Valores <> '' then
  begin
    Campos := II1Fld + Valores;
    Valores := Geral.FF0(Idx) + Valores;
    Valores := Geral.Substitui(Valores, ' UserCad,', ' ' + Geral.FF0(VAR_USUARIO) + ',');
    Valores := Geral.Substitui(Valores, ' DataCad,', ' "' + Geral.FDT(Date, 1) + '",');
    Valores := Geral.Substitui(Valores, ' AlterWeb,', ' 1,');
    QrExec.Close;
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(QrExec, QrExec.Database, [
    'INSERT INTO ' + Tabela + ' (' + Campos + ') ',
    'SELECT ' + Valores,
    'FROM ' + Tabela,
    'WHERE ' + II1Fld + '=' + Geral.FF0(II1Cod),
    '']);
    //
  end else Result := False;
end;

procedure TUModule.ExportaRegistrosEntreDBs_Antigo(TabOrig, TabDest, CondicaoSQL: String;
BaseOrig, BaseDest: TmySQLDatabase; RichEdit: TRichEdit);
  procedure Info(RichEdit: TRichEdit; TabDest, Texto: String);
  begin
    if RichEdit <> nil then
    begin
      RichEdit.SelAttributes.Color := clBlue;
      RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(lowercase(TabDest)) + Texto + sLineBreak + RichEdit.Text;
      RichEdit.Update;
      Application.ProcessMessages;
    end;
  end;
var
  Dir1, Dir2, Arq: String;
  F: TextFile;
  ArqWeb1, ArqWeb2, S, Campos, Prefix: String;
  Query, Qr1, Qr2, Qr3: TmySQLQuery;
  QtdFlds: Integer;
begin
  Dir1 := 'C:\Dermatek\Web\' + Application.Title + '\Conf\';
  Dir2 := 'C:/Dermatek/Web/' + Application.Title + '/Data/';
  ArqWeb1 :=  Dir1 + 'SQL_%s.%s';
  ArqWeb2 :=  Dir2 + 'SQL_%s.%s';
  if not ForceDirectories(Dir1) then
  begin
    Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio "' + Dir1 +
    '". Tente cri�-lo manualmente!');
    Exit;
  end;
  if not ForceDirectories(Dir2) then
  begin
    Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio "' + Dir2 +
    '". Tente cri�-lo manualmente!');
    Exit;
  end;
  //
  Query := TmySQLQuery.Create(Dmod);
  Query.Close;
  Query.Database := BaseOrig;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + lowercase(TabOrig));
  Query.SQL.Add(CondicaoSQL);
  UnDmkDAC_PF.AbreQuery(Query, BaseOrig);
  //
  Qr1 := TmySQLQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Database := BaseOrig;
  Qr1.SQL.Clear;
  //
  Qr2 := TmySQLQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Database := BaseDest;
  Qr2.SQL.Clear;
  //
  Qr3 := TmySQLQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Database := BaseDest;
  Qr3.SQL.Clear;
  //
  Campos := '';
  Prefix := '';
  Info(RichEdit, lowercase(TabDest), ' - Obtendo campos da tabela no destino...');
  Arq := Format(ArqWeb1, [lowercase(TabDest), 'sql']);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Campos := Campos + S;
    end;
    CloseFile(F);
  end else begin
    Campos := ObtemCamposDeTabelaIdentica(BaseDest, lowercase(TabDest), Prefix, QtdFlds);
    Geral.SalvaTextoEmArquivo(Arq, Campos, True);
  end;

  //

  Info(RichEdit, lowercase(TabDest), ' - Exportando para arquivo tempor�rio...');
  Arq := Format(ArqWeb2, [lowercase(TabDest), 'txt']);
  if FileExists(Arq) then
    DeleteFile(Arq);
  if FileExists(dmkPF.DuplicaBarras(Arq)) then
    DeleteFile(dmkPF.DuplicaBarras(Arq));
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ lowercase(TabOrig));
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.ExecSQL;
  //

  Info(RichEdit, lowercase(TabDest), ' - Excluindo registros duplicados na web...');
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + lowercase(TabDest) );
  Qr3.SQL.Add( CondicaoSQL );
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //
  Info(RichEdit, lowercase(TabDest), ' - Importando de arquivo tempor�rio...');
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+lowercase(TabDest));
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(taborig)+' SET AlterWeb=0');
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.ExecSQL;
  //
  Info(RichEdit, lowercase(TabDest), ' - Transfer�ncia de dados finalizada!');
  Info(RichEdit, '', '===============================================');
end;

procedure TUModule.ExportaRegistrosEntreDBs_Novo(TabOrig, TabDest, CondicaoSQL: String;
BaseOrig, BaseDest: TmySQLDatabase; RichEdit: TRichEdit);
  procedure Info(RichEdit: TRichEdit; TabDest, Texto: String);
  begin
    if RichEdit <> nil then
    begin
      RichEdit.SelAttributes.Color := clBlue;
      RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(lowercase(TabDest)) + Texto + sLineBreak + RichEdit.Text;
      RichEdit.Update;
      Application.ProcessMessages;
    end;
  end;
var
  Pasta: String;
  //F: TextFile;
  //S,
  Arquivo, Campos, Prefix, DataDir, Barra: String;
  //QrL,
  Query, Qr1, Qr2, Qr3: TmySQLQuery;
  ArrCampos: TUMyArray_Str;
  ArrValues: TUMyArray_Var;
  QtdFlds: Integer;
begin
  Query := TmySQLQuery.Create(Dmod);
  Query.Close;
  Query.Database := BaseOrig;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + lowercase(TabOrig));
  Query.SQL.Add(CondicaoSQL);
  UnDmkDAC_PF.AbreQuery(Query, BaseOrig);
  //
{
  QrL := TmySQLQuery.Create(Dmod);
  QrL.Close;
  QrL.Database := BaseLocl;
  QrL.SQL.Clear;
}
  //
  Qr1 := TmySQLQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Database := BaseOrig;
  Qr1.SQL.Clear;
  //
  Qr2 := TmySQLQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Database := BaseDest;
  Qr2.SQL.Clear;
  //
  Qr3 := TmySQLQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Database := BaseDest;
  Qr3.SQL.Clear;
  //
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SHOW VARIABLES LIKE "datadir"');
  UnDmkDAC_PF.AbreQuery(Qr1, BaseOrig);
  DataDir := Qr1.FieldByName('Value').AsString;
  //
  if DataDir = '' then
    Barra := '\'
  else
  if pos('\', DataDir) > 0 then
    Barra := '\'
  else
    Barra := '/';
  //
  Pasta := 'C:' +
    Barra + 'Dermatek' +
    Barra + 'Web' +
    Barra + Application.Title +
    Barra + 'Data' +
    Barra;
  Arquivo :=  Pasta + 'SQL_' + lowercase(TabDest) + '.txt';
  if not ForceDirectories(Pasta) then
  begin
    Geral.MB_Aviso('N�o foi poss�vel criar o diret�rio "' + Pasta +
    '". Tente cri�-lo manualmente!');
    Exit;
  end;
{
  if not ForceDirectories(Dir2) then
  begin
    Geral.MB_Erro('N�o foi poss�vel criar o diret�rio "' + Dir2 +
    '". Tente cri�-lo manualmente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
}
  //
  Campos := '';
  Prefix := '';
  Info(RichEdit, lowercase(TabDest), ' - Obtendo campos da tabela de destino...');
  Campos := ObtemCamposDeTabelaIdentica(BaseDest, lowercase(TabDest), Prefix, QtdFlds);
  ArrCampos := ArrayDeTabelaIdentica_Campos(BaseDest, lowercase(TabDest));

  //

  Info(RichEdit, lowercase(TabDest), ' - Obtendo dados da tabela de origem...');
  if FileExists(Arquivo) then
    DeleteFile(Arquivo);
  Arquivo := dmkPF.DuplicaBarras(Arquivo);
  if FileExists(Arquivo) then
    DeleteFile(Arquivo);
  Application.ProcessMessages;

  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ lowercase(TabOrig));
  Qr1.SQL.Add( CondicaoSQL );
  // mudado 2011-10-31 s� funcionava no servidor
  //Qr1.SQL.Add('INTO OUTFILE "' + Arquivo + '"');
  //Qr1.ExecSQL;
  UnDmkDAC_PF.AbreQuery(Qr1, Qr1.Database);
  //

  Info(RichEdit, lowercase(TabDest), ' - Excluindo registros duplicados na web...');
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + lowercase(TabDest) );
  Qr3.SQL.Add( CondicaoSQL );
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //

  Qr1.First;
  Qr2.SQL.Clear;
  Info(RichEdit, lowercase(TabDest), ' - Inserindo lan�amentos na web...');
  while not Qr1.Eof do
  begin
    ArrValues := ArrayDeTabelaIdentica_Values(Qr1);
    //
    UMyMod.SQLInsUpd(Qr2, stIns, lowercase(TabDest), False,
      ArrCampos, [], ArrValues, [], False);
    //
    Qr1.Next;
  end;

{
  Info(RichEdit, lowercase(TabDest), ' - Importando de arquivo tempor�rio...');
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arquivo + '"');
  Qr2.SQL.Add('INTO Table '+lowercase(TabDest));
  Qr2.ExecSQL;
}
  //
  Info(RichEdit, lowercase(TabDest), ' - Finalizando ratifica��o de parcelamento...');
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(taborig)+' SET AlterWeb=0');
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.ExecSQL;
  //
  Info(RichEdit, lowercase(TabDest), ' - Transfer�ncia de dados finalizada!');
  Info(RichEdit, '', '===============================================');
end;

//  Duplica��o de registros
function TUModule.EspelhaRegistroEntreTabelas(RichEdit: TRichEdit;
//SQLCampos: array of String; ValCampos: array of Variant;
TabOrig, TabDest: String; BaseOrig, BaseDest: TmySQLDatabase;
WHERE_Condition: String): Boolean;
var
  Qr1, Qr2, Qr3, Qr4: TmySQLQuery;
  //i, j: Integer;
  x: PChar;
  Virgula: String;
begin
  if RichEdit <> nil then
  begin
    RichEdit.SelAttributes.Color := clBlue;
    RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
      'Fazendo download da tabela ' + '"' + lowercase(TabOrig) + '"' +
      sLineBreak + RichEdit.Text;
    RichEdit.Update;
    Application.ProcessMessages;
  end;
  //
  //Result := False;
  Qr1 := TmySQLQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Database := BaseOrig;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SHOW Fields FROM ' + lowercase(TabOrig));
  UnDmkDAC_PF.AbreQuery(Qr1, BaseOrig);
  //
  Qr2 := TmySQLQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Database := BaseDest;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('SHOW Fields FROM ' + lowercase(TabDest));
  UnDmkDAC_PF.AbreQuery(Qr2, BaseDest);
  //
  Qr3 := TmySQLQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Database := BaseDest;
  Qr3.SQL.Clear;
  //
  Qr4 := TmySQLQuery.Create(Dmod);
  Qr4.Close;
  Qr4.Database := BaseOrig;
  Qr4.SQL.Clear;
  Qr4.SQL.Add('SELECT * FROM '+lowercase(taborig));
  Qr4.SQL.Add(WHERE_Condition);
  //
  if Qr1.RecordCount <> Qr2.RecordCount then
  begin
    x := PChar('A tabela destino "' +
    BaseOrig.DatabaseName + '.' + lowercase(TabOrig) + '" cont�m quantidade diferente ' +
    'de campos (' + IntToStr(Qr2.RecordCount) + ') da tabela origem "' +
    BaseDest.DatabaseName + '.' + lowercase(TabDest) + '" (' +
    IntToStr(Qr1.RecordCount) + '). Espelhamento de registro cancelado!');
    Geral.MB_Aviso(x);
    Result := False;
    Exit;
  end;
  //
  Qr1.First;
  while not Qr1.Eof do
  begin
    if not Qr2.Locate('Field', Qr1.Fields[0].AsString, [loCaseInsensitive]) then
    begin
      x := PChar('A tabela de destino "' +
      BaseDest.DatabaseName + '.' + lowercase(TabDest) + '" n�o cont�m o campo ' +
      Qr1.Fields[0].AsString + ' em sua estrutura!');
      Geral.MB_Aviso(x);
      Result := False;
      Exit;
    end;
    Qr1.Next;
  end;
  //
  Qr3.Close;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + lowercase(tabdest));
  Qr3.SQL.Add(WHERE_Condition);
  Qr3.ExecSQL;

  //

  UnDmkDAC_PF.AbreQuery(Qr4, Qr4.Database);
  Qr4.First;
  while not Qr4.Eof do
  begin
    Virgula := '';
    Qr3.SQL.Clear;
    Qr3.SQL.Add('INSERT INTO ' + lowercase(TabDest) + ' SET ' );
    Qr1.First;
    while not Qr1.Eof do
    begin
      if Qr2.Locate('Field', Qr1.Fields[0].AsString, [loCaseInsensitive]) then
      begin
        Qr3.SQL.Add(Virgula + Qr1.Fields[0].AsString + '=' +
        Geral.VariavelToString(Qr4.FieldByName(Qr1.Fields[0].AsString).Value));
      end;
      Virgula := ', ';
      Qr1.Next;
    end;
    Qr3.ExecSQL;
    Qr4.Next;
  end;
  if RichEdit <> nil then
  begin
    RichEdit.SelAttributes.Color := clBlue;
    RichEdit.Text :=  '===============================================' +
      sLineBreak + RichEdit.Text;
    RichEdit.Update;
    Application.ProcessMessages;
  end;
  //
  Result := True;
end;

function TUModule.BuscaPrimeiroCodigoLivre(Tabela, Campo: String): Integer;
var
  Query: TmySQLQuery;
begin
  Result := 0;
  Query := TmySQLQuery.Create(Dmod);
  Query.Close;
  Query.Database := Dmod.MyDB;


  Query.SQL.Clear;
  Query.SQL.Add('LOCK TABLES ' + Tabela + ' WRITE;');
  Query.ExecSQL;




  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + ' > 0');
  Query.SQL.Add('ORDER BY ' + Campo);
  UnDmkDAC_PF.AbreQuery(Query, Query.Database);
  //
  Query.Last;
  if Query.FieldByName(Campo).AsInteger = Query.RecNo then
    Result := Query.RecordCount + 1 else
  begin
    Query.First;
    while not Query.Eof do
    begin
      if Query.FieldByName(Campo).AsInteger > Query.RecNo then
      begin
        Result := Query.RecNo;
        Query.SQL.Clear;
        Query.SQL.Add('UNLOCK TABLES;');
        Query.ExecSQL;
        Query.Free;
        Exit;
      end;
      Query.Next;
    end;
  end;
  //
  Query.SQL.Clear;
  Query.SQL.Add('UNLOCK TABLES;');
  Query.ExecSQL;
  Query.Free;
end;

function TUModule.BuscaProximaFatParcelaDeFatID(FatID: Integer; Tabela: String): Integer;
begin
  Result := BuscaProximoGerlSeq1Int32(Tabela, 'FatParcela',
    'FatID=' + Geral.FF0(FatID), '', tsDef, stIns, 0, Null);
end;


function TUModule.BuscaProximoGerlSeq1Int32(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Integer;
  Reservados: Variant; DB: TmySQLDatabase; Quantidade: Integer): Integer;
const
  txtNeg = 'BigIntNeg';
  txtPos = 'BigIntPos';
  txtErr = 'BigInt???';
var
  Tab, Fld, MiM: String;
  Atual, P, Ultimo: Integer;
  Forma: TSQLType;
  Sinal: TTipoSinal;
  DataBase: TmySQLDatabase;
  Existe: Boolean;
begin
  Result := 0;
  case SQLType of
    stUpd: Result := DefUpd;
    stIns:
    begin
      Tab := LowerCase(Tabela);
      P := pos('.', Tab);
      if P > 0 then
        Tab := Copy(Tab, P + 1);
      //
      Fld := txtErr;
      case TipoSinal of
        tsPos: Fld := TxtPos;
        tsNeg: Fld := txtNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Fld := txtNeg
          else
            Fld := txtPos;
        end;
      end;
      if DB = nil then
        DataBase := DMod.MyDB
      else
        DataBase := DB;
      //
      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;');
      QvUpdY.ExecSQL;
      //
      QvLivreY.Close;
      QvLivreY.Database := Database;
      UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
      'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
      'WHERE Tabela="' + LowerCase(Tab) + '" ',
      'AND Campo="' + LowerCase(Campo) + '" ',
      'AND _WHERE="' + LowerCase(_WHERE) + '" ',
      'AND _AND="' + LowerCase(_AND) + '" ',
      '']);
      Atual := QvLivreY.FieldByName('Codigo').AsInteger;
      // ini 2022-02-16
      Existe := QvLivreY.RecordCount > 0;
      // fim 2022-02-16

      // Verificar se j� existe
      UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
      'SELECT ' + Campo + ' Codigo FROM ' + Tab,
      'WHERE ' + Campo + '=' + Geral.FF0(Atual),
      Geral.ATS_if(_WHERE<>'', ['AND ' + _WHERE]),
      Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
      '']);

      if (Atual = 0) then
      // ini 2022-02-16
      //Forma := stIns
      begin
        if Existe then
          Forma := stUpd
        else
          Forma := stIns
      end else
      // fim 2022-02-16
        Forma := stUpd;
      // Evitar duplicidade buscando na tabela de inser��o:
      if (Atual = 0) or (QvLivreY.RecordCount > 0) then
      begin
        case TipoSinal of
          tsPos: MiM := 'MAX(';
          tsNeg: MiM := 'MIN(';
          tsDef:
          begin
            if VAR_ForcaBigIntNeg then
              MiM := 'MIN('
            else
              MiM := 'MAX(';
          end;
          else MiM := 'MINMAX?(';
        end;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
        'SELECT ' + MiM + Campo + ') Codigo FROM ' + Tab,
        Geral.ATS_if(_WHERE<>'', ['WHERE ' + _WHERE]),
        Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
        '']);
        //
        Atual := QvLivreY.FieldByName('Codigo').AsInteger;
      end;
      // Fim evitar duplicidade!
      //
      case TipoSinal of
        tsPos: Sinal := tsPos;
        tsNeg: Sinal := tsNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Sinal := tsNeg
          else
            Sinal := tsPos;
        end;
        else Sinal := tsPos;
      end;

      case Sinal of
        tsPos:
        begin
          if Atual < 1 then
            Result := 1 // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
          else
            Result := Atual + 1;
        end;
        tsNeg:
        begin
          if Atual > -1 then
            Result := -1
          else
            Result := Atual - 1;
        end;
      end;
      //
      // 2013-04-06
      if Reservados <> Null then
      begin
        if Result <= Reservados then
          Result := Reservados + 1;
      end;
      // fim 2013-04-06
      QvLivreY.Close;
      // ini 2023-10-27
      if Quantidade > 1 then
      begin
        if Result > 0 then
          Ultimo := Result + Quantidade - 1;
        if Result < 0 then
          Ultimo := Result - Quantidade + 1;  // n�o testado
      end else
        Ultimo := Result;
      // fim 2023-10-27
      //
      //try
        // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
        SQLInsUpd(QvUpdY, Forma, 'gerlseq1', False, [
        Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
      // ini 2023-10-27
        //Result], [Tab, Campo, _WHERE, _AND], True);
        Ultimo], [Tab, Campo, _WHERE, _AND], True);
      // fim 2023-10-27
        //, '', False, False);
      {
      except
        SQLInsUpd(QvUpdY, stUpd, 'gerlseq1', False, [
        Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
        Result], [Tab, Campo, _WHERE, _AND], True);
      end;}
      //
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UNLOCK TABLES;');
      QvUpdY.ExecSQL;
    end;
    else Geral.MB_Erro(
    '"SQLType" n�o implementado no "BuscaProximoGerlSeq1Int32"');
  end;
  //
end;

function TUModule.BPGS1I64(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64;
  DB: TmySQLDatabase): Int64;
const
  txtNeg = 'BigIntNeg';
  txtPos = 'BigIntPos';
  txtErr = 'BigInt???';
var
  Tab, Fld, MiM: String;
  Atual, P: Int64;
  Forma: TSQLType;
  Sinal: TTipoSinal;
  DataBase: TmySQLDatabase;
begin
  Result := 0;
  case SQLType of
    stUpd: Result := DefUpd;
    stIns:
    begin
      Tab := LowerCase(Tabela);
      P := pos('.', Tab);
      if P > 0 then
        Tab := Copy(Tab, P + 1);
      //
      Fld := txtErr;
      case TipoSinal of
        tsPos: Fld := TxtPos;
        tsNeg: Fld := txtNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Fld := txtNeg
          else
            Fld := txtPos;
        end;
      end;
      if DB = nil then
        DataBase := DMod.MyDB
      else
        DataBase := DB;
      //
      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('LOCK TABLES gerlseq1 WRITE, ' + Tab + ' WRITE;');
      QvUpdY.ExecSQL;
      //
      QvLivreY.Close;
      QvLivreY.Database := Database;
      UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
      'SELECT ' + Fld + ' Codigo FROM gerlseq1 ',
      'WHERE Tabela="' + LowerCase(Tab) + '" ',
      'AND Campo="' + LowerCase(Campo) + '" ',
      'AND _WHERE="' + LowerCase(_WHERE) + '" ',
      'AND _AND="' + LowerCase(_AND) + '" ',
      '']);
      if QvLivreY.FieldByName('Codigo').AsVariant <> Null then
        Atual := QvLivreY.FieldByName('Codigo').AsVariant
      else
        Atual := 0;
      // Verificar se j� existe
      UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
      'SELECT ' + Campo + ' Codigo FROM ' + Tab,
      'WHERE ' + Campo + '=' + Geral.FF0(Atual),
      Geral.ATS_if(_WHERE<>'', ['AND ' + _WHERE]),
      Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
      '']);

      if (Atual = 0) then
        Forma := stIns
      else
        Forma := stUpd;
      // Evitar duplicidade buscando na tabela de inser��o:
      if (Atual = 0) or (QvLivreY.RecordCount > 0) then
      begin
        case TipoSinal of
          tsPos: MiM := 'MAX(';
          tsNeg: MiM := 'MIN(';
          tsDef:
          begin
            if VAR_ForcaBigIntNeg then
              MiM := 'MIN('
            else
              MiM := 'MAX(';
          end;
          else MiM := 'MINMAX?(';
        end;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
        'SELECT ' + MiM + Campo + ') Codigo FROM ' + Tab,
        Geral.ATS_if(_WHERE<>'', ['WHERE ' + _WHERE]),
        Geral.ATS_if(_AND<>'', ['AND ' + _AND]),
        '']);
        //
        if QvLivreY.FieldByName('Codigo').AsVariant <> Null then
          Atual := QvLivreY.FieldByName('Codigo').AsVariant
        else
          Atual := 0;
      end;
      // Fim evitar duplicidade!
      //
      case TipoSinal of
        tsPos: Sinal := tsPos;
        tsNeg: Sinal := tsNeg;
        tsDef:
        begin
          if VAR_ForcaBigIntNeg then
            Sinal := tsNeg
          else
            Sinal := tsPos;
        end;
        else Sinal := tsPos;
      end;

      case Sinal of
        tsPos:
        begin
          if Atual < 1 then
            Result := 1 // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
          else
            Result := Atual + 1;
        end;
        tsNeg:
        begin
          if Atual > -1 then
            Result := -1
          else
            Result := Atual - 1;
        end;
      end;
      //
      QvLivreY.Close;
      //
      //try
        // est� fazendo -1 + 1 = 0 quando existem negativos padr�es sem nenhum positivo incluido!
        SQLInsUpd(QvUpdY, Forma, 'gerlseq1', False, [
        Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
        Result], [Tab, Campo, _WHERE, _AND], True);
        //, '', False, False);
      {
      except
        SQLInsUpd(QvUpdY, stUpd, 'gerlseq1', False, [
        Fld], ['Tabela', 'Campo', '_WHERE', '_AND'], [
        Result], [Tab, Campo, _WHERE, _AND], True);
      end;}
      //
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UNLOCK TABLES;');
      QvUpdY.ExecSQL;
    end;
    else Geral.MB_Erro(
    '"SQLType" n�o implementado no "BuscaProximoGerlSeq1Int64"');
  end;
  //
end;

function TUModule.BPGS1I64_Reaproveita(Tabela, Campo, _WHERE, _AND: String;
  TipoSinal: TTipoSinal; SQLType: TSQLType; DefUpd: Int64; DB: TmySQLDatabase): Int64;
const
  TabLivre = 'livre2';
var
  Codigo : Int64;
  //FldCtrl: String;
  Continua: Boolean;
  Database: TmySQLDatabase;
begin
  Codigo := 0;
  Continua := True;
  Database := Dmod.MyDB;
  //
  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    Lowercase(Tabela) + ' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;

  while Continua do
  begin
    QvLivreY.Close;
    QvLivreY.Database := Database;
    UnDmkDAC_PF.AbreMySQLQuery0(QvLivreY, Database, [
    'SELECT Codigo ',
    'FROM ' + TabLivre,
    'WHERE Tabela="' + Lowercase(Tabela) + '" ',
    'AND Campo="' + Campo + '" ',
    'ORDER BY Codigo ',
    'LIMIT 1',
    '']);
    //
    if (QvLivreY.FieldByName('Codigo').AsVariant <> Null) and
    (QvLivreY.FieldByName('Codigo').AsVariant > 0) then
    begin
      Codigo := QvLivreY.FieldByName('Codigo').AsVariant;
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add(DELETE_FROM + Lowercase(TabLivre));
      QvUpdY.SQL.Add('WHERE Tabela="' + Lowercase(Tabela) + '"');
      QvUpdY.SQL.Add('AND Campo="' + Campo + '"');
      QvUpdY.SQL.Add('AND Codigo=' + Geral.FF0(Codigo));
      QvUpdY.ExecSQL;
      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT ' + Campo + ' Codigo FROM ' + Lowercase(Tabela));
      QvLivreY.SQL.Add('WHERE ' + Campo + '=' + Geral.FF0(Codigo));
      UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
      if QvLivreY.RecordCount > 0 then
      begin
        Codigo := 0;
        Continua := True;
      end else Continua := False;
    end else Continua := False;
  end;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  //

  if Codigo = 0 then
  begin
    Result := BPGS1I64(
      Tabela, Campo, _WHERE, _AND, TipoSinal, SQLType, DefUpd, DB);
  end else Result := Codigo;
end;

{
function TUModule.BuscaProximoCtrlGeral(Campo: String): Integer;
begin
  QvUpdY.Close;
  QvUpdY.Database := DMod.MyDB;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ctrlgeral WRITE;');
  QvUpdY.ExecSQL;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE ctrlgeral SET ' + Campo + '=' + Campo + '+1'); // FatGrupo
  QvUpdY.ExecSQL;
  //
  QvLivreY.Close;
  QvLivreY.Database := Dmod.MyDB;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT ' + Campo + ' Codigo FROM ctrlgeral');
  UnDmkDAC_PF.AbreQuery(QvLivreY, Database);
  Result := QvLivreY.FieldByName('Codigo').AsInteger;
  QvLivreY.Close;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;
  //
end;
}

function TUModule.Busca_IDCtrl_CTe(Acao: TSQLType; Atual: Integer): Integer;
begin
  Result := UMyMod.BPGS1I32('ctecaba', 'IDCtrl', '', '', tsPos, Acao, Atual);
end;

function TUModule.Busca_IDCtrl_MDFe(Acao: TSQLType; Atual: Integer): Integer;
begin
  Result := UMyMod.BPGS1I32('mdfecaba', 'IDCtrl', '', '', tsPos, Acao, Atual);
end;

function TUModule.Busca_IDCtrl_NFe(Acao: TSQLType; Atual: Integer): Integer;
var
  Cod1, Cod2, Cod3, Cod4: Integer;
begin
  if Acao = stUpd then
  begin
    Result := Atual;
    Exit;
  end;
  //
  Cod1 := UMyMod.BuscaEmLivreY_Def('stqmovnfsa', 'IDCtrl', Acao, Atual);
  //Cod2 := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', Acao, Atual);
{$IfNDef semNFe_v0000}
  Cod2 := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', Atual);
{$Else}
  Cod2 := 0;
{$EndIf}
  Cod4 := DModG.BuscaProximoInteiro('stqmovitsa', 'IDCtrl', '', Atual);
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('LOCK TABLES controle WRITE');
  Dmod.QrAux.ExecSQL;
  //
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT IDCtrl_NFe FROM Controle');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  Cod3 := Dmod.QrAux.FieldByName('IDCtrl_NFe').AsInteger;
  Cod3 := Cod3 + 1;
  //
  if Cod1 >= Cod2 then
  begin
    if Cod1 >= Cod3 then
      Result := Cod1
    else
      Result := Cod3;
  end else
  begin
    if Cod2 >= Cod3 then
      Result := Cod2
    else
      Result := Cod3;
  end;
  if Cod4 > Result then
    Result := Cod4;
  //
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('UPDATE controle SET IDCtrl_NFe=' + FormatFloat('0', Result));
  Dmod.QrAux.ExecSQL;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('UNLOCK TABLES ');
  Dmod.QrAux.ExecSQL;
  //
end;

(*function TUModule.AbreBDEQuery(Query: TQuery; DataBaseName: String;
  Aviso: String): Boolean;
*)
function TUModule.AbreBDEQuery(Query: TDataSet; DataBaseName: String;
  Aviso: String): Boolean;
  //
  procedure MostraQuery(Query: TDataSet; Aviso: String = '');
  var
    i: Integer;
    NomeParente: String;
    Texto: WideString;
  begin
    if Query.Owner <> nil then
      NomeParente := 'Owner: ' + TComponent(Query.Owner).Name
    else
      NomeParente := 'SEM OWNER!';
    Texto := '/* ' + Aviso +
      sLineBreak + NomeParente +
      sLineBreak + ' */' +
{$IfDef cDbTable}
      sLineBreak + TQuery(Query).SQL.Text;
    //
    for i := 1 to Query.ParamCount do
    begin
      Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
        TParam(Query.Params[i-1]).AsString + '*/';
    end;
{$Else}
      sLineBreak (*+ Query.SQL.Text*);
{$EndIf}
    Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
    Geral.MB_Erro(Texto);
  end;
var
  Texto: String;
begin
  Screen.Cursor := crSQLWait;
  try
    Query.Close;
    if Trim(DatabaseName) <> '' then
    begin
{$IfDef cDbTable}
      TQuert(Query).DatabaseName := DataBaseName;
{$Else}
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');  //  TODO Berlin
{$EndIf}
    end;
    Query. Open ;
    Result := True;
  except
    on E: Exception do
    begin
      Texto := E.Message + sLineBreak;
      MostraQuery(Query, Texto + Aviso);
      Screen.Cursor := crDefault;
      raise;
    end else
    begin
      MostraQuery(Query, Aviso);
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

                //AbreMySQLQuery2
function TUModule.AbreMySQLQueryA(Query: TMySQLQuery; Database: TmySQLDatabase;
  SQLPre: array of String; Condicoes: array of Boolean; SQLCompl: array of String;
              SQLPos: array of String): Boolean;
var
  I, J, N: Integer;
  SQL: array of String;
begin
  Result := False;
  I := High(Condicoes);
  J := High(SQLCompl);
  if I <> J then
  begin
    Geral.MB_Erro('ERRO! Existem ' + IntToStr(I + 1) + ' campos e ' +
    IntToStr(J + 1) + ' valores para estes campos em "AbreMySQLQueryA()"!');
    Exit;
  end;
  //
  N := Length(SQLPre) + I + 1 + Length(SQLPos);
  SetLength(SQL, n);
  N := 0;
  for I := 0 to High(SQLPre) do
  begin
    N := N + 1;
    SQL[N] := SQLPre[I];
  end;
  //j := High(SQLCompl);
  for I := Low(Condicoes) to j do
  begin
    if Condicoes[I] then
    begin
      N := N + 1;
      SQL[N] := SQLCompl[I];
    end;
  end;
  //
  for I := 0 to High(SQLPos) do
  begin
    N := N + 1;
    SQL[N] := SQLPos[I];
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, SQL);
end;

function TUModule.AbreQuery(Query: TmySQLQuery; DB: TmySQLDatabase; Aviso: String = ''): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
  procedure MostraQuery(Query: TmySQLQuery);
  var
    i: Integer;
    NomeParente: String;
    Texto: WideString;
  begin
    if Query.Owner <> nil then
      NomeParente := 'Owner: ' + TComponent(Query.Owner).Name
    else
      NomeParente := 'SEM OWNER!';
    Texto := '/* ' + Aviso +
      sLineBreak + NomeParente +
      sLineBreak + ' */' +
      sLineBreak + Query.SQL.Text;
    //
    for i := 1 to Query.ParamCount do
    begin
      Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
        TParam(Query.Params[i-1]).AsString + '*/';
    end;
    Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
    Geral.MB_Erro(Texto);
    //

    {
    MyObjects.CriaForm_AcessoTotal(TFmAviso, FmAviso);
    with FmAviso do
    begin
      Memo.Text := '/* ' + Aviso + ' */' + sLineBreak + Query.SQL.Text;
      BtOK.Enabled := True;
      CkNaoMais.Visible := False;
      LaNumero.Caption := '004';
      //
      for i := 1 to Query.ParamCount do
      begin
        Memo.Lines.Add('/* Params[' + IntToStr(i-1) + '] = ' +
          TParam(Query.Params[i-1]).AsString + '*/');
      end;
      //
      Memo.Lines.Add('/*' + Query.Name + '*/');
      ShowModal;
      Destroy;
    end;
    }
  end;
begin
  //Result := False;
  try
    Query.Close;
    Query.Database := DB;
    UnDmkDAC_PF.AbreQuery(Query, DB);
    Result := True;
  except
    MostraQuery(Query);
    raise;
  end;
end;

function TUModule.AbreSQL_ABD(Qry: TmySQLQuery; NomeTabACriar, TabLctA, TabLctB,
  TabLctD, TabID_A, TabID_B, TabID_D: String; DataI, DataF, DtEncer, DtMorto:
  TDateTime; SQL_ABD, SQLExtra: array of String;
  ExcluiTab: TTipoExcluiTab; Aviso: String): Boolean;
  //
  procedure GeraParteSQL(TabLct, Tab_ID: String);
  var
    J: Integer;
    Txt: String;
  begin
    for J := Low(SQL_ABD) to High(SQL_ABD) do
    begin
      Txt := SQL_ABD[J];
      while pos(LCT_MASK, Txt) > 0 do
        Txt := Geral.Substitui(Txt, LCT_MASK, TabLct);
      while pos(_ID_MASK, Txt) > 0 do
        Txt := Geral.Substitui(Txt, _ID_MASK, Tab_ID);
      Qry.SQL.Add(Txt);
    end;
  end;
var
  I: Integer;
  //TxtCria: String;
  GerouTbX: Boolean;
  dIni, dFim: TDateTime;
begin
  Result := False;
  GerouTbX := False;
  if DataF >= DataI then
  begin
    dIni := Int(DataI);
    dFim := Int(DataF);
  end else begin
    // inverte
    dIni := Int(DataF);
    dFim := Int(DataI);
  end;
  //
  Qry.Close;
  Qry.SQL.Clear;
  if NomeTabACriar <> '' then
  begin
    if ExcluiTab <> tetNaoExclui then
    begin
      if Qry.Database <> DModG.MyPID_DB then
      begin
        Geral.MB_Erro('O Aplicativo ser� encerrado!' + sLineBreak +
        'Avise a DERMATEK: AbreSQL_ABD()');
        Halt(0);
        Exit;
      end else
    end;
    if ExcluiTab in ([tetExcuiSoAntes, tetExcluiAntesEDepois]) then
    begin
      Qry.SQL.Add('DROP TABLE IF EXISTS ' + NomeTabACriar + ';');
      Qry.SQL.Add('CREATE TABLE ' + NomeTabACriar + '');
      Qry.SQL.Add('');
    end
  end else
  begin
    if ExcluiTab <> tetNaoExclui then
      Geral.MB_Erro('Solicita��o de exclus�o de tabela tempor�ria negada!' +
      sLineBreak + 'Avise a DERMATEK: AbreSQL_ABD()');
  end;
  //
  if (dIni + dFim < 4) or (dFim >= Int(DtEncer)) then
  begin
    GeraParteSQL(TabLctA, TabID_A);
    GerouTbX := True;
  end;
  if dIni <= Int(DtEncer) then
  begin
    if GerouTbX then
      Qry.SQL.Add('UNION');
    GeraParteSQL(TabLctB, TabID_B);
    GerouTbX := True;
  end;
  if dIni <= Int(DtMorto) then
  begin
    if GerouTbX then
      Qry.SQL.Add('UNION');
    GeraParteSQL(TabLctD, TabID_D);
  end;
  Qry.SQL.Add(';');

  //

  if NomeTabACriar <> '' then
  begin
    Qry.SQL.Add('');
    Qry.SQL.Add('SELECT * FROM ' + NomeTabACriar + '');
    //
    for I := Low(SQLExtra) to High(SQLExtra) do
      Qry.SQL.Add(SQLExtra[I]);
    //
    Qry.SQL.Add(';');

    if ExcluiTab = tetExcluiAntesEDepois then
    begin
      Qry.SQL.Add('');
      Qry.SQL.Add('DROP TABLE IF EXISTS ' + NomeTabACriar + ';');
      Qry.SQL.Add('');
    end;
  end;
  //
  AbreQuery(Qry, Qry.DataBase, 'AbreSQL_ABD() - ' + Aviso);
end;

function TUModule.AbreTable(Table: TmySQLTable; DB: TmySQLDatabase;
  Aviso: String): Boolean;
begin
  //Result := False;
  try
    Table.Close;
    Table.Database := DB;
    UnDmkDAC_PF.AbreTable(Table, DB);
    Result := True;
  except
    Geral.MB_Erro('N�o foi poss�vel abrir a TABLE: ' +
    Table.Name + ' (tabela "' + Table.Name + '"');
    raise;
  end;
end;

function TUModule.AcertaOrdemTodosRegistros(Tabela, FldIndice, FldOrdem: String;
  QueryAReordenar, QrUpd: TmySQLQuery): Boolean;
  procedure AtualizaOrdemDoItem(Ordem, Indice: Integer);
  begin
    UMyMod.SQLInsUpd(QrUpd, stUpd, Lowercase(Tabela), False, [
    FldOrdem], [FldIndice], [Ordem], [Indice], False);
  end;
var
  I, Indice: Integer;
begin
  //Result := False;
  Indice := QueryAReordenar.FieldByName(FldIndice).AsInteger;
  //
  I := 0;
  QueryAReordenar.First;
  while not QueryAReordenar.Eof do
  begin
    I := I + 1;
    AtualizaOrdemDoItem(I, QueryAReordenar.FieldByName(FldIndice).AsInteger);
    QueryAReordenar.Next;
  end;
  //
  QueryAReordenar.Close;
  UnDmkDAC_PF.AbreQuery(QueryAReordenar, QueryAReordenar.Database);

  Result := QueryAReordenar.Locate(FldIndice, Indice, []);
end;

function TUModule.ExecutaQuery(Query: TmySQLQuery): Boolean;
  procedure MostraQuery(Query: TmySQLQuery);
  var
    i: Integer;
    Texto: WideString;
  begin
    Texto := Query.SQL.Text;
    //
    for i := 1 to Query.ParamCount do
    begin
      Texto := Texto + sLineBreak + '/* Params[' + IntToStr(i-1) + '] = ' +
        TParam(Query.Params[i-1]).AsString + '*/';
    end;
    //
    Texto := Texto + sLineBreak + '/*' + Query.Name + '*/';
    //
    Geral.MB_Erro(Texto);
  end;
begin
  try
    Query.ExecSQL;
    Result := True;
  except
    Geral.MB_Erro(Query.SQL.Text);
    MostraQuery(Query);
    raise;
  end;
end;

function TUModule.ExecutaDB(DB: TmySQLDatabase; SQL: String): Boolean;
  procedure MostraSQL();
  var
    Texto: String;
  begin
    Texto := SQL + sLineBreak + '/*' + DB.Name + '*/';
    //
    Geral.MB_Erro(Texto);
  end;
begin
  try
    DB.Execute(SQL);
    Result := True;
  except
    MostraSQL();
    raise;
  end;
end;

function TUModule.ExecutaMySQLQuery1(Query: TMySQLQuery;
  SQL: array of String): Boolean;
var
  I: Integer;
begin
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) > 0 then
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no MySQLQuery!' + sLineBreak +
      'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.ExecSQL;
      //Geral.MB_Erro(Query.SQL.Text, 'SQL', MB_OK+MB_ICONINFORMATION);
      Result := True;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MB_Erro('Erro ao tentar executar uma SQL no MySQLQuery!' + sLineBreak +
    'Avise a DERMATEK!');
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(Query, '', nil, True, True);
    raise;
  end;
end;

// Mudei 2011-07-02 :: coloquei MinRec
function TUModule.HabilitaMenuItemInt(MenuItens: array of TMenuItem;
Query: TmySQLQuery; Campo: String; MinRec, MinVal: Integer): Boolean;
var
  i: Integer;
begin
{
  if Query.State = dsInactive then Result := False else
  Result :=  Query.FieldByName(Campo).AsInteger >= MinVal;
  for i := Low(MenuItens) to High(MenuItens) do
    if MenuItens[i] <> nil then
      MenuItens[i].Enabled := Result;
}
  if Query.State = dsInactive then Result := False else
  begin
    Result := Query.RecordCount >= MinRec;
    if Result and (Campo <> '') then
      Result := Query.FieldByName(Campo).AsInteger >= MinVal;
  end;
  for i := Low(MenuItens) to High(MenuItens) do
    if MenuItens[i] <> nil then
      MenuItens[i].Enabled := Result;
end;

{  N�o usar mais! Usar ExcluiLct_?
function TUModule.ExcluiLct(QrAux, QrUpd: TmySQLQuery; Campos: array of String;
Valores: array of Variant; TabLctA: String): Boolean;
// Para facilitar controle saldo carteiras
var
  Liga, Valor, Condicao: String;
  i, j: integer;
begin
  // Fazer saldo (final) de conta
  Result := False;
  Liga := 'WHERE ';
  j := High(Campos);
  Condicao := '';
  for i := Low(Campos) to j do
  begin
    Valor := Geral.VariavelToString(Valores[i]);
    Condicao := Condicao + Liga + Campos[i] + '=' + Valor + ' ' + sLineBreak;
    Liga := 'AND ';
  end;
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT MIN(Data) Data, CliInt FROM ' + TabLctA + '');
  QrAux.SQL.Add(Condicao);
  QrAux.SQL.Add('GROUP BY CliInt');
  UnDmkDAC_PF.AbreQuery(QrAux, Database);
  QrAux.First;
  while not QrAux.Eof do
  begin
    AtzEntiAltDtPlaCt(QrUpd, QrAux.FieldByName('CliInt').AsInteger,
      QrAux.FieldByName('Data').AsDateTime);
    QrAux.Next;
  end;

  QrUpd.SQL.Clear;
  QrUpd.SQL.Add(EXCLUI_DE + TabLctA + '');
  QrUpd.SQL.Add(Condicao);
  //
  try
    QrUpd.ExecSQL;
    //
    Result := True;
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
  end;
end;
}

procedure TUModule.AtzEntiAltDtPlaCt(QrUpd: TmySQLQuery; CliInt: Integer;
Data: TDateTime);
var
 DtTxt: String;
begin
  DtTxt := Geral.FDT(Data, 1);
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, ');
  QrUpd.SQL.Add('AltDtPlaCt=:P0 ');
  QrUpd.SQL.Add('WHERE Codigo=:P1 ');
  QrUpd.SQL.Add('AND AltDtPlaCt > :P2');
  QrUpd.Params[00].AsInteger := CliInt;
  QrUpd.Params[01].AsString  := DtTxt;
  QrUpd.Params[02].AsString  := DtTxt;
  QrUpd.ExecSQL;
end;

function TUModule.BuscaNovoCodigo_Int(QrAux: TmySQLQuery; Tabela, Campo: String;
CamposExtras: array of String; ValExtras: array of Variant;
SQLType: TSQLType; Padrao: Integer; Sinal: TSinal;
dmkEdit: TdmkEdit): Integer;
var
  i, j: Integer;
  Liga, Valor: String;
begin
  if SQLType = stUpd then
  begin
    Result := Padrao;
    Exit;
  end;
  QrAux.Close;
  QrAux.SQL.Clear;
  if Sinal = siPositivo then
    QrAux.SQL.Add('SELECT MAX(' + Campo + ') Campo FROM ' + lowercase(Tabela))
  else
    QrAux.SQL.Add('SELECT MIN(' + Campo + ') Campo FROM ' + lowercase(Tabela));

  //

  Liga := 'WHERE';
  j := High(CamposExtras);
  for i := Low(CamposExtras) to j do
  begin
    Valor := Geral.VariavelToString(ValExtras[i]);
    {
    if (i < j) then
      QrAux.SQL.Add(Liga + ' ' + CamposExtras[i] + '=' + Valor + ', ')
    else
    }
      QrAux.SQL.Add(Liga + ' ' + CamposExtras[i] + '=' + Valor);
    Liga := 'AND';
  end;
  //
  try
    //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrAux, '', nil, True, True);
    UnDmkDAC_PF.AbreQuery(QrAux, QrAux.Database);
  except
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrAux, '', nil, True, True);
  end;
  if Sinal = siPositivo then
  begin
    Result := QrAux.Fields[0].AsInteger + 1;
    //Mudado em 19/06/2014
    //if Result = 0 then Result := 1;
    if Result < 1 then Result := 1;
  end else begin
    Result := QrAux.Fields[0].AsInteger - 1;
    //Mudado em 19/06/2014
    //if Result = 0 then Result := -1;
    if Result > -1 then Result := -1;
  end;
  //
  if dmkEdit <> nil then
    dmkEdit.ValueVariant := Result;
end;

procedure TUModule.AtualizaEntidadesCodigoToCodUsu(Tabela: String);
var
  Existe: Boolean;
  Tab: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  if Geral.MB_Pergunta('Esta a��o igualar� todos c�digos de ' +
  'usu�rio ao ID (C�digo de identifica��o interno do sistema) da tabela "' +
  Tabela + '". Deseja ' + 'continar assim mesmo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Tab := Lowercase(Tabela);
      // Ver se o �ndice existe, e caso exista, ecluir
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SHOW INDEX FROM ' + Tab);
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.QrAux.Database);
      //
      Existe := False;
      while not Dmod.QrAux.Eof do
      begin
        if Uppercase(Dmod.QrAux.FieldByName('Key_Name').AsString) = 'UNIQUE1' then
          Existe := True;
        Dmod.QrAux.Next;
      end;
      if Existe then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('ALTER TABLE ' + Tab + ' DROP INDEX UNIQUE1');
        Dmod.QrUpd.ExecSQL;
      end;

      // Igualar CodUsu ao codigo de todos cadastros
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + Tab + ' SET CodUsu = Codigo');
      Dmod.QrUpd.ExecSQL;

      // Refazer �ndice
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('ALTER TABLE ' + Tab + ' ADD UNIQUE UNIQUE1 (CodUsu)');
      Dmod.QrUpd.ExecSQL;
      //
      Geral.MB_Info('Atualiza��o realizada com sucesso na ' +
      'tabela "' + Tab + '!')
    finally
     Screen.Cursor := crDefault;
    end;
  end;
end;

end.

