unit Servidor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, UnInternalConsts, dmkGeral;

type
  TFmServidor = class(TForm)
    RGTipo: TRadioGroup;
    BtConfirma: TBitBtn;
    LaApp: TLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmServidor: TFmServidor;

implementation

{$R *.DFM}

procedure TFmServidor.BtConfirmaClick(Sender: TObject);
//var
  //AppName: String;
begin
  //if LaApp.Caption = 'Cashier' then AppName := 'Dermatek'
  //
  Geral.WriteAppKeyLM2('Server', Application.Title, RGTipo.ItemIndex, ktInteger);
  //
  VAR_SERVIDOR := RGTipo.ItemIndex;
  Close;
end;

procedure TFmServidor.FormActivate(Sender: TObject);
begin
  //MyObjects.CorIniComponente();
end;

end.
