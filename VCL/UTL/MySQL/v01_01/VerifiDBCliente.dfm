object FmVerifiDBCliente: TFmVerifiDBCliente
  Left = 382
  Top = 202
  Caption = 'FER-VRFBD-002 :: Verifica'#231#227'o de Banco de Dados Cliente'
  ClientHeight = 446
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Verifica'#231#227'o de Banco de Dados Cliente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 684
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 394
    Width = 688
    Height = 52
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 125
      Left = 10
      Top = 4
      Width = 112
      Height = 44
      Cursor = crHandPoint
      Caption = '&Atualiza'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtSair: TBitBtn
      Tag = 13
      Left = 558
      Top = 4
      Width = 112
      Height = 44
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSairClick
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 688
    Height = 346
    Align = alClient
    TabOrder = 2
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 329
      Width = 686
      Height = 16
      Align = alBottom
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 686
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 686
        Height = 60
        Align = alClient
        Caption = ' Verifica'#231#245'es: '
        TabOrder = 0
        object CkEstrutura: TCheckBox
          Left = 172
          Top = 16
          Width = 161
          Height = 17
          Caption = 'Estrutura do banco de dados.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkPergunta: TCheckBox
          Left = 12
          Top = 16
          Width = 153
          Height = 17
          Caption = 'Pergunta antes de executar.'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object CkEstrutLoc: TCheckBox
          Left = 336
          Top = 16
          Width = 185
          Height = 17
          Caption = 'Estrutura do banco de dados local.'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object CkRegObrigat: TCheckBox
          Left = 524
          Top = 16
          Width = 157
          Height = 17
          Caption = 'Recria registros obrigat'#243'rios.'
          Enabled = False
          TabOrder = 3
        end
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 61
      Width = 686
      Height = 268
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Avisos'
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 678
          Height = 240
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 140
    Top = 120
  end
end
