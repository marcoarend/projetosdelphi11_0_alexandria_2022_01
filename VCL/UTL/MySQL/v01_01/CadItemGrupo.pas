unit CadItemGrupo;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmCadItemGrupo = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCad: TmySQLQuery;
    DsCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdNivSup: TdmkEditCB;
    CBNivSup: TdmkDBLookupComboBox;
    LaNivSup: TLabel;
    QrGru: TmySQLQuery;
    DsGru: TDataSource;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    QrGruCodigo: TIntegerField;
    QrGruNome: TWideStringField;
    SbNivSup: TSpeedButton;
    QrCadNO_NIVSUP: TWideStringField;
    QrCadNivSup: TIntegerField;
    QrCadCodigo: TIntegerField;
    QrCadNome: TWideStringField;
    BtNivelAcima: TBitBtn;
    QrCadOrdem: TIntegerField;
    Label4: TLabel;
    EdOrdem: TdmkEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    CkContinuar: TCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbNivSupClick(Sender: TObject);
    procedure BtNivelAcimaClick(Sender: TObject);
    procedure QrCadBeforePost(DataSet: TDataSet);
    procedure QrCadBeforeClose(DataSet: TDataSet);
    procedure QrCadAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FForm: TForm;
    FDataBase: TmySQLDatabase;
    FListaNiveis: TAppGrupLst;
    FNomTabCad, FNomTabGru: String;
    FNovoCodigo: TNovoCodigo;
    //
    function CriaOForm: TForm;
    procedure ReopenGru(Codigo: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCadItemGrupo: TFmCadItemGrupo;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal, DmkDAC_PF, CfgCadLista;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCadItemGrupo.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCadItemGrupo.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCadItemGrupo.DefParams;
begin
  VAR_GOTOTABELA := FNomTabCad;
  VAR_GOTOMYSQLTABLE := QrCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := FDataBase;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  if FNomTabGru <> '' then
  begin
    VAR_SQLx.Add('SELECT gru.Nome NO_NIVSUP, cad.*');
    VAR_SQLx.Add('FROM ' + FNomTabCad + ' cad');
    VAR_SQLx.Add('LEFT JOIN ' + FNomTabGru + ' gru ON gru.Codigo=cad.NivSup');
  end else
  begin
    VAR_SQLx.Add('SELECT "N�o tem" NO_NIVSUP, cad.*');
    VAR_SQLx.Add('FROM ' + FNomTabCad + ' cad');
  end;
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE cad.Codigo > 0');
  //
  VAR_SQL1.Add('AND cad.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cad.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cad.Nome LIKE :P0');
  //
end;

function TFmCadItemGrupo.CriaOForm: TForm;
begin
  Result := Self;
  //
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCadItemGrupo.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCadItemGrupo.ReopenGru(Codigo: Integer);
var
  C: Integer;
begin
  if Codigo <> 0 then
    C := Codigo
  else
    C := EdNivSup.ValueVariant;
  EdNivSup.ValueVariant := 0;
  CBNivSup.KeyValue := 0;
  //
  EdNivSup.Enabled := False;
  CBNivSup.Enabled := False;
  SbNivSup.Enabled := False;
  //
  QrGru.Close;
  if FNomTabGru <> '' then
  begin
    EdNivSup.Enabled := True;
    CBNivSup.Enabled := True;
    SbNivSup.Enabled := True;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrGru, FDatabase, [
    'SELECT Codigo, Nome ',
    'FROM ' + FNomTabGru,
    'ORDER BY Nome ',
    '']);
    //
    if QrGru.Locate('Codigo', C, []) then
    begin
      EdNivSup.ValueVariant := C;
      CBNivSup.KeyValue := C;
    end;
  end else
  begin
  end;
end;

procedure TFmCadItemGrupo.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCadItemGrupo.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCadItemGrupo.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCadItemGrupo.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCadItemGrupo.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCadItemGrupo.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCadItemGrupo.SbNivSupClick(Sender: TObject);
begin
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, FNomTabGru,
    FListaNiveis, FNovoCodigo);
  UMyMod.SetaCodigoPesquisado(EdNivSup, CBNivSup, QrGru, VAR_CADASTRO);
end;

procedure TFmCadItemGrupo.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCadItemGrupo.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, FNomTabCad);
end;

procedure TFmCadItemGrupo.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCadCodigo.Value;
  Close;
end;

procedure TFmCadItemGrupo.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  NivSup, Codigo, Ordem: Integer;
begin
  NivSup         := EdNivSup.ValueVariant;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32(FNomTabCad, 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrCadCodigo.Value);
(*
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    FNomTabCad, Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
*)
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FNomTabCad, False, [
  'NivSup', 'Nome', 'Ordem'], [
  'Codigo'], [
  NivSup, Nome, Ordem], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if CkContinuar.Checked then
      BtIncluiClick(BtInclui);
  end;
end;

procedure TFmCadItemGrupo.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, FDataBase, FNomTabCad, 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCadItemGrupo.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCad, [PnDados],
    [PnEdita], EdNome, ImgTipo, FNomTabCad);
  //
  EdOrdem.ValueVariant := 0;
end;

procedure TFmCadItemGrupo.BtNivelAcimaClick(Sender: TObject);
begin
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, FNomTabGru,
    FListaNiveis, ncGerlSeq1);
end;

procedure TFmCadItemGrupo.FormCreate(Sender: TObject);
begin
  FNomTabCad := '';
  FNomTabGru := '';
  //
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
end;

procedure TFmCadItemGrupo.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCadItemGrupo.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCadItemGrupo.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCadItemGrupo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCadItemGrupo.QrCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCadItemGrupo.QrCadAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrCadCodigo.Value <> 0;
  BtAltera.Enabled := Habilita;
  //BtExclui.Enabled := Habilita;
end;

procedure TFmCadItemGrupo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCadItemGrupo.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, FNomTabCad, FDataBase, CO_VAZIO));
end;

procedure TFmCadItemGrupo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCadItemGrupo.QrCadBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmCadItemGrupo.QrCadBeforeOpen(DataSet: TDataSet);
begin
  QrCadCodigo.DisplayFormat := FFormatFloat;
  //
  BtNivelAcima.Enabled := True;
  BtNivelAcima.Visible := FNomTabGru <> '';
end;

procedure TFmCadItemGrupo.QrCadBeforePost(DataSet: TDataSet);
begin
  BtNivelAcima.Enabled := False;
end;

end.

