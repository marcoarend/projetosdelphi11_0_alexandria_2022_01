object FmDmkSVGEditor: TFmDmkSVGEditor
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: ??? ??? ???'
  ClientHeight = 609
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnClick = FormClick
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 812
    Height = 38
    AutoSize = True
    ButtonHeight = 38
    ButtonWidth = 39
    Caption = 'ToolBar1'
    Images = FmMyGlyfs.Lista_32X32_Textos
    TabOrder = 0
    object TTBAbrir: TToolButton
      Left = 0
      Top = 0
      Hint = 'Abre arquivo existente'
      Caption = 'Open'
      ImageIndex = 1
      ParentShowHint = False
      ShowHint = True
      OnClick = TTBAbrirClick
    end
    object ToolButton5: TToolButton
      Left = 39
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object TTBPainel: TToolButton
      Left = 47
      Top = 0
      Caption = 'Mostra painel dados'
      ImageIndex = 4
      Style = tbsCheck
      OnClick = TTBPainelClick
    end
    object ToolButton1: TToolButton
      Left = 86
      Top = 0
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 28
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 94
      Top = 0
      Caption = 'ToolButton2'
      ImageIndex = 28
      OnClick = ToolButton2Click
    end
    object ToolButton3: TToolButton
      Left = 133
      Top = 0
      Caption = 'ToolButton3'
      ImageIndex = 27
      OnClick = ToolButton3Click
    end
    object ToolButton4: TToolButton
      Left = 172
      Top = 0
      Caption = 'ToolButton4'
      ImageIndex = 28
      OnClick = ToolButton4Click
    end
  end
  object PnDados: TPanel
    Left = 609
    Top = 38
    Width = 203
    Height = 571
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 31
    ExplicitHeight = 578
    object PCDados: TPageControl
      Left = 1
      Top = 1
      Width = 201
      Height = 569
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 576
      object TabSheet3: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Croqui'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object TVSVGImg: TTreeView
          Left = 0
          Top = 26
          Width = 193
          Height = 421
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          HideSelection = False
          Indent = 19
          ParentFont = False
          TabOrder = 0
          OnChange = TVSVGImgChange
          ExplicitTop = 21
          ExplicitWidth = 195
          ExplicitHeight = 435
        end
        object Edit1: TEdit
          Left = 0
          Top = 0
          Width = 193
          Height = 26
          Align = alTop
          TabOrder = 1
          TextHint = 'Nome do ID para pesquisar'
          OnChange = Edit1Change
          ExplicitWidth = 195
        end
        object Panel4: TPanel
          Left = 0
          Top = 447
          Width = 193
          Height = 93
          Align = alBottom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ExplicitTop = 456
          ExplicitWidth = 195
          object LaEscala: TLabel
            Left = 16
            Top = 46
            Width = 36
            Height = 14
            Caption = 'Escala:'
          end
          object BtLimpar: TButton
            Left = 16
            Top = 7
            Width = 163
            Height = 39
            Caption = 'Limpar sele'#231#227'o'
            TabOrder = 0
            OnClick = BtLimparClick
          end
          object TBEscala: TTrackBar
            Left = 16
            Top = 61
            Width = 163
            Height = 28
            Max = 100
            Min = 1
            Position = 10
            TabOrder = 1
            OnChange = TBEscalaChange
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Objetos'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object TVObjetos: TListView
          Left = 0
          Top = 0
          Width = 195
          Height = 549
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Align = alClient
          Columns = <>
          TabOrder = 0
          OnDblClick = TVObjetosDblClick
        end
      end
    end
  end
  object CPGPropriedades: TCategoryPanelGroup
    Left = 397
    Top = 38
    Width = 212
    Height = 571
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    VertScrollBar.Tracking = True
    Align = alRight
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -13
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = []
    TabOrder = 2
    OnResize = CPGPropriedadesResize
    ExplicitTop = 31
    ExplicitHeight = 578
    object CPnPropriedades: TCategoryPanel
      Top = 345
      Height = 163
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Propriedades'
      TabOrder = 0
      object PnObjetos: TPanel
        Left = 0
        Top = 0
        Width = 208
        Height = 137
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 141
        object Label154: TLabel
          Left = 8
          Top = 11
          Width = 10
          Height = 13
          Caption = 'X:'
          FocusControl = EdX
        end
        object Label1: TLabel
          Left = 8
          Top = 36
          Width = 10
          Height = 13
          Caption = 'Y:'
          FocusControl = EdY
        end
        object Label2: TLabel
          Left = 8
          Top = 61
          Width = 30
          Height = 13
          Caption = 'Altura:'
          FocusControl = EdAltura
        end
        object Label3: TLabel
          Left = 8
          Top = 84
          Width = 39
          Height = 13
          Caption = 'Largura:'
          FocusControl = EdLargura
        end
        object Label4: TLabel
          Left = 8
          Top = 111
          Width = 43
          Height = 13
          Caption = 'Unidade:'
          FocusControl = EdLargura
        end
        object EdX: TdmkEdit
          Left = 65
          Top = 8
          Width = 98
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Profissao'
          UpdCampo = 'Profissao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdY: TdmkEdit
          Left = 65
          Top = 33
          Width = 98
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Profissao'
          UpdCampo = 'Profissao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAltura: TdmkEdit
          Left = 65
          Top = 59
          Width = 98
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Profissao'
          UpdCampo = 'Profissao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLargura: TdmkEdit
          Left = 65
          Top = 84
          Width = 98
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Profissao'
          UpdCampo = 'Profissao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CBUnidade: TComboBox
          Left = 65
          Top = 109
          Width = 98
          Height = 21
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 4
          Text = 'Pixel'
          Items.Strings = (
            'Pixel'
            'Cm')
        end
      end
    end
    object CPnObjetos: TCategoryPanel
      Top = 0
      Height = 345
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Objetos'
      TabOrder = 1
      object LVObjetos: TListView
        Left = 0
        Top = 0
        Width = 208
        Height = 324
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
        Columns = <>
        LargeImages = ILObj
        ReadOnly = True
        TabOrder = 0
        OnDblClick = LVObjetosDblClick
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 38
    Width = 397
    Height = 571
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    ExplicitTop = 31
    ExplicitHeight = 578
    object TabSheet1: TTabSheet
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Croqui'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet2: TTabSheet
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'Paint Box'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PaintBox1: TPaintBox
        Left = 0
        Top = 0
        Width = 391
        Height = 553
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
      end
    end
  end
  object SBCroqui: TScrollBox
    Left = 0
    Top = 38
    Width = 397
    Height = 571
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alClient
    TabOrder = 4
    ExplicitTop = 31
    ExplicitHeight = 578
    object LaCarregando: TLabel
      Left = 0
      Top = 0
      Width = 393
      Height = 29
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Align = alTop
      Alignment = taCenter
      Caption = 'Carregando! Aguarde...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -25
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 249
    end
  end
  object MainMenu1: TMainMenu
    Left = 136
    Top = 160
    object Arquivo1: TMenuItem
      Caption = 'Arquivo'
      object Abrir1: TMenuItem
        Caption = 'Abrir'
        OnClick = Abrir1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Fechar1: TMenuItem
        Caption = 'Fechar'
        OnClick = Fechar1Click
      end
    end
  end
  object RSSVGDocument1: TRSSVGDocument
    DefaultTextRendering = txrnOptimizeLegibility
    PreferredLanguage = 'EN'
    Left = 328
    Top = 168
  end
  object ILObj: TImageList
    Height = 32
    Width = 32
    Left = 128
    Top = 360
  end
end
