unit UnDmkImg;

interface

uses Forms, Controls, Windows, SysUtils, ComCtrls, ExtCtrls, Dialogs,
  Vcl.StdCtrls, System.Classes, Vcl.Graphics, System.Math, Vcl.Imaging.GIFImg,
  Vcl.Imaging.jpeg, Vcl.Imaging.pngimage, ShellApi,
  Soap.EncdDecd,
  System.NetEncoding,
  //FMX.Surfaces, FMX.Graphics,
  UnDmkEnums, DmkGeral;

const
  PixelMax = 32768;

type
  TPixelArray = array[0..PixelMax-1] Of TRGBTriple;
  pPixelArray = ^TPixelArray;
  TRGBArray = array[Word] of TRGBTriple;
  pRGBArray = ^TRGBArray;
  //
  PixArray = array [0..2] of Byte;
  //
  TUnDmkImg = class(TObject)
  private

  public
    function  SalvaImgJPEGEmDisco(Caminho, NomeArq: String; Foto: TJPEGImage;
              DelSeExiste: Boolean = False): String;
    function  CapturaImagemDaInterfaceDeComponente(const WinControl:
              TWinControl; const FormaCopiaInterfaceCompo:
              TFormaCopiaInterfaceCompo; var BMP: TBitmap): Boolean;
    function  FlipReverse(const Flip, Reverse: Boolean; const Bitmap: TBitmap;
              const FormaFlipReverseBmp: TFormaFlipReverseBmp): TBitmap;
    function  ValidaExtensao(Imagem: String; var Ext: String): Boolean;
    function  RedimencionaImgMiniatura(Imagem: String): TGraphic;
    procedure GetFileIcon(Arquivo: String; var Image: TImage);
    //
    procedure ResizeImage_Bitmap(var BitMap: TBitmap; const MaxWidth, MaxHeight: Integer);
    procedure ResizeImage_Bitmap_Novo(var BitMap: TBitmap; const MaxWidth, MaxHeight: Integer);
    procedure RotateBitmap_ads(SourceBitmap: TBitmap; out DestBitmap: TBitmap;
              Center: TPoint; Angle: Double) ;

    function  RotateScanLine90(const Angle: Integer; const Bitmap: TBitmap): TBitmap;
    procedure SmoothResize(var Src, Dst: TBitmap);
    function  TColorToHex(Cor: TColor): String;
    function  HexToTColor(Cor: String): TColor;
    // N�o testado!!!  { TODO : TESTAR }
    procedure Antialiasing(Bitmap: TBitmap; Rect: TRect; Percent: Integer);
    // N�o testado!!!  { TODO : TESTAR }
    procedure DrawAntialisedLine(Canvas: TCanvas; const AX1, AY1, AX2, AY2: real; const LineColor: TColor);
////////////////////////////////// INI BASE 64 /////////////////////////////////////////////////////////////////
///
    function  Base64FromBitmap(Bitmap: TBitmap): string;
    function  BitmapFromBase64(const Base64: string): TBitmap;
    //function  Base64FromBitmap2(Bitmap: TBitmap): string;

///
////////////////////////////////// FIM BASE 64 /////////////////////////////////////////////////////////////////
  end;

var
  DmkImg: TUnDmkImg;

implementation

{ TUnDmkImg }

function TUnDmkImg.SalvaImgJPEGEmDisco(Caminho, NomeArq: String; Foto: TJPEGImage;
  DelSeExiste: Boolean = False): String;
var
  Nome: String;
begin
  Result := '';
  //
  if not DirectoryExists(Caminho) then
  begin
    Geral.MB_Info('O diret�rio "' + Caminho + '" n�o foi localizado!' +
      sLineBreak + 'Verifique se ele existe ou se est� acess�vel e tente novamente!');
    Exit;
  end;
  Nome := Caminho + NomeArq;
  //
  if DelSeExiste then
  begin
    if FileExists(Nome) then
      DeleteFile(Nome);
  end;
  Foto.SaveToFile(Nome);
  //
  if FileExists(Nome) then
    Result := Nome;
end;

procedure TUnDmkImg.GetFileIcon(Arquivo: String; var Image: TImage);
var
  Icon: TIcon;
  FileInfo: TSHFileInfo;
begin
  FillChar(FileInfo, SizeOf(FileInfo), 0);
  SHGetFileInfo(PChar(Arquivo), FILE_ATTRIBUTE_NORMAL,
    FileInfo, SizeOf(FileInfo), SHGFI_USEFILEATTRIBUTES or SHGFI_ICON or
    SHGFI_LARGEICON);
  Icon := TIcon.Create;
  try
    Icon.Handle := FileInfo.hIcon;
    Image.Canvas.Draw(20, 20, Icon);
    //
    try
      Image.Picture.Icon := Icon;
    except
      Image.Picture := nil;
    end;
  finally
    Icon.Free;
  end;
end;

function TUnDmkImg.ValidaExtensao(Imagem: String; var Ext: String): Boolean;
const
  ExtVal = '.jpeg .jpg .png .gif .bmp';
begin
  //Extens�es suportadas
  //.jpeg / .jpg / .png / .gif / .bmp

  Ext := ExtractFileExt(Imagem);
  Ext := LowerCase(Ext);

  if Pos(Ext, ExtVal) > 0 then
    Result := True
  else
    Result := False;
end;

function TUnDmkImg.RedimencionaImgMiniatura(Imagem: String): TGraphic;
  procedure ExibeMensagem(Msg: String);
  begin
    ShowMessage(Msg);
  end;
const
  Lar = 192;
  Alt = 144;
var
  Bitmap: TBitmap;
  JpgImg: TJPEGImage;
  GifImg: TGIFImage;
  PngImg: TPngImage;
  Ext: String;
begin
  Result := nil;
  //
  if not ValidaExtensao(Imagem, Ext) then
  begin
    ExibeMensagem('Extens�o inv�lida!');
    Exit;
  end;
  if (Ext = '.jpg') or (Ext = '.jpeg') then
  begin
    try
      JpgImg := TJPEGImage.Create;
      JpgImg.CompressionQuality := 100;
      JpgImg.LoadFromFile(Imagem);
      //
      if JpgImg <> nil then
      begin
        try
          Bitmap := TBitmap.Create;
          Bitmap.Assign(JpgImg);
          //
          ResizeImage_Bitmap_Novo(Bitmap, Lar, Alt);
          JpgImg.Assign(Bitmap);
          //
          Result := JpgImg;
        finally
          Bitmap.Free;
        end;
      end;
    except
      ExibeMensagem('Falha ao redimencionar imagem!');
    end;
  end else
  if (Ext = '.gif') then
  begin
    try
      GifImg := TGIFImage.Create;
      GifImg.LoadFromFile(Imagem);
      //
      if GifImg <> nil then
      begin
        try
          Bitmap := TBitmap.Create;
          Bitmap.Assign(GifImg);
          //
          ResizeImage_Bitmap_Novo(Bitmap, Lar, Alt);
          GifImg.Assign(Bitmap);
          //
          Result := GifImg;
        finally
          Bitmap.Free;
        end;
      end;
    except
      ExibeMensagem('Falha ao redimencionar imagem!');
    end;
  end else
  if (Ext = '.png') then
  begin
    try
      PngImg := TPngImage.Create;
      PngImg.LoadFromFile(Imagem);
      //
      if PngImg <> nil then
      begin
        try
          Bitmap := TBitmap.Create;
          Bitmap.Assign(PngImg);
          //
          ResizeImage_Bitmap_Novo(Bitmap, Lar, Alt);
          PngImg.Assign(Bitmap);
          //
          Result := PngImg;
        finally
          Bitmap.Free;
        end;
      end;
    except
      ExibeMensagem('Falha ao redimencionar imagem!');
    end;
  end else
  if (Ext = '.bmp') then
  begin
    try
      Bitmap := TBitmap.Create;
      Bitmap.LoadFromFile(Imagem);
      //
      ResizeImage_Bitmap_Novo(Bitmap, Lar, Alt);
      //
      Result := Bitmap;
    except
      ExibeMensagem('Falha ao redimencionar imagem!');
    end;
  end;
end;


function TUnDmkImg.Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
begin
  Input := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Soap.EncdDecd.EncodeStream(Input, Output);
      Result := Output.DataString;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;


function TUnDmkImg.BitmapFromBase64(const Base64: string): TBitmap;
var
  Input: TStringStream;
  Output: TBytesStream;
begin
  Input := TStringStream.Create(base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Soap.EncdDecd.DecodeStream(Input, Output);
      Output.Position := 0;
      Result := TBitmap.Create;
      try
        Result.LoadFromStream(Output);
      except
        Result.Free;
        raise;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

(*
function Base64FromBitmap2(Bitmap: TBitmap): string;
  function Base64FromStream(MemStream: TMemoryStream): string;
  var
    Input: TBytesStream;
    Output: TStringStream;
    Encoding: TBase64Encoding;
  begin
    Input := TBytesStream.Create;
    try
      //Bitmap.SaveToStream(Input);
      input.LoadFromStream(MemStream);
      Input.Position := 0;
      Output := TStringStream.Create('', TEncoding.ASCII);
      try
        Encoding := TBase64Encoding.Create(0);
        try
          Encoding.Encode(Input, Output);
          Result := Output.DataString;
        finally
          Encoding.Free;
        end;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;
var
  Strm : TMemoryStream;
  Surface : TBitmapSurface;
  Input: TBytesStream;
  Output: TStringStream;
begin
  Strm := TMemoryStream.Create;
  try
    Surface := TBitmapSurface.Create;
    try
      Surface.Assign(Bitmap);
  {$IfDef MSWINDOWS}
      TBitmapCodecManager.SaveToStream(Strm, Surface, '.bmp');
  {$Else}
      TBitmapCodecManager.SaveToStream(Strm, Surface, '.jpg');
  {$EndIf}
      Result := Base64FromStream(Strm);
    finally
      Surface.Free;
    end;
    Strm.Position := 0;
    //BMP.LoadFromStream(Strm);
  finally
    Strm.Free;
  end;
end;
*)


// Fotografa Componente
// Screen Shot Componente
// Print Screen Componente
function TUnDmkImg.CapturaImagemDaInterfaceDeComponente(const WinControl:
TWinControl; const FormaCopiaInterfaceCompo: TFormaCopiaInterfaceCompo; var BMP:
TBitmap): Boolean;
  function PrintControl(AControl: TWinControl; AOut: TBitmap): Boolean;
  var
    DC: HDC;
  begin
    Result := False;
    if not Assigned(AOut) then
      Exit;
    if not Assigned(AControl) then
      Exit;
    //
    DC := GetWindowDC(AControl.Handle);
    AOut.Width  := AControl.Width;
    AOut.Height := AControl.Height;
    with AOut do
    BitBlt(
      Canvas.Handle, 0, 0, Width, Height, DC, 0, 0, SrcCopy
    );
    //
    ReleaseDC(AControl.Handle, DC);
    Result := AOut <> nil;
  end;
  //
  function Control2Bitmap(const Control_: TWinControl; var OK:
  Boolean): TBitmap;
  begin
    Result := TBitmap.Create;
    with Result do
    begin
      Height := Control_.Height;
      Width  := Control_.Width;
      Canvas.Handle := CreateDC(nil, nil, nil, nil);
      Canvas.Lock;
      Control_.PaintTo(Canvas.Handle, 0, 0);
      Canvas.Unlock;
      DeleteDC(Canvas.Handle);
    end;
    OK := Result <> nil;
  end;
begin
  case FormaCopiaInterfaceCompo of
    fcicBitBlt: Result := PrintControl(WinControl, BMP);
    fcicPaintTo: BMP := Control2Bitmap(WinControl, Result);
    else Geral.MB_Erro(
    '"FormaCopiaInterfaceCompo" n�o implementado em TUnDmkImg.CapturaImagemDaInterfaceDeComponente()');
  end;
end;

procedure TUnDmkImg.DrawAntialisedLine(Canvas: TCanvas; const AX1, AY1, AX2,
  AY2: real; const LineColor: TColor);
var
  swapped: boolean;

  procedure plot(const x, y, c: real);
  var
    resclr: TColor;
  begin
    if swapped then
      resclr := Canvas.Pixels[round(y), round(x)]
    else
      resclr := Canvas.Pixels[round(x), round(y)];
    resclr := RGB(round(GetRValue(resclr) * (1-c) + GetRValue(LineColor) * c),
                  round(GetGValue(resclr) * (1-c) + GetGValue(LineColor) * c),
                  round(GetBValue(resclr) * (1-c) + GetBValue(LineColor) * c));
    if swapped then
      Canvas.Pixels[round(y), round(x)] := resclr
    else
      Canvas.Pixels[round(x), round(y)] := resclr;
  end;

  function rfrac(const x: real): real; inline;
  begin
    rfrac := 1 - frac(x);
  end;

  procedure swap(var a, b: real);
  var
    tmp: real;
  begin
    tmp := a;
    a := b;
    b := tmp;
  end;

var
  x1, x2, y1, y2, dx, dy, gradient, xend, yend, xgap, xpxl1, ypxl1,
  xpxl2, ypxl2, intery: real;
  x: integer;

begin

  x1 := AX1;
  x2 := AX2;
  y1 := AY1;
  y2 := AY2;

  dx := x2 - x1;
  dy := y2 - y1;
  swapped := abs(dx) < abs(dy);
  if swapped then
  begin
    swap(x1, y1);
    swap(x2, y2);
    swap(dx, dy);
  end;
  if x2 < x1 then
  begin
    swap(x1, x2);
    swap(y1, y2);
  end;

  gradient := dy / dx;

  xend := round(x1);
  yend := y1 + gradient * (xend - x1);
  xgap := rfrac(x1 + 0.5);
  xpxl1 := xend;
  ypxl1 := floor(yend);
  plot(xpxl1, ypxl1, rfrac(yend) * xgap);
  plot(xpxl1, ypxl1 + 1, frac(yend) * xgap);
  intery := yend + gradient;

  xend := round(x2);
  yend := y2 + gradient * (xend - x2);
  xgap := frac(x2 + 0.5);
  xpxl2 := xend;
  ypxl2 := floor(yend);
  plot(xpxl2, ypxl2, rfrac(yend) * xgap);
  plot(xpxl2, ypxl2 + 1, frac(yend) * xgap);

  for x := round(xpxl1) + 1 to round(xpxl2) - 1 do
  begin
    plot(x, floor(intery), rfrac(intery));
    plot(x, floor(intery) + 1, frac(intery));
    intery := intery + gradient;
  end;
end;

function TUnDmkImg.FlipReverse(const Flip, Reverse: Boolean; const Bitmap:
TBitmap; const FormaFlipReverseBmp: TFormaFlipReverseBmp): TBitmap;

  function FlipReverseScanLine(const Flip, Reverse: Boolean; const Bitmap:
  TBitmap): TBitmap;
  var
    i     :  integer;
    j     :  integer;
    RowIn :  pRGBArray;
    RowOut:  pRGBArray;
  begin
    if   Bitmap.PixelFormat <> pf24bit then
      Geral.MB_Erro('Can Flip/Reverse only 24-bit bitmap');
    //
    Result := TBitmap.Create;
    Result.Width       := Bitmap.Width;
    Result.Height      := Bitmap.Height;
    Result.PixelFormat := Bitmap.PixelFormat;

    for j := 0 to Bitmap.Height-1 do
    begin
      RowIn := Bitmap.Scanline[j];
      if   Flip
      then RowOut := Result.Scanline[Bitmap.Height - 1 - j]
      else RowOut := Result.Scanline[j];

      // Optimization technique:  Use two for loops so if is outside of inner loop
      if Reverse then
      begin
        for i := 0 to Bitmap.Width-1 do
          RowOut[i] := RowIn[Bitmap.Width-1-i]
      end
      else
      begin
        for i := 0 to Bitmap.Width-1 do
          RowOut[i] := RowIn[i]
      end
    end
  end {FlipReverseScanLine};


  //////////////////////////////////////////////////////////////////////////////

  // This function implements a suggestion by David Ullrich in a July 25, 1997
  // post to comp.lang.pascal.delphi.misc.
  //
  // The Graphics.PAS unit shows that CopyRect calls the Windows StretchBlt API
  // function.
  function FlipReverseCopyRect(const Flip, Reverse: Boolean; const Bitmap:
  TBitmap):  TBitmap;
  var
    Bottom:  integer;
    Left  :  integer;
    Right :  integer;
    Top   :  integer;
  begin
    Result := TBitmap.Create;
    Result.Width       := Bitmap.Width;
    Result.Height      := Bitmap.Height;
    Result.PixelFormat := Bitmap.PixelFormat;

    // Flip Top to Bottom
    if Flip then
    begin
      // Unclear why extra "-1" is needed here.
      Top    := Bitmap.Height-1;
      Bottom := -1
    end
    else begin
      Top    := 0;
      Bottom := Bitmap.Height
    end;

    // Reverse Left to Right
    if  Reverse
    then begin
      // Unclear why extra "-1" is needed here.
      Left  := Bitmap.Width-1;
      Right := -1;
    end
    else begin
      Left  := 0;
      Right := Bitmap.Width;
    end;

    Result.Canvas.CopyRect(Rect(Left,Top, Right,Bottom),
                           Bitmap.Canvas,
                           Rect(0,0, Bitmap.Width,Bitmap.Height));
  end {FlipReverseCopyRect};


  //////////////////////////////////////////////////////////////////////////////

  function FlipReverseStretchBlt(const Flip, Reverse:  Boolean; const Bitmap:
  TBitmap):  TBitmap;
  var
    Bottom:  integer;
    Left  :  integer;
    Right :  integer;
    Top   :  integer;
  begin
    Result := TBitmap.Create;
    Result.Width       := Bitmap.Width;
    Result.Height      := Bitmap.Height;
    Result.PixelFormat := Bitmap.PixelFormat;

    // Flip Top to Bottom
    if  Flip
    then begin
      // Unclear why extra "-1" is needed here.
      Top    := Bitmap.Height-1;
      Bottom := -1
    end
    else begin
      Top    := 0;
      Bottom := Bitmap.Height
    end;

    // Reverse Left to Right
    if  Reverse
    then begin
      // Unclear why extra "-1" is needed here.
      Left  := Bitmap.Width-1;
      Right := -1;
    end
    else begin
      Left  := 0;
      Right := Bitmap.Width;
    end;

    StretchBlt(Result.Canvas.Handle, Left, Top, Right-Left, Bottom-Top,
               Bitmap.Canvas.Handle,
               0,0, Bitmap.Width, Bitmap.Height, cmSrcCopy);
  end {FlipReverseStretchBlt};


begin
  case FormaFlipReverseBmp of
    ffrbScanLine:   Result := FlipReverseScanLine(Flip, Reverse, Bitmap);
    ffrbCopyRect:   Result := FlipReverseCopyRect(Flip, Reverse, Bitmap);
    ffrbStretchBlt: Result := FlipReverseStretchBlt(Flip, Reverse, Bitmap);
    else Geral.MB_Erro(
    '"FormaFlipReverseBmp" n�o definido em "TUnDmkImg.FlipReverse("');
  end;
end;

function TUnDmkImg.HexToTColor(Cor: String): TColor;
begin
  Result := RGB(StrToInt('$' + Copy(Cor, 1, 2)),
              StrToInt('$' + Copy(Cor, 3, 2)), StrToInt('$' + Copy(Cor, 5, 2)));
end;

procedure TUnDmkImg.Antialiasing(Bitmap: TBitmap; Rect: TRect;
  Percent: Integer);
var
  pix, prevscan, nextscan, hpix: ^PixArray;
  l, p: Integer;
  R, G, B: Integer;
  R1, R2, G1, G2, B1, B2: Byte;
begin
  Bitmap.PixelFormat := pf24bit;
  with Bitmap.Canvas do
  begin
    Brush.Style := bsclear;
    for l := Rect.Top to Rect.Bottom - 1 do
    begin
      pix:= Bitmap.ScanLine[l];
      if l <> Rect.Top then prevscan := Bitmap.ScanLine[l-1]
      else prevscan := nil;
      if l <> Rect.Bottom - 1 then nextscan := Bitmap.ScanLine[l+1]
      else nextscan := nil;

      for p := Rect.Left to Rect.Right - 1 do
      begin
        R1 := pix^[2];
        G1 := pix^[1];
        B1 := pix^[0];

        if p <> Rect.Left then
        begin
          //Pixel links
          //Pixel left

          hpix := pix;
          dec(hpix);
          R2 := hpix^[2];
          G2 := hpix^[1];
          B2 := hpix^[0];

          if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
          begin
            R := R1 + (R2 - R1) * 50 div (Percent + 50);
            G := G1 + (G2 - G1) * 50 div (Percent + 50);
            B := B1 + (B2 - B1) * 50 div (Percent + 50);
            hpix^[2] := R;
            hpix^[1] := G;
            hpix^[0] := B;
          end;
        end;

        if p <> Rect.Right - 1 then
        begin
          //Pixel rechts
          //Pixel right
          hpix := pix;
          inc(hpix);
          R2 := hpix^[2];
          G2 := hpix^[1];
          B2 := hpix^[0];

          if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
          begin
            R := R1 + (R2 - R1) * 50 div (Percent + 50);
            G := G1 + (G2 - G1) * 50 div (Percent + 50);
            B := B1 + (B2 - B1) * 50 div (Percent + 50);
            hpix^[2] := R;
            hpix^[1] := G;
            hpix^[0] := B;
          end;
        end;

        if prevscan <> nil then
        begin
          //Pixel oben
          //Pixel up
          R2 := prevscan^[2];
          G2 := prevscan^[1];
          B2 := prevscan^[0];

          if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
          begin
            R := R1 + (R2 - R1) * 50 div (Percent + 50);
            G := G1 + (G2 - G1) * 50 div (Percent + 50);
            B := B1 + (B2 - B1) * 50 div (Percent + 50);
            prevscan^[2] := R;
            prevscan^[1] := G;
            prevscan^[0] := B;
          end;
          Inc(prevscan);
        end;

        if nextscan <> nil then
        begin
          //Pixel unten
          //Pixel down
          R2 := nextscan^[2];
          G2 := nextscan^[1];
          B2 := nextscan^[0];

          if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
          begin
            R := R1 + (R2 - R1) * 50 div (Percent + 50);
            G := G1 + (G2 - G1) * 50 div (Percent + 50);
            B := B1 + (B2 - B1) * 50 div (Percent + 50);
            nextscan^[2] := R;
            nextscan^[1] := G;
            nextscan^[0] := B;
          end;
          Inc(nextscan);
        end;
        Inc(pix);
      end;
    end;
  end;
end;

procedure TUnDmkImg.SmoothResize(var Src, Dst: TBitmap);
var
  x, y: Integer;
  xP, yP: Integer;
  xP2, yP2: Integer;
  SrcLine1, SrcLine2: pRGBArray;
  t3: Integer;
  z, z2, iz2: Integer;
  DstLine: pRGBArray;
  DstGap: Integer;
  w1, w2, w3, w4: Integer;
begin
  Src.PixelFormat := pf24Bit;
  Dst.PixelFormat := pf24Bit;

  if (Src.Width = Dst.Width) and (Src.Height = Dst.Height) then
    Dst.Assign(Src)
  else
  begin
    DstLine := Dst.ScanLine[0];
    DstGap := Integer(Dst.ScanLine[1]) - Integer(DstLine);

    xP2 := MulDiv(pred(Src.Width), $10000, Dst.Width);
    yP2 := MulDiv(pred(Src.Height), $10000, Dst.Height);
    yP := 0;

    for y := 0 to pred(Dst.Height) do
    begin
      xP := 0;

      SrcLine1 := Src.ScanLine[yP shr 16];

      if (yP shr 16 < pred(Src.Height)) then
        SrcLine2 := Src.ScanLine[succ(yP shr 16)]
      else
        SrcLine2 := Src.ScanLine[yP shr 16];

      z2 := succ(yP and $FFFF);
      iz2 := succ((not yp) and $FFFF);
      for x := 0 to pred(Dst.Width) do begin
        t3 := xP shr 16;
        z := xP and $FFFF;
        w2 := MulDiv(z, iz2, $10000);
        w1 := iz2 - w2;
        w4 := MulDiv(z, z2, $10000);
        w3 := z2 - w4;
        DstLine[x].rgbtRed := (SrcLine1[t3].rgbtRed * w1 +
          SrcLine1[t3 + 1].rgbtRed * w2 +
          SrcLine2[t3].rgbtRed * w3 + SrcLine2[t3 + 1].rgbtRed * w4) shr 16;
        DstLine[x].rgbtGreen :=
          (SrcLine1[t3].rgbtGreen * w1 + SrcLine1[t3 + 1].rgbtGreen * w2 +

          SrcLine2[t3].rgbtGreen * w3 + SrcLine2[t3 + 1].rgbtGreen * w4) shr 16;
        DstLine[x].rgbtBlue := (SrcLine1[t3].rgbtBlue * w1 +
          SrcLine1[t3 + 1].rgbtBlue * w2 +
          SrcLine2[t3].rgbtBlue * w3 +
          SrcLine2[t3 + 1].rgbtBlue * w4) shr 16;
        Inc(xP, xP2);
      end;
      Inc(yP, yP2);
      DstLine := pRGBArray(Integer(DstLine) + DstGap);
    end;
  end;
end;

function TUnDmkImg.TColorToHex(Cor: TColor): String;
begin
  Result :=
    IntToHex(GetRValue(Cor), 2) +
    IntToHex(GetGValue(Cor), 2) +
    IntToHex(GetBValue(Cor), 2) ;
end;

procedure TUnDmkImg.RotateBitmap_ads(SourceBitmap: TBitmap;
  out DestBitmap: TBitmap; Center: TPoint; Angle: Double);
Var
   cosRadians : Double;
   inX : Integer;
   inXOriginal : Integer;
   inXPrime : Integer;
   inXPrimeRotated : Integer;
   inY : Integer;
   inYOriginal : Integer;
   inYPrime : Integer;
   inYPrimeRotated : Integer;
   OriginalRow : pPixelArray;
   Radians : Double;
   RotatedRow : pPixelArray;
   sinRadians : Double;
begin
   DestBitmap.Width := SourceBitmap.Width;
   DestBitmap.Height := SourceBitmap.Height;
   DestBitmap.PixelFormat := pf24bit;
   Radians := -(Angle) * PI / 180;
   sinRadians := Sin(Radians) ;
   cosRadians := Cos(Radians) ;
   For inX := DestBitmap.Height-1 Downto 0 Do
   Begin
     RotatedRow := DestBitmap.Scanline[inX];
     inXPrime := 2*(inX - Center.y) + 1;
     For inY := DestBitmap.Width-1 Downto 0 Do
     Begin
       inYPrime := 2*(inY - Center.x) + 1;
       inYPrimeRotated := Round(inYPrime * CosRadians - inXPrime * sinRadians) ;
       inXPrimeRotated := Round(inYPrime * sinRadians + inXPrime * cosRadians) ;
       inYOriginal := (inYPrimeRotated - 1) Div 2 + Center.x;
       inXOriginal := (inXPrimeRotated - 1) Div 2 + Center.y;
       If
         (inYOriginal >= 0) And
         (inYOriginal <= SourceBitmap.Width-1) And
         (inXOriginal >= 0) And
         (inXOriginal <= SourceBitmap.Height-1)
       then
       begin
         OriginalRow := SourceBitmap.Scanline[inXOriginal];
         RotatedRow[inY] := OriginalRow[inYOriginal]
       end
       else
       begin
         RotatedRow[inY].rgbtBlue := 255;
         RotatedRow[inY].rgbtGreen := 0;
         RotatedRow[inY].rgbtRed := 0
       end;
     end;
   end;
end;

  // Rotate 24-bits/pixel Bitmap any multiple of 90 degrees.
function TUnDmkImg.RotateScanLine90(const Angle: integer;
  const Bitmap: TBitmap): TBitmap;
    // These four internal functions parallel the four cases in rotating a
    // bitmap using the Pixels property.  See the RotatePixels example on
    // the Image Processing page of efg's Computer Lab for an example of the
    // use of the Pixels property (which is very slow).

    // A Bitmap.Assign could be used for a simple copy.  A complete example
    // using ScanLine is included here to help explain the other three cases.
  function SimpleCopy: TBitmap;
    var
      i     :  integer;
      j     :  integer;
      rowIn :  pRGBArray;
      rowOut:  pRGBArray;
  begin
    Result := TBitmap.Create;
    Result.Width  := Bitmap.Width;
    Result.Height := Bitmap.Height;
    Result.Pixelformat := Bitmap.Pixelformat;    // only pf24bit for now

    // Out[i, j] = In[i, j]

    for j := 0 to Bitmap.Height - 1 do
    begin
      rowIn  := Bitmap.ScanLine[j];
      rowOut := Result.ScanLine[j];

      // Could optimize the following by using a function like CopyMemory
      // from the Windows unit.
      for i := 0 to Bitmap.Width - 1 do
      begin
        // Why does this crash with RowOut[i] := RowIn[i]?  Alignment?
        // Use this longer form as workaround.
        WITH rowOut[i] do
        begin
          rgbtRed   := rowIn[i].rgbtRed;
          rgbtGreen := rowIn[i].rgbtGreen;
          rgbtBlue  := rowIn[i].rgbtBlue;
        end
      end
    end
  end (*SimpleCopy*);


  function Rotate90DegreesCounterClockwise:  TBitmap;
     var
      i     :  integer;
      j     :  integer;
      rowIn :  pRGBArray;
  begin
    Result := TBitmap.Create;
    Result.Width  := Bitmap.Height;
    Result.Height := Bitmap.Width;
    Result.Pixelformat := Bitmap.Pixelformat;    // only pf24bit for now

    // Out[j, Right - i - 1] = In[i, j]
    for  j := 0 TO Bitmap.Height - 1 do
    begin
      rowIn  := Bitmap.ScanLine[j];
      for i := 0 TO Bitmap.Width - 1 do
        pRGBArray(Result.ScanLine[Bitmap.Width - i - 1])[j] := rowIn[i]
    end
  end (*Rotate90DegreesCounterClockwise*);


  // Could use Rotate90DegreesCounterClockwise twice to get a
  // Rotate180DegreesCounterClockwise.  Rotating 180 degrees is the same
  // as a Flip and Reverse
  function Rotate180DegreesCounterClockwise:  TBitmap;
    var
      i     :  integer;
      j     :  integer;
      rowIn :  pRGBArray;
      rowOut:  pRGBArray;
  begin
    Result := TBitmap.Create;
    Result.Width  := Bitmap.Width;
    Result.Height := Bitmap.Height;
    Result.Pixelformat := Bitmap.Pixelformat;    // only pf24bit for now

    // Out[Right - i - 1, Bottom - j - 1] = In[i, j]
    for  j := 0 TO Bitmap.Height - 1 do
    begin
      rowIn  := Bitmap.ScanLine[j];
      rowOut := Result.ScanLine[Bitmap.Height - j - 1];
      for i := 0 TO Bitmap.Width - 1 do
        rowOut[Bitmap.Width - i - 1] := rowIn[i]
    end

  end (*Rotate180DegreesCounterClockwise*);


  // Could use Rotate90DegreesCounterClockwise three times to get a
  // Rotate270DegreesCounterClockwise
  function Rotate270DegreesCounterClockwise:  TBitmap;
    var
      i    :  integer;
      j    :  integer;
      rowIn:  pRGBArray;
  begin
    Result := TBitmap.Create;
    Result.Width  := Bitmap.Height;
    Result.Height := Bitmap.Width;
    Result.Pixelformat := Bitmap.Pixelformat;    // only pf24bit for now

    // Out[Bottom - j - 1, i] = In[i, j]
    for  j := 0 TO Bitmap.Height - 1 do
    begin
      rowIn  := Bitmap.ScanLine[j];
      for i := 0 TO Bitmap.Width - 1 do
        pRGBARray(Result.Scanline[i])[Bitmap.Height - j - 1] := rowIn[i]
    end
  end (*Rotate270DegreesCounterClockwise*);


begin
  if Bitmap.PixelFormat <> pf24bit then
    Geral.MB_Erro('Apenas bitmaps 24-bit podem ser rotacionados!');

  if (angle >= 0) and (angle mod 90 <> 0) then
    Geral.MB_Erro('O Angulo deve ser positivo e multiplo de 90 graus!');

  case (angle div 90) mod 4 of
    0:  Result := SimpleCopy;
    1:  Result := Rotate90DegreesCounterClockwise;  // Anticlockwise for the Brits
    2:  Result := Rotate180DegreesCounterClockwise;
    3:  Result := Rotate270DegreesCounterClockwise
    else
      Result := nil    // avoid compiler warning
  end;
end; //RotateScanline90*

procedure TUnDmkImg.ResizeImage_Bitmap(var BitMap: TBitmap; const
  MaxWidth, MaxHeight: Integer);
var
  NewBitmap: TBitmap;
  FatorW, FatorH: Double;
begin
  NewBitmap := TBitmap.Create;
  try
    if MaxWidth > 0 then
      FatorW := Bitmap.Width / MaxWidth
    else FatorW := 1;
    //
    if MaxHeight > 0 then
      FatorH := Bitmap.Height / MaxHeight
    else FatorH := 1;
    //
    if (FatorW >= FatorH) and (FatorW > 1) then
    begin
      NewBitmap.Width := MaxWidth;
      NewBitmap.Height := MulDiv(MaxWidth, Bitmap.Height, Bitmap.Width);
      SmoothResize(Bitmap, NewBitmap);
      BitMap.Assign(NewBitmap);
    end else
    if (FatorH >= FatorW) and (FatorH > 1) then
    begin
      NewBitmap.Height := MaxHeight;
      NewBitmap.Width := MulDiv(MaxHeight, Bitmap.Width, Bitmap.Height);
      SmoothResize(Bitmap, NewBitmap);
      BitMap.Assign(NewBitmap);
    end;
  finally
    NewBitmap.Free;
  end;
end;

procedure TUnDmkImg.ResizeImage_Bitmap_Novo(var BitMap: TBitmap;
  const MaxWidth, MaxHeight: Integer);
var
  NewBitmap: TBitmap;
begin
  begin
    NewBitmap := TBitmap.Create;
    try
      if MaxWidth > MaxHeight then
      begin
        NewBitmap.Width  := MaxWidth;
        NewBitmap.Height := MulDiv(MaxWidth, BitMap.Height, BitMap.Width);
      end else
      begin
        NewBitmap.Height := MaxHeight;
        NewBitmap.Width  := MulDiv(MaxHeight, BitMap.Width, BitMap.Height);
      end;
      SmoothResize(BitMap, NewBitmap);
      BitMap.Assign(NewBitmap);
    finally
      NewBitmap.Free;
    end;
  end;
end;

end.
