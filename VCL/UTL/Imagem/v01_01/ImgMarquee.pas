unit ImgMarquee;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg, Math, UnDmkImg;

type
  TDrawFigure = (mbClear, mbLine, mbRectangle, mbEllipse, mbOff);//main menu states
  TmsEvent = (msDown, msMove, msUp);  //mouse events
  TAjusteImagem = (aiNenhum, aiAuto, aiExposicao, aiManual);
  //
  PRGB24 = ^TRGB24;
  TRGB24 = record B, G, R: Byte; end;
  PRGBArray = ^TRGBArray;
  TRGBArray = array[0..0] of TRGB24;
  THistogram = array[0..255] of Byte;

  //
  TUnImgMarquee = class(TObject)
  private
    {private declaration}
    procedure DrawSwitch(var DrawRect: TRect; DrawFigure: TDrawFigure;
              const Px1, Py1, Px2, Py2: Integer;
              var BoxRect: TRect; var BoxFlag: Boolean; var PMap: TBitmap);
    procedure Map2Box(var PaintBox: TPaintBox; GraphicCanvas: TCanvas;
              var BitMap2: TBitmap; var BoxRect: TRect; var BoxFlag: Boolean);
    procedure Restore(const R: Trect; var BitMap1, BitMap2: TBitmap;
              var BoxRect: TRect; var BoxFlag: Boolean);
    procedure UpdateBoxRect(R: Trect; var BoxRect: TRect; var BoxFlag: Boolean);
    function  XY2Rect(X1, Y1, X2, Y2: Integer) : Trect;
    function  UniRect(R1, R2 : TRect): TRect;

  public
    {public declaration}
    procedure Prepare(const BitMap0: TBitmap; var BitMap1, BitMap2, PMap: TBitMap;
              PaintBox: TPaintBox; Cria1e2: Boolean);
    procedure DrawControl(const Ms: TmsEvent; const X, Y: Integer;
              var PaintBox: TPaintBox; GraphicCanvas: TCanvas;
              var DrawRect: TRect; var BitMap1, BitMap2: TBitmap;
              var PControl: Byte; var Px1, Py1, Px2, Py2: Integer; var PMap: TBitmap;
              var BoxRect: TRect; var BoxFlag: Boolean; DrawFigure: TDrawFigure);
    procedure InitMaps(const BitMap0: TBitMap; var BitMap1, BitMap2: TBitmap;
              PaintBox: TPaintBox);
    procedure LimpaImagem(Bitmap0, Bitmap1, Bitmap2: TBitmap;
              PaintBox: TPaintBox; var PControl: Byte; BoxFlag: Boolean);
    procedure DoBCS(Source: TBitmap; Dest: TImage; AjusteImagem: TAjusteImagem;
              Exposicao, Brilho, Contraste, Saturacao: Integer);


  end;

var
  UnImgMarquee: TUnImgMarquee;

implementation

uses UnDmkProcFunc;

{ TUnImgMarquee }

// This is the routine for brightness, contrast and saturation adjustment...
procedure fxBCS(dest: TBitmap; b, c, s: integer);
var
  x, y, i, k, v, pv: Integer;
  ci1, ci2, ci3: integer;
  alpha: integer;
  a: double;
  BrightnesLut: array[0..255] of byte;
  ContrastLut: array[0..255] of byte;
  BCLut: array[0..255] of byte;
  SaturationLut: record
    Grays: array[0..767] of Integer;
    Alpha: array[Byte] of Word;
  end;
  SLDest, DestOff: integer;
  p: pRGB24;
  s1, s2: integer;
  bool: boolean;
  color: tRGB24;
begin
  if c = 100 then c := 99;
  for i := 0 to 255 do begin
    if c > 0 then
      a := 1 / cos(c * (PI / 200))
    else
      a := 1 * cos(c * (3.1416 / 200));
    v := Round(a * (i - 170) + 170);
    if v > 255 then v := 255 else if v < 0 then v := 0;
    ContrastLut[i] := v;
  end;
  for i := 0 to 255 do begin
    alpha := b;
    k := 256 - alpha;
    v := (k + alpha * i) div 256;
    if v < 0 then v := 0 else if v > 255 then  v := 255;
    BCLut[i] := ContrastLut[v];
  end;
  x := 0;
  for i := 1 to 256 do SaturationLut.Alpha[i - 1] := (i * s) shr 8;
  for i := 0 to 255 do begin
    y := i - SaturationLut.Alpha[i];
    SaturationLut.Grays[x] := y;
    Inc(x);
    SaturationLut.Grays[x] := y;
    Inc(x);
    SaturationLut.Grays[x] := y;
    Inc(x);
  end;
  SLDest := Integer(dest.ScanLine[0]);
  DestOff := Integer(dest.ScanLine[1]) - SLDest;
  for y := 0 to Dest.Height - 1 do begin
    for x := 0 to Dest.Width - 1 do begin
      color := pRGBArray(SLDest)[x];
      v := color.R + color.G + color.B;
      ci1 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.B];
      ci2 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.G];
      ci3 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.R];
       if ci1 < 0 then ci1 := 0 else if ci1 > 255 then ci1 := 255;
      if ci2 < 0 then ci2 := 0 else if ci2 > 255 then ci2 := 255;
      if ci3 < 0 then ci3 := 0 else if ci3 > 255 then ci3 := 255;
       PRGBArray(SLDest)[x].B := BCLut[ci1];
      PRGBArray(SLDest)[x].G := BCLut[ci2];
      PRGBArray(SLDest)[x].R := BCLut[ci3];
    end;
    inc(SLDest, DestOff);
  end;
end;

procedure fxHistCalc(src: tBitmap; var histR, HistG, HistB: THistogram);
var
  RGB: PRGBArray;
  x, y: Integer;
begin
  for x := 0 to 255 do begin
    histR[x] := 0;
    histG[x] := 0;
    histB[x] := 0;
  end;
  for y := 0 to src.Height - 1 do begin
    RGB := src.ScanLine[y];
    for x := 0 to src.Width - 1 do begin
      inc(histR[RGB[x].R]);
      inc(histG[RGB[x].G]);
      inc(histB[RGB[x].B]);
    end;
  end;
end;

procedure fxHistEqu(src: tBitmap; z: single);
type
  THistSingle = array[0..255] of Single;
var
  RGB: PRGBArray;
  x, y(*, pv*): integer;
  q1, q2, q3: Single;
  histR, HistG, HistB: Thistogram;
  Hist, VCumSumR, VCumSumG, VCumSumB: THistSingle;
  //r, g, b, cy, ccr, ccb: byte;

  function CumSum(hist: THistSingle): THistSingle;
  var
    x: Byte;
    Temp: THistsingle;
  begin
    Temp[0] := Hist[0];
    for x := 1 to 255 do Temp[x] := Temp[x - 1] + Hist[x];
    CumSum := Temp;
  end;

begin
  fxHistCalc(src, histR, histG, histB);
  q1 := 0;   // RED Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistR[x], z);
    q1 := q1 + Hist[x];
  end;
  vcumsumR := cumsum(Hist);
  q2 := 0;   // GREEN Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistG[x], z);
    q2 := q2 + Hist[x];
  end;
  vcumsumG := cumsum(Hist);
  q3 := 0;   // BLUE Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistB[x], z);
    q3 := q3 + Hist[x];
  end;
  vcumsumB := cumsum(Hist);
  for y := 0 to src.height - 1 do
  begin
    RGB := src.scanline[y];
    for x := 0 to src.width - 1 do begin
      RGB[x].R := Trunc((255 / q1) * vcumsumR[RGB[x].R]);
      RGB[x].G := Trunc((255 / q2) * vcumsumG[RGB[x].G]);
      RGB[x].B := Trunc((255 / q3) * vcumsumB[RGB[x].B]);
    end;
  end;
end;

procedure fxExposure(src: TBitmap; k: Single);
var
  RGB: PRGBArray;
  i, x, y, RGBOffset: Integer;
  lut: array[0..255] of integer;
begin
  for i := 0 to 255 do begin
    if k < 0 then
      lut[i]:= i - ((-Round((1 - Exp((i / -128)*(k / 128)))*256)*(i xor 255)) shr 8)
    else
      lut[i]:= i + ((Round((1 - Exp((i / -128)*(k / 128)))*256)*(i xor  255)) shr 8);
    if lut[i] < 0 then lut[i] := 0 else if lut[i] > 255 then lut[i] := 255;
  end;
  RGB := src.ScanLine[0];
  RGBOffset := Integer(src.ScanLine[1]) - Integer(RGB);
  for y := 0 to src.Height - 1 do begin
    for x := 0 to src.Width - 1 do begin
      RGB[x].R := LUT[RGB[x].R];
      RGB[x].G := LUT[RGB[x].G];
      RGB[x].B := LUT[RGB[x].B];
    end;
    RGB:= PRGBArray(Integer(RGB) + RGBOffset);
  end;
end;

procedure SmoothReSize(Src, Dest: TBitmap);
var
  x, y, px, py: Integer;
  i, x1, x2, z, z2, iz2: Integer;
  w1, w2, w3, w4: Integer;
  Ratio: Integer;
  sDst, sDstOff: Integer;
  sScanLine: Array of PRGBArray;
  Src1, Src2: PRGBArray;
  C, C1, C2: TRGB24;
begin
  sDst:= Integer(src.ScanLine[0]);
  sDstOff:= Integer(src.ScanLine[1]) - Integer(sDst);
  SetLength(sScanLine, Src.Height);
  for i:= 0 to Src.Height - 1 do begin
    sScanLine[i]:= PRGBArray(sDst);
    sDst:= sDst + sDstOff;
  end;
  sDst:= Integer(Dest.ScanLine[0]);
  sDstOff:= Integer(Dest.ScanLine[1]) - sDst;
  Ratio:= ((Src.Width - 1) shl 15) div Dest.Width;
  py:= 0;
  for y := 0 to Dest.Height - 1 do begin
    Src1:= sScanLine[py shr 15];
    if py shr 15 < Src.Height - 1 then Src2:= sScanLine[py shr 15 + 1] else Src2:= Src1;
    z2:= py and $7FFF;
    iz2:= $8000 - z2;
    px:= 0;
    for x := 0 to Dest.Width - 1 do begin
      x1 := px shr 15;
      x2:= x1 + 1;
      C1:= Src1[x1];
      C2:= Src2[x1];
      z:= px and $7FFF;
      w2:= (z * iz2) shr 15;
      w1:= iz2 - w2;
      w4:= (z * z2) shr 15;
      w3:= z2 - w4;
      C.R:= (C1.R * w1 + Src1[x2].R * w2 + C2.R * w3 + Src2[x2].R * w4) shr 15;
      C.G:= (C1.G * w1 + Src1[x2].G * w2 + C2.G * w3 + Src2[x2].G * w4) shr 15;
      C.B:= (C1.B * w1 + Src2[x2].B * w2 + C2.B * w3 + Src2[x2].B * w4) shr 15;
      PRGBArray(sDst)[x]:= C;
      Inc(px, Ratio);
    end;
    sDst:= sDst + SDstOff;
    Inc(py, Ratio);
  end;
  SetLength(sScanLine, 0);
end;

function CalcImgSize(w, h, tw, th: integer): TPoint;
begin
  Result.X := 0;
  Result.Y := 0;
  if (w < tw) and (h < th) then begin
    Result.X := w;
    Result.Y := h;
  end
  else if (w = 0) or (h = 0) then Exit
  else begin
    if w > h then begin
      if w < tw then tw := w;
      Result.X := tw;
      Result.Y := Trunc(tw * h / w);
      if Result.Y > th then begin
        Result.Y := th;
        Result.X := Trunc(th * w / h);
      end;
    end else begin
      if h < th then th := h;
      Result.Y := th;
      Result.X := Trunc(th * w / h);
      if Result.X > tw then begin
        Result.X := tw;
        Result.Y := Trunc(tw * h / w);
      end;
    end;
  end;
end;

procedure TUnImgMarquee.DoBCS(Source: TBitmap; Dest: TImage; AjusteImagem:
  TAjusteImagem; Exposicao, Brilho, Contraste, Saturacao: Integer);
var
  tmp, bmp: TBitmap;
begin
  bmp:= TBitmap.Create;
  //bmp.Assign(img);
  bmp.Assign(Source);
  bmp.PixelFormat:= pf24Bit;
  tmp:= TBitmap.Create;
  tmp.PixelFormat:= pf24Bit;
  //tmp.Width:= Img3.Picture.Bitmap.Width;
  tmp.Width:= Dest.Picture.Bitmap.Width;
  //tmp.Height:= Img3.Picture.Bitmap.Height;
  tmp.Height:= Dest.Picture.Bitmap.Height;
  //
  SmoothReSize(bmp, tmp);
  //if radAuto.Checked then
  case AjusteImagem of
    aiAuto: fxHistEqu(tmp, 0.3);
  //else if radExp.Checked then
    //fxExposure(tmp, tbExposure.Position)
    aiExposicao: fxExposure(tmp, Exposicao);
  //else
  //fxBCS(tmp, tbBright.Position, tbContrast.Position, tbSaturation.Position);
    aiManual: fxBCS(tmp, Brilho, Contraste, Saturacao);
    //
    else
  end;

  Dest.Picture.Bitmap.Assign(tmp);
  Dest.Refresh;
  bmp.Free;
  tmp.Free;
end;

procedure TUnImgMarquee.DrawControl(const Ms: TmsEvent; const X, Y: Integer;
  var PaintBox: TPaintBox; GraphicCanvas: TCanvas; var DrawRect: TRect;
  var BitMap1, BitMap2: TBitmap;
  var PControl: Byte; var Px1, Py1, Px2, Py2: Integer; var PMap: TBitmap;
  var BoxRect: TRect; var BoxFlag: Boolean; DrawFigure: TDrawFigure);
//receives mouse events, controls drawing
begin
  case ms of
    msDown:
    if PControl = 0 then
    begin
      PControl := 1;
      PMap := Bitmap2;     //paint in bitmap2
      Px1 := X;
      Py1 := Y;  //save coordinates
    end;
    msMove:
    begin
      if PControl = 2 then
        Restore(DrawRect, BitMap1, BitMap2, BoxRect, BoxFlag);  //restore previous drawing
      if PControl = 1 then
        PControl := 2;
      if PControl <> 0 then
      begin
        px2 := x; py2 := y;
        DrawSwitch(DrawRect, DrawFigure, Px1, Py1, Px2, Py2, BoxRect, BoxFlag, PMap); //select drawing proc, make drawrect
        UpdateBoxRect(DrawRect, BoxRect, BoxFlag);
        Map2Box(PaintBox, GraphicCanvas, BitMap2, BoxRect, BoxFlag); //show drawing changes in boxrect
      end;
    end;
    msUp:
    begin
      if PControl = 2 then
      begin
        Pmap := Bitmap1;
        DrawSwitch(DrawRect, DrawFigure, Px1, Py1, Px2, Py2, BoxRect, BoxFlag, PMap);
        Restore(DrawRect, BitMap1, BitMap2, BoxRect, BoxFlag);
        Map2Box(PaintBox, GraphicCanvas, BitMap2, BoxRect, BoxFlag); //show drawing changes in boxrect
      end;
      PControl := 0;
    end;
  end;//case
end;

procedure TUnImgMarquee.DrawSwitch(var DrawRect: TRect; DrawFigure: TDrawFigure;
  const Px1, Py1, Px2, Py2: Integer; var BoxRect: TRect; var BoxFlag: Boolean;
  var PMap: TBitmap);
//select proper drawing procedure, make drawrect
procedure LineProc();
//paint a red line
begin
  with PMap do
  with Canvas do
  begin
    Pen.Color := $0000ff;
    MoveTo(Px1, Py1);
    LineTo(Px2, Py2);
  end;
end;

procedure RectProc;
//paint a green rectangle
begin
  with PMap do
  with Canvas do
  begin
    Pen.Color := $00c000;
    Brush.Style := bsClear;
    Rectangle(Px1, Py1, Px2, Py2);
  end;
end;

procedure EllipseProc();
//paint a blue ellipse
begin
  with PMap do
  with Canvas do
  begin
    Pen.Color := $ff0000;
    Brush.Style := bsClear;
    Ellipse(Px1, Py1, Px2, Py2);
  end;
end;

begin
  DrawRect := XY2Rect(Px1, Py1, Px2, Py2);
  with DrawRect do
  begin
    Left := Left -1;
    Right := Right + 1;
    Top := Top - 1;
    Bottom := Bottom + 1;
  end;
  if BoxFlag then
    BoxRect := UniRect(BoxRect, DrawRect)
  else begin
    BoxRect := DrawRect;
    BoxFlag := True;
  end;
  case DrawFigure of
    mbLine      : LineProc();
    mbRectangle : RectProc();
    mbEllipse   : EllipseProc();
  end;
end;

procedure TUnImgMarquee.InitMaps(const BitMap0: TBitmap; var BitMap1, BitMap2: TBitmap;
  PaintBox: TPaintBox);
//paint grid in bitmap1, copy to bitmap2
var
  I: Integer;
  BitmapOriginal: TBitmap;
begin
 with bitmap1 do with canvas do
  begin
   brush.color := $e0ffff;
   brush.style := bsSolid;
   pen.color := $fff0f0;
   fillrect(rect(0,0,width,height));
   pen.Width := 1;
   i := 0;
   while i < width do
    begin
     moveto(i,0); lineto(i,height);
     inc(i,20);
    end;
   i := 0;
   while i < height do
    begin
     moveto(0,i); lineto(width,i);
     inc(i,20);
    end;
  end;
  BitmapOriginal := TBitmap.Create;
  BitmapOriginal.Width := BitMap0.Width;
  BitmapOriginal.Height := BitMap0.Height;
  BitmapOriginal.PixelFormat := pf24bit;
  BitmapOriginal.Assign(BitMap0);
  DmkImg.ResizeImage_Bitmap(BitmapOriginal, PaintBox.Width, PaintBox.Height);
  Bitmap1.Canvas.Draw(0, 0, BitmapOriginal);
  BitmapOriginal.Free;
  Bitmap2.Canvas.Draw(0, 0, BitMap1);
  PaintBox.Invalidate;
end;

procedure TUnImgMarquee.LimpaImagem(Bitmap0, Bitmap1, Bitmap2: TBitmap;
  PaintBox: TPaintBox; var PControl: Byte; BoxFlag: Boolean);
begin
  InitMaps(Bitmap0, Bitmap1, Bitmap2, PaintBox);
  PControl := 0;
  //BoxFlag := false;
end;

procedure TUnImgMarquee.Map2Box(var PaintBox: TPaintBox;
  GraphicCanvas: TCanvas; var BitMap2: TBitmap;
  var BoxRect: TRect; var BoxFlag: Boolean);
begin
  GraphicCanvas.CopyRect(BoxRect, Bitmap2.Canvas, BoxRect);
  BoxFlag := False; //PaintBox update done
end;

procedure TUnImgMarquee.Prepare(const BitMap0: TBitmap; var BitMap1, BitMap2, PMap: TBitMap;
  PaintBox: TPaintBox; Cria1e2: Boolean);
begin
  //if BitMap1 = nil then
  if Cria1e2 then
    BitMap1 := TBitmap.Create;
  with Bitmap1 do
  begin
    Width := PaintBox.Width;
    Height := PaintBox.Height;
    PixelFormat := pf32bit;
  end;
  if Cria1e2 then
  //if BitMap2 = nil then
    BitMap2 := TBitmap.Create;
  with bitmap2 do
  begin
    Width := PaintBox.Width;
    Height := PaintBox.Height;
    PixelFormat := pf32bit;
  end;
  PMap := nil;
  InitMaps(BitMap0, Bitmap1, Bitmap2, PaintBox);
end;

procedure TUnImgMarquee.Restore(const R: Trect; var BitMap1, BitMap2: TBitmap;
var BoxRect: TRect; var BoxFlag: Boolean);
begin
  Bitmap2.Canvas.CopyRect(R, Bitmap1.Canvas, R);
  UpdateBoxRect(R, BoxRect, BoxFlag);
end;

function TUnImgMarquee.UniRect(R1, R2: TRect): TRect;
//union of rectangles
begin
  with result do
  begin
    if r1.left   < r2.left   then left   := r1.left   else left   := r2.Left;
    if r1.top    < r2.top    then top    := r1.top    else top    := r2.top;
    if r1.right  > r2.right  then right  := r1.right  else right  := r2.right;
    if r1.bottom > r2.bottom then bottom := r1.bottom else bottom := r2.bottom;
  end;
end;

procedure TUnImgMarquee.UpdateBoxRect(R: TRect; var BoxRect: TRect; var BoxFlag: Boolean);
begin
  if BoxFlag then
    BoxRect := UniRect(BoxRect, R)  //union of rectangles
  else
  begin
    BoxRect := R;
    BoxFlag := True;
  end;
end;

function TUnImgMarquee.XY2Rect(X1, Y1, X2, Y2: Integer): TRect;
//make rectangle out of coordinates
begin
  with Result do
  begin
    if X1 < X2 then
    begin
      Left := X1;
      Right := X2;
    end
    else
    begin
      Left := X2;
      Right := X1;
    end;
    if Y1 < Y2 then
    begin
      Top := Y1;
      Bottom := Y2;
    end
    else begin
      Top := Y2;
      Bottom := Y1;
    end;
  end;
end;

end.
