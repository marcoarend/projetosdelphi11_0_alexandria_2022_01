unit ObtemFoto4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, Vcl.Menus, VFrames, Jpeg;

type
  TFmObtemFoto4 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PMOpcoes: TPopupMenu;
    Cmera1: TMenuItem;
    Vdeo1: TMenuItem;
    PnTopo: TPanel;
    Label1: TLabel;
    CBDevices: TComboBox;
    CheckBox_Crosshair: TCheckBox;
    CheckBox_Mirror: TCheckBox;
    BtIniciar: TBitBtn;
    BtParar: TBitBtn;
    BtSalvar: TBitBtn;
    BtOpcoes: TBitBtn;
    Panel2: TPanel;
    PaintBox1: TPaintBox;
    Timer1: TTimer;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Cmera1Click(Sender: TObject);
    procedure Vdeo1Click(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
    procedure BtIniciarClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FActivated: Boolean;
    FVideoImage : TVideoImage;
    FVideoBitmap: TBitmap;
    procedure OnNewVideoFrame(Sender : TObject; Width, Height: integer; DataPtr: pointer);
  public
    { Public declarations }
    FJPGImage: TJPEGImage;
  end;

  var
  FmObtemFoto4: TFmObtemFoto4;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmObtemFoto4.BtIniciarClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    //
    Application.ProcessMessages;
    FVideoImage.VideoStart(CBDevices.Items[CBDevices.ItemIndex]);
    Application.ProcessMessages;
    //
    BtIniciar.Enabled := False;
    BtParar.Enabled   := True;
    BtSalvar.Enabled  := True;
    BtOpcoes.Enabled  := True;
    //
    //Timer1.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmObtemFoto4.BtOpcoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOpcoes, BtOpcoes);
end;

procedure TFmObtemFoto4.BtPararClick(Sender: TObject);
begin
  FVideoImage.VideoStop;
  //
  BtIniciar.Enabled := True;
  BtParar.Enabled   := False;
  BtSalvar.Enabled  := False;
  BtOpcoes.Enabled  := False;
end;

procedure TFmObtemFoto4.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmObtemFoto4.BtSalvarClick(Sender: TObject);
var
  Bmp: TBitmap;
begin
  Screen.Cursor := crHourGlass;
  try
    Bmp := TBitmap.Create;
    Bmp.Assign(FVideoBitmap);
    //
    FJPGImage := TJPEGImage.Create;
    FJPGImage.Assign(Bmp);
    FVideoImage.VideoStop;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmObtemFoto4.Cmera1Click(Sender: TObject);
begin
  FVideoImage.ShowProperty;
end;

procedure TFmObtemFoto4.FormActivate(Sender: TObject);
var
  DeviceList: TStringList;
begin
  MyObjects.CorIniComponente();
  //
  if FActivated then
    Exit;
  FActivated       := True;
  BtParar.Enabled  := False;
  BtSalvar.Enabled := False;
  BtOpcoes.Enabled := False;
  //
  // Obtem a lista das c�meras dispon�veis
  DeviceList := TStringList.Create;
  FVideoImage.GetListOfDevices(DeviceList);

  if DeviceList.Count < 1 then
  begin
    BtIniciar.Enabled := False;
    //
    Geral.MB_Aviso('N�o foi detectado nenhum disposit�vo de v�deo!');
    Exit;
  end else
  begin
    CBDevices.Items.Assign(DeviceList);
    CBDevices.ItemIndex := 0;
    BtIniciar.Enabled   := True;
  end;
end;

procedure TFmObtemFoto4.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FJPGImage                   := nil;
  PaintBox1.Align             := alClient;
  FActivated                  := False;
  FVideoBitmap                := TBitmap.Create;
  FVideoImage                 := TVideoImage.Create;
  //FVideoImage.SetDisplayCanvas(PaintBox1.Canvas);
  FVideoImage.OnNewVideoFrame := OnNewVideoFrame;
end;

procedure TFmObtemFoto4.FormDestroy(Sender: TObject);
begin
  FVideoImage.VideoStop;
  FVideoImage.Free;
  FVideoBitmap.Free;
  FJPGImage.Free;
end;

procedure TFmObtemFoto4.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmObtemFoto4.OnNewVideoFrame(Sender: TObject; Width, Height: integer;
  DataPtr: pointer);
var
  I, R: Integer;
begin
  // Retreive latest video image
  FVideoImage.GetBitmap(FVideoBitmap);

  // Paint a crosshair onto video image
  if CheckBox_Crosshair <> nil then
  begin
    if CheckBox_Crosshair.Checked then
    begin
      with FVideoBitmap.Canvas do
      begin
        Brush.Style := bsClear;
        Pen.Width   := 3;
        Pen.Color   := clRed;
        MoveTo(0, FVideoBitmap.Height div 2);
        LineTo(FVideoBitmap.Width, FVideoBitmap.Height div 2);
        MoveTo(FVideoBitmap.Width div 2, 0);
        LineTo(FVideoBitmap.Width div 2, FVideoBitmap.Height);
        //
        for I := 1 to 3 do
        begin
          R := (FVideoBitmap.Height div 8) * I;
          //
          Ellipse(FVideoBitmap.Width div 2 - R, FVideoBitmap.Height div 2  - R,
            FVideoBitmap.Width div 2 + R, FVideoBitmap.Height div 2 + R);
        end;
      end;
    end;
  end;

  // Paint image onto screen, either normally or flipped.
  if CheckBox_Mirror <> nil then
  begin
    if CheckBox_Mirror.Checked then
      PaintBox1.Canvas.CopyRect(Rect(0, 0, FVideoBitmap.Width,
        FVideoBitmap.Height), FVideoBitmap.Canvas, Rect(FVideoBitmap.Width - 1,
        0, 0, FVideoBitmap.Height))
    else
      PaintBox1.Canvas.Draw(0, 0, FVideoBitmap);
  end;
end;

procedure TFmObtemFoto4.Timer1Timer(Sender: TObject);
begin
  //Timer1.Enabled := False;
  //
  //FVideoImage.VideoStop;
end;

procedure TFmObtemFoto4.Vdeo1Click(Sender: TObject);
begin
  FVideoImage.ShowProperty_Stream;
end;

end.
