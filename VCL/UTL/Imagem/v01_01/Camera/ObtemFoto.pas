unit ObtemFoto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls,  Math, ExtDlgs, Jpeg, DBImageEx,
  dmkGeral, dmkEdit, DB, mySQLDbTables, UnDmkProcFunc,
  // Captura
  DSPack, DSUtil, DirectShow9,
  // Filtros
  frxClass, frxExportPDF, Buttons;

type
  PRGB24 = ^TRGB24;
  TRGB24 = record B, G, R: Byte; end;
  PRGBArray = ^TRGBArray;
  TRGBArray = array[0..0] of TRGB24;
  THistogram = array[0..255] of Byte;

  TFmObtemFoto = class(TForm)
    dlgOpen: TOpenPictureDialog;
    dlgSave: TSavePictureDialog;
    MyTest: TmySQLDatabase;
    TbMyBlobs: TmySQLTable;
    TbMyBlobsCodigo: TIntegerField;
    TbMyBlobsImagem: TBlobField;
    DsMyBlobs: TDataSource;
    VideoSourceFilter: TFilter;
    CaptureGraph: TFilterGraph;
    SampleGrabber: TSampleGrabber;
    Timer: TTimer;
    Panel2: TPanel;
    Panel3: TPanel;
    Img1: TImage;
    Label10: TLabel;
    VideoWindow: TVideoWindow;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdQualidade: TdmkEdit;
    EdMaxAltu: TdmkEdit;
    EdMaxLarg: TdmkEdit;
    BtFotoPrepara: TButton;
    RadioGroupMethod: TRadioGroup;
    RadioGroupRotate: TRadioGroup;
    CheckBoxFlip: TCheckBox;
    CheckBoxReverse: TCheckBox;
    CallBack: TCheckBox;
    PnAjusta: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    tbBright: TTrackBar;
    radAuto: TRadioButton;
    radManuell: TRadioButton;
    tbContrast: TTrackBar;
    tbSaturation: TTrackBar;
    radExp: TRadioButton;
    tbExposure: TTrackBar;
    Panel4: TPanel;
    StatusBar: TStatusBar;
    dmkEdit1: TdmkEdit;
    btnSave: TButton;
    Button1: TButton;
    frxPDFExport: TfrxPDFExport;
    frxReport1: TfrxReport;
    EdW: TdmkEdit;
    EdH: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    Panel5: TPanel;
    VideoFormats: TListBox;
    Panel6: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    VideoCapFilters: TListBox;
    StartButton: TButton;
    StopButton: TButton;
    Panel7: TPanel;
    Img2: TImage;
    Img0: TImage;
    PaintBox1: TPaintBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Img3: TImage;
    Img4: TImage;
    btnOpen: TButton;
    BtSnapShot: TButton;
    Panel8: TPanel;
    PaintBox2: TPaintBox;
    Label13: TLabel;
    LabelTimer: TLabel;
    procedure btnOpenClick(Sender: TObject);
    procedure tbBrightChange(Sender: TObject);
    procedure MakePreviewImage;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure VideoCapFiltersClick(Sender: TObject);
    procedure SampleGrabberBuffer(sender: TObject; SampleTime: Double;
      pBuffer: Pointer; BufferLen: Integer);
    procedure StartButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure dmkEdit1Exit(Sender: TObject);
    procedure BtSnapShotClick(Sender: TObject);
    procedure BtFotoPreparaClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Img1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Img1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Img1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Img0MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Img0MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Img0MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    fH, fW: Double;
    FTempo: TDateTime;
    BitmapOriginal, BitmapRotacionado: TBitmap;
    pInicio, pFim: TPoint;
    //
    // Captura
    function  BmpToJpeg_1(BMP: TBitmap): TJPegImage;
    procedure RotacionaOuInverteImagem();
    // Filtro
    procedure DoBCS;
    // Recorte
    procedure RecortaParteBitmap();
  public
    { Public declarations }
  end;

var
  FmObtemFoto: TFmObtemFoto;
  // Captura
  CapEnum: TSysDevEnum;
  VideoMediaTypes: TEnumMediaType;
  CapFile: WideString = 'c:\capture.avi';
  // Filtro
  mx,my: array[Word]of Integer;
  img: TBitmap;
  ImgLoaded: Boolean;
  // Selecionar
  PDown, PActually: TPoint;
  MouseIsDown: Boolean;

implementation

uses UnMyObjects;

{$R *.dfm}

// This is the routine for brightness, contrast and saturation adjustment...
procedure fxBCS(dest: TBitmap; b, c, s: integer);
var
  x, y, i, k, v, pv: Integer;
  ci1, ci2, ci3: integer;
  alpha: integer;
  a: double;
  BrightnesLut: array[0..255] of byte;
  ContrastLut: array[0..255] of byte;
  BCLut: array[0..255] of byte;
  SaturationLut: record
    Grays: array[0..767] of Integer;
    Alpha: array[Byte] of Word;
  end;
  SLDest, DestOff: integer;
  p: pRGB24;
  s1, s2: integer;
  bool: boolean;
  color: tRGB24;
begin
  if c = 100 then c := 99;
  for i := 0 to 255 do begin
    if c > 0 then
      a := 1 / cos(c * (PI / 200))
    else
      a := 1 * cos(c * (3.1416 / 200));
    v := Round(a * (i - 170) + 170);
    if v > 255 then v := 255 else if v < 0 then v := 0;
    ContrastLut[i] := v;
  end;
  for i := 0 to 255 do begin
    alpha := b;
    k := 256 - alpha;
    v := (k + alpha * i) div 256;
    if v < 0 then v := 0 else if v > 255 then  v := 255;
    BCLut[i] := ContrastLut[v];
  end;
  x := 0;
  for i := 1 to 256 do SaturationLut.Alpha[i - 1] := (i * s) shr 8;
  for i := 0 to 255 do begin
    y := i - SaturationLut.Alpha[i];
    SaturationLut.Grays[x] := y;
    Inc(x);
    SaturationLut.Grays[x] := y;
    Inc(x);
    SaturationLut.Grays[x] := y;
    Inc(x);
  end;
  SLDest := Integer(dest.ScanLine[0]);
  DestOff := Integer(dest.ScanLine[1]) - SLDest;
  for y := 0 to Dest.Height - 1 do begin
    for x := 0 to Dest.Width - 1 do begin
      color := pRGBArray(SLDest)[x];
      v := color.R + color.G + color.B;
      ci1 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.B];
      ci2 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.G];
      ci3 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.R];
       if ci1 < 0 then ci1 := 0 else if ci1 > 255 then ci1 := 255;
      if ci2 < 0 then ci2 := 0 else if ci2 > 255 then ci2 := 255;
      if ci3 < 0 then ci3 := 0 else if ci3 > 255 then ci3 := 255;
       PRGBArray(SLDest)[x].B := BCLut[ci1];
      PRGBArray(SLDest)[x].G := BCLut[ci2];
      PRGBArray(SLDest)[x].R := BCLut[ci3];
    end;
    inc(SLDest, DestOff);
  end;
end;

procedure fxHistCalc(src: tBitmap; var histR, HistG, HistB: THistogram);
var
  RGB: PRGBArray;
  x, y: Integer;
begin
  for x := 0 to 255 do begin
    histR[x] := 0;
    histG[x] := 0;
    histB[x] := 0;
  end;
  for y := 0 to src.Height - 1 do begin
    RGB := src.ScanLine[y];
    for x := 0 to src.Width - 1 do begin
      inc(histR[RGB[x].R]);
      inc(histG[RGB[x].G]);
      inc(histB[RGB[x].B]);
    end;
  end;
end;

procedure fxHistEqu(src: tBitmap; z: single);
type
  THistSingle = array[0..255] of Single;
var
  RGB: PRGBArray;
  x, y, pv: integer;
  q1, q2, q3: Single;
  histR, HistG, HistB: Thistogram;
  Hist, VCumSumR, VCumSumG, VCumSumB: THistSingle;
  r, g, b, cy, ccr, ccb: byte;

  function CumSum(hist: THistSingle): THistSingle;
  var
    x: Byte;
    Temp: THistsingle;
  begin
    Temp[0] := Hist[0];
    for x := 1 to 255 do Temp[x] := Temp[x - 1] + Hist[x];
    CumSum := Temp;
  end;

begin
  fxHistCalc(src, histR, histG, histB);
  q1 := 0;   // RED Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistR[x], z);
    q1 := q1 + Hist[x];
  end;
  vcumsumR := cumsum(Hist);
  q2 := 0;   // GREEN Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistG[x], z);
    q2 := q2 + Hist[x];
  end;
  vcumsumG := cumsum(Hist);
  q3 := 0;   // BLUE Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistB[x], z);
    q3 := q3 + Hist[x];
  end;
  vcumsumB := cumsum(Hist);
  for y := 0 to src.height - 1 do
  begin
    RGB := src.scanline[y];
    for x := 0 to src.width - 1 do begin
      RGB[x].R := Trunc((255 / q1) * vcumsumR[RGB[x].R]);
      RGB[x].G := Trunc((255 / q2) * vcumsumG[RGB[x].G]);
      RGB[x].B := Trunc((255 / q3) * vcumsumB[RGB[x].B]);
    end;
  end;
end;

procedure fxExposure(src: TBitmap; k: Single);
var
  RGB: PRGBArray;
  i, x, y, RGBOffset: Integer;
  lut: array[0..255] of integer;
begin
  for i := 0 to 255 do begin
    if k < 0 then
      lut[i]:= i - ((-Round((1 - Exp((i / -128)*(k / 128)))*256)*(i xor 255)) shr 8)
    else
      lut[i]:= i + ((Round((1 - Exp((i / -128)*(k / 128)))*256)*(i xor  255)) shr 8);
    if lut[i] < 0 then lut[i] := 0 else if lut[i] > 255 then lut[i] := 255;
  end;
  RGB := src.ScanLine[0];
  RGBOffset := Integer(src.ScanLine[1]) - Integer(RGB);
  for y := 0 to src.Height - 1 do begin
    for x := 0 to src.Width - 1 do begin
      RGB[x].R := LUT[RGB[x].R];
      RGB[x].G := LUT[RGB[x].G];
      RGB[x].B := LUT[RGB[x].B];
    end;
    RGB:= PRGBArray(Integer(RGB) + RGBOffset);
  end;
end;

procedure SmoothReSize(Src, Dest: TBitmap);
var
  x, y, px, py: Integer;
  i, x1, x2, z, z2, iz2: Integer;
  w1, w2, w3, w4: Integer;
  Ratio: Integer;
  sDst, sDstOff: Integer;
  sScanLine: Array of PRGBArray;
  Src1, Src2: PRGBArray;
  C, C1, C2: TRGB24;
begin
  sDst:= Integer(src.ScanLine[0]);
  sDstOff:= Integer(src.ScanLine[1]) - Integer(sDst);
  SetLength(sScanLine, Src.Height);
  for i:= 0 to Src.Height - 1 do begin
    sScanLine[i]:= PRGBArray(sDst);
    sDst:= sDst + sDstOff;
  end;
  sDst:= Integer(Dest.ScanLine[0]);
  sDstOff:= Integer(Dest.ScanLine[1]) - sDst;
  Ratio:= ((Src.Width - 1) shl 15) div Dest.Width;
  py:= 0;
  for y := 0 to Dest.Height - 1 do begin
    Src1:= sScanLine[py shr 15];
    if py shr 15 < Src.Height - 1 then Src2:= sScanLine[py shr 15 + 1] else Src2:= Src1;
    z2:= py and $7FFF;
    iz2:= $8000 - z2;
    px:= 0;
    for x := 0 to Dest.Width - 1 do begin
      x1 := px shr 15;
      x2:= x1 + 1;
      C1:= Src1[x1];
      C2:= Src2[x1];
      z:= px and $7FFF;
      w2:= (z * iz2) shr 15;
      w1:= iz2 - w2;
      w4:= (z * z2) shr 15;
      w3:= z2 - w4;
      C.R:= (C1.R * w1 + Src1[x2].R * w2 + C2.R * w3 + Src2[x2].R * w4) shr 15;
      C.G:= (C1.G * w1 + Src1[x2].G * w2 + C2.G * w3 + Src2[x2].G * w4) shr 15;
      C.B:= (C1.B * w1 + Src2[x2].B * w2 + C2.B * w3 + Src2[x2].B * w4) shr 15;
      PRGBArray(sDst)[x]:= C;
      Inc(px, Ratio);
    end;
    sDst:= sDst + SDstOff;
    Inc(py, Ratio);
  end;
  SetLength(sScanLine, 0);
end;

function CalcImgSize(w, h, tw, th: integer): TPoint;
begin
  Result.X := 0;
  Result.Y := 0;
  if (w < tw) and (h < th) then begin
    Result.X := w;
    Result.Y := h;
  end
  else if (w = 0) or (h = 0) then Exit
  else begin
    if w > h then begin
      if w < tw then tw := w;
      Result.X := tw;
      Result.Y := Trunc(tw * h / w);
      if Result.Y > th then begin
        Result.Y := th;
        Result.X := Trunc(th * w / h);
      end;
    end else begin
      if h < th then th := h;
      Result.Y := th;
      Result.X := Trunc(th * w / h);
      if Result.X > tw then begin
        Result.X := tw;
        Result.Y := Trunc(tw * h / w);
      end;
    end;
  end;
end;

procedure TFmObtemFoto.btnSaveClick(Sender: TObject);
var
  Jpg: TJPegImage;
  //Stream: TMemoryStream;
begin
  //Img4.Picture.Bitmap.SaveToFile('C:/Teste5_2.jpeg');
  Jpg := BmpToJpeg_1(Img4.Picture.Bitmap);
  Jpg.SaveToFile('C:/Teste5.jpeg');
{
  Stream := TMemoryStream.Create;
  try
    Jpg.SaveToStream(Stream);
    Stream.Position := 0;
  finally

  end;
}
end;

procedure TFmObtemFoto.BtFotoPreparaClick(Sender: TObject);
var
  //Jpg: TJPegImage;
  Centro: TPoint;
begin
  Img0.Picture.Bitmap := nil;
  Label13.Caption := 'Iniciando captura....';
  //
  FTempo := Now();
  SampleGrabber.GetBitmap(Img1.Picture.Bitmap);
  BitmapOriginal.Assign(Img1.Picture.Bitmap);
  if BitmapOriginal.PixelFormat = pf24bit then
  begin
    RadioGroupMethod.ItemIndex := 0;
    RadioGroupMethod.Enabled := True
  end
  else
  begin
    RadioGroupMethod.ItemIndex := 1;
    // Force CopyRect to be only method available
    RadioGroupMethod.Enabled := False;
  end;
  dmkPF.ResizeImage_Bitmap(BitmapOriginal, EdMaxLarg.ValueVariant, EdMaxAltu.ValueVariant);
  if BitmapOriginal.Width > BitmapOriginal.Height then
  begin
    RotacionaOuInverteImagem();
    //Jpg := BmpToJpeg_1(BitmapRotacionado);
    Img2.Picture.Graphic := BitmapRotacionado;
  end else
  begin
    //Jpg := BmpToJpeg_1(BitmapOriginal);
    Img2.Picture.Graphic := BitmapOriginal;
  end;
  //Jpg.SaveToFile('C:/Teste5.jpeg');
  //

  { PDF - N�o d�! O arquivo fica muito maior!
  MyObjects.frxPrepara(frxReport1, 'Teste 5');
  frxReport1.Export(frxPDFExport);
  }
  FTempo := Now() - FTempo;
  Label13.Caption := 'Captura finalizada em: ' + FormatDateTime('hh:nn:ss:zzz', FTempo);
end;

procedure TFmObtemFoto.dmkEdit1Exit(Sender: TObject);
begin
  TbMyBlobs.Close;
  MyTest.Connected := true;
  TbMyBlobs.Filter := 'Codigo=' + Geral.FF0(dmkEdit1.ValueVariant);
  TbMyBlobs.Open;
end;

procedure TFmObtemFoto.DoBCS;
var
  tmp, bmp: TBitmap;
  pt: TPoint;
begin
  bmp:= TBitmap.Create;
  bmp.Assign(img);
  bmp.PixelFormat:= pf24Bit;
  tmp:= TBitmap.Create;
  tmp.PixelFormat:= pf24Bit;
  pt:= CalcImgSize(bmp.Width, bmp.Height, Img4.Width, Img4.Height);
  tmp.Width:= pt.X;
  tmp.Height:= pt.Y;
  SmoothReSize(bmp, tmp);
  if radAuto.Checked then
    fxHistEqu(tmp, 0.3)
  else if radExp.Checked then
    fxExposure(tmp, tbExposure.Position)
  else
    fxBCS(tmp, tbBright.Position, tbContrast.Position, tbSaturation.Position);
  Img4.Picture.Bitmap.Assign(tmp);
  Img4.Refresh;
  bmp.Free;
  tmp.Free;
end;

procedure TFmObtemFoto.MakePreviewImage();
var
  pt: TPoint;
begin
  if not ImgLoaded then
    Exit;  
  pt:= CalcImgSize(Img3.Picture.Width, Img3.Picture.Height, Img4.Width, Img4.Height);
  img.Width:= pt.X;
  img.Height:= pt.Y;
  Img3.Picture.Bitmap.PixelFormat:= pf24Bit;
  SmoothReSize(Img3.Picture.Bitmap, img);
  DoBCS;
  PnAjusta.Enabled := True;
end;

procedure TFmObtemFoto.RotacionaOuInverteImagem();
var
  StartTick: Integer;
begin
  StartTick := GetTickCount;

  // First Flip and/or reverse the bitmap.
  // Treat BitmapFlipReverse functions much like a TBitmap.Create.
  case RadioGroupMethod.ItemIndex OF
    0:  BitmapRotacionado := FlipReverseScanLine(
        CheckBoxFlip.Checked, CheckBoxReverse.Checked, BitmapOriginal);

    1:  BitmapRotacionado := FlipReverseCopyRect(
        CheckBoxFlip.Checked, CheckBoxReverse.Checked, BitmapOriginal);

    2:  BitmapRotacionado := FlipReverseStretchBlt(
        CheckBoxFlip.Checked, CheckBoxReverse.Checked, BitmapOriginal);
    ELSE
      // Should never happen.  Silence the compiler warning.
      (*BitmapFlipReverse*)BitmapRotacionado := NIL
  END;

  IF   RadioGroupRotate.Visible
  THEN BEGIN
    // Rotate 0, 90, 180, 270 degrees counterclockwise
    (*BitmapRotate*)BitmapRotacionado := RotateScanLine90(90*RadioGroupRotate.ItemIndex,
                                     (*BitmapFlipReverse*)BitmapRotacionado);
    //ImgRotacionado.Picture.Graphic := (*BitmapRotate*)BitmapRotacionado;
    //BitmapRotate.Free
  END
  ELSE ;//ImgRotacionado.Picture.Graphic := (*BitmapFlipReverse*)BitmapRotacionado;

//  A refresh may help reduce flicker on some screens but adds quite a bit
//  to the total time.
//  ImageModified.Refresh;

  //BitmapFlipReverse.Free;

  LabelTimer.Caption := IntToStr(GetTickCount - StartTick) + ' ms'
end;

procedure TFmObtemFoto.RecortaParteBitmap();
var
  I, J, I1, J1: Integer;
  Bmp: TBitmap;
begin
  Bmp := TBitmap.Create;
  try
    Bmp.Width := pFim.X - pInicio.X;
    Bmp.Height := pFim.Y - pInicio.Y;
    Bmp.PixelFormat := pf24bit;
    //
    I1 := 0;
    for I := pInicio.X to pFim.X do
    begin
      I1 := I1 + 1;
      J1 := 0;
      for J := pInicio.Y to pFim.Y do
      begin
        J1 := J1 + 1;
        Bmp.Canvas.Pixels[I1, J1] := Img2.Picture.Bitmap.Canvas.Pixels[I, J];
      end;
    end;
    Img3.Picture.Assign(Bmp);
  finally
    Bmp.Free;
  end;
end;

procedure TFmObtemFoto.SampleGrabberBuffer(sender: TObject; SampleTime: Double;
  pBuffer: Pointer; BufferLen: Integer);
begin
{ Acho que n�o precisa
  if CallBack.Checked then
  begin
    Image.Canvas.Lock;
    try
      SampleGrabber.GetBitmap(Image.Picture.Bitmap, pBuffer, BufferLen);
    finally
      Image.Canvas.Unlock;
    end;
  end;
}
end;

procedure TFmObtemFoto.BtSnapShotClick(Sender: TObject);
begin
  SampleGrabber.GetBitmap(Img1.Picture.Bitmap);
end;

procedure TFmObtemFoto.Button1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxReport1, 'Teste 5');
  {MyObjects.frxPrepara(frxReport1, 'Teste 5');
  frxReport1.Export(frxPDFExport);}
end;

procedure TFmObtemFoto.StartButtonClick(Sender: TObject);
var
  multiplexer: IBaseFilter;
  Writer: IFileSinkFilter;
  PinList: TPinList;
  i: integer;
begin
{
  CaptureGraph.ClearGraph;
  CaptureGraph.Active := false;
  Filter.BaseFilter.Moniker := SysDev.GetMoniker(TMenuItem(Sender).tag);
  CaptureGraph.Active := true;
  CaptureGraph.Play;
}

  // Activate the filter graph, at this stage the source filters are added to the graph
  CaptureGraph.Active := true;

  // configure output Audio media type + source
{
  if AudioSourceFilter.FilterGraph <> nil then
  begin
    PinList := TPinList.Create(AudioSourceFilter as IBaseFilter);
    i := 0;
    while i < PinList.Count do
      if PinList.PinInfo[i].dir = PINDIR_OUTPUT then
        begin
          if AudioFormats.ItemIndex <> -1 then
            with (PinList.Items[i] as IAMStreamConfig) do
              SetFormat(AudioMediaTypes.Items[AudioFormats.ItemIndex].AMMediaType^);
          PinList.Delete(i);
        end else inc(i);
    if InputLines.ItemIndex <> -1 then
      with (PinList.Items[InputLines.ItemIndex] as IAMAudioInputMixer) do
        put_Enable(true);
    PinList.Free;
  end;
}

  // configure output Video media type
  if VideoSourceFilter.FilterGraph <> nil then
  begin
    PinList := TPinList.Create(VideoSourceFilter as IBaseFilter);
    if VideoFormats.ItemIndex <> -1 then
      with (PinList.First as IAMStreamConfig) do
        SetFormat(VideoMediaTypes.Items[VideoFormats.ItemIndex].AMMediaType^);
    PinList.Free;
  end;


  // now render streams
{
  with CaptureGraph as IcaptureGraphBuilder2 do
  begin
    // set the output filename
    SetOutputFileName(MEDIASUBTYPE_Avi, PWideChar(CapFile), multiplexer, Writer);

    // Connect Video preview (VideoWindow)
    if VideoSourceFilter.BaseFilter.DataLength > 0 then
      RenderStream(@PIN_CATEGORY_PREVIEW, nil, VideoSourceFilter as IBaseFilter,
        nil , VideoWindow as IBaseFilter);

    // Connect Video capture streams
    if VideoSourceFilter.FilterGraph <> nil then
      RenderStream(@PIN_CATEGORY_CAPTURE, nil, VideoSourceFilter as IBaseFilter,
        nil, multiplexer as IBaseFilter);

    // Connect Audio capture streams
    if AudioSourceFilter.FilterGraph <> nil then
    begin

      RenderStream(nil, nil, AudioSourceFilter as IBaseFilter,
        nil, multiplexer as IBaseFilter);
    end;
  end;
}
  //  ver!
  with CaptureGraph as ICaptureGraphBuilder2 do
    RenderStream(@PIN_CATEGORY_PREVIEW, nil, VideoSourceFilter as IBaseFilter, SampleGrabber as IBaseFilter, VideoWindow as IbaseFilter);
  //
  CaptureGraph.Play;
  StopButton.Enabled := true;
  StartButton.Enabled := false;
  {
  AudioFormats.Enabled := false;
  AudioCapFilters.Enabled := false;}
  VideoFormats.Enabled := false;
  VideoCapFilters.Enabled := false;
  //
  BtSnapShot.Enabled := True;
  BtFotoPrepara.Enabled := True;
  //
  Timer.Enabled := True;
end;

procedure TFmObtemFoto.StopButtonClick(Sender: TObject);
begin
  Timer.Enabled := false;
  StopButton.Enabled := false;
  StartButton.Enabled := true;
  CaptureGraph.Stop;
  CaptureGraph.Active := False;
  {
  AudioFormats.Enabled := true;
  AudioCapFilters.Enabled := true;
  }
  VideoFormats.Enabled := true;
  VideoCapFilters.Enabled := true;
  //
  BtSnapShot.Enabled := True;
  BtFotoPrepara.Enabled := True;
  //
end;

procedure TFmObtemFoto.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  DoubleBuffered := True;
  Panel7.DoubleBuffered := True;
  //
  img:= TBitmap.Create;
  img.PixelFormat:= pf24Bit;
  ImgLoaded:= False;
  //
  // Setup BitmapOriginal in case Flip /Reverse checkboxes used before
  // an image is loaded.
  BitmapOriginal := TBitmap.Create;
  BitmapOriginal.Width := Img1.Width;
  BitmapOriginal.Height := Img1.Height;
  BitmapOriginal.PixelFormat := pf24bit;
  //
  BitmapRotacionado := TBitMap.Create;
  //
  CapEnum := TSysDevEnum.Create(CLSID_VideoInputDeviceCategory);
  for i := 0 to CapEnum.CountFilters - 1 do
    VideoCapFilters.Items.Add(CapEnum.Filters[i].FriendlyName);

  CapEnum.SelectGUIDCategory(CLSID_AudioInputDeviceCategory);
{
  for i := 0 to CapEnum.CountFilters - 1 do
    AudioCapFilters.Items.Add(CapEnum.Filters[i].FriendlyName);
}

  VideoMediaTypes := TEnumMediaType.Create;
{
  AudioMediaTypes := TEnumMediaType.Create;
}
  if VideoCapFilters.ItemIndex = -1 then
  begin
    VideoCapFilters.ItemIndex := VideoCapFilters.Items.Count -1;
    VideoCapFiltersClick(Self);
  end;
  if VideoFormats.ItemIndex = -1 then
  begin
    VideoFormats.ItemIndex := 0;
  end;
  if (VideoCapFilters.ItemIndex >= 0) and (VideoFormats.ItemIndex >= 0) then
    StartButtonClick(Self);
end;

procedure TFmObtemFoto.FormDestroy(Sender: TObject);
begin
  StopButtonClick(Self);
  BitmapOriginal.Free;
  BitmapRotacionado.Free;
  //
  CapEnum.Free;
  VideoMediaTypes.Free;
  {
  AudioMediaTypes.Free;
  }
  //
  img.Free;
end;

procedure TFmObtemFoto.FormResize(Sender: TObject);
begin
  MakePreviewImage;
end;

procedure TFmObtemFoto.Img0MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  W, H, F: Double;
begin
  ///////////////    I M A G E 0    ////////////////////////////////////////////
  Img0.Picture.Bitmap := nil;
  //
  Img0.Transparent := true;
  Img0.Canvas.Pen.Color := clLime;
  Img0.Canvas.Pen.Width := 1;
  //Img0.Canvas.Pen.Mode := pmMask; { makes semi-transparent, but wrong color }
  //Img0.Canvas.Rectangle(Rect(x,y,x,y));
  MouseIsDown := True;
  PDown := Point(x,y);
  PActually := Point(x,y);
  //
  ///////////////    I M A G E 2    ////////////////////////////////////////////
  //
  W := Img2.Width / Img2.Picture.Bitmap.Width;
  H := Img2.Height / Img2.Picture.Bitmap.Height;
  //
  F := H / W;
  fH := 1;
  fW := 1;
  if F > 0 then
    fH := F
  else
    fW := -F;
  pInicio.Y := Trunc(Y / Img2.Height * Img2.Picture.Bitmap.Height * fH);
  pInicio.X := Trunc(X / Img2.Width * Img2.Picture.Bitmap.Width * fW);
end;

procedure TFmObtemFoto.Img0MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  ///////////////    I M A G E 0    ////////////////////////////////////////////
  if MouseIsDown then
  begin
    Img0.Picture := nil;
    Img0.Canvas.Pen.Color := clLime;
    Img0.Canvas.Pen.Width := 1;
    if X > Img0.Width then
      X := Img0.Width
    else
    if X < 0 then
      X := 0;
    if Y > Img0.Height then
      Y := Img0.Height
    else
    if Y < 0 then
      Y := 0;
    if (PDown.X <= PActually.X) and (PDown.Y <= PActually.Y) then
    begin
      //Img0.Canvas.Rectangle(Rect(PDown.X,PDown.Y,PActually.X,PActually.Y));
      PActually := Point(x,y);
      //Img0.Canvas.Rectangle(Rect(PDown.X,PDown.Y,x,y));
    end
    else
    begin
      if (PDown.X >= PActually.X) and (Pdown.Y >= PActually.Y) then
      begin
        //Img0.Canvas.Rectangle(Rect(PActually.X,PActually.Y,PDown.X, PDown.Y));
        PActually := Point(x,y);
        //Img0.Canvas.Rectangle(Rect(x,y,PDown.X,PDown.Y));
      end
      else
      begin
        if (PDown.X >= PActually.X) and (PDown.Y <= PActually.Y) then
        begin
          //Img0.Canvas.Rectangle(Rect(PActually.X,PDown.Y,PDown.X, PActually.Y));
          PActually := Point(x,y);
          //Img0.Canvas.Rectangle(Rect(x,PDown.Y,PDown.X,y));
        end
        else
        begin
          if (PDown.X <= PActually.X) and (PDown.Y >= PActually.Y) then
          begin
            //Img0.Canvas.Rectangle(Rect(PDown.X,PActually.Y,Pactually.X,  PDown.Y));
            PActually := Point(x,y);
            //Img0.Canvas.Rectangle(Rect(PDown.X,y,x,PDown.Y));
          end;
        end;
      end;
    end;
  end;
  //
  ///////////////    I M A G E 2    ////////////////////////////////////////////
  EdW.ValueVariant := X;
  EdH.ValueVariant := Y;
end;

procedure TFmObtemFoto.Img0MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
  ///////////////    I M A G E 0    ////////////////////////////////////////////
    //Img0.Canvas.Rectangle(Rect(PDown.X,PDown.Y,PActually.X,PActually.Y));
    //Img0.Canvas.Rectangle(Rect(PDown.X,PDown.Y,x,y));
    MouseIsDown := False;
    Img0.Canvas.MoveTo(50,10);
    Img0.Canvas.LineTo(100,20);
    Img0.Canvas.LineTo(200,30);
  ///////////////    I M A G E 2    ////////////////////////////////////////////
    //
    pFim.Y := Trunc(Y / Img2.Height * Img2.Picture.Bitmap.Height * fH);
    pFim.X := Trunc(X / Img2.Width * Img2.Picture.Bitmap.Width * fW);
    //
    if (pFim.Y <= pInicio.Y) or (pFim.X <= pInicio.X) (*or
    (pFim.Y = pInicio.Y) or (pFim.X = pInicio.X)*) then
      Exit;
    RecortaParteBitmap();
    ImgLoaded:= True;
    MakePreviewImage;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmObtemFoto.Img1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //Img1.Picture := nil;
  Img1.Transparent := true;
  Img1.Canvas.Pen.Color := clLime;
  Img1.Canvas.Pen.Width := 1;
  Img1.Canvas.Rectangle(Rect(x,y,x,y));
  MouseIsDown := true;
  PDown := Point(x,y);
  PActually := Point(x,y);
end;

procedure TFmObtemFoto.Img1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
   if MouseIsDown then
   begin
     Img1.Picture := nil;
     Img1.Canvas.Pen.Color := clLime;
     Img1.Canvas.Pen.Width := 1;
     if X > Img1.Width then
       X := Img1.Width
     else
       if X < 0 then
         X := 0;
     if Y > Img1.Height then
       Y := Img1.Height
     else
       if Y < 0 then
         Y := 0;
     if (PDown.X <= PActually.X) and (PDown.Y <= PActually.Y) then
     begin
       Img1.Canvas.Rectangle(Rect(PDown.X,PDown.Y,PActually.X,PActually.Y));
       PActually := Point(x,y);
       Img1.Canvas.Rectangle(Rect(PDown.X,PDown.Y,x,y));
     end
     else
     begin
       if (PDown.X >= PActually.X) and (Pdown.Y >= PActually.Y) then
       begin
         Img1.Canvas.Rectangle(Rect(PActually.X,PActually.Y,PDown.X, PDown.Y));
         PActually := Point(x,y);
         Img1.Canvas.Rectangle(Rect(x,y,PDown.X,PDown.Y));
       end
       else
       begin
         if (PDown.X >= PActually.X) and (PDown.Y <= PActually.Y) then
         begin
           Img1.Canvas.Rectangle(Rect(PActually.X,PDown.Y,PDown.X, PActually.Y));
           PActually := Point(x,y);
           Img1.Canvas.Rectangle(Rect(x,PDown.Y,PDown.X,y));
         end
         else
         begin
           if (PDown.X <= PActually.X) and (PDown.Y >= PActually.Y) then
           begin
             Img1.Canvas.Rectangle(Rect(PDown.X,PActually.Y,Pactually.X,  PDown.Y));
             PActually := Point(x,y);
             Img1.Canvas.Rectangle(Rect(PDown.X,y,x,PDown.Y));
           end;
         end;
       end;
     end;
   end;
end;

procedure TFmObtemFoto.Img1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Img1.Canvas.Rectangle(Rect(PDown.X,PDown.Y,PActually.X,PActually.Y));
  Img1.Canvas.Rectangle(Rect(PDown.X,PDown.Y,x,y));
  MouseIsDown := False;
end;

procedure TFmObtemFoto.tbBrightChange(Sender: TObject);
begin
  DoBCS;
end;

procedure TFmObtemFoto.TimerTimer(Sender: TObject);
var
  position: int64;
  Hour, Min, Sec, MSec: Word;
const MiliSecInOneDay = 86400000;
begin
  if CaptureGraph.Active then
  begin
    with CaptureGraph as IMediaSeeking do
      GetCurrentPosition(position);
    DecodeTime(position div 10000 / MiliSecInOneDay, Hour, Min, Sec, MSec);
    StatusBar.SimpleText := Format('%d:%d:%d:%d',[Hour, Min, Sec, MSec]);
  end;
end;

procedure TFmObtemFoto.VideoCapFiltersClick(Sender: TObject);
var
  PinList: TPinList;
  i: integer;
begin
  CapEnum.SelectGUIDCategory(CLSID_VideoInputDeviceCategory);
  if VideoCapFilters.ItemIndex <> -1 then
  begin
    VideoSourceFilter.BaseFilter.Moniker := CapEnum.GetMoniker(VideoCapFilters.ItemIndex);
    VideoSourceFilter.FilterGraph := CaptureGraph;
    CaptureGraph.Active := true;
    PinList := TPinList.Create(VideoSourceFilter as IBaseFilter);
    VideoFormats.Clear;
    VideoMediaTypes.Assign(PinList.First);
    for i := 0 to VideoMediaTypes.Count - 1 do
      VideoFormats.Items.Add(VideoMediaTypes.MediaDescription[i]);
    CaptureGraph.Active := false;
    PinList.Free;
    StartButton.Enabled := true;
  end;
end;

function TFmObtemFoto.BmpToJpeg_1(BMP: TBitmap): TJPegImage;
begin
  Result := TJpegImage.Create;
  Result.CompressionQuality := EdQualidade.ValueVariant;
  Result.Compress;
  Result.Assign(BMP);
end;

procedure TFmObtemFoto.btnOpenClick(Sender: TObject);
{
var
  Jpg: TJpegImage;
  Extensao: String;
}
begin
{
  if dlgOpen.Execute then
  begin
    Extensao := UpperCase(ExtractFileExt(dlgOpen.FileName));
    if (Extensao = '.JPG') or (Extensao = '.JPEG') then
    begin
      Jpg:= TJpegImage.Create;
      Jpg.LoadFromFile(dlgOpen.FileName);
      Img3.Picture.Bitmap.PixelFormat:= pf24Bit;
      Img3.Picture.Bitmap.Width:= Jpg.Width;
      Img3.Picture.Bitmap.Height:= Jpg.Height;
      Img3.Canvas.Draw(0,0, Jpg);
      Jpg.Free;
    end
    else
      Img3.Picture.LoadFromFile(dlgOpen.FileName);
    imgLoaded:= True;
  end;
  MakePreviewImage;
}
end;

end.
