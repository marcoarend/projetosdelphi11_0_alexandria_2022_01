object FmObtemFoto3: TFmObtemFoto3
  Left = 0
  Top = 0
  Caption = 'XXX-XXXXX-999 ::  Captura de Imagem Fotogr'#225'fica'
  ClientHeight = 562
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 784
    Height = 562
    ActivePage = TabSheet6
    Align = alClient
    TabOrder = 0
    object TabSheet7: TTabSheet
      Caption = 'Logs'
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 534
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Img1: TImage
          Left = 172
          Top = 0
          Width = 604
          Height = 534
          Align = alClient
          Proportional = True
          Stretch = True
          ExplicitLeft = 2
          ExplicitWidth = 211
          ExplicitHeight = 475
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 534
          Align = alLeft
          Caption = 'Panel5'
          TabOrder = 0
          object Memo3: TMemo
            Left = 1
            Top = 380
            Width = 170
            Height = 153
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object Memo2: TMemo
            Left = 1
            Top = 154
            Width = 170
            Height = 226
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
          end
          object Memo4: TMemo
            Left = 1
            Top = 1
            Width = 170
            Height = 153
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 2
          end
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = ' Captura da imagem'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 61
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 65
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 61
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 307
              Height = 61
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object dmkLabelRotate1: TdmkLabelRotate
                Left = 0
                Top = 0
                Width = 17
                Height = 61
                Angle = ag90
                Caption = 'C'#226'mera'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Align = alLeft
                ExplicitTop = -4
              end
              object LBVideoCapFilters: TListBox
                Left = 18
                Top = 5
                Width = 244
                Height = 51
                ItemHeight = 13
                TabOrder = 0
                OnClick = LBVideoCapFiltersClick
              end
              object StartButton: TButton
                Left = 263
                Top = 5
                Width = 40
                Height = 25
                Caption = 'Inicia'
                Enabled = False
                TabOrder = 1
                OnClick = StartButtonClick
              end
              object StopButton: TButton
                Left = 263
                Top = 31
                Width = 40
                Height = 25
                Caption = 'P'#225'ra'
                Enabled = False
                TabOrder = 2
                OnClick = StopButtonClick
              end
            end
            object Panel13: TPanel
              Left = 307
              Top = 0
              Width = 186
              Height = 61
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object RGRotate: TRadioGroup
                Left = 0
                Top = 0
                Width = 105
                Height = 61
                Align = alLeft
                Caption = 'Rota'#231'ao em graus: '
                Columns = 2
                ItemIndex = 1
                Items.Strings = (
                  '0'
                  '90'
                  '180'
                  '270')
                TabOrder = 0
              end
              object RGMethod: TRadioGroup
                Left = 105
                Top = 0
                Width = 80
                Height = 61
                Align = alLeft
                Caption = 'M'#233'todo:'
                ItemIndex = 0
                Items.Strings = (
                  'ScanLine'
                  'CopyRect'
                  'StretchBlt')
                TabOrder = 1
              end
            end
            object GroupBox1: TGroupBox
              Left = 493
              Top = 0
              Width = 184
              Height = 61
              Align = alLeft
              Caption = 'Valores m'#225'ximos:'
              TabOrder = 2
              object Label7: TLabel
                Left = 10
                Top = 16
                Width = 52
                Height = 13
                Caption = 'Qualidade:'
              end
              object Label8: TLabel
                Left = 68
                Top = 16
                Width = 41
                Height = 13
                Caption = 'Largura:'
              end
              object Label9: TLabel
                Left = 125
                Top = 16
                Width = 33
                Height = 13
                Caption = 'Altura:'
              end
              object EdQualidade: TdmkEdit
                Left = 10
                Top = 33
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '100'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '75'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 75
                ValWarn = False
              end
              object EdMaxAltu: TdmkEdit
                Left = 68
                Top = 32
                Width = 52
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '768'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 768
                ValWarn = False
                OnChange = EdMaxLargChange
              end
              object EdMaxLarg: TdmkEdit
                Left = 125
                Top = 33
                Width = 52
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1024'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1024
                ValWarn = False
                OnChange = EdMaxAltuChange
              end
            end
            object Panel14: TPanel
              Left = 677
              Top = 0
              Width = 99
              Height = 61
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 3
              object CkReverse: TCheckBox
                Left = 7
                Top = 8
                Width = 72
                Height = 17
                Caption = 'Reverse'
                TabOrder = 0
              end
              object CkFlip: TCheckBox
                Left = 7
                Top = 25
                Width = 72
                Height = 17
                Caption = 'Flip'
                TabOrder = 1
              end
              object CkCallBack: TCheckBox
                Left = 7
                Top = 41
                Width = 72
                Height = 17
                Caption = 'CkCallBack'
                TabOrder = 2
              end
            end
          end
        end
      end
      object Panel15: TPanel
        Left = 0
        Top = 61
        Width = 776
        Height = 473
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 53
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label5: TLabel
              Left = 4
              Top = 2
              Width = 88
              Height = 13
              Caption = 'Formato do v'#237'deo:'
            end
            object LaTempoFilmando: TLabel
              Left = 8
              Top = 38
              Width = 54
              Height = 13
              Caption = '0:00:00:00'
            end
            object Label13: TLabel
              Left = 80
              Top = 38
              Width = 12
              Height = 13
              Caption = '...'
            end
            object LabelTimer: TLabel
              Left = 468
              Top = 38
              Width = 12
              Height = 13
              Caption = '...'
            end
            object CBVideoFormats: TComboBox
              Left = 4
              Top = 16
              Width = 765
              Height = 21
              TabOrder = 0
            end
          end
        end
        object Panel17: TPanel
          Left = 648
          Top = 53
          Width = 128
          Height = 420
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object Panel4: TPanel
            Left = 0
            Top = 372
            Width = 128
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object BtSaida: TBitBtn
              Tag = 13
              Left = 4
              Top = 5
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtSalvaConfig: TBitBtn
            Tag = 24
            Left = 4
            Top = 0
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Salva Config.'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtSalvaConfigClick
          end
          object BtAtualiza: TBitBtn
            Tag = 18
            Left = 4
            Top = 42
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Atualiz. C'#226'mera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtAtualizaClick
          end
          object BtFotoPrepara: TBitBtn
            Tag = 14
            Left = 4
            Top = 84
            Width = 120
            Height = 80
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = 'Fotografa'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtFotoPreparaClick
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Recorte da Imagem '
      ImageIndex = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 534
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object PaintBox1: TPaintBox
          Left = 0
          Top = 0
          Width = 776
          Height = 534
          Align = alClient
          OnMouseDown = PaintBox1MouseDown
          OnMouseMove = PaintBox1MouseMove
          OnMouseUp = PaintBox1MouseUp
          OnPaint = PaintBox1Paint
          ExplicitLeft = 511
          ExplicitTop = 2
          ExplicitWidth = 267
          ExplicitHeight = 475
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = ' Salvar Imagem '
      ImageIndex = 3
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 534
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Img3: TImage
          Left = 172
          Top = 0
          Width = 604
          Height = 534
          Align = alClient
          Center = True
          Proportional = True
          Stretch = True
          ExplicitLeft = 152
          ExplicitTop = 24
          ExplicitWidth = 487
          ExplicitHeight = 710
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 534
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PnAjusta: TPanel
            Left = 0
            Top = 0
            Width = 172
            Height = 213
            Align = alTop
            BevelOuter = bvNone
            Color = clWhite
            Enabled = False
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 16
              Top = 84
              Width = 30
              Height = 13
              Caption = 'Brilho:'
            end
            object Label2: TLabel
              Left = 17
              Top = 125
              Width = 52
              Height = 13
              Caption = 'Contraste:'
            end
            object Label3: TLabel
              Left = 17
              Top = 164
              Width = 53
              Height = 13
              Caption = 'Satura'#231#227'o:'
            end
            object Label4: TLabel
              Left = 16
              Top = 25
              Width = 101
              Height = 13
              Caption = 'Ajuste de exposi'#231#227'o:'
            end
            object tbBright: TTrackBar
              Left = 9
              Top = 96
              Width = 154
              Height = 31
              Max = 512
              Frequency = 16
              Position = 325
              TabOrder = 0
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
            object radAuto: TRadioButton
              Left = 5
              Top = 4
              Width = 76
              Height = 17
              Caption = 'Auto ajuste'
              TabOrder = 1
              OnClick = tbBrightChange
            end
            object radManuell: TRadioButton
              Left = 16
              Top = 66
              Width = 113
              Height = 17
              Caption = 'Ajuste manual'
              Checked = True
              TabOrder = 2
              TabStop = True
              OnClick = tbBrightChange
            end
            object tbContrast: TTrackBar
              Left = 9
              Top = 136
              Width = 154
              Height = 30
              Max = 100
              Min = -100
              Frequency = 10
              Position = 14
              TabOrder = 3
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
            object tbSaturation: TTrackBar
              Left = 9
              Top = 176
              Width = 154
              Height = 41
              Max = 512
              Frequency = 16
              Position = 255
              TabOrder = 4
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
            object radExp: TRadioButton
              Left = 93
              Top = 3
              Width = 68
              Height = 17
              Caption = 'Exposi'#231#227'o'
              TabOrder = 5
              OnClick = tbBrightChange
            end
            object tbExposure: TTrackBar
              Left = 9
              Top = 37
              Width = 154
              Height = 31
              Max = 512
              Min = -255
              Frequency = 32
              TabOrder = 6
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
          end
          object btnSave: TBitBtn
            Tag = 24
            Left = 3
            Top = 218
            Width = 162
            Height = 40
            Cursor = crHandPoint
            Caption = '&Salva'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btnSaveClick
          end
        end
      end
    end
  end
  object dlgOpen: TOpenPictureDialog
    Left = 220
    Top = 392
  end
  object dlgSave: TSavePictureDialog
    Left = 300
    Top = 392
  end
  object Timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = TimerTimer
    Left = 288
    Top = 176
  end
end
