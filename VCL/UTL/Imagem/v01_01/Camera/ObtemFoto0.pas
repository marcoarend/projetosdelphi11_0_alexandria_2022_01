unit ObtemFoto0;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, CodeBase;

type
  TFmObtemFoto0 = class(TForm)
    PnDados: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEmpCB4: TmySQLQuery;
    DsEmpCB4: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    EdEmpCB4: TdmkEditCB;
    CBEmpCB4: TdmkDBLookupComboBox;
    QrEmpCB4CodCliInt: TIntegerField;
    QrEmpCB4CodEnti: TIntegerField;
    QrEmpCB4NOMEENTI: TWideStringField;
    Image1: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    EdIP_Src: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdHD_Src: TdmkEdit;
    Label4: TLabel;
    EdUserID: TdmkEdit;
    Label5: TLabel;
    EdTercei: TdmkEdit;
    Label6: TLabel;
    EdNFiscl: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure EdEmpCB4Exit(Sender: TObject);
    procedure EdEmpCB4Change(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FCB4: CODE4;
    FData4: DATA4;
    //
    Fld4_Codigo: FIELD4;
    Fld4_IP_Src: FIELD4;
    Fld4_HD_Src: FIELD4;
    Fld4_UserID: FIELD4;
    Fld4_Tercei: FIELD4;
    Fld4_NFiscl: FIELD4;
    Fld4_Imagem: FIELD4;
    //
    FEmpresa: Integer;
    //
    //procedure DefParams;
    //procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure CarregaRegistroCB4();
    //procedure LocalizaRegistro(Codigo: Integer);
    procedure RedefineCB4();
  public
    { Public declarations }
  end;

var
  FmObtemFoto0: TFmObtemFoto0;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, MyDBCheck, CB4_PF;

{$R *.DFM}

{
procedure TFmObtemFoto0.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;
}

procedure TFmObtemFoto0.Va(Para: TVaiPara);
var
  rc: Integer;
begin
  {
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCadastro_SimplesCodigo.Value, LaRegistro.Caption[2]);
  }

  case Para of
    vpFirst : rc := d4top(FData4);
    vpNext  : rc := d4skip(FData4, + 1);
    vpPrior : rc := d4skip(FData4, - 1);
    vpLast  : rc := d4bottom(FData4);
    else rc := -999999999;
  end;
  if rc = 0 then
    CarregaRegistroCB4();
end;

/////////////////////////////////////////////////////////////////////////////////////

{
procedure TFmObtemFoto0.DefParams;
begin
  VAR_GOTOTABELA := 'cadastro_simples';
  VAR_GOTOMYSQLTABLE := QrCadastro_Simples;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cadastro_simples');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;
}

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmObtemFoto0.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmObtemFoto0.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmObtemFoto0.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmObtemFoto0.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmObtemFoto0.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmObtemFoto0.CarregaRegistroCB4();
(*
var
  Codigo: Integer;
*)
begin
  Fld4_Codigo := d4field(FData4, 'CODIGO');
  Fld4_IP_Src := d4field(FData4, 'IP_SRC');
  Fld4_HD_Src := d4field(FData4, 'HD_SRC');
  Fld4_UserID := d4field(FData4, 'USERID');
  Fld4_Tercei := d4field(FData4, 'TERCEI');
  Fld4_NFiscl := d4field(FData4, 'NFISCL');
  Fld4_Imagem := d4field(FData4, 'IMAGEM');
  //
  EdCodigo.ValueVariant := Geral.IMV(f4memoStr(Fld4_Codigo));
  EdIP_Src.ValueVariant := String(   f4memoStr(Fld4_IP_Src));
  EdHD_Src.ValueVariant := String(   f4memoStr(Fld4_HD_Src));
  EdUserID.ValueVariant := Geral.IMV(f4memoStr(Fld4_UserID));
  EdTercei.ValueVariant := Geral.IMV(f4memoStr(Fld4_Tercei));
  EdNFiscl.ValueVariant := Geral.IMV(f4memoStr(Fld4_NFiscl));
  UnCB4_PF.CarregaImagemEmTImage(Fld4_Imagem, Image1);
end;

procedure TFmObtemFoto0.EdEmpCB4Change(Sender: TObject);
begin
  if not EdEmpCB4.Focused then
    RedefineCB4();
end;

procedure TFmObtemFoto0.EdEmpCB4Exit(Sender: TObject);
begin
  RedefineCB4();
end;

procedure TFmObtemFoto0.BtSaidaClick(Sender: TObject);
begin
  //VAR_CADASTRO := QrCadastro_SimplesCodigo.Value;
  Close;
end;

procedure TFmObtemFoto0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpCB4, Dmod.MyDB, [
  'SELECT CodCliInt, CodEnti, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTI ',
  'FROM enticliint eci ',
  'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti ',
  'WHERE eci.CodCliInt IN ( ',
  '     SELECT Empresa ',
  '     FROM cb4data4 ',
  '     WHERE Tabela=' + Geral.FF0(CO_TAB_INDX_CB4_FOTODOCU),
  ') ',
  'ORDER BY NOMEENTI ',
  '']);
end;

procedure TFmObtemFoto0.FormDestroy(Sender: TObject);
begin
  if FCB4 <> nil then
  begin
    code4close(FCB4);
    code4initUndo(FCB4);
    FCB4 := nil;
  end;
end;

procedure TFmObtemFoto0.SbNumeroClick(Sender: TObject);
var
  Num: String;
  Cod, rc: Integer;
(*
  Continua: Boolean;
*)
begin
  Num := InputBox(VAR_GOTOTABELA,'Digite o c�digo.', '' );
  {
  try
    Cod := Geral.IMV(Num);
    Continua := False;
    case VAR_GOTONEG of
      gotoNeg: Continua := Cod < 0;
      gotoNiZ: Continua := Cod <= 0;
      gotoAll: Continua := True;
      gotoNiP: Continua := Cod <> 0;
      gotoPiZ: Continua := Cod >= 0;
      gotoPos: Continua := Cod > 0;
    end;
    //if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not Continua then
    begin
      Geral.MensagemBox('C�digo n�o acess�vel nesta janela!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not GOTOy.LocalizaCodigo(Atual, Cod) then
      Geral.MensagemBox('O registro n� ' + Num +
      ' n�o foi localizado.', 'Erro', MB_OK+MB_ICONERROR)
    else
    begin
      if VAR_GOTOMySQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOMySQLTABLE.DisableControls;
        VAR_GOTOMySQLTABLE.Last;
        VAR_GOTOMySQLTABLE.EnableControls;
        VAR_GOTOMySQLTABLE.First;
      end;
      Result := '[1...]: '+TxtLaRegistro(VAR_GOTOMySQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MensagemBox('"'+Num+
      SMLA_NUMEROINVALIDO, 'Erro', MB_OK+MB_ICONERROR);
  end;
  }
  Cod := Geral.IMV(Num);
  rc := d4seek(FData4, PChar(IntToStr(Cod)));
  if rc = r4success (*or r4after*) then
    //UnGeral.write4ln(Memo1, ['Codigo: ', f4str( Codigo )] );
    CarregaRegistroCB4();
end;

procedure TFmObtemFoto0.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmObtemFoto0.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrCadastro_SimplesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmObtemFoto0.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmObtemFoto0.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmObtemFoto0.SbQueryClick(Sender: TObject);
begin
  {
  LocCod(QrCadastro_SimplesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cadastro_simples', Dmod.MyDB, CO_VAZIO));
  }
end;

procedure TFmObtemFoto0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmObtemFoto0.LocalizaRegistro(Codigo: Integer);
var
   rc, I, J, N: Integer;
begin
   fl4Codigo := d4field(FData4, 'Codigo');
   d4tagSelect(FData4, d4tag(FData4, 'TAG_Codigo'));
   d4top(FData4);
   (*
   N := d4recCount(FData4);
   for I := 1 to 500 do
   begin
     Randomize;
     J := Trunc(Random(N));
   *)
     rc := d4seek(FData4, PChar(IntToStr(Codigo)));
     if rc = r4success (*or r4after*) then
       Memo1.Lines.Add('Codigo: ' + f4str( fl4Codigo ));
   //end;
   //
   code4close(FCB4);
   code4initUndo(FCB4);
end;
}

procedure TFmObtemFoto0.RedefineCB4();
var
  Habilita: Boolean;
begin
  FEmpresa := EdEmpCB4.ValueVariant;
  Habilita := FEmpresa <> 0;
  //
  SpeedButton1.Enabled := Habilita;
  SpeedButton2.Enabled := Habilita;
  SpeedButton3.Enabled := Habilita;
  SpeedButton4.Enabled := Habilita;
  //
  SbImprime.Enabled := Habilita;
  SbNumero.Enabled := Habilita;
  SbNome.Enabled := Habilita;
  SbQuery.Enabled := Habilita;
  //
  if FEmpresa <> 0 then
    UnCB4_PF.AbreTabelaFotos(FEmpresa, FCB4, FData4)
end;

end.

