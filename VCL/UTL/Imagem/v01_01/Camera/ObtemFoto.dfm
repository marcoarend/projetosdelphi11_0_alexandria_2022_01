object FmObtemFoto: TFmObtemFoto
  Left = 0
  Top = 0
  Caption = 'XXX-XXXXX-999 :: Color adjustment demo'
  ClientHeight = 773
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 509
    Height = 773
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 149
      Width = 509
      Height = 624
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Img1: TImage
        Left = 11
        Top = 365
        Width = 279
        Height = 210
        Proportional = True
        Stretch = True
        OnMouseDown = Img1MouseDown
        OnMouseMove = Img1MouseMove
        OnMouseUp = Img1MouseUp
      end
      object Label10: TLabel
        Left = 207
        Top = 295
        Width = 75
        Height = 13
        Caption = 'Registro no DB:'
      end
      object Label11: TLabel
        Left = 292
        Top = 22
        Width = 41
        Height = 13
        Caption = 'Largura:'
      end
      object Label12: TLabel
        Left = 292
        Top = 62
        Width = 33
        Height = 13
        Caption = 'Altura:'
      end
      object PaintBox1: TPaintBox
        Left = 304
        Top = 152
        Width = 105
        Height = 105
      end
      object Label13: TLabel
        Left = 12
        Top = 584
        Width = 12
        Height = 13
        Caption = '...'
      end
      object LabelTimer: TLabel
        Left = 12
        Top = 600
        Width = 12
        Height = 13
        Caption = '...'
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 306
        Width = 189
        Height = 57
        Caption = 'Valores m'#225'ximos:'
        TabOrder = 0
        object Label7: TLabel
          Left = 10
          Top = 16
          Width = 52
          Height = 13
          Caption = 'Qualidade:'
        end
        object Label8: TLabel
          Left = 70
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Largura:'
        end
        object Label9: TLabel
          Left = 130
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Altura:'
        end
        object EdQualidade: TdmkEdit
          Left = 12
          Top = 32
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '100'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '75'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 75
          ValWarn = False
        end
        object EdMaxAltu: TdmkEdit
          Left = 68
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '768'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 768
          ValWarn = False
        end
        object EdMaxLarg: TdmkEdit
          Left = 128
          Top = 32
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1024'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1024
          ValWarn = False
        end
      end
      object BtFotoPrepara: TButton
        Left = 204
        Top = 338
        Width = 85
        Height = 25
        Caption = 'Fotografa'
        Enabled = False
        TabOrder = 1
        OnClick = BtFotoPreparaClick
      end
      object RadioGroupMethod: TRadioGroup
        Left = 8
        Top = 250
        Width = 221
        Height = 33
        Caption = 'M'#233'todo:'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'ScanLine'
          'CopyRect'
          'StretchBlt')
        TabOrder = 2
      end
      object RadioGroupRotate: TRadioGroup
        Left = 109
        Top = 218
        Width = 180
        Height = 33
        Caption = 'Rota'#231'ao em graus: '
        Columns = 4
        ItemIndex = 1
        Items.Strings = (
          '0'
          '90'
          '180'
          '270')
        TabOrder = 3
      end
      object CheckBoxFlip: TCheckBox
        Left = 80
        Top = 286
        Width = 52
        Height = 17
        Caption = 'Flip'
        TabOrder = 4
      end
      object CheckBoxReverse: TCheckBox
        Left = 16
        Top = 286
        Width = 61
        Height = 17
        Caption = 'Reverse'
        TabOrder = 5
      end
      object CallBack: TCheckBox
        Left = 136
        Top = 286
        Width = 65
        Height = 17
        Caption = 'CallBack'
        TabOrder = 6
      end
      object PnAjusta: TPanel
        Left = 336
        Top = 6
        Width = 169
        Height = 213
        BevelOuter = bvNone
        Color = clWhite
        Enabled = False
        ParentBackground = False
        TabOrder = 7
        object Label1: TLabel
          Left = 16
          Top = 84
          Width = 30
          Height = 13
          Caption = 'Brilho:'
        end
        object Label2: TLabel
          Left = 17
          Top = 125
          Width = 52
          Height = 13
          Caption = 'Contraste:'
        end
        object Label3: TLabel
          Left = 17
          Top = 164
          Width = 53
          Height = 13
          Caption = 'Satura'#231#227'o:'
        end
        object Label4: TLabel
          Left = 16
          Top = 25
          Width = 101
          Height = 13
          Caption = 'Ajuste de exposi'#231#227'o:'
        end
        object tbBright: TTrackBar
          Left = 9
          Top = 96
          Width = 154
          Height = 31
          Max = 512
          Frequency = 16
          Position = 255
          TabOrder = 0
          TickMarks = tmTopLeft
          OnChange = tbBrightChange
        end
        object radAuto: TRadioButton
          Left = 5
          Top = 4
          Width = 76
          Height = 17
          Caption = 'Auto ajuste'
          TabOrder = 1
          OnClick = tbBrightChange
        end
        object radManuell: TRadioButton
          Left = 16
          Top = 66
          Width = 113
          Height = 17
          Caption = 'Ajuste manual'
          TabOrder = 2
          OnClick = tbBrightChange
        end
        object tbContrast: TTrackBar
          Left = 9
          Top = 136
          Width = 154
          Height = 30
          Max = 100
          Min = -100
          Frequency = 10
          TabOrder = 3
          TickMarks = tmTopLeft
          OnChange = tbBrightChange
        end
        object tbSaturation: TTrackBar
          Left = 9
          Top = 176
          Width = 154
          Height = 41
          Max = 512
          Frequency = 16
          Position = 255
          TabOrder = 4
          TickMarks = tmTopLeft
          OnChange = tbBrightChange
        end
        object radExp: TRadioButton
          Left = 93
          Top = 3
          Width = 68
          Height = 17
          Caption = 'Exposi'#231#227'o'
          Checked = True
          TabOrder = 5
          TabStop = True
          OnClick = tbBrightChange
        end
        object tbExposure: TTrackBar
          Left = 9
          Top = 37
          Width = 154
          Height = 31
          Max = 512
          Min = -255
          Frequency = 32
          TabOrder = 6
          TickMarks = tmTopLeft
          OnChange = tbBrightChange
        end
      end
      object Panel4: TPanel
        Left = 12
        Top = 222
        Width = 93
        Height = 25
        TabOrder = 8
        object StatusBar: TStatusBar
          Left = 1
          Top = 1
          Width = 91
          Height = 19
          Align = alTop
          Panels = <>
          SimplePanel = True
        end
      end
      object dmkEdit1: TdmkEdit
        Left = 207
        Top = 311
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = dmkEdit1Exit
      end
      object btnSave: TButton
        Left = 336
        Top = 222
        Width = 169
        Height = 25
        Caption = 'Salva'
        TabOrder = 10
        OnClick = btnSaveClick
      end
      object Button1: TButton
        Left = 336
        Top = 266
        Width = 169
        Height = 25
        Caption = 'Imprime'
        TabOrder = 11
        OnClick = Button1Click
      end
      object EdW: TdmkEdit
        Left = 288
        Top = 38
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = dmkEdit1Exit
      end
      object EdH: TdmkEdit
        Left = 288
        Top = 78
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = dmkEdit1Exit
      end
      object Panel7: TPanel
        Left = 295
        Top = 297
        Width = 210
        Height = 279
        BevelOuter = bvNone
        TabOrder = 13
        object Img2: TImage
          Left = 0
          Top = 0
          Width = 210
          Height = 279
          Align = alClient
          Proportional = True
          Stretch = True
          ExplicitTop = -28
        end
        object Img0: TImage
          Left = 0
          Top = 0
          Width = 210
          Height = 279
          Align = alClient
          Proportional = True
          Stretch = True
          Transparent = True
          OnMouseDown = Img0MouseDown
          OnMouseMove = Img0MouseMove
          OnMouseUp = Img0MouseUp
          ExplicitLeft = 1
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 509
      Height = 149
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object VideoFormats: TListBox
        Left = 0
        Top = 49
        Width = 509
        Height = 100
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 509
        Height = 49
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label5: TLabel
          Left = 59
          Top = 4
          Width = 41
          Height = 13
          Caption = 'C'#226'mera:'
        end
        object Label6: TLabel
          Left = 1
          Top = 36
          Width = 99
          Height = 13
          Caption = 'Formato da captura:'
        end
        object VideoCapFilters: TListBox
          Left = 106
          Top = 2
          Width = 244
          Height = 45
          ItemHeight = 13
          TabOrder = 0
          OnClick = VideoCapFiltersClick
        end
        object StartButton: TButton
          Left = 356
          Top = 12
          Width = 75
          Height = 25
          Caption = 'Inicia'
          Enabled = False
          TabOrder = 1
          OnClick = StartButtonClick
        end
        object StopButton: TButton
          Left = 432
          Top = 12
          Width = 75
          Height = 25
          Caption = 'P'#225'ra'
          Enabled = False
          TabOrder = 2
          OnClick = StopButtonClick
        end
      end
    end
  end
  object PageControl1: TPageControl
    Left = 509
    Top = 0
    Width = 499
    Height = 773
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 491
        Height = 745
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object PaintBox2: TPaintBox
          Left = 1
          Top = 1
          Width = 489
          Height = 743
          Align = alClient
          ExplicitLeft = 158
          ExplicitTop = 24
          ExplicitWidth = 333
          ExplicitHeight = 497
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Foto final'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 491
        Height = 745
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Img3: TImage
          Left = 0
          Top = 0
          Width = 491
          Height = 745
          Align = alClient
          Center = True
          Visible = False
          ExplicitLeft = 152
          ExplicitTop = 24
          ExplicitWidth = 487
          ExplicitHeight = 710
        end
        object Img4: TImage
          Left = 0
          Top = 0
          Width = 491
          Height = 745
          Align = alClient
          Center = True
          Proportional = True
          ExplicitLeft = 2
        end
        object btnOpen: TButton
          Left = 176
          Top = 40
          Width = 75
          Height = 25
          Caption = 'Open...'
          TabOrder = 0
          Visible = False
          OnClick = btnOpenClick
        end
        object BtSnapShot: TButton
          Left = 176
          Top = 12
          Width = 125
          Height = 25
          Caption = 'Fotografa (1'#186' teste)'
          Enabled = False
          TabOrder = 1
          Visible = False
          OnClick = BtSnapShotClick
        end
      end
    end
  end
  object dlgOpen: TOpenPictureDialog
    Left = 456
    Top = 32
  end
  object dlgSave: TSavePictureDialog
    Left = 536
    Top = 32
  end
  object MyTest: TmySQLDatabase
    DatabaseName = 'test'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'PWD=wkljweryhvbirt'
      'UID=root'
      'DatabaseName=test')
    DatasetOptions = []
    Left = 912
    Top = 168
  end
  object TbMyBlobs: TmySQLTable
    Database = MyTest
    Filter = 'Codigo=0'
    Filtered = True
    TableName = 'myblobs'
    Left = 940
    Top = 168
    object TbMyBlobsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbMyBlobsImagem: TBlobField
      FieldName = 'Imagem'
      Size = 4
    end
  end
  object DsMyBlobs: TDataSource
    DataSet = TbMyBlobs
    Left = 968
    Top = 168
  end
  object Timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = TimerTimer
    Left = 264
    Top = 256
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    FileName = 'C:/teste.pdf'
    UseFileCache = False
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbedFontsIfProtected = False
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = False
    Quality = 95
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 72
    Top = 112
  end
  object frxReport1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41065.439579675930000000
    ReportOptions.LastChange = 41065.439579675930000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  Picture1.LoadFromFile('#39'C://Teste5.jpeg'#39');                     ' +
        '     '
      'end.')
    Left = 104
    Top = 112
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Picture1: TfrxPictureView
        Width = 793.701300000000000000
        Height = 1122.520410000000000000
        ShowHint = False
        Stretched = False
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
    end
  end
end
