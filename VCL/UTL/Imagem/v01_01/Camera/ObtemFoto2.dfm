object FmObtemFoto2: TFmObtemFoto2
  Left = 0
  Top = 0
  Caption = 'XXX-XXXXX-999 ::  Captura de Imagem Fotogr'#225'fica'
  ClientHeight = 562
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 784
    Height = 562
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    object TabSheet7: TTabSheet
      Caption = 'Logs'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 534
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Img1: TImage
          Left = 172
          Top = 0
          Width = 604
          Height = 534
          Align = alClient
          Proportional = True
          Stretch = True
          ExplicitLeft = 2
          ExplicitWidth = 211
          ExplicitHeight = 475
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 534
          Align = alLeft
          Caption = 'Panel5'
          TabOrder = 0
          object Memo3: TMemo
            Left = 1
            Top = 380
            Width = 170
            Height = 153
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object Memo2: TMemo
            Left = 1
            Top = 154
            Width = 170
            Height = 226
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
          end
          object Memo4: TMemo
            Left = 1
            Top = 1
            Width = 170
            Height = 153
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 2
          end
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = ' Captura da imagem'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 61
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 65
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 61
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 307
              Height = 61
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object dmkLabelRotate1: TdmkLabelRotate
                Left = 0
                Top = 0
                Width = 17
                Height = 61
                Angle = ag90
                Caption = 'C'#226'mera'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Align = alLeft
                ExplicitTop = -4
              end
              object LBVideoCapFilters: TListBox
                Left = 18
                Top = 5
                Width = 244
                Height = 51
                ItemHeight = 13
                TabOrder = 0
                OnClick = LBVideoCapFiltersClick
              end
              object StartButton: TButton
                Left = 263
                Top = 5
                Width = 40
                Height = 25
                Caption = 'Inicia'
                Enabled = False
                TabOrder = 1
                OnClick = StartButtonClick
              end
              object StopButton: TButton
                Left = 263
                Top = 31
                Width = 40
                Height = 25
                Caption = 'P'#225'ra'
                Enabled = False
                TabOrder = 2
                OnClick = StopButtonClick
              end
            end
            object Panel13: TPanel
              Left = 307
              Top = 0
              Width = 186
              Height = 61
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object RGRotate: TRadioGroup
                Left = 0
                Top = 0
                Width = 105
                Height = 61
                Align = alLeft
                Caption = 'Rota'#231'ao em graus: '
                Columns = 2
                ItemIndex = 1
                Items.Strings = (
                  '0'
                  '90'
                  '180'
                  '270')
                TabOrder = 0
              end
              object RGMethod: TRadioGroup
                Left = 105
                Top = 0
                Width = 80
                Height = 61
                Align = alLeft
                Caption = 'M'#233'todo:'
                ItemIndex = 0
                Items.Strings = (
                  'ScanLine'
                  'CopyRect'
                  'StretchBlt')
                TabOrder = 1
              end
            end
            object GroupBox1: TGroupBox
              Left = 493
              Top = 0
              Width = 184
              Height = 61
              Align = alLeft
              Caption = 'Valores m'#225'ximos:'
              TabOrder = 2
              object Label7: TLabel
                Left = 10
                Top = 16
                Width = 52
                Height = 13
                Caption = 'Qualidade:'
              end
              object Label8: TLabel
                Left = 70
                Top = 16
                Width = 41
                Height = 13
                Caption = 'Largura:'
              end
              object Label9: TLabel
                Left = 130
                Top = 16
                Width = 33
                Height = 13
                Caption = 'Altura:'
              end
              object EdQualidade: TdmkEdit
                Left = 12
                Top = 32
                Width = 53
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '100'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '75'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 75
                ValWarn = False
              end
              object EdMaxAltu: TdmkEdit
                Left = 68
                Top = 32
                Width = 52
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '768'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 768
                ValWarn = False
                OnChange = EdMaxLargChange
              end
              object EdMaxLarg: TdmkEdit
                Left = 126
                Top = 33
                Width = 52
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1024'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1024
                ValWarn = False
                OnChange = EdMaxAltuChange
              end
            end
            object Panel14: TPanel
              Left = 677
              Top = 0
              Width = 99
              Height = 61
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 3
              object CkReverse: TCheckBox
                Left = 7
                Top = 8
                Width = 72
                Height = 17
                Caption = 'Reverse'
                TabOrder = 0
              end
              object CkFlip: TCheckBox
                Left = 7
                Top = 25
                Width = 72
                Height = 17
                Caption = 'Flip'
                TabOrder = 1
              end
              object CkCallBack: TCheckBox
                Left = 7
                Top = 41
                Width = 72
                Height = 17
                Caption = 'CkCallBack'
                TabOrder = 2
              end
            end
          end
        end
      end
      object Panel15: TPanel
        Left = 0
        Top = 61
        Width = 776
        Height = 473
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 53
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 53
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label5: TLabel
              Left = 4
              Top = 2
              Width = 88
              Height = 13
              Caption = 'Formato do v'#237'deo:'
            end
            object LaTempoFilmando: TLabel
              Left = 8
              Top = 38
              Width = 54
              Height = 13
              Caption = '0:00:00:00'
            end
            object Label13: TLabel
              Left = 80
              Top = 38
              Width = 12
              Height = 13
              Caption = '...'
            end
            object LabelTimer: TLabel
              Left = 468
              Top = 38
              Width = 12
              Height = 13
              Caption = '...'
            end
            object CBVideoFormats: TComboBox
              Left = 4
              Top = 16
              Width = 765
              Height = 21
              TabOrder = 0
            end
          end
        end
        object Panel17: TPanel
          Left = 648
          Top = 53
          Width = 128
          Height = 420
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSalvaConfig: TButton
            Left = 4
            Top = 3
            Width = 120
            Height = 40
            Caption = 'Salva Config.'
            TabOrder = 0
            OnClick = BtSalvaConfigClick
          end
          object BtFotoPrepara: TButton
            Left = 4
            Top = 378
            Width = 120
            Height = 40
            Caption = 'Fotografa'
            Enabled = False
            TabOrder = 1
            OnClick = BtFotoPreparaClick
          end
          object Button3: TButton
            Left = 4
            Top = 188
            Width = 121
            Height = 40
            Caption = 'Refresh '#13#10'C'#226'mera'
            TabOrder = 2
            OnClick = Button3Click
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Recorte da Imagem '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 534
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object PaintBox1: TPaintBox
          Left = 172
          Top = 0
          Width = 604
          Height = 534
          Align = alClient
          OnMouseDown = PaintBox1MouseDown
          OnMouseMove = PaintBox1MouseMove
          OnMouseUp = PaintBox1MouseUp
          OnPaint = PaintBox1Paint
          ExplicitLeft = 511
          ExplicitTop = 2
          ExplicitWidth = 267
          ExplicitHeight = 475
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 534
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = ' Salvar Imagem '
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 534
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Img3: TImage
          Left = 172
          Top = 0
          Width = 604
          Height = 534
          Align = alClient
          Center = True
          Proportional = True
          Stretch = True
          ExplicitLeft = 152
          ExplicitTop = 24
          ExplicitWidth = 487
          ExplicitHeight = 710
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 172
          Height = 534
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label10: TLabel
            Left = 3
            Top = 377
            Width = 75
            Height = 13
            Caption = 'Registro no DB:'
          end
          object PnAjusta: TPanel
            Left = 0
            Top = 0
            Width = 172
            Height = 213
            Align = alTop
            BevelOuter = bvNone
            Color = clWhite
            Enabled = False
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 16
              Top = 84
              Width = 30
              Height = 13
              Caption = 'Brilho:'
            end
            object Label2: TLabel
              Left = 17
              Top = 125
              Width = 52
              Height = 13
              Caption = 'Contraste:'
            end
            object Label3: TLabel
              Left = 17
              Top = 164
              Width = 53
              Height = 13
              Caption = 'Satura'#231#227'o:'
            end
            object Label4: TLabel
              Left = 16
              Top = 25
              Width = 101
              Height = 13
              Caption = 'Ajuste de exposi'#231#227'o:'
            end
            object tbBright: TTrackBar
              Left = 9
              Top = 96
              Width = 154
              Height = 31
              Max = 512
              Frequency = 16
              Position = 325
              TabOrder = 0
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
            object radAuto: TRadioButton
              Left = 5
              Top = 4
              Width = 76
              Height = 17
              Caption = 'Auto ajuste'
              TabOrder = 1
              OnClick = tbBrightChange
            end
            object radManuell: TRadioButton
              Left = 16
              Top = 66
              Width = 113
              Height = 17
              Caption = 'Ajuste manual'
              Checked = True
              TabOrder = 2
              TabStop = True
              OnClick = tbBrightChange
            end
            object tbContrast: TTrackBar
              Left = 9
              Top = 136
              Width = 154
              Height = 30
              Max = 100
              Min = -100
              Frequency = 10
              Position = 14
              TabOrder = 3
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
            object tbSaturation: TTrackBar
              Left = 9
              Top = 176
              Width = 154
              Height = 41
              Max = 512
              Frequency = 16
              Position = 255
              TabOrder = 4
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
            object radExp: TRadioButton
              Left = 93
              Top = 3
              Width = 68
              Height = 17
              Caption = 'Exposi'#231#227'o'
              TabOrder = 5
              OnClick = tbBrightChange
            end
            object tbExposure: TTrackBar
              Left = 9
              Top = 37
              Width = 154
              Height = 31
              Max = 512
              Min = -255
              Frequency = 32
              TabOrder = 6
              TickMarks = tmTopLeft
              OnChange = tbBrightChange
            end
          end
          object btnSave: TButton
            Left = 2
            Top = 331
            Width = 162
            Height = 40
            Caption = 'Salva'
            TabOrder = 1
            OnClick = btnSaveClick
          end
          object Button1: TButton
            Left = 4
            Top = 488
            Width = 162
            Height = 40
            Caption = 'Imprime'
            Enabled = False
            TabOrder = 2
            OnClick = Button1Click
          end
          object dmkEdit1: TdmkEdit
            Left = 84
            Top = 374
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = dmkEdit1Exit
          end
          object CkSaveDisco: TCheckBox
            Left = 5
            Top = 465
            Width = 97
            Height = 17
            Caption = 'Salva em disco.'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 4
          end
        end
      end
    end
  end
  object dlgOpen: TOpenPictureDialog
    Left = 220
    Top = 392
  end
  object dlgSave: TSavePictureDialog
    Left = 300
    Top = 392
  end
  object Timer: TTimer
    Enabled = False
    Interval = 1
    OnTimer = TimerTimer
    Left = 288
    Top = 176
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    FileName = 'C:/teste.pdf'
    UseFileCache = False
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbedFontsIfProtected = False
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = False
    Quality = 95
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 104
    Top = 248
  end
  object frxReport1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41065.439579675930000000
    ReportOptions.LastChange = 41099.756537789360000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  Picture1.LoadFromFile('#39'C://Teste5.jpeg'#39');                     ' +
        '     '
      'end.')
    Left = 136
    Top = 248
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Picture1: TfrxPictureView
        Left = 75.590600000000000000
        Top = 37.795300000000000000
        Width = 680.315400000000000000
        Height = 1046.929810000000000000
        ShowHint = False
        Center = True
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
    end
  end
  object TimerCB4: TTimer
    Enabled = False
    Interval = 50
    OnTimer = TimerCB4Timer
    Left = 320
    Top = 176
  end
  object IdIPWatch1: TIdIPWatch
    Active = False
    HistoryEnabled = False
    HistoryFilename = 'iphist.dat'
    Left = 296
    Top = 84
  end
  object TimerLct: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerLctTimer
    Left = 352
    Top = 176
  end
end
