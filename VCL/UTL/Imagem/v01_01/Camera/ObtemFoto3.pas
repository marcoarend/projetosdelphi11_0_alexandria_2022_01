unit ObtemFoto3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls,  Math, ExtDlgs, Jpeg, (*DBImageEx,*)
  dmkGeral, dmkEdit, DB, mySQLDbTables, UnDmkProcFunc, CodeBase,
  // Captura
  DSPack, DSUtil, DirectShow9,
  // Desenho
  ImgMarquee, IdBaseComponent, IdComponent, IdIPWatch, frxClass, frxExportPDF,
  dmkLabelRotate,
  // Filtros
  Buttons;

type
  PRGB24 = ^TRGB24;
  TRGB24 = record B, G, R: Byte; end;
  PRGBArray = ^TRGBArray;
  TRGBArray = array[0..0] of TRGB24;
  THistogram = array[0..255] of Byte;

  TFmObtemFoto3 = class(TForm)
    dlgOpen: TOpenPictureDialog;
    dlgSave: TSavePictureDialog;
    VideoSourceFilter: TFilter;
    CaptureGraph: TFilterGraph;
    SampleGrabber: TSampleGrabber;
    Timer: TTimer;
    PageControl1: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Panel2: TPanel;
    Panel10: TPanel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Img1: TImage;
    Panel1: TPanel;
    Panel7: TPanel;
    Img3: TImage;
    Panel8: TPanel;
    PaintBox1: TPaintBox;
    PnAjusta: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    tbBright: TTrackBar;
    radAuto: TRadioButton;
    radManuell: TRadioButton;
    tbContrast: TTrackBar;
    tbSaturation: TTrackBar;
    radExp: TRadioButton;
    tbExposure: TTrackBar;
    Panel5: TPanel;
    Memo3: TMemo;
    Memo2: TMemo;
    Memo4: TMemo;
    Panel6: TPanel;
    Panel9: TPanel;
    Panel12: TPanel;
    dmkLabelRotate1: TdmkLabelRotate;
    LBVideoCapFilters: TListBox;
    StartButton: TButton;
    StopButton: TButton;
    Panel13: TPanel;
    RGRotate: TRadioGroup;
    RGMethod: TRadioGroup;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdQualidade: TdmkEdit;
    EdMaxAltu: TdmkEdit;
    EdMaxLarg: TdmkEdit;
    Panel14: TPanel;
    CkReverse: TCheckBox;
    CkFlip: TCheckBox;
    CkCallBack: TCheckBox;
    Panel15: TPanel;
    Panel3: TPanel;
    VideoWindow: TVideoWindow;
    CBVideoFormats: TComboBox;
    Label5: TLabel;
    Panel16: TPanel;
    LaTempoFilmando: TLabel;
    Label13: TLabel;
    LabelTimer: TLabel;
    Panel17: TPanel;
    Panel4: TPanel;
    BtSaida: TBitBtn;
    BtSalvaConfig: TBitBtn;
    BtAtualiza: TBitBtn;
    BtFotoPrepara: TBitBtn;
    btnSave: TBitBtn;
    procedure tbBrightChange(Sender: TObject);
    //procedure MakePreviewImage;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure LBVideoCapFiltersClick(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure StopButtonClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure BtFotoPreparaClick(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBox1Paint(Sender: TObject);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EdMaxAltuChange(Sender: TObject);
    procedure EdMaxLargChange(Sender: TObject);
    procedure BtSalvaConfigClick(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    FCB4: CODE4;
    FJaAtivou: Boolean;
    //
    FTempo: TDateTime;
    BitmapOriginal, BitmapRotacionado: TBitmap;
    procedure Log(Memo: TMemo; Texto: String);
    //
    // Captura
    procedure RotacionaOuInverteImagem();
    procedure LimpaFotoCapturada();
    // Desenho
    procedure DrawControl(const Ms: TmsEvent; const X, Y: Integer);
    // Recorte
    procedure RecortaParteBitmap2();
    // Filtro
    procedure DoBCS;
    //
    procedure IniciaCamera();
    procedure ParaCamera();
  public
    { Public declarations }
    FImage: TJPEGImage;
  end;

var
  FmObtemFoto3: TFmObtemFoto3;
  // Captura
  CapEnum: TSysDevEnum;
  VideoMediaTypes: TEnumMediaType;
  // Filtro
  mx,my: array[Word]of Integer;
  img: TBitmap;
  ImgLoaded: Boolean;
  // Selecionar
  PDown, PActually, PIni2, PFim2: TPoint;
  MouseIsDown: Boolean;

implementation

uses UnMyObjects, Module, MyDBCheck;

var
  BitMap1, Bitmap2, PMap: TBitmap;
  DrawFigure: TDrawFigure = mbRectangle;     //select drawing operation
  PControl: Byte;                            //drawing control counter
  BoxRect, DrawRect: TRect;
  BoxFlag: Boolean = False;
  Px1, Py1, Px2, Py2: Integer;               //coordinates from mouse events

{$R *.dfm}

// This is the routine for brightness, contrast and saturation adjustment...
procedure fxBCS(dest: TBitmap; b, c, s: integer);
var
  x, y, i, k, v, pv: Integer;
  ci1, ci2, ci3: integer;
  alpha: integer;
  a: double;
  BrightnesLut: array[0..255] of byte;
  ContrastLut: array[0..255] of byte;
  BCLut: array[0..255] of byte;
  SaturationLut: record
    Grays: array[0..767] of Integer;
    Alpha: array[Byte] of Word;
  end;
  SLDest, DestOff: integer;
  p: pRGB24;
  s1, s2: integer;
  bool: boolean;
  color: tRGB24;
begin
  if c = 100 then c := 99;
  for i := 0 to 255 do begin
    if c > 0 then
      a := 1 / cos(c * (PI / 200))
    else
      a := 1 * cos(c * (3.1416 / 200));
    v := Round(a * (i - 170) + 170);
    if v > 255 then v := 255 else if v < 0 then v := 0;
    ContrastLut[i] := v;
  end;
  for i := 0 to 255 do begin
    alpha := b;
    k := 256 - alpha;
    v := (k + alpha * i) div 256;
    if v < 0 then v := 0 else if v > 255 then  v := 255;
    BCLut[i] := ContrastLut[v];
  end;
  x := 0;
  for i := 1 to 256 do SaturationLut.Alpha[i - 1] := (i * s) shr 8;
  for i := 0 to 255 do begin
    y := i - SaturationLut.Alpha[i];
    SaturationLut.Grays[x] := y;
    Inc(x);
    SaturationLut.Grays[x] := y;
    Inc(x);
    SaturationLut.Grays[x] := y;
    Inc(x);
  end;
  SLDest := Integer(dest.ScanLine[0]);
  DestOff := Integer(dest.ScanLine[1]) - SLDest;
  for y := 0 to Dest.Height - 1 do begin
    for x := 0 to Dest.Width - 1 do begin
      color := pRGBArray(SLDest)[x];
      v := color.R + color.G + color.B;
      ci1 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.B];
      ci2 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.G];
      ci3 := SaturationLut.Grays[v] + SaturationLut.Alpha[color.R];
       if ci1 < 0 then ci1 := 0 else if ci1 > 255 then ci1 := 255;
      if ci2 < 0 then ci2 := 0 else if ci2 > 255 then ci2 := 255;
      if ci3 < 0 then ci3 := 0 else if ci3 > 255 then ci3 := 255;
       PRGBArray(SLDest)[x].B := BCLut[ci1];
      PRGBArray(SLDest)[x].G := BCLut[ci2];
      PRGBArray(SLDest)[x].R := BCLut[ci3];
    end;
    inc(SLDest, DestOff);
  end;
end;

procedure fxHistCalc(src: tBitmap; var histR, HistG, HistB: THistogram);
var
  RGB: PRGBArray;
  x, y: Integer;
begin
  for x := 0 to 255 do begin
    histR[x] := 0;
    histG[x] := 0;
    histB[x] := 0;
  end;
  for y := 0 to src.Height - 1 do begin
    RGB := src.ScanLine[y];
    for x := 0 to src.Width - 1 do begin
      inc(histR[RGB[x].R]);
      inc(histG[RGB[x].G]);
      inc(histB[RGB[x].B]);
    end;
  end;
end;

procedure fxHistEqu(src: tBitmap; z: single);
type
  THistSingle = array[0..255] of Single;
var
  RGB: PRGBArray;
  x, y(*, pv*): integer;
  q1, q2, q3: Single;
  histR, HistG, HistB: Thistogram;
  Hist, VCumSumR, VCumSumG, VCumSumB: THistSingle;
  //r, g, b, cy, ccr, ccb: byte;

  function CumSum(hist: THistSingle): THistSingle;
  var
    x: Byte;
    Temp: THistsingle;
  begin
    Temp[0] := Hist[0];
    for x := 1 to 255 do Temp[x] := Temp[x - 1] + Hist[x];
    CumSum := Temp;
  end;

begin
  fxHistCalc(src, histR, histG, histB);
  q1 := 0;   // RED Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistR[x], z);
    q1 := q1 + Hist[x];
  end;
  vcumsumR := cumsum(Hist);
  q2 := 0;   // GREEN Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistG[x], z);
    q2 := q2 + Hist[x];
  end;
  vcumsumG := cumsum(Hist);
  q3 := 0;   // BLUE Channel
  for x := 0 to 255 do begin
    Hist[x] := power(HistB[x], z);
    q3 := q3 + Hist[x];
  end;
  vcumsumB := cumsum(Hist);
  for y := 0 to src.height - 1 do
  begin
    RGB := src.scanline[y];
    for x := 0 to src.width - 1 do begin
      RGB[x].R := Trunc((255 / q1) * vcumsumR[RGB[x].R]);
      RGB[x].G := Trunc((255 / q2) * vcumsumG[RGB[x].G]);
      RGB[x].B := Trunc((255 / q3) * vcumsumB[RGB[x].B]);
    end;
  end;
end;

procedure fxExposure(src: TBitmap; k: Single);
var
  RGB: PRGBArray;
  i, x, y, RGBOffset: Integer;
  lut: array[0..255] of integer;
begin
  for i := 0 to 255 do begin
    if k < 0 then
      lut[i]:= i - ((-Round((1 - Exp((i / -128)*(k / 128)))*256)*(i xor 255)) shr 8)
    else
      lut[i]:= i + ((Round((1 - Exp((i / -128)*(k / 128)))*256)*(i xor  255)) shr 8);
    if lut[i] < 0 then lut[i] := 0 else if lut[i] > 255 then lut[i] := 255;
  end;
  RGB := src.ScanLine[0];
  RGBOffset := Integer(src.ScanLine[1]) - Integer(RGB);
  for y := 0 to src.Height - 1 do begin
    for x := 0 to src.Width - 1 do begin
      RGB[x].R := LUT[RGB[x].R];
      RGB[x].G := LUT[RGB[x].G];
      RGB[x].B := LUT[RGB[x].B];
    end;
    RGB:= PRGBArray(Integer(RGB) + RGBOffset);
  end;
end;

procedure SmoothReSize(Src, Dest: TBitmap);
var
  x, y, px, py: Integer;
  i, x1, x2, z, z2, iz2: Integer;
  w1, w2, w3, w4: Integer;
  Ratio: Integer;
  sDst, sDstOff: Integer;
  sScanLine: Array of PRGBArray;
  Src1, Src2: PRGBArray;
  C, C1, C2: TRGB24;
begin
  sDst:= Integer(src.ScanLine[0]);
  sDstOff:= Integer(src.ScanLine[1]) - Integer(sDst);
  SetLength(sScanLine, Src.Height);
  for i:= 0 to Src.Height - 1 do begin
    sScanLine[i]:= PRGBArray(sDst);
    sDst:= sDst + sDstOff;
  end;
  sDst:= Integer(Dest.ScanLine[0]);
  sDstOff:= Integer(Dest.ScanLine[1]) - sDst;
  Ratio:= ((Src.Width - 1) shl 15) div Dest.Width;
  py:= 0;
  for y := 0 to Dest.Height - 1 do begin
    Src1:= sScanLine[py shr 15];
    if py shr 15 < Src.Height - 1 then Src2:= sScanLine[py shr 15 + 1] else Src2:= Src1;
    z2:= py and $7FFF;
    iz2:= $8000 - z2;
    px:= 0;
    for x := 0 to Dest.Width - 1 do begin
      x1 := px shr 15;
      x2:= x1 + 1;
      C1:= Src1[x1];
      C2:= Src2[x1];
      z:= px and $7FFF;
      w2:= (z * iz2) shr 15;
      w1:= iz2 - w2;
      w4:= (z * z2) shr 15;
      w3:= z2 - w4;
      C.R:= (C1.R * w1 + Src1[x2].R * w2 + C2.R * w3 + Src2[x2].R * w4) shr 15;
      C.G:= (C1.G * w1 + Src1[x2].G * w2 + C2.G * w3 + Src2[x2].G * w4) shr 15;
      C.B:= (C1.B * w1 + Src2[x2].B * w2 + C2.B * w3 + Src2[x2].B * w4) shr 15;
      PRGBArray(sDst)[x]:= C;
      Inc(px, Ratio);
    end;
    sDst:= sDst + SDstOff;
    Inc(py, Ratio);
  end;
  SetLength(sScanLine, 0);
end;

function CalcImgSize(w, h, tw, th: integer): TPoint;
begin
  Result.X := 0;
  Result.Y := 0;
  if (w < tw) and (h < th) then begin
    Result.X := w;
    Result.Y := h;
  end
  else if (w = 0) or (h = 0) then Exit
  else begin
    if w > h then begin
      if w < tw then tw := w;
      Result.X := tw;
      Result.Y := Trunc(tw * h / w);
      if Result.Y > th then begin
        Result.Y := th;
        Result.X := Trunc(th * w / h);
      end;
    end else begin
      if h < th then th := h;
      Result.Y := th;
      Result.X := Trunc(th * w / h);
      if Result.X > tw then begin
        Result.X := tw;
        Result.Y := Trunc(tw * h / w);
      end;
    end;
  end;
end;

procedure TFmObtemFoto3.btnSaveClick(Sender: TObject);
begin
  FImage := TJPEGImage.Create;
  FImage.Assign(Img3.Picture.Bitmap);
  //
  Close;
end;

procedure TFmObtemFoto3.BtFotoPreparaClick(Sender: TObject);
begin
  Memo4.Text := '';
  Log(Memo4, 'Preparando captura');
  Img3.Picture.Bitmap := nil;
  Img1.Picture.Bitmap := nil;
  Label13.Caption := 'Iniciando captura....';
  //
  Log(Memo4, 'Capturando');
  FTempo := Now();
  SampleGrabber.GetBitmap(Img1.Picture.Bitmap);
  Log(Memo4, 'Captura: W=' + Geral.FF0(Img1.Picture.Bitmap.Width) + ' H=' + Geral.FF0(Img1.Picture.Bitmap.Height));
  Log(Memo4, 'Copiando imagem');
  BitmapOriginal.Assign(Img1.Picture.Bitmap);
  if BitmapOriginal.PixelFormat = pf24bit then
  begin
    RGMethod.ItemIndex := 0;
    RGMethod.Enabled := True
  end
  else
  begin
    RGMethod.ItemIndex := 1;
    // Force CopyRect to be only method available
    RGMethod.Enabled := False;
  end;
  dmkPF.ResizeImage_Bitmap_Novo(BitmapOriginal, EdMaxLarg.ValueVariant, EdMaxAltu.ValueVariant);
  if BitmapOriginal.Width > BitmapOriginal.Height then
  begin
    Log(Memo4, 'Rotacionando c�pia');
    RotacionaOuInverteImagem();
    Img1.Picture.Graphic := BitmapRotacionado;
  end else
  begin
    Img1.Picture.Graphic := BitmapOriginal;
  end;
  FTempo := Now() - FTempo;
  Label13.Caption := 'Captura finalizada em: ' + FormatDateTime('hh:nn:ss:zzz', FTempo);
  Log(Memo4, 'Mostrando imagem');
  UnImgMarquee.InitMaps(Img1.Picture.Bitmap, Bitmap1, Bitmap2, PaintBox1);
  Log(Memo4, 'Captura finalizada!');
  //
  TabSheet2.TabVisible := True;
  PageControl1.ActivePageIndex := 2;
end;

procedure TFmObtemFoto3.DoBCS;
var
  tmp, bmp: TBitmap;
begin
  bmp:= TBitmap.Create;
  bmp.Assign(img);
  bmp.PixelFormat:= pf24Bit;
  tmp:= TBitmap.Create;
  tmp.PixelFormat:= pf24Bit;
  tmp.Width:= Img3.Picture.Bitmap.Width;
  tmp.Height:= Img3.Picture.Bitmap.Height;
  //
  SmoothReSize(bmp, tmp);
  if radAuto.Checked then
    fxHistEqu(tmp, 0.3)
  else if radExp.Checked then
    fxExposure(tmp, tbExposure.Position)
  else
    fxBCS(tmp, tbBright.Position, tbContrast.Position, tbSaturation.Position);
  Img3.Picture.Bitmap.Assign(tmp);
  Img3.Refresh;
  bmp.Free;
  tmp.Free;
end;

procedure TFmObtemFoto3.DrawControl(const Ms: TmsEvent; const X, Y: Integer);
begin
  UnImgMarquee.DrawControl(Ms, X, Y, PaintBox1,
    PaintBox1.Canvas, DrawRect, BitMap1, BitMap2,
    PControl, Px1, Py1, Px2, Py2, PMap, BoxRect, BoxFlag, DrawFigure);
end;

procedure TFmObtemFoto3.EdMaxAltuChange(Sender: TObject);
begin
  LimpaFotoCapturada();
  Img1.Height := EdMaxLarg.ValueVariant;
end;

procedure TFmObtemFoto3.EdMaxLargChange(Sender: TObject);
begin
  LimpaFotoCapturada();
  Img1.Width := EdMaxLarg.ValueVariant;
end;

procedure TFmObtemFoto3.PaintBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  PIni2.X := X;
  PIni2.Y := Y;
  //
  Memo2.Text := '';
  Log(Memo2, '===== Iniciando marca��o =====');
  Log(Memo2, 'W = ' + Geral.FF0(PaintBox1.Width));
  Log(Memo2, 'H = ' + Geral.FF0(PaintBox1.Height));
  Log(Memo2, 'T = ' + Geral.FF0(X));
  Log(Memo2, 'L = ' + Geral.FF0(Y));
  //
  Img3.Picture.Bitmap := nil;
  UnImgMarquee.LimpaImagem(Img1.Picture.Bitmap, Bitmap1, Bitmap2,
    PaintBox1, PControl, BoxFlag);
  DrawControl(msDown, X, Y);
  //
  Log(Memo2, '=== FIM Iniciando marca��o ===');
end;

procedure TFmObtemFoto3.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  DrawControl(msMove, X, Y);
end;

procedure TFmObtemFoto3.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  PFim2.X := X;
  PFim2.Y := Y;
  if (pIni2.X = pFim2.X) or (pIni2.Y = pFim2.Y) then
    Exit;
  if ((PFim2.X - PIni2.X) <= 0) or ((PFim2.Y - PIni2.Y) <= 0) then
    Exit; 
  Log(Memo2, '===== Finalizando Marca��o =====');
  Log(Memo2, 'W = ' + Geral.FF0(PaintBox1.Width));
  Log(Memo2, 'H = ' + Geral.FF0(PaintBox1.Height));
  Log(Memo2, 'T = ' + Geral.FF0(X));
  Log(Memo2, 'L = ' + Geral.FF0(Y));
  //
  DrawControl(msUp, X, Y);
  //
  Log(Memo2, '===== FIM Finalizando Marca��o =====');
  Screen.Cursor := crHourGlass;
  try
    Log(Memo2, '======= Recortando Imagem =======');
    RecortaParteBitmap2();
    ImgLoaded:= True;
    //MakePreviewImage;
    DoBCS;
    Log(Memo2, 'Img Ori: W=' + Geral.FF0(Img3.Picture.Bitmap.Width) + ' H=' + Geral.FF0(Img3.Picture.Bitmap.Height));
    Log(Memo2, 'Rec BCD: W=' + Geral.FF0(Img1.Picture.Bitmap.Width) + ' H=' + Geral.FF0(Img1.Picture.Bitmap.Height));
    Log(Memo2, '===== FIM Recortando Imagem =====');
  finally
    Screen.Cursor := crDefault;
  end;
  //////////////////////////////////////////////////////////////////////////////
  TabSheet3.TabVisible := True;
  PageControl1.ActivePageIndex := 3;
  PnAjusta.Enabled := True;
end;

procedure TFmObtemFoto3.PaintBox1Paint(Sender: TObject);
begin
  PaintBox1.Canvas.Draw(0, 0, Bitmap2);
end;

procedure TFmObtemFoto3.ParaCamera;
begin
  Timer.Enabled := false;
  StopButton.Enabled := false;
  StartButton.Enabled := true;
  CaptureGraph.Stop;
  CaptureGraph.Active := False;
  CBVideoFormats.Enabled := true;
  LBVideoCapFilters.Enabled := true;
  //
  BtFotoPrepara.Enabled := True;
end;

procedure TFmObtemFoto3.RotacionaOuInverteImagem();
var
  StartTick: Cardinal;
begin
  StartTick := GetTickCount;

  // First Flip and/or reverse the bitmap.
  // Treat BitmapFlipReverse functions much like a TBitmap.Create.
  case RGMethod.ItemIndex of
    0:  BitmapRotacionado := FlipReverseScanLine(
        CkFlip.Checked, CkReverse.Checked, BitmapOriginal);

    1:  BitmapRotacionado := FlipReverseCopyRect(
        CkFlip.Checked, CkReverse.Checked, BitmapOriginal);

    2:  BitmapRotacionado := FlipReverseStretchBlt(
        CkFlip.Checked, CkReverse.Checked, BitmapOriginal);
    else
      // Should never happen.  Silence the compiler warning.
      (*BitmapFlipReverse*)BitmapRotacionado := nil
  end;

  if RGRotate.Visible then
  begin
    // Rotate 0, 90, 180, 270 degrees counterclockwise
    (*BitmapRotate*)BitmapRotacionado := RotateScanLine90(90*RGRotate.ItemIndex,
                                     (*BitmapFlipReverse*)BitmapRotacionado);
    //ImgRotacionado.Picture.Graphic := (*BitmapRotate*)BitmapRotacionado;
    //BitmapRotate.Free
  end
  else ;//ImgRotacionado.Picture.Graphic := (*BitmapFlipReverse*)BitmapRotacionado;

//  A refresh may help reduce flicker on some screens but adds quite a bit
//  to the total time.
//  ImageModified.Refresh;
  LabelTimer.Caption := IntToStr(GetTickCount - StartTick) + ' ms.';
end;

procedure TFmObtemFoto3.RecortaParteBitmap2();
var
  mW, mH, mP, mI: Double;
  pW, pH, iW, iH,
  BaseW, BaseH, IniX, IniY, FimX, FimY: Integer;
  procedure CopiaRetangulo();
  var
    I, J, I1, J1: Integer;
  begin
    Img.Width := FimX - IniX;
    Log(Memo2, 'Img.Width := ' + Geral.FF0(FimX) + ' - ' + Geral.FF0(IniX));
    Log(Memo2, 'Img.Width := ' + Geral.FF0(Img.Width));

    Img.Height := FimY - IniY;
    Log(Memo2, 'Img.Height := ' + Geral.FF0(FimY) + ' - ' + Geral.FF0(IniY));
    Log(Memo2, 'Img.Height := ' + Geral.FF0(Img.Height));

    Img.PixelFormat := pf24bit;
    //
    I1 := -1;
    for I := IniX to FimX do
    begin
      I1 := I1 + 1;
      J1 := -1;
      for J := IniY to FimY do
      begin
        J1 := J1 + 1;
        Img.Canvas.Pixels[I1, J1] := Img1.Picture.Bitmap.Canvas.Pixels[I, J];
      end;
    end;
    Img3.Picture.Assign(Img);
    Log(Memo2, 'Recorte: W=' + Geral.FF0(Img3.Picture.Bitmap.Width) + ' H=' + Geral.FF0(Img3.Picture.Bitmap.Height));
  end;
begin
  pW := PaintBox1.Width;
  pH := PaintBox1.Height;
  iW := Img1.Picture.Bitmap.Width;
  iH := Img1.Picture.Bitmap.Height;
  if (pW > iW) and (pH > iH) then
  begin
    BaseW := iW;
    BaseH := iH;
  end else
  if (pW = iW) and (pH = iH) then
  begin
    BaseW := iW;
    BaseH := iH;
  end else
  begin
    mW := pW / iW;
    mH := pH / iH;
    //
    mP := pH / pW;
    mI := iH / iW;
    //
    if mP < mI then
    begin
      if pH < iH then
      begin
        BaseH := pH;
        if iH = 0 then
          BaseW := 0
        else
          BaseW := Trunc(pW * (mP / mI));
      end
      else
      begin
        BaseW := 0;
        BaseH := 0;
      end;
    end else
    begin
      if mW < mH then
      begin
        if pW > iW then
        begin
          BaseH := pH;
          if iH = 0 then
            BaseW := 0
          else
            BaseW := Trunc(iW * (pH / iH));
        end
        else
        begin
          BaseW := pW;
          if iW = 0 then
            BaseH := 0
          else
            BaseH := Trunc(iH * (pW / iW));
        end;
      end else
      begin
        BaseW := 0;
        BaseH := 0;
      end;
    end;
  end;
  Log(Memo2, 'Base W = ' + Geral.FF0(BaseW));
  Log(Memo2, 'Base H = ' + Geral.FF0(BaseH));
  //
  IniX := Trunc((pIni2.X / BaseW) * iW);
  IniY := Trunc((pIni2.Y / BaseH) * iH);
  FimX := Trunc((pFim2.X / BaseW) * iW);
  FimY := Trunc((pFim2.Y / BaseH) * iH);
  //
  Log(Memo2, 'Ini X = ' + Geral.FF0(IniX));
  Log(Memo2, 'Ini Y = ' + Geral.FF0(IniY));

  Log(Memo2, 'Fim X = ' + Geral.FF0(FimX));
  Log(Memo2, 'Fim Y = ' + Geral.FF0(FimY));
  //

{

###///////
###///////
###///////

===== Preparando Marca��o =====
W = 1309
H = 424
T = 1
L = 0
=== FIM Preparando marca��o ===
===== Preparando Marca��o =====
W = 1309
H = 424
T = 318
L = 424
=== FIM Copiando imagem conforme marca��o ===
Img: W=768 H=1024



###
###
###
///
///
///
///
===== Preparando Marca��o =====
W = 254
H = 754
T = 1
L = 0
=== FIM Preparando marca��o ===
===== Preparando Marca��o =====
W = 254
H = 754
T = 254
L = 339
=== FIM Copiando imagem conforme marca��o ===
Img: W=768 H=1024
}
  CopiaRetangulo();
end;

procedure TFmObtemFoto3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmObtemFoto3.BtSalvaConfigClick(Sender: TObject);
var
  VideoCapFilter, VideoFormat: String;
begin
  if LBVideoCapFilters.ItemIndex > -1 then
    VideoCapFilter := LBVideoCapFilters.Items[LBVideoCapFilters.ItemIndex]
  else
    VideoCapFilter := '';
  //
  VideoFormat := CBVideoFormats.Text;
  //
  Geral.WriteAppKeyLM('VideoCapFilter', 'Dermatek\WebCams', VideoCapFilter,ktString);
  Geral.WriteAppKeyLM('VideoFormat', 'Dermatek\WebCams', VideoFormat, ktString);
  Geral.WriteAppKeyLM('Rotate', 'Dermatek\WebCams', RGRotate.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM('Method', 'Dermatek\WebCams', RGMethod.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM('Reverse', 'Dermatek\WebCams', Geral.BoolToInt(CkReverse.Checked), ktInteger);
  Geral.WriteAppKeyLM('Flip', 'Dermatek\WebCams', Geral.BoolToInt(CkFlip.Checked), ktInteger);
  Geral.WriteAppKeyLM('CallBack', 'Dermatek\WebCams', Geral.BoolToInt(CkCallBack.Checked), ktInteger);
  Geral.WriteAppKeyLM('Qualidade', 'Dermatek\WebCams', EdQualidade.ValueVariant, ktInteger);
  Geral.WriteAppKeyLM('MaxAltu', 'Dermatek\WebCams', EdMaxAltu.ValueVariant, ktInteger);
  Geral.WriteAppKeyLM('MaxLarg', 'Dermatek\WebCams', EdMaxLarg.ValueVariant, ktInteger);
end;

procedure TFmObtemFoto3.BtAtualizaClick(Sender: TObject);
begin
  CaptureGraph.Stop;
  CaptureGraph.Play;
end;

procedure TFmObtemFoto3.StartButtonClick(Sender: TObject);
begin
  IniciaCamera();
end;

procedure TFmObtemFoto3.StopButtonClick(Sender: TObject);
begin
  ParaCamera();
end;

procedure TFmObtemFoto3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if not FJaAtivou then
    FJaAtivou := True;
end;

procedure TFmObtemFoto3.FormCreate(Sender: TObject);
var
  I, K: Integer;
  VideoFormat, VideoCapFilter: String;
begin
  FImage := nil;
  //
  TabSheet7.TabVisible := False;
  TabSheet2.TabVisible := False;
  TabSheet3.TabVisible := False;
  //
  PageControl1.ActivePageIndex := 1; // Idx 0 = Logs
  // Tem que ser texto, pois os perif�ricos podem mudar de �ndice a cada carregamento!
  VideoCapFilter := Geral.ReadAppKeyLM('VideoCapFilter', 'Dermatek\WebCams', ktString, '');
  VideoFormat := Geral.ReadAppKeyLM('VideoFormat', 'Dermatek\WebCams', ktString, '');
  RGRotate.ItemIndex := Geral.ReadAppKeyLM('Rotate', 'Dermatek\WebCams', ktInteger, 1);
  RGMethod.ItemIndex := Geral.ReadAppKeyLM('Method', 'Dermatek\WebCams', ktInteger, 0);
  CkReverse.Checked := Geral.ReadAppKeyLM('Reverse', 'Dermatek\WebCams', ktInteger, 0) = 1;
  CkFlip.Checked := Geral.ReadAppKeyLM('Flip', 'Dermatek\WebCams', ktInteger, 0) = 1;
  CkCallBack.Checked := Geral.ReadAppKeyLM('CallBack', 'Dermatek\WebCams', ktInteger, 0) = 1;
  EdQualidade.ValueVariant := Geral.ReadAppKeyLM('Qualidade', 'Dermatek\WebCams', ktInteger, 80);
  EdMaxAltu.ValueVariant := Geral.ReadAppKeyLM('MaxAltu', 'Dermatek\WebCams', ktInteger, 768);
  EdMaxLarg.ValueVariant := Geral.ReadAppKeyLM('MaxLarg', 'Dermatek\WebCams', ktInteger, 1024);
  //
  //
  // Desenho
  UnImgMarquee.Prepare(Img1.Picture.Bitmap, BitMap1, BitMap2, PMap, PaintBox1, True);
  // Fim Desenho
  DoubleBuffered := True;
  //
  img:= TBitmap.Create;
  img.PixelFormat:= pf24Bit;
  ImgLoaded:= False;
  //
  // Setup BitmapOriginal in case Flip /Reverse checkboxes used before
  // an image is loaded.
  BitmapOriginal := TBitmap.Create;
  BitmapOriginal.Width := Img1.Width;
  BitmapOriginal.Height := Img1.Height;
  BitmapOriginal.PixelFormat := pf24bit;
  //
  BitmapRotacionado := TBitMap.Create;
  //
  CapEnum := TSysDevEnum.Create(CLSID_VideoInputDeviceCategory);
  K := -1;
  for I := 0 to CapEnum.CountFilters - 1 do
  begin
    LBVideoCapFilters.Items.Add(CapEnum.Filters[i].FriendlyName);
    if VideoCapFilter <> '' then
      if VideoCapFilter = CapEnum.Filters[i].FriendlyName then
        K := I;
  end;

  CapEnum.SelectGUIDCategory(CLSID_AudioInputDeviceCategory);

  VideoMediaTypes := TEnumMediaType.Create;

  if K >= 0 then
  begin
    LBVideoCapFilters.ItemIndex := K;
    LBVideoCapFiltersClick(Self);
  end;
  if VideoFormat <> '' then
  begin
    for I := 0 to CBVideoFormats.Items.Count - 1 do
    begin
      if CBVideoFormats.Items[I] = VideoFormat then
        CBVideoFormats.ItemIndex := I;
    end;
  end;
  if (LBVideoCapFilters.ItemIndex >= 0) and (CBVideoFormats.ItemIndex >= 0) then
    StartButtonClick(Self);
end;

procedure TFmObtemFoto3.FormDestroy(Sender: TObject);
begin
  StopButtonClick(Self);
  BitmapOriginal.Free;
  BitmapRotacionado.Free;
  //
  CapEnum.Free;
  VideoMediaTypes.Free;
  //
  img.Free;
  BitMap1.Free;
  Bitmap2.Free;
  //
  BitMap1 := nil;
  BitMap2 := nil;
  //
  FImage.Free;
  FImage := nil;
  //
  if FCB4 <> nil then
  begin
    code4close(FCB4);
    code4initUndo(FCB4);
    FCB4 := nil;
  end;
end;

procedure TFmObtemFoto3.FormResize(Sender: TObject);
begin
  UnImgMarquee.Prepare(Img1.Picture.Bitmap, BitMap1, BitMap2, PMap, PaintBox1, False);
end;

procedure TFmObtemFoto3.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DrawControl(msDown, X, Y);
end;

procedure TFmObtemFoto3.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
 DrawControl(msMove, X, Y);
end;

procedure TFmObtemFoto3.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DrawControl(msUp, X, Y);
end;

procedure TFmObtemFoto3.IniciaCamera;
var
  PinList: TPinList;
begin
  // Activate the filter graph, at this stage the source filters are added to the graph
  CaptureGraph.Active := True;

  BtSalvaConfig.Enabled := CaptureGraph.Active;
  BtAtualiza.Enabled    := CaptureGraph.Active;
  BtFotoPrepara.Enabled := CaptureGraph.Active;

  // configure output Video media type
  if VideoSourceFilter.FilterGraph <> nil then
  begin
    PinList := TPinList.Create(VideoSourceFilter as IBaseFilter);
    if CBVideoFormats.ItemIndex <> -1 then
      with (PinList.First as IAMStreamConfig) do
        SetFormat(VideoMediaTypes.Items[CBVideoFormats.ItemIndex].AMMediaType^);
    PinList.Free;
  end;

  with CaptureGraph as ICaptureGraphBuilder2 do
    RenderStream(@PIN_CATEGORY_PREVIEW, nil, VideoSourceFilter as IBaseFilter, SampleGrabber as IBaseFilter, VideoWindow as IbaseFilter);
  //
  CaptureGraph.Play;
  StopButton.Enabled := true;
  StartButton.Enabled := false;
  CBVideoFormats.Enabled := false;
  LBVideoCapFilters.Enabled := false;
  //
  BtFotoPrepara.Enabled := True;
  //
  Timer.Enabled := True;
end;

procedure TFmObtemFoto3.LimpaFotoCapturada();
begin
  if (Bitmap1 <> nil) and (Bitmap2 <> nil) then
  begin
    Img1.Picture.Bitmap := nil;
    UnImgMarquee.InitMaps(Img1.Picture.Bitmap, Bitmap1, Bitmap2, PaintBox1);
  end;
end;

procedure TFmObtemFoto3.Log(Memo: TMemo; Texto: String);
begin
  Memo.Lines.Add(Texto);
end;

procedure TFmObtemFoto3.tbBrightChange(Sender: TObject);
begin
  DoBCS();
end;

procedure TFmObtemFoto3.TimerTimer(Sender: TObject);
var
  position: int64;
  Hour, Min, Sec, MSec: Word;
const MiliSecInOneDay = 86400000;
begin
  if CaptureGraph.Active then
  begin
    with CaptureGraph as IMediaSeeking do
      GetCurrentPosition(position);
    DecodeTime(position div 10000 / MiliSecInOneDay, Hour, Min, Sec, MSec);
    LaTempoFilmando.Caption := Format('%d:%d:%d:%d',[Hour, Min, Sec, MSec]);
  end;
end;

procedure TFmObtemFoto3.LBVideoCapFiltersClick(Sender: TObject);
var
  PinList: TPinList;
  i: integer;
begin
  CapEnum.SelectGUIDCategory(CLSID_VideoInputDeviceCategory);
  if LBVideoCapFilters.ItemIndex <> -1 then
  begin
    VideoSourceFilter.BaseFilter.Moniker := CapEnum.GetMoniker(LBVideoCapFilters.ItemIndex);
    VideoSourceFilter.FilterGraph := CaptureGraph;
    CaptureGraph.Active := true;
    PinList := TPinList.Create(VideoSourceFilter as IBaseFilter);
    CBVideoFormats.Clear;
    VideoMediaTypes.Assign(PinList.First);
    for i := 0 to VideoMediaTypes.Count - 1 do
      CBVideoFormats.Items.Add(VideoMediaTypes.MediaDescription[i]);
    CaptureGraph.Active := false;
    PinList.Free;
    StartButton.Enabled := true;
  end;
end;

end.
