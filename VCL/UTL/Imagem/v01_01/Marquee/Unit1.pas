unit Unit1;
{
 smooth, flicker free, drawing demo
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  davArrayBtn, ExtCtrls, jpeg, ImgMarquee;

type
  TForm1 = class(TForm)
    DavArrayBtn1: TDavArrayBtn;
    PaintBox1: TPaintBox;
    Image1: TImage;
    procedure DavArrayBtn1BtnPaint(sender: TObject; BtnNr: Byte;
      status: TBtnStatus);
    procedure FormCreate(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure DavArrayBtn1BtnChange(sender: TObject; BtnNr: Byte;
      status: TBtnStatus; button: TMouseButton);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure DrawControl(const Ms: TmsEvent; const X, Y: Integer);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

var
  BitMap1, Bitmap2, PMap: TBitmap;
  DrawFigure: TDrawFigure = mbOff;     //select drawing operation
  PControl: Byte;                //drawing control counter
  BoxRect, DrawRect: TRect;
  BoxFlag: Boolean = False;
  Px1, Py1, Px2, Py2: Integer;      //coordinates from mouse events

procedure TForm1.DavArrayBtn1BtnChange(sender: TObject; BtnNr: Byte;
  status: TBtnStatus; button: TMouseButton);
//main menu button change
begin
 with davarrayBtn1 do
  begin
   if status <> stDown then DrawFigure := mbOff
    else
     begin
      DrawFigure := TDrawFigure(BtnNr);
      case DrawFigure of
       mbClear :
       begin
         UnImgMarquee.LimpaImagem(Image1.Picture.Bitmap, Bitmap1, Bitmap2,
           PaintBox1, PControl, BoxFlag);
         BtnRelease(byte(DrawFigure));
       end;
       mbLine  : ;
       mbRectangle : ;
       mbEllipse : ;
      end;//case
     end;
  end;//with
end;

procedure TForm1.PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  {
  UnImgMarquee.LimpaImagem(Image1.Picture.Bitmap, Bitmap1, Bitmap2,
    PaintBox1, PControl, BoxFlag);
  }
  DrawControl(msDown, X, Y);
end;

procedure TForm1.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
 DrawControl(msMove, X, Y);
end;

procedure TForm1.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DrawControl(msUp, X, Y);
end;

procedure TForm1.DavArrayBtn1BtnPaint(sender: TObject; BtnNr: Byte;
  status: TBtnStatus);
const dtext : array[0..3] of string =
              ('clear','line','rectangle','ellipse');
var r : Trect;
    x,y : integer;
begin
 with davarrayBtn1 do with canvas do
  begin
   if status = stHI then font.style := [fsBold] else font.style := [];
   r := getBtnRect(BtnNr);
   x := r.left + (btnWidth - textwidth(dtext[btnNr])) div 2;
   y := r.top + (btnheight - textheight(dtext[btnNr])) div 2;
   textout(x,y,dtext[BtnNr]);
  end;
end;

procedure TForm1.DrawControl(const Ms: TmsEvent; const X, Y: Integer);
begin
  UnImgMarquee.DrawControl(Ms, X, Y, TGraphicControl(PaintBox1),
    PaintBox1.Canvas, DrawRect, BitMap1, BitMap2,
    PControl, Px1, Py1, Px2, Py2, PMap, BoxRect, BoxFlag, DrawFigure);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  with DavArrayBtn1.Canvas do
    Font.Height := 20;
  //
  Image1.Picture.Bitmap.LoadFromFile('C:\Teste5.bmp');
  UnImgMarquee.OnCreateForm(BitMap1, BitMap2, PMap, PaintBox1);
  UnImgMarquee.InitMaps(Image1.Picture.Bitmap, Bitmap1, Bitmap2, PaintBox1);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  BitMap1.Free;
  Bitmap2.Free;
end;

procedure TForm1.PaintBox1Paint(Sender: TObject);
begin
  PaintBox1.Canvas.Draw(0, 0, Bitmap2);
end;

procedure TForm1.FormPaint(Sender: TObject);
//paint edge around paintbox
// Contorna o PaintBox1 com uma linha. N�o precisa!
var x1,y1,x2,y2 : integer;
begin
{
 with paintbox1 do
  begin
   x1 := left-1;
   y1 := top-1;
   x2 := left+width;
   y2 := top + height;
  end;
 with canvas do
  begin
   pen.color := $303030;
   pen.width := 1;
   moveto(x1,y1); lineto(x1,y2);
   lineto(x2,y2); lineto(x2,y1); lineto(x1,y1);
  end;
}
end;

end.
