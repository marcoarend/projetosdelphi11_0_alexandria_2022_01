unit DmkSVGEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, Vcl.Menus,  Vcl.ToolWin,
  System.StrUtils, System.Variants, System.Contnrs, RSSVG, RSGdiPlusCtrls,
  RSSVGCtrls, RSSVGTypes, RSGdiPlusGraphicsTypes, dmkEdit, RSGraphics,
  Vcl.Imaging.pngimage, Vcl.ImgList, UnDmkImg;

type
  TRSSVGImageObj = record
   Id: Integer;
   Nome: String;
   ImgArq: String;
   EmUso: Boolean;
   W: Integer;
   H: Integer;
   X: Integer;
   Y: Integer;
  end;
  TFmDmkSVGEditor = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Abrir1: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    ToolBar1: TToolBar;
    TTBAbrir: TToolButton;
    ToolButton5: TToolButton;
    PnDados: TPanel;
    RSSVGDocument1: TRSSVGDocument;
    TTBPainel: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton1: TToolButton;
    CPGPropriedades: TCategoryPanelGroup;
    CPnPropriedades: TCategoryPanel;
    PnObjetos: TPanel;
    Label154: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdX: TdmkEdit;
    EdY: TdmkEdit;
    EdAltura: TdmkEdit;
    EdLargura: TdmkEdit;
    CBUnidade: TComboBox;
    CPnObjetos: TCategoryPanel;
    LVObjetos: TListView;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaintBox1: TPaintBox;
    ToolButton4: TToolButton;
    ILObj: TImageList;
    SBCroqui: TScrollBox;
    RSSVGImage1: TRSSVGImage;
    LaCarregando: TLabel;
    PCDados: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TVSVGImg: TTreeView;
    Edit1: TEdit;
    Panel4: TPanel;
    LaEscala: TLabel;
    BtLimpar: TButton;
    TBEscala: TTrackBar;
    TVObjetos: TListView;
    procedure Fechar1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TTBAbrirClick(Sender: TObject);
    procedure Abrir1Click(Sender: TObject);
    procedure TVSVGImgChange(Sender: TObject; Node: TTreeNode);
    procedure Edit1Change(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure TBEscalaChange(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure RSSVGImage1Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure TTBPainelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure CPGPropriedadesResize(Sender: TObject);
    procedure LVObjetosDblClick(Sender: TObject);
    procedure TVObjetosDblClick(Sender: TObject);
  private
    { Private declarations }
    FRep, FNodePositioning: Boolean;
    FOldPos: TPoint;
    FNodes: TObjectList;
    FCurrentNodeControl: TWinControl;
    procedure Abrir();
    procedure LimparSelecao;
    procedure CarregaTreeView;
    procedure SetNodesVisible(Visible: Boolean);
    procedure ControlMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ControlMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure ControlClick(Sender: TObject);
    procedure ControlMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PositionNodes(AroundControl: TWinControl);
    procedure CreateNodes;
    procedure NodeMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure NodeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure NodeMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CarregaTreeViewItem( E: TSVGElement; tvItem: TTreeNode );
    //
    procedure InsereObjeto(Item: TListItem);
    procedure ConfiguraJanela();
    procedure ConfiguraObjetos();
    //
    procedure SelecionaObjeto(Objeto: TObject);
    //
    procedure InsereObjetos();
    //
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    { Public declarations }
    FSvgImgPosi, FSvgImgNPosi: array of TRSSVGImageObj;
  end;

  var
    FmDmkSVGEditor: TFmDmkSVGEditor;
  const
    CO_Miniatura_Altura  = 32;
    CO_Miniatura_Largura = 32;
    CO_POSI_Esquerda     = 200;
    CO_POSI_Topo         = 200;
    CO_Texto_Carregando  = 'Carregando! Aguarde...';

implementation

uses UnMyObjects, Module, MyGlyfs;

{$R *.DFM}

procedure TFmDmkSVGEditor.CarregaTreeViewItem(E: TSVGElement; tvItem: TTreeNode);
var
  i: Integer;
  Item: TTreeNode;
begin
  for i := 0 to E.Items.Count - 1 do
  begin
    Item := TVSVGImg.Items.AddChildObject(tvItem, E.Items[i].ID + '(' +
              E.Items[i].SVGTypeName + ')', E.Items[i]);
    CarregaTreeViewItem(E.Items[i], Item);
  end;
end;

procedure TFmDmkSVGEditor.SelecionaObjeto(Objeto: TObject);
begin
  if TRSSVGPanel(Objeto).Handle <> 0 then
  begin
    FRep := True;
    //
    SetCapture(TRSSVGPanel(Objeto).Handle);
    GetCursorPos(FOldPos);
    PositionNodes(TWinControl(Objeto));
  end;
end;

procedure TFmDmkSVGEditor.ConfiguraJanela();
begin
  PnObjetos.Enabled := False;
  //
  CBUnidade.Enabled   := False;
  CBUnidade.ItemIndex := 0;
  //
  PnDados.Visible         := False;
  PCDados.ActivePageIndex := 0;
  //
  TVObjetos.Items.Clear;
  TVSVGImg.Items.Clear;
  //Fazer => A escala n�o est� implementado
  LaEscala.Visible := False;
  TBEscala.Visible := False;
end;

procedure TFmDmkSVGEditor.CPGPropriedadesResize(Sender: TObject);
begin
  CPnPropriedades.Height := 200;
  CPnObjetos.Height      := CPGPropriedades.Height - CPnPropriedades.Height;
end;

procedure TFmDmkSVGEditor.CarregaTreeView();
begin
  TVSVGImg.Items.BeginUpdate;
  try
    TVSVGImg.OnChange := nil;
    TVSVGImg.Items.Clear;
    CarregaTreeViewItem(RSSVGDocument1.SVG, nil);
  finally
    TVSVGImg.OnChange := TVSVGImgChange;
    TVSVGImg.Items.EndUpdate;
  end;
end;

procedure TFmDmkSVGEditor.ControlMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Sender is TRSSVGImage) or (Sender is TRSSVGPanel) then
    SelecionaObjeto(Sender);
end;

procedure TFmDmkSVGEditor.ControlMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  newPos: TPoint;
  frmPoint : TPoint;
  NX, NY: Integer;
begin
  if (FRep) and (Sender <> nil) then
  begin
    with TWinControl(Sender) do
    begin
      GetCursorPos(newPos);
      //
      Screen.Cursor := crSize;
      //
      NX := Left - FOldPos.X + newPos.X;
      NY := Top - FOldPos.Y + newPos.Y;
      //
      if NX <= 0 then
        NX := 0 + RSSVGImage1.Margins.Left
      else if NX > (RSSVGImage1.Width - RSSVGImage1.Margins.Right -
        TRSSVGImage(Sender).Margins.Right - TRSSVGImage(Sender).Width)
      then
        NX := RSSVGImage1.Width - RSSVGImage1.Margins.Right -
                TRSSVGImage(Sender).Margins.Right - TRSSVGImage(Sender).Width;
      //
      if NY <= 0 then
        NY := 0 + RSSVGImage1.Margins.Top
      else if NY > (RSSVGImage1.Height - RSSVGImage1.Margins.Bottom -
        TRSSVGImage(Sender).Height - TRSSVGImage(Sender).Margins.Bottom)
      then
        NY := RSSVGImage1.Height - RSSVGImage1.Margins.Bottom -
                TRSSVGImage(Sender).Margins.Bottom - TRSSVGImage(Sender).Height;
      //
      Left    := NX;
      Top     := NY;
      FOldPos := newPos;
    end;
    PositionNodes(TWinControl(Sender));
  end;
end;

procedure TFmDmkSVGEditor.ControlMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FRep then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    FRep := False;
  end;
end;

procedure TFmDmkSVGEditor.ConfiguraObjetos;
var
  Img: String;
  I, ImgIdx: Integer;
  Item: TListItem;
  Bmp: TBitmap;
begin
  Screen.Cursor := crHourGlass;
  try
    LaCarregando.Visible := True;
    LaCarregando.Caption := CO_Texto_Carregando;
    //
    RSSVGImage1.Align := alNone;
    RSSVGImage1.Top   := 0;
    RSSVGImage1.Left  := 0;
    //
    ILObj.Clear;
    ILObj.Height := CO_Miniatura_Altura;
    ILObj.Width  := CO_Miniatura_Largura;
    //
    LVObjetos.Items.Clear;
    LVObjetos.LargeImages := ILObj;
    LVObjetos.ViewStyle   := vsIcon;
    LVObjetos.ReadOnly    := True;
    //
    if Length(FSvgImgNPosi) > 0 then
    begin
      for I := Low(FSvgImgNPosi) to High(FSvgImgNPosi) do
      begin
        if FSvgImgNPosi[I].EmUso = False then
        begin
          Img := FSvgImgNPosi[I].ImgArq;
          Img := ChangeFileExt(Img, '');
          Img := Img + '_min.bmp';
          //
          if FileExists(Img) then
          begin
            ImgIdx := -1;
            Bmp    := TBitmap.Create;
            try
              Bmp.LoadFromFile(Img);
              //
              if (Bmp.Width = CO_Miniatura_Largura) and
                (Bmp.Height = CO_Miniatura_Altura)
              then
                ImgIdx := ILObj.Add(Bmp, nil);
            finally
              Bmp.Free;
            end;
          end else
            ImgIdx := -1;
          //
          Item := LVObjetos.Items.Add;
          Item.Caption    := FSvgImgNPosi[I].Nome;
          Item.StateIndex := I;
          Item.ImageIndex := ImgIdx;
        end;
      end;
    end;
  finally
    LaCarregando.Visible := False;
    LaCarregando.Caption := '';
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDmkSVGEditor.ControlClick(Sender: TObject);
begin
  PnObjetos.Enabled      := True;
  EdX.ValueVariant       := TRSSVGImage(Sender).Left;
  EdY.ValueVariant       := TRSSVGImage(Sender).Top;
  EdAltura.ValueVariant  := TRSSVGImage(Sender).Height;
  EdLargura.ValueVariant := TRSSVGImage(Sender).Width;
  //
  EdX.SetFocus;
end;

procedure TFmDmkSVGEditor.Edit1Change(Sender: TObject);

  function ObtemNodeTexto(ATree : TTreeView; AValue: String;
    AVisible: Boolean): TTreeNode;
  var
    Node: TTreeNode;
  begin
    Result := nil;
    //
    if ATree.Items.Count = 0 then Exit;
    //
    Node := ATree.Items[0];
    //
    while Node <> nil do
    begin
      if ContainsText(Node.Text, AValue) then
      begin
        Result := Node;
        if AVisible then
        begin
          result.Selected := True;
          Result.MakeVisible;
        end;
        Break;
      end;
      Node := Node.GetNext;
    end;
  end;
begin
  ObtemNodeTexto(TVSVGImg, Edit1.Text, True);
end;

procedure TFmDmkSVGEditor.Abrir();
var
  Arquivo: String;
  Dialog: TOpenDialog;
begin
  Dialog := TOpenDialog.Create(nil);
  try
    Dialog.Filter := 'Imagens|*.SVG|Todos Arquivos|*.*';
    //
    if Dialog.Execute then
    begin
      Arquivo := Dialog.FileName;
      //
      SetCurrentDir(ExtractFileDir(Arquivo));
      RSSVGDocument1.DPI := 300;
      RSSVGDocument1.SVG.LoadFromFile(Arquivo);
      RSSVGImage1.SVGRootID := '';
      //
      RSSVGImage1.Top    := 0;
      RSSVGImage1.Left   := 0;
      RSSVGImage1.Height := Round(RSSVGDocument1.SVG.Height);
      RSSVGImage1.Width  := Round(RSSVGDocument1.SVG.Width);
      //
      CarregaTreeView;
   end else
      Exit;
  finally
    Dialog.Free;
  end;
end;

procedure TFmDmkSVGEditor.Abrir1Click(Sender: TObject);
begin
  Abrir();
end;

procedure TFmDmkSVGEditor.BtLimparClick(Sender: TObject);
begin
  LimparSelecao();
end;

procedure TFmDmkSVGEditor.LimparSelecao();
begin
  TVSVGImg.Selected     := nil;
  RSSVGImage1.SVGRootID := '';
end;

procedure TFmDmkSVGEditor.LVObjetosDblClick(Sender: TObject);
var
  Item: TListItem;
begin
  Item := LVObjetos.Selected;
  //
  if Item <> nil then
    InsereObjeto(Item);
end;

procedure TFmDmkSVGEditor.NodeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TWinControl then
  begin
    if TWinControl(Sender).Handle <> 0 then
    begin
      FNodePositioning := True;
      //
      SetCapture(TWinControl(Sender).Handle);
      GetCursorPos(FoldPos);
    end;
  end;
end;

procedure TFmDmkSVGEditor.NodeMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
const
  minWidth = 20;
  minHeight = 20;
var
  newPos: TPoint;
  frmPoint: TPoint;
  OldRect: TRect;
  AdjL, AdjR, AdjT, AdjB: Boolean;
  Larg, Altu, PosX, PosY: Integer;
begin
  if FNodePositioning then
  begin
    if (Sender is TWinControl) then
    begin
      with TWinControl(Sender) do
      begin
        GetCursorPos(newPos);
        //
        with FCurrentNodeControl do
        begin //resize
          frmPoint := FCurrentNodeControl.Parent.ScreenToClient(Mouse.CursorPos);
          OldRect  := FCurrentNodeControl.BoundsRect;
          //
          AdjL := False;
          AdjR := False;
          AdjT := False;
          AdjB := False;
          //
          case FNodes.IndexOf(TWinControl(Sender)) of
            0:
            begin
              AdjL := True;
              AdjT := True;
            end;
            1:
            begin
              AdjT := True;
            end;
            2:
            begin
              AdjR := True;
              AdjT := True;
            end;
            3:
            begin
              AdjR := True;
            end;
            4:
            begin
              AdjR := True;
              AdjB := True;
            end;
            5:
            begin
              AdjB := True;
            end;
            6:
            begin
              AdjL := True;
              AdjB := True;
            end;
            7:
            begin
              AdjL := True;
            end;
          end;
          //
          if AdjL then
            OldRect.Left := frmPoint.X;
          if AdjR then
            OldRect.Right := frmPoint.X;
          if AdjT then
            OldRect.Top := frmPoint.Y;
          if AdjB then
            OldRect.Bottom := frmPoint.Y;
          //
          Larg := OldRect.Right - OldRect.Left;
          Altu := OldRect.Bottom - OldRect.Top;
          //
          if Larg < minWidth then
            Larg := minWidth;
          if Altu < minHeight then
            Altu := minHeight;
          //
          SetBounds(OldRect.Left, OldRect.Top, Larg, Altu);
        end;
        PosX := Left - FOldPos.X + newPos.X;
        PosY := Top - FOldPos.Y + newPos.Y;
        //
        Left    := PosX;
        Top     := PosY;
        FOldPos := newPos;
      end;
      PositionNodes(FCurrentNodeControl);
    end;
  end;
end;

procedure TFmDmkSVGEditor.NodeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FNodePositioning then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    FNodePositioning := False;
  end;
end;

procedure TFmDmkSVGEditor.PositionNodes(AroundControl: TWinControl);
var
  Node, T, L, CT, CL, FR, FB, FT, FL, Y, X: Integer;
  TopLeft: TPoint;
begin
  FCurrentNodeControl := nil;
  //
  for Node := 0 to 7 do
  begin
    with AroundControl do
    begin
      CL := (Width div 2) + Left -2;
      CT := (Height div 2) + Top -2;
      FR := Left + Width - 2;
      FB := Top + Height - 2;
      FT := Top - 2;
      FL := Left - 2;
      //
      case Node of
        0:
        begin
          T := FT;
          L := FL;
        end;
        1:
        begin
          T := FT;
          L := CL;
        end;
        2:
        begin
          T := FT;
          L := FR;
        end;
        3:
        begin
          T := CT;
          L := FR;
        end;
        4:
        begin
          T := FB;
          L := FR;
        end;
        5:
        begin
          T := FB;
          L := CL;
        end;
        6:
        begin
          T := FB;
          L := FL;
        end;
        7:
        begin
          T := CT;
          L := FL;
        end;
        else
          T := 0;
          L := 0;
      end;
      TopLeft := Parent.ClientToScreen(Point(L,T));
    end;
    with TPanel(FNodes[Node]) do
    begin
      TopLeft := Parent.ScreenToClient(TopLeft);
      Top     := TopLeft.Y;
      Left    := TopLeft.X;
    end;
  end;
  FCurrentNodeControl := AroundControl;
  SetNodesVisible(True);
end;

procedure TFmDmkSVGEditor.RSSVGImage1Click(Sender: TObject);
begin
  SetNodesVisible(False);
end;

procedure TFmDmkSVGEditor.SetNodesVisible(Visible: Boolean);
var
  Node: Integer;
begin
  if FNodes <> nil then
  begin
    for Node := 0 to 7 do
    begin
      TWinControl(FNodes.Items[Node]).Visible := Visible;
      //
      if Visible = False then
        PnObjetos.Enabled := False;
    end;
  end;
end;

procedure TFmDmkSVGEditor.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmDmkSVGEditor.FormClick(Sender: TObject);
begin
  SetNodesVisible(False);
end;

procedure TFmDmkSVGEditor.FormCreate(Sender: TObject);
begin
  ToolBar1.Images := FmMyGlyfs.Lista_32X32_Textos;
  //
  Self.KeyPreview := True;
  //
  SetLength(FSvgImgPosi, 0);
  SetLength(FSvgImgNPosi, 0);
  //
  TabSheet2.TabVisible := False;
end;

procedure TFmDmkSVGEditor.FormKeyPress(Sender: TObject; var Key: Char);
begin
  Key := #0;
end;

procedure TFmDmkSVGEditor.FormShow(Sender: TObject);
begin
  ConfiguraJanela;
  ConfiguraObjetos;
end;

procedure TFmDmkSVGEditor.InsereObjeto(Item: TListItem);
var
  Doc: TRSSVGDocument;
  Img: TRSSVGImage;
  ImgSrc, Nome: String;
  Index: Integer;
  ItemObj: TListItem;
begin
  if RSSVGDocument1.SVG <> nil then
  begin
    Index  := Item.StateIndex;
    ImgSrc := FSvgImgNPosi[Index].ImgArq;
    Nome   := 'Obj' + Geral.FF0(Index);
    //
    Doc := TRSSVGDocument.Create(Self);
    Doc.PreferredLanguage := 'EN';
    Doc.SVG.LoadFromFile(ImgSrc);
    //
    Img               := TRSSVGImage.Create(Self);
    Img.Parent        := SBCroqui;
    Img.Tag           := 1;
    Img.Height        := Round(Doc.SVG.Height);
    Img.Width         := Round(Doc.SVG.Width);
    Img.Left          := CO_POSI_Esquerda;
    Img.Top           := CO_POSI_Topo;
    Img.ScaleOriginal := 1;
    Img.SVGDocument   := Doc;
    Img.Name          := Nome;
    Img.ShowHint      := True;
    Img.Hint          := FSvgImgNPosi[Index].Nome;
    //
    Img.OnMouseDown := ControlMouseDown;
    Img.OnMouseMove := ControlMouseMove;
    Img.OnMouseUp   := ControlMouseUp;
    Img.OnClick     := ControlClick;
    //
    FSvgImgNPosi[Index].EmUso := True;
    //
    LVObjetos.Items[Item.Index].Delete;
    //
    ItemObj            := TVObjetos.Items.Add;
    ItemObj.Caption    := Nome;
    ItemObj.StateIndex := Index;
    //
    SetNodesVisible(False);
  end;
end;

procedure TFmDmkSVGEditor.InsereObjetos;
var
  I: Integer;
  Item: TListItem;
  Doc: TRSSVGDocument;
  aMatrix: TSVGMatrix;
  aCanvas: TSVGCanvas;
  Bmp: TBitmap;
  Source, Dest: TRect;
begin
  Doc := TRSSVGDocument.Create(Self);
  Doc.PreferredLanguage := 'EN';
  Doc.SVG.LoadFromFile('C:\Comps\RiverSoftAVG\SVG\Objects\Dragon.svg');
  //
  Bmp     := TBitmap.Create;
  aCanvas := TSVGCanvas.Create(PaintBox1.Canvas);
  try
    aCanvas.SmoothingMode := smHighQuality;
    aMatrix := Doc.SVG.GetViewMatrix(RectToRSRect(PaintBox1.ClientRect));
    aMatrix := RSMatrixMultiply(CreateScaleRSMatrix(1, 1), aMatrix);
    Doc.SVG.Draw(aMatrix, aCanvas, RectToRSRect(PaintBox1.ClientRect));
    //
    Bmp.Width  := PaintBox1.Width;
    Bmp.Height := PaintBox1.Height;
    Dest       := Rect(0, 0, Bmp.Width, Bmp.Height);
    //
    Source := Rect(0, 0, Width, Height);
    Bmp.Canvas.CopyRect(Dest, PaintBox1.Canvas, Source);
  finally
    Bmp.Free;
  end;
  //
  LVObjetos.Items.BeginUpdate;
  try
    LVObjetos.Items.Clear;
    //
    if Length(FSvgImgNPosi) > 0 then
    begin
      for I := Low(FSvgImgNPosi) to High(FSvgImgNPosi) do
      begin
        Item := LVObjetos.Items.Add;
        Item.StateIndex := FSvgImgNPosi[I].Id;
        Item.Caption    := FSvgImgNPosi[I].Nome;
        Item.ImageIndex := 0;
      end;
    end;
  finally
    LVObjetos.Items.EndUpdate;
  end;
end;

procedure TFmDmkSVGEditor.TBEscalaChange(Sender: TObject);
(*
var
  I: Integer;
  OEsc, NEsc: Double;
  CompNome: String;
*)
begin
  (*
  LimparSelecao();
  //
  LaEscala.Caption := 'Escala: ' + FloatToStr((Sender as TTrackBar).Position / 10);
  //
  for I := 0 to FmDmkSVGEditor.ComponentCount - 1 do
  begin
    if FmDmkSVGEditor.Components[I] is TRSSVGImage then
    begin
      NEsc     := (Sender as TTrackBar).Position / 10;
      OEsc     := TRSSVGImage(FmDmkSVGEditor.Components[I]).ScaleOriginal;
      CompNome := TRSSVGImage(FmDmkSVGEditor.Components[I]).Name;
      //
      if NEsc <> OEsc then
      begin
        if Pos('Teste', CompNome) > 0 then
        begin
          if FH = 0 then
            FH := TRSSVGImage(FmDmkSVGEditor.Components[I]).Height;
          if FW = 0 then
            FW := TRSSVGImage(FmDmkSVGEditor.Components[I]).Width;
          if FX = 0 then
            FX := TRSSVGImage(FmDmkSVGEditor.Components[I]).Left;
          if FY = 0 then
            FY := TRSSVGImage(FmDmkSVGEditor.Components[I]).Top;
          //
          if NEsc <> 1 then
          begin
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Height := Round(FH * NEsc);
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Width  := Round(FW * NEsc);
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Left   := Round(FX * NEsc);
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Top    := Round(FY * NEsc);
          end else
          begin
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Height := FH;
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Width  := FW;
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Left   := FX;
            TRSSVGImage(FmDmkSVGEditor.Components[I]).Top    := FY;
          end;
        end else
          TRSSVGImage(FmDmkSVGEditor.Components[I]).ScaleOriginal := NEsc;
      end;
    end;
  end;
  *)
end;

procedure TFmDmkSVGEditor.TTBPainelClick(Sender: TObject);
begin
  PnDados.Visible := TTBPainel.Down;
end;

procedure TFmDmkSVGEditor.ToolButton2Click(Sender: TObject);
const
  ImgSrc = 'C:\Comps\RiverSoftAVG\SVG\Objects\Dragon.svg';
var
  Doc: TRSSVGDocument;
  Img: TRSSVGImage;
begin
  Doc := TRSSVGDocument.Create(Self);
  Doc.PreferredLanguage := 'EN';
  Doc.SVG.LoadFromFile(ImgSrc);
  //
  Img := TRSSVGImage.Create(Self);
  Img.Parent := SBCroqui;
  Img.Tag    := 1;
  Img.Height := 200;
  Img.Width  := 200;
  Img.Left   := 200;
  Img.Top    := 200;
  Img.ScaleOriginal := 1;
  Img.SVGDocument := Doc;
  Img.Name := 'ImgTeste';
  Img.ShowHint := True;
  Img.Hint := 'Teste';
  //
  Img.OnMouseDown := ControlMouseDown;
  Img.OnMouseMove := ControlMouseMove;
  Img.OnMouseUp   := ControlMouseUp;
  //Img.OnClick     := ControlClick;
end;

procedure TFmDmkSVGEditor.ToolButton3Click(Sender: TObject);
var
  Img: TSVGCircle;
begin
  Img := TSVGCircle.Create(RSSVGDocument1.SVG);
  Img.ID := 'Teste';
  RSSVGDocument1.SVG.Items.Add(Img);
  //
  Img.Width  := 100;
  Img.Height := 100;
  Img.X := 10;
  Img.Y := 10;
  //
  RSSVGImage1.SVGRootID := '';
  //
  CarregaTreeView;
end;

procedure TFmDmkSVGEditor.ToolButton4Click(Sender: TObject);
const
  Img = 'C:\Comps\RiverSoftAVG\SVG\Objects\Dragon.svg';
var
  I, ImgW, ImgH: Integer;
  Item: TListItem;
  Doc: TRSSVGDocument;
  aMatrix: TSVGMatrix;
  aCanvas: TSVGCanvas;
  Bmp: TBitmap;
  Source, Dest: TRect;
begin
  Doc := TRSSVGDocument.Create(Self);
  Doc.PreferredLanguage := 'EN';
  Doc.SVG.LoadFromFile(Img);
  //
  Bmp     := TBitmap.Create;
  aCanvas := TSVGCanvas.Create(PaintBox1.Canvas);
  try
    aCanvas.SmoothingMode := smHighQuality;
    aMatrix := Doc.SVG.GetViewMatrix(RectToRSRect(PaintBox1.ClientRect));
    aMatrix := RSMatrixMultiply(CreateScaleRSMatrix(1, 1), aMatrix);
    Doc.SVG.Draw(aMatrix, aCanvas, RectToRSRect(PaintBox1.ClientRect));
    //
    ImgW := Round(RSSVGDocument1.SVG.Width);
    ImgH := Round(RSSVGDocument1.SVG.Height);
    //
    (*
    Bmp.Width  := ImgW;
    Bmp.Height := ImgH;
    *)
    if ImgW > ImgH then
    begin
      Bmp.Width  := ImgW;
      Bmp.Height := ImgW;
    end else
    begin
      Bmp.Width  := ImgH;
      Bmp.Height := ImgH;
    end;
    Dest   := Rect(0, 0, ImgW, ImgH);
    Source := Rect(0, 0, ImgW, ImgH);
    //
    Bmp.Canvas.CopyRect(Dest, PaintBox1.Canvas, Source);
    //
    DmkImg.ResizeImage_Bitmap(Bmp, 32, 32);
    //
    Bmp.SaveToFile('C:\Users\LATITUDE E6520\Desktop\Teste.bmp');
    //
    ILObj.Clear;
    ILObj.Add(Bmp, nil);
  finally
    Bmp.Free;
  end;
  LVObjetos.LargeImages := ILObj;
  //
  LVObjetos.Items.BeginUpdate;
  try
    LVObjetos.Items.Clear;
    //
    if Length(FSvgImgNPosi) > 0 then
    begin
      for I := Low(FSvgImgNPosi) to High(FSvgImgNPosi) do
      begin
        Item := LVObjetos.Items.Add;
        Item.StateIndex := FSvgImgNPosi[I].Id;
        Item.Caption    := FSvgImgNPosi[I].Nome;
        Item.ImageIndex := 0;
      end;
    end;
  finally
    LVObjetos.Items.EndUpdate;
  end;
end;

procedure TFmDmkSVGEditor.TVObjetosDblClick(Sender: TObject);
var
  Comp: TComponent;
  Item: TListItem;
begin
  Item := TVObjetos.Selected;
  //
  if Item <> nil then
  begin
    Comp := FindComponent('Obj' + Geral.FF0(Item.StateIndex));
    //
    if Comp <> nil then
      SelecionaObjeto(Comp);
  end;
end;

procedure TFmDmkSVGEditor.TTBAbrirClick(Sender: TObject);
begin
  Abrir();
end;

procedure TFmDmkSVGEditor.TVSVGImgChange(Sender: TObject; Node: TTreeNode);
begin
  if (TVSVGImg.Selected <> nil) and (TObject(TVSVGImg.Selected.Data) is TSVGGraphicElement) then
    RSSVGImage1.SVGRootID := (TObject(TVSVGImg.Selected.Data) as TSVGGraphicElement).ID;
end;

constructor TFmDmkSVGEditor.Create(AOwner: TComponent);
begin
  inherited;
  FNodes := TObjectList.Create(False);
  CreateNodes;
end; procedure TFmDmkSVGEditor.CreateNodes;
var
  Node: Integer;
  Panel: TPanel;
begin
  for Node := 0 to 7 do
  begin
    Panel := TPanel.Create(Self);
    //
    FNodes.Add(Panel);
    //
    with Panel do
    begin
      BevelOuter := bvNone;
      Color      := clBlack;
      Name       := 'Node' + IntToStr(Node);
      Width      := 5;
      Height     := 5;
      Parent     := Self;
      Visible    := False;

      case Node of
        0,4: Cursor := crSizeNWSE;
        1,5: Cursor := crSizeNS;
        2,6: Cursor := crSizeNESW;
        3,7: Cursor := crSizeWE;
      end;
      OnMouseDown := NodeMouseDown;
      OnMouseMove := NodeMouseMove;
      OnMouseUp   := NodeMouseUp;
    end;
  end;
end;

(*Create*)

destructor TFmDmkSVGEditor.Destroy;
begin
  FNodes.Free;
  inherited;
end; (*Destroy*)

end.
