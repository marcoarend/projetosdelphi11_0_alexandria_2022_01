unit AlignedTStringGrid;

interface

uses Windows,SysUtils,Classes,Grids;

type
  TStringGrid=class(Grids.TStringGrid)
  private
    FCellsAlignment:TStringList;
    FColsDefaultAlignment:TStringList;
    function GetCellsAlignment(ACol,ARow:Integer):TAlignment;
    procedure SetCellsAlignment(ACol,ARow:Integer;const Alignment:TAlignment);
    function GetColsDefaultAlignment(ACol:Integer):TAlignment;
    procedure SetColsDefaultAlignment(ACol:Integer;const Alignment:TAlignment);
  protected
    procedure DrawCell(ACol,ARow:Longint;ARect:TRect;AState:TGridDrawState);override;
  public
    constructor Create(AOwner:TComponent);override;
    destructor Destroy;override;
    property CellsAlignment[ACol,ARow:Integer]:TAlignment read GetCellsAlignment write SetCellsAlignment;
    property ColsDefaultAlignment[ACol:Integer]:TAlignment read GetColsDefaultAlignment write SetColsDefaultAlignment;
  end;

implementation

constructor TStringGrid.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FCellsAlignment:=TStringList.Create;
  FCellsAlignment.CaseSensitive:=True;
  FCellsAlignment.Sorted:=True;
  FCellsAlignment.Duplicates:=dupIgnore;
  FColsDefaultAlignment:=TStringList.Create;
  FColsDefaultAlignment.CaseSensitive:=True;
  FColsDefaultAlignment.Sorted:=True;
  FColsDefaultAlignment.Duplicates:=dupIgnore;
end;

destructor TStringGrid.Destroy;
begin
  FCellsAlignment.Free;
  FColsDefaultAlignment.Free;
  inherited Destroy;
end;

procedure TStringGrid.SetCellsAlignment(ACol,ARow: Integer; const Alignment: TAlignment);
var
  Index:Integer;
begin
  if (-1 < Index) then begin
    FCellsAlignment.Objects[Index]:= TObject(Alignment);
  end else begin
    FCellsAlignment.AddObject(IntToStr(ACol) + '-' + IntToStr(ARow), TObject(Alignment));
  end;
end;

function TStringGrid.GetCellsAlignment(ACol,ARow: Integer): TAlignment;
var
  Index:Integer;
begin
  Index:= FCellsAlignment.IndexOf(IntToStr(ACol)+'-'+IntToStr(ARow));
  if (-1 < Index) then begin
    GetCellsAlignment:= TAlignment(FCellsAlignment.Objects[Index]);
  end else begin
    GetCellsAlignment:= ColsDefaultAlignment[ACol];
  end;
end;

procedure TStringGrid.SetColsDefaultAlignment(ACol: Integer; const Alignment: TAlignment);
var
  Index:Integer;
begin
  Index:= FColsDefaultAlignment.IndexOf(IntToStr(ACol));
  if (-1 < Index) then begin
    FColsDefaultAlignment.Objects[Index]:= TObject(Alignment);
  end else begin
    FColsDefaultAlignment.AddObject(IntToStr(ACol), TObject(Alignment));
  end;
end;

function TStringGrid.GetColsDefaultAlignment(ACol:Integer):TAlignment;
var
  Index:Integer;
begin
  Index:= FColsDefaultAlignment.IndexOf(IntToStr(ACol));
  if (-1 < Index) then begin
    GetColsDefaultAlignment:= TAlignment(FColsDefaultAlignment.Objects[Index]);
  end else begin
    GetColsDefaultAlignment:=taLeftJustify;
  end;
end;

procedure TStringGrid.DrawCell(ACol,ARow:Longint;ARect:TRect;AState:TGridDrawState);
var
  Old_DefaultDrawing:Boolean;
begin
  if DefaultDrawing then begin
    case CellsAlignment[ACol,ARow] of
      taLeftJustify: begin
        Canvas.TextRect(ARect,ARect.Left+2,ARect.Top+2,Cells[ACol,ARow]);
      end;
      taRightJustify: begin
        Canvas.TextRect(ARect,ARect.Right -2 -Canvas.TextWidth(Cells[ACol,ARow]), ARect.Top+2,Cells[ACol,ARow]);
      end;
      taCenter: begin
        Canvas.TextRect(ARect,(ARect.Left+ARect.Right-Canvas.TextWidth(Cells[ACol,ARow]))div 2,ARect.Top+2,Cells[ACol,ARow]);
      end;
    end;
  end;
  Old_DefaultDrawing:= DefaultDrawing;
  DefaultDrawing:=False;
  inherited DrawCell(ACol,ARow,ARect,AState);
  DefaultDrawing:= Old_DefaultDrawing;
end;

end.

