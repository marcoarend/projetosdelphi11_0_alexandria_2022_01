object FmCambioCot: TFmCambioCot
  Left = 339
  Top = 185
  Caption = 'FIN-MOEDA-002 :: Cota'#231#227'o'
  ClientHeight = 438
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 90
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 497
    Height = 65
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 59
    object Label2: TLabel
      Left = 10
      Top = 10
      Width = 26
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data:'
    end
    object TPDataC: TdmkEditDateTimePicker
      Left = 10
      Top = 30
      Width = 128
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 39791.746310937500000000
      Time = 39791.746310937500000000
      TabOrder = 0
      OnClick = TPDataCClick
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 117
    Width = 497
    Height = 205
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    DataSource = DsMoedas
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sigla'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Simbolo'
        Title.Caption = 'S'#237'mbolo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Valor'
        Width = 104
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 497
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 438
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitHeight = 59
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      ExplicitHeight = 59
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 379
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitHeight = 59
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 102
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cota'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 102
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cota'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 102
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cota'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 322
    Width = 497
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 298
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 493
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 376
    Width = 497
    Height = 62
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 4
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 493
      Height = 45
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 18
      ExplicitHeight = 66
      object PnSaiDesis: TPanel
        Left = 316
        Top = 0
        Width = 177
        Height = 45
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 66
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 0
          Width = 148
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 0
        Width = 147
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object dmkValUsu1: TdmkValUsu
    QryCampo = 'GraTamCad'
    UpdCampo = 'GraTamCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
  object DsMoedas: TDataSource
    Left = 40
    Top = 184
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mda.Codigo, mda.CodUsu, mda.Sigla, mda.Nome'
      'FROM cambiomda mda'
      'WHERE NOT (mda.Codigo IN ('
      '  SELECT MoedaBr'
      '  FROM controle))'
      'ORDER BY mda.CodUsu')
    Left = 192
    Top = 136
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCambioMdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 5
    end
    object QrCambioMdaCod_BC: TIntegerField
      FieldName = 'Cod_BC'
    end
    object QrCambioMdaSimbolo: TWideStringField
      FieldName = 'Simbolo'
      Size = 15
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object QrCambioCot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Valor'
      'FROM cambiocot cot'
      'WHERE cot.DataC=:P0'
      '')
    Left = 112
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCambioCotCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCambioCotValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
end
