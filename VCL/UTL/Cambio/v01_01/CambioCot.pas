unit CambioCot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, ComCtrls, dmkEditDateTimePicker, Grids, DBGrids,
  (*&&ABSMain,*) dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCambioCot = class(TForm)
    Panel1: TPanel;
    dmkValUsu1: TdmkValUsu;
    Label2: TLabel;
    TPDataC: TdmkEditDateTimePicker;
    DBGrid1: TDBGrid;
    DsMoedas: TDataSource;
    QrCambioMda: TmySQLQuery;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    QrCambioCot: TmySQLQuery;
    QrCambioCotCodigo: TIntegerField;
    QrCambioCotValor: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrCambioMdaCod_BC: TIntegerField;
    QrCambioMdaSimbolo: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TPDataCClick(Sender: TObject);
    procedure QrMoedasAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCambioMda: Boolean;
    procedure CriaSQLMoedas(Data: TDateTime);
  end;

  var
  FmCambioCot: TFmCambioCot;

implementation

uses UnMyObjects, Module, UMySQLModule, CambioMda, UnInternalConsts, DmkDAC_PF,
ModuleGeral;

{$R *.DFM}

procedure TFmCambioCot.BtOKClick(Sender: TObject);
var
  SQLType: TSQLType;
  Valor: Double;
  Codigo: Integer;
  DataC: String;
begin
(*&&
  QrMoedas.First;
  while not QrMoedas.Eof do
  begin
    if QrMoedasExiste.Value = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //
    Valor  := QrMoedasValor.Value;
    Codigo := QrMoedasCodigo.Value;
    DataC  := Geral.FDT(TPDataC.Date, 1);
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cambiocot', False, [
    'Valor'
    ], ['Codigo', 'DataC'], [
    Valor], [
    Codigo, DataC], True);
    //
    QrMoedas.Next;
  end;
  CriaSQLMoedas(TPDataC.Date);
  Geral.MB_Info('Cota��es atualizadas com sucesso!');
  if FCambioMda then
    FmCambioMda.ReopenCambioCot(TPDataC.Date);
  DModG.ReopenCambio();
  Close;
*)
end;

procedure TFmCambioCot.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCambioCot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCambioCot.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPDataC.Date := Date;
  //
end;

procedure TFmCambioCot.CriaSQLMoedas(Data: TDateTime);
var
  Existe: Byte;
  Valor: Double;
  Insercoes: String;
begin
(*&&
  Insercoes := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCambioCot, Dmod.MyDB, [
  'SELECT Codigo, Valor ',
  'FROM cambiocot cot ',
  'WHERE cot.DataC="' + Geral.FDT(TPDataC.Date, 1) + '"',
  ' ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCambioMda, Dmod.MyDB, [
  'SELECT mda.Codigo, mda.CodUsu, mda.Sigla, ',
  'mda.Simbolo, mda.Cod_BC, mda.Nome ',
  'FROM cambiomda mda ',
  'WHERE NOT (mda.Codigo IN ( ',
  '  SELECT MoedaBr ',
  '  FROM controle)) ',
  'ORDER BY mda.CodUsu ',
  '']);
  //
  while not QrCambioMda.Eof do
  begin
    if QrCambioCot.Locate('Codigo', QrCambioMdaCodigo.Value, []) then
    begin
      Existe := 1;
      Valor  := QrCambioCotValor.Value;
    end else begin
      Existe := 0;
      Valor  := 0;
    end;
    Insercoes := Insercoes +
      'INSERT INTO cambios (' +
      'Existe, Codigo, Sigla, Simbolo, Nome, Valor) VALUES (' +
      Geral.FF0(Existe) + ', ' +
      Geral.FF0(QrCambioMdaCodigo.Value) + ', ' +
      '"' + QrCambioMdaSigla.Value + '", ' +
      '"' + QrCambioMdaSimbolo.Value + '", ' +
      '"' + QrCambioMdaNome.Value + '", ' +
      dmkPF.FFP(Valor, 6) + ');' + sLineBreak;
    //
    QrCambioMda.Next;
  end;
  UnDmkDAC_PF.AbreDataSetQuery(QrMoedas, [
  'DROP TABLE cambios; ',
  'CREATE TABLE cambios (',
  '  Existe    smallint    ,',
  '  Codigo    integer     ,',
  '  Sigla     varchar(5)  ,',
  '  Simbolo   varchar(15) ,',
  '  Nome      varchar(30) ,',
  '  Valor     float       ',
  '); ',
  Insercoes,
  'SELECT * FROM cambios; ',
  '']);
*)
end;

procedure TFmCambioCot.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCambioCot.QrMoedasAfterPost(DataSet: TDataSet);
begin
(*&&
  if (QrMoedasNome.Value = '') and (QrMoedasSigla.Value = '') then
  QrMoedas.Delete;
*)
end;

procedure TFmCambioCot.TPDataCClick(Sender: TObject);
begin
  CriaSQLMoedas(TPDataC.Date);
end;

(*&&
object QrMoedas: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  AfterPost = QrMoedasAfterPost
  RequestLive = True
  Left = 40
  Top = 136
  object QrMoedasExiste: TSmallintField
    FieldName = 'Existe'
  end
  object QrMoedasCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrMoedasSigla: TWideStringField
    FieldName = 'Sigla'
    ReadOnly = True
    Size = 5
  end
  object QrMoedasNome: TWideStringField
    FieldName = 'Nome'
    ReadOnly = True
    Size = 30
  end
  object QrMoedasValor: TFloatField
    FieldName = 'Valor'
    DisplayFormat = '#,###,##0.000000'
  end
  object QrMoedasSimbolo: TWideStringField
    FieldName = 'Simbolo'
    Size = 15
  end
end
*)

end.

