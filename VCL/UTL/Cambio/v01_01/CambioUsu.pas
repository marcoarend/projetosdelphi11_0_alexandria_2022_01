unit CambioUsu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Variants, DB, mySQLDbTables, DmkDAC_PF,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, Mask, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmCambioUsu = class(TForm)
    Panel1: TPanel;
    EdNumero: TdmkEditCB;
    CBNumero: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrSenhas: TmySQLQuery;
    DsSenhas: TDataSource;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSenhasNOMEFUNCI: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCambioUsu: TFmCambioUsu;

implementation

uses UnMyObjects, Module, UMySQLModule, Opcoes;

{$R *.DFM}

procedure TFmCambioUsu.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdNumero.ValueVariant;
  if Codigo = 0 then
  begin
    Geral.MB_Aviso('Informe o login!');
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cambiousu', False, [], ['Codigo'],
  [], [Codigo], True) then
  begin
    FmOpcoes.ReopenCambioUsu(Codigo);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('O login "' + QrSenhasLogin.Value +
      '" foi adicionado com sucesso!');
      QrSenhas.Close;
      UnDmkDAC_PF.AbreQuery(QrSenhas, Dmod.MyDB);
      EdNumero.ValueVariant := 0;
      CBNumero.KeyValue     := Null;
      //
      EdNumero.SetFocus;
    end else Close;
  end;
end;

procedure TFmCambioUsu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCambioUsu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCambioUsu.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrSenhas, Dmod.MyDB);
end;

procedure TFmCambioUsu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
