object FmCambioUsu: TFmCambioUsu
  Left = 339
  Top = 185
  Caption = 'FIN-MOEDA-003 :: Login Cotador de Moeda'
  ClientHeight = 291
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 486
    Height = 129
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Usu'#225'rio'
    end
    object Label2: TLabel
      Left = 12
      Top = 52
      Width = 58
      Height = 13
      Caption = 'Funcion'#225'rio:'
      FocusControl = DBEdit1
    end
    object EdNumero: TdmkEditCB
      Left = 12
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBNumero
      IgnoraDBLookupComboBox = False
    end
    object CBNumero: TdmkDBLookupComboBox
      Left = 72
      Top = 24
      Width = 400
      Height = 21
      KeyField = 'Numero'
      ListField = 'Login'
      ListSource = DsSenhas
      TabOrder = 1
      dmkEditCB = EdNumero
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object DBEdit1: TDBEdit
      Left = 12
      Top = 68
      Width = 57
      Height = 21
      TabStop = False
      DataField = 'Funcionario'
      DataSource = DsSenhas
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 72
      Top = 68
      Width = 400
      Height = 21
      TabStop = False
      DataField = 'NOMEFUNCI'
      DataSource = DsSenhas
      TabOrder = 3
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 100
      Width = 121
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 4
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 486
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 438
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 390
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 299
        Height = 32
        Caption = 'Login Cotador de Moeda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 32
        Caption = 'Login Cotador de Moeda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 299
        Height = 32
        Caption = 'Login Cotador de Moeda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 177
    Width = 486
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 221
    Width = 486
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 338
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sen.Login, sen.Numero, sen.Funcionario, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFUNCI'
      'FROM senhas sen '
      'LEFT JOIN entidades ent ON ent.Codigo=sen.Funcionario'
      'WHERE sen.Numero > 0'
      'AND NOT (sen.Numero IN ('
      '  SELECT Codigo'
      '  FROM cambiousu'
      '))'
      'ORDER BY Login')
    Left = 84
    Top = 76
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Size = 100
    end
  end
  object DsSenhas: TDataSource
    DataSet = QrSenhas
    Left = 112
    Top = 76
  end
end
