unit CambioMda;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCambioMda = class(TForm)
    PnDados: TPanel;
    DsCambioMda: TDataSource;
    QrCambioMda: TmySQLQuery;
    PnEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodUsu: TIntegerField;
    QrCambioMdaNome: TWideStringField;
    DBGItens: TdmkDBGrid;
    PMMoedas: TPopupMenu;
    QrCambioCot: TmySQLQuery;
    DsCambioCot: TDataSource;
    QrCambioMdaSigla: TWideStringField;
    DBEdit2: TDBEdit;
    QrCambioCotDataC: TDateField;
    QrCambioCotValor: TFloatField;
    Incluinovamoeda1: TMenuItem;
    Alteramoedaatual1: TMenuItem;
    Excluimoedaatual1: TMenuItem;
    EdSigla: TdmkEdit;
    Label5: TLabel;
    Panel4: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtMoedas: TBitBtn;
    BtCotacao: TBitBtn;
    EdSimbolo: TdmkEdit;
    Label10: TLabel;
    EdCod_BC: TdmkEdit;
    Label6: TLabel;
    Label11: TLabel;
    QrCambioMdaSimbolo: TWideStringField;
    QrCambioMdaCod_BC: TIntegerField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCambioMdaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCambioMdaBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtMoedasClick(Sender: TObject);
    procedure PMMoedasPopup(Sender: TObject);
    procedure QrCambioMdaBeforeClose(DataSet: TDataSet);
    procedure QrCambioMdaAfterScroll(DataSet: TDataSet);
    procedure BtCotacaoClick(Sender: TObject);
    procedure EdCodUsuExit(Sender: TObject);
    procedure Incluinovamoeda1Click(Sender: TObject);
    procedure Alteramoedaatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCambioCot(Data: TDateTime);
  end;

var
  FmCambioMda: TFmCambioMda;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CambioCot, MyDBCheck, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCambioMda.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCambioMda.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCambioMdaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCambioMda.DefParams;
begin
  VAR_GOTOTABELA := 'CambioMda';
  VAR_GOTOMYSQLTABLE := QrCambioMda;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome, Simbolo, Cod_BC, Sigla');
  VAR_SQLx.Add('FROM cambiomda');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCambioMda.EdCodUsuExit(Sender: TObject);
begin
  DModG.VerificaDuplicidadeCodUsu('CambioMda', 'Nome', ['CodUsu'], EdNome,
  [Geral.IMV(EdCodUsu.Text)], ImgTipo, True);
end;

procedure TFmCambioMda.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'CambioMda', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmCambioMda.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      Pnedita.Visible := False;
    end;
    1:
    begin
      Pnedita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Info('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCambioMda.PMMoedasPopup(Sender: TObject);
begin
  Alteramoedaatual1.Enabled :=
    (QrCambioMda.State <> dsInactive) and (QrCambioMda.RecordCount > 0);
end;

procedure TFmCambioMda.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCambioMda.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCambioMda.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCambioMda.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCambioMda.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCambioMda.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCambioMda.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCambioMda.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCambioMdaCodigo.Value;
  Close;
end;

procedure TFmCambioMda.Alteramoedaatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdit, QrCambioMda, [PnDados],
  [Pnedita], EdSigla, ImgTipo, 'cambiomda');
end;

procedure TFmCambioMda.BtMoedasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMoedas, BtMoedas);
end;

procedure TFmCambioMda.BtConfirmaClick(Sender: TObject);
var
  Sigla, Nome, Simbolo: String;
  Codigo, CodUsu, Cod_BC: Integer;
begin
  Nome           := EdNome.Text;
  Sigla          := EdSigla.Text;
  Simbolo        := EdSimbolo.Text;
  Cod_BC         := EdCod_BC.ValueVariant;
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Sigla = '', EdSigla, 'Defina uma sigla!') then Exit;
  if MyObjects.FIC(Simbolo = '', EdSimbolo, 'Defina uma simbolo!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('CambioMda', 'Codigo', ImgTipo.SQLType,
    QrCambioMdaCodigo.Value);
  CodUsu         := Codigo;

  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cambiomda', False, [
  'CodUsu', 'Sigla', 'Nome',
  'Simbolo', 'Cod_BC'], [
  'Codigo'], [
  CodUsu, Sigla, Nome,
  Simbolo, Cod_BC], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCambioMda.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CambioMda', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CambioMda', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CambioMda', 'Codigo');
end;

procedure TFmCambioMda.BtCotacaoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCambioCot, FmCambioCot, afmoNegarComAviso) then
  begin
    FmCambioCot.FCambioMda := True;
    FmCambioCot.TPDataC.Date := Date;
    FmCambioCot.ShowModal;
    FmCambioCot.Destroy;
  end;
end;

procedure TFmCambioMda.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  DBGItens.Align    := alClient;
  CriaOForm;
end;

procedure TFmCambioMda.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCambioMdaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCambioMda.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCambioMda.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCambioMda.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCambioMdaCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmCambioMda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCambioMda.QrCambioMdaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCambioMda.QrCambioMdaAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := Dmod.QrControle.FieldByName('MoedaBr').AsInteger <> QrCambioMdaCodigo.Value;
  BtCotacao.Enabled := Habilita;
  DBGItens.Visible  := Habilita;
  ReopenCambioCot(Date);
end;

procedure TFmCambioMda.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCambioMda.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCambioMdaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CambioMda', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCambioMda.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCambioMda.Incluinovamoeda1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrCambioMda, [PnDados],
  [Pnedita], EdSigla, ImgTipo, 'cambiomda');
end;

procedure TFmCambioMda.QrCambioMdaBeforeClose(DataSet: TDataSet);
begin
  QrCambioCot.Close;
end;

procedure TFmCambioMda.QrCambioMdaBeforeOpen(DataSet: TDataSet);
begin
  QrCambioMdaCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCambioMda.ReopenCambioCot(Data: TDateTime);
begin
(*
  QrCambioCot.Close;
  QrCambioCot.Params[0].AsInteger :=   QrCambioMdaCodigo.Value;
  QrCambioCot. O p e n ;
  //
*)
  UnDmkDAC_PF.AbremySQLQuery0(QrCambioCot, Dmod.MyDB, [
  'SELECT DataC, Valor ',
  'FROM cambiocot cot ',
  'WHERE cot.Codigo=' + Geral.FF0(QrCambioMdaCodigo.Value),
  '']);
  QrCambioCot.Locate('DataC', Data, []);
end;

end.

