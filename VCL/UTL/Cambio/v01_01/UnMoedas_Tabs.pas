unit UnMoedas_Tabs;

{ Colocar no MyListas:

Uses UnMoedas_Tabs;


//
function TMyListas.CriaListaTabelas:
      Moedas_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnMoedas_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Moedas_Tabs: TUnMoedas_Tabs;

implementation

function TUnMoedas_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    //Linguas.AdTbLst(Lista, False, Lowercase('Cambios'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('CambioCot'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('CambioMda'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('CambioUsu'), '');
    //
    //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnMoedas_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('CambioMda') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|CodUsu|Sigla|Simbolo|Cod_BC|Nome');
    //FListaSQL.Add('0|     0|     ""|   ""');
    FListaSQL.Add('1|     1|     "R$" |"BRL"  |   790| "Real Brasil"');
    FListaSQL.Add('2|     2|     "US$"|"USD"  |   220| "D�lar dos EUA"');
    FListaSQL.Add('3|     3|     "�"  |"EUR"  |   978| "Euro"');
    FListaSQL.Add('4|     4|     "IDX"|"IDX"  |     0| "Meu Indexador"');
  end else
end;

function TUnMoedas_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
}
end;


function TUnMoedas_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  {
  if Uppercase(TabelaBase) = Uppercase('Cambios') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Data';
    FLIndices.Add(FRIndices);
    //
  end else
  }
  if Uppercase(Tabela) = Uppercase('CambioCot') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'DataC';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('CambioMda') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodUsu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE2';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Cod_BC';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('CambioUsu') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnMoedas_Tabs.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TUnMoedas_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    {
    end else
    if Uppercase(Tabela) = Uppercase('Cambios') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dolar';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Euro';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Indexador';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    }
    if Uppercase(Tabela) = Uppercase('CambioCot') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CambioMda') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Simbolo';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cod_BC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CambioUsu') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnMoedas_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'CambioMdaAtz';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnMoedas_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  //
  // FIN-MOEDA-001 :: Cadastro de Moedas
  New(FRJanelas);
  FRJanelas.ID        := 'FIN-MOEDA-001';
  FRJanelas.Nome      := 'FmCambioMda';
  FRJanelas.Descricao := 'Cadastro de moedas';
  FLJanelas.Add(FRJanelas);
  //
  // FIN-MOEDA-002 :: Cota��o de Moedas
  New(FRJanelas);
  FRJanelas.ID        := 'FIN-MOEDA-002';
  FRJanelas.Nome      := 'FmCambioCot';
  FRJanelas.Descricao := 'Cota��o de moedas';
  FLJanelas.Add(FRJanelas);
  //
  // FIN-MOEDA-003 :: Login Cotador de Moeda
  New(FRJanelas);
  FRJanelas.ID        := 'FIN-MOEDA-003';
  FRJanelas.Nome      := 'FmCambioUsu';
  FRJanelas.Descricao := 'Logins respons�veis por informar a cota��o di�ria de moedas';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
