unit UnMoedas;

interface

uses System.Math, Forms, Controls, Windows, SysUtils, ComCtrls, Classes,
  ExtCtrls, mySQLDbTables, Dialogs, UnDmkEnums, Variants, DB, DmkDAC_PF,
  UnDmkProcFunc;

type
  TUnMoedas = class(TObject)
  private

  public
    function  ObtemListaMoedas(): TStringList;
    function  ObtemNomeMoeda(Moeda: Integer): String;
  end;

var
  UMoedas: TUnMoedas;
const
  MaxMoedas = 3;
  sListaMoedas: array[0..MaxMoedas] of String = ('R$', 'U$', '�', '??');

implementation

uses dmkGeral;

{ TUnMoedas }

function TUnMoedas.ObtemListaMoedas: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(sListaMoedas) - 1 do
      Lista.Add(sListaMoedas[I]);
  finally
    Result := Lista;
  end;
end;

function TUnMoedas.ObtemNomeMoeda(Moeda: Integer): String;
begin
  Result := sListaMoedas[Moeda];
end;

end.
