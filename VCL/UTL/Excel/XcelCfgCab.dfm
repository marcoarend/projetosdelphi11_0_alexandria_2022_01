object FmXcelCfgCab: TFmXcelCfgCab
  Left = 368
  Top = 194
  Caption = 'XLS-CONFG-001 :: Configura'#231#227'o de Importa'#231#227'o de Arquivo Excel'#169
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 644
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 595
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 56
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        Enabled = False
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label6: TLabel
        Left = 900
        Top = 4
        Width = 32
        Height = 13
        Caption = 'Lin.Tit.'
      end
      object Label10: TLabel
        Left = 948
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Lin.Ini.'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 749
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdLinTit: TdmkEdit
        Left = 900
        Top = 20
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinTit'
        UpdCampo = 'LinTit'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinIni: TdmkEdit
        Left = 948
        Top = 20
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'LinIni'
        UpdCampo = 'LinIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 644
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      ParentBackground = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1006
        Height = 48
        Align = alClient
        TabOrder = 0
        object Label2: TLabel
          Left = 148
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 64
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label4: TLabel
          Left = 900
          Top = 4
          Width = 32
          Height = 13
          Caption = 'Lin.Tit.'
          FocusControl = DBEdit2
        end
        object Label5: TLabel
          Left = 948
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Lin.Ini.'
          FocusControl = DBEdit3
        end
        object DBEdNome: TdmkDBEdit
          Left = 148
          Top = 20
          Width = 749
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsXcelCfgCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 64
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsXcelCfgCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsXcelCfgCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit2: TDBEdit
          Left = 900
          Top = 20
          Width = 44
          Height = 21
          DataField = 'LinTit'
          DataSource = DsXcelCfgCab
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 948
          Top = 20
          Width = 44
          Height = 21
          DataField = 'LinIni'
          DataSource = DsXcelCfgCab
          TabOrder = 4
        end
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 595
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TStaticText
        Left = 173
        Top = 1
        Width = 30
        Height = 17
        Align = alClient
        BevelKind = bkTile
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 436
        Top = 1
        Width = 569
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 460
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 1006
      Height = 484
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 2
      object TabSheet2: TTabSheet
        Caption = 'Colunas do Arquivo '
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 456
          Align = alClient
          Caption = 'Panel7'
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 1
            Top = 1
            Width = 996
            Height = 454
            Align = alClient
            DataSource = DsXcelCfgIts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ID_TabFld'
                Title.Caption = 'ID'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TabFld'
                Title.Caption = 'Relacionamento (XLS > Blue Derm)'
                Width = 269
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Coluna'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'T'#237'tulo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_DataType'
                Title.Caption = 'Tipo de dados'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = 
      '                              Configura'#231#227'o de Importa'#231#227'o de Arqu' +
      'ivo Excel'#169
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsXcelCfgCab: TDataSource
    DataSet = QrXcelCfgCab
    Left = 40
    Top = 12
  end
  object QrXcelCfgCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrXcelCfgCabBeforeOpen
    AfterOpen = QrXcelCfgCabAfterOpen
    BeforeClose = QrXcelCfgCabBeforeClose
    AfterScroll = QrXcelCfgCabAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM xcelcfgcab')
    Left = 12
    Top = 12
    object QrXcelCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrXcelCfgCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrXcelCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrXcelCfgCabLinTit: TIntegerField
      FieldName = 'LinTit'
    end
    object QrXcelCfgCabLinIni: TIntegerField
      FieldName = 'LinIni'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 460
    Top = 612
    object NovaConfigurao1: TMenuItem
      Caption = 'Nova &Configura'#231#227'o'
      OnClick = NovaConfigurao1Click
    end
    object Colunaconfiguraoatual1: TMenuItem
      Caption = '&Coluna '#224' configura'#231#227'o atual'
      Enabled = False
      OnClick = Colunaconfiguraoatual1Click
    end
  end
  object QrXcelCfgIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrXcelCfgItsCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM XcelCfgIts'
      'WHERE Codigo=:P0'
      'ORDER BY ID_TabFld, Coluna, Controle')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrXcelCfgItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'xcelcfgits.Codigo'
    end
    object QrXcelCfgItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'xcelcfgits.Controle'
    end
    object QrXcelCfgItsColuna: TWideStringField
      FieldName = 'Coluna'
      Origin = 'xcelcfgits.Coluna'
      Size = 3
    end
    object QrXcelCfgItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'xcelcfgits.Nome'
      Size = 50
    end
    object QrXcelCfgItsDataType: TSmallintField
      FieldName = 'DataType'
      Origin = 'xcelcfgits.DataType'
    end
    object QrXcelCfgItsCasas: TSmallintField
      FieldName = 'Casas'
      Origin = 'xcelcfgits.Casas'
    end
    object QrXcelCfgItsTabSource: TWideStringField
      FieldName = 'TabSource'
      Origin = 'xcelcfgits.TabSource'
    end
    object QrXcelCfgItsFldSource: TWideStringField
      FieldName = 'FldSource'
      Origin = 'xcelcfgits.FldSource'
      Size = 30
    end
    object QrXcelCfgItsTabDest: TWideStringField
      FieldName = 'TabDest'
      Origin = 'xcelcfgits.TabDest'
    end
    object QrXcelCfgItsFldDest: TWideStringField
      FieldName = 'FldDest'
      Origin = 'xcelcfgits.FldDest'
      Size = 30
    end
    object QrXcelCfgItsID_TabFld: TIntegerField
      FieldName = 'ID_TabFld'
      Origin = 'xcelcfgits.ID_TabFld'
    end
    object QrXcelCfgItsNO_TabFld: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TabFld'
      Size = 255
      Calculated = True
    end
    object QrXcelCfgItsNO_DataType: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DataType'
      Size = 50
      Calculated = True
    end
  end
  object DsXcelCfgIts: TDataSource
    DataSet = QrXcelCfgIts
    Left = 124
    Top = 12
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 556
    Top = 608
    object ConfiguraoAtual1: TMenuItem
      Caption = 'Configura'#231#227'o &Atual'
      Enabled = False
      OnClick = ConfiguraoAtual1Click
    end
    object Colunaselecionada1: TMenuItem
      Caption = 'Coluna &Selecionada'
      Enabled = False
      OnClick = Colunaselecionada1Click
    end
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 652
    Top = 608
    object ConfiguraoAtual2: TMenuItem
      Caption = 'Configura'#231#227'o &Atual'
      Enabled = False
      OnClick = ConfiguraoAtual2Click
    end
    object ColunaSelecionada2: TMenuItem
      Caption = 'Coluna &Selecionada'
      Enabled = False
      OnClick = ColunaSelecionada2Click
    end
  end
end
