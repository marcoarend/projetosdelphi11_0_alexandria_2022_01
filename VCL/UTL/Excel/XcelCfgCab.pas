unit XcelCfgCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkRadioGroup, UnDmkProcFunc, UnDmkEnums;

type
  TFmXcelCfgCab = class(TForm)
    PainelDados: TPanel;
    DsXcelCfgCab: TDataSource;
    QrXcelCfgCab: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMInclui: TPopupMenu;
    QrXcelCfgIts: TmySQLQuery;
    DsXcelCfgIts: TDataSource;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    PMAltera: TPopupMenu;
    Panel6: TPanel;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    PMExclui: TPopupMenu;
    QrXcelCfgItsCodigo: TIntegerField;
    QrXcelCfgItsControle: TIntegerField;
    QrXcelCfgItsColuna: TWideStringField;
    QrXcelCfgItsNome: TWideStringField;
    QrXcelCfgItsDataType: TSmallintField;
    QrXcelCfgItsCasas: TSmallintField;
    QrXcelCfgItsTabSource: TWideStringField;
    QrXcelCfgItsFldSource: TWideStringField;
    QrXcelCfgItsTabDest: TWideStringField;
    QrXcelCfgItsFldDest: TWideStringField;
    QrXcelCfgCabCodigo: TIntegerField;
    QrXcelCfgCabCodUsu: TIntegerField;
    QrXcelCfgCabNome: TWideStringField;
    QrXcelCfgCabLinTit: TIntegerField;
    QrXcelCfgCabLinIni: TIntegerField;
    NovaConfigurao1: TMenuItem;
    Colunaconfiguraoatual1: TMenuItem;
    ConfiguraoAtual1: TMenuItem;
    Colunaselecionada1: TMenuItem;
    ConfiguraoAtual2: TMenuItem;
    ColunaSelecionada2: TMenuItem;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    Label10: TLabel;
    EdLinTit: TdmkEdit;
    EdLinIni: TdmkEdit;
    QrXcelCfgItsID_TabFld: TIntegerField;
    QrXcelCfgItsNO_TabFld: TWideStringField;
    QrXcelCfgItsNO_DataType: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrXcelCfgCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrXcelCfgCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrXcelCfgCabBeforeClose(DataSet: TDataSet);
    procedure PMAlteraPopup(Sender: TObject);
    procedure QrXcelCfgCabAfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure NovaConfigurao1Click(Sender: TObject);
    procedure Colunaconfiguraoatual1Click(Sender: TObject);
    procedure ConfiguraoAtual1Click(Sender: TObject);
    procedure Colunaselecionada1Click(Sender: TObject);
    procedure ConfiguraoAtual2Click(Sender: TObject);
    procedure ColunaSelecionada2Click(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure QrXcelCfgItsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure MostraEdicaoIts(SQLType: TSQLType);
  public
    { Public declarations }
    FCodigos: array of Integer;
    FNomes: array of String;
    //
    FListaTabFld: String;
    //
    procedure ReopenXcelCfgIts(Controle: Integer);
    //
  end;

var
  FmXcelCfgCab: TFmXcelCfgCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, XcelCfgIts, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmXcelCfgCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmXcelCfgCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrXcelCfgCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmXcelCfgCab.DefParams;
begin
  VAR_GOTOTABELA := 'xcelcfgcab';
  VAR_GOTOMYSQLTABLE := QrXcelCfgCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM xcelcfgcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmXcelCfgCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'xcelcfgcab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmXcelCfgCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmXcelCfgCab.MostraEdicaoIts(SQLType: TSQLType);
begin
  PageControl1.ActivePageIndex := 0;
  if UmyMod.FormInsUpd_Cria(TFmXcelCfgIts, FmXcelCfgIts, afmoNegarComAviso,
  QrXcelCfgIts, SQLType) then
  begin
    if SQLType = stIns then
      FmXcelCfgIts.RGDataType.ItemIndex := 0;
    FmXcelCfgIts.ShowModal;
    FmXcelCfgIts.Destroy;
  end;
end;

procedure TFmXcelCfgCab.NovaConfigurao1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrXcelCfgCab, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'xcelcfgcab');
end;

procedure TFmXcelCfgCab.PMAlteraPopup(Sender: TObject);
begin
  ConfiguraoAtual1.Enabled :=
    (QrXcelCfgCab.State <> dsInactive) and
    (QrXcelCfgCab.RecordCount > 0);
 Colunaselecionada1.Enabled :=
   ConfiguraoAtual1.Enabled and
    (QrXcelCfgIts.State <> dsInactive) and
    (QrXcelCfgIts.RecordCount > 0);
end;

procedure TFmXcelCfgCab.PMExcluiPopup(Sender: TObject);
begin
  ConfiguraoAtual2.Enabled :=
    (QrXcelCfgCab.State <> dsInactive) and
    (QrXcelCfgCab.RecordCount > 0) and
    (QrXcelCfgIts.State <> dsInactive) and
    (QrXcelCfgIts.RecordCount = 0);
 Colunaselecionada2.Enabled :=
    (ConfiguraoAtual2.Enabled = False) and
    (QrXcelCfgIts.State <> dsInactive) and
    (QrXcelCfgIts.RecordCount > 0);
end;

procedure TFmXcelCfgCab.PMIncluiPopup(Sender: TObject);
begin
  Colunaconfiguraoatual1.Enabled :=
    (QrXcelCfgCab.State <> dsInactive) and
    (QrXcelCfgCab.RecordCount > 0);
end;

procedure TFmXcelCfgCab.Colunaconfiguraoatual1Click(Sender: TObject);
begin
  MostraEdicaoIts(stIns);
end;

procedure TFmXcelCfgCab.Colunaselecionada1Click(Sender: TObject);
begin
  MostraEdicaoIts(stUpd);
end;

procedure TFmXcelCfgCab.ColunaSelecionada2Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da coluna?', 'xcelcfgits',
    'Controle', QrXcelCfgItsControle.Value, Dmod.MyDB);
  ReopenXcelCfgIts(QrXcelCfgItsControle.Value);

end;

procedure TFmXcelCfgCab.ConfiguraoAtual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrXcelCfgCab, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'xcelcfgcab');
end;

procedure TFmXcelCfgCab.ConfiguraoAtual2Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da configura��o?', 'xcelcfgcab',
    'Codigo', QrXcelCfgCabCodigo.Value, Dmod.MyDB);
  LocCod(0,0);
end;

procedure TFmXcelCfgCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
{ Problemas na defini�ao do NO_TabFld
  Va(vpLast);
}
end;

procedure TFmXcelCfgCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmXcelCfgCab.ReopenXcelCfgIts(Controle: Integer);
begin
  QrXcelCfgIts.Close;
  QrXcelCfgIts.Params[0].AsInteger := QrXcelCfgCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrXcelCfgIts, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrXcelCfgIts.Locate('Controle', Controle, []);
end;

procedure TFmXcelCfgCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmXcelCfgCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmXcelCfgCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmXcelCfgCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmXcelCfgCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmXcelCfgCab.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmXcelCfgCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrXcelCfgCabCodigo.Value;
  Close;
end;

procedure TFmXcelCfgCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('xcelcfgcab', 'Codigo', LaTipo.SQLType,
    QrXcelCfgCabCodigo.Value);
  EdCodUsu.ValueVariant := Codigo;  
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmXcelCfgCab, PainelEdit,
    'xcelcfgcab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmXcelCfgCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'xcelcfgcab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'xcelcfgcab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'xcelcfgcab', 'Codigo');
end;

procedure TFmXcelCfgCab.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmXcelCfgCab.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmXcelCfgCab.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  PainelEdit.Align   := alClient;
  CriaOForm;
end;

procedure TFmXcelCfgCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrXcelCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmXcelCfgCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmXcelCfgCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrXcelCfgCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmXcelCfgCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmXcelCfgCab.QrXcelCfgCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmXcelCfgCab.QrXcelCfgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenXcelCfgIts(0);
end;

procedure TFmXcelCfgCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmXcelCfgCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrXcelCfgCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'xcelcfgcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmXcelCfgCab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmXcelCfgCab.QrXcelCfgCabBeforeClose(DataSet: TDataSet);
begin
  QrXcelCfgIts.Close;
end;

procedure TFmXcelCfgCab.QrXcelCfgCabBeforeOpen(DataSet: TDataSet);
begin
  QrXcelCfgCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmXcelCfgCab.QrXcelCfgItsCalcFields(DataSet: TDataSet);
begin
  if High(FNomes) >= QrXcelCfgItsID_TabFld.Value then
    QrXcelCfgItsNO_TabFld.Value := FNomes[QrXcelCfgItsID_TabFld.Value]
  else
    QrXcelCfgItsNO_TabFld.Value := '? ? ?';
  QrXcelCfgItsNO_DataType.Value :=
    Geral.ObtemTextoDeAllFormat(QrXcelCfgItsDataType.Value);
end;

end.

