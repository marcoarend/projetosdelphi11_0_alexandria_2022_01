object FmXcelCfgIts: TFmXcelCfgIts
  Left = 339
  Top = 185
  Caption = 'XLS-CONFG-002 :: Coluna Excel'#169' em Importa'#231#227'o de Arquivo'
  ClientHeight = 369
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 321
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 3
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Coluna Excel'#169' em Importa'#231#227'o de Arquivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 4
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 297
    Width = 784
    Height = 24
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 64
    Align = alTop
    Caption = ' Dados da configura'#231#227'o: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label4: TLabel
      Left = 72
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Left = 156
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmXcelCfgCab.DsXcelCfgCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdit1: TDBEdit
      Left = 72
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'CodUsu'
      DataSource = FmXcelCfgCab.DsXcelCfgCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 156
      Top = 36
      Width = 617
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      DataSource = FmXcelCfgCab.DsXcelCfgCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 112
    Width = 784
    Height = 185
    Align = alTop
    Caption = ' Dados da coluna:'
    TabOrder = 1
    object Label2: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
      Enabled = False
    end
    object Label8: TLabel
      Left = 96
      Top = 16
      Width = 36
      Height = 13
      Caption = 'Coluna:'
    end
    object Label9: TLabel
      Left = 12
      Top = 124
      Width = 244
      Height = 13
      Caption = 'Tipo de relacionamento da coluna com o aplicativo:'
    end
    object Label1: TLabel
      Left = 140
      Top = 16
      Width = 31
      Height = 13
      Caption = 'T'#237'tulo:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdColuna: TdmkEdit
      Left = 96
      Top = 32
      Width = 41
      Height = 21
      Alignment = taCenter
      CharCase = ecUpperCase
      MaxLength = 3
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Coluna'
      UpdCampo = 'Coluna'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdColunaChange
    end
    object CBID_TabFld: TdmkDBLookupComboBox
      Left = 68
      Top = 140
      Width = 705
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTabFld
      TabOrder = 5
      dmkEditCB = EdID_TabFld
      QryCampo = 'ID_TabFld'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdID_TabFld: TdmkEditCB
      Left = 12
      Top = 140
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ID_TabFld'
      UpdCampo = 'ID_TabFld'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdColunaChange
      DBLookupComboBox = CBID_TabFld
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdNome: TdmkEdit
      Left = 140
      Top = 32
      Width = 633
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdColunaChange
    end
    object RGDataType: TdmkRadioGroup
      Left = 12
      Top = 56
      Width = 761
      Height = 65
      Caption = ' Tipo de dados na coluna: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'MyObjects.ConfiguraAllFmtStr(RGDataType, 4, 0);')
      TabOrder = 3
      OnClick = EdColunaChange
      QryCampo = 'DataType'
      UpdCampo = 'DataType'
      UpdType = utYes
      OldValor = 0
    end
  end
  object QrTabFld: TMySQLQuery
    SQL.Strings = (
      'SELECT * FROM _tab_fld_'
      'ORDER BY Nome')
    Left = 4
    Top = 12
    object QrTabFldCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTabFldNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsTabFld: TDataSource
    DataSet = QrTabFld
    Left = 32
    Top = 12
  end
end
