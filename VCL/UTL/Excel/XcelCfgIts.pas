unit XcelCfgIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, UnDmkEnums;

type
  TFmXcelCfgIts = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdControle: TdmkEdit;
    EdColuna: TdmkEdit;
    CBID_TabFld: TdmkDBLookupComboBox;
    EdID_TabFld: TdmkEditCB;
    EdNome: TdmkEdit;
    Label1: TLabel;
    RGDataType: TdmkRadioGroup;
    QrTabFld: TmySQLQuery;
    DsTabFld: TDataSource;
    QrTabFldCodigo: TIntegerField;
    QrTabFldNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdColunaChange(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaOk();
  public
    { Public declarations }
  end;

  var
  FmXcelCfgIts: TFmXcelCfgIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, XcelCfgCab,
  ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmXcelCfgIts.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('xcelcfgits', 'Controle', LaTipo.SQLType,
  EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, LaTipo.SQLType, 'xcelcfgits',
  Controle, Dmod.QrUpd) then
  begin
    FmXcelCfgCab.ReopenXcelCfgIts(Controle);
    if CkContinuar.Checked then
    begin
      Latipo.SQLType           := stIns;
      EdControle.ValueVariant  := 0;
      EdColuna.ValueVariant    := '';
      EdNome.ValueVariant      := '';
      EdID_TabFld.ValueVariant := 0;
      CBID_TabFld.KeyValue     := Null;
      RGDataType.ItemIndex     := 0;
      //
      EdColuna.SetFocus;
    end else Close;
  end;
end;

procedure TFmXcelCfgIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmXcelCfgIts.EdColunaChange(Sender: TObject);
begin
  HabilitaOk();
end;

procedure TFmXcelCfgIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmXcelCfgIts.FormCreate(Sender: TObject);
begin
  MyObjects.ConfiguraAllFmtStr(RGDataType, 4, 0);
  QrTabFld.Close;
  QrTabFld.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrTabFld, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmXcelCfgIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmXcelCfgIts.HabilitaOk();
begin
  BtOk.Enabled :=
    (Trim(EdColuna.Text) <> '') and
    (Trim(EdNome.Text) <> '') and
    (RGDataType.ItemIndex > 0) and
    (EdID_TabFld.ValueVariant <> 0);
end;

end.
