unit UnMyJSON;

interface

uses SysUtils, Types, Classes, IOUtils, uLkJSON, TypInfo, Variants,
  vcl.StdCtrls, System.JSON;

type
  rInverter = record
    InverterNo: String;
    DeviceType: String;
    SerialNo: String;
  end;
  TMyJsonBasesArr = array of TlkJSONbase;
  TMyJsonValores = array of Variant;
  TrInverters = array of rInverter;
{
  TDmkJsonFormats = (mjfUnknown=0, mjfDouble=1, mjfInteger=2, mjfString=3,
                    mjfWideString=4, mjfBoolean=5);
  TMyJsonFormats = array of TDmkJsonFormats;
}
  //
  TUnMyJSON = class(TObject)
  private
    { Private declarations }
    procedure Avisa(Avisa: Boolean; Msg: String; LBAvisos: TListBox; OnlyLB:
              Boolean = False);
    function  ConcatCampos(const Lista: array of String): String;
    function  Posicao(const Lista: array of String; const Texto: String): Integer;

  public
    { Public declarations }
{$IfDef UsaWSuport}
    function  GetJsonObjectFromURL(const Host, Request: String; var JSON:
              TlkJSONobject): Boolean;
    function  GetJsonTextFromURL(const Host, Request: String; var JSON:
              String; const AvisaErro: Boolean = True; const LBAvisos:
              TListBox = nil): Boolean;
{$EndIf}
    function  GetJsonObjectFromJsonText(const Texto: String; var JSON:
              TlkJSONobject): Boolean;
    //function  IsolateJSONObjectDeepLevel Fazer!!!
    //
    function  JObjValFlu(jsonObj: TJSONObject; Campo: String): Double;
    function  JObjValInt(jsonObj: TJSONObject; Campo: String): Integer;
    function  JObjValStr(jsonObj: TJSONObject; Campo: String): String;

    function  SliceExpectedBaseItems(const Base: TlkJSONbase; const WarnErr:
              Boolean; const Names: array of String; var Itens:
              TMyJsonBasesArr; const LBAvisos: TListBox = nil): Boolean;
{
    function  SliceExpectedBaseValues(const Base: TlkJSONbase; const WarnErr:
              Boolean; const Names: array of String; var Valores:
              TMyJsonValores): Boolean;
}
{
    function  SliceExpectedObjectValues(const Objeto: TlkJSONObject; const
              WarnErr: Boolean; const Names: array of String; const Formats:
              TMyJsonFormats; var Valores: TMyJsonValores): Boolean;
}
  end;
var
  MyJSON: TUnMyJSON;

implementation

uses
{$IfDef UsaWSuport} UnDmkWeb, {$EndIf}
//UnProjGroup_Vars
dmkGeral;

{ TUnMyJSON }

procedure TUnMyJSON.Avisa(Avisa: Boolean; Msg: String; LBAvisos: TListBox;
  OnlyLB: Boolean = False);
begin
  if Avisa then
  begin
    if LBAvisos <> nil then
      LBAvisos.Items.Add(Msg)
    else if not OnlyLB then
      Geral.MB_Erro(Msg);
  end;
end;

function TUnMyJSON.ConcatCampos(const Lista: array of String): String;
var
  cci: Integer;
begin
  Result := '';
  for cci := 0 to Length(Lista)-1 do
  begin
    Result := Result + '"' + Lista[cci] + '" ';
  end;
end;

function TUnMyJSON.GetJsonObjectFromJsonText(const Texto: String;
  var JSON: TlkJSONobject): Boolean;
begin
  Result := False;
  try
    if JSON = nil then
      JSON := TlkJSONobject.Create;
    JSON := TlkJSON.ParseText(Texto) as TlkJSONobject;
    Result := True;
  except
    Geral.MB_Erro('N�o foi poss�vel obter o objeto JSON');
  end;
end;

function TUnMyJSON.JObjValFlu(jsonObj: TJSONObject; Campo: String): Double;
const
  sProcNome = 'TUnMyJSON.JObjValFlu()';
var
  jv: TJSONValue;
begin
  try
    jv := jsonObj.GetValue(Campo);
    try
      if jv <> nil then
        Result := Geral.DMV_Dot(jv.Value)
      else
        Result := 0;
    except
      Geral.MB_Erro('N�o foi poss�vel obter o valor do Campo JSON : "' +
      Campo + '"' + sLineBreak + sProcNome);
    end;
  except
    Geral.MB_Erro('Campo JSON inexistente: "' + Campo + '"' + sLineBreak +
      sProcNome);
  end;
end;

function TUnMyJSON.JObjValInt(jsonObj: TJSONObject; Campo: String): Integer;
const
  sProcNome = 'TUnMyJSON.JObjValInt()';
var
  jv: TJSONValue;
begin
  try
    jv := jsonObj.GetValue(Campo);
    try
      if jv <> nil then
        Result := StrToInt(jv.Value)
      else
        Result := 0;
    except
      Geral.MB_Erro('N�o foi poss�vel obter o valor do Campo JSON : "' +
      Campo + '"' + sLineBreak + sProcNome);
    end;
  except
    Geral.MB_Erro('Campo JSON inexistente: "' + Campo + '"' + sLineBreak +
      sProcNome);
  end;
end;

function TUnMyJSON.JObjValStr(jsonObj: TJSONObject; Campo: String): String;
const
  sProcNome = 'TUnMyJSON.JObjValStr()';
var
  jv: TJSONValue;
begin
  try
    jv := jsonObj.GetValue(Campo);
    try
      if jv <> nil then
        Result := jv.Value
      else
        Result := '';
    except
      Geral.MB_Erro('N�o foi poss�vel obter o valor do Campo JSON : "' +
      Campo + '"' + sLineBreak + sProcNome);
    end;
  except
    Geral.MB_Erro('Campo JSON inexistente: "' + Campo + '"' + sLineBreak +
      sProcNome);
  end;
end;

{$IfDef UsaWSuport}

function TUnMyJSON.GetJsonObjectFromURL(const Host, Request: String;
  var JSON: TlkJSONobject): Boolean;
var
  Texto: String;
begin
  Result := False;
  if GetJsonTextFromURL(Host, Request, Texto) then
    Result := GetJsonObjectFromJsonText(Texto, JSON);
end;

function TUnMyJSON.GetJsonTextFromURL(const Host, Request: String; var JSON:
  String; const AvisaErro: Boolean = True; const LBAvisos: TListBox = nil):
  Boolean;
var
  Texto: String;
begin
  //if MyObjects.Ping(Host, CO_HostPingTimeOutNormal) then
  begin
    Result := DmkWeb.URLGetMem(Request, JSON, AvisaErro);
    if not Result then
      Avisa(AvisaErro, 'N�o foi poss�vel obter o texto JSON de ' + Request, LBAvisos, True);
  end
  //else
    //Avisa(AvisaErro, 'N�o foi poss�vel conectar no Host ' + Host, LBAvisos, True);
end;
{$EndIf}

function TUnMyJSON.Posicao(const Lista: array of String; const Texto: String): Integer;
var
  pii: Integer;
begin
  Result := -1;
  for pii := 0 to Length(Lista)-1 do
  begin
    if Lista[pii] = Texto then
    begin
      Result := pii;
      Exit;
    end;
  end;
end;

function TUnMyJSON.SliceExpectedBaseItems(const Base: TlkJSONbase; const WarnErr:
Boolean; const Names: array of String; var Itens: TMyJsonBasesArr; const LBAvisos:
TListBox): Boolean;
  //
const
  sProcName = 'TUnMyJSON.SliceExpectedBaseItems()';
var
  qtdI, I, N, M, Erros: Integer;
  Item: TlkJSONbase;
  Nome, Encontrados: String;
  Objeto: TlkJSONobject;
  Texto: TlkJSONstring;
  Numero: TlkJSONnumber;
  //Lista: TlkJSONobject;
  Lista: TlkJSONbase;
  Msg: String;
  Nulo: TlkJSONnull;
begin
  Erros  := 0;
  Result := False;
  //
  qtdI := Base.Count;
  if qtdI <> Length(Names) then
  begin
    //Erros := Erros + 1;
    if WarnErr then
    begin
      Encontrados := '';
      for I := 0 to qtdI -1 do
      begin
        Item := TlkJSONObject(Base).child[I];
        if Item <> nil then
          Encontrados := Encontrados + '"' + TlkJSONobjectmethod(Item).Name + '" ';
      end;
      //
      Avisa(WarnErr, 'A quantidade de itens encontrados => ' + Geral.FF0(qtdI) +
      ' difere do esperado => ' + Geral.FF0(Length(Names)) + '.' + sLineBreak +
      'Campos esperados: ' + ConcatCampos(Names) + sLineBreak +
      'Campos encontrados: ' + ConcatCampos(Encontrados) + sLineBreak + sProcName +
      sLineBreak + TlkJSON.GenerateText(Base), LBAvisos);
    end;
  end;
  SetLength(Itens, qtdI);
  // Limpar mem�ria
  for I := 0 to qtdI -1 do
    Itens[I] := nil;
  // Definir itens v�lidos
  for I := 0 to qtdI -1 do
  begin
    Item := TlkJSONObject(Base).child[I];
    if Item <> nil then
    begin
      Nome := TlkJSONobjectmethod(Item).Name;
      //Geral.MB_Info(Objeto.Field[Nome].ClassName);
      N := Posicao(Names, Nome);
      if N > -1 then
      begin
        Itens[N] := Item;
        //
        if Base.Field[Nome] is TlkJSONobject then
        begin
          Objeto := Base.Field[Nome] as TlkJSONobject;
          Itens[N] := Objeto;
        end else
        if Base.Field[Nome] is TlkJSONstring then
        begin
          Texto := Base.Field[Nome] as TlkJSONstring;
          Itens[N] := Texto;
        end else
        if Base.Field[Nome] is TlkJSONnumber then
        begin
          Numero := Base.Field[Nome] as TlkJSONnumber;
          Itens[N] := Numero;
        end else
        if Base.Field[Nome] is TlkJSONlist then
        begin
          Lista := Base.Field[Nome] as TlkJSONbase;
          //js := TlkJSONlist.Create;
          ///
          //Lista := TlkJSONobject(TlkJSONobjectmethod(Base).Objvalue);
          //Lista := Base.Field[Nome] as TlkJSONobject;
          //Geral.MB_Info(TlkJSON.GenerateText(Lista));
          Itens[N] := Lista;
        end else
        if Base.Field[Nome] is TlkJSONnull then
        begin
          Nulo := Base.Field[Nome] as TlkJSONnull;
          Itens[N] := Nulo;
        end else
        begin
          Erros  := Erros + 1;
          Avisa(WarnErr, 'Tipo de TlkJSON n�o implementado: => ' + Nome + '.' +
            sLineBreak + 'Tipo: ' + Item.ClassName + sLineBreak +
            ConcatCampos(Names) + sLineBreak + sProcName + sLineBreak +
            TlkJSON.GenerateText(Base), LBAvisos);
        end;
      end
      else
      begin
        Erros  := Erros + 1;
        Avisa(WarnErr, 'Item n�o esperado: => ' + Nome + '.' + sLineBreak +
          'Campos esperados: ' + ConcatCampos(Names) + sLineBreak + sProcName +
          sLineBreak + TlkJSON.GenerateText(Base), LBAvisos);
      end;
    end;
  end;
  Result := Erros = 0;
end;

{
function TUnMyJSON.SliceExpectedBaseValues(const Base: TlkJSONbase;
  const WarnErr: Boolean; const Names: array of String;
  var Valores: TMyJsonValores): Boolean;
var
  j: Integer;
  Subitem: TlkJSONbase;
  SubObjeto: TlkJSONobject;
  Nome: String;
begin
    for j := 0 to Base.Count - 1 do
    begin
      SubItem := TlkJSONObject(Base).child[j];
      if SubItem <> nil then
      begin
        Nome := TlkJSONobjectmethod(SubItem).Name;
        //Geral.MB_Info(Objeto.Field[Nome].ClassName);
        if Base.Field[Nome] is TlkJSONobject then
        begin
          SubObjeto := Base.Field[Nome] as TlkJSONobject;

          Geral.MB_Info('  ' + Nome + ' [' + Geral.FF0(SubObjeto.Count) + ' iten(s)]');
        end else
        begin
          Geral.MB_Info('  ' + Nome + ' = ' + Subitem.Value);
        end;
      end;
    end;
end;
}

{
function TUnMyJSON.SliceExpectedObjectValues(const Objeto: TlkJSONObject; const
  WarnErr: Boolean; const Names: array of String; const Formats: TMyJsonFormats;
  var Valores: TMyJsonValores): Boolean;
const
  sProcName = 'TUnMyJSON.SliceExpectedObjectValues()';
var
  qtdI, I, N, Erros: Integer;
  Item: TlkJSONbase;
  Nome: String;
  //Objeto: TlkJSONobject;
  Texto: TlkJSONstring;
  Numero: TlkJSONnumber;
begin
  Erros  := 0;
  Result := False;
  //
  qtdI := Objeto.Count;
  if qtdI <> Length(Names) then
  begin
    Erros := Erros + 1;
    if WarnErr then
      Geral.MB_Erro('A quantidade de itens encontrados => ' + Geral.FF0(qtdI) +
      ' difere do esperado => ' + Geral.FF0(Length(Names)) + '.' + sLineBreak +
      'Campos esperados: ' + ConcatCampos(Names) + sLineBreak + sProcName +
      sLineBreak + TlkJSON.GenerateText(Objeto));
    //
  end;
  if Length(Formats) <> Length(Names) then
  begin
    Erros := Erros + 1;
    if WarnErr then
      Geral.MB_Erro('A quantidade de formatos => ' + Geral.FF0(Length(Formats)) +
      ' difere da quantidade de campos esperados => ' + Geral.FF0(Length(Names))
      + '.' + sLineBreak + 'Campos esperados: ' + ConcatCampos(Names) +
      sLineBreak + sProcName + sLineBreak + TlkJSON.GenerateText(Objeto));
    //
  end;
  SetLength(Valores, qtdI);
  // Limpar mem�ria
  for I := 0 to qtdI -1 do
    Valores[I] := null;
  // Definir Valores v�lidos
  for I := 0 to qtdI -1 do
  begin
    case Formats[I] of
      (*1*)mjfDouble     : Valores[I] := Objeto.getDouble(Names[I]);
      (*2*)mjfInteger    : Valores[I] := Objeto.getInt(Names[I]);
      (*3*)mjfString     : Valores[I] := Objeto.getString(Names[I]);
      (*4*)mjfWideString : Valores[I] := Objeto.getWideString(Names[I]);
      (*5*)mjfBoolean    : Valores[I] := Objeto.getBoolean(Names[I]);
    end;
  end;
  Result := Erros = 0;
end;
}
end.
