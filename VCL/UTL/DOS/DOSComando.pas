unit DOSComando;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DosCommand, ComCtrls, UnDmkProcFunc;

type
  TFmDOSComando = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Button2: TButton;
    Button3: TButton;
    Panel5: TPanel;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit1: TEdit;
    Memo1: TMemo;
    //DosCommand1: TDosCommand;
    Button1: TButton;
    Button4: TButton;
    ProgressBar1: TProgressBar;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DosCommand1NewLine(Sender: TObject; NewLine: String;
      OutputType: TOutputType);
    procedure DosCommand1Terminated(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmDOSComando: TFmDOSComando;

implementation

{$R *.DFM}

uses UnMyObjects, UnMLAGeral;

function IsWinNT: boolean;
var
  osv: OSVERSIONINFO;
begin
  osv.dwOSVersionInfoSize := sizeof(osv);
  GetVersionEx(osv);
  result:= osv.dwPlatformId = VER_PLATFORM_WIN32_NT;
end;

procedure TFmDOSComando.Button2Click(Sender: TObject);
begin
  Edit1.Text := String(dmkPF.NomeLongoParaCurto(AnsiString(Edit3.Text))) + Edit4.Text;
end;

procedure TFmDOSComando.Button3Click(Sender: TObject);
var
  buf: array[0..4095] of byte;
  si: STARTUPINFO;
  sa: SECURITY_ATTRIBUTES;
  sd: SECURITY_DESCRIPTOR;
  pi: PROCESS_INFORMATION;
  newstdin,newstdout,read_stdout,write_stdin: THandle;
  exitcod,bread,avail: Cardinal;
begin
  if IsWinNT  then
    begin
      InitializeSecurityDescriptor(@sd,SECURITY_DESCRIPTOR_REVISION);
      SetSecurityDescriptorDacl(@sd, true, nil, false);
      sa.lpSecurityDescriptor := @sd;
    end else sa.lpSecurityDescriptor := nil;
  sa.nLength := sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle := true;
  // Tuberias
  if CreatePipe(newstdin,write_stdin,@sa,0) then
    begin
      if CreatePipe(read_stdout,newstdout,@sa,0) then
         begin
           GetStartupInfo(si);
           with si do
             begin
               dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
               wShowWindow := SW_HIDE;
               hStdOutput  := newstdout;
               hStdError   := newstdout;
               hStdInput   := newstdin;
             end;
           fillchar(buf,sizeof(buf),0);
           GetEnvironmentVariable('COMSPEC',@buf,sizeof(buf)-1);
           strcat(@buf, PChar(' /c '+Edit1.Text));
           if CreateProcess(nil,@buf,nil,nil,TRUE,CREATE_NEW_CONSOLE,nil,nil,si,pi) then
             begin
               memo1.lines.Clear;
               GetExitCodeProcess(pi.hProcess,exitcod);
               PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
               while (exitcod = STILL_ACTIVE) or (bread > 0) do
                  begin
                    if (bread > 0) then
                      begin
                        fillchar(buf,sizeof(buf),0);
                        if (avail > sizeof(buf)-1) then
                          while (bread >= sizeof(buf)-1)  do
                            begin
                              ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                              memo1.lines.text := memo1.lines.Text + String(StrPas(PAnsiChar(@buf)));
                              fillchar(buf,sizeof(buf),0);
                            end else
                            begin
                              ReadFile(read_stdout,buf,sizeof(buf)-1,bread,nil);
                              memo1.lines.text := memo1.lines.Text + String(StrPas(PAnsiChar(@buf)));
                            end;
                      end;
                    GetExitCodeProcess(pi.hProcess,exitcod);
                    PeekNamedPipe(read_stdout,@buf,sizeof(buf)-1,@bread,@avail,nil);
                  end;
             end;
            CloseHandle(read_stdout);
            CloseHandle(newstdout);
         end;
      CloseHandle(newstdin);
      CloseHandle(write_stdin);
    end;
end;

procedure TFmDOSComando.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDOSComando.Button1Click(Sender: TObject);
begin
  (*DosCommand1.CommandLine := Edit1.Text;
  DosCommand1.Execute;*)
end;

procedure TFmDOSComando.DosCommand1NewLine(Sender: TObject;
  NewLine: String; OutputType: TOutputType);
begin
  Memo1.Lines.Add(NewLine);
end;

procedure TFmDOSComando.DosCommand1Terminated(Sender: TObject);
begin
  Memo1.Lines.Add('Comando finalizado!');
end;

{$WARNINGS OFF}
procedure RunConsoleApp(const CommandLine: String; AStrings: TStrings);
type
  TCharBuffer = array[0..MaxInt - 1] of AnsiChar;
const
  MaxBufSize = 1024;
var
  I: Longword;
  SI: TStartupInfo;
  PI: TProcessInformation;
  SA: PSecurityAttributes;
  SD: PSECURITY_DESCRIPTOR;
  NewStdIn: THandle;
  NewStdOut: THandle;
  ReadStdOut: THandle;
  WriteStdIn: THandle;
  Buffer: ^TCharBuffer;
  BufferSize: Cardinal;
  Last: WideString;
  Str: WideString;
  ExitCode: DWORD; 
  Bread: DWORD; 
  Avail: DWORD; 
begin 
  GetMem(SA, SizeOf(TSecurityAttributes)); 

  case Win32Platform of 
    VER_PLATFORM_WIN32_NT: 
      begin 
        GetMem(SD, SizeOf(SECURITY_DESCRIPTOR)); 
        Win32Check(InitializeSecurityDescriptor(SD, SECURITY_DESCRIPTOR_REVISION)); 
        Win32Check(SetSecurityDescriptorDacl(SD, True, nil, False)); 
        SA.lpSecurityDescriptor := SD; 
      end; {end VER_PLATFORM_WIN32_NT} 
    else 
      SA.lpSecurityDescriptor := nil; 
  end; {end case} 

  SA.nLength := SizeOf(SECURITY_ATTRIBUTES); 
  SA.bInheritHandle := True; 

  Win32Check(CreatePipe(NewStdIn, WriteStdIn, SA, 0)); 

  if not CreatePipe(ReadStdOut, NewStdOut, SA, 0) then 
  begin 
    CloseHandle(NewStdIn); 
    CloseHandle(WriteStdIn); 
    RaiseLastWin32Error; 
  end; {end if} 

  GetStartupInfo(SI); 
  SI.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW; 
  SI.wShowWindow := SW_SHOWNORMAL; 
  SI.hStdOutput := NewStdOut; 
  SI.hStdError := NewStdOut; 
  SI.hStdInput := NewStdIn; 

  if not CreateProcess(nil, PChar(CommandLine), nil, nil, True, 
    CREATE_NEW_CONSOLE, nil, nil, SI, PI) then 
  begin 
    CloseHandle(NewStdIn); 
    CloseHandle(NewStdOut); 
    CloseHandle(ReadStdOut); 
    CloseHandle(WriteStdIn); 
    RaiseLastWin32Error; 
  end; {end if} 

  Last := ''; 
  BufferSize := MaxBufSize; 
  Buffer := AllocMem(BufferSize); 

  try 
    repeat 
      Win32Check(GetExitCodeProcess(PI.hProcess, ExitCode)); 
      PeekNamedPipe(ReadStdOut, Buffer, BufferSize, @Bread, @Avail, nil); 

      if (Bread <> 0) then 
      begin 
        if (BufferSize < Avail) then 
        begin
          BufferSize := Avail; 
          ReallocMem(Buffer, BufferSize); 
        end; {end if} 
        FillChar(Buffer^, BufferSize, #0); 
        ReadFile(ReadStdOut, Buffer^, BufferSize, Bread, nil); 
        Str := Last; 
        I := 0; 

        while (I < Bread) do 
        begin 

          case Buffer^[I] of 
            #0: inc(I); 

            #10: 
              begin 
                inc(I); 
                AStrings.Add(Str); 
                Str := ''; 
              end; {end #10} 

            #13: 
              begin 
                inc(I); 
                if (I < Bread) and (Buffer^[I] = #10) then 
                  inc(I); 
                AStrings.Add(Str); 
                Str := ''; 
              end; {end #13} 

            else 
              begin 
                Str := Str + Buffer^[I]; 
                inc(I); 
              end; {end else} 

          end; {end case} 
        end; {end while} 
        Last := Str; 
      end; {end if} 
      Sleep(1); 
      Application.ProcessMessages; 
    until (ExitCode <> STILL_ACTIVE); 

    if Last <> '' then 
      AStrings.Add(Last); 

  finally 
    FreeMem(Buffer); 
  end; {end try/finally} 

  CloseHandle(PI.hThread); 
  CloseHandle(PI.hProcess); 
  CloseHandle(NewStdIn); 
  CloseHandle(NewStdOut); 
  CloseHandle(ReadStdOut); 
  CloseHandle(WriteStdIn); 

end; {end procedure}
{$WARNINGS ON}

procedure TFmDOSComando.Button4Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Para a��es com muito retorno o processo '+
  'pode ser muito mais demorado que "Executa cmd" o qual n�o possui retorno '+
  'at� que seja finalizado. Deseja continuar assim mesmo?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  RunConsoleApp(Edit1.Text, Memo1.Lines);
end;


end.
