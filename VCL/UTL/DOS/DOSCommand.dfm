object FmDOSCommand: TFmDOSCommand
  Left = 347
  Top = 169
  Width = 800
  Height = 500
  Caption = 'Linhas de Comando em DOS'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 466
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 144
      Align = alTop
      TabOrder = 0
      object Panel4: TPanel
        Left = 1
        Top = 102
        Width = 788
        Height = 41
        Align = alBottom
        TabOrder = 0
        object Button2: TButton
          Left = 244
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Gera cmd'
          TabOrder = 0
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 148
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Executa cmd'
          TabOrder = 1
          OnClick = Button3Click
        end
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 788
        Height = 101
        Align = alClient
        TabOrder = 1
        object Edit3: TEdit
          Left = 3
          Top = 4
          Width = 778
          Height = 21
          TabOrder = 0
          Text = 'C:\Arquivos de programas\MySQL\MySQL Server 4.1\bin\'
        end
        object Edit4: TEdit
          Left = 3
          Top = 28
          Width = 778
          Height = 21
          TabOrder = 1
          Text = 
            'mysqldump -h localhost -u root -p wkljweryhvbirt -B mymoney -r C' +
            ':\Avisis.txt > C:\testeX1.sql'
        end
        object Edit1: TEdit
          Left = 4
          Top = 56
          Width = 777
          Height = 21
          TabOrder = 2
        end
      end
    end
    object Memo1: TMemo
      Left = 1
      Top = 145
      Width = 790
      Height = 320
      Align = alClient
      BorderStyle = bsNone
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -12
      Font.Name = 'Lucida Console'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
end
