unit Email_SSL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit,
WinInet, // para verifica��o de conex�o
IdMessage, IdSMTP, // componemtes envio de email
IdBaseComponent, IdComponent, IdIOHandler, IdIOHandlerSocket,IdSSLOpenSSL, 
  IdIOHandlerStack, IdSSL, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase; // componentes SSL

type
  TFmEmail_SSL = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    EdEMail: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdConta: TdmkEdit;
    Label3: TLabel;
    EdSenha: TdmkEdit;
    Label4: TLabel;
    EdSMTP: TdmkEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    dmkEdit3: TdmkEdit;
    Label6: TLabel;
    dmkEdit4: TdmkEdit;
    Label7: TLabel;
    dmkEdit5: TdmkEdit;
    CheckBox1: TCheckBox;
    Label8: TLabel;
    EdPortaSMTP: TdmkEdit;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdSMTP1: TIdSMTP;
    IdMessage1: TIdMessage;
    IdSMTP2: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL2: TIdSSLIOHandlerSocketOpenSSL;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    //function IsConnected: Boolean;
  public
    { Public declarations }
  end;

  var
  FmEmail_SSL: TFmEmail_SSL;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmEmail_SSL.BtOKClick(Sender: TObject);
begin
{
EnviaMail('email@servidor.com.br', //  Email
          'conta', //Conta sem_arroba_ou_com_arroba_depende_do_servidor
          'senha',// Senha
          'S', // Autentica
          'smtp.gmail.com', // Smtp
          'S', // Auth_SSL
          'Nome para exibi��o', // Nom_exibe
          'Porta', // Porta_smtp - SEMPRE UM VALOR NUMERICO, mas com APOSTROFE '
          'Corpo do email', // Corpo - Texto que vai na mensagem
          'Destinatario(s)', // Destinatario, ex: eu@vc.com;teste@teste.com...
          'Assunto');  // Assunto
}
{
EnviaMail(EdEmail.Text, //  Email
          EdConta.Text, //Conta sem_arroba_ou_com_arroba_depende_do_servidor
          EdSenha.Text,// Senha
          'S', // Autentica
          EdSMTP.Text, // Smtp
          'S', // Auth_SSL
          'Marcelo Winter', // Nom_exibe
          EdPortaSMTP.Text, // Porta_smtp - SEMPRE UM VALOR NUMERICO, mas com APOSTROFE '
          'Corpo do email', // Corpo - Texto que vai na mensagem
          'marco@dermatek.com.br', // Destinatario, ex: eu@vc.com;teste@teste.com...
          'Assunto');  // Assunto
}
  IdSMTP2.Connect;
  IdSMTP2.Send(IdMessage1);
  IdSMTP2.Disconnect;

end;

procedure TFmEmail_SSL.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmail_SSL.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmail_SSL.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

{ Desabilitado porque n�o usa
function TFmEmail_SSL.IsConnected: Boolean;
const
  INTERNET_CONNECTION_MODEM = 1;
  INTERNET_CONNECTION_LAN = 2;
  INTERNET_CONNECTION_PROXY = 4;
  INTERNET_CONNECTION_MODEM_BUSY = 8;
var
  dwConnectionTypes : DWORD;
begin
  dwConnectionTypes := INTERNET_CONNECTION_MODEM + INTERNET_CONNECTION_LAN + INTERNET_CONNECTION_PROXY;
  If InternetGetConnectedState(@dwConnectionTypes,0) then
    Result := True
  else
    Result := False;
end;
}

end.
