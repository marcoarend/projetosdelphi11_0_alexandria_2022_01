object FmEmail_SSL: TFmEmail_SSL
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ??? ????? ???'
  ClientHeight = 494
  ClientWidth = 956
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 956
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitWidth = 784
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 844
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 672
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 956
    Height = 48
    Align = alTop
    Caption = '??? ????? ???'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 784
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 872
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 873
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 956
    Height = 398
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 784
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Email:'
    end
    object Label2: TLabel
      Left = 12
      Top = 48
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label3: TLabel
      Left = 12
      Top = 88
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label4: TLabel
      Left = 12
      Top = 128
      Width = 33
      Height = 13
      Caption = 'SMTP:'
    end
    object Label8: TLabel
      Left = 12
      Top = 168
      Width = 61
      Height = 13
      Caption = 'Porta SMTP:'
    end
    object EdEMail: TdmkEdit
      Left = 12
      Top = 24
      Width = 265
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'marcelomcw@gmail.com'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'marcelomcw@gmail.com'
      ValWarn = False
    end
    object EdConta: TdmkEdit
      Left = 12
      Top = 64
      Width = 265
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'marcelomcw@gmail.com'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'marcelomcw@gmail.com'
      ValWarn = False
    end
    object EdSenha: TdmkEdit
      Left = 12
      Top = 104
      Width = 265
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'MicellMCW'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'MicellMCW'
      ValWarn = False
    end
    object EdSMTP: TdmkEdit
      Left = 12
      Top = 144
      Width = 265
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'smtp.gmail.com'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'smtp.gmail.com'
      ValWarn = False
    end
    object GroupBox1: TGroupBox
      Left = 304
      Top = 20
      Width = 381
      Height = 149
      Caption = '       '
      TabOrder = 4
      Visible = False
      object Label5: TLabel
        Left = 16
        Top = 20
        Width = 28
        Height = 13
        Caption = 'Email:'
      end
      object Label6: TLabel
        Left = 16
        Top = 60
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label7: TLabel
        Left = 16
        Top = 100
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object dmkEdit3: TdmkEdit
        Left = 16
        Top = 36
        Width = 265
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdit4: TdmkEdit
        Left = 16
        Top = 76
        Width = 265
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdit5: TdmkEdit
        Left = 16
        Top = 116
        Width = 265
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object CheckBox1: TCheckBox
      Left = 316
      Top = 20
      Width = 73
      Height = 17
      Caption = 'Autentica: '
      TabOrder = 5
      Visible = False
    end
    object EdPortaSMTP: TdmkEdit
      Left = 12
      Top = 184
      Width = 265
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '465'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '465'
      ValWarn = False
    end
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv2
    SSLOptions.SSLVersions = [sslvSSLv2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 496
    Top = 244
  end
  object IdSMTP1: TIdSMTP
    SASLMechanisms = <>
    Left = 440
    Top = 244
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    Body.Strings = (
      'wefwefewf'
      'wegf'
      'ewrfg'
      'weg'
      'ewrg'
      'ewr'
      'gewr'
      'g')
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
        Address = 'marcelomcw@gmail.com'
        Name = 'Marcelo Winter'
        Text = 'Marcelo Winter <marcelomcw@gmail.com>'
        Domain = 'gmail.com'
        User = 'marcelomcw'
      end>
    From.Address = 'marcelomcw@gmail.com'
    From.Name = 'Marcelo Winter'
    From.Text = 'Marcelo Winter <marcelomcw@gmail.com>'
    From.Domain = 'gmail.com'
    From.User = 'marcelomcw'
    Recipients = <
      item
        Address = 'marco@dermatek.com.br'
        Name = 'Marco'
        Text = 'Marco <marco@dermatek.com.br>'
        Domain = 'dermatek.com.br'
        User = 'marco'
      end>
    ReplyTo = <>
    Subject = 'Ol'#225' Bob'#227'o'
    ConvertPreamble = True
    Left = 468
    Top = 244
  end
  object IdSMTP2: TIdSMTP
    IOHandler = IdSSLIOHandlerSocketOpenSSL2
    Host = 'smtp.gmail.com'
    Password = 'MicellMCW'
    Port = 587
    SASLMechanisms = <>
    UseTLS = utUseExplicitTLS
    Username = 'marcelomcw@gmail.com'
    Left = 792
    Top = 312
  end
  object IdSSLIOHandlerSocketOpenSSL2: TIdSSLIOHandlerSocketOpenSSL
    Destination = 'smtp.gmail.com:587'
    Host = 'smtp.gmail.com'
    MaxLineAction = maException
    Port = 587
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 764
    Top = 312
  end
end
