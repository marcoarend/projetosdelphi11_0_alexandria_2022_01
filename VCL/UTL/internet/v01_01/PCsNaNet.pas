unit PCsNaNet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, ComCtrls,
  dmkGeral, WinSock, dmkEdit, ShlObj, ActiveX, dmkImage, UnDMkEnums;
  //, Jpeg, IniFiles, Menus, FileCtrl;

type
  PNetResourceArray = ^TNetResourceArray;
  TNetResourceArray = array[0..100] of TNetResource;
  TFmPCsNaNet = class(TForm)
    Panel1: TPanel;
    lstEstacoes: TListView;
    Panel3: TPanel;
    Button1: TButton;
    EdPastaServDmk: TEdit;
    Panel4: TPanel;
    BtPesq1: TBitBtn;
    BtPesq2: TBitBtn;
    BtPesq3: TBitBtn;
    BtPesq4: TBitBtn;
    Panel5: TPanel;
    Label2: TLabel;
    ListaUsuarios: TListBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel7: TPanel;
    BtAltera: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesq1Click(Sender: TObject);
    procedure BtPesq2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtPesq3Click(Sender: TObject);
    procedure BtPesq4Click(Sender: TObject);
    procedure lstEstacoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPastaServDmkChange(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    { Private declarations }
    // Teste 1
    FPlataforma: String;
    FListaUsuarios: TStringList;
    procedure IncluiEstacao(AEstacao, AIP: string);
    procedure ListaAmbienteRede();
    function GetIP(AEstacao: string): String;
    function VersaoWindows(): string;
    // Teste 2
    procedure ScanNetworkResources(ResourceType, DisplayType: DWord; List: TStrings);
    function CreateNetResourceList(ResourceType: DWord;
                              NetResource: PNetResource;
                              out Entries: DWord;
                              out List: PNetResourceArray): Boolean;
    // Teste 3
    function BrowseComputer(DialogTitle: string; var CompName: string;
             bNewStyle: Boolean): Boolean;
    // Todos testes
    procedure PreparaPesquisa();
    procedure ListaEstacoesEIPs();
  public
    { Public declarations }
  end;

  var
  FmPCsNaNet: TFmPCsNaNet;

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

const
  FTxtAguarde = 'A pesquisa poder� levar alguns minutos';

procedure TFmPCsNaNet.BtPesq2Click(Sender: TObject);
var
  I: Integer;
  Nome: String;
begin
  Screen.Cursor := crHourGlass;
  try
    PreparaPesquisa();
    ScanNetworkResources(RESOURCETYPE_DISK, RESOURCEDISPLAYTYPE_SERVER, ListaUsuarios.Items);
    for I := 0 to ListaUsuarios.Count -1 do
    begin
      Nome := ListaUsuarios.Items[I];
      if Copy(Nome, 1, 2) = '\\' then
        Nome := Copy(Nome, 3);
      if Nome <> '' then
        FListaUsuarios.Add(Nome);
    end;
    ListaEstacoesEIPs();
  finally
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmPCsNaNet.BtPesq3Click(Sender: TObject);
var
  Computer: string;
begin
  try
    PreparaPesquisa();
    BrowseComputer(Application.Title, Computer, True);
    EdPastaServDmk.Text := '\\' + Computer + '\Dermatek\';
    FListaUsuarios.Add(Computer);
    ListaEstacoesEIPs();
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmPCsNaNet.BtPesq4Click(Sender: TObject);
  (*
var
  //dwResultEnum: DWORD;
  hEnum, dwResult: DWORD;
  LpnrLocal : array
        [0..16384 div SizeOf(TNetResource)-1] of TNetResource; // ponteiro para as estruturas enumeradas
  i : integer;
  cEntries : Longint;
  cbBuffer: DWORD;
  Nome: String;
  *)
begin
  (*
  adaptar para XE2
  Screen.Cursor := crHourGlass;
  try
    PreparaPesquisa();
    centries := -1;    // enumera todas as entradas poss�veis
    cbBuffer := 16384; // 16K

    // chama a fun��o WNetOpenEnum para inciar a enumera��o.
    dwResult := WNetOpenEnum(
                            RESOURCE_CONTEXT,  // Enumera o recursos
                                               //atualmente conectados.
                            RESOURCETYPE_DISK, // todos os recursos
                            0,                 // enumera todos os recursos
                            nil,               // NULL
                            hEnum              // handle para os recursos
                            );

    if (dwResult <> NO_ERROR) then
    begin
      // poderia processar os erros com um manipulador de erros
      // definido na aplica��o.
      Exit;
    end;

    // inicializa o buffer.
    FillChar( LpnrLocal, cbBuffer, 0 );

    ListaUsuarios.Clear;
    // preencho uma listbox
    for i := 0 to cEntries - 1 do
    begin
      // l� cada estrutura para pegar o nome do
      // recurso remoto (LpnrLocal[i].lpRemoteName)
      Nome := LpnrLocal[i].lpRemoteName;
      if Copy(Nome, 1, 2) = '\\' then
        Nome := Copy(Nome, 3);
      if Trim(Nome) <> '' then
      begin
        ListaUsuarios.Items.Add(Nome);
        FListaUsuarios.Add(Nome);
      end;
    end;

    ListaEstacoesEIPs();
  finally
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
  *)
end;

function TFmPCsNaNet.BrowseComputer(DialogTitle: string; var CompName: string;
  bNewStyle: Boolean): Boolean;
  // bNewStyle: If True, this code will try to use the "new"
  // BrowseForFolders UI on Windows 2000/XP
const
  BIF_USENEWUI = 28;
var
  BrowseInfo: TBrowseInfo;
  ItemIDList: PItemIDList;
  ComputerName: array[0..MAX_PATH] of Char;
  Title: string;
  WindowList: Pointer;
  ShellMalloc: IMalloc;
begin
  if Failed(SHGetSpecialFolderLocation(Application.Handle, CSIDL_NETWORK, ItemIDList)) then
    raise Exception.Create('Unable open browse computer dialog');
  try
    FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
    BrowseInfo.hwndOwner := Application.Handle;
    BrowseInfo.pidlRoot := ItemIDList;
    BrowseInfo.pszDisplayName := ComputerName;
    Title := DialogTitle;
    BrowseInfo.lpszTitle := PChar(Pointer(Title));
    if bNewStyle then
      BrowseInfo.ulFlags := BIF_BROWSEFORCOMPUTER or BIF_USENEWUI
    else
      BrowseInfo.ulFlags := BIF_BROWSEFORCOMPUTER;
    WindowList := DisableTaskWindows(0);
    try
      Result := SHBrowseForFolder(BrowseInfo) <> nil;
    finally
      EnableTaskWindows(WindowList);
    end;
    if Result then CompName := ComputerName;
  finally
    if Succeeded(SHGetMalloc(ShellMalloc)) then
      ShellMalloc.Free(ItemIDList);
  end;
end;

procedure TFmPCsNaNet.BtAlteraClick(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a altera��o da pasta "Dermatek" do servidor?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE ctrlgeral SET DmkNetPath=:P0');
    DMod.QrUpd.Params[0].AsString := EdPastaServDmk.Text;
    DMod.QrUpd.ExecSQL;
    //
    Close;
  end;
end;

procedure TFmPCsNaNet.BtPesq1Click(Sender: TObject);
{
1. Crie uma tela como no exemplo acima. Inclua um TListView e configure como abaixo:
a. Na propriedade ViewStyle coloque como fsReport.
b. Clique duas vezes no TListView e adicione duas colunas. Uma com o nome Esta��es e outra como IP.
c. Aumente senecess�rio as colunas pra melhorar a visualiza��o.
2. Inclua um bot�o na tela e digite o c�digo abaixo:


procedure TForm1.BitBtn1Click(Sender: TObject);
}
begin
  try
    PreparaPesquisa();
    Application.ProcessMessages();
    //**aniLocalizaEstadoes.Active := True;
    Screen.Cursor := crHourGlass;
    FPlataforma := VersaoWindows();
    ListaAmbienteRede;
    lstEstacoes.Items.Clear;
    ListaEstacoesEIPs();
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //**aniLocalizaEstadoes.Active := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPCsNaNet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPCsNaNet.Button1Click(Sender: TObject);
var
  ret: DWORD;
  s: String;
begin
  ret := Geral.GetVolumeserialnumber(DModG.EhOServidor(), EdPastaServDmk.Text);//'\\server\c\');
  s := IntToHex(HiWord(ret), 4) + '-' + IntToHex(Loword(ret), 4);
  ShowMessage(s);
end;

function TFmPCsNaNet.CreateNetResourceList(ResourceType: DWord;
  NetResource: PNetResource; out Entries: DWord;
  out List: PNetResourceArray): Boolean;
var
  EnumHandle: THandle; 
  BufSize: DWord; 
  Res: DWord;
begin 
  Result := False; 
  List := Nil; 
  Entries := 0; 
  if WNetOpenEnum(RESOURCE_GLOBALNET, 
                  ResourceType, 
                  0, 
                  NetResource, 
                  EnumHandle) = NO_ERROR then begin 
    try 
      BufSize := $4000;  // 16 kByte 
      GetMem(List, BufSize); 
      try 
        repeat 
          Entries := DWord(-1); 
          FillChar(List^, BufSize, 0); 
          Res := WNetEnumResource(EnumHandle, Entries, List, BufSize); 
          if Res = ERROR_MORE_DATA then 
          begin 
            ReAllocMem(List, BufSize); 
          end; 
        until Res <> ERROR_MORE_DATA; 
        Result := Res = NO_ERROR; 
        if not Result then 
        begin 
          FreeMem(List); 
          List := Nil; 
          Entries := 0; 
        end; 
      except 
        FreeMem(List); 
        raise; 
      end; 
    finally 
      WNetCloseEnum(EnumHandle); 
    end;
  end; 
end;

procedure TFmPCsNaNet.EdPastaServDmkChange(Sender: TObject);
begin
  BtAltera.Enabled := Trim(EdPastaServDmk.Text) <> '';
end;

procedure TFmPCsNaNet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPCsNaNet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FListaUsuarios.Free;
end;

procedure TFmPCsNaNet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  lstEstacoes.Columns[00].Width := 200;
  lstEstacoes.Columns[01].Width := 134;
  //
  FListaUsuarios := TStringList.Create;
end;

procedure TFmPCsNaNet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmPCsNaNet.GetIP(AEstacao: string): String;
var
  WSAData: TWSAData;
  HostEnt: PHostEnt;
begin
  WSAStartup(2, WSAData);
  HostEnt := GetHostByName(PAnsiChar(AEstacao));
  with HostEnt^ do
    Result := Format('%d.%d.%d.%d', [Byte(h_addr^[0]), Byte(h_addr^[1]), Byte(h_addr^[2]), Byte(h_addr^[3])]);
  WSACleanup;
end;

procedure TFmPCsNaNet.IncluiEstacao(AEstacao, AIP: string);
//3. O c�digo do bot�o Listar Esta��es faz chama a ouras fun��es. Por isso inclua tamb�m as fun��es abaixo:
var
  Item: TListItem;
begin
  with lstEstacoes do
  begin
    Item := Items.Add;
    Item.Caption := AEstacao;
    Item.SubItems.Add(AIP);
  end;
end;

procedure TFmPCsNaNet.ListaAmbienteRede();
  procedure Enumera(Res: PnetResource);
  var
    Hnd: THandle;
    NumeroEntradas: DWord;
    Buffer: array[1..255] of TNetResource;
    LongBuffer: DWord;
    N: Integer;
    S: string;
  begin
    LongBuffer := SizeOf(Buffer);
    if WNetOpenEnum(RESOURCE_GLOBALNET, RESOURCETYPE_ANY, 0, Res, Hnd) = No_ERROR then
    begin
      NumeroEntradas := 255;
      WNetEnumResource(Hnd, NumeroEntradas, @Buffer[1], LongBuffer);
      for N := 1 to NumeroEntradas do
      begin
        S := string(Buffer[N].lpRemoteName);
        ListaUsuarios.Items.Add(s);
        ListaUsuarios.Update;
        Application.ProcessMessages;
        if FPlataforma = 'xp' then
          if (Buffer[1].dwType = 0) and (Copy(S, 1, 2) = '\\') then
            FListaUsuarios.Add(Copy(S, 3, Length(S) - 2));
        if FPlataforma = '9x' then
          if (Buffer[1].dwType = 3) and (Copy(S, 1, 2) = '\\') then
            FListaUsuarios.Add(Copy(S, 3, Length(S) - 2));
        if (Buffer[N].dwUsage and RESOURCEUSAGE_CONTAINER) = RESOURCEUSAGE_CONTAINER then
          Enumera(@Buffer[N]);
        Update;
      end;
    end;
  end;
begin
  Enumera(nil);
end;

procedure TFmPCsNaNet.ListaEstacoesEIPs();
var
  I: Integer;
  Nome, IP: String;
begin
  lstEstacoes.Items.Clear;
  for I := 0 to FListaUsuarios.Count - 1 do
  begin
    Nome := FListaUsuarios[I];
    if Copy(Nome, 1, 2) = '\\' then
      Nome := Copy(Nome, 3);
    if Nome <> '' then
      try
        IP := GetIP(Nome);
      except
        IP := ' ';
      end;
    IncluiEstacao(Nome, IP);
    Update;
  end;
end;

procedure TFmPCsNaNet.lstEstacoesClick(Sender: TObject);
begin
  EdPastaServDmk.Text := '\\' + lstEstacoes.Items[lstEstacoes.ItemIndex].Caption + '\Dermatek';
end;

procedure TFmPCsNaNet.PreparaPesquisa();
begin
  ListaUsuarios.Items.Clear;
  lstEstacoes.Items.Clear;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, FTxtAguarde);
  FListaUsuarios.Clear;
  Update;
  Application.ProcessMessages;
end;

procedure TFmPCsNaNet.ScanNetworkResources(ResourceType, DisplayType: DWord;
  List: TStrings);
  procedure ScanLevel(NetResource: PNetResource);
  var
    Entries: DWord;
    NetResourceList: PNetResourceArray;
    i: Integer;
  begin
    if CreateNetResourceList(ResourceType, NetResource, Entries, NetResourceList) then try
      for i := 0 to Integer(Entries) - 1 do
      begin
        if (DisplayType = RESOURCEDISPLAYTYPE_GENERIC) or
          (NetResourceList[i].dwDisplayType = DisplayType) then begin
          List.AddObject(NetResourceList[i].lpRemoteName,
                        Pointer(NetResourceList[i].dwDisplayType));
        end;
        if (NetResourceList[i].dwUsage and RESOURCEUSAGE_CONTAINER) <> 0 then
          ScanLevel(@NetResourceList[i]);
      end;
    finally
      FreeMem(NetResourceList);
    end;
  end;
begin
  ScanLevel(Nil);
end;

function TFmPCsNaNet.VersaoWindows(): string;
var
  PlatformId, CSDVersion: string;
begin
  CSDVersion := '';
  case Win32Platform of
    VER_PLATFORM_WIN32_WINDOWS:
    begin
      if Win32MajorVersion = 4 then
      case Win32MinorVersion of
        0: PlatformId := '9x';
        10: PlatformId := '9x';
        90: PlatformId := 'ME';
      end else PlatformId := '9x';
      Result := '9x';
    end;

    VER_PLATFORM_WIN32_NT:
    begin
      if Length(Win32CSDVersion) > 0 then
        CSDVersion := Win32CSDVersion;
      if Win32MajorVersion <= 4 then
        PlatformId := 'NT'
      else if Win32MajorVersion = 5 then
        case Win32MinorVersion of
          0: PlatformId := '2000';
          1: PlatformId := 'XP';
          2: PlatformId := '2003';
        else PlatformId := '?';
      end
      else
        PlatformId := '?';
      Result := 'xp';
    end;
  end;
end;

(*
interface
uses
  Windows, SysUtils, Registry, WinSock, WinInet;
type
  TConnectionType = (ctNone, ctProxy, ctDialup);
function ConnectedToInternet: TConnectionType;
function RasConnectionCount: Integer;
implementation
const
  cERROR_BUFFER_TOO_SMALL = 603;
  cRAS_MaxEntryName       = 256;
  cRAS_MaxDeviceName      = 128;
  cRAS_MaxDeviceType      = 16;
type
  ERasError = class(Exception);
  HRASConn = DWORD;
  PRASConn = ^TRASConn;
  TRASConn = record
    dwSize: DWORD;
    rasConn: HRASConn;
    szEntryName: array[0..cRAS_MaxEntryName] of Char;
    szDeviceType: array[0..cRAS_MaxDeviceType] of Char;
    szDeviceName: array [0..cRAS_MaxDeviceName] of Char;
  end;
  TRasEnumConnections =
    function(RASConn: PrasConn; { buffer to receive Connections data }
    var BufSize: DWORD;    { size in bytes of buffer }
    var Connections: DWORD { number of Connections written to buffer }
    ): Longint;
  stdcall;
  //End RasConnectionCount =======================
function ConnectedToInternet: TConnectionType;
var
  Reg:       TRegistry;
  bUseProxy: Boolean;
  UseProxy:  LongWord;
begin
  Result := ctNone;
  Reg    := TRegistry.Create; 
  with REG do 
    try 
      try 
        RootKey := HKEY_CURRENT_USER; 
        if OpenKey('\Software\Microsoft\Windows\CurrentVersion\Internet settings', False) then  
        begin 
          //I just try to read it, and trap an exception 
          if GetDataType('ProxyEnable') = rdBinary then 
            ReadBinaryData('ProxyEnable', UseProxy, SizeOf(Longword)) 
          else  
          begin 
            bUseProxy := ReadBool('ProxyEnable'); 
            if bUseProxy then 
              UseProxy := 1 
            else 
              UseProxy := 0; 
          end; 
          if (UseProxy <> 0) and (ReadString('ProxyServer') <> '') then 
            Result := ctProxy; 
        end; 
      except
        //Obviously not connected through a proxy 
      end; 
    finally 
      Free; 
    end; 
  //We can check RasConnectionCount even if dialup networking is not installed 
  //simply because it will return 0 if the DLL is not found. 
  if Result = ctNone then  
  begin 
    if RasConnectionCount > 0 then Result := ctDialup; 
  end; 
end; 
function RasConnectionCount: Integer; 
var 
  RasDLL:    HInst; 
  Conns:     array[1..4] of TRasConn; 
  RasEnums:  TRasEnumConnections; 
  BufSize:   DWORD; 
  NumConns:  DWORD; 
  RasResult: Longint; 
begin 
  Result := 0; 
  //Load the RAS DLL 
  RasDLL := LoadLibrary('rasapi32.dll'); 
  if RasDLL = 0 then Exit; 
  try 
    RasEnums := GetProcAddress(RasDLL, 'RasEnumConnectionsA'); 
    if @RasEnums = nil then 
      raise ERasError.Create('RasEnumConnectionsA not found in rasapi32.dll'); 
    Conns[1].dwSize := SizeOf(Conns[1]); 
    BufSize         := SizeOf(Conns); 
    RasResult := RasEnums(@Conns, BufSize, NumConns); 
    if (RasResult = 0) or (Result = cERROR_BUFFER_TOO_SMALL) then Result := NumConns; 
  finally 
    FreeLibrary(RasDLL); 
  end; 
end;
*)
end.
