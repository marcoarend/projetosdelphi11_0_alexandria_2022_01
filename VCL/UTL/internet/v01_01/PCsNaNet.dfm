object FmPCsNaNet: TFmPCsNaNet
  Left = 339
  Top = 185
  Caption = 'NET-LOCAL-001 :: Esta'#231#245'es Conectadas na Rede Local'
  ClientHeight = 622
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object lstEstacoes: TListView
      Left = 316
      Top = 48
      Width = 468
      Height = 340
      Align = alClient
      Columns = <
        item
          Caption = 'Nome da Esta'#231#227'o'
          Width = 360
        end
        item
          Caption = 'IP da Esta'#231#227'o'
          Width = 150
        end>
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = lstEstacoesClick
    end
    object Panel3: TPanel
      Left = 0
      Top = 388
      Width = 784
      Height = 72
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 465
        Height = 13
        Caption = 
          'Clique no nome da esta'#231#227'o para informar o diret'#243'rio dermatek do ' +
          'servidor ou informe manualmente:'
      end
      object Label4: TLabel
        Left = 8
        Top = 48
        Width = 46
        Height = 13
        Caption = 'Exemplo: '
      end
      object Label5: TLabel
        Left = 56
        Top = 48
        Width = 143
        Height = 13
        Caption = '\\SERVIDOR\Dermatek\'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Button1: TButton
        Left = 480
        Top = 20
        Width = 185
        Height = 25
        Caption = 'Volume Serial Number'
        TabOrder = 0
        OnClick = Button1Click
      end
      object EdPastaServDmk: TEdit
        Left = 8
        Top = 24
        Width = 469
        Height = 21
        TabOrder = 1
        OnChange = EdPastaServDmkChange
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object BtPesq1: TBitBtn
        Tag = 22
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Pesquisa &1'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesq1Click
      end
      object BtPesq2: TBitBtn
        Tag = 22
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Pesquisa &2'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesq2Click
      end
      object BtPesq3: TBitBtn
        Tag = 22
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Pesquisa &3'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPesq3Click
      end
      object BtPesq4: TBitBtn
        Tag = 22
        Left = 392
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Pesquisa &4'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtPesq4Click
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 48
      Width = 316
      Height = 340
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel5'
      TabOrder = 3
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 316
        Height = 24
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Recursos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 183
      end
      object ListaUsuarios: TListBox
        Left = 0
        Top = 24
        Width = 316
        Height = 316
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 451
        Height = 32
        Caption = 'Esta'#231#245'es Conectadas na Rede Local'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 451
        Height = 32
        Caption = 'Esta'#231#245'es Conectadas na Rede Local'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 451
        Height = 32
        Caption = 'Esta'#231#245'es Conectadas na Rede Local'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 508
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 552
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtAlteraClick
      end
    end
  end
end
