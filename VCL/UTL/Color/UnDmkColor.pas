unit UnDmkColor;
{
# Author: Arah J. Leonard
# Copyright 01AUG09
# Distributed under the LGPL - http://www.gnu.org/copyleft/lesser.html
# ALSO distributed under the The MIT License from the Open Source Initiative (OSI) - http://www.opensource.org/licenses/mit-license.php
# You may use EITHER of these licenses to work with / distribute this source code.
# Enjoy!

# Convert a red-green-blue system to a red-yellow-blue system.
def rgb2ryb(r, g, b):
	t = type(r)

	# Remove the whiteness from the color.
	w = float(min(r, g, b))
	r = float(r) - w
	g = float(g) - w
	b = float(b) - w

	mg = max(r, g, b)

	# Get the yellow out of the red+green.
	y = min(r, g)
	r -= y
	g -= y

	# If this unfortunate conversion combines blue and green, then cut each in half to preserve the value's maximum range.
	if b and g:
		b /= 2.0
		g /= 2.0

	# Redistribute the remaining green.
	y += g
	b += g

	# Normalize to values.
	my = max(r, y, b)
	if my:
		n = mg / my
		r *= n
		y *= n
		b *= n

	# Add the white back in.
	r += w
	y += w
	b += w

	# And return back the ryb typed accordingly.
	return t(r), t(y), t(b)

# Convert a red-yellow-blue system to a red-green-blue system.
def ryb2rgb(r, y, b):
	t = type(r)

	# Remove the whiteness from the color.
	w = float(min(r, y, b))
	r = float(r) - w
	y = float(y) - w
	b = float(b) - w

	my = max(r, y, b)

	# Get the green out of the yellow and blue
	g = min(y, b)
	y -= g
	b -= g

	if b and g:
		b *= 2.0
		g *= 2.0

	# Redistribute the remaining yellow.
	r += y
	g += y

	# Normalize to values.
	mg = max(r, g, b)
	if mg:
		n = my / mg
		r *= n
		g *= n
		b *= n

	# Add the white back in.
	r += w
	g += w
	b += w

	# And return back the ryb typed accordingly.
	return t(r), t(g), t(b)

# Return the complementary color values for a given color.  You must also give it the upper limit of the color values, typically 255 for GUIs, 1.0 for OpenGL.
def complimentary(r, g, b, limit=255):
	return limit - r, limit - g, limit - b

# Debugging test code.  Not intended to be used as an application.
if __name__=="__main__":
	red = (255, 0, 0)
	green = (0, 255, 0)
	blue = (0, 0, 255)
	cyan = (0, 255, 255)
	magenta = (255, 0, 255)
	yellow = (255, 255, 0)
	black = (0, 0, 0)
	white = (255, 255, 255)
	orange = (255, 128, 0)
	darkgreen = (0, 128, 0)
	tests = [red, green, blue, cyan, magenta, yellow, black, white, orange, darkgreen, (255, 128, 64), (255, 64, 128), (64, 255, 128), (128, 255, 64), (64, 128, 255), (128, 64, 255)]
	for test in tests:
		ryb = rgb2ryb(test[0], test[1], test[2])
		rgb = ryb2rgb(ryb[0], ryb[1], ryb[2])
		cryb = complimentary(ryb[0], ryb[1], ryb[2])
		crgb = ryb2rgb(cryb[0], cryb[1], cryb[2])
		print test, "rgb2ryb", ryb, "ryb2rgb", rgb
		print "complimentary rgb", complimentary(rgb[0], rgb[1], rgb[2])
		print "complimentary ryb", cryb, "to rgb", crgb
		print
Tags: adobe, blue, cmy, color, complementary, component, conversion, convert, cyan, gimp, green, gui, lgpl, magenta, mit license, paint shop pro, photoshop, primary, red, rgb, rgb to ryb, rgb2ryb, rgb2ryb.py, ryb, ryb to rgb, secondary, yellow
Category: computer programming  |  6 Comments
}

interface

uses  System.SysUtils, System.Types, System.Math,
    Windows,                      // TRGBQuad
    Graphics,                     // TBitmap
    DmkGeral, UnDmkEnums, UnDmkProcFunc;

type
  THSBColor = record
   Hue,
   Saturation,
   Brightness : Double;
  end;

  THSV = record  // hue saturation value (HSV)
    Hue,
    Sat,
    Val: Double;
  end;
  TRGBFlu = record  // red green blue (RGB)
    R,
    G,
    B: Double;
  end;
  TRGBColor = record
  case Integer of
    0 : (Color : TColor) ;
    1 : (Red, Green, Blue, Intensity : Byte) ;
    2 : (Colors : array[0..3] of Byte) ;
  end;

  TDmkColor = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ColorToRGBTriple(const Color: TColor): TRGBTriple;
    procedure HSLtoRGB2(H, S, L: Double; var R, G, B: Double);
    { testei mas n�o gostei
    function  HSLToRGBTriple(H, S, L: Integer;
              const MaxHue: Integer = 239;
              const MaxSat: Integer = 240;
              const MaxLum: Integer = 240): TRGBTriple;
    }
    //procedure HSLtoRGB2(H, S, L:Double; var R,G,B: Double);
    function  RGBToHSB(rgb: TRGBTriple): THSBColor;
    procedure RGB2RYB(const RGBr, RGBg, RGBb: Double; var RYBr, RYBy, RYBb: Double);
    procedure RYB2RGB(const RYBr, RYBy, RYBb: Double; var RGBr, RGBg, RGBb: Double);
    function  RGB2HSV(R, G, B: Byte): THSV;
    procedure RGBtoHSLRange(const RGB: TColor; var H1, S1, L1: Integer;
              const MaxHue: Integer = 239;
              const MaxSat: Integer = 240;
              const MaxLum: Integer = 240);
    procedure RGBtoHSLRange2(const RGBr, RGBg, RGBb: Double; var HSLh, HSLs,
              HSLl: Double;
              const MaxHue: Integer = 239;
              const MaxSat: Integer = 240;
              const MaxLum: Integer = 240);
    procedure RGBTripleToCMY(const RGB: TRGBTriple; var C, M, Y: Integer);
    procedure RGBTripleToCMYK(const RGB: TRGBTriple; var C, M, Y, K: Integer);
    procedure RGBTripleToCMYKW(const RGB: TRGBTriple; var C, M, Y, K, W: Double);
    procedure RGBTripleToCMYKW_1(const RGB: TRGBTriple; var C, M, Y, K, W: Double); //?
    procedure RGBTripleToCMYKW_2(const RGB: TRGBTriple; var C, M, Y, K, W: Double);
    procedure RGBTripleToCMYKW_3(const RGB: TRGBTriple; var C, M, Y, K, W: Double); // 2
    procedure RGBTripleToCMYKW_4(const RGB: TRGBTriple; var C, M, Y, K, W: Double); // 1
    //
    procedure Matiz_Mistura2CoresRGB(const RGBr1, RGBg1, RGBb1, RGBr2, RGBg2,
              RGBb2: Double; const Perc1, Perc2: Double; const Forca1, Forca2:
              Double; var RGBr, RGBg, RGBb: Double);
  end;
var
  DmkColor: TDmkColor;


implementation

{ TDmkColor }

function TDmkColor.ColorToRGBTriple(const Color: TColor): TRGBTriple;
begin
   with Result do
   begin
     rgbtRed   := GetRValue(Color);
     rgbtGreen := GetGValue(Color);
     rgbtBlue  := GetBValue(Color)
   end;
end;

procedure TDmkColor.HSLtoRGB2(H, S, L: Double; var R, G, B: Double);
var
  Sat,Lum : Double;
begin
  R := 0;
  G := 0;
  B := 0;
  if (H < 360) and (H >= 0) and (S <= 100) and (S >= 0) and (L <= 100) and (L >= 0) then begin
    if H <=60 then begin
      R := 255;
      G := (*Round*)((255/60)*H);
      B := 0;
    end
    else if H <=120 then begin
      R := (*Round*)(255-(255/60)*(H-60));
      G := 255;
      B := 0;
    end
    else if H <=180 then begin
      R := 0;
      G := 255;
      B := (*Round*)((255/60)*(H-120));
    end
    else if H <=240 then begin
      R := 0;
      G := (*Round*)(255-(255/60)*(H-180));
      B := 255;
    end
    else if H <=300 then begin
      R := (*Round*)((255/60)*(H-240));
      G := 0;
      B := 255;
    end
    else if H <360 then begin
      R := 255;
      G := 0;
      B := (*Round*)(255-(255/60)*(H-300));
    end;

    Sat := Abs((S-100)/100);
{
    R := (*Round*)(R-((R-128)*Sat));
    G := (*Round*)(G-((G-128)*Sat));
    B := (*Round*)(B-((B-128)*Sat));
}

    R := (*Round*)(R-((R-127.5)*Sat));
    G := (*Round*)(G-((G-127.5)*Sat));
    B := (*Round*)(B-((B-127.5)*Sat));

    Lum := (L-50)/50;
    if Lum > 0 then begin
      R := (*Round*)(R+((255-R)*Lum));
      G := (*Round*)(G+((255-G)*Lum));
      B := (*Round*)(B+((255-B)*Lum));
    end
    else if Lum < 0 then begin
      R := (*Round*)(R+(R*Lum));
      G := (*Round*)(G+(G*Lum));
      B := (*Round*)(B+(B*Lum));
    end;
  end;
end;

procedure TDmkColor.Matiz_Mistura2CoresRGB(const RGBr1, RGBg1, RGBb1, RGBr2,
  RGBg2, RGBb2: Double; const Perc1, Perc2: Double; const Forca1, Forca2: Double;
  var RGBr, RGBg, RGBb: Double);
var
  RYBr1, RYBy1, RYBb1, RYBr2, RYBy2, RYBb2: Double;
  MinVal: Double;
begin
  DmkColor.RGB2RYB(RGBr1, RGBg1, RGBb1, RYBr1, RYBy1, RYBb1);
  DmkColor.RGB2RYB(RGBr2, RGBg2, RGBb2, RYBr2, RYBy2, RYBb2);
  //
  MinVal := dmkPF.MinArrFlu([RYBr1, RYBy1, RYBb1, RYBr2, RYBy2, RYBb2]);
  Geral.MB_Info(Geral.FFT(MinVal, 2, siNegativo));
{
  /
  MinArrDobl([
/
}
end;

{ Testei mas n�o gostei
function TDmkColor.HSLToRGBTriple(H, S, L: Integer; const MaxHue, MaxSat,
  MaxLum: Integer): TRGBTriple;
begin
  //
  procedure Clamp(var Input: integer; Min, Max: integer);
  begin
   if (Input < Min) then Input := Min;
   if (Input > Max) then Input := Max;
  end;
const
  Divisor = 255*60;
var
  hTemp, f, LS, p, q, r: integer;
begin
  Clamp(H, 0, MaxHue);
  Clamp(S, 0, MaxSat);
  Clamp(L, 0, MaxLum);
  if (S = 0) then
    Result := RGBToRGBTriple(L, L, L)
  else
  begin
    hTemp := H mod MaxHue;
    f := hTemp mod 60;
    hTemp := hTemp div 60;
    LS := L*S;
    p := L - LS div MaxLum;
    q := L - (LS*f) div Divisor;
    r := L - (LS*(60 - f)) div Divisor;
    case hTemp of
      0: Result := RGBToRGBTriple(L, r, p);
      1: Result := RGBToRGBTriple(q, L, p);
      2: Result := RGBToRGBTriple(p, L, r);
      3: Result := RGBToRGBTriple(p, q, L);
      4: Result := RGBToRGBTriple(r, p, L);
      5: Result := RGBToRGBTriple(L, p, q);
      else Result := RGBToRGBTriple(0, 0, 0);
    end;
  end;
end;
}

function TDmkColor.RGB2HSV(R, G, B: Byte): THSV;
var
  Min_, Max_, Delta : Double;
  H , S , V : Double ;
begin
  H := 0.0 ;
  Min_ := Min (Min( R,G ), B);
  Max_ := Max (Max( R,G ), B);
  Delta := ( Max_ - Min_ );
  V := Max_ ;
  If ( Max_ <> 0.0 ) then
    S := 255.0 * Delta / Max_
  else
    S := 0.0 ;
  If (S <> 0.0) then
    begin
      If R = Max_ then
        H := (G - B) / Delta
      else
        If G = Max_ then
          H := 2.0 + (B - R) / Delta
        else
          If B = Max_ then
            H := 4.0 + (R - G) / Delta
    End
  else
    H := -1.0 ;
  H := H * 60 ;
  If H < 0.0 then H := H + 360.0;
  with Result Do
    begin
      Hue := H ;             // Hue -> 0..360
      Sat := S * 100 / 255; // Saturation -> 0..100 %
      Val := V * 100 / 255; // Value - > 0..100 %
    end;
end;

procedure TDmkColor.RGB2RYB(const RGBr, RGBg, RGBb: Double; var RYBr, RYBy, RYBb: Double);
var
  r, g, b, y, w, mg, my, n: Double;
begin
{
# Author: Arah J. Leonard
# Copyright 01AUG09
# Distributed under the LGPL - http://www.gnu.org/copyleft/lesser.html
# ALSO distributed under the The MIT License from the Open Source Initiative (OSI) - http://www.opensource.org/licenses/mit-license.php
# You may use EITHER of these licenses to work with / distribute this source code.
# Enjoy!

# Convert a red-green-blue system to a red-yellow-blue system.
def rgb2ryb(r, g, b):
	t = type(r)

	# Remove the whiteness from the color.
	w = float(min(r, g, b))
	r = float(r) - w
	g = float(g) - w
	b = float(b) - w
	mg = max(r, g, b)
}
  w := Min(RGBr, Min(RGBg, RGBb));
  r := RGBr - w;
  g := RGBg - w;
  b := RGBb - w;
 	mg := Max(r, Max(g, b));
{
	# Get the yellow out of the red+green.
	y = min(r, g)
	r -= y
	g -= y
}
  y := Min(r, g);
  r := r - y;
  g := g - y;
{	# If this unfortunate conversion combines blue and green, then cut each in
    half to preserve the value's maximum range.
	if b and g:
		b /= 2.0
		g /= 2.0
}
  //if b = g then
  if (b > 0) and (g > 0) then
  begin
   b := b / 2.0;
   g := g / 2.0;
  end;
{
	# Redistribute the remaining green.
	y += g
	b += g
}
  y := y + g;
  b := b + g;
{
	# Normalize to values.
	my = max(r, y, b)
	if my:
		n = mg / my
		r *= n
		y *= n
		b *= n
}
  my := Max(r, Max(y, b));
  if my <> 0 then
  begin
    n := mg / my;
    r := r * n;
    y := y * n;
    b := b * n;
  end;
{	# Add the white back in.
	r += w
	y += w
	b += w
}
  r := r + w;
  y := y + w;
  b := b + w;
{
	# And return back the ryb typed accordingly.
	return t(r), t(y), t(b)
}
  RYBr := r;
  RYBy := y;
  RYBb := b;
end;

function TDmkColor.RGBToHSB(rgb: TRGBTriple): THSBColor;
// Hue   Saturaton Brightness
// Matiz Contraste Brilho
var
   minRGB, maxRGB, delta : Double;
   h , s , b : Double ;
begin
   H := 0.0 ;
   minRGB := Min(Min(rgb.rgbtRed, rgb.rgbtGreen), rgb.rgbtBlue) ;
   maxRGB := Max(Max(rgb.rgbtRed, rgb.rgbtGreen), rgb.rgbtBlue) ;
   delta := ( maxRGB - minRGB ) ;
   b := maxRGB ;
   if (maxRGB <> 0.0) then s := 255.0 * Delta / maxRGB
   else s := 0.0;
   if (s <> 0.0) then
   begin
     if rgb.rgbtRed = maxRGB then h := (rgb.rgbtGreen - rgb.rgbtBlue) / Delta
     else
       if rgb.rgbtGreen = maxRGB then h := 2.0 + (rgb.rgbtBlue - rgb.rgbtRed) / Delta
       else
         if rgb.rgbtBlue = maxRGB then h := 4.0 + (rgb.rgbtRed - rgb.rgbtGreen) / Delta
   end
   else h := -1.0;
   h := h * 60 ;
   if h < 0.0 then h := h + 360.0;
   with result do
   begin
     Hue := h;
     Saturation := s * 100 / 255;
     Brightness := b * 100 / 255;
   end;
end;

procedure TDmkColor.RGBtoHSLRange(const RGB: TColor; var H1, S1, L1: Integer;
  const MaxHue, MaxSat, MaxLum: Integer);
var
  R, G, B, D, Cmax, Cmin, h, s, l: double;
begin
  H := h1;
  S := s1;
  L := l1;
  R := GetRValue (RGB) / 255;
  G := GetGValue (RGB) / 255;
  B := GetBValue (RGB) / 255;
  Cmax := Max (R, Max (G, B));
  Cmin := Min (R, Min (G, B));
  L := (Cmax + Cmin) / 2;
  if Cmax = Cmin then
  begin
    H := 0;
    S := 0;
  end
  else
  begin
    D := Cmax - Cmin;
    //calc L
    if L < 0.5 then
      S := D / (Cmax + Cmin)
    else
      S := D / (2 - Cmax - Cmin);
    //calc H
    if R = Cmax then
      H := (G - B) / D
    else
    if G = Cmax then
      H := 2 + (B - R) /D
    else
      H := 4 + (R - G) / D;
    H := H / 6;
    if H < 0 then
      H := H + 1;
  end;
  H1 := round (H * MaxHue);
  S1 := round (S * MaxSat);
  L1 := round (L * MaxLum);
end;

procedure TDmkColor.RGBtoHSLRange2(const RGBr, RGBg, RGBb: Double; var HSLh,
  HSLs, HSLl: Double; const MaxHue, MaxSat, MaxLum: Integer);
var
  R, G, B, D, Cmax, Cmin, H, S, L: Double;
begin
  H := HSLh;
  S := HSLs;
  L := HSLl;
(*
  R := GetRValue (RGB) / 255;
  G := GetGValue (RGB) / 255;
  B := GetBValue (RGB) / 255;
*)
  R := RGBr / 255;
  G := RGBg / 255;
  B := RGBb / 255;
  //
  Cmax := Max (R, Max (G, B));
  Cmin := Min (R, Min (G, B));
  L := (Cmax + Cmin) / 2;
  if Cmax = Cmin then
  begin
    H := 0;
    S := 0;
  end
  else
  begin
    D := Cmax - Cmin;
    //calc L
    if L < 0.5 then
      S := D / (Cmax + Cmin)
    else
      S := D / (2 - Cmax - Cmin);
    //calc H
    if R = Cmax then
      H := (G - B) / D
    else
    if G = Cmax then
      H := 2 + (B - R) /D
    else
      H := 4 + (R - G) / D;
    H := H / 6;
    if H < 0 then
      H := H + 1;
  end;
  HSLh := (*round*) (H * MaxHue);
  HSLs := (*round*) (S * MaxSat);
  HSLl := (*round*) (L * MaxLum);
end;

procedure TDmkColor.RGBTripleToCMY(const RGB: TRGBTriple; var C, M, Y: Integer);
begin
  with RGB do
  begin
    C := 255 - rgbtRed;
    M := 255 - rgbtGreen;
    Y := 255 - rgbtBlue
  end
end;

procedure TDmkColor.RGBTripleToCMYK(const RGB: TRGBTriple; var C, M, Y,
  K: Integer);
begin
  RGBTripleToCMY(RGB, C,M,Y);
  K := MinIntValue([C, M, Y]);
  C := C - K;
  M := M - K;
  Y := Y - K
end;

procedure TDmkColor.RGBTripleToCMYKW(const RGB: TRGBTriple; var C, M, Y, K,
  W: Double);
// por Marco Arend
var
  C1, M1, Y1, K1, t: Integer;
  Fator: Double;
var
  HSB: THSBColor;
begin
  HSB := RGBToHSB(RGB);
  //
  RGBTripleToCMY(RGB, C1,M1,Y1);
  K1 := MinIntValue([C1, M1, Y1]);
  W := 255 - MinIntValue([C1, M1, Y1]);
  C1 := C1 - K1;
  M1 := M1 - K1;
  Y1 := Y1 - K1;
  //
  t := C1 + M1 + Y1 + K1;
  W := HSB.Brightness - HSB.Saturation;
  if W < 0 then W := 0;
  if t > 0 then
  begin
    Fator := (100 - W) / t;
    C := C1 * Fator;
    M := M1 * Fator;
    Y := Y1 * Fator;
    K := K1 * Fator;
  end else begin
    C := 0;
    M := 0;
    Y := 0;
    K := 0;
  end;
end;

procedure TDmkColor.RGBTripleToCMYKW_1(const RGB: TRGBTriple; var C, M, Y, K,
  W: Double);
// por Marco Arend
var
  C1, M1, Y1, K1: Integer;
  t, Fator: Double;
var
  HSB: THSBColor;
begin
  Fator := 0;
  HSB := RGBToHSB(RGB);
  //
  RGBTripleToCMY(RGB, C1,M1,Y1);
  K1 := MinIntValue([C1, M1, Y1]);
  //W1 := 1 - ((C1 + M1 + Y1) / (3 * 255));
  C1 := C1 - K1;
  M1 := M1 - K1;
  Y1 := Y1 - K1;
  //
  t  := C1 + M1 + Y1;
  HSB.Saturation := 100 - HSB.Saturation;
  if t = 0 then
  begin
    W  := HSB.Brightness;
    K  := 100 - W;
    Fator := 0;
  end else if HSB.Saturation > 0 then
  begin
    W  := HSB.Saturation * HSB.Brightness / 100;
    K  := HSB.Saturation - W;
    Fator := (100-HSB.Saturation) / t;
  end;
  C := C1 * Fator;
  M := M1 * Fator;
  Y := Y1 * Fator;
end;

procedure TDmkColor.RGBTripleToCMYKW_2(const RGB: TRGBTriple; var C, M, Y, K,
  W: Double);
// por Marco Arend
var
  C1, M1, Y1, K1: Integer;
  t, Fator: Double;
var
  HSB: THSBColor;
begin
  HSB := RGBToHSB(RGB);
  //
  RGBTripleToCMY(RGB, C1,M1,Y1);
  K1 := MinIntValue([C1, M1, Y1]);
  //W1 := 1 - ((C1 + M1 + Y1) / (3 * 255));
  C1 := C1 - K1;
  M1 := M1 - K1;
  Y1 := Y1 - K1;
  //
  t  := C1 + M1 + Y1;
  W  := (100 - HSB.Saturation) * HSB.Brightness / 100;
  K  := (100 - HSB.Saturation) - W;
  if t = 0 then Fator := 0 else Fator := HSB.Saturation / t;
  C := C1 * Fator;
  M := M1 * Fator;
  Y := Y1 * Fator;
end;

procedure TDmkColor.RGBTripleToCMYKW_3(const RGB: TRGBTriple; var C, M, Y, K,
  W: Double);
// por Marco Arend
var
  C1, M1, Y1, K1: Integer;
  W1, t, Fator: Double;
var
  HSB: THSBColor;
begin
  HSB := RGBToHSB(RGB);
  //
  RGBTripleToCMY(RGB, C1,M1,Y1);
  K1 := MinIntValue([C1, M1, Y1]);
  W1 := 1 - ((C1 + M1 + Y1) / (3 * 255));
  C1 := C1 - K1;
  M1 := M1 - K1;
  Y1 := Y1 - K1;
  //
  W  := W1 * K1;
  K  := K1 - W;
  t  := C1 + M1 + Y1 + K + W;
  if t > 0 then
  begin
    Fator := (1 / t) * 100;
    C := C1 * Fator;
    M := M1 * Fator;
    Y := Y1 * Fator;
    K := K  * Fator;
    W := W  * Fator;
  end else begin
    C := 0;
    M := 0;
    Y := 0;
    K := 0;
    W := 100;
  end;
end;

procedure TDmkColor.RGBTripleToCMYKW_4(const RGB: TRGBTriple; var C, M, Y, K,
  W: Double);
  //PROCEDURE RGBToHSV (CONST R,G,B:  TReal; VAR H,S,V:  TReal);
var
  (*H,*) S, V, Delta:  Double;
  Min, CMYt: Double;
  R, G, B, Ci, Mi, Yi, Ki: Integer;
begin
  R := RGB.rgbtRed;
  G := RGB.rgbtGreen;
  B := RGB.rgbtBlue;
  Min := MinValue( [R, G, B] );
  V   := MaxValue( [R, G, B] );
  //H   := 0;
  //
  Delta := V - Min;

  // Calculate saturation:  saturation is 0 if r, g and b are all 0
  if   V =  0.0
  then S := 0
  else S := Delta / V;

  (*Marco*)V := V / 255;  // Porcentagem de branco na satura��o ?
  RGBTripleToCMYK(RGB, Ci, Mi, Yi, Ki);
  if S > 0 then
    CMYt := (Ci + Mi + Yi) / (S * 100)
  else
    CMYt := 0;
  if CMYt > 0 then
  begin
    C := (Ci / CMYt);
    M := (Mi / CMYt);
    Y := (Yi / CMYt);
  end else begin
    C := 0;
    M := 0;
    Y := 0;
  end;
  S := 100 - (S * 100);
  W := S * V;
  K := S - W;
end;

procedure TDmkColor.RYB2RGB(const RYBr, RYBy, RYBb: Double; var RGBr, RGBg,
  RGBb: Double);
var
  r, g, b, y, w, mg, my, n: Double;
begin
{
# Convert a red-yellow-blue system to a red-green-blue system.
def ryb2rgb(r, y, b):
	t = type(r)

	# Remove the whiteness from the color.
	w = float(min(r, y, b))
	r = float(r) - w
	y = float(y) - w
	b = float(b) - w
	my = max(r, y, b)
}
  w := Min(RYBr, Min(RYBy, RYBb));
  r := RYBr - w;
  y := RYBy - w;
  b := RYBb - w;
 	my := Max(r, Max(y, b));
{
	# Get the green out of the yellow and blue
	g = min(y, b)
	y -= g
	b -= g

	if b and g:
		b *= 2.0
		g *= 2.0
}
  g := Min(y, b);
  y := y - g;
  b := b - g;

  if (b > 0) and (g > 0) then
  begin
    b := b * 2.0;
    g := g * 2.0;
  end;

{	# Redistribute the remaining yellow.
	r += y
	g += y
}
  r := r + y;
  g := g + y;
{	# Normalize to values.
	mg = max(r, g, b)
	if mg:
		n = my / mg
		r *= n
		g *= n
		b *= n
}
  mg := Max(r, Max(g, b));
  if mg <> 0 then
  begin
    n := my / mg;
    r := r * n;
    g := g * n;
    b := b * n;
  end;
{	# Add the white back in.
	r += w
	g += w
	b += w
}
  r := r + w;
  g := g + w;
  b := b + w;
{	# And return back the ryb typed accordingly.
	return t(r), t(g), t(b)
}
  RGBr := r;
  RGBg := g;
  RGBb := b;
end;

end.
