unit uOnScreenDisplay;

// Author Dorin Duminica
// Free to use for personal and/or commercial purpose

interface

uses
  Windows,
  SysUtils,
  Messages,
  Classes,
  Graphics,
  StdCtrls,
  ExtCtrls,
  Controls,
  Forms;

type
  TOnScreenDisplay = class(TComponent)
  private
    FOSDForm: TForm;
    FOSDText: TLabel;
    FTimer: TTimer;
    FOnShow: TNotifyEvent;
    FOnHide: TNotifyEvent;
    procedure OnTimer(Sender: TObject);
    function GetHeight: Integer;
    procedure SetHeight(const Value: Integer);
    function GetFont: TFont;
    procedure SetFont(const Value: TFont);
    function GetColor: TColor;
    procedure SetColor(const Value: TColor);
    function GetOnHide: TNotifyEvent;
    function GetOnShow: TNotifyEvent;
    procedure SetOnHide(const Value: TNotifyEvent);
    procedure SetOnShow(const Value: TNotifyEvent);
    function GetAlphaBlendValue: Byte;
    procedure SetAlphaBlendValue(const Value: Byte);
  public
    constructor Create;
    destructor Destroy; override;
  public
    function OSDForm: TForm;
    function OSDText: TLabel;
    procedure Show(const AMessage: string; const TimeOut: Cardinal = 2500);
    procedure ShowFmt(const AMessage: string; const Args: array of const;
      const TimeOut: Cardinal = 2500);
  published
    property AlphaBlendValue: Byte read GetAlphaBlendValue write SetAlphaBlendValue;
    property Color: TColor read GetColor write SetColor;
    property Height: Integer read GetHeight write SetHeight;
    property Font: TFont read GetFont write SetFont;
    // events
    property OnShow: TNotifyEvent read GetOnShow write SetOnShow;
    property OnHide: TNotifyEvent read GetOnHide write SetOnHide;
  end;

var
  GlobalOSD: TOnScreenDisplay;

implementation

{ TOnScreenDisplay }

constructor TOnScreenDisplay.Create;
begin
  // OSDForm
  FOSDForm := TForm.Create(NIL);
  FOSDForm.AlphaBlend := True;
  FOSDForm.AlphaBlendValue := 150;
  FOSDForm.Color := clBlack;
  FOSDForm.Align := alTop;
  FOSDForm.Height := 55;
  FOSDForm.BorderStyle := bsNone;
  FOSDForm.FormStyle := fsStayOnTop;
  SetWindowLong(FOSDForm.Handle, GWL_EXSTYLE, WS_EX_TRANSPARENT or WS_EX_LAYERED);
  // if AlphaBlending is not available uncomment the next line
  // SetLayeredWindowAttributes(FOSDForm.Handle, 0, 150{transparent value}, LWA_ALPHA);
  // OSDText
  FOSDText := TLabel.Create(FOSDForm);
  FOSDText.Parent := FOSDForm;
  FOSDText.Align := alClient;
  FOSDText.AlignWithMargins := True;
  FOSDText.Margins.SetBounds(10, 10, 10, 10);
  FOSDText.Font.Size := 20;
  FOSDText.Font.Name := 'Verdana';
  FOSDText.Font.Style := [fsBold];
  FOSDText.Font.Color := clInfoBk;
  FOSDText.Alignment := taCenter;
  FOSDText.WordWrap := True;
  // timer
  FTimer := TTimer.Create(FOSDForm);
  FTimer.Enabled := False;
  FTimer.OnTimer := Self.OnTimer;
end;

destructor TOnScreenDisplay.Destroy;
begin
  FreeAndNil(FOSDForm);
  inherited;
end;

function TOnScreenDisplay.GetAlphaBlendValue: Byte;
begin
  Result := FOSDForm.AlphaBlendValue;
end;

function TOnScreenDisplay.GetColor: TColor;
begin
  Result := FOSDForm.Color;
end;

function TOnScreenDisplay.GetFont: TFont;
begin
  Result := FOSDText.Font;
end;

function TOnScreenDisplay.GetHeight: Integer;
begin
  Result := FOSDForm.Height;
end;

function TOnScreenDisplay.GetOnHide: TNotifyEvent;
begin
  Result := FOSDForm.OnHide;
end;

function TOnScreenDisplay.GetOnShow: TNotifyEvent;
begin
  Result := FOSDForm.OnShow;
end;

procedure TOnScreenDisplay.OnTimer(Sender: TObject);
begin
  FTimer.Enabled := False;
  FOSDForm.Close;
end;

function TOnScreenDisplay.OSDForm: TForm;
begin
  Result := FOSDForm;
end;

function TOnScreenDisplay.OSDText: TLabel;
begin
  Result := FOSDText;
end;

procedure TOnScreenDisplay.SetAlphaBlendValue(const Value: Byte);
begin
  FOSDForm.AlphaBlendValue := Value;
end;

procedure TOnScreenDisplay.SetColor(const Value: TColor);
begin
  FOSDForm.Color := Value;
end;

procedure TOnScreenDisplay.SetFont(const Value: TFont);
begin
  FOSDText.Font := Value;
end;

procedure TOnScreenDisplay.SetHeight(const Value: Integer);
begin
  FOSDForm.Height := Value;
end;

procedure TOnScreenDisplay.SetOnHide(const Value: TNotifyEvent);
begin
  FOSDForm.OnHide := Value;
end;

procedure TOnScreenDisplay.SetOnShow(const Value: TNotifyEvent);
begin
  FOSDForm.OnShow := Value;
end;

procedure TOnScreenDisplay.Show(const AMessage: string; const TimeOut: Cardinal);
begin
  FTimer.Enabled := False;
  FOSDText.Caption := AMessage;
  FTimer.Interval := TimeOut;
  FTimer.Enabled := True;
  FOSDForm.Show;
end;

procedure TOnScreenDisplay.ShowFmt(const AMessage: string;
  const Args: array of const; const TimeOut: Cardinal);
begin
  Show(Format(AMessage, Args), TimeOut);
end;

initialization
  GlobalOSD := TOnScreenDisplay.Create;

finalization
  FreeAndNil(GlobalOSD);

end.
