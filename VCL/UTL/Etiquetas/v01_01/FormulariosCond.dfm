object FmFormulariosCond: TFmFormulariosCond
  Left = 339
  Top = 185
  Caption = 'ETQ-MALAD-009 :: Adi'#231#227'o de Cond'#244'minos'
  ClientHeight = 502
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 562
    Height = 340
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 562
      Height = 46
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 56
        Top = 20
        Width = 493
        Height = 21
        KeyField = 'CO_SHOW'
        ListField = 'NO_EMPRESA'
        ListSource = DsEmp
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 0
      Top = 46
      Width = 562
      Height = 294
      OnAfterSQLExec = dmkDBGridDAC1AfterSQLExec
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Propriet'#225'rio'
          Width = 390
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTmpMalaDirCon
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Propriet'#225'rio'
          Width = 390
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 562
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 514
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 466
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 280
        Height = 32
        Caption = 'Adi'#231#227'o de Cond'#244'minos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 280
        Height = 32
        Caption = 'Adi'#231#227'o de Cond'#244'minos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 280
        Height = 32
        Caption = 'Adi'#231#227'o de Cond'#244'minos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 388
    Width = 562
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 558
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 432
    Width = 562
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 558
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 414
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 158
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 282
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrEmp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, uf.Nome NO_UF, en.CliInt, '
      'FLOOR(IF(en.Codigo<-10,en.Filial,en.CliInt)) CO_SHOW,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_EMPRESA,'
      'IF(en.Tipo=0,en.ECidade,en.PCidade) CIDADE'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'WHERE en.Codigo < -10'
      'OR en.Codigo=-1'
      'OR en.CliInt <> 0')
    Left = 108
    Top = 136
    object QrEmpCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEmpNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEmpNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Required = True
      Size = 100
    end
    object QrEmpCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEmpCO_SHOW: TLargeintField
      FieldName = 'CO_SHOW'
      Required = True
    end
    object QrEmpCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsEmp: TDataSource
    DataSet = QrEmp
    Left = 136
    Top = 136
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cni.Conta, cni.Unidade, CASE WHEN ent.Tipo=0 THEN'
      'ent.RazaoSocial ELSE ent.Nome END NOMEDONO'
      'FROM CondImov cni'
      'LEFT JOIN Entidades ent ON ent.Codigo=cni.Propriet'
      'WHERE cni.Codigo=:P0'
      'ORDER BY cni.Unidade')
    Left = 164
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrAptosNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
  end
  object QrTmpMalaDirCon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tmp.Codigo, tmp.Ativo, tmp.Nome, imv.Unidade, '
      'imv.Codigo CliInt, imv.Propriet'
      'FROM __2__cod_nom_ tmp'
      'LEFT JOIN synker.condimov imv ON imv.Conta = tmp.Codigo'
      'ORDER BY imv.Unidade')
    Left = 192
    Top = 136
    object QrTmpMalaDirConCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTmpMalaDirConAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrTmpMalaDirConNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrTmpMalaDirConUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrTmpMalaDirConCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrTmpMalaDirConPropriet: TIntegerField
      FieldName = 'Propriet'
    end
  end
  object DsTmpMalaDirCon: TDataSource
    DataSet = QrTmpMalaDirCon
    Left = 220
    Top = 136
  end
end
