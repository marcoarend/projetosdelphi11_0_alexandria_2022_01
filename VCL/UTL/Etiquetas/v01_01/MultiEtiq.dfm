object FmMultiEtiq: TFmMultiEtiq
  Left = 343
  Top = 214
  Caption = 'ETQ-MALAD-002 :: Configura'#231#245'es de Impress'#245'es'
  ClientHeight = 495
  ClientWidth = 748
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 550
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 748
    Height = 379
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 748
      Height = 160
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Codigo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 92
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 12
        Top = 48
        Width = 66
        Height = 13
        Caption = 'Observa'#231#245'es:'
      end
      object DBEdCodigo: TDBEdit
        Left = 12
        Top = 20
        Width = 76
        Height = 21
        TabStop = False
        Color = clWhite
        DataField = 'Codigo'
        DataSource = DsMultietq
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 3
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 92
        Top = 20
        Width = 509
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsMultietq
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBMemo1: TDBMemo
        Left = 12
        Top = 64
        Width = 521
        Height = 89
        DataField = 'Observa'
        DataSource = DsMultietq
        TabOrder = 2
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 536
        Top = 60
        Width = 65
        Height = 93
        Caption = ' Linhas: '
        DataField = 'Linhas'
        DataSource = DsMultietq
        Items.Strings = (
          '1'
          '2'
          '3'
          '4'
          '5')
        TabOrder = 3
        Values.Strings = (
          '1'
          '2'
          '3'
          '4'
          '5')
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 160
      Width = 748
      Height = 219
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object pnUnamed: TPanel
        Left = 0
        Top = 0
        Width = 748
        Height = 219
        Align = alClient
        BevelOuter = bvNone
        Caption = 'pnUnamed'
        ParentColor = True
        TabOrder = 0
        object Label4: TLabel
          Left = 244
          Top = 92
          Width = 33
          Height = 13
          Caption = 'Codigo'
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 748
          Height = 175
          Align = alClient
          DataSource = DsMultiEtiqIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto1'
              Width = 236
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto2'
              Width = 236
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto3'
              Width = 236
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto4'
              Width = 236
              Visible = True
            end>
        end
        object GBAvisos1: TGroupBox
          Left = 0
          Top = 175
          Width = 748
          Height = 44
          Align = alBottom
          Caption = ' Avisos: '
          TabOrder = 1
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 744
            Height = 27
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAviso1: TLabel
              Left = 13
              Top = 2
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso2: TLabel
              Left = 12
              Top = 1
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 748
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 700
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 484
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 362
        Height = 32
        Caption = 'Configura'#231#245'es de Impress'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 362
        Height = 32
        Caption = 'Configura'#231#245'es de Impress'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 362
        Height = 32
        Caption = 'Configura'#231#245'es de Impress'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 431
    Width = 748
    Height = 64
    Align = alBottom
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 172
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object LaRegistro: TStaticText
      Left = 174
      Top = 15
      Width = 30
      Height = 17
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel3: TPanel
      Left = 225
      Top = 15
      Width = 521
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Panel4: TPanel
        Left = 388
        Top = 0
        Width = 133
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 124
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 244
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
    end
  end
  object QrMultietq: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrMultietqAfterScroll
    SQL.Strings = (
      'SELECT * FROM multietq'
      '')
    Left = 228
    Top = 60
    object QrMultietqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.multietq.Codigo'
    end
    object QrMultietqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.multietq.Nome'
      Size = 128
    end
    object QrMultietqLinhas: TIntegerField
      FieldName = 'Linhas'
      Origin = 'DBMMONEY.multietq.Linhas'
    end
    object QrMultietqObserva: TWideMemoField
      FieldName = 'Observa'
      Origin = 'DBMMONEY.multietq.Observa'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrMultietqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'DBMMONEY.multietq.DataCad'
    end
    object QrMultietqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'DBMMONEY.multietq.DataAlt'
    end
    object QrMultietqUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'DBMMONEY.multietq.UserCad'
    end
    object QrMultietqUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'DBMMONEY.multietq.UserAlt'
    end
    object QrMultietqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.multietq.Lk'
    end
  end
  object DsMultietq: TDataSource
    AutoEdit = False
    DataSet = QrMultietq
    Left = 256
    Top = 60
  end
  object TbMultiEtiqIts: TMySQLTable
    Database = Dmod.MyDB
    IndexFieldNames = 'Codigo'
    MasterFields = 'Codigo'
    MasterSource = DsMultietq
    TableName = 'multietqits'
    Left = 109
    Top = 246
    object TbMultiEtiqItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbMultiEtiqItsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbMultiEtiqItsTexto1: TWideStringField
      FieldName = 'Texto1'
      Size = 100
    end
    object TbMultiEtiqItsTexto2: TWideStringField
      FieldName = 'Texto2'
      Size = 100
    end
    object TbMultiEtiqItsTexto3: TWideStringField
      FieldName = 'Texto3'
      Size = 100
    end
    object TbMultiEtiqItsTexto4: TWideStringField
      FieldName = 'Texto4'
      Size = 100
    end
  end
  object DsMultiEtiqIts: TDataSource
    DataSet = TbMultiEtiqIts
    Left = 137
    Top = 246
  end
end
