unit FormulariosEntidades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Buttons, ComCtrls, Db, (*DBTables,*) DmkDAC_PF,
  UnInternalConsts, ExtCtrls, dmkGeral, mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmFormulariosEntidades = class(TForm)
    DsEntidades: TDataSource;
    Panel2: TPanel;
    c: TLabel;
    CBEntidade: TDBLookupComboBox;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmFormulariosEntidades: TFmFormulariosEntidades;

implementation

uses UnMyObjects, Module, Maladireta, ModuleGeral;

{$R *.DFM}

procedure TFmFormulariosEntidades.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  BtConfirma.SetFocus;
  CBEntidade.SetFocus;
end;

procedure TFmFormulariosEntidades.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, DMod.MyDB);
end;

procedure TFmFormulariosEntidades.BtConfirmaClick(Sender: TObject);
begin
  if Trim(CBEntidade.Text) = '' then
    Geral.MB_Aviso('Defina a entidade')
  else begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO lista1 SET Tipo=1, Nome='''+CBEntidade.Text+'''');
    DModG.QrUpdPID1.ExecSQL;
    Geral.MB_Info('A entidade ' + CBEntidade.Text + ' foi inserida');
    FmMaladireta.ReopenEnti;
  end;
end;

procedure TFmFormulariosEntidades.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulariosEntidades.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
