unit Pages;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, ExtDlgs, dmkLabel,
  ZCF2, ResIntStrings, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, UnGOTOy, UnMySQLCuringa, Grids, DBGrids, Menus,
  ComCtrls, mySQLDbTables, UnDmkProcFunc, dmkImage, dmkGeral, UnDmkEnums;

type
  TFmPages = class(TForm)
    PainelDados: TPanel;
    QrPages: TmySQLQuery;
    DsPages: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBMemo1: TDBMemo;
    Label3: TLabel;
    QrPagesNome: TWideStringField;
    QrPagesCodigo: TIntegerField;
    QrPagesPagCol: TIntegerField;
    QrPagesPagLin: TIntegerField;
    QrPagesPagLef: TIntegerField;
    QrPagesPagTop: TIntegerField;
    QrPagesPagWid: TIntegerField;
    QrPagesPagHei: TIntegerField;
    QrPagesPagGap: TIntegerField;
    QrPagesBanCol: TIntegerField;
    QrPagesBanLin: TIntegerField;
    QrPagesBanWid: TIntegerField;
    QrPagesBanHei: TIntegerField;
    QrPagesBanLef: TIntegerField;
    QrPagesBanTop: TIntegerField;
    QrPagesBanGap: TIntegerField;
    QrPagesOrient: TIntegerField;
    QrPagesDataCad: TDateField;
    QrPagesDataAlt: TDateField;
    QrPagesUserCad: TSmallintField;
    QrPagesUserAlt: TSmallintField;
    QrPagesLk: TIntegerField;
    QrPagesObserva: TWideMemoField;
    QrPagesBa2Hei: TIntegerField;
    QrPagesColWid: TIntegerField;
    QrPagesFoSize: TSmallintField;
    QrPagesMemHei: TIntegerField;
    QrPagesEspaco: TSmallintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    EdPagCol: TDBEdit;
    EdPagLin: TDBEdit;
    EdPagLef: TDBEdit;
    EdPagTop: TDBEdit;
    EdPagWid: TDBEdit;
    EdPagHei: TDBEdit;
    EdPagGap: TDBEdit;
    EdBanWid: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    EdBanCol: TDBEdit;
    EdBanLin: TDBEdit;
    EdBanHei: TDBEdit;
    EdBanLef: TDBEdit;
    EdBanTop: TDBEdit;
    EdBanGap: TDBEdit;
    DBEdit2: TDBEdit;
    GroupBox3: TGroupBox;
    Bevel1: TBevel;
    ImageP: TImage;
    ImageL: TImage;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    RGOrientation: TDBRadioGroup;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label27: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit10: TDBEdit;
    QrPagesBarLef: TIntegerField;
    QrPagesBarTop: TIntegerField;
    QrPagesBarWid: TIntegerField;
    QrPagesBarHei: TIntegerField;
    Label22: TLabel;
    DBEdit5: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnSemNome: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure QrPagesAfterScroll(DataSet: TDataSet);
    procedure QrPagesAfterOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;

   //Procedures do form
    procedure CriaPagesNew(Tipo: Integer);

  public
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ExcluiMovimento;

    { Public declarations }
  end;

var
  FmPages: TFmPages;
  Cs_ControleIts, Cs_ControleIts2: Double;

implementation

uses UnMyObjects, Module, PagesNew;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPages.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPages.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPagesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPages.DefParams;
begin
  VAR_GOTOTABELA := 'Pages';
  VAR_GOTOmySQLTABLE := QrPages;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := 'Codigo';
  VAR_GOTONOME := '';
  VAR_GOTOVAR := 0;//1;
  //VAR_GOTOVAR1 := 'Empresa='+IntToStr(VAR_TERCEIRO);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT * From pages');
  //
  VAR_SQL1.Add('WHERE Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmPages.CriaOForm;
begin
  //if Screen.Width > 800 then WindowState := wsMaximized;
  DefParams;
  Va(vpLast);
end;
///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmPages.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPages.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPages.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPages.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPages.BtSaidaClick(Sender: TObject);
begin
  VAR_CONFIPAGE := QrPagesCodigo.Value;
  Close;
end;

procedure TFmPages.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmPages.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPagesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPages.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPages.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPages.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmPages.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPages.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPages.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPagesCodigo.Value,
  CuringaLoc.CriaForm('Codigo', 'Descricao', 'Pages', VAR_GOTOMySQLDBNAME, CO_VAZIO));
end;

procedure TFmPages.QrPagesAfterScroll(DataSet: TDataSet);
begin
  //
end;

procedure TFmPages.QrPagesAfterOpen(DataSet: TDataSet);
begin
  BtExclui.Enabled := BtInclui.Enabled;
  case QrPagesOrient.Value of
    0: ImageL.Visible := True;
    1: ImageP.Visible := True;
  end;
  case QrPagesOrient.Value of
    0: ImageP.Visible := False;
    1: ImageL.Visible := False;
  end;
end;

procedure TFmPages.BtExcluiClick(Sender: TObject);
begin
  ExcluiMovimento;
end;

procedure TFmPages.ExcluiMovimento;
begin
end;

procedure TFmPages.CriaPagesNew(Tipo: Integer);
var
  Codigo: Integer;
begin
  Application.CreateForm(TFmPagesNew, FmPagesNew);
  with FmPagesNew do
  begin
    if Tipo = 0 then
    begin
      ImgTipo.SQLType := stIns;
      Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Pages', 'Pages', 'Codigo')
    end else begin
      ImgTipo.SQLType := stUpd;
      Codigo := QrPagesCodigo.Value;
      EdDescricao.Text := QrPagesNome.Value;
      Memo1.Text := QrPagesObserva.Value;

      EdPagCol.Text := IntToStr(QrPagesPagCol.Value);
      EdPagLin.Text := IntToStr(QrPagesPagLin.Value);
      EdPagLef.Text := IntToStr(QrPagesPagLef.Value);
      EdPagTop.Text := IntToStr(QrPagesPagTop.Value);
      EdPagWid.Text := IntToStr(QrPagesPagWid.Value);
      EdPagHei.Text := IntToStr(QrPagesPagHei.Value);
      EdPagGap.Text := IntToStr(QrPagesPagGap.Value);
      //
      EdBanCol.Text := IntToStr(QrPagesBanCol.Value);
      EdBanLin.Text := IntToStr(QrPagesBanLin.Value);
      EdBanLef.Text := IntToStr(QrPagesBanLef.Value);
      EdBanTop.Text := IntToStr(QrPagesBanTop.Value);
      EdBanWid.Text := IntToStr(QrPagesBanWid.Value);
      EdBanHei.Text := IntToStr(QrPagesBanHei.Value);
      EdBa2Hei.Text := IntToStr(QrPagesBa2Hei.Value);
      EdBanGap.Text := IntToStr(QrPagesBanGap.Value);
      //
      EdColWid.Text := IntToStr(QrPagesColWid.Value);
      EdFoSize.Text := IntToStr(QrPagesFoSize.Value);
      EdMemHei.Text := IntToStr(QrPagesMemHei.Value);
      //
      RGOrientation.ItemIndex := QrPagesOrient.Value;
      RGLinhaNula.ItemIndex := QrPagesEspaco.Value;
      //
      EdBarLef.Text := IntToStr(QrPagesBarLef.Value);
      EdBarTop.Text := IntToStr(QrPagesBarTop.Value);
      EdBarWid.Text := IntToStr(QrPagesBarWid.Value);
      EdBarHei.Text := IntToStr(QrPagesBarHei.Value);
    end;
    EdCodigo.Text := IntToStr(Codigo);
    ShowModal;
    Destroy;
  end;
  DefParams;
  LocCod(Codigo, Codigo);
end;

procedure TFmPages.BtIncluiClick(Sender: TObject);
begin
  CriaPagesNew(0);
end;

procedure TFmPages.BtAlteraClick(Sender: TObject);
begin
  CriaPagesNew(1);
end;

procedure TFmPages.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

