unit FormulariosCond;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkDBGrid, dmkDBGridDAC, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmFormulariosCond = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrEmp: TmySQLQuery;
    QrEmpCodigo: TIntegerField;
    QrEmpNO_UF: TWideStringField;
    QrEmpNO_EMPRESA: TWideStringField;
    QrEmpCIDADE: TWideStringField;
    QrEmpCO_SHOW: TLargeintField;
    QrEmpCliInt: TIntegerField;
    DsEmp: TDataSource;
    QrAptos: TmySQLQuery;
    QrTmpMalaDirCon: TmySQLQuery;
    DsTmpMalaDirCon: TDataSource;
    QrTmpMalaDirConAtivo: TSmallintField;
    QrTmpMalaDirConNome: TWideStringField;
    QrTmpMalaDirConUnidade: TWideStringField;
    QrAptosConta: TIntegerField;
    QrAptosUnidade: TWideStringField;
    QrAptosNOMEDONO: TWideStringField;
    QrTmpMalaDirConCodigo: TIntegerField;
    dmkDBGridDAC1: TdmkDBGridDAC;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtSaida: TBitBtn;
    QrTmpMalaDirConCliInt: TIntegerField;
    QrTmpMalaDirConPropriet: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkDBGridDAC1AfterSQLExec(Sender: TObject);
  private
    { Private declarations }
    FMalaDirCon: String;
    procedure MontaTmpTable(CliInt: Integer);
    procedure ReopenTmpMalaDirCon;
    procedure AtualizaTodos(Status: Integer);
  public
    { Public declarations }
  end;

  var
  FmFormulariosCond: TFmFormulariosCond;

implementation

uses UnMyObjects, ModuleGeral, UCreate, UnInternalConsts, UMySQLModule,
  Maladireta, Module;

{$R *.DFM}

procedure TFmFormulariosCond.AtualizaTodos(Status: Integer);
begin
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FMalaDirCon + ' SET Ativo=:P0');
  DmodG.QrUpdPID1.Params[00].AsInteger := Status;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenTmpMalaDirCon;
end;

procedure TFmFormulariosCond.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmFormulariosCond.BtOKClick(Sender: TObject);
var
  Codigo, Depto, CliInt: Integer;
  Nome: String;
begin
  QrTmpMalaDirCon.First;
  while not QrTmpMalaDirCon.Eof do
  begin
    Depto  := QrTmpMalaDirConCodigo.Value;
    Nome   := QrTmpMalaDirConNome.Value;
    CliInt := QrTmpMalaDirConCliInt.Value;
    Codigo := QrTmpMalaDirConPropriet.Value;
    //
    if QrTmpMalaDirConAtivo.Value = 1 then
    begin
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO lista1 SET Tipo=1, Nome=:P0, ');
      DModG.QrUpdPID1.SQL.Add('Codigo=:P1, CliInt=:P2, Depto=:P3 ');
      DModG.QrUpdPID1.Params[00].AsString  := Nome;
      DModG.QrUpdPID1.Params[01].AsInteger := Codigo;
      DModG.QrUpdPID1.Params[02].AsInteger := CliInt;
      DModG.QrUpdPID1.Params[03].AsInteger := Depto;
      DModG.QrUpdPID1.ExecSQL;
    end;
    //
    QrTmpMalaDirCon.Next;
  end;
  FmMaladireta.ReopenEnti;
  //
  Close;
end;

procedure TFmFormulariosCond.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulariosCond.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmFormulariosCond.dmkDBGridDAC1AfterSQLExec(Sender: TObject);
begin
  ReopenTmpMalaDirCon;
end;

procedure TFmFormulariosCond.EdEmpresaChange(Sender: TObject);
begin
  MontaTmpTable(EdEmpresa.ValueVariant);
end;

procedure TFmFormulariosCond.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulariosCond.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UnDmkDAC_PF.AbreQuery(QrEmp, Dmod.MyDB);
end;

procedure TFmFormulariosCond.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulariosCond.MontaTmpTable(CliInt: Integer);
begin
  FMalaDirCon := UCriar.RecriaTempTableNovo(ntrtt_CodNom, DmodG.QrUpdPID1, True, 1, 'MalaDirCon');
  dmkDBGridDAC1.SQLTable := FMalaDirCon;
  //
  QrAptos.Close;
  QrAptos.Params[00].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(QrAptos, Dmod.MyDB);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FMalaDirCon + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Codigo=:P0, Nome=:P1, Ativo=:P2');
  while not QrAptos.Eof do
  begin
    DmodG.QrUpdPID1.Params[00].AsInteger := QrAptosConta.Value;
    DmodG.QrUpdPID1.Params[01].AsString  := QrAptosNOMEDONO.Value;
    DmodG.QrUpdPID1.Params[02].AsInteger := 0;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrAptos.Next;
  end;
  QrAptos.Close;
  ReopenTmpMalaDirCon;
end;

procedure TFmFormulariosCond.RadioGroup1Click(Sender: TObject);
begin
  //
end;

procedure TFmFormulariosCond.ReopenTmpMalaDirCon;
begin
  QrTmpMalaDirCon.Close;
  QrTmpMalaDirCon.SQL.Clear;
  QrTmpMalaDirCon.SQL.Add('SELECT tmp.Codigo, tmp.Ativo, tmp.Nome, ');
  QrTmpMalaDirCon.SQL.Add('imv.Unidade, imv.Codigo CliInt, imv.Propriet');
  QrTmpMalaDirCon.SQL.Add('FROM ' + FMalaDirCon + ' tmp');
  QrTmpMalaDirCon.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov imv ON imv.Conta = tmp.Codigo');
  QrTmpMalaDirCon.SQL.Add('ORDER BY imv.Unidade');
  UnDmkDAC_PF.AbreQuery(QrTmpMalaDirCon, Dmod.MyDB);
end;

end.
