unit MultiEtiq;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ZCF2, ResIntStrings, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UnGOTOy, UnMySQLCuringa, Grids, DBGrids,
  Menus, UMySQLModule, mySQLDbTables, UnDmkProcFunc, dmkImage, dmkGeral,
  UnDmkEnums;

type
  TFmMultiEtiq = class(TForm)
    PainelDados: TPanel;
    QrMultietq: TmySQLQuery;
    DsMultietq: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBMemo1: TDBMemo;
    Label3: TLabel;
    QrMultietqCodigo: TIntegerField;
    QrMultietqNome: TWideStringField;
    QrMultietqLinhas: TIntegerField;
    QrMultietqObserva: TWideMemoField;
    QrMultietqDataCad: TDateField;
    QrMultietqDataAlt: TDateField;
    QrMultietqUserCad: TSmallintField;
    QrMultietqUserAlt: TSmallintField;
    QrMultietqLk: TIntegerField;
    DBRadioGroup1: TDBRadioGroup;
    TbMultiEtiqIts: TmySQLTable;
    DsMultiEtiqIts: TDataSource;
    TbMultiEtiqItsCodigo: TIntegerField;
    TbMultiEtiqItsOrdem: TIntegerField;
    TbMultiEtiqItsTexto1: TWideStringField;
    TbMultiEtiqItsTexto2: TWideStringField;
    TbMultiEtiqItsTexto3: TWideStringField;
    TbMultiEtiqItsTexto4: TWideStringField;
    Label4: TLabel;
    DBGrid1: TDBGrid;
    pnUnamed: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel4: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure QrMultietqAfterScroll(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;

   //Procedures do form
    procedure CriaMultiEtqNew(Tipo: Integer);

  public
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);

    { Public declarations }
  end;

var
  FmMultiEtiq: TFmMultiEtiq;
  Cs_ControleIts, Cs_ControleIts2: Double;

implementation

uses UnMyObjects, Module, MultiEtiqNew, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMultiEtiq.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMultiEtiq.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMultiEtqCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMultiEtiq.DefParams;
begin
  VAR_GOTOTABELA := 'MultiEtq';
  VAR_GOTOmySQLTABLE := QrMultiEtq;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := 'Codigo';
  VAR_GOTONOME := '';
  VAR_GOTOVAR := 0;//1;
  //VAR_GOTOVAR1 := 'Empresa='+IntToStr(VAR_TERCEIRO);

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT * FROM multietq');
  //
  VAR_SQL1.Add('WHERE Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmMultiEtiq.CriaOForm;
begin
  //if Screen.Width > 800 then WindowState := wsMaximized;
  DefParams;
  Va(vpLast);
end;
///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmMultiEtiq.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMultiEtiq.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMultiEtiq.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMultiEtiq.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMultiEtiq.BtSaidaClick(Sender: TObject);
begin
  VAR_CONFIETIQ := QrMultiEtqCodigo.Value;
  Close;
end;

procedure TFmMultiEtiq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CriaOForm;
  UnDmkDAC_PF.AbreTable(TbMultiEtiqIts, Dmod.MyDB);
end;

procedure TFmMultiEtiq.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMultiEtqCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMultiEtiq.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmMultiEtiq.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMultiEtiq.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmMultiEtiq.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMultiEtiq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMultiEtiq.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMultiEtqCodigo.Value,
  CuringaLoc.CriaForm('Codigo', 'Descricao', 'MultiEtq', VAR_GOTOMySQLDBNAME, CO_VAZIO));
end;

procedure TFmMultiEtiq.QrMultietqAfterScroll(DataSet: TDataSet);
begin
  //
end;

procedure TFmMultiEtiq.CriaMultiEtqNew(Tipo: Integer);
var
  Codigo: Integer;
begin
  Application.CreateForm(TFmMultiEtiqNew, FmMultiEtiqNew);
  with FmMultiEtiqNew do
  begin
    if Tipo = 0 then
    begin
      ImgTipo.SQLType := stIns;
      Codigo := UMyMod.BuscaEmLivreY(DMod.MyDB, 'Livres', 'Controle', 'MultiEtq', 'MultiEtq', 'Codigo')
    end else begin
      ImgTipo.SQLType := stUpd;
      Codigo := QrMultiEtqCodigo.Value;
      EdDescricao.Text := QrMultiEtqNome.Value;
      Memo1.Text := QrMultiEtqObserva.Value;

      RGLinhas.ItemIndex := QrMultietqLinhas.Value - 1;
    end;
    EdCodigo.Text := IntToStr(Codigo);
    ShowModal;
    Destroy;
  end;
  DefParams;
  LocCod(Codigo, Codigo);
end;

procedure TFmMultiEtiq.BtIncluiClick(Sender: TObject);
begin
  CriaMultiEtqNew(0);
end;

procedure TFmMultiEtiq.BtAlteraClick(Sender: TObject);
begin
  CriaMultiEtqNew(1);
end;

procedure TFmMultiEtiq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

