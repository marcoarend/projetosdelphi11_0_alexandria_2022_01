unit FormulariosCidades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Buttons, ComCtrls, Db, (*DBTables,*) DmkDAC_PF,
  UnInternalConsts, UMySQLModule, ExtCtrls, dmkGeral, mySQLDbTables, dmkImage,
  UnDmkEnums;

type
  TFmFormulariosCidades = class(TForm)
    QrCidades: TmySQLQuery;
    DsCidades: TDataSource;
    QrCidadesCIDADE: TWideStringField;
    Panel2: TPanel;
    c: TLabel;
    CBCidade: TDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtConfirma: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmFormulariosCidades: TFmFormulariosCidades;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreate, Maladireta;

{$R *.DFM}

procedure TFmFormulariosCidades.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  BtConfirma.SetFocus;
  CBCidade.SetFocus;
end;

procedure TFmFormulariosCidades.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrCidades.SQL.Clear;
  case VAR_LOCATE001 of
    0:
    begin
      QrCidades.SQL.Add('SELECT DISTINCT');
      QrCidades.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ECidade  else en.PCidade END CIDADE');
      QrCidades.SQL.Add('FROM entidades en');
      QrCidades.SQL.Add('WHERE (CASE WHEN en.Tipo=0 THEN en.ECidade  else en.PCidade END ) <> ""');
      QrCidades.SQL.Add('ORDER BY CIDADE');
    end;
    1:
    begin
      QrCidades.SQL.Add('SELECT DISTINCT PCidade CIDADE');
      QrCidades.SQL.Add('FROM entidades');
      QrCidades.SQL.Add('WHERE PCidade <> ""');
      QrCidades.SQL.Add('ORDER BY CIDADE');
    end;
    2:
    begin
      QrCidades.SQL.Add('SELECT DISTINCT ECidade CIDADE');
      QrCidades.SQL.Add('FROM entidades');
      QrCidades.SQL.Add('WHERE ECidade <> ""');
      QrCidades.SQL.Add('ORDER BY CIDADE');
    end;
    3:
    begin
      QrCidades.SQL.Add('SELECT DISTINCT CCidade CIDADE');
      QrCidades.SQL.Add('FROM entidades');
      QrCidades.SQL.Add('WHERE CCidade <> ""');
      QrCidades.SQL.Add('ORDER BY CIDADE');
    end;
    4:
    begin
      QrCidades.SQL.Add('SELECT DISTINCT LCidade CIDADE');
      QrCidades.SQL.Add('FROM entidades');
      QrCidades.SQL.Add('WHERE LCidade <> ""');
      QrCidades.SQL.Add('ORDER BY CIDADE');
    end;
  end;
  UnDmkDAC_PF.AbreQuery(QrCidades, DMod.MyDB);
end;

procedure TFmFormulariosCidades.BtConfirmaClick(Sender: TObject);
begin
  if Trim(CBCidade.Text) = '' then
    Geral.MB_Aviso('Defina a cidade')
  else begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO lista1 SET Tipo=2, Nome='''+CBCidade.Text+'''');
    DModG.QrUpdPID1.ExecSQL;
    Geral.MB_Info('A Cidade ' + CBCidade.Text + ' foi inserida');
    FmMaladireta.ReopenCidades;
  end;
end;

procedure TFmFormulariosCidades.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulariosCidades.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

