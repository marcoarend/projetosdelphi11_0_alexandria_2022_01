unit FormulariosResult;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmFormulariosResult = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    LaEncontrados: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtAltera: TBitBtn;
    BtSair: TBitBtn;
    procedure BtSairClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmFormulariosResult: TFmFormulariosResult;

implementation

{$R *.DFM}

uses UnMyObjects, MalaDireta, Principal, ModuleGeral, UnEntities;

procedure TFmFormulariosResult.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulariosResult.BtAlteraClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(FmMalaDireta.QrMalaDireta2Codigo.Value, fmcadEntidade2, fmcadEntidade2);
  FmMalaDireta.ReopenEntidades;
end;

procedure TFmFormulariosResult.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulariosResult.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormulariosResult.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

end.
