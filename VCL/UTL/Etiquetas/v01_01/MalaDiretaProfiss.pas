unit MalaDiretaProfiss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Buttons, ComCtrls, Db, (*DBTables,*) DmkDAC_PF,
  UnInternalConsts, UMySQLModule, ExtCtrls, dmkGeral, mySQLDbTables, dmkImage,
  UnDmkEnums;

type
  TFmMalaDiretaProfiss = class(TForm)
    QrProfissoes: TmySQLQuery;
    DsProfissoes: TDataSource;
    QrProfissoesProfissao: TWideStringField;
    Panel2: TPanel;
    c: TLabel;
    CBProfiss: TDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMalaDiretaProfiss: TFmMalaDiretaProfiss;

implementation

uses UnMyObjects, Module, Maladireta, ModuleGeral;

{$R *.DFM}

procedure TFmMalaDiretaProfiss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  BtConfirma.SetFocus;
  CBProfiss.SetFocus;
end;

procedure TFmMalaDiretaProfiss.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrProfissoes.Close;
  UnDmkDAC_PF.AbreQuery(QrProfissoes, DMod.MyDB);
end;

procedure TFmMalaDiretaProfiss.BtConfirmaClick(Sender: TObject);
begin
  if Trim(CBProfiss.Text) = '' then
    Geral.MB_Aviso('Defina a profiss�o')
  else begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO lista1 SET Tipo=3, Nome='''+CBProfiss.Text+'''');
    DModG.QrUpdPID1.ExecSQL;
    Geral.MB_Aviso('A Profiss�o ' + CBProfiss.Text + ' foi inserida');
    FmMaladireta.ReopenProfissoes;
  end;
end;

procedure TFmMalaDiretaProfiss.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMalaDiretaProfiss.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
