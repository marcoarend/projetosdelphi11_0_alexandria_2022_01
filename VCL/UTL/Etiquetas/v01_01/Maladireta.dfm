object FmMaladireta: TFmMaladireta
  Left = 337
  Top = 166
  Caption = 'ETQ-MALAD-001 :: Impress'#245'es de Mala Direta'
  ClientHeight = 735
  ClientWidth = 929
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 929
    Height = 564
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 660
      Top = 0
      Width = 269
      Height = 564
      Align = alClient
      TabOrder = 0
      object PainelProfissoes: TPanel
        Left = 1
        Top = 419
        Width = 267
        Height = 144
        Align = alBottom
        Caption = 'Panel1'
        TabOrder = 0
        object Panel3: TPanel
          Left = 1
          Top = 95
          Width = 265
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object BtInclui2: TBitBtn
            Tag = 10
            Left = 10
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtInclui2Click
          end
          object BtExclui2: TBitBtn
            Tag = 12
            Left = 162
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui banco atual'
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtExclui2Click
          end
        end
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 265
          Height = 94
          Align = alClient
          DataSource = DsProfiss
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Profiss'#245'es'
              Width = 226
              Visible = True
            end>
        end
      end
      object PainelEntidades: TPanel
        Left = 1
        Top = 1
        Width = 267
        Height = 144
        Align = alTop
        Caption = 'Panel1'
        TabOrder = 1
        object Panel5: TPanel
          Left = 1
          Top = 95
          Width = 265
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object BtInclui: TBitBtn
            Tag = 10
            Left = 10
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
          object BtExclui: TBitBtn
            Tag = 12
            Left = 162
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui banco atual'
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtExcluiClick
          end
        end
        object DBGrid2: TDBGrid
          Left = 1
          Top = 1
          Width = 265
          Height = 94
          Align = alClient
          DataSource = DsEnti
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Entidades'
              Width = 226
              Visible = True
            end>
        end
      end
      object PainelCidades: TPanel
        Left = 1
        Top = 145
        Width = 267
        Height = 274
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 2
        object Panel7: TPanel
          Left = 1
          Top = 225
          Width = 265
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object BtInclui1: TBitBtn
            Tag = 10
            Left = 10
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtInclui1Click
          end
          object BtExclui1: TBitBtn
            Tag = 12
            Left = 162
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Exclui banco atual'
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtExclui1Click
          end
        end
        object DBGrid3: TDBGrid
          Left = 1
          Top = 1
          Width = 265
          Height = 224
          Align = alClient
          DataSource = DsCidades
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Cidades'
              Width = 226
              Visible = True
            end>
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 660
      Height = 564
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 660
        Height = 329
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object CkVazios: TCheckBox
          Left = 540
          Top = 260
          Width = 105
          Height = 17
          Caption = 'Sem nomes nulos.'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = CkVaziosClick
        end
        object CkGrade: TCheckBox
          Left = 540
          Top = 276
          Width = 61
          Height = 17
          Caption = 'Grade.'
          TabOrder = 3
        end
        object PCEntidade: TPageControl
          Left = 0
          Top = 0
          Width = 660
          Height = 205
          ActivePage = TabSheet4
          Align = alTop
          TabOrder = 0
          object TabSheet3: TTabSheet
            Caption = ' V'#225'rias entidades'
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 652
              Height = 177
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 136
                Width = 17
                Height = 13
                Caption = 'UF:'
              end
              object Label9: TLabel
                Left = 56
                Top = 136
                Width = 53
                Height = 13
                Caption = 'CEP inicial:'
              end
              object Label10: TLabel
                Left = 120
                Top = 136
                Width = 46
                Height = 13
                Caption = 'CEP final:'
              end
              object Label2: TLabel
                Left = 184
                Top = 136
                Width = 68
                Height = 13
                Caption = 'Espa'#231'amento:'
              end
              object Label6: TLabel
                Left = 260
                Top = 136
                Width = 68
                Height = 13
                Caption = 'Coluna inicial: '
              end
              object Label7: TLabel
                Left = 336
                Top = 136
                Width = 58
                Height = 13
                Caption = 'Linha inicial:'
              end
              object LaRepeticoes: TLabel
                Left = 412
                Top = 136
                Width = 57
                Height = 13
                Caption = 'Repeti'#231#245'es:'
              end
              object RGOrdem: TRadioGroup
                Left = 7
                Top = 4
                Width = 645
                Height = 35
                Caption = ' Ordem: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  'UF, Cidade e CEP'
                  'Cidade e CEP'
                  'CEP'
                  'Nome')
                TabOrder = 0
                OnClick = RGOrdemClick
              end
              object RGEndereco: TRadioGroup
                Left = 7
                Top = 40
                Width = 645
                Height = 35
                Caption = ' Endere'#231'o cadastrado: '
                Columns = 5
                ItemIndex = 0
                Items.Strings = (
                  'Entidade'
                  'Pessoal'
                  'Empresa'
                  'Cobran'#231'a'
                  'Entrega')
                TabOrder = 1
                OnClick = RGEnderecoClick
              end
              object GroupBox2: TGroupBox
                Left = 7
                Top = 76
                Width = 645
                Height = 57
                Caption = ' Tipo de cadastro: '
                TabOrder = 2
                object CkCliente1: TCheckBox
                  Left = 8
                  Top = 16
                  Width = 100
                  Height = 17
                  Caption = 'C1'
                  TabOrder = 0
                  OnClick = CkCliente1Click
                end
                object CkFornece1: TCheckBox
                  Left = 223
                  Top = 16
                  Width = 100
                  Height = 17
                  Caption = 'F1'
                  TabOrder = 2
                  OnClick = CkFornece1Click
                end
                object CkFornece2: TCheckBox
                  Left = 223
                  Top = 36
                  Width = 100
                  Height = 17
                  Caption = 'F2'
                  TabOrder = 3
                  Visible = False
                  OnClick = CkFornece2Click
                end
                object CkFornece3: TCheckBox
                  Left = 330
                  Top = 16
                  Width = 100
                  Height = 17
                  Caption = 'F3'
                  TabOrder = 4
                  Visible = False
                  OnClick = CkFornece3Click
                end
                object CkFornece4: TCheckBox
                  Left = 330
                  Top = 36
                  Width = 100
                  Height = 17
                  Caption = 'F4'
                  TabOrder = 5
                  Visible = False
                  OnClick = CkFornece4Click
                end
                object CkTerceir1: TCheckBox
                  Left = 545
                  Top = 16
                  Width = 96
                  Height = 17
                  Caption = 'T1'
                  TabOrder = 6
                  OnClick = CkTerceir1Click
                end
                object CkCliente2: TCheckBox
                  Left = 8
                  Top = 36
                  Width = 100
                  Height = 17
                  Caption = 'C2'
                  TabOrder = 1
                  Visible = False
                  OnClick = CkCliente2Click
                end
                object CkCliente3: TCheckBox
                  Left = 116
                  Top = 16
                  Width = 100
                  Height = 17
                  Caption = 'C3'
                  TabOrder = 7
                  OnClick = CkCliente1Click
                end
                object CkCliente4: TCheckBox
                  Left = 116
                  Top = 36
                  Width = 100
                  Height = 17
                  Caption = 'C4'
                  TabOrder = 8
                  Visible = False
                  OnClick = CkCliente2Click
                end
                object CkFornece5: TCheckBox
                  Left = 438
                  Top = 16
                  Width = 100
                  Height = 17
                  Caption = 'F5'
                  TabOrder = 9
                  Visible = False
                  OnClick = CkFornece3Click
                end
                object CkFornece6: TCheckBox
                  Left = 438
                  Top = 36
                  Width = 100
                  Height = 17
                  Caption = 'F6'
                  TabOrder = 10
                  Visible = False
                  OnClick = CkFornece4Click
                end
              end
              object CBUF: TDBLookupComboBox
                Left = 8
                Top = 152
                Width = 45
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsUFs
                TabOrder = 3
                OnClick = CBUFClick
                OnKeyDown = CBUFKeyDown
              end
              object EdCEPIni: TdmkEdit
                Left = 56
                Top = 152
                Width = 61
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCEPIniChange
                OnExit = EdCEPIniExit
              end
              object EDCepFim: TdmkEdit
                Left = 120
                Top = 152
                Width = 61
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EDCepFimChange
                OnExit = EDCepFimExit
              end
              object EdEspacamento: TdmkEdit
                Left = 184
                Top = 152
                Width = 73
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnExit = EdEspacamentoExit
              end
              object EdColIni: TdmkEdit
                Left = 260
                Top = 152
                Width = 73
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnExit = EdColIniExit
              end
              object EdLinIni: TdmkEdit
                Left = 336
                Top = 152
                Width = 73
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnExit = EdLinIniExit
              end
              object EdRepeticoes: TdmkEdit
                Left = 412
                Top = 152
                Width = 73
                Height = 21
                Alignment = taRightJustify
                TabOrder = 9
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1
                ValWarn = False
                OnExit = EdRepeticoesExit
              end
              object CkFamilias: TCheckBox
                Left = 489
                Top = 156
                Width = 161
                Height = 17
                Caption = 'Somente chefes de fam'#237'lia.'
                TabOrder = 10
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Entidade '#250'nica '
            ImageIndex = 1
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 652
              Height = 177
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label5: TLabel
                Left = 4
                Top = 4
                Width = 74
                Height = 13
                Caption = 'Entidade '#250'nica:'
              end
              object EdEntidade: TdmkEditCB
                Left = 4
                Top = 19
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEntidade
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEntidade: TdmkDBLookupComboBox
                Left = 72
                Top = 19
                Width = 473
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOMEENTIDADE'
                ListSource = DsEntidades
                TabOrder = 1
                dmkEditCB = EdEntidade
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object BtEntidades: TBitBtn
                Tag = 149
                Left = 552
                Top = 0
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo banco'
                Caption = 'Entidades'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtEntidadesClick
              end
            end
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 205
          Width = 660
          Height = 124
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 131
            Height = 13
            Caption = 'Configura'#231#227'o de impress'#227'o:'
          end
          object EdConfImp: TdmkEditCB
            Left = 4
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBConfImp
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBConfImp: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 477
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPage
            TabOrder = 1
            dmkEditCB = EdConfImp
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object RGImpressao: TRadioGroup
            Left = 3
            Top = 44
            Width = 486
            Height = 35
            Caption = ' A'#231#227'o ap'#243's gera'#231#227'o: '
            Columns = 4
            ItemIndex = 1
            Items.Strings = (
              'Imprime mala direta'
              'Preview mala direta'
              'Preview relat'#243'rio'
              'Editar dados')
            TabOrder = 2
          end
          object BtImpConfig1: TBitBtn
            Tag = 256
            Left = 556
            Top = 2
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Caption = 'Config.'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtImpConfig1Click
          end
          object RGTipoImp: TRadioGroup
            Left = 4
            Top = 84
            Width = 305
            Height = 35
            Caption = ' Tipo de impress'#227'o:'
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Windows'
              'DOS Normal'
              'DOS Comprimido')
            TabOrder = 4
          end
          object RadioGroup1: TRadioGroup
            Left = 312
            Top = 84
            Width = 225
            Height = 35
            Caption = ' Situa'#231#227'o do Cadastro: '
            TabOrder = 5
          end
          object CkAtivo: TCheckBox
            Left = 324
            Top = 98
            Width = 68
            Height = 17
            Caption = 'Ativo'
            Checked = True
            State = cbChecked
            TabOrder = 6
          end
          object CkStand: TCheckBox
            Left = 396
            Top = 98
            Width = 68
            Height = 17
            Caption = 'Stand by'
            TabOrder = 7
          end
          object CkInati: TCheckBox
            Left = 468
            Top = 98
            Width = 68
            Height = 17
            Caption = 'Inativo'
            TabOrder = 8
          end
          object CkInfoTel: TCheckBox
            Left = 548
            Top = 96
            Width = 101
            Height = 17
            Caption = 'Informa telefone.'
            TabOrder = 9
          end
          object CkInfoCod: TCheckBox
            Left = 496
            Top = 56
            Width = 153
            Height = 17
            Caption = 'Informa c'#243'digo de cadastro.'
            Checked = True
            State = cbChecked
            TabOrder = 10
          end
        end
      end
      object PCConfig: TPageControl
        Left = 0
        Top = 329
        Width = 660
        Height = 235
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = ' Endere'#231'o '
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 652
            Height = 207
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Minha Etiqueta '
          ImageIndex = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 652
            Height = 207
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label3: TLabel
              Left = 4
              Top = 4
              Width = 122
              Height = 13
              Caption = 'Configura'#231#227'o da etiqueta:'
            end
            object EdMinhaEtiq: TdmkEditCB
              Left = 4
              Top = 20
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMinhaEtiq
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMinhaEtiq: TdmkDBLookupComboBox
              Left = 72
              Top = 20
              Width = 473
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMultiEtq
              TabOrder = 1
              dmkEditCB = EdMinhaEtiq
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object BtImpConfig2: TBitBtn
              Tag = 256
              Left = 547
              Top = 2
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Inclui novo banco'
              Caption = 'Config.'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtImpConfig2Click
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = ' Meu Cart'#227'o '
          ImageIndex = 2
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 652
            Height = 207
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object PageControl1: TPageControl
              Left = 0
              Top = 0
              Width = 652
              Height = 207
              Align = alClient
              TabOrder = 0
            end
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 652
              Height = 207
              Align = alClient
              ParentBackground = False
              TabOrder = 1
              object Panel16: TPanel
                Left = 1
                Top = 1
                Width = 650
                Height = 48
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label8: TLabel
                  Left = 4
                  Top = 4
                  Width = 137
                  Height = 13
                  Caption = 'Configura'#231#227'o do meu cart'#227'o:'
                end
                object EdCustomFR3: TdmkEditCB
                  Left = 4
                  Top = 20
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBCustomFR3
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBCustomFR3: TdmkDBLookupComboBox
                  Left = 72
                  Top = 20
                  Width = 473
                  Height = 21
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  ListSource = DsCustomFR3
                  TabOrder = 1
                  dmkEditCB = EdCustomFR3
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
                object BtCustomFR3: TBitBtn
                  Tag = 256
                  Left = 547
                  Top = 2
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Inclui novo banco'
                  Caption = 'Config.'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtCustomFR3Click
                end
              end
              object PCCartao: TPageControl
                Left = 1
                Top = 49
                Width = 650
                Height = 157
                ActivePage = TabSheet6
                Align = alClient
                TabOrder = 1
                object TabSheet6: TTabSheet
                  Caption = ' Com quadro definido '
                  object Panel17: TPanel
                    Left = 0
                    Top = 0
                    Width = 642
                    Height = 129
                    Align = alClient
                    BevelOuter = bvNone
                    Caption = 
                      'Ser'#225' usado o nome da c'#233'lula do cadastro de quadros: ENT-GEREN-01' +
                      '5 :: Quadros de Entidades'
                    ParentBackground = False
                    TabOrder = 0
                  end
                end
                object TabSheet7: TTabSheet
                  Caption = ' Sem defini'#231#227'o de quadros (definir quadro)'
                  ImageIndex = 1
                  object Panel15: TPanel
                    Left = 0
                    Top = 0
                    Width = 642
                    Height = 129
                    Align = alClient
                    ParentBackground = False
                    TabOrder = 0
                    object RGQuadro: TGroupBox
                      Left = 4
                      Top = 4
                      Width = 633
                      Height = 61
                      Caption = ' Quadro: Coluna: [COLUNA] Linha: [LINHA]'
                      TabOrder = 0
                      object Label11: TLabel
                        Left = 8
                        Top = 20
                        Width = 73
                        Height = 13
                        Caption = 'Coluna m'#237'nima:'
                      end
                      object Label12: TLabel
                        Left = 8
                        Top = 40
                        Width = 74
                        Height = 13
                        Caption = 'Coluna m'#225'xima:'
                      end
                      object Label13: TLabel
                        Left = 140
                        Top = 40
                        Width = 67
                        Height = 13
                        Caption = 'Linha m'#225'xima:'
                      end
                      object Label14: TLabel
                        Left = 140
                        Top = 20
                        Width = 66
                        Height = 13
                        Caption = 'Linha m'#237'nima:'
                      end
                      object Label15: TLabel
                        Left = 268
                        Top = 40
                        Width = 58
                        Height = 13
                        Caption = 'Linha inicial:'
                      end
                      object Label16: TLabel
                        Left = 268
                        Top = 20
                        Width = 66
                        Height = 13
                        Caption = 'Coluna Inicial:'
                      end
                      object EdColMin: TdmkEdit
                        Left = 84
                        Top = 16
                        Width = 48
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '1'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1
                        ValWarn = False
                      end
                      object EdColMax: TdmkEdit
                        Left = 84
                        Top = 36
                        Width = 48
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '1'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1
                        ValWarn = False
                      end
                      object EdLinMin: TdmkEdit
                        Left = 212
                        Top = 16
                        Width = 48
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '1'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1
                        ValWarn = False
                      end
                      object EdLinMax: TdmkEdit
                        Left = 212
                        Top = 36
                        Width = 48
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 3
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '1'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1
                        ValWarn = False
                      end
                      object EdColI: TdmkEdit
                        Left = 340
                        Top = 16
                        Width = 48
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 4
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '1'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1
                        ValWarn = False
                      end
                      object EdLinI: TdmkEdit
                        Left = 340
                        Top = 36
                        Width = 48
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 5
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '1'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '1'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 1
                        ValWarn = False
                      end
                      object CkColLetras: TCheckBox
                        Left = 392
                        Top = 16
                        Width = 141
                        Height = 17
                        Caption = 'Usar letras para colunas'
                        Checked = True
                        State = cbChecked
                        TabOrder = 6
                      end
                      object CkLinLetras: TCheckBox
                        Left = 392
                        Top = 40
                        Width = 141
                        Height = 17
                        Caption = 'Usar letras para linhas'
                        TabOrder = 7
                      end
                      object RGIncremento: TRadioGroup
                        Left = 532
                        Top = 8
                        Width = 93
                        Height = 49
                        Caption = ' Incremento: '
                        ItemIndex = 0
                        Items.Strings = (
                          'Linha'
                          'Coluna')
                        TabOrder = 8
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 929
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 881
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 833
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 327
        Height = 32
        Caption = 'Impress'#245'es de Mala Direta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 327
        Height = 32
        Caption = 'Impress'#245'es de Mala Direta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 327
        Height = 32
        Caption = 'Impress'#245'es de Mala Direta'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 612
    Width = 929
    Height = 53
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel18: TPanel
      Left = 2
      Top = 15
      Width = 925
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Progress: TProgressBar
        Left = 0
        Top = 20
        Width = 925
        Height = 16
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 665
    Width = 929
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel19: TPanel
      Left = 2
      Top = 15
      Width = 925
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 781
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSair: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSairClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtASCII: TBitBtn
        Tag = 155
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = 'C'#243'digo DOS'
        TabOrder = 2
        OnClick = BtASCIIClick
      end
      object CkDesign: TCheckBox
        Left = 260
        Top = 20
        Width = 97
        Height = 17
        Caption = 'Design mode.'
        TabOrder = 3
      end
      object BtGrade: TBitBtn
        Tag = 255
        Left = 368
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Visualiza'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtGradeClick
      end
    end
  end
  object QrUFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ufs'
      'ORDER BY Nome')
    Left = 8
    Top = 4
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrUFsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUFsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUFsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUFsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUFsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUFsICMS_C: TFloatField
      FieldName = 'ICMS_C'
    end
    object QrUFsICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 36
    Top = 4
  end
  object DsCidades: TDataSource
    DataSet = QrCidades
    Left = 812
    Top = 236
  end
  object QrMalaDireta2: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMalaDireta2AfterOpen
    AfterClose = QrMalaDireta2AfterClose
    OnCalcFields = QrMalaDireta2CalcFields
    SQL.Strings = (
      'SELECT'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP    else en.PCEP    END + 0.000 ' +
        'CEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.EUF     else en.PUF     END + 0.000 ' +
        'UF,'
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero else en.PNumero END + 0.000 ' +
        'NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial       else en.Nome      ' +
        ' END NOMEENTIDADE,'
      'CASE WHEN en.Tipo=0 THEN en.ERua    else en.PRua    END Rua,'
      'CASE WHEN en.Tipo=0 THEN en.ECompl  else en.PCompl  END Compl,'
      'CASE WHEN en.Tipo=0 THEN en.EBairro else en.PBairro END Bairro,'
      'CASE WHEN en.Tipo=0 THEN en.ECidade else en.PCidade END Cidade,'
      'CASE WHEN en.Tipo=0 THEN en.EPais   else en.PPais   END Pais,'
      'CASE WHEN en.Tipo=0 THEN en.ETe1     else en.PTe1     END Te1,'
      'CASE WHEN en.Tipo=0 THEN en.ECel    else en.PCel    END Cel,'
      'uf.Nome NOMEUF, en.Codigo , eq.Celula'
      'FROM entidades en, UFs uf , entiquadro eq'
      'WHERE uf.Codigo=(CASE WHEN en.Tipo=0 '
      'THEN en.EUF else en.PUF END)  AND eq.EntiCod=en.Codigo '
      'AND en.Codigo = 1790'
      
        'ORDER BY NOMEUF, CIDADE, IF(en.Tipo=0, en.ECEP, en.PCEP), NOMEEN' +
        'TIDADE'
      '')
    Left = 696
    Top = 4
    object QrMalaDireta2CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrMalaDireta2Codigo_NOMEENTIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Codigo_NOMEENTIDADE'
      Size = 120
      Calculated = True
    end
    object QrMalaDireta2Rua_Numero_Compl: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Rua_Numero_Compl'
      Size = 300
      Calculated = True
    end
    object QrMalaDireta2NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Calculated = True
    end
    object QrMalaDireta2CIDADE_UF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CIDADE_UF'
      Size = 100
      Calculated = True
    end
    object QrMalaDireta2CEP_PAIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_PAIS'
      Size = 100
      Calculated = True
    end
    object QrMalaDireta2NOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrMalaDireta2Rua: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Rua'
      Size = 30
    end
    object QrMalaDireta2Compl: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Compl'
      Size = 30
    end
    object QrMalaDireta2Bairro: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Bairro'
      Size = 30
    end
    object QrMalaDireta2Cidade: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Cidade'
      Size = 25
    end
    object QrMalaDireta2Pais: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Pais'
    end
    object QrMalaDireta2Te1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Te1'
    end
    object QrMalaDireta2Cel: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Cel'
    end
    object QrMalaDireta2NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrMalaDireta2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMalaDireta2Te1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 50
      Calculated = True
    end
    object QrMalaDireta2Celula: TWideStringField
      FieldName = 'Celula'
      Size = 50
    end
    object QrMalaDireta2CEP: TFloatField
      FieldName = 'CEP'
    end
    object QrMalaDireta2UF: TFloatField
      FieldName = 'UF'
    end
    object QrMalaDireta2NUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrPage: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pages')
    Left = 332
    Top = 228
    object QrPageNome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrPageObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrPageCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPagePagCol: TIntegerField
      FieldName = 'PagCol'
    end
    object QrPagePagLin: TIntegerField
      FieldName = 'PagLin'
    end
    object QrPagePagLef: TIntegerField
      FieldName = 'PagLef'
    end
    object QrPagePagTop: TIntegerField
      FieldName = 'PagTop'
    end
    object QrPagePagWid: TIntegerField
      FieldName = 'PagWid'
    end
    object QrPagePagHei: TIntegerField
      FieldName = 'PagHei'
    end
    object QrPagePagGap: TIntegerField
      FieldName = 'PagGap'
    end
    object QrPageBanCol: TIntegerField
      FieldName = 'BanCol'
    end
    object QrPageBanLin: TIntegerField
      FieldName = 'BanLin'
    end
    object QrPageBanWid: TIntegerField
      FieldName = 'BanWid'
    end
    object QrPageBanHei: TIntegerField
      FieldName = 'BanHei'
    end
    object QrPageBanLef: TIntegerField
      FieldName = 'BanLef'
    end
    object QrPageBanTop: TIntegerField
      FieldName = 'BanTop'
    end
    object QrPageBanGap: TIntegerField
      FieldName = 'BanGap'
    end
    object QrPageOrient: TIntegerField
      FieldName = 'Orient'
    end
    object QrPageDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPageDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPageUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrPageUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrPageLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPageColWid: TIntegerField
      FieldName = 'ColWid'
    end
    object QrPageBa2Hei: TIntegerField
      FieldName = 'Ba2Hei'
    end
    object QrPageFoSize: TSmallintField
      FieldName = 'FoSize'
    end
    object QrPageMemHei: TIntegerField
      FieldName = 'MemHei'
    end
    object QrPageEspaco: TSmallintField
      FieldName = 'Espaco'
    end
  end
  object DsPage: TDataSource
    DataSet = QrPage
    Left = 360
    Top = 228
  end
  object QrMinhaEtiq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMalaDireta2AfterOpen
    AfterClose = QrMalaDireta2AfterClose
    OnCalcFields = QrMalaDireta2CalcFields
    SQL.Strings = (
      'SELECT * FROM minhaetiq'
      'ORDER BY Nome, Conta')
    Left = 696
    Top = 32
    object QrMinhaEtiqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMLOCAL.minhaetiq.Nome'
      Size = 128
    end
    object QrMinhaEtiqConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMLOCAL.minhaetiq.Conta'
    end
    object QrMinhaEtiqTexto1: TWideStringField
      FieldName = 'Texto1'
      Origin = 'DBMLOCAL.minhaetiq.Texto1'
      Size = 128
    end
    object QrMinhaEtiqTexto2: TWideStringField
      FieldName = 'Texto2'
      Origin = 'DBMLOCAL.minhaetiq.Texto2'
      Size = 128
    end
    object QrMinhaEtiqTexto3: TWideStringField
      FieldName = 'Texto3'
      Origin = 'DBMLOCAL.minhaetiq.Texto3'
      Size = 128
    end
    object QrMinhaEtiqTexto4: TWideStringField
      FieldName = 'Texto4'
      Origin = 'DBMLOCAL.minhaetiq.Texto4'
      Size = 128
    end
  end
  object QrMultiEtq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM multietq'
      'ORDER BY Nome')
    Left = 264
    Top = 340
    object QrMultiEtqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.multietq.Codigo'
    end
    object QrMultiEtqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.multietq.Nome'
      Size = 128
    end
    object QrMultiEtqLinhas: TIntegerField
      FieldName = 'Linhas'
      Origin = 'DBMMONEY.multietq.Linhas'
    end
    object QrMultiEtqObserva: TWideMemoField
      FieldName = 'Observa'
      Origin = 'DBMMONEY.multietq.Observa'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrMultiEtqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'DBMMONEY.multietq.DataCad'
    end
    object QrMultiEtqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'DBMMONEY.multietq.DataAlt'
    end
    object QrMultiEtqUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'DBMMONEY.multietq.UserCad'
    end
    object QrMultiEtqUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'DBMMONEY.multietq.UserAlt'
    end
    object QrMultiEtqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.multietq.Lk'
    end
  end
  object DsMultiEtq: TDataSource
    DataSet = QrMultiEtq
    Left = 292
    Top = 340
  end
  object QrMinhaEtqIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM multietqits'
      'WHERE Codigo=:P0'
      'ORDER BY Ordem')
    Left = 328
    Top = 396
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMinhaEtqItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.multietqits.Codigo'
    end
    object QrMinhaEtqItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'DBMMONEY.multietqits.Ordem'
    end
    object QrMinhaEtqItsTexto1: TWideStringField
      FieldName = 'Texto1'
      Origin = 'DBMMONEY.multietqits.Texto1'
      Size = 128
    end
    object QrMinhaEtqItsTexto2: TWideStringField
      FieldName = 'Texto2'
      Origin = 'DBMMONEY.multietqits.Texto2'
      Size = 128
    end
    object QrMinhaEtqItsTexto3: TWideStringField
      FieldName = 'Texto3'
      Origin = 'DBMMONEY.multietqits.Texto3'
      Size = 128
    end
    object QrMinhaEtqItsTexto4: TWideStringField
      FieldName = 'Texto4'
      Origin = 'DBMMONEY.multietqits.Texto4'
      Size = 128
    end
  end
  object DsMinhaEtqIts: TDataSource
    DataSet = QrMinhaEtqIts
    Left = 356
    Top = 396
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTIDADE,'
      'ent.Codigo'
      'FROM entidades ent'
      'ORDER BY NOMEENTIDADE')
    Left = 240
    Top = 444
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 49
    end
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 268
    Top = 444
  end
  object QrEnderecos: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMalaDireta2AfterOpen
    AfterClose = QrMalaDireta2AfterClose
    OnCalcFields = QrMalaDireta2CalcFields
    SQL.Strings = (
      'SELECT * FROM enderecos'
      'ORDER BY Linha1, Conta')
    Left = 696
    Top = 60
    object QrEnderecosConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DBMLOCAL.enderecos.Conta'
    end
    object QrEnderecosLinha1: TWideStringField
      FieldName = 'Linha1'
      Origin = 'DBMLOCAL.enderecos.Linha1'
      Size = 48
    end
    object QrEnderecosLinha2: TWideStringField
      FieldName = 'Linha2'
      Origin = 'DBMLOCAL.enderecos.Linha2'
      Size = 33
    end
    object QrEnderecosLinha3: TWideStringField
      FieldName = 'Linha3'
      Origin = 'DBMLOCAL.enderecos.Linha3'
      Size = 41
    end
    object QrEnderecosLinha4: TWideStringField
      FieldName = 'Linha4'
      Origin = 'DBMLOCAL.enderecos.Linha4'
      Size = 13
    end
    object QrEnderecosLinha5: TWideStringField
      FieldName = 'Linha5'
      Size = 255
    end
  end
  object DsProfiss: TDataSource
    DataSet = QrProfiss
    Left = 812
    Top = 392
  end
  object DsEnti: TDataSource
    DataSet = QrEnti
    Left = 824
    Top = 80
  end
  object QrEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lista1'
      'WHERE Tipo=1')
    Left = 797
    Top = 80
    object QrEntiNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEntiTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrEntiCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEntiDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEnti2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lista1'
      'WHERE Tipo=1')
    Left = 797
    Top = 108
    object QrEnti2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrEnti2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrEnti2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnti2Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEnti2CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrCidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lista1'
      'WHERE Tipo=2')
    Left = 785
    Top = 236
    object QrCidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCidadesTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrCidades2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lista1'
      'WHERE Tipo=2')
    Left = 785
    Top = 264
    object QrCidades2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCidades2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrProfiss: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lista1'
      'WHERE Tipo=3')
    Left = 785
    Top = 392
    object QrProfissNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrProfissTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrProfiss2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lista1'
      'WHERE Tipo=3')
    Left = 785
    Top = 420
    object QrProfiss2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrProfiss2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object frxMalaDireta2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.345732233800000000
    ReportOptions.LastChange = 39721.345732233800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxMalaDireta2GetValue
    Left = 724
    Top = 4
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMalaDireta2
        DataSetName = 'frxDsMalaDireta2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 853.779530000000000000
          Top = 1.984230000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 313.700990000000000000
        Width = 1046.929810000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 773.779530000000000000
          Top = 5.102039999999988000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.000000000000000000
        Top = 238.110390000000000000
        Width = 1046.929810000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMalaDireta2
        DataSetName = 'frxDsMalaDireta2'
        RowCount = 0
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 2.440940000000000000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo14OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 46.440940000000000000
          Width = 232.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMalaDireta2."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 278.440940000000000000
          Width = 180.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo4OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Rua"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 458.440940000000000000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo5OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###;#,###;S/N'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Numero"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 502.440940000000000000
          Width = 80.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Compl"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 582.440940000000000000
          Width = 148.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Bairro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 730.440940000000000000
          Width = 128.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Cidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 858.440940000000000000
          Width = 60.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMalaDireta2."CEP_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 918.440940000000000000
          Width = 28.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMalaDireta2."NOMEUF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 946.440940000000000000
          Width = 76.000000000000000000
          Height = 14.000000000000000000
          OnBeforePrint = 'Memo19OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMalaDireta2."Pais"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 70.000000000000000000
        Top = 64.252010000000000000
        Width = 1046.929810000000000000
        object Picture2: TfrxPictureView
          AllowVectorExport = True
          Left = 6.220470000000000000
          Top = 0.629869999999996800
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 182.220470000000000000
          Top = 40.629870000000000000
          Width = 500.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 182.220470000000000000
          Top = 0.629869999999996800
          Width = 408.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Lista de Endere'#231'os Pesquisados')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 674.220470000000000000
          Top = 0.629869999999996800
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Ordenado por: ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 754.220470000000000000
          Top = 0.629869999999996800
          Width = 272.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[ORDEM]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.700680000000000000
        Top = 158.740260000000000000
        Width = 1046.929810000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 2.440940000000000000
          Top = 1.700680000000006000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 46.440940000000000000
          Top = 1.700680000000006000
          Width = 232.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 278.440940000000000000
          Top = 1.700680000000006000
          Width = 180.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Logradouro')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 458.440940000000000000
          Top = 1.700680000000006000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 502.440940000000000000
          Top = 1.700680000000006000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Complemento')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 582.440940000000000000
          Top = 1.700680000000006000
          Width = 148.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Bairro')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 730.440940000000000000
          Top = 1.700680000000006000
          Width = 128.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 858.440940000000000000
          Top = 1.700680000000006000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 918.440940000000000000
          Top = 1.700680000000006000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 946.440940000000000000
          Top = 1.700680000000006000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pa'#237's')
          ParentFont = False
        end
      end
    end
  end
  object frxDsMalaDireta2: TfrxDBDataset
    UserName = 'frxDsMalaDireta2'
    CloseDataSource = False
    DataSet = QrMalaDireta2
    BCDToCurrency = False
    
    Left = 752
    Top = 4
  end
  object frxDsMinhaEtiq: TfrxDBDataset
    UserName = 'frxDsMinhaEtiq'
    CloseDataSource = False
    DataSet = QrMinhaEtiq
    BCDToCurrency = False
    
    Left = 752
    Top = 32
  end
  object frxDsEnderecos: TfrxDBDataset
    UserName = 'frxDsEnderecos'
    CloseDataSource = False
    DataSet = QrEnderecos
    BCDToCurrency = False
    
    Left = 752
    Top = 60
  end
  object frxEnderecos: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.512178449100000000
    ReportOptions.LastChange = 39721.512178449100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 724
    Top = 60
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEnderecos
        DataSetName = 'frxDsEnderecos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object frxDiversos: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.525010486100000000
    ReportOptions.LastChange = 39721.525010486100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxDiversosGetValue
    Left = 724
    Top = 32
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMinhaEtiq
        DataSetName = 'frxDsMinhaEtiq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object DsMalaDireta2: TDataSource
    DataSet = QrMalaDireta2
    Left = 668
    Top = 4
  end
  object QrCustomFR3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM customfr3'
      'ORDER BY Nome')
    Left = 408
    Top = 436
    object QrCustomFR3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCustomFR3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCustomFR3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCustomFR3Report: TWideMemoField
      FieldName = 'Report'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsCustomFR3: TDataSource
    DataSet = QrCustomFR3
    Left = 436
    Top = 436
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39025.537419340290000000
    ReportOptions.LastChange = 39025.537419340290000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReport1GetValue
    Left = 724
    Top = 88
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMalaDireta2
        DataSetName = 'frxDsMalaDireta2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object PMIncluiEntidade: TPopupMenu
    Left = 768
    Top = 144
    object Entidades1: TMenuItem
      Caption = '&Entidades'
      OnClick = Entidades1Click
    end
    object Condminos1: TMenuItem
      Caption = '&Cond'#244'minos'
      OnClick = Condminos1Click
    end
  end
end
