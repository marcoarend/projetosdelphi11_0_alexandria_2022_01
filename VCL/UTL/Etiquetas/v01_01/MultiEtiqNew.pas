unit MultiEtiqNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UMySQLModule, Mask, dmkGeral, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmMultiEtiqNew = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    Label2: TLabel;
    EdDescricao: TEdit;
    Memo1: TMemo;
    Label3: TLabel;
    RGLinhas: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMultiEtiqNew: TFmMultiEtiqNew;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmMultiEtiqNew.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  UMyMod.PoeEmLivreY(DMod.MyDB, 'Livres', 'MultiEtq', Geral.IMV(EdCodigo.Text));
  Close;
end;

procedure TFmMultiEtiqNew.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := StrToInt(EdCodigo.Text);
  if Trim(EdDescricao.Text) = '' then begin
    ShowMessage('Informe uma descriação!');
    EdDescricao.SetFocus;
    Exit;
  end;
  Dmod.QrUpdM.SQL.Clear;
  if ImgTipo.SQLType = stIns then begin
    Dmod.QrUpdM.SQL.Add('INSERT INTO multietq SET ');
    Dmod.QrUpdM.SQL.Add('Nome=:P0, Observa=:P1, Linhas=:P2,');
    Dmod.QrUpdM.SQL.Add('');
    Dmod.QrUpdM.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz ');
  end else begin
    Dmod.QrUpdM.SQL.Add('UPDATE multietq SET ');
    Dmod.QrUpdM.SQL.Add('Nome=:P0, Observa=:P1, Linhas=:P2,');
    Dmod.QrUpdM.SQL.Add('');
    Dmod.QrUpdM.SQL.Add('DataCad=:Px, UserCad=:Py WHERE Codigo=:Pz ');
  end;
  Dmod.QrUpdM.Params[00].AsString := EdDescricao.Text;
  Dmod.QrUpdM.Params[01].AsString := Memo1.Text;

  Dmod.QrUpdM.Params[02].AsInteger := RGLinhas.ItemIndex + 1;

  Dmod.QrUpdM.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdM.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdM.Params[05].AsInteger := Codigo;
  Dmod.QrUpdM.ExecSQL;
  Close;
end;

procedure TFmMultiEtiqNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmMultiEtiqNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
