unit Maladireta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) DBCtrls, ComCtrls,
  UnInternalConsts, mySQLDbTables, Grids, DBGrids, printers, UCreate, UnGOTOy,
  frxClass, frxDBSet, Variants, frxBarcod, frxBarcode, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, dmkGeral, Menus, dmkImage, UnDmkProcFunc, DmkDAC_PF,
  UnDmkEnums, UnProjGroup_Consts;

type
  TFmMaladireta = class(TForm)
    PainelDados: TPanel;
    QrUFs: TMySQLQuery;
    DsUFs: TDataSource;
    DsCidades: TDataSource;
    QrMalaDireta2: TMySQLQuery;
    QrMalaDireta2CEP_TXT: TWideStringField;
    QrPage: TMySQLQuery;
    DsPage: TDataSource;
    QrPageNome: TWideStringField;
    QrPageObserva: TWideMemoField;
    QrPageCodigo: TIntegerField;
    QrPagePagCol: TIntegerField;
    QrPagePagLin: TIntegerField;
    QrPagePagLef: TIntegerField;
    QrPagePagTop: TIntegerField;
    QrPagePagWid: TIntegerField;
    QrPagePagHei: TIntegerField;
    QrPagePagGap: TIntegerField;
    QrPageBanCol: TIntegerField;
    QrPageBanLin: TIntegerField;
    QrPageBanWid: TIntegerField;
    QrPageBanHei: TIntegerField;
    QrPageBanLef: TIntegerField;
    QrPageBanTop: TIntegerField;
    QrPageBanGap: TIntegerField;
    QrPageOrient: TIntegerField;
    QrPageDataCad: TDateField;
    QrPageDataAlt: TDateField;
    QrPageUserCad: TSmallintField;
    QrPageUserAlt: TSmallintField;
    QrPageLk: TIntegerField;
    QrPageColWid: TIntegerField;
    QrMalaDireta2Codigo_NOMEENTIDADE: TWideStringField;
    QrMalaDireta2Rua_Numero_Compl: TWideStringField;
    QrMalaDireta2NUMERO_TXT: TWideStringField;
    QrPageBa2Hei: TIntegerField;
    QrPageFoSize: TSmallintField;
    QrPageMemHei: TIntegerField;
    QrPageEspaco: TSmallintField;
    QrMinhaEtiq: TMySQLQuery;
    QrMultiEtq: TMySQLQuery;
    DsMultiEtq: TDataSource;
    QrMultiEtqCodigo: TIntegerField;
    QrMultiEtqNome: TWideStringField;
    QrMultiEtqLinhas: TIntegerField;
    QrMultiEtqObserva: TWideMemoField;
    QrMultiEtqDataCad: TDateField;
    QrMultiEtqDataAlt: TDateField;
    QrMultiEtqUserCad: TSmallintField;
    QrMultiEtqUserAlt: TSmallintField;
    QrMultiEtqLk: TIntegerField;
    QrMinhaEtqIts: TMySQLQuery;
    DsMinhaEtqIts: TDataSource;
    QrMinhaEtqItsCodigo: TIntegerField;
    QrMinhaEtqItsOrdem: TIntegerField;
    QrMinhaEtqItsTexto1: TWideStringField;
    QrMinhaEtqItsTexto2: TWideStringField;
    QrMinhaEtqItsTexto3: TWideStringField;
    QrMinhaEtqItsTexto4: TWideStringField;
    QrMinhaEtiqNome: TWideStringField;
    QrMinhaEtiqConta: TIntegerField;
    QrMinhaEtiqTexto1: TWideStringField;
    QrMinhaEtiqTexto2: TWideStringField;
    QrMinhaEtiqTexto3: TWideStringField;
    QrMinhaEtiqTexto4: TWideStringField;
    QrEntidades: TMySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesCodigo: TIntegerField;
    QrEnderecos: TMySQLQuery;
    QrEnderecosConta: TIntegerField;
    QrEnderecosLinha1: TWideStringField;
    QrEnderecosLinha2: TWideStringField;
    QrEnderecosLinha3: TWideStringField;
    QrEnderecosLinha4: TWideStringField;
    Panel1: TPanel;
    PainelProfissoes: TPanel;
    Panel3: TPanel;
    BtInclui2: TBitBtn;
    BtExclui2: TBitBtn;
    DBGrid1: TDBGrid;
    PainelEntidades: TPanel;
    Panel5: TPanel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    DBGrid2: TDBGrid;
    DsProfiss: TDataSource;
    PainelCidades: TPanel;
    Panel7: TPanel;
    BtInclui1: TBitBtn;
    BtExclui1: TBitBtn;
    DBGrid3: TDBGrid;
    DsEnti: TDataSource;
    QrEnti: TmySQLQuery;
    QrEnti2: TmySQLQuery;
    QrCidades: TmySQLQuery;
    QrCidades2: TmySQLQuery;
    QrProfiss: TmySQLQuery;
    QrProfiss2: TmySQLQuery;
    QrEntiNome: TWideStringField;
    QrEntiTipo: TIntegerField;
    QrEnti2Nome: TWideStringField;
    QrEnti2Tipo: TIntegerField;
    QrCidadesNome: TWideStringField;
    QrCidadesTipo: TIntegerField;
    QrCidades2Nome: TWideStringField;
    QrCidades2Tipo: TIntegerField;
    QrProfissNome: TWideStringField;
    QrProfissTipo: TIntegerField;
    QrProfiss2Nome: TWideStringField;
    QrProfiss2Tipo: TIntegerField;
    Panel2: TPanel;
    QrMalaDireta2CIDADE_UF: TWideStringField;
    QrMalaDireta2CEP_PAIS: TWideStringField;
    QrEnderecosLinha5: TWideStringField;
    QrMalaDireta2NOMEENTIDADE: TWideStringField;
    QrMalaDireta2Rua: TWideStringField;
    QrMalaDireta2Compl: TWideStringField;
    QrMalaDireta2Bairro: TWideStringField;
    QrMalaDireta2Cidade: TWideStringField;
    QrMalaDireta2Pais: TWideStringField;
    QrMalaDireta2Te1: TWideStringField;
    QrMalaDireta2Cel: TWideStringField;
    QrMalaDireta2NOMEUF: TWideStringField;
    QrMalaDireta2Codigo: TIntegerField;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    QrUFsLk: TIntegerField;
    QrUFsDataCad: TDateField;
    QrUFsDataAlt: TDateField;
    QrUFsUserCad: TIntegerField;
    QrUFsUserAlt: TIntegerField;
    QrUFsICMS_C: TFloatField;
    QrUFsICMS_V: TFloatField;
    QrMalaDireta2Te1_TXT: TWideStringField;
    frxMalaDireta2: TfrxReport;
    frxDsMalaDireta2: TfrxDBDataset;
    frxDsMinhaEtiq: TfrxDBDataset;
    frxDsEnderecos: TfrxDBDataset;
    frxEnderecos: TfrxReport;
    frxDiversos: TfrxReport;
    Panel6: TPanel;
    CkVazios: TCheckBox;
    CkGrade: TCheckBox;
    PCConfig: TPageControl;
    TabSheet1: TTabSheet;
    Panel8: TPanel;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    EdMinhaEtiq: TdmkEditCB;
    CBMinhaEtiq: TdmkDBLookupComboBox;
    BtImpConfig2: TBitBtn;
    Label3: TLabel;
    PCEntidade: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel10: TPanel;
    RGOrdem: TRadioGroup;
    RGEndereco: TRadioGroup;
    GroupBox2: TGroupBox;
    CkCliente1: TCheckBox;
    CkFornece1: TCheckBox;
    CkFornece2: TCheckBox;
    CkFornece3: TCheckBox;
    CkFornece4: TCheckBox;
    CkTerceir1: TCheckBox;
    CkCliente2: TCheckBox;
    CkCliente3: TCheckBox;
    CkCliente4: TCheckBox;
    CkFornece5: TCheckBox;
    CkFornece6: TCheckBox;
    Label4: TLabel;
    CBUF: TDBLookupComboBox;
    Label9: TLabel;
    EdCEPIni: TdmkEdit;
    Label10: TLabel;
    EDCepFim: TdmkEdit;
    Label2: TLabel;
    EdEspacamento: TdmkEdit;
    Label6: TLabel;
    EdColIni: TdmkEdit;
    EdLinIni: TdmkEdit;
    Label7: TLabel;
    LaRepeticoes: TLabel;
    EdRepeticoes: TdmkEdit;
    CkFamilias: TCheckBox;
    Panel11: TPanel;
    Panel12: TPanel;
    Label1: TLabel;
    EdConfImp: TdmkEditCB;
    CBConfImp: TdmkDBLookupComboBox;
    RGImpressao: TRadioGroup;
    BtImpConfig1: TBitBtn;
    RGTipoImp: TRadioGroup;
    RadioGroup1: TRadioGroup;
    CkAtivo: TCheckBox;
    CkStand: TCheckBox;
    CkInati: TCheckBox;
    CkInfoTel: TCheckBox;
    CkInfoCod: TCheckBox;
    Label5: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    BtEntidades: TBitBtn;
    DsMalaDireta2: TDataSource;
    TabSheet5: TTabSheet;
    Panel13: TPanel;
    QrCustomFR3: TmySQLQuery;
    DsCustomFR3: TDataSource;
    QrCustomFR3Codigo: TIntegerField;
    QrCustomFR3CodUsu: TIntegerField;
    QrCustomFR3Nome: TWideStringField;
    QrCustomFR3Report: TWideMemoField;
    frxReport1: TfrxReport;
    PageControl1: TPageControl;
    Panel14: TPanel;
    Panel16: TPanel;
    Label8: TLabel;
    EdCustomFR3: TdmkEditCB;
    CBCustomFR3: TdmkDBLookupComboBox;
    BtCustomFR3: TBitBtn;
    PCCartao: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Panel15: TPanel;
    RGQuadro: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EdColMin: TdmkEdit;
    EdColMax: TdmkEdit;
    EdLinMin: TdmkEdit;
    EdLinMax: TdmkEdit;
    EdColI: TdmkEdit;
    EdLinI: TdmkEdit;
    CkColLetras: TCheckBox;
    CkLinLetras: TCheckBox;
    RGIncremento: TRadioGroup;
    Panel17: TPanel;
    QrMalaDireta2Celula: TWideStringField;
    PMIncluiEntidade: TPopupMenu;
    Entidades1: TMenuItem;
    Condminos1: TMenuItem;
    QrMalaDireta2CEP: TFloatField;
    QrMalaDireta2UF: TFloatField;
    QrMalaDireta2NUMERO: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel18: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel19: TPanel;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    BtConfirma: TBitBtn;
    BtASCII: TBitBtn;
    CkDesign: TCheckBox;
    BtGrade: TBitBtn;
    Progress: TProgressBar;
    QrEntiCliInt: TIntegerField;
    QrEntiDepto: TIntegerField;
    QrEntiCodigo: TIntegerField;
    QrEnti2Codigo: TIntegerField;
    QrEnti2Depto: TIntegerField;
    QrEnti2CliInt: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CBUFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCEPIniExit(Sender: TObject);
    procedure EDCepFimExit(Sender: TObject);
    procedure BtGradeClick(Sender: TObject);
    procedure CBUFClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtSairClick(Sender: TObject);
    procedure QrMalaDireta2AfterOpen(DataSet: TDataSet);
    procedure QrMalaDireta2AfterClose(DataSet: TDataSet);
    procedure RGOrdemClick(Sender: TObject);
    procedure RGEnderecoClick(Sender: TObject);
    procedure EdCEPIniChange(Sender: TObject);
    procedure EDCepFimChange(Sender: TObject);
    procedure QrCidades2AfterOpen(DataSet: TDataSet);
    procedure CkCliente1Click(Sender: TObject);
    procedure CkFornece1Click(Sender: TObject);
    procedure CkCliente2Click(Sender: TObject);
    procedure CkFornece2Click(Sender: TObject);
    procedure CkFornece3Click(Sender: TObject);
    procedure CkFornece4Click(Sender: TObject);
    procedure CkTerceir1Click(Sender: TObject);
    procedure CkVaziosClick(Sender: TObject);
    procedure QrCidadesAfterOpen(DataSet: TDataSet);
    procedure QrMalaDireta2CalcFields(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtImpConfig1Click(Sender: TObject);
    procedure EdEspacamentoExit(Sender: TObject);
    procedure BtImpConfig2Click(Sender: TObject);
    procedure CBEntidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtEntidadesClick(Sender: TObject);
    procedure EdColIniExit(Sender: TObject);
    procedure EdLinIniExit(Sender: TObject);
    procedure BtASCIIClick(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure BtInclui2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtExclui1Click(Sender: TObject);
    procedure BtInclui1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdRepeticoesExit(Sender: TObject);
    procedure frxMalaDireta2GetValue(const VarName: String;
      var Value: Variant);
    procedure frxDiversosGetValue(const VarName: String;
      var Value: Variant);
    procedure BtCustomFR3Click(Sender: TObject);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
    procedure Entidades1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure Condminos1Click(Sender: TObject);

  private
    { Private declarations }
    FColIni,FColMin, FColMax, FColCount,
    FLinIni,FLinMin, FLinMax, FLinCount: Integer;
    procedure EtiquetasDiversas;
    procedure EtiquetasEnderecos;
    procedure ReopenMinhaEtqIts;
    procedure ImprimeEmDOS(Tabela: Integer; Comprime, Show: Boolean);
    procedure ImprimeEnderecosEmEtiquetas;
    procedure ImprimeDiversosEmEtiquetas;
    procedure MostraFormulariosEntidades;
    procedure MostraFormulariosCondominos;
    //procedure ImprimeMinhaEtiqueta2();
  public
    { Public declarations }
    function ReopenEntidades(): Boolean;
    procedure ReopenCidades;
    procedure ReopenEnti;
    procedure ReopenProfissoes;
  end;

var
  FmMaladireta: TFmMaladireta;

const
  FDiretorio = 'C:\Dermatek\Temp\';

implementation

uses UnMyObjects, Module, FormulariosCidades, FormulariosResult, Principal,
  //ImpDOS, Imprime,
  MalaDiretaProfiss, FormulariosEntidades, CustomFR3, MyDBCheck,
  {$IFDEF TEM_UH} FormulariosCond, {$ENDIF}
  UMySQLModule, ModuleGeral, Pages, MultiEtiq, MyListas, UnEntities;

{$R *.DFM}

var
  Imp_Espacos: String;
  Imp_LinhasEtiq, Imp_LinhasEtiqHei, FContaCol: Integer;
  pOrient: TPrinterOrientation;

procedure TFmMaladireta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrMinhaEtiq.Database := DModG.MyPID_DB;
  QrEnderecos.Database := DModG.MyPID_DB;
  QrEnti.Database      := DModG.MyPID_DB;
  QrEnti2.Database     := DModG.MyPID_DB;
  QrCidades.Database   := DModG.MyPID_DB;
  QrCidades2.Database  := DModG.MyPID_DB;
  QrProfiss.Database   := DModG.MyPID_DB;
  QrProfiss2.Database  := DModG.MyPID_DB;
  //
  PCConfig.ActivePageIndex := 0;
  PCEntidade.ActivePageIndex := 0;
  PCCartao.ActivePageIndex := 0;
  //
  //UCriar.GerenciaTabelaLocal('Lista1', acCreate);
  {
  UCriar.GerenciaTempTable('Lista1', acCreate,
    DModG, DModG.MyPID_DB, DModG.QrUpdPID1, False);
  }
  UCriar.RecriaTempTable('Lista1', DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPage, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrMultiEtq, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUFs, Dmod.MyDB);
  ReopenEnti;
  ReopenCidades;
  ReopenProfissoes;
  UnDmkDAC_PF.AbreQuery(QrCustomFR3, Dmod.MyDB);
  //
  CkCliente1.Caption := VAR_CLIENTE1;
  CkCliente2.Caption := VAR_CLIENTE2;
  CkCliente3.Caption := VAR_CLIENTE3;
  CkCliente4.Caption := VAR_CLIENTE4;
  CkFornece1.Caption := VAR_FORNECE1;
  CkFornece2.Caption := VAR_FORNECE2;
  CkFornece3.Caption := VAR_FORNECE3;
  CkFornece4.Caption := VAR_FORNECE4;
  CkFornece5.Caption := VAR_FORNECE5;
  CkFornece6.Caption := VAR_FORNECE6;
  CkTerceir1.Caption := VAR_TERCEIR2;

  if VAR_CLIENTE2 <> '' then CkCliente2.Visible := True;
  if VAR_FORNECE1 <> '' then CkFornece1.Visible := True;
  if VAR_FORNECE2 <> '' then CkFornece2.Visible := True;
  if VAR_FORNECE3 <> '' then CkFornece3.Visible := True;
  if VAR_FORNECE4 <> '' then CkFornece4.Visible := True;
end;

procedure TFmMaladireta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMaladireta.ReopenCidades;
begin
  QrCidades.Close;
  UnDmkDAC_PF.AbreQuery(QrCidades, DModG.MyPID_DB);
  QrCidades.Last;
end;

procedure TFmMaladireta.ReopenEnti;
begin
  QrEnti.Close;
  UnDmkDAC_PF.AbreQuery(QrEnti, DModG.MyPID_DB);
  QrEnti.Last;
end;

procedure TFmMaladireta.ReopenProfissoes;
begin
  QrProfiss.Close;
  UnDmkDAC_PF.AbreQuery(QrProfiss, DModG.MyPID_DB);
  QrProfiss.Last;
end;

procedure TFmMaladireta.CBUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    CBUF.KeyValue := NULL;
    QrMalaDireta2.Close;
  end;
end;

procedure TFmMaladireta.EdCEPIniExit(Sender: TObject);
begin
  EdCEPIni.Text := Geral.FormataCEP_TT(EdCEPIni.Text);
end;

procedure TFmMaladireta.EDCepFimExit(Sender: TObject);
begin
  EdCEPFim.Text := Geral.FormataCEP_TT(EdCEPFim.Text);
end;

procedure TFmMaladireta.BtGradeClick(Sender: TObject);
begin
  Application.CreateForm(TFmFormulariosResult, FmFormulariosResult);
  FmFormulariosResult.LaEncontrados.Caption :=
  FmFormulariosResult.LaEncontrados.Caption +
  IntToStr(QrMalaDireta2.RecordCount);
  FmFormulariosResult.ShowModal;
  FmFormulariosResult.Destroy;
end;

procedure TFmMaladireta.CBUFClick(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in ([VK_RETURN, VK_TAB]) then RGOrdem.SetFocus;
end;

procedure TFmMaladireta.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMaladireta.QrMalaDireta2AfterOpen(DataSet: TDataSet);
begin
  BtGrade.Enabled := True;
end;

procedure TFmMaladireta.QrMalaDireta2AfterClose(DataSet: TDataSet);
begin
  BtGrade.Enabled := False;
end;

procedure TFmMaladireta.RGOrdemClick(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.RGEnderecoClick(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.EdCEPIniChange(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.EDCepFimChange(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.QrCidades2AfterOpen(DataSet: TDataSet);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkCliente1Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkFornece1Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkCliente2Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkFornece2Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkFornece3Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkFornece4Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkTerceir1Click(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.CkVaziosClick(Sender: TObject);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.Condminos1Click(Sender: TObject);
begin
  MostraFormulariosCondominos;
end;

procedure TFmMaladireta.QrCidadesAfterOpen(DataSet: TDataSet);
begin
  QrMalaDireta2.Close;
end;

procedure TFmMaladireta.QrMalaDireta2CalcFields(DataSet: TDataSet);
begin
  QrMalaDireta2CEP_TXT.Value := Geral.FormataCEP_NT(QrMalaDireta2CEP.Value);
  QrMalaDireta2Te1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrMalaDireta2Te1.Value);

  //
  QrMalaDireta2Codigo_NOMEENTIDADE.Value :=
  FormatFloat('00000', QrMalaDireta2Codigo.Value) + '   ' +
  QrMalaDireta2NOMEENTIDADE.Value;
  //
  if QrMalaDireta2Numero.Value = 0 then
  QrMalaDireta2NUMERO_TXT.Value := 'S/N' else
  QrMalaDireta2NUMERO_TXT.Value :=
  FormatFloat('0', QrMalaDireta2Numero.Value);
  //+
  QrMalaDireta2Rua_Numero_Compl.Value :=
  QrMalaDireta2Rua.Value + ', ' +
  QrMalaDireta2NUMERO_TXT.Value + '  '+
  QrMalaDireta2Compl.Value;
  //
  QrMalaDireta2Cidade_UF.Value :=
  QrMalaDireta2Cidade.Value + '  -  ' +
  QrMalaDireta2NOMEUF.Value;
  //
  QrMalaDireta2CEP_Pais.Value :=
  QrMalaDireta2CEP_TXT.Value + '        ' +
  QrMalaDireta2Pais.Value;
  //
end;

procedure TFmMaladireta.BtConfirmaClick(Sender: TObject);
var
  k, Repet, i, Conta, Col, Lin, MaxCol, ConfImp, MinhaEtiq, MeuCartao: Integer;
  Arquivo, Texto: String;
begin
  Conta := 0;
  Repet := 0;
  FColIni := EdColI.ValueVariant;
  FColMin := EdColMin.ValueVariant;
  FColMax := EdColMax.ValueVariant;
  FLinIni := EdLinI.ValueVariant;
  FLinMin := EdLinMin.ValueVariant;
  FLinMax := EdLinMax.ValueVariant;
  FColCount := FColIni;
  FLinCount := FLinIni;
  //
  Progress.Update;
  // Etiquetas (endereco ou diversos)
  if not ReopenEntidades() then Exit;
  //
  if PCConfig.ActivePageIndex <> 2 then
  begin
    if RGImpressao.ItemIndex < 2 then
    begin
      ConfImp := Geral.IMV(EdConfImp.Text);
      if ConfImp = 0 then
      begin
        Geral.MB_Aviso('Defina a "Configura��o de impress�o"!');
        EdConfImp.SetFocus;
        Exit;
      end;
    end;
    if RGTipoImp.ItemIndex = 0 then MaxCol := QrPageBanCol.Value
    else MaxCol := 1;
    Col := Geral.IMV(EdColIni.Text) - 1;
    if (Col >= MaxCol) and (PCConfig.ActivePageIndex > 0) then
    begin
      Geral.MB_Aviso('Coluna inicial fora dos par�metros!');
      EdColIni.SetFocus;
      Exit;
    end;
    Lin := Geral.IMV(EdLinIni.Text) - 1;
    Conta := (Lin * QrPageBanCol.Value) + Col;
    Repet := Geral.IMV(EdRepeticoes.Text);

    //
  end;
  case PCConfig.ActivePageIndex of
    0:
    begin
      UCriar.RecriaTempTable('Enderecos', DModG.QrUpdPID1, False);
      ////
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO enderecos SET Conta=:P0, Linha1=:P1,');
      DModG.QrUpdPID1.SQL.Add('Linha2=:P2, Linha3=:P3, Linha4=:P4, Linha5=:P5');
      Progress.Position := 0;
      Progress.Max := Conta + QrMalaDireta2.RecordCount;
      for i  := 1 to Conta do
      begin
        Progress.Position := Progress.Position + 1;
        DModG.QrUpdPID1.Params[0].AsInteger := i;
        DModG.QrUpdPID1.Params[1].AsString := CO_VAZIO;
        DModG.QrUpdPID1.Params[2].AsString := CO_VAZIO;
        DModG.QrUpdPID1.Params[3].AsString := CO_VAZIO;
        DModG.QrUpdPID1.Params[4].AsString := CO_VAZIO;
        DModG.QrUpdPID1.Params[5].AsString := CO_VAZIO;
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrMalaDireta2.First;
      while not QrMalaDireta2.Eof do
      begin
        for k := 1 to Repet do
        begin
          Progress.Position := Progress.Position + 1;
          Conta := Conta + 1;
          DModG.QrUpdPID1.Params[0].AsInteger := Conta;
          DModG.QrUpdPID1.Params[1].AsString := Imp_Espacos+QrMalaDireta2Codigo_NOMEENTIDADE.Value;
          DModG.QrUpdPID1.Params[2].AsString := Imp_Espacos+QrMalaDireta2Rua_Numero_Compl.Value;
          DModG.QrUpdPID1.Params[3].AsString := Imp_Espacos+QrMalaDireta2Bairro.Value;
          DModG.QrUpdPID1.Params[4].AsString := Imp_Espacos+QrMalaDireta2Cidade_UF.Value;
          DModG.QrUpdPID1.Params[5].AsString := Imp_Espacos+QrMalaDireta2CEP_Pais.Value;
          DModG.QrUpdPID1.ExecSQL;
        end;
        QrMalaDireta2.Next;
      end;
      Progress.Max := Progress.Max * 4{Linhas};
      Progress.Position := 0;
      EtiquetasEnderecos;
    end;
    1:
    begin
      MinhaEtiq := Geral.IMV(EdMinhaEtiq.Text);
      if MinhaEtiq = 0 then
      begin
        Geral.MB_Aviso('Defina a "Minha etiqueta"!');
        EdMinhaEtiq.SetFocus;
        Exit;
      end;
      UCriar.RecriaTempTable('MinhaEtiq', DModG.QrUpdPID1, False);
      ////
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO minhaetiq SET Nome=:P0, Conta=:P1');
      if QrMultiEtqLinhas.Value > 1 then DModG.QrUpdPID1.SQL.Add(', Texto1=:P2');
      if QrMultiEtqLinhas.Value > 2 then DModG.QrUpdPID1.SQL.Add(', Texto2=:P3');
      if QrMultiEtqLinhas.Value > 3 then DModG.QrUpdPID1.SQL.Add(', Texto3=:P4');
      if QrMultiEtqLinhas.Value > 4 then DModG.QrUpdPID1.SQL.Add(', Texto4=:P5');
      Progress.Position := 0;
      Progress.Max := Conta + GOTOy.Registros(QrMalaDireta2);
      for i  := 1 to Conta do
      begin
        Progress.Position := Progress.Position + 1;
         DModG.QrUpdPID1.Params[0].AsString := CO_VAZIO;
         DModG.QrUpdPID1.Params[1].AsInteger := Conta;
         if QrMultiEtqLinhas.Value > 1 then
           DModG.QrUpdPID1.Params[2].AsString := CO_VAZIO;
         if QrMultiEtqLinhas.Value > 2 then
           DModG.QrUpdPID1.Params[3].AsString := CO_VAZIO;
         if QrMultiEtqLinhas.Value > 3 then
           DModG.QrUpdPID1.Params[4].AsString := CO_VAZIO;
         if QrMultiEtqLinhas.Value > 4 then
           DModG.QrUpdPID1.Params[5].AsString := CO_VAZIO;
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrMalaDireta2.First;
      ReopenMinhaEtqIts;
      while not QrMalaDireta2.Eof do
      begin
        for k := 1 to Repet do
        begin
          QrMinhaEtqIts.First;
          while not QrMinhaEtqIts.Eof do
          begin
            Conta := Conta + 1;
            Progress.Position := Progress.Position + 1;
            Texto := QrMalaDireta2NOMEENTIDADE.Value;
            if CkInfoTel.Checked then Texto := Texto + QrMalaDireta2Te1_TXT.Value + ' ' + Texto;
            if CkInfoCod.Checked then Texto := FormatFloat('0000',  QrMalaDireta2Codigo.Value) + ' - ' + Texto;
            DModG.QrUpdPID1.Params[0].AsString := Texto;
            DModG.QrUpdPID1.Params[1].AsInteger := Conta;
            if QrMultiEtqLinhas.Value > 1 then
              DModG.QrUpdPID1.Params[2].AsString := QrMinhaEtqItsTexto1.Value;
            if QrMultiEtqLinhas.Value > 2 then
              DModG.QrUpdPID1.Params[3].AsString := QrMinhaEtqItsTexto2.Value;
            if QrMultiEtqLinhas.Value > 3 then
              DModG.QrUpdPID1.Params[4].AsString := QrMinhaEtqItsTexto3.Value;
            if QrMultiEtqLinhas.Value > 4 then
              DModG.QrUpdPID1.Params[5].AsString := QrMinhaEtqItsTexto4.Value;
            DModG.QrUpdPID1.ExecSQL;
            QrMinhaEtqIts.Next;
          end;
        end;
        QrMalaDireta2.Next;
      end;
      Progress.Max := Progress.Max * 4{Linhas};
      Progress.Position := 0;
      EtiquetasDiversas;
    end;
    2:
    begin
      MeuCartao := EdCustomFR3.ValueVariant;
      if MeuCartao = 0 then
      begin
        Geral.MB_Aviso('Defina a "Configura��o do meu cart�o"!');
        EdCustomFR3.SetFocus;
        Exit;
      end;
      Arquivo := FDiretorio + FormatFloat('000000', QrCustomFR3Codigo.Value) + '.fr3';
      if Trim(QrCustomFR3Report.Value) <> '' then
      begin
        if FileExists(Arquivo) then
          DeleteFile(Arquivo);
        QrCustomFR3Report.SaveToFile(Arquivo);
        frxReport1.LoadFromFile(Arquivo);
      end else frxReport1.Clear;
(*
      MyObjects.frxDefineDatasets(frxReport1, [
      /  ????
      ]);
*)
      if not CkDesign.Checked then
        MyObjects.frxMostra(frxReport1, QrCustomFR3Nome.Value)
      else
        frxReport1.DesignReport;
    end;
    else Geral.MB_Aviso('Impress�o n�o implementada!');
  end;
  Progress.Position := 0;
end;

procedure TFmMaladireta.EtiquetasEnderecos;
begin
  QrEnderecos.Close;
  UnDmkDAC_PF.AbreQuery(QrEnderecos, DModG.MyPID_DB);
  case RGImpressao.ItemIndex of
    0:
    begin
      case RGTipoImp.ItemIndex of
        0: ImprimeEnderecosEmEtiquetas;
        1: ImprimeEmDOS(1, False, False);
        2: ImprimeEmDOS(1, True,  False);
      end;
    end;
    1:
    begin
      case RGTipoImp.ItemIndex of
        0: ImprimeEnderecosEmEtiquetas;
        1: ImprimeEmDOS(1, False, True);
        2: ImprimeEmDOS(1, True,  True);
      end;
    end;
    2:
    begin
      MyObjects.frxDefineDatasets(frxMalaDireta2, [
        DmodG.frxDsDono,
        frxDsMalaDireta2
      ]);
      MyObjects.frxMostra(frxMalaDireta2, 'Lista de endere�os');
    end;
    3: BtGradeClick(Self);
  end;
  QrEnderecos.Close;
end;

procedure TFmMaladireta.EtiquetasDiversas;
begin
  QrMinhaEtiq.Close;
  UnDmkDAC_PF.AbreQuery(QrMinhaEtiq, DModG.MyPID_DB);
  case RGImpressao.ItemIndex of
    0:
    begin
      case RGTipoImp.ItemIndex of
        0: ImprimeDiversosEmEtiquetas;
        1: ImprimeEmDOS(2, False, False);
        2: ImprimeEmDOS(2, True,  False);
      end;
    end;
    1:
    begin
      case RGTipoImp.ItemIndex of
        0: ImprimeDiversosEmEtiquetas;
        1: ImprimeEmDOS(2, False, True);
        2: ImprimeEmDOS(2, True,  True);
      end;
    end;
    2:
    begin
      MyObjects.frxDefineDatasets(frxMalaDireta2, [
        DmodG.frxDsDono,
        frxDsMalaDireta2
      ]);
       MyObjects.frxMostra(frxMalaDireta2, 'Lista de endere�os');
    end;
    3: BtGradeClick(Self);
  end;
  QrMinhaEtiq.Close;
end;

procedure TFmMaladireta.BtImpConfig1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPages, FmPages, afmoNegarComAviso) then
  begin
    FmPages.ShowModal;
    FmPages.Destroy;
    QrPage.Close;
    UnDmkDAC_PF.AbreQuery(QrPage, Dmod.MyDB);
    EdConfimp.Text := IntToStr(VAR_CONFIPAGE);
    CBConfimp.KeyValue := VAR_CONFIPAGE;
  end;
end;

procedure TFmMaladireta.EdEspacamentoExit(Sender: TObject);
begin
  EdEspacamento.Text := Geral.TFT_MinMax(EdEspacamento.Text, 0, 50, 0);
end;

procedure TFmMaladireta.BtImpConfig2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
  QrMultiEtq.Close;
  UnDmkDAC_PF.AbreQuery(QrMultiEtq, Dmod.MyDB);
  EdMinhaEtiq.Text := IntToStr(VAR_CONFIETIQ);
  CBMinhaEtiq.KeyValue := VAR_CONFIETIQ;
end;

procedure TFmMaladireta.ReopenMinhaEtqIts;
begin
  QrMinhaEtqIts.Close;
  QrMinhaEtqIts.Params[0].AsInteger := QrMultiEtqCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMinhaEtqIts, Dmod.MyDB);
end;

procedure TFmMaladireta.ImprimeEmDOS(Tabela: Integer; Comprime, Show: Boolean);
{
var
  Linha2, Linha3, Linha4, Linha5: String;
}
begin
{
  VAR_COMPRESS := Comprime;
  Application.CreateForm(TFmImpDOS, FmImpDOS);
  if Tabela = 1 then
  begin
    QrMalaDireta2.First;
    while not QrMalaDireta2.Eof do
    begin
      with FmImpDOS do
      begin
        Memo.Lines.Add('');
        Memo.Lines.Add('');
        Memo.Lines.Add(QrMalaDireta2Codigo_NOMEENTIDADE.Value);
        Memo.Lines.Add(QrMalaDireta2Rua_Numero_Compl.Value);
        Memo.Lines.Add(QrMalaDireta2Bairro.Value);
        Memo.Lines.Add(QrMalaDireta2Cidade_UF.Value);
        Memo.Lines.Add(QrMalaDireta2CEP_PAIS.Value);
        Memo.Lines.Add('');
        Memo.Lines.Add('');
      end;
      QrMalaDireta2.Next;
    end;
    if Show then FmImpDos.ShowModal
    else FmImpDos.Imprime;
  end;
  /////
  if Tabela = 2 then
  begin
    QrMinhaEtiq.Close;
    UnDmkDAC_PF.AbreQuery(QrMinhaEtiq, DModG.MyPID_DB);
    QrMinhaEtiq.First;
    while not QrMinhaEtiq.Eof do
    begin
      with FmImpDOS do
      begin
        if QrMultiEtqLinhas.Value > 1 then Linha2 := QrMinhaEtiqTexto1.Value else Linha2 := '';
        if QrMultiEtqLinhas.Value > 2 then Linha3 := QrMinhaEtiqTexto2.Value else Linha3 := '';
        if QrMultiEtqLinhas.Value > 3 then Linha4 := QrMinhaEtiqTexto3.Value else Linha4 := '';
        if QrMultiEtqLinhas.Value > 4 then Linha5 := QrMinhaEtiqTexto4.Value else Linha5 := '';
        Memo.Lines.Add('');
        Memo.Lines.Add('');
        Memo.Lines.Add(QrMinhaEtiqNome.Value);
        Memo.Lines.Add(Linha2);
        Memo.Lines.Add(Linha3);
        Memo.Lines.Add(Linha4);
        Memo.Lines.Add(Linha5);
        Memo.Lines.Add('');
        Memo.Lines.Add('');
      end;
      QrMinhaEtiq.Next;
    end;
    if Show then FmImpDos.ShowModal
    else FmImpDos.Imprime;
    QrMinhaEtiq.Close;
  end;
  FmImpDOS.Destroy;
  }
  Geral.MB_Aviso('Tipo de impress�o indispon�vel!');
end;

function TFmMaladireta.ReopenEntidades(): Boolean;
var
  Nome, C1, C2, C3, C4, F1, F2, F3, F4, F5, F6, T1, P, E, N, R, T: String;
  CEPi, CEPf, Entidade: Integer;
  A1, A2, A3: Integer;
  MeuCartaoA, MeuCartaoB, MeuCartaoC: String;
begin
  CEPf := 0;
  CEPi := 0;
  Entidade := 0;
  A1 := 0;
  A2 := 0;
  A3 := 0;
  //
  Result := False;
  //
  //Memo2.Clear;
  FContaCol := 0;
  Imp_Espacos := Geral.CompletaString(
  ' ', ' ', Geral.IMV(EdEspacamento.Text), taRightJustify, True);
  QrMalaDireta2.Close;
  //
  case PCEntidade.ActivePageIndex of
    0:
    begin
      Entidade := 0;
      if CkCliente1.Checked then C1 := 'V' else C1 := 'X';
      if CkCliente2.Checked then C2 := 'V' else C2 := 'X';
      if CkCliente3.Checked then C3 := 'V' else C3 := 'X';
      if CkCliente4.Checked then C4 := 'V' else C4 := 'X';
      if CkFornece1.Checked then F1 := 'V' else F1 := 'X';
      if CkFornece2.Checked then F2 := 'V' else F2 := 'X';
      if CkFornece3.Checked then F3 := 'V' else F3 := 'X';
      if CkFornece4.Checked then F4 := 'V' else F4 := 'X';
      if CkFornece5.Checked then F5 := 'V' else F5 := 'X';
      if CkFornece6.Checked then F6 := 'V' else F6 := 'X';
      if CkTerceir1.Checked then T1 := 'V' else T1 := 'X';
      if (C1='X') and (C2='X') and (C3='X') and (C4='X') and
         (F1='X') and (F2='X') and (F3='X') and (F4='X') and
         (F5='X') and (F6='X') and (T1='X') then
      begin
        Geral.MB_Aviso('Defina pelo menos um "Tipo de cadastro"');
        Exit;
      end;
      if CkAtivo.Checked then A1 := 0 else A1 := -1000;
      if CkStand.Checked then A2 := 1 else A2 := -1000;
      if CkInati.Checked then A3 := 2 else A3 := -1000;
      if (A1=-1000) and (A2=-1000) and (A3=-1000) then
      begin
        Geral.MB_Aviso('Defina pelo menos um "Situa��o de cadastro"');
        Exit;
      end;
      CEPi := Geral.IMV(Geral.SoNumero_TT(EdCEPIni.Text));
      CEPf := Geral.IMV(Geral.SoNumero_TT(EdCEPFim.Text));
      if CEPf = 0 then CEPf := 100000000;
    end;
    1:
    begin
      Entidade := Geral.IMV(EdEntidade.Text);
      if Entidade = 0 then
      begin
        Geral.MB_Aviso('Defina a "Entidade �nica"');
        Exit;
      end;
    end;
  end;

  //

  if (CBConfimp.KeyValue = NULL) and (RGImpressao.ItemIndex=0) then
  begin
    Geral.MB_Aviso(
    'Defina a "Configura��o de Impress�o" para imprimir etiquetas!');
    CBConfimp.SetFocus;
    Exit;
  end;

  //

  MeuCartaoA  := ', "" Celula';
  MeuCartaoB  := '';
  MeuCartaoC  := '';
  case PCConfig.ActivePageIndex of
    0: // Endere�o
    begin
      Imp_LinhasEtiqHei := 5;
      Imp_LinhasEtiq := 5;
    end;
    1: // Minha Etiqueta
    begin
      if (CBMinhaEtiq.KeyValue = NULL) then
      begin
        Geral.MB_Aviso('Defina a "Minha Etiqueta" para imprimir as etiquetas!');
        CBMinhaEtiq.SetFocus;
        Exit;
      end;
      Imp_LinhasEtiqHei := 1;//QrMultiEtqLinhas.Value;
      Imp_LinhasEtiq := QrMultiEtqLinhas.Value;
    end;
    2: // Meu cart�o (CustomFR3)
    begin
      MeuCartaoA  := ', eq.Celula';
      MeuCartaoB  := ', entiquadro eq';             //  Parei Aqui Falta Fazer!
      MeuCartaoC  := ' AND eq.EntiCod=en.Codigo '; // ' AND eq.EntiEmp=?'
      // Apenas por compatibilidade !!!
      Imp_LinhasEtiqHei := 1;
      // Apenas por compatibilidade !!!
      Imp_LinhasEtiq := 1;
      //

    end;
    else Imp_LinhasEtiqHei := 0;
  end;
  if Imp_LinhasEtiqHei = 0 then
  begin
    Geral.MB_Aviso('Linhas nas etiquetas n�o definidas!');
    Exit;
  end;
  //
  if QrPageOrient.Value = 0 then pOrient := poLandscape else pOrient := poPortrait;
  QrCidades2.Close;
  UnDmkDAC_PF.AbreQuery(QrCidades2, DModG.MyPID_DB);
  QrProfiss2.Close;
  UnDmkDAC_PF.AbreQuery(QrProfiss2, DModG.MyPID_DB);
  QrEnti2.Close;
  UnDmkDAC_PF.AbreQuery(QrEnti2, DModG.MyPID_DB);
  QrMalaDireta2.SQL.Clear;
  case RGEndereco.ItemIndex of
    0: P := 'P';
    1: P := 'P';
    2: P := 'E';
    3: P := 'C';
    4: P := 'L';
  end;
  case RGEndereco.ItemIndex of
    0: E := 'E';
    1: E := 'P';
    2: E := 'E';
    3: E := 'C';
    4: E := 'L';
  end;
  case RGEndereco.ItemIndex of
    0: N := 'Nome';
    1: N := 'Nome';
    2: N := 'RazaoSocial';
    3: N := 'Nome';
    4: N := 'Nome';
  end;
  case RGEndereco.ItemIndex of
    0: R := 'RazaoSocial';
    1: R := 'Nome';
    2: R := 'RazaoSocial';
    3: R := 'RazaoSocial';
    4: R := 'RazaoSocial';
  end;
  case RGEndereco.ItemIndex of
    0: T := 'Te1';
    1: T := 'Te1';
    2: T := 'Te1';
    3: T := 'Tel';
    4: T := 'Tel';
  end;
  QrMalaDireta2.SQL.Add('SELECT');
  // Fim 2011-02-13 Migra��o MySQL 5.x ERRO TLargeIntField
{
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Numero else en.'+P+'Numero END Numero,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'UF     else en.'+P+'UF     END UF,');
}
  //QrMalaDireta2.SQL.Add('IF(en.Tipo=0, en.'+E+'CEP, en.'+P+'CEP)');
  //QrMalaDireta2.SQL.Add('ECEP, PCEP, EUF, PUF, ENumero, PNumero,');
  // Fim 2011-02-13
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'CEP    else en.'+P+'CEP    END + 0.000 CEP,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'UF     else en.'+P+'UF     END + 0.000 UF,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Numero else en.'+P+'Numero END + 0.000 NUMERO,');
  //
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+R+'       else en.'+N+'       END NOMEENTIDADE,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Rua    else en.'+P+'Rua    END Rua,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Compl  else en.'+P+'Compl  END Compl,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Bairro else en.'+P+'Bairro END Bairro,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Cidade else en.'+P+'Cidade END Cidade,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Pais   else en.'+P+'Pais   END Pais,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+T+'     else en.'+P+T+'     END Te1,');
  QrMalaDireta2.SQL.Add('CASE WHEN en.Tipo=0 THEN en.'+E+'Cel    else en.'+P+'Cel    END Cel,');
  QrMalaDireta2.SQL.Add('uf.Nome NOMEUF, en.Codigo ' + MeuCartaoA);
  QrMalaDireta2.SQL.Add('FROM entidades en, UFs uf ' + MeuCartaoB);
  //
  if CkFamilias.Checked then
    QrMalaDireta2.SQL.Add(', Familias fa');
  //
  if CO_DMKID_APP in [4, 5] then //Syndic e Synker
    QrMalaDireta2.SQL.Add('LEFT JOIN condimov im ON im.Propriet=en.Codigo');
  //
  QrMalaDireta2.SQL.Add('WHERE uf.Codigo=(CASE WHEN en.Tipo=0 ');
  QrMalaDireta2.SQL.Add('THEN en.'+E+'UF else en.'+P+'UF END) ' + MeuCartaoC);
  //
  if CkFamilias.Checked then
    QrMalaDireta2.SQL.Add('AND fa.Chefe=en.Codigo');
  //
  if Entidade <> 0 then
    QrMalaDireta2.SQL.Add('AND en.Codigo = '+IntToStr(Entidade))
  else
  begin
    QrMalaDireta2.SQL.Add('AND (CASE WHEN en.Tipo=0 THEN en.'+E+'CEP else en.'+P+'CEP END) BETWEEN '+IntToStr(CEPi)+' AND '+IntToStr(CEPf));
    //
    if CBUF.KeyValue <> NULL then
      QrMalaDireta2.SQL.Add('AND (CASE WHEN en.Tipo=0 THEN en.'+E+'UF else en.'+P+'UF END) = '+IntToStr(CBUF.KeyValue));
    //
    if GOTOy.Registros(QrCidades2) = 1 then
      QrMalaDireta2.SQL.Add('AND (CASE WHEN en.Tipo=0 THEN en.'+E+'Cidade else en.'+P+'Cidade END) = '''+QrCidades2Nome.Value+'''');
    if GOTOy.Registros(QrCidades2) > 1 then
    begin
      QrMalaDireta2.SQL.Add('AND ((CASE WHEN en.Tipo=0 THEN en.'+E+'Cidade else en.'+P+'Cidade END) = '''+QrCidades2Nome.Value+'''');
      QrCidades2.Next;
      while not QrCidades2.Eof do
      begin
        QrMalaDireta2.SQL.Add('OR (CASE WHEN en.Tipo=0 THEN en.'+E+'Cidade else en.'+P+'Cidade END) = '''+QrCidades2Nome.Value+'''');
        QrCidades2.Next;
      end;
      QrMalaDireta2.SQL.Add(')');
    end;
    //
    if GOTOy.Registros(QrProfiss2) = 1 then
      QrMalaDireta2.SQL.Add('AND profissao = '''+QrProfiss2Nome.Value+'''');
    if GOTOy.Registros(QrProfiss2) > 1 then
    begin
      QrMalaDireta2.SQL.Add('AND (profissao = '''+QrProfiss2Nome.Value+'''');
      QrProfiss2.Next;
      while not QrProfiss2.Eof do
      begin
        QrMalaDireta2.SQL.Add('OR profissao = '''+QrProfiss2Nome.Value+'''');
        QrProfiss2.Next;
      end;
      QrMalaDireta2.SQL.Add(')');
    end;
    //
    case RGEndereco.ItemIndex of
      0: Nome := '(CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END)';
      1: Nome := 'en.Nome';
      2: Nome := 'en.RazaoSocial';
      3: Nome := '(CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END)';
      4: Nome := '(CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END)';
    end;
    if GOTOy.Registros(QrEnti2) = 1 then
    begin
      QrMalaDireta2.SQL.Add('AND en.Codigo = "' + Geral.FF0(QrEnti2Codigo.Value) + '"');
      //
      if CO_DMKID_APP in [4, 5] then //Syndic e Synker
      begin
        QrMalaDireta2.SQL.Add('AND im.Conta = "' + Geral.FF0(QrEnti2Depto.Value) + '"');
        QrMalaDireta2.SQL.Add('AND im.Codigo = "' + Geral.FF0(QrEnti2CliInt.Value) + '"');
      end;
    end;
    if GOTOy.Registros(QrEnti2) > 1 then
    begin
      QrMalaDireta2.SQL.Add('AND ((en.Codigo='+ Geral.FF0(QrEnti2Codigo.Value));
      //
      if CO_DMKID_APP in [4, 5] then //Syndic e Synker
      begin
        QrMalaDireta2.SQL.Add('AND im.Codigo='+ Geral.FF0(QrEnti2CliInt.Value));
        QrMalaDireta2.SQL.Add('AND im.Conta='+ Geral.FF0(QrEnti2Depto.Value));
      end;
      QrMalaDireta2.SQL.Add(')');
      QrEnti2.Next;
      while not QrEnti2.Eof do
      begin
        QrMalaDireta2.SQL.Add('OR (en.Codigo='+ Geral.FF0(QrEnti2Codigo.Value));
        //
        if CO_DMKID_APP in [4, 5] then //Syndic e Synker
        begin
          QrMalaDireta2.SQL.Add('AND im.Codigo='+ Geral.FF0(QrEnti2CliInt.Value));
          QrMalaDireta2.SQL.Add('AND im.Conta='+ Geral.FF0(QrEnti2Depto.Value));
        end;
        QrMalaDireta2.SQL.Add(')');
        QrEnti2.Next;
      end;
      QrMalaDireta2.SQL.Add(')');
    end;
    //
    QrMalaDireta2.SQL.Add('AND (en.Cliente1="'+C1+'" OR en.Cliente2="'+C2+'"');
    QrMalaDireta2.SQL.Add('  OR en.Cliente3="'+C3+'" OR en.Cliente4="'+C4+'"');
    QrMalaDireta2.SQL.Add('  OR en.Fornece1="'+F1+'" OR en.Fornece2="'+F2+'"');
    QrMalaDireta2.SQL.Add('  OR en.Fornece3="'+F3+'" OR en.Fornece4="'+F4+'"');
    QrMalaDireta2.SQL.Add('  OR en.Fornece5="'+F5+'" OR en.Fornece6="'+F6+'"');
    QrMalaDireta2.SQL.Add('  OR Terceiro="'+T1+'")');
    //
    if CkVazios.Checked then
      QrMalaDireta2.SQL.Add('AND (CASE WHEN en.Tipo=0 THEN en.'+R+' else en.'+N+' END) <> ""');
    //
    QrMalaDireta2.SQL.Add('AND Situacao in ('+IntToStr(A1)+', '+IntToStr(A2)+', '+IntToStr(A3)+')');
    //
  end;
  case RGOrdem.ItemIndex of
    0: QrMalaDireta2.SQL.Add('ORDER BY NOMEUF, CIDADE, IF(en.Tipo=0, en.'+E+'CEP, en.'+P+'CEP), NOMEENTIDADE');
    1: QrMalaDireta2.SQL.Add('ORDER BY CIDADE, IF(en.Tipo=0, en.'+E+'CEP, en.'+P+'CEP), NOMEENTIDADE');
    2: QrMalaDireta2.SQL.Add('ORDER BY IF(en.Tipo=0, en.'+E+'CEP, en.'+P+'CEP), NOMEENTIDADE');
    3: QrMalaDireta2.SQL.Add('ORDER BY NOMEENTIDADE');
  end;
  UnDmkDAC_PF.AbreQuery(QrMalaDireta2, Dmod.MyDB);
  Result := True;
end;

procedure TFmMaladireta.CBEntidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DELETE then
  begin
    CBEntidade.KeyValue := NULL;
    EdEntidade.Text     := '0';
  end;
end;

procedure TFmMaladireta.BtEntidadesClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(VAR_ENTIDADE, fmcadEntidade2, fmcadEntidade2);
  QrEntidades.Close;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  EdEntidade.Text := IntToStr(VAR_ENTIDADE);
  CBEntidade.KeyValue := VAR_ENTIDADE;
end;

procedure TFmMaladireta.EdColIniExit(Sender: TObject);
begin
  EdColIni.Text := Geral.TFT_MinMax(EdColIni.Text, 1, 1000, 0);
end;

procedure TFmMaladireta.EdLinIniExit(Sender: TObject);
begin
  EdLinIni.Text := Geral.TFT_MinMax(EdLinIni.Text, 1, 1000, 0);
end;

procedure TFmMaladireta.BtCustomFR3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCustomFR3, FmCustomFR3, afmoNegarComAviso) then
  begin
    FmCustomFR3.ShowModal;
    FmCustomFR3.Destroy;
  end;
  QrCustomFR3.Close;
  UnDmkDAC_PF.AbreQuery(QrCustomFR3, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdCustomFR3, CBCustomFR3, QrCustomFR3,
    VAR_CADASTRO, 'CodUsu');
    //EdCustomFR3.ValueVariant := VAR_CADASTRO;
    //CBCustomFR3.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmMaladireta.BtASCIIClick(Sender: TObject);
begin
  //if Memo2.Visible then Memo2.Visible := False else Visible := True;
end;

procedure TFmMaladireta.BtExclui2Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja eliminar esta profiss�o da consulta?') = ID_YES Then
  begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add(DELETE_FROM + ' lista1 WHERE Nome='''+QrProfissNome.Value+'''');
    DModG.QrUpdPID1.SQL.Add('AND Tipo='+IntToStr(QrProfissTipo.Value));
    DModG.QrUpdPID1.ExecSQL;
    ReopenProfissoes;
  end;
end;

procedure TFmMaladireta.BtInclui2Click(Sender: TObject);
begin
  Application.CreateForm(TFmMalaDiretaProfiss, FmMalaDiretaProfiss);
  FmMalaDiretaProfiss.ShowModal;
  FmMalaDiretaProfiss.Destroy;
  ReopenProfissoes;
end;

procedure TFmMaladireta.BtIncluiClick(Sender: TObject);
begin
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.MostraPopUpDeBotao(PMIncluiEntidade, BtInclui)
  else
    MostraFormulariosEntidades;
end;

procedure TFmMaladireta.FormResize(Sender: TObject);
var
  Tam: Integer;
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  Tam := Trunc((PainelEntidades.Height + PainelCidades.Height +
    PainelProfissoes.Height) / 3);
  PainelEntidades.Height := Tam;
  PainelProfissoes.Height := Tam;
end;

procedure TFmMaladireta.BtExclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja eliminar esta cidade da consulta?') = ID_YES Then
  begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add(DELETE_FROM + ' lista1 WHERE Nome='''+QrCidadesNome.Value+'''');
    DModG.QrUpdPID1.SQL.Add('AND Tipo='+IntToStr(QrCidadesTipo.Value));
    DModG.QrUpdPID1.ExecSQL;
    ReopenCidades;
  end;
end;

procedure TFmMaladireta.BtInclui1Click(Sender: TObject);
begin
  Application.CreateForm(TFmFormulariosCidades, FmFormulariosCidades);
  VAR_LOCATE001 := RGEndereco.ItemIndex;
  FmFormulariosCidades.ShowModal;
  FmFormulariosCidades.Destroy;
  ReopenCidades;
end;

procedure TFmMaladireta.BtExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(DModG.QrUpdPID1, QrEnti, DBGrid2,
    'lista1', ['Nome', 'Tipo'], ['Nome', 'Tipo'], istPergunta, '');
{
  if Geral.MB_('Deseja eliminar esta entidade da consulta?',
  'Elimina��o de entidade da Consulta',MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1
  +MB_APPLMODAL) = ID_YES Then
  begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add(DELETE_FROM + ' lista1 WHERE Nome='''+QrEntiNome.Value+'''');
    DModG.QrUpdPID1.SQL.Add('AND Tipo='+IntToStr(QrEntiTipo.Value));
    DModG.QrUpdPID1.ExecSQL;
    ReopenEnti;
  end;
}
end;

procedure TFmMaladireta.EdRepeticoesExit(Sender: TObject);
begin
  EdRepeticoes.Text := Geral.TFT_MinMax(EdRepeticoes.Text, 1, 1000, 0);
end;

procedure TFmMaladireta.Entidades1Click(Sender: TObject);
begin
  MostraFormulariosEntidades;
end;

procedure TFmMaladireta.frxMalaDireta2GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'ORDEM' then Value := RGOrdem.Items[RGOrdem.ItemIndex];
end;

procedure TFmMaladireta.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'COLUNA_LINHA' then
  begin
    case PCCartao.ActivePageIndex of
      0: Value := QrMalaDireta2Celula.Value;
      1:
      begin
        if CkColLetras.Checked then
          Value := dmkPF.NumeroToLetras(FColCount)
        else
          Value := FormatFloat('0', FColCount);
        Value := Value + '-';
        if CkLinLetras.Checked then
          Value := Value + dmkPF.NumeroToLetras(FLinCount)
        else
          Value := Value + FormatFloat('0', FLinCount);
        //
        // Incrementar;
        if RGIncremento.ItemIndex = 0 then
        begin
          if FLinCount = FLinMax then
          begin
            FColCount := FColCount + 1;
            FLinCount := 1;
          end else FLinCount := FLinCount + 1;
        end else begin
          if FColCount = FColMax then
          begin
            FLinCount := FLinCount + 1;
            FColCount := 1;
          end else FColCount := FColCount + 1;
        end;
      end;
      else Geral.MB_Aviso('Item de impress�o de "Meu Cart�o" n�o implementado!');
    end;
  end else
end;

procedure TFmMaladireta.ImprimeDiversosEmEtiquetas;
var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align, i,
  L, T, W, H: Integer;
  Fator: Double;
begin
  // Configura p�gina
  while frxDiversos.PagesCount > 0 do
    frxDiversos.Pages[0].Free;
  Page := TfrxReportPage.Create(frxDiversos);
  Page.CreateUniqueName;
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  Page.Height       := Trunc(QrPagePagHei.Value / VAR_DOTIMP);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  H := Trunc(QrPagePagTop.Value / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  // Configura Banda
  BANLEF := Trunc(QrPageBanLef.Value / VAR_DOTIMP);
  BANTOP := Trunc(QrPageBanTop.Value / VAR_DOTIMP);
  BANWID := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  BANHEI := Trunc(QrPageBanHei.Value / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  //BA2HEI := Trunc(QrPageBa2Hei.Value / VAR_DOTIMP / (QrPageEspaco.Value + 1));
  //for i := 1 to 5 do
  //begin
    // Configura memo da Linha 1
    // Left:
    Fator := Trunc(FContaCol / Imp_LinhasEtiqHei);
    MEMLEF := Trunc((QrPageBanLef.Value / VAR_DOTIMP) + (Fator *
       ((QrPageColWid.Value / VAR_DOTIMP)+(QrPageBanGap.Value / VAR_DOTIMP))));
    FContaCol := FContaCol + 1;
    if FContaCol >= (QrPageBanCol.Value * Imp_LinhasEtiqHei) then FContaCol := 0;
    // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
    MEMTOP := 0;//Trunc(((i-1) * QrPageMemHei.Value) / VAR_DOTIMP);
    // Width := QrPageColWid.Value / VAR_DOTIMP;
    MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
    // Height:= QrPageMemHei.Value / VAR_DOTIMP;
    // Mesmo altura da banda, pois as linhas s�o diretoas no texto
    MEMHEI := BANHEI;//Trunc(QrPageMemHei.Value / VAR_DOTIMP);
    //
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    //
    //Align := (QrItensASet_TAli.Value * 100) + QrItensASet_BAli.Value;
    //MyObjects.frxMemoAlignment(Memo, Align);
    //
    Memo.Font.Name := 'Univers Light Condensed';
    Memo.Font.Size := QrPageFoSize.Value;
    Memo.Memo.Text := '[MINHAETIQUETA]';
    //
    if CkGrade.Checked then
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    //
  //end;
  //////////////////////////////////////////////////////////////////////////////
  // Imprime ou mostra design do relat�rio
  if CkDesign.Checked then
    frxDiversos.DesignReport
  else
    if RGImpressao.ItemIndex = 0 then
      MyObjects.frxImprime(frxDiversos, 'Minha etiqueta')
    else
    begin
      MyObjects.frxDefineDatasets(frxDiversos, [
        DmodG.frxDsDono,
        frxDsMinhaEtiq
      ]);
      MyObjects.frxMostra(frxDiversos, 'Minha etiqueta');
    end;
end;

procedure TFmMaladireta.ImprimeEnderecosEmEtiquetas;
var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  L, T, W, H, i: Integer;
  Fator: Double;
begin
  // Configura p�gina
  while frxEnderecos.PagesCount > 0 do
    frxEnderecos.Pages[0].Free;
  Page := TfrxReportPage.Create(frxEnderecos);
  Page.CreateUniqueName;
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  //
  Page.PaperSize    := DMPAPER_USER;
  Page.PaperHeight  := QrPagePagHei.Value;
  Page.PaperWidth   := QrPagePagWid.Value;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  H := Trunc(QrPagePagTop.Value / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  // Configura Banda
  BANLEF := Trunc(QrPageBanLef.Value / VAR_DOTIMP);
  BANTOP := Trunc(QrPageBanTop.Value / VAR_DOTIMP);
  BANWID := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  BANHEI := Trunc(QrPageBanHei.Value / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsEnderecos;
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  //BA2HEI := Trunc(QrPageBa2Hei.Value / VAR_DOTIMP / (QrPageEspaco.Value + 1));
  for i := 1 to 5 do
  begin
    // Configura memo da Linha 1
    // Left:
    Fator := Trunc(FContaCol / Imp_LinhasEtiqHei);
    MEMLEF := Trunc((QrPageBanLef.Value / VAR_DOTIMP) + (Fator *
       ((QrPageColWid.Value / VAR_DOTIMP)+(QrPageBanGap.Value / VAR_DOTIMP))));
    FContaCol := FContaCol + 1;
    if FContaCol >= (QrPageBanCol.Value * Imp_LinhasEtiqHei) then FContaCol := 0;
    // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
    MEMTOP := Trunc(((i-1) * QrPageMemHei.Value) / VAR_DOTIMP);//BANTOP + BA2HEI + Trunc((i-1) * QrPageMemHei.Value);
    // Width := QrPageColWid.Value / VAR_DOTIMP;
    MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
    // Height:= QrPageMemHei.Value / VAR_DOTIMP;
    MEMHEI := Trunc(QrPageMemHei.Value / VAR_DOTIMP);
    //
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    //
    //Align := (QrItensASet_TAli.Value * 100) + QrItensASet_BAli.Value;
    //MyObjects.frxMemoAlignment(Memo, Align);
    //
    Memo.Font.Name := 'Univers Light Condensed';
    Memo.Font.Size := QrPageFoSize.Value;
    Memo.Memo.Text := '[frxDsEnderecos."Linha' + FormatFloat('0', i) + '"]';
    //
    if CkGrade.Checked then
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    //
  end;
  //////////////////////////////////////////////////////////////////////////////
  // Imprime ou mosrea design do relat�rio
  if CkDesign.Checked then
    frxEnderecos.DesignReport
  else
    if RGImpressao.ItemIndex = 0 then
      MyObjects.frxImprime(frxEnderecos, 'Minha etiqueta')
    else
    begin
      MyObjects.frxDefineDatasets(frxEnderecos, [
        DmodG.frxDsDono,
        frxDsEnderecos
      ]);
      MyObjects.frxMostra(frxEnderecos, 'Minha etiqueta');
    end;
end;

procedure TFmMaladireta.MostraFormulariosCondominos;
begin
 {$IFDEF TEM_UH}
  Application.CreateForm(TFmFormulariosCond, FmFormulariosCond);
  //VAR_LOCATE001 := RGEndereco.ItemIndex;
  FmFormulariosCond.ShowModal;
  FmFormulariosCond.Destroy;
  ReopenEnti;
  {$ELSE}
  Geral.MB_Aviso(
  'Esta aplica��o n�o esta habilitada para cadastro de cond�minos');
  {$ENDIF}
end;

procedure TFmMaladireta.MostraFormulariosEntidades;
begin
  Application.CreateForm(TFmFormulariosEntidades, FmFormulariosEntidades);
  //VAR_LOCATE001 := RGEndereco.ItemIndex;
  FmFormulariosEntidades.ShowModal;
  FmFormulariosEntidades.Destroy;
  ReopenEnti;
end;

procedure TFmMaladireta.frxDiversosGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'MINHAETIQUETA' then
  begin
    Value := Imp_Espacos + QrMinhaEtiqNome.Value;
    if Imp_LinhasEtiq > 1 then Value := Value + sLineBreak +
    Imp_Espacos + QrMinhaEtiqTexto1.Value;
    if Imp_LinhasEtiq > 2 then Value := Value + sLineBreak +
    Imp_Espacos + QrMinhaEtiqTexto2.Value;
    if Imp_LinhasEtiq > 3 then Value := Value + sLineBreak +
    Imp_Espacos + QrMinhaEtiqTexto3.Value;
    if Imp_LinhasEtiq > 4 then Value := Value + sLineBreak +
    Imp_Espacos + QrMinhaEtiqTexto4.Value;
  end
end;

(*
procedure TFmMaladireta.ImprimeMinhaEtiqueta2();
var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  BARLEF, BARTOP, BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  BarCode: TfrxBarCodeView;
  //Align, i,
  L, T, W, H: Integer;
  Fator: Double;
begin
  {
  // Configura p�gina
  while frxEtiquetas.PagesCount > 0 do
    frxEtiquetas.Pages[0].Free;
  Page := TfrxReportPage.Create(frxEtiquetas);
  Page.CreateUniqueName;
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  Page.Height       := Trunc(QrPagePagHei.Value / VAR_DOTIMP);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  H := Trunc(QrPagePagTop.Value / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  // Configura Banda
  BANLEF := Trunc(QrPageBanLef.Value / VAR_DOTIMP);
  BANTOP := Trunc(QrPageBanTop.Value / VAR_DOTIMP);
  BANWID := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  BANHEI := Trunc(QrPageBanHei.Value / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  //BA2HEI := Trunc(QrPageBa2Hei.Value / VAR_DOTIMP / (QrPageEspaco.Value + 1));
  //for i := 1 to 5 do
  //begin
    // Configura memo da Linha 1

  // Left:
  // Left:
  Fator := Trunc(FContaCol / Imp_LinhasEtiqHei);
  MEMLEF := Trunc((QrPageBanLef.Value / VAR_DOTIMP) + (Fator *
     ((QrPageColWid.Value / VAR_DOTIMP)+(QrPageBanGap.Value / VAR_DOTIMP))));
  if FContaCol >= (QrPageBanCol.Value * Imp_LinhasEtiqHei) then FContaCol := 0;
  BARLEF := MEMLEF + Trunc(QrPageBarLef.Value / VAR_DOTIMP);
  FContaCol := FContaCol + 1;
  if FContaCol >= (QrPageBanCol.Value * Imp_LinhasEtiqHei) then FContaCol := 0;

  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  BARHEI := Trunc(QrPageBarHei.Value / VAR_DOTIMP);

  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  BARTOP := Trunc(QrPageBarTop.Value / VAR_DOTIMP);

  // Width := QrPageColWid.Value / VAR_DOTIMP;
  //BARWID := Trunc(QrPageBarWid.Value / VAR_DOTIMP);
  //
  BarCode               := TfrxBarCodeView.Create(Band);
  BarCode.Name          := 'BarCode';
  BarCode.Visible       := True;
  BarCode.OnBeforePrint := 'BarCodeOnBeforePrint';
  BarCode.Left          := BARLEF;
  BarCode.Top           := BARTOP;
  BarCode.Height        := BARHEI;
  BarCode.Font.Size     := QrPageFoSize.Value;
  BarCode.BarType       := bcCodeEan13;
  BarCode.Expression    := '<VARF_BARCODE_BARC>';
  if CkGrade.Checked then
    BarCode.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //



  // MEMO 1 - Nome do produto
  //
  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  MEMHEI := Trunc(QrPageMemHei.Value / VAR_DOTIMP);
  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  MEMTOP := BARTOP + BARHEI;//Trunc(((i-1) * QrPageMemHei.Value) / VAR_DOTIMP);
  // Width := QrPageColWid.Value / VAR_DOTIMP;
  MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  //
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_BARCODE_NOME]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //



  // MEMO 2 - Pre�o do produto
  //
  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  MEMHEI := Trunc(QrPageMemHei.Value / VAR_DOTIMP);
  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  MEMTOP := MEMTOP + MEMHEI;//Trunc(((i-1) * QrPageMemHei.Value) / VAR_DOTIMP);
  // Width := QrPageColWid.Value / VAR_DOTIMP;
  MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  //
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_BARCODE_VALR]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //



  //////////////////////////////////////////////////////////////////////////////
  if CkDesign.Checked then
    frxEtiquetas.DesignReport
  else
    MyObjects.frxDefineDatasets(frx, [
    /
    ]);
    MyObjects.frxMostra(frxEtiquetas, 'Etiquetas de C�digo de Barras');
  }
end;
*)

end.

