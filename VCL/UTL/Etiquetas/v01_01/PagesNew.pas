unit PagesNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UMySQLModule, Mask, ComCtrls, dmkEdit, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmPagesNew = class(TForm)
    PainelDados: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label18: TLabel;
    EdPagCol: TdmkEdit;
    EdPagLin: TdmkEdit;
    EdPagLef: TdmkEdit;
    EdPagTop: TdmkEdit;
    EdPagWid: TdmkEdit;
    EdPagHei: TdmkEdit;
    EdPagGap: TdmkEdit;
    EdBanWid: TdmkEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label19: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdBanCol: TdmkEdit;
    EdColWid: TdmkEdit;
    EdBanLin: TdmkEdit;
    EdBanHei: TdmkEdit;
    EdBanLef: TdmkEdit;
    EdBanTop: TdmkEdit;
    EdBanGap: TdmkEdit;
    GroupBox3: TGroupBox;
    Bevel1: TBevel;
    ImageP: TImage;
    ImageL: TImage;
    Label13: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    RGOrientation: TRadioGroup;
    EdBa2Hei: TdmkEdit;
    EdFoSize: TdmkEdit;
    EdMemHei: TdmkEdit;
    RGLinhaNula: TRadioGroup;
    Panel1: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    EdDescricao: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Memo1: TMemo;
    TabSheet2: TTabSheet;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    EdBarLef: TdmkEdit;
    EdBarTop: TdmkEdit;
    EdBarWid: TdmkEdit;
    EdBarHei: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPagColExit(Sender: TObject);
    procedure EdPagLinExit(Sender: TObject);
    procedure EdPagLefExit(Sender: TObject);
    procedure EdPagTopExit(Sender: TObject);
    procedure EdPagWidExit(Sender: TObject);
    procedure EdPagGapExit(Sender: TObject);
    procedure EdBanColExit(Sender: TObject);
    procedure EdBanHeiExit(Sender: TObject);
    procedure EdBanLefExit(Sender: TObject);
    procedure EdBanTopExit(Sender: TObject);
    procedure EdBanGapExit(Sender: TObject);
    procedure RGOrientationClick(Sender: TObject);
    procedure EdPagHeiExit(Sender: TObject);
    procedure EdBanWidExit(Sender: TObject);
    procedure EdColWidExit(Sender: TObject);
    procedure EdBanLinExit(Sender: TObject);
    procedure EdBa2HeiExit(Sender: TObject);
    procedure EdFoSizeExit(Sender: TObject);
    procedure EdMemHeiExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPagesNew: TFmPagesNew;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmPagesNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  case RGOrientation.ItemIndex of
    0: ImageL.Visible := True;
    1: ImageP.Visible := True;
  end;
  case RGOrientation.ItemIndex of
    0: ImageP.Visible := False;
    1: ImageL.Visible := False;
  end;
end;

procedure TFmPagesNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPagesNew.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Pages', Geral.IMV(EdCodigo.Text));
  Close;
end;

procedure TFmPagesNew.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := StrToInt(EdCodigo.Text);
  if Trim(EdDescricao.Text) = '' then begin
    ShowMessage('Informe uma descriação!');
    EdDescricao.SetFocus;
    Exit;
  end;
  {
  Dmod.QrUpdM.SQL.Clear;
  if ImgTipo.SQLType = stIns then begin
    Dmod.QrUpdM.SQL.Add('INSERT INTO pages SET ');
    Dmod.QrUpdM.SQL.Add('Nome=:P0, Observa=:P1, ');
    Dmod.QrUpdM.SQL.Add('PagCol=:P2,  PagLin=:P3,  PagLef=:P4,  PagTop=:P5,');
    Dmod.QrUpdM.SQL.Add('PagWid=:P6,  PagHei=:P7,  PagGap=:P8,  BanCol=:P9,');
    Dmod.QrUpdM.SQL.Add('BanLin=:P10, BanLef=:P11, BanTop=:P12, BanWid=:P13,');
    Dmod.QrUpdM.SQL.Add('BanHei=:P14, BanGap=:P15, Orient=:P16, Ba2Hei=:P17,');
    Dmod.QrUpdM.SQL.Add('ColWid=:P18, FoSize=:P19, MemHei=:P20, Espaco=:P21,');
    Dmod.QrUpdM.SQL.Add('');
    Dmod.QrUpdM.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz ');
  end else begin
    Dmod.QrUpdM.SQL.Add('UPDATE pages SET ');
    Dmod.QrUpdM.SQL.Add('Nome=:P0, Observa=:P1, ');
    Dmod.QrUpdM.SQL.Add('PagCol=:P2,  PagLin=:P3,  PagLef=:P4,  PagTop=:P5,');
    Dmod.QrUpdM.SQL.Add('PagWid=:P6,  PagHei=:P7,  PagGap=:P8,  BanCol=:P9,');
    Dmod.QrUpdM.SQL.Add('BanLin=:P10, BanLef=:P11, BanTop=:P12, BanWid=:P13,');
    Dmod.QrUpdM.SQL.Add('BanHei=:P14, BanGap=:P15, Orient=:P16, Ba2Hei=:P17,');
    Dmod.QrUpdM.SQL.Add('ColWid=:P18, FoSize=:P19, MemHei=:P20, Espaco=:P21,');
    Dmod.QrUpdM.SQL.Add('');
    Dmod.QrUpdM.SQL.Add('DataCad=:Px, UserCad=:Py WHERE Codigo=:Pz ');
  end;
  }
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pages', False, [
    'Nome', 'Observa',
    'PagCol', 'PagLin', 'PagLef',
    'PagTop', 'PagWid', 'PagHei',
    'PagGap', 'BanCol', 'BanLin',
    'BanLef', 'BanTop', 'BanWid',
    'BanHei', 'BanGap', 'Orient',
    'Ba2Hei', 'ColWid', 'FoSize',
    'MemHei', 'Espaco', 'BarLef',
    'BarTop', 'BarWid', 'BarHei'
  ], ['Codigo'], [
    EdDescricao.Text, Memo1.Text,
    Geral.IMV(EdPagCol.Text), Geral.IMV(EdPagLin.Text), Geral.IMV(EdPagLef.Text),
    Geral.IMV(EdPagTop.Text), Geral.IMV(EdPagWid.Text), Geral.IMV(EdPagHei.Text),
    Geral.IMV(EdPagGap.Text), Geral.IMV(EdBanCol.Text), Geral.IMV(EdBanLin.Text),
    Geral.IMV(EdBanLef.Text), Geral.IMV(EdBanTop.Text), Geral.IMV(EdBanWid.Text),
    Geral.IMV(EdBanHei.Text), Geral.IMV(EdBanGap.Text), RGOrientation.ItemIndex,
    Geral.IMV(EdBa2Hei.Text), Geral.IMV(EdColWid.Text), Geral.IMV(EdFoSize.Text),
    Geral.IMV(EdMemHei.Text), RGLinhaNula.ItemIndex      , Geral.IMV(EdBarLef.Text),
    Geral.IMV(EdBarTop.Text), Geral.IMV(EdBarWid.Text), Geral.IMV(EdBarHei.Text)
  ], [Codigo], True) then Close;

  {
  Dmod.QrUpdM.Params[00].AsString := EdDescricao.Text;
  Dmod.QrUpdM.Params[01].AsString := Memo1.Text;

  Dmod.QrUpdM.Params[02].AsInteger := Geral.IMV(EdPagCol.Text);
  Dmod.QrUpdM.Params[03].AsInteger := Geral.IMV(EdPagLin.Text);
  Dmod.QrUpdM.Params[04].AsInteger := Geral.IMV(EdPagLef.Text);
  Dmod.QrUpdM.Params[05].AsInteger := Geral.IMV(EdPagTop.Text);
  Dmod.QrUpdM.Params[06].AsInteger := Geral.IMV(EdPagWid.Text);
  Dmod.QrUpdM.Params[07].AsInteger := Geral.IMV(EdPagHei.Text);
  Dmod.QrUpdM.Params[08].AsInteger := Geral.IMV(EdPagGap.Text);
  Dmod.QrUpdM.Params[09].AsInteger := Geral.IMV(EdBanCol.Text);
  Dmod.QrUpdM.Params[10].AsInteger := Geral.IMV(EdBanLin.Text);
  Dmod.QrUpdM.Params[11].AsInteger := Geral.IMV(EdBanLef.Text);
  Dmod.QrUpdM.Params[12].AsInteger := Geral.IMV(EdBanTop.Text);
  Dmod.QrUpdM.Params[13].AsInteger := Geral.IMV(EdBanWid.Text);
  Dmod.QrUpdM.Params[14].AsInteger := Geral.IMV(EdBanHei.Text);
  Dmod.QrUpdM.Params[15].AsInteger := Geral.IMV(EdBanGap.Text);
  Dmod.QrUpdM.Params[16].AsInteger := RGOrientation.ItemIndex;
  Dmod.QrUpdM.Params[17].AsInteger := Geral.IMV(EdBa2Hei.Text);
  Dmod.QrUpdM.Params[18].AsInteger := Geral.IMV(EdColWid.Text);
  Dmod.QrUpdM.Params[19].AsInteger := Geral.IMV(EdFoSize.Text);
  Dmod.QrUpdM.Params[20].AsInteger := Geral.IMV(EdMemHei.Text);
  Dmod.QrUpdM.Params[21].AsInteger := RGLinhaNula.ItemIndex;
  Dmod.QrUpdM.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdM.Params[23].AsInteger := VAR_USUARIO;
  Dmod.QrUpdM.Params[24].AsInteger := Codigo;
  Dmod.QrUpdM.ExecSQL;
  Close;
  }
end;

procedure TFmPagesNew.EdPagColExit(Sender: TObject);
begin
  EdPagCol.Text := Geral.TFT_MinMax(EdPagCol.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdPagLinExit(Sender: TObject);
begin
  EdPagLin.Text := Geral.TFT_MinMax(EdPagLin.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdPagLefExit(Sender: TObject);
begin
  EdPagLef.Text := Geral.TFT_MinMax(EdPagLef.Text, -300, 10000, 0);
end;

procedure TFmPagesNew.EdPagTopExit(Sender: TObject);
begin
  EdPagTop.Text := Geral.TFT_MinMax(EdPagTop.Text, -300, 10000, 0);
end;

procedure TFmPagesNew.EdPagWidExit(Sender: TObject);
begin
  EdPagWid.Text := Geral.TFT_MinMax(EdPagWid.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdPagGapExit(Sender: TObject);
begin
  EdPagGap.Text := Geral.TFT_MinMax(EdPagGap.Text, 0, 10000, 0);
end;

procedure TFmPagesNew.EdBanColExit(Sender: TObject);
begin
  EdBanCol.Text := Geral.TFT_MinMax(EdBanCol.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdBanHeiExit(Sender: TObject);
begin
  EdBanHei.Text := Geral.TFT_MinMax(EdBanHei.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdBanLefExit(Sender: TObject);
begin
  EdBanLef.Text := Geral.TFT_MinMax(EdBanLef.Text, -300, 10000, 0);
end;

procedure TFmPagesNew.EdBanTopExit(Sender: TObject);
begin
  EdBanTop.Text := Geral.TFT_MinMax(EdBanTop.Text, -300, 10000, 0);
end;

procedure TFmPagesNew.EdBanGapExit(Sender: TObject);
begin
  EdBanGap.Text := Geral.TFT_MinMax(EdBanGap.Text, 0, 10000, 0);
end;

procedure TFmPagesNew.RGOrientationClick(Sender: TObject);
begin
  case RGOrientation.ItemIndex of
    0: ImageL.Visible := True;
    1: ImageP.Visible := True;
  end;
  case RGOrientation.ItemIndex of
    0: ImageP.Visible := False;
    1: ImageL.Visible := False;
  end;
end;

procedure TFmPagesNew.EdPagHeiExit(Sender: TObject);
begin
  EdPagHei.Text := Geral.TFT_MinMax(EdPagHei.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdBanWidExit(Sender: TObject);
begin
  EdBanWid.Text := Geral.TFT_MinMax(EdBanWid.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdColWidExit(Sender: TObject);
begin
  EdColWid.Text := Geral.TFT_MinMax(EdColWid.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdBanLinExit(Sender: TObject);
begin
  EdBanLin.Text := Geral.TFT_MinMax(EdBanLin.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdBa2HeiExit(Sender: TObject);
begin
  EdBa2Hei.Text := Geral.TFT_MinMax(EdBa2Hei.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdFoSizeExit(Sender: TObject);
begin
  EdFoSize.Text := Geral.TFT_MinMax(EdFoSize.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.EdMemHeiExit(Sender: TObject);
begin
  EdMemHei.Text := Geral.TFT_MinMax(EdMemHei.Text, 1, 10000, 0);
end;

procedure TFmPagesNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
