object FmVerifiDB_SQLite: TFmVerifiDB_SQLite
  Left = 339
  Top = 185
  Caption = 'FER-VRFBD-006 :: Verifica'#231#227'o de Banco de Dados'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 380
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 380
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 380
        Height = 32
        Caption = 'Verifica'#231#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 451
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 451
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 101
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaAvisoP1: TLabel
            Left = 14
            Top = 80
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 808
            Height = 77
            Align = alTop
            Caption = ' Avisos: '
            TabOrder = 0
            object Panel13: TPanel
              Left = 2
              Top = 15
              Width = 804
              Height = 60
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 678
                Height = 60
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object LaAvisoG1: TLabel
                  Left = 12
                  Top = 33
                  Width = 120
                  Height = 17
                  Caption = '..............................'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGreen
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaAvisoB1: TLabel
                  Left = 12
                  Top = 17
                  Width = 120
                  Height = 17
                  Caption = '..............................'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
                object LaAvisoR1: TLabel
                  Left = 12
                  Top = 1
                  Width = 120
                  Height = 17
                  Caption = '..............................'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -15
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                  Transparent = True
                end
              end
              object Panel14: TPanel
                Left = 790
                Top = 0
                Width = 14
                Height = 60
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
              end
              object Panel6: TPanel
                Left = 722
                Top = 0
                Width = 68
                Height = 60
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 2
                object LaTempoI: TLabel
                  Left = 0
                  Top = 0
                  Width = 68
                  Height = 14
                  Align = alTop
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = '...'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object LaTempoF: TLabel
                  Left = 0
                  Top = 14
                  Width = 68
                  Height = 14
                  Align = alTop
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = '...'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGreen
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object LaTempoT: TLabel
                  Left = 0
                  Top = 28
                  Width = 68
                  Height = 14
                  Align = alTop
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = '...'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentColor = False
                  ParentFont = False
                  Transparent = True
                end
              end
              object Panel8: TPanel
                Left = 678
                Top = 0
                Width = 44
                Height = 60
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 3
                object Label4: TLabel
                  Left = 0
                  Top = 0
                  Width = 44
                  Height = 14
                  Align = alTop
                  AutoSize = False
                  Caption = 'In'#237'cio:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                end
                object Label5: TLabel
                  Left = 0
                  Top = 14
                  Width = 44
                  Height = 14
                  Align = alTop
                  AutoSize = False
                  Caption = 'T'#233'rmino:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentFont = False
                end
                object Label6: TLabel
                  Left = 0
                  Top = 28
                  Width = 44
                  Height = 14
                  Align = alTop
                  AutoSize = False
                  Caption = 'Tempo:'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  Transparent = True
                end
              end
            end
          end
        end
        object Panel11: TPanel
          Left = 625
          Top = 116
          Width = 185
          Height = 333
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object GridPanel1: TGridPanel
            Left = 0
            Top = 0
            Width = 185
            Height = 333
            Align = alClient
            Alignment = taLeftJustify
            ColumnCollection = <
              item
                Value = 100.000000000000000000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = MeFldsNoNeed
                Row = 0
              end
              item
                Column = 0
                Control = MeTabsNoNeed
                Row = 1
              end>
            RowCollection = <
              item
                Value = 50.000000000000000000
              end
              item
                Value = 50.000000000000000000
              end>
            TabOrder = 0
            object MeFldsNoNeed: TMemo
              Left = 1
              Top = 1
              Width = 183
              Height = 165
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
            object MeTabsNoNeed: TMemo
              Left = 1
              Top = 166
              Width = 183
              Height = 166
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
            end
          end
        end
        object Panel16: TPanel
          Left = 2
          Top = 116
          Width = 623
          Height = 333
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Splitter1: TSplitter
            Left = 0
            Top = 291
            Width = 623
            Height = 5
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = 265
          end
          object Memo1: TMemo
            Left = 0
            Top = 0
            Width = 623
            Height = 291
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
          object Memo3: TMemo
            Left = 0
            Top = 296
            Width = 623
            Height = 37
            Align = alBottom
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            Lines.Strings = (
              'Lista InsUpd')
            ParentFont = False
            TabOrder = 1
          end
        end
      end
    end
    object PB1: TProgressBar
      Left = 0
      Top = 451
      Width = 812
      Height = 16
      Align = alBottom
      TabOrder = 1
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 125
        Left = 10
        Top = 3
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Atualiza'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtCriaListaSQL: TButton
        Left = 168
        Top = 12
        Width = 137
        Height = 25
        Caption = 'Cria lista InsUpd da tabela: '
        TabOrder = 1
        OnClick = BtCriaListaSQLClick
      end
      object EdCriaListaSQL: TEdit
        Left = 308
        Top = 16
        Width = 173
        Height = 21
        TabOrder = 2
        Text = 'controle'
      end
      object BtExcluiImportar: TButton
        Left = 492
        Top = 12
        Width = 137
        Height = 25
        Caption = 'Exclui tabelas a importar: '
        TabOrder = 3
        OnClick = BtExcluiImportarClick
      end
    end
  end
  object QrCampos: TZQuery
    Connection = DMod.MyDB
    SQL.Strings = (
      'PRAGMA table_info(controle);')
    Params = <>
    Left = 420
    Top = 220
    object QrCamposcid: TLargeintField
      FieldName = 'cid'
    end
    object QrCamposname: TWideMemoField
      FieldName = 'name'
      BlobType = ftWideMemo
    end
    object QrCampostype: TWideMemoField
      FieldName = 'type'
      BlobType = ftWideMemo
    end
    object QrCamposnotnull: TLargeintField
      FieldName = 'notnull'
    end
    object QrCamposdflt_value: TWideMemoField
      FieldName = 'dflt_value'
      BlobType = ftWideMemo
    end
    object QrCampospk: TLargeintField
      FieldName = 'pk'
    end
  end
end
