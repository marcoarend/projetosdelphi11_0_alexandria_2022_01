unit VerifiDB_SQLite;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Buttons, DB, Grids, DBGrids, DBCtrls,
  ZAbstractConnection, ZConnection, ZCompatibility, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, dmkGeral, dmkLabel, dmkImage, UnInternalConsts,
  UnDmkEnums, Vcl.CheckLst;

type
  TFmVerifiDB_SQLite = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    BtConfirma: TBitBtn;
    Panel5: TPanel;
    LaAvisoP1: TLabel;
    GroupBox2: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    LaAvisoG1: TLabel;
    LaAvisoB1: TLabel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Panel11: TPanel;
    Panel16: TPanel;
    Memo1: TMemo;
    GridPanel1: TGridPanel;
    MeFldsNoNeed: TMemo;
    MeTabsNoNeed: TMemo;
    LaAvisoR1: TLabel;
    PB1: TProgressBar;
    BtCriaListaSQL: TButton;
    EdCriaListaSQL: TEdit;
    Memo3: TMemo;
    QrCampos: TZQuery;
    QrCamposcid: TLargeintField;
    QrCamposname: TWideMemoField;
    QrCampostype: TWideMemoField;
    QrCamposnotnull: TLargeintField;
    QrCamposdflt_value: TWideMemoField;
    QrCampospk: TLargeintField;
    Splitter1: TSplitter;
    BtExcluiImportar: TButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtCriaListaSQLClick(Sender: TObject);
    procedure BtExcluiImportarClick(Sender: TObject);
  private
    { Private declarations }
    FVerificou: Boolean;
    //
    procedure CriaListaSQL();
  public
    { Public declarations }
    FVerifica: Boolean;
    FDataBaseAll, FDataBaseUsu: TZConnection;
    procedure VerificaBD();
  end;

  var
  FmVerifiDB_SQLite: TFmVerifiDB_SQLite;

implementation

uses UnMyObjects, Module, UnGrl_DmkDB, MyListas, UnVCL_ZDB;

{$R *.DFM}

procedure TFmVerifiDB_SQLite.BtConfirmaClick(Sender: TObject);
begin
  VerificaBD();
end;

procedure TFmVerifiDB_SQLite.BtCriaListaSQLClick(Sender: TObject);
  const
    FNaoCopiar: array[0..6] of String =
      ('LK', 'DATACAD', 'DATAALT', 'USERCAD', 'USERALT', 'ALTERWEB', 'ATIVO');
  function Posicao(Campo: String; NaoCopiar: array of String): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := 0 to 6 do
      if NaoCopiar[i] = Campo then
      begin
        Result := i + 1;
        Exit;
      end;
  end;
  //
var
  Txt_00, Txt_01, Txt_02, Txt_03, Txt_04, Txt_11, Txt_12: String;
  P, Conta: Integer;
  Campo, DefVarStr, DefVarInt, DefVarDbl: String;
  //
  Tabela: String;
begin
{
SELECT * FROM sqlite_master
WHERE type='table'
ORDER BY name;
}
  Self.WindowState := wsMaximized;
  Memo3.Height := Memo3.Height + Memo1.Height - 80;
  //
  DefVarStr := '';
  DefVarInt := '';
  DefVarDbl := '';
  Txt_00 := '';
  //
  Memo3.Lines.Clear;
  Tabela := (*QrTabelas.Fields[0].AsString*) EdCriaListaSQL.Text;
  //Txt_11 := '? := UMyMod.BuscaEmLivreY_Def(''' + Tabela + ''', ';
  //Txt_11 := '? := Grl_DmkDB.GetNxtCodigoInt(Dmod.QrAux, Dmod.MyDB, ''' + Tabela + ''', ';
  Txt_11 := '? := Grl_DmkDB.GetNxtCodigoInt(''' + Tabela + ''', ';
  //
  Txt_12 := 'ou > ? := UMyMod.BPGS1I32(''' + Tabela + ''', ';//
  //
(*
  Txt_01 := 'if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, ''' + Tabela +
  ''', auto_increment?[' +  #13#10;
*)
  Txt_01 := 'if Grl_DmkDB.SQLInsUpd(Dmod.QrUpd, Dmod.MyDB, SQLType, ''' + Tabela +
  ''', auto_increment?[' +  #13#10;
  Txt_02 := '';
  Txt_03 :=  #13#10;
  Txt_04 := '';
  Conta := -1;
  //////////////////////////////////////////////////////////////////////////////
  Grl_DmkDB.AbreSQLQuery0(QrCampos, Dmod.MyDB, [
  'PRAGMA table_info(' + Tabela + ')',
  '']);
(*
cid-name-type---------------------notnull-dflt_value-----------pk
seq-nome-tipo---------------------naonulo-valordefault---------primarykey
0  "Codigo"	          "INT(11)"      	"1"  	"0"	               "1"
1  "Versao"	          "BIGINT(20)"	  "1"	  "0"	               "0"
2  "DeviceHost"	      "varchar(255)"	"0"		null               "0"
3  "URLBaseAPI"	      "varchar(255)"	"0"		null               "0"
4  "DeviceAssembler"	"int(11)"	      "1"	  "0"	               "0"
5  "Lk"	              "int(11)"	      "0"	  "0"	               "0"
6  "DataCad"	        "date"        	"0"		null               "0"
7  "DataAlt"        	"date"        	"0"		null               "0"
8  "UserCad"        	"int(11)"	      "0"	  "0"	               "0"
9  "UserAlt"        	"int(11)"     	"0"	  "0"	               "0"
10 "AlterWeb"       	"tinyint(1)"	  "1"	  "1"	               "0"
11 "Ativo"	          "tinyint(1)"   	"1"	  "1"	               "0"
12 "DeviceMACAdress"	"varchar(255)"	"1"	  "'??-??-??-??-??-??'"	"0"
13 "DeviceMACAddress"	"varchar(255)"	"1"	  "'??-??-??-??-??-??'"	"0"  //////////////////////////////////////////////////////////////////////////////
*)
  QrCampos.First;
  while not QrCampos.Eof do
  begin
    //if Posicao(Uppercase(String(QrCampos.FieldByName('Field').AsString)), FNaoCopiar) = 0 then
    if Posicao(Uppercase(String(QrCampos.FieldByName('name').AsString)), FNaoCopiar) = 0 then
    begin
      Campo := Lowercase(String(QrCampos.FieldByName('type').AsString));
      P := pos('(', Campo);
      if P > 0 then
        Campo := Copy(Campo, 1, P-1);

      if (Campo = 'date') or (Campo = 'datetime') or (Campo = 'time')
      or (Campo = 'year') or (Campo = 'timestamp') or (Campo = 'char')
      or (Campo = 'varchar') or (Campo = 'text') or (Campo = 'tinytext')
      or (Campo = 'mediumtext') or (Campo = 'longblob') or (Campo = 'blob')
      or (Campo = 'tinyblob') or (Campo = 'mediumblob') or (Campo = 'longblob')
      or (Campo = 'bynary') or (Campo = 'varbinary') or (Campo = 'enum')
      or (Campo = 'set') then
      begin
        if DefVarStr <> '' then
          DefVarStr := DefVarStr + ', ';
        DefVarStr := DefVarStr + String(QrCampos.FieldByName('name').AsString);
      end else
      if (Campo = 'int') or (Campo = 'tinyint') or (Campo = 'mediumint')
      or (Campo = 'longint') or (Campo = 'integer') or (Campo = 'smallint') then
      begin
        if DefVarInt <> '' then
          DefVarInt := DefVarInt + ', ';
        DefVarInt := DefVarInt + String(QrCampos.FieldByName('name').AsString);
      end else
      if (Campo = 'bigint') or (Campo = 'double') or (Campo = 'decimal')
      or (Campo = 'numeric') or (Campo = 'float') or (Campo = 'real')
      or (Campo = 'double precision') or (Campo = 'dec') or (Campo = 'fixed') then
      begin
        if DefVarDbl <> '' then
          DefVarDbl := DefVarDbl + ', ';
        DefVarDbl := DefVarDbl + String(QrCampos.FieldByName('name').AsString);
      end;
      //
      Txt_00 := Txt_00 + '  ' + Geral.CompletaString(String(QrCampos.FieldByName('name').AsString), ' ',
        20, taLeftJustify, False) + ':= ;' + #13#10;
{
    end;
    //
    if Posicao(Uppercase(QrCampos.FieldByName('name').AsString), FNaoCopiar) = 0 then
    begin
}
      //if QrCampos.FieldByName('Key').AsString = 'PRI'  then
      if QrCampos.FieldByName('pk').AsString = '1'  then
      begin
        Txt_02 := Txt_02 + '''' + String(QrCampos.FieldByName('name').AsString) + ''', ';
        Txt_04 := Txt_04 + String(QrCampos.FieldByName('name').AsString) + ', ';
      end else begin
        Inc(Conta, 1);
        //
        if Conta = 3 then
        begin
          Txt_01  := Txt_01 + #13#10;
          Txt_03  := Txt_03 + #13#10;
          Conta := 0;
        end;
        Txt_01 := Txt_01 + '''' + String(QrCampos.FieldByName('name').AsString) + ''', ';
        Txt_03 := Txt_03 + String(QrCampos.FieldByName('name').AsString) + ', ';
      end;
    end;
    QrCampos.Next;
  end;
  Txt_01 := Copy(Txt_01, 1, Length(Txt_01) - 2) + '], [';
  if Length(Txt_02) > 1 then Txt_02 := Copy(Txt_02, 1, Length(Txt_02) - 2);
  Txt_03 := Copy(Txt_03, 1, Length(Txt_03) - 2) + '], [';
  if Length(Txt_04) > 0 then Txt_04 := Copy(Txt_04, 1, Length(Txt_04) - 2);
  //
  Memo3.Lines.Add('var');
  if DefVarStr <> '' then
    Memo3.Lines.Add('  ' + DefVarStr + ': String;');
  if DefVarInt <> '' then
    Memo3.Lines.Add('  ' + DefVarInt + ': Integer;');
  if DefVarDbl <> '' then
    Memo3.Lines.Add('  ' + DefVarDbl + ': Double;');
  //
  Memo3.Lines.Add('  SQLType: TSQLType;');
  //
  Memo3.Lines.Add('begin');
  //Memo3.Lines.Add('  SQLType := ImgTipo.SQLType?;');
  Memo3.Lines.Add('  ' + Geral.CompletaString('SQLType', ' ',
        20, taLeftJustify, False) + ':= ImgTipo.SQLType?;');
  Memo3.Lines.Add('  //');
  Memo3.Lines.Add(Txt_00);
  Memo3.Lines.Add('  //');
  //
  Memo3.Lines.Add(Txt_11 + Txt_02 + ', SQLType, CodAtual?);');
  Memo3.Lines.Add(Txt_12 + Txt_02 + ', '''', '''', tsPosNeg?, stInsUpd?, CodAtual?);');
  Memo3.Lines.Add(Txt_01  + #13#10 + Txt_02 + '], [' + Txt_03  +
    #13#10 + Txt_04 + '], UserDataAlterweb?, TdmkSQLInsert.dmksqlins??' +
    ', ComplUpd?: String; Device: TDeviceType.st??, Sincro?False?');
end;

procedure TFmVerifiDB_SQLite.BtExcluiImportarClick(Sender: TObject);
var
  Qry: TZQuery;
  //
  procedure ExcluiTabela(Tabela: String);
  begin
    Dmod.MyDB.Disconnect;
    Dmod.MyDB.Connect;
    Grl_DmkDB.ExecutaSQLQuery0(Qry, Dmod.MyDB, [
    'DROP TABLE ' + Tabela,
    '']);
  end;
begin
  Qry := TZQuery.Create(Dmod);
  try
    ExcluiTabela('EmbalagensCad');
    ExcluiTabela('EmbalagensTar');
    ExcluiTabela('EntidadesAccn');
    ExcluiTabela('EntidadesEmpr');
    ExcluiTabela('EntidadesGrup');
    ExcluiTabela('EntidadesCptt');
    //ExcluiTabela('LoadSalesData');
    ExcluiTabela('ProdDivsaoCab');
    ExcluiTabela('ProdDivSubCab');
    ExcluiTabela('ProdLinhasCab');
    ExcluiTabela('ProdutosCabec');
    ExcluiTabela('ProdutosCadas');
    ExcluiTabela('ProdutosEmbal');
    ExcluiTabela('ProdutosFamil');
    ExcluiTabela('ProdutosTabls');
    ExcluiTabela('SalesOperacao');
    ExcluiTabela('SalesPerfilCl');
    ExcluiTabela('SegmenGruCads');
    ExcluiTabela('SegmentosCads');
    ExcluiTabela('SegmentosSGEA');
  finally
    Qry.Free;
  end;
end;

procedure TFmVerifiDB_SQLite.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDB_SQLite.CriaListaSQL;
begin

end;

procedure TFmVerifiDB_SQLite.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVerifiDB_SQLite.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if (not FVerificou) and (Dmod.QrControle.Active = False) then
    Application.Terminate;
end;

procedure TFmVerifiDB_SQLite.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  BringToFront;
  //
  FDataBaseUsu := nil;
  FDataBaseAll := nil;
  FVerifica    := False;
  FVerificou   := False;
  //
  LaAvisoR1.Caption := '...';
  LaAvisoG1.Caption := '...';
  LaAvisoB1.Caption := '...';
  LaAvisoP1.Caption := '...';
  LaTempoI.Caption  := '...';
  LaTempoF.Caption  := '...';
  LaTempoT.Caption  := '...';
  //
  Memo1.Text        := '';
  MeFldsNoNeed.Text := '';
  MeTabsNoNeed.Text := '';
end;

procedure TFmVerifiDB_SQLite.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDB_SQLite.VerificaBD;

  function VerificaDB(DataBase: TZConnection): Boolean;
  var
    DBNome: String;
  begin
    DBNome := CO_DBNome;
    //
    Result := Grl_DmkDB.EfetuaVerificacoes(DBNome, Memo1,
                True(*CkEstrutura.Checked*), True(*CkEstrutLoc.Checked*), True,
                False(*CkPergunta.Checked*), False(*CkRegObrigat.Checked*),
                LaAvisoP1, nil, LaAvisoR1, nil, LaAvisoB1, nil, LaAvisoG1, nil,
                LaTempoI, LaTempoF, LaTempoT, MeTabsNoNeed, PB1, MeFldsNoNeed,
                stDesktop, istSQLite, DataBase, CO_VERSAO);
  end;

var
  Versao, Resp: Integer;
  Continua, Verifica: Boolean;
begin
  Continua := False;

  if (FDataBaseAll = nil) and (FDataBaseUsu = nil) then
  begin
    Geral.MB_Erro('Banco de dados n�o definido!');
    Exit;
  end;

  Memo1.Lines.Clear;

  Verifica := True;
  Versao   := Grl_DmkDB.ObtemVersaoAppDB(Dmod.QrLoc, Dmod.MyDB, istSQLite, stDesktop);

  if Versao <= CO_VERSAO then
  begin
    Verifica := True;
  end else
  begin
    Resp := Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
              'banco de dados atual. N�o � recomendado executar a verifica��o com '+
              'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
              'Confirma assim mesmo a verifica��o?');

    if Resp = ID_YES then
      Verifica := True
  end;

  if Verifica then
  begin
    Application.ProcessMessages;

    if FDataBaseAll <> nil then
      Continua := VerificaDB(FDataBaseAll);

    if FDataBaseUsu <> nil then
      Continua := VerificaDB(FDataBaseUsu);

    if Continua = True then
    begin
      VCL_ZDB.ReopenControle(Dmod.MyDB, Dmod.QrControle);
      //
      FVerificou := True;
      //
      if Memo1.Text = '' then
        Close
      else
        Geral.MB_Aviso('Verifica��o foi realizada, mas existem mensagens!');
    end;
  end;
end;

end.
