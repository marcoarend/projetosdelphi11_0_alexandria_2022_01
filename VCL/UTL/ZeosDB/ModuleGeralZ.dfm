object DModGZ: TDModGZ
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 461
  Width = 690
  object QrSB5: TZQuery
    Params = <>
    Left = 188
    Top = 191
    object QrSB5Codigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSB5CodUsu: TLargeintField
      FieldName = 'CodUsu'
    end
    object QrSB5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB5: TDataSource
    DataSet = QrSB5
    Left = 248
    Top = 191
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 248
    Top = 143
  end
  object QrSB4: TZQuery
    Params = <>
    Left = 188
    Top = 143
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSB4CodUsu: TWideStringField
      FieldName = 'CodUsu'
      Size = 255
    end
  end
  object QrSB3: TZQuery
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Params = <>
    Left = 188
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrSB3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 248
    Top = 95
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 248
    Top = 47
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 248
    Top = 3
  end
  object QrSB: TZQuery
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome, 0 CodUsu'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    Left = 188
    Top = 3
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrSBNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSBCodUsu: TLargeintField
      FieldName = 'CodUsu'
    end
  end
  object QrSB2: TZQuery
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Params = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    Left = 188
    Top = 47
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrSB2CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrAux_: TZQuery
    Params = <>
    Left = 188
    Top = 243
    object LargeintField1: TLargeintField
      FieldName = 'Codigo'
    end
    object LargeintField2: TLargeintField
      FieldName = 'CodUsu'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrNTV: TZQuery
    Params = <>
    Left = 248
    Top = 244
  end
end
