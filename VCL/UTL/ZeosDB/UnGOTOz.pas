unit UnGOTOz;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls,
  UnInternalConsts2, Mask, ComCtrls, (*DBTables,*) ResIntStrings, DmkDAC_PF,
  (*DBIProcs,*) Registry, ZCF2, buttons, UnInternalConsts3, UnDmkProcFunc,
  Grids, DBGrids, TypInfo, Winsock, IniFiles, Variants, dmkGeral, UnDmkEnums,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection,
  ZConnection, UMySQLDB, UnGrl_DmkDB, UnMyObjects;

type
  TUnGOTOz = class(TObject)
  private
    { Private declarations }
    function  DefineBaseDados(): TZConnection;
    function  FiltroNeg(): String;
    function  LocalizaCodigo(Atual, Codigo: Integer): Boolean;
    function  LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
    //function  LocalizaTxtUsu(Atual, Codigo: String): Boolean; Fazer!!
    function  TxtLaRegistro(RecCount, Increm : Integer) : String;
    function  Vai(Para: TVaiPara; Atual: Integer; Sort: String) : String;
  public
    { Public declarations }
    procedure BotoesSb(LaTipo: String); overload;
    procedure BotoesSb(SQLType: TSQLType); overload;
    procedure BotoesSb(SQLType: TSQLType; Form: TForm); overload;
    function  Codigo(Atual: Integer; Padrao: String): String;
    function  CodUsu(Atual: Integer; Padrao: String): String;
    function  CriaForm(NomeDoCampoCodigo, NomeDoCampoNome,
              NomeDaTabela: String; DataBase: TZConnection; Extra: string;
              ReturnCodUsu: Boolean = False; LEFT_JOIN: String = ''): Integer;
    function  CriaLocalizador(Codigo: Integer): Boolean;
    function  CriaLocalizadorCodUsu(Codigo: Integer): Boolean;
    function  ExecutouPesq01(TextoAPesquisar, NomeDoCampoCodigo,
              NomeDoCampoNome, NomeDaTabela: String; DataBase: TZConnection;
              Extra, LEFT_JOIN: string; var Erro: String): Boolean;
    function  ExecutouPesq05(NomeDoCampoCodigo, NomeDoCampoNome1,
              NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4,
              NomeDaTabela, NomeDoCampoTipo:
              String; Tipo1: Integer; DataBase: TZConnection; Extra: string;
              SohNum: Boolean; var Erro: String): Boolean;
    function  Fechar(Caption: String) : TCloseAction; overload;
    function  Fechar(SQLType: TSQLType) : TCloseAction; overload;
    function  Go(Para: TVaiPara; Atual: Integer; Sort: String) : String;
    function  LC(Atual, Codigo: Integer): Boolean;
    procedure LimpaVAR_SQL();
    function  Nome(Padrao: String): String;
    function  NomeDoCampoCodUsu(Database: TZConnection; NomeDaTabela,
              NomeDoCampoCodigo: String): String;
    function  ObtemSQLPesquisaText(Mascara: Integer; Texto: String): String;
    procedure UpdUnlockZ(Registro: Integer; Database: TZConnection; Table,
              Field: String);
  end;

var
  GOTOz: TUnGOTOz;

implementation

uses ZModule, NomeX, CuringaZ, ModuleGeralZ;

{ TUnGOTOz }

procedure TUnGOTOz.BotoesSb(LaTipo: String);
var
  Acao: Boolean;
  i: Integer;
begin
  // Evitar erro
  if Screen.ActiveForm = nil then Exit;
  if LaTipo = CO_TRAVADO then Acao := True else Acao := False;
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Screen.ActiveForm.Components[i].Name = 'SbNovo')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNumero')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNome')
    or  (Screen.ActiveForm.Components[i].Name = 'SbQuery'))
    then begin
      if (Screen.ActiveForm.Components[i] is TBitBtn) then
        TBitBtn(Screen.ActiveForm.Components[i]).Enabled := Acao;
      if (Screen.ActiveForm.Components[i] is TSpeedButton) then
        TSpeedButton(Screen.ActiveForm.Components[i]).Enabled := Acao;
    end;
  end;
end;

procedure TUnGOTOz.BotoesSb(SQLType: TSQLType);
var
  Acao: Boolean;
  i, j: Integer;
begin
  if Screen.ActiveForm = nil then
    Exit;
  if SQLType = stLok then
    Acao := True
  else
    Acao := False;
  //
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Screen.ActiveForm.Components[i].Name = 'SbNovo')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNumero')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNome')
    or  (Screen.ActiveForm.Components[i].Name = 'SbQuery'))
    then begin
      if (Screen.ActiveForm.Components[i] is TBitBtn) then
        TBitBtn(Screen.ActiveForm.Components[i]).Enabled := Acao;
      if (Screen.ActiveForm.Components[i] is TSpeedButton) then
        TSpeedButton(Screen.ActiveForm.Components[i]).Enabled := Acao;
    end;
  end;
end;

procedure TUnGOTOz.BotoesSb(SQLType: TSQLType; Form: TForm);
var
  Acao: Boolean;
  i: Integer;
begin
  if SQLType = stLok then Acao := True else Acao := False;
  for i := 0 to Form.ComponentCount -1 do
  begin
    if  ((Form.Components[i].Name = 'SbNovo')
    or  (Form.Components[i].Name = 'SbNumero')
    or  (Form.Components[i].Name = 'SbNome')
    or  (Form.Components[i].Name = 'SbQuery'))
    then begin
      if (Form.Components[i] is TBitBtn) then
        TBitBtn(Form.Components[i]).Enabled := Acao;
      if (Form.Components[i] is TSpeedButton) then
        TSpeedButton(Form.Components[i]).Enabled := Acao;
    end;
  end;
end;

function TUnGOTOz.Codigo(Atual: Integer; Padrao: String): String;
var
  Num : String;
  Cod : Integer;
  Continua, Passa: Boolean;
begin
  Passa := False;
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA, 'Digite o c�digo.', '' );
  try
    Cod := Geral.IMV(Num);
    Continua := False;
    case VAR_GOTONEG of
      gotoNeg: Continua := Cod < 0;
      gotoNiZ: Continua := Cod <= 0;
      gotoAll: Continua := True;
      gotoNiP: Continua := Cod <> 0;
      gotoPiZ: Continua := Cod >= 0;
      gotoPos: Continua := Cod > 0;
    end;
    //if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not Continua then
    begin
      if (LowerCase(VAR_GOTOTABELA) = 'entidades') and (Cod = -2) then
        Passa := True
      else
      if (LowerCase(VAR_GOTOTABELA) = 'cunscad cun') and (Cod = -2) then
        Passa := True
      else
      begin
        Geral.MB_Aviso('C�digo n�o acess�vel nesta janela!');
        Exit;
      end;
    end;
    if (Cod > 0) or Passa or
    (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not LocalizaCodigo(Atual, Cod) then
      Geral.MB_Erro('O registro n� ' + Num +
      ' n�o foi localizado.')
    else
    begin
      if VAR_GOTOzSQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOzSQLTABLE.DisableControls;
        VAR_GOTOzSQLTABLE.Last;
        VAR_GOTOzSQLTABLE.EnableControls;
        VAR_GOTOzSQLTABLE.First;
      end;
      Result := '[1...]: '+TxtLaRegistro(VAR_GOTOzSQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
end;

function TUnGOTOz.CodUsu(Atual: Integer; Padrao: String): String;
var
  Num : String;
  Cod : Integer;
  Continua: Boolean;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite a refer�ncia.', '' );
  try
    Cod := Geral.IMV(Num);
    Continua := False;
    case VAR_GOTONEG of
      gotoNeg: Continua := Cod < 0;
      gotoNiZ: Continua := Cod <= 0;
      gotoAll: Continua := True;
      gotoNiP: Continua := Cod <> 0;
      gotoPiZ: Continua := Cod >= 0;
      gotoPos: Continua := Cod > 0;
    end;
    //if (Cod > 0) or (VAR_GOTONEG IN [gotoAll, gotoNeg, gotoNiP, gotoNiZ]) then
    if not Continua then
    begin
      Geral.MB_Aviso('C�digo n�o acess�vel nesta janela!');
      Exit;
    end;
    if not LocalizaCodUsu(Atual, Cod) then
      Geral.MB_Erro('A refer�ncia n� ' + Num + ' n�o foi localizada.')
    else
    begin
      if VAR_GOTOzSQLTABLE.RecordCount = -1 then
      begin
        VAR_GOTOzSQLTABLE.DisableControls;
        VAR_GOTOzSQLTABLE.Last;
        VAR_GOTOzSQLTABLE.EnableControls;
        VAR_GOTOzSQLTABLE.First;
      end;
      Result := '[R...]: '+TxtLaRegistro(VAR_GOTOzSQLTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"'+Num+
      SMLA_NUMEROINVALIDO);
  end;
end;

function TUnGOTOz.CriaForm(NomeDoCampoCodigo, NomeDoCampoNome,
  NomeDaTabela: String; DataBase: TZConnection; Extra: string;
  ReturnCodUsu: Boolean; LEFT_JOIN: String): Integer;
var
  TextoAPesquisar, Erro, TxtOriginal: String;
  Continua: Boolean;
begin
  try
    Result := 0;
    MyObjects.FormCria(TFmNomeX, FmNomeX);
    FmNomeX.Caption := 'Procura e Lista';
    FmNomeX.ShowModal;
    TextoAPesquisar := FmNomeX.FTexto;
    TxtOriginal := FmNomeX.FOrigi;
    Continua := FmNomeX.FContinua;
    FmNomeX.Destroy;
    VAR_CODIGO := 0;
    if Continua = True then
    begin
      if ExecutouPesq01(TextoAPesquisar, NomeDoCampoCodigo, NomeDoCampoNome, NomeDaTabela,
      DataBase, Extra, LEFT_JOIN, Erro) then
      begin
        MyObjects.FormCria(TFmCuringaZ, FmCuringaZ);
        FmCuringaZ.EdPesq.Text := TxtOriginal;

        FmCuringaZ.FTextoAPesquisar   := TextoAPesquisar;
        FmCuringaZ.FNomeDoCampoCodigo := NomeDoCampoCodigo;
        FmCuringaZ.FNomeDoCampoNome1  := NomeDoCampoNome;
        FmCuringaZ.FNomeDoCampoNome2  := '?';
        FmCuringaZ.FNomeDoCampoNome3  := '?';
        FmCuringaZ.FNomeDoCampoNome4  := '?';
        FmCuringaZ.FNomeDaTabela      := NomeDaTabela;
        FmCuringaZ.FDataBase          := DataBase;
        FmCuringaZ.FExtra             := Extra;
        FmCuringaZ.FLEFT_JOIN         := LEFT_JOIN;
        FmCuringaZ.FNomeDoCampoTipo   := '';
        FmCuringaZ.FTipo1             := 0;
        FmCuringaZ.FSohNum            := False;

        //Deve ser depois!
        FmCuringaZ.FOrigemPesquisa      := fpcTexto;
        FmCuringaZ.FPodeReabrirPesquisa := True;
        FmCuringaZ.PnPesquisa.Visible   := True;

        MyObjects.Informa2(FmCuringaZ.LaAviso1, FmCuringaZ.LaAviso2, False, Erro);

        FmCuringaZ.ShowModal;
        FmCuringaZ.Destroy;
      end;
      //
      if ReturnCodUsu then
        Result := VAR_CODUSU
      else
        Result := VAR_CODIGO;
    end;
  finally
    DModGZ.QrSB.Close;
  end;
end;

function TUnGOTOz.CriaLocalizador(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOzSQLTABLE.Connection <> DefineBaseDados) then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizador". Tabela: '+VAR_GOTOzSQLTABLE.Name);
  if GetPropInfo(VAR_GOTOzSQLTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizador". Tabela: '+VAR_GOTOzSQLTABLE.Name)
  else begin
    VAR_GOTOzSQLTABLE.Close;
    try
      VAR_GOTOzSQLTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLy[i]);
      for i := 0 to VAR_SQL1.Count -1 do
        VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQL1[i]);
      // Cuidado erro frequente AND na SQL sem WHERE !!!!
      VAR_GOTOzSQLTABLE.Params[0].AsInteger := Codigo;
      //Grl_DmkDB.AbreQuery(VAR_GOTOzSQLTABLE, VAR_GOTOzSQLTABLE.Connection);
      if Grl_DmkDB.AbreQuery(VAR_GOTOzSQLTABLE, VAR_GOTOzSQLDBNAME) then
        Result := VAR_GOTOzSQLTABLE.RecordCount > 0;
    except;
      Grl_DmkDB.LeMeuSQL_Fixo_z(VAR_GOTOzSQLTABLE, '', nil, True, True);
      Geral.MB_Erro('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizador". Tabela: '+VAR_GOTOzSQLTABLE.Name);
      raise;
    end;
  end;
end;

function TUnGOTOz.CriaLocalizadorCodUsu(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOzSQLTABLE.Connection <> DefineBaseDados) then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOzSQLTABLE.Name);
  if GetPropInfo(VAR_GOTOzSQLTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOzSQLTABLE.Name)
  else begin
    VAR_GOTOzSQLTABLE.Close;
    try
      VAR_GOTOzSQLTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLy[i]);
      if VAR_SQL2 <> nil then
      for i := 0 to VAR_SQL2.Count -1 do
        VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQL2[i])
      else
        Geral.MB_Aviso('"VAR_SQL2" n�o configurado!  AVISE A DERMATEK!');
      VAR_GOTOzSQLTABLE.Params[0].AsInteger := Codigo;
      Grl_DmkDB.AbreQuery(VAR_GOTOzSQLTABLE, VAR_GOTOzSQLTABLE.Connection);
      if VAR_GOTOzSQLTABLE.RecordCount > 0 then Result := True;
    except;
      Geral.MB_Erro('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOzSQLTABLE.Name);
      raise;
    end;
  end;
end;

function TUnGOTOz.DefineBaseDados(): TZConnection;
begin
  if VAR_GOTOzSQLDBNAME = nil then Result := VAR_GOTOzSQLDBNAME2
  else result := VAR_GOTOzSQLDBNAME;
end;

function TUnGOTOz.ExecutouPesq01(TextoAPesquisar, NomeDoCampoCodigo,
  NomeDoCampoNome, NomeDaTabela: String; DataBase: TZConnection; Extra,
  LEFT_JOIN: string; var Erro: String): Boolean;
var
  SQLNeg: String;
begin
  Erro := '';
  //Result := False;
  case VAR_GOTONEG of
    gotoNeg : SQLNeg := ' AND '+NomeDoCampoCodigo+'<0';
    gotoNiZ : SQLNeg := ' AND '+NomeDoCampoCodigo+'<=0';
    gotoNiP : SQLNeg := ' AND '+NomeDoCampoCodigo+'<>0';
    gotoPiZ : SQLNeg := ' AND '+NomeDoCampoCodigo+'>=0';
    gotoPos : SQLNeg := ' AND '+NomeDoCampoCodigo+'>0';
    else (*gotoAll :*) SQLNeg := '';
  end;
  try
    Result := Grl_DmkDB.AbreSQLQuery0(DmodGZ.QrSB, DataBase, [
    'SELECT ' + NomeDoCampoCodigo + ' Codigo, ',
    NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu, ',
    NomeDoCampoNome + ' Nome FROM ' + lowercase(NomeDaTabela) + ' ' + LEFT_JOIN,
    ' WHERE '+NomeDoCampoNome+' LIKE "' + TextoAPesquisar + '"',
    SQLNeg,
    Extra,
    ' ORDER BY ' + NomeDoCampoNome,
    ' ']);
  except
    Geral.MB_ERRO('N�o foi poss�vel a localiza��o pelo nome!' + sLineBreak +
    sLineBreak + 'SQL:' + sLineBreak + DmodGZ.QrSB.SQL.Text);
    //
    raise;
  end;
  if (DmodGZ.QrSB.RecordCount = 0) then
    Erro := 'N�o foi encontrado nenhum registro!';
  //
end;

function TUnGOTOz.ExecutouPesq05(NomeDoCampoCodigo, NomeDoCampoNome1,
  NomeDoCampoNome2, NomeDoCampoNome3, NomeDoCampoNome4, NomeDaTabela,
  NomeDoCampoTipo: String; Tipo1: Integer; DataBase: TZConnection;
  Extra: string; SohNum: Boolean; var Erro: String): Boolean;
var
  SQL1, SQL2, SQL3, SQL4, SQLNeg, WHR1, WHR2, WHR3, WHR4, SQLx: String;
begin
  Erro := '';
  case VAR_GOTONEG of
    gotoNeg : SQLNeg := ' AND ' + NomeDoCampoCodigo + '<0';
    gotoNiZ : SQLNeg := ' AND ' + NomeDoCampoCodigo + '<=0';
    gotoNiP : SQLNeg := ' AND ' + NomeDoCampoCodigo + '<>0';
    gotoPiZ : SQLNeg := ' AND ' + NomeDoCampoCodigo + '>=0';
    gotoPos : SQLNeg := ' AND ' + NomeDoCampoCodigo + '>0';
    else (*gotoAll :*) SQLNeg := '';
  end;
  try
    if SohNum then
      VAR_NOME_S := '%' + Geral.SoNumero_TT(VAR_NOME_S) + '%';
    //
    if NomeDoCampoNome1 = '' then SQL1 := '' else SQL1 :=
    //'IF(' + NomeDoCampoNome1 + '<>"", CONCAT(" [R] ", ' + NomeDoCampoNome1 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome1 + '<>"" THEN CONCAT(" [R] ", ' + NomeDoCampoNome1 + ') ELSE "" END, ';
    if NomeDoCampoNome2 = '' then SQL2 := '' else SQL2 :=
    //'IF(' + NomeDoCampoNome2 + '<>"", CONCAT(" [N] ", ' + NomeDoCampoNome2 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome2 + '<>"" THEN CONCAT(" [N] ", ' + NomeDoCampoNome2 + ') ELSE "" END, ';
    if NomeDoCampoNome3 = '' then SQL3 := '' else SQL3 :=
    //'IF(' + NomeDoCampoNome3 + '<>"", CONCAT(" [F] ", ' + NomeDoCampoNome3 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome3 + '<>"" THEN CONCAT(" [F] ", ' + NomeDoCampoNome3 + ') ELSE "" END, ';
    if NomeDoCampoNome4 = '' then SQL4 := '' else SQL4 :=
    //'IF(' + NomeDoCampoNome4 + '<>"", CONCAT(" [A] ", ' + NomeDoCampoNome4 + '), ""), ';
    'CASE WHEN ' + NomeDoCampoNome4 + '<>"" THEN CONCAT(" [A] ", ' + NomeDoCampoNome4 + ') ELSE "" END, ';
    //
    SQLx := SQL1 + SQL2 + SQL3 + SQL4;
    SQLx := Copy(SQLx, 1, Length(SQLx) - 2);
    //
    WHR1 := 'WHERE (' + NomeDoCampoNome1 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome2 = '' then WHR2 := '' else
      WHR2 := 'OR ' + NomeDoCampoNome2 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome3 = '' then WHR3 := '' else
      WHR3 := 'OR ' + NomeDoCampoNome3 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    if NomeDoCampoNome4 = '' then WHR4 := '' else
      WHR4 := 'OR ' + NomeDoCampoNome4 + ' LIKE UCASE("' + VAR_NOME_S + '") ';
    //
    Result := Grl_DmkDB.AbreSQLQuery0(DmodGZ.QrSB, DataBase, [
    'SELECT ' + NomeDoCampoCodigo + ' Codigo,  ',
    NomeDoCampoCodUsu(Database, NomeDaTabela, NomeDoCampoCodigo) + ' CodUsu,  ',
    'CONCAT(',
    SQLx,
    //'"") NOME FROM ' + LowerCase(NomeDaTabela),
    ') NOME FROM ' + LowerCase(NomeDaTabela),
    WHR1,
    WHR2,
    WHR3,
    WHR4,
    ')',
    Extra,
    SQLNeg,
    ' ORDER BY NOME',
    '']);
  except
    Geral.MB_ERRO('N�o foi poss�vel a localiza��o pelo nome!' + sLineBreak +
    sLineBreak + 'SQL:' + sLineBreak + DmodGZ.QrSB.SQL.Text);
    //
    raise;
  end;
  if (DmodGZ.QrSB.RecordCount = 0) then
    Erro := 'N�o foi encontrado nenhum registro!';
end;

function TUnGOTOz.Fechar(Caption: String): TCloseAction;
var
  TextoMB : PChar;
begin
  Result := caFree;
  if Caption <> CO_TRAVADO then
  begin
    if Caption = CO_DESISTE then
    begin
      Geral.MB_Aviso('Feche o formul�rio pelo bot�o "Desiste" ou "Sair"!');
      Result := caNone;
    end else begin
      TextoMB := PChar(SMLA_AVISOFORMCLOSE+Caption+'.');
      Geral.MB_Aviso(TextoMB);
      Result := caNone;
    end;
  end;
end;

function TUnGOTOz.Fechar(SQLType: TSQLType): TCloseAction;
var
  TextoMB : PChar;
begin
  Result := caFree;
  if SQLType <> stLok then
  begin
    if SQLType = stUnd then // Desiste
    begin
      Geral.MB_Aviso('Feche o formul�rio pelo bot�o "Desiste" ou "Sair"!');
      Result := caNone;
    end else begin
      TextoMB := PChar(SMLA_AVISOFORMCLOSE + DmkEnums.NomeTipoSQL(SQLType) + '.');
      Geral.MB_Aviso(TextoMB);
      Result := caNone;
    end;
  end;
end;

function TUnGOTOz.FiltroNeg(): String;
begin
  case VAR_GOTONEG of
    gotoNeg: Result := ' < 0 ';
    gotoNiZ: Result := ' <= 0 ';
    gotoAll: Result := ' ';
    gotoNiP: Result := ' <> 0 ';
    gotoPiZ: Result := ' >= 0 ';
    gotoPos: Result := ' > 0 ';
  end;
end;

function TUnGOTOz.Go(Para: TVaiPara; Atual: Integer; Sort: String): String;
begin
  Result := Vai(Para, Atual, Sort);
end;

function TUnGOTOz.LC(Atual, Codigo: Integer): Boolean;
begin
  Result := LocalizaCodigo(Atual, Codigo);
end;

procedure TUnGOTOz.LimpaVAR_SQL();
begin
  //VAR_SQLy.Clear;
  VAR_SQLx.Clear;
  VAR_SQL1.Clear;
  VAR_SQL2.Clear;
  VAR_SQLa.Clear;
end;

function TUnGOTOz.LocalizaCodigo(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    try
      Result := CriaLocalizador(Codigo);
    except
       raise;
    end;
  finally
     if not Result then
     begin
       EAbort.Create('Erro na fun��o "CriaLocalizador".');
     end;
  end;
  try
    //if (VAR_GOTOzSQLTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    if (VAR_GOTOzSQLTABLE.State <> dsInactive) and (VAR_GOTOzSQLTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizador(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodigo".');
    raise;
  end;
end;

function TUnGOTOz.Nome(Padrao: String): String;
var
  X : String;
  i: Integer;
  Continua: Boolean;
begin
  Result := Padrao;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura Pelo Nome';
  FmNomeX.ShowModal;
  X := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  if Continua = True then
  begin
    VAR_GOTOzSQLTABLE.Close;
    VAR_GOTOzSQLTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOzSQLTABLE.SQL.Add(VAR_SQLa[i]);
    VAR_GOTOzSQLTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOzSQLTABLE.Params[0].AsString := X;
    if VAR_GOTOzSQLTABLE.Params.Count > 1 then
    VAR_GOTOzSQLTABLE.Params[1].AsString := X;
    Grl_DmkDB.AbreQuery(VAR_GOTOzSQLTABLE, VAR_GOTOzSQLTABLE.Connection);
    if VAR_GOTOzSQLTABLE.RecordCount = -1 then
    begin
      VAR_GOTOzSQLTABLE.DisableControls;
      VAR_GOTOzSQLTABLE.Last;
      VAR_GOTOzSQLTABLE.EnableControls;
      VAR_GOTOzSQLTABLE.First;
    end;
    Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOzSQLTABLE.RecordCount, 0);
  end;
end;

function TUnGOTOz.NomeDoCampoCodUsu(Database: TZConnection; NomeDaTabela,
  NomeDoCampoCodigo: String): String;
var
  P: Integer;
  NomeTab: String;
  Qry: TZQuery;
begin
  Result := NomeDoCampoCodigo;
  P := Pos(' ', NomeDaTabela);
  if P > 0 then
    NomeTab := Copy(NomeDaTabela, 1, P - 1)
  else
    NomeTab := NomeDaTabela;
  //
  Qry := TZQuery.Create(DModGZ);
  try
    Grl_DmkDB.AbreSQLQuery0(Qry, DModGZ.QrAux_.Connection, [
    'PRAGMA table_info(' + Lowercase(NomeTab) + ')',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if Uppercase(String(Qry.FieldByName('name').AsString)) = 'CODUSU' then
      begin
        Result := 'CodUsu';
        Exit;
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnGOTOz.ObtemSQLPesquisaText(Mascara: Integer; Texto: String): String;
var
  Txt, Res: String;
begin
  Txt := Texto;
  //
  if Mascara in ([0, 3]) then
    Txt := StringReplace(Txt, ' ', '%', [rfReplaceAll, rfIgnoreCase]);
  //
  Res := '';
  //
  if Mascara in ([0, 1, 2]) then
    Res := '%';
  //
  Res := Res + Txt;
  //
  if Mascara in ([0, 1, 4]) then
    Res := Res + '%';
  //
  Result := Res;
end;

function TUnGOTOz.TxtLaRegistro(RecCount, Increm: Integer): String;
var
  Total, p: Integer;
  CountTxt: String;
begin
  QvCountZ.Close;
  QvCountZ.Connection := DefineBaseDados;

  QvCountZ.SQL.Clear;
  // 2012-08-13
  //'gragrux ggx, gragru1 gg1'
  p := Pos(',', VAR_GOTOTABELA);
  if p > 0 then
    QvCountZ.SQL.Add('SELECT COUNT(*) Record From '+Lowercase(Copy(VAR_GOTOTABELA, 1, p-1))+'')
  else
  // FIM 2012-08-13
  QvCountZ.SQL.Add('SELECT COUNT(*) Record From '+Lowercase(VAR_GOTOTABELA)+'');
  Grl_DmkDB.AbreQuery(QvCountZ, QvCountZ.Connection);
  Total := QvCountZ.FieldByName('Record').AsInteger;
  QvCountZ.Close;
  case Increm of
    -2 : VAR_RECNO := 0;
     0 : if Total > 0 then VAR_RECNO := 1 else VAR_RECNO := 0;
     2 : VAR_RECNO := Total;
     else VAR_RECNO := VAR_RECNO + Increm;
  end;
  if VAR_RECNO > RecCount then VAR_RECNO := RecCount;
  if VAR_RECNO < 0 then VAR_RECNO := 0;
  if (RecCount > 0) and (VAR_RECNO = 0) then VAR_RECNO := 1;
  if RecCount = -1 then CountTxt := '?'
  else CountTxt := IntToStr(RecCount);
  Result := (*IntToStr(VAR_RECNO)+CO_DE+*)CountTxt+CO_DE+IntToStr(Total);
end;

procedure TUnGOTOz.UpdUnlockZ(Registro: Integer; Database: TZConnection; Table,
  Field: String);
begin
  QvUpdZ.Close;
  QvUpdZ.Connection := Database;
  QvUpdZ.SQL.Text :=
  'UPDATE ' + LowerCase(Table) + ' SET Lk=0' + sLineBreak +
  'WHERE ' + Field + '=' + Geral.FF0(Registro);
  //
  QvUpdZ.ExecSQL;
end;

function TUnGOTOz.LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorCodUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorCodUsu".');
  end;
  try
    if (VAR_GOTOzSQLTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizadorCodUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodUsu".');
    raise;
  end;
end;

{
function TUnGOTOz.LocalizaTxtUsu(Atual, Codigo: String): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorTxtUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorTxtUsu".');
  end;
  try
    if (Registros(VAR_GOTOzSQLTABLE) = 0) (*and ((Atual <> '') or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizadorTxtUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaTxtUsu".');
    raise;
  end;
end;
}

function TUnGOTOz.Vai(Para: TVaiPara; Atual: Integer; Sort: String): String;
const
  Mostra = False; //True;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOzSQLTABLE.First else
    if Para = vpPrior then VAR_GOTOzSQLTABLE.Prior else
    if Para = vpNext then VAR_GOTOzSQLTABLE.Next else
    if Para = vpLast then VAR_GOTOzSQLTABLE.Last else
    if Para = vpThis then
      if not VAR_GOTOzSQLTABLE.Locate('Periodo', Geral.Periodo2000(Date), []) then
        Geral.MB_Aviso('Per�odo atual n�o localizado!');
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocZ.Close;
    QvLocZ.Connection := DefineBaseDados;
    QvLocZ.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocZ.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocZ.SQL.Add('FROM '+lowercase(var_gototabela));
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocZ.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocZ.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      }
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocZ.SQL.Add('AND ')
        else
          QvLocZ.SQL.Add('WHERE ');
        QvLocZ.SQL.Add(VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocZ.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocZ.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocZ.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocZ.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocZ.SQL.Add(VAR_SQLy[I]);
      end;
      }
      if VAR_GOTONEG <> gotoAll then
        QvLocZ.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocZ.Params[0].AsInteger := Atual;
      Increm := -1;
      Quando := True;
    end else
    if Para = vpNext then
    begin
      QvLocZ.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocZ.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocZ.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocZ.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocZ.SQL.Add(VAR_SQLy[I]);
      end;
      }
      if VAR_GOTONEG <> gotoAll then
        QvLocZ.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      QvLocZ.Params[0].AsInteger := Atual;
      Increm := 1;
      Quando := True;
    end else
    if Para = vpLast then
    begin
      QvLocZ.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocZ.SQL.Add('FROM '+lowercase(var_gototabela));
      {
      if VAR_SQLy.Count > 0 then
      begin
        QvLocZ.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocZ.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      }
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocZ.SQL.Add('AND ')
        else
          QvLocZ.SQL.Add('WHERE ');
        QvLocZ.SQL.Add(VAR_GOTOCAMPO + FiltroNeg());
        Quando := True;
      end;
      Increm := 2;
    end else
    if Para = vpThis then
    begin
      QvLocZ.SQL.Add('SELECT '+VAR_GOTOCAMPO+' Record');
      QvLocZ.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocZ.SQL.Add('WHERE '+VAR_GOTOCAMPO+'='+IntToStr(Geral.Periodo2000(Date)));
      Quando := True;
      if VAR_GOTONEG <> gotoAll then
        QvLocZ.SQL.Add('AND '+VAR_GOTOCAMPO + FiltroNeg());
      Increm := 0;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocZ.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocZ.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    //
    //QvLocZ. O p e n ;
    try
      Grl_DmkDB.AbreQuery(QvLocZ, QvLocZ.Connection);
    except
      on E: Exception do
        Geral.MB_Erro(E.Message + slineBreak +
        'SQL:' + sLineBreak + QvLocZ.SQL.Text);
    end;
    //
    if Mostra then
      dmkPF.LeMeuTexto(QvLocZ.SQL.Text);
    QvLocZ.First;
    if (QvLocZ.RecordCount > 0) then
    begin
      if (QvLocZ.FieldByName('Record').AsString <> '') then
        if (QvLocZ.FieldByName('Record').AsInteger <> null) then
          LocalizaCodigo(Atual, QvLocZ.FieldByName('Record').AsInteger);
    end else
    begin
      if Para = vpThis then
      begin
        // Colocar a mensagem no localizador de per�odo
        //Geral.MB_Aviso(VAR_PERIODO_NAO_LOC + ': Atual'));
        Result := VAR_PERIODO_NAO_LOC;
        QvLocZ.Close;
        Exit;
      end;
    end;
    QvLocZ.Close;
  end;
  if VAR_GOTOzSQLTABLE.State = dsBrowse then
    Qtd := VAR_GOTOzSQLTABLE.RecordCount;
  Result := Seq + TxtLaRegistro(Qtd, increm);
end;

end.
