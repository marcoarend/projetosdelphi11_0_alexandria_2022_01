unit ZModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) ZAbstractRODataset, ZAbstractDataset, ZDataset,
  ZAbstractConnection, ZConnection;

type
  TZMod = class(TDataModule)
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CriaQv_GOTOz;

  public
    { Public declarations }
  end;

var
  ZMod: TZMod;
  QvAllZ      : TZQuery;
  QvUpdZ      : TZQuery;
  QvLivreZ    : TZQuery;
  QvUserZ     : TZQuery;
  QvLocZ      : TZQuery;
  QvCountZ    : TZQuery;
  QvRecCountZ : TZQuery;
  QvSelZ      : TZQuery;
  QvDuplicStrZ: TZQuery;
  QvDuplicIntZ: TZQuery;
  QvInsLogZ   : TZQuery;
  QvDelLogZ   : TZQuery;

implementation

uses UnInternalConsts;

{$R *.DFM}

procedure TZMod.CriaQv_GOTOz;
begin
{
  QvAgora := TmySQLQuery.Create(Gmod);
  QvAgora.DataBase := VAR_GOTOMySQLDBNAME;
  QvAgora.SQL.Add('SELECT NOW() Agora');
  //
  QvLocX := TmySQLQuery.Create(Gmod);
  QvLocX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvUpdX := TmySQLQuery.Create(Gmod);
  QvUpdX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvSelX := TmySQLQuery.Create(Gmod);
  QvSelX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvCountX := TmySQLQuery.Create(Gmod);
  QvCountX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvDuplicStrX := TmySQLQuery.Create(Gmod);
  QvDuplicStrX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvDuplicIntX := TmySQLQuery.Create(Gmod);
  QvDuplicIntX.DataBase := VAR_GOTOMySQLDBNAME;
  //
  QvInsLogX := TmySQLQuery.Create(Gmod);
  QvInsLogX.DataBase := VAR_GOTOMySQLDBNAME;
  QvInsLogX.SQL.Add('INSERT INTO logs SET Data=NOW(), Tipo=:P0, ');
  QvInsLogX.SQL.Add('Usuario=:P1, ID=:P2');
  //
  QvDelLogX := TmySQLQuery.Create(Gmod);
  QvDelLogX.DataBase := VAR_GOTOMySQLDBNAME;
  QvDelLogX.SQL.Add(DELETE_FROM + ' logs WHERE Tipo=:P0');
  QvDelLogX.SQL.Add('AND Usuario=:P1 AND ID=:P2');
  //
  QvInsLogY := TMySQLQuery.Create(Gmod);
  QvInsLogY.DataBase := VAR_GOTOMySQLDBNAME;
  QvInsLogY.SQL.Add('INSERT INTO logs SET Data=NOW(), Tipo=:P0, ');
  QvInsLogY.SQL.Add('Usuario=:P1, ID=:P2');
  //
  QvDelLogY := TMySQLQuery.Create(Gmod);
  QvDelLogY.DataBase := VAR_GOTOMySQLDBNAME;
  QvDelLogY.SQL.Add(DELETE_FROM + ' logs WHERE Tipo=:P0');
  QvDelLogY.SQL.Add('AND Usuario=:P1 AND ID=:P2');
  //
  QvLivreX := TmySQLQuery.Create(Gmod);
  QvLivreX.DataBase := VAR_GOTOMySQLDBNAME;
  //
}
  QvLocZ := TZQuery.Create(Zmod);
  QvLocZ.Connection := VAR_GOTOzSQLDBNAME;
  //
{
  QvUser := TmySQLQuery.Create(Gmod);
  QvUser.DataBase := VAR_GOTOMySQLDBNAME;
  QvUser.SQL.Add('SELECT Login FROM senhas');
  QvUser.SQL.Add('WHERE Numero=:Usuario');
  //
}
  QvCountZ := TZQuery.Create(Zmod);
  QvCountZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvUpdZ := TZQuery.Create(Zmod);
  QvUpdZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvLivreZ := TZQuery.Create(Zmod);
  QvLivreZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvRecCountZ := TZQuery.Create(Zmod);
  QvRecCountZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvSelZ := TZQuery.Create(Zmod);
  QvSelZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvDuplicStrZ := TZQuery.Create(Zmod);
  QvDuplicStrZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvDuplicIntZ := TZQuery.Create(Zmod);
  QvDuplicIntZ.Connection := VAR_GOTOzSQLDBNAME;
  //
  QvAllZ := TZQuery.Create(Self);
  QvAllZ.Connection := VAR_GOTOzSQLDBNAME;
  //
end;

procedure TZMod.DataModuleCreate(Sender: TObject);
begin
  CriaQv_GOTOz;
end;


end.

