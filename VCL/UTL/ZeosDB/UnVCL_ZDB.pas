unit UnVCL_ZDB;

interface

uses System.SysUtils, System.Generics.Collections, System.Classes, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.Forms, Winapi.Windows, ZAbstractConnection, ZConnection,
  ZCompatibility, ZAbstractRODataset, ZAbstractDataset, ZDataset, UnDmkEnums,
  dmkGeral, MyListas, UnMyLinguas, UnDmkProcFunc, UnGrl_Vars,
  UnInternalConsts;

type
  TUnVCL_ZDB = class(TObject)
  private

  public
    function  ConfiguraDB_SQLite(const DataBase: TZConnection; const Usuario,
              Senha, NomeDataBase: String; const CriaSeNaoExiste: Boolean; var
              DBNome: String): Boolean;
    function  ConfiguraBD(DataBase: TZConnection; QueryControle,
              QueryLoc: TZQuery; CriaSeNaoExiste: Boolean = True): Boolean;
    procedure MostraVerifyDB(Verifica: Boolean; DataBase: TZConnection);
    procedure ReopenControle(DataBase: TZConnection; QueryControle: TZQuery);
    (*
    Movido para UnGrl_DmkDB
    function  AbreSQLQuery0(Query: TZQuery; DataBase: TZConnection;
              SQL: array of String): Boolean;
    function  EfetuaVerificacoes(DBName: String; Memo: TMemo; Estrutura,
              EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
              LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
              LaTempoT: TLabel; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DriverNome: String; DataBase: TZConnection;
              VersaoAtual: Integer): Boolean;
    procedure AtualizaVersaoTBControle(DataBase: TZConnection; Versao: Integer);
    function  CarregaSQLInsUpd(QrUpd: TZQuery; DataBase: TZConnection;
              Tipo: TSQLType; Tabela: String; Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String; ValCampos,
              ValIndex: array of Variant; UserDataAlterweb, IGNORE: Boolean;
              ComplUpd: String; Device: TDeviceType;
              Sincro: Boolean = False): Boolean;
    //S Q L I T E
    function  EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo; Estrutura,
              EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
              LaAvisoP1, LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
              LaTempoT: TLabel; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DataBase: TZConnection): Boolean;
    function  VerificaEstrutura_SQLite(DBName: String; Memo: TMemo;
              RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B,
              LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI, LaTempoF,
              LaTempoT: TLabel; MeTabsNoNeed: TMemo; PB: TProgressBar;
              MeFldsNoNeed: TMemo; DataBase: TZConnection): Boolean;
    function  VerificaCampos_SQLite(DBName: String; TabelaNome,
              TabelaBase: String; Memo: TMemo; RecriaRegObrig,
              Tem_Del, Tem_Sync: Boolean; LaAviso1B, LaAviso2B, LaAviso1G,
              LaAviso2G: TLabel; MeFldsNoNeed: TMemo; DataBase:
              TZConnection): TResultVerify;
    *)
  end;

var
  VCL_ZDB: TUnVCL_ZDB;

implementation

uses UnMyObjects, VerifiDB_SQLite, UnGrl_DmkDB;

{ TUnVCL_ZDB }

function TUnVCL_ZDB.ConfiguraBD(DataBase: TZConnection; QueryControle,
  QueryLoc: TZQuery; CriaSeNaoExiste: Boolean): Boolean;
var
  Versao: Integer;
  DBNome: String;
begin
  Result := False;
  //
  if ConfiguraDB_SQLite(DataBase, 'root', CO_USERSPNOW, CO_DBNome,
  CriaSeNaoExiste, DBNome) then
  begin
    Versao := Grl_DmkDB.ObtemVersaoAppDB(QueryLoc, DataBase, istSQLite, stDesktop);
    //
    if Versao < CO_VERSAO then
      MostraVerifyDB(True, DataBase)
    else
      ReopenControle(DataBase, QueryControle);
    Result := True;
  end else
    Geral.MB_Erro('Falha ao conectar no banco de dados!');
end;

function TUnVCL_ZDB.ConfiguraDB_SQLite(const DataBase: TZConnection; const
  Usuario, Senha, NomeDataBase: String; const CriaSeNaoExiste: Boolean; var
  DBNome: String): Boolean;
var
  Continua: Boolean;
  DBDir, NomeDriver: String;
begin
  Result := True;
  //
  if DataBase.Connected then
    DataBase.Connected := False;
  //
  NomeDriver := LowerCase('SQLite-3');
  //
  try
    //DBNome := ExtractFilePath(ParamStr(0)) + NomeDataBase + '.s3db';
    Continua := True;
    if Trim(VAR_TMeuDB_ZDir) <> '' then
      DBDir    := VAR_TMeuDB_ZDir
    else
      DBDir    := CO_DIR_RAIZ_DMK + '\SQLite\';
    //
    if not DirectoryExists(DBDir) then
      Continua := ForceDirectories(DBDir);
    //
    if not Continua then
    begin
      Result := False;
      Exit;
    end;
    DBNome := DBDir + NomeDataBase + '.s3db';
    //
    if (CriaSeNaoExiste = False) and (FileExists(DBNome) = False) then
    begin
      Result := False;
      Exit;
    end;
    DataBase.User     := Usuario;
    DataBase.Password := Senha;
    DataBase.Database := DBNome;
    //
    DataBase.ClientCodepage   := 'UTF-8';
    DataBase.ControlsCodePage := cGET_ACP;
    DataBase.LibraryLocation  := 'sqlite3.dll';
    DataBase.Protocol         := NomeDriver;
    DataBase.LoginPrompt      := False;
    DataBase.Connected        := True;
    Result                    := DataBase.Connected;
  except
    Result := False;
  end;
end;

procedure TUnVCL_ZDB.MostraVerifyDB(Verifica: Boolean; DataBase: TZConnection);
begin
  Application.CreateForm(TFmVerifiDB_SQLite, FmVerifiDB_SQLite);
  FmVerifiDB_SQLite.FVerifica    := Verifica;
  FmVerifiDB_SQLite.FDataBaseAll := DataBase;
  FmVerifiDB_SQLite.FDataBaseUsu := nil;
  FmVerifiDB_SQLite.Show;
  if Verifica then
    FmVerifiDB_SQLite.VerificaBD;
end;

procedure TUnVCL_ZDB.ReopenControle(DataBase: TZConnection;
  QueryControle: TZQuery);
begin
  Grl_DmkDB.AbreSQLQuery0(QueryControle, DataBase, [
    'SELECT Versao ',
    'FROM controle ',
    '']);
end;

(*
Movido para UnGrl_DmkDB
function TUnVCL_ZDB.AbreSQLQuery0(Query: TZQuery; DataBase: TZConnection;
  SQL: array of String): Boolean;
var
  I: Integer;
begin
  Result := False;
  Query.Close;
  Query.Connection := DataBase;
  //
  try
    Query.Close;
    if High(SQL) >= 0 then // 0 = 1 linha
    begin
      Query.SQL.Clear;
      for I := Low(SQL) to High(SQL) do
      begin
        if SQL[I] <> '' then
          Query.SQL.Add(SQL[I]);
      end;
    end;
    if Query.SQL.Text = '' then
    begin
      Geral.MB_Erro('Texto da SQL indefinido no SQLQuery!' + sLineBreak +
        'Avise a DERMATEK!');
      Result := False;
    end
    else begin
      Query.Open;
      Result := True;
    end;
  except
    on e: Exception do
    begin
      Geral.MB_Erro('Erro ao tentar abrir uma SQL no SQLQuery!' +
        sLineBreak + TComponent(Query.Owner).Name + '.' + Query.Name +
        sLineBreak + e.Message + sLineBreak + Query.SQL.Text);
    end;
  end;
end;

function TUnVCL_ZDB.EfetuaVerificacoes(DBName: String; Memo: TMemo; Estrutura,
  EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1, LaAvisoP2,
  LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
  LaTempoF, LaTempoT: TLabel; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; DriverNome: String; DataBase: TZConnection;
  VersaoAtual: Integer): Boolean;
begin
  Result := False;
  //
  if LowerCase(DriverNome) = 'sqlite' then
  begin
      Result := EfetuaVerificacoes_SQLite(DBName, Memo, Estrutura,
                  EstrutLoc, Controle, Pergunta, RecriaRegObrig, LaAvisoP1,
                  LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B,
                  LaAviso1G, LaAviso2G, LaTempoI, LaTempoF, LaTempoT,
                  MeTabsNoNeed, PB, MeFldsNoNeed, DataBase);
      if Result then
      begin
        if DBName = CO_DBNome then
          AtualizaVersaoTBControle(DataBase, VersaoAtual);
      end;
  end else
  begin
    Geral.MB_Aviso('N�o implementado!');
    Result := False;
  end;
end;

procedure TUnVCL_ZDB.AtualizaVersaoTBControle(DataBase: TZConnection;
  Versao: Integer);
var
  QueryUpd: TZQuery;
begin
  QueryUpd := TZQuery.Create(DataBase);
  try
    CarregaSQLInsUpd(QueryUpd, DataBase, stUpd, 'controle', False,
      ['Versao'], ['Codigo'], [Versao], [1], False, False, '', stMobile);
  finally
    if QueryUpd <> nil then
      QueryUpd.Free;
  end;
end;

function TUnVCL_ZDB.CarregaSQLInsUpd(QrUpd: TZQuery; DataBase: TZConnection;
  Tipo: TSQLType; Tabela: String; Auto_increment: Boolean; SQLCampos,
  SQLIndex: array of String; ValCampos, ValIndex: array of Variant;
  UserDataAlterweb, IGNORE: Boolean; ComplUpd: String; Device: TDeviceType;
  Sincro: Boolean): Boolean;
var
  QueryLoc: TZQuery;
  LastAcao: TLastAcao;
  AlterWeb: TAlterWeb;
  i, j, k, CliIntSync: Integer;
  LastModifi: TDateTime;
  Valor, Liga, Data, LastModifi_Txt, Tab, Campos, Valores, SQLWhere: String;
begin
  Result := False;
  i      := High(SQLCampos);
  j      := High(ValCampos);

  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' campos e ' +  Geral.FF0(j+1) +
      ' valores para estes campos em "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  i := High(SQLIndex);
  j := High(ValIndex);

  if i <> j then
  begin
    Geral.MB_Erro('ERRO! Existem ' + Geral.FF0(i+1) + ' �ndices e ' +
      Geral.FF0(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  if (i >= 0) and Auto_increment then
  begin
    Geral.MB_Erro('AVISO! Existem ' + Geral.FF0(i+1) + ' �ndices informados ' +
      'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + sLineBreak +
      'Tabela: "' + Tabela + '"');
    Exit;
  end;

  Tab      := LowerCase(Tabela);
  AlterWeb := alDsk;

  case Device of
    stDesktop:
      AlterWeb := alDsk;
    stWeb:
      AlterWeb := alWeb;
    stMobile:
      AlterWeb := alMob;
  end;

  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MB_Erro('AVISO: O status da a��o est� definida como ' +
      '"' + Grl_DmkDB.NomeTipoSQL(Tipo) + '"');
  end;

  Data := #39 + Geral.FDT(Date, 1) + #39;

  QrUpd.SQL.Clear;
  QrUpd.Connection := DataBase;

  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab) + ' (')
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' (');
  end else
  begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');

    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb='+ Geral.FF0(Integer(AlterWeb)) +', ');
  end;

  Campos  := '';
  Valores := '';
  j       := System.High(SQLCampos);

  for i := System.Low(SQLCampos) to j do
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', ' + SQLCampos[i];
      Valor   := Geral.VariavelToString(ValCampos[i]);
      Valores := Valores + ', ' + Valor;
    end else
    begin
      if SQLCampos[i] = CO_JOKE_SQL then
      begin
        if ((i < j) or UserDataAlterweb) and (Trim(ValCampos[i]) <> '') then
          QrUpd.SQL.Add(ValCampos[i] + ', ')
        else
          QrUpd.SQL.Add(ValCampos[i]);
      end else
      begin
        Valor := Geral.VariavelToString(ValCampos[i]);

        if (i < j) or UserDataAlterweb then
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
        else
          QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
      end;
    end;
  end;

  if Sincro then
  begin
    LastModifi     := dmkPF.DateTime_MyTimeZoneToUTC(Now);
    LastModifi_Txt := #39 + Geral.FDT(LastModifi, 109) + #39 ;
    CliIntSync     := VAR_WEB_USER_USRENT;

    if Tipo = stIns then
    begin
      LastAcao := laIns; //Inclus�o
    end else
    begin
      //Se n�o faz sincronismo, marter o "lastacao" = "inclus�o" por causa do sincronismo
      LastAcao := laUpd;//Altera��o
      SQLWhere := '';

      for k := Low(SQLIndex) to High(SQLIndex) do
      begin
        SQLWhere := SQLWhere + ' AND ' + SQLIndex[k] + '="'+ dmkPF.VariantToString(ValIndex[k]) +'"';
      end;
      QueryLoc := TZQuery.Create(DataBase);
      try
        AbreSQLQuery0(QueryLoc, DataBase, [
          'SELECT LastAcao ',
          'FROM ' + Tabela,
          'WHERE AlterWeb=2 ',
          SQLWhere,
          '']);
        if QueryLoc.RecordCount > 0 then
        begin
          if QueryLoc.FieldByName('LastAcao').AsInteger = 0 then //Inclus�o
            LastAcao := laIns;
        end;
      finally
        QueryLoc.Free;
      end;
    end;

    if Tipo = stIns then
    begin
      Campos  := Campos + ', LastModifi, LastAcao, CliIntSync';
      Valores := Valores + ', ' + LastModifi_Txt + ', ' + Geral.FF0(Integer(LastAcao)) +
                   ', ' + Geral.FF0(CliIntSync);
    end else
      QrUpd.SQL.Add('LastModifi=' + LastModifi_Txt + ', LastAcao=' +
        Geral.FF0(Integer(LastAcao)) + ', CliIntSync=' + Geral.FF0(CliIntSync) + ', ');
  end;

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
      Campos  := Campos + ', DataCad, UserCad, AlterWeb';
      Valores := Valores + ', ' + Data + ', ' + Geral.FF0(VAR_USUARIO) + ', ' + Geral.FF0(Integer(AlterWeb));
    end else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + Geral.FF0(VAR_USUARIO));
  end;

  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MB_Erro('SQL com Auto Increment e Indice ao mesmo tempo!');
  end else
  begin
    for i := System.Low(SQLIndex) to System.High(SQLIndex) do
    begin
      if Tipo = stIns then
      begin
        Liga := ', ';
      end else
      begin
        if i = 0 then
          Liga := 'WHERE '
        else
          Liga := 'AND ';
      end;

      if SQLIndex[i] = CO_JOKE_SQL then
      begin
        QrUpd.SQL.Add(Liga + ValIndex[i]);
      end else
      begin
        if Tipo = stIns then
        begin
          Campos  := Campos + ', ' + SQLIndex[i];
          Valor   := Geral.VariavelToString(ValIndex[i]);
          Valores := Valores + ', ' + Valor;
        end else
        begin
          Valor := Geral.VariavelToString(ValIndex[i]);

          QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
        end;
      end;
    end;
  end;

  if Tipo = stIns then
  begin
    Campos  := Copy(Campos, 3, Length(Campos)) + ') VALUES (';
    Valores := Copy(Valores, 3, Length(Valores)) + ')';

    QrUpd.SQL.Add(Campos);
    QrUpd.SQL.Add(Valores);
  end;

  if Trim(ComplUpd) <> '' then
  begin
    QrUpd.SQL.Add(ComplUpd);
  end;

  QrUpd.SQL.Add(';');
  QrUpd.ExecSQL();
  Result := True;
end;

function TUnVCL_ZDB.EfetuaVerificacoes_SQLite(DBName: String; Memo: TMemo;
  Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean; LaAvisoP1,
  LaAvisoP2, LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
  LaTempoI, LaTempoF, LaTempoT: TLabel; MeTabsNoNeed: TMemo; PB: TProgressBar;
  MeFldsNoNeed: TMemo; DataBase: TZConnection): Boolean;
var
  Qry: TZQuery;
  Res: Integer;
begin
  Result := False;
  Qry := TZQuery.Create(DataBase);
  try
    MeTabsNoNeed.Lines.Clear;

    Res := Geral.MB_Pergunta('Ser� realizada uma verifica��o na base de dados: ' +
              sLineBreak + 'Banco de dados: ' + DBName + sLineBreak+
              'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
              'perante restaura��o de backup.');

    if Res <> ID_YES then
      Exit;

    Application.ProcessMessages;

    Result    := True;
    FPergunta := Pergunta;
    try
      // verifica se a vers�o atual � mais nova
      // se n�o � impede a verifica��o
      if DBName <> '' then
      begin
        AbreSQLQuery0(Qry, DataBase, [
        'SELECT name FROM sqlite_master ',
        'WHERE type=''table''',
        'AND name LIKE ''Controle''',
        'ORDER BY name;',
        '']);
         if Qry.RecordCount > 0 then
        begin
          AbreSQLQuery0(Qry, DataBase, [
          'PRAGMA table_info(controle);',
          '']);
          if Qry.RecordCount > 0 then
          begin
            AbreSQLQuery0(Qry, DataBase, [
            'SELECT Versao FROM controle',
            '']);
            if CO_VERSAO < Qry.FieldByName('Versao').AsLargeInt then
            begin
              Geral.MB_Aviso('Verifica��o de banco de dados cancelada! ' +
                sLineBreak + ' A vers�o deste arquivo � inferior a cadastrada ' +
                'no banco de dados.');
              Exit;
            end;
          end;
        end;
      end else
        Geral.MB_Erro('Banco de dados n�o definido na verifica��o!');
      //
      VerificaEstrutura_SQLite(DBName, Memo, RecriaRegObrig, LaAviso1R,
        LaAviso2R, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G, LaTempoI,
        LaTempoF, LaTempoT, MeTabsNoNeed, PB, MeFldsNoNeed, DataBase);
    except
      raise;
    end;
    Geral.MB_Aviso('Verifica��o de banco de dados finalizada!');
  finally
    Qry.Free;
  end;
end;

function TUnVCL_ZDB.VerificaEstrutura_SQLite(DBName: String; Memo: TMemo;
  RecriaRegObrig: Boolean; LaAviso1R, LaAviso2R, LaAviso1B, LaAviso2B,
  LaAviso1G, LaAviso2G, LaTempoI, LaTempoF, LaTempoT: TLabel;
  MeTabsNoNeed: TMemo; PB: TProgressBar; MeFldsNoNeed: TMemo;
  DataBase: TZConnection): Boolean;
var
  i: Integer;
  TabNome, TabBase, Item: String;
  TempoI, TempoF, TempoT: TTime;
  DefTabela: TTabelas;
  Tem_Del, Tem_Sync: Boolean;
  QrTabs: TZQuery;
  Texto: String;
begin
  QrTabs := TZQuery.Create(DataBase);
  try
    Tem_Del      := False;
    Tem_Sync     := False;
    TempoI       := Time();
    FViuControle := 0;
    Result       := True;

    MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgEstruturaBD, [DBName]));
    MyObjects.Informa(LaTempoI, False, FormatDateTime('hh:nn:ss:zzz', TempoI));

    try
      FTabelas := TList<TTabelas>.Create;

      MyList.CriaListaTabelas(DBName, FTabelas);

      try
        if LaAviso1R <> nil then
          Texto := LaAviso1R.Caption
        else
          Texto := '';
        MyObjects.Informa2(LaAviso1R, LaAviso2R, True, Texto + '(' +
          Geral.FF0(FTabelas.Count) + ' tabelas)');

        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas j� criadas');

        AbreSQLQuery0(QrTabs, DataBase, [
          'SELECT name FROM sqlite_master ',
          'WHERE type=''table'' ',
          'ORDER BY name; ',
          '']);

        if PB <> nil then
        begin
          PB.Max      := QrTabs.RecordCount;
          PB.Position := 0;
        end;

        QrTabs.First;
        while not QrTabs.Eof do
        begin
          if PB <> nil then
            PB.Position := PB.Position + 1;
          // Fazer Primeiro controle !!!
          if FViuControle = 0 then
          begin
            TabNome      := 'controle';
            FViuControle := 1;
          end else
          if FViuControle = 2 then
          begin
            QrTabs.Prior;
            FViuControle   := 3;
            TabNome        := QrTabs.Fields[0].AsString;
          end else
            TabNome := QrTabs.Fields[0].AsString;

          TabBase := '';

          for i := 0 to FTabelas.Count -1 do
          begin
            DefTabela := FTabelas[i];

            if Uppercase(DefTabela.TabCria) = Uppercase(TabNome) then
            begin
              TabBase  := DefTabela.TabBase;
              Tem_Del  := DefTabela.Tem_Del;
              Tem_Sync := DefTabela.Tem_Sync;
              Break;
            end;
          end;

          if TabBase = '' then
            TabBase := TabNome;

          MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando estrutura de tabelas j� criadas');
          MyObjects.Informa2(LaAviso1G, LaAviso2G, True, 'Verificando tabela "' + TabNome + '"');

          if not Grl_DmkDB.TabelaAtiva(TabNome) then
          begin
            Item := Format(ivTabela_Nome, [TabNome]);

            if Lowercase(Copy(TabNome, 2, 4)) <> 'lct0' then
              MeTabsNoNeed.Lines.Add(TabNome);
          end else
          begin
            if VerificaCampos_SQLite(DBName, TabNome, TabBase, Memo,
              RecriaRegObrig, Tem_Del, Tem_Sync, LaAviso1B, LaAviso2B,
              LaAviso1G, LaAviso2G, MeFldsNoNeed, DataBase) <> rvOK then
            begin
              GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
              Exit;
            end;
          end;
          QrTabs.Next;
        end;

        MyObjects.Informa2(LaAviso1B, LaAviso2B, True, 'Verificando tabelas a serem criadas');

        if PB <> nil then
        begin
          PB.Max      := FTabelas.Count;
          PB.Position := 0;
        end;

        for i := 0 to FTabelas.Count -1 do
        begin
          MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"' + TabNome + '" verificando se existe');

          if PB <> nil then
            PB.Position := PB.Position + 1;

          DefTabela := FTabelas[i];
          TabNome   := DefTabela.TabCria;
          TabBase   := DefTabela.TabBase;
          Tem_Del   := DefTabela.Tem_Del;
          Tem_Sync  := DefTabela.Tem_Sync;

          if TabBase = '' then
            TabBase := TabNome;

          if not TabelaExiste_SQLite(TabNome, QrTabs) then
          begin
              MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '"' + TabNome + '" criando tabela ');
              CriaTabela_SQLite(DBName, TabNome, TabBase, Memo, actCreate,
                Tem_Del, LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G,
                DataBase, Tem_Sync);
          end;

        end;
        MyObjects.Informa2(LaAviso1G, LaAviso2G, True, '');
        QrTabs.Close;
      finally
        FTabelas.Free;
      end;
      MyObjects.Informa2(LaAviso1G, LaAviso2G, False, '...');
      MyObjects.Informa2(LaAviso1B, LaAviso2B, False, '...');
      MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));

      TempoF := Time();

      MyObjects.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));

      TempoT := TempoF - TempoI;

      if TempoT < 0 then
        TempoT := TempoT + 1;

      MyObjects.Informa(LaTempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
    except
      MyObjects.Informa2(LaAviso1R, LaAviso2R, False, Format(ivMsgFimAnalBD, [DBName]));
      raise;
    end;
  finally
    QrTabs.Free;
  end;
end;

function TUnVCL_ZDB.VerificaCampos_SQLite(DBName, TabelaNome,
  TabelaBase: String; Memo: TMemo; RecriaRegObrig, Tem_Del, Tem_Sync: Boolean;
  LaAviso1B, LaAviso2B, LaAviso1G, LaAviso2G: TLabel; MeFldsNoNeed: TMemo;
  DataBase: TZConnection): TResultVerify;
var
  i, k, Resp, EhIdx: Integer;
  Opcoes, Item, Nome, IdxNome: String;
  Indices: TStringList;
  IdxExiste: Boolean;
  TemControle: TTemControle;
  QrNTV, QrUpd, QrIdx: TFDQuery;
  SQL: String;
begin
  Result := rvOK;
  QrNTV := TFDQuery.Create(DataBase);
  QrUpd := TFDQuery.Create(DataBase);
  QrIdx := TFDQuery.Create(DataBase);
  try
    AbreSQLQuery0(QrNTV, DataBase, [
      'PRAGMA table_info(' + Lowercase(TabelaNome) + ');',
      '']);
    try
      FLCampos  := TList<TCampos>.Create;
      FLIndices := TList<TIndices>.Create;
      try
        FCriarAtivoAlterWeb := True;
        TemControle := [];
        MyList.CriaListaCampos(TabelaBase, FLCampos, TemControle);
        //
        if (tctrlLok in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Lk';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataCad';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'DataAlt';
          FRCampos.Tipo       := 'date';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlCad in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserCad';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAlt in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserAlt';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := 'YES';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlWeb in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'AlterWeb';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if (tctrlAti in TemControle) then
        begin
          New(FRCampos);
          FRCampos.Field      := 'Ativo';
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if Tem_Del then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'DataDel';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'MotvDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        if Tem_Sync then
        begin
          New(FRCampos);
          FRCampos.Field      := 'LastModifi';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'LastAcao';   //0 => Inclus�o
          FRCampos.Tipo       := 'tinyint(1)'; //1 => Altera��o
          FRCampos.Null       := '';           //2 => Exclus�o
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'CliIntSync';
          FRCampos.Tipo       := 'int';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0'; // 0=N�o info
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
        while not QrNTV.Eof do
        begin
          Nome := QrNTV.FieldByName('name').AsString;
          Item := Format(ivCampo_Nome, [TabelaNome, Nome]);

          case CampoAtivo_SQLite(DBName, TabelaNome, TabelaBase, Nome,
            Memo, QrNTV, DataBase) of
            1:
            begin
              try
                Opcoes := FRCampos.Tipo;
                if FRCampos.Null <> 'YES' then
                  Opcoes := Opcoes + ' NOT NULL ';
                if FRCampos.Default <> myco_ then
                  AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);
                //
                if FRCampos.Extra <> myco_ then
                begin
                  if Uppercase(FRCampos.Extra).IndexOf(Uppercase('auto_increment')) > 0 then
                    Opcoes := Opcoes + ' auto_increment ';
                end;
                RTL_Geral.MB_Aviso('O campo ' + Nome +
                  ' da tabela ' + TabelaNome +
                  ' difere do esperado e o RDBM SQLite n�o permite regulariz�-lo.');
              except
                GravaAviso(Item + ivMsgERROAlterar, Memo);
                raise;
              end;
            end;
            9:
            begin
              AdicionaFldNoNeed_SQLite(MeFldsNoNeed, TabelaNome, Nome);
                GravaAviso('O campo "' + TabelaNome + '.' + Nome +
                '" deveria ser exclu�do!', Memo);
            end;
          end;
          QrNTV.Next;
        end;
        for i:= 0 to FLCampos.Count - 1 do
        begin
          FRCampos := FLCampos[i];
          if CampoExiste_SQLite(TabelaNome, FRCampos.Field, Memo, QrNTV) = 2 then
          begin
            Item := Format(ivCampo_Nome, [TabelaNome, FRCampos.Field]);
            try
              Opcoes := FRCampos.Tipo;
              if FRCampos.Null <> 'YES' then
                Opcoes := Opcoes + ' NOT NULL ';
              if FRCampos.Default <> myco_ then
                AdicionaDefault_SQLite(Opcoes, FRCampos.Tipo, FRCampos.Default);
              //
              SQL := mycoALTERTABLE + TabelaNome + mycoADD + FRCampos.Field +
                mycoEspaco + Opcoes;
              if not FPergunta then Resp := mrYes else
              Resp := RTL_Geral.MB_Pergunta('O campo ' + FRCampos.Field +
              ' n�o existe na tabela ' + TabelaNome + ' e ser� criado.' +
              sLineBreak + 'Confirma a cria��o do campo?');
              if Resp = mrYes then
              begin
                ExecutaSQLQuery0(QrUpd, DataBase, [
                  SQL,
                  '']);
                GravaAviso(Item + ivcriado, Memo);
              end else begin
                GravaAviso(Item + ivAbortInclUser, Memo);
                if Resp = mrCancel then
                begin
                  Result := rvAbort;
                  GravaAviso(Item + ivAbortInclUser, Memo);
                  //
                  Exit;
                end;
              end;
            except
              GravaAviso(Item + ivMsgERROCriar, Memo);
              raise;
            end;
          end;
        end;
        Indices := TStringList.Create;
        Indices.Sorted := True;
        Indices.Duplicates := (dupIgnore);
        try
          for k := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[k];
            Indices.Add(FRIndices.Key_name);
          end;
          ExecutaSQLQuery0(QrIdx, DataBase, [
          'PRAGMA INDEX_LIST(''' + LowerCase(TabelaNome) + ''')',
          '']);

          while not QrIdx.Eof do
          begin
            IdxNome := QrIdx.FieldByName('name').AsString;
            begin
              IdxExiste := False;
              Item := Format(ivIndice_Nome, [TabelaNome, IdxNome]);
              for k := 0 to Indices.Count -1 do
              begin
                if UpperCase(Indices[k]) = 'PRIMARY' then
                begin
                  if UpperCase(IdxNome) =
                    Uppercase('idx_' + TabelaNome + '_primary') then
                      IdxExiste := True
                  else
                  if UpperCase(IdxNome) =
                    Uppercase('sqlite_autoindex_' + TabelaNome + '_1') then
                      IdxExiste := True;
                end else
                if Indices[k] = IdxNome then
                  IdxExiste := True;
              end;
              if not IdxExiste then
              begin
                if not FPergunta then Resp := mrYes else
                Resp := RTL_Geral.MB_Pergunta(Format(ivExclIndice,
                  [Application.Title, IdxNome, TabelaNome]));
                if Resp = mrYes then
                begin
                  if ExcluiIndice_SQLite(DBName, TabelaNome,
                    IdxNome, Item, mymotSemRef, Memo, DataBase) <> rvOK
                  then
                    Result := rvAbort;
                end else
                begin
                  GravaAviso(Item + ivAbortExclUser, Memo);

                  if Resp = mrCancel then
                  begin
                    Result := rvAbort;
                    Exit;
                  end;
                end;
              end;
            end;
            QrIdx.Next;
          end;
          QrIdx.Close;
          for k := 0 to Indices.Count -1 do
          begin
            EhIdx := IndiceExiste_SQLite(TabelaNome, Indices[k], Memo, DataBase);

            if EhIdx in ([2,3]) then
              if RecriaIndice_SQLite(TabelaNome, Indices[k], Item, Memo, DataBase) <> rvOK
              then begin
                Result := rvAbort;
                Exit;
              end;
            if EhIdx in ([0]) then
            begin
              if CriaIndice_SQLite(TabelaNome, Indices[k], Item, Memo,
                DataBase) <> rvOK then
              begin
                Result := rvAbort;
              end;
            end;
          end;
        finally
          Indices.Free;
        end;
        ////
      finally
        FLCampos.Free;
        FLIndices.Free;
      end;
    finally
      QrNTV.Close;
    end;
  finally
    if QrNTV <> nil then
      QrNTV.Free;
    if QrUpd <> nil then
      QrUpd.Free;
    if QrIdx <> nil then
      QrIdx.Free;
  end;
end;
*)

end.
