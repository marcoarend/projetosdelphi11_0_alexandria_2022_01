unit ModuleGeralZ;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables, ZAbstractRODataset,
  ZAbstractDataset, ZDataset;

type
  TDModGZ = class(TDataModule)
    QrSB5: TZQuery;
    QrSB5Codigo: TLargeintField;
    QrSB5CodUsu: TLargeintField;
    QrSB5Nome: TWideStringField;
    DsSB5: TDataSource;
    DsSB4: TDataSource;
    QrSB4: TZQuery;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrSB4CodUsu: TWideStringField;
    QrSB3: TZQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    QrSB3CodUsu: TIntegerField;
    DsSB3: TDataSource;
    DsSB2: TDataSource;
    DsSB: TDataSource;
    QrSB: TZQuery;
    QrSB2: TZQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    QrSB2CodUsu: TIntegerField;
    QrAux_: TZQuery;
    LargeintField1: TLargeintField;
    LargeintField2: TLargeintField;
    StringField1: TWideStringField;
    QrSBCodigo: TLargeintField;
    QrSBNome: TWideStringField;
    QrSBCodUsu: TLargeintField;
    QrNTV: TZQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DModGZ: TDModGZ;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

uses Module;

{$R *.dfm}

procedure TDModGZ.DataModuleCreate(Sender: TObject);
begin
  QrSB.Connection := Dmod.MyDB;
  //QrSB1.Connection := Dmod.MyDB;
  QrSB2.Connection := Dmod.MyDB;
  QrSB3.Connection := Dmod.MyDB;
  QrSB4.Connection := Dmod.MyDB;
  QrSB5.Connection := Dmod.MyDB;
  QrAux_.Connection := Dmod.MyDB;
end;

end.
