unit MeuDBUsesZ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, DBGrids, Buttons, StdCtrls, Menus, DB, dmkGeral,
  frxClass, frxDBSet, frxCross, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkEditF7, TypInfo, dmkDBEdit, UnDmkProcFunc, UnDmkEnums,
  mySQLDbTables, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  ZAbstractConnection, ZConnection, Rtti, UnGrl_dmkDB;

type
  TMyFuncType = function(a: integer; b: integer): string of object;

  TMyClass = class
  published
    function GetHvSrDBCad(a: integer; b: integer): string;
    //function Func2(a: integer; b: integer): string;
    //function Func3(a: integer; b: integer): string;
  public
    function Call(MethodName: string; a, b: integer): string;
  end;


  TFmMeuDBUsesZ = class(TForm)
    PnImprime: TPanel;
    Grade: TStringGrid;
    DBGrid1: TDBGrid;
    PnConfirma: TPanel;
    Button1: TButton;
    BtImprime: TBitBtn;
    Button2: TButton;
    PMImpressao: TPopupMenu;
    Visualizaimpresso1: TMenuItem;
    DesignerModificarelatrioantesdeimprimir1: TMenuItem;
    ImprimeImprimesemvizualizar1: TMenuItem;
    frxReport1: TfrxReport;
    frxCrossObject1: TfrxCrossObject;
    frxReport2: TfrxReport;
    frxDsPG: TfrxDBDataset;
    DsPG: TDataSource;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PnInativo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TEdit;
    EdCPF: TEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    dmkDBLookupComboBox1: TdmkDBLookupComboBox;
    dmkEditCB1: TdmkEditCB;
    dmkEditF71: TdmkEditF7;
    QrPG: TZQuery;
    QrUpd: TZQuery;
    frxSG: TfrxReport;
    frxCOGB: TfrxCrossObject;
    procedure Button2Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Visualizaimpresso1Click(Sender: TObject);
    procedure DesignerModificarelatrioantesdeimprimir1Click(Sender: TObject);
    procedure ImprimeImprimesemvizualizar1Click(Sender: TObject);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure frxSGBeforePrint(Sender: TfrxReportComponent);
  private
    { Private declarations }
    FPrintGrid: String;
    FTitulo: String;
    FGrade: TStringGrid;
    FDBGrid: TDBGrid;
    FTipoGrade: Integer;
    FContaCol: Integer;
    procedure Imprime_frx2(Mostra: Byte);
    procedure ExecMethod(MethodName: String; const Args: array of TValue);
    function  ExecMethodExist(MethodName: String): Boolean;

  public
    { Public declarations }
    //function  ConectaDBTest(): Boolean;
    procedure ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
    procedure ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
    procedure ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
    procedure PesquisaNome(Tabela, CampoNome, CampoCodigo: String;
              DefinedSQL: String = '');
    procedure VirtualKey_F5();
    procedure VirtualKey_F7(Texto: String);
    procedure VirtualKey_F8();
    procedure ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor: Variant;
              var UpdType: TUpdType);
    function  ObtemCodigoENomeDeEnum(Titulo: String; const MyArrStr:
              array of String; const MyArrInt: array of Integer; var Codigo:
              Integer; var Nome: String): Boolean;
  end;

var
  FmMeuDBUsesZ: TFmMeuDBUsesZ;
  PID_DB_TEST: TmySQLDatabase;
  AID_DB_TEST: TmySQLDatabase;


implementation

uses UnMyObjects, (***UCreateEDrop,***) PesqNomeZ, Module, UnInternalConsts, UnMsgInt,
Printers, ModuleGeralZ, DmkDAC_PF, SelOnStringGrid;

{$R *.dfm}

{ TFmMeuDBUses }

procedure TFmMeuDBUsesZ.BtImprimeClick(Sender: TObject);
begin
  Imprime_frx2(1);
end;

procedure TFmMeuDBUsesZ.Button1Click(Sender: TObject);
begin
  Hide;
  PnImprime.Visible := False;
  PnInativo.Visible := True;
end;

procedure TFmMeuDBUsesZ.Button2Click(Sender: TObject);
begin
  //FmPrincipal.SalvaArquivo(EdNome, EdCPF, Grade, Date, 'Dermatek.ini', True);
end;

procedure TFmMeuDBUsesZ.DesignerModificarelatrioantesdeimprimir1Click(
  Sender: TObject);
begin
  Imprime_Frx2(1);
end;

procedure TFmMeuDBUsesZ.ExecMethod(MethodName: string;
  const Args: array of TValue);
{
var
 R : TRttiContext;
 T : TRttiType;
 M : TRttiMethod;
}
begin
{ N�o funcionou
  T := R.GetType(TProcClass);
  for M in t.GetMethods do
    if (m.Parent = t) and (m.Name = MethodName) then
      M.Invoke(TProcClass, Args)
}
end;

function TFmMeuDBUsesZ.ExecMethodExist(MethodName: String): Boolean;
var
 R : TRttiContext;
 T : TRttiType;
 M : TRttiMethod;
begin
  Result := False;
  //T := R.GetType(TProcClass);
  T := R.GetType(TMyClass);
  for M in t.GetMethods do
  begin
    if (m.Parent = t) and (m.Name = MethodName) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

procedure TFmMeuDBUsesZ.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    if FTipoGrade = 0 then
    begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[j - 1, i - 1]]);
    end else begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[i - 1, j - 1]]);
    end;
  end;
end;

procedure TFmMeuDBUsesZ.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'Titulo' then Value := FTitulo;
end;

procedure TFmMeuDBUsesZ.frxSGBeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    for i := 1 to FGrade.ColCount do
      for j := 1 to FGrade.RowCount do
        Cross.AddValue([i], [j], [FGrade.Cells[i - 1, j - 1]]);
  end;
end;

procedure TFmMeuDBUsesZ.ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
var
  Query: TZQuery;
begin
  Query := TZQuery(DBGrid.DataSource.DataSet);
  frxDsPG.DataSet := Query;
  //
  FTipoGrade := 1; // Grade com origem de DataAware. DBGrid ...
  FTitulo := Titulo;
  FDBGrid := DBGrid;
  FGrade := TStringGrid(DBGrid);
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUsesZ.ImprimeImprimesemvizualizar1Click(Sender: TObject);
begin
  Imprime_Frx2(2);
end;

procedure TFmMeuDBUsesZ.ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
{
var
  Cross: TfrxCrossView;
  i, j: Integer;
}
begin
  FGrade := StringGrid;
  frxSG.ShowReport;
{
  Cross := frxSG.FindObject('Cross1') as TfrxCrossView;
  //if c is TfrxCrossView then
  begin
    //Cross := TfrxCrossView(c);
    for i := 1 to StringGrid.ColCount do
      for j := 1 to StringGrid.RowCount do
        Cross.AddValue([i], [j], [StringGrid.Cells[i - 1, j - 1]]);
    //
   frxSG.ShowReport;
 end;
}



(*** desmarcar e FAZER!!!
var
  i, j, c, r: Integer;
  Xt: array of String;
  Xv: array of String;
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  c := StringGrid.ColCount;
  r := StringGrid.RowCount;
  // 2012-02-24
  //ConectaDBTest();
  //QrUpd.Database := DBTest;
  QrUpd.Database := DmodGZ.MyPID_DB;
  FPrintGrid := UCriaEDrop.RecriaDropedTab(ntrtt_PrintGrid, QrUpd, c);
  SetLength(Xt, StringGrid.ColCount);
  SetLength(Xv, StringGrid.ColCount);
  for i := 0 to c - 1 do
    Xt[i] := 'Col' + FormatFloat('000', i);
  for i := 0 to r - 1 do
  begin
    for j := 0 to c - 1 do
      Xv[j] := StringGrid.Cells[j,i];
    //
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('INSERT INTO ' + FPrintGrid + ' SET ');
    for j := 0 to c - 2 do
      QrUpd.SQL.Add(Xt[j] + '="' + dmkPF.DuplicaAspasDuplas(Xv[j]) + '",');
    QrUpd.SQL.Add(Xt[c-1] + '="' + dmkPF.DuplicaAspasDuplas(Xv[c-1]) + '"');

    QrUpd.ExecSQL;
  end;
  FGrade  := StringGrid;
  FDBGrid := nil;
  FTitulo := Titulo;
  // 2012-02-24
  //QrPg.Database   := DBTest;
  //frxDsPG.DataSet := QrPG;
  QrPg.Close;
  QrPg.Database   := DmodGZ.MyPID_DB;
  frxDsPG.DataSet := QrPG;
  QrPG.SQL.Clear;
  QrPG.SQL.Add('SELECT * ');
  QrPG.SQL.Add('FROM ' + FPrintGrid);
  QrPG.SQL.Add(';');
  QrPG.SQL.Add('DROP TABLE ' + FPrintGrid);
  QrPG.SQL.Add(';');
  UnDmkDAC_PF.AbreQuery(QrPG, DmodGZ.MyPID_DB);
  //  fim 2012-02-24
  MyObjects.MostraPopUpNoCentro(PMImpressao);
***)
end;

procedure TFmMeuDBUsesZ.ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  FGrade := StringGrid;
  FTitulo := Titulo;
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUsesZ.Imprime_frx2(Mostra: Byte);
const
  PagePagHei = 279;

  PageBanWid = 0;
  PagePagTop = 117; // >(=1,01) - (1,91 = 200)
  PagePagFot = 100;

  PageBanLef = 100;
  PageBanTop = 0;
  //PageBanWid = 0;
  PageBanHei = 50;
  PageMemHei = 50;
  PageColWid = 210;

var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  Foot: TfrxPageFooter;
  BANLEF, BANTOP, BANWID, BANHEI,
  //BARLEF, BARTOP, BARWID, BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  i,
  L, T, W, H, (*k, *)TopoIni: Integer;
  //Fator: Double;
  Campo: String;
  Menos: Byte;
  Orientacao: Integer;
begin
  Orientacao := MyObjects.SelRadioGroup('Orienta��o de Impress�o',
  'Selecione a orienta��o', ['Retrato', 'Paisagem'], 1);
  if not (Orientacao in ([0,1])) then
    Exit;
  Menos := 0;
  //k := -1;
  // Configura p�gina
  while frxReport2.PagesCount > 0 do
    frxReport2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxReport2);
  Page.CreateUniqueName;
  Page.LeftMargin   := 20;
  Page.RightMargin  := 10;
  Page.TopMargin    := 10;
  Page.BottomMargin := 10;
  Page.Height       := Trunc(PagePagHei / VAR_DOTIMP);
  case Orientacao of 
    0: Page.Orientation := poPortrait;
    1: Page.Orientation := poLandscape;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(PageBanWid / VAR_DOTIMP);
  H := Trunc(PagePagTop / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  TITULO DO RELAT�RIO
  FContaCol := 0;
  //TopoIni    := 0;
  MEMTOP := 0;
  MEMHEI := 24;//Trunc(PageMemHei / VAR_DOTIMP); 1,59 = 60 0,59=
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 1800;
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 12;
  //
  Memo.Memo.Text := FTitulo;
{
  if CkGrade.Checked then
  begin
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.TopLine.Width := 0.1;
    Memo.Frame.LeftLine.Width := 0.1;
    Memo.Frame.RightLine.Width := 0.1;
    Memo.Frame.BottomLine.Width := 0.1;
  end;
}
  ///  T�tulos dos campos
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    //  TITULOS DAS COLUNAS
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni + 24;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := 'Teste 1'
    else begin
      Memo.Memo.Text := FDBGrid.Columns[i].Title.Caption;
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  // Configura Banda de
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(PageBanTop / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PageBanHei / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.DataSet := frxDsPG;
  {
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  }
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i + Menos - 1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 10;
    //
    if frxDsPG.DataSet = QrPG then
      Memo.Memo.Text := '[frxDsPG."Col' + FormatFloat('000', i) + '"]'
    else begin
      Campo := FDBGrid.Columns[i].FieldName;
      Memo.Memo.Text := '[frxDsPG."' + Campo + '"]';
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  {
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_TXT1]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_NOME]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  }
  //////////////////////////////////////////////////////////////////////////////
  // Configura Foota
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(269 / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PagePagFot / VAR_DOTIMP);
  Foot := TfrxPageFooter.Create(Page);
  Foot.CreateUniqueName;
  Foot.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
{$IFNDEF NAO_USA_FRX}
  case Mostra of
    0: MyObjects.frxMostra(frxReport2, FTitulo);
    1: frxReport2.DesignReport();
    2: MyObjects.frxImprime(frxReport2, FTitulo);
  end;
{$ELSE}
  Geral.MB_Info('"FRX" sem "DEFINE"!');
{$ENDIF}

end;

procedure TFmMeuDBUsesZ.MenuItem1Click(Sender: TObject);
begin
  Visualizaimpresso1Click(Self);
end;

procedure TFmMeuDBUsesZ.MenuItem2Click(Sender: TObject);
begin
  DesignerModificarelatrioantesdeimprimir1Click(Self);
end;

procedure TFmMeuDBUsesZ.MenuItem3Click(Sender: TObject);
begin
  ImprimeImprimesemvizualizar1Click(Self);
end;

function TFmMeuDBUsesZ.ObtemCodigoENomeDeEnum(Titulo: String; const MyArrStr:
  array of String; const MyArrInt: array of Integer; var Codigo: Integer; var
  Nome: String): Boolean;
var
  I: Integer;
begin
  Codigo :=  0;
  Nome   :=  '';
  //
  if Grl_dmkDB.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := Titulo;
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Grade.RowCount := High(MyArrInt) + 1;
      //
      for I := Low(MyArrInt) to High(MyArrInt) do
      begin
        Grade.Cells[0, I] := IntToStr(I + 1);
        Grade.Cells[1, I] := Geral.FF0(MyArrInt[I]);
        Grade.Cells[2, I] := MyArrStr[I];
      end;
      ShowModal;
      if FResult <> '' then
      begin
        with FmSelOnStringGrid do
        begin
          Codigo := Geral.IMV(Grade.Cells[1, Grade.Row]);
          Nome   := Grade.Cells[2, Grade.Row];
        end;
      end;
      Destroy;
    end;
  end;
end;

procedure TFmMeuDBUsesZ.ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor:
  Variant; var UpdType: TUpdType);
var
  Campo: String;
begin
  Campo := GetPropValue(Objeto, 'UpdCampo');
  try
  Valor := TmySQLDataSet(TDataSource(TdmkDBEdit(Objeto).DataSource).
    DataSet).FieldByName(Campo).Value;
  UpdType := TdmkDBEdit(Objeto).UpdType;
  except
    Geral.MB_Erro('N�o foi poss�vel obter o "Tabela.Campo.Valor" do TDBEdit! ' +
    sLineBreak + 'TDBEdit: ' + TdmkDBEdit(Objeto).Name +
    sLineBreak + 'Campo: ' + TdmkDBEdit(Objeto).DataField);
    //
    raise;
  end;
end;

procedure TFmMeuDBUsesZ.PesquisaNome(Tabela, CampoNome, CampoCodigo, DefinedSQL: String);
begin
  VAR_CADASTRO := 0;
  //
  MyObjects.CriaForm_AcessoTotal(TFmPesqNomeZ, FmPesqNomeZ);
  //
  FmPesqNomeZ.LaDoc.Visible := False;
  FmPesqNomeZ.EdDoc.Visible := False;
  //
  FmPesqNomeZ.FND              := True;
  FmPesqNomeZ.FNomeTabela      := Tabela;
  FmPesqNomeZ.FNomeCampoNome   := CampoNome;
  FmPesqNomeZ.FNomeCampoCodigo := CampoCodigo;
  FmPesqNomeZ.FDefinedSQL      := DefinedSQL;
  FmPesqNomeZ.ShowModal;
  FmPesqNomeZ.Destroy;
end;

procedure TFmMeuDBUsesZ.Visualizaimpresso1Click(Sender: TObject);
begin
  Imprime_Frx2(0);
end;

procedure TFmMeuDBUsesZ.VirtualKey_F5();
var
  Controle: TControl;
  OK: Boolean;
  DBLookupComboBox: TDBLookupComboBox;
  DataSource: TDataSource;
  DataSet: TDataSet;
begin
  if Screen.ActiveForm = nil then Exit;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB)(* or
    (Controle is TdmkEditF7)*);
    if OK then
    begin
      if Controle is TDBLookupComboBox then
        DBLookupComboBox := TDBLookupComboBox(Controle)
      else
      if Controle is TdmkDBLookupComboBox then
        DBLookupComboBox := TdmkDBLookupComboBox(Controle)
      else
      if Controle is TdmkEditCB then
        DBLookupComboBox := TdmkDBLookupComboBox(TdmkEditCB(Controle).DBLookupComboBox)
      else
      //if Controle is TdmkEditF7 then
        DBLookupComboBox := nil;
      //
      if DBLookupComboBox <> nil then
      begin
        DataSource := TDataSource(DBLookupComboBox.ListSource);
        if DataSource <> nil then
        begin
          DataSet := TDataSet(DataSource.DataSet);
          if DataSet <> nil then
          begin
            if DataSet is TmySQLDataSet then
            begin
              // Evitar quebra de seguran�a!
              // S� abrir se j� estiver aberto!
              if TdataSet(DataSet).State <> dsInactive then
              begin
                TdataSet(DataSet).Close;
                TdataSet(DataSet). Open ;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmMeuDBUsesZ.VirtualKey_F7(Texto: String);
var
  Controle: TControl;
  OK: Boolean;
  LS: TDataSource;
  Qr: TZQuery;
  dmkCB: TDmkDBLookupComboBox;
  dmkEd: TdmkEditCB;
  //
var
  MyClass: TMyClass;
begin
  if Screen.ActiveForm = nil then Exit;
  VAR_CaptionFormOrigem := TForm(Screen.Activeform).Caption;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    if (Controle is TdmkEditF7) then
    begin
{
      if ExecMethodExist(TdmkEditF7(Controle).LocF7TableName) then
      begin
        //if
        ExecMethod(TdmkEditF7(Controle).LocF7TableName, [
        TdmkEditF7(Controle).LocF7NameFldName,
        TdmkEditF7(Controle).LocF7CodiFldName]);
        //
        TdmkEditF7(Controle).ValueVariant := VAR_CADASTRO;
      end;
}
      if ExecMethodExist(TdmkEditF7(Controle).LocF7TableName) then
      begin
        MyClass := TMyClass.Create;
        MyClass.Call(TdmkEditF7(Controle).LocF7TableName, 0, 0);
        //MyClass.Call(TdmkEditF7(Controle).LocF7TableName, 6, 4);
        MyClass.Destroy;
        TdmkEditF7(Controle).ValueVariant := VAR_CADASTRO;
        //
        Exit;
      end;
    end;
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB) or
    (Controle is TdmkEditF7);
    if OK then
    begin
      VAR_CADASTRO := 0;
      MyObjects.CriaForm_AcessoTotal(TFmPesqNomeZ, FmPesqNomeZ);
      if (Controle is TdmkEditCB) then
        FmPesqNomeZ.EdTxtPesq.Text := TdmkEditCB(Controle).TxtPesq;
      FmPesqNomeZ.FTexto := Texto;
      if Controle is TdmkEditF7 then
      begin
        {
        if (Controle is TdmkEditF7) then
          Controle := TdmkEditF7(Controle).dmkEditCod;
        }
        FmPesqNomeZ.FF7 := TdmkEdit(Controle);
        FmPesqNomeZ.FCB := nil;
        VAR_NomeCompoF7 := TdmkEdit(Controle).Name;
        //
      end else
      begin
        if (Controle is TdmkEditCB) then
          Controle := TdmkEditCB(Controle).DBLookupComboBox;
        FmPesqNomeZ.FCB := TDBLookupComboBox(Controle);
        FmPesqNomeZ.FF7 := TdmkEdit(Controle);
        VAR_NomeCompoF7 := TDBLookupComboBox(Controle).Name;
      end;
      FmPesqNomeZ.FDB := nil;
      if (FmPesqNomeZ.FCB <> nil) and (FmPesqNomeZ.FCB.ListSource <> nil) then
      begin
        LS := TDataSource(FmPesqNomeZ.FCB.ListSource);
        if (LS <> nil) and (LS.Dataset <> nil) then
        begin
          Qr := TZQuery(LS.DataSet);
          if (Qr <> nil) and (Qr.Connection <> nil) then
            FmPesqNomeZ.FDB := TZConnection(Qr.Connection);
        end;
      end;
      if FmPesqNomeZ.FDB = nil then
        FmPesqNomeZ.FDB := Dmod.MyDB;
      FmPesqNomeZ.ShowModal;
      if VAR_CADASTRO <> 0 then
      begin
        if FmPesqNomeZ.FCB <> nil then
        begin
          if Controle is TdmkDBLookupComboBox then
          begin
            dmkCB := TdmkDBLookupComboBox(Controle);
            if dmkCB <> nil then
            begin
              if dmkCB.ListField = dmkCB.KeyField then
                dmkCB.KeyValue := VAR_CAD_NOME
              else
                dmkCB.KeyValue := VAR_CADASTRO;
              dmkEd := TdmkEditCB(dmkCB.dmkEditCB);
              if dmkEd <> nil then
              begin
                dmkEd.ValueVariant := VAR_CADASTRO;
                dmkEd.OnFim;
              end;
            end;
          end else
            TDBLookupComboBox(Controle).KeyValue := VAR_CADASTRO;
        end
        else
        if FmPesqNomeZ.FF7 <> nil then
        begin
          case TdmkEditF7(Controle).FormatType of
            dmktfString: TdmkEditF7(Controle).Text := VAR_CAD_NOME;
            dmktfInt64,
            dmktfInteger,
            dmktfLongint: TdmkEditF7(Controle).ValueVariant := VAR_CADASTRO;
          end;
          if (TdmkEditF7(Controle).dmkEditCod <> nil) then
          begin
            //Show Message(TdmkEditF7(Controle).Name);
            //Show Message(TdmkEdit(TdmkEditF7(Controle).dmkEditCod).Name);
            TdmkEdit(TdmkEditF7(Controle).dmkEditCod).ValueVariant := VAR_CADASTRO;
          end;
        end;
      end;
      VAR_CaptionFormOrigem := '';
      VAR_NomeCompoF7 := '';
      FmPesqNomeZ.Destroy;
    end;
  end;
end;

procedure TFmMeuDBUsesZ.VirtualKey_F8();
begin

end;

{ TMyClass }

function TMyClass.Call(MethodName: string; a, b: integer): string;
var
  m: TMethod;
begin
  m.Code := Self.MethodAddress(MethodName); //find method code
  m.Data := pointer(Self); //store pointer to object instance
  Result := TMyFuncType(m)(a, b);
end;

function TMyClass.GetHvSrDBCad(a, b: integer): string;
var
  Codigo: Integer;
  Nome: String;
begin
  FmMeuDBUsesZ.ObtemCodigoENomeDeEnum('Forma de obten��o da informa��o',
  sHaveSrcDB, iHaveSrcDB, VAR_CADASTRO, VAR_CAD_NOME);
end;

end.
