u n i t   T e s t e 3 2 ;  
  
 i n t e r f a c e  
  
 u s e s  
     S t d C t r l s ,   E x t C t r l s ,   W i n d o w s ,   M e s s a g e s ,   S y s U t i l s ,   C l a s s e s ,   G r a p h i c s ,   C o n t r o l s ,  
     F o r m s ,   D i a l o g s ,   M e n u s ,   U n M s g I n t ,   U n I n t e r n a l C o n s t s ,   D b ,   D b C t r l s ,   U n  
     U n I n t e r n a l C o n s t s 2 ,   C o m C t r l s ,   R e g i s t r y ,   T y p I n f o ,   d m k E d i t ,   d m k E d i t C B ,   d m k G e r a l ,  
     d m k E d i t D a t e T i m e P i c k e r ,   d m k D B L o o k u p C o m b o b o x ,   d m k C h e c k B o x ,   d m k R a d i o G r o u p ,  
     d m k D B E d i t ,   d m k V a l U s u ,   d m k C h e c k G r o u p ,   m y S Q L D b T a b l e s ,   d m k M e m o ,   V a r i a n t s ,  
     G r i d s ,   B u t t o n s ,   D B G r i d s ;  
  
 t y p e  
     T T e s t e 3 2   =   c l a s s ( T O b j e c t )  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
         f u n c t i o n   G e n e r a t e P W D S e c u t i t y S t r i n g :   s t r i n g ;  
         f u n c t i o n   E n c o d e P W D E x ( D a t a ,   S e c u r i t y S t r i n g :   s t r i n g ;   M i n V :   I n t e g e r   =   0 ;  
                           M a x V :   I n t e g e r   =   5 ) :   s t r i n g ;  
         f u n c t i o n   D e c o d e P W D E x ( D a t a ,   S e c u r i t y S t r i n g :   s t r i n g ) :   s t r i n g ;  
     e n d ;  
  
 c o n s t  
     C o d e s 3 2   =   ' A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 3 4 5 6 7 8 ' ;  
  
 v a r  
     T 3 2 :   T T e s t e 3 2 ;  
  
 i m p l e m e n t a t i o n  
  
 / / u s e s   ;  
  
  
     / /   y o u   m u s t   u s e   t h i s   f u n c t i o n   t o   g e n e r a t e   s p e c i a l  
     / /   s e c u r i t y   s t r i n g ,   w h i c h   i s   u s e d   i n   m a i n   e n c o d e / d e c o d e   r o u t i n e s .  
     / /   N O T E :   y o u   m u s t   g e n e r a t e   t h e   s e c u r i t y   s t r i n g   o n l y   o n c e   a n d   t h e n   u s e   i t   i n   e n c o d e / d e c o d e   f u n c t i o n s .  
  
 f u n c t i o n   T T e s t e 3 2 . G e n e r a t e P W D S e c u t i t y S t r i n g :   s t r i n g ;  
 v a r  
     i ,   x :   i n t e g e r ;  
     s 1 ,   s 2 :   s t r i n g ;  
 b e g i n  
     s 1   : =   C o d e s 3 2 ;  
     s 2   : =   ' ' ;  
     f o r   i   : =   0   t o   1 5   d o  
     b e g i n  
         x     : =   R a n d o m ( L e n g t h ( s 1 ) ) ;  
         x     : =   L e n g t h ( s 1 )   -   x ;  
         s 2   : =   s 2   +   s 1 [ x ] ;  
         s 1   : =   C o p y ( s 1 ,   1 , x   -   1 )   +   C o p y ( s 1 ,   x   +   1 , L e n g t h ( s 1 ) ) ;  
     e n d ;  
     R e s u l t   : =   s 2 ;  
 e n d ;  
  
 / /   t h i s   f u n c t i o n   g e n e r a t e   r a n d o m   s t r i n g   u s i n g  
 / /   a n y   c h a r a c t e r s   f r o m   " C H A R S "   s t r i n g   a n d   l e n g t h  
 / /   o f   " C O U N T "   -   i t   w i l l   b e   u s e d   i n   e n c o d e   r o u t i n e  
 / /   t o   a d d   " n o i s e "   i n t o   y o u r   e n c o d e d   d a t a .  
  
 f u n c t i o n   M a k e R N D S t r i n g ( C h a r s :   s t r i n g ;   C o u n t :   I n t e g e r ) :   s t r i n g ;  
 v a r  
     i ,   x :   i n t e g e r ;  
 b e g i n  
     R e s u l t   : =   ' ' ;  
     f o r   i   : =   0   t o   C o u n t   -   1   d o  
     b e g i n  
         x   : =   L e n g t h ( c h a r s )   -   R a n d o m ( L e n g t h ( c h a r s ) ) ;  
         R e s u l t   : =   R e s u l t   +   c h a r s [ x ] ;  
         c h a r s   : =   C o p y ( c h a r s ,   1 , x   -   1 )   +   C o p y ( c h a r s ,   x   +   1 , L e n g t h ( c h a r s ) ) ;  
     e n d ;  
 e n d ;  
  
  
 / /   T h i s   w i l l   e n c o d e   y o u r   d a t a .  
 / /   " S e c u r i t y S t r i n g "   m u s t   b e   g e n e r a t e d   u s i n g   m e t h o d  
 / /   d e s c r i b e d   a b o v e ,   a n d   t h e n   s t o r e d   a n y w h e r e   t o  
 / /   u s e   i t   i n   D e c o d e   f u n c t i o n .  
 / /   " D a t a "   i s   y o u r   s t r i n g   ( y o u   c a n   u s e   a n y   c h a r a c t e r s   h e r e )  
 / /   " M i n V "   -   m i n i m u m   q u a n t i t y   o f   " n o i s e "   c h a r s   b e f o r e   e a c h   e n c o d e d   d a t a   c h a r .  
 / /   " M a x V "   -   m a x i m u m   q u a n t i t y   o f   " n o i s e "   c h a r s   b e f o r e   e a c h   e n c o d e d   d a t a   c h a r .  
  
 f u n c t i o n   T T e s t e 3 2 . E n c 