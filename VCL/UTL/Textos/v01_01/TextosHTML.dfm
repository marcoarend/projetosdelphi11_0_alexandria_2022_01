object FmTextosHTML: TFmTextosHTML
  Left = 0
  Top = 0
  Caption = 'TXT-TEXTO-002 :: Editor de Textos HTML Dermatek'
  ClientHeight = 528
  ClientWidth = 1007
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object ToolBar2: TToolBar
    Left = 0
    Top = 38
    Width = 1007
    Height = 38
    AutoSize = True
    ButtonHeight = 38
    ButtonWidth = 39
    Caption = 'ToolBar2'
    Images = FmMyGlyfs.Lista_32X32_Textos
    TabOrder = 0
    object ToolButton29: TToolButton
      Left = 0
      Top = 0
      Hint = 'Cor da Fonte'
      Caption = 'Font Color'
      ImageIndex = 15
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton29Click
    end
    object ToolButton30: TToolButton
      Left = 39
      Top = 0
      Width = 8
      Caption = 'ToolButton30'
      ImageIndex = 27
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 47
      Top = 0
      Hint = 'Negrito'
      Caption = 'Bold'
      ImageIndex = 8
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = ToolButton7Click
    end
    object ToolButton11: TToolButton
      Left = 86
      Top = 0
      Hint = 'It'#225'lico'
      Caption = 'Italic'
      ImageIndex = 9
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = ToolButton11Click
    end
    object ToolButton12: TToolButton
      Left = 125
      Top = 0
      Caption = 'Underline'
      ImageIndex = 10
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = ToolButton12Click
    end
    object ToolButton13: TToolButton
      Left = 164
      Top = 0
      Hint = 'Tachado'
      Caption = 'Strikethrough'
      ImageIndex = 22
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = ToolButton13Click
    end
    object ToolButton16: TToolButton
      Left = 203
      Top = 0
      Width = 8
      Caption = 'ToolButton16'
      ImageIndex = 24
      Style = tbsSeparator
    end
    object TTBSub: TToolButton
      Left = 211
      Top = 0
      Hint = 'Subscrito'
      Caption = 'Subscript'
      ImageIndex = 24
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = TTBSubClick
    end
    object TTBSob: TToolButton
      Left = 250
      Top = 0
      Hint = 'Sobrescrito'
      Caption = 'Superscript'
      ImageIndex = 23
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = TTBSobClick
    end
    object ToolButton17: TToolButton
      Left = 289
      Top = 0
      Width = 8
      Caption = 'ToolButton17'
      ImageIndex = 24
      Style = tbsSeparator
    end
    object ToolButton18: TToolButton
      Left = 297
      Top = 0
      Hint = 'Lista - Marcadores'
      Caption = 'Markers'
      ImageIndex = 14
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton18Click
    end
    object ToolButton19: TToolButton
      Left = 336
      Top = 0
      Hint = 'Lista - Numera'#231#227'o'
      Caption = 'Numbering'
      ImageIndex = 17
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton19Click
    end
    object ToolButton22: TToolButton
      Left = 375
      Top = 0
      Width = 8
      Caption = 'ToolButton22'
      ImageIndex = 19
      Style = tbsSeparator
    end
    object ToolButton20: TToolButton
      Left = 383
      Top = 0
      Hint = 'Diminuir recuo'
      Caption = 'Decrease Indent'
      ImageIndex = 18
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton20Click
    end
    object ToolButton21: TToolButton
      Left = 422
      Top = 0
      Hint = 'Aumentar recuo'
      Caption = 'Increase Indent'
      ImageIndex = 19
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton21Click
    end
    object ToolButton23: TToolButton
      Left = 461
      Top = 0
      Width = 8
      Caption = 'ToolButton23'
      ImageIndex = 19
      Style = tbsSeparator
    end
    object TTBAlinEsq: TToolButton
      Left = 469
      Top = 0
      Hint = 'Alinhar '#224' esquerda'
      Caption = 'Align Left'
      ImageIndex = 11
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = TTBAlinEsqClick
    end
    object TTBAlinCen: TToolButton
      Left = 508
      Top = 0
      Hint = 'Centralizar'
      Caption = 'Center'
      ImageIndex = 12
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = TTBAlinCenClick
    end
    object TTBAlinDir: TToolButton
      Left = 547
      Top = 0
      Hint = 'Alinhar '#224' direita'
      Caption = 'Align Right'
      ImageIndex = 13
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = TTBAlinDirClick
    end
    object TTBAlinJus: TToolButton
      Left = 586
      Top = 0
      Hint = 'Justificar'
      Caption = 'Justify'
      ImageIndex = 25
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = TTBAlinJusClick
    end
    object ToolButton15: TToolButton
      Left = 625
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 27
      Style = tbsSeparator
    end
    object ToolButton24: TToolButton
      Left = 633
      Top = 0
      Hint = 'Adicionar Link'
      Caption = 'Link'
      ImageIndex = 21
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton24Click
    end
    object ToolButton31: TToolButton
      Left = 672
      Top = 0
      Hint = 'Inserir Imagem'
      Caption = 'Image'
      ImageIndex = 27
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton31Click
    end
    object TBVideo: TToolButton
      Left = 711
      Top = 0
      Caption = 'TBBVideo'
      ImageIndex = 29
      OnClick = TBVideoClick
    end
    object ToolButton32: TToolButton
      Left = 750
      Top = 0
      Hint = 'Inserir Tabela'
      Caption = 'Insert Table'
      ImageIndex = 28
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton32Click
    end
    object ToolButton14: TToolButton
      Left = 789
      Top = 0
      Hint = 'Inserir linha'
      Caption = 'Line'
      ImageIndex = 20
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton14Click
    end
    object ToolButton27: TToolButton
      Left = 828
      Top = 0
      Width = 8
      Caption = 'ToolButton27'
      ImageIndex = 22
      Style = tbsSeparator
    end
    object ToolButton28: TToolButton
      Left = 836
      Top = 0
      Hint = 'Pesquisa'
      Caption = 'Search'
      ImageIndex = 26
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton28Click
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1007
    Height = 38
    AutoSize = True
    ButtonHeight = 38
    ButtonWidth = 39
    Caption = 'ToolBar1'
    Images = FmMyGlyfs.Lista_32X32_Textos
    TabOrder = 1
    object TTBNovo: TToolButton
      Left = 0
      Top = 0
      Hint = 'Cria um novo arquivo'
      Caption = 'New'
      ImageIndex = 0
      ParentShowHint = False
      ShowHint = True
      OnClick = TTBNovoClick
    end
    object TTBAbrir: TToolButton
      Left = 39
      Top = 0
      Hint = 'Abre arquivo existente'
      Caption = 'Open'
      ImageIndex = 1
      ParentShowHint = False
      ShowHint = True
      OnClick = TTBAbrirClick
    end
    object TTBSalvar: TToolButton
      Left = 78
      Top = 0
      Hint = 'Salva o arquivo atual'
      Caption = 'Save'
      ImageIndex = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = TTBSalvarClick
    end
    object ToolButton5: TToolButton
      Left = 117
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 125
      Top = 0
      Hint = 'Imprime o arquivo atual'
      Caption = 'Print'
      ImageIndex = 3
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton4Click
    end
    object ToolButton6: TToolButton
      Left = 164
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 172
      Top = 0
      Hint = 'Corta a sele'#231#227'o atual'
      Caption = 'Cut'
      ImageIndex = 5
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton8Click
    end
    object ToolButton9: TToolButton
      Left = 211
      Top = 0
      Hint = 'Copia a sele'#231#227'o atual para a '#225'rea de transfer'#234'ncia'
      Caption = 'Copy'
      ImageIndex = 6
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton9Click
    end
    object ToolButton10: TToolButton
      Left = 250
      Top = 0
      Hint = 'Cola dados da '#225'rea de transfer'#234'ncia'
      Caption = 'Paste'
      ImageIndex = 7
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton10Click
    end
    object ToolButton26: TToolButton
      Left = 289
      Top = 0
      Width = 8
      Caption = 'ToolButton26'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton25: TToolButton
      Left = 297
      Top = 0
      Hint = 'Desfaz a '#250'ltima a'#231#227'o'
      Caption = 'Undo'
      ImageIndex = 4
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton25Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 95
    Width = 1007
    Height = 433
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    TabPosition = tpBottom
    OnChange = PageControl1Change
    ExplicitTop = 188
    ExplicitHeight = 340
    object TabSheet1: TTabSheet
      Caption = 'Texto'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object WBTexto: TWebBrowser
        Left = 0
        Top = 0
        Width = 999
        Height = 404
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ControlData = {
          4C00000084520000672100000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E12620A000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'HTML'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object MeHTML: TMemo
        Left = 0
        Top = 0
        Width = 999
        Height = 404
        Align = alClient
        Lines.Strings = (
          'MeHTML')
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  object ToolBar3: TToolBar
    Left = 0
    Top = 76
    Width = 1007
    Height = 19
    AutoSize = True
    ButtonHeight = 19
    Caption = 'ToolBar3'
    TabOrder = 3
    ExplicitWidth = 1006
    object CBFontName: TfrxFontComboBox
      Left = 0
      Top = 0
      Width = 150
      Height = 22
      MRURegKey = '\Software\Fast Reports\MRUFont'
      Text = 'CBFontName'
      DropDownCount = 12
      TabOrder = 1
      OnChange = CBFontNameChange
    end
    object CBFontSize: TComboBox
      Left = 150
      Top = 0
      Width = 100
      Height = 19
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 0
      OnChange = CBFontSizeChange
      Items.Strings = (
        'Pequena'
        'M'#233'dia'
        'Normal'
        'Maior'
        'Grande'
        'Extra Grande')
    end
  end
  object MainMenu1: TMainMenu
    Left = 136
    Top = 160
    object Arquivo1: TMenuItem
      Caption = 'Arquivo'
      object Novo1: TMenuItem
        Caption = 'Novo'
        OnClick = Novo1Click
      end
      object Abrir1: TMenuItem
        Caption = 'Abrir'
        OnClick = Abrir1Click
      end
      object SalvarComo1: TMenuItem
        Caption = 'Salvar Como'
        OnClick = SalvarComo1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Imprimir1: TMenuItem
        Caption = 'Imprimir'
        OnClick = Imprimir1Click
      end
      object VisualizarImpresso1: TMenuItem
        Caption = 'Visualizar Impress'#227'o'
        OnClick = VisualizarImpresso1Click
      end
      object ConfigurarPgina1: TMenuItem
        Caption = 'Configurar P'#225'gina'
        OnClick = ConfigurarPgina1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Fechar1: TMenuItem
        Caption = 'Fechar'
        OnClick = Fechar1Click
      end
    end
    object Editar1: TMenuItem
      Caption = 'Editar'
      object Recortar1: TMenuItem
        Caption = 'Recortar'
        OnClick = Recortar1Click
      end
      object Copiar1: TMenuItem
        Caption = 'Copiar'
        OnClick = Copiar1Click
      end
      object Colar1: TMenuItem
        Caption = 'Colar'
        OnClick = Colar1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object LocalizaNestaPgina1: TMenuItem
        Caption = 'Localiza [Nesta P'#225'gina]'
        OnClick = LocalizaNestaPgina1Click
      end
    end
  end
end
