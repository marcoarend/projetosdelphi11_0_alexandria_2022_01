unit TextosHTML;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Classes, System.SysUtils, System.Variants,
  Vcl.Forms, Vcl.Graphics, Vcl.Controls, Vcl.Dialogs, Vcl.Menus, Vcl.ComCtrls,
  Vcl.OleCtrls, Vcl.ToolWin, Vcl.ImgList, Vcl.StdCtrls, MSHTML, SHDocVw,
  frxCtrls, mySQLDbTables;

type
  TFmTextosHTML = class(TForm)
    ToolBar2: TToolBar;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton16: TToolButton;
    TTBSub: TToolButton;
    TTBSob: TToolButton;
    ToolButton17: TToolButton;
    ToolBar1: TToolBar;
    TTBNovo: TToolButton;
    TTBAbrir: TToolButton;
    TTBSalvar: TToolButton;
    ToolButton5: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    ToolButton22: TToolButton;
    ToolButton23: TToolButton;
    TTBAlinEsq: TToolButton;
    TTBAlinCen: TToolButton;
    TTBAlinDir: TToolButton;
    TTBAlinJus: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton24: TToolButton;
    ToolButton25: TToolButton;
    ToolButton26: TToolButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WBTexto: TWebBrowser;
    MeHTML: TMemo;
    ToolButton27: TToolButton;
    ToolButton28: TToolButton;
    ToolBar3: TToolBar;
    CBFontSize: TComboBox;
    CBFontName: TfrxFontComboBox;
    ToolButton29: TToolButton;
    ToolButton30: TToolButton;
    ToolButton31: TToolButton;
    ToolButton32: TToolButton;
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Novo1: TMenuItem;
    Abrir1: TMenuItem;
    SalvarComo1: TMenuItem;
    N1: TMenuItem;
    Imprimir1: TMenuItem;
    VisualizarImpresso1: TMenuItem;
    ConfigurarPgina1: TMenuItem;
    N2: TMenuItem;
    Fechar1: TMenuItem;
    Editar1: TMenuItem;
    Recortar1: TMenuItem;
    Copiar1: TMenuItem;
    Colar1: TMenuItem;
    N4: TMenuItem;
    LocalizaNestaPgina1: TMenuItem;
    TBVideo: TToolButton;
    procedure Abrir1Click(Sender: TObject);
    procedure SalvarComo1Click(Sender: TObject);
    procedure Imprimir1Click(Sender: TObject);
    procedure VisualizarImpresso1Click(Sender: TObject);
    procedure ConfigurarPgina1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Recortar1Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Colar1Click(Sender: TObject);
    procedure TTBNovoClick(Sender: TObject);
    procedure TTBAbrirClick(Sender: TObject);
    procedure TTBSalvarClick(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure TTBSubClick(Sender: TObject);
    procedure TTBSobClick(Sender: TObject);
    procedure ToolButton18Click(Sender: TObject);
    procedure ToolButton19Click(Sender: TObject);
    procedure ToolButton20Click(Sender: TObject);
    procedure ToolButton21Click(Sender: TObject);
    procedure TTBAlinEsqClick(Sender: TObject);
    procedure TTBAlinCenClick(Sender: TObject);
    procedure TTBAlinDirClick(Sender: TObject);
    procedure TTBAlinJusClick(Sender: TObject);
    procedure ToolButton14Click(Sender: TObject);
    procedure ToolButton24Click(Sender: TObject);
    procedure ToolButton25Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure LocalizaNestaPgina1Click(Sender: TObject);
    procedure ToolButton28Click(Sender: TObject);
    procedure CBFontSizeChange(Sender: TObject);
    procedure CBFontNameChange(Sender: TObject);
    procedure ToolButton29Click(Sender: TObject);
    procedure ToolButton31Click(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure ToolButton32Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure TBVideoClick(Sender: TObject);
  private
    { Private declarations }
    FSalvaDB: Boolean;
    procedure SalvaTexto();
    procedure MostraEdicao();
    procedure Salvar();
    procedure Abrir();
    procedure Novo();
  public
    { Public declarations }
    FDataBase: TmySQLDatabase;
    FCodigo: Integer;
    FCustom: Boolean;
    FFldCodigo, FFldTexto, FTablela: String;
    FHTML: WideString;
  end;

var
  FmTextosHTML: TFmTextosHTML;

implementation

uses MyGlyfs, UMySQLModule, UnDmkEnums, dmkGeral, UnDmkHTML2, UnTextos_Jan,
  UnAppPF;

{$R *.dfm}

procedure TFmTextosHTML.Abrir;
begin
  if FSalvaDB = False then
    dmkHTML2.Abrir(WBTexto)
  else
    Geral.MB_Aviso('Fun��o n�o implementada!');
end;

procedure TFmTextosHTML.Abrir1Click(Sender: TObject);
begin
  Abrir();
end;

procedure TFmTextosHTML.CBFontNameChange(Sender: TObject);
begin
  dmkHTML2.FonteMuda(WBTexto, CBFontName.Text);
end;

procedure TFmTextosHTML.CBFontSizeChange(Sender: TObject);
begin
  dmkHTML2.FonteMudaTamanho(WBTexto, CBFontSize);
end;

procedure TFmTextosHTML.Colar1Click(Sender: TObject);
begin
  dmkHTML2.Colar(WBTexto);
end;

procedure TFmTextosHTML.ConfigurarPgina1Click(Sender: TObject);
begin
  dmkHTML2.ConfigurarPagina(WBTexto);
end;

procedure TFmTextosHTML.Copiar1Click(Sender: TObject);
begin
  dmkHTML2.Copiar(WBTexto);
end;

procedure TFmTextosHTML.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmTextosHTML.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if Geral.MB_Pergunta('Salva as altera��es?') = ID_YES then
    Salvar;
end;

procedure TFmTextosHTML.FormCreate(Sender: TObject);
begin
  ToolBar1.Images := FmMyGlyfs.Lista_32X32_Textos;
  ToolBar2.Images := FmMyGlyfs.Lista_32X32_Textos;
  //
  PageControl1.ActivePageIndex := 0;
  FSalvaDB                     := False;
  FCustom                      := False;
  //
  dmkHTML2.DocumentoEmBranco(WBTexto, True);
end;

procedure TFmTextosHTML.FormShow(Sender: TObject);
begin
  MostraEdicao;
end;

procedure TFmTextosHTML.Imprimir1Click(Sender: TObject);
begin
  dmkHTML2.ImprimirPagina(WBTexto);
end;

procedure TFmTextosHTML.LocalizaNestaPgina1Click(Sender: TObject);
begin
  dmkHTML2.LocalizarNaPagina(WBTexto);
end;

procedure TFmTextosHTML.MostraEdicao;
var
  Mostra: Boolean;
begin
  if (FTablela <> '') and (FFldCodigo <> '') and (FFldTexto <> '') and
    (FCodigo <> 0) and (FDataBase <> nil) then
  begin
    FSalvaDB := True;
    Mostra   := False;
    //
    dmkHTML2.ConfiguraTextoNoWebBrowser(WBTexto, FHTML);
  end else
  begin
    FSalvaDB := False;
    Mostra   := True;
  end;
  TTBNovo.Enabled  := Mostra;
  TTBAbrir.Enabled := Mostra;
  Novo1.Enabled    := Mostra;
  Abrir1.Enabled   := Mostra;
  //
  TBVideo.Visible := FCustom;
end;

procedure TFmTextosHTML.Novo;
begin
  if FSalvaDB = False then
    dmkHTML2.DocumentoEmBranco(WBTexto, True)
  else
    Geral.MB_Aviso('Fun��o n�o implementada!');
end;

procedure TFmTextosHTML.Novo1Click(Sender: TObject);
begin
  Novo();
end;

procedure TFmTextosHTML.PageControl1Change(Sender: TObject);
var
  Lista: TStringlist;
begin
  ToolBar1.Enabled := PageControl1.ActivePageIndex = 0;
  ToolBar2.Enabled := PageControl1.ActivePageIndex = 0;
  //
  if PageControl1.ActivePageIndex = 1 then
    MeHTML.Text := dmkHTML2.ObtemHTMLdeWebBrowser(WBTexto)
  else
    dmkHTML2.ConfiguraTextoNoWebBrowser(WBTexto, MeHTML.Text);
end;

procedure TFmTextosHTML.Recortar1Click(Sender: TObject);
begin
  dmkHTML2.Recortar(WBTexto);
end;

procedure TFmTextosHTML.Salvar;
begin
  if FSalvaDB = False then
    dmkHTML2.Salvar(WBTexto)
  else
    SalvaTexto;
end;

procedure TFmTextosHTML.SalvarComo1Click(Sender: TObject);
begin
  Salvar();
end;

procedure TFmTextosHTML.SalvaTexto();
var
  Qry: TmySQLQuery;
  HTML: WideString;
begin
  HTML := '';
  //
  if (FCodigo <> 0) and (FFldCodigo <> '') and (FFldTexto <> '') and
    (FTablela <> '') and (FDataBase <> nil) then
  begin
    Screen.Cursor := crHourGlass;
    try
      HTML := dmkHTML2.ObtemHTMLdeWebBrowser(WBTexto);
      //
      if HTML <> '' then
      begin
        Qry := TmySQLQuery.Create(FDataBase.Owner);
        Qry.Database := FDataBase;
        try
          if not UMyMod.SQLInsUpd(Qry, stUpd, FTablela, False,
            [FFldTexto], [FFldCodigo], [HTML], [FCodigo], True)
          then
            Geral.MB_Erro('Erro ao salvar texto!');
        finally
          Qry.Free;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Geral.MB_Erro('Dados incompletos para salvar registro!');
end;

procedure TFmTextosHTML.TBVideoClick(Sender: TObject);
begin
  if not FCustom then
    Geral.MB_Aviso('Op��o indispon�vel!')
  else
    AppPF.Html_ConfigarVideo(WBTexto);
end;

procedure TFmTextosHTML.ToolButton10Click(Sender: TObject);
begin
  dmkHTML2.Colar(WBTexto);
end;

procedure TFmTextosHTML.ToolButton11Click(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WBTexto, taItalic);
end;

procedure TFmTextosHTML.ToolButton12Click(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WBTexto, taUnderline);
end;

procedure TFmTextosHTML.ToolButton13Click(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WBTexto, taStrikeThrough);
end;

procedure TFmTextosHTML.ToolButton14Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecLinha);
end;

procedure TFmTextosHTML.TTBSubClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WBTexto, taSubscript, TTBSub, TTBSob);
end;

procedure TFmTextosHTML.TTBSobClick(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WBTexto, taSuperscript, TTBSub, TTBSob);
end;

procedure TFmTextosHTML.ToolButton18Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecListMarcador);
end;

procedure TFmTextosHTML.ToolButton19Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecListNumeros);
end;

procedure TFmTextosHTML.TTBNovoClick(Sender: TObject);
begin
  Novo();
end;

procedure TFmTextosHTML.ToolButton20Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecRecuoOut);
end;

procedure TFmTextosHTML.ToolButton21Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecRecuoIn);
end;

procedure TFmTextosHTML.ToolButton24Click(Sender: TObject);
begin
  if not FCustom then
    dmkHTML2.ExecutaComando(WBTexto, tecLink)
  else
    AppPF.Html_ConfigarUrl(WBTexto);
end;

procedure TFmTextosHTML.ToolButton25Click(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecDesfazer);
end;

procedure TFmTextosHTML.ToolButton28Click(Sender: TObject);
begin
  dmkHTML2.LocalizarNaPagina(WBTexto);
end;

procedure TFmTextosHTML.ToolButton29Click(Sender: TObject);
begin
  dmkHTML2.FonteCor(WBTexto);
end;

procedure TFmTextosHTML.TTBAlinEsqClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecAlinEsq, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmTextosHTML.TTBAlinCenClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecAlinCen, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmTextosHTML.TTBAlinDirClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecAlinDir, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmTextosHTML.TTBAlinJusClick(Sender: TObject);
begin
  dmkHTML2.ExecutaComando(WBTexto, tecAlinJus, TTBAlinEsq, TTBAlinDir, TTBAlinCen, TTBAlinJus);
end;

procedure TFmTextosHTML.TTBAbrirClick(Sender: TObject);
begin
  Abrir();
end;

procedure TFmTextosHTML.ToolButton31Click(Sender: TObject);
begin
  if not FCustom then
    dmkHTML2.ExecutaComando(WBTexto, tecImagem)
  else
    AppPF.Html_ConfigarImagem(WBTexto);
end;

procedure TFmTextosHTML.ToolButton32Click(Sender: TObject);
begin
  Textos_Jan.ConfigarTabela(WBTexto);
end;

procedure TFmTextosHTML.TTBSalvarClick(Sender: TObject);
begin
  Salvar();
end;

procedure TFmTextosHTML.ToolButton4Click(Sender: TObject);
begin
  dmkHTML2.VisualizarImpressao(WBTexto);
end;

procedure TFmTextosHTML.ToolButton7Click(Sender: TObject);
begin
  dmkHTML2.SetAtributo(WBTexto, taBold);
end;

procedure TFmTextosHTML.ToolButton8Click(Sender: TObject);
begin
  dmkHTML2.Recortar(WBTexto);
end;

procedure TFmTextosHTML.ToolButton9Click(Sender: TObject);
begin
  dmkHTML2.Copiar(WBTexto);
end;

procedure TFmTextosHTML.VisualizarImpresso1Click(Sender: TObject);
begin
  dmkHTML2.VisualizarImpressao(WBTexto);
end;

end.
