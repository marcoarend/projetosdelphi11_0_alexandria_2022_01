// ini Delphi 28 Alexandria
//{$I dmk.inc}
// ini Delphi 28 Alexandria

unit Textos;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Menus, ComCtrls, ClipBrd, ToolWin, ActnList,
  ImgList, Db, mySQLDbTables, DBCtrls, ShellAPI, dmkGeral, System.Actions,
  System.ImageList;

type
  TTipoLoc = (vpNone, vpIni, vpLoc, vpFim, vpRowPrior, vpRowNext);
  TFmTextos = class(TForm)
    MainMenu: TMainMenu;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    FilePrintItem: TMenuItem;
    FileExitItem: TMenuItem;
    EditUndoItem: TMenuItem;
    EditCutItem: TMenuItem;
    EditCopyItem: TMenuItem;
    EditPasteItem: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PrintDialog: TPrintDialog;
    FontDialog1: TFontDialog;
    N5: TMenuItem;
    miEditFont: TMenuItem;
    ToolbarImages: TImageList;
    ActionList1: TActionList;
    FileNewCmd: TAction;
    FileOpenCmd: TAction;
    FileSaveCmd: TAction;
    FilePrintCmd: TAction;
    FileExitCmd: TAction;
    EditCutCmd: TAction;
    EditCopyCmd: TAction;
    EditPasteCmd: TAction;
    EditUndoCmd: TAction;
    EditFontCmd: TAction;
    FileSaveAsCmd: TAction;
    StatusBar: TStatusBar;
    ToolBar1: TToolBar;
    NewButton: TToolButton;
    OpenButton: TToolButton;
    SaveButton: TToolButton;
    PrintButton: TToolButton;
    ToolButton5: TToolButton;
    CutButton: TToolButton;
    CopyButton: TToolButton;
    PasteButton: TToolButton;
    UndoButton: TToolButton;
    BoldButton: TToolButton;
    ItalicButton: TToolButton;
    UnderlineButton: TToolButton;
    LeftAlign: TToolButton;
    CenterAlign: TToolButton;
    RightAlign: TToolButton;
    ToolButton20: TToolButton;
    BulletsButton: TToolButton;
    ImageList1: TImageList;
    Panel1: TPanel;
    LbItensMD: TListBox;
    Panel2: TPanel;
    Ruler: TPanel;
    FirstInd: TLabel;
    LeftInd: TLabel;
    RulerLine: TBevel;
    RightInd: TLabel;
    Bevel1: TBevel;
    Splitter1: TSplitter;
    ToolBar2: TToolBar;
    FontName: TComboBox;
    ToolButton2: TToolButton;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    Editor: TRichEdit;
    ColorDialog1: TColorDialog;
    ToolButton7: TToolButton;
    BitBtn1: TBitBtn;
    CBFonte: TComboBox;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    Button1: TButton;
    Panel3: TPanel;
    Button2: TButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    FindDialog1: TFindDialog;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    procedure SelectionChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FileNew(Sender: TObject);
    procedure FileOpen(Sender: TObject);
    procedure FileSave(Sender: TObject);
    procedure FileSaveAs(Sender: TObject);
    procedure FilePrint(Sender: TObject);
    procedure FileExit(Sender: TObject);
    procedure EditUndo(Sender: TObject);
    procedure EditCut(Sender: TObject);
    procedure EditCopy(Sender: TObject);
    procedure EditPaste(Sender: TObject);
    procedure SelectFont(Sender: TObject);
    procedure RulerResize(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure BtNegritoClick(Sender: TObject);
    procedure BtItalicoClick(Sender: TObject);
    procedure AlignButtonClick(Sender: TObject);
    procedure BtSublinhadoClick(Sender: TObject);
    procedure BtTopicoClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RulerItemMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RulerItemMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FirstIndMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LeftIndMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RightIndMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure RichEditChange(Sender: TObject);
    procedure SwitchLanguage(Sender: TObject);
    procedure ActionList2Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure FontNameChange(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CBFonteChange(Sender: TObject);
    procedure CBFonteClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure EditorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FFileName: string;
    FUpdating: Boolean;
    FDragOfs: Integer;
    FDragging: Boolean;
    function CurrText: TTextAttributes;
    procedure GetFontNames;
    procedure SetFileName(const FileName: String);
    procedure CheckFileSave;
    procedure SetupRuler;
    procedure SetEditRect;
    procedure UpdateCursorPos;
    procedure WMDropFiles(var Msg: TWMDropFiles); message WM_DROPFILES;
    procedure PerformFileOpen(const AFileName: string);
    procedure SetModified(Value: Boolean);
    procedure SetSelBgColor(RichEdit: TRichEdit; AColor: TColor);
    procedure VaiParaLinhaColuna(Tipo: TTipoLoc; Col, Row: Integer);
    //function  BitmapToRTF(Graph: TBitmap): string;
{$IFDEF DELPHI12_UP}
    procedure ObtemRFT_de_RichEdit();
{$ELSE}
    // D2007 n�o faz nada!
{$ENDIF}
  public
    FAtributesSize: Integer;
    FAtributesName: String;
    FSaveSit: Integer;
    FTabela: TmySQLTable;
    FDBRichEdit: TDBRichEdit;
    FMemoryStream: TMemoryStream;
    FAberto: Boolean;
    FCampo: TWideMemoField;
    //FRichEdit: T L M DRichEdit;
    FRichEdit: TRichEdit;
    FTipoLoc: TTipoLoc;
    FvpCol, FvpRow: Integer;
    //
    FTextoPlano: WideString;
    // 2013-05-29 !XE2
    FTextoRTF: String;
    FTextoDef: Boolean;
    // 2013-05-29 Fim !XE2
  end;

var
  FmTextos: TFmTextos;

implementation

uses UnMyObjects, RichEdit, ReInit;

resourcestring
  sSaveChanges = 'Salva as altera��es de %s?';
  sOverWrite = 'OK para sobrescrever %s';
  sUntitled = 'Sem t�tulo';
  sModified = 'Modificado';
  sColRowInfo = 'Linha: %3d   Coluna: %3d';
  sPergunta = 'Pergunta';
  
const
  RulerAdj = 4/3;
  GutterWid = 6;

  ENGLISH = (SUBLANG_ENGLISH_US shl 10) or LANG_ENGLISH;
  FRENCH  = (SUBLANG_FRENCH shl 10) or LANG_FRENCH;
  GERMAN  = (SUBLANG_GERMAN shl 10) or LANG_GERMAN;

{$R *.DFM}

procedure TFmTextos.SelectionChange(Sender: TObject);
begin
  with Editor.Paragraph do
  try
    FUpdating := True;
    FirstInd.Left := Trunc(FirstIndent*RulerAdj)-4+GutterWid;
    LeftInd.Left := Trunc((LeftIndent+FirstIndent)*RulerAdj)-4+GutterWid;
    RightInd.Left := Ruler.ClientWidth-6-Trunc((RightIndent+GutterWid)*RulerAdj);
    BoldButton.Down := fsBold in Editor.SelAttributes.Style;
    ItalicButton.Down := fsItalic in Editor.SelAttributes.Style;
    UnderlineButton.Down := fsUnderline in Editor.SelAttributes.Style;
    BulletsButton.Down := Boolean(Numbering);
    CBFonte.Text := IntToStr(Editor.SelAttributes.Size);
    FontName.Text := Editor.SelAttributes.Name;
    case Ord(Alignment) of
      0: LeftAlign.Down := True;
      1: RightAlign.Down := True;
      2: CenterAlign.Down := True;
    end;
    UpdateCursorPos;
  finally
    FUpdating := False;
  end;
end;

function TFmTextos.CurrText: TTextAttributes;
begin
  if Editor.SelLength > 0 then Result := Editor.SelAttributes
  else Result := Editor.DefAttributes;
end;

function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
  FontType: Integer; Data: Pointer): Integer; stdcall;
begin
  TStrings(Data).Add(LogFont.lfFaceName);
  Result := 1;
end;

procedure TFmTextos.GetFontNames;
var
  DC: HDC;
begin
  DC := GetDC(0);
  EnumFonts(DC, nil, @EnumFontsProc, Pointer(FontName.Items));
  ReleaseDC(0, DC);
  FontName.Sorted := True;
end;

procedure TFmTextos.SetFileName(const FileName: String);
begin
  FFileName := FileName;
  Caption := Format('%s - %s', [ExtractFileName(FileName), Application.Title]);
end;

procedure TFmTextos.CBFonteChange(Sender: TObject);
begin
  if FUpdating then Exit;
  CurrText.Size := StrToInt(CBFonte.Text);
end;

procedure TFmTextos.CBFonteClick(Sender: TObject);
begin
  if FUpdating then Exit;
  CurrText.Size := StrToInt(CBFonte.Text);
end;

procedure TFmTextos.CheckFileSave;
var
  SaveResp: Integer;
begin
  if not Editor.Modified then Exit;
  SaveResp := Geral.MB_Pergunta(Format(sSaveChanges, [FFileName]));
  case SaveResp of
    ID_YES: FileSave(Self);
    ID_NO: {Nothing};
    ID_CANCEL: Abort;
  end;
end;

procedure TFmTextos.SetupRuler;
var
  I: Integer;
  S: String;
begin
  SetLength(S, 201);
  I := 1;
  while I < 200 do
  begin
    S[I] := #9;
    S[I+1] := '|';
    Inc(I, 2);
  end;
  Ruler.Caption := S;
end;

procedure TFmTextos.SetEditRect;
var
  R: TRect;
begin
  with Editor do
  begin
    R := Rect(GutterWid, 0, ClientWidth-GutterWid, ClientHeight);
    SendMessage(Handle, EM_SETRECT, 0, Longint(@R));
  end;
end;

{ Event Handlers }

procedure TFmTextos.FormCreate(Sender: TObject);
begin
  FTextoRTF := '';
  FTextoDef := False;
  //
  FTipoLoc := vpNone;
  FvpCol   := 0;
  FvpRow   := 0;
  //
  OpenDialog.InitialDir := ExtractFilePath(ParamStr(0));
  SaveDialog.InitialDir := OpenDialog.InitialDir;
  SetFileName(sUntitled);
  GetFontNames;
  SetupRuler;
  SelectionChange(Self);
end;

procedure TFmTextos.FileNew(Sender: TObject);
begin
  SetFileName(sUntitled);
  Editor.Lines.Clear;
  Editor.Modified := False;
  SetModified(False);
end;

procedure TFmTextos.PerformFileOpen(const AFileName: string);
begin
  Editor.Lines.LoadFromFile(AFileName);
  SetFileName(AFileName);
  Editor.SetFocus;
  Editor.Modified := False;
  SetModified(False);
end;

procedure TFmTextos.FileOpen(Sender: TObject);
begin
  CheckFileSave;
  if OpenDialog.Execute then
  begin
    PerformFileOpen(OpenDialog.FileName);
    Editor.ReadOnly := ofReadOnly in OpenDialog.Options;
  end;
end;

procedure TFmTextos.FileSave(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  //
{$IFDEF DELPHI12_UP}
  ObtemRFT_de_RichEdit();
{$ELSE}
    // D2007 n�o faz nada!
{$ENDIF}
  //
  case FSaveSit of
    0:
    begin
      if FFileName = sUntitled then
        FileSaveAs(Sender)
      else
      begin
        Editor.Lines.SaveToFile(FFileName);
        Editor.Modified := False;
        SetModified(False);
      end;
    end;
    1:
    begin
      if (FTabela <> nil) and (FDBRichEdit <> nil) then
      begin
        FMemoryStream := TMemoryStream.Create;
        Editor.Lines.SaveToStream(FMemoryStream);
        FMemoryStream.Position := 0; // reset to the beginning of the stream
        FTabela.Edit;
        FDBRichEdit.Lines.LoadFromStream(FMemoryStream);
        FTabela.Post;
        FTabela.Refresh;
        //
        FMemoryStream.Free;
      end else Geral.MB_Erro('Tabela ou "DBRichEdit" indefinido(s).');
    end;
    2:
    begin
      if (FRichEdit <> nil) then
      begin
        FMemoryStream := TMemoryStream.Create;
        Editor.Lines.SaveToStream(FMemoryStream);
        FMemoryStream.Position := 0; // reset to the beginning of the stream
        FRichEdit.Lines.LoadFromStream(FMemoryStream);
        //
        FRichEdit.Tag := 1;
        FMemoryStream.Free;
      end else Geral.MB_Erro('"RichEdit" indefinido.');
    end;
    3:
    begin
      if (FRichEdit <> nil) then
        MyObjects.TextoEntreRichEdits(Editor, FRichEdit)
      else Geral.MB_Erro('"RichEdit" indefinido.');
    end;
    else Geral.MB_Erro('O arquivo n�o pode ser salvo. '+
         'Forma de salvamento indefinida!');
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmTextos.FileSaveAs(Sender: TObject);
begin
  if SaveDialog.Execute then
  begin
    if FileExists(SaveDialog.FileName) then
      if MessageDlg(Format(sOverWrite, [SaveDialog.FileName]),
        mtConfirmation, mbYesNoCancel, 0) <> idYes then Exit;
    Editor.Lines.SaveToFile(SaveDialog.FileName);
    SetFileName(SaveDialog.FileName);
    Editor.Modified := False;
    SetModified(False);
  end;
end;

procedure TFmTextos.FindDialog1Find(Sender: TObject);
begin
  MyObjects.LocalizaTextoEmRich(Editor, FindDialog1);
end;

procedure TFmTextos.FilePrint(Sender: TObject);
begin
  if PrintDialog.Execute then
    Editor.Print(FFileName);
end;

procedure TFmTextos.FileExit(Sender: TObject);
begin
  Close;
end;

procedure TFmTextos.EditUndo(Sender: TObject);
begin
  with Editor do
    if HandleAllocated then SendMessage(Handle, EM_UNDO, 0, 0);
end;

procedure TFmTextos.EditCut(Sender: TObject);
begin
  Editor.CutToClipboard;
end;

procedure TFmTextos.EditorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 070 then //letra F
  begin
    if ssCtrl in Shift then
      FindDialog1.Execute;
  end;
end;

procedure TFmTextos.EditCopy(Sender: TObject);
begin
  Editor.CopyToClipboard;
end;

procedure TFmTextos.EditPaste(Sender: TObject);
begin
  Editor.PasteFromClipboard;
end;

procedure TFmTextos.SelectFont(Sender: TObject);
begin
  FontDialog1.Font.Assign(Editor.SelAttributes);
  if FontDialog1.Execute then
    CurrText.Assign(FontDialog1.Font);
  SelectionChange(Self);
  Editor.SetFocus;
end;

procedure TFmTextos.RulerResize(Sender: TObject);
begin
  RulerLine.Width := Ruler.ClientWidth - (RulerLine.Left*2);
end;

procedure TFmTextos.FormResize(Sender: TObject);
begin
  SetEditRect;
  SelectionChange(Sender);
end;

procedure TFmTextos.FormPaint(Sender: TObject);
begin
  SetEditRect;
end;

procedure TFmTextos.BtNegritoClick(Sender: TObject);
begin
  if FUpdating then Exit;
  if BoldButton.Down then
    CurrText.Style := CurrText.Style + [fsBold]
  else
    CurrText.Style := CurrText.Style - [fsBold];
end;

procedure TFmTextos.BtItalicoClick(Sender: TObject);
begin
  if FUpdating then Exit;
  if ItalicButton.Down then
    CurrText.Style := CurrText.Style + [fsItalic]
  else
    CurrText.Style := CurrText.Style - [fsItalic];
end;

procedure TFmTextos.AlignButtonClick(Sender: TObject);
begin
  if FUpdating then Exit;
  Editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure TFmTextos.BtSublinhadoClick(Sender: TObject);
begin
  if FUpdating then Exit;
  if UnderlineButton.Down then
    CurrText.Style := CurrText.Style + [fsUnderline]
  else
    CurrText.Style := CurrText.Style - [fsUnderline];
end;

procedure TFmTextos.BtTopicoClick(Sender: TObject);
begin
  if FUpdating then Exit;
  Editor.Paragraph.Numbering := TNumberingStyle(BulletsButton.Down);
end;

procedure TFmTextos.Button1Click(Sender: TObject);
{
var
  SS:TStringStream;
}
begin
{
  SS:=TStringStream.Create(BitmapToRtf(Image1.Picture.Bitmap));
  Editor.PlainText:=False;
  //Editor.StreamMode:=[smSelection];
  Editor.Lines.LoadFromStream(SS);
  SS.Free;
}
end;

procedure TFmTextos.Button2Click(Sender: TObject);
begin
{$IFDEF DELPHI12_UP}
  ObtemRFT_de_RichEdit();
{$ELSE}
    // D2007 n�o faz nada!
{$ENDIF}
end;

procedure TFmTextos.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  try
    CheckFileSave;
  except
    CanClose := False;
  end;
end;

{ Ruler Indent Dragging }

procedure TFmTextos.RulerItemMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragOfs := (TLabel(Sender).Width div 2);
  TLabel(Sender).Left := TLabel(Sender).Left+X-FDragOfs;
  FDragging := True;
end;

procedure TFmTextos.RulerItemMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if FDragging then
    TLabel(Sender).Left :=  TLabel(Sender).Left+X-FDragOfs
end;

procedure TFmTextos.FirstIndMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
  Editor.Paragraph.FirstIndent := Trunc((FirstInd.Left+FDragOfs-GutterWid) / RulerAdj);
  LeftIndMouseUp(Sender, Button, Shift, X, Y);
end;

procedure TFmTextos.LeftIndMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
  Editor.Paragraph.LeftIndent := Trunc((LeftInd.Left+FDragOfs-GutterWid) / RulerAdj)-Editor.Paragraph.FirstIndent;
  SelectionChange(Sender);
end;

{$IFDEF DELPHI12_UP}
procedure TFmTextos.ObtemRFT_de_RichEdit();
{
var ms:TMemoryStream;
    strS:TStringStream;
    aStr:string;
    aPos:integer;
    found:boolean;
begin
 aStr:='Hello';
 ms:=TMemoryStream.Create;
 ms.LoadFromFile('c:\aFile.txt');
 strS:=TStringStream.Create;
 strS.LoadFromStream(ms);
 aPos:=pos(aStr,strS.dataString);
 found:=aPos>0;
end;
}
var
  MemoryStream: TMemoryStream;
  strS: TStringStream;
  aStr: String;
  aPos: Integer;
  found: Boolean;
begin
 {aStr:='Hello';
 ms:=TMemoryStream.Create;
 ms.LoadFromFile('c:\aFile.txt');}
  MemoryStream := TMemoryStream.Create;
  Editor.Lines.SaveToStream(MemoryStream);
{
  MemoryStream.Position := 0;
  FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
}
 strS := TStringStream.Create;
 strS.LoadFromStream(MemoryStream);
 {
 aPos:=pos(aStr,strS.dataString);
 found:=aPos>0;
 }
 FTextoRTF := strS.dataString;
 FTextoDef := True;
  //
 MemoryStream.Free;
 strS.Free;
end;
{$ELSE}
    // D2007 n�o faz nada!
{$ENDIF}

procedure TFmTextos.RightIndMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
  Editor.Paragraph.RightIndent := Trunc((Ruler.ClientWidth-RightInd.Left+FDragOfs-2) / RulerAdj)-2*GutterWid;
  SelectionChange(Sender);
end;

procedure TFmTextos.UpdateCursorPos;
var
  CharPos: TPoint;
begin
  CharPos.Y := SendMessage(Editor.Handle, EM_EXLINEFROMCHAR, 0,
    Editor.SelStart);
  CharPos.X := (Editor.SelStart -
    SendMessage(Editor.Handle, EM_LINEINDEX, CharPos.Y, 0));
  Inc(CharPos.Y);
  Inc(CharPos.X);
  StatusBar.Panels[0].Text := Format(sColRowInfo, [CharPos.Y, CharPos.X]);
end;

procedure TFmTextos.FormShow(Sender: TObject);
begin
  UpdateCursorPos;
  DragAcceptFiles(Handle, True);
  RichEditChange(nil);
  Editor.SetFocus;
  (*{ Check if we should load a file from the command line }
  if (ParamCount > 0) and FileExists(ParamStr(1)) then
    PerformFileOpen(ParamStr(1));*)
end;

procedure TFmTextos.WMDropFiles(var Msg: TWMDropFiles);
var
  CFileName: array[0..MAX_PATH] of Char;
begin
  try
    if DragQueryFile(Msg.Drop, 0, CFileName, MAX_PATH) > 0 then
    begin
      CheckFileSave;
      PerformFileOpen(CFileName);
      Msg.Result := 0;
    end;
  finally
    DragFinish(Msg.Drop);
  end;
end;

procedure TFmTextos.RichEditChange(Sender: TObject);
begin
  SetModified(Editor.Modified);
end;

procedure TFmTextos.SetModified(Value: Boolean);
begin
  if Value then StatusBar.Panels[1].Text := sModified
  else StatusBar.Panels[1].Text := '';
end;

procedure TFmTextos.SwitchLanguage(Sender: TObject);
var
  Name : String;
  Size : Integer;
begin
  if LoadNewResourceModule(TComponent(Sender).Tag) <> 0 then
  begin
    Name := FontName.Text;
    Size := StrToInt(CBFonte.Text);
    ReinitializeForms;
    (*LanguageEnglish.Checked := LanguageEnglish = Sender;
    LanguageFrench.Checked  := LanguageFrench  = Sender;
    LanguageGerman.Checked  := LanguageGerman  = Sender;*)

    CurrText.Name := Name;
    CurrText.Size := Size;
    SelectionChange(Self);
    FontName.SelLength := 0;

    SetupRuler;
    if Visible then Editor.SetFocus;
  end;
end;

procedure TFmTextos.ActionList2Update(Action: TBasicAction;
  var Handled: Boolean);
begin
 { Update the status of the edit commands }
  //EditCutCmd.Enabled := Editor.SelLength > 0;
  //EditCopyCmd.Enabled := EditCutCmd.Enabled;
  if Editor.HandleAllocated then
  begin
    //EditUndoCmd.Enabled := Editor.Perform(EM_CANUNDO, 0, 0) <> 0;
    //EditPasteCmd.Enabled := Editor.Perform(EM_CANPASTE, 0, 0) <> 0;
  end;
end;

procedure TFmTextos.FormActivate(Sender: TObject);
(*var
  AtributesSize: Integer;
  AtributesName: String;*)
begin
  MyObjects.CorIniComponente();
  if (not FAberto) then
  begin
    if (FDBRichEdit <> nil) then
    begin
      Editor.Text := String(FCampo.Value);
      //
      if (FAtributesSize <> 0)  and (FAtributesName = '') then
      begin
        Editor.DefAttributes.Name := FAtributesName;
        Editor.DefAttributes.Size := FAtributesSize;
      end;
      Editor.SelStart := Editor.SelStart + 1;
      Editor.SelStart := Editor.SelStart - 1;
      if FTipoLoc <> vpNone then
        VaiParaLinhaColuna(FTipoLoc, FvpCol, FvpRow);
    end else if (FRichEdit <> nil) then
    begin
      Editor.SelStart := Editor.SelStart + 1;
      Editor.SelStart := Editor.SelStart - 1;
      if FTipoLoc <> vpNone then
        VaiParaLinhaColuna(FTipoLoc, FvpCol, FvpRow);
    end;
  end;

  FAberto := True;
end;

procedure TFmTextos.LbItensMDDblClick(Sender: TObject);
var
  Texto: String;
  Ate: Integer;
begin
  Texto := LbItensMD.Items[LbItensMD.ItemIndex];
  if Texto <> '' then
  begin
    Ate := Pos(']', Texto);
    Texto := Copy(Texto, 1, Ate);
    Clipboard.AsText := Texto;
    Editor.PasteFromClipboard;
  end;
  Editor.SetFocus;
end;

procedure TFmTextos.FontNameChange(Sender: TObject);
begin
  if FUpdating then Exit;
  CurrText.Name := FontName.Items[FontName.ItemIndex];
  Editor.SetFocus; // Marco
end;

procedure TFmTextos.ToolButton10Click(Sender: TObject);
begin
  if Editor.Paragraph.FirstIndent > 10 then
    Editor.Paragraph.FirstIndent := Editor.Paragraph.FirstIndent - 10;
end;

procedure TFmTextos.ToolButton11Click(Sender: TObject);
begin
  Editor.Paragraph.FirstIndent := Editor.Paragraph.FirstIndent + 10;
end;

procedure TFmTextos.ToolButton13Click(Sender: TObject);
begin
  FindDialog1.Execute;
end;

procedure TFmTextos.ToolButton6Click(Sender: TObject);
begin
  if FUpdating then Exit;
  if ColorDialog1.Execute then
    CurrText.Color  := ColorDialog1.Color;
end;

procedure TFmTextos.ToolButton7Click(Sender: TObject);
begin
  if FUpdating then Exit;
  if ColorDialog1.Execute then SetSelBgColor(Editor, ColorDialog1.Color);

end;

procedure TFmTextos.SetSelBgColor(RichEdit: TRichEdit; AColor: TColor);
var
  Format: CHARFORMAT2;
begin
  FillChar(Format, SizeOf(Format), 0);
  with Format do
  begin
    cbSize := SizeOf(Format);
    dwMask := CFM_BACKCOLOR;
    crBackColor := AColor;
    Richedit.Perform(EM_SETCHARFORMAT, SCF_SELECTION, Longint(@Format));
  end;
end;

procedure TFmTextos.VaiParaLinhaColuna(Tipo: TTipoLoc; Col, Row: Integer);
var
  itemp: Integer;
begin
  with Editor do
  begin
    case Tipo of
      vpFim:
      begin
        // Move to the last line:
        // Zur letzten Zeile scrollen:
        SelStart := Length(Text);
        //Perform(EM_SCROLLCARET, 0, 0);

        // or: Perform(WM_VSCROLL, SB_BOTTOM,0);
      end;
      vpIni:
      begin
        // Move to the first line:
        // Zur ersten Zeile Scrollen:
        SelStart := Perform(EM_LINEINDEX, 0, 0);
        //Perform(EM_SCROLLCARET, 0, 0);
      end;
      vpLoc:
      begin
        // Move to Line X, Character Y:
        // Cursor auf Linie x, Postion Y setzen:
        SelStart := Perform(EM_LINEINDEX, Row, 0) + Col;
        //Perform(EM_SCROLLCARET, 0, 0);
      end;

      vpRowPrior:
      begin
        // Scroll down 1 Line
        // Eine linie nach unten scrollen
        itemp := SendMessage(Handle, EM_LINEFROMchar, SelStart, 0);
        SelStart := Perform(EM_LINEINDEX, itemp - 1, 0);
        //Perform(EM_SCROLLCARET, 0, 0);
      end;

      vpRowNext:
      begin
        itemp := SendMessage(Handle, EM_LINEFROMchar, SelStart, 0);
        SelStart := Perform(EM_LINEINDEX, itemp + 1, 0);
        //Perform(EM_SCROLLCARET, 0, 0);
      end;
    end;
    SetFocus;
    keybd_event(VK_LEFT, 0, 0, 0);
    keybd_event(VK_RIGHT, 0, 0, 0);
  end;
end;

procedure TFmTextos.BitBtn1Click(Sender: TObject);
begin
  Editor.SelStart := Editor.Perform(EM_LINEINDEX, 100, 0) + 10;
end;

(*
function TFmTextos.BitmapToRTF(Graph: TBitmap): string;
var
  bi, bb, rtf:string;
  bis, bbs:cardinal;
  achar:ShortString;
  HexGraph:string;
  I:Integer;
begin
  GetDIBSizes(graph.Handle, bis, bbs);
  SetLength(bi,bis);
  SetLength(bb,bbs);
  GetDIB(graph.Handle, graph.Palette, PChar(bi)^, PChar(bb)^);
  rtf:='{\rtf1 {\pict\dibitmap ';
  SetLength(HexGraph,(Length(bb) + Length(bi)) * 2);
  I:=2;
  for bis:=1 to Length(bi) do
  begin
    achar:=Format('%x',[Integer(bi[bis])]);
    if Length(achar)=1 then achar:='0'+achar;
    HexGraph[I-1]:=achar[1];
    HexGraph[I]:=achar[2];
    Inc(I,2);
  end;
  for bbs:=1 to Length(bb) do
  begin
    achar:=Format('%x',[Integer(bb[bbs])]);
    if Length(achar)=1 then achar:='0'+achar;
    HexGraph[I-1]:=achar[1];
    HexGraph[I]:=achar[2];
    Inc(I,2);
  end;
  rtf:=rtf + HexGraph + ' }}';
  Result:=rtf;
end;
*)

end.

