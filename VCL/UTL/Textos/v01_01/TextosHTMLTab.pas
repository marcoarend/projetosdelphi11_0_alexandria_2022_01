unit TextosHTMLTab;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  dmkCheckBox, Vcl.Buttons;

type
  TFmTextosHTMLTab = class(TForm)
    Panel1: TPanel;
    Label32: TLabel;
    EdColunas: TdmkEdit;
    CkListrado: TdmkCheckBox;
    EdLinhas: TdmkEdit;
    Label1: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FListrado: Boolean;
    FColunas, FLinhas: Integer;
  end;

var
  FmTextosHTMLTab: TFmTextosHTMLTab;

implementation

uses UnMyObjects;

{$R *.dfm}

procedure TFmTextosHTMLTab.BtOKClick(Sender: TObject);
begin
  FColunas  := EdColunas.ValueVariant;
  FLinhas   := EdLinhas.ValueVariant;
  FListrado := CkListrado.Checked;
  //
  if MyObjects.FIC(FColunas = 0, EdColunas,
    'A quantidade de colunas deve ser maior que zero!')
  then
    Exit;
  if MyObjects.FIC(FLinhas = 0, EdLinhas,
    'A quantidade de linhas deve ser maior que zero!')
  then
    Exit;
  //
  Close;
end;

procedure TFmTextosHTMLTab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTextosHTMLTab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTextosHTMLTab.FormCreate(Sender: TObject);
begin
  FListrado := False;
  FColunas  := 0;
  FLinhas   := 0;
end;

procedure TFmTextosHTMLTab.FormShow(Sender: TObject);
begin
  EdColunas.SetFocus;
end;

end.
