unit UMemModule;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls,
  UnInternalConsts2, Mask, ComCtrls, (*DBTables,*) ResIntStrings,
  (*DBIProcs,*) Registry, ZCF2, buttons, TypInfo,
  dbGrids, dmkGeral, dmkValUsu, dmkImage,
  dmkLabel, dmkEdit, dmkDBLookupCombobox, dmkEditCB, dmkEditDateTimePicker,
  dmkCheckGroup, dmkMemo, dmkRadioGroup, dmkCheckBox, dmkDBEdit,
  Variants, ABSMain, MemDBCheck, UnDmkProcFunc,
  dmkPopOutFntCBox, UnDmkEnums;

const
  TMaxConfWinControl = 255;

type
{
  TBELY = (belyCodigo, belyControle, belyNumero);
  TConfWinControl = array[0..TMaxConfWinControl] of TControl;
}
  TUMemModule = class(TObject)

  private
    { Private declarations }

  public
    { Public declarations }
    //
    function CarregaSQLInsUpd(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb, IGNORE: Boolean;
             ComplUpd: String): Boolean;
{
    function CarregaSQLIns_ON_DUPLICATE_KEY(QrUpd: TABSQuery; Tabela: String;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex, SQLUpdate: array of String;
              ValCampos, ValIndex, ValUpdate: array of Variant;
              UserDataAlterweb: Boolean;
              CampoIncrementa: String = '';
              Incremento: Integer = 1): Boolean;
    function CarregaSQLReplace(QrUpd: TABSQuery; Tabela: String;
              //Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean): Boolean;
    function SQLInsUpd_Old(QrUpd: TABSQuery; Tipo: String; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant): Boolean;
    function LctUpd(QrUpd: TABSQuery; Tabela: String;
             SQLCampos: array of String; ValCampos: array of Variant;
             Data: TDateTime; Tipo, Carteira: Integer; Controle: Int64;
             Sub: Integer): Boolean;
}
    function SQLInsUpd(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean; ComplUpd: String = '';
             InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
{
    function SQLIns_ON_DUPLICATE_KEY(
             QrUpd: TABSQuery; Tabela: String; Auto_increment: Boolean;
             SQLCampos, SQLIndex, SQLUpdate: array of String;
             ValCampos, ValIndex, ValUpdate: array of Variant;
             UserDataAlterweb: Boolean;
             CampoIncrementa: String = '';
             Incremento: Integer = 1): Boolean;
    function SQLInsUpd_IGNORE(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean; ComplUpd: String = ''): Boolean;
    function SQLInsUpd_UpdExtra(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex, SQLSoUpd: array of String;
             ValCampos, ValIndex, ValSoUpd: array of Variant;
             UserDataAlterWeb: Boolean): Boolean;
             //Novo CriaFormInsUpd = FormInsUpd_Show
    function SQLReplace(QrUpd: TABSQuery; Tabela: String;
             //Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean): Boolean;
    function FormInsUpd_Show(InstanceClass: TComponentClass; var Reference;
             ModoAcesso: TAcessFmModo; Query: TABSQuery;
             Acao: TSQLType): Boolean;
             // Multiplas queries
    function FormInsUpd_Mul_Show(InstanceClass: TComponentClass; var Reference;
             ModoAcesso: TAcessFmModo; Queries: array of TABSQuery; Acao: TSQLType): Boolean;
             //para cria e fazer ShowModal manualmente
}
    function FormInsUpd_Cria(InstanceClass: TComponentClass; var Reference;
             ModoAcesso: TAcessFmModo; Query: TABSQuery;
             Acao: TSQLType): Boolean;
             //
{
    function FormInsUpd_Mul_Cria(InstanceClass: TComponentClass; var Reference;
             ModoAcesso: TAcessFmModo; Queries: array of TABSQuery;
             Acao: TSQLType): Boolean;
}
    function ConfigPanelInsUpd(Acao: TSQLType; Form: TForm; Panel: TPanel;
             Query: TABSQuery; PanelsToHide, PanelsToShow: array of TPanel;
             CompoToFocus: TWinControl; ImgTipo: TdmkImage; Tabela: String):
             Boolean;
{    function TravaFmEmPanelInsUpd(PanelsToHide, PanelsToShow: array of TPanel;
             LaTipo: TdmkLabel): Boolean;
}
    function ExecSQLInsUpdFm(Form: TForm; Acao: TSQLType; Tabela:
             String; NewItem: Variant; QrExec: TABSQuery): Boolean;
{
    function BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType; Atual: Integer;
             EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
    function BuscaEmLivreY_Def_Geral(Table, Field: String; Acao: TSQLType; Atual: Integer;
             EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
    function BuscaEmLivreY_Def_SPED_EFD(Table, Field: String; Acao: TSQLType; Atual: Integer;
             EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
    function BuscaEmLivreY_Def_Old(Table, Field: String; Acao: String; Atual: Integer): Integer;
    //
    function SenhaEspecial: Boolean;
    function AtualizaLaRegistro(Database: TM y S Q LDatabase; Tabela : String;
             RecCount, Increm : Integer) : String;
    function IncrementaControle(Database:TM y S Q LDatabase; TabControle,
             FieldControl: String) : Integer;
    function BuscaPrimeiroCodigoLivre(Tabela, Campo: String): Integer;
    function BuscaIntSafe(Database: TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Integer;
    function BuscaEmLivreY(Database:TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String; Forca_1: Boolean = True) : Integer;
    function BuscaEmLivreY_MinMax(Database: TM y S Q LDatabase; TabLivre,
             TabControle, Table, FieldControl, FieldTable : String;
             Minimo, Maximo: Integer) : Integer;
    function BuscaEmLivreY_Double(Database: TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Double;
    function BuscaEmLivreInt64Y(Database: TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Int64;
    function SelLockY(Registro : Integer; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
    function SelLockY_Double(Registro : Double; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
    function SelLockInt64Y(Registro : Int64; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
    function SelLockY_Text(Registro : String; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
    function SelLockQualquerY(Database: TM y S Q LDatabase; Table, NomeTable : String) : Boolean;
    function SelLockA(Registro : String; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
    function VerificaDuplicadoStr(Database: TM y S Q LDatabase; Tabela, CampoDescri, CampoCod, Descricao,
             CampoAnterior: String; Codigo : Integer) : Boolean;
}
    function  VerificaDuplicado1(Avisa: Boolean; Database, Tabela: String;
              CamposFind: array of String; ValoresFind: array of Variant;
              CamposOmit: array of String; ValuesOmit: array of Variant;
              CampoRes: String; var ValueRes: Variant): Boolean;

{
    function NegaInclusaoY(Database: TM y S Q LDatabase; Tabela, Campo, TabLivre:
             String; Maximo: Integer) : Boolean;
    function NaoPermiteExclusao(Tabela, Form : String; Msg: Integer) : Boolean;
    //function AcessoNegadoAoForm(Tabela, Form : String; Msg: Integer) : Boolean;
    function DiasUteis(DataI, DataF: TDateTime): Integer;
    //function FormShow(InstanceClass: TComponentClass;
             //Reference: TComponent; Form: TForm; ModoAcesso: TAcessFormMode): Boolean;
    //function FormShow(InstanceClass: TComponentClass;
             //var Reference; ModoAcesso: TAcessFormModo): Boolean;
    //function FormCria(InstanceClass: TComponentClass; var Reference;
             //ModoAcesso: TAcessFormModo): Boolean;
    //function FormPode(InstanceClass: TComponentClass; var Reference;
      //       ModoAcesso: TAcessFormModo): Boolean;
    //
    function DiaInutil(Data: TDateTime): Integer;
    function CalculaDataDeposito(Vencimento: TDateTime): TDateTime;
    function CalculaDiasArray_Pula(Data, Vcto: TDateTime; DMai, Comp: Integer): MyArrayI1k;
    function CalculaDiasArray_Normal(Data, Vcto: TDateTime; DMai, Comp: Integer): MyArrayI1k;
    function CalculaDiasArray_NaoPula(Data, Vcto: TDateTime; DMai,
             Comp:Integer): MyArrayI1k;
    function CalculaDias_Pula(Data, Vcto: TDateTime; DMai, Comp: Integer): Integer;
    function CalculaDias_Normal(Data, Vcto: TDateTime; DMai, Comp: Integer): Integer;
    function CalculaDias_NaoPula(Data, Vcto: TDateTime; DMai,
             Comp:Integer): Integer;
    function CalculaDiasArray(Data, Vcto: TDateTime; DMai, Comp, Pula: Integer):
             MyArrayI1k;
    function CalculaDias(Data, Vcto: TDateTime; DMai, Comp, Pula, CBE: Integer): Integer;
    function DiaUtilMes(DataIni: TDateTime; DiaIni, QtdUteis, MaxDia: Integer): Integer;
    //
}
    function ProximoRegistro(Query: TABSQuery; Campo: String; Atual:
             Integer): Integer;
{
    //Exclui todos registros
    function ExcluiTodosRegistros(Pergunta, Tabela: String): Integer;
}
    //ExcluiRegistroI1
    function ExcluiRegistroInt1(Pergunta, Tabela, Campo: String; Inteiro1:
             Integer): Integer;
{
    //ExcluiRegistroI2
    function ExcluiRegistroInt2(Pergunta, Tabela, Campo1, Campo2: String;
             Inteiro1, Inteiro2: Integer): Integer;
    function ExcluiRegistroTxt1(Pergunta, Tabela, Campo: String; Texto1:
             String): Integer;
    function ObtemCamposDeTabelaIdentica(DataBase: TM y S Q LDatabase;
             Tabela, Prefix: String): String;// Prefix -> 'la.' = ref tabela
    function ConfigJanela(Janela: String; Lista: TConfWinControl): Boolean;
    function ConfigJanela10(Janela: String;
             c0, c1, c2, c3, c4, c5, c6, c7, c8, c9:TControl): Boolean;
    function ConfigJanela20(Janela: String;
             c0, c1, c2, c3, c4, c5, c6, c7, c8, c9,
             c10, c11, c12, c13, c14, c15, c16, c17, c18, c19:TControl): Boolean;

    procedure LogDel(Database: TM y S Q LDataBase; Tipo, ID: Integer);
    procedure LogIns(Database: TM y S Q LDataBase; Tipo, ID: Integer);
    procedure UpdateControleData(Database: TM y S Q LDatabase; TabControle, Field : String; Data : TDate);
    procedure UpdLockY(Registro : Integer; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdLockY_Text(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdLockY_Double(Registro : Double; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdLockA(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdLockInt64Y(Registro : Int64; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdUnlockY(Registro : Integer; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdUnlockY_Text(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdUnlockY_Double(Registro : Double; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdUnlockInt64Y(Registro : Int64; Database: TM y S Q LDatabase; Table, Field : String);
    procedure UpdLockTudoY(Database: TM y S Q LDatabase; Table : String);
    procedure UpdUnlockTudoW(Database: TM y S Q LDatabase; Table : String);
    procedure UpdUnlockA(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
    procedure PoeEmLivreY(Database: TM y S Q LDatabase; TabLivre, Table : String; Codigo : Integer);
    //procedure PoeEmLivreY_Text(Database: TM y S Q LDatabase; TabLivre, Table : String; Codigo : String);
    procedure PoeEmLivreY_Double(Database: TM y S Q LDatabase; TabLivre, Table : String; Codigo : Double);
    function SituacaoLancamento(QrLct: TABSQuery): String;
    // TM y S Q LTable
    function VerificaCamposObrigatorios(Tabela: TM y S Q LTable): Boolean;
    procedure DadosAutomaticosTb(Tabela: TM y S Q LTable);
    procedure MostraEdicaoTb(Form: TForm);
    procedure OcultaEdicaoTb(Form: TForm);
    procedure AlteraRegistroTb(Form: TForm; Tabela: TM y S Q LTable;
              ComponentToFocus: TWinControl; LaTipo: TdmkLabel);
    procedure IncluiRegistroTb(Form: TForm; Tabela: TM y S Q LTable;
              ComponentToFocus: TWinControl; LaTipo: TdmkLabel);
    //procedure DesisteRegistroTb(Form: TForm; Tabela: TM y S Q LTable);
    procedure DesisteRegistroTb(Form: TForm; Tabela: TM y S Q LTable;
              NomeTabela, NomeCampoCodigo: String;
              SQLType: TSQLType; CodigoALiberar: Integer);
    procedure ConfirmaRegistroTb_Codigo(Form: TForm; Tabela: TM y S Q LTable);
    procedure ConfirmaRegistroTb_Numero(Form: TForm; Tabela: TM y S Q LTable);
    procedure BELY_Tb(Tabela: TM y S Q LTable; Campo: TBELY);
    procedure VerificaFeriadosFuturos(ComponentClass: TComponentClass;
              Reference: TComponent);
    //
    function SQLInsUpdL(QrUpd: TABSQuery; Tipo, Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant): Boolean;
    function SQLDel1(QrExec, QrData: TABSQuery; Tabela, Campo: String;
             Valor: Variant; PerguntaSeExclui: Boolean;
             PerguntaAlternativa: String): Boolean;
    function SQLDel2(QrExec, QrData: TABSQuery; Tabela, CampoLoc: String;
             Campos: array of String; Valores: array of Variant;
             PerguntaSeExclui: Boolean; PerguntaAlternativa: String): Boolean;
    function SQLLoc1(Query: TABSQuery; Tabela, Campo: String;
             Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
    //

    procedure ExportaRegistrosEntreDBs(TabOrig, TabDest, CondicaoSQL: String;
              BaseOrig, BaseDest: TM y S Q LDatabase; RichEdit: TRichEdit);
    function  EspelhaRegistroEntreTabelas(RichEdit: TRichEdit;
              //SQLCampos: array of String; ValCampos: array of Variant;
              TabOrig, TabDest: String; BaseOrig, BaseDest: TM y S Q LDatabase;
              WHERE_Condition: String): Boolean;
    // OpenQuery - QueryOpen          
    function  AbreQuery(Query: TABSQuery; Aviso: String): Boolean;
    function  ReabreQuery(Query: TABSQuery; Valores: array of Variant;
              Aviso: String): Boolean;
}
    function  ExecutaQuery(Query: TABSQuery): Boolean;
{
    function  ExecutaDB(DB: TM y S Q LDatabase; SQL: String): Boolean;
}

    function  HabilitaMenuItemInt(MenuItens: array of TMenuItem;
              Query: TABSQuery; Campo: String; MinRec, MinVal: Integer): Boolean;
{
    procedure AtzEntiAltDtPlaCt(QrUpd: TABSQuery; CliInt: Integer;
              Data: TDateTime);
}
    function ExecSQLInsUpdPanel(Acao: TSQLType; Form: TForm; Panel: TPanel;
             Tabela: String; NewItem: Variant; QrExec: TABSQuery;
             PanelsToHide, PanelsToShow: array of TPanel; ImgTipo: TdmkImage;
             UserDataAlterweb: Boolean; ShowSQLText: Boolean = False): Boolean;
{
    function CancelSQLInsUpdPanel(Acao: TSQLType; Form: TForm; Panel: TPanel;
             Tabela: String; NewItem: Variant; QrExec: TABSQuery;
             PanelsToHide, PanelsToShow: array of TPanel; LaTipo: TdmkLabel;
             Campo: String): Boolean;
}
    function BuscaNovoCodigo_Int(QrAux: TABSQuery; Tabela, Campo: String;
             CamposExtras: array of String; ValExtras: array of Variant;
             SQLType: TSQLType; Padrao: Integer; Sinal: TSinal;
             dmkEdit: TdmkEdit): Integer;
    function AbreABSQuery1(Query: TABSQuery; SQL: array of String): Boolean;
    function AbreABSTable1(Table: TABSTable): Boolean;
    //
{
    procedure StartTransaction(Qry: TABSQuery);
    procedure Commit(Qry: TABSQuery);
    procedure RollBack(Qry: TABSQuery);
    function ObtemCodigoDeCodUsu(EdCodUsu: TdmkEditCB; var Codigo: Integer;
             Aviso: String; Campo: String = 'Codigo'; CodUsu: String =
             'CodUsu'): Boolean;
    function ObtemValorDoCampoXDeIndex_Int(Index: Integer;
              CampoX, CampoIndex: String; Query: TABSQuery;
              dmkEditCB: TdmkEditCB; dmkDBlookupComboBox: TdmkDBlookupComboBox;
              var Valor: Integer): Boolean;
    function SetaCodUsuDeCodigo(EdCodUsu: TdmkEditCB; CBCodUsu:
             TdmkDBLookupComboBox; Qry: TABSQuery; Codigo: Integer;
             Campo: String = 'Codigo'; CodUsu: String =
             'CodUsu'): Boolean;
}
             //SetaCodigoSelecionado
    function SetaCodigoPesquisado(EdCodigo: TdmkEditCB; CBCodigo:
             TdmkDBLookupComboBox; Qry: TABSQuery; Codigo: Integer;
             Campo: String = 'Codigo'): Boolean;
{
    function Busca_IDCtrl_NFe(Acao: TSQLType; Atual: Integer): Integer;
    function ObtemProximoCHTalao(Carteira: Integer; EdSerieCH, EdDoc: TdmkEdit): Boolean;
    function ObtemQtdeCHTaloes(Carteira: Integer): Integer;
    function MudaOrdemRegistroAtual(Tabela, FldIndice, FldOrdem: String;
             QueryAReordenar, QrUpd: TABSQuery): Boolean;
    function AcertaOrdemTodosRegistros(Tabela, FldIndice, FldOrdem: String;
             QueryAReordenar, QrUpd: TABSQuery): Boolean;
    function  DefineDataBase(MeuDB: String): Boolean;
    function  RegistrosNaTabela(Tabela, ComplSQL: String; Database: TM y S Q LDatabase): Integer;
    function  MontaOrdemSQL(Pertence: array of Boolean; Campo: array of String):
              String;
}
   function ImpedeExclusaoPeloNomeDaTabela(Tabela: String): Boolean;

 end;

var
  UMemMod: TUMemModule;

implementation


uses UnMyObjects, (*Module,*) UnGOTOm, UnMyVCLRef, GModule {UnLock_MLA, SenhaEspecial, ConfJanela, Principal,
SelDBs, AdvGlowButton, AdvToolBar};


{
function TUMemModule.SQLInsUpd_Old(QrUpd: TABSQuery; Tipo: String; Tabela: String;
         Auto_increment: Boolean;
         SQLCampos, SQLIndex: array of String;
         ValCampos, ValIndex: array of Variant): Boolean;
var
  SQLType: TSQLType;

begin
  if Tipo = CO_INCLUSAO then SQLTYpe := stIns else
  if Tipo = CO_ALTERACAO then SQLTYpe := stUpd else
  SQLTYpe := stLok;
  Result := SQLInsUpd(QrUpd, SQLType, lowercase(Tabela), Auto_increment,
    SQLCampos, SQLIndex, ValCampos, ValIndex, True);
end;
}

function TUMemModule.CarregaSQLInsUpd(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb, IGNORE: Boolean; ComplUpd: String): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab, StrCampos, StrValues: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j+1) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if (i >= 0) and Auto_increment then
  begin
    Geral.MensagemBox('AVISO! Existem ' + IntToStr(i+1) + ' �ndices informados ' +
    'mas foi definido que � "AUTO_INCREMENT" no "CarregaSQLInsUpd"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    //Exit;
  end;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MensagemBox('AVISO: O status da a��o est� definida como ' +
    '"Travado"', 'AVISO IMPORTANTE', MB_OK+MB_ICONWARNING);
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = stIns then
  begin
    if IGNORE then
      // n�o gera erro nem inclui o registro quando poderia duplicar registro com chave
      QrUpd.SQL.Add('INSERT IGNORE INTO ' + Lowercase(Tab))
    else
      QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab));
  end else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  StrCampos := '';
  StrValues := '';
  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if Tipo = stIns then
    begin
      if (i < j) or UserDataAlterweb then
      begin
        StrCampos := StrCampos + SQLCampos[i] + ', ';
        StrValues := StrValues + Valor + ', ';
      end else
      begin
        StrCampos := StrCampos + SQLCampos[i];
        StrValues := StrValues + Valor;
      end;
    end else
    begin
      if (i < j) or UserDataAlterweb then
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
      else
        QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
    end;
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
    begin
        StrCampos := StrCampos + ', DataCad, UserCad';
        StrValues := StrValues + ', ' + Data + ', ' + IntToStr(VAR_USUARIO);
    end else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MensagemBox('SQL com Auto Increment e Indice ao mesmo tempo!',
      'ERRO', MB_OK+MB_ICONERROR);
    // N�o faz nada
  end else begin

    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      if Tipo = stIns then
      begin
        StrCampos := StrCampos + ', ' + SQLIndex[i];
        StrValues := StrValues + ', ' + Valor;
      end else
      begin
        if Tipo = stIns then Liga := ', ' else
        begin
          if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
        end;
        QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
      end;
    end;
  end;
  if Tipo = stIns then
  begin
    QrUpd.SQL.Add('(' + StrCampos + ') VALUES (' + StrValues + ')');
  end else begin
    if Trim(ComplUpd) <> '' then
    begin
      QrUpd.SQL.Add(ComplUpd);
    end;
  end;
  QrUpd.SQL.Add(';');
  Result := True;
end;

{
function TUMemModule.CarregaSQLReplace(QrUpd: TABSQuery; Tabela: String;
  //Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j) + ' valores para estes �ndices em "CarregaSQLInsUpd"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Tab := LowerCase(Tabela);
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('REPLACE INTO ' + Lowercase(Tab) + ' SET ');
  if UserDataAlterweb then
    QrUpd.SQL.Add('AlterWeb=1, ');
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //
  if UserDataAlterweb then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO));
  //
  (*
  if Auto_increment and (Tipo = stIns) then
  begin
    if High(SQLIndex) > 0 then
      Geral.MensagemBox('SQL com Auto Increment e Indice ao mesmo tempo!',
      'ERRO', MB_OK+MB_ICONERROR);
    // N�o faz nada
  end else begin
  *)
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      //if Tipo = stIns then
      Liga := ', '
      (*
      else begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end*);
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  //end;
  QrUpd.SQL.Add(';');
  Result := True;
end;

function TUMemModule.LctUpd(QrUpd: TABSQuery; Tabela: String;
 SQLCampos: array of String; ValCampos: array of Variant;
 Data: TDateTime; Tipo, Carteira: Integer; Controle: Int64;
 Sub: Integer): Boolean;
begin
  Result := CarregaSQLInsUpd(QrUpd, stUpd, lowercase(Tabela), False,
  SQLCampos, ['Data', 'Tipo', 'Carteira', 'Controle', 'Sub'],
  ValCampos, [Geral.FDT(Data, 1), Tipo, Carteira, Controle, Sub], True, False, '');
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
      //GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
    except
      GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
      raise;
    end;
  end else;
end;
}

function TUMemModule.SQLInsUpd(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; ComplUpd: String = '';
  InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
begin
  Result := CarregaSQLInsUpd(QrUpd, Tipo, lowercase(Tabela), Auto_increment,
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, False, ComplUpd);
  if Result then
  begin
    //try
      QrUpd.ExecSQL;
      Result := True;
      //GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
    (*
    except
      if InfoSQLOnError then
        GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
      if InfoErro then
        raise;
    end;
    *)
  end else;
end;

{
// CUIDADO Replace exclui a chave antes de inserir
// para n�o excluir use ON DUPLICATE KEY (ON_DUPLICATE_KEY)
function TUMemModule.SQLReplace(QrUpd: TABSQuery; Tabela: String;
  //Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean): Boolean;
begin
  Result := CarregaSQLReplace(QrUpd, lowercase(Tabela),
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb);
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
      //GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
    except
      GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
      raise;
    end;
  end else;
end;

// ON DUPLICATE KEY
function TUMemModule.SQLIns_ON_DUPLICATE_KEY(
  QrUpd: TABSQuery; Tabela: String; Auto_increment: Boolean;
  SQLCampos, SQLIndex, SQLUpdate: array of String;
  ValCampos, ValIndex, ValUpdate: array of Variant;
  UserDataAlterweb: Boolean;
  CampoIncrementa: String = '';
  Incremento: Integer = 1): Boolean;
begin
  Result := CarregaSQLIns_ON_DUPLICATE_KEY(QrUpd, lowercase(Tabela), Auto_increment,
  SQLCampos, SQLIndex, SQLUpdate, ValCampos, ValIndex, ValUpdate,
  UserDataAlterweb, CampoIncrementa, Incremento);
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
      //GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
    except
      GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
      raise;
    end;
  end else;
end;

function TUMemModule.CarregaSQLIns_ON_DUPLICATE_KEY(QrUpd: TABSQuery; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex, SQLUpdate: array of String;
  ValCampos, ValIndex, ValUpdate: array of Variant;
  UserDataAlterweb: Boolean;
  CampoIncrementa: String = '';
  Incremento: Integer = 1): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  Result := False;
  i := High(SQLCampos);
  j := High(ValCampos);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' campos e ' +
    IntToStr(j+1) + ' valores para estes campos em "CarregaSQLInsUpd_ON_DUPLICATE_KEY"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  i := High(SQLIndex);
  j := High(ValIndex);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' �ndices e ' +
    IntToStr(j) + ' valores para estes �ndices em "CarregaSQLInsUpd_ON_DUPLICATE_KEY"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  i := High(SQLUpdate);
  j := High(ValUpdate);
  if i <> j then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(i+1) + ' campos de update e ' +
    IntToStr(j) + ' valores para estes campos de update em "CarregaSQLInsUpd_ON_DUPLICATE_KEY"!' + #13#10 +
    'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Tab := LowerCase(Tabela);
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ');
  if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //

  if UserDataAlterweb then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO));
    //QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  //
  if Auto_increment then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      Liga := ', ';
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  end;

  // ON DUPLICATE KEY

  QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE ');
  j := High(SQLUpdate);
  for i := Low(SQLUpdate) to j do
  begin
    Valor := Geral.VariavelToString(ValUpdate[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLUpdate[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLUpdate[i] + '=' + Valor);
    if Trim(CampoIncrementa) <> '' then
      QrUpd.SQL.Add(', ' + CampoIncrementa + '=' + CampoIncrementa + ' + ' +
      FormatFloat('0', Incremento));
  end;
  if UserDataAlterweb then
    QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  //
  Result := True;
end;

function TUMemModule.SQLInsUpd_IGNORE(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; ComplUpd: String = ''): Boolean;
begin
  Result := CarregaSQLInsUpd(QrUpd, Tipo, lowercase(Tabela), Auto_increment,
  SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, True, ComplUpd);
  if Result then
  begin
    try
      QrUpd.ExecSQL;
      Result := True;
    except
      GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
      raise;
    end;
  end;
end;

function TUMemModule.SQLInsUpd_UpdExtra(QrUpd: TABSQuery; Tipo: TSQLType; Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex, SQLSoUpd: array of String;
  ValCampos, ValIndex, ValSoUpd: array of Variant;
  UserDataAlterWeb: Boolean): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  //Result := False;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> stIns) and (Tipo <> stUpd) ) then
  begin
    Geral.MensagemBox('AVISO: O status da a��o est� definida como ' +
    '"' + Geral.LaTipo(Tipo) + '"', 'AVISO IMPORTANTE', MB_OK+MB_ICONWARNING);
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = stIns then
    QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ')
  else begin
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');
    if UserDataAlterweb then
      QrUpd.SQL.Add('AlterWeb=1, ');
  end;
  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    if (i < j) or UserDataAlterweb then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ')
    else
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  //

  if UserDataAlterweb then
  begin
    if Tipo = stIns then
      QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
    else
      QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  end;
  //
  if Auto_increment and (Tipo = stIns) then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      if Tipo = stIns then Liga := ', ' else
      begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end;
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
    //
    if Tipo = stUpd then
    begin
      for i := Low(SQLSoUpd) to High(SQLSoUpd) do
      begin
        Valor := Geral.VariavelToString(ValSoUpd[i]);
        QrUpd.SQL.Add('AND ' + SQLSoUpd[i] + '=' + Valor);
      end;
    end;
    //
  end;
  //
  try
    QrUpd.ExecSQL;
    Result := True;
  except
    GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
    raise;
  end;
end;

function TUMemModule.FormInsUpd_Show(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Query: TABSQuery; Acao: TSQLType): Boolean;
begin
  Result := False;
  if FormInsUpd_Cria(InstanceClass, Reference, ModoAcesso, Query, Acao) then
  begin
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    Result := True;
  end;
end;

function TUMemModule.FormInsUpd_Mul_Show(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Queries: array of TABSQuery; Acao: TSQLType): Boolean;
begin
  Result := False;
  if FormInsUpd_Mul_Cria(InstanceClass, Reference, ModoAcesso, Queries, Acao) then
  begin
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    Result := True;
  end;
end;
}

function TUMemModule.FormInsUpd_Cria(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Query: TABSQuery; Acao: TSQLType): Boolean;
  procedure Mensagem(Campo: String; Objeto: TObject);
  begin
    Geral.MensagemBox('N�o foi poss�vel definir o valor ' +
    'do campo "' + Campo + '" no componente "' +
    TComponent(Objeto).Name + '"', 'Erro', MB_OK+MB_ICONERROR);
  end;
var
  Objeto: TObject;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PropInfo: PPropInfo;
  i: Integer;
  Campo: String;
  Valor: Variant;
  MySou: TDataSource;
  MySet: TDataSet;
  //MyQry: TABSQuery;
  MyFld: String;
  //PropVal: Variant;
  Achou: Integer;
begin
  Result := DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso);
  if not Result then Exit;
  with TForm(Reference) do
  begin
    Achou := 0;

    Campo := 'LaTipo';
    if (FindComponent(Campo) as TLabel) <> nil then
    begin
      TdmkLabel(FindComponent(Campo) as TLabel).SQLType := Acao;
      if TdmkLabel(FindComponent(Campo) as TLabel).SQLType <> Acao then
      Geral.MensagemBox('N�o foi poss�vel definir o tipo de a��o ' +
      'na janela ' + TForm(Reference).Name + '!', 'Erro', MB_OK+MB_ICONERROR)
      else Achou := Achou + 1;
    end;

    Campo := 'ImgTipo';
    if (FindComponent(Campo) as TdmkImage) <> nil then
    begin
      TdmkImage(FindComponent(Campo) as TImage).SQLType := Acao;
      if TdmkImage(FindComponent(Campo) as TdmkImage).SQLType <> Acao then
      Geral.MensagemBox('N�o foi poss�vel definir o tipo de a��o ' +
      'na janela ' + TForm(Reference).Name + '!', 'Erro', MB_OK+MB_ICONERROR)
      else Achou := Achou + 1;
    end;

    if Achou = 0 then Geral.MensagemBox('O formul�rio ' +
    TForm(Reference).Name + ' n�o possui "label" para definir o tipo de a��o!',
    'Erro', MB_OK+MB_ICONERROR);
    //
    with TForm(Reference) do
    begin
      for i := 0 to ComponentCount -1 do
      begin
        Objeto := Components[i];
        //
        if Acao = stUpd then
        begin
          PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
          PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
          PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
          PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
          if PI_OldValor <> nil then
          begin
            if (PI_DataField <> nil) and (PI_DataSource <> nil) then
            begin
              //
              //PropVal := GetPropValue(Objeto, 'DataSource');
              //MySet   := TDataSource(PropVal);
              //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
              MySou := TDataSource(TDBEdit(Objeto).DataSource);
              MySet := TDataSource(MySou).DataSet;
              //MyQry := TABSQuery(TDataSource(MySet));
              MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
              SetPropValue(Objeto, 'OldValor',
                TDataSet(MySet).FieldByName(MyFld).AsVariant);

            end else if PI_QryCampo <> nil then
            begin
              Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
              if Campo <> '' then
              begin
                PropInfo := GetPropInfo(Objeto, 'OldValor');
                if PropInfo <> nil then
                try
                  SetPropValue(Objeto, 'OldValor',
                    TABSQuery(Query).FieldByName(Campo).AsVariant);
                except
                  Geral.MensagemBox('N�o foi poss�vel definir o ' +
                  '"OldValor" = "' + Geral.VariantTostring(
                  TABSQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                  Campo + '" no objeto "' + TComponent(Objeto).Name + '"', 'ERRO',
                  MB_OK+MB_ICONERROR);
                end;
              end;
            end;
          end;
        end;
        //fim OldValor
        //
        PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
        if PropInfo <> nil then
        begin
          Campo := GetStrProp(TComponent(Objeto), PropInfo);
          if Campo <> '' then
          begin
            //
            if Acao = stIns then Valor := Null else Valor :=
              TABSQuery(Query).FieldByName(Campo).AsVariant;
            try
              if (Objeto is TdmkEdit) then
              begin
                if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                begin
                  Valor := Geral.MesEAnoDoMez(Valor);
                  TdmkEdit(Objeto).Texto := Valor;
                end else
                if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                begin
                  Valor := Geral.AnoEMesDoMez(Valor);
                  TdmkEdit(Objeto).Texto := Valor;
                end else
                TdmkEdit(Objeto).ValueVariant := Valor;
              end
              //
              else if (Objeto is TdmkDBLookupCombobox) then
                TdmkDBLookupCombobox(Objeto).KeyValue := Valor
              //
              else if (Objeto is TdmkEditCB) then
                TdmkEditCB(Objeto).ValueVariant := Valor
              //
              else if (Objeto is TdmkEditDateTimePicker) then
              begin
                if Valor <> Null then
                  //TdmkEditDateTimePicker(Objeto).Date := Date
                //else
                  TdmkEditDateTimePicker(Objeto).Date := Valor;
              end
              //
              else if (Objeto is TdmkMemo) then
              begin
                if Valor = Null then Valor := '';
                TdmkMemo(Objeto).Text := Valor;
              end
              //
              else if (Objeto is TdmkRadioGroup) then
              begin
                if Valor = Null then Valor := -1;
                TdmkRadioGroup(Objeto).ItemIndex := Valor;
              end
              //
              else if (Objeto is TdmkCheckBox) then
              begin
                if Valor = Null then Valor := False;
                TdmkCheckBox(Objeto).Checked := Geral.IntToBool(Valor);
              end
              //
              else if (Objeto is TdmkCheckGroup) then
              begin
                if Valor = Null then Valor := 0;
                TdmkCheckGroup(Objeto).Value := Valor;
              end
              //
              else if (Objeto is TdmkPopOutFntCBox) then
                TdmkPopOutFntCBox(Objeto).FonteNome := Valor
              //
              else if (Objeto is TdmkValUsu) then
                TdmkValUsu(Objeto).ValueVariant := Valor
              //

              //

              else if (Objeto is TdmkDBEdit) then begin end //nada
              //
              else Mensagem(Campo, Objeto);
            except
              Mensagem(Campo, Objeto);
            end;
          end;
        end;
      end;
    end;
    //
    (*
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    *)
  end;
end;

{
function TUMemModule.FormInsUpd_Mul_Cria(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; Queries: array of TABSQuery; Acao: TSQLType): Boolean;
  procedure Mensagem(Campo: String; Objeto: TObject);
  begin
    Geral.MensagemBox('N�o foi poss�vel definir o valor ' +
    'do campo "' + Campo + '" no componente "' +
    TComponent(Objeto).Name + '"', 'Erro', MB_OK+MB_ICONERROR);
  end;
var
  Objeto: TObject;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PI_QryNome: PPropInfo;
  i, j: Integer;
  NomeQry: String;
  Campo: String;
  Valor: Variant;
  MySou: TDataSource;
  MySet: TDataSet;
  Query: TABSQuery;
  MyFld: String;
  OldValor: String;
  //PropVal: Variant;
begin
  Result := DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso);
  if not Result then Exit;
  with TForm(Reference) do
  begin
    Campo := 'LaTipo';
    if (FindComponent(Campo) as TLabel) <> nil then
    begin
      TdmkLabel(FindComponent(Campo) as TLabel).SQLType := Acao;
      if TdmkLabel(FindComponent(Campo) as TLabel).SQLType <> Acao then
      Geral.MensagemBox('N�o foi poss�vel definir o tipo de a��o ' +
      'na janela ' + TForm(Reference).Name + '!', 'Erro', MB_OK+MB_ICONERROR);
    end else Geral.MensagemBox('O formul�rio ' +
    TForm(Reference).Name + ' n�o possui "label" para definir o tipo de a��o!',
    'Erro', MB_OK+MB_ICONERROR);
    //
    with TForm(Reference) do
    begin
      Query := nil;
      for i := 0 to ComponentCount -1 do
      begin
        Objeto := Components[i];
        //
        if Acao = stUpd then
        begin
          PI_QryNome := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryName');
          if PI_QryNome <> nil then
          begin
            NomeQry := GetStrProp(TComponent(Objeto), PI_QryNome);
            for j := Low(Queries) to High(Queries) do
            begin
              if TABSQuery(Queries[j]).Name = NomeQry then
              begin
                Query := Queries[j];
                Break;
              end;
            end;
          end;
          // else Geral.MensagemBox(
            //'N�o foi poss�vel definir a query pelo "QryName"', 'Aviso',
            //MB_OK+MB_ICONERROR);
          //
          PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
          PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
          PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
          PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
          if PI_OldValor <> nil then
          begin
            if (PI_DataField <> nil) and (PI_DataSource <> nil) then
            begin
              //
              //PropVal := GetPropValue(Objeto, 'DataSource');
              //MySet   := TDataSource(PropVal);
              //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
              MySou := TDataSource(TDBEdit(Objeto).DataSource);
              MySet := TDataSource(MySou).DataSet;
              //MyQry := TABSQuery(TDataSource(MySet));
              MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
              SetPropValue(Objeto, 'OldValor',
                TDataSet(MySet).FieldByName(MyFld).AsVariant);

            end else if PI_QryCampo <> nil then
            begin
              Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
              if Campo <> '' then
              begin
                PI_OldValor := GetPropInfo(Objeto, 'OldValor');
                if PI_OldValor <> nil then
                try
                  if TABSQuery(Query).State <> dsInactive then
                  begin
                    OldValor := M L A G e r a l.VariantToString(
                      TABSQuery(Query).FieldByName(Campo).AsVariant);
                    SetPropValue(Objeto, 'OldValor', OldValor);
                  end;
                except
                  Geral.MensagemBox('N�o foi poss�vel definir o ' +
                  '"OldValor" = "' + M L A G e r a l.VariantTostring(
                  TABSQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                  Query.Name + '.' + Campo + '" no objeto "' +
                  TComponent(Objeto).Name + '"', 'ERRO',
                  MB_OK+MB_ICONERROR);
                end;
              end;
            end;
          end;
        end;
        //fim OldValor
        //
        PI_QryCampo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
        if PI_QryCampo <> nil then
        begin
          Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
          if Campo <> '' then
          begin
            if Acao = stIns then Valor := Null else Valor :=
              TABSQuery(Query).FieldByName(Campo).AsVariant;
            try
              if (Objeto is TdmkEdit) then
              begin
                try
                if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                  Valor := M L A G e r a l.MesEAnoDoMez(Valor)
                else
                if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                  Valor := M L A G e r a l.AnoEMesDoMez(Valor);
                //
                TdmkEdit(Objeto).ValueVariant := Valor;
                except
                  ShowMessage(TdmkEdit(Objeto).Name);
                end;
              end
              //
              else if (Objeto is TdmkDBLookupCombobox) then
                TdmkDBLookupCombobox(Objeto).KeyValue := Valor
              //
              else if (Objeto is TdmkEditCB) then
                TdmkEditCB(Objeto).ValueVariant := Valor
              //
              else if (Objeto is TdmkEditDateTimePicker) then
              begin
                if Valor <> Null then
                  //TdmkEditDateTimePicker(Objeto).Date := Date
                //else
                  TdmkEditDateTimePicker(Objeto).Date := Valor;
              end
              //
              else if (Objeto is TdmkMemo) then
              begin
                if Valor = Null then Valor := '';
                TdmkMemo(Objeto).Text := Valor;
              end
              //
              else if (Objeto is TdmkRadioGroup) then
              begin
                if Valor = Null then Valor := -1;
                TdmkRadioGroup(Objeto).ItemIndex := Valor;
              end
              //
              else if (Objeto is TdmkCheckBox) then
              begin
                if Valor = Null then Valor := False;
                TdmkCheckBox(Objeto).Checked := M L A G e r a l.IntToBool(Valor);
              end
              //
              else if (Objeto is TdmkCheckGroup) then
              begin
                if Valor = Null then Valor := 0;
                TdmkCheckGroup(Objeto).Value := Valor;
              end
              //
              else if (Objeto is TdmkPopOutFntCBox) then
                TdmkPopOutFntCBox(Objeto).FontName := Valor
              //
              else if (Objeto is TdmkValUsu) then
                TdmkValUsu(Objeto).ValueVariant := Valor
              //

              //

              else if (Objeto is TdmkDBEdit) then begin end //nada
              //
              else Mensagem(Campo, Objeto);
            except
              Mensagem(Campo, Objeto);
            end;
          end;
        end;
      end;
    end;
    //
    (*
    TForm(Reference).ShowModal;
    TForm(Reference).Destroy;
    *)
  end;
end;
}

function TUMemModule.ExecSQLInsUpdFm(Form: TForm; Acao: TSQLType; Tabela:
String; NewItem: Variant; QrExec: TABSQuery): Boolean;
var
  SQL: String;
  Cursor: TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Result := False;
    if Acao in ([stIns, stUpd]) then
    begin
      SQL := MyVCLref.CriaSQLdmkForm(Form, Acao, lowercase(Tabela), NewItem, True);
      QrExec.Close;
      QrExec.SQL.Text := SQL;
      try
        QrExec.ExecSQL;
        Result := True;
      except
        GOTOm.LeMeuSQL_Fixo_m(QrExec, '', nil, True, True);
        raise;
      end;
    end else Geral.MensagemBox('A��o n�o implementada: "Travado"' + #13#10 +
      'function TUMemModule.ExecSQLInsUpdFm()',
    'Erro', MB_OK+MB_ICONERROR);
  finally
    Screen.Cursor := Cursor;
  end;
end;

{
function TUMemModule.CancelSQLInsUpdPanel(Acao: TSQLType; Form: TForm;
Panel: TPanel; Tabela: String; NewItem: Variant; QrExec: TABSQuery;
PanelsToHide, PanelsToShow: array of TPanel; LaTipo: TdmkLabel;
Campo: String): Boolean;
var
  i, j, k: Integer;
begin
  //
  i := Low(PanelsToShow);
  j := High(PanelsToShow);
  for k := i to j do PanelsToShow[i].Visible := True;
  //
  i := Low(PanelsToHide);
  j := High(PanelsToHide);
  for k := i to j do PanelsToHide[i].Visible := False;
  //
  //if Acao = stIns then
    //UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', lowercase(Tabela), NewItem);
  UMyMod.UpdUnlockY(NewItem, QrExec.Database, lowercase(Tabela), Campo);
  Result := True;
end;
}

function TUMemModule.ExecSQLInsUpdPanel(Acao: TSQLType; Form: TForm;
Panel: TPanel; Tabela: String; NewItem: Variant; QrExec: TABSQuery;
PanelsToHide, PanelsToShow: array of TPanel; ImgTipo: TdmkImage;
UserDataAlterweb: Boolean; ShowSQLText: Boolean = False): Boolean;
var
  SQL: String;
  i, j, k: Integer;
  Compo: TComponent;
begin
  Result := False;
  SQL := MyVCLref.CriaSQLdmkPanel_Sets(Form, Panel, Acao, lowercase(Tabela), NewItem, UserDataAlterweb);
  QrExec.Close;
  QrExec.SQL.Text := SQL;
  try
    ExecutaQuery(QrExec);
    Result := True;
    //
    i := Low(PanelsToShow);
    j := High(PanelsToShow);
    for k := i to j do PanelsToShow[i].Visible := True;
    //
    i := Low(PanelsToHide);
    j := High(PanelsToHide);
    for k := i to j do PanelsToHide[i].Visible := False;
    //
    if ImgTipo <> nil then
      ImgTipo.SQLType := stLok;
    for i := 0 to Form.ComponentCount - 1 do
    begin
      Compo := Form.Components[i];
      if  (Compo.Name = 'SbNumero')
      or  (Compo.Name = 'SbNome')
      or  (Compo.Name = 'SbQuery')
      then begin
        if (Compo is TBitBtn) then
          TBitBtn(Compo).Enabled := True
        else
        if (Compo is TSpeedButton) then
          TSpeedButton(Compo).Enabled := True;
      end;
    end;
    Result := True;
    if ShowSQLText then
      GOTOm.LeMeuSQL_Fixo_m(QrExec, '', nil, True, True);
  finally
    if not Result then
      GOTOm.LeMeuSQL_Fixo_m(QrExec, '', nil, True, True);
  end;
end;

{
function TUMemModule.BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUMemModule.BuscaEmLivreY_Def_Geral(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'ctrlgeral',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUMemModule.BuscaEmLivreY_Def_SPED_EFD(Table, Field: String; Acao: TSQLType;
  Atual: Integer; EdControle: TdmkEdit = nil; Forca_1: Boolean = True): Integer;
begin
  case Acao of
    stUpd: Result := Atual;
    stIns: Result := BuscaEmLivreY(Dmod.MyDB, 'livres', 'spedefdctrl',
      lowercase(Table), lowercase(Table), Field, Forca_1);
    else Result := 0;
  end;
  if EdControle <> nil then
    EdControle.ValueVariant := Result;
end;

function TUMemModule.BuscaEmLivreY_Def_Old(Table, Field: String; Acao: String;
  Atual: Integer): Integer;
begin
  if Acao = CO_ALTERACAO then Result := Atual else
  if Acao = CO_INCLUSAO then Result :=
    BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle', lowercase(Table), lowercase(Table), Field)
  else Result := 0;
end;

function TUMemModule.SituacaoLancamento(QrLct: TABSQuery): String;
begin
  //if QrLct.FieldByName('Sit.Value = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if QrLct.FieldByName('Sit').AsInteger = -1 then
     Result := CO_IMPORTACAO
  else
    if QrLct.FieldByName('Sit').AsInteger = 1 then
     Result := CO_PAGTOPARCIAL
  else
    if QrLct.FieldByName('Sit').AsInteger = 2 then
       Result := CO_QUITADA
  else if QrLct.FieldByName('Sit').AsInteger = 3 then
  begin
    // Caixa com prazo
    if (QrLct.FieldByName('Carteira').AsInteger = 0) and
       (QrLct.FieldByName('Tipo').AsInteger = 0) then
    begin
      if QrLct.FieldByName('Vencimento').AsDateTime < Date then
      Result := CO_QUIT_AUTOM
      else
      Result := CO_PREDATADO;
    end else Result := CO_COMPENSADA;
  end else
    if QrLct.FieldByName('Vencimento').AsDateTime < Date then
       Result := CO_VENCIDA
  else
       Result := CO_EMABERTO;
end;

procedure TUMemModule.UpdUnlockA(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdX.Close;
  QvUpdX.DataBase := VAR_GOTOM y S Q LDBNAME;
  QvUpdX.SQL.Clear;
  QvUpdX.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdX.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdX.Params[0].AsString := Registro;
  QvUpdX.ExecSQL;
end;

procedure TUMemModule.UpdLockA(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdX.Close;
  QvUpdX.DataBase := VAR_GOTOM y S Q LDBNAME;
  QvUpdX.SQL.Clear;
  QvUpdX.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdX.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdX.Params[0].AsInteger := VAR_USUARIO;
  QvUpdX.Params[1].AsString := Registro;
  QvUpdX.ExecSQL;
end;

function TUMemModule.SelLockA(Registro : String; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelX.Close;
  QvSelX.DataBase := VAR_GOTOM y S Q LDBNAME;
  QvSelX.SQL.Clear;
  QvSelX.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelX.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelX.Params[0].AsString := Registro;
  QvSelX.Open;
  if (QvSelX.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
    QvUser.Params[0].AsInteger := QvSelX.FieldByName('Lk').AsInteger;
    QvUser.Open;
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;

    if (QvSelX.FieldByName('Lk').AsInteger = VAR_USUARIO) then
    begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_21+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end else if (VAR_SENHA = VAR_BOSS) then
    begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_1+Usuario+CO_PONTO+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end else
    begin
      ShowMessage(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_1+Chr(13)+Chr(10)+Usuario+CO_PONTO);
      QvUser.Close;
    end;
  end
  else
    Result := False;
  QvSelX.Close;
end;

function TUMemModule.IncrementaControle(Database:TM y S Q LDatabase; TabControle,
 FieldControl: String) : Integer;
begin
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.Close;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT '+FieldControl+' Codigo FROM '+lowercase(tabcontrole));
  QvLivreY.Open;
  Result := QvLivreY.FieldByName('Codigo').AsInteger + 1;
 //
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.Close;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FieldControl+'=:P0');
  QvUpdY.Params[0].AsInteger := Result;
  QvUpdY.ExecSQL;
end;

function TUMemModule.BuscaEmLivreY(Database: TM y S Q LDatabase;
  TabLivre, TabControle, Table, FieldControl, FieldTable : String;
  Forca_1: Boolean = True) : Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
  QvLivreY.SQL.Add('ORDER BY Codigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  QvLivreY.Open;
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
    QvUpdY.SQL.Add('AND Codigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsInteger := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
    QvLivreY.Params[0].AsInteger := Codigo;
    QvLivreY.Open;
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      QvLivreY.Open;
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
      QvLivreY.Open;
      if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
      //
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsInteger := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    QvLivreY.Open;
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
    QvLivreY.Open;
    if (QvLivreY.FieldByName('Codigo').AsString = '') and Forca_1 then
    begin
      if Codigo = 0 then
        Codigo := 1;
    end else if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
    //
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsInteger := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUMemModule.BuscaEmLivreY_MinMax(Database: TM y S Q LDatabase;
  TabLivre, TabControle, Table, FieldControl, FieldTable : String; Minimo,
  Maximo: Integer) : Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  Codigo := 0;
  QvUpdY.Close;
  QvUpdY.Database := Database;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;
  //
  if Minimo = 0 then
  begin
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
    QvUpdY.ExecSQL;
    QvLivreY.Close;
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
    QvLivreY.SQL.Add('ORDER BY Codigo');
    QvLivreY.Params[0].AsString  := lowercase(Table);
    QvLivreY.Open;
    if (QvLivreY.FieldByName('Codigo').AsInteger > 0) and (Minimo = 0) then
    begin
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
      QvUpdY.SQL.Add('AND Codigo=:P1');
      QvUpdY.Params[0].AsString := lowercase(Table);
      QvUpdY.Params[1].AsInteger := Codigo;
      QvUpdY.ExecSQL;
      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
      QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
      QvLivreY.Params[0].AsInteger := Codigo;
      QvLivreY.Open;
      if QvLivreY.RecordCount > 0 then
      begin
        QvLivreY.Close;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
        QvLivreY.Open;
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

        //Novo// Evitar duplica��o
        QvLivreY.Database := Database;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
        QvLivreY.Open;
        if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
          Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
        //
        QvUpdY.Close;
        QvUpdY.SQL.Clear;
        QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
        QvUpdY.Params[0].AsInteger := Codigo;
        QvUpdY.ExecSQL;
      end;
    end;
  end
  else
  begin
    //Novo// Evitar duplica��o
    QvLivreY.Database := Database;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add('WHERE '+FieldTable+' BETWEEN :MINIMO AND :MAXIMO');
    QvLivreY.Params[0].AsInteger := Minimo;
    QvLivreY.Params[1].AsInteger := Maximo-1;
    QvLivreY.Open;
    if QvLivreY.FieldByName('Codigo').AsString = '' then Codigo := Minimo
    else if QvLivreY.FieldByName('Codigo').AsInteger >= Minimo then
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
    if Codigo > Maximo then Codigo := Maximo;
    //
  end;
  QvLivreY.Close;


  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUMemModule.BuscaIntSafe(Database: TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Integer;
var
  Codigo : Integer;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  //Novo// Evitar duas chamadas conjuntas
  QvLivreY.Close;
  QvLivreY.Database := Database;

  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  (*QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('LOCK TABLES '+lowercase(TabControle) + ' READ, ' + lowercase(TabLivre) + ' READ');
  QvLivreY.ExecSQL;*)
  try
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
    QvUpdY.ExecSQL;
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela=:Table');
    QvLivreY.SQL.Add('ORDER BY Codigo');
    QvLivreY.Params[0].AsString := lowercase(Table);
    QvLivreY.Open;
    if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
    begin
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Tabela=:P0');
      QvUpdY.SQL.Add('AND Codigo=:P1');
      QvUpdY.Params[0].AsString := lowercase(Table);
      QvUpdY.Params[1].AsInteger := Codigo;
      QvUpdY.ExecSQL;
      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
      QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
      QvLivreY.Params[0].AsInteger := Codigo;
      QvLivreY.Open;
      if QvLivreY.RecordCount > 0 then
      begin
        //
        QvLivreY.Close;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
        QvLivreY.Open;
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

        //Evitar duplica��o
        QvLivreY.Database := Database;
        QvLivreY.SQL.Clear;
        QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
        QvLivreY.Open;
        if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
          Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
        //
        QvUpdY.Close;
        QvUpdY.SQL.Clear;
        QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
        QvUpdY.Params[0].AsInteger := Codigo;
        QvUpdY.ExecSQL;
      end;
    end
    else
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      QvLivreY.Open;
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      //Novo// Evitar duplica��o
      QvLivreY.Database := Database;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT MAX('+FieldTable+') Codigo FROM '+lowercase(table)+'');
      QvLivreY.Open;
      if QvLivreY.FieldByName('Codigo').AsString = '' then Codigo := 1
      else if QvLivreY.FieldByName('Codigo').AsInteger >= Codigo then
        Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;
      //
      QvUpdY.Close;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsInteger := Codigo;
      QvUpdY.ExecSQL;
    end;
    QvLivreY.Close;
  finally
    //Novo// Evitar duas chamadas conjuntas
    (*QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('UNLOCK TABLES');
    QvLivreY.ExecSQL;*)
  end;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUMemModule.AtualizaLaRegistro(Database: TM y S Q LDatabase; Tabela : String;
 RecCount, Increm : Integer) : String;
var
  Total : Integer;
begin
  QvRecCountY.SQL.Clear;
  QvRecCountY.Database := Database;
  QvRecCountY.SQL.Add('SELECT COUNT(*) Record From '+lowercase(Tabela)+'');
  QvRecCountY.Open;
  Total := QvRecCountY.FieldByName('Record').AsInteger;
  QvRecCountY.Close;
  case Increm of
    -2 : VAR_RECNO := 0;
     0 : if Total > 0 then VAR_RECNO := 1 else VAR_RECNO := 0;
     2 : VAR_RECNO := Total;
     else VAR_RECNO := VAR_RECNO + Increm;
  end;
  if VAR_RECNO > RecCount then VAR_RECNO := RecCount;
  if VAR_RECNO < 0 then VAR_RECNO := 0;
  if (RecCount > 0) and (VAR_RECNO = 0) then VAR_RECNO := 1;
  Result := IntToStr(VAR_RECNO)+CO_DE+IntToStr(RecCount)+CO_DE+IntToStr(Total);
end;

procedure TUMemModule.UpdLockY(Registro : Integer; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsInteger := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdLockY_Text(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsString := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdLockY_Double(Registro : Double; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdLockInt64Y(Registro : Int64; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.Params[1].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdUnlockY(Registro : Integer; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsInteger := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdUnlockY_Text(Registro : String; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsString := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdUnlockY_Double(Registro : Double; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdUnlockInt64Y(Registro : Int64; Database: TM y S Q LDatabase; Table, Field : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvUpdY.Params[0].AsFloat := Registro;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdLockTudoY(Database: TM y S Q LDatabase; Table : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=:Usuario');
  QvUpdY.Params[0].AsInteger := VAR_USUARIO;
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.UpdUnlockTudoW(Database: TM y S Q LDatabase; Table : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(table)+' SET Lk=0');
  QvUpdY.ExecSQL;
end;

procedure TUMemModule.PoeEmLivreY(Database: TM y S Q LDatabase; TabLivre, Table : String; Codigo : Integer);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('INSERT INTO '+lowercase(tablivre)+' SET Codigo=:P0, Tabela=:P1');
  QvUpdY.Params[0].AsInteger := Codigo;
  QvUpdY.Params[1].AsString := lowercase(Table);
  QvUpdY.ExecSQL;
end;

(*procedure TUMemModule.PoeEmLivreY_Text(Database: TM y S Q LDatabase; TabLivre, Table : String; Codigo : String);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('INSERT INTO '+lowercase(tablivre)+' SET Codigo=:P0, Tabela=:P1');
  QvUpdY.Params[0].AsString := Codigo;
  QvUpdY.Params[1].AsString := lowercase(Table);
  QvUpdY.ExecSQL;
end;*)

function TUMemModule.SelLockY(Registro : Integer; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsInteger := Registro;
  QvSelY.Open;
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    QvUser.Open;
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;

    if (QvSelY.FieldByName('Lk').AsInteger = VAR_USUARIO) then
    begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_21+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end else begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_1+Usuario+CO_PONTO+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUMemModule.SelLockY_Text(Registro : String; Database: TM y S Q LDatabase; Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsString := Registro;
  QvSelY.Open;
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    QvUser.Open;
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;

    if (QvSelY.FieldByName('Lk').AsInteger = VAR_USUARIO) then
    begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_21+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end else begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_1+Usuario+CO_PONTO+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUMemModule.SelLockY_Double(Registro : Double; Database: TM y S Q LDatabase;
  Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsFloat := Registro;
  QvSelY.Open;
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    QvUser.Open;
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;
    if (QvSelY.FieldByName('Lk').AsInteger <> VAR_USUARIO) then
    begin
      Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_1+Chr(13)+Chr(10)+Usuario+CO_PONTO, 'Erro', MB_OK+MB_ICONERROR);
      QvUser.Close;
    end
    else
    begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_21+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUMemModule.SelLockInt64Y(Registro : Int64; Database: TM y S Q LDatabase;
  Table, Field : String) : Boolean;
var
  Usuario : String;
begin
  QvSelY.Close;
  QvSelY.Database := Database;;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE '+Field+'=:Codigo');
  QvSelY.Params[0].AsFloat := Registro;
  QvSelY.Open;
  if (QvSelY.FieldByName('Lk').AsInteger <> 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    QvUser.Close;
    QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
    QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
    QvUser.Open;
    if QvUser.RecordCount > 0 then
       Usuario := QvUser.FieldByName('Login').AsString;
    if (QvSelY.FieldByName('Lk').AsInteger <> VAR_USUARIO) then
    begin
      Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_1+Chr(13)+Chr(10)+Usuario+CO_PONTO, 'Erro', MB_OK+MB_ICONERROR);
      QvUser.Close;
    end
    else
    begin
      if Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO1+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO3_21+Chr(13)+Chr(10)+FIN_MSG_REGISTROTRAVADO3_22,
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
      MB_APPLMODAL) = ID_YES then Result := False;
    end;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUMemModule.SelLockQualquerY(Database: TM y S Q LDatabase; Table, NomeTable : String) : Boolean;
var
  Usuario : String;
  Usuario2 : Integer;
  Mensagem: PChar;
begin
  Mensagem := PChar('???');
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT Lk FROM '+lowercase(table)+'');
  QvSelY.SQL.Add('WHERE Lk <> 0');
  QvSelY.SQL.Add('ORDER BY Lk');
  QvSelY.Open;
  if (QvSelY.RecordCount > 0) then
  begin
    Usuario := CO_DESCONHECIDO;
    Result := True;
    if (QvSelY.RecordCount = 1) then
    begin
      QvUser.Close;
      QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
      QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
      QvUser.Open;
      if QvUser.RecordCount > 0 then
         Usuario := QvUser.FieldByName('Login').AsString;
      Geral.MensagemBox(FIN_MSG_REGISTROTRAVADO4_1+NomeTable+Chr(13)+
      Chr(10)+FIN_MSG_REGISTROTRAVADO4_2+Usuario+CO_PONTO,
      'Erro', MB_OK+MB_ICONERROR);
    end
    else
    begin
      Mensagem := PChar(FIN_MSG_REGISTROTRAVADO5_1+
      IntToStr(QvSelY.RecordCount)+
      FIN_MSG_REGISTROTRAVADO5_2+NomeTable+Chr(13)+Chr(10)+
      FIN_MSG_REGISTROTRAVADO5_3);
      QvSelY.First;
      Usuario2 := 0;
      while not QvSelY.Eof do
      begin
        if QvSelY.FieldByName('Lk').AsInteger <> Usuario2 then
        begin
          Usuario2 := QvSelY.FieldByName('Lk').AsInteger;
          QvUser.Close;
          QvUser.DataBase := VAR_GOTOM y S Q LDBNAME;
          QvUser.Params[0].AsInteger := QvSelY.FieldByName('Lk').AsInteger;
          QvUser.Open;
          if QvUser.RecordCount > 0 then
          Mensagem := PChar(Mensagem+
          Chr(13)+Chr(10)+QvUser.FieldByName('Login').AsString);
        end;
        QvSelY.Next;
      end;
    end;
    Geral.MensagemBox(Mensagem, 'Erro', MB_OK+MB_ICONERROR);
    QvUser.Close;
  end
  else
    Result := False;
  QvSelY.Close;
end;

function TUMemModule.BuscaEmLivreY_Double(Database: TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Double;
var
  Codigo : Double;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE (Codigo < 1) and (BigCodigo < 1)');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT BigCodigo Codigo FROM '+lowercase(tablivre)+' WHERE BigTabela=:Table');
  QvLivreY.SQL.Add('ORDER BY BigCodigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  QvLivreY.Open;
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE BigTabela=:P0');
    QvUpdY.SQL.Add('AND BigCodigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsFloat := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
//    QvLivreM.Params[0].AsString := lowercase(Table);
    QvLivreY.Params[0].AsFloat := Codigo;
    QvLivreY.Open;
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      QvLivreY.Open;
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsFloat := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    QvLivreY.Open;
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsFloat := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;

  Result := Codigo;
end;

function TUMemModule.BuscaEmLivreInt64Y(Database: TM y S Q LDatabase; TabLivre, TabControle, Table, FieldControl, FieldTable : String) : Int64;
var
  Codigo : Int64;
  FldCtrl: String;
begin
  if FieldControl = VAR_LCT then
    FldCtrl := LAN_CTOS
  else
    FldCtrl := FieldControl;

  //

  QvUpdY.Close;
  QvUpdY.Database := Database;

  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('LOCK TABLES ' + lowercase(TabLivre) + ' WRITE, ' +
    lowercase(TabControle) + ' WRITE, '+lowercase(Table)+' WRITE;');
  QvUpdY.ExecSQL;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE (Codigo < 1) and (BigCodigo < 1)');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT BigCodigo Codigo FROM '+lowercase(tablivre)+' WHERE BigTabela=:Table');
  QvLivreY.SQL.Add('ORDER BY BigCodigo');
  QvLivreY.Params[0].AsString := lowercase(Table);
  QvLivreY.Open;
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then
  begin
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger;
    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE BigTabela=:P0');
    QvUpdY.SQL.Add('AND BigCodigo=:P1');
    QvUpdY.Params[0].AsString := lowercase(Table);
    QvUpdY.Params[1].AsFloat := Codigo;
    QvUpdY.ExecSQL;
    //Novo// Evitar duplica��o
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FieldTable+' Codigo FROM '+lowercase(table)+'');
    QvLivreY.SQL.Add( 'WHERE '+FieldTable+'=:P0');
//    QvLivreM.Params[0].AsString := lowercase(Table);
    QvLivreY.Params[0].AsFloat := Codigo;
    QvLivreY.Open;
    if QvLivreY.RecordCount > 0 then
    begin
      QvLivreY.Close;
      QvLivreY.SQL.Clear;
      QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
      QvLivreY.Open;
      Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

      QvUpdY.Close;
      QvUpdY.Database := Database;
      QvUpdY.SQL.Clear;
      QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
      QvUpdY.Params[0].AsFloat := Codigo;
      QvUpdY.ExecSQL;
    end;
  end
  else
  begin
    QvLivreY.Close;
    QvLivreY.SQL.Clear;
    QvLivreY.SQL.Add('SELECT '+FldCtrl+' Codigo FROM '+lowercase(tabcontrole));
    QvLivreY.Open;
    Codigo := QvLivreY.FieldByName('Codigo').AsInteger + 1;

    QvUpdY.Close;
    QvUpdY.SQL.Clear;
    QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+FldCtrl+'=:P0');
    QvUpdY.Params[0].AsFloat := Codigo;
    QvUpdY.ExecSQL;
  end;
  QvLivreY.Close;
  //
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UNLOCK TABLES;');
  QvUpdY.ExecSQL;
  //
  Result := Codigo;
end;

procedure TUMemModule.PoeEmLivreY_Double(Database: TM y S Q LDatabase; TabLivre, Table : String; Codigo : Double);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('INSERT INTO '+lowercase(tablivre)+' (BigCodigo, BigTabela) VALUES (:P0, :P1)');
  QvUpdY.Params[0].AsFloat := Codigo;
  QvUpdY.Params[1].AsString := lowercase(Table);
  QvUpdY.ExecSQL;
end;

function TUMemModule.VerificaDuplicadoStr(Database: TM y S Q LDatabase; Tabela, CampoDescri, CampoCod, Descricao,
 CampoAnterior: String; Codigo : Integer) : Boolean;
var
  Duplicado : Boolean;
begin
  Duplicado := False;
  QvDuplicStrY.Close;
  QvDuplicStrY.Database := Database;
  QvDuplicStrY.SQL.Clear;
  QvDuplicStrY.SQL.Add('SELECT '+CampoDescri+' NOME, '+CampoCod+
  ' CODIGO, '+CampoAnterior+' ANTERIOR FROM '+lowercase(tabela)+'');
  QvDuplicStrY.SQL.Add('WHERE '+CampoDescri+'=:Nome');
  QvDuplicStrY.Params[0].AsString := Descricao;
  QvDuplicStrY.Open;
  if QvDuplicStrY.RecordCount > 1 then Duplicado := True;
  if (QvDuplicStrY.RecordCount = 1) and
     (QvDuplicStrY.FieldByName('Codigo').AsInteger <> Codigo) then Duplicado := True;
  if Duplicado then M L A G e r a l.Msg1(VAR_MSG_ITEMDUPLICADO, FIN_MSG_WARNING);
  if Duplicado = True then
  begin
    VAR_DUPLICCOD := QvDuplicStrY.FieldByName('Codigo').AsInteger;
    VAR_DUPLICANT := QvDuplicStrY.FieldByName('ANTERIOR').AsInteger;
  end;
  Result := Duplicado;
  QvDuplicStrY.Close;
end;
}

function TUMemModule.VerificaDuplicado1(Avisa: Boolean; Database, Tabela: String;
CamposFind: array of String; ValoresFind: array of Variant;
CamposOmit: array of String; ValuesOmit: array of Variant;
CampoRes: String; var ValueRes: Variant): Boolean;
var
  Valor: String;
  SQL: array of String;
  I, J, K: Integer;
begin
  Result := False;
  //
  K := High(CamposOmit);
  J := High(ValuesOmit);
  if K <> J then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(K + 1) + ' campos de omiss�o e ' +
    IntToStr(J + 1) + ' valores para estes campos em "VerificaDuplicado1"!' +
    #13#10 + 'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  I := High(CamposFind);
  J := High(ValoresFind);
  if I <> J then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(I + 1) + ' campos e ' +
    IntToStr(J + 1) + ' valores para estes campos em "VerificaDuplicado1"!' +
    #13#10 + 'Tabela: "' + Tabela + '"', 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  SetLength(SQL, I + K + 3);
  //
  SQL[0] := 'SELECT * FROM ' + Tabela;
  for I := Low(CamposFind) to High(CamposFind) do
  begin

    Valor := Geral.VariavelToString(ValoresFind[I]);
    if (I = Low(CamposFind)) then
      SQL[I + 1] := 'WHERE ' + CamposFind[I] + '=' + Valor
    else
      SQL[I + 1] := 'AND ' + CamposFind[I] + '=' + Valor
  end;
  //
  for K := Low(CamposOmit) to High(CamposOmit) do
  begin
    Valor := Geral.VariavelToString(ValuesOmit[K]);
    SQL[K + I + 1] := 'AND ' + CamposOmit[K] + '<>' + Valor
  end;
  //
  QvDuplicIntY.Close;
  if DataBase <> '' then
    QvDuplicIntY.DatabaseName := Database;
  AbreABSQuery1(QvDuplicIntY, SQL);
  Result := QvDuplicIntY.RecordCount > 0;
  if Result and Avisa then
    //M L A G e r a l.Msg1(VAR_MSG_ITEMDUPLICADO, FIN_MSG_WARNING);
    Geral.MensagemBox(VAR_MSG_ITEMDUPLICADO, FIN_MSG_WARNING, MB_OK+MB_ICONWARNING);
  if CampoRes <> '' then
    ValueRes := QvDuplicIntY.FieldByName(CampoRes).AsVariant
  else
    ValueRes := Null;
  QvDuplicIntY.Close;
end;

{
procedure TUMemModule.UpdateControleData(Database: TM y S Q LDatabase; TabControle, Field : String; Data : TDate);
begin
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add('UPDATE '+lowercase(tabcontrole)+' SET '+Field+'=:P0');
  QvUpdY.Params[0].AsDate := Data;
  QvUpdY.ExecSQL;
end;

function TUMemModule.NegaInclusaoY(Database: TM y S Q LDatabase; Tabela, Campo, TabLivre: String;
 Maximo: Integer) : Boolean;
begin
  Result := True;
  //
  QvRecCountY.Close;
  QvRecCountY.SQL.Clear;
  QvRecCountY.Database := Database;
  QvRecCountY.SQL.Add('SELECT '+Campo+' Record FROM '+lowercase(tabela));
  QvRecCountY.Open;
  if QvRecCountY.FieldByName('Record').AsInteger < Maximo then
  begin
    Result := False;
    QvRecCountY.Close;
    Exit;
  end;
  //
  QvUpdY.Close;
  QvUpdY.Database := Database;
  QvUpdY.SQL.Clear;
  QvUpdY.SQL.Add(DELETE_FROM+lowercase(tablivre)+' WHERE Codigo < 1');
  QvUpdY.ExecSQL;
  QvLivreY.Close;
  QvLivreY.Database := Database;
  QvLivreY.SQL.Clear;
  QvLivreY.SQL.Add('SELECT Codigo FROM '+lowercase(tablivre)+' WHERE Tabela="'+Campo+'"');
  QvLivreY.SQL.Add('ORDER BY Codigo');
  QvLivreY.Open;
  if QvLivreY.FieldByName('Codigo').AsInteger > 0 then Result := False;
  QvLivreY.Close;
end;

procedure TUMemModule.LogIns(Database: TM y S Q LDataBase; Tipo, ID: Integer);

(*Tipo:
  1 - Pesagem de Insumos qu�micos
  2 - Altera��o de pesagem de PQ
  3 - Transferencia em Money
*)
begin
  QvInsLogY.DataBase := Database;
  QvInsLogY.Params[0].AsInteger := Tipo;
  QvInsLogY.Params[1].AsInteger := VAR_USUARIO;
  QvInsLogY.Params[2].AsInteger := ID;
  QvInsLogY.ExecSQL;
  //dbiSaveChanges(QvInsLogY.Handle);
end;

procedure TUMemModule.LogDel(Database: TM y S Q LDataBase; Tipo, ID: Integer);
begin
  QvDelLogY.DataBase := DataBase;
  QvDelLogY.Params[0].AsInteger := Tipo;
  QvDelLogY.Params[1].AsInteger := VAR_USUARIO;
  QvDelLogY.Params[2].AsInteger := ID;
  QvDelLogY.ExecSQL;
  //dbiSaveChanges(QvDelLogY.Handle);
end;

procedure TUMemModule.OcultaEdicaoTb(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := True else
    if Form.Components[i] is TPanel then
    begin
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
        TPanel(Form.Components[i]).Enabled := False else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
        TPanel(Form.Components[i]).Visible := False else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
        TPanel(Form.Components[i]).Visible := True;
    end;
  end;
end;

function TUMemModule.MontaOrdemSQL(Pertence: array of Boolean;
  Campo: array of String): String;
var
  I, J: Integer;
begin
  Result := '';
  I := High(Pertence);
  J := High(Campo);
  if I <> I then
  begin
    Geral.MensagemBox('ERRO! Existem ' + IntToStr(J+1) + ' campos e ' +
    IntToStr(I+1) + ' indica��es de ordena��o em  "MontaOrdemSQL"!',
    'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  for I := 0 to J do
  begin
    if Pertence[I] = True then
      Result := Result + Campo[I] + ', ';
  end;
  J := Length(Result);
  if J > 0 then
    Result := 'ORDER BY ' + Copy(Result, 1, J - 2);
end;

procedure TUMemModule.MostraEdicaoTb(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := False else
    if Form.Components[i] is TPanel then
    begin
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
        TPanel(Form.Components[i]).Enabled := True else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
        TPanel(Form.Components[i]).Visible := True else
      if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
        TPanel(Form.Components[i]).Visible := False;
    end;
  end;
end;

function TUMemModule.MudaOrdemRegistroAtual(Tabela, FldIndice, FldOrdem: String;
  QueryAReordenar, QrUpd: TABSQuery): Boolean;
  procedure AtualizaOrdemDoItem(Ordem, Indice: Integer);
  begin
    UMyMod.SQLInsUpd(QrUpd, stUpd, Lowercase(Tabela), False, [
    FldOrdem], [FldIndice], [Ordem], [Indice], False);
  end;
var
  Ordem: Double;
  I, N, K, Indice: Integer;
begin
  Result := False;
  Indice := QueryAReordenar.FieldByName(FldIndice).AsInteger;
  if dmkPF.ObtemValorDouble(Ordem, 0) then
  begin
    N := Trunc(Ordem);
    if (N < 1) or (N > QueryAReordenar.RecordCount) then
    begin
      Geral.MensagemBox('A ordem ' + IntToStr(N) + ' n�o existe!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    I := 0;
    K := QueryAReordenar.FieldByName(FldIndice).AsInteger;
    //
    AtualizaOrdemDoItem(N, K);
    //
    QueryAReordenar.First;
    while not QueryAReordenar.Eof do
    begin
      if K <> QueryAReordenar.FieldByName(FldIndice).AsInteger then
      begin
        I := I + 1;
        if (I = N) then
          I := I + 1;
        //
        AtualizaOrdemDoItem(I, QueryAReordenar.FieldByName(FldIndice).AsInteger);
      end;
      QueryAReordenar.Next;
    end;
    //
    QueryAReordenar.Close;
    QueryAReordenar.Open;

    if QueryAReordenar.Locate(FldIndice, Indice, []) then
      Result := QueryAReordenar.FieldByName(FldOrdem).AsInteger = N;
  end;
end;

procedure TUMemModule.IncluiRegistroTb(Form: TForm; Tabela: TM y S Q LTable;
  ComponentToFocus: TWinControl; LaTipo: TdmkLabel);
begin
  Screen.Cursor := crHourGlass;
  MostraEdicaoTb(Form);
  Tabela.Insert;
  if ComponentToFocus <> nil then
  try
    ComponentToFocus.SetFocus;
  except
  end;
  if LaTipo <> nil then LaTipo.SQLType := stIns;
  Screen.Cursor := crDefault;
end;

procedure TUMemModule.AlteraRegistroTb(Form: TForm; Tabela: TM y S Q LTable;
  ComponentToFocus: TWinControl; LaTipo: TdmkLabel);
begin
  Screen.Cursor := crHourGlass;
  try
    MostraEdicaoTb(Form);
    if LaTipo <> nil then LaTipo.SQLType := stUpd;
    Tabela.Edit;
    if ComponentToFocus <> nil then
      ComponentToFocus.SetFocus;
  finally
    Screen.Cursor := crDefault;
  end;  
end;

function TUMemModule.VerificaCamposObrigatorios(Tabela: TM y S Q LTable): Boolean;
var
  i: integer;
begin
  Result := True;
  if not (Tabela.State in ([dsInsert, dsEdit])) then exit;
  for i := 0 to Tabela.FieldCount - 1 do
  begin
    if Uppercase(Tabela.Fields[i].FieldName) = 'NOME'
      then if trim(Tabela.FieldByName('Nome').AsString)= '' then
      begin
        Geral.MensagemBox('Informa um nome!', 'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Result := False;
      end;
  end;
end;

procedure TUMemModule.DadosAutomaticosTb(Tabela: TM y S Q LTable);
begin
  if Tabela.State = dsInsert then
  begin
    Tabela.FieldByName('DataCad').AsDateTime := Date;//FormatDateTime(VAR_FORMATDATE, Date);
    Tabela.FieldByName('UserCad').AsInteger  := VAR_USUARIO;
  end else if Tabela.State = dsEdit then
  begin
    Tabela.FieldByName('DataAlt').AsDateTime := Date;//FormatDateTime(VAR_FORMATDATE, Date);
    Tabela.FieldByName('UserAlt').AsInteger  := VAR_USUARIO;
  end;
end;

procedure TUMemModule.ConfirmaRegistroTb_Codigo(Form: TForm; Tabela: TM y S Q LTable);
begin
  Screen.Cursor := crHourGlass;
  if not VerificaCamposObrigatorios(Tabela) then Exit;
  DadosAutomaticosTb(Tabela);
  if Tabela.State = dsInsert then
  begin
    Tabela.FieldByName('Codigo').AsInteger := BuscaEmLivreY(VAR_GOTOM y S Q LDBNAME, 'livres',
    'controle', Tabela.TableName, Tabela.TableName, 'Codigo');
  end;
  OcultaEdicaoTb(Form);
  Tabela.Post;
  Screen.Cursor := crDefault;
end;

procedure TUMemModule.ConfirmaRegistroTb_Numero(Form: TForm; Tabela: TM y S Q LTable);
var
  i: integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if not (Tabela.State in ([dsInsert, dsEdit])) then Tabela.Edit;
    if not VerificaCamposObrigatorios(Tabela) then Exit;
    DadosAutomaticosTb(Tabela);
    if Tabela.State = dsInsert then
    begin
      if Tabela.FieldByName('Numero').AsInteger = 0 then
      Tabela.FieldByName('Numero').AsInteger := BuscaEmLivreY(VAR_GOTOM y S Q LDBNAME, 'livres',
      'controle', Tabela.TableName, Tabela.TableName, 'Numero');
    end;
    Tabela.Post;
    for i := 0 to Form.ComponentCount -1 do
    begin
      if Form.Components[i] is TDBGrid then TDBGrid(Form.Components[i]).ReadOnly := True else
      if Form.Components[i] is TdmkLabel then
      begin
        if Uppercase(TdmkLabel(Form.Components[i]).Name) = 'LATIPO' then
          TdmkLabel(Form.Components[i]).SQLType := stLok;
      end else
      if Form.Components[i] is TLabel then
      begin
        if Uppercase(TLabel(Form.Components[i]).Name) = 'LATIPO' then
          TLabel(Form.Components[i]).Caption := CO_TRAVADO;
      end else
      if Form.Components[i] is TPanel then
      begin
        if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELDADOS' then
          TPanel(Form.Components[i]).Enabled := False else
        if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONFIRMA' then
          TPanel(Form.Components[i]).Visible := False else
        if Uppercase(TPanel(Form.Components[i]).Name) = 'PAINELCONTROLE' then
          TPanel(Form.Components[i]).Visible := True;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUMemModule.DefineDataBase(MeuDB: String): Boolean;
var
  I, Conta: Integer;
begin
  Result := False;
  TMeuDB := '';
  Conta := Geral.ReadAppKeyCU('Count', Application.Title+'\Databases', ktInteger, 0);
  if Conta > 0 then
  begin
    if MyObjects.FormCria(TFmSelDBs, FmSelDBs) then
    begin
      FmSelDBs.RGDBs.Items.Clear;
      FmSelDBs.RGDBs.Items.Add(MeuDB);
      //
      for I := 1 to Conta do
        FmSelDBs.RGDBs.Items.Add(Geral.ReadAppKeyCU(FormatFloat('0', I),
        Application.Title+'\Databases', ktString, MeuDB));
      FmSelDBs.ShowModal;
      FmSelDBs.Destroy;
      //
      Result := TMeuDB <> '';
      if not Result then
        Halt(0);
    end;
  end else TMeuDB := MeuDB;
end;

procedure TUMemModule.DesisteRegistroTb(Form: TForm; Tabela: TM y S Q LTable;
  NomeTabela, NomeCampoCodigo: String;
  SQLType: TSQLType; CodigoALiberar: Integer);
begin
  Screen.Cursor := crHourGlass;
  OcultaEdicaoTb(Form);
  Tabela.Cancel;
  if SQLType= stIns then
    PoeEmLivreY(Dmod.MyDB, 'livres', NomeTabela, CodigoALiberar)
  else if SQLType = stUpd then
    UpdUnlockY(CodigoALiberar, Dmod.MyDB, NomeTabela, NomeCampoCodigo);
  Screen.Cursor := crDefault;
end;

procedure TUMemModule.BELY_Tb(Tabela: TM y S Q LTable; Campo: TBELY);
var
  Atual: Double;
begin
  Screen.Cursor := crHourGlass;
  case Campo of
    belyCodigo   : Atual := Tabela.FieldByName('Codigo').AsInteger;
    belyControle : Atual := Tabela.FieldByName('controle').AsInteger;
    belyNumero   : Atual := Tabela.FieldByName('Numero').AsInteger;
    else Atual   := 0;
  end;
  if Atual < 0.1 then
  begin
    case Campo of
      belyCodigo   : Tabela.FieldByName('Codigo').AsInteger := BuscaEmLivreY(VAR_GOTOM y S Q LDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'Codigo');
      belyControle : Tabela.FieldByName('controle').AsInteger := BuscaEmLivreY(VAR_GOTOM y S Q LDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'controle');
      belyNumero   : Tabela.FieldByName('Numero').AsInteger := BuscaEmLivreY(VAR_GOTOM y S Q LDBNAME, 'livres', 'controle', Tabela.TableName, Tabela.TableName, 'Numero');
      else begin
        Geral.MensagemBox('ERRO. Campo desconhecido na procedure "BELY"',
        'Erro', MB_OK+MB_ICONERROR);
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TUMemModule.DiaInutil(Data : TDateTime): Integer;
var
  DiaSemana : Integer;
  Util: Integer;
begin
  DiaSemana := DayOfWeek(Data);
  case DiaSemana of
    1: Util := 201;// Sabado
    7: Util := 207;//Domingo
    else Util := 0;
  end;
  if Util = 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM feriados WHERE Data=:P0');
    Dmod.QrAux.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, Data);
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then Util := 208; // Feriado
  end;
  Result := Util;
  (*
  Resultados:
  0: Dia �til
  201: S�bado
  207: Domingo
  208: Feriado cadastrado na tabela M y S Q L Feriados
  *)
end;

function TUMemModule.CalculaDiasArray_Pula(Data, Vcto: TDateTime; DMai, Comp:Integer):
  MyArrayI1k;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  Mudou: Boolean;
begin
  for i := 0 to HIGH(Result) do
    Result[i] := 0;
  Przo := Trunc(Vcto - Data);
  for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  Mudou := False;
  if (Comp>0) or (DMai>0) then
  begin
    while Util <> 0 do
    begin
      Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      Mudou := True;
    end;
    if Mudou then Przo := Przo - 1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      Result[Przo] := 101;
    end;
    //
    for i := 1 to DMai do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      Result[Przo] := 102;
    end;
  end;
  //
  //Mudou := False;
  while Util <> 0 do
  begin
    Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
    //Mudou := True;
  end;
  //if Mudou then Przo := Przo - 1;
  Result[0] := Przo;
end;

function TUMemModule.CalculaDiasArray_Normal(Data, Vcto: TDateTime; DMai, Comp:Integer):
  MyArrayI1k;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  for i := 0 to HIGH(Result) do
    Result[i] := 0;
  Przo := Trunc(Vcto - Data);
  for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  if (Comp>0) or (DMai>0) then
  begin
    while Util <> 0 do
    begin
      Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      //Mudou := True;
    end;
    //if Mudou then Przo := Przo -1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      Result[Przo] := 101;
    end;
  end;
  while Util <> 0 do
  begin
    Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result[0] := Przo+DMai;
end;

function TUMemModule.CalculaDiasArray_NaoPula(Data, Vcto: TDateTime; DMai, Comp:Integer):
  MyArrayI1k;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  for i := 0 to HIGH(Result) do
    Result[i] := 0;
  Przo := Trunc(Vcto - Data) + Comp + DMai;
  for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  while Util <> 0 do
  begin
    Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result[0] := Przo;
end;

function TUMemModule.CalculaDias_Pula(Data, Vcto: TDateTime; DMai, Comp:Integer): Integer;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  Mudou: Boolean;
begin
  //for i := 0 to HIGH(Result) do
    //Result(*[i]*) := 0;
  Przo := Trunc(Vcto - Data);
  //for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  Mudou := False;
  if (Comp>0) or (DMai>0) then
  begin
    while Util <> 0 do
    begin
      //Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      Mudou := True;
    end;
    if Mudou then Przo := Przo - 1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        //Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      //Result[Przo] := 101;
    end;
    //
    for i := 1 to DMai do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        //Result[Przo] := Util;
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
      //
      //Result[Przo] := 102;
    end;
  end;
  //
  //Mudou := False;
  while Util <> 0 do
  begin
    //Result[Przo] := Util;
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
    //Mudou := True;
  end;
  //if Mudou then Przo := Przo - 1;
  Result(*[0]*) := Przo;
end;

function TUMemModule.CalculaDataDeposito(Vencimento: TDateTime): TDateTime;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  Util: integer;
  //Mudou: Boolean;
begin
  Real := Vencimento;
  Util := DiaInutil(Real);
  while Util <> 0 do
  begin
    Real := Real+1;
    Util := DiaInutil(Real);
  end;
  Result := Real;
end;

function TUMemModule.CalculaDias_Normal(Data, Vcto: TDateTime; DMai, Comp:Integer): Integer;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  i, Przo, Util: integer;
  //Mudou: Boolean;
begin
  //for i := 0 to HIGH(Result) do
    //Result(*[i]*) := 0;
  Przo := Trunc(Vcto - Data);
  //for i := 1 to Przo do Result[i] := 1;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  if (Comp>0) then
  begin
    while Util <> 0 do
    begin
      //Result[Przo] := Util;
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      //Mudou := True;
    end;
    //if Mudou then Przo := Przo - 1;
    for i := 1 to Comp do
    begin
      Przo := Przo + 1;
      Real := Data + Przo;
      Util := DiaInutil(Real);
      while Util <> 0 do
      begin
        Przo := Przo + 1;
        Real := Data + Przo;
        Util := DiaInutil(Real);
      end;
    end;
  end;
  //
  while Util <> 0 do
  begin
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result := Przo+DMai;
end;

function TUMemModule.CalculaDias_NaoPula(Data, Vcto: TDateTime; DMai,
  Comp:Integer): Integer;
var
  Real: TDateTime;
  //i, Dias, Przo, j, k, Total: Integer;
  Przo, Util: integer;
  //Mudou: Boolean;
begin
  //Result := 0;
  Przo := Trunc(Vcto - Data) + Comp + Dmai;
  Real := Data + Przo;
  Util := DiaInutil(Real);
  //Mudou := False;
  while Util <> 0 do
  begin
    Przo := Przo + 1;
    Real := Data + Przo;
    Util := DiaInutil(Real);
  end;
  Result := Przo;
end;

function TUMemModule.CalculaDias(Data, Vcto: TDateTime; DMai,
  Comp, Pula, CBE: Integer): Integer;
begin
  case Pula of
    0: Result := CalculaDias_Normal(Data, Vcto, DMai, Comp);
    1: Result := CalculaDias_NaoPula(Data, Vcto, DMai, Comp);
    2: Result := CalculaDias_Pula(Data, Vcto, DMai, Comp);
    else Result := 0;
  end;
  if Result < CBE then Result := CBE;
end;

function TUMemModule.CalculaDiasArray(Data, Vcto: TDateTime; DMai,
  Comp, Pula:Integer): MyArrayI1k;
begin
  if Pula = 0 then Result := CalculaDiasArray_Normal(Data, Vcto, DMai, Comp);
  if Pula = 1 then Result := CalculaDiasArray_NaoPula(Data, Vcto, DMai, Comp);
  if Pula = 2 then Result := CalculaDiasArray_Pula(Data, Vcto, DMai, Comp);
end;
}

function TUMemModule.ProximoRegistro(Query: TABSQuery; Campo: String; Atual:
Integer): Integer;
begin
  Query.Next;
  if Query.FieldByName(Campo).AsInteger = Atual then Query.Prior;
  Result := Query.FieldByName(Campo).AsInteger;
end;

{
function TUMemModule.ReabreQuery(Query: TABSQuery; Valores: array of Variant;
  Aviso: String): Boolean;
var
  I, J: Integer;
begin
  J := High(Valores);
  Result := True;
  for I := Low(Valores) to j do
  begin
    case VarType(Valores[I]) of
      varEmpty    (*= $0000;*) (* vt_empty        0 *) : Query.Params[I].AsString   := '';
      varNull     (*= $0001;*) (* vt_null         1 *) : Query.Params[I].AsString   := 'NULL';
      varSmallint (*= $0002;*) (* vt_i2           2 *) : Query.Params[I].AsInteger  := Valores[I];
      varInteger  (*= $0003;*) (* vt_i4           3 *) : Query.Params[I].AsInteger  := Valores[I];
      varSingle   (*= $0004;*) (* vt_r4           4 *) : Query.Params[I].AsFloat    := Valores[I];
      varDouble   (*= $0005;*) (* vt_r8           5 *) : Query.Params[I].AsFloat    := Valores[I];
      varCurrency (*= $0006;*) (* vt_cy           6 *) : Query.Params[I].AsFloat    := Valores[I];
      varDate     (*= $0007;*) (* vt_date         7 *) : Query.Params[I].AsDateTime := Valores[I];
      //varOleStr   (*= $0008;*) (* vt_bstr         8 *) : Query.Params[I].AsString   := Valores[I];
      //varDispatch (*= $0009;*) (* vt_dispatch     9 *) : Result := '"' + Variavel + '"';
      //varError    (*= $000A;*) (* vt_error       10 *) : Result := '"' + Variavel + '"';
      //varBoolean  (*= $000B;*) (* vt_bool        11 *) : Result := IntToStr(BoolToInt(Variavel));
      //varVariant  (*= $000C;*) (* vt_variant     12 *) : Result := '"' + Variavel + '"';
      //varUnknown  (*= $000D;*) (* vt_unknown     13 *) : Result := '"' + Variavel + '"';
    //varDecimal  (*= $000E;*) (* vt_decimal     14 *) (*UNSUPPORTED as of v6.x code base*)
    //varUndef0F  (*= $000F;*) (* undefined      15 *) (*UNSUPPORTED per Microsoft*)
      varShortInt (*= $0010;*) (* vt_i1          16 *) : Query.Params[I].AsInteger  := Valores[I];
      varByte     (*= $0011;*) (* vt_ui1         17 *) : Query.Params[I].AsInteger  := Valores[I];
      varWord     (*= $0012;*) (* vt_ui2         18 *) : Query.Params[I].AsInteger  := Valores[I];
      varLongWord (*= $0013;*) (* vt_ui4         19 *) : Query.Params[I].AsInteger  := Valores[I];
      varInt64    (*= $0014;*) (* vt_i8          20 *) : Query.Params[I].AsInteger  := Valores[I];
    //varWord64   (*= $0015;*) (* vt_ui8         21 *) (*UNSUPPORTED as of v6.x code base*)
    (*  if adding new items, update Variants' varLast, BaseTypeMap and OpTypeMap *)

      //varStrArg   (*= $0048;*) (* vt_clsid       72 *) : Result := '"' + Variavel + '"';
      varString   (*= $0100;*) (* Pascal string 256 *) (*not OLE compatible *) : Query.Params[I].AsString := Valores[I];
      //varAny      (*= $0101;*) (* Corba any     257 *) (*not OLE compatible *) : Result := '"' + Variavel + '"';
      // custom types range from $110 (272) to $7FF (2047)
      else begin
        //raise EAbort.Create('Vari�vel n�o definida!');
        Geral.MensagemBox('Vari�vel n�o definida!' + #13#10 + Aviso,
        'Aviso', MB_OK+MB_ICONWARNING);
        Result := False;
      end;
    end;
  end;
  //
  if Result then
    Result := AbreQuery(Query, Aviso);
end;

function TUMemModule.RegistrosNaTabela(Tabela, ComplSQL: String;
  Database: TM y S Q LDatabase): Integer;
const
  Campo = 'Registros';
var
  Qry: TABSQuery;
begin
  //Result := 0;
  Qry := TABSQuery.Create(Dmod);
  try
    Qry.DataBase := Database;
    //
    Qry.SQL.Add('SELECT COUNT(*) ' + Campo);
    Qry.SQL.Add('FROM ' + LowerCase(Tabela));
    Qry.SQL.Add(ComplSQL);
    Qry.Open;
    //
    Result := Qry.FieldByName(Campo).AsInteger;
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TUMemModule.RollBack(Qry: TABSQuery);
begin
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('ROLLBACK;');
  Qry.ExecSQL;
end;

function TUMemModule.ExcluiTodosRegistros(Pergunta, Tabela: String): Integer;
begin
  Result := ID_NO;
  if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MensagemBox(Pergunta, 'Pergunta de exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION);
  if Result = ID_YES then
  begin
    if Pergunta = '' then
      Result := ID_YES
    else
      Result := Geral.MensagemBox('TODOS REGISTROS da tabela "' + Tabela +
      '" ser�o exclu�dos! Confirma a exclus�o?', 'Confirma��o de exclus�o',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Result = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(DELETE_FROM + lowercase(tabela));
      Dmod.QrUpd.ExecSQL;
    end;
  end;
end;
}

function TUMemModule.ExcluiRegistroInt1(Pergunta, Tabela, Campo: String;
Inteiro1: Integer): Integer;
var
  Qry: TABSQuery;
begin
  Result := ID_NO;
  if ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MensagemBox(Pergunta, 'Pergunta de exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION);
  if Result = ID_YES then
  begin
(*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + lowercase(tabela)+
    ' WHERE '+Campo+'='+IntToStr(Inteiro1));
    Dmod.QrUpd.ExecSQL;
*)
    Qry := TABSQuery.Create(nil);
    try
      Qry.DatabaseName := 'MEMORY';
      Qry.InMemory := True;
      Qry.SQL.Clear;
      Qry.SQL.Add(DELETE_FROM + lowercase(tabela)+
      ' WHERE '+Campo+'='+IntToStr(Inteiro1));
      Qry.ExecSQL;
    finally
      Qry.Free;
    end;
  end;
end;

{
function TUMemModule.ExcluiRegistroInt2(Pergunta, Tabela, Campo1, Campo2: String;
  Inteiro1, Inteiro2: Integer): Integer;
begin
  Result := ID_NO;
  if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MensagemBox(Pergunta, 'Pergunta de exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION);
  if Result = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + lowercase(tabela)+
    ' WHERE '+Campo1+'='+IntToStr(Inteiro1) +
    ' AND '+Campo2+'='+IntToStr(Inteiro2));
    Dmod.QrUpd.ExecSQL;
  end;
end;

function TUMemModule.ExcluiRegistroTxt1(Pergunta, Tabela, Campo: String; Texto1: String):
Integer;
begin
  Result := ID_NO;
  if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  if Pergunta = '' then
    Result := ID_YES
  else
    Result := Geral.MensagemBox(Pergunta, 'Pergunta de exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION);
  if Result = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + lowercase(tabela)+
    ' WHERE '+Campo+'="'+Texto1 + '"');
    Dmod.QrUpd.ExecSQL;
  end;
end;

function TUMemModule.ObtemCamposDeTabelaIdentica(DataBase: TM y S Q LDatabase;
  Tabela, Prefix: String): String;// Prefix -> 'la.' = ref tabela
var
  j: Integer;
begin
  QvSelY.Close;
  QvSelY.Database := Database;
  QvSelY.SQL.Clear;
  QvSelY.SQL.Add('SELECT * FROM '+lowercase(tabela));
  QvSelY.SQL.Add('LIMIT 1');
  QvSelY.Open;
  //
  Result := ' '+Prefix+QvSelY.Fields[0].FieldName + Chr(13) + Chr(10);
  for j := 1 to QvSelY.Fields.Count-1 do
    Result := Result + ', '+Prefix+QvSelY.Fields[j].FieldName + Chr(13) + Chr(10);
end;

function TUMemModule.ObtemValorDoCampoXDeIndex_Int(Index: Integer;
CampoX, CampoIndex: String; Query: TABSQuery;
dmkEditCB: TdmkEditCB; dmkDBlookupComboBox: TdmkDBlookupComboBox;
var Valor: Integer): Boolean;
begin
  Result := False;
  if Query.Locate(CampoIndex, Index, []) then
  begin
    Valor := Query.FieldByName(CampoX).AsInteger;
    if dmkEditCB <> nil then
      dmkEditCB.ValueVariant := Valor;
    if dmkDBlookupComboBox <> nil then
      dmkDBlookupComboBox.KeyValue := Valor;
    Result := True;
  end else
    Valor := 0;
end;

function TUMemModule.ObtemCodigoDeCodUsu(EdCodUsu: TdmkEditCB;
  var Codigo: Integer; Aviso: String; Campo: String = 'Codigo'; CodUsu: String =
  'CodUsu'): Boolean;
var
  Query: TABSQuery;
  DB: TDBLookupComboBox;
  DS: TDataSource;
  CU: Integer;
begin
  Codigo := 0;
  Result := False;
  if EdCodUsu.ValueVariant <> Null then
  begin
    if EdCodUsu.DBLookupComboBox <> nil then
    begin
      DB := EdCodUsu.DBLookupComboBox;
      if DB <> nil then
        DS := DB.ListSource
      else
        Ds := nil;
      if DS <> nil then
      begin
        Query := TABSQuery(DS.DataSet);
        if Query <> nil then
        begin
          CU := EdCodUsu.ValueVariant;
          if Query.Locate(CodUsu, CU, []) then
          begin
            Codigo := Query.FieldByName(Campo).AsInteger;
            Result := True;
          end;
        end;
      end;
    end;
  end;
  if not Result and (Aviso <> '') then
    Geral.MensagemBox(Aviso, 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TUMemModule.ObtemProximoCHTalao(Carteira: Integer; EdSerieCH,
  EdDoc: TdmkEdit): Boolean;
var
  Qry: TABSQuery;
  //NumFim,
  Controle, NumIni,NumAtu, Taloes: Integer;
  Serie: String;
begin
  Result := False;
  Qry := TABSQuery.Create(Dmod);
  Qry.DataBase := DMod.MyDB;
  //
  Qry.SQL.Add('LOCK TABLES carttalch WRITE;');
  Qry.ExecSQL;
  Qry.SQL.Add('SELECT *');
  Qry.SQL.Add('FROM carttalch');
  Qry.SQL.Add('WHERE Codigo=' + FormatFloat('0', Carteira));
  Qry.SQL.Add('AND NumAtu < NumFim');
  Qry.SQL.Add('ORDER BY Controle');
  Qry.Open;
  Taloes := Qry.RecordCount;
  if Qry.RecordCount > 0 then
  begin
    Controle := Qry.FieldByName('Controle').AsInteger;
    NumIni   := Qry.FieldByName('NumIni').AsInteger;
    //NumFim   := Qry.FieldByName('NumFim').AsInteger;
    NumAtu   := Qry.FieldByName('NumAtu').AsInteger;
    Serie    := Qry.FieldByName('Serie').AsString;
    if NumAtu = 0 then
      NumAtu := NumIni
    else
      NumAtu := NumAtu + 1;
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('UPDATE carttalch SET NumAtu=' + FormatFloat('0', NumAtu));
    Qry.SQL.Add('WHERE controle=' + FormatFloat('0', Controle));
    Qry.ExecSQL;
    //
    EdSerieCH.ValueVariant := Serie;
    EdDoc.ValueVariant     := NumAtu;
    //
    Result := True;
  end else Geral.MensagemBox('N�o h� mais numera��o de cheque dispon�vel!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('UNLOCK TABLES;');
  Qry.ExecSQL;
  Qry.Free;
  //
  if Taloes < 2 then Geral.MensagemBox('� necess�rio solicitar mais tal�es ' +
  'de cheque para esta carteira!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TUMemModule.ObtemQtdeCHTaloes(Carteira: Integer): Integer;
var
  Qry: TABSQuery;
  //Controle,
  NumIni, NumFim, NumAtu: Integer;
  //Serie: String;
  //Cheques: Integer;
begin
  Result  := 0;
  Qry := TABSQuery.Create(Dmod);
  Qry.DataBase := DMod.MyDB;
  //
  Qry.SQL.Add('SELECT *');
  Qry.SQL.Add('FROM carttalch');
  Qry.SQL.Add('WHERE Codigo=' + FormatFloat('0', Carteira));
  Qry.SQL.Add('AND NumAtu < NumFim');
  Qry.SQL.Add('ORDER BY Controle');
  Qry.Open;
  if Qry.RecordCount > 0 then
  begin
    Qry.First;
    while not Qry.Eof do
    begin
      NumIni   := Qry.FieldByName('NumIni').AsInteger;
      NumFim   := Qry.FieldByName('NumFim').AsInteger;
      NumAtu   := Qry.FieldByName('NumAtu').AsInteger;
      if NumAtu = 0 then
        Result := Result + NumFim - NumIni + 1
      else
        Result := Result + NumFim - NumAtu;
      //
      Qry.Next;  
    end;
  end;
  Qry.Free;
end;

function TUMemModule.SenhaEspecial: Boolean;
begin
  MyObjects.FormCria(TFmSenhaEspecial, FmSenhaEspecial);
  FmSenhaEspecial.ShowModal;
  Result := FmSenhaEspecial.FStatusAcesso;
  FmSenhaEspecial.Destroy;
  //
end;

  // GetCodUsuDeCodigo
  // ObtemCodUsuDeCodigo
  // � "Seta" e n�o "Obtem" porque seta o Edit e o DBLookupComboBox
function TUMemModule.SetaCodUsuDeCodigo(EdCodUsu: TdmkEditCB;
  CBCodUsu: TdmkDBLookupComboBox; Qry: TABSQuery; Codigo: Integer;
  Campo, CodUsu: String): Boolean;
begin
  Result := False;
  Qry.Close;
  Qry.Open;
  if Qry.Locate(Campo, Codigo, []) then
  begin
    if EdCodUsu <> nil then
      EdCodUsu.ValueVariant := Qry.FieldByName(CodUsu).AsInteger;
    if CBCodUsu <> nil then
      CBCodUsu.KeyValue := Qry.FieldByName(CodUsu).AsInteger;
    //
    Result := True;
  end;
end;
}

function TUMemModule.SetaCodigoPesquisado(EdCodigo: TdmkEditCB;
  CBCodigo: TdmkDBLookupComboBox; Qry: TABSQuery; Codigo: Integer;
  Campo: String): Boolean;
begin
  Result := False;
  Qry.Close;
  Qry.Open;
  if Codigo <> 0 then
  begin
    if Qry.Locate(Campo, Codigo, []) then
    begin
      if EdCodigo <> nil then
        EdCodigo.ValueVariant := Qry.FieldByName(Campo).AsInteger;
      if CBCodigo <> nil then
        CBCodigo.KeyValue := Qry.FieldByName(Campo).AsInteger;
      //
      Result := True;
    end;
  end;
end;

{
procedure TUMemModule.Commit(Qry: TABSQuery);
begin
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('COMMIT;');
  Qry.ExecSQL;
end;

function TUMemModule.ConfigJanela(Janela: String; Lista: TConfWinControl): Boolean;
begin
  MyObjects.FormCria(TFmConfJanela, FmConfJanela);
  with FmConfJanela do
  begin
    FJanela := Janela;
    FListaCWC := Lista;
    ReopenConfJanela('');
    ShowModal;
    Destroy;
  end;
  Result := True;
end;

function TUMemModule.ConfigJanela10(Janela: String;
  c0, c1, c2, c3, c4, c5, c6, c7, c8, c9:TControl): Boolean;
var
  Lista: TConfWinControl;
  i: Integer;
begin
  Lista[00] := c0;
  Lista[01] := c1;
  Lista[02] := c2;
  Lista[03] := c3;
  Lista[04] := c4;
  Lista[05] := c5;
  Lista[06] := c6;
  Lista[07] := c7;
  Lista[08] := c8;
  Lista[09] := c9;
  for i := 10 to TMaxConfWinControl do Lista[i] := nil;
  Result := ConfigJanela(Janela, Lista);
end;

function TUMemModule.ConfigJanela20(Janela: String;
  c0, c1, c2, c3, c4, c5, c6, c7, c8, c9,
  c10, c11, c12, c13, c14, c15, c16, c17, c18, c19:TControl): Boolean;
var
  Lista: TConfWinControl;
  i: Integer;
begin
  Lista[00] := c0;
  Lista[01] := c1;
  Lista[02] := c2;
  Lista[03] := c3;
  Lista[04] := c4;
  Lista[05] := c5;
  Lista[06] := c6;
  Lista[07] := c7;
  Lista[08] := c8;
  Lista[09] := c9;
  Lista[10] := c10;
  Lista[11] := c11;
  Lista[12] := c12;
  Lista[13] := c13;
  Lista[14] := c14;
  Lista[15] := c15;
  Lista[16] := c16;
  Lista[17] := c17;
  Lista[18] := c18;
  Lista[19] := c19;
  for i := 20 to TMaxConfWinControl do Lista[i] := nil;
  Result := ConfigJanela(Janela, Lista);
end;

function TUMemModule.TravaFmEmPanelInsUpd(PanelsToHide, PanelsToShow: array of TPanel;
             LaTipo: TdmkLabel): Boolean;
var
  i, j, k: Integer;
begin
  i := Low(PanelsToHide);
  j := High(PanelsToHide);
  for k := i to j do PanelsToHide[i].Visible := False;
  //
  i := Low(PanelsToShow);
  j := High(PanelsToShow);
  for k := i to j do PanelsToShow[i].Visible := True;
  //
  if LaTipo <> nil then
    LaTipo.SQLType := stLok;
  Result := True;
end;
}

function TUMemModule.ConfigPanelInsUpd(Acao: TSQLType; Form: TForm; Panel: TPanel;
Query: TABSQuery; PanelsToHide, PanelsToShow: array of TPanel;
CompoToFocus: TWinControl; ImgTipo: TdmkImage; Tabela: String): Boolean;
  { N�o tem valor padr�o!
  function ValorPadrao(Qry: TABSQuery; Campo: String): Variant;
  var
    Tipo, DefVal: String;
    e, k: Integer;
    d: Double;
  begin
    Result := Null;
    if Qry.Locate('Field', Campo, []) then
    begin
      //try
        if VarType(Qry.FieldByName('Default').AsVariant) = varNull then
          DefVal := ''
        else
          DefVal := Qry.FieldByName('Default').AsVariant;
      //except
        //DefVal := Trim(Qry.FieldByName('Default').AsWideString);
      //end;
      if String(DefVal) <> '' then
      begin
        Tipo := Uppercase(Qry.FieldByName('Type').AsString);
        k := pos('(', Tipo);
        if k > 0 then
          Tipo := Copy(Tipo, 1, k-1);
        if (Tipo = 'TINYINT') or (Tipo = 'SMALLINT') or (Tipo = 'MEDIUMINT')
        or (Tipo = 'INT') or (Tipo = 'INTEGER') or (Tipo = 'BIGINT') then
          Result := Qry.FieldByName('Default').AsVariant
        else
        if (Tipo = 'FLOAT') or (Tipo = 'DOUBLE') or (Tipo = 'DOUBLEPRECISION')
        or (Tipo = 'REAL') or (Tipo = 'DECIMAL') or (Tipo = 'NUMERIC') then
        begin
          Val(Qry.FieldByName('Default').AsVariant, d, e);
          Result := d;
        end else
        if (Tipo = 'CHAR') or (Tipo = 'VARCHAR') or (Tipo = 'TINYBLOB')
        or (Tipo = 'BLOB') or (Tipo = 'MEDIUMBLOB') or (Tipo = 'LONGBLOB')
        or (Tipo = 'TINYTEXT') or (Tipo = 'TEXT') or (Tipo = 'MEDIUMTEXT')
        or (Tipo = 'LONGTEXT') or (Tipo = 'ENUM') or (Tipo = 'SET')
        then
          Result := Qry.FieldByName('Default').AsVariant
        else
        if (Tipo = 'DATE') or (Tipo = 'DATETIME') or (Tipo = 'TIMESTAMP')
        or (Tipo = 'TIME') or (Tipo = 'YEAR') then
          // Precisa modificar??
          Result := Qry.FieldByName('Default').AsString
        else
          Geral.MensagemBox('O campo do tipo "' + Tipo +
          '" ainda n�o implementado na function "ValorPadrao". ' +
          '   AVISE A DERMATEK!   Isto n�o � uma notofica��o de erro!',
          'Mensagem', MB_OK+MB_ICONEXCLAMATION);
      end;
    end;
  end;
  }
  procedure Mensagem(Campo: String; Objeto: TObject);
  begin
    Geral.MensagemBox('N�o foi poss�vel definir o valor ' +
    'do campo "' + Campo + '" no componente "' +
    TComponent(Objeto).Name + '"', 'Erro', MB_OK+MB_ICONERROR);
  end;
var
  i, j, k: Integer;
  c, Objeto: TComponent;
  PI_DataField, PI_DataSource, PI_QryCampo, PI_OldValor, PropInfo: PPropInfo;
  Campo, s: String;
  Valor: Variant;
  IsOk: Boolean;
  Qry: TABSQuery;
  MySou: TDataSource;
  MySet: TDataSet;
  //MyQry: TABSQuery;
  MyFld: String;
begin
  //IncluiRegistro;
  Screen.Cursor := crHourGlass;
  { N�o tem valor padr�o?
  if Acao = stIns then
  begin
    Qry:= TABSQuery.Create(Dmod);
    Qry.DatabaseName := 'MEMORY';
    Qry.InMemory := True;
    Qry.SQL.Add('SHOW  ??? ' + lowercase(Tabela));
    Qry.Open;
  end else} Qry := nil;
  for i := 0 to Form.ComponentCount - 1 do
  begin
    IsOk := False;
    Objeto := Form.Components[i];
    if  (Objeto.Name = 'SbNumero')
    or  (Objeto.Name = 'SbNome')
    or  (Objeto.Name = 'SbQuery')
    then begin
      if (Objeto is TBitBtn) then
        TBitBtn(Objeto).Enabled := False
      else
      if (Objeto is TSpeedButton) then
        TSpeedButton(Objeto).Enabled := False;
    end
    else if (Objeto is TdmkValUsu) then
    begin
      if TdmkValUsu(Objeto).Panel <> nil then
      begin
        if TdmkValUsu(Objeto).Panel.Name = Panel.Name then
          IsOK := True
        else
          IsOK := False;
      end else Geral.MensagemBox('TdmkValUsu sem panel cadastrado!',
      'Erro', MB_OK+MB_ICONERROR);
    end else begin
      IsOK := False;
      c := TComponent(Form.Components[i]).GetParentComponent;
      while c <> nil do
      begin
        if TComponent(c).Name = Panel.Name then
        begin
          IsOk := True;
          Break;
        end
        else c := TComponent(c).GetParentComponent;
      end;
    end;
    if IsOk then
    begin
      Objeto := Form.Components[i];
      //
      if Acao = stUpd then
      begin
        PI_DataField  := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataField');
        PI_DataSource := GetPropInfo(TComponent(Objeto).ClassInfo, 'DataSource');
        PI_QryCampo   := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
        PI_OldValor   := GetPropInfo(TComponent(Objeto).ClassInfo, 'OldValor');
        if PI_OldValor <> nil then
        begin
          if (PI_DataField <> nil) and (PI_DataSource <> nil) then
          begin
            //
            //PropVal := GetPropValue(Objeto, 'DataSource');
            //MySet   := TDataSource(PropVal);
            //MySet := TDataSet(TDataSource(TDBEdit(Objeto).DataSource).DataSet;
            MySou := TDataSource(TDBEdit(Objeto).DataSource);
            MySet := TDataSource(MySou).DataSet;
            //MyQry := TABSQuery(TDataSource(MySet));
            MyFld := GetStrProp(TComponent(Objeto), PI_DataField);
            SetPropValue(Objeto, 'OldValor',
              TDataSet(MySet).FieldByName(MyFld).AsVariant);

          end else if PI_QryCampo <> nil then
          begin
            Campo := GetStrProp(TComponent(Objeto), PI_QryCampo);
            if Campo <> '' then
            begin
              PropInfo := GetPropInfo(Objeto, 'OldValor');
              if PropInfo <> nil then
              try
                SetPropValue(Objeto, 'OldValor',
                  TABSQuery(Query).FieldByName(Campo).AsVariant);
              except
                Geral.MensagemBox('N�o foi poss�vel definir o ' +
                '"OldValor" = "' + Geral.VariantTostring(
                TABSQuery(Query).FieldByName(Campo).AsVariant) + '" do campo "' +
                Campo + '" no objeto "' + TComponent(Objeto).Name + '"', 'ERRO',
                MB_OK+MB_ICONERROR);
              end;
            end;
          end;
        end;
      end;
      //fim OldValor
      //
      PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'QryCampo');
      if PropInfo <> nil then
      begin
        Campo := GetStrProp(TComponent(Objeto), PropInfo);
        if Campo <> '' then
        begin
          if Acao = stIns then
            // N�o tem valor padr�o!
            //Valor := ValorPadrao(Qry, Campo)
            Valor := Null
          else
            try
              Valor := TABSQuery(Query).FieldByName(Campo).AsVariant;
            except
              Geral.MensagemBox('N�o foi poss�vel definir o valor "' +
              Geral.VariavelToString(Valor) + '" para o objeto "' +
              TComponent(Objeto).Name + '"', 'ERRO', MB_OK+MB_ICONERROR);
              //
            end;
          try
            if (Objeto is TdmkEdit) then
            begin
              if TdmkEdit(Objeto).FormatType = dmktfMesAno then
                Valor := Geral.MesEAnoDoMez(Valor)
              else
              if TdmkEdit(Objeto).FormatType = dmktf_AAAAMM then
                Valor := Geral.AnoEMesDoMez(Valor);
              TdmkEdit(Objeto).ValueVariant := Valor;
            end
            //
            else if (Objeto is TdmkDBLookupCombobox) then
              TdmkDBLookupCombobox(Objeto).KeyValue := Valor
            //
            else if (Objeto is TdmkEditCB) then
              TdmkEditCB(Objeto).ValueVariant := Valor
            //
            else if (Objeto is TdmkEditDateTimePicker) then
            begin
              if Valor <> Null then
              begin
                if VarType(Valor) = varString then
                  TdmkEditDateTimePicker(Objeto).Date :=
                    Geral.ValidaDataSimples(Valor, False)
                else
                  TdmkEditDateTimePicker(Objeto).Date := Valor;
              end;
            end
            //
            else if (Objeto is TdmkMemo) then
            begin
              if Valor = Null then Valor := '';
              TdmkMemo(Objeto).Text := Valor;
            end
            //
            else if (Objeto is TdmkRadioGroup) then
            begin
              // -1 complica inclus�o !!
              if Valor = Null then Valor := 0;
              TdmkRadioGroup(Objeto).ItemIndex := Valor;
            end
            //
            else if (Objeto is TdmkCheckBox) then
            begin
              if Valor = Null then Valor := False;
              if VarType(Valor) = varString then
              begin
                s := Uppercase(Valor);
                if (s = 'V') or (s = 'S') or (s = '1') then
                  TdmkCheckBox(Objeto).Checked := True
                else
                if (s = 'F') or (s = 'N') or (s = '') or (s = '0') then
                  TdmkCheckBox(Objeto).Checked := False
                else
                  Geral.MensagemBox('Valor indefinido para "TdmkCheckBox"!',
                  'ERRO', MB_OK+MB_ICONERROR);
              end else
                TdmkCheckBox(Objeto).Checked := Geral.IntToBool(Valor);
            end
            //
            else if (Objeto is TdmkCheckGroup) then
            begin
              if Valor = Null then Valor := False;
              TdmkCheckGroup(Objeto).Value := Valor;
            end
            //
            else if (Objeto is TdmkPopOutFntCBox) then
            begin
              if TdmkPopOutFntCBox(Objeto).Items.Count = 0 then
                TdmkPopOutFntCBox(Objeto).Items.Assign(Screen.Fonts);
              TdmkPopOutFntCBox(Objeto).FonteNome := Valor;
            end
            //
            else if (Objeto is TdmkValUsu) then
              TdmkValUsu(Objeto).ValueVariant := Valor
            //

            //

            else if (Objeto is TdmkDBEdit) then begin end //nada
            //
            else Mensagem(Campo, Objeto);
          except
            Mensagem(Campo, Objeto);
          end;
        end;
      end;
    end;
  end;
  Panel.Visible := True;
  //
  i := Low(PanelsToHide);
  j := High(PanelsToHide);
  for k := i to j do PanelsToHide[i].Visible := False;
  //
  i := Low(PanelsToShow);
  j := High(PanelsToShow);
  for k := i to j do PanelsToShow[i].Visible := True;
  //
  if CompoToFocus <> nil then
  try
    if Form.Visible then
      TWinControl(CompoToFocus).SetFocus;
  except
    ;
  end;
  if ImgTipo <> nil then
    ImgTipo.SQLType := Acao;
  Result := True;
  if (Acao = stIns) and (Qry <> nil) then
    Qry.Free;
  Screen.Cursor := crDefault;
end;

{
procedure TUMemModule.VerificaFeriadosFuturos(ComponentClass: TComponentClass;
  Reference: TComponent);
var
  Query: TABSQuery;
begin
  Query := TABSQuery.Create(Dmod);
  Query.Close;
  Query.Database := Dmod.MyDB;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT Data ');
  Query.SQL.Add('FROM feriados');
  Query.SQL.Add('WHERE Data > SYSDATE()');
  Query.Open;
  if Query.RecordCount = 0 then
  begin
    if Geral.MensagemBox('N�o h� nenhum feriado futuro. ' + Chr(13) +
    Chr(10) + 'Deseja cadastrar agora?', 'Pergunta',
    MB_YESNO+MB_ICONQUEsTION) = ID_YES then
      MyObjects.FormShow(ComponentClass, Reference);
  end;
end;

function TUMemModule.DiasUteis(DataI, DataF: TDateTime): Integer;
var
  Query: TABSQuery;
  i, k, ini, fim, Dias: Integer;
begin
  Query := TABSQuery.Create(Dmod);
  Query.Close;
  Query.Database := Dmod.MyDB;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT *');
  Query.SQL.Add('FROM feriados');
  Query.SQL.Add('WHERE WEEKDAY(Data) < 5'); // 5>Sabado  6>Domingo
  Query.SQL.Add('AND Data BETWEEN :P0 AND :P1');
  Query.SQL.Add('');
  Query.Params[00].AsDateTime := DataI;
  Query.Params[01].AsDateTime := DataF;
  Query.Open;
  ini := Trunc(Int(DataI));
  fim := Trunc(Int(DataF));
  Dias := fim - ini + 1 - Query.RecordCount;
  if Dias > 0 then
  begin
    Dias := 0;
    for i := ini to fim do
    begin
      k := DayOfWeek(i);
      if (k > 1) and (k < 7) then inc(Dias, 1);
    end;
    Dias := Dias - Query.RecordCount;
  end;
  if Dias < 0 then Dias := 0;
  Result := Dias;
end;

function TUMemModule.SQLInsUpdL(QrUpd: TABSQuery; Tipo, Tabela: String;
  Auto_increment: Boolean;
  SQLCampos, SQLIndex: array of String;
  ValCampos, ValIndex: array of Variant): Boolean;
var
  i, j: Integer;
  Valor, Liga, Data, Tab: String;
begin
  //Result := False;
  Tab := LowerCase(Tabela);
  if ( (Tipo <> CO_INCLUSAO) and (Tipo <> CO_ALTERACAO) ) then
  begin
    Geral.MensagemBox('AVISO: O status do formul�rio est� ' +
    'definido como "' + Tipo + '"', 'AVISO IMPORTANTE', MB_OK+MB_ICONWARNING);
  end;
  Data := '"' + Geral.FDT(Date, 1) + '"';
  QrUpd.SQL.Clear;
  if Tipo = CO_INCLUSAO then
    QrUpd.SQL.Add('INSERT INTO ' + Lowercase(Tab) + ' SET ')
  else
    QrUpd.SQL.Add('UPDATE ' + Lowercase(Tab) + ' SET ');

  //

  j := High(SQLCampos);
  for i := Low(SQLCampos) to j-1 do
  begin
    Valor := Geral.VariavelToString(ValCampos[i]);
    //if i < j then
      QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor + ', ');
    //else
      //QrUpd.SQL.Add(SQLCampos[i] + '=' + Valor);
  end;
  Valor := Geral.VariavelToString(ValCampos[j]);
  QrUpd.SQL.Add(SQLCampos[j] + '=' + Valor);
  //

  (*
  if Tipo = CO_INCLUSAO then
    QrUpd.SQL.Add('DataCad=' + Data + ', UserCad=' + IntToStr(VAR_USUARIO))
  else
    QrUpd.SQL.Add('DataAlt=' + Data + ', UserAlt=' + IntToStr(VAR_USUARIO));
  *)
  //
  if Auto_increment and (Tipo = CO_INCLUSAO) then
  begin
    ; // N�o faz nada
  end else begin
    for i := Low(SQLIndex) to High(SQLIndex) do
    begin
      Valor := Geral.VariavelToString(ValIndex[i]);
      if Tipo = CO_INCLUSAO then Liga := ', ' else
      begin
        if i = 0 then Liga := 'WHERE ' else Liga := 'AND ';
      end;
      QrUpd.SQL.Add(Liga + SQLIndex[i] + '=' + Valor);
    end;
  end;
  //
  try
    QrUpd.ExecSQL;
    Result := True;
  except
    GOTOm.LeMeuSQL_Fixo_m(QrUpd, '', nil, True, True);
    raise;
  end;
end;

function TUMemModule.SQLDel1(QrExec, QrData: TABSQuery; Tabela, Campo: String;
  Valor: Variant; PerguntaSeExclui: Boolean; PerguntaAlternativa: String): Boolean;
var
  Txt, Perg: String;
  Prox: Variant;
  Exclui: Boolean;
begin
  Result := False;
  if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  //
  Txt := Geral.VariavelToString(Valor);
  if PerguntaSeExclui then
  begin
    if Trim(PerguntaAlternativa) <> '' then Perg := PerguntaAlternativa else Perg :=
    'Confirma a exclus�o do registro selecionado?';
    Exclui := Geral.MensagemBox(Perg, 'Pergunta de Exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES;
  end else Exclui := True;
  if Exclui then
  begin
    QrExec.SQL.Clear;
    QrExec.SQL.Add(DELETE_FROM  + lowercase(Tabela));
    QrExec.SQL.Add('WHERE ' + Campo + '="' + String(Valor) +'"');
    try
      QrExec.ExecSQL;
      //
      Result := True;
    except
      GOTOm.LeMeuSQL_Fixo_m(QrExec, '', nil, True, True);
    end;
    if QrData <> nil then
    begin
      try
        QrData.Close;
        QrData.Open;
        Prox := ProximoRegistro(QrData, Campo, 0);
        QrData.Locate(Campo, Prox, []);
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUMemModule.SQLDel2(QrExec, QrData: TABSQuery; Tabela, CampoLoc: String;
  Campos: array of String; Valores: array of Variant; PerguntaSeExclui: Boolean;
  PerguntaAlternativa: String): Boolean;
var
  Valor, Perg, Liga: String;
  Prox: Variant;
  Exclui: Boolean;
  i, j: Integer;
begin
  Result := False;
  if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(Tabela) then
    Exit;
  if PerguntaSeExclui then
  begin
    if Trim(PerguntaAlternativa) <> '' then Perg := PerguntaAlternativa else Perg :=
    'Confirma a exclus�o do registro selecionado?';
    Exclui := Geral.MensagemBox(Perg, 'Pergunta de Exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES;
  end else Exclui := True;
  if Exclui then
  begin
    Liga := 'WHERE ';
    QrExec.SQL.Clear;
    QrExec.SQL.Add(DELETE_FROM + lowercase(Tabela));
    //
    j := High(Campos);
    for i := Low(Campos) to j do
    begin
      Valor := Geral.VariavelToString(Valores[i]);
      QrExec.SQL.Add(Liga + Campos[i] + '=' + Valor + ' ');
      Liga := 'AND ';
    end;
    //
    try
      QrExec.ExecSQL;
      //
      Result := True;
    except
      GOTOm.LeMeuSQL_Fixo_m(QrExec, '', nil, True, True);
    end;
    if QrData <> nil then
    begin
      try
        QrData.Close;
        QrData.Open;
        if CampoLoc <> '' then
        begin
          Prox := ProximoRegistro(QrData, CampoLoc, 0);
          QrData.Locate(CampoLoc, Prox, []);
        end;  
      except
        ;
      end;
    end;
  end else Result := False;
end;

function TUMemModule.SQLLoc1(Query: TABSQuery; Tabela, Campo: String;
  Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
var
  Txt: String;
begin
  Txt := Geral.VariavelToString(Valor);
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + '=' + Txt);
  Query.Open;
  //
  Result := Query.RecordCount;
  //
  if (Result = 0) and (Trim(MsgNaoLoc) <> '') then
    Geral.MensagemBox(MsgNaoLoc, 'Item n�o Localizado',
    MB_OK+MB_ICONWARNING);
  if (Result > 0) and (Trim(MsgLoc) <> '') then
    Geral.MensagemBox(MsgLoc + Chr(13) + Chr(10) + 'Itens = ' +
    IntToStr(Result), 'Itenm Localizado', MB_OK+MB_ICONEXCLAMATION);
end;

procedure TUMemModule.StartTransaction(Qry: TABSQuery);
begin
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('START TRANSACTION;');
  Qry.ExecSQL;
end;

function TUMemModule.DiaUtilMes(DataIni: TDateTime; DiaIni, QtdUteis,
  MaxDia: Integer): Integer;
var
  Ano1, Mes1, Dia1: Word;
  Ano2, Mes2, Dia2: Word;
  DiaA, Conta, Uteis: Integer;
  DataA: TDateTime;
begin
  //Result := DiaIni;
  DecodeDate(DataIni, Ano1, Mes1, Dia1);
  DataA := EncodeDate(Ano1, Mes1, DiaIni);
  //
  Uteis := 0;
  Conta := 0;
  DiaA  := DiaIni;
  while Uteis < QtdUteis do
  begin
    if DiaInutil(EncodeDate(Ano1, Mes1, DiaA)) = 0 then
      Uteis := Uteis + 1;
    inc(DiaA, 1);
    DecodeDate(DataA+Conta, Ano2, Mes2, Dia2);
    if Mes2 <> Mes1 then
    begin
      Result := DiaA -1;
      Exit;
    end;
    if DiaA >= MaxDia then
    begin
      Result := MaxDia;
      Exit;
    end;
  end;
  if DiaA > 1 then inc(DiaA, -1);
  Result := DiaA;
end;

function TUMemModule.NaoPermiteExclusao(Tabela, Form : String; Msg: Integer) : Boolean;
var
  Loc: Boolean;
begin
  if Msg = -2 then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER))
    or ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    then Result := False else
    Geral.MensagemBox('Acesso Restrito', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if Msg = 0 then
    begin
      Geral.MensagemBox('Tabela de perfis n�o definida ao verificar acesso.',
      'Acesso a formul�rio', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((VAR_LOGIN = 'ADMIN') and (Uppercase(VAR_SENHA) = VAR_ADMIN) and (VAR_ADMIN <> ''))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  then Result := False
  else begin
    if Dmod.QrPerfis.State <> dsBrowse then
    begin
      Result := False;
      if (Copy(Uppercase(Application.Title), 1, 5) <> 'EXULT')
      and (Copy(Uppercase(Application.Title), 1, 5) <> 'EXUL2') then
      Geral.MensagemBox('Tabela de perfis n�o est� aberta!. Avise a Dermatek',
        'Acesso a formul�rio', MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    Dmod.QrPerfis.Close;
    Dmod.QrPerfis.Open;
    if not Dmod.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('INSERT INTO perfisits SET Janela=:P0');
        Dmod.QrAux.Params[0].AsString  := Form;
        Dmod.QrAux.ExecSQL;
        Dmod.Privilegios(-1000);
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (Dmod.QrPerfisLibera.Value = 0) or (Loc=False) then
    //if Form = 'F' then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MensagemBox('Somente a senha gerencial (BOSS) tem acesso a este formul�rio!',
          'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION)
      else begin
        case Msg of
         -1: ;// nada;
          0: Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          1: Geral.MensagemBox('Login com acesso parcial!', 'Acesso a formul�rio',
             MB_OK+MB_ICONEXCLAMATION);
          2: Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          3: Geral.MensagemBox('Login com acesso parcial, sem acesso a edi��o!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
        end;
      end;
    end else Result := False;
  end;
end;

procedure TUMemModule.ExportaRegistrosEntreDBs(TabOrig, TabDest, CondicaoSQL: String;
BaseOrig, BaseDest: TM y S Q LDatabase; RichEdit: TRichEdit);
  procedure Info(RichEdit: TRichEdit; TabDest, Texto: String);
  begin
    if RichEdit <> nil then
    begin
      RichEdit.SelAttributes.Color := clBlue;
      RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
        Uppercase(lowercase(TabDest)) + Texto + Chr(13) + Chr(10) + RichEdit.Text;
      RichEdit.Update;
      Application.ProcessMessages;
    end;
  end;
var
  Dir1, Dir2, Arq: String;
  F: TextFile;
  ArqWeb1, ArqWeb2, S, Campos, Prefix: String;
  Query, Qr1, Qr2, Qr3: TABSQuery;
begin
  Dir1 := 'C:\Dermatek\Web\' + Application.Title + '\Conf\';
  Dir2 := 'C:/Dermatek/Web/' + Application.Title + '/Data/';
  ArqWeb1 :=  Dir1 + 'SQL_%s.%s';
  ArqWeb2 :=  Dir2 + 'SQL_%s.%s';
  if not ForceDirectories(Dir1) then
  begin
    Geral.MensagemBox('N�o foi poss�vel criar o diret�rio "' + Dir1 +
    '". Tente cri�-lo manualmente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if not ForceDirectories(Dir2) then
  begin
    Geral.MensagemBox('N�o foi poss�vel criar o diret�rio "' + Dir2 +
    '". Tente cri�-lo manualmente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Query := TABSQuery.Create(Dmod);
  Query.Close;
  Query.Database := BaseOrig;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + lowercase(TabOrig));
  Query.SQL.Add(CondicaoSQL);
  Query.Open;
  //
  Qr1 := TABSQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Database := BaseOrig;
  Qr1.SQL.Clear;
  //
  Qr2 := TABSQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Database := BaseDest;
  Qr2.SQL.Clear;
  //
  Qr3 := TABSQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Database := BaseDest;
  Qr3.SQL.Clear;
  //
  Campos := '';
  Prefix := '';
  Info(RichEdit, lowercase(TabDest), ' - Obtendo campos da tabela no destino...');
  Arq := Format(ArqWeb1, [lowercase(TabDest), 'sql']);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Campos := Campos + S;
    end;
    CloseFile(F);
  end else begin
    Campos := ObtemCamposDeTabelaIdentica(BaseDest, lowercase(TabDest), Prefix);
    Geral.SalvaTextoEmArquivo(Arq, Campos, True);
  end;

  //

  Info(RichEdit, lowercase(TabDest), ' - Exportando para arquivo tempor�rio...');
  Arq := Format(ArqWeb2, [lowercase(TabDest), 'txt']);
  if FileExists(Arq) then
    DeleteFile(Arq);
  if FileExists(M L A G e r a l.DuplicaBarras(Arq)) then
    DeleteFile(M L A G e r a l.DuplicaBarras(Arq));
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ lowercase(TabOrig));
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.ExecSQL;
  //

  Info(RichEdit, lowercase(TabDest), ' - Excluindo registros duplicados na web...');
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + lowercase(TabDest) );
  Qr3.SQL.Add( CondicaoSQL );
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //
  Info(RichEdit, lowercase(TabDest), ' - Importando de arquivo tempor�rio...');
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+lowercase(TabDest));
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(taborig)+' SET AlterWeb=0');
  Qr1.SQL.Add( CondicaoSQL );
  Qr1.ExecSQL;
  //
  Info(RichEdit, lowercase(TabDest), ' - Transfer�ncia de dados finalizada!');
  Info(RichEdit, '', '===============================================');
end;

//  Duplica��o de registros
function TUMemModule.EspelhaRegistroEntreTabelas(RichEdit: TRichEdit;
//SQLCampos: array of String; ValCampos: array of Variant;
TabOrig, TabDest: String; BaseOrig, BaseDest: TM y S Q LDatabase;
WHERE_Condition: String): Boolean;
var
  Qr1, Qr2, Qr3, Qr4: TABSQuery;
  //i, j: Integer;
  x: PChar;
  Virgula: String;
begin
  if RichEdit <> nil then
  begin
    RichEdit.SelAttributes.Color := clBlue;
    RichEdit.Text := FormatDateTime('hh:nn:ss:zzz', now()) + ' > ' +
      'Fazendo download da tabela ' + '"' + lowercase(TabOrig) + '"' +
      Chr(13) + Chr(10) + RichEdit.Text;
    RichEdit.Update;
    Application.ProcessMessages;
  end;
  //
  //Result := False;
  Qr1 := TABSQuery.Create(Dmod);
  Qr1.Close;
  Qr1.Database := BaseOrig;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SHOW Fields FROM ' + lowercase(TabOrig));
  Qr1.Open;
  //
  Qr2 := TABSQuery.Create(Dmod);
  Qr2.Close;
  Qr2.Database := BaseDest;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('SHOW Fields FROM ' + lowercase(TabDest));
  Qr2.Open;
  //
  Qr3 := TABSQuery.Create(Dmod);
  Qr3.Close;
  Qr3.Database := BaseDest;
  Qr3.SQL.Clear;
  //
  Qr4 := TABSQuery.Create(Dmod);
  Qr4.Close;
  Qr4.Database := BaseOrig;
  Qr4.SQL.Clear;
  Qr4.SQL.Add('SELECT * FROM '+lowercase(taborig));
  Qr4.SQL.Add(WHERE_Condition);
  //
  if Qr1.RecordCount <> Qr2.RecordCount then
  begin
    x := PChar('A tabela destino "' +
    BaseOrig.DatabaseName + '.' + lowercase(TabOrig) + '" cont�m quantidade diferente ' +
    'de campos (' + IntToStr(Qr2.RecordCount) + ') da tabela origem "' +
    BaseDest.DatabaseName + '.' + lowercase(TabDest) + '" (' +
    IntToStr(Qr1.RecordCount) + '). Espelhamento de registro cancelado!');
    Geral.MensagemBox(x, 'Aviso', MB_OK+MB_ICONWARNING);
    Result := False;
    Exit;
  end;
  //
  Qr1.First;
  while not Qr1.Eof do
  begin
    if not Qr2.Locate('Field', Qr1.Fields[0].AsString, [loCaseInsensitive]) then
    begin
      x := PChar('A tabela de destino "' +
      BaseDest.DatabaseName + '.' + lowercase(TabDest) + '" n�o cont�m o campo ' +
      Qr1.Fields[0].AsString + ' em sua estrutura!');
      Geral.MensagemBox(x, 'Aviso', MB_OK+MB_ICONWARNING);
      Result := False;
      Exit;
    end;
    Qr1.Next;
  end;
  //
  Qr3.Close;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + lowercase(tabdest));
  Qr3.SQL.Add(WHERE_Condition);
  Qr3.ExecSQL;

  //

  Qr4.Open;
  Qr4.First;
  while not Qr4.Eof do
  begin
    Virgula := '';
    Qr3.SQL.Clear;
    Qr3.SQL.Add('INSERT INTO ' + lowercase(TabDest) + ' SET ' );
    Qr1.First;
    while not Qr1.Eof do
    begin
      if Qr2.Locate('Field', Qr1.Fields[0].AsString, [loCaseInsensitive]) then
      begin
        Qr3.SQL.Add(Virgula + Qr1.Fields[0].AsString + '=' +
        Geral.VariavelToString(Qr4.FieldByName(Qr1.Fields[0].AsString).Value));
      end;
      Virgula := ', ';
      Qr1.Next;
    end;
    Qr3.ExecSQL;
    Qr4.Next;
  end;
  if RichEdit <> nil then
  begin
    RichEdit.SelAttributes.Color := clBlue;
    RichEdit.Text :=  '===============================================' +
      Chr(13) + Chr(10) + RichEdit.Text;
    RichEdit.Update;
    Application.ProcessMessages;
  end;
  //
  Result := True;
end;

function TUMemModule.BuscaPrimeiroCodigoLivre(Tabela, Campo: String): Integer;
var
  Query: TABSQuery;
begin
  Result := 0;
  Query := TABSQuery.Create(Dmod);
  Query.Close;
  Query.Database := Dmod.MyDB;


  Query.SQL.Clear;
  Query.SQL.Add('LOCK TABLES ' + Tabela + ' WRITE;');
  Query.ExecSQL;




  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + ' > 0');
  Query.SQL.Add('ORDER BY ' + Campo);
  Query.Open;
  //
  Query.Last;
  if Query.FieldByName(Campo).AsInteger = Query.RecNo then
    Result := Query.RecordCount + 1 else
  begin
    Query.First;
    while not Query.Eof do
    begin
      if Query.FieldByName(Campo).AsInteger > Query.RecNo then
      begin
        Result := Query.RecNo;
        Query.SQL.Clear;
        Query.SQL.Add('UNLOCK TABLES;');
        Query.ExecSQL;
        Query.Free;
        Exit;
      end;
      Query.Next;
    end;
  end;
  //
  Query.SQL.Clear;
  Query.SQL.Add('UNLOCK TABLES;');
  Query.ExecSQL;
  Query.Free;
end;

function TUMemModule.Busca_IDCtrl_NFe(Acao: TSQLType; Atual: Integer): Integer;
var
  Cod1, Cod2, Cod3, Cod4: Integer;
begin
  if Acao = stUpd then
  begin
    Result := Atual;
    Exit;
  end;
  //
  Cod1 := UMyMod.BuscaEmLivreY_Def('stqmovnfsa', 'IDCtrl', Acao, Atual);
  //Cod2 := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', Acao, Atual);
  Cod2 := DModG.BuscaProximoInteiro('nfecaba', 'IDCtrl', '', Atual);
  Cod4 := DModG.BuscaProximoInteiro('stqmovitsa', 'IDCtrl', '', Atual);
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('LOCK TABLES controle WRITE');
  Dmod.QrAux.ExecSQL;
  //
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT IDCtrl_NFe FROM Controle');
  Dmod.QrAux.Open;
  Cod3 := Dmod.QrAux.FieldByName('IDCtrl_NFe').AsInteger;
  Cod3 := Cod3 + 1;
  //
  if Cod1 >= Cod2 then
  begin
    if Cod1 >= Cod3 then
      Result := Cod1
    else
      Result := Cod3;
  end else
  begin
    if Cod2 >= Cod3 then
      Result := Cod2
    else
      Result := Cod3;
  end;
  if Cod4 > Result then
    Result := Cod4;
  //
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('UPDATE controle SET IDCtrl_NFe=' + FormatFloat('0', Result));
  Dmod.QrAux.ExecSQL;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('UNLOCK TABLES ');
  Dmod.QrAux.ExecSQL;
  //
end;
}

function TUMemModule.AbreABSQuery1(Query: TABSQuery; SQL: array of String): Boolean;
var
  I: Integer;
begin
  //Result := False;
  try
    Screen.Cursor := crSQLWait;
    Query.Close;
    if High(SQL) > 0 then
    begin
      Query.SQL.Clear;
      for I := 0 to High(SQL) do
        Query.SQL.Add(SQL[I]);
    end;
    Query.Open;
    Result := True;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MensagemBox('Erro ao tentar executar uma SQL no ABSQuery!' + #13#10 +
    'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
    dmkPF.LeMeuTexto(Query.SQL.Text);
    raise;
  end;
end;

function TUMemModule.AbreABSTable1(Table: TABSTable): Boolean;
begin
  //Result := False;
  try
    Screen.Cursor := crSQLWait;
    Table.Close;
    Table.Open;
    Result := True;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Geral.MensagemBox('Erro ao tentar abrir ABSTable:' + #13#10 +
    'Nome componente: ' + Table.Name + #13#10 +
    'Nome tabela: ' + Table.TableName + #13#10 +
    'Avise a DERMATEK!', 'ERRO', MB_OK+MB_ICONWARNING);
    raise;
  end;
end;

{
function TUMemModule.AbreQuery(Query: TABSQuery; Aviso: String): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
  procedure MostraQuery(Query: TABSQuery);
  var
    i: Integer;
    Texto: WideString;
  begin
    Texto := '/* ' + Aviso + ' */' + #13#10 + Query.SQL.Text;
    for i := 1 to Query.ParamCount do
    begin
      Texto := Texto + #13#10 + '/* Params[' + IntToStr(i-1) + '] = ' +
        TParam(Query.Params[i-1]).AsString + '*/';
    end;
    Texto := Texto + #13#10 + '/*' + Query.Name + '*/';
    Geral.MensagemBox(Texto, 'Erro de SQL', MB_OK+MB_ICONERROR);
    //

    (*
    MyObjects.CriaForm_AcessoTotal(TFmAviso, FmAviso);
    with FmAviso do
    begin
      Memo.Text := '/* ' + Aviso + ' */' + #13#10 + Query.SQL.Text;
      BtOK.Enabled := True;
      CkNaoMais.Visible := False;
      LaNumero.Caption := '004';
      //
      for i := 1 to Query.ParamCount do
      begin
        Memo.Lines.Add('/* Params[' + IntToStr(i-1) + '] = ' +
          TParam(Query.Params[i-1]).AsString + '*/');
      end;
      //
      Memo.Lines.Add('/*' + Query.Name + '*/');
      ShowModal;
      Destroy;
    end;
    *)
  end;
begin
  //Result := False;
  try
    Query.Close;
    Query.Open;
    Result := True;
  except
    MostraQuery(Query);
    raise;
  end;
end;

function TUMemModule.AcertaOrdemTodosRegistros(Tabela, FldIndice, FldOrdem: String;
  QueryAReordenar, QrUpd: TABSQuery): Boolean;
  procedure AtualizaOrdemDoItem(Ordem, Indice: Integer);
  begin
    UMyMod.SQLInsUpd(QrUpd, stUpd, Lowercase(Tabela), False, [
    FldOrdem], [FldIndice], [Ordem], [Indice], False);
  end;
var
  I, Indice: Integer;
begin
  //Result := False;
  Indice := QueryAReordenar.FieldByName(FldIndice).AsInteger;
  //
  I := 0;
  QueryAReordenar.First;
  while not QueryAReordenar.Eof do
  begin
    I := I + 1;
    AtualizaOrdemDoItem(I, QueryAReordenar.FieldByName(FldIndice).AsInteger);
    QueryAReordenar.Next;
  end;
  //
  QueryAReordenar.Close;
  QueryAReordenar.Open;

  Result := QueryAReordenar.Locate(FldIndice, Indice, []);
end;
}

function TUMemModule.ExecutaQuery(Query: TABSQuery): Boolean;
// AbreSQL - OpenSQL - SQLAbre - SQLOpen
  procedure MostraQuery(Query: TABSQuery);
  var
    i: Integer;
    Texto: String;
  begin
    Texto := '';
    for i := 1 to Query.ParamCount do
    begin
      Texto := Texto + '/* Params[' + IntToStr(i-1) + '] = ' +
        TParam(Query.Params[i-1]).AsString + '*/' + #13#10;
    end;
    Texto := Texto + #13#10 + '/*' + Query.Name + '*/';
    Geral.MensagemBox(Query.SQL.Text + #13#10 + Texto, 'Aviso', MB_OK+MB_ICONWARNING);
    {
    MyObjects.CriaForm_AcessoTotal(TFmAviso, FmAviso);
    with FmAviso do
    begin
      Memo.Lines := Query.SQL;
      BtOK.Enabled := True;
      CkNaoMais.Visible := False;
      LaNumero.Caption := '004';
      //
      for i := 1 to Query.ParamCount do
      begin
        Memo.Lines.Add('/* Params[' + IntToStr(i-1) + '] = ' +
          TParam(Query.Params[i-1]).AsString + '*/');
      end;
      //
      Memo.Lines.Add('/*' + Query.Name + '*/');
      ShowModal;
      Destroy;
    end;
    }

  end;
begin
  //Result := False;
  try
    Query.ExecSQL;
    Result := True;
  except
    //MostraQuery(Query);
    Geral.MensagemBox(Query.SQL.Text, 'Erro de Execu��o de Query', MB_OK+MB_ICONERROR);
    raise;
  end;
end;

{
function TUMemModule.ExecutaDB(DB: TM y S Q LDatabase; SQL: String): Boolean;
// Execute
  procedure MostraSQL();
  //var
    //i: Integer;
  begin
    MyObjects.CriaForm_AcessoTotal(TFmAviso, FmAviso);
    with FmAviso do
    begin
      Memo.Text := SQL;
      BtOK.Enabled := True;
      CkNaoMais.Visible := False;
      LaNumero.Caption := '004';
      //
      (*
      for i := 1 to Query.ParamCount do
      begin
        Memo.Lines.Add('/* Params[' + IntToStr(i-1) + '] = ' +
          TParam(Query.Params[i-1]).AsString + '*/');
      end;
      //
      *)
      Memo.Lines.Add('/*' + DB.Name + '*/');
      ShowModal;
      Destroy;
    end;
  end;
begin
  //Result := False;
  try
    DB.Execute(SQL);
    Result := True;
    //MostraSQL();
  except
    MostraSQL();
    raise;
  end;
end;
}

function TUMemModule.HabilitaMenuItemInt(MenuItens: array of TMenuItem;
Query: TABSQuery; Campo: String; MinRec, MinVal: Integer): Boolean;
var
  i: Integer;
begin
  if Query.State = dsInactive then Result := False else
  begin
    Result := Query.RecordCount >= MinRec;
    if Result and (Campo <> '') then
      Result := Query.FieldByName(Campo).AsInteger >= MinVal;
  end;
  for i := Low(MenuItens) to High(MenuItens) do
    if MenuItens[i] <> nil then
      MenuItens[i].Enabled := Result;
end;

function TUMemModule.ImpedeExclusaoPeloNomeDaTabela(Tabela: String): Boolean;
begin
  if Lowercase(Tabela) = Lowercase(VAR_LCT) then
  begin
    Result := True;
    Geral.MensagemBox('Exclus�o cancelada! A tabela "' + Tabela +
      '" n�o pode ter lan�amentos exclu�dos nesta rotina! Avise a Dermatek!',
      'Aviso', MB_OK+MB_ICONWARNING);
  end else Result := False;
end;

{
procedure TUMemModule.AtzEntiAltDtPlaCt(QrUpd: TABSQuery; CliInt: Integer;
Data: TDateTime);
var
 DtTxt: String;
begin
  DtTxt := Geral.FDT(Data, 1);
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, ');
  QrUpd.SQL.Add('AltDtPlaCt=:P0 ');
  QrUpd.SQL.Add('WHERE Codigo=:P1 ');
  QrUpd.SQL.Add('AND AltDtPlaCt > :P2');
  QrUpd.Params[00].AsInteger := CliInt;
  QrUpd.Params[01].AsString  := DtTxt;
  QrUpd.Params[02].AsString  := DtTxt;
  QrUpd.ExecSQL;
end;
}

function TUMemModule.BuscaNovoCodigo_Int(QrAux: TABSQuery; Tabela, Campo: String;
CamposExtras: array of String; ValExtras: array of Variant;
SQLType: TSQLType; Padrao: Integer; Sinal: TSinal;
dmkEdit: TdmkEdit): Integer;
var
  i, j: Integer;
  Liga, Valor: String;
begin
  Result := 0;
  if SQLType = stUpd then
  begin
    Result := Padrao;
    Exit;
  end;
  QrAux.Close;
  QrAux.SQL.Clear;
  if Sinal = siPositivo then
    QrAux.SQL.Add('SELECT MAX(' + Campo + ') Campo FROM ' + lowercase(Tabela))
  else
    QrAux.SQL.Add('SELECT MIN(' + Campo + ') Campo FROM ' + lowercase(Tabela));

  //

  Liga := 'WHERE';
  j := High(CamposExtras);
  for i := Low(CamposExtras) to j do
  begin
    Valor := Geral.VariavelToString(ValExtras[i]);
    if (i < j) then
      QrAux.SQL.Add(Liga + ' ' + CamposExtras[i] + '=' + Valor + ', ')
    else
      QrAux.SQL.Add(Liga + ' ' + CamposExtras[i] + '=' + Valor);
    Liga := 'AND';
  end;
  //
  try
    QrAux.Open;
  except
    GOTOm.LeMeuSQL_Fixo_m(QrAux, '', nil, True, True);
  end;
  if Sinal = siPositivo then
  begin
    Result := QrAux.Fields[0].AsInteger + 1;
    if Result = 0 then Result := 1;
  end else begin
    Result := QrAux.Fields[0].AsInteger - 1;
    if Result = 0 then Result := -1;
  end;
  //
  if dmkEdit <> nil then
    dmkEdit.ValueVariant := Result;
end;

end.

