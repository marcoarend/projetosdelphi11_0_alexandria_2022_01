unit MemDBCheck;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls,  ComCtrls, StdCtrls, DB, (*DBTables,*) (*UnMyLinguas,*)
  UnInternalConsts, (*UnGOTOm,*) dbgrids, (*MyListas,*) dmkPermissoes,
  Variants, TypInfo, dmkGeral, UnDmkEnums;

type
{
  TAcaoCriaTabela   = (actCreate, actDescribe);
  TResultVerify = (rvOK, rvErr, rvAbort);
  TMyMotivo = (mymotDifere, mymotSemRef);
  TTabStepCod = (tscEntidades, tscEtqPrinCad);
}

  TMyDBCheck = class(TObject)
  private
    { Private declarations }
{
    function VerificaEstrutura(DataBase: String; Memo: TMemo;
             RecriaRegObrig: Boolean; LaAvisoRed, LaAvisoGre, LaAvisoBlu,
             LaTempoI, LaTempoF, LaTempoT: TLabel; LBTabs: TListBox): Boolean;
    function VerificaEstrutLoc(DataBase: String; Memo: TMemo;
             LaAvisoRed, LaTempoF, LaTempoT: TLabel): Boolean;
    function TabelaAtiva(Tabela: String): Boolean;
    function IndiceExiste(DataBase: String; Tabela, Indice: String;
             Memo: TMemo): Integer;
    function CampoAtivo(DataBase: String; TabelaNome, TabelaBase, Campo: String;
             Memo: TMemo): Integer;
    function ExcluiIndice(DataBase: String; Tabela, IdxNome,
             Aviso: String; Motivo: TMyMotivo; Memo: TMemo): TResultVerify;
    function CriaIndice(DataBase: String; Tabela, IdxNome, Aviso:
             String; Memo: TMemo): TResultVerify;
    function RecriaIndice(DataBase: String; Tabela, IdxNome, Aviso:
             String; Memo: TMemo): TResultVerify;
    //function VerificaRegistrosDePerfis(DataBase: String;
             //Memo: TMemo; Avisa: Boolean): TResultVerify;
    function VerificaRegistrosDeUserSets(DataBase: String;
             Memo: TMemo; Avisa: Boolean): TResultVerify;
    function VerificaRegistrosDeImpDOS(DataBase: String;
             Memo: TMemo; Avisa: Boolean): TResultVerify;
    function VerificaRegistrosDeCheque(DataBase: String;
             Memo: TMemo; Avisa: Boolean): TResultVerify;
    function VerificaRegistrosDeJanelas(DataBase: String;
             Memo: TMemo; Avisa: Boolean): TResultVerify;
    //
    procedure VerificaControle(Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: String; Memo: TMemo);
    function VerificaRegistrosDeArquivo(DataBase: String; TabelaNome, TabelaBase: String;
             Memo: TMemo; Avisa: Boolean): TResultVerify;
    procedure VerificaVariaveis(Memo: TMemo);
    //
    // T E M P O R A R I O
    procedure ConsertaTabela(Tabela: String);
}
  public
    { Public declarations }
{
    FProgress: TProgressBar;
    function TabelaExiste(Tabela: String): Boolean;
    function ImpedeVerificacao(): Boolean;
    procedure GravaAviso(Aviso: String; Memo: TMemo; ToFile: Boolean = True);
    function EfetuaVerificacoes(DataBase, DataBaseLoc: T m y S Q L DataBase;
             Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta,
             RecriaRegObrig: Boolean; LaAvisoRed, LaAvisoGre, LaAvisoBlu,
             LaTempoI, LaTempoF, LaTempoT: TLabel; LBTabs: TListBox): Boolean;
    function CriaDatabase(Query: T m y S Q L Query; Database: String): Boolean;
    function CriaTabela(DataBase: String; TabelaNome, TabelaBase: String;
             Memo: TMemo; Acao: TAcaoCriaTabela): Boolean;
    function VerificaCampos(DataBase: String; TabelaNome, TabelaBase: String;
             Memo: TMemo; RecriaRegObrig: Boolean): TResultVerify;
    function CampoExiste(Tabela, Campo: String; Memo: TMemo): Integer;
    function VerificaRegistrosObrigatorios_Inclui(DataBase: String;
             TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean): TResultVerify;
    function VerificaRegistrosObrigatorios_Corrige(DataBase: String;
             TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean): TResultVerify;
    function AcessoNegadoAoForm_2(Tabela, Form : String; ModoAcesso:
             TAcessFormModo): Boolean;
    function AcessoNegadoAoForm_3(Tabela, Form, Descricao: String; ModoAcesso:
             TAcessFormModo): Boolean;
    function AcessoAoForm(Form, Descricao: String; ModoAcesso:
             TAcessFmModo): Integer;
    function CriaFormAntigo(InstanceClass: TComponentClass; var Reference;
             ModoAcesso: TAcessFormModo): Boolean;
}
    function CriaFm(InstanceClass: TComponentClass; var Reference;
             ModoAcesso: TAcessFmModo): Boolean;
{
    //function Account(DataBase, Tabela, CampoA, CampoS: String; Procura: TBDProcura):
    //         TFoundInBD;
    //function QuaisItens: TSelType;
    function Quais_Selecionou(const Query: T m y S Q L Query; DBGrid:
             TDBGrid; var Quais: TSelType): Boolean;
    function Quais_Selecionou_ExecProc(const Query: T m y S Q L Query; DBGrid:
             TDBGrid; var Quais: TSelType; Procedure1: TProcedure): Boolean;
             // QuaisExclui Quais exclui Quais_Exclui
    function ExcluiRegistro(QrExecSQL, QrDados: T m y S Q L Query; TabelaTarget: String;
             CamposSource, CamposTarget: array of String; ReopenQrDados: Boolean;
             Pergunta: String = ''): Boolean;
    function QuaisItens_Exclui(QrExecSQL, QrDados: T m y S Q L Query; DBGrid: TDBGrid;
             TabelaTarget: String; CamposSource, CamposTarget: array of String;
             Quais: TSelType; SQL_AfterDel: String): Integer;
    function QuaisItens_Altera(QrExecSQL, QrDados: T m y S Q L Query;
             DBGrid: TDBGrid; Tabela: String;
             SQLCamposAlt, SQLIndexAlt, SQLIndexSource: array of String;
             SQLValores: array of Variant; Quais: TSelType;
             Pergunta, AvisaAlterados: Boolean): Boolean;
    function ObtemData(const DtDefault: TDateTime; var DtSelect:
             TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
             const HabilitaHora: Boolean = False): Boolean;
    function ObtemData_Juros_Multa(const MinData, DtDefault: TDateTime;
             var DtSelect: TDateTime; const DefJuros, DefMulta: Double;
             var Juros, Multa: Double): Boolean;
    function LiberaPelaSenhaBoss: Boolean;
    function LiberaPelaSenhaAdmin: Boolean;
    function EscolheCodigo(Aviso, Caption, Prompt: String; ListSource: TDataSource;
             ListField: String; Default: Variant): Variant;
    function ObtemCodUsuZStepCodUni(Tabela: TTabStepCod): Integer;
    function Obtem_verProc: String;
    procedure VerificaEntiCliInt();
}
  end;

var
  DBCheck: TMyDBCheck;
{
  FText      : TextFile;
  FTabelas   : TStringList;
  FTabLoc    : TStringList;
  FIncrement : TStringList;
  FCheque    : TStringList;
  FLCampos   : TList;
  FLIndices  : TList;
  FLJanelas  : TList;
  FRCampos   : TCampos;
  FRIndices  : TIndices;
  FRJanelas  : TJanelas;
  FPergunta  : Boolean;
  FListaSQL  : TStringList;
  FPerfis    : TStringList;
  FUserSets  : TStringList;
  FImpDOS    : TStringList;
  FViuControle: Integer;
  FCriarAtivoAlterWeb: Boolean;
}

implementation

// � chamado por FmVerifiDB
{
uses UnMyObjects, Module, UCreate, UnMsgInt, QuaisItens, GetData, CashTabs, ModuleGeral,
Senha, SelCod, ZStepCodUni, UM y S Q LModule;
}

{
function TMyDBCheck.EfetuaVerificacoes(DataBase, DataBaseLoc: T m y S Q L DataBase;
Memo: TMemo; Estrutura, EstrutLoc, Controle, Pergunta, RecriaRegObrig: Boolean;
LaAvisoRed, LaAvisoGre, LaAvisoBlu, LaTempoI, LaTempoF, LaTempoT: TLabel;
LBTabs: TListBox): Boolean;
var
  WinPath : array[0..144] of Char;
  Name: String;
  //Cursor: TCursor;
begin
  LBTabs.Items.Clear;
  LaTempoI.Caption := '...';
  LaTempoF.Caption := '...';
  LaTempoT.Caption := '...';
  Result := False;
  if ImpedeVerificacao() then
  begin
    Exit;
  end;
  Result := False;
  if Geral.MensagemBox('Ser� realizada uma verifica��o na base de dados: '+
  Chr(13)+Chr(10)+'Servidor: ' + Database.Host +
  Chr(13)+Chr(10)+'Banco de dados: ' + Database.DatabaseName + Chr(13)+Chr(10)+
  'A altera��es realizados nesta verifica��o somente ser�o revers�veis ' +
  'perante restaura��o de backup.' + Chr(13) + Chr(10)+
  'Para fazer um backup clique em "n�o" e feche esta janela.' +
  ' Na janela principal h� um bot�o de backup!' +
  Chr(13)+Chr(10)+ 'Confirma assim mesmo a verifica��o?',
  'Verifica��o de Banco de Dados', MB_YESNO+MB_ICONWARNING) <> ID_YES then Exit;
  Memo.Update;
  Application.ProcessMessages;
  Result := True;
  FPergunta := Pergunta;
  //Cursor := Screen.ActiveForm.Cursor;
  Screen.Cursor := crHourGlass;
  try
    // verifica se a vers�o atual � mais nova
    // se n�o � impede a verifica��o
    if VAR_GOTOM y S Q LDBNAME.DatabaseName <> '' then
    begin
      Dmod.QrMas.Close;
      Dmod.QrMas.Database := VAR_GOTOM y S Q LDBNAME;
      Dmod.QrMas.SQL.Clear;
      Dmod.QrMas.SQL.Add('SHOW TABLES FROM ' + VAR_GOTOM y S Q LDBNAME.DatabaseName);
      Dmod.QrMas.SQL.Add('LIKE "Controle"');
      Dmod.QrMas.Open;
      if Dmod.QrMas.RecordCount > 0 then
      begin
        Dmod.QrMas.Close;
        Dmod.QrMas.Database := VAR_GOTOM y S Q LDBNAME;
        Dmod.QrMas.SQL.Clear;
        Dmod.QrMas.SQL.Add('SHOW COLUMNS FROM controle LIKE "versao"');
        Dmod.QrMas.Open;
        if Dmod.QrMas.RecordCount > 0 then
        begin
          Dmod.QrMas.Close;
          Dmod.QrMas.Database := VAR_GOTOM y S Q LDBNAME;
          Dmod.QrMas.SQL.Clear;
          Dmod.QrMas.SQL.Add('SELECT Versao FROM controle');
          Dmod.QrMas.Open;
          //
          if CO_VERSAO < Dmod.QrMas.FieldByName('Versao').AsInteger then
          begin
            Geral.MensagemBox('Verifica��o de banco de dados cancelada! '+
            Chr(13) + Chr(10) + ' A vers�o deste arquivo � inferior a cadastrada ' +
            'no banco de dados.', 'AVISO', MB_OK+MB_ICONWARNING);
            Exit;
          end;
        end;
      end;
    end else Geral.MensagemBox('Banco de dados n�o definido na verifica��o!',
    'Erro', MB_OK+MB_ICONERROR);
    //
    GetWindowsDirectory(WinPath,144);
    Name := StrPas(WinPath)+'\Dermatek';
    if not DirectoryExists(Name) then ForceDirectories(Name);
    Name := Name+'\LogFile.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(FText, Name);
    ReWrite(FText);
    try
      MyList.VerificaOutrosAntes(DataBase, Memo);
      VerificaVariaveis(Memo);
      if Memo <> nil then Memo.Lines.Clear;
      Memo.Refresh;
      Memo.Update;
      // Primeiro VAR_MYARQDB para definir tabelas no final como VAR_GOTOM y S Q LDBNAME
      if VAR_MYARQDB <> nil then
        VerificaEstrutura(VAR_MYARQDB, Memo, RecriaRegObrig, LaAvisoRed,
        LaAvisoGre, LaAvisoBlu, LaTempoI, LaTempoF, LaTempoT, LBTabs)
      else if Memo <> nil then
        //Memo.Lines.Add('Informa��o: "VAR_MYARQDB" n�o definido.');
        MyObjects.Informa(LaAvisoRed, False, '"VAR_MYARQDB" n�o definido.');
      if Estrutura then VerificaEstrutura(Database, Memo, RecriaRegObrig,
        LaAvisoRed, LaAvisoGre, LaAvisoBlu, LaTempoI, LaTempoF, LaTempoT,
        LBTabs)
      else begin
        Dmod.QrMas.Close;
        Dmod.QrMas.Database := Database;//VAR_GOTOM y S Q LDBNAME;
        Dmod.QrSQL.Close;
        Dmod.QrSQL.Database := Database;//VAR_GOTOM y S Q LDBNAME;
        Dmod.QrIdx.Close;
        Dmod.QrIdx.Database := Database;//VAR_GOTOM y S Q LDBNAME;
      end;
      if EstrutLoc then VerificaEstrutLoc(DataBaseLoc, Memo, LaAvisoRed,
        LaAvisoGre, LaAvisoBlu);
      if Controle then VerificaControle(Memo);
      VerificaOutrosDepois(DataBase, Memo);
      CloseFile(FText);
      Geral.WriteAppKey('Versao', Application.Title, CO_VERSAO, ktInteger,
        HKEY_LOCAL_MACHINE);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE controle SET Versao =:P0');
      Dmod.QrUpd.Params[0].AsInteger := CO_VERSAO;
      Dmod.QrUpd.ExecSQL;
    except
      CloseFile(FText);
      raise;
    end;
    Screen.Cursor := crDefault;
  except
    //Result := False;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TMyDBCheck.VerificaEstrutura(DataBase: String;
Memo: TMemo; RecriaRegObrig: Boolean; LaAvisoRed, LaAvisoGre, LaAvisoBlu,
LaTempoI, LaTempoF, LaTempoT: TLabel; LBTabs: TListBox): Boolean;
var
  i, Resp, p: Integer;
  TabNome, TabBase, Item, TabA, TabB: String;
  TempoI, TempoF, TempoT: TTime;
begin
  TempoI := Time();
  if DataBase = Dmod.MyDB then FViuControle := 0 else FViuControle := 4;
  Result := True;
  //GravaAviso(Format(ivMsgEstruturaBD, [DataBase.DatabaseName]), Memo);
  MyObjects.Informa(LaAvisoBlu, False, Format(ivMsgEstruturaBD, [DataBase.DatabaseName]));
  //GravaAviso(FormatDateTime(ivFormatIni, Now), Memo);
  MyObjects.Informa(LaTempoI, False, FormatDateTime('hh:nn:ss:zzz', TempoI));
  try
    FTabelas := TStringList.Create;
    MyList.CriaListaTabelas(DataBase, FTabelas);
    try
      //Session.GetTableNames(mycoSkinTanX, mycoAsterisco, True, True, FArquivos);
      Dmod.QrMas.Close;
      Dmod.QrMas.Database := DataBase;
      Dmod.QrMas.SQL.Clear;
      Dmod.QrMas.SQL.Add('SHOW TABLES FROM '+database.databasename);
      Dmod.QrMas.Open;
      while not Dmod.QrMas.Eof do
      begin
        // Fazer Primeiro controle !!!
        if FViuControle = 0 then
        begin
          TabNome := 'controle';
          FViuControle := 1;
          try
            Dmod.QrControle.Close;
            Dmod.QrControle.Open;
          except
            TabNome := Dmod.QrMas.Fields[0].AsString;
          end;
        end else
        // Fazer depois ctrlgeral !!!
        (*
        if FViuControle = 1 then
        begin
          TabNome := 'ctrlgeral';
          FViuControle := 2;
          try
            Dmod.QrControle.Close;
            Dmod.QrControle.Open;
          except
            TabNome := Dmod.QrMas.Fields[0].AsString;
          end;
        end else
        *)
        if FViuControle = 2 then
        begin
          Dmod.QrMas.Prior;
          FViuControle := 3;
          TabNome := Dmod.QrMas.Fields[0].AsString;
        end else TabNome := Dmod.QrMas.Fields[0].AsString;


        //TabBase := TabNome;
        TabBase := '';
        //if Lowercase(TabNome) = 'stqmovitsb' then
          //ShowMessage(TabNome);
        for i := 0 to FTabelas.Count -1 do
        begin
          p := pos(';', FTabelas[i]);
          if p > 0 then
          begin
            TabA := Uppercase(Copy(FTabelas[i], 1, p - 1));
            TabB := Uppercase(TabNome);
            if TabA = TabB then
            begin
              TabBase := Copy(FTabelas[i], p + 1);
              Break;
            end;
          end;
        end;
        if TabBase = '' then
          TabBase := TabNome;


        //ChangeFileExt(FArquivos[i], myco_);
        if not TabelaAtiva(TabNome) then
        begin
          Item := Format(ivTabela_Nome, [TabNome]);
          if MyList.ExcluiTab then
          begin
            if not FPergunta then Resp := ID_YES else
            Resp := Geral.MensagemBox(Format(ivExclTabela,
            [Application.Title, TabNome]), PChar(ivTitAvis),
            MB_YESNOCANCEL+MB_ICONWARNING);
            if Resp = ID_YES then
            begin
              try
                // Ver lct
                if Lowercase(Copy(TabNome, 1, 4)) <> 'lct0' then
                  //GravaAviso(Item+ivMsgExcluidaSeria, Memo);
                  LBTabs.Items.Add(TabNome);
              except
                GravaAviso(Item+ivMsgERROExcluir, Memo);
              end;
            end else begin
              GravaAviso(Item+ivAbortExclUser, Memo);
              if Resp = ID_CANCEL then
              begin
                GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
                Exit;
              end;
            end;
          end;
        end else begin
          if VerificaCampos(DataBase, TabNome, TabBase, Memo, RecriaRegObrig) <> rvOK then
          begin
            GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
            Exit;
          end;
        end;
        Dmod.QrMas.Next;
      end;
      for i := 0 to FTabelas.Count -1 do
      begin
        p := pos(';', FTabelas[i]);
        if p = 0 then
        begin
          TabNome := FTabelas[i];
          TabBase := FTabelas[i];
        end else begin
          TabNome := Copy(FTabelas[i], 1, p - 1);
          TabBase := Copy(FTabelas[i], p + 1);
        end;
        if not TabelaExiste(TabNome) then
          CriaTabela(Database, TabNome, TabBase, Memo, actCreate);
      end;
      Dmod.QrMas.Close;
    finally
      FTabelas.Free;
    end;
    if DataBase = Dmod.MyDB then
    begin
      //VerificaRegistrosDePerfis(DataBase, Memo, True);
      VerificaRegistrosDeUserSets(DataBase, Memo, True);
      VerificaRegistrosDeImpDOS(DataBase, Memo, True);
      VerificaRegistrosDeCheque(DataBase, Memo, True);
      VerificaRegistrosDeJanelas(DataBase, Memo, True);
      MyList.VerificaOutrosDepois(DataBase, Memo);
    end;
    //GravaAviso(Format(ivMsgFimAnalBD, [Database.DatabaseName]), Memo);
    MyObjects.Informa(LaAvisoBlu, False, Format(ivMsgFimAnalBD, [Database.DatabaseName]));
    //GravaAviso(FormatDateTime(ivFormatFim, Now), Memo);
    TempoF := Time();
    MyObjects.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));
    TempoT := TempoF - TempoI;
    if TempoT < 0 then
      TempoT := TempoT + 1;
    MyObjects.Informa(LaTempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
  except
    //Geral.MensagemBox(Format(ivMsgFimAnalBD, [Database.DatabaseName]), PChar(ivTitErro), MB_OK+MB_ICONERROR);
    MyObjects.Informa(LaAvisoRed, False, Format(ivMsgFimAnalBD, [Database.DatabaseName]));
    raise;
  end;
end;

procedure TMyDBCheck.VerificaOutrosDepois(DataBase: String;
  Memo: TMemo);
begin
  if DataBase.DatabaseName = TMeuDB then
  begin
    // Verifica tabela de Clientes Internos
    VerificaEntiCliInt();
  end;
end;

procedure TMyDBCheck.VerificaEntiCliInt();
var
  CodEnti, CodFilial, CodCliInt: Integer;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.Database := Dmod.MyDB;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW TABLES FROM ' + DMod.MyDB.DatabaseName);
  Dmod.QrAux.SQL.Add('LIKE "enticliint"');
  Dmod.QrAux.Open;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT ent.Codigo, ent.CliInt, ent.Filial,');
    Dmod.QrAux.SQL.Add('eci.CodCliInt, eci.CodEnti, eci.CodFilial,');
    Dmod.QrAux.SQL.Add('IF(eci.CodCliInt IS NULL, 0, 1) Existe');
    Dmod.QrAux.SQL.Add('FROM entidades ent');
    Dmod.QrAux.SQL.Add('LEFT JOIN enticliint eci ON eci.CodCliInt=ent.CliInt');
    Dmod.QrAux.SQL.Add('WHERE ent.CliInt <> 0');
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.First;
      while not Dmod.QrAux.Eof do
      begin
        CodEnti   := Dmod.QrAux.FieldByName('Codigo').AsInteger;
        CodCliInt := Dmod.QrAux.FieldByName('CliInt').AsInteger;
        CodFilial := Dmod.QrAux.FieldByName('Filial').AsInteger;
        if Dmod.QrAux.FieldByName('Existe').AsInteger = 0 then
        begin
          // Criar pois n�o existe
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticliint', False, [
          'CodEnti', 'CodFilial'], ['CodCliInt'], [
          CodEnti, CodFilial], [CodCliInt], True);
        end else begin
          if Dmod.QrAux.FieldByName('Filial').AsInteger
          <> Dmod.QrAux.FieldByName('CodFilial').AsInteger then
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
            'CodFilial'], ['CodCliInt'], [
            CodFilial], [CodCliInt], True);
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
end;

function TMyDBCheck.VerificaEstrutLoc(DataBase: String; Memo: TMemo;
LaAvisoRed, LaTempoF, LaTempoT: TLabel): Boolean;
var
  i: Integer;
  Cria, termina: Boolean;
  TempoF: TTime;
begin
  Result := True;
  //GravaAviso(ivMsgEstruturaBDLoc, Memo);
  //GravaAviso(FormatDateTime(ivFormatIni, Now), Memo);
  try
    FTabLoc := TStringList.Create;
    MyList.CriaListaTabelasLocais(FTabLoc);
    try
      for i := 0 to FTabLoc.Count -1 do
      begin
        Cria    := False;
        Termina := False;
        if FPergunta then
        begin
          case Geral.MensagemBox('Deseja excluir e recriar a tabela '+
          'local "'+FTabLoc[i]+'"?', PChar(VAR_APPNAME),
          MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+ MB_APPLMODAL) of
            ID_YES: Cria := True;
            ID_Cancel: Termina := True;
          end;
          if Termina then Exit;
        end else Cria := True;
        if Cria then
        begin
          GravaAviso(Format(ivMsgRecriandoTabLoc, [FTabLoc[i]]), Memo);
          Ucriar.RecriaTempTable(FTabLoc[i], DModG.QrUpdPID1, False);
          Ucriar.RecriaTabelaLocal(FTabLoc[i], 1);
        end;
      end;
    finally
      FTabLoc.Free;
    end;
    GravaAviso(ivMsgFimAnalBDLoc, Memo);
    //GravaAviso(FormatDateTime(ivFormatFim, Now), Memo);
    TempoF := Time();
    MyObjects.Informa(LaTempoF, False, FormatDateTime('hh:nn:ss:zzz', TempoF));
(*
    TempoT := TempoF - TempoI;
    if TempoT < 0 then
      TempoT := TempoT + 1;
    MyObjects.Informa(LaTempoT, False, FormatDateTime('hh:nn:ss:zzz', TempoT));
*)
  except
    MyObjects.Informa(LaAvisoRed, False, Format(ivMsgFimAnalBD, [Database.DatabaseName]));
    //Geral.MensagemBox(ivMsgFimAnalBDLoc, PChar(ivTitErro),
      //MB_OK+MB_ICONERROR);
    raise;
  end;
end;

function TMyDBCheck.VerificaCampos(DataBase: String; TabelaNome, TabelaBase: String;
  Memo: TMemo; RecriaRegObrig: Boolean): TResultVerify;
var
  i, k, Resp, EhIdx: Integer;
  Opcoes, Item, Nome, IdxNome: String;
  Indices: TStringList;
  IdxExiste: Boolean;
begin
  Result := rvOK;
  try
    //if Uppercase(TabelaNome) = 'LANCTOZ' then
      //ShowMessage(TabelaNome);
    Dmod.QrNTV.Close;
    Dmod.QrNTV.Database := Database;
    Dmod.QrNTV.SQL.Clear;
    Dmod.QrNTV.SQL.Add('SHOW FIELDS FROM ' + lowercase(TabelaNome));
    UMyMod.AbreQuery(Dmod.QrNTV, 'TMyDBCheck.VerificaCampos() > Dmod.QrNTV');
    try
      FLCampos  := TList.Create;
      FLIndices := TList.Create;
      try
        FCriarAtivoAlterWeb := True;
        MyList.CriaListaCampos(TabelaBase, FLCampos);
        //
        if Uppercase(TabelaNome) = Uppercase('lanctoz') then
        begin
          New(FRCampos);
          FRCampos.Field      := 'UserDel';
          FRCampos.Tipo       := 'int(11)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := 'DataDel';
          FRCampos.Tipo       := 'datetime';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '0000-00-00 00:00:00';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
        end;
        if FCriarAtivoAlterWeb then
        begin
          New(FRCampos);
          FRCampos.Field      := mycoCampoAlterWeb;
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
          New(FRCampos);
          FRCampos.Field      := mycoCampoAtivo;
          FRCampos.Tipo       := 'tinyint(1)';
          FRCampos.Null       := '';
          FRCampos.Key        := '';
          FRCampos.Default    := '1';
          FRCampos.Extra      := '';
          FLCampos.Add(FRCampos);
          //
        end;
        MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
        while not Dmod.QrNTV.Eof do
        begin
          Nome := Dmod.QrNTV.FieldByName('Field').AsString;
          Item := Format(ivCampo_Nome, [TabelaNome, Nome]);
          //if Lowercase(TabelaNome) = 'stqmovitsb' then
            //ShowMessage(TabelaNome);
          case CampoAtivo(DataBase, TabelaNome, TabelaBase, Nome, Memo) of
            1:
            begin
              try
                Opcoes := FRCampos.Tipo;
                if FRCampos.Null <> 'YES' then
                  Opcoes := Opcoes + ' NOT NULL ';
                if FRCampos.Default <> myco_ then
                  Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                  FRCampos.Default + mycoAspasDuplas;
                if FRCampos.Extra <> myco_ then
                begin
                  if Uppercase(FRCampos.Extra) = Uppercase('auto_increment')
                  then Opcoes := Opcoes + FRCampos.Extra;
                end;
                Dmod.QrSQL.Close;
                Dmod.QrSQL.DataBase := DataBase;
                Dmod.QrSQL.SQL.Clear;
                Dmod.QrSQL.SQL.Add(mycoALTERTABLE+TabelaNome+mycoCHANGE+
                  Nome+mycoEspaco+Nome+mycoEspaco+Opcoes);
                if ((not FPergunta) and (not MyList.ExcluiReg))
                then Resp := ID_YES else
                Resp := Geral.MensagemBox('O campo '+Nome+
                  ' da tabela '+TabelaNome+' difere do esperado e ser� regularizado.'
                  +Chr(13)+Chr(10)+'Confirma a altera��o?',
                  'Altera��o de Campo de Tabela de Banco de Dados',
                   MB_YESNOCANCEL+MB_ICONQUESTION);
                if Resp = ID_YES then
                begin
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  GravaAviso(Item+ivAlterado, Memo);
                end else begin
                  GravaAviso(Item+ivAbortAlteUser, Memo);
                  if Resp = ID_CANCEL then
                  begin
                    Result := rvAbort;
                    Exit;
                  end;
                end;
              except
                GravaAviso(Item+ivMsgERROAlterar, Memo);
                raise;
              end;
            end;
            9:
            begin
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MensagemBox(Format(ivExclCampo,
                [Application.Title, Nome, TabelaNome]),
                PChar(ivTitAvis), MB_YESNOCANCEL+MB_ICONWARNING);
              if Resp = ID_YES then
              begin
                try
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.DataBase := DataBase;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add(mycoALTERTABLE+TabelaNome+mycoDROP+
                    Nome);
                  if MyList.ExcluiReg then
                  begin
                    if not FPergunta then Resp := ID_YES else
                    Resp := Geral.MensagemBox('Confirma a EXCLUS�O do campo '+
                    Nome+'?', 'Exclus�o de campo de tabela', MB_YESNOCANCEL+
                    MB_ICONQUESTION);
                    if Resp = ID_YES then
                    begin
                      Dmod.QrSQL.ExecSQL;
                      GravaAviso(Item+ivMsgExcluida, Memo);
                    end else begin
                      GravaAviso(Item+ivAbortExclUser, Memo);
                      if Resp = ID_CANCEL then
                      begin
                        Result := rvAbort;
                        Exit;
                      end;
                    end;
                  end;
                except
                  GravaAviso(Item+ivMsgERROexcluir, Memo);
                  raise;
                end;
              end else begin
                GravaAviso(Item+ivAbortExclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  Result := rvAbort;
                  Exit;
                end;
              end;
            end;
          end;
          Dmod.QrNTV.Next;
        end;
        for i:= 0 to FLCampos.Count - 1 do
        begin
          FRCampos := FLCampos[i];
          if CampoExiste(TabelaNome, FRCampos.Field, Memo) = 2 then
          begin
            Item := Format(ivCampo_Nome, [TabelaNome, FRCampos.Field]);
            try
              Opcoes := FRCampos.Tipo;
              if FRCampos.Null <> 'YES' then
                Opcoes := Opcoes + ' NOT NULL ';
              if FRCampos.Default <> myco_ then
                Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
                FRCampos.Default + mycoAspasDuplas;
              Dmod.QrSQL.Close;
              Dmod.QrSQL.DataBase := DataBase;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add(mycoALTERTABLE+TabelaNome+mycoADD+
              FRCampos.Field+mycoEspaco+Opcoes);
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MensagemBox('O campo '+FRCampos.Field+
              ' n�o existe na tabela '+TabelaNome+' e ser� criado.'+Chr(13)+
              Chr(10)+'Confirma a cria��o do campo?',
              'Cria��o de Campo de Tabela de Banco de Dados',
              MB_YESNOCANCEL+MB_ICONQUESTION);
              if Resp = ID_YES then
              begin
                UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                Dmod.QrSQL.ExecSQL;
                GravaAviso(Item+ivcriado, Memo);
              end else begin
                GravaAviso(Item+ivAbortInclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  Result := rvAbort;
                  GravaAviso(Item+ivAbortInclUser, Memo);
                  //
                  Exit;
                end;
              end;
            except
              GravaAviso(Item+ivMsgERROCriar, Memo);
              raise;
            end;
          end;
        end;
        Indices := TStringList.Create;
        Indices.Sorted := True;
        Indices.Duplicates := (dupIgnore);
        try
          for k := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[k];
            Indices.Add(FRIndices.Key_name);
          end;
          Dmod.QrIdx.Close;
          Dmod.QrIdx.Database := DataBase;
          Dmod.QrIdx.SQL.Clear;
          Dmod.QrIdx.SQL.Add('SHOW KEYS FROM ' + LowerCase(TabelaNome));
          UMyMod.AbreQuery(Dmod.QrIdx, 'TMyDBCheck.VerificaCampos()');
          while not Dmod.QrIdx.Eof do
          begin
            IdxExiste := False;
            IdxNome := Dmod.QrIdx.FieldByName('Key_name').AsString;
            //if (Uppercase(Tabela) =  'ENTIDADES') and (Uppercase(IdxNome) =  'UNIQUE1') then
              //ShowMessage('ENTIDADES.UNIQUE1');
            Item := Format(ivIndice_Nome, [TabelaNome, IdxNome]);
            for k := 0 to Indices.Count -1 do
              if Indices[k] = IdxNome then
              IdxExiste := True;
            if not IdxExiste then
            begin
              if not FPergunta then Resp := ID_YES else
              Resp := Geral.MensagemBox(Format(ivExclIndice,
                [Application.Title, IdxNome, TabelaNome]),
                PChar(ivTitAvis), MB_YESNOCANCEL+MB_ICONWARNING);
              if Resp = ID_YES then
              begin
                if ExcluiIndice(Database, TabelaNome, IdxNome, Item, mymotSemRef, Memo) <>
                   rvOK then Result := rvAbort;
              end else begin
                GravaAviso(Item+ivAbortExclUser, Memo);
                if Resp = ID_CANCEL then
                begin
                  Result := rvAbort;
                  Exit;
                end;
              end;
            end;
            Dmod.QrIdx.Next;
          end;
          Dmod.QrIdx.Close;
          for k := 0 to Indices.Count -1 do
          begin
            EhIdx := IndiceExiste(Database, TabelaNome, Indices[k], Memo);
            if EhIdx in ([2,3]) then
              if RecriaIndice(Database, TabelaNome, Indices[k], Item, Memo) <> rvOK
              then begin
                Result := rvAbort;
                Exit;
              end;
            if EhIdx in ([0]) then
            begin
              if CriaIndice(Database, TabelaNome, Indices[k], Item, Memo) <> rvOK then
              begin
                Result := rvAbort;
                //Exit;
              end;
            end;
          end;
        finally
          Indices.Free;
        end;
        ////
        VerificaRegistrosObrigatorios_Inclui(Database, TabelaNome, TabelaBase, Memo, True, RecriaRegObrig);
        VerificaRegistrosDeArquivo(Database, TabelaNome, TabelaBase, Memo, RecriaRegObrig);
      finally
        FLCampos.Free;
        FLIndices.Free;
      end;
    finally
      Dmod.QrNTV.Close;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosObrigatorios_Inclui(DataBase: String;
  TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean): TResultVerify;
var
  i, j, n, OK, z: Integer;
  ListaCampos, ListaValores: TStringList;
  Linha, Campo, Valor, Item, Virgula, LisSQL: String;
  Criar: Boolean;
begin
  Result := rvOK;
  try
    FListaSQL := TStringList.Create;
    try
      MyList.CriaListaSQL(TabelaBase, FListaSQL);
      MyList.CompletaListaSQL(TabelaBase, FListaSQL);
      if FListaSQL.Count > 1 then
      begin
        ListaCampos := TStringList.Create;
        try
          j := 0;
          Linha :=  FListaSQL[0];
          while j < (Length(Linha)) do
          begin
            OK := 0;
            Campo := '';
            while OK < 1 do
            begin
              j := j + 1;
              if j> Length(Linha) then Break;
              if Linha[j] = CO_APOSTROFO then OK := OK - 1
              //else if Linha[j] = ',' then OK := OK + 1
              else if Linha[j] = Char(124) (* separador = "|" *) then
                OK := OK + 1
              else
                Campo := Campo + Linha[j];
            end;
            ListaCampos.Add(Campo);
          end;
          ListaValores := TStringList.Create;
          try
            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) (* separador = "|" *) then
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              Campo := ListaCampos[0];
              Valor := ListaValores[0];
              Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);
              Dmod.QrNTV.Close;
              Dmod.QrNTV.Database := Database;
              Dmod.QrNTV.SQL.Clear;
              Dmod.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
              Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
              try
                UMyMod.AbreQuery(Dmod.QrNTV, 'TMyDBCheck.VerificaRegistrosObrigatorios_Inclui()');
              except
                GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo);
                raise;
              end;
              //Criar := False;
              if GOTOy.Registros(Dmod.QrNTV) <> 0 then
              begin
                if (Recria and
                // Evitar destruir dados pois tabela tem
                // registro obrigatorio modificado p�s inclu�o
                (Uppercase(TabelaNome) <> Uppercase('controle')) and
                (Uppercase(TabelaNome) <> Uppercase('master')) and
                (Uppercase(TabelaNome) <> Uppercase('entidades')) and
                (Uppercase(TabelaNome) <> Uppercase('senhas')))
                //or ( (Uppercase(TabelaNome) = Uppercase('entidades')) and (Valor = '0'))
                then
                begin
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrNTV.Close;
                  Dmod.QrNTV.Database := Database;
                  Dmod.QrNTV.SQL.Clear;
                  Dmod.QrNTV.SQL.Add(DELETE_FROM + Lowercase(TabelaNome));
                  Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
                  Dmod.QrNTV.ExecSQL;
                  Criar := True;
                  GravaAviso(Item+'foi exclu�do para ser recriado.', Memo);
                end else Criar := False;
              end else Criar := True;
              if Criar then
              begin
                if Avisa and (GOTOy.Registros(Dmod.QrNTV) = 0) then
                  GravaAviso(Item+'n�o existe.', Memo);
                try
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.Database := Database;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrSQL.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  if avisa then GravaAviso(Item+'inclu�do.', Memo);
                except
                  LisSQL := 'Lista de valores:' + #13#10;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + #13#10;
                  GravaAviso(LisSQL + Dmod.QrSQL.SQL.Text + #13#10 + Item+ivMsgERROIncluir, Memo);
                  //raise;
                end;
              end;
            end;
          finally
            ListaValores.Free;
          end;
        finally
          ListaCampos.Free;
        end;
      end;
    finally
      FListaSQL.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosObrigatorios_Corrige(DataBase: String;
  TabelaNome, TabelaBase: String; Memo: TMemo; Avisa, Recria: Boolean): TResultVerify;
var
  i, j, n, OK, z: Integer;
  ListaCampos, ListaValores: TStringList;
  Linha, Campo, Valor, Item, Virgula, LisSQL: String;
  Criar: Boolean;
begin
  Result := rvOK;
  try
    FListaSQL := TStringList.Create;
    try
      MyList.CriaListaSQL(TabelaBase, FListaSQL);
      MyList.CompletaListaSQL(TabelaBase, FListaSQL);
      if FListaSQL.Count > 1 then
      begin
        ListaCampos := TStringList.Create;
        try
          j := 0;
          Linha :=  FListaSQL[0];
          while j < (Length(Linha)) do
          begin
            OK := 0;
            Campo := '';
            while OK < 1 do
            begin
              j := j + 1;
              if j> Length(Linha) then Break;
              if Linha[j] = CO_APOSTROFO then OK := OK - 1
              //else if Linha[j] = ',' then OK := OK + 1
              else if Linha[j] = Char(124) (* separador = "|" *) then
                OK := OK + 1
              else
                Campo := Campo + Linha[j];
            end;
            ListaCampos.Add(Campo);
          end;
          ListaValores := TStringList.Create;
          try
            for i := 1 to FListaSQL.Count-1 do
            begin
              ListaValores.Clear;
              j := 0;
              Linha := FListaSQL[i];
              while j < Length(Linha) do
              begin
                OK := 0;
                Valor := '';
                while OK < 1 do
                begin
                  j := j + 1;
                  if j > Length(Linha) then Break;
                  if Linha[j] = CO_APOSTROFO then OK := OK - 1
                  //else if Linha[j] = ',' then OK := OK + 1
                  else if Linha[j] = Char(124) (* separador = "|" *) then
                    OK := OK + 1
                  else Valor := Valor + Linha[j];
                end;
                ListaValores.Add(Valor);
              end;
              Campo := ListaCampos[0];
              Valor := ListaValores[0];
              Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);
              Dmod.QrNTV.Close;
              Dmod.QrNTV.Database := Database;
              Dmod.QrNTV.SQL.Clear;
              Dmod.QrNTV.SQL.Add('SELECT * FROM ' + Lowercase(TabelaNome));
              Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
              try
                UMyMod.AbreQuery(Dmod.QrNTV, 'TMyDBCheck.VerificaRegistrosObrigatorios_Corrige()');
              except
                GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo, False);
                raise;
              end;
              //Criar := False;
              if GOTOy.Registros(Dmod.QrNTV) <> 0 then
              begin
                if (Recria and
                // Evitar destruir dados pois tabela tem
                // registro obrigatorio modificado p�s inclu�o
                (Uppercase(TabelaNome) <> Uppercase('controle')) and
                (Uppercase(TabelaNome) <> Uppercase('master')) and
                (Uppercase(TabelaNome) <> Uppercase('entidades')) and
                (Uppercase(TabelaNome) <> Uppercase('senhas')))
                //or ( (Uppercase(TabelaNome) = Uppercase('entidades')) and (Valor = '0'))
                then
                begin
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrNTV.Close;
                  Dmod.QrNTV.Database := Database;
                  Dmod.QrNTV.SQL.Clear;
                  Dmod.QrNTV.SQL.Add(DELETE_FROM + Lowercase(TabelaNome));
                  Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ='+Valor);
                  Dmod.QrNTV.ExecSQL;
                  Criar := True;
                  GravaAviso(Item+'foi exclu�do para ser recriado.', Memo, False);
                end else Criar := False;
              end else Criar := True;
              if Criar then
              begin
                if Avisa and (GOTOy.Registros(Dmod.QrNTV) = 0) then
                  GravaAviso(Item+'n�o existe.', Memo, False);
                try
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.Database := Database;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 0 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrSQL.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  if avisa then GravaAviso(Item+'inclu�do.', Memo, False);
                except
                  LisSQL := 'Lista de valores:' + #13#10;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + #13#10;
                  GravaAviso(LisSQL + Dmod.QrSQL.SQL.Text + #13#10 + Item+ivMsgERROIncluir, Memo, False);
                  //raise;
                end;
              end else
              begin
                if Avisa and (GOTOy.Registros(Dmod.QrNTV) = 0) then
                  GravaAviso(Item+'ser� corrigido.', Memo, False);
                try
                  //if (Uppercase(TabelaNome) = Uppercase('entidades')) then
                    //ShowMessage('Aqui: registros obrigat�rios de entidades!');
                  Dmod.QrSQL.Close;
                  Dmod.QrSQL.Database := Database;
                  Dmod.QrSQL.SQL.Clear;
                  Dmod.QrSQL.SQL.Add('UPDATE ' + Lowercase(TabelaNome) + ' SET ');
                  for n := 1 to ListaCampos.Count-1 do
                  begin
                    if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                    Dmod.QrSQL.SQL.Add(ListaCampos[n]+'='+ListaValores[n]+ Virgula);
                  end;
                  Dmod.QrSQL.SQL.Add(#13#10 + 'WHERE ' + ListaCampos[0]+'='+ListaValores[0]);
                  UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
                  Dmod.QrSQL.ExecSQL;
                  if avisa then GravaAviso(Item+'alterado.', Memo, False);
                except
                  dmkPF.LeMeuTexto(Dmod.QrSQL.Text);
                  LisSQL := 'Lista de valores:' + #13#10;
                  for z := 0 to FListaSQL.Count-1 do
                    LisSQL := LisSQL + FListaSQL[z] + #13#10;
                  GravaAviso(LisSQL + Dmod.QrSQL.SQL.Text + #13#10 + Item+ivMsgERROIncluir, Memo, False);
                  //raise;
                end;
              end;
            end;
          finally
            ListaValores.Free;
          end;
        finally
          ListaCampos.Free;
        end;
      end;
    finally
      FListaSQL.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosDeUserSets(DataBase: String;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i, Posi: Integer;
  Item: String;
  Nome, Descricao: String;
begin
  Result := rvOK;
  try
    FUserSets := TStringList.Create;
    try
      MyList.CriaListaUserSets(FUserSets);
      if FUserSets.Count > 1 then
      begin
        Dmod.QrSQL.Close;
        Dmod.QrSQL.Database := Database;
        Dmod.QrSQL.SQL.Clear;
        Dmod.QrSQL.SQL.Add('UPDATE usersets SET Ativo=0');
        Dmod.QrSQL.ExecSQL;
        for i := 0 to FUserSets.Count-1 do
        begin
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM usersets');
          Dmod.QrNTV.SQL.Add('WHERE NomeForm=:P0 AND Definicao=:P1');
          Posi := pos(';', FUserSets[i]);
          if Posi > 0 then
          begin
            Nome := Copy(FUserSets[i], 1, Posi-1);
            Descricao := Copy(FUserSets[i], Posi+1, Length(FUserSets[i])-Posi);
          end else begin
            Nome := FUserSets[i];
            Descricao := '';
          end;
          Dmod.QrNTV.Params[0].AsString := Nome;
          Dmod.QrNTV.Params[1].AsString := Descricao;
          try
            Dmod.QrNTV.Open;
          except
            GravaAviso('N�o � poss�vel verificar se existe o campo "' + Nome +
            '" na tabela "UserSets"', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+
            '" n�o existe na tabela "UserSets".', Memo);
            try
              Dmod.QrSQL.Close;
              Dmod.QrSQL.Database := Database;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO usersets SET');
              Dmod.QrSQL.SQL.Add('NomeForm = :P0, Definicao=:P1');
              Dmod.QrSQL.Params[0].AsString := Nome;
              Dmod.QrSQL.Params[1].AsString := Descricao;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+'" foi inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end else begin
            Dmod.QrSQL.Close;
            Dmod.QrSQL.Database := Database;
            Dmod.QrSQL.SQL.Clear;
            Dmod.QrSQL.SQL.Add('UPDATE usersets SET Ativo=1');
            Dmod.QrSQL.SQL.Add('WHERE NomeForm = :P0 AND Definicao=:P1');
            Dmod.QrSQL.Params[0].AsString := Nome;
            Dmod.QrSQL.Params[1].AsString := Descricao;
            Dmod.QrSQL.ExecSQL;
          end;
        end;
        Dmod.QrSQL.Close;
        Dmod.QrSQL.Database := Database;
        Dmod.QrSQL.SQL.Clear;
        Dmod.QrSQL.SQL.Add('DELETE FROM usersets WHERE Ativo=0');
        Dmod.QrSQL.ExecSQL;
      end;
    finally
      FUserSets.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosDeCheque(DataBase: String;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i, Posi: Integer;
  Item: String;
  Nome, Descricao: String;
begin
  Result := rvOK;
  try
    FCheque := TStringList.Create;
    try
      CashTb.CriaListaCheque(FCheque);
      if FCheque.Count > 1 then
      begin
        for i := 0 to FCheque.Count-1 do
        begin
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM chconfits');
          Dmod.QrNTV.SQL.Add('WHERE Campo=:P0 AND Nome=:P1');
          Posi := pos(';', FCheque[i]);
          if Posi > 0 then
          begin
            Nome := Copy(FCheque[i], 1, Posi-1);
            Descricao := Copy(FCheque[i], Posi+1, Length(FCheque[i])-Posi);
          end else begin
            Nome := FCheque[i];
            Descricao := '';
          end;
          Dmod.QrNTV.Params[0].AsString := Nome;
          Dmod.QrNTV.Params[1].AsString := Descricao;
          try
            Dmod.QrNTV.Open;
          except
            GravaAviso('N�o � poss�vel verificar se existe o campo '+Nome+
            ' na tabela "chconfits"', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+
            '" n�o existe na tabela "chconfits".', Memo);
            try
              Dmod.QrSQL.Close;
              Dmod.QrSQL.Database := Database;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO chconfits SET');
              Dmod.QrSQL.SQL.Add('Campo = :P0, Nome=:P1');
              Dmod.QrSQL.Params[0].AsString := Nome;
              Dmod.QrSQL.Params[1].AsString := Descricao;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso('O campo "'+Nome+'.'+Descricao+'" foi inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end;
        end;
      end;
    finally
      FCheque.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosDeImpDOS(DataBase: String;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i, j, k, Tam: Integer;
  Item: String;
  //Posi: MyArrayI02;
  Codigo, Tabela, Descricao: String;
begin
  Result := rvOK;
  try
    FImpDOS := TStringList.Create;
    try
      MyList.CriaListaImpDOS(FImpDOS);
      if FImpDOS.Count > 1 then
      begin
        for i := 0 to FImpDOS.Count-1 do
        begin
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM impdosits');
          Dmod.QrNTV.SQL.Add('WHERE Codigo=:P0 AND Tabela=:P1 AND Descricao=:P2');
          //Posi1 := pos(';', FImpDOS[i]);
          Tam := Length(FImpDOS[i]);
          k := 1;
          Codigo    := '';
          Tabela    := '';
          Descricao := '';
          for j := 1 to Tam do
          begin
            if FImpDos[i][j] = ';' then k := k + 1
            else begin
              case k of
                1: Codigo    := Codigo    + FImpDos[i][j];
                2: Tabela    := Tabela    + FImpDos[i][j];
                3: Descricao := Descricao + FImpDos[i][j];
              end;
            end;
          end;
          Dmod.QrNTV.Params[0].AsString := Codigo;
          Dmod.QrNTV.Params[1].AsString := Tabela;
          Dmod.QrNTV.Params[2].AsString := Descricao;
          try
            Dmod.QrNTV.Open;
          except
            GravaAviso('N�o � poss�vel verificar se existe o campo "'+Descricao+
            '" na tabela "impdosits"', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso('O campo "'+Descricao+
            '" n�o existe na tabela "ImpDOS".', Memo);
            try
              Dmod.QrSQL.Close;
              Dmod.QrSQL.Database := Database;
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('DELETE FROM impdosits ');
              Dmod.QrSQL.SQL.Add('WHERE Codigo = :P0');
              Dmod.QrSQL.Params[0].AsString := Codigo;
              Dmod.QrSQL.ExecSQL;
              //
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO impdosits SET');
              Dmod.QrSQL.SQL.Add('Codigo = :P0, Tabela=:P1, Descricao=:P2');
              Dmod.QrSQL.Params[0].AsString := Codigo;
              Dmod.QrSQL.Params[1].AsString := Tabela;
              Dmod.QrSQL.Params[2].AsString := Descricao;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso('O campo "'+Descricao+'" foi inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end;
        end;
      end;
    finally
      FImpDOS.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosDeArquivo(DataBase: String;
 TabelaNome, TabelaBase: String; Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i, j, n: Integer;
  ListaCampos, ListaValores: TStringList;
  Campo, Valor, Item, Virgula: String;
  F: TextFile;
  S: string;
  Name: String;
  Lista: TStringList;
  Campos, Registros: Integer;
begin
  Result := rvOK;
  Name := 'C:\Dermatek\Listas\'+TabelaNome+'.myl';
  if not FileExists(Name) then Exit;
  AssignFile(F, Name);
  Reset(F);
  Lista := TStringList.Create;
  while not Eof(F) do
  begin
    Readln(F, S);
    Lista.Add(S);
  end;
  CloseFile(F);
  if Uppercase(Lista[0]) <> '>>LISTA_TABELA_DERMATEK' then Exit;
  try
    Campos := 0;
    i := 1;
    ListaCampos := TStringList.Create;
    try
      ListaValores := TStringList.Create;
      try
        while not (Copy(Lista[i], 1,2) = '<<') do
        begin
          if Uppercase(Copy(Lista[i], 1,5)) = 'CAMPO' then
          begin
            Campos := Campos+1;
            ListaCampos.Add(Copy(Lista[i], 7, Length(Lista[i])-6));
          end;
          i := i + 1;
        end;
        for i := i+1 to Lista.Count-1 do ListaValores.Add(Lista[i]);
        Registros := Trunc(ListaValores.Count / Campos);
        //
        for j := 0 to Registros-1 do
        begin
          Campo := ListaCampos[0];
          Valor := ListaValores[j*Campos];
          Item := Format(ivValorCampo_Nome, [TabelaNome, Valor]);
          Dmod.QrNTV.Close;
          Dmod.QrNTV.Database := Database;
          Dmod.QrNTV.SQL.Clear;
          Dmod.QrNTV.SQL.Add('SELECT * FROM ' + lowercase(TabelaNome));
          Dmod.QrNTV.SQL.Add('WHERE '+Campo+' ="'+Valor+'"');
          try
            Dmod.QrNTV.Open;
          except
            GravaAviso(Item+'n�o � poss�vel verificar se existe.', Memo);
            raise;
          end;
          if GOTOy.Registros(Dmod.QrNTV) = 0 then
          begin
            if Avisa then GravaAviso(Item+'n�o existe.', Memo);
            try
              Dmod.QrSQL.SQL.Clear;
              Dmod.QrSQL.SQL.Add('INSERT INTO ' + Lowercase(TabelaNome) + ' SET ');
              for n := 0 to ListaCampos.Count-1 do
              begin
                if n < ListaCampos.Count-1 then Virgula := ',' else Virgula := '';
                Dmod.QrSQL.SQL.Add(ListaCampos[n]+'="'+ListaValores[(j*Campos)+n]+ '"'+Virgula);
              end;
              UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
              Dmod.QrSQL.ExecSQL;
              if avisa then GravaAviso(Item+'inclu�do.', Memo);
            except
              GravaAviso(Item+ivMsgERROIncluir, Memo);
              raise;
            end;
          end;
        end;
        //
      finally
        ListaValores.Free;
      end;
    finally
      ListaCampos.Free;
    end;
  except
    //Result := rvErr;
    raise;
  end;
end;

function TMyDBCheck.VerificaRegistrosDeJanelas(DataBase: String;
  Memo: TMemo; Avisa: Boolean): TResultVerify;
var
  i: Integer;
begin
  Result := rvErr;
  FLJanelas := TList.Create;
  try
    MyList.CriaListaJanelas(FLJanelas);
    CashTb.CompletaListaFRJanelasCashier(FRJanelas, FLJanelas);
    for i:= 0 to FLJanelas.Count - 1 do
    begin
      FRJanelas := FLJanelas[i];

      Dmod.QrNTV.Close;
      Dmod.QrNTV.Database := Database;
      Dmod.QrNTV.SQL.Clear;
      Dmod.QrNTV.SQL.Add('SELECT Janela, Nome, Descricao');
      Dmod.QrNTV.SQL.Add('FROM perfjan');
      Dmod.QrNTV.SQL.Add('WHERE Janela=:P0');
      Dmod.QrNTV.Params[0].AsString := FRJanelas.ID;
      Dmod.QrNTV.Open;
      if Dmod.QrNTV.RecordCount = 0 then
      begin
        GravaAviso('Ser� adicionado o campo "' +
          FRJanelas.ID + '" na tabela perfjan.', Memo);
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO perfjan SET ');
        Dmod.QrUpd.SQL.Add('Janela=:P0, Nome=:P1, Descricao=:P2');
        Dmod.QrUpd.Params[00].AsString := FRJanelas.ID;
        Dmod.QrUpd.Params[01].AsString := FRJanelas.Nome;
        Dmod.QrUpd.Params[02].AsString := FRJanelas.Descricao;
        Dmod.QrUpd.ExecSQL;
        GravaAviso('Registro "' + FRJanelas.ID + '" adicionado com sucesso.', Memo);
        Result := rvOk;
      end;
    end;
  finally
    FLJanelas.Free;
  end;
end;

function TMyDBCheck.ExcluiIndice(DataBase: String; Tabela, IdxNome,
 Aviso: String; Motivo: TMyMotivo; Memo: TMemo): TResultVerify;
var
  Resp: Integer;
  Txt: String;
begin
  if MyList.ExcluiIdx then
  begin
    Result := rvErr;
    case Motivo of
      mymotDifere: Txt := Format('O �ndice %s da Tabela %s difere do esperado e '+
      'deve ser excluido.', [IdxNome, Tabela]);
      mymotSemRef: Txt := Format('N�o h� refer�ncia ao �ndice %s da Tabela %s, '+
      'que deve ser excluido.', [IdxNome, Tabela]);
    end;
    if not FPergunta then Resp := ID_YES else
    Resp := Geral.MensagemBox(Txt+Chr(13)+Chr(10)+
            'Confirma a exclus�o do �ndice ?',
            PChar(ivTitPerg), MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      try
        Dmod.QrSQL.Close;
        Dmod.QrSQL.DataBase := DataBase;
        Dmod.QrSQL.SQL.Clear;
        if Uppercase(IdxNome) = Uppercase('PRIMARY') then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY')
        else
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome);
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
        Dmod.QrSQL.ExecSQL;
        Result := rvOK;
        GravaAviso(Aviso+ivMsgExcluido, Memo);
      except;
        //Result := rvErr;
        GravaAviso(Aviso+ivMsgERROExcluir, Memo);
        raise;
      end;
    end else begin
      GravaAviso(Aviso+ivAbortExclUser, Memo);
      if Resp = ID_CANCEL then
      begin
        Result := rvAbort;
        Exit;
      end;
    end;
  end else Result := rvOK;
end;

function TMyDBCheck.ExcluiRegistro(QrExecSQL, QrDados: T m y S Q L Query;
TabelaTarget: String; CamposSource, CamposTarget: array of String;
ReopenQrDados: Boolean; Pergunta: String): Boolean;
var
  Continua, i, j: Integer;
  Valor: String;
begin
  Result := False;
  if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(TabelaTarget) then
    Exit;
  if Trim(Pergunta) = '' then
    Continua := ID_YES
  else
    Continua := Geral.MensagemBox(Pergunta, 'Confirma��o de exclus�o',
    MB_YESNOCANCEL+MB_ICONQUESTION);
  //
  if Continua = ID_YES then
  begin
    QrExecSQL.SQL.Clear;
    QrExecSQL.SQL.Add(DELETE_FROM  + Lowercase(TabelaTarget) + ' WHERE ');
    //
    j := High(CamposSource);
    for i := Low(CamposSource) to j do
    begin
      Valor := Geral.VariavelToString(QrDados.FieldByName(CamposSource[i]).Value);
      if i < j then
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor + ' AND ')
      else
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor);
    end;
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
      QrExecSQL.ExecSQL;
      Result := True;
      if ReopenQrDados then
      begin
        QrDados.Close;
        QrDados.Open;
      end;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
    end;
  end;
end;

function TMyDBCheck.CriaIndice(DataBase: String; Tabela, IdxNome, Aviso: String; Memo: TMemo):
  TResultVerify;
var
  i, j, K: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
begin
  Result := rvOK;
  if not FPergunta then Resp := ID_YES else
  Resp := Geral.MensagemBox('Confirma a cria��o do �ndice '+IdxNome+
  ' da tabela '+Tabela+'?', PChar(ivTitPerg), MB_YESNOCANCEL+MB_ICONQUESTION);
  if Resp = ID_YES then
  begin
    //
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for i := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[i];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      K := Geral.IMV(Conta[Conta.Count-1])-1;
      if K = -1 then
        GravaAviso('FRIndices.Seq_in_index inv�lido (0)!' +
        'ERRO na cria��o do �ndice ' + IdxNome + ' na tabela '+ Tabela, Memo);
      for j := 0 to K do
      begin
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      Dmod.QrSQL.Close;
      Dmod.QrSQL.Database := DataBase;
      Dmod.QrSQL.SQL.Clear;
      // Verificar se n�o haveria viola��o de �ndice!!!
      Dmod.QrSQL.Close;
      Dmod.QrSQL.SQL.Clear;
      Dmod.QrSQL.SQL.Add('SELECT COUNT(*) _ITENS_ FROM ' + lowercase(Tabela));
      Dmod.QrSQL.SQL.Add('GROUP BY ' + Campos);
      Dmod.QrSQL.SQL.Add('ORDER BY _ITENS_ DESC');
      Dmod.QrSQL.Open;
      if (Dmod.QrSQL.RecordCount > 0) and
      (Dmod.QrSQL.FieldByName('_ITENS_').AsFloat > 1) then
      begin
        GravaAviso('O �ndice "' + IdxNome + '" n�o foi inclu�do na tabela "' +
        Tabela + '" pois j� seria criado com viola��o!', Memo);
        (*
        Geral.MensagemBox('O �ndice "' + IdxNome +
        '" n�o foi inclu�do na tabela "' + Tabela +
        '" pois j� seria criado com viola��o!', 'Aviso', MB_OK+MB_ICONWARNING);
        *)
      end else begin
        Dmod.QrSQL.Close;
        Dmod.QrSQL.SQL.Clear;
        //
        if Uppercase(IdxNome) = Uppercase('PRIMARY') then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' ADD PRIMARY KEY ('+Campos+')')
        else
        if Uppercase(Copy(IdxNome, 1, 6)) = 'UNIQUE' then
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' ADD UNIQUE '+IdxNome+' ('+Campos+')')
        else
          Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' Add INDEX '+IdxNome+' ('+Campos+')');
         UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
        Dmod.QrSQL.ExecSQL;
        Result := rvOK;
        GravaAviso(Aviso+ivCriado, Memo);
      end;
      Dmod.QrSQL.Close;
    except;
      UMyMod.MostraSQL(Dmod.QrSQL, Memo, 'ERRO na cria��o de �ndice na tabela '+Tabela);
      Conta.Free;
      //Result := rvErr;
      GravaAviso(Aviso+ivMsgERROCriar, Memo);
      raise;
    end;
  end else begin
    GravaAviso(Aviso+ivAbortInclUser, Memo);
    if Resp = ID_CANCEL then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TMyDBCheck.RecriaIndice(DataBase: String; Tabela, IdxNome,
 Aviso: String; Memo: TMemo): TResultVerify;
var
  i, j: Integer;
  Conta: TStringList;
  Campos: String;
  Resp: Integer;
begin
  Result := rvOK;
  if not FPergunta then Resp := ID_YES else
  Resp := Geral.MensagemBox('Confirma a alter��o do �ndice '+IdxNome+
  ' da tabela '+Tabela+'?', PChar(ivTitPerg), MB_YESNOCANCEL+MB_ICONQUESTION);
  if Resp = ID_YES then
  begin
    Conta := TStringList.Create;
    Campos := myco_;
    try
      for i := 0 to FLIndices.Count -1 do
      begin
        FRIndices := FLIndices.Items[i];
        if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
      end;
      for j := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
      begin
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          if Uppercase(FRIndices.Key_name) = Uppercase(IdxNome) then
          if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
          begin
            if Campos <> myco_ then Campos := Campos + ',';
            Campos := Campos + FRIndices.Column_name;
          end;
        end;
      end;
      Dmod.QrSQL.Close;
      Dmod.QrSQL.Database := DataBase;
      Dmod.QrSQL.SQL.Clear;
      if Uppercase(IdxNome) = Uppercase('PRIMARY') then
        Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP PRIMARY KEY, ADD PRIMARY KEY ('+Campos+')')
      else
      if Uppercase(Copy(IdxNome, 1, 6)) = Uppercase('UNIQUE') then
        Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome+', ADD UNIQUE '+IdxNome+' ('+Campos+')')
      else
        Dmod.QrSQL.SQL.Add('ALTER TABLE '+Tabela+' DROP INDEX '+IdxNome+', ADD INDEX '+IdxNome+' ('+Campos+')');
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrSQL, '', Memo, False, False);
      Dmod.QrSQL.ExecSQL;
      Result := rvOK;
      GravaAviso(Aviso+ivAlterado, Memo);
    except;
      ConsertaTabela(Tabela);
      Conta.Free;
      //Result := rvErr;
      GravaAviso(Aviso+ivMsgERROAlterar, Memo);
      raise;
    end;
  end else begin
    GravaAviso(Aviso+ivAbortAlteUser, Memo);
    if Resp = ID_CANCEL then
    begin
      Result := rvAbort;
      Exit;
    end;
  end;
end;

function TMyDBCheck.TabelaAtiva(Tabela: String): Boolean;
var
  i, p: Integer;
  Tab: String;
begin
  Result := False;
  try
    for i := 0 to FTabelas.Count -1 do
    begin
      //Tab := FTabelas[i];
      p := pos(';', FTabelas[i]);
      if p = 0 then
        Tab := FTabelas[i]
      else
        Tab := Copy(FTabelas[i], 1, p - 1);
      if Uppercase(Tab) = Uppercase(Tabela) then
      begin
        //if Uppercase(Tabela) = 'CONTROLE' then ShowMessage('Achou');
        Result := True;
        Exit;
      end;
    end;
  except
    raise;
  end;
end;

function TMyDBCheck.TabelaExiste(Tabela: String): Boolean;
var
  Tab: String;
  P: Integer;
begin
  Result := False;
  try
    Dmod.QrMas.First;
    P := pos('.', Tabela);
    if P > 0 then
      Tab := Copy(Tabela, P + 1)
    else
      Tab := Tabela;
    while not Dmod.QrMas.Eof do
    begin
      if Uppercase(Tab) = Uppercase(Dmod.QrMas.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      Dmod.QrMas.Next;
    end;
  except
    raise;
  end;
end;

function TMyDBCheck.ImpedeVerificacao(): Boolean;
begin
  $WARNINGS OFF
  //////////////////////////////////////////////////////////////////////////////
  // Impedir Verifica��o em vers�es novas                                     //
  //////////////////////////////////////////////////////////////////////////////
  if CO_VERSAO mod 10000 = 9999 then
  begin
    VAR_VERIFI_DB_CANCEL := True;
    Result := True;
    Geral.MensagemBox('Esta � uma vers�o provis�ria.' + #13#10 +
    'N�o ser� feita nenhuma verifica��o no banco de dados.' + #13#10 +
    'Favor reportar qualquer tipo de erro que venha a ocorrer imediatamente ' +
    '� DERMATEK, para ser solucionado antes da vers�o est�vel.',
    'AVISO', MB_OK + MB_ICONWARNING);
    Exit;
  end else Result := False;
  //////////////////////////////////////////////////////////////////////////////
  // FIM  Impedir Verifica��o em vers�es novas                                //
  //////////////////////////////////////////////////////////////////////////////
  $WARNINGS ON
end;

function TMyDBCheck.IndiceExiste(DataBase: String; Tabela, Indice: String;
 Memo: TMemo): Integer;
var
  i: Integer;
  Need, Find, Have: Integer;
  Item: String;
begin
  Item := Format(ivCampo_Nome, [Tabela, Indice])+mycoEspaco + 'N�o encontrado:';
  Result := 0;
  Need := 0;
  Find := 0;
  Have := 0;
  Dmod.QrIdx.Close;
  Dmod.QrIdx.Database := DataBase;
  Dmod.QrIdx.SQL.Clear;
  Dmod.QrIdx.SQL.Add('SHOW KEYS FROM ' + Lowercase(Tabela));
  Dmod.QrIdx.Open;
  while not Dmod.QrIdx.Eof do
  begin
    if Uppercase(Dmod.QrIdx.FieldByName('Key_name').AsString) =
    Uppercase(Indice) then Have := Have + 1;
    Dmod.QrIdx.Next;
  end;
  try
    for i := 0 to FLIndices.Count -1 do
    begin
      FRIndices := FLIndices.Items[i];
      if Uppercase(FRIndices.Key_name) = Uppercase(Indice) then
      begin
        Dmod.QrIdx.First;
        Need := Need + 1;
        while not Dmod.QrIdx.Eof do
        begin
          if (Uppercase(FRIndices.Key_name)  =
              Uppercase(Dmod.QrIdx.FieldByName('Key_name').AsString)) then
          begin
            if Result = 0 then Result := 1;
            if (FRIndices.Non_unique  =
               Dmod.QrIdx.FieldByName('Non_unique').AsInteger)
            and (FRIndices.Seq_in_index  =
               Dmod.QrIdx.FieldByName('Seq_in_index').AsInteger)
            and (Uppercase(FRIndices.Column_name)  =
               Uppercase(Dmod.QrIdx.FieldByName('Column_name').AsString))
            then Find := Find + 1;
          end;
          Dmod.QrIdx.Next;
        end;
        if Find < Need then
        begin
          if Find > 0 then
          begin
            GravaAviso('A coluna '+FRIndices.Column_name+
            ' n�o foi encontrada no �ndice '+Indice+'.', Memo);
            Result := 2;
          end else GravaAviso('O �ndice '+Indice+' n�o existe.', Memo);
        end;
      end;
      //if Result = 2 then Exit;
    end;
  except
    Dmod.QrIdx.Close;
    raise;
  end;
  if Have > Find then
  begin
    GravaAviso('H� excesso de colunas no �ndice '+Indice+' da tabela '+Tabela+
    '.', Memo);
    MyList.ModificaDadosDeIndicesAlterados(Indice, Tabela, Database, Memo);
    Result := 3;
  end;
end;

function TMyDBCheck.CampoAtivo(DataBase: String; TabelaNome, TabelaBase, Campo: String;
 Memo: TMemo): Integer;
var
  i: Integer;
  //Change: Boolean;
  Item: String;
begin
  Item := Format(ivCampo_Nome, [TabelaNome, Campo])+mycoEspaco + ivMsgDiferenca;
  Result := 9;
  //Change := False;
  try
    for i := 0 to FLCampos.Count -1 do
    begin
      FRCampos := FLCampos.Items[i];
      if Uppercase(FRCampos.Field) = Uppercase(Campo) then
      begin
        Result := 0;
        if Uppercase(FRCampos.Tipo)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Type').AsString) then
        begin
          Result := 1;
          GravaAviso(Item+mycoTypeDataType, Memo);
        end;
        if Uppercase(FRCampos.Null)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Null').AsString) then
        begin
          if (Uppercase(FRCampos.Null) = '') and
          (Uppercase(Dmod.QrNTV.FieldByName('Null').AsString) = 'NO') then
          begin
            // N�o faz nada
            // mudou no M y S Q L 5.0 de '' para 'NO'
          end else begin
            Result := 1;
            GravaAviso(Item+'"NULL"', Memo);
          end;
        end;
        if Uppercase(FRCampos.Default) <>
           Uppercase(Dmod.QrNTV.FieldByName('Default').AsString) then
        begin
          Result := 1;
          GravaAviso(Item+'"DEFAULT"', Memo);
        end;
        if Uppercase(FRCampos.Extra)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Extra').AsString) then
        begin
          Result := 1;
          GravaAviso(Item+'"EXTRA"', Memo);
        end;
        if Uppercase(FRCampos.Key)  <>
           Uppercase(Dmod.QrNTV.FieldByName('Key').AsString) then
        begin
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Result = 9 then Result := 3;
            GravaAviso(Item+'"KEY"', Memo);
          end;
        end;
      end;
      if Result < 9 then Exit;
    end;
  except
    raise;
  end;
end;

function TMyDBCheck.CampoExiste(Tabela, Campo: String; Memo: TMemo): Integer;
var
  Item: String;
begin
  Item := Format(ivCampo_Nome, [Tabela, Campo])+mycoEspaco+ivMsgNaoExiste;
  Result := 2;
  try
    Dmod.QrNTV.First;
    while not Dmod.QrNTV.Eof do
    begin
      if Uppercase(Dmod.QrNTV.FieldByName('Field').AsString) =
         Uppercase(FRCampos.Field) then Result := 0;
      if Result < 2 then Exit;
      Dmod.QrNTV.Next;
    end;
    if Result = 2 then
      GravaAviso(Item, Memo);
  except
    raise;
  end;
end;

procedure TMyDBCheck.ConsertaTabela(Tabela: String);
var
  ID: Integer;
  TbUper: String;
begin
  TbUper := Uppercase(Tabela);
  if ((TbUper = 'STQMOVITSA') or (TbUper = 'STQMOVITSB')) then
  begin
    DmodG.QrAux.Close;
    DmodG.QrAux.SQL.Clear;
    DmodG.QrAux.SQL.Add('SELECT Tipo, OriCodi, OriCtrl');
    DmodG.QrAux.SQL.Add('FROM ' + Lowercase(Tabela));
    DmodG.QrAux.SQL.Add('WHERE IDCtrl=0');
    DmodG.QrAux.Open;
    if DmodG.QrAux.RecordCount > 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + Lowercase(Tabela) + ' SET ');
      Dmod.QrUpd.SQL.Add('IDCtrl=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P1 ');
      Dmod.QrUpd.SQL.Add('AND OriCodi=:P2');
      Dmod.QrUpd.SQL.Add('AND OriCtrl=:P3 ');
      while not DmodG.QrAux.Eof do
      begin
        ID := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
        Dmod.QrUpd.Params[00].AsInteger := ID;
        Dmod.QrUpd.Params[01].AsInteger := DmodG.QrAux.FieldByName('Tipo').AsInteger;
        Dmod.QrUpd.Params[02].AsInteger := DmodG.QrAux.FieldByName('OriCodi').AsInteger;
        Dmod.QrUpd.Params[03].AsInteger := DmodG.QrAux.FieldByName('OriCtrl').AsInteger;
        Dmod.QrUpd.ExecSQL;
        //
        DmodG.QrAux.Next;
      end;
    end;
  end;  
end;

function TMyDBCheck.CriaTabela(DataBase: String; TabelaNome,
TabelaBase: String; Memo: TMemo; Acao: TAcaoCriaTabela): Boolean;
var
  i, j, k, u: Integer;
  Item, Opcoes, Campos: String;
  Indices, Conta: TStringList;
  Texto: TStringList;
begin
  Result := True;
  FLCampos  := TList.Create;
  FLIndices := TList.Create;
  Texto     := TStringList.Create;
  try
    MyList.CriaListaCampos(TabelaBase, FLCampos);
    //
    New(FRCampos);
    FRCampos.Field      := mycoCampoAlterWeb;
    FRCampos.Tipo       := 'tinyint(1)';
    FRCampos.Null       := '';
    FRCampos.Key        := '';
    FRCampos.Default    := '1';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
    New(FRCampos);
    FRCampos.Field      := mycoCampoAtivo;
    FRCampos.Tipo       := 'tinyint(1)';
    FRCampos.Null       := '';
    FRCampos.Key        := '';
    FRCampos.Default    := '1';
    FRCampos.Extra      := '';
    FLCampos.Add(FRCampos);
    //
    MyList.CriaListaIndices(TabelaBase, TabelaNome, FLIndices);
    try
      Dmod.QrNTI.Close;
      Dmod.QrNTI.SQL.Clear;
      Dmod.QrNTI.Database := DataBase;
      Dmod.QrNTI.SQL.Clear;
      Dmod.QrNTI.SQL.Add('CREATE TABLE '+Lowercase(TabelaNome)+' (');
      //
      Item := Format(ivTabela_Nome, [TabelaNome]);
      for i:= 0 to FLCampos.Count - 1 do
      begin
        Opcoes := myco_;
        FRCampos := FLCampos[i];
        //ShowMensagem(FRCampos.Field);
        Opcoes := FRCampos.Tipo;
        if FRCampos.Null <> 'YES' then
          Opcoes := Opcoes + ' NOT NULL ';
        if FRCampos.Default <> myco_ then
          Opcoes := Opcoes + ' DEFAULT ' + mycoAspasDuplas +
          FRCampos.Default + mycoAspasDuplas;
        if FRCampos.Extra <> myco_ then
        begin
          if Uppercase(FRCampos.Extra) = Uppercase('auto_increment')
          then Opcoes := Opcoes + FRCampos.Extra;
        end;
        Opcoes := mycoEspacos2+FRCampos.Field+mycoEspaco+Opcoes;
        if i < (FLCampos.Count-1) then Opcoes := Opcoes + mycoVirgula;
        Texto.Add(Opcoes);
        //Texto[Texto.Count] := Opcoes;
      end;
      Indices := TStringList.Create;
      Indices.Sorted := True;
      Indices.Duplicates := (dupIgnore);
      try
        for i := 0 to FLIndices.Count -1 do
        begin
          FRIndices := FLIndices.Items[i];
          Indices.Add(FRIndices.Key_name);
        end;
        u := Texto.Count-1;
        if Indices.Count > 0 then Texto[u] := Texto[u] + mycoVirgula;
        for k := 0 to Indices.Count -1 do
        begin
          Conta := TStringList.Create;
          Campos := myco_;
          for i := 0 to FLIndices.Count -1 do
          begin
            FRIndices := FLIndices.Items[i];
            if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              Conta.Add(FormatFloat('000', FRIndices.Seq_in_index))
          end;
          for j := 0 to Geral.IMV(Conta[Conta.Count-1])-1 do
          begin
            for i := 0 to FLIndices.Count -1 do
            begin
              FRIndices := FLIndices.Items[i];
              if Uppercase(FRIndices.Key_name) = Uppercase(Indices[k]) then
              if StrToInt(Conta[j]) = FRIndices.Seq_in_index then
              begin
                if Campos <> myco_ then Campos := Campos + ',';
                Campos := Campos + FRIndices.Column_name;
              end;
            end;
          end;
          if Uppercase(TabelaNome) <> Uppercase('lanctoz') then
          begin
            if Uppercase(Indices[k]) = Uppercase('PRIMARY') then
              Texto.Add('  PRIMARY KEY (' + Campos + ')')
            else if Uppercase(Copy(Indices[k], 1, 6)) = Uppercase('UNIQUE') then
              Texto.Add('  UNIQUE KEY '+ Indices[k] + ' ('+Campos+')')
            else
              Texto.Add('  INDEX '     + Indices[k] + ' ('+Campos+')');
            if k < (Indices.Count-1) then
            begin
             u := Texto.Count-1;
             Texto[u] := Texto[u]+mycoVirgula;
            end;
          end;
          Conta.Free;
        end;
      finally
        Indices.Free;
      end;
      Texto.Add(')');
      // 15-06-2009
      //Texto.Add('ENGINE = InnoDB1');
      Texto.Add('CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
      // fim 15-06-2009
      for i := 0 to Texto.Count-1 do
      Dmod.QrNTI.SQL.Add(Texto[i]);
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(Dmod.QrNTI, '', Memo, False, False);
      if Acao = actCreate then
      begin
        Dmod.QrNTI.ExecSQL;
        GravaAviso(Item+ivCriado, Memo);
        VerificaRegistrosObrigatorios_Inclui(Database, TabelaNome, TabelaBase, Memo, False, True);
        VerificaRegistrosDeArquivo(Database, TabelaNome, TabelaBase, Memo, False);
      end;
    except
      //GravaAviso(Item+ivMsgERROCriar, Memo);
      on E: Exception do
        GravaAviso(Item+E.Message+Chr(13)+Chr(10)+ivMsgERROCriar, Memo);
    end;
  finally
    FLCampos.Free;
    FLIndices.Free;
    Texto.Free;
  end;
end;

procedure TMyDBCheck.VerificaVariaveis(Memo: TMemo);
const
  Texto= 'Variavel ''%s'' n�o definida';
begin
  if VAR_CAMPOTRANSPORTADORA = '' then
  Memo.Lines.Add(Format(Texto, ['VAR_CAMPOTRANSPORTADORA']));
end;

procedure TMyDBCheck.GravaAviso(Aviso: String; Memo: TMemo; ToFile: Boolean);
begin
  if Memo <> nil then
  begin
    Memo.Lines.Add(Aviso);
    if ToFile then
      WriteLn(FText, Aviso);
  end;  
end;

procedure TMyDBCheck.VerificaControle(Memo: TMemo);
begin
  // N�o necess�rio??
end;

function TMyDBCheck.AcessoNegadoAoForm_2(Tabela, Form : String; ModoAcesso:
  TAcessFormModo): Boolean;

  function Privilegios(Usuario: Integer): Boolean;
  begin
    FM_MASTER := 'F';
    Dmod.QrPerfis.Close;
    // adicionado em 2008.03.17
    Dmod.QrPerfis.SQL.Clear;
    Dmod.QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    Dmod.QrPerfis.SQL.Add('FROM perfisits pit');
    Dmod.QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    Dmod.QrPerfis.SQL.Add('LEFT JOIN senhas sen ON sen.Perfil=pip.Codigo');
    Dmod.QrPerfis.SQL.Add('WHERE sen.Numero=:P0');
    Dmod.QrPerfis.SQL.Add('');
    // fim adicionado em 2008.03.17
    Dmod.QrPerfis.Params[0].AsInteger := Usuario;
    Dmod.QrPerfis.Open;
    Result := Dmod.QrPerfis.RecordCount > 0;
    if not Result then Geral.MensagemBox('N�o h� privil�gios ' +
      'para o usu�rio ' + IntToStr(VAR_USUARIO) + ' - ' + VAR_LOGIN + '!',
      'Aviso', MB_OK+MB_ICONWARNING);
    // N�o pode fechar!!
    //Dmod.QrPerfis.Close;
  end;
const
  Tfm = 'TFM';
var
  Loc: Boolean;
begin
  if ModoAcesso = afmAcessoTotal then
  begin
    Result := False;
    Exit;
  end;
  if Uppercase(Copy(Form, 1, 3)) = Tfm then Form := Copy(Form, 4, Length(Form)-3);
  if ModoAcesso = afmSoMaster then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then Result := False
    else Geral.MensagemBox('Acesso Restrito', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if ModoAcesso = afmNegarComAviso then
    begin
      Geral.MensagemBox('Tabela de perfis n�o definida ao verificar acesso.',
      'Acesso a formul�rio', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  or (Dmod.QrMasterSolicitaSenha.Value = 0)
  then Result := False
  else begin
    Privilegios(VAR_USUARIO);
    if not Dmod.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
        Dmod.QrAux.Params[0].AsString  := Form;
        Dmod.QrAux.Open;
        if Dmod.QrAux.RecordCount = 0 then
        begin
          DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'perfisits', 'perfisits', nil,
            True, True);
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
          Dmod.QrAux.Params[0].AsString  := Form;
          Dmod.QrAux.Open;
          if Dmod.QrAux.RecordCount = 0 then
          begin
            Dmod.QrAux.Close;
            Dmod.QrAux.SQL.Clear;
            Dmod.QrAux.SQL.Add('INSERT INTO perfisits SET Janela=:P0');
            Dmod.QrAux.Params[0].AsString  := Form;
            Dmod.QrAux.ExecSQL;
            Dmod.Privilegios(-1000);
          end;
        end;
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (Dmod.QrPerfisLibera.Value = 0) or (Loc=False) then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MensagemBox('Somente o administrador tem acesso a este formul�rio!',
          'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION)
      else begin
        case ModoAcesso of
          afmNegarSemAviso: ;// nada;  Msg=-1
          afmNegarComAviso: //  Msg = 0 ou 2
            Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          afmParcial:  // Msg = 1
            Geral.MensagemBox('Login com acesso parcial!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
          afmParcialSemEdicao:   // Msg = 3
            Geral.MensagemBox('Login com acesso parcial, sem acesso a edi��o!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
        end;
      end;
    end else Result := False;
  end;
end;

function TMyDBCheck.AcessoNegadoAoForm_3(Tabela, Form, Descricao: String;
ModoAcesso: TAcessFormModo): Boolean;

  function Privilegios(Usuario: Integer): Boolean;
  begin
    FM_MASTER := 'F';
    Dmod.QrPerfis.Close;
    // adicionado em 2008.03.17
    Dmod.QrPerfis.SQL.Clear;
    Dmod.QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    Dmod.QrPerfis.SQL.Add('FROM perfisits pit');
    Dmod.QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    Dmod.QrPerfis.SQL.Add('LEFT JOIN senhas sen ON sen.Perfil=pip.Codigo');
    Dmod.QrPerfis.SQL.Add('WHERE sen.Numero=:P0');
    Dmod.QrPerfis.SQL.Add('');
    // fim adicionado em 2008.03.17
    Dmod.QrPerfis.Params[0].AsInteger := Usuario;
    Dmod.QrPerfis.Open;
    Result := Dmod.QrPerfis.RecordCount > 0;
    if not Result then Geral.MensagemBox('N�o h� privil�gios ' +
      'para o usu�rio ' + IntToStr(VAR_USUARIO) + ' - ' + VAR_LOGIN + '!',
      'Aviso', MB_OK+MB_ICONWARNING);
    // N�o pode fechar!!
    //Dmod.QrPerfis.Close;
  end;
const
  Tfm = 'TFM';
var
  Loc: Boolean;
begin
  if ModoAcesso = afmAcessoTotal then
  begin
    Result := False;
    Exit;
  end;
  if Uppercase(Copy(Form, 1, 3)) = Tfm then Form := Copy(Form, 4, Length(Form)-3);
  if ModoAcesso = afmSoMaster then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then Result := False
    else Geral.MensagemBox('Acesso Restrito (Master)', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if ModoAcesso = afmSoBoss then
  begin
    Result := True;
    if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then Result := False
    else Geral.MensagemBox('Acesso Restrito (Administrador)',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Trim(Tabela) = '' then
  begin
    Result := False;
    if ModoAcesso = afmNegarComAviso then
    begin
      Geral.MensagemBox('Tabela de perfis n�o definida ao verificar acesso.',
      'Acesso a formul�rio', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  if ((VAR_LOGIN = 'MASTER') and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
  or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  or (Dmod.QrMasterSolicitaSenha.Value = 0)
  then Result := False
  else begin
    Privilegios(VAR_USUARIO);
    if not Dmod.QrPerfis.Locate('Janela', Form, [loCaseInsensitive]) then
    begin
      Loc := False;
      if (Form <> '') and (Uppercase(Form) <> 'MASTER') then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
        Dmod.QrAux.Params[0].AsString  := Form;
        Dmod.QrAux.Open;
        if Dmod.QrAux.RecordCount = 0 then
        begin
          DBCheck.VerificaRegistrosObrigatorios_Inclui(Dmod.MyDB, 'perfisits', 'perfisits', nil,
            True, True);
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT * FROM perfisits WHERE Janela=:P0');
          Dmod.QrAux.Params[0].AsString  := Form;
          Dmod.QrAux.Open;
          if Dmod.QrAux.RecordCount = 0 then
          begin
            Dmod.QrAux.Close;
            Dmod.QrAux.SQL.Clear;
            Dmod.QrAux.SQL.Add('INSERT INTO perfisits ');
            Dmod.QrAux.SQL.Add('SET Janela=:P0, Descricao=:P1');
            Dmod.QrAux.Params[0].AsString  := Form;
            Dmod.QrAux.Params[1].AsString  := Descricao;
            Dmod.QrAux.ExecSQL;
            Dmod.Privilegios(-1000);
          end;
        end;
        Dmod.QrAux.Close;
      end;
    end else Loc := True;
    if (Dmod.QrPerfisLibera.Value = 0) or (Loc=False) then
    begin
      Result := True;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MensagemBox('Somente o administrador tem acesso a este formul�rio!',
          'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION)
      else begin
        case ModoAcesso of
          afmNegarSemAviso: ;// nada;  Msg=-1
          afmNegarComAviso: //  Msg = 0 ou 2
            Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          afmParcial:  // Msg = 1
            Geral.MensagemBox('Login com acesso parcial!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
          afmParcialSemEdicao:   // Msg = 3
            Geral.MensagemBox('Login com acesso parcial, sem acesso a edi��o!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
        end;
      end;
    end else Result := False;
  end;
end;

function TMyDBCheck.CriaFormAntigo(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFormModo): Boolean;
var
  Instance: TComponent;
  Form: String;
  MyCursor: TCursor;
begin
  Result := False;
  Form := InstanceClass.ClassName;
  if Pos('TFm', Form) = 1 then
    Form := Copy(Form, 4, Length(Form)-3);
  //ShowMessage(Form);
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      if VAR_LIBERA_TODOS_FORMS = False then
      begin
        //ShowMessage(TForm(Instance).Caption);
        if AcessoNegadoAoForm_3('Perfis', Form, TForm(Instance).Caption, ModoAcesso) then
        begin
          TForm(Instance).Destroy;
          Exit;
        end else Result := True;
      end else Result := True;
    except
      TComponent(Reference) := nil;
      raise;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TMyDBCheck.CriaDatabase(Query: T m y S Q L Query; Database: String): Boolean;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('CREATE DATABASE ' + (*Lowercase(*)Database(*)*));
  Query.SQL.Add('DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
  Query.ExecSQL;
  Result := True;
end;
}

function TMyDBCheck.CriaFm(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo): Boolean;
  procedure Habilita(Compo: TComponent; Visivel: Boolean);
  var
    PropInfo: PPropInfo;
  begin
    if Compo <> nil then
    begin
      PropInfo := GetPropInfo(Compo, 'Visible');
      if PropInfo <> nil then
        SetPropValue(Compo, 'Visible', Visivel);
    end;
  end;
var
  Instance: TComponent;
  Form, Form_ID, Nome: String;
  MyCursor: TCursor;
  k, i, Acesso: Integer;
  Inclui, Altera, Exclui: Boolean;
  //ModoAntigo: TAcessFormModo;
  Objeto: TObject;
  PropInfo: PPropInfo;
begin
  Acesso := 0;
{ 2011-06-25
  if  (UpperCase(Application.Title) = 'CREDITOR')
  or  (UpperCase(Application.Title) = 'SYNDIC') then
  begin
    case ModoAcesso of
      afmoSoMaster:      ModoAntigo := afmSoMaster;
      afmoSoBoss:        ModoAntigo := afmSoBoss;
      afmoNegarSemAviso: ModoAntigo := afmNegarSemAviso;
      afmoNegarComAviso: ModoAntigo := afmNegarComAviso;
      afmoLiberado(*afmoProvisorio*)   : ModoAntigo := afmAcessoTotal;
      //
      else ModoAntigo := afmNegarComAviso;
    end;
    Result := CriaFormAntigo(InstanceClass, Reference, ModoAntigo);
    Exit;
  end;
}
  Result := False;
  Form := InstanceClass.ClassName;
  if Pos('TFm', Form) = 1 then
    Form := Copy(Form, 4, Length(Form)-3);
  //ShowMessage(Form);
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Instance := TComponent(InstanceClass.NewInstance);
    TComponent(Reference) := Instance;
    try
      Instance.Create(Application);
      ///
      if VAR_SOMAIUSCULAS then
      begin
        with TForm(Reference) do
        begin
          for i := 0 to ComponentCount -1 do
          begin
            Objeto := Components[i];
            PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'CharCase');
            if PropInfo <> nil then
            begin
              Nome := Uppercase(TComponent(Objeto).Name);
              if (Nome <> 'EDEMAIL') and
              (Nome <> 'EDPEMAIL') and (Nome <> 'EDEEMAIL') then
              try
                {
                if not (Objeto is T L M D DirDlg) then
                begin
                }
                  //
                  if GetPropValue(TComponent(Objeto), 'CharCase') = 'ecNormal' then
                  SetPropValue(Objeto, 'CharCase', 'ecUpperCase');
                //end;
              except
                //
              end;
            end;
          end;
        end;
      end;
      if  (UpperCase(Application.Title) = 'CREDITOR') 
      or  (UpperCase(Application.Title) = 'SYNDIC') then
      begin
        Screen.Cursor := MyCursor;
        Result := True;
        Exit;
      end;

      ///
      //k := pos('::', TForm(Instance).Caption);
      //if k = 0 then
      k := 13;
      Form_ID := Copy(TForm(Instance).Caption, 1, k);
      // Verifica se a janela existe
{ 2011-06-25
      DModG.QrPerfJan.Close;
      DModG.QrPerfJan.Params[0].AsString := Form_ID;
      DModG.QrPerfJan.Open;
      if DModG.QrPerfJan.RecordCount = 0 then
      begin
        if Form_ID <> 'MAS-CADAS-000' then
        begin
          Geral.MensagemBox('A janela ' + Form_ID + ' n�o est� ' +
          'cadastrada para ser consultada!'#13#10'Informe a DERMATEK!',
          'Aviso', MB_ICONERROR+MB_OK);
          if (VAR_USUARIO >=0) or (Form_ID <> 'FER-VRFBD-001') then
          begin
            TForm(Instance).Destroy;
            Screen.Cursor := MyCursor;
            Exit;
          end;
        end;
      end;
}
      if VAR_LIBERA_TODOS_FORMS = False then
      begin
        //ShowMessage(TForm(Instance).Caption);
{ 2011-06-25
        Acesso := AcessoAoForm(Form_ID, TForm(Instance).Caption, ModoAcesso);
        if Acesso = 0 then
        begin
          TForm(Instance).Destroy;
          Screen.Cursor := MyCursor;
          Exit;
        end else
}        begin
          Result := True;
          if Acesso < 8 then
          begin
{ 2011-06-25
            Inclui := DmodG.QrPerfInclui.Value = 1;
            Altera := DmodG.QrPerfAltera.Value = 1;
            Exclui := DmodG.QrPerfExclui.Value = 1;
}
            Inclui := True;
            Altera := True;
            Exclui := True;
// Fim 2011-06-25
            for i := 0 to TComponent(Reference).ComponentCount - 1 do
            begin
              if TComponent(Reference).Components[i] is TdmkPermissoes then
              begin
                if TdmkPermissoes(TComponent(Reference)) <> nil then
                begin
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns01, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns02, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns03, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns04, Inclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanIns05, Inclui);
                  //
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd01, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd02, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd03, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd04, Altera);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanUpd05, Altera);
                  //
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel01, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel02, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel03, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel04, Exclui);
                  Habilita(TdmkPermissoes(TComponent(Reference).Components[i]).CanDel05, Exclui);
                  //
                end;
              end;
            end;
          end;
        end;
      end;
    except
      //VOSTRO_1320
      raise;
      TComponent(Reference) := nil;
    end;
    Screen.Cursor := MyCursor;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

{
// escolher ( selecionar ) quais exclui
function TMyDBCheck.QuaisItens_Exclui(QrExecSQL, QrDados: T m y S Q L Query; DBGrid: TDBGrid;
             TabelaTarget: String; CamposSource, CamposTarget: array of String;
             Quais: TSelType; SQL_AfterDel: String): Integer;
  function ExecutaSQL: Boolean;
  var
    i, j: Integer;
    Valor: String;
  begin
    Result := False;
    if M L A G e r a l.ImpedeExclusaoPeloNomeDaTabela(TabelaTarget) then
      Exit;
    QrExecSQL.SQL.Clear;
    QrExecSQL.SQL.Add(DELETE_FROM + Lowercase(TabelaTarget) + ' WHERE ');
    //
    j := High(CamposSource);
    for i := Low(CamposSource) to j do
    begin
      Valor := Geral.VariavelToString(QrDados.FieldByName(CamposSource[i]).Value);
      if i < j then
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor + ' AND ')
      else
        QrExecSQL.SQL.Add(CamposTarget[i] + '=' + Valor);
    end;
    try
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
      QrExecSQL.ExecSQL;
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
    end;
    //
  end;
var
  n, m: integer;
  q: TSelType;
begin
  q := istNenhum;
  Result := 0;
  if (QrDados.State <> dsBrowse) or (QrDados.RecordCount = 0) then
  begin
    Geral.MensagemBox('N�o h� item a ser exclu�do!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Quais = istPergunta then
  begin
    if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
    begin
      with FmQuaisItens do
      begin
        ShowModal;
        if not FSelecionou then
        begin
          Geral.MensagemBox('Exclus�o cancelada pelo usu�rio!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          q := istDesiste;
        end else q := FEscolha;
        Destroy;
      end;
    end;
  end
  else q := Quais;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;
  m := 0;
  if (q = istSelecionados) and (DBGrid.SelectedRows.Count < 2) then
    q := istAtual;
  case q of
    istAtual:
    begin
      if Geral.MensagemBox('Confirma a exclus�o do item selecionado?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        if ExecutaSQL() then m := 1;
    end;
    istSelecionados:
    begin
      if Geral.MensagemBox('Confirma a exclus�o dos ' +
      IntToStr(DBGrid.SelectedRows.Count) + ' itens selecionados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with DBGrid.DataSource.DataSet do
        for n := 0 to DBGrid.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGrid.SelectedRows.Items[n]));
          if ExecutaSQL() then inc(m, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MensagemBox('Confirma a exclus�o de todos os ' +
      IntToStr(QrDados.RecordCount) + ' itens pesquisados?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrDados.First;
        while not QrDados.Eof do
        begin
          if ExecutaSQL() then inc(m, 1);
          QrDados.Next;
        end;
      end;
    end;
  end;
  if m > 0 then
  begin
    Result := m;
    if Trim(SQL_AfterDel) <> '' then
    begin
      QrExecSQL.SQL.Clear;
      QrExecSQL.SQL.Add(SQL_AfterDel);
      QrExecSQL.ExecSQL;
    end;
    QrDados.Close;
    QrDados.Open;
    if m = 1 then Geral.MensagemBox('Um item foi exclu�do!',
      'Informa��o', MB_OK+MB_ICONINFORMATION) else
    Geral.MensagemBox(IntToStr(m) + ' itens foram exclu�dos!',
      'Informa��o', MB_OK+MB_ICONINFORMATION);
  end;
  Screen.Cursor := crDefault;
end;

function TMyDBCheck.Quais_Selecionou(const Query: T m y S Q L Query; DBGrid:
TDBGrid; var Quais: TSelType): Boolean;
begin
  Result := True;
  Quais := istNenhum;
  if (Query <> nil) and (Query.State <> dsBrowse) and (Query.RecordCount = 0) then
  begin
    Geral.MensagemBox('N�o h� item a ser selecionado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Geral.MensagemBox('Sele��o cancelada pelo usu�rio!', 'Aviso',
          MB_OK+MB_ICONWARNING);
        Quais := istDesiste;
      end else Quais := FEscolha;
      Destroy;
    end;
  end;
  if (Quais = istSelecionados) and (DBGrid <> nil)
  and (DBGrid.SelectedRows.Count < 2) then
    Quais := istAtual;
  //
  Result := not (Quais in ([istNenhum, istPergunta, istDesiste]));
end;

function TMyDBCheck.Quais_Selecionou_ExecProc(const Query: T m y S Q L Query; DBGrid:
TDBGrid; var Quais: TSelType; Procedure1: TProcedure): Boolean;
var
  q: TSelType;
  n: Integer;
begin
  Result := Quais_Selecionou(Query, DBGrid, q);
  if not Result then Exit;
  //
  case q of
    istAtual:
    begin
      Procedure1;
      //procedure2;
    end;
    istSelecionados:
    begin
      with DBGrid.DataSource.DataSet do
      for n := 0 to DBGrid.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid.SelectedRows.Items[n]));
        Procedure1;
        //Procedure2;
      end;
    end;
    istTodos:
    begin
      (*
      if Geral.MensagemBox('Confirma a exclus�o de todos os ' +
      IntToStr(QrStqMovIts.RecordCount) + ' itens pesquisados?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
      *)
        Query.First;
        while not Query.Eof do
        begin
          Procedure1;
          //procedure2;
          Query.Next;
        end;
      //end;
    end;
  end;
end;

function TMyDBCheck.QuaisItens_Altera(QrExecSQL, QrDados: T m y S Q L Query;
DBGrid: TDBGrid; Tabela: String;
SQLCamposAlt, SQLIndexAlt, SQLIndexSource: array of String;
SQLValores: array of Variant; Quais: TSelType;
Pergunta, AvisaAlterados: Boolean): Boolean;
  function ExecutaSQL(): Boolean;
  var
    i, j: Integer;
    Valor: String;
  begin
    Result := False;
    QrExecSQL.SQL.Clear;
    QrExecSQL.SQL.Add('UPDATE ' + Lowercase(Tabela) + ' SET ');
    //
    j := High(SQLCamposAlt);
    for i := Low(SQLCamposAlt) to j do
    begin
      Valor := Geral.VariavelToString(SQLValores[i]);
      if i < j then
        QrExecSQL.SQL.Add(SQLCamposAlt[i] + '=' + Valor + ', ')
      else
        QrExecSQL.SQL.Add(SQLCamposAlt[i] + '=' + Valor);
    end;
    //
    j := High(SQLIndexAlt);
    for i := Low(SQLIndexAlt) to j do
    begin
      Valor := Geral.VariavelToString(QrDados.FieldByName(SQLIndexSource[i]).Value);
      if i = 0 then
        QrExecSQL.SQL.Add(' WHERE ' + SQLIndexAlt[i] + '=' + Valor + ' ')
      else if i < j then
        QrExecSQL.SQL.Add(' AND ' + SQLIndexAlt[i] + '=' + Valor + ' ');
      end;
    try
      QrExecSQL.ExecSQL;
      //UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
      Result := True;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrExecSQL, '', nil, True, True);
    end;
    //
  end;
  var
    n, Afetados: integer;
    q: TSelType;
    Executa: Boolean;
begin
  q := istNenhum;
  Afetados := 0;
  //
  Result := False;
  if (QrDados.State <> dsBrowse) or (QrDados.RecordCount = 0) then
  begin
    if AvisaAlterados then
      Geral.MensagemBox('N�o h� item a ser alterado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Quais = istPergunta then
  begin
    if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
    begin
      with FmQuaisItens do
      begin
        ShowModal;
        if not FSelecionou then
        begin
          if AvisaAlterados then
            Geral.MensagemBox('Exclus�o cancelada pelo usu�rio!', 'Aviso',
              MB_OK+MB_ICONWARNING);
          q := istDesiste;
        end else q := FEscolha;
        Destroy;
      end;
    end;
  end else q := Quais;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;

  if (q = istSelecionados) and (DBGrid.SelectedRows.Count < 2) then
     q := istAtual;
  case q of
    istAtual:
    begin
      if Pergunta then
      begin
        Executa := Geral.MensagemBox('Confirma a altera��o do item selecionado?',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
      end else Executa := True;
      if Executa then
        if ExecutaSQL() then Afetados := 1;
    end;
    istSelecionados:
    begin
      if Pergunta then
        Executa := Geral.MensagemBox('Confirma a altera��o dos ' +
        IntToStr(DBGrid.SelectedRows.Count) + ' itens selecionados?',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
      else
        Executa := True;
      if Executa then
      begin
        with DBGrid.DataSource.DataSet do
        for n := 0 to DBGrid.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGrid.SelectedRows.Items[n]));
          if ExecutaSQL() then inc(Afetados, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Pergunta then Executa :=
        Geral.MensagemBox('Confirma a altera��o de todos os ' +
        IntToStr(QrDados.RecordCount) + ' itens pesquisados?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
      else
        Executa := True;
      if Executa then
      begin
        QrDados.First;
        while not QrDados.Eof do
        begin
          if ExecutaSQL() then inc(Afetados, 1);
          QrDados.Next;
        end;
      end;
    end;
  end;
  if Afetados > 0 then
  begin
    Result := True;
    QrDados.Close;
    QrDados.Open;
    if AvisaAlterados then
    begin
      if Afetados = 1 then Geral.MensagemBox('Um item foi alterado!',
        'Informa��o', MB_OK+MB_ICONINFORMATION) else
      Geral.MensagemBox(IntToStr(Afetados) + ' itens foram alterados!',
        'Informa��o', MB_OK+MB_ICONINFORMATION);
    end;    
  end;
  Screen.Cursor := crDefault;
end;

function TMyDBCheck.ObtemData(const DtDefault: TDateTime; var DtSelect:
TDateTime; const MinData: TDateTime; const HrDefault: TTime = 0;
const HabilitaHora: Boolean = False): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData) then
  begin
    with FmGetData do
    begin
      FMinData        := MinData;
      TPData.Date     := DtDefault;
      LaMulta.Visible := False;
      EdMulta.Visible := False;
      LaJuros.Visible := False;
      EdJuros.Visible := False;
      // 2011-05-21
      EdHora.ValueVariant := HrDefault;
      EdHora.Enabled := HabilitaHora;
      LaHora.Enabled := HabilitaHora;
      // Fim 2011-05-21
      ShowModal;
      if FSelecionou then
      begin
        DtSelect := Int(TPData.Date) + EdHora.ValueVariant;
        Result := True;
      end;
      Destroy;
    end;
  end;
end;

function TMyDBCheck.ObtemData_Juros_Multa(const MinData, DtDefault: TDateTime;
var DtSelect: TDateTime; const DefJuros, DefMulta: Double;
var Juros, Multa: Double): Boolean;
begin
  Result := False;
  if MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData) then
  begin
    with FmGetData do
    begin
      FMinData             := MinData;
      if DtDefault >= MinData then
        TPData.Date        := DtDefault
      else
        TPData.Date        := MinData;
      LaMulta.Visible      := True;
      EdMulta.Visible      := True;
      LaJuros.Visible      := True;
      EdJuros.Visible      := True;
      EdMulta.ValueVariant := DefMulta;
      EdJuros.ValueVariant := DefJuros;
      ShowModal;
      if FSelecionou then
      begin
        DtSelect := TPData.Date;
        Juros    := EdJuros.ValueVariant;
        Multa    := EdMulta.ValueVariant;
        Result   := True;
      end;
      Destroy;
    end;
  end;
end;

function TMyDBCheck.AcessoAoForm(Form, Descricao: String;
ModoAcesso: TAcessFmModo): Integer;

  function Privilegios(Usuario: Integer; ModoAcesso: TAcessFmModo): Boolean;
  begin
    FM_MASTER := 'F';
    DmodG.QrPerf.Close;
    DmodG.QrPerf.Params[0].AsInteger := Usuario;
    DmodG.QrPerf.Open;
    Result := DmodG.QrPerf.RecordCount > 0;
    if not Result and (ModoAcesso = afmoNegarComAviso) then
      Geral.MensagemBox('N�o h� privil�gios ' +
      'para o usu�rio ' + IntToStr(VAR_USUARIO) + ' - ' + VAR_LOGIN + '!',
      'Aviso', MB_OK+MB_ICONWARNING);
    // N�o pode fechar!!
    //DmodG.QrPerf.Close;
  end;
var
  Loc: Boolean;
begin
  if ModoAcesso = afmoSoMaster then
  begin
    Result := 0;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then Result := 9
    else Geral.MensagemBox('Acesso Restrito. Fale com a DERMATEK',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if ModoAcesso = afmoSoAdmin then
  begin
    Result := 0;
    if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) or
    ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    then Result := 9
    else Geral.MensagemBox('Acesso Restrito. Fale com o administrador!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) or
  ((VAR_LOGIN = 'ADMIN') and (VAR_SENHA = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
  or (Dmod.QrMasterSolicitaSenha.Value = 0) then Result := 9
  else if ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
  and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
  then Result := 8
  else begin
    Privilegios(VAR_USUARIO, ModoAcesso);
    Loc := DmodG.QrPerf.Locate('Janela', Form, [loCaseInsensitive]);
    if (DmodG.QrPerfOlha.Value = 0) or (Loc=False) then
    begin
      Result := 0;
      if (Form = '') or (Uppercase(Form) = 'MASTER') then
        Geral.MensagemBox('Somente o administrador tem acesso a este formul�rio!',
          'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION)
      else begin
        case ModoAcesso of
          afmoNegarSemAviso: ;// nada;  Msg=-1
          afmoNegarComAviso: //  Msg = 0 ou 2
            Geral.MensagemBox(VAR_ACESSONEGADO+Chr(13)+Chr(10)+'['+
            Form+']','Acesso a formul�rio', MB_OK+MB_ICONWARNING);
          (*
          afmoParcial:  // Msg = 1
            Geral.MensagemBox('Login com acesso parcial!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
          afmoParcialSemEdicao:   // Msg = 3
            Geral.MensagemBox('Login com acesso parcial, sem acesso a edi��o!',
            'Acesso a formul�rio', MB_OK+MB_ICONEXCLAMATION);
          *)
        end;
      end;
    end else Result := 1;
  end;
end;

function TMyDBCheck.LiberaPelaSenhaBoss: Boolean;
begin
  VAR_FSENHA := 5;
  if MyObjects.CriaForm_AcessoTotal(TFmSenha, FmSenha) then
  begin
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
end;

function TMyDBCheck.LiberaPelaSenhaAdmin(): Boolean;
begin
  VAR_FSENHA := 3;
  if MyObjects.CriaForm_AcessoTotal(TFmSenha, FmSenha) then
  begin
    FmSenha.ShowModal;
    FmSenha.Destroy;
  end;
  Result := VAR_SENHARESULT = 2;
end;

function TMyDBCheck.EscolheCodigo(Aviso, Caption, Prompt: String;
ListSource: TDataSource; ListField: String; Default: Variant): Variant;
// ObtemCodigo
begin
  if MyObjects.CriaForm_AcessoTotal(TFmSelCod, FmSelCod) then
  begin
    FmSelCod.STAviso.Caption  := Aviso;
    FmSelCod.Caption          := Caption;
    FmSelCod.LaPrompt.Caption := Prompt;
    //precisa ser antes
    FmSelCod.CBSel.ListField  := ListField;
    FmSelCod.CBSel.ListSource := ListSource;
    //
    if Default <> Null then
    begin
      FmSelCod.EdSel.ValueVariant := Default;
      FmSelCod.CBSel.KeyValue     := Default;
    end;
    FmSelCod.ShowModal;
    if FmSelCod.FSelected then
      Result := FmSelCod.EdSel.ValueVariant;
    FmSelCod.Destroy;
  end;
end;

function TMyDBCheck.ObtemCodUsuZStepCodUni(Tabela: TTabStepCod): Integer;
var
  NomeTab, DescriTab: String;
  //Mostra: Boolean;
begin
  Result := 0;
  //Mostra := true;
  case Tabela of
    tscEntidades:
    begin
      NomeTab := 'entidades';
      DescriTab := 'Cadastro de Pessoas F�sicas e Jur�dicas';
    end;
    tscEtqPrinCad:
    begin
      NomeTab := 'etqprincad';
      DescriTab := 'Cadastro Impressoras de Etiquetas';
    end;
    (*
    N�o precisa
    else begin
      Mostra := false;

    end;
    *)
  end;
  if DBCheck.CriaFm(TFmZStepCodUni, FmZStepCodUni, afmoNegarComAviso) then
  begin
    FmZStepCodUni.EdTabela.Text    := NomeTab;
    FmZStepCodUni.EdTabDescri.Text := DescriTab;
    FmZStepCodUni.ReopenZStepCod;
    FmZStepCodUni.ShowModal;
    Result := FmZStepCodUni.FResult;
    FmZStepCodUni.Destroy;
  end;
end;

function TMyDBCheck.Obtem_verProc: String;
begin
  Result := CO_SIGLA_APP + '_' + FormatFloat('0', CO_VERSAO)
end;
}
//
end.

