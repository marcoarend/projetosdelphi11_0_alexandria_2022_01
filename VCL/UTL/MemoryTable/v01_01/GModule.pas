unit GModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) ABSMain, UnDMkEnums, UnApp_Consts;

type
  TGMod = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CriaQv_GOTO;

  public
    { Public declarations }
    //
    function  NomeCompletoArquivo(Tabela, SubDir, Ext: String): String;
    function  SalvaXML_De_Tabela(Tabela, SubDir, Ext: String; Texto: String): Boolean;
    function  PostTable(Table: TABSTable; Arquivo, Mensagem: String): Boolean;

  end;

var
  GMod: TGMod;
  QvAgora     : TABSQuery;
  QvLocX      : TABSQuery;
  QvSelX      : TABSQuery;
  QvCountX    : TABSQuery;
  QvLivreX    : TABSQuery;
  QvDuplicStrX: TABSQuery;
  QvDuplicIntX: TABSQuery;
  QvInsLogX   : TABSQuery;
  QvDelLogX   : TABSQuery;
  QvUpdX      : TABSQuery;
  //
  QvAllY      : TABSQuery;
  QvUpdY      : TABSQuery;
  QvLivreY    : TABSQuery;
  QvUser      : TABSQuery;
  QvLocY      : TABSQuery;
  QvCountY    : TABSQuery;
  QvRecCountY : TABSQuery;
  QvSelY      : TABSQuery;
  QvDuplicStrY: TABSQuery;
  QvDuplicIntY: TABSQuery;
  QvInsLogY   : TABSQuery;
  QvDelLogY   : TABSQuery;

implementation

uses UnInternalConsts, dmkGeral, UnDMkProcFunc;

{$R *.DFM}

procedure TGMod.CriaQv_GOTO;
begin
  QvAgora := TABSQuery.Create(Gmod);
  QvAgora.DataBaseName := 'MEMORY';
  QvAgora.SQL.Add('SELECT NOW() Agora');
  //
  QvLocX := TABSQuery.Create(Gmod);
  QvLocX.DataBaseName := 'MEMORY';
  //
  QvUpdX := TABSQuery.Create(Gmod);
  QvUpdX.DataBaseName := 'MEMORY';
  //
  QvSelX := TABSQuery.Create(Gmod);
  QvSelX.DataBaseName := 'MEMORY';
  //
  QvCountX := TABSQuery.Create(Gmod);
  QvCountX.DataBaseName := 'MEMORY';
  //
  QvDuplicStrX := TABSQuery.Create(Gmod);
  QvDuplicStrX.DataBaseName := 'MEMORY';
  //
  QvDuplicIntX := TABSQuery.Create(Gmod);
  QvDuplicIntX.DataBaseName := 'MEMORY';
  //
  QvInsLogX := TABSQuery.Create(Gmod);
  QvInsLogX.DataBaseName := 'MEMORY';
  QvInsLogX.SQL.Add('INSERT INTO logs SET Data=NOW(), Tipo=:P0, ');
  QvInsLogX.SQL.Add('Usuario=:P1, ID=:P2');
  //
  QvDelLogX := TABSQuery.Create(Gmod);
  QvDelLogX.DataBaseName := 'MEMORY';
  QvDelLogX.SQL.Add('DELETE FROM logs WHERE Tipo=:P0');
  QvDelLogX.SQL.Add('AND Usuario=:P1 AND ID=:P2');
  //
  QvInsLogY := TABSQuery.Create(Gmod);
  QvInsLogY.DataBaseName := 'MEMORY';
  QvInsLogY.SQL.Add('INSERT INTO logs SET Data=NOW(), Tipo=:P0, ');
  QvInsLogY.SQL.Add('Usuario=:P1, ID=:P2');
  //
  QvDelLogY := TABSQuery.Create(Gmod);
  QvDelLogY.DataBaseName := 'MEMORY';
  QvDelLogY.SQL.Add('DELETE FROM logs WHERE Tipo=:P0');
  QvDelLogY.SQL.Add('AND Usuario=:P1 AND ID=:P2');
  //
  QvLivreX := TABSQuery.Create(Gmod);
  QvLivreX.DataBaseName := 'MEMORY';
  //
  QvLocY := TABSQuery.Create(Gmod);
  QvLocY.DataBaseName := 'MEMORY';
  //
  QvUser := TABSQuery.Create(Gmod);
  QvUser.DataBaseName := 'MEMORY';
  QvUser.SQL.Add('SELECT Login FROM senhas');
  QvUser.SQL.Add('WHERE Numero=:Usuario');
  //
  QvCountY := TABSQuery.Create(Gmod);
  QvCountY.DataBaseName := 'MEMORY';
  //
  QvUpdY := TABSQuery.Create(Gmod);
  QvUpdY.DataBaseName := 'MEMORY';
  //
  QvLivreY := TABSQuery.Create(Gmod);
  QvLivreY.DataBaseName := 'MEMORY';
  //
  QvRecCountY := TABSQuery.Create(Gmod);
  QvRecCountY.DataBaseName := 'MEMORY';
  //
  QvSelY := TABSQuery.Create(Gmod);
  QvSelY.DataBaseName := 'MEMORY';
  //
  QvDuplicStrY := TABSQuery.Create(Gmod);
  QvDuplicStrY.DataBaseName := 'MEMORY';
  //
  QvDuplicIntY := TABSQuery.Create(Gmod);
  QvDuplicIntY.DataBaseName := 'MEMORY';
  //
  QvAllY := TABSQuery.Create(Self);
  QvAllY.DataBaseName := 'MEMORY';
  //
end;

procedure TGMod.DataModuleCreate(Sender: TObject);
begin
  CriaQv_GOTO;
end;


function TGMod.NomeCompletoArquivo(Tabela, SubDir, Ext: String): String;
var
  Diretorio: String;
begin
  if SubDir <> '' then
    Diretorio := CO_DB_APP_PATH + SubDir + '\'
  else
    Diretorio := CO_DB_APP_PATH;
  //
  if not DirectoryExists(Diretorio) then
  try
    ForceDirectories(Diretorio);
  except
    Geral.MB_Erro('N�o foi pos�vel criar o diret�rio: ' + sLineBreak +
    Diretorio);
  end;
  Result := Diretorio + Tabela + '.' + Ext;
end;

function TGMod.PostTable(Table: TABSTable; Arquivo, Mensagem: String): Boolean;
begin
  try
    Table.Post;
  except
    on E: Exception do
    begin
      Geral.MB_Erro('ERRO ao carregar tabela: ' + sLineBreak +
      'Arquivo f�sico:' + Arquivo + sLineBreak +
      'Tabela em mem�ria: ' + Table.TableName + sLineBreak +
      Geral.ATS_If(Mensagem <> '', [
        'Mensagem janela: ' + sLineBreak + Mensagem + sLineBreak]) +
      'Mensagem sistema: ' + sLineBreak + E.Message);
    end;
  end;
end;

function TGMod.SalvaXML_De_Tabela(Tabela, SubDir, Ext: String; Texto: String): Boolean;
const
  Exclui = True;
var
  Arquivo: String;
  Txt: String;
  InputString: string;
begin
  Arquivo := NomeCompletoArquivo(Tabela, SubDir, Ext);
  Txt := StringReplace(Texto,'amp;', '', [rfReplaceAll]);
  Result := Geral.SalvaTextoEmArquivo(Arquivo, Txt, Exclui, fetUTF_8);
end;

end.

