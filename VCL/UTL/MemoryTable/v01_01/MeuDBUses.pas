unit MeuDBUses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, DBGrids, Buttons, StdCtrls, Menus, DB, dmkGeral,
  mySQLDbTables, frxClass, frxDBSet, frxCross, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkEditF7, TypInfo, dmkDBEdit, UnDmkProcFunc, UnDmkEnums,
  ABSMain;

type
  TFmMeuDBUses = class(TForm)
    PnImprime: TPanel;
    Grade: TStringGrid;
    DBGrid1: TDBGrid;
    PnConfirma: TPanel;
    Button1: TButton;
    BtImprime: TBitBtn;
    Button2: TButton;
    PMImpressao: TPopupMenu;
    Visualizaimpresso1: TMenuItem;
    DesignerModificarelatrioantesdeimprimir1: TMenuItem;
    ImprimeImprimesemvizualizar1: TMenuItem;
    frxReport1: TfrxReport;
    frxCrossObject1: TfrxCrossObject;
    frxReport2: TfrxReport;
    frxDsPG: TfrxDBDataset;
    DsPG: TDataSource;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    PnInativo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TEdit;
    EdCPF: TEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    dmkDBLookupComboBox1: TdmkDBLookupComboBox;
    dmkEditCB1: TdmkEditCB;
    dmkEditF71: TdmkEditF7;
    TbPG: TABSTable;
    QrUpd: TABSQuery;
    procedure Button2Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Visualizaimpresso1Click(Sender: TObject);
    procedure DesignerModificarelatrioantesdeimprimir1Click(Sender: TObject);
    procedure ImprimeImprimesemvizualizar1Click(Sender: TObject);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
  private
    { Private declarations }
    FPrintGrid: String;
    FTitulo: String;
    FGrade: TStringGrid;
    FDBGrid: TDBGrid;
    FTipoGrade: Integer;
    FContaCol: Integer;
    procedure Imprime_frx2(Mostra: Byte);
  public
    { Public declarations }
    //function  ConectaDBTest(): Boolean;
    procedure ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
    procedure ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
    procedure ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
    procedure PesquisaNome(Tabela, CampoNome, CampoCodigo: String;
              DefinedSQL: String = '');
    procedure VirtualKey_F7(Texto: String);
    procedure VirtualKey_F5();
    procedure ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor: Variant;
              var UpdType: TUpdType);
  end;

var
  FmMeuDBUses: TFmMeuDBUses;
  PID_DB_TEST: TmySQLDatabase;
  AID_DB_TEST: TmySQLDatabase;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnMsgInt, Printers, SelRadioGroup,
  UnAppCreate, UMemModule, GModule;

{$R *.dfm}

{ TFmMeuDBUses }

procedure TFmMeuDBUses.BtImprimeClick(Sender: TObject);
begin
  Imprime_frx2(1);
end;

procedure TFmMeuDBUses.Button1Click(Sender: TObject);
begin
  Hide;
  PnImprime.Visible := False;
  PnInativo.Visible := True;
end;

procedure TFmMeuDBUses.Button2Click(Sender: TObject);
begin
  //FmPrincipal.SalvaArquivo(EdNome, EdCPF, Grade, Date, 'Dermatek.ini', True);
end;

procedure TFmMeuDBUses.DesignerModificarelatrioantesdeimprimir1Click(
  Sender: TObject);
begin
  Imprime_Frx2(1);
end;

procedure TFmMeuDBUses.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    if FTipoGrade = 0 then
    begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[j - 1, i - 1]]);
    end else begin
      for i := 1 to FGrade.RowCount do
        for j := 1 to FGrade.ColCount do
          Cross.AddValue([i], [j], [FGrade.Cells[i - 1, j - 1]]);
    end;
  end;
end;

procedure TFmMeuDBUses.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'Titulo' then Value := FTitulo;
end;

procedure TFmMeuDBUses.ImprimeDBGrid(DBGrid: TDBGrid; Titulo: String);
var
  Query: TmySQLQuery;
begin
  Query := TmySQLQuery(DBGrid.DataSource.DataSet);
  frxDsPG.DataSet := Query;
  //
  FTipoGrade := 1; // Grade com origem de DataAware. DBGrid ...
  FTitulo := Titulo;
  FDBGrid := DBGrid;
  FGrade := TStringGrid(DBGrid);
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.ImprimeImprimesemvizualizar1Click(Sender: TObject);
begin
  Imprime_Frx2(2);
end;

procedure TFmMeuDBUses.ImprimeStringGrid(StringGrid: TStringGrid; Titulo: String);
const
  Ativo = 1;
  Arquivo = 'StringGrid';
var
  i, j, c, r: Integer;
  Xt: array of String;
  Xv: array of String;
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  c := StringGrid.ColCount;
  r := StringGrid.RowCount;
  // 2012-02-24
  //ConectaDBTest();
  //QrUpd.Database := DBTest;
  (*
  QrUpd.Database := DmodG.MyPID_DB;
  FPrintGrid := UCriaEDrop.RecriaDropedTab(ntrtt_PrintGrid, QrUpd, c);
  *)


  //
  if TbPG.Exists then
    TbPG.DeleteTable;
  AppCreate.RecriaMemTable(ntcPrintGrid,  '_PrintGrid_', QrUpd, False, False, c,
  '_PrintGrid_');
  //AppCreate.RecriaMemTable(ntcPrintGrid, '_PrintGrid_', True, False, c);
  TbPG.TableName := '_PrintGrid_';
  UMemMod.AbreABSTable1(TbPG);
  //



  SetLength(Xt, StringGrid.ColCount);
  SetLength(Xv, StringGrid.ColCount);
  for i := 0 to c - 1 do
    Xt[i] := 'Col' + FormatFloat('000', i);
  for i := 0 to r - 1 do
  begin
    for j := 0 to c - 1 do
      Xv[j] := StringGrid.Cells[j,i];
    //
(*
    QrUpd.SQL.Clear;
    QrUpd.SQL.Add('INSERT INTO ' + FPrintGrid + ' SET ');
    for j := 0 to c - 2 do
      QrUpd.SQL.Add(Xt[j] + '="' + dmkPF.DuplicaAspasDuplas(Xv[j]) + '",');
    QrUpd.SQL.Add(Xt[c-1] + '="' + dmkPF.DuplicaAspasDuplas(Xv[c-1]) + '"');

    QrUpd.ExecSQL;
*)
    TbPg.Insert;
    for j := 0 to c - 2 do
      TbPg.FieldByName(Xt[j]).AsString := Xv[j];
    TbPg.FieldByName(Xt[c-1]).AsString := Xv[c-1];
    //
    TbPg.FieldByName('Ativo').AsInteger    := Ativo;
    GMod.PostTable(TbPg, Arquivo, 'AbreListaDePrecosDeProdutos');
    //
  end;
  FGrade  := StringGrid;
  FDBGrid := nil;
  FTitulo := Titulo;
  // 2012-02-24
  //QrPg.Database   := DBTest;
  //frxDsPG.DataSet := TbPG;
  TbPg.Close;
  TbPg.DatabaseName   := 'MEMORY';
  TbPg.Open;
  frxDsPG.DataSet := TbPG;
  //  fim 2012-02-24
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.ImprimeStringGrid_B(StringGrid: TStringGrid; Titulo: String);
begin
  FTipoGrade := 0; // // Grade com origem de Texto. TStringGrid...
  FGrade := StringGrid;
  FTitulo := Titulo;
  MyObjects.MostraPopUpNoCentro(PMImpressao);
end;

procedure TFmMeuDBUses.Imprime_frx2(Mostra: Byte);
const
  PagePagHei = 279;

  PageBanWid = 0;
  PagePagTop = 117; // >(=1,01) - (1,91 = 200)
  PagePagFot = 100;

  PageBanLef = 100;
  PageBanTop = 0;
  //PageBanWid = 0;
  PageBanHei = 50;
  PageMemHei = 50;
  PageColWid = 210;

var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  Foot: TfrxPageFooter;
  BANLEF, BANTOP, BANWID, BANHEI,
  //BARLEF, BARTOP, BARWID, BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align,
  i,
  L, T, W, H, (*k, *)TopoIni: Integer;
  //Fator: Double;
  Campo: String;
  Menos: Byte;
  Orientacao: Integer;
begin
  Orientacao := MyObjects.SelRadioGroup('Orienta��o de Impress�o',
  'Selecione a orienta��o', ['Retrato', 'Paisagem'], 1);
  if not (Orientacao in ([0,1])) then
    Exit;
  Menos := 0;
  //k := -1;
  // Configura p�gina
  while frxReport2.PagesCount > 0 do
    frxReport2.Pages[0].Free;
  Page := TfrxReportPage.Create(frxReport2);
  Page.CreateUniqueName;
  Page.LeftMargin   := 20;
  Page.RightMargin  := 10;
  Page.TopMargin    := 10;
  Page.BottomMargin := 10;
  Page.Height       := Trunc(PagePagHei / VAR_DOTIMP);
  case Orientacao of 
    0: Page.Orientation := poPortrait;
    1: Page.Orientation := poLandscape;
  end;
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(PageBanWid / VAR_DOTIMP);
  H := Trunc(PagePagTop / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///
  ///  TITULO DO RELAT�RIO
  FContaCol := 0;
  //TopoIni    := 0;
  MEMTOP := 0;
  MEMHEI := 24;//Trunc(PageMemHei / VAR_DOTIMP); 1,59 = 60 0,59=
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 1800;
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 12;
  //
  Memo.Memo.Text := FTitulo;
{
  if CkGrade.Checked then
  begin
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.TopLine.Width := 0.1;
    Memo.Frame.LeftLine.Width := 0.1;
    Memo.Frame.RightLine.Width := 0.1;
    Memo.Frame.BottomLine.Width := 0.1;
  end;
}
  ///  T�tulos dos campos
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    //  TITULOS DAS COLUNAS
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i+Menos-1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni + 24;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 10;
    //
    //if frxDsPG.DataSet = TbPG then
      //Memo.Memo.Text := 'Teste 1'
    //else begin
    if FDBGrid <> nil then
      Memo.Memo.Text := FDBGrid.Columns[i].Title.Caption
    else
    if FGrade <> nil then
      Memo.Memo.Text := Geral.FF0(I)//FGrade.Cells[I, 0]
    else
      Memo.Memo.Text := 'Titulo?';
    //end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  // Configura Banda de
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(PageBanTop / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PageBanHei / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.DataSet := frxDsPG;
  {
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  }
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  TopoIni    := 0;
  MEMHEI := Trunc(PageMemHei / VAR_DOTIMP);
  MEMLEF := Trunc(PageBanLef / VAR_DOTIMP);
  MEMWID := 0;
  case FTipoGrade of
    0: Menos := 1;
    1: Menos := 2;
  end;
  for i := 0 to FGrade.ColCount - Menos do
  begin
    MEMLEF := MEMLEF + MEMWID;
    MEMWID := Trunc(FGrade.ColWidths[i + Menos - 1]);
    //
    //inc(k, 1);
    MEMTOP := TopoIni;// + (k * MEMHEI);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.Visible := True;
    Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := 10;
    //
    if frxDsPG.DataSet = TbPG then
      Memo.Memo.Text := '[frxDsPG."Col' + FormatFloat('000', i) + '"]'
    else
    begin
      Campo := FDBGrid.Columns[i].FieldName;
      Memo.Memo.Text := '[frxDsPG."' + Campo + '"]';
    end;
    //if CkGrade.Checked then
    //begin
      Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      Memo.Frame.TopLine.Width := 0.1;
      Memo.Frame.LeftLine.Width := 0.1;
      Memo.Frame.RightLine.Width := 0.1;
      Memo.Frame.BottomLine.Width := 0.1;
    //end;
  end;

  {
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_TXT1]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_NOME]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  }
  //////////////////////////////////////////////////////////////////////////////
  // Configura Foota
  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(269 / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PagePagFot / VAR_DOTIMP);
  Foot := TfrxPageFooter.Create(Page);
  Foot.CreateUniqueName;
  Foot.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
{$IFNDEF NAO_USA_FRX}
  case Mostra of
    0: MyObjects.frxMostra(frxReport2, FTitulo);
    1: frxReport2.DesignReport();
    2: MyObjects.frxImprime(frxReport2, FTitulo);
  end;
{$ELSE}
  Geral.MB_Info('"FRX" sem "DEFINE"!');
{$ENDIF}

end;

procedure TFmMeuDBUses.MenuItem1Click(Sender: TObject);
begin
  Visualizaimpresso1Click(Self);
end;

procedure TFmMeuDBUses.MenuItem2Click(Sender: TObject);
begin
  DesignerModificarelatrioantesdeimprimir1Click(Self);
end;

procedure TFmMeuDBUses.MenuItem3Click(Sender: TObject);
begin
  ImprimeImprimesemvizualizar1Click(Self);
end;

procedure TFmMeuDBUses.ObtemValorDe_dmkDBEdit(const Objeto: TObject; var Valor:
  Variant; var UpdType: TUpdType);
var
  Campo: String;
begin
  Campo := GetPropValue(Objeto, 'UpdCampo');
  try
  Valor := TmySQLDataSet(TDataSource(TdmkDBEdit(Objeto).DataSource).
    DataSet).FieldByName(Campo).Value;
  UpdType := TdmkDBEdit(Objeto).UpdType;
  except
    Geral.MB_Erro('N�o foi poss�vel obter o "Tabela.Campo.Valor" do TDBEdit! ' +
    sLineBreak + 'TDBEdit: ' + TdmkDBEdit(Objeto).Name +
    sLineBreak + 'Campo: ' + TdmkDBEdit(Objeto).DataField);
    //
    raise;
  end;
end;

procedure TFmMeuDBUses.PesquisaNome(Tabela, CampoNome, CampoCodigo, DefinedSQL: String);
begin
  VAR_CADASTRO := 0;
  //
(*
  MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
  //
  FmPesqNome.LaDoc.Visible := False;
  FmPesqNome.EdDoc.Visible := False;
  //
  FmPesqNome.FND              := True;
  FmPesqNome.FNomeTabela      := Tabela;
  FmPesqNome.FNomeCampoNome   := CampoNome;
  FmPesqNome.FNomeCampoCodigo := CampoCodigo;
  FmPesqNome.FDefinedSQL      := DefinedSQL;
  FmPesqNome.ShowModal;
  FmPesqNome.Destroy;
*)
end;

procedure TFmMeuDBUses.Visualizaimpresso1Click(Sender: TObject);
begin
  Imprime_Frx2(0);
end;

procedure TFmMeuDBUses.VirtualKey_F5();
var
  Controle: TControl;
  OK: Boolean;
  DBLookupComboBox: TDBLookupComboBox;
  DataSource: TDataSource;
  DataSet: TDataSet;
begin
  if Screen.ActiveForm = nil then Exit;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB)(* or
    (Controle is TdmkEditF7)*);
    if OK then
    begin
      if Controle is TDBLookupComboBox then
        DBLookupComboBox := TDBLookupComboBox(Controle)
      else
      if Controle is TdmkDBLookupComboBox then
        DBLookupComboBox := TdmkDBLookupComboBox(Controle)
      else
      if Controle is TdmkEditCB then
        DBLookupComboBox := TdmkDBLookupComboBox(TdmkEditCB(Controle).DBLookupComboBox)
      else
      //if Controle is TdmkEditF7 then
        DBLookupComboBox := nil;
      //
      if DBLookupComboBox <> nil then
      begin
        DataSource := TDataSource(DBLookupComboBox.ListSource);
        if DataSource <> nil then
        begin
          DataSet := TDataSet(DataSource.DataSet);
          if DataSet <> nil then
          begin
            if DataSet is TmySQLDataSet then
            begin
              // Evitar quebra de seguran�a!
              // S� abrir se j� estiver aberto!
              if TdataSet(DataSet).State <> dsInactive then
              begin
                TdataSet(DataSet).Close;
                TdataSet(DataSet). Open ;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmMeuDBUses.VirtualKey_F7(Texto: String);
var
  Controle: TControl;
  OK: Boolean;
  LS: TDataSource;
  Qr: TmySQLQuery;
  dmkCB: TDmkDBLookupComboBox;
  dmkEd: TdmkEditCB;
begin
(*
  if Screen.ActiveForm = nil then Exit;
  VAR_CaptionFormOrigem := TForm(Screen.Activeform).Caption;
  Controle := Screen.ActiveForm.ActiveControl;
  if Controle <> nil then
  begin
    OK :=  (Controle is TDBLookupComboBox) or
    (Controle is TdmkDBLookupComboBox) or
    (Controle is TdmkEditCB) or
    (Controle is TdmkEditF7);
    if OK then
    begin
      VAR_CADASTRO := 0;
      MyObjects.CriaForm_AcessoTotal(TFmPesqNome, FmPesqNome);
      FmPesqNome.FTexto := Texto;
      if Controle is TdmkEditF7 then
      begin
        {
        if (Controle is TdmkEditF7) then
          Controle := TdmkEditF7(Controle).dmkEditCod;
        }
        FmPesqNome.FF7 := TdmkEdit(Controle);
        FmPesqNome.FCB := nil;
        VAR_NomeCompoF7 := TdmkEdit(Controle).Name;
      end else
      begin
        if (Controle is TdmkEditCB) then
          Controle := TdmkEditCB(Controle).DBLookupComboBox;
        FmPesqNome.FCB := TDBLookupComboBox(Controle);
        FmPesqNome.FF7 := TdmkEdit(Controle);
        VAR_NomeCompoF7 := TDBLookupComboBox(Controle).Name;
      end;
      FmPesqNome.FDB := nil;
      if (FmPesqNome.FCB <> nil) and (FmPesqNome.FCB.ListSource <> nil) then
      begin
        LS := TDataSource(FmPesqNome.FCB.ListSource);
        if (LS <> nil) and (LS.Dataset <> nil) then
        begin
          Qr := TmySQLQuery(LS.DataSet);
          if (Qr <> nil) and (Qr.Database <> nil) then
            FmPesqNome.FDB := Qr.Database;
        end;
      end;
      if FmPesqNome.FDB = nil then
        FmPesqNome.FDB := Dmod.MyDB;
      FmPesqNome.ShowModal;
      if VAR_CADASTRO <> 0 then
      begin
        if FmPesqNome.FCB <> nil then
        begin
          if Controle is TdmkDBLookupComboBox then
          begin
            dmkCB := TdmkDBLookupComboBox(Controle);
            if dmkCB <> nil then
            begin
              if dmkCB.ListField = dmkCB.KeyField then
                dmkCB.KeyValue := VAR_CAD_NOME
              else
                dmkCB.KeyValue := VAR_CADASTRO;
              dmkEd := TdmkEditCB(dmkCB.dmkEditCB);
              if dmkEd <> nil then
              begin
                dmkEd.ValueVariant := VAR_CADASTRO;
                dmkEd.OnFim;
              end;
            end;
          end else
            TDBLookupComboBox(Controle).KeyValue := VAR_CADASTRO;
        end
        else
        if FmPesqNome.FF7 <> nil then
        begin
          TdmkEditF7(Controle).Text := VAR_CAD_NOME;
          if TdmkEditF7(Controle).dmkEditCod <> nil then
          begin
            //Show Message(TdmkEditF7(Controle).Name);
            //Show Message(TdmkEdit(TdmkEditF7(Controle).dmkEditCod).Name);
            TdmkEdit(TdmkEditF7(Controle).dmkEditCod).ValueVariant := VAR_CADASTRO;
          end;
        end;
      end;
      VAR_CaptionFormOrigem := '';
      VAR_NomeCompoF7 := '';
      FmPesqNome.Destroy;
    end;
  end;
*)
end;

end.
