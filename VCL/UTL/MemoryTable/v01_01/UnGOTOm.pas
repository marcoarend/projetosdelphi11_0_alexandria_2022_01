unit UnGOTOm;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Math, Db, DbCtrls,
  UnInternalConsts2, Mask, ComCtrls, (*DBTables,*) ResIntStrings, (*DBIProcs,*)
  Registry, ZCF2, buttons, ABSMain, Grids, DBGrids, TypInfo, Winsock, IniFiles,
  Variants, dmkGeral, UnDmkProcFunc, UnMyObjects, UnDmkEnums;

type
  TUnGOTOm = class(TObject)
  private
    { Private declarations }
    function LocalizaCodigo(Atual, Codigo: Integer): Boolean;
{
    function LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
    function LocalizaTxtUsu(Atual, Codigo: String): Boolean;
}
    function Vai(Para: TVaiPara; Atual: Integer; Sort: String) : String;
  public
    { Public declarations }
    function TxtLaRegistro(RecCount, Increm : Integer) : String;
    function Go(Para: TVaiPara; Atual: Integer; Sort: String) : String;
{
    function VaiNumTxt(Para: TVaiPara; Atual, Sort: String) : String;
    function VaiTxt(Para: TVaiPara; Atual, Sort: String) : String;
    function VaiData(Para: TVaiPara; Atual: TDate; Sort: String) : String;
    function VaiInt(Para: TVaiPara; Qry: TM y S Q LQuery; Tabela, Campo, Extra:
             String; Negativo: Boolean): Boolean;
}
    function LC(Atual, Codigo: Integer): Boolean;
{
    function LocalizaNumTxt(Atual, Codigo: String): Boolean;
    function LocalizaTxt(Atual, Texto: String): Boolean;
    function LocalizaPriorNextNumTxt(Tabela, Campo, Atual: String;
             DataBase: TM y S Q LDataBase): String;
    function LocalizaPriorNextIntQr(Tabela: TM y S Q LQuery;
             Campo: TIntegerField; Atual: Integer): Integer;
    function LocalizaPriorNextTxtQr(Tabela: TM y S Q LQuery;
             Campo: TWideStringField; Atual: String): String;
    function LocalizaPriorNextInt_SQL(Tabela, Campo: String; Atual: Integer;
             DataBase: TM y S Q LDataBase): Integer;
    function LocalizaData(Atual, Data: TDate): Boolean;
    function BtEnabled(Codigo: Integer; PodeNeg: Boolean): Boolean;
    function BtEnabledNumTxt(Codigo: String; PodeNeg: Boolean): Boolean;
}
    function Nome(Padrao: String): String;
{
    function NomeDireto(Padrao: String): String;
    function CodigoTxt(Padrao: String): String;
}
    function Codigo(Atual: Integer; Padrao: String): String;
{
    function CodUsu(Atual: Integer; Padrao: String): String;
    function TxtUsu(Atual, Padrao: String): String;
    function NumTxt(Atual, Padrao: String): String;
    function Registros(Tabela: TM y S Q LQuery): Integer;
    function MyLocate(DataBase: TM y S Q LDataBase; Tabela: TM y S Q LQuery;
             Campo: String; Valor: Variant; Grid: TDBGrid): Boolean;
    function Destrava(DataFinal: TDateTime): Boolean;
    function CriaFmUnLock_MLA: Boolean;
    function VerificaTipoDaCarteira(Carteira: Integer): Integer;
    ////////////////////////////////////////////////////////////////////////////
    procedure LiberaUso;
    procedure LiberaUso2;
    procedure VerificaLiberacao;
}
    procedure LimpaVAR_SQL();
    procedure BotoesSb(LaTipo: String); overload;
    procedure BotoesSb(SQLType: TSQLType); overload;
    function CriaLocalizador(Codigo: Integer): Boolean;
{
    function CriaLocalizadorCodUsu(Codigo: Integer): Boolean;
    function CriaLocalizadorTxtUsu(Codigo: String): Boolean;
    procedure CriaLocalNumTxt(Codigo: String);
    procedure CriaLocalTxt(Texto: String);
    procedure CriaLocalizadorData(Data: TDate);
    procedure VerificaTerminal;
    function ConectaAoServidor(IP: String; HostPort, CancelTimeMs: Word): Boolean;
    procedure DefineSenhaBoss;
    function DefineSenhaUsuario(Login: String): String;
    procedure AnaliseSenha(EdLogin, EdSenha: TEdit; FmMLA, FmMain: TForm;
              Componente: TComponent; ForcaSenha: Boolean; Memo: TRichEdit;
              QrGeraSenha: TM y S Q LQuery);
    procedure AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit; FmMLA, FmMain: TForm;
              Componente: TComponent; ForcaSenha: Boolean; Memo: TRichEdit;
              QrGeraSenha, DmodFinQrCarts: TM y S Q LQuery);
    function OM y S Q LEstaInstalado(StaticText: TStaticText; ProgessBar:
             TProgressBar): Boolean;//VerificaInstalacaoDoM y S Q L;
    function DefineSenhaDia(Numero: Integer; Forcar: Boolean; Query:
             TM y S Q LQuery): Boolean;
    procedure EmiteRecibo(Emitente, Beneficiario: Integer; Valor, ValorP,
              ValorE: Double; NumRecibo, Referente, ReferenteP, ReferenteE:
              String; Data: TDateTime; Sit: Integer);// Sit >> 2010-03-13
    function SenhaDesconhecida: Boolean;
    function MudaSenhaBD(Senha, Usuario: String; MsgErr: Boolean): Boolean;
    function TabelaExiste(Tabela: String; Database: TM y S Q LDatabase): Boolean;
    function EnderecoDeEntidade(Entidade, TipoDeEndereco: Integer): String;
    function EnderecoDeEntidade1(Entidade, TipoDeEndereco: Integer): String;
    function EnderecoDeEntidade2(Entidade, TipoDeEndereco: Integer): String;
    function ItemDeEndereco(Item, Valor: String): String;
    function DefinePathM y S Q L: Boolean;
}
    procedure DefineBaseDados(Qry: TABSQuery);
    function  Fechar(SQLType: TSQLType) : TCloseAction; overload;
    procedure LeMeuSQL_m(Query: TABSQuery);
    procedure LeMeuSQL_Fixo_m(Query: TABSQuery; Arquivo: String; Memo: TMemo;
              WinArq, AskShow: Boolean);
    procedure LeMeuSQLForce_m(Query: TABSQuery; Arquivo: String; Memo: TMemo;
              WinArq, AskShow, Force: Boolean);

  end;

var
  GOTOm: TUnGOTOm;
  //VAR_FILME: String[8];

implementation


uses GModule, NomeX
{
Module, UnLock_MLA, UM y S Q LModule, SenhaBoss, InstallM y S Q L41,
Recibo, Recibos, Principal,
Servidor, Senha, Terminais, ServiceManager, ServicoManager, ServerConnect,
AvisoIndef, ModuleGeral};


////////////////////////INICIO CONTROLE GERAL BD \\\\\\\\\\\\\\\\\\\\\\\\\

function TUnGOTOm.TxtLaRegistro(RecCount, Increm : Integer) : String;
var
  Total : Integer;
  CountTxt: String;
begin
  QvCountY.Close;
  DefineBaseDados(QvCountY);

  QvCountY.SQL.Clear;
  QvCountY.SQL.Add('SELECT COUNT(*) Record From '+Lowercase(VAR_GOTOTABELA)+'');
  QvCountY.Open;
  Total := QvCountY.FieldByName('Record').AsInteger;
  QvCountY.Close;
  case Increm of
    -2 : VAR_RECNO := 0;
     0 : if Total > 0 then VAR_RECNO := 1 else VAR_RECNO := 0;
     2 : VAR_RECNO := Total;
     else VAR_RECNO := VAR_RECNO + Increm;
  end;
  if VAR_RECNO > RecCount then VAR_RECNO := RecCount;
  if VAR_RECNO < 0 then VAR_RECNO := 0;
  if (RecCount > 0) and (VAR_RECNO = 0) then VAR_RECNO := 1;
  if RecCount = -1 then CountTxt := '?'
  else CountTxt := IntToStr(RecCount);
  Result := (*IntToStr(VAR_RECNO)+CO_DE+*)CountTxt+CO_DE+IntToStr(Total);
end;

function TUnGOTOm.Go(Para: TVaiPara; Atual: Integer; Sort: String): String;
begin
  Result := Vai(Para, Atual, Sort);
end;

function TUnGOTOm.Vai(Para: TVaiPara; Atual: Integer; Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOABSTABLE.First else
    if Para = vpPrior then VAR_GOTOABSTABLE.Prior else
    if Para = vpNext then VAR_GOTOABSTABLE.Next else
    if Para = vpLast then VAR_GOTOABSTABLE.Last else
    if Para = vpThis then
      if not VAR_GOTOABSTABLE.Locate('Periodo', Geral.Periodo2000(Date), []) then
        Geral.MensagemBox('Per�odo atual n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    DefineBaseDados(QvLocY);
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      (*
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      *)
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocY.SQL.Add('AND ')
        else
          QvLocY.SQL.Add('WHERE ');
        QvLocY.SQL.Add(VAR_GOTOCAMPO+' > 0');
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      (*
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
      end;
      *)
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      QvLocY.Params[0].AsInteger := Atual;
      Increm := -1;
      Quando := True;
    end else
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      (*
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('AND ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
      end;
      *)
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      QvLocY.Params[0].AsInteger := Atual;
      Increm := 1;
      Quando := True;
    end else
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      (*
      if VAR_SQLy.Count > 0 then
      begin
        QvLocY.SQL.Add('WHERE ');
        for I := 0 to VAR_SQLy.Count -1 do
          QvLocY.SQL.Add(VAR_SQLy[I]);
        Quando := True;
      end;
      *)
      if VAR_GOTONEG <> gotoAll then
      begin
        if Quando then
          QvLocY.SQL.Add('AND ')
        else
          QvLocY.SQL.Add('WHERE ');
        QvLocY.SQL.Add(VAR_GOTOCAMPO+' > 0');
        Quando := True;
      end;
      Increm := 2;
    end else
    if Para = vpThis then
    begin
      QvLocY.SQL.Add('SELECT '+VAR_GOTOCAMPO+' Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+'='+IntToStr(Geral.Periodo2000(Date)));
      Quando := True;
      if VAR_GOTONEG <> gotoAll then
        QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      Increm := 0;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    QvLocY.Open;
    QvLocY.First;
    if (QvLocY.RecordCount > 0) then
    begin
      if (QvLocY.FieldByName('Record').AsString <> '') then
        if (QvLocY.FieldByName('Record').AsInteger <> null) then
          LocalizaCodigo(Atual, QvLocY.FieldByName('Record').AsInteger);
    end else
    begin
      if Para = vpThis then
      begin
        // Colocar a mensagem no localizador de per�odo
        //Geral.MensagemBox(PChar(VAR_PERIODO_NAO_LOC + ': Atual'), 'Aviso',
          //MB_OK+MB_ICONWARNING);
        Result := VAR_PERIODO_NAO_LOC;
        QvLocY.Close;
        Exit;
      end;
    end;
    QvLocY.Close;
  end;
  if VAR_GOTOABSTABLE.State = dsBrowse then Qtd := VAR_GOTOABSTABLE.RecordCount;
  Result := Seq + TxtLaRegistro(Qtd, increm);
end;

{
function TUnGOTOm.VaiNumTxt(Para: TVaiPara; Atual, Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOABSTABLE.First;
    if Para = vpPrior then VAR_GOTOABSTABLE.Prior;
    if Para = vpNext then VAR_GOTOABSTABLE.Next;
    if Para = vpLast then VAR_GOTOABSTABLE.Last;
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    DefineBasedados(QvLocY);
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if not VAR_GOTONEG then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' > 0');
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      if not VAR_GOTONEG then QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      QvLocY.Params[0].AsString := Atual;
      Increm := -1;
      Quando := True;
    end;
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      if not VAR_GOTONEG then QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      QvLocY.Params[0].AsString := Atual;
      Increm := 1;
      Quando := True;
    end;
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if not VAR_GOTONEG then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' > 0');
        Quando := True;
      end;
      Increm := 2;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    QvLocY.Open;
    if QvLocY.RecordCount > 0 then
      if QvLocY.FieldByName('Record').AsString <> '' then
        LocalizaNumTxt(Atual, QvLocY.FieldByName('Record').AsString);
    QvLocY.Close;
  end;
  if VAR_GOTOABSTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOABSTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOm.VaiTxt(Para: TVaiPara; Atual, Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Limit: Integer;
  Sentido: String;
begin
  Limit := 0;
  Qtd := 0;
  increm := 0;
  Sentido := '';
  if Sort = 'A' then
  begin
    case Para of
      vpFirst: VAR_GOTOABSTABLE.First;
      vpPrior: VAR_GOTOABSTABLE.Prior;
      vpNext : VAR_GOTOABSTABLE.Next;
      vpLast : VAR_GOTOABSTABLE.Last;
    end;
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[1...]: ';
    QvLocY.Close;
    DefineBaseDados(QvLocY);
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <= "'+Atual+'"');
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <= "'+Atual+'"');
      Increm := -1;
      Limit := 2;
      Sentido := 'DESC';
    end;
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >= "'+Atual+'"');
      Increm := 1;
      Limit := 2;
    end;
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT ('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >= "'+Atual+'"');
      Increm := 2;
    end;
    if VAR_GOTOVAR > 0 then QvLocY.SQL.Add('AND '+VAR_GOTOVAR1);
    QvLocY.SQL.Add('ORDER BY Record '+Sentido);
    if Limit > 0 then QvLocY.SQL.Add('LIMIT '+IntToStr(Limit));
    QvLocY.Open;
    case Para of
      vpFirst: QvLocY.First;
      vpPrior: QvLocY.Next; // DESC
      vpNext : QvLocY.Next;
      vpLast : QvLocY.Last;
    end;
    if QvLocY.RecordCount > 0 then
      if QvLocY.FieldByName('Record').AsString <> '' then
        LocalizaTxt(Atual, QvLocY.FieldByName('Record').AsString);
    QvLocY.Close;
  end;
  if VAR_GOTOABSTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOABSTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOm.VaiData(Para: TVaiPara; Atual: TDate; Sort: String): String;
var
  Seq: String;
  Qtd, Increm: Integer;
  Quando: Boolean;
begin
  Quando := False;
  Qtd := 0;
  increm := 0;
  if Sort = 'A' then
  begin
    if Para = vpFirst then VAR_GOTOABSTABLE.First;
    if Para = vpPrior then VAR_GOTOABSTABLE.Prior;
    if Para = vpNext then VAR_GOTOABSTABLE.Next;
    if Para = vpLast then VAR_GOTOABSTABLE.Last;
    Seq := '[A..Z]: ';
  end else begin
    Seq := '[J..D]: ';
    QvLocY.Close;
    DefineBaseDados(QvLocY);
    QvLocY.SQL.Clear;
    if Para = vpFirst then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if not VAR_GOTONEG then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' > 0');
        Quando := True;
      end;
      Increm := -2;
    end;
    if Para = vpPrior then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' <:P0');
      if not VAR_GOTONEG then QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      QvLocY.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, Atual);
      Increm := -1;
      Quando := True;
    end;
    if Para = vpNext then
    begin
      QvLocY.SQL.Add('SELECT MIN('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' >:P0');
      if not VAR_GOTONEG then QvLocY.SQL.Add('AND '+VAR_GOTOCAMPO+' > 0');
      QvLocY.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, Atual);
      Increm := 1;
      Quando := True;
    end;
    if Para = vpLast then
    begin
      QvLocY.SQL.Add('SELECT MAX('+VAR_GOTOCAMPO+') Record');
      QvLocY.SQL.Add('FROM '+lowercase(var_gototabela));
      if not VAR_GOTONEG then
      begin
        QvLocY.SQL.Add('WHERE '+VAR_GOTOCAMPO+' > 0');
        Quando := True;
      end;
      Increm := 2;
    end;
    if VAR_GOTOVAR > 0 then
    begin
      if Quando then
        QvLocY.SQL.Add('AND '+VAR_GOTOVAR1)
      else
        QvLocY.SQL.Add('WHERE '+VAR_GOTOVAR1);
    end;
    QvLocY.Open;
    if QvLocY.RecordCount > 0 then
       LocalizaData(Atual, QvLocY.FieldByName('Record').AsDateTime);
    QvLocY.Close;
  end;
  if VAR_GOTOABSTABLE.State = dsBrowse then Qtd := Registros(VAR_GOTOABSTABLE);
  Result := Seq+TxtLaRegistro(Qtd, increm);
end;

function TUnGOTOm.VaiInt(Para: TVaiPara; Qry: TM y S Q LQuery; Tabela, Campo, Extra:
String; Negativo: Boolean): Boolean;
var
  //Seq: String;
  //Increm, Qtd,
  Atual: Integer;
  Quando: Boolean;
begin
  Result := False;
  Quando := False;
  //Qtd := 0;
  //increm := 0;
  QvLocY.Close;
  QvLocY.Database := Qry.Database;
  QvLocY.SQL.Clear;
  Atual := Qry.FieldByName(Campo).AsInteger;
  if Para = vpFirst then
  begin
    QvLocY.SQL.Add('SELECT MIN('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    if not Negativo then
    begin
      QvLocY.SQL.Add('WHERE '+Campo+' > 0');
      Quando := True;
    end;
    //Increm := -2;
  end;
  if Para = vpPrior then
  begin
    QvLocY.SQL.Add('SELECT MAX('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    QvLocY.SQL.Add('WHERE '+Campo+' <:P0');
    if not Negativo then QvLocY.SQL.Add('AND '+Campo+' > 0');
    QvLocY.Params[0].AsInteger := Atual;
    //Increm := -1;
    Quando := True;
  end else
  if Para = vpNext then
  begin
    QvLocY.SQL.Add('SELECT MIN('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    QvLocY.SQL.Add('WHERE '+Campo+' >:P0');
    if not Negativo then QvLocY.SQL.Add('AND '+Campo+' > 0');
    QvLocY.Params[0].AsInteger := Atual;
    //Increm := 1;
    Quando := True;
  end else
  if Para = vpLast then
  begin
    QvLocY.SQL.Add('SELECT MAX('+Campo+') Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    if not Negativo then
    begin
      QvLocY.SQL.Add('WHERE '+Campo+' > 0');
      Quando := True;
    end;
    //Increm := 2;
  end else
  if Para = vpThis then
  begin
    QvLocY.SQL.Add('SELECT '+Campo+' Record');
    QvLocY.SQL.Add('FROM '+lowercase(Tabela));
    QvLocY.SQL.Add('WHERE '+Campo+'='+IntToStr(Geral.Periodo2000(Date)));
    Quando := True;
    if not Negativo then
      QvLocY.SQL.Add('AND '+Campo+' > 0');
    //Increm := 0;
  end;
  if Trim(Extra) <> '' then
  begin
    if Quando then
      QvLocY.SQL.Add('AND ' + Extra)
    else
      QvLocY.SQL.Add('WHERE ' + Extra);
  end;
  QvLocY.Open;
  QvLocY.First;
  if (QvLocY.RecordCount > 0) then
  begin
    if (QvLocY.FieldByName('Record').AsString <> '') then
      if (QvLocY.FieldByName('Record').AsInteger <> null) then
      begin
        Qry.Close;
        Qry.Params[00].AsInteger := QvLocY.FieldByName('Record').AsInteger;
        Qry.Open;
        //
        Result := True;
      end;
  end;
end;
}

function TUnGOTOm.CriaLocalizador(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOABSTABLE.DatabaseName <> 'MEMORY') then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizador". Tabela: '+VAR_GOTOABSTABLE.Name);
  if GetPropInfo(VAR_GOTOABSTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizador". Tabela: '+VAR_GOTOABSTABLE.Name)
  else begin
    VAR_GOTOABSTABLE.Close;
    try
      VAR_GOTOABSTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
      for i := 0 to VAR_SQL1.Count -1 do
        VAR_GOTOABSTABLE.SQL.Add(VAR_SQL1[i]);
      // Cuidado erro frequente AND na SQL sem WHERE !!!! 
      VAR_GOTOABSTABLE.Params[0].AsInteger := Codigo;
      VAR_GOTOABSTABLE.Open;
      if VAR_GOTOABSTABLE.RecordCount > 0 then Result := True;
    except;
      dmkPF.LeMeuTexto(VAR_GOTOABSTABLE.SQL.Text);
      Geral.MensagemBox(PChar('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizador". Tabela: '+VAR_GOTOABSTABLE.Name), 'Erro',
      MB_OK+MB_ICONERROR);
      raise;
    end;
  end;
end;

{
function TUnGOTOm.CriaLocalizadorCodUsu(Codigo: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOABSTABLE.DatabaseName <> 'MEMORY') then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOABSTABLE.Name);
  if GetPropInfo(VAR_GOTOABSTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOABSTABLE.Name)
  else begin
    VAR_GOTOABSTABLE.Close;
    try
      VAR_GOTOABSTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
      if VAR_SQL2 <> nil then
      for i := 0 to VAR_SQL2.Count -1 do
        VAR_GOTOABSTABLE.SQL.Add(VAR_SQL2[i])
      else
        Geral.MensagemBox('"VAR_SQL2" n�o configurado!  AVISE A DERMATEK!',
        'Aviso', MB_OK+MB_ICONWARNING);
      VAR_GOTOABSTABLE.Params[0].AsInteger := Codigo;
      VAR_GOTOABSTABLE.Open;
      if Registros(VAR_GOTOABSTABLE) > 0 then Result := True;
    except;
      Geral.MensagemBox(PChar('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOABSTABLE.Name), 'Erro',
      MB_OK+MB_ICONERROR);
      raise;
    end;
  end;
end;

function TUnGOTOm.CriaLocalizadorTxtUsu(Codigo: String): Boolean;
var
  i: Integer;
begin
  Result := False;
  if (VAR_GOTOABSTABLE.Database <> 'MEMORY') then
    EAbort.Create('Tabela com base de dados diferente da informada!'+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOABSTABLE.Name);
  if GetPropInfo(VAR_GOTOABSTABLE, 'SQL') = nil then
    EAbort.Create('Erro no procedimento de reabertura de tabela '+
    '"CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOABSTABLE.Name)
  else begin
    VAR_GOTOABSTABLE.Close;
    try
      VAR_GOTOABSTABLE.SQL.Clear;
      for i := 0 to VAR_SQLx.Count -1 do
        VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
      //for i := 0 to VAR_SQLy.Count -1 do
        //VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
      if VAR_SQL2 <> nil then
      for i := 0 to VAR_SQL2.Count -1 do
        VAR_GOTOABSTABLE.SQL.Add(VAR_SQL2[i])
      else
        Geral.MensagemBox('"VAR_SQL2" n�o configurado!  AVISE A DERMATEK!',
        'Aviso', MB_OK+MB_ICONWARNING);
      VAR_GOTOABSTABLE.Params[0].AsString := Codigo;
      VAR_GOTOABSTABLE.Open;
      if Registros(VAR_GOTOABSTABLE) > 0 then Result := True;
    except;
      Geral.MensagemBox(PChar('Erro no procedimento de reabertura de '+
      'tabela "CriaLocalizadorCodUsu". Tabela: '+VAR_GOTOABSTABLE.Name), 'Erro',
      MB_OK+MB_ICONERROR);
      raise;
    end;
  end;
end;

procedure TUnGOTOm.CriaLocalNumTxt(Codigo: String);
var
  i: Integer;
begin
  VAR_GOTOABSTABLE.Close;
  VAR_GOTOABSTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do
    VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do
    //VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQL1.Count -1 do
    VAR_GOTOABSTABLE.SQL.Add(VAR_SQL1[i]);
  VAR_GOTOABSTABLE.Params[0].AsString := Codigo;
  VAR_GOTOABSTABLE.Open;
end;

procedure TUnGOTOm.CriaLocalTxt(Texto: String);
var
  i: Integer;
begin
  VAR_GOTOABSTABLE.Close;
  VAR_GOTOABSTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do
    VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do
    //VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQL1.Count -1 do
    VAR_GOTOABSTABLE.SQL.Add(VAR_SQL1[i]);
  VAR_GOTOABSTABLE.Params[0].AsString := Texto;
  VAR_GOTOABSTABLE.Open;
end;

procedure TUnGOTOm.CriaLocalizadorData(Data: TDate);
var
  i: Integer;
begin
  VAR_GOTOABSTABLE.Close;
  VAR_GOTOABSTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do
    VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do
    //VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQL1.Count -1 do
    VAR_GOTOABSTABLE.SQL.Add(VAR_SQL1[i]);
  VAR_GOTOABSTABLE.Params[0].AsDate := Data;
  VAR_GOTOABSTABLE.Open;
end;
}

function TUnGOTOm.LC(Atual, Codigo: Integer): Boolean;
begin
  Result := LocalizaCodigo(Atual, Codigo);
end;

procedure TUnGOTOm.LeMeuSQLForce_m(Query: TABSQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow, Force: Boolean);
var
  i: Integer;
  WinPath : Array[0..144] of Char;
  Text1, Text2: TextFile;
  Name, Diretorio: String;
  Other: Boolean;
  Texto1, Texto2, Texto3, Texto4: String;
begin
  Texto1 := '/*'+ Query.DatabaseName+'.'+Query.Name+'*/';
  Texto2 := '/*********** SQL ***********/';
  Texto3 := '/********* FIM SQL *********/';
  Texto4 := #13#10;
  if WinArq then
  begin
    GetWindowsDirectory(WinPath,144);
    Diretorio := StrPas(WinPath)+'\Dermatek';
    if not DirectoryExists(Diretorio) then ForceDirectories(Diretorio);
    Name := Diretorio + '\SQL.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(Text1, Name);
    ReWrite(Text1);
    WriteLn(Text1, Texto1);
  end;
  if Trim(Arquivo) <> '' then
  begin
    Other := True;
    if FileExists(Arquivo) then DeleteFile(Arquivo);
    AssignFile(Text2, Arquivo);
    ReWrite(Text2);
    WriteLn(Text2, Texto1);
  end else Other := False;
  if Memo <> nil then
  begin
    if Query.DatabaseName <> '' then
    begin
      Memo.Lines.Add(Texto1);
      Memo.Lines.Add(Texto2);
    end;
  end;
  for i := 0 to Query.SQL.Count -1 do
  begin
    if WinArq then WriteLn(Text1, Query.SQL[i]);
    if Other  then WriteLn(Text2, Query.SQL[i]);
    if Memo <> nil then Memo.Lines.Add(Query.SQL[i]);
  end;
  //
  if Query.Params.Count > 0 then
  begin
    Texto4 := Texto4 + '/***** Parametros *******/' + #13#10;
    for i := 0 to Query.Params.Count -1 do
    begin
      Texto4 := Texto4 + '/***** ' + Query.Params[i].Name + ' = ' +
        Geral.VariantToString(Query.Params[i].Value) + ' ******/'  + #13#10;
    end;
    Texto4 := Texto4 + '/***** FIM Parametros *******/' + #13#10;
  end else begin
    Texto4 := Texto4 + '/*****Query sem parametros*******/' + #13#10;
  end;
  if WinArq then WriteLn(Text1, Texto4);
  if Other  then WriteLn(Text2, Texto4);
  if Memo <> nil then Memo.Lines.Add(Texto4);
  //
  if WinArq then CloseFile(Text1);
  if Other  then CloseFile(Text2);
  if Query.DatabaseName <> '' then
    if Memo <> nil then Memo.Lines.Add(Texto3);
  if AskShow then
  begin
    if (FM_MASTER = 'V') or (Uppercase(VAR_LOGIN) = 'A') or Force then
    begin
      Geral.MensagemBox(Texto1 + #13#10 + Texto2 + #13#10 + Query.SQL.Text +
      Texto3 + #13#10 + Texto4, 'Aviso', MB_OK+MB_ICONWARNING);
      {
      MyObjects.FormCria(TFmAviso, FmAviso);
      FmAviso.Memo.Lines.Add(Texto1);
      FmAviso.Memo.Lines.Add(Texto2);
      FmAviso.Memo.Lines.Add();

      FmAviso.Memo.Lines.Add(Texto3);
      FmAviso.Memo.Lines.Add(Texto4);
      FmAviso.BtOK.Enabled := True;
      FmAviso.CkNaoMais.Visible := False;
      FmAviso.LaNumero.Caption := '004';
      FmAviso.ShowModal;
      FmAviso.Destroy;
      }
    end;
  end;
end;

procedure TUnGOTOm.LeMeuSQL_Fixo_m(Query: TABSQuery; Arquivo: String;
  Memo: TMemo; WinArq, AskShow: Boolean);
begin
  LeMeuSQLForce_m(Query, Arquivo, Memo, WinArq, AskShow, False);
end;

procedure TUnGOTOm.LeMeuSQL_m(Query: TABSQuery);
begin
  LeMeuSQLForce_m(Query, '', nil, False, True, True);
end;

function TUnGOTOm.LocalizaCodigo(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizador(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizador".');
  end;
  try
    if (VAR_GOTOABSTABLE.RecordCount = 0) (*and ((Atual > 0) or (VAR_GOTONEG = True))*) then
    begin
      CriaLocalizador(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodigo".');
    raise;
  end;
end;

{
function TUnGOTOm.LocalizaCodUsu(Atual, Codigo: Integer): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorCodUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorCodUsu".');
  end;
  try
    if (Registros(VAR_GOTOABSTABLE) = 0) and ((Atual > 0) or (VAR_GOTONEG = True)) then
    begin
      CriaLocalizadorCodUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaCodUsu".');
    raise;
  end;
end;

function TUnGOTOm.LocalizaTxtUsu(Atual, Codigo: String): Boolean;
begin
  Result := False;
  //
  try
    Result := CriaLocalizadorTxtUsu(Codigo);
  finally
     if not Result then EAbort.Create('Erro na fun��o "CriaLocalizadorTxtUsu".');
  end;
  try
    if (Registros(VAR_GOTOABSTABLE) = 0) and ((Atual <> '') or (VAR_GOTONEG = True)) then
    begin
      CriaLocalizadorTxtUsu(Atual);
    end;
  except
    EAbort.Create('Erro na fun��o "LocalizaTxtUsu".');
    raise;
  end;
end;

function TUnGOTOm.LocalizaPriorNextIntQr(Tabela: TM y S Q LQuery;
 Campo: TIntegerField; Atual: Integer): Integer;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;

function TUnGOTOm.LocalizaPriorNextTxtQr(Tabela: TM y S Q LQuery;
 Campo: TWideStringField; Atual: String): String;
begin
  Result := Atual;
  Tabela.Prior;
  if Campo.Value <> Atual then Result := Campo.Value
  else begin
    Tabela.Next;
    if Campo.Value <> Atual then
    begin
      Result := Campo.Value;
      Tabela.Prior;
    end;
  end;
end;

function TUnGOTOm.LocalizaPriorNextInt_SQL(Tabela, Campo: String; Atual: Integer;
 DataBase: TM y S Q LDataBase): Integer;
begin
  //Result := 0;
  Dmod.QrPriorNext.Close;
  Dmod.QrPriorNext.SQL.Clear;
  Dmod.QrPriorNext.SQL.Add('SELECT MAX('+Campo+') Codigo');
  Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
  Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' < '+IntToStr(Atual));
  Dmod.QrPriorNext.Open;
  if Dmod.QrPriorNext.RecordCount > 0 then Result :=
  Dmod.QrPriorNext.FieldByName(Campo).AsInteger else
  begin
    Dmod.QrPriorNext.Close;
    Dmod.QrPriorNext.SQL.Clear;
    Dmod.QrPriorNext.SQL.Add('SELECT MIN('+Campo+') Codigo');
    Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
    Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' > "'+IntToStr(Atual));
    Dmod.QrPriorNext.Open;
    if Dmod.QrPriorNext.RecordCount > 0 then Result :=
    Dmod.QrPriorNext.FieldByName(Campo).AsInteger else Result := 0;
  end;
end;

function TUnGOTOm.MyLocate(DataBase: TM y S Q LDataBase; Tabela: TM y S Q LQuery;
Campo: String; Valor: Variant; Grid: TDBGrid): Boolean;
begin
  Result := False;
  //if Tabela.Locate
end;

function TUnGOTOm.LocalizaPriorNextNumTxt(Tabela, Campo, Atual: String;
 DataBase: TM y S Q LDataBase): String;
begin
  Result := '';
  Dmod.QrPriorNext.Close;
  Dmod.QrPriorNext.SQL.Clear;
  Dmod.QrPriorNext.SQL.Add('SELECT MAX('+Campo+') Codigo');
  Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
  Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' < "'+Atual+'"');
  Dmod.QrPriorNext.Open;
  if Dmod.QrPriorNext.RecordCount > 0 then Result :=
  Dmod.QrPriorNext.FieldByName('Codigo').AsString else
  begin
    Dmod.QrPriorNext.Close;
    Dmod.QrPriorNext.SQL.Clear;
    Dmod.QrPriorNext.SQL.Add('SELECT MIN('+Campo+') Codigo');
    Dmod.QrPriorNext.SQL.Add('FROM '+lowercase(tabela));
    Dmod.QrPriorNext.SQL.Add('WHERE '+Campo+' > "'+Atual+'"');
    Dmod.QrPriorNext.Open;
    if Dmod.QrPriorNext.RecordCount > 0 then Result :=
    Dmod.QrPriorNext.FieldByName('Codigo').AsString else Result := '';
  end;
end;

function TUnGOTOm.LocalizaNumTxt(Atual, Codigo: String): Boolean;
begin
  Result := False;
  CriaLocalNumTxt(Codigo);
  //
  if (Registros(VAR_GOTOABSTABLE) = 0) and ((Atual <> '0000000000000') or (VAR_GOTONEG = True)) then
  begin
    CriaLocalNumTxt(Atual);
    Result := True;
  end;
end;

function TUnGOTOm.LocalizaTxt(Atual, Texto: String): Boolean;
begin
  Result := False;
  CriaLocalTxt(Texto);
  //
  if (Registros(VAR_GOTOABSTABLE) = 0) and ((Atual <> '') or (VAR_GOTONEG = True)) then
  begin
    CriaLocalTxt(Atual);
    Result := True;
  end;
end;

function TUnGOTOm.LocalizaData(Atual, Data: TDate): Boolean;
begin
  Result := False;
  CriaLocalizadorData(Data);
  if (VAR_GOTOABSTABLE.RecordCount = 0) and ((Atual > 0) or (VAR_GOTONEG = True)) then
  begin
    CriaLocalizadorData(Data);
    Result := True;
  end;
end;
}

procedure TUnGOTOm.BotoesSb(LaTipo: String);
var
  Acao: Boolean;
  i: Integer;
begin
  // Evitar erro
  if Screen.ActiveForm = nil then Exit;
  if LaTipo = CO_TRAVADO then Acao := True else Acao := False;
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Screen.ActiveForm.Components[i].Name = 'SbNumero')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNome')
    or  (Screen.ActiveForm.Components[i].Name = 'SbQuery'))
    then begin
      if (Screen.ActiveForm.Components[i] is TBitBtn) then
        TBitBtn(Screen.ActiveForm.Components[i]).Enabled := Acao;
      if (Screen.ActiveForm.Components[i] is TSpeedButton) then
        TSpeedButton(Screen.ActiveForm.Components[i]).Enabled := Acao;
    end;
  end;
end;

procedure TUnGOTOm.BotoesSb(SQLType: TSQLType);
var
  Acao: Boolean;
  i: Integer;
begin
  if SQLType = stLok then Acao := True else Acao := False;
  for i := 0 to Screen.ActiveForm.ComponentCount -1 do
  begin
    if  ((Screen.ActiveForm.Components[i].Name = 'SbNumero')
    or  (Screen.ActiveForm.Components[i].Name = 'SbNome')
    or  (Screen.ActiveForm.Components[i].Name = 'SbQuery'))
    then begin
      if (Screen.ActiveForm.Components[i] is TBitBtn) then
        TBitBtn(Screen.ActiveForm.Components[i]).Enabled := Acao;
      if (Screen.ActiveForm.Components[i] is TSpeedButton) then
        TSpeedButton(Screen.ActiveForm.Components[i]).Enabled := Acao;
    end;
  end;
end;

{
function TUnGOTOm.BtEnabled(Codigo: Integer; PodeNeg: Boolean): Boolean;
begin
  if (Codigo = 0) or ((Codigo < 0) and (PodeNeg = False)) then
    Result := False
  else
    Result := True;
end;

function TUnGOTOm.BtEnabledNumTxt(Codigo: String; PodeNeg: Boolean): Boolean;
begin
  if (Length(Trim(Codigo)) = 0) and (PodeNeg = False) then
    Result := False
  else
    Result := True;
end;
}

function TUnGOTOm.Codigo(Atual: Integer; Padrao: String): String;
var
  Num : String;
  Cod : Integer;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA, 'Digite o c�digo.', '' );
  try
    Cod := Geral.IMV(Num);
    if (Cod > 0) or (VAR_GOTONEG IN ([gotoNeg, gotoNiZ, gotoAll, gotoNiP])) then
    if not GOTOm.LocalizaCodigo(Atual, Cod) then
      Geral.MB_Erro('O registro n� ' + Num + ' n�o foi localizado.')
    else
    begin
      if VAR_GOTOABSTABLE.RecordCount = -1 then
      begin
        VAR_GOTOABSTABLE.DisableControls;
        VAR_GOTOABSTABLE.Last;
        VAR_GOTOABSTABLE.EnableControls;
        VAR_GOTOABSTABLE.First;
      end;
      Result := '[1...]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MB_Erro('"' + Num + SMLA_NUMEROINVALIDO);
  end;
end;

{
function TUnGOTOm.CodUsu(Atual: Integer; Padrao: String): String;
var
  Num : String;
  Cod : Integer;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite a refer�ncia.', '' );
  try
    Cod := Geral.IMV(Num);
    if (Cod > 0) or (VAR_GOTONEG) then
    if not GOTOm.LocalizaCodUsu(Atual, Cod) then
      Geral.MensagemBox(PChar('A refer�ncia n� ' + Num +
      ' n�o foi localizada.'), 'Erro', MB_OK+MB_ICONERROR)
    else
    begin
      if VAR_GOTOABSTABLE.RecordCount = -1 then
      begin
        VAR_GOTOABSTABLE.DisableControls;
        VAR_GOTOABSTABLE.Last;
        VAR_GOTOABSTABLE.EnableControls;
        VAR_GOTOABSTABLE.First;
      end;
      Result := '[R...]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MensagemBox(PChar('"'+Num+
      SMLA_NUMEROINVALIDO), 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TUnGOTOm.TxtUsu(Atual, Padrao: String): String;
var
  Num : String;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite a refer�ncia.', '' );
  try
    if (Num <> '') or (VAR_GOTONEG) then
    if not GOTOm.LocalizaTxtUsu(Atual, Num) then
      Geral.MensagemBox(PChar('A refer�ncia n� ' + Num +
      ' n�o foi localizada.'), 'Erro', MB_OK+MB_ICONERROR)
    else
    begin
      if VAR_GOTOABSTABLE.RecordCount = -1 then
      begin
        VAR_GOTOABSTABLE.DisableControls;
        VAR_GOTOABSTABLE.Last;
        VAR_GOTOABSTABLE.EnableControls;
        VAR_GOTOABSTABLE.First;
      end;
      Result := '[R...]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MensagemBox(PChar('"'+Num+
      SMLA_NUMEROINVALIDO), 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TUnGOTOm.NumTxt(Atual, Padrao: String): String;
var
  Num : String;
begin
  Result := Padrao;
  Num := InputBox(VAR_GOTOTABELA,'Digite o c�digo.', '' );
  try
    if GOTOm.LocalizaNumTxt(Atual, Num) then
      Geral.MensagemBox(PChar('O registro n� ' + Num +
      ' n�o foi localizado.'), 'Erro', MB_OK+MB_ICONERROR)
    else
    begin
      if VAR_GOTOABSTABLE.RecordCount = -1 then
      begin
        VAR_GOTOABSTABLE.DisableControls;
        VAR_GOTOABSTABLE.Last;
        VAR_GOTOABSTABLE.EnableControls;
        VAR_GOTOABSTABLE.First;
      end;
      Result := '[1...]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
    end
  except
    on EConvertError do
      if Num <> '' then Geral.MensagemBox(PChar('"'+Num+
      SMLA_NUMEROINVALIDO), 'Erro', MB_OK+MB_ICONERROR)
  end;
end;
}

function TUnGOTOm.Nome(Padrao: String): String;
var
  Nome : String;
  i: Integer;
  Continua: Boolean;
begin
  Result := Padrao;
  MyObjects.FormCria(TFmNomeX, FmNomeX);
  FmNomeX.Caption := 'Procura Pelo Nome';
  FmNomeX.ShowModal;
  Nome := FmNomeX.FTexto;
  Continua := FmNomeX.FContinua;
  FmNomeX.Destroy;
  if Continua = True then
  begin
    VAR_GOTOABSTABLE.Close;
    VAR_GOTOABSTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLa[i]);
    VAR_GOTOABSTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOABSTABLE.Params[0].AsString := Nome;
    if VAR_GOTOABSTABLE.Params.Count > 1 then
    VAR_GOTOABSTABLE.Params[1].AsString := Nome;
    VAR_GOTOABSTABLE.Open;
    if VAR_GOTOABSTABLE.RecordCount = -1 then
    begin
      VAR_GOTOABSTABLE.DisableControls;
      VAR_GOTOABSTABLE.Last;
      VAR_GOTOABSTABLE.EnableControls;
      VAR_GOTOABSTABLE.First;
    end;
    Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
  end;
end;

{
function TUnGOTOm.NomeDireto(Padrao: String): String;
var
  Nome : String;
  i: Integer;
begin
  Nome := '%'+Padrao+'%';
  VAR_GOTOABSTABLE.Close;
  VAR_GOTOABSTABLE.SQL.Clear;
  for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
  //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
  for i := 0 to VAR_SQLa.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLa[i]);
  VAR_GOTOABSTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
  VAR_GOTOABSTABLE.Params[0].AsString := Nome;
  if VAR_GOTOABSTABLE.Params.Count > 1 then
  VAR_GOTOABSTABLE.Params[1].AsString := Nome;
  VAR_GOTOABSTABLE.Open;
  if VAR_GOTOABSTABLE.RecordCount = -1 then
  begin
    VAR_GOTOABSTABLE.DisableControls;
    VAR_GOTOABSTABLE.Last;
    VAR_GOTOABSTABLE.EnableControls;
    VAR_GOTOABSTABLE.First;
  end;
  Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
end;

function TUnGOTOm.CodigoTxt(Padrao: String): String;
var
  Codigo : String;
  i: Integer;
begin
  Result := Padrao;
  Codigo := InputBox(VAR_GOTOTABELA, 'Digite parte do c�digo', '%%' );
  if Codigo <> CO_VAZIO then
  begin
    VAR_GOTOABSTABLE.Close;
    VAR_GOTOABSTABLE.SQL.Clear;
    for i := 0 to VAR_SQLx.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLx[i]);
    //for i := 0 to VAR_SQLy.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQLy[i]);
    for i := 0 to VAR_SQL1.Count -1 do VAR_GOTOABSTABLE.SQL.Add(VAR_SQL1[i]);
    VAR_GOTOABSTABLE.SQL.Add('ORDER BY '+VAR_GOTONOME);
    VAR_GOTOABSTABLE.Params[0].AsString := Codigo;
    VAR_GOTOABSTABLE.Open;
    if VAR_GOTOABSTABLE.RecordCount = -1 then
    begin
      VAR_GOTOABSTABLE.DisableControls;
      VAR_GOTOABSTABLE.Last;
      VAR_GOTOABSTABLE.EnableControls;
      VAR_GOTOABSTABLE.First;
    end;
    Result := '[A..Z]: '+TxtLaRegistro(VAR_GOTOABSTABLE.RecordCount, 0);
  end;
end;

function TUnGOTOm.Registros(Tabela: TM y S Q LQuery): Integer;
begin
  if Tabela.State = dsInactive then Result := 0
  else begin
    if Tabela.RecordCount = -1 then
    begin
      Tabela.Last;
      Tabela.First;
    end;
    Result := Tabela.RecordCount;
  end;
end;
}

procedure TUnGOTOm.LimpaVAR_SQL();
begin
  //VAR_SQLy.Clear;
  VAR_SQLx.Clear;
  VAR_SQL1.Clear;
  VAR_SQL2.Clear;
  VAR_SQLa.Clear;
end;

{
function TUnGOTOm.Destrava(DataFinal: TDateTime): Boolean;
var
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje : TDate;
  Agora: TTime;
  Licenca: int64;
  Chave: Double;
  Serial: Integer;
  CNPJ, Enti: String;
begin
  Result := False;
  try
    M L A G e r a l.CriaChavedeAcesso;
    M L A G e r a l.CriaChavedeAcesso;
    Chave  := Geral.DMV(UMyMod.LeChavedeAcesso);
    Serial := dmkPF.GetDiskSerialNumber('C');
    Licenca := Trunc((Serial * Chave) - Serial - Chave);
    //
    DModG.ReopenMaster2();
    DMod.QrAgora.Close;
    DMod.QrAgora.Open;
    Hoje  := EncodeDate(DMod.QrAgoraAno.Value, DMod.QrAgoraMes.Value, DMod.QrAgoraDia.Value);
    Agora := EncodeTime(DMod.QrAgoraHora.Value, DMod.QrAgoraMINUTO.Value, DMod.QrAgoraSEGUNDO.Value, 0);
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Dono FROM controle');
    Dmod.QrAux.Open;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha="", Dono=:P0, CNPJ=:P1')
    else
      Dmod.QrUpd.SQL.Add('INSERT INTO controle SET Dono=:P0, CNPJ=:P1');
    Dmod.QrUpd.Params[0].AsInteger := -11;
    Dmod.QrUpd.Params[1].AsString  := FmUnLock_MLA.FCNPJ;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    // Pode ainda n�o haver dono!!!
    DModG.ReopenMaster2();
    if Dmod.QrMaster2.RecordCount > 0 then
    begin
      Dmod.QrUpd.SQL.Add('UPDATE master SET ');
    end else
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO master SET ');
    end;
    Dmod.QrUpd.SQL.Add('Monitorar=1, Distorcao=0, Limite=0, DiasExtra=0, ');
    Dmod.QrUpd.SQL.Add('Hoje="' + FormatDateTime(VAR_FORMATDATE,  Hoje) + '", ');
    Dmod.QrUpd.SQL.Add('Hora="'+FormatDateTime(VAR_FORMATTIME2, Agora)+'", ');
    Dmod.QrUpd.SQL.Add('DataF="'+FormatDateTime(VAR_FORMATDATE,  DataFinal)+'",');
    Dmod.QrUpd.SQL.Add('Em="'+FmUnLock_MLA.FRazaoSocial+'", ');
    Dmod.QrUpd.SQL.Add('CNPJ="'+FmUnLock_MLA.FCNPJ+'" ');
    if VAR_SERVIDOR = 2 then
      Dmod.QrUpd.SQL.Add(', Licenca=' + FormatFloat('0', Licenca));
    if (Dmod.QrMaster2MasSenha.Value = '') and (Dmod.QrMaster2MasLogin.Value = '') then
    begin
      Dmod.QrUpd.SQL.Add(', SitSenha=1, MasLogin=''geren'', MasSenha=AES_ENCRYPT(''geren'', ''' +
      CO_USERSPNOW + ''')');
    end;
    Dmod.QrUpd.ExecSQL;
    //
    VAR_ESTEIP := Geral.ObtemIP(1);
    Dmod.QrTerminal.Close;
    Dmod.QrTerminal.Params[0].AsString := VAR_IP_LICENCA;
    Dmod.QrTerminal.Open;
    //
    if Dmod.QrTerminal.RecordCount = 0 then
    begin
      VAR_IP_LICENCA := '';
      MyObjects.FormShow(TFmTerminais, FmTerminais);
      Geral.WriteAppKey('IPClient', Application.Title, VAR_IP_LICENCA,
        ktString, HKEY_LOCAL_MACHINE);
    end;
    if VAR_IP_LICENCA <> '' then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0 WHERE IP=:P1 ');
      Dmod.QrUpd.Params[0].AsFloat  := Licenca;
      Dmod.QrUpd.Params[1].AsString := VAR_IP_LICENCA;
      Dmod.QrUpd.ExecSQL;
      //
    end else begin
      Geral.MensagemBox(PChar('IP sem Terminal definido!'+
      Chr(13)+Chr(10)+'IP: '+VAR_IP_LICENCA), 'Aviso', MB_OK+MB_ICONWARNING);
    end;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Tipo FROM entidades WHERE Codigo in (-1,-11)');
    Dmod.QrAux.Open;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, ')
    else Dmod.QrUpd.SQL.Add('INSERT INTO entidades SET Tipo=0, ');
    if Dmod.QrAux.FieldByName('Tipo').AsInteger = 1 then Enti := 'Nome' else Enti := 'RazaoSocial';
    if Dmod.QrAux.FieldByName('Tipo').AsInteger = 1 then CNPJ := 'CPF' else CNPJ := 'CNPJ';
    Dmod.QrUpd.SQL.Add(Enti+'=:P0, '+CNPJ+'=:P1');
    if Dmod.QrAux.RecordCount > 0 then
      Dmod.QrUpd.SQL.Add('WHERE Codigo in (-1,-11)')
    else Dmod.QrUpd.SQL.Add(', Filial=1, Codigo=-11');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_MLA.FRazaoSocial;
    Dmod.QrUpd.Params[1].AsString := FmUnLock_MLA.FCNPJ;
    Dmod.QrUpd.ExecSQL;
  except;
    raise;
  end;
end;

function TUnGOTOm.CriaFmUnLock_MLA: Boolean;
begin
  Result := False;
    DModG.ReopenMaster2();
  if Dmod.QrMaster2Limite.Value < 30 then
  begin
    Dmod.QrControle.Close;
    Dmod.QrControle.Open;
    MyObjects.FormCria(TFmUnLock_MLA, FmUnLock_MLA);
    FmUnLock_MLA.FContraSenha := Dmod.QrControleContraSenha.Value;
    if (Trim(Dmod.QrMaster2Em.Value) <> '') and (Dmod.QrMaster2CNPJ.Value <> '') then
    begin
      FmUnLock_MLA.EdRazao.Text := Dmod.QrMaster2Em.Value;
      FmUnLock_MLA.EdCNPJ.ValueVariant := Dmod.QrMaster2CNPJ.Value;
    end else begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NO_ENT,');
      Dmod.QrAux.SQL.Add('IF(Tipo=0,CNPJ,CPF) CNPJ_CPF');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.Params[0].AsInteger := Dmod.QrControleDono.Value;
      Dmod.QrAux.Open;
      FmUnLock_MLA.EdRazao.Text := Dmod.QrAux.FieldByName('NO_ENT').AsString;
      FmUnLock_MLA.EdCNPJ.ValueVariant := Dmod.QrAux.FieldByName('CNPJ_CPF').AsString;
    end;
    FmUnLock_MLA.ShowModal;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE controle SET ContraSenha=:P0');
    Dmod.QrUpd.Params[0].AsString := FmUnLock_MLA.FContraSenha;
    Dmod.QrUpd.ExecSQL;
    //
    DModG.ReopenMaster2();
    if FmUnlock_MLA.FCrypt then Destrava(FmUnLock_MLA.MCNova.Date)
    else begin
      Dmod.QrUpdU.SQL.Clear;
      if Dmod.QrMaster2.RecordCount = 0 then
        Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Limite=1')
      else Dmod.QrUpdU.SQL.Add('UPDATE master SET Limite=Limite+1');
      Dmod.QrUpdU.ExecSQL;
      Geral.MensagemBox(PChar('Senha inv�lida.'+Chr(13)+Chr(10)+
      'H� possibilidade de mais '+IntToStr(29-Dmod.QrMaster2Limite.Value)+
      ' tentativas de reativa��o do sistema. Ap�s, '+
      'o sistema travar� em definitivo.'), 'Erro', MB_OK+MB_ICONERROR);
    end;
    FmUnLock_MLA.Destroy;
    //
    Result := True;
  end else Geral.MensagemBox('Tentativas de reativa��o do sistema esgotadas.'
  ,' Sistema Travado', MB_OK+MB_ICONERROR);
end;

procedure TUnGOTOm.LiberaUso;
var
  Licenca: String;
  Dias, Chave: Double;
  Serial: Integer;
  Data: TDateTime;
begin
  if VAR_STDATABASES <> nil then
  begin
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' +
    Dmod.MyLocDataBase.DataBaseName + VAR_BDsEXTRAS;
    VAR_STDATABASES.Width := Length(VAR_STDATABASES.Text) * 7 + 8;
  end;
    DModG.ReopenMaster2();
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := Dmod.QrMaster2Distorcao.Value;
    Data := Dmod.QrMaster2DataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
  Licenca := Dmod.QrMaster2Licenca.Value;
  Chave   := Geral.DMV(UMyMod.LeChavedeAcesso);
  Serial  := dmkPF.GetDiskSerialNumber('C');
  if Licenca <> FloatToStr(Trunc(((Chave*Serial)-Chave-Serial))) then
  begin
    DModG.ReopenMaster2();
    Dmod.QrUpdU.SQL.Clear;
    if Dmod.QrMaster2.RecordCount=0 then
      Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Monitorar=2')
    else
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=2');
    Dmod.QrUpdU.ExecSQL;
    DModG.ReopenMaster2();
    VerificaLiberacao;
  end else if
    Dmod.QrMaster2Monitorar.Value = 2 then
      VerificaLiberacao;
end;

procedure TUnGOTOm.LiberaUso2;
var
  Achou: Boolean;
  MinhaLi: String;
  Dias, Chave: Double;
  Data: TDateTime;
  Licenca: String;
  Serial: Integer;
begin
  Achou := False;
  if VAR_STDATABASES <> nil then
  begin
    VAR_STDATABASES.Text := Dmod.MyDB.DatabaseName + ' :: ' +
    Dmod.MyLocDataBase.DataBaseName + VAR_BDsEXTRAS;
    VAR_STDATABASES.Width := Length(VAR_STDATABASES.Text) * 7 + 8;
  end;
    DModG.ReopenMaster2();
  Dias := Dmod.QrMaster2Distorcao.Value;
  Data := Dmod.QrMaster2DataF.Value;
  if VAR_STDATALICENCA <> nil then
  begin
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+' ['+FloatToStr(Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
  Licenca  := Dmod.QrMaster2Licenca.Value;
  Chave    := Geral.DMV(UMyMod.LeChavedeAcesso);
  Serial   := dmkPF.GetDiskSerialNumber('C');
  MinhaLi := FloatToStr(Trunc(((Chave*Serial)-Chave-Serial)));
  if (Licenca  <> FloatToStr(Trunc(((Chave*Serial)-Chave-Serial))))
  or (Data - Dias - Date < 1) then
  begin
    Dmod.QrTerminais.Close;
    Dmod.QrTerminais.Open;
    while not Dmod.QrTerminais.Eof do
    begin
      Licenca  := Dmod.QrTerminaisLicenca.Value;
      if Licenca = MinhaLi then
      begin
        Achou := True;
        Break;
        Exit;
      end;
      Dmod.QrTerminais.Next;
    end;
    if not Achou then
    begin
      VAR_ESTEIP := Geral.ObtemIP(1);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE terminais SET Licenca=:P0');
      Dmod.QrUpd.SQL.Add('WHERE IP=:P1');
      Dmod.QrUpd.Params[00].AsString := MinhaLi;
      Dmod.QrUpd.Params[01].AsString := VAR_ESTEIP;
      Dmod.QrUpd.ExecSQL;
      //
    DModG.ReopenMaster2();
      Dmod.QrUpdU.SQL.Clear;
      if Dmod.QrMaster2.RecordCount=0 then
        Dmod.QrUpdU.SQL.Add('INSERT INTO master SET Monitorar=2')
      else
        Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=2');
      Dmod.QrUpdU.ExecSQL;
    DModG.ReopenMaster2();
      VerificaLiberacao;
    end;
  end else if
    Dmod.QrMaster2Monitorar.Value = 2 then
      VerificaLiberacao;
  //VerificaLiberacao;
end;

procedure TUnGOTOm.VerificaLiberacao();
var
  Dias, Tempo : Double;
  Distorcao, Monitorar: Integer;
  //Ano, Mes, Dia, Hora, Min, Seg, Mili: Word;
  Hoje, Data: TDate;
  Agora: TTime;
  Exec: String;
begin
  Exec := Uppercase(ExtractFileName(Application.ExeName));
  //if Exec = Uppercase('Syndic.exe') then Exit;
    DModG.ReopenMaster2();
  if Dmod.QrMaster2Monitorar.Value = 0 then  // travado ap�s per�odo de avalia��o
  begin
    CriaFmUnLock_MLA;
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end else begin
    if Dmod.QrMaster2Monitorar.Value in [1, 3] then // sob avalia��o
    begin
      Monitorar := Dmod.QrMaster2Monitorar.Value;
      Distorcao := Dmod.QrMaster2Distorcao.Value;
      DMod.QrAgora.Close;
      DMod.QrAgora.Open;
      Hoje  := EncodeDate(DModG.QrAgoraAno.Value, DModG.QrAgoraMes.Value, DModG.QrAgoraDia.Value);
      Agora := EncodeTime(DModG.QrAgoraHora.Value, DModG.QrAgoraMINUTO.Value, DModG.QrAgoraSEGUNDO.Value, 0);

      Dias := Trunc(Hoje) - Trunc(Dmod.QrMaster2Hoje.Value);
      Tempo := Agora - Dmod.QrMaster2Hora.Value;
      if Dias < 0 then inc(Distorcao, 1)
      else if (Dias = 0) and (Tempo < 0) then inc(Distorcao, 1)
      else if Trunc(Agora) < Dmod.QrMaster2Hoje.Value then
         Agora := Dmod.QrMaster2Hoje.Value + 1 + Round(SQRT(Distorcao));
      if (Hoje + Distorcao ) > Dmod.QrMaster2DataF.Value then inc(Monitorar, -1);

      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE master SET Monitorar=:P0, Distorcao=:P1, ');
      Dmod.QrUpdU.SQL.Add('Hoje=:P2, Hora=:P3');
      Dmod.QrUpdU.Params[0].AsInteger := Monitorar;
      Dmod.QrUpdU.Params[1].AsInteger := Distorcao;
      Dmod.QrUpdU.Params[2].AsString := FormatDateTime(VAR_FORMATDATE,  Hoje);
      Dmod.QrUpdU.Params[3].AsString := FormatDateTime(VAR_FORMATTIME2, Agora);
      Dmod.QrUpdU.ExecSQL;
    end
    else
  //          Close;
  end;
  if Dmod.QrMaster2Monitorar.Value = 2  then  // aluguel vencido
  begin
    GOTOm.CriaFmUnLock_MLA;
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end;
  if Dmod.QrMaster2Monitorar.Value = 4  then  // Liberado eternamente
  begin
    //
  end;
  if VAR_STDATALICENCA <> nil then
  begin
    Dias := Dmod.QrMaster2Distorcao.Value;
    Data := Dmod.QrMaster2DataF.Value;
    VAR_STDATALICENCA.Text :=
    FormatDateTime(VAR_FORMATDATE3, Data)+' ['+
    FormatDateTime(VAR_FORMATDATE3, Data-Dias)+']';
    //if Data - Dias - Date < 10 then VAR_STDATALICENCA.Font.Color := clRed;
  end;
end;

function TUnGOTOm.VerificaTipoDaCarteira(Carteira: Integer): Integer;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Tipo FROM carteiras');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.Params[0].AsInteger := Carteira;
  Dmod.QrAux.Open;
  Result := Dmod.QrAux.FieldByName('Tipo').AsInteger;
  Dmod.QrAux.Close;
end;

procedure TUnGOTOm.VerificaTerminal;
var
  wVersionRequested: Word;
  wsaData: TWSAData;
  IP: String;
begin
  wVersionRequested := MAKEWORD(1,1);
  WSAStartup(wVersionRequested, wsaData);
  //if VAR_SERVIDOR = 1 then
  //begin
    VAR_ESTEIP := Geral.ObtemIP(1);
    if VAR_IP_LICENCA <> '' then IP := VAR_IP_LICENCA else IP := VAR_ESTEIP;
    Dmod.QrTerminal.Close;
    Dmod.QrTerminal.Params[0].AsString := IP;
    Dmod.QrTerminal.Open;
    if Dmod.QrTerminal.RecordCount = 0 then
    begin
      if VAR_STAVISOS <> nil then
      VAR_STAVISOS.Text := ' M�quina cliente sem terminal definido!';
    end else VAR_TERMINAL := Dmod.QrTerminalTerminal.Value;
  //end else VAR_TERMINAL := 0;
  if VAR_STTERMINAL <> nil then
    VAR_STTERMINAL.Text := IntToStr(VAR_TERMINAL);
  (*if (VAR_SERVIDOR = 1)
  and ((VAR_IP='127.0.0.1') or (VAR_IP='localhost') or (VAR_IP = ''))
  then begin
    Geral.MensagemBox('IP inv�lido para m�quina cliente!', 'Erro',
    MB_OK+MB_ICONERROR);
    ZZTerminate := True;
    Application.Terminate;
  end;*)
end;

procedure TUnGOTOm.DefineSenhaBoss;
var
  Texto, a: String;
begin
  ///////////////////////////////////////////
  // Excluir QrBoss e QrSenha de Fm???_MLA //
  ///////////////////////////////////////////
  Dmod.QrBSit.Close;
  Dmod.QrBSit.Open;
  case Dmod.QrBSitSitSenha.Value of
    1: Texto := CO_USERSP001;
    else Texto := 'xxx';
  end;
  if Dmod.QrBSitSitSenha.Value <> CO_USERSPSEQ then
  begin
    Dmod.QrBoss.Close;
    Dmod.QrBoss.Database := Dmod.MyDB;
    Dmod.QrBoss.Params[0].AsString := Texto;
    Dmod.QrBoss.Open;
    if Dmod.QrBSitSitSenha.Value = 0 then a := Dmod.QrBossMasZero.Value
    else a := Dmod.QrBossMasSenha.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE master SET SitSenha=:P0, ');
    Dmod.QrUpd.SQL.Add('MasSenha=AES_ENCRYPT(:P1, :P2) ');
    Dmod.QrUpd.Params[00].AsInteger := CO_USERSPSEQ;
    Dmod.QrUpd.Params[01].AsString  := a;
    Dmod.QrUpd.Params[02].AsString  := CO_USERSPNOW;
    Dmod.QrUpd.ExecSQL;
  end;
  Dmod.QrBSit.Close;
  Dmod.QrBoss.Close;
  Dmod.QrBoss.Database := Dmod.MyDB;
  Dmod.QrBoss.Params[0].AsString := CO_USERSPNOW;
  Dmod.QrBoss.Open;
  if Dmod.QrBossMasSenha.Value <> '' then
    VAR_BOSS := Uppercase(Dmod.QrBossMasSenha.Value)
  else
    VAR_BOSS := CO_MASTER;
  VAR_BOSSLOGIN := Dmod.QrBossMasLogin.Value;
  VAR_BOSSSENHA := Uppercase(Dmod.QrBossMasSenha.Value);
  if VAR_SENHA = CO_MASTER then
    VAR_USUARIO := -1;
  if Uppercase(VAR_SENHA) = Uppercase(Dmod.QrBossMasSenha.Value) then
    VAR_USUARIO := -2;
  if ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
    VAR_USUARIO := -3;
end;

function TUnGOTOm.DefineSenhaUsuario(Login: String): String;
var
  Texto, a: String;
begin
  Dmod.QrSSit.Close;
  Dmod.QrSSit.Params[0].AsString := Login;
  Dmod.QrSSit.Open;
  if Dmod.QrSSit.RecordCount = 0 then
  begin
    Result := '';
    // Somente abrir para eviat exception
    Dmod.QrSenhas.Close;
    Dmod.QrSenhas.Database := Dmod.MyDB;
    Dmod.QrSenhas.Params[00].AsString := CO_USERSPNOW;
    Dmod.QrSenhas.Params[01].AsString := Login;
    Dmod.QrSenhas.Open;
    Exit;
  end;
  case Dmod.QrSSitSitSenha.Value of
    1: Texto := CO_USERSP001;
    else Texto := 'xxx';
  end;
  if Dmod.QrSSitSitSenha.Value <> CO_USERSPSEQ then
  begin
    Dmod.QrSenhas.Close;
    Dmod.QrSenhas.Database := Dmod.MyDB;
    Dmod.QrSenhas.Params[00].AsString := Texto;
    Dmod.QrSenhas.Params[01].AsString := Login;
    Dmod.QrSenhas.Open;
    if Dmod.QrSSitSitSenha.Value = 0 then a := Dmod.QrSenhasSenhaOld.Value
    else a := Dmod.QrSenhasSenhaNew.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE senhas SET SitSenha=:P0, ');
    Dmod.QrUpd.SQL.Add('Senha=AES_ENCRYPT(:P1, :P2) ');
    Dmod.QrUpd.SQL.Add('WHERE Numero=:P3 ');
    Dmod.QrUpd.Params[00].AsInteger := CO_USERSPSEQ;
    Dmod.QrUpd.Params[01].AsString  := a;
    Dmod.QrUpd.Params[02].AsString  := CO_USERSPNOW;
    Dmod.QrUpd.Params[03].AsInteger := Dmod.QrSenhasNumero.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  Dmod.QrSSit.Close;
  Dmod.QrSenhas.Close;
  Dmod.QrSenhas.Database := Dmod.MyDB;
  Dmod.QrSenhas.Params[00].AsString := CO_USERSPNOW;
  Dmod.QrSenhas.Params[01].AsString := Login;
  Dmod.QrSenhas.Open;
  Result := Dmod.QrSenhasSenhaNew.Value;
end;

procedure TUnGOTOm.AnaliseSenha(EdLogin, EdSenha: TEdit; FmMLA, FmMain:
  TForm; Componente: TComponent; ForcaSenha: Boolean; Memo: TRichEdit;
  QrGeraSenha: TM y S Q LQuery);
var
  Mostra: Boolean;
  SenhaUsuario: String;
begin
  try
    VAR_LOGIN := Uppercase(EdLogin.Text);
    VAR_SENHA := Uppercase(EdSenha.Text);
    //VAR_FIRMA := Geral.IMV(EdEmpresa.Text);
    DefineSenhaBoss;
    if (Dmod.QrMasterSolicitaSenha.Value = 0)
    and (Dmod.QrMaster.RecordCount > 0)
    and (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
    begin
      VAR_LOGIN := '(?)';
      VAR_SENHA := '*';
    end;
    if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
    begin
      if (Trim(Dmod.QrBossMasSenha.Value) = '')
      or (Trim(Dmod.QrBossMasLogin.Value) = '') then
      begin
        if Geral.MensagemBox(PChar('N�o h� cadastro da senha administradora '+
        'do aplicativo. Deseja cadastr�-la agora?'), 'Pergunta',
        MB_YESNO+MB_ICONQUESTION)= ID_YES then begin
          VAR_BOSS        := '';
          VAR_SENHATXT    := '';
          VAR_NOMEEMPRESA := Dmod.QrBossEm.Value;
          VAR_CNPJEMPRESA := Dmod.QrBossCNPJ.Value;
          MyObjects.FormShow(TFmSenhaBoss, FmSenhaBoss);
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE master SET MasSenha=AES_ENCRYPT(:P0, :P1), ');
          Dmod.QrUpd.SQL.Add('MasLogin=:P2, Em=:P3, CNPJ=:P4, SitSenha=:P5');
          Dmod.QrUpd.SQL.Add('');
          Dmod.QrUpd.Params[00].AsString := VAR_SENHATXT;
          Dmod.QrUpd.Params[01].AsString := CO_USERSPNOW;
          Dmod.QrUpd.Params[02].AsString := VAR_BOSS;
          Dmod.QrUpd.Params[03].AsString := VAR_NOMEEMPRESA;
          Dmod.QrUpd.Params[04].AsString := VAR_CNPJEMPRESA;
          Dmod.QrUpd.Params[05].AsInteger := CO_USERSPSEQ;
          Dmod.QrUpd.ExecSQL;
          //
          ZZTerminate := True;
          Application.Terminate;
        end;
      end;
      if Componente <> nil then
        if Componente.Name = 'BtConfirma' then
        begin
          if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
          begin
            Geral.MensagemBox('Informe um Login e uma senha!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdLogin.SetFocus;
          end else if (Trim(VAR_LOGIN) = '') then
          begin
            Geral.MensagemBox('Informe um Login!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdLogin.SetFocus;
          end else begin
            Geral.MensagemBox('Informe uma senha!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdSenha.SetFocus;
          end;
        end;
      Exit;
    end else if Trim(VAR_LOGIN) = '' then
    begin
      EdLogin.SetFocus;
      Exit;
    end else if Trim(VAR_SENHA) = '' then
    begin
      EdSenha.SetFocus;
      Exit;
    end;
    VAR_BOSSLOGIN := Dmod.QrBossMasLogin.Value;
    VAR_BOSSSENHA := Uppercase(Dmod.QrBossMasSenha.Value);
    if VAR_SENHA = CO_MASTER then
      VAR_USUARIO := -1;
    if Uppercase(VAR_SENHA) = Uppercase(Dmod.QrBossMasSenha.Value) then
      VAR_USUARIO := -2;
    if ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
      VAR_USUARIO := -3;
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then FM_MASTER := 'V';
    //
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
    or (Dmod.QrMasterSolicitaSenha.Value = 0) then
    begin
      if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
      if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := ' '+VAR_LOGIN;
      if VAR_LOGOFFDE <> nil then VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
      FmMain.Enabled := True;
      Application.ProcessMessages;
      FmMLA.Hide;
      if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
      inc(VAR_FMPRINCIPALLIBERADO, 1);
      Exit;
    end;
    SenhaUsuario := DefineSenhaUsuario(EdLogin.Text);
    if Dmod.QrSenhas.RecordCount > 0 then
    begin
      VAR_USUARIO := Dmod.QrSenhasNumero.Value;
      VAR_FUNCILOGIN := Dmod.QrSenhasFuncionario.Value;
      if Uppercase(SenhaUsuario) = Uppercase(VAR_SENHA) then
      begin
        if VAR_LOGOFFDE <> nil then
        VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
        if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := Dmod.QrSenhasLogin.Value;
        if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
        if (Dmod.Privilegios(Dmod.QrSenhasPerfil.Value) = True) then
        begin
          //if CkRegerar.Checked then Forcar := True else Forcar := False;
          if QrGeraSenha <> nil then
            Mostra := DefineSenhaDia(VAR_USUARIO, ForcaSenha, QrGeraSenha)
          else Mostra := True;
          if Mostra then
          begin
            FmMLA.Hide;
            if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
            FmMain.Enabled := True;
            inc(VAR_FMPRINCIPALLIBERADO, 1);
          end else begin
            if (QrGeraSenha <> nil) and (Memo <> nil) then
              Memo.Lines.Add('Erro ao definir senha do dia.');
            Exit;
          end;
        end else begin
          Geral.MensagemBox('Erro. Perfil inexistente. [1]', 'Erro',
          MB_OK+MB_ICONERROR);
          ZZTerminate := True;
          Application.Terminate;
          Exit;
        end;
      end else begin
        M L A G e r a l.AvisoForm(TFmAvisoIndef, FmAvisoIndef, 'Senha inv�lida.');
        //MessageBox(0,'Senha inv�lida.',PChar(Application.Title),MB_OK);
        VAR_SKINCONTA := VAR_SKINCONTA + 1;
        if VAR_SKINCONTA <= 2 then EdSenha.SetFocus
        else begin
          ZZTerminate := True;
          Application.Terminate;
        end;  
      end;
    end else begin
      MessageBox(0,'Login n�o cadastrado.',PChar(Application.Title),MB_OK);
      EdLogin.SetFocus;
      Exit;
    end;
  finally
    Dmod.QrSenhas.Close;
    Dmod.QrBoss.Close;
  end;
end;

procedure TUnGOTOm.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit; FmMLA, FmMain:
  TForm; Componente: TComponent; ForcaSenha: Boolean; Memo: TRichEdit;
  QrGeraSenha, DmodFinQrCarts: TM y S Q LQuery);
var
  Mostra: Boolean;
  SenhaUsuario, Filiais: String;
  I: Integer;
  NomeQr: String;
begin
  Screen.Cursor := crHourGlass;
  try
    try
      // impedir usu�rio n�o habilitado a ver lan�amentos

      // TIREI DAQUI PARA N�O CHAMAR A PARTE FINANCEIRA NOS
      // APLICATIVOS QUE N�O USAM!!!!
      //DmodFinQrCarts.Close;
      if DmodFinQrCarts <> nil then
        DmodFinQrCarts.Close;
    except
    end;
    try
      // Compatibilidade
      // impedir usu�rio n�o habilitado a ver lan�amentos
      for I := 0 to Dmod.ComponentCount - 1 do
      begin
        if (Dmod.Components[i] is TM y S Q LQuery) then
        begin
          NomeQr := Uppercase(TM y S Q LQuery(Dmod.Components[i]).Name);
          if (NomeQr = 'QRCARTEIRAS') or (NomeQr = 'QRCARTS') then
            TM y S Q LQuery(Dmod.Components[i]).Close;
        end;
      end;
    except
    end;
    try
      // Compatibilidade
      // impedir usu�rio n�o habilitado a ver lan�amentos
      for I := 0 to FmPrincipal.ComponentCount - 1 do
      begin
        if (FmPrincipal.Components[i] is TM y S Q LQuery) then
        begin
          NomeQr := Uppercase(TM y S Q LQuery(FmPrincipal.Components[i]).Name);
          if (NomeQr = 'QRCARTEIRAS') or (NomeQr = 'QRCARTS') then
            TM y S Q LQuery(FmPrincipal.Components[i]).Close;
        end;
      end;
    except
    end;

    //

    VAR_LOGIN := Uppercase(EdLogin.Text);
    VAR_SENHA := Uppercase(EdSenha.Text);
    //VAR_FIRMA := Geral.IMV(EdEmpresa.Text);
    Filiais   := Trim(EdEmpresa.Text);
    VAR_LIB_FILIAIS := Geral.SoNumeroEVirgula_TT(Filiais);
    if (VAR_LIB_FILIAIS <> Filiais) or (Filiais = '') then
    begin
      Screen.Cursor := crDefault;
      if Filiais <> '' then
        Geral.MensagemBox('Empresa desconhecida!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      EdEmpresa.SetFocus;
      Exit;
    end;
    //
    // Parei aqui
    //VAR_EMPRESA := DmodG.QrFiliaisCodigo.Value;
    //if VAR_FIRMA <> 0 then VAR_FIRMA := - VAR_FIRMA;
    DmodG.QrFiliLog.Close;
    DmodG.QrFiliLog.SQL.Clear;
    DmodG.QrFiliLog.SQL.Add('SELECT Codigo, Filial, ');
    // compatibilidade com DBCheck.EscolheCodigo
    DmodG.QrFiliLog.SQL.Add('IF(Tipo=0,RazaoSocial,Nome) NomeEmp ');
    //
    DmodG.QrFiliLog.SQL.Add('FROM entidades');
    DmodG.QrFiliLog.SQL.Add('WHERE Codigo < -10');
    DmodG.QrFiliLog.SQL.Add('AND Filial in (' + VAR_LIB_FILIAIS + ')');
    DmodG.QrFiliLog.Open;
    //  no after open � criado o array
    DmodG.QrFiliLog.First;
    //
    Filiais := VAR_LIB_FILIAIS;
    VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrFiliLogCodigo.Value);
    VAR_LIB_FILIAIS  := FormatFloat('0', DmodG.QrFiliLogFilial.Value);
    if DmodG.QrFiliLog.RecordCount > 1 then
    begin
      DmodG.QrFiliLog.Next;
      while not DmodG.QrFiliLog.Eof do
      begin
        VAR_LIB_EMPRESAS := VAR_LIB_EMPRESAS + ',' + FormatFloat('0', DmodG.QrFiliLogCodigo.Value);
        VAR_LIB_FILIAIS  := VAR_LIB_FILIAIS  + ',' + FormatFloat('0', DmodG.QrFiliLogFilial.Value);
        DmodG.QrFiliLog.Next;
      end;
      VAR_LIB_EMPRESA_SEL := 0;
    end else
      VAR_LIB_EMPRESA_SEL := DModG.QrFiliLogCodigo.Value;
    if (VAR_LIB_FILIAIS <> Filiais) or (VAR_LIB_FILIAIS = '') then
    begin
      Screen.Cursor := crDefault;
      Geral.MensagemBox('Empresa n�o reconhecida!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      EdEmpresa.SetFocus;
      Exit;
    end;

    //
    DefineSenhaBoss;
    if (Dmod.QrMasterSolicitaSenha.Value = 0)
    and (Dmod.QrMaster.RecordCount > 0)
    and (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') then
    begin
      VAR_LOGIN := '(?)';
      VAR_SENHA := '*';
    end;
    if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') and (Filiais = '') then
    begin
      if (Trim(Dmod.QrBossMasSenha.Value) = '')
      or (Trim(Dmod.QrBossMasLogin.Value) = '') then
      begin
        if Geral.MensagemBox(PChar('N�o h� cadastro da senha administradora '+
        'do aplicativo. Deseja cadastr�-la agora?'), 'Pergunta',
        MB_YESNO+MB_ICONQUESTION)= ID_YES then begin
          VAR_BOSS        := '';
          VAR_SENHATXT    := '';
          VAR_NOMEEMPRESA := Dmod.QrBossEm.Value;
          VAR_CNPJEMPRESA := Dmod.QrBossCNPJ.Value;
          MyObjects.FormShow(TFmSenhaBoss, FmSenhaBoss);
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE master SET MasSenha=AES_ENCRYPT(:P0, :P1), ');
          Dmod.QrUpd.SQL.Add('MasLogin=:P2, Em=:P3, CNPJ=:P4, SitSenha=:P5');
          Dmod.QrUpd.SQL.Add('');
          Dmod.QrUpd.Params[00].AsString := VAR_SENHATXT;
          Dmod.QrUpd.Params[01].AsString := CO_USERSPNOW;
          Dmod.QrUpd.Params[02].AsString := VAR_BOSS;
          Dmod.QrUpd.Params[03].AsString := VAR_NOMEEMPRESA;
          Dmod.QrUpd.Params[04].AsString := VAR_CNPJEMPRESA;
          Dmod.QrUpd.Params[05].AsInteger := CO_USERSPSEQ;
          Dmod.QrUpd.ExecSQL;
          //
          ZZTerminate := True;
          Application.Terminate;
        end;
      end;
      if Componente <> nil then
        if Componente.Name = 'BtConfirma' then
        begin
          if (Trim(VAR_LOGIN) = '') and (Trim(VAR_SENHA)='') and (Filiais = '') then
          begin
            Geral.MensagemBox('Informe um Login, uma senha e uma empresa!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdLogin.SetFocus;
          end else if (Trim(VAR_LOGIN) = '') then
          begin
            Geral.MensagemBox('Informe um Login!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdLogin.SetFocus;
          end else if (Trim(VAR_SENHA) = '') then
          begin
            Geral.MensagemBox('Informe uma senha!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdSenha.SetFocus;
          end else begin
            Geral.MensagemBox('Informe uma empresa!', 'Erro',
              MB_OK+MB_ICONERROR);
            EdSenha.SetFocus;
          end;
        end;
      Exit;
    end else if Trim(VAR_LOGIN) = '' then
    begin
      EdLogin.SetFocus;
      Exit;
    end else if Trim(VAR_SENHA) = '' then
    begin
      EdSenha.SetFocus;
      Exit;
    end else if Filiais = '' then
    begin
      EdEmpresa.SetFocus;
      Exit;
    end;
    VAR_BOSSLOGIN := Dmod.QrBossMasLogin.Value;
    VAR_BOSSSENHA := Uppercase(Dmod.QrBossMasSenha.Value);
    if VAR_SENHA = CO_MASTER then
      VAR_USUARIO := -1;
    if VAR_SENHA = Uppercase(Dmod.QrBossMasSenha.Value) then
      VAR_USUARIO := -2;
    if ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
      VAR_USUARIO := -3;
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA))) then FM_MASTER := 'V';
    //
    if ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)))
    or ((Uppercase(VAR_LOGIN) = 'ADMIN')
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> ''))
    or ((Uppercase(VAR_LOGIN) = Uppercase(VAR_BOSSLOGIN))
    and (Uppercase(VAR_SENHA) = Uppercase(VAR_BOSSSENHA)))
    or (Dmod.QrMasterSolicitaSenha.Value = 0) then
    begin
      if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
      if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := ' '+VAR_LOGIN;
      if VAR_LOGOFFDE <> nil then VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
      FmMain.Enabled := True;
      Application.ProcessMessages;
      FmMLA.Hide;
      if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
      inc(VAR_FMPRINCIPALLIBERADO, 1);
      Exit;
    end;
    SenhaUsuario := DefineSenhaUsuario(EdLogin.Text);
    if Dmod.QrSenhas.RecordCount > 0 then
    begin
      VAR_USUARIO := Dmod.QrSenhasNumero.Value;
      VAR_FUNCILOGIN := Dmod.QrSenhasFuncionario.Value;
      if Uppercase(SenhaUsuario) = Uppercase(VAR_SENHA) then
      begin
        if Uppercase(Application.Title) = 'PLANNING' then
        begin
          Dmod.QrUpdU.SQL.Clear;
          Dmod.QrUpdU.SQL.Add('SELECT *');
          Dmod.QrUpdU.SQL.Add('FROM senhasits si');
          Dmod.QrUpdU.SQL.Add('LEFT JOIN entidades en ');
          Dmod.QrUpdU.SQL.Add('  ON si.Empresa=en.Codigo AND en.Codigo < -10');
          Dmod.QrUpdU.SQL.Add('WHERE en.Filial in (' + VAR_LIB_FILIAIS + ')');
          Dmod.QrUpdU.SQL.Add('AND si.Numero=:P0');
          Dmod.QrUpdU.Params[0].AsInteger := VAR_USUARIO;
          Dmod.QrUpdU.Open;
          //
          if Dmod.QrUpdU.RecordCount = 0 then
          begin
            Geral.MensagemBox('Empresa inv�lida!', 'Aviso',
              MB_OK+MB_ICONWARNING);
            EdEmpresa.SetFocus;
            Exit;
          end;
        end;
        if VAR_LOGOFFDE <> nil then
        VAR_LOGOFFDE.Caption := '&Logoff de '+VAR_LOGIN;
        if VAR_STEMPRESA <> nil then VAR_STEMPRESA.Text := VAR_LIB_FILIAIS;
        if VAR_STLOGIN <> nil then VAR_STLOGIN.Text := Dmod.QrSenhasLogin.Value;
        if (Dmod.Privilegios(Dmod.QrSenhasPerfil.Value) = True) then
        begin
          //if CkRegerar.Checked then Forcar := True else Forcar := False;
          if QrGeraSenha <> nil then
            Mostra := DefineSenhaDia(VAR_USUARIO, ForcaSenha, QrGeraSenha)
          else Mostra := True;
          if Mostra then
          begin
            FmMLA.Hide;
            if VAR_MASTER1 <> nil then if FM_MASTER = 'V' then VAR_MASTER1.Visible := True;
            FmMain.Enabled := True;
            Inc(VAR_FMPRINCIPALLIBERADO, 1);
          end else begin
            if (QrGeraSenha <> nil) and (Memo <> nil) then
              Memo.Lines.Add('Erro ao definir senha do dia.');
            Exit;
          end;
        end else begin
          Geral.MensagemBox('Erro. Perfil inexistente. [2]', 'Erro',
          MB_OK+MB_ICONERROR);
          ZZTerminate := True;
          Application.Terminate;
          Exit;
        end;
      end else begin
        M L A G e r a l.AvisoForm(TFmAvisoIndef, FmAvisoIndef, 'Senha inv�lida.');
        //MessageBox(0,'Senha inv�lida.',PChar(Application.Title),MB_OK);
        VAR_SKINCONTA := VAR_SKINCONTA + 1;
        if VAR_SKINCONTA <= 2 then EdSenha.SetFocus
        else begin
          ZZTerminate := True;
          Application.Terminate;
        end;
      end;
    end else begin
      MessageBox(0,'Login n�o cadastrado.',PChar(Application.Title),MB_OK);
      EdLogin.SetFocus;
      Exit;
    end;
  finally
    Screen.Cursor := crDefault;
    Dmod.QrSenhas.Close;
    Dmod.QrBoss.Close;
  end;
end;

function TUnGOTOm.DefineSenhaDia(Numero: Integer; Forcar: Boolean;
  Query: TM y S Q LQuery): Boolean;
var
  Nova: Double;
  Senha: String;
begin
  try
    Query.Close;
    Query.Params[0].AsInteger := Numero;
    Query.Open;
    Nova := 0;
    if Forcar or (int(Query.FieldByName('DataSenha').AsDateTime) < int(Date)) then
    begin
      Randomize;
      while (Nova = 0) or (Nova=Geral.IMV(Query.FieldByName('SenhaDia').AsString)) do
      begin
        Nova := Random(10000);
        Nova := int(Nova);
        if Nova > 9999 then Nova := 0;
      end;
      if Nova < 1000 then Nova := Nova * 10;
      Senha := FormatFloat('0000', Nova);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE senhas SET');
      Dmod.QrUpd.SQL.Add('DataSenha=:P0,');
      Dmod.QrUpd.SQL.Add('SenhaDia=:P1');
      Dmod.QrUpd.SQL.Add('WHERE Numero=:P2');
      Dmod.QrUpd.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, int(Date));
      Dmod.QrUpd.Params[1].AsString  := Senha;
      Dmod.QrUpd.Params[2].AsInteger := Numero;
      Dmod.QrUpd.ExecSQL;
      Geral.MensagemBox(PChar('Sua senha do dia �:'+Chr(10)+Chr(13)+
      Chr(10)+Chr(13)+Senha+Chr(10)+Chr(13)), 'Senha do Dia', MB_OK+MB_ICONEXCLAMATION);
      if VAR_USACARTAOPONTO > 0 then
      begin
(*
        $IfNDef SemPontoFunci
        MyObjects.FormCria(TFmPontoFunci, FmPontoFunci);
        with FmPontoFunci do
        begin
          EdFuncionario.Text := IntToStr(Query.FieldByName('Funcionario').AsInteger);
          CBFuncionario.KeyValue := Query.FieldByName('Funcionario').AsInteger;
          EdLogin.Text  := Query.FieldByName('Login').AsString;
          EdSenhaA.Text := Senha;
          EdSenhaP.Text := Query.FieldByName('Senha').AsString;
          MostraPonto;
          Destroy;
        end;
        $EndIf
*)
      end;
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnGOTOm.OM y S Q LEstaInstalado(StaticText: TStaticText; ProgessBar:
             TProgressBar): Boolean;//VerificaInstalacaoDoM y S Q L;
var
  (*CamExec: String;
  Servico: TServiceManager;*)
  StatusServico: Integer;
begin
  //Result := True;
  if not M L A G e r a l.ExecutavelEstaRodando('M y S Q Ld-nt.exe') then
  if not M L A G e r a l.ExecutavelEstaRodando('M y S Q Ld.exe') then
  if not M L A G e r a l.ExecutavelEstaRodando('M y S Q Ld-opt.exe') then
  begin
    //Result := False;
    //
    if not M L A G e r a l.ExecutavelEstaInstalado('M y S Q L Server') then
    begin
      if Geral.MensagemBox(PChar('O '+Application.Title+' n�o detectou a '+
      'instala��o do M y S Q L Server! Deseja instal�-lo agora?'), 'Pergunta',
      MB_ICONQUESTION+MB_YESNOCANCEL)=ID_YES then
        MyObjects.FormShow(TFmInstallM y S Q L41, FmInstallM y S Q L41);
    end else begin
      MyObjects.FormCria(TFmServicoManager, FmServicoManager);
      FmServicoManager.ShowModal;
      StatusServico := FmServicoManager.FStatusServico;
      FmServicoManager.Destroy;
      //
      if StatusServico <> 2 then
        MyObjects.FormShow(TFmServerConnect, FmServerConnect);
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  DModG.DefineServidorEIP();
////////////////////////////////////////////////////////////////////////////////
  if ProgessBar <> nil then VAR_CONNECTIPPROGRESS := ProgessBar;
  if StaticText <> nil then VAR_CONNECTIPSTATICTX := StaticText;
  //VAR_PORTA := 3306; j� setado no Dmod
  // 21000 � o m�ximo. Mais que isso a conex�o falha automaticamente
  if ConectaAoServidor(VAR_IP,  VAR_PORTA, 21000) = True then
  begin
    // Inicio Marco Arend
    if VAR_CONNECTIPSTATICTX <> nil then
    begin
      if Uppercase(Application.Title) <> 'PLANNING' then
        VAR_CONNECTIPSTATICTX.Caption   := 'Conex�o a porta '+
        IntToStr(VAR_PORTA) + ' do IP ' + VAR_IP + ' realizada com sucesso!!';
      VAR_CONNECTIPSTATICTX.Update;
    end;
    if VAR_CONNECTIPPROGRESS <> nil then
    begin
      VAR_CONNECTIPPROGRESS.Position  := 0;
      VAR_CONNECTIPPROGRESS.Update;
    end;
    //
    Result := True;
  end else result := False;
end;

procedure TUnGOTOm.EmiteRecibo(Emitente, Beneficiario: Integer; Valor, ValorP, ValorE: Double;
NumRecibo, Referente, ReferenteP, ReferenteE: String; Data: TDateTime; Sit: Integer);
var
  Responsavel, LocalEData: String;
begin
  if (Valor <> 0) and (ValorP <> 0) then
  begin
    Geral.MensagemBox(PChar('Informados "Valor" e "ValorP". "ValorP" '+
    'ser� o considerado!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if Trim(Dmod.QrTerceiroRespons1.Value) <> CO_VAZIO then
  Responsavel := Dmod.QrTerceiroRespons1.Value else Responsavel := CO_VAZIO;
  if Trim(Dmod.QrTerceiroRespons2.Value) <> CO_VAZIO then
  begin
    if Responsavel <> CO_VAZIO then
    Responsavel := Responsavel + ' ou '+ Dmod.QrTerceiroRespons1.Value
    else Responsavel := Dmod.QrTerceiroRespons1.Value;
  end;
  if Responsavel = CO_VAZIO then Responsavel := 'Respons�vel';
  if (Valor <> 0) or (Sit = 4) then
  begin
    MyObjects.FormCria(TFmRecibo, FmRecibo);
    with FmRecibo do
    begin
      // Mudei 2011-02-13 de Dmod para DModG
      EnderecoDeEntidade(Emitente, -1);
      EdNumero.Text    := NumRecibo;
      EdValor.Text     := Geral.FFT(Valor, 2, siPositivo);
      EdEmitente.Text  := DmodG.QrEnderecoNOME_ENT.Value;
      EdECNPJ.Text     := ItemDeEndereco(DmodG.QrEnderecoNO_TIPO_DOC.Value + ': ', DmodG.QrEnderecoCNPJ_TXT.Value);
      EdERua.Text      := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
      EdENumero.Text   := ItemDeEndereco('N�', DmodG.QrEnderecoNUMERO_TXT.Value);
      EdECompl.Text    := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
      EdEBairro.Text   := ItemDeEndereco('Bairro: ', DmodG.QrEnderecoBAIRRO.Value);
      EdECidade.Text   := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
      EdEUF.Text       := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
      EdECEP.Text      := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
      EdEPais.Text     := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
      EdETe1.Text      := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      Texto.Text       := Referente+'.';
      //
      EnderecoDeEntidade(Beneficiario, -1);
      EdBeneficiario.Text := DmodG.QrEnderecoNOME_ENT.Value;
      EdBCNPJ.Text    := ItemDeEndereco(DmodG.QrEnderecoNO_TIPO_DOC.Value + ': ', DmodG.QrEnderecoCNPJ_TXT.Value);
      EdBRua.Text     := ItemDeEndereco(DmodG.QrEnderecoNOMELOGRAD.Value, DmodG.QrEnderecoRUA.Value);
      EdBNumero.Text  := ItemDeEndereco('N�:', DmodG.QrEnderecoNUMERO_TXT.Value);
      EdBCompl.Text   := ItemDeEndereco('Compl.:', DmodG.QrEnderecoCOMPL.Value);
      EdBBairro.Text  := ItemDeEndereco('Bairro:', DmodG.QrEnderecoBAIRRO.Value);
      EdBCidade.Text  := ItemDeEndereco('Cidade:', DmodG.QrEnderecoCIDADE.Value);
      EdBUF.Text      := ItemDeEndereco('UF:', DmodG.QrEnderecoNOMEUF.Value);
      EdBCEP.Text     := ItemDeEndereco('CEP:',Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value));
      EdBPais.Text    := ItemDeEndereco('Pa�s:', DmodG.QrEnderecoPais.Value);
      EdBTe1.Text     := ItemDeEndereco('Tel.:', DmodG.QrEnderecoTE1_TXT.Value);
      //
      LocalEData := Geral.Maiusculas(
        FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Data),
        Geral.EhMinusculas(DmodG.QrEnderecoCIDADE.Value, False));
      if DmodG.QrEnderecoCIDADE.Value <> '' then LocalEData :=
        DmodG.QrEnderecoCIDADE.Value + ', ' + LocalEData;
      EdLocalData.Text := LocalEData+'.';
      EdResponsavel.Text := DmodG.QrEnderecoRespons1.Value;
      // Fim 2011-02-13 de Dmod para DModG
      //
      ShowModal;
      Destroy;
    end;
  end else begin
    Dmod.QrTerceiro.Close;
    Dmod.QrTerceiro.Params[0].AsInteger := Emitente;
    Dmod.QrTerceiro.Open;

    MyObjects.FormCria(TFmRecibos, FmRecibos);
    with FmRecibos do
    begin
      ///////
      // PARTE PESSOAL
      //////
      if Valor <> 0 then
      begin
        if Dmod.QrTerceiroTipo.Value = 1 then
        begin
          EdNumeroP.Text   := NumRecibo;
          EdValorP.Text    := Geral.FFT(Valor, 2, siPositivo);
          EdPPagador.Text  := Dmod.QrTerceiroNome.Value;
          EdPCNPJC.Text    := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCPF.Value);
          EdPRua.Text      := Dmod.QrTerceiroPRua.Value;
          EdPNumero.Text   := IntToStr(Dmod.QrTerceiroPNumero.Value);
          EdPCompl.Text    := Dmod.QrTerceiroPCompl.Value;
          EdPBairro.Text   := Dmod.QrTerceiroPBairro.Value;
          EdPCidade.Text   := Dmod.QrTerceiroPCidade.Value;
          EdPUF.Text       := Dmod.QrTerceiroNOMEpUF.Value;
          EdPCEP.Text      :=Geral.FormataCEP_NT(Dmod.QrTerceiroPCEP.Value);
          EdPPais.Text     := Dmod.QrTerceiroPPais.Value;
          EdPTe1.Text      := Geral.FormataTelefone_TT(Dmod.QrTerceiroPTe1.Value);
          TextoP.Text      := Referente;

        end else begin
          EdNumeroE.Text   := NumRecibo;
          EdValorE.Text    := Geral.FFT(Valor, 2, siPositivo);
          EdEPagador.Text  := Dmod.QrTerceiroRazaoSocial.Value;
          EdECNPJC.Text    := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCNPJ.Value);
          EdERuaC.Text     := Dmod.QrTerceiroERua.Value;
          EdENumeroC.Text  := IntToStr(Dmod.QrTerceiroENumero.Value);
          EdEComplC.Text   := Dmod.QrTerceiroECompl.Value;
          EdEBairroC.Text  := Dmod.QrTerceiroEBairro.Value;
          EdECidadeC.Text  := Dmod.QrTerceiroECidade.Value;
          EdeUFC.Text      := Dmod.QrTerceiroNOMEeUF.Value;
          EdeCEPC.Text     :=Geral.FormataCEP_NT(Dmod.QrTerceiroECEP.Value);
          EdePaisC.Text    := Dmod.QrTerceiroEPais.Value;
          EdeTe1C.Text     := Geral.FormataTelefone_TT(Dmod.QrTerceiroETe1.Value);
          TextoE.Text      := Referente;
        end;
        // Emitente
        Dmod.QrTerceiro.Close;
        Dmod.QrTerceiro.Params[0].AsInteger := Beneficiario;
        Dmod.QrTerceiro.Open;
        if Dmod.QrTerceiroTipo.Value = 1 then
        begin
          EdEmitenteP.Text := Dmod.QrTerceiroNome.Value;
          EdCNPJP.Text     := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCPF.Value);
          EdPRuaE.Text     := Dmod.QrTerceiroPRua.Value;
          EdPNumeroE.Text  := IntToStr(Dmod.QrTerceiroPNumero.Value);
          EdPComplE.Text   := Dmod.QrTerceiroPCompl.Value;
          EdPBairroE.Text  := Dmod.QrTerceiroPBairro.Value;
          EdPCidadeE.Text  := Dmod.QrTerceiroPCidade.Value;
          EdPUFE.Text      := Dmod.QrTerceiroNOMEpUF.Value;
          EdPCEPE.Text     :=Geral.FormataCEP_NT(Dmod.QrTerceiroPCEP.Value);
          EdPPaisE.Text    := Dmod.QrTerceiroPPais.Value;
          EdPTe1E.Text     := Geral.FormataTelefone_TT(Dmod.QrTerceiroPTe1.Value);
        end else begin
          EdEmitenteP.Text := Dmod.QrTerceiroRazaoSocial.Value;
          EdCNPJP.Text     := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCNPJ.Value);
          EdPRuaE.Text     := Dmod.QrTerceiroERua.Value;
          EdPNumeroE.Text  := IntToStr(Dmod.QrTerceiroENumero.Value);
          EdPComplE.Text   := Dmod.QrTerceiroECompl.Value;
          EdPBairroE.Text  := Dmod.QrTerceiroEBairro.Value;
          EdPCidadeE.Text  := Dmod.QrTerceiroECidade.Value;
          EdPUFE.Text      := Dmod.QrTerceiroNOMEeUF.Value;
          EdPCEPE.Text     :=Geral.FormataCEP_NT(Dmod.QrTerceiroECEP.Value);
          EdPPaisE.Text    := Dmod.QrTerceiroEPais.Value;
          EdPTe1E.Text     := Geral.FormataTelefone_TT(Dmod.QrTerceiroETe1.Value);
        end;
      end;
      if ValorP <> 0 then
      begin
        EdNumeroP.Text   := NumRecibo + '-P';
        EdValorP.Text    := Geral.FFT(ValorP, 2, siPositivo);
        EdPPagador.Text  := Dmod.QrTerceiroNome.Value;
        EdPCNPJC.Text    := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCPF.Value);
        EdPRua.Text      := Dmod.QrTerceiroPRua.Value;
        EdPNumero.Text   := IntToStr(Dmod.QrTerceiroPNumero.Value);
        EdPCompl.Text    := Dmod.QrTerceiroPCompl.Value;
        EdPBairro.Text   := Dmod.QrTerceiroPBairro.Value;
        EdPCidade.Text   := Dmod.QrTerceiroPCidade.Value;
        EdPUF.Text       := Dmod.QrTerceiroNOMEpUF.Value;
        EdPCEP.Text      :=Geral.FormataCEP_NT(Dmod.QrTerceiroPCEP.Value);
        EdPPais.Text     := Dmod.QrTerceiroPPais.Value;
        EdPTe1.Text      := Geral.FormataTelefone_TT(Dmod.QrTerceiroPTe1.Value);
        TextoP.Text      := ReferenteP;
        // Emitente
        Dmod.QrTerceiro.Close;
        Dmod.QrTerceiro.Params[0].AsInteger := Beneficiario;
        Dmod.QrTerceiro.Open;
        EdEmitenteP.Text := Dmod.QrTerceiroNome.Value;
        EdCNPJP.Text     := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCPF.Value);
        EdPRuaE.Text     := Dmod.QrTerceiroPRua.Value;
        EdPNumeroE.Text  := IntToStr(Dmod.QrTerceiroPNumero.Value);
        EdPComplE.Text   := Dmod.QrTerceiroPCompl.Value;
        EdPBairroE.Text  := Dmod.QrTerceiroPBairro.Value;
        EdPCidadeE.Text  := Dmod.QrTerceiroPCidade.Value;
        EdPUFE.Text      := Dmod.QrTerceiroNOMEpUF.Value;
        EdPCEPE.Text     :=Geral.FormataCEP_NT(Dmod.QrTerceiroPCEP.Value);
        EdPPaisE.Text    := Dmod.QrTerceiroPPais.Value;
        EdPTe1E.Text     := Geral.FormataTelefone_TT(Dmod.QrTerceiroPTe1.Value);
      end;
      ///////
      //  PARTE EMPRESA
      //////
      if ValorE <> 0 then
      begin
        EdNumeroE.Text   := NumRecibo + '-E';
        EdValorE.Text    := Geral.FFT(ValorE, 2, siPositivo);
        EdEPagador.Text  := Dmod.QrTerceiroRazaoSocial.Value;
        EdECNPJC.Text    := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCNPJ.Value);
        EdERuaC.Text     := Dmod.QrTerceiroERua.Value;
        EdENumeroC.Text  := IntToStr(Dmod.QrTerceiroENumero.Value);
        EdEComplC.Text   := Dmod.QrTerceiroECompl.Value;
        EdEBairroC.Text  := Dmod.QrTerceiroEBairro.Value;
        EdECidadeC.Text  := Dmod.QrTerceiroECidade.Value;
        EdeUFC.Text      := Dmod.QrTerceiroNOMEeUF.Value;
        EdeCEPC.Text     :=Geral.FormataCEP_NT(Dmod.QrTerceiroECEP.Value);
        EdePaisC.Text    := Dmod.QrTerceiroEPais.Value;
        EdeTe1C.Text     := Geral.FormataTelefone_TT(Dmod.QrTerceiroETe1.Value);
        TextoE.Text      := ReferenteE;
        // Emitente
        Dmod.QrTerceiro.Close;
        Dmod.QrTerceiro.Params[0].AsInteger := Beneficiario;
        Dmod.QrTerceiro.Open;
        EdEmitenteP.Text := Dmod.QrTerceiroRazaoSocial.Value;
        EdCNPJP.Text     := Geral.FormataCNPJ_TT(Dmod.QrTerceiroCNPJ.Value);
        EdPRuaE.Text     := Dmod.QrTerceiroERua.Value;
        EdPNumeroE.Text  := IntToStr(Dmod.QrTerceiroENumero.Value);
        EdPComplE.Text   := Dmod.QrTerceiroECompl.Value;
        EdPBairroE.Text  := Dmod.QrTerceiroEBairro.Value;
        EdPCidadeE.Text  := Dmod.QrTerceiroECidade.Value;
        EdPUFE.Text      := Dmod.QrTerceiroNOMEeUF.Value;
        EdPCEPE.Text     :=Geral.FormataCEP_NT(Dmod.QrTerceiroECEP.Value);
        EdPPaisE.Text    := Dmod.QrTerceiroEPais.Value;
        EdPTe1E.Text     := Geral.FormataTelefone_TT(Dmod.QrTerceiroETe1.Value);
      end;
      EdLocalData.Text := Dmod.QrTerceiroECidade.Value;
      if Trim(Dmod.QrTerceiroECidade.Value) <> CO_VAZIO then
      EdLocalData.Text := EdLocalData.Text + ', ';
      EdLocalData.Text := EdLocalData.Text +
        FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Now);
      LaResponsavelP.Caption := Responsavel;
      LaResponsavelE.Caption := Responsavel;
      ShowModal;
      Destroy;
    end;
  end;
end;

function TUnGOTOm.SenhaDesconhecida: Boolean;
  procedure TentaAlterarSenha(NomeBD: String);
  begin
    if not MudaSenhaBD('', 'root', False) then
    if not MudaSenhaBD('852456', 'root', False) then
    begin
      VAR_LOGIN := '';
      VAR_SENHA := '';
      M L A G e r a l.Senha(TFmSenha, FmSenha, 0, '', '', '', '', '', True, '', 0, '');
      if not MudaSenhaBD(VAR_SENHA, VAR_LOGIN, True) then
      begin
        Geral.MensagemBox(PChar('O banco de dados possui senha desconhecida '+
        'e a aplica��o n�o poder� ser iniciada!'), 'Erro', MB_OK+MB_ICONERROR);
        FmPrincipal.Close;
        Application.Terminate;
        Exit;
      end;
    end else Result := False;
    Dmod.ZZDB.Connected := False;
    Dmod.ZZDB.DatabaseName := NomeBD;
  end;
var
  NomeBD: String;
begin
  Result := True;
  try
    Dmod.ZZDB.Connected := False;
    NomeBD := Dmod.ZZDB.DatabaseName;
    Dmod.ZZDB.DatabaseName := 'M y S Q L';
    Dmod.ZZDB.UserPassword := VAR_BDSENHA;
    Dmod.ZZDB.Port         := VAR_PORTA;
    Dmod.ZZDB.ConnectionTimeout := 10;
    try
      Dmod.ZZDB.Connect;
    except
      Geral.MensagemBox(
      'N�o foi poss�vel conectar o "ZZDB" para configura��o do M y S Q L:' +
      #13#10 + #13#10+
      'Database: ' + DMod.ZZDB.DatabaseName + #13#10 +
      'Host: ' + DMod.ZZDB.Host + #13#10 +
      'Porta: ' + FormatFloat('0', VAR_PORTA),
      'ERRO', MB_OK+MB_ICONERROR);
    end;
    if Dmod.ZZDB.Connected then
    begin
      Result := False;
      Dmod.ZZDB.Connected := False;
      Dmod.ZZDB.DatabaseName := NomeBD
    end else begin
      TentaAlterarSenha(NomeBD);
    end;
  except
    TentaAlterarSenha(NomeBD);
  end;
end;

function TUnGOTOm.MudaSenhaBD(Senha, Usuario: String; MsgErr: Boolean): Boolean;
begin
  Result := False;
  Dmod.ZZDB.UserPassword := Senha;
  Dmod.ZZDB.UserName := Usuario;
  try
    Dmod.ZZDB.Connected := True;
    try
      Dmod.ZZDB.Connected := True;
      Dmod.QrUpd.Database := Dmod.ZZDB;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM user ');//WHERE User="root" OR User=""');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO user SET User="root", ');
      Dmod.QrUpd.SQL.Add('Password=PASSWORD("'+VAR_BDSENHA+'"), ');
      Dmod.QrUpd.SQL.Add('Host="localhost",');
      Dmod.QrUpd.SQL.Add('Select_priv="Y",');
      Dmod.QrUpd.SQL.Add('Insert_priv="Y",');
      Dmod.QrUpd.SQL.Add('Update_priv="Y",');
      Dmod.QrUpd.SQL.Add('Delete_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_priv="Y",');
      Dmod.QrUpd.SQL.Add('Drop_priv="Y",');
      Dmod.QrUpd.SQL.Add('Reload_priv="Y",');
      Dmod.QrUpd.SQL.Add('Shutdown_priv="Y",');
      Dmod.QrUpd.SQL.Add('Process_priv="Y",');
      Dmod.QrUpd.SQL.Add('File_priv="Y",');
      Dmod.QrUpd.SQL.Add('Grant_priv="Y",');
      Dmod.QrUpd.SQL.Add('References_priv="Y",');
      Dmod.QrUpd.SQL.Add('Index_priv="Y",');
      Dmod.QrUpd.SQL.Add('Alter_priv="Y",');
      Dmod.QrUpd.SQL.Add('Show_db_priv="Y",');
      Dmod.QrUpd.SQL.Add('Super_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_tmp_table_priv="Y",');
      Dmod.QrUpd.SQL.Add('Lock_tables_priv="Y",');
      Dmod.QrUpd.SQL.Add('Execute_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_slave_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_client_priv="Y"');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO user SET User="root", ');
      Dmod.QrUpd.SQL.Add('Password=PASSWORD("'+VAR_BDSENHA+'"), ');
      Dmod.QrUpd.SQL.Add('Host="%",');
      Dmod.QrUpd.SQL.Add('Select_priv="Y",');
      Dmod.QrUpd.SQL.Add('Insert_priv="Y",');
      Dmod.QrUpd.SQL.Add('Update_priv="Y",');
      Dmod.QrUpd.SQL.Add('Delete_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_priv="Y",');
      Dmod.QrUpd.SQL.Add('Drop_priv="Y",');
      Dmod.QrUpd.SQL.Add('Reload_priv="Y",');
      Dmod.QrUpd.SQL.Add('Shutdown_priv="Y",');
      Dmod.QrUpd.SQL.Add('Process_priv="Y",');
      Dmod.QrUpd.SQL.Add('File_priv="Y",');
      Dmod.QrUpd.SQL.Add('Grant_priv="Y",');
      Dmod.QrUpd.SQL.Add('References_priv="Y",');
      Dmod.QrUpd.SQL.Add('Index_priv="Y",');
      Dmod.QrUpd.SQL.Add('Alter_priv="Y",');
      Dmod.QrUpd.SQL.Add('Show_db_priv="Y",');
      Dmod.QrUpd.SQL.Add('Super_priv="Y",');
      Dmod.QrUpd.SQL.Add('Create_tmp_table_priv="Y",');
      Dmod.QrUpd.SQL.Add('Lock_tables_priv="Y",');
      Dmod.QrUpd.SQL.Add('Execute_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_slave_priv="Y",');
      Dmod.QrUpd.SQL.Add('Repl_client_priv="Y"');
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('FLUSH PRIVILEGES');
      Dmod.QrUpd.ExecSQL;
      ///////////
      Dmod.QrUpd.Database := Dmod.MyDB;
      ///////////
      Result := True;
      ///////////
    except
      if MsgErr then Geral.MensagemBox('N�o foi poss�vel reconfigurar o banco de dados!',
      'Erro', MB_OK+MB_ICONERROR);
    end;
  except
    //Geral.MensagemBox('Senha e/ou usu�rio invalidos!', 'Erro', MB_OK+
    //MB_ICONERROR);
  end;
end;

function TUnGOTOm.TabelaExiste(Tabela: String; Database: TM y S Q LDatabase): Boolean;
begin
  Result := False;
  try
    Dmod.QrMas.Close;
    Dmod.QrMas.Database := Database;
    Dmod.QrMas.SQL.Clear;
    Dmod.QrMas.SQL.Add('SHOW TABLES FROM '+database.databasename);
    Dmod.QrMas.Open;
    //
    Dmod.QrMas.First;
    while not Dmod.QrMas.Eof do
    begin
      if Uppercase(Tabela) = Uppercase(Dmod.QrMas.Fields[0].AsString) then
      begin
        Result := True;
        Exit;
      end;
      Dmod.QrMas.Next;
    end;
  except
    raise;
  end;
end;

function TUnGOTOm.ConectaAoServidor(IP: String; HostPort, CancelTimeMs: Word):
 Boolean;
var
  Conectado: Boolean;
  PError: array[0..255] of Char;
begin
  if VAR_CONNECTIPSTATICTX <> nil then
  begin
    if Uppercase(Application.Title) <> 'PLANNING' then
      VAR_CONNECTIPSTATICTX.Caption   := 'Conectando a porta '+
        IntToStr(HostPort) + ' do IP ' + IP + '...';
    VAR_CONNECTIPSTATICTX.Update;
  end;
  if VAR_CONNECTIPPROGRESS <> nil then
  begin
    VAR_CONNECTIPPROGRESS.Max       := CancelTimeMs;
    VAR_CONNECTIPPROGRESS.Position  := CancelTimeMs;
    VAR_CONNECTIPPROGRESS.Update;
  end;
  //
  Application.ProcessMessages;
  Conectado := M L A G e r a l.IsConnectedToNet(IP, HostPort, CancelTimeMs, 0, PError);
  //Conectado := M L A G e r a l.IsConnectedToNet(IP, HostPort, CancelTimeMs, 3, PError);
  if StrLen(PError) > 0 then
  begin
    Geral.MensagemBox('N�o foi poss�vel a conex�o ao IP: [' +IP+']'+
    Chr(13) + Chr(10)+'Erro retornado: "' + PError+'"'+#13#10+
    'function TUnGOTOm.ConectaAoServidor()',
    'Conex�o ao servidor falhou!', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Result := False;
    Exit;
  end;
  Result := Conectado;
end;

function TUnGOTOm.EnderecoDeEntidade(Entidade, TipoDeEndereco: Integer): String;
begin
  // 2011-02-13 Mudei Dmod para DModG
  DmodG.ReopenEndereco(Entidade);
  if DmodG.QrEndereco.RecordCount > 0 then
  begin
    case TipoDeEndereco of
      0: Result := DmodG.QrEnderecoE_ALL.Value;
      else Result := '************';
    end;
  end else Result := '';
  // fim 2011-02-13
  // ATEN��O:
  // N�o fechar
  //DmodG.QrEndereco.Close;
end;

function TUnGOTOm.EnderecoDeEntidade1(Entidade, TipoDeEndereco: Integer): String;
begin
  DmodG.ReopenEndereco(Entidade);
  //
  if DmodG.QrEndereco.RecordCount > 0 then
  begin
    case TipoDeEndereco of
      0: Result := DmodG.QrEnderecoE_ALL.Value;
      else Result := '************';
    end;
  end else Result := '';
  // N�o fechar
  //Dmod.QrEndereco.Close;
end;

function TUnGOTOm.EnderecoDeEntidade2(Entidade, TipoDeEndereco: Integer): String;
begin
  DmodG.QrEndereco2.Close;
  DmodG.QrEndereco2.Params[0].AsInteger := Entidade;
  DmodG.QrEndereco2.Open;
  if DmodG.QrEndereco2.RecordCount > 0 then
  begin
    case TipoDeEndereco of
      0: Result := DmodG.QrEndereco2E_ALL.Value;
      else Result := '************';
    end;
  end else Result := '';
  // N�o fechar
  //Dmod.QrEndereco.Close;
end;

function TUnGOTOm.ItemDeEndereco(Item, Valor: String): String;
begin
  if Item <> '' then Result := Item + ' ' + Valor
  else Result := Valor;
end;

function TUnGOTOm.DefinePathM y S Q L: Boolean;
begin
  Result := False;
  Dmod.QrAux.Close;
  Dmod.QrAux.Database := Dmod.MyDB;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SHOW VARIABLES');
  Dmod.QrAux.Open;
  VAR_M y S Q LBASEDIR := '';
  VAR_M y S Q LDATADIR := '';
  while not Dmod.QrAux.Eof do
  begin
    if Dmod.QrAux.FieldByName('Variable_name').AsString = 'basedir' then
      VAR_M y S Q LBASEDIR := Dmod.QrAux.FieldByName('Value').AsString;
    if Dmod.QrAux.FieldByName('Variable_name').AsString = 'datadir' then
      VAR_M y S Q LDATADIR := Dmod.QrAux.FieldByName('Value').AsString;
    Dmod.QrAux.Next;
    if (VAR_M y S Q LBASEDIR <> '') and  (VAR_M y S Q LDATADIR <> '') then
    begin
      Result := True;
      Break;
    end;
  end;
  if not Result then
end;
}

procedure TUnGOTOm.DefineBaseDados(Qry: TABSQuery);
begin
{
  if VAR_GOTOM y S Q LDBNAME = nil then Result := VAR_GOTOM y S Q LDBNAME2
  else result := VAR_GOTOM y S Q LDBNAME;
}
  Qry.DatabaseName := 'MEMORY';
  Qry.InMemory := True;
end;

function TUnGOTOm.Fechar(SQLType: TSQLType): TCloseAction;
begin
  Result := caFree;
  if SQLType <> stLok then
  begin
    if SQLType = stUnd then // Desiste
    begin
      Geral.MB_Aviso('Feche o formul�rio pelo bot�o "Desiste" ou "Sair"!');
      Result := TCloseAction.caNone;
    end else
    begin
      Geral.MB_Aviso(SMLA_AVISOFORMCLOSE + DmkEnums.NomeTipoSQL(SQLType) + '.');
      Result := TCloseAction.caNone;
    end;
  end;
end;

end.


