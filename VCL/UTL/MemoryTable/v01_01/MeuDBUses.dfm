object FmMeuDBUses: TFmMeuDBUses
  Left = 0
  Top = 0
  Caption = 'FmMeuDBUses'
  ClientHeight = 633
  ClientWidth = 830
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 120
  TextHeight = 17
  object PnImprime: TPanel
    Left = 0
    Top = 75
    Width = 830
    Height = 330
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 0
    object Grade: TStringGrid
      Left = 131
      Top = 84
      Width = 576
      Height = 179
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ColCount = 2
      DefaultColWidth = 200
      DefaultRowHeight = 18
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
      TabOrder = 0
      RowHeights = (
        18
        18
        18
        18
        18)
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 188
      Top = 31
      Width = 190
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 1
    end
    object dmkDBLookupComboBox1: TdmkDBLookupComboBox
      Left = 408
      Top = 37
      Width = 190
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 2
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object dmkEditCB1: TdmkEditCB
      Left = 643
      Top = 37
      Width = 74
      Height = 27
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      IgnoraDBLookupComboBox = False
    end
    object dmkEditF71: TdmkEditF7
      Left = 466
      Top = 10
      Width = 104
      Height = 28
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'dmkEditF71'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'dmkEditF71'
      ValWarn = False
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 405
    Width = 830
    Height = 174
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    DataSource = DsPG
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object PnConfirma: TPanel
    Left = 0
    Top = 579
    Width = 830
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object Button1: TButton
      Left = 664
      Top = 10
      Width = 98
      Height = 33
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Fecha'
      TabOrder = 0
      OnClick = Button1Click
    end
    object BtImprime: TBitBtn
      Left = 392
      Top = 10
      Width = 98
      Height = 33
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Imprime'
      TabOrder = 1
      OnClick = BtImprimeClick
    end
    object Button2: TButton
      Left = 188
      Top = 10
      Width = 98
      Height = 33
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Grava'
      Enabled = False
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object PnInativo: TPanel
    Left = 0
    Top = 0
    Width = 830
    Height = 75
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 136
      Top = 10
      Width = 41
      Height = 17
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome:'
    end
    object Label2: TLabel
      Left = 565
      Top = 10
      Width = 74
      Height = 17
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'CNPJ / CPF:'
    end
    object EdNome: TEdit
      Left = 136
      Top = 31
      Width = 425
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
    end
    object EdCPF: TEdit
      Left = 565
      Top = 31
      Width = 148
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 1
    end
  end
  object PMImpressao: TPopupMenu
    Left = 380
    Top = 276
    object Visualizaimpresso1: TMenuItem
      Caption = '&Preview (Visualiza impress'#227'o)'
      OnClick = Visualizaimpresso1Click
    end
    object DesignerModificarelatrioantesdeimprimir1: TMenuItem
      Caption = '&Designer (Modifica relat'#243'rio antes de imprimir)'
      OnClick = DesignerModificarelatrioantesdeimprimir1Click
    end
    object ImprimeImprimesemvizualizar1: TMenuItem
      Caption = '&Imprime (Imprime sem vizualizar)'
      OnClick = ImprimeImprimesemvizualizar1Click
    end
  end
  object frxReport1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 38007.714423611100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'procedure Cross1OnPrintCell(Memo: TfrxMemoView; RowIndex, Column' +
        'Index, CellIndex: Integer; RowValues, ColumnValues, Value: Varia' +
        'nt);'
      'var'
      '  s1, s2: String;'
      '  i: Integer;                           '
      'begin'
      '  if Length(Memo.Text) > 0 then        '
      '  begin              '
      '    s1 := Trim(Memo.Text);'
      '    s2 := '#39#39';                        '
      '    for i := 1 to Length(s1) do'
      
        '      if s1[i] in (['#39'0'#39'..'#39'9'#39', '#39','#39', '#39'+'#39', '#39'-'#39', '#39'.'#39']) then s2 := s2' +
        ' + s1[i];'
      '    if s2 = s1 then'
      '      Memo.HAlign := haRight    '
      '    else                '
      '      Memo.HAlign := haLeft;        '
      '  end else Cross1Cell0.HAlign := haCenter;'
      
        '  //ShowMessage(IntToStr(ColumnIndex) + '#39'.'#39' + IntToStr(RowIndex)' +
        ' + '#39' = '#39' + s1);'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnBeforePrint = frxReport1BeforePrint
    OnGetValue = frxReport1GetValue
    Left = 196
    Top = 276
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo2: TfrxMemoView
          Left = 11.338590000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Titulo]')
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 102.047310000000000000
        Width = 1046.929810000000000000
        RowCount = 1
        object Cross1: TfrxCrossView
          Left = 11.338590000000000000
          Width = 100.000000000000000000
          Height = 62.000000000000000000
          DownThenAcross = False
          MinWidth = 30
          ShowColumnHeader = False
          ShowColumnTotal = False
          ShowRowHeader = False
          ShowRowTotal = False
          OnPrintCell = 'Cross1OnPrintCell'
          Memos = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D227574
            662D3822207374616E64616C6F6E653D226E6F223F3E3C63726F73733E3C6365
            6C6C6D656D6F733E3C546672784D656D6F56696577204C6566743D2233312C33
            333835392220546F703D223132322C3034373331222057696474683D22363022
            204865696768743D22323222205265737472696374696F6E733D223234222041
            6C6C6F7745787072657373696F6E733D2246616C73652220466F6E742E436861
            727365743D22312220466F6E742E436F6C6F723D22302220466F6E742E486569
            6768743D222D392220466F6E742E4E616D653D22417269616C2220466F6E742E
            5374796C653D223022204672616D652E5479703D2231352220476170583D2233
            2220476170593D2233222048416C69676E3D22686143656E7465722220506172
            656E74466F6E743D2246616C7365222056416C69676E3D22766143656E746572
            2220546578743D2230222F3E3C546672784D656D6F56696577205461673D2231
            22204C6566743D22302220546F703D2230222057696474683D22302220486569
            6768743D223022205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C736522204672616D652E5479703D22313522
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            687422205374796C653D2263656C6C222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577205461673D2232
            22204C6566743D22302220546F703D2230222057696474683D22302220486569
            6768743D223022205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C736522204672616D652E5479703D22313522
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            687422205374796C653D2263656C6C222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C546672784D656D6F56696577205461673D2233
            22204C6566743D22302220546F703D2230222057696474683D22302220486569
            6768743D223022205265737472696374696F6E733D22382220416C6C6F774578
            7072657373696F6E733D2246616C736522204672616D652E5479703D22313522
            20476170583D22332220476170593D2233222048416C69676E3D226861526967
            687422205374796C653D2263656C6C222056416C69676E3D22766143656E7465
            722220546578743D22222F3E3C2F63656C6C6D656D6F733E3C63656C6C686561
            6465726D656D6F733E3C546672784D656D6F56696577204C6566743D22302220
            546F703D2230222057696474683D223022204865696768743D22302220526573
            7472696374696F6E733D22382220416C6C6F7745787072657373696F6E733D22
            46616C736522204672616D652E5479703D2231352220476170583D2233222047
            6170593D2233222056416C69676E3D22766143656E7465722220546578743D22
            222F3E3C546672784D656D6F56696577204C6566743D22302220546F703D2230
            222057696474683D223022204865696768743D22302220526573747269637469
            6F6E733D22382220416C6C6F7745787072657373696F6E733D2246616C736522
            204672616D652E5479703D2231352220476170583D22332220476170593D2233
            222056416C69676E3D22766143656E7465722220546578743D22222F3E3C2F63
            656C6C6865616465726D656D6F733E3C636F6C756D6E6D656D6F733E3C546672
            784D656D6F56696577205461673D2231303022204C6566743D22302220546F70
            3D2230222057696474683D2232303022204865696768743D2231303030302220
            5265737472696374696F6E733D2232342220416C6C6F7745787072657373696F
            6E733D2246616C736522204672616D652E5479703D2231352220476170583D22
            332220476170593D2233222048416C69676E3D22686143656E74657222205374
            796C653D22636F6C756D6E222056416C69676E3D22766143656E746572222054
            6578743D22222F3E3C2F636F6C756D6E6D656D6F733E3C636F6C756D6E746F74
            616C6D656D6F733E3C546672784D656D6F56696577205461673D223330302220
            4C6566743D22302220546F703D2230222057696474683D223332222048656967
            68743D22313030303022205265737472696374696F6E733D2238222056697369
            626C653D2246616C73652220416C6C6F7745787072657373696F6E733D224661
            6C73652220466F6E742E436861727365743D22312220466F6E742E436F6C6F72
            3D22302220466F6E742E4865696768743D222D31332220466F6E742E4E616D65
            3D22417269616C2220466F6E742E5374796C653D223122204672616D652E5479
            703D2231352220476170583D22332220476170593D2233222048416C69676E3D
            22686143656E7465722220506172656E74466F6E743D2246616C736522205374
            796C653D22636F6C6772616E64222056416C69676E3D22766143656E74657222
            20546578743D224772616E6420546F74616C222F3E3C2F636F6C756D6E746F74
            616C6D656D6F733E3C636F726E65726D656D6F733E3C546672784D656D6F5669
            6577204C6566743D22302220546F703D2230222057696474683D223230302220
            4865696768743D223022205265737472696374696F6E733D2238222056697369
            626C653D2246616C73652220416C6C6F7745787072657373696F6E733D224661
            6C736522204672616D652E5479703D2231352220476170583D22332220476170
            593D2233222048416C69676E3D22686143656E746572222056416C69676E3D22
            766143656E7465722220546578743D22222F3E3C546672784D656D6F56696577
            204C6566743D22302220546F703D2230222057696474683D2232303022204865
            696768743D223022205265737472696374696F6E733D2238222056697369626C
            653D2246616C73652220416C6C6F7745787072657373696F6E733D2246616C73
            6522204672616D652E5479703D2231352220476170583D22332220476170593D
            2233222048416C69676E3D22686143656E746572222056416C69676E3D227661
            43656E7465722220546578743D22222F3E3C546672784D656D6F56696577204C
            6566743D22302220546F703D2230222057696474683D22302220486569676874
            3D223022205265737472696374696F6E733D2238222056697369626C653D2246
            616C73652220416C6C6F7745787072657373696F6E733D2246616C7365222046
            72616D652E5479703D2231352220476170583D22332220476170593D22332220
            48416C69676E3D22686143656E746572222056416C69676E3D22766143656E74
            65722220546578743D22222F3E3C546672784D656D6F56696577204C6566743D
            22302220546F703D2230222057696474683D2232303022204865696768743D22
            3022205265737472696374696F6E733D22382220416C6C6F7745787072657373
            696F6E733D2246616C736522204672616D652E5479703D223135222047617058
            3D22332220476170593D2233222048416C69676E3D22686143656E7465722220
            56416C69676E3D22766143656E7465722220546578743D22222F3E3C2F636F72
            6E65726D656D6F733E3C726F776D656D6F733E3C546672784D656D6F56696577
            205461673D2232303022204C6566743D22302220546F703D2230222057696474
            683D2232303022204865696768743D2231303030302220526573747269637469
            6F6E733D2232342220416C6C6F7745787072657373696F6E733D2246616C7365
            22204672616D652E5479703D2231352220476170583D22332220476170593D22
            33222048416C69676E3D22686143656E74657222205374796C653D22726F7722
            2056416C69676E3D22766143656E7465722220546578743D22222F3E3C2F726F
            776D656D6F733E3C726F77746F74616C6D656D6F733E3C546672784D656D6F56
            696577205461673D2234303022204C6566743D22302220546F703D2230222057
            696474683D22333222204865696768743D223130303030222052657374726963
            74696F6E733D2238222056697369626C653D2246616C73652220416C6C6F7745
            787072657373696F6E733D2246616C73652220466F6E742E436861727365743D
            22312220466F6E742E436F6C6F723D22302220466F6E742E4865696768743D22
            2D31332220466F6E742E4E616D653D22417269616C2220466F6E742E5374796C
            653D223122204672616D652E5479703D2231352220476170583D223322204761
            70593D2233222048416C69676E3D22686143656E7465722220506172656E7446
            6F6E743D2246616C736522205374796C653D22726F776772616E64222056416C
            69676E3D22766143656E7465722220546578743D224772616E6420546F74616C
            222F3E3C2F726F77746F74616C6D656D6F733E3C63656C6C66756E6374696F6E
            733E3C6974656D20302F3E3C2F63656C6C66756E6374696F6E733E3C636F6C75
            6D6E736F72743E3C6974656D20322F3E3C2F636F6C756D6E736F72743E3C726F
            77736F72743E3C6974656D20322F3E3C2F726F77736F72743E3C2F63726F7373
            3E}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 177.637910000000000000
        Width = 1046.929810000000000000
        object Memo1: TfrxMemoView
          Left = 551.710592530000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 224
    Top = 276
  end
  object frxReport2: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40111.836308298610000000
    ReportOptions.LastChange = 40111.836308298610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 264
    Top = 276
    Datasets = <
      item
        DataSet = frxDsPG
        DataSetName = 'frxDsPG'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxDsPG: TfrxDBDataset
    UserName = 'frxDsPG'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 292
    Top = 276
  end
  object DsPG: TDataSource
    Left = 348
    Top = 276
  end
  object PopupMenu1: TPopupMenu
    Left = 380
    Top = 276
    object MenuItem1: TMenuItem
      Caption = '&Preview (Visualiza impress'#227'o)'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = '&Designer (Modifica relat'#243'rio antes de imprimir)'
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = '&Imprime (Imprime sem vizualizar)'
      OnClick = MenuItem3Click
    end
  end
  object TbPG: TABSTable
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Exclusive = False
    Left = 308
    Top = 184
  end
  object QrUpd: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 440
    Top = 152
  end
end
