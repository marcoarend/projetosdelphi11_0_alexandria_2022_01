unit CustomFR3Imp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, frxClass, UnDmkEnums;

type
  TFmCustomFR3Imp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrAtrFrx3Cad: TmySQLQuery;
    QrAtrFrx3CadCodigo: TIntegerField;
    QrAtrFrx3CadCodUsu: TIntegerField;
    QrAtrFrx3CadNome: TWideStringField;
    DsAtrFrx3Cad: TDataSource;
    QrAtrFrx3Its: TmySQLQuery;
    QrAtrFrx3ItsControle: TIntegerField;
    QrAtrFrx3ItsCodUsu: TIntegerField;
    QrAtrFrx3ItsNome: TWideStringField;
    DsAtrFrx3Its: TDataSource;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label6: TLabel;
    EdJanela: TdmkEdit;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdAtrFrx3Cad: TdmkEditCB;
    CBAtrFrx3Cad: TdmkDBLookupComboBox;
    CBAtrFrx3Its: TdmkDBLookupComboBox;
    EdAtrFrx3Its: TdmkEditCB;
    QrCustomFR3: TmySQLQuery;
    DsCustomFR3: TDataSource;
    DBGrid1: TDBGrid;
    QrCustomFR3Codigo: TIntegerField;
    QrCustomFR3CodUsu: TIntegerField;
    QrCustomFR3Nome: TWideStringField;
    QrCustomFR3Report: TWideMemoField;
    QrCustomFR3Janela: TWideStringField;
    QrCustomFR3AtrFrx3Cad: TIntegerField;
    QrCustomFR3AtrFrx3Its: TIntegerField;
    QrCustomFR3NO_ATRCAD: TWideStringField;
    QrCustomFR3NO_ATRITS: TWideStringField;
    frxReport1: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAtrFrx3CadChange(Sender: TObject);
    procedure EdJanelaChange(Sender: TObject);
    procedure EdAtrFrx3ItsChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCustomFR3();
    procedure ReopenAtrFrx3Its(Controle: Integer);
    procedure ReopenAtrFrx3Cad();
  public
    { Public declarations }
    FApenasDefine: Boolean;
    FCustomFR3: WideString;
  end;

  var
  FmCustomFR3Imp: TFmCustomFR3Imp;

implementation

uses UnMyObjects,
{$IFNDEF NO_USE_MYSQLMODULE}
DmkDAC_PF,
{$ENDIF}
Module;

{$R *.DFM}

procedure TFmCustomFR3Imp.BtOKClick(Sender: TObject);
var
  frx: TfrxReport;
  Str: String;
  MemoryStream: TMemoryStream;
begin
  if FApenasDefine then
  begin
    FCustomFR3 := QrCustomFR3Report.Value;
    Close;
  end else
  begin
    MemoryStream := TMemoryStream.Create;
    try
      Str := QrCustomFR3Report.Value;
      MemoryStream.WriteBuffer(Pointer(Str)^, Length(Str));
      MemoryStream.Position := 0;
      frx := TfrxReport.Create(self);
      frx.LoadFromStream(MemoryStream);
      MyObjects.frxMostra(frx, 'Protocolo com desenho pr�-definido');
      //
    finally
      MemoryStream.Free;
    end;
  end;
end;

procedure TFmCustomFR3Imp.BtSaidaClick(Sender: TObject);
begin
  FCustomFR3 := '';
  Close;
end;

procedure TFmCustomFR3Imp.EdAtrFrx3CadChange(Sender: TObject);
begin
  EdAtrFrx3Its.ValueVariant := 0;
  ReopenAtrFrx3Its(0);
  ReopenCustomFR3();
end;

procedure TFmCustomFR3Imp.EdAtrFrx3ItsChange(Sender: TObject);
begin
  ReopenCustomFR3();
end;

procedure TFmCustomFR3Imp.EdJanelaChange(Sender: TObject);
begin
  ReopenCustomFR3();
end;

procedure TFmCustomFR3Imp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCustomFR3Imp.FormCreate(Sender: TObject);
begin
  FApenasDefine := False;
  FCustomFR3 := '';
  //
  ImgTipo.SQLType := stLok;
  //
  ReopenAtrFrx3Cad();
  //
  EdJanela.Text := VAR_FORM_ID;
end;

procedure TFmCustomFR3Imp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCustomFR3Imp.ReopenAtrFrx3Cad;
begin
{$IFNDEF NO_USE_MYSQLMODULE}
  QrAtrFrx3Cad.Close;
  QrAtrFrx3Cad.Database := Dmod.MyDB;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrFrx3Cad, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome ',
  'FROM atrfrx3cad ',
  'ORDER BY Nome ',
  '']);
{$ENDIF}
end;

procedure TFmCustomFR3Imp.ReopenAtrFrx3Its(Controle: Integer);
begin
{$IFNDEF NO_USE_MYSQLMODULE}
  QrAtrFrx3Its.Close;
  QrAtrFrx3Its.Database := Dmod.MyDB;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrFrx3Its, Dmod.MyDB, [
  'SELECT Controle, CodUsu, Nome ',
  'FROM atrfrx3its',
  'WHERE Codigo=' + Geral.FF0(QrAtrFrx3CadCodigo.Value),
  'ORDER BY Nome ',
  '']);
  if Controle <> 0 then
    QrAtrFrx3Its.Locate('Controle', Controle, []);
{$ENDIF}
end;

procedure TFmCustomFR3Imp.ReopenCustomFR3;
var
  Janela, AtrCad, AtrIts: String;
begin
{$IFNDEF NO_USE_MYSQLMODULE}
  if Trim(EdJanela.Text) <> '' then
    Janela := 'AND cfr.Janela = "' + EdJanela.Text + '"';
  if EdAtrFrx3Cad.ValueVariant <> 0 then
    AtrCad := 'AND cfr.AtrFrx3Cad=' + Geral.FF0(EdAtrFrx3Cad.ValueVariant);
  if EdAtrFrx3Its.ValueVariant <> 0 then
    AtrIts := 'AND cfr.AtrFrx3Its=' + Geral.FF0(EdAtrFrx3Its.ValueVariant);
  //
  QrCustomFR3.Close;
  QrCustomFR3.Database := Dmod.MyDB;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCustomFR3, Dmod.MyDB, [
  'SELECT cfr.Codigo, cfr.CodUsu, cfr.Nome, cfr.Report, ',
  'cfr.Janela, cfr.AtrFrx3Cad, cfr.AtrFrx3Its, ',
  'cad.Nome NO_ATRCAD, its.Nome NO_ATRITS ',
  'FROM customfr3 cfr ',
  'LEFT JOIN atrfrx3cad cad ON cad.Codigo=cfr.AtrFrx3Cad ',
  'LEFT JOIN atrfrx3its its ON its.Controle=cfr.AtrFrx3Its ',
  'WHERE cfr.Codigo <> 0 ',
  Janela,
  AtrCad,
  AtrIts,
  'ORDER BY cfr.Nome ',
  '']);
{$ENDIF}
end;

end.
