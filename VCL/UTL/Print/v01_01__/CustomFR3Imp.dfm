object FmCustomFR3Imp: TFmCustomFR3Imp
  Left = 339
  Top = 185
  Caption = 'FRX-PRINT-002 :: Impress'#227'o Customizada'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 293
        Height = 32
        Caption = 'Impress'#227'o Customizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 293
        Height = 32
        Caption = 'Impress'#227'o Customizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 293
        Height = 32
        Caption = 'Impress'#227'o Customizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 330
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 780
          Height = 106
          Align = alTop
          Caption = ' Filtros de pesquisa:'
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 776
            Height = 30
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label6: TLabel
              Left = 12
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Janela:'
            end
            object EdJanela: TdmkEdit
              Left = 48
              Top = 4
              Width = 150
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Nome'
              UpdCampo = 'Nome'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdJanelaChange
            end
          end
          object GroupBox3: TGroupBox
            Left = 2
            Top = 45
            Width = 776
            Height = 59
            Align = alClient
            Caption = ' Atributo diferenciador: '
            TabOrder = 1
            object Label10: TLabel
              Left = 8
              Top = 16
              Width = 77
              Height = 13
              Caption = 'Tipo de atributo:'
            end
            object Label11: TLabel
              Left = 380
              Top = 16
              Width = 80
              Height = 13
              Caption = 'Valor do atributo:'
            end
            object EdAtrFrx3Cad: TdmkEditCB
              Left = 12
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdAtrFrx3CadChange
              DBLookupComboBox = CBAtrFrx3Cad
              IgnoraDBLookupComboBox = False
            end
            object CBAtrFrx3Cad: TdmkDBLookupComboBox
              Left = 68
              Top = 32
              Width = 305
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAtrFrx3Cad
              TabOrder = 1
              dmkEditCB = EdAtrFrx3Cad
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBAtrFrx3Its: TdmkDBLookupComboBox
              Left = 436
              Top = 32
              Width = 321
              Height = 21
              KeyField = 'Controle'
              ListField = 'Nome'
              ListSource = DsAtrFrx3Its
              TabOrder = 2
              dmkEditCB = EdAtrFrx3Its
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdAtrFrx3Its: TdmkEditCB
              Left = 380
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdAtrFrx3ItsChange
              DBLookupComboBox = CBAtrFrx3Its
              IgnoraDBLookupComboBox = False
            end
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 121
          Width = 780
          Height = 207
          Align = alClient
          DataSource = DsCustomFR3
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'd. Usu.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Janela'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ATRCAD'
              Title.Caption = 'Tipo de atributo'
              Width = 152
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ATRITS'
              Title.Caption = 'Valor do Atributo'
              Width = 152
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrAtrFrx3Cad: TmySQLQuery
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM atrfrx3cad'
      'ORDER BY Nome'
      ' ')
    Left = 444
    Top = 12
    object QrAtrFrx3CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtrFrx3CadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrFrx3CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsAtrFrx3Cad: TDataSource
    DataSet = QrAtrFrx3Cad
    Left = 472
    Top = 12
  end
  object QrAtrFrx3Its: TmySQLQuery
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome '
      'FROM ?'
      'WHERE Codigo=:P0'
      'ORDER BY Nome'
      ' ')
    Left = 500
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrFrx3ItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAtrFrx3ItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrFrx3ItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsAtrFrx3Its: TDataSource
    DataSet = QrAtrFrx3Its
    Left = 528
    Top = 12
  end
  object QrCustomFR3: TmySQLQuery
    SQL.Strings = (
      'SELECT cfr.Codigo, cfr.CodUsu, cfr.Nome, cfr.Report, '
      'cfr.Janela, cfr.AtrFrx3Cad, cfr.AtrFrx3Its, '
      'cad.Nome NO_ATRCAD, its.Nome NO_ATRITS'
      'FROM customfr3 cfr'
      'LEFT JOIN atrfrx3cad cad ON cad.Codigo=cfr.AtrFrx3Cad'
      'LEFT JOIN atrfrx3its its ON its.Controle=cfr.AtrFrx3Its'
      'ORDER BY cfr.Nome')
    Left = 360
    Top = 12
    object QrCustomFR3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCustomFR3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCustomFR3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCustomFR3Report: TWideMemoField
      FieldName = 'Report'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCustomFR3Janela: TWideStringField
      FieldName = 'Janela'
    end
    object QrCustomFR3AtrFrx3Cad: TIntegerField
      FieldName = 'AtrFrx3Cad'
    end
    object QrCustomFR3AtrFrx3Its: TIntegerField
      FieldName = 'AtrFrx3Its'
    end
    object QrCustomFR3NO_ATRCAD: TWideStringField
      FieldName = 'NO_ATRCAD'
      Size = 30
    end
    object QrCustomFR3NO_ATRITS: TWideStringField
      FieldName = 'NO_ATRITS'
      Size = 50
    end
  end
  object DsCustomFR3: TDataSource
    DataSet = QrCustomFR3
    Left = 388
    Top = 12
  end
  object frxReport1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41041.751498576390000000
    ReportOptions.LastChange = 41041.751498576390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 564
    Top = 12
    Datasets = <>
    Variables = <>
    Style = <>
  end
end
