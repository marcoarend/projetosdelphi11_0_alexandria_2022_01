unit CustomFR3;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkMemo, frxClass,
  UnDmkProcFunc, dmkImage, dmkValUsu, dmkDBLookupComboBox, dmkEditCB, DmkDAC_PF,
  UnDmkEnums;

type
  TFmCustomFR3 = class(TForm)
    PainelDados: TPanel;
    DsCustomFR3: TDataSource;
    QrCustomFR3: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrCustomFR3Codigo: TIntegerField;
    QrCustomFR3CodUsu: TIntegerField;
    QrCustomFR3Nome: TWideStringField;
    QrCustomFR3Arquivo: TWideStringField;
    QrCustomFR3Report: TWideMemoField;
    frxReport1: TfrxReport;
    DBMemo1: TDBMemo;
    MeReport: TdmkMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBData: TGroupBox;
    PnData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GBEdit: TGroupBox;
    PnEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    SpeedButton5: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdCodUsu: TdmkEdit;
    EdArquivo: TdmkEdit;
    QrCustomFR3Janela: TWideStringField;
    QrCustomFR3AtrFrx3Cad: TIntegerField;
    QrCustomFR3AtrFrx3Its: TIntegerField;
    Label6: TLabel;
    EdJanela: TdmkEdit;
    GroupBox1: TGroupBox;
    SpeedButton6: TSpeedButton;
    Label10: TLabel;
    Label11: TLabel;
    VUAtrCad: TdmkValUsu;
    VUAtrIts: TdmkValUsu;
    QrAtrFrx3Cad: TmySQLQuery;
    DsAtrFrx3Cad: TDataSource;
    DsAtrFrx3Its: TDataSource;
    QrAtrFrx3Its: TmySQLQuery;
    QrAtrFrx3ItsControle: TIntegerField;
    QrAtrFrx3ItsCodUsu: TIntegerField;
    QrAtrFrx3ItsNome: TWideStringField;
    EdAtrFrx3Cad: TdmkEditCB;
    CBAtrFrx3Cad: TdmkDBLookupComboBox;
    CBAtrFrx3Its: TdmkDBLookupComboBox;
    EdAtrFrx3Its: TdmkEditCB;
    QrAtrFrx3CadCodigo: TIntegerField;
    QrAtrFrx3CadCodUsu: TIntegerField;
    QrAtrFrx3CadNome: TWideStringField;
    Label12: TLabel;
    DBEdit3: TDBEdit;
    QrCustomFR3NO_ATRCAD: TWideStringField;
    QrCustomFR3NO_ATRITS: TWideStringField;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QrCustomFR3CU_ATRCAD: TIntegerField;
    QrCustomFR3CU_ATRITS: TIntegerField;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCustomFR3AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCustomFR3BeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure EdAtrFrx3CadChange(Sender: TObject);
    procedure EdAtrFrx3ItsChange(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ImportaFR3();
    procedure ReopenAtrFrx3Its(Controle: Integer);
  public
    { Public declarations }
  end;

var
  FmCustomFR3: TFmCustomFR3;
const
  FFormatFloat = '00000';
  FDiretorio = 'C:\Dermatek\Temp\';

implementation

uses UnMyObjects, Module, CfgAtributos;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCustomFR3.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCustomFR3.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCustomFR3Codigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCustomFR3.DefParams;
begin
  VAR_GOTOTABELA := 'CustomFR3';
  VAR_GOTOMYSQLTABLE := QrCustomFR3;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cfr.Codigo, cfr.CodUsu, cfr.Nome, cfr.Arquivo,');
  VAR_SQLx.Add('cfr.Report, cfr.Janela, cfr.AtrFrx3Cad, cfr.AtrFrx3Its,');
  VAR_SQLx.Add('cad.Nome NO_ATRCAD, its.Nome NO_ATRITS,');
  VAR_SQLx.Add('cad.CodUsu CU_ATRCAD, its.CodUsu CU_ATRITS');
  VAR_SQLx.Add('FROM customfr3 cfr');
  VAR_SQLx.Add('LEFT JOIN atrfrx3cad cad ON cad.Codigo=cfr.AtrFrx3Cad');
  VAR_SQLx.Add('LEFT JOIN atrfrx3its its ON its.Controle=cfr.AtrFrx3Its');
  
  VAR_SQLx.Add('WHERE cfr.Codigo <> 0');
  //
  VAR_SQL1.Add('AND cfr.Codigo=:P0');
  //
  VAR_SQL2.Add('AND cfr.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cfr.Nome Like :P0');
  //
end;

procedure TFmCustomFR3.EdAtrFrx3CadChange(Sender: TObject);
begin
  //UnCfgAtributos.DefineTextoItem('atrfrx3cad', EdAtrFrx3Cad.ValueVariant, EdAtrFrx3Cad_TXT);
  EdAtrFrx3Its.ValueVariant := 0;
  ReopenAtrFrx3Its(0);
end;

procedure TFmCustomFR3.EdAtrFrx3ItsChange(Sender: TObject);
begin
  //UnCfgAtributos.DefineTextoItem('atrfrx3its', EdAtrFrx3Its.ValueVariant, EdAtrFrx3Its_TXT);
end;

procedure TFmCustomFR3.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'customfr3', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmCustomFR3.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Para salvar o relat�rio na tabela, desenhe, salve (na pr�pria janela de desenho) e em seguida importe o relat�rio.');
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCustomFR3.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmCustomFR3.AlteraRegistro;
var
  CustomFR3 : Integer;
begin
  CustomFR3 := QrCustomFR3Codigo.Value;
  if QrCustomFR3Codigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(CustomFR3, Dmod.MyDB, 'CustomFR3', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CustomFR3, Dmod.MyDB, 'CustomFR3', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCustomFR3.IncluiRegistro;
var
  Cursor : TCursor;
  CustomFR3 : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    CustomFR3 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CustomFR3', 'CustomFR3', 'Codigo');
    if Length(FormatFloat(FFormatFloat, CustomFR3))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, CustomFR3);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmCustomFR3.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCustomFR3.ReopenAtrFrx3Its(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtrFrx3Its, Dmod.MyDB, [
  'SELECT Controle, CodUsu, Nome ',
  'FROM atrfrx3its',
  'WHERE Codigo=' + Geral.FF0(QrAtrFrx3CadCodigo.Value),
  'ORDER BY Nome ',
  '']);
  if Controle <> 0 then
    QrAtrFrx3Its.Locate('Controle', Controle, []);
end;

procedure TFmCustomFR3.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCustomFR3.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCustomFR3.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCustomFR3.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCustomFR3.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCustomFR3.SpeedButton5Click(Sender: TObject);
begin
  ImportaFR3();
end;

procedure TFmCustomFR3.SpeedButton6Click(Sender: TObject);
begin
  UnCfgAtributos.InsAltAtrDef_CAB('atrfrx3cad', 'atrfrx3its', '',
  EdAtrFrx3Cad, EdAtrFrx3Its, nil,
  CBAtrFrx3Cad, CBAtrFrx3Its,
  QrAtrFrx3Cad, QrAtrFrx3Its, nil);
end;

procedure TFmCustomFR3.ImportaFR3();
const
  Titulo = 'Arquivo Fast Report';
var
  IniDir, Arquivo: String;
begin
  IniDir  := ExtractFilePath(EdArquivo.Text);
  Arquivo := ExtractFileName(EdArquivo.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, Titulo, '*.fr3|*.fr3', [],
  Arquivo) then
  begin
    EdArquivo.Text := Arquivo;
    MeReport.Lines.LoadFromFile(Arquivo);
  end;
end;

procedure TFmCustomFR3.BitBtn1Click(Sender: TObject);
var
  Arquivo: String;
begin
  if Trim(MeReport.Text) <> '' then
  begin
    Arquivo := FDiretorio + FormatFloat('000000', EdCodigo.ValueVariant) + '.fr3';
    if FileExists(Arquivo) then
      DeleteFile(Arquivo);
    MeReport.Lines.SaveToFile(Arquivo);
    frxReport1.LoadFromFile(Arquivo);
  end else frxReport1.Clear;
  frxReport1.DesignReport;
  MeReport.Lines.LoadFromFile(Arquivo);
end;

procedure TFmCustomFR3.BitBtn2Click(Sender: TObject);
begin
  ImportaFR3();
end;

procedure TFmCustomFR3.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCustomFR3, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'customfr3');
end;

procedure TFmCustomFR3.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCustomFR3Codigo.Value;
  Close;
end;

procedure TFmCustomFR3.BtConfirmaClick(Sender: TObject);
var
  Codigo, CodUsu, AtrFrx3Cad, AtrFrx3Its: Integer;
  Nome, Arquivo, Janela: String;
  Report: WideString;
begin
  Nome := EdNome.Text;
  if MyObjects.FIC(Length(Trim(Nome)) = 0, EdNome, 'Defina uma descri��o.') then
    Exit;
  {
  Aplicacao := RGAplicacao.ItemIndex;
  if MyObjects.FIC(Aplicacao < 0, nil,
  'Informe o tipo de informa��o contida no relat�rio!') then
    Exit;
  }
  AtrFrx3Cad := EdAtrFrx3Cad.ValueVariant;
  AtrFrx3Its := EdAtrFrx3Its.ValueVariant;
  Janela := EdJanela.Text;
  CodUsu  := EdCodUsu.ValueVariant;
  Report  := DmkPF.DuplicaSlash(dmkPF.DuplicaAspasDuplas(MeReport.Text));
  Arquivo := (*????.DuplicaBarras(*)EdArquivo.Text;//);

  Codigo  := UMyMod.BuscaEmLivreY_Def('CustomFR3', 'Codigo', ImgTipo.SQLType,
    QrCustomFR3Codigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'customfr3', False, [
    'CodUsu', 'Nome', 'Arquivo', 'Report',
    'Janela', 'AtrFrx3Cad', 'AtrFrx3Its'
  ], ['Codigo'], [
    CodUsu, Nome, Arquivo, Report,
    Janela, AtrFrx3Cad, AtrFrx3Its
  ], [Codigo], True) then
  begin
    LocCod(Codigo, Codigo);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmCustomFR3.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CustomFR3', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CustomFR3', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CustomFR3', 'Codigo');
end;

procedure TFmCustomFR3.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmCustomFR3.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrCustomFR3, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'customfr3');
  EdJanela.Text := VAR_FORM_ID;
end;

procedure TFmCustomFR3.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQueryApenas(QrAtrFrx3Cad);
  ImgTipo.SQLType := stLok;
  ForceDirectories(FDiretorio);
  MeReport.Align := alClient;
  DBMemo1.Align  := alClient;
  CriaOForm;
end;

procedure TFmCustomFR3.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCustomFR3Codigo.Value, LaRegistro.Caption);
end;

procedure TFmCustomFR3.SbImprimeClick(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := FDiretorio + FormatFloat('000000', QrCustomFR3Codigo.Value) + '.fr3';
  if Trim(QrCustomFR3Report.Value) <> '' then
  begin
    if FileExists(Arquivo) then
      DeleteFile(Arquivo);
    QrCustomFR3Report.SaveToFile(Arquivo);
    frxReport1.LoadFromFile(Arquivo);
  end else frxReport1.Clear;
  MyObjects.frxMostra(frxReport1, QrCustomFR3Nome.Value);
end;

procedure TFmCustomFR3.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCustomFR3.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCustomFR3CodUsu.Value, LaRegistro.Caption);
end;

procedure TFmCustomFR3.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCustomFR3.QrCustomFR3AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCustomFR3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCustomFR3.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCustomFR3Codigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CustomFR3', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCustomFR3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCustomFR3.QrCustomFR3BeforeOpen(DataSet: TDataSet);
begin
  QrCustomFR3Codigo.DisplayFormat := FFormatFloat;
end;

end.

