object FmCustomFR3: TFmCustomFR3
  Left = 368
  Top = 194
  Caption = 'FRX-PRINT-001 :: Desenho de Layout de Impress'#227'o'
  ClientHeight = 557
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 461
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object DBMemo1: TDBMemo
      Left = 0
      Top = 232
      Width = 792
      Height = 165
      Align = alBottom
      DataField = 'Report'
      DataSource = DsCustomFR3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
      WantReturns = False
      WordWrap = False
    end
    object GBData: TGroupBox
      Left = 0
      Top = 0
      Width = 792
      Height = 165
      Align = alTop
      TabOrder = 1
      object PnData: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 86
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 148
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 64
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label12: TLabel
          Left = 628
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Janela:'
        end
        object Label5: TLabel
          Left = 4
          Top = 44
          Width = 142
          Height = 13
          Caption = 'Arquivo (somente informativo):'
        end
        object DBEdCodigo: TDBEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCustomFR3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 148
          Top = 20
          Width = 477
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsCustomFR3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 64
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsCustomFR3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 4
          Top = 60
          Width = 777
          Height = 21
          DataField = 'Arquivo'
          DataSource = DsCustomFR3
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 628
          Top = 20
          Width = 150
          Height = 21
          DataField = 'Janela'
          DataSource = DsCustomFR3
          TabOrder = 4
        end
      end
      object GroupBox2: TGroupBox
        Left = 2
        Top = 101
        Width = 788
        Height = 62
        Align = alClient
        Caption = ' Atributo diferenciador: '
        TabOrder = 1
        object Label13: TLabel
          Left = 8
          Top = 16
          Width = 77
          Height = 13
          Caption = 'Tipo de atributo:'
        end
        object Label14: TLabel
          Left = 380
          Top = 16
          Width = 80
          Height = 13
          Caption = 'Valor do atributo:'
        end
        object DBEdit4: TDBEdit
          Left = 68
          Top = 32
          Width = 305
          Height = 21
          DataField = 'NO_ATRCAD'
          DataSource = DsCustomFR3
          TabOrder = 0
        end
        object DBEdit5: TDBEdit
          Left = 436
          Top = 32
          Width = 345
          Height = 21
          DataField = 'NO_ATRITS'
          DataSource = DsCustomFR3
          TabOrder = 1
        end
        object DBEdit6: TDBEdit
          Left = 12
          Top = 32
          Width = 56
          Height = 21
          DataField = 'CU_ATRCAD'
          DataSource = DsCustomFR3
          TabOrder = 2
        end
        object DBEdit7: TDBEdit
          Left = 380
          Top = 32
          Width = 56
          Height = 21
          DataField = 'CU_ATRITS'
          DataSource = DsCustomFR3
          TabOrder = 3
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 397
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 461
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object MeReport: TdmkMemo
      Left = 0
      Top = 260
      Width = 792
      Height = 138
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
      WordWrap = False
      QryCampo = 'Report'
      UpdCampo = 'Report'
      UpdType = utYes
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 398
      Width = 792
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 658
        Top = 15
        Width = 132
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 4
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BitBtn1: TBitBtn
        Left = 272
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desenhar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Left = 396
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Importar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BitBtn2Click
      end
    end
    object GBEdit: TGroupBox
      Left = 0
      Top = 0
      Width = 792
      Height = 165
      Align = alTop
      TabOrder = 2
      object PnEdit: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 86
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label8: TLabel
          Left = 64
          Top = 4
          Width = 57
          Height = 13
          Caption = 'C'#243'digo: [F4]'
        end
        object Label9: TLabel
          Left = 148
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label4: TLabel
          Left = 4
          Top = 44
          Width = 142
          Height = 13
          Caption = 'Arquivo (somente informativo):'
        end
        object SpeedButton5: TSpeedButton
          Left = 760
          Top = 60
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label6: TLabel
          Left = 628
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Janela:'
        end
        object EdCodigo: TdmkEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 148
          Top = 20
          Width = 477
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 64
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodUsuKeyDown
        end
        object EdArquivo: TdmkEdit
          Left = 4
          Top = 60
          Width = 753
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Arquivo'
          UpdCampo = 'Arquivo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdJanela: TdmkEdit
          Left = 628
          Top = 20
          Width = 150
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Janela'
          UpdCampo = 'Janela'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox1: TGroupBox
        Left = 2
        Top = 101
        Width = 788
        Height = 62
        Align = alClient
        Caption = ' Atributo diferenciador: '
        TabOrder = 1
        object SpeedButton6: TSpeedButton
          Left = 760
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton6Click
        end
        object Label10: TLabel
          Left = 8
          Top = 16
          Width = 77
          Height = 13
          Caption = 'Tipo de atributo:'
        end
        object Label11: TLabel
          Left = 380
          Top = 16
          Width = 80
          Height = 13
          Caption = 'Valor do atributo:'
        end
        object EdAtrFrx3Cad: TdmkEditCB
          Left = 12
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdAtrFrx3CadChange
          DBLookupComboBox = CBAtrFrx3Cad
          IgnoraDBLookupComboBox = False
        end
        object CBAtrFrx3Cad: TdmkDBLookupComboBox
          Left = 68
          Top = 32
          Width = 305
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAtrFrx3Cad
          TabOrder = 1
          dmkEditCB = EdAtrFrx3Cad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBAtrFrx3Its: TdmkDBLookupComboBox
          Left = 436
          Top = 32
          Width = 321
          Height = 21
          KeyField = 'Controle'
          ListField = 'Nome'
          ListSource = DsAtrFrx3Its
          TabOrder = 2
          dmkEditCB = EdAtrFrx3Its
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAtrFrx3Its: TdmkEditCB
          Left = 380
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAtrFrx3Its
          IgnoraDBLookupComboBox = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 410
        Height = 32
        Caption = 'Desenho de Layout de Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 410
        Height = 32
        Caption = 'Desenho de Layout de Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 410
        Height = 32
        Caption = 'Desenho de Layout de Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCustomFR3: TDataSource
    DataSet = QrCustomFR3
    Left = 656
    Top = 20
  end
  object QrCustomFR3: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCustomFR3BeforeOpen
    AfterOpen = QrCustomFR3AfterOpen
    SQL.Strings = (
      'SELECT cfr.Codigo, cfr.CodUsu, cfr.Nome, cfr.Arquivo,'
      'cfr.Report, cfr.Janela, cfr.AtrFrx3Cad, cfr.AtrFrx3Its,'
      'cad.Nome NO_ATRCAD, its.Nome NO_ATRITS,'
      'cad.CodUsu CU_ATRCAD, its.CodUsu CU_ATRITS'
      'FROM customfr3 cfr'
      'LEFT JOIN atrfrx3cad cad ON cad.Codigo=cfr.AtrFrx3Cad'
      'LEFT JOIN atrfrx3its its ON its.Controle=cfr.AtrFrx3Its'
      'ORDER BY cfr.Nome')
    Left = 628
    Top = 20
    object QrCustomFR3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCustomFR3CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCustomFR3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrCustomFR3Arquivo: TWideStringField
      FieldName = 'Arquivo'
      Size = 255
    end
    object QrCustomFR3Report: TWideMemoField
      FieldName = 'Report'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCustomFR3Janela: TWideStringField
      FieldName = 'Janela'
    end
    object QrCustomFR3AtrFrx3Cad: TIntegerField
      FieldName = 'AtrFrx3Cad'
    end
    object QrCustomFR3AtrFrx3Its: TIntegerField
      FieldName = 'AtrFrx3Its'
    end
    object QrCustomFR3NO_ATRCAD: TWideStringField
      FieldName = 'NO_ATRCAD'
      Size = 30
    end
    object QrCustomFR3NO_ATRITS: TWideStringField
      FieldName = 'NO_ATRITS'
      Size = 50
    end
    object QrCustomFR3CU_ATRCAD: TIntegerField
      FieldName = 'CU_ATRCAD'
      Required = True
    end
    object QrCustomFR3CU_ATRITS: TIntegerField
      FieldName = 'CU_ATRITS'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel2
    CanUpd01 = BtInclui
    CanDel01 = BtAltera
    Left = 684
    Top = 20
  end
  object frxReport1: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39025.537419340290000000
    ReportOptions.LastChange = 39025.537419340290000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 716
    Top = 20
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object VUAtrCad: TdmkValUsu
    dmkEditCB = EdAtrFrx3Cad
    Panel = PainelEdita
    QryCampo = 'AtrFrx3Cad'
    UpdCampo = 'AtrFrx3Cad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 264
    Top = 64
  end
  object VUAtrIts: TdmkValUsu
    dmkEditCB = EdAtrFrx3Its
    Panel = PainelEdita
    QryCampo = 'AtrFrx3Its'
    UpdCampo = 'AtrFrx3Its'
    RefCampo = 'Controle'
    UpdType = utYes
    Left = 292
    Top = 64
  end
  object QrAtrFrx3Cad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM atrfrx3cad'
      'ORDER BY Nome'
      ' ')
    Left = 320
    Top = 64
    object QrAtrFrx3CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtrFrx3CadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrFrx3CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsAtrFrx3Cad: TDataSource
    DataSet = QrAtrFrx3Cad
    Left = 348
    Top = 64
  end
  object DsAtrFrx3Its: TDataSource
    DataSet = QrAtrFrx3Its
    Left = 404
    Top = 64
  end
  object QrAtrFrx3Its: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome '
      'FROM ?'
      'WHERE Codigo=:P0'
      'ORDER BY Nome'
      ' ')
    Left = 376
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrFrx3ItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAtrFrx3ItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrFrx3ItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
