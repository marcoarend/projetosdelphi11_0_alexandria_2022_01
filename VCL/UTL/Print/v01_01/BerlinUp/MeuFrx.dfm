object FmMeuFrx: TFmMeuFrx
  Left = 242
  Top = 181
  Caption = 'FRX-PRINT-001 :: Preview de Impress'#227'o fr3'
  ClientHeight = 384
  ClientWidth = 791
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnMouseWheel = FormMouseWheel
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnControle: TPanel
    Left = 0
    Top = 0
    Width = 791
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 166
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BtSalva: TBitBtn
        Tag = 161
        Left = 82
        Top = 4
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtSalvaClick
      end
      object BtAbre: TBitBtn
        Tag = 274
        Left = 43
        Top = 4
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAbreClick
      end
      object BtImprime: TBitBtn
        Tag = 159
        Left = 4
        Top = 4
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 122
        Top = 4
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtPesquisaClick
      end
    end
    object Panel2: TPanel
      Left = 166
      Top = 0
      Width = 225
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 46
        Top = 6
        Width = 56
        Height = 13
        Caption = 'Zoom atual:'
      end
      object BtZoomMais: TBitBtn
        Tag = 272
        Left = 3
        Top = 4
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtZoomMaisClick
      end
      object BtZoomMenos: TBitBtn
        Tag = 273
        Left = 142
        Top = 4
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtZoomMenosClick
      end
      object BtTelaCheia: TBitBtn
        Tag = 275
        Left = 181
        Top = 4
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTelaCheiaClick
      end
      object CbZoom: TComboBox
        Left = 46
        Top = 22
        Width = 93
        Height = 21
        TabOrder = 3
        Text = '100%'
        OnClick = CbZoomClick
      end
    end
    object Panel3: TPanel
      Left = 766
      Top = 0
      Width = 180
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object BtExporta: TBitBtn
        Tag = 10036
        Left = 5
        Top = 4
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtExportaClick
      end
      object BtCustom: TBitBtn
        Tag = 473
        Left = 52
        Top = 4
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtCustomClick
      end
      object BtDesign: TBitBtn
        Tag = 474
        Left = 91
        Top = 4
        Width = 40
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtDesignClick
      end
      object BtGerencia: TBitBtn
        Tag = 472
        Left = 131
        Top = 4
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtGerenciaClick
      end
    end
    object Panel4: TPanel
      Left = 744
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
      object BtSaida: TBitBtn
        Tag = 13
        Left = 4
        Top = 4
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 391
      Top = 0
      Width = 167
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 4
      object BtOutline: TBitBtn
        Tag = 277
        Left = 4
        Top = 4
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOutlineClick
      end
      object BtThumbnail: TBitBtn
        Tag = 278
        Left = 43
        Top = 4
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtThumbnailClick
      end
      object BtSetup: TBitBtn
        Tag = 276
        Left = 82
        Top = 4
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtSetupClick
      end
      object BtEdit: TBitBtn
        Tag = 11
        Left = 122
        Top = 4
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtEditClick
      end
    end
    object Panel6: TPanel
      Left = 558
      Top = 0
      Width = 208
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 5
      object EdPageE: TdmkEdit
        Left = 86
        Top = 16
        Width = 37
        Height = 14
        Alignment = taRightJustify
        BorderStyle = bsNone
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = True
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnClick = EdPageEClick
        OnKeyDown = EdPageEKeyDown
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 5
        Top = 4
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 44
        Top = 4
        Width = 40
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 127
        Top = 4
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton3Click
      end
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 166
        Top = 4
        Width = 40
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = SpeedButton4Click
      end
    end
  end
  object PvVer: TfrxPreview
    Left = 0
    Top = 47
    Width = 791
    Height = 321
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    OutlineVisible = False
    OutlineWidth = 120
    ThumbnailVisible = False
    FindFmVisible = False
    OnPageChanged = PvVerPageChanged
    UseReportHints = True
    OutlineTreeSortType = dtsUnsorted
    HideScrolls = False
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 368
    Width = 791
    Height = 16
    Panels = <
      item
        Text = 'Page 1 of 1000'
        Width = 160
      end
      item
        Width = 40
      end>
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    DefaultPath = 'C:\Dermatek'
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    EmbedFontsIfProtected = False
    InteractiveFormsFontSubset = 'A-Z,a-z,0-9,#43-#47 '
    OpenAfterExport = False
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    Creator = 'FastReport (http://www.fast-report.com)'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = True
    PDFStandard = psPDFA_2a
    PDFVersion = pv17
    Left = 192
    Top = 60
  end
  object frxHTMLExport1: TfrxHTMLExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    FixedWidth = True
    Background = False
    Centered = False
    EmptyLines = True
    Print = False
    PictureType = gpPNG
    Outline = False
    Left = 220
    Top = 60
  end
  object frxBMPExport1: TfrxBMPExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Left = 332
    Top = 60
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = False
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 304
    Top = 60
  end
  object PMExporta: TPopupMenu
    Left = 164
    Top = 60
    object WEB1: TMenuItem
      Caption = 'Publicar na WEB'
      OnClick = WEB1Click
    end
    object EMAIL1: TMenuItem
      Caption = 'E-MAIL (Dermatek)'
      OnClick = EMAIL1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object PDF1: TMenuItem
      Caption = 'PDF'
      OnClick = PDF1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object XLS1: TMenuItem
      Caption = 'XLS'
      OnClick = XLS1Click
    end
    object XLSX1: TMenuItem
      Caption = 'XLSX'
      OnClick = XLSX1Click
    end
    object PPTX1: TMenuItem
      Caption = 'PPTX'
      OnClick = PPTX1Click
    end
    object DOCX1: TMenuItem
      Caption = 'DOCX'
      OnClick = DOCX1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object JPG1: TMenuItem
      Caption = 'JPG'
      OnClick = JPG1Click
    end
    object BMP1: TMenuItem
      Caption = 'BMP'
      OnClick = BMP1Click
    end
    object SVG1: TMenuItem
      Caption = 'SVG'
      OnClick = SVG1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object HTML51: TMenuItem
      Caption = 'HTML 5'
      OnClick = HTML51Click
    end
    object HTML41: TMenuItem
      Caption = 'HTML 4'
      OnClick = HTML41Click
    end
    object HTML1: TMenuItem
      Caption = 'HTML'
      OnClick = HTML1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object XLM1: TMenuItem
      Caption = 'XML'
      OnClick = XLM1Click
    end
    object RTF1: TMenuItem
      Caption = 'RTF'
      OnClick = RTF1Click
    end
    object TIF1: TMenuItem
      Caption = 'TIF'
      OnClick = TIF1Click
    end
    object TXT1: TMenuItem
      Caption = 'TXT'
      OnClick = TXT1Click
    end
    object CSV1: TMenuItem
      Caption = 'CSV'
      OnClick = CSV1Click
    end
    object ODS1: TMenuItem
      Caption = 'ODS'
      OnClick = ODS1Click
    end
    object ODT1: TMenuItem
      Caption = 'ODT'
      OnClick = ODT1Click
    end
    object DBF1: TMenuItem
      Caption = 'DBF'
      OnClick = DBF1Click
    end
    object BIFF1: TMenuItem
      Caption = 'BIFF'
      OnClick = BIFF1Click
    end
    object Emeio1: TMenuItem
      Caption = 'E-MAIL'
      OnClick = Emeio1Click
    end
  end
  object frxJPEGExport1: TfrxJPEGExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Left = 360
    Top = 60
  end
  object frxTIFFExport1: TfrxTIFFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Left = 388
    Top = 60
  end
  object frxGIFExport1: TfrxGIFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Left = 416
    Top = 60
  end
  object frxSimpleTextExport1: TfrxSimpleTextExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Frames = False
    EmptyLines = False
    OEMCodepage = False
    UTF8 = False
    OpenAfterExport = False
    DeleteEmptyColumns = True
    Left = 444
    Top = 60
  end
  object frxCSVExport1: TfrxCSVExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Separator = ';'
    OEMCodepage = False
    UTF8 = False
    OpenAfterExport = False
    NoSysSymbols = True
    ForcedQuotes = False
    Left = 472
    Top = 60
  end
  object PMPesquisa: TPopupMenu
    Left = 612
    Top = 60
    object Localizar1: TMenuItem
      Caption = 'Localizar'
      OnClick = Localizar1Click
    end
    object Localizarprximo1: TMenuItem
      Caption = 'Localizar pr'#243'ximo'
      OnClick = Localizarprximo1Click
    end
  end
  object frxOLEObject1: TfrxOLEObject
    Left = 652
    Top = 284
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 488
    Top = 360
  end
  object frxRichObject1: TfrxRichObject
    Left = 796
    Top = 296
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 740
    Top = 224
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 372
    Top = 280
  end
  object frxXLSXExport1: TfrxXLSXExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ChunkSize = 0
    OpenAfterExport = False
    PictureType = gpPNG
    Left = 1052
    Top = 100
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 1052
    Top = 148
  end
  object frxXMLExport1: TfrxXMLExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Background = True
    Creator = 'FastReport'
    EmptyLines = True
    SuppressPageHeadersFooters = False
    RowsCount = 0
    Split = ssNotSplit
    Left = 1052
    Top = 196
  end
  object frxSVGExport1: TfrxSVGExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    MultiPage = False
    Formatted = False
    PictureFormat = pfPNG
    UnifiedPictures = True
    Navigation = False
    EmbeddedPictures = True
    EmbeddedCSS = True
    Outline = False
    Left = 1052
    Top = 244
  end
  object frxPPTXExport1: TfrxPPTXExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PictureType = gpPNG
    Left = 1052
    Top = 292
  end
  object frxHTML4DivExport1: TfrxHTML4DivExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    MultiPage = False
    Formatted = False
    PictureFormat = pfPNG
    UnifiedPictures = True
    Navigation = False
    EmbeddedPictures = False
    EmbeddedCSS = False
    Outline = False
    HTML5 = False
    AllPictures = False
    ExportAnchors = True
    PictureTag = 0
    Left = 1052
    Top = 340
  end
  object frxHTML5DivExport1: TfrxHTML5DivExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    MultiPage = False
    Formatted = False
    PictureFormat = pfPNG
    UnifiedPictures = True
    Navigation = False
    EmbeddedPictures = True
    EmbeddedCSS = True
    Outline = False
    HTML5 = True
    AllPictures = False
    ExportAnchors = True
    PictureTag = 0
    Left = 1052
    Top = 388
  end
  object frxBIFFExport1: TfrxBIFFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    RowHeightScale = 1.000000000000000000
    ChunkSize = 0
    Inaccuracy = 10.000000000000000000
    FitPages = False
    Pictures = True
    ParallelPages = 0
    Left = 1052
    Top = 436
  end
  object frxDBFExport1: TfrxDBFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OEMCodepage = False
    Left = 1052
    Top = 484
  end
  object frxDOCXExport1: TfrxDOCXExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PictureType = gpPNG
    ExportType = dxTable
    Left = 1052
    Top = 532
  end
  object frxMailExport1: TfrxMailExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ShowExportDialog = True
    SmtpPort = 25
    UseIniFile = True
    TimeOut = 60
    ConfurmReading = False
    UseMAPI = SMTP
    MAPISendFlag = 0
    Left = 960
    Top = 100
  end
  object frxODSExport1: TfrxODSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 0.000000000000000000
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = False
    Background = True
    Creator = 'FastReport'
    SingleSheet = False
    Language = 'en'
    SuppressPageHeadersFooters = False
    Left = 960
    Top = 148
  end
  object frxODTExport1: TfrxODTExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 0.000000000000000000
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = False
    Background = True
    Creator = 'FastReport'
    SingleSheet = False
    Language = 'en'
    SuppressPageHeadersFooters = False
    Left = 960
    Top = 196
  end
end
