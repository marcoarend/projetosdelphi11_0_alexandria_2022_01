unit MeuFrx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, frxClass, frxPreview, ComCtrls,
  (*frxExportODF, frxExportMail, frxExportXML, frxExportXLS, frxExportSVG,
  frxExportHTMLDiv, frxExportPPTX, frxExportDOCX, frxExportDBF, frxExportBIFF,
  frxExportXLSX, *)
  frxExportCSV, frxExportText, frxExportImage, frxExportRTF, frxExportHTML,
  frxExportPDF, UnMyObjects, UnInternalConsts, frxGradient, frxChBox, frxRich,
  frxBarcode, frxOLE, frxChart, dmkEdit, TypInfo, Vcl.Menus, frxExportXLSX,
  frxExportXML, frxExportXLS, frxExportBIFF, frxExportHTMLDiv, frxExportPPTX,
  frxExportSVG, frxExportODF, frxExportMail, frxExportDOCX, frxExportDBF,
  //frxExportBaseDialog,
  frxExportHelpers, frxExportBaseDialog;

const
  WM_UPDATEzoom = WM_USER + 1;

type
  TFmMeuFrx = class(TForm)
    PnControle: TPanel;
    PvVer: TfrxPreview;
    Panel1: TPanel;
    BtSalva: TBitBtn;
    BtAbre: TBitBtn;
    BtImprime: TBitBtn;
    Panel2: TPanel;
    BtZoomMais: TBitBtn;
    BtZoomMenos: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    BtSaida: TBitBtn;
    BtTelaCheia: TBitBtn;
    StatusBar: TStatusBar;
    Panel5: TPanel;
    Panel6: TPanel;
    EdPageE: TdmkEdit;
    SpeedButton1: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton4: TBitBtn;
    CbZoom: TComboBox;
    Label1: TLabel;
    BtOutline: TBitBtn;
    BtThumbnail: TBitBtn;
    BtSetup: TBitBtn;
    BtEdit: TBitBtn;
    frxPDFExport1: TfrxPDFExport;
    BtExporta: TBitBtn;
    frxHTMLExport1: TfrxHTMLExport;
    frxBMPExport1: TfrxBMPExport;
    frxRTFExport1: TfrxRTFExport;
    PMExporta: TPopupMenu;
    PDF1: TMenuItem;
    HTML1: TMenuItem;
    XLS1: TMenuItem;
    XLM1: TMenuItem;
    RTF1: TMenuItem;
    BMP1: TMenuItem;
    frxJPEGExport1: TfrxJPEGExport;
    frxTIFFExport1: TfrxTIFFExport;
    frxGIFExport1: TfrxGIFExport;
    frxSimpleTextExport1: TfrxSimpleTextExport;
    frxCSVExport1: TfrxCSVExport;
    JPG1: TMenuItem;
    TIF1: TMenuItem;
    TXT1: TMenuItem;
    CSV1: TMenuItem;
    Emeio1: TMenuItem;
    ODS1: TMenuItem;
    ODT1: TMenuItem;
    BtCustom: TBitBtn;
    BtDesign: TBitBtn;
    BtGerencia: TBitBtn;
    BtPesquisa: TBitBtn;
    PMPesquisa: TPopupMenu;
    Localizar1: TMenuItem;
    Localizarprximo1: TMenuItem;
    N1: TMenuItem;
    WEB1: TMenuItem;
    EMAIL1: TMenuItem;
    frxOLEObject1: TfrxOLEObject;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxRichObject1: TfrxRichObject;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxGradientObject1: TfrxGradientObject;
    SVG1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    HTML51: TMenuItem;
    HTML41: TMenuItem;
    PPTX1: TMenuItem;
    DOCX1: TMenuItem;
    DBF1: TMenuItem;
    N5: TMenuItem;
    BIFF1: TMenuItem;
    N4: TMenuItem;
    XLSX1: TMenuItem;
    frxXLSXExport1: TfrxXLSXExport;
    frxXLSExport1: TfrxXLSExport;
    frxXMLExport1: TfrxXMLExport;
    frxSVGExport1: TfrxSVGExport;
    frxPPTXExport1: TfrxPPTXExport;
    frxHTML4DivExport1: TfrxHTML4DivExport;
    frxHTML5DivExport1: TfrxHTML5DivExport;
    frxBIFFExport1: TfrxBIFFExport;
    frxDBFExport1: TfrxDBFExport;
    frxDOCXExport1: TfrxDOCXExport;
    frxMailExport1: TfrxMailExport;
    frxODSExport1: TfrxODSExport;
    frxODTExport1: TfrxODTExport;
    procedure BtSalvaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure BtZoomMaisClick(Sender: TObject);
    procedure BtZoomMenosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTelaCheiaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdPageEClick(Sender: TObject);
    procedure CbZoomClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure BtOutlineClick(Sender: TObject);
    procedure BtThumbnailClick(Sender: TObject);
    procedure BtSetupClick(Sender: TObject);
    procedure BtEditClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtExportaClick(Sender: TObject);
    procedure PDF1Click(Sender: TObject);
    procedure HTML1Click(Sender: TObject);
    procedure XLS1Click(Sender: TObject);
    procedure XLM1Click(Sender: TObject);
    procedure RTF1Click(Sender: TObject);
    procedure BMP1Click(Sender: TObject);
    procedure JPG1Click(Sender: TObject);
    procedure TIF1Click(Sender: TObject);
    procedure TXT1Click(Sender: TObject);
    procedure CSV1Click(Sender: TObject);
    procedure Emeio1Click(Sender: TObject);
    procedure ODS1Click(Sender: TObject);
    procedure ODT1Click(Sender: TObject);
    procedure BtCustomClick(Sender: TObject);
    procedure BtDesignClick(Sender: TObject);
    procedure BtGerenciaClick(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure BtPesquisaClick(Sender: TObject);
    procedure Localizar1Click(Sender: TObject);
    procedure Localizarprximo1Click(Sender: TObject);
    procedure WEB1Click(Sender: TObject);
    procedure EMAIL1Click(Sender: TObject);
    procedure EdPageEKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SVG1Click(Sender: TObject);
    procedure HTML51Click(Sender: TObject);
    procedure HTML41Click(Sender: TObject);
    procedure PPTX1Click(Sender: TObject);
    procedure DOCX1Click(Sender: TObject);
    procedure DBF1Click(Sender: TObject);
    procedure BIFF1Click(Sender: TObject);
    procedure XLSX1Click(Sender: TObject);
    procedure PvVerPageChanged(Sender: TfrxPreview; PageNo: Integer);
  private
    { Private declarations }
    FOldBS: TFormBorderStyle;
    FOldState: TWindowState;
    FFullScreen: Boolean;
    FBtnState: Integer;
    procedure WMUpdateZoom(var Message: TMessage); message WM_UPDATEzoom;
    procedure VerPageChange(PageNo: Integer);
    function  ObtemCaminho(Sender: TfrxCustomExportFilter): Boolean;
    procedure InfoNoExport();
  public
    { Public declarations }
    FMeuRelatorio, FNomeArq: String;
    FMeuRelShowed: Boolean;
    FImprimiu: Boolean;
    procedure SwitchToFullScreen;
    procedure SaveToFile;
    procedure LoadFromFile;
    procedure UpdateZoom;
    function ChangeBtnState(Clicked: Integer): Integer;
  end;

  var
  FmMeuFrx: TFmMeuFrx;

implementation

uses
{$IfNDef NO_USE_FR3CUSTOM}
CustomFR3Imp, CustomFR3,
{$EndIf}
{$IfDef UsaModuloFTP}
Module, ModuleGeral, UnDmkWeb, UnFTP, MyListas,
{$EndIf}
{$IfDef TEM_UH and TEM_DBWEB}
//Igual ao de cima pois n�o funciona o comando "OR" no "IFDEF"
Module, ModuleGeral, UnDmkWeb, UnFTP, MyListas,
{$EndIf}
{$IfNDef NO_USE_EMAILDMK} UnMailEnv, {$EndIf}
UnDmkEnums, UnDmkProcFunc, dmkGeral, UnGrl_Geral;

{$R *.DFM}

procedure TFmMeuFrx.BtSalvaClick(Sender: TObject);
begin
  SaveToFile;
end;

procedure TFmMeuFrx.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMeuFrx.BtImprimeClick(Sender: TObject);
begin
  PvVer.Print;
end;

procedure TFmMeuFrx.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMeuFrx.SaveToFile;
var
  SaveDlg: TSaveDialog;
begin
  //if FRunning then Exit;
  SaveDlg := TSaveDialog.Create(Application);
  try
    SaveDlg.Filter := 'Arquivos Fast Report (*.fp3)|*.fp3';
    if SaveDlg.Execute then
    begin
      //FWorkspace.Repaint;
      PvVer.SaveToFile(ChangeFileExt(SaveDlg.FileName, '.fp3'));
    end;
  finally
    SaveDlg.Free;
  end;
end;

procedure TFmMeuFrx.BtAbreClick(Sender: TObject);
begin
  LoadFromFile;
end;

procedure TFmMeuFrx.LoadFromFile;
var
  OpenDlg: TOpenDialog;
begin
  //if FRunning then Exit;
  OpenDlg := TOpenDialog.Create(nil);
  try
    OpenDlg.Options := [ofHideReadOnly];
    OpenDlg.Filter := 'Arquivos Fast Report preparados (*.fp3)|*.fp3';
    if OpenDlg.Execute then
    //begin
      //FWorkspace.Repaint;
      PvVer.LoadFromFile(OpenDlg.FileName);
    //end;
  finally
    OpenDlg.Free;
  end;
end;

procedure TFmMeuFrx.Localizar1Click(Sender: TObject);
begin
  PvVer.Find;
  //
  MyObjects.MostraPopUpDeBotao(PMPesquisa, BtPesquisa);
end;

procedure TFmMeuFrx.Localizarprximo1Click(Sender: TObject);
begin
  PvVer.FindNext;
  //
  MyObjects.MostraPopUpDeBotao(PMPesquisa, BtPesquisa);
end;

procedure TFmMeuFrx.BtZoomMaisClick(Sender: TObject);
begin
  PvVer.Zoom := PvVer.Zoom + 0.25;
end;

procedure TFmMeuFrx.BtZoomMenosClick(Sender: TObject);
begin
  PvVer.Zoom := PvVer.Zoom - 0.25;
end;

procedure TFmMeuFrx.FormCreate(Sender: TObject);
begin
  WEB1.Visible := False;
  //
  {$IfDef UsaModuloFTP}
  WEB1.Visible := True;
  {$EndIf}
  {$IfDef TEM_UH and TEM_DBWEB}
  WEB1.Visible := True;
  {$EndIf}
  {$IfNDef NO_USE_EMAILDMK}
  EMAIL1.Visible := True;
  {$Else}
  EMAIL1.Visible := False;
  {$EndIf}
  //
  FMeuRelatorio := '';
  FMeuRelShowed := False;
  //
  CbZoom.Items.Clear;
  CbZoom.Items.Add('25%');
  CbZoom.Items.Add('50%');
  CbZoom.Items.Add('75%');
  CbZoom.Items.Add('100%');
  CbZoom.Items.Add('150%');
  CbZoom.Items.Add('200%');
  CbZoom.Items.Add('1 P�g. largura');
  CbZoom.Items.Add('1 P�g. inteira');
  FFullScreen := False;
  FBtnState := 0;
  //
  MyObjects.frxConfiguraPDF(frxPDFExport1);
end;

procedure TFmMeuFrx.SwitchToFullScreen;
begin
  if not FFullScreen then
  begin
    StatusBar.Visible  := False;
    PnControle.Visible := False;
    FOldBS             := BorderStyle;
    FOldState          := WindowState;
    BorderStyle        := bsNone;
    WindowState        := wsMaximized;
    FFullScreen        := True;
  end else begin
    WindowState        := FOldState;
    BorderStyle        := FOldBS;
    FFullScreen        := False;
    StatusBar.Visible  := True;
    PnControle.Visible := True;
  end;
end;

procedure TFmMeuFrx.BtTelaCheiaClick(Sender: TObject);
begin
  SwitchToFullScreen;
end;

procedure TFmMeuFrx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    BtSaidaClick(Self)
  else
  if Key = VK_F11 then
    SwitchToFullScreen;
  if Key = VK_PRIOR then
  begin
    if ssShift in Shift then
      PvVer.First
    else
      PvVer.Prior;
  end else
  if Key = VK_NEXT then
      PvVer.Next
  else
  if Key = VK_PRIOR then
    PvVer.Prior
  else
  if Key = VK_HOME then
  begin
    if ssCtrl in Shift then
      PvVer.First;
  end else
  if Key = VK_END then
  begin
    if ssCtrl in Shift then
      PvVer.Last;
  end else
  if Key = 070 then //letra F
  begin
    if ssCtrl in Shift then
    begin
      PvVer.Find;
      //
      MyObjects.MostraPopUpDeBotao(PMPesquisa, BtPesquisa);
    end;
  end;
  //if Key = VK_F1 then
    //frxResources.Help(Self);
end;

procedure TFmMeuFrx.UpdateZoom;
begin
  CbZoom.Text := IntToStr(Round(PvVer.Zoom * 100)) + '%';
end;

procedure TFmMeuFrx.VerPageChange(PageNo: Integer);
var
  FirstPass: Boolean;
begin
  FirstPass := False;
  if PvVer.PreviewPages <> nil then
    FirstPass := not PvVer.PreviewPages.Engine.FinalPass;

  if FirstPass then
    StatusBar.Panels[0].Text := 'Primeiro passe ' +
      IntToStr(PvVer.PageCount)
  else
    StatusBar.Panels[0].Text := Format('P�gina %d de %d',
      [PageNo, PvVer.PageCount]);
  EdPageE.ValueVariant := PageNo;
end;

procedure TFmMeuFrx.WEB1Click(Sender: TObject);
begin
  {$IfDef UsaModuloFTP}
  UFTP.PublicaRelatorioFRXnaWeb(CO_DMKID_APP, DModG.QrMasterHabilModulos.Value,
  Dmod.MyDBn, DmodG.QrWebParams, Dmod.QrUpdN, Dmod.QrAuxN, frxPDFExport1, PvVer);
  {$EndIf}
  {$IfDef TEM_UH and TEM_DBWEB}
  UFTP.PublicaRelatorioFRXnaWeb(CO_DMKID_APP, DModG.QrMasterHabilModulos.Value,
  Dmod.MyDBn, DmodG.QrWebParams, Dmod.QrUpdN, Dmod.QrAuxN, frxPDFExport1, PvVer);
  {$EndIf}
end;

procedure TFmMeuFrx.WMUpdateZoom(var Message: TMessage);
begin
  UpdateZoom;
end;

procedure TFmMeuFrx.SpeedButton1Click(Sender: TObject);
begin
  PvVer.First;
end;

procedure TFmMeuFrx.SpeedButton2Click(Sender: TObject);
begin
  PvVer.Prior;
end;

procedure TFmMeuFrx.SpeedButton3Click(Sender: TObject);
begin
  PvVer.Next;
end;

procedure TFmMeuFrx.SpeedButton4Click(Sender: TObject);
begin
  PvVer.Last;
end;

procedure TFmMeuFrx.SVG1Click(Sender: TObject);
begin
  PvVer.Export(frxSVGExport1);
  //InfoNoExport();
end;

procedure TFmMeuFrx.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    if ActiveControl = CbZoom then
      CbZoomClick(nil);
    if ActiveControl = EdPageE then
      EdPageEClick(nil);
  end;
end;

procedure TFmMeuFrx.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
{$IfDef VER320}
  //PvVer.MouseWheelScroll(WheelDelta);
  PvVer.MouseWheelScroll(WheelDelta, Shift, MousePos);
{$Else}
  PvVer.MouseWheelScroll(WheelDelta, Shift, MousePos);
{$EndIf}
end;

procedure TFmMeuFrx.EdPageEClick(Sender: TObject);
begin
  //
end;

procedure TFmMeuFrx.EdPageEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  PageE: Integer;
begin
  if Key = VK_RETURN then
  begin
    Key := 0;
    PageE := EdPageE.ValueVariant;
    PvVer.PageNo := PageE;
  end;
end;

procedure TFmMeuFrx.CbZoomClick(Sender: TObject);
var
  s: String;
begin
  PvVer.SetFocus;

  if CbZoom.ItemIndex = 6 then
    PvVer.ZoomMode := zmPageWidth
  else if CbZoom.ItemIndex = 7 then
    PvVer.ZoomMode := zmWholePage
  else
  begin
    s := CbZoom.Text;

    if Pos('%', s) <> 0 then
      s[Pos('%', s)] := ' ';
    while Pos(' ', s) <> 0 do
      Delete(s, Pos(' ', s), 1);

    if s <> '' then
      PvVer.Zoom := Geral.DMV(s) / 100;
  end;

  PostMessage(Handle, WM_UPDATEzoom, 0, 0);
end;

procedure TFmMeuFrx.FormDblClick(Sender: TObject);
begin
  if FFullScreen then
    SwitchToFullScreen;
end;

procedure TFmMeuFrx.BtOutlineClick(Sender: TObject);
begin
  PvVer.OutlineVisible := Geral.IntToBool_0(ChangeBtnState(1));
end;

procedure TFmMeuFrx.BtPesquisaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesquisa, BtPesquisa);
end;

procedure TFmMeuFrx.BtThumbnailClick(Sender: TObject);
begin
  PvVer.ThumbnailVisible := Geral.IntToBool_0(ChangeBtnState(2));
end;

procedure TFmMeuFrx.BtSetupClick(Sender: TObject);
begin
  PvVer.PageSetupDlg;
end;

procedure TFmMeuFrx.BtEditClick(Sender: TObject);
begin
  PvVer.Edit;
end;

function TFmMeuFrx.ChangeBtnState(Clicked: Integer): Integer;
begin
  if Clicked = FBtnState then FBtnState := 0
  else FBtnState := Clicked;
  Result := FBtnState;
end;

procedure TFmMeuFrx.FormShow(Sender: TObject);
begin
  if (FMeuRelatorio <> '') and (FMeuRelShowed = False) then
  begin
    FMeuRelShowed := True;
    PvVer.LoadFromFile(FMeuRelatorio);
  end;
  //
  VAR_TEMPO_SHOW_FRX := Now() - VAR_TEMPO_SHOW_FRX;
  //
  VerPageChange(1);
end;

procedure TFmMeuFrx.BtExportaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExporta, BtExporta);
end;

procedure TFmMeuFrx.BtGerenciaClick(Sender: TObject);
begin
{$IFNDEF NO_USE_FR3CUSTOM}
  MyObjects.FormCria(TFmCustomFR3, FmCustomFR3);
  FmCustomFR3.ShowModal;
  FmCustomFR3.Destroy;
{$ENDIF}
end;

procedure TFmMeuFrx.PDF1Click(Sender: TObject);
var
  Dir: String;
begin
(*
  // Evitar erro de acesso a pasta negada pelo SO
  //if DModG.QrCtrlGeralMyPathsFrx.Value > 0 then
  if VAR_CtrlGeralMyPathsFrx > 0 then
  begin
    Dir := CO_DIR_RAIZ_DMK + '\Email\' + FormatDateTime('YYYMMDD', Date);
    try
      ForceDirectories(Dir);
      frxPDFExport1.FileName   := Dir + '\' + FNomeArq;
    except
      Geral.MB_Erro('N�o foi poss�vel criar e/ou acessar o diret�rio sugerido pela janela de origem!');
      PvVer.Export(frxPDFExport1);
    end;
  end;
*)
  ObtemCaminho(frxPDFExport1);
  //
  //PvVer.Export(frxPDFExport1);
end;

procedure TFmMeuFrx.PPTX1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxPPTXExport1);
end;

procedure TFmMeuFrx.PvVerPageChanged(Sender: TfrxPreview; PageNo: Integer);
begin
  VerPageChange(PageNo);
end;

procedure TFmMeuFrx.HTML1Click(Sender: TObject);
begin
  PvVer.Export(frxHTMLExport1);
end;

procedure TFmMeuFrx.HTML41Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxHTML4DivExport1);
end;

procedure TFmMeuFrx.HTML51Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxHTML5DivExport1);
end;

procedure TFmMeuFrx.InfoNoExport();
begin
  Geral.MB_Info('Exporta��o n�o realizada! Solicite implementa��o � Dermatek (frx Export...)');
end;

procedure TFmMeuFrx.XLS1Click(Sender: TObject);
begin
  //InfoNoExport();
  ObtemCaminho(frxXLSExport1);
end;

procedure TFmMeuFrx.XLSX1Click(Sender: TObject);
begin
  //InfoNoExport();
  ObtemCaminho(frxXLSXExport1);
end;

procedure TFmMeuFrx.XLM1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxXMLExport1);
end;

procedure TFmMeuFrx.RTF1Click(Sender: TObject);
begin
  PvVer.Export(frxRTFExport1);
end;

procedure TFmMeuFrx.BtCustomClick(Sender: TObject);
{$IFNDEF NO_USE_FR3CUSTOM}
var
  Str: String;
  MemoryStream: TMemoryStream;
  Titulo: String;
{$ENDIF}
begin
{$IFNDEF NO_USE_FR3CUSTOM}
  Str := '';
  Titulo := PvVer.Report.ReportOptions.Name;
  MyObjects.FormCria(TFmCustomFR3Imp, FmCustomFR3Imp);
  with FmCustomFR3Imp do
  begin
    FApenasDefine := True;
    FCustomFR3 := '';
    ShowModal;
    //
    Str := FCustomFR3;
    Destroy;
  end;
  if Str <> '' then
  begin
    MemoryStream := TMemoryStream.Create;
    try
      MemoryStream.WriteBuffer(Pointer(Str)^, Length(Str));
      MemoryStream.Position := 0;
      //
      PvVer.Report.LoadFromStream(MemoryStream);
      PvVer.Report.PrepareReport();
      //
    finally
      MemoryStream.Free;
    end;
  end;
{$ENDIF}
end;

procedure TFmMeuFrx.BtDesignClick(Sender: TObject);
begin
  PvVer.Report.DesignReport;
end;

procedure TFmMeuFrx.BIFF1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxBIFFExport1);
end;

procedure TFmMeuFrx.BMP1Click(Sender: TObject);
begin
  PvVer.Export(frxBMPExport1);
end;

procedure TFmMeuFrx.JPG1Click(Sender: TObject);
begin
  PvVer.Export(frxJPEGExport1);
end;

procedure TFmMeuFrx.TIF1Click(Sender: TObject);
begin
  PvVer.Export(frxTIFFExport1);
end;

procedure TFmMeuFrx.TXT1Click(Sender: TObject);
begin
  PvVer.Export(frxSimpleTextExport1);
end;

procedure TFmMeuFrx.CSV1Click(Sender: TObject);
begin
  PvVer.Export(frxCSVExport1);
end;

procedure TFmMeuFrx.DBF1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxDBFExport1);
end;

procedure TFmMeuFrx.DOCX1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxDOCXExport1);
end;

procedure TFmMeuFrx.EMAIL1Click(Sender: TObject);
{$IfNDef NO_USE_EMAILDMK}
var
  Nome, Destino, Arquivo: String;
  Arquivos: TStringList;
begin
  Arquivos := TStringList.Create;
  try
    Destino := CO_DIR_RAIZ_DMK + '\Email\' + Geral.FF0(VAR_USUARIO);
    //
    if not DirectoryExists(Destino) then
    begin
      if not ForceDirectories(Destino) then
      begin
        Geral.MB_Erro('Falha ao criar diret�rio tempor�rio no destino: ' + Destino + '!');
        Exit;
      end;
    end;
    Arquivo := Destino + '\' + Geral.FDT(Now(), 26) + '.pdf';
    //
    Arquivos.Add(Arquivo);
    //
    frxPDFExport1.FileName   := Arquivo;
    frxPDFExport1.ShowDialog := True;
    PvVer.Export(frxPDFExport1);
    //
    if FileExists(Arquivo) then
    begin
      Nome := PvVer.Report.ReportOptions.Name;
      //
      UMailEnv.Monta_e_Envia_Mail(['0', '', '', 0], meAvul, [Arquivo], [], True);
    end;
  finally
    Arquivos.Free;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappEmail);
{$EndIf}
end;

procedure TFmMeuFrx.Emeio1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxMailExport1);
end;

function TFmMeuFrx.ObtemCaminho(Sender: TfrxCustomExportFilter): Boolean;
var
  Dir: String;
begin
  Result := False;
  // Evitar erro de acesso a pasta negada pelo SO
  //if DModG.QrCtrlGeralMyPathsFrx.Value > 0 then
  if VAR_CtrlGeralMyPathsFrx > 0 then
  begin
    Dir := CO_DIR_RAIZ_DMK + '\Email\' + FormatDateTime('YYYMMDD', Date);
    try
      ForceDirectories(Dir);
      //frxPDFExport1.FileName   := Dir + '\' + FNomeArq;
      Dir := Dir + '\' + FNomeArq;
      SetPropValue(Sender, 'FileName', Dir);
      PvVer.Export(Sender);
      Result := True;
    except
      Geral.MB_Erro('N�o foi poss�vel criar e/ou acessar o diret�rio sugerido pela janela de origem!');
      PvVer.Export(Sender);
    end;
  end else
    PvVer.Export(Sender);
end;

procedure TFmMeuFrx.ODS1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxODSExport1);
end;

procedure TFmMeuFrx.ODT1Click(Sender: TObject);
begin
  //InfoNoExport();
  PvVer.Export(frxODTExport1);
end;

end.

