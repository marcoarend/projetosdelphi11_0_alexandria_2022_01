unit Produsys2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkDBGrid, dmkDBGridDAC,
  UnDmkProcFunc, Variants, dmkImage, UnDmkEnums;

type
  TFmProdusys2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label2: TLabel;
    TPVencto: TdmkEditDateTimePicker;
    Label3: TLabel;
    EdMes: TdmkEdit;
    RGEstagio: TRadioGroup;
    QrLocPrev: TmySQLQuery;
    QrLocPrevCodigo: TIntegerField;
    QrLocPrevPeriodo: TIntegerField;
    QrLocPrevCond: TIntegerField;
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolLeiBoleto: TFloatField;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolArrBoleto: TFloatField;
    QrPPI: TmySQLQuery;
    QrPPIPROTOCOLO: TIntegerField;
    QrPPIDataE: TDateField;
    QrPPIDataD: TDateField;
    QrPPICancelado: TIntegerField;
    QrPPIMotivo: TIntegerField;
    QrPPIID_Cod1: TIntegerField;
    QrPPIID_Cod2: TIntegerField;
    QrPPIID_Cod3: TIntegerField;
    QrPPIID_Cod4: TIntegerField;
    QrPPITAREFA: TWideStringField;
    QrPPIDELIVER: TWideStringField;
    QrPPILOTE: TIntegerField;
    QrPPICliInt: TIntegerField;
    QrPPICliente: TIntegerField;
    QrPPIPeriodo: TIntegerField;
    QrPPIDATAE_TXT: TWideStringField;
    QrPPIDATAD_TXT: TWideStringField;
    QrPPIDef_Sender: TIntegerField;
    QrPPIDocum: TFloatField;
    QrUsers: TmySQLQuery;
    QrUsersCodigoEnt: TIntegerField;
    QrUsersCodigoEsp: TIntegerField;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    QrBoletos: TmySQLQuery;
    QrBoletosApto: TIntegerField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosSUB_ARR: TFloatField;
    QrBoletosSUB_LEI: TFloatField;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosPropriet: TIntegerField;
    QrBoletosNOMEPROPRIET: TWideStringField;
    QrBoletosBOLAPTO: TWideStringField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosUSERNAME: TWideStringField;
    QrBoletosPASSWORD: TWideStringField;
    QrBoletosPWD_WEB: TWideStringField;
    QrBoletosPROTOCOLO: TIntegerField;
    QrBoletosAndar: TIntegerField;
    QrBoletosBoleto: TFloatField;
    QrBoletosBLOQUETO: TFloatField;
    QrBoletosKGT: TLargeintField;
    QrBoletosLOTE_PROTOCO: TIntegerField;
    QrBoletosOrdem: TIntegerField;
    QrBoletosFracaoIdeal: TFloatField;
    DsBoletos: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    MeGerado: TMemo;
    QrBoletosAtivo: TSmallintField;
    DBGradeS: TdmkDBGridDAC;
    Panel4: TPanel;
    BtReabre: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrVenctos: TmySQLQuery;
    QrVenctosVencto: TDateField;
    PnGrids: TPanel;
    DBGridC: TDBGrid;
    QrBoletosC: TmySQLQuery;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsTEXTO_IMP: TWideStringField;
    QrBoletosItsMedAnt: TFloatField;
    QrBoletosItsMedAtu: TFloatField;
    QrBoletosItsConsumo: TFloatField;
    QrBoletosItsCasas: TLargeintField;
    QrBoletosItsUnidLei: TWideStringField;
    QrBoletosItsUnidImp: TWideStringField;
    QrBoletosItsUnidFat: TFloatField;
    QrBoletosItsTipo: TLargeintField;
    QrBoletosItsVENCTO_TXT: TWideStringField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    QrBoletosItsGeraTyp: TLargeintField;
    QrBoletosItsGeraFat: TFloatField;
    QrBoletosItsCasRat: TLargeintField;
    QrBoletosItsNaoImpLei: TLargeintField;
    DsBoletosIts: TDataSource;
    Panel6: TPanel;
    Panel33: TPanel;
    DBGrid10: TDBGrid;
    Splitter1: TSplitter;
    QrBoletosItsTabela: TWideStringField;
    QrImv: TmySQLQuery;
    QrImvUnidade: TWideStringField;
    QrImvcim_IDExporta: TWideStringField;
    QrImvcnb_IDExporta: TWideStringField;
    QrBoletosItsApto: TIntegerField;
    QrImvCIM_IDEXPORTA_TXT: TWideStringField;
    QrBoletosItsPreco: TFloatField;
    QrBoletosItsExport_Tip: TLargeintField;
    QrBoletosItsCODI_CONS: TLargeintField;
    QrPrc: TmySQLQuery;
    QrPrcExport_Apl: TSmallintField;
    QrPrcExport_Med: TSmallintField;
    QrBoletosItsPRECO_LEI: TFloatField;
    BtGerar: TBitBtn;
    BtSalvar: TBitBtn;
    DBGridDR: TDBGrid;
    DBGridDI: TDBGrid;
    DBGridR: TDBGrid;
    DsBoletosC: TDataSource;
    DsBoletosDR: TDataSource;
    QrBoletosDR: TmySQLQuery;
    DsBoletosDI: TDataSource;
    QrBoletosDI: TmySQLQuery;
    DsBoletosR: TDataSource;
    QrBoletosR: TmySQLQuery;
    QrBoletosCLinha: TIntegerField;
    QrBoletosCTipo: TWideStringField;
    QrBoletosCCNPJ: TWideStringField;
    QrBoletosCAno: TIntegerField;
    QrBoletosCMes: TIntegerField;
    QrBoletosCVencto: TDateField;
    QrBoletosCAtivo: TSmallintField;
    QrBoletosDRLinha: TIntegerField;
    QrBoletosDRTipo: TWideStringField;
    QrBoletosDRCompl: TWideStringField;
    QrBoletosDRBloco: TWideStringField;
    QrBoletosDRValor: TFloatField;
    QrBoletosDRAtivo: TSmallintField;
    QrBoletosDILinha: TIntegerField;
    QrBoletosDITipo: TWideStringField;
    QrBoletosDICodTaxa: TIntegerField;
    QrBoletosDICompl: TWideStringField;
    QrBoletosDIBloco: TWideStringField;
    QrBoletosDIUnidade: TWideStringField;
    QrBoletosDIQtde: TFloatField;
    QrBoletosDILeituAnt: TFloatField;
    QrBoletosDILeituAtu: TFloatField;
    QrBoletosDIPreco: TFloatField;
    QrBoletosDIValor: TFloatField;
    QrBoletosDIAtivo: TSmallintField;
    QrBoletosRLinha: TIntegerField;
    QrBoletosRTipo: TWideStringField;
    QrBoletosRRegistros: TIntegerField;
    QrBoletosRAtivo: TSmallintField;
    QrBoletosDITMU: TSmallintField;
    QrDR: TmySQLQuery;
    QrAptos: TmySQLQuery;
    QrAptosAptos: TLargeintField;
    QrDRConta: TIntegerField;
    QrDRValor: TFloatField;
    QrBoletosDRCodTaxa: TIntegerField;
    QrBoletosItsGenero: TIntegerField;
    QrPrcExport_Tip: TSmallintField;
    QrLocDR: TmySQLQuery;
    QrSumDI: TmySQLQuery;
    QrSumDR: TmySQLQuery;
    QrSumDRValor: TFloatField;
    QrSumDIValor: TFloatField;
    QrDRITENS: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1A: TLabel;
    LaAviso1B: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    Panel7: TPanel;
    StatusBar: TStatusBar;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    LaAviso2B: TLabel;
    LaAviso2A: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtGerarClick(Sender: TObject);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
    procedure QrBoletosAfterOpen(DataSet: TDataSet);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdMesChange(Sender: TObject);
    procedure TPVenctoChange(Sender: TObject);
    procedure TPVenctoClick(Sender: TObject);
    procedure RGEstagioClick(Sender: TObject);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure QrBoletosItsCalcFields(DataSet: TDataSet);
    procedure QrImvCalcFields(DataSet: TDataSet);
    procedure BtSalvarClick(Sender: TObject);
    procedure MeGeradoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MeGeradoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MeGeradoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FCond, FCodEnti, FPeriodo, FPrev, FLancto, FAptos: Integer;
    FVencto, FTabPrvA, FTabAriA, FTabCnsA: String;
    FTotal: Double;
    //
    FBoletosC, FBoletosDR, FBoletosDI, FBoletosR, FBoletosSR: String;
    //
    FcReg: String;
    F_Fmt: Char;
    F_Casas: Byte;
    FtxtLin, FxLinha: String;
    F_Tam, F_PosI, F_PosF,
    FMeGerado_SelCol, FMeGerado_SelLin,
    //FMeImportado_SelCol, FMeImportado_SelLin,
    FQtdC, FQtdDR, FQtdDI, FQtdR,
    FnLinha: Integer;
    F_Obrigatorio: Boolean;
    procedure ReopenBoletos(BOLAPTO: String);
    procedure AtivaTodos(Ativo: Integer);
    procedure FechaPesquisa();
    //
    function AddItemX(const Registro: String; const Campo: Integer; SubTipo: Char; Valor: String;
             IgnoraObrigatorio: Boolean = False; AjustaAMenor: TAjustaAMenor = aamNone): Boolean;
    function ObtemDescricaoDeCampo(nReg: String; PosI: Integer; SubTipo: Char): String;
    function CoordenadasCampo(const Registro: String; const Campo: Integer; const SubTipo:
             Char; var Tam, Ini, Fim: Integer; var Fmt: Char; var Deci:
             Byte; var Obrigatorio: Boolean): Boolean;
    function LimpaValor(Valor: String): String;
    function IncluiRegistroC(): Boolean;
    function IncluiRegistroDR(): Boolean;
    function IncluiRegistroDI_Arrecad(): Boolean;
    function IncluiRegistroDI_Leitura(): Boolean;
    function IncluiRegistroR(): Boolean;
    function AdicionaLinhaAoMeGerado(const Tipo, Texto: String;
             const Tamanho: Integer; var Contador: Integer): Boolean;
    procedure ReopenBoletosIts();
    procedure InfoPosMeGerado();
    function CodigoTaxa(const Conta: Integer; var Cod: String): Boolean;
  public
    { Public declarations }
  end;

  var
  FmProdusys2: TFmProdusys2;

implementation

uses UnMyObjects, ModuleBloq, ModuleGeral, UnInternalConsts, Module, MyDBCheck,
Produsys1Vencto, UMySQLModule, UCreate, ModuleFin;

{$R *.DFM}

function TFmProdusys2.AddItemX(const Registro: String; const Campo: Integer; SubTipo: Char;
  Valor: String; IgnoraObrigatorio: Boolean = False; AjustaAMenor: TAjustaAMenor = aaMNone): Boolean;
  function Sai(): Boolean;
  var
    Txt: String;
    I: Integer;
  begin
    Txt := '';
    for I := 1 to Length(FTxtLin) do
      Txt := Txt + MLAGeral.CharToLetraMaiuscula(Ord(FTxtLin[I]));
    //
    MeGerado.Lines.Add(Txt);
    Result := True;
  end;

  function Mensagem(): String;
  begin
    Result := sLineBreak + 'Function: "TFmSintegra_Arq.AddItemX()"' + sLineBreak +
    'Linha: ' + FxLinha + sLineBreak +
    'Registro: "' + FcReg + '"' + sLineBreak +
    'Posi��o inicial: ' + Geral.FF0(F_PosI) + sLineBreak +
    'Posi��o Final: ' + Geral.FF0(F_PosF) + sLineBreak +
    'Tamanho campo: ' + Geral.FF0(F_Tam) + sLineBreak +
    'Texto campo: ' + Valor + sLineBreak + sLineBreak +
    'Campo: ' + ObtemDescricaoDeCampo(FcReg, F_PosI, SubTipo);
  end;
var
  xVal: String;
  tI, tT: Integer;
begin
  Result  := False;
  CoordenadasCampo(Registro, Campo, SubTipo, F_Tam, F_PosI, F_PosF, F_Fmt,
    F_Casas, F_Obrigatorio);
  if F_Obrigatorio and (IgnoraObrigatorio = False) and (LimpaValor(Valor) = '') then
  begin
    Geral.MB_Aviso('Valor � obrigat�rio:' + Mensagem);
    Sai();
    Exit;
  end;
  //FxReg   := Copy(FtxtLin, 1, 2);
  FnLinha := MeGerado.Lines.Count + 1;
  FxLinha := FormatFloat('000', FnLinha);
  //
  tI := Length(FtxtLin);
  if tI <> F_PosI - 1 then
  begin
    Geral.MB_Aviso('Tamanho inicial n�o permite posi��o inicial:' + Mensagem);
    Sai();
    Exit;
  end;
  // verifica se o tamanho confere com a posi��o final - inicial
  if F_Tam <> F_PosF - F_PosI + 1 then
  begin
    Geral.MB_Aviso('Tamanho de campo n�o confere com posi��o inicial e final:' + Mensagem);
    Sai();
    Exit;
  end;
  // verifica se o tamanho do texto a adicionar � igual ou inferior ao m�ximo
  xVal := Valor;
  tT := Length(xVal);
  // Ajusta tamanho do campo quando maior que o m�ximo
  if (AjustaAMenor <> aamNone) and (tT > F_Tam) then
  begin
    case AjustaAMenor of
      aamTxt:
      begin
        xVal := Copy(xVal, 1, F_Tam);
        tT := Length(xVal);
      end;
      aamNum:
      begin
        while tT > F_Tam do
        begin
          xVal := Copy(xVal, 2);
          tT := Length(xVal);
        end;
      end;
    end;
  end;
  if F_Tam < tT then
  begin
    Geral.MB_Aviso('Tamanho do valor maior que o permitido para o campo:' + Mensagem);
    Sai();
    Exit;
  end;
  // Ajusta tamanho do campo quando menor que o m�ximo
  if F_Fmt = 'X' then
  begin
    // Texto. Preenchimento com espa�os em branco ap�s texto
    while tT < F_Tam do
    begin
      xVal := xVal + ' ';
      tT := Length(xVal);
    end;
  end else
  if F_Fmt = 'N' then
  begin
    // N�meros. Preenchimento com zeros antes do n�mero
    while tT < F_Tam do
    begin
      xVal := '0' + xVal;
      tT := Length(xVal);
    end;
  end else
  if F_Fmt = 'D' then
  begin
    // Data. Campo deve ter tamanho exato.
    if F_Tam <> tT then
    begin
      Geral.MB_Aviso('Tamanho fixo de data diferente do permitido para o campo:' + Mensagem);
      Sai();
      Exit;
    end;
  end
  else
  begin
    Geral.MB_Aviso('Filler n�o implementado: "' + F_Fmt + '"' + Mensagem);
    Exit;
    Sai();
  end;
  // Adiciona campo ao registro
  FtxtLin := FtxtLin + xVal;
  if F_PosF <> Length(FtxtLin) then
  begin
    Geral.MB_Aviso('Tamanho da Linha maior que o permitido ap�s adicionar o campo:' + Mensagem);
    Exit;
    Sai();
  end;
  //
  Result := True;
  //
end;

function TFmProdusys2.AdicionaLinhaAoMeGerado(const Tipo, Texto: String;
  const Tamanho: Integer; var Contador: Integer): Boolean;
var
  Txt: String;
  I, T: Integer;
  //Dta: TDateTime;
  Ano, Mes(*, Dia, Linha*): Integer;
  // C
  CNPJ, Periodo, Vencto: String;
  //AnoMes: Integer;
  // DR
  CodTaxa, Compl, Bloco: String;
  Valor: Double;
  // DI
  Unidade: String;
  TMU: Integer;
  Qtde, LeituAnt, LeituAtu, Preco: Double;
  // R
  Registros: Integer;
begin
  Result := False;
  //
  T := Length(Texto);
  if T <> Tamanho then
  begin
    Geral.MB_Aviso('Tamanho inv�lido para a linha ' + FxLinha + '.' + sLineBreak
      + 'Tamanho da linha: ' + Geral.FF0(T) + sLineBreak +
      'Tamanho correto: ' + Geral.FF0(Tamanho));
    Exit;
  end;
  //
  Txt := '';
  for I := 1 to Length(Texto) do
    Txt := Txt + MLAGeral.CharToLetraMaiuscula(Ord(Texto[I]));
  //
  T := Length(Txt);
  if T <> Tamanho then
  begin
    Geral.MB_Aviso('Tamanho inv�lido para a linha ' + FxLinha +
      ' ap�s transformar texto em mai�sculas. Ser� utilizado o texto original!'
      + sLineBreak + 'Tamanho da linha: ' + Geral.FF0(T) + sLineBreak +
      'Tamanho correto: ' + Geral.FF0(Tamanho));
    Txt := Texto;
  end;
  //
  MeGerado.Lines.Add(Txt);
  Contador := Contador + 1;
  Result := True;
  //
  // Tabela
  //TIPO      := Copy(Txt, 001, 001);
  if TIPO = 'C' then
  begin
    CNPJ      := Copy(Txt, 002, 014);
    Periodo   := Copy(Txt, 016, 006);
    Vencto    := Copy(Txt, 022, 008);
    // Mes e ano de competencia
    Ano  := Geral.IMV(Copy(Periodo, 3, 4));
    Mes  := Geral.IMV(Copy(Periodo, 1, 2));
    // Vencimento
    Vencto := Geral.FDT(Geral.ValidaDataBR(Vencto, False, False), 1);
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FBoletosC, False, [
    'Linha', 'TIPO', 'CNPJ',
    'Ano', 'Mes', 'Vencto'], [
    ], [
    FnLinha, TIPO, CNPJ,
    Ano, Mes, Vencto], [
    ], False) then ;
  end else
{
Detalhe de Taxas para     Rateio
Fld  Posi��o  Tamanho  Tipo      Descri��o
[01] 1        2        Fixo      Preencher com as letras DR
[02] 3        3        Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6        15       Texto     Complemento
[04] 16(21)   6        Texto     Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
[05] 27       9        Num�rico  Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500

 Exemplo: DR001TESTE                000015000
}
  if TIPO = 'DR' then
  begin
    CodTaxa := Copy(Txt, 003, 003);
    Compl   := Copy(Txt, 006, 015);
    Bloco   := Copy(Txt, 021, 006);
    Valor   := Geral.IMV(Copy(Txt, 027, 009)) / 100;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FBoletosDR, False, [
    'Linha', 'Tipo', 'CodTaxa',
    'Compl', 'Bloco', 'Valor'], [
    ], [
    FnLinha, TIPO, CodTaxa,
    Compl, Bloco, Valor], [
    ], False);
  end else
{
Detalhe de Taxas Individuais
Fld  Posi��o Tamanho Tipo      Descri��o
[01] 1       2       Fixo      Preencher com as letras DI
[02] 3       3       Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6       15      Texto     Complemento
[04] 16(21)  6       Texto     Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
[05] 27      4       Texto     Unidade (ver tabela 1)
[06] 31      1       Num�rico  Tipo de Medida utilizada (ver tabela 2)
[07] 32      7       Num�rico  Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s e �gua.
[08] 39      7       Num�rico  Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s.
[09] 46      7       Num�rico  Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas e G�s.
[10] 53      9       Num�rico  Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
[11] 62      9       Num�rico  Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500

Exemplo: DI006               A     04  1000011900001230000400000002000000000238
}
  if TIPO = 'DI' then
  begin
    CodTaxa  := Copy(Txt, 003, 003);
    Compl    := Copy(Txt, 006, 015);
    Bloco    := Copy(Txt, 021, 006);
    Unidade  := Copy(Txt, 027, 004);
    TMU      := Geral.IMV(Copy(Txt, 031, 001));
    Qtde     := Geral.IMV(Copy(Txt, 032, 007)) / 1000;
    LeituAnt := Geral.IMV(Copy(Txt, 039, 007)) / 1000;
    LeituAtu := Geral.IMV(Copy(Txt, 046, 007)) / 1000;
    Preco    := Geral.IMV(Copy(Txt, 053, 009)) / 100;
    Valor    := Geral.IMV(Copy(Txt, 062, 009)) / 100;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FBoletosDI, False, [
    'Linha', 'Tipo', 'CodTaxa',
    'Compl', 'Bloco', 'Unidade',
    'TMU', 'Qtde', 'LeituAnt',
    'LeituAtu', 'Preco', 'Valor'], [
    ], [
    FnLinha, TIPO, CodTaxa,
    Compl, Bloco, Unidade,
    TMU, Qtde, LeituAnt,
    LeituAtu, Preco, Valor], [
    ], False);
  end else
{
Rodap�
Posi��o Tamanho Tipo      Descri��o
1       1       Fixo      Preencher com a letra R
2       6       Num�rico  Total de linhas do arquivo formatado com zeros a esquerda, exemplo 000099

Exemplo: R000099
}
  if TIPO = 'R' then
  begin
    Registros := Geral.IMV(Copy(Txt, 002, 006));
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FBoletosR, False, [
    'Linha', 'Tipo', 'Registros'], [
    ], [
    FnLinha, TIPO, Registros], [
    ], False);
  end;
end;

procedure TFmProdusys2.AtivaTodos(Ativo: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE _boletos_');
  DModG.QrUpdPID1.SQL.Add('SET Ativo=' + dmkPF.FFP(Ativo, 0));
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenBoletos(QrBoletosBOLAPTO.Value);
end;

procedure TFmProdusys2.BtNenhumClick(Sender: TObject);
begin
  AtivaTodos(0);
end;

procedure TFmProdusys2.BtGerarClick(Sender: TObject);
var
  Totais: Double;
  A, B: Integer;
begin
 { TODO -oEuroAdm -cCobran�a : Refazer exporta��o de cobran�a }
  try
    BtSalvar.Enabled := False;
    FLancto := 0;
    FTotal  := 0;
    QrBoletos.DisableControls;
    QrBoletosIts.DisableControls;
    MeGerado.Lines.Clear;
    FQtdC := 0 ; FQtdDR := 0; FQtdDI := 0; FQtdR := 0;
    FBoletosC  := UCriar.RecriaTempTableNovo(ntrttBoletosC, DmodG.QrUpdPID1, False);
    FBoletosDR := UCriar.RecriaTempTableNovo(ntrttBoletosDR, DmodG.QrUpdPID1, False);
    FBoletosDI := UCriar.RecriaTempTableNovo(ntrttBoletosDI, DmodG.QrUpdPID1, False);
    FBoletosR  := UCriar.RecriaTempTableNovo(ntrttBoletosR, DmodG.QrUpdPID1, False);
    FBoletosSR := UCriar.RecriaTempTableNovo(ntrttBoletosSR, DmodG.QrUpdPID1, False);
    if not IncluiRegistroC() then Exit;
    if not IncluiRegistroDR() then Exit;
    QrBoletos.First;
    while not QrBoletos.Eof do
    begin
      Application.ProcessMessages;
      // Somente selecionados!
      if QrBoletosAtivo.Value = 1 then
      begin
        QrBoletosIts.First;
        while not QrBoletosIts.Eof do
        begin
          FTotal := FTotal + QrBoletosItsValor.Value;
          //
          QrImv.Close;
          QrImv.Params[0].AsInteger := QrBoletosItsApto.Value;
          QrImv.Open;
          if QrBoletosItsTabela.Value = 'A' then
          begin
            QrLocDR.Close;
            QrLocDR.SQL.Clear;
            QrLocDR.SQL.Add('SELECT * FROM ' + FBoletosSR);
            QrLocDR.SQL.Add('WHERE Itens=:P0');
            QrLocDR.SQL.Add('AND Conta=:P1');
            QrLocDR.SQL.Add('AND Valor=:P2');
            QrLocDR.Params[00].AsInteger := FAptos;
            QrLocDR.Params[01].AsInteger := QrBoletosItsGenero.Value;
            QrLocDR.Params[02].AsFloat   := QrBoletosItsVALOR.Value;
            QrLocDR.Open;
            if QrLocDR.RecordCount = 0 then
              if not IncluiRegistroDI_Arrecad() then Exit;
          end else
          begin
            if not IncluiRegistroDI_Leitura() then Exit;
          end;
          QrBoletosIts.Next;
        end;
      end;
      QrBoletos.Next;
    end;
    if not IncluiRegistroR() then Exit;
    //
    MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Comparando totais de valores');
    QrSumDR.Close;
    QrSumDR.Open;
    QrSumDI.Close;
    QrSumDI.Open;
    //
    Totais := (QrSumDRValor.Value * FAptos) + QrSumDIValor.Value;
    A := Trunc((FTotal + 0.005) * 100);
    B := Trunc((Totais + 0.005) * 100);
    if A <> B then
    begin
      Geral.MB_Erro(
        'ERRO! Valores totais exportados n�o conferem com os valores dos bloquetos!'
        + sLineBreak + 'Valores dos bloquetos: ' + FormatFloat('#,###,###,##0.00', FTotal) + ' (A = ' + Geral.FF0(A) + ') '
        + sLineBreak + 'Valores exportados:     ' + FormatFloat('#,###,###,##0.00', Totais)+ ' (B = ' + Geral.FF0(B) + ') ');
    end else begin
      //
      MyObjects.Informa2(LaAviso1A, LaAviso1B, False, '...');
      //PageControl1.ActivePageIndex := 1;
      BtSalvar.Enabled := MeGerado.Lines.Count > 2;
      //
      Geral.MB_Aviso(
        'Valores exportados com sucesso!'
        + sLineBreak + 'Valores dos bloquetos: ' + FormatFloat('#,###,###,##0.00', FTotal)
        + sLineBreak + 'Valores exportados:     ' + FormatFloat('#,###,###,##0.00', Totais));
    end;
  finally
    QrBoletos.EnableControls;
    QrBoletosIts.EnableControls;
    //
    if LaAviso1A.Caption <> '...' then
      MyObjects.Informa2(LaAviso1A, LaAviso1B, False, 'ABORTADO EM: ' +
        Copy(LaAviso1A.Caption, Pos('...', LaAviso1A.Caption) + 3));
    //
    QrBoletosC.Open;
    QrBoletosDR.Open;
    QrBoletosDI.Open;
    QrBoletosR.Open;
  end;
end;

procedure TFmProdusys2.BtReabreClick(Sender: TObject);
  function TipoBol(): String;
  begin
    Result := ' #ERRO# ';
    case RGEstagio.ItemIndex of
      0: Result := '=';
      1: Result := '<>';
      else Geral.MB_Erro('Est�gio de bloqueto n�o implementado!');
    end;
  end;
var
  Continua: Boolean;
begin
  //Continua := False;
  FCond := EdEmpresa.ValueVariant;
  FCodEnti := DModG.QrEmpresasCodigo.Value;
  if MyObjects.FIC(FCond = 0, EdEmpresa, 'Informe o condom�nio!') then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    FPeriodo := Geral.Periodo2000(EdMes.ValueVariant);
    FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, FCond);
    FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, FCond);
    FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, FCond);
    //
    QrLocPrev.Close;
    QrLocPrev.SQL.Clear;
    QrLocPrev.SQL.Add('SELECT Codigo, Periodo, Cond');
    QrLocPrev.SQL.Add('FROM ' + FTabPrvA);
    QrLocPrev.SQL.Add('WHERE Cond=:P0');
    QrLocPrev.SQL.Add('AND Periodo=:P1');
    QrLocPrev.SQL.Add('');
    QrLocPrev.Params[00].AsInteger := FCond;
    QrLocPrev.Params[01].AsInteger := FPeriodo;
    QrLocPrev.Open;
    //
    FPrev := QrLocPrevCodigo.Value;
    //
    QrAptos.Close;
    QrAptos.Params[0].AsInteger := FCond;
    QrAptos.Open;
    FAptos   := QrAptosAptos.Value;
    //
    if QrLocPrev.RecordCount > 0 then
    begin
      Continua := True;
    end else
    begin
      Geral.MB_Aviso(
        'N�o foi localizado nenhum movimento para o per�odo / condom�nio selecionado!');
      Screen.Cursor := crDefault;  
      Exit;
    end;
    if Continua then
    begin
      FVencto := Geral.FDT(TPVencto.Date, 1);
      DmBloq.ReopenBoletosQry(QrBoletos, QrBolArr, QrBolLei, QrPPI, QrUsers,
        FPrev, QrLocPrevCond.Value, QrLocPrevPeriodo.Value,
        TipoBol(), FTabAriA, FTabCnsA, FVencto, True);
      if QrBoletos.RecordCount = 0 then
      begin
        QrVenctos.Close;
        QrVenctos.SQL.Clear;
        QrVenctos.SQL.Add('SELECT DISTINCT ari.Vencto');
        QrVenctos.SQL.Add('FROM ' + FTabAriA + ' ari');
        QrVenctos.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
        QrVenctos.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
        QrVenctos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
        QrVenctos.SQL.Add('WHERE ari.Codigo=' + FormatFloat('0', FPrev));
        QrVenctos.SQL.Add('AND ari.Boleto ' + TipoBol() + ' 0');
        QrVenctos.SQL.Add('');
        QrVenctos.SQL.Add('UNION');
        QrVenctos.SQL.Add('');
        QrVenctos.SQL.Add('SELECT DISTINCT cni.Vencto');
        QrVenctos.SQL.Add('FROM ' + FTabCnsA + ' cni');
        QrVenctos.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
        QrVenctos.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
        QrVenctos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
        QrVenctos.SQL.Add('WHERE cni.Cond=' + FormatFloat('0', QrLocPrevCond.Value));
        QrVenctos.SQL.Add('AND cni.Periodo=' + FormatFloat('0', QrLocPrevPeriodo.Value));;
        QrVenctos.SQL.Add('AND cni.Boleto ' + TipoBol() + ' 0');
        QrVenctos.SQL.Add('');
        QrVenctos.SQL.Add('ORDER BY Vencto');
        QrVenctos.SQL.Add('');
        QrVenctos.Open;
        //
        if QrVenctos.RecordCount > 0 then
        begin
          if DBCheck.CriaFm(TFmProdusys1Vencto, FmProdusys1Vencto, afmoLiberado) then
          begin
            FmProdusys1Vencto.RGVencto.Items.Clear;
            QrVenctos.First;
            while not QrVenctos.Eof do
            begin
              FmProdusys1Vencto.RGVencto.Items.Add(
                Geral.FDT(QrVenctosVencto.Value, 2));
              QrVenctos.Next;
            end;
            FmProdusys1Vencto.ShowModal;
            if FmProdusys1Vencto.FVencto > 0 then
            begin
              TPVencto.Date := FmProdusys1Vencto.FVencto;
              FVencto := Geral.FDT(TPVencto.Date, 1);
              DmBloq.ReopenBoletosQry(QrBoletos, QrBolArr, QrBolLei, QrPPI, QrUsers,
                FPrev, QrLocPrevCond.Value, QrLocPrevPeriodo.Value,
                TipoBol(), FTabAriA, FTabCnsA, FVencto, True);
            end;
            FmProdusys1Vencto.Destroy;
          end;
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmProdusys2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdusys2.BtSalvarClick(Sender: TObject);
const
  Raiz = 'C:\Dermatek\Produsys\';
var
  Mes, Dir, Arq, Arquivo: String;
  p: Integer;
  Continua: Boolean;
begin
  p := pos('/', EdMes.Text);
  Mes := Copy(EdMes.Text, p + 1) +  '_' +
         Copy(EdMes.Text, 1, p - 1);
  Dir := Raiz + Mes + '\';
  //
  ForceDirectories(Dir);
  //
  Arq := DModG.QrEmpresasCNPJ_CPF.Value + '_' + Mes;
  Arquivo := dmkPF.CaminhoArquivo(Dir, Arq, 'txt');
  if FileExists(Arquivo) then
    Continua := Geral.MB_Pergunta('O arquivo "' + Arquivo +
    ' j� existe. Deseja sobrescrev�-lo?') = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    if MLAGeral.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, True, 1310, Null) then
    begin
      StatusBar.Panels[3].Text := Arquivo;
      //
      Geral.MB_Aviso('O arquivo foi salvo como "' + Arquivo + '". ');
    end;
  end;
end;

procedure TFmProdusys2.BtTodosClick(Sender: TObject);
begin
  AtivaTodos(1);
end;

function TFmProdusys2.CodigoTaxa(const Conta: Integer;
  var Cod: String): Boolean;
begin
  begin
    Result := False;
    //
    DModFin.ReopenContasEnt(Conta, FCodEnti);
    //
    if DModFin.QrContasEnt.RecordCount > 0 then
    begin
      Cod := Trim(DModFin.QrContasEntCodExporta.Value);
      if (Cod = Geral.SoNumero_TT(Cod)) and (Cod <> '') and (Length(Cod) <= 3) then
      begin
        while Length(Cod) < 3 do
          Cod := '0' + Cod;
        Result := True;  
      end else
      Geral.MB_Aviso('Inclus�o de registro abortado!' + sLineBreak +
        'O c�digo de exporta��o "' + DModFin.QrContasEntCodExporta.Value +
        '" para a entidade ' + FormatFloat('0', FCodEnti) + sLineBreak +
        ' no cadastro da conta n� ' + FormatFloat('0', Conta) +
        ' do plano de contas pode estar incorreto!');
    end else
    if Result = False then
      Geral.MB_Aviso('Inclus�o de registro abortado!' + sLineBreak +
        'Falta definir o c�digo de exporta��o para a entidade ' + FormatFloat('0', FCodEnti)
        + sLineBreak + 'no cadastro da conta n� ' + FormatFloat('0', Conta) +
        ' do plano de contas!');
  end;
end;

function TFmProdusys2.CoordenadasCampo(const Registro: String; const Campo: Integer;
  const SubTipo: Char; var Tam, Ini, Fim: Integer; var Fmt: Char;
  var Deci: Byte; var Obrigatorio: Boolean): Boolean;
var
  T, I, F, D, O: Integer;
  X: Char;
begin
  T(*Tam*) := 0;
  I(*Ini*) := 0;
  F(*Fim*) := 0;
  X(*Fmt*) := #0;
  O(*Obrigatorio*) := 1;
  D        := 0;
  if Registro = 'C' then
  begin
{
Cabe�alho
Posi��o	Tamanho	Tipo      	Descri��o
1	      1	      Fixo	      Preencher com a letra C
2	      14	    Num�rico    CNPJ do condom�nio sem pontos, barras ou tra�o e com zeros a esquerda
16	    6	      Num�rico	   M�s e Ano de refer�ncia no formato mmaaaa
22	    8	      Num�rico	   Data de Vencimento das Taxas no formato ddmmaaaa

 Exemplo: C0686668400019512201010012011
}
    case Campo of
      01:	begin T := 001;	I := 001;	F := 001; X := 'X'; D := 0; O := 1; end; // Tipo de registro: C - Cabe�alho
      02:	begin T := 014;	I := 002;	F := 015; X := 'X'; D := 0; O := 1; end; // CNPJ do condom�nio sem pontos, barras ou tra�o e com zeros a esquerda
      03:	begin T := 006;	I := 016;	F := 021; X := 'D'; D := 2; O := 1; end; // M�s e Ano de refer�ncia no formato mmaaaa
      04:	begin T := 008;	I := 022;	F := 029; X := 'D'; D := 0; O := 1; end; // Data de Vencimento das Taxas no formato ddmmaaaa
    end;
  end else
  if Registro = 'DR' then
  begin
{
Detalhe de Taxas para     Rateio
Fld  Posi��o  Tamanho  Tipo      Descri��o
[01] 1        2        Fixo      Preencher com as letras DR
[02] 3        3        Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6        15       Texto     Complemento
[04] 16(21)   6        Texto     Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
[05] 27       9        Num�rico  Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500

 Exemplo: DR001TESTE                000015000
}
    case Campo of
      01:	begin T := 002;	I := 001;	F := 002; X := 'X'; D := 0; O := 1; end; // Tipo de registro: DR - Detalhe Rateio
      02:	begin T := 003;	I := 003;	F := 005; X := 'N'; D := 0; O := 1; end; // C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
      03:	begin T := 015;	I := 006;	F := 020; X := 'X'; D := 0; O := 0; end; // Complemento
      04:	begin T := 006;	I := 021;	F := 026; X := 'X'; D := 0; O := 0; end; // Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
      05:	begin T := 009;	I := 027;	F := 035; X := 'N'; D := 2; O := 1; end; // Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500
    end;
  end else
  if Registro = 'DI' then
  begin
{
Detalhe de Taxas Individuais
Fld  Posi��o Tamanho Tipo      Descri��o
[01] 1       2       Fixo      Preencher com as letras DI
[02] 3       3       Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6       15      Texto     Complemento
[04] 16(21)  6       Texto     Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
[05] 27      4       Texto     Unidade (ver tabela 1)
[06] 31      1       Num�rico  Tipo de Medida utilizada (ver tabela 2)
[07] 32      7       Num�rico  Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s e �gua.
[08] 39      7       Num�rico  Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s.
[09] 46      7       Num�rico  Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas e G�s.
[10] 53      9       Num�rico  Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
[11] 62      9       Num�rico  Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500

Exemplo: DI006               A     04  1000011900001230000400000002000000000238
}
    case Campo of
      01:	begin T := 002;	I := 001;	F := 002; X := 'X'; D := 0; O := 1; end; // Tipo de registro: DI - Detalhe Individual
      02:	begin T := 003;	I := 003;	F := 005; X := 'N'; D := 0; O := 1; end; // C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
      03:	begin T := 015;	I := 006;	F := 020; X := 'X'; D := 0; O := 0; end; // Complemento
      04:	begin T := 006;	I := 021;	F := 026; X := 'X'; D := 0; O := 0; end; // Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
      05:	begin T := 004;	I := 027;	F := 030; X := 'X'; D := 0; O := 1; end; // Unidade (ver tabela 1)
      06:	begin T := 001;	I := 031;	F := 031; X := 'N'; D := 0; O := 1; end; // Tipo de Medida utilizada (ver tabela 2)
      07:	begin T := 007;	I := 032;	F := 038; X := 'N'; D := 3; O := 1; end; // Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas de G�s e �gua.
      08:	begin T := 007;	I := 039;	F := 045; X := 'N'; D := 3; O := 1; end; // Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas de G�s.
      09:	begin T := 007;	I := 046;	F := 052; X := 'N'; D := 3; O := 1; end; // Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas e G�s.
      10:	begin T := 009;	I := 053;	F := 061; X := 'N'; D := 2; O := 1; end; // Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
      11:	begin T := 009;	I := 062;	F := 070; X := 'N'; D := 2; O := 1; end; // Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
    end;
  end else
  if Registro = 'R' then
  begin
{
Rodap�
Posi��o Tamanho Tipo      Descri��o
1       1       Fixo      Preencher com a letra R
2       6       Num�rico  Total de linhas do arquivo formatado com zeros a esquerda, exemplo 000099

Exemplo: R000099
}
    case Campo of
      01:	begin T := 001;	I := 001;	F := 001; X := 'X'; D := 0; O := 1; end; // Tipo de registro: R - Rodap�
      02:	begin T := 006;	I := 002;	F := 007; X := 'N'; D := 0; O := 1; end; // Total de linhas do arquivo formatado com zeros a esquerda, exemplo 000099
    end;
  end;

  //
  Tam  := T;
  Ini  := I;
  Fim  := F;
  Fmt  := X;
  Deci := D;
  Obrigatorio := O = 1;
  Result := True;
end;

procedure TFmProdusys2.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmProdusys2.EdMesChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmProdusys2.FechaPesquisa();
begin
  QrBoletos.Close;
  BtTodos.Enabled := False;
  BtNenhum.Enabled := False;
  BtGerar.Enabled := False;
  BtSalvar.Enabled := False;
  QrBoletosC.Close;
  QrBoletosDR.Close;
  QrBoletosDI.Close;
  QrBoletosR.Close;
end;

procedure TFmProdusys2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdusys2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  PnGrids.Height := 64;
  PageControl1.ActivePageIndex := 0;
  TPVencto.Date := Date;
  QrBoletos.Close;
  QrBoletos.Database := DModG.MyPID_DB;
  QrBoletosC.Close;
  QrBoletosC.Database := DModG.MyPID_DB;
  QrBoletosDR.Close;
  QrBoletosDR.Database := DModG.MyPID_DB;
  QrBoletosDI.Close;
  QrBoletosDI.Database := DModG.MyPID_DB;
  QrBoletosR.Close;
  QrBoletosR.Database := DModG.MyPID_DB;
  QrDR.Close;
  QrDR.Database := DModG.MyPID_DB;
  QrLocDR.Close;
  QrLocDR.Database := DModG.MyPID_DB;
  QrSumDR.Close;
  QrSumDR.Database := DModG.MyPID_DB;
  QrSumDI.Close;
  QrSumDI.Database := DModG.MyPID_DB;
end;

procedure TFmProdusys2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1A, LaAviso2A], Caption, True, taCenter, 2, 10, 20);
end;

function TFmProdusys2.IncluiRegistroC(): Boolean;
const
  TamMax = 29;
begin
  Result := False;
  FtxtLin := '';
  MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Inclu�ndo registro do tipo "C"');
{
Cabe�alho
Posi��o	Tamanho	Tipo      	Descri��o
1	      1	      Fixo	      Preencher com a letra C
2	      14	    Num�rico    CNPJ do condom�nio sem pontos, barras ou tra�o e com zeros a esquerda
16	    6	      Num�rico	   M�s e Ano de refer�ncia no formato mmaaaa
22	    8	      Num�rico	   Data de Vencimento das Taxas no formato ddmmaaaa

 Exemplo: C0686668400019512201010012011
}
  FcReg := 'C';
  if not AddItemX(FcReg, 01, #0, FcReg) then Exit;
// 02	CNPJ do condom�nio sem pontos, barras ou tra�o e com zeros a esquerda
  if not AddItemX(FcReg, 02, #0, Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value)) then Exit;
// 03	M�s e Ano de refer�ncia no formato mmaaaa
  if not AddItemX(FcReg, 03, #0, Geral.SoNumero_TT(EdMes.Text)) then Exit;
// 04 Data de Vencimento das Taxas no formato ddmmaaaa
  if not AddItemX(FcReg, 04, #0, Geral.FDT(QrBoletosVencto.Value, 23)) then Exit;
  //
  Result := AdicionaLinhaAoMeGerado(FcReg, FTxtLin, TamMax, FQtdC);
end;

function TFmProdusys2.IncluiRegistroDI_Arrecad(): Boolean;
{
  function CodigoMedida(var Codigo: String): Boolean;
  begin
    QrPrc.Close;
    QrPrc.Params[00].AsInteger := QrBoletosItsCODI_CONS.Value;
    QrPrc.Params[01].AsInteger := FCond;
    QrPrc.Open;
    if QrPrcExport_Tip.Value = 0 then
      Geral.MB_Aviso('CUIDADO! ' + sLineBreak +
      'O Tipo de Leitura (G�s, �gua, etc.) n�o foi definido para o cadastro n� ' +
      FormatFloat('0', QrBoletosItsCODI_CONS.Value) +
      ' de consumo por leitura!');
    //
    Codigo := FormatFloat('0', QrPrcExport_Med.Value);
    Result := QrPrcExport_Med.Value <> 0;
    if not Result then
      Geral.MB_Aviso('Inclus�o de registro abortado!' + sLineBreak +
      'O c�digo de medida (Forma de cobran�a) "' + Codigo +
      '" para o condom�nio ' + FormatFloat('0', FCond) + sLineBreak +
      ' no cadastro n� ' + FormatFloat('0', QrBoletosItsCODI_CONS.Value) +
      ' de consumo por leitura pode estar incorreto!');
  end;
}
  function VerificaTam(Texto: String; Tam: Integer): String;
  var
    T: Integer;
  begin
    T := Length(Texto);
    if (Tam <> 0) and (T > Tam) then
      Result := Copy(Texto, T - Tam + 1)
    else
      Result := Texto;
  end;
const
  TamMax = 70;
var
  xTax, xCMe, xMed, xAnt, xAtu, xPrc, xVal: String;
begin
  Result := False;
  FtxtLin := '';
  FLancto := FLancto + 1;
  MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Inclu�ndo o lan�amento ' +
    FormatFloat('0000', FLancto) + ' (registro do tipo "DI") arrecada��o n�o global');
{
Detalhe de Taxas Individuais
Fld  Posi��o Tamanho Tipo      Descri��o
[01] 1       2       Fixo      Preencher com as letras DI
[02] 3       3       Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6       15      Texto     Complemento
[04] 16(21)  6       Texto     Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
[05] 27      4       Texto     Unidade (ver tabela 1)
[06] 31      1       Num�rico  Tipo de Medida utilizada (ver tabela 2)
[07] 32      7       Num�rico  Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s e �gua.
[08] 39      7       Num�rico  Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s.
[09] 46      7       Num�rico  Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas e G�s.
[10] 53      9       Num�rico  Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
[11] 62      9       Num�rico  Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500

Exemplo: DI006               A     04  1000011900001230000400000002000000000238
}
  FcReg := 'DI';
  if not AddItemX(FcReg, 01, #0, FcReg) then Exit;
// 02	C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
  if not CodigoTaxa(QrBoletosItsGenero.Value, xTax) then Exit;
  if not AddItemX(FcReg, 02, #0, xTax) then Exit;
// 03 Complemento
  if not AddItemX(FcReg, 03, #0, ' ') then Exit;
// 04 Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
  if not AddItemX(FcReg, 04, #0, QrImvcnb_IDExporta.Value) then Exit;
// 05 Unidade (ver tabela 1)
  if not AddItemX(FcReg, 05, #0, QrImvCIM_IDEXPORTA_TXT.Value) then Exit;
// 06 Tipo de Medida utilizada (ver tabela 2)
  xCMe := '0';
  if not AddItemX(FcReg, 06, #0, xCMe, True) then Exit;
// 07 Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas de G�s e �gua.
  xMed := Geral.FTX(0, 4, 3, siPositivo);
  if not AddItemX(FcReg, 07, #0, xMed, True) then Exit;
// 08 Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas de G�s.
  xAnt := Geral.FTX(0, 4, 3, siPositivo);
  if not AddItemX(FcReg, 08, #0, VerificaTam(xAnt, 7), True) then Exit;
// 09 Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas e G�s.
  xAtu := Geral.FTX(0, 4, 3, siPositivo);
  if not AddItemX(FcReg, 09, #0, VerificaTam(xAtu, 7), True) then Exit;
// 10 Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
  xPrc := Geral.FTX(0, 7, 2, siPositivo);
  if not AddItemX(FcReg, 10, #0, xPrc, True) then Exit;
// 11 Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
  xVal := Geral.FTX(QrBoletosItsValor.Value, 7, 2, siPositivo);
  if not AddItemX(FcReg, 11, #0, xVal) then Exit;
//
  Result := AdicionaLinhaAoMeGerado(FcReg, FTxtLin, TamMax, FQtdDI);
end;

function TFmProdusys2.IncluiRegistroDI_Leitura(): Boolean;
  {
  function Produto(Codigo: Integer): String;
  begin
    case Codigo of
      1: Result := 'GAS ';
      2: Result := 'AGUA';
      else Result := '????';
    end;
  end;
  }
  function CodigoMedida(var Codigo: String): Boolean;
  begin
    QrPrc.Close;
    QrPrc.Params[00].AsInteger := QrBoletosItsCODI_CONS.Value;
    QrPrc.Params[01].AsInteger := FCond;
    QrPrc.Open;
    if QrPrcExport_Tip.Value = 0 then
      Geral.MB_Aviso('CUIDADO! ' + sLineBreak +
        'O Tipo de Leitura (G�s, �gua, etc.) n�o foi definido para o cadastro n� ' +
        FormatFloat('0', QrBoletosItsCODI_CONS.Value) +
        ' de consumo por leitura!');
    //
    Codigo := FormatFloat('0', QrPrcExport_Med.Value);
    Result := QrPrcExport_Med.Value <> 0;
    //
    if not Result then
      Geral.MB_Aviso('Inclus�o de registro abortado!' + sLineBreak +
        'O c�digo de medida (Forma de cobran�a) "' + Codigo +
        '" para o condom�nio ' + FormatFloat('0', FCond) + sLineBreak +
        ' no cadastro n� ' + FormatFloat('0', QrBoletosItsCODI_CONS.Value) +
        ' de consumo por leitura pode estar incorreto!');
  end;
  function VerificaTam(Texto: String; Tam: Integer): String;
  var
    T: Integer;
  begin
    T := Length(Texto);
    if (Tam <> 0) and (T > Tam) then
      Result := Copy(Texto, T - Tam + 1)
    else
      Result := Texto;
  end;
const
  TamMax = 70;
var
  xTax, xCMe, xMed, xAnt, xAtu, xPrc, xVal: String;
begin
  Result := False;
  FtxtLin := '';
  FLancto := FLancto + 1;
  MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Inclu�ndo o lan�amento ' +
    FormatFloat('0000', FLancto) + ' (registro do tipo "DI" - consumo por leitura)');
{
Detalhe de Taxas Individuais
Fld  Posi��o Tamanho Tipo      Descri��o
[01] 1       2       Fixo      Preencher com as letras DI
[02] 3       3       Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6       15      Texto     Complemento
[04] 16(21)  6       Texto     Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
[05] 27      4       Texto     Unidade (ver tabela 1)
[06] 31      1       Num�rico  Tipo de Medida utilizada (ver tabela 2)
[07] 32      7       Num�rico  Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s e �gua.
[08] 39      7       Num�rico  Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s.
[09] 46      7       Num�rico  Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas e G�s.
[10] 53      9       Num�rico  Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
[11] 62      9       Num�rico  Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500

Exemplo: DI006               A     04  1000011900001230000400000002000000000238
}
  FcReg := 'DI';
  if not AddItemX(FcReg, 01, #0, FcReg) then Exit;
// 02	C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
  if not CodigoTaxa(QrBoletosItsGenero.Value, xTax) then Exit;
  if not AddItemX(FcReg, 02, #0, xTax) then Exit;
// 03 Complemento
  if not AddItemX(FcReg, 03, #0, ' ') then Exit;
// 04 Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
  if not AddItemX(FcReg, 04, #0, QrImvcnb_IDExporta.Value) then Exit;
// 05 Unidade (ver tabela 1)
  if not AddItemX(FcReg, 05, #0, QrImvCIM_IDEXPORTA_TXT.Value) then Exit;
// 06 Tipo de Medida utilizada (ver tabela 2)
  if not CodigoMedida(xCMe) then Exit;
  if not AddItemX(FcReg, 06, #0, xCMe) then Exit;
// 07 Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas de G�s e �gua.
  xMed := Geral.FTX(QrBoletosItsConsumo.Value, 4, 3, siPositivo);
  if not AddItemX(FcReg, 07, #0, xMed) then Exit;
// 08 Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas de G�s.
  xAnt := Geral.FTX(QrBoletosItsMedAnt.Value, 4, 3, siPositivo);
  if not AddItemX(FcReg, 08, #0, VerificaTam(xAnt, 7)) then Exit;
// 09 Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010 Utilizar para taxas e G�s.
  xAtu := Geral.FTX(QrBoletosItsMedAtu.Value, 4, 3, siPositivo);
  if not AddItemX(FcReg, 09, #0, VerificaTam(xAtu, 7)) then Exit;
// 10 Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
  xPrc := Geral.FTX(QrBoletosItsPRECO_LEI.Value, 7, 2, siPositivo);
  if not AddItemX(FcReg, 10, #0, xPrc) then Exit;
// 11 Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
  xVal := Geral.FTX(QrBoletosItsValor.Value, 7, 2, siPositivo);
  if not AddItemX(FcReg, 11, #0, xVal) then Exit;
//
  Result := AdicionaLinhaAoMeGerado(FcReg, FTxtLin, TamMax, FQtdDI);
end;

function TFmProdusys2.IncluiRegistroDR(): Boolean;
const
  TamMax = 35;
var
  Valor: String;
  Boletos, Ativos: Integer;
begin
  // Colocar True para poder fazer os DI caso n�o haja DR
  Result := True;
  //
  MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Verificando se todos boletos ser�o exportados');
  Boletos := QrBoletos.RecordCount;
  //
  if FAptos <> Boletos then
  begin
    Geral.MB_Aviso(
      'Para seguran�a das informa��es n�o ser� criado nenhum registro do tipo "DR"'
      + sLineBreak + 'DR = Detalhe de Taxas para Rateio' + sLineBreak +
      'Esta medida ser� tomada pois a quantidade de UHs difere da quantidade de boletos!'
      + sLineBreak + 'Quantidade de UHs: ' + FormatFloat('0', FAptos)
      + sLineBreak + 'Quantidade de boletos: ' + FormatFloat('0', Boletos));
    Exit;
  end;
  Ativos  := 0;
  QrBoletos.First;
  while not QrBoletos.Eof do
  begin
    MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Verificando se o boleto ' +
    QrBoletosBOLAPTO.Value + ' est� ativo');
    if QrBoletosAtivo.Value = 1 then
      Ativos := Ativos + 1;
    //
    QrBoletos.Next;
  end;
  if Ativos <> Boletos then
  begin
    Geral.MB_Aviso(
      'Para seguran�a das informa��es n�o ser� criado nenhum registro do tipo "DR"'
      + sLineBreak + 'DR = Detalhe de Taxas para Rateio' + sLineBreak +
      'Esta medida ser� tomada pois ' + FormatFloat('0', Boletos - Ativos) +
      ' boletos n�o est�o ativos para exporta��o!'
      + sLineBreak + 'Quantidade de boletos criados: ' + FormatFloat('0', Boletos)
      + sLineBreak + 'Quantidade de boletos ativos:  ' + FormatFloat('0', Ativos));
    Exit;
  end;
  QrDR.Close;
  QrDR.SQL.Clear;
  QrDR.SQL.Add('DELETE FROM ' + FBoletosSR + ';');
  QrDR.SQL.Add('INSERT INTO ' + FBoletosSR);
  QrDR.SQL.Add('SELECT COUNT(Conta) ITENS, Conta, Valor');
  QrDR.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrDR.SQL.Add('WHERE ari.Codigo=' + FormatFloat('0', FPrev));
  QrDR.SQL.Add('AND Vencto="' + FVencto + '"');
  QrDR.SQL.Add('GROUP BY Conta, Valor');
  QrDR.SQL.Add('ORDER BY ITENS DESC;');
  QrDR.SQL.Add('DELETE FROM ' + FBoletosSR );
  QrDR.SQL.Add('WHERE ITENS <> ' + FormatFloat('0', FAptos) + ';');
  QrDR.SQL.Add('SELECT * FROM ' + FBoletosSR + ';');
  QrDR.Open;
  if QrDR.RecordCount = 0 then
  begin
    Geral.MB_Aviso(
      'Para seguran�a das informa��es n�o ser� criado nenhum registro do tipo "DR"'
      + sLineBreak + 'DR = Detalhe de Taxas para Rateio' + sLineBreak +
      'Esta medida ser� tomada pois n�o foi localizada nenhuma cobran�a igual ' + sLineBreak +
      'para todas as ' + FormatFloat('0', FAptos) + ' UHs do condom�nio selecionado!');
    Exit;
  end;
  //
  Result := False;
{
Detalhe de Taxas para     Rateio
Fld  Posi��o  Tamanho  Tipo      Descri��o
[01] 1        2        Fixo      Preencher com as letras DR
[02] 3        3        Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6        15       Texto     Complemento
[04] 16(21)   6        Texto     Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
[05] 27       9        Num�rico  Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500

 Exemplo: DR001TESTE                000015000
}
  FcReg := 'DR';
  QrDR.First;
  while not QrDR.Eof do
  begin
    FtxtLin := '';
    FLancto := FLancto + 1;
    MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Inclu�ndo o lan�amento ' +
      FormatFloat('0000', FLancto) + ' (registro do tipo "DR")');
    //
    if not AddItemX(FcReg, 01, #0, FcReg) then Exit;
  // 02	C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
    if not CodigoTaxa(QrDRConta.Value, Valor) then Exit;
    if not AddItemX(FcReg, 02, #0, Valor) then Exit;
  // 03 Complemento
    if not AddItemX(FcReg, 03, #0, ' ') then Exit;
  // 04 Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
    if not AddItemX(FcReg, 04, #0, ' ') then Exit;
  // 05 Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500
    Valor := Geral.FTX(QrDRValor.Value, 7, 2, siPositivo);
    if not AddItemX(FcReg, 05, #0, Valor) then Exit;
    //
    Result := AdicionaLinhaAoMeGerado(FcReg, FTxtLin, TamMax, FQtdDR);
    //
    if not Result then
      Exit;
    //
    QrDR.Next;
  end;
end;

function TFmProdusys2.IncluiRegistroR(): Boolean;
const
  TamMax = 7;
var
  xQtd: String;
begin
  Result := False;
  FtxtLin := '';
  FLancto := FLancto + 1;
  MyObjects.Informa2(LaAviso1A, LaAviso1B, True, 'Gerando o registro do tipo "R"');
{
Rodap�
Posi��o Tamanho Tipo        Descri��o
1       1       Fixo        Preencher com a letra R
2       6       Num�rico    Total de linhas do arquivo formatado com zeros a esquerda, exemplo 000099

 Exemplo: R000099
}
  FcReg := 'R';
  if not AddItemX(FcReg, 01, #0, FcReg) then Exit;
// 02 Total de linhas do arquivo formatado com zeros a esquerda, exemplo 000099    
  xQtd := Geral.FTX(FQtdC + FQtdDR + FQtdDI + 1, 6, 0, siPositivo);
  if not AddItemX(FcReg, 02, #0, xQtd) then Exit;
  //
  Result := AdicionaLinhaAoMeGerado(FcReg, FTxtLin, TamMax, FQtdR);
end;

procedure TFmProdusys2.InfoPosMeGerado();
var
  L, C: Integer;
  MudouLinha: Boolean;
  L_Txt, Linha: String;
  //
  Registro: String;
  //Campo: Integer;
  //SubTipo: Char;
  //DescriCampo, ConteudoCampo, ValorCampo: String;
begin
  MudouLinha := False;
  L := Geral.RichRow(MeGerado) + 1;
  C := Geral.RichCol(MeGerado) + 1;
  L_Txt := FormatFloat('0', L);
  StatusBar.Panels[1].Text := Format('L: %3d   C: %3d', [L, C]);
  //
  if (L <> FMeGerado_SelLin) then
  begin
    FMeGerado_SelLin := L;
    MudouLinha    := True;
  end;
  //
  if (C <> FMeGerado_SelCol) then
  begin
    FMeGerado_SelCol := C;
  end;
  if MudouLinha then
  begin
    Linha := MeGerado.Lines[L - 1];
    //
    Registro := Copy(Linha, 1, 2);
    if (Registro <> 'DR') and (Registro <> 'DI') then
      Registro := Copy(Linha, 1, 1);
    QrBoletosDR.Filter := 'Linha=' + L_Txt;
    QrBoletosDI.Filter := 'Linha=' + L_Txt;
    DBGridC.Visible  := Registro = 'C';
    DBGridDR.Visible := Registro = 'DR';
    DBGridDI.Visible := Registro = 'DI';
    DBGridR.Visible  := Registro = 'R';
    //
  end;
end;

function TFmProdusys2.LimpaValor(Valor: String): String;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(Valor) do
    if (Valor[I] <> ' ') and (Valor[I] <> '0') then
      Result := Result + Valor[I];
end;

procedure TFmProdusys2.MeGeradoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  InfoPosMeGerado();
end;

procedure TFmProdusys2.MeGeradoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

procedure TFmProdusys2.MeGeradoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  InfoPosMeGerado();
end;

function TFmProdusys2.ObtemDescricaoDeCampo(nReg: String; PosI: Integer;
  SubTipo: Char): String;
begin
  Result := '?????';
  if nReg = 'C' then
  begin
    case PosI of
      001: Result := 'Tipo de registro: C - Cabe�alho';
      002: Result := 'CNPJ do condom�nio';
      016: Result := 'M�s e Ano de refer�ncia';
      022: Result := 'Data de Vencimento das Taxas';
    end;
  end else
  if nReg = 'DR' then
  begin
{
Detalhe de Taxas para     Rateio
Fld  Posi��o  Tamanho  Tipo      Descri��o
[01] 1        2        Fixo      Preencher com as letras DR
[02] 3        3        Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6        15       Texto     Complemento
[04] 16(21)   6        Texto     Bloco, preencher com brancos para rateio entre todos as unidades e todos os blocos
[05] 27       9        Num�rico  Valor por unidade, somente n�meros, os �ltimos dois n�mero devem ser as decimais, para R$ 155,00 utilizar 000015500

 Exemplo: DR001TESTE                000015000
}
    case PosI of
      001: Result := 'Tipo de registro: DR - Detalhe de Rateio';
      003: Result := 'C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda';
      006: Result := 'Complemento';
      021: Result := 'Bloco';
      027: Result := 'Valor por unidade';
    end;
  end else
  if nReg = 'DI' then
  begin
{
Detalhe de Taxas Individuais
Fld  Posi��o Tamanho Tipo      Descri��o
[01] 1       2       Fixo      Preencher com as letras DI
[02] 3       3       Num�rico  C�digo de identifica��o da taxa (ver tabela 3) com zeros a esquerda
[03] 6       15      Texto     Complemento
[04] 16(21)  6       Texto     Bloco, preencher com brancos caso n�o exista bloco (ver tabela 1)
[05] 27      4       Texto     Unidade (ver tabela 1)
[06] 31      1       Num�rico  Tipo de Medida utilizada (ver tabela 2)
[07] 32      7       Num�rico  Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s e �gua.
[08] 39      7       Num�rico  Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas de G�s.
[09] 46      7       Num�rico  Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais, para 1,010 utilizar 0001010
                          Utilizar para taxas e G�s.
[10] 53      9       Num�rico  Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500
[11] 62      9       Num�rico  Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais, para R$ 155,00 utilizar 000015500

Exemplo: DI006               A     04  1000011900001230000400000002000000000238
}
    case PosI of
      001: Result := 'Tipo de registro: DI - Detalhe de Individual';
      003: Result := 'C�digo de identifica��o da taxa (ver tabela 3)';
      006: Result := 'Complemento';
      021: Result := 'Bloco';
      027: Result := 'Unidade (ver tabela 1)';
      031: Result := 'Tipo de Medida utilizada (ver tabela 2)';
      032: Result := 'Quantidade, somente n�meros, os �ltimos tres n�meros devem ser as decimais';
      039: Result := 'Leitura anterior, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais';
      046: Result := 'Leitura atual, somente n�meros, os �ltimos tr�s n�meros devem ser as decimais';
      053: Result := 'Valor por quantidade unit�ria, somente n�meros, os �ltimos dois n�meros devem ser as decimais';
      062: Result := 'Valor total, somente n�meros, os �ltimos dois n�meros devem ser as decimais';
    end;
  end else
  if nReg = 'R' then
  begin
    case PosI of
      001: Result := 'Tipo de registro: R - Rodap�';
      002: Result := 'Quantidade total de registros no arquivo';
    end;
  end;
end;

procedure TFmProdusys2.QrBoletosAfterOpen(DataSet: TDataSet);
begin
  BtGerar.Enabled  := QrBoletos.RecordCount > 0;
  BtTodos.Enabled  := QrBoletos.RecordCount > 1;
  BtNenhum.Enabled := QrBoletos.RecordCount > 1;
end;

procedure TFmProdusys2.QrBoletosAfterScroll(DataSet: TDataSet);
begin
  ReopenBoletosIts();
end;

procedure TFmProdusys2.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  BtGerar.Enabled := False;
  BtSalvar.Enabled := False;
  QrBoletosIts.Close;
end;

procedure TFmProdusys2.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosSUB_TOT.Value :=
  QrBoletosSUB_ARR.Value +
  QrBoletosSUB_LEI.Value;
  //
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
  //
  if ( (Trim(QrBoletosUSERNAME.Value) <> '')
  and (Trim(QrBoletosPASSWORD.Value) <> '')) then
    QrBoletosPWD_WEB.Value := 'Login: ' + QrBoletosUSERNAME.Value +
    '   Senha: ' + QrBoletosPASSWORD.Value
  else QrBoletosPWD_WEB.Value := '';
end;

procedure TFmProdusys2.QrBoletosItsCalcFields(DataSet: TDataSet);
begin
  QrBoletosItsTEXTO_IMP.Value := QrBoletosItsTEXTO.Value +
    DmBloq.TextoExplicativoItemBoleto(
    QrBoletosItsTipo.Value, QrBoletosItsCasas.Value,
    QrBoletosItsMedAnt.Value, QrBoletosItsMedAtu.Value,
    QrBoletosItsConsumo.Value, QrBoletosItsUnidFat.Value,
    QrBoletosItsUnidLei.Value, QrBoletosItsUnidImp.Value,
    QrBoletosItsGeraTyp.Value, QrBoletosItsCasRat.Value,
    QrBoletosItsNaoImpLei.Value, QrBoletosItsGeraFat.Value);
  //
  QrBoletosItsVENCTO_TXT.Value := Geral.FDT(QrBoletosItsVencto.Value, 3);
  //
  QrBoletosItsPRECO_LEI.Value :=
    QrBoletosItsPreco.Value * QrBoletosItsUnidFat.Value;
end;

procedure TFmProdusys2.QrImvCalcFields(DataSet: TDataSet);
begin
  if Trim(QrImvcim_IDExporta.Value) <> '' then
    QrImvCIM_IDEXPORTA_TXT.Value := QrImvcim_IDExporta.Value
  else if (Length(QrImvUnidade.Value) = 3)
  and (Length(Geral.SoNumero_TT(QrImvUnidade.Value)) = 3) then
    QrImvCIM_IDEXPORTA_TXT.Value := QrImvUnidade.Value
  else
    QrImvCIM_IDEXPORTA_TXT.Value := '';
end;

procedure TFmProdusys2.ReopenBoletos(BOLAPTO: String);
begin
  QrBoletos.Close;
  QrBoletos.Open;
  QrBoletos.Locate('BOLAPTO', BOLAPTO, []);
end;

procedure TFmProdusys2.ReopenBoletosIts();
begin
  QrBoletosIts.Close;
  QrBoletosIts.SQL.Clear;
  QrBoletosIts.SQL.Add('SELECT "A" Tabela, ari.Apto, '); // Arrecada��o
  QrBoletosIts.SQL.Add('ari.Texto TEXTO, ari.Valor VALOR, ari.Vencto,');
  QrBoletosIts.SQL.Add('0 Tipo, 0 MedAnt, 0 MedAtu, 0 Consumo, 0 Casas,');
  QrBoletosIts.SQL.Add('"" UnidLei, "" UnidImp, 1 UnidFat, Controle, Lancto,');
  QrBoletosIts.SQL.Add('0 GeraTyp, 0 GeraFat, 0 CasRat, 0 NaoImpLei, ');
  QrBoletosIts.SQL.Add('0 Preco, 0 Export_Tip, 0 CODI_CONS, Conta Genero');
  QrBoletosIts.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrBoletosIts.SQL.Add('WHERE ari.Boleto=:P0');
  QrBoletosIts.SQL.Add('AND ari.Apto=:P1');
  QrBoletosIts.SQL.Add('AND ari.Codigo=:P2');
  QrBoletosIts.SQL.Add('');
  QrBoletosIts.SQL.Add('UNION');
  QrBoletosIts.SQL.Add('');
  QrBoletosIts.SQL.Add('SELECT "L" Tabela, cni.Apto, '); // Leitura
  QrBoletosIts.SQL.Add('cns.Nome TEXTO, cni.Valor VALOR, cni.Vencto,');
  QrBoletosIts.SQL.Add('1 Tipo, MedAnt, MedAtu, Consumo, Casas,');
  QrBoletosIts.SQL.Add('UnidLei,UnidImp, UnidFat, Controle, Lancto,');
  QrBoletosIts.SQL.Add('cni.GeraTyp,cni.GeraFat, cni.CasRat, cni.NaoImpLei,');
  QrBoletosIts.SQL.Add('cni.Preco, cns.Export_Tip, cns.Codigo CODI_CONS, cns.Genero');
  QrBoletosIts.SQL.Add('FROM cons cns');
  QrBoletosIts.SQL.Add('LEFT JOIN ' + FTabCnsA + ' cni ON cni.Codigo=cns.Codigo');
  QrBoletosIts.SQL.Add('WHERE cni.Boleto=:P3');
  QrBoletosIts.SQL.Add('AND cni.Apto=:P4');
  QrBoletosIts.SQL.Add('AND cni.Periodo=:P5');
  QrBoletosIts.SQL.Add('');
  QrBoletosIts.SQL.Add('ORDER BY VALOR DESC');
  QrBoletosIts.Params[00].AsFloat   := QrBoletosBoleto.Value;
  QrBoletosIts.Params[01].AsInteger := QrBoletosApto.Value;
  QrBoletosIts.Params[02].AsInteger := FPrev;
  QrBoletosIts.Params[03].AsFloat   := QrBoletosBoleto.Value;
  QrBoletosIts.Params[04].AsInteger := QrBoletosApto.Value;
  QrBoletosIts.Params[05].AsInteger := FPeriodo;
  QrBoletosIts.Open;
  //MLAGeral.LeMeuSQLy(QrBoletosIts, '', nil, False, True);
  //
{
  if ? then
  //if PageIndex = 4 then
    QrBoletosIts.Locate('TEXTO', FBolItsSim, [])
  else
    QrBoletosIts.Locate('TEXTO', FBolItsNao, []);
}
end;

procedure TFmProdusys2.RGEstagioClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmProdusys2.TPVenctoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmProdusys2.TPVenctoClick(Sender: TObject);
begin
  FechaPesquisa();
end;

{
Tabela 3
C�DIGO TAXA
1 TAXA DE CONDOMINIO
2	COPEL
3	SANEPAR
4	FUNDO DE RESERVA
5	CHAM CAPITAL
6	GAS
7	TAXA DE MUDAN�A
8	SALAO DE FESTA
}
 //Parei aqui!
{ TODO -oEuroadministradora -cExporta Pro condomino : 
Fazer somat�ria de valores
Comparando total de boletos pesquisados
com as tabelas tempor�rias criadas }
end.
