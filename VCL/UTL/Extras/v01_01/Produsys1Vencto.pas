unit Produsys1Vencto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmProdusys1Vencto = class(TForm)
    Panel1: TPanel;
    RGVencto: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGVenctoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FVencto: TDateTime;
  end;

  var
  FmProdusys1Vencto: TFmProdusys1Vencto;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmProdusys1Vencto.BtOKClick(Sender: TObject);
begin
  FVencto := Geral.ValidaDataBR(RGVencto.Items[RGVencto.ItemIndex], True, False);
  Close;
end;

procedure TFmProdusys1Vencto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdusys1Vencto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdusys1Vencto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FVencto := 0;
end;

procedure TFmProdusys1Vencto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProdusys1Vencto.RGVenctoClick(Sender: TObject);
begin
  BtOK.Enabled := RGVencto.ItemIndex > -1;
end;

end.
