object FmProdusys1: TFmProdusys1
  Left = 339
  Top = 185
  Caption = 'PRO-DUSYS-001 :: Exporta Produsys'
  ClientHeight = 502
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 888
      Top = 1
      Width = 119
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 10
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 887
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object StatusBar: TStatusBar
        Left = 0
        Top = 27
        Width = 887
        Height = 19
        Panels = <
          item
            Text = ' Posi'#231#227'o do cursor no texto gerado:'
            Width = 192
          end
          item
            Width = 100
          end
          item
            Text = ' Arquivo salvo:'
            Width = 96
          end
          item
            Width = 50
          end>
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Exporta Produsys'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 406
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 101
        Height = 13
        Caption = 'Cliente (Condom'#237'nio):'
      end
      object Label2: TLabel
        Left = 584
        Top = 5
        Width = 41
        Height = 13
        Caption = 'Per'#237'odo:'
      end
      object Label3: TLabel
        Left = 640
        Top = 4
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object EdCond: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdCondChange
        DBLookupComboBox = CBCond
      end
      object CBCond: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 501
        Height = 21
        KeyField = 'CodCliInt'
        ListField = 'NOMECLI'
        ListSource = DsCond
        TabOrder = 1
        dmkEditCB = EdCond
        UpdType = utYes
      end
      object TPVencto: TdmkEditDateTimePicker
        Left = 640
        Top = 20
        Width = 112
        Height = 21
        Date = 40675.814365277780000000
        Time = 40675.814365277780000000
        TabOrder = 3
        OnClick = TPVenctoClick
        OnChange = TPVenctoChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdMes: TdmkEdit
        Left = 584
        Top = 20
        Width = 54
        Height = 21
        Alignment = taCenter
        TabOrder = 2
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        QryCampo = 'Mez'
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        OnChange = EdMesChange
      end
      object RGEstagio: TRadioGroup
        Left = 756
        Top = 1
        Width = 241
        Height = 46
        Caption = ' Est'#225'gio: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Pr'#233'-bloqueto'
          'Bloqueto')
        TabOrder = 4
        OnClick = RGEstagioClick
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 1006
      Height = 356
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Boletos '
        object Splitter1: TSplitter
          Left = 643
          Top = 0
          Width = 5
          Height = 287
          Align = alRight
          ExplicitLeft = 745
        end
        object DBGradeS: TdmkDBGridDAC
          Left = 129
          Top = 0
          Width = 514
          Height = 287
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'BOLAPTO')
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Boleto'
              Title.Caption = 'Bloqueto'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 173
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB_TOT'
              Title.Caption = 'Valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_TXT'
              Title.Caption = 'Vencimen.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOTE_PROTOCO'
              Title.Caption = 'Lote'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTOCOLO'
              Title.Caption = 'Protocolo'
              Width = 56
              Visible = True
            end>
          Color = clWindow
          DataSource = DsBoletos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          SQLTable = '_boletos_'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Boleto'
              Title.Caption = 'Bloqueto'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROPRIET'
              Title.Caption = 'Propriet'#225'rio'
              Width = 173
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB_TOT'
              Title.Caption = 'Valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VENCTO_TXT'
              Title.Caption = 'Vencimen.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LOTE_PROTOCO'
              Title.Caption = 'Lote'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTOCOLO'
              Title.Caption = 'Protocolo'
              Width = 56
              Visible = True
            end>
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 129
          Height = 287
          Align = alLeft
          ParentBackground = False
          TabOrder = 1
          object BtReabre: TBitBtn
            Tag = 18
            Left = 4
            Top = 16
            Width = 120
            Height = 40
            Caption = '&Reabre'
            TabOrder = 0
            OnClick = BtReabreClick
            NumGlyphs = 2
          end
          object BtTodos: TBitBtn
            Tag = 127
            Left = 4
            Top = 84
            Width = 120
            Height = 40
            Caption = '&Todos'
            Enabled = False
            TabOrder = 1
            OnClick = BtTodosClick
            NumGlyphs = 2
          end
          object BtNenhum: TBitBtn
            Tag = 128
            Left = 4
            Top = 128
            Width = 120
            Height = 40
            Caption = '&Nenhum'
            Enabled = False
            TabOrder = 2
            OnClick = BtNenhumClick
            NumGlyphs = 2
          end
          object BtGerar: TBitBtn
            Tag = 41
            Left = 4
            Top = 196
            Width = 120
            Height = 40
            Caption = '&Gerar texto'
            Enabled = False
            TabOrder = 3
            OnClick = BtGerarClick
            NumGlyphs = 2
          end
          object BtSalvar: TBitBtn
            Tag = 24
            Left = 4
            Top = 240
            Width = 120
            Height = 40
            Caption = '&Salvar texto'
            Enabled = False
            TabOrder = 4
            OnClick = BtSalvarClick
            NumGlyphs = 2
          end
        end
        object Panel16: TPanel
          Left = 0
          Top = 287
          Width = 998
          Height = 41
          Align = alBottom
          TabOrder = 2
          object LaAviso: TLabel
            Left = 8
            Top = 4
            Width = 41
            Height = 13
            Caption = '..........'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object PB1: TProgressBar
            Left = 8
            Top = 20
            Width = 981
            Height = 17
            TabOrder = 0
          end
        end
        object Panel6: TPanel
          Left = 648
          Top = 0
          Width = 350
          Height = 287
          Align = alRight
          ParentBackground = False
          TabOrder = 3
          object Panel33: TPanel
            Left = 1
            Top = 1
            Width = 348
            Height = 25
            Align = alTop
            BevelOuter = bvNone
            Caption = 'Itens do bloqueto selecionado'
            TabOrder = 0
          end
          object DBGrid10: TDBGrid
            Left = 1
            Top = 26
            Width = 348
            Height = 260
            Align = alClient
            DataSource = DsBoletosIts
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'TEXTO_IMP'
                Title.Caption = 'Texto para impress'#227'o'
                Width = 244
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALOR'
                Title.Caption = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VENCTO_TXT'
                Title.Caption = 'Vencimen.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lancto'
                Title.Caption = 'Lan'#231'amento'
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Texto Exporta'#231#227'o'
        ImageIndex = 1
        object MeGerado: TMemo
          Left = 0
          Top = 0
          Width = 998
          Height = 68
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          OnKeyUp = MeGeradoKeyUp
          OnMouseDown = MeGeradoMouseDown
          OnMouseUp = MeGeradoMouseUp
        end
        object PnGrids: TPanel
          Left = 0
          Top = 68
          Width = 998
          Height = 260
          Align = alBottom
          ParentBackground = False
          TabOrder = 1
          object DBGridC: TDBGrid
            Left = 1
            Top = 1
            Width = 996
            Height = 64
            Align = alTop
            DataSource = DsBoletosC
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Linha'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CNPJ'
                Width = 112
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ano'
                Width = 34
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Mes'
                Title.Caption = 'M'#234's'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencto'
                Title.Caption = 'Vencimento'
                Visible = True
              end>
          end
          object DBGridDR: TDBGrid
            Left = 1
            Top = 65
            Width = 996
            Height = 64
            Align = alTop
            DataSource = DsBoletosDR
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Linha'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UH'
                Title.Caption = 'C'#243'd. Tabela 1'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Compl'
                Title.Caption = 'Complemento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bloco'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 80
                Visible = True
              end>
          end
          object DBGridDI: TDBGrid
            Left = 1
            Top = 129
            Width = 996
            Height = 64
            Align = alTop
            DataSource = DsBoletosDI
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Linha'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UH'
                Title.Caption = 'C'#243'd. Tabela 1'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Compl'
                Title.Caption = 'Complemento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Bloco'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Unidade'
                Title.Caption = 'UNID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TMU'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LeituAnt'
                Title.Caption = 'Leit. ant.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LeituAtu'
                Title.Caption = 'Leit. atual'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Preco'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 80
                Visible = True
              end>
          end
          object DBGridR: TDBGrid
            Left = 1
            Top = 193
            Width = 996
            Height = 64
            Align = alTop
            DataSource = DsBoletosR
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Linha'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Registros'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, '
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, '
      'eci.CodCliInt, eci.CodEnti'
      'FROM enticliint eci'
      'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti'
      'WHERE Cliente1="V"'
      'AND TipoTabLct=1'
      'ORDER BY NOMECLI')
    Left = 4
    Top = 4
    object QrCondNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCondCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrCondCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrCondCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 32
    Top = 4
  end
  object QrLocPrev: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Periodo, Cond '
      'FROM prev'
      'WHERE Cond=:P0'
      'AND Periodo=:P1'
      '')
    Left = 972
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocPrevCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPrevPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrLocPrevCond: TIntegerField
      FieldName = 'Cond'
    end
  end
  object QrBolLei: TmySQLQuery
    Database = Dmod.MyDB
    Left = 116
    Top = 4
    object QrBolLeiValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolLeiApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolLeiBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolLeiBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrBolArr: TmySQLQuery
    Database = Dmod.MyDB
    Left = 144
    Top = 4
    object QrBolArrValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
    object QrBolArrApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrBolArrBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBolArrBoleto: TFloatField
      FieldName = 'Boleto'
      Required = True
    end
  end
  object QrPPI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      'ppi.Docum, ptc.Nome TAREFA, ptc.Def_Sender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1')
    Left = 172
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPPIPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'protpakits.Conta'
      Required = True
    end
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
      Required = True
    end
    object QrPPIDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
      Required = True
    end
    object QrPPICancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
      Required = True
    end
    object QrPPIMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
      Required = True
    end
    object QrPPIID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
      Required = True
    end
    object QrPPIID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
      Required = True
    end
    object QrPPIID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
      Required = True
    end
    object QrPPIID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
      Required = True
    end
    object QrPPITAREFA: TWideStringField
      FieldName = 'TAREFA'
      Origin = 'protocolos.Nome'
      Size = 100
    end
    object QrPPIDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPILOTE: TIntegerField
      FieldName = 'LOTE'
      Origin = 'protpakits.Controle'
      Required = True
    end
    object QrPPICliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
      Required = True
    end
    object QrPPICliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
      Required = True
    end
    object QrPPIPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
      Required = True
    end
    object QrPPIDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPIDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPIDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
      Origin = 'protocolos.Def_Sender'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
      Origin = 'protpakits.Docum'
      Required = True
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT usu.CodigoEnt, usu.CodigoEsp,'
      'usu.Username, usu.Password'
      'FROM condimov imv'
      
        'LEFT JOIN entidades ent ON IF(imv.Usuario>0, imv.Usuario, imv.Pr' +
        'opriet)=ent.Codigo'
      'LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet '
      '  AND usu.CodigoEsp=imv.Conta '
      '  AND (usu.Tipo=1 OR usu.Tipo IS NULL)'
      'WHERE imv.Codigo=:P0')
    Left = 200
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
      Origin = 'users.CodigoEnt'
    end
    object QrUsersCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Origin = 'users.CodigoEsp'
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'users.Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'users.Password'
      Size = 32
    end
  end
  object QrBoletos: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrBoletosAfterOpen
    BeforeClose = QrBoletosBeforeClose
    AfterScroll = QrBoletosAfterScroll
    OnCalcFields = QrBoletosCalcFields
    Left = 60
    Top = 4
    object QrBoletosApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'Apto'
      Required = True
    end
    object QrBoletosUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'Unidade'
      Size = 10
    end
    object QrBoletosSUB_ARR: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_ARR'
      LookupDataSet = QrBolArr
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_LEI: TFloatField
      FieldKind = fkLookup
      FieldName = 'SUB_LEI'
      LookupDataSet = QrBolLei
      LookupKeyFields = 'BOLAPTO'
      LookupResultField = 'Valor'
      KeyFields = 'BOLAPTO'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
    object QrBoletosSUB_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUB_TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrBoletosVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'Propriet'
      Required = True
    end
    object QrBoletosNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Origin = 'NOMEPROPRIET'
      Size = 100
    end
    object QrBoletosBOLAPTO: TWideStringField
      FieldName = 'BOLAPTO'
      Origin = 'BOLAPTO'
      Required = True
      Size = 23
    end
    object QrBoletosVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosUSERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Username'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBoletosPASSWORD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'PASSWORD'
      LookupDataSet = QrUsers
      LookupKeyFields = 'CodigoEsp'
      LookupResultField = 'Password'
      KeyFields = 'Apto'
      Size = 100
      Lookup = True
    end
    object QrBoletosPWD_WEB: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PWD_WEB'
      Size = 255
      Calculated = True
    end
    object QrBoletosPROTOCOLO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'PROTOCOLO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'PROTOCOLO'
      KeyFields = 'Boleto'
      DisplayFormat = '00000;-000000; '
      Lookup = True
    end
    object QrBoletosAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'Andar'
    end
    object QrBoletosBoleto: TFloatField
      FieldName = 'Boleto'
      Origin = 'Boleto'
      Required = True
    end
    object QrBoletosBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrBoletosKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrBoletosLOTE_PROTOCO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LOTE_PROTOCO'
      LookupDataSet = QrPPI
      LookupKeyFields = 'Docum'
      LookupResultField = 'LOTE'
      KeyFields = 'Boleto'
      Lookup = True
    end
    object QrBoletosOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrBoletosFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
    end
    object QrBoletosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsBoletos: TDataSource
    DataSet = QrBoletos
    Left = 88
    Top = 4
  end
  object QrVenctos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 944
    Top = 12
    object QrVenctosVencto: TDateField
      FieldName = 'Vencto'
    end
  end
  object QrBoletosC: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM _boletos_c_')
    Left = 416
    Top = 124
    object QrBoletosCLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrBoletosCTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrBoletosCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrBoletosCAno: TIntegerField
      FieldName = 'Ano'
    end
    object QrBoletosCMes: TIntegerField
      FieldName = 'Mes'
    end
    object QrBoletosCVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrBoletosCAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrBoletosIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrBoletosItsCalcFields
    SQL.Strings = (
      '/*syndic.QrBoletosIts*/'
      '/*********** SQL ***********/'
      'SELECT "A" Tabela, ari.Apto, '
      'ari.Texto TEXTO, ari.Valor VALOR, ari.Vencto,'
      '0 Tipo, 0 MedAnt, 0 MedAtu, 0 Consumo, 0 Casas,'
      '"" UnidLei, "" UnidImp, 1 UnidFat, Controle, Lancto,'
      '0 GeraTyp, 0 GeraFat, 0 CasRat, 0 NaoImpLei,'
      '0 Preco, 0 Export_Tip, 0 CODI_CONS'
      'FROM syndic.ari0008a ari'
      'WHERE ari.Boleto=646873'
      'AND ari.Apto=319'
      'AND ari.Codigo=1609'
      ''
      'UNION'
      ''
      'SELECT "L" Tabela, cni.Apto,'
      'cns.Nome TEXTO, cni.Valor VALOR, cni.Vencto,'
      '1 Tipo, MedAnt, MedAtu, Consumo, Casas,'
      'UnidLei,UnidImp, UnidFat, Controle, Lancto,'
      'cni.GeraTyp,cni.GeraFat, cni.CasRat, cni.NaoImpLei,'
      'cni.Preco, cns.Export_Tip, cns.Codigo CODI_CONS'
      'FROM cons cns'
      'LEFT JOIN syndic.cns0008a cni ON cni.Codigo=cns.Codigo'
      'WHERE cni.Boleto=646873 '
      'AND cni.Apto=319'
      'AND cni.Periodo=136'
      ''
      'ORDER BY VALOR DESC'
      ''
      '/********* FIM SQL *********/'
      ''
      '/***** Parametros *******/'
      '/***** P0 = 646873 ******/'
      '/***** P1 = 319 ******/'
      '/***** P2 = 1609 ******/'
      '/***** P3 = 646873 ******/'
      '/***** P4 = 319 ******/'
      '/***** P5 = 136 ******/'
      '/***** FIM Parametros *******/'
      ''
      '')
    Left = 228
    Top = 4
    object QrBoletosItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Required = True
      Size = 50
    end
    object QrBoletosItsVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrBoletosItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBoletosItsTEXTO_IMP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_IMP'
      Size = 255
      Calculated = True
    end
    object QrBoletosItsMedAnt: TFloatField
      FieldName = 'MedAnt'
    end
    object QrBoletosItsMedAtu: TFloatField
      FieldName = 'MedAtu'
    end
    object QrBoletosItsConsumo: TFloatField
      FieldName = 'Consumo'
    end
    object QrBoletosItsCasas: TLargeintField
      FieldName = 'Casas'
    end
    object QrBoletosItsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrBoletosItsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 5
    end
    object QrBoletosItsUnidFat: TFloatField
      FieldName = 'UnidFat'
    end
    object QrBoletosItsTipo: TLargeintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrBoletosItsVENCTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VENCTO_TXT'
      Size = 10
      Calculated = True
    end
    object QrBoletosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBoletosItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrBoletosItsGeraTyp: TLargeintField
      FieldName = 'GeraTyp'
    end
    object QrBoletosItsGeraFat: TFloatField
      FieldName = 'GeraFat'
    end
    object QrBoletosItsCasRat: TLargeintField
      FieldName = 'CasRat'
    end
    object QrBoletosItsNaoImpLei: TLargeintField
      FieldName = 'NaoImpLei'
    end
    object QrBoletosItsTabela: TWideStringField
      FieldName = 'Tabela'
      Required = True
      Size = 1
    end
    object QrBoletosItsApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrBoletosItsPreco: TFloatField
      FieldName = 'Preco'
    end
    object QrBoletosItsExport_Tip: TLargeintField
      FieldName = 'Export_Tip'
      Required = True
    end
    object QrBoletosItsCODI_CONS: TLargeintField
      FieldName = 'CODI_CONS'
      Required = True
    end
    object QrBoletosItsPRECO_LEI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO_LEI'
      Calculated = True
    end
  end
  object DsBoletosIts: TDataSource
    DataSet = QrBoletosIts
    Left = 256
    Top = 4
  end
  object QrImv: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrImvCalcFields
    SQL.Strings = (
      'SELECT cim.Unidade, cim.IDExporta cim_IDExporta,'
      'cnb.IDExporta cnb_IDExporta'
      'FROM condimov cim'
      'LEFT JOIN condbloco cnb ON cnb.Controle=cim.Controle'
      'WHERE cim.Conta=:P0'
      '')
    Left = 656
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImvUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrImvcim_IDExporta: TWideStringField
      FieldName = 'cim_IDExporta'
    end
    object QrImvcnb_IDExporta: TWideStringField
      FieldName = 'cnb_IDExporta'
    end
    object QrImvCIM_IDEXPORTA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CIM_IDEXPORTA_TXT'
      Size = 3
      Calculated = True
    end
  end
  object QrPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Export_Apl, Export_Med'
      'FROM consprc'
      'WHERE Codigo=:P0'
      'AND Cond=:P1')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrcExport_Apl: TSmallintField
      FieldName = 'Export_Apl'
    end
    object QrPrcExport_Med: TSmallintField
      FieldName = 'Export_Med'
    end
  end
  object DsBoletosC: TDataSource
    DataSet = QrBoletosC
    Left = 444
    Top = 124
  end
  object DsBoletosDR: TDataSource
    DataSet = QrBoletosDR
    Left = 444
    Top = 152
  end
  object QrBoletosDR: TmySQLQuery
    Database = DModG.MyPID_DB
    Filtered = True
    SQL.Strings = (
      'SELECT * '
      'FROM _boletos_dr_')
    Left = 416
    Top = 152
    object QrBoletosDRLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrBoletosDRTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 2
    end
    object QrBoletosDRUH: TIntegerField
      FieldName = 'UH'
    end
    object QrBoletosDRCompl: TWideStringField
      FieldName = 'Compl'
      Size = 15
    end
    object QrBoletosDRBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 6
    end
    object QrBoletosDRValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBoletosDRAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsBoletosDI: TDataSource
    DataSet = QrBoletosDI
    Left = 444
    Top = 180
  end
  object QrBoletosDI: TmySQLQuery
    Database = DModG.MyPID_DB
    Filtered = True
    SQL.Strings = (
      'SELECT * '
      'FROM _boletos_di_')
    Left = 416
    Top = 180
    object QrBoletosDILinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrBoletosDITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 2
    end
    object QrBoletosDIUH: TIntegerField
      FieldName = 'UH'
    end
    object QrBoletosDICompl: TWideStringField
      FieldName = 'Compl'
      Size = 15
    end
    object QrBoletosDIBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 6
    end
    object QrBoletosDIUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 4
    end
    object QrBoletosDIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,##0.000;-#,##0.000; '
    end
    object QrBoletosDILeituAnt: TFloatField
      FieldName = 'LeituAnt'
      DisplayFormat = '#,##0.000;-#,##0.000; '
    end
    object QrBoletosDILeituAtu: TFloatField
      FieldName = 'LeituAtu'
      DisplayFormat = '#,##0.000;-#,##0.000; '
    end
    object QrBoletosDIPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBoletosDIValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrBoletosDIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBoletosDITMU: TSmallintField
      FieldName = 'TMU'
    end
  end
  object DsBoletosR: TDataSource
    DataSet = QrBoletosR
    Left = 444
    Top = 208
  end
  object QrBoletosR: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM _boletos_r_')
    Left = 416
    Top = 208
    object QrBoletosRLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrBoletosRTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 2
    end
    object QrBoletosRRegistros: TIntegerField
      FieldName = 'Registros'
    end
    object QrBoletosRAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
