unit ComandaFat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkValUsu, DMKpnfsConversao,
  {$IfNDef semNFSe_0000}  NFSe_PF_0201, {$EndIf}
  UnDmkEnums, UnAppPF, DmkDAC_PF, dmkRadioGroup, UnAppEnums;

type
  TFmComandaFat = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    PnEdita: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdCliente: TDBEdit;
    DBEdNome: TDBEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label1: TLabel;
    EdControle: TdmkEdit;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    Label21: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    EdHoraFat: TdmkEdit;
    Label7: TLabel;
    EdCartEmis: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    EdNumNF: TdmkEdit;
    Label13: TLabel;
    EdSerNF: TdmkEdit;
    Label14: TLabel;
    SBCondicaoPG: TSpeedButton;
    SBCarteira: TSpeedButton;
    QrLoc: TmySQLQuery;
    SbNFSe: TSpeedButton;
    EDValVenda: TdmkEdit;
    Label15: TLabel;
    EdValServi: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdValDesco: TdmkEdit;
    EdValTotal: TdmkEdit;
    Label12: TLabel;
    EdValBruto: TdmkEdit;
    Label16: TLabel;
    CGCodHist: TdmkCheckGroup;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    Label17: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    SbConta: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValLocadChange(Sender: TObject);
    procedure EdValConsuChange(Sender: TObject);
    procedure EdValUsadoChange(Sender: TObject);
    procedure EdValServiChange(Sender: TObject);
    procedure EdValDescoChange(Sender: TObject);
    procedure SBCondicaoPGClick(Sender: TObject);
    procedure SBCarteiraClick(Sender: TObject);
    procedure SbNFSeClick(Sender: TObject);
    procedure EdNumNFChange(Sender: TObject);
    procedure EDValVendaChange(Sender: TObject);
    procedure SbContaClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
  public
    { Public declarations }
    FControle: Integer;
    //
    //FQrLoc,
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FEmpresa, (*FFilial,*) FCliente: Integer;
    FDataAbriu: TDateTime;
    //FEncerra: Boolean;
  end;

  var
  FmComandaFat: TFmComandaFat;

implementation

uses UnMyObjects, Module, ModuleFatura, UMySQLModule, Principal, UnPraz_PF,
  ModuleGeral, UnFinanceiroJan;

{$R *.DFM}

procedure TFmComandaFat.BtOKClick(Sender: TObject);
const
  TipoFat = tfatServico;
  FatID = VAR_FATID_7001;
  TipoFatura = TTipoFiscal.tfaMultiSV; // tfatServico; {TTipoFatura}
  FaturaDta = dfEncerramento; {TDataFatura}
  Financeiro = tfinCred; {TTipoFinanceiro}
var
  DtHrFat, DtHrBxa, SerNF: String;
  Codigo, Controle, NumNF, CondicaoPG, CartEmis, Conta: Integer;
  ValVenda, ValServi, ValDesco, ValBruto, ValTotal,
  //ValTotalFat: Double;
  ValorPago: Double;
var
  Entidade, FatNum, Cliente, IDDuplicata, NumeroNF, TipoCart, Represen: Integer;
  DataAbriu, DataEncer: TDateTime;
  SerieNF: String;
  CodHist: Integer;
begin
  //
  Codigo     := Geral.IMV(Geral.SoNumero_TT(DBEdCodigo.Text));
  Controle   := EdControle.ValueVariant;
  DtHrFat    := Geral.FDT(Trunc(TPDataFat.Date), 1) + ' ' + EdHoraFat.Text;
  ValVenda   := EdValVenda.ValueVariant;
  ValServi   := EdValServi.ValueVariant;
  ValDesco   := EdValDesco.ValueVariant;
  ValBruto   := EdValBruto.ValueVariant;
  ValTotal   := EdValTotal.ValueVariant;
  SerNF      := EdSerNF.Text;
  NumNF      := EdNumNF.ValueVariant;
  CondicaoPG := EdCondicaoPG.ValueVariant;
  CartEmis   := EdCartEmis.ValueVariant;
  //Conta      := DModG.QrParamsEmpCtaServico.Value;
  Conta      := EdConta.ValueVariant;
  CodHist    := CGCodHist.Value;
  //
  if MyObjects.FIC(CondicaoPG = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(CartEmis = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta,
    'Informe a conta do plano de contas!') then Exit;
  if ValTotal < 0.01 then
  begin
{
    if FEncerra then
    begin
      if Geral.MB_Pergunta('Valor inv�lido para faturamento!' + sLineBreak +
      'Deseja encerrar assim mesmo?') <> ID_YES then
        Exit;
    end else
}
    begin
      Geral.MB_Aviso('Valor inv�lido para faturamento!');
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if ValTotal >= 0.01 then
    begin
      Controle := UMyMod.BPGS1I32('comandafat', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'comandafat', False, [
      'Codigo', 'DtHrFat', 'ValServi',
      'ValDesco', 'ValTotal', 'SerNF',
      'NumNF', 'CondicaoPG', 'CartEmis',
      'ValVenda', 'ValBruto', 'CodHist',
      'Genero'], [
      'Controle'], [
      Codigo, DtHrFat, ValServi,
      ValDesco, ValTotal, SerNF,
      NumNF, CondicaoPG, CartEmis,
      ValVenda, ValBruto, CodHist,
      Conta], [
      Controle], True) then
      begin
        Entidade     := FEmpresa;
        FatNum       := Controle;
        Cliente      := FCliente;
        IDDuplicata  := Controle;
        NumeroNF     := NumNF;
        CartEmis     := CartEmis;
        TipoCart     := DmFatura.QrCartEmisTipo.Value;
        CondicaoPG   := CondicaoPG;
        Represen     := 0;
        DataAbriu    := FDataAbriu;
        DataEncer    := TPDataFat.Date;
        SerieNF      := SerNF;
        //
        DmFatura.EmiteFaturas(Codigo, FatID, Entidade, FatID, FatNum, Cliente,
          DataAbriu, DataEncer, IDDuplicata, NumeroNF, SerieNF, CartEmis,
          TipoCart,  Conta, CondicaoPG, Represen, TipoFatura, FaturaDta,
          Financeiro, ValTotal,
          // 2020-10-27
          (*Associada*) 0, (*AFP_Sit*)0, (*AFP_Per*)0.00, (*ASS_CtaFaturas*)0,
          (*ASS_IDDuplicata*)0, (*DataFatPreDef*)0, (*AskIns*)False,
          (*ExcluiLct*)True, CodHist);
        end;
    end;
    //
    AppPF.CalculaTotaisComandaFat(Codigo, (*FEncerra*)False, (*DtHrFat*)'');
    //
    if FQrIts <> nil then
    begin
      UMyMod.AbreQuery(FQrIts, Dmod.MyDB);
      FQrIts.Locate('Controle', Controle, []);
    end;
    //
    FControle := Controle;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmComandaFat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComandaFat.CalculaValores();
var
  ValVenda, ValServi, ValDesco, ValBruto,
  ValTotal: Double;
begin
  ValVenda := EdValVenda.ValueVariant;
  ValServi := EdValServi.ValueVariant;
  ValDesco := EdValDesco.ValueVariant;
  ValBruto := ValVenda + ValServi;
  ValTotal := ValBruto - ValDesco;
  //
  EdValBruto.ValueVariant := ValBruto;
  EdValTotal.ValueVariant := ValTotal;
end;

procedure TFmComandaFat.EdNumNFChange(Sender: TObject);
begin
  SbNFSe.Enabled := EdNumNF.ValueVariant = 0;
end;

procedure TFmComandaFat.EdValConsuChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmComandaFat.EdValDescoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmComandaFat.EdValServiChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmComandaFat.EdValLocadChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmComandaFat.EdValUsadoChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmComandaFat.EDValVendaChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmComandaFat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmComandaFat.FormCreate(Sender: TObject);
const
  Colunas = 4;
  Default = 0;
  FirstAll = False;
begin
  ImgTipo.SQLType := stLok;
  //
{
  MyObjects.ConfiguraCheckGroup(CGCodHist, sCodHistTextos, Colunas, Default, FirstAll);
(*
  sCodHistTextos: array[0..MaxCodHist] of string  = (
  CO_TXT_codhistAdiantamentoDeLocacao  ,
  CO_TXT_codhistValorParcialDeLocacao  ,
  CO_TXT_codhistRenovacaoDeLocacao     ,
  CO_TXT_codhistQuitacaoDeLocacao      ,
  CO_TXT_codhistFrete                  ,
  CO_TXT_codhistVendaDeMercadoria      ,
  CO_TXT_codhistServicos               ,
  CO_TXT_codhistOutros                 //,
  );
*)
}
  CGCodHist.Value := 0;
  DmFatura.ReopenPediPrzCab();
  DmFatura.ReopenCartEmis(0, True);
  CBCondicaoPG.ListSource := DmFatura.DsPediPrzCab;
  CBCartEmis.ListSource := DmFatura.DsCartEmis;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
  'SELECT con.Codigo, con.Nome  ',
  'FROM contas con ',
  'WHERE con.Codigo>0 ',
  'AND con.Ativo = 1 ',
  'AND con.Credito = "V" ',
  'AND  ',
  '( ',
  '  Codigo=' + Geral.FF0(DModG.QrParamsEmpCtaProdVen.Value),
  '  OR ',
  '  Codigo=' + Geral.FF0(DModG.QrParamsEmpCtaServico.Value),
  '  OR ',
  '  Codigo=' + Geral.FF0(DModG.QrParamsEmpCtaFretPrest.Value),
  ') ',
  'ORDER BY con.Nome ',
  '']);
end;

procedure TFmComandaFat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmComandaFat.SBCarteiraClick(Sender: TObject);
begin
  FinanceiroJan.InsereEDefineCarteira(EdCartEmis.ValueVariant, EdCartEmis,
  CBCartEmis, DmFatura.QrCartEmis);
end;

procedure TFmComandaFat.SBCondicaoPGClick(Sender: TObject);
begin
  Praz_PF.InsereEDefinePediPrzCab(EdCondicaoPG.ValueVariant, EdCondicaoPG,
  CBCondicaoPG, DmFatura.QrPediPrzCab);
end;

procedure TFmComandaFat.SbContaClick(Sender: TObject);
begin
  FinanceiroJan.InsereEDefineConta(EdConta.ValueVariant, EdConta, CBConta,
    QrContas);
end;

procedure TFmComandaFat.SbNFSeClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  //Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, Tomador: Integer;
  Valor: Double;
  SerieNF: String;
  NumNF: Integer;
begin
{$IfNDef semNFSe_0000}
  Prestador := FFilial; //DmodG.QrFiliLogFilial.Value;
  Tomador   := FCliente;
  Valor     := EdValTotal.ValueVariant;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType, Prestador, Tomador, Intermediario,
    MeuServico, ItemListSrv, Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico,
    nil, Valor, SerieNF, NumNF, nil);
  //
  if NumNF <> 0  then
  begin
    EdSerNF.ValueVariant := SerieNF;
    EdNumNF.ValueVariant := NumNF;
  end;
{$Else}
  Geral.MB_Info('ERP sem NFSe habilitado!');
{$EndIf}
end;

end.
