unit ComandaIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmComandaIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DsEstoque: TDataSource;
    DsStqCenCad: TDataSource;
    QrEstoque: TMySQLQuery;
    QrEstoqueQtde: TFloatField;
    QrStqCenCad: TMySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    QrGraGruX: TMySQLQuery;
    DsGraGruX: TDataSource;
    QrPesq1: TMySQLQuery;
    QrPesq1Controle: TIntegerField;
    QrPesq1EAN13: TWideStringField;
    QrPesq2: TMySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq2EAN13: TWideStringField;
    QrItem: TMySQLQuery;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrItemMadeBy: TSmallintField;
    QrItemFracio: TSmallintField;
    QrItemHowBxaEstq: TSmallintField;
    QrItemGerBxaEstq: TSmallintField;
    QrItemprod_indTot: TSmallintField;
    QrCli: TMySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliNUMERO: TFloatField;
    QrCliUF: TFloatField;
    QrCliCEP: TFloatField;
    QrCliLograd: TFloatField;
    QrCliindIEDest: TSmallintField;
    QrCliL_Ativo: TSmallintField;
    QrFatPedCab: TMySQLQuery;
    QrFatPedCabEmpresa: TIntegerField;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabPedido: TIntegerField;
    QrFatPedCabCondicaoPG: TIntegerField;
    QrFatPedCabTabelaPrc: TIntegerField;
    QrFatPedCabMedDDReal: TFloatField;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedCabRegrFiscal: TIntegerField;
    QrFatPedCabNO_TabelaPrc: TWideStringField;
    QrFatPedCabTipMediaDD: TSmallintField;
    QrFatPedCabAFP_Sit: TSmallintField;
    QrFatPedCabAFP_Per: TFloatField;
    QrFatPedCabAssociada: TIntegerField;
    QrFatPedCabCU_PediVda: TIntegerField;
    QrFatPedCabAbertura: TDateTimeField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabFatSemEstq: TSmallintField;
    QrFatPedCabENCERROU_TXT: TWideStringField;
    QrFatPedCabEMP_CtaProdVen: TIntegerField;
    QrFatPedCabEMP_FaturaSeq: TSmallintField;
    QrFatPedCabEMP_FaturaSep: TWideStringField;
    QrFatPedCabEMP_FaturaDta: TSmallintField;
    QrFatPedCabEMP_TxtProdVen: TWideStringField;
    QrFatPedCabEMP_FILIAL: TIntegerField;
    QrFatPedCabASS_CtaProdVen: TIntegerField;
    QrFatPedCabASS_FaturaSeq: TSmallintField;
    QrFatPedCabASS_FaturaSep: TWideStringField;
    QrFatPedCabASS_FaturaDta: TSmallintField;
    QrFatPedCabASS_TxtProdVen: TWideStringField;
    QrFatPedCabASS_NO_UF: TWideStringField;
    QrFatPedCabASS_CO_UF: TIntegerField;
    QrFatPedCabASS_FILIAL: TIntegerField;
    QrFatPedCabCliente: TIntegerField;
    QrFatPedCabSerieDesfe: TIntegerField;
    QrFatPedCabNFDesfeita: TIntegerField;
    QrFatPedCabPEDIDO_TXT: TWideStringField;
    QrFatPedCabID_Pedido: TIntegerField;
    QrFatPedCabJurosMes: TFloatField;
    QrFatPedCabNOMEFISREGCAD: TWideStringField;
    QrFatPedCabEMP_UF: TIntegerField;
    QrFatPedCabPedidoCli: TWideStringField;
    PainelDados: TPanel;
    SBCadastro: TSpeedButton;
    Label7: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    LaQtdeLocacao: TLabel;
    Label8: TLabel;
    SbDesconto: TSpeedButton;
    Label13: TLabel;
    Label14: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    DBEdQtde: TDBEdit;
    EdQtdeItem: TdmkEdit;
    EdValrItem: TdmkEdit;
    EdPrecoUnit: TdmkEdit;
    DBEdNCM: TDBEdit;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Edprod_vFrete: TdmkEdit;
    Edprod_vSeg: TdmkEdit;
    Edprod_vOutro: TdmkEdit;
    Edprod_vDesc: TdmkEdit;
    EdPercDesco: TdmkEdit;
    EdValrBrut: TdmkEdit;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXNO_UnidMed: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXNO_PrdGrupTip: TWideStringField;
    EdNome: TdmkEdit;
    Label16: TLabel;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXReferencia: TWideStringField;
    QrGraGruXEAN13: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruVal: TMySQLQuery;
    QrGraGruValCustoPreco: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdEAN13Change(Sender: TObject);
    procedure EdEAN13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEAN13Redefinido(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdQtdeItemRedefinido(Sender: TObject);
    procedure EdPrecoUnitRedefinido(Sender: TObject);
    procedure EdPercDescoRedefinido(Sender: TObject);
    procedure Edprod_vFreteRedefinido(Sender: TObject);
    procedure Edprod_vSegRedefinido(Sender: TObject);
    procedure Edprod_vOutroRedefinido(Sender: TObject);
    procedure SbDescontoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    //
    procedure ReopenIts(Controle: Integer);
    procedure ReopenEstoque();
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorEAN13(Limpa: Boolean);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    //
    procedure AtualizaValorTotal();
  public
    { Public declarations }
    FCodigo, FControle, FEmpresa, FGraCusPrc: Integer;
    FAlterou: Boolean;
    //
    //FEmpresa: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmComandaIts: TFmComandaIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  MeuDBUses, UnGrade_Jan, GetValor, UnAppPF;

{$R *.DFM}

procedure TFmComandaIts.AtualizaValorTotal();
var
  Quantidade, PrcUni, ValorTotal: Double;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  PercDesco, ValorBruto: Double;
begin
  Quantidade     := EdQtdeItem.ValueVariant;
  PrcUni         := EdPrecoUnit.ValueVariant;
  ValorBruto     := Quantidade * PrcUni;
  PercDesco      := EdPercDesco.ValueVariant;
  prod_vDesc     := ValorBruto * (PercDesco / 100);
  if prod_vDesc < 0 then
    prod_vDesc := 0;
  //
  prod_vFrete    := Edprod_vFrete.ValueVariant;
  prod_vSeg      := Edprod_vSeg.ValueVariant;
  prod_vOutro    := Edprod_vOutro.ValueVariant;
  //
  ValorTotal     := ValorBruto + (prod_vFrete + prod_vSeg - prod_vDesc + prod_vOutro);
  ValorBruto     := ValorTotal + prod_vDesc;
  //
  Edprod_vDesc.ValueVariant := prod_vDesc;
  EdValrItem.ValueVariant   := ValorTotal;
  EdValrBrut.ValueVariant   := ValorBruto;
end;

procedure TFmComandaIts.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Produto, Unidade: Integer;
  QtdeItem, PrecoUnit, ValrBrut, PercDesc, prod_vFrete, prod_vSeg, prod_vDesc,
  prod_vOutro, ValrItem(*, ValrCstm, ValrItCu*): Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := FControle;
  Produto        := EdGraGruX.ValueVariant;
  Unidade        := QrGraGruXUnidMed.Value;
  QtdeItem       := EdQtdeItem.ValueVariant;
  PrecoUnit      := EdPrecoUnit.ValueVariant;
  ValrBrut       := EdValrBrut.ValueVariant;
  PercDesc       := EdPercDesco.ValueVariant;
  prod_vFrete    := Edprod_vFrete.ValueVariant;
  prod_vSeg      := Edprod_vSeg.ValueVariant;
  prod_vDesc     := Edprod_vDesc.ValueVariant;
  prod_vOutro    := Edprod_vOutro.ValueVariant;
  ValrItem       := EdValrItem.ValueVariant;
  //ValrCstm       := EdValrCstm.ValueVariant;
  //ValrItCu       := EdValrItCu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Produto = 0, EdGraGruX, 'Informe o produto!') then
    Exit;
  if MyObjects.FIC(QtdeItem = 0, EdQtdeItem, 'Informe a quantidade!') then
    Exit;
  if PrecoUnit <= 0 then
    if Geral.MB_Pergunta('Confirma o pre�o $ "' + EdPrecoUnit.Text + '" ?') <> ID_YES then
      Exit;
  //
  Controle := UMyMod.BPGS1I32('comandaits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'comandaits', False, [
  'Codigo', 'Produto', 'Unidade',
  'QtdeItem', 'PrecoUnit', 'ValrBrut',
  'PercDesc', 'prod_vFrete', 'prod_vSeg',
  'prod_vDesc', 'prod_vOutro', 'ValrItem',
  (*'ValrCstm', 'ValrItCu',*) 'Nome'], [
  'Controle'], [
  Codigo, Produto, Unidade,
  QtdeItem, PrecoUnit, ValrBrut,
  PercDesc, prod_vFrete, prod_vSeg,
  prod_vDesc, prod_vOutro, ValrItem,
  (*ValrCstm, ValrItCu,*) Nome], [
  Controle], True) then
  begin
    AppPF.CalculaTotaisComandaIts(Codigo, Controle);
    //AppPF.CalculaTotaisComandaCab(Codigo);
    ReopenIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := Null;
      EdQtdeItem.ValueVariant  := 0.000;
      EdPrecoUnit.ValueVariant := 0.00;
      EdPercDesco.ValueVariant := 0.0000000000;
      EdNome.ValueVariant      := '';
      //
      EdGraGruX.SetFocus;
    end else Close;
  end;
end;

procedure TFmComandaIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComandaIts.EdEAN13Change(Sender: TObject);
begin
(*
  if EdEAN13.Focused then
    PesquisaPorEAN13(False);
*)
end;

procedure TFmComandaIts.EdEAN13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
(*
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
*)
end;

procedure TFmComandaIts.EdEAN13Redefinido(Sender: TObject);
var
  EAN13: String;
begin
(*
  EAN13 := Geral.SoNumero_TT(StringReplace(EdEAN13.ValueVariant, #$D#$A, '', [rfReplaceAll]));
  if EAN13 <> EdEAN13.ValueVariant then
    EdEAN13.ValueVariant := EAN13;
*)
end;

procedure TFmComandaIts.EdGraGruXChange(Sender: TObject);
begin
(*
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
  if EdGraGruX.ValueVariant <> 0 then
    DBEdNCM.DataField := 'NCM'
  else
    DBEdNCM.DataField := '';
*)
end;

procedure TFmComandaIts.EdGraGruXRedefinido(Sender: TObject);
var
  GraGruX: Integer;
begin
(*
  if ImgTipo.SQLType = stIns then
  begin
    EdPrcUni.ValueVariant := QrGraGXVendItemValr.Value;
  end;
  ReopenEstoque();
*)
  GraGruX := EdGraGruX.ValueVariant;
  if (GraGruX <> 0) and (not FCriando) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, Dmod.MyDB, [
    'SELECT CustoPreco ',
    'FROM gragruval',
    'WHERE Entidade=' + Geral.FF0(FEmpresa),
    'AND GraCusPrc=' + Geral.FF0(FGraCusPrc),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    EdPrecoUnit.ValueVariant := QrGraGruValCustoPreco.Value;
  end;
end;

procedure TFmComandaIts.EdPercDescoRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaIts.EdPrecoUnitRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaIts.Edprod_vFreteRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaIts.Edprod_vOutroRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaIts.Edprod_vSegRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaIts.EdQtdeItemRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaIts.EdReferenciaChange(Sender: TObject);
begin
(*
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
*)
end;

procedure TFmComandaIts.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
(*
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
*)
end;

procedure TFmComandaIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmComandaIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCriando        := True;
  //
  FAlterou     := False;
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmComandaIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmComandaIts.FormShow(Sender: TObject);
begin
  FCriando := False;
end;

procedure TFmComandaIts.PesquisaPorEAN13(Limpa: Boolean);
var
  EAN13: String;
begin
  Exit;
(*
  ////////////////////////////////////////////////////////////////////////////
  EAN13 := EdEAN13.Text;
  if Length(EAN13) = 13 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.EAN13 ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
    'WHERE ggx.EAN13="' + EAN13 + '"' ,
    'AND NOT (ggo.GraGruX IS NULL)',
    'AND ggo.Aplicacao <> 0 ',
    //
    'AND ggx.Ativo=1 ',
    '']);
    //
    //Geral.MB_Teste(QrPesq1.SQL.Text);
    //
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
    begin
      if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      begin
        EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      end;
      if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
        CBGraGruX.KeyValue := QrPesq1Controle.Value;
      //
      if QrEstoqueQtde.Value > 0 then
        DBEdQtde.SetFocus
      else
        Geral.MB_Info('Mercadoria sem estoque positivo!');
    end else if Limpa then
    begin
      EdEAN13.ValueVariant := '';
      EdReferencia.ValueVariant := '';
    end;
  end;
*)
end;

procedure TFmComandaIts.PesquisaPorGraGruX();
begin
  Exit;
(*
  ////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    //
    if EdEAN13.ValueVariant <> QrPesq2EAN13.Value then
      EdEAN13.ValueVariant := QrPesq2EAN13.Value;
    //
  end else
  begin
    EdReferencia.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
*)
end;

procedure TFmComandaIts.PesquisaPorReferencia(Limpa: Boolean);
begin
  Exit;
  ////////////////////////////////////////////////////////////////////////////
end;

procedure TFmComandaIts.ReopenIts(Controle: Integer);
begin
  if FQrCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrCab, Dmod.MyDB);
    if FQrIts <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
      //
      if Controle <> 0 then
        FQrIts.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmComandaIts.ReopenEstoque();
var
  GraGruX, StqCenCad: Integer;
begin
{
  GraGruX    := EdGraGruX.ValueVariant;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstoque, Dmod.MyDB, [
  'SELECT SUM(Qtde * Baixa) Qtde ',
  'FROM stqmovitsa ',
  'WHERE Ativo=1 ',
  'AND ValiStq <> 101',
  'AND GraGruX=' + Geral.FF0(GraGruX),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND StqCenCad=' + Geral.FF0(StqCenCad),
  '']);
}
end;

procedure TFmComandaIts.SBCadastroClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  VAR_CADASTRO := 0;
  Nivel1 := QrGraGruXGraGru1.Value;
  Grade_Jan.MostraFormGraGruN(Nivel1);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGruX, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  //EdGraGruX.SetFocus;
end;

procedure TFmComandaIts.SbDescontoClick(Sender: TObject);
  function ObtemDesconto(var Desco: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Desco         := 0;
    Result        := False;
    CasasDecimais := 2;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Desconto', 'Informe o valor de  desconto: ',
    0, ResVar) then
    begin
      Desco := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  prod_vDesc, PercDesco, ValorBruto: Double;
begin
  ValorBruto := EdValrBrut.ValueVariant;
  if ValorBruto > 0 then
  begin
    if ObtemDesconto(prod_vDesc) then
    begin
      PercDesco := (prod_vDesc / ValorBruto) * 100;
      if PercDesco > 100 then
        PercDesco := 100;
      EdPercDesco.ValueVariant := PercDesco;
    end;
  end else
    Geral.MB_Aviso('Valor bruto n�o definido!');
end;

end.
