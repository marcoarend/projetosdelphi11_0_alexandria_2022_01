unit ComandaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, frxClass, frxDBSet;

type
  TFmComandaCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrComandaCab: TMySQLQuery;
    DsComandaCab: TDataSource;
    QrComandaIts: TMySQLQuery;
    DsComandaIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    EdEmpresa: TdmkEdit;
    Label3: TLabel;
    Label18: TLabel;
    TPDtHrPedido: TdmkEditDateTimePicker;
    EdDtHrPedido: TdmkEdit;
    TPDtHrLiProd: TdmkEditDateTimePicker;
    EdDtHrLiProd: TdmkEdit;
    Label4: TLabel;
    TPDtHrRetird: TdmkEditDateTimePicker;
    EdDtHrRetird: TdmkEdit;
    Label5: TLabel;
    Label15: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SBCliente: TSpeedButton;
    QrEntidades: TMySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesAtivo: TSmallintField;
    DsEntidades: TDataSource;
    QrComandaCabNO_Cliente: TWideStringField;
    QrComandaCabCodigo: TIntegerField;
    QrComandaCabEmpresa: TIntegerField;
    QrComandaCabDtHrPedido: TDateTimeField;
    QrComandaCabDtHrLiProd: TDateTimeField;
    QrComandaCabDtHrRetird: TDateTimeField;
    QrComandaCabCliente: TIntegerField;
    QrComandaCabQtdeTotal: TFloatField;
    QrComandaCabValrTotal: TFloatField;
    QrComandaCabValrNPag: TFloatField;
    QrComandaCabValrPago: TFloatField;
    QrComandaCabNome: TWideStringField;
    QrComandaCabLk: TIntegerField;
    QrComandaCabDataCad: TDateField;
    QrComandaCabDataAlt: TDateField;
    QrComandaCabUserCad: TIntegerField;
    QrComandaCabUserAlt: TIntegerField;
    QrComandaCabAlterWeb: TSmallintField;
    QrComandaCabAWServerID: TIntegerField;
    QrComandaCabAWStatSinc: TSmallintField;
    QrComandaCabAtivo: TSmallintField;
    QrComandaItsCodigo: TIntegerField;
    QrComandaItsControle: TIntegerField;
    QrComandaItsProduto: TIntegerField;
    QrComandaItsUnidade: TIntegerField;
    QrComandaItsQtdeItem: TFloatField;
    QrComandaItsPrecoUnit: TFloatField;
    QrComandaItsValrBrut: TFloatField;
    QrComandaItsPercDesc: TFloatField;
    QrComandaItsprod_vFrete: TFloatField;
    QrComandaItsprod_vSeg: TFloatField;
    QrComandaItsprod_vDesc: TFloatField;
    QrComandaItsprod_vOutro: TFloatField;
    QrComandaItsValrItem: TFloatField;
    QrComandaItsValrCstm: TFloatField;
    QrComandaItsValrItCu: TFloatField;
    QrComandaItsNome: TWideStringField;
    QrComandaCabDtHrLiProd_TXT: TWideStringField;
    QrComandaCabDtHrRetird_TXT: TWideStringField;
    QrGraCusPrc: TMySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcCodUsu: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    EdGraCusPrc: TdmkEditCB;
    Label13: TLabel;
    CBGraCusPrc: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrComandaCabGraCusPrc: TIntegerField;
    QrComandaCabNO_GraCusPrc: TWideStringField;
    QrComandaCabBrutTotal: TFloatField;
    QrComandaCus: TMySQLQuery;
    DsComandaCus: TDataSource;
    QrComandaCusCodigo: TIntegerField;
    QrComandaCusControle: TIntegerField;
    QrComandaCusConta: TIntegerField;
    QrComandaCusProduto: TIntegerField;
    QrComandaCusUnidade: TIntegerField;
    QrComandaCusQtdeCstm: TFloatField;
    QrComandaCusPrecoUnit: TFloatField;
    QrComandaCusValrBrut: TFloatField;
    QrComandaCusPercDesc: TFloatField;
    QrComandaCusprod_vFrete: TFloatField;
    QrComandaCusprod_vSeg: TFloatField;
    QrComandaCusprod_vDesc: TFloatField;
    QrComandaCusprod_vOutro: TFloatField;
    QrComandaCusValrCstm: TFloatField;
    QrComandaCusNome: TWideStringField;
    BtCus: TBitBtn;
    PMCus: TPopupMenu;
    CusInclui1: TMenuItem;
    CusAltera1: TMenuItem;
    CusExclui1: TMenuItem;
    QrComandaItsNO_GG1: TWideStringField;
    QrComandaCusNO_GG1: TWideStringField;
    QrComandaCusNO_PRD_TAM_COR: TWideStringField;
    QrComandaCusNO_UnidMed: TWideStringField;
    QrComandaCusSigla: TWideStringField;
    QrComandaCusGraGru1: TIntegerField;
    QrComandaCusNCM: TWideStringField;
    QrComandaCusReferencia: TWideStringField;
    QrComandaCusEAN13: TWideStringField;
    QrComandaCusUnidMed: TIntegerField;
    QrComandaCusPrdGrupTip: TIntegerField;
    QrComandaCusNO_PrdGrupTip: TWideStringField;
    QrComandaItsNO_PRD_TAM_COR: TWideStringField;
    QrComandaItsNO_UnidMed: TWideStringField;
    QrComandaItsSigla: TWideStringField;
    QrComandaItsGraGru1: TIntegerField;
    QrComandaItsNCM: TWideStringField;
    QrComandaItsReferencia: TWideStringField;
    QrComandaItsEAN13: TWideStringField;
    QrComandaItsUnidMed: TIntegerField;
    QrComandaItsPrdGrupTip: TIntegerField;
    QrComandaItsNO_PrdGrupTip: TWideStringField;
    QrComandaItsBrutItCu: TFloatField;
    QrComandaItsBrutCstm: TFloatField;
    BtSvc: TBitBtn;
    PMSvc: TPopupMenu;
    SvcInclui1: TMenuItem;
    SvcAltera1: TMenuItem;
    SvcExclui1: TMenuItem;
    QrComandaSvc: TMySQLQuery;
    QrComandaSvcNO_GG1: TWideStringField;
    QrComandaSvcNO_PRD_TAM_COR: TWideStringField;
    QrComandaSvcGraGru1: TIntegerField;
    QrComandaSvcNCM: TWideStringField;
    QrComandaSvcReferencia: TWideStringField;
    QrComandaSvcEAN13: TWideStringField;
    QrComandaSvcUnidMed: TIntegerField;
    QrComandaSvcPrdGrupTip: TIntegerField;
    QrComandaSvcNO_PrdGrupTip: TWideStringField;
    QrComandaSvcCodigo: TIntegerField;
    QrComandaSvcControle: TIntegerField;
    QrComandaSvcProduto: TIntegerField;
    QrComandaSvcQtdeItem: TFloatField;
    QrComandaSvcPrecoUnit: TFloatField;
    QrComandaSvcValrBrut: TFloatField;
    QrComandaSvcPercDesc: TFloatField;
    QrComandaSvcprod_vFrete: TFloatField;
    QrComandaSvcprod_vSeg: TFloatField;
    QrComandaSvcprod_vDesc: TFloatField;
    QrComandaSvcprod_vOutro: TFloatField;
    QrComandaSvcValrItem: TFloatField;
    QrComandaSvcNome: TWideStringField;
    DsComandaSvc: TDataSource;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    Label20: TLabel;
    DBEdit12: TDBEdit;
    Label19: TLabel;
    DBEdit11: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtFat: TBitBtn;
    PMFat: TPopupMenu;
    FaturaParcial1: TMenuItem;
    QrComandaFat: TMySQLQuery;
    DsComandaFat: TDataSource;
    QrLctFatRef: TMySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    DsLctFatRef: TDataSource;
    Visualizarbloquetos1: TMenuItem;
    DBEdit13: TDBEdit;
    Label25: TLabel;
    QrComandaCabValrAPag: TFloatField;
    QrComandaFatCodigo: TIntegerField;
    QrComandaFatControle: TIntegerField;
    QrComandaFatDtHrFat: TDateTimeField;
    QrComandaFatValVenda: TFloatField;
    QrComandaFatValServi: TFloatField;
    QrComandaFatValFrete: TFloatField;
    QrComandaFatValDesco: TFloatField;
    QrComandaFatValBruto: TFloatField;
    QrComandaFatValTotal: TFloatField;
    QrComandaFatSerNF: TWideStringField;
    QrComandaFatNumNF: TIntegerField;
    QrComandaFatCondicaoPG: TIntegerField;
    QrComandaFatCartEmis: TIntegerField;
    QrComandaFatCodHist: TIntegerField;
    QrComandaFatGenero: TIntegerField;
    PCMovimento: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Splitter5: TSplitter;
    GroupBox4: TGroupBox;
    DBGrid3: TDBGrid;
    GroupBox7: TGroupBox;
    DBGLctFatRef: TDBGrid;
    PnProdutos: TPanel;
    Label21: TLabel;
    Splitter2: TSplitter;
    DBGIts: TDBGrid;
    Panel8: TPanel;
    Label23: TLabel;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    Panel9: TPanel;
    Label24: TLabel;
    DBGCus: TDBGrid;
    Excluifaturamentoselecionado1: TMenuItem;
    QrComandaFatNO_CondicaoPG: TWideStringField;
    QrComandaFatNo_CartEmis: TWideStringField;
    QrComandaFatNo_Genero: TWideStringField;
    frxCMD_PEDID_001_01: TfrxReport;
    frxDsComandaCab: TfrxDBDataset;
    frxDsComandaIts: TfrxDBDataset;
    QrComandaItsValrDesc: TFloatField;
    frxDsComandaCus: TfrxDBDataset;
    QrComandaItsItCuDesc: TFloatField;
    frxDsComandaSvc: TfrxDBDataset;
    QrComandaSvcValrDesc: TFloatField;
    QrComandaCusValrDesc: TFloatField;
    Label26: TLabel;
    EdCondPgCombin: TdmkEditCB;
    CBCondPgCombin: TdmkDBLookupComboBox;
    SBCondicaoPG: TSpeedButton;
    QrComandaCabCondPgCombin: TIntegerField;
    QrComandaCabNO_CondPgCombin: TWideStringField;
    N1: TMenuItem;
    N2: TMenuItem;
    Descontoparatodositens1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrComandaCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrComandaCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrComandaCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrComandaCabBeforeClose(DataSet: TDataSet);
    procedure QrComandaItsAfterScroll(DataSet: TDataSet);
    procedure QrComandaItsBeforeClose(DataSet: TDataSet);
    procedure BtCusClick(Sender: TObject);
    procedure PMCusPopup(Sender: TObject);
    procedure CusInclui1Click(Sender: TObject);
    procedure CusAltera1Click(Sender: TObject);
    procedure CusExclui1Click(Sender: TObject);
    procedure BtSvcClick(Sender: TObject);
    procedure SvcInclui1Click(Sender: TObject);
    procedure SvcAltera1Click(Sender: TObject);
    procedure SvcExclui1Click(Sender: TObject);
    procedure PMSvcPopup(Sender: TObject);
    procedure FaturaParcial1Click(Sender: TObject);
    procedure BtFatClick(Sender: TObject);
    procedure QrComandaFatAfterScroll(DataSet: TDataSet);
    procedure QrComandaFatBeforeClose(DataSet: TDataSet);
    procedure Visualizarbloquetos1Click(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure Excluifaturamentoselecionado1Click(Sender: TObject);
    procedure frxCMD_PEDID_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure SBCondicaoPGClick(Sender: TObject);
    procedure Descontoparatodositens1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormComandaIts(SQLType: TSQLType);
    procedure MostraFormComandaCus(SQLType: TSQLType);
    procedure MostraFormComandaSvc(SQLType: TSQLType);
    procedure MostraFormComandaFat();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenComandaIts(Controle: Integer);
    procedure ReopenComandaCus(Conta: Integer);
    procedure ReopenComandaSvc(Controle: Integer);
    procedure ReopenComandaFat(Controle: Integer);
    procedure ReopenLctFatRef(Conta: Integer);

  end;

var
  FmComandaCab: TFmComandaCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnGrl_Geral,
  ComandaIts, ComandaCus, ComandaSvc, ComandaFat, ModuleFatura, ModuleGeral,
  ModuleFin, UnAppPF, GetValor;

{$R *.DFM}

const
  PCMov_PageIndex_Vend = 0;
  PCMov_PageIndex_Fatu = 1;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmComandaCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmComandaCab.MostraFormComandaCus(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmComandaCus, FmComandaCus, afmoNegarComAviso) then
  begin
    FmComandaCus.ImgTipo.SQLType := SQLType;
    FmComandaCus.FCodigo         := QrComandaCabCodigo.Value;
    FmComandaCus.FControle       := QrComandaItsControle.Value;
    FmComandaCus.FEmpresa        := QrComandaCabEmpresa.Value;
    FmComandaCus.FGraCusPrc      := QrComandaCabGraCusPrc.Value;
    //
    FmComandaCus.FQrCab := QrComandaCab;
    FmComandaCus.FDsCab := DsComandaCab;
    FmComandaCus.FQrIts := QrComandaIts;
    FmComandaCus.FQrCus := QrComandaCus;
    //
    if SQLType = stIns then
      //
    else
    begin
      FmComandaCus.FConta                     := QrComandaCusConta.Value;
      //FmComandaCus.EdControle.ValueVariant  := QrComandaCusControle.Value;
      //
      FmComandaCus.EdGraGruX.ValueVariant     := QrComandaCusProduto.Value;
      FmComandaCus.CBGraGruX.KeyValue         := QrComandaCusProduto.Value;
      FmComandaCus.EdQtdeCstm.ValueVariant    := QrComandaCusQtdeCstm.Value;
      FmComandaCus.EdPrecoUnit.ValueVariant   := QrComandaCusPrecoUnit.Value;
      FmComandaCus.EdValrBrut.ValueVariant    := QrComandaCusValrBrut.Value;
      FmComandaCus.EdPercDesco.ValueVariant   := QrComandaCusPercDesc.Value;
      FmComandaCus.Edprod_vFrete.ValueVariant := QrComandaCusprod_vFrete.Value;
      FmComandaCus.Edprod_vSeg.ValueVariant   := QrComandaCusprod_vSeg.Value;
      FmComandaCus.Edprod_vDesc.ValueVariant  := QrComandaCusprod_vDesc.Value;
      FmComandaCus.Edprod_vOutro.ValueVariant := QrComandaCusprod_vOutro.Value;
      FmComandaCus.EdValrCstm.ValueVariant    := QrComandaCusValrCstm.Value;
      FmComandaCus.EdNome.Text                := QrComandaCusNome.Value;
    end;
    FmComandaCus.ShowModal;
    FmComandaCus.Destroy;
  end;
end;

procedure TFmComandaCab.MostraFormComandaFat();
const
  SQLType = stIns;
var
  Codigo, Controle: Integer;
begin
  Codigo := QrComandaCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmComandaFat, FmComandaFat, afmoNegarComAviso) then
  begin
    FmComandaFat.ImgTipo.SQLType        := SQLType;
    FmComandaFat.DBEdCodigo.DataSource  := DsComandaCab;
    FmComandaFat.DBEdCliente.DataSource := DsComandaCab;
    FmComandaFat.DBEdNome.DataSource    := DsComandaCab;
    FmComandaFat.FQrCab                 := QrComandaCab;
    FmComandaFat.FDsCab                 := DsComandaCab;
    FmComandaFat.FQrIts                 := QrComandaFat;
    FmComandaFat.FEmpresa               := QrComandaCabEmpresa.Value;
    //FmComandaFat.FFilial                := QrComandaCabFilial.Value;
    FmComandaFat.FCliente               := QrComandaCabCliente.Value;
    FmComandaFat.FDataAbriu             := QrComandaCabDtHrPedido.Value;
    //FmComandaFat.FEncerra               := Encerra;
    //
    if SQLType = stIns then
    begin
      FmComandaFat.TPDataFat.Date := Trunc(Date);
      FmComandaFat.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
      FmComandaFat.EdCondicaoPG.ValueVariant := QrComandaCabCondPgCombin.Value;
      FmComandaFat.CBCondicaoPG.KeyValue     := QrComandaCabCondPgCombin.Value;
    end else
    begin
      FmComandaFat.EdControle.ValueVariant   := QrComandaFatControle.Value;
      FmComandaFat.EdCondicaoPG.ValueVariant := QrComandaFatCondicaoPG.Value;
      FmComandaFat.CBCondicaoPG.KeyValue     := QrComandaFatCondicaoPG.Value;
      FmComandaFat.EdCartEmis.ValueVariant   := QrComandaFatCartEmis.Value;
      FmComandaFat.CBCartEmis.KeyValue       := QrComandaFatCartEmis.Value;
      FmComandaFat.EdValVenda.ValueVariant   := QrComandaFatValVenda.Value;
      FmComandaFat.EdValServi.ValueVariant   := QrComandaFatValServi.Value;
      FmComandaFat.EdValDesco.ValueVariant   := QrComandaFatValDesco.Value;
      FmComandaFat.EdValTotal.ValueVariant   := QrComandaFatValTotal.Value;
      FmComandaFat.EdSerNF.ValueVariant      := QrComandaFatSerNF.Value;
      FmComandaFat.EdNumNF.ValueVariant      := QrComandaFatNumNF.Value;
      FmComandaFat.TPDataFat.Date            := Trunc(QrComandaFatDtHrFat.Value);
      FmComandaFat.EdHoraFat.Text            := FormatDateTime('hh:nn:ss', QrComandaFatDtHrFat.Value);
      FmComandaFat.CGCodHist.Value           := QrComandaFatCodHist.Value;
    end;
    FmComandaFat.ShowModal;
    Controle := FmComandaFat.FControle;
    FmComandaFat.Destroy;
    //
    LocCod(Codigo, Codigo);
    QrComandaFat.Locate('Controle', Controle, []);
    //PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  end;
end;

procedure TFmComandaCab.MostraFormComandaIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmComandaIts, FmComandaIts, afmoNegarComAviso) then
  begin
    FmComandaIts.ImgTipo.SQLType := SQLType;
    FmComandaIts.FCodigo         := QrComandaCabCodigo.Value;
    FmComandaIts.FEmpresa        := QrComandaCabEmpresa.Value;
    FmComandaIts.FGraCusPrc      := QrComandaCabGraCusPrc.Value;
    //
    FmComandaIts.FQrCab := QrComandaCab;
    FmComandaIts.FDsCab := DsComandaCab;
    FmComandaIts.FQrIts := QrComandaIts;
    //
    if SQLType = stIns then
      //
    else
    begin
      FmComandaIts.FControle                  := QrComandaItsControle.Value;
      //FmComandaIts.EdControle.ValueVariant  := QrComandaItsControle.Value;
      //
      FmComandaIts.EdGraGruX.ValueVariant     := QrComandaItsProduto.Value;
      FmComandaIts.CBGraGruX.KeyValue         := QrComandaItsProduto.Value;
      FmComandaIts.EdQtdeItem.ValueVariant    := QrComandaItsQtdeItem.Value;
      FmComandaIts.EdPrecoUnit.ValueVariant   := QrComandaItsPrecoUnit.Value;
      FmComandaIts.EdValrBrut.ValueVariant    := QrComandaItsValrBrut.Value;
      FmComandaIts.EdPercDesco.ValueVariant   := QrComandaItsPercDesc.Value;
      FmComandaIts.Edprod_vFrete.ValueVariant := QrComandaItsprod_vFrete.Value;
      FmComandaIts.Edprod_vSeg.ValueVariant   := QrComandaItsprod_vSeg.Value;
      FmComandaIts.Edprod_vDesc.ValueVariant  := QrComandaItsprod_vDesc.Value;
      FmComandaIts.Edprod_vOutro.ValueVariant := QrComandaItsprod_vOutro.Value;
      FmComandaIts.EdValrItem.ValueVariant    := QrComandaItsValrItem.Value;
      FmComandaIts.EdNome.Text                := QrComandaItsNome.Value;
    end;
    FmComandaIts.ShowModal;
    FmComandaIts.Destroy;
  end;
end;

procedure TFmComandaCab.MostraFormComandaSvc(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmComandaSvc, FmComandaSvc, afmoNegarComAviso) then
  begin
    FmComandaSvc.ImgTipo.SQLType := SQLType;
    FmComandaSvc.FCodigo         := QrComandaCabCodigo.Value;
    FmComandaSvc.FEmpresa        := QrComandaCabEmpresa.Value;
    FmComandaSvc.FGraCusPrc      := QrComandaCabGraCusPrc.Value;
    //
    FmComandaSvc.FQrCab := QrComandaCab;
    FmComandaSvc.FDsCab := DsComandaCab;
    FmComandaSvc.FQrSvc := QrComandaSvc;
    //
    if SQLType = stIns then
      //
    else
    begin
      FmComandaSvc.FControle                  := QrComandaSvcControle.Value;
      //FmComandaSvc.EdControle.ValueVariant  := QrComandaSvcControle.Value;
      //
      FmComandaSvc.EdGraGruX.ValueVariant     := QrComandaSvcProduto.Value;
      FmComandaSvc.CBGraGruX.KeyValue         := QrComandaSvcProduto.Value;
      FmComandaSvc.EdQtdeItem.ValueVariant    := QrComandaSvcQtdeItem.Value;
      FmComandaSvc.EdPrecoUnit.ValueVariant   := QrComandaSvcPrecoUnit.Value;
      FmComandaSvc.EdValrBrut.ValueVariant    := QrComandaSvcValrBrut.Value;
      FmComandaSvc.EdPercDesco.ValueVariant   := QrComandaSvcPercDesc.Value;
      FmComandaSvc.Edprod_vFrete.ValueVariant := QrComandaSvcprod_vFrete.Value;
      FmComandaSvc.Edprod_vSeg.ValueVariant   := QrComandaSvcprod_vSeg.Value;
      FmComandaSvc.Edprod_vDesc.ValueVariant  := QrComandaSvcprod_vDesc.Value;
      FmComandaSvc.Edprod_vOutro.ValueVariant := QrComandaSvcprod_vOutro.Value;
      FmComandaSvc.EdValrItem.ValueVariant    := QrComandaSvcValrItem.Value;
      FmComandaSvc.EdNome.Text                := QrComandaSvcNome.Value;
    end;
    FmComandaSvc.ShowModal;
    FmComandaSvc.Destroy;
  end;
end;

procedure TFmComandaCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrComandaCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrComandaCab, QrComandaIts);
end;

procedure TFmComandaCab.PMCusPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(CusInclui1, QrComandaIts);
  MyObjects.HabilitaMenuItemItsUpd(CusAltera1, QrComandaCus);
  MyObjects.HabilitaMenuItemItsDel(CusExclui1, QrComandaCus);
end;

procedure TFmComandaCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrComandaCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrComandaIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrComandaIts);
end;

procedure TFmComandaCab.PMSvcPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(SvcInclui1, QrComandaCab);
  MyObjects.HabilitaMenuItemItsUpd(SvcAltera1, QrComandaSvc);
  MyObjects.HabilitaMenuItemItsDel(SvcExclui1, QrComandaSvc);
end;

procedure TFmComandaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrComandaCabCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmComandaCab.Visualizarbloquetos1Click(Sender: TObject);
begin
  {$IfNDef SBLQ}
  UBloquetos.MostraBloGeren(0, VAR_FATID_7001, 0, QrComandaCabCodigo.Value,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  //
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappBloquetos);
  {$EndIf}
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmComandaCab.DefParams;
begin
  VAR_GOTOTABELA := 'comandacab';
  VAR_GOTOMYSQLTABLE := QrComandaCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('IF(cab.DtHrLiProd < 2, "", DATE_FORMAT(cab.DtHrLiProd,');
  VAR_SQLx.Add('"%d/%m/%Y %H:%i")) DtHrLiProd_TXT,');
  VAR_SQLx.Add('IF(cab.DtHrRetird < 2, "", DATE_FORMAT(cab.DtHrRetird,');
  VAR_SQLx.Add('"%d/%m/%Y %H:%i")) DtHrRetird_TXT,');
  VAR_SQLx.Add('gcp.Nome NO_GraCusPrc, ppc.Nome NO_CondPgCombin, cab.*  ');
  VAR_SQLx.Add('FROM comandacab cab ');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=cab.Cliente ');
  VAR_SQLx.Add('LEFT JOIN gracusprc gcp ON gcp.Codigo=cab.GraCusPrc ');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondPgCombin ');
  VAR_SQLx.Add('WHERE cab.Codigo<>0 ');
    //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmComandaCab.Descontoparatodositens1Click(Sender: TObject);
  procedure AtualizaValorTotal_Servico(PercDesco: Double);
  var
    Quantidade, PrcUni, ValorTotal: Double;
    prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
    ValorBruto: Double;
    Controle: Integer;
  begin
    Quantidade     := QrComandaSvcQtdeItem.Value;
    PrcUni         := QrComandaSvcPrecoUnit.Value;
    ValorBruto     := Quantidade * PrcUni;
    //PercDesco      := QrComandaSvcPercDesc.Value;
    prod_vDesc     := ValorBruto * (PercDesco / 100);
    if prod_vDesc < 0 then
      prod_vDesc := 0;
    //
    prod_vFrete    := QrComandaSvcprod_vFrete.Value;
    prod_vSeg      := QrComandaSvcprod_vSeg.Value;
    prod_vOutro    := QrComandaSvcprod_vOutro.Value;
    //
    ValorTotal     := ValorBruto + (prod_vFrete + prod_vSeg - prod_vDesc + prod_vOutro);
    ValorBruto     := ValorTotal + prod_vDesc;
    //
    Controle       := QrComandaSvcControle.Value;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandasvc', False, [
    (*'Codigo', 'Produto', 'QtdeItem',
    'PrecoUnit',*) 'ValrBrut', 'PercDesc',
    (*'prod_vFrete', 'prod_vSeg',*) 'prod_vDesc',
    (*'prod_vOutro',*) 'ValrItem'(*, 'Nome'*)], [
    'Controle'], [
    (*Codigo, Produto, QtdeItem,
    PrecoUnit,*) ValorBruto, PercDesco,
    (*prod_vFrete, prod_vSeg,*) prod_vDesc,
    (*prod_vOutro,*) ValorTotal(*, Nome*)], [
    Controle], True);
  end;
  procedure AtualizaValorTotal_Itens(PercDesco: Double);
  var
    Quantidade, PrcUni, ValorTotal: Double;
    prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
    ValorBruto: Double;
    Controle: Integer;
  begin
    Quantidade     := QrComandaItsQtdeItem.Value;
    PrcUni         := QrComandaItsPrecoUnit.Value;
    ValorBruto     := Quantidade * PrcUni;
    //PercDesco      := EdPercDesco.ValueVariant;
    prod_vDesc     := ValorBruto * (PercDesco / 100);
    if prod_vDesc < 0 then
      prod_vDesc := 0;
    //
    prod_vFrete    := QrComandaItsprod_vFrete.Value;
    prod_vSeg      := QrComandaItsprod_vSeg.Value;
    prod_vOutro    := QrComandaItsprod_vOutro.Value;
    //
    ValorTotal     := ValorBruto + (prod_vFrete + prod_vSeg - prod_vDesc + prod_vOutro);
    ValorBruto     := ValorTotal + prod_vDesc;
    //
    Controle       := QrComandaItsControle.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandaits', False, [
    (*'Codigo', 'Produto', 'QtdeItem',
    'PrecoUnit',*) 'ValrBrut', 'PercDesc',
    (*'prod_vFrete', 'prod_vSeg',*) 'prod_vDesc',
    (*'prod_vOutro',*) 'ValrItem'(*, 'Nome'*)], [
    'Controle'], [
    (*Codigo, Produto, QtdeItem,
    PrecoUnit,*) ValorBruto, PercDesco,
    (*prod_vFrete, prod_vSeg,*) prod_vDesc,
    (*prod_vOutro,*) ValorTotal(*, Nome*)], [
    Controle], True);
  end;

  procedure AtualizaValorTotal_Custom(PercDesco: Double);
  var
    Quantidade, PrcUni, ValorTotal: Double;
    prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
    ValorBruto: Double;
    Conta: Integer;
  begin
    Quantidade     := QrComandaCusQtdeCstm.Value;
    PrcUni         := QrComandaCusPrecoUnit.Value;
    ValorBruto     := Quantidade * PrcUni;
    //PercDesco      := EdPercDesco.Value;
    prod_vDesc     := ValorBruto * (PercDesco / 100);
    if prod_vDesc < 0 then
      prod_vDesc := 0;
    //
    prod_vFrete    := QrComandaCusprod_vFrete.Value;
    prod_vSeg      := QrComandaCusprod_vSeg.Value;
    prod_vOutro    := QrComandaCusprod_vOutro.Value;
    //
    ValorTotal     := ValorBruto + (prod_vFrete + prod_vSeg - prod_vDesc + prod_vOutro);
    ValorBruto     := ValorTotal + prod_vDesc;
    //
    //
    Conta       := QrComandaCusConta.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandacus', False, [
    (*'Codigo', 'Produto', 'QtdeItem',
    'PrecoUnit',*) 'ValrBrut', 'PercDesc',
    (*'prod_vFrete', 'prod_vSeg',*) 'prod_vDesc',
    (*'prod_vOutro',*) 'ValrCstm'(*, 'Nome'*)], [
    'Conta'], [
    (*Codigo, Produto, QtdeItem,
    PrecoUnit,*) ValorBruto, PercDesco,
    (*prod_vFrete, prod_vSeg,*) prod_vDesc,
    (*prod_vOutro,*) ValorTotal(*, Nome*)], [
    Conta], True);
  end;

var
  Desco: Variant;
  PercDesco: Double;
  Codigo: Integer;
begin
  Codigo := QrComandaCabCodigo.Value;
  Desco := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, Desco, 10, 0,
  '', '', True, 'Desconto', 'Informe o Desconto: ', 0, Desco) then
  begin
    Screen.Cursor := crHourGlass;
    try
      PercDesco := Desco;
      QrComandaSvc.First;
      while not QrComandaSvc.Eof do
      begin
        AtualizaValorTotal_Servico(PercDesco);
        QrComandaSvc.Next;
      end;
      AppPF.CalculaTotaisComandaCab(Codigo);
      //
      QrComandaIts.First;
      while not QrComandaIts.Eof do
      begin
        QrComandaCus.First;
        while not QrComandaCus.Eof do
        begin
          AtualizaValorTotal_Custom(PercDesco);
          QrComandaCus.Next;
        end;
        //
        AtualizaValorTotal_Itens(PercDesco);
        AppPF.CalculaTotaisComandaIts(Codigo, QrComandaItsControle.Value);
        QrComandaIts.Next;
      end;
      AppPF.CalculaTotaisComandaCab(Codigo);
      LocCod(QrComandaCabCodigo.Value, Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmComandaCab.Excluifaturamentoselecionado1Click(Sender: TObject);
var
  Codigo, Filial, Quitados: Integer;
  TabLctA: String;
begin
  if MyObjects.FIC(QrComandaFatSerNF.Value <> '', nil, 'Exclus�o abortada!' + sLineBreak +
    'Motivo: J� foi emitida um Nota Fiscal para este faturamento!') then Exit;
  //
  if MyObjects.FIC((QrComandaFat.State = dsInactive) or (QrComandaFat.RecordCount = 0),
    nil, 'N�o h� nenhum faturamento para ser exclu�do!') then Exit;
  //
  Filial  := DmFatura.ObtemFilialDeEntidade(QrComandaCabEmpresa.Value);
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  Codigo  := QrComandaCabCodigo.Value;
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT COUNT(Controle) Itens ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_7001),
  'AND FatNum=' + Geral.FF0(QrComandaFatControle.Value),
  'AND (Sit > 0 OR Compensado > 2) ',
  '']);
*)
  //
  Quitados := DModFin.FaturamentosQuitados(
    TabLctA, VAR_FATID_7001, QrComandaFatControle.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MensagemBox('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
  {$IfNDef SBLQ}
    if UBloquetos.DesfazerBoletosFatID(QrLoc, Dmod.MyDB, VAR_FATID_7001,
      QrComandaCabCodigo.Value, TabLctA) then
  {$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappBloquetos);
  {$EndIf}
    begin
      Screen.Cursor := crHourGlass;
      try
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM ' + TabLctA,
          'WHERE FatID=' + Geral.FF0(VAR_FATID_7001),
          'AND FatNum=' + Geral.FF0(QrComandaFatControle.Value),
          '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM lctfatref',
          'WHERE Controle=' + Geral.FF0(QrComandaFatControle.Value),
          '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'DELETE FROM comandaFat ',
          'WHERE Controle=' + Geral.FF0(QrComandaFatControle.Value),
          '']);
        //
{
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandacab', False, [
          'ValorTot', 'DtHrBxa'], ['Codigo'], [0, '0000-00-00 00:00:00'], [Codigo], True);
}
        //
        ReopenComandaFat(0);
        LocCod(Codigo, Codigo);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    //
    AppPF.CalculaTotaisComandaFat(Codigo, False(*Encerra*), (*DtHrFat*)'');
    LocCod(Codigo, Codigo);
    //QrComandaFat.Locate('Controle', Controle, []);
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  end;
end;

procedure TFmComandaCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormComandaIts(stUpd);
end;

procedure TFmComandaCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmComandaCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmComandaCab.CusAltera1Click(Sender: TObject);
begin
  MostraFormComandaCus(stUpd);
end;

procedure TFmComandaCab.CusExclui1Click(Sender: TObject);
var
  Codigo, Controle, Conta: Integer;
begin
  Codigo   := QrComandaCabCodigo.Value;
  Controle := QrComandaItsControle.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Comandacus', 'Conta', QrComandaCusConta.Value, Dmod.MyDB) = ID_YES then
  begin
    AppPF.CalculaTotaisComandaCab(Codigo);
    //
    Conta := GOTOy.LocalizaPriorNextIntQr(QrComandaCus,
      QrComandaCusConta, QrComandaCusConta.Value);
    LocCod(Codigo, Codigo);
    ReopenComandaIts(Controle);
    ReopenComandaCus(Conta);
  end;
end;

procedure TFmComandaCab.CusInclui1Click(Sender: TObject);
begin
  MostraFormComandaCus(stIns);
end;

procedure TFmComandaCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmComandaCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Codigo := QrComandaCabCodigo.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ComandaIts', 'Controle', QrComandaItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    AppPF.CalculaTotaisComandaCab(Codigo);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrComandaIts,
      QrComandaItsControle, QrComandaItsControle.Value);
    LocCod(Codigo, Codigo);
    ReopenComandaIts(Controle);
  end;
end;

procedure TFmComandaCab.ReopenComandaCus(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrComandaCus, Dmod.MyDB, [
  'SELECT gg1.Nome NO_GG1, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, med.Nome NO_UnidMed, med.Sigla, ggx.GraGru1, gg1.NCM, ',
  'gg1.Referencia, ggx.EAN13, gg1.UnidMed, gg1.PrdGrupTip, ',
  'pgt.Nome NO_PrdGrupTip, ',
  '(cus.ValrBrut - cus.ValrCstm) ValrDesc, ',
  'cus.*',
  ' ',
  'FROM comandacus cus',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cus.Produto',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  '',
  'WHERE cus.Controle=' + Geral.FF0(QrComandaItsControle.Value),
  '']);
  //
  QrComandaCus.Locate('Conta', Conta, []);
end;

procedure TFmComandaCab.ReopenComandaFat(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrComandaFat, Dmod.MyDB, [
  'SELECT fat.*, ppc.Nome NO_CondicaoPG, ',
  'car.Nome No_CartEmis, cta.Nome No_Genero',
  'FROM comandafat fat ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=fat.CondicaoPG',
  'LEFT JOIN carteiras car ON car.Codigo=fat.CartEmis',
  'LEFT JOIN contas    cta ON cta.Codigo=fat.Genero',
  'WHERE fat.Codigo=' + Geral.FF0(QrComandaCabCodigo.Value),
  '']);
  //
  QrComandaFat.Locate('Controle', Controle, []);
end;

procedure TFmComandaCab.ReopenComandaIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrComandaIts, Dmod.MyDB, [
  'SELECT gg1.Nome NO_GG1, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, med.Nome NO_UnidMed, med.Sigla, ggx.GraGru1, gg1.NCM, ',
  'gg1.Referencia, ggx.EAN13, gg1.UnidMed, gg1.PrdGrupTip, ',
  'pgt.Nome NO_PrdGrupTip, ',
  '(its.ValrBrut - its.ValrItem) ValrDesc, ',
  '(its.BrutItCu - its.ValrItCu) ItCuDesc, ',
  'its.*',
  ' ',
  'FROM comandaits its',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=its.Produto',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
  '',
  'WHERE its.Codigo=' + Geral.FF0(QrComandaCabCodigo.Value),
  '']);
  //
  QrComandaIts.Locate('Controle', Controle, []);
end;


procedure TFmComandaCab.ReopenComandaSvc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrComandaSvc, Dmod.MyDB, [
  'SELECT gg1.Nome NO_GG1, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ggx.GraGru1, gg1.NCM, ',
  'gg1.Referencia, ggx.EAN13, gg1.UnidMed, gg1.PrdGrupTip, ',
  'pgt.Nome NO_PrdGrupTip, ',
  '(svc.ValrBrut - svc.ValrItem) ValrDesc, ',
  'svc.*',
  ' ',
  'FROM comandasvc svc',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=Svc.Produto',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  '',
  'WHERE Svc.Codigo=' + Geral.FF0(QrComandaCabCodigo.Value),
  '']);
  //
  QrComandaSvc.Locate('Controle', Controle, []);
end;

procedure TFmComandaCab.ReopenLctFatRef(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(QrComandaFatControle.Value),
  'AND FatID IN(0, '+ Geral.FF0(VAR_FATID_7001) + ') ',
  '']);
  //
  QrLctFatRef.Locate('Conta', Conta, []);
end;

procedure TFmComandaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmComandaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmComandaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmComandaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmComandaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmComandaCab.SvcAltera1Click(Sender: TObject);
begin
  MostraFormComandaSvc(stUpd);
end;

procedure TFmComandaCab.SvcExclui1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrComandaCabCodigo.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Comandasvc', 'Controle', QrComandaSvcControle.Value, Dmod.MyDB) = ID_YES then
  begin
    AppPF.CalculaTotaisComandaCab(Codigo);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrComandaSvc,
      QrComandaSvcControle, QrComandaSvcControle.Value);
    LocCod(Codigo, Codigo);
    ReopenComandaSvc(Controle);
  end;
end;

procedure TFmComandaCab.SvcInclui1Click(Sender: TObject);
begin
  MostraFormComandaSvc(stIns);
end;

procedure TFmComandaCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComandaCab.BtSvcClick(Sender: TObject);
begin
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
  MyObjects.MostraPopUpDeBotao(PMSvc, BtSvc);
end;

procedure TFmComandaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrComandaCabCodigo.Value;
  Close;
end;

procedure TFmComandaCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormComandaIts(stIns);
end;

procedure TFmComandaCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrComandaCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'comandacab');
end;

procedure TFmComandaCab.BtConfirmaClick(Sender: TObject);
var
  DtHrPedido, DtHrLiProd, DtHrRetird, Nome: String;
  Codigo, Empresa, Cliente, GraCusPrc, CondPgCombin: Integer;
  //QtdeTotal, ValrTotal, ValrNPag, ValrPago, ValrAPag: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  DtHrPedido     := Geral.FDT_TP_Ed(TPDtHrPedido.Date, EdDtHrPedido.Text);
  DtHrLiProd     := Geral.FDT_TP_Ed(TPDtHrLiProd.Date, EdDtHrLiProd.Text);
  DtHrRetird     := Geral.FDT_TP_Ed(TPDtHrRetird.Date, EdDtHrRetird.Text);
  Cliente        := EdCliente.ValueVariant;
  //QtdeTotal      := ;
  //ValrTotal      := ;
  //ValrNPag       := ;
  //ValrPago       := ;
  //ValrAPag       := ;
  Nome           := EdNome.ValueVariant;
  GraCusPrc      := Geral.IMV(EdGraCusPrc.Text);
  CondPgCombin   := EdCondPgCombin.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina o Empresa!') then Exit;
  if MyObjects.FIC(TPDtHrPedido.Date < 2, TPDtHrPedido, 'Defina a data do pedido!') then Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Defina o Cliente!') then Exit;
  if MyObjects.FIC(GraCusPrc = 0, EdGraCusPrc, 'Defina a lista de pre�os!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('comandacab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'comandacab', False, [
  'Empresa', 'DtHrPedido', 'DtHrLiProd',
  'DtHrRetird', 'Cliente', (*'QtdeTotal',
  'ValrTotal', 'ValrNPag', 'ValrPago',
  'ValrAPag',*) 'Nome', 'GraCusPrc',
  'CondPgCombin'], [
  'Codigo'], [
  Empresa, DtHrPedido, DtHrLiProd,
  DtHrRetird, Cliente, (*QtdeTotal,
  ValrTotal, ValrNPag, ValrPago,
  ValrAPag,*) Nome, GraCusPrc,
  CondPgCombin], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmComandaCab.BtCusClick(Sender: TObject);
begin
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
  MyObjects.MostraPopUpDeBotao(PMCus, BtCus);
end;

procedure TFmComandaCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'comandacab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'comandacab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmComandaCab.BtFatClick(Sender: TObject);
begin
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
  //
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmComandaCab.BtItsClick(Sender: TObject);
begin
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmComandaCab.BtCabClick(Sender: TObject);
begin
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmComandaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType    := stLok;
  GBEdita.Align      := alClient;
  PCMovimento.Align := alClient;
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Vend;
  CriaOForm;
  FSeq := 0;
  DmFatura.ReopenPediPrzCab();
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraCusPrc, Dmod.MyDB);
  CBCondPgCombin.ListSource := DmFatura.DsPediPrzCab;
end;

procedure TFmComandaCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrComandaCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmComandaCab.SBCondicaoPGClick(Sender: TObject);
begin
//
end;

procedure TFmComandaCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxCMD_PEDID_001_01, [
  frxDsComandaCab,
  frxDsComandaIts,
  frxDsComandaCus,
  frxDsComandaSvc
  ]);
  //
  MyObjects.frxMostra(frxCMD_PEDID_001_01,
    'Comanda ' + Geral.FF0(QrComandaCabCodigo.Value))
end;

procedure TFmComandaCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmComandaCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrComandaCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmComandaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmComandaCab.QrComandaCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmComandaCab.QrComandaCabAfterScroll(DataSet: TDataSet);
begin
  ReopenComandaIts(0);
  ReopenComandaSvc(0);
  ReopenComandaFat(0);
end;

procedure TFmComandaCab.FaturaParcial1Click(Sender: TObject);
begin
  MostraFormComandaFat();
  PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
end;

procedure TFmComandaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrComandaCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmComandaCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrComandaCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'comandacab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmComandaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmComandaCab.frxCMD_PEDID_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := VAR_LIB_EMPRESAS_NOME
  else
end;

procedure TFmComandaCab.Gerabloqueto1Click(Sender: TObject);
var
  Entidade, Filial: Integer;
  TabLctA: String;
begin
  {$IfNDef SBLQ}
  try
    Screen.Cursor         := crHourGlass;
    Gerabloqueto1.Enabled := False;
    //
    Entidade := QrComandaCabEmpresa.Value;
    Filial   := DmFatura.ObtemFilialDeEntidade(Entidade);
    TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    //
    UBloquetos.BloquetosParcelasByFatNum(Entidade, VAR_FATID_7001,
      QrComandaFatControle.Value, QrComandaCabCodigo.Value, TabLctA, True, PB1);
    PCMovimento.ActivePageIndex := PCMov_PageIndex_Fatu;
    //
  finally
    Gerabloqueto1.Enabled := True;
    Screen.Cursor         := crDefault;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappBloquetos);
  {$EndIf}
end;

procedure TFmComandaCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrComandaCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'comandacab');
  //
  EdNome.ValueVariant := '';
  EdEmpresa.ValueVariant := -11;
  TPDtHrPedido.Date := Now();
  TPDtHrLiProd.Date := 0;
  TPDtHrRetird.Date := 0;
end;

procedure TFmComandaCab.QrComandaCabBeforeClose(
  DataSet: TDataSet);
begin
  QrComandaIts.Close;
  QrComandaSvc.Close;
  QrComandaFat.Close;
end;

procedure TFmComandaCab.QrComandaCabBeforeOpen(DataSet: TDataSet);
begin
  QrComandaCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmComandaCab.QrComandaFatAfterScroll(DataSet: TDataSet);
begin
  ReopenLctFatRef(0);
end;

procedure TFmComandaCab.QrComandaFatBeforeClose(DataSet: TDataSet);
begin
  QrLctFatRef.Close;
end;

procedure TFmComandaCab.QrComandaItsAfterScroll(DataSet: TDataSet);
begin
  ReopenComandaCus(0);
end;

procedure TFmComandaCab.QrComandaItsBeforeClose(DataSet: TDataSet);
begin
  QrComandaCus.Close;
end;

end.

