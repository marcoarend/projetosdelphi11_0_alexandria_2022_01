unit ComandaSvc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmComandaSvc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TMySQLQuery;
    DsGraGruX: TDataSource;
    PainelDados: TPanel;
    SBCadastro: TSpeedButton;
    Label7: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    LaQtdeLocacao: TLabel;
    SbDesconto: TSpeedButton;
    Label13: TLabel;
    Label14: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdQtdeItem: TdmkEdit;
    EdValrItem: TdmkEdit;
    EdPrecoUnit: TdmkEdit;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Edprod_vFrete: TdmkEdit;
    Edprod_vSeg: TdmkEdit;
    Edprod_vOutro: TdmkEdit;
    Edprod_vDesc: TdmkEdit;
    EdPercDesco: TdmkEdit;
    EdValrBrut: TdmkEdit;
    QrGraGruXNO_GG1: TWideStringField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXNO_UnidMed: TWideStringField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXUnidMed: TIntegerField;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrGraGruXNO_PrdGrupTip: TWideStringField;
    EdNome: TdmkEdit;
    Label16: TLabel;
    QrGraGruXNCM: TWideStringField;
    QrGraGruXReferencia: TWideStringField;
    QrGraGruXEAN13: TWideStringField;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruVal: TMySQLQuery;
    QrGraGruValCustoPreco: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBCadastroClick(Sender: TObject);
    procedure EdEAN13Change(Sender: TObject);
    procedure EdEAN13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEAN13Redefinido(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdQtdeItemRedefinido(Sender: TObject);
    procedure EdPrecoUnitRedefinido(Sender: TObject);
    procedure EdPercDescoRedefinido(Sender: TObject);
    procedure Edprod_vFreteRedefinido(Sender: TObject);
    procedure Edprod_vSegRedefinido(Sender: TObject);
    procedure Edprod_vOutroRedefinido(Sender: TObject);
    procedure SbDescontoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    //
    procedure ReopenSvc(Controle: Integer);
    procedure PesquisaPorGraGruX();
    procedure PesquisaPorEAN13(Limpa: Boolean);
    procedure PesquisaPorReferencia(Limpa: Boolean);
    //
    procedure AtualizaValorTotal();
  public
    { Public declarations }
    FCodigo, FControle, FEmpresa, FGraCusPrc: Integer;
    FAlterou: Boolean;
    //
    //FEmpresa: Integer;
    FQrCab, FQrSvc: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmComandaSvc: TFmComandaSvc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  MeuDBUses, UnGrade_Jan, GetValor, UnAppPF;

{$R *.DFM}

procedure TFmComandaSvc.AtualizaValorTotal();
var
  Quantidade, PrcUni, ValorTotal: Double;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  PercDesco, ValorBruto: Double;
begin
  Quantidade     := EdQtdeItem.ValueVariant;
  PrcUni         := EdPrecoUnit.ValueVariant;
  ValorBruto     := Quantidade * PrcUni;
  PercDesco      := EdPercDesco.ValueVariant;
  prod_vDesc     := ValorBruto * (PercDesco / 100);
  if prod_vDesc < 0 then
    prod_vDesc := 0;
  //
  prod_vFrete    := Edprod_vFrete.ValueVariant;
  prod_vSeg      := Edprod_vSeg.ValueVariant;
  prod_vOutro    := Edprod_vOutro.ValueVariant;
  //
  ValorTotal     := ValorBruto + (prod_vFrete + prod_vSeg - prod_vDesc + prod_vOutro);
  ValorBruto     := ValorTotal + prod_vDesc;
  //
  Edprod_vDesc.ValueVariant := prod_vDesc;
  EdValrItem.ValueVariant   := ValorTotal;
  EdValrBrut.ValueVariant   := ValorBruto;
end;

procedure TFmComandaSvc.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Produto: Integer;
  QtdeItem, PrecoUnit, ValrBrut, PercDesc, prod_vFrete, prod_vSeg, prod_vDesc,
  prod_vOutro, ValrItem(*, ValrCstm, ValrItCu*): Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := FControle;
  Produto        := EdGraGruX.ValueVariant;
  QtdeItem       := EdQtdeItem.ValueVariant;
  PrecoUnit      := EdPrecoUnit.ValueVariant;
  ValrBrut       := EdValrBrut.ValueVariant;
  PercDesc       := EdPercDesco.ValueVariant;
  prod_vFrete    := Edprod_vFrete.ValueVariant;
  prod_vSeg      := Edprod_vSeg.ValueVariant;
  prod_vDesc     := Edprod_vDesc.ValueVariant;
  prod_vOutro    := Edprod_vOutro.ValueVariant;
  ValrItem       := EdValrItem.ValueVariant;
  //ValrCstm       := EdValrCstm.ValueVariant;
  //ValrItCu       := EdValrItCu.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Produto = 0, EdGraGruX, 'Informe o produto!') then
    Exit;
  if MyObjects.FIC(QtdeItem = 0, EdQtdeItem, 'Informe a quantidade!') then
    Exit;
  if PrecoUnit <= 0 then
    if Geral.MB_Pergunta('Confirma o pre�o $ "' + EdPrecoUnit.Text + '" ?') <> ID_YES then
      Exit;
  //
  Controle := UMyMod.BPGS1I32('comandasvc', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'comandasvc', False, [
  'Codigo', 'Produto',
  'QtdeItem', 'PrecoUnit', 'ValrBrut',
  'PercDesc', 'prod_vFrete', 'prod_vSeg',
  'prod_vDesc', 'prod_vOutro', 'ValrItem',
  (*'ValrCstm', 'ValrItCu',*) 'Nome'], [
  'Controle'], [
  Codigo, Produto,
  QtdeItem, PrecoUnit, ValrBrut,
  PercDesc, prod_vFrete, prod_vSeg,
  prod_vDesc, prod_vOutro, ValrItem,
  (*ValrCstm, ValrItCu,*) Nome], [
  Controle], True) then
  begin
    AppPF.CalculaTotaisComandaCab(Codigo);
    ReopenSvc(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdGraGruX.ValueVariant   := 0;
      CBGraGruX.KeyValue       := Null;
      EdQtdeItem.ValueVariant  := 0.000;
      EdPrecoUnit.ValueVariant := 0.00;
      EdPercDesco.ValueVariant := 0.0000000000;
      EdNome.ValueVariant      := '';
      //
      EdGraGruX.SetFocus;
    end else Close;
  end;
end;

procedure TFmComandaSvc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComandaSvc.EdEAN13Change(Sender: TObject);
begin
(*
  if EdEAN13.Focused then
    PesquisaPorEAN13(False);
*)
end;

procedure TFmComandaSvc.EdEAN13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
(*
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
*)
end;

procedure TFmComandaSvc.EdEAN13Redefinido(Sender: TObject);
var
  EAN13: String;
begin
(*
  EAN13 := Geral.SoNumero_TT(StringReplace(EdEAN13.ValueVariant, #$D#$A, '', [rfReplaceAll]));
  if EAN13 <> EdEAN13.ValueVariant then
    EdEAN13.ValueVariant := EAN13;
*)
end;

procedure TFmComandaSvc.EdGraGruXChange(Sender: TObject);
begin
(*
  if not EdReferencia.Focused then
    PesquisaPorGraGruX();
  if EdGraGruX.ValueVariant <> 0 then
    DBEdNCM.DataField := 'NCM'
  else
    DBEdNCM.DataField := '';
*)
end;

procedure TFmComandaSvc.EdGraGruXRedefinido(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if (GraGruX <> 0) and (not FCriando) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruVal, Dmod.MyDB, [
    'SELECT CustoPreco ',
    'FROM gragruval',
    'WHERE Entidade=' + Geral.FF0(FEmpresa),
    'AND GraCusPrc=' + Geral.FF0(FGraCusPrc),
    'AND GraGruX=' + Geral.FF0(GraGruX),
    '']);
    EdPrecoUnit.ValueVariant := QrGraGruValCustoPreco.Value;
  end;
end;

procedure TFmComandaSvc.EdPercDescoRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaSvc.EdPrecoUnitRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaSvc.Edprod_vFreteRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaSvc.Edprod_vOutroRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaSvc.Edprod_vSegRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaSvc.EdQtdeItemRedefinido(Sender: TObject);
begin
  AtualizaValorTotal();
end;

procedure TFmComandaSvc.EdReferenciaChange(Sender: TObject);
begin
(*
  if EdReferencia.Focused then
    PesquisaPorReferencia(False);
*)
end;

procedure TFmComandaSvc.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
(*
  if Key = VK_F7 then
  begin
    CBGraGruX.SetFocus;
    FmMeuDBUses.VirtualKey_F7('')
  end;
*)
end;

procedure TFmComandaSvc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmComandaSvc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCriando        := True;
  //
  FAlterou     := False;
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
end;

procedure TFmComandaSvc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmComandaSvc.FormShow(Sender: TObject);
begin
  FCriando := False;
end;

procedure TFmComandaSvc.PesquisaPorEAN13(Limpa: Boolean);
var
  EAN13: String;
begin
  Exit;
(*
  ////////////////////////////////////////////////////////////////////////////
  EAN13 := EdEAN13.Text;
  if Length(EAN13) = 13 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.EAN13 ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
    'WHERE ggx.EAN13="' + EAN13 + '"' ,
    'AND NOT (ggo.GraGruX IS NULL)',
    'AND ggo.Aplicacao <> 0 ',
    //
    'AND ggx.Ativo=1 ',
    '']);
    //
    //Geral.MB_Teste(QrPesq1.SQL.Text);
    //
    QrPesq1.Open;
    if QrPesq1.RecordCount > 0 then
    begin
      if EdGraGruX.ValueVariant <> QrPesq1Controle.Value then
      begin
        EdGraGruX.ValueVariant := QrPesq1Controle.Value;
      end;
      if CBGraGruX.KeyValue <> QrPesq1Controle.Value then
        CBGraGruX.KeyValue := QrPesq1Controle.Value;
      //
      if QrEstoqueQtde.Value > 0 then
        DBEdQtde.SetFocus
      else
        Geral.MB_Info('Mercadoria sem estoque positivo!');
    end else if Limpa then
    begin
      EdEAN13.ValueVariant := '';
      EdReferencia.ValueVariant := '';
    end;
  end;
*)
end;

procedure TFmComandaSvc.PesquisaPorGraGruX();
begin
  Exit;
(*
  ////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT gg1.Referencia, ggx.EAN13 ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxvend ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
    //
    if EdEAN13.ValueVariant <> QrPesq2EAN13.Value then
      EdEAN13.ValueVariant := QrPesq2EAN13.Value;
    //
  end else
  begin
    EdReferencia.ValueVariant := '';
    EdEAN13.ValueVariant := '';
  end;
*)
end;

procedure TFmComandaSvc.PesquisaPorReferencia(Limpa: Boolean);
begin
  Exit;
  ////////////////////////////////////////////////////////////////////////////
end;

procedure TFmComandaSvc.ReopenSvc(Controle: Integer);
begin
  if FQrCab <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrCab, Dmod.MyDB);
    if FQrSvc <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrSvc, Dmod.MyDB);
      //
      if Controle <> 0 then
        FQrSvc.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmComandaSvc.SBCadastroClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  VAR_CADASTRO := 0;
  Nivel1 := QrGraGruXGraGru1.Value;
  Grade_Jan.MostraFormGraGruN(Nivel1);
  UMyMod.SetaCodigoPesquisado(EdGraGruX, CBGraGruX, QrGraGruX, VAR_CADASTRO, 'Controle');
  PesquisaPorGraGruX();
  //EdGraGruX.SetFocus;
end;

procedure TFmComandaSvc.SbDescontoClick(Sender: TObject);
  function ObtemDesconto(var Desco: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Desco         := 0;
    Result        := False;
    CasasDecimais := 2;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'Desconto', 'Informe o valor de  desconto: ',
    0, ResVar) then
    begin
      Desco := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  prod_vDesc, PercDesco, ValorBruto: Double;
begin
  ValorBruto := EdValrBrut.ValueVariant;
  if ValorBruto > 0 then
  begin
    if ObtemDesconto(prod_vDesc) then
    begin
      PercDesco := (prod_vDesc / ValorBruto) * 100;
      if PercDesco > 100 then
        PercDesco := 100;
      EdPercDesco.ValueVariant := PercDesco;
    end;
  end else
    Geral.MB_Aviso('Valor bruto n�o definido!');
end;

end.
