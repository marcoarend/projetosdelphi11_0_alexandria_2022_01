object FmComandaSvc: TFmComandaSvc
  Left = 339
  Top = 185
  Caption = 'CMD-PEDID-004 :: Item de Servi'#231'o'
  ClientHeight = 408
  ClientWidth = 682
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 263
    Width = 682
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 682
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 634
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 586
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 189
        Height = 32
        Caption = 'Item de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 189
        Height = 32
        Caption = 'Item de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 189
        Height = 32
        Caption = 'Item de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 294
    Width = 682
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 678
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 338
    Width = 682
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 536
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 534
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 682
    Height = 215
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SBCadastro: TSpeedButton
      Left = 647
      Top = 34
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SBCadastroClick
    end
    object Label7: TLabel
      Left = 68
      Top = 18
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label1: TLabel
      Left = 10
      Top = 18
      Width = 48
      Height = 13
      Caption = 'Reduzido:'
    end
    object Label4: TLabel
      Left = 463
      Top = 117
      Width = 49
      Height = 13
      Caption = 'Valor item:'
      Enabled = False
    end
    object Label3: TLabel
      Left = 94
      Top = 58
      Width = 63
      Height = 13
      Caption = '$ Pre'#231'o unit.:'
    end
    object LaQtdeLocacao: TLabel
      Left = 10
      Top = 58
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object SbDesconto: TSpeedButton
      Left = 302
      Top = 74
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbDescontoClick
    end
    object Label13: TLabel
      Left = 198
      Top = 58
      Width = 60
      Height = 13
      Caption = '% Desconto:'
    end
    object Label14: TLabel
      Left = 327
      Top = 58
      Width = 91
      Height = 13
      Caption = 'Valor bruto servi'#231'o:'
      Enabled = False
    end
    object Label16: TLabel
      Left = 8
      Top = 170
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 68
      Top = 34
      Width = 577
      Height = 21
      KeyField = 'GraGruX'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 1
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pPrdTamCor
    end
    object EdGraGruX: TdmkEditCB
      Left = 10
      Top = 34
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraGruXChange
      OnRedefinido = EdGraGruXRedefinido
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdQtdeItem: TdmkEdit
      Left = 11
      Top = 74
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdQtdeItemRedefinido
    end
    object EdValrItem: TdmkEdit
      Left = 464
      Top = 133
      Width = 205
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPrecoUnit: TdmkEdit
      Left = 95
      Top = 74
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdPrecoUnitRedefinido
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 100
      Width = 445
      Height = 65
      Caption = ' Valores para NFC-e'
      TabOrder = 6
      object Panel3: TPanel
        Left = 2
        Top = 15
        Width = 441
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 3
          Top = 2
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label10: TLabel
          Left = 107
          Top = 2
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label12: TLabel
          Left = 315
          Top = 2
          Width = 112
          Height = 13
          Caption = '$ Outros (desp. acess.):'
        end
        object Label11: TLabel
          Left = 211
          Top = 2
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
          Enabled = False
        end
        object Edprod_vFrete: TdmkEdit
          Left = 4
          Top = 18
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = Edprod_vFreteRedefinido
        end
        object Edprod_vSeg: TdmkEdit
          Left = 108
          Top = 18
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = Edprod_vSegRedefinido
        end
        object Edprod_vOutro: TdmkEdit
          Left = 316
          Top = 18
          Width = 117
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = Edprod_vOutroRedefinido
        end
        object Edprod_vDesc: TdmkEdit
          Left = 212
          Top = 18
          Width = 100
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object EdPercDesco: TdmkEdit
      Left = 199
      Top = 74
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 10
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMax = '100'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdPercDescoRedefinido
    end
    object EdValrBrut: TdmkEdit
      Left = 328
      Top = 74
      Width = 113
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 8
      Top = 186
      Width = 661
      Height = 21
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX,  gg1.Nome NO_GG1, '
      'CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      'NO_PRD_TAM_COR, med.Nome NO_UnidMed, ggx.GraGru1, gg1.NCM, '
      'gg1.Referencia, ggx.EAN13,'
      'gg1.UnidMed, gg1.PrdGrupTip, pgt.Nome NO_PrdGrupTip'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed')
    Left = 488
    Top = 14
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXNO_PrdGrupTip: TWideStringField
      FieldName = 'NO_PrdGrupTip'
      Size = 60
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGruXEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 488
    Top = 62
  end
  object QrGraGruVal: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 236
    Top = 165
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
end
