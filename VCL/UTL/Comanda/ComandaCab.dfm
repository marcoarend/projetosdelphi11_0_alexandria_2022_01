object FmComandaCab: TFmComandaCab
  Left = 368
  Top = 194
  Caption = 'CMD-PEDID-001 :: Comanda'
  ClientHeight = 724
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 672
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 229
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 68
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 936
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label18: TLabel
        Left = 540
        Top = 104
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object Label4: TLabel
        Left = 692
        Top = 104
        Width = 84
        Height = 13
        Caption = 'Limite de entrega:'
      end
      object Label5: TLabel
        Left = 844
        Top = 104
        Width = 46
        Height = 13
        Caption = 'Entregue:'
      end
      object Label15: TLabel
        Left = 8
        Top = 56
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object SBCliente: TSpeedButton
        Left = 606
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
      end
      object Label13: TLabel
        Left = 630
        Top = 56
        Width = 75
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lista de pre'#231'os:'
      end
      object SpeedButton5: TSpeedButton
        Left = 970
        Top = 72
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Visible = False
      end
      object Label26: TLabel
        Left = 8
        Top = 104
        Width = 174
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento combinado:'
      end
      object SBCondicaoPG: TSpeedButton
        Left = 516
        Top = 120
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBCondicaoPGClick
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 32
        Width = 865
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 936
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-11'
        QryCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -11
        ValWarn = False
      end
      object TPDtHrPedido: TdmkEditDateTimePicker
        Left = 540
        Top = 120
        Width = 109
        Height = 21
        Date = 44196.000000000000000000
        Time = 44196.000000000000000000
        ShowCheckbox = True
        TabOrder = 9
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrPedido'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrPedido: TdmkEdit
        Left = 652
        Top = 120
        Width = 36
        Height = 21
        TabOrder = 10
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrPedido'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrLiProd: TdmkEditDateTimePicker
        Left = 692
        Top = 120
        Width = 109
        Height = 21
        Date = 44196.000000000000000000
        Time = 44196.000000000000000000
        ShowCheckbox = True
        TabOrder = 11
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrLiProd'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrLiProd: TdmkEdit
        Left = 804
        Top = 120
        Width = 36
        Height = 21
        TabOrder = 12
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrLiProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrRetird: TdmkEditDateTimePicker
        Left = 844
        Top = 120
        Width = 109
        Height = 21
        Date = 44196.000000000000000000
        Time = 44196.000000000000000000
        ShowCheckbox = True
        TabOrder = 13
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrRetird'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrRetird: TdmkEdit
        Left = 956
        Top = 120
        Width = 36
        Height = 21
        TabOrder = 14
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrRetird'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 64
        Top = 72
        Width = 541
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsEntidades
        TabOrder = 4
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGraCusPrc: TdmkEditCB
        Left = 630
        Top = 72
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraCusPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraCusPrc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraCusPrc: TdmkDBLookupComboBox
        Left = 700
        Top = 72
        Width = 269
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 6
        dmkEditCB = EdGraCusPrc
        QryCampo = 'GraCusPrc'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCondPgCombin: TdmkEditCB
        Left = 8
        Top = 120
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CondPgCombin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCondPgCombin
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCondPgCombin: TdmkDBLookupComboBox
        Left = 66
        Top = 120
        Width = 447
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 8
        dmkEditCB = EdCondPgCombin
        QryCampo = 'CondPgCombin'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 609
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 672
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 608
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 89
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 263
        Top = 15
        Width = 743
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 610
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 2
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Comanda'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtCus: TBitBtn
          Tag = 110
          Left = 246
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Custom.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCusClick
        end
        object BtSvc: TBitBtn
          Tag = 110
          Left = 368
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Servi'#231'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSvcClick
        end
        object BtFat: TBitBtn
          Tag = 110
          Left = 490
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fatura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtFatClick
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 161
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 0
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 0
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label8: TLabel
        Left = 12
        Top = 40
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label14: TLabel
        Left = 12
        Top = 80
        Width = 75
        Height = 13
        Caption = 'Lista de pre'#231'os:'
      end
      object Label16: TLabel
        Left = 12
        Top = 120
        Width = 81
        Height = 13
        Caption = 'Quantidade total:'
        FocusControl = DBEdit9
      end
      object Label22: TLabel
        Left = 152
        Top = 120
        Width = 77
        Height = 13
        Caption = 'Valor bruto total:'
        FocusControl = DBEdit14
      end
      object Label17: TLabel
        Left = 292
        Top = 120
        Width = 82
        Height = 13
        Caption = 'Valor l'#237'quido total'
        FocusControl = DBEdit10
      end
      object Label20: TLabel
        Left = 432
        Top = 120
        Width = 54
        Height = 13
        Caption = 'Valor pago:'
        FocusControl = DBEdit12
      end
      object Label19: TLabel
        Left = 572
        Top = 120
        Width = 65
        Height = 13
        Caption = 'Valor perdido:'
        FocusControl = DBEdit11
      end
      object Label10: TLabel
        Left = 544
        Top = 80
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object Label11: TLabel
        Left = 696
        Top = 80
        Width = 84
        Height = 13
        Caption = 'Limite de entrega:'
      end
      object Label12: TLabel
        Left = 848
        Top = 80
        Width = 46
        Height = 13
        Caption = 'Entregue:'
      end
      object Label6: TLabel
        Left = 936
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label25: TLabel
        Left = 712
        Top = 120
        Width = 77
        Height = 13
        Caption = 'Valor em aberto:'
        FocusControl = DBEdit13
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 16
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsComandaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 16
        Width = 861
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsComandaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit2: TDBEdit
        Left = 12
        Top = 56
        Width = 57
        Height = 21
        DataField = 'Cliente'
        DataSource = DsComandaCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 68
        Top = 56
        Width = 925
        Height = 21
        DataField = 'NO_Cliente'
        DataSource = DsComandaCab
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 12
        Top = 96
        Width = 57
        Height = 21
        DataField = 'GraCusPrc'
        DataSource = DsComandaCab
        TabOrder = 4
      end
      object DBEdit8: TDBEdit
        Left = 68
        Top = 96
        Width = 473
        Height = 21
        DataField = 'NO_GraCusPrc'
        DataSource = DsComandaCab
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 12
        Top = 136
        Width = 134
        Height = 21
        DataField = 'QtdeTotal'
        DataSource = DsComandaCab
        TabOrder = 6
      end
      object DBEdit14: TDBEdit
        Left = 152
        Top = 136
        Width = 134
        Height = 21
        DataField = 'BrutTotal'
        DataSource = DsComandaCab
        TabOrder = 7
      end
      object DBEdit10: TDBEdit
        Left = 292
        Top = 136
        Width = 134
        Height = 21
        DataField = 'ValrTotal'
        DataSource = DsComandaCab
        TabOrder = 8
      end
      object DBEdit12: TDBEdit
        Left = 432
        Top = 136
        Width = 134
        Height = 21
        DataField = 'ValrPago'
        DataSource = DsComandaCab
        TabOrder = 9
      end
      object DBEdit11: TDBEdit
        Left = 572
        Top = 136
        Width = 134
        Height = 21
        DataField = 'ValrNPag'
        DataSource = DsComandaCab
        TabOrder = 10
      end
      object DBEdit4: TDBEdit
        Left = 544
        Top = 96
        Width = 149
        Height = 21
        DataField = 'DtHrPedido'
        DataSource = DsComandaCab
        TabOrder = 11
      end
      object DBEdit5: TDBEdit
        Left = 696
        Top = 96
        Width = 149
        Height = 21
        DataField = 'DtHrLiProd_TXT'
        DataSource = DsComandaCab
        TabOrder = 12
      end
      object DBEdit6: TDBEdit
        Left = 848
        Top = 96
        Width = 149
        Height = 21
        DataField = 'DtHrRetird_TXT'
        DataSource = DsComandaCab
        TabOrder = 13
      end
      object DBEdit1: TDBEdit
        Left = 936
        Top = 16
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsComandaCab
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 712
        Top = 136
        Width = 134
        Height = 21
        DataField = 'ValrAPag'
        DataSource = DsComandaCab
        TabOrder = 15
      end
    end
    object PCMovimento: TPageControl
      Left = 0
      Top = 161
      Width = 1008
      Height = 428
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Venda '
        object Splitter1: TSplitter
          Left = 0
          Top = 125
          Width = 1000
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitLeft = -12
          ExplicitTop = 56
        end
        object PnProdutos: TPanel
          Left = 0
          Top = 130
          Width = 1000
          Height = 270
          Align = alClient
          TabOrder = 0
          object Label21: TLabel
            Left = 1
            Top = 1
            Width = 998
            Height = 13
            Align = alTop
            Caption = ' Produtos:'
            ExplicitWidth = 48
          end
          object Splitter2: TSplitter
            Left = 1
            Top = 43
            Width = 998
            Height = 5
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = 108
            ExplicitWidth = 1006
          end
          object DBGIts: TDBGrid
            Left = 1
            Top = 14
            Width = 998
            Height = 29
            Align = alClient
            DataSource = DsComandaIts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Produto'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Drescri'#231#227'o do produto'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtdeItem'
                Title.Caption = 'Quantidade'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sigla'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoUnit'
                Title.Caption = '$ Unit.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PercDesc'
                Title.Caption = '% Desc.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValrBrut'
                Title.Caption = '$ Bruto'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vDesc'
                Title.Caption = '$ Desc.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vFrete'
                Title.Caption = '$ Frete'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vOutro'
                Title.Caption = '$ Outros'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValrItem'
                Title.Caption = '$ liq. Item'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BrutCstm'
                Title.Caption = '$ Brt. Custom.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValrCstm'
                Title.Caption = '$ liq. custom.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BrutItCu'
                Title.Caption = '$ Bru. It+Cust.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValrItCu'
                Title.Caption = '$ L'#237'q. It+Cust.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vSeg'
                Title.Caption = '$ Seguro'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Observa'#231#227'o'
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 1
            Top = 48
            Width = 998
            Height = 221
            Align = alBottom
            Caption = 'Panel8'
            TabOrder = 1
            object Label23: TLabel
              Left = 1
              Top = 1
              Width = 996
              Height = 13
              Align = alTop
              Caption = ' Personaliza'#231#227'o do produto selecionado:'
              ExplicitWidth = 192
            end
            object DBGrid1: TDBGrid
              Left = 1
              Top = 14
              Width = 996
              Height = 206
              Align = alClient
              DataSource = DsComandaCus
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Produto'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Drescri'#231#227'o do produto'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdeItem'
                  Title.Caption = 'Quantidade'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Sigla'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PrecoUnit'
                  Title.Caption = '$ Unit.'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PercDesc'
                  Title.Caption = '% Desc.'
                  Width = 42
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrBrut'
                  Title.Caption = '$ Bruto'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vDesc'
                  Title.Caption = '$ Desc.'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vFrete'
                  Title.Caption = '$ Frete'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vOutro'
                  Title.Caption = '$ Outros'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrItem'
                  Title.Caption = '$ liq. Item'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'BrutCstm'
                  Title.Caption = '$ Brt. Custom.'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrCstm'
                  Title.Caption = '$ liq. custom.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'BrutItCu'
                  Title.Caption = '$ Bru. It+Cust.'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValrItCu'
                  Title.Caption = '$ L'#237'q. It+Cust.'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vSeg'
                  Title.Caption = '$ Seguro'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Observa'#231#227'o'
                  Visible = True
                end>
            end
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 125
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label24: TLabel
            Left = 0
            Top = 0
            Width = 1000
            Height = 13
            Align = alTop
            Caption = ' Servi'#231'os: '
            ExplicitWidth = 50
          end
          object DBGCus: TDBGrid
            Left = 0
            Top = 13
            Width = 1000
            Height = 112
            Align = alClient
            DataSource = DsComandaSvc
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Produto'
                Title.Caption = 'Servi'#231'o'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Drescri'#231#227'o do produto'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtdeItem'
                Title.Caption = 'Quantidade'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoUnit'
                Title.Caption = '$ Unit.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PercDesc'
                Title.Caption = '% Desc.'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValrBrut'
                Title.Caption = '$ Bruto'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vDesc'
                Title.Caption = '$ Desc.'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vFrete'
                Title.Caption = '$ Frete'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vOutro'
                Title.Caption = '$ Outros'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValrItem'
                Title.Caption = '$ liq. Item'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Observa'#231#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vSeg'
                Title.Caption = '$ Seguro'
                Width = 66
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Faturamento '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 400
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          object Splitter5: TSplitter
            Left = 1
            Top = 174
            Width = 998
            Height = 6
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ExplicitWidth = 1229
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 1
            Width = 998
            Height = 173
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Faturamento: '
            TabOrder = 0
            object DBGrid3: TDBGrid
              Left = 2
              Top = 15
              Width = 994
              Height = 156
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsComandaFat
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DtHrFat'
                  Title.Caption = 'Faturado em'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NumNF'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValTotal'
                  Title.Caption = '$ TOTAL'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLocad'
                  Title.Caption = '$ Loca'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValUsado'
                  Title.Caption = '$ Uso'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValConsu'
                  Title.Caption = '$ Consumo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValVenda'
                  Title.Caption = '$ Venda'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValServi'
                  Title.Caption = '$ Servi'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValDesco'
                  Title.Caption = '$ Desconto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'No_CartEmis'
                  Title.Caption = 'Carteira'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CondicaoPG'
                  Title.Caption = 'Condi'#231#227'o de pagamento'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'No_Genero'
                  Title.Caption = 'Conta do plano de contas'
                  Width = 140
                  Visible = True
                end>
            end
          end
          object GroupBox7: TGroupBox
            Left = 1
            Top = 180
            Width = 998
            Height = 219
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Parcelas do faturamento selecionado: '
            TabOrder = 1
            object DBGLctFatRef: TDBGrid
              Left = 2
              Top = 15
              Width = 994
              Height = 202
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsLctFatRef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PARCELA'
                  Title.Caption = 'Parc.'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Width = 82
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencto'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 173
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 117
        Height = 32
        Caption = 'Comanda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 117
        Height = 32
        Caption = 'Comanda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 117
        Height = 32
        Caption = 'Comanda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 389
      Top = 0
      Width = 571
      Height = 52
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 567
        Height = 35
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrComandaCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrComandaCabBeforeOpen
    AfterOpen = QrComandaCabAfterOpen
    BeforeClose = QrComandaCabBeforeClose
    AfterScroll = QrComandaCabAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, '
      'IF(cab.DtHrLiProd < 2, "", DATE_FORMAT(cab.DtHrLiProd,'
      '"%d/%m/%y %H:%i")) DtHrLiProd_TXT,'
      'IF(cab.DtHrRetird < 2, "", DATE_FORMAT(cab.DtHrRetird,'
      '"%d/%m/%y %H:%i")) DtHrRetird_TXT,'
      'cab.*  '
      'FROM comandacab cab '
      'LEFT JOIN entidades cli ON cli.Codigo=cab.Cliente '
      'WHERE cli.Codigo<>0 ')
    Left = 80
    Top = 241
    object QrComandaCabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrComandaCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrComandaCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrComandaCabDtHrPedido: TDateTimeField
      FieldName = 'DtHrPedido'
      Required = True
    end
    object QrComandaCabDtHrLiProd: TDateTimeField
      FieldName = 'DtHrLiProd'
      Required = True
    end
    object QrComandaCabDtHrRetird: TDateTimeField
      FieldName = 'DtHrRetird'
      Required = True
    end
    object QrComandaCabCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrComandaCabQtdeTotal: TFloatField
      FieldName = 'QtdeTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrComandaCabValrTotal: TFloatField
      FieldName = 'ValrTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaCabValrNPag: TFloatField
      FieldName = 'ValrNPag'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaCabValrPago: TFloatField
      FieldName = 'ValrPago'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrComandaCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrComandaCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrComandaCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrComandaCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrComandaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrComandaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrComandaCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrComandaCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrComandaCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrComandaCabDtHrLiProd_TXT: TWideStringField
      FieldName = 'DtHrLiProd_TXT'
    end
    object QrComandaCabDtHrRetird_TXT: TWideStringField
      FieldName = 'DtHrRetird_TXT'
    end
    object QrComandaCabGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrComandaCabNO_GraCusPrc: TWideStringField
      FieldName = 'NO_GraCusPrc'
      Size = 30
    end
    object QrComandaCabBrutTotal: TFloatField
      FieldName = 'BrutTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaCabValrAPag: TFloatField
      FieldName = 'ValrAPag'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaCabCondPgCombin: TIntegerField
      FieldName = 'CondPgCombin'
    end
    object QrComandaCabNO_CondPgCombin: TWideStringField
      FieldName = 'NO_CondPgCombin'
      Size = 50
    end
  end
  object DsComandaCab: TDataSource
    DataSet = QrComandaCab
    Left = 80
    Top = 285
  end
  object QrComandaIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrComandaItsBeforeClose
    AfterScroll = QrComandaItsAfterScroll
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, '
      'CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      
        'NO_PRD_TAM_COR, med.Nome NO_UnidMed, med.Sigla, ggx.GraGru1, gg1' +
        '.NCM, '
      'gg1.Referencia, ggx.EAN13, gg1.UnidMed, gg1.PrdGrupTip, '
      'pgt.Nome NO_PrdGrupTip, '
      '(its.ValrBrut - its.ValrItem) ValrDesc, '
      '(its.BrutItCu - its.ValrItCu) ItCuDesc, '
      'its.*'
      ' '
      'FROM comandaits its'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=its.Produto'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed')
    Left = 172
    Top = 241
    object QrComandaItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrComandaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrComandaItsProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrComandaItsUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrComandaItsQtdeItem: TFloatField
      FieldName = 'QtdeItem'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrComandaItsPrecoUnit: TFloatField
      FieldName = 'PrecoUnit'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrComandaItsValrBrut: TFloatField
      FieldName = 'ValrBrut'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsPercDesc: TFloatField
      FieldName = 'PercDesc'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsValrItem: TFloatField
      FieldName = 'ValrItem'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsValrCstm: TFloatField
      FieldName = 'ValrCstm'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsValrItCu: TFloatField
      FieldName = 'ValrItCu'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrComandaItsNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrComandaItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrComandaItsNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrComandaItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrComandaItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrComandaItsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrComandaItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrComandaItsEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrComandaItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrComandaItsPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrComandaItsNO_PrdGrupTip: TWideStringField
      FieldName = 'NO_PrdGrupTip'
      Size = 60
    end
    object QrComandaItsBrutItCu: TFloatField
      FieldName = 'BrutItCu'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsBrutCstm: TFloatField
      FieldName = 'BrutCstm'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrComandaItsValrDesc: TFloatField
      FieldName = 'ValrDesc'
    end
    object QrComandaItsItCuDesc: TFloatField
      FieldName = 'ItCuDesc'
    end
  end
  object DsComandaIts: TDataSource
    DataSet = QrComandaIts
    Left = 172
    Top = 285
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Descontoparatodositens1: TMenuItem
      Caption = 'Desconto para todos itens'
      OnClick = Descontoparatodositens1Click
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE, Ativo' +
        ' '
      'FROM entidades '
      'ORDER BY NOMEENTIDADE ')
    Left = 344
    Top = 240
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 344
    Top = 284
  end
  object QrGraCusPrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracusprc'
      'ORDER BY Nome')
    Left = 428
    Top = 236
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 428
    Top = 284
  end
  object QrComandaCus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, '
      'CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      
        'NO_PRD_TAM_COR, med.Nome NO_UnidMed, med.Sigla, ggx.GraGru1, gg1' +
        '.NCM, '
      'gg1.Referencia, ggx.EAN13, gg1.UnidMed, gg1.PrdGrupTip, '
      'pgt.Nome NO_PrdGrupTip, '
      '(cus.ValrBrut - cus.ValrCstm) ValrDesc, '
      'cus.*'
      ' '
      'FROM comandacus cus'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=cus.Produto'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      ''
      'WHERE cus.Controle>0')
    Left = 264
    Top = 241
    object QrComandaCusCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrComandaCusControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrComandaCusConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrComandaCusProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrComandaCusUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrComandaCusQtdeCstm: TFloatField
      FieldName = 'QtdeCstm'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrComandaCusPrecoUnit: TFloatField
      FieldName = 'PrecoUnit'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrComandaCusValrBrut: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'ValrBrut'
      Required = True
    end
    object QrComandaCusPercDesc: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'PercDesc'
      Required = True
    end
    object QrComandaCusprod_vFrete: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrComandaCusprod_vSeg: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrComandaCusprod_vDesc: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrComandaCusprod_vOutro: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrComandaCusValrCstm: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'ValrCstm'
      Required = True
    end
    object QrComandaCusNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrComandaCusNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrComandaCusNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrComandaCusNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrComandaCusSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrComandaCusGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrComandaCusNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrComandaCusReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrComandaCusEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrComandaCusUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrComandaCusPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrComandaCusNO_PrdGrupTip: TWideStringField
      FieldName = 'NO_PrdGrupTip'
      Size = 60
    end
    object QrComandaCusValrDesc: TFloatField
      FieldName = 'ValrDesc'
      Required = True
    end
  end
  object DsComandaCus: TDataSource
    DataSet = QrComandaCus
    Left = 264
    Top = 285
  end
  object PMCus: TPopupMenu
    OnPopup = PMCusPopup
    Left = 640
    Top = 416
    object CusInclui1: TMenuItem
      Caption = '&Inclui item de customiza'#231#227'o'
      OnClick = CusInclui1Click
    end
    object CusAltera1: TMenuItem
      Caption = '&Altera item de customiza'#231#227'o'
      OnClick = CusAltera1Click
    end
    object CusExclui1: TMenuItem
      Caption = '&Exclui item de customiza'#231#227'o'
      OnClick = CusExclui1Click
    end
  end
  object PMSvc: TPopupMenu
    OnPopup = PMSvcPopup
    Left = 780
    Top = 480
    object SvcInclui1: TMenuItem
      Caption = '&Inclui servi'#231'o'
      OnClick = SvcInclui1Click
    end
    object SvcAltera1: TMenuItem
      Caption = '&Altera o servi'#231'o selecionado'
      OnClick = SvcAltera1Click
    end
    object SvcExclui1: TMenuItem
      Caption = '&Exclui o servi'#231'o selecionado'
      OnClick = SvcExclui1Click
    end
  end
  object QrComandaSvc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_GG1, '
      'CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      'NO_PRD_TAM_COR, ggx.GraGru1, gg1.NCM, '
      'gg1.Referencia, ggx.EAN13, gg1.UnidMed, gg1.PrdGrupTip, '
      'pgt.Nome NO_PrdGrupTip, '
      '(its.ValrBrut - its.ValrItem) ValrDesc, '
      'its.*'
      ' '
      'FROM comandasvc its'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=its.Produto'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gratamcad  gtc ON gtc.Codigo=gti.Codigo '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip '
      ''
      'WHERE its.Codigo>0')
    Left = 536
    Top = 240
    object QrComandaSvcNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrComandaSvcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrComandaSvcGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrComandaSvcNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrComandaSvcReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrComandaSvcEAN13: TWideStringField
      FieldName = 'EAN13'
      Size = 13
    end
    object QrComandaSvcUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrComandaSvcPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrComandaSvcNO_PrdGrupTip: TWideStringField
      FieldName = 'NO_PrdGrupTip'
      Size = 60
    end
    object QrComandaSvcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrComandaSvcControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrComandaSvcProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrComandaSvcQtdeItem: TFloatField
      DefaultExpression = '#,###,###,##0.000;-#,###,###,##0.000; '
      FieldName = 'QtdeItem'
      Required = True
    end
    object QrComandaSvcPrecoUnit: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'PrecoUnit'
      Required = True
    end
    object QrComandaSvcValrBrut: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'ValrBrut'
      Required = True
    end
    object QrComandaSvcPercDesc: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'PercDesc'
      Required = True
    end
    object QrComandaSvcprod_vFrete: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrComandaSvcprod_vSeg: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrComandaSvcprod_vDesc: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vDesc'
      Required = True
    end
    object QrComandaSvcprod_vOutro: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrComandaSvcValrItem: TFloatField
      DefaultExpression = '#,###,###,##0.00;-#,###,###,##0.00; '
      FieldName = 'ValrItem'
      Required = True
    end
    object QrComandaSvcNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrComandaSvcValrDesc: TFloatField
      FieldName = 'ValrDesc'
    end
  end
  object DsComandaSvc: TDataSource
    DataSet = QrComandaSvc
    Left = 536
    Top = 288
  end
  object PMFat: TPopupMenu
    Left = 856
    Top = 492
    object FaturaParcial1: TMenuItem
      Caption = 'Fatura &Parcial'
      OnClick = FaturaParcial1Click
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite boleto(s) do faturamento selecionado'
      OnClick = Gerabloqueto1Click
    end
    object Visualizarbloquetos1: TMenuItem
      Caption = 'Visualizar boletos'
      OnClick = Visualizarbloquetos1Click
    end
    object Excluifaturamentoselecionado1: TMenuItem
      Caption = '&Exclui faturamento selecionado'
      OnClick = Excluifaturamentoselecionado1Click
    end
  end
  object QrComandaFat: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrComandaFatBeforeClose
    AfterScroll = QrComandaFatAfterScroll
    SQL.Strings = (
      'SELECT fat.*, ppc.Nome NO_CondicaoPG, '
      'car.Nome No_CartEmis, cta.Nome No_Genero'
      'FROM comandafat fat '
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=fat.CondicaoPG'
      'LEFT JOIN carteiras car ON car.Codigo=fat.CartEmis'
      'LEFT JOIN contas    cta ON cta.Codigo=fat.Genero'
      ''
      'WHERE fat.Codigo>0'
      '')
    Left = 624
    Top = 240
    object QrComandaFatCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrComandaFatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrComandaFatDtHrFat: TDateTimeField
      FieldName = 'DtHrFat'
      Required = True
    end
    object QrComandaFatValVenda: TFloatField
      FieldName = 'ValVenda'
      Required = True
    end
    object QrComandaFatValServi: TFloatField
      FieldName = 'ValServi'
      Required = True
    end
    object QrComandaFatValFrete: TFloatField
      FieldName = 'ValFrete'
      Required = True
    end
    object QrComandaFatValDesco: TFloatField
      FieldName = 'ValDesco'
      Required = True
    end
    object QrComandaFatValBruto: TFloatField
      FieldName = 'ValBruto'
      Required = True
    end
    object QrComandaFatValTotal: TFloatField
      FieldName = 'ValTotal'
      Required = True
    end
    object QrComandaFatSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrComandaFatNumNF: TIntegerField
      FieldName = 'NumNF'
      Required = True
    end
    object QrComandaFatCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Required = True
    end
    object QrComandaFatCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrComandaFatCodHist: TIntegerField
      FieldName = 'CodHist'
      Required = True
    end
    object QrComandaFatGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrComandaFatNO_CondicaoPG: TWideStringField
      FieldName = 'NO_CondicaoPG'
      Size = 50
    end
    object QrComandaFatNo_CartEmis: TWideStringField
      FieldName = 'No_CartEmis'
      Size = 100
    end
    object QrComandaFatNo_Genero: TWideStringField
      FieldName = 'No_Genero'
      Size = 50
    end
  end
  object DsComandaFat: TDataSource
    DataSet = QrComandaFat
    Left = 624
    Top = 288
  end
  object QrLctFatRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lfi.*'
      'FROM locfits lfi'
      'WHERE lfi.Controle>0'
      '')
    Left = 716
    Top = 240
    object QrLctFatRefPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLctFatRefCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLctFatRefControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctFatRefConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLctFatRefLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLctFatRefValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctFatRefVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsLctFatRef: TDataSource
    DataSet = QrLctFatRef
    Left = 720
    Top = 292
  end
  object frxCMD_PEDID_001_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo52OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '   Memo52.Visible := Memo52.Text <> '#39#39';                         ' +
        '                                     '
      'end;'
      ''
      'procedure Memo57OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '   Memo57.Visible := Memo57.Memo.Text <> '#39#39';  '
      'end;'
      ''
      'procedure MD_ITSOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsComandaIts."Nome"> = '#39#39' then'
      '    MD_ITS.Height := 17.00785            '
      '  else'
      '    MD_ITS.Height := 34.0157;          '
      'end;'
      ''
      'procedure DD_CUSOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsComandaCus."Nome"> = '#39#39' then'
      '    DD_CUS.Height := 17.00785            '
      '  else'
      '    DD_CUS.Height := 34.0157;  '
      'end;'
      ''
      'procedure MD_SVCOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsComandaSvc."Nome"> = '#39#39' then                         '
      '    MD_SVC.Height := 17.00785            '
      '  else'
      '    MD_SVC.Height := 34.0157;      '
      'end;'
      ''
      'begin'
      '    '
      'end.')
    OnGetValue = frxCMD_PEDID_001_01GetValue
    Left = 800
    Top = 236
    Datasets = <
      item
        DataSet = frxDsComandaCab
        DataSetName = 'frxDsComandaCab'
      end
      item
        DataSet = frxDsComandaCus
        DataSetName = 'frxDsComandaCus'
      end
      item
        DataSet = frxDsComandaIts
        DataSetName = 'frxDsComandaIts'
      end
      item
        DataSet = frxDsComandaSvc
        DataSetName = 'frxDsComandaSvc'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 14.314470000000000000
        Top = 782.362710000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 166.299305350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574654250000000000
          Width = 230.551232360000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data do Pedido: [frxDsComandaCab."DtHrPedido"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 90.456710000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Valor total:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 100.015770000000000000
          Top = 113.385734020000000000
          Width = 572.188930000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsComandaCab."ValrTotal"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 552.740260000000000000
          Top = 98.267604250000000000
          Width = 119.464440000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCab."ValrPago"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 90.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Quantidade total:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 100.015770000000000000
          Top = 83.149506220000000000
          Width = 572.629870000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsComandaCab."QtdeTotal"]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'COMANDA [frxDsComandaCab."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo36: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsComandaCab."NO_Cliente"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 370.393940000000000000
          Top = 41.574830000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Data limite da produ'#231#227'o:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 200.315090000000000000
          Top = 41.574830000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsComandaCab."DtHrPedido"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 521.575140000000000000
          Top = 41.574830000000000000
          Width = 151.181102360000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCab."DtHrLiProd"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 487.559370000000000000
          Top = 98.267780000000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 143.622140000000000000
          Width = 252.976500000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Condi'#231#227'o de pagamento combinada:')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 252.756030000000000000
          Top = 143.622140000000000000
          Width = 421.007730000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsComandaCab."NO_CondPgCombin"]')
          ParentFont = False
        end
      end
      object MD_ITS: TfrxMasterData
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MD_ITSOnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsComandaIts
        DataSetName = 'frxDsComandaIts'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          Left = 37.795265830000000000
          Width = 219.212510550000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          Left = 283.464486380000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'QtdeItem'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaIts."QtdeItem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 328.818768270000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'PrecoUnit'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaIts."PrecoUnit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 612.283459690000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValrItem'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaIts."ValrItem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsComandaIts."Produto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          Left = 257.008040000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'Sigla'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaIts."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 559.370440000000000000
          Top = 0.000165980000000010
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValrDesc'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaIts."ValrDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 521.575140000000000000
          Top = 0.000165980000000010
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'PercDesc'
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaIts."PercDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 453.543600000000000000
          Top = 0.000165980000000010
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaIts."ValrBrut"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 15.118120000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaIts."Nome"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 385.512060000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsComandaIts."prod_vFrete">+<frxDsComandaIts."prod_vSeg">+<' +
              'frxDsComandaIts."prod_vOutro">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 529.134200000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          Left = 26.456710000000000000
          Width = 424.850650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total da Venda:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 612.283459690000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsComandaIts."ValrItCu">,MD_ITS)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 559.370440000000000000
          Top = 0.000165980000000010
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsComandaIts."ItCuDesc">,MD_ITS)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 453.543600000000000000
          Top = 0.000165980000000010
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsComandaIts."BrutItCu">,MD_ITS)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 521.575140000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795456220000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsComandaIts."Codigo"'
        object Memo38: TfrxMemoView
          Left = 521.575213230000000000
          Top = 22.677180000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% des.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo09: TfrxMemoView
          Width = 680.314960630000000000
          Height = 21.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDA DE PRODUTOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Top = 22.677180000000000000
          Width = 257.007776380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 283.464481500000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 328.818841500000000000
          Top = 22.677180000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 612.283591500000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 257.008040000000000000
          Top = 22.677345980000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Un.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 559.370571810000000000
          Top = 22.677345980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Desc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 453.543731810000000000
          Top = 22.677345980000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Bruto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 385.512060000000000000
          Top = 22.677345980000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Outros')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsComandaCus."Controle"'
        object Memo10: TfrxMemoView
          Width = 680.314960630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '     PERSONALIZA'#199#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Top = 483.779840000000000000
        Width = 680.315400000000000000
      end
      object DD_CUS: TfrxDetailData
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'DD_CUSOnBeforePrint'
        DataSet = frxDsComandaCus
        DataSetName = 'frxDsComandaCus'
        RowCount = 0
        object Memo43: TfrxMemoView
          Left = 37.795265830000000000
          Width = 219.212510550000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaCus."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 283.464486380000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'QtdeCstm'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCus."QtdeCstm"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 328.818768270000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'PrecoUnit'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCus."PrecoUnit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 612.283459690000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValrCstm'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCus."ValrCstm"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Width = 37.795265830000000000
          Height = 15.118110240000000000
          DataField = 'Produto'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsComandaCus."Produto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 257.008040000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'NO_UnidMed'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaCus."NO_UnidMed"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 559.370440000000000000
          Top = 0.000165980000000010
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValrDesc'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCus."ValrDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 521.575140000000000000
          Top = 0.000165980000000010
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'PercDesc'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCus."PercDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 453.543600000000000000
          Top = 0.000165980000000010
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValrBrut'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaCus."ValrBrut"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Top = 15.118120000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'Memo52OnBeforePrint'
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaCus."Nome"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 385.512060000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsComandaCus."prod_vFrete">+<frxDsComandaCus."prod_vSeg">+<' +
              'frxDsComandaCus."prod_vOutro">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD_SVC: TfrxMasterData
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 638.740570000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MD_SVCOnBeforePrint'
        DataSet = frxDsComandaSvc
        DataSetName = 'frxDsComandaSvc'
        RowCount = 0
        object Memo24: TfrxMemoView
          Left = 37.795265830000000000
          Width = 245.669220550000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaSvc."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 283.464486380000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'QtdeItem'
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaSvc."QtdeItem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 328.818768270000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'PrecoUnit'
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaSvc."PrecoUnit"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 612.283459690000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaSvc."ValrItem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Produto'
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsComandaSvc."Produto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 559.370440000000000000
          Top = 0.000165980000000010
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValrDesc'
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaSvc."ValrDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 521.575140000000000000
          Top = 0.000165980000000010
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaSvc."PercDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 453.543600000000000000
          Top = 0.000165980000000010
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValrBrut'
          DataSet = frxDsComandaSvc
          DataSetName = 'frxDsComandaSvc'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsComandaSvc."ValrBrut"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Top = 15.118120000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'Memo57OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsComandaSvc."Nome"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 385.512060000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsComandaSvc."prod_vFrete">+<frxDsComandaSvc."prod_vSeg">+<' +
              'frxDsComandaSvc."prod_vOutro">]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        PrintChildIfInvisible = True
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 506.457020000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795456220000000000
        Top = 578.268090000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsComandaIts."Codigo"'
        object Memo64: TfrxMemoView
          Width = 680.314960630000000000
          Height = 21.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDA DE SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 521.575213230000000000
          Top = 22.677180000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% des.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Top = 22.677180000000000000
          Width = 283.464486380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 283.464481500000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 328.818841500000000000
          Top = 22.677180000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Unit'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 612.283591500000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 559.370571810000000000
          Top = 22.677345980000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Desc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 453.543731810000000000
          Top = 22.677345980000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Bruto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 385.512060000000000000
          Top = 22.677345980000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Outros')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 695.433520000000000000
        Width = 680.315400000000000000
        object Memo65: TfrxMemoView
          Left = 26.456710000000000000
          Width = 424.850650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total da Servi'#231'os:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 612.283459690000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsComandaSvc."ValrItem">,MD_SVC)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 559.370440000000000000
          Top = 0.000165980000000010
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsComandaSvc."ValrDesc">,MD_SVC)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 453.543600000000000000
          Top = 0.000165980000000010
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaIts
          DataSetName = 'frxDsComandaIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsComandaSvc."ValrBrut">,MD_SVC)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 521.575140000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsComandaCus
          DataSetName = 'frxDsComandaCus'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsComandaCab: TfrxDBDataset
    UserName = 'frxDsComandaCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Cliente=NO_Cliente'
      'Codigo=Codigo'
      'Empresa=Empresa'
      'DtHrPedido=DtHrPedido'
      'DtHrLiProd=DtHrLiProd'
      'DtHrRetird=DtHrRetird'
      'Cliente=Cliente'
      'QtdeTotal=QtdeTotal'
      'ValrTotal=ValrTotal'
      'ValrNPag=ValrNPag'
      'ValrPago=ValrPago'
      'Nome=Nome'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'DtHrLiProd_TXT=DtHrLiProd_TXT'
      'DtHrRetird_TXT=DtHrRetird_TXT'
      'GraCusPrc=GraCusPrc'
      'NO_GraCusPrc=NO_GraCusPrc'
      'BrutTotal=BrutTotal'
      'ValrAPag=ValrAPag'
      'CondPgCombin=CondPgCombin'
      'NO_CondPgCombin=NO_CondPgCombin')
    DataSet = QrComandaCab
    BCDToCurrency = False
    Left = 77
    Top = 334
  end
  object frxDsComandaIts: TfrxDBDataset
    UserName = 'frxDsComandaIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Produto=Produto'
      'Unidade=Unidade'
      'QtdeItem=QtdeItem'
      'PrecoUnit=PrecoUnit'
      'ValrBrut=ValrBrut'
      'PercDesc=PercDesc'
      'prod_vFrete=prod_vFrete'
      'prod_vSeg=prod_vSeg'
      'prod_vDesc=prod_vDesc'
      'prod_vOutro=prod_vOutro'
      'ValrItem=ValrItem'
      'ValrCstm=ValrCstm'
      'ValrItCu=ValrItCu'
      'Nome=Nome'
      'NO_GG1=NO_GG1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_UnidMed=NO_UnidMed'
      'Sigla=Sigla'
      'GraGru1=GraGru1'
      'NCM=NCM'
      'Referencia=Referencia'
      'EAN13=EAN13'
      'UnidMed=UnidMed'
      'PrdGrupTip=PrdGrupTip'
      'NO_PrdGrupTip=NO_PrdGrupTip'
      'BrutItCu=BrutItCu'
      'BrutCstm=BrutCstm'
      'ValrDesc=ValrDesc'
      'ItCuDesc=ItCuDesc')
    DataSet = QrComandaIts
    BCDToCurrency = False
    Left = 169
    Top = 334
  end
  object frxDsComandaCus: TfrxDBDataset
    UserName = 'frxDsComandaCus'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Produto=Produto'
      'Unidade=Unidade'
      'QtdeCstm=QtdeCstm'
      'PrecoUnit=PrecoUnit'
      'ValrBrut=ValrBrut'
      'PercDesc=PercDesc'
      'prod_vFrete=prod_vFrete'
      'prod_vSeg=prod_vSeg'
      'prod_vDesc=prod_vDesc'
      'prod_vOutro=prod_vOutro'
      'ValrCstm=ValrCstm'
      'Nome=Nome'
      'NO_GG1=NO_GG1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_UnidMed=NO_UnidMed'
      'Sigla=Sigla'
      'GraGru1=GraGru1'
      'NCM=NCM'
      'Referencia=Referencia'
      'EAN13=EAN13'
      'UnidMed=UnidMed'
      'PrdGrupTip=PrdGrupTip'
      'NO_PrdGrupTip=NO_PrdGrupTip'
      'ValrDesc=ValrDesc')
    DataSet = QrComandaCus
    BCDToCurrency = False
    Left = 261
    Top = 334
  end
  object frxDsComandaSvc: TfrxDBDataset
    UserName = 'frxDsComandaSvc'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_GG1=NO_GG1'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'GraGru1=GraGru1'
      'NCM=NCM'
      'Referencia=Referencia'
      'EAN13=EAN13'
      'UnidMed=UnidMed'
      'PrdGrupTip=PrdGrupTip'
      'NO_PrdGrupTip=NO_PrdGrupTip'
      'Codigo=Codigo'
      'Controle=Controle'
      'Produto=Produto'
      'QtdeItem=QtdeItem'
      'PrecoUnit=PrecoUnit'
      'ValrBrut=ValrBrut'
      'PercDesc=PercDesc'
      'prod_vFrete=prod_vFrete'
      'prod_vSeg=prod_vSeg'
      'prod_vDesc=prod_vDesc'
      'prod_vOutro=prod_vOutro'
      'ValrItem=ValrItem'
      'Nome=Nome'
      'ValrDesc=ValrDesc')
    DataSet = QrComandaSvc
    BCDToCurrency = False
    Left = 537
    Top = 338
  end
end
