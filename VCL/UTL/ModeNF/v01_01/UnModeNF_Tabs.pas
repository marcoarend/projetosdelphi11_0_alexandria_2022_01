Deprecado => Remover do projeto e usar o UnImprime_Tabs (2015/12/23)
unit UnModeNF_Tabs;

{ Colocar no MyListas:

Uses UnModeNF_Tabs;


//
function TMyListas.CriaListaTabelas:
      ModeNF_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    ModeNF_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    ModeNF_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      ModeNF_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      ModeNF_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    ModeNF_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  ModeNF_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnModeNF_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  ModeNF_Tabs: TUnModeNF_Tabs;

implementation

function TUnModeNF_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('Imprime'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ImprimeBand'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ImprimeView'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ImprimeEmpr'), '');
    //
    //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnModeNF_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
{
separador = "|"
}
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('???') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    //FListaSQL.Add('0|   ""');
  end else
end;

function TUnModeNF_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"ND"');
    FListaSQL.Add('1|"Telefone"');
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
}
end;


function TUnModeNF_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('Imprime') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ImprimeBand') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ImprimeView') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ImprimeEmpr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TUnModeNF_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('Imprime') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '(INDEFINIDO)';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Titulo';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Relat. '+Application.Title;
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Orientacao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Topo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Altu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Larg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_MEsq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_MSup';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_MSup2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeFonte';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'Univers Light Condensed';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPI';  // caracteres per inch
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MSupDOS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '50';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoImpressao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {
      New(FRCampos);
      FRCampos.Field      := 'Empresa'; // Filial > Entidades.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieNF'; // ParamsNFs.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
      New(FRCampos);
      FRCampos.Field      := 'GruposFat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GruposNCM';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ImprimeBand') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '(INDEFINIDO)';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dataset';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Topo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Altu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Col_Qtd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Col_Lar';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Col_GAP';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linhas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Repetencia';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ImprimeView') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Band';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '(INDEFINIDO)';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tab_Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tab_Campo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Padrao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Prefixo';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sufixo';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Topo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Altu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_Larg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pos_MEsq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Alt_Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_TAli';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_BAli';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_Negr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_Ital';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_Unde';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_Tam';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Formato';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Substitui';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Substituicao';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Set_T_DOS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nulo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ImprimeEmpr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParamsNFs';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TUnModeNF_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnModeNF_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  //
  // IMP-CONFG-001 :: Formatos de Impress�o
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-CONFG-001';
  FRJanelas.Nome      := 'FmImprime';
  FRJanelas.Descricao := 'Formatos de Impress�o';
  FLJanelas.Add(FRJanelas);
  //
  // IMP-CONFG-004 :: Formatos de Impress�o - Filiais
  New(FRJanelas);
  FRJanelas.ID        := 'IMP-CONFG-004';
  FRJanelas.Nome      := 'FmImprimeEmpr';
  FRJanelas.Descricao := 'Formatos de Impress�o - Filiais';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.

