object FmTesteXML: TFmTesteXML
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 502
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abre'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Button1: TButton
      Left = 324
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = '???? ???? ????'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 406
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 88
      Align = alTop
      TabOrder = 0
      object Label110: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
      end
      object SpeedButton16: TSpeedButton
        Left = 755
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton16Click
      end
      object Label1: TLabel
        Left = 8
        Top = 44
        Width = 47
        Height = 13
        Caption = 'Ver. XML:'
      end
      object Label2: TLabel
        Left = 64
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Codifica'#231#227'o:'
      end
      object Label3: TLabel
        Left = 716
        Top = 44
        Width = 17
        Height = 13
        Caption = 'N'#243':'
      end
      object Label4: TLabel
        Left = 128
        Top = 44
        Width = 29
        Height = 13
        Caption = 'Local:'
      end
      object Label5: TLabel
        Left = 192
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Prefixo:'
      end
      object Label6: TLabel
        Left = 240
        Top = 44
        Width = 221
        Height = 13
        Caption = 'Declara'#231#227'o de Namespace (Namespace URI):'
      end
      object EdArquivo: TdmkEdit
        Left = 8
        Top = 20
        Width = 745
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 
          'C:\TFLr\XML\TFC\HX07  NATURIN 12-14  COLD COFFE  TPX - 19-1436  ' +
          '  28 01 2013.xml'
        QryCampo = 'DirSchema'
        UpdCampo = 'DirSchema'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 
          'C:\TFLr\XML\TFC\HX07  NATURIN 12-14  COLD COFFE  TPX - 19-1436  ' +
          '  28 01 2013.xml'
        ValWarn = False
      end
      object EdVersion: TdmkEdit
        Left = 8
        Top = 60
        Width = 52
        Height = 21
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEncoding: TdmkEdit
        Left = 64
        Top = 60
        Width = 60
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNodeName: TdmkEdit
        Left = 716
        Top = 60
        Width = 60
        Height = 21
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdLocalName: TdmkEdit
        Left = 128
        Top = 60
        Width = 60
        Height = 21
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPrefix: TdmkEdit
        Left = 192
        Top = 60
        Width = 44
        Height = 21
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNamespaceURI: TdmkEdit
        Left = 240
        Top = 60
        Width = 472
        Height = 21
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object TreeView1: TTreeView
      Left = 221
      Top = 89
      Width = 562
      Height = 316
      Align = alClient
      Indent = 19
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitWidth = 782
    end
    object Memo1: TMemo
      Left = 1
      Top = 89
      Width = 220
      Height = 316
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 4
    Top = 8
    DOMVendorDesc = 'MSXML'
  end
end
