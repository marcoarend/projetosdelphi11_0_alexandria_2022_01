unit BackupDB_XML;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkMemoBar;

type
  TFmBackupDB_XML = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCaminho1: TdmkEdit;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Label1: TLabel;
    EdCaminho2: TdmkEdit;
    SbDir: TSpeedButton;
    SpeedButton1: TSpeedButton;
    EdNomeArq: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbDirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    function  Backup_BD_XML(Dir, Arq: String): String;
    procedure DefineDir(Edit: TdmkEdit);
  public
    { Public declarations }
  end;

  var
  FmBackupDB_XML: TFmBackupDB_XML;

implementation

uses Module, UnMyObjects, UMemModule, ZForge;

{$R *.DFM}

function TFmBackupDB_XML.Backup_BD_XML(Dir, Arq: String): String;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  if MyObjects.CriaForm_AcessoTotal(TFmZForge, FmZForge) then
  begin
    FmZForge.Show;
    FmZForge.ZipaArquivo(1, Dir, FAppDirBD, Arq, '', False, False);
    FmZForge.Destroy;
    Result := MLAGeral.CaminhoArquivo(Dir, Arq, '');
  end;
  Screen.Cursor := MyCursor;
end;

procedure TFmBackupDB_XML.BtOKClick(Sender: TObject);
var
  N: Integer;
  Txt: String;
begin
  N := 0;
  if Trim(EdCaminho1.Text) <> '' then
  begin
    if Backup_BD_XML(EdCaminho1.Text, EdNomeArq.Text) <> '' then
      N := N + 1;
  end;
  if Trim(EdCaminho2.Text) <> '' then
  begin
    if Backup_BD_XML(EdCaminho2.Text, EdNomeArq.Text) <> '' then
      N := N + 1;
  end;
  case N of
    0: Txt := 'N�o foi feito nenhum backup!';
    1: Txt := 'O backup foi realizado com sucesso!';
    2: Txt := 'Foram realizados dois backups com sucesso!';
  end;
  Geral.MensagemBox(Txt, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmBackupDB_XML.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBackupDB_XML.DefineDir(Edit: TdmkEdit);
var
  Dir, Titulo, IniDir: String;
begin
  Dir     := TdmkEdit(Edit).Text;
  Titulo  := 'Diret�rio de Backup';
  IniDir  := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Dir, Titulo, '', [], Dir) then
    TdmkEdit(Edit).Text := ExtractFilePath(Dir);
end;

procedure TFmBackupDB_XML.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
  ImgTipo.SQLType := stUpd;
end;

procedure TFmBackupDB_XML.FormCreate(Sender: TObject);
begin
  EdNomeArq.Text := 'Backup_' + FormatDateTime('YYYYMMDD"_"HHNNSS', Now());
end;

procedure TFmBackupDB_XML.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmBackupDB_XML.SbDirClick(Sender: TObject);
begin
  DefineDir(EdCaminho1);
end;

procedure TFmBackupDB_XML.SpeedButton1Click(Sender: TObject);
begin
  DefineDir(EdCaminho2);
end;

end.
