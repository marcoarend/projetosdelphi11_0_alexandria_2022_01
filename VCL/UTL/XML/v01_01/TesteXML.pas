﻿unit TesteXML;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, XMLDoc, XMLIntf, xmldom, msxmldom, UnDmkEnums;

type
  PRSSFeedData = ^TRSSFeedData;
  TRSSFeedData = record
    URL : string;
    Description : string;
    Date : string;
  end;
  
  TFmTesteXML = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    EdArquivo: TdmkEdit;
    Label110: TLabel;
    SpeedButton16: TSpeedButton;
    TreeView1: TTreeView;
    XMLDocument1: TXMLDocument;
    EdVersion: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdEncoding: TdmkEdit;
    EdNodeName: TdmkEdit;
    Label3: TLabel;
    EdLocalName: TdmkEdit;
    Label4: TLabel;
    EdPrefix: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdNamespaceURI: TdmkEdit;
    Button1: TButton;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    //procedure PopulateTree();
    procedure ClearTree();

  public
    { Public declarations }
  end;

  var
  FmTesteXML: TFmTesteXML;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmTesteXML.BtOKClick(Sender: TObject);
  procedure DomToTree (XmlNode: IXMLNode; TreeNode: TTreeNode);
  var
    I: Integer;
    NewTreeNode: TTreeNode;
    NodeText: string;
    AttrNode: IXMLNode;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MensagemBox(NodeText, 'Texto do nó pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    // add the node itself
    NodeText := XmlNode.NodeName;
    if XmlNode.IsTextElement then
      NodeText := NodeText + ' = ' + XmlNode.NodeValue;
    NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      TreeView1.Items.AddChild(NewTreeNode,
        '[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);
  end;
var
  Arquivo: String;
  //Texto: WideString;
  //xmlDocA, xmlDocB: IXMLDocument;
  //xmlNodeA,
  xmlNodeB, xmlNodeC: IXMLNode;
  //xmlList: IXMLNodeList;
  //
  TV1: TTreeNode;
  j(*, N*): Integer;
  X: IXMLNode;
begin
  Arquivo := EdArquivo.Text;
  if not FileExists(Arquivo) then
  begin
    Geral.MensagemBox('Arquivo não localizado:' + sLineBreak + Arquivo,
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  ClearTree;
  XMLDocument1.FileName := Arquivo;
  XMLDocument1.Active := True;
  EdVersion.Text := XMLDocument1.Version;
  EdEncoding.Text := XMLDocument1.Encoding;
  // Erro
  //ShowMessage(XMLDocument1.DocumentElement.Text);  // Erro
  // Todo texto do XML  após a declaração: <?xml version="1.0" encoding="utf-8" ?>
  //ShowMessage(XMLDocument1.DocumentElement.XML);
  EdLocalName.Text    := XMLDocument1.DocumentElement.LocalName;    // NFe
  EdPrefix.Text       := XMLDocument1.DocumentElement.Prefix;       // [nada]
  EdNamespaceURI.Text := XMLDocument1.DocumentElement.NamespaceURI; // http://www.portalfiscal.inf.br/nfe
  EdNodeName.Text     := XMLDocument1.DocumentElement.NodeName;     // NFe
  //ShowMessage(XMLDocument1.DocumentElement.NodeValue);            // Erro
  xmlNodeB := XMLDocument1.DocumentElement.ChildNodes.First;
  while xmlNodeB <> nil do
  begin
    TV1 := TreeView1.Items.Add(nil, xmlNodeB.NodeName);
    for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
    begin
      X := xmlNodeB.AttributeNodes.Nodes[J];
      TreeView1.Items.AddChild(TV1, '[' + X.NodeName + ' = "' + X.Text + '"]');
    end;
    //
    xmlNodeC := xmlNodeB.ChildNodes.First;
    while xmlNodeC <> nil do
    begin
      //N := N + 1;
      DomToTree(xmlNodeC, TV1);
      xmlNodeC := xmlNodeC.NextSibling;
    end;
    xmlNodeB := xmlNodeB.NextSibling;
  end;
  //
  // Expande árvore
  TreeView1.Items.GetFirstNode.Expand(true);
  //
  XMLDocument1.Active := False;
end;

procedure TFmTesteXML.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTesteXML.Button1Click(Sender: TObject);
const
  Exclui = True;
  //
  procedure DomToTree (XmlNode: IXMLNode; TreeNode: TTreeNode);
  var
    I: Integer;
    NewTreeNode: TTreeNode;
    NodeText: string;
    AttrNode: IXMLNode;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MensagemBox(NodeText, 'Texto do nó pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    // add the node itself
    NodeText := XmlNode.NodeName;
    if XmlNode.IsTextElement then
      NodeText := NodeText + ' = ' + XmlNode.NodeValue;
    NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      TreeView1.Items.AddChild(NewTreeNode,
        '[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);
  end;
var
  Arquivo: String;
  //Texto: WideString;
  //xmlDocA, xmlDocB: IXMLDocument;
  //xmlNodeA,
  xmlNodeB, xmlNodeC: IXMLNode;
  //xmlList: IXMLNodeList;
  //
  TV1: TTreeNode;
  j(*, N*): Integer;
  X: IXMLNode;
var
  //Codigo: RawByteString;
  I: Integer;
  Texto: String;
begin
(*
  Codigo := UTF8Encode('ç');
  Geral.MB_Aviso('ç = ' + Codigo + ' (' + IntToStr(ord(Codigo[1])) + ')');
*)
  Memo1.Lines.Clear;
  Arquivo := 'C:\Dermatek\XML\SPF1\TesteLoop.xml';
  EdArquivo.Text := Arquivo;
  for I := 32 to 255 do
  begin
    Texto :=
'<?xml version="1.0"?>' + sLineBreak +
'<RecRib xmlns="http://www.dermatek.com.br/www/apps/TFC" versao="0.01">' +
'<Receita><Cabecalho><Artigo>NATURIN' +
    Char(I) +
'</Artigo></Cabecalho></Receita></RecRib>';
      Geral.SalvaTextoEmArquivo(Arquivo, Texto, Exclui, fetUTF_8);
      try
        BtOKClick(Self);
      except
        //Geral.MB_Erro('Char ' + Geral.FF0(I) + ' não passou!');
        Memo1.Lines.Add('Char ' + Geral.FF0(I) + ' não passou!');
      end;
    end;
end;

procedure TFmTesteXML.ClearTree();
var
  I: integer;
begin
  for I := 0 to TreeView1.Items.Count - 1 do
    Dispose(TreeView1.Items[I].Data);
  TreeView1.Items.Clear;
end;

procedure TFmTesteXML.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTesteXML.FormCreate(Sender: TObject);
begin
  //PopulateTree();
end;

procedure TFmTesteXML.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

{
procedure TFmTesteXML.PopulateTree();
var
  nd : IXMLNode;

  procedure ProcessItem();
  var
    rssFeedData : PRSSFeedData;
    tn : TTreeNode;
    title : string;
  begin
    New(rssFeedData);

    title := nd.ChildNodes.FindNode('title').Text;

    with nd.ChildNodes do
    begin
      rssFeedData.URL := FindNode('link').Text;
      rssFeedData.Description := FindNode('description').Text;
      rssFeedData.Date  := FindNode('dc:date').Text;
    end;

    tn := TreeView1.Items.AddChild(TreeView1.Items.GetFirstNode, title);
    tn.Data := rssFeedData;
  end;
begin
  ClearTree;

  //Download a file from the Internet with progress indicator
  //http://delphi.about.com/cs/adptips2003/a/bltip0903_2.htm
  XMLDocument1.FileName := 'C:\Projetos\Delphi 2007\XML\Teste\RssFeedTree\ADPHealines.xml';
  XMLDocument1.Active := True;

  //"chanel"
  nd := XMLDocument1.DocumentElement.ChildNodes.First;
  //title
  nd := nd.ChildNodes.First;
  Caption := nd.Text + ' RSS Feed';
  while nd.NodeName <> 'item' do nd := nd.NextSibling;

  //add top tree node
  TreeView1.Items.AddChild(nil,'http://z.about.com/6/g/delphi/b/index.xml');

  //loop through "items"
  while nd <> nil do
  begin
    ProcessItem();
    nd := nd.NextSibling;
  end;

  //expand tree
  TreeView1.Items.GetFirstNode.Expand(true);

  //"close" RSS XML file
  XMLDocument1.Active := false;
end;
}

procedure TFmTesteXML.SpeedButton16Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArquivo.Text);
  Arquivo := ExtractFileName(EdArquivo.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    EdArquivo.Text := Arquivo;
end;

end.
