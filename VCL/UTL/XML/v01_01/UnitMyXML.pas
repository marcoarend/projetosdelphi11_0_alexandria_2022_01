unit UnitMyXML;

interface

uses Windows, Dialogs, SysUtils, OmniXML, OmniXMLUtils, Vcl.StdCtrls,
 //Xml.XMLIntf,
 UnDmkProcFunc, UnDmkEnums, TypInfo;

type
  TTipoTagXML = (
    ttx_Id                ,
    ttx_versao            ,
    ttx_tpAmb             ,
    ttx_verAplic          ,
    ttx_cStat             ,
    ttx_xMotivo           ,
    ttx_cUF               ,
    ttx_cNF               ,
    ttx_natOp             ,
    ttx_indPag            ,
    ttx_nNF               ,
    ttx_dSaiEnt           ,
    ttx_dhSaiEnt          ,
    ttx_tpNF              ,
    ttx_idDest            ,
    ttx_cMunFG            ,
    ttx_tpImp             ,
    ttx_tpEmis            ,
    ttx_cDV               ,
    ttx_finNFe            ,
    ttx_indFinal          ,
    ttx_indpres           ,
    ttx_indIntermed       ,
    ttx_procEmi           ,
    ttx_verProc           ,
    ttx_dhCont            ,
    ttx_xJust             ,
    //ttx_               ,
    ttx_dhRecbto          ,
    ttx_chNFe             ,
    ttx_nProt             ,
    ttx_digVal            ,
    ttx_ano               ,
    ttx_CNPJ              ,
    ttx_CPF               ,
    ttx_mod               ,
    ttx_serie             ,
    ttx_nNFIni            ,
    ttx_nNFFin            ,
    ttx_nRec              ,
    ttx_tMed              ,
    // NFe
    ttx_xNome             ,
    ttx_xFant             ,
    ttx_IE                ,
    ttx_xLgr              ,
    ttx_nro               ,
    ttx_xCpl              ,
    ttx_xBairro           ,
    ttx_cMun              ,
    ttx_xMun              ,
    ttx_UF                ,
    ttx_CEP               ,
    ttx_cPais             ,
    ttx_xPais             ,
    ttx_fone              ,
    ttx_IEST              ,
    ttx_IM                ,
    ttx_email             ,
    ttx_CNAE              ,
    ttx_idEstrangeiro     ,
    ttx_indIEDest         ,
    ttx_CRT               ,
    ttx_ISUF              ,
    ttx_xEnder            ,
    ttx_vICMS             ,
    ttx_vICMSDeson        ,
    ttx_vFCPUFDest        ,
    ttx_vICMSUFDest       ,
    ttx_vICMSUFRemet      ,
    ttx_vFCP              ,
    ttx_vFCPST            ,
    ttx_vFCPSTRet         ,
    ttx_vProd             ,
    ttx_vFrete            ,
    ttx_vSeg              ,
    ttx_vDesc             ,
    ttx_vIPI              ,
    ttx_vIPIDevol         ,
    ttx_vPIS              ,
    ttx_vCOFINS           ,
    ttx_vOutro            ,
    ttx_vNF               ,
    ttx_vTotTrib          ,
    ttx_vST               ,
    ttx_vII               ,
    ttx_qVol              ,
    ttx_esp               ,
    ttx_marca             ,
    ttx_nVol              ,
    ttx_pesoL             ,
    ttx_pesoB             ,
    ttx_nItem             ,
    ttx_dEmi              ,
    ttx_dhEmi             ,
    ttx_cProd             ,
    ttx_cEAN              ,
    ttx_xProd             ,
    ttx_NCM               ,
    ttx_EXTIPI            ,
    ttx_Genero            ,
    ttx_CFOP              ,
    ttx_uCom              ,
    ttx_qCom              ,
    ttx_vUnCom            ,
    ttx_cEANTrib          ,
    ttx_uTrib             ,
    ttx_qTrib             ,
    ttx_vUnTrib           ,
    ttx_orig              ,
    ttx_CST               ,
    ttx_modBC             ,
    ttx_vBC               ,
    ttx_qUnid             ,
    ttx_vUnid             ,
    ttx_pRedBC            ,
    ttx_pICMS             ,
    ttx_modBCST           ,
    ttx_pMVAST            ,
    ttx_pRedBCST          ,
    ttx_vBCST             ,
    ttx_pICMSST           ,
    ttx_vICMSST           ,
    ttx_clEnq             ,
    ttx_CNPJProd          ,
    ttx_cSelo             ,
    ttx_qSelo             ,
    ttx_cEnq              ,
    ttx_pIPI              ,
    ttx_pPIS              ,
    ttx_qBCProd           ,
    ttx_VAliqProd         ,
    ttx_pCOFINS           ,
    ttx_modFrete          ,
    ttx_infAdFisco        ,
    ttx_infCpl            ,
    ttx_InfAdProd         ,
    ttx_indEscala         ,
    ttx_CNPJFab           ,
    ttx_cBenef            ,
    ttx_nItemPed          ,
    ttx_CSOSN             ,
    ttx_pCredSN           ,
    ttx_vCredICMSSN       ,
    ttx_vBCFCPST          ,
    ttx_pFCPST            ,
    ttx_vCPST             ,
    ttx_vUFST             ,
    ttx_pBCOp             ,
    ttx_vBCSTRet          ,
    ttx_pST               ,
    ttx_vICMSSTRet        ,
    ttx_vBCFCPSTRet       ,
    ttx_pFCPSTRet         ,
    ttx_motDesICMS        ,
    ttx_vDespAdu          ,
    ttx_vIOF              ,
    ttx_nFat              ,
    ttx_vOrig             ,
    ttx_vLiq              ,
    ttx_nDup              ,
    ttx_dVenc             ,
    ttx_vDup              ,
    ttx_refNFe            ,
    ttx_refNFeSig         ,
    ttx_refNF_cUF         ,
    ttx_refNF_AAMM        ,
    ttx_refNF_CNPJ        ,
    ttx_refNF_mod         ,
    ttx_refNF_serie       ,
    ttx_refNF_nNF         ,
    ttx_refNFP_cUF        ,
    ttx_refNFP_AAMM       ,
    ttx_refNFP_CNPJ       ,
    ttx_refNFP_CPF        ,
    ttx_refNFP_IE         ,
    ttx_refNFP_mod        ,
    ttx_refNFP_serie      ,
    ttx_refNFP_nNF        ,
    ttx_refCTe            ,
    ttx_refECF_mod        ,
    ttx_refECF_nECF       ,
    ttx_refECF_nCOO       ,
    ttx_pDevol            ,
    ttx_nLote             ,
    ttx_qLote             ,
    ttx_dFab              ,
    ttx_dVal              ,
    ttx_cAgreg            ,
    ttx_xNEmp             ,
    ttx_xPed              ,
    ttx_xCont             ,
    ttx_nProc             ,
    ttx_procRef           ,
    ttx_tpAto             ,

    ttx_);

  TUnitMyXML = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  CarregaXML(const Arquivo: String; var Texto: WideString):
              Boolean;
    function  DefineXMLDoc(var xmlDoc: IXMLDocument; const Texto: WideString): Boolean;
    function  LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML): WideString;
    function  ObtemNomeDaTag(Tag: TTipoTagXML): String;
    function  SelecionaUmNoDeDoc(IXMLDoc: IXMLDocument; Pattern: string): IXMLNode;
    function  SelecionaUmNoDeNoh(Informa: TInfoUserMsg; IXMLNoh: IXMLNode;
              Pattern: string; MemoWarn, MemoInfo, MeAusentes: TMemo): IXMLNode;
    function  Teste(Informa: TInfoUserMsg; XMLNoh: IXMLNode; MeAvisos: TMemo;
              CaminhoPai: String; Atribs, Campos: array of String): String;
    function  NodeTypeAsText(NodeType: Integer): String;
  end;

  var
  UnMyXML: TUnitMyXML;

implementation

uses UnMyObjects, dmkGeral;

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';

function TUnitMyXML.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id             : Result :=      'Id';
    ttx_versao         : Result :=      'versao';
    ttx_tpAmb          : Result :=      'tpAmb';
    ttx_verAplic       : Result :=      'verAplic';
    ttx_cStat          : Result :=      'cStat';
    ttx_xMotivo        : Result :=      'xMotivo';
    ttx_cUF            : Result :=      'cUF';
    ttx_cNF            : Result :=      'cNF';
    ttx_natOp          : Result :=      'natOp';
    ttx_indPag         : Result :=      'indPag';
    ttx_nNF            : Result :=      'nNF';
    ttx_dSaiEnt        : Result :=      'dSaiEnt';
    ttx_dhSaiEnt       : Result :=      'dhSaiEnt';
    ttx_tpNF           : Result :=      'tpNF';
    ttx_idDest         : Result :=      'idDest';
    ttx_cMunFG         : Result :=      'cMunFG';
    ttx_tpImp          : Result :=      'tpImp';
    ttx_tpEmis         : Result :=      'tpEmis';
    ttx_cDV            : Result :=      'cDV';
    ttx_finNFe         : Result :=      'finNFe';
    ttx_indFinal       : Result :=      'indFinal';
    ttx_indpres        : Result :=      'indPres';
    ttx_indIntermed    : Result :=      'indIntermed';
    ttx_procEmi        : Result :=      'procEmi';
    ttx_verProc        : Result :=      'verProc';
    ttx_dhCont         : Result :=      'dhCont';
    ttx_xJust          : Result :=      'xJust';
    //ttx_             : Result :=      'x_
    ttx_dhRecbto       : Result :=      'dhRecbto';
    ttx_chNFe          : Result :=      'chNFe';
    ttx_nProt          : Result :=      'nProt';
    ttx_digVal         : Result :=      'digVal';
    ttx_ano            : Result :=      'ano';
    ttx_CNPJ           : Result :=      'CNPJ';
    ttx_CPF            : Result :=      'CPF';
    ttx_mod            : Result :=      'mod';
    ttx_serie          : Result :=      'serie';
    ttx_nNFIni         : Result :=      'nNFIni';
    ttx_nNFFin         : Result :=      'nNFFin';
    ttx_nRec           : Result :=      'nRec';
    ttx_tMed           : Result :=      'tMed';
    // NFe             : Result :=      'Fe
    ttx_xNome          : Result :=      'xNome';
    ttx_xFant          : Result :=      'xFant';
    ttx_IE             : Result :=      'IE';
    ttx_xLgr           : Result :=      'xLgr';
    ttx_nro            : Result :=      'nro';
    ttx_xCpl           : Result :=      'xCpl';
    ttx_xBairro        : Result :=      'xBairro';
    ttx_cMun           : Result :=      'cMun';
    ttx_xMun           : Result :=      'xMun';
    ttx_UF             : Result :=      'UF';
    ttx_CEP            : Result :=      'CEP';
    ttx_cPais          : Result :=      'cPais';
    ttx_xPais          : Result :=      'xPais';
    ttx_fone           : Result :=      'fone';
    ttx_IEST           : Result :=      'IEST';
    ttx_IM             : Result :=      'IM';
    ttx_email          : Result :=      'email';
    ttx_CNAE           : Result :=      'CNAE';
    ttx_idEstrangeiro  : Result := 'idEstrangeiro';
    ttx_indIEDest      : Result :=      'indIEDest';
    ttx_CRT            : Result :=      'CRT';
    ttx_ISUF           : Result :=      'ISUF';
    ttx_xEnder         : Result :=      'xEnder';
    ttx_vICMS          : Result :=      'vICMS';
    ttx_vICMSDeson     : Result :=      'vICMSDeson';
    ttx_vFCPUFDest     : Result :=    'vFCPUFDest';
    ttx_vICMSUFDest    : Result :=    'vICMSUFDest';
    ttx_vICMSUFRemet   : Result :=    'vICMSUFRemet';
    ttx_vFCP           : Result :=    'vFCP';
    ttx_vFCPST         : Result :=    'vFCPST';
    ttx_vProd          : Result :=      'vProd';
    ttx_vFrete         : Result :=      'vFrete';
    ttx_vSeg           : Result :=      'vSeg';
    ttx_vDesc          : Result :=      'vDesc';
    ttx_vIPI           : Result :=      'vIPI';
    ttx_vIPIDevol      : Result :=      'vIPIDevol';
    ttx_vPIS           : Result :=      'vPIS';
    ttx_vCOFINS        : Result :=      'vCOFINS';
    ttx_vOutro         : Result :=      'vOutro';
    ttx_vNF            : Result :=      'vNF';
    ttx_vTotTrib       : Result :=      'vTotTrib';
    ttx_vST            : Result :=      'vST';
    ttx_vII            : Result :=      'vII';
    ttx_qVol           : Result :=      'qVol';
    ttx_esp            : Result :=      'esp';
    ttx_marca          : Result :=      'marca';
    ttx_nVol           : Result :=      'nVol';
    ttx_pesoL          : Result :=      'pesoL';
    ttx_pesoB          : Result :=      'pesoB';
    ttx_nItem          : Result :=      'nItem';
    ttx_dEmi           : Result :=      'dEmi';
    ttx_dhEmi          : Result :=      'dhEmi';
    ttx_cProd          : Result :=      'cProd';
    ttx_cEAN           : Result :=      'cEAN';
    ttx_xProd          : Result :=      'xProd';
    ttx_NCM            : Result :=      'NCM';
    ttx_EXTIPI         : Result :=      'EXTIPI';
    ttx_Genero         : Result :=      'Genero';
    ttx_CFOP           : Result :=      'CFOP';
    ttx_uCom           : Result :=      'uCom';
    ttx_qCom           : Result :=      'qCom';
    ttx_vUnCom         : Result :=      'vUnCom';
    ttx_cEANTrib       : Result :=      'cEANTrib';
    ttx_uTrib          : Result :=      'uTrib';
    ttx_qTrib          : Result :=      'qTrib';
    ttx_vUnTrib        : Result :=      'vUnTrib';
    ttx_orig           : Result :=      'orig';
    ttx_CST            : Result :=      'CST';
    ttx_modBC          : Result :=      'modBC';
    ttx_vBC            : Result :=      'vBC';
    ttx_qUnid          : Result :=      'qUnid';
    ttx_vUnid          : Result :=      'vUnid';
    ttx_pRedBC         : Result :=      'pRedBC';
    ttx_pICMS          : Result :=      'pICMS';
    ttx_modBCST        : Result :=      'modBCST';
    ttx_pMVAST         : Result :=      'pMVAST';
    ttx_pRedBCST       : Result :=      'pRedBCST';
    ttx_vBCST          : Result :=      'vBCST';
    ttx_pICMSST        : Result :=      'pICMSST';
    ttx_vICMSST        : Result :=      'vICMSST';
    ttx_clEnq          : Result :=      'clEnq';
    ttx_CNPJProd       : Result :=      'CNPJProd';
    ttx_cSelo          : Result :=      'cSelo';
    ttx_qSelo          : Result :=      'qSelo';
    ttx_cEnq           : Result :=      'cEnq';
    ttx_pIPI           : Result :=      'pIPI';
    ttx_pPIS           : Result :=      'pPIS';
    ttx_qBCProd        : Result :=      'qBCProd';
    ttx_VAliqProd      : Result :=      'VAliqProd';
    ttx_pCOFINS        : Result :=      'pCOFINS';
    ttx_modFrete       : Result :=      'modFrete';
    ttx_infAdFisco     : Result :=      'infAdFisco';
    ttx_infCpl         : Result :=      'infCpl';
    ttx_infAdProd      : Result :=      'infAdProd';
    ttx_indEscala      : Result :=      'indEscala';
    ttx_CNPJFab        : Result :=      'CNPJFab';
    ttx_cBenef         : Result :=      'cBenef';
    ttx_nItemPed       : Result :=      'nItemPed';
    ttx_CSOSN          : Result :=      'CSOSN';
    ttx_pCredSN        : Result :=      'pCredSN';
    ttx_vCredICMSSN    : Result :=      'vCredICMSSN';
    ttx_vBCFCPST       : Result :=      'vBCFCPST';
    ttx_pFCPST         : Result :=      'pFCPST';
    ttx_vCPST          : Result :=      'vCPST';
    ttx_vUFST          : Result :=      'vUFST';
    ttx_pBCOp          : Result :=      'pBCOp';
    ttx_vBCSTRet       : Result :=      'vBCSTRet';
    ttx_pST            : Result :=      'pST';
    ttx_vICMSSTRet     : Result :=      'vICMSSTRet';
    ttx_vBCFCPSTRet    : Result :=      'vBCFCPSTRet';
    ttx_pFCPSTRet      : Result :=      'pFCPSTRet';
    ttx_motDesICMS     : Result :=      'motDesICMS';
    ttx_vDespAdu       : Result :=      'vDespAdu';
    ttx_vIOF           : Result :=      'vIOF';
    ttx_nFat           : Result :=      'nFat';
    ttx_vOrig          : Result :=      'vOrig';
    ttx_vLiq           : Result :=      'vLiq';
    ttx_nDup           : Result :=      'nDup';
    ttx_dVenc          : Result :=      'dVenc';
    ttx_vDup           : Result :=      'vDup';
    ttx_refNFe         : Result :=      'refNFe';
    ttx_refNFeSig      : Result :=      'refNFeSig';
    ttx_refNF_cUF      : Result :=      'refNF_cUF';
    ttx_refNF_AAMM     : Result :=      'refNF_AAMM';
    ttx_refNF_CNPJ     : Result :=      'refNF_CNPJ';
    ttx_refNF_mod      : Result :=      'refNF_mod';
    ttx_refNF_serie    : Result :=      'refNF_serie';
    ttx_refNF_nNF      : Result :=      'refNF_nNF';
    ttx_refNFP_cUF     : Result :=      'refNFP_cUF';
    ttx_refNFP_AAMM    : Result :=      'refNFP_AAMM';
    ttx_refNFP_CNPJ    : Result :=      'refNFP_CNPJ';
    ttx_refNFP_CPF     : Result :=      'refNFP_CPF';
    ttx_refNFP_IE      : Result :=      'refNFP_IE';
    ttx_refNFP_mod     : Result :=      'refNFP_mod';
    ttx_refNFP_serie   : Result :=      'refNFP_serie';
    ttx_refNFP_nNF     : Result :=      'refNFP_nNF';
    ttx_refCTe         : Result :=      'refCTe';
    ttx_refECF_mod     : Result :=      'refECF_mod';
    ttx_refECF_nECF    : Result :=      'refECF_nECF';
    ttx_refECF_nCOO    : Result :=      'refECF_nCOO';
    ttx_pDevol         : Result :=      'pDevol';
    ttx_nLote          : Result :=      'nLote';
    ttx_qLote          : Result :=      'qLote';
    ttx_dFab           : Result :=      'dFab';
    ttx_dVal           : Result :=      'dVal';
    ttx_cAgreg         : Result :=      'cAgreg';
    ttx_xNEmp          : Result :=      'xNEmp';
    ttx_xPed           : Result :=      'xPed';
    ttx_xCont          : Result :=      'xCont';
    ttx_nProc          : Result :=      'nProc';
    ttx_procRef        : Result :=      'procRef';
    ttx_tpAto          : Result :=      'tpAto';
    //
    ttx_               : Result :=      '???'               ;
    else                 Result :=      '???';
  end;
end;

function TUnitMyXML.SelecionaUmNoDeDoc(IXMLDoc: IXMLDocument; Pattern: string): IXMLNode;
begin
  if IXMLDoc <> nil then
  begin
    try
      Result := IXMLDoc.SelectSingleNode(Pattern);
    except
      Result := nil;
      Geral.MB_Erro('N�o foi possivel carregar o n�: ' + sLineBreak + Pattern + sLineBreak);
    end;
  end else
    Result := nil;
end;

function TUnitMyXML.SelecionaUmNoDeNoh(Informa: TInfoUserMsg; IXMLNoh: IXMLNode;
  Pattern: string; MemoWarn, MemoInfo, MeAusentes: TMemo): IXMLNode;
const
  sProcName = 'TUnitMyXML.SelecionaUmNoDeNoh()';
var
  Avisou: Boolean;
begin
  Avisou := False;
  if IXMLNoh <> nil then
  begin
    try
      Result := IXMLNoh.SelectSingleNode(Pattern);
      if Result <> nil then
      begin
        case Informa of
          TInfoUserMsg.iumInfo:
          begin
            if MemoInfo <> nil then
            begin
              MemoInfo.Text := Pattern + sLineBreak + MemoInfo.Text;
            end;
          end;
          TInfoUserMsg.iumNoInfo: ;
          TInfoUserMsg.iumWarn:
            Geral.MB_Info(sLineBreak +
            '================================================================' + sLineBreak +
            '=====================  A T E N � � O  ==========================' + sLineBreak +
            '================================================================' + sLineBreak +
            'Favor enviar o XML desta NFe e esta mensagem completa para a DERMATEK!' + sLineBreak +
            'XML a ser implementado neste ERP:' + sLineBreak +
            Result.XML + sLineBreak);
          //TInfoUserMsg.iumIndef
          else
           Geral.MB_Erro('"TInfoUserMsg" indefinido em "' + sProcName + '"' + sLineBreak + Result.XML);
        end;
        Avisou := True;
      end else
      begin
        if MeAusentes <> nil then
          MeAusentes.Text := 'O XML n�o possui a tag: "' + Pattern +
           //'" em "' + sProcName + '"' +
           sLineBreak + MeAusentes.Text + sLineBreak;
         Avisou := True;
      end;
    except
      Result := nil;
    end;
    if (Result = nil) then
    begin
      if not Avisou then
      begin
        if MemoWarn <> nil then
        begin
          case Informa of
            TInfoUserMsg.iumInfo: MemoWarn.Lines.Add('N�o foi poss�vel carregar a tag: ' + Pattern + sLineBreak);
            TInfoUserMsg.iumNoInfo: ;
            TInfoUserMsg.iumWarn: ;
  (*
            begin
              Memo.Lines.Add('N�o foi possivel carregar o n�: ' + Pattern);
              Geral.MB_Info(sLineBreak +
              '================================================================' + sLineBreak +
              '=====================  A T E N � � O  ==========================' + sLineBreak +
              '========= N�o foi possivel carregar o n�: ' + Pattern             + sLineBreak +
              '================================================================' + sLineBreak +
              'Favor Avisar a DERMATEK!' + sLineBreak +
              'XML a ser implementado neste ERP:' + sLineBreak +
              IXMLNoh.XML);
            end;
  *)
            //TInfoUserMsg.iumIndef
            else
             Geral.MB_Erro('"TInfoUserMsg" indefinido em "' + sProcName + '"' + sLineBreak + Result.XML);
          end;
        end;
      end;
    end;
  end else
    Result := nil;
end;

function TUnitMyXML.CarregaXML(const Arquivo: String; var Texto: WideString):
Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  //buf: char;
  buf: AnsiChar;
  //
  //xmldoc, ref, nome, mensagem: Widestring;
  //
  //ArqPath: String;
begin
  Result := False;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('Arquivo n�o encontrado: ' + Arquivo);
    Exit;
  end;
  //
  AssignFile(fEntrada, Arquivo);
  Reset(fEntrada, 1);
  Texto := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    Texto := Texto + buf;
  end;

  CloseFile(fEntrada);
  Result := True;
end;

function TUnitMyXML.DefineXMLDoc(var xmlDoc: IXMLDocument; const Texto:
WideString): Boolean;
{
    xmlDoc : IXMLDocument;
    xmlNode: IXMLNode;
    xmlList: IXMLNodeList;
}
begin
  xmlDoc := CreateXMLDoc;
  if not XMLLoadFromAnsiString(xmlDoc, dmkPF.TextoToHTMLUnicode(Texto)) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(Texto, FXML_Load_Failure);
  end else Result := True;
end;


function TUnitMyXML.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML;
  Tag: TTipoTagXML): WideString;
const
  sProcName = 'TUnitMyXML.LeNoXML()';
var
  NomeTipo: String;
begin
  //try
    Result := '';
    case Tipo of
      tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
      tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
      else Result := '???' + ObtemNomeDaTag(Tag) + '???';
    end;
{
  except
    on E: Exception do
    begin
      NOmeTipo :=  GetEnumName(TypeInfo(TTipoTagXML), Integer(Tag));
      Geral.MB_Erro(sProcName + slineBreak + sLineBreak +
      E.Message + sLineBreak + sLineBreak +
      'Tag: ' + NomeTipo + sLineBreak + sLineBreak +
      ''(*No.Xml*));
    end;
  end;
}
end;


function TUnitMyXML.NodeTypeAsText(NodeType: Integer): String;
begin
  case NodeType of
    // element node
    //ELEMENT_NODE = 1;
    ELEMENT_NODE: Result := 'Element node';

    // attribute node
    //ATTRIBUTE_NODE = 2;
    ATTRIBUTE_NODE: Result := 'Attribute node';

    // text node
    //TEXT_NODE = 3;
    TEXT_NODE: Result := 'Text node';

    // CDATA section node
    //CDATA_SECTION_NODE = 4;
    CDATA_SECTION_NODE: Result := 'CDATA section node';

    // entity reference node
    //ENTITY_REFERENCE_NODE = 5;
    ENTITY_REFERENCE_NODE: Result := 'Entity reference node';

    // entity node
    //ENTITY_NODE = 6;
    ENTITY_NODE: Result := 'Entity node';

    // processing instruction node
    //PROCESSING_INSTRUCTION_NODE = 7;
    PROCESSING_INSTRUCTION_NODE: Result := 'Processing instruction node';

    // comment node
    //COMMENT_NODE = 8;
    COMMENT_NODE: Result := 'Comment node';

    // document node
    //DOCUMENT_NODE = 9;
    DOCUMENT_NODE: Result := 'Document node';

    // document type node
    //DOCUMENT_TYPE_NODE = 10;
    DOCUMENT_TYPE_NODE: Result := 'Document type node';

    // document fragment node
    //DOCUMENT_FRAGMENT_NODE = 11;
    DOCUMENT_FRAGMENT_NODE: Result := 'Document fragment node';

    // notation node
    //NOTATION_NODE = 12;
    NOTATION_NODE: Result := 'Notation node';


    else  Result := '??????????????';
  end;
end;

function TUnitMyXML.Teste(Informa: TInfoUserMsg; XMLNoh: IXMLNode; MeAvisos: TMemo;
  CaminhoPai: String; Atribs, Campos: array of String): String;

const
  sProcName = 'TUnitMyXML.Teste()';
  //

  procedure Info(Tipo: Integer; NodeName, NodeValue: String);
  begin
    //////////////////////////////////////////////
    EXIT;
    /////////////////////////////////////////////
    if MeAvisos <> nil then
    begin
      MeAvisos.Lines.Add(NodeTypeAsText(Tipo) + ' : ' + NodeName + ' = ' + NodeValue);
    end;
  end;
  //
var
  I, J: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  Campo, Mensagem: String;
  Achou: Boolean;
begin
  if XMLNoh = nil then
    Exit;
  Result := EmptyStr;
  //if not (XMLNoh.NodeType = ELEMENT_NODE(*ntElement*)) then
    //Exit;
(* O qu fazer???
  if XMLNoh.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo A00!');
*)
  //for I := 0 to XMLNoh.AttributeNodes.Count - 1 do
  for I := 0 to XMLNoh.Attributes.Length - 1 do
  begin
(*
    Geral.MB_Info('######### ATRIBUTO ################' + sLineBreak +
    XMLNoh.Attributes.Item[I].NodeName + sLineBreak +
    XMLNoh.Attributes.Item[I].NodeValue + sLineBreak);
*)

    Achou := False;
    for J := 0 to Length(Atribs) - 1 do
    begin
      if XMLNoh.Attributes.Item[I].NodeName = Atribs[J] then
      begin
        Achou := True;
        Break;
      end;
    end;
    if not Achou then
    begin
      Mensagem := CaminhoPai + '.*' + XMLNoh.Attributes.Item[I].NodeName;
      case Informa of
        (*1*)TInfoUserMsg.iumInfo: Result := Result + Mensagem + sLineBreak;
        (*2*)TInfoUserMsg.iumNoInfo: ;
        (*3*)TInfoUserMsg.iumWarn:
        begin
          Result := Result + Mensagem + sLineBreak;
          Geral.MB_Info(sLineBreak +
          '================================================================' + sLineBreak +
          '=====================  A T E N � � O  ==========================' + sLineBreak +
          '================================================================' + sLineBreak +
          'Favor enviar o XML desta NFe e esta mensagem completa para a DERMATEK!' + sLineBreak +
          'XML a ser implementado neste ERP:' + sLineBreak +
          XMLNoh.XML);
        end;
        //(*0*)TInfoUserMsg.iumIndef:
        else
        begin
         Geral.MB_Erro('"TInfoUserMsg" indefinido em "' + sProcName + '"' + sLineBreak + Mensagem);
         Result := Result + Mensagem + sLineBreak;
        end;
      end;
    end;
    Info(-1, XMLNoh.Attributes.Item[I].NodeName, XMLNoh.Attributes.Item[I].NodeValue);
    //

  end;
(*
  begin
    AttrNode := XMLNoh.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //if Campo = LowerCase('versao') then
    //  versao := dmkPF.XMLValFlu(AttrNode) else
    //if Campo = LowerCase('Id') then
    //  ID := dmkPF.XMLValTxt(AttrNode) else
    AvisaItemDesconhecido('A00', AttrNode.NodeName);
  end;
*)
  // Subn�s
  if XMLNoh.HasChildNodes then
  begin
    //for I := 0 to XMLNoh.ChildNodes.Count - 1 do
    for I := 0 to XMLNoh.ChildNodes.Length - 1 do
    begin
      //XMLNohNivel1 := XMLNoh.ChildNodes.Nodes[I];
      XMLNohNivel1 := XMLNoh.ChildNodes.Item[I];
      //if (XMLNohNivel1.NodeType = ELEMENT_NODE(*ntElement*)) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
(*
        Geral.MB_Info('Tipo:' +  NodeTypeAsText(XMLNohNivel1.NodeType) + sLineBreak +
        XMLNohNivel1.NodeName + sLineBreak +
        XMLNohNivel1.NodeValue + sLineBreak);
*)
        Info(XMLNohNivel1.NodeType, XMLNohNivel1.NodeName, XMLNohNivel1.NodeValue);
        if XMLNohNivel1.NodeType = ELEMENT_NODE then
        begin
          Achou := False;
          for J := 0 to Length(Campos) - 1 do
          begin
            if XMLNohNivel1.NodeName = Campos[J] then
            begin
              Achou := True;
              Break;
            end;
          end;
          if not Achou then
            Result := Result + CaminhoPai + '.' + XMLNohNivel1.NodeName + sLineBreak;
        end;
{
        if Campo = LowerCase('infNFe')      then
          ImportaNFe_Grupo_A01(XMLNohNivel1) else
        if Campo = LowerCase('Signature')   then
          FAssinado := 1(*ImportaNFe_Grupo_(XMLNohNivel1)*) else
        //
        AvisaItemDesconhecido('A00', XMLNohNivel1.NodeName);
}
      end;
    end;
  end;
  if Result <> EmptyStr then
  begin
    if Lowercase(Result) <> Lowercase('.NFe' + sLineBreak) then
      Geral.MB_Info(Result);
  end;
end;

end.
