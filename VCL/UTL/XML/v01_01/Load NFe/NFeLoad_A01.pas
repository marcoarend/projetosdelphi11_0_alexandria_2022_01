unit NFeLoad_A01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_A01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel21: TPanel;
    Label186: TLabel;
    Label187: TLabel;
    Label188: TLabel;
    Label189: TLabel;
    Label191: TLabel;
    Label192: TLabel;
    EdFatID: TdmkEdit;
    EdSequencial: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdIDCtrl: TdmkEdit;
    Edversao: TdmkEdit;
    EdId: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_A01: TFmNFeLoad_A01;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmNFeLoad_A01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_A01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_A01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_A01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
