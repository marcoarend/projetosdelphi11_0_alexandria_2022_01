unit NFeLoad_Dir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, xmldom, XMLIntf, msxmldom, XMLDoc, Variants, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_Dir = class(TForm)
    XMLDocument1: TXMLDocument;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label110: TLabel;
    SBArquivo: TSpeedButton;
    SbCarrega: TSpeedButton;
    EdDiretorio: TEdit;
    Panel5: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    MeExtensoes: TMemo;
    CkSub1: TCheckBox;
    GroupBox1: TGroupBox;
    Ck_nfeProc: TCheckBox;
    Ck_nfe: TCheckBox;
    Ck_procCancNFe: TCheckBox;
    Ck_procInutNFe: TCheckBox;
    Panel8: TPanel;
    CkSoAmbiente1: TCheckBox;
    GroupBox2: TGroupBox;
    LBEleIgnor: TListBox;
    GroupBox3: TGroupBox;
    LBEleDesc: TListBox;
    Panel9: TPanel;
    Label1: TLabel;
    LBArqs: TListBox;
    Panel6: TPanel;
    Label3: TLabel;
    MeUnknowFile: TMemo;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    QrNFeB: TmySQLQuery;
    DsNFeB: TDataSource;
    DsNotB: TDataSource;
    QrNotB: TmySQLQuery;
    QrNFeBSeqArq: TIntegerField;
    QrNFeBSeqNFe: TIntegerField;
    QrNFeBversao: TFloatField;
    QrNFeBId: TWideStringField;
    QrNFeBide_cUF: TSmallintField;
    QrNFeBide_natOp: TWideStringField;
    QrNFeBide_indPag: TSmallintField;
    QrNFeBide_mod: TSmallintField;
    QrNFeBide_serie: TIntegerField;
    QrNFeBide_nNF: TIntegerField;
    QrNFeBide_dEmi: TDateField;
    QrNFeBide_dSaiEnt: TDateField;
    QrNFeBide_hSaiEnt: TTimeField;
    QrNFeBide_tpNF: TSmallintField;
    QrNFeBide_cMunFG: TIntegerField;
    QrNFeBide_tpImp: TSmallintField;
    QrNFeBide_tpEmis: TSmallintField;
    QrNFeBide_cDV: TSmallintField;
    QrNFeBide_tpAmb: TSmallintField;
    QrNFeBide_finNFe: TSmallintField;
    QrNFeBide_procEmi: TSmallintField;
    QrNFeBide_verProc: TWideStringField;
    QrNFeBide_dhCont: TDateTimeField;
    QrNFeBide_xJust: TWideStringField;
    TabSheet5: TTabSheet;
    DBGrid3: TDBGrid;
    QrErrB: TmySQLQuery;
    DsErrB: TDataSource;
    QrErrI: TmySQLQuery;
    DsErrI: TDataSource;
    DBGrid4: TDBGrid;
    QrErrInItem: TIntegerField;
    QrErrIGraGruX: TIntegerField;
    QrErrIprod_xProd: TWideStringField;
    QrErrIprod_uCom: TWideStringField;
    QrErrIprod_qCom: TFloatField;
    QrErrIprod_vUnCom: TFloatField;
    QrErrIprod_vProd: TFloatField;
    QrNotBSeqArq: TIntegerField;
    QrNotBSeqNFe: TIntegerField;
    QrNotBversao: TFloatField;
    QrNotBId: TWideStringField;
    QrNotBide_cUF: TSmallintField;
    QrNotBide_natOp: TWideStringField;
    QrNotBide_indPag: TSmallintField;
    QrNotBide_mod: TSmallintField;
    QrNotBide_serie: TIntegerField;
    QrNotBide_nNF: TIntegerField;
    QrNotBide_dEmi: TDateField;
    QrNotBide_dSaiEnt: TDateField;
    QrNotBide_hSaiEnt: TTimeField;
    QrNotBide_tpNF: TSmallintField;
    QrNotBide_cMunFG: TIntegerField;
    QrNotBide_tpImp: TSmallintField;
    QrNotBide_tpEmis: TSmallintField;
    QrNotBide_cDV: TSmallintField;
    QrNotBide_tpAmb: TSmallintField;
    QrNotBide_finNFe: TSmallintField;
    QrNotBide_procEmi: TSmallintField;
    QrNotBide_verProc: TWideStringField;
    QrNotBide_dhCont: TDateTimeField;
    QrNotBide_xJust: TWideStringField;
    QrErrBSeqArq: TIntegerField;
    QrErrBSeqNFe: TIntegerField;
    QrErrBversao: TFloatField;
    QrErrBId: TWideStringField;
    QrErrBide_cUF: TSmallintField;
    QrErrBide_natOp: TWideStringField;
    QrErrBide_indPag: TSmallintField;
    QrErrBide_mod: TSmallintField;
    QrErrBide_serie: TIntegerField;
    QrErrBide_nNF: TIntegerField;
    QrErrBide_dEmi: TDateField;
    QrErrBide_dSaiEnt: TDateField;
    QrErrBide_hSaiEnt: TTimeField;
    QrErrBide_tpNF: TSmallintField;
    QrErrBide_cMunFG: TIntegerField;
    QrErrBide_tpImp: TSmallintField;
    QrErrBide_tpEmis: TSmallintField;
    QrErrBide_cDV: TSmallintField;
    QrErrBide_tpAmb: TSmallintField;
    QrErrBide_finNFe: TSmallintField;
    QrErrBide_procEmi: TSmallintField;
    QrErrBide_verProc: TWideStringField;
    QrErrBide_dhCont: TDateTimeField;
    QrErrBide_xJust: TWideStringField;
    QrErrBErrEmit: TSmallintField;
    QrErrBErrDest: TSmallintField;
    QrErrBErrProd: TSmallintField;
    QrLocAnt: TmySQLQuery;
    QrLocAntFatID: TIntegerField;
    QrLocAntFatNum: TIntegerField;
    QrLocAntEmpresa: TIntegerField;
    QrNFeC: TmySQLQuery;
    DsNFeC: TDataSource;
    QrNFeCemit_CNPJ: TWideStringField;
    QrNFeCemit_CPF: TWideStringField;
    QrNFeCemit_xNome: TWideStringField;
    QrNFeCemit_xFant: TWideStringField;
    QrNFeCemit_xLgr: TWideStringField;
    QrNFeCemit_nro: TWideStringField;
    QrNFeCemit_xCpl: TWideStringField;
    QrNFeCemit_xBairro: TWideStringField;
    QrNFeCemit_cMun: TIntegerField;
    QrNFeCemit_xMun: TWideStringField;
    QrNFeCemit_UF: TWideStringField;
    QrNFeCemit_CEP: TIntegerField;
    QrNFeCemit_cPais: TIntegerField;
    QrNFeCemit_xPais: TWideStringField;
    QrNFeCemit_fone: TWideStringField;
    QrNFeCemit_IE: TWideStringField;
    QrNFeCemit_IEST: TWideStringField;
    QrNFeCemit_IM: TWideStringField;
    QrNFeCemit_CNAE: TWideStringField;
    QrNFeCemit_CRT: TSmallintField;
    QrNFeCemit_xCNAE: TWideStringField;
    QrNFeCemit_xCRT: TWideStringField;
    QrNFeCCodInfoEmit: TIntegerField;
    QrNFeBide_cNF: TIntegerField;
    QrNFeE: TmySQLQuery;
    DsNFeE: TDataSource;
    QrNFeEdest_CNPJ: TWideStringField;
    QrNFeEdest_CPF: TWideStringField;
    QrNFeEdest_xNome: TWideStringField;
    QrNFeEdest_xLgr: TWideStringField;
    QrNFeEdest_nro: TWideStringField;
    QrNFeEdest_xCpl: TWideStringField;
    QrNFeEdest_xBairro: TWideStringField;
    QrNFeEdest_cMun: TIntegerField;
    QrNFeEdest_xMun: TWideStringField;
    QrNFeEdest_UF: TWideStringField;
    QrNFeEdest_CEP: TWideStringField;
    QrNFeEdest_cPais: TIntegerField;
    QrNFeEdest_xPais: TWideStringField;
    QrNFeEdest_fone: TWideStringField;
    QrNFeEdest_IE: TWideStringField;
    QrNFeEdest_ISUF: TWideStringField;
    QrNFeEdest_email: TWideStringField;
    QrNFeECodInfoDest: TIntegerField;
    QrNFeW02: TmySQLQuery;
    QrNFeW02ICMSTot_vBC: TFloatField;
    QrNFeW02ICMSTot_vICMS: TFloatField;
    QrNFeW02ICMSTot_vBCST: TFloatField;
    QrNFeW02ICMSTot_vST: TFloatField;
    QrNFeW02ICMSTot_vProd: TFloatField;
    QrNFeW02ICMSTot_vFrete: TFloatField;
    QrNFeW02ICMSTot_vSeg: TFloatField;
    QrNFeW02ICMSTot_vDesc: TFloatField;
    QrNFeW02ICMSTot_vII: TFloatField;
    QrNFeW02ICMSTot_vIPI: TFloatField;
    QrNFeW02ICMSTot_vPIS: TFloatField;
    QrNFeW02ICMSTot_vCOFINS: TFloatField;
    QrNFeW02ICMSTot_vOutro: TFloatField;
    QrNFeW02ICMSTot_vNF: TFloatField;
    QrNFeW17: TmySQLQuery;
    QrNFeW17ISSQNtot_vServ: TFloatField;
    QrNFeW17ISSQNtot_vBC: TFloatField;
    QrNFeW17ISSQNtot_vISS: TFloatField;
    QrNFeW17ISSQNtot_vPIS: TFloatField;
    QrNFeW17ISSQNtot_vCOFINS: TFloatField;
    DsNFeW02: TDataSource;
    DsNFeW17: TDataSource;
    QrNFeW23: TmySQLQuery;
    DsNFeW23: TDataSource;
    QrNFeX01: TmySQLQuery;
    DsNFeX01: TDataSource;
    QrNFeW23RetTrib_vRetPIS: TFloatField;
    QrNFeW23RetTrib_vRetCOFINS: TFloatField;
    QrNFeW23RetTrib_vRetCSLL: TFloatField;
    QrNFeW23RetTrib_vBCIRRF: TFloatField;
    QrNFeW23RetTrib_vIRRF: TFloatField;
    QrNFeW23RetTrib_vBCRetPrev: TFloatField;
    QrNFeW23RetTrib_vRetPrev: TFloatField;
    DsNFeY01: TDataSource;
    QrNFeY01: TmySQLQuery;
    QrNFeX01ModFrete: TSmallintField;
    QrNFeX01xModFrete: TWideStringField;
    QrNFeX01Transporta_CNPJ: TWideStringField;
    QrNFeX01Transporta_CPF: TWideStringField;
    QrNFeX01Transporta_XNome: TWideStringField;
    QrNFeX01Transporta_IE: TWideStringField;
    QrNFeX01Transporta_XEnder: TWideStringField;
    QrNFeX01Transporta_XMun: TWideStringField;
    QrNFeX01Transporta_UF: TWideStringField;
    QrNFeX01RetTransp_vServ: TFloatField;
    QrNFeX01RetTransp_vBCRet: TFloatField;
    QrNFeX01RetTransp_PICMSRet: TFloatField;
    QrNFeX01RetTransp_vICMSRet: TFloatField;
    QrNFeX01RetTransp_CFOP: TWideStringField;
    QrNFeX01RetTransp_CMunFG: TWideStringField;
    QrNFeX01RetTransp_xMunFG: TWideStringField;
    QrNFeX01VeicTransp_Placa: TWideStringField;
    QrNFeX01VeicTransp_UF: TWideStringField;
    QrNFeX01VeicTransp_RNTC: TWideStringField;
    QrNFeX01Vagao: TWideStringField;
    QrNFeX01Balsa: TWideStringField;
    DsNFeZ01: TDataSource;
    QrNFeZ01: TmySQLQuery;
    QrNFeY01Cobr_Fat_nFat: TWideStringField;
    QrNFeY01Cobr_Fat_vOrig: TFloatField;
    QrNFeY01Cobr_Fat_vDesc: TFloatField;
    QrNFeY01Cobr_Fat_vLiq: TFloatField;
    QrNFeZA01: TmySQLQuery;
    DsNFeZA01: TDataSource;
    QrNFeZ01InfAdic_InfAdFisco: TWideMemoField;
    QrNFeZ01InfAdic_InfCpl: TWideMemoField;
    DsNFeZB01: TDataSource;
    QrNFeZB01: TmySQLQuery;
    QrNFeZA01Exporta_UFEmbarq: TWideStringField;
    QrNFeZA01Exporta_XLocEmbarq: TWideStringField;
    DsPR01: TDataSource;
    QrPR01: TmySQLQuery;
    QrNFeZB01Compra_XNEmp: TWideStringField;
    QrNFeZB01Compra_XPed: TWideStringField;
    QrNFeZB01Compra_XCont: TWideStringField;
    QrCR01: TmySQLQuery;
    DsCR01: TDataSource;
    QrPR01protNFe_versao: TFloatField;
    QrPR01infProt_Id: TWideStringField;
    QrPR01infProt_tpAmb: TSmallintField;
    QrPR01infProt_xAmb: TWideStringField;
    QrPR01infProt_verAplic: TWideStringField;
    QrPR01infProt_chNFe: TWideStringField;
    QrPR01infProt_dhRecbto: TDateTimeField;
    QrPR01infProt_nProt: TLargeintField;
    QrPR01infProt_digVal: TWideStringField;
    QrPR01infProt_cStat: TIntegerField;
    QrPR01infProt_xMotivo: TWideStringField;
    QrDR01: TmySQLQuery;
    DsDR01: TDataSource;
    QrCR01retCanc_versao: TFloatField;
    QrCR01infCanc_Id: TWideStringField;
    QrCR01infCanc_tpAmb: TSmallintField;
    QrCR01infCanc_xAmb: TWideStringField;
    QrCR01infCanc_verAplic: TWideStringField;
    QrCR01infCanc_cStat: TIntegerField;
    QrCR01infCanc_xMotivo: TWideStringField;
    QrCR01infCanc_cUF: TSmallintField;
    QrCR01infCanc_xUF: TWideStringField;
    QrCR01infCanc_chNFe: TWideStringField;
    QrCR01infCanc_dhRecbto: TDateTimeField;
    QrCR01infCanc_nProt: TLargeintField;
    Panel16: TPanel;
    EdFatID: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEdit;
    Label5: TLabel;
    QrNFeB12a: TmySQLQuery;
    DsNFeB12a: TDataSource;
    QrNFeB12arefNFe: TWideStringField;
    QrNFeB12arefNF_cUF: TSmallintField;
    QrNFeB12arefNF_xUF: TWideStringField;
    QrNFeB12arefNF_AAMM: TIntegerField;
    QrNFeB12arefNF_CNPJ: TWideStringField;
    QrNFeB12arefNF_mod: TSmallintField;
    QrNFeB12arefNF_serie: TIntegerField;
    QrNFeB12arefNF_nNF: TIntegerField;
    QrNFeB12arefNFP_cUF: TSmallintField;
    QrNFeB12arefNFP_xUF: TWideStringField;
    QrNFeB12arefNFP_AAMM: TSmallintField;
    QrNFeB12arefNFP_CNPJ: TWideStringField;
    QrNFeB12arefNFP_CPF: TWideStringField;
    QrNFeB12arefNFP_IE: TWideStringField;
    QrNFeB12arefNFP_mod: TSmallintField;
    QrNFeB12arefNFP_serie: TSmallintField;
    QrNFeB12arefNFP_nNF: TIntegerField;
    QrNFeB12arefCTe: TWideStringField;
    QrNFeB12arefECF_mod: TWideStringField;
    QrNFeB12arefECF_nECF: TSmallintField;
    QrNFeB12arefECF_nCOO: TIntegerField;
    QrNFeF: TmySQLQuery;
    DsNFeF: TDataSource;
    DsNFeG: TDataSource;
    QrNFeG: TmySQLQuery;
    QrNFeFretirada_CNPJ: TWideStringField;
    QrNFeFretirada_CPF: TWideStringField;
    QrNFeFretirada_xLgr: TWideStringField;
    QrNFeFretirada_nro: TWideStringField;
    QrNFeFretirada_xCpl: TWideStringField;
    QrNFeFretirada_xBairro: TWideStringField;
    QrNFeFretirada_cMun: TIntegerField;
    QrNFeFretirada_xMun: TWideStringField;
    QrNFeFretirada_UF: TWideStringField;
    QrNFeGentrega_CNPJ: TWideStringField;
    QrNFeGentrega_CPF: TWideStringField;
    QrNFeGentrega_xLgr: TWideStringField;
    QrNFeGentrega_nro: TWideStringField;
    QrNFeGentrega_xCpl: TWideStringField;
    QrNFeGentrega_xBairro: TWideStringField;
    QrNFeGentrega_cMun: TIntegerField;
    QrNFeGentrega_xMun: TWideStringField;
    QrNFeGentrega_UF: TWideStringField;
    QrNFeX22: TmySQLQuery;
    DsNFeX22: TDataSource;
    QrNFeX22placa: TWideStringField;
    QrNFeX22UF: TWideStringField;
    QrNFeX22RNTC: TWideStringField;
    QrNFeX26: TmySQLQuery;
    DsNFeX26: TDataSource;
    QrNFeX26qVol: TFloatField;
    QrNFeX26esp: TWideStringField;
    QrNFeX26marca: TWideStringField;
    QrNFeX26nVol: TWideStringField;
    QrNFeX26pesoL: TFloatField;
    QrNFeX26pesoB: TFloatField;
    QrNFeX33: TmySQLQuery;
    DsNFeX33: TDataSource;
    QrNFeX33nLacre: TWideStringField;
    QrNFeX26SeqArq: TIntegerField;
    QrNFeX26SeqNFe: TIntegerField;
    QrNFeX26Controle: TIntegerField;
    QrNFeY07: TmySQLQuery;
    DsNFeY07: TDataSource;
    QrNFeY07nDup: TWideStringField;
    QrNFeY07dVenc: TDateField;
    QrNFeY07vDup: TFloatField;
    QrNFeZ04: TmySQLQuery;
    DsNFeZ04: TDataSource;
    QrNFeZ04xCampo: TWideStringField;
    QrNFeZ04xTexto: TWideStringField;
    DsNFeZ07: TDataSource;
    QrNFeZ07: TmySQLQuery;
    QrNFeZ07xCampo: TWideStringField;
    QrNFeZ07xTexto: TWideStringField;
    QrNFeZ10: TmySQLQuery;
    DsNFeZ10: TDataSource;
    QrNFeZ10nProc: TWideStringField;
    QrNFeZ10indProc: TSmallintField;
    QrItsI: TmySQLQuery;
    DsItsI: TDataSource;
    QrItsIprod_cProd: TWideStringField;
    QrItsIprod_cEAN: TWideStringField;
    QrItsIprod_xProd: TWideStringField;
    QrItsIprod_NCM: TWideStringField;
    QrItsIprod_EXTIPI: TWideStringField;
    QrItsIprod_CFOP: TIntegerField;
    QrItsIprod_uCom: TWideStringField;
    QrItsIprod_qCom: TFloatField;
    QrItsIprod_vUnCom: TFloatField;
    QrItsIprod_vProd: TFloatField;
    QrItsIprod_cEANTrib: TWideStringField;
    QrItsIprod_uTrib: TWideStringField;
    QrItsIprod_qTrib: TFloatField;
    QrItsIprod_vUnTrib: TFloatField;
    QrItsIprod_vFrete: TFloatField;
    QrItsIprod_vSeg: TFloatField;
    QrItsIprod_vDesc: TFloatField;
    QrItsIprod_vOutro: TFloatField;
    QrItsIprod_indTot: TSmallintField;
    QrItsIprod_xPed: TWideStringField;
    QrItsIprod_nItemPed: TIntegerField;
    QrItsIGraGruX: TIntegerField;
    QrItsInItem: TIntegerField;
    QrItsIDi: TmySQLQuery;
    DsItsIDi: TDataSource;
    QrItsIDiDI_nDI: TWideStringField;
    QrItsIDiDI_dDI: TDateField;
    QrItsIDiDI_xLocDesemb: TWideStringField;
    QrItsIDiDI_UFDesemb: TWideStringField;
    QrItsIDiDI_dDesemb: TDateField;
    QrItsIDiDI_cExportador: TWideStringField;
    QrItsIDiControle: TIntegerField;
    QrItsIDiA: TmySQLQuery;
    DsItsIDiA: TDataSource;
    QrItsIDiADI_nDI: TWideStringField;
    QrItsIDiAAdi_nAdicao: TIntegerField;
    QrItsIDiAAdi_nSeqAdic: TIntegerField;
    QrItsIDiAAdi_cFabricante: TWideStringField;
    QrItsIDiAAdi_vDescDI: TFloatField;
    QrItsN: TmySQLQuery;
    DsItsN: TDataSource;
    QrItsNICMS_Orig: TSmallintField;
    QrItsNICMS_CST: TSmallintField;
    QrItsNICMS_modBC: TSmallintField;
    QrItsNICMS_pRedBC: TFloatField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_modBCST: TSmallintField;
    QrItsNICMS_pMVAST: TFloatField;
    QrItsNICMS_pRedBCST: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_pICMSST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsNICMS_CSOSN: TIntegerField;
    QrItsNICMS_UFST: TWideStringField;
    QrItsNICMS_pBCOp: TFloatField;
    QrItsNICMS_vBCSTRet: TFloatField;
    QrItsNICMS_vICMSSTRet: TFloatField;
    QrItsNICMS_motDesICMS: TSmallintField;
    QrItsNICMS_pCredSN: TFloatField;
    QrItsNICMS_vCredICMSSN: TFloatField;
    QrItsO: TmySQLQuery;
    DsItsO: TDataSource;
    DsItsP: TDataSource;
    QrItsP: TmySQLQuery;
    DsItsQ: TDataSource;
    QrItsQ: TmySQLQuery;
    QrItsPII_vBC: TFloatField;
    QrItsPII_vDespAdu: TFloatField;
    QrItsPII_vII: TFloatField;
    QrItsPII_vIOF: TFloatField;
    QrItsQPIS_CST: TSmallintField;
    QrItsQPIS_vBC: TFloatField;
    QrItsQPIS_pPIS: TFloatField;
    QrItsQPIS_vPIS: TFloatField;
    QrItsQPIS_qBCProd: TFloatField;
    QrItsQPIS_vAliqProd: TFloatField;
    QrItsQPIS_fatorBC: TFloatField;
    QrItsR: TmySQLQuery;
    DsItsR: TDataSource;
    QrItsS: TmySQLQuery;
    DsItsS: TDataSource;
    QrItsRPISST_vBC: TFloatField;
    QrItsRPISST_pPIS: TFloatField;
    QrItsRPISST_qBCProd: TFloatField;
    QrItsRPISST_vAliqProd: TFloatField;
    QrItsRPISST_vPIS: TFloatField;
    QrItsRPISST_fatorBCST: TFloatField;
    QrItsT: TmySQLQuery;
    DsItsT: TDataSource;
    QrItsU: TmySQLQuery;
    DsItsU: TDataSource;
    QrItsSCOFINS_CST: TSmallintField;
    QrItsSCOFINS_vBC: TFloatField;
    QrItsSCOFINS_pCOFINS: TFloatField;
    QrItsSCOFINS_qBCProd: TFloatField;
    QrItsSCOFINS_vAliqProd: TFloatField;
    QrItsSCOFINS_vCOFINS: TFloatField;
    QrItsSCOFINS_fatorBC: TFloatField;
    QrItsTCOFINSST_vBC: TFloatField;
    QrItsTCOFINSST_pCOFINS: TFloatField;
    QrItsTCOFINSST_qBCProd: TFloatField;
    QrItsTCOFINSST_vAliqProd: TFloatField;
    QrItsTCOFINSST_vCOFINS: TFloatField;
    QrItsTCOFINSST_fatorBCST: TFloatField;
    QrItsUISSQN_vBC: TFloatField;
    QrItsUISSQN_vAliq: TFloatField;
    QrItsUISSQN_vISSQN: TFloatField;
    QrItsUISSQN_cMunFG: TIntegerField;
    QrItsUISSQN_cListServ: TIntegerField;
    QrItsUISSQN_cSitTrib: TWideStringField;
    QrItsOIPI_clEnq: TWideStringField;
    QrItsOIPI_CNPJProd: TWideStringField;
    QrItsOIPI_cSelo: TWideStringField;
    QrItsOIPI_qSelo: TFloatField;
    QrItsOIPI_cEnq: TWideStringField;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_vBC: TFloatField;
    QrItsOIPI_qUnid: TFloatField;
    QrItsOIPI_vUnid: TFloatField;
    QrItsOIPI_pIPI: TFloatField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsV: TmySQLQuery;
    DsItsV: TDataSource;
    QrItsVInfAdProd: TWideMemoField;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    DBGrid5: TDBGrid;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid2: TDBGrid;
    QrLocID: TmySQLQuery;
    QrLocIDFatID: TIntegerField;
    QrLocIDFatNum: TIntegerField;
    QrLocIDEmpresa: TIntegerField;
    DBGrid6: TDBGrid;
    QrDR01retInutNFe_versao: TFloatField;
    QrDR01retInutNFe_Id: TWideStringField;
    QrDR01retInutNFe_tpAmb: TSmallintField;
    QrDR01retInutNFe_xAmb: TWideStringField;
    QrDR01retInutNFe_verAplic: TWideStringField;
    QrDR01retInutNFe_cStat: TIntegerField;
    QrDR01retInutNFe_xMotivo: TWideStringField;
    QrDR01retInutNFe_cUF: TSmallintField;
    QrDR01retInutNFe_xUF: TWideStringField;
    QrDR01retInutNFe_ano: TSmallintField;
    QrDR01retInutNFe_CNPJ: TWideStringField;
    QrDR01retInutNFe_mod: TSmallintField;
    QrDR01retInutNFe_serie: TIntegerField;
    QrDR01retInutNFe_nNFIni: TIntegerField;
    QrDR01retInutNFe_nNFFin: TIntegerField;
    QrDR01retInutNFe_dhRecbto: TDateTimeField;
    QrDR01retInutNFe_nProt: TLargeintField;
    QrInut: TmySQLQuery;
    QrJust: TmySQLQuery;
    QrInutinutNFe_xJust: TWideStringField;
    QrJustCodigo: TIntegerField;
    QrNFeArqs: TmySQLQuery;
    QrNFeArqsArqDir: TWideStringField;
    QrNFeArqsArqName: TWideStringField;
    QrDR01SeqArq: TIntegerField;
    QrNFeInut: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel17: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PBx: TProgressBar;
    GBRodaPe: TGroupBox;
    Panel18: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BtCarregar: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox5: TGroupBox;
    Panel2: TPanel;
    Panel19: TPanel;
    BitBtn6: TBitBtn;
    GroupBox6: TGroupBox;
    Panel14: TPanel;
    Panel15: TPanel;
    BtEmit: TBitBtn;
    BtDest: TBitBtn;
    BtProd: TBitBtn;
    BitBtn4: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBArquivoClick(Sender: TObject);
    procedure SbCarregaClick(Sender: TObject);
    procedure BtCarregarClick(Sender: TObject);
    procedure CkSoAmbiente1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrErrBAfterOpen(DataSet: TDataSet);
    procedure QrErrBBeforeClose(DataSet: TDataSet);
    procedure QrErrBAfterScroll(DataSet: TDataSet);
    procedure QrErrIAfterScroll(DataSet: TDataSet);
    procedure BtDestClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrNFeX26BeforeClose(DataSet: TDataSet);
    procedure QrNFeX26AfterScroll(DataSet: TDataSet);
    procedure QrItsIAfterScroll(DataSet: TDataSet);
    procedure QrItsIDiAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FSeqArq, FSeqNFe: Integer;
    procedure ImportaNFes();
    procedure AbreCarregados();
    function ExcluiNotaAntiga(var FatNum: Integer): Boolean;
    function IncluiNFeCabA(const FatID: Integer; var FatNum: Integer; const
             Empresa: Integer): Boolean;
    function IncluiNFeCabB(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabF(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabG(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabXReb(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabXVol(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabXLac(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabY(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabZCon(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabZFis(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabZPro(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeItsI(FatID, FatNum, Empresa: Integer): Boolean;
    {
    function IncluiNFeItsIDi(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsIDiA(FatID, FatNum, Empresa, nItem, Controle: Integer): Boolean;
    }
    function IncluiNFeItsN(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsO(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsP(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsQ(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsR(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsS(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsT(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsU(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsV(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    //
    function ObtemXML_De_Arquivo(Arquivo: String): WideString;
    //
    procedure UpdateInfoErros();
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_Dir: TFmNFeLoad_Dir;

implementation

uses UnMyObjects, NFeLoad_Arq, ModuleGeral, ModuleNFe_0000, UMySQLModule,
Module, NFeXMLGerencia, DmkDAC_PF;

{$R *.DFM}

procedure TFmNFeLoad_Dir.AbreCarregados();
  procedure ReabreQuery(qry: TmySQLQuery; Filtro1, Filtro2: String);
  begin
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT a.SeqArq, a.SeqNFe, a.versao, a.Id,');
    Qry.SQL.Add('b.ide_cUF, b.ide_cNF, b.ide_natOp, b.ide_indPag, b.ide_mod,');
    Qry.SQL.Add('b.ide_serie, b.ide_nNF, b.ide_dEmi, b.ide_dSaiEnt,');
    Qry.SQL.Add('b.ide_hSaiEnt, b.ide_tpNF, b.ide_cMunFG, b.ide_tpImp,');
    Qry.SQL.Add('b.ide_tpEmis, b.ide_cDV, b.ide_tpAmb, b.ide_finNFe,');
    Qry.SQL.Add('b.ide_procEmi, b.ide_verProc, b.ide_dhCont, b.ide_xJust,');
    Qry.SQL.Add('a.ErrEmit, a.ErrDest, a.ErrTrsp, a.ErrProd');
    Qry.SQL.Add('FROM _nfe_b_ b');
    Qry.SQL.Add('LEFT JOIN _nfe_a_ a ON a.SeqArq=b.SeqArq AND a.SeqNFe=b.SeqNFe');
    Qry.SQL.Add(Filtro1);
    Qry.SQL.Add(Filtro2);
    Qry.SQL.Add('ORDER BY b.ide_mod, b.ide_serie, b.ide_nNF');
    //
    UnDmkDAC_PF.AbreQuery(Qry, DModG.MyPID_DB);
  end;
var
  tpAmbs: String;
begin
  if CkSoAmbiente1.Checked then
    tpAmbs := '1'
  else
    tpAmbs := '1,2';
  ReabreQuery(QrNFeB,
    'WHERE b.ide_tpAmb IN (' + tpAmbs + ')',
    'AND ErrEmit<1 AND ErrTrsp < 1 AND ErrDest<1 AND ErrProd<1');
  ReabreQuery(QrErrB,
    'WHERE b.ide_tpAmb IN (' + tpAmbs + ')',
    'AND (ErrEmit>0 OR ErrDest>0 OR ErrTrsp >0 OR ErrProd>0)');
  ReabreQuery(QrNotB,
  'WHERE NOT (b.ide_tpAmb IN (' + tpAmbs + '))', '');
  //
  QrCR01.Close;
  QrCR01.SQL.Clear;
  QrCR01.SQL.Add('SELECT');
  QrCR01.SQL.Add('retCanc_versao, infCanc_Id, infCanc_tpAmb,');
  QrCR01.SQL.Add('infCanc_xAmb, infCanc_verAplic, infCanc_cStat,');
  QrCR01.SQL.Add('infCanc_xMotivo, infCanc_cUF, infCanc_xUF,');
  QrCR01.SQL.Add('infCanc_chNFe, infCanc_dhRecbto, infCanc_nProt');
  QrCR01.SQL.Add('FROM _nfe_cr01_');
  QrCR01.SQL.Add('WHERE infCanc_tpAmb in (' + tpAmbs + ')');
  //
  UnDmkDAC_PF.AbreQuery(QrCR01, DModG.MyPID_DB);
  //
  QrDR01.Close;
  QrDR01.SQL.Clear;
  QrDR01.SQL.Add('SELECT SeqArq, ');
  QrDR01.SQL.Add('retInutNFe_versao, retInutNFe_Id, retInutNFe_tpAmb,');
  QrDR01.SQL.Add('retInutNFe_xAmb, retInutNFe_verAplic, retInutNFe_cStat,');
  QrDR01.SQL.Add('retInutNFe_xMotivo, retInutNFe_cUF, retInutNFe_xUF,');
  QrDR01.SQL.Add('retInutNFe_ano, retInutNFe_CNPJ, retInutNFe_mod,');
  QrDR01.SQL.Add('retInutNFe_serie, retInutNFe_nNFIni, retInutNFe_nNFFin,');
  QrDR01.SQL.Add('retInutNFe_dhRecbto, retInutNFe_nProt');
  QrDR01.SQL.Add('FROM _nfe_dr01_');
  QrDR01.SQL.Add('WHERE retInutNFe_tpAmb in (' + tpAmbs + ')');
  //
  UnDmkDAC_PF.AbreQuery(QrDR01, DModG.MyPID_DB);
  //
  BtInclui.Enabled := (QrErrB.RecordCount = 0)
  and (QrNFeB.RecordCount + QrCR01.RecordCount + QrDR01.RecordCount > 0);
end;

procedure TFmNFeLoad_Dir.BtCarregarClick(Sender: TObject);
var
  I, J: Integer;
  Ele_Pai: String;
  Importa, Achou: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    if MyObjects.FormCria(TFmNFeLoad_Arq, FmNFeLoad_Arq) then
    try
      FmNFeLoad_Arq.FAvisaErros := False;
      //
      // Recria tabelas e reseta variaveis do form
      FmNFeLoad_Arq.ResetaVarsETabs();
      //
      PBx.Position := 0;
      PBx.Max := LBArqs.Items.Count;
      //
      for I := 0 to LBArqs.Items.Count - 1 do
      begin
        Importa := False;
        Achou   := False;
        //
        PBx.Position := PBx.Position + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
            ExtractFileName(LBArqs.Items[I]) + '... analisando');
        //
        XMLDocument1.Active := False;
        XMLDocument1.FileName := LBArqs.Items[I];
        XMLDocument1.Active := True;
        Ele_Pai := XMLDocument1.DocumentElement.NodeName;
        //
        if LowerCase(Ele_Pai) = 'nfeproc' then
          Importa := Ck_nfeProc.Checked
        else
        if LowerCase(Ele_Pai) = 'nfe' then
          Importa := Ck_NFe.Checked
        else
        if LowerCase(Ele_Pai) = 'proccancnfe' then
          Importa := Ck_procCancNFe.Checked
        else
        if LowerCase(Ele_Pai) = 'procinutnfe' then
          Importa := Ck_procInutNFe.Checked
        else
        if (LowerCase(Ele_Pai) = 'cancnfe')
        or (LowerCase(Ele_Pai) = 'inutnfe')
        or (LowerCase(Ele_Pai) = 'retcancnfe')
        or (LowerCase(Ele_Pai) = 'retinutnfe') then
        begin
          for J := 0 to LBEleIgnor.Count - 1 do
          begin
            if LBEleIgnor.Items[J] = Ele_Pai then
            begin
              Achou := True;
              Break;
            end;
          end;
          if not Achou then
            LBEleIgnor.Items.Add(Ele_Pai);
        end
        else begin
          Importa := False;
          MeUnknowFile.Lines.Add(LBArqs.Items[I]);
          Achou := False;
          for J := 0 to LBEleDesc.Count - 1 do
          begin
            if LBEleDesc.Items[J] = Ele_Pai then
            begin
              Achou := True;
              Break;
            end;
          end;
          if not Achou then
            LBEleDesc.Items.Add(Ele_Pai);
        end;
        if Importa then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True,
            ExtractFileName(LBArqs.Items[I]) + '... carregando (elemento ' + Ele_Pai + ' )');
          //
          FmNFeLoad_Arq.FSeqArq := FmNFeLoad_Arq.FSeqArq + 1;
          FmNFeLoad_Arq.CarregaArquivoNoTempBD(XMLDocument1, LBArqs.Items[I]);
        end;
      end;
      XMLDocument1.Active := False;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Carregamento finalizado!');
      UpdateInfoErros();
    finally
      FmNFeLoad_Arq.Destroy;
      //
      AbreCarregados();
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFeLoad_Dir.BtDestClick(Sender: TObject);
begin
  FSeqArq := QrErrBSeqArq.Value;
  FSeqNFe := QrErrBSeqArq.Value;
  //
  if MyObjects.FormCria(TFmNFeLoad_Arq, FmNFeLoad_Arq) then
  begin
    FmNFeLoad_Arq.PnEdita.Visible := True;
    //
    FmNFeLoad_Arq.QrArqs.SQL.Clear;
    FmNFeLoad_Arq.QrArqs.SQL.Add('SELECT * FROM _nfe_arqs_');
    FmNFeLoad_Arq.QrArqs.SQL.Add('WHERE SeqArq=' + FormatFloat('0', QrErrBSeqArq.Value));
    //
    UnDmkDAC_PF.AbreQuery(FmNFeLoad_Arq.QrArqs, DModG.MyPID_DB);
    //
    FmNFeLoad_Arq.QrNFeA.SQL.Clear;
    FmNFeLoad_Arq.QrNFeA.SQL.Add('SELECT * FROM _nfe_a_');
    FmNFeLoad_Arq.QrNFeA.SQL.Add('WHERE SeqNFe=' + FormatFloat('0', QrErrBSeqNfe.Value));
    //
    UnDmkDAC_PF.AbreQuery(FmNFeLoad_Arq.QrNFeA, DModG.MyPID_DB);
    //
    FmNFeLoad_Arq.FPermiteAlterarCodInfoDest := True;
    FmNFeLoad_Arq.ShowModal;
    //
    UpdateInfoErros();
    AbreCarregados();
    QrErrB.Locate('SeqArq;SeqNFe', VarArrayOf([FseqArq, FSeqNFe]), []);
    //
    FmNFeLoad_Arq.Destroy;
    //
  end;
end;

procedure TFmNFeLoad_Dir.BtIncluiClick(Sender: TObject);
var
  FatID, FatNum, Empresa(*, CliInt*): Integer;
  x: String;
  // Cancelamento
  Cancela: Boolean;
  // Inutiliza��o
  Codigo, CodUsu, Aplicacao, Justif: Integer;
  Nome, DataC, xJust: String;
  XML_Inu: WideString;
begin
  FatID := 0;
  // Fazer inclus�o!
  if Geral.MB_Pergunta(
  'Caso existam NFs com a mesma serie / n�mero para o emitente,' + sLineBreak +
  'os dados antigos ser�o exclu�dos. Deseja continuar assim mesmo?') = ID_YES then
  if Geral.MB_Pergunta(
  'Confirma os seguintes dados abaixo?' + sLineBreak +
  'FatID = ' + EdFatID.Text + sLineBreak + 'Empresa = ' + EdEmpresa.Text) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    PBx.Position := 0;
    PBx.Max := QrNFeB.RecordCount + QrCR01.RecordCount;
    // verifica FatNum
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MAX(FatNum) FatNum');
    Dmod.QrAux.SQL.Add('FROM nfecaba');
    Dmod.QrAux.SQL.Add('WHERE FatID=' + FormatFloat('0', EdFatID.ValueVariant));
    Dmod.QrAux.SQL.Add('AND Empresa=' + FormatFloat('0', EdEmpresa.ValueVariant));
    //
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    x := FormatFloat('0', Dmod.QrAux.FieldByName('FatNum').AsInteger);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE Controle SET FatPedCab='+x+' WHERE FatPedCab<'+x);
    Dmod.QrUpd.ExecSQL;
    //
    try
      QrNFeB.First;
      while not QrNFeB.Eof do
      begin
        PBx.Position := PBx.Position + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, True, QrNFeBId.Value + ' -  Incluindo');
        //
        QrNFeB12a.Close;
        QrNFeB12a.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeB12a.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeB12a, DModG.MyPID_DB);
        //
        QrNFeC.Close;
        QrNFeC.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeC.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeC, DModG.MyPID_DB);
        //
        QrNFeE.Close;
        QrNFeE.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeE.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeE, DModG.MyPID_DB);
        //
        QrNFeW02.Close;
        QrNFeW02.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeW02.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeW02, DModG.MyPID_DB);
        //
        QrNFeW17.Close;
        QrNFeW17.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeW17.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeW17, DModG.MyPID_DB);
        //
        QrNFeW23.Close;
        QrNFeW23.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeW23.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeW23, DModG.MyPID_DB);
        //
        QrNFeX01.Close;
        QrNFeX01.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeX01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeX01, DModG.MyPID_DB);
        //
        QrNFeX22.Close;
        QrNFeX22.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeX22.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeX22, DModG.MyPID_DB);
        //
        QrNFeX26.Close;
        QrNFeX26.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeX26.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeX26, DModG.MyPID_DB);
        //
        QrNFeY01.Close;
        QrNFeY01.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeY01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeY01, DModG.MyPID_DB);
        //
        QrNFeZ01.Close;
        QrNFeZ01.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeZ01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeZ01, DModG.MyPID_DB);
        //
        QrNFeZA01.Close;
        QrNFeZA01.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeZA01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeZA01, DModG.MyPID_DB);
        //
        QrNFeZB01.Close;
        QrNFeZB01.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrNFeZB01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeZB01, DModG.MyPID_DB);
        //
        QrPR01.Close;
        QrPR01.Params[00].AsInteger := QrNFeBSeqArq.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrPR01, DModG.MyPID_DB);
        //
        QrItsI.Close;
        QrItsI.Params[00].AsInteger := QrNFeBSeqArq.Value;
        QrItsI.Params[01].AsInteger := QrNFeBSeqNFe.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrItsI, DModG.MyPID_DB);
        //
        FatID   := EdFatID.ValueVariant;
        FatNum  := 0;
        Empresa := EdEmpresa.ValueVariant;
        if ExcluiNotaAntiga(FatNum) then
        if IncluiNFeCabA(FatID, FatNum, Empresa) then
        if IncluiNFeCabB(FatID, FatNum, Empresa) then
        if IncluiNFeCabF(FatID, FatNum, Empresa) then
        if IncluiNFeCabG(FatID, FatNum, Empresa) then
        if IncluiNFeCabXReb(FatID, FatNum, Empresa) then
        if IncluiNFeCabXVol(FatID, FatNum, Empresa) then
        if IncluiNFeCabXLac(FatID, FatNum, Empresa) then
        if IncluiNFeCabY(FatID, FatNum, Empresa) then
        if IncluiNFeCabZCon(FatID, FatNum, Empresa) then
        if IncluiNFeCabZFis(FatID, FatNum, Empresa) then
        if IncluiNFeCabZPro(FatID, FatNum, Empresa) then
        if IncluiNFeItsI(FatID, FatNum, Empresa) then
        ;
        //
        QrNFeB.Next;
      end;

      
      // Cancelar NF-es

      QrCR01.First;
      while not QrCR01.Eof do
      begin
        PBx.Position := PBx.Position + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, True, QrCR01infCanc_chNFe.Value + ' -  Cancelando');
        //
        if QrCR01infCanc_chNFe.Value <> '' then
        begin
          QrLocID.Close;
          QrLocID.Params[0].AsString := QrCR01infCanc_chNFe.Value;
          //
          UnDmkDAC_PF.AbreQuery(QrLocID, Dmod.MyDB);
          if QrLocID.RecordCount = 0 then
            Geral.MB_Aviso(
            'N�o foi localizada a NF-e abaixo para ser cancelada:' + sLineBreak +
            'Chave da NF-e: ' + QrCR01infCanc_chNFe.Value)
          else begin
            if QrLocID.RecordCount > 1 then
              Cancela := Geral.MB_Aviso(
              'Foram localizados ' + IntToStr(QrLocID.RecordCount) +
              'registros para a NF-e abaixo para ser cancelada:' + sLineBreak +
              'Chave da NF-e: ' + QrCR01infCanc_chNFe.Value) = ID_YES
            else Cancela := True;
            //
            if Cancela then
            begin
              QrLocID.First;
              while not QrLocID.Eof do
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'retCancNFe_versao',
                'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                'infCanc_dhRecbto', 'infCanc_nProt', (*'infCanc_digVal',*)
                'infCanc_cStat', 'infCanc_xMotivo'], [
                'FatID', 'FatNum', 'Empresa'], [
                QrCR01retCanc_versao.Value,
                QrCR01infCanc_Id.Value, QrCR01infCanc_tpAmb.Value, QrCR01infCanc_verAplic.Value,
                QrCR01infCanc_dhRecbto.Value, QrCR01infCanc_nProt.Value, (*infCanc_digVal,*)
                QrCR01infCanc_cStat.Value, QrCR01infCanc_xMotivo.Value], [
                FatID, FatNum, Empresa], True) then ;
                //
                QrLocID.Next;
              end;
            end;
          end;
        end;
        QrCR01.Next;
      end;
      //



      // Inutilizar numera��es de NF-es

      QrDR01.First;
      while not QrDR01.Eof do
      begin
        PBx.Position := PBx.Position + 1;
        QrNFeInut.Close;
        QrNFeInut.Params[00].AsInteger :=  QrDR01retInutNFe_serie.Value;
        QrNFeInut.Params[01].AsInteger :=  QrDR01retInutNFe_nNFIni.Value;
        QrNFeInut.Params[02].AsInteger :=  QrDR01retInutNFe_nNFFin.Value;
        //
        UnDmkDAC_PF.AbreQuery(QrNFeInut, DModG.MyPID_DB);
        if QrNFeInut.RecordCount = 0 then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, IntToStr(QrDR01retInutNFe_nNFFin.Value) + ' -  Inutilizando');
          //
          if QrDR01retInutNFe_cStat.Value = 102 then
          begin
            Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinut', '', 0);
            CodUsu  := Codigo; // Parei Aqui! N�o pode haver inclus�es anteriores!
            Nome    := 'Importado da base da dados anterior';
            DModG.ObtemEntidadeDeCNPJCFP(QrDR01retInutNFe_CNPJ.Value, Empresa);
            DataC   := Geral.FDT(QrDR01retInutNFe_dhRecbto.Value, 1);
            xJust   := ''; // ??
            //
            QrInut.Close;
            QrInut.Params[00].AsInteger :=  QrDR01retInutNFe_serie.Value;
            QrInut.Params[01].AsInteger :=  QrDR01retInutNFe_nNFIni.Value;
            QrInut.Params[02].AsInteger :=  QrDR01retInutNFe_nNFFin.Value;
            //
            UnDmkDAC_PF.AbreQuery(QrInut, DModG.MyPID_DB);
            //
            if QrInut.RecordCount > 0 then
            begin
              QrJust.Close;
              QrJust.Params[0].AsString := QrInutinutNFe_xJust.Value;
              //
              UnDmkDAC_PF.AbreQuery(QrJust, Dmod.MyDB);
              //
              if QrJust.RecordCount > 0 then
                Justif := QrJustCodigo.Value
              else
              begin
                Aplicacao := 2; // Inutiliza��o
                //Justif := UMyMod.BuscaEmLivreY_Def('nfejust', 'Codigo', stIns, 0);
                Justif := DModG.BuscaProximoCodigoInt('nfectrl', 'nfejust', '', 0);
                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfejust', False, [
                'CodUsu', 'Nome', 'Aplicacao'], [
                'Codigo'], [
                Justif, QrInutinutNFe_xJust.Value, Aplicacao], [
                Justif], True);
              end;
            end else
            begin
              Justif := 0;
            end;

            //Parei aqui! Copiar xml para XML_Inu
            QrNFeArqs.Close;
            QrNFeArqs.Params[0].AsInteger := QrDR01SeqArq.Value;
            //
            UnDmkDAC_PF.AbreQuery(QrNFeArqs, DModG.MyPID_DB);
            //
            XML_Inu := Geral.WideStringToSQLString(
              ObtemXML_De_Arquivo(QrNFeArqsArqDir.Value + '/' + QrNFeArqsArqName.Value));
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinut', False, [
            'CodUsu', 'Nome', 'Empresa',
            'DataC', 'versao', 'Id',
            'tpAmb', 'cUF', 'ano',
            'CNPJ', 'modelo', 'Serie',
            'nNFIni', 'nNFFim', 'Justif',
            'xJust', 'cStat', 'xMotivo',
            'dhRecbto', 'nProt', 'XML_Inu'], [
            'Codigo'], [
            CodUsu, Nome, Empresa,
            DataC, QrDR01retInutNFe_versao.Value, QrDR01retInutNFe_Id.Value,
            QrDR01retInutNFe_tpAmb.Value, QrDR01retInutNFe_cUF.Value,
            QrDR01retInutNFe_ano.Value,
            QrDR01retInutNFe_CNPJ.Value, QrDR01retInutNFe_mod.Value, QrDR01retInutNFe_Serie.Value,
            QrDR01retInutNFe_nNFIni.Value, QrDR01retInutNFe_nNFFin.Value, Justif,
            xJust, QrDR01retInutNFe_cStat.Value, QrDR01retInutNFe_xMotivo.Value,
            QrDR01retInutNFe_dhRecbto.Value, QrDR01retInutNFe_nProt.Value, XML_Inu], [
            Codigo], True) then ;
          end;
        end;
        QrDR01.Next;
      end;

      //

      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Inclus�o finalizada!');
      //
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFeLoad_Dir.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_Dir.CkSoAmbiente1Click(Sender: TObject);
begin
  if QrNfeB.State <> dsInactive then
    AbreCarregados();
end;

function TFmNFeLoad_Dir.ExcluiNotaAntiga(var FatNum: Integer): Boolean;
const
  DeleteDI = True;
  DeleteGA = True;
  Status = 0;
{
var
  FatID, Empresa: Integer;
}
begin
  FatNum := 0;
  QrLocAnt.Close;
  QrLocAnt.Params[00].AsString  := QrNFeCemit_CNPJ.Value;
  QrLocAnt.Params[01].AsInteger := QrNFeBide_mod.Value;
  QrLocAnt.Params[02].AsInteger := QrNFeBide_serie.Value;
  QrLocAnt.Params[03].AsInteger := QrNFeBide_nNF.Value;
  QrLocAnt.Params[04].AsString  := Geral.FDT(QrNFeBide_dEmi.Value, 1);
  //
  UnDmkDAC_PF.AbreQuery(QrLocAnt, DModG.MyPID_DB);
  if QrLocAnt.RecordCount > 1 then
    Geral.MB_Aviso('Foram econtrados ' + IntToStr(QrLocAnt.RecordCount) +
    ' registros para a NF:' + sLineBreak +
    'CNPJ do emitente: ' + QrNFeCemit_CNPJ.Value + sLineBreak +
    'Modelo da NF: ' + IntToStr(QrNFeBide_mod.Value) + sLineBreak +
    'S�rie da NF: ' + IntToStr(QrNFeBide_serie.Value) + sLineBreak +
    'N�mero da NF: ' + IntToStr(QrNFeBide_nNF.Value) + sLineBreak +
    'Data de emiss�o: ' + Geral.FDT(QrNFeBide_dEmi.Value, 2) + sLineBreak + sLineBreak +
    'Por seguran�a, nenhuma ser� exclu�da!')
  else
  if QrLocAnt.RecordCount = 1 then
  begin
    FatNum := QrLocAntFatNum.Value;
    DmNFe_0000.ExcluiNfe(Status, QrLocAntFatID.Value, QrLocAntFatNum.Value,
    QrLocAntEmpresa.Value, DeleteDI, DeleteGA);
  end;
  Result := True;
end;

procedure TFmNFeLoad_Dir.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_Dir.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrNFeB.Database := DModG.MyPID_DB;
  QrErrB.Database := DModG.MyPID_DB;
  QrErrI.Database := DModG.MyPID_DB;
  QrNotB.Database := DModG.MyPID_DB;
  QrNFeC.Database := DModG.MyPID_DB;
  // Parei aqui! colocar outros
  //QrNFe.Database := DModG.MyPID_DB;
  //
  QrJust.DataBase := Dmod.MyDB;
  QrNFeArqs.DataBase := DModG.MyPID_DB;
  QrInut.DataBase := DModG.MyPID_DB;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmNFeLoad_Dir.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeLoad_Dir.ImportaNFes();
var
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    MeUnknowFile.Lines.Clear;
    LBEleDesc.Clear;
    LBArqs.Clear;
    //MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      MyObjects.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], LBArqs, False, LaAviso1, LaAviso2);
    end;
    BtCarregar.Enabled := LBArqs.Items.Count > 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabA(const FatID: Integer; var FatNum: Integer;
  const Empresa: Integer): Boolean;
const
  LoteEnv = 0;
  infCanc_digVal = '';
  infCanc_cJust = 0;
  infCanc_xJust = '';
var
  IDCtrl, Status: Integer;
begin
  IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
{
  N�o tem relacionamento!  Ser� feito a parte depois das inclus�es!
  if QrCR01infCanc_cStat.Value = 101 then
    Status := 101
  else
}
  if QrPR01infProt_cStat.Value = 100 then
    Status := 100
  else
    Status := 0;
  //
  if FatNum = 0 then
    FatNum := UMyMod.BuscaEmLivreY_Def('FatPedCab', 'Codigo', stIns, 0, nil, False);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
  'IDCtrl', 'LoteEnv', 'versao',
  'Id', 'ide_cUF', 'ide_cNF',
  'ide_natOp', 'ide_indPag', 'ide_mod',
  'ide_serie', 'ide_nNF', 'ide_dEmi',
  'ide_dSaiEnt', 'ide_hSaiEnt', 'ide_tpNF',
  'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis',
  'ide_cDV', 'ide_tpAmb', 'ide_finNFe',
  'ide_procEmi', 'ide_verProc', 'ide_dhCont',
  'ide_xJust', 'emit_CNPJ', 'emit_CPF',
  'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_UF',
  'emit_CEP', 'emit_cPais', 'emit_xPais',
  'emit_fone', 'emit_IE', 'emit_IEST',
  'emit_IM', 'emit_CNAE', 'emit_CRT',
  'dest_CNPJ', 'dest_CPF', 'dest_xNome',
  'dest_xLgr', 'dest_nro', 'dest_xCpl',
  'dest_xBairro', 'dest_cMun', 'dest_xMun',
  'dest_UF', 'dest_CEP', 'dest_cPais',
  'dest_xPais', 'dest_fone', 'dest_IE',
  'dest_ISUF', 'dest_email', 'ICMSTot_vBC',
  'ICMSTot_vICMS', 'ICMSTot_vBCST', 'ICMSTot_vST',
  'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
  'ICMSTot_vDesc', 'ICMSTot_vII', 'ICMSTot_vIPI',
  'ICMSTot_vPIS', 'ICMSTot_vCOFINS', 'ICMSTot_vOutro',
  'ICMSTot_vNF', 'ISSQNtot_vServ', 'ISSQNtot_vBC',
  'ISSQNtot_vISS', 'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS',
  'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
  'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
  'RetTrib_vRetPrev', 'ModFrete', 'Transporta_CNPJ',
  'Transporta_CPF', 'Transporta_XNome', 'Transporta_IE',
  'Transporta_XEnder', 'Transporta_XMun', 'Transporta_UF',
  'RetTransp_vServ', 'RetTransp_vBCRet', 'RetTransp_PICMSRet',
  'RetTransp_vICMSRet', 'RetTransp_CFOP', 'RetTransp_CMunFG',
  'VeicTransp_Placa', 'VeicTransp_UF', 'VeicTransp_RNTC',
  'Vagao', 'Balsa', 'Cobr_Fat_nFat',
  'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
  'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
  'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
  'Compra_XCont', 'Status', 'protNFe_versao',
  'infProt_Id', 'infProt_tpAmb', 'infProt_verAplic',
  'infProt_dhRecbto', 'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo', (*'retCancNFe_versao',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
  'infCanc_xJust', (*'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
  'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
  'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
  'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
  'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
  'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',
  'DataFiscal', 'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta',
  'SINTEGRA_ExpNat', 'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta',
  'SINTEGRA_ExpConhNum', 'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip',
  'SINTEGRA_ExpPais', 'SINTEGRA_ExpAverDta',*) 'CodInfoEmit',
  'CodInfoDest'(*, 'CriAForca', 'CodInfoTrsp',
  'OrdemServ', 'Situacao'*)], [
  'FatID', 'FatNum', 'Empresa'], [
  IDCtrl, LoteEnv, QrNFeBversao.Value,
  QrNFeBId.Value, QrNFeBide_cUF.Value, QrNFeBide_cNF.Value,
  QrNFeBide_natOp.Value, QrNFeBide_indPag.Value, QrNFeBide_mod.Value,
  QrNFeBide_serie.Value, QrNFeBide_nNF.Value, QrNFeBide_dEmi.Value,
  QrNFeBide_dSaiEnt.Value, QrNFeBide_hSaiEnt.Value, QrNFeBide_tpNF.Value,
  QrNFeBide_cMunFG.Value, QrNFeBide_tpImp.Value, QrNFeBide_tpEmis.Value,
  QrNFeBide_cDV.Value, QrNFeBide_tpAmb.Value, QrNFeBide_finNFe.Value,
  QrNFeBide_procEmi.Value, QrNFeBide_verProc.Value, QrNFeBide_dhCont.Value,
  QrNFeBide_xJust.Value, QrNFeCemit_CNPJ.Value, QrNFeCemit_CPF.Value,
  QrNFeCemit_xNome.Value, QrNFeCemit_xFant.Value, QrNFeCemit_xLgr.Value,
  QrNFeCemit_nro.Value, QrNFeCemit_xCpl.Value, QrNFeCemit_xBairro.Value,
  QrNFeCemit_cMun.Value, QrNFeCemit_xMun.Value, QrNFeCemit_UF.Value,
  QrNFeCemit_CEP.Value, QrNFeCemit_cPais.Value, QrNFeCemit_xPais.Value,
  QrNFeCemit_fone.Value, QrNFeCemit_IE.Value, QrNFeCemit_IEST.Value,
  QrNFeCemit_IM.Value, QrNFeCemit_CNAE.Value, QrNFeCemit_CRT.Value,
  QrNFeEdest_CNPJ.Value, QrNFeEdest_CPF.Value, QrNFeEdest_xNome.Value,
  QrNFeEdest_xLgr.Value, QrNFeEdest_nro.Value, QrNFeEdest_xCpl.Value,
  QrNFeEdest_xBairro.Value, QrNFeEdest_cMun.Value, QrNFeEdest_xMun.Value,
  QrNFeEdest_UF.Value, QrNFeEdest_CEP.Value, QrNFeEdest_cPais.Value,
  QrNFeEdest_xPais.Value, QrNFeEdest_fone.Value, QrNFeEdest_IE.Value,
  QrNFeEdest_ISUF.Value, QrNFeEdest_email.Value, QrNFeW02ICMSTot_vBC.Value,
  QrNFeW02ICMSTot_vICMS.Value, QrNFeW02ICMSTot_vBCST.Value, QrNFeW02ICMSTot_vST.Value,
  QrNFeW02ICMSTot_vProd.Value, QrNFeW02ICMSTot_vFrete.Value, QrNFeW02ICMSTot_vSeg.Value,
  QrNFeW02ICMSTot_vDesc.Value, QrNFeW02ICMSTot_vII.Value, QrNFeW02ICMSTot_vIPI.Value,
  QrNFeW02ICMSTot_vPIS.Value, QrNFeW02ICMSTot_vCOFINS.Value, QrNFeW02ICMSTot_vOutro.Value,
  QrNFeW02ICMSTot_vNF.Value, QrNFeW17ISSQNtot_vServ.Value, QrNFeW17ISSQNtot_vBC.Value,
  QrNFeW17ISSQNtot_vISS.Value, QrNFeW17ISSQNtot_vPIS.Value, QrNFeW17ISSQNtot_vCOFINS.Value,
  QrNFeW23RetTrib_vRetPIS.Value, QrNFeW23RetTrib_vRetCOFINS.Value, QrNFeW23RetTrib_vRetCSLL.Value,
  QrNFeW23RetTrib_vBCIRRF.Value, QrNFeW23RetTrib_vIRRF.Value, QrNFeW23RetTrib_vBCRetPrev.Value,
  QrNFeW23RetTrib_vRetPrev.Value, QrNFeX01ModFrete.Value, QrNFeX01Transporta_CNPJ.Value,
  QrNFeX01Transporta_CPF.Value, QrNFeX01Transporta_XNome.Value, QrNFeX01Transporta_IE.Value,
  QrNFeX01Transporta_XEnder.Value, QrNFeX01Transporta_XMun.Value, QrNFeX01Transporta_UF.Value,
  QrNFeX01RetTransp_vServ.Value, QrNFeX01RetTransp_vBCRet.Value, QrNFeX01RetTransp_PICMSRet.Value,
  QrNFeX01RetTransp_vICMSRet.Value, QrNFeX01RetTransp_CFOP.Value, QrNFeX01RetTransp_CMunFG.Value,
  QrNFeX01VeicTransp_Placa.Value, QrNFeX01VeicTransp_UF.Value, QrNFeX01VeicTransp_RNTC.Value,
  QrNFeX01Vagao.Value, QrNFeX01Balsa.Value, QrNFeY01Cobr_Fat_nFat.Value,
  QrNFeY01Cobr_Fat_vOrig.Value, QrNFeY01Cobr_Fat_vDesc.Value, QrNFeY01Cobr_Fat_vLiq.Value,
  QrNFeZ01InfAdic_InfAdFisco.Value, QrNFeZ01InfAdic_InfCpl.Value, QrNFeZA01Exporta_UFEmbarq.Value,
  QrNFeZA01Exporta_XLocEmbarq.Value, QrNFeZB01Compra_XNEmp.Value, QrNFeZB01Compra_XPed.Value,
  QrNFeZB01Compra_XCont.Value, Status, QrPR01protNFe_versao.Value,
  QrPR01infProt_Id.Value, QrPR01infProt_tpAmb.Value, QrPR01infProt_verAplic.Value,
  QrPR01infProt_dhRecbto.Value, QrPR01infProt_nProt.Value, QrPR01infProt_digVal.Value,
  QrPR01infProt_cStat.Value, QrPR01infProt_xMotivo.Value, (*QrCR01retCanc_versao.Value,
  QrCR01infCanc_Id.Value, QrCR01infCanc_tpAmb.Value, QrCR01infCanc_verAplic.Value,
  QrCR01infCanc_dhRecbto.Value, QrCR01infCanc_nProt.Value, infCanc_digVal,
  QrCR01infCanc_cStat.Value, QrCR01infCanc_xMotivo.Value, infCanc_cJust,
  infCanc_xJust, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  FreteExtra, SegurExtra, ICMSRec_pRedBC,
  ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
  IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
  COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,
  DataFiscal, SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
  SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
  SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
  SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,*) QrNFeCCodInfoEmit.Value,
  QrNFeECodInfoDest.Value(*, CriAForca, CodInfoTrsp,
  OrdemServ, Situacao*)], [
  FatID, FatNum, Empresa], True);
end;

function TFmNFeLoad_Dir.IncluiNFeCabB(FatID, FatNum, Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeB12a.RecordCount > 0 then
  begin
    QrNFeB12a.First;
    while not QrNFeB12a.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabb', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabb', False, [
      'refNFe', 'refNF_cUF', 'refNF_AAMM',
      'refNF_CNPJ', 'refNF_mod', 'refNF_serie',
      'refNF_nNF', 'refNFP_cUF', 'refNFP_AAMM',
      'refNFP_CNPJ', 'refNFP_CPF', 'refNFP_IE',
      'refNFP_mod', 'refNFP_serie', 'refNFP_nNF',
      'refCTe', 'refECF_mod', 'refECF_nECF',
      'refECF_nCOO'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeB12arefNFe.Value, QrNFeB12arefNF_cUF.Value, QrNFeB12arefNF_AAMM.Value,
      QrNFeB12arefNF_CNPJ.Value, QrNFeB12arefNF_mod.Value, QrNFeB12arefNF_serie.Value,
      QrNFeB12arefNF_nNF.Value, QrNFeB12arefNFP_cUF.Value, QrNFeB12arefNFP_AAMM.Value,
      QrNFeB12arefNFP_CNPJ.Value, QrNFeB12arefNFP_CPF.Value, QrNFeB12arefNFP_IE.Value,
      QrNFeB12arefNFP_mod.Value, QrNFeB12arefNFP_serie.Value, QrNFeB12arefNFP_nNF.Value,
      QrNFeB12arefCTe.Value, QrNFeB12arefECF_mod.Value, QrNFeB12arefECF_nECF.Value,
      QrNFeB12arefECF_nCOO.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeB12a.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabF(FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result := False;
  if QrNFeF.RecordCount > 0 then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabf', False, [
    'retirada_CNPJ', 'retirada_CPF', 'retirada_xLgr',
    'retirada_nro', 'retirada_xCpl', 'retirada_xBairro',
    'retirada_cMun', 'retirada_xMun', 'retirada_UF'], [
    'FatID', 'FatNum', 'Empresa'], [
    QrNFeFretirada_CNPJ.Value, QrNFeFretirada_CPF.Value, QrNFeFretirada_xLgr.Value,
    QrNFeFretirada_nro.Value, QrNFeFretirada_xCpl.Value, QrNFeFretirada_xBairro.Value,
    QrNFeFretirada_cMun.Value, QrNFeFretirada_xMun.Value, QrNFeFretirada_UF.Value], [
    FatID, FatNum, Empresa], True);
    //
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabG(FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result := False;
  if QrNFeG.RecordCount > 0 then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabf', False, [
    'entrega_CNPJ', 'entrega_CPF', 'entrega_xLgr',
    'entrega_nro', 'entrega_xCpl', 'entrega_xBairro',
    'entrega_cMun', 'entrega_xMun', 'entrega_UF'], [
    'FatID', 'FatNum', 'Empresa'], [
    QrNFeGentrega_CNPJ.Value, QrNFeGentrega_CPF.Value, QrNFeGentrega_xLgr.Value,
    QrNFeGentrega_nro.Value, QrNFeGentrega_xCpl.Value, QrNFeGentrega_xBairro.Value,
    QrNFeGentrega_cMun.Value, QrNFeGentrega_xMun.Value, QrNFeGentrega_UF.Value], [
    FatID, FatNum, Empresa], True);
    //
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabXLac(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Conta: Integer;
begin
  Result := False;
  if QrNFeX33.RecordCount > 0 then
  begin
    QrNFeX33.First;
    while not QrNFeX33.Eof do
    begin
      Conta := DModG.BuscaProximoInteiro('nfecabxlac', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxlac', False, [
      'nLacre'], ['FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
      QrNFeX33nLacre.Value], [
      FatID, FatNum, Empresa, QrNFeX26Controle.Value, Conta], True);
      //
      QrNFeX33.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabXReb(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeX22.RecordCount > 0 then
  begin
    QrNFeX22.First;
    while not QrNFeX22.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabxreb', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxreb', False, [
      'placa', 'UF', 'RNTC'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeX22placa.Value, QrNFeX22UF.Value, QrNFeX22RNTC.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeX22.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabXVol(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeX26.RecordCount > 0 then
  begin
    QrNFeX26.First;
    while not QrNFeX26.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabxvol', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
      'qVol', 'esp', 'marca',
      'nVol', 'pesoL', 'pesoB'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeX26qVol.Value, QrNFeX26esp.Value, QrNFeX26marca.Value,
      QrNFeX26nVol.Value, QrNFeX26pesoL.Value, QrNFeX26pesoB.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      IncluiNFeCabXLac(FatID, FatNum, Empresa);
      //
      QrNFeX26.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabY(FatID, FatNum, Empresa: Integer): Boolean;
const
  Lancto = 0;
  Sub = 0;
var
  Controle, FatParcela: Integer;
begin
  Result := False;
  if QrNFeY07.RecordCount > 0 then
  begin
    QrNFeY07.First;
    while not QrNFeY07.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecaby', 'Controle', '', 0);
      FatParcela := QrNFeY07.RecNo;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaby', False, [
      'nDup', 'dVenc', 'vDup',
      'Lancto', 'Sub'], [
      'FatID', 'FatNum', 'FatParcela', 'Empresa', 'Controle'], [
      QrNFeY07nDup.Value, QrNFeY07dVenc.Value, QrNFeY07vDup.Value,
      Lancto, Sub], [
      FatID, FatNum, FatParcela, Empresa, Controle], True);
      //
      QrNFeY07.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabZCon(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeZ04.RecordCount > 0 then
  begin
    QrNFeZ04.First;
    while not QrNFeZ04.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabzcon', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabzcon', False, [
      'xCampo', 'xTexto'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeZ04xCampo.Value, QrNFeZ04xTexto.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeZ04.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabZFis(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeZ07.RecordCount > 0 then
  begin
    QrNFeZ07.First;
    while not QrNFeZ07.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabzfis', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabzfis', False, [
      'xCampo', 'xTexto'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeZ07xCampo.Value, QrNFeZ07xTexto.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeZ07.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeCabZPro(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeZ10.RecordCount > 0 then
  begin
    QrNFeZ10.First;
    while not QrNFeZ10.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabzpro', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabzpro', False, [
      'nProc', 'indProc'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeZ10nProc.Value, QrNFeZ10indProc.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeZ10.Next;
    end;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsI(FatID, FatNum, Empresa: Integer): Boolean;
const
  MeuID = 0;
begin
  QrItsI.First;
  while not QrItsI.Eof do
  begin
    //Controle := DModG.BuscaProximoInteiro('nfeitsi', 'Controle', '', 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
    'prod_cProd', 'prod_cEAN', 'prod_xProd',
    'prod_NCM', 'prod_EXTIPI', (*'prod_genero',*)
    'prod_CFOP', 'prod_uCom', 'prod_qCom',
    'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
    'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
    'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
    'prod_vOutro', 'prod_indTot', 'prod_xPed',
    'prod_nItemPed', (*'Tem_IPI',
    'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
    'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
    'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
    'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
    'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
    'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
    'MeuID', 'Nivel1'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsIprod_cProd.Value, QrItsIprod_cEAN.Value, QrItsIprod_xProd.Value,
    QrItsIprod_NCM.Value, QrItsIprod_EXTIPI.Value, (*prod_genero,*)
    QrItsIprod_CFOP.Value, QrItsIprod_uCom.Value, QrItsIprod_qCom.Value,
    QrItsIprod_vUnCom.Value, QrItsIprod_vProd.Value, QrItsIprod_cEANTrib.Value,
    QrItsIprod_uTrib.Value, QrItsIprod_qTrib.Value, QrItsIprod_vUnTrib.Value,
    QrItsIprod_vFrete.Value, QrItsIprod_vSeg.Value, QrItsIprod_vDesc.Value,
    QrItsIprod_vOutro.Value, QrItsIprod_indTot.Value, QrItsIprod_xPed.Value,
    QrItsIprod_nItemPed.Value, (*QrItsITem_IPI.Value,
    QrItsIInfAdCuztm.Value, QrItsIEhServico.Value, QrItsIICMSRec_pRedBC.Value,
    QrItsIICMSRec_vBC.Value, QrItsIICMSRec_pAliq.Value, QrItsIICMSRec_vICMS.Value,
    QrItsIIPIRec_pRedBC.Value, QrItsIIPIRec_vBC.Value, QrItsIIPIRec_pAliq.Value,
    QrItsIIPIRec_vIPI.Value, QrItsIPISRec_pRedBC.Value, QrItsIPISRec_vBC.Value,
    QrItsIPISRec_pAliq.Value, QrItsIPISRec_vPIS.Value, QrItsICOFINSRec_pRedBC.Value,
    QrItsICOFINSRec_vBC.Value, QrItsICOFINSRec_pAliq.Value, QrItsICOFINSRec_vCOFINS.Value,*)
    MeuID, QrItsIGraGruX.Value], [
    FatID, FatNum, Empresa, QrItsInItem.Value], True) then
    begin
      IncluiNFeItsN(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsO(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsP(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsQ(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsR(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsS(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsT(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsU(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsV(FatID, FatNum, Empresa, QrItsInItem.Value);
    end;
    //
    QrItsI.Next;
  end;
  Result := True;
end;

{
function TFmNFeLoad_Dir.IncluiNFeItsIDi(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
var
  Controle: Integer;
begin
  QrItsIDi.First;
  while not QrItsIDi.Eof do
  begin
    Controle := DModG.BuscaProximoInteiro('nfeitsidi', 'Controle', '', 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsidi', False, [
    'DI_nDI', 'DI_dDI', 'DI_xLocDesemb',
    'DI_UFDesemb', 'DI_dDesemb', 'DI_cExportador'], [
    'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle'], [
    QrItsIDiDI_nDI.Value, QrItsIDiDI_dDI.Value, QrItsIDiDI_xLocDesemb.Value,
    QrItsIDiDI_UFDesemb.Value, QrItsIDiDI_dDesemb.Value, QrItsIDiDI_cExportador.Value], [
    FatID, FatNum, Empresa, nItem, Controle], True) then
      IncluiNFeItsIDiA(FatID, FatNum, Empresa, nItem, Controle);
    //
    QrItsIDi.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Dir.IncluiNFeItsIDiA(FatID, FatNum, Empresa,
  nItem, Controle: Integer): Boolean;
var
  Conta: Integer;
begin
  QrItsIDiA.First;
  while not QrItsIDiA.Eof do
  begin
    Conta := DModG.BuscaProximoInteiro('nfeitsidia', 'Controle', '', 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsidia', False, [
    'Adi_nAdicao', 'Adi_nSeqAdic', 'Adi_cFabricante',
    'Adi_vDescDI'], [
    'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle', 'Conta'], [
    QrItsIDiAAdi_nAdicao.Value, QrItsIDiAAdi_nSeqAdic.Value, QrItsIDiAAdi_cFabricante.Value,
    QrItsIDiAAdi_vDescDI.Value], [
    FatID, FatNum, Empresa, nItem, Controle, Conta], True) then;
    //
    QrItsIDiA.Next;
  end;
  Result := True;
end;
}

function TFmNFeLoad_Dir.IncluiNFeItsN(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsN.First;
  while not QrItsN.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
    'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
    'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
    'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
    'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
    'ICMS_vICMSST', 'ICMS_CSOSN',
    'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
    'ICMS_vICMSSTRet', 'ICMS_motDesICMS', 'ICMS_pCredSN',
    'ICMS_vCredICMSSN'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsNICMS_Orig.Value, QrItsNICMS_CST.Value, QrItsNICMS_modBC.Value,
    QrItsNICMS_pRedBC.Value, QrItsNICMS_vBC.Value, QrItsNICMS_pICMS.Value,
    QrItsNICMS_vICMS.Value, QrItsNICMS_modBCST.Value, QrItsNICMS_pMVAST.Value,
    QrItsNICMS_pRedBCST.Value, QrItsNICMS_vBCST.Value, QrItsNICMS_pICMSST.Value,
    QrItsNICMS_vICMSST.Value, QrItsNICMS_CSOSN.Value,
    QrItsNICMS_UFST.Value, QrItsNICMS_pBCOp.Value, QrItsNICMS_vBCSTRet.Value,
    QrItsNICMS_vICMSSTRet.Value, QrItsNICMS_motDesICMS.Value, QrItsNICMS_pCredSN.Value,
    QrItsNICMS_vCredICMSSN.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsN.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsO(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsO.First;
  while not QrItsO.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
    'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
    'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',
    'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',
    'IPI_pIPI', 'IPI_vIPI'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsOIPI_clEnq.Value, QrItsOIPI_CNPJProd.Value, QrItsOIPI_cSelo.Value,
    QrItsOIPI_qSelo.Value, QrItsOIPI_cEnq.Value, QrItsOIPI_CST.Value,
    QrItsOIPI_vBC.Value, QrItsOIPI_qUnid.Value, QrItsOIPI_vUnid.Value,
    QrItsOIPI_pIPI.Value, QrItsOIPI_vIPI.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsO.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsP(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsP.First;
  while not QrItsP.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsp', False, [
    'II_vBC', 'II_vDespAdu', 'II_vII',
    'II_vIOF'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsPII_vBC.Value, QrItsPII_vDespAdu.Value, QrItsPII_vII.Value,
    QrItsPII_vIOF.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsP.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsQ(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsQ.First;
  while not QrItsQ.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsq', False, [
    'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
    'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
    'PIS_fatorBC'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsQPIS_CST.Value, QrItsQPIS_vBC.Value, QrItsQPIS_pPIS.Value,
    QrItsQPIS_vPIS.Value, QrItsQPIS_qBCProd.Value, QrItsQPIS_vAliqProd.Value,
    QrItsQPIS_fatorBC.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsQ.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsR(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsR.First;
  while not QrItsR.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsr', False, [
    'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
    'PISST_vAliqProd', 'PISST_vPIS', 'PISST_fatorBCST'
    ], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsRPISST_vBC.Value, QrItsRPISST_pPIS.Value, QrItsRPISST_qBCProd.Value,
    QrItsRPISST_vAliqProd.Value, QrItsRPISST_vPIS.Value, QrItsRPISST_fatorBCST.Value
    ], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsR.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsS(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsS.First;
  while not QrItsS.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItss', False, [
    'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
    'COFINS_qBCProd', 'COFINS_vAliqProd', 'COFINS_vCOFINS',
    'COFINS_fatorBC'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsSCOFINS_CST.Value, QrItsSCOFINS_vBC.Value, QrItsSCOFINS_pCOFINS.Value,
    QrItsSCOFINS_qBCProd.Value, QrItsSCOFINS_vAliqProd.Value, QrItsSCOFINS_vCOFINS.Value,
    QrItsSCOFINS_fatorBC.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsS.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsT(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsT.First;
  while not QrItsT.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItst', False, [
    'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
    'COFINSST_vAliqProd', 'COFINSST_vCOFINS', 'COFINSST_fatorBCST'
    ], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsTCOFINSST_vBC.Value, QrItsTCOFINSST_pCOFINS.Value, QrItsTCOFINSST_qBCProd.Value,
    QrItsTCOFINSST_vAliqProd.Value, QrItsTCOFINSST_vCOFINS.Value, QrItsTCOFINSST_fatorBCST.Value
    ], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsT.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsU(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsU.First;
  while not QrItsU.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsu', False, [
    'ISSQN_vBC', 'ISSQN_vAliq', 'ISSQN_vISSQN',
    'ISSQN_cMunFG', 'ISSQN_cListServ', 'ISSQN_cSitTrib'
    ], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsUISSQN_vBC.Value, QrItsUISSQN_vAliq.Value, QrItsUISSQN_vISSQN.Value,
    QrItsUISSQN_cMunFG.Value, QrItsUISSQN_cListServ.Value, QrItsUISSQN_cSitTrib.Value
    ], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsU.Next;
  end;
end;

function TFmNFeLoad_Dir.IncluiNFeItsV(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := False;
  QrItsV.First;
  while not QrItsV.Eof do
  begin
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsv', False, [
    'InfAdProd'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsVInfAdProd.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsV.Next;
  end;
end;

function TFmNFeLoad_Dir.ObtemXML_De_Arquivo(Arquivo: String): WideString;
var
  mTexto: TStringList;
  XML: PChar;
begin
  if FileExists(Arquivo) then
  begin
    mTexto := TStringList.Create;
    mTexto.Clear;
    mTexto.LoadFromFile(Arquivo);
    XML := PChar(mTexto.Text);
    Result := Copy(XML, 1, Length(XML));
    mTexto.Free;
  end else
  begin
    Result := '';
    Geral.MB_Erro('Arquivo n�o localizado para copiar XML:' + sLineBreak +
    Arquivo);
  end;
end;

procedure TFmNFeLoad_Dir.PageControl1Change(Sender: TObject);
begin
  try
    if PageControl1.ActivePageIndex = 1 then
      AbreCarregados();
  except
    ;
  end;
end;

procedure TFmNFeLoad_Dir.QrErrBAfterOpen(DataSet: TDataSet);
begin
  if QrErrB.RecordCount > 0 then
  begin
    PageControl1.ActivePageIndex := 1;
    PageControl2.ActivePageIndex := 1;
  end;
end;

procedure TFmNFeLoad_Dir.QrErrBAfterScroll(DataSet: TDataSet);
begin
  QrErrI.Close;
  QrErrI.Params[00].AsInteger := QrErrBSeqArq.Value;
  QrErrI.Params[01].AsInteger := QrErrBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrErrI, DModG.MyPID_DB);
  BtEmit.Enabled := QrErrBErrEmit.Value > 0;
  BtDest.Enabled := QrErrBErrDest.Value > 0;
end;

procedure TFmNFeLoad_Dir.QrErrBBeforeClose(DataSet: TDataSet);
begin
  QrErrI.Close;
  BtEmit.Enabled := False;
  BtDest.Enabled := False;
  BtProd.Enabled := False;
end;

procedure TFmNFeLoad_Dir.QrErrIAfterScroll(DataSet: TDataSet);
begin
  BtProd.Enabled := QrErrIGraGruX.Value = 0;
end;

procedure TFmNFeLoad_Dir.QrItsIAfterScroll(DataSet: TDataSet);
begin
  QrItsIDi.Close;
  QrItsIDi.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsIDi.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsIDi.Params[02].AsInteger := QrItsInItem.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsIDi, DModG.MyPID_DB);
  //
  QrItsN.Close;
  QrItsN.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsN.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsN, DModG.MyPID_DB);
  //
  QrItsO.Close;
  QrItsO.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsO.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsO, DModG.MyPID_DB);
  //
  QrItsP.Close;
  QrItsP.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsP.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsP, DModG.MyPID_DB);
  //
  QrItsQ.Close;
  QrItsQ.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsQ.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsQ, DModG.MyPID_DB);
  //
  QrItsR.Close;
  QrItsR.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsR.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsR, DModG.MyPID_DB);
  //
  QrItsS.Close;
  QrItsS.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsS.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsS, DModG.MyPID_DB);
  //
  QrItsT.Close;
  QrItsT.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsT.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsT, DModG.MyPID_DB);
  //
  QrItsU.Close;
  QrItsU.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsU.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsU, DModG.MyPID_DB);
  //
  QrItsV.Close;
  QrItsV.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsV.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsV, DModG.MyPID_DB);
end;

procedure TFmNFeLoad_Dir.QrItsIDiAfterScroll(DataSet: TDataSet);
begin
  QrItsIDiA.Close;
  QrItsIDiA.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsIDiA.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsIDiA.Params[02].AsInteger := QrItsInItem.Value;
  QrItsIDiA.Params[03].AsInteger := QrItsIDiControle.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrItsIDiA, DModG.MyPID_DB);
end;

procedure TFmNFeLoad_Dir.QrNFeX26AfterScroll(DataSet: TDataSet);
begin
  QrNFeX33.Close;
  QrNFeX33.Params[00].AsInteger := QrNFeX26SeqArq.Value;
  QrNFeX33.Params[01].AsInteger := QrNFeX26SeqNFe.Value;
  QrNFeX33.Params[02].AsInteger := QrNFeX26Controle.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrNFeX33, DModG.MyPID_DB);
end;

procedure TFmNFeLoad_Dir.QrNFeX26BeforeClose(DataSet: TDataSet);
begin
  QrNFeX33.Close;
end;

procedure TFmNFeLoad_Dir.SBArquivoClick(Sender: TObject);
var
  IniDir: String;
begin
  IniDir := EdDiretorio.Text;
  if MyObjects.FileOpenDialog(Self, IniDir, '',
  'Selecione o diret�rio', '', [], IniDir) then
  begin
    EdDiretorio.Text := ExtractFileDir(IniDir);
    ImportaNFes();
  end;
end;

procedure TFmNFeLoad_Dir.SbCarregaClick(Sender: TObject);
begin
  ImportaNFes();
end;

procedure TFmNFeLoad_Dir.UpdateInfoErros();
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ SET ');
    DModG.QrUpdPID1.SQL.Add('ErrEmit=-1, ErrDest=-1, ErrTrsp=-1, ErrProd=-1;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_c_ c');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrEmit=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=c.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=c.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND c.CodInfoEmit=0;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_e_ e');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrDest=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=e.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=e.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND e.CodInfoDest=0;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_x_01 x');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrTrsp=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=x.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=x.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND ((x.Transporta_CNPJ<>""');
    DModG.QrUpdPID1.SQL.Add('OR x.Transporta_CPF<>"")');
    DModG.QrUpdPID1.SQL.Add('AND x.CodInfoTrsp=0);');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_i_ i');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrProd=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=i.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=i.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND i.GraGruX=0;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.ExecSQL;
    //
   finally
     Screen.Cursor := MyCursor;
  end;
end;

end.
