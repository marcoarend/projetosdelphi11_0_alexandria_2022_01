unit NFeLoad_B01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_B01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel22: TPanel;
    Label195: TLabel;
    Label196: TLabel;
    Label197: TLabel;
    Label198: TLabel;
    Label199: TLabel;
    Label200: TLabel;
    Label201: TLabel;
    Label202: TLabel;
    Label203: TLabel;
    Label204: TLabel;
    Label205: TLabel;
    Label206: TLabel;
    Label207: TLabel;
    Label208: TLabel;
    Label209: TLabel;
    Label210: TLabel;
    Label193: TLabel;
    Label194: TLabel;
    Edide_natOp: TdmkEdit;
    Edide_indPag: TdmkEdit;
    Edide_mod: TdmkEdit;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Edide_tpNF: TdmkEdit;
    Edide_cMunFG: TdmkEdit;
    Edide_tpImp: TdmkEdit;
    Edide_tpEmis: TdmkEdit;
    Edide_cDV: TdmkEdit;
    Edide_tpAmb: TdmkEdit;
    Edide_finNFe: TdmkEdit;
    Edide_procEmi: TdmkEdit;
    Edide_verProc: TdmkEdit;
    Edide_indPag_Txt: TdmkEdit;
    Edide_tpNF_TXT: TdmkEdit;
    Edide_cMunFG_TXT: TdmkEdit;
    Edide_TpImp_TXT: TdmkEdit;
    Edide_tpEmis_TXT: TdmkEdit;
    Edide_tpAmb_TXT: TdmkEdit;
    Edide_finNFe_TXT: TdmkEdit;
    Edide_procEmi_TXT: TdmkEdit;
    TPide_dEmi: TdmkEditDateTimePicker;
    TPide_dSaiEnt: TdmkEditDateTimePicker;
    Edide_cUF: TdmkEdit;
    Edide_xUF: TdmkEdit;
    Edide_hSaiEnt: TdmkEdit;
    Edide_cNF: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edide_cUFChange(Sender: TObject);
    procedure Edide_indPagChange(Sender: TObject);
    procedure Edide_tpNFChange(Sender: TObject);
    procedure Edide_cMunFGChange(Sender: TObject);
    procedure Edide_tpImpChange(Sender: TObject);
    procedure Edide_tpEmisChange(Sender: TObject);
    procedure Edide_tpAmbChange(Sender: TObject);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure Edide_procEmiChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_B01: TFmNFeLoad_B01;

implementation

uses UnMyObjects, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeLoad_B01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_B01.Edide_cMunFGChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edide_cMunFG.ValueVariant, []) then
    Edide_cMunFG_TXT.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Edide_cMunFG_TXT.Text := '';
end;

procedure TFmNFeLoad_B01.Edide_cUFChange(Sender: TObject);
begin
  Edide_xUF.Text := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Edide_cUF.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_finNFeChange(Sender: TObject);
begin
  Edide_finNFe_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_indPagChange(Sender: TObject);
begin
  Edide_indPag_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_indPag,  Edide_indPag.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_procEmiChange(Sender: TObject);
begin
  Edide_procEmi_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_procEmi, Edide_procEmi.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_tpAmbChange(Sender: TObject);
begin
  Edide_tpAmb_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_tpAmb,   Edide_tpAmb.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_tpEmisChange(Sender: TObject);
begin
  Edide_tpEmis_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_tpEmis,  Edide_tpEmis.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_tpImpChange(Sender: TObject);
begin
  Edide_tpImp_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_tpImp,   Edide_tpImp.ValueVariant);
end;

procedure TFmNFeLoad_B01.Edide_tpNFChange(Sender: TObject);
begin
  Edide_tpNF_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTide_tpNF,    Edide_tpNF.ValueVariant);
end;

procedure TFmNFeLoad_B01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_B01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_B01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
