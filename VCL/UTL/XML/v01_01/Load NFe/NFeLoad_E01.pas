unit NFeLoad_E01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_E01 = class(TForm)
    Panel1: TPanel;
    PnDadosE01: TPanel;
    Label13: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label109: TLabel;
    Label22: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    Label23: TLabel;
    Eddest_CNPJ: TdmkEdit;
    Eddest_xNome: TdmkEdit;
    Eddest_xLgr: TdmkEdit;
    Eddest_nro: TdmkEdit;
    Eddest_xCpl: TdmkEdit;
    Eddest_xBairro: TdmkEdit;
    Eddest_cMun: TdmkEdit;
    Eddest_UF: TdmkEdit;
    Eddest_CEP: TdmkEdit;
    Eddest_cPais: TdmkEdit;
    Eddest_fone: TdmkEdit;
    Eddest_IE: TdmkEdit;
    Eddest_ISUF: TdmkEdit;
    Eddest_xMun: TdmkEdit;
    Eddest_xPais: TdmkEdit;
    Eddest_CPF: TdmkEdit;
    Eddest_email: TdmkEdit;
    PnCodInfoDest: TPanel;
    Label305: TLabel;
    SbEntiDest: TSpeedButton;
    EdCodInfoDest: TdmkEditCB;
    CBCodInfoDest: TdmkDBLookupComboBox;
    LaF5_BuscaDados: TLabel;
    QrEntiDest: TmySQLQuery;
    QrEntiDestCodigo: TIntegerField;
    QrEntiDestNO_ENT: TWideStringField;
    DsEntiDest: TDataSource;
    Panel3: TPanel;
    Label1: TLabel;
    EdSeqArq: TdmkEdit;
    Label2: TLabel;
    EdSeqNFe: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Eddest_cMunChange(Sender: TObject);
    procedure Eddest_UFChange(Sender: TObject);
    procedure Eddest_cPaisChange(Sender: TObject);
    procedure EdCodInfoDestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBCodInfoDestKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbEntiDestClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaDadosDest();
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_E01: TFmNFeLoad_E01;

implementation

uses UnMyObjects, ModuleNFe_0000, UnInternalConsts, MyDBCheck, Entidade2,
UMySQLModule, ModuleGeral, NFeLoad_Arq, Module;

{$R *.DFM}

procedure TFmNFeLoad_E01.BtOKClick(Sender: TObject);
var
  SeqArq, SeqNFe, CodInfoDest: Integer;
  NomeDest: String;
begin
  SeqArq := EdSeqArq.ValueVariant;
  SeqNFe := EdSeqNFe.ValueVariant;
  CodInfoDest := EdCodInfoDest.ValueVariant;
  NomeDest    := CBCodInfoDest.Text;
  //
  if PnCodInfoDest.Enabled then
  begin
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, ImgTipo.SQLType, '_nfe_e_', False, [
    'CodInfoDest', 'NomeDest'], ['SeqArq', 'SeqNFe'], [
    CodInfoDest, NomeDest], [SeqArq, SeqNFe], False) then;
  end;
{
  if PnCodInfoDest.Enabled then
  begin
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, ImgTipo.SQLType, '_nfe_e_', False, [
    'FatID', 'FatNum', 'Empresa',
    'dest_CNPJ', 'dest_CPF', 'dest_xNome',
    'dest_xLgr', 'dest_nro', 'dest_xCpl',
    'dest_xBairro', 'dest_cMun', 'dest_xMun',
    'dest_UF', 'dest_CEP', 'dest_cPais',
    'dest_xPais', 'dest_fone', 'dest_IE',
    'dest_ISUF', 'dest_email', 'CodInfoDest',
    'NomeDest'], [
    'SeqArq', 'SeqNFe'], [
    FatID, FatNum, Empresa,
    dest_CNPJ, dest_CPF, dest_xNome,
    dest_xLgr, dest_nro, dest_xCpl,
    dest_xBairro, dest_cMun, dest_xMun,
    dest_UF, dest_CEP, dest_cPais,
    dest_xPais, dest_fone, dest_IE,
    dest_ISUF, dest_email, CodInfoDest,
    NomeDest], [
    SeqArq, SeqNFe], False) then;
  end;
}
  FmNFeLoad_Arq.QrNFeE.Close;
  UnDmkDAC_PF.AbreQuery(FmNFeLoad_Arq.QrNFeE, Dmod.MyDB);
  Close;
end;

procedure TFmNFeLoad_E01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_E01.BuscaDadosDest;
var
  Lograd_Rua: String;
begin
  if PnDadosE01.Enabled then
  begin
    DmNFe_0000.ReopenDest(EdCodInfoDest.ValueVariant);
    if DmNFe_0000.QrDestNO_LOGRAD.Value <> '' then
      Lograd_Rua := DmNFe_0000.QrDestNO_LOGRAD.Value + ' ';
    Lograd_Rua := Lograd_Rua + DmNFe_0000.QrDestRUA.Value;
    //
    Eddest_IE     .ValueVariant := '';
    Eddest_ISUF   .ValueVariant := '';
    Eddest_CNPJ   .ValueVariant := '';
    Eddest_CPF    .ValueVariant := '';
    Eddest_xNome  .ValueVariant := '';
    Eddest_xLgr   .ValueVariant := '';
    Eddest_nro    .ValueVariant := '';
    Eddest_xCpl   .ValueVariant := '';
    Eddest_xBairro.ValueVariant := '';
    Eddest_cMun   .ValueVariant := 0;
    Eddest_xMun   .ValueVariant := '';
    Eddest_CEP    .ValueVariant := '';
    Eddest_cPais  .ValueVariant := 0;
    Eddest_xPais  .ValueVariant := '';
    Eddest_fone   .ValueVariant := '';
    Eddest_UF     .ValueVariant := '';
    //
    Eddest_CNPJ   .ValueVariant := Geral.FormataCNPJ_TT(DmNFe_0000.QrDestCNPJ.Value);
    Eddest_CPF    .ValueVariant := Geral.FormataCNPJ_TT(DmNFe_0000.QrDestCPF.Value);
    Eddest_xNome  .ValueVariant := DmNFe_0000.QrDestNO_ENT.Value;
    Eddest_xLgr   .ValueVariant := Lograd_Rua;
    Eddest_nro    .ValueVariant := DmNFe_0000.QrDestNumero.Value;
    Eddest_xCpl   .ValueVariant := DmNFe_0000.QrDestCOMPL.Value;
    Eddest_xBairro.ValueVariant := DmNFe_0000.QrDestBAIRRO.Value;
    Eddest_cMun   .ValueVariant := DmNFe_0000.QrDestCodMunici.Value;
    Eddest_xMun   .ValueVariant := DmNFe_0000.QrDestNO_Munici.Value;
    Eddest_UF     .ValueVariant := DmNFe_0000.QrDestNO_UF.Value;
    Eddest_CEP    .ValueVariant := DmNFe_0000.QrDestCEP.Value;
    Eddest_cPais  .ValueVariant := DmNFe_0000.QrDestCodiPais.Value;
    Eddest_xPais  .ValueVariant := DmNFe_0000.QrDestNO_Pais.Value;
    Eddest_fone   .ValueVariant := DmNFe_0000.QrDestTe1.Value;
    Eddest_IE     .ValueVariant := DmNFe_0000.QrDestIE.Value;
    Eddest_ISUF   .ValueVariant := DmNFe_0000.QrDestSUFRAMA.Value;
  end else Geral.MB_Aviso('Os dados n�o podem ser modificados!');
end;

procedure TFmNFeLoad_E01.CBCodInfoDestKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BuscaDadosDest();
end;

procedure TFmNFeLoad_E01.EdCodInfoDestKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BuscaDadosDest();
end;

procedure TFmNFeLoad_E01.Eddest_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Eddest_cMun.ValueVariant, []) then
    Eddest_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Eddest_xMun.Text := '';
end;

procedure TFmNFeLoad_E01.Eddest_cPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', Eddest_cPais.ValueVariant, []) then
    Eddest_xPais.Text := DmNFe_0000.QrBACEN_PaisNome.Value
  else
    Eddest_xPais.Text := '';
end;

procedure TFmNFeLoad_E01.Eddest_UFChange(Sender: TObject);
begin
  Eddest_IE.LinkMsk := Eddest_UF.Text;
end;

procedure TFmNFeLoad_E01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_E01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntiDest, Dmod.MyDB);
end;

procedure TFmNFeLoad_E01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeLoad_E01.SbEntiDestClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdCodInfoDest, CBCodInfoDest, QrEntiDest,
    VAR_CADASTRO, 'Codigo', 'Codigo');
end;

end.
