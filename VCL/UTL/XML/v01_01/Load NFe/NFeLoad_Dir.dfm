object FmNFeLoad_Dir: TFmNFeLoad_Dir
  Left = 339
  Top = 185
  Caption = 'NFe-LOADD-003 :: Importa'#231#227'o de NF-e(s) Emitidas'
  ClientHeight = 853
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 101
    Width = 1008
    Height = 752
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Listar e carregar'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 724
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 998
          Height = 457
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Panel4: TPanel
            Left = 1
            Top = 1
            Width = 996
            Height = 44
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object Label110: TLabel
              Left = 8
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Diret'#243'rio:'
            end
            object SBArquivo: TSpeedButton
              Left = 947
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBArquivoClick
            end
            object SbCarrega: TSpeedButton
              Left = 967
              Top = 20
              Width = 21
              Height = 21
              Caption = '>'
              OnClick = SbCarregaClick
            end
            object EdDiretorio: TEdit
              Left = 8
              Top = 20
              Width = 937
              Height = 21
              TabOrder = 0
              Text = 'C:\_MLArend\Clientes\Curtidora Igap'#243'\NFes\Autorizadas'
            end
          end
          object Panel5: TPanel
            Left = 1
            Top = 45
            Width = 228
            Height = 411
            Align = alLeft
            TabOrder = 1
            object Panel7: TPanel
              Left = 1
              Top = 1
              Width = 226
              Height = 92
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label2: TLabel
                Left = 0
                Top = 0
                Width = 226
                Height = 13
                Align = alTop
                Caption = ' Extens'#245'es:'
                ExplicitWidth = 55
              end
              object MeExtensoes: TMemo
                Left = 0
                Top = 13
                Width = 226
                Height = 48
                Align = alTop
                Lines.Strings = (
                  '*.xml')
                TabOrder = 0
                WantReturns = False
              end
              object CkSub1: TCheckBox
                Left = 4
                Top = 64
                Width = 117
                Height = 17
                Caption = 'Incluir subdiret'#243'rios.'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
            end
            object GroupBox1: TGroupBox
              Left = 1
              Top = 93
              Width = 226
              Height = 104
              Align = alTop
              Caption = ' Tipos a serem importados: '
              TabOrder = 1
              object Ck_nfeProc: TCheckBox
                Left = 8
                Top = 20
                Width = 169
                Height = 17
                Caption = 'NF-e autorizada e protocolada.'
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
              object Ck_nfe: TCheckBox
                Left = 8
                Top = 40
                Width = 169
                Height = 17
                Caption = 'NF-e sem autoriza'#231#227'o.'
                TabOrder = 1
              end
              object Ck_procCancNFe: TCheckBox
                Left = 8
                Top = 60
                Width = 189
                Height = 17
                Caption = 'Protocolo de cancelamento de NF-e.'
                Checked = True
                State = cbChecked
                TabOrder = 2
              end
              object Ck_procInutNFe: TCheckBox
                Left = 8
                Top = 80
                Width = 209
                Height = 17
                Caption = 'Protocolo de inutiliza'#231#227'o de numera'#231#227'o.'
                TabOrder = 3
              end
            end
            object Panel8: TPanel
              Left = 1
              Top = 197
              Width = 226
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 2
              object CkSoAmbiente1: TCheckBox
                Left = 8
                Top = 4
                Width = 217
                Height = 17
                Caption = 'Somente ambiente de produ'#231#227'o.'
                Checked = True
                State = cbChecked
                TabOrder = 0
                OnClick = CkSoAmbiente1Click
              end
            end
            object GroupBox2: TGroupBox
              Left = 1
              Top = 332
              Width = 226
              Height = 78
              Align = alBottom
              Caption = ' Elementos ignorados: '
              TabOrder = 3
              object LBEleIgnor: TListBox
                Left = 2
                Top = 15
                Width = 222
                Height = 61
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ItemHeight = 13
                ParentFont = False
                TabOrder = 0
              end
            end
            object GroupBox3: TGroupBox
              Left = 1
              Top = 221
              Width = 226
              Height = 111
              Align = alClient
              Caption = ' Elementos desconhecidos: '
              TabOrder = 4
              object LBEleDesc: TListBox
                Left = 2
                Top = 15
                Width = 222
                Height = 94
                Align = alClient
                ItemHeight = 13
                TabOrder = 0
              end
            end
          end
          object Panel9: TPanel
            Left = 229
            Top = 45
            Width = 768
            Height = 411
            Align = alClient
            TabOrder = 2
            object Label1: TLabel
              Left = 1
              Top = 1
              Width = 766
              Height = 13
              Align = alTop
              Caption = 'Lista de aquivos XML existentes no diret'#243'rio selecionado:'
              ExplicitWidth = 270
            end
            object LBArqs: TListBox
              Left = 1
              Top = 14
              Width = 766
              Height = 396
              Align = alClient
              ItemHeight = 13
              TabOrder = 0
            end
          end
        end
        object Panel6: TPanel
          Left = 1
          Top = 458
          Width = 998
          Height = 201
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label3: TLabel
            Left = 0
            Top = 0
            Width = 998
            Height = 13
            Align = alTop
            Caption = 'Arquivos com xml desconhecido ou n'#227'o implementado:'
            ExplicitWidth = 259
          end
          object MeUnknowFile: TMemo
            Left = 0
            Top = 13
            Width = 998
            Height = 188
            Align = alClient
            TabOrder = 0
          end
        end
        object GroupBox4: TGroupBox
          Left = 1
          Top = 659
          Width = 998
          Height = 64
          Align = alBottom
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 2
          object Panel10: TPanel
            Left = 2
            Top = 15
            Width = 994
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel11: TPanel
              Left = 850
              Top = 0
              Width = 144
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object BtSaida: TBitBtn
                Tag = 13
                Left = 14
                Top = 3
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Sair'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
            object BtCarregar: TBitBtn
              Left = 8
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Carregar'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtCarregarClick
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Inserir no Banco de Dados'
      ImageIndex = 1
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 1000
        Height = 724
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = 'Registros aptos para iclus'#227'o no banco de dados'
          object Panel16: TPanel
            Left = 0
            Top = 600
            Width = 992
            Height = 32
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label4: TLabel
              Left = 8
              Top = 8
              Width = 29
              Height = 13
              Caption = 'FatID:'
            end
            object Label5: TLabel
              Left = 88
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object EdFatID: TdmkEdit
              Left = 40
              Top = 4
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object EdEmpresa: TdmkEdit
              Left = 136
              Top = 4
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '-11'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = -11
              ValWarn = False
            end
          end
          object PageControl3: TPageControl
            Left = 0
            Top = 0
            Width = 992
            Height = 600
            ActivePage = TabSheet8
            Align = alClient
            TabOrder = 1
            object TabSheet8: TTabSheet
              Caption = '  NF-es autorizadas'
              object DBGrid1: TDBGrid
                Left = 0
                Top = 0
                Width = 984
                Height = 572
                Align = alClient
                DataSource = DsNFeB
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'ide_mod'
                    Title.Caption = 'mod'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_serie'
                    Title.Caption = 'serie'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_nNF'
                    Title.Caption = 'N'#186' NF'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_dEmi'
                    Title.Caption = 'Emiss'#227'o'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Id'
                    Visible = True
                  end>
              end
            end
            object TabSheet9: TTabSheet
              Caption = 'Cancelamentos'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid5: TDBGrid
                Left = 0
                Top = 0
                Width = 984
                Height = 572
                Align = alClient
                DataSource = DsCR01
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'infCanc_chNFe'
                    Title.Caption = 'Chave da NF-e'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'infCanc_cStat'
                    Title.Caption = 'Status'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'infCanc_dhRecbto'
                    Title.Caption = 'Data / hora recibo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'infCanc_nProt'
                    Title.Caption = 'Protocolo'
                    Visible = True
                  end>
              end
            end
            object TabSheet10: TTabSheet
              Caption = 'Inutiliza'#231#245'es'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid6: TDBGrid
                Left = 0
                Top = 0
                Width = 984
                Height = 572
                Align = alClient
                DataSource = DsDR01
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_Id'
                    Title.Caption = 'Id'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_cStat'
                    Title.Caption = 'Status'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_serie'
                    Title.Caption = 'S'#233'rie'
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_nNFIni'
                    Title.Caption = 'N'#186' Inicial NFe'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_nNFFin'
                    Title.Caption = 'N'#186' Final NFe'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_dhRecbto'
                    Title.Caption = 'Data / hora recibo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_nProt'
                    Title.Caption = 'Protocolo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'retInutNFe_xMotivo'
                    Title.Caption = 'Descri'#231#227'o Status'
                    Width = 265
                    Visible = True
                  end>
              end
            end
          end
          object GBRodaPe: TGroupBox
            Left = 0
            Top = 632
            Width = 992
            Height = 64
            Align = alBottom
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 2
            object Panel18: TPanel
              Left = 2
              Top = 15
              Width = 988
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object PnSaiDesis: TPanel
                Left = 844
                Top = 0
                Width = 144
                Height = 47
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 0
                object BitBtn2: TBitBtn
                  Tag = 13
                  Left = 14
                  Top = 3
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sair'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
              object BtInclui: TBitBtn
                Left = 8
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Incluir'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtIncluiClick
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Registros com pend'#234'ncias'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 385
            Height = 632
            Align = alLeft
            DataSource = DsErrB
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ide_mod'
                Title.Caption = 'mod'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_serie'
                Title.Caption = 'serie'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_nNF'
                Title.Caption = 'N'#186' NF'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ide_dEmi'
                Title.Caption = 'Emiss'#227'o'
                Width = 56
                Visible = True
              end>
          end
          object DBGrid4: TDBGrid
            Left = 385
            Top = 0
            Width = 607
            Height = 632
            Align = alClient
            DataSource = DsErrI
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'nItem'
                Title.Caption = 'Item'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_xProd'
                Title.Caption = 'Nome produto'
                Width = 250
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_uCom'
                Title.Caption = 'Unidade'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_qCom'
                Title.Caption = 'Qtde'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vUnCom'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'prod_vProd'
                Title.Caption = 'Total'
                Visible = True
              end>
          end
          object GroupBox6: TGroupBox
            Left = 0
            Top = 632
            Width = 992
            Height = 64
            Align = alBottom
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 2
            ExplicitTop = 236
            ExplicitWidth = 635
            object Panel14: TPanel
              Left = 2
              Top = 15
              Width = 988
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel15: TPanel
                Left = 844
                Top = 0
                Width = 144
                Height = 47
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 0
                object BitBtn4: TBitBtn
                  Tag = 13
                  Left = 14
                  Top = 3
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sair'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
              object BtEmit: TBitBtn
                Left = 8
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Emitente'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 1
              end
              object BtDest: TBitBtn
                Left = 132
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Destinat'#225'rio'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtDestClick
              end
              object BtProd: TBitBtn
                Left = 256
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Produto'
                Enabled = False
                NumGlyphs = 2
                TabOrder = 3
              end
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Registros ignorados'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl4: TPageControl
            Left = 0
            Top = 0
            Width = 992
            Height = 632
            ActivePage = TabSheet6
            Align = alClient
            TabOrder = 0
            object TabSheet6: TTabSheet
              Caption = 'NF-es'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid2: TDBGrid
                Left = 0
                Top = 0
                Width = 984
                Height = 604
                Align = alClient
                DataSource = DsNotB
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'ide_mod'
                    Title.Caption = 'mod'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_serie'
                    Title.Caption = 'serie'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_nNF'
                    Title.Caption = 'N'#186' NF'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_dEmi'
                    Title.Caption = 'Emiss'#227'o'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Id'
                    Visible = True
                  end>
              end
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 632
            Width = 992
            Height = 64
            Align = alBottom
            Color = clBtnFace
            ParentBackground = False
            ParentColor = False
            TabOrder = 1
            object Panel2: TPanel
              Left = 2
              Top = 15
              Width = 988
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel19: TPanel
                Left = 844
                Top = 0
                Width = 144
                Height = 47
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 0
                object BitBtn6: TBitBtn
                  Tag = 13
                  Left = 14
                  Top = 3
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Sair'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 386
        Height = 32
        Caption = 'Importa'#231#227'o de NF-e(s) Emitidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 386
        Height = 32
        Caption = 'Importa'#231#227'o de NF-e(s) Emitidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 386
        Height = 32
        Caption = 'Importa'#231#227'o de NF-e(s) Emitidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel17: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 19
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PBx: TProgressBar
      Left = 2
      Top = 34
      Width = 1004
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 4
    Top = 4
    DOMVendorDesc = 'MSXML'
  end
  object QrNFeB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT a.SeqArq, a.SeqNFe, a.versao, a.Id,'
      'b.ide_cUF,b.ide_cNF, b.ide_natOp, b.ide_indPag, b.ide_mod, '
      'b.ide_serie, b.ide_nNF, b.ide_dEmi, b.ide_dSaiEnt, '
      'b.ide_hSaiEnt, b.ide_tpNF, b.ide_cMunFG, b.ide_tpImp,'
      'b.ide_tpEmis, b.ide_cDV, b.ide_tpAmb, b.ide_finNFe,'
      'b.ide_procEmi, b.ide_verProc, b.ide_dhCont, b.ide_xJust'
      'FROM _nfe_b_ b'
      'LEFT JOIN _nfe_a_ a ON a.SeqArq=b.SeqArq AND a.SeqNFe=b.SeqNFe'
      'WHERE NOT (b.ide_tpAmb IN (1))'
      'ORDER BY b.ide_mod, b.ide_serie, b.ide_nNF')
    Left = 32
    Top = 4
    object QrNFeBSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Required = True
    end
    object QrNFeBSeqNFe: TIntegerField
      FieldName = 'SeqNFe'
      Required = True
    end
    object QrNFeBversao: TFloatField
      FieldName = 'versao'
    end
    object QrNFeBId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeBide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrNFeBide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeBide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNFeBide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrNFeBide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrNFeBide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFeBide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeBide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeBide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrNFeBide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNFeBide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNFeBide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrNFeBide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrNFeBide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrNFeBide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrNFeBide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrNFeBide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrNFeBide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrNFeBide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrNFeBide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNFeBide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
  end
  object DsNFeB: TDataSource
    DataSet = QrNFeB
    Left = 60
    Top = 4
  end
  object DsNotB: TDataSource
    DataSet = QrNotB
    Left = 228
    Top = 4
  end
  object QrNotB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT a.SeqArq, a.SeqNFe, a.versao, a.Id,'
      'b.ide_cUF, b.ide_natOp, b.ide_indPag, b.ide_mod, '
      'b.ide_serie, b.ide_nNF, b.ide_dEmi, b.ide_dSaiEnt, '
      'b.ide_hSaiEnt, b.ide_tpNF, b.ide_cMunFG, b.ide_tpImp,'
      'b.ide_tpEmis, b.ide_cDV, b.ide_tpAmb, b.ide_finNFe,'
      'b.ide_procEmi, b.ide_verProc, b.ide_dhCont, b.ide_xJust'
      'FROM _nfe_b_ b'
      'LEFT JOIN _nfe_a_ a ON a.SeqArq=b.SeqArq AND a.SeqNFe=b.SeqNFe')
    Left = 200
    Top = 4
    object QrNotBSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Required = True
    end
    object QrNotBSeqNFe: TIntegerField
      FieldName = 'SeqNFe'
      Required = True
    end
    object QrNotBversao: TFloatField
      FieldName = 'versao'
    end
    object QrNotBId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNotBide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrNotBide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNotBide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrNotBide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrNotBide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNotBide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNotBide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNotBide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrNotBide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNotBide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNotBide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrNotBide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrNotBide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrNotBide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrNotBide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrNotBide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrNotBide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrNotBide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrNotBide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNotBide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
  end
  object QrErrB: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErrBAfterOpen
    BeforeClose = QrErrBBeforeClose
    AfterScroll = QrErrBAfterScroll
    SQL.Strings = (
      'SELECT a.SeqArq, a.SeqNFe, a.versao, a.Id,'
      'b.ide_cUF, b.ide_natOp, b.ide_indPag, b.ide_mod, '
      'b.ide_serie, b.ide_nNF, b.ide_dEmi, b.ide_dSaiEnt, '
      'b.ide_hSaiEnt, b.ide_tpNF, b.ide_cMunFG, b.ide_tpImp,'
      'b.ide_tpEmis, b.ide_cDV, b.ide_tpAmb, b.ide_finNFe,'
      'b.ide_procEmi, b.ide_verProc, b.ide_dhCont, b.ide_xJust,'
      'a.ErrEmit, a.ErrDest, a.ErrProd'
      'FROM _nfe_b_ b'
      'LEFT JOIN _nfe_a_ a ON a.SeqArq=b.SeqArq AND a.SeqNFe=b.SeqNFe'
      'WHERE NOT (b.ide_tpAmb IN (1))'
      'ORDER BY b.ide_mod, b.ide_serie, b.ide_nNF')
    Left = 88
    Top = 4
    object QrErrBSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Required = True
    end
    object QrErrBSeqNFe: TIntegerField
      FieldName = 'SeqNFe'
      Required = True
    end
    object QrErrBversao: TFloatField
      FieldName = 'versao'
    end
    object QrErrBId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrErrBide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrErrBide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrErrBide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrErrBide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrErrBide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrErrBide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrErrBide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrBide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrErrBide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrErrBide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrErrBide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrErrBide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrErrBide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrErrBide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrErrBide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrErrBide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrErrBide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrErrBide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrErrBide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrErrBide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrErrBErrEmit: TSmallintField
      FieldName = 'ErrEmit'
    end
    object QrErrBErrDest: TSmallintField
      FieldName = 'ErrDest'
    end
    object QrErrBErrProd: TSmallintField
      FieldName = 'ErrProd'
    end
  end
  object DsErrB: TDataSource
    DataSet = QrErrB
    Left = 116
    Top = 4
  end
  object QrErrI: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrErrIAfterScroll
    SQL.Strings = (
      
        'SELECT i.nItem, i.GraGruX, i.prod_xProd, i.prod_uCom, i.prod_qCo' +
        'm,'
      'i.prod_vUnCom, i.prod_vProd'
      'FROM _nfe_i_ i'
      'WHERE i.SeqArq=:P0'
      'AND i.SeqNFe=:P1')
    Left = 144
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrErrInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrErrIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrErrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrErrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrErrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrErrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrErrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsErrI: TDataSource
    DataSet = QrErrI
    Left = 172
    Top = 4
  end
  object QrLocAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa'
      'FROM NFeCabA'
      'WHERE emit_CNPJ=:P0'
      'AND ide_mod=:P1'
      'AND ide_serie=:P2'
      'AND ide_nNF=:P3'
      'AND ide_dEmi=:P4')
    Left = 972
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrLocAntFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocAntFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrLocAntEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrNFeC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'emit_CNPJ, emit_CPF, emit_xNome, '
      'emit_xFant, emit_xLgr, emit_nro, '
      'emit_xCpl, emit_xBairro, emit_cMun, '
      'emit_xMun, emit_UF, emit_CEP, '
      'emit_cPais, emit_xPais, emit_fone, '
      'emit_IE, emit_IEST, emit_IM, '
      'emit_CNAE, emit_CRT, emit_xCNAE, '
      'emit_xCRT, CodInfoEmit'
      'FROM _nfe_c_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 436
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNFeCemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrNFeCemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrNFeCemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrNFeCemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrNFeCemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrNFeCemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrNFeCemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrNFeCemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrNFeCemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrNFeCemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrNFeCemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrNFeCemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrNFeCemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrNFeCemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrNFeCemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrNFeCemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrNFeCemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrNFeCemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrNFeCemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrNFeCemit_xCNAE: TWideStringField
      FieldName = 'emit_xCNAE'
      Size = 100
    end
    object QrNFeCemit_xCRT: TWideStringField
      FieldName = 'emit_xCRT'
      Size = 100
    end
    object QrNFeCCodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
  end
  object DsNFeC: TDataSource
    DataSet = QrNFeC
    Left = 464
    Top = 136
  end
  object QrNFeE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'dest_CNPJ, dest_CPF, dest_xNome, '
      'dest_xLgr, dest_nro, dest_xCpl, '
      'dest_xBairro, dest_cMun, dest_xMun, '
      'dest_UF, dest_CEP, dest_cPais, '
      'dest_xPais, dest_fone, dest_IE, '
      'dest_ISUF, dest_email, CodInfoDest'
      'FROM _nfe_e_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 492
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeEdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrNFeEdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrNFeEdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrNFeEdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrNFeEdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrNFeEdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrNFeEdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrNFeEdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrNFeEdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrNFeEdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrNFeEdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrNFeEdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrNFeEdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrNFeEdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrNFeEdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrNFeEdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrNFeEdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrNFeECodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
  end
  object DsNFeE: TDataSource
    DataSet = QrNFeE
    Left = 520
    Top = 136
  end
  object QrNFeW02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, '
      'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete, '
      'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, '
      'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, '
      'ICMSTot_vOutro, ICMSTot_vNF'
      'FROM _nfe_w_02'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 548
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeW02ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNFeW02ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFeW02ICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrNFeW02ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNFeW02ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrNFeW02ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNFeW02ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNFeW02ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNFeW02ICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrNFeW02ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNFeW02ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNFeW02ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNFeW02ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNFeW02ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
  end
  object QrNFeW17: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'ISSQNtot_vServ, ISSQNtot_vBC, ISSQNtot_vISS, '
      'ISSQNtot_vPIS, ISSQNtot_vCOFINS'
      'FROM _nfe_w_17'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      '')
    Left = 604
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeW17ISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrNFeW17ISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrNFeW17ISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrNFeW17ISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrNFeW17ISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
  end
  object DsNFeW02: TDataSource
    DataSet = QrNFeW02
    Left = 576
    Top = 136
  end
  object DsNFeW17: TDataSource
    DataSet = QrNFeW17
    Left = 632
    Top = 136
  end
  object QrNFeW23: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL, '
      'RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev, '
      'RetTrib_vRetPrev'
      'FROM _nfe_w_23'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 660
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeW23RetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrNFeW23RetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrNFeW23RetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrNFeW23RetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrNFeW23RetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrNFeW23RetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrNFeW23RetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
  end
  object DsNFeW23: TDataSource
    DataSet = QrNFeW23
    Left = 688
    Top = 136
  end
  object QrNFeX01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'ModFrete, xModFrete, Transporta_CNPJ, '
      'Transporta_CPF, Transporta_XNome, Transporta_IE, '
      'Transporta_XEnder, Transporta_XMun, Transporta_UF, '
      'RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, '
      'RetTransp_vICMSRet, RetTransp_CFOP, RetTransp_CMunFG, '
      'RetTransp_xMunFG, VeicTransp_Placa, VeicTransp_UF, '
      'VeicTransp_RNTC, Vagao, Balsa'
      'FROM _nfe_x_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 716
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeX01ModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrNFeX01xModFrete: TWideStringField
      FieldName = 'xModFrete'
      Size = 50
    end
    object QrNFeX01Transporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrNFeX01Transporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrNFeX01Transporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrNFeX01Transporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
    end
    object QrNFeX01Transporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrNFeX01Transporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrNFeX01Transporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrNFeX01RetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrNFeX01RetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrNFeX01RetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrNFeX01RetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrNFeX01RetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrNFeX01RetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrNFeX01RetTransp_xMunFG: TWideStringField
      FieldName = 'RetTransp_xMunFG'
      Size = 60
    end
    object QrNFeX01VeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrNFeX01VeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrNFeX01VeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrNFeX01Vagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrNFeX01Balsa: TWideStringField
      FieldName = 'Balsa'
    end
  end
  object DsNFeX01: TDataSource
    DataSet = QrNFeX01
    Left = 744
    Top = 136
  end
  object DsNFeY01: TDataSource
    DataSet = QrNFeY01
    Left = 800
    Top = 136
  end
  object QrNFeY01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'Cobr_Fat_nFat, Cobr_Fat_vOrig, Cobr_Fat_vDesc, '
      'Cobr_Fat_vLiq'
      'FROM _nfe_y_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 772
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeY01Cobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrNFeY01Cobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrNFeY01Cobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrNFeY01Cobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
  end
  object DsNFeZ01: TDataSource
    DataSet = QrNFeZ01
    Left = 856
    Top = 136
  end
  object QrNFeZ01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'InfAdic_InfAdFisco, InfAdic_InfCpl'
      'FROM _nfe_z_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 828
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ01InfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeZ01InfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrNFeZA01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'Exporta_UFEmbarq, Exporta_XLocEmbarq'
      'FROM _nfe_za_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 884
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZA01Exporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrNFeZA01Exporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
  end
  object DsNFeZA01: TDataSource
    DataSet = QrNFeZA01
    Left = 912
    Top = 136
  end
  object DsNFeZB01: TDataSource
    DataSet = QrNFeZB01
    Left = 968
    Top = 128
  end
  object QrNFeZB01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'Compra_XNEmp, Compra_XPed, Compra_XCont'
      'FROM _nfe_zb_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 940
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZB01Compra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrNFeZB01Compra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrNFeZB01Compra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
  end
  object DsPR01: TDataSource
    DataSet = QrPR01
    Left = 464
    Top = 164
  end
  object QrPR01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'protNFe_versao, infProt_Id, infProt_tpAmb, '
      'infProt_xAmb, infProt_verAplic, infProt_chNFe, '
      'infProt_dhRecbto, infProt_nProt, infProt_digVal, '
      'infProt_cStat, infProt_xMotivo'
      'FROM _nfe_pr01_'
      'WHERE SeqArq=:P0')
    Left = 436
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPR01protNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
    end
    object QrPR01infProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 46
    end
    object QrPR01infProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrPR01infProt_xAmb: TWideStringField
      FieldName = 'infProt_xAmb'
      Size = 11
    end
    object QrPR01infProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
    end
    object QrPR01infProt_chNFe: TWideStringField
      FieldName = 'infProt_chNFe'
      Size = 44
    end
    object QrPR01infProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrPR01infProt_nProt: TLargeintField
      FieldName = 'infProt_nProt'
    end
    object QrPR01infProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrPR01infProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrPR01infProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
  end
  object QrCR01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'retCanc_versao, infCanc_Id, infCanc_tpAmb, '
      'infCanc_xAmb, infCanc_verAplic, infCanc_cStat, '
      'infCanc_xMotivo, infCanc_cUF, infCanc_xUF, '
      'infCanc_chNFe, infCanc_dhRecbto, infCanc_nProt'
      'FROM _nfe_cr01_'
      '')
    Left = 492
    Top = 164
    object QrCR01retCanc_versao: TFloatField
      FieldName = 'retCanc_versao'
      Origin = '_nfe_cr01_.retCanc_versao'
    end
    object QrCR01infCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Origin = '_nfe_cr01_.infCanc_Id'
      Size = 60
    end
    object QrCR01infCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
      Origin = '_nfe_cr01_.infCanc_tpAmb'
    end
    object QrCR01infCanc_xAmb: TWideStringField
      FieldName = 'infCanc_xAmb'
      Origin = '_nfe_cr01_.infCanc_xAmb'
      Size = 11
    end
    object QrCR01infCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Origin = '_nfe_cr01_.infCanc_verAplic'
    end
    object QrCR01infCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = '_nfe_cr01_.infCanc_cStat'
    end
    object QrCR01infCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Origin = '_nfe_cr01_.infCanc_xMotivo'
      Size = 255
    end
    object QrCR01infCanc_cUF: TSmallintField
      FieldName = 'infCanc_cUF'
      Origin = '_nfe_cr01_.infCanc_cUF'
    end
    object QrCR01infCanc_xUF: TWideStringField
      FieldName = 'infCanc_xUF'
      Origin = '_nfe_cr01_.infCanc_xUF'
      Size = 2
    end
    object QrCR01infCanc_chNFe: TWideStringField
      FieldName = 'infCanc_chNFe'
      Origin = '_nfe_cr01_.infCanc_chNFe'
      Size = 44
    end
    object QrCR01infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
      Origin = '_nfe_cr01_.infCanc_dhRecbto'
    end
    object QrCR01infCanc_nProt: TLargeintField
      FieldName = 'infCanc_nProt'
      Origin = '_nfe_cr01_.infCanc_nProt'
    end
  end
  object DsCR01: TDataSource
    DataSet = QrCR01
    Left = 520
    Top = 164
  end
  object QrDR01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  SeqArq,'
      ''
      'retInutNFe_versao, retInutNFe_Id, retInutNFe_tpAmb, '
      'retInutNFe_xAmb, retInutNFe_verAplic, retInutNFe_cStat, '
      'retInutNFe_xMotivo, retInutNFe_cUF, retInutNFe_xUF, '
      'retInutNFe_ano, retInutNFe_CNPJ, retInutNFe_mod, '
      'retInutNFe_serie, retInutNFe_nNFIni, retInutNFe_nNFFin, '
      'retInutNFe_dhRecbto, retInutNFe_nProt'
      ''
      'FROM _nfe_dr01_'
      '')
    Left = 548
    Top = 164
    object QrDR01retInutNFe_versao: TFloatField
      FieldName = 'retInutNFe_versao'
    end
    object QrDR01retInutNFe_Id: TWideStringField
      FieldName = 'retInutNFe_Id'
      Size = 17
    end
    object QrDR01retInutNFe_tpAmb: TSmallintField
      FieldName = 'retInutNFe_tpAmb'
    end
    object QrDR01retInutNFe_xAmb: TWideStringField
      FieldName = 'retInutNFe_xAmb'
      Size = 11
    end
    object QrDR01retInutNFe_verAplic: TWideStringField
      FieldName = 'retInutNFe_verAplic'
    end
    object QrDR01retInutNFe_cStat: TIntegerField
      FieldName = 'retInutNFe_cStat'
    end
    object QrDR01retInutNFe_xMotivo: TWideStringField
      FieldName = 'retInutNFe_xMotivo'
      Size = 255
    end
    object QrDR01retInutNFe_cUF: TSmallintField
      FieldName = 'retInutNFe_cUF'
    end
    object QrDR01retInutNFe_xUF: TWideStringField
      FieldName = 'retInutNFe_xUF'
      Size = 2
    end
    object QrDR01retInutNFe_ano: TSmallintField
      FieldName = 'retInutNFe_ano'
    end
    object QrDR01retInutNFe_CNPJ: TWideStringField
      FieldName = 'retInutNFe_CNPJ'
      Size = 14
    end
    object QrDR01retInutNFe_mod: TSmallintField
      FieldName = 'retInutNFe_mod'
    end
    object QrDR01retInutNFe_serie: TIntegerField
      FieldName = 'retInutNFe_serie'
    end
    object QrDR01retInutNFe_nNFIni: TIntegerField
      FieldName = 'retInutNFe_nNFIni'
    end
    object QrDR01retInutNFe_nNFFin: TIntegerField
      FieldName = 'retInutNFe_nNFFin'
    end
    object QrDR01retInutNFe_dhRecbto: TDateTimeField
      FieldName = 'retInutNFe_dhRecbto'
    end
    object QrDR01retInutNFe_nProt: TLargeintField
      FieldName = 'retInutNFe_nProt'
    end
    object QrDR01SeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
  end
  object DsDR01: TDataSource
    DataSet = QrDR01
    Left = 576
    Top = 164
  end
  object QrNFeB12a: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'refNFe, refNF_cUF, refNF_xUF, '
      'refNF_AAMM, refNF_CNPJ, refNF_mod, '
      'refNF_serie, refNF_nNF, refNFP_cUF, '
      'refNFP_xUF, refNFP_AAMM, refNFP_CNPJ, '
      'refNFP_CPF, refNFP_IE, refNFP_mod, '
      'refNFP_serie, refNFP_nNF, refCTe, '
      'refECF_mod, refECF_nECF, refECF_nCOO'
      'FROM _nfe_b_12a'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 436
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeB12arefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrNFeB12arefNF_cUF: TSmallintField
      FieldName = 'refNF_cUF'
    end
    object QrNFeB12arefNF_xUF: TWideStringField
      FieldName = 'refNF_xUF'
      Size = 2
    end
    object QrNFeB12arefNF_AAMM: TIntegerField
      FieldName = 'refNF_AAMM'
    end
    object QrNFeB12arefNF_CNPJ: TWideStringField
      FieldName = 'refNF_CNPJ'
      Size = 14
    end
    object QrNFeB12arefNF_mod: TSmallintField
      FieldName = 'refNF_mod'
    end
    object QrNFeB12arefNF_serie: TIntegerField
      FieldName = 'refNF_serie'
    end
    object QrNFeB12arefNF_nNF: TIntegerField
      FieldName = 'refNF_nNF'
    end
    object QrNFeB12arefNFP_cUF: TSmallintField
      FieldName = 'refNFP_cUF'
    end
    object QrNFeB12arefNFP_xUF: TWideStringField
      FieldName = 'refNFP_xUF'
      Size = 2
    end
    object QrNFeB12arefNFP_AAMM: TSmallintField
      FieldName = 'refNFP_AAMM'
    end
    object QrNFeB12arefNFP_CNPJ: TWideStringField
      FieldName = 'refNFP_CNPJ'
      Size = 14
    end
    object QrNFeB12arefNFP_CPF: TWideStringField
      FieldName = 'refNFP_CPF'
      Size = 11
    end
    object QrNFeB12arefNFP_IE: TWideStringField
      FieldName = 'refNFP_IE'
      Size = 14
    end
    object QrNFeB12arefNFP_mod: TSmallintField
      FieldName = 'refNFP_mod'
    end
    object QrNFeB12arefNFP_serie: TSmallintField
      FieldName = 'refNFP_serie'
    end
    object QrNFeB12arefNFP_nNF: TIntegerField
      FieldName = 'refNFP_nNF'
    end
    object QrNFeB12arefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrNFeB12arefECF_mod: TWideStringField
      FieldName = 'refECF_mod'
      Size = 2
    end
    object QrNFeB12arefECF_nECF: TSmallintField
      FieldName = 'refECF_nECF'
    end
    object QrNFeB12arefECF_nCOO: TIntegerField
      FieldName = 'refECF_nCOO'
    end
  end
  object DsNFeB12a: TDataSource
    DataSet = QrNFeB12a
    Left = 464
    Top = 192
  end
  object QrNFeF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'retirada_CNPJ, retirada_CPF, retirada_xLgr, '
      'retirada_nro, retirada_xCpl, retirada_xBairro, '
      'retirada_cMun, retirada_xMun, retirada_UF'
      ''
      'FROM _nfe_f_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 492
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeFretirada_CNPJ: TWideStringField
      FieldName = 'retirada_CNPJ'
      Size = 14
    end
    object QrNFeFretirada_CPF: TWideStringField
      FieldName = 'retirada_CPF'
      Size = 11
    end
    object QrNFeFretirada_xLgr: TWideStringField
      FieldName = 'retirada_xLgr'
      Size = 60
    end
    object QrNFeFretirada_nro: TWideStringField
      FieldName = 'retirada_nro'
      Size = 60
    end
    object QrNFeFretirada_xCpl: TWideStringField
      FieldName = 'retirada_xCpl'
      Size = 60
    end
    object QrNFeFretirada_xBairro: TWideStringField
      FieldName = 'retirada_xBairro'
      Size = 60
    end
    object QrNFeFretirada_cMun: TIntegerField
      FieldName = 'retirada_cMun'
    end
    object QrNFeFretirada_xMun: TWideStringField
      FieldName = 'retirada_xMun'
      Size = 60
    end
    object QrNFeFretirada_UF: TWideStringField
      FieldName = 'retirada_UF'
      Size = 2
    end
  end
  object DsNFeF: TDataSource
    DataSet = QrNFeF
    Left = 520
    Top = 192
  end
  object DsNFeG: TDataSource
    DataSet = QrNFeG
    Left = 576
    Top = 192
  end
  object QrNFeG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'entrega_CNPJ, entrega_CPF, entrega_xLgr, '
      'entrega_nro, entrega_xCpl, entrega_xBairro, '
      'entrega_cMun, entrega_xMun, entrega_UF'
      ''
      'FROM _nfe_g_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 548
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeGentrega_CNPJ: TWideStringField
      FieldName = 'entrega_CNPJ'
      Size = 14
    end
    object QrNFeGentrega_CPF: TWideStringField
      FieldName = 'entrega_CPF'
      Size = 11
    end
    object QrNFeGentrega_xLgr: TWideStringField
      FieldName = 'entrega_xLgr'
      Size = 60
    end
    object QrNFeGentrega_nro: TWideStringField
      FieldName = 'entrega_nro'
      Size = 60
    end
    object QrNFeGentrega_xCpl: TWideStringField
      FieldName = 'entrega_xCpl'
      Size = 60
    end
    object QrNFeGentrega_xBairro: TWideStringField
      FieldName = 'entrega_xBairro'
      Size = 60
    end
    object QrNFeGentrega_cMun: TIntegerField
      FieldName = 'entrega_cMun'
    end
    object QrNFeGentrega_xMun: TWideStringField
      FieldName = 'entrega_xMun'
      Size = 60
    end
    object QrNFeGentrega_UF: TWideStringField
      FieldName = 'entrega_UF'
      Size = 2
    end
  end
  object QrNFeX22: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'placa, UF, RNTC'
      'FROM _nfe_x_22'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 604
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeX22placa: TWideStringField
      FieldName = 'placa'
      Size = 8
    end
    object QrNFeX22UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrNFeX22RNTC: TWideStringField
      FieldName = 'RNTC'
    end
  end
  object DsNFeX22: TDataSource
    DataSet = QrNFeX22
    Left = 632
    Top = 192
  end
  object QrNFeX26: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeX26BeforeClose
    AfterScroll = QrNFeX26AfterScroll
    SQL.Strings = (
      'SELECT  '
      ''
      'SeqArq, SeqNFe, Controle,'
      'qVol, esp, marca, '
      'nVol, pesoL, pesoB'
      ''
      'FROM _nfe_x_26'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 660
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeX26qVol: TFloatField
      FieldName = 'qVol'
    end
    object QrNFeX26esp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrNFeX26marca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrNFeX26nVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrNFeX26pesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrNFeX26pesoB: TFloatField
      FieldName = 'pesoB'
    end
    object QrNFeX26SeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrNFeX26SeqNFe: TIntegerField
      FieldName = 'SeqNFe'
    end
    object QrNFeX26Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsNFeX26: TDataSource
    DataSet = QrNFeX26
    Left = 688
    Top = 192
  end
  object QrNFeX33: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'nLacre'
      ''
      'FROM _nfe_x_33'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND Controle=:P2')
    Left = 716
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeX33nLacre: TWideStringField
      FieldName = 'nLacre'
      Size = 60
    end
  end
  object DsNFeX33: TDataSource
    DataSet = QrNFeX33
    Left = 744
    Top = 192
  end
  object QrNFeY07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'nDup, dVenc, vDup'
      ''
      'FROM _nfe_y_07'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      '')
    Left = 772
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeY07nDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrNFeY07dVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrNFeY07vDup: TFloatField
      FieldName = 'vDup'
    end
  end
  object DsNFeY07: TDataSource
    DataSet = QrNFeY07
    Left = 800
    Top = 192
  end
  object QrNFeZ04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'xCampo, xTexto'
      ''
      'FROM _nfe_z_04'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      '')
    Left = 828
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ04xCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeZ04xTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object DsNFeZ04: TDataSource
    DataSet = QrNFeZ04
    Left = 856
    Top = 192
  end
  object DsNFeZ07: TDataSource
    DataSet = QrNFeZ07
    Left = 912
    Top = 192
  end
  object QrNFeZ07: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'xCampo, xTexto'
      ''
      'FROM _nfe_z_07'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      '')
    Left = 884
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ07xCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeZ07xTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object QrNFeZ10: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      ''
      'nProc, indProc'
      ''
      'FROM _nfe_z_10'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      '')
    Left = 940
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ10nProc: TWideStringField
      FieldName = 'nProc'
      Size = 60
    end
    object QrNFeZ10indProc: TSmallintField
      FieldName = 'indProc'
    end
  end
  object DsNFeZ10: TDataSource
    DataSet = QrNFeZ10
    Left = 968
    Top = 192
  end
  object QrItsI: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsIAfterScroll
    SQL.Strings = (
      'SELECT '
      ''
      'nItem, '
      'prod_cProd, prod_cEAN, prod_xProd, '
      'prod_NCM, prod_EXTIPI, prod_CFOP, '
      'prod_uCom, prod_qCom, prod_vUnCom, '
      'prod_vProd, prod_cEANTrib, prod_uTrib, '
      'prod_qTrib, prod_vUnTrib, prod_vFrete, '
      'prod_vSeg, prod_vDesc, prod_vOutro, '
      'prod_indTot, prod_xPed, prod_nItemPed, '
      'GraGruX'
      ''
      'FROM _nfe_i_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      '')
    Left = 324
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItsInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrItsIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrItsIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrItsIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrItsIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrItsIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrItsIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrItsIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrItsIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrItsIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrItsIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrItsIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrItsIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrItsIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrItsIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrItsIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrItsIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrItsIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrItsIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
    end
    object QrItsIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrItsIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrItsIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrItsIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsItsI: TDataSource
    DataSet = QrItsI
    Left = 352
    Top = 220
  end
  object QrItsIDi: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsIDiAfterScroll
    SQL.Strings = (
      'SELECT '
      ''
      'Controle,'
      'DI_nDI, DI_dDI, DI_xLocDesemb, '
      'DI_UFDesemb, DI_dDesemb, DI_cExportador'
      ''
      'FROM _nfe_i_di'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 380
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsIDiDI_nDI: TWideStringField
      FieldName = 'DI_nDI'
      Size = 10
    end
    object QrItsIDiDI_dDI: TDateField
      FieldName = 'DI_dDI'
    end
    object QrItsIDiDI_xLocDesemb: TWideStringField
      FieldName = 'DI_xLocDesemb'
      Size = 60
    end
    object QrItsIDiDI_UFDesemb: TWideStringField
      FieldName = 'DI_UFDesemb'
      Size = 2
    end
    object QrItsIDiDI_dDesemb: TDateField
      FieldName = 'DI_dDesemb'
    end
    object QrItsIDiDI_cExportador: TWideStringField
      FieldName = 'DI_cExportador'
      Size = 60
    end
    object QrItsIDiControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsItsIDi: TDataSource
    DataSet = QrItsIDi
    Left = 408
    Top = 220
  end
  object QrItsIDiA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      'DI_nDI, Adi_nAdicao, Adi_nSeqAdic, '
      'Adi_cFabricante, Adi_vDescDI'
      ''
      'FROM _nfe_i_dia'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2'
      'AND Controle=:P3')
    Left = 436
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrItsIDiADI_nDI: TWideStringField
      FieldName = 'DI_nDI'
      Size = 10
    end
    object QrItsIDiAAdi_nAdicao: TIntegerField
      FieldName = 'Adi_nAdicao'
    end
    object QrItsIDiAAdi_nSeqAdic: TIntegerField
      FieldName = 'Adi_nSeqAdic'
    end
    object QrItsIDiAAdi_cFabricante: TWideStringField
      FieldName = 'Adi_cFabricante'
      Size = 60
    end
    object QrItsIDiAAdi_vDescDI: TFloatField
      FieldName = 'Adi_vDescDI'
    end
  end
  object DsItsIDiA: TDataSource
    DataSet = QrItsIDiA
    Left = 464
    Top = 220
  end
  object QrItsN: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsIAfterScroll
    SQL.Strings = (
      'SELECT '
      ''
      'ICMS_Orig, ICMS_CST, ICMS_modBC, '
      'ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, '
      'ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST, '
      'ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, '
      'ICMS_vICMSST, ICMS_CSOSN, ICMS_UFST, '
      'ICMS_pBCOp, ICMS_vBCSTRet, ICMS_vICMSSTRet, '
      'ICMS_motDesICMS, ICMS_pCredSN, ICMS_vCredICMSSN'
      ''
      'FROM _nfe_n_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 492
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrItsNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrItsNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrItsNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrItsNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrItsNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrItsNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrItsNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrItsNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrItsNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrItsNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrItsNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrItsNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrItsNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrItsNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrItsNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
    end
    object QrItsNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
    end
    object QrItsNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
    end
    object QrItsNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrItsNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
    end
    object QrItsNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
    end
  end
  object DsItsN: TDataSource
    DataSet = QrItsN
    Left = 520
    Top = 220
  end
  object QrItsO: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsIAfterScroll
    SQL.Strings = (
      'SELECT '
      ''
      'IPI_clEnq, IPI_CNPJProd, IPI_cSelo, '
      'IPI_qSelo, IPI_cEnq, IPI_CST, '
      'IPI_vBC, IPI_qUnid, IPI_vUnid, '
      'IPI_pIPI, IPI_vIPI'
      ''
      'FROM _nfe_o_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 548
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrItsOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrItsOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrItsOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrItsOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrItsOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrItsOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrItsOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrItsOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrItsOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrItsOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object DsItsO: TDataSource
    DataSet = QrItsO
    Left = 576
    Top = 220
  end
  object DsItsP: TDataSource
    DataSet = QrItsP
    Left = 632
    Top = 220
  end
  object QrItsP: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsIAfterScroll
    SQL.Strings = (
      'SELECT '
      ''
      'II_vBC, II_vDespAdu, II_vII, '
      'II_vIOF'
      ''
      'FROM _nfe_p_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 604
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsPII_vBC: TFloatField
      FieldName = 'II_vBC'
    end
    object QrItsPII_vDespAdu: TFloatField
      FieldName = 'II_vDespAdu'
    end
    object QrItsPII_vII: TFloatField
      FieldName = 'II_vII'
    end
    object QrItsPII_vIOF: TFloatField
      FieldName = 'II_vIOF'
    end
  end
  object DsItsQ: TDataSource
    DataSet = QrItsQ
    Left = 688
    Top = 220
  end
  object QrItsQ: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrItsIAfterScroll
    SQL.Strings = (
      'SELECT '
      ''
      'PIS_CST, PIS_vBC, PIS_pPIS, '
      'PIS_vPIS, PIS_qBCProd, PIS_vAliqProd, '
      'PIS_fatorBC'
      ''
      'FROM _nfe_q_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 660
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
    end
    object QrItsQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
    end
    object QrItsQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
    end
    object QrItsQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
    end
    object QrItsQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
    end
    object QrItsQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
    end
    object QrItsQPIS_fatorBC: TFloatField
      FieldName = 'PIS_fatorBC'
    end
  end
  object QrItsR: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      'PISST_vBC, PISST_pPIS, PISST_qBCProd, '
      'PISST_vAliqProd, PISST_vPIS, PISST_fatorBCST'
      ''
      'FROM _nfe_r_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 716
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
    end
    object QrItsRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
    end
    object QrItsRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
    end
    object QrItsRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
    end
    object QrItsRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
    end
    object QrItsRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
    end
  end
  object DsItsR: TDataSource
    DataSet = QrItsR
    Left = 744
    Top = 220
  end
  object QrItsS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      'COFINS_CST, COFINS_vBC, COFINS_pCOFINS, '
      'COFINS_qBCProd, COFINS_vAliqProd, COFINS_vCOFINS, '
      'COFINS_fatorBC'
      ''
      'FROM _nfe_s_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 772
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
    end
    object QrItsSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
    end
    object QrItsSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
    end
    object QrItsSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
    end
    object QrItsSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
    end
    object QrItsSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
    end
    object QrItsSCOFINS_fatorBC: TFloatField
      FieldName = 'COFINS_fatorBC'
    end
  end
  object DsItsS: TDataSource
    DataSet = QrItsS
    Left = 800
    Top = 220
  end
  object QrItsT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      'COFINSST_vBC, COFINSST_pCOFINS, COFINSST_qBCProd, '
      'COFINSST_vAliqProd, COFINSST_vCOFINS, COFINSST_fatorBCST'
      ''
      'FROM _nfe_t_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 828
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
    end
    object QrItsTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
    end
    object QrItsTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
    end
    object QrItsTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
    end
    object QrItsTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
    end
    object QrItsTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
    end
  end
  object DsItsT: TDataSource
    DataSet = QrItsT
    Left = 856
    Top = 220
  end
  object QrItsU: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      'ISSQN_vBC, ISSQN_vAliq, ISSQN_vISSQN, '
      'ISSQN_cMunFG, ISSQN_cListServ, ISSQN_cSitTrib'
      ''
      'FROM _nfe_u_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 884
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsUISSQN_vBC: TFloatField
      FieldName = 'ISSQN_vBC'
    end
    object QrItsUISSQN_vAliq: TFloatField
      FieldName = 'ISSQN_vAliq'
    end
    object QrItsUISSQN_vISSQN: TFloatField
      FieldName = 'ISSQN_vISSQN'
    end
    object QrItsUISSQN_cMunFG: TIntegerField
      FieldName = 'ISSQN_cMunFG'
    end
    object QrItsUISSQN_cListServ: TIntegerField
      FieldName = 'ISSQN_cListServ'
    end
    object QrItsUISSQN_cSitTrib: TWideStringField
      FieldName = 'ISSQN_cSitTrib'
      Size = 1
    end
  end
  object DsItsU: TDataSource
    DataSet = QrItsU
    Left = 912
    Top = 220
  end
  object QrItsV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      ''
      'InfAdProd'
      ''
      'FROM _nfe_v_'
      ''
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 940
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItsVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsItsV: TDataSource
    DataSet = QrItsV
    Left = 968
    Top = 220
  end
  object QrLocID: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa'
      'FROM nfecaba'
      'WHERE ID=:P0')
    Left = 608
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocIDFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocIDFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrLocIDEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrInut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_dp01_'
      'WHERE inutNFe_serie=:P0'
      'AND inutNFe_nNFIni=:P1'
      'AND inutNFe_nNFFin=:P2')
    Left = 736
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrInutinutNFe_xJust: TWideStringField
      FieldName = 'inutNFe_xJust'
      Size = 255
    end
  end
  object QrJust: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfejust'
      'WHERE Nome=:P0'
      '')
    Left = 764
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrNFeArqs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ArqDir, ArqName'
      'FROM _nfe_arqs_'
      'WHERE SeqArq=:P0')
    Left = 792
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeArqsArqDir: TWideStringField
      FieldName = 'ArqDir'
      Size = 255
    end
    object QrNFeArqsArqName: TWideStringField
      FieldName = 'ArqName'
      Size = 255
    end
  end
  object QrNFeInut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeinut'
      'WHERE Serie=:P0'
      'AND nNFIni=:P1'
      'AND nNFFim=:P2'
      '')
    Left = 820
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
end
