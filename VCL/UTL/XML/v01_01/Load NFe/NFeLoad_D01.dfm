object FmNFeLoad_D01: TFmNFeLoad_D01
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 503
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 455
    Width = 999
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 887
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object Panel4: TPanel
    Left = 104
    Top = 48
    Width = 999
    Height = 407
    ParentBackground = False
    TabOrder = 1
    object Label8: TLabel
      Left = 8
      Top = 8
      Width = 30
      Height = 13
      Caption = 'CNPJ:'
      FocusControl = Edavulsa_CNPJ
    end
    object Label9: TLabel
      Left = 8
      Top = 48
      Width = 48
      Height = 13
      Caption = 'Matr'#237'cula:'
      FocusControl = Edavulsa_nDAR
    end
    object Label10: TLabel
      Left = 8
      Top = 92
      Width = 45
      Height = 13
      Caption = 'Telefone:'
      FocusControl = Edavulsa_fone
    end
    object Label11: TLabel
      Left = 508
      Top = 8
      Width = 279
      Height = 13
      Caption = 'N'#250'mero do Documento de Arrecada'#231#227'o de Receita (DAR):'
      FocusControl = Edavulsa_matr
    end
    object Label12: TLabel
      Left = 360
      Top = 92
      Width = 128
      Height = 13
      Caption = 'Reparti'#231#227'o Fiscal emitente:'
      FocusControl = Edavulsa_repEmi
    end
    object Label14: TLabel
      Left = 128
      Top = 8
      Width = 75
      Height = 13
      Caption = #211'rg'#227'o emitente:'
      FocusControl = Edavulsa_xOrgao
    end
    object Label15: TLabel
      Left = 500
      Top = 48
      Width = 37
      Height = 13
      Caption = 'Agente:'
      FocusControl = Edavulsa_xAgente
    end
    object Label16: TLabel
      Left = 108
      Top = 92
      Width = 17
      Height = 13
      Caption = 'UF:'
      FocusControl = Edavulsa_UF
    end
    object Label17: TLabel
      Left = 256
      Top = 92
      Width = 91
      Height = 13
      Caption = 'Valor total do DAR:'
      FocusControl = Edavulsa_vDAR
    end
    object Label20: TLabel
      Left = 136
      Top = 92
      Width = 82
      Height = 13
      Caption = 'Data da emiss'#227'o:'
    end
    object Label21: TLabel
      Left = 872
      Top = 92
      Width = 112
      Height = 13
      Caption = 'Data de pagto do DAR:'
    end
    object Edavulsa_CNPJ: TdmkEdit
      Left = 8
      Top = 24
      Width = 116
      Height = 21
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_CNPJ'
      UpdCampo = 'avulsa_CNPJ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_nDAR: TdmkEdit
      Left = 508
      Top = 24
      Width = 480
      Height = 21
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_nDAR'
      UpdCampo = 'avulsa_nDAR'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_fone: TdmkEdit
      Left = 8
      Top = 108
      Width = 96
      Height = 21
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_fone'
      UpdCampo = 'avulsa_fone'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_matr: TdmkEdit
      Left = 8
      Top = 64
      Width = 488
      Height = 21
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_matr'
      UpdCampo = 'avulsa_matr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_repEmi: TdmkEdit
      Left = 360
      Top = 108
      Width = 508
      Height = 21
      ReadOnly = True
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_repEmi'
      UpdCampo = 'avulsa_repEmi'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_xOrgao: TdmkEdit
      Left = 128
      Top = 24
      Width = 377
      Height = 21
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_xOrgao'
      UpdCampo = 'avulsa_xOrgao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_xAgente: TdmkEdit
      Left = 500
      Top = 64
      Width = 488
      Height = 21
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_xAgente'
      UpdCampo = 'avulsa_xAgente'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_UF: TdmkEdit
      Left = 108
      Top = 108
      Width = 24
      Height = 21
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'avulsa_UF'
      UpdCampo = 'avulsa_UF'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edavulsa_vDAR: TdmkEdit
      Left = 256
      Top = 108
      Width = 100
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'avulsa_vDAR'
      UpdCampo = 'avulsa_vDAR'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object TPavulsa_dEmi: TdmkEditDateTimePicker
      Left = 136
      Top = 108
      Width = 116
      Height = 21
      Date = 0.941292650466493800
      Time = 0.941292650466493800
      TabOrder = 7
      ReadOnly = True
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'avulsa_dEmi'
      UpdCampo = 'avulsa_dEmi'
      UpdType = utYes
    end
    object TPavulsa_dPag: TdmkEditDateTimePicker
      Left = 872
      Top = 108
      Width = 116
      Height = 21
      Date = 0.941292650466493800
      Time = 0.941292650466493800
      TabOrder = 10
      ReadOnly = True
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'avulsa_dPag'
      UpdCampo = 'avulsa_dPag'
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = -1
    ExplicitWidth = 1000
    object GB_R: TGroupBox
      Left = 951
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 952
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 903
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 904
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
end
