unit NFeLoad_Inn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, xmldom, XMLIntf, msxmldom, XMLDoc, Variants,
  CheckLst, UnDmkProcFunc, dmkImage, UnInternalConsts, UnDmkEnums,
  Vcl.Menus;

type
  TFmNFeLoad_Inn = class(TForm)
    XMLDocument1: TXMLDocument;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label110: TLabel;
    SBArquivo: TSpeedButton;
    SbCarrega: TSpeedButton;
    EdDiretorio: TEdit;
    Panel5: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    MeExtensoes: TMemo;
    CkSub1: TCheckBox;
    GroupBox1: TGroupBox;
    Ck_nfeProc: TCheckBox;
    Ck_nfe: TCheckBox;
    Ck_procCancNFe: TCheckBox;
    Ck_procInutNFe: TCheckBox;
    Panel8: TPanel;
    CkSoAmbiente1: TCheckBox;
    GroupBox2: TGroupBox;
    LBEleIgnor: TListBox;
    GroupBox3: TGroupBox;
    LBEleDesc: TListBox;
    Panel9: TPanel;
    Label1: TLabel;
    Panel6: TPanel;
    Label3: TLabel;
    MeUnknowFile: TMemo;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    QrNFeB: TmySQLQuery;
    DsNFeB: TDataSource;
    DsNotB: TDataSource;
    QrNotB: TmySQLQuery;
    QrNFeBSeqArq: TIntegerField;
    QrNFeBSeqNFe: TIntegerField;
    QrNFeBversao: TFloatField;
    QrNFeBId: TWideStringField;
    QrNFeBide_cUF: TSmallintField;
    QrNFeBide_natOp: TWideStringField;
    QrNFeBide_indPag: TSmallintField;
    QrNFeBide_mod: TSmallintField;
    QrNFeBide_serie: TIntegerField;
    QrNFeBide_nNF: TIntegerField;
    QrNFeBide_dEmi: TDateField;
    QrNFeBide_dSaiEnt: TDateField;
    QrNFeBide_hSaiEnt: TTimeField;
    QrNFeBide_tpNF: TSmallintField;
    QrNFeBide_cMunFG: TIntegerField;
    QrNFeBide_tpImp: TSmallintField;
    QrNFeBide_tpEmis: TSmallintField;
    QrNFeBide_cDV: TSmallintField;
    QrNFeBide_tpAmb: TSmallintField;
    QrNFeBide_finNFe: TSmallintField;
    QrNFeBide_procEmi: TSmallintField;
    QrNFeBide_verProc: TWideStringField;
    QrNFeBide_dhCont: TDateTimeField;
    QrNFeBide_xJust: TWideStringField;
    TabSheet5: TTabSheet;
    DBGrid3: TDBGrid;
    QrErrB: TmySQLQuery;
    DsErrB: TDataSource;
    QrErrI: TmySQLQuery;
    DsErrI: TDataSource;
    DBGrid4: TDBGrid;
    QrErrInItem: TIntegerField;
    QrErrIGraGruX: TIntegerField;
    QrErrIprod_xProd: TWideStringField;
    QrErrIprod_uCom: TWideStringField;
    QrErrIprod_qCom: TFloatField;
    QrErrIprod_vUnCom: TFloatField;
    QrErrIprod_vProd: TFloatField;
    QrNotBSeqArq: TIntegerField;
    QrNotBSeqNFe: TIntegerField;
    QrNotBversao: TFloatField;
    QrNotBId: TWideStringField;
    QrNotBide_cUF: TSmallintField;
    QrNotBide_natOp: TWideStringField;
    QrNotBide_indPag: TSmallintField;
    QrNotBide_mod: TSmallintField;
    QrNotBide_serie: TIntegerField;
    QrNotBide_nNF: TIntegerField;
    QrNotBide_dEmi: TDateField;
    QrNotBide_dSaiEnt: TDateField;
    QrNotBide_hSaiEnt: TTimeField;
    QrNotBide_tpNF: TSmallintField;
    QrNotBide_cMunFG: TIntegerField;
    QrNotBide_tpImp: TSmallintField;
    QrNotBide_tpEmis: TSmallintField;
    QrNotBide_cDV: TSmallintField;
    QrNotBide_tpAmb: TSmallintField;
    QrNotBide_finNFe: TSmallintField;
    QrNotBide_procEmi: TSmallintField;
    QrNotBide_verProc: TWideStringField;
    QrNotBide_dhCont: TDateTimeField;
    QrNotBide_xJust: TWideStringField;
    QrErrBSeqArq: TIntegerField;
    QrErrBSeqNFe: TIntegerField;
    QrErrBversao: TFloatField;
    QrErrBId: TWideStringField;
    QrErrBide_cUF: TSmallintField;
    QrErrBide_natOp: TWideStringField;
    QrErrBide_indPag: TSmallintField;
    QrErrBide_mod: TSmallintField;
    QrErrBide_serie: TIntegerField;
    QrErrBide_nNF: TIntegerField;
    QrErrBide_dEmi: TDateField;
    QrErrBide_dSaiEnt: TDateField;
    QrErrBide_hSaiEnt: TTimeField;
    QrErrBide_tpNF: TSmallintField;
    QrErrBide_cMunFG: TIntegerField;
    QrErrBide_tpImp: TSmallintField;
    QrErrBide_tpEmis: TSmallintField;
    QrErrBide_cDV: TSmallintField;
    QrErrBide_tpAmb: TSmallintField;
    QrErrBide_finNFe: TSmallintField;
    QrErrBide_procEmi: TSmallintField;
    QrErrBide_verProc: TWideStringField;
    QrErrBide_dhCont: TDateTimeField;
    QrErrBide_xJust: TWideStringField;
    QrErrBErrEmit: TSmallintField;
    QrErrBErrDest: TSmallintField;
    QrErrBErrProd: TSmallintField;
    QrLocAnt: TmySQLQuery;
    QrLocAntFatID: TIntegerField;
    QrLocAntFatNum: TIntegerField;
    QrLocAntEmpresa: TIntegerField;
    QrNFeC: TmySQLQuery;
    DsNFeC: TDataSource;
    QrNFeCemit_CNPJ: TWideStringField;
    QrNFeCemit_CPF: TWideStringField;
    QrNFeCemit_xNome: TWideStringField;
    QrNFeCemit_xFant: TWideStringField;
    QrNFeCemit_xLgr: TWideStringField;
    QrNFeCemit_nro: TWideStringField;
    QrNFeCemit_xCpl: TWideStringField;
    QrNFeCemit_xBairro: TWideStringField;
    QrNFeCemit_cMun: TIntegerField;
    QrNFeCemit_xMun: TWideStringField;
    QrNFeCemit_UF: TWideStringField;
    QrNFeCemit_CEP: TIntegerField;
    QrNFeCemit_cPais: TIntegerField;
    QrNFeCemit_xPais: TWideStringField;
    QrNFeCemit_fone: TWideStringField;
    QrNFeCemit_IE: TWideStringField;
    QrNFeCemit_IEST: TWideStringField;
    QrNFeCemit_IM: TWideStringField;
    QrNFeCemit_CNAE: TWideStringField;
    QrNFeCemit_CRT: TSmallintField;
    QrNFeCemit_xCNAE: TWideStringField;
    QrNFeCemit_xCRT: TWideStringField;
    QrNFeCCodInfoEmit: TIntegerField;
    QrNFeBide_cNF: TIntegerField;
    QrNFeE: TmySQLQuery;
    DsNFeE: TDataSource;
    QrNFeEdest_CNPJ: TWideStringField;
    QrNFeEdest_CPF: TWideStringField;
    QrNFeEdest_xNome: TWideStringField;
    QrNFeEdest_xLgr: TWideStringField;
    QrNFeEdest_nro: TWideStringField;
    QrNFeEdest_xCpl: TWideStringField;
    QrNFeEdest_xBairro: TWideStringField;
    QrNFeEdest_cMun: TIntegerField;
    QrNFeEdest_xMun: TWideStringField;
    QrNFeEdest_UF: TWideStringField;
    QrNFeEdest_CEP: TWideStringField;
    QrNFeEdest_cPais: TIntegerField;
    QrNFeEdest_xPais: TWideStringField;
    QrNFeEdest_fone: TWideStringField;
    QrNFeEdest_IE: TWideStringField;
    QrNFeEdest_ISUF: TWideStringField;
    QrNFeEdest_email: TWideStringField;
    QrNFeECodInfoDest: TIntegerField;
    QrNFeW02: TmySQLQuery;
    QrNFeW02ICMSTot_vBC: TFloatField;
    QrNFeW02ICMSTot_vICMS: TFloatField;
    QrNFeW02ICMSTot_vBCST: TFloatField;
    QrNFeW02ICMSTot_vST: TFloatField;
    QrNFeW02ICMSTot_vProd: TFloatField;
    QrNFeW02ICMSTot_vFrete: TFloatField;
    QrNFeW02ICMSTot_vSeg: TFloatField;
    QrNFeW02ICMSTot_vDesc: TFloatField;
    QrNFeW02ICMSTot_vII: TFloatField;
    QrNFeW02ICMSTot_vIPI: TFloatField;
    QrNFeW02ICMSTot_vPIS: TFloatField;
    QrNFeW02ICMSTot_vCOFINS: TFloatField;
    QrNFeW02ICMSTot_vOutro: TFloatField;
    QrNFeW02ICMSTot_vNF: TFloatField;
    QrNFeW17: TmySQLQuery;
    QrNFeW17ISSQNtot_vServ: TFloatField;
    QrNFeW17ISSQNtot_vBC: TFloatField;
    QrNFeW17ISSQNtot_vISS: TFloatField;
    QrNFeW17ISSQNtot_vPIS: TFloatField;
    QrNFeW17ISSQNtot_vCOFINS: TFloatField;
    DsNFeW02: TDataSource;
    DsNFeW17: TDataSource;
    QrNFeW23: TmySQLQuery;
    DsNFeW23: TDataSource;
    QrNFeX01: TmySQLQuery;
    DsNFeX01: TDataSource;
    QrNFeW23RetTrib_vRetPIS: TFloatField;
    QrNFeW23RetTrib_vRetCOFINS: TFloatField;
    QrNFeW23RetTrib_vRetCSLL: TFloatField;
    QrNFeW23RetTrib_vBCIRRF: TFloatField;
    QrNFeW23RetTrib_vIRRF: TFloatField;
    QrNFeW23RetTrib_vBCRetPrev: TFloatField;
    QrNFeW23RetTrib_vRetPrev: TFloatField;
    DsNFeY01: TDataSource;
    QrNFeY01: TmySQLQuery;
    QrNFeX01ModFrete: TSmallintField;
    QrNFeX01xModFrete: TWideStringField;
    QrNFeX01Transporta_CNPJ: TWideStringField;
    QrNFeX01Transporta_CPF: TWideStringField;
    QrNFeX01Transporta_XNome: TWideStringField;
    QrNFeX01Transporta_IE: TWideStringField;
    QrNFeX01Transporta_XEnder: TWideStringField;
    QrNFeX01Transporta_XMun: TWideStringField;
    QrNFeX01Transporta_UF: TWideStringField;
    QrNFeX01RetTransp_vServ: TFloatField;
    QrNFeX01RetTransp_vBCRet: TFloatField;
    QrNFeX01RetTransp_PICMSRet: TFloatField;
    QrNFeX01RetTransp_vICMSRet: TFloatField;
    QrNFeX01RetTransp_CFOP: TWideStringField;
    QrNFeX01RetTransp_CMunFG: TWideStringField;
    QrNFeX01RetTransp_xMunFG: TWideStringField;
    QrNFeX01VeicTransp_Placa: TWideStringField;
    QrNFeX01VeicTransp_UF: TWideStringField;
    QrNFeX01VeicTransp_RNTC: TWideStringField;
    QrNFeX01Vagao: TWideStringField;
    QrNFeX01Balsa: TWideStringField;
    DsNFeZ01: TDataSource;
    QrNFeZ01: TmySQLQuery;
    QrNFeY01Cobr_Fat_nFat: TWideStringField;
    QrNFeY01Cobr_Fat_vOrig: TFloatField;
    QrNFeY01Cobr_Fat_vDesc: TFloatField;
    QrNFeY01Cobr_Fat_vLiq: TFloatField;
    QrNFeZA01: TmySQLQuery;
    DsNFeZA01: TDataSource;
    QrNFeZ01InfAdic_InfAdFisco: TWideMemoField;
    QrNFeZ01InfAdic_InfCpl: TWideMemoField;
    DsNFeZB01: TDataSource;
    QrNFeZB01: TmySQLQuery;
    QrNFeZA01Exporta_UFEmbarq: TWideStringField;
    QrNFeZA01Exporta_XLocEmbarq: TWideStringField;
    DsPR01: TDataSource;
    QrPR01: TmySQLQuery;
    QrNFeZB01Compra_XNEmp: TWideStringField;
    QrNFeZB01Compra_XPed: TWideStringField;
    QrNFeZB01Compra_XCont: TWideStringField;
    QrCR01: TmySQLQuery;
    DsCR01: TDataSource;
    QrPR01protNFe_versao: TFloatField;
    QrPR01infProt_Id: TWideStringField;
    QrPR01infProt_tpAmb: TSmallintField;
    QrPR01infProt_xAmb: TWideStringField;
    QrPR01infProt_verAplic: TWideStringField;
    QrPR01infProt_chNFe: TWideStringField;
    QrPR01infProt_dhRecbto: TDateTimeField;
    QrPR01infProt_nProt: TLargeintField;
    QrPR01infProt_digVal: TWideStringField;
    QrPR01infProt_cStat: TIntegerField;
    QrPR01infProt_xMotivo: TWideStringField;
    QrDR01: TmySQLQuery;
    DsDR01: TDataSource;
    QrCR01retCanc_versao: TFloatField;
    QrCR01infCanc_Id: TWideStringField;
    QrCR01infCanc_tpAmb: TSmallintField;
    QrCR01infCanc_xAmb: TWideStringField;
    QrCR01infCanc_verAplic: TWideStringField;
    QrCR01infCanc_cStat: TIntegerField;
    QrCR01infCanc_xMotivo: TWideStringField;
    QrCR01infCanc_cUF: TSmallintField;
    QrCR01infCanc_xUF: TWideStringField;
    QrCR01infCanc_chNFe: TWideStringField;
    QrCR01infCanc_dhRecbto: TDateTimeField;
    QrCR01infCanc_nProt: TLargeintField;
    Panel16: TPanel;
    EdFatID: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEdit;
    Label5: TLabel;
    QrNFeB12a: TmySQLQuery;
    DsNFeB12a: TDataSource;
    QrNFeB12arefNFe: TWideStringField;
    QrNFeB12arefNF_cUF: TSmallintField;
    QrNFeB12arefNF_xUF: TWideStringField;
    QrNFeB12arefNF_AAMM: TIntegerField;
    QrNFeB12arefNF_CNPJ: TWideStringField;
    QrNFeB12arefNF_mod: TSmallintField;
    QrNFeB12arefNF_serie: TIntegerField;
    QrNFeB12arefNF_nNF: TIntegerField;
    QrNFeB12arefNFP_cUF: TSmallintField;
    QrNFeB12arefNFP_xUF: TWideStringField;
    QrNFeB12arefNFP_AAMM: TSmallintField;
    QrNFeB12arefNFP_CNPJ: TWideStringField;
    QrNFeB12arefNFP_CPF: TWideStringField;
    QrNFeB12arefNFP_IE: TWideStringField;
    QrNFeB12arefNFP_mod: TSmallintField;
    QrNFeB12arefNFP_serie: TSmallintField;
    QrNFeB12arefNFP_nNF: TIntegerField;
    QrNFeB12arefCTe: TWideStringField;
    QrNFeB12arefECF_mod: TWideStringField;
    QrNFeB12arefECF_nECF: TSmallintField;
    QrNFeB12arefECF_nCOO: TIntegerField;
    QrNFeF: TmySQLQuery;
    DsNFeF: TDataSource;
    DsNFeG: TDataSource;
    QrNFeG: TmySQLQuery;
    QrNFeFretirada_CNPJ: TWideStringField;
    QrNFeFretirada_CPF: TWideStringField;
    QrNFeFretirada_xLgr: TWideStringField;
    QrNFeFretirada_nro: TWideStringField;
    QrNFeFretirada_xCpl: TWideStringField;
    QrNFeFretirada_xBairro: TWideStringField;
    QrNFeFretirada_cMun: TIntegerField;
    QrNFeFretirada_xMun: TWideStringField;
    QrNFeFretirada_UF: TWideStringField;
    QrNFeGentrega_CNPJ: TWideStringField;
    QrNFeGentrega_CPF: TWideStringField;
    QrNFeGentrega_xLgr: TWideStringField;
    QrNFeGentrega_nro: TWideStringField;
    QrNFeGentrega_xCpl: TWideStringField;
    QrNFeGentrega_xBairro: TWideStringField;
    QrNFeGentrega_cMun: TIntegerField;
    QrNFeGentrega_xMun: TWideStringField;
    QrNFeGentrega_UF: TWideStringField;
    QrNFeX22: TmySQLQuery;
    DsNFeX22: TDataSource;
    QrNFeX22placa: TWideStringField;
    QrNFeX22UF: TWideStringField;
    QrNFeX22RNTC: TWideStringField;
    QrNFeX26: TmySQLQuery;
    DsNFeX26: TDataSource;
    QrNFeX26qVol: TFloatField;
    QrNFeX26esp: TWideStringField;
    QrNFeX26marca: TWideStringField;
    QrNFeX26nVol: TWideStringField;
    QrNFeX26pesoL: TFloatField;
    QrNFeX26pesoB: TFloatField;
    QrNFeX33: TmySQLQuery;
    DsNFeX33: TDataSource;
    QrNFeX33nLacre: TWideStringField;
    QrNFeX26SeqArq: TIntegerField;
    QrNFeX26SeqNFe: TIntegerField;
    QrNFeX26Controle: TIntegerField;
    QrNFeY07: TmySQLQuery;
    DsNFeY07: TDataSource;
    QrNFeY07nDup: TWideStringField;
    QrNFeY07dVenc: TDateField;
    QrNFeY07vDup: TFloatField;
    QrNFeZ04: TmySQLQuery;
    DsNFeZ04: TDataSource;
    QrNFeZ04xCampo: TWideStringField;
    QrNFeZ04xTexto: TWideStringField;
    DsNFeZ07: TDataSource;
    QrNFeZ07: TmySQLQuery;
    QrNFeZ07xCampo: TWideStringField;
    QrNFeZ07xTexto: TWideStringField;
    QrNFeZ10: TmySQLQuery;
    DsNFeZ10: TDataSource;
    QrNFeZ10nProc: TWideStringField;
    QrNFeZ10indProc: TSmallintField;
    QrItsI: TmySQLQuery;
    DsItsI: TDataSource;
    QrItsIprod_cProd: TWideStringField;
    QrItsIprod_cEAN: TWideStringField;
    QrItsIprod_xProd: TWideStringField;
    QrItsIprod_NCM: TWideStringField;
    QrItsIprod_EXTIPI: TWideStringField;
    QrItsIprod_CFOP: TIntegerField;
    QrItsIprod_uCom: TWideStringField;
    QrItsIprod_qCom: TFloatField;
    QrItsIprod_vUnCom: TFloatField;
    QrItsIprod_vProd: TFloatField;
    QrItsIprod_cEANTrib: TWideStringField;
    QrItsIprod_uTrib: TWideStringField;
    QrItsIprod_qTrib: TFloatField;
    QrItsIprod_vUnTrib: TFloatField;
    QrItsIprod_vFrete: TFloatField;
    QrItsIprod_vSeg: TFloatField;
    QrItsIprod_vDesc: TFloatField;
    QrItsIprod_vOutro: TFloatField;
    QrItsIprod_indTot: TSmallintField;
    QrItsIprod_xPed: TWideStringField;
    QrItsIprod_nItemPed: TIntegerField;
    QrItsIGraGruX: TIntegerField;
    QrItsInItem: TIntegerField;
    QrItsIDi: TmySQLQuery;
    DsItsIDi: TDataSource;
    QrItsIDiDI_nDI: TWideStringField;
    QrItsIDiDI_dDI: TDateField;
    QrItsIDiDI_xLocDesemb: TWideStringField;
    QrItsIDiDI_UFDesemb: TWideStringField;
    QrItsIDiDI_dDesemb: TDateField;
    QrItsIDiDI_cExportador: TWideStringField;
    QrItsIDiControle: TIntegerField;
    QrItsIDiA: TmySQLQuery;
    DsItsIDiA: TDataSource;
    QrItsIDiADI_nDI: TWideStringField;
    QrItsIDiAAdi_nAdicao: TIntegerField;
    QrItsIDiAAdi_nSeqAdic: TIntegerField;
    QrItsIDiAAdi_cFabricante: TWideStringField;
    QrItsIDiAAdi_vDescDI: TFloatField;
    QrItsM: TmySQLQuery;
    DsItsM: TDataSource;
    QrItsO: TmySQLQuery;
    DsItsO: TDataSource;
    DsItsP: TDataSource;
    QrItsP: TmySQLQuery;
    DsItsQ: TDataSource;
    QrItsQ: TmySQLQuery;
    QrItsPII_vBC: TFloatField;
    QrItsPII_vDespAdu: TFloatField;
    QrItsPII_vII: TFloatField;
    QrItsPII_vIOF: TFloatField;
    QrItsQPIS_CST: TSmallintField;
    QrItsQPIS_vBC: TFloatField;
    QrItsQPIS_pPIS: TFloatField;
    QrItsQPIS_vPIS: TFloatField;
    QrItsQPIS_qBCProd: TFloatField;
    QrItsQPIS_vAliqProd: TFloatField;
    QrItsQPIS_fatorBC: TFloatField;
    QrItsR: TmySQLQuery;
    DsItsR: TDataSource;
    QrItsS: TmySQLQuery;
    DsItsS: TDataSource;
    QrItsRPISST_vBC: TFloatField;
    QrItsRPISST_pPIS: TFloatField;
    QrItsRPISST_qBCProd: TFloatField;
    QrItsRPISST_vAliqProd: TFloatField;
    QrItsRPISST_vPIS: TFloatField;
    QrItsRPISST_fatorBCST: TFloatField;
    QrItsT: TmySQLQuery;
    DsItsT: TDataSource;
    QrItsU: TmySQLQuery;
    DsItsU: TDataSource;
    QrItsSCOFINS_CST: TSmallintField;
    QrItsSCOFINS_vBC: TFloatField;
    QrItsSCOFINS_pCOFINS: TFloatField;
    QrItsSCOFINS_qBCProd: TFloatField;
    QrItsSCOFINS_vAliqProd: TFloatField;
    QrItsSCOFINS_vCOFINS: TFloatField;
    QrItsSCOFINS_fatorBC: TFloatField;
    QrItsTCOFINSST_vBC: TFloatField;
    QrItsTCOFINSST_pCOFINS: TFloatField;
    QrItsTCOFINSST_qBCProd: TFloatField;
    QrItsTCOFINSST_vAliqProd: TFloatField;
    QrItsTCOFINSST_vCOFINS: TFloatField;
    QrItsTCOFINSST_fatorBCST: TFloatField;
    QrItsUISSQN_vBC: TFloatField;
    QrItsUISSQN_vAliq: TFloatField;
    QrItsUISSQN_vISSQN: TFloatField;
    QrItsUISSQN_cMunFG: TIntegerField;
    QrItsUISSQN_cListServ: TIntegerField;
    QrItsUISSQN_cSitTrib: TWideStringField;
    QrItsOIPI_clEnq: TWideStringField;
    QrItsOIPI_CNPJProd: TWideStringField;
    QrItsOIPI_cSelo: TWideStringField;
    QrItsOIPI_qSelo: TFloatField;
    QrItsOIPI_cEnq: TWideStringField;
    QrItsOIPI_CST: TSmallintField;
    QrItsOIPI_vBC: TFloatField;
    QrItsOIPI_qUnid: TFloatField;
    QrItsOIPI_vUnid: TFloatField;
    QrItsOIPI_pIPI: TFloatField;
    QrItsOIPI_vIPI: TFloatField;
    QrItsV: TmySQLQuery;
    DsItsV: TDataSource;
    QrItsVInfAdProd: TWideMemoField;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    DBGrid5: TDBGrid;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid2: TDBGrid;
    QrLocID: TmySQLQuery;
    QrLocIDFatID: TIntegerField;
    QrLocIDFatNum: TIntegerField;
    QrLocIDEmpresa: TIntegerField;
    DBGrid6: TDBGrid;
    QrDR01retInutNFe_versao: TFloatField;
    QrDR01retInutNFe_Id: TWideStringField;
    QrDR01retInutNFe_tpAmb: TSmallintField;
    QrDR01retInutNFe_xAmb: TWideStringField;
    QrDR01retInutNFe_verAplic: TWideStringField;
    QrDR01retInutNFe_cStat: TIntegerField;
    QrDR01retInutNFe_xMotivo: TWideStringField;
    QrDR01retInutNFe_cUF: TSmallintField;
    QrDR01retInutNFe_xUF: TWideStringField;
    QrDR01retInutNFe_ano: TSmallintField;
    QrDR01retInutNFe_CNPJ: TWideStringField;
    QrDR01retInutNFe_mod: TSmallintField;
    QrDR01retInutNFe_serie: TIntegerField;
    QrDR01retInutNFe_nNFIni: TIntegerField;
    QrDR01retInutNFe_nNFFin: TIntegerField;
    QrDR01retInutNFe_dhRecbto: TDateTimeField;
    QrDR01retInutNFe_nProt: TLargeintField;
    QrInut: TmySQLQuery;
    QrJust: TmySQLQuery;
    QrInutinutNFe_xJust: TWideStringField;
    QrJustCodigo: TIntegerField;
    QrNFeArqs: TmySQLQuery;
    QrNFeArqsArqDir: TWideStringField;
    QrNFeArqsArqName: TWideStringField;
    QrDR01SeqArq: TIntegerField;
    QrNFeInut: TmySQLQuery;
    TabSheet7: TTabSheet;
    GradeErrXML: TStringGrid;
    QrErrBCodInfoEmit: TIntegerField;
    QrErrIprod_cProd: TWideStringField;
    QrErrIprod_NCM: TWideStringField;
    QrErrIprod_EXTIPI: TWideStringField;
    QrErrIprod_cEAN: TWideStringField;
    QrErrIInfAdProd: TWideMemoField;
    QrErrIICMS_Orig: TSmallintField;
    QrErrIICMS_CST: TSmallintField;
    QrErrIIPI_cEnq: TWideStringField;
    QrErrIPIS_CST: TSmallintField;
    QrErrICOFINS_CST: TSmallintField;
    QrErrIIPI_CST: TSmallintField;
    QrLocAntDataFiscal: TDateField;
    LBArqs: TCheckListBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel17: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PBx: TProgressBar;
    GBRodaPe: TGroupBox;
    Panel18: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtCarregar: TBitBtn;
    GroupBox4: TGroupBox;
    Panel2: TPanel;
    Panel19: TPanel;
    BtInclui: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox5: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BtEmit: TBitBtn;
    BtDest: TBitBtn;
    BtProd: TBitBtn;
    BitBtn4: TBitBtn;
    GroupBox6: TGroupBox;
    Panel12: TPanel;
    Panel13: TPanel;
    BitBtn6: TBitBtn;
    QrFatID_0052: TmySQLQuery;
    QrFatID_0052FatID: TIntegerField;
    QrFatID_0052FatNum: TIntegerField;
    QrFatID_0052Empresa: TIntegerField;
    QrFatID_0052IDCtrl: TIntegerField;
    QrFatID_0052eveMDe_Id: TWideStringField;
    QrFatID_0052eveMDe_tpAmb: TSmallintField;
    QrFatID_0052eveMDe_verAplic: TWideStringField;
    QrFatID_0052eveMDe_cOrgao: TSmallintField;
    QrFatID_0052eveMDe_cStat: TIntegerField;
    QrFatID_0052eveMDe_xMotivo: TWideStringField;
    QrFatID_0052eveMDe_chNFe: TWideStringField;
    QrFatID_0052eveMDe_tpEvento: TIntegerField;
    QrFatID_0052eveMDe_xEvento: TWideStringField;
    QrFatID_0052eveMDe_nSeqEvento: TSmallintField;
    QrFatID_0052eveMDe_CNPJDest: TWideStringField;
    QrFatID_0052eveMDe_CPFDest: TWideStringField;
    QrFatID_0052eveMDe_emailDest: TWideStringField;
    QrFatID_0052eveMDe_dhRegEvento: TDateTimeField;
    QrFatID_0052eveMDe_TZD_UTC: TFloatField;
    QrFatID_0052eveMDe_nProt: TWideStringField;
    QrFatID_0052cSitNFe: TSmallintField;
    QrFatID_0052cSitConf: TSmallintField;
    DBGrid7: TDBGrid;
    QrNFeI: TmySQLQuery;
    QrNFeInItem: TIntegerField;
    QrNFeIGraGruX: TIntegerField;
    QrNFeIprod_xProd: TWideStringField;
    QrNFeIprod_uCom: TWideStringField;
    QrNFeIprod_qCom: TFloatField;
    QrNFeIprod_vUnCom: TFloatField;
    QrNFeIprod_vProd: TFloatField;
    QrNFeIprod_cProd: TWideStringField;
    QrNFeIprod_NCM: TWideStringField;
    QrNFeIprod_EXTIPI: TWideStringField;
    QrNFeIprod_cEAN: TWideStringField;
    QrNFeIInfAdProd: TWideMemoField;
    QrNFeIICMS_Orig: TSmallintField;
    QrNFeIICMS_CST: TSmallintField;
    QrNFeIIPI_cEnq: TWideStringField;
    QrNFeIPIS_CST: TSmallintField;
    QrNFeICOFINS_CST: TSmallintField;
    QrNFeIIPI_CST: TSmallintField;
    DsNFeI: TDataSource;
    QrNFeBCodInfoEmit: TIntegerField;
    QrNFeBide_hEmi: TTimeField;
    QrNFeBide_dhEmiTZD: TFloatField;
    QrNFeBide_dhSaiEntTZD: TFloatField;
    QrNFeBide_idDest: TSmallintField;
    QrNFeBide_indFinal: TSmallintField;
    QrNFeBide_indPres: TSmallintField;
    QrNFeEdest_indIEDest: TSmallintField;
    QrNFeEdest_IM: TWideStringField;
    QrNFeW02ICMSTot_vICMSDeson: TFloatField;
    QrNFeW02ICMSTot_vFCPUFDest: TFloatField;
    QrNFeW02ICMSTot_vICMSUFDest: TFloatField;
    QrNFeW02ICMSTot_vICMSUFRemet: TFloatField;
    QrNFeZA01Exporta_XLocDespacho: TWideStringField;
    QrItsIprod_nFCI: TWideStringField;
    QrItsIprod_CEST: TIntegerField;
    QrItsINivel1: TIntegerField;
    DsItsN: TDataSource;
    QrItsN: TmySQLQuery;
    QrItsNICMS_Orig: TSmallintField;
    QrItsNICMS_CST: TSmallintField;
    QrItsNICMS_modBC: TSmallintField;
    QrItsNICMS_pRedBC: TFloatField;
    QrItsNICMS_vBC: TFloatField;
    QrItsNICMS_pICMS: TFloatField;
    QrItsNICMS_vICMS: TFloatField;
    QrItsNICMS_modBCST: TSmallintField;
    QrItsNICMS_pMVAST: TFloatField;
    QrItsNICMS_pRedBCST: TFloatField;
    QrItsNICMS_vBCST: TFloatField;
    QrItsNICMS_pICMSST: TFloatField;
    QrItsNICMS_vICMSST: TFloatField;
    QrItsNICMS_CSOSN: TIntegerField;
    QrItsNICMS_UFST: TWideStringField;
    QrItsNICMS_pBCOp: TFloatField;
    QrItsNICMS_vBCSTRet: TFloatField;
    QrItsNICMS_vICMSSTRet: TFloatField;
    QrItsNICMS_motDesICMS: TSmallintField;
    QrItsNICMS_pCredSN: TFloatField;
    QrItsNICMS_vCredICMSSN: TFloatField;
    QrItsMnItem: TIntegerField;
    QrItsMvTotTrib: TFloatField;
    QrItsNICMS_vICMSOp: TFloatField;
    QrItsNICMS_pDif: TFloatField;
    QrItsNICMS_vICMSDif: TFloatField;
    QrItsNICMS_vICMSDeson: TFloatField;
    QrItsUISSQN_vDeducao: TFloatField;
    QrItsUISSQN_vOutro: TFloatField;
    QrItsUISSQN_vDescIncond: TFloatField;
    QrItsUISSQN_vDescCond: TFloatField;
    QrItsUISSQN_vISSRet: TFloatField;
    QrItsUISSQN_indISS: TSmallintField;
    QrItsUISSQN_cServico: TWideStringField;
    QrItsUISSQN_cMun: TIntegerField;
    QrItsUISSQN_cPais: TIntegerField;
    QrItsUISSQN_nProcesso: TWideStringField;
    QrItsUISSQN_indIncentivo: TSmallintField;
    QrNFeGA: TmySQLQuery;
    DsNFeGA: TDataSource;
    QrNFeGAautXML_CNPJ: TWideStringField;
    QrNFeGAautXML_CPF: TWideStringField;
    PMProduto: TPopupMenu;
    CodificaoporFornecedor1: TMenuItem;
    N1: TMenuItem;
    CadastrodeUsoeconsumo1: TMenuItem;
    QrNFeBNomeEmit: TWideStringField;
    QrErrBNomeEmit: TWideStringField;
    Cadastrodeprodutodiretonagrade1: TMenuItem;
    Cadastrodemateriaprima1: TMenuItem;
    Cadastrodeprodutoacabado1: TMenuItem;
    Cadastrodesubproduto1: TMenuItem;
    CadastrodeservioNFe1: TMenuItem;
    QrNFeINO_PRD_TAM_COR: TWideStringField;
    QrNFeISIGLAUNIDMED: TWideStringField;
    QrErrBemit_CNPJ: TWideStringField;
    QrErrBemit_CPF: TWideStringField;
    CadastrodeUsopermanente1: TMenuItem;
    QrErrIprod_CFOP: TIntegerField;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    QrErrV: TmySQLQuery;
    DsErrV: TDataSource;
    QrErrVInfAdProd: TWideMemoField;
    Removeratrelamentocomreduzido1: TMenuItem;
    Definiratrelamentoavulsocomreduzido1: TMenuItem;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrErrIKlsGGXE: TSmallintField;
    CkRecriaTbTmp: TCheckBox;
    CriarIDdecodificaomltipla1: TMenuItem;
    N2: TMenuItem;
    QrErrINomeGGX: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBArquivoClick(Sender: TObject);
    procedure SbCarregaClick(Sender: TObject);
    procedure BtCarregarClick(Sender: TObject);
    procedure CkSoAmbiente1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrErrBAfterOpen(DataSet: TDataSet);
    procedure QrErrBBeforeClose(DataSet: TDataSet);
    procedure QrErrBAfterScroll(DataSet: TDataSet);
    procedure QrErrIAfterScroll(DataSet: TDataSet);
    procedure BtDestClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrNFeX26BeforeClose(DataSet: TDataSet);
    procedure QrNFeX26AfterScroll(DataSet: TDataSet);
    procedure QrItsIAfterScroll(DataSet: TDataSet);
    procedure QrItsIDiAfterScroll(DataSet: TDataSet);
    procedure BtProdClick(Sender: TObject);
    procedure QrNFeBAfterScroll(DataSet: TDataSet);
    procedure QrNFeBBeforeClose(DataSet: TDataSet);
    procedure CodificaoporFornecedor1Click(Sender: TObject);
    procedure CadastrodeUsoeconsumo1Click(Sender: TObject);
    procedure Cadastrodeprodutodiretonagrade1Click(Sender: TObject);
    procedure Cadastrodemateriaprima1Click(Sender: TObject);
    procedure Cadastrodeprodutoacabado1Click(Sender: TObject);
    procedure CadastrodeservioNFe1Click(Sender: TObject);
    procedure Cadastrodesubproduto1Click(Sender: TObject);
    procedure BtEmitClick(Sender: TObject);
    procedure CadastrodeUsopermanente1Click(Sender: TObject);
    procedure QrErrIBeforeClose(DataSet: TDataSet);
    procedure Definiratrelamentoavulsocomreduzido1Click(Sender: TObject);
    procedure Removeratrelamentocomreduzido1Click(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure CriarIDdecodificaomltipla1Click(Sender: TObject);
  private
    { Private declarations }
    FSeqArq, FSeqNFe: Integer;
    procedure ImportaNFes();
    procedure AbreCarregados();
    procedure AtualizaTodosGGXIguaisMul(cProd, xProd: String; CodInfoEmit:
              Integer);
    procedure AtualizaTodosGGXIguaisUni(cProd, xProd, NCM, uCom: String;
              CFOP, CodInfoEmit, GraGruX: Integer; NomeGGX: String);
    function ExcluiNotaAntiga(var FatNum: Integer; var DataFiscal: TDateTime): Boolean;
    function IncluiNFeCabA(const FatID: Integer; var FatNum: Integer;
             const Empresa: Integer; const DtaEnt, DtaFis: TDateTime): Boolean;
    function IncluiNFeCabB(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabF(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabG(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabGA(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabXReb(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabXVol(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabXLac(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabY(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabZCon(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabZFis(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeCabZPro(FatID, FatNum, Empresa: Integer): Boolean;
    function IncluiNFeItsI(FatID, FatNum, Empresa: Integer): Boolean;
    {
    function IncluiNFeItsIDi(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsIDiA(FatID, FatNum, Empresa, nItem, Controle: Integer): Boolean;
    }
    function IncluiNFeItsM(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsN(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsO(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsP(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsQ(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsR(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsS(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsT(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsU(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    function IncluiNFeItsV(FatID, FatNum, Empresa, nItem: Integer): Boolean;
    //
    function IncluiStqMovItsA(FatID, FatNum, Empresa: Integer; DataFiscal: TDateTime): Boolean;
    //
    procedure MarcaTodosLB(Marca: Boolean);
    function  ObtemXML_De_Arquivo(Arquivo: String): WideString;
    //
    procedure UpdateInfoErros();
    function  VerificaGGE(): Boolean;

  public
    { Public declarations }
  end;

  var
  FmNFeLoad_Inn: TFmNFeLoad_Inn;

implementation

uses UnMyObjects, NFeLoad_Arq, ModuleGeral, ModuleNFe_0000, UMySQLModule,
  Module, NFeXMLGerencia, GraGruEIts, MyDBCheck, DmkDAC_PF, NFe_PF, UnAppPF,
  UnGrade_Jan, UnGrade_PF;

{$R *.DFM}

procedure TFmNFeLoad_Inn.AbreCarregados();
  procedure ReabreQuery(qry: TmySQLQuery; Filtro1, Filtro2: String);
  begin
(*
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT c.CodInfoEmit, a.SeqArq, a.SeqNFe, a.versao, a.Id,');
    Qry.SQL.Add('b.ide_cUF, b.ide_cNF, b.ide_natOp, b.ide_indPag, b.ide_mod,');
    Qry.SQL.Add('b.ide_serie, b.ide_nNF, b.ide_dEmi, b.ide_dSaiEnt,');
    Qry.SQL.Add('b.ide_hSaiEnt, b.ide_tpNF, b.ide_cMunFG, b.ide_tpImp,');
    Qry.SQL.Add('b.ide_tpEmis, b.ide_cDV, b.ide_tpAmb, b.ide_finNFe,');
    Qry.SQL.Add('b.ide_procEmi, b.ide_verProc, b.ide_dhCont, b.ide_xJust,');
    Qry.SQL.Add('a.ErrEmit, a.ErrDest, a.ErrTrsp, a.ErrProd');
    Qry.SQL.Add('FROM _nfe_b_ b');
    Qry.SQL.Add('LEFT JOIN _nfe_a_ a ON a.SeqArq=b.SeqArq AND a.SeqNFe=b.SeqNFe');
    Qry.SQL.Add('LEFT JOIN _nfe_c_ c ON c.SeqArq=b.SeqArq AND c.SeqNFe=b.SeqNFe');
    Qry.SQL.Add(Filtro1);
    Qry.SQL.Add(Filtro2);
    Qry.SQL.Add('ORDER BY b.ide_mod, b.ide_serie, b.ide_nNF');
    UnDmkDAC_PF.AbreQuery(Qry, DModG.MyPID_DB);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT c.CodInfoEmit, a.SeqArq, a.SeqNFe, a.versao, a.Id, ',
    'b.ide_cUF, b.ide_cNF, b.ide_natOp, b.ide_indPag, b.ide_mod, ',
    'b.ide_serie, b.ide_nNF, b.ide_dEmi, b.ide_dSaiEnt, ',
    'b.ide_hSaiEnt, b.ide_tpNF, b.ide_cMunFG, b.ide_tpImp, ',
    'b.ide_tpEmis, b.ide_cDV, b.ide_tpAmb, b.ide_finNFe, ',
    'b.ide_procEmi, b.ide_verProc, b.ide_dhCont, b.ide_xJust, ',
    'b.ide_hEmi, b.ide_dhEmiTZD, b.ide_dhSaiEntTZD, ',
    'b.ide_idDest, b.ide_indFinal, b.ide_indPres, ',
    '/*b.ide_dhContTZD,*/ ',
    'a.ErrEmit, a.ErrDest, a.ErrTrsp, a.ErrProd, ',
    'c.NomeEmit, emit_CNPJ, emit_CPF ',
    'FROM _nfe_b_ b ',
    'LEFT JOIN _nfe_a_ a ON a.SeqArq=b.SeqArq AND a.SeqNFe=b.SeqNFe ',
    'LEFT JOIN _nfe_c_ c ON c.SeqArq=b.SeqArq AND c.SeqNFe=b.SeqNFe ',
    Filtro1,
    Filtro2,
    'ORDER BY b.ide_mod, b.ide_serie, b.ide_nNF ',
    '']);
  end;
var
  tpAmbs: String;
begin
  if CkSoAmbiente1.Checked then
    tpAmbs := '1'
  else
    tpAmbs := '1,2';
  ReabreQuery(QrNFeB,
    'WHERE b.ide_tpAmb IN (' + tpAmbs + ')',
    'AND ErrEmit<1 AND ErrTrsp < 1 AND ErrDest<1 AND ErrProd<1');
  ReabreQuery(QrErrB,
    'WHERE b.ide_tpAmb IN (' + tpAmbs + ')',
    'AND (ErrEmit>0 OR ErrDest>0 OR ErrTrsp >0 OR ErrProd>0)');
  ReabreQuery(QrNotB,
  'WHERE NOT (b.ide_tpAmb IN (' + tpAmbs + '))', '');
  //
  QrCR01.Close;
  QrCR01.SQL.Clear;
  QrCR01.SQL.Add('SELECT');
  QrCR01.SQL.Add('retCanc_versao, infCanc_Id, infCanc_tpAmb,');
  QrCR01.SQL.Add('infCanc_xAmb, infCanc_verAplic, infCanc_cStat,');
  QrCR01.SQL.Add('infCanc_xMotivo, infCanc_cUF, infCanc_xUF,');
  QrCR01.SQL.Add('infCanc_chNFe, infCanc_dhRecbto, infCanc_nProt');
  QrCR01.SQL.Add('FROM _nfe_cr01_');
  QrCR01.SQL.Add('WHERE infCanc_tpAmb in (' + tpAmbs + ')');
  UnDmkDAC_PF.AbreQuery(QrCR01, DModG.MyPID_DB);
  //
  QrDR01.Close;
  QrDR01.SQL.Clear;
  QrDR01.SQL.Add('SELECT SeqArq, ');
  QrDR01.SQL.Add('retInutNFe_versao, retInutNFe_Id, retInutNFe_tpAmb,');
  QrDR01.SQL.Add('retInutNFe_xAmb, retInutNFe_verAplic, retInutNFe_cStat,');
  QrDR01.SQL.Add('retInutNFe_xMotivo, retInutNFe_cUF, retInutNFe_xUF,');
  QrDR01.SQL.Add('retInutNFe_ano, retInutNFe_CNPJ, retInutNFe_mod,');
  QrDR01.SQL.Add('retInutNFe_serie, retInutNFe_nNFIni, retInutNFe_nNFFin,');
  QrDR01.SQL.Add('retInutNFe_dhRecbto, retInutNFe_nProt');
  QrDR01.SQL.Add('FROM _nfe_dr01_');
  QrDR01.SQL.Add('WHERE retInutNFe_tpAmb in (' + tpAmbs + ')');
  UnDmkDAC_PF.AbreQuery(QrDR01, DModG.MyPID_DB);
  //
  BtInclui.Enabled := (QrErrB.RecordCount = 0)
  and (QrNFeB.RecordCount + QrCR01.RecordCount + QrDR01.RecordCount > 0);
end;

procedure TFmNFeLoad_Inn.AtualizaTodosGGXIguaisMul(cProd, xProd: String;
  CodInfoEmit: Integer);
const
  KlsGGXE = TKindLocSelGGXE.klsggxeLocMul;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(DModG);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT itsi.SeqArq, itsi.SeqNFe, itsI.nItem ',
    'FROM _nfe_i_ itsi ',
    'LEFT JOIN _nfe_c_ nfec ',
    '  ON nfec.SeqArq=itsi.SeqArq ',
    '  AND nfec.SeqNFe=itsi.SeqArq ',
    'WHERE prod_cProd="' + cProd + '"',
    'AND prod_cProd="' + xProd + '"',
    'AND nfec.CodInfoEmit=' + Geral.FF0(CodInfoEmit),
    'AND itsi.GraGruX=0 ',
    '']);
    while not Qry.Eof do
    begin
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_i_', False, [
      'KlsGGXE'], [
      'SeqArq', 'SeqNFe', 'nItem'], [
      Integer(KlsGGXE)], [
      Qry.FieldByName('SeqArq').AsInteger, Qry.FieldByName('SeqNFe').AsInteger,
      Qry.FieldByName('nItem').AsInteger], False);
      //
      Qry.Next;
    end;
    //
    UpdateInfoErros();
    AbreCarregados();
  finally
    Qry.Free;
  end;
end;

procedure TFmNFeLoad_Inn.AtualizaTodosGGXIguaisUni(cProd, xProd, NCM, uCom: String;
  CFOP, CodInfoEmit, GraGruX: Integer; NomeGGX: String);
const
  KlsGGXE = TKindLocSelGGXE.klsggxeLocUni;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(DModG);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT itsi.SeqArq, itsi.SeqNFe, itsI.nItem ',
    'FROM _nfe_i_ itsi ',
    'LEFT JOIN _nfe_c_ nfec ',
    '  ON nfec.SeqArq=itsi.SeqArq ',
    '  AND nfec.SeqNFe=itsi.SeqArq ',
    'WHERE prod_cProd="' + cProd + '"',
    'AND prod_xProd="' + xProd + '"',
    'AND prod_NCM="' + Geral.SoNumero_TT(NCM) + '"',
    'AND prod_CFOP=' + Geral.FF0(CFOP),
    'AND prod_uCom="' + uCom + '"',
    'AND nfec.CodInfoEmit=' + Geral.FF0(CodInfoEmit),
    'AND itsi.GraGruX=0 ',
    '']);
    while not Qry.Eof do
    begin
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_i_', False, [
      'KlsGGXE', 'GraGruX', 'NomeGGX'], [
      'SeqArq', 'SeqNFe', 'nItem'], [
      Integer(KlsGGXE), GraGruX, NomeGGX], [
      Qry.FieldByName('SeqArq').AsInteger, Qry.FieldByName('SeqNFe').AsInteger,
      Qry.FieldByName('nItem').AsInteger], False);
      //
      Qry.Next;
    end;
    //
    UpdateInfoErros();
    AbreCarregados();
  finally
    Qry.Free;
  end;
end;

{
procedure TFmNFeLoad_Inn.BtCarregarClick(Sender: TObject);
var
  I, J, K: Integer;
  DestName, Ele_Pai: String;
  Importa, Achou: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.LimpaGrade(GradeErrXML, 1, 1, True);
    if MyObjects.FormCria(TFmNFeLoad_Arq, FmNFeLoad_Arq) then
    try
      FmNFeLoad_Arq.FAvisaErros := False;
      //
      // Recria tabelas e reseta variaveis do form
      FmNFeLoad_Arq.ResetaVarsETabs();
      //
      PBx.Position := 0;
      PBx.Max := LBArqs.Items.Count;
      //
      for I := 0 to LBArqs.Items.Count - 1 do
      begin
        Importa := False;
        Achou   := False;
        //
        PBx.Position := PBx.Position + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
            ExtractFileName(LBArqs.Items[I]) + '... analisando');
        //
        XMLDocument1.Active := False;
        XMLDocument1.FileName := LBArqs.Items[I];
        try
          XMLDocument1.Active := True;
          Ele_Pai := XMLDocument1.DocumentElement.NodeName;
          //
          if LowerCase(Ele_Pai) = 'nfeproc' then
            Importa := Ck_nfeProc.Checked
          else
          if LowerCase(Ele_Pai) = 'nfe' then
            Importa := Ck_NFe.Checked
          else
          if LowerCase(Ele_Pai) = 'proccancnfe' then
            Importa := Ck_procCancNFe.Checked
          else
          if LowerCase(Ele_Pai) = 'procinutnfe' then
            Importa := Ck_procInutNFe.Checked
          else
          if (LowerCase(Ele_Pai) = 'cancnfe')
          or (LowerCase(Ele_Pai) = 'inutnfe')
          or (LowerCase(Ele_Pai) = 'retcancnfe')
          or (LowerCase(Ele_Pai) = 'retinutnfe') then
          begin
            for J := 0 to LBEleIgnor.Count - 1 do
            begin
              if LBEleIgnor.Items[J] = Ele_Pai then
              begin
                Achou := True;
                Break;
              end;
            end;
            if not Achou then
              LBEleIgnor.Items.Add(Ele_Pai);
          end
          else begin
            Importa := False;
            MeUnknowFile.Lines.Add(LBArqs.Items[I]);
            Achou := False;
            for J := 0 to LBEleDesc.Count - 1 do
            begin
              if LBEleDesc.Items[J] = Ele_Pai then
              begin
                Achou := True;
                Break;
              end;
            end;
            if not Achou then
              LBEleDesc.Items.Add(Ele_Pai);
          end;
          if Importa then
          begin
            MyObjects.Informa2(LaAviso1, LaAviso2, True,
              ExtractFileName(LBArqs.Items[I]) + '... carregando (elemento ' + Ele_Pai + ' )');
            //
            FmNFeLoad_Arq.FSeqArq := FmNFeLoad_Arq.FSeqArq + 1;
            FmNFeLoad_Arq.CarregaArquivoNoTempBD(XMLDocument1, LBArqs.Items[I]);
            //
            // Mover arquivo
            if FileExists(LBArqs.Items[I]) then
            begin
              DestName := dmkPF.CaminhoArquivo(ExtractFilePath(LBArqs.Items[I]), 'Lidos', '');
              ForceDirectories(DestName);
              dmkPF.MoveArq(PChar(LBArqs.Items[I]), PChar(DestName));
            end;
            // Fim mover arquivo
          end;
        except
          on E: Exception do
          begin
            K := GradeErrXML.RowCount - 1;
            if GradeErrXML.Cells[0, K] <> '' then
            begin
              GradeErrXML.RowCount := GradeErrXML.RowCount + 1;
              K := K + 1;
            end;
            GradeErrXML.Cells[0, K] := IntToStr(K);
            GradeErrXML.Cells[1, K] := LBArqs.Items[I];
            GradeErrXML.Cells[2, K] := E.Message;
          end;
          //raise;
          //Geral.MB_Erro(raise.?
        end;
      end;
      XMLDocument1.Active := False;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Carregamento finalizado!');
      UpdateInfoErros();
    finally
      FmNFeLoad_Arq.Destroy;
      //
      AbreCarregados();
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmNFeLoad_Inn.BtCarregarClick(Sender: TObject);
var
  I, J, K: Integer;
  DestName, Ele_Pai, NaoExistem: String;
  Importa, Achou: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    NaoExistem := '';
    MyObjects.LimpaGrade(GradeErrXML, 1, 1, True);
    if MyObjects.FormCria(TFmNFeLoad_Arq, FmNFeLoad_Arq) then
    try
      FmNFeLoad_Arq.FRecriaTbTmp := CkRecriaTbTmp.Checked;
      FmNFeLoad_Arq.FAvisaErros  := False;
      FmNFeLoad_Arq.FLaAviso1    := LaAviso1;
      FmNFeLoad_Arq.FLaAviso2    := LaAviso2;
      //
      // Recria tabelas e reseta variaveis do form
      FmNFeLoad_Arq.ResetaVarsETabs();
      //
      PBx.Position := 0;
      PBx.Max := LBArqs.Items.Count;
      //
      for I := 0 to LBArqs.Items.Count - 1 do
      begin
        //if LBArqs.Selected[I] then
        if LBArqs.Checked[I] then
        begin
          if FileExists(LBArqs.Items[I]) then
          begin
            Importa := False;
            Achou   := False;
            //
            PBx.Position := PBx.Position + 1;
            MyObjects.Informa2(LaAviso1, LaAviso2, True,
                ExtractFileName(LBArqs.Items[I]) + '... analisando');
            //
            XMLDocument1.Active := False;
            XMLDocument1.FileName := LBArqs.Items[I];
            try
              XMLDocument1.Active := True;
              Ele_Pai := XMLDocument1.DocumentElement.NodeName;
              //
              if LowerCase(Ele_Pai) = 'nfeproc' then
                Importa := Ck_nfeProc.Checked
              else
              if LowerCase(Ele_Pai) = 'nfe' then
                Importa := Ck_NFe.Checked
              else
              if LowerCase(Ele_Pai) = 'proccancnfe' then
                Importa := Ck_procCancNFe.Checked
              else
              if LowerCase(Ele_Pai) = 'procinutnfe' then
                Importa := Ck_procInutNFe.Checked
              else
              if (LowerCase(Ele_Pai) = 'cancnfe')
              or (LowerCase(Ele_Pai) = 'inutnfe')
              or (LowerCase(Ele_Pai) = 'retcancnfe')
              or (LowerCase(Ele_Pai) = 'retinutnfe') then
              begin
                for J := 0 to LBEleIgnor.Count - 1 do
                begin
                  if LBEleIgnor.Items[J] = Ele_Pai then
                  begin
                    Achou := True;
                    Break;
                  end;
                end;
                if not Achou then
                  LBEleIgnor.Items.Add(Ele_Pai);
              end
              else begin
                Importa := False;
                MeUnknowFile.Lines.Add(LBArqs.Items[I]);
                Achou := False;
                for J := 0 to LBEleDesc.Count - 1 do
                begin
                  if LBEleDesc.Items[J] = Ele_Pai then
                  begin
                    Achou := True;
                    Break;
                  end;
                end;
                if not Achou then
                  LBEleDesc.Items.Add(Ele_Pai);
              end;
              if Importa then
              begin
                MyObjects.Informa2(LaAviso1, LaAviso2, True,
                  ExtractFileName(LBArqs.Items[I]) + '... carregando (elemento ' + Ele_Pai + ' )');
                //
                FmNFeLoad_Arq.FSeqArq := FmNFeLoad_Arq.FSeqArq + 1;
                FmNFeLoad_Arq.CarregaArquivoNoTempBD(XMLDocument1, LBArqs.Items[I]);
                //
                // Mover arquivo
                if FileExists(LBArqs.Items[I]) then
                begin
                  DestName := dmkPF.CaminhoArquivo(ExtractFilePath(LBArqs.Items[I]), 'Lidos', '');
                  ForceDirectories(DestName);
                  dmkPF.MoveArq(PChar(LBArqs.Items[I]), PChar(DestName));
                end;
                // Fim mover arquivo
              end;
            except
              on E: Exception do
              begin
                Geral.MB_Erro(E.Message);
                K := GradeErrXML.RowCount - 1;
                if GradeErrXML.Cells[0, K] <> '' then
                begin
                  GradeErrXML.RowCount := GradeErrXML.RowCount + 1;
                  K := K + 1;
                end;
                GradeErrXML.Cells[0, K] := IntToStr(K);
                GradeErrXML.Cells[1, K] := LBArqs.Items[I];
                GradeErrXML.Cells[2, K] := E.Message;
              end;
              //raise;
              //Geral.MB_Erro(raise.?
            end;
          end else
            NaoExistem := NaoExistem + LBArqs.Items[I] + sLineBreak;
        end;
        XMLDocument1.Active := False;
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Carregamento finalizado!');
        UpdateInfoErros();
      end;
    finally
      FmNFeLoad_Arq.Destroy;
      //
      AbreCarregados();
    end;
    if PageControl1.ActivePageIndex = 0 then
    begin
      if QrNFeB.RecordCount > 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        PageControl2.ActivePageIndex := 0;
        PageControl3.ActivePageIndex := 0;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if NaoExistem <> '' then
    Geral.MB_Aviso('Arquivos n�o localizados:' + sLineBreak + NaoExistem);
end;

procedure TFmNFeLoad_Inn.BtDestClick(Sender: TObject);
begin
  FSeqArq := QrErrBSeqArq.Value;
  FSeqNFe := QrErrBSeqArq.Value;
  //
  if MyObjects.FormCria(TFmNFeLoad_Arq, FmNFeLoad_Arq) then
  begin
    FmNFeLoad_Arq.PnEdita.Visible := True;
    FmNFeLoad_Arq.FLaAviso1   := LaAviso1;
    FmNFeLoad_Arq.FLaAviso2   := LaAviso2;
    //
    FmNFeLoad_Arq.QrArqs.SQL.Clear;
    FmNFeLoad_Arq.QrArqs.SQL.Add('SELECT * FROM _nfe_arqs_');
    FmNFeLoad_Arq.QrArqs.SQL.Add('WHERE SeqArq=' + FormatFloat('0', QrErrBSeqArq.Value));
    UnDmkDAC_PF.AbreQuery(FmNFeLoad_Arq.QrArqs, DModG.MyPID_DB);
    //
    FmNFeLoad_Arq.QrNFeA.SQL.Clear;
    FmNFeLoad_Arq.QrNFeA.SQL.Add('SELECT * FROM _nfe_a_');
    FmNFeLoad_Arq.QrNFeA.SQL.Add('WHERE SeqNFe=' + FormatFloat('0', QrErrBSeqNfe.Value));
    UnDmkDAC_PF.AbreQuery(FmNFeLoad_Arq.QrNFeA, DModG.MyPID_DB);
    //
    FmNFeLoad_Arq.FPermiteAlterarCodInfoDest := True;
    FmNFeLoad_Arq.ShowModal;
    //
    UpdateInfoErros();
    AbreCarregados();
    QrErrB.Locate('SeqArq;SeqNFe', VarArrayOf([FseqArq, FSeqNFe]), []);
    //
    FmNFeLoad_Arq.Destroy;
    //
  end;
end;

procedure TFmNFeLoad_Inn.BtEmitClick(Sender: TObject);
var
  SeqArq, SeqNFe, CodInfoEmit, ErrEmit: Integer;
  NomeEmit: String;
begin
  CodInfoEmit := QrErrBCodInfoEmit.Value;
  VAR_CNPJ_A_CADASTRAR := QrErrBemit_CNPJ.Value;
  DmodG.CadastroDeEntidade(CodInfoEmit, fmcadEntidade2, fmcadEntidade2);
  DModG.ObtemEntidadeDeCNPJCFP(QrErrBemit_CNPJ.Value, CodInfoEmit);
  if CodInfoEmit <> 0 then
  begin
    SeqArq := QrErrBSeqArq.Value;
    SeqNFe := QrErrBSeqNFe.Value;
    DModG.ObtemEntidadeNomeDeCodigo(CodInfoEmit, NomeEmit);
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_c_', False, [
    'CodInfoEmit', 'NomeEmit'], [
    'SeqArq', 'SeqNFe'], [
    CodInfoEmit, NomeEmit], [
    SeqArq, SeqNFe], False) then
    begin
      ErrEmit := 0;
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_a_', False, [
      'ErrEmit'], [
      'SeqArq', 'SeqNFe'], [
      ErrEmit], [
      SeqArq, SeqNFe], False) then
      begin
        UnDmkDAC_PF.AbreQuery(QrErrB, DModG.MyPID_DB);
        QrErrB.Locate('SeqArq;SeqNFe', VarArrayOf([SeqArq, SeqNFe]), [])
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Inn.BtIncluiClick(Sender: TObject);
var
  FatID, FatNum, Empresa, (*CliInt,*) IncFatNum: Integer;
  Prox: String;
  // Cancelamento
  Cancela(*, Achou*): Boolean;
  // Inutiliza��o
  Codigo, CodUsu, Aplicacao, Justif: Integer;
  Nome, DataC, xJust, Id: String;
  XML_Inu: WideString;
  DataEntrada, DataFiscal: TDateTime;
  SQLType: TSQLType;
  //
  eveMDe_tpAmb, eveMDe_cOrgao, eveMDe_cStat, eveMDe_tpEvento,
  eveMDe_nSeqEvento, cSitNFe, cSitConf: Integer;
  eveMDe_dhRegEvento: TDateTime;
  eveMDe_TZD_UTC: Double;
  eveMDe_Id, eveMDe_verAplic, eveMDe_xMotivo, eveMDe_chNFe, eveMDe_xEvento,
  eveMDe_CNPJDest, eveMDe_CPFDest, eveMDe_emailDest, eveMDe_nProt: WideString;
begin
  FatID := 0;
  // Fazer inclus�o!
  if Geral.MB_Pergunta(
  'Caso existam NFs com a mesma serie / n�mero para o emitente,' + sLineBreak +
  'os dados antigos ser�o exclu�dos. Deseja continuar assim mesmo?') = ID_YES then
  begin
    if Geral.MB_Pergunta(
    'Confirma os seguintes dados abaixo?' + sLineBreak +
    'FatID = ' + EdFatID.Text + sLineBreak + 'Empresa = ' + EdEmpresa.Text) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      PBx.Position := 0;
      PBx.Max := QrNFeB.RecordCount + QrCR01.RecordCount;

      // verifica FatNum
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT MAX(FatNum) FatNum');
      Dmod.QrAux.SQL.Add('FROM nfecaba');
      Dmod.QrAux.SQL.Add('WHERE FatID=' + FormatFloat('0', EdFatID.ValueVariant));
      Dmod.QrAux.SQL.Add('AND Empresa=' + FormatFloat('0', EdEmpresa.ValueVariant));
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      //
      IncFatNum := Dmod.QrAux.FieldByName('FatNum').AsInteger;
      Prox := FormatFloat('0', IncFatNum);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE Controle SET FatPedCab=' + Prox);
      Dmod.QrUpd.SQL.Add('WHERE FatPedCab<' + Prox);
      Dmod.QrUpd.ExecSQL;
      //
      try
        QrNFeB.First;
        while not QrNFeB.Eof do
        begin
          PBx.Position := PBx.Position + 1;
          MyObjects.Informa2(LaAviso1, LaAviso2, True, QrNFeBId.Value + ' -  Incluindo');
          //
          QrNFeB12a.Close;
          QrNFeB12a.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeB12a.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeB12a, DModG.MyPID_DB);
          //
          QrNFeC.Close;
          QrNFeC.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeC.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeC, DModG.MyPID_DB);
          //
(*
          QrNFeE.Close;
          QrNFeE.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeE.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeE, DModG.MyPID_DB);
*)
          UnDmkDAC_PF.AbreMySQLQuery0(QrNFeE, DModG.MyPID_DB, [
          'SELECT   ',
          'dest_CNPJ, dest_CPF, dest_xNome,  ',
          'dest_xLgr, dest_nro, dest_xCpl,  ',
          'dest_xBairro, dest_cMun, dest_xMun,  ',
          'dest_UF, dest_CEP, dest_cPais,  ',
          'dest_xPais, dest_fone, dest_IE,  ',
          'dest_ISUF, dest_email, dest_IM, ',
          'dest_indIEDest, CodInfoDest ',
          'FROM _nfe_e_ ',
          'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
          'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
          '']);
            //
          QrNFeF.Close;
          QrNFeF.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeF.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeF, DModG.MyPID_DB);
          //

          QrNFeG.Close;
          QrNFeG.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeG.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeG, DModG.MyPID_DB);
          //
          QrNFeGA.Close;
          QrNFeGA.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeGA.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeGA, DModG.MyPID_DB);
          //
(*
          QrNFeW02.Close;
          QrNFeW02.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeW02.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeW02, DModG.MyPID_DB);
*)
          UnDmkDAC_PF.AbreMySQLQuery0(QrNFeW02, DModG.MyPID_DB, [
          'SELECT  ',
          'ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ',
          'ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete, ',
          'ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ',
          'ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS, ',
          'ICMSTot_vOutro, ICMSTot_vNF, ICMSTot_vICMSDeson, ',
          'ICMSTot_vFCPUFDest, ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet ',
          'FROM _nfe_w_02',
          'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
          'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
          '']);
          //
          QrNFeW17.Close;
          QrNFeW17.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeW17.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeW17, DModG.MyPID_DB);
          //
          QrNFeW23.Close;
          QrNFeW23.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeW23.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeW23, DModG.MyPID_DB);
          //
          QrNFeX01.Close;
          QrNFeX01.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeX01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeX01, DModG.MyPID_DB);
          //
          QrNFeX22.Close;
          QrNFeX22.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeX22.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeX22, DModG.MyPID_DB);
          //
          QrNFeX26.Close;
          QrNFeX26.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeX26.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeX26, DModG.MyPID_DB);
          //
          QrNFeY01.Close;
          QrNFeY01.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeY01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeY01, DModG.MyPID_DB);
          //
          QrNFeY07.Close;
          QrNFeY07.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeY07.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeY07, DModG.MyPID_DB);
          //
          QrNFeZ01.Close;
          QrNFeZ01.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeZ01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeZ01, DModG.MyPID_DB);
          //
          QrNFeZ04.Close;
          QrNFeZ04.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeZ04.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeZ04, DModG.MyPID_DB);
          //
          QrNFeZ07.Close;
          QrNFeZ07.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeZ07.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeZ07, DModG.MyPID_DB);
          //
          QrNFeZ10.Close;
          QrNFeZ10.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeZ10.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeZ10, DModG.MyPID_DB);
          //
          QrNFeZA01.Close;
          QrNFeZA01.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeZA01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeZA01, DModG.MyPID_DB);
          //
          QrNFeZB01.Close;
          QrNFeZB01.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrNFeZB01.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeZB01, DModG.MyPID_DB);
          //
          QrPR01.Close;
          QrPR01.Params[00].AsInteger := QrNFeBSeqArq.Value;
          UnDmkDAC_PF.AbreQuery(QrPR01, DModG.MyPID_DB);
          //
(*
          QrItsI.Close;
          QrItsI.Params[00].AsInteger := QrNFeBSeqArq.Value;
          QrItsI.Params[01].AsInteger := QrNFeBSeqNFe.Value;
          UnDmkDAC_PF.AbreQuery(QrItsI, DModG.MyPID_DB);
*)
          UnDmkDAC_PF.AbreMySQLQuery0(QrItsI, DModG.MyPID_DB, [
          'SELECT ',
          '',
          'nItem, ',
          'prod_cProd, prod_cEAN, prod_xProd, ',
          'prod_NCM, prod_EXTIPI, prod_CFOP, ',
          'prod_uCom, prod_qCom, prod_vUnCom, ',
          'prod_vProd, prod_cEANTrib, prod_uTrib, ',
          'prod_qTrib, prod_vUnTrib, prod_vFrete, ',
          'prod_vSeg, prod_vDesc, prod_vOutro, ',
          'prod_indTot, prod_xPed, prod_nItemPed, ',
          'prod_nFCI, prod_CEST, ',
          'GraGruX, Nivel1',
          '',
          'FROM _nfe_i_',
          '',
          'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
          'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
          '']);
          //
          FatID   := EdFatID.ValueVariant;
          FatNum  := 0;
          Empresa := EdEmpresa.ValueVariant;
          //
          DataEntrada := 0;
          DataFiscal  := 0;
          if ExcluiNotaAntiga(FatNum, DataFiscal) then
            //Achou := True
          else begin
            //Achou := False;
            IncFatNum  := IncFatNum + 1;
            FatNum     := incFatNum;
          end;
          if DataFiscal < 2 then
          begin
            //DataFiscal := Int(DmodG.ObtemAgora());
            //
            if Geral.SoNumero_TT(QrNFeCemit_CNPJ.Value) =
            Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value) then
              DataFiscal := QrNFeBide_dEmi.Value
            else
              DataFiscal := QrNFeBide_dEmi.Value + 2;
          end;
            if not UnNFe_PF.ObtemDataEntradaEFiscal(QrNFeBide_serie.Value,
              QrNFeBide_nNF.VAlue, QrNFeCCodInfoEmit.Value,
              QrNFeBId.Value, QrNFeCemit_xNome.Value, DataFiscal, DataEntrada,
              DataFiscal) then
              begin
                 Close;
                 Exit;
              end;
           //
          if IncluiNFeCabA(FatID, FatNum, Empresa, DataEntrada, DataFiscal) then
          if IncluiNFeCabB(FatID, FatNum, Empresa) then
          if IncluiNFeCabF(FatID, FatNum, Empresa) then
          if IncluiNFeCabG(FatID, FatNum, Empresa) then
          if IncluiNFeCabGA(FatID, FatNum, Empresa) then
          if IncluiNFeCabXReb(FatID, FatNum, Empresa) then
          if IncluiNFeCabXVol(FatID, FatNum, Empresa) then
          if IncluiNFeCabY(FatID, FatNum, Empresa) then
          if IncluiNFeCabZCon(FatID, FatNum, Empresa) then
          if IncluiNFeCabZFis(FatID, FatNum, Empresa) then
          if IncluiNFeCabZPro(FatID, FatNum, Empresa) then
          if IncluiNFeItsI(FatID, FatNum, Empresa) then
          //if Achou then
          begin
            if IncluiStqMovItsA(FatID, FatNum, Empresa, DataFiscal) then
            ;
          end;
          //
////////////////////////////////////////////////////////////////////////////////
// Atualizar aqui a MDe - Manifesta��o do Destinat�rio
{
          Id := QrNFeBId.Value;
          UnDmkDAC_PF.AbreMySQLQuery0(QrFatID_0052, Dmod.MyDB, [
          'SELECT FatID, FatNum, Empresa, IDCtrl, ',
          'eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic, ',
          'eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo, ',
          'eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento, ',
          'eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest, ',
          'eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC, ',
          'eveMDe_nProt, cSitNFe, cSitConf ',
          'FROM nfecaba ',
          'WHERE FatID=' + Geral.FF0(VAR_FatID_0052),
          'AND Id="' + Id + '" ',
          '']);
          if QrFatID_0052.RecordCount = 1 then
          begin
            eveMDe_Id             := QrFatID_0052eveMDe_Id         .Value;
            eveMDe_tpAmb          := QrFatID_0052eveMDe_tpAmb      .Value;
            eveMDe_verAplic       := QrFatID_0052eveMDe_verAplic   .Value;
            eveMDe_cOrgao         := QrFatID_0052eveMDe_cOrgao     .Value;
            eveMDe_cStat          := QrFatID_0052eveMDe_cStat      .Value;
            eveMDe_xMotivo        := QrFatID_0052eveMDe_xMotivo    .Value;
            eveMDe_chNFe          := QrFatID_0052eveMDe_chNFe      .Value;
            eveMDe_tpEvento       := QrFatID_0052eveMDe_tpEvento   .Value;
            eveMDe_xEvento        := QrFatID_0052eveMDe_xEvento    .Value;
            eveMDe_nSeqEvento     := QrFatID_0052eveMDe_nSeqEvento .Value;
            eveMDe_CNPJDest       := QrFatID_0052eveMDe_CNPJDest   .Value;
            eveMDe_CPFDest        := QrFatID_0052eveMDe_CPFDest    .Value;
            eveMDe_emailDest      := QrFatID_0052eveMDe_emailDest  .Value;
            eveMDe_dhRegEvento    := QrFatID_0052eveMDe_dhRegEvento.Value;
            eveMDe_TZD_UTC        := QrFatID_0052eveMDe_TZD_UTC    .Value;
            eveMDe_nProt          := QrFatID_0052eveMDe_nProt      .Value;
            cSitNFe               := QrFatID_0052cSitNFe           .Value;
            cSitConf              := QrFatID_0052cSitConf          .Value;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
            'eveMDe_Id', 'eveMDe_tpAmb', 'eveMDe_verAplic',
            'eveMDe_cOrgao', 'eveMDe_cStat', 'eveMDe_xMotivo',
            'eveMDe_chNFe', 'eveMDe_tpEvento', 'eveMDe_xEvento',
            'eveMDe_nSeqEvento', 'eveMDe_CNPJDest', 'eveMDe_CPFDest',
            'eveMDe_emailDest', 'eveMDe_dhRegEvento', 'eveMDe_TZD_UTC',
            'eveMDe_nProt', 'cSitNFe', 'cSitConf'
            ], ['FatID', 'FatNum', 'Empresa'], [
            eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic,
            eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo,
            eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento,
            eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest,
            eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC,
            eveMDe_nProt, cSitNFe, cSitConf
            ], [FatID, FatNum, Empresa], True) then
            begin
              DmNFe_0000.ExcluiNfe(0, QrFatID_0052FatID.Value,
              QrFatID_0052FatNum.Value, QrFatID_0052Empresa.Value, True);
            end;
          end;
}
////////////////////////////////////////////////////////////////////////////////
          QrNFeB.Next;
        end;


        // Cancelar NF-es

        QrCR01.First;
        while not QrCR01.Eof do
        begin
          PBx.Position := PBx.Position + 1;
          MyObjects.Informa2(LaAviso1, LaAviso2, True, QrCR01infCanc_chNFe.Value + ' -  Cancelando');
          //
          if QrCR01infCanc_chNFe.Value <> '' then
          begin
            QrLocID.Close;
            QrLocID.Params[0].AsString := QrCR01infCanc_chNFe.Value;
            UnDmkDAC_PF.AbreQuery(QrLocID, DModG.MyPID_DB);
            if QrLocID.RecordCount = 0 then
              Geral.MB_Aviso(
              'N�o foi localizada a NF-e abaixo para ser cancelada:' + sLineBreak +
              'Chave da NF-e: ' + QrCR01infCanc_chNFe.Value)
            else begin
              if QrLocID.RecordCount > 1 then
                Cancela := Geral.MB_Pergunta(
                'Foram localizados ' + IntToStr(QrLocID.RecordCount) +
                'registros para a NF-e abaixo para ser cancelada:' + sLineBreak +
                'Chave da NF-e: ' + QrCR01infCanc_chNFe.Value) = ID_YES
              else Cancela := True;
              //
              if Cancela then
              begin
                QrLocID.First;
                while not QrLocID.Eof do
                begin
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                  'retCancNFe_versao',
                  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'infCanc_dhRecbto', 'infCanc_nProt', (*'infCanc_digVal',*)
                  'infCanc_cStat', 'infCanc_xMotivo'], [
                  'FatID', 'FatNum', 'Empresa'], [
                  QrCR01retCanc_versao.Value,
                  QrCR01infCanc_Id.Value, QrCR01infCanc_tpAmb.Value, QrCR01infCanc_verAplic.Value,
                  QrCR01infCanc_dhRecbto.Value, QrCR01infCanc_nProt.Value, (*infCanc_digVal,*)
                  QrCR01infCanc_cStat.Value, QrCR01infCanc_xMotivo.Value], [
                  FatID, FatNum, Empresa], True) then ;
                  //
                  QrLocID.Next;
                end;
              end;
            end;
          end;
          QrCR01.Next;
        end;
        //



        // Inutilizar numera��es de NF-es

        QrDR01.First;
        while not QrDR01.Eof do
        begin
          PBx.Position := PBx.Position + 1;
          QrNFeInut.Close;
          QrNFeInut.Params[00].AsInteger :=  QrDR01retInutNFe_serie.Value;
          QrNFeInut.Params[01].AsInteger :=  QrDR01retInutNFe_nNFIni.Value;
          QrNFeInut.Params[02].AsInteger :=  QrDR01retInutNFe_nNFFin.Value;
          UnDmkDAC_PF.AbreQuery(QrNFeInut, DModG.MyPID_DB);
          if QrNFeInut.RecordCount = 0 then
          begin
            MyObjects.Informa2(LaAviso1, LaAviso2, True, IntToStr(QrDR01retInutNFe_nNFFin.Value) + ' -  Inutilizando');
            //
            if QrDR01retInutNFe_cStat.Value = 102 then
            begin
              Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinut', '', 0);
              CodUsu  := Codigo; // Parei Aqui! N�o pode haver inclus�es anteriores!
              Nome    := 'Importado da base da dados anterior';
              DModG.ObtemEntidadeDeCNPJCFP(QrDR01retInutNFe_CNPJ.Value, Empresa);
              DataC   := Geral.FDT(QrDR01retInutNFe_dhRecbto.Value, 1);
              xJust   := ''; // ??
              //
              QrInut.Close;
              QrInut.Params[00].AsInteger :=  QrDR01retInutNFe_serie.Value;
              QrInut.Params[01].AsInteger :=  QrDR01retInutNFe_nNFIni.Value;
              QrInut.Params[02].AsInteger :=  QrDR01retInutNFe_nNFFin.Value;
              UnDmkDAC_PF.AbreQuery(QrInut, DModG.MyPID_DB);
              //
              if QrInut.RecordCount > 0 then
              begin
                QrJust.Close;
                QrJust.Params[0].AsString := QrInutinutNFe_xJust.Value;
                UnDmkDAC_PF.AbreQuery(QrJust, Dmod.MyDB);
                //
                if QrJust.RecordCount > 0 then
                  Justif := QrJustCodigo.Value
                else
                begin
                  Aplicacao := 2; // Inutiliza��o
                  //Justif := UMyMod.BuscaEmLivreY_Def('nfejust', 'Codigo', stIns, 0);
                  Justif := DModG.BuscaProximoCodigoInt('nfectrl', 'nfejust', '', 0);
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfejust', False, [
                  'CodUsu', 'Nome', 'Aplicacao'], [
                  'Codigo'], [
                  Justif, QrInutinutNFe_xJust.Value, Aplicacao], [
                  Justif], True);
                end;
              end else
              begin
                Justif := 0;
              end;

              QrNFeArqs.Close;
              QrNFeArqs.Params[0].AsInteger := QrDR01SeqArq.Value;
              UnDmkDAC_PF.AbreQuery(QrNFeArqs, DModG.MyPID_DB);
              //
              XML_Inu := Geral.WideStringToSQLString(
                ObtemXML_De_Arquivo(QrNFeArqsArqDir.Value + '/' + QrNFeArqsArqName.Value));
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinut', False, [
              'CodUsu', 'Nome', 'Empresa',
              'DataC', 'versao', 'Id',
              'tpAmb', 'cUF', 'ano',
              'CNPJ', 'modelo', 'Serie',
              'nNFIni', 'nNFFim', 'Justif',
              'xJust', 'cStat', 'xMotivo',
              'dhRecbto', 'nProt', 'XML_Inu'], [
              'Codigo'], [
              CodUsu, Nome, Empresa,
              DataC, QrDR01retInutNFe_versao.Value, QrDR01retInutNFe_Id.Value,
              QrDR01retInutNFe_tpAmb.Value, QrDR01retInutNFe_cUF.Value,
              QrDR01retInutNFe_ano.Value,
              QrDR01retInutNFe_CNPJ.Value, QrDR01retInutNFe_mod.Value, QrDR01retInutNFe_Serie.Value,
              QrDR01retInutNFe_nNFIni.Value, QrDR01retInutNFe_nNFFin.Value, Justif,
              xJust, QrDR01retInutNFe_cStat.Value, QrDR01retInutNFe_xMotivo.Value,
              QrDR01retInutNFe_dhRecbto.Value, QrDR01retInutNFe_nProt.Value, XML_Inu], [
              Codigo], True) then ;
            end;
          end;
          QrDR01.Next;
        end;

        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Inclus�o finalizada!');
        Geral.MB_Aviso('Inclus�o finalizada!');
        Close;
        //
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Inn.BtNenhumClick(Sender: TObject);
begin
  MarcaTodosLB(False);
end;

procedure TFmNFeLoad_Inn.BtProdClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProduto, BtProd);
end;

procedure TFmNFeLoad_Inn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_Inn.BtTodosClick(Sender: TObject);
begin
  MarcaTodosLB(True);
end;

procedure TFmNFeLoad_Inn.Cadastrodemateriaprima1Click(Sender: TObject);
begin
  AppPF.CadastroMateriaPrima(QrErrIGraGruX.Value, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.Cadastrodeprodutoacabado1Click(Sender: TObject);
begin
  AppPF.CadastroProdutoAcabado(QrErrIGraGruX.Value, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.Cadastrodeprodutodiretonagrade1Click(Sender: TObject);
const
  GraGruX = 0;
begin
  Grade_Jan.MostraFormGraGruN_GGX(GraGruX, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.CadastrodeservioNFe1Click(Sender: TObject);
begin
  AppPF.CadastroServicoNFe(QrErrIGraGruX.Value, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.Cadastrodesubproduto1Click(Sender: TObject);
begin
  AppPF.CadastroSubProduto(QrErrIGraGruX.Value, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.CadastrodeUsoeconsumo1Click(Sender: TObject);
begin
  AppPF.CadastroInsumo(QrErrIGraGruX.Value, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.CadastrodeUsopermanente1Click(Sender: TObject);
begin
  AppPF.CadastroUsoPermanente(QrErrIGraGruX.Value, QrErrIprod_xProd.Value);
end;

procedure TFmNFeLoad_Inn.CkSoAmbiente1Click(Sender: TObject);
begin
  if QrNfeB.State <> dsInactive then
    AbreCarregados();
end;

procedure TFmNFeLoad_Inn.CodificaoporFornecedor1Click(Sender: TObject);
var
  NomeGGX: String;
  GraGruX: Integer;
begin
  if TKindLocSelGGXE(QrErrIKlsGGXE.Value) = TKindLocSelGGXE.klsggxeLocMul then
    if Geral.MB_Pergunta('Produto definido como gen�rico!' +
    sLineBreak + 'Deseja selecionar um reduzido avulso?') = ID_YES then
    begin
      Definiratrelamentoavulsocomreduzido1Click(Self);
      Exit;
    end;
  if VerificaGGE() = False then
  begin
    if Grade_Jan.MostraFormGraGruEIts(stIns, QrErrBCodInfoEmit.Value,
    (*, PrdGrupTip*) QrErrIprod_cProd.Value, QrErrIprod_xProd.Value,
    QrErrIprod_cEAN.Value, QrErrIprod_NCM.Value,
    QrErrIprod_uCom.Value, QrErrIInfAdProd.Value,
    QrErrIprod_CFOP.Value, QrErrIICMS_Orig.Value,
    QrErrIICMS_CST.Value, QrErrIIPI_CST.Value, QrErrIPIS_CST.Value,
    QrErrICOFINS_CST.Value, QrErrIprod_EXTIPI.Value, QrErrIIPI_cEnq.Value,
    GraGruX, NomeGGX) then
      AtualizaTodosGGXIguaisUni(QrErrIprod_cProd.Value, QrErrIprod_xProd.Value,
      QrErrIprod_NCM.Value, QrErrIprod_uCom.Value, QrErrIprod_CFOP.Value,
        QrErrBCodInfoEmit.Value, GraGruX, NomeGGX);
  end;
end;

procedure TFmNFeLoad_Inn.CriarIDdecodificaomltipla1Click(Sender: TObject);
begin
  if Grade_Jan.MostraFormGraGruECad(stIns, nil, QrErrBCodInfoEmit.Value,
  QrErrIprod_cProd.Value, QrErrIprod_xProd.Value) then
    AtualizaTodosGGXIguaisMul(QrErrIprod_cProd.Value, QrErrIprod_xProd.Value,
      QrErrBCodInfoEmit.Value);
end;

procedure TFmNFeLoad_Inn.Definiratrelamentoavulsocomreduzido1Click(
  Sender: TObject);
  function SelecionaReduzido(var Cod: Integer; var Nom: String): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Reduzido';
    Prompt = 'Selecione o reduzido';
    Campo  = 'Descricao';
  var
    Terceiro, Controle: Integer;
    Resp: Variant;
  begin
    Result := False;
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
      'SELECT ggx.Controle Codigo, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'Descricao',
      'FROM gragrux ggx',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
(*
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
*)
      'ORDER BY descricao, Codigo',
      ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      Result := True;
      Cod := Resp;
      Nom := VAR_SELNOM;
    end;
  end;
var
  GragruX: Integer;
  NomeGGX: String;
begin
  GragruX := 0;
  NomeGGX := '';
  if SelecionaReduzido(GraGruX, NomeGGX) then
  begin
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_i_', False, [
    'GraGruX', 'NomeGGX'], [
    'SeqArq', 'SeqNFe', 'nItem'], [
    GraGruX, NomeGGX], [
    QrErrBSeqArq.Value, QrErrBSeqNFe.Value, QrErrInItem.Value], False) then
    begin
      UpdateInfoErros();
      AbreCarregados();
    end;
  end;
end;

function TFmNFeLoad_Inn.ExcluiNotaAntiga(var FatNum: Integer; var DataFiscal: TDateTime): Boolean;
const
  DeleteDI = True;
  DeleteGA = False;
  Status = 0;
{
var
  FatID, Empresa: Integer;
}
begin
  Result     := False;
  FatNum     := 0;
  DataFiscal := 0;
  QrLocAnt.Close;
  QrLocAnt.Params[00].AsString  := QrNFeCemit_CNPJ.Value;
  QrLocAnt.Params[01].AsInteger := QrNFeBide_mod.Value;
  QrLocAnt.Params[02].AsInteger := QrNFeBide_nNF.Value;
  QrLocAnt.Params[03].AsString  := Geral.FDT(QrNFeBide_dEmi.Value, 1);
  //QrLocAnt.Params[04].AsInteger := QrNFeBide_serie.Value;
  UnDmkDAC_PF.AbreQuery(QrLocAnt, Dmod.MyDB);
  if QrLocAnt.RecordCount > 1 then
    Geral.MB_Info('Foram econtrados ' + IntToStr(QrLocAnt.RecordCount) +
    ' registros para a NF:' + sLineBreak +
    'CNPJ do emitente: ' + QrNFeCemit_CNPJ.Value + sLineBreak +
    'Modelo da NF: ' + IntToStr(QrNFeBide_mod.Value) + sLineBreak +
    'S�rie da NF: ' + IntToStr(QrNFeBide_serie.Value) + sLineBreak +
    'N�mero da NF: ' + IntToStr(QrNFeBide_nNF.Value) + sLineBreak +
    'Data de emiss�o: ' + Geral.FDT(QrNFeBide_dEmi.Value, 2) + sLineBreak + sLineBreak +
    'Por seguran�a, nenhuma ser� exclu�da!')
  else
  if QrLocAnt.RecordCount = 1 then
  begin
    FatNum     := QrLocAntFatNum.Value;
    DataFiscal := QrLocAntDataFiscal.Value;
    DmNFe_0000.ExcluiNfe(Status, QrLocAntFatID.Value, QrLocAntFatNum.Value,
    QrLocAntEmpresa.Value, DeleteDI, DeleteGA);
    Result := True;
  end;
end;

procedure TFmNFeLoad_Inn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_Inn.FormCreate(Sender: TObject);
var
  Dir: String;
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  QrNFeB.Database := DModG.MyPID_DB;
  QrNFeI.Database := DModG.MyPID_DB;
  QrNFeB12a.Database := DModG.MyPID_DB;
  QrErrB.Database := DModG.MyPID_DB;
  QrErrI.Database := DModG.MyPID_DB;
  QrNotB.Database := DModG.MyPID_DB;
  QrNFeC.Database := DModG.MyPID_DB;
  QrNFeE.Database := DModG.MyPID_DB;
  QrNFeF.Database := DModG.MyPID_DB;
  QrNFeG.Database := DModG.MyPID_DB;
  QrNFeGA.Database := DModG.MyPID_DB;
  QrNFeW02.Database := DModG.MyPID_DB;
  QrNFeW17.Database := DModG.MyPID_DB;
  QrNFeW23.Database := DModG.MyPID_DB;
  QrNFeX01.Database := DModG.MyPID_DB;
  QrNFeX22.Database := DModG.MyPID_DB;
  QrNFeX26.Database := DModG.MyPID_DB;
  QrNFeX33.Database := DModG.MyPID_DB;
  QrNFeY01.Database := DModG.MyPID_DB;
  QrNFeY07.Database := DModG.MyPID_DB;
  QrNFeZ01.Database := DModG.MyPID_DB;
  QrNFeZ04.Database := DModG.MyPID_DB;
  QrNFeZ07.Database := DModG.MyPID_DB;
  QrNFeZ10.Database := DModG.MyPID_DB;
  QrNFeZA01.Database := DModG.MyPID_DB;
  QrNFeZB01.Database := DModG.MyPID_DB;

  QrItsI.Database := DModG.MyPID_DB;
  QrItsIDi.Database := DModG.MyPID_DB;
  QrItsIDiA.Database := DModG.MyPID_DB;
  QrItsN.Database := DModG.MyPID_DB;
  QrItsM.DataBase := DModG.MyPID_DB;
  QrItsO.Database := DModG.MyPID_DB;
  QrItsP.Database := DModG.MyPID_DB;
  QrItsQ.Database := DModG.MyPID_DB;
  QrItsR.Database := DModG.MyPID_DB;
  QrItsS.Database := DModG.MyPID_DB;
  QrItsT.Database := DModG.MyPID_DB;
  QrItsU.Database := DModG.MyPID_DB;
  QrItsV.Database := DModG.MyPID_DB;

  QrPR01.Database := DModG.MyPID_DB;
  QrCR01.Database := DModG.MyPID_DB;
  QrDR01.Database := DModG.MyPID_DB;
  //
  QrJust.DataBase    := Dmod.MyDB;
  QrNFeArqs.DataBase := DModG.MyPID_DB;
  QrInut.DataBase    := DModG.MyPID_DB;
  //QrLocIXE.DataBase  := DModG.MyPID_DB;
  //
  PageControl1.ActivePageIndex := 0;
  GradeErrXML.Cells[0,0] := 'Seq';
  GradeErrXML.Cells[1,0] := 'Arquivo';
  GradeErrXML.Cells[2,0] := 'Mensagem de erro';
  //
  DmNFe_0000.ObtemDirXML(NFE_EXT_RET_DOW_NFE_NFE_XML,  Dir, False);
  //
  Decodedate(DModG.ObtemAgora(), Ano, Mes, Dia);
  if Mes = 1 then
  begin
    Mes := 12;
    Ano := Ano -1 ;
  end else
    Mes := Mes - 1;
  Dir := Dir + Geral.FF0(Ano) + ' ' + Geral.FFN(Mes, 2);
  EdDiretorio.Text := Dir;
end;

procedure TFmNFeLoad_Inn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFeLoad_Inn.ImportaNFes();
var
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    MeUnknowFile.Lines.Clear;
    LBEleDesc.Clear;
    LBArqs.Clear;
    //MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      MyObjects.GetAllFiles(CkSub1.Checked, EdDiretorio.Text+'\'+
      MeExtensoes.Lines[I], LBArqs, False, LaAviso1, LaAviso2);
    end;
    BtCarregar.Enabled := LBArqs.Items.Count > 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFeLoad_Inn.IncluiNFeCabA(const FatID: Integer; var FatNum: Integer;
const Empresa: Integer; const DtaEnt, DtaFis: TDateTime): Boolean;
const
  LoteEnv = 0;
  infCanc_digVal = '';
  infCanc_cJust = 0;
  infCanc_xJust = '';
var
  IDCtrl, Status: Integer;
  DataFiscal, ide_dSaiEnt: String;
begin
  ide_dSaiEnt := Geral.FDT(DtaEnt, 1);
  DataFiscal  := Geral.FDT(DtaFis, 1);
  IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
{
  N�o tem relacionamento!  Ser� feito a parte depois das inclus�es!
  if QrCR01infCanc_cStat.Value = 101 then
    Status := 101
  else
}
  if QrPR01infProt_cStat.Value = 100 then
    Status := 100
  else
    Status := 0;
  //
  if FatNum = 0 then
    FatNum := UMyMod.BuscaEmLivreY_Def('StqInnCad', 'Codigo', stIns, 0, nil, False);


  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaba', False, [
  'IDCtrl', 'LoteEnv', 'versao',
  'Id', 'ide_cUF', 'ide_cNF',
  'ide_natOp', 'ide_indPag', 'ide_mod',
  'ide_serie', 'ide_nNF', 'ide_dEmi',
  'ide_hEmi', 'ide_dhEmiTZD', 'ide_dhSaiEntTZD',
  'ide_dSaiEnt', 'ide_hSaiEnt', 'ide_tpNF',
  'ide_idDest', 'ide_indFinal', 'ide_indPres',
  'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis',
  'ide_cDV', 'ide_tpAmb', 'ide_finNFe',
  'ide_procEmi', 'ide_verProc', 'ide_dhCont',
  (*'ide_dhContTZD',*)
  'ide_xJust', 'emit_CNPJ', 'emit_CPF',
  'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_UF',
  'emit_CEP', 'emit_cPais', 'emit_xPais',
  'emit_fone', 'emit_IE', 'emit_IEST',
  'emit_IM', 'emit_CNAE', 'emit_CRT',
  'dest_CNPJ', 'dest_CPF',
  (*'EstrangDef', 'dest_idEstrangeiro',*)
  'dest_xNome',
  'dest_xLgr', 'dest_nro', 'dest_xCpl',
  'dest_xBairro', 'dest_cMun', 'dest_xMun',
  'dest_UF', 'dest_CEP', 'dest_cPais',
  'dest_xPais', 'dest_fone',
  'dest_indIEDest',
  'dest_IE',
  'dest_ISUF',
  'dest_IM',
  'dest_email', 'ICMSTot_vBC',
  'ICMSTot_vICMS',
  'ICMSTot_vICMSDeson',
  'ICMSTot_vBCST', 'ICMSTot_vST',
  'ICMSTot_vProd', 'ICMSTot_vFrete', 'ICMSTot_vSeg',
  'ICMSTot_vDesc', 'ICMSTot_vII', 'ICMSTot_vIPI',
  'ICMSTot_vPIS', 'ICMSTot_vCOFINS', 'ICMSTot_vOutro',
  'ICMSTot_vNF', 'ISSQNtot_vServ', 'ISSQNtot_vBC',
  'ISSQNtot_vISS', 'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS',
  (*'ISSQNtot_dCompet', 'ISSQNtot_vDeducao', 'ISSQNtot_vOutro',
  'ISSQNtot_vDescIncond', 'ISSQNtot_vDescCond', 'ISSQNtot_vISSRet',
  'ISSQNtot_cRegTrib',*)
  'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
  'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
  'RetTrib_vRetPrev', 'ModFrete', 'Transporta_CNPJ',
  'Transporta_CPF', 'Transporta_XNome', 'Transporta_IE',
  'Transporta_XEnder', 'Transporta_XMun', 'Transporta_UF',
  'RetTransp_vServ', 'RetTransp_vBCRet', 'RetTransp_PICMSRet',
  'RetTransp_vICMSRet', 'RetTransp_CFOP', 'RetTransp_CMunFG',
  'VeicTransp_Placa', 'VeicTransp_UF', 'VeicTransp_RNTC',
  'Vagao', 'Balsa', 'Cobr_Fat_nFat',
  'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
  'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
  'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
  'Compra_XCont', 'Status', 'protNFe_versao',
  ////////////// Nao fazer! zera FatID 53 quando preenchido!!! /////////////////
  (*'infProt_Id',*)
  //////////////////////////////////////////////////////////////////////////////
  'infProt_tpAmb', 'infProt_verAplic',
  'infProt_dhRecbto',
  (*'infProt_dhRecbtoTZD',*)
  'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo', (*'retCancNFe_versao',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_dhRecbtoTZD',  'infCanc_nProt', 'infCanc_digVal',
  'infCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
  'infCanc_xJust', (*'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'FreteExtra', 'SegurExtra', 'ICMSRec_pRedBC',
  'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
  'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
  'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
  'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
  'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
  'DataFiscal', (*'SINTEGRA_ExpDeclNum', 'SINTEGRA_ExpDeclDta',
  'SINTEGRA_ExpNat', 'SINTEGRA_ExpRegNum', 'SINTEGRA_ExpRegDta',
  'SINTEGRA_ExpConhNum', 'SINTEGRA_ExpConhDta', 'SINTEGRA_ExpConhTip',
  'SINTEGRA_ExpPais', 'SINTEGRA_ExpAverDta',*) 'CodInfoEmit',
  'CodInfoDest', (*'CriAForca', 'CodInfoTrsp',
  'OrdemServ', 'Situacao', 'Antigo'
  NFG_Serie, NF_ICMSAlq, NF_CFOP,
  Importado, NFG_SubSerie, NFG_ValIsen,
  NFG_NaoTrib, NFG_Outros, COD_MOD,
  COD_SIT, VL_ABAT_NT, EFD_INN_AnoMes,
  EFD_INN_Empresa, EFD_INN_LinArq, ICMSRec_vBCST,
  ICMSRec_vICMSST, EFD_EXP_REG, ICMSRec_pAliqST,
  eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic,
  eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo,
  eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento,
  eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest,
  eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC,
  eveMDe_nProt, cSitNFe, cSitConf,
  NFeNT2013_003LTT, vTotTrib, vBasTrib,
  pTotTrib, infCCe_verAplic, infCCe_cOrgao,
  infCCe_tpAmb, infCCe_CNPJ, infCCe_CPF,
  infCCe_chNFe, infCCe_dhEvento, infCCe_dhEventoTZD,
  infCCe_tpEvento, infCCe_nSeqEvento, infCCe_verEvento,
  infCCe_xCorrecao, infCCe_cStat, infCCe_dhRegEvento,
  infCCe_dhRegEventoTZD, infCCe_nProt, infCCe_nCondUso,*)
  (*'InfCpl_totTrib',*) 'ICMSTot_vFCPUFDest', 'ICMSTot_vICMSUFDest',
  'ICMSTot_vICMSUFRemet', 'Exporta_XLocDespacho'(*, 'CodInfoCliI'*)
  ], [
  'FatID', 'FatNum', 'Empresa'], [
  IDCtrl, LoteEnv, QrNFeBversao.Value,
  QrNFeBId.Value, QrNFeBide_cUF.Value, QrNFeBide_cNF.Value,
  QrNFeBide_natOp.Value, QrNFeBide_indPag.Value, QrNFeBide_mod.Value,
  QrNFeBide_serie.Value, QrNFeBide_nNF.Value, QrNFeBide_dEmi.Value,
  QrNFeBide_hEmi.Value, QrNFeBide_dhEmiTZD.Value, QrNFeBide_dhSaiEntTZD.Value,
  (*QrNFeBide_dSaiEnt.Value*)ide_dSaiEnt, QrNFeBide_hSaiEnt.Value, QrNFeBide_tpNF.Value,
  QrNFeBide_idDest.VAlue, QrNFeBide_indFinal.Value, QrNFeBide_indPres.Value,
  QrNFeBide_cMunFG.Value, QrNFeBide_tpImp.Value, QrNFeBide_tpEmis.Value,
  QrNFeBide_cDV.Value, QrNFeBide_tpAmb.Value, QrNFeBide_finNFe.Value,
  QrNFeBide_procEmi.Value, QrNFeBide_verProc.Value, QrNFeBide_dhCont.Value,
  (*QrNFeBide_dhContTZD.Value,*)
  QrNFeBide_xJust.Value, QrNFeCemit_CNPJ.Value, QrNFeCemit_CPF.Value,
  QrNFeCemit_xNome.Value, QrNFeCemit_xFant.Value, QrNFeCemit_xLgr.Value,
  QrNFeCemit_nro.Value, QrNFeCemit_xCpl.Value, QrNFeCemit_xBairro.Value,
  QrNFeCemit_cMun.Value, QrNFeCemit_xMun.Value, QrNFeCemit_UF.Value,
  QrNFeCemit_CEP.Value, QrNFeCemit_cPais.Value, QrNFeCemit_xPais.Value,
  QrNFeCemit_fone.Value, QrNFeCemit_IE.Value, QrNFeCemit_IEST.Value,
  QrNFeCemit_IM.Value, QrNFeCemit_CNAE.Value, QrNFeCemit_CRT.Value,
  QrNFeEdest_CNPJ.Value, QrNFeEdest_CPF.Value,
  (*EstrangDef, dest_idEstrangeiro,*)
  QrNFeEdest_xNome.Value,
  QrNFeEdest_xLgr.Value, QrNFeEdest_nro.Value, QrNFeEdest_xCpl.Value,
  QrNFeEdest_xBairro.Value, QrNFeEdest_cMun.Value, QrNFeEdest_xMun.Value,
  QrNFeEdest_UF.Value, QrNFeEdest_CEP.Value, QrNFeEdest_cPais.Value,
  QrNFeEdest_xPais.Value, QrNFeEdest_fone.Value,
  QrNFeEdest_indIEDest.Value,
  QrNFeEdest_IE.Value,
  QrNFeEdest_ISUF.Value,
  QrNFeEdest_IM.Value,
  QrNFeEdest_email.Value, QrNFeW02ICMSTot_vBC.Value,
  QrNFeW02ICMSTot_vICMS.Value,
  QrNFeW02ICMSTot_vICMSDeson.Value,
  QrNFeW02ICMSTot_vBCST.Value, QrNFeW02ICMSTot_vST.Value,
  QrNFeW02ICMSTot_vProd.Value, QrNFeW02ICMSTot_vFrete.Value, QrNFeW02ICMSTot_vSeg.Value,
  QrNFeW02ICMSTot_vDesc.Value, QrNFeW02ICMSTot_vII.Value, QrNFeW02ICMSTot_vIPI.Value,
  QrNFeW02ICMSTot_vPIS.Value, QrNFeW02ICMSTot_vCOFINS.Value, QrNFeW02ICMSTot_vOutro.Value,
  QrNFeW02ICMSTot_vNF.Value, QrNFeW17ISSQNtot_vServ.Value, QrNFeW17ISSQNtot_vBC.Value,
  QrNFeW17ISSQNtot_vISS.Value, QrNFeW17ISSQNtot_vPIS.Value, QrNFeW17ISSQNtot_vCOFINS.Value,
  (*QrNFeW17ISSQNtot_dCompet, QrNFeW17ISSQNtot_vDeducao, QrNFeW17ISSQNtot_vOutro,
  QrNFeW17ISSQNtot_vDescIncond, QrNFeW17ISSQNtot_vDescCond, QrNFeW17ISSQNtot_vISSRet,
  QrNFeW17ISSQNtot_cRegTrib,*)
  QrNFeW23RetTrib_vRetPIS.Value, QrNFeW23RetTrib_vRetCOFINS.Value, QrNFeW23RetTrib_vRetCSLL.Value,
  QrNFeW23RetTrib_vBCIRRF.Value, QrNFeW23RetTrib_vIRRF.Value, QrNFeW23RetTrib_vBCRetPrev.Value,
  QrNFeW23RetTrib_vRetPrev.Value, QrNFeX01ModFrete.Value, QrNFeX01Transporta_CNPJ.Value,
  QrNFeX01Transporta_CPF.Value, QrNFeX01Transporta_XNome.Value, QrNFeX01Transporta_IE.Value,
  QrNFeX01Transporta_XEnder.Value, QrNFeX01Transporta_XMun.Value, QrNFeX01Transporta_UF.Value,
  QrNFeX01RetTransp_vServ.Value, QrNFeX01RetTransp_vBCRet.Value, QrNFeX01RetTransp_PICMSRet.Value,
  QrNFeX01RetTransp_vICMSRet.Value, QrNFeX01RetTransp_CFOP.Value, QrNFeX01RetTransp_CMunFG.Value,
  QrNFeX01VeicTransp_Placa.Value, QrNFeX01VeicTransp_UF.Value, QrNFeX01VeicTransp_RNTC.Value,
  QrNFeX01Vagao.Value, QrNFeX01Balsa.Value, QrNFeY01Cobr_Fat_nFat.Value,
  QrNFeY01Cobr_Fat_vOrig.Value, QrNFeY01Cobr_Fat_vDesc.Value, QrNFeY01Cobr_Fat_vLiq.Value,
  QrNFeZ01InfAdic_InfAdFisco.Value, QrNFeZ01InfAdic_InfCpl.Value, QrNFeZA01Exporta_UFEmbarq.Value,
  QrNFeZA01Exporta_XLocEmbarq.Value, QrNFeZB01Compra_XNEmp.Value, QrNFeZB01Compra_XPed.Value,
  QrNFeZB01Compra_XCont.Value, Status, QrPR01protNFe_versao.Value,
  ////////////// Nao fazer! zera FatID 53 quando preenchido!!! /////////////////
  (*QrPR01infProt_Id.Value,*)
  //////////////////////////////////////////////////////////////////////////////
  QrPR01infProt_tpAmb.Value, QrPR01infProt_verAplic.Value,
  QrPR01infProt_dhRecbto.Value,
  (*infProt_dhRecbtoTZD,*)
  QrPR01infProt_nProt.Value, QrPR01infProt_digVal.Value,
  QrPR01infProt_cStat.Value, QrPR01infProt_xMotivo.Value, (*QrCR01retCanc_versao.Value,
  QrCR01infCanc_Id.Value, QrCR01infCanc_tpAmb.Value, QrCR01infCanc_verAplic.Value,
  QrCR01infCanc_dhRecbto.Value, infCanc_dhRecbtoTZD?, QrCR01infCanc_nProt.Value, infCanc_digVal,
  QrCR01infCanc_cStat.Value, QrCR01infCanc_xMotivo.Value, infCanc_cJust,
  infCanc_xJust, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  FreteExtra, SegurExtra, ICMSRec_pRedBC,
  ICMSRec_vBC, ICMSRec_pAliq, ICMSRec_vICMS,
  IPIRec_pRedBC, IPIRec_vBC, IPIRec_pAliq,
  IPIRec_vIPI, PISRec_pRedBC, PISRec_vBC,
  PISRec_pAliq, PISRec_vPIS, COFINSRec_pRedBC,
  COFINSRec_vBC, COFINSRec_pAliq, COFINSRec_vCOFINS,*)
  DataFiscal, (*SINTEGRA_ExpDeclNum, SINTEGRA_ExpDeclDta,
  SINTEGRA_ExpNat, SINTEGRA_ExpRegNum, SINTEGRA_ExpRegDta,
  SINTEGRA_ExpConhNum, SINTEGRA_ExpConhDta, SINTEGRA_ExpConhTip,
  SINTEGRA_ExpPais, SINTEGRA_ExpAverDta,*) QrNFeCCodInfoEmit.Value,
  QrNFeECodInfoDest.Value, (*CriAForca, CodInfoTrsp,
  OrdemServ, Situacao, Antigo
  NFG_Serie, NF_ICMSAlq, NF_CFOP,
  Importado, NFG_SubSerie, NFG_ValIsen,
  NFG_NaoTrib, NFG_Outros, COD_MOD,
  COD_SIT, VL_ABAT_NT, EFD_INN_AnoMes,
  EFD_INN_Empresa, EFD_INN_LinArq, ICMSRec_vBCST,
  ICMSRec_vICMSST, EFD_EXP_REG, ICMSRec_pAliqST,
  eveMDe_Id, eveMDe_tpAmb, eveMDe_verAplic,
  eveMDe_cOrgao, eveMDe_cStat, eveMDe_xMotivo,
  eveMDe_chNFe, eveMDe_tpEvento, eveMDe_xEvento,
  eveMDe_nSeqEvento, eveMDe_CNPJDest, eveMDe_CPFDest,
  eveMDe_emailDest, eveMDe_dhRegEvento, eveMDe_TZD_UTC,
  eveMDe_nProt, cSitNFe, cSitConf,
  NFeNT2013_003LTT, vTotTrib, vBasTrib,
  pTotTrib, infCCe_verAplic, infCCe_cOrgao,
  infCCe_tpAmb, infCCe_CNPJ, infCCe_CPF,
  infCCe_chNFe, infCCe_dhEvento, infCCe_dhEventoTZD,
  infCCe_tpEvento, infCCe_nSeqEvento, infCCe_verEvento,
  infCCe_xCorrecao, infCCe_cStat, infCCe_dhRegEvento,
  infCCe_dhRegEventoTZD, infCCe_nProt, infCCe_nCondUso,*)
  (*InfCpl_totTrib,*) QrNFeW02ICMSTot_vFCPUFDest.Value,
  QrNFeW02ICMSTot_vICMSUFDest.Value, QrNFeW02ICMSTot_vICMSUFRemet.Value,
  QrNFeZA01Exporta_XLocDespacho.Value(*, CodInfoCliI*)
  ], [
  FatID, FatNum, Empresa], True);
  //
  if Result and (Trim(QrPR01infProt_Id.Value) <> '') then
  begin
    ////////// fazer aqui! para nao zerar FatID 53 quando preenchido!!! ////////
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
    'infProt_Id'
    ], [
    'FatID', 'FatNum', 'Empresa'], [
    QrPR01infProt_Id.Value
    ], [
    FatID, FatNum, Empresa], True);
  end;
  if QrNFeBId.Value <> '' then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM nfecaba ',
    'WHERE FatID=' + Geral.FF0(VAR_FatID_0053),
    'AND Empresa=' + Geral.FF0(Empresa),
    'AND Id="' + QrNFeBId.Value + '" ' +
    '']);
  end;
end;

function TFmNFeLoad_Inn.IncluiNFeCabB(FatID, FatNum, Empresa: Integer): Boolean;
const
  QualNFref = -1;
var
  Controle: Integer;
begin
  if QrNFeB12a.RecordCount > 0 then
  begin
    QrNFeB12a.First;
    while not QrNFeB12a.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabb', 'Controle', '', 0);
      //
      //Result :=
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabb', False, [
      'refNFe', 'refNF_cUF', 'refNF_AAMM',
      'refNF_CNPJ', 'refNF_mod', 'refNF_serie',
      'refNF_nNF', 'refNFP_cUF', 'refNFP_AAMM',
      'refNFP_CNPJ', 'refNFP_CPF', 'refNFP_IE',
      'refNFP_mod', 'refNFP_serie', 'refNFP_nNF',
      'refCTe', 'refECF_mod', 'refECF_nECF',
      'refECF_nCOO', 'QualNFref'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeB12arefNFe.Value, QrNFeB12arefNF_cUF.Value, QrNFeB12arefNF_AAMM.Value,
      QrNFeB12arefNF_CNPJ.Value, QrNFeB12arefNF_mod.Value, QrNFeB12arefNF_serie.Value,
      QrNFeB12arefNF_nNF.Value, QrNFeB12arefNFP_cUF.Value, QrNFeB12arefNFP_AAMM.Value,
      QrNFeB12arefNFP_CNPJ.Value, QrNFeB12arefNFP_CPF.Value, QrNFeB12arefNFP_IE.Value,
      QrNFeB12arefNFP_mod.Value, QrNFeB12arefNFP_serie.Value, QrNFeB12arefNFP_nNF.Value,
      QrNFeB12arefCTe.Value, QrNFeB12arefECF_mod.Value, QrNFeB12arefECF_nECF.Value,
      QrNFeB12arefECF_nCOO.Value, QualNFref], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeB12a.Next;
    end;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabF(FatID, FatNum, Empresa: Integer): Boolean;
begin
  if QrNFeF.RecordCount > 0 then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabf', False, [
    'retirada_CNPJ', 'retirada_CPF', 'retirada_xLgr',
    'retirada_nro', 'retirada_xCpl', 'retirada_xBairro',
    'retirada_cMun', 'retirada_xMun', 'retirada_UF'], [
    'FatID', 'FatNum', 'Empresa'], [
    QrNFeFretirada_CNPJ.Value, QrNFeFretirada_CPF.Value, QrNFeFretirada_xLgr.Value,
    QrNFeFretirada_nro.Value, QrNFeFretirada_xCpl.Value, QrNFeFretirada_xBairro.Value,
    QrNFeFretirada_cMun.Value, QrNFeFretirada_xMun.Value, QrNFeFretirada_UF.Value], [
    FatID, FatNum, Empresa], True);
    //
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabG(FatID, FatNum, Empresa: Integer): Boolean;
begin
  if QrNFeG.RecordCount > 0 then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabg', False, [
    'entrega_CNPJ', 'entrega_CPF', 'entrega_xLgr',
    'entrega_nro', 'entrega_xCpl', 'entrega_xBairro',
    'entrega_cMun', 'entrega_xMun', 'entrega_UF'], [
    'FatID', 'FatNum', 'Empresa'], [
    QrNFeGentrega_CNPJ.Value, QrNFeGentrega_CPF.Value, QrNFeGentrega_xLgr.Value,
    QrNFeGentrega_nro.Value, QrNFeGentrega_xCpl.Value, QrNFeGentrega_xBairro.Value,
    QrNFeGentrega_cMun.Value, QrNFeGentrega_xMun.Value, QrNFeGentrega_UF.Value], [
    FatID, FatNum, Empresa], True);
    //
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabGA(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Qry: TmySQLQuery;
  SQLType: TSQLType;
  Controle: Integer;
begin
  if QrNFeGA.RecordCount > 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      QrNFeGA.First;
      while not QrNFeGA.Eof do
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM nfecabga ',
        'WHERE FatID=' + Geral.FF0(FatID),
        'AND FatNum=' + Geral.FF0(FatNum),
        'AND EMpresa=' + Geral.FF0(Empresa),
        'AND autXML_CNPJ = "' + QrNFeGAautXML_CNPJ.Value + '" ',
        'AND autXML_CPF = "' + QrNFeGAautXML_CPF.Value + '" ',
        '']);
        if Qry.RecordCount > 0 then
        begin
          SQLType  := stUpd;
          Controle := Qry.FieldByName('Controle').AsInteger;
        end else
        begin
          SQLType  := stIns;
          Controle := 0;
        end;
        Controle := UMyMod.BPGS1I32('nfecabga', 'Controle', '', '', tsPos, SQLType, Controle);
        Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabga', False, [
        'autXML_CNPJ', 'autXML_CPF'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        QrNFeGAautXML_CNPJ.Value, QrNFeGAautXML_CPF.Value], [
        FatID, FatNum, Empresa, Controle], True);
        //
        QrNFeGA.Next;
      end;
    finally
      Qry.Free;
    end;
    //
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabXLac(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Conta: Integer;
begin
  Result := False;
  if QrNFeX33.RecordCount > 0 then
  begin
    QrNFeX33.First;
    while not QrNFeX33.Eof do
    begin
      Conta := DModG.BuscaProximoInteiro('nfecabxlac', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxlac', False, [
      'nLacre'], ['FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
      QrNFeX33nLacre.Value], [
      FatID, FatNum, Empresa, QrNFeX26Controle.Value, Conta], True);
      //
      QrNFeX33.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabXReb(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeX22.RecordCount > 0 then
  begin
    QrNFeX22.First;
    while not QrNFeX22.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabxreb', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxreb', False, [
      'placa', 'UF', 'RNTC'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeX22placa.Value, QrNFeX22UF.Value, QrNFeX22RNTC.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeX22.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabXVol(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeX26.RecordCount > 0 then
  begin
    QrNFeX26.First;
    while not QrNFeX26.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabxvol', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabxvol', False, [
      'qVol', 'esp', 'marca',
      'nVol', 'pesoL', 'pesoB'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeX26qVol.Value, QrNFeX26esp.Value, QrNFeX26marca.Value,
      QrNFeX26nVol.Value, QrNFeX26pesoL.Value, QrNFeX26pesoB.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      IncluiNFeCabXLac(FatID, FatNum, Empresa);
      //
      QrNFeX26.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabY(FatID, FatNum, Empresa: Integer): Boolean;
const
  Lancto = 0;
  Sub = 0;
var
  Controle, FatParcela: Integer;
begin
  Result := False;
  if QrNFeY07.RecordCount > 0 then
  begin
    QrNFeY07.First;
    while not QrNFeY07.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecaby', 'Controle', '', 0);
      FatParcela := QrNFeY07.RecNo;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecaby', False, [
      'nDup', 'dVenc', 'vDup',
      'Lancto', 'Sub'], [
      'FatID', 'FatNum', 'FatParcela', 'Empresa', 'Controle'], [
      QrNFeY07nDup.Value, QrNFeY07dVenc.Value, QrNFeY07vDup.Value,
      Lancto, Sub], [
      FatID, FatNum, FatParcela, Empresa, Controle], True);
      //
      QrNFeY07.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabZCon(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeZ04.RecordCount > 0 then
  begin
    QrNFeZ04.First;
    while not QrNFeZ04.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabzcon', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabzcon', False, [
      'xCampo', 'xTexto'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeZ04xCampo.Value, QrNFeZ04xTexto.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeZ04.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabZFis(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeZ07.RecordCount > 0 then
  begin
    QrNFeZ07.First;
    while not QrNFeZ07.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabzfis', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabzfis', False, [
      'xCampo', 'xTexto'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeZ07xCampo.Value, QrNFeZ07xTexto.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeZ07.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeCabZPro(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Controle: Integer;
begin
  Result := False;
  if QrNFeZ10.RecordCount > 0 then
  begin
    QrNFeZ10.First;
    while not QrNFeZ10.Eof do
    begin
      Controle := DModG.BuscaProximoInteiro('nfecabzpro', 'Controle', '', 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabzpro', False, [
      'nProc', 'indProc'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      QrNFeZ10nProc.Value, QrNFeZ10indProc.Value], [
      FatID, FatNum, Empresa, Controle], True);
      //
      QrNFeZ10.Next;
    end;
  end else
    Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsI(FatID, FatNum, Empresa: Integer): Boolean;
const
  MeuID = 0;
begin
  QrItsI.First;
  while not QrItsI.Eof do
  begin
    //Controle := DModG.BuscaProximoInteiro('nfeitsi', 'Controle', '', 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsi', False, [
    'prod_cProd', 'prod_cEAN', 'prod_xProd',
    'prod_NCM', 'prod_EXTIPI', (*'prod_genero',*)
    'prod_CFOP', 'prod_uCom', 'prod_qCom',
    'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
    'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
    'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
    'prod_vOutro', 'prod_indTot', 'prod_xPed',
    'prod_nItemPed', (*'Tem_IPI',
    'InfAdCuztm', 'EhServico', 'ICMSRec_pRedBC',
    'ICMSRec_vBC', 'ICMSRec_pAliq', 'ICMSRec_vICMS',
    'IPIRec_pRedBC', 'IPIRec_vBC', 'IPIRec_pAliq',
    'IPIRec_vIPI', 'PISRec_pRedBC', 'PISRec_vBC',
    'PISRec_pAliq', 'PISRec_vPIS', 'COFINSRec_pRedBC',
    'COFINSRec_vBC', 'COFINSRec_pAliq', 'COFINSRec_vCOFINS',*)
    'MeuID', 'Nivel1', 'GraGruX',
    (*UnidMedCom, UnidMedTrib, ICMSRec_vBCST,
    ICMSRec_vICMSST, ICMSRec_pAliqST, Tem_II,*)
    'prod_nFCI', 'prod_CEST'(*, StqMovValA*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsIprod_cProd.Value, QrItsIprod_cEAN.Value, QrItsIprod_xProd.Value,
    QrItsIprod_NCM.Value, QrItsIprod_EXTIPI.Value, (*prod_genero,*)
    QrItsIprod_CFOP.Value, QrItsIprod_uCom.Value, QrItsIprod_qCom.Value,
    QrItsIprod_vUnCom.Value, QrItsIprod_vProd.Value, QrItsIprod_cEANTrib.Value,
    QrItsIprod_uTrib.Value, QrItsIprod_qTrib.Value, QrItsIprod_vUnTrib.Value,
    QrItsIprod_vFrete.Value, QrItsIprod_vSeg.Value, QrItsIprod_vDesc.Value,
    QrItsIprod_vOutro.Value, QrItsIprod_indTot.Value, QrItsIprod_xPed.Value,
    QrItsIprod_nItemPed.Value, (*QrItsITem_IPI.Value,
    QrItsIInfAdCuztm.Value, QrItsIEhServico.Value, QrItsIICMSRec_pRedBC.Value,
    QrItsIICMSRec_vBC.Value, QrItsIICMSRec_pAliq.Value, QrItsIICMSRec_vICMS.Value,
    QrItsIIPIRec_pRedBC.Value, QrItsIIPIRec_vBC.Value, QrItsIIPIRec_pAliq.Value,
    QrItsIIPIRec_vIPI.Value, QrItsIPISRec_pRedBC.Value, QrItsIPISRec_vBC.Value,
    QrItsIPISRec_pAliq.Value, QrItsIPISRec_vPIS.Value, QrItsICOFINSRec_pRedBC.Value,
    QrItsICOFINSRec_vBC.Value, QrItsICOFINSRec_pAliq.Value, QrItsICOFINSRec_vCOFINS.Value,*)
    MeuID, QrItsINivel1.Value, QrItsIGraGruX.Value,
    QrItsIprod_nFCI.Value, QrItsIprod_CEST.Value], [
    FatID, FatNum, Empresa, QrItsInItem.Value], True) then
    begin
      IncluiNFeItsM(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsN(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsO(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsP(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsQ(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsR(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsS(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsT(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsU(FatID, FatNum, Empresa, QrItsInItem.Value);
      IncluiNFeItsV(FatID, FatNum, Empresa, QrItsInItem.Value);
      //
{}
      UnNFe_PF.MostraFormNFeEFD_C170(stNil, FatID, FatNum, Empresa, QrItsInItem.Value);
{}
    end;
    //
    QrItsI.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsM(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsM.First;
  while not QrItsM.Eof do
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsm', False, [
    (*'iTotTrib',*) 'vTotTrib'(*, 'vBasTrib',
    'pTotTrib', 'fontTotTrib', 'tabTotTrib',
    'verTotTrib', 'tpAliqTotTrib'*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    (*iTotTrib,*) QrItsMvTotTrib.Value(*, vBasTrib,
    pTotTrib, fontTotTrib, tabTotTrib,
    verTotTrib, tpAliqTotTrib*)], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsM.Next;
  end;
  Result := True;
end;

{ N�o Usa
function TFmNFeLoad_Inn.IncluiNFeItsIDi(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
var
  Controle: Integer;
begin
  QrItsIDi.First;
  while not QrItsIDi.Eof do
  begin
    Controle := DModG.BuscaProximoInteiro('nfeitsidi', 'Controle', '', 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsidi', False, [
    'DI_nDI', 'DI_dDI', 'DI_xLocDesemb',
    'DI_UFDesemb', 'DI_dDesemb', 'DI_cExportador'], [
    'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle'], [
    QrItsIDiDI_nDI.Value, QrItsIDiDI_dDI.Value, QrItsIDiDI_xLocDesemb.Value,
    QrItsIDiDI_UFDesemb.Value, QrItsIDiDI_dDesemb.Value, QrItsIDiDI_cExportador.Value], [
    FatID, FatNum, Empresa, nItem, Controle], True) then
      IncluiNFeItsIDiA(FatID, FatNum, Empresa, nItem, Controle);
    //
    QrItsIDi.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsIDiA(FatID, FatNum, Empresa,
  nItem, Controle: Integer): Boolean;
var
  Conta: Integer;
begin
  QrItsIDiA.First;
  while not QrItsIDiA.Eof do
  begin
    Conta := DModG.BuscaProximoInteiro('nfeitsidia', 'Controle', '', 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsidia', False, [
    'Adi_nAdicao', 'Adi_nSeqAdic', 'Adi_cFabricante',
    'Adi_vDescDI'], [
    'FatID', 'FatNum', 'Empresa', 'nItem', 'Controle', 'Conta'], [
    QrItsIDiAAdi_nAdicao.Value, QrItsIDiAAdi_nSeqAdic.Value, QrItsIDiAAdi_cFabricante.Value,
    QrItsIDiAAdi_vDescDI.Value], [
    FatID, FatNum, Empresa, nItem, Controle, Conta], True) then;
    //
    QrItsIDiA.Next;
  end;
  Result := True;
end;
}

function TFmNFeLoad_Inn.IncluiNFeItsN(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsN.First;
  while not QrItsN.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitsn', False, [
    'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
    'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
    'ICMS_vICMSOp', 'ICMS_pDif', 'ICMS_vICMSDif',
    'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
    'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
    'ICMS_vICMSST', 'ICMS_CSOSN',
    'ICMS_UFST', 'ICMS_pBCOp', 'ICMS_vBCSTRet',
    'ICMS_vICMSSTRet', 'ICMS_vICMSDeson', 'ICMS_motDesICMS',
    'ICMS_pCredSN', 'ICMS_vCredICMSSN'(*, COD_NAT*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsNICMS_Orig.Value, QrItsNICMS_CST.Value, QrItsNICMS_modBC.Value,
    QrItsNICMS_pRedBC.Value, QrItsNICMS_vBC.Value, QrItsNICMS_pICMS.Value,
    QrItsNICMS_vICMSOp.Value, QrItsNICMS_pDif.Value, QrItsNICMS_vICMSDif.Value,
    QrItsNICMS_vICMS.Value, QrItsNICMS_modBCST.Value, QrItsNICMS_pMVAST.Value,
    QrItsNICMS_pRedBCST.Value, QrItsNICMS_vBCST.Value, QrItsNICMS_pICMSST.Value,
    QrItsNICMS_vICMSST.Value, QrItsNICMS_CSOSN.Value,
    QrItsNICMS_UFST.Value, QrItsNICMS_pBCOp.Value, QrItsNICMS_vBCSTRet.Value,
    QrItsNICMS_vICMSSTRet.Value, QrItsNICMS_vICMSDeson.Value, QrItsNICMS_motDesICMS.Value,
    QrItsNICMS_pCredSN.Value, QrItsNICMS_vCredICMSSN.Value(*, COD_NAT*)], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsN.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsO(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsO.First;
  while not QrItsO.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeitso', False, [
    'IPI_clEnq', 'IPI_CNPJProd', 'IPI_cSelo',
    'IPI_qSelo', 'IPI_cEnq', 'IPI_CST',
    'IPI_vBC', 'IPI_qUnid', 'IPI_vUnid',
    'IPI_pIPI', 'IPI_vIPI'(*, 'IND_APUR'*)], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsOIPI_clEnq.Value, QrItsOIPI_CNPJProd.Value, QrItsOIPI_cSelo.Value,
    QrItsOIPI_qSelo.Value, QrItsOIPI_cEnq.Value, QrItsOIPI_CST.Value,
    QrItsOIPI_vBC.Value, QrItsOIPI_qUnid.Value, QrItsOIPI_vUnid.Value,
    QrItsOIPI_pIPI.Value, QrItsOIPI_vIPI.Value(*, IND_APUR*)], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsO.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsP(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsP.First;
  while not QrItsP.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsp', False, [
    'II_vBC', 'II_vDespAdu', 'II_vII',
    'II_vIOF'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsPII_vBC.Value, QrItsPII_vDespAdu.Value, QrItsPII_vII.Value,
    QrItsPII_vIOF.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsP.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsQ(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsQ.First;
  while not QrItsQ.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsq', False, [
    'PIS_CST', 'PIS_vBC', 'PIS_pPIS',
    'PIS_vPIS', 'PIS_qBCProd', 'PIS_vAliqProd',
    'PIS_fatorBC'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsQPIS_CST.Value, QrItsQPIS_vBC.Value, QrItsQPIS_pPIS.Value,
    QrItsQPIS_vPIS.Value, QrItsQPIS_qBCProd.Value, QrItsQPIS_vAliqProd.Value,
    QrItsQPIS_fatorBC.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsQ.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsR(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsR.First;
  while not QrItsR.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsr', False, [
    'PISST_vBC', 'PISST_pPIS', 'PISST_qBCProd',
    'PISST_vAliqProd', 'PISST_vPIS', 'PISST_fatorBCST'
    ], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsRPISST_vBC.Value, QrItsRPISST_pPIS.Value, QrItsRPISST_qBCProd.Value,
    QrItsRPISST_vAliqProd.Value, QrItsRPISST_vPIS.Value, QrItsRPISST_fatorBCST.Value
    ], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsR.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsS(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsS.First;
  while not QrItsS.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItss', False, [
    'COFINS_CST', 'COFINS_vBC', 'COFINS_pCOFINS',
    'COFINS_qBCProd', 'COFINS_vAliqProd', 'COFINS_vCOFINS',
    'COFINS_fatorBC'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsSCOFINS_CST.Value, QrItsSCOFINS_vBC.Value, QrItsSCOFINS_pCOFINS.Value,
    QrItsSCOFINS_qBCProd.Value, QrItsSCOFINS_vAliqProd.Value, QrItsSCOFINS_vCOFINS.Value,
    QrItsSCOFINS_fatorBC.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsS.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsT(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsT.First;
  while not QrItsT.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItst', False, [
    'COFINSST_vBC', 'COFINSST_pCOFINS', 'COFINSST_qBCProd',
    'COFINSST_vAliqProd', 'COFINSST_vCOFINS', 'COFINSST_fatorBCST'
    ], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsTCOFINSST_vBC.Value, QrItsTCOFINSST_pCOFINS.Value, QrItsTCOFINSST_qBCProd.Value,
    QrItsTCOFINSST_vAliqProd.Value, QrItsTCOFINSST_vCOFINS.Value, QrItsTCOFINSST_fatorBCST.Value
    ], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsT.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsU(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsU.First;
  while not QrItsU.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsu', False, [
    'ISSQN_vBC', 'ISSQN_vAliq', 'ISSQN_vISSQN',
    'ISSQN_cMunFG', 'ISSQN_cListServ', 'ISSQN_cSitTrib',
    'ISSQN_vDeducao', 'ISSQN_vOutro',
    'ISSQN_vDescIncond', 'ISSQN_vDescCond', 'ISSQN_vISSRet',
    'ISSQN_indISS', 'ISSQN_cServico', 'ISSQN_cMun',
    'ISSQN_cPais', 'ISSQN_nProcesso', 'ISSQN_indIncentivo'
    ], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsUISSQN_vBC.Value, QrItsUISSQN_vAliq.Value, QrItsUISSQN_vISSQN.Value,
    QrItsUISSQN_cMunFG.Value, QrItsUISSQN_cListServ.Value, QrItsUISSQN_cSitTrib.Value,
    QrItsUISSQN_vDeducao.Value, QrItsUISSQN_vOutro.Value,
    QrItsUISSQN_vDescIncond.Value, QrItsUISSQN_vDescCond.Value, QrItsUISSQN_vISSRet.Value,
    QrItsUISSQN_indISS.Value, QrItsUISSQN_cServico.Value, QrItsUISSQN_cMun.Value,
    QrItsUISSQN_cPais.Value, QrItsUISSQN_nProcesso.Value, QrItsUISSQN_indIncentivo.Value
    ], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsU.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiNFeItsV(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  QrItsV.First;
  while not QrItsV.Eof do
  begin
    //
    //Result :=
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeItsv', False, [
    'InfAdProd'], [
    'FatID', 'FatNum', 'Empresa', 'nItem'], [
    QrItsVInfAdProd.Value], [
    FatID, FatNum, Empresa, nItem], True);
    //
    QrItsV.Next;
  end;
  Result := True;
end;

function TFmNFeLoad_Inn.IncluiStqMovItsA(FatID, FatNum, Empresa: Integer;
DataFiscal: TDateTime): Boolean;
const
  StqCenCad = 1;
  Baixa = 1;
var
  DataHora: String;
  Tipo, OriCodi, OriCtrl, GraGruX, IDCtrl: Integer;
  Qtde, Peso, CustoAll: Double;
begin
  DataHora := Geral.FDT(DataFiscal, 1);
  Tipo     := FatID;
  OriCodi  := FatNum;
// Deve-se antes, excluir os j� existes!
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE Tipo=:P0');
  Dmod.QrUpd.SQL.Add('AND OriCodi=:P1 AND Empresa=:P2');
  Dmod.QrUpd.Params[00].AsInteger := Tipo;
  Dmod.QrUpd.Params[01].AsInteger := OriCodi;
  Dmod.QrUpd.Params[02].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  QrItsI.First;
  while not QrItsI.Eof do
  begin
    GraGruX  := QrItsIGraGruX.Value;
    Qtde     := QrItsIprod_qCom.Value;
    Peso     := Qtde;
    CustoAll := QrItsIprod_vProd.Value;
    //
    IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', stIns, 0);
    OriCtrl  := IDCtrl;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
    'DataHora', 'Tipo', 'OriCodi',
    'OriCtrl',
    'Empresa', 'StqCenCad', 'GraGruX',
    'Qtde', 'Peso',
    'CustoAll',
    'Baixa'], [
    'IDCtrl'], [
    DataHora, Tipo, OriCodi,
    OriCtrl,
    Empresa, StqCenCad, GraGruX,
    Qtde, Peso,
    CustoAll,
    Baixa], [
    IDCtrl], False) then ;
    //
    QrItsI.Next;
  end;
  Result := True;
end;

procedure TFmNFeLoad_Inn.MarcaTodosLB(Marca: Boolean);
var
  I: Integer;
begin
  for I := 0 to LBArqs.Count -1 do
    LBArqs.Checked[I] := Marca;
end;

function TFmNFeLoad_Inn.ObtemXML_De_Arquivo(Arquivo: String): WideString;
var
  mTexto: TStringList;
  XML: PChar;
begin
  if FileExists(Arquivo) then
  begin
    mTexto := TStringList.Create;
    mTexto.Clear;
    mTexto.LoadFromFile(Arquivo);
    XML := PChar(mTexto.Text);
    Result := Copy(XML, 1, Length(XML));
    mTexto.Free;
  end else
  begin
    Result := '';
    Geral.MB_Aviso('Arquivo n�o localizado para copiar XML:' + sLineBreak +
    Arquivo);
  end;
end;

procedure TFmNFeLoad_Inn.PageControl1Change(Sender: TObject);
begin
  try
    if PageControl1.ActivePageIndex = 1 then
      AbreCarregados();
  except
    ;
  end;
end;

procedure TFmNFeLoad_Inn.QrErrBAfterOpen(DataSet: TDataSet);
begin
  if QrErrB.RecordCount > 0 then
  begin
    PageControl1.ActivePageIndex := 1;
    PageControl2.ActivePageIndex := 1;
  end;
end;

procedure TFmNFeLoad_Inn.QrErrBAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrI, DModG.MyPID_DB, [
  'SELECT i.nItem, i.GraGruX, i.prod_cProd, i.prod_xProd, i.NomeGGX, ',
  'i.prod_uCom, i.prod_qCom, i.prod_vUnCom, i.prod_vProd, ',
  'i.prod_NCM, i.prod_EXTIPI, i.prod_cEAN, i.prod_CFOP, i.KlsGGXE, ',
  'v.InfAdProd, n.ICMS_Orig, n.ICMS_CST, o.IPI_cEnq, o.IPI_CST,  ',
  'q.PIS_CST, s.COFINS_CST ',
  'FROM _nfe_i_ i ',
  'LEFT JOIN _nfe_n_ n ON n.SeqArq=i.SeqArq AND n.SeqNFe=i.SeqNFe AND n.nItem=i.nItem ',
  'LEFT JOIN _nfe_o_ o ON o.SeqArq=i.SeqArq AND o.SeqNFe=i.SeqNFe AND o.nItem=i.nItem ',
  'LEFT JOIN _nfe_v_ v ON v.SeqArq=i.SeqArq AND v.SeqNFe=i.SeqNFe AND v.nItem=i.nItem ',
  'LEFT JOIN _nfe_q_ q ON q.SeqArq=i.SeqArq AND q.SeqNFe=i.SeqNFe AND q.nItem=i.nItem ',
  'LEFT JOIN _nfe_s_ s ON s.SeqArq=i.SeqArq AND s.SeqNFe=i.SeqNFe AND s.nItem=i.nItem ',
  'WHERE i.SeqArq=' + Geral.FF0(QrErrBSeqArq.Value),
  'AND i.SeqNFe=' + Geral.FF0(QrErrBSeqNFe.Value),
  '']);
  BtEmit.Enabled := QrErrBErrEmit.Value > 0;
  BtDest.Enabled := QrErrBErrDest.Value > 0;
end;

procedure TFmNFeLoad_Inn.QrErrBBeforeClose(DataSet: TDataSet);
begin
  QrErrI.Close;
  BtEmit.Enabled := False;
  BtDest.Enabled := False;
  BtProd.Enabled := False;
end;

procedure TFmNFeLoad_Inn.QrErrIAfterScroll(DataSet: TDataSet);
begin
  //BtProd.Enabled := QrErrIGraGruX.Value = 0;
  BtProd.Enabled := QrErrI.RecordCount > 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrV, DModG.MyPID_DB, [
  'SELECT ',
  'InfAdProd ',
  'FROM _nfe_v_ ',
  'WHERE SeqArq=' + Geral.FF0(QrErrBSeqArq.Value),
  'AND SeqNFe=' + Geral.FF0(QrErrBSeqNFe.Value),
  'AND nItem=' + Geral.FF0(QrErrInItem.Value),
  '']);
end;

procedure TFmNFeLoad_Inn.QrErrIBeforeClose(DataSet: TDataSet);
begin
  QrErrV.Close;
end;

procedure TFmNFeLoad_Inn.QrItsIAfterScroll(DataSet: TDataSet);
begin
  QrItsIDi.Close;
  QrItsIDi.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsIDi.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsIDi.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsIDi, DModG.MyPID_DB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsM, DModG.MyPID_DB, [
  'SELECT ',
  'nItem, vTotTrib',
  'FROM _nfe_m_',
  'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
  'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  '']);
  //
(*
  QrItsN.Close;
  QrItsN.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsN.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  UnDmkDAC_PF.AbreQuery(QrItsN, DModG.MyPID_DB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsN, DModG.MyPID_DB, [
  'SELECT ',
  'ICMS_Orig, ICMS_CST, ICMS_modBC, ',
  'ICMS_pRedBC, ICMS_vBC, ICMS_pICMS, ',
  'ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST, ',
  'ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST, ',
  'ICMS_vICMSST, ICMS_CSOSN, ICMS_UFST, ',
  'ICMS_pBCOp, ICMS_vBCSTRet, ICMS_vICMSSTRet, ',
  'ICMS_motDesICMS, ICMS_pCredSN, ICMS_vCredICMSSN, ',
  'ICMS_pDif, ICMS_vICMSDif, ICMS_vICMSDeson, ',
  'ICMS_vICMSOp ',
  'FROM _nfe_n_',
  'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
  'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  '']);
  //
  QrItsO.Close;
  QrItsO.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsO.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsO.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsO, DModG.MyPID_DB);
  //
  QrItsP.Close;
  QrItsP.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsP.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsP.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsP, DModG.MyPID_DB);
  //
  QrItsQ.Close;
  QrItsQ.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsQ.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsQ.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsQ, DModG.MyPID_DB);
  //
  QrItsR.Close;
  QrItsR.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsR.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsR.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsR, DModG.MyPID_DB);
  //
  QrItsS.Close;
  QrItsS.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsS.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsS.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsS, DModG.MyPID_DB);
  //
  QrItsT.Close;
  QrItsT.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsT.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsT.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsT, DModG.MyPID_DB);
  //
(*
  QrItsU.Close;
  QrItsU.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsU.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsU.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsU, DModG.MyPID_DB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsU, DModG.MyPID_DB, [
  'SELECT',
  'ISSQN_vBC, ISSQN_vAliq, ISSQN_vISSQN,',
  'ISSQN_cMunFG, ISSQN_cListServ, ISSQN_cSitTrib,',
  'ISSQN_vDeducao, ISSQN_vOutro, ISSQN_vDescIncond,',
  'ISSQN_vDescCond, ISSQN_vISSRet, ISSQN_indISS,',
  'ISSQN_cServico, ISSQN_cMun, ISSQN_cPais,',
  'ISSQN_nProcesso, ISSQN_indIncentivo',
  'FROM _nfe_u_',
  'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
  'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  '']);
  //
(*
  QrItsV.Close;
  QrItsV.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsV.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsV.Params[02].AsInteger := QrItsInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrItsV, DModG.MyPID_DB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsV, DModG.MyPID_DB, [
  'SELECT ',
  'InfAdProd ',
  'FROM _nfe_v_ ',
  'WHERE SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
  'AND SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
  'AND nItem=' + Geral.FF0(QrItsInItem.Value),
  '']);
end;

procedure TFmNFeLoad_Inn.QrItsIDiAfterScroll(DataSet: TDataSet);
begin
  QrItsIDiA.Close;
  QrItsIDiA.Params[00].AsInteger := QrNFeBSeqArq.Value;
  QrItsIDiA.Params[01].AsInteger := QrNFeBSeqNFe.Value;
  QrItsIDiA.Params[02].AsInteger := QrItsInItem.Value;
  QrItsIDiA.Params[03].AsInteger := QrItsIDiControle.Value;
  UnDmkDAC_PF.AbreQuery(QrItsIDiA, DModG.MyPID_DB);
end;

procedure TFmNFeLoad_Inn.QrNFeBAfterScroll(DataSet: TDataSet);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeI, DModG.MyPID_DB, [
  'SELECT i.nItem, i.GraGruX, i.prod_cProd, i.prod_xProd,  ',
  'i.prod_uCom, i.prod_qCom, i.prod_vUnCom, i.prod_vProd, ',
  'i.prod_NCM, i.prod_EXTIPI, i.prod_cEAN, v.InfAdProd, ',
  'n.ICMS_Orig, n.ICMS_CST, o.IPI_cEnq, o.IPI_CST,  ',
  'q.PIS_CST, s.COFINS_CST ',
  'FROM _nfe_i_ i ',
  'LEFT JOIN _nfe_n_ n ON n.SeqArq=i.SeqArq AND n.SeqNFe=i.SeqNFe AND n.nItem=i.nItem ',
  'LEFT JOIN _nfe_o_ o ON o.SeqArq=i.SeqArq AND o.SeqNFe=i.SeqNFe AND o.nItem=i.nItem ',
  'LEFT JOIN _nfe_v_ v ON v.SeqArq=i.SeqArq AND v.SeqNFe=i.SeqNFe AND v.nItem=i.nItem ',
  'LEFT JOIN _nfe_q_ q ON q.SeqArq=i.SeqArq AND q.SeqNFe=i.SeqNFe AND q.nItem=i.nItem ',
  'LEFT JOIN _nfe_s_ s ON s.SeqArq=i.SeqArq AND s.SeqNFe=i.SeqNFe AND s.nItem=i.nItem ',
  'WHERE i.SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
  'AND i.SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeI, DModG.MyPID_DB, [
  'SELECT i.nItem, i.GraGruX, i.prod_cProd, i.prod_xProd,   ',
  'i.prod_uCom, i.prod_qCom, i.prod_vUnCom, i.prod_vProd,  ',
  'i.prod_NCM, i.prod_EXTIPI, i.prod_cEAN, v.InfAdProd,  ',
  'n.ICMS_Orig, n.ICMS_CST, o.IPI_cEnq, o.IPI_CST,   ',
  'q.PIS_CST, s.COFINS_CST, ',
  ' ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED ',
  '  ',
  'FROM _nfe_i_ i  ',
  'LEFT JOIN _nfe_n_ n ON n.SeqArq=i.SeqArq AND n.SeqNFe=i.SeqNFe AND n.nItem=i.nItem  ',
  'LEFT JOIN _nfe_o_ o ON o.SeqArq=i.SeqArq AND o.SeqNFe=i.SeqNFe AND o.nItem=i.nItem  ',
  'LEFT JOIN _nfe_v_ v ON v.SeqArq=i.SeqArq AND v.SeqNFe=i.SeqNFe AND v.nItem=i.nItem  ',
  'LEFT JOIN _nfe_q_ q ON q.SeqArq=i.SeqArq AND q.SeqNFe=i.SeqNFe AND q.nItem=i.nItem  ',
  'LEFT JOIN _nfe_s_ s ON s.SeqArq=i.SeqArq AND s.SeqNFe=i.SeqNFe AND s.nItem=i.nItem  ',
  ' ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=i.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN ' + TMeuDB + '.unidmed    unm ON unm.Codigo=gg1.UnidMed  ',
  ' ',
  'WHERE i.SeqArq=' + Geral.FF0(QrNFeBSeqArq.Value),
  'AND i.SeqNFe=' + Geral.FF0(QrNFeBSeqNFe.Value),
  ' ']);
  //Geral.MB_SQL(Self, QrNFeI);
end;

procedure TFmNFeLoad_Inn.QrNFeBBeforeClose(DataSet: TDataSet);
begin
  QrNFeI.Close;
end;

procedure TFmNFeLoad_Inn.QrNFeX26AfterScroll(DataSet: TDataSet);
begin
  QrNFeX33.Close;
  QrNFeX33.Params[00].AsInteger := QrNFeX26SeqArq.Value;
  QrNFeX33.Params[01].AsInteger := QrNFeX26SeqNFe.Value;
  QrNFeX33.Params[02].AsInteger := QrNFeX26Controle.Value;
  UnDmkDAC_PF.AbreQuery(QrNFeX33, DModG.MyPID_DB);
end;

procedure TFmNFeLoad_Inn.QrNFeX26BeforeClose(DataSet: TDataSet);
begin
  QrNFeX33.Close;
end;

procedure TFmNFeLoad_Inn.Removeratrelamentocomreduzido1Click(Sender: TObject);
const
  GragruX = 0;
  NomeGGX = '';
begin
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_i_', False, [
  'GraGruX', 'NomeGGX'], [
  'SeqArq', 'SeqNFe', 'nItem'], [
  GraGruX, NomeGGX], [
  QrErrBSeqArq.Value, QrErrBSeqNFe.Value, QrErrInItem.Value], False) then
  begin
    UpdateInfoErros();
    AbreCarregados();
  end;
end;

procedure TFmNFeLoad_Inn.SBArquivoClick(Sender: TObject);
var
  IniDir: String;
begin
  IniDir := EdDiretorio.Text;
  if MyObjects.FileOpenDialog(Self, IniDir, '',
  'Selecione o diret�rio', '', [], IniDir) then
  begin
    EdDiretorio.Text := ExtractFileDir(IniDir);
    ImportaNFes();
  end;
end;

procedure TFmNFeLoad_Inn.SbCarregaClick(Sender: TObject);
begin
  ImportaNFes();
end;

procedure TFmNFeLoad_Inn.UpdateInfoErros();
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ SET ');
    DModG.QrUpdPID1.SQL.Add('ErrEmit=-1, ErrDest=-1, ErrTrsp=-1, ErrProd=-1;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_c_ c');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrEmit=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=c.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=c.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND c.CodInfoEmit=0;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_e_ e');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrDest=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=e.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=e.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND e.CodInfoDest=0;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_x_01 x');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrTrsp=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=x.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=x.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND ((x.Transporta_CNPJ<>""');
    DModG.QrUpdPID1.SQL.Add('OR x.Transporta_CPF<>"")');
    DModG.QrUpdPID1.SQL.Add('AND x.CodInfoTrsp=0);');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.SQL.Add('UPDATE _nfe_a_ a, _nfe_i_ i');
    DModG.QrUpdPID1.SQL.Add('SET a.ErrProd=1');
    DModG.QrUpdPID1.SQL.Add('WHERE a.SeqArq=i.SeqArq');
    DModG.QrUpdPID1.SQL.Add('AND a.SeqNFe=i.SeqNFe');
    DModG.QrUpdPID1.SQL.Add('AND i.GraGruX=0;');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.ExecSQL;
    //
   finally
     Screen.Cursor := MyCursor;
  end;
end;

function TFmNFeLoad_Inn.VerificaGGE(): Boolean;
var
  GraGruX, GraGru1, SeqArq, SeqNFe, nItem: Integer;
  NomeGGX: String;
  KlsGGXE: TKindLocSelGGXE;
begin
  Result := False;
  //
  KlsGGXE := TKindLocSelGGXE.klsggxeIndef;
  KlsGGXE := Grade_PF.SelecionaGraGruXDeGraGruE(QrErrBCodInfoEmit.Value,
  QrErrIprod_cProd.Value, QrErrIprod_xProd.Value, QrErrIprod_NCM.Value,
  QrErrIprod_uCom.Value, QrErrIprod_CFOP.Value, GraGruX, GraGru1, NomeGGX);
  if KlsGGXE <> TKindLocSelGGXE.klsggxeIndef then
  begin
    if GraGruX <> 0 then
    begin
      SeqArq  := QrErrBSeqArq.Value;
      SeqNFe  := QrErrBSeqNFe.Value;
      nItem   := QrErrInItem.Value;
      //
      Result := UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_nfe_i_', False, [
      'GraGruX', 'NomeGGX', 'KlsGGXE'], [
      'SeqArq', 'SeqNFe', 'nItem'], [
      GraGruX, NomeGGX, Integer(KlsGGXE)], [
      SeqArq, SeqNFe, nItem], False);
      //
      if Result then
      begin
        Geral.MB_Info(
        'N�o ser� necess�rio cadastrar o produto. Ele foi localizado!');
        //
        UpdateInfoErros();
        AbreCarregados();
      end;
    end;
  end;
end;

end.
