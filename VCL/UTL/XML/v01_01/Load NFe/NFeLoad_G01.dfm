object FmNFeLoad_G01: TFmNFeLoad_G01
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 232
  ClientWidth = 989
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 184
    Width = 989
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 877
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object Panel5: TPanel
    Left = 116
    Top = 52
    Width = 989
    Height = 136
    ParentBackground = False
    TabOrder = 1
    object Label24: TLabel
      Left = 4
      Top = 4
      Width = 39
      Height = 13
      Caption = 'CNPJ...:'
      FocusControl = Edentrega_CNPJ
    end
    object Label26: TLabel
      Left = 4
      Top = 44
      Width = 57
      Height = 13
      Caption = 'Logradouro:'
      FocusControl = Edentrega_xLgr
    end
    object Label27: TLabel
      Left = 428
      Top = 44
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
      FocusControl = Edentrega_nro
    end
    object Label28: TLabel
      Left = 480
      Top = 44
      Width = 64
      Height = 13
      Caption = 'Complemento'
      FocusControl = Edentrega_xCpl
    end
    object Label29: TLabel
      Left = 4
      Top = 84
      Width = 30
      Height = 13
      Caption = 'Bairro:'
      FocusControl = Edentrega_xBairro
    end
    object Label30: TLabel
      Left = 428
      Top = 84
      Width = 141
      Height = 13
      Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
      FocusControl = Edentrega_cMun
    end
    object Label31: TLabel
      Left = 952
      Top = 84
      Width = 17
      Height = 13
      Caption = 'UF:'
      FocusControl = Edentrega_UF
    end
    object Label37: TLabel
      Left = 120
      Top = 4
      Width = 50
      Height = 13
      Caption = '... ou CPF:'
      FocusControl = Edentrega_CPF
    end
    object Edentrega_CNPJ: TdmkEdit
      Left = 4
      Top = 20
      Width = 113
      Height = 21
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtCPFJ_NFe
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_CNPJ'
      UpdCampo = 'entrega_CNPJ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_xLgr: TdmkEdit
      Left = 4
      Top = 60
      Width = 420
      Height = 21
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_xLgr'
      UpdCampo = 'entrega_xLgr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_nro: TdmkEdit
      Left = 428
      Top = 60
      Width = 48
      Height = 21
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_nro'
      UpdCampo = 'entrega_nro'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_xCpl: TdmkEdit
      Left = 480
      Top = 60
      Width = 501
      Height = 21
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_xCpl'
      UpdCampo = 'entrega_xCpl'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_xBairro: TdmkEdit
      Left = 4
      Top = 100
      Width = 420
      Height = 21
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_xBairro'
      UpdCampo = 'entrega_xBairro'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_cMun: TdmkEdit
      Left = 428
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      MaxLength = 7
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'entrega_cMun'
      UpdCampo = 'entrega_cMun'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = Edentrega_cMunChange
    end
    object Edentrega_UF: TdmkEdit
      Left = 952
      Top = 100
      Width = 30
      Height = 21
      ReadOnly = True
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_UF'
      UpdCampo = 'entrega_UF'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_xMun: TdmkEdit
      Left = 484
      Top = 100
      Width = 464
      Height = 21
      ReadOnly = True
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_xMun'
      UpdCampo = 'entrega_xMun'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object Edentrega_CPF: TdmkEdit
      Left = 120
      Top = 20
      Width = 113
      Height = 21
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtCPFJ_NFe
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'entrega_CPF'
      UpdCampo = 'entrega_CPF'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 989
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = -11
    ExplicitWidth = 1000
    object GB_R: TGroupBox
      Left = 941
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 952
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 893
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 904
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
end
