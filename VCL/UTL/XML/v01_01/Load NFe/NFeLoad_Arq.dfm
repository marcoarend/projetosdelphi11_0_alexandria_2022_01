object FmNFeLoad_Arq: TFmNFeLoad_Arq
  Left = 339
  Top = 185
  Caption = 'NFe-LOADD-002 :: Importa'#231#227'o de NF-e de Arquivo'
  ClientHeight = 814
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnAbre: TPanel
    Left = 0
    Top = 720
    Width = 993
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object Label294: TLabel
      Left = 123
      Top = 6
      Width = 120
      Height = 16
      Caption = '..............................'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clSilver
      Font.Height = -14
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label295: TLabel
      Left = 122
      Top = 5
      Width = 120
      Height = 16
      Caption = '..............................'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -14
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 88
      Height = 39
      Caption = '&Abre'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
    end
    object Panel2: TPanel
      Left = 883
      Top = 0
      Width = 110
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 88
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PB1: TProgressBar
      Left = 122
      Top = 24
      Width = 757
      Height = 16
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 90
    Width = 993
    Height = 630
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnCarrega: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 43
      Align = alTop
      TabOrder = 0
      Visible = False
      object Label110: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
      end
      object SBArquivo: TSpeedButton
        Left = 940
        Top = 20
        Width = 21
        Height = 20
        Caption = '...'
        OnClick = SBArquivoClick
      end
      object SbCarrega: TSpeedButton
        Left = 960
        Top = 20
        Width = 21
        Height = 20
        Caption = '>'
        OnClick = SbCarregaClick
      end
      object EdArquivo: TEdit
        Left = 8
        Top = 20
        Width = 930
        Height = 21
        TabOrder = 0
        Text = 
          'C:\Dermatek\Nfe\TERRITORIO\_Destinadas\4113050271786100011055009' +
          '0000000061780170615-nfe.xml'
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 122
      Width = 993
      Height = 508
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' '#193'rvore XML '
        object TreeView1: TTreeView
          Left = 0
          Top = 0
          Width = 985
          Height = 480
          Align = alClient
          Indent = 15
          ReadOnly = True
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Lote'
        ImageIndex = 1
        object Panel37: TPanel
          Left = 0
          Top = 0
          Width = 985
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label254: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
            FocusControl = EdenviNFe_versao
          end
          object Label190: TLabel
            Left = 51
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Lote:'
            FocusControl = EdenviNFe_idLote
          end
          object EdenviNFe_versao: TdmkEdit
            Left = 8
            Top = 20
            Width = 40
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdenviNFe_idLote: TdmkEdit
            Left = 51
            Top = 20
            Width = 99
            Height = 20
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInt64
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object DBGrid11: TDBGrid
          Left = 0
          Top = 48
          Width = 985
          Height = 338
          Align = alClient
          DataSource = DsNFeA
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Sequencial'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'versao'
              Title.Caption = 'Vers'#227'o'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Id'
              Title.Caption = 'Id NF-e'
              Width = 250
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'NF-e'
        ImageIndex = 2
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 985
          Height = 480
          ActivePage = TabSheet9
          Align = alClient
          TabOrder = 0
          object TabSheet4: TTabSheet
            Caption = ' Cabe'#231'alho'
            ImageIndex = 1
            object Panel22: TPanel
              Left = 0
              Top = 47
              Width = 977
              Height = 202
              Align = alTop
              ParentBackground = False
              TabOrder = 1
              object Label195: TLabel
                Left = 142
                Top = 4
                Width = 109
                Height = 13
                Caption = 'Natureza da opera'#231#227'o:'
              end
              object Label196: TLabel
                Left = 744
                Top = 4
                Width = 165
                Height = 13
                Caption = 'Indicador de forma de pagamento: '
              end
              object Label197: TLabel
                Left = 4
                Top = 43
                Width = 27
                Height = 13
                Caption = 'Mod.:'
              end
              object Label198: TLabel
                Left = 35
                Top = 43
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
              end
              object Label199: TLabel
                Left = 71
                Top = 43
                Width = 32
                Height = 13
                Caption = 'N'#186' NF:'
              end
              object Label200: TLabel
                Left = 154
                Top = 43
                Width = 82
                Height = 13
                Caption = 'Data da emiss'#227'o:'
              end
              object Label201: TLabel
                Left = 240
                Top = 43
                Width = 151
                Height = 13
                Caption = 'Data e hora da sa'#237'da / entrada:'
              end
              object Label202: TLabel
                Left = 394
                Top = 43
                Width = 90
                Height = 13
                Caption = 'Tipo do doc. fiscal:'
              end
              object Label203: TLabel
                Left = 488
                Top = 43
                Width = 178
                Height = 13
                Caption = 'C'#243'digo do munic'#237'pio do fato gerador: '
              end
              object Label204: TLabel
                Left = 866
                Top = 43
                Width = 89
                Height = 13
                Caption = 'Tipo de impress'#227'o:'
              end
              object Label205: TLabel
                Left = 4
                Top = 82
                Width = 138
                Height = 13
                Caption = 'Forma de impress'#227'o da NF-e:'
                FocusControl = Edide_tpEmis
              end
              object Label206: TLabel
                Left = 4
                Top = 122
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label207: TLabel
                Left = 27
                Top = 122
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
              end
              object Label208: TLabel
                Left = 158
                Top = 122
                Width = 148
                Height = 13
                Caption = 'Finalidade de emiss'#227'o da NF-e:'
              end
              object Label209: TLabel
                Left = 307
                Top = 122
                Width = 144
                Height = 13
                Caption = 'Processo de emiss'#227'o da NF-e:'
              end
              object Label210: TLabel
                Left = 818
                Top = 122
                Width = 153
                Height = 13
                Caption = 'Vers'#227'o do processo de emiss'#227'o:'
              end
              object Label193: TLabel
                Left = 4
                Top = 4
                Width = 48
                Height = 13
                Caption = 'UF (DTB):'
              end
              object Label194: TLabel
                Left = 75
                Top = 4
                Width = 58
                Height = 13
                Caption = 'C'#243'd. chave:'
              end
              object Label259: TLabel
                Left = 4
                Top = 162
                Width = 193
                Height = 13
                Caption = 'Data / hora de entrada em conting'#234'ncia:'
                FocusControl = Edide_dhCont_TXT
              end
              object Label260: TLabel
                Left = 201
                Top = 162
                Width = 193
                Height = 13
                Caption = 'Justificativa da entrada em conting'#234'ncia:'
                FocusControl = Edide_xJust
              end
              object Edide_tpEmis: TDBEdit
                Left = 4
                Top = 98
                Width = 25
                Height = 21
                DataField = 'ide_tpEmis'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 18
              end
              object Edide_xIndPag: TDBEdit
                Left = 772
                Top = 20
                Width = 197
                Height = 21
                DataField = 'ide_xIndPag'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 5
              end
              object Edide_tpNF_TXT: TDBEdit
                Left = 414
                Top = 59
                Width = 72
                Height = 21
                DataField = 'ide_xTpNF'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 13
              end
              object Edide_cMunFG_TXT: TDBEdit
                Left = 539
                Top = 59
                Width = 323
                Height = 21
                DataField = 'ide_xMunFG'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 15
              end
              object Edide_TpImp_TXT: TDBEdit
                Left = 893
                Top = 59
                Width = 75
                Height = 21
                DataField = 'ide_xTpImp'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 17
              end
              object Edide_tpEmis_TXT: TDBEdit
                Left = 31
                Top = 98
                Width = 938
                Height = 21
                DataField = 'ide_xTpEmis'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 19
              end
              object Edide_xAmb: TDBEdit
                Left = 55
                Top = 138
                Width = 99
                Height = 21
                DataField = 'ide_xAmb'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 22
              end
              object Edide_finNFe_TXT: TDBEdit
                Left = 181
                Top = 138
                Width = 123
                Height = 21
                DataField = 'ide_xFInNFe'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 24
              end
              object Edide_procEmi_TXT: TDBEdit
                Left = 334
                Top = 138
                Width = 478
                Height = 21
                DataField = 'ide_xProcEmi'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 26
              end
              object Edide_xUF: TDBEdit
                Left = 39
                Top = 20
                Width = 33
                Height = 21
                DataField = 'ide_xUF'
                DataSource = DsNFeB
                ReadOnly = True
                TabOrder = 1
              end
              object Edide_cUF: TDBEdit
                Left = 4
                Top = 20
                Width = 33
                Height = 21
                DataField = 'ide_cUF'
                DataSource = DsNFeB
                TabOrder = 0
              end
              object Edide_cNF: TDBEdit
                Left = 75
                Top = 20
                Width = 63
                Height = 21
                DataField = 'ide_cNF'
                DataSource = DsNFeB
                TabOrder = 2
              end
              object Edide_natOp: TDBEdit
                Left = 142
                Top = 20
                Width = 599
                Height = 21
                DataField = 'ide_natOp'
                DataSource = DsNFeB
                TabOrder = 3
              end
              object Edide_indPag: TDBEdit
                Left = 744
                Top = 20
                Width = 24
                Height = 21
                DataField = 'ide_indPag'
                DataSource = DsNFeB
                TabOrder = 4
              end
              object Edide_mod: TDBEdit
                Left = 4
                Top = 59
                Width = 27
                Height = 21
                DataField = 'ide_mod'
                DataSource = DsNFeB
                TabOrder = 6
              end
              object Edide_serie: TDBEdit
                Left = 35
                Top = 59
                Width = 32
                Height = 21
                DataField = 'ide_serie'
                DataSource = DsNFeB
                TabOrder = 7
              end
              object Edide_nNF: TDBEdit
                Left = 71
                Top = 59
                Width = 79
                Height = 21
                DataField = 'ide_nNF'
                DataSource = DsNFeB
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 8
              end
              object Edide_dEmi: TDBEdit
                Left = 154
                Top = 59
                Width = 82
                Height = 21
                DataField = 'ide_dEmi'
                DataSource = DsNFeB
                TabOrder = 9
              end
              object Edide_dSaiEnt: TDBEdit
                Left = 240
                Top = 59
                Width = 104
                Height = 21
                DataField = 'ide_dSaiEnt'
                DataSource = DsNFeB
                TabOrder = 10
              end
              object Edide_hSaiEnt: TDBEdit
                Left = 342
                Top = 59
                Width = 48
                Height = 21
                DataField = 'ide_hSaiEnt'
                DataSource = DsNFeB
                TabOrder = 11
              end
              object Edide_tpNF: TDBEdit
                Left = 394
                Top = 59
                Width = 20
                Height = 21
                DataField = 'ide_tpNF'
                DataSource = DsNFeB
                TabOrder = 12
              end
              object Edide_cMunFG: TDBEdit
                Left = 488
                Top = 59
                Width = 51
                Height = 21
                DataField = 'ide_cMunFG'
                DataSource = DsNFeB
                TabOrder = 14
              end
              object Edide_tpImp: TDBEdit
                Left = 866
                Top = 59
                Width = 24
                Height = 21
                DataField = 'ide_tpImp'
                DataSource = DsNFeB
                TabOrder = 16
              end
              object Edide_tpAmb: TDBEdit
                Left = 27
                Top = 138
                Width = 24
                Height = 21
                DataField = 'ide_tpAmb'
                DataSource = DsNFeB
                TabOrder = 21
              end
              object Edide_cDV: TDBEdit
                Left = 4
                Top = 138
                Width = 20
                Height = 21
                DataField = 'ide_cDV'
                DataSource = DsNFeB
                TabOrder = 20
              end
              object Edide_finNFe: TDBEdit
                Left = 158
                Top = 138
                Width = 20
                Height = 21
                DataField = 'ide_finNFe'
                DataSource = DsNFeB
                TabOrder = 23
              end
              object Edide_procEmi: TDBEdit
                Left = 307
                Top = 138
                Width = 24
                Height = 21
                DataField = 'ide_procEmi'
                DataSource = DsNFeB
                TabOrder = 25
              end
              object Edide_verProc: TDBEdit
                Left = 815
                Top = 138
                Width = 152
                Height = 21
                DataField = 'ide_verProc'
                DataSource = DsNFeB
                TabOrder = 27
              end
              object Edide_dhCont_TXT: TDBEdit
                Left = 4
                Top = 178
                Width = 194
                Height = 21
                DataField = 'ide_dhCont_TXT'
                DataSource = DsNFeB
                TabOrder = 28
              end
              object Edide_xJust: TDBEdit
                Left = 201
                Top = 178
                Width = 765
                Height = 21
                DataField = 'ide_xJust'
                DataSource = DsNFeB
                TabOrder = 29
              end
            end
            object Panel21: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 47
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label186: TLabel
                Left = 75
                Top = 4
                Width = 29
                Height = 13
                Caption = 'FatID:'
              end
              object Label187: TLabel
                Left = 8
                Top = 4
                Width = 45
                Height = 13
                Caption = 'Seq NFe:'
              end
              object Label188: TLabel
                Left = 209
                Top = 4
                Width = 44
                Height = 13
                Caption = 'Empresa:'
              end
              object Label189: TLabel
                Left = 256
                Top = 4
                Width = 26
                Height = 13
                Caption = 'IDCtrl'
                Enabled = False
              end
              object Label191: TLabel
                Left = 323
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
              end
              object Label192: TLabel
                Left = 366
                Top = 4
                Width = 264
                Height = 13
                Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
              end
              object Label255: TLabel
                Left = 142
                Top = 4
                Width = 40
                Height = 13
                Caption = 'FatNum:'
                FocusControl = EdFatNum
              end
              object EdFatID: TDBEdit
                Left = 75
                Top = 20
                Width = 63
                Height = 21
                DataField = 'FatID'
                DataSource = DsNFeA
                TabOrder = 1
              end
              object EdSeqNFe: TDBEdit
                Left = 8
                Top = 20
                Width = 63
                Height = 21
                DataField = 'SeqNFe'
                DataSource = DsNFeA
                TabOrder = 0
              end
              object EdFatNum: TDBEdit
                Left = 142
                Top = 20
                Width = 63
                Height = 21
                DataField = 'FatNum'
                DataSource = DsNFeA
                TabOrder = 2
              end
              object EdEmpresa: TDBEdit
                Left = 209
                Top = 20
                Width = 43
                Height = 21
                DataField = 'Empresa'
                DataSource = DsNFeA
                TabOrder = 3
              end
              object EdIDCtrl: TDBEdit
                Left = 256
                Top = 20
                Width = 63
                Height = 21
                DataField = 'IDCtrl'
                DataSource = DsNFeA
                TabOrder = 4
              end
              object Edversao: TDBEdit
                Left = 323
                Top = 20
                Width = 39
                Height = 21
                DataField = 'versao'
                DataSource = DsNFeA
                TabOrder = 5
              end
              object EdId: TDBEdit
                Left = 366
                Top = 20
                Width = 268
                Height = 21
                DataField = 'Id'
                DataSource = DsNFeA
                TabOrder = 6
              end
            end
            object Panel39: TPanel
              Left = 0
              Top = 249
              Width = 977
              Height = 109
              Align = alClient
              ParentBackground = False
              TabOrder = 2
              object dmkLabelRotate2: TdmkLabelRotate
                Left = 1
                Top = 1
                Width = 16
                Height = 107
                Angle = ag90
                Caption = 'NFs 1 / 1A Referenciadas'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                Align = alLeft
              end
              object DBGrid12: TDBGrid
                Left = 17
                Top = 1
                Width = 959
                Height = 107
                Align = alClient
                DataSource = DsNFeB12a
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
              end
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Emitente'
            ImageIndex = 2
            object Panel56: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 48
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label359: TLabel
                Left = 4
                Top = 4
                Width = 44
                Height = 13
                Caption = 'Emitente:'
              end
              object EdCodInfoEmit: TDBEdit
                Left = 4
                Top = 20
                Width = 55
                Height = 21
                DataField = 'CodInfoEmit'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 0
              end
              object EdNomeEmit: TDBEdit
                Left = 63
                Top = 20
                Width = 903
                Height = 21
                DataField = 'NomeEmit'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 1
              end
            end
            object Panel16: TPanel
              Left = 0
              Top = 48
              Width = 977
              Height = 310
              Align = alClient
              ParentBackground = False
              TabOrder = 1
              object Label120: TLabel
                Left = 4
                Top = 4
                Width = 42
                Height = 13
                Caption = 'CNPJ ...:'
                FocusControl = Edemit_CNPJ
              end
              object Label121: TLabel
                Left = 232
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = Edemit_xNome
              end
              object Label122: TLabel
                Left = 741
                Top = 4
                Width = 74
                Height = 13
                Caption = 'Nome Fantasia:'
                FocusControl = Edemit_xFant
              end
              object Label123: TLabel
                Left = 4
                Top = 43
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = Edemit_xLgr
              end
              object Label124: TLabel
                Left = 422
                Top = 43
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = Edemit_nro
              end
              object Label125: TLabel
                Left = 473
                Top = 43
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = Edemit_xCpl
              end
              object Label126: TLabel
                Left = 4
                Top = 82
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = Edemit_xBairro
              end
              object Label127: TLabel
                Left = 422
                Top = 82
                Width = 141
                Height = 13
                Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                FocusControl = Edemit_cMun
              end
              object Label128: TLabel
                Left = 4
                Top = 122
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = Edemit_UF
              end
              object Label129: TLabel
                Left = 35
                Top = 122
                Width = 24
                Height = 13
                Caption = 'CEP:'
                FocusControl = Edemit_CEP
              end
              object Label130: TLabel
                Left = 98
                Top = 122
                Width = 113
                Height = 13
                Caption = 'C'#243'digo e nome do pa'#237's:'
                FocusControl = Edemit_cPais
              end
              object Label131: TLabel
                Left = 831
                Top = 122
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = Edemit_fone
              end
              object Label132: TLabel
                Left = 4
                Top = 162
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                FocusControl = Edemit_IE
              end
              object Label133: TLabel
                Left = 138
                Top = 162
                Width = 127
                Height = 13
                Caption = 'I.E. do Substituto tribut'#225'rio:'
                FocusControl = Edemit_IEST
              end
              object Label134: TLabel
                Left = 272
                Top = 162
                Width = 94
                Height = 13
                Caption = 'Inscri'#231#227'o Municipal:'
                FocusControl = Edemit_IM
              end
              object Label135: TLabel
                Left = 406
                Top = 162
                Width = 62
                Height = 13
                Caption = 'CNAE Fiscal:'
              end
              object Label136: TLabel
                Left = 118
                Top = 4
                Width = 50
                Height = 13
                Caption = '... ou CPF:'
                FocusControl = Edemit_CPF
              end
              object Label7: TLabel
                Left = 737
                Top = 162
                Width = 25
                Height = 13
                Caption = 'CRT:'
              end
              object Edemit_CNPJ: TDBEdit
                Left = 4
                Top = 20
                Width = 110
                Height = 21
                DataField = 'emit_CNPJ'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 0
              end
              object Edemit_xNome: TDBEdit
                Left = 232
                Top = 20
                Width = 506
                Height = 21
                DataField = 'emit_xNome'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 2
              end
              object Edemit_xFant: TDBEdit
                Left = 741
                Top = 20
                Width = 225
                Height = 21
                DataField = 'emit_xFant'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 3
              end
              object Edemit_xLgr: TDBEdit
                Left = 4
                Top = 59
                Width = 414
                Height = 21
                DataField = 'emit_xLgr'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 4
              end
              object Edemit_nro: TDBEdit
                Left = 422
                Top = 59
                Width = 47
                Height = 21
                DataField = 'emit_nro'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 5
              end
              object Edemit_xCpl: TDBEdit
                Left = 473
                Top = 59
                Width = 493
                Height = 21
                DataField = 'emit_xCpl'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 6
              end
              object Edemit_xBairro: TDBEdit
                Left = 4
                Top = 98
                Width = 414
                Height = 21
                DataField = 'emit_xBairro'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 7
              end
              object Edemit_cMun: TDBEdit
                Left = 422
                Top = 98
                Width = 55
                Height = 21
                DataField = 'emit_cMun'
                DataSource = DsNFeC
                MaxLength = 7
                ReadOnly = True
                TabOrder = 8
              end
              object Edemit_UF: TDBEdit
                Left = 4
                Top = 138
                Width = 30
                Height = 21
                DataField = 'emit_UF'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 10
              end
              object Edemit_CEP: TDBEdit
                Left = 35
                Top = 138
                Width = 60
                Height = 21
                DataField = 'emit_CEP'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 11
              end
              object Edemit_cPais: TDBEdit
                Left = 98
                Top = 138
                Width = 48
                Height = 21
                DataField = 'emit_cPais'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 12
              end
              object Edemit_fone: TDBEdit
                Left = 830
                Top = 138
                Width = 136
                Height = 21
                DataField = 'emit_fone'
                DataSource = DsNFeC
                TabOrder = 14
              end
              object Edemit_IE: TDBEdit
                Left = 4
                Top = 178
                Width = 130
                Height = 21
                DataField = 'emit_IE'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 15
              end
              object Edemit_IEST: TDBEdit
                Left = 138
                Top = 178
                Width = 130
                Height = 21
                DataField = 'emit_IEST'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 16
              end
              object Edemit_IM: TDBEdit
                Left = 272
                Top = 178
                Width = 130
                Height = 21
                DataField = 'emit_IM'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 17
              end
              object Edemit_xMun: TDBEdit
                Left = 477
                Top = 98
                Width = 489
                Height = 21
                DataField = 'emit_xMun'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 9
              end
              object Edemit_xPais: TDBEdit
                Left = 146
                Top = 138
                Width = 682
                Height = 21
                DataField = 'emit_xPais'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 13
              end
              object Edemit_CPF: TDBEdit
                Left = 118
                Top = 20
                Width = 111
                Height = 21
                DataField = 'emit_CPF'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 1
              end
              object Edemit_CNAE: TDBEdit
                Left = 406
                Top = 178
                Width = 51
                Height = 21
                DataField = 'emit_CNAE'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 18
              end
              object Edemit_xCNAE: TDBEdit
                Left = 457
                Top = 178
                Width = 277
                Height = 21
                DataField = 'emit_xCNAE'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 19
              end
              object Edemit_CRT: TDBEdit
                Left = 737
                Top = 178
                Width = 23
                Height = 21
                DataField = 'emit_CRT'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 20
              end
              object Edemit_xCRT: TDBEdit
                Left = 760
                Top = 178
                Width = 206
                Height = 21
                DataField = 'emit_xCRT'
                DataSource = DsNFeC
                ReadOnly = True
                TabOrder = 21
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Fisco emitente'
            ImageIndex = 3
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 358
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label8: TLabel
                Left = 8
                Top = 8
                Width = 30
                Height = 13
                Caption = 'CNPJ:'
                FocusControl = Edavulsa_CNPJ
              end
              object Label9: TLabel
                Left = 8
                Top = 47
                Width = 48
                Height = 13
                Caption = 'Matr'#237'cula:'
                FocusControl = Edavulsa_nDAR
              end
              object Label10: TLabel
                Left = 8
                Top = 90
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = Edavulsa_fone
              end
              object Label11: TLabel
                Left = 500
                Top = 8
                Width = 279
                Height = 13
                Caption = 'N'#250'mero do Documento de Arrecada'#231#227'o de Receita (DAR):'
                FocusControl = Edavulsa_matr
              end
              object Label12: TLabel
                Left = 350
                Top = 90
                Width = 128
                Height = 13
                Caption = 'Reparti'#231#227'o Fiscal emitente:'
                FocusControl = Edavulsa_repEmi
              end
              object Label14: TLabel
                Left = 126
                Top = 8
                Width = 75
                Height = 13
                Caption = #211'rg'#227'o emitente:'
                FocusControl = Edavulsa_xOrgao
              end
              object Label15: TLabel
                Left = 492
                Top = 47
                Width = 37
                Height = 13
                Caption = 'Agente:'
                FocusControl = Edavulsa_xAgente
              end
              object Label16: TLabel
                Left = 106
                Top = 90
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = Edavulsa_UF
              end
              object Label17: TLabel
                Left = 248
                Top = 90
                Width = 91
                Height = 13
                Caption = 'Valor total do DAR:'
                FocusControl = Edavulsa_vDAR
              end
              object Label20: TLabel
                Left = 134
                Top = 90
                Width = 82
                Height = 13
                Caption = 'Data da emiss'#227'o:'
              end
              object Label21: TLabel
                Left = 854
                Top = 90
                Width = 112
                Height = 13
                Caption = 'Data de pagto do DAR:'
              end
              object Edavulsa_CNPJ: TDBEdit
                Left = 8
                Top = 24
                Width = 114
                Height = 21
                DataField = 'avulsa_CNPJ'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 0
              end
              object Edavulsa_nDAR: TDBEdit
                Left = 500
                Top = 24
                Width = 470
                Height = 21
                DataField = 'avulsa_nDAR'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 2
              end
              object Edavulsa_fone: TDBEdit
                Left = 8
                Top = 106
                Width = 94
                Height = 21
                DataField = 'avulsa_fone'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 5
              end
              object Edavulsa_matr: TDBEdit
                Left = 8
                Top = 63
                Width = 480
                Height = 21
                DataField = 'avulsa_matr'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 3
              end
              object Edavulsa_repEmi: TDBEdit
                Left = 350
                Top = 106
                Width = 500
                Height = 21
                DataField = 'avulsa_repEmi'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 9
              end
              object Edavulsa_xOrgao: TDBEdit
                Left = 126
                Top = 24
                Width = 372
                Height = 21
                DataField = 'avulsa_xOrgao'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 1
              end
              object Edavulsa_xAgente: TDBEdit
                Left = 492
                Top = 63
                Width = 478
                Height = 21
                DataField = 'avulsa_xAgente'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 4
              end
              object Edavulsa_UF: TDBEdit
                Left = 106
                Top = 106
                Width = 24
                Height = 21
                DataField = 'avulsa_UF'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 6
              end
              object Edavulsa_vDAR: TDBEdit
                Left = 248
                Top = 106
                Width = 98
                Height = 21
                DataField = 'avulsa_vDAR'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 8
              end
              object TPavulsa_dEmi: TDBEdit
                Left = 134
                Top = 106
                Width = 108
                Height = 21
                DataField = 'avulsa_dEmi'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 7
              end
              object TPavulsa_dPag: TDBEdit
                Left = 854
                Top = 106
                Width = 115
                Height = 21
                DataField = 'avulsa_dPag'
                DataSource = DsNFeD
                ReadOnly = True
                TabOrder = 10
              end
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Destinat'#225'rio'
            ImageIndex = 4
            object Panel42: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 48
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label305: TLabel
                Left = 4
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Destinat'#225'rio:'
              end
              object EdCodInfoDest: TDBEdit
                Left = 4
                Top = 20
                Width = 55
                Height = 21
                DataField = 'CodInfoDest'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
              end
              object EdNomeDest: TDBEdit
                Left = 63
                Top = 20
                Width = 903
                Height = 21
                DataField = 'NomeDest'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
              end
            end
            object Panel15: TPanel
              Left = 0
              Top = 48
              Width = 977
              Height = 310
              Align = alClient
              ParentBackground = False
              TabOrder = 1
              object Label13: TLabel
                Left = 4
                Top = 4
                Width = 39
                Height = 13
                Caption = 'CNPJ...:'
                FocusControl = Eddest_CNPJ
              end
              object Label18: TLabel
                Left = 232
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = Eddest_xNome
              end
              object Label19: TLabel
                Left = 4
                Top = 43
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = Eddest_xLgr
              end
              object Label109: TLabel
                Left = 422
                Top = 43
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = Eddest_nro
              end
              object Label22: TLabel
                Left = 473
                Top = 43
                Width = 64
                Height = 13
                Caption = 'Complemento'
                FocusControl = Eddest_xCpl
              end
              object Label111: TLabel
                Left = 4
                Top = 82
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = Eddest_xBairro
              end
              object Label112: TLabel
                Left = 422
                Top = 82
                Width = 141
                Height = 13
                Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                FocusControl = Eddest_cMun
              end
              object Label113: TLabel
                Left = 4
                Top = 122
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = Eddest_UF
              end
              object Label114: TLabel
                Left = 35
                Top = 122
                Width = 24
                Height = 13
                Caption = 'CEP:'
                FocusControl = Eddest_CEP
              end
              object Label115: TLabel
                Left = 98
                Top = 122
                Width = 113
                Height = 13
                Caption = 'C'#243'digo e nome do pa'#237's:'
                FocusControl = Eddest_cPais
              end
              object Label116: TLabel
                Left = 630
                Top = 122
                Width = 45
                Height = 13
                Caption = 'Telefone:'
                FocusControl = Eddest_fone
              end
              object Label117: TLabel
                Left = 725
                Top = 122
                Width = 90
                Height = 13
                Caption = 'Inscri'#231#227'o Estadual:'
                FocusControl = Eddest_IE
              end
              object Label118: TLabel
                Left = 846
                Top = 122
                Width = 116
                Height = 13
                Caption = 'Inscri'#231#227'o na SUFRAMA:'
                FocusControl = Eddest_ISUF
              end
              object Label119: TLabel
                Left = 118
                Top = 4
                Width = 50
                Height = 13
                Caption = '... ou CPF:'
                FocusControl = Eddest_CPF
              end
              object Label23: TLabel
                Left = 4
                Top = 162
                Width = 31
                Height = 13
                Caption = 'E-mail:'
                FocusControl = Eddest_email
              end
              object Eddest_CNPJ: TDBEdit
                Left = 4
                Top = 20
                Width = 111
                Height = 21
                DataField = 'dest_CNPJ'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
              end
              object Eddest_xNome: TDBEdit
                Left = 232
                Top = 20
                Width = 612
                Height = 21
                DataField = 'dest_xNome'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
              end
              object Eddest_xLgr: TDBEdit
                Left = 4
                Top = 59
                Width = 414
                Height = 21
                DataField = 'dest_xLgr'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
              end
              object Eddest_nro: TDBEdit
                Left = 422
                Top = 59
                Width = 47
                Height = 21
                DataField = 'dest_nro'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 4
              end
              object Eddest_xCpl: TDBEdit
                Left = 473
                Top = 59
                Width = 493
                Height = 21
                DataField = 'dest_xCpl'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 5
              end
              object Eddest_xBairro: TDBEdit
                Left = 4
                Top = 98
                Width = 414
                Height = 21
                DataField = 'dest_xBairro'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 6
              end
              object Eddest_cMun: TDBEdit
                Left = 422
                Top = 98
                Width = 55
                Height = 21
                DataField = 'dest_cMun'
                DataSource = DsNFeE
                MaxLength = 7
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 7
              end
              object Eddest_UF: TDBEdit
                Left = 4
                Top = 138
                Width = 30
                Height = 21
                DataField = 'dest_UF'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 9
              end
              object Eddest_CEP: TDBEdit
                Left = 35
                Top = 138
                Width = 60
                Height = 21
                DataField = 'dest_CEP'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 10
              end
              object Eddest_cPais: TDBEdit
                Left = 98
                Top = 138
                Width = 48
                Height = 21
                DataField = 'dest_cPais'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 11
              end
              object Eddest_fone: TDBEdit
                Left = 629
                Top = 138
                Width = 91
                Height = 21
                DataField = 'dest_fone'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 13
              end
              object Eddest_IE: TDBEdit
                Left = 725
                Top = 138
                Width = 118
                Height = 21
                DataField = 'dest_IE'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 14
              end
              object Eddest_ISUF: TDBEdit
                Left = 846
                Top = 138
                Width = 119
                Height = 21
                DataField = 'dest_ISUF'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 15
              end
              object Eddest_xMun: TDBEdit
                Left = 477
                Top = 98
                Width = 489
                Height = 21
                DataField = 'dest_xMun'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 8
              end
              object Eddest_xPais: TDBEdit
                Left = 146
                Top = 138
                Width = 480
                Height = 21
                DataField = 'dest_xPais'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 12
              end
              object Eddest_CPF: TDBEdit
                Left = 122
                Top = 20
                Width = 112
                Height = 21
                DataField = 'dest_CPF'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 1
              end
              object Eddest_email: TDBEdit
                Left = 4
                Top = 178
                Width = 962
                Height = 21
                DataField = 'dest_email'
                DataSource = DsNFeE
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 16
              end
              object BtCadDest: TBitBtn
                Left = 846
                Top = 8
                Width = 119
                Height = 39
                Caption = 'Cadastrar entidade'
                Enabled = False
                TabOrder = 17
                OnClick = BtCadDestClick
              end
            end
          end
          object TabSheet8: TTabSheet
            Caption = 'Retidada / Entrega'
            ImageIndex = 5
            object GroupBox13: TGroupBox
              Left = 0
              Top = 0
              Width = 977
              Height = 146
              Align = alTop
              Caption = 'Local de Retirada: '
              TabOrder = 0
              object Panel6: TPanel
                Left = 2
                Top = 14
                Width = 972
                Height = 131
                Align = alClient
                ParentBackground = False
                TabOrder = 0
                object Label25: TLabel
                  Left = 4
                  Top = 4
                  Width = 39
                  Height = 13
                  Caption = 'CNPJ...:'
                  FocusControl = Edretirada_CNPJ
                end
                object Label32: TLabel
                  Left = 4
                  Top = 43
                  Width = 57
                  Height = 13
                  Caption = 'Logradouro:'
                  FocusControl = Edretirada_xLgr
                end
                object Label33: TLabel
                  Left = 422
                  Top = 43
                  Width = 40
                  Height = 13
                  Caption = 'N'#250'mero:'
                  FocusControl = Edretirada_nro
                end
                object Label34: TLabel
                  Left = 473
                  Top = 43
                  Width = 64
                  Height = 13
                  Caption = 'Complemento'
                  FocusControl = Edretirada_xCpl
                end
                object Label35: TLabel
                  Left = 4
                  Top = 82
                  Width = 30
                  Height = 13
                  Caption = 'Bairro:'
                  FocusControl = Edretirada_xBairro
                end
                object Label36: TLabel
                  Left = 422
                  Top = 82
                  Width = 141
                  Height = 13
                  Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                  FocusControl = Edretirada_cMun
                end
                object Label38: TLabel
                  Left = 938
                  Top = 82
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                  FocusControl = Edretirada_UF
                end
                object Label39: TLabel
                  Left = 118
                  Top = 4
                  Width = 50
                  Height = 13
                  Caption = '... ou CPF:'
                  FocusControl = Edretirada_CPF
                end
                object Edretirada_CNPJ: TDBEdit
                  Left = 4
                  Top = 20
                  Width = 111
                  Height = 21
                  DataField = 'retirada_CNPJ'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 0
                end
                object Edretirada_xLgr: TDBEdit
                  Left = 4
                  Top = 59
                  Width = 414
                  Height = 21
                  DataField = 'retirada_xLgr'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 2
                end
                object Edretirada_nro: TDBEdit
                  Left = 422
                  Top = 59
                  Width = 47
                  Height = 21
                  DataField = 'retirada_nro'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 3
                end
                object Edretirada_xCpl: TDBEdit
                  Left = 473
                  Top = 59
                  Width = 493
                  Height = 21
                  DataField = 'retirada_xCpl'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 4
                end
                object Edretirada_xBairro: TDBEdit
                  Left = 4
                  Top = 98
                  Width = 414
                  Height = 21
                  DataField = 'retirada_xBairro'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 5
                end
                object Edretirada_cMun: TDBEdit
                  Left = 422
                  Top = 98
                  Width = 55
                  Height = 21
                  DataField = 'retirada_cMun'
                  DataSource = DsNFeF
                  MaxLength = 7
                  ReadOnly = True
                  TabOrder = 6
                end
                object Edretirada_UF: TDBEdit
                  Left = 938
                  Top = 98
                  Width = 29
                  Height = 21
                  DataField = 'retirada_UF'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 8
                end
                object Edretirada_xMun: TDBEdit
                  Left = 477
                  Top = 98
                  Width = 457
                  Height = 21
                  DataField = 'retirada_xMun'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 7
                end
                object Edretirada_CPF: TDBEdit
                  Left = 118
                  Top = 20
                  Width = 112
                  Height = 21
                  DataField = 'retirada_CPF'
                  DataSource = DsNFeF
                  ReadOnly = True
                  TabOrder = 1
                end
              end
            end
            object GroupBox14: TGroupBox
              Left = 0
              Top = 146
              Width = 977
              Height = 148
              Align = alTop
              Caption = ' Local de Entrega: '
              TabOrder = 1
              object Panel5: TPanel
                Left = 2
                Top = 14
                Width = 972
                Height = 131
                Align = alClient
                ParentBackground = False
                TabOrder = 0
                object Label24: TLabel
                  Left = 4
                  Top = 4
                  Width = 39
                  Height = 13
                  Caption = 'CNPJ...:'
                  FocusControl = Edentrega_CNPJ
                end
                object Label26: TLabel
                  Left = 4
                  Top = 43
                  Width = 57
                  Height = 13
                  Caption = 'Logradouro:'
                  FocusControl = Edentrega_xLgr
                end
                object Label27: TLabel
                  Left = 422
                  Top = 43
                  Width = 40
                  Height = 13
                  Caption = 'N'#250'mero:'
                  FocusControl = Edentrega_nro
                end
                object Label28: TLabel
                  Left = 473
                  Top = 43
                  Width = 64
                  Height = 13
                  Caption = 'Complemento'
                  FocusControl = Edentrega_xCpl
                end
                object Label29: TLabel
                  Left = 4
                  Top = 82
                  Width = 30
                  Height = 13
                  Caption = 'Bairro:'
                  FocusControl = Edentrega_xBairro
                end
                object Label30: TLabel
                  Left = 422
                  Top = 82
                  Width = 141
                  Height = 13
                  Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
                  FocusControl = Edentrega_cMun
                end
                object Label31: TLabel
                  Left = 938
                  Top = 82
                  Width = 17
                  Height = 13
                  Caption = 'UF:'
                  FocusControl = Edentrega_UF
                end
                object Label37: TLabel
                  Left = 118
                  Top = 4
                  Width = 50
                  Height = 13
                  Caption = '... ou CPF:'
                  FocusControl = Edentrega_CPF
                end
                object Edentrega_CNPJ: TDBEdit
                  Left = 4
                  Top = 20
                  Width = 111
                  Height = 21
                  DataField = 'entrega_CNPJ'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 0
                end
                object Edentrega_xLgr: TDBEdit
                  Left = 4
                  Top = 59
                  Width = 414
                  Height = 21
                  DataField = 'entrega_xLgr'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 2
                end
                object Edentrega_nro: TDBEdit
                  Left = 422
                  Top = 59
                  Width = 47
                  Height = 21
                  DataField = 'entrega_nro'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 3
                end
                object Edentrega_xCpl: TDBEdit
                  Left = 473
                  Top = 59
                  Width = 493
                  Height = 21
                  DataField = 'entrega_xCpl'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 4
                end
                object Edentrega_xBairro: TDBEdit
                  Left = 4
                  Top = 98
                  Width = 414
                  Height = 21
                  DataField = 'entrega_xBairro'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 5
                end
                object Edentrega_cMun: TDBEdit
                  Left = 422
                  Top = 98
                  Width = 55
                  Height = 21
                  DataField = 'entrega_cMun'
                  DataSource = DsNFeG
                  MaxLength = 7
                  ReadOnly = True
                  TabOrder = 6
                end
                object Edentrega_UF: TDBEdit
                  Left = 938
                  Top = 98
                  Width = 29
                  Height = 21
                  DataField = 'entrega_UF'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 8
                end
                object Edentrega_xMun: TDBEdit
                  Left = 477
                  Top = 98
                  Width = 457
                  Height = 21
                  DataField = 'entrega_xMun'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 7
                end
                object Edentrega_CPF: TDBEdit
                  Left = 118
                  Top = 20
                  Width = 112
                  Height = 21
                  DataField = 'entrega_CPF'
                  DataSource = DsNFeG
                  ReadOnly = True
                  TabOrder = 1
                end
              end
            end
            object Panel35: TPanel
              Left = 0
              Top = 294
              Width = 977
              Height = 64
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
            end
          end
          object TabSheet9: TTabSheet
            Caption = 'Produtos'
            ImageIndex = 7
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 977
              Height = 134
              Align = alClient
              DataSource = DsNFeI
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'nItem'
                  Title.Caption = 'Item'
                  Width = 19
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_cProd'
                  Title.Caption = 'C'#243'digo produto (do emitente)'
                  Width = 113
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_xProd'
                  Title.Caption = 'Descri'#231#227'o do produto (do emitente)'
                  Width = 223
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_CFOP'
                  Title.Caption = 'CFOP'
                  Width = 27
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_NCM'
                  Title.Caption = 'NCM'
                  Width = 43
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_qTrib'
                  Title.Caption = 'Qtd. trib.'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_uTrib'
                  Title.Caption = 'Unidade'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vUnTrib'
                  Title.Caption = '$ Unit. Trib.'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vProd'
                  Title.Caption = '$ Outro'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vDesc'
                  Title.Caption = '$ desconto'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vFrete'
                  Title.Caption = '$ Frete'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vOutro'
                  Title.Caption = '$ Outro'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_vSeg'
                  Title.Caption = '$ Seguro'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_xPed'
                  Title.Caption = 'Pedido'
                  Width = 87
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'prod_xProd'
                  Title.Caption = 'Item Pedido'
                  Width = 675
                  Visible = True
                end>
            end
            object PageControl3: TPageControl
              Left = 0
              Top = 134
              Width = 977
              Height = 318
              ActivePage = TabSheet10
              Align = alBottom
              TabOrder = 1
              object TabSheet10: TTabSheet
                Caption = 'Produto / ICMS / IPI'
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 969
                  Height = 138
                  Align = alTop
                  Caption = ' Dados do Produto: '
                  TabOrder = 0
                  object Panel7: TPanel
                    Left = 2
                    Top = 15
                    Width = 965
                    Height = 121
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label40: TLabel
                      Left = 4
                      Top = 0
                      Width = 55
                      Height = 13
                      Caption = 'prod_cProd'
                      FocusControl = DBEdit1
                    end
                    object Label41: TLabel
                      Left = 4
                      Top = 39
                      Width = 55
                      Height = 13
                      Caption = 'prod_cEAN'
                      FocusControl = DBEdit2
                    end
                    object Label42: TLabel
                      Left = 244
                      Top = 0
                      Width = 54
                      Height = 13
                      Caption = 'prod_xProd'
                      FocusControl = DBEdit3
                    end
                    object Label43: TLabel
                      Left = 362
                      Top = 78
                      Width = 51
                      Height = 13
                      Caption = 'prod_NCM'
                      FocusControl = DBEdit4
                    end
                    object Label44: TLabel
                      Left = 473
                      Top = 78
                      Width = 37
                      Height = 13
                      Caption = 'EXTIPI:'
                      FocusControl = DBEdit5
                    end
                    object Label45: TLabel
                      Left = 516
                      Top = 78
                      Width = 31
                      Height = 13
                      Caption = 'CFOP:'
                      FocusControl = DBEdit6
                    end
                    object Label46: TLabel
                      Left = 90
                      Top = 39
                      Width = 54
                      Height = 13
                      Caption = 'prod_uCom'
                      FocusControl = DBEdit7
                    end
                    object Label47: TLabel
                      Left = 174
                      Top = 39
                      Width = 54
                      Height = 13
                      Caption = 'prod_qCom'
                      FocusControl = DBEdit8
                    end
                    object Label48: TLabel
                      Left = 268
                      Top = 39
                      Width = 68
                      Height = 13
                      Caption = 'prod_vUnCom'
                      FocusControl = DBEdit9
                    end
                    object Label49: TLabel
                      Left = 362
                      Top = 39
                      Width = 55
                      Height = 13
                      Caption = 'prod_vProd'
                      FocusControl = DBEdit10
                    end
                    object Label50: TLabel
                      Left = 4
                      Top = 78
                      Width = 73
                      Height = 13
                      Caption = 'prod_cEANTrib'
                      FocusControl = DBEdit11
                    end
                    object Label51: TLabel
                      Left = 90
                      Top = 78
                      Width = 51
                      Height = 13
                      Caption = 'prod_uTrib'
                      FocusControl = DBEdit12
                    end
                    object Label52: TLabel
                      Left = 174
                      Top = 78
                      Width = 51
                      Height = 13
                      Caption = 'prod_qTrib'
                      FocusControl = DBEdit13
                    end
                    object Label53: TLabel
                      Left = 268
                      Top = 78
                      Width = 65
                      Height = 13
                      Caption = 'prod_vUnTrib'
                      FocusControl = DBEdit14
                    end
                    object Label54: TLabel
                      Left = 457
                      Top = 39
                      Width = 57
                      Height = 13
                      Caption = 'prod_vFrete'
                      FocusControl = DBEdit15
                    end
                    object Label55: TLabel
                      Left = 551
                      Top = 39
                      Width = 52
                      Height = 13
                      Caption = 'prod_vSeg'
                      FocusControl = DBEdit16
                    end
                    object Label56: TLabel
                      Left = 646
                      Top = 39
                      Width = 58
                      Height = 13
                      Caption = 'prod_vDesc'
                      FocusControl = DBEdit17
                    end
                    object Label57: TLabel
                      Left = 741
                      Top = 39
                      Width = 59
                      Height = 13
                      Caption = 'prod_vOutro'
                      FocusControl = DBEdit18
                    end
                    object Label58: TLabel
                      Left = 559
                      Top = 78
                      Width = 51
                      Height = 13
                      Caption = 'prod_xPed'
                      FocusControl = DBEdit19
                    end
                    object Label59: TLabel
                      Left = 843
                      Top = 78
                      Width = 72
                      Height = 13
                      Caption = 'prod_nItemPed'
                      FocusControl = DBEdit20
                    end
                    object Label60: TLabel
                      Left = 835
                      Top = 39
                      Width = 57
                      Height = 13
                      Caption = 'prod_indTot'
                      FocusControl = DBEdit21
                    end
                    object DBEdit1: TDBEdit
                      Left = 4
                      Top = 16
                      Width = 236
                      Height = 21
                      DataField = 'prod_cProd'
                      DataSource = DsNFeI
                      TabOrder = 0
                    end
                    object DBEdit2: TDBEdit
                      Left = 4
                      Top = 55
                      Width = 82
                      Height = 21
                      DataField = 'prod_cEAN'
                      DataSource = DsNFeI
                      TabOrder = 1
                    end
                    object DBEdit3: TDBEdit
                      Left = 244
                      Top = 16
                      Width = 729
                      Height = 21
                      DataField = 'prod_xProd'
                      DataSource = DsNFeI
                      TabOrder = 2
                    end
                    object DBEdit4: TDBEdit
                      Left = 362
                      Top = 94
                      Width = 107
                      Height = 21
                      DataField = 'prod_NCM'
                      DataSource = DsNFeI
                      TabOrder = 3
                    end
                    object DBEdit5: TDBEdit
                      Left = 473
                      Top = 94
                      Width = 39
                      Height = 21
                      DataField = 'prod_EXTIPI'
                      DataSource = DsNFeI
                      TabOrder = 4
                    end
                    object DBEdit6: TDBEdit
                      Left = 516
                      Top = 94
                      Width = 39
                      Height = 21
                      DataField = 'prod_CFOP'
                      DataSource = DsNFeI
                      TabOrder = 5
                    end
                    object DBEdit7: TDBEdit
                      Left = 90
                      Top = 55
                      Width = 80
                      Height = 21
                      DataField = 'prod_uCom'
                      DataSource = DsNFeI
                      TabOrder = 6
                    end
                    object DBEdit8: TDBEdit
                      Left = 174
                      Top = 55
                      Width = 90
                      Height = 21
                      DataField = 'prod_qCom'
                      DataSource = DsNFeI
                      TabOrder = 7
                    end
                    object DBEdit9: TDBEdit
                      Left = 268
                      Top = 55
                      Width = 90
                      Height = 21
                      DataField = 'prod_vUnCom'
                      DataSource = DsNFeI
                      TabOrder = 8
                    end
                    object DBEdit10: TDBEdit
                      Left = 362
                      Top = 55
                      Width = 91
                      Height = 21
                      DataField = 'prod_vProd'
                      DataSource = DsNFeI
                      TabOrder = 9
                    end
                    object DBEdit11: TDBEdit
                      Left = 4
                      Top = 94
                      Width = 82
                      Height = 21
                      DataField = 'prod_cEANTrib'
                      DataSource = DsNFeI
                      TabOrder = 10
                    end
                    object DBEdit12: TDBEdit
                      Left = 90
                      Top = 94
                      Width = 80
                      Height = 21
                      DataField = 'prod_uTrib'
                      DataSource = DsNFeI
                      TabOrder = 11
                    end
                    object DBEdit13: TDBEdit
                      Left = 174
                      Top = 94
                      Width = 90
                      Height = 21
                      DataField = 'prod_qTrib'
                      DataSource = DsNFeI
                      TabOrder = 12
                    end
                    object DBEdit14: TDBEdit
                      Left = 268
                      Top = 94
                      Width = 90
                      Height = 21
                      DataField = 'prod_vUnTrib'
                      DataSource = DsNFeI
                      TabOrder = 13
                    end
                    object DBEdit15: TDBEdit
                      Left = 457
                      Top = 55
                      Width = 90
                      Height = 21
                      DataField = 'prod_vFrete'
                      DataSource = DsNFeI
                      TabOrder = 14
                    end
                    object DBEdit16: TDBEdit
                      Left = 551
                      Top = 55
                      Width = 91
                      Height = 21
                      DataField = 'prod_vSeg'
                      DataSource = DsNFeI
                      TabOrder = 15
                    end
                    object DBEdit17: TDBEdit
                      Left = 646
                      Top = 55
                      Width = 91
                      Height = 21
                      DataField = 'prod_vDesc'
                      DataSource = DsNFeI
                      TabOrder = 16
                    end
                    object DBEdit18: TDBEdit
                      Left = 741
                      Top = 55
                      Width = 90
                      Height = 21
                      DataField = 'prod_vOutro'
                      DataSource = DsNFeI
                      TabOrder = 17
                    end
                    object DBEdit19: TDBEdit
                      Left = 559
                      Top = 94
                      Width = 280
                      Height = 21
                      DataField = 'prod_xPed'
                      DataSource = DsNFeI
                      TabOrder = 18
                    end
                    object DBEdit20: TDBEdit
                      Left = 843
                      Top = 94
                      Width = 130
                      Height = 21
                      DataField = 'prod_nItemPed'
                      DataSource = DsNFeI
                      TabOrder = 19
                    end
                    object DBEdit21: TDBEdit
                      Left = 835
                      Top = 55
                      Width = 138
                      Height = 21
                      DataField = 'prod_indTot'
                      DataSource = DsNFeI
                      TabOrder = 20
                    end
                  end
                end
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 138
                  Width = 969
                  Height = 94
                  Align = alTop
                  Caption = ' ICMS:  '
                  TabOrder = 1
                  object Panel8: TPanel
                    Left = 2
                    Top = 15
                    Width = 965
                    Height = 77
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label61: TLabel
                      Left = 4
                      Top = 0
                      Width = 24
                      Height = 13
                      Caption = 'CST:'
                      FocusControl = DBEdit22
                    end
                    object Label62: TLabel
                      Left = 43
                      Top = 0
                      Width = 37
                      Height = 13
                      Caption = 'modBC:'
                      FocusControl = DBEdit24
                    end
                    object Label63: TLabel
                      Left = 181
                      Top = 0
                      Width = 54
                      Height = 13
                      Caption = '% Red. BC:'
                      FocusControl = DBEdit25
                    end
                    object Label64: TLabel
                      Left = 86
                      Top = 0
                      Width = 79
                      Height = 13
                      Caption = 'Base de c'#225'lculo:'
                      FocusControl = DBEdit26
                    end
                    object Label65: TLabel
                      Left = 240
                      Top = 0
                      Width = 40
                      Height = 13
                      Caption = '% ICMS:'
                      FocusControl = DBEdit27
                    end
                    object Label66: TLabel
                      Left = 287
                      Top = 0
                      Width = 56
                      Height = 13
                      Caption = 'Valor ICMS:'
                      FocusControl = DBEdit28
                    end
                    object Label67: TLabel
                      Left = 378
                      Top = 0
                      Width = 51
                      Height = 13
                      Caption = 'modBCST:'
                      FocusControl = DBEdit29
                    end
                    object Label68: TLabel
                      Left = 437
                      Top = 0
                      Width = 51
                      Height = 13
                      Caption = '% MVAST:'
                      FocusControl = DBEdit30
                    end
                    object Label69: TLabel
                      Left = 496
                      Top = 0
                      Width = 65
                      Height = 13
                      Caption = '% Red.BCST:'
                      FocusControl = DBEdit31
                    end
                    object Label70: TLabel
                      Left = 567
                      Top = 0
                      Width = 58
                      Height = 13
                      Caption = 'Valor BCST:'
                      FocusControl = DBEdit32
                    end
                    object Label71: TLabel
                      Left = 662
                      Top = 0
                      Width = 57
                      Height = 13
                      Caption = '% ICMS ST:'
                      FocusControl = DBEdit33
                    end
                    object Label72: TLabel
                      Left = 725
                      Top = 0
                      Width = 73
                      Height = 13
                      Caption = 'Valor ICMS ST:'
                      FocusControl = DBEdit34
                    end
                    object Label73: TLabel
                      Left = 299
                      Top = 39
                      Width = 40
                      Height = 13
                      Caption = 'CSOSN:'
                      FocusControl = DBEdit35
                    end
                    object Label74: TLabel
                      Left = 4
                      Top = 39
                      Width = 34
                      Height = 13
                      Caption = 'UF ST:'
                      FocusControl = DBEdit36
                    end
                    object Label75: TLabel
                      Left = 43
                      Top = 39
                      Width = 46
                      Height = 13
                      Caption = '% BC OP:'
                      FocusControl = DBEdit37
                    end
                    object Label76: TLabel
                      Left = 94
                      Top = 39
                      Width = 92
                      Height = 13
                      Caption = 'Valor BCST Retido:'
                      FocusControl = DBEdit38
                    end
                    object Label77: TLabel
                      Left = 189
                      Top = 39
                      Width = 107
                      Height = 13
                      Caption = 'Valor ICMS ST Retido:'
                      FocusControl = DBEdit39
                    end
                    object Label78: TLabel
                      Left = 815
                      Top = 0
                      Width = 78
                      Height = 13
                      Caption = 'Mot. Des. ICMS:'
                      FocusControl = DBEdit40
                    end
                    object Label79: TLabel
                      Left = 346
                      Top = 39
                      Width = 54
                      Height = 13
                      Caption = '% Cred.SN:'
                      FocusControl = DBEdit41
                    end
                    object Label80: TLabel
                      Left = 410
                      Top = 39
                      Width = 99
                      Height = 13
                      Caption = 'Valor Cred.ICMS SN:'
                      FocusControl = DBEdit42
                    end
                    object DBEdit22: TDBEdit
                      Left = 4
                      Top = 16
                      Width = 16
                      Height = 21
                      DataField = 'ICMS_Orig'
                      DataSource = DsNFeN
                      TabOrder = 0
                    end
                    object DBEdit23: TDBEdit
                      Left = 20
                      Top = 16
                      Width = 19
                      Height = 21
                      DataField = 'ICMS_CST'
                      DataSource = DsNFeN
                      TabOrder = 1
                    end
                    object DBEdit24: TDBEdit
                      Left = 43
                      Top = 16
                      Width = 39
                      Height = 21
                      DataField = 'ICMS_modBC'
                      DataSource = DsNFeN
                      TabOrder = 2
                    end
                    object DBEdit25: TDBEdit
                      Left = 181
                      Top = 16
                      Width = 55
                      Height = 21
                      DataField = 'ICMS_pRedBC'
                      DataSource = DsNFeN
                      TabOrder = 3
                    end
                    object DBEdit26: TDBEdit
                      Left = 86
                      Top = 16
                      Width = 92
                      Height = 21
                      DataField = 'ICMS_vBC'
                      DataSource = DsNFeN
                      TabOrder = 4
                    end
                    object DBEdit27: TDBEdit
                      Left = 240
                      Top = 16
                      Width = 43
                      Height = 21
                      DataField = 'ICMS_pICMS'
                      DataSource = DsNFeN
                      TabOrder = 5
                    end
                    object DBEdit28: TDBEdit
                      Left = 287
                      Top = 16
                      Width = 87
                      Height = 21
                      DataField = 'ICMS_vICMS'
                      DataSource = DsNFeN
                      TabOrder = 6
                    end
                    object DBEdit29: TDBEdit
                      Left = 378
                      Top = 16
                      Width = 56
                      Height = 21
                      DataField = 'ICMS_modBCST'
                      DataSource = DsNFeN
                      TabOrder = 7
                    end
                    object DBEdit30: TDBEdit
                      Left = 437
                      Top = 16
                      Width = 55
                      Height = 21
                      DataField = 'ICMS_pMVAST'
                      DataSource = DsNFeN
                      TabOrder = 8
                    end
                    object DBEdit31: TDBEdit
                      Left = 496
                      Top = 16
                      Width = 67
                      Height = 21
                      DataField = 'ICMS_pRedBCST'
                      DataSource = DsNFeN
                      TabOrder = 9
                    end
                    object DBEdit32: TDBEdit
                      Left = 567
                      Top = 16
                      Width = 91
                      Height = 21
                      DataField = 'ICMS_vBCST'
                      DataSource = DsNFeN
                      TabOrder = 10
                    end
                    object DBEdit33: TDBEdit
                      Left = 662
                      Top = 16
                      Width = 59
                      Height = 21
                      DataField = 'ICMS_pICMSST'
                      DataSource = DsNFeN
                      TabOrder = 11
                    end
                    object DBEdit34: TDBEdit
                      Left = 725
                      Top = 16
                      Width = 86
                      Height = 21
                      DataField = 'ICMS_vICMSST'
                      DataSource = DsNFeN
                      TabOrder = 12
                    end
                    object DBEdit35: TDBEdit
                      Left = 299
                      Top = 55
                      Width = 43
                      Height = 21
                      DataField = 'ICMS_CSOSN'
                      DataSource = DsNFeN
                      TabOrder = 13
                    end
                    object DBEdit36: TDBEdit
                      Left = 4
                      Top = 55
                      Width = 35
                      Height = 21
                      DataField = 'ICMS_UFST'
                      DataSource = DsNFeN
                      TabOrder = 14
                    end
                    object DBEdit37: TDBEdit
                      Left = 43
                      Top = 55
                      Width = 47
                      Height = 21
                      DataField = 'ICMS_pBCOp'
                      DataSource = DsNFeN
                      TabOrder = 15
                    end
                    object DBEdit38: TDBEdit
                      Left = 94
                      Top = 55
                      Width = 91
                      Height = 21
                      DataField = 'ICMS_vBCSTRet'
                      DataSource = DsNFeN
                      TabOrder = 16
                    end
                    object DBEdit39: TDBEdit
                      Left = 189
                      Top = 55
                      Width = 106
                      Height = 21
                      DataField = 'ICMS_vICMSSTRet'
                      DataSource = DsNFeN
                      TabOrder = 17
                    end
                    object DBEdit40: TDBEdit
                      Left = 815
                      Top = 16
                      Width = 79
                      Height = 21
                      DataField = 'ICMS_motDesICMS'
                      DataSource = DsNFeN
                      TabOrder = 18
                    end
                    object DBEdit41: TDBEdit
                      Left = 346
                      Top = 55
                      Width = 60
                      Height = 21
                      DataField = 'ICMS_pCredSN'
                      DataSource = DsNFeN
                      TabOrder = 19
                    end
                    object DBEdit42: TDBEdit
                      Left = 410
                      Top = 55
                      Width = 98
                      Height = 21
                      DataField = 'ICMS_vCredICMSSN'
                      DataSource = DsNFeN
                      TabOrder = 20
                    end
                  end
                end
                object GroupBox3: TGroupBox
                  Left = 0
                  Top = 232
                  Width = 969
                  Height = 58
                  Align = alTop
                  Caption = ' IPI: '
                  TabOrder = 2
                  object Panel9: TPanel
                    Left = 2
                    Top = 15
                    Width = 965
                    Height = 41
                    Align = alClient
                    ParentBackground = False
                    TabOrder = 0
                    object Label81: TLabel
                      Left = 4
                      Top = 0
                      Width = 34
                      Height = 13
                      Caption = 'Classe:'
                      FocusControl = DBEdit43
                    end
                    object Label82: TLabel
                      Left = 47
                      Top = 0
                      Width = 73
                      Height = 13
                      Caption = 'CNPJ Produtor:'
                      FocusControl = DBEdit44
                    end
                    object Label83: TLabel
                      Left = 142
                      Top = 0
                      Width = 160
                      Height = 13
                      Caption = 'C'#243'digo do selo de controle de IPI:'
                      FocusControl = DBEdit45
                    end
                    object Label84: TLabel
                      Left = 362
                      Top = 0
                      Width = 63
                      Height = 13
                      Caption = 'Qtde de selo:'
                      FocusControl = DBEdit46
                    end
                    object Label85: TLabel
                      Left = 445
                      Top = 0
                      Width = 25
                      Height = 13
                      Caption = 'Enq.:'
                      FocusControl = DBEdit47
                    end
                    object Label86: TLabel
                      Left = 477
                      Top = 0
                      Width = 24
                      Height = 13
                      Caption = 'CST:'
                      FocusControl = DBEdit48
                    end
                    object Label87: TLabel
                      Left = 504
                      Top = 0
                      Width = 44
                      Height = 13
                      Caption = 'Valor BC:'
                      FocusControl = DBEdit49
                    end
                    object Label88: TLabel
                      Left = 598
                      Top = 0
                      Width = 71
                      Height = 13
                      Caption = 'Qtd. Unidades:'
                      FocusControl = DBEdit50
                    end
                    object Label89: TLabel
                      Left = 693
                      Top = 0
                      Width = 50
                      Height = 13
                      Caption = '$ unidade:'
                      FocusControl = DBEdit51
                    end
                    object Label90: TLabel
                      Left = 788
                      Top = 0
                      Width = 27
                      Height = 13
                      Caption = '% IPI:'
                      FocusControl = DBEdit52
                    end
                    object Label91: TLabel
                      Left = 882
                      Top = 0
                      Width = 43
                      Height = 13
                      Caption = 'Valor IPI:'
                      FocusControl = DBEdit53
                    end
                    object DBEdit43: TDBEdit
                      Left = 4
                      Top = 16
                      Width = 39
                      Height = 21
                      DataField = 'IPI_clEnq'
                      DataSource = DsNFeO
                      TabOrder = 0
                    end
                    object DBEdit44: TDBEdit
                      Left = 47
                      Top = 16
                      Width = 91
                      Height = 21
                      DataField = 'IPI_CNPJProd'
                      DataSource = DsNFeO
                      TabOrder = 1
                    end
                    object DBEdit45: TDBEdit
                      Left = 142
                      Top = 16
                      Width = 216
                      Height = 21
                      DataField = 'IPI_cSelo'
                      DataSource = DsNFeO
                      TabOrder = 2
                    end
                    object DBEdit46: TDBEdit
                      Left = 362
                      Top = 16
                      Width = 79
                      Height = 21
                      DataField = 'IPI_qSelo'
                      DataSource = DsNFeO
                      TabOrder = 3
                    end
                    object DBEdit47: TDBEdit
                      Left = 445
                      Top = 16
                      Width = 28
                      Height = 21
                      DataField = 'IPI_cEnq'
                      DataSource = DsNFeO
                      TabOrder = 4
                    end
                    object DBEdit48: TDBEdit
                      Left = 477
                      Top = 16
                      Width = 23
                      Height = 21
                      DataField = 'IPI_CST'
                      DataSource = DsNFeO
                      TabOrder = 5
                    end
                    object DBEdit49: TDBEdit
                      Left = 504
                      Top = 16
                      Width = 90
                      Height = 21
                      DataField = 'IPI_vBC'
                      DataSource = DsNFeO
                      TabOrder = 6
                    end
                    object DBEdit50: TDBEdit
                      Left = 598
                      Top = 16
                      Width = 92
                      Height = 21
                      DataField = 'IPI_qUnid'
                      DataSource = DsNFeO
                      TabOrder = 7
                    end
                    object DBEdit51: TDBEdit
                      Left = 693
                      Top = 16
                      Width = 91
                      Height = 21
                      DataField = 'IPI_vUnid'
                      DataSource = DsNFeO
                      TabOrder = 8
                    end
                    object DBEdit52: TDBEdit
                      Left = 788
                      Top = 16
                      Width = 90
                      Height = 21
                      DataField = 'IPI_pIPI'
                      DataSource = DsNFeO
                      TabOrder = 9
                    end
                    object DBEdit53: TDBEdit
                      Left = 882
                      Top = 16
                      Width = 91
                      Height = 21
                      DataField = 'IPI_vIPI'
                      DataSource = DsNFeO
                      TabOrder = 10
                    end
                  end
                end
              end
              object TabSheet11: TTabSheet
                Caption = 'II / PIS/ COFINS / Informa'#231#245'es Adicionais do Produto'
                ImageIndex = 1
                object dmkLabelRotate1: TdmkLabelRotate
                  Left = 0
                  Top = 182
                  Width = 13
                  Height = 108
                  Angle = ag90
                  Caption = 'Inform. Adicionais'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -12
                  Font.Name = 'Arial'
                  Font.Style = []
                  Align = alLeft
                end
                object GroupBox4: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 967
                  Height = 44
                  Align = alTop
                  Caption = 'II (Imposto de importa'#231#227'o): '
                  TabOrder = 0
                  object Panel10: TPanel
                    Left = 2
                    Top = 14
                    Width = 963
                    Height = 28
                    Align = alClient
                    ParentBackground = False
                    TabOrder = 0
                    object Label92: TLabel
                      Left = 8
                      Top = 8
                      Width = 17
                      Height = 13
                      Caption = 'BC:'
                      FocusControl = DBEdit54
                    end
                    object Label93: TLabel
                      Left = 126
                      Top = 8
                      Width = 55
                      Height = 13
                      Caption = 'Desp. adu.:'
                      FocusControl = DBEdit55
                    end
                    object Label94: TLabel
                      Left = 287
                      Top = 8
                      Width = 39
                      Height = 13
                      Caption = 'Valor I.I.'
                      FocusControl = DBEdit56
                    end
                    object Label95: TLabel
                      Left = 430
                      Top = 8
                      Width = 47
                      Height = 13
                      Caption = 'Valor IOF:'
                      FocusControl = DBEdit57
                    end
                    object DBEdit54: TDBEdit
                      Left = 31
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'II_vBC'
                      DataSource = DsNFeP
                      TabOrder = 0
                    end
                    object DBEdit55: TDBEdit
                      Left = 185
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'II_vDespAdu'
                      DataSource = DsNFeP
                      TabOrder = 1
                    end
                    object DBEdit56: TDBEdit
                      Left = 331
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'II_vII'
                      DataSource = DsNFeP
                      TabOrder = 2
                    end
                    object DBEdit57: TDBEdit
                      Left = 481
                      Top = 4
                      Width = 90
                      Height = 21
                      DataField = 'II_vIOF'
                      DataSource = DsNFeP
                      TabOrder = 3
                    end
                  end
                end
                object GroupBox5: TGroupBox
                  Left = 0
                  Top = 44
                  Width = 967
                  Height = 70
                  Align = alTop
                  Caption = ' PIS: '
                  TabOrder = 1
                  object Panel11: TPanel
                    Left = 2
                    Top = 14
                    Width = 963
                    Height = 53
                    Align = alClient
                    ParentBackground = False
                    TabOrder = 0
                    object Label96: TLabel
                      Left = 4
                      Top = 8
                      Width = 24
                      Height = 13
                      Caption = 'CST:'
                      FocusControl = DBEdit58
                    end
                    object Label97: TLabel
                      Left = 63
                      Top = 8
                      Width = 64
                      Height = 13
                      Caption = 'Valor BC PIS:'
                      FocusControl = DBEdit59
                    end
                    object Label98: TLabel
                      Left = 225
                      Top = 8
                      Width = 31
                      Height = 13
                      Caption = '% PIS:'
                      FocusControl = DBEdit60
                    end
                    object Label99: TLabel
                      Left = 666
                      Top = 8
                      Width = 47
                      Height = 13
                      Caption = 'Valor PIS:'
                      FocusControl = DBEdit61
                    end
                    object Label100: TLabel
                      Left = 303
                      Top = 8
                      Width = 77
                      Height = 13
                      Caption = 'BC Produto PIS:'
                      FocusControl = DBEdit62
                    end
                    object Label101: TLabel
                      Left = 477
                      Top = 8
                      Width = 98
                      Height = 13
                      Caption = 'Valor Aliq. Prod. PIS:'
                      FocusControl = DBEdit63
                    end
                    object Label102: TLabel
                      Left = 4
                      Top = 31
                      Width = 81
                      Height = 13
                      Caption = 'Valor BC PIS ST:'
                      FocusControl = DBEdit64
                    end
                    object Label103: TLabel
                      Left = 181
                      Top = 31
                      Width = 48
                      Height = 13
                      Caption = '% PIS ST:'
                      FocusControl = DBEdit65
                    end
                    object Label104: TLabel
                      Left = 276
                      Top = 31
                      Width = 82
                      Height = 13
                      Caption = 'BC Prod. PIS ST:'
                      FocusControl = DBEdit66
                    end
                    object Label105: TLabel
                      Left = 453
                      Top = 31
                      Width = 115
                      Height = 13
                      Caption = 'Valor Aliq. Prod. PIS ST:'
                      FocusControl = DBEdit67
                    end
                    object Label106: TLabel
                      Left = 654
                      Top = 31
                      Width = 64
                      Height = 13
                      Caption = 'Valor PIS ST:'
                      FocusControl = DBEdit68
                    end
                    object Label107: TLabel
                      Left = 807
                      Top = 8
                      Width = 64
                      Height = 13
                      Caption = 'Fator BC PIS:'
                      FocusControl = DBEdit69
                    end
                    object Label108: TLabel
                      Left = 815
                      Top = 31
                      Width = 81
                      Height = 13
                      Caption = 'Fator BC PIS ST:'
                      FocusControl = DBEdit70
                    end
                    object DBEdit58: TDBEdit
                      Left = 31
                      Top = 4
                      Width = 28
                      Height = 21
                      DataField = 'PIS_CST'
                      DataSource = DsNFeQ
                      TabOrder = 0
                    end
                    object DBEdit59: TDBEdit
                      Left = 130
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'PIS_vBC'
                      DataSource = DsNFeQ
                      TabOrder = 1
                    end
                    object DBEdit60: TDBEdit
                      Left = 260
                      Top = 4
                      Width = 39
                      Height = 21
                      DataField = 'PIS_pPIS'
                      DataSource = DsNFeQ
                      TabOrder = 2
                    end
                    object DBEdit61: TDBEdit
                      Left = 713
                      Top = 4
                      Width = 90
                      Height = 21
                      DataField = 'PIS_vPIS'
                      DataSource = DsNFeQ
                      TabOrder = 3
                    end
                    object DBEdit62: TDBEdit
                      Left = 382
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'PIS_qBCProd'
                      DataSource = DsNFeQ
                      TabOrder = 4
                    end
                    object DBEdit63: TDBEdit
                      Left = 579
                      Top = 4
                      Width = 79
                      Height = 21
                      DataField = 'PIS_vAliqProd'
                      DataSource = DsNFeQ
                      TabOrder = 5
                    end
                    object DBEdit64: TDBEdit
                      Left = 86
                      Top = 27
                      Width = 92
                      Height = 21
                      DataField = 'PISST_vBC'
                      DataSource = DsNFeR
                      TabOrder = 6
                    end
                    object DBEdit65: TDBEdit
                      Left = 232
                      Top = 27
                      Width = 40
                      Height = 21
                      DataField = 'PISST_pPIS'
                      DataSource = DsNFeR
                      TabOrder = 7
                    end
                    object DBEdit66: TDBEdit
                      Left = 358
                      Top = 27
                      Width = 91
                      Height = 21
                      DataField = 'PISST_qBCProd'
                      DataSource = DsNFeR
                      TabOrder = 8
                    end
                    object DBEdit67: TDBEdit
                      Left = 571
                      Top = 27
                      Width = 79
                      Height = 21
                      DataField = 'PISST_vAliqProd'
                      DataSource = DsNFeR
                      TabOrder = 9
                    end
                    object DBEdit68: TDBEdit
                      Left = 721
                      Top = 27
                      Width = 90
                      Height = 21
                      DataField = 'PISST_vPIS'
                      DataSource = DsNFeR
                      TabOrder = 10
                    end
                    object DBEdit69: TDBEdit
                      Left = 874
                      Top = 4
                      Width = 89
                      Height = 21
                      DataField = 'PIS_fatorBC'
                      DataSource = DsNFeQ
                      TabOrder = 11
                    end
                    object DBEdit70: TDBEdit
                      Left = 898
                      Top = 27
                      Width = 65
                      Height = 21
                      DataField = 'PISST_fatorBCST'
                      DataSource = DsNFeR
                      TabOrder = 12
                    end
                  end
                end
                object GroupBox6: TGroupBox
                  Left = 0
                  Top = 114
                  Width = 967
                  Height = 68
                  Align = alTop
                  Caption = ' COFINS: '
                  TabOrder = 2
                  object Panel12: TPanel
                    Left = 2
                    Top = 14
                    Width = 963
                    Height = 53
                    Align = alClient
                    ParentBackground = False
                    TabOrder = 0
                    object Label137: TLabel
                      Left = 4
                      Top = 8
                      Width = 24
                      Height = 13
                      Caption = 'CST:'
                      FocusControl = DBEdit71
                    end
                    object Label138: TLabel
                      Left = 63
                      Top = 8
                      Width = 44
                      Height = 13
                      Caption = 'Valor BC:'
                      FocusControl = DBEdit72
                    end
                    object Label139: TLabel
                      Left = 205
                      Top = 8
                      Width = 53
                      Height = 13
                      Caption = '% COFINS:'
                      FocusControl = DBEdit73
                    end
                    object Label140: TLabel
                      Left = 658
                      Top = 8
                      Width = 69
                      Height = 13
                      Caption = 'Valor COFINS:'
                      FocusControl = DBEdit74
                    end
                    object Label141: TLabel
                      Left = 307
                      Top = 8
                      Width = 57
                      Height = 13
                      Caption = 'BC Produto:'
                      FocusControl = DBEdit75
                    end
                    object Label142: TLabel
                      Left = 477
                      Top = 8
                      Width = 78
                      Height = 13
                      Caption = 'Valor Aliq. Prod.:'
                      FocusControl = DBEdit76
                    end
                    object Label143: TLabel
                      Left = 4
                      Top = 31
                      Width = 61
                      Height = 13
                      Caption = 'Valor BC ST:'
                      FocusControl = DBEdit77
                    end
                    object Label144: TLabel
                      Left = 162
                      Top = 31
                      Width = 70
                      Height = 13
                      Caption = '% COFINS ST:'
                      FocusControl = DBEdit78
                    end
                    object Label145: TLabel
                      Left = 295
                      Top = 31
                      Width = 62
                      Height = 13
                      Caption = 'BC Prod. ST:'
                      FocusControl = DBEdit79
                    end
                    object Label146: TLabel
                      Left = 461
                      Top = 31
                      Width = 95
                      Height = 13
                      Caption = 'Valor Aliq. Prod. ST:'
                      FocusControl = DBEdit80
                    end
                    object Label147: TLabel
                      Left = 642
                      Top = 31
                      Width = 86
                      Height = 13
                      Caption = 'Valor COFINS ST:'
                      FocusControl = DBEdit81
                    end
                    object Label148: TLabel
                      Left = 827
                      Top = 8
                      Width = 44
                      Height = 13
                      Caption = 'Fator BC:'
                      FocusControl = DBEdit82
                    end
                    object Label149: TLabel
                      Left = 827
                      Top = 31
                      Width = 61
                      Height = 13
                      Caption = 'Fator BC ST:'
                      FocusControl = DBEdit83
                    end
                    object DBEdit71: TDBEdit
                      Left = 31
                      Top = 4
                      Width = 28
                      Height = 21
                      DataField = 'COFINS_CST'
                      DataSource = DsNFeS
                      TabOrder = 0
                    end
                    object DBEdit72: TDBEdit
                      Left = 110
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'COFINS_vBC'
                      DataSource = DsNFeS
                      TabOrder = 1
                    end
                    object DBEdit73: TDBEdit
                      Left = 260
                      Top = 4
                      Width = 39
                      Height = 21
                      DataField = 'COFINS_pCOFINS'
                      DataSource = DsNFeS
                      TabOrder = 2
                    end
                    object DBEdit74: TDBEdit
                      Left = 729
                      Top = 4
                      Width = 90
                      Height = 21
                      DataField = 'COFINS_vCOFINS'
                      DataSource = DsNFeS
                      TabOrder = 3
                    end
                    object DBEdit75: TDBEdit
                      Left = 366
                      Top = 4
                      Width = 91
                      Height = 21
                      DataField = 'COFINS_qBCProd'
                      DataSource = DsNFeS
                      TabOrder = 4
                    end
                    object DBEdit76: TDBEdit
                      Left = 559
                      Top = 4
                      Width = 79
                      Height = 21
                      DataField = 'COFINS_vAliqProd'
                      DataSource = DsNFeS
                      TabOrder = 5
                    end
                    object DBEdit77: TDBEdit
                      Left = 67
                      Top = 27
                      Width = 91
                      Height = 21
                      DataField = 'COFINSST_vBC'
                      DataSource = DsNFeT
                      TabOrder = 6
                    end
                    object DBEdit78: TDBEdit
                      Left = 232
                      Top = 27
                      Width = 40
                      Height = 21
                      DataField = 'COFINSST_pCOFINS'
                      DataSource = DsNFeT
                      TabOrder = 7
                    end
                    object DBEdit79: TDBEdit
                      Left = 358
                      Top = 27
                      Width = 91
                      Height = 21
                      DataField = 'COFINSST_qBCProd'
                      DataSource = DsNFeT
                      TabOrder = 8
                    end
                    object DBEdit80: TDBEdit
                      Left = 559
                      Top = 27
                      Width = 79
                      Height = 21
                      DataField = 'COFINSST_vAliqProd'
                      DataSource = DsNFeT
                      TabOrder = 9
                    end
                    object DBEdit81: TDBEdit
                      Left = 733
                      Top = 27
                      Width = 90
                      Height = 21
                      DataField = 'COFINSST_vCOFINS'
                      DataSource = DsNFeT
                      TabOrder = 10
                    end
                    object DBEdit82: TDBEdit
                      Left = 874
                      Top = 4
                      Width = 89
                      Height = 21
                      DataField = 'COFINS_fatorBC'
                      DataSource = DsNFeS
                      TabOrder = 11
                    end
                    object DBEdit83: TDBEdit
                      Left = 898
                      Top = 27
                      Width = 65
                      Height = 21
                      DataField = 'COFINSST_fatorBCST'
                      DataSource = DsNFeT
                      TabOrder = 12
                    end
                  end
                end
                object DBMemo1: TDBMemo
                  Left = 13
                  Top = 182
                  Width = 954
                  Height = 108
                  Align = alClient
                  DataField = 'InfAdProd'
                  DataSource = DsNFeV
                  TabOrder = 3
                end
              end
              object TabSheet12: TTabSheet
                Caption = 'Importa'#231#227'o'
                ImageIndex = 2
                object Panel34: TPanel
                  Left = 0
                  Top = 0
                  Width = 967
                  Height = 290
                  Align = alClient
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox12: TGroupBox
                    Left = 1
                    Top = 1
                    Width = 965
                    Height = 288
                    Align = alClient
                    Caption = ' Declara'#231#227'o de Importa'#231#227'o: '
                    TabOrder = 0
                    object DBGrid2: TDBGrid
                      Left = 2
                      Top = 14
                      Width = 576
                      Height = 272
                      Align = alLeft
                      DataSource = DsNFeIDi
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'DI_nDI'
                          Title.Caption = 'N'#186' DI'
                          Width = 59
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_dDI'
                          Title.Caption = 'Data DI'
                          Width = 45
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_xLocDesemb'
                          Title.Caption = 'Local do desembara'#231'o'
                          Width = 213
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_UFDesemb'
                          Title.Caption = 'UF'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_dDesemb'
                          Title.Caption = 'Dta desem.'
                          Width = 45
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'DI_cExportador'
                          Title.Caption = 'Cad. Exportador'
                          Width = 66
                          Visible = True
                        end>
                    end
                    object DBGrid3: TDBGrid
                      Left = 578
                      Top = 14
                      Width = 385
                      Height = 272
                      Align = alClient
                      DataSource = DsNFeIDiA
                      TabOrder = 1
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Adi_nAdicao'
                          Title.Caption = 'N'#186' Adi'#231#227'o'
                          Width = 59
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_nSeqAdic'
                          Title.Caption = 'Sequencial'
                          Width = 59
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_cFabricante'
                          Title.Caption = 'C'#243'd. Fabricante'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Adi_vDescDI'
                          Title.Caption = '$ desconto adi'#231#227'o'
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object TabSheet16: TTabSheet
                Caption = 'ISSQN'
                ImageIndex = 3
                object Panel27: TPanel
                  Left = 0
                  Top = 0
                  Width = 967
                  Height = 290
                  Align = alClient
                  ParentBackground = False
                  TabOrder = 0
                  object Label150: TLabel
                    Left = 4
                    Top = 4
                    Width = 17
                    Height = 13
                    Caption = 'BC:'
                    FocusControl = DBEdit84
                  end
                  object Label232: TLabel
                    Left = 98
                    Top = 4
                    Width = 34
                    Height = 13
                    Caption = '% Aliq.:'
                    FocusControl = DBEdit85
                  end
                  object Label233: TLabel
                    Left = 142
                    Top = 4
                    Width = 63
                    Height = 13
                    Caption = 'Valor ISSQN:'
                    FocusControl = DBEdit86
                  end
                  object Label234: TLabel
                    Left = 236
                    Top = 4
                    Width = 50
                    Height = 13
                    Caption = 'Munic'#237'pio:'
                    FocusControl = DBEdit87
                  end
                  object Label235: TLabel
                    Left = 295
                    Top = 4
                    Width = 64
                    Height = 13
                    Caption = 'C'#243'd. Servi'#231'o:'
                    FocusControl = DBEdit88
                  end
                  object Label236: TLabel
                    Left = 366
                    Top = 4
                    Width = 113
                    Height = 13
                    Caption = 'Situa'#231#227'o da tributa'#231#227'o: '
                    FocusControl = DBEdit89
                  end
                  object DBEdit84: TDBEdit
                    Left = 4
                    Top = 20
                    Width = 90
                    Height = 21
                    DataField = 'ISSQN_vBC'
                    DataSource = DsNFeU
                    TabOrder = 0
                  end
                  object DBEdit85: TDBEdit
                    Left = 98
                    Top = 20
                    Width = 40
                    Height = 21
                    DataField = 'ISSQN_vAliq'
                    DataSource = DsNFeU
                    TabOrder = 1
                  end
                  object DBEdit86: TDBEdit
                    Left = 142
                    Top = 20
                    Width = 90
                    Height = 21
                    DataField = 'ISSQN_vISSQN'
                    DataSource = DsNFeU
                    TabOrder = 2
                  end
                  object DBEdit87: TDBEdit
                    Left = 236
                    Top = 20
                    Width = 55
                    Height = 21
                    DataField = 'ISSQN_cMunFG'
                    DataSource = DsNFeU
                    TabOrder = 3
                  end
                  object DBEdit88: TDBEdit
                    Left = 295
                    Top = 20
                    Width = 67
                    Height = 21
                    DataField = 'ISSQN_cListServ'
                    DataSource = DsNFeU
                    TabOrder = 4
                  end
                  object DBEdit89: TDBEdit
                    Left = 366
                    Top = 20
                    Width = 17
                    Height = 21
                    DataField = 'ISSQN_cSitTrib'
                    DataSource = DsNFeU
                    TabOrder = 5
                  end
                  object DBEdit90: TDBEdit
                    Left = 382
                    Top = 20
                    Width = 99
                    Height = 21
                    DataField = 'ISSQN_cSitTrib_TXT'
                    DataSource = DsNFeU
                    TabOrder = 6
                  end
                end
              end
            end
          end
          object TabSheet13: TTabSheet
            Caption = 'Totais'
            ImageIndex = 8
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 166
              Align = alTop
              TabOrder = 0
              object Panel18: TPanel
                Left = 592
                Top = 1
                Width = 382
                Height = 165
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object GroupBox7: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 382
                  Height = 56
                  Align = alTop
                  Caption = ' Valores totais referentes ao ISSQN: '
                  TabOrder = 0
                  object Label151: TLabel
                    Left = 8
                    Top = 16
                    Width = 69
                    Height = 13
                    Caption = 'Valor servi'#231'os:'
                    FocusControl = EdISSQNtot_vServ
                  end
                  object Label152: TLabel
                    Left = 82
                    Top = 16
                    Width = 52
                    Height = 13
                    Caption = 'BC do ISS:'
                    FocusControl = EdISSQNtot_vBC
                  end
                  object Label153: TLabel
                    Left = 158
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Total do ISS:'
                    FocusControl = EdISSQNtot_vISS
                  end
                  object Label154: TLabel
                    Left = 232
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Valor do PIS:'
                    FocusControl = EdISSQNtot_vPIS
                  end
                  object Label155: TLabel
                    Left = 307
                    Top = 16
                    Width = 69
                    Height = 13
                    Caption = 'Valor COFINS:'
                    FocusControl = EdISSQNtot_vCOFINS
                  end
                  object EdISSQNtot_vServ: TDBEdit
                    Left = 8
                    Top = 31
                    Width = 70
                    Height = 21
                    DataField = 'ISSQNtot_vServ'
                    DataSource = DsNFeW17
                    ReadOnly = True
                    TabOrder = 0
                  end
                  object EdISSQNtot_vBC: TDBEdit
                    Left = 82
                    Top = 31
                    Width = 72
                    Height = 21
                    DataField = 'ISSQNtot_vBC'
                    DataSource = DsNFeW17
                    ReadOnly = True
                    TabOrder = 1
                  end
                  object EdISSQNtot_vISS: TDBEdit
                    Left = 158
                    Top = 31
                    Width = 71
                    Height = 21
                    DataField = 'ISSQNtot_vISS'
                    DataSource = DsNFeW17
                    ReadOnly = True
                    TabOrder = 2
                  end
                  object EdISSQNtot_vPIS: TDBEdit
                    Left = 232
                    Top = 31
                    Width = 71
                    Height = 21
                    DataField = 'ISSQNtot_vPIS'
                    DataSource = DsNFeW17
                    ReadOnly = True
                    TabOrder = 3
                  end
                  object EdISSQNtot_vCOFINS: TDBEdit
                    Left = 307
                    Top = 31
                    Width = 71
                    Height = 21
                    DataField = 'ISSQNtot_vCOFINS'
                    DataSource = DsNFeW17
                    ReadOnly = True
                    TabOrder = 4
                  end
                end
              end
              object Panel19: TPanel
                Left = 1
                Top = 1
                Width = 591
                Height = 165
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox8: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 590
                  Height = 99
                  Align = alTop
                  Caption = ' Valores totais referentes ao ICMS: '
                  TabOrder = 0
                  object Label156: TLabel
                    Left = 8
                    Top = 16
                    Width = 61
                    Height = 13
                    Caption = 'BC do ICMS:'
                    FocusControl = EdICMSTot_vBC
                  end
                  object Label157: TLabel
                    Left = 90
                    Top = 16
                    Width = 71
                    Height = 13
                    Caption = 'Valor do ICMS:'
                    FocusControl = EdICMSTot_vICMS
                  end
                  object Label158: TLabel
                    Left = 174
                    Top = 16
                    Width = 78
                    Height = 13
                    Caption = 'BC do ICMS ST:'
                    FocusControl = EdICMSTot_vBCST
                  end
                  object Label159: TLabel
                    Left = 256
                    Top = 16
                    Width = 73
                    Height = 13
                    Caption = 'Valor ICMS ST:'
                    FocusControl = EdICMSTot_vST
                  end
                  object Label160: TLabel
                    Left = 338
                    Top = 16
                    Width = 81
                    Height = 13
                    Caption = 'Tot. prod e serv.:'
                    FocusControl = EdICMSTot_vProd
                  end
                  object Label161: TLabel
                    Left = 422
                    Top = 16
                    Width = 51
                    Height = 13
                    Caption = 'Total frete:'
                    FocusControl = EdICMSTot_vFrete
                  end
                  object Label162: TLabel
                    Left = 504
                    Top = 16
                    Width = 62
                    Height = 13
                    Caption = 'Total seguro:'
                    FocusControl = EdICMSTot_vSeg
                  end
                  object Label163: TLabel
                    Left = 8
                    Top = 55
                    Width = 76
                    Height = 13
                    Caption = 'Total Desconto:'
                    FocusControl = EdICMSTot_vDesc
                  end
                  object Label164: TLabel
                    Left = 90
                    Top = 55
                    Width = 51
                    Height = 13
                    Caption = 'Total do II:'
                    FocusControl = EdICMSTot_vII
                  end
                  object Label165: TLabel
                    Left = 174
                    Top = 55
                    Width = 58
                    Height = 13
                    Caption = 'Total do IPI:'
                    FocusControl = EdICMSTot_vIPI
                  end
                  object Label166: TLabel
                    Left = 256
                    Top = 55
                    Width = 62
                    Height = 13
                    Caption = 'Total do PIS:'
                    FocusControl = EdICMSTot_vPIS
                  end
                  object Label167: TLabel
                    Left = 338
                    Top = 55
                    Width = 69
                    Height = 13
                    Caption = 'Total COFINS:'
                    FocusControl = EdICMSTot_vCOFINS
                  end
                  object Label168: TLabel
                    Left = 422
                    Top = 55
                    Width = 61
                    Height = 13
                    Caption = 'Total Outros:'
                    FocusControl = EdICMSTot_vOutro
                  end
                  object Label169: TLabel
                    Left = 504
                    Top = 55
                    Width = 53
                    Height = 13
                    Caption = 'Total NF-e:'
                    FocusControl = EdICMSTot_vNF
                  end
                  object EdICMSTot_vBC: TDBEdit
                    Left = 8
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vBC'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 0
                  end
                  object EdICMSTot_vICMS: TDBEdit
                    Left = 90
                    Top = 31
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vICMS'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 1
                  end
                  object EdICMSTot_vBCST: TDBEdit
                    Left = 174
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vBCST'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 2
                  end
                  object EdICMSTot_vST: TDBEdit
                    Left = 256
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vST'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 3
                  end
                  object EdICMSTot_vProd: TDBEdit
                    Left = 338
                    Top = 31
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vProd'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 4
                  end
                  object EdICMSTot_vFrete: TDBEdit
                    Left = 422
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vFrete'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 5
                  end
                  object EdICMSTot_vSeg: TDBEdit
                    Left = 504
                    Top = 31
                    Width = 79
                    Height = 21
                    DataField = 'ICMSTot_vSeg'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 6
                  end
                  object EdICMSTot_vDesc: TDBEdit
                    Left = 8
                    Top = 71
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vDesc'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 7
                  end
                  object EdICMSTot_vII: TDBEdit
                    Left = 90
                    Top = 71
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vII'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 8
                  end
                  object EdICMSTot_vIPI: TDBEdit
                    Left = 174
                    Top = 71
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vIPI'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 9
                  end
                  object EdICMSTot_vPIS: TDBEdit
                    Left = 256
                    Top = 71
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vPIS'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 10
                  end
                  object EdICMSTot_vCOFINS: TDBEdit
                    Left = 338
                    Top = 71
                    Width = 80
                    Height = 21
                    DataField = 'ICMSTot_vCOFINS'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 11
                  end
                  object EdICMSTot_vOutro: TDBEdit
                    Left = 422
                    Top = 71
                    Width = 78
                    Height = 21
                    DataField = 'ICMSTot_vOutro'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 12
                  end
                  object EdICMSTot_vNF: TDBEdit
                    Left = 504
                    Top = 71
                    Width = 79
                    Height = 21
                    DataField = 'ICMSTot_vNF'
                    DataSource = DsNFeW02
                    ReadOnly = True
                    TabOrder = 13
                  end
                end
                object GroupBox9: TGroupBox
                  Left = 0
                  Top = 99
                  Width = 590
                  Height = 66
                  Align = alClient
                  Caption = ' Reten'#231#227'o de Tributos (em valores - $): '
                  TabOrder = 1
                  object Label170: TLabel
                    Left = 8
                    Top = 16
                    Width = 20
                    Height = 13
                    Caption = 'PIS:'
                    FocusControl = EdRetTrib_vRetPIS
                  end
                  object Label171: TLabel
                    Left = 90
                    Top = 16
                    Width = 42
                    Height = 13
                    Caption = 'COFINS:'
                    FocusControl = EdRetTrib_vRetCOFINS
                  end
                  object Label172: TLabel
                    Left = 174
                    Top = 16
                    Width = 29
                    Height = 13
                    Caption = 'CSLL:'
                    FocusControl = EdRetTrib_vRetCSLL
                  end
                  object Label173: TLabel
                    Left = 256
                    Top = 16
                    Width = 60
                    Height = 13
                    Caption = 'BC do IRRF:'
                    FocusControl = EdRetTrib_vBCIRRF
                  end
                  object Label174: TLabel
                    Left = 338
                    Top = 16
                    Width = 28
                    Height = 13
                    Caption = 'IRRF:'
                    FocusControl = EdRetTrib_vIRRF
                  end
                  object Label175: TLabel
                    Left = 422
                    Top = 16
                    Width = 75
                    Height = 13
                    Caption = 'BC Prev. social:'
                    FocusControl = EdRetTrib_vBCRetPrev
                  end
                  object Label176: TLabel
                    Left = 504
                    Top = 16
                    Width = 58
                    Height = 13
                    Caption = 'Prev. social:'
                    FocusControl = EdRetTrib_vRetPrev
                  end
                  object EdRetTrib_vRetPIS: TDBEdit
                    Left = 8
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'RetTrib_vRetPIS'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 0
                  end
                  object EdRetTrib_vRetCOFINS: TDBEdit
                    Left = 90
                    Top = 31
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vRetCOFINS'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 1
                  end
                  object EdRetTrib_vRetCSLL: TDBEdit
                    Left = 174
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'RetTrib_vRetCSLL'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 2
                  end
                  object EdRetTrib_vBCIRRF: TDBEdit
                    Left = 256
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'RetTrib_vBCIRRF'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 3
                  end
                  object EdRetTrib_vIRRF: TDBEdit
                    Left = 338
                    Top = 31
                    Width = 80
                    Height = 21
                    DataField = 'RetTrib_vIRRF'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 4
                  end
                  object EdRetTrib_vBCRetPrev: TDBEdit
                    Left = 422
                    Top = 31
                    Width = 78
                    Height = 21
                    DataField = 'RetTrib_vBCRetPrev'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 5
                  end
                  object EdRetTrib_vRetPrev: TDBEdit
                    Left = 504
                    Top = 31
                    Width = 79
                    Height = 21
                    DataField = 'RetTrib_vRetPrev'
                    DataSource = DsNFeW23
                    ReadOnly = True
                    TabOrder = 6
                  end
                end
              end
            end
            object Panel52: TPanel
              Left = 0
              Top = 166
              Width = 974
              Height = 271
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
            end
          end
          object TabSheet14: TTabSheet
            Caption = 'Transporte'
            ImageIndex = 9
            object Panel20: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 154
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label177: TLabel
                Left = 8
                Top = 4
                Width = 97
                Height = 13
                Caption = 'Modalidade do frete:'
                FocusControl = EdModFrete
              end
              object Label178: TLabel
                Left = 236
                Top = 4
                Width = 157
                Height = 13
                Caption = 'CNPJ da transportadora, ou CPF:'
                FocusControl = Edtransporta_CNPJ
              end
              object Label179: TLabel
                Left = 465
                Top = 4
                Width = 112
                Height = 13
                Caption = 'Raz'#227'o Social ou Nome:'
                FocusControl = Edtransporta_xNome
              end
              object Label180: TLabel
                Left = 8
                Top = 43
                Width = 19
                Height = 13
                Caption = 'I.E.:'
                FocusControl = EdTransporta_IE
              end
              object Label181: TLabel
                Left = 122
                Top = 43
                Width = 49
                Height = 13
                Caption = 'Endere'#231'o:'
                FocusControl = EdTransporta_XEnder
              end
              object Label182: TLabel
                Left = 590
                Top = 43
                Width = 96
                Height = 13
                Caption = 'Nome do Munic'#237'pio:'
                FocusControl = EdTransporta_XMun
              end
              object Label183: TLabel
                Left = 942
                Top = 43
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = EdTransporta_UF
              end
              object Label184: TLabel
                Left = 8
                Top = 86
                Width = 79
                Height = 13
                Caption = 'Valor do servi'#231'o:'
                FocusControl = EdRetTransp_vServ
              end
              object Label185: TLabel
                Left = 8
                Top = 106
                Width = 121
                Height = 13
                Caption = 'BC da reten'#231#227'o do ICMS:'
                FocusControl = EdRetTransp_vBCRet
              end
              object Label211: TLabel
                Left = 221
                Top = 82
                Width = 108
                Height = 13
                Caption = 'Al'#237'quota da Reten'#231#227'o:'
                FocusControl = EdRetTransp_PICMSRet
              end
              object Label212: TLabel
                Left = 221
                Top = 110
                Width = 100
                Height = 13
                Caption = 'Valor do ICMS retido:'
                FocusControl = EdRetTransp_vICMSRet
              end
              object Label213: TLabel
                Left = 414
                Top = 86
                Width = 31
                Height = 13
                Caption = 'CFOP:'
                FocusControl = EdRetTransp_CFOP
              end
              object Label214: TLabel
                Left = 4
                Top = 134
                Width = 352
                Height = 13
                Caption = 
                  'C'#243'digo do munic'#237'pio de ocorr'#234'ncia do fato gerador do ICMS do tra' +
                  'nsporte:'
                FocusControl = EdRetTransp_CMunFG
              end
              object Label215: TLabel
                Left = 512
                Top = 86
                Width = 30
                Height = 13
                Caption = 'Placa:'
                FocusControl = EdVeicTransp_Placa
              end
              object Label216: TLabel
                Left = 658
                Top = 86
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = EdVeicTransp_UF
              end
              object Label217: TLabel
                Left = 414
                Top = 110
                Width = 33
                Height = 13
                Caption = 'RNTC:'
                FocusControl = EdVeicTransp_RNTC
              end
              object SpeedButton8: TSpeedButton
                Left = 349
                Top = 20
                Width = 21
                Height = 20
                Caption = '?'
              end
              object Label218: TLabel
                Left = 772
                Top = 86
                Width = 34
                Height = 13
                Caption = 'Vag'#227'o:'
                FocusControl = Edvagao
              end
              object Label219: TLabel
                Left = 776
                Top = 110
                Width = 29
                Height = 13
                Caption = 'Balsa:'
                FocusControl = Edbalsa
              end
              object EdModFrete: TDBEdit
                Left = 8
                Top = 20
                Width = 29
                Height = 21
                DataField = 'ModFrete'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 0
              end
              object EdxModFrete: TDBEdit
                Left = 39
                Top = 20
                Width = 195
                Height = 21
                ReadOnly = True
                TabOrder = 1
              end
              object Edtransporta_CNPJ: TDBEdit
                Left = 236
                Top = 20
                Width = 111
                Height = 21
                DataField = 'Transporta_CNPJ'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 2
              end
              object Edtransporta_CPF: TDBEdit
                Left = 370
                Top = 20
                Width = 91
                Height = 21
                DataField = 'Transporta_CPF'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 3
              end
              object Edtransporta_xNome: TDBEdit
                Left = 466
                Top = 20
                Width = 504
                Height = 21
                DataField = 'Transporta_XNome'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 4
              end
              object EdTransporta_IE: TDBEdit
                Left = 8
                Top = 59
                Width = 111
                Height = 21
                DataField = 'Transporta_IE'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 5
              end
              object EdTransporta_XEnder: TDBEdit
                Left = 122
                Top = 59
                Width = 466
                Height = 21
                DataField = 'Transporta_XEnder'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 6
              end
              object EdTransporta_XMun: TDBEdit
                Left = 590
                Top = 59
                Width = 348
                Height = 21
                DataField = 'Transporta_XMun'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 7
              end
              object EdTransporta_UF: TDBEdit
                Left = 942
                Top = 59
                Width = 29
                Height = 21
                DataField = 'Transporta_UF'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 8
              end
              object EdRetTransp_vServ: TDBEdit
                Left = 138
                Top = 82
                Width = 79
                Height = 21
                DataField = 'RetTransp_vServ'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 9
              end
              object EdRetTransp_vBCRet: TDBEdit
                Left = 138
                Top = 106
                Width = 79
                Height = 21
                DataField = 'RetTransp_vBCRet'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 10
              end
              object EdRetTransp_PICMSRet: TDBEdit
                Left = 331
                Top = 82
                Width = 79
                Height = 21
                DataField = 'RetTransp_PICMSRet'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 11
              end
              object EdRetTransp_vICMSRet: TDBEdit
                Left = 331
                Top = 106
                Width = 79
                Height = 21
                DataField = 'RetTransp_vICMSRet'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 12
              end
              object EdRetTransp_CFOP: TDBEdit
                Left = 449
                Top = 82
                Width = 55
                Height = 21
                DataField = 'RetTransp_CFOP'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 13
              end
              object EdRetTransp_CMunFG: TDBEdit
                Left = 358
                Top = 130
                Width = 56
                Height = 21
                DataField = 'RetTransp_CMunFG'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 17
              end
              object EdVeicTransp_Placa: TDBEdit
                Left = 547
                Top = 82
                Width = 107
                Height = 21
                DataField = 'VeicTransp_Placa'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 14
              end
              object EdVeicTransp_UF: TDBEdit
                Left = 678
                Top = 82
                Width = 29
                Height = 21
                DataField = 'VeicTransp_UF'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 15
              end
              object EdVeicTransp_RNTC: TDBEdit
                Left = 453
                Top = 106
                Width = 320
                Height = 21
                DataField = 'VeicTransp_RNTC'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 16
              end
              object EdRetTransp_xMunFG: TDBEdit
                Left = 418
                Top = 130
                Width = 556
                Height = 21
                DataField = 'RetTransp_xMunFG'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 18
              end
              object Edvagao: TDBEdit
                Left = 807
                Top = 82
                Width = 166
                Height = 21
                DataField = 'Vagao'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 19
              end
              object Edbalsa: TDBEdit
                Left = 807
                Top = 106
                Width = 166
                Height = 21
                DataField = 'Balsa'
                DataSource = DsNFeX01
                ReadOnly = True
                TabOrder = 20
              end
            end
            object Panel13: TPanel
              Left = 0
              Top = 154
              Width = 234
              Height = 283
              Align = alLeft
              ParentBackground = False
              TabOrder = 1
              object Label220: TLabel
                Left = 1
                Top = 1
                Width = 231
                Height = 17
                Align = alTop
                Alignment = taCenter
                AutoSize = False
                Caption = 'Reboques'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBGrid4: TDBGrid
                Left = 1
                Top = 18
                Width = 231
                Height = 264
                Align = alClient
                DataSource = DsNFeX22
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'placa'
                    Title.Caption = 'Placa'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'RNTC'
                    Width = 115
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'UF'
                    Visible = True
                  end>
              end
            end
            object Panel14: TPanel
              Left = 234
              Top = 154
              Width = 740
              Height = 283
              Align = alClient
              ParentBackground = False
              TabOrder = 2
              object Label221: TLabel
                Left = 1
                Top = 1
                Width = 740
                Height = 17
                Align = alTop
                Alignment = taCenter
                AutoSize = False
                Caption = 'Volumes transportados'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBGrid5: TDBGrid
                Left = 1
                Top = 18
                Width = 557
                Height = 264
                Align = alClient
                DataSource = DsNFeX26
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'qVol'
                    Title.Caption = 'Quantidade'
                    Width = 59
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'pesoB'
                    Title.Caption = 'kg Bruto'
                    Width = 59
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'pesoL'
                    Title.Caption = 'kg L'#237'quido'
                    Width = 59
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'esp'
                    Title.Caption = 'Esp'#233'cie'
                    Width = 114
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'marca'
                    Title.Caption = 'Marca'
                    Width = 114
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'nVol'
                    Title.Caption = 'Numera'#231#227'o'
                    Width = 114
                    Visible = True
                  end>
              end
              object Panel23: TPanel
                Left = 558
                Top = 18
                Width = 183
                Height = 264
                Align = alRight
                BevelOuter = bvLowered
                ParentBackground = False
                TabOrder = 1
                object Label222: TLabel
                  Left = 1
                  Top = 1
                  Width = 180
                  Height = 17
                  Align = alTop
                  Alignment = taCenter
                  AutoSize = False
                  Caption = 'Lacres do volume selecionado'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object DBGrid6: TDBGrid
                  Left = 1
                  Top = 18
                  Width = 180
                  Height = 244
                  Align = alClient
                  DataSource = DsNFeX33
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'nLacre'
                      Title.Caption = 'N'#250'mero do lacre'
                      Width = 117
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet15: TTabSheet
            Caption = 'Cobran'#231'a'
            ImageIndex = 10
            object Panel24: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 473
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object GroupBox10: TGroupBox
                Left = 1
                Top = 1
                Width = 973
                Height = 43
                Align = alTop
                Caption = ' Dados da Fatura: '
                TabOrder = 0
                object Label223: TLabel
                  Left = 12
                  Top = 20
                  Width = 85
                  Height = 13
                  Caption = 'N'#250'mero da fatura:'
                  FocusControl = Edcobr_fat_nFat
                end
                object Label224: TLabel
                  Left = 287
                  Top = 20
                  Width = 63
                  Height = 13
                  Caption = 'Valor original:'
                  FocusControl = Edcobr_fat_vOrig
                end
                object Label225: TLabel
                  Left = 434
                  Top = 20
                  Width = 92
                  Height = 13
                  Caption = 'Valor do desconto: '
                  FocusControl = Edcobr_fat_vDesc
                end
                object Label226: TLabel
                  Left = 602
                  Top = 20
                  Width = 62
                  Height = 13
                  Caption = 'Valor l'#237'quido:'
                  FocusControl = Edcobr_fat_vLiq
                end
                object Edcobr_fat_nFat: TDBEdit
                  Left = 98
                  Top = 16
                  Width = 183
                  Height = 21
                  DataField = 'Cobr_Fat_nFat'
                  DataSource = DsNFeY01
                  ReadOnly = True
                  TabOrder = 0
                end
                object Edcobr_fat_vOrig: TDBEdit
                  Left = 354
                  Top = 16
                  Width = 72
                  Height = 21
                  DataField = 'Cobr_Fat_vOrig'
                  DataSource = DsNFeY01
                  ReadOnly = True
                  TabOrder = 1
                end
                object Edcobr_fat_vDesc: TDBEdit
                  Left = 528
                  Top = 16
                  Width = 70
                  Height = 21
                  DataField = 'Cobr_Fat_vDesc'
                  DataSource = DsNFeY01
                  ReadOnly = True
                  TabOrder = 2
                end
                object Edcobr_fat_vLiq: TDBEdit
                  Left = 670
                  Top = 16
                  Width = 71
                  Height = 21
                  DataField = 'Cobr_Fat_vLiq'
                  DataSource = DsNFeY01
                  ReadOnly = True
                  TabOrder = 3
                end
              end
              object DBGrid7: TDBGrid
                Left = 1
                Top = 44
                Width = 973
                Height = 428
                Align = alClient
                DataSource = DsNFeY07
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'nDup'
                    Title.Caption = 'N'#250'mero da Duplicata'
                    Width = 339
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'dVenc'
                    Title.Caption = 'Vencimento'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'vDup'
                    Title.Caption = 'Valor'
                    Width = 67
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet17: TTabSheet
            Caption = ' Informa'#231#245'es adicionais'
            ImageIndex = 11
            object Panel25: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 473
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label228: TLabel
                Left = 1
                Top = 103
                Width = 330
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Informa'#231#245'es Complementares de Interesse do Contribuinte'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object MeinfCpl: TDBMemo
                Left = 1
                Top = 116
                Width = 973
                Height = 177
                Align = alTop
                DataField = 'InfAdic_InfCpl'
                DataSource = DsNFeZ01
                TabOrder = 0
              end
              object Panel26: TPanel
                Left = 1
                Top = 293
                Width = 973
                Height = 179
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Panel28: TPanel
                  Left = 0
                  Top = 0
                  Width = 973
                  Height = 179
                  Align = alClient
                  TabOrder = 0
                  object Panel29: TPanel
                    Left = 490
                    Top = 1
                    Width = 482
                    Height = 177
                    Align = alRight
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label227: TLabel
                      Left = 0
                      Top = 0
                      Width = 131
                      Height = 13
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Campos livres do Fisco'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object DBGrid9: TDBGrid
                      Left = 0
                      Top = 13
                      Width = 496
                      Height = 165
                      Align = alLeft
                      DataSource = DsNFeZ07
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'xCampo'
                          Title.Caption = 'Campo'
                          Width = 115
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'xTexto'
                          Title.Caption = 'Texto'
                          Width = 261
                          Visible = True
                        end>
                    end
                  end
                  object Panel30: TPanel
                    Left = 1
                    Top = 1
                    Width = 489
                    Height = 177
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label230: TLabel
                      Left = 0
                      Top = 0
                      Width = 169
                      Height = 13
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Campos livres do Contribuinte'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -12
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object DBGrid8: TDBGrid
                      Left = 0
                      Top = 13
                      Width = 496
                      Height = 165
                      Align = alLeft
                      DataSource = DsNFeZ04
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'xCampo'
                          Title.Caption = 'Campo'
                          Width = 115
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'xTexto'
                          Title.Caption = 'Texto'
                          Width = 272
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object Panel31: TPanel
                Left = 1
                Top = 1
                Width = 973
                Height = 102
                Align = alTop
                TabOrder = 2
                object Panel32: TPanel
                  Left = 1
                  Top = 1
                  Width = 480
                  Height = 101
                  Align = alClient
                  TabOrder = 0
                  object Label229: TLabel
                    Left = 1
                    Top = 1
                    Width = 477
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    AutoSize = False
                    Caption = 'Informa'#231#245'es Adicionais de Interesse do Fisco'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object MeinfAdFisco: TDBMemo
                    Left = 1
                    Top = 14
                    Width = 477
                    Height = 85
                    Align = alClient
                    DataField = 'InfAdic_InfAdFisco'
                    DataSource = DsNFeZ01
                    TabOrder = 0
                  end
                end
                object Panel33: TPanel
                  Left = 481
                  Top = 1
                  Width = 491
                  Height = 101
                  Align = alRight
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label231: TLabel
                    Left = 0
                    Top = 0
                    Width = 491
                    Height = 14
                    Align = alTop
                    Alignment = taCenter
                    AutoSize = False
                    Caption = 'Processos Referenciados'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -12
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object DBGrid10: TDBGrid
                    Left = 0
                    Top = 14
                    Width = 496
                    Height = 87
                    Align = alLeft
                    DataSource = DsNFeZ10
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'nProc'
                        Title.Caption = 'Identificador do Processo'
                        Width = 254
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'indProc'
                        Title.Caption = 'O'
                        Width = 14
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'indProc_TXT'
                        Title.Caption = 'Origem'
                        Width = 115
                        Visible = True
                      end>
                  end
                end
              end
            end
          end
          object TabSheet18: TTabSheet
            Caption = 'Exporta'#231#227'o / Compra p'#250'blica'
            ImageIndex = 11
            object Panel36: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 473
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object GroupBox11: TGroupBox
                Left = 1
                Top = 1
                Width = 973
                Height = 43
                Align = alTop
                Caption = 'Informa'#231#245'es de Com'#233'rcio Exterior:'
                TabOrder = 0
                object Label237: TLabel
                  Left = 8
                  Top = 20
                  Width = 82
                  Height = 13
                  Caption = 'UF de embarque:'
                  FocusControl = EdExporta_UFEmbarq
                end
                object Label238: TLabel
                  Left = 118
                  Top = 20
                  Width = 94
                  Height = 13
                  Caption = 'Local de embarque:'
                  FocusControl = EdExporta_XLocEmbarq
                end
                object EdExporta_UFEmbarq: TDBEdit
                  Left = 90
                  Top = 16
                  Width = 24
                  Height = 21
                  DataField = 'Exporta_UFEmbarq'
                  DataSource = DsNFeZA01
                  ReadOnly = True
                  TabOrder = 0
                end
                object EdExporta_XLocEmbarq: TDBEdit
                  Left = 213
                  Top = 16
                  Width = 480
                  Height = 21
                  DataField = 'Exporta_XLocEmbarq'
                  DataSource = DsNFeZA01
                  ReadOnly = True
                  TabOrder = 1
                end
              end
              object GroupBox15: TGroupBox
                Left = 1
                Top = 44
                Width = 973
                Height = 91
                Align = alTop
                Caption = 'Informa'#231#227'o da Nota de Empenho de Compra P'#250'blica: '
                TabOrder = 1
                object Label239: TLabel
                  Left = 8
                  Top = 20
                  Width = 88
                  Height = 13
                  Caption = 'Nota de empenho:'
                  FocusControl = EdCompra_XNEmp
                end
                object Label240: TLabel
                  Left = 8
                  Top = 43
                  Width = 36
                  Height = 13
                  Caption = 'Pedido:'
                  FocusControl = EdCompra_XPed
                end
                object Label241: TLabel
                  Left = 8
                  Top = 67
                  Width = 40
                  Height = 13
                  Caption = 'Contrato'
                  FocusControl = EdCompra_XCont
                end
                object EdCompra_XNEmp: TDBEdit
                  Left = 98
                  Top = 16
                  Width = 142
                  Height = 21
                  DataField = 'Compra_XNEmp'
                  DataSource = DsNFeZB01
                  ReadOnly = True
                  TabOrder = 0
                end
                object EdCompra_XPed: TDBEdit
                  Left = 98
                  Top = 39
                  Width = 481
                  Height = 21
                  DataField = 'Compra_XPed'
                  DataSource = DsNFeZB01
                  ReadOnly = True
                  TabOrder = 1
                end
                object EdCompra_XCont: TDBEdit
                  Left = 98
                  Top = 63
                  Width = 481
                  Height = 21
                  DataField = 'Compra_XCont'
                  DataSource = DsNFeZB01
                  ReadOnly = True
                  TabOrder = 2
                end
              end
            end
          end
        end
      end
      object TabSheet19: TTabSheet
        Caption = 'Autoriza'#231#227'o'
        ImageIndex = 3
        object GroupBox16: TGroupBox
          Left = 0
          Top = 0
          Width = 985
          Height = 44
          Align = alTop
          Caption = 'Leiaute da Distribui'#231#227'o: NF-e: < XR01 > '
          TabOrder = 0
          ExplicitWidth = 986
          object Panel45: TPanel
            Left = 2
            Top = 14
            Width = 980
            Height = 28
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 8
              Width = 36
              Height = 13
              Caption = 'Vers'#227'o:'
            end
            object DBEdit91: TDBEdit
              Left = 47
              Top = 4
              Width = 39
              Height = 21
              DataField = 'nfeProcNFe_versao'
              DataSource = DsXR01
              TabOrder = 0
            end
          end
        end
        object PageControl5: TPageControl
          Left = 0
          Top = 44
          Width = 985
          Height = 436
          ActivePage = TabSheet21
          Align = alClient
          TabOrder = 1
          ExplicitHeight = 433
          object TabSheet21: TTabSheet
            Caption = 'Solicita'#231#227'o < ? >'
          end
          object TabSheet22: TTabSheet
            Caption = 'Resposta < PR01 >'
            ImageIndex = 1
            object Panel38: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 346
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label242: TLabel
                Left = 8
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
                FocusControl = EdprotNFe_versao
              end
              object Label244: TLabel
                Left = 51
                Top = 4
                Width = 106
                Height = 13
                Caption = 'Protocolo de resposta:'
                FocusControl = EdinfProt_Id
              end
              object Label245: TLabel
                Left = 394
                Top = 43
                Width = 33
                Height = 13
                Caption = 'Status:'
                FocusControl = EdinfProt_cStat
              end
              object Label243: TLabel
                Left = 232
                Top = 4
                Width = 47
                Height = 13
                Caption = 'Ambiente:'
                FocusControl = EdinfProt_tpAmb
              end
              object Label246: TLabel
                Left = 346
                Top = 4
                Width = 161
                Height = 13
                Caption = 'Vers'#227'o do processamento do lote:'
                FocusControl = EdinfProt_verAplic
              end
              object Label247: TLabel
                Left = 516
                Top = 4
                Width = 264
                Height = 13
                Caption = 'Chave NF-e p/ cosulta no site www.nfe.fazenda.gov.br:'
                FocusControl = EdinfProt_chNFe
              end
              object Label248: TLabel
                Left = 788
                Top = 4
                Width = 149
                Height = 13
                Caption = 'Data e hora do processamento:'
                FocusControl = EdinfProt_dhRecbto
              end
              object Label249: TLabel
                Left = 8
                Top = 43
                Width = 89
                Height = 13
                Caption = 'Protocolo da NF-e:'
                FocusControl = EdinfProt_nProt
              end
              object Label250: TLabel
                Left = 138
                Top = 43
                Width = 162
                Height = 13
                Caption = 'Digest Value da NF-e processada:'
                FocusControl = EdinfProt_digVal
              end
              object EdprotNFe_versao: TDBEdit
                Left = 8
                Top = 20
                Width = 39
                Height = 21
                DataField = 'protNFe_versao'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 0
              end
              object EdinfProt_Id: TDBEdit
                Left = 51
                Top = 20
                Width = 178
                Height = 21
                DataField = 'infProt_Id'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 1
              end
              object EdinfProt_cStat: TDBEdit
                Left = 394
                Top = 59
                Width = 32
                Height = 21
                DataField = 'infProt_cStat'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 2
              end
              object EdinfProt_tpAmb: TDBEdit
                Left = 232
                Top = 20
                Width = 16
                Height = 21
                DataField = 'infProt_tpAmb'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 3
              end
              object EdinfProt_xAmb: TDBEdit
                Left = 248
                Top = 20
                Width = 94
                Height = 21
                DataField = 'infProt_xAmb'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 4
              end
              object EdinfProt_verAplic: TDBEdit
                Left = 346
                Top = 20
                Width = 166
                Height = 21
                DataField = 'infProt_verAplic'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 5
              end
              object EdinfProt_chNFe: TDBEdit
                Left = 516
                Top = 20
                Width = 268
                Height = 21
                DataField = 'infProt_chNFe'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 6
              end
              object EdinfProt_dhRecbto: TDBEdit
                Left = 788
                Top = 20
                Width = 150
                Height = 21
                DataField = 'infProt_dhRecbto'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 7
              end
              object EdinfProt_nProt: TDBEdit
                Left = 8
                Top = 59
                Width = 126
                Height = 21
                DataField = 'infProt_nProt'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 8
              end
              object EdinfProt_digVal: TDBEdit
                Left = 138
                Top = 59
                Width = 252
                Height = 21
                DataField = 'infProt_digVal'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 9
              end
              object EdinfProt_xMotivo: TDBEdit
                Left = 430
                Top = 59
                Width = 508
                Height = 21
                DataField = 'infProt_xMotivo'
                DataSource = DsPR01
                ReadOnly = True
                TabOrder = 10
              end
            end
          end
        end
      end
      object TabSheet20: TTabSheet
        Caption = 'Cancelamento'
        ImageIndex = 4
        object PageControl4: TPageControl
          Left = 0
          Top = 44
          Width = 985
          Height = 436
          ActivePage = TabSheet27
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 433
          object TabSheet27: TTabSheet
            Caption = 'Solicita'#231#227'o < CP01 > '
            object Panel40: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 95
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label256: TLabel
                Left = 8
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
                FocusControl = EdretCanc_versao
              end
              object Label257: TLabel
                Left = 51
                Top = 4
                Width = 12
                Height = 13
                Caption = 'Id:'
                FocusControl = EdinfCanc_Id
              end
              object Label258: TLabel
                Left = 338
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
              end
              object Label261: TLabel
                Left = 469
                Top = 4
                Width = 39
                Height = 13
                Caption = 'Servi'#231'o:'
              end
              object Label262: TLabel
                Left = 571
                Top = 4
                Width = 127
                Height = 13
                Caption = 'Chave de acesso da NF-e:'
                FocusControl = EdinfCanc_chNFe
              end
              object Label263: TLabel
                Left = 846
                Top = 4
                Width = 45
                Height = 13
                Caption = 'Protocolo'
                FocusControl = EdinfCanc_nProt
              end
              object Label264: TLabel
                Left = 8
                Top = 43
                Width = 58
                Height = 13
                Caption = 'Justificativa:'
                FocusControl = EdinfCanc_xJust
              end
              object EdretCanc_versao: TDBEdit
                Left = 8
                Top = 20
                Width = 39
                Height = 21
                DataField = 'cancNFe_versao'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 0
              end
              object EdinfCanc_Id: TDBEdit
                Left = 51
                Top = 20
                Width = 283
                Height = 21
                DataField = 'infCanc_Id'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 1
              end
              object EdinfCanc_tpAmb: TDBEdit
                Left = 338
                Top = 20
                Width = 24
                Height = 21
                DataField = 'infCanc_tpAmb'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 2
              end
              object EdinfCanc_xAmb: TDBEdit
                Left = 362
                Top = 20
                Width = 103
                Height = 21
                DataField = 'infCanc_xAmb'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 3
              end
              object EdinfCanc_xServ: TDBEdit
                Left = 469
                Top = 20
                Width = 98
                Height = 21
                DataField = 'infCanc_xServ'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 4
              end
              object EdinfCanc_chNFe: TDBEdit
                Left = 571
                Top = 20
                Width = 272
                Height = 21
                DataField = 'infCanc_chNFe'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 5
              end
              object EdinfCanc_nProt: TDBEdit
                Left = 846
                Top = 20
                Width = 100
                Height = 21
                DataField = 'infCanc_nProt'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 6
              end
              object EdinfCanc_xJust: TDBEdit
                Left = 8
                Top = 59
                Width = 938
                Height = 21
                DataField = 'infCanc_xJust'
                DataSource = DsCP01
                ReadOnly = True
                TabOrder = 7
              end
            end
          end
          object TabSheet28: TTabSheet
            Caption = 'Resposta < CR01 >'
            ImageIndex = 1
            object Panel41: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 135
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label265: TLabel
                Left = 8
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
                FocusControl = EdretCancNFe_versao
              end
              object Label266: TLabel
                Left = 51
                Top = 4
                Width = 12
                Height = 13
                Caption = 'Id:'
                FocusControl = EdretCancNFe_Id
              end
              object Label267: TLabel
                Left = 338
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
              end
              object Label268: TLabel
                Left = 469
                Top = 4
                Width = 99
                Height = 13
                Caption = 'Vers'#227'o do aplicativo:'
              end
              object Label269: TLabel
                Left = 8
                Top = 82
                Width = 127
                Height = 13
                Caption = 'Chave de acesso da NF-e:'
                FocusControl = EdretCancNFe_chNFe
              end
              object Label271: TLabel
                Left = 8
                Top = 43
                Width = 200
                Height = 13
                Caption = 'C'#243'digo e descri'#231#227'o do Status da resposta:'
                FocusControl = EdretCancNFe_cStat
              end
              object Label270: TLabel
                Left = 283
                Top = 82
                Width = 149
                Height = 13
                Caption = 'Data e hora do processamento:'
                FocusControl = EdretCancNFe_dhRecbto
              end
              object Label272: TLabel
                Left = 434
                Top = 82
                Width = 48
                Height = 13
                Caption = 'Protocolo:'
                FocusControl = EdretCancNFe_nProt
              end
              object Label273: TLabel
                Left = 638
                Top = 4
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = EdretCancNFe_cUF
              end
              object EdretCancNFe_versao: TDBEdit
                Left = 8
                Top = 20
                Width = 39
                Height = 21
                DataField = 'retCanc_versao'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 0
              end
              object EdretCancNFe_Id: TDBEdit
                Left = 51
                Top = 20
                Width = 283
                Height = 21
                DataField = 'infCanc_Id'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 1
              end
              object EdretCancNFe_tpAmb: TDBEdit
                Left = 338
                Top = 20
                Width = 24
                Height = 21
                DataField = 'infCanc_tpAmb'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 2
              end
              object EdretCancNFe_xAmb: TDBEdit
                Left = 362
                Top = 20
                Width = 103
                Height = 21
                DataField = 'infCanc_xAmb'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 3
              end
              object EdretCancNFe_verAplic: TDBEdit
                Left = 469
                Top = 20
                Width = 165
                Height = 21
                DataField = 'infCanc_verAplic'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 4
              end
              object EdretCancNFe_chNFe: TDBEdit
                Left = 8
                Top = 98
                Width = 268
                Height = 21
                DataField = 'infCanc_chNFe'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 9
              end
              object EdretCancNFe_cStat: TDBEdit
                Left = 8
                Top = 59
                Width = 23
                Height = 21
                DataField = 'infCanc_cStat'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 7
              end
              object EdretCancNFe_xMotivo: TDBEdit
                Left = 35
                Top = 59
                Width = 926
                Height = 21
                DataField = 'infCanc_xMotivo'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 8
              end
              object EdretCancNFe_dhRecbto: TDBEdit
                Left = 280
                Top = 98
                Width = 150
                Height = 21
                DataField = 'infCanc_dhRecbto'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 10
              end
              object EdretCancNFe_nProt: TDBEdit
                Left = 434
                Top = 98
                Width = 98
                Height = 21
                DataField = 'infCanc_nProt'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 11
              end
              object EdretCancNFe_cUF: TDBEdit
                Left = 638
                Top = 20
                Width = 24
                Height = 21
                DataField = 'infCanc_cUF'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 5
              end
              object EdretCancNFe_xUF: TDBEdit
                Left = 662
                Top = 20
                Width = 24
                Height = 21
                DataField = 'infCanc_xUF'
                DataSource = DsCR01
                ReadOnly = True
                TabOrder = 6
              end
            end
          end
        end
        object GBprocCancNFe: TGroupBox
          Left = 0
          Top = 0
          Width = 985
          Height = 44
          Align = alTop
          Caption = ' Leiaute de Distribui'#231#227'o: Cancelamento de NF-e: < YR01 > '
          TabOrder = 1
          ExplicitWidth = 986
          object Panel43: TPanel
            Left = 2
            Top = 14
            Width = 980
            Height = 28
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Label274: TLabel
              Left = 8
              Top = 8
              Width = 36
              Height = 13
              Caption = 'Vers'#227'o:'
            end
            object EdprocCancNFe_versao: TDBEdit
              Left = 47
              Top = 4
              Width = 39
              Height = 21
              DataField = 'procCancNFe_versao'
              DataSource = DsYR01
              TabOrder = 0
            end
          end
        end
      end
      object TabSheet26: TTabSheet
        Caption = 'Inutiliza'#231#227'o'
        ImageIndex = 10
        object GroupBox17: TGroupBox
          Left = 0
          Top = 0
          Width = 985
          Height = 44
          Align = alTop
          Caption = 
            ' Leiaute de Distribui'#231#227'o: Inutiliza'#231#227'o de Numera'#231#227'o de NF-e: < ?' +
            '??? > '
          TabOrder = 0
          ExplicitWidth = 986
          object Panel46: TPanel
            Left = 2
            Top = 14
            Width = 980
            Height = 28
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Label2: TLabel
              Left = 8
              Top = 8
              Width = 36
              Height = 13
              Caption = 'Vers'#227'o:'
              Enabled = False
            end
            object DBEdit92: TDBEdit
              Left = 47
              Top = 4
              Width = 39
              Height = 21
              DataField = 'procInutNFe_versao'
              DataSource = DsZR01
              Enabled = False
              TabOrder = 0
            end
          end
        end
        object PageControl6: TPageControl
          Left = 0
          Top = 44
          Width = 985
          Height = 436
          ActivePage = TabSheet24
          Align = alClient
          TabOrder = 1
          ExplicitHeight = 433
          object TabSheet24: TTabSheet
            Caption = 'Solicita'#231#227'o: < DP01 >'
            object Panel47: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 91
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label3: TLabel
                Left = 8
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
                FocusControl = DBEdit93
              end
              object Label4: TLabel
                Left = 51
                Top = 4
                Width = 12
                Height = 13
                Caption = 'Id:'
                FocusControl = DBEdit94
              end
              object Label5: TLabel
                Left = 338
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
              end
              object Label6: TLabel
                Left = 469
                Top = 4
                Width = 39
                Height = 13
                Caption = 'Servi'#231'o:'
              end
              object Label251: TLabel
                Left = 8
                Top = 43
                Width = 58
                Height = 13
                Caption = 'Justificativa:'
                FocusControl = DBEdit98
              end
              object Label252: TLabel
                Left = 590
                Top = 4
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit99
              end
              object Label253: TLabel
                Left = 638
                Top = 4
                Width = 22
                Height = 13
                Caption = 'Ano:'
                FocusControl = DBEdit101
              end
              object Label275: TLabel
                Left = 666
                Top = 4
                Width = 30
                Height = 13
                Caption = 'CNPJ:'
                FocusControl = DBEdit102
              end
              object Label276: TLabel
                Left = 780
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Mod.:'
                FocusControl = DBEdit103
              end
              object Label277: TLabel
                Left = 807
                Top = 4
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
                FocusControl = DBEdit104
              end
              object Label278: TLabel
                Left = 839
                Top = 4
                Width = 45
                Height = 13
                Caption = 'N'#186' Inicial:'
                FocusControl = DBEdit105
              end
              object Label279: TLabel
                Left = 906
                Top = 4
                Width = 40
                Height = 13
                Caption = 'N'#186' Final:'
                FocusControl = DBEdit106
              end
              object DBEdit93: TDBEdit
                Left = 8
                Top = 20
                Width = 39
                Height = 21
                DataField = 'inutNFe_versao'
                DataSource = DsDP01
                ReadOnly = True
                TabOrder = 0
              end
              object DBEdit94: TDBEdit
                Left = 51
                Top = 20
                Width = 283
                Height = 21
                DataField = 'inutNFe_Id'
                DataSource = DsDP01
                ReadOnly = True
                TabOrder = 1
              end
              object DBEdit95: TDBEdit
                Left = 338
                Top = 20
                Width = 24
                Height = 21
                DataField = 'inutNFe_tpAmb'
                DataSource = DsDP01
                ReadOnly = True
                TabOrder = 2
              end
              object DBEdit96: TDBEdit
                Left = 362
                Top = 20
                Width = 103
                Height = 21
                DataField = 'inutNFe_xAmb'
                DataSource = DsDP01
                ReadOnly = True
                TabOrder = 3
              end
              object DBEdit97: TDBEdit
                Left = 469
                Top = 20
                Width = 118
                Height = 21
                DataField = 'inutNFe_xServ'
                DataSource = DsDP01
                ReadOnly = True
                TabOrder = 4
              end
              object DBEdit98: TDBEdit
                Left = 8
                Top = 59
                Width = 961
                Height = 21
                DataField = 'inutNFe_xJust'
                DataSource = DsDP01
                ReadOnly = True
                TabOrder = 5
              end
              object DBEdit99: TDBEdit
                Left = 590
                Top = 20
                Width = 20
                Height = 21
                DataField = 'inutNFe_cUF'
                DataSource = DsDP01
                TabOrder = 6
              end
              object DBEdit100: TDBEdit
                Left = 610
                Top = 20
                Width = 24
                Height = 21
                DataField = 'inutNFe_xUF'
                DataSource = DsDP01
                TabOrder = 7
              end
              object DBEdit101: TDBEdit
                Left = 638
                Top = 20
                Width = 24
                Height = 21
                DataField = 'inutNFe_ano'
                DataSource = DsDP01
                TabOrder = 8
              end
              object DBEdit102: TDBEdit
                Left = 666
                Top = 20
                Width = 110
                Height = 21
                DataField = 'inutNFe_CNPJ'
                DataSource = DsDP01
                TabOrder = 9
              end
              object DBEdit103: TDBEdit
                Left = 780
                Top = 20
                Width = 23
                Height = 21
                DataField = 'inutNFe_mod'
                DataSource = DsDP01
                TabOrder = 10
              end
              object DBEdit104: TDBEdit
                Left = 807
                Top = 20
                Width = 28
                Height = 21
                DataField = 'inutNFe_serie'
                DataSource = DsDP01
                TabOrder = 11
              end
              object DBEdit105: TDBEdit
                Left = 839
                Top = 20
                Width = 63
                Height = 21
                DataField = 'inutNFe_nNFIni'
                DataSource = DsDP01
                TabOrder = 12
              end
              object DBEdit106: TDBEdit
                Left = 906
                Top = 20
                Width = 63
                Height = 21
                DataField = 'inutNFe_nNFFin'
                DataSource = DsDP01
                TabOrder = 13
              end
            end
          end
          object TabSheet25: TTabSheet
            Caption = 'Resposta: < DR01 >'
            ImageIndex = 1
            object Panel48: TPanel
              Left = 0
              Top = 0
              Width = 974
              Height = 88
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Label280: TLabel
                Left = 8
                Top = 4
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
                FocusControl = DBEdit107
              end
              object Label281: TLabel
                Left = 51
                Top = 4
                Width = 12
                Height = 13
                Caption = 'Id:'
                FocusControl = DBEdit108
              end
              object Label282: TLabel
                Left = 426
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Identifica'#231#227'o do ambiente:'
              end
              object Label284: TLabel
                Left = 8
                Top = 43
                Width = 35
                Height = 13
                Caption = 'Motivo:'
                FocusControl = DBEdit112
              end
              object Label285: TLabel
                Left = 590
                Top = 4
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit113
              end
              object Label286: TLabel
                Left = 638
                Top = 4
                Width = 22
                Height = 13
                Caption = 'Ano:'
                FocusControl = DBEdit115
              end
              object Label287: TLabel
                Left = 666
                Top = 4
                Width = 30
                Height = 13
                Caption = 'CNPJ:'
                FocusControl = DBEdit116
              end
              object Label288: TLabel
                Left = 780
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Mod.:'
                FocusControl = DBEdit117
              end
              object Label289: TLabel
                Left = 807
                Top = 4
                Width = 27
                Height = 13
                Caption = 'S'#233'rie:'
                FocusControl = DBEdit118
              end
              object Label290: TLabel
                Left = 839
                Top = 4
                Width = 45
                Height = 13
                Caption = 'N'#186' Inicial:'
                FocusControl = DBEdit119
              end
              object Label291: TLabel
                Left = 906
                Top = 4
                Width = 40
                Height = 13
                Caption = 'N'#186' Final:'
                FocusControl = DBEdit120
              end
              object Label283: TLabel
                Left = 555
                Top = 4
                Width = 33
                Height = 13
                Caption = 'Status:'
                FocusControl = DBEdit111
              end
              object Label292: TLabel
                Left = 717
                Top = 43
                Width = 149
                Height = 13
                Caption = 'Data e hora do processamento:'
                FocusControl = DBEdit121
              end
              object Label293: TLabel
                Left = 870
                Top = 43
                Width = 48
                Height = 13
                Caption = 'Protocolo:'
                FocusControl = DBEdit122
              end
              object DBEdit107: TDBEdit
                Left = 8
                Top = 20
                Width = 39
                Height = 21
                DataField = 'retInutNFe_versao'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 0
              end
              object DBEdit108: TDBEdit
                Left = 51
                Top = 20
                Width = 371
                Height = 21
                DataField = 'retInutNFe_Id'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 1
              end
              object DBEdit109: TDBEdit
                Left = 426
                Top = 20
                Width = 23
                Height = 21
                DataField = 'retInutNFe_tpAmb'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 2
              end
              object DBEdit110: TDBEdit
                Left = 449
                Top = 20
                Width = 102
                Height = 21
                DataField = 'retInutNFe_xAmb'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 3
              end
              object DBEdit112: TDBEdit
                Left = 8
                Top = 59
                Width = 706
                Height = 21
                DataField = 'retInutNFe_xMotivo'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 4
              end
              object DBEdit113: TDBEdit
                Left = 590
                Top = 20
                Width = 20
                Height = 21
                DataField = 'retInutNFe_cUF'
                DataSource = DsDR01
                TabOrder = 5
              end
              object DBEdit114: TDBEdit
                Left = 610
                Top = 20
                Width = 24
                Height = 21
                DataField = 'retInutNFe_xUF'
                DataSource = DsDR01
                TabOrder = 6
              end
              object DBEdit115: TDBEdit
                Left = 638
                Top = 20
                Width = 24
                Height = 21
                DataField = 'retInutNFe_ano'
                DataSource = DsDR01
                TabOrder = 7
              end
              object DBEdit116: TDBEdit
                Left = 666
                Top = 20
                Width = 110
                Height = 21
                DataField = 'retInutNFe_CNPJ'
                DataSource = DsDR01
                TabOrder = 8
              end
              object DBEdit117: TDBEdit
                Left = 780
                Top = 20
                Width = 23
                Height = 21
                DataField = 'retInutNFe_mod'
                DataSource = DsDR01
                TabOrder = 9
              end
              object DBEdit118: TDBEdit
                Left = 807
                Top = 20
                Width = 28
                Height = 21
                DataField = 'retInutNFe_serie'
                DataSource = DsDR01
                TabOrder = 10
              end
              object DBEdit119: TDBEdit
                Left = 839
                Top = 20
                Width = 63
                Height = 21
                DataField = 'retInutNFe_nNFIni'
                DataSource = DsDR01
                TabOrder = 11
              end
              object DBEdit120: TDBEdit
                Left = 906
                Top = 20
                Width = 63
                Height = 21
                DataField = 'retInutNFe_nNFFin'
                DataSource = DsDR01
                TabOrder = 12
              end
              object DBEdit111: TDBEdit
                Left = 555
                Top = 20
                Width = 32
                Height = 21
                DataField = 'retInutNFe_cStat'
                DataSource = DsDR01
                TabOrder = 13
              end
              object DBEdit121: TDBEdit
                Left = 717
                Top = 59
                Width = 149
                Height = 21
                DataField = 'retInutNFe_dhRecbto_TXT'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 14
              end
              object DBEdit122: TDBEdit
                Left = 870
                Top = 59
                Width = 99
                Height = 21
                DataField = 'retInutNFe_nProt'
                DataSource = DsDR01
                ReadOnly = True
                TabOrder = 15
              end
            end
          end
        end
      end
      object TabSheet23: TTabSheet
        Caption = 'Campos ignorados'
        ImageIndex = 6
        object MeIgnorados: TMemo
          Left = 0
          Top = 0
          Width = 985
          Height = 386
          Align = alClient
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object Panel44: TPanel
      Left = 0
      Top = 43
      Width = 993
      Height = 79
      Align = alTop
      TabOrder = 2
      object DBGrid13: TDBGrid
        Left = 1
        Top = 1
        Width = 991
        Height = 77
        Align = alClient
        DataSource = DsArqs
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SeqArq'
            Title.Caption = 'Seq.'
            Width = 22
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NodeName'
            Title.Caption = 'Leiaute'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xTipoDoc'
            Title.Caption = 'Tipo de documento'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ArqName'
            Title.Caption = 'Nome do arquivo'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ArqDir'
            Title.Caption = 'Pasta do arquivo'
            Width = 192
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Versao'
            Title.Caption = 'Vers'#227'o'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codificacao'
            Title.Caption = 'Codifica'#231#227'o'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LocalName'
            Title.Caption = 'Local name'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NamespaceURI'
            Title.Caption = 'Namespace URI'
            Width = 1431
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Prefix'
            Title.Caption = 'Prefixo'
            Width = 115
            Visible = True
          end>
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 767
    Width = 993
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object BtAltera: TBitBtn
      Tag = 11
      Left = 20
      Top = 4
      Width = 88
      Height = 39
      Caption = '&Altera'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtAlteraClick
    end
    object Panel49: TPanel
      Left = 883
      Top = 0
      Width = 110
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn2: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 88
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtConverte: TBitBtn
      Tag = 14
      Left = 110
      Top = 4
      Width = 89
      Height = 39
      Caption = '&Converte'
      NumGlyphs = 2
      TabOrder = 2
      Visible = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 359
        Height = 31
        Caption = 'Importa'#231#227'o de NF-e de Arquivo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 359
        Height = 31
        Caption = 'Importa'#231#227'o de NF-e de Arquivo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 359
        Height = 31
        Caption = 'Importa'#231#227'o de NF-e de Arquivo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 993
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 989
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object XMLDoc: TXMLDocument
    Left = 8
    Top = 48
    DOMVendorDesc = 'MSXML'
  end
  object QrNFeI: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeIBeforeClose
    AfterScroll = QrNFeIAfterScroll
    SQL.Strings = (
      'SELECT * FROM _nfe_i_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 484
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeIFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeIFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeInItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrNFeIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrNFeIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrNFeIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrNFeIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrNFeIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrNFeIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrNFeIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
      DisplayFormat = '#,###,###,###,##0.0000;-#,###,###,###,##0.0000; '
    end
    object QrNFeIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
      DisplayFormat = '#,###,###,###,##0.0000;-#,###,###,###,##0.0000; '
    end
    object QrNFeIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrNFeIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrNFeIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
      DisplayFormat = '#,###,###,###,##0.0000;-#,###,###,###,##0.0000; '
    end
    object QrNFeIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
      DisplayFormat = '#,###,###,###,##0.0000;-#,###,###,###,##0.0000; '
    end
    object QrNFeIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeIprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeIprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrNFeIprod_xPed: TWideStringField
      FieldName = 'prod_xPed'
      Size = 15
    end
    object QrNFeIprod_nItemPed: TIntegerField
      FieldName = 'prod_nItemPed'
    end
    object QrNFeIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrNFeINomeGGX: TWideStringField
      DisplayWidth = 100
      FieldName = 'NomeGGX'
      Size = 100
    end
  end
  object DsNFeI: TDataSource
    DataSet = QrNFeI
    Left = 512
    Top = 48
  end
  object QrNFeN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_n_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 652
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeNFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeNFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeNEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNFeNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
      DisplayFormat = '00'
    end
    object QrNFeNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNFeNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNFeNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
    object QrNFeNICMS_UFST: TWideStringField
      FieldName = 'ICMS_UFST'
      Size = 2
    end
    object QrNFeNICMS_pBCOp: TFloatField
      FieldName = 'ICMS_pBCOp'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_motDesICMS: TSmallintField
      FieldName = 'ICMS_motDesICMS'
    end
    object QrNFeNICMS_pCredSN: TFloatField
      FieldName = 'ICMS_pCredSN'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNICMS_vCredICMSSN: TFloatField
      FieldName = 'ICMS_vCredICMSSN'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeNAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeN: TDataSource
    DataSet = QrNFeN
    Left = 680
    Top = 48
  end
  object QrNFeO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_o_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 708
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeOFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeOFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeOEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrNFeOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrNFeOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrNFeOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
      DisplayFormat = '#,###,###,###,##0;-#,###,###,###,##0; '
    end
    object QrNFeOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrNFeOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
      DisplayFormat = '00'
    end
    object QrNFeOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
      DisplayFormat = '#,###,###,###,##0.0000;-#,###,###,###,##0.0000; '
    end
    object QrNFeOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
      DisplayFormat = '#,###,###,###,##0.0000;-#,###,###,###,##0.0000; '
    end
    object QrNFeOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeO: TDataSource
    DataSet = QrNFeO
    Left = 736
    Top = 48
  end
  object DsNFeQ: TDataSource
    DataSet = QrNFeQ
    Left = 848
    Top = 48
  end
  object QrNFeQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_q_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 820
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeQFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeQFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeQEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeQnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeQPIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      DisplayFormat = '00'
    end
    object QrNFeQPIS_vBC: TFloatField
      FieldName = 'PIS_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeQPIS_pPIS: TFloatField
      FieldName = 'PIS_pPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeQPIS_vPIS: TFloatField
      FieldName = 'PIS_vPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeQPIS_qBCProd: TFloatField
      FieldName = 'PIS_qBCProd'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeQPIS_vAliqProd: TFloatField
      FieldName = 'PIS_vAliqProd'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeQPIS_fatorBC: TFloatField
      FieldName = 'PIS_fatorBC'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeQAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNFeP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_p_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 764
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFePFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFePFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFePEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFePnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFePII_vBC: TFloatField
      FieldName = 'II_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFePII_vDespAdu: TFloatField
      FieldName = 'II_vDespAdu'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFePII_vII: TFloatField
      FieldName = 'II_vII'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFePII_vIOF: TFloatField
      FieldName = 'II_vIOF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFePAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeP: TDataSource
    DataSet = QrNFeP
    Left = 792
    Top = 48
  end
  object DsNFeV: TDataSource
    DataSet = QrNFeV
    Left = 148
    Top = 76
  end
  object QrNFeV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_v_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 120
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeVFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeVFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeVEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeVnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeVAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrNFeS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_s_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 932
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeSFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeSFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeSEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeSnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeSCOFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      DisplayFormat = '00'
    end
    object QrNFeSCOFINS_vBC: TFloatField
      FieldName = 'COFINS_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeSCOFINS_pCOFINS: TFloatField
      FieldName = 'COFINS_pCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeSCOFINS_qBCProd: TFloatField
      FieldName = 'COFINS_qBCProd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrNFeSCOFINS_vAliqProd: TFloatField
      FieldName = 'COFINS_vAliqProd'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrNFeSCOFINS_vCOFINS: TFloatField
      FieldName = 'COFINS_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeSCOFINS_fatorBC: TFloatField
      FieldName = 'COFINS_fatorBC'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrNFeSAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeS: TDataSource
    DataSet = QrNFeS
    Left = 960
    Top = 48
  end
  object QrNFeIDi: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeIDiBeforeClose
    AfterScroll = QrNFeIDiAfterScroll
    SQL.Strings = (
      'SELECT * FROM _nfe_i_di'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 540
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeIDiFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeIDiFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeIDiEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeIDinItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeIDiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeIDiDI_nDI: TWideStringField
      FieldName = 'DI_nDI'
      Size = 10
    end
    object QrNFeIDiDI_dDI: TDateField
      FieldName = 'DI_dDI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeIDiDI_xLocDesemb: TWideStringField
      FieldName = 'DI_xLocDesemb'
      Size = 60
    end
    object QrNFeIDiDI_UFDesemb: TWideStringField
      FieldName = 'DI_UFDesemb'
      Size = 2
    end
    object QrNFeIDiDI_dDesemb: TDateField
      FieldName = 'DI_dDesemb'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeIDiDI_cExportador: TWideStringField
      FieldName = 'DI_cExportador'
      Size = 60
    end
    object QrNFeIDiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeIDi: TDataSource
    DataSet = QrNFeIDi
    Left = 568
    Top = 48
  end
  object QrNFeIDiA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_i_dia'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND DI_nDI=:P2')
    Left = 596
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeIDiAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeIDiAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeIDiAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeIDiAnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeIDiAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeIDiAConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeIDiAAdi_nAdicao: TIntegerField
      FieldName = 'Adi_nAdicao'
    end
    object QrNFeIDiAAdi_nSeqAdic: TIntegerField
      FieldName = 'Adi_nSeqAdic'
    end
    object QrNFeIDiAAdi_cFabricante: TWideStringField
      FieldName = 'Adi_cFabricante'
      Size = 60
    end
    object QrNFeIDiAAdi_vDescDI: TFloatField
      FieldName = 'Adi_vDescDI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeIDiAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeIDiADI_nDI: TWideStringField
      FieldName = 'DI_nDI'
      Size = 10
    end
  end
  object DsNFeIDiA: TDataSource
    DataSet = QrNFeIDiA
    Left = 624
    Top = 48
  end
  object QrNFeR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_r_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 876
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeRFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeRFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeREmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeRnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeRPISST_vBC: TFloatField
      FieldName = 'PISST_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeRPISST_pPIS: TFloatField
      FieldName = 'PISST_pPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeRPISST_qBCProd: TFloatField
      FieldName = 'PISST_qBCProd'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeRPISST_vAliqProd: TFloatField
      FieldName = 'PISST_vAliqProd'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeRPISST_vPIS: TFloatField
      FieldName = 'PISST_vPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeRPISST_fatorBCST: TFloatField
      FieldName = 'PISST_fatorBCST'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeRAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeR: TDataSource
    DataSet = QrNFeR
    Left = 904
    Top = 48
  end
  object QrNFeT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_t_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 8
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeTFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeTEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeTnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNFeTCOFINSST_vBC: TFloatField
      FieldName = 'COFINSST_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeTCOFINSST_pCOFINS: TFloatField
      FieldName = 'COFINSST_pCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeTCOFINSST_qBCProd: TFloatField
      FieldName = 'COFINSST_qBCProd'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeTCOFINSST_vAliqProd: TFloatField
      FieldName = 'COFINSST_vAliqProd'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeTCOFINSST_vCOFINS: TFloatField
      FieldName = 'COFINSST_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeTCOFINSST_fatorBCST: TFloatField
      FieldName = 'COFINSST_fatorBCST'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrNFeTAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeT: TDataSource
    DataSet = QrNFeT
    Left = 36
    Top = 76
  end
  object QrNFeX22: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_x_22'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 400
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeX22Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeX22placa: TWideStringField
      FieldName = 'placa'
      Size = 8
    end
    object QrNFeX22UF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrNFeX22RNTC: TWideStringField
      FieldName = 'RNTC'
    end
  end
  object DsNFeX22: TDataSource
    DataSet = QrNFeX22
    Left = 428
    Top = 76
  end
  object QrNFeX26: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeX26BeforeClose
    AfterScroll = QrNFeX26AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_x_26'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 456
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeX26Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeX26qVol: TFloatField
      FieldName = 'qVol'
    end
    object QrNFeX26esp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrNFeX26marca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrNFeX26nVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrNFeX26pesoL: TFloatField
      FieldName = 'pesoL'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrNFeX26pesoB: TFloatField
      FieldName = 'pesoB'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
  end
  object DsNFeX26: TDataSource
    DataSet = QrNFeX26
    Left = 484
    Top = 76
  end
  object QrNFeX33: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_x_33'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND Controle=:P2')
    Left = 512
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeX33Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeX33Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrNFeX33nLacre: TWideStringField
      FieldName = 'nLacre'
      Size = 60
    end
  end
  object DsNFeX33: TDataSource
    DataSet = QrNFeX33
    Left = 540
    Top = 76
  end
  object QrNFeY07: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_y_07'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 624
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeY07Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFeY07nDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrNFeY07dVenc: TDateField
      FieldName = 'dVenc'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFeY07vDup: TFloatField
      FieldName = 'vDup'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsNFeY07: TDataSource
    DataSet = QrNFeY07
    Left = 652
    Top = 76
  end
  object QrNFeZ04: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_z_04'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 736
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ04xCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeZ04xTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object DsNFeZ04: TDataSource
    DataSet = QrNFeZ04
    Left = 764
    Top = 76
  end
  object QrNFeZ07: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_z_07'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 792
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ07xCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrNFeZ07xTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
  end
  object DsNFeZ07: TDataSource
    DataSet = QrNFeZ07
    Left = 820
    Top = 76
  end
  object QrNFeZ10: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeZ10CalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_z_10'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 848
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ10nProc: TWideStringField
      FieldName = 'nProc'
      Size = 60
    end
    object QrNFeZ10indProc: TSmallintField
      FieldName = 'indProc'
    end
    object QrNFeZ10indProc_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'indProc_TXT'
      Calculated = True
    end
  end
  object DsNFeZ10: TDataSource
    DataSet = QrNFeZ10
    Left = 876
    Top = 76
  end
  object QrNFeU: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeUCalcFields
    SQL.Strings = (
      'SELECT * FROM _nfe_u_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1'
      'AND nItem=:P2')
    Left = 64
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeUISSQN_vBC: TFloatField
      FieldName = 'ISSQN_vBC'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeUISSQN_vAliq: TFloatField
      FieldName = 'ISSQN_vAliq'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeUISSQN_vISSQN: TFloatField
      FieldName = 'ISSQN_vISSQN'
      DisplayFormat = '#,###,###,###,##0.00;-#,###,###,###,##0.00; '
    end
    object QrNFeUISSQN_cMunFG: TIntegerField
      FieldName = 'ISSQN_cMunFG'
    end
    object QrNFeUISSQN_cListServ: TIntegerField
      FieldName = 'ISSQN_cListServ'
    end
    object QrNFeUISSQN_cSitTrib: TWideStringField
      FieldName = 'ISSQN_cSitTrib'
      Size = 1
    end
    object QrNFeUISSQN_cSitTrib_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ISSQN_cSitTrib_TXT'
      Calculated = True
    end
  end
  object DsNFeU: TDataSource
    DataSet = QrNFeU
    Left = 92
    Top = 76
  end
  object QrNFeA: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeABeforeClose
    AfterScroll = QrNFeAAfterScroll
    SQL.Strings = (
      'SELECT * FROM _nfe_a_'
      'WHERE SeqArq=:P0')
    Left = 36
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeALoteEnv: TLargeintField
      FieldName = 'LoteEnv'
    end
    object QrNFeAversao: TFloatField
      FieldName = 'versao'
      DisplayFormat = '0.00'
    end
    object QrNFeAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeASeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrNFeASeqNFe: TIntegerField
      FieldName = 'SeqNFe'
    end
  end
  object DsNFeA: TDataSource
    DataSet = QrNFeA
    Left = 64
    Top = 48
  end
  object QrNFeB: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeBCalcFields
    SQL.Strings = (
      'SELECT * FROM _nfe_b_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 92
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeBFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeBFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeBEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeBide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrNFeBide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeBide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrNFeBide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrNFeBide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrNFeBide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFeBide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeBide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeBide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrNFeBide_hSaiEnt: TTimeField
      FieldName = 'ide_hSaiEnt'
    end
    object QrNFeBide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrNFeBide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrNFeBide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrNFeBide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrNFeBide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrNFeBide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrNFeBide_finNFe: TSmallintField
      FieldName = 'ide_finNFe'
    end
    object QrNFeBide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrNFeBide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrNFeBide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrNFeBide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrNFeBAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeBide_dhCont_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ide_dhCont_TXT'
      Calculated = True
    end
    object QrNFeBide_xUF: TWideStringField
      FieldName = 'ide_xUF'
      Size = 2
    end
    object QrNFeBide_xAmb: TWideStringField
      FieldName = 'ide_xAmb'
      Size = 11
    end
    object QrNFeBide_xTpNF: TWideStringField
      FieldName = 'ide_xTpNF'
      Size = 10
    end
    object QrNFeBide_xTpImp: TWideStringField
      FieldName = 'ide_xTpImp'
      Size = 10
    end
    object QrNFeBide_xTpEmis: TWideStringField
      FieldName = 'ide_xTpEmis'
      Size = 100
    end
    object QrNFeBide_xFInNFe: TWideStringField
      FieldName = 'ide_xFInNFe'
      Size = 30
    end
    object QrNFeBide_xMunFG: TWideStringField
      FieldName = 'ide_xMunFG'
      Size = 60
    end
    object QrNFeBide_xIndPag: TWideStringField
      FieldName = 'ide_xIndPag'
      Size = 30
    end
    object QrNFeBide_xProcEmi: TWideStringField
      FieldName = 'ide_xProcEmi'
      Size = 100
    end
  end
  object DsNFeB: TDataSource
    DataSet = QrNFeB
    Left = 120
    Top = 48
  end
  object QrNFeC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_c_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 204
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrNFeCemit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrNFeCemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrNFeCemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrNFeCemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrNFeCemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrNFeCemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrNFeCemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrNFeCemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrNFeCemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrNFeCemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrNFeCemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrNFeCemit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrNFeCemit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrNFeCemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrNFeCemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrNFeCemit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrNFeCemit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrNFeCemit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrNFeCemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
    end
    object QrNFeCemit_xCNAE: TWideStringField
      FieldName = 'emit_xCNAE'
      Size = 100
    end
    object QrNFeCemit_xCRT: TWideStringField
      FieldName = 'emit_xCRT'
      Size = 100
    end
    object QrNFeCCodInfoEmit: TIntegerField
      FieldName = 'CodInfoEmit'
    end
    object QrNFeCNomeEmit: TWideStringField
      FieldName = 'NomeEmit'
      Size = 100
    end
  end
  object DsNFeC: TDataSource
    DataSet = QrNFeC
    Left = 232
    Top = 48
  end
  object QrNFeB12a: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeBCalcFields
    SQL.Strings = (
      'SELECT * FROM _nfe_b_12a'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 148
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeB12aControle: TIntegerField
      FieldName = 'Controle'
      Origin = '_nfe_b_12a.Controle'
    end
    object QrNFeB12arefNFe: TWideStringField
      FieldName = 'refNFe'
      Origin = '_nfe_b_12a.refNFe'
      Size = 44
    end
    object QrNFeB12arefNF_cUF: TSmallintField
      FieldName = 'refNF_cUF'
      Origin = '_nfe_b_12a.refNF_cUF'
    end
    object QrNFeB12arefNF_AAMM: TIntegerField
      FieldName = 'refNF_AAMM'
      Origin = '_nfe_b_12a.refNF_AAMM'
    end
    object QrNFeB12arefNF_CNPJ: TWideStringField
      FieldName = 'refNF_CNPJ'
      Origin = '_nfe_b_12a.refNF_CNPJ'
      Size = 14
    end
    object QrNFeB12arefNF_mod: TSmallintField
      FieldName = 'refNF_mod'
      Origin = '_nfe_b_12a.refNF_mod'
    end
    object QrNFeB12arefNF_serie: TIntegerField
      FieldName = 'refNF_serie'
      Origin = '_nfe_b_12a.refNF_serie'
    end
    object QrNFeB12arefNF_nNF: TIntegerField
      FieldName = 'refNF_nNF'
      Origin = '_nfe_b_12a.refNF_nNF'
    end
    object QrNFeB12arefNFP_cUF: TSmallintField
      FieldName = 'refNFP_cUF'
      Origin = '_nfe_b_12a.refNFP_cUF'
    end
    object QrNFeB12arefNFP_AAMM: TSmallintField
      FieldName = 'refNFP_AAMM'
      Origin = '_nfe_b_12a.refNFP_AAMM'
    end
    object QrNFeB12arefNFP_CNPJ: TWideStringField
      FieldName = 'refNFP_CNPJ'
      Origin = '_nfe_b_12a.refNFP_CNPJ'
      Size = 14
    end
    object QrNFeB12arefNFP_CPF: TWideStringField
      FieldName = 'refNFP_CPF'
      Origin = '_nfe_b_12a.refNFP_CPF'
      Size = 11
    end
    object QrNFeB12arefNFP_IE: TWideStringField
      FieldName = 'refNFP_IE'
      Origin = '_nfe_b_12a.refNFP_IE'
      Size = 14
    end
    object QrNFeB12arefNFP_mod: TSmallintField
      FieldName = 'refNFP_mod'
      Origin = '_nfe_b_12a.refNFP_mod'
    end
    object QrNFeB12arefNFP_serie: TSmallintField
      FieldName = 'refNFP_serie'
      Origin = '_nfe_b_12a.refNFP_serie'
    end
    object QrNFeB12arefNFP_nNF: TIntegerField
      FieldName = 'refNFP_nNF'
      Origin = '_nfe_b_12a.refNFP_nNF'
    end
    object QrNFeB12arefCTe: TWideStringField
      FieldName = 'refCTe'
      Origin = '_nfe_b_12a.refCTe'
      Size = 44
    end
    object QrNFeB12arefECF_mod: TWideStringField
      FieldName = 'refECF_mod'
      Origin = '_nfe_b_12a.refECF_mod'
      Size = 2
    end
    object QrNFeB12arefECF_nECF: TSmallintField
      FieldName = 'refECF_nECF'
      Origin = '_nfe_b_12a.refECF_nECF'
    end
    object QrNFeB12arefECF_nCOO: TIntegerField
      FieldName = 'refECF_nCOO'
      Origin = '_nfe_b_12a.refECF_nCOO'
    end
  end
  object DsNFeB12a: TDataSource
    DataSet = QrNFeB12a
    Left = 176
    Top = 48
  end
  object QrNFeD: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_d_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 260
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeDFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeDFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeDEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeDavulsa_CNPJ: TWideStringField
      FieldName = 'avulsa_CNPJ'
      Size = 14
    end
    object QrNFeDavulsa_xOrgao: TWideStringField
      FieldName = 'avulsa_xOrgao'
      Size = 60
    end
    object QrNFeDavulsa_matr: TWideStringField
      FieldName = 'avulsa_matr'
      Size = 60
    end
    object QrNFeDavulsa_xAgente: TWideStringField
      FieldName = 'avulsa_xAgente'
      Size = 60
    end
    object QrNFeDavulsa_fone: TWideStringField
      FieldName = 'avulsa_fone'
      Size = 14
    end
    object QrNFeDavulsa_UF: TWideStringField
      FieldName = 'avulsa_UF'
      Size = 2
    end
    object QrNFeDavulsa_nDAR: TWideStringField
      FieldName = 'avulsa_nDAR'
      Size = 60
    end
    object QrNFeDavulsa_dEmi: TDateField
      FieldName = 'avulsa_dEmi'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrNFeDavulsa_vDAR: TFloatField
      FieldName = 'avulsa_vDAR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeDavulsa_repEmi: TWideStringField
      FieldName = 'avulsa_repEmi'
      Size = 60
    end
    object QrNFeDavulsa_dPag: TDateField
      FieldName = 'avulsa_dPag'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrNFeDAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeD: TDataSource
    DataSet = QrNFeD
    Left = 288
    Top = 48
  end
  object QrNFeE: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFeEBeforeClose
    AfterScroll = QrNFeEAfterScroll
    SQL.Strings = (
      'SELECT * FROM _nfe_e_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 316
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeEFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeEFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeEEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeEdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrNFeEdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrNFeEdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrNFeEdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrNFeEdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrNFeEdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrNFeEdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrNFeEdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrNFeEdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrNFeEdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrNFeEdest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrNFeEdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrNFeEdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrNFeEdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrNFeEdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrNFeEdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrNFeEdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrNFeEAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFeECodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
    end
    object QrNFeENomeDest: TWideStringField
      FieldName = 'NomeDest'
      Size = 100
    end
    object QrNFeESeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrNFeESeqNFe: TIntegerField
      FieldName = 'SeqNFe'
    end
  end
  object DsNFeE: TDataSource
    DataSet = QrNFeE
    Left = 344
    Top = 48
  end
  object QrNFeF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_f_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 372
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeFFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeFEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeFretirada_CNPJ: TWideStringField
      FieldName = 'retirada_CNPJ'
      Size = 14
    end
    object QrNFeFretirada_CPF: TWideStringField
      FieldName = 'retirada_CPF'
      Size = 11
    end
    object QrNFeFretirada_xLgr: TWideStringField
      FieldName = 'retirada_xLgr'
      Size = 60
    end
    object QrNFeFretirada_nro: TWideStringField
      FieldName = 'retirada_nro'
      Size = 60
    end
    object QrNFeFretirada_xCpl: TWideStringField
      FieldName = 'retirada_xCpl'
      Size = 60
    end
    object QrNFeFretirada_xBairro: TWideStringField
      FieldName = 'retirada_xBairro'
      Size = 60
    end
    object QrNFeFretirada_cMun: TIntegerField
      FieldName = 'retirada_cMun'
    end
    object QrNFeFretirada_xMun: TWideStringField
      FieldName = 'retirada_xMun'
      Size = 60
    end
    object QrNFeFretirada_UF: TWideStringField
      FieldName = 'retirada_UF'
      Size = 2
    end
    object QrNFeFAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeF: TDataSource
    DataSet = QrNFeF
    Left = 400
    Top = 48
  end
  object QrNFeG: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_g_'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 428
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeGFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeGFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeGEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFeGentrega_CNPJ: TWideStringField
      FieldName = 'entrega_CNPJ'
      Size = 14
    end
    object QrNFeGentrega_CPF: TWideStringField
      FieldName = 'entrega_CPF'
      Size = 11
    end
    object QrNFeGentrega_xLgr: TWideStringField
      FieldName = 'entrega_xLgr'
      Size = 60
    end
    object QrNFeGentrega_nro: TWideStringField
      FieldName = 'entrega_nro'
      Size = 60
    end
    object QrNFeGentrega_xCpl: TWideStringField
      FieldName = 'entrega_xCpl'
      Size = 60
    end
    object QrNFeGentrega_xBairro: TWideStringField
      FieldName = 'entrega_xBairro'
      Size = 60
    end
    object QrNFeGentrega_cMun: TIntegerField
      FieldName = 'entrega_cMun'
    end
    object QrNFeGentrega_xMun: TWideStringField
      FieldName = 'entrega_xMun'
      Size = 60
    end
    object QrNFeGentrega_UF: TWideStringField
      FieldName = 'entrega_UF'
      Size = 2
    end
    object QrNFeGAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsNFeG: TDataSource
    DataSet = QrNFeG
    Left = 456
    Top = 48
  end
  object QrNFeW02: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_w_02'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 176
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeW02ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW02ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsNFeW02: TDataSource
    DataSet = QrNFeW02
    Left = 204
    Top = 76
  end
  object QrNFeW17: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_w_17'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 232
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeW17ISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW17ISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW17ISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW17ISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW17ISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsNFeW17: TDataSource
    DataSet = QrNFeW17
    Left = 260
    Top = 76
  end
  object QrNFeW23: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM _nfe_w_23'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 288
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeW23RetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW23RetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW23RetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW23RetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW23RetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW23RetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeW23RetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsNFeW23: TDataSource
    DataSet = QrNFeW23
    Left = 316
    Top = 76
  end
  object QrNFeX01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_x_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 344
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeX01ModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrNFeX01Transporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrNFeX01Transporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrNFeX01Transporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrNFeX01Transporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
    end
    object QrNFeX01Transporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrNFeX01Transporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrNFeX01Transporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrNFeX01RetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeX01RetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeX01RetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeX01RetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeX01RetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrNFeX01RetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrNFeX01VeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrNFeX01VeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrNFeX01VeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrNFeX01Vagao: TWideStringField
      FieldName = 'Vagao'
    end
    object QrNFeX01Balsa: TWideStringField
      FieldName = 'Balsa'
    end
    object QrNFeX01xModFrete: TWideStringField
      FieldName = 'xModFrete'
      Size = 50
    end
    object QrNFeX01RetTransp_xMunFG: TWideStringField
      FieldName = 'RetTransp_xMunFG'
      Size = 50
    end
  end
  object DsNFeX01: TDataSource
    DataSet = QrNFeX01
    Left = 372
    Top = 76
  end
  object QrNFeY01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_y_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 568
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeY01Cobr_Fat_nFat: TWideStringField
      FieldName = 'Cobr_Fat_nFat'
      Size = 60
    end
    object QrNFeY01Cobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeY01Cobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrNFeY01Cobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsNFeY01: TDataSource
    DataSet = QrNFeY01
    Left = 596
    Top = 76
  end
  object QrNFeZ01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_z_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 680
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZ01InfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFeZ01InfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsNFeZ01: TDataSource
    DataSet = QrNFeZ01
    Left = 708
    Top = 76
  end
  object QrNFeZA01: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeZ10CalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_za_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 904
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZA01Exporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrNFeZA01Exporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
  end
  object DsNFeZA01: TDataSource
    DataSet = QrNFeZA01
    Left = 932
    Top = 76
  end
  object QrNFeZB01: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFeZ10CalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_zb_01'
      'WHERE SeqArq=:P0'
      'AND SeqNFe=:P1')
    Left = 960
    Top = 76
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeZB01Compra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrNFeZB01Compra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrNFeZB01Compra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
  end
  object DsNFeZB01: TDataSource
    DataSet = QrNFeZB01
    Left = 984
    Top = 32
  end
  object QrCP01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_cp01_'
      'WHERE SeqArq=:P0')
    Left = 84
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCP01cancNFe_versao: TFloatField
      FieldName = 'cancNFe_versao'
      Origin = '_nfe_cp01_.cancNFe_versao'
    end
    object QrCP01infCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Origin = '_nfe_cp01_.infCanc_Id'
      Size = 46
    end
    object QrCP01infCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
      Origin = '_nfe_cp01_.infCanc_tpAmb'
    end
    object QrCP01infCanc_xAmb: TWideStringField
      FieldName = 'infCanc_xAmb'
      Origin = '_nfe_cp01_.infCanc_xAmb'
      Size = 11
    end
    object QrCP01infCanc_xServ: TWideStringField
      FieldName = 'infCanc_xServ'
      Origin = '_nfe_cp01_.infCanc_xServ'
      Size = 8
    end
    object QrCP01infCanc_chNFe: TWideStringField
      FieldName = 'infCanc_chNFe'
      Origin = '_nfe_cp01_.infCanc_chNFe'
      Size = 44
    end
    object QrCP01infCanc_nProt: TLargeintField
      FieldName = 'infCanc_nProt'
      Origin = '_nfe_cp01_.infCanc_nProt'
    end
    object QrCP01infCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Origin = '_nfe_cp01_.infCanc_xJust'
      Size = 255
    end
  end
  object DsCP01: TDataSource
    DataSet = QrCP01
    Left = 112
    Top = 636
  end
  object QrCR01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_cr01_'
      'WHERE SeqArq=:P0')
    Left = 140
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCR01retCanc_versao: TFloatField
      FieldName = 'retCanc_versao'
      Origin = '_nfe_cr01_.retCanc_versao'
    end
    object QrCR01infCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Origin = '_nfe_cr01_.infCanc_Id'
      Size = 60
    end
    object QrCR01infCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
      Origin = '_nfe_cr01_.infCanc_tpAmb'
    end
    object QrCR01infCanc_xAmb: TWideStringField
      FieldName = 'infCanc_xAmb'
      Origin = '_nfe_cr01_.infCanc_xAmb'
      Size = 11
    end
    object QrCR01infCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Origin = '_nfe_cr01_.infCanc_verAplic'
    end
    object QrCR01infCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = '_nfe_cr01_.infCanc_cStat'
    end
    object QrCR01infCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Origin = '_nfe_cr01_.infCanc_xMotivo'
      Size = 255
    end
    object QrCR01infCanc_cUF: TSmallintField
      FieldName = 'infCanc_cUF'
      Origin = '_nfe_cr01_.infCanc_cUF'
    end
    object QrCR01infCanc_xUF: TWideStringField
      FieldName = 'infCanc_xUF'
      Origin = '_nfe_cr01_.infCanc_xUF'
      Size = 2
    end
    object QrCR01infCanc_chNFe: TWideStringField
      FieldName = 'infCanc_chNFe'
      Origin = '_nfe_cr01_.infCanc_chNFe'
      Size = 44
    end
    object QrCR01infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
      Origin = '_nfe_cr01_.infCanc_dhRecbto'
    end
    object QrCR01infCanc_nProt: TLargeintField
      FieldName = 'infCanc_nProt'
      Origin = '_nfe_cr01_.infCanc_nProt'
    end
  end
  object DsCR01: TDataSource
    DataSet = QrCR01
    Left = 168
    Top = 636
  end
  object QrYR01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_yr01_'
      'WHERE SeqArq=:P0')
    Left = 252
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrYR01procCancNFe_versao: TFloatField
      FieldName = 'procCancNFe_versao'
    end
  end
  object DsYR01: TDataSource
    DataSet = QrYR01
    Left = 280
    Top = 636
  end
  object QrArqs: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrArqsBeforeClose
    AfterScroll = QrArqsAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_arqs_')
    Left = 28
    Top = 636
    object QrArqsSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrArqsVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrArqsCodificacao: TWideStringField
      FieldName = 'Codificacao'
      Size = 30
    end
    object QrArqsLocalName: TWideStringField
      FieldName = 'LocalName'
      Size = 50
    end
    object QrArqsPrefix: TWideStringField
      FieldName = 'Prefix'
    end
    object QrArqsNodeName: TWideStringField
      FieldName = 'NodeName'
      Size = 100
    end
    object QrArqsNamespaceURI: TWideStringField
      FieldName = 'NamespaceURI'
      Size = 255
    end
    object QrArqsArqDir: TWideStringField
      FieldName = 'ArqDir'
      Size = 255
    end
    object QrArqsArqName: TWideStringField
      FieldName = 'ArqName'
      Size = 255
    end
    object QrArqsxTipoDoc: TWideStringField
      FieldName = 'xTipoDoc'
      Size = 255
    end
    object QrArqsAssinado: TSmallintField
      FieldName = 'Assinado'
    end
  end
  object DsArqs: TDataSource
    DataSet = QrArqs
    Left = 56
    Top = 636
  end
  object QrPR01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_pr01_'
      'WHERE SeqArq=:P0')
    Left = 136
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPR01protNFe_versao: TFloatField
      FieldName = 'protNFe_versao'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPR01infProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 46
    end
    object QrPR01infProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrPR01infProt_xAmb: TWideStringField
      FieldName = 'infProt_xAmb'
      Size = 11
    end
    object QrPR01infProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
    end
    object QrPR01infProt_chNFe: TWideStringField
      FieldName = 'infProt_chNFe'
      Size = 44
    end
    object QrPR01infProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrPR01infProt_nProt: TLargeintField
      FieldName = 'infProt_nProt'
    end
    object QrPR01infProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrPR01infProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrPR01infProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
  end
  object DsPR01: TDataSource
    DataSet = QrPR01
    Left = 164
    Top = 592
  end
  object QrXR01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_xr01_'
      'WHERE SeqArq=:P0')
    Left = 196
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrXR01nfeProcNFe_versao: TFloatField
      FieldName = 'nfeProcNFe_versao'
      DisplayFormat = '0.00;-0.00; '
    end
  end
  object DsXR01: TDataSource
    DataSet = QrXR01
    Left = 224
    Top = 636
  end
  object QrFilial: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodFilial '
      'FROM enticliint'
      'WHERE CodEnti=:P0')
    Left = 28
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFilialCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
  end
  object QrLocGGX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM gragrux'
      'WHERE Controle=:P0')
    Left = 56
    Top = 608
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocGGXItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrDP01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_dp01_'
      'WHERE SeqArq=:P0')
    Left = 308
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDP01inutNFe_versao: TFloatField
      FieldName = 'inutNFe_versao'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrDP01inutNFe_Id: TWideStringField
      FieldName = 'inutNFe_Id'
      Size = 43
    end
    object QrDP01inutNFe_tpAmb: TSmallintField
      FieldName = 'inutNFe_tpAmb'
    end
    object QrDP01inutNFe_xAmb: TWideStringField
      FieldName = 'inutNFe_xAmb'
      Size = 11
    end
    object QrDP01inutNFe_xServ: TWideStringField
      FieldName = 'inutNFe_xServ'
      Size = 10
    end
    object QrDP01inutNFe_cUF: TSmallintField
      FieldName = 'inutNFe_cUF'
    end
    object QrDP01inutNFe_xUF: TWideStringField
      FieldName = 'inutNFe_xUF'
      Size = 2
    end
    object QrDP01inutNFe_ano: TSmallintField
      FieldName = 'inutNFe_ano'
      DisplayFormat = '00'
    end
    object QrDP01inutNFe_CNPJ: TWideStringField
      FieldName = 'inutNFe_CNPJ'
      Size = 14
    end
    object QrDP01inutNFe_mod: TSmallintField
      FieldName = 'inutNFe_mod'
      DisplayFormat = '00'
    end
    object QrDP01inutNFe_serie: TIntegerField
      FieldName = 'inutNFe_serie'
      DisplayFormat = '000'
    end
    object QrDP01inutNFe_nNFIni: TIntegerField
      FieldName = 'inutNFe_nNFIni'
      DisplayFormat = '000000000'
    end
    object QrDP01inutNFe_nNFFin: TIntegerField
      FieldName = 'inutNFe_nNFFin'
      DisplayFormat = '000000000'
    end
    object QrDP01inutNFe_xJust: TWideStringField
      FieldName = 'inutNFe_xJust'
      Size = 255
    end
  end
  object DsDP01: TDataSource
    DataSet = QrDP01
    Left = 336
    Top = 636
  end
  object QrDR01: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDR01CalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_dr01_'
      'WHERE SeqArq=:P0')
    Left = 364
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDR01retInutNFe_versao: TFloatField
      FieldName = 'retInutNFe_versao'
    end
    object QrDR01retInutNFe_Id: TWideStringField
      FieldName = 'retInutNFe_Id'
      Size = 17
    end
    object QrDR01retInutNFe_tpAmb: TSmallintField
      FieldName = 'retInutNFe_tpAmb'
    end
    object QrDR01retInutNFe_xAmb: TWideStringField
      FieldName = 'retInutNFe_xAmb'
      Size = 11
    end
    object QrDR01retInutNFe_verAplic: TWideStringField
      FieldName = 'retInutNFe_verAplic'
    end
    object QrDR01retInutNFe_cStat: TIntegerField
      FieldName = 'retInutNFe_cStat'
    end
    object QrDR01retInutNFe_xMotivo: TWideStringField
      FieldName = 'retInutNFe_xMotivo'
      Size = 255
    end
    object QrDR01retInutNFe_cUF: TSmallintField
      FieldName = 'retInutNFe_cUF'
    end
    object QrDR01retInutNFe_xUF: TWideStringField
      FieldName = 'retInutNFe_xUF'
      Size = 2
    end
    object QrDR01retInutNFe_ano: TSmallintField
      FieldName = 'retInutNFe_ano'
    end
    object QrDR01retInutNFe_CNPJ: TWideStringField
      FieldName = 'retInutNFe_CNPJ'
      Size = 14
    end
    object QrDR01retInutNFe_mod: TSmallintField
      FieldName = 'retInutNFe_mod'
    end
    object QrDR01retInutNFe_serie: TIntegerField
      FieldName = 'retInutNFe_serie'
    end
    object QrDR01retInutNFe_nNFIni: TIntegerField
      FieldName = 'retInutNFe_nNFIni'
    end
    object QrDR01retInutNFe_nNFFin: TIntegerField
      FieldName = 'retInutNFe_nNFFin'
    end
    object QrDR01retInutNFe_dhRecbto: TDateTimeField
      FieldName = 'retInutNFe_dhRecbto'
    end
    object QrDR01retInutNFe_nProt: TLargeintField
      FieldName = 'retInutNFe_nProt'
    end
    object QrDR01retInutNFe_dhRecbto_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'retInutNFe_dhRecbto_TXT'
      Calculated = True
    end
  end
  object DsDR01: TDataSource
    DataSet = QrDR01
    Left = 392
    Top = 636
  end
  object QrZR01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _nfe_zr01_'
      'WHERE SeqArq=:P0')
    Left = 420
    Top = 636
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrZR01procInutNFe_versao: TFloatField
      FieldName = 'procInutNFe_versao'
      DisplayFormat = '0.00;-0.00; '
    end
  end
  object DsZR01: TDataSource
    DataSet = QrZR01
    Left = 448
    Top = 636
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 116
    Top = 688
    object Cdigo1: TMenuItem
      Caption = 'C'#243'digo'
      object DaentidadedoEmitente1: TMenuItem
        Caption = 'Da entidade do &Emitente'
        Enabled = False
      end
      object DaentidadedoDestinatrio1: TMenuItem
        Caption = 'Da entidade do &Destinat'#225'rio'
        Enabled = False
        OnClick = DaentidadedoDestinatrio1Click
      end
      object Doreduzidodoproduto1: TMenuItem
        Caption = 'Do &Reduzido do produto'
        Enabled = False
      end
    end
  end
end
