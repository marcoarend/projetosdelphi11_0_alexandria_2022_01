unit NFeLoad_X01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_X01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel20: TPanel;
    Label177: TLabel;
    Label178: TLabel;
    Label179: TLabel;
    Label180: TLabel;
    Label181: TLabel;
    Label182: TLabel;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label216: TLabel;
    Label217: TLabel;
    SpeedButton8: TSpeedButton;
    Label218: TLabel;
    Label219: TLabel;
    EdModFrete: TdmkEdit;
    EdModFrete_TXT: TdmkEdit;
    Edtransporta_CNPJ: TdmkEdit;
    Edtransporta_CPF: TdmkEdit;
    Edtransporta_xNome: TdmkEdit;
    EdTransporta_IE: TdmkEdit;
    EdTransporta_XEnder: TdmkEdit;
    EdTransporta_XMun: TdmkEdit;
    EdTransporta_UF: TdmkEdit;
    EdRetTransp_vServ: TdmkEdit;
    EdRetTransp_vBCRet: TdmkEdit;
    EdRetTransp_PICMSRet: TdmkEdit;
    EdRetTransp_vICMSRet: TdmkEdit;
    EdRetTransp_CFOP: TdmkEdit;
    EdRetTransp_CMunFG: TdmkEdit;
    EdVeicTransp_Placa: TdmkEdit;
    EdVeicTransp_UF: TdmkEdit;
    EdVeicTransp_RNTC: TdmkEdit;
    EdRetTransp_CMunFG_TXT: TdmkEdit;
    Edvagao: TdmkEdit;
    Edbalsa: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdModFreteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_X01: TFmNFeLoad_X01;

implementation

uses UnMyObjects, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeLoad_X01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_X01.EdModFreteChange(Sender: TObject);
begin
  EdModFrete_TXT.Text :=
    DmNFe_0000.TextoDeCodigoNFe(nfeCTModFrete, EdModFrete.ValueVariant);
end;

procedure TFmNFeLoad_X01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_X01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_X01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
