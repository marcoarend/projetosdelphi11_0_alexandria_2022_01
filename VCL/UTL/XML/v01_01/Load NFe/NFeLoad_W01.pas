unit NFeLoad_W01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_W01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel17: TPanel;
    Panel18: TPanel;
    GroupBox7: TGroupBox;
    Label151: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label154: TLabel;
    Label155: TLabel;
    EdISSQNtot_vServ: TdmkEdit;
    EdISSQNtot_vBC: TdmkEdit;
    EdISSQNtot_vISS: TdmkEdit;
    EdISSQNtot_vPIS: TdmkEdit;
    EdISSQNtot_vCOFINS: TdmkEdit;
    Panel19: TPanel;
    GroupBox8: TGroupBox;
    Label156: TLabel;
    Label157: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    Label162: TLabel;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    Label166: TLabel;
    Label167: TLabel;
    Label168: TLabel;
    Label169: TLabel;
    EdICMSTot_vBC: TdmkEdit;
    EdICMSTot_vICMS: TdmkEdit;
    EdICMSTot_vBCST: TdmkEdit;
    EdICMSTot_vST: TdmkEdit;
    EdICMSTot_vProd: TdmkEdit;
    EdICMSTot_vFrete: TdmkEdit;
    EdICMSTot_vSeg: TdmkEdit;
    EdICMSTot_vDesc: TdmkEdit;
    EdICMSTot_vII: TdmkEdit;
    EdICMSTot_vIPI: TdmkEdit;
    EdICMSTot_vPIS: TdmkEdit;
    EdICMSTot_vCOFINS: TdmkEdit;
    EdICMSTot_vOutro: TdmkEdit;
    EdICMSTot_vNF: TdmkEdit;
    GroupBox9: TGroupBox;
    Label170: TLabel;
    Label171: TLabel;
    Label172: TLabel;
    Label173: TLabel;
    Label174: TLabel;
    Label175: TLabel;
    Label176: TLabel;
    EdRetTrib_vRetPIS: TdmkEdit;
    EdRetTrib_vRetCOFINS: TdmkEdit;
    EdRetTrib_vRetCSLL: TdmkEdit;
    EdRetTrib_vBCIRRF: TdmkEdit;
    EdRetTrib_vIRRF: TdmkEdit;
    EdRetTrib_vBCRetPrev: TdmkEdit;
    EdRetTrib_vRetPrev: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_W01: TFmNFeLoad_W01;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmNFeLoad_W01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_W01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_W01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_W01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
