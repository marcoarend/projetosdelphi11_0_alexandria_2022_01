unit NFeLoad_C01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_C01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel56: TPanel;
    Label359: TLabel;
    EdCodInfoEmit: TdmkEdit;
    EdNomeEmit: TdmkEdit;
    Panel16: TPanel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    Label7: TLabel;
    Edemit_CNPJ: TdmkEdit;
    Edemit_xNome: TdmkEdit;
    Edemit_xFant: TdmkEdit;
    Edemit_xLgr: TdmkEdit;
    Edemit_nro: TdmkEdit;
    Edemit_xCpl: TdmkEdit;
    Edemit_xBairro: TdmkEdit;
    Edemit_cMun: TdmkEdit;
    Edemit_UF: TdmkEdit;
    Edemit_CEP: TdmkEdit;
    Edemit_cPais: TdmkEdit;
    Edemit_fone: TdmkEdit;
    Edemit_IE: TdmkEdit;
    Edemit_IEST: TdmkEdit;
    Edemit_IM: TdmkEdit;
    Edemit_xMun: TdmkEdit;
    Edemit_xPais: TdmkEdit;
    Edemit_CPF: TdmkEdit;
    Edemit_CNAE: TdmkEdit;
    Edemit_CNAE_TXT: TdmkEdit;
    Edemit_CRT: TdmkEdit;
    Edemit_CRT_TXT: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edemit_CNPJChange(Sender: TObject);
    procedure Edemit_CPFChange(Sender: TObject);
    procedure Edemit_cPaisChange(Sender: TObject);
    procedure Edemit_UFChange(Sender: TObject);
    procedure Edemit_cMunChange(Sender: TObject);
    procedure Edemit_CNAEChange(Sender: TObject);
    procedure Edemit_CRTChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DefineEmitente();
  end;

  var
  FmNFeLoad_C01: TFmNFeLoad_C01;

implementation

uses UnMyObjects, ModuleNFe_0000, ModuleGeral, UnFinanceiro;

{$R *.DFM}

procedure TFmNFeLoad_C01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_C01.DefineEmitente();
var
  CNPJ, CPF, Nome: String;
  Codigo: Integer;
begin
  Nome   := '';
  Codigo := 0;
  CNPJ   := Geral.SoNumero_TT(Edemit_CNPJ.Text);
  CPF    := Geral.SoNumero_TT(Edemit_CPF.Text);
  if CNPJ <> '' then
    DModG.ObtemEntidadeDeCNPJCFP(CNPJ, Codigo)
  else
    DModG.ObtemEntidadeDeCNPJCFP(CPF, Codigo);
  if Codigo <> 0 then
  begin
    if DModG.ReopenEndereco(Codigo) then
      Nome := DModG.QrEnderecoNOME_ENT.Value;
  end;
  EdCodInfoEmit.ValueVariant := Codigo;
  EdNomeEmit.Text := Nome;
end;

procedure TFmNFeLoad_C01.Edemit_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edemit_cMun.ValueVariant, []) then
    Edemit_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Edemit_xMun.Text := '';
end;

procedure TFmNFeLoad_C01.Edemit_CNAEChange(Sender: TObject);
begin
  if DmNFe_0000.QrCNAE.Locate('CodTxt', Edemit_CNAE.Text, []) then
    Edemit_CNAE_TXT.Text := DmNFe_0000.QrCNAENome.Value
  else
    Edemit_CNAE_TXT.Text := '';
end;

procedure TFmNFeLoad_C01.Edemit_CNPJChange(Sender: TObject);
begin
  DefineEmitente()
end;

procedure TFmNFeLoad_C01.Edemit_cPaisChange(Sender: TObject);
begin
  if DmNFe_0000.QrBACEN_Pais.Locate('Codigo', Edemit_cPais.ValueVariant, []) then
    Edemit_xPais.Text := DmNFe_0000.QrBACEN_PaisNome.Value
  else
    Edemit_xPais.Text := '';
end;

procedure TFmNFeLoad_C01.Edemit_CPFChange(Sender: TObject);
begin
  DefineEmitente();
end;

procedure TFmNFeLoad_C01.Edemit_CRTChange(Sender: TObject);
begin
  Edemit_CRT_TXT.Text := UFinanceiro.CRT_Get(Edemit_CRT.ValueVariant);
end;

procedure TFmNFeLoad_C01.Edemit_UFChange(Sender: TObject);
begin
  Edemit_IE.LinkMsk := Edemit_UF.Text;
end;

procedure TFmNFeLoad_C01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_C01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_C01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
