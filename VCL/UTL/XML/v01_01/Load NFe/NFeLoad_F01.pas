unit NFeLoad_F01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_F01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    Label25: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Edretirada_CNPJ: TdmkEdit;
    Edretirada_xLgr: TdmkEdit;
    Edretirada_nro: TdmkEdit;
    Edretirada_xCpl: TdmkEdit;
    Edretirada_xBairro: TdmkEdit;
    Edretirada_cMun: TdmkEdit;
    Edretirada_UF: TdmkEdit;
    Edretirada_xMun: TdmkEdit;
    Edretirada_CPF: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edretirada_cMunChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_F01: TFmNFeLoad_F01;

implementation

uses UnMyObjects, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeLoad_F01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_F01.Edretirada_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edretirada_cMun.ValueVariant, []) then
    Edretirada_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Edretirada_xMun.Text := '';
end;

procedure TFmNFeLoad_F01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_F01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_F01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
