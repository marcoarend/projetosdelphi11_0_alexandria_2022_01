unit NFeLoad_Arq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, Variants,
  mySQLDbTables, ComCtrls, XMLDoc, XMLIntf, xmldom, msxmldom, StrUtils,
  dmkEditDateTimePicker, Mask, dmkLabelRotate, dmkMemo, dmkCheckBox, Menus,
  UnDmkProcFunc, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmNFeLoad_Arq = class(TForm)
    PnAbre: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PnCarrega: TPanel;
    EdArquivo: TEdit;
    Label110: TLabel;
    SBArquivo: TSpeedButton;
    PB1: TProgressBar;
    SbCarrega: TSpeedButton;
    XMLDoc: TXMLDocument;
    QrNFeI: TmySQLQuery;
    DsNFeI: TDataSource;
    QrNFeIFatID: TIntegerField;
    QrNFeIFatNum: TIntegerField;
    QrNFeIEmpresa: TIntegerField;
    QrNFeInItem: TIntegerField;
    QrNFeIprod_cProd: TWideStringField;
    QrNFeIprod_cEAN: TWideStringField;
    QrNFeIprod_xProd: TWideStringField;
    QrNFeIprod_NCM: TWideStringField;
    QrNFeIprod_EXTIPI: TWideStringField;
    QrNFeIprod_CFOP: TIntegerField;
    QrNFeIprod_uCom: TWideStringField;
    QrNFeIprod_qCom: TFloatField;
    QrNFeIprod_vUnCom: TFloatField;
    QrNFeIprod_vProd: TFloatField;
    QrNFeIprod_cEANTrib: TWideStringField;
    QrNFeIprod_uTrib: TWideStringField;
    QrNFeIprod_qTrib: TFloatField;
    QrNFeIprod_vUnTrib: TFloatField;
    QrNFeIprod_vFrete: TFloatField;
    QrNFeIprod_vSeg: TFloatField;
    QrNFeIprod_vDesc: TFloatField;
    QrNFeIprod_vOutro: TFloatField;
    QrNFeIprod_indTot: TSmallintField;
    QrNFeIprod_xPed: TWideStringField;
    QrNFeIprod_nItemPed: TIntegerField;
    QrNFeIAtivo: TSmallintField;
    QrNFeN: TmySQLQuery;
    DsNFeN: TDataSource;
    QrNFeO: TmySQLQuery;
    DsNFeO: TDataSource;
    DsNFeQ: TDataSource;
    QrNFeQ: TmySQLQuery;
    QrNFeP: TmySQLQuery;
    DsNFeP: TDataSource;
    DsNFeV: TDataSource;
    QrNFeV: TmySQLQuery;
    QrNFeS: TmySQLQuery;
    DsNFeS: TDataSource;
    QrNFeNFatID: TIntegerField;
    QrNFeNFatNum: TIntegerField;
    QrNFeNEmpresa: TIntegerField;
    QrNFeNnItem: TIntegerField;
    QrNFeNICMS_Orig: TSmallintField;
    QrNFeNICMS_CST: TSmallintField;
    QrNFeNICMS_modBC: TSmallintField;
    QrNFeNICMS_pRedBC: TFloatField;
    QrNFeNICMS_vBC: TFloatField;
    QrNFeNICMS_pICMS: TFloatField;
    QrNFeNICMS_vICMS: TFloatField;
    QrNFeNICMS_modBCST: TSmallintField;
    QrNFeNICMS_pMVAST: TFloatField;
    QrNFeNICMS_pRedBCST: TFloatField;
    QrNFeNICMS_vBCST: TFloatField;
    QrNFeNICMS_pICMSST: TFloatField;
    QrNFeNICMS_vICMSST: TFloatField;
    QrNFeNICMS_CSOSN: TIntegerField;
    QrNFeNICMS_UFST: TWideStringField;
    QrNFeNICMS_pBCOp: TFloatField;
    QrNFeNICMS_vBCSTRet: TFloatField;
    QrNFeNICMS_vICMSSTRet: TFloatField;
    QrNFeNICMS_motDesICMS: TSmallintField;
    QrNFeNICMS_pCredSN: TFloatField;
    QrNFeNICMS_vCredICMSSN: TFloatField;
    QrNFeNAtivo: TSmallintField;
    QrNFeOFatID: TIntegerField;
    QrNFeOFatNum: TIntegerField;
    QrNFeOEmpresa: TIntegerField;
    QrNFeOnItem: TIntegerField;
    QrNFeOIPI_clEnq: TWideStringField;
    QrNFeOIPI_CNPJProd: TWideStringField;
    QrNFeOIPI_cSelo: TWideStringField;
    QrNFeOIPI_qSelo: TFloatField;
    QrNFeOIPI_cEnq: TWideStringField;
    QrNFeOIPI_CST: TSmallintField;
    QrNFeOIPI_vBC: TFloatField;
    QrNFeOIPI_qUnid: TFloatField;
    QrNFeOIPI_vUnid: TFloatField;
    QrNFeOIPI_pIPI: TFloatField;
    QrNFeOIPI_vIPI: TFloatField;
    QrNFeOAtivo: TSmallintField;
    QrNFePFatID: TIntegerField;
    QrNFePFatNum: TIntegerField;
    QrNFePEmpresa: TIntegerField;
    QrNFePnItem: TIntegerField;
    QrNFePII_vBC: TFloatField;
    QrNFePII_vDespAdu: TFloatField;
    QrNFePII_vII: TFloatField;
    QrNFePII_vIOF: TFloatField;
    QrNFePAtivo: TSmallintField;
    QrNFeQFatID: TIntegerField;
    QrNFeQFatNum: TIntegerField;
    QrNFeQEmpresa: TIntegerField;
    QrNFeQnItem: TIntegerField;
    QrNFeQPIS_CST: TSmallintField;
    QrNFeQPIS_vBC: TFloatField;
    QrNFeQPIS_pPIS: TFloatField;
    QrNFeQPIS_vPIS: TFloatField;
    QrNFeQPIS_qBCProd: TFloatField;
    QrNFeQPIS_vAliqProd: TFloatField;
    QrNFeQPIS_fatorBC: TFloatField;
    QrNFeQAtivo: TSmallintField;
    QrNFeSFatID: TIntegerField;
    QrNFeSFatNum: TIntegerField;
    QrNFeSEmpresa: TIntegerField;
    QrNFeSnItem: TIntegerField;
    QrNFeSCOFINS_CST: TSmallintField;
    QrNFeSCOFINS_vBC: TFloatField;
    QrNFeSCOFINS_pCOFINS: TFloatField;
    QrNFeSCOFINS_qBCProd: TFloatField;
    QrNFeSCOFINS_vAliqProd: TFloatField;
    QrNFeSCOFINS_vCOFINS: TFloatField;
    QrNFeSCOFINS_fatorBC: TFloatField;
    QrNFeSAtivo: TSmallintField;
    QrNFeVFatID: TIntegerField;
    QrNFeVFatNum: TIntegerField;
    QrNFeVEmpresa: TIntegerField;
    QrNFeVnItem: TIntegerField;
    QrNFeVInfAdProd: TWideMemoField;
    QrNFeVAtivo: TSmallintField;
    QrNFeIDi: TmySQLQuery;
    DsNFeIDi: TDataSource;
    QrNFeIDiFatID: TIntegerField;
    QrNFeIDiFatNum: TIntegerField;
    QrNFeIDiEmpresa: TIntegerField;
    QrNFeIDinItem: TIntegerField;
    QrNFeIDiControle: TIntegerField;
    QrNFeIDiDI_nDI: TWideStringField;
    QrNFeIDiDI_dDI: TDateField;
    QrNFeIDiDI_xLocDesemb: TWideStringField;
    QrNFeIDiDI_UFDesemb: TWideStringField;
    QrNFeIDiDI_dDesemb: TDateField;
    QrNFeIDiDI_cExportador: TWideStringField;
    QrNFeIDiAtivo: TSmallintField;
    QrNFeIDiA: TmySQLQuery;
    DsNFeIDiA: TDataSource;
    QrNFeIDiAFatID: TIntegerField;
    QrNFeIDiAFatNum: TIntegerField;
    QrNFeIDiAEmpresa: TIntegerField;
    QrNFeIDiAnItem: TIntegerField;
    QrNFeIDiAControle: TIntegerField;
    QrNFeIDiAConta: TIntegerField;
    QrNFeIDiAAdi_nAdicao: TIntegerField;
    QrNFeIDiAAdi_nSeqAdic: TIntegerField;
    QrNFeIDiAAdi_cFabricante: TWideStringField;
    QrNFeIDiAAdi_vDescDI: TFloatField;
    QrNFeIDiAAtivo: TSmallintField;
    QrNFeIDiADI_nDI: TWideStringField;
    QrNFeR: TmySQLQuery;
    DsNFeR: TDataSource;
    QrNFeT: TmySQLQuery;
    DsNFeT: TDataSource;
    QrNFeTFatID: TIntegerField;
    QrNFeTFatNum: TIntegerField;
    QrNFeTEmpresa: TIntegerField;
    QrNFeTnItem: TIntegerField;
    QrNFeTCOFINSST_vBC: TFloatField;
    QrNFeTCOFINSST_pCOFINS: TFloatField;
    QrNFeTCOFINSST_qBCProd: TFloatField;
    QrNFeTCOFINSST_vAliqProd: TFloatField;
    QrNFeTCOFINSST_vCOFINS: TFloatField;
    QrNFeTCOFINSST_fatorBCST: TFloatField;
    QrNFeTAtivo: TSmallintField;
    QrNFeRFatID: TIntegerField;
    QrNFeRFatNum: TIntegerField;
    QrNFeREmpresa: TIntegerField;
    QrNFeRnItem: TIntegerField;
    QrNFeRPISST_vBC: TFloatField;
    QrNFeRPISST_pPIS: TFloatField;
    QrNFeRPISST_qBCProd: TFloatField;
    QrNFeRPISST_vAliqProd: TFloatField;
    QrNFeRPISST_vPIS: TFloatField;
    QrNFeRPISST_fatorBCST: TFloatField;
    QrNFeRAtivo: TSmallintField;
    QrNFeX22: TmySQLQuery;
    DsNFeX22: TDataSource;
    QrNFeX26: TmySQLQuery;
    DsNFeX26: TDataSource;
    QrNFeX33: TmySQLQuery;
    DsNFeX33: TDataSource;
    QrNFeX22placa: TWideStringField;
    QrNFeX22UF: TWideStringField;
    QrNFeX22RNTC: TWideStringField;
    QrNFeX26qVol: TFloatField;
    QrNFeX26esp: TWideStringField;
    QrNFeX26marca: TWideStringField;
    QrNFeX26nVol: TWideStringField;
    QrNFeX26pesoL: TFloatField;
    QrNFeX26pesoB: TFloatField;
    QrNFeX33nLacre: TWideStringField;
    QrNFeX33Controle: TIntegerField;
    QrNFeX33Conta: TIntegerField;
    QrNFeX26Controle: TIntegerField;
    QrNFeX22Controle: TIntegerField;
    QrNFeY07: TmySQLQuery;
    DsNFeY07: TDataSource;
    QrNFeY07Controle: TIntegerField;
    QrNFeY07nDup: TWideStringField;
    QrNFeY07dVenc: TDateField;
    QrNFeY07vDup: TFloatField;
    QrNFeZ04: TmySQLQuery;
    DsNFeZ04: TDataSource;
    QrNFeZ07: TmySQLQuery;
    DsNFeZ07: TDataSource;
    QrNFeZ04xCampo: TWideStringField;
    QrNFeZ04xTexto: TWideStringField;
    QrNFeZ07xCampo: TWideStringField;
    QrNFeZ07xTexto: TWideStringField;
    QrNFeZ10: TmySQLQuery;
    DsNFeZ10: TDataSource;
    QrNFeZ10nProc: TWideStringField;
    QrNFeZ10indProc: TSmallintField;
    QrNFeZ10indProc_TXT: TWideStringField;
    QrNFeU: TmySQLQuery;
    DsNFeU: TDataSource;
    QrNFeUISSQN_vBC: TFloatField;
    QrNFeUISSQN_vAliq: TFloatField;
    QrNFeUISSQN_vISSQN: TFloatField;
    QrNFeUISSQN_cMunFG: TIntegerField;
    QrNFeUISSQN_cListServ: TIntegerField;
    QrNFeUISSQN_cSitTrib: TWideStringField;
    QrNFeUISSQN_cSitTrib_TXT: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    Panel22: TPanel;
    Label195: TLabel;
    Label196: TLabel;
    Label197: TLabel;
    Label198: TLabel;
    Label199: TLabel;
    Label200: TLabel;
    Label201: TLabel;
    Label202: TLabel;
    Label203: TLabel;
    Label204: TLabel;
    Label205: TLabel;
    Label206: TLabel;
    Label207: TLabel;
    Label208: TLabel;
    Label209: TLabel;
    Label210: TLabel;
    Label193: TLabel;
    Edide_tpEmis: TDBEdit;
    Edide_xIndPag: TDBEdit;
    Edide_tpNF_TXT: TDBEdit;
    Edide_cMunFG_TXT: TDBEdit;
    Edide_TpImp_TXT: TDBEdit;
    Edide_tpEmis_TXT: TDBEdit;
    Edide_xAmb: TDBEdit;
    Edide_finNFe_TXT: TDBEdit;
    Edide_procEmi_TXT: TDBEdit;
    Edide_xUF: TDBEdit;
    TabSheet5: TTabSheet;
    Panel56: TPanel;
    Label359: TLabel;
    EdCodInfoEmit: TDBEdit;
    EdNomeEmit: TDBEdit;
    Panel16: TPanel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    Label7: TLabel;
    Edemit_CNPJ: TDBEdit;
    Edemit_xNome: TDBEdit;
    Edemit_xFant: TDBEdit;
    Edemit_xLgr: TDBEdit;
    Edemit_nro: TDBEdit;
    Edemit_xCpl: TDBEdit;
    Edemit_xBairro: TDBEdit;
    Edemit_cMun: TDBEdit;
    Edemit_UF: TDBEdit;
    Edemit_CEP: TDBEdit;
    Edemit_cPais: TDBEdit;
    Edemit_fone: TDBEdit;
    Edemit_IE: TDBEdit;
    Edemit_IEST: TDBEdit;
    Edemit_IM: TDBEdit;
    Edemit_xMun: TDBEdit;
    Edemit_xPais: TDBEdit;
    Edemit_CPF: TDBEdit;
    Edemit_CNAE: TDBEdit;
    Edemit_xCNAE: TDBEdit;
    Edemit_CRT: TDBEdit;
    Edemit_xCRT: TDBEdit;
    TabSheet6: TTabSheet;
    Panel4: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Edavulsa_CNPJ: TDBEdit;
    Edavulsa_nDAR: TDBEdit;
    Edavulsa_fone: TDBEdit;
    Edavulsa_matr: TDBEdit;
    Edavulsa_repEmi: TDBEdit;
    Edavulsa_xOrgao: TDBEdit;
    Edavulsa_xAgente: TDBEdit;
    Edavulsa_UF: TDBEdit;
    Edavulsa_vDAR: TDBEdit;
    TPavulsa_dEmi: TDBEdit;
    TPavulsa_dPag: TDBEdit;
    TabSheet7: TTabSheet;
    Panel42: TPanel;
    Label305: TLabel;
    EdCodInfoDest: TDBEdit;
    EdNomeDest: TDBEdit;
    Panel15: TPanel;
    Label13: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label109: TLabel;
    Label22: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    Label23: TLabel;
    Eddest_CNPJ: TDBEdit;
    Eddest_xNome: TDBEdit;
    Eddest_xLgr: TDBEdit;
    Eddest_nro: TDBEdit;
    Eddest_xCpl: TDBEdit;
    Eddest_xBairro: TDBEdit;
    Eddest_cMun: TDBEdit;
    Eddest_UF: TDBEdit;
    Eddest_CEP: TDBEdit;
    Eddest_cPais: TDBEdit;
    Eddest_fone: TDBEdit;
    Eddest_IE: TDBEdit;
    Eddest_ISUF: TDBEdit;
    Eddest_xMun: TDBEdit;
    Eddest_xPais: TDBEdit;
    Eddest_CPF: TDBEdit;
    Eddest_email: TDBEdit;
    TabSheet8: TTabSheet;
    GroupBox13: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Edretirada_CNPJ: TDBEdit;
    Edretirada_xLgr: TDBEdit;
    Edretirada_nro: TDBEdit;
    Edretirada_xCpl: TDBEdit;
    Edretirada_xBairro: TDBEdit;
    Edretirada_cMun: TDBEdit;
    Edretirada_UF: TDBEdit;
    Edretirada_xMun: TDBEdit;
    Edretirada_CPF: TDBEdit;
    GroupBox14: TGroupBox;
    Panel5: TPanel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label37: TLabel;
    Edentrega_CNPJ: TDBEdit;
    Edentrega_xLgr: TDBEdit;
    Edentrega_nro: TDBEdit;
    Edentrega_xCpl: TDBEdit;
    Edentrega_xBairro: TDBEdit;
    Edentrega_cMun: TDBEdit;
    Edentrega_UF: TDBEdit;
    Edentrega_xMun: TDBEdit;
    Edentrega_CPF: TDBEdit;
    Panel35: TPanel;
    TabSheet9: TTabSheet;
    DBGrid1: TDBGrid;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    TabSheet11: TTabSheet;
    dmkLabelRotate1: TdmkLabelRotate;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    GroupBox5: TGroupBox;
    Panel11: TPanel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    GroupBox6: TGroupBox;
    Panel12: TPanel;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    Label140: TLabel;
    Label141: TLabel;
    Label142: TLabel;
    Label143: TLabel;
    Label144: TLabel;
    Label145: TLabel;
    Label146: TLabel;
    Label147: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    DBMemo1: TDBMemo;
    TabSheet12: TTabSheet;
    Panel34: TPanel;
    GroupBox12: TGroupBox;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    TabSheet16: TTabSheet;
    Panel27: TPanel;
    Label150: TLabel;
    Label232: TLabel;
    Label233: TLabel;
    Label234: TLabel;
    Label235: TLabel;
    Label236: TLabel;
    DBEdit84: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    DBEdit87: TDBEdit;
    DBEdit88: TDBEdit;
    DBEdit89: TDBEdit;
    DBEdit90: TDBEdit;
    TabSheet13: TTabSheet;
    Panel17: TPanel;
    Panel18: TPanel;
    GroupBox7: TGroupBox;
    Label151: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label154: TLabel;
    Label155: TLabel;
    EdISSQNtot_vServ: TDBEdit;
    EdISSQNtot_vBC: TDBEdit;
    EdISSQNtot_vISS: TDBEdit;
    EdISSQNtot_vPIS: TDBEdit;
    EdISSQNtot_vCOFINS: TDBEdit;
    Panel19: TPanel;
    GroupBox8: TGroupBox;
    Label156: TLabel;
    Label157: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    Label162: TLabel;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    Label166: TLabel;
    Label167: TLabel;
    Label168: TLabel;
    Label169: TLabel;
    EdICMSTot_vBC: TDBEdit;
    EdICMSTot_vICMS: TDBEdit;
    EdICMSTot_vBCST: TDBEdit;
    EdICMSTot_vST: TDBEdit;
    EdICMSTot_vProd: TDBEdit;
    EdICMSTot_vFrete: TDBEdit;
    EdICMSTot_vSeg: TDBEdit;
    EdICMSTot_vDesc: TDBEdit;
    EdICMSTot_vII: TDBEdit;
    EdICMSTot_vIPI: TDBEdit;
    EdICMSTot_vPIS: TDBEdit;
    EdICMSTot_vCOFINS: TDBEdit;
    EdICMSTot_vOutro: TDBEdit;
    EdICMSTot_vNF: TDBEdit;
    GroupBox9: TGroupBox;
    Label170: TLabel;
    Label171: TLabel;
    Label172: TLabel;
    Label173: TLabel;
    Label174: TLabel;
    Label175: TLabel;
    Label176: TLabel;
    EdRetTrib_vRetPIS: TDBEdit;
    EdRetTrib_vRetCOFINS: TDBEdit;
    EdRetTrib_vRetCSLL: TDBEdit;
    EdRetTrib_vBCIRRF: TDBEdit;
    EdRetTrib_vIRRF: TDBEdit;
    EdRetTrib_vBCRetPrev: TDBEdit;
    EdRetTrib_vRetPrev: TDBEdit;
    Panel52: TPanel;
    TabSheet14: TTabSheet;
    Panel20: TPanel;
    Label177: TLabel;
    Label178: TLabel;
    Label179: TLabel;
    Label180: TLabel;
    Label181: TLabel;
    Label182: TLabel;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    Label211: TLabel;
    Label212: TLabel;
    Label213: TLabel;
    Label214: TLabel;
    Label215: TLabel;
    Label216: TLabel;
    Label217: TLabel;
    SpeedButton8: TSpeedButton;
    Label218: TLabel;
    Label219: TLabel;
    EdModFrete: TDBEdit;
    EdxModFrete: TDBEdit;
    Edtransporta_CNPJ: TDBEdit;
    Edtransporta_CPF: TDBEdit;
    Edtransporta_xNome: TDBEdit;
    EdTransporta_IE: TDBEdit;
    EdTransporta_XEnder: TDBEdit;
    EdTransporta_XMun: TDBEdit;
    EdTransporta_UF: TDBEdit;
    EdRetTransp_vServ: TDBEdit;
    EdRetTransp_vBCRet: TDBEdit;
    EdRetTransp_PICMSRet: TDBEdit;
    EdRetTransp_vICMSRet: TDBEdit;
    EdRetTransp_CFOP: TDBEdit;
    EdRetTransp_CMunFG: TDBEdit;
    EdVeicTransp_Placa: TDBEdit;
    EdVeicTransp_UF: TDBEdit;
    EdVeicTransp_RNTC: TDBEdit;
    EdRetTransp_xMunFG: TDBEdit;
    Edvagao: TDBEdit;
    Edbalsa: TDBEdit;
    Panel13: TPanel;
    Label220: TLabel;
    DBGrid4: TDBGrid;
    Panel14: TPanel;
    Label221: TLabel;
    DBGrid5: TDBGrid;
    Panel23: TPanel;
    Label222: TLabel;
    DBGrid6: TDBGrid;
    TabSheet15: TTabSheet;
    Panel24: TPanel;
    GroupBox10: TGroupBox;
    Label223: TLabel;
    Label224: TLabel;
    Label225: TLabel;
    Label226: TLabel;
    Edcobr_fat_nFat: TDBEdit;
    Edcobr_fat_vOrig: TDBEdit;
    Edcobr_fat_vDesc: TDBEdit;
    Edcobr_fat_vLiq: TDBEdit;
    DBGrid7: TDBGrid;
    TabSheet17: TTabSheet;
    Panel25: TPanel;
    Label228: TLabel;
    MeinfCpl: TDBMemo;
    Panel26: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Label227: TLabel;
    DBGrid9: TDBGrid;
    Panel30: TPanel;
    Label230: TLabel;
    DBGrid8: TDBGrid;
    Panel31: TPanel;
    Panel32: TPanel;
    Label229: TLabel;
    MeinfAdFisco: TDBMemo;
    Panel33: TPanel;
    Label231: TLabel;
    DBGrid10: TDBGrid;
    TabSheet18: TTabSheet;
    Panel36: TPanel;
    GroupBox11: TGroupBox;
    Label237: TLabel;
    Label238: TLabel;
    EdExporta_UFEmbarq: TDBEdit;
    EdExporta_XLocEmbarq: TDBEdit;
    GroupBox15: TGroupBox;
    Label239: TLabel;
    Label240: TLabel;
    Label241: TLabel;
    EdCompra_XNEmp: TDBEdit;
    EdCompra_XPed: TDBEdit;
    EdCompra_XCont: TDBEdit;
    TreeView1: TTreeView;
    TabSheet19: TTabSheet;
    TabSheet3: TTabSheet;
    Panel37: TPanel;
    EdenviNFe_versao: TdmkEdit;
    Label254: TLabel;
    EdenviNFe_idLote: TdmkEdit;
    Label190: TLabel;
    DBGrid11: TDBGrid;
    Label194: TLabel;
    QrNFeA: TmySQLQuery;
    DsNFeA: TDataSource;
    TabSheet20: TTabSheet;
    Panel21: TPanel;
    Label186: TLabel;
    Label187: TLabel;
    Label188: TLabel;
    Label189: TLabel;
    Label191: TLabel;
    Label192: TLabel;
    QrNFeAFatID: TIntegerField;
    QrNFeAFatNum: TIntegerField;
    QrNFeAEmpresa: TIntegerField;
    QrNFeAIDCtrl: TIntegerField;
    QrNFeALoteEnv: TLargeintField;
    QrNFeAversao: TFloatField;
    QrNFeAId: TWideStringField;
    QrNFeAAtivo: TSmallintField;
    EdFatID: TDBEdit;
    EdSeqNFe: TDBEdit;
    Label255: TLabel;
    EdFatNum: TDBEdit;
    EdEmpresa: TDBEdit;
    EdIDCtrl: TDBEdit;
    Edversao: TDBEdit;
    EdId: TDBEdit;
    QrNFeB: TmySQLQuery;
    DsNFeB: TDataSource;
    QrNFeC: TmySQLQuery;
    DsNFeC: TDataSource;
    QrNFeBFatID: TIntegerField;
    QrNFeBFatNum: TIntegerField;
    QrNFeBEmpresa: TIntegerField;
    QrNFeBide_cUF: TSmallintField;
    QrNFeBide_cNF: TIntegerField;
    QrNFeBide_natOp: TWideStringField;
    QrNFeBide_indPag: TSmallintField;
    QrNFeBide_mod: TSmallintField;
    QrNFeBide_serie: TIntegerField;
    QrNFeBide_nNF: TIntegerField;
    QrNFeBide_dEmi: TDateField;
    QrNFeBide_dSaiEnt: TDateField;
    QrNFeBide_hSaiEnt: TTimeField;
    QrNFeBide_tpNF: TSmallintField;
    QrNFeBide_cMunFG: TIntegerField;
    QrNFeBide_tpImp: TSmallintField;
    QrNFeBide_tpEmis: TSmallintField;
    QrNFeBide_cDV: TSmallintField;
    QrNFeBide_tpAmb: TSmallintField;
    QrNFeBide_finNFe: TSmallintField;
    QrNFeBide_procEmi: TSmallintField;
    QrNFeBide_verProc: TWideStringField;
    QrNFeBide_dhCont: TDateTimeField;
    QrNFeBide_xJust: TWideStringField;
    QrNFeBAtivo: TSmallintField;
    Edide_cUF: TDBEdit;
    Edide_cNF: TDBEdit;
    Edide_natOp: TDBEdit;
    Edide_indPag: TDBEdit;
    Edide_mod: TDBEdit;
    Edide_serie: TDBEdit;
    Edide_nNF: TDBEdit;
    Edide_dEmi: TDBEdit;
    Edide_dSaiEnt: TDBEdit;
    Edide_hSaiEnt: TDBEdit;
    Edide_tpNF: TDBEdit;
    Edide_cMunFG: TDBEdit;
    Edide_tpImp: TDBEdit;
    Edide_tpAmb: TDBEdit;
    Edide_cDV: TDBEdit;
    Edide_finNFe: TDBEdit;
    Edide_procEmi: TDBEdit;
    Edide_verProc: TDBEdit;
    Label259: TLabel;
    Edide_dhCont_TXT: TDBEdit;
    Label260: TLabel;
    Edide_xJust: TDBEdit;
    QrNFeBide_dhCont_TXT: TWideStringField;
    QrNFeB12a: TmySQLQuery;
    DsNFeB12a: TDataSource;
    QrNFeB12aControle: TIntegerField;
    QrNFeB12arefNFe: TWideStringField;
    QrNFeB12arefNF_cUF: TSmallintField;
    QrNFeB12arefNF_AAMM: TIntegerField;
    QrNFeB12arefNF_CNPJ: TWideStringField;
    QrNFeB12arefNF_mod: TSmallintField;
    QrNFeB12arefNF_serie: TIntegerField;
    QrNFeB12arefNF_nNF: TIntegerField;
    QrNFeB12arefNFP_cUF: TSmallintField;
    QrNFeB12arefNFP_AAMM: TSmallintField;
    QrNFeB12arefNFP_CNPJ: TWideStringField;
    QrNFeB12arefNFP_CPF: TWideStringField;
    QrNFeB12arefNFP_IE: TWideStringField;
    QrNFeB12arefNFP_mod: TSmallintField;
    QrNFeB12arefNFP_serie: TSmallintField;
    QrNFeB12arefNFP_nNF: TIntegerField;
    QrNFeB12arefCTe: TWideStringField;
    QrNFeB12arefECF_mod: TWideStringField;
    QrNFeB12arefECF_nECF: TSmallintField;
    QrNFeB12arefECF_nCOO: TIntegerField;
    Panel39: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    DBGrid12: TDBGrid;
    QrNFeD: TmySQLQuery;
    DsNFeD: TDataSource;
    QrNFeDFatID: TIntegerField;
    QrNFeDFatNum: TIntegerField;
    QrNFeDEmpresa: TIntegerField;
    QrNFeDavulsa_CNPJ: TWideStringField;
    QrNFeDavulsa_xOrgao: TWideStringField;
    QrNFeDavulsa_matr: TWideStringField;
    QrNFeDavulsa_xAgente: TWideStringField;
    QrNFeDavulsa_fone: TWideStringField;
    QrNFeDavulsa_UF: TWideStringField;
    QrNFeDavulsa_nDAR: TWideStringField;
    QrNFeDavulsa_dEmi: TDateField;
    QrNFeDavulsa_vDAR: TFloatField;
    QrNFeDavulsa_repEmi: TWideStringField;
    QrNFeDavulsa_dPag: TDateField;
    QrNFeDAtivo: TSmallintField;
    QrNFeE: TmySQLQuery;
    DsNFeE: TDataSource;
    QrNFeEFatID: TIntegerField;
    QrNFeEFatNum: TIntegerField;
    QrNFeEEmpresa: TIntegerField;
    QrNFeEdest_CNPJ: TWideStringField;
    QrNFeEdest_CPF: TWideStringField;
    QrNFeEdest_xNome: TWideStringField;
    QrNFeEdest_xLgr: TWideStringField;
    QrNFeEdest_nro: TWideStringField;
    QrNFeEdest_xCpl: TWideStringField;
    QrNFeEdest_xBairro: TWideStringField;
    QrNFeEdest_cMun: TIntegerField;
    QrNFeEdest_xMun: TWideStringField;
    QrNFeEdest_UF: TWideStringField;
    QrNFeEdest_CEP: TWideStringField;
    QrNFeEdest_cPais: TIntegerField;
    QrNFeEdest_xPais: TWideStringField;
    QrNFeEdest_fone: TWideStringField;
    QrNFeEdest_IE: TWideStringField;
    QrNFeEdest_ISUF: TWideStringField;
    QrNFeEdest_email: TWideStringField;
    QrNFeEAtivo: TSmallintField;
    QrNFeF: TmySQLQuery;
    DsNFeF: TDataSource;
    QrNFeG: TmySQLQuery;
    DsNFeG: TDataSource;
    QrNFeW02: TmySQLQuery;
    DsNFeW02: TDataSource;
    QrNFeFFatID: TIntegerField;
    QrNFeFFatNum: TIntegerField;
    QrNFeFEmpresa: TIntegerField;
    QrNFeFretirada_CNPJ: TWideStringField;
    QrNFeFretirada_CPF: TWideStringField;
    QrNFeFretirada_xLgr: TWideStringField;
    QrNFeFretirada_nro: TWideStringField;
    QrNFeFretirada_xCpl: TWideStringField;
    QrNFeFretirada_xBairro: TWideStringField;
    QrNFeFretirada_cMun: TIntegerField;
    QrNFeFretirada_xMun: TWideStringField;
    QrNFeFretirada_UF: TWideStringField;
    QrNFeFAtivo: TSmallintField;
    QrNFeGFatID: TIntegerField;
    QrNFeGFatNum: TIntegerField;
    QrNFeGEmpresa: TIntegerField;
    QrNFeGentrega_CNPJ: TWideStringField;
    QrNFeGentrega_CPF: TWideStringField;
    QrNFeGentrega_xLgr: TWideStringField;
    QrNFeGentrega_nro: TWideStringField;
    QrNFeGentrega_xCpl: TWideStringField;
    QrNFeGentrega_xBairro: TWideStringField;
    QrNFeGentrega_cMun: TIntegerField;
    QrNFeGentrega_xMun: TWideStringField;
    QrNFeGentrega_UF: TWideStringField;
    QrNFeGAtivo: TSmallintField;
    QrNFeW17: TmySQLQuery;
    DsNFeW17: TDataSource;
    QrNFeW23: TmySQLQuery;
    DsNFeW23: TDataSource;
    QrNFeW02ICMSTot_vBC: TFloatField;
    QrNFeW02ICMSTot_vICMS: TFloatField;
    QrNFeW02ICMSTot_vBCST: TFloatField;
    QrNFeW02ICMSTot_vST: TFloatField;
    QrNFeW02ICMSTot_vProd: TFloatField;
    QrNFeW02ICMSTot_vFrete: TFloatField;
    QrNFeW02ICMSTot_vSeg: TFloatField;
    QrNFeW02ICMSTot_vDesc: TFloatField;
    QrNFeW02ICMSTot_vII: TFloatField;
    QrNFeW02ICMSTot_vIPI: TFloatField;
    QrNFeW02ICMSTot_vPIS: TFloatField;
    QrNFeW02ICMSTot_vCOFINS: TFloatField;
    QrNFeW02ICMSTot_vOutro: TFloatField;
    QrNFeW02ICMSTot_vNF: TFloatField;
    QrNFeW17ISSQNtot_vServ: TFloatField;
    QrNFeW17ISSQNtot_vBC: TFloatField;
    QrNFeW17ISSQNtot_vISS: TFloatField;
    QrNFeW17ISSQNtot_vPIS: TFloatField;
    QrNFeW17ISSQNtot_vCOFINS: TFloatField;
    QrNFeW23RetTrib_vRetPIS: TFloatField;
    QrNFeW23RetTrib_vRetCOFINS: TFloatField;
    QrNFeW23RetTrib_vRetCSLL: TFloatField;
    QrNFeW23RetTrib_vBCIRRF: TFloatField;
    QrNFeW23RetTrib_vIRRF: TFloatField;
    QrNFeW23RetTrib_vBCRetPrev: TFloatField;
    QrNFeW23RetTrib_vRetPrev: TFloatField;
    QrNFeX01: TmySQLQuery;
    DsNFeX01: TDataSource;
    QrNFeX01ModFrete: TSmallintField;
    QrNFeX01Transporta_CNPJ: TWideStringField;
    QrNFeX01Transporta_CPF: TWideStringField;
    QrNFeX01Transporta_XNome: TWideStringField;
    QrNFeX01Transporta_IE: TWideStringField;
    QrNFeX01Transporta_XEnder: TWideStringField;
    QrNFeX01Transporta_XMun: TWideStringField;
    QrNFeX01Transporta_UF: TWideStringField;
    QrNFeX01RetTransp_vServ: TFloatField;
    QrNFeX01RetTransp_vBCRet: TFloatField;
    QrNFeX01RetTransp_PICMSRet: TFloatField;
    QrNFeX01RetTransp_vICMSRet: TFloatField;
    QrNFeX01RetTransp_CFOP: TWideStringField;
    QrNFeX01RetTransp_CMunFG: TWideStringField;
    QrNFeX01VeicTransp_Placa: TWideStringField;
    QrNFeX01VeicTransp_UF: TWideStringField;
    QrNFeX01VeicTransp_RNTC: TWideStringField;
    QrNFeX01Vagao: TWideStringField;
    QrNFeX01Balsa: TWideStringField;
    QrNFeY01: TmySQLQuery;
    DsNFeY01: TDataSource;
    QrNFeY01Cobr_Fat_nFat: TWideStringField;
    QrNFeY01Cobr_Fat_vOrig: TFloatField;
    QrNFeY01Cobr_Fat_vDesc: TFloatField;
    QrNFeY01Cobr_Fat_vLiq: TFloatField;
    QrNFeZ01: TmySQLQuery;
    DsNFeZ01: TDataSource;
    QrNFeZ01InfAdic_InfAdFisco: TWideMemoField;
    QrNFeZ01InfAdic_InfCpl: TWideMemoField;
    QrNFeZA01: TmySQLQuery;
    DsNFeZA01: TDataSource;
    QrNFeZB01: TmySQLQuery;
    DsNFeZB01: TDataSource;
    QrNFeZA01Exporta_UFEmbarq: TWideStringField;
    QrNFeZA01Exporta_XLocEmbarq: TWideStringField;
    QrNFeZB01Compra_XNEmp: TWideStringField;
    QrNFeZB01Compra_XPed: TWideStringField;
    QrNFeZB01Compra_XCont: TWideStringField;
    TabSheet26: TTabSheet;
    PageControl4: TPageControl;
    TabSheet27: TTabSheet;
    TabSheet28: TTabSheet;
    Panel40: TPanel;
    Label256: TLabel;
    Label257: TLabel;
    Label258: TLabel;
    Label261: TLabel;
    Label262: TLabel;
    Label263: TLabel;
    Label264: TLabel;
    EdinfCanc_Id: TDBEdit;
    EdinfCanc_tpAmb: TDBEdit;
    EdinfCanc_xServ: TDBEdit;
    EdinfCanc_chNFe: TDBEdit;
    EdinfCanc_nProt: TDBEdit;
    EdinfCanc_xJust: TDBEdit;
    EdinfCanc_xAmb: TDBEdit;
    Panel41: TPanel;
    Label265: TLabel;
    Label266: TLabel;
    Label267: TLabel;
    Label268: TLabel;
    Label269: TLabel;
    Label270: TLabel;
    Label271: TLabel;
    Label272: TLabel;
    Label273: TLabel;
    EdretCancNFe_Id: TDBEdit;
    EdretCancNFe_tpAmb: TDBEdit;
    EdretCancNFe_verAplic: TDBEdit;
    EdretCancNFe_chNFe: TDBEdit;
    EdretCancNFe_cStat: TDBEdit;
    EdretCancNFe_xMotivo: TDBEdit;
    EdretCancNFe_dhRecbto: TDBEdit;
    EdretCancNFe_nProt: TDBEdit;
    EdretCancNFe_cUF: TDBEdit;
    EdretCancNFe_xAmb: TDBEdit;
    EdretCancNFe_xUF: TDBEdit;
    GBprocCancNFe: TGroupBox;
    Panel43: TPanel;
    Label274: TLabel;
    QrCP01: TmySQLQuery;
    DsCP01: TDataSource;
    QrCR01: TmySQLQuery;
    DsCR01: TDataSource;
    QrYR01: TmySQLQuery;
    DsYR01: TDataSource;
    QrArqs: TmySQLQuery;
    DsArqs: TDataSource;
    QrNFeASeqArq: TIntegerField;
    QrNFeASeqNFe: TIntegerField;
    Panel44: TPanel;
    DBGrid13: TDBGrid;
    QrArqsSeqArq: TIntegerField;
    QrArqsVersao: TFloatField;
    QrArqsCodificacao: TWideStringField;
    QrArqsLocalName: TWideStringField;
    QrArqsPrefix: TWideStringField;
    QrArqsNodeName: TWideStringField;
    QrArqsNamespaceURI: TWideStringField;
    QrArqsArqDir: TWideStringField;
    QrArqsArqName: TWideStringField;
    QrNFeBide_xUF: TWideStringField;
    QrNFeBide_xAmb: TWideStringField;
    QrNFeBide_xTpNF: TWideStringField;
    QrNFeBide_xTpImp: TWideStringField;
    QrNFeBide_xTpEmis: TWideStringField;
    QrNFeBide_xFInNFe: TWideStringField;
    QrNFeBide_xMunFG: TWideStringField;
    QrNFeBide_xIndPag: TWideStringField;
    QrNFeBide_xProcEmi: TWideStringField;
    QrNFeCemit_CNPJ: TWideStringField;
    QrNFeCemit_CPF: TWideStringField;
    QrNFeCemit_xNome: TWideStringField;
    QrNFeCemit_xFant: TWideStringField;
    QrNFeCemit_xLgr: TWideStringField;
    QrNFeCemit_nro: TWideStringField;
    QrNFeCemit_xCpl: TWideStringField;
    QrNFeCemit_xBairro: TWideStringField;
    QrNFeCemit_cMun: TIntegerField;
    QrNFeCemit_xMun: TWideStringField;
    QrNFeCemit_UF: TWideStringField;
    QrNFeCemit_CEP: TIntegerField;
    QrNFeCemit_cPais: TIntegerField;
    QrNFeCemit_xPais: TWideStringField;
    QrNFeCemit_fone: TWideStringField;
    QrNFeCemit_IE: TWideStringField;
    QrNFeCemit_IEST: TWideStringField;
    QrNFeCemit_IM: TWideStringField;
    QrNFeCemit_CNAE: TWideStringField;
    QrNFeCemit_CRT: TSmallintField;
    QrNFeCCodInfoEmit: TIntegerField;
    QrNFeCNomeEmit: TWideStringField;
    QrNFeCemit_xCRT: TWideStringField;
    QrNFeCemit_xCNAE: TWideStringField;
    QrNFeECodInfoDest: TIntegerField;
    QrNFeENomeDest: TWideStringField;
    QrNFeX01xModFrete: TWideStringField;
    QrNFeX01RetTransp_xMunFG: TWideStringField;
    QrPR01: TmySQLQuery;
    DsPR01: TDataSource;
    QrXR01: TmySQLQuery;
    DsXR01: TDataSource;
    GroupBox16: TGroupBox;
    Panel45: TPanel;
    Label1: TLabel;
    DBEdit91: TDBEdit;
    QrXR01nfeProcNFe_versao: TFloatField;
    PageControl5: TPageControl;
    TabSheet21: TTabSheet;
    TabSheet22: TTabSheet;
    Panel38: TPanel;
    Label242: TLabel;
    Label244: TLabel;
    Label245: TLabel;
    Label243: TLabel;
    Label246: TLabel;
    Label247: TLabel;
    Label248: TLabel;
    Label249: TLabel;
    Label250: TLabel;
    EdprotNFe_versao: TDBEdit;
    EdinfProt_Id: TDBEdit;
    EdinfProt_cStat: TDBEdit;
    EdinfProt_tpAmb: TDBEdit;
    EdinfProt_xAmb: TDBEdit;
    EdinfProt_verAplic: TDBEdit;
    EdinfProt_chNFe: TDBEdit;
    EdinfProt_dhRecbto: TDBEdit;
    EdinfProt_nProt: TDBEdit;
    EdinfProt_digVal: TDBEdit;
    EdinfProt_xMotivo: TDBEdit;
    QrPR01protNFe_versao: TFloatField;
    QrPR01infProt_Id: TWideStringField;
    QrPR01infProt_tpAmb: TSmallintField;
    QrPR01infProt_xAmb: TWideStringField;
    QrPR01infProt_verAplic: TWideStringField;
    QrPR01infProt_chNFe: TWideStringField;
    QrPR01infProt_dhRecbto: TDateTimeField;
    QrPR01infProt_nProt: TLargeintField;
    QrPR01infProt_digVal: TWideStringField;
    QrPR01infProt_cStat: TIntegerField;
    QrPR01infProt_xMotivo: TWideStringField;
    QrArqsxTipoDoc: TWideStringField;
    QrArqsAssinado: TSmallintField;
    QrFilial: TmySQLQuery;
    QrNFeIGraGruX: TIntegerField;
    QrNFeINomeGGX: TWideStringField;
    QrFilialCodFilial: TIntegerField;
    QrLocGGX: TmySQLQuery;
    QrLocGGXItens: TLargeintField;
    TabSheet23: TTabSheet;
    MeIgnorados: TMemo;
    GroupBox17: TGroupBox;
    Panel46: TPanel;
    Label2: TLabel;
    DBEdit92: TDBEdit;
    QrDP01: TmySQLQuery;
    DsDP01: TDataSource;
    QrDP01inutNFe_versao: TFloatField;
    QrDP01inutNFe_Id: TWideStringField;
    QrDP01inutNFe_tpAmb: TSmallintField;
    QrDP01inutNFe_xAmb: TWideStringField;
    QrDP01inutNFe_xServ: TWideStringField;
    QrDP01inutNFe_cUF: TSmallintField;
    QrDP01inutNFe_xUF: TWideStringField;
    QrDP01inutNFe_ano: TSmallintField;
    QrDP01inutNFe_CNPJ: TWideStringField;
    QrDP01inutNFe_mod: TSmallintField;
    QrDP01inutNFe_serie: TIntegerField;
    QrDP01inutNFe_nNFIni: TIntegerField;
    QrDP01inutNFe_nNFFin: TIntegerField;
    QrDP01inutNFe_xJust: TWideStringField;
    PageControl6: TPageControl;
    TabSheet24: TTabSheet;
    Panel47: TPanel;
    Label3: TLabel;
    DBEdit93: TDBEdit;
    Label4: TLabel;
    DBEdit94: TDBEdit;
    Label5: TLabel;
    DBEdit95: TDBEdit;
    DBEdit96: TDBEdit;
    Label6: TLabel;
    DBEdit97: TDBEdit;
    Label251: TLabel;
    DBEdit98: TDBEdit;
    Label252: TLabel;
    DBEdit99: TDBEdit;
    DBEdit100: TDBEdit;
    Label253: TLabel;
    DBEdit101: TDBEdit;
    Label275: TLabel;
    DBEdit102: TDBEdit;
    Label276: TLabel;
    DBEdit103: TDBEdit;
    DBEdit104: TDBEdit;
    Label277: TLabel;
    Label278: TLabel;
    DBEdit105: TDBEdit;
    Label279: TLabel;
    DBEdit106: TDBEdit;
    TabSheet25: TTabSheet;
    QrDR01: TmySQLQuery;
    DsDR01: TDataSource;
    QrZR01: TmySQLQuery;
    DsZR01: TDataSource;
    QrZR01procInutNFe_versao: TFloatField;
    QrDR01retInutNFe_versao: TFloatField;
    QrDR01retInutNFe_Id: TWideStringField;
    QrDR01retInutNFe_tpAmb: TSmallintField;
    QrDR01retInutNFe_xAmb: TWideStringField;
    QrDR01retInutNFe_verAplic: TWideStringField;
    QrDR01retInutNFe_cStat: TIntegerField;
    QrDR01retInutNFe_xMotivo: TWideStringField;
    QrDR01retInutNFe_cUF: TSmallintField;
    QrDR01retInutNFe_xUF: TWideStringField;
    QrDR01retInutNFe_ano: TSmallintField;
    QrDR01retInutNFe_CNPJ: TWideStringField;
    QrDR01retInutNFe_mod: TSmallintField;
    QrDR01retInutNFe_serie: TIntegerField;
    QrDR01retInutNFe_nNFIni: TIntegerField;
    QrDR01retInutNFe_nNFFin: TIntegerField;
    QrDR01retInutNFe_dhRecbto: TDateTimeField;
    QrDR01retInutNFe_nProt: TLargeintField;
    Panel48: TPanel;
    Label280: TLabel;
    Label281: TLabel;
    Label282: TLabel;
    Label284: TLabel;
    Label285: TLabel;
    Label286: TLabel;
    Label287: TLabel;
    Label288: TLabel;
    Label289: TLabel;
    Label290: TLabel;
    Label291: TLabel;
    DBEdit107: TDBEdit;
    DBEdit108: TDBEdit;
    DBEdit109: TDBEdit;
    DBEdit110: TDBEdit;
    DBEdit112: TDBEdit;
    DBEdit113: TDBEdit;
    DBEdit114: TDBEdit;
    DBEdit115: TDBEdit;
    DBEdit116: TDBEdit;
    DBEdit117: TDBEdit;
    DBEdit118: TDBEdit;
    DBEdit119: TDBEdit;
    DBEdit120: TDBEdit;
    Label283: TLabel;
    DBEdit111: TDBEdit;
    Label292: TLabel;
    DBEdit121: TDBEdit;
    DBEdit122: TDBEdit;
    Label293: TLabel;
    QrDR01retInutNFe_dhRecbto_TXT: TWideStringField;
    PnEdita: TPanel;
    BtAltera: TBitBtn;
    Panel49: TPanel;
    BitBtn2: TBitBtn;
    PMAltera: TPopupMenu;
    Cdigo1: TMenuItem;
    DaEntidadeDoEmitente1: TMenuItem;
    DaEntidadeDoDestinatrio1: TMenuItem;
    DoReduzidoDoProduto1: TMenuItem;
    QrNFeESeqArq: TIntegerField;
    QrNFeESeqNFe: TIntegerField;
    QrCP01cancNFe_versao: TFloatField;
    QrCP01infCanc_Id: TWideStringField;
    QrCP01infCanc_tpAmb: TSmallintField;
    QrCP01infCanc_xAmb: TWideStringField;
    QrCP01infCanc_xServ: TWideStringField;
    QrCP01infCanc_chNFe: TWideStringField;
    QrCP01infCanc_nProt: TLargeintField;
    QrCP01infCanc_xJust: TWideStringField;
    QrCR01retCanc_versao: TFloatField;
    QrCR01infCanc_Id: TWideStringField;
    QrCR01infCanc_tpAmb: TSmallintField;
    QrCR01infCanc_xAmb: TWideStringField;
    QrCR01infCanc_verAplic: TWideStringField;
    QrCR01infCanc_cStat: TIntegerField;
    QrCR01infCanc_xMotivo: TWideStringField;
    QrCR01infCanc_cUF: TSmallintField;
    QrCR01infCanc_xUF: TWideStringField;
    QrCR01infCanc_chNFe: TWideStringField;
    QrCR01infCanc_dhRecbto: TDateTimeField;
    QrCR01infCanc_nProt: TLargeintField;
    QrYR01procCancNFe_versao: TFloatField;
    BtCadDest: TBitBtn;
    BtConverte: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label294: TLabel;
    Label295: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SBArquivoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbCarregaClick(Sender: TObject);
    procedure QrNFeIBeforeClose(DataSet: TDataSet);
    procedure QrNFeIAfterScroll(DataSet: TDataSet);
    procedure QrNFeIDiAfterScroll(DataSet: TDataSet);
    procedure QrNFeIDiBeforeClose(DataSet: TDataSet);
    procedure QrNFeX26BeforeClose(DataSet: TDataSet);
    procedure QrNFeX26AfterScroll(DataSet: TDataSet);
    procedure QrNFeZ10CalcFields(DataSet: TDataSet);
    procedure QrNFeUCalcFields(DataSet: TDataSet);
    procedure QrNFeAAfterScroll(DataSet: TDataSet);
    procedure QrNFeABeforeClose(DataSet: TDataSet);
    procedure QrNFeBCalcFields(DataSet: TDataSet);
    procedure QrArqsBeforeClose(DataSet: TDataSet);
    procedure QrArqsAfterScroll(DataSet: TDataSet);
    procedure QrDR01CalcFields(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure DaentidadedoDestinatrio1Click(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure QrNFeEBeforeClose(DataSet: TDataSet);
    procedure QrNFeEAfterScroll(DataSet: TDataSet);
    procedure BtCadDestClick(Sender: TObject);
  private
    { Private declarations }
    FAssinado: Byte;
    FCodInfoEmit,
    FCtrlB12a, FCtrlIDi, FCtrlIDiA, FCtrlY07, FCtrlX22, FCtrlX26, FCtrlX33,
    FCtrlZ04, FCtrlZ07, FCtrlZ10, FSeqNFe: Integer;
    //
    FxmlArqs, FxmlCP01, FxmlCR01, FxmlDP01, FxmlDR01, FxmlPR01, FxmlXR01,
    FxmlYR01, FxmlZR01,
    FCabA, FCabB, FcabB12a, FCabC, FCabD, FCabE, FCabF, FCabG, FCabGA,
    FCabW02, FCabW17, FCabW23,
    FItsI, FItsIDi, FItsIDiA, FItsM, FItsN, FItsO, FItsP, FItsQ, FItsR, FItsS,
    FItsT, FItsU, FItsV, FCabX01, FItsX22, FItsX26, FItsX33, FCabY01, FItsY07,
    FCabZ01, FItsZ04, FItsZ07, FItsZ10, FCabZA01, FCabZB01: String;
    //procedure PopulateTree();
    procedure ClearTree();
    function ApenasCarregaArquivo(): Boolean;
    procedure ImportaGrupo_AP01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_CP01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_CR01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_DP01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_DR01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_PR01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_XR01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_YR01(XMLNohNivel0: IXMLNode);
    procedure ImportaGrupo_ZR01(XMLNohNivel0: IXMLNode);
    //
    procedure ImportaNFe_Grupo_A00(XMLNohNivel0: IXMLNode);
    procedure ImportaNFe_Grupo_A01(XMLNohNivel0: IXMLNode);
    procedure ImportaNFe_Grupo_B01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_B12a(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_C01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_D01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_E01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_F01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_G01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_GA01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_H01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_I01(XMLNohNivel2: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_I18(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_J01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_K01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_L01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_L101(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_M01(XMLNohNivel2: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_N01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_O01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_P01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_Q01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_R01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_S01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_T01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_U01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_V01(XMLNohNivel3: IXMLNode; nItem: WideString);
    procedure ImportaNFe_Grupo_W01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_W02(XMLNohNivel2: IXMLNode);
    procedure ImportaNFe_Grupo_W17(XMLNohNivel2: IXMLNode);
    procedure ImportaNFe_Grupo_W23(XMLNohNivel2: IXMLNode);
    procedure ImportaNFe_Grupo_X01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_Y01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_Z01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_ZA01(XMLNohNivel1: IXMLNode);
    procedure ImportaNFe_Grupo_ZB01(XMLNohNivel1: IXMLNode);
    // Protocolo
    //
    procedure AvisaItemDesconhecido(Grupo, NodeName: WideString);
    function  EhEmissaoPropria(): Boolean;
    function  DefineEntidade(XMLNoh: IXMLNode; const CNPJ, CPF: String;
              var Codigo: Integer; var Nome: WideString): Boolean;
    function  ImportaXML(): Boolean;
    function  CadastraEntidade(TipoCad: TTipoCadNFe; CNPJ, CPF, xNome,
              xFant, IE, xLgr, nro, xBairro: WideString; cMun: Integer; xMun,
              UF: WideString; CEP,  cPais: Integer; xPais, fone, IEST, IM, CNAE,
              xCpl, xEnder: WideString): Integer;
  public
    { Public declarations }
    FPB1: TProgressBar;
    FLaAviso1, FLaAviso2: TLabel;
    FSeqArq: Integer;
    FAvisaErros: Boolean;
    FPermiteAlterarDadosE01, FPermiteAlterarCodInfoDest, FRecriaTbTmp: Boolean;
    procedure ResetaVarsETabs();
    function  CarregaArquivoNoTempBD(XMLDocument: TXMLDocument;
              FullNameArq: String): Boolean;
  end;

  var
  FmNFeLoad_Arq: TFmNFeLoad_Arq;

implementation

uses UnMyObjects, ModuleNFe_0000, ModuleGeral, UnFinanceiro, UMySQLModule,
  CreateNFe, NFeXMLGerencia, Module, NFeLoad_E01, MyDBCheck, Entidade2,
  GraGruEIts, NFe_PF, UnXXe_PF, UnGrade_PF;

{$R *.DFM}

const

  FElementos: array[0..8] of string = ('nfeproc', 'nfe', 'envinfe', 'cancnfe',
              'retcancnfe', 'proccancnfe', 'inutnfe', 'retinutnfe',
              'procinutnfe');
                                      //B    C       d         E       f
  FGrupos: array[0..11] of string = ('ide', 'emit', 'avulsa', 'dest', 'retirada',
                                      //G        H      W        X         Y
                                     'entrega', 'det', 'total', 'transp', 'cobr',
                                      //Z         ZB
                                     'infadic', 'compra');
  FGrupo_A01:  array[0..1] of string = ('versao', 'id');
  FGrupo_B01:  array[0..21] of string = ('cuf', 'cnf', 'natop', 'indpag', 'mod',
               'serie', 'nnf', 'demi', 'dsaient', 'hsaient', 'tpnf', 'cmunfg',
               'nfref', 'tpimp', 'tpemis', 'cdv', 'tpamb', 'finnfe', 'procemi',
               'verproc', 'dhcont','xjust');
  FGrupo_B12a: array[0..2] of string = ('refnfe', 'refnf', 'refnfp');
  FGrupo_B14:  array[0..5] of string = ('cuf', 'aamm', 'cnpj', 'mod', 'serie', 'nnf');
  FGrupo_B20a: array[0..9] of string = ('cfu', 'aamm', 'cnpj', 'cpf', 'ie',
               'mod', 'serie', 'nNf', 'refcte', 'refecf');
  FGrupo_B20j: array[0..2] of string = ('mod', 'necf', 'ncoo');
  FGrupo_C01:  array[0..9] of string = ('cnpj', 'cpf', 'xnome', 'xfant',
               'enderemit', 'ie', 'iest', 'im', 'cnae', 'crt');
  FGrupo_C05:  array[0..10] of string = ('xlgr', 'nro', 'xcpl', 'xbairro', 'cmun',
               'xmun', 'uf', 'cep', 'cpais', 'xpais', 'fone');
  FGrupo_D01:  array[0..10] of string = ('cnpj', 'xorgao', 'matr', 'xagente',
               'fone', 'uf', 'ndar', 'demi', 'vdar', 'repemi', 'dpag');
  FGrupo_E01:  array[0..7] of string = ('cnpj', 'cpf', 'xnome', 'enderdest',
               'ie', 'isuf', 'email', 'indIEDest');
  FGrupo_E05:  array[0..10] of string = ('xlgr', 'nro', 'xcpl', 'xbairro',
               'cmun', 'xmun', 'uf', 'cep', 'cpais', 'xpais', 'fone');
  FGrupo_F01:  array[0..8] of string = ('cnpj', 'cpf', 'xlgr', 'nro', 'xcpl',
               'xbairro', 'cmun', 'xmun', 'uf');
  FGrupo_G01:  array[0..8] of string = ('cnpj', 'cpf', 'xlgr', 'nro', 'xcpl',
               'xbairro', 'cmun', 'xmun', 'uf');
  FGrupo_GA01:  array[0..1] of string = ('cnpj', 'cpf');
  FGrupo_H01:  array[0..3] of string = ('nitem', 'prod', 'imposto', 'infadprod');
  FGrupo_I01:  array[0..21] of string = ('cprod', 'cean', 'xprod', 'ncm',
               'extipi', 'cfop', 'ucom', 'qcom', 'vuncom', 'ceantrib', 'utrib',
               'qtrib', 'vuntrib', 'vfrete', 'vseg', 'vdesc', 'voutro',
               'indtot', 'di', 'xped', 'nitemped',
               // deprecado
               'genero'
               // fim deprecado
               );
  FGrupo_I18:  array[0..6] of string = ('ndi', 'ddi', 'xlocdesemb', 'ufdesemb',
               'ddesemb', 'cexportador', 'adi');
  FGrupo_I25:  array[0..3] of string = ('nadicao', 'nseqadic', 'cfabricante',
               'vdescdi');
  FGrupo_M01:  array[0..7] of string = ('icms', 'ipi', 'ii', 'pis', 'pisst',
               'cofins', 'cofinsst', 'issqn');
  FGrupo_N01:  array[0..16] of string = ('icms00', 'icms10', 'icms20', 'icms30',
               'icms40', 'icms51', 'icms60', 'icms70', 'icms90', 'icmspart',
               'icmsst', 'icmssn101', 'icmssn102', 'icmssn201', 'icmssn202',
               'icmssn500', 'icmssn900');
  FGrupo_Nxx:  array[0..20] of string = ('orig', 'cst', 'modbc', 'predbc',
               'vbc', 'picms', 'vicms', 'modbcst', 'pmvast', 'predbcst',
               'vbcst', 'picmsst', 'vicmsst', 'csosn', 'ufst',
               'pbcop', 'vbcstret', 'vicmsstret', 'motdesicms', 'pcredsn',
               'vcredicmssn');
  FGrupo_O01:  array[0..6] of string = ('clenq', 'cnpjprod', 'cselo', 'qselo',
               'cenq', 'ipitrib', 'ipint');
  FGrupo_O07:  array[0..5] of string = ('cst', 'vbc', 'pipi', 'qunid', 'vunid',
               'vipi');
  FGrupo_O08:  array[0..0] of string = ('cst');
  FGrupo_P01:  array[0..3] of string = ('vbc', 'vdespadu', 'vii', 'viof');
  FGrupo_Q01:  array[0..3] of string = ('pisaliq', 'pisqtde', 'pisnt', 'pisoutr');
  FGrupo_Q02:  array[0..3] of string = ('cst', 'vbc', 'ppis', 'vpis');
  FGrupo_Q03:  array[0..3] of string = ('cst', 'qbcprod', 'valiqprod', 'vpis');
  FGrupo_Q04:  array[0..0] of string = ('cst');
  FGrupo_Q05:  array[0..5] of string = ('cst', 'vbc', 'ppis', 'qbcprod',
               'valiqprod', 'vpis');
  FGrupo_R01:  array[0..4] of string = ('vbc', 'ppis', 'qbcprod', 'valiqprod',
               'vpis');
  FGrupo_S01:  array[0..3] of string = ('cofinsaliq', 'cofinsqtde', 'cofinsnt',
               'cofinsoutr');
  FGrupo_S02:  array[0..3] of string = ('cst', 'vbc', 'pcofins', 'vcofins');
  FGrupo_S03:  array[0..3] of string = ('cst', 'qbcprod', 'valiqprod', 'vcofins');
  FGrupo_S04:  array[0..0] of string = ('cst');
  FGrupo_S05:  array[0..5] of string = ('cst', 'vbc', 'pcofins', 'qbcprod',
               'valiqprod', 'vcofins');
  FGrupo_T01:  array[0..4] of string = ('vbc', 'pcofins', 'qbcprod', 'valiqprod',
               'vcofins');
  FGrupo_U01:  array[0..5] of string = ('vbc', 'valiq', 'vissqn', 'cmunfg',
               'clistserv', 'csittrib');
  FGrupo_W01:  array[0..3] of string = ('total', 'icmstot', 'issqntot', 'rettrib');
  FGrupo_W02:  array[0..13] of string = ('vbc', 'vicms', 'vbcst', 'vst', 'vprod',
               'vfrete', 'vseg', 'vdesc', 'vii', 'vipi', 'vpis', 'vcofins',
               'voutro', 'vnf');
  FGrupo_W17:  array[0..4] of string = ('vserv', 'vbc', 'viss', 'vpis', 'vcofins');
  FGrupo_W23:  array[0..6] of string = ('vretpis', 'vretcofins', 'vretcsll',
               'vbcirrf', 'virrf', 'vbcretprev', 'vretprev');
  FGrupo_X01:  array[0..7] of string = ('modfrete', 'transporta', 'rettransp',
               'veictransp', 'reboque', 'vagao', 'balsa', 'vol');
  FGrupo_X03:  array[0..6] of string = ('cnpj', 'cpf', 'xnome', 'ie', 'xender',
               'xmun', 'uf');
  FGrupo_X11:  array[0..5] of string = ('vserv', 'vbcret', 'picmsret',
               'vicmsret', 'cfop', 'cmunfg');
  FGrupo_X18:  array[0..2] of string = ('placa', 'uf', 'rntc');
  FGrupo_X22:  array[0..2] of string = ('placa', 'uf', 'rntc');
  FGrupo_X26:  array[0..6] of string = ('qvol', 'esp', 'marca', 'nvol', 'pesol',
               'pesob', 'lacres');
  FGrupo_X33:  array[0..0] of string = ('nlacre');
  FGrupo_Y01:  array[0..1] of string = ('fat', 'dup');
  FGrupo_Y02:  array[0..3] of string = ('nfat' , 'vorig', 'vdesc', 'vliq');
  FGrupo_Y07:  array[0..2] of string = ('ndup', 'dvenc', 'vdup');
  FGrupo_Z01:  array[0..3] of string = ('infcpl', 'obscont', 'obsfisco', 'procref');
  FGrupo_Z04:  array[0..1] of string = ('xcampo', 'xtexto');
  FGrupo_Z07:  array[0..1] of string = ('xcampo', 'xtexto');
  FGrupo_Z10:  array[0..1] of string = ('nproc', 'indproc');
  FGrupo_ZA01:  array[0..1] of string = ('ufembarq', 'xlocembarq');
  FGrupo_ZB01:  array[0..2] of string = ('xnemp', 'xped', 'xcont');
  //, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
  //, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
  //, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
  //

procedure TFmNFeLoad_Arq.AvisaItemDesconhecido(Grupo, NodeName: WideString);
begin
  if Lowercase(NodeName) = LowerCase('xmlns') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:xsi') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:ds') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:xsd') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xsi:schemaLocation') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
{
  else
  if Lowercase(NodeName) = LowerCase('xsi:ns2') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
}
  else
  if Lowercase(NodeName) = LowerCase('xmlns:ns2') then
    MeIgnorados.Lines.Add(Grupo + ': ' + NodeName)
  else
    Geral.MB_Erro('TAG do XML desconhecido no grupo ' + Grupo + ':' +
    sLineBreak + NodeName + sLineBreak +
    'Avise a Dermatek!');
end;

procedure TFmNFeLoad_Arq.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmNFeLoad_Arq.BtCadDestClick(Sender: TObject);
begin
  //
end;

procedure TFmNFeLoad_Arq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmNFeLoad_Arq.ApenasCarregaArquivo(): Boolean;
  procedure DomToTree (XmlNode: IXMLNode; TreeNode: TTreeNode);
  var
    I: Integer;
    NewTreeNode: TTreeNode;
    NodeText: string;
    AttrNode: IXMLNode;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MB_Aviso(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    // add the node itself
    NodeText := XmlNode.NodeName;
    if XmlNode.IsTextElement then
      NodeText := NodeText + ' = ' + XmlNode.NodeValue;
    NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      TreeView1.Items.AddChild(NewTreeNode,
        '[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);
  end;
var
  Arquivo: String;
  xmlNodeB, xmlNodeC: IXMLNode;
  //
  TV1, TV2: TTreeNode;
  j: Integer;
  X: IXMLNode;
  MyCursor: TCursor;
begin
  Result := False;
  Arquivo := EdArquivo.Text;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('Arquivo n�o localizado:' + sLineBreak + Arquivo);
    Exit;
  end;
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Carregando XML');
    ClearTree;
    XMLDoc.Active := False;
    XMLDoc.FileName := Arquivo;
    XMLDoc.Active := True;
    TV1 := TreeView1.Items.Add(nil, XMLDoc.DocumentElement.NodeName);
    //XMLDoc.DocumentElement.NodeValue;                         // Erro
    xmlNodeB := XMLDoc.DocumentElement.ChildNodes.First;
    while xmlNodeB <> nil do
    begin
      TV2 := TreeView1.Items.Add(TV1, xmlNodeB.NodeName);
      for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
      begin
        X := xmlNodeB.AttributeNodes.Nodes[J];
        TreeView1.Items.AddChild(TV2, '[' + X.NodeName + ' = "' + X.Text + '"]');
      end;
      //
      xmlNodeC := xmlNodeB.ChildNodes.First;
      while xmlNodeC <> nil do
      begin
        //N := N + 1;
        DomToTree(xmlNodeC, TV2);
        xmlNodeC := xmlNodeC.NextSibling;
      end;
      xmlNodeB := xmlNodeB.NextSibling;
    end;
    //
    // Expande �rvore
    TreeView1.Items.GetFirstNode.Expand(True);
    //
    // Vai ser usado para carregar dados no BtOK
    //XMLDocument1.Active := False;
    //
    EdArquivo.ReadOnly := True;
    SbArquivo.Enabled := False;
    SbCarrega.Enabled := False;
    //
    Result := True;
    //BtOK.Enabled := True;
    MyObjects.Informa2(FLaAviso1, FLaAviso2, False, 'XML carregado!');
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmNFeLoad_Arq.ClearTree();
var
  I: integer;
begin
  for I := 0 to TreeView1.Items.Count - 1 do
    Dispose(TreeView1.Items[I].Data);
  TreeView1.Items.Clear;
end;

procedure TFmNFeLoad_Arq.DaEntidadeDoDestinatrio1Click(Sender: TObject);
begin
  if UMyMod.FormInsUpd_Cria(TFmNFeLoad_E01, FmNFeLoad_E01, afmoLiberado,
    QrNFeE, stUpd) then
  begin
    FmNFeLoad_E01.ShowModal;
    FmNFeLoad_E01.PnDadosE01.Enabled    := FPermiteAlterarDadosE01;
    FmNFeLoad_E01.PnCodInfoDest.Enabled := FPermiteAlterarCodInfoDest;
    FmNFeLoad_E01.Destroy;
  end;
end;

function TFmNFeLoad_Arq.DefineEntidade(XMLNoh: IXMLNode; const CNPJ, CPF: String;
var Codigo: Integer; var Nome: WideString): Boolean;
var
  Doc: String;
begin
  Result := False;
  Nome   := '';
  Codigo := 0;
  if CNPJ <> '' then
    Doc := CNPJ
  else
    Doc := CPF;
  DModG.ObtemEntidadeDeCNPJCFP(Doc, Codigo);
  if Codigo <> 0 then
  begin
    Result := True;
    if DModG.ReopenEndereco(Codigo) then
      Nome := DModG.QrEnderecoNOME_ENT.Value;
  end else begin
    if FAvisaErros then
      Geral.MB_Aviso('Entidade n�o definida para o documento:' +
      sLineBreak + Doc);
  end;
end;

function TFmNFeLoad_Arq.EhEmissaoPropria(): Boolean;
begin
  Result := (QrFilial.State <> dsInactive) and (QrFilial.RecordCount > 0);
end;

procedure TFmNFeLoad_Arq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_Arq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FRecriaTbTmp := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  FPermiteAlterarDadosE01 := False;
  FPermiteAlterarCodInfoDest := False;
  FSeqArq := 0;
  FAvisaErros := True;
  //
  //UMyMod.AbreQuery(DmNFe_0000.QrDTB_UFs, DModG.AllID_DB);
  UMyMod.AbreQuery(DmNFe_0000.QrDTB_UFs, Dmod.MyDB);
  UMyMod.AbreQuery(DmNFe_0000.QrDTB_Munici, DModG.AllID_DB);
  UMyMod.AbreQuery(DmNFe_0000.QrBACEN_Pais, DModG.AllID_DB);
  UMyMod.AbreQuery(DmNFe_0000.QrCNAE, DModG.AllID_DB);
  //PopulateTree();
  QrArqs.Database := DModG.MyPID_DB;
  QrCP01.Database := DModG.MyPID_DB;
  QrCR01.Database := DModG.MyPID_DB;
  QrDP01.Database := DModG.MyPID_DB;
  QrDR01.Database := DModG.MyPID_DB;
  QrPR01.Database := DModG.MyPID_DB;
  QrXR01.Database := DModG.MyPID_DB;
  QrYR01.Database := DModG.MyPID_DB;
  QrZR01.Database := DModG.MyPID_DB;
  //
  QrNFeA.Database := DModG.MyPID_DB;
  QrNFeB.Database := DModG.MyPID_DB;
  QrNFeB12a.Database := DModG.MyPID_DB;
  QrNFeC.Database := DModG.MyPID_DB;
  QrNFeD.Database := DModG.MyPID_DB;
  QrNFeE.Database := DModG.MyPID_DB;
  QrNFeF.Database := DModG.MyPID_DB;
  QrNFeG.Database := DModG.MyPID_DB;
  //
  QrNFeI.Database := DModG.MyPID_DB;
  QrNFeIDi.Database := DModG.MyPID_DB;
  QrNFeIDiA.Database := DModG.MyPID_DB;
  QrNFeN.Database := DModG.MyPID_DB;
  QrNFeO.Database := DModG.MyPID_DB;
  QrNFeP.Database := DModG.MyPID_DB;
  QrNFeQ.Database := DModG.MyPID_DB;
  QrNFeR.Database := DModG.MyPID_DB;
  QrNFeS.Database := DModG.MyPID_DB;
  QrNFeT.Database := DModG.MyPID_DB;
  QrNFeU.Database := DModG.MyPID_DB;
  QrNFeV.Database := DModG.MyPID_DB;
  QrNFeW02.Database := DModG.MyPID_DB;
  QrNFeW17.Database := DModG.MyPID_DB;
  QrNFeW23.Database := DModG.MyPID_DB;
  QrNFeX01.Database := DModG.MyPID_DB;
  QrNFeX22.Database := DModG.MyPID_DB;
  QrNFeX26.Database := DModG.MyPID_DB;
  QrNFeX33.Database := DModG.MyPID_DB;
  QrNFeY01.Database := DModG.MyPID_DB;
  QrNFeY07.Database := DModG.MyPID_DB;
  QrNFeZ01.Database := DModG.MyPID_DB;
  QrNFeZ04.Database := DModG.MyPID_DB;
  QrNFeZ07.Database := DModG.MyPID_DB;
  QrNFeZ10.Database := DModG.MyPID_DB;
  QrNFeZA01.Database := DModG.MyPID_DB;
  QrNFeZB01.Database := DModG.MyPID_DB;
  //
  PageControl1.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmNFeLoad_Arq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmTesteXML.PopulateTree();
var
  nd : IXMLNode;

  procedure ProcessItem();
  var
    rssFeedData : PRSSFeedData;
    tn : TTreeNode;
    title : string;
  begin
    New(rssFeedData);

    title := nd.ChildNodes.FindNode('title').Text;

    with nd.ChildNodes do
    begin
      rssFeedData.URL := FindNode('link').Text;
      rssFeedData.Description := FindNode('description').Text;
      rssFeedData.Date  := FindNode('dc:date').Text;
    end;

    tn := TreeView1.Items.AddChild(TreeView1.Items.GetFirstNode, title);
    tn.Data := rssFeedData;
  end;
begin
  ClearTree;

  //Download a file from the Internet with progress indicator
  //http://delphi.about.com/cs/adptips2003/a/bltip0903_2.htm
  XMLDocument1.FileName := 'C:\Projetos\Delphi 2007\XML\Teste\RssFeedTree\ADPHealines.xml';
  XMLDocument1.Active := True;

  //"chanel"
  nd := XMLDocument1.DocumentElement.ChildNodes.First;
  //title
  nd := nd.ChildNodes.First;
  Caption := nd.Text + ' RSS Feed';
  while nd.NodeName <> 'item' do nd := nd.NextSibling;

  //add top tree node
  TreeView1.Items.AddChild(nil,'http://z.about.com/6/g/delphi/b/index.xml');

  //loop through "items"
  while nd <> nil do
  begin
    ProcessItem();
    nd := nd.NextSibling;
  end;

  //expand tree
  TreeView1.Items.GetFirstNode.Expand(true);

  //"close" RSS XML file
  XMLDocument1.Active := false;
end;
}

procedure TFmNFeLoad_Arq.SBArquivoClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArquivo.Text);
  Arquivo := ExtractFileName(EdArquivo.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
  begin
    EdArquivo.Text := Arquivo;
    ImportaXML();
  end;
end;

procedure TFmNFeLoad_Arq.SbCarregaClick(Sender: TObject);
begin
  ImportaXML();
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_AP01(XMLNohNivel0: IXMLNode);
var
  I: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  Campo: String;
begin
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo AP01!'
    );
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_AP01);
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('AP01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('idLote') then
          EdenviNFe_idLote.ValueVariant := dmkPF.XMLValI64(XMLNohNivel1)
        else
        if Campo = LowerCase('NFe') then
          ImportaNFe_Grupo_A00(XMLNohNivel1)
        else
          AvisaItemDesconhecido('AP01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_CP01(XMLNohNivel0: IXMLNode);
var
  I, J, K: Integer;
  AttrNode, XMLNohNivel1, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  cancNFe_versao: Double;
  infCanc_tpAmb: Integer;
  infCanc_nProt: Int64;
  infCanc_xAmb, infCanc_Id, infCanc_xServ, infCanc_chNFe, infCanc_xJust: WideString;
begin
  cancNFe_versao := 0; infCanc_tpAmb := 0; infCanc_nProt := 0; infCanc_Id := '';
  infCanc_xServ := ''; infCanc_chNFe := ''; infCanc_xJust := ''; infCanc_xAmb := '';
  //
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo CP01!'
    );
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_CP01);
    if Campo = LowerCase('versao') then
      cancNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('CP01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('infCanc') then
        begin
          for J := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel1.AttributeNodes.Nodes[J];
            Campo := Lowercase(AttrNode.NodeName);
            if Campo = LowerCase('Id') then
              infCanc_Id := dmkPF.XMLValTxt(AttrNode) else
            //
            AvisaItemDesconhecido('CP01', AttrNode.NodeName);
          end;
          if XMLNohNivel1.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
            begin
              XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[J];
              for K := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
              begin
                AttrNode := XMLNohNivel2.AttributeNodes.Nodes[K];
                Campo := Lowercase(AttrNode.NodeName);
                //if Campo = LowerCase('') then
                  //infCanc_ := Valor(AttrNode) else
                //
                AvisaItemDesconhecido('CP01', AttrNode.NodeName);
              end;
              if (XMLNohNivel2.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel2.NodeName);
                if Campo = LowerCase('tpAmb') then
                  infCanc_tpAmb := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('xServ') then
                  infCanc_xServ := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('chNFe') then
                  infCanc_chNFe := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('nProt') then
                  infCanc_nProt := dmkPF.XMLValI64(XMLNohNivel2)
                else
                if Campo = LowerCase('xJust') then
                  infCanc_xJust := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                  AvisaItemDesconhecido('CP01 -> infCanc', Campo);
              end;
            end;
          end;
        end else
        if Campo = LowerCase('Signature') then
          (**)
        else
        //
        AvisaItemDesconhecido('CP01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  infCanc_xAmb :=
    UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, infCanc_tpAmb);
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlCP01, False, [
  'cancNFe_versao', 'infCanc_Id', 'infCanc_tpAmb',
  'infCanc_xAmb', 'infCanc_xServ', 'infCanc_chNFe',
  'infCanc_nProt', 'infCanc_xJust'], [
  'SeqArq'], [
  cancNFe_versao, infCanc_Id, infCanc_tpAmb,
  infCanc_xAmb, infCanc_xServ, infCanc_chNFe,
  infCanc_nProt, infCanc_xJust], [
  FSeqArq], False) then ;
  //
  PageControl1.ActivePageIndex := 4;
  PageControl4.ActivePageIndex := 0;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_CR01(XMLNohNivel0: IXMLNode);
var
  I, J, K: Integer;
  AttrNode, XMLNohNivel1, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  retCanc_versao: Double;
  infCanc_tpAmb, infCanc_cStat, infCanc_cUF: Integer;
  infCanc_Id, infCanc_verAplic, infCanc_xMotivo, infCanc_chNFe,
  infCanc_dhRecbto, infCanc_xAmb, infCanc_xUF: String;
  infCanc_nProt: Int64;
begin
  retCanc_versao := 0;
  infCanc_tpAmb := 0; infCanc_cStat := 0; infCanc_cUF := 0;
  infCanc_Id := ''; infCanc_verAplic := ''; infCanc_xMotivo := '';
  infCanc_chNFe := ''; infCanc_dhRecbto := '';
  infCanc_nProt := 0;
  //
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo CR01!');
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_CR01);
    if Campo = LowerCase('versao') then
      retCanc_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('CR01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('infCanc') then
        begin
          for J := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel1.AttributeNodes.Nodes[J];
            Campo := Lowercase(AttrNode.NodeName);
            if Campo = LowerCase('Id') then
              infCanc_Id := dmkPF.XMLValTxt(AttrNode) else
            //
            AvisaItemDesconhecido('CR01', AttrNode.NodeName);
          end;
          if XMLNohNivel1.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
            begin
              XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[J];
              for K := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
              begin
                AttrNode := XMLNohNivel2.AttributeNodes.Nodes[K];
                Campo := Lowercase(AttrNode.NodeName);
                //if Campo = LowerCase('') then
                  //infCanc_ := Valor(AttrNode) else
                //
                AvisaItemDesconhecido('CR01', AttrNode.NodeName);
              end;
              if (XMLNohNivel2.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel2.NodeName);
                if Campo = LowerCase('tpAmb') then
                  infCanc_tpAmb := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('verAplic') then
                  infCanc_verAplic := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('cStat') then
                  infCanc_cStat := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('xMotivo') then
                  infCanc_xMotivo := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('cUF') then
                  infCanc_cUF := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('chNFe') then
                  infCanc_chNFe := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('dhRecbto') then
                  infCanc_dhRecbto := dmkPF.XMLValDth(XMLNohNivel2)
                else
                if Campo = LowerCase('nProt') then
                  infCanc_nProt := dmkPF.XMLValI64(XMLNohNivel2)
                else
                  AvisaItemDesconhecido('CR01 -> infCanc', Campo);
              end;
            end;
          end;
        end else
        if Campo = LowerCase('Signature') then
          (**)
        else
        //
        AvisaItemDesconhecido('CR01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  infCanc_xAmb := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, infCanc_tpAmb);
  infCanc_xUF  := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(infCanc_cUF);
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlCR01, False, [
  'retCanc_versao', 'infCanc_Id', 'infCanc_tpAmb',
  'infCanc_xAmb', 'infCanc_verAplic', 'infCanc_cStat',
  'infCanc_xMotivo', 'infCanc_cUF', 'infCanc_xUF',
  'infCanc_chNFe', 'infCanc_dhRecbto', 'infCanc_nProt'], [
  'SeqArq'], [
  retCanc_versao, infCanc_Id, infCanc_tpAmb,
  infCanc_xAmb, infCanc_verAplic, infCanc_cStat,
  infCanc_xMotivo, infCanc_cUF, infCanc_xUF,
  infCanc_chNFe, infCanc_dhRecbto, infCanc_nProt], [
  FSeqArq], False) then ;
  //
  PageControl1.ActivePageIndex := 4;
  PageControl4.ActivePageIndex := 1;
end;

// Solicita��o de inutiliza��o de Numera��o de NF-e
procedure TFmNFeLoad_Arq.ImportaGrupo_DP01(XMLNohNivel0: IXMLNode);
var
  I, J, K: Integer;
  AttrNode, XMLNohNivel1, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  inutNFe_versao: Double;
  inutNFe_tpAmb, inutNFe_cUF, inutNFe_ano, inutNFe_mod, inutNFe_serie,
  inutNFe_nNFIni, inutNFe_nNFFin: Integer;
  inutNFe_Id, inutNFe_xAmb, inutNFe_xServ, inutNFe_CNPJ, inutNFe_xJust,
  inutNFe_xUF: WideString;
begin
  inutNFe_versao := 0;
  inutNFe_tpAmb := 0; inutNFe_cUF := 0; inutNFe_ano := 0; inutNFe_mod := 0;
  inutNFe_serie := 0; inutNFe_nNFIni := 0; inutNFe_nNFFin := 0;
  inutNFe_Id := ''; inutNFe_xAmb := ''; inutNFe_xServ := ''; inutNFe_CNPJ := '';
  inutNFe_xJust := ''; inutNFe_xUF := '';
  //
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo DP01!');
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_DP01);
    if Campo = LowerCase('versao') then
      inutNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('DP01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('infInut') then
        begin
          for J := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel1.AttributeNodes.Nodes[J];
            Campo := Lowercase(AttrNode.NodeName);
            if Campo = LowerCase('Id') then
              inutNFe_Id := dmkPF.XMLValTxt(AttrNode) else
            //
            AvisaItemDesconhecido('DP01', AttrNode.NodeName);
          end;
          if XMLNohNivel1.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
            begin
              XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[J];
              for K := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
              begin
                AttrNode := XMLNohNivel2.AttributeNodes.Nodes[K];
                Campo := Lowercase(AttrNode.NodeName);
                //if Campo = LowerCase('') then
                  //cancNFe_ := Valor(AttrNode) else
                //
                AvisaItemDesconhecido('DP01', AttrNode.NodeName);
              end;
              if (XMLNohNivel2.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel2.NodeName);
                if Campo = LowerCase('tpAmb') then
                  inutNFe_tpAmb := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('xServ') then
                  inutNFe_xServ := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('cUF') then
                  inutNFe_cUF := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('ano') then
                  inutNFe_ano := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('CNPJ') then
                  inutNFe_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('mod') then
                  inutNFe_mod := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('serie') then
                  inutNFe_serie := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('nNFIni') then
                  inutNFe_nNFIni := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('nNFFin') then
                  inutNFe_nNFFin := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('xJust') then
                  inutNFe_xJust := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                  AvisaItemDesconhecido('DP01 -> infInut', Campo);
              end;
            end;
          end;
        end else
        if Campo = LowerCase('Signature') then
          (**)
        else
        //
        AvisaItemDesconhecido('DP01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  inutNFe_xAmb :=
    UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, inutNFe_tpAmb);

  inutNFe_xUF :=
    Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(inutNFe_cUF);

  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlDP01, False, [
  'inutNFe_versao', 'inutNFe_Id', 'inutNFe_tpAmb',
  'inutNFe_xAmb', 'inutNFe_xServ', 'inutNFe_cUF',
  'inutNFe_xUF', 'inutNFe_ano', 'inutNFe_CNPJ',
  'inutNFe_mod', 'inutNFe_serie', 'inutNFe_nNFIni',
  'inutNFe_nNFFin', 'inutNFe_xJust'], [
  'SeqArq'], [
  inutNFe_versao, inutNFe_Id, inutNFe_tpAmb,
  inutNFe_xAmb, inutNFe_xServ, inutNFe_cUF,
  inutNFe_xUF, inutNFe_ano, inutNFe_CNPJ,
  inutNFe_mod, inutNFe_serie, inutNFe_nNFIni,
  inutNFe_nNFFin, inutNFe_xJust], [
  FSeqArq], False) then ;
  //
  PageControl1.ActivePageIndex := 5;
  PageControl6.ActivePageIndex := 0;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_DR01(XMLNohNivel0: IXMLNode);
var
  I, J, K: Integer;
  AttrNode, XMLNohNivel1, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  retInutNFe_versao: Double;
  retInutNFe_tpAmb, retInutNFe_cStat, retInutNFe_cUF, retInutNFe_ano,
  retInutNFe_mod, retInutNFe_serie, retInutNFe_nNFIni, retInutNFe_nNFFin,
  retInutNFe_nProt: Integer;
  retInutNFe_Id, retInutNFe_xAmb, retInutNFe_verAplic, retInutNFe_xMotivo,
  retInutNFe_xUF, retInutNFe_CNPJ, retInutNFe_dhRecbto: String;
begin
  retInutNFe_versao := 0; retInutNFe_tpAmb := 0; retInutNFe_cStat := 0;
  retInutNFe_cUF := 0; retInutNFe_ano := 0; retInutNFe_mod := 0;
  retInutNFe_serie := 0; retInutNFe_nNFIni := 0; retInutNFe_nNFFin := 0;
  retInutNFe_nProt := 0;
  retInutNFe_Id := ''; retInutNFe_xAmb := ''; retInutNFe_verAplic := '';
  retInutNFe_xMotivo := ''; retInutNFe_xUF := ''; retInutNFe_CNPJ := '';
  retInutNFe_dhRecbto := '';
  //
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo DR01!');
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_DR01);
    if Campo = LowerCase('versao') then
      retInutNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('DR01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('infInut') then
        begin
          for J := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel1.AttributeNodes.Nodes[J];
            Campo := Lowercase(AttrNode.NodeName);
            if Campo = LowerCase('Id') then
              retInutNFe_Id := dmkPF.XMLValTxt(AttrNode) else
            //
            AvisaItemDesconhecido('DR01', AttrNode.NodeName);
          end;
          if XMLNohNivel1.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
            begin
              XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[J];
              for K := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
              begin
                AttrNode := XMLNohNivel2.AttributeNodes.Nodes[K];
                Campo := Lowercase(AttrNode.NodeName);
                //if Campo = LowerCase('') then
                  //retInutNFe_ := Valor(AttrNode) else
                //
                AvisaItemDesconhecido('DR01', AttrNode.NodeName);
              end;
              if (XMLNohNivel2.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel2.NodeName);
                if Campo = LowerCase('tpAmb') then
                  retInutNFe_tpAmb := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('verAplic') then
                  retInutNFe_verAplic := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('cStat') then
                  retInutNFe_cStat := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('xMotivo') then
                  retInutNFe_xMotivo := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('cUF') then
                  retInutNFe_cUF := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('ano') then
                  retInutNFe_ano := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('CNPJ') then
                  retInutNFe_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('mod') then
                  retInutNFe_mod := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('serie') then
                  retInutNFe_serie := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('nNFIni') then
                  retInutNFe_nNFIni := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('nNFFin') then
                  retInutNFe_nNFFin := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('dhRecbto') then
                  retInutNFe_dhRecbto := dmkPF.XMLValDth(XMLNohNivel2)
                else
                if Campo = LowerCase('nProt') then
                  retInutNFe_nProt := dmkPF.XMLValI64(XMLNohNivel2)
                else
                  AvisaItemDesconhecido('DR01 -> infInut', Campo);
              end;
            end;
          end;
        end else
        if Campo = LowerCase('Signature') then
          (**)
        else
        //
        AvisaItemDesconhecido('DR01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  retInutNFe_xAmb := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, retInutNFe_tpAmb);
  retInutNFe_xUF  := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(retInutNFe_cUF);
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlDR01, False, [
  'retInutNFe_versao', 'retInutNFe_Id', 'retInutNFe_tpAmb',
  'retInutNFe_xAmb', 'retInutNFe_verAplic', 'retInutNFe_cStat',
  'retInutNFe_xMotivo', 'retInutNFe_cUF', 'retInutNFe_xUF',
  'retInutNFe_ano', 'retInutNFe_CNPJ', 'retInutNFe_mod',
  'retInutNFe_serie', 'retInutNFe_nNFIni', 'retInutNFe_nNFFin',
  'retInutNFe_dhRecbto', 'retInutNFe_nProt'], [
  'SeqArq'], [
  retInutNFe_versao, retInutNFe_Id, retInutNFe_tpAmb,
  retInutNFe_xAmb, retInutNFe_verAplic, retInutNFe_cStat,
  retInutNFe_xMotivo, retInutNFe_cUF, retInutNFe_xUF,
  retInutNFe_ano, retInutNFe_CNPJ, retInutNFe_mod,
  retInutNFe_serie, retInutNFe_nNFIni, retInutNFe_nNFFin,
  retInutNFe_dhRecbto, retInutNFe_nProt], [
  FSeqArq], False) then ;
  //
  PageControl1.ActivePageIndex := 5;
  PageControl6.ActivePageIndex := 1;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_XR01(XMLNohNivel0: IXMLNode);
var
  I: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  nfeProcNFe_versao: Double;
  Campo: String;
begin
  nfeProcNFe_versao := 0;
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo XR01!');
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_XR01);
    if Campo = LowerCase('versao') then
      nfeProcNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('XR01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('NFe') then
          ImportaNFe_Grupo_A00(XMLNohNivel1)
        else
        if Campo = LowerCase('protNFe') then
          ImportaGrupo_PR01(XMLNohNivel1)
        else
        AvisaItemDesconhecido('XR01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlXR01, False, [
  'nfeProcNFe_versao'], [
  'SeqArq'], [
  nfeProcNFe_versao], [
  FSeqArq], False) then;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_YR01(XMLNohNivel0: IXMLNode);
var
  I: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  Campo: String;
  //
  procCancNFe_versao: Double;
begin
  procCancNFe_versao := 0;
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo YR01!');
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_YR01);
    if Campo = LowerCase('versao') then
      procCancNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('YR01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('cancNFe') then
          ImportaGrupo_CP01(XMLNohNivel1)
        else
        if Campo = LowerCase('retCancNFe') then
          ImportaGrupo_CR01(XMLNohNivel1)
        else
        AvisaItemDesconhecido('YR01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlYR01 , False, [
  'procCancNFe_versao'], [
  'SeqArq'], [
  procCancNFe_versao], [
  FSeqArq], False) then ;
  //
  PageControl1.ActivePageIndex := 4;
  PageControl4.ActivePageIndex := 1;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_ZR01(XMLNohNivel0: IXMLNode);
var
  I: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  Campo: String;
  //
  procInutNFe_versao: Double;
begin
  procInutNFe_versao := 0;
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
  begin
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo ZR01!');
    ShowMessage(XMLNohNivel0.Text);
  end;
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_ZR01);
    if Campo = LowerCase('versao') then
      procInutNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('ZR01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('inutNFe') then
          ImportaGrupo_DP01(XMLNohNivel1)
        else
        if Campo = LowerCase('retInutNFe') then
          ImportaGrupo_DR01(XMLNohNivel1)
        else
        AvisaItemDesconhecido('ZR01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlZR01 , False, [
  'procInutNFe_versao'], [
  'SeqArq'], [
  procInutNFe_versao], [
  FSeqArq], False) then ;
  //
  PageControl1.ActivePageIndex := 5;
  PageControl6.ActivePageIndex := 1;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_A00(XMLNohNivel0: IXMLNode);
var
  I: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  Campo: String;
begin
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo A00!');
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
{
    if Campo = LowerCase('versao') then
      versao := dmkPF.XMLValFlu(AttrNode) else
    if Campo = LowerCase('Id') then
      ID := dmkPF.XMLValTxt(AttrNode) else
}
    AvisaItemDesconhecido('A00', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('infNFe')      then
          ImportaNFe_Grupo_A01(XMLNohNivel1) else
        if Campo = LowerCase('Signature')   then
          FAssinado := 1(*ImportaNFe_Grupo_(XMLNohNivel1)*) else
        //
        AvisaItemDesconhecido('A00', XMLNohNivel1.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_A01(XMLNohNivel0: IXMLNode);
const
(*
  FatID   = 0;
  FatNum  = 0;
  Empresa = 0;
  Ativo   = 1;
*)
  IDCtrl  = 0;
  LoteEnv = 0;
var
  I: Integer;
  AttrNode, XMLNohNivel1: IXMLNode;
  Campo: String;
  versao: Double;
  Id: WideString;
begin
  // Para garantir que n�o usara do XML do arquivo anterior quando ler v�rios
  FCodInfoEmit := 0;
  //
  versao := 0;
  Id := '';
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo A01!');
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_A01);
    if Campo = LowerCase('versao') then
      versao := dmkPF.XMLValFlu(AttrNode) else
    if Campo = LowerCase('Id') then
      ID := Geral.SoNumero_TT(dmkPF.XMLValTxt(AttrNode)) else
    AvisaItemDesconhecido('A01', AttrNode.NodeName);
  end;
  FSeqNFe := FSeqNFe + 1;
  //Geral.MB_Info('Parei aqui! NFeLoad_Arq.2743');
  //Parei aqui
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabA, False, [
  //'FatID', 'FatNum', 'Empresa',
  'IDCtrl', 'LoteEnv',
  'versao', 'Id'(*,
  'Ativo'*)], [
  'SeqArq', 'SeqNFe'], [
  //FatID, FatNum, Empresa,
  IDCtrl, LoteEnv,
  versao, Id(*,
  Ativo*)], [
  FSeqArq, FSeqNFe], False) then
  begin
    // Subn�s
    if XMLNohNivel0.HasChildNodes then
    begin
      for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
      begin
        XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
        if (XMLNohNivel1.NodeType = ntElement) then
        begin
          Campo := Lowercase(XMLNohNivel1.NodeName);
          if Campo = LowerCase('ide')      then ImportaNFe_Grupo_B01(XMLNohNivel1) else
          if Campo = LowerCase('emit')     then ImportaNFe_Grupo_C01(XMLNohNivel1) else
          if Campo = LowerCase('avulsa')   then ImportaNFe_Grupo_D01(XMLNohNivel1) else
          if Campo = LowerCase('dest')     then ImportaNFe_Grupo_E01(XMLNohNivel1) else
          if Campo = LowerCase('retirada') then ImportaNFe_Grupo_F01(XMLNohNivel1) else
          if Campo = LowerCase('entrega')  then ImportaNFe_Grupo_G01(XMLNohNivel1) else
          if Campo = LowerCase('autxml')   then ImportaNFe_Grupo_GA01(XMLNohNivel1) else
          if Campo = LowerCase('det')      then ImportaNFe_Grupo_H01(XMLNohNivel1) else
          if Campo = LowerCase('total')    then ImportaNFe_Grupo_W01(XMLNohNivel1) else
          if Campo = LowerCase('transp')   then ImportaNFe_Grupo_X01(XMLNohNivel1) else
          if Campo = LowerCase('cobr')     then ImportaNFe_Grupo_Y01(XMLNohNivel1) else
          if Campo = LowerCase('infAdic')  then ImportaNFe_Grupo_Z01(XMLNohNivel1) else
          if Campo = LowerCase('exporta')  then ImportaNFe_Grupo_ZA01(XMLNohNivel1) else
          if Campo = LowerCase('compra')   then ImportaNFe_Grupo_ZB01(XMLNohNivel1) else
          //
          AvisaItemDesconhecido('A01', XMLNohNivel1.NodeName);
        end;
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_B01(XMLNohNivel1: IXMLNode);
(*
const
  FatID   = 0;
  FatNum  = 0;
  Empresa = 0;
*)
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  ide_cUF, ide_cNF, ide_indPag, ide_mod, ide_serie, ide_nNF, ide_tpNF,
  ide_cMunFG, ide_tpImp, ide_tpEmis, ide_cDV, ide_tpAmb, ide_finNFe,
  ide_procEmi, ide_idDest, ide_indFinal, ide_indPres: Integer;
  ide_natOp, ide_verProc, ide_xJust: WideString;
  ide_dEmi, ide_dhEmi, ide_dSaiEnt, ide_hSaiEnt, ide_dhCont, ide_xUF, ide_xTpNF,
  ide_xTpImp, ide_xTpEmis, ide_xAmb, ide_xFInNFe, ide_xIndPag, ide_xMunFG,
  ide_xProcEmi, ide_hEmi, ide_dhSaiEnt: String;
  ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo B01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_B01);
    if P = -1 then
      AvisaItemDesconhecido('B01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    ide_cUF := 0; ide_cNF := 0; ide_indPag := 0; ide_mod := 0; ide_serie := 0;
    ide_nNF := 0; ide_tpNF := 0; ide_cMunFG := 0; ide_tpImp := 0;
    ide_tpEmis := 0; ide_cDV := 0; ide_tpAmb := 0; ide_finNFe := 0;
    ide_procEmi := 0; ide_idDest := 0; ide_indFinal := 0; ide_indPres := 0;
    ide_natOp := ''; ide_verProc := ''; ide_xJust := ''; ide_xUF := '';
    ide_xTpNF := ''; ide_xTpImp := ''; ide_xTpEmis := ''; ide_xAmb := '';
    ide_xFInNFe := ''; ide_xIndPag := ''; ide_xMunFG := ''; ide_xProcEmi := '';
    //
    ide_dEmi := '0000-00-00';
    ide_hEmi := '00:00:00';
    ide_dSaiEnt := '0000-00-00';
    ide_hSaiEnt := '00:00:00';
    ide_dhCont := '0000-00-00 00:00:00';
    //
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('cUF') then
          ide_cUF := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('cNF') then
          ide_cNF := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('natOp') then
          ide_natOp := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('indPag') then
          ide_indPag := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('mod') then
          ide_mod := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('serie') then
          ide_serie := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('nNF') then
          ide_nNF := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('dEmi') then
          //TPide_dEmi.Date := XMLNohNivel2) else
          ide_dEmi := dmkPF.XMLValDta(XMLNohNivel2) else
        if Campo = LowerCase('dhEmi') then
          //ide_dhEmi := dmkPF.XMLValDth(XMLNohNivel2) else
          ide_dhEmi := dmkPF.XMLValTZD(XMLNohNivel2,  ide_dhEmiTZD) else
        if Campo = LowerCase('dSaiEnt') then
          //TPide_dSaiEnt.Date := XMLNohNivel2) else
          ide_dSaiEnt := dmkPF.XMLValDta(XMLNohNivel2) else
        if Campo = LowerCase('hSaiEnt') then
          ide_hSaiEnt := dmkPF.XMLValHra(XMLNohNivel2) else
        if Campo = LowerCase('dhSaiEnt') then
          //ide_dhSaiEnt := dmkPF.XMLValDth(XMLNohNivel2) else
          ide_dhSaiEnt := dmkPF.XMLValTZD(XMLNohNivel2,  ide_dhSaiEntTZD) else
        if Campo = LowerCase('tpNF') then
          ide_tpNF := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('idDest') then
          ide_idDest := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('cMunFG') then
          ide_cMunFG := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('NFref') then
          ImportaNFe_Grupo_B12a(XMLNohNivel2) else
        if Campo = LowerCase('tpImp') then
          ide_tpImp := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('tpEmis') then
          ide_tpEmis := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('cDV') then
          ide_cDV := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('tpAmb') then
          ide_tpAmb := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('finNFe') then
          ide_finNFe := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('indFinal') then
          ide_indFinal := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('indPres') then
          ide_indPres := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('procEmi') then
          ide_procEmi := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('verProc') then
          ide_verProc := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('dhCont') then
          ide_dhCont := dmkPF.XMLValDth(XMLNohNivel2) else
        if Campo = LowerCase('xJust') then
          ide_xJust := dmkPF.XMLValTxt(XMLNohNivel2) else

        AvisaItemDesconhecido('B01', Campo);
      end;
    end;
    if DmNFe_0000.QrDTB_Munici.Locate('Codigo', ide_cMunFG, []) then
      ide_xMunFG := DmNFe_0000.QrDTB_MuniciNome.Value;
    ide_xUF      := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(ide_cUF);
    ide_xTpNF    := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpNF,    ide_tpNF);
    ide_xTpImp   := Copy(UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpImp,   ide_tpImp), 1, 36);
    ide_xTpEmis  := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpEmis,  ide_tpEmis);
    ide_xAmb     := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb,   ide_tpAmb);
    ide_xFInNFe  := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  ide_finNFe);
    ide_xIndPag  := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_indPag,  ide_indPag);
    ide_xProcEmi := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_procEmi, ide_procEmi);
    //
    if ide_dhEmi <> '' then
    begin
      //XXe_PF.Ajusta_dh_XXe_UTC(ide_dhEmi, ide_dhEmiTZD);
      ide_dEmi := Copy(ide_dhEmi, 1, 10);
      ide_hEmi := Copy(ide_dhEmi, 11);
    end else
    begin
    end;
    if ide_dhSaiEnt <> '' then
    begin
      //XXe_PF.Ajusta_dh_XXe_UTC(ide_dhSaiEnt, ide_dhSaiEntTZD);
      ide_dSaiEnt := Copy(ide_dhSaiEnt, 1, 10);
      ide_hSaiEnt := Copy(ide_dhSaiEnt, 11);
    end else
    begin
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabB, False, [
    //'FatID', 'FatNum', 'Empresa',
    'ide_cUF', 'ide_xUF', 'ide_cNF',
    'ide_natOp', 'ide_indPag', 'ide_xIndPag',
    'ide_mod', 'ide_serie', 'ide_nNF',
    'ide_dEmi', 'ide_dSaiEnt', 'ide_hSaiEnt',
    'ide_tpNF', 'ide_xTpNF', 'ide_cMunFG',
    'ide_xMunFG', 'ide_tpImp', 'ide_xTpImp',
    'ide_tpEmis', 'ide_xTpEmis', 'ide_cDV',
    'ide_tpAmb', 'ide_xAmb', 'ide_finNFe',
    'ide_xFInNFe', 'ide_procEmi', 'ide_xProcEmi',
    'ide_verProc', 'ide_dhCont', 'ide_xJust',
    // 3.10
    'ide_hEmi', 'ide_dhEmiTZD', 'ide_idDest',
    'ide_dhSaiEntTZD', 'ide_indFinal',
    'ide_indPres'
    // FIM 3.10
    ], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    ide_cUF, ide_xUF, ide_cNF,
    ide_natOp, ide_indPag, ide_xIndPag,
    ide_mod, ide_serie, ide_nNF,
    ide_dEmi, ide_dSaiEnt, ide_hSaiEnt,
    ide_tpNF, ide_xTpNF, ide_cMunFG,
    ide_xMunFG, ide_tpImp, ide_xTpImp,
    ide_tpEmis, ide_xTpEmis, ide_cDV,
    ide_tpAmb, ide_xAmb, ide_finNFe,
    ide_xFInNFe, ide_procEmi, ide_xProcEmi,
    ide_verProc, ide_dhCont, ide_xJust,
    //
    ide_hEmi, ide_dhEmiTZD, ide_idDest,
    ide_dhSaiEntTZD, ide_indFinal,
    ide_indPres
    ], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_B12a(XMLNohNivel1: IXMLNode);
var
  I, J, P: Integer;
  AttrNode, XMLNohNivel2, XMLNohNivel3: IXMLNode;
  Campo: String;
  refNFe, refNF_CNPJ, refNFP_CNPJ, refNFP_CPF, refNFP_IE, refCTe, refECF_mod,
  refNFP_xUF, refNF_xUF: WideString;
  refNF_cUF, refNF_AAMM, refNF_mod, refNF_serie, refNF_nNF, refNFP_cUF,
  refNFP_AAMM, refNFP_mod, refNFP_serie, refNFP_nNF, refECF_nECF, refECF_nCOO,
  Controle: Integer;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo B12a!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_B12a);
    if P = -1 then
      AvisaItemDesconhecido('B12a', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    refNFe := ''; refNF_CNPJ := ''; refNFP_CNPJ := ''; refNFP_CPF := '';
    refNFP_IE := ''; refCTe := ''; refECF_mod := ''; refNFP_xUF := '';
    refNF_xUF := '';
    refNF_cUF := 0; refNF_AAMM := 0; refNF_mod := 0; refNF_serie := 0;
    refNF_nNF := 0; refNFP_cUF := 0; refNFP_AAMM := 0; refNFP_mod := 0;
    refNFP_serie := 0; refNFP_nNF := 0; refECF_nECF := 0; refECF_nCOO := 0;
    //
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('refNFe') then
          refNFe := dmkPF.XMLValTxt(XMLNohNivel2)
        else
        if Campo = LowerCase('refNF') then
        begin
          for J := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel2.AttributeNodes.Nodes[J];
            P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_B14);
            if P = -1 then
              AvisaItemDesconhecido('B14', AttrNode.NodeName);
          end;
          for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
          begin
            XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
            if (XMLNohNivel3.NodeType = ntElement) then
            begin
              Campo := Lowercase(XMLNohNivel3.NodeName);
              if Campo = LowerCase('cUF') then
                refNF_cUF := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('AAMM') then
                refNF_AAMM := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('CNPJ') then
                refNF_CNPJ := dmkPF.XMLValTxt(XMLNohNivel3)
              else
              if Campo = LowerCase('mod') then
                refNF_mod := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('serie') then
                refNF_serie := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('nNF') then
                refNF_nNF := dmkPF.XMLValInt(XMLNohNivel3)
              else
              //
              AvisaItemDesconhecido('B14', Campo);
            end;
          end;
        end
        else
        if Campo = LowerCase('refNFP') then
        begin
          for J := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel2.AttributeNodes.Nodes[J];
            P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_B20a);
            if P = -1 then
              AvisaItemDesconhecido('B20a', AttrNode.NodeName);
          end;
          for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
          begin
            XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
            if (XMLNohNivel3.NodeType = ntElement) then
            begin
              Campo := Lowercase(XMLNohNivel3.NodeName);
              if Campo = LowerCase('cUF') then
                refNFP_cUF := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('AAMM') then
                refNFP_AAMM := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('CNPJ') then
                refNFP_CNPJ := dmkPF.XMLValTxt(XMLNohNivel3)
              else
              if Campo = LowerCase('CPF') then
                refNFP_CPF := dmkPF.XMLValTxt(XMLNohNivel3)
              else
              if Campo = LowerCase('IE') then
                refNFP_IE := dmkPF.XMLValTxt(XMLNohNivel3)
              else
              if Campo = LowerCase('mod') then
                refNFP_mod := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('serie') then
                refNFP_serie := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('nNF') then
                refNFP_nNF := dmkPF.XMLValInt(XMLNohNivel3)
              else
              //
              AvisaItemDesconhecido('B20a', Campo);
            end;
          end;
        end
        else
        if Campo = LowerCase('refCTe') then
          refCTe := dmkPF.XMLValTxt(XMLNohNivel2)
        else
        if Campo = LowerCase('refECF') then
        begin
          for J := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel2.AttributeNodes.Nodes[J];
            P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_B20j);
            if P = -1 then
              AvisaItemDesconhecido('B20j', AttrNode.NodeName);
          end;
          for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
          begin
            XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
            if (XMLNohNivel3.NodeType = ntElement) then
            begin
              Campo := Lowercase(XMLNohNivel3.NodeName);
              if Campo = LowerCase('mod') then
                refECF_mod := dmkPF.XMLValTxt(XMLNohNivel3)
              else
              if Campo = LowerCase('nECF') then
                refECF_nECF := dmkPF.XMLValInt(XMLNohNivel3)
              else
              if Campo = LowerCase('nCOO') then
                refECF_nCOO := dmkPF.XMLValInt(XMLNohNivel3)
              else
              //
              AvisaItemDesconhecido('B20j', Campo);
            end;
          end;
        end
        else
        AvisaItemDesconhecido('B12a', Campo);
      end;
    end;
    FCtrlB12a  := FCtrlB12a + 1;
    Controle   := FCtrlB12a;
    refNF_xUF  := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(refNF_cUF);
    refNFP_xUF := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(refNFP_cUF);
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabB12a, False, [
    //'FatID', 'FatNum', 'Empresa',
    'refNFe', 'refNF_cUF', 'refNF_xUF',
    'refNF_AAMM', 'refNF_CNPJ', 'refNF_mod',
    'refNF_serie', 'refNF_nNF', 'refNFP_cUF',
    'refNFP_xUF', 'refNFP_AAMM', 'refNFP_CNPJ',
    'refNFP_CPF', 'refNFP_IE', 'refNFP_mod',
    'refNFP_serie', 'refNFP_nNF', 'refCTe',
    'refECF_mod', 'refECF_nECF', 'refECF_nCOO'], [
    'SeqArq', 'SeqNFe', 'Controle'], [
    //FatID, FatNum, Empresa,
    refNFe, refNF_cUF, refNF_xUF,
    refNF_AAMM, refNF_CNPJ, refNF_mod,
    refNF_serie, refNF_nNF, refNFP_cUF,
    refNFP_xUF, refNFP_AAMM, refNFP_CNPJ,
    refNFP_CPF, refNFP_IE, refNFP_mod,
    refNFP_serie, refNFP_nNF, refCTe,
    refECF_mod, refECF_nECF, refECF_nCOO], [
    FSeqArq, FSeqNFe, Controle], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_C01(XMLNohNivel1: IXMLNode);
var
  I, J, P: Integer;
  AttrNode, XMLNohNivel2, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  emit_cMun, emit_CEP, emit_cPais, emit_CRT: Integer;
  emit_CNPJ, emit_CPF, emit_xNome, emit_xFant, emit_xLgr, emit_nro, emit_xCpl,
  emit_xBairro, emit_xMun, emit_UF, emit_xPais, emit_fone, emit_IE, emit_IEST,
  emit_IM, emit_CNAE, emit_xCNAE, emit_xCRT, NomeEmit: WideString;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo C01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_C01);
    if P = -1 then
      AvisaItemDesconhecido('C01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    emit_cMun := 0; emit_CEP := 0; emit_cPais := 0; emit_CRT := 0;
    // Para garantir que n�o usara do XML do arquivo anterior quando ler v�rios
    FCodInfoEmit := 0;
    emit_CNPJ := ''; emit_CPF := ''; emit_xNome := ''; emit_xFant := '';
    emit_xLgr := ''; emit_nro := ''; emit_xCpl := ''; emit_xBairro := '';
    emit_xMun := ''; emit_UF := ''; emit_xPais := ''; emit_fone := '';
    emit_IE := ''; emit_IEST := ''; emit_IM := ''; emit_CNAE := '';
    emit_xCNAE := ''; emit_xCRT := ''; NomeEmit := '';
    //
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('CNPJ') then
          emit_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CPF') then
          emit_CPF := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xNome') then
          emit_xNome := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xFant') then
          emit_xFant := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('enderEmit') then
        begin
          //ImportaNFe_Grupo_C05(XMLSubNode) else
          for j := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel2.AttributeNodes.Nodes[I];
            P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_C05);
            if P = -1 then
              AvisaItemDesconhecido('C05', AttrNode.NodeName);
          end;
          // Subn�s
          if XMLNohNivel2.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                if Campo = Lowercase('xLgr')  then
                  emit_xLgr := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('nro')  then
                  emit_nro := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('xCpl')  then
                  emit_xCpl := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('xBairro')  then
                  emit_xBairro := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('cMun')  then
                  emit_cMun := dmkPF.XMLValInt(XMLNohNivel3) else
                if Campo = Lowercase('xMun')  then
                  emit_xMun := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('UF')  then
                  emit_UF := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('CEP')  then
                  emit_CEP := dmkPF.XMLValInt(XMLNohNivel3) else
                if Campo = Lowercase('cPais')  then
                  emit_cPais := dmkPF.XMLValInt(XMLNohNivel3) else
                if Campo = Lowercase('xPais')  then
                  emit_xPais := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('fone')  then
                  emit_fone := dmkPF.XMLValTxt(XMLNohNivel3) else
                //
                AvisaItemDesconhecido('C05', XMLNohNivel3.NodeName);
              end;
            end;
          end;
        end
        else
        if Campo = LowerCase('IE') then
          emit_IE := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('IEST') then
          emit_IEST := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('IM') then
          emit_IM := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CNAE') then
          emit_CNAE := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CRT') then
          emit_CRT := dmkPF.XMLValInt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('C01', XMLNohNivel2.NodeName);
      end;
    end;
    if emit_CNAE = '' then
      emit_xCNAE := ''
    else
    if DmNFe_0000.QrCNAE.Locate('CodAlf', emit_CNAE, []) then
      emit_xCNAE := Copy(DmNFe_0000.QrCNAENome.Value, 1, 100)
    else
    if DmNFe_0000.QrCNAE.Locate('CodTxt', emit_CNAE, []) then
      emit_xCNAE := Copy(DmNFe_0000.QrCNAENome.Value, 1, 100)
    else
      Geral.MB_Aviso('C�digo CNAE "' + emit_CNAE + '" n�o localizado!' + sLineBreak +
      'Verifique se a tabela p�blica de CNAEs est� atualizada!');
    //  
    emit_xCRT := UFinanceiro.CRT_Get(emit_CRT);
    DefineEntidade(XMLNohNivel2, emit_CNPJ, emit_CPF, FCodInfoEmit, NomeEmit);
    QrFilial.Close;
    if FCodInfoEmit <> 0 then
    begin
      QrFilial.Params[0].AsInteger := FCodInfoEmit;
      UnDmkDAC_PF.AbreQueryApenas(QrFilial);
    end;
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabC, False, [
    //'FatID', 'FatNum', 'Empresa',
{
    'emit_CNPJ', 'emit_CPF', 'emit_xNome',
    'emit_xFant', 'emit_xLgr', 'emit_nro',
    'emit_xCpl', 'emit_xBairro', 'emit_cMun',
    'emit_xMun', 'emit_UF', 'emit_CEP',
    'emit_cPais', 'emit_xPais', 'emit_fone',
    'emit_IE', 'emit_IEST', 'emit_IM',
    'emit_CNAE', 'emit_CRT'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    emit_CNPJ, emit_CPF, emit_xNome,
    emit_xFant, emit_xLgr, emit_nro,
    emit_xCpl, emit_xBairro, emit_cMun,
    emit_xMun, emit_UF, emit_CEP,
    emit_cPais, emit_xPais, emit_fone,
    emit_IE, emit_IEST, emit_IM,
    emit_CNAE, emit_CRT], [
}
    'emit_CNPJ', 'emit_CPF', 'emit_xNome',
    'emit_xFant', 'emit_xLgr', 'emit_nro',
    'emit_xCpl', 'emit_xBairro', 'emit_cMun',
    'emit_xMun', 'emit_UF', 'emit_CEP',
    'emit_cPais', 'emit_xPais', 'emit_fone',
    'emit_IE', 'emit_IEST', 'emit_IM',
    'emit_CNAE', 'emit_CRT', 'emit_xCRT',
    'CodInfoEmit', 'NomeEmit'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    emit_CNPJ, emit_CPF, emit_xNome,
    emit_xFant, emit_xLgr, emit_nro,
    emit_xCpl, emit_xBairro, emit_cMun,
    emit_xMun, emit_UF, emit_CEP,
    emit_cPais, emit_xPais, emit_fone,
    emit_IE, emit_IEST, emit_IM,
    emit_CNAE, emit_CRT, emit_xCRT,
    FCodInfoEmit, NomeEmit], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_D01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  avulsa_CNPJ, avulsa_xOrgao, avulsa_matr, avulsa_xAgente, avulsa_fone,
  avulsa_UF, avulsa_nDAR, avulsa_dEmi, avulsa_repEmi, avulsa_dPag: String;
  avulsa_vDAR: Double;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo D01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_D01);
    if P = -1 then
      AvisaItemDesconhecido('D01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    avulsa_CNPJ := ''; avulsa_xOrgao := ''; avulsa_matr := '';
    avulsa_xAgente := ''; avulsa_fone := ''; avulsa_UF := ''; avulsa_nDAR := '';
    avulsa_dEmi := ''; avulsa_repEmi := ''; avulsa_dPag := '';
    avulsa_vDAR := 0;
    //
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('CNPJ') then
          avulsa_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xOrgao') then
          avulsa_xOrgao := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('matr') then
          avulsa_matr := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xAgente') then
          avulsa_xAgente := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('fone') then
          avulsa_fone := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('UF') then
          avulsa_UF := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('nDAR') then
          avulsa_nDAR := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('dEmi') then
          avulsa_dEmi := dmkPF.XMLValDta(XMLNohNivel2) else
        if Campo = LowerCase('vDAR') then
          avulsa_vDAR := dmkPF.XMLValFlu(XMLNohNivel2) else
        if Campo = LowerCase('repEmi') then
          avulsa_repEmi := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('dPag') then
          avulsa_dPag := dmkPF.XMLValDta(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('D01', XMLNohNivel2.NodeName);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabD, False, [
    //'FatID', 'FatNum', 'Empresa',
    'avulsa_CNPJ', 'avulsa_xOrgao', 'avulsa_matr',
    'avulsa_xAgente', 'avulsa_fone', 'avulsa_UF',
    'avulsa_nDAR', 'avulsa_dEmi', 'avulsa_vDAR',
    'avulsa_repEmi', 'avulsa_dPag'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    avulsa_CNPJ, avulsa_xOrgao, avulsa_matr,
    avulsa_xAgente, avulsa_fone, avulsa_UF,
    avulsa_nDAR, avulsa_dEmi, avulsa_vDAR,
    avulsa_repEmi, avulsa_dPag], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_E01(XMLNohNivel1: IXMLNode);
const
  xFant = ''; IEST = ''; IM = ''; CNAE = ''; xPais = ''; xEnder = '';
var
  I, J, P: Integer;
  AttrNode, XMLNohNivel2, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  dest_CNPJ, dest_CPF, dest_xNome, dest_xLgr, dest_nro, dest_xCpl, dest_xBairro,
  dest_xMun, dest_UF, dest_xPais, dest_fone, dest_IE, dest_ISUF, dest_email,
  dest_IM, NomeDest: WideString;
  dest_cMun, dest_CEP, dest_cPais, CodInfoDest, dest_indIEDest: Integer;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo E01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_E01);
    if P = -1 then
      AvisaItemDesconhecido('E01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    dest_CNPJ := ''; dest_CPF := ''; dest_xNome := ''; dest_xLgr := '';
    dest_nro := ''; dest_xCpl := ''; dest_xBairro := ''; dest_xMun := '';
    dest_UF := ''; dest_xPais := ''; dest_fone := ''; dest_IE := '';
    dest_ISUF := ''; dest_email := ''; dest_IM := '';
    dest_cMun := 0; dest_CEP := 0; dest_cPais := 0; dest_indIEDest := 0;
    //
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('CNPJ') then
          dest_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CPF') then
          dest_CPF := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xNome') then
          dest_xNome := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('enderdest') then
        begin
          //ImportaNFe_Grupo_E05(XMLNohNivel2) else
          for J := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel2.AttributeNodes.Nodes[J];
            P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_E05);
            if P = -1 then
              AvisaItemDesconhecido('E05', AttrNode.NodeName);
          end;
          // Subn�s
          if XMLNohNivel2.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                if Campo = Lowercase('xLgr')  then
                  dest_xLgr := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('nro')  then
                  dest_nro := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('xCpl')  then
                  dest_xCpl := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('xBairro')  then
                  dest_xBairro := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('cMun')  then
                  dest_cMun := dmkPF.XMLValInt(XMLNohNivel3) else
                if Campo = Lowercase('xMun')  then
                  dest_xMun := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('UF')  then
                  dest_UF := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('CEP')  then
                  dest_CEP := dmkPF.XMLValInt(XMLNohNivel3) else
                if Campo = Lowercase('cPais')  then
                  dest_cPais := dmkPF.XMLValInt(XMLNohNivel3) else
                if Campo = Lowercase('xPais')  then
                  dest_xPais := dmkPF.XMLValTxt(XMLNohNivel3) else
                if Campo = Lowercase('fone')  then
                  dest_fone := dmkPF.XMLValTxt(XMLNohNivel3) else
                //
                AvisaItemDesconhecido('E01 -> enderdest', XMLNohNivel3.NodeName);
              end;
            end;
          end;
        end
        else
        if Campo = Lowercase('indIEDest')  then
          dest_indIEDest := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('IE') then
          dest_IE := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('ISUF') then
          dest_ISUF := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('email') then
          dest_email := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('IM') then
          dest_IM := dmkPF.XMLValTxt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('E01', XMLNohNivel2.NodeName);
      end;
    end;
    if not DefineEntidade(
      XMLNohNivel2, dest_CNPJ, dest_CPF, CodInfoDest, NomeDest) then
    CodInfoDest := CadastraEntidade(entNFe_dest, dest_CNPJ, dest_CPF,
      dest_xNome, xFant, dest_IE, dest_xLgr, dest_nro, dest_xBairro,
      dest_cMun, dest_xMun, dest_UF, dest_CEP, dest_cPais, xPais,
      dest_fone, IEST, IM, CNAE, dest_xCpl, xEnder);
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FcabE, False, [
    //'FatID', 'FatNum', 'Empresa',
{
    'dest_CNPJ', 'dest_CPF', 'dest_xNome',
    'dest_xLgr', 'dest_nro', 'dest_xCpl',
    'dest_xBairro', 'dest_cMun', 'dest_xMun',
    'dest_UF', 'dest_CEP', 'dest_cPais',
    'dest_xPais', 'dest_fone', 'dest_IE',
    'dest_ISUF', 'dest_email'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    dest_CNPJ, dest_CPF, dest_xNome,
    dest_xLgr, dest_nro, dest_xCpl,
    dest_xBairro, dest_cMun, dest_xMun,
    dest_UF, dest_CEP, dest_cPais,
    dest_xPais, dest_fone, dest_IE,
    dest_ISUF, dest_email], [
}
    'dest_CNPJ', 'dest_CPF', 'dest_xNome',
    'dest_xLgr', 'dest_nro', 'dest_xCpl',
    'dest_xBairro', 'dest_cMun', 'dest_xMun',
    'dest_UF', 'dest_CEP', 'dest_cPais',
    'dest_xPais', 'dest_fone', 'dest_IE',
    'dest_ISUF', 'dest_email', 'dest_IM',
    'CodInfoDest', 'NomeDest', 'dest_indIEDest'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    dest_CNPJ, dest_CPF, dest_xNome,
    dest_xLgr, dest_nro, dest_xCpl,
    dest_xBairro, dest_cMun, dest_xMun,
    dest_UF, dest_CEP, dest_cPais,
    dest_xPais, dest_fone, dest_IE,
    dest_ISUF, dest_email, dest_IM,
    CodInfoDest, NomeDest, dest_indIEDest], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_F01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  retirada_CNPJ, retirada_CPF, retirada_xLgr, retirada_nro, retirada_xCpl,
  retirada_xBairro, retirada_xMun, retirada_UF: WideString;
  retirada_cMun: Integer;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo F01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_F01);
    if P = -1 then
      AvisaItemDesconhecido('F01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    retirada_CNPJ := ''; retirada_CPF := ''; retirada_xLgr := '';
    retirada_nro := ''; retirada_xCpl := ''; retirada_xBairro := '';
    retirada_xMun := ''; retirada_UF := '';
    retirada_cMun := 0;
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('CNPJ') then
          retirada_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CPF') then
          retirada_CPF := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xLgr') then
          retirada_xLgr := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('nro') then
          retirada_nro := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xCpl') then
          retirada_xCpl := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xBairro') then
          retirada_xBairro := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('cMun') then
          retirada_cMun := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('xMun') then
          retirada_xMun := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('UF') then
          retirada_UF := dmkPF.XMLValTxt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('F01', XMLNohNivel2.NodeName);
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FCabF, False, [
    //'FatID', 'FatNum', 'Empresa',
    'retirada_CNPJ', 'retirada_CPF', 'retirada_xLgr',
    'retirada_nro', 'retirada_xCpl', 'retirada_xBairro',
    'retirada_cMun', 'retirada_xMun', 'retirada_UF'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    retirada_CNPJ, retirada_CPF, retirada_xLgr,
    retirada_nro, retirada_xCpl, retirada_xBairro,
    retirada_cMun, retirada_xMun, retirada_UF], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_G01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  entrega_CNPJ, entrega_CPF, entrega_xLgr, entrega_nro, entrega_xCpl,
  entrega_xBairro, entrega_xMun, entrega_UF: WideString;
  entrega_cMun: Integer;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo G01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_G01);
    if P = -1 then
      AvisaItemDesconhecido('G01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    entrega_CNPJ := ''; entrega_CPF := ''; entrega_xLgr := '';
    entrega_nro := ''; entrega_xCpl := ''; entrega_xBairro := '';
    entrega_xMun := ''; entrega_UF := '';
    entrega_cMun := 0;
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('CNPJ') then
          entrega_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CPF') then
          entrega_CPF := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xLgr') then
          entrega_xLgr := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('nro') then
          entrega_nro := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xCpl') then
          entrega_xCpl := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xBairro') then
          entrega_xBairro := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('cMun') then
          entrega_cMun := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('xMun') then
          entrega_xMun := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('UF') then
          entrega_UF := dmkPF.XMLValTxt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('G01', XMLNohNivel2.NodeName);
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FCabG, False, [
    //'FatID', 'FatNum', 'Empresa',
    'entrega_CNPJ', 'entrega_CPF', 'entrega_xLgr',
    'entrega_nro', 'entrega_xCpl', 'entrega_xBairro',
    'entrega_cMun', 'entrega_xMun', 'entrega_UF'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    entrega_CNPJ, entrega_CPF, entrega_xLgr,
    entrega_nro, entrega_xCpl, entrega_xBairro,
    entrega_cMun, entrega_xMun, entrega_UF], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_GA01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  autXML_CNPJ, autXML_CPF: WideString;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo GA01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    P := AnsiIndexStr(Lowercase(AttrNode.NodeName), FGrupo_GA01);
    if P = -1 then
      AvisaItemDesconhecido('GA01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    autXML_CNPJ := ''; autXML_CPF := '';
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('CNPJ') then
          autXML_CNPJ := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('CPF') then
          autXML_CPF := dmkPF.XMLValTxt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('GA01', XMLNohNivel2.NodeName);
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FCabGA, False, [
    //'FatID', 'FatNum', 'Empresa',
    'autXML_CNPJ', 'autXML_CPF'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    autXML_CNPJ, autXML_CPF], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_H01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  nItem, Campo: String;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo H01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_H01);
    if P = -1 then
      AvisaItemDesconhecido('H01', AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_H01);
    if Campo = LowerCase('nItem') then
      nItem := AttrNode.NodeValue else
    AvisaItemDesconhecido('H01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('prod') then
          ImportaNFe_Grupo_I01(XMLNohNivel2, nItem) else
        if Campo = LowerCase('veicProd') then
          ImportaNFe_Grupo_J01(XMLNohNivel2, nItem) else
        if Campo = LowerCase('med') then
          ImportaNFe_Grupo_K01(XMLNohNivel2, nItem) else
        if Campo = LowerCase('arma') then
          ImportaNFe_Grupo_L01(XMLNohNivel2, nItem) else
        if Campo = LowerCase('comb') then
          ImportaNFe_Grupo_L101(XMLNohNivel2, nItem) else
        if Campo = LowerCase('imposto') then
          ImportaNFe_Grupo_M01(XMLNohNivel2, nItem) else
        if Campo = LowerCase('infAdProd') then
          ImportaNFe_Grupo_V01(XMLNohNivel2, nItem) else
        //
        AvisaItemDesconhecido('H01', Campo);
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_I01(XMLNohNivel2: IXMLNode; nItem: WideString);
var
  I, P, GraGruX, nItemPed, prod_CEST, Nivel1, CFOP, indTot: Integer;
  AttrNode, XMLNohNivel3: IXMLNode;
  Campo, cProd_, NomeGGX: String;
  //
  cProd, cEAN, xProd, NCM, EXTIPI, uCom, qCom, vUnCom, vProd, cEANTrib, uTrib,
  qTrib, vUnTrib, vFrete, vSeg, vDesc, vOutro, xPed, prod_nFCI: WideString;
  KlsGGXE: TKindLocSelGGXE;
  Achou: Boolean;
begin
  if not (XMLNohNivel2.NodeType = ntElement) then
    Exit;
  if XMLNohNivel2.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo I01!');
  for I := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel2.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_I01);
    if P = -1 then
      AvisaItemDesconhecido('I01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel2.HasChildNodes then
  begin
    cProd := '';  cEAN := '';
    xProd := '';  NCM := '';  EXTIPI := '';
    CFOP := 0;  uCom := '';  qCom := '';
    vUnCom := '';  vProd := '';  cEANTrib := '';
    uTrib := '';  qTrib := '0';  vUnTrib := '0';
    vFrete := '0';  vSeg := '0';  vDesc := '0';
    vOutro := '0';  indTot := 0;  xPed := '';
    nItemPed := 0; NomeGGX := '';  prod_nFCI := ''; prod_CEST := 0;
    KlsGGXE := TKindLocSelGGXE.klsggxeIndef;
    //GraGruX := 0;
    for I := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
    begin
      XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
      if (XMLNohNivel3.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel3.NodeName);
        if Campo = LowerCase('cProd') then
          cProd := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('cEAN') then
          cEAN := dmkPF.XMLValTxt(XMLNohNivel3) else
        if Campo = LowerCase('xProd') then
          xProd := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('NCM') then
          NCM := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('EXTIPI') then
          EXTIPI := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('CFOP') then
          CFOP := dmkPF.XMLValInt(XMLNohNivel3) else
        if Campo = LowerCase('uCom') then
          uCom := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('qCom') then
          qCom := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vUnCom') then
          vUnCom := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vProd') then
          vProd := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('cEANTrib') then
          cEANTrib := dmkPF.XMLValTxt(XMLNohNivel3) else
        if Campo = LowerCase('uTrib') then
          uTrib := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('qTrib') then
          qTrib := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vUnTrib') then
          vUnTrib := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vFrete') then
          vFrete := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vSeg') then
          vSeg := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vDesc') then
          vDesc := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('vOutro') then
          vOutro := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('indTot') then
          indTot := dmkPF.XMLValInt(XMLNohNivel3) else
        if Campo = LowerCase('DI') then
          ImportaNFe_Grupo_I18(XMLNohNivel3, nItem) else
        if Campo = LowerCase('xPed') then
          xPed := XMLNohNivel3.NodeValue else
        if Campo = LowerCase('nItemPed') then
          nItemPed := dmkPF.XMLValInt(XMLNohNivel3) else
        if Campo = LowerCase('nFCI') then
          prod_nFCI := dmkPF.XMLValTxt(XMLNohNivel3) else
        if Campo = LowerCase('CEST') then
          prod_CEST := dmkPF.XMLValInt(XMLNohNivel3) else
        // Deprecado
        if Campo = LowerCase('genero') then
           else
        // Fim Deprecado
        AvisaItemDesconhecido('I01', Campo);
      end;
    end;
    //
    NomeGGX := '';
    GraGruX := 0;
    Achou := False;
    if EhEmissaoPropria() then
    begin
      cProd_ := Trim(cProd);
      if Geral.SoNumero_TT(cProd_) = cProd_ then
      begin
        GraGruX := Geral.IMV(cProd_);
        (**)
        QrLocGGX.Close;
        QrLocGGX.Params[0].AsInteger := GraGruX;
        UnDmkDAC_PF.AbreQueryApenas(QrLocGGX);
        if QrLocGGXItens.Value = 0 then
        begin
          GraGruX := 0;
          (*
          Geral.MB_Aviso(
          'O reduzido: ' + sLineBreak + cProd + ' ( "' + xProd +
          '" ) n�o foi localizado no cadastro de produtos por grade!');
          *)
        end else
          Achou := True;
      end; (*else
        Geral.MB_Aviso(
        'Produto n�o localizado pelo reduzido para emiss�o pr�pria!')*);
      if Achou = False then
      begin
        KlsGGXE := Grade_PF.SelecionaGraGruXDeGraGruE(FCodInfoEmit, cProd, xProd, NCM, uCom,
        CFOP, GraGruX, Nivel1, NomeGGX);
        if GraGruX = 0 then
          Geral.MB_Aviso(
          'O reduzido: ' + sLineBreak + cProd + ' ( "' + xProd +
          '" ) n�o foi localizado no cadastro de produtos por grade!')(**);
      end;
    end
    else begin
      KlsGGXE := Grade_PF.SelecionaGraGruXDeGraGruE(FCodInfoEmit, cProd, xProd, NCM, uCom,
      CFOP, GraGruX, Nivel1, NomeGGX);
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsI, False, [
    //'FatID', 'FatNum', 'Empresa',
{
    'nItem', 'prod_cProd', 'prod_cEAN',
    'prod_xProd', 'prod_NCM', 'prod_EXTIPI',
    'prod_CFOP', 'prod_uCom', 'prod_qCom',
    'prod_vUnCom', 'prod_vProd', 'prod_cEANTrib',
    'prod_uTrib', 'prod_qTrib', 'prod_vUnTrib',
    'prod_vFrete', 'prod_vSeg', 'prod_vDesc',
    'prod_vOutro', 'prod_indTot', 'prod_xPed',
    'prod_nItemPed'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, cProd, cEAN,
    xProd, NCM, EXTIPI,
    CFOP, uCom, qCom,
    vUnCom, vProd, cEANTrib,
    uTrib, qTrib, vUnTrib,
    vFrete, vSeg, vDesc,
    vOutro, indTot, xPed,
    nItemPed], [
}
    'prod_cProd', 'prod_cEAN', 'prod_xProd',
    'prod_NCM', 'prod_EXTIPI', 'prod_CFOP',
    'prod_uCom', 'prod_qCom', 'prod_vUnCom',
    'prod_vProd', 'prod_cEANTrib', 'prod_uTrib',
    'prod_qTrib', 'prod_vUnTrib', 'prod_vFrete',
    'prod_vSeg', 'prod_vDesc', 'prod_vOutro',
    'prod_indTot', 'prod_xPed', 'prod_nItemPed',
    'GraGruX', 'NomeGGX', 'Nivel1',
    'prod_nFCI', 'prod_CEST', 'KlsGGXE'], [
    'SeqArq', 'SeqNFe', 'nItem'], [
    //FatID, FatNum, Empresa,
    cProd, cEAN, xProd,
    NCM, EXTIPI, CFOP,
    uCom, qCom, vUnCom,
    vProd, cEANTrib, uTrib,
    qTrib, vUnTrib, vFrete,
    vSeg, vDesc, vOutro,
    indTot, xPed, nItemPed,
    GraGruX, NomeGGX, Nivel1,
    prod_nFCI, prod_CEST, Integer(KlsGGXE)], [
    FSeqArq, FSeqNFe, nItem], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_I18(XMLNohNivel3: IXMLNode; nItem:
  WideString);
var
  I, J, P, Controle, Conta: Integer;
  AttrNode, XMLNohNivel4, XMLNohNivel5: IXMLNode;
  Campo: String;
  //
  nDI, dDI, xLocDesemb, UFDesemb, dDesemb, cExportador,
  nAdicao, nSeqAdic, cFabricante, vDescDI: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo I18!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_I18);
    if P = -1 then
      AvisaItemDesconhecido('I18', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    nDI := ''; dDI := ''; xLocDesemb := ''; UFDesemb := ''; dDesemb := '0000/00/00';
    cExportador := '';
    //
    FCtrlIDi := FCtrlIDi + 1;
    Controle := FCtrlIDi;
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_I18);
        if P = -1 then
          AvisaItemDesconhecido('I18', Campo)
        else begin
          if Campo = LowerCase('nDI') then
            nDI := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('dDI') then
            dDI := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('xLocDesemb') then
            xLocDesemb := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('UFDesemb') then
            UFDesemb := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('dDesemb') then
            dDesemb := dmkPF.XMLValDta(XMLNohNivel4) else
          if Campo = LowerCase('cExportador') then
            cExportador := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('adi') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              nAdicao := ''; nSeqAdic := ''; cFabricante := ''; vDescDI := '';
              //
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_I25);
                  if P = -1 then
                    AvisaItemDesconhecido('I25', Campo)
                  else begin
                    if Campo = LowerCase('nAdicao') then
                      nAdicao := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('nSeqAdic') then
                      nSeqAdic := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('cFabricante') then
                      cFabricante := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vDescDI') then
                      vDescDI := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('I25', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
              FCtrlIDiA := FCtrlIDiA + 1;
              Conta := FCtrlIDiA;
              //
              if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsIDiA, False, [
              //'FatID', 'FatNum', 'Empresa',
              'Controle', 'Conta', 'DI_nDI',
              'Adi_nAdicao', 'Adi_nSeqAdic', 'Adi_cFabricante',
              'Adi_vDescDI'], [
              'SeqArq', 'SeqNFe', 'nItem'], [
              //FatID, FatNum, Empresa,
              Controle, Conta, nDI,
              nAdicao, nSeqAdic, cFabricante,
              vDescDI], [
              FSeqArq, FSeqNFe, nItem], False) then ;
            end;
          end else
            AvisaItemDesconhecido('I18', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsIDi, False, [
    //'FatID', 'FatNum', 'Empresa',
    'DI_nDI', 'DI_dDI', 'DI_xLocDesemb',
    'DI_UFDesemb', 'DI_dDesemb', 'DI_cExportador'], [
    'nItem', 'SeqArq', 'SeqNFe', 'Controle'], [
    //FatID, FatNum, Empresa,
    nDI, dDI, xLocDesemb,
    UFDesemb, dDesemb, cExportador], [
    nItem, FSeqArq, FSeqNFe, Controle
    ], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_J01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
begin
  Geral.MB_Aviso(
  'Detalhamento Espec�fico de Ve�culos Novos n�o implementado! AVISE A DERMATEK');
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_K01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
begin
  Geral.MB_Aviso(
  'Detalhamento Espec�fico de Medicamento n�o implementado! AVISE A DERMATEK');

end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_L01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
begin
  Geral.MB_Aviso(
  'Detalhamento espec�fico de Armamento n�o implementado! AVISE A DERMATEK');

end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_L101(XMLNohNivel3: IXMLNode;
  nItem: WideString);
begin
  Geral.MB_Aviso(
  'Detalhamento espec�fico de Combust�veis n�o implementado! AVISE A DERMATEK');

end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_M01(XMLNohNivel2: IXMLNode;
  nItem: WideString);
var
  I, P: Integer;
  AttrNode, XMLNohNivel3: IXMLNode;
  Campo: String;
  vTotTrib: Double;
begin
  if not (XMLNohNivel2.NodeType = ntElement) then
    Exit;
  if XMLNohNivel2.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo M01!');
  for I := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel2.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_M01);
    if P = -1 then
      AvisaItemDesconhecido('M01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel2.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
    begin
      XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
      if (XMLNohNivel3.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel3.NodeName);
        if Campo = LowerCase('vTotTrib') then
        begin
          vTotTrib := dmkPF.XMLValFlu(XMLNohNivel3); // else
          //
          if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsM, False, [
          //'FatID', 'FatNum', 'Empresa',
          'vTotTrib'], [
          'SeqArq', 'SeqNFe', 'nItem'], [
          //FatID, FatNum, Empresa,
          vTotTrib], [
          FSeqArq, FSeqNFe, nItem], False) then ;
        end else
        //
        if Campo = LowerCase('ICMS') then
          ImportaNFe_Grupo_N01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('IPI') then
          ImportaNFe_Grupo_O01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('II') then
          ImportaNFe_Grupo_P01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('PIS') then
          ImportaNFe_Grupo_Q01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('PISST') then
          ImportaNFe_Grupo_R01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('COFINS') then
          ImportaNFe_Grupo_S01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('COFINSST') then
          ImportaNFe_Grupo_T01(XMLNohNivel3, nItem) else
        if Campo = LowerCase('ISSQN') then
          ImportaNFe_Grupo_U01(XMLNohNivel3, nItem) else
        //
          AvisaItemDesconhecido('M01', Campo);
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_N01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, P: Integer;
  AttrNode, XMLNohNivel4, XMLNohNivel5: IXMLNode;
  Campo: String;
  //
  ICMS_Orig, ICMS_CST, ICMS_modBC, ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
  ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST, ICMS_pRedBCST, ICMS_vBCST,
  ICMS_pICMSST, ICMS_vICMSST, ICMS_CSOSN, ICMS_UFST, ICMS_pBCOp, ICMS_vBCSTRet,
  ICMS_vICMSSTRet, ICMS_motDesICMS, ICMS_pCredSN, ICMS_vCredICMSSN,
  ICMS_vICMSOp, ICMS_pDif, ICMS_vICMSDif, ICMS_vICMSDeson:
  WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo N01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_N01);
    if P = -1 then
      AvisaItemDesconhecido('N01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    if XMLNohNivel3.ChildNodes.Count <> 1 then
      Geral.MB_Aviso('Existem ' + IntToStr(XMLNohNivel3.ChildNodes.Count) +
      ' n�s de ICMS para o item ' + nItem + ' quando deveria ter 1 (um)!');
    if XMLNohNivel3.ChildNodes.Count > 0 then
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[0];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        ICMS_Orig := '0';  ICMS_CST := '0'; ICMS_modBC := '0';
        ICMS_pRedBC := '0';  ICMS_vBC := '0'; ICMS_pICMS := '0';
        ICMS_vICMS := '0';  ICMS_modBCST := '0'; ICMS_pMVAST := '0';
        ICMS_pRedBCST := '0';  ICMS_vBCST := '0'; ICMS_pICMSST := '0';
        ICMS_vICMSST := '0'; ICMS_CSOSN := '0'; ICMS_UFST := '';
        ICMS_pBCOp := '0';  ICMS_vBCSTRet := '0'; ICMS_vICMSSTRet := '0';
        ICMS_motDesICMS := '0';  ICMS_pCredSN := '0'; ICMS_vCredICMSSN := '0';
        ICMS_pDif := '0'; ICMS_vICMSDif := '0';
        ICMS_vICMSDeson := '0'; ICMS_vICMSOp := '0';
        //
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_N01);
        if P = -1 then
          AvisaItemDesconhecido('N01', Campo)
        else begin
          for I := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
          begin
            XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[I];
            if (XMLNohNivel4.NodeType = ntElement) then
            begin
              Campo := Lowercase(XMLNohNivel5.NodeName);
              P := AnsiIndexStr(Campo, FGrupo_Nxx);
              if P = -1 then
                AvisaItemDesconhecido('Nxx', Campo)
              else begin
                if Campo = LowerCase('orig') then
                  ICMS_orig := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('CST') then
                  ICMS_CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('modBC') then
                  ICMS_modBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pRedBC') then
                  ICMS_pRedBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vBC') then
                  ICMS_vBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pICMS') then
                  ICMS_pICMS := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vICMS') then
                  ICMS_vICMS := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('modBCST') then
                  ICMS_modBCST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pMVAST') then
                  ICMS_pMVAST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pRedBCST') then
                  ICMS_pRedBCST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vBCST') then
                  ICMS_vBCST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pICMSST') then
                  ICMS_pICMSST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vICMSST') then
                  ICMS_vICMSST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('CSOSN') then
                  ICMS_CSOSN := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('UFST') then
                  ICMS_UFST := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pBCOp') then
                  ICMS_pBCOp := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vBCSTRet') then
                  ICMS_VBCSTRet := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vICMSSTRet') then
                  ICMS_vICMSSTRet := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('motDesICMS') then
                  ICMS_motDesICMS := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('pCredSN') then
                  ICMS_pCredSN := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('vCredICMSSN') then
                  ICMS_vCredICMSSN := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('ICMS_vICMSOp') then
                  ICMS_vICMSOp := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('ICMS_pDif') then
                  ICMS_pDif := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('ICMS_vICMSDif') then
                  ICMS_vICMSDif := dmkPF.XMLValTxt(XMLNohNivel5) else
                if Campo = LowerCase('ICMS_vICMSDeson') then
                  ICMS_vICMSDeson := dmkPF.XMLValTxt(XMLNohNivel5) else
                //
                AvisaItemDesconhecido('Nxx', Campo);
              end;
            end;
          end;
        end;
        if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsN, False, [
        //'FatID', 'FatNum', 'Empresa',
        'ICMS_Orig', 'ICMS_CST', 'ICMS_modBC',
        'ICMS_pRedBC', 'ICMS_vBC', 'ICMS_pICMS',
        'ICMS_vICMS', 'ICMS_modBCST', 'ICMS_pMVAST',
        'ICMS_pRedBCST', 'ICMS_vBCST', 'ICMS_pICMSST',
        'ICMS_vICMSST', 'ICMS_CSOSN', 'ICMS_UFST',
        'ICMS_pBCOp', 'ICMS_vBCSTRet', 'ICMS_vICMSSTRet',
        'ICMS_motDesICMS', 'ICMS_pCredSN', 'ICMS_vCredICMSSN',
        'ICMS_pDif', 'ICMS_vICMSDif', 'ICMS_vICMSDeson',
        'ICMS_vICMSOp'], [
        'SeqArq', 'SeqNFe', 'nItem'], [
        //FatID, FatNum, Empresa,
        ICMS_Orig, ICMS_CST, ICMS_modBC,
        ICMS_pRedBC, ICMS_vBC, ICMS_pICMS,
        ICMS_vICMS, ICMS_modBCST, ICMS_pMVAST,
        ICMS_pRedBCST, ICMS_vBCST, ICMS_pICMSST,
        ICMS_vICMSST, ICMS_CSOSN, ICMS_UFST,
        ICMS_pBCOp, ICMS_vBCSTRet, ICMS_vICMSSTRet,
        ICMS_motDesICMS, ICMS_pCredSN, ICMS_vCredICMSSN,
        ICMS_pDif, ICMS_vICMSDif, ICMS_vICMSDeson,
        ICMS_vICMSOp], [
        FSeqArq, FSeqNFe, nItem], False) then ;
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_O01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, J, P: Integer;
  AttrNode, XMLNohNivel4, XMLNohNivel5: IXMLNode;
  Campo: String;
  //
  clEnq, CNPJProd,
  cSelo, qSelo, cEnq,
  CST, vBC, qUnid,
  vUnid, pIPI, vIPI: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo O01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_O01);
    if P = -1 then
      AvisaItemDesconhecido('O01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    clEnq := ''; CNPJProd := ''; cSelo := ''; qSelo := '0'; cEnq := '';
    CST := '0'; vBC := '0'; qUnid := '0'; vUnid := '0'; pIPI := '0'; vIPI := '0';
    //
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_O01);
        if P = -1 then
          AvisaItemDesconhecido('O01', Campo)
        else begin
          if Campo = LowerCase('clEnq') then
            clEnq := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('CNPJProd') then
            CNPJProd := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cSelo') then
            cSelo := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('qSelo') then
            qSelo := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cEnq') then
            cEnq := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('IPITrib') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_O07);
                  if P = -1 then
                    AvisaItemDesconhecido('O07', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vBC') then
                      vBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('pIPI') then
                      pIPI := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('qUnid') then
                      qUnid := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vUnid') then
                      vUnid := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vIPI') then
                      vIPI := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('O07', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('IPINT') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_O08);
                  if P = -1 then
                    AvisaItemDesconhecido('O08', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('O08', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
            AvisaItemDesconhecido('O01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsO, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'IPI_clEnq', 'IPI_CNPJProd',
    'IPI_cSelo', 'IPI_qSelo', 'IPI_cEnq',
    'IPI_CST', 'IPI_vBC', 'IPI_qUnid',
    'IPI_vUnid', 'IPI_pIPI', 'IPI_vIPI'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, clEnq, CNPJProd,
    cSelo, qSelo, cEnq,
    CST, vBC, qUnid,
    vUnid, pIPI, vIPI], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_P01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, P: Integer;
  AttrNode, XMLNohNivel4: IXMLNode;
  Campo: String;
  //
  vBC, vDespAdu, vII, vIOF: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo P01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_P01);
    if P = -1 then
      AvisaItemDesconhecido('P01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    vBC := '0';  vDespAdu := '0';  vII := '0';  vIOF := '0';
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_P01);
        if P = -1 then
          AvisaItemDesconhecido('P01', Campo)
        else begin
          if Campo = LowerCase('vBC') then
            vBC := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vDespAdu') then
            vDespAdu := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vII') then
            vII := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vIOF') then
            vIOF := dmkPF.XMLValTxt(XMLNohNivel4) else
          AvisaItemDesconhecido('P01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsP, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'II_vBC', 'II_vDespAdu', 'II_vII', 'II_vIOF'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, vBC, vDespAdu, vII, vIOF], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_Q01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, J, P: Integer;
  AttrNode, XMLNohNivel4, XMLNohNivel5: IXMLNode;
  Campo: String;
  //
    CST, vBC,
    pPIS, vPIS, qBCProd,
    vAliqProd, fatorBC: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo Q01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_Q01);
    if P = -1 then
      AvisaItemDesconhecido('Q01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    CST := '0'; vBC := '0'; pPIS := '0'; vPIS := '0'; qBCProd := '0';
    vAliqProd := '0'; fatorBC := '0';
    //
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_Q01);
        if P = -1 then
          AvisaItemDesconhecido('Q01', Campo)
        else begin
{
          if Campo = LowerCase('?') then
            ? := dmkPF.XMLValTxt(XMLNohNivel4) else
}
          if Campo = LowerCase('PISAliq') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_Q02);
                  if P = -1 then
                    AvisaItemDesconhecido('Q02', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vBC') then
                      vBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('pPIS') then
                      pPIS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vPIS') then
                      vPIS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('Q02', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('PISQtde') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_Q03);
                  if P = -1 then
                    AvisaItemDesconhecido('Q03', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('qBCProd') then
                      qBCProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vAliqProd') then
                      vAliqProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vPIS') then
                      vPIS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('Q03', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('PISNT') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_Q04);
                  if P = -1 then
                    AvisaItemDesconhecido('Q04', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('Q04', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('PISOutr') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_Q05);
                  if P = -1 then
                    AvisaItemDesconhecido('Q05', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vBC') then
                      vBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('pPIS') then
                      pPIS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('qBCProd') then
                      qBCProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vAliqProd') then
                      vAliqProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vPIS') then
                      vPIS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('Q05', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
            AvisaItemDesconhecido('Q01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsQ, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'PIS_CST', 'PIS_vBC',
    'PIS_pPIS', 'PIS_vPIS', 'PIS_qBCProd',
    'PIS_vAliqProd', 'PIS_fatorBC'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, CST, vBC,
    pPIS, vPIS, qBCProd,
    vAliqProd, fatorBC], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_R01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, P: Integer;
  AttrNode, XMLNohNivel4: IXMLNode;
  Campo: String;
  //
  vBC, pPIS,
  qBCProd, vAliqProd, vPIS,
  fatorBCST: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo R01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_R01);
    if P = -1 then
      AvisaItemDesconhecido('R01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    vBC := '0'; pPIS := '0'; qBCProd := '0'; vAliqProd := '0'; vPIS := '0';
    fatorBCST := '0';
    //
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_R01);
        if P = -1 then
          AvisaItemDesconhecido('R01', Campo)
        else begin
          if Campo = LowerCase('vBC') then
            vBC := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('pPIS') then
            pPIS := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('qBCProd') then
            qBCProd := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vAliqProd') then
            vAliqProd := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vPIS') then
            vPIS := dmkPF.XMLValTxt(XMLNohNivel4) else
          AvisaItemDesconhecido('R01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsR, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'PISST_vBC', 'PISST_pPIS',
    'PISST_qBCProd', 'PISST_vAliqProd', 'PISST_vPIS',
    'PISST_fatorBCST'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, vBC, pPIS,
    qBCProd, vAliqProd, vPIS,
    fatorBCST], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_S01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, J, P: Integer;
  AttrNode, XMLNohNivel4, XMLNohNivel5: IXMLNode;
  Campo: String;
  //
    CST, vBC,
    pCOFINS, vCOFINS, qBCProd,
    vAliqProd, fatorBC: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo S01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_S01);
    if P = -1 then
      AvisaItemDesconhecido('S01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    CST := '0'; vBC := '0'; pCOFINS := '0'; vCOFINS := '0'; qBCProd := '0';
    vAliqProd := '0'; fatorBC := '0';
    //
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_S01);
        if P = -1 then
          AvisaItemDesconhecido('S01', Campo)
        else begin
{
          if Campo = LowerCase('?') then
            ? := dmkPF.XMLValTxt(XMLNohNivel4) else
}
          if Campo = LowerCase('COFINSAliq') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_S02);
                  if P = -1 then
                    AvisaItemDesconhecido('S02', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vBC') then
                      vBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('pCOFINS') then
                      pCOFINS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vCOFINS') then
                      vCOFINS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('S02', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('COFINSQtde') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_S03);
                  if P = -1 then
                    AvisaItemDesconhecido('S03', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('qBCProd') then
                      qBCProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vAliqProd') then
                      vAliqProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vCOFINS') then
                      vCOFINS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('S03', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('COFINSNT') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_S04);
                  if P = -1 then
                    AvisaItemDesconhecido('S04', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('S04', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
          if Campo = LowerCase('COFINSOutr') then
          begin
            if XMLNohNivel4.ChildNodes.Count > 0 then
            begin
              for J := 0 to XMLNohNivel4.ChildNodes.Count - 1 do
              begin
                XMLNohNivel5 := XMLNohNivel4.ChildNodes.Nodes[J];
                if (XMLNohNivel5.NodeType = ntElement) then
                begin
                  Campo := Lowercase(XMLNohNivel5.NodeName);
                  P := AnsiIndexStr(Campo, FGrupo_S05);
                  if P = -1 then
                    AvisaItemDesconhecido('S05', Campo)
                  else begin
                    if Campo = LowerCase('CST') then
                      CST := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vBC') then
                      vBC := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('pCOFINS') then
                      pCOFINS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('qBCProd') then
                      qBCProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vAliqProd') then
                      vAliqProd := dmkPF.XMLValTxt(XMLNohNivel5) else
                    if Campo = LowerCase('vCOFINS') then
                      vCOFINS := dmkPF.XMLValTxt(XMLNohNivel5) else
                    AvisaItemDesconhecido('S05', XMLNohNivel5.NodeName);
                  end;
                end;
              end;
            end;
          end else
            AvisaItemDesconhecido('S01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsS, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'COFINS_CST', 'COFINS_vBC',
    'COFINS_pCOFINS', 'COFINS_vCOFINS', 'COFINS_qBCProd',
    'COFINS_vAliqProd', 'COFINS_fatorBC'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, CST, vBC,
    pCOFINS, vCOFINS, qBCProd,
    vAliqProd, fatorBC], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_T01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, P: Integer;
  AttrNode, XMLNohNivel4: IXMLNode;
  Campo: String;
  //
  vBC, pCOFINS,
  qBCProd, vAliqProd, vCOFINS,
  fatorBCST: WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo T01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_T01);
    if P = -1 then
      AvisaItemDesconhecido('T01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    vBC := '0'; pCOFINS := '0'; qBCProd := '0'; vAliqProd := '0'; vCOFINS := '0';
    fatorBCST := '0';
    //
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_T01);
        if P = -1 then
          AvisaItemDesconhecido('T01', Campo)
        else begin
          if Campo = LowerCase('vBC') then
            vBC := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('pCOFINS') then
            pCOFINS := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('qBCProd') then
            qBCProd := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vAliqProd') then
            vAliqProd := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vCOFINS') then
            vCOFINS := dmkPF.XMLValTxt(XMLNohNivel4) else
          AvisaItemDesconhecido('T01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsT, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'COFINSST_vBC', 'COFINSST_pCOFINS',
    'COFINSST_qBCProd', 'COFINSST_vAliqProd', 'COFINSST_vCOFINS',
    'COFINSST_fatorBCST'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, vBC, pCOFINS,
    qBCProd, vAliqProd, vCOFINS,
    fatorBCST], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_U01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  I, P: Integer;
  AttrNode, XMLNohNivel4: IXMLNode;
  Campo: String;
  //
  vBC, vAliq, vISSQN, cMunFG, cListServ, cSitTrib, ISSQN_vDeducao, ISSQN_vOutro,
  ISSQN_vDescIncond, ISSQN_vDescCond, ISSQN_vISSRet, ISSQN_indISS,
  ISSQN_cServico, ISSQN_cMun, ISSQN_cPais, ISSQN_nProcesso, ISSQN_indIncentivo:
  WideString;
begin
  if not (XMLNohNivel3.NodeType = ntElement) then
    Exit;
  if XMLNohNivel3.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo U01!');
  for I := 0 to XMLNohNivel3.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel3.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_U01);
    if P = -1 then
      AvisaItemDesconhecido('U01', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel3.HasChildNodes then
  begin
    vBC := '0'; vAliq := '0'; vISSQN := '0'; cMunFG := '0'; cListServ := '0';
    cSitTrib := '0'; ISSQN_vDeducao := '0'; ISSQN_vOutro := '0';
    ISSQN_vDescIncond := '0'; ISSQN_vDescCond := '0'; ISSQN_vISSRet := '0';
    ISSQN_indISS := '0'; ISSQN_cServico := ''; ISSQN_cMun := '0';
    ISSQN_cPais := '0'; ISSQN_nProcesso := '0'; ISSQN_indIncentivo := '0';
    //
    for I := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
    begin
      XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[I];
      if (XMLNohNivel4.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel4.NodeName);
        P := AnsiIndexStr(Campo, FGrupo_U01);
        if P = -1 then
          AvisaItemDesconhecido('U01', Campo)
        else begin
          if Campo = LowerCase('vBC') then
            vBC := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vAliq') then
            vAliq := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vISSQN') then
            vISSQN := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cMunFG') then
            cMunFG := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cListServ') then
            cListServ := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cSitTrib') then
            cSitTrib := dmkPF.XMLValTxt(XMLNohNivel4) else
          //
          if Campo = LowerCase('vDeducao') then
            ISSQN_vDeducao := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vOutro') then
            ISSQN_vOutro := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vDescIncond') then
            ISSQN_vDescIncond := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vDescCond') then
            ISSQN_vDescCond := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('vISSRet') then
            ISSQN_vISSRet := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('indISS') then
            ISSQN_indISS := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cServico') then
            ISSQN_cServico := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cMun') then
            ISSQN_cMun := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('cPais') then
            ISSQN_cPais := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('nProcesso') then
            ISSQN_nProcesso := dmkPF.XMLValTxt(XMLNohNivel4) else
          if Campo = LowerCase('indIncentivo') then
            ISSQN_indIncentivo := dmkPF.XMLValTxt(XMLNohNivel4) else

          AvisaItemDesconhecido('U01', XMLNohNivel4.NodeName);
        end;
      end;
    end;
    if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FItsU, False, [
    //'FatID', 'FatNum', 'Empresa',
    'nItem', 'ISSQN_vBC', 'ISSQN_vAliq',
    'ISSQN_vISSQN', 'ISSQN_cMunFG', 'ISSQN_cListServ',
    'ISSQN_cSitTrib'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    nItem, vBC, vAliq,
    vISSQN, cMunFG, cListServ,
    cSitTrib], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_V01(XMLNohNivel3: IXMLNode;
  nItem: WideString);
var
  InfAdProd: WideString;
begin
  InfAdProd := XMLNohNivel3.NodeValue;
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsV, False, [
  //'FatID', 'FatNum', 'Empresa',
  'nItem', 'InfAdProd'], [
  'SeqArq', 'SeqNFe'], [
  //FatID, FatNum, Empresa,
  nItem, InfAdProd], [
  FSeqArq, FSeqNFe], False) then ;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_W01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo W01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_W01);
    if P = -1 then
      AvisaItemDesconhecido('W01', AttrNode.NodeName);
{
    if Campo = LowerCase('nItem') then
      nItem := AttrNode.NodeValue else
    AvisaItemDesconhecido('W01', AttrNode.NodeName);
}
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('ICMSTot') then
          ImportaNFe_Grupo_W02(XMLNohNivel2) else
        if Campo = LowerCase('ISSQNTot') then
          ImportaNFe_Grupo_W17(XMLNohNivel2) else
        if Campo = LowerCase('retTrib') then
          ImportaNFe_Grupo_W23(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('W01', Campo);
      end;
    end;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_W02(XMLNohNivel2: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST, ICMSTot_vST, ICMSTot_vProd,
  ICMSTot_vFrete, ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII, ICMSTot_vIPI,
  ICMSTot_vPIS, ICMSTot_vCOFINS, ICMSTot_vOutro, ICMSTot_vNF,
  ICMSTot_vICMSDeson, ICMSTot_vTotTrib, ICMSTot_vFCPUFDest,
  ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet: Double;
begin
  if not (XMLNohNivel2.NodeType = ntElement) then
    Exit;
  if XMLNohNivel2.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo W02!');
  for I := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel2.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_W02);
    if P = -1 then
      AvisaItemDesconhecido('W02', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel2.HasChildNodes then
  begin
    ICMSTot_vBC := 0; ICMSTot_vICMS := 0; ICMSTot_vBCST := 0;
    ICMSTot_vST := 0; ICMSTot_vProd := 0; ICMSTot_vFrete := 0;
    ICMSTot_vSeg := 0; ICMSTot_vDesc := 0; ICMSTot_vII := 0;
    ICMSTot_vIPI := 0; ICMSTot_vPIS := 0; ICMSTot_vCOFINS := 0;
    ICMSTot_vOutro := 0; ICMSTot_vNF := 0; ICMSTot_vICMSDeson := 0;
    ICMSTot_vTotTrib := 0; ICMSTot_vFCPUFDest := 0; ICMSTot_vICMSUFDest := 0;
    ICMSTot_vICMSUFRemet := 0;
    for I := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
    begin
      XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
      if (XMLNohNivel3.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel3.NodeName);
        if Campo = LowerCase('vBC') then
          ICMSTot_vBC := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vICMS') then
          ICMSTot_vICMS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vBCST') then
          ICMSTot_vBCST := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vST') then
          ICMSTot_vST := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vProd') then
          ICMSTot_vProd := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vFrete') then
          ICMSTot_vFrete := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vSeg') then
          ICMSTot_vSeg := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vDesc') then
          ICMSTot_vDesc := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vII') then
          ICMSTot_vII := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vIPI') then
          ICMSTot_vIPI := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vPIS') then
          ICMSTot_vPIS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vCOFINS') then
          ICMSTot_vCOFINS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vOutro') then
          ICMSTot_vOutro := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vNF') then
          ICMSTot_vNF := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vICMSDeson') then
          ICMSTot_vICMSDeson := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vTotTrib') then
          ICMSTot_vTotTrib := dmkPF.XMLValFlu(XMLNohNivel3) else

        if Campo = LowerCase('vFCPUFDest') then
          ICMSTot_vFCPUFDest := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vICMSUFDest') then
          ICMSTot_vICMSUFDest := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vICMSUFRemet') then
          ICMSTot_vICMSUFRemet := dmkPF.XMLValFlu(XMLNohNivel3) else
        //
        AvisaItemDesconhecido('W02', Campo);
(*
    Qry.SQL.Add('  ICMSTot_vBC                    double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vICMS                  double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vICMSDeson             double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vBCST                  double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vST                    double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vProd                  double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vFrete                 double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vSeg                   double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vDesc                  double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vII                    double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vIPI                   double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vPIS                   double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vCOFINS                double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vOutro                 double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vNF                    double(15,2)           DEFAULT "0.00"      ,');


    Qry.SQL.Add('  ICMSTot_vFCPUFDest             double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vICMSUFDest            double(15,2)           DEFAULT "0.00"      ,');
    Qry.SQL.Add('  ICMSTot_vICMSUFRemet           double(15,2)           DEFAULT "0.00"      ,');
*)
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabW02, False, [
    //'FatID', 'FatNum', 'Empresa',
    'ICMSTot_vBC', 'ICMSTot_vICMS', 'ICMSTot_vBCST',
    'ICMSTot_vST', 'ICMSTot_vProd', 'ICMSTot_vFrete',
    'ICMSTot_vSeg', 'ICMSTot_vDesc', 'ICMSTot_vII',
    'ICMSTot_vIPI', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
    'ICMSTot_vOutro', 'ICMSTot_vNF',
    'ICMSTot_vICMSDeson', 'ICMSTot_vTotTrib',
    'ICMSTot_vFCPUFDest', 'ICMSTot_vICMSUFDest', 'ICMSTot_vICMSUFRemet'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    ICMSTot_vBC, ICMSTot_vICMS, ICMSTot_vBCST,
    ICMSTot_vST, ICMSTot_vProd, ICMSTot_vFrete,
    ICMSTot_vSeg, ICMSTot_vDesc, ICMSTot_vII,
    ICMSTot_vIPI, ICMSTot_vPIS, ICMSTot_vCOFINS,
    ICMSTot_vOutro, ICMSTot_vNF,
    ICMSTot_vICMSDeson, ICMSTot_vTotTrib,
    ICMSTot_vFCPUFDest, ICMSTot_vICMSUFDest, ICMSTot_vICMSUFRemet], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_W17(XMLNohNivel2: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  ISSQNtot_vServ, ISSQNtot_vBC, ISSQNtot_vISS, ISSQNtot_vPIS, ISSQNtot_vCOFINS:
  Double;
begin
  if not (XMLNohNivel2.NodeType = ntElement) then
    Exit;
  if XMLNohNivel2.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo W17!');
  for I := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel2.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_W17);
    if P = -1 then
      AvisaItemDesconhecido('W17', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel2.HasChildNodes then
  begin
    ISSQNtot_vServ := 0; ISSQNtot_vBC := 0; ISSQNtot_vISS := 0;
    ISSQNtot_vPIS := 0; ISSQNtot_vCOFINS := 0;
    for I := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
    begin
      XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
      if (XMLNohNivel3.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel3.NodeName);
        if Campo = LowerCase('vServ') then
          ISSQNtot_vServ := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vBC') then
          ISSQNtot_vBC := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vISS') then
          ISSQNtot_vISS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vPIS') then
          ISSQNtot_vPIS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vCOFINS') then
          ISSQNtot_vCOFINS := dmkPF.XMLValFlu(XMLNohNivel3) else
        //
        AvisaItemDesconhecido('W17', Campo);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabW17, False, [
    //'FatID', 'FatNum', 'Empresa',
    'ISSQNtot_vServ', 'ISSQNtot_vBC', 'ISSQNtot_vISS',
    'ISSQNtot_vPIS', 'ISSQNtot_vCOFINS'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    ISSQNtot_vServ, ISSQNtot_vBC, ISSQNtot_vISS,
    ISSQNtot_vPIS, ISSQNtot_vCOFINS], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_W23(XMLNohNivel2: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
  RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
  RetTrib_vRetPrev: Double;
begin
  if not (XMLNohNivel2.NodeType = ntElement) then
    Exit;
  if XMLNohNivel2.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo W23!');
  for I := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel2.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_W23);
    if P = -1 then
      AvisaItemDesconhecido('W23', AttrNode.NodeName)
  end;
  // Subn�s
  if XMLNohNivel2.HasChildNodes then
  begin
    RetTrib_vRetPIS := 0; RetTrib_vRetCOFINS := 0; RetTrib_vRetCSLL := 0;
    RetTrib_vBCIRRF := 0; RetTrib_vIRRF := 0; RetTrib_vBCRetPrev := 0;
    RetTrib_vRetPrev := 0;
    //
    for I := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
    begin
      XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[I];
      if (XMLNohNivel3.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel3.NodeName);
        if Campo = LowerCase('vRetPIS') then
          RetTrib_vRetPIS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vRetCOFINS') then
          RetTrib_vRetCOFINS := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vRetCSLL') then
          RetTrib_vRetCSLL := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vBCIRRF') then
          RetTrib_vBCIRRF := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vIRRF') then
          RetTrib_vIRRF := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vBCRetPrev') then
          RetTrib_vBCRetPrev := dmkPF.XMLValFlu(XMLNohNivel3) else
        if Campo = LowerCase('vRetPrev') then
          RetTrib_vRetPrev := dmkPF.XMLValFlu(XMLNohNivel3) else
        //
        AvisaItemDesconhecido('W23', Campo);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabW23, False, [
    //'FatID', 'FatNum', 'Empresa',
    'RetTrib_vRetPIS', 'RetTrib_vRetCOFINS', 'RetTrib_vRetCSLL',
    'RetTrib_vBCIRRF', 'RetTrib_vIRRF', 'RetTrib_vBCRetPrev',
    'RetTrib_vRetPrev'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    RetTrib_vRetPIS, RetTrib_vRetCOFINS, RetTrib_vRetCSLL,
    RetTrib_vBCIRRF, RetTrib_vIRRF, RetTrib_vBCRetPrev,
    RetTrib_vRetPrev], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_X01(XMLNohNivel1: IXMLNode);
const
  xFant = ''; xLgr = ''; nro = ''; xBairro = ''; cMun = 0; CEP = 0; cPais = 0;
  fone = ''; IEST = ''; IM = ''; CNAE = ''; xCpl = '';
  xPais = '';
var
  I, J, K, P: Integer;
  AttrNode, XMLNohNivel2, XMLNohNivel3, XMLNohNivel4: IXMLNode;
  Campo: String;
  //
  ModFrete: Integer;
  Transporta_CNPJ, Transporta_CPF, Transporta_XNome, Transporta_IE,
  Transporta_XEnder, Transporta_XMun, Transporta_UF: String;
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet: Double;
  RetTransp_CFOP, RetTransp_CMunFG, VeicTransp_Placa,
  VeicTransp_UF, VeicTransp_RNTC, Vagao, Balsa, RetTransp_xMunFG: String;
  //
  placa, UF, RNTC, qVol, esp, marca, nVol, pesoL, pesoB, nlacre, xModFrete:
  WideString;
  //reboque, vol, lacres,
  Controle, Conta, CodInfoTrsp: Integer;
  NomeTrsp: WideString;
begin
  //reboque := 0;
  //vol     := 0;
  //lacres  := 0;
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo X01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_X01);
    if P = -1 then
      AvisaItemDesconhecido('X01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    ModFrete := 0;
    Transporta_CNPJ := ''; Transporta_CPF := ''; Transporta_XNome := '';
    Transporta_IE := ''; Transporta_XEnder := ''; Transporta_XMun := '';
    Transporta_UF := '';
    RetTransp_vServ := 0; RetTransp_vBCRet := 0; RetTransp_PICMSRet := 0;
    RetTransp_vICMSRet := 0;
    RetTransp_CFOP := ''; RetTransp_CMunFG := ''; VeicTransp_Placa := '';
    VeicTransp_UF := ''; VeicTransp_RNTC := ''; Vagao := ''; Balsa := '';
    RetTransp_xMunFG := '';
    //
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('modfrete') then
          modFrete := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('transporta') then
        begin
          //ImportaNFe_Grupo_X03(XMLNohNivel2, nItem) else
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_X03);
                if P = -1 then
                  AvisaItemDesconhecido('X03', Campo)
                else begin
                  if Campo = LowerCase('CNPJ') then
                    transporta_CNPJ := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('CPF') then
                    transporta_CPF := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('xNome') then
                    transporta_xNome := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('IE') then
                    transporta_IE := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('xEnder') then
                    transporta_xEnder := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('xMun') then
                    transporta_xMun := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('UF') then
                    transporta_UF := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('X03', XMLNohNivel3.NodeName);
                end;
              end;
            end;
          end;
          if not DefineEntidade(
            XMLNohNivel2, transporta_CNPJ, transporta_CPF, CodInfoTrsp, NomeTrsp) then
          CodInfoTrsp := CadastraEntidade(entNFe_transp, transporta_CNPJ, transporta_CPF,
            transporta_xNome, xFant, transporta_IE, xLgr, nro, xBairro,
            cMun, transporta_xMun, transporta_UF, CEP, cPais, xPais,
            fone, IEST, IM, CNAE, xCpl, transporta_xEnder);
        end else
        if Campo = LowerCase('rettransp') then
        begin
          //ImportaNFe_Grupo_X11(XMLNohNivel2, nItem) else
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_X11);
                if P = -1 then
                  AvisaItemDesconhecido('X11', Campo)
                else begin
                  if Campo = LowerCase('vServ') then
                    retTransp_vServ := dmkPF.XMLValFlu(XMLNohNivel3) else
                  if Campo = LowerCase('vBCRet') then
                    retTransp_vBCRet := dmkPF.XMLValFlu(XMLNohNivel3) else
                  if Campo = LowerCase('pICMSRet') then
                    retTransp_pICMSRet := dmkPF.XMLValFlu(XMLNohNivel3) else
                  if Campo = LowerCase('vICMSRet') then
                    retTransp_vICMSRet := dmkPF.XMLValFlu(XMLNohNivel3) else
                  if Campo = LowerCase('CFOP') then
                    retTransp_CFOP := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('cMunFG') then
                    retTransp_cMunFG := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('X11', XMLNohNivel3.NodeName);
                end;
              end;
            end;
          end;
        end else
        if Campo = LowerCase('veictransp') then
        begin
          //ImportaNFe_Grupo_X18(XMLNohNivel2, nItem) else
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_X18);
                if P = -1 then
                  AvisaItemDesconhecido('X18', Campo)
                else begin
                  if Campo = LowerCase('placa') then
                    veicTransp_placa := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('UF') then
                    veicTransp_UF := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('RNTC') then
                    veicTransp_RNTC := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('X18', XMLNohNivel3.NodeName);
                end;
              end;
            end;
          end;
        end else
        if Campo = LowerCase('reboque') then
        begin
          //ImportaNFe_Grupo_X22(XMLNohNivel2, nItem) else
          placa := '';
          UF    := '';
          RNTC  := '';
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            //reboque := reboque + 1;
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_X22);
                if P = -1 then
                  AvisaItemDesconhecido('X22', Campo)
                else begin
                  if Campo = LowerCase('placa') then
                    placa := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('UF') then
                    UF := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('RNTC') then
                    RNTC := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('X22', XMLNohNivel3.NodeName);
                end;
              end;
            end;
            FCtrlX22 := FCtrlX22 + 1;
            Controle := FCtrlX22;
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsX22, False, [
            //'FatID', 'FatNum', 'Empresa',
{
            'Controle',
            'placa', 'UF', 'RNTC'], [
            'SeqArq', 'SeqNFe'], [
            //FatID, FatNum, Empresa,
            reboque,
            placa, UF, RNTC], [
            FSeqArq, FSeqNFe, Controle], False) then ;
}
            'placa', 'UF', 'RNTC'], [
            'SeqArq', 'SeqNFe', 'Controle'], [
            //FatID, FatNum, Empresa,
            placa, UF, RNTC], [
            FSeqArq, FSeqNFe, Controle], False) then ;
          end;
        end else
        if Campo = LowerCase('vagao') then
          vagao := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('balsa') then
          balsa := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('vol') then
        begin
          //ImportaNFe_Grupo_X26(XMLNohNivel2, nItem) else
          qVol   := '0';
          esp    := '';
          marca  := '';
          nVol   := '';
          pesoL  := '0';
          pesoB  := '0';
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            FCtrlX26 := FCtrlX26 + 1;
            Controle := FCtrlX26;
            //vol := vol + 1;
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_X26);
                if P = -1 then
                  AvisaItemDesconhecido('X26', Campo)
                else begin
                  if Campo = LowerCase('qVol') then
                    qVol := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('esp') then
                    esp := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('marca') then
                    marca := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('nVol') then
                    nVol := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('pesoL') then
                    pesoL := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('pesoB') then
                    pesoB := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  if Campo = LowerCase('lacres') then
                  begin
                    //lacres := lacres + 1;
                    //ImportaNFe_Grupo_X33(XMLNohNivel3, nItem) else
                    nLacre := '';
                    if XMLNohNivel3.ChildNodes.Count > 0 then
                    begin
                      for K := 0 to XMLNohNivel3.ChildNodes.Count - 1 do
                      begin
                        XMLNohNivel4 := XMLNohNivel3.ChildNodes.Nodes[K];
                        if (XMLNohNivel4.NodeType = ntElement) then
                        begin
                          Campo := Lowercase(XMLNohNivel4.NodeName);
                          P := AnsiIndexStr(Campo, FGrupo_X33);
                          if P = -1 then
                            AvisaItemDesconhecido('X33', Campo)
                          else begin
                            if Campo = LowerCase('nLacre') then
                              qVol := dmkPF.XMLValTxt(XMLNohNivel4) else
                            //
                            AvisaItemDesconhecido('X33', XMLNohNivel4.NodeName);
                          end;
                        end;
                      end;
                      FCtrlX33 := FCtrlX33 + 1;
                      Conta    := FCtrlX33;
                      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsX33, False, [
                      //'FatID', 'FatNum', 'Empresa',
{
                      'Controle', 'Conta', 'nLacre'], [
                      'SeqArq', 'SeqNFe'], [
                      //FatID, FatNum, Empresa,
                      vol, lacres, nLacre], [
                      FSeqArq, FSeqNFe], False) then ;
}
                      'nLacre'], [
                      'SeqArq', 'SeqNFe', 'Controle', 'Conta'], [
                      //FatID, FatNum, Empresa,
                      nLacre], [
                      FSeqArq, FSeqNFe, Controle, Conta], False) then ;
                    end;
                  end else
                  AvisaItemDesconhecido('X26', XMLNohNivel3.NodeName);
                end;
              end;
            end;
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsX26, False, [
            //'FatID', 'FatNum', 'Empresa',
{
            'Controle',
            'qVol', 'esp', 'marca', 'nVol', 'pesoL', 'pesoB'], [
            'SeqArq', 'SeqNFe'], [
            //FatID, FatNum, Empresa,
            vol,
            qVol, esp, marca, nVol, pesoL, pesoB], [
            FSeqArq, FSeqNFe], False) then ;
}
            'qVol', 'esp', 'marca',
            'nVol', 'pesoL', 'pesoB'], [
            'SeqArq', 'SeqNFe', 'Controle'], [
            //FatID, FatNum, Empresa,
            qVol, esp, marca,
            nVol, pesoL, pesoB], [
            FSeqArq, FSeqNFe, Controle], False) then ;
          end;
        end else
        //
        AvisaItemDesconhecido('X01', Campo);
      end;
    end;
    xModFrete := UnNFe_PF.TextoDeCodigoNFe(nfeCTModFrete, ModFrete);
    if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Geral.IMV(RetTransp_CMunFG), []) then
      RetTransp_xMunFG := DmNFe_0000.QrDTB_MuniciNome.Value;
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabX01, False, [
    //'FatID', 'FatNum', 'Empresa',
    'ModFrete', 'Transporta_CNPJ', 'Transporta_CPF',
    'Transporta_XNome', 'Transporta_IE', 'Transporta_XEnder',
    'Transporta_XMun', 'Transporta_UF', 'RetTransp_vServ',
    'RetTransp_vBCRet', 'RetTransp_PICMSRet', 'RetTransp_vICMSRet',
    'RetTransp_CFOP', 'RetTransp_CMunFG', 'VeicTransp_Placa',
    'VeicTransp_UF', 'VeicTransp_RNTC', 'Vagao',
    'Balsa', 'xModFrete', 'RetTransp_xMunFG',
    'CodInfoTrsp', 'NomeTrsp'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    ModFrete, Transporta_CNPJ, Transporta_CPF,
    Transporta_XNome, Transporta_IE, Transporta_XEnder,
    Transporta_XMun, Transporta_UF, RetTransp_vServ,
    RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet,
    RetTransp_CFOP, RetTransp_CMunFG, VeicTransp_Placa,
    VeicTransp_UF, VeicTransp_RNTC, Vagao,
    Balsa, xModFrete, RetTransp_xMunFG,
    CodInfoTrsp, NomeTrsp], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_Y01(XMLNohNivel1: IXMLNode);
var
  I, J, P, Controle: Integer;
  AttrNode, XMLNohNivel2, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  nDup, dVenc, vDup: WideString;
  cobr_fat_nFat: WideString;
  cobr_fat_vOrig, cobr_fat_vDesc, cobr_fat_vLiq: Double;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo Y01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_Y01);
    if P = -1 then
      AvisaItemDesconhecido('Y01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    cobr_fat_nFat := '';
    cobr_fat_vOrig := 0; cobr_fat_vDesc := 0; cobr_fat_vLiq := 0;
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      //
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        //if Campo = LowerCase('?') then
          //? := dmkPF.XMLValInt(XMLNohNivel2) else
        if Campo = LowerCase('fat') then
        begin
          //ImportaNFe_Grupo_Y02(XMLNohNivel2, nItem) else
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_Y02);
                if P = -1 then
                  AvisaItemDesconhecido('Y02', Campo)
                else begin
                  if Campo = LowerCase('nFat') then
                    cobr_fat_nFat := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('vOrig') then
                    cobr_fat_vOrig := dmkPF.XMLValFlu(XMLNohNivel3) else
                  if Campo = LowerCase('vDesc') then
                    cobr_fat_vDesc := dmkPF.XMLValFlu(XMLNohNivel3) else
                  if Campo = LowerCase('vLiq') then
                    cobr_fat_vLiq := dmkPF.XMLValFlu(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('Y02', XMLNohNivel3.NodeName);
                end;
              end;
            end;
          end;
        end else
        if Campo = LowerCase('dup') then
        begin
          //ImportaNFe_Grupo_Y07(XMLNohNivel2, nItem) else
          nDup  := '';
          dVenc := '';
          vDup  := '';
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_Y07);
                if P = -1 then
                  AvisaItemDesconhecido('Y07', Campo)
                else begin
                  if Campo = LowerCase('nDup') then
                    nDup := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('dVenc') then
                    dVenc := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('vDup') then
                    vDup := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('Y07', XMLNohNivel3.NodeName);
                end;
              end;
            end;
            FCtrlY07 := FCtrlY07 + 1;
            Controle := FCtrlY07;
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsY07, False, [
{
            //'FatID', 'FatNum', 'Empresa',
            'Controle',
            'nDup', 'dVenc', 'vDup'], [
            'SeqArq', 'SeqNFe'], [
            //FatID, FatNum, Empresa, Controle
            nDup, dVenc, vDup], [
            FSeqArq, FSeqNFe], False) then ;
}
            //'FatID', 'FatNum', 'Empresa',
            'nDup', 'dVenc', 'vDup'], [
            'SeqArq', 'SeqNFe', 'Controle'], [
            //FatID, FatNum, Empresa,
            nDup, dVenc, vDup], [
            FSeqArq, FSeqNFe, Controle], False) then ;
          end;
        end else
        //
        AvisaItemDesconhecido('Y01', Campo);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabY01, False, [
    //'FatID', 'FatNum', 'Empresa',
    'Cobr_Fat_nFat', 'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc',
    'Cobr_Fat_vLiq'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    Cobr_Fat_nFat, Cobr_Fat_vOrig, Cobr_Fat_vDesc,
    Cobr_Fat_vLiq], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_Z01(XMLNohNivel1: IXMLNode);
var
  I, J, A, P, Controle: Integer;
  AttrNode, XMLNohNivel2, XMLNohNivel3: IXMLNode;
  Campo: String;
  //
  xCampo, xTexto, nProc, indProc: WideString;
  InfAdic_infAdFisco, InfAdic_infCpl: WideString;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo Z01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_Z01);
    if P = -1 then
      AvisaItemDesconhecido('Z01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    InfAdic_infAdFisco := ''; InfAdic_infCpl := '';
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('infAdFisco') then
          InfAdic_infAdFisco := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('infCpl') then
          InfAdic_infCpl := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('obsCont') then
        begin
          //ImportaNFe_Grupo_Z04(XMLNohNivel2, nItem) else
          xCampo := '';
          xTexto := '';
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for A := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
            begin
              AttrNode := XMLNohNivel2.AttributeNodes.Nodes[A];
              Campo := Lowercase(AttrNode.NodeName);
              if Campo = LowerCase('xCampo') then
                xCampo := AttrNode.NodeValue else
              AvisaItemDesconhecido('Z04', AttrNode.NodeName);
            end;
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_Z04);
                if P = -1 then
                  AvisaItemDesconhecido('Z04', Campo)
                else begin
                  if Campo = LowerCase('xTexto') then
                    xTexto := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('Z04', XMLNohNivel3.NodeName);
                end;
              end;
            end;
            FCtrlZ04 := FCtrlZ04 + 1;
            Controle := FCtrlZ04;
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsZ04, False, [
            //'FatID', 'FatNum', 'Empresa', 'Controle',
            'xCampo', 'xTexto'], [
            'SeqArq', 'SeqNFe', 'Controle'], [
            //FatID, FatNum, Empresa, Controle,
            xCampo, xTexto], [
            FSeqArq, FSeqNFe, Controle], False) then ;
          end;
        end else
        if Campo = LowerCase('obsFisco') then
        begin
          //ImportaNFe_Grupo_Z07(XMLNohNivel2, nItem) else
          xCampo := '';
          xTexto := '';
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for A := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
            begin
              AttrNode := XMLNohNivel2.AttributeNodes.Nodes[A];
              Campo := Lowercase(AttrNode.NodeName);
              if Campo = LowerCase('xCampo') then
                xCampo := AttrNode.NodeValue else
              AvisaItemDesconhecido('Z07', AttrNode.NodeName);
            end;
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_Z07);
                if P = -1 then
                  AvisaItemDesconhecido('Z07', Campo)
                else begin
                  if Campo = LowerCase('xTexto') then
                    xTexto := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('Z07', XMLNohNivel3.NodeName);
                end;
              end;
            end;
            FCtrlZ07 := FCtrlZ07 + 1;
            Controle := FCtrlZ07;
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsZ07, False, [
            //'FatID', 'FatNum', 'Empresa',
            'xCampo', 'xTexto'], [
            'SeqArq', 'SeqNFe', 'Controle'], [
            //FatID, FatNum, Empresa,
            xCampo, xTexto], [
            FSeqArq, FSeqNFe, Controle], False) then ;
          end;
        end else
        if Campo = LowerCase('procRef') then
        begin
          //ImportaNFe_Grupo_Z10(XMLNohNivel2, nItem) else
          nProc   := '';
          indProc := '';
          if XMLNohNivel2.ChildNodes.Count > 0 then
          begin
            for A := 0 to XMLNohNivel2.AttributeNodes.Count - 1 do
            begin
              AttrNode := XMLNohNivel2.AttributeNodes.Nodes[A];
              Campo := Lowercase(AttrNode.NodeName);
              //if Campo = LowerCase('?') then
                //? := AttrNode.NodeValue else
              AvisaItemDesconhecido('Z10', AttrNode.NodeName);
            end;
            for J := 0 to XMLNohNivel2.ChildNodes.Count - 1 do
            begin
              XMLNohNivel3 := XMLNohNivel2.ChildNodes.Nodes[J];
              if (XMLNohNivel3.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel3.NodeName);
                P := AnsiIndexStr(Campo, FGrupo_Z10);
                if P = -1 then
                  AvisaItemDesconhecido('Z10', Campo)
                else begin
                  if Campo = LowerCase('nProc') then
                    nProc := dmkPF.XMLValTxt(XMLNohNivel3) else
                  if Campo = LowerCase('indProc') then
                    indProc := dmkPF.XMLValTxt(XMLNohNivel3) else
                  //
                  AvisaItemDesconhecido('Z10', XMLNohNivel3.NodeName);
                end;
              end;
            end;
            FCtrlZ10 := FCtrlZ10 + 1;
            Controle := FCtrlZ10;
            if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FItsZ10, False, [
            //'FatID', 'FatNum', 'Empresa',
            'nProc', 'indProc'], [
            'SeqArq', 'SeqNFe', 'Controle'], [
            //FatID, FatNum, Empresa,
            nProc, indProc], [
            FSeqArq, FSeqNFe, Controle], False) then ;
          end;
        end else
        //
        AvisaItemDesconhecido('Z01', Campo);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabZ01, False, [
    //'FatID', 'FatNum', 'Empresa',
    'InfAdic_InfAdFisco', 'InfAdic_InfCpl'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    Geral.WideStringToSQLString(InfAdic_InfAdFisco),
    Geral.WideStringToSQLString(InfAdic_InfCpl)], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_ZA01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  Exporta_UFEmbarq, Exporta_XLocEmbarq, Exporta_XLocDespacho: WideString;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo ZA01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_ZA01);
    if P = -1 then
      AvisaItemDesconhecido('ZA01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    Exporta_UFEmbarq := ''; Exporta_XLocEmbarq := '';
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if (Campo = LowerCase('UFEmbarq'))
        or (Campo = LowerCase('UFSaidaPais')) then
          Exporta_UFEmbarq := dmkPF.XMLValTxt(XMLNohNivel2) else
        if (Campo = LowerCase('xLocEmbarq'))
        or (Campo = LowerCase('xLocExporta')) then
          Exporta_XLocEmbarq := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xLocDespacho') then
          Exporta_XLocDespacho := dmkPF.XMLValTxt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('ZA01', Campo);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabZA01, False, [
    //'FatID', 'FatNum', 'Empresa',
    'Exporta_UFEmbarq', 'Exporta_XLocEmbarq', 'Exporta_XLocDespacho'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    Exporta_UFEmbarq, Exporta_XLocEmbarq, Exporta_XLocDespacho], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaNFe_Grupo_ZB01(XMLNohNivel1: IXMLNode);
var
  I, P: Integer;
  AttrNode, XMLNohNivel2: IXMLNode;
  Campo: String;
  Compra_xNEmp, Compra_xPed, Compra_xCont: WideString;
begin
  if not (XMLNohNivel1.NodeType = ntElement) then
    Exit;
  if XMLNohNivel1.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo ZB01!');
  for I := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel1.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    P := AnsiIndexStr(Campo, FGrupo_ZB01);
    if P = -1 then
      AvisaItemDesconhecido('ZB01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel1.HasChildNodes then
  begin
    Compra_xNEmp := ''; Compra_xPed := ''; Compra_xCont := '';
    for I := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
    begin
      XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[I];
      if (XMLNohNivel2.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel2.NodeName);
        if Campo = LowerCase('xNEmp') then
          Compra_xNEmp := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xPed') then
          Compra_xPed := dmkPF.XMLValTxt(XMLNohNivel2) else
        if Campo = LowerCase('xCont') then
          Compra_xCont := dmkPF.XMLValTxt(XMLNohNivel2) else
        //
        AvisaItemDesconhecido('ZB01', Campo);
      end;
    end;
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FCabZB01, False, [
    //'FatID', 'FatNum', 'Empresa',
    'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont'], [
    'SeqArq', 'SeqNFe'], [
    //FatID, FatNum, Empresa,
    Compra_XNEmp, Compra_XPed, Compra_XCont], [
    FSeqArq, FSeqNFe], False) then ;
  end;
end;

procedure TFmNFeLoad_Arq.ImportaGrupo_PR01(XMLNohNivel0: IXMLNode);
var
  I, J: Integer;
  AttrNode, XMLNohNivel1, XMLNohNivel2: IXMLNode;
  Campo: String;
  //
  protNFe_versao: Double;
  infProt_tpAmb, infProt_cStat: Integer;
  infProt_nProt: Int64;
  infProt_Id, infProt_xAmb, infProt_verAplic, infProt_chNFe,
  infProt_dhRecbto, infProt_digVal, infProt_xMotivo: String;
begin
  protNFe_versao := 0; infProt_tpAmb := 0; infProt_cStat := 0; infProt_nProt := 0;
  infProt_Id := ''; infProt_xAmb := ''; infProt_verAplic := '';
  infProt_chNFe := ''; infProt_dhRecbto := ''; infProt_digVal := '';
  infProt_xMotivo := '';
  //
  if not (XMLNohNivel0.NodeType = ntElement) then
    Exit;
  if XMLNohNivel0.IsTextElement then
    Geral.MB_Erro('Elemento de texto desconhecido no Grupo PR01!');
  for I := 0 to XMLNohNivel0.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNohNivel0.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //P := AnsiIndexStr(Campo, FGrupo_PR01);
    if Campo = LowerCase('versao') then
      protNFe_versao := dmkPF.XMLValFlu(AttrNode) else
    //
    AvisaItemDesconhecido('PR01', AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNohNivel0.HasChildNodes then
  begin
    for I := 0 to XMLNohNivel0.ChildNodes.Count - 1 do
    begin
      XMLNohNivel1 := XMLNohNivel0.ChildNodes.Nodes[I];
      if (XMLNohNivel1.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohNivel1.NodeName);
        if Campo = LowerCase('infProt') then
        begin
          //ImportaGrupo_PR03(XMLNohNivel1) else
          if not (XMLNohNivel1.NodeType = ntElement) then
            Exit;
          if XMLNohNivel1.IsTextElement then
            Geral.MB_Erro('Elemento de texto desconhecido no Grupo PR03!');
          for J := 0 to XMLNohNivel1.AttributeNodes.Count - 1 do
          begin
            AttrNode := XMLNohNivel1.AttributeNodes.Nodes[J];
            Campo := Lowercase(AttrNode.NodeName);
            //P := AnsiIndexStr(Campo, FGrupo_PR03);
            if Campo = LowerCase('Id') then
              infProt_Id := Geral.SoNumero_TT(AttrNode.NodeValue) else // Tirar o "Id" do in�cio!
            //
            AvisaItemDesconhecido('PR03', AttrNode.NodeName);
          end;
          // Subn�s
          if XMLNohNivel1.HasChildNodes then
          begin
            for J := 0 to XMLNohNivel1.ChildNodes.Count - 1 do
            begin
              XMLNohNivel2 := XMLNohNivel1.ChildNodes.Nodes[J];
              if (XMLNohNivel2.NodeType = ntElement) then
              begin
                Campo := Lowercase(XMLNohNivel2.NodeName);
                if Campo = LowerCase('tpAmb') then
                  infProt_tpAmb := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('verAplic') then
                  infProt_verAplic := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('chNFe') then
                  infProt_chNFe := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('dhRecbto') then
                  infProt_dhRecbto := dmkPF.XMLValDth(XMLNohNivel2)
                else
                if Campo = LowerCase('nProt') then
                  infProt_nProt := dmkPF.XMLValI64(XMLNohNivel2)
                else
                if Campo = LowerCase('digVal') then
                  infProt_digVal := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('cStat') then
                  infProt_cStat := dmkPF.XMLValInt(XMLNohNivel2)
                else
                if Campo = LowerCase('xMotivo') then
                  infProt_xMotivo := dmkPF.XMLValTxt(XMLNohNivel2)
                else
                if Campo = LowerCase('Signature') then
                  FAssinado := 1
                else
                //
                AvisaItemDesconhecido('PR03', XMLNohNivel2.NodeName);
              end;
            end;
          end;
        end else
        //
        AvisaItemDesconhecido('PR01', XMLNohNivel1.NodeName);
      end;
    end;
  end;
  infProt_xAmb := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_tpAmb, infProt_tpAmb);
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlPR01, False, [
  'protNFe_versao', 'infProt_Id', 'infProt_tpAmb',
  'infProt_xAmb', 'infProt_verAplic', 'infProt_chNFe',
  'infProt_dhRecbto', 'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo'], [
  'SeqArq'], [
  protNFe_versao, infProt_Id, infProt_tpAmb,
  infProt_xAmb, infProt_verAplic, infProt_chNFe,
  infProt_dhRecbto, infProt_nProt, infProt_digVal,
  infProt_cStat, infProt_xMotivo], [
  FSeqArq], False) then ;
end;

function TFmNFeLoad_Arq.ImportaXML(): Boolean;
const
  MaxNos = 0;
  Nos: Array[0..MaxNos] of String = ('nfeproc');
{
var
  P: Integer;
  //
  function IndexNoh(Nome: String): Integer;
  var
    Y: Integer;
  begin
    Result := -1;
    for Y := 0 to MaxNos do
    begin
      if Nome = Nos[Y] then
      begin
        Result := Y;
        Break;
      end;
    end;
  end;
var
  ArqDir, ArqName, Codificacao, LocalName, Prefix, NamespaceURI, NodeName,
  xTipoDoc: String;
  Versao: Double;
}
begin
  MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Importando XML.');
  Result := False;
  FSeqArq := FSeqArq + 1;
  //
  if ApenasCarregaArquivo() then
  begin
    QrArqs.Close;
    //
    ResetaVarsETabs();
    //
    if CarregaArquivoNoTempBD(XMLDoc, EdArquivo.Text) then
    begin
      {
      Campo := LowerCase(XMLDoc.DocumentElement.NodeName);
      //
      P := AnsiIndexStr(Lowercase(Campo), FElementos);
      if P = -1 then
      begin
        if not MyObjects.IgnoraFIC(True, nil,
          'Tipo de elemento n�o implementado: ' + Campo) then
        Exit;
      end;
      //
      if Campo = LowerCase('nfe') then
      begin
        if FAssinado = 1 then
          cTipoDoc := NFeTipoDoc_nfe_1
        else
          cTipoDoc := NFeTipoDoc_nfe_0;
      end else
      if Campo = LowerCase('enviNFe') then
        cTipoDoc := NFeTipoDoc_enviNFe
      else
      if Campo = LowerCase('nfeProc') then
        cTipoDoc := NFeTipoDoc_nfeProc
      else
      if Campo = LowerCase('cancNFe') then
        cTipoDoc := NFeTipoDoc_cancNFe
      else
      if Campo = LowerCase('retCancNFe') then
        cTipoDoc := NFeTipoDoc_retCancNFe
      else
      if Campo = LowerCase('procCancNFe') then
        cTipoDoc := NFeTipoDoc_procCancNFe
      else
      if Campo = LowerCase('inutnfe') then
        cTipoDoc := NFeTipoDoc_inutNFe
      else
      if Campo = LowerCase('retinutnfe') then
        cTipoDoc := NFeTipoDoc_retInutNFe
      else
      if Campo = LowerCase('procInutNFe') then
        cTipoDoc := NFeTipoDoc_procInutNFe
      else begin
        Geral.MB_Erro('Elemento desconhecido na defini��o do tipo de documento: ' + Campo,
          );
        cTipoDoc := NFeTipoDoc_Desconhecido;
      end;
      //
      Versao       := Geral.DMV_Dot(XMLDoc.Version);
      Codificacao  := XMLDoc.Encoding;
      //XMLDoc.DocumentElement;                              // Erro
      //XMLDoc.DocumentElement.XML;                               // Todo texto do XML  ap�s a declara��o: <?xml version="1.0" encoding="utf-8" ?>
      LocalName    := XMLDoc.DocumentElement.LocalName;    // NFe
      Prefix       := XMLDoc.DocumentElement.Prefix;       // [nada]
      NamespaceURI := XMLDoc.DocumentElement.NamespaceURI; // http://www.portalfiscal.inf.br/nfe
      NodeName     := XMLDoc.DocumentElement.NodeName;     // NF
      //
      ArqDir   := ExtractFileDir(EdArquivo.Text);
      ArqName  := ExtractFileName(EdArquivo.Text);

      xTipoDoc := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_TipoDoc, Integer(cTipoDoc));

      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlArqs, False, [
      'Versao', 'Codificacao', 'LocalName',
      'Prefix', 'NodeName', 'NamespaceURI',
      'ArqDir', 'ArqName', 'xTipoDoc',
      'Assinado'], [
      'SeqArq'], [
      Versao, Codificacao, LocalName,
      Prefix, NodeName, NamespaceURI,
      ArqDir, ArqName, xTipoDoc,
      FAssinado], [
      FSeqArq], False) then ;
      //
      XMLDoc.Active := False;
      }
      Result := True;
      UnDmkDAC_PF.AbreQueryApenas(QrArqs);
    end;
  end;
end;

procedure TFmNFeLoad_Arq.PMAlteraPopup(Sender: TObject);
begin
  DaEntidadeDoDestinatrio1.Enabled :=
    FPermiteAlterarDadosE01 or FPermiteAlterarCodInfoDest;
end;

procedure TFmNFeLoad_Arq.QrArqsAfterScroll(DataSet: TDataSet);
begin
  if not MyObjects.IgnoraFIC(QrArqsVersao.Value <> 1.0, nil,
        'Vers�o do XML n�o implementada!') then ;
  if not MyObjects.IgnoraFIC(LowerCase(QrArqsCodificacao.Value) <> 'utf-8', nil,
        'Codifica�ao n�o implementada!') then ;
  //
  QrCP01.Close;
  QrCP01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrCP01);
  //
  QrCR01.Close;
  QrCR01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrCR01);
  //
  QrDP01.Close;
  QrDP01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrDP01);
  //
  QrDR01.Close;
  QrDR01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrDR01);
  //
  QrPR01.Close;
  QrPR01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrPR01);
  //
  QrXR01.Close;
  QrXR01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrXR01);
  //
  QrYR01.Close;
  QrYR01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrYR01);
  //
  QrZR01.Close;
  QrZR01.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrZR01);
  //
  QrNFeA.Close;
  QrNFeA.Params[00].AsInteger := QrArqsSeqArq.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeA);
end;

procedure TFmNFeLoad_Arq.QrArqsBeforeClose(DataSet: TDataSet);
begin
  QrCP01.Close;
  QrCR01.Close;
  QrDP01.Close;
  QrDR01.Close;
  QrPR01.Close;
  QrXR01.Close;
  QrYR01.Close;
  QrZR01.Close;
  QrNFeA.Close;
end;

procedure TFmNFeLoad_Arq.QrDR01CalcFields(DataSet: TDataSet);
begin
  QrDR01retInutNFe_dhRecbto_TXT.Value :=
    Geral.FDT(QrDR01retInutNFe_dhRecbto.Value, 0, True);
end;

procedure TFmNFeLoad_Arq.QrNFeAAfterScroll(DataSet: TDataSet);
begin
  QrNFeB.Close;
  QrNFeB.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeB.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeB);
  //
  QrNFeB12a.Close;
  QrNFeB12a.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeB12a.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeB12a);
  //
  QrNFeC.Close;
  QrNFeC.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeC.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeC);
  //
  QrNFeD.Close;
  QrNFeD.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeD.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeD);
  //
  QrNFeE.Close;
  QrNFeE.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeE.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeE);
  //
  QrNFeF.Close;
  QrNFeF.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeF.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeF);
  //
  QrNFeG.Close;
  QrNFeG.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeG.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeG);
  //
  QrNFeI.Close;
  QrNFeI.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeI.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeI);
  //
  QrNFeW02.Close;
  QrNFeW02.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeW02.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeW02);
  //
  QrNFeW17.Close;
  QrNFeW17.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeW17.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeW17);
  //
  QrNFeW23.Close;
  QrNFeW23.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeW23.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeW23);
  //
  QrNFeX01.Close;
  QrNFeX01.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeX01.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeX01);
  //
  QrNFeX22.Close;
  QrNFeX22.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeX22.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeX22);
  //
  QrNFeX26.Close;
  QrNFeX26.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeX26.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeX26);
  //
  QrNFeX33.Close;
  QrNFeX33.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeX33.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeX33);
  //
  QrNFeY01.Close;
  QrNFeY01.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeY01.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeY01);
  //
  QrNFeY07.Close;
  QrNFeY07.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeY07.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeY07);
  //
  QrNFeZ01.Close;
  QrNFeZ01.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeZ01.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeZ01);
  //
  QrNFeZ04.Close;
  QrNFeZ04.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeZ04.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeZ04);
  //
  QrNFeZ07.Close;
  QrNFeZ07.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeZ07.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeZ07);
  //
  QrNFeZ10.Close;
  QrNFez10.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFez10.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeZ10);
  //
  QrNFeZA01.Close;
  QrNFeZA01.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeZA01.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeZA01);
  //
  QrNFeZB01.Close;
  QrNFeZB01.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeZB01.Params[01].AsInteger := QrNFeASeqNFe.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeZB01);
  //
end;

procedure TFmNFeLoad_Arq.QrNFeABeforeClose(DataSet: TDataSet);
begin
  QrNFeB.Close;
  QrNFeB12a.Close;
  QrNFeC.Close;
  QrNFeD.Close;
  QrNFeE.Close;
  QrNFeF.Close;
  QrNFeG.Close;
  //...
  QrNFeI.Close;
  QrNFeW02.Close;
  QrNFeW17.Close;
  QrNFeW23.Close;
  QrNFeX01.Close;
  QrNFeX22.Close;
  QrNFeX26.Close;
  QrNFeX33.Close;
  QrNFeY01.Close;
  QrNFeY07.Close;
  QrNFeZ01.Close;
  QrNFeZ04.Close;
  QrNFeZ07.Close;
  QrNFeZB01.Close;
  QrNFeZA01.Close;
  QrNFeZB01.Close;
end;

procedure TFmNFeLoad_Arq.QrNFeBCalcFields(DataSet: TDataSet);
begin
  QrNFeBide_dhCont_TXT.Value := Geral.FDT(QrNFeBide_dhCont.Value, 0, True);
end;

procedure TFmNFeLoad_Arq.QrNFeEAfterScroll(DataSet: TDataSet);
begin
  BtCadDest.Enabled := (QrNFeECodInfoDest.Value = 0) and
  ((QrNFeEdest_CNPJ.Value <> '') or (QrNFeEdest_CPF.Value <> ''));
end;

procedure TFmNFeLoad_Arq.QrNFeEBeforeClose(DataSet: TDataSet);
begin
  BtCadDest.Enabled := False;
end;

procedure TFmNFeLoad_Arq.QrNFeIAfterScroll(DataSet: TDataSet);
begin
  QrNFeIDi.Close;
  QrNFeIDi.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeIDi.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeIDi.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeIDi);
  //
  QrNFeN.Close;
  QrNFeN.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeN.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeN.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeN);
  //
  QrNFeO.Close;
  QrNFeO.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeO.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeO.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeO);
  //
  QrNFeP.Close;
  QrNFeP.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeP.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeP.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeP);
  //
  QrNFeQ.Close;
  QrNFeQ.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeQ.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeQ.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeQ);
  //
  QrNFeR.Close;
  QrNFeR.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeR.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeR.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeR);
  //
  QrNFeS.Close;
  QrNFeS.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeS.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeS.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeS);
  //
  QrNFeT.Close;
  QrNFeT.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeT.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeT.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeT);
  //
  QrNFeU.Close;
  QrNFeU.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeU.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeU.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeU);
  //
  QrNFeV.Close;
  QrNFeV.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeV.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeV.Params[02].AsInteger := QrNFeInItem.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeV);
  //
end;

procedure TFmNFeLoad_Arq.QrNFeIBeforeClose(DataSet: TDataSet);
begin
  QrNFeIDi.Close;
  QrNFeN.Close;
  QrNFeO.Close;
  QrNFeP.Close;
  QrNFeQ.Close;
  QrNFeR.Close;
  QrNFeS.Close;
  QrNFeT.Close;
  QrNFeU.Close;
  QrNFeV.Close;
end;

procedure TFmNFeLoad_Arq.QrNFeIDiAfterScroll(DataSet: TDataSet);
begin
  QrNFeIDiA.Close;
  QrNFeIDiA.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeIDiA.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeIDiA.Params[02].AsString  := QrNFeIDiDI_nDI.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeIDiA);
end;

procedure TFmNFeLoad_Arq.QrNFeIDiBeforeClose(DataSet: TDataSet);
begin
  QrNFeIDiA.Close;
end;

procedure TFmNFeLoad_Arq.QrNFeUCalcFields(DataSet: TDataSet);
begin
  if QrNFeUISSQN_cSitTrib.Value = 'N' then
    QrNFeUISSQN_cSitTrib_TXT.Value := 'NORMAL' else
  if QrNFeUISSQN_cSitTrib.Value = 'R' then
    QrNFeUISSQN_cSitTrib_TXT.Value := 'RETIDA' else
  if QrNFeUISSQN_cSitTrib.Value = 'S' then
    QrNFeUISSQN_cSitTrib_TXT.Value := 'SUBSTITUTA' else
  if QrNFeUISSQN_cSitTrib.Value = 'I' then
    QrNFeUISSQN_cSitTrib_TXT.Value := 'ISENTA' else
    QrNFeUISSQN_cSitTrib_TXT.Value := '? ? ? ? ?';
end;

procedure TFmNFeLoad_Arq.QrNFeX26AfterScroll(DataSet: TDataSet);
begin
  QrNFeX33.Close;
  QrNFeX33.Params[00].AsInteger := QrNFeASeqArq.Value;
  QrNFeX33.Params[01].AsInteger := QrNFeASeqNFe.Value;
  QrNFeX33.Params[02].AsInteger := QrNFeX26Controle.Value;
  UnDmkDAC_PF.AbreQueryApenas(QrNFeX33);
end;

procedure TFmNFeLoad_Arq.QrNFeX26BeforeClose(DataSet: TDataSet);
begin
  QrNFeX33.Close;
end;

procedure TFmNFeLoad_Arq.QrNFeZ10CalcFields(DataSet: TDataSet);
begin
  QrNFeZ10indProc_TXT.Value := UFinanceiro.IndProc_Get(QrNFeZ10indProc.Value);
end;

procedure TFmNFeLoad_Arq.ResetaVarsETabs();
const
  QtdTabelas = 45;
var
  n: integer;
  function  RecriaTabela(Tabela: TNomeTabRecriaTempTable): String;
  var
    NoTab: String;
  begin
    n := n + 1;
    MyObjects.Informa2EUpdPB(FPB1, FLaAviso1, FLaAviso2, True,
      'Criando tabelas tempor�rias: ' + Geral.FF0(n) + ' de ' + Geral.FF0(
      QtdTabelas));
    if FRecriaTbTmp then
      Result := UnCreateNFe.RecriaTempTableNovo(Tabela, DmodG.QrUpdPID1, False)
    else
    begin
      try
        NoTab := UnCreateNFe.NomeTabela(Tabela);
        DmodG.MyPID_DB.Execute('DELETE FROM ' + NoTab);
        Result := NoTab;
      except
        try
          Result := UnCreateNFe.RecriaTempTableNovo(Tabela, DmodG.QrUpdPID1, False);
        except
          on e: exception do
            Geral.MB_Erro(E.Message + sLineBreak + DmodG.QrUpdPID1.SQL.Text);
        end;
      end;
    end;
  end;
begin
  FSeqNFe   := 0;
  FCtrlB12a := 0;
  FCtrlIDiA := 0;
  FCtrlY07  := 0;
  FCtrlX22  := 0;
  FCtrlX26  := 0;
  FCtrlX33  := 0;
  FCtrlZ04  := 0;
  FCtrlZ07  := 0;
  FCtrlZ10  := 0;
  //
  FCodInfoEmit := 0;
  //
  if FPB1 <> nil then
  begin
    FPB1.Position := 0;
    FPB1.Max      := QtdTabelas;
  end;
  FxmlArqs := RecriaTabela(ntrttNFeArqs);
  FxmlCP01 := RecriaTabela(ntrttNFeCP01);
  FxmlCR01 := RecriaTabela(ntrttNFeCR01);
  FxmlDP01 := RecriaTabela(ntrttNFeDP01);
  FxmlDR01 := RecriaTabela(ntrttNFeDR01);
  FxmlPR01 := RecriaTabela(ntrttNFePR01);
  FxmlXR01 := RecriaTabela(ntrttNFeXR01);
  FxmlYR01 := RecriaTabela(ntrttNFeYR01);
  FxmlZR01 := RecriaTabela(ntrttNFeZR01);
  //
  FCabA    := RecriaTabela(ntrttNFeA);
  FCabB    := RecriaTabela(ntrttNFeB);
  FCabB12a := RecriaTabela(ntrttNFeB12a);
  FCabC    := RecriaTabela(ntrttNFeC);
  FCabD    := RecriaTabela(ntrttNFeD);
  FCabE    := RecriaTabela(ntrttNFeE);
  FCabF    := RecriaTabela(ntrttNFeF);
  FCabG    := RecriaTabela(ntrttNFeG);
  FCabGA   := RecriaTabela(ntrttNFeGA);
  //
  FItsI    := RecriaTabela(ntrttNFeI);
  FItsIDi  := RecriaTabela(ntrttNFeIDi);
  FItsIDiA := RecriaTabela(ntrttNFeIDiA);
  FItsM    := RecriaTabela(ntrttNFeM);
  FItsN    := RecriaTabela(ntrttNFeN);
  FItsO    := RecriaTabela(ntrttNFeO);
  FItsP    := RecriaTabela(ntrttNFeP);
  FItsQ    := RecriaTabela(ntrttNFeQ);
  FItsR    := RecriaTabela(ntrttNFeR);
  FItsS    := RecriaTabela(ntrttNFeS);
  FItsT    := RecriaTabela(ntrttNFeT);
  FItsU    := RecriaTabela(ntrttNFeU);
  FItsV    := RecriaTabela(ntrttNFeV);
  FCabW02  := RecriaTabela(ntrttNFeW02);
  FCabW17  := RecriaTabela(ntrttNFeW17);
  FCabW23  := RecriaTabela(ntrttNFeW23);
  FCabX01  := RecriaTabela(ntrttNFeX01);
  FItsX22  := RecriaTabela(ntrttNFeX22);
  FItsX26  := RecriaTabela(ntrttNFeX26);
  FItsX33  := RecriaTabela(ntrttNFeX33);
  FCabY01  := RecriaTabela(ntrttNFeY01);
  FItsY07  := RecriaTabela(ntrttNFeY07);
  FCabZ01  := RecriaTabela(ntrttNFeZ01);
  FItsZ04  := RecriaTabela(ntrttNFeZ04);
  FItsZ07  := RecriaTabela(ntrttNFeZ07);
  FItsZ10  := RecriaTabela(ntrttNFeZ10);
  FCabZA01  := RecriaTabela(ntrttNFeZA01);
  FCabZB01  := RecriaTabela(ntrttNFeZB01);
end;

function TFmNFeLoad_Arq.CadastraEntidade(TipoCad: TTipoCadNFe; CNPJ, CPF, xNome,
              xFant, IE, xLgr, nro, xBairro: WideString; cMun: Integer; xMun,
              UF: WideString; CEP,  cPais: Integer; xPais, fone, IEST, IM, CNAE,
              xCpl, xEnder: WideString): Integer;
var
  Msg: WideString;
begin
  //////////////////////////////////////////////////////////////////////////////
  if (Trim(CNPJ) = EmptyStr) and (Trim(CPF) = EmptyStr) then Exit;
  //////////////////////////////////////////////////////////////////////////////

  Result := 0;
  //
  case TipoCad of
    entNFe_emit: Msg := 'O fornecedor abaixo n�o est� cadastrado:';
    entNFe_dest: Msg := 'O cliente abaixo n�o est� cadastrado:';
    entNFe_transp: Msg := 'A transportadora abaixo n�o est� cadastrada:';
    else Msg := 'A entidade abaixo n�o est� cadastrada:';
  end;
  Msg := Msg + sLineBreak +
  'Nome: ' + xNome + sLineBreak;
  if CNPJ <> '' then
    Msg := Msg + 'CNPJ: ' + Geral.FormataCNPJ_TT(CNPJ) + sLineBreak;
  if CPF <> '' then
    Msg := Msg + 'CPF: ' + Geral.FormataCNPJ_TT(CPF) + sLineBreak;
  if IE <> '' then
    Msg := Msg + 'I.E.: ' + IE + sLineBreak;
  Msg := Msg + sLineBreak + 'Deseja cadastrar agora?';
  //
  if Geral.MB_Pergunta(Msg) = ID_YES then
  begin
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      FmEntidade2.Incluinovaentidade1Click(Self);
      FmEntidade2.FSetando := True;
      FmEntidade2.CadastroPorXML_NFe_2(TipoCad, CNPJ, CPF, xNome, xFant, IE,
        xLgr, nro, xBairro, cMun, xMun, UF, CEP, cPais, xPais, fone, IEST, IM,
        CNAE, xCpl, xEnder);
      FmEntidade2.FSetando := False;
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
      //
      Result := DmNFe_0000.DefineEntidadePeloDoc(CNPJ, CPF);
    end;
  end;
end;

function TFmNFeLoad_Arq.CarregaArquivoNoTempBD(XMLDocument: TXMLDocument; FullNameArq: String): Boolean;
var
  xmlNodeA: IXMLNode;
  MyCursor: TCursor;
  Campo: String;
  cTipoDoc: TNFeCTide_TipoDoc;
  //
  ArqDir, ArqName, Codificacao, LocalName, Prefix, NamespaceURI, NodeName,
  xTipoDoc: String;
  Versao: Double;
  P: Integer;
begin
  Result := False;
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    // Para garantir que n�o usara do XML do arquivo anterior quando ler v�rios
    FCodInfoEmit := 0;
    //
    FAssinado := 0;
    Campo := '';
    //cTipoDoc := NFeTipoDoc_Desconhecido;
    MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Importando dados carregados');
    // Lote de envio de NF-e(s)
    xmlNodeA := XMLDocument.DocumentElement;
    if LowerCase(xmlNodeA.NodeName) = LowerCase('enviNFe') then
      ImportaGrupo_AP01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('nfeProc') then
      ImportaGrupo_XR01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('cancNFe') then
      ImportaGrupo_CP01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('retCancNFe') then
      ImportaGrupo_CR01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('procCancNFe') then
      ImportaGrupo_YR01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('inutNFe') then
      ImportaGrupo_DP01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('retInutNFe') then
      ImportaGrupo_DR01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('procInutNFe') then
      ImportaGrupo_ZR01(xmlNodeA)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('NFe') then
      ImportaNFe_Grupo_A00(xmlNodeA)
    else begin
      Geral.MB_Aviso('"DocumentElement" desconhecido ou n�o implementado:' +
      sLineBreak + xmlNodeA.NodeName);
      //
      Exit;
    end;
    //
    Campo := LowerCase(XMLDocument.DocumentElement.NodeName);
    //
    P := AnsiIndexStr(Lowercase(Campo), FElementos);
    if P = -1 then
    begin
      if not MyObjects.IgnoraFIC(True, nil,
        'Tipo de elemento n�o implementado: ' + Campo) then
      Exit;
    end;
    //
    if Campo = LowerCase('nfe') then
    begin
      if FAssinado = 1 then
        cTipoDoc := NFeTipoDoc_nfe_1
      else
        cTipoDoc := NFeTipoDoc_nfe_0;
    end else
    if Campo = LowerCase('enviNFe') then
      cTipoDoc := NFeTipoDoc_enviNFe
    else
    if Campo = LowerCase('nfeProc') then
      cTipoDoc := NFeTipoDoc_nfeProc
    else
    if Campo = LowerCase('cancNFe') then
      cTipoDoc := NFeTipoDoc_cancNFe
    else
    if Campo = LowerCase('retCancNFe') then
      cTipoDoc := NFeTipoDoc_retCancNFe
    else
    if Campo = LowerCase('procCancNFe') then
      cTipoDoc := NFeTipoDoc_procCancNFe
    else
    if Campo = LowerCase('inutnfe') then
      cTipoDoc := NFeTipoDoc_inutNFe
    else
    if Campo = LowerCase('retinutnfe') then
      cTipoDoc := NFeTipoDoc_retInutNFe
    else
    if Campo = LowerCase('procInutNFe') then
      cTipoDoc := NFeTipoDoc_procInutNFe
    else begin
      Geral.MB_Erro('Elemento desconhecido na defini��o do tipo de documento: ' + Campo);
      cTipoDoc := NFeTipoDoc_Desconhecido;
    end;
    //
    Versao       := Geral.DMV_Dot(XMLDocument.Version);
    Codificacao  := XMLDocument.Encoding;
    //XMLDocument.DocumentElement;                            // Erro
    //XMLDocument.DocumentElement.XML;                        // Todo texto do XML  ap�s a declara��o: <?xml version="1.0" encoding="utf-8" ?>
    LocalName    := XMLDocument.DocumentElement.LocalName;    // NFe
    Prefix       := XMLDocument.DocumentElement.Prefix;       // [nada]
    NamespaceURI := XMLDocument.DocumentElement.NamespaceURI; // http://www.portalfiscal.inf.br/nfe
    NodeName     := XMLDocument.DocumentElement.NodeName;     // NF
    //
    ArqDir   := ExtractFileDir(FullNameArq);
    ArqName  := ExtractFileName(FullNameArq);

    xTipoDoc := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_TipoDoc, Integer(cTipoDoc));

    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FxmlArqs, False, [
    'Versao', 'Codificacao', 'LocalName',
    'Prefix', 'NodeName', 'NamespaceURI',
    'ArqDir', 'ArqName', 'xTipoDoc',
    'Assinado'], [
    'SeqArq'], [
    Versao, Codificacao, LocalName,
    Prefix, NodeName, NamespaceURI,
    ArqDir, ArqName, xTipoDoc,
    FAssinado], [
    FSeqArq], False) then ;
    //
    XMLDocument.Active := False;
    //
    Result := True;
    MyObjects.Informa2(FLaAviso1, FLaAviso2, False, 'XML importado!');
  finally
    Screen.Cursor := MyCursor;
  end;
end;

end.
