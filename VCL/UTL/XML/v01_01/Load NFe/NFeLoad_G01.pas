unit NFeLoad_G01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_G01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label37: TLabel;
    Edentrega_CNPJ: TdmkEdit;
    Edentrega_xLgr: TdmkEdit;
    Edentrega_nro: TdmkEdit;
    Edentrega_xCpl: TdmkEdit;
    Edentrega_xBairro: TdmkEdit;
    Edentrega_cMun: TdmkEdit;
    Edentrega_UF: TdmkEdit;
    Edentrega_xMun: TdmkEdit;
    Edentrega_CPF: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Edentrega_cMunChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_G01: TFmNFeLoad_G01;

implementation

uses UnMyObjects, ModuleNFe_0000;

{$R *.DFM}

procedure TFmNFeLoad_G01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_G01.Edentrega_cMunChange(Sender: TObject);
begin
  if DmNFe_0000.QrDTB_Munici.Locate('Codigo', Edentrega_cMun.ValueVariant, []) then
    Edentrega_xMun.Text := DmNFe_0000.QrDTB_MuniciNome.Value
  else
    Edentrega_xMun.Text := '';
end;

procedure TFmNFeLoad_G01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_G01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_G01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
