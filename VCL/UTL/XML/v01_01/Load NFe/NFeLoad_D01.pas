unit NFeLoad_D01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkImage, UnDmkEnums;

type
  TFmNFeLoad_D01 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel4: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Edavulsa_CNPJ: TdmkEdit;
    Edavulsa_nDAR: TdmkEdit;
    Edavulsa_fone: TdmkEdit;
    Edavulsa_matr: TdmkEdit;
    Edavulsa_repEmi: TdmkEdit;
    Edavulsa_xOrgao: TdmkEdit;
    Edavulsa_xAgente: TdmkEdit;
    Edavulsa_UF: TdmkEdit;
    Edavulsa_vDAR: TdmkEdit;
    TPavulsa_dEmi: TdmkEditDateTimePicker;
    TPavulsa_dPag: TdmkEditDateTimePicker;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmNFeLoad_D01: TFmNFeLoad_D01;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmNFeLoad_D01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_D01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFeLoad_D01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmNFeLoad_D01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
