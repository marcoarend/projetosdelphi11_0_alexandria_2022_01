object FmNFeLoad_C01: TFmNFeLoad_C01
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 360
  ClientWidth = 991
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 312
    Width = 991
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 879
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object Panel1: TPanel
    Left = 101
    Top = 48
    Width = 991
    Height = 264
    TabOrder = 0
    object Panel56: TPanel
      Left = 1
      Top = 1
      Width = 989
      Height = 49
      Align = alTop
      ParentBackground = False
      TabOrder = 0
      object Label359: TLabel
        Left = 4
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Emitente:'
      end
      object EdCodInfoEmit: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodInfoEmit'
        UpdCampo = 'CodInfoEmit'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNomeEmit: TdmkEdit
        Left = 64
        Top = 20
        Width = 917
        Height = 21
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object Panel16: TPanel
      Left = 1
      Top = 50
      Width = 989
      Height = 213
      Align = alClient
      ParentBackground = False
      TabOrder = 1
      ExplicitLeft = 105
      ExplicitTop = 54
      object Label120: TLabel
        Left = 4
        Top = 4
        Width = 42
        Height = 13
        Caption = 'CNPJ ...:'
        FocusControl = Edemit_CNPJ
      end
      object Label121: TLabel
        Left = 236
        Top = 4
        Width = 112
        Height = 13
        Caption = 'Raz'#227'o Social ou Nome:'
        FocusControl = Edemit_xNome
      end
      object Label122: TLabel
        Left = 752
        Top = 4
        Width = 74
        Height = 13
        Caption = 'Nome Fantasia:'
        FocusControl = Edemit_xFant
      end
      object Label123: TLabel
        Left = 4
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Logradouro:'
        FocusControl = Edemit_xLgr
      end
      object Label124: TLabel
        Left = 428
        Top = 44
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = Edemit_nro
      end
      object Label125: TLabel
        Left = 480
        Top = 44
        Width = 64
        Height = 13
        Caption = 'Complemento'
        FocusControl = Edemit_xCpl
      end
      object Label126: TLabel
        Left = 4
        Top = 84
        Width = 30
        Height = 13
        Caption = 'Bairro:'
        FocusControl = Edemit_xBairro
      end
      object Label127: TLabel
        Left = 428
        Top = 84
        Width = 141
        Height = 13
        Caption = 'C'#243'digo e Nome do Munic'#237'pio:'
        FocusControl = Edemit_cMun
      end
      object Label128: TLabel
        Left = 4
        Top = 124
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = Edemit_UF
      end
      object Label129: TLabel
        Left = 36
        Top = 124
        Width = 24
        Height = 13
        Caption = 'CEP:'
        FocusControl = Edemit_CEP
      end
      object Label130: TLabel
        Left = 100
        Top = 124
        Width = 113
        Height = 13
        Caption = 'C'#243'digo e nome do pa'#237's:'
        FocusControl = Edemit_cPais
      end
      object Label131: TLabel
        Left = 844
        Top = 124
        Width = 45
        Height = 13
        Caption = 'Telefone:'
        FocusControl = Edemit_fone
      end
      object Label132: TLabel
        Left = 4
        Top = 164
        Width = 90
        Height = 13
        Caption = 'Inscri'#231#227'o Estadual:'
        FocusControl = Edemit_IE
      end
      object Label133: TLabel
        Left = 140
        Top = 164
        Width = 127
        Height = 13
        Caption = 'I.E. do Substituto tribut'#225'rio:'
        FocusControl = Edemit_IEST
      end
      object Label134: TLabel
        Left = 276
        Top = 164
        Width = 94
        Height = 13
        Caption = 'Inscri'#231#227'o Municipal:'
        FocusControl = Edemit_IM
      end
      object Label135: TLabel
        Left = 412
        Top = 164
        Width = 62
        Height = 13
        Caption = 'CNAE Fiscal:'
      end
      object Label136: TLabel
        Left = 120
        Top = 4
        Width = 50
        Height = 13
        Caption = '... ou CPF:'
        FocusControl = Edemit_CPF
      end
      object Label7: TLabel
        Left = 748
        Top = 164
        Width = 25
        Height = 13
        Caption = 'CRT:'
      end
      object Edemit_CNPJ: TdmkEdit
        Left = 4
        Top = 20
        Width = 112
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtCPFJ_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_CNPJ'
        UpdCampo = 'emit_CNPJ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = Edemit_CNPJChange
      end
      object Edemit_xNome: TdmkEdit
        Left = 236
        Top = 20
        Width = 513
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xNome'
        UpdCampo = 'emit_xNome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_xFant: TdmkEdit
        Left = 752
        Top = 20
        Width = 229
        Height = 21
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xFant'
        UpdCampo = 'emit_xFant'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_xLgr: TdmkEdit
        Left = 4
        Top = 60
        Width = 420
        Height = 21
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xLgr'
        UpdCampo = 'emit_xLgr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_nro: TdmkEdit
        Left = 428
        Top = 60
        Width = 48
        Height = 21
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_nro'
        UpdCampo = 'emit_nro'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_xCpl: TdmkEdit
        Left = 480
        Top = 60
        Width = 501
        Height = 21
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xCpl'
        UpdCampo = 'emit_xCpl'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_xBairro: TdmkEdit
        Left = 4
        Top = 100
        Width = 420
        Height = 21
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xBairro'
        UpdCampo = 'emit_xBairro'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_cMun: TdmkEdit
        Left = 428
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        MaxLength = 7
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emit_cMun'
        UpdCampo = 'emit_cMun'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = Edemit_cMunChange
      end
      object Edemit_UF: TdmkEdit
        Left = 4
        Top = 140
        Width = 30
        Height = 21
        ReadOnly = True
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_UF'
        UpdCampo = 'emit_UF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = Edemit_UFChange
      end
      object Edemit_CEP: TdmkEdit
        Left = 36
        Top = 140
        Width = 61
        Height = 21
        ReadOnly = True
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtCEP_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_CEP'
        UpdCampo = 'emit_CEP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_cPais: TdmkEdit
        Left = 100
        Top = 140
        Width = 48
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emit_cPais'
        UpdCampo = 'emit_cPais'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = Edemit_cPaisChange
      end
      object Edemit_fone: TdmkEdit
        Left = 843
        Top = 140
        Width = 138
        Height = 21
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtTel_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_fone'
        UpdCampo = 'emit_fone'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_IE: TdmkEdit
        Left = 4
        Top = 180
        Width = 132
        Height = 21
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfString
        MskType = fmtIE_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_IE'
        UpdCampo = 'emit_IE'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_IEST: TdmkEdit
        Left = 140
        Top = 180
        Width = 132
        Height = 21
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfString
        MskType = fmtIE_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_IEST'
        UpdCampo = 'emit_IEST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_IM: TdmkEdit
        Left = 276
        Top = 180
        Width = 132
        Height = 21
        ReadOnly = True
        TabOrder = 17
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_IM'
        UpdCampo = 'emit_IM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_xMun: TdmkEdit
        Left = 484
        Top = 100
        Width = 497
        Height = 21
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xMun'
        UpdCampo = 'emit_xMun'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_xPais: TdmkEdit
        Left = 148
        Top = 140
        Width = 693
        Height = 21
        ReadOnly = True
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_xPais'
        UpdCampo = 'emit_xPais'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_CPF: TdmkEdit
        Left = 120
        Top = 20
        Width = 112
        Height = 21
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtCPFJ_NFe
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_CPF'
        UpdCampo = 'emit_CPF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = Edemit_CPFChange
      end
      object Edemit_CNAE: TdmkEdit
        Left = 412
        Top = 180
        Width = 52
        Height = 21
        ReadOnly = True
        TabOrder = 18
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'emit_CNAE'
        UpdCampo = 'emit_CNAE'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = Edemit_CNAEChange
      end
      object Edemit_CNAE_TXT: TdmkEdit
        Left = 464
        Top = 180
        Width = 281
        Height = 21
        ReadOnly = True
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object Edemit_CRT: TdmkEdit
        Left = 748
        Top = 180
        Width = 24
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emit_CRT'
        UpdCampo = 'emit_CRT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = Edemit_CRTChange
      end
      object Edemit_CRT_TXT: TdmkEdit
        Left = 772
        Top = 180
        Width = 209
        Height = 21
        ReadOnly = True
        TabOrder = 21
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 991
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = -9
    ExplicitWidth = 1000
    object GB_R: TGroupBox
      Left = 943
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 952
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 895
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 904
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 452
        Height = 32
        Caption = '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ?'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
end
