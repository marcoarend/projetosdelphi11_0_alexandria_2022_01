unit CNAE21Cad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkMemo, dmkLabelRotate, Menus,
  frxClass, frxDBSet, UnDmkEnums;

type
  TFmCNAE21Cad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCNAE21CadCodigo: TIntegerField;
    QrCNAE21CadNivel: TSmallintField;
    QrCNAE21CadCodTxt: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    Panel6: TPanel;
    PnNome: TPanel;
    dmkLabelRotate1: TdmkLabelRotate;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrCNAE21CadNO_NIVEL: TWideStringField;
    Panel7: TPanel;
    EdCodigo: TdmkEdit;
    Panel8: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    MeNome: TdmkMemo;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdNivel: TdmkEditCB;
    CBNivel: TdmkDBLookupComboBox;
    EdCodAlf: TdmkEdit;
    QrCNAE21Niv: TmySQLQuery;
    DsCNAE21Niv: TDataSource;
    QrCNAE21NivCodigo: TIntegerField;
    QrCNAE21NivNome: TWideStringField;
    DBMemo1: TDBMemo;
    PMExclui: TPopupMenu;
    ExcluiItematual1: TMenuItem;
    ExcluiTODOSitens1: TMenuItem;
    BtImporta: TBitBtn;
    frxNFS_TBTER_002_001: TfrxReport;
    QrAll: TmySQLQuery;
    frxDsAll: TfrxDBDataset;
    QrAllCodigo: TIntegerField;
    QrAllNivel: TSmallintField;
    QrAllCodTxt: TWideStringField;
    QrAllNome: TWideStringField;
    QrAllLk: TIntegerField;
    QrAllDataCad: TDateField;
    QrAllDataAlt: TDateField;
    QrAllUserCad: TIntegerField;
    QrAllUserAlt: TIntegerField;
    QrAllAlterWeb: TSmallintField;
    QrAllAtivo: TSmallintField;
    QrAllNivel1: TWideStringField;
    QrAllNivel2: TWideStringField;
    QrAllNivel3: TWideStringField;
    QrAllNivel4: TWideStringField;
    QrAllNivel5: TWideStringField;
    Label4: TLabel;
    EdCodTxt: TdmkEdit;
    QrCNAE21CadNivel1: TWideStringField;
    QrCNAE21CadNivel2: TWideStringField;
    QrCNAE21CadNivel3: TWideStringField;
    QrCNAE21CadNivel4: TWideStringField;
    QrCNAE21CadNivel5: TWideStringField;
    QrCNAE21CadCodAlf: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCNAE21CadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCNAE21CadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ExcluiTODOSitens1Click(Sender: TObject);
    procedure ExcluiItematual1Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure EdCodAlfChange(Sender: TObject);
    procedure EdCodAlfExit(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCNAE21Cad: TFmCNAE21Cad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, ModuleGeral, CNAE21, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCNAE21Cad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCNAE21Cad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCNAE21CadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCNAE21Cad.DefParams;
begin
  VAR_GOTOTABELA := 'cnae21cad';
  VAR_GOTOMYSQLTABLE := QrCNAE21Cad;
  VAR_GOTONEG := gotoNiP;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := DModG.AllID_DB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT niv.Nome NO_NIVEL, cad.*');
  VAR_SQLx.Add('FROM cnae21cad cad');
  VAR_SQLx.Add('LEFT JOIN cnae21niv niv ON niv.Codigo=cad.Nivel');
  VAR_SQLx.Add('WHERE cad.Codigo <> 0');
  //
  VAR_SQL1.Add('AND cad.Codigo=:P0');
  //
  VAR_SQL2.Add('AND cad.CodTxt=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmCNAE21Cad.EdCodAlfChange(Sender: TObject);
begin
  if EdCodAlf.Focused then
    EdCodTxt.Text := Geral.FormataCNAE21(EdCodAlf.Text);
end;

procedure TFmCNAE21Cad.EdCodAlfExit(Sender: TObject);
begin
  EdCodAlf.Text := Geral.SoNumeroELetra_TT(EdCodAlf.Text);
end;

procedure TFmCNAE21Cad.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    if ImgTipo.SQLType = stIns then
      EdCodigo.ValueVariant := DModG.BuscaProximoInteiro_Negativo(
        DModG.AllID_DB, 'cnae21cad', 'Codigo', '', 0);
end;

procedure TFmCNAE21Cad.ExcluiItematual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item selecionado?') = ID_YES then
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrAllUpd, [
    DELETE_FROM + ' cnae21cad',
    'WHERE Codigo=' + Geral.FF0(QrCNAE21CadCodigo.Value),
    '']);
    //
    Va(vpNext);
  end;
end;

procedure TFmCNAE21Cad.ExcluiTODOSitens1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o de TODOS itens?') = ID_YES then
  begin
    UMyMod.ExecutaMySQLQuery1(DModG.QrAllUpd, [
    DELETE_FROM + ' cnae21cad',
    '']);
    //
    QrCNAE21Cad.Close;
    Va(vpLast);
  end;
end;

procedure TFmCNAE21Cad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCNAE21Cad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCNAE21Cad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCNAE21Cad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCNAE21Cad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCNAE21Cad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCNAE21Cad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCNAE21Cad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAE21Cad.BtAlteraClick(Sender: TObject);
begin
  EdCodigo.Enabled := False;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCNAE21Cad, [PnDados],
  [PnEdita], EdNivel, ImgTipo, 'cnae21cad');
end;

procedure TFmCNAE21Cad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTROX := QrCNAE21CadCodTxt.Value;
  VAR_CADASTRO  := QrCNAE21CadCodigo.Value;
  Close;
end;

procedure TFmCNAE21Cad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  CodTxt: String;
begin
  CodTxt := EdCodTxt.Text;
  //CodAlf := Geral.SoNumeroELetra_TT(CodTxt);
  if MyObjects.FIC(Length(Geral.SoNumeroELetra_TT(CodTxt)) = 0, EdCodAlf,
    'Defina um c�digo v�lido!') then Exit;
  //
  Codigo := EdCodigo.ValueVariant;
  if MyObjects.FIC(Codigo = 0, EdCodigo,
    'Defina um ID v�lido!') then Exit;
  //

{ N�o pode!!
  Codigo := UMyMod.BPGS1I32('cnae21cad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrCNAE21CadCodigo.Value);
}
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'cnae21cad', Codigo, DModG.QrAllUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCNAE21Cad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, DModG.AllID_DB, 'cnae21cad', 'Codigo');
end;

procedure TFmCNAE21Cad.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmCNAE21Cad.BtImportaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAE21, FmCNAE21, afmoNegarComAviso) then
  begin
    FmCNAE21.ShowModal;
    FmCNAE21.Destroy;
    //
    Va(vpLast);
  end;
end;

procedure TFmCNAE21Cad.BtIncluiClick(Sender: TObject);
begin
  EdCodigo.Enabled := True;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCNAE21Cad, [PnDados],
    [PnEdita], EdNivel, ImgTipo, 'cnae21cad');
  //
  EdCodigo.ValueVariant := DModG.BuscaProximoInteiro_Negativo(
                             DModG.AllID_DB, 'cnae21cad', 'Codigo', '', 0);
end;

procedure TFmCNAE21Cad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrCNAE21Niv, DModG.AllID_DB);
end;

procedure TFmCNAE21Cad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCNAE21CadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCNAE21Cad.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxNFS_TBTER_002_001, [DModG.frxDsDono, frxDsAll]);
  MyObjects.frxMostra(frxNFS_TBTER_002_001, 'Lista CNAE');
end;

procedure TFmCNAE21Cad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCNAE21Cad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCNAE21CadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCNAE21Cad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCNAE21Cad.QrCNAE21CadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCNAE21Cad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAE21Cad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCNAE21CadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cnae21cad', DModG.AllID_DB, CO_VAZIO));
end;

procedure TFmCNAE21Cad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAE21Cad.QrCNAE21CadBeforeOpen(DataSet: TDataSet);
begin
  QrCNAE21CadCodigo.DisplayFormat := FFormatFloat;
end;

end.

