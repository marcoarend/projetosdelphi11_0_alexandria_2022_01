unit CFOP2003;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkMemo, Grids, ComCtrls,
  Menus, DBGrids, dmkDBGrid, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkDBGridZTO;

type
  TFmCFOP2003 = class(TForm)
    PainelDados: TPanel;
    DsCFOP2003: TDataSource;
    QrCFOP2003: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    QrCFOP2003Codigo: TWideStringField;
    QrCFOP2003Nome: TWideStringField;
    QrCFOP2003Descricao: TWideMemoField;
    QrCFOP2003Complementacao: TWideMemoField;
    Label7: TLabel;
    MeDescricao: TdmkMemo;
    Panel7: TPanel;
    Label5: TLabel;
    MeComplementacao: TdmkMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCab: TBitBtn;
    BtItens: TBitBtn;
    BtImporta: TBitBtn;
    PnCFOPCFOP: TPanel;
    Splitter2: TSplitter;
    PMCab: TPopupMenu;
    IncluiCFOP1: TMenuItem;
    AlteraCFOPatual1: TMenuItem;
    ExcluiCFOPatual1: TMenuItem;
    ExcluioitemAtual2: TMenuItem;
    ExcluiositensPesquisados2: TMenuItem;
    ExcluiTodositenscadastrados1: TMenuItem;
    PMItens: TPopupMenu;
    Incluiitemdeentradaxsada1: TMenuItem;
    Alteraitemdeentradaxsada1: TMenuItem;
    Excluiitemdeentradaxsada1: TMenuItem;
    QrInn: TmySQLQuery;
    DsInn: TDataSource;
    NCFOP: TIntegerField;
    QrInnCFOP: TWideStringField;
    QrInnNome: TWideStringField;
    QrOut: TmySQLQuery;
    DsOut: TDataSource;
    PnInn: TPanel;
    LaInn: TLabel;
    PnOut: TPanel;
    LaOut: TLabel;
    DBGCFOPOut: TdmkDBGridZTO;
    DBGCFOPInn: TdmkDBGridZTO;
    QrOutCodigo: TIntegerField;
    QrOutCFOP: TWideStringField;
    QrOutNome: TWideStringField;
    Panel4: TPanel;
    PainelData: TPanel;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    Panel6: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Panel9: TPanel;
    Label4: TLabel;
    Splitter1: TSplitter;
    DBGPesq: TdmkDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCFOP2003AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure IncluiCFOP1Click(Sender: TObject);
    procedure AlteraCFOPatual1Click(Sender: TObject);
    procedure ExcluioitemAtual2Click(Sender: TObject);
    procedure ExcluiositensPesquisados2Click(Sender: TObject);
    procedure ExcluiTodositenscadastrados1Click(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure Incluiitemdeentradaxsada1Click(Sender: TObject);
    procedure Alteraitemdeentradaxsada1Click(Sender: TObject);
    procedure Excluiitemdeentradaxsada1Click(Sender: TObject);
    procedure QrCFOP2003AfterScroll(DataSet: TDataSet);
    procedure DBGCFOPOutEnter(Sender: TObject);
    procedure DBGCFOPInnEnter(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCodigoRedefinido(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    //
    procedure MostraFormCFOPCFOP(SQLType: TSQLType);
    procedure ReopenInn();
    procedure ReopenOut();
    procedure ReopenXXX(Query: TmySQLQuery; FldShow, FldPesq: String);
  public
    { Public declarations }
  end;

var
  FmCFOP2003: TFmCFOP2003;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CFOP2003Load, MyDBCheck, CFOPCFOP,
{$IfNDef _tmp_EhLgistic}
NFe_PF, UnFiscal_PF, {$EndIf}
DmkDAC_PF;

{$R *.DFM}

procedure TFmCFOP2003.DefParams;
begin
  VAR_GOTOTABELA := 'CFOP2003';
  VAR_GOTOMYSQLTABLE := QrCFOP2003;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_DESCRICAO;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cfop2003');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo LIKE :P0');
  //
  VAR_SQLa.Add('AND Descricao LIKE :P0');
  //
  LaRegistro.Caption := GOTOy.VaiNumTxt(vpLast, '1.000', CO_NUMERICA);
end;

procedure TFmCFOP2003.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  CFOP, Nome: String;
begin
  if Key = VK_F4 then
  begin
    CFOP := '';
{$IfNDef _tmp_EhLgistic}
    if Fiscal_PF.BuscaCFOP_NatOp(CFOP, Nome) then
    begin
      EdCodigo.Text := CFOP;
      EdNome.Text := Nome;
    end;
{$EndIf}
  end;
end;

procedure TFmCFOP2003.EdCodigoRedefinido(Sender: TObject);
begin
  EdCodigo.Text := Geral.FormataCFOP(EdCodigo.Text);
end;

procedure TFmCFOP2003.Excluiitemdeentradaxsada1Click(Sender: TObject);
var
  Controle: Integer;
begin
(*
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrCadastro_Com_Itens_ITSControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCadastro_Com_Itens_ITS,
      QrCadastro_Com_Itens_ITSControle, QrCadastro_Com_Itens_ITSControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
*)
end;

procedure TFmCFOP2003.ExcluioitemAtual2Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroTxt1('Confirma a exclus�o do CFOP selecionado?',
    'CFOP2003', 'Codigo', QrCFOP2003Codigo.Value, '', Dmod.MyDB);
  QrCFOP2003.Close;
end;

procedure TFmCFOP2003.ExcluiositensPesquisados2Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a exclus�o dos ' + IntToStr(
  QrCFOP2003.RecordCount) + ' CFOPs selecionados?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrCFOP2003.First;
    while not QrCFOP2003.Eof do
    begin
      UMyMod.ExcluiRegistroTxt1('', 'CFOP2003', 'Codigo',
        QrCFOP2003Codigo.Value, '', Dmod.MyDB);
      QrCFOP2003.Next;
    end;
    QrCFOP2003.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCFOP2003.ExcluiTodositenscadastrados1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar(
  'Confirma a exclus�o de TODOS CFOPs CADASTRADOS?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cfop2003');
    Dmod.QrUpd.ExecSQL;
    QrCFOP2003.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCFOP2003.MostraFormCFOPCFOP(SQLType: TSQLType);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFe_PF.MostraFormCFOPCFOP(SQLType, QrCFOP2003, DsCFOP2003, QrInn, QrOut,
  QrOutCodigo.Value, QrOutNome.Value, QrCFOP2003Codigo.Value, QrOutCFOP.Value);
{$EndIf}
end;

procedure TFmCFOP2003.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCFOP2003.PMItensPopup(Sender: TObject);
var
  Hab: Boolean;
  Its: Integer;
begin
  Hab := QrCFOP2003.State <> dsInactive;
  Its := QrCFOP2003.RecordCount;
  Excluioitematual2.Enabled := Hab and (Its > 0);
  Excluiositenspesquisados2.Caption := '&Exclui os itens &Pesquisados (' +
  Geral.FF0(QrCFOP2003.RecordCount) + ')';
  Excluiositenspesquisados2.Enabled := Hab and (Its > 1);
end;

procedure TFmCFOP2003.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
end;

procedure TFmCFOP2003.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCFOP2003.ReopenInn();
begin
  ReopenXXX(QrInn, 'CFOP_OUT', 'CFOP_INN');
end;

procedure TFmCFOP2003.ReopenOut();
begin
  ReopenXXX(QrOut, 'CFOP_INN', 'CFOP_OUT');
end;

procedure TFmCFOP2003.ReopenXXX(Query: TmySQLQuery; FldShow, FldPesq: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT its.Codigo, ' + FldShow + ' CFOP, its.Nome ',
  'FROM cfopcfop its',
  'WHERE its.' + FldPesq + '="' + QrCFOP2003Codigo.Value + '"',
  '']);
end;

procedure TFmCFOP2003.DBGCFOPInnEnter(Sender: TObject);
begin
  BtItens.Enabled := False;
end;

procedure TFmCFOP2003.DBGCFOPOutEnter(Sender: TObject);
begin
  BtItens.Enabled := True;
end;

procedure TFmCFOP2003.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCFOP2003.SpeedButton1Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpFirst, QrCFOP2003Codigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCFOP2003.SpeedButton2Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpPrior, QrCFOP2003Codigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCFOP2003.SpeedButton3Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpNext, QrCFOP2003Codigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCFOP2003.SpeedButton4Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpLast, QrCFOP2003Codigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCFOP2003.AlteraCFOPatual1Click(Sender: TObject);
var
  Codigo: String;
begin
  Codigo := Geral.FormataCFOP(QrCFOP2003Codigo.Value);
  if QrCFOP2003Codigo.Value <> Codigo then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cfop2003', False, [
    'Codigo'], ['Codigo'], [
    Codigo], [QrCFOP2003Codigo.Value], True) then ;
  end;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCFOP2003, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'CFOP2003');
end;

procedure TFmCFOP2003.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCFOP2003.BtSaidaClick(Sender: TObject);
begin
  VAR_CADTEXTO := QrCFOP2003Codigo.Value;
  Close;
end;

procedure TFmCFOP2003.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  VAR_CFOP2003 := EdCodigo.Text;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmCFOP2003, PainelEdita,
    'CFOP2003', VAR_CFOP2003, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo,
    True) then
  begin
    GOTOy.LocalizaNumTxt(VAR_CFOP2003, VAR_CFOP2003);
  end;
end;

procedure TFmCFOP2003.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CFOP2003', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CFOP2003', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CFOP2003', 'Codigo');
end;

procedure TFmCFOP2003.BtImportaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCFOP2003Load, FmCFOP2003Load, afmoNegarComAviso) then
  begin
    FmCFOP2003Load.ShowModal;
    FmCFOP2003Load.Destroy;
  end;
end;

procedure TFmCFOP2003.Alteraitemdeentradaxsada1Click(Sender: TObject);
begin
  MostraFormCFOPCFOP(stUpd);
end;

procedure TFmCFOP2003.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmCFOP2003.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType        := stLok;
  PnCFOPCFOP.Align       := alClient;
  MeComplementacao.Align := alClient;
  DBGCFOPInn.Align       := alClient;
  DBGCFOPOut.Align       := alClient;
  PnInn.Align            := alClient;
  CriaOForm;
end;

procedure TFmCFOP2003.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodigoTxt(QrCFOP2003Codigo.Value);
end;

procedure TFmCFOP2003.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCFOP2003.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCFOP2003.SbNovoClick(Sender: TObject);
begin
  //
end;

procedure TFmCFOP2003.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCFOP2003.QrCFOP2003AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCFOP2003.QrCFOP2003AfterScroll(DataSet: TDataSet);
begin
  ReopenInn();
  ReopenOut();
end;

procedure TFmCFOP2003.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCFOP2003.SbQueryClick(Sender: TObject);
begin
  GOTOy.LocalizaNumTxt(QrCFOP2003Codigo.Value,
  CuringaLoc.CriaForm4(CO_CODIGO, CO_NOME, 'CFOP2003', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCFOP2003.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCFOP2003.IncluiCFOP1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrCFOP2003, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'CFOP2003');
end;

procedure TFmCFOP2003.Incluiitemdeentradaxsada1Click(Sender: TObject);
begin
  MostraFormCFOPCFOP(stIns);
end;

end.

