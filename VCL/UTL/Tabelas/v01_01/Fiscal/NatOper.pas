unit NatOper;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkMemo, Grids, ComCtrls,
  Menus, DBGrids, dmkDBGrid, dmkCheckBox, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmNatOper = class(TForm)
    PainelDados: TPanel;
    DsNatOper: TDataSource;
    QrNatOper: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Panel4: TPanel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    PMExclui: TPopupMenu;
    Excluioitematual1: TMenuItem;
    Excluiositenspesquisados1: TMenuItem;
    ExcluiTodositens1: TMenuItem;
    DBGPesq: TdmkDBGrid;
    Splitter1: TSplitter;
    QrNatOperCodigo: TWideStringField;
    QrNatOperNome: TWideStringField;
    QrNatOperICMS: TIntegerField;
    QrNatOperIPI: TIntegerField;
    QrNatOperEspecial: TIntegerField;
    QrNatOperGeraCR: TIntegerField;
    QrNatOperCodEnt: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    CkICMS: TdmkCheckBox;
    CkIPI: TdmkCheckBox;
    CkEspecial: TdmkCheckBox;
    CkGeraCR: TdmkCheckBox;
    EdCodEnt: TdmkEdit;
    Label3: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtImporta: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNatOperAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure Excluioitematual1Click(Sender: TObject);
    procedure Excluiositenspesquisados1Click(Sender: TObject);
    procedure ExcluiTodositens1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
  public
    { Public declarations }
  end;

var
  FmNatOper: TFmNatOper;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck;

{$R *.DFM}

procedure TFmNatOper.DefParams;
begin
  VAR_GOTOTABELA := 'NatOper';
  VAR_GOTOMYSQLTABLE := QrNatOper;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_DESCRICAO;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM NatOper');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo LIKE :P0');
  //
  VAR_SQLa.Add('AND Descricao LIKE :P0');
  //
  LaRegistro.Caption := GOTOy.VaiNumTxt(vpLast, '1.000', CO_NUMERICA);
end;

procedure TFmNatOper.Excluioitematual1Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroTxt1('Confirma a exclus�o do CFOP selecionado?',
    'NatOper', 'Codigo', QrNatOperCodigo.Value, '', Dmod.MyDB);
  QrNatOper.Close;
end;

procedure TFmNatOper.Excluiositenspesquisados1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a exclus�o dos ' + IntToStr(
  QrNatOper.RecordCount) + ' CFOPs selecionados?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrNatOper.First;
    while not QrNatOper.Eof do
    begin
      UMyMod.ExcluiRegistroTxt1('', 'NatOper', 'Codigo',
        QrNatOperCodigo.Value, '', Dmod.MyDB);
      QrNatOper.Next;
    end;
    QrNatOper.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNatOper.ExcluiTodositens1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar(
  'Confirma a exclus�o de TODOS CFOPs CADASTRADOS?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM NatOper');
    Dmod.QrUpd.ExecSQL;
    QrNatOper.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNatOper.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNatOper.PMExcluiPopup(Sender: TObject);
var
  Hab: Boolean;
  Its: Integer;
begin
  Hab := QrNatOper.State <> dsInactive;
  Its := QrNatOper.RecordCount;
  Excluioitematual1.Enabled := Hab and (Its > 0);
  Excluiositenspesquisados1.Caption := '&Exclui os itens &Pesquisados (' +
  intToStr(QrNatOper.RecordCount) + ')';
  Excluiositenspesquisados1.Enabled := Hab and (Its > 1);
end;

procedure TFmNatOper.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
end;

procedure TFmNatOper.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNatOper.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNatOper.SpeedButton1Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpFirst, QrNatOperCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNatOper.SpeedButton2Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpPrior, QrNatOperCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNatOper.SpeedButton3Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpNext, QrNatOperCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNatOper.SpeedButton4Click(Sender: TObject);
begin
  LaRegistro.Caption :=
  GOTOy.VaiNumTxt(vpLast, QrNatOperCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNatOper.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNatOper, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'NatOper');
end;

procedure TFmNatOper.BtSaidaClick(Sender: TObject);
begin
  VAR_CADTEXTO := QrNatOperCodigo.Value;
  Close;
end;

procedure TFmNatOper.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  VAR_CFOP2003 := EdCodigo.Text;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNatOper, PainelEdit,
    'NatOper', VAR_CFOP2003, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo,
    True) then
  begin
    GOTOy.LocalizaNumTxt(VAR_CFOP2003, VAR_CFOP2003);
  end;
end;

procedure TFmNatOper.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NatOper', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NatOper', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NatOper', 'Codigo');
end;

procedure TFmNatOper.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmNatOper.BtIncluiClick(Sender: TObject);
var
  N: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNatOper, [PainelDados],
  [PainelEdita], EdCodigo, ImgTipo, 'NatOper');
  if (VAR_CADTEXTO <> '') then
  begin
    EdCodigo.Text := VAR_CADTEXTO;
    N := Geral.IMV(VAR_CADTEXTO[1]);
    if N > 4 then
    begin
      N := N - 4;
      EdCodEnt.Text := IntToStr(N) + Copy(VAR_CADTEXTO, 2);
    end;
    // erro quando chama de fora
    //EdNome.SetFocus;
  end;
  VAR_CADTEXTO := '';
end;

procedure TFmNatOper.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  // N�o habilitar!
  ///VAR_CADTEXTO := '';
  PainelData.Align := alClient;
  CriaOForm;
end;

procedure TFmNatOper.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodigoTxt(QrNatOperCodigo.Value);
end;

procedure TFmNatOper.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNatOper.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNatOper.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmNatOper.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNatOper.QrNatOperAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNatOper.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNatOper.SbQueryClick(Sender: TObject);
begin
  GOTOy.LocalizaNumTxt(QrNatOperCodigo.Value,
  CuringaLoc.CriaForm4(CO_CODIGO, CO_NOME, 'NatOper', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNatOper.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

