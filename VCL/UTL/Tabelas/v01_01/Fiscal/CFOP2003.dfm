object FmCFOP2003: TFmCFOP2003
  Left = 368
  Top = 194
  Caption = 'FIS-CFOP3-001 :: Cadastro de CFOP 2003'
  ClientHeight = 541
  ClientWidth = 1022
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1022
    Height = 445
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1022
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo [F4]:'
      end
      object Label9: TLabel
        Left = 92
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label7: TLabel
        Left = 8
        Top = 44
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdNome: TdmkEdit
        Left = 96
        Top = 20
        Width = 909
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
        OnKeyDown = EdCodigoKeyDown
        OnRedefinido = EdCodigoRedefinido
      end
    end
    object MeDescricao: TdmkMemo
      Left = 0
      Top = 60
      Width = 1022
      Height = 89
      Align = alTop
      TabOrder = 1
      QryCampo = 'Descricao'
      UpdCampo = 'Descricao'
      UpdType = utYes
    end
    object Panel7: TPanel
      Left = 0
      Top = 149
      Width = 1022
      Height = 23
      Align = alTop
      TabOrder = 2
      object Label5: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'Complementa'#231#227'o:'
      end
    end
    object MeComplementacao: TdmkMemo
      Left = 0
      Top = 172
      Width = 1022
      Height = 72
      Align = alTop
      TabOrder = 3
      QryCampo = 'Complementacao'
      UpdCampo = 'Complementacao'
      UpdType = utYes
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 382
      Width = 1022
      Height = 63
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 912
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1022
    Height = 445
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 381
      Width = 1022
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 44
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 2
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 152
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 326
        Top = 15
        Width = 694
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 561
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtItens: TBitBtn
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens S>E'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtImporta: TBitBtn
          Tag = 19
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtImportaClick
        end
      end
    end
    object PnCFOPCFOP: TPanel
      Left = 0
      Top = 201
      Width = 1022
      Height = 112
      Align = alTop
      TabOrder = 2
      object Splitter2: TSplitter
        Left = 521
        Top = 1
        Height = 110
      end
      object PnInn: TPanel
        Left = 552
        Top = 1
        Width = 469
        Height = 110
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object LaInn: TLabel
          Left = 0
          Top = 0
          Width = 469
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = 'CFOPs de entrada que usam este como sa'#237'da'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 260
        end
        object DBGCFOPInn: TdmkDBGridZTO
          Left = 43
          Top = 26
          Width = 320
          Height = 120
          DataSource = DsInn
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnEnter = DBGCFOPInnEnter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CFOP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end>
        end
      end
      object PnOut: TPanel
        Left = 1
        Top = 1
        Width = 520
        Height = 110
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object LaOut: TLabel
          Left = 0
          Top = 0
          Width = 520
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = 'CFOPs de sa'#237'da que usam este como entrada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitWidth = 260
        end
        object DBGCFOPOut: TdmkDBGridZTO
          Left = 0
          Top = 13
          Width = 484
          Height = 97
          Align = alLeft
          DataSource = DsOut
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnEnter = DBGCFOPOutEnter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CFOP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end>
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1022
      Height = 201
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 746
        Top = 0
        Width = 5
        Height = 201
        Align = alRight
      end
      object PainelData: TPanel
        Left = 0
        Top = 0
        Width = 746
        Height = 201
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object DBMemo1: TDBMemo
          Left = 0
          Top = 124
          Width = 746
          Height = 77
          Align = alClient
          DataField = 'Complementacao'
          DataSource = DsCFOP2003
          TabOrder = 0
        end
        object DBMemo2: TDBMemo
          Left = 0
          Top = 60
          Width = 746
          Height = 41
          Align = alTop
          DataField = 'Descricao'
          DataSource = DsCFOP2003
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 746
          Height = 60
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 92
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdNome
          end
          object Label1: TLabel
            Left = 8
            Top = 44
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            DataField = 'Codigo'
            DataSource = DsCFOP2003
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object DBEdNome: TDBEdit
            Left = 92
            Top = 20
            Width = 649
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'Nome'
            DataSource = DsCFOP2003
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 33
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 101
          Width = 746
          Height = 23
          Align = alTop
          TabOrder = 3
          object Label4: TLabel
            Left = 8
            Top = 4
            Width = 85
            Height = 13
            Caption = 'Complementa'#231#227'o:'
            FocusControl = DBMemo1
          end
        end
      end
      object DBGPesq: TdmkDBGrid
        Left = 751
        Top = 0
        Width = 271
        Height = 201
        Align = alRight
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCFOP2003
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1022
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 974
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 758
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 300
        Height = 32
        Caption = 'Cadastro de CFOP 2003'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 300
        Height = 32
        Caption = 'Cadastro de CFOP 2003'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 300
        Height = 32
        Caption = 'Cadastro de CFOP 2003'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1022
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1018
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCFOP2003: TDataSource
    DataSet = QrCFOP2003
    Left = 184
    Top = 196
  end
  object QrCFOP2003: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCFOP2003AfterOpen
    AfterScroll = QrCFOP2003AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM cfop2003')
    Left = 184
    Top = 148
    object QrCFOP2003Codigo: TWideStringField
      FieldName = 'Codigo'
      Origin = 'cfop2003.Codigo'
      Size = 5
    end
    object QrCFOP2003Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cfop2003.Nome'
      Size = 255
    end
    object QrCFOP2003Descricao: TWideMemoField
      FieldName = 'Descricao'
      Origin = 'cfop2003.Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOP2003Complementacao: TWideMemoField
      FieldName = 'Complementacao'
      Origin = 'cfop2003.Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    CanUpd01 = BtItens
    CanDel01 = BtSaida
    Left = 68
    Top = 12
  end
  object PMCab: TPopupMenu
    Left = 360
    Top = 436
    object IncluiCFOP1: TMenuItem
      Caption = '&Inclui CFOP'
      OnClick = IncluiCFOP1Click
    end
    object AlteraCFOPatual1: TMenuItem
      Caption = '&Altera CFOP atual'
      OnClick = AlteraCFOPatual1Click
    end
    object ExcluiCFOPatual1: TMenuItem
      Caption = '&Exclui CFOP atual'
      object ExcluioitemAtual2: TMenuItem
        Caption = 'Exclui o item &Atual'
        OnClick = ExcluioitemAtual2Click
      end
      object ExcluiositensPesquisados2: TMenuItem
        Caption = '&Exclui os itens &Pesquisados (??)'
        OnClick = ExcluiositensPesquisados2Click
      end
      object ExcluiTodositenscadastrados1: TMenuItem
        Caption = 'Exclui &Todos itens cadastrados'
        OnClick = ExcluiTodositenscadastrados1Click
      end
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 464
    Top = 440
    object Incluiitemdeentradaxsada1: TMenuItem
      Caption = '&Inclui item de entrada x sa'#237'da'
      OnClick = Incluiitemdeentradaxsada1Click
    end
    object Alteraitemdeentradaxsada1: TMenuItem
      Caption = '&Altera item de entrada x sa'#237'da'
      OnClick = Alteraitemdeentradaxsada1Click
    end
    object Excluiitemdeentradaxsada1: TMenuItem
      Caption = '&Exclui item de entrada x sa'#237'da'
      OnClick = Excluiitemdeentradaxsada1Click
    end
  end
  object QrInn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 584
    Top = 168
    object NCFOP: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInnCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrInnNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsInn: TDataSource
    DataSet = QrInn
    Left = 584
    Top = 220
  end
  object QrOut: TMySQLQuery
    Database = Dmod.MyDB
    Left = 640
    Top = 168
    object QrOutCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOutCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrOutNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsOut: TDataSource
    DataSet = QrOut
    Left = 640
    Top = 220
  end
end
