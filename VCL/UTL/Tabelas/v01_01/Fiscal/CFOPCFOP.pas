unit CFOPCFOP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  SPED_Listas, dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCFOPCFOP = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label2: TLabel;
    EdCFOP_OUT: TdmkEditCB;
    CBCFOP_OUT: TdmkDBLookupComboBox;
    SbCFOP_OUT: TSpeedButton;
    QrInn: TmySQLQuery;
    QrInnCodigo: TWideStringField;
    QrInnNome: TWideStringField;
    DsInn: TDataSource;
    QrOut: TmySQLQuery;
    QrOutCodigo: TWideStringField;
    QrOutNome: TWideStringField;
    DsOut: TDataSource;
    EdCFOP_INN: TdmkEditCB;
    CBCFOP_INN: TdmkDBLookupComboBox;
    SbCFOP_INN: TSpeedButton;
    SBNewCFOP_Out: TSpeedButton;
    SBNewCFOP_Inn: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCFOP_OUTClick(Sender: TObject);
    procedure SbCFOP_INNClick(Sender: TObject);
    procedure SBNewCFOP_OutClick(Sender: TObject);
    procedure SBNewCFOP_InnClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
    procedure CadastraCFOP(Ed: TdmkEditCB; CB: TdmkDBLookupComboBox; Qr:
              TmySQLQuery);
    procedure CadShowCFOP(Query: TmySQLQuery; Ed: TdmkEditCB; CB:
              TdmkDBLookupComboBox);
  public
    { Public declarations }
    FQrCab, FQrInn, FQrOut: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCFOPCFOP: TFmCFOPCFOP;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck,
{$IfNDef _tmp_EhLgistic} UnGrade_Jan, UnFiscal_PF, {$EndIf}
DmkDAC_PF;

{$R *.DFM}

procedure TFmCFOPCFOP.BtOKClick(Sender: TObject);
var
  Nome, CFOP_INN, CFOP_OUT: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  CFOP_INN       := EdCFOP_INN.ValueVariant;
  CFOP_OUT       := EdCFOP_OUT.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  if MyObjects.FIC(Trim(CFOP_OUT) = '', EdCFOP_OUT, 'Informe o CFOP de sa�da!') then
    Exit;
  if MyObjects.FIC(Trim(CFOP_INN) = '', EdCFOP_INN, 'Informe o CFOP de entrada!') then
    Exit;
  if MyObjects.FIC(Trim(CFOP_INN) = Trim(CFOP_OUT), nil, 'O CFOP de entrada e sa�da n�o pode ser o mesmo!') then
    Exit;
  Codigo := UMyMod.BPGS1I32('cfopcfop', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cfopcfop', False, [
  'CFOP_INN', 'CFOP_OUT', 'Nome'], [
  'Codigo'], [
  CFOP_INN, CFOP_OUT, Nome], [
  Codigo], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmCFOPCFOP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCFOPCFOP.CadastraCFOP(Ed: TdmkEditCB; CB: TdmkDBLookupComboBox;
  Qr: TmySQLQuery);
var
  CFOP, Codigo, Nome, Descricao: String;
  Qry: TmySQLQuery;
begin
  CFOP := Ed.ValueVariant;
  if CFOP <> '' then
  begin
    CFOP := Geral.FormataCFOP(CFOP);
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM cfop2003 ',
      'WHERE REPLACE(Codigo, ".", "")="' + Geral.SoNumero_TT(CFOP) + '" ',
      '']);
      if MyObjects.FIC(Qry.RecordCount > 0, nil, 'CFOP j� cadastrado!') then
        Exit;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
      'WHERE REPLACE(CodTxt, ".", "")="' + Geral.SoNumero_TT(CFOP) + '" ',
      '']);
      if MyObjects.FIC(Qry.RecordCount = 0, nil,
      'CFOP n�o localizado na tabela ' + CO_NOME_TbSPEDEFD_CFOP + '!') then
        Exit else
      begin
        Codigo := Geral.FormataCFOP(Qry.FieldByName('CodTxt').AsString);
        Descricao := Qry.FieldByName('Nome').AsString;
        Nome := Copy(Descricao, 1, 255);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cfop2003', False, [
        'Nome', 'Descricao'], [
        'Codigo'], [
        Nome, Descricao], [
        Codigo], True) then
        begin
          UnDMkDAC_PF.AbreQuery(Qr, Dmod.MyDB);
          Ed.ValueVariant := Codigo;
          CB.KeyValue     := Codigo;
        end;
      end;
    finally
      Qry.Free;
    end;

  end;
end;

procedure TFmCFOPCFOP.CadShowCFOP(Query: TmySQLQuery; Ed: TdmkEditCB; CB:
  TdmkDBLookupComboBox);
var
  CFOP, Nome: String;
  //
  function InsereCFOP2003(): Boolean;
  var
    Codigo, Nome, Descricao, Complementacao: String;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := Geral.FormataCFOP(CFOP);
    //Nome           := ;
    Descricao      := '';
    Complementacao := '';
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cfop2003', False, [
    'Nome', 'Descricao', 'Complementacao'], [
    'Codigo'], [
    Nome, Descricao, Complementacao], [
    Codigo], True);
  end;
var
  Qry: TmySQLQuery;
begin
{$IfNDef _tmp_EhLgistic}
  CFOP := '';
  if Fiscal_PF.BuscaCFOP_NatOp(CFOP, Nome) then
  begin
    EdCodigo.Text := CFOP;
    EdNome.Text := Nome;
    Qry := TmySQLQuery.Create(Dmod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM CFOP2003 ',
    'WHERE REPLACE(Codigo, ".", "")="' + Geral.SoNumero_TT(CFOP) + '" ',
    '']);
    if Qry.RecordCount = 0 then
      InsereCFOP2003();
  end;
  if CFOP <> '' then
  begin
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    Ed.ValueVariant := CFOP;
    CB.KeyValue     := CFOP;
  end;
{$EndIf}
end;

procedure TFmCFOPCFOP.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmCFOPCFOP.FormCreate(Sender: TObject);
  procedure AbreCFOP(Query: TmySQLQuery);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM CFOP2003 ',
    'ORDER BY Nome ',
    '']);
  end;
begin
  ImgTipo.SQLType := stLok;
  //
  AbreCFOP(QrInn);
  AbreCFOP(QrOut);
end;

procedure TFmCFOPCFOP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCFOPCFOP.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrInn <> nil then
    UnDmkDAC_PF.AbreQuery(FQrInn, Dmod.MyDB);
    //
  if FQrOut <> nil then
    UnDmkDAC_PF.AbreQuery(FQrOut, Dmod.MyDB);
    //
end;

procedure TFmCFOPCFOP.SbCFOP_INNClick(Sender: TObject);
begin
  CadShowCFOP(QrInn, EdCFOP_INN, CBCFOP_INN);
end;

procedure TFmCFOPCFOP.SbCFOP_OUTClick(Sender: TObject);
begin
  CadShowCFOP(QrOut, EdCFOP_OUT, CBCFOP_OUT);
end;

procedure TFmCFOPCFOP.SBNewCFOP_InnClick(Sender: TObject);
begin
  CadastraCFOP(EdCFOP_OUT, CBCFOP_OUT, QrOut);
end;

procedure TFmCFOPCFOP.SBNewCFOP_OutClick(Sender: TObject);
begin
  CadastraCFOP(EdCFOP_OUT, CBCFOP_OUT, QrOut);
end;

end.
