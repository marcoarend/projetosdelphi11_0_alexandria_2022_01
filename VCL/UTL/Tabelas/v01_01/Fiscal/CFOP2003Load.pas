unit CFOP2003Load;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkCheckGroup, dmkCheckBox, ComCtrls,
  Grids, ComObj, Variants, Math, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmCFOP2003Load = class(TForm)
    Panel1: TPanel;
    CGGrupos: TdmkCheckGroup;
    CkOutros: TdmkCheckBox;
    Panel3: TPanel;
    GradeB: TStringGrid;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    BtAbreArq: TBitBtn;
    BtCarrega: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    GradeA: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAbreArqClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
  private
    { Private declarations }
    function Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
  public
    { Public declarations }
  end;

  var
  FmCFOP2003Load: TFmCFOP2003Load;

implementation

uses UnMyObjects, CFOP2003, dmkGeral, UMySQLModule, Module;

{$R *.DFM}

procedure TFmCFOP2003Load.BtAbreArqClick(Sender: TObject);
var
  Arquivo: String;
  I, N, K: Integer;
  Codigo, Nome, Texto: String;
begin
  if MyObjects.FileOpenDialog(Self, ''(*IniDir*), ''(*Arquivo*),
  'Arquivo CFOP 2003', ''(*Filtro*), [](*Opcoes*), Arquivo) then
    Xls_To_StringGrid(GradeB, Arquivo);
  PB1.Position := 0;
  N := (GradeB.RowCount + 2) div 3;
  PB1.Max := N;
  GradeA.RowCount := N + 1;
  //
  GradeA.Cells[0, 0] := 'Seq';
  GradeA.Cells[1, 0] := 'Codigo';
  GradeA.Cells[2, 0] := 'Titulo';
  GradeA.Cells[3, 0] := 'Descri��o';
  GradeA.Align := alClient;
  GradeA.Visible := True;
  GradeB.Visible := False;
  for I := 0 to N (* -1*) do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    K := I * 3;
    Codigo := GradeB.Cells[1, K + 0];
    Nome   := GradeB.Cells[1, K + 1];
    Texto  := GradeB.Cells[1, K + 2];
    //
    K := I + 1;
    GradeA.Cells[0, K] := Geral.FF0(K);
    GradeA.Cells[1, K] := Codigo;
    GradeA.Cells[2, K] := Nome;
    GradeA.Cells[3, K] := Texto;
  end;
end;

procedure TFmCFOP2003Load.BtCarregaClick(Sender: TObject);
var
  I, N: Integer;
  Codigo, Nome, Descricao, Complementacao: String;
  Ck1, Ck2, Ck3, {Ck4,} Ck5, Ck6, Ck7, Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Max := GradeA.RowCount;
  //Itens := 0;
  Ck1 := dmkPF.IntInConjunto2( 1, CGGrupos.Value);
  Ck2 := dmkPF.IntInConjunto2( 2, CGGrupos.Value);
  Ck3 := dmkPF.IntInConjunto2( 4, CGGrupos.Value);
  //Ck4 := dmkPF.IntInConjunto2( 8, CGGrupos.Value);
  Ck5 := dmkPF.IntInConjunto2( 8, CGGrupos.Value);
  Ck6 := dmkPF.IntInConjunto2(16, CGGrupos.Value);
  Ck7 := dmkPF.IntInConjunto2(32, CGGrupos.Value);
  //
  for I := 1 to GradeA.RowCount - 1 do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    Codigo := GradeA.Cells[1,I];
    if Length(Codigo) = 5 then
    begin
      n := Geral.IMV(Codigo[1]);
      case n of
        1: Continua := Ck1;
        2: Continua := Ck2;
        3: Continua := Ck3;
        //4: Continua := Ck4;
        5: Continua := Ck5;
        6: Continua := Ck6;
        7: Continua := Ck7;
        else Continua := CkOutros.Checked;
      end;
      if Continua then
      begin
        Descricao := GradeA.Cells[2,I];
        Nome := Copy(Descricao, 1, 255);
        Descricao := Nome;
        Complementacao := GradeA.Cells[3,I];
        //
        UMyMod.ExcluiRegistroTxt1('', 'CFOP2003', 'Codigo', Codigo, '', Dmod.MyDB);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cfop2003', False, [
        'Nome', 'Descricao', 'Complementacao'], ['Codigo'], [
        Nome, Descricao, Complementacao], [Codigo], True) then
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando CFOP ' + Codigo);
      end;
    end;
  end;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmCFOP2003Load.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCFOP2003Load.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCFOP2003Load.FormCreate(Sender: TObject);
begin
  CGGrupos.Value := 63;
end;

procedure TFmCFOP2003Load.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmCFOP2003Load.Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
  {
  procedure AtulizaTitulo(var n: Integer; const Titulo: String);
  begin
    inc(n, 1);
    Grade2.ColCount := n + 1;
    Grade2.Cells[n, 0] := Titulo;
  end;
  }
  {
  procedure AtulizaColuna(var n: Integer; const Lin2, Lin1, Coluna: Integer;
    const CharWid: Double; var ColWid: array of Integer);
  var
    i, Item, Larg: Integer;
    s: String;
    Neg: Boolean;
  begin
    if Coluna < 1 then Exit;
    inc(n, 1);
    s := Grade1.Cells[Coluna, Lin1];
    Item := -1;
    for i := 0 to FTits-1 do
      if FTitulos[i] = Grade2.Cells[n, 0] then Item := i;
    if Item in ([6,7,8,10]) then
    begin
      if Length(s) > 0 then
      begin
        Neg := (Uppercase(s[1]) = 'D');
        if not Neg then Neg := (Uppercase(s[Length(s)]) = 'D');
      end else Neg := False;
      s := ???.SoNumeroValor_TT(s);
      if Length(s) > 0 then
      begin
        if (s[Length(s)] = '-') then
          s := '-' + Copy(s, 1, Length(s)-1)
        else if Neg then s := '-' + s;
      end;
      s := Geral.TFT(s, 2, siNegativo);
    end else if Item = 0 then
    begin
      s := ???.SoNumeroEExtra_TT(s, '/', True);
      s := Geral.FDT(Geral.ValidaDataSimples(Trim(s), True), 2);
    end;
    if s = '0,00' then s := '';
    Grade2.Cells[n, Lin2] := s;
    Larg := Length(s);
    if Larg > 2 then
    begin
      Larg := Round(Larg * CharWid);
      if Larg > ColWid[n] then ColWid[n] := Larg;
    end;
  end;
  function EliminaLinhaVazia: Boolean;
  var
    i: Integer;
  begin
    if Grade2.RowCount = 2 then
    begin
      Result := False;
      Exit;
    end;
    Result := True;
    for i := 1 to Grade2.ColCount -1 do
      if Trim(Grade2.Cells[i, Grade2.RowCount -1]) <> '' then Result := False
  end;
  }
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, m, n: Integer;
  //XLSData, XLSComp, XLSCPMF, XLSHist, XLSDocu, XLSHiDO, XLSCred, XLSDebi,
  //XLSDouC, XLSCrDb, XLSSldo: Integer;
  ColWid: array[0..255] of Integer;
  Texto, CFOP: String;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  Result := False;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    PB1.Max := x * y;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Lendo arquivo excel');
    Update;
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    k := 1; // 1? 2?
    m := Round(CharWid * 3);
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Update;
      Application.ProcessMessages;
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        //
        Texto := RangeMatrix[k, r];
        // Formata n�mero de CFOP
        if (r = 1) and (k>1)  then
        begin
          CFOP := Geral.SoNumero_TT(Texto);
          if (CFOP = Texto) and (Length(Texto) = 4) then
            Texto := Texto[1] + '.' + Texto[2] + Texto[3] + Texto[4];
        end;
        AGrid.Cells[r, (k - 1)] := Texto;
        //Larg := Round((Length(Texto) + 1) * CharWid);
        //if Larg > ColWid[r] then
          //ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    RangeMatrix := Unassigned;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Arquivo excel lido com sucesso!');
    PB1.Position := PB1.Max;
    BtCarrega.Enabled := True;
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;

end.
