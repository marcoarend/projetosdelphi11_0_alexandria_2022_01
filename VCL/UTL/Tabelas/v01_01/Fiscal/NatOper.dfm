object FmNatOper: TFmNatOper
  Left = 368
  Top = 194
  Caption = 'FIS-CFOP3-001 :: Configura'#231#227'o de CFOP'
  ClientHeight = 315
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 219
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 80
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label9: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 336
        Top = 48
        Width = 70
        Height = 13
        Caption = 'CFOP entrada:'
      end
      object EdNome: TdmkEdit
        Left = 64
        Top = 20
        Width = 408
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 52
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
      end
      object CkICMS: TdmkCheckBox
        Left = 8
        Top = 48
        Width = 49
        Height = 17
        Caption = 'ICMS'
        TabOrder = 2
        QryCampo = 'ICMS'
        UpdCampo = 'ICMS'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkIPI: TdmkCheckBox
        Left = 60
        Top = 48
        Width = 49
        Height = 17
        Caption = 'IPI'
        TabOrder = 3
        QryCampo = 'IPI'
        UpdCampo = 'IPI'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkEspecial: TdmkCheckBox
        Left = 112
        Top = 48
        Width = 65
        Height = 17
        Caption = 'Especial'
        TabOrder = 4
        QryCampo = 'Especial'
        UpdCampo = 'Especial'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkGeraCR: TdmkCheckBox
        Left = 184
        Top = 48
        Width = 69
        Height = 17
        Caption = 'Gera CR'
        TabOrder = 5
        QryCampo = 'GeraCR'
        UpdCampo = 'GeraCR'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdCodEnt: TdmkEdit
        Left = 420
        Top = 44
        Width = 52
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CodEnt'
        UpdCampo = 'CodEnt'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 155
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 636
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 219
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 779
      Top = 81
      Width = 5
      Height = 74
      Align = alRight
      ExplicitLeft = 733
      ExplicitTop = 281
      ExplicitHeight = 44
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 81
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 80
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label2: TLabel
          Left = 64
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label4: TLabel
          Left = 336
          Top = 48
          Width = 70
          Height = 13
          Caption = 'CFOP entrada:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 20
          Width = 52
          Height = 21
          DataField = 'Codigo'
          DataSource = DsNatOper
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 64
          Top = 20
          Width = 408
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsNatOper
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 33
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBCheckBox1: TDBCheckBox
          Left = 8
          Top = 48
          Width = 49
          Height = 17
          Caption = 'ICMS'
          DataField = 'ICMS'
          DataSource = DsNatOper
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 60
          Top = 48
          Width = 49
          Height = 17
          Caption = 'IPI'
          DataField = 'IPI'
          DataSource = DsNatOper
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox3: TDBCheckBox
          Left = 112
          Top = 48
          Width = 65
          Height = 17
          Caption = 'Especial'
          DataField = 'Especial'
          DataSource = DsNatOper
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 184
          Top = 48
          Width = 69
          Height = 17
          Caption = 'Gera CR'
          DataField = 'GeraCR'
          DataSource = DsNatOper
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdit2: TDBEdit
          Left = 420
          Top = 44
          Width = 52
          Height = 21
          DataField = 'CodEnt'
          DataSource = DsNatOper
          TabOrder = 6
        end
      end
    end
    object DBGPesq: TdmkDBGrid
      Left = 508
      Top = 81
      Width = 271
      Height = 74
      Align = alRight
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsNatOper
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 63
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 155
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
        object BtImporta: TBitBtn
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 279
        Height = 32
        Caption = 'Configura'#231#227'o de CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 279
        Height = 32
        Caption = 'Configura'#231#227'o de CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 279
        Height = 32
        Caption = 'Configura'#231#227'o de CFOP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNatOper: TDataSource
    DataSet = QrNatOper
    Left = 40
    Top = 12
  end
  object QrNatOper: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNatOperAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM NatOper')
    Left = 12
    Top = 12
    object QrNatOperCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 6
    end
    object QrNatOperNome: TWideStringField
      FieldName = 'Nome'
      Size = 25
    end
    object QrNatOperICMS: TIntegerField
      FieldName = 'ICMS'
    end
    object QrNatOperIPI: TIntegerField
      FieldName = 'IPI'
    end
    object QrNatOperEspecial: TIntegerField
      FieldName = 'Especial'
    end
    object QrNatOperGeraCR: TIntegerField
      FieldName = 'GeraCR'
    end
    object QrNatOperCodEnt: TWideStringField
      FieldName = 'CodEnt'
      Size = 6
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtImporta
    CanUpd01 = BtExclui
    CanDel01 = BtAltera
    Left = 68
    Top = 12
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 580
    Top = 88
    object Excluioitematual1: TMenuItem
      Caption = 'Exclui o item &Atual'
      OnClick = Excluioitematual1Click
    end
    object Excluiositenspesquisados1: TMenuItem
      Caption = '&Exclui os itens &Pesquisados (??)'
      OnClick = Excluiositenspesquisados1Click
    end
    object ExcluiTodositens1: TMenuItem
      Caption = 'Exclui &Todos itens cadastrados'
      OnClick = ExcluiTodositens1Click
    end
  end
end
