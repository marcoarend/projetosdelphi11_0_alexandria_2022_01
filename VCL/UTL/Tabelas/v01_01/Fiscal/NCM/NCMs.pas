unit NCMs;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkCheckBox, dmkImage,
  UnDmkEnums;

type
  TFmNCMs = class(TForm)
    PainelDados: TPanel;
    DsNCMs: TDataSource;
    QrNCMs: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdLetras: TdmkEdit;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    EdNCM: TdmkEdit;
    QrNCMsCodigo: TIntegerField;
    QrNCMsNCM: TWideStringField;
    QrNCMsNome: TWideStringField;
    QrNCMsLetras: TWideStringField;
    QrNCMsNCM_TXT: TWideStringField;
    QrNCMsImpNaLista: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    CkImpNaLista: TdmkCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNCMsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNCMsBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNCMsCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmNCMs: TFmNCMs;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNCMs.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNCMs.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNCMsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNCMs.DefParams;
begin
  VAR_GOTOTABELA := 'NCMs';
  VAR_GOTOMYSQLTABLE := QrNCMs;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, NCM, Nome, Letras, ImpNaLista');
  VAR_SQLx.Add('FROM ncms');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND NCM=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmNCMs.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmNCMs.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmNCMs.AlteraRegistro;
var
  NCMs : Integer;
begin
  NCMs := QrNCMsCodigo.Value;
  if QrNCMsCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(NCMs, Dmod.MyDB, 'NCMs', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NCMs, Dmod.MyDB, 'NCMs', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNCMs.IncluiRegistro;
var
  Cursor : TCursor;
  NCMs : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    NCMs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'NCMs', 'NCMs', 'Codigo');
    if Length(FormatFloat(FFormatFloat, NCMs))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, NCMs);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmNCMs.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNCMs.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNCMs.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNCMs.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNCMs.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNCMs.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNCMs.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNCMs, [PainelDados],
  [PainelEdita], EdNCM, ImgTipo, 'ncms');
end;

procedure TFmNCMs.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNCMsCodigo.Value;
  Close;
end;

procedure TFmNCMs.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if Length(EdNCM.Text) = 0 then
  begin
    Application.MessageBox('Defina o c�digo da NCM.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if Length(EdLetras.Text) = 0 then
  begin
    Application.MessageBox('Defina uma letra para emiss�o de NF.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('NCMs', 'Codigo', ImgTipo.SQLType,
    QrNCMsCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmNCMs, PainelEdit,
    'NCMs', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNCMs.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NCMs', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NCMs', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NCMs', 'Codigo');
end;

procedure TFmNCMs.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrNCMs, [PainelDados],
  [PainelEdita], EdNCM, ImgTipo, 'ncms');
end;

procedure TFmNCMs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmNCMs.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNCMsCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNCMs.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNCMs.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNCMs.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrNCMsNCM.Value, LaRegistro.Caption);
end;

procedure TFmNCMs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNCMs.QrNCMsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNCMs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNCMs.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNCMsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NCMs', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNCMs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNCMs.QrNCMsBeforeOpen(DataSet: TDataSet);
begin
  QrNCMsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNCMs.QrNCMsCalcFields(DataSet: TDataSet);
begin
  QrNCMsNCM_TXT.Value := Geral.FormataNCM(QrNCMsNCM.Value);
end;

end.

