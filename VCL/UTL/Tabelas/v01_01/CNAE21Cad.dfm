object FmCNAE21Cad: TFmCNAE21Cad
  Left = 368
  Top = 194
  Caption = 
    'GER-TBTER-001 :: CNAE CONCLA 2.1- Estrutura Detalhada: Subclasse' +
    's'
  ClientHeight = 319
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 223
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 4
          Width = 35
          Height = 13
          Caption = 'ID [F4]:'
          FocusControl = DBEdCodigo
        end
        object Label6: TLabel
          Left = 92
          Top = 4
          Width = 29
          Height = 13
          Caption = 'N'#237'vel:'
          FocusControl = DBEdNome
        end
        object Label7: TLabel
          Left = 296
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = dmkDBEdit1
        end
        object Label4: TLabel
          Left = 400
          Top = 4
          Width = 89
          Height = 13
          Caption = 'C'#243'digo Formatado:'
          Enabled = False
          FocusControl = dmkDBEdit1
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodigoKeyDown
        end
        object EdNivel: TdmkEditCB
          Left = 92
          Top = 20
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Nivel'
          UpdCampo = 'Nivel'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBNivel
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBNivel: TdmkDBLookupComboBox
          Left = 116
          Top = 20
          Width = 176
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCNAE21Niv
          TabOrder = 2
          dmkEditCB = EdNivel
          QryName = 'Nivel'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCodAlf: TdmkEdit
          Left = 296
          Top = 20
          Width = 100
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CodAlf'
          UpdCampo = 'CodAlf'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdCodAlfChange
          OnExit = EdCodAlfExit
        end
        object EdCodTxt: TdmkEdit
          Left = 400
          Top = 20
          Width = 100
          Height = 21
          Enabled = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CodTxt'
          UpdCampo = 'CodTxt'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel8: TPanel
        Left = 2
        Top = 65
        Width = 1004
        Height = 58
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object dmkLabelRotate2: TdmkLabelRotate
          Left = 0
          Top = 0
          Width = 17
          Height = 58
          Angle = ag90
          Caption = 'Descri'#231#227'o:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Align = alLeft
          ExplicitLeft = 572
          ExplicitTop = 16
          ExplicitHeight = 181
        end
        object MeNome: TdmkMemo
          Left = 17
          Top = 0
          Width = 987
          Height = 58
          Align = alClient
          TabOrder = 0
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 160
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 223
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 159
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 352
        Top = 15
        Width = 654
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 520
          Top = 0
          Width = 134
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtImporta: TBitBtn
          Tag = 19
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtImportaClick
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 129
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 92
          Top = 4
          Width = 29
          Height = 13
          Caption = 'N'#237'vel:'
          FocusControl = DBEdNome
        end
        object Label3: TLabel
          Left = 296
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = dmkDBEdit1
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 20
          Width = 80
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCNAE21Cad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 92
          Top = 20
          Width = 200
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'NO_NIVEL'
          DataSource = DsCNAE21Cad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 296
          Top = 20
          Width = 100
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'CodTxt'
          DataSource = DsCNAE21Cad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object PnNome: TPanel
        Left = 2
        Top = 65
        Width = 1004
        Height = 62
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object dmkLabelRotate1: TdmkLabelRotate
          Left = 0
          Top = 0
          Width = 17
          Height = 62
          Angle = ag90
          Caption = 'Descri'#231#227'o:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Align = alLeft
          ExplicitLeft = 572
          ExplicitTop = 16
          ExplicitHeight = 181
        end
        object DBMemo1: TDBMemo
          Left = 17
          Top = 0
          Width = 987
          Height = 62
          Align = alClient
          DataField = 'Nome'
          DataSource = DsCNAE21Cad
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 657
        Height = 32
        Caption = 'CNAE CONCLA 2.1- Estrutura Detalhada: Subclasses'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 657
        Height = 32
        Caption = 'CNAE CONCLA 2.1- Estrutura Detalhada: Subclasses'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 657
        Height = 32
        Caption = 'CNAE CONCLA 2.1- Estrutura Detalhada: Subclasses'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCNAE21Cad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCNAE21CadBeforeOpen
    AfterOpen = QrCNAE21CadAfterOpen
    SQL.Strings = (
      'SELECT niv.Nome NO_NIVEL, cad.*'
      'FROM cnae21cad cad'
      'LEFT JOIN cnae21niv niv ON niv.Codigo=cad.Nivel'
      '')
    Left = 64
    Top = 64
    object QrCNAE21CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAE21CadNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrCNAE21CadCodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAE21CadNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Size = 50
    end
    object QrCNAE21CadNivel1: TWideStringField
      FieldName = 'Nivel1'
    end
    object QrCNAE21CadNivel2: TWideStringField
      FieldName = 'Nivel2'
    end
    object QrCNAE21CadNivel3: TWideStringField
      FieldName = 'Nivel3'
    end
    object QrCNAE21CadNivel4: TWideStringField
      FieldName = 'Nivel4'
    end
    object QrCNAE21CadNivel5: TWideStringField
      FieldName = 'Nivel5'
    end
    object QrCNAE21CadCodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrCNAE21Niv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cnae21niv'
      'ORDER BY Nome')
    Left = 148
    Top = 64
    object QrCNAE21NivCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAE21NivNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAE21Niv: TDataSource
    DataSet = QrCNAE21Niv
    Left = 176
    Top = 64
  end
  object PMExclui: TPopupMenu
    Left = 696
    Top = 228
    object ExcluiItematual1: TMenuItem
      Caption = 'Exclui &Item atual'
      OnClick = ExcluiItematual1Click
    end
    object ExcluiTODOSitens1: TMenuItem
      Caption = 'Exclui &TODOS itens'
      OnClick = ExcluiTODOSitens1Click
    end
  end
  object frxNFS_TBTER_002_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 320
    Top = 76
    Datasets = <
      item
        DataSet = frxDsAll
        DataSetName = 'frxDsAll'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsAll
        DataSetName = 'frxDsAll'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 37.795275590000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Nivel4'
          DataSet = frxDsAll
          DataSetName = 'frxDsAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAll."Nivel4"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 113.385826770000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Nivel2'
          DataSet = frxDsAll
          DataSetName = 'frxDsAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAll."Nivel2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 75.590551180000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Nivel3'
          DataSet = frxDsAll
          DataSetName = 'frxDsAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAll."Nivel3"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 151.181002280000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'Nivel1'
          DataSet = frxDsAll
          DataSetName = 'frxDsAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAll."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Nivel5'
          DataSet = frxDsAll
          DataSetName = 'frxDsAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAll."Nivel5"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 207.874150000000000000
          Width = 472.441213390000000000
          Height = 13.228346460000000000
          DataField = 'Nome'
          DataSet = frxDsAll
          DataSetName = 'frxDsAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAll."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 86.929190000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 37.795275590000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Divis'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 113.385826770000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Classe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 75.590551180000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 151.181002280000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Subclasse')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Se'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 207.874150000000000000
          Width = 472.441213390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Denimina'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CNAE - Cadastro Nacional de Atividades Econ'#244'micas')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrAll: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCNAE21CadBeforeOpen
    AfterOpen = QrCNAE21CadAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM cnae21cad'
      'ORDER BY Codigo')
    Left = 348
    Top = 76
    object QrAllCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAllNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrAllCodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrAllNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrAllLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAllDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAllDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAllUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAllUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAllAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAllAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAllNivel1: TWideStringField
      FieldName = 'Nivel1'
    end
    object QrAllNivel2: TWideStringField
      FieldName = 'Nivel2'
    end
    object QrAllNivel3: TWideStringField
      FieldName = 'Nivel3'
    end
    object QrAllNivel4: TWideStringField
      FieldName = 'Nivel4'
    end
    object QrAllNivel5: TWideStringField
      FieldName = 'Nivel5'
    end
  end
  object frxDsAll: TfrxDBDataset
    UserName = 'frxDsAll'
    CloseDataSource = False
    DataSet = QrAll
    BCDToCurrency = False
    Left = 376
    Top = 76
  end
end
