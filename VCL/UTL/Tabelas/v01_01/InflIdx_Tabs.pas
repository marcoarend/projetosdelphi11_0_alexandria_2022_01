unit InflIdx_Tabs;
{ Colocar no MyListas:

Uses InflIdx_Tabs;


//
function TMyListas.CriaListaTabelas(:
      UnInflIdx_Tabs.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnInflIdx_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnInflIdx_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnInflIdx_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnInflIdx_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnInflIdx_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnInflIdx_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,  Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts,
  dmkGeral, UnDmkEnums;

type
  TInflIdx_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(TabelaBase: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

var
  UnInflIdx_Tabs: TInflIdx_Tabs;

const
  CO_INDICE_INFLACIO_COD_NENHUM  = 0;
  CO_INDICE_INFLACIO_COD_INPC    = 1;
  CO_INDICE_INFLACIO_COD_IGPDI   = 2;

  CO_INDICE_INFLACIO_TXT_NENHUM  = 'Nenhum';
  CO_INDICE_INFLACIO_TXT_INPC    = 'INPC - IBGE';
  CO_INDICE_INFLACIO_TXT_IGPDI   = 'IGP-DI - FGV';

  Max_INDICE_INFLACIO = 2;
  sLista_INDICE_INFLACIO: array[0..Max_INDICE_INFLACIO]of String = (
    CO_INDICE_INFLACIO_TXT_NENHUM,
    CO_INDICE_INFLACIO_TXT_INPC,
    CO_INDICE_INFLACIO_TXT_IGPDI
    );
  //

implementation

uses UMySQLModule;

function TInflIdx_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('???') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|SloganFoot');
    FListaSQL.Add('1|"O justo viver� por f�! Rm 1.17"');
  end else
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Numero|Login');
    FListaSQL.Add('0|""');
  end;
}
end;

function TInflIdx_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('CfgInfl'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('IGPDI'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('INPC'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TInflIdx_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('') then
    begin
    {
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"Lan�amentos"');
    }
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TInflIdx_Tabs.CarregaListaFRIndices(TabelaBase: String; FRIndices:
  TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('CfgInfl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('IGPDI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('INPC') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TInflIdx_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
  TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('CfgInfl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Indic1';  // CO_INDICE_INFLACIO_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percent1';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MesAnt1';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Indic2';  // CO_INDICE_INFLACIO_...
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Percent2';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MesAnt2';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('IGPDI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IGPDI_D';
      FRCampos.Tipo       := 'double(20,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IGPDI_X';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('INPC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'INPC_D';
      FRCampos.Tipo       := 'double(20,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'INPC_X';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TInflIdx_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
  TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := '???';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('OpcoesGerl') then
    begin
      New(FRCampos);
      FRCampos.Field      := '???';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
}
  except
    raise;
    Result := False;
  end;
end;

function TInflIdx_Tabs.CompletaListaFRJanelas(FRJanelas:
  TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // CFG-INFLA-001 :: Configura��o de Composi��o de �ndices
  New(FRJanelas);
  FRJanelas.ID        := 'CFG-INFLA-001';
  FRJanelas.Nome      := 'FmCfgInfl';
  FRJanelas.Descricao := 'Configura��o de Composi��o de �ndices';
  FLJanelas.Add(FRJanelas);
  //
  // TAB-IGPDI-001 :: IGP-DI - FGV
  New(FRJanelas);
  FRJanelas.ID        := 'TAB-IGPDI-001';
  FRJanelas.Nome      := 'FmIGPDI';
  FRJanelas.Descricao := 'IGP-DI - FGV';
  FLJanelas.Add(FRJanelas);
  //
  //
  // TAB-INPC1-001 :: INPC - IBGE
  New(FRJanelas);
  FRJanelas.ID        := 'TAB-INPC1-001';
  FRJanelas.Nome      := 'FmINPC1';
  FRJanelas.Descricao := 'INPC - IBGE';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.

