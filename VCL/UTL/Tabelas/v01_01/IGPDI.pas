unit IGPDI;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, Grids, ComObj, ComCtrls,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, dmkImage,
  UnDmkEnums, UnDmkProcFunc;

type
  TFmIGPDI = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    QrIGPDI: TmySQLQuery;
    DsIGPDI: TDataSource;
    dmkDBGrid1: TDBGrid;
    QrIGPDIIGPDI_D: TFloatField;
    QrIGPDIAnoMes: TIntegerField;
    QrIGPDIANOMES_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdArqXLS: TdmkEdit;
    PB1: TProgressBar;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BitBtn3: TBitBtn;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel6: TPanel;
    BitBtn4: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrIGPDIIGPDI_X: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrIGPDICalcFields(DataSet: TDataSet);
    procedure QrIGPDIAfterOpen(DataSet: TDataSet);
    procedure QrIGPDIAfterScroll(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenIGPDI(AnoMes: Integer);
    procedure CarregaXLS();
    procedure InsAltIndice(SQLType: TSQLType; Default: Double);
  public
    { Public declarations }
  end;

  var
  FmIGPDI: TFmIGPDI;

implementation

uses UnMyObjects, Module, UMySQLModule, MyVCLSkin, DmkDAC_PF, GetValor;

const
  FTits = 2;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'Per�odo',
{01}                            'Indice');

{$R *.DFM}

procedure TFmIGPDI.BitBtn3Click(Sender: TObject);
const
  DestFile = 'C:\Dermatek\IGPDI.xls';
var
  Fonte: String;
begin
  Fonte := EdArqXLS.Text;
  if FileExists(DestFile) then DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  if MLAGeral.DownloadFile(Fonte, DestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, EdArqXLS.Text, PB1, LaAviso1, LaAviso2) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Erro durante o download de "' +Fonte + '"');
end;

procedure TFmIGPDI.BtAbreClick(Sender: TObject);
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArqXLS.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
end;

procedure TFmIGPDI.BtAlteraClick(Sender: TObject);
begin
  if QrIGPDIIGPDI_D.Value <> Geral.DMV(QrIGPDIIGPDI_x.Value) then
    Geral.MB_Aviso('�ndice flutuante difere do �ndice texto!' + sLineBreak +
    'Altera��o n�o ser� poss�vel. Contate a DERMATEK!')
  else
    InsAltIndice(stUpd, QrIGPDIIGPDI_D.Value);
end;

procedure TFmIGPDI.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmIGPDI.BtExcluiClick(Sender: TObject);
begin
  if QrIGPDIIGPDI_D.Value <> Geral.DMV(QrIGPDIIGPDI_x.Value) then
    Geral.MB_Aviso('�ndice flutuante difere do �ndice texto!' + sLineBreak +
    'Exclus�o n�o ser� poss�vel. Contate a DERMATEK!')
  else
  begin
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do �ndice selecionado?',
    'igpdi', 'AnoMes', QrIGPDIAnoMes.Value, Dmod.MyDB);
    ReopenIGPDI(0);
  end;
end;

procedure TFmIGPDI.BtIncluiClick(Sender: TObject);
begin
  InsAltIndice(stIns, 0.000);
end;

procedure TFmIGPDI.CarregaXLS();
var
  R, AnoMes: Integer;
  DataStr, IGPDI_X, Mes, Ano: String;
  IGPDI_D: Double;
  Data: TDateTime;
begin
  if Geral.MB_Pergunta('Ser�o exclu�dos todos os registros da tabela!' + sLineBreak +
  'Deseja continuar assim mesmo?') <> ID_YES then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM igpdi');
    Dmod.QrUpd.ExecSQL;
    //
    for R := 1 to Grade1.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      IGPDI_X   := Trim(Grade1.Cells[02,R]);
      if Length(IGPDI_X) > 0 then
        IGPDI_D := StrToFloat(IGPDI_X)
      else
        IGPDI_D := 0;
      DataStr      := Trim(Grade1.Cells[01,R]);
      if Length(DataStr) > 0 then
      begin
        Mes  := Copy(DataStr, 4, 2);
        Ano  := Copy(DataStr, 7, 4);
        Data := EncodeDate(StrToInt(Ano), StrToInt(Mes), 1);
        //
        if Data > 0 then
        begin
          AnoMes := Geral.Periodo2000(Data);
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'igpdi', False,
          ['IGPDI_D', 'IGPDI_X'], [
          'AnoMes'], [IGPDI_D, IGPDI_X], [
          AnoMes], False);
        end;
      end;
    end;
    PB1.Position := PB1.Max;
    ReopenIGPDI(0);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor   := crDefault;
  end;
end;

procedure TFmIGPDI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIGPDI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmIGPDI.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  //
  Grade1.ColCount := FTits;
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  ReopenIGPDI(0);
end;

procedure TFmIGPDI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIGPDI.InsAltIndice(SQLType: TSQLType; Default: Double);
const
  //Default = 0.000;
  Casas = 3;
  LeftZeros = 0;
  ValMin = '';
  ValMax = '';
  Obrigatorio = True;
  FormCaption = 'Novo �ndice';
  ValCaption = 'Fator (n�o � o %!):';
  WidthVal = 120;
  DateCaption = 'Data "01MMAAAA":';
  HabilitaEdit = True;
var
  Resultado: Variant;
  Data: TDateTime;
  //
  Ano, Mes, Dia: Word;
  AnoMes: Integer;
  IGPDI_D: Double;
  IGPDI_X: String;
  HabilitaDate: Boolean;
  DataDef: TDateTime;
begin
  HabilitaDate := SQLType = stIns;
  DataDef := Geral.PeriodoToDate(QrIGPDIAnoMes.Value, 1, False);
  MyObjects.GetValorEDataDmk(TFmGetValor, FmGetValor, dmktfDouble, Default,
  Casas, LeftZeros, ValMin, ValMax,Obrigatorio, FormCaption, ValCaption,
  WidthVal, DateCaption, HabilitaEdit, HabilitaDate, Resultado, Data);
  if SQLType = stIns then
  begin
    DecodeDate(Data, Ano, Mes, Dia);
    Data := EncodeDate(Ano, Mes, 1);
  end else
  begin
    Data := DataDef;
  end;
  //
  if (Resultado <> Null) and (Data > 2) then
  begin
    IGPDI_D := Resultado;
    IGPDI_X := Geral.FFT(IGPDI_D, 3, siNegativo);
    //
    AnoMes := Geral.Periodo2000(Data);
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'igpdi', False,
    ['IGPDI_D', 'IGPDI_X'], [
    'AnoMes'], [IGPDI_D, IGPDI_X], [
    AnoMes], False);
    //
    ReopenIGPDI(AnoMes);
  end;
end;

procedure TFmIGPDI.QrIGPDIAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrIGPDI.RecordCount > 0;
end;

procedure TFmIGPDI.QrIGPDIAfterScroll(DataSet: TDataSet);
begin
  BtExclui.Enabled := (QrIGPDI.RecordCount > 0) and (QrIGPDI.RecNo = 1);
end;

procedure TFmIGPDI.QrIGPDICalcFields(DataSet: TDataSet);
begin
  QrIGPDIANOMES_TXT.Value := dmkPF.MesEAnoDoPeriodo(QrIGPDIAnoMes.Value);
end;

procedure TFmIGPDI.ReopenIGPDI(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIGPDI, Dmod.MyDB, [
  'SELECT AnoMes, IGPDI_D, IGPDI_X ',
  'FROM igpdi ',
  'ORDER BY AnoMes DESC ',
  '']);
  //
  if AnoMes <> 0 then
    QrIGPDI.Locate('AnoMes', AnoMes, []);
end;

procedure TFmIGPDI.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := '';//ExtractFileDir(EdArqXLS.Text);
  Arquivo := '';//ExtractFileName(EdArqXLS.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArqXLS.Text := Arquivo;
end;

end.
