unit IGPM;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, ComObj, ComCtrls, UnDmkWeb,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, dmkImage,
  UnDmkEnums;

type
  TFmIGPM = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdCaminho: TdmkEdit;
    PB1: TProgressBar;
    QrIGPM: TmySQLQuery;
    DsIGPM: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrIGPMIGPM_D: TFloatField;
    QrIGPMAnoMes: TIntegerField;
    QrIGPMANOMES_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BitBtn3: TBitBtn;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrIGPMCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenIGPM();
    procedure CarregaXLS();
  public
    { Public declarations }
  end;

  var
  FmIGPM: TFmIGPM;

implementation

uses UnMyObjects, Module, UMySQLModule, MyVCLSkin, UnDmkProcFunc;

const
  FURL = 'http://www.dermatek.com.br/Tabelas_Publicas/IGPM_FGV.xls';
  FDestFile = 'C:\Dermatek\Tabelas\IGPM\IGPM_FGV.xls';
  FTits = 2;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'ID',
{01}                            'Nome');

{$R *.DFM}

procedure TFmIGPM.BitBtn3Click(Sender: TObject);
var
  Fonte: String;
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) = 0 then
  begin
    Geral.MB_Info('Voc� deve digitar um URL para baixar o arquivo!');
    Exit;
  end;
  //
  Fonte := EdCaminho.Text;
  if FileExists(FDestFile) then DeleteFile(FDestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  //
  if not DirectoryExists(ExtractFileDir(FDestFile)) then
    ForceDirectories(ExtractFileDir(FDestFile));
  //
  if DmkWeb.DownloadFile(Fonte, FDestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, FDestFile, PB1, LaAviso1, LaAviso2) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Erro durante o download de "' +Fonte + '"');
end;

procedure TFmIGPM.BtAbreClick(Sender: TObject);
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
end;

procedure TFmIGPM.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmIGPM.CarregaXLS();
var
  R: Integer;
  DataStr, IndiceStr, Mes, Ano: String;
  Indice: Double;
  Data: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM igpm');
    Dmod.QrUpd.ExecSQL;
    //
    for R := 10 to Grade1.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      IndiceStr   := Trim(Grade1.Cells[02,R]);
      if Length(IndiceStr) > 0 then
        Indice := StrToFloat(IndiceStr)
      else
        Indice := 0;
      DataStr      := Trim(Grade1.Cells[01,R]);
      if Length(DataStr) > 0 then
      begin
        Mes  := Copy(DataStr, 1, 2);
        Ano  := Copy(DataStr, 4, 4);
        Data := EncodeDate(StrToInt(Ano), StrToInt(Mes), 1);
        //
        if Data > 0 then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'igpm', False,
          ['IGPM_D'], ['AnoMes'], [Indice], [Geral.Periodo2000(Data)], False);
        end;
      end;
    end;
    PB1.Position := PB1.Max;
    ReopenIGPM();
  finally
    LaAviso.Caption := '';
    Screen.Cursor   := crDefault;
  end;
end;

procedure TFmIGPM.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIGPM.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmIGPM.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType        := stLok;
  EdCaminho.ValueVariant := FURL;
  //
  Grade1.ColCount := FTits;
  //
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  //
  ReopenIGPM();
  //
  if QrIGPM.RecordCount > 0 then
    PageControl1.ActivePageIndex := 1;
end;

procedure TFmIGPM.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIGPM.QrIGPMCalcFields(DataSet: TDataSet);
begin
  QrIGPMANOMES_TXT.Value := dmkPF.MesEAnoDoPeriodo(QrIGPMAnoMes.Value);
end;

procedure TFmIGPM.ReopenIGPM();
begin
  UMyMod.AbreQuery(QrIGPM, Dmod.MyDB);
end;

procedure TFmIGPM.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := '';
  Arquivo := '';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo',
    '', [], Arquivo)
  then
    EdCaminho.Text := Arquivo;
end;

end.
