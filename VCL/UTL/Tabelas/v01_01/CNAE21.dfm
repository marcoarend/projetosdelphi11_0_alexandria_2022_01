object FmCNAE21: TFmCNAE21
  Left = 339
  Top = 185
  Caption = 'ENT-CNAE2-001 :: Importa'#231#227'o do CNAE 2.1'
  ClientHeight = 562
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 444
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 52
      Width = 1008
      Height = 335
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Abertos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 307
          Align = alClient
          ColCount = 2
          DefaultColWidth = 44
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Carregados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 433
          Top = 0
          Height = 307
          ExplicitLeft = 412
          ExplicitTop = 116
          ExplicitHeight = 100
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 433
          Height = 307
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 433
            Height = 307
            Align = alClient
            Color = clWindow
            DataSource = DsCNAE21Cad
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object Panel5: TPanel
          Left = 436
          Top = 0
          Width = 564
          Height = 307
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 564
            Height = 21
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object EdPesq: TdmkEdit
              Left = 0
              Top = 0
              Width = 564
              Height = 21
              Align = alClient
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'EdPesq'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'EdPesq'
              ValWarn = False
              OnExit = EdPesqExit
            end
          end
          object dmkDBGrid2: TdmkDBGrid
            Left = 0
            Top = 21
            Width = 564
            Height = 286
            Align = alClient
            Color = clWindow
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label27: TLabel
        Left = 8
        Top = 7
        Width = 116
        Height = 13
        Caption = 'Arquivo a ser carregado:'
      end
      object SpeedButton8: TSpeedButton
        Left = 974
        Top = 23
        Width = 21
        Height = 21
        OnClick = SpeedButton8Click
      end
      object EdCaminho: TdmkEdit
        Left = 8
        Top = 23
        Width = 965
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'C:\Dermatek\Tabelas\CNAE_CONCLA\cnae21_estrutura_detalhada.xls'
        QryCampo = 'DirNFeGer'
        UpdCampo = 'DirNFeGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'C:\Dermatek\Tabelas\CNAE_CONCLA\cnae21_estrutura_detalhada.xls'
        ValWarn = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 387
      Width = 1008
      Height = 57
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 2
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 23
          Width = 1004
          Height = 17
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 303
        Height = 32
        Caption = 'Importa'#231#227'o do CNAE 2.1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 303
        Height = 32
        Caption = 'Importa'#231#227'o do CNAE 2.1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 303
        Height = 32
        Caption = 'Importa'#231#227'o do CNAE 2.1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 492
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtBaixar: TBitBtn
        Tag = 19
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Baixar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtBaixarClick
      end
      object BtAbre: TBitBtn
        Left = 100
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Abre xls'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAbreClick
      end
      object BtCarrega: TBitBtn
        Left = 192
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Carrega xls'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCarregaClick
      end
    end
  end
  object QrCNAE21Cad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCNAE21CadBeforeClose
    AfterScroll = QrCNAE21CadAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM cnae21cad'
      'ORDER BY Nivel DESC, CodTxt')
    Left = 32
    Top = 204
    object QrCNAE21CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAE21CadNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object QrCNAE21CadCodTxt: TWideStringField
      FieldName = 'CodTxt'
    end
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 60
    Top = 204
  end
end
