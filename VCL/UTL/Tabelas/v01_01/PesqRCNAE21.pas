unit PesqRCNAE21;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, Grids, DBGrids, dmkDBGrid, frxClass,
  dmkGeral, frxDBSet, Mask, dmkDBEdit, dmkImage, UnDmkEnums;

type
  TFmPesqRCNAE21 = class(TForm)
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    Panel1: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    EdPesq: TdmkEdit;
    dmkDBGrid5: TdmkDBGrid;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtPesquisa: TBitBtn;
    QrCNAE21CadCodAlf: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdPesqExit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCNAE21();
  public
    { Public declarations }
    FCodAlf: String;
  end;

  var
  FmPesqRCNAE21: TFmPesqRCNAE21;

implementation

{$R *.DFM}

uses UnMyObjects, ModuleGeral, MyListas;

procedure TFmPesqRCNAE21.BtPesquisaClick(Sender: TObject);
begin
  FCodAlf := QrCNAE21CadCodAlf.Value;
  Close;
end;

procedure TFmPesqRCNAE21.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqRCNAE21.EdPesqExit(Sender: TObject);
begin
  ReopenCNAE21();
end;

procedure TFmPesqRCNAE21.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqRCNAE21.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  ReopenCNAE21();
end;

procedure TFmPesqRCNAE21.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqRCNAE21.ReopenCNAE21();
begin
  QrCNAE21Cad.Close;
  QrCNAE21Cad.Params[0].AsString := '%' + EdPesq.ValueVariant + '%';
  UMyMod.AbreQuery(QrCNAE21Cad, DModG.AllID_DB);
end;

end.



