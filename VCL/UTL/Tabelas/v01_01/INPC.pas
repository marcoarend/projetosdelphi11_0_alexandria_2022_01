unit INPC;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, Grids, ComObj, ComCtrls,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, dmkImage,
  UnDmkEnums, UnDmkProcFunc;

type
  TFmINPC = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    QrINPC: TmySQLQuery;
    DsINPC: TDataSource;
    dmkDBGrid1: TDBGrid;
    QrINPCINPC_D: TFloatField;
    QrINPCAnoMes: TIntegerField;
    QrINPCANOMES_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdArqXLS: TdmkEdit;
    PB1: TProgressBar;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BitBtn3: TBitBtn;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel6: TPanel;
    BitBtn4: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrINPCINPC_X: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrINPCCalcFields(DataSet: TDataSet);
    procedure QrINPCAfterOpen(DataSet: TDataSet);
    procedure QrINPCAfterScroll(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenINPC(AnoMes: Integer);
    procedure CarregaXLS();
    procedure InsAltIndice(SQLType: TSQLType; Default: Double);
  public
    { Public declarations }
  end;

  var
  FmINPC: TFmINPC;

implementation

uses UnMyObjects, Module, UMySQLModule, MyVCLSkin, DmkDAC_PF, GetValor;

const
  FTits = 2;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'Per�odo',
{01}                            'Indice');

{$R *.DFM}

procedure TFmINPC.BitBtn3Click(Sender: TObject);
const
  DestFile = 'C:\Dermatek\INPC.xls';
var
  Fonte: String;
begin
  Fonte := EdArqXLS.Text;
  if FileExists(DestFile) then DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  if MLAGeral.DownloadFile(Fonte, DestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, EdArqXLS.Text, PB1, LaAviso1, LaAviso2) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Erro durante o download de "' +Fonte + '"');
end;

procedure TFmINPC.BtAbreClick(Sender: TObject);
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArqXLS.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
end;

procedure TFmINPC.BtAlteraClick(Sender: TObject);
begin
  if QrINPCINPC_D.Value <> Geral.DMV(QrINPCINPC_x.Value) then
    Geral.MB_Aviso('�ndice flutuante difere do �ndice texto!' + sLineBreak +
    'Altera��o n�o ser� poss�vel. Contate a DERMATEK!')
  else
    InsAltIndice(stUpd, QrINPCINPC_D.Value);
end;

procedure TFmINPC.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmINPC.BtExcluiClick(Sender: TObject);
begin
  if QrINPCINPC_D.Value <> Geral.DMV(QrINPCINPC_x.Value) then
    Geral.MB_Aviso('�ndice flutuante difere do �ndice texto!' + sLineBreak +
    'Exclus�o n�o ser� poss�vel. Contate a DERMATEK!')
  else
  begin
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do �ndice selecionado?',
    'inpc', 'AnoMes', QrINPCAnoMes.Value, Dmod.MyDB);
    ReopenINPC(0);
  end;
end;

procedure TFmINPC.BtIncluiClick(Sender: TObject);
begin
  InsAltIndice(stIns, 0.000);
end;

procedure TFmINPC.CarregaXLS();
var
  R, AnoMes, Mes, Ano, YAnt: Integer;
  INPC_X, MStr: String;
  INPC_D: Double;
  Data: TDateTime;
  //
  Achou: Boolean;
begin
  if Geral.MB_Pergunta('Ser�o exclu�dos todos os registros da tabela!' + sLineBreak +
  'Deseja continuar assim mesmo?') <> ID_YES then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Achou := False;
    YAnt  := 0;
    //
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM inpc');
    Dmod.QrUpd.ExecSQL;
    //
    for R := 3 to Grade1.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      Ano := Geral.IMV(Trim(Grade1.Cells[01,R]));
      if Ano = 0 then
        Ano := YAnt
      else
        YAnt := Ano;
      MStr := Trim(Grade1.Cells[02,R]);
      //
      INPC_X   := Trim(Grade1.Cells[03,R]);
      if Length(INPC_X) > 0 then
        INPC_D := StrToFloat(INPC_X)
      else
        INPC_D := 0;
      if (Ano > 0) and (MStr <> '') and (INPC_D > 0) then
      begin
        Mes := Geral.MesIntDeMesStr3Letras(MStr);
        Data := EncodeDate(Ano, Mes, 1);
        //
        if Data > 0 then
        begin
          AnoMes := Geral.Periodo2000(Data);
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'inpc', False,
          ['INPC_D', 'INPC_X'], [
          'AnoMes'], [INPC_D, INPC_X], [
          AnoMes], False);
        end else
          Exit;
      end else
        Exit;
    end;
    PB1.Position := PB1.Max;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    ReopenINPC(0);
    Screen.Cursor   := crDefault;
  end;
end;

procedure TFmINPC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmINPC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmINPC.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  //
  Grade1.ColCount := FTits;
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  ReopenINPC(0);
end;

procedure TFmINPC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmINPC.InsAltIndice(SQLType: TSQLType; Default: Double);
const
  //Default = 0.000;
  Casas = 3;
  LeftZeros = 0;
  ValMin = '';
  ValMax = '';
  Obrigatorio = True;
  FormCaption = 'Novo �ndice';
  ValCaption = 'Fator (n�o � o %!):';
  WidthVal = 120;
  DateCaption = 'Data "01MMAAAA":';
  HabilitaEdit = True;
var
  Resultado: Variant;
  Data: TDateTime;
  //
  Ano, Mes, Dia: Word;
  AnoMes: Integer;
  INPC_D: Double;
  INPC_X: String;
  HabilitaDate: Boolean;
  DataDef: TDateTime;
begin
  HabilitaDate := SQLType = stIns;
  DataDef := Geral.PeriodoToDate(QrINPCAnoMes.Value, 1, False);
  MyObjects.GetValorEDataDmk(TFmGetValor, FmGetValor, dmktfDouble, Default,
  Casas, LeftZeros, ValMin, ValMax,Obrigatorio, FormCaption, ValCaption,
  WidthVal, DateCaption, HabilitaEdit, HabilitaDate, Resultado, Data);
  if SQLType = stIns then
  begin
    DecodeDate(Data, Ano, Mes, Dia);
    Data := EncodeDate(Ano, Mes, 1);
  end else
  begin
    Data := DataDef;
  end;
  //
  if (Resultado <> Null) and (Data > 2) then
  begin
    INPC_D := Resultado;
    INPC_X := Geral.FFT(INPC_D, 3, siNegativo);
    //
    AnoMes := Geral.Periodo2000(Data);
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'inpc', False,
    ['INPC_D', 'INPC_X'], [
    'AnoMes'], [INPC_D, INPC_X], [
    AnoMes], False);
    //
    ReopenINPC(AnoMes);
  end;
end;

procedure TFmINPC.QrINPCAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrINPC.RecordCount > 0;
end;

procedure TFmINPC.QrINPCAfterScroll(DataSet: TDataSet);
begin
  BtExclui.Enabled := (QrINPC.RecordCount > 0) and (QrINPC.RecNo = 1);
end;

procedure TFmINPC.QrINPCCalcFields(DataSet: TDataSet);
begin
  QrINPCANOMES_TXT.Value := dmkPF.MesEAnoDoPeriodo(QrINPCAnoMes.Value);
end;

procedure TFmINPC.ReopenINPC(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrINPC, Dmod.MyDB, [
  'SELECT AnoMes, INPC_D, INPC_X ',
  'FROM inpc ',
  'ORDER BY AnoMes DESC ',
  '']);
  //
  if AnoMes <> 0 then
    QrINPC.Locate('AnoMes', AnoMes, []);
end;

procedure TFmINPC.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := '';//ExtractFileDir(EdArqXLS.Text);
  Arquivo := '';//ExtractFileName(EdArqXLS.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArqXLS.Text := Arquivo;
end;

end.
