unit PesqCNAE21;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkImage, dmkGeral, UnDMkEnums;

type
  TFmPesqCNAE21 = class(TForm)
    Qr1: TmySQLQuery;
    Ds1: TDataSource;
    Ds2: TDataSource;
    Qr2: TmySQLQuery;
    Ds3: TDataSource;
    Qr3: TmySQLQuery;
    Ds4: TDataSource;
    Qr4: TmySQLQuery;
    Ds5: TDataSource;
    Qr5: TmySQLQuery;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    EdPesq4: TdmkEdit;
    dmkDBGrid1: TdmkDBGrid;
    Panel6: TPanel;
    Panel7: TPanel;
    EdPesq3: TdmkEdit;
    dmkDBGrid2: TdmkDBGrid;
    Panel8: TPanel;
    Panel9: TPanel;
    EdPesq2: TdmkEdit;
    dmkDBGrid3: TdmkDBGrid;
    Panel10: TPanel;
    Panel11: TPanel;
    EdPesq1: TdmkEdit;
    dmkDBGrid4: TdmkDBGrid;
    Panel12: TPanel;
    Panel13: TPanel;
    EdPesq5: TdmkEdit;
    dmkDBGrid5: TdmkDBGrid;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel14: TPanel;
    BtPesquisa: TBitBtn;
    Qr1CodAlf: TWideStringField;
    Qr1Nome: TWideStringField;
    Qr2CodAlf: TWideStringField;
    Qr2Nome: TWideStringField;
    Qr3CodAlf: TWideStringField;
    Qr3Nome: TWideStringField;
    Qr4CodAlf: TWideStringField;
    Qr4Nome: TWideStringField;
    Qr5CodAlf: TWideStringField;
    Qr5Nome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Qr1AfterScroll(DataSet: TDataSet);
    procedure Qr1BeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure Qr2AfterScroll(DataSet: TDataSet);
    procedure Qr2BeforeClose(DataSet: TDataSet);
    procedure Qr3BeforeClose(DataSet: TDataSet);
    procedure Qr3AfterScroll(DataSet: TDataSet);
    procedure Qr4AfterScroll(DataSet: TDataSet);
    procedure Qr4BeforeClose(DataSet: TDataSet);
    procedure EdPesq2Exit(Sender: TObject);
    procedure EdPesq3Exit(Sender: TObject);
    procedure EdPesq4Exit(Sender: TObject);
    procedure EdPesq5Exit(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdPesq1Exit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCNAE21a();
    procedure ReopenCNAE21b();
    procedure ReopenCNAE21c();
    procedure ReopenCNAE21d();
    procedure ReopenCNAE21e();
  public
    { Public declarations }
    FCodAlf: String;
  end;

  var
  FmPesqCNAE21: TFmPesqCNAE21;

implementation

{$R *.DFM}

uses UnMyObjects, ModuleGeral, MyListas;

procedure TFmPesqCNAE21.BtPesquisaClick(Sender: TObject);
begin
  FCodAlf := Qr5CodAlf.Value;
  Close;
end;

procedure TFmPesqCNAE21.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCNAE21.EdPesq1Exit(Sender: TObject);
begin
  ReopenCNAE21a();
end;

procedure TFmPesqCNAE21.EdPesq2Exit(Sender: TObject);
begin
  ReopenCNAE21b();
end;

procedure TFmPesqCNAE21.EdPesq3Exit(Sender: TObject);
begin
  ReopenCNAE21c();
end;

procedure TFmPesqCNAE21.EdPesq4Exit(Sender: TObject);
begin
  ReopenCNAE21d();
end;

procedure TFmPesqCNAE21.EdPesq5Exit(Sender: TObject);
begin
  ReopenCNAE21e();
end;

procedure TFmPesqCNAE21.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqCNAE21.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  ReopenCNAE21a();
end;

procedure TFmPesqCNAE21.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqCNAE21.Qr1AfterScroll(DataSet: TDataSet);
begin
  ReopenCNAE21b();
end;

procedure TFmPesqCNAE21.Qr1BeforeClose(DataSet: TDataSet);
begin
  Qr2.Close;
end;

procedure TFmPesqCNAE21.Qr2AfterScroll(DataSet: TDataSet);
begin
  ReopenCNAE21c();
end;

procedure TFmPesqCNAE21.Qr2BeforeClose(DataSet: TDataSet);
begin
  Qr3.Close;
end;

procedure TFmPesqCNAE21.Qr3AfterScroll(DataSet: TDataSet);
begin
  ReopenCNAE21d();
end;

procedure TFmPesqCNAE21.Qr3BeforeClose(DataSet: TDataSet);
begin
  Qr4.Close;
end;

procedure TFmPesqCNAE21.Qr4AfterScroll(DataSet: TDataSet);
begin
  ReopenCNAE21e();
end;

procedure TFmPesqCNAE21.Qr4BeforeClose(DataSet: TDataSet);
begin
  Qr5.Close;
end;

procedure TFmPesqCNAE21.ReopenCNAE21a;
begin
  Qr1.Close;
  Qr1.Params[0].AsString := '%' + EdPesq1.ValueVariant + '%';
  UMyMod.AbreQuery(Qr1, DModG.AllID_DB);
end;

procedure TFmPesqCNAE21.ReopenCNAE21b;
begin
  Qr2.Close;
  Qr2.Params[0].AsString := Qr1CodAlf.Value;
  Qr2.Params[1].AsString := '%' + EdPesq2.ValueVariant + '%';
  UMyMod.AbreQuery(Qr2, DModG.AllID_DB);
end;

procedure TFmPesqCNAE21.ReopenCNAE21c;
begin
  Qr3.Close;
  Qr3.Params[0].AsString := Qr2CodAlf.Value;
  Qr3.Params[1].AsString := '%' + EdPesq3.ValueVariant + '%';
  UMyMod.AbreQuery(Qr3, DModG.AllID_DB);
end;

procedure TFmPesqCNAE21.ReopenCNAE21d;
begin
  Qr4.Close;
  Qr4.Params[0].AsString := Qr3CodAlf.Value;
  Qr4.Params[1].AsString := '%' + EdPesq4.ValueVariant + '%';
  UMyMod.AbreQuery(Qr4, DModG.AllID_DB);
end;

procedure TFmPesqCNAE21.ReopenCNAE21e;
begin
  Qr5.Close;
  Qr5.Params[0].AsString := Qr4CodAlf.Value;
  Qr5.Params[1].AsString := '%' + EdPesq5.ValueVariant + '%';
  UMyMod.AbreQuery(Qr5, DModG.AllID_DB);
end;

end.



