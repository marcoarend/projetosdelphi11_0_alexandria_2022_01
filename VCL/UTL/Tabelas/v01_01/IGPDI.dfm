object FmIGPDI: TFmIGPDI
  Left = 339
  Top = 185
  ActiveControl = PageControl1
  Caption = 'TAB-IGPDI-001 :: IGP-DI - FGV'
  ClientHeight = 494
  ClientWidth = 608
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 105
    Width = 608
    Height = 389
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 608
      Height = 389
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = ' Registros '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 600
          Height = 297
          Align = alClient
          DataSource = DsIGPDI
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ANOMES_TXT'
              Title.Caption = 'M'#234's / Ano'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGPDI_D'
              Title.Caption = #205'ndice'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGPDI_X'
              Title.Caption = 'Texto do fator'
              Visible = True
            end>
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 297
          Width = 600
          Height = 64
          Align = alBottom
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 1
          object Panel2: TPanel
            Left = 2
            Top = 15
            Width = 596
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel6: TPanel
              Left = 464
              Top = 0
              Width = 132
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object BitBtn4: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
            object BtInclui: TBitBtn
              Tag = 10
              Left = 4
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Inclui'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtIncluiClick
            end
            object BtAltera: TBitBtn
              Tag = 11
              Left = 128
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtAlteraClick
            end
            object BtExclui: TBitBtn
              Tag = 12
              Left = 252
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Exclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BtExcluiClick
            end
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Carregamento '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade1: TStringGrid
          Left = 0
          Top = 49
          Width = 600
          Height = 248
          Align = alClient
          ColCount = 2
          DefaultColWidth = 44
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 600
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label27: TLabel
            Left = 8
            Top = 7
            Width = 116
            Height = 13
            Caption = 'Arquivo a ser carregado:'
          end
          object SpeedButton8: TSpeedButton
            Left = 566
            Top = 23
            Width = 21
            Height = 21
            OnClick = SpeedButton8Click
          end
          object EdArqXLS: TdmkEdit
            Left = 8
            Top = 23
            Width = 557
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'C:\Dermatek\IGP-DI\IGP-DI FGV.xlsx'
            QryCampo = 'DirNFeGer'
            UpdCampo = 'DirNFeGer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'C:\Dermatek\IGP-DI\IGP-DI FGV.xlsx'
            ValWarn = False
          end
        end
        object GBRodaPe: TGroupBox
          Left = 0
          Top = 297
          Width = 600
          Height = 64
          Align = alBottom
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 2
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 596
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object PnSaiDesis: TPanel
              Left = 464
              Top = 0
              Width = 132
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object BtSaida: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
            object BitBtn3: TBitBtn
              Tag = 19
              Left = 8
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Baixar'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BitBtn3Click
            end
            object BtAbre: TBitBtn
              Left = 132
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Abre xls'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtAbreClick
            end
            object BtCarrega: TBitBtn
              Left = 256
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Carrega xls'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 3
              OnClick = BtCarregaClick
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 608
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 560
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 512
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 163
        Height = 32
        Caption = 'IGP-DI - FGV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 163
        Height = 32
        Caption = 'IGP-DI - FGV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 163
        Height = 32
        Caption = 'IGP-DI - FGV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 608
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 604
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 604
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object QrIGPDI: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrIGPDIAfterOpen
    AfterScroll = QrIGPDIAfterScroll
    OnCalcFields = QrIGPDICalcFields
    SQL.Strings = (
      'SELECT AnoMes, IGPM_D'
      'FROM igpdi'
      'ORDER BY AnoMes DESC'
      '')
    Left = 32
    Top = 204
    object QrIGPDIIGPDI_D: TFloatField
      FieldName = 'IGPDI_D'
      DisplayFormat = '#,###,###,###,##0.000'
    end
    object QrIGPDIAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrIGPDIANOMES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ANOMES_TXT'
      Calculated = True
    end
    object QrIGPDIIGPDI_X: TWideStringField
      FieldName = 'IGPDI_X'
      Size = 60
    end
  end
  object DsIGPDI: TDataSource
    DataSet = QrIGPDI
    Left = 60
    Top = 204
  end
end
