unit IBPTaxLoad;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, ComObj, ComCtrls,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, dmkImage,
  UnDmkEnums, MySQLBatch;

type
  TFmIBPTaxLoad = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdCaminho: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel9: TPanel;
    BtBaixar: TBitBtn;
    BtAbre: TBitBtn;
    BitBtn1: TBitBtn;
    MySQLBatchExecute1: TMySQLBatchExecute;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtBaixarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ObtemCodigoNome1(const Linha: String; var Codigo, EX, Tabela,
                Descricao, AliqNac, AliqImp, Estadual, Municipal, VigenIni,
                VigenFim, Chave, Versao, FonteStr: String);
    function  ObtemCodigoNome2(const Linha, UF: String): String;
    procedure InsereRegistro(Codigo, EX, Tabela, Descricao, AliqNac, AliqImp,
                Estadual, Municipal, VigenIni, VigenFim, Chave, Versao,
                FonteStr, UF: String);
    //function  ObtemVersao(Linha: String): String;
    procedure CarregaTabela1();
    procedure CarregaTabela2();
    procedure ImportaMassivo(lstArq: TStringList; UF: String);
  public
    { Public declarations }
  end;

  var
  FmIBPTaxLoad: TFmIBPTaxLoad;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, MyVCLSkin, UnInternalConsts,
  UnWAceites, Module, DmkDAC_PF;

const
  //FURL = 'http://www.dermatek.com.br/Tabelas_Publicas/CNAE_CONCLA_21.xls';
  FDestFile = 'C:\Dermatek\Tabelas\IBPT\IBPTax.0.0.2.csv';

{$R *.DFM}

procedure TFmIBPTaxLoad.BtBaixarClick(Sender: TObject);
(*
var
  Fonte: String;
*)
begin
(*
  if pos('http', LowerCase(EdCaminho.ValueVariant)) = 0 then
  begin
    Geral.MB_Info('Voc� deve digitar um URL para baixar o arquivo!');
    Exit;
  end;
  //
  Fonte := EdCaminho.Text;
  if FileExists(FDestFile) then DeleteFile(FDestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  //
  if not DirectoryExists(ExtractFileDir(FDestFile)) then
    ForceDirectories(ExtractFileDir(FDestFile));
  //
  if MLAGeral.DownloadFile(Fonte, FDestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, FDestFile, PB1, LaAviso1, LaAviso2) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Erro durante o download de "' +Fonte + '"');
*)
end;

procedure TFmIBPTaxLoad.BitBtn1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Esta op��o ir� excluir toda tabela atual do IBPTax antes de carregar a nova.'
  + sLineBreak + 'Deseja continuar assim mesmo?') <> ID_Yes then Exit;
  //
  CarregaTabela2();
end;

procedure TFmIBPTaxLoad.BtAbreClick(Sender: TObject);
begin
  CarregaTabela1();
end;
procedure TFmIBPTaxLoad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIBPTaxLoad.CarregaTabela1();
var
  I, Usuario, Termo: Integer;
  Linha, Codigo, EX, Tabela, Descricao, AliqNac, AliqImp, Estadual, Municipal,
  VigenIni, VigenFim, Chave, Versao, FonteStr, Arquivo, UF: String;
  lstArq: TStringList;
  DtaHora: TDateTime;
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  if not InputQuery('Sigla do estado (UF)', 'Digite a sigla do estado (UF):', UF) then Exit;
  //
  UF := Geral.FormataUF(UpperCase(UF));
  //
  if UF = '' then
  begin
    Geral.MB_Aviso('UF inv�lido!');
    Exit
  end;
  //
  if Pos(UF, ExtractFileName(EdCaminho.ValueVariant)) = 0 then
  begin
    if Geral.MB_Pergunta('O arquivo que voc� selecionou n�o parece ser da UF "NOMEUF"!' +
      sLineBreak + 'As al�quotas podem ser diferentes de estado para estado' + sLineBreak +
      'Mas se voc� deseja importar precione o bot�o "Sim" caso contr�rio clique no bot�o "N�o" ou "Cancela" e selecione o arquivo correto!') <> ID_YES
    then
      Exit;
  end;
  //
  if UWAceites.AceitaTermos(istIBPTax, Termo, Usuario, DtaHora) then
  begin
    if not UMyMod.SQLInsUpd(DMod.QrUpd, stUpd, 'OpcoesGerl', False,
      ['IBPTax_TermoAceite', 'IBPTax_DataHoraAceite', 'IBPTax_UsuarioAceite'], ['Codigo'],
      [Termo, Geral.FDT(DtaHora, 109), Usuario], [1], False) then
    begin
      Geral.MB_Aviso('Falha ao atualizar dados!');
      Exit;
    end;
  end else
  begin
    Geral.MB_Aviso('Importa��o abortada!');
    Exit;
  end;
  //
{
  MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
}
  //FItens := 0;
  Arquivo := EdCaminho.Text;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    lstArq := TStringList.Create;
    try
      lstArq.LoadFromFile(Arquivo);
      if lstArq.Count > 0 then
      begin
        PB1.Position := 0;
        PB1.Max := lstArq.Count;
        //
        Linha := lstArq[0];
        (*
        Versao := Trim(ObtemVersao(Linha));
        if Versao = '' then
        begin
          Geral.MB_Aviso('Importa��o abortada! N�o foi poss�vel obter a vers�o!');
          Exit;
        end;
        *)
        for I := 1 to lstArq.Count - 1 do
        begin
          //PB1.Position := PB1.Position + 1;
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //
          Linha := lstArq[I];
          //
          ObtemCodigoNome1(Linha, Codigo, EX, Tabela, Descricao, AliqNac,
            AliqImp, Estadual, Municipal, VigenIni, VigenFim, Chave, Versao,
            FonteStr);
          //
          InsereRegistro(Codigo, EX, Tabela, Descricao, AliqNac, AliqImp,
            Estadual, Municipal, VigenIni, VigenFim, Chave, Versao, FonteStr, UF);
        end;
      end;
      //
      Geral.MB_Aviso('Carregamento finalizado!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB1.Position := 0;
    finally
      if lstArq <> nil then
        lstArq.Free;
    end;
  end;
end;

procedure TFmIBPTaxLoad.CarregaTabela2();
var
  I, Usuario, Termo: Integer;
  Linha, Codigo, EX, Tabela, Descricao, AliqNac, AliqImp, Estadual, Municipal,
  VigenIni, VigenFim, Chave, Versao, FonteStr, Arquivo, UF: String;
  lstArq: TStringList;
  DtaHora: TDateTime;
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� precisa selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  if not InputQuery('Sigla do estado (UF)', 'Digite a sigla do estado (UF):', UF) then Exit;
  //
  UF := Geral.FormataUF(UpperCase(UF));
  //
  if UF = '' then
  begin
    Geral.MB_Aviso('UF inv�lido!');
    Exit
  end;
  //
  if Pos(UF, ExtractFileName(EdCaminho.ValueVariant)) = 0 then
  begin
    if Geral.MB_Pergunta('O arquivo que voc� selecionou n�o parece ser da UF "NOMEUF"!' +
      sLineBreak + 'As al�quotas podem ser diferentes de estado para estado' + sLineBreak +
      'Mas se voc� deseja importar precione o bot�o "Sim" caso contr�rio clique no bot�o "N�o" ou "Cancela" e selecione o arquivo correto!') <> ID_YES
    then
      Exit;
  end;
  //
  if UWAceites.AceitaTermos(istIBPTax, Termo, Usuario, DtaHora) then
  begin
    if not UMyMod.SQLInsUpd(DMod.QrUpd, stUpd, 'OpcoesGerl', False,
      ['IBPTax_TermoAceite', 'IBPTax_DataHoraAceite', 'IBPTax_UsuarioAceite'], ['Codigo'],
      [Termo, Geral.FDT(DtaHora, 109), Usuario], [1], False) then
    begin
      Geral.MB_Aviso('Falha ao atualizar dados!');
      Exit;
    end;
  end else
  begin
    Geral.MB_Aviso('Importa��o abortada!');
    Exit;
  end;
  //
{
  MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
}
  //FItens := 0;
  Arquivo := EdCaminho.Text;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    lstArq := TStringList.Create;
    try
      lstArq.LoadFromFile(Arquivo);
      if lstArq.Count > 0 then
      begin
        PB1.Position := 0;
        PB1.Max := lstArq.Count;
        //
        Linha := lstArq[0];
        //
(*
        for I := 1 to lstArq.Count - 1 do
        begin
          //PB1.Position := PB1.Position + 1;
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //
          Linha := lstArq[I];
          //
          ObtemCodigoNome1(Linha, Codigo, EX, Tabela, Descricao, AliqNac,
            AliqImp, Estadual, Municipal, VigenIni, VigenFim, Chave, Versao,
            FonteStr);
          //
          InsereRegistro(Codigo, EX, Tabela, Descricao, AliqNac, AliqImp,
            Estadual, Municipal, VigenIni, VigenFim, Chave, Versao, FonteStr, UF);
        end;
*)
        ImportaMassivo(lstArq, UF);
      end;
      //
      Geral.MB_Aviso('Carregamento finalizado!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB1.Position := 0;
    finally
      if lstArq <> nil then
        lstArq.Free;
    end;
  end;
end;

procedure TFmIBPTaxLoad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdCaminho.CharCase := ecNormal;
  //EdCaminho.ValueVariant := FURL;
end;

procedure TFmIBPTaxLoad.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmIBPTaxLoad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIBPTaxLoad.ImportaMassivo(lstArq: TStringList; UF: String);
const
  ItensPorVez = 100;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec, Linha, SQL_LinAtu: String;
  I, ItsOK, Total, Step, ItemAtuDoBloco, ItemAtuDoTotal, ItemFimDoBloco: Integer;
  Continua: Boolean;
  //
begin
  Screen.Cursor := crHourGlass;
  //
  try
  MySQLBatchExecute1.Database := DModG.AllID_DB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ibptax ( ',
    'Codigo, EX, Tabela, UF, Descricao, ' +
    'AliqNac, AliqImp, Estadual, Municipal, ' +
    'VigenIni, VigenFim, Chave, Versao, FonteStr) VALUES ']);
  //

  ItemAtuDoBloco := 1;  // primeira linha (0) tem os nomes dos campos!
  ItemAtuDoTotal := 1;
  ItsOK   := 0;

  //

  //
  Total := lstArq.Count;
  Step :=  ItensPorVez;
  PB1.Position := 0;
  PB1.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(DModG.AllID_DB, 'DELETE FROM ibptax');
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB1, nil, nil);
    SQL := 'SELECT ';
    //if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro ' +
        Geral.FF0(ItemAtuDoTotal) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      //while not SDMarca.Eof do
      begin
        //

        ItemFimDoBloco := ItemFimDoBloco + Step;
        if ItemFimDoBloco >= Total then
          ItemFimDoBloco := Total;
        //
        ItemAtuDoBloco := 1;
        SQLRec := '';
        for I := ItemAtuDoTotal to ItemFimDoBloco - 1 do
        begin
          //PB1.Position := PB1.Position + 1;
          //
          if ItemAtuDoBloco = 1 then
            SQLRec    := SQLRec + ' ('
          else
            SQLRec    := SQLRec + ', (';
          //
          Linha := lstArq[I];
          //
          SQL_LinAtu := ObtemCodigoNome2(Linha, UF);
          //
          SQLRec := SQLRec + SQL_LinAtu + ') ';
          //
          ItemAtuDoTotal := ItemAtuDoTotal + 1;
          ItemAtuDoBloco := ItemAtuDoBloco + 1;
        end;

        //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  //PB1.Position := Max?
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Registro ' +
    //Geral.FF0(ItemAtuDoTotal + ItsOK) + ' de ' + Geral.FF0(Total));
    Geral.FF0(ItemAtuDoTotal) + ' de ' + Geral.FF0(Total));
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmIBPTaxLoad.InsereRegistro(Codigo, EX, Tabela, Descricao, AliqNac,
  AliqImp, Estadual, Municipal, VigenIni, VigenFim, Chave, Versao, FonteStr, UF: String);
var
  Nome: String;
begin
  //Geral.MB_Aviso(Codigo);
  Nome := Copy(Descricao, 1, 255);
  if EX = '' then
    EX := '0';
  //
  UMyMod.SQLIns_ON_DUPLICATE_KEY(DModG.QrAllUpd, 'ibptax', False,
    ['Nome', 'Descricao', 'AliqNac', 'AliqImp', 'Estadual', 'Municipal', 'VigenIni', 'VigenFim', 'Chave', 'FonteStr'],
    ['Codigo', 'EX', 'Tabela', 'UF', 'Versao'],
    ['Nome', 'Descricao', 'AliqNac', 'AliqImp', 'Estadual', 'Municipal', 'VigenIni', 'VigenFim', 'Chave', 'FonteStr'],
    [Nome, Descricao, AliqNac, AliqImp, Estadual, Municipal, VigenIni, VigenFim, Chave, FonteStr],
    [Codigo, EX, Tabela, UF, Versao],
    [Nome, Descricao, AliqNac, AliqImp, Estadual, Municipal, VigenIni, VigenFim, Chave, FonteStr],
    False);
end;

procedure TFmIBPTaxLoad.ObtemCodigoNome1(const Linha: String; var Codigo, EX,
  Tabela, Descricao, AliqNac, AliqImp, Estadual, Municipal, VigenIni, VigenFim,
  Chave, Versao, FonteStr: String);
var
  lstLin: TStringList;
  P: Integer;
begin
  codigo    := '';
  ex        := '';
  tabela    := '';
  descricao := '';
  aliqNac   := '';
  aliqImp   := '';
  //

  P := pos(';', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      Geral.MyExtractStrings([';'], [' '], PChar(Linha + ';'), lstLin);
      Codigo  := Copy(Linha, 1, P-1);
      if lstLin.Count > 0 then
        EX := lstLin[00];
      if lstLin.Count > 1 then
        Tabela := lstLin[01];
      if lstLin.Count > 2 then
        Descricao := lstLin[02];
      if lstLin.Count > 3 then
        AliqNac := lstLin[03];
      if lstLin.Count > 4 then
        AliqImp := lstLin[04];
      if lstLin.Count > 5 then
        Estadual := lstLin[05];
      if lstLin.Count > 6 then
        Municipal := lstLin[06];
      if lstLin.Count > 7 then
        VigenIni := FormatDateTime('yyyy-mm-dd', Geral.ValidaDataBR(lstLin[07], True, False));
      if lstLin.Count > 8 then
        VigenFim := FormatDateTime('yyyy-mm-dd', Geral.ValidaDataBR(lstLin[08], True, False));
      if lstLin.Count > 9 then
        Chave := lstLin[09];
      if lstLin.Count > 10 then
        Versao := lstLin[10];
      if lstLin.Count > 11 then
        FonteStr := lstLin[11];
    finally
      if lstLin <> nil then
        lstLin.Free;
    end;
  end;
end;

function TFmIBPTaxLoad.ObtemCodigoNome2(const Linha, UF: String): String;
var
  lstLin: TStringList;
  P: Integer;//
var
  Codigo, EX, Tabela, Descricao, AliqNac, AliqImp, Estadual, Municipal,
  VigenIni, VigenFim, Chave, Versao, FonteStr: String;
begin
  codigo    := '';
  ex        := '';
  tabela    := '';
  descricao := '';
  aliqNac   := '';
  aliqImp   := '';
  //

  P := pos(';', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      Geral.MyExtractStrings([';'], [' '], PChar(Linha + ';'), lstLin);
      Codigo  := Copy(Linha, 1, P-1);
      if lstLin.Count > 0 then
        EX := lstLin[00];
      if lstLin.Count > 1 then
        Tabela := lstLin[01];
      if lstLin.Count > 2 then
        Descricao := lstLin[02];
      if lstLin.Count > 3 then
        AliqNac := lstLin[03];
      if lstLin.Count > 4 then
        AliqImp := lstLin[04];
      if lstLin.Count > 5 then
        Estadual := lstLin[05];
      if lstLin.Count > 6 then
        Municipal := lstLin[06];
      if lstLin.Count > 7 then
        VigenIni := FormatDateTime('yyyy-mm-dd', Geral.ValidaDataBR(lstLin[07], True, False));
      if lstLin.Count > 8 then
        VigenFim := FormatDateTime('yyyy-mm-dd', Geral.ValidaDataBR(lstLin[08], True, False));
      if lstLin.Count > 9 then
        Chave := lstLin[09];
      if lstLin.Count > 10 then
        Versao := lstLin[10];
      if lstLin.Count > 11 then
        FonteStr := lstLin[11];
    finally
      if lstLin <> nil then
        lstLin.Free;
    end;
    Result :=
    Geral.FF0(Geral.IMV(Codigo))                                        + ', ' +
    Geral.FF0(Geral.IMV(EX))                                            + ', ' +
    Geral.VariavelToString(Tabela)                                      + ', ' +
    '"' + UF + '"'                                                      + ', ' +
    Geral.VariavelToString(Descricao)                                   + ', ' +
    (*Geral.FFT_Dot(Geral.DMV(AliqNac), 10, siNegativo)  *)  AliqNac    + ', ' +
    (*Geral.FFT_Dot(Geral.DMV(AliqImp), 10, siNegativo)  *)  AliqImp    + ', ' +
    (*Geral.FFT_Dot(Geral.DMV(Estadual), 10, siNegativo) *)  Estadual   + ', ' +
    (*Geral.FFT_Dot(Geral.DMV(Municipal), 10, siNegativo)*)  Municipal  + ', ' +
    '"' + VigenIni + '"'                                                + ', ' +
    '"' + VigenFim + '"'                                                + ', ' +
    Geral.VariavelToString(Chave)                                       + ', ' +
    Geral.VariavelToString(Versao)                                      + ', ' +
    Geral.VariavelToString(FonteStr)                                    +
    '';
  end;
end;

(*
function TFmIBPTaxLoad.ObtemVersao(Linha: String): String;
var
  lstLin: TStringList;
  P: Integer;
begin
  Result := '';
  //
  P := pos(';', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      Geral.MyExtractStrings([';'], [' '], PChar(Linha + ';'), lstLin);
      if lstLin.Count > 5 then
        Result := lstLin[05];
    finally
      if lstLin <> nil then
        lstLin.Free;
    end;
  end;
end;
*)

procedure TFmIBPTaxLoad.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(FDestFile);
  Arquivo := ExtractFileName(FDestFile);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdCaminho.Text := Arquivo;
end;

end.
