object FmPesqCNAE21: TFmPesqCNAE21
  Left = 339
  Top = 185
  Caption = 'ENT-CNAE2-002 :: Pesquisa CNAE 2.1'
  ClientHeight = 531
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 369
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter4: TSplitter
      Left = 0
      Top = 208
      Width = 1008
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 1
      ExplicitTop = 129
      ExplicitWidth = 148
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 208
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 227
        Top = 0
        Height = 208
        ExplicitLeft = 5
        ExplicitTop = 1
        ExplicitHeight = 126
      end
      object Splitter2: TSplitter
        Left = 486
        Top = 0
        Height = 208
        ExplicitLeft = 516
        ExplicitTop = -4
        ExplicitHeight = 126
      end
      object Splitter3: TSplitter
        Left = 745
        Top = 0
        Height = 208
        ExplicitLeft = 775
        ExplicitTop = 5
        ExplicitHeight = 126
      end
      object Panel4: TPanel
        Left = 748
        Top = 0
        Width = 260
        Height = 208
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 260
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesq4: TdmkEdit
            Left = 0
            Top = 0
            Width = 260
            Height = 21
            Align = alClient
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdPesq4Exit
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 21
          Width = 260
          Height = 187
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CodTxt'
              Title.Caption = 'ID'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end>
          Color = clWindow
          DataSource = Ds4
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodTxt'
              Title.Caption = 'ID'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end>
        end
      end
      object Panel6: TPanel
        Left = 489
        Top = 0
        Width = 256
        Height = 208
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 256
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesq3: TdmkEdit
            Left = 0
            Top = 0
            Width = 256
            Height = 21
            Align = alClient
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdPesq3Exit
          end
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 21
          Width = 256
          Height = 187
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CodTxt'
              Title.Caption = 'ID'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end>
          Color = clWindow
          DataSource = Ds3
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodTxt'
              Title.Caption = 'ID'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end>
        end
      end
      object Panel8: TPanel
        Left = 230
        Top = 0
        Width = 256
        Height = 208
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 256
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesq2: TdmkEdit
            Left = 0
            Top = 0
            Width = 256
            Height = 21
            Align = alClient
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdPesq2Exit
          end
        end
        object dmkDBGrid3: TdmkDBGrid
          Left = 0
          Top = 21
          Width = 256
          Height = 187
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CodTxt'
              Title.Caption = 'ID'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end>
          Color = clWindow
          DataSource = Ds2
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodTxt'
              Title.Caption = 'ID'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 160
              Visible = True
            end>
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 227
        Height = 208
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 3
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 227
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdPesq1: TdmkEdit
            Left = 0
            Top = 0
            Width = 227
            Height = 21
            Align = alClient
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdPesq1Exit
          end
        end
        object dmkDBGrid4: TdmkDBGrid
          Left = 0
          Top = 21
          Width = 227
          Height = 187
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'CodAlf'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 150
              Visible = True
            end>
          Color = clWindow
          DataSource = Ds1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodAlf'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 150
              Visible = True
            end>
        end
      end
    end
    object Panel12: TPanel
      Left = 0
      Top = 211
      Width = 1008
      Height = 158
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 21
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object EdPesq5: TdmkEdit
          Left = 0
          Top = 0
          Width = 1008
          Height = 21
          Align = alClient
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdPesq5Exit
        end
      end
      object dmkDBGrid5: TdmkDBGrid
        Left = 0
        Top = 21
        Width = 1008
        Height = 137
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CodTxt'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 450
            Visible = True
          end>
        Color = clWindow
        DataSource = Ds5
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodTxt'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 450
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 240
        Height = 32
        Caption = 'Pesquisa CNAE 2.1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 240
        Height = 32
        Caption = 'Pesquisa CNAE 2.1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 240
        Height = 32
        Caption = 'Pesquisa CNAE 2.1'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 417
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 461
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel14: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
    end
  end
  object Qr1: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = Qr1BeforeClose
    AfterScroll = Qr1AfterScroll
    SQL.Strings = (
      'SELECT CodAlf, Nome '
      'FROM cnae21cad'
      'WHERE Nome LIKE :P0')
    Left = 296
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object Qr1CodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object Qr1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object Ds1: TDataSource
    DataSet = Qr1
    Left = 324
    Top = 11
  end
  object Ds2: TDataSource
    DataSet = Qr2
    Left = 380
    Top = 11
  end
  object Qr2: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = Qr2BeforeClose
    AfterScroll = Qr2AfterScroll
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM cnae21cad'
      'WHERE LENGTH(CodTxt) = 2'
      'AND CodAlf LIKE :P0'
      'AND Nome LIKE :P1')
    Left = 356
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr2CodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object Qr2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object Ds3: TDataSource
    DataSet = Qr3
    Left = 436
    Top = 11
  end
  object Qr3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = Qr3BeforeClose
    AfterScroll = Qr3AfterScroll
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM cnae21cad'
      'WHERE LENGTH(CodTxt) = 3'
      'AND LEFT(CodTxt, 2) = :P0'
      'AND Nome LIKE :P1')
    Left = 408
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr3CodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object Qr3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object Ds4: TDataSource
    DataSet = Qr4
    Left = 492
    Top = 11
  end
  object Qr4: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = Qr4BeforeClose
    AfterScroll = Qr4AfterScroll
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM cnae21cad'
      'WHERE LENGTH(CodTxt) = 5'
      'AND LEFT(CodTxt, 3) = :P0'
      'AND Nome LIKE :P1')
    Left = 464
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr4CodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object Qr4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object Ds5: TDataSource
    DataSet = Qr5
    Left = 548
    Top = 11
  end
  object Qr5: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM cnae21cad'
      'WHERE LENGTH(CodTxt) = 7'
      'AND LEFT(CodTxt, 5) = :P0'
      'AND Nome LIKE :P1')
    Left = 520
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr5CodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object Qr5Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
end
