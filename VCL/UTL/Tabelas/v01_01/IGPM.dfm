object FmIGPM: TFmIGPM
  Left = 339
  Top = 185
  ActiveControl = PageControl1
  Caption = 'TAB-IGPM1-001 :: Importa'#231#227'o do IGP-M'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 96
      Width = 1008
      Height = 242
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Abertos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 214
          Align = alClient
          ColCount = 2
          DefaultColWidth = 44
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
          ExplicitLeft = -3
          ExplicitTop = -2
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Carregados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 214
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'ANOMES_TXT'
              Title.Caption = 'M'#234's / Ano'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGPM_D'
              Title.Caption = #205'ndice'
              Width = 75
              Visible = True
            end>
          Color = clWindow
          DataSource = DsIGPM
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ANOMES_TXT'
              Title.Caption = 'M'#234's / Ano'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGPM_D'
              Title.Caption = #205'ndice'
              Width = 75
              Visible = True
            end>
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 96
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label27: TLabel
        Left = 8
        Top = 7
        Width = 116
        Height = 13
        Caption = 'Arquivo a ser carregado:'
      end
      object SpeedButton8: TSpeedButton
        Left = 974
        Top = 23
        Width = 21
        Height = 21
        OnClick = SpeedButton8Click
      end
      object LaAviso: TLabel
        Left = 12
        Top = 48
        Width = 15
        Height = 22
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object EdCaminho: TdmkEdit
        Left = 8
        Top = 23
        Width = 965
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DirNFeGer'
        UpdCampo = 'DirNFeGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object PB1: TProgressBar
        Left = 12
        Top = 72
        Width = 985
        Height = 17
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 260
        Height = 32
        Caption = 'Importa'#231#227'o do IGP-M'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 260
        Height = 32
        Caption = 'Importa'#231#227'o do IGP-M'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 260
        Height = 32
        Caption = 'Importa'#231#227'o do IGP-M'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 430
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BitBtn3: TBitBtn
        Tag = 19
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Baixar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn3Click
      end
      object BtAbre: TBitBtn
        Left = 130
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abre xls'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAbreClick
      end
      object BtCarrega: TBitBtn
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Carrega xls'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtCarregaClick
      end
    end
  end
  object QrIGPM: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrIGPMCalcFields
    SQL.Strings = (
      'SELECT AnoMes, IGPM_D'
      'FROM igpm'
      'ORDER BY AnoMes DESC')
    Left = 32
    Top = 204
    object QrIGPMIGPM_D: TFloatField
      FieldName = 'IGPM_D'
    end
    object QrIGPMAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrIGPMANOMES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ANOMES_TXT'
      Calculated = True
    end
  end
  object DsIGPM: TDataSource
    DataSet = QrIGPM
    Left = 60
    Top = 204
  end
end
