unit IBPTaxCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkMemo, dmkLabelRotate, Menus,
  frxClass, frxDBSet, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  Vcl.Grids, Vcl.DBGrids;

type
  TFmIBPTaxCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrIBPTaxIts: TmySQLQuery;
    DsIBPTaxIts: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel7: TPanel;
    Panel8: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    MeNome: TdmkMemo;
    PMExclui: TPopupMenu;
    ExcluiItematual1: TMenuItem;
    ExcluiTODOSitens1: TMenuItem;
    BtImporta: TBitBtn;
    QrIBPTaxItsEx: TIntegerField;
    QrIBPTaxItsTabela: TIntegerField;
    QrIBPTaxItsNome: TWideStringField;
    QrIBPTaxItsDescricao: TWideMemoField;
    QrIBPTaxItsAliqNac: TFloatField;
    QrIBPTaxItsAliqImp: TFloatField;
    QrIBPTaxItsVersao: TWideStringField;
    QrIBPTaxItsTABELA_NO: TWideStringField;
    Label18: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdEx: TdmkEdit;
    Label5: TLabel;
    EdTabela: TdmkEdit;
    EdFonteStr: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    Label19: TLabel;
    EdAliqNac: TdmkEdit;
    EdAliqImp: TdmkEdit;
    Label20: TLabel;
    Label21: TLabel;
    EdMunicipal: TdmkEdit;
    EdEstadual: TdmkEdit;
    Label22: TLabel;
    Label23: TLabel;
    TPVigenIni: TdmkEditDateTimePicker;
    Label24: TLabel;
    TPVigenFim: TdmkEditDateTimePicker;
    Label25: TLabel;
    EdChave: TdmkEdit;
    EdVersao: TdmkEdit;
    Label26: TLabel;
    QrIBPTaxItsVigenIni_TXT: TWideStringField;
    QrIBPTaxItsVigenFim_TXT: TWideStringField;
    QrIBPTaxItsEstadual: TFloatField;
    QrIBPTaxItsMunicipal: TFloatField;
    QrIBPTaxItsFonteStr: TWideStringField;
    QrIBPTaxItsChave: TWideStringField;
    ExcluiExpirados1: TMenuItem;
    QrIBPTaxItsVigenIni: TDateField;
    QrIBPTaxItsVigenFim: TDateField;
    QrIBPTaxItsCodigo: TLargeintField;
    Label27: TLabel;
    EdUF: TdmkEdit;
    QrIBPTaxItsUF: TWideStringField;
    GBDados: TGroupBox;
    DBGIBPTax: TDBGrid;
    DBGIBPTaxIts: TDBGrid;
    QrIBPTax: TmySQLQuery;
    DsIBPTax: TDataSource;
    QrIBPTaxCodigo: TLargeintField;
    Panel9: TPanel;
    PnNome: TPanel;
    dmkLabelRotate1: TdmkLabelRotate;
    DBMemo1: TDBMemo;
    Panel6: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label28: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    dmkDBEdit13: TdmkDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure ExcluiItematual1Click(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure ExcluiExpirados1Click(Sender: TObject);
    procedure ExcluiTODOSitens1Click(Sender: TObject);
    procedure QrIBPTaxBeforeClose(DataSet: TDataSet);
    procedure QrIBPTaxAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Double);
    procedure Va(Para: TVaiPara);
    procedure ReopenIBPTaxIts();
  public
    { Public declarations }
  end;

var
  FmIBPTaxCad: TFmIBPTaxCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, ModuleGeral, IBPTaxLoad, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmIBPTaxCad.LocCod(Atual, Codigo: Double);
begin
  DefParams;
  GOTOy.LC(Trunc(Atual), Trunc(Codigo));
end;

procedure TFmIBPTaxCad.PMExcluiPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrIBPTax.State <> dsInactive) and (QrIBPTax.RecordCount > 0);
  //
  ExcluiItematual1.Enabled  := Enab;
  ExcluiExpirados1.Enabled  := Enab;
  ExcluiTODOSitens1.Enabled := Enab;
end;

procedure TFmIBPTaxCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrIBPTaxCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmIBPTaxCad.DefParams;
begin
  VAR_GOTOTABELA := 'ibptax';
  VAR_GOTOMYSQLTABLE := QrIBPTax;
  VAR_GOTONEG := gotoNiP;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := DModG.AllID_DB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT DISTINCT ibp.Codigo ');
  VAR_SQLx.Add('FROM ibptax ibp ');
  VAR_SQLx.Add('WHERE ibp.Codigo <> 0 ');
  //
  VAR_SQL1.Add('AND ibp.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ibp.CodTxt=:P0');
  //
  VAR_SQLa.Add('AND ibp.Nome LIKE :P0');
end;

procedure TFmIBPTaxCad.ReopenIBPTaxIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIBPTaxIts, DModG.AllID_DB, [
    'SELECT ibp.Codigo, ibp.*, ',
    'ELT(ibp.Tabela + 1, "NCM", "NBS", "Item da LC 116", "???") TABELA_NO, ',
    'IF(ibp.VigenIni <= "1899-12-30", "", DATE_FORMAT(ibp.VigenIni, "%d/%m/%Y")) VigenIni_TXT, ',
    'IF(ibp.VigenFim <= "1899-12-30", "", DATE_FORMAT(ibp.VigenFim, "%d/%m/%Y")) VigenFim_TXT ',
    'FROM ibptax ibp ',
    'WHERE ibp.Codigo=' + Geral.FF0(QrIBPTaxCodigo.Value),
    '']);
end;

procedure TFmIBPTaxCad.ExcluiExpirados1Click(Sender: TObject);
var
  Codigo: Integer;
  VigenFim: String;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o dos registros expirados?') = ID_YES then
  begin
    Codigo   := QrIBPTaxCodigo.Value;
    VigenFim := Geral.FDT(QrIBPTaxItsVigenFim.Value, 1);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'DELETE FROM ibptax WHERE VigenFim < "'+ VigenFim + '"',
      '']);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmIBPTaxCad.ExcluiItematual1Click(Sender: TObject);
var
  Codigo, Ex, Tabela: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do registro atual?') = ID_YES then
  begin
    Codigo := QrIBPTaxCodigo.Value;
    Ex     := QrIBPTaxItsEx.Value;
    Tabela := QrIBPTaxItsTabela.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'DELETE FROM ibptax WHERE Codigo='+ Geral.FF0(Codigo),
      ' AND Ex='+ Geral.FF0(Ex),
      ' AND Tabela=' + Geral.FF0(Tabela),
      '']);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmIBPTaxCad.ExcluiTODOSitens1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o de todos os registros?') = ID_YES then
  begin
    Codigo := QrIBPTaxCodigo.Value;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'DELETE FROM ibptax ',
      '']);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmIBPTaxCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmIBPTaxCad.QrIBPTaxAfterScroll(DataSet: TDataSet);
begin
  ReopenIBPTaxIts;
end;

procedure TFmIBPTaxCad.QrIBPTaxBeforeClose(DataSet: TDataSet);
begin
  QrIBPTaxIts.Close;
end;

procedure TFmIBPTaxCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmIBPTaxCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmIBPTaxCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmIBPTaxCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmIBPTaxCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmIBPTaxCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmIBPTaxCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIBPTaxCad.BtAlteraClick(Sender: TObject);
begin
  if (QrIBPTax.State <> dsInactive) and (QrIBPTax.RecordCount > 0) then
  begin
    EdCodigo.Enabled := False;
    //
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrIBPTax, [PnDados],
      [PnEdita], EdEx, ImgTipo, 'ibptax');
  end;
end;

procedure TFmIBPTaxCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrIBPTaxCodigo.Value;
  Close;
end;

procedure TFmIBPTaxCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  UF: String;
  VigenIni, VigenFim: TDate;
begin
  Codigo   := EdCodigo.ValueVariant;
  VigenIni := TPVigenIni.Date;
  VigenFim := TPVigenFim.Date;
  UF       := Geral.FormataUF(EdUF.ValueVariant);
  //
  if MyObjects.FIC(Codigo = 0, EdCodigo, 'Defina um C�digo v�lido!') then Exit;
  if MyObjects.FIC(VigenIni = 0, TPVigenIni, 'Defina o in�cio da vig�ncia!') then Exit;
  if MyObjects.FIC(VigenFim = 0, TPVigenFim, 'Defina o final da vig�ncia!') then Exit;
  if MyObjects.FIC(UF = '', EdUF, 'UF n�o definido ou inv�lido!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'ibptax', Codigo, DModG.QrAllUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmIBPTaxCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, DModG.AllID_DB, 'ibptax', 'Codigo');
end;

procedure TFmIBPTaxCad.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmIBPTaxCad.BtImportaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIBPTaxLoad, FmIBPTaxLoad, afmoNegarComAviso) then
  begin
    FmIBPTaxLoad.ShowModal;
    FmIBPTaxLoad.Destroy;
    //
    Va(vpLast);
  end;
end;

procedure TFmIBPTaxCad.BtIncluiClick(Sender: TObject);
begin
  EdCodigo.Enabled := True;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrIBPTax, [PnDados],
    [PnEdita], EdCodigo, ImgTipo, 'ibptax');
end;

procedure TFmIBPTaxCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  GBEdita.Align     := alClient;
  GBDados.Align     := alClient;
  QrIBPTax.Database := DModG.AllID_DB;
  //
  DBGIBPTax.DataSource    := DsIBPTax;
  DBGIBPTaxIts.DataSource := DsIBPTaxIts;
  //
  CriaOForm;
end;

procedure TFmIBPTaxCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrIBPTaxCodigo.Value, LaRegistro.Caption);
end;

procedure TFmIBPTaxCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmIBPTaxCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmIBPTaxCad.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu((QrIBPTaxCodigo.Value), LaRegistro.Caption);
end;

procedure TFmIBPTaxCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmIBPTaxCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmIBPTaxCad.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrIBPTaxCodigo.Value,
  CuringaLoc.CriaForm5(CO_CODIGO, CO_NOME, 'ibptax', DModG.AllID_DB, CO_VAZIO));
  *)
  dmkPF.LeMeuTexto(QrIBPTax.SQL.Text);
end;

procedure TFmIBPTaxCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

