unit IGPMCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGrid, dmkEdit,
  mySQLDbTables, UnDmkProcFunc;

type
  TFmIGPMCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    BtExclui: TBitBtn;
    PnDados: TPanel;
    Label32: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAnoI: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    Panel7: TPanel;
    BtConfirma: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    SbImporta: TBitBtn;
    Label1: TLabel;
    EdIndice: TdmkEdit;
    QrIGPM: TmySQLQuery;
    QrIGPMIGPM_D: TFloatField;
    QrIGPMAnoMes: TIntegerField;
    QrIGPMANOMES_TXT: TWideStringField;
    DsIGPM: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbImportaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrIGPMCalcFields(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType);
  public
    { Public declarations }
  end;

  var
  FmIGPMCad: TFmIGPMCad;

implementation

uses UnMyObjects, Module, IGPM, MyDBCheck, ModuleGeral,
  UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmIGPMCad.BtConfirmaClick(Sender: TObject);
var
  Periodo: Integer;
  Indice: Double;
begin
  Periodo := dmkPF.PeriodoEncode(Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex + 1);
  Indice  := EdIndice.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM igpm ',
    'WHERE AnoMes=' + Geral.FF0(Periodo),
    '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Geral.MB_Aviso('O �ndice para o periodo selecionado j� foi cadastrado!');
    Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'igpm', False,
    ['IGPM_D'], ['AnoMes'], [Indice], [Periodo], False) then
  begin
    UMyMod.AbreQuery(QrIGPM, Dmod.MyDB);
    MostraEdicao(stLok);
  end;
end;

procedure TFmIGPMCad.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmIGPMCad.BtExcluiClick(Sender: TObject);
var
  Periodo: Integer;
begin
  if (QrIGPM.State <> dsInactive) and (QrIGPM.RecordCount > 0) then
  begin
    Periodo := QrIGPMAnoMes.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do �ndice ' +
      FloatToStr(QrIGPMIGPM_D.Value) + '?', 'igpm', 'AnoMes', Periodo, Dmod.MyDB) <> 0
    then
      UMyMod.AbreQuery(QrIGPM, Dmod.MyDB);
  end;
end;

procedure TFmIGPMCad.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmIGPMCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmIGPMCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmIGPMCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao(stLok);
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, 0);
  //
  UMyMod.AbreQuery(QrIGPM, Dmod.MyDB);
end;

procedure TFmIGPMCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmIGPMCad.MostraEdicao(SQLType: TSQLType);
var
  Data: TDateTime;
  Dia, Mes, Ano: Word;
begin
  if SQLType = stIns then
  begin
    dmkDBGrid1.Enabled := False;
    PnDados.Visible    := True;
    BtInclui.Visible   := False;
    BtExclui.Visible   := False;
    //
    Data := DModG.ObtemAgora - 30;
    DecodeDate(Data, Ano, Mes, Dia);
    //
    CBMes.ItemIndex := (Mes - 1);
    CBAno.ItemIndex := (Ano - 2000);
    //
    EdIndice.ValueVariant := 0;
    CBMes.SetFocus;
  end else
  begin
    dmkDBGrid1.Enabled := True;
    PnDados.Visible    := False;
    BtInclui.Visible   := True;
    BtExclui.Visible   := True;
  end;
end;

procedure TFmIGPMCad.QrIGPMCalcFields(DataSet: TDataSet);
begin
  QrIGPMANOMES_TXT.Value := dmkPF.MesEAnoDoPeriodo(QrIGPMAnoMes.Value);
end;

procedure TFmIGPMCad.SbImportaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmIGPM, FmIGPM, afmoNegarComAviso) then
  begin
    FmIGPM.ShowModal;
    FmIGPM.Destroy;
    //
    UMyMod.AbreQuery(QrIGPM, Dmod.MyDB);
  end;
end;

end.
