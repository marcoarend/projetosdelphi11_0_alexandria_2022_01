object FmIGPMCad: TFmIGPMCad
  Left = 339
  Top = 185
  Caption = 'TAB-IGPM1-002 :: IGP-M - Cadastro'
  ClientHeight = 452
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 496
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 448
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImporta: TBitBtn
        Tag = 39
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImportaClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 400
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 213
        Height = 32
        Caption = 'IGP-M - Cadastro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 213
        Height = 32
        Caption = 'IGP-M - Cadastro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 213
        Height = 32
        Caption = 'IGP-M - Cadastro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 496
    Height = 290
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 496
      Height = 290
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 496
        Height = 290
        Align = alClient
        TabOrder = 0
        object PnDados: TPanel
          Left = 2
          Top = 15
          Width = 492
          Height = 130
          Align = alTop
          TabOrder = 0
          object Label32: TLabel
            Left = 12
            Top = 7
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object LaAnoI: TLabel
            Left = 166
            Top = 7
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object Label1: TLabel
            Left = 236
            Top = 7
            Width = 32
            Height = 13
            Caption = #205'ndice:'
          end
          object CBMes: TComboBox
            Left = 12
            Top = 24
            Width = 150
            Height = 21
            Style = csDropDownList
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object CBAno: TComboBox
            Left = 166
            Top = 24
            Width = 64
            Height = 21
            Style = csDropDownList
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object GroupBox2: TGroupBox
            Left = 1
            Top = 59
            Width = 490
            Height = 70
            Align = alBottom
            TabOrder = 3
            object Panel6: TPanel
              Left = 344
              Top = 15
              Width = 144
              Height = 53
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtDesiste: TBitBtn
                Tag = 15
                Left = 15
                Top = 4
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
              end
            end
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 342
              Height = 53
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object BtConfirma: TBitBtn
                Tag = 14
                Left = 9
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Confirma'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtConfirmaClick
              end
            end
          end
          object EdIndice: TdmkEdit
            Left = 236
            Top = 24
            Width = 120
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 145
          Width = 492
          Height = 143
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'ANOMES_TXT'
              Title.Caption = 'Per'#237'odo'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGPM_D'
              Title.Caption = #205'ndice'
              Width = 90
              Visible = True
            end>
          Color = clWindow
          DataSource = DsIGPM
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'ANOMES_TXT=AnoMes')
          Columns = <
            item
              Expanded = False
              FieldName = 'ANOMES_TXT'
              Title.Caption = 'Per'#237'odo'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGPM_D'
              Title.Caption = #205'ndice'
              Width = 90
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 338
    Width = 496
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 492
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 382
    Width = 496
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 350
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 348
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 135
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtExcluiClick
      end
    end
  end
  object QrIGPM: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrIGPMCalcFields
    SQL.Strings = (
      'SELECT AnoMes, IGPM_D'
      'FROM igpm'
      'ORDER BY AnoMes DESC')
    Left = 232
    Top = 220
    object QrIGPMIGPM_D: TFloatField
      FieldName = 'IGPM_D'
    end
    object QrIGPMAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrIGPMANOMES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ANOMES_TXT'
      Calculated = True
    end
  end
  object DsIGPM: TDataSource
    DataSet = QrIGPM
    Left = 260
    Top = 220
  end
end
