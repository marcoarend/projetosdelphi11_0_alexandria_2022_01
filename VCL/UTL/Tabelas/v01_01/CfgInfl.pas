unit CfgInfl;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmCfgInfl = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCfgInfl: TmySQLQuery;
    QrCfgInflCodigo: TIntegerField;
    QrCfgInflNome: TWideStringField;
    DsCfgInfl: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GroupBox1: TGroupBox;
    RGIndic1: TdmkRadioGroup;
    RGMesAnt1: TdmkRadioGroup;
    Panel6: TPanel;
    Label3: TLabel;
    EdPercent1: TdmkEdit;
    GroupBox2: TGroupBox;
    RGIndic2: TdmkRadioGroup;
    RGMesAnt2: TdmkRadioGroup;
    Panel7: TPanel;
    Label4: TLabel;
    EdPercent2: TdmkEdit;
    GroupBox3: TGroupBox;
    DBRGIndic1: TDBRadioGroup;
    dmkRadioGroup2: TDBRadioGroup;
    Panel8: TPanel;
    Label5: TLabel;
    dmkEdit1: TDBEdit;
    GroupBox4: TGroupBox;
    DBRGIndic2: TDBRadioGroup;
    dmkRadioGroup4: TDBRadioGroup;
    Panel9: TPanel;
    Label6: TLabel;
    dmkEdit2: TDBEdit;
    QrCfgInflIndic1: TSmallintField;
    QrCfgInflPercent1: TFloatField;
    QrCfgInflMesAnt1: TSmallintField;
    QrCfgInflIndic2: TSmallintField;
    QrCfgInflPercent2: TFloatField;
    QrCfgInflMesAnt2: TSmallintField;
    QrCfgInflLk: TIntegerField;
    QrCfgInflDataCad: TDateField;
    QrCfgInflDataAlt: TDateField;
    QrCfgInflUserCad: TIntegerField;
    QrCfgInflUserAlt: TIntegerField;
    QrCfgInflAlterWeb: TSmallintField;
    QrCfgInflAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCfgInflAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCfgInflBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCfgInfl: TFmCfgInfl;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, InflIdx_Tabs;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCfgInfl.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCfgInfl.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCfgInflCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCfgInfl.DefParams;
begin
  VAR_GOTOTABELA := 'cfginfl';
  VAR_GOTOMYSQLTABLE := QrCfgInfl;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cfginfl');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCfgInfl.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCfgInfl.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCfgInfl.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmCfgInfl.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCfgInfl.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCfgInfl.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCfgInfl.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCfgInfl.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCfgInfl.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCfgInfl, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cfginfl');
end;

procedure TFmCfgInfl.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCfgInflCodigo.Value;
  Close;
end;

procedure TFmCfgInfl.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Indic1, MesAnt1, Indic2, MesAnt2: Integer;
  Percent1, Percent2: Double;
begin
  Codigo         := QrCfgInflCodigo.Value;
  Nome           := EdNome.ValueVariant;
  Indic1         := RGIndic1.ItemIndex;
  Percent1       := EdPercent1.ValueVariant;
  MesAnt1        := RGMesAnt1.ItemIndex;;
  Indic2         := RGIndic2.ItemIndex;;
  Percent2       := EdPercent2.ValueVariant;
  MesAnt2        := RGMesAnt2.ItemIndex;;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('cfginfl', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cfginfl', False, [
  'Nome', 'Indic1', 'Percent1',
  'MesAnt1', 'Indic2', 'Percent2',
  'MesAnt2'], [
  'Codigo'], [
  Nome, Indic1, Percent1,
  MesAnt1, Indic2, Percent2,
  MesAnt2], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCfgInfl.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cfginfl', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCfgInfl.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCfgInfl, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'cfginfl');
end;

procedure TFmCfgInfl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGIndic1, sLista_INDICE_INFLACIO, 3, 0);
  MyObjects.ConfiguraRadioGroup(RGIndic2, sLista_INDICE_INFLACIO, 3, 0);
  //
  MyObjects.ConfiguraRadioGroup(TRadioGroup(DBRGIndic1), sLista_INDICE_INFLACIO, 3, 0);
  MyObjects.ConfiguraRadioGroup(TRadioGroup(DBRGIndic2), sLista_INDICE_INFLACIO, 3, 0);
  //
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  //
  CriaOForm;
end;

procedure TFmCfgInfl.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCfgInflCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCfgInfl.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCfgInfl.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCfgInflCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCfgInfl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCfgInfl.QrCfgInflAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCfgInfl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCfgInfl.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCfgInflCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cfginfl', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCfgInfl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCfgInfl.QrCfgInflBeforeOpen(DataSet: TDataSet);
begin
  QrCfgInflCodigo.DisplayFormat := FFormatFloat;
end;

end.

