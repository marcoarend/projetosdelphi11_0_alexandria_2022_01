object FmCfgInfl: TFmCfgInfl
  Left = 368
  Top = 194
  Caption = 'CFG-INFLA-001 :: Configura'#231#227'o de Composi'#231#227'o de '#205'ndices'
  ClientHeight = 367
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 271
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 56
        Width = 749
        Height = 61
        Caption = 'Indice 1:'
        TabOrder = 2
        object RGIndic1: TdmkRadioGroup
          Left = 2
          Top = 15
          Width = 371
          Height = 44
          Align = alLeft
          Caption = ' '#205'ndice na composi'#231#227'o:'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '0'
            'MyObjects.ConfiguraRadioGroup(RGIndic1, sLista_INDICE_INFLACIO'
            '2')
          TabOrder = 0
          QryCampo = 'Indic1'
          UpdCampo = 'Indic1'
          UpdType = utYes
          OldValor = 0
        end
        object RGMesAnt1: TdmkRadioGroup
          Left = 373
          Top = 15
          Width = 256
          Height = 44
          Align = alLeft
          Caption = ' Buscar "x" meses antes: do mes de vencimento:'
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            '0'
            '1'
            '2')
          TabOrder = 1
          QryCampo = 'MesAnt1'
          UpdCampo = 'MesAnt1'
          UpdType = utYes
          OldValor = 0
        end
        object Panel6: TPanel
          Left = 629
          Top = 15
          Width = 118
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 86
            Height = 13
            Caption = '% na composi'#231#227'o:'
          end
          object EdPercent1: TdmkEdit
            Left = 8
            Top = 20
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'Percent1'
            UpdCampo = 'Percent1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 116
        Width = 749
        Height = 61
        Caption = 'Indice 2:'
        TabOrder = 3
        object RGIndic2: TdmkRadioGroup
          Left = 2
          Top = 15
          Width = 371
          Height = 44
          Align = alLeft
          Caption = ' '#205'ndice na composi'#231#227'o:'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            '0'
            'MyObjects.ConfiguraRadioGroup(RGIndic1, sLista_INDICE_INFLACIO'
            '2')
          TabOrder = 0
          QryCampo = 'Indic2'
          UpdCampo = 'Indic2'
          UpdType = utYes
          OldValor = 0
        end
        object RGMesAnt2: TdmkRadioGroup
          Left = 373
          Top = 15
          Width = 256
          Height = 44
          Align = alLeft
          Caption = ' Buscar "x" meses antes: do mes de vencimento:'
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            '0'
            '1'
            '2')
          TabOrder = 1
          QryCampo = 'MesAnt2'
          UpdCampo = 'MesAnt2'
          UpdType = utYes
          OldValor = 0
        end
        object Panel7: TPanel
          Left = 629
          Top = 15
          Width = 118
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Label4: TLabel
            Left = 8
            Top = 4
            Width = 86
            Height = 13
            Caption = '% na composi'#231#227'o:'
          end
          object EdPercent2: TdmkEdit
            Left = 8
            Top = 20
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'Percent2'
            UpdCampo = 'Percent2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 208
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 271
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCfgInfl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCfgInfl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 56
        Width = 749
        Height = 61
        Caption = 'Indice 1:'
        TabOrder = 2
        object DBRGIndic1: TDBRadioGroup
          Left = 2
          Top = 15
          Width = 371
          Height = 44
          Align = alLeft
          Caption = ' '#205'ndice na composi'#231#227'o:'
          Columns = 3
          DataField = 'Indic1'
          DataSource = DsCfgInfl
          Items.Strings = (
            '0'
            'MyObjects.ConfiguraRadioGroup(RGIndic1, sLista_INDICE_INFLACIO'
            '2')
          ParentBackground = True
          TabOrder = 0
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object dmkRadioGroup2: TDBRadioGroup
          Left = 373
          Top = 15
          Width = 256
          Height = 44
          Align = alLeft
          Caption = ' Buscar "x" meses antes: do mes de vencimento:'
          Columns = 3
          DataField = 'MesAnt1'
          DataSource = DsCfgInfl
          Items.Strings = (
            '0'
            '1'
            '2')
          ParentBackground = True
          TabOrder = 1
        end
        object Panel8: TPanel
          Left = 629
          Top = 15
          Width = 118
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object Label5: TLabel
            Left = 8
            Top = 4
            Width = 86
            Height = 13
            Caption = '% na composi'#231#227'o:'
          end
          object dmkEdit1: TDBEdit
            Left = 8
            Top = 20
            Width = 105
            Height = 21
            DataField = 'Percent1'
            DataSource = DsCfgInfl
            TabOrder = 0
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 16
        Top = 116
        Width = 749
        Height = 61
        Caption = 'Indice 2:'
        TabOrder = 3
        object DBRGIndic2: TDBRadioGroup
          Left = 2
          Top = 15
          Width = 371
          Height = 44
          Align = alLeft
          Caption = ' '#205'ndice na composi'#231#227'o:'
          Columns = 3
          DataField = 'Indic2'
          DataSource = DsCfgInfl
          Items.Strings = (
            '0'
            'MyObjects.ConfiguraRadioGroup(RGIndic1, sLista_INDICE_INFLACIO'
            '2')
          ParentBackground = True
          TabOrder = 0
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object dmkRadioGroup4: TDBRadioGroup
          Left = 373
          Top = 15
          Width = 256
          Height = 44
          Align = alLeft
          Caption = ' Buscar "x" meses antes: do mes de vencimento:'
          Columns = 3
          DataField = 'MesAnt2'
          DataSource = DsCfgInfl
          Items.Strings = (
            '0'
            '1'
            '2')
          ParentBackground = True
          TabOrder = 1
        end
        object Panel9: TPanel
          Left = 629
          Top = 15
          Width = 118
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object Label6: TLabel
            Left = 8
            Top = 4
            Width = 86
            Height = 13
            Caption = '% na composi'#231#227'o:'
          end
          object dmkEdit2: TDBEdit
            Left = 8
            Top = 20
            Width = 105
            Height = 21
            DataField = 'Percent2'
            DataSource = DsCfgInfl
            TabOrder = 0
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 207
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 491
        Height = 32
        Caption = 'Configura'#231#227'o de Composi'#231#227'o de '#205'ndices'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 491
        Height = 32
        Caption = 'Configura'#231#227'o de Composi'#231#227'o de '#205'ndices'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 491
        Height = 32
        Caption = 'Configura'#231#227'o de Composi'#231#227'o de '#205'ndices'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCfgInfl: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCfgInflBeforeOpen
    AfterOpen = QrCfgInflAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM cfginfl')
    Left = 64
    Top = 64
    object QrCfgInflCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCfgInflNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrCfgInflIndic1: TSmallintField
      FieldName = 'Indic1'
    end
    object QrCfgInflPercent1: TFloatField
      FieldName = 'Percent1'
      DisplayFormat = '0.000000'
    end
    object QrCfgInflMesAnt1: TSmallintField
      FieldName = 'MesAnt1'
    end
    object QrCfgInflIndic2: TSmallintField
      FieldName = 'Indic2'
    end
    object QrCfgInflPercent2: TFloatField
      FieldName = 'Percent2'
      DisplayFormat = '0.000000'
    end
    object QrCfgInflMesAnt2: TSmallintField
      FieldName = 'MesAnt2'
    end
    object QrCfgInflLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCfgInflDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCfgInflDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCfgInflUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCfgInflUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCfgInflAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCfgInflAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCfgInfl: TDataSource
    DataSet = QrCfgInfl
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
