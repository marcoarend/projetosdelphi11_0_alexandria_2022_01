unit CNAE21;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, ComObj, ComCtrls,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, dmkImage,
  UnDmkEnums;

type
  TFmCNAE21 = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdCaminho: TdmkEdit;
    Panel4: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    Splitter1: TSplitter;
    Panel5: TPanel;
    Panel6: TPanel;
    EdPesq: TdmkEdit;
    dmkDBGrid2: TdmkDBGrid;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel9: TPanel;
    BtBaixar: TBitBtn;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    QrCNAE21CadCodigo: TIntegerField;
    QrCNAE21CadNivel: TSmallintField;
    QrCNAE21CadCodTxt: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtBaixarClick(Sender: TObject);
    procedure QrCNAE21CadAfterScroll(DataSet: TDataSet);
    procedure QrCNAE21CadBeforeClose(DataSet: TDataSet);
    procedure EdPesqExit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCNAE21Cad();
    procedure CarregaXLS();
  public
    { Public declarations }
  end;

  var
  FmCNAE21: TFmCNAE21;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, (*MyVCLSkin,*) UnInternalConsts,
  UnDmkWeb, DmkDAC_PF, Module;

const
  FURL = 'http://www.dermatek.com.br/Tabelas_Publicas/CNAE_CONCLA_21.xls';
  FDestFile = 'C:\Dermatek\Tabelas\CNAE_CONCLA\CNAE_Baixado.xls';
  FTits = 2;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'ID',
{01}                            'Nome');

{$R *.DFM}

procedure TFmCNAE21.BitBtn1Click(Sender: TObject);
begin
  MyObjects.SalvaStringGridToFile(Grade1, 'C:\Dermatek\StringGrids\CNAE21.txt');
end;

procedure TFmCNAE21.BtBaixarClick(Sender: TObject);
var
  Fonte: String;
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) = 0 then
  begin
    Geral.MB_Info('Voc� deve digitar um URL para baixar o arquivo!');
    Exit;
  end;
  //
  Fonte := EdCaminho.Text;
  if FileExists(FDestFile) then DeleteFile(FDestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Baixando arquivo!');
  Application.ProcessMessages;
  //
  if not DirectoryExists(ExtractFileDir(FDestFile)) then
    ForceDirectories(ExtractFileDir(FDestFile));
  //
  if dmkWeb.DownloadFile(Fonte, FDestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, FDestFile, PB1, LaAviso1, LaAviso2) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Erro durante o download de "' +Fonte + '"');
end;

procedure TFmCNAE21.BtAbreClick(Sender: TObject);
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
end;

procedure TFmCNAE21.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmCNAE21.CarregaXLS();
var
  Codigo, Nivel, Sim, Nao: Integer;
  Nivel1, Nivel2, Nivel3, Nivel4, Nivel5, CodTxt, CodAlf, Nome: String;
  Continua, Erro: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    Sim := 0;
    Nao := 0;
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    DmodG.QrAllUpd.SQL.Clear;
    DmodG.QrAllUpd.SQL.Add(DELETE_FROM + ' CNAE21Cad');
    DmodG.QrAllUpd.ExecSQL;
    //
    for Codigo := 1 to Grade1.RowCount -1 do
    begin
      Continua := False;
      Erro := False;
      Nivel := 0;
      PB1.Position := PB1.Position + 1;

      Nivel5 := Trim(Grade1.Cells[02,Codigo]);
      Nivel4 := Trim(Grade1.Cells[03,Codigo]);
      Nivel3 := Trim(Grade1.Cells[04,Codigo]);
      Nivel2 := Trim(Grade1.Cells[05,Codigo]);
      Nivel1 := Trim(Grade1.Cells[06,Codigo]);
      //
      if Nivel5 <> '' then
      begin
        if Length(Nivel5) = 1 then
        begin
          Continua := True;
          Nivel := 5;
        end else
          Erro := True;
      end;
      if Nivel4 <> '' then
      begin
        if Continua then
          Erro := True
        else
        begin
          Continua := True;
          Nivel := 4;
        end;
      end;
      if Nivel3 <> '' then
      begin
        if Continua then
          Erro := True
        else
        begin
          Continua := True;
          Nivel := 3;
        end;
      end;
      if Nivel2 <> '' then
      begin
        if Continua then
          Erro := True
        else
        begin
          Continua := True;
          Nivel := 2;
        end;
      end;
      if Nivel1 <> '' then
      begin
        if Continua then
          Erro := True
        else
        begin
          Continua := True;
          Nivel := 1;
        end;
      end;
      if Continua and not Erro then
      begin
        case Nivel of
          1: CodTxt := Nivel1;
          2: CodTxt := Nivel2;
          3: CodTxt := Nivel3;
          4: CodTxt := Nivel4;
          5: CodTxt := Nivel5;
          else CodTxt := '#ERRO#';
        end;
        Nome := Trim(Grade1.Cells[07,Codigo]);
        CodAlf := Geral.SoNumeroELetra_TT(CodTxt);
        if UMyMod.SQLInsUpd(DmodG.QrAllUpd, stIns, 'cnae21cad', False, [
        'Nivel', 'CodTxt', 'Nome',
        'Nivel1', 'Nivel2', 'Nivel3',
        'Nivel4', 'Nivel5', 'CodAlf'], [
        'Codigo'], [
        Nivel, CodTxt, Nome,
        Nivel1, Nivel2, Nivel3,
        Nivel4, Nivel5, CodAlf], [
        Codigo], True) then
          Sim := Sim + 1;

      end else
        Nao := Nao + 1;
    end;
    PB1.Position := PB1.Max;
    Geral.MB_Aviso('Sim = ' + Geral.FF0(Sim) + sLineBreak +
                      'Nao = ' + Geral.FF0(Nao) + sLineBreak +
                      'All = ' + Geral.FF0(Sim + Nao));
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Screen.Cursor   := crDefault;
  end;
end;

procedure TFmCNAE21.EdPesqExit(Sender: TObject);
begin
  ReopenCNAE21Cad;
end;

procedure TFmCNAE21.BtSaidaClick(Sender: TObject);
begin
  if QrCNAE21Cad.State <> dsInactive then
    VAR_CADASTROX := QrCNAE21CadCodTxt.Value;
  //
  Close;
end;

procedure TFmCNAE21.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdCaminho.CharCase := ecNormal;
  EdCaminho.ValueVariant := FURL;
end;

procedure TFmCNAE21.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  VAR_CADASTROX := '';
  EdPesq.ValueVariant := '';
  Grade1.ColCount := FTits;
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  PageControl1.ActivePageIndex := 1;
end;

procedure TFmCNAE21.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAE21.QrCNAE21CadAfterScroll(DataSet: TDataSet);
begin
  ReopenCNAE21Cad();
end;

procedure TFmCNAE21.QrCNAE21CadBeforeClose(DataSet: TDataSet);
begin
  QrCNAE21Cad.Close;
end;

procedure TFmCNAE21.ReopenCNAE21Cad();
begin
  QrCNAE21Cad.Close;
  QrCNAE21Cad.Params[1].AsString := '%' + EdPesq.ValueVariant + '%';
  UnDmkDAC_PF.AbreQuery(QrCNAE21Cad, Dmod.MyDB);
end;

procedure TFmCNAE21.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(FDestFile);
  Arquivo := ExtractFileName(FDestFile);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdCaminho.Text := Arquivo;
end;

end.
