unit ListServ;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnDmkWeb,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkMemo, ComCtrls, Menus, UnDMkEnums;

type
  TFmListServ = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrListServ: TmySQLQuery;
    DsListServ: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrListServCodTxt: TWideStringField;
    QrListServCodigo: TIntegerField;
    QrListServNome: TWideStringField;
    QrListServDescricao: TWideMemoField;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    DBMemo1: TDBMemo;
    EdCodAlf: TdmkEdit;
    Label4: TLabel;
    MeDescricao: TdmkMemo;
    BtImporta: TBitBtn;
    Panel6: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdArq: TdmkEdit;
    MeLista: TMemo;
    PB1: TProgressBar;
    QrPesq: TmySQLQuery;
    PMExclui: TPopupMenu;
    ExcluioitemAtual1: TMenuItem;
    ExcluiTodaLista1: TMenuItem;
    EdCodTxt: TdmkEdit;
    Label5: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrListServAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrListServBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ExcluioitemAtual1Click(Sender: TObject);
    procedure ExcluiTodaLista1Click(Sender: TObject);
    procedure EdCodAlfChange(Sender: TObject);
    procedure EdCodAlfExit(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ImportaDeArquivo(Arquivo: String);
    procedure Exclui(Quais: TSelType);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmListServ: TFmListServ;
const
  FFormatFloat = '00000';
  FURL = 'http://www.dermatek.com.br/Tabelas_Publicas/LC11603_ListaServ.txt';
  FDestFile = 'C:\Dermatek\Tabelas\LISTA_SERVICOS\LC11603_ListaServ.txt';

implementation

uses UnMyObjects, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmListServ.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmListServ.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrListServCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmListServ.DefParams;
begin
  VAR_GOTOTABELA := 'listserv';
  VAR_GOTOMYSQLTABLE := QrListServ;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := DModG.AllID_DB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM listserv');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmListServ.EdCodAlfChange(Sender: TObject);
begin
  if EdCodAlf.Focused then
    EdCodTxt.Text := Geral.FormataLC11603(EdCodAlf.Text);
end;

procedure TFmListServ.EdCodAlfExit(Sender: TObject);
begin
  EdCodAlf.Text := Geral.SoNumeroELetra_TT(EdCodAlf.Text);
end;

procedure TFmListServ.Exclui(Quais: TSelType);
var
  SQLQuais: String;
begin
  if Quais <> istTodos then
    SQLQuais := 'WHERE Codigo=' + Geral.FF0(QrListServCodigo.Value)
  else
    SQLQuais := '';
  //
  UMyMod.ExecutaMySQLQuery1(DModG.QrAllUpd, [
  'DELETE FROM listserv',
  SQLQuais,
  '']);
  //
  if Quais <> istTodos then
    Va(vpNext)
  else
    QrListServ.Close;
end;

procedure TFmListServ.ExcluioitemAtual1Click(Sender: TObject);
begin
  Exclui(istAtual);
end;

procedure TFmListServ.ExcluiTodaLista1Click(Sender: TObject);
begin
  Exclui(istTodos);
end;

procedure TFmListServ.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmListServ.QueryPrincipalAfterOpen;
begin
end;

procedure TFmListServ.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmListServ.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmListServ.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmListServ.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmListServ.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmListServ.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(FDestFile);
  Arquivo := ExtractFileName(FDestFile);
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArq.Text := Arquivo;
end;

procedure TFmListServ.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmListServ.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrListServ, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'listserv');
end;

procedure TFmListServ.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrListServCodigo.Value;
  VAR_CADASTROX := QrListServCodTxt.Value;
  //
  Close;
end;

procedure TFmListServ.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('listserv', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrListServCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'listserv', Codigo, DModG.QrAllUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmListServ.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, DModG.AllID_DB, 'listserv', 'Codigo');
end;

procedure TFmListServ.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmListServ.BtImportaClick(Sender: TObject);
var
  Fonte: String;
begin
  if Lowercase(Copy(EdArq.Text, 1, 4)) = 'http' then
  begin
    Fonte := EdArq.Text;
    if FileExists(FDestFile) then DeleteFile(FDestFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde... Baixando arquivo!');
    Application.ProcessMessages;
    //
    if not DirectoryExists(ExtractFileDir(FDestFile)) then
      ForceDirectories(ExtractFileDir(FDestFile));
    //
    if dmkWeb.DownloadFile(Fonte, FDestFile) then
      ImportaDeArquivo(FDestFile);
  end else
    ImportaDeArquivo(EdArq.Text);
end;

procedure TFmListServ.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrListServ, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'listserv');
end;

procedure TFmListServ.FormCreate(Sender: TObject);
begin
  EdArq.ValueVariant := FURL;
  //
  VAR_CADASTROX := '';
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmListServ.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrListServCodigo.Value, LaRegistro.Caption);
end;

procedure TFmListServ.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmListServ.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrListServCodigo.Value, LaRegistro.Caption);
end;

procedure TFmListServ.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmListServ.QrListServAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmListServ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdArq.CharCase := ecNormal;
  EdArq.Text     := FURL;
end;

procedure TFmListServ.SbQueryClick(Sender: TObject);
begin
  LocCod(QrListServCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'listserv', DModG.AllID_DB, CO_VAZIO));
end;

procedure TFmListServ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmListServ.ImportaDeArquivo(Arquivo: String);
var
  Linha, CodTxt, Nome, Descricao, Codigo, CodAlf: String;
  I, N, P: Integer;
begin
  //P := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando arquivo...');
    Update;
    Application.ProcessMessages;
    //
    Geral.LeArquivoToMemo(Arquivo, MeLista);
    PB1.Max := MeLista.Lines.Count;
    for I := 0 to MeLista.Lines.Count -1 do
    begin
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      Linha  := Trim(MeLista.Lines[I]);
      if Linha <> '' then
      begin
        CodTxt := '';
        N := Length(Linha);
        P := pos(#150, Linha);
        if P = 0 then
          P := pos(#45, Linha);
        if P > 0 then
          CodTxt := Trim(Copy(Linha, 1, P-1))
        else
          Geral.MB_Aviso('Linha n�o reconhecida:' + sLineBreak +
          Linha);
        if CodTxt <> '' then
        begin
          Codigo := Geral.SoNumero_TT(CodTxt);
          //CodTxt := Geral.SoNumero_TT(CodTxt);
          if (CodTxt <> '') and (P > 0) and (P < N) then
          begin
            if (Length(CodTxt) = 4) and (pos('.', CodTxt) = 2) then
            begin
              CodTxt := '0' + CodTxt;
            end;
            CodAlf := Geral.SoNumeroELetra_TT(CodTxt);
            Nome := Trim(Copy(Linha, P + 1));
            Nome := Copy(Nome, 1, 255); // Tamanho maximo 2015-06-25
            //Show Message('CodTxt = ' + CodTxt + sLineBreak + 'Texto = ' + Nome);
            UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, DModG.AllID_DB, [
            'SELECT * ',
            'FROM listserv ',
            'WHERE Codigo="' + Codigo + '" ',
            '']);
            if QrPesq.RecordCount = 0 then
            begin
              Descricao := Nome;
              UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'listserv', False, [
              'Codigo', 'Nome', 'Descricao', 'CodAlf'], ['CodTxt'], [
              Codigo, Nome, Descricao, CodAlf], [CodTxt], False);
            end;
          end;
        end;
      end;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    PB1.Position := 0;
    Va(vpLast);
    Geral.MB_Info('Importa��o realizada com sucesso!')
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmListServ.QrListServBeforeOpen(DataSet: TDataSet);
begin
  QrListServCodigo.DisplayFormat := FFormatFloat;
end;

end.

