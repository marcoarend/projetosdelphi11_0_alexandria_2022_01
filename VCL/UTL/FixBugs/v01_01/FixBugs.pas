unit FixBugs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkCheckGroup, mySQLDbTables,
  DateUtils, DmkDAC_PF, UnDmkProcFunc, DockForm;

type
  TFmFixBugs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PCAtualiz: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CGApp: TdmkCheckGroup;
    CGGeral: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function  VerificaAcaoNaoExecutada(FixBug: Integer; FBGeral: Boolean): Boolean;
    function  SalvaExecucaoDeAcao(FixBug: Integer; FixBugStr: String; FBGeral: Boolean): Boolean;
    function  EncerraExecucaoDeAcao(FixBug: Integer; FBGeral, Executou: Boolean): Boolean;
    procedure VerificaAtualizacoesGerais(FixBug: Integer; FixBugStr: String; Executa: Boolean);
    procedure VerificaAtualizacoesApp(FixBug: Integer; FixBugStr: String; Executa: Boolean);
    procedure CriaFormProgresso(Descricao: String = '');
    ///////////////////////////// FUN��ES DE ATUALIZA��ES GERAIS - IN�CIO //////
    procedure MigraAtualizacoesGerais();
    function  Corrige9Digito(): Boolean;
    {$IfNDef SemNFe_0000}
    procedure CorrigeLctosFaturaNFes(FixBugNom: String);
    procedure AtualizaTodasNotasCanceladas(FixBugNom: String);
    procedure CorrigeListaCFOP(FixBugNom: String);
    {$EndIf}
    {$IfNDef sBLQ}
    procedure AtualizaModeloDeBoletoPrev(FixBugNom: String);
    procedure Migra_BloToProtocolo();
    procedure AtualizaProtocolosBoletos(FixBugNom: String);
    {$EndIf}
    {$IFNDEF NO_USE_EMAILDMK}
    procedure AtualizaPreEmail();
    {$EndIf}
    {$IfDef ComSPED_0000}
    procedure AtualizaLogTabelasSped();
    {$EndIf}
    (* Desativado em: 30/08/2016 => Motivc: est� zerando as cidades
    procedure AtualizaEntidadesParaEntidade2(ProgreesBar: TProgressBar; DataBase, AllDataBase: TmySQLDatabase);
    *)
    ///////////////////////////// FUN��ES DE ATUALIZA��ES GERAIS - FIM    //////
  public
    { Public declarations }
    FAtzTodos: Boolean;
    procedure ConfiguraAtualizacoesGerais(Lista_Str: array of string;
              MarcarTodos: Boolean);
    procedure ConfiguraAtualizacoesApp(Lista_Str: array of string;
              MarcarTodos: Boolean);
    procedure ExecutaAtualizacoes(Executa: Boolean = False);
  end;

  var
    FmFixBugs: TFmFixBugs;
    DockWindow: TFmDockForm;
  const
    CO_MAX_TEMPO_ATZ_HORAS = 2;


implementation

uses UnMyObjects, Module, MyDBCheck, UMySQLModule, ModuleGeral, Enti9Digit,
  {$IfNDef SemNFe_0000}ModuleNFe_0000,{$EndIf}
  {$IFNDEF NO_USE_EMAILDMK}MailCfg, {$EndIf}
  Principal, UnFixBugs;

{$R *.DFM}

procedure TFmFixBugs.VerificaAtualizacoesGerais(FixBug: Integer;
  FixBugStr: String; Executa: Boolean);
var
  Executou: Boolean;
  FixBugNom: String;
begin
  if not Geral.IntInConjunto(UFixBugs.ListaFixBugs_Cod(FixBug), CGGeral.Value) then
    Exit;
  //
  FixBugNom := FixBugStr;
  Executou  := True;
  //
  if (Executa = True) or (VerificaAcaoNaoExecutada(FixBug, True)) then
  begin
    if not SalvaExecucaoDeAcao(FixBug, FixBugStr, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    //
    if FixBug = 0 then
    begin
      MigraAtualizacoesGerais();
    end else
    if FixBug = 1 then
    begin
      // J� fez em todos?
      // Executou := Corrige9Digito();
    end else
    if FixBug = 2 then
    begin
      {$IfNDef SemNFe_0000}
      CorrigeLctosFaturaNFes(FixBugNom);
      {$EndIf}
    end else
    if FixBug = 3 then
    begin
      {$IfNDef SemNFe_0000}
      AtualizaTodasNotasCanceladas(FixBugNom);
      {$EndIf}
    end else
    if FixBug = 4 then
    begin
      {$IfNDef sBLQ}
      AtualizaModeloDeBoletoPrev(FixBugNom);
      {$EndIf}
    end else
    if FixBug = 5 then
    begin
      {$IfNDef sBLQ}
      Migra_BloToProtocolo();
      {$EndIf}
    end else
    if FixBug = 6 then
    begin
      {$IfNDef sBLQ}
      AtualizaProtocolosBoletos(FixBugNom);
      {$EndIf}
    end else
    if FixBug = 7 then
    begin
      {$IFNDEF NO_USE_EMAILDMK}
      AtualizaPreEmail();
      {$EndIf}
    end else
    if FixBug = 8 then
    begin
      {$IfDef ComSPED_0000}
      AtualizaLogTabelasSped();
      {$EndIf}
    end else
    if FixBug = 9 then
    begin
      {$IfNDef SemNFe_0000}
      CorrigeListaCFOP(FixBugNom);
      {$EndIf}
    end else
    begin
      Geral.MB_Erro('Atualiza��o n�o implementada na fun��o ' +
        '"TFmFixBugs.VerificaAtualizacoesGerais"!' + sLineBreak + sLineBreak +
        'Dados da fun��o:' + sLineBreak + sLineBreak +
        'ID: ' + Geral.FF0(FixBug) + sLineBreak +
        'Nome: ' + FixBugStr);
      Executou := False;
    end;
    if not EncerraExecucaoDeAcao(FixBug, True, Executou) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
  end;
end;

procedure TFmFixBugs.VerificaAtualizacoesApp(FixBug: Integer; FixBugStr: String;
  Executa: Boolean);
var
  Executou: Boolean;
  FixBugNom: String;
begin
  if not Geral.IntInConjunto(UFixBugs.ListaFixBugs_Cod(FixBug), CGApp.Value) then
    Exit;
  //
  FixBugNom := FixBugStr;
  Executou  := True;
  //
  if (Executa = True) or (VerificaAcaoNaoExecutada(FixBug, False)) then
  begin
    if not SalvaExecucaoDeAcao(FixBug, FixBugStr, False) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    //
    Executou := FmPrincipal.FixBugAtualizacoesApp(FixBug, FixBugStr);
    //
    if not Executou then
    begin
      Geral.MB_Erro('Atualiza��o n�o implementada na fun��o ' +
        '"TFmFixBugs.VerificaAtualizacoesApp"!' + sLineBreak + sLineBreak +
        'Dados da fun��o:' + sLineBreak + sLineBreak +
        'ID: ' + Geral.FF0(FixBug) + sLineBreak +
        'Nome: ' + FixBugStr);
    end;
    if not EncerraExecucaoDeAcao(FixBug, False, Executou) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
  end;
end;

procedure TFmFixBugs.ConfiguraAtualizacoesGerais(Lista_Str: array of string;
  MarcarTodos: Boolean);
var
  I, PCIndex: Integer;
begin
  CGGeral.Items.Clear;
  //
  if Length(Lista_Str) > 0 then
  begin
    PCIndex := 0;
    //
    for I := Low(Lista_Str) to High(Lista_Str) do
    begin
      CGGeral.Items.Add(Lista_Str[I]);
    end;
    if MarcarTodos then
      dmkPF.CheckGroupTodos(CGGeral, True)
    else
      CGGeral.Value := DModG.QrCtrlGeralFixBugsGrl.Value;
  end else
    PCIndex := 1;
  //
  PCAtualiz.ActivePageIndex := PCIndex;
end;

procedure TFmFixBugs.ConfiguraAtualizacoesApp(Lista_Str: array of string;
  MarcarTodos: Boolean);
var
  I: Integer;
begin
  CGApp.Items.Clear;
  //
  if Length(Lista_Str) > 0 then
  begin
    for I := Low(Lista_Str) to High(Lista_Str) do
    begin
      CGApp.Items.Add(Lista_Str[I]);
    end;
    if MarcarTodos then
      dmkPF.CheckGroupTodos(CGApp, True)
    else
      CGGeral.Value := DModG.QrCtrlGeralFixBugsApp.Value;
  end;
end;

procedure TFmFixBugs.BtOKClick(Sender: TObject);
begin
  ExecutaAtualizacoes(True);
end;

procedure TFmFixBugs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFixBugs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFixBugs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenOpcoesGerl;
end;

procedure TFmFixBugs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmFixBugs.VerificaAcaoNaoExecutada(FixBug: Integer; FBGeral: Boolean): Boolean;
var
  Qry: TmySQLQuery;
  FixBugCod: Integer;
  FixBugs: Largeint;
  FixBugNom, NomePC: String;
  FixBugDta, DtaLimite, DtaAgora: TDateTime;
begin
  Result    := False;
  FixBugCod := UFixBugs.ListaFixBugs_Cod(FixBug);
  //
  if FBGeral then
    FixBugs := DModG.QrCtrlGeralFixBugsGrl.Value
  else
    FixBugs := DModG.QrCtrlGeralFixBugsApp.Value;
  //
  if not Geral.IntInConjunto(FixBugCod, FixBugs) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM fixbugs ',
        'WHERE Geral=' + Geral.FF0(Geral.BoolToInt(FBGeral)),
        'AND FixBugCod & ' + Geral.FF0(FixBugCod),
        '']);
      if Qry.RecordCount > 0 then
      begin
        NomePC    := Qry.FieldByName('NomePC').AsString;
        FixBugNom := Qry.FieldByName('FixBugNom').AsString;
        FixBugDta := Qry.FieldByName('FixBugDta').AsDateTime;
        DtaLimite := IncHour(FixBugDta, CO_MAX_TEMPO_ATZ_HORAS);
        DtaAgora  := DmodG.ObtemAgora();
        //
        if DtaLimite < DtaAgora then
        begin
          if Geral.MB_Pergunta('A execu��o da atualiza��o "' + FixBugNom +
            '" j� foi iniciada em outro computador!' + sLineBreak +
            'Voc� realmente deseja iniciar a mesma atualiza��o neste computador?' +
            sLineBreak + sLineBreak + 'Computador: ' + NomePC + sLineBreak +
            'Iniciado em: ' + Geral.FDT(FixBugDta, 0)) = ID_YES then
          begin
            if DBCheck.LiberaPelaSenhaBoss then
              Result := True;
          end;
        end;
      end else
        Result := True;
    finally
      Qry.Free;
    end;
  end;
end;

function TFmFixBugs.SalvaExecucaoDeAcao(FixBug: Integer; FixBugStr: String;
  FBGeral: Boolean): Boolean;
var
  QryUpd: TmySQLQuery;
  FixBugCod, Terminal: Integer;
  FixBugNom, NomePC, DataHora: String;
  ExecSpedLog: Boolean;
begin
  {$IfDef ComSPED_0000}
  ExecSpedLog := False;
  {$Else}
  ExecSpedLog := True;
  {$EndIf}
  if (FixBug = 8) and (ExecSpedLog = False) then
  begin
    Result := True;
    Exit;
  end;
  Result    := False;
  FixBugCod := UFixBugs.ListaFixBugs_Cod(FixBug);
  FixBugNom := FixBugStr;
  DataHora  := Geral.FDT(DmodG.ObtemAgora(), 9);
  NomePC    := Geral.IndyComputerName();
  Terminal  := VAR_TERMINAL;
  //
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    QryUpd.DataBase := Dmod.MyDB;
    if UMyMod.SQLInsUpd(QryUpd, stIns, 'fixbugs', False,
      ['FixBugCod', 'FixBugNom', 'FixBugDta', 'Geral', 'NomePC', 'Terminal'], [],
      [FixBugCod, FixBugNom, DataHora, Geral.BoolToInt(FBGeral), NomePC, Terminal], [], False)
    then
      Result := True;
  finally
    QryUpd.Destroy;
  end;
end;

function TFmFixBugs.EncerraExecucaoDeAcao(FixBug: Integer; FBGeral,
  Executou: Boolean): Boolean;
var
  Qry: TmySQLQuery;
  Valor: Largeint;
  FixBugCod: Integer;
  SQL, Campo: String;
  ExecSpedLog: Boolean;
begin
  {$IfDef ComSPED_0000}
  ExecSpedLog := False;
  {$Else}
  ExecSpedLog := True;
  {$EndIf}
  if (FixBug = 8) and (ExecSpedLog = False) then
  begin
    Result := True;
    Exit;
  end;
  FixBugCod := UFixBugs.ListaFixBugs_Cod(FixBug);
  Qry       := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    if Executou then
    begin
      DModG.ReopenCtrlGeral();
      //
      if FBGeral then
      begin
        Campo := 'FixBugsGrl';
        Valor := DModG.QrCtrlGeralFixBugsGrl.Value;
      end else
      begin
        Campo := 'FixBugsApp';
        Valor := DModG.QrCtrlGeralFixBugsApp.Value;
      end;
      if not Geral.IntInConjunto(FixBugCod, Valor) then
        Valor := Valor + FixBugCod;
      //
      SQL := '; UPDATE ctrlgeral SET ' + Campo + '=' + Geral.FI64(Valor);
    end else
      SQL := '';
    //
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
                'DELETE FROM fixbugs ',
                'WHERE FixBugCod=' + Geral.FF0(FixBugCod),
                'AND Geral=' + Geral.FF0(Geral.BoolToInt(FBGeral)),
                SQL,
                '']);
  finally
    Qry.Free;
  end;
end;

procedure TFmFixBugs.ExecutaAtualizacoes(Executa: Boolean = False);
var
  I: Integer;
begin
  if CGGeral.Items.Count > 0 then
  begin
    //Executa atualiza��es gerais
    for I := 0 to CGGeral.Items.Count - 1 do
    begin
      VerificaAtualizacoesGerais(I, CGGeral.Items[I], Executa);
    end;
  end;
  if CGApp.Items.Count > 0 then
  begin
    //Executa atualiza��es espec�ficas
    for I := 0 to CGApp.Items.Count - 1 do
    begin
      VerificaAtualizacoesApp(I, CGApp.Items[I], Executa);
    end;
  end;
end;

procedure TFmFixBugs.CriaFormProgresso(Descricao: String = '');
var
  Txt, Txt2: String;
begin
  Txt := 'XXX-XXXXX-001 :: Executando atualiza��es';
  //
  if Descricao = '' then
    Txt2 := '..............................'
  else
    Txt2 := Descricao;
  //
  DockWindow := TFmDockForm.Create(Application);
  //
  DockWindow.Caption            := Txt;
  DockWindow.LaTitulo1A.Caption := Txt;
  DockWindow.LaTitulo1B.Caption := Txt;
  DockWindow.LaTitulo1C.Caption := Txt;
  //
  DockWindow.LaAviso1.Caption := Descricao;
  DockWindow.LaAviso2.Caption := Descricao;
  //
  DockWindow.Show;
  DockWindow.Refresh;
end;

///////////////////////////////// FUN��ES DE ATUALIZA��ES GERAIS - IN�CIO //////
procedure TFmFixBugs.MigraAtualizacoesGerais();
var
  Atz: Integer;
  QryAux, QryUpd: TmySQLQuery;
begin
  QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    {$IfNDef SemNFe_0000}
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW Fields ',
      'FROM ctrlgeral ',
      'LIKE "AtualizouLctsNFe" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT AtualizouLctsNFe ',
        'FROM ctrlgeral ',
        '']);
      Atz := QryAux.FieldByName('AtualizouLctsNFe').AsInteger;
      //
      if Atz = 1 then
      begin
        if not EncerraExecucaoDeAcao(2, True, True) then
        begin
          Geral.MB_Erro('Falha ao atualizar rotina!');
          Exit;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
        'ALTER TABLE ctrlgeral DROP COLUMN AtualizouLctsNFe',
        '']);
    end;
    {$Else}
    if not EncerraExecucaoDeAcao(2, True, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    {$EndIf}

    {$IfNDef SemNFe_0000}
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW Fields ',
      'FROM ctrlgeral ',
      'LIKE "AtualizouNFeCan" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT AtualizouNFeCan ',
        'FROM ctrlgeral ',
        '']);
      Atz := QryAux.FieldByName('AtualizouNFeCan').AsInteger;
      //
      if Atz = 1 then
      begin
        if not EncerraExecucaoDeAcao(3, True, True) then
        begin
          Geral.MB_Erro('Falha ao atualizar rotina!');
          Exit;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
        'ALTER TABLE ctrlgeral DROP COLUMN AtualizouNFeCan',
        '']);
    end;
    {$Else}
    if not EncerraExecucaoDeAcao(3, True, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    {$EndIf}


    {$IfNDef sBLQ}
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW Fields ',
      'FROM controle ',
      'LIKE "AtualizBloPrev" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT AtualizBloPrev ',
        'FROM controle ',
        '']);
      Atz := QryAux.FieldByName('AtualizBloPrev').AsInteger;
      //
      if Atz = 1 then
      begin
        if not EncerraExecucaoDeAcao(4, True, True) then
        begin
          Geral.MB_Erro('Falha ao atualizar rotina!');
          Exit;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
        'ALTER TABLE controle DROP COLUMN AtualizBloPrev',
        '']);
    end;
    {$Else}
    if not EncerraExecucaoDeAcao(4, True, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    {$EndIf}



    {$IfNDef sBLQ}
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW Fields ',
      'FROM bloopcoes ',
      'LIKE "CorrigiuTabProt" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT CorrigiuTabProt ',
        'FROM bloopcoes ',
        '']);
      Atz := QryAux.FieldByName('CorrigiuTabProt').AsInteger;
      //
      if Atz = 1 then
      begin
        if not EncerraExecucaoDeAcao(5, True, True) then
        begin
          Geral.MB_Erro('Falha ao atualizar rotina!');
          Exit;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
        'ALTER TABLE bloopcoes DROP COLUMN CorrigiuTabProt',
        '']);
    end;
    {$Else}
    if not EncerraExecucaoDeAcao(5, True, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    {$EndIf}


    {$IfNDef sBLQ}
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW Fields ',
      'FROM controle ',
      'LIKE "AtualizBloProto" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT AtualizBloProto ',
        'FROM controle ',
        '']);
      Atz := QryAux.FieldByName('AtualizBloProto').AsInteger;
      //
      if Atz = 1 then
      begin
        if not EncerraExecucaoDeAcao(6, True, True) then
        begin
          Geral.MB_Erro('Falha ao atualizar rotina!');
          Exit;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
        'ALTER TABLE controle DROP COLUMN AtualizBloProto',
        '']);
    end;
    {$Else}
    if not EncerraExecucaoDeAcao(6, True, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    {$EndIf}


    {$IFNDEF NO_USE_EMAILDMK}
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW Fields ',
      'FROM ctrlgeral ',
      'LIKE "AtualizouPreEmail" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT AtualizouPreEmail ',
        'FROM ctrlgeral ',
        '']);
      Atz := QryAux.FieldByName('AtualizouPreEmail').AsInteger;
      //
      if Atz = 1 then
      begin
        if not EncerraExecucaoDeAcao(7, True, True) then
        begin
          Geral.MB_Erro('Falha ao atualizar rotina!');
          Exit;
        end;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
        'ALTER TABLE ctrlgeral DROP COLUMN AtualizouPreEmail',
        '']);
    end;
    {$Else}
    if not EncerraExecucaoDeAcao(7, True, True) then
    begin
      Geral.MB_Erro('Falha ao atualizar rotina!');
      Exit;
    end;
    {$EndIf}
  finally
    QryAux.Free;
    QryUpd.Free;
  end;
end;

function TFmFixBugs.Corrige9Digito(): Boolean;
begin
  Result := False;
  //
  if DBCheck.CriaFm(TFmEnti9Digit, FmEnti9Digit, afmoNegarComAviso) then
  begin
    FmEnti9Digit.ShowModal;
    //
    Result := FmEnti9Digit.FExecutou;
    //
    FmEnti9Digit.Destroy;
  end;
end;

{$IfDef ComSPED_0000}
procedure TFmFixBugs.AtualizaLogTabelasSped;

  procedure AtualizaTabela(TabelaOri, TabelaDes, CamposLog: String);
  var
    QryAux, QryAux2: TmySQLQuery;
    LstFld, Lst: TStringList;
    Campo, Valor: String;
    I: Integer;
  begin
    LstFld  := TStringList.Create;
    Lst     := TStringList.Create;
    QryAux  := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
    QryAux2 := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SHOW TABLES ',
        'LIKE "' + TabelaDes + '" ',
        '']);
      if QryAux.RecordCount > 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QryAux2, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + TabelaDes,
          'WHERE MotvDel = 998 ',
          '']);
        if QryAux2.RecordCount > 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
            'SHOW KEYS ',
            'FROM ' + TabelaOri,
            '']);
          if QryAux.RecordCount > 0 then
          begin
            QryAux.First;
            while not QryAux.Eof do
            begin
              Campo := QryAux.FieldByName('Column_name').AsString;
              //
              if Campo <> '' then
                LstFld.Add(Campo);
              //
              QryAux.Next;
            end;
          end;
          if LstFld.Count > 0 then
          begin
            QryAux2.First;
            while not QryAux2.Eof do
            begin
              for I := 0 to LstFld.Count - 1 do
              begin
                Valor := Geral.VariavelToString(QryAux2.FieldByName(LstFld[I]).AsVariant);
                //
                Lst.Add(LstFld[I] + '=' + Valor);
              end;
              UMyMod.SaveLogTable(Dmod.MyDB, TabelaOri, TabelaDes, Lst, CamposLog);
            end;
          end;
        end;
      end;
    finally
      QryAux.Free;
      QryAux2.Free;
      LstFld.Free;
      Lst.Free;
    end;
  end;

begin
  AtualizaTabela('entidades', 'entidadez', 'Nome, RazaoSocial, PRua, ERua');
end;
{$EndIf}

{$IfNDef SemNFe_0000}
procedure TFmFixBugs.CorrigeLctosFaturaNFes(FixBugNom: String);
var
  QryAux, QryUpd: TmySQLQuery;
  FatID, FatNum, Empresa, PBPos, PBMax: Integer;
begin
  QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM nfecaba ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      PBPos := 0;
      PBMax := QryAux.RecordCount;
      //
      CriaFormProgresso(FixBugNom);
      DockWindow.AtualizaDados(PBPos, PBMax);
      //
      while not QryAux.Eof do
      begin
        FatID   := QryAux.FieldByName('FatID').AsInteger;
        FatNum  := QryAux.FieldByName('FatNum').AsInteger;
        Empresa := QryAux.FieldByName('Empresa').AsInteger;
        //
        DmNFe_0000.AtualizaLctoFaturaNFe(FatID, FatNum, Empresa);
        //
        PBPos := PBPos + 1;
        DockWindow.AtualizaDados(PBPos, PBMax);
        //
        QryAux.Next;
      end;
      DockWindow.Hide;
    end;
  finally
    QryAux.Free;
    QryUpd.Free;
  end;
end;
{$EndIf}

{$IfNDef SemNFe_0000}
procedure TFmFixBugs.CorrigeListaCFOP(FixBugNom: String);
var
  QryAux, QryAux2, QryUpd: TmySQLQuery;
  CFOPAtu, CFOPNew: String;
  PBPos, PBMax: Integer;
begin
  QryAux  := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryAux2 := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd  := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SELECT * ',
      'FROM cfop2003 ',
      'WHERE LENGTH(Codigo) > 1 ',
      'AND POSITION("." IN Codigo) = 0 ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      PBPos := 0;
      PBMax := QryAux.RecordCount;
      //
      CriaFormProgresso(FixBugNom);
      DockWindow.AtualizaDados(PBPos, PBMax);
      //
      while not QryAux.Eof do
      begin
        CFOPAtu := QryAux.FieldByName('Codigo').AsString;
        CFOPNew := Copy(CFOPAtu, 1, 1) + '.' + Copy(CFOPAtu, 2, Length(CFOPAtu) - 1);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryAux2, Dmod.MyDB, [
          'SELECT * ',
          'FROM cfop2003 ',
          'WHERE Codigo = "' + CFOPNew + '"',
          '']);
        if QryAux2.RecordCount > 0 then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
            'DELETE FROM cfop2003 ',
            'WHERE Codigo=' + CFOPAtu,
            '']);
        end else
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
            'UPDATE cfop2003 SET ',
            'Codigo=' + CFOPNew,
            'WHERE Codigo=' + CFOPAtu,
            '']);
        end;
        PBPos := PBPos + 1;
        DockWindow.AtualizaDados(PBPos, PBMax);
        //
        QryAux.Next;
      end;
      DockWindow.Hide;
    end;
  finally
    QryAux.Free;
    QryAux2.Free;
    QryUpd.Free;
  end;
end;
{$EndIf}

{$IfNDef SemNFe_0000}
procedure TFmFixBugs.AtualizaTodasNotasCanceladas(FixBugNom: String);
var
  QryAux, QryUpd: TmySQLQuery;
  PBPos, PBMax: Integer;
  ChaveNFe: String;
begin
  QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SELECT Id ',
      'FROM nfecaba ',
      'WHERE infCanc_cStat = 101 ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      PBPos := 0;
      PBMax := QryAux.RecordCount;
      //
      CriaFormProgresso(FixBugNom);
      DockWindow.AtualizaDados(PBPos, PBMax);
      //
      while not QryAux.Eof do
      begin
        ChaveNFe := QryAux.FieldByName('Id').AsString;
        //
        DmNFe_0000.CancelaFaturamento(ChaveNFe);
        //
        PBPos := PBPos + 1;
        DockWindow.AtualizaDados(PBPos, PBMax);
        //
        QryAux.Next;
      end;
      DockWindow.Hide;
    end;
  finally
    QryAux.Free;
    QryUpd.Free;
  end;
end;
{$EndIf}

{$IfNDef sBLQ}
procedure TFmFixBugs.AtualizaModeloDeBoletoPrev(FixBugNom: String);
var
  QryAux, QryUpd: TmySQLQuery;
  Aviso01, Aviso02, Aviso03, Aviso04, Aviso05, Aviso06, Aviso07, Aviso08,
  Aviso09, Aviso10, AvisoVerso: String;
  Codigo, ModelBloq, BloqFV, Compe, PBPos, PBMax: Integer;
begin
  QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  //
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SELECT Codigo, ModelBloq, BloqFV, Compe, AvisoVerso, ',
      'Aviso01, Aviso02, Aviso03, Aviso04, Aviso05, ',
      'Aviso06, Aviso07, Aviso08, Aviso09, Aviso10 ',
      'FROM prev ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      PBPos := 0;
      PBMax := QryAux.RecordCount;
      //
      CriaFormProgresso(FixBugNom);
      DockWindow.AtualizaDados(PBPos, PBMax);
      //
      while not QryAux.Eof do
      begin
        Codigo     := QryAux.FieldByName('Codigo').AsInteger;
        Aviso01    := QryAux.FieldByName('Aviso01').AsString;
        Aviso02    := QryAux.FieldByName('Aviso02').AsString;
        Aviso03    := QryAux.FieldByName('Aviso03').AsString;
        Aviso04    := QryAux.FieldByName('Aviso04').AsString;
        Aviso05    := QryAux.FieldByName('Aviso05').AsString;
        Aviso06    := QryAux.FieldByName('Aviso06').AsString;
        Aviso07    := QryAux.FieldByName('Aviso07').AsString;
        Aviso08    := QryAux.FieldByName('Aviso08').AsString;
        Aviso09    := QryAux.FieldByName('Aviso09').AsString;
        Aviso10    := QryAux.FieldByName('Aviso10').AsString;
        AvisoVerso := QryAux.FieldByName('AvisoVerso').AsString;
        ModelBloq  := QryAux.FieldByName('ModelBloq').AsInteger;
        BloqFV     := QryAux.FieldByName('BloqFV').AsInteger;
        Compe      := QryAux.FieldByName('Compe').AsInteger;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
          'UPDATE arreits SET ',
          'Aviso01="' + Aviso01 + '", Aviso02="' + Aviso02 + '", ',
          'Aviso03="' + Aviso03 + '", Aviso04="' + Aviso04 + '", ',
          'Aviso05="' + Aviso05 + '", Aviso06="' + Aviso06 + '", ',
          'Aviso07="' + Aviso07 + '", Aviso08="' + Aviso08 + '", ',
          'Aviso09="' + Aviso09 + '", Aviso10="' + Aviso10 + '", ',
          'AvisoVerso="' + AvisoVerso + '", ModelBloq=' + Geral.FF0(ModelBloq) + ', ',
          'BloqFV=' + Geral.FF0(BloqFV) + ', Compe=' + Geral.FF0(Compe),
          'WHERE Codigo=' + Geral.FF0(Codigo),
          '']);
        //
        PBPos := PBPos + 1;
        DockWindow.AtualizaDados(PBPos, PBMax);
        //
        QryAux.Next;
      end;
      DockWindow.Hide;
    end;
  finally
    Screen.Cursor := crDefault;
    //
    QryAux.Free;
    QryUpd.Free;
  end;
end;
{$EndIf}

{$IfNDef sBLQ}
procedure TFmFixBugs.Migra_BloToProtocolo();
var
  Qry: TmySQLQuery;
  BloEnPr, BloEnPrIt: Integer;
begin
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    //Migra e exclui tabela "bloenpr"
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW TABLES ',
      'LIKE "bloenpr" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'INSERT INTO proenpr (Codigo, Protocolo) ',
        'SELECT Codigo, Protocolo  ',
        'FROM bloenpr ',
        '']) then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
          'DROP TABLE bloenpr ',
          '']);
      end;
    end;
    //Migra e exclui tabela "bloenprit"
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW TABLES ',
      'LIKE "bloenprit" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      try
        if UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
          'INSERT INTO proenprit (Codigo, Controle, Entidade, Lk, DataCad, ',
          'DataAlt, UserCad, UserAlt, AlterWeb, Ativo) ',
          'SELECT Codigo, Controle, Entidade, Lk, DataCad, ',
          'DataAlt, UserCad, UserAlt, AlterWeb, Ativo  ',
          'FROM bloenprit ',
          '']) then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
            'DROP TABLE bloenprit ',
            '']);
        end;
      except
        ;
      end;
    end;
    //Migra e exclui coluna "bloenpr" da tabela "controle"
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW Columns ',
      'FROM controle ',
      'LIKE "bloenpr" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT BloEnPr ',
        'FROM controle ',
        '']);
      BloEnPr := Qry.FieldByName('BloEnPr').AsInteger;
      //
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE controle SET ProEnPr=' + Geral.FF0(BloEnPr),
        '']) then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
          'ALTER TABLE controle ',
          'DROP COLUMN BloEnPr ',
            '']);
      end;
    end;
    //Migra e exclui coluna "bloenprit" da tabela "controle"
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW Columns ',
      'FROM controle ',
      'LIKE "bloenprit" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT BloEnPrIt ',
        'FROM controle ',
        '']);
      BloEnPrIt := Qry.FieldByName('BloEnPrIt').AsInteger;
      //
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
        'UPDATE controle SET ProEnPrIt=' + Geral.FF0(BloEnPrIt),
        '']) then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
          'ALTER TABLE controle ',
          'DROP COLUMN BloEnPrIt ',
            '']);
      end;
    end;
  finally
    Qry.Free;
  end;
end;
{$EndIf}

{$IfNDef sBLQ}
procedure TFmFixBugs.AtualizaProtocolosBoletos(FixBugNom: String);
var
  QryAux, QryUpd: TmySQLQuery;
  Empresa, Entidade, ProtocoPak, ProtocoPakCR, CNAB_Cfg, PBPos, PBMax: Integer;
  Boleto: Double;
begin
  Screen.Cursor := crHourGlass;
  //
  QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW COLUMNS ',
      'FROM arreits ',
      'LIKE "ProtocoPakCR" ',
      '']);
    if QryAux.RecordCount > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT ari.*, pre.Empresa ',
        'FROM arreits ari ',
        'LEFT JOIN prev pre ON pre.Codigo = ari.Codigo ',
        'WHERE (ari.ProtocoPak <> 0 OR ari.ProtocoPakCR <> 0) ',
        '']);
      if QryAux.RecordCount > 0 then
      begin
        PBPos := 0;
        PBMax := QryAux.RecordCount;
        //
        CriaFormProgresso(FixBugNom);
        DockWindow.AtualizaDados(PBPos, PBMax);
        //
        while not QryAux.Eof do
        begin
          Boleto       := QryAux.FieldByName('Boleto').AsFloat;
          Empresa      := QryAux.FieldByName('Empresa').AsInteger;
          Entidade     := QryAux.FieldByName('Entidade').AsInteger;
          CNAB_Cfg     := QryAux.FieldByName('CNAB_Cfg').AsInteger;
          ProtocoPak   := QryAux.FieldByName('ProtocoPak').AsInteger;
          ProtocoPakCR := QryAux.FieldByName('ProtocoPakCR').AsInteger;
          //
          if ProtocoPak <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
              'UPDATE protpakits SET ',
              'ID_Cod2=' + Geral.FF0(Empresa) + ', ',
              'ID_Cod4=' + Geral.FF0(CNAB_Cfg) + ' ',
              'WHERE Docum=' + Geral.FFI(Boleto),
              'AND Cliente=' + Geral.FF0(Entidade),
              'AND Controle=' + Geral.FF0(ProtocoPak),
              '']);
          end;
          if ProtocoPakCR <> 0 then
          begin
            UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
              'UPDATE protpakits SET ',
              'ID_Cod2=' + Geral.FF0(Empresa) + ', ',
              'ID_Cod4=' + Geral.FF0(CNAB_Cfg) + ' ',
              'WHERE Docum=' + Geral.FFI(Boleto),
              'AND Cliente=' + Geral.FF0(Entidade),
              'AND Controle=' + Geral.FF0(ProtocoPakCR),
              '']);
          end;
          //
          PBPos := PBPos + 1;
          DockWindow.AtualizaDados(PBPos, PBMax);
          //
          QryAux.Next;
        end;
        DockWindow.Hide;
      end;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
  end;
end;
{$EndIf}

{$IFNDEF NO_USE_EMAILDMK}
procedure TFmFixBugs.AtualizaPreEmail();

  function LocalizaMailCfg(Send_Mail: String): Integer;
  var
    QryAux: TmySQLQuery;
  begin
    Result := 0;
    QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM emailconta ',
        'WHERE Usuario = "' + Send_Mail + '" ',
        'AND TipoConta=1 ',
        'AND PermisNiv=0 ',
        '']);
      if QryAux.RecordCount > 0 then
        Result := QryAux.FieldByName('Codigo').AsInteger;
    finally
      QryAux.Free;
    end;
  end;
var
  QryAux, QryUpd: TmySQLQuery;
  PermisNiv, MailCfg: Integer;
begin
  QryAux := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  QryUpd := TmySQLQuery.Create(TDataModule(Dmod.MyDB));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
      'SHOW COLUMNS FROM preemail LIKE "Send_Mail"',
      '']);
    if QryAux.RecordCount > 0 then
    begin
     //Cadastra contas
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT * ',
        'FROM preemail ',
        'GROUP BY Send_Mail ',
        '']);
      if QryAux.RecordCount > 0 then
      begin
        QryAux.First;
        while not QryAux.EOF do
        begin
          MailCfg   := LocalizaMailCfg(QryAux.FieldByName('Send_Mail').AsString);
          PermisNiv := 0;
          //
          if MailCfg = 0 then
            FmMailCfg.InsereMailCfg(stIns, PermisNiv, 'Conta e-mail POP',
              QryAux.FieldByName('SMTPServer').AsString,
              QryAux.FieldByName('Send_Name').AsString,
              QryAux.FieldByName('Send_Mail').AsString,
              QryAux.FieldByName('Pass_Mail').AsString,
              QryAux.FieldByName('Logi_Name').AsString,
              QryAux.FieldByName('Logi_Pass').AsString, '',
              QryAux.FieldByName('Logi_Auth').AsInteger,
              QryAux.FieldByName('Porta_Mail').AsInteger,
              QryAux.FieldByName('Logi_SSL').AsInteger,
              0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, '');
          //
          QryAux.Next;
        end;
      end;
      //Atualiza pr� e-mail
      UnDmkDAC_PF.AbreMySQLQuery0(QryAux, Dmod.MyDB, [
        'SELECT Codigo, Usuario ',
        'FROM emailconta ',
        'WHERE PermisNiv=0 ',
        'AND TipoConta=1 ',
        '']);
      if QryAux.RecordCount > 0 then
      begin
        QryAux.First;
        while not QryAux.EOF do
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Dmod.MyDB, [
            'UPDATE preemail SET EmailConta=' + Geral.FF0(QryAux.FieldByName('Codigo').AsInteger),
            'WHERE Send_Mail="' + QryAux.FieldByName('Usuario').AsString + '"',
            '']);
          //
          QryAux.Next;
        end;
      end;
    end;
  finally
    QryAux.Free;
    QryUpd.Free;
  end;
end;
{$EndIf}

(* Desativado em: 30/08/2016 => Motivc: est� zerando as cidades
procedure TUnEntities.AtualizaEntidadesParaEntidade2(ProgreesBar: TProgressBar;
  DataBase, AllDataBase: TmySQLDatabase);

  function InsereEntiMail(Email: String; Codigo, Controle: Integer;
    Database: TmySQLDatabase): Integer;
  var
    QueryUpd: TmySQLQuery;
    Conta: Integer;
  begin
    Result   := 0;
    QueryUpd := TmySQLQuery.Create(DataBase);
    QueryUpd.Database := Database;
    try
      Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
      //
      if UMyMod.SQLInsUpd(QueryUpd, stIns, 'entimail', False,
        ['EMail', 'Codigo', 'Controle'], ['Conta'],
        [Email, Codigo, Controle], [Conta], True) then
      begin
        Result := Conta;
      end;
    finally
      QueryUpd.Free;
    end;
  end;

var
  QueryUpd, QueryUpd2, QueryEnt: TmySQLQuery;
  Codigo, Controle, ECodiPais, PCodiPais, ECodMunici, PCodMunici,
  CCodMunici, LCodMunici: Integer;
  EEmail, PEmail, EPais, PPais, ECidade, PCidade, CCidade,
  LCidade: String;
begin
  QueryEnt  := TmySQLQuery.Create(DataBase);
  QueryUpd  := TmySQLQuery.Create(DataBase);
  QueryUpd2 := TmySQLQuery.Create(DataBase);
  //
  QueryEnt.Database  := DataBase;
  QueryUpd.Database  := DataBase;
  QueryUpd2.Database := DataBase;
  try
    Screen.Cursor := crHourGlass;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QueryEnt, DataBase, [
      'SELECT Codigo, EEmail, PEmail, EPais, PPais, ',
      'ECidade, PCidade, CCidade, LCidade ',
      'FROM entidades ',
      '']);
    if QueryEnt.RecordCount > 0 then
    begin
      ProgreesBar.Visible  := True;
      ProgreesBar.Position := 0;
      ProgreesBar.Max      := QueryEnt.RecordCount;
      //
      QueryEnt.First;
      while not QueryEnt.Eof do
      begin
        ProgreesBar.Position := ProgreesBar.Position + 1;
        ProgreesBar.Update;
        Application.ProcessMessages;
        //
        Codigo := QueryEnt.FieldByName('Codigo').AsInteger;
        //
        EPais := QueryEnt.FieldByName('EPais').AsString;
        PPais := QueryEnt.FieldByName('PPais').AsString;
        //
        ECidade := QueryEnt.FieldByName('ECidade').AsString;
        PCidade := QueryEnt.FieldByName('PCidade').AsString;
        CCidade := QueryEnt.FieldByName('CCidade').AsString;
        LCidade := QueryEnt.FieldByName('LCidade').AsString;
        //
        EEmail := QueryEnt.FieldByName('EEmail').AsString;
        PEmail := QueryEnt.FieldByName('PEmail').AsString;
        //
        ECodiPais := ObtemCodBacenPais(EPais, AllDataBase);
        PCodiPais := ObtemCodBacenPais(PPais, AllDataBase);
        //
        ECodMunici := ObtemCodMunicipio(ECidade, AllDataBase);
        PCodMunici := ObtemCodMunicipio(PCidade, AllDataBase);
        CCodMunici := ObtemCodMunicipio(CCidade, AllDataBase);
        LCodMunici := ObtemCodMunicipio(LCidade, AllDataBase);
        //
        if UMyMod.SQLInsUpd(QueryUpd, stUpd, 'entidades', False,
          [
          'ECodiPais', 'PCodiPais', 'ECodMunici',
          'PCodMunici', 'CCodMunici', 'LCodMunici'
          ], ['Codigo'],
          [
          ECodiPais, PCodiPais, ECodMunici,
          PCodMunici, CCodMunici, LCodMunici
          ], [Codigo], True) then
        begin
          if (EEmail <> '') or (PEmail <> '') then
          begin
            Controle := CriaContatoGenerico(Codigo, QueryUpd2);
            //
            if (Controle <> 0) and (EEmail <> '') then
              InsereEntiMail(EEmail, Codigo, Controle, DataBase);
            if (Controle <> 0) and (PEmail <> '') then
              InsereEntiMail(PEmail, Codigo, Controle, DataBase);
          end;
        end;
        //
        QueryEnt.Next;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, DataBase, [
        'UPDATE ctrlgeral SET AtualizouEntidades=1',
        '']);
    end;
  finally
    QueryEnt.Free;
    QueryUpd.Free;
    QueryUpd2.Free;
    //
    ProgreesBar.Visible := False;
    //
    Screen.Cursor := crDefault;
    //
    Geral.MB_Aviso('Atualiza��o de entidades finalizada!');
  end;
end;
*)
///////////////////////////////// FUN��ES DE ATUALIZA��ES GERAIS - FIM    //////

end.
