unit UnFixBugs;

interface

uses System.Math, Forms, Controls, Windows, SysUtils, ComCtrls, Classes,
  ExtCtrls, UnInternalConsts, mySQLDbTables, Dialogs, UnDmkEnums, Variants, DB,
  DmkDAC_PF, UnDmkProcFunc;

type
  TUnFixBugs = class(TObject)
  private

  public
    function  ListaFixBugs_Cod(Idx: Integer): Largeint;
    procedure MostraFixBugs(Lista_Str: array of String; MarcarTodos: Boolean = True);
  end;

var
  UFixBugs: TUnFixBugs;

implementation

uses dmkGeral, UnMyObjects, Module, MyDBCheck, FixBugs;

{ TUnFixBugs }

function TUnFixBugs.ListaFixBugs_Cod(Idx: Integer): Largeint;
begin
  Result := Trunc(Power(2, Idx));
end;

procedure TUnFixBugs.MostraFixBugs(Lista_Str: array of String;
  MarcarTodos: Boolean = True);
begin
  if MarcarTodos = False then
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if DBCheck.CriaFm(TFmFixBugs, FmFixBugs, afmoNegarComAviso) then
  begin
    FmFixBugs.ConfiguraAtualizacoesGerais([
      'Migrando atualiza��es',
      'Atualizando 9 d�gito nas entidades',
      'Corrigindo faturas NF-es',
      'Corrigindo notas canceladas',
      'Corrigindo modelos de boletos',
      'Migrando protocolos de boletos',
      'Corrigindo protocolos de boletos',
      'Atualizando pr� e-mails',
      'Atualizando tabelas de log',
      'Atualizando tabela CFOP'
    ], MarcarTodos);
    FmFixBugs.ConfiguraAtualizacoesApp(Lista_Str, MarcarTodos);
    //
    if MarcarTodos then
      FmFixBugs.ExecutaAtualizacoes
    else
      FmFixBugs.ShowModal;
    //
    FmFixBugs.Destroy;
  end;
end;

end.
