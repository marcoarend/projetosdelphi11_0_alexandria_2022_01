{*******************************************************}
{                                                       }
{       GSM Demo project for                            }
{       nrCommLib Communication Library for Delphi      }
{       Copyright (c) 1997-2008 Roman Novgorodov        }
{                                                       }
{       e-mail:noro@deepsoftware.com                    }
{       DeepSoftware                                    }
{       http://www.deepsoftware.com/                    }
{                                                       }
{*******************************************************}

unit GSM_Serial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, nrclasses, nratcmd, nrgsm, nrcomm, StdCtrls, nrcommbox,
  nrlogfile, Spin, nrgsmpdu, ComCtrls, ExtCtrls, dmkImage,
  Buttons, DmkGeral, dmkEdit;

type
  TFmGSM_Serial = class(TForm)
    nrComm1: TnrComm;                      
    nrGsm1: TnrGsm;
    nrLogFile1: TnrLogFile;
    Panel2: TPanel;
    CheckBox1: TCheckBox;
    nrDeviceBox1: TnrDeviceBox;
    Button1: TButton;
    Button3: TButton;
    Label11: TLabel;
    EdPIN: TdmkEdit;
    Panel3: TPanel;
    Pages: TPageControl;
    TabSheet3: TTabSheet;
    lvInfo: TListView;
    TabSheet1: TTabSheet;
    Label3: TLabel;
    Label7: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    Memo1: TMemo;
    chConfirm: TCheckBox;
    Button8: TButton;
    chAlert: TCheckBox;
    cbPort: TComboBox;
    ePortSrc: TSpinEdit;
    ePortDest: TSpinEdit;
    TabSheet2: TTabSheet;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    eMem: TComboBox;
    eNo: TSpinEdit;
    Button4: TButton;
    Button6: TButton;
    Memo2: TMemo;
    Button5: TButton;
    cbStatus: TComboBox;
    pgTerminal: TTabSheet;
    Memo3: TMemo;
    pgBook: TTabSheet;
    Label8: TLabel;
    lvPhones: TListView;
    Panel1: TPanel;
    ePhoneBookType: TComboBox;
    Button7: TButton;
    pgOptions: TTabSheet;
    Label2: TLabel;
    Label12: TLabel;
    cbOnNewSms: TComboBox;
    eCmd: TEdit;
    bSendCommand: TButton;
    pgDriver: TTabSheet;
    Label9: TLabel;
    chAutodetect: TCheckBox;
    eDrivers: TComboBox;
    bLoadDriver: TButton;
    bDelAll: TButton;
    Panel4: TPanel;
    Label1: TLabel;
    ListBox1: TListBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure CheckBox1Click(Sender: TObject);
    procedure nrGsm1AfterInit(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure nrGsm1GsmError(Sender: TObject; sData: String);
    procedure nrGsm1Ring(Sender: TObject; sData: String);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure nrGsm1SmsReceived(Sender: TObject; aMem: String;
      idSms: Integer; aSms: TnrPduSms);
    procedure nrGsm1SmsListItem(Sender: TObject; aMem: String;
      idSms: Integer; aSms: TnrPduSms);
    procedure Button6Click(Sender: TObject);
    procedure cbOnNewSmsChange(Sender: TObject);
    procedure nrGsm1SmsSent(Sender: TObject; aMem: String; idSms: Integer;
      aSms: TnrPduSms);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure nrGsm1PhoneListFinish(Sender: TObject);
    procedure nrGsm1PhoneListProgress(Sender: TObject;
      var flBreak: Boolean);
    procedure Memo3DblClick(Sender: TObject);
    procedure chAutodetectClick(Sender: TObject);
    procedure bLoadDriverClick(Sender: TObject);
    procedure bDelAllClick(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure bSendCommandClick(Sender: TObject);
    procedure cbPortChange(Sender: TObject);
    procedure EdPINDblClick(Sender: TObject);
    procedure nrDeviceBox1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure nrGsm1DataSend(Sender: TObject; ptrBuffer: PAnsiChar;
      Len: Cardinal; flSynchro: Boolean; var flHandled: Boolean);
    procedure nrGsm1Diagnostic(Sender: TObject; aAT, aReply: string;
      var flBreak: Boolean);
    procedure nrGsm1FatalError(Sender: TObject; ErrorCode, Detail: Cardinal;
      ErrorMsg: string; var RaiseException: Boolean);
    procedure nrGsm1GsmChangeState(Sender: TObject);
    procedure nrGsm1Unsolicited(Sender: TObject; sData: string);
  private
    { Private declarations }
    procedure WMDeviceChange(var Msg:TMessage); message WM_DEVICECHANGE;
    procedure AddInfo(sCapt, sVal: string);
    procedure ReloadDriveList;
    procedure SendtSmsAsPdu;
  public
    { Public declarations }
  end;

var
  FmGSM_Serial: TFmGSM_Serial;

implementation

uses clipbrd, UnMyObjects;

{$R *.dfm}

procedure TFmGSM_Serial.FormShow(Sender: TObject);
var i:integer;
begin
  ReloadDriveList;
  chAutodetect.Checked := nrGsm1.Autodetect;
  Pages.ActivePageIndex := 0;
  for i := 0 to lvInfo.Items.Count - 1 do lvInfo.Items[i].SubItems.Add('...');
  cbOnNewSms.ItemIndex := Integer(nrGsm1.NewSmsMode);
  EdPin.Text := nrGsm1.PIN;
end;

procedure TFmGSM_Serial.CheckBox1Click(Sender: TObject);
begin
  try
    nrGsm1.NewSmsMode := TgsmNewSmsMode(cbOnNewSms.ItemIndex);
    nrGsm1.Autodetect := chAutodetect.Checked;
    nrGsm1.PIN := EdPin.Text;
    if not nrGsm1.Autodetect then nrGsm1.DriverIndex := eDrivers.ItemIndex;

    nrComm1.Active := CheckBox1.Checked;
  except
    CheckBox1.Checked := nrComm1.Active;
    raise;
  end;
end;

procedure TFmGSM_Serial.EdPINDblClick(Sender: TObject);
begin
  ShowMessage(IntToHex(nrComm1.Device[nrComm1.DeviceIndex].DevInst, 8) + ' ' + nrComm1.Device[nrComm1.DeviceIndex].InstanceId);
end;

procedure TFmGSM_Serial.AddInfo(sCapt, sVal: string);
var Itm:TListItem;
begin
  if Trim(sVal) = '' then exit;
  with lvInfo do begin
    Itm := Items.Add;
    Itm.Caption := sCapt;
    Itm.SubItems.Add(sVal);
  end;
end;

procedure TFmGSM_Serial.nrDeviceBox1Click(Sender: TObject);
begin
  CheckBox1.Enabled := nrDeviceBox1.ItemIndex > -1;
end;

procedure TFmGSM_Serial.nrGsm1AfterInit(Sender: TObject);
begin
  ListBox1.Items.Add('========= Aparelho GSM conectado ===========');
  lvInfo.Clear;
  //AddInfo('Manufacturer', nrGsm1.DeviceManufacturer);
  AddInfo('Fabricante', nrGsm1.DeviceManufacturer);
  AddInfo('Aparelho', nrGsm1.DeviceName);
  AddInfo('Vers�o', nrGsm1.DeviceVersion);
  AddInfo('IMEI', nrGsm1.DeviceIMEI);
  AddInfo('Driver', nrGsm1.Driver.Caption);

  eMem.Items.Text := nrGsm1.MemTypesRead.Text;
  eMem.ItemIndex := eMem.Items.Count - 1;
  ListBox1.TopIndex := ListBox1.Count - 1;

end;

procedure TFmGSM_Serial.nrGsm1DataSend(Sender: TObject; ptrBuffer: PAnsiChar;
  Len: Cardinal; flSynchro: Boolean; var flHandled: Boolean);
begin
  ListBox1.Items.Add('ptrBuffer: ' + PAnsiChar(ptrBuffer));
  ListBox1.Items.Add('Len: ' + IntToStr(Len));
  ListBox1.Items.Add('flSynchro: ' + Geral.FF0(Geral.BoolToInt(flSynchro)));
  ListBox1.Items.Add('flHandled: ' + Geral.FF0(Geral.BoolToInt(flHandled)));
end;

procedure TFmGSM_Serial.nrGsm1Diagnostic(Sender: TObject; aAT, aReply: string;
  var flBreak: Boolean);
begin
  ListBox1.Items.Add('aAT: ' + aAT);
  ListBox1.Items.Add('aRepaly: ' + aReply);
  ListBox1.Items.Add('flBreak: ' + Geral.FF0(Geral.BoolToInt(flBreak)));
end;

procedure TFmGSM_Serial.nrGsm1FatalError(Sender: TObject; ErrorCode,
  Detail: Cardinal; ErrorMsg: string; var RaiseException: Boolean);
begin
  ListBox1.Items.Add('ErrorCode: ' + Geral.FF0(ErrorCode));
  ListBox1.Items.Add('Detail: ' + Geral.FF0(Detail));
  ListBox1.Items.Add('ErrorMsg: ' + ErrorMsg);
end;

procedure TFmGSM_Serial.Button2Click(Sender: TObject);
begin
  {
  if MyObjects.FIC(EdPIN.ValueVariant = 0, EdPIN,
  'Informe o PIN da operadora!')then
    Exit;
  }
  if (not chAlert.Checked) and  (cbPort.ItemIndex <= 0)
    then nrGSm1.SmsSend(Edit1.Text,Memo1.Text, chConfirm.Checked)
    else SendtSmsAsPdu;
end;

procedure TFmGSM_Serial.nrGsm1GsmChangeState(Sender: TObject);
begin
  ListBox1.Items.Add('GSM change state!');
end;

procedure TFmGSM_Serial.nrGsm1GsmError(Sender: TObject; sData: String);
begin
  ListBox1.Items.Add('Erro GSM: ' + sData);
  ListBox1.TopIndex := ListBox1.Count - 1;
  ListBox1.ItemIndex := ListBox1.Count - 1;
end;

procedure TFmGSM_Serial.nrGsm1Ring(Sender: TObject; sData: String);
begin
  ListBox1.Items.Add('Chamada: ' + sData);
  ListBox1.TopIndex := ListBox1.Count - 1;
  ListBox1.ItemIndex := ListBox1.Count - 1;
end;

procedure TFmGSM_Serial.Button3Click(Sender: TObject);
begin
  nrGsm1.RunDiagnostic;
  ShowMessage('O arquivo de log com diagn�stico de conforma��o foi salvo como '#13+
               nrLogFile1.FileName);
end;

procedure TFmGSM_Serial.Button1Click(Sender: TObject);
begin
  nrComm1.ConfigDialog;
end;

procedure TFmGSM_Serial.WMDeviceChange(var Msg: TMessage);
begin
  nrComm1.WMDeviceChange(Msg);
end;

procedure TFmGSM_Serial.Button5Click(Sender: TObject);
begin
  nrGSM1.MemoryRead :=  eMem.Text;
  if nrGSM1.SmsDelete(eNo.Value)
    then ListBox1.AddItem('OK',nil)
    else ListBox1.AddItem('Erro',nil) ;
end;

procedure TFmGSM_Serial.nrGsm1SmsReceived(Sender: TObject; aMem: String;
  idSms: Integer; aSms: TnrPduSms);
begin

  if aMem <> ''
    then ListBox1.Items.Add('SMS recebido "' + aMem+'",'+ IntToStr(idSms))
    else ListBox1.Items.Add('SMS recebido ');

  if aSms <> nil then begin
    if aSms.Report
      then ListBox1.Items.Add(aSms.ReportText)
      else ListBox1.Items.Add('SMS: de: ' + aSms.Phone + ' texto: '+ aSms.Text);
  end;

  ListBox1.TopIndex := ListBox1.Count - 1;
  ListBox1.ItemIndex := ListBox1.Count - 1;

end;

procedure TFmGSM_Serial.nrGsm1SmsListItem(Sender: TObject; aMem: String;
  idSms: Integer; aSms: TnrPduSms);
begin
  if aSms <> nil then begin
    if aSms.Report then begin
      Memo2.Lines.Add('RELAT�RIO: ' + aMem + ',' + IntToStr(idSms) + ' ======= ');
      Memo2.Lines.Add(aSms.ReportText);
    end else begin
      Memo2.Lines.Add('SMS: ' + aMem + ',' + IntToStr(idSms) + ' ======= ');
      Memo2.Lines.Add(aSms.Text);
    end;
  end else Memo2.Lines.Add('======= LISTA DE SMS FINALIZADA!!! =============');
end;

procedure TFmGSM_Serial.Button6Click(Sender: TObject);
begin
  nrGSM1.MemoryRead :=  eMem.Text;
  nrGSM1.SmsList(False, TSmsStatus(cbStatus.ItemIndex));
end;

procedure TFmGSM_Serial.Button4Click(Sender: TObject);
begin
  nrGSM1.MemoryRead :=  eMem.Text;
  with nrGSM1.SmsGet(eNo.Value) do
    if not IsEmpty then begin
      if Report
        then Memo2.Text := ReportText
        else Memo2.Text := Text;
      ListBox1.AddItem('OK',nil);
    end else ListBox1.AddItem('Erro', nil);
end;

procedure TFmGSM_Serial.cbOnNewSmsChange(Sender: TObject);
begin
  nrGsm1.NewSmsMode := TgsmNewSmsMode(cbOnNewSms.ItemIndex);
end;

procedure TFmGSM_Serial.cbPortChange(Sender: TObject);
begin
  ePortSrc.Enabled := cbPort.ItemIndex > 0;
  ePortDest.Enabled := cbPort.ItemIndex > 0;
end;

procedure TFmGSM_Serial.nrGsm1SmsSent(Sender: TObject; aMem: String;
  idSms: Integer; aSms: TnrPduSms);
begin
  ListBox1.Items.Add('O SMS foi enviado');
  ListBox1.TopIndex := ListBox1.Count - 1;
  ListBox1.ItemIndex := ListBox1.Count - 1;
end;

procedure TFmGSM_Serial.nrGsm1Unsolicited(Sender: TObject; sData: string);
begin
  ListBox1.Items.Add('Unsolicited: ' + sdata);
end;

procedure TFmGSM_Serial.Button7Click(Sender: TObject);
begin
  nrGsm1.PhoneListType := TnrPhoneBookType(ePhoneBookType.ItemIndex);
  nrGsm1.PhoneBookRead;
  Label8.Caption := 'Lendo ... ';
end;

procedure TFmGSM_Serial.nrGsm1PhoneListFinish(Sender: TObject);
var i : integer;
    itm:TListItem;
begin
  lvPhones.Clear;
  for i := 0 to nrGsm1.PhoneList.Count - 1 do begin
    itm := lvPhones.Items.Add;
    itm.Caption := nrGsm1.PhoneList.Item[i].Caption;
    itm.SubItems.Add(nrGsm1.PhoneList.Item[i].Phone);
  end;
  Label8.Caption := 'Pronto. Total:'+IntToStr(nrGsm1.PhoneList.Count);
end;

procedure TFmGSM_Serial.nrGsm1PhoneListProgress(Sender: TObject;
  var flBreak: Boolean);
begin
  Label8.Caption := 'Lendo :'+ IntToStr(nrGsm1.PhoneList.Count);
end;

procedure TFmGSM_Serial.FormCreate(Sender: TObject);
begin
  // hide terminal page ...
  lvInfo.Items.Add;

  if (ParamCount > 0) and (ParamStr(1) = 'd')
    then exit;

  nrComm1.Terminal := nil;
  pgTerminal.Free;
end;

procedure TFmGSM_Serial.Memo3DblClick(Sender: TObject);
var s:string;
    i:integer;
begin
  // insert from clipboard into  terminal ...
  s := Clipboard.AsText;
  s := StringReplace(s, '\1Ah', #$1a, []);
  s := StringReplace(s, '\0Dh', #$0D, []);
  for i := 1 to Length(s) do PostMessage(Memo3.Handle, WM_CHAR, DWord(s[i]),0 );

end;

procedure TFmGSM_Serial.chAutodetectClick(Sender: TObject);
begin
  eDrivers.Enabled := not chAutodetect.Checked;
  bLoadDriver.Enabled := eDrivers.Enabled;
  bDelAll.Enabled := eDrivers.Enabled;
end;

procedure TFmGSM_Serial.bLoadDriverClick(Sender: TObject);
var dlg:TOpenDialog;
begin
  dlg := TOpenDialog.Create(Self);
  dlg.Filter := '*.inf|*.inf|*.*|*.*';
  if dlg.Execute
    then nrGsm1.AddDriverFromFile('', dlg.FileName);
  dlg.Free;
  ReloadDriveList;
end;

procedure TFmGSM_Serial.ReloadDriveList;
var i : integer;
begin
  eDrivers.Clear;
  for i := 0 to nrGsm1.DriverCount - 1
    do eDrivers.Items.Add(nrGsm1.Drivers[i].Caption);
end;

procedure TFmGSM_Serial.bDelAllClick(Sender: TObject);
begin
  nrGsm1.DriversClearAll;
  ReloadDriveList;
end;

procedure TFmGSM_Serial.Button8Click(Sender: TObject);
var s:string;
begin
  if MyObjects.FIC(EdPIN.ValueVariant = 0, EdPIN,
  'Informe o PIN da operadora!')then
    Exit;
  s := nrGSm1.SmsSendWaitRefID(Edit1.Text, Memo1.Text, chConfirm.Checked);
  ShowMessage('SMS enviado. Ref#:' + s);
end;

procedure TFmGSM_Serial.bSendCommandClick(Sender: TObject);
var s:string;
begin
  s := nrGsm1.CmdSendAndWaitResult(eCmd.Text);
  ShowMessage('Resposta:' + s);
end;

procedure TFmGSM_Serial.SendtSmsAsPdu;
var pdu:TnrPduSms;
begin
  pdu := TnrPduSms.Create(false);
  pdu.Text := Memo1.Text;
  pdu.Phone := Edit1.Text;
  pdu.Confirm := chConfirm.Checked;
  pdu.Alert := True;
  pdu.PortType := TnrPduPort(cbPort.ItemIndex);
  pdu.PortSrc := ePortSrc.Value;
  pdu.PortDest := ePortDest.Value;
  nrGsm1.SmsSend(pdu);
  pdu.Free;
end;

procedure TFmGSM_Serial.SpeedButton1Click(Sender: TObject);
begin
  Geral.MB_Info('PIN padr�es' + sLineBreak + '-----------' + sLineBreak +
                    '1010 - TIM' + sLineBreak +
                    '3636 - Claro' + sLineBreak +
                    '8888 - OI' + sLineBreak +
                    '1414 - BrT GSM' + sLineBreak +
                    '1414 - Amaz/Telemig' + sLineBreak +
                    '1212 - CTBC' + sLineBreak +
                    '8486 - Vivo');
end;

procedure TFmGSM_Serial.SpeedButton2Click(Sender: TObject);
begin
  ShowMessage(nrGsm1.PIN);
end;

{
 // Operadora
 AT+COPS?<cr>
}

{
SMS
To send a text message, you can use the AT+CMGS="+15555555555" command,
where you specify the addressees phone number.
By default, the Telit will be in PDU mode,
you can change this by setting the message format to normal text with AT+CMGF=1.
In that case, some of the commands will have to be sent using text as well
}

{
http://www.developershome.com/sms/
}
end.
