unit PortSerBal;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkEdit,
  UnDmkProcFunc, dmkGeral;

type
  TFmPortSerBal = class(TForm)
    PainelDados: TPanel;
    DsPortSerBal: TDataSource;
    QrPortSerBal: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdDescricao: TdmkEdit;
    GroupBox1: TGroupBox;
    CbCommPort: TComboBox;
    Label3: TLabel;
    CbBaudRate: TComboBox;
    Label4: TLabel;
    CbDataBits: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    CbParity: TComboBox;
    GroupBox2: TGroupBox;
    RgRTS_input: TRadioGroup;
    RgDTR_input: TRadioGroup;
    CkCTS_output: TCheckBox;
    CkDSR_output: TCheckBox;
    CkXonXof_input: TCheckBox;
    CkXonXof_output: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    QrPesq: TmySQLQuery;
    Label8: TLabel;
    CbStopBits: TComboBox;
    GroupBox3: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    GroupBox4: TGroupBox;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    QrPortSerBalPARITY_NOME: TWideStringField;
    QrPortSerBalCodigo: TIntegerField;
    QrPortSerBalDescricao: TWideStringField;
    QrPortSerBalCommPort: TSmallintField;
    QrPortSerBalBaudRate: TSmallintField;
    QrPortSerBalStopBits: TSmallintField;
    QrPortSerBalDataBits: TSmallintField;
    QrPortSerBalParity: TSmallintField;
    QrPortSerBalRTS_input: TSmallintField;
    QrPortSerBalDTR_input: TSmallintField;
    QrPortSerBalCTS_output: TSmallintField;
    QrPortSerBalDSR_output: TSmallintField;
    QrPortSerBalXonXof_input: TSmallintField;
    QrPortSerBalXonXof_output: TSmallintField;
    QrPortSerBalLk: TIntegerField;
    QrPortSerBalDataCad: TDateField;
    QrPortSerBalDataAlt: TDateField;
    QrPortSerBalUserCad: TIntegerField;
    QrPortSerBalUserAlt: TIntegerField;
    QrPortSerBalAlterWeb: TSmallintField;
    QrPortSerBalAtivo: TSmallintField;
    QrPortSerBalCOMMPORT_NOME: TWideStringField;
    QrPortSerBalBAUDRATE_NOME: TWideStringField;
    QrPortSerBalSTOPBITS_NOME: TWideStringField;
    QrPortSerBalDATABITS_NOME: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPortSerBalAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPortSerBalBeforeOpen(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure QrPortSerBalCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    {
    function CommPortIndexOfText(Texto: String): Integer;
    function CommPortTextOfIndex(Index: Integer): String;
    function ParityIndexOfText(Texto: String): Integer;
    function ParityTextOfIndex(Index: Integer): String;
    function DataBitsIndexOfVal(Val: Integer): Integer;
    function DataBitsValOfIndex(Index: Integer): Integer;
    }
  public
    { Public declarations }
  end;

var
  FmPortSerBal: TFmPortSerBal;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPortSerBal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPortSerBal.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPortSerBalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPortSerBal.DefParams;
begin
  VAR_GOTOTABELA := 'PortSerBal';
  VAR_GOTOMYSQLTABLE := QrPortSerBal;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM portserbal');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPortSerBal.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text           := FormatFloat(FFormatFloat, Codigo);
        EdDescricao.Text        := '';
        CbCommPort.ItemIndex    := 0;
        CbBaudRate.ItemIndex    := 5;//4800;
        CbStopBits.ItemIndex    := 0;//'1';
        CbDataBits.ItemIndex    := 3;//'8';
        CbParity.ItemIndex      := 0;//'None';
        RgRTS_input.ItemIndex   := 0;
        RgDTR_input.ItemIndex   := 1;
        CkCTS_output.Checked    := False;
        CkDSR_output.Checked    := False;
        CkXonXof_input.Checked  := False;
        CkXonXof_output.Checked := False;
      end else begin
        EdCodigo.Text           := DBEdCodigo.Text;
        EdDescricao.Text        := DBEdNome.Text;
        CbCommPort.ItemIndex    := QrPortSerBalCommPort.Value;
        CbBaudRate.ItemIndex    := QrPortSerBalBaudRate.Value;
        CbStopBits.ItemIndex    := QrPortSerBalStopBits.Value;
        CbDataBits.ItemIndex    := QrPortSerBalDataBits.Value;
        CbParity.ItemIndex      := QrPortSerBalParity.Value;
        RgRTS_input.ItemIndex   := QrPortSerBalRTS_input.Value;
        RgDTR_input.ItemIndex   := QrPortSerBalDTR_input.Value;
        CkCTS_output.Checked    := Geral.IntToBool(QrPortSerBalCTS_output.Value);
        CkDSR_output.Checked    := Geral.IntToBool(QrPortSerBalDSR_output.Value);
        CkXonXof_input.Checked  := Geral.IntToBool(QrPortSerBalXonXof_input.Value);
        CkXonXof_output.Checked := Geral.IntToBool(QrPortSerBalXonXof_output.Value);
      end;
      EdDescricao.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPortSerBal.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPortSerBal.AlteraRegistro;
var
  PortSerBal : Integer;
begin
  PortSerBal := QrPortSerBalCodigo.Value;
  if QrPortSerBalCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PortSerBal, Dmod.MyDB, 'PortSerBal', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PortSerBal, Dmod.MyDB, 'PortSerBal', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPortSerBal.IncluiRegistro;
var
  Cursor : TCursor;
  PortSerBal : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PortSerBal := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PortSerBal', 'PortSerBal', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PortSerBal))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, PortSerBal);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmPortSerBal.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPortSerBal.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPortSerBal.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPortSerBal.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPortSerBal.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPortSerBal.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPortSerBal.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmPortSerBal.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPortSerBal.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPortSerBalCodigo.Value;
  Close;
end;

procedure TFmPortSerBal.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Descricao : String;
  //Cadastros: String;
begin
  Descricao := EdDescricao.Text;
  if Length(Descricao) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  {
  N�o fazer, pois poder� ser usado em mais de um computador
  QrPesq.Close;
  QrPesq.Params[00].AsString  := CbCommPort.Text;
  QrPesq.Params[01].AsInteger := IntToStr(EdCodigo.Text);
  QrPesq . O p e n ;
  if QrPesq.RecordCount > 0 then
  begin

    Application.N
  end;
  }
  Codigo := UMyMod.BuscaEmLivreY_Def_Old('portserbal', 'Codigo',
    LaTipo.Caption, QrPortSerBalCodigo.Value);
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'portserbal', False, [
    'CommPort', 'BaudRate', 'StopBits', 'DataBits',
    'Parity', 'RTS_input', 'DTR_input',
    'CTS_output', 'DSR_output',
    'XonXof_input', 'XonXof_output', 'Descricao'
  ], ['Codigo'], [
    CbCommPort.ItemIndex, CbBaudRate.ItemIndex, CbStopBits.ItemIndex,
    CbDataBits.ItemIndex, CbParity.ItemIndex,
    RgRTS_input.ItemIndex, RgDTR_input.ItemIndex,
    Geral.BoolToInt(CkCTS_output.Checked),
    Geral.BoolToInt(CkDSR_output.Checked),
    Geral.BoolToInt(CkXonXof_input.Checked),
    Geral.BoolToInt(CkXonXof_output.Checked), EdDescricao.Text
  ], [Codigo]) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PortSerBal', 'Codigo');
    MostraEdicao(0, CO_TRAVADO, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmPortSerBal.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PortSerBal', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PortSerBal', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PortSerBal', 'Codigo');
end;

procedure TFmPortSerBal.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmPortSerBal.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPortSerBalCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPortSerBal.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPortSerBal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPortSerBal.QrPortSerBalAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPortSerBal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPortSerBal.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPortSerBalCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PortSerBal', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPortSerBal.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPortSerBal.QrPortSerBalBeforeOpen(DataSet: TDataSet);
begin
  QrPortSerBalCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPortSerBal.Button1Click(Sender: TObject);
begin
  RgRTS_input.ItemIndex := 2;
  CkCTS_output.Checked  := True;
end;

procedure TFmPortSerBal.Button2Click(Sender: TObject);
begin
  RgDTR_input.ItemIndex := 2;
  CkDSR_output.Checked  := True;
end;

procedure TFmPortSerBal.Button3Click(Sender: TObject);
begin
  CkXonXof_input.Checked  := True;
  CkXonXof_output.Checked := True;
end;

procedure TFmPortSerBal.Button4Click(Sender: TObject);
begin
  RgRTS_input.ItemIndex   := 0;
  RgDTR_input.ItemIndex   := 0;
  CkCTS_output.Checked    := False;
  CkDSR_output.Checked    := False;
  CkXonXof_input.Checked  := False;
  CkXonXof_output.Checked := False;
end;

{
function TFmPortSerBal.ParityIndexOfText(Texto: String): Integer;
begin
  if Text = 'None'  then Result := 0 else
  if Text = 'Odd'   then Result := 1 else
  if Text = 'Even'  then Result := 2 else
  if Text = 'Mark'  then Result := 3 else
  if Text = 'Space' then Result := 4 else
                         Result := -1;
end;

function TFmPortSerBal.ParityTextOfIndex(Index: Integer): String;
begin
  case Index of
    0: Result := 'None';
    1: Result := 'Odd';
    2: Result := 'Even';
    3: Result := 'Mark';
    4: Result := 'Space';
    else Result := 'None';
  end;
end;

function TFmPortSerBal.DataBitsIndexOfVal(Val: Integer): Integer;
begin
  case Val of
    5: Result := 0;
    6: Result := 1;
    7: Result := 2;
    8: Result := 3;
    else Result := -1;
  end;
end;

function TFmPortSerBal.DataBitsValOfIndex(Index: Integer): Integer;
begin
  case Index of
    0: Result := 5;
    1: Result := 6;
    2: Result := 7;
    3: Result := 8;
    else Result := 8;
  end;
end;

function TFmPortSerBal.CommPortIndexOfText(Texto: String): Integer;
var
  Txt: String;
begin
  Txt := Copy(Texto, 4, Length(Texto));
  if Length(Txt) = 0 then Result := -1
  else Result := Geral.IMV(Txt)-1;
end;

function TFmPortSerBal.CommPortTextOfIndex(Index: Integer): String;
begin
  Result := 'COM' + IntToStr(Index+1);
end;}

procedure TFmPortSerBal.QrPortSerBalCalcFields(DataSet: TDataSet);
begin
  QrPortSerBalPARITY_NOME.Value   := CbParity.Items[QrPortSerBalParity.Value];
  QrPortSerBalCOMMPORT_NOME.Value := CbCommPort.Items[QrPortSerBalCommPort.Value];
  QrPortSerBalBAUDRATE_NOME.Value := CbBaudRate.Items[QrPortSerBalBaudRate.Value];
  QrPortSerBalSTOPBITS_NOME.Value := CbStopBits.Items[QrPortSerBalStopBits.Value];
  QrPortSerBalDATABITS_NOME.Value := CbDataBits.Items[QrPortSerBalDataBits.Value];
end;


end.

