object FmCOMSelPorta: TFmCOMSelPorta
  Left = 339
  Top = 185
  Caption = 'COM-PORTA-001 :: Sele'#231#227'o de Porta COM'
  ClientHeight = 536
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 628
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 580
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 532
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 280
        Height = 32
        Caption = 'Sele'#231#227'o de Porta COM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 280
        Height = 32
        Caption = 'Sele'#231#227'o de Porta COM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 280
        Height = 32
        Caption = 'Sele'#231#227'o de Porta COM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 628
    Height = 374
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 628
      Height = 374
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 628
        Height = 324
        Align = alClient
        Caption = ' Aparelhos dispon'#237'veis: '
        TabOrder = 0
        object Label1: TLabel
          Left = 16
          Top = 24
          Width = 104
          Height = 13
          Caption = 'Listagem A (ComPort):'
        end
        object Label2: TLabel
          Left = 16
          Top = 64
          Width = 98
          Height = 13
          Caption = 'Listagem B (Device):'
        end
        object LBDevices: TListBox
          Left = 16
          Top = 80
          Width = 585
          Height = 233
          ItemHeight = 13
          TabOrder = 0
          OnDblClick = LBDevicesDblClick
        end
        object nrDeviceBox1: TnrDeviceBox
          Left = 16
          Top = 40
          Width = 585
          Height = 21
          nrComm = nrComm1
          ResetOnChanged = False
          TabOrder = 1
          OnClick = nrDeviceBox1Click
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 324
        Width = 628
        Height = 50
        Align = alBottom
        Caption = ' Item Selecionado: '
        TabOrder = 1
        ExplicitTop = 280
        object EdDevice: TEdit
          Left = 8
          Top = 20
          Width = 605
          Height = 21
          TabOrder = 0
          OnChange = EdDeviceChange
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 422
    Width = 628
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 378
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 624
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 466
    Width = 628
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 422
    object PnSaiDesis: TPanel
      Left = 482
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 480
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object nrComm1: TnrComm
    Active = False
    BaudRate = 115200
    Parity = pNone
    StopBits = sbOne
    ByteSize = 8
    ComPortNo = 1
    ComPort = cpCOM1
    TraceStates = [tsRxChar, tsTxEmpty]
    EventChar = #0
    StreamProtocol = spHardware
    BufferInSize = 4096
    BufferOutSize = 4096
    TimeoutRead = 0
    TimeoutWrite = 100
    RS485Mode = False
    EnumPorts = epFullPresent
    UseMainThread = True
    TerminalUsage = tuBoth
    TerminalEcho = False
    Left = 144
    Top = 8
  end
end
