object FmGSM_Serial: TFmGSM_Serial
  Left = 230
  Top = 197
  Caption = 'GSM-TELEF-001 :: Envio de SMS'
  ClientHeight = 486
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label11: TLabel
      Left = 492
      Top = 32
      Width = 21
      Height = 13
      Caption = 'PIN:'
    end
    object SpeedButton1: TSpeedButton
      Left = 564
      Top = 28
      Width = 21
      Height = 21
      Caption = '?'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 588
      Top = 28
      Width = 23
      Height = 22
      OnClick = SpeedButton2Click
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 8
      Width = 269
      Height = 17
      Caption = 'Ativar o aparelho selecionado abaixo:'
      Enabled = False
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object nrDeviceBox1: TnrDeviceBox
      Left = 8
      Top = 28
      Width = 357
      Height = 21
      nrComm = nrComm1
      ResetOnChanged = False
      TabOrder = 1
      OnClick = nrDeviceBox1Click
    end
    object Button1: TButton
      Left = 374
      Top = 28
      Width = 33
      Height = 20
      Caption = '..'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button3: TButton
      Left = 413
      Top = 28
      Width = 72
      Height = 21
      Caption = 'Diagn'#243'stico'
      TabOrder = 3
      OnClick = Button3Click
    end
    object EdPIN: TdmkEdit
      Left = 514
      Top = 28
      Width = 47
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 4
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnDblClick = EdPINDblClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 105
    Width = 624
    Height = 381
    Align = alClient
    Caption = 'Panel3'
    TabOrder = 1
    ExplicitTop = 81
    ExplicitHeight = 405
    object Pages: TPageControl
      Left = 1
      Top = 1
      Width = 622
      Height = 243
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 267
      object TabSheet3: TTabSheet
        Caption = 'Info do aparelho'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object lvInfo: TListView
          Left = 0
          Top = 0
          Width = 614
          Height = 215
          Align = alClient
          Columns = <
            item
              Caption = 'Nome'
              Width = 100
            end
            item
              AutoSize = True
              Caption = 'Valor'
            end>
          GridLines = True
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Envio de SMS'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 239
        object Label3: TLabel
          Left = 13
          Top = 22
          Width = 75
          Height = 13
          Caption = 'Para o telefone:'
        end
        object Label7: TLabel
          Left = 0
          Top = 85
          Width = 614
          Height = 13
          Align = alBottom
          Caption = 'Escreva a mensagem SMS mesage aqui:'
          Color = clSkyBlue
          ParentColor = False
          ExplicitWidth = 194
        end
        object Label13: TLabel
          Left = 346
          Top = 53
          Width = 77
          Height = 13
          Caption = 'Porta de origem:'
        end
        object Label14: TLabel
          Left = 346
          Top = 84
          Width = 80
          Height = 13
          Caption = 'Porta de destino:'
        end
        object Edit1: TEdit
          Left = 92
          Top = 20
          Width = 97
          Height = 21
          TabOrder = 0
          Text = '+554499999999'
        end
        object Button2: TButton
          Left = 192
          Top = 18
          Width = 52
          Height = 25
          Caption = 'Envia'
          TabOrder = 1
          OnClick = Button2Click
        end
        object Memo1: TMemo
          Left = 0
          Top = 98
          Width = 614
          Height = 117
          Align = alBottom
          Lines.Strings = (
            'Texto do SMS')
          TabOrder = 2
          ExplicitTop = 122
        end
        object chConfirm: TCheckBox
          Left = 256
          Top = 21
          Width = 81
          Height = 17
          Caption = 'Confirmar'
          TabOrder = 3
        end
        object Button8: TButton
          Left = 92
          Top = 44
          Width = 153
          Height = 25
          Caption = 'Envia e espera Ref#'
          TabOrder = 4
          OnClick = Button8Click
        end
        object chAlert: TCheckBox
          Left = 256
          Top = 51
          Width = 73
          Height = 17
          Caption = 'Alertar'
          TabOrder = 5
        end
        object cbPort: TComboBox
          Left = 343
          Top = 19
          Width = 162
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 6
          Text = 'App Port : None'
          OnChange = cbPortChange
          Items.Strings = (
            'App Port : None'
            'App Port : 8 bit'
            'App Port : 16 bit')
        end
        object ePortSrc: TSpinEdit
          Left = 431
          Top = 49
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 7
          Value = 0
        end
        object ePortDest: TSpinEdit
          Left = 431
          Top = 81
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 8
          Value = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Armazenamento de SMS'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 239
        DesignSize = (
          614
          215)
        object Label4: TLabel
          Left = 13
          Top = 10
          Width = 43
          Height = 13
          Caption = 'Mem'#243'ria:'
        end
        object Label5: TLabel
          Left = 305
          Top = 10
          Width = 40
          Height = 13
          Caption = 'ID SMS:'
        end
        object Label6: TLabel
          Left = 0
          Top = 66
          Width = 26
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          Caption = 'SMS:'
          Color = clSkyBlue
          ParentColor = False
        end
        object Label10: TLabel
          Left = 104
          Top = 10
          Width = 38
          Height = 13
          Caption = 'Estatus:'
        end
        object eMem: TComboBox
          Left = 13
          Top = 27
          Width = 76
          Height = 21
          Style = csDropDownList
          TabOrder = 0
        end
        object eNo: TSpinEdit
          Left = 304
          Top = 28
          Width = 49
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
        end
        object Button4: TButton
          Left = 355
          Top = 28
          Width = 54
          Height = 21
          Caption = 'Ler'
          TabOrder = 2
          OnClick = Button4Click
        end
        object Button6: TButton
          Left = 191
          Top = 26
          Width = 102
          Height = 22
          Caption = 'Obtem lista de SMS'
          TabOrder = 3
          OnClick = Button6Click
        end
        object Memo2: TMemo
          Left = 0
          Top = 79
          Width = 617
          Height = 136
          Anchors = [akLeft, akTop, akRight, akBottom]
          Color = clInfoBk
          ScrollBars = ssBoth
          TabOrder = 4
        end
        object Button5: TButton
          Left = 411
          Top = 29
          Width = 54
          Height = 20
          Caption = 'Apagar'
          TabOrder = 5
          OnClick = Button5Click
        end
        object cbStatus: TComboBox
          Left = 101
          Top = 27
          Width = 76
          Height = 21
          Style = csDropDownList
          ItemIndex = 4
          TabOrder = 6
          Text = 'All'
          Items.Strings = (
            'UnRead'
            'Read'
            'UnSent'
            'Sent'
            'All')
        end
      end
      object pgTerminal: TTabSheet
        Caption = 'Terminal:'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Memo3: TMemo
          Left = 0
          Top = 0
          Width = 614
          Height = 215
          Align = alClient
          TabOrder = 0
          OnDblClick = Memo3DblClick
        end
      end
      object pgBook: TTabSheet
        Caption = 'Agenda do celular'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 239
        object Label8: TLabel
          Left = 0
          Top = 49
          Width = 614
          Height = 13
          Align = alTop
          Alignment = taRightJustify
          Caption = 'Pronto '
          ExplicitLeft = 580
          ExplicitWidth = 34
        end
        object lvPhones: TListView
          Left = 0
          Top = 62
          Width = 614
          Height = 153
          Align = alClient
          Columns = <
            item
              Caption = 'Nome'
              Width = 100
            end
            item
              AutoSize = True
              Caption = 'Valor'
            end>
          GridLines = True
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 614
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object ePhoneBookType: TComboBox
            Left = 8
            Top = 13
            Width = 137
            Height = 21
            Style = csDropDownList
            ItemIndex = 0
            TabOrder = 0
            Text = 'ME (device) memory'
            Items.Strings = (
              'ME (device) memory'
              'SIM memory'
              'Dialed Calls'
              'Received Calls'
              'Missed Calls')
          end
          object Button7: TButton
            Left = 160
            Top = 13
            Width = 53
            Height = 21
            Caption = 'Ler'
            TabOrder = 1
            OnClick = Button7Click
          end
        end
      end
      object pgOptions: TTabSheet
        Caption = 'Op'#231#245'es'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 239
        object Label2: TLabel
          Left = 13
          Top = 16
          Width = 98
          Height = 13
          Caption = 'Ao receber um SMS:'
        end
        object Label12: TLabel
          Left = 12
          Top = 48
          Width = 94
          Height = 13
          Caption = 'Comando AT direto:'
        end
        object cbOnNewSms: TComboBox
          Left = 145
          Top = 14
          Width = 184
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          OnChange = cbOnNewSmsChange
          Items.Strings = (
            'Notify only'
            'Skip Store and Show'
            'Read and Show')
        end
        object eCmd: TEdit
          Left = 144
          Top = 48
          Width = 185
          Height = 21
          TabOrder = 1
          Text = 'ATI<cr>'
        end
        object bSendCommand: TButton
          Left = 344
          Top = 48
          Width = 75
          Height = 25
          Caption = 'Envia'
          TabOrder = 2
          OnClick = bSendCommandClick
        end
      end
      object pgDriver: TTabSheet
        Caption = 'Driver'
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 239
        object Label9: TLabel
          Left = 7
          Top = 43
          Width = 58
          Height = 13
          Caption = 'Driver GSM:'
        end
        object chAutodetect: TCheckBox
          Left = 5
          Top = 12
          Width = 168
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Autodetec'#231#227'o de driver GSM:'
          TabOrder = 0
          OnClick = chAutodetectClick
        end
        object eDrivers: TComboBox
          Left = 136
          Top = 39
          Width = 185
          Height = 21
          Style = csDropDownList
          TabOrder = 1
        end
        object bLoadDriver: TButton
          Left = 136
          Top = 72
          Width = 75
          Height = 25
          Caption = 'Carrega'
          TabOrder = 2
          OnClick = bLoadDriverClick
        end
        object bDelAll: TButton
          Left = 224
          Top = 72
          Width = 75
          Height = 25
          Caption = 'Exclui todos'
          TabOrder = 3
          OnClick = bDelAllClick
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 244
      Width = 622
      Height = 136
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 268
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 77
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Log de eventos:'
        Color = clSkyBlue
        ParentColor = False
      end
      object ListBox1: TListBox
        Left = 0
        Top = 13
        Width = 622
        Height = 123
        Align = alClient
        Color = clInfoBk
        ItemHeight = 13
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 172
        Height = 32
        Caption = 'Envio de SMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 172
        Height = 32
        Caption = 'Envio de SMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 172
        Height = 32
        Caption = 'Envio de SMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object nrComm1: TnrComm
    Active = False
    BaudRate = 115200
    Parity = pNone
    StopBits = sbOne
    ByteSize = 8
    ComPortNo = 1
    ComPort = cpCOM1
    TraceStates = [tsRxChar, tsTxEmpty]
    EventChar = #0
    StreamProtocol = spHardware
    DataProcessor = nrGsm1
    BufferInSize = 4096
    BufferOutSize = 4096
    TimeoutRead = 0
    TimeoutWrite = 100
    RS485Mode = False
    EnumPorts = epFullPresent
    UseMainThread = True
    TerminalUsage = tuBoth
    TerminalEcho = False
    Left = 144
    Top = 8
  end
  object nrGsm1: TnrGsm
    Active = True
    Autodetect = True
    IgnoreInitErrors = True
    TimeOut = 10000
    Log = nrLogFile1
    NewSmsMode = nsmSkipSave
    PIN = '0000'
    OnDataSend = nrGsm1DataSend
    OnFatalError = nrGsm1FatalError
    OnSmsReceived = nrGsm1SmsReceived
    OnSmsSent = nrGsm1SmsSent
    OnSmsListItem = nrGsm1SmsListItem
    OnAfterInit = nrGsm1AfterInit
    OnRing = nrGsm1Ring
    OnGsmError = nrGsm1GsmError
    OnPhoneListProgress = nrGsm1PhoneListProgress
    OnPhoneListFinish = nrGsm1PhoneListFinish
    OnGsmChangeState = nrGsm1GsmChangeState
    OnUnsolicited = nrGsm1Unsolicited
    OnDiagnostic = nrGsm1Diagnostic
    Left = 192
    Top = 8
  end
  object nrLogFile1: TnrLogFile
    SizeLimit = 204800
    FileName = 'nrcommlib.log'
    DetailLevel = dlDebug
    Options = [loSafeMode, loAutoFileName, loSharedLogFile, loDate, loTime, loDebugOut]
    AnsiOnly = True
    Left = 264
    Top = 8
  end
end
