unit COMSelPorta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  AxSms_TLB, // TLB da ActiveXperts > http://activexperts.com/
  dmkImage, nrcommbox, nrclasses, nrcomm, UnDmkEnums;

type
  TFmCOMSelPorta = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LBDevices: TListBox;
    nrComm1: TnrComm;
    nrDeviceBox1: TnrDeviceBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    EdDevice: TEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LBDevicesDblClick(Sender: TObject);
    procedure nrDeviceBox1Click(Sender: TObject);
    procedure EdDeviceChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ListagemB();
  public
    { Public declarations }
    FResult: Boolean;
  end;

  var
  FmCOMSelPorta: TFmCOMSelPorta;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmCOMSelPorta.BtOKClick(Sender: TObject);
begin
  FResult := True;
  //
  Close;
end;

procedure TFmCOMSelPorta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCOMSelPorta.EdDeviceChange(Sender: TObject);
begin
  BtOK.Enabled := Trim(EdDevice.Text) <> '';
end;

procedure TFmCOMSelPorta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCOMSelPorta.FormCreate(Sender: TObject);
begin
  FResult := False;
  ImgTipo.SQLType := stLok;
  //
  ListagemB();
end;

procedure TFmCOMSelPorta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCOMSelPorta.LBDevicesDblClick(Sender: TObject);
begin
  EdDevice.Text := LBDevices.Items[LBDevices.ItemIndex];
end;

procedure TFmCOMSelPorta.ListagemB();
var
  objGsm: Gsm;
begin
  try
    objGsm := CoGsm.Create;
    //
  {
      strDeviceName := objGsm.FindFirstDevice();
      while objGsm.LastError = 0 do
      begin
        strMessage := strMessage + sLineBreak + '  ' +
          IntToStr(iIndex) + ': ' + strDeviceName;
        iIndex := iIndex + 1;
        SetLength(aDeviceNames, iIndex);
        aDeviceNames[iIndex - 1] := strDeviceName;
        strDeviceName := objGsm.FindNextDevice();
      end;
  }
    LBDevices.Items.Clear;
    LBDevices.Items.Add(objGsm.FindFirstDevice());
    while objGsm.LastError = 0 do
      LBDevices.Items.Add(objGsm.FindNextDevice());
    //


      // Add COM ports
  {
      strDeviceName := objGsm.FindFirstPort();
      while objGsm.LastError = 0 do
      begin
        strMessage := strMessage + sLineBreak + '  ' +
          IntToStr(iIndex) + ': ' + strDeviceName;
        iIndex := iIndex + 1;
        SetLength(aDeviceNames, iIndex);
        aDeviceNames[iIndex - 1] := strDeviceName;
        strDeviceName := objGsm.FindNextPort();
      end;
  }
    //LBDevices.Items.Clear;
    LBDevices.Items.Add(objGsm.FindFirstPort());
    while objGsm.LastError = 0 do
      LBDevices.Items.Add(objGsm.FindNextPort());
    //


    //
    objGsm.Close();
  except
    Geral.MB_Erro('ERRO na listagem "B"!');
    raise;
  end;
end;

procedure TFmCOMSelPorta.nrDeviceBox1Click(Sender: TObject);
begin
  EdDevice.Text := nrDeviceBox1.Text;
end;

end.
