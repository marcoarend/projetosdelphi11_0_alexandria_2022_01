object FmPortSerBal: TFmPortSerBal
  Left = 368
  Top = 194
  Caption = 'XXX-XXXXX-999 :: Cadastro de Balancas Comunicativas'
  ClientHeight = 348
  ClientWidth = 803
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 803
    Height = 300
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 251
      Width = 801
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 331
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 801
      Height = 212
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPortSerBal
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 68
        Top = 24
        Width = 725
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Descricao'
        DataSource = DsPortSerBal
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 52
        Width = 153
        Height = 148
        Caption = ' Configura'#231#245'es b'#225'sicas: '
        TabOrder = 2
        object Label11: TLabel
          Left = 8
          Top = 20
          Width = 110
          Height = 13
          Caption = 'Porta de comunica'#231#227'o:'
        end
        object Label12: TLabel
          Left = 8
          Top = 60
          Width = 82
          Height = 13
          Caption = 'Velocidade (bps):'
        end
        object Label13: TLabel
          Left = 8
          Top = 100
          Width = 45
          Height = 13
          Caption = 'Data bits:'
        end
        object Label17: TLabel
          Left = 60
          Top = 100
          Width = 45
          Height = 13
          Caption = 'Paridade:'
        end
        object Label18: TLabel
          Left = 96
          Top = 60
          Width = 44
          Height = 13
          Caption = 'Stop bits:'
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 36
          Width = 137
          Height = 21
          DataField = 'COMMPORT_NOME'
          DataSource = DsPortSerBal
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 8
          Top = 76
          Width = 85
          Height = 21
          DataField = 'BAUDRATE_NOME'
          DataSource = DsPortSerBal
          TabOrder = 1
        end
        object DBEdit4: TDBEdit
          Left = 96
          Top = 76
          Width = 49
          Height = 21
          DataField = 'STOPBITS_NOME'
          DataSource = DsPortSerBal
          TabOrder = 2
        end
        object DBEdit5: TDBEdit
          Left = 8
          Top = 116
          Width = 45
          Height = 21
          DataField = 'DATABITS_NOME'
          DataSource = DsPortSerBal
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 64
          Top = 252
          Width = 64
          Height = 21
          DataField = 'Parity'
          DataSource = DsPortSerBal
          TabOrder = 4
        end
        object DBEdit7: TDBEdit
          Left = 60
          Top = 116
          Width = 85
          Height = 21
          DataField = 'PARITY_NOME'
          DataSource = DsPortSerBal
          TabOrder = 5
        end
      end
      object GroupBox4: TGroupBox
        Left = 176
        Top = 52
        Width = 617
        Height = 148
        Caption = ' Controle de fluxo: '
        TabOrder = 3
        object DBRadioGroup1: TDBRadioGroup
          Left = 8
          Top = 32
          Width = 117
          Height = 100
          Caption = ' Entrada RTS: '
          DataField = 'RTS_input'
          DataSource = DsPortSerBal
          Items.Strings = (
            'Disable'
            'Enable'
            'Handshake'
            'Toggle')
          ParentBackground = True
          TabOrder = 0
          Values.Strings = (
            '0'
            '1'
            '2'
            '3')
        end
        object DBRadioGroup2: TDBRadioGroup
          Left = 132
          Top = 32
          Width = 117
          Height = 100
          Caption = ' Entrada DTR: '
          DataField = 'DTR_input'
          DataSource = DsPortSerBal
          Items.Strings = (
            'Disable'
            'Enable'
            'Handshake')
          ParentBackground = True
          TabOrder = 1
          Values.Strings = (
            '0'
            '1'
            '2')
        end
        object DBCheckBox1: TDBCheckBox
          Left = 256
          Top = 35
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle da sa'#237'da CTS.'
          DataField = 'CTS_output'
          DataSource = DsPortSerBal
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 256
          Top = 62
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle da sa'#237'da DSR.'
          DataField = 'DSR_output'
          DataSource = DsPortSerBal
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox3: TDBCheckBox
          Left = 256
          Top = 89
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle da sa'#237'da Xon/Xoff.'
          DataField = 'XonXof_output'
          DataSource = DsPortSerBal
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 256
          Top = 116
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle de entrada Xon/Xoff.'
          DataField = 'XonXof_input'
          DataSource = DsPortSerBal
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 803
    Height = 300
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 251
      Width = 801
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 692
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 801
      Height = 220
      Align = alTop
      TabOrder = 1
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDescricao: TdmkEdit
        Left = 68
        Top = 24
        Width = 725
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 52
        Width = 153
        Height = 148
        Caption = ' Configura'#231#245'es b'#225'sicas: '
        TabOrder = 2
        object Label3: TLabel
          Left = 8
          Top = 20
          Width = 110
          Height = 13
          Caption = 'Porta de comunica'#231#227'o:'
        end
        object Label4: TLabel
          Left = 8
          Top = 60
          Width = 82
          Height = 13
          Caption = 'Velocidade (bps):'
        end
        object Label6: TLabel
          Left = 8
          Top = 100
          Width = 45
          Height = 13
          Caption = 'Data bits:'
        end
        object Label7: TLabel
          Left = 60
          Top = 100
          Width = 45
          Height = 13
          Caption = 'Paridade:'
        end
        object Label8: TLabel
          Left = 96
          Top = 60
          Width = 44
          Height = 13
          Caption = 'Stop bits:'
        end
        object CbCommPort: TComboBox
          Left = 8
          Top = 36
          Width = 137
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          Items.Strings = (
            'COM1'
            'COM2'
            'COM3'
            'COM4'
            'COM5'
            'COM6'
            'COM7'
            'COM8'
            'COM9'
            'COM10'
            'COM11'
            'COM12'
            'COM13'
            'COM14'
            'COM15'
            'COM16'
            'COM17'
            'COM18'
            'COM19'
            'COM20'
            'COM21'
            'COM22'
            'COM23'
            'COM24'
            'COM25'
            'COM26'
            'COM27'
            'COM28'
            'COM29'
            'COM30'
            'COM31'
            'COM32'
            'COM33'
            'COM34'
            'COM35'
            'COM36'
            'COM37'
            'COM38'
            'COM39'
            'COM40'
            'COM41'
            'COM42'
            'COM43'
            'COM44'
            'COM45'
            'COM46'
            'COM47'
            'COM48'
            'COM49'
            'COM50'
            'COM51'
            'COM52'
            'COM53'
            'COM54'
            'COM55'
            'COM56'
            'COM57'
            'COM58'
            'COM59'
            'COM60'
            'COM61'
            'COM62'
            'COM63'
            'COM64'
            'COM65'
            'COM66'
            'COM67'
            'COM68'
            'COM69'
            'COM70'
            'COM71'
            'COM72'
            'COM73'
            'COM74'
            'COM75'
            'COM76'
            'COM77'
            'COM78'
            'COM79'
            'COM80'
            'COM81'
            'COM82'
            'COM83'
            'COM84'
            'COM85'
            'COM86'
            'COM87'
            'COM88'
            'COM89'
            'COM90'
            'COM91'
            'COM92'
            'COM93'
            'COM94'
            'COM95'
            'COM96'
            'COM97'
            'COM98'
            'COM99'
            'COM100'
            'COM101'
            'COM102'
            'COM103'
            'COM104'
            'COM105'
            'COM106'
            'COM107'
            'COM108'
            'COM109'
            'COM110'
            'COM111'
            'COM112'
            'COM113'
            'COM114'
            'COM115'
            'COM116'
            'COM117'
            'COM118'
            'COM119'
            'COM120'
            'COM121'
            'COM122'
            'COM123'
            'COM124'
            'COM125'
            'COM126'
            'COM127'
            'COM128'
            'COM129'
            'COM130'
            'COM131'
            'COM132'
            'COM133'
            'COM134'
            'COM135'
            'COM136'
            'COM137'
            'COM138'
            'COM139'
            'COM140'
            'COM141'
            'COM142'
            'COM143'
            'COM144'
            'COM145'
            'COM146'
            'COM147'
            'COM148'
            'COM149'
            'COM150'
            'COM151'
            'COM152'
            'COM153'
            'COM154'
            'COM155'
            'COM156'
            'COM157'
            'COM158'
            'COM159'
            'COM160'
            'COM161'
            'COM162'
            'COM163'
            'COM164'
            'COM165'
            'COM166'
            'COM167'
            'COM168'
            'COM169'
            'COM170'
            'COM171'
            'COM172'
            'COM173'
            'COM174'
            'COM175'
            'COM176'
            'COM177'
            'COM178'
            'COM179'
            'COM180'
            'COM181'
            'COM182'
            'COM183'
            'COM184'
            'COM185'
            'COM186'
            'COM187'
            'COM188'
            'COM189'
            'COM190'
            'COM191'
            'COM192'
            'COM193'
            'COM194'
            'COM195'
            'COM196'
            'COM197'
            'COM198'
            'COM199'
            'COM200'
            'COM201'
            'COM202'
            'COM203'
            'COM204'
            'COM205'
            'COM206'
            'COM207'
            'COM208'
            'COM209'
            'COM210'
            'COM211'
            'COM212'
            'COM213'
            'COM214'
            'COM215'
            'COM216'
            'COM217'
            'COM218'
            'COM219'
            'COM220'
            'COM221'
            'COM222'
            'COM223'
            'COM224'
            'COM225'
            'COM226'
            'COM227'
            'COM228'
            'COM229'
            'COM230'
            'COM231'
            'COM232'
            'COM233'
            'COM234'
            'COM235'
            'COM236'
            'COM237'
            'COM238'
            'COM239'
            'COM240'
            'COM241'
            'COM242'
            'COM243'
            'COM244'
            'COM245'
            'COM246'
            'COM247'
            'COM248'
            'COM249'
            'COM250'
            'COM251'
            'COM252'
            'COM253'
            'COM254'
            'COM255'
            'COM256')
        end
        object CbBaudRate: TComboBox
          Left = 8
          Top = 76
          Width = 85
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            '110'
            '300'
            '600'
            '1200'
            '2400'
            '4800'
            '9600'
            '14400'
            '19200'
            '38400'
            '56000'
            '57600'
            '115200')
        end
        object CbDataBits: TComboBox
          Left = 8
          Top = 116
          Width = 49
          Height = 21
          Style = csDropDownList
          TabOrder = 3
          Items.Strings = (
            '5'
            '6'
            '7'
            '8')
        end
        object CbParity: TComboBox
          Left = 60
          Top = 116
          Width = 85
          Height = 21
          Style = csDropDownList
          TabOrder = 4
          Items.Strings = (
            'None'
            'Odd'
            'Even'
            'Mark'
            'Space')
        end
        object CbStopBits: TComboBox
          Left = 96
          Top = 76
          Width = 49
          Height = 21
          Style = csDropDownList
          TabOrder = 2
          Items.Strings = (
            '1'
            '1.5'
            '2')
        end
      end
      object GroupBox2: TGroupBox
        Left = 176
        Top = 52
        Width = 617
        Height = 148
        Caption = ' Controle de fluxo: '
        TabOrder = 3
        object RgRTS_input: TRadioGroup
          Left = 8
          Top = 32
          Width = 117
          Height = 100
          Caption = ' Entrada RTS: '
          ItemIndex = 0
          Items.Strings = (
            'Disable'
            'Enable'
            'Handshake'
            'Toggle')
          TabOrder = 0
        end
        object RgDTR_input: TRadioGroup
          Left = 132
          Top = 32
          Width = 117
          Height = 100
          Caption = ' Entrada DTR: '
          ItemIndex = 1
          Items.Strings = (
            'Disable'
            'Enable'
            'Handshake')
          TabOrder = 1
        end
        object CkCTS_output: TCheckBox
          Left = 256
          Top = 35
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle da sa'#237'da CTS.'
          TabOrder = 2
        end
        object CkDSR_output: TCheckBox
          Left = 256
          Top = 62
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle da sa'#237'da DSR.'
          TabOrder = 3
        end
        object CkXonXof_input: TCheckBox
          Left = 256
          Top = 89
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle da sa'#237'da Xon/Xoff.'
          TabOrder = 4
        end
        object CkXonXof_output: TCheckBox
          Left = 256
          Top = 116
          Width = 210
          Height = 17
          Caption = 'Fluxo de controle de entrada Xon/Xoff.'
          TabOrder = 5
        end
        object Button1: TButton
          Left = 468
          Top = 12
          Width = 141
          Height = 32
          Caption = 'Hardware (RTS/CTS)'
          TabOrder = 6
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 468
          Top = 44
          Width = 141
          Height = 32
          Caption = 'Hardware (DTR/DSR)'
          TabOrder = 7
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 468
          Top = 76
          Width = 141
          Height = 32
          Caption = 'Software (Xon/Xoff)'
          TabOrder = 8
          OnClick = Button3Click
        end
        object Button4: TButton
          Left = 468
          Top = 108
          Width = 141
          Height = 32
          Caption = 'Limpa tudo'
          TabOrder = 9
          OnClick = Button4Click
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 803
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cadastro de Balan'#231'as Comunicativas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 720
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 719
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 494
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 492
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsPortSerBal: TDataSource
    DataSet = QrPortSerBal
    Left = 756
    Top = 25
  end
  object QrPortSerBal: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPortSerBalBeforeOpen
    AfterOpen = QrPortSerBalAfterOpen
    OnCalcFields = QrPortSerBalCalcFields
    SQL.Strings = (
      'SELECT * FROM portserbal'
      'WHERE Codigo > 0')
    Left = 728
    Top = 25
    object QrPortSerBalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPortSerBalDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPortSerBalCommPort: TSmallintField
      FieldName = 'CommPort'
    end
    object QrPortSerBalBaudRate: TSmallintField
      FieldName = 'BaudRate'
    end
    object QrPortSerBalStopBits: TSmallintField
      FieldName = 'StopBits'
    end
    object QrPortSerBalDataBits: TSmallintField
      FieldName = 'DataBits'
    end
    object QrPortSerBalParity: TSmallintField
      FieldName = 'Parity'
    end
    object QrPortSerBalRTS_input: TSmallintField
      FieldName = 'RTS_input'
    end
    object QrPortSerBalDTR_input: TSmallintField
      FieldName = 'DTR_input'
    end
    object QrPortSerBalCTS_output: TSmallintField
      FieldName = 'CTS_output'
    end
    object QrPortSerBalDSR_output: TSmallintField
      FieldName = 'DSR_output'
    end
    object QrPortSerBalXonXof_input: TSmallintField
      FieldName = 'XonXof_input'
    end
    object QrPortSerBalXonXof_output: TSmallintField
      FieldName = 'XonXof_output'
    end
    object QrPortSerBalLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPortSerBalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPortSerBalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPortSerBalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPortSerBalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPortSerBalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPortSerBalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPortSerBalPARITY_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PARITY_NOME'
      Calculated = True
    end
    object QrPortSerBalCOMMPORT_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMMPORT_NOME'
      Size = 10
      Calculated = True
    end
    object QrPortSerBalBAUDRATE_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BAUDRATE_NOME'
      Size = 10
      Calculated = True
    end
    object QrPortSerBalSTOPBITS_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STOPBITS_NOME'
      Size = 10
      Calculated = True
    end
    object QrPortSerBalDATABITS_NOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATABITS_NOME'
      Size = 10
      Calculated = True
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM portserbal'
      'WHERE CommPort=:P0'
      'AND Codigo<>:P1')
    Left = 321
    Top = 53
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
end
