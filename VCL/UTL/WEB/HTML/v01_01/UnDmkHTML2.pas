unit UnDmkHTML2;

interface

uses Vcl.OleCtrls, Vcl.ComCtrls, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Graphics,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, MSHTML,
  WBFuncs, SHDocVw, IdHTTP, IdMultipartFormData, IdSSLOpenSSL, UnDmkEnums,
  System.NetEncoding;

const
  IDM_DESFAZER = 43;
  IDM_JUSTIFYFULL = 50;
  IDM_CENTRALIZAR = 57;
  IDM_JUSTIFICAR = 58;
  IDM_ALINHARESQ = 59;
  IDM_ALINHADIR = 60;
  IDM_HYPERLINK = 2124;
  IDM_LINHAHORIZ = 2150;
  IDM_IMAGEM = 2168;
  IDM_MARCADOR = 2184;
  IDM_MARCADOR_LISTA = 2185;
  IDM_INDENT = 2186;
  IDM_OUTDENT = 2187;

type
  TtabLayout = (ttlLayout1);
  TexeComan  = (tecListMarcador, tecListNumeros, tecRecuoIn, tecRecuoOut,
    tecAlinEsq, tecAlinDir, tecAlinCen, tecAlinJus, tecLinha, tecLink,
    tecDesfazer, tecImagem);
  TtxtAtrib = (taBold, taItalic, taUnderline, taStrikeThrough, taSubscript,
    taSuperscript);
  TUnDmkHTML2 = class(TObject)
  private
    { Private declarations }
    function  GetIEHandle(WebBrowser: TWebbrowser; ClassName: string): HWND;
  public
    { Public declarations }
    // B R O W S E R
    procedure DocumentoEmBranco(WebBrowser: TWebBrowser; DesignMode: Boolean = False);
    procedure ExecutaComando(WebBrowser: TWebBrowser; Comando: TexeComan;
              BtAlinEsq: TToolButton = nil; BtAlinDir: TToolButton = nil;
              BtAlinCen: TToolButton = nil; BtAlinJus: TToolButton = nil);
    procedure SetAtributo(WebBrowser: TWebbrowser; Atributo: TtxtAtrib;
              BotaoSub: TToolButton = nil; BotaoSob: TToolButton = nil);
    //
    procedure Colar(WebBrowser: TWebBrowser);
    procedure Copiar(WebBrowser: TWebBrowser);
    procedure Recortar(WebBrowser: TWebBrowser);
    //
    procedure ImprimirPagina(WebBrowser: TWebBrowser);
    procedure VisualizarImpressao(WebBrowser: TWebBrowser);
    //
    procedure Novo(WebBrowser: TWebBrowser; DesignMode: Boolean = False);
    procedure Abrir(WebBrowser: TWebBrowser);
    procedure Salvar(WebBrowser: TWebBrowser);
    //
    procedure PropriedadesDaPagina(WebBrowser: TWebBrowser);
    procedure ConfigurarPagina(WebBrowser: TWebBrowser);
    //
    procedure LocalizarNaPagina(WebBrowser: TWebBrowser);
    //
    function  ObtemHTMLdeWebBrowser(WebBrowser: TWebBrowser): WideString;
    procedure ConfiguraTextoNoWebBrowser(WebBrowser: TWebBrowser;
              TextoHtml: WideString);
    //
    procedure FonteMudaTamanho(WebBrowser: TWebBrowser; ComboBox: TComboBox);
    procedure FonteMuda(WebBrowser: TWebBrowser; Fonte: String);
    procedure FonteCor(WebBrowser: TWebBrowser);
    //
    procedure InserirTabela(WebBrowser: TWebBrowser; Layout: TtabLayout; Colunas,
              Linhas: Integer; Listrado: Boolean = False);
    //
    function  ObtemHTMLByTag(WebBrowser: TWebBrowser; Tag: String;
              RemoveTag: Boolean): WideString;
    // M � T O D O S
    function  HttpGetUrl(URL: String; HeaderParams: array of THttpHeader;
              Parametros: array of THttpParam; var Resultado: String;
              TimeOut: Integer = 0; Debug: Boolean = False): Integer;
    function  HttpPostURL(URL: String; HeaderParams: array of THttpHeader;
              Parametros: array of THttpParam; var Resultado: String;
              TimeOut: Integer = 0; Debug: Boolean = False): Integer;
    function  HttpPutURL(URL: String; HeaderParams: array of THttpHeader;
              ParamVal: TStringStream; var Resultado: String;
              TimeOut: Integer = 0): Integer;
  end;

var
  dmkHTML2: TUndmkHTML2;
  HTMLDocumento: IHTMLDocument2;

implementation

uses dmkGeral;

{ TUndmkHTML }

procedure TUnDmkHTML2.Abrir(WebBrowser: TWebBrowser);
var
  Texto: TStringList;
  Dialog: TOpenDialog;
begin
  Dialog := TOpenDialog.Create(nil);
  try
    Dialog.Filter := 'P�ginas da WEB|*.htm; *.html|Imagens|*.jpg;*.jpeg;*.bmp|Todos Arquivos|*.*';
    //
    if Dialog.Execute then
    begin
      Texto  := TStringList.Create;
      try
        Texto.LoadFromFile(Dialog.FileName);
        ConfiguraTextoNoWebBrowser(WebBrowser, Texto.Text);
      finally
        Texto.Free;
      end;
   end else
      Exit;
  finally
    Dialog.Free;
  end;
end;

procedure TUnDmkHTML2.Colar(WebBrowser: TWebBrowser);
begin
  WB_paste(WebBrowser);
end;

procedure TUnDmkHTML2.ConfigurarPagina(WebBrowser: TWebBrowser);
begin
  WB_ShowPageSetup(WebBrowser);
end;

procedure TUnDmkHTML2.ConfiguraTextoNoWebBrowser(WebBrowser: TWebBrowser;
  TextoHtml: WideString);
var
  Doc: Variant;
begin
  Doc := WebBrowser.Document;
  Doc.Clear;
  Doc.Write(TextoHtml);
  Doc.Close;
end;

procedure TUnDmkHTML2.Copiar(WebBrowser: TWebBrowser);
begin
  WB_Copy(WebBrowser);
end;

procedure TUnDmkHTML2.DocumentoEmBranco(WebBrowser: TWebBrowser;
  DesignMode: Boolean = False);
begin
  WebBrowser.Navigate('about:blank');
  //
  if DesignMode = True then
    (WebBrowser.Document as IHTMLDocument2).designMode := 'On'
  else
    (WebBrowser.Document as IHTMLDocument2).designMode := 'Off';
end;

procedure TUnDmkHTML2.ExecutaComando(WebBrowser: TWebBrowser; Comando: TexeComan;
  BtAlinEsq, BtAlinDir, BtAlinCen, BtAlinJus: TToolButton);
var
  ComandoInt: Integer;
begin
  case Comando of
    tecListMarcador:
      ComandoInt := IDM_MARCADOR_LISTA;
    tecListNumeros:
      ComandoInt := IDM_MARCADOR;
    tecRecuoIn:
      ComandoInt := IDM_INDENT;
    tecRecuoOut:
      ComandoInt := IDM_OUTDENT;
    tecAlinEsq:
    begin
      ComandoInt := IDM_ALINHARESQ;
      //
      if (BtAlinEsq <> nil) and (BtAlinDir <> nil) and (BtAlinCen <> nil) and
        (BtAlinJus <> nil) then
      begin
        if BtAlinEsq.Down = True then
        begin
          BtAlinDir.Down := False;
          BtAlinCen.Down := False;
          BtAlinJus.Down := False;
        end;
      end;
    end;
    tecAlinDir:
    begin
      ComandoInt := IDM_ALINHADIR;
      //
      if (BtAlinEsq <> nil) and (BtAlinDir <> nil) and (BtAlinCen <> nil) and
        (BtAlinJus <> nil) then
      begin
        if BtAlinDir.Down = True then
        begin
          BtAlinEsq.Down := False;
          BtAlinCen.Down := False;
          BtAlinJus.Down := False;
        end;
      end;
    end;
    tecAlinCen:
    begin
      ComandoInt := IDM_CENTRALIZAR;
      //
      if (BtAlinEsq <> nil) and (BtAlinDir <> nil) and (BtAlinCen <> nil) and
        (BtAlinJus <> nil) then
      begin
        if BtAlinCen.Down = True then
        begin
          BtAlinEsq.Down := False;
          BtAlinDir.Down := False;
          BtAlinJus.Down := False;
        end;
      end;
    end;
    tecAlinJus:
    begin
      ComandoInt := IDM_JUSTIFYFULL;
      //
      if (BtAlinEsq <> nil) and (BtAlinDir <> nil) and (BtAlinCen <> nil) and
        (BtAlinJus <> nil) then
      begin
        if BtAlinJus.Down = True then
        begin
          BtAlinEsq.Down := False;
          BtAlinDir.Down := False;
          BtAlinCen.Down := False;
        end;
      end;
    end;
    tecLinha:
      ComandoInt := IDM_LINHAHORIZ;
    tecLink:
      ComandoInt := IDM_HYPERLINK;
    tecDesfazer:
      ComandoInt := IDM_DESFAZER;
    tecImagem:
      ComandoInt := IDM_IMAGEM;
  end;
  SendMessage(GetIEHandle(WebBrowser, 'Internet Explorer_Server'), WM_COMMAND,
    ComandoInt, 0);
end;

function TUndmkHTML2.GetIEHandle(WebBrowser: TWebbrowser;
  ClassName: string): HWND;
var
  hwndChild, hwndTmp: HWND;
  oleCtrl: TOleControl;
  szClass: array [0..255] of char;
begin
  oleCtrl := WebBrowser;
  hwndTmp := oleCtrl.Handle;
  //
  while (True) do
  begin
    hwndChild := GetWindow(hwndTmp, GW_CHILD);
    //
    GetClassName(hwndChild, szClass, SizeOf(szClass));
    //
    if (string(szClass) = ClassName) then
    begin
      Result := hwndChild;
      Exit;
    end;
    hwndTmp := hwndChild;
  end;
  Result := 0;
end;

function TUnDmkHTML2.HttpGetUrl(URL: String; HeaderParams: array of THttpHeader;
  Parametros: array of THttpParam; var Resultado: String; TimeOut: Integer;
  Debug: Boolean): Integer;
var
  HTTPClient: TIdHTTP;
  SSL: TIdSSLIOHandlerSocketOpenSSL;
  I, J: Integer;
  Res: TStringStream;
  HeaderStr, HeaderVal, ParamNome, ParamValor, Params, DebugStr: String;
begin
  Result := 0;
  //
  if Debug then
    DebugStr := 'M�todo: GET' + sLineBreak + 'URL: ' + URL + sLineBreak +
                'Par�metros: ' + sLineBreak;
  //
  HTTPClient := TIdHTTP.Create;
  //
  if TimeOut > 0 then
  begin
    HTTPClient.ConnectTimeout := TimeOut;
    HTTPClient.ReadTimeout    := TimeOut;
  end;
  //
  Res := TStringStream.Create('', TEncoding.UTF8);
  //
  try
    //Configura SSL
    if Pos('https', LowerCase(URL)) > 0 then
    begin
      SSL := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      //SSL.SSLOptions.Method      := sslvSSLv3;
      //SSL.SSLOptions.Mode        := sslmClient;
      //SSL.SSLOptions.SSLVersions := [sslvSSLv3];
      //
      HTTPClient.IOHandler := SSL;
    end;
    //Adiciona header
    if Length(HeaderParams) > 0 then
    begin
      for I := Low(HeaderParams) to High(HeaderParams) do
      begin
        HeaderStr := HeaderParams[I].Nome;
        HeaderVal := HeaderParams[I].Valor;
        //
        HTTPClient.Request.CustomHeaders.AddValue(HeaderStr, HeaderVal);
      end;
    end;
    //Adiciona par�metros
    if Length(Parametros) > 0 then
    begin
      Params := '?';
      J      := 0;
      //
      for I := Low(Parametros) to High(Parametros) do
      begin
        ParamNome  := Parametros[I].Nome;
        ParamValor := TNetEncoding.URL.Encode(Parametros[I].Valor);
        //
        if (J = 0) then
        begin
          Params := Params + ParamNome + '=' + ParamValor
        end else
          Params := Params + '&' + ParamNome + '=' + ParamValor;
        //
        J := J + 1;
      end;
    end else
      Params := '';
    //
    if Debug then
      Geral.MB_Aviso(DebugStr + Params);
    //
    try
      HTTPClient.Get(URL + Params, Res);
      //
      Resultado := Res.DataString;
      Result    := HTTPClient.ResponseCode;
    except
      on E: EIdHTTPProtocolException do
      begin
        Resultado := E.ErrorMessage;
        Result    := HTTPClient.ResponseCode;
      end;
    end;
  finally
    HTTPClient.Free;
    Res.Free;
  end;
end;

function TUndmkHTML2.HttpPostURL(URL: String; HeaderParams: array of THttpHeader;
  Parametros: array of THttpParam; var Resultado: String; TimeOut: Integer;
  Debug: Boolean): Integer;
var
  Params: TIdMultiPartFormDataStream;
  HTTPClient: TIdHTTP;
  SSL: TIdSSLIOHandlerSocketOpenSSL;
  I: Integer;
  Res: TStringStream;
  HeaderStr, HeaderVal, ParamNome, ParamValor, ParamACharse,
  ParamAContentType, DebugStr: String;
begin
  Result := 0;
  //
  HTTPClient := TIdHTTP.Create;
  //
  if Debug then
    DebugStr := 'M�todo: POST' + sLineBreak + 'URL: ' + URL + sLineBreak +
                'Par�metros: ' + sLineBreak;
  //
  if TimeOut > 0 then
  begin
    HTTPClient.ConnectTimeout := TimeOut;
    HTTPClient.ReadTimeout    := TimeOut;
  end;
  //
  Params := TIdMultiPartFormDataStream.Create;
  Res    := TStringStream.Create('', TEncoding.UTF8);
  //
  try
    //Configura SSL
    if Pos('https', LowerCase(URL)) > 0 then
    begin
      SSL := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      //SSL.SSLOptions.Method      := sslvSSLv3;
      //SSL.SSLOptions.Mode        := sslmClient;
      //SSL.SSLOptions.SSLVersions := [sslvSSLv3];
      //
      HTTPClient.IOHandler := SSL;
    end;
    //Adiciona header
    if Length(HeaderParams) > 0 then
    begin
      for I := Low(HeaderParams) to High(HeaderParams) do
      begin
        HeaderStr := HeaderParams[I].Nome;
        HeaderVal := HeaderParams[I].Valor;
        //
        HTTPClient.Request.CustomHeaders.AddValue(HeaderStr, HeaderVal);
      end;
    end;
    //Adiciona par�metros
    if Length(Parametros) > 0 then
    begin
      for I := Low(Parametros) to High(Parametros) do
      begin
        ParamNome         := Parametros[I].Nome;
        ParamValor        := Parametros[I].Valor;
        ParamACharse      := Parametros[I].ACharse;
        ParamAContentType := Parametros[I].AContentType;
        //
        if Debug then
          DebugStr := DebugStr + ParamNome + ': ' + ParamValor + sLineBreak;
        //
        if LowerCase(ParamAContentType) = 'text/xml' then
          Params.AddFormField(ParamNome, ParamValor, ParamACharse, ParamAContentType).ContentTransfer := '8bit'
        else
          Params.AddFormField(ParamNome, ParamValor, ParamACharse, ParamAContentType);
      end;
    end;
    //
    if Debug then
      Geral.MB_Aviso(DebugStr);
    //
    try
      HTTPClient.Post(URL, Params, Res);
      //
      Resultado := Res.DataString;
      Result    := HTTPClient.ResponseCode;
    except
      on E: EIdHTTPProtocolException do
      begin
        Resultado := E.ErrorMessage;
        Result    := HTTPClient.ResponseCode;
      end;
    end;
  finally
    HTTPClient.Free;
    Params.Free;
    Res.Free;
  end;
end;

function TUnDmkHTML2.HttpPutURL(URL: String; HeaderParams: array of THttpHeader;
  ParamVal: TStringStream; var Resultado: String; TimeOut: Integer): Integer;
var
  HTTPClient: TIdHTTP;
  HeaderStr, HeaderVal: String;
  I: Integer;
begin
  Resultado := '';
  //
  HTTPClient := TIdHTTP.Create;
  //
  if TimeOut > 0 then
  begin
    HTTPClient.ConnectTimeout := TimeOut;
    HTTPClient.ReadTimeout    := TimeOut;
  end;
  //
  try
    if HTTPClient.Connected then
      HTTPClient.Disconnect;
    //
    HTTPClient.Request.ContentType := 'application/x-www-form-urlencoded';
    //
    //Adiciona header
    if Length(HeaderParams) > 0 then
    begin
      for I := Low(HeaderParams) to High(HeaderParams) do
      begin
        HeaderStr := HeaderParams[I].Nome;
        HeaderVal := HeaderParams[I].Valor;
        //
        HTTPClient.Request.CustomHeaders.AddValue(HeaderStr, HeaderVal);
      end;
    end;
    //
    try
      Resultado := HTTPClient.Put(URL, ParamVal);
      Result    := HTTPClient.ResponseCode;
    except
      Result := 0;
    end;
  finally
    HTTPClient.Free;
  end;
end;

procedure TUnDmkHTML2.ImprimirPagina(WebBrowser: TWebBrowser);
begin
  WB_ShowPrintDialog(WebBrowser);
end;

procedure TUnDmkHTML2.InserirTabela(WebBrowser: TWebBrowser; Layout: TtabLayout;
  Colunas, Linhas: Integer; Listrado: Boolean = False);

  procedure Tabela_Layout1(var StyleTable, StyleTr, StyleTh, StyleTd,
    StyleListr: String);
  begin
    StyleTable := 'font-family: arial, sans-serif; border-collapse: collapse';
    StyleTr    := '';
    StyleTh    := 'border: 1px solid #dddddd; text-align: left; padding: 2px';
    StyleTd    := StyleTh;
    StyleListr := 'background-color: #dddddd';
  end;

var
  Sel: IHTMLSelectionObject;
  Range: IHTMLTxtRange;
  Doc: Variant;
  EstTab, EstTr, EstTh, EstTd, EstListr, Html: String;
  i, j: Integer;
begin
  if (Linhas > 0) and (Colunas > 0) then
  begin
    case Layout of
      ttlLayout1: Tabela_Layout1(EstTab, EstTr, EstTh, EstTd, EstListr);
    end;
    //Cria tabela
    Html := '<TABLE';
    if EstTab <> '' then
      Html := Html + ' STYLE="' + EstTab + '"';
    Html := Html + '>';
    //
    //Insere o cabe�alho
    Html := Html + '<TR';
    if EstTr <> '' then
      Html := Html + ' STYLE="' + EstTr + '"';
    Html := Html + '>';
    //
    for i := 1 to Colunas do
    begin
      Html := Html + '<TH';
      if EstTh <> '' then
        Html := Html + ' STYLE="' + EstTh + '"';
      Html := Html + '>Coluna ' + Geral.FF0(i) + '</TH>';
    end;
    Html := Html + '</TR>';
    for i := 1 to Linhas - 1 do
    begin
      //Insere itens
      Html := Html + '<TR';
      if EstTr <> '' then
        Html := Html + ' STYLE="' + EstTr + '"';
      Html := Html + '>';
      //
      //Inserir colunas na linha
      for j := 1 to Colunas do
      begin
        Html := Html + '<TD';
        if EstTd <> '' then
        begin
          if (Listrado = True) and (Odd(i) = True) and (EstListr <> '') then //Listrado, �mprar e poss�i formata��o
            Html := Html + ' STYLE="' + EstTd + ';' + EstListr + '"'
          else
            Html := Html + ' STYLE="' + EstTd + '"';
        end;
        Html := Html + '>&nbsp;</TD>';
      end;
      //
      Html := Html + '</TR>';
    end;
    Html := Html + '</TABLE>';
  end else
  begin
    Geral.MB_Aviso('O n�mero de colunas e linhas deve ser superior a zero!');
    Exit;
  end;
  HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
  Sel := HTMLDocumento.selection;
  if Assigned(Sel) then
  begin
    if (Sel.type_ = 'None') or (Sel.type_ = 'Text') then
    begin
      Range := Sel.createRange as IHTMLTxtRange;
      Range.pasteHTML(HTML);
    end;
  end;
end;

procedure TUnDmkHTML2.LocalizarNaPagina(WebBrowser: TWebBrowser);
begin
  WB_ShowFindDialog(WebBrowser);
end;

procedure TUnDmkHTML2.Novo(WebBrowser: TWebBrowser; DesignMode: Boolean);
begin
  DocumentoEmBranco(WebBrowser, DesignMode);
end;

procedure TUnDmkHTML2.FonteCor(WebBrowser: TWebBrowser);
var
  Cor: TColorDialog;
begin
  Cor := TColorDialog.Create(nil);
  try
    HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
    //
    if Cor.Execute then
      HTMLDocumento.execCommand('ForeColor', False, Cor.Color);
  finally
    Cor.Free;
  end;
end;

procedure TUnDmkHTML2.FonteMuda(WebBrowser: TWebBrowser; Fonte: String);
begin
  HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
  HTMLDocumento.execCommand('FontName', False, Fonte);
end;

procedure TUnDmkHTML2.FonteMudaTamanho(WebBrowser: TWebBrowser; ComboBox: TComboBox);
begin
  HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
  //
  case ComboBox.ItemIndex of
    0: HTMLDocumento.execCommand('FontSize', False, 1);
    1: HTMLDocumento.execCommand('FontSize', False, 2);
    2: HTMLDocumento.execCommand('FontSize', False, 3);
    3: HTMLDocumento.execCommand('FontSize', False, 5);
    4: HTMLDocumento.execCommand('FontSize', False, 6);
    5: HTMLDocumento.execCommand('FontSize', False, 7);
  end;
end;

function TUnDmkHTML2.ObtemHTMLByTag(WebBrowser: TWebBrowser; Tag: String;
  RemoveTag: Boolean): WideString;
var
  I: Integer;
  Doc: IHTMLDocument3;
  DocHtml: IHTMLElementCollection;
  Html: IHTMLElement;
  Res: WideString;
begin
  //AVISO => Usar somente o nome da TAG! Ex: body e n�o <body>
  Res     := '';
  Doc     := WebBrowser.Document as IHTMLDocument3;
  DocHtml := Doc.getElementsByTagName(Tag);
  //
  if DocHtml.length > 0 then
  begin
    for I := 0 to Pred(DocHtml.length) do
      Html := DocHtml.item(I, EmptyParam) as IHTMLElement;
    //
    Res := Html.outerHTML;
    //
    if RemoveTag = True then
    begin
      Res := StringReplace(Res, '<' + Tag + '>', '', [rfIgnoreCase]);
      Res := StringReplace(Res, '</' + Tag + '>', '', [rfIgnoreCase]);
    end;
  end;
  Result := Res;
end;

function TUnDmkHTML2.ObtemHTMLdeWebBrowser(WebBrowser: TWebBrowser): WideString;
var
  Elemento: IHTMLElement;
begin
  Result := '';
  if Assigned(WebBrowser.Document) then
  begin
     Elemento := (WebBrowser.Document as IHTMLDocument2).body;
     //
     while Elemento.parentElement <> nil do
     begin
       Elemento := Elemento.parentElement;
     end;
     Result := Elemento.outerHTML;
  end;
end;

procedure TUnDmkHTML2.PropriedadesDaPagina(WebBrowser: TWebBrowser);
begin
  WB_ShowPropertiesDialog(WebBrowser);
end;

procedure TUnDmkHTML2.Recortar(WebBrowser: TWebBrowser);
begin
  WB_Cut(WebBrowser);
end;

procedure TUnDmkHTML2.Salvar(WebBrowser: TWebBrowser);
begin
  WB_Save(WebBrowser);
end;

procedure TUnDmkHTML2.SetAtributo(WebBrowser: TWebbrowser; Atributo: TtxtAtrib;
  BotaoSub, BotaoSob: TToolButton);
var
  AtribStr: String;
begin
  case Atributo of
    taBold:
      AtribStr := 'Bold';
    taItalic:
      AtribStr := 'Italic';
    taUnderline:
      AtribStr := 'Underline';
    taStrikeThrough:
      AtribStr := 'StrikeThrough';
    taSubscript:
    begin
      AtribStr := 'Subscript';
      //
      if (BotaoSub <> nil) and (BotaoSob <> nil) then
      begin
        if BotaoSub.Down = True then
          BotaoSob.Down := False;
      end;
    end;
    taSuperscript:
    begin
      AtribStr := 'Superscript';
      //
      if (BotaoSub <> nil) and (BotaoSob <> nil) then
      begin
        if BotaoSob.Down = True then
          BotaoSub.Down := False;
      end;
    end;
  end;
  HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
  HTMLDocumento.execCommand(AtribStr, False, 0);
end;

procedure TUnDmkHTML2.VisualizarImpressao(WebBrowser: TWebBrowser);
begin
  WB_ShowPrintPreview(WebBrowser);
end;

end.
