unit WBEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls,ToolWin, OleCtrls, SHDocVw,mshtml,StdCtrls, ExtCtrls, XPMan,
  Buttons, Menus, (*embeddedwb,*) AppEvnts, ActiveX, DB, mySQLDbTables, dmkGeral,
  DBCtrls;

const
IDM_MARCADOR = 2184;
IDM_MARCADOR_LISTA = 2185;
IDM_OUTDENT = 2187;
IDM_INDENT = 2186;
IDM_ALINHARESQ = 59;
IDM_CENTRALIZAR = 57;
IDM_ALINHADIR = 60;
IDM_IMAGEM = 2168;
IDM_LINHAHORIZ = 2150;
IDM_RECORTAR = 16;
IDM_COPIAR = 15;
IDM_COLAR = 26;
IDM_HYPERLINK = 2124;
IDM_DESFAZER = 43;

type
  TFmWBEdit = class(TForm)
    cdColor: TColorDialog;
    Panel7: TPanel;
    Panel4: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Panel2: TPanel;
    btnNumList: TSpeedButton;
    btnBullet: TSpeedButton;
    btnDecreaseIndent: TSpeedButton;
    btnIncreaseIndent: TSpeedButton;
    Panel1: TPanel;
    btnAlignLeft: TSpeedButton;
    btnCenter: TSpeedButton;
    btnAlignRight: TSpeedButton;
    Panel3: TPanel;
    SpeedButton5: TSpeedButton;
    btnHR: TSpeedButton;
    Panel5: TPanel;
    Panel6: TPanel;
    btnCut: TSpeedButton;
    btnCopy: TSpeedButton;
    btnPaste: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    Panel8: TPanel;
    Label1: TLabel;
    ComboSize: TComboBox;
    combofont: TComboBox;
    Label3: TLabel;
    WebBrowser1: TWebBrowser;
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Abrir1: TMenuItem;
    Salvar1: TMenuItem;
    OpenDialog1: TOpenDialog;
    Novo1: TMenuItem;
    N1: TMenuItem;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ComboFontChange(Sender: TObject);
    procedure btnBoldClick(Sender: TObject);
    procedure btnItalicClick(Sender: TObject);
    procedure btnUnderlineClick(Sender: TObject);
    procedure btnColorClick(Sender: TObject);
    procedure btnNumListClick(Sender: TObject);
    procedure btnBulletClick(Sender: TObject);
    procedure btnDecreaseIndentClick(Sender: TObject);
    procedure btnIncreaseIndentClick(Sender: TObject);
    procedure BtnImageClick(Sender: TObject);
    procedure btnAlignLeftClick(Sender: TObject);
    procedure btnCenterClick(Sender: TObject);
    procedure btnAlignRightClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnCutClick(Sender: TObject);
    procedure btnPasteClick(Sender: TObject);
    procedure btnHRClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ComboSizeChange(Sender: TObject);
    procedure Abrir1Click(Sender: TObject);
    procedure Salvar1Click(Sender: TObject);
    procedure Novo1Click(Sender: TObject);

  private
    { Private declarations }

  public
    { Public declarations }
    FAtributesSize: Integer;
    FAtributesName: String;
    FSaveSit: Integer;
    FQuery: TmySQLQuery;
    FTabela: TmySQLTable;
    FDBMemo: TDBMemo;
    FMemoryStream: TMemoryStream;
    FAberto: Boolean;
    FCampo: TWideMemoField;
    //FMemo: T L M DMemo;
    FMemo: TMemo;
    //FTipoLoc: TTipoLoc;
    FvpCol, FvpRow: Integer;
  end;

var
  FmWBEdit: TFmWBEdit;
  HTMLDocumento:IHTMLDocument2;

implementation

{$R *.dfm}

function GetIEHandle(WebBrowser: TWebbrowser; ClassName: string): HWND;
var
  hwndChild, hwndTmp: HWND;
  oleCtrl: TOleControl;
  szClass: array [0..255] of char;
begin
  oleCtrl :=WebBrowser;
  hwndTmp := oleCtrl.Handle;
  while (true) do
  begin
    hwndChild := GetWindow(hwndTmp, GW_CHILD);
    GetClassName(hwndChild, szClass, SizeOf(szClass));
    if (string(szClass)=ClassName) then
    begin
      Result :=hwndChild;
      Exit;
    end;
    hwndTmp := hwndChild;
  end;
  Result := 0;
end;

procedure DocumentoEmBranco(WebBrowser: TWebBrowser);
begin
  WebBrowser.Navigate('about:blank');
end;

procedure TFmWBEdit.FormCreate(Sender: TObject);
begin
DocumentoEmBranco(webbrowser1);
//altera o modo design do webbrowse para ON
(WebBrowser1.Document as IHTMLDocument2).designMode := 'On';

combofont.Items:=screen.Fonts;
end;

procedure TFmWBEdit.ToolButton1Click(Sender: TObject);
begin
HTMLDocumento.execCommand('Bold', False, 0);
HTMLDocumento.execCommand('ForeColor', False, '#0000FF');
end;

procedure TFmWBEdit.ComboFontChange(Sender: TObject);
begin
HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
HTMLDocumento.execCommand('FontName', False,ComboFont.Text);
end;

procedure TFmWBEdit.btnBoldClick(Sender: TObject);
begin
HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
HTMLDocumento.execCommand('Bold', False,0);
end;

procedure TFmWBEdit.btnItalicClick(Sender: TObject);
begin
HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
HTMLDocumento.execCommand('Italic', False,0);
end;

procedure TFmWBEdit.btnUnderlineClick(Sender: TObject);
begin
HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
HTMLDocumento.execCommand('Underline', False,0);
end;

procedure TFmWBEdit.btnColorClick(Sender: TObject);
begin
HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
if cdColor.Execute then
HTMLDocumento.execCommand('ForeColor', False,cdColor.Color)
else
abort;
end;

procedure TFmWBEdit.btnNumListClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_MARCADOR, 0);
end;

procedure TFmWBEdit.btnBulletClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_MARCADOR_LISTA, 0);
end;

procedure TFmWBEdit.btnDecreaseIndentClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_OUTDENT, 0);
end;

procedure TFmWBEdit.btnIncreaseIndentClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_INDENT, 0);
end;

procedure TFmWBEdit.BtnImageClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_IMAGEM, 0);
end;

procedure TFmWBEdit.btnAlignLeftClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_ALINHARESQ, 0);
end;

procedure TFmWBEdit.btnCenterClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_CENTRALIZAR, 0);
end;

procedure TFmWBEdit.btnAlignRightClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_ALINHADIR, 0);
end;

procedure TFmWBEdit.btnCopyClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_COPIAR, 0);
end;

procedure TFmWBEdit.btnCutClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_RECORTAR, 0);
end;

procedure TFmWBEdit.btnPasteClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_COLAR, 0);
end;

procedure TFmWBEdit.btnHRClick(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_LINHAHORIZ, 0);
end;

procedure TFmWBEdit.SpeedButton6Click(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
 WM_COMMAND,IDM_HYPERLINK,0);
end;

procedure TFmWBEdit.SpeedButton7Click(Sender: TObject);
begin
SendMessage(GetIEHandle(webbrowser1, 'Internet Explorer_Server'),
WM_COMMAND,IDM_DESFAZER,0);
end;

procedure TFmWBEdit.ComboSizeChange(Sender: TObject);
begin
//altera o tamanho da fonte
HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
case Combosize.ItemIndex of
0: HTMLDocumento.execCommand('FontSize', False,1);
1: HTMLDocumento.execCommand('FontSize', False,2);
2: HTMLDocumento.execCommand('FontSize', False,3);
3: HTMLDocumento.execCommand('FontSize', False,5);
4: HTMLDocumento.execCommand('FontSize', False,6);
5: HTMLDocumento.execCommand('FontSize', False,7);
end;
end;

procedure TFmWBEdit.Abrir1Click(Sender: TObject);
begin
if OpenDialog1.Execute=true then
webbrowser1.Navigate(OpenDialog1.FileName)
else
abort;
end;

procedure TFmWBEdit.Salvar1Click(Sender: TObject);
const
  MyHTM = 'C:\_Teste_.htm';

  procedure SalvaHTML(Arquivo: String);
  var
    HTMLDocument: IHTMLDocument2;
    PersistFile: IPersistFile;
  begin
    HTMLDocument := WebBrowser1.Document as IHTMLDocument2;
    PersistFile  := HTMLDocument as IPersistFile;
    PersistFile.Save(StringToOleStr(Arquivo), System.True);
  end;
begin
  Screen.Cursor := crHourGlass;
  case FSaveSit of
    0:
    begin
      if SaveDialog1.Execute then
        SalvaHTML(SaveDialog1.FileName);
    end;
    1:
    begin
      if (FTabela <> nil) (*and (FDBMemo <> nil)*) then
      begin
        if FileExists(MyHTM) then
          DeleteFile(MyHTM);
        SalvaHTML(MyHTM);
        {
        FMemoryStream := TMemoryStream.Create;
        FMemoryStream.LoadFromFile(MyArq);
        FMemoryStream.Position := 0; // reset to the beginning of the stream
        }
        FQuery.Close;
        FQuery.SQL.Clear;
        FQuery.SQL.Add('UPDATE ' + FTabela.TableName);
        FQuery.SQL.Add('SET ' + FDBMemo.DataField + '=""');
        FQuery.ExecSQL;
        //
        FTabela.Refresh;
        FTabela.Edit;
        FDBMemo.Lines.LoadFromFile(MyHTM);
        FTabela.Post;
        FTabela.Refresh;
        //
        FMemoryStream.Free;
      end else Geral.MensagemBox('Tabela ou "Rich Edit" indefinido(s).',
      'Erro', MB_OK+MB_ICONERROR);
    end;
{
    2:
    begin
      if (FMemo <> nil) then
      begin
        FMemoryStream := TMemoryStream.Create;
        Editor.Lines.SaveToStream(FMemoryStream);
        FMemoryStream.Position := 0; // reset to the beginning of the stream
        FMemo.Lines.LoadFromStream(FMemoryStream);
        //
        FMemo.Tag := 1;
        FMemoryStream.Free;
      end else
        Geral.MB_Aviso('"Rich Edit" indefinido.');
    end;
}
    else
      Geral.MB_Aviso('O arquivo n�o pode ser salvo. Forma de salvamento indefinida!');
  end;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmWBEdit.Novo1Click(Sender: TObject);
begin
DocumentoEmBranco(webbrowser1);
end;

end.
