unit UnVCL_Sincro;

interface

uses System.Classes, System.SysUtils, Vcl.StdCtrls, mySQLDbTables;

type
  TUnVCL_Sincro = class(TObject)
  private
    { Private declarations }
    procedure GeraAviso(Tabela: String; LabelAviso1, LabelAviso2: TLabel);
    procedure LimpaAviso(LabelAviso1, LabelAviso2: TLabel);
  public
    { Public declarations }
    function  SincronizaTabelas(Database: TmySQLDatabase; Tabelas: Array of String;
              LabelAviso1, LabelAviso2: TLabel;  var Msg: String): Boolean;
  end;

var
  VCL_Sincro: TUnVCL_Sincro;

implementation

uses UnGrl_Sincro, UnDmkEnums, UnMyObjects;

{ TUnVCL_Sincro }

procedure TUnVCL_Sincro.GeraAviso(Tabela: String; LabelAviso1, LabelAviso2: TLabel);
begin
  if (LabelAviso1 <> nil) and (LabelAviso2 <> nil) then
  begin
    MyObjects.Informa2(LabelAviso1, LabelAviso2, True,
      'Atualizando tabela: "' + Tabela + '"!');
  end;
end;

procedure TUnVCL_Sincro.LimpaAviso(LabelAviso1, LabelAviso2: TLabel);
begin
  if (LabelAviso1 <> nil) and (LabelAviso2 <> nil) then
  begin
    MyObjects.Informa2(LabelAviso1, LabelAviso2, False,
      '..............................');
  end;
end;

function TUnVCL_Sincro.SincronizaTabelas(Database: TmySQLDatabase;
  Tabelas: array of String; LabelAviso1, LabelAviso2: TLabel;
  var Msg: String): Boolean;
var
  I: Integer;
  Tabela: String;
  QryUpd, QryAux: TmySQLQuery;
begin
  Result := False;
  Msg    := '';
  QryUpd := TmySQLQuery.Create(Database);
  QryAux := TmySQLQuery.Create(Database);
  try
    for I := Low(Tabelas) to High(Tabelas) do
    begin
      Tabela := LowerCase(Tabelas[I]);
      //
      if Tabela = 'agencab' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agencab', ['Codigo'], Msg);
      end else
      if Tabela = 'agendasits' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agendasits', ['Controle'], Msg);
      end else
      if Tabela = 'agendaslem' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agendaslem', ['Conta'], Msg);
      end else
      if Tabela = 'agendaspar' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agendaspar', ['Conta'], Msg);
      end else
      if Tabela = 'agendassta' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agendassta', ['Codigo'], Msg);
      end else
      if Tabela = 'agentarcab' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agentarcab', ['Codigo'], Msg);
      end else
      if Tabela = 'agentarits' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'agentarits', ['Controle'], Msg);
      end else
      if Tabela = 'helpcab' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'helpcab', ['Codigo'], Msg);
      end else
      if Tabela = 'helpfaq' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'helpfaq', ['Codigo'], Msg);
      end else
      if Tabela = 'helpfaqhis' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'helpfaqhis', ['Controle'], Msg);
      end else
      if Tabela = 'helptopic' then
      begin
        GeraAviso(Tabela, LabelAviso1, LabelAviso2);
        Result := Grl_Sincro.SincronizaTabela(stDesktop, Database, QryUpd,
                    QryAux, 'helptopic', ['Controle'], Msg);
      end else
      begin
        Msg    := 'A "' + Tabela + '" n�o est� implementada na fun��o: "TUnVCL_Sincro.SincronizaTabelas"';
        Result := False;
      end;
      //
      if Result = False then
        Exit;
    end;
  finally
    QryUpd.Free;
    QryAux.Free;
    //
    LimpaAviso(LabelAviso1, LabelAviso2);
  end;
end;

end.
