unit NFeLoad_Web;

interface

uses
  SHDocVw, WinInet, MSHTML, (*GifImage,*) UrlMon,
  //
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, OleCtrls, UnDmkEnums;

type
  TFmNFeLoad_Web = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    IEAddress1: TEdit;
    Panel8: TPanel;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Image1: TImage;
    Label3: TLabel;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edtChaveNFe: TEdit;
    edtCaptcha: TEdit;
    Panel11: TPanel;
    btnNovaConsulta: TButton;
    btnGerarXML: TButton;
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    ProgressBar1: TProgressBar;
    BitBtn1: TBitBtn;
    TabSheet4: TTabSheet;
    Button2: TButton;
    TabSheet5: TTabSheet;
    MeTeste: TMemo;
    WebBrowser1: TWebBrowser;
    Panel12: TPanel;
    WBXML: TWebBrowser;
    Memo2: TMemo;
    LaWarning1: TLabel;
    LaWarning2: TLabel;
    LaWarning3: TLabel;
    LaWarning4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure WebBrowser1ProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnNovaConsultaClick(Sender: TObject);
    procedure WebBrowser1DocumentComplete(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
  private
    { Private declarations }
    FJaNavegouPrimeiraVez: Boolean;
    //
    procedure PegarHTML();
    function  DownloadFile(SourceFile, DestFile: string): Boolean;
    function  StripHTML(S: string): string;
    procedure GeraXml();
    procedure NovaConsulta();
    procedure DeleteIECache();

  public
    { Public declarations }
    FPath: string;
  end;

  var
  FmNFeLoad_Web: TFmNFeLoad_Web;

implementation

uses UnMyObjects,
//{$IFNDef semNFe_ v 0 2 0 0} NFeBaixaXMLdaWeb_ 0 2 0 0, {$EndIF}
  Module, NFeXMLGerencia;

{$R *.DFM}

procedure TFmNFeLoad_Web.BitBtn1Click(Sender: TObject);
begin
  Memo2.Lines := MeTeste.Lines;
  GeraXml();
end;

procedure TFmNFeLoad_Web.btnNovaConsultaClick(Sender: TObject);
begin
  NovaConsulta();
end;

procedure TFmNFeLoad_Web.BtOKClick(Sender: TObject);
{
  procedure SavePageToStream();
  var
    Stm: TMemoryStream;
  begin
    Stm := TMemoryStream.Create;
    try
      //UpdateEditor();
      pctrlWB.Pages[1].Caption := 'Browser - Save To Stream';
      EmbeddedWB1.SaveToStream(Stm);
      Stm.Position := 0;
      RichEditWB1.Lines.LoadFromStream(Stm);
      RichEditWB1.DoHighlightHTML;
    finally
      Stm.Free;
    end;
  end;
}
begin
{
  SavePageToStream();
}
end;

procedure TFmNFeLoad_Web.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFeLoad_Web.Button1Click(Sender: TObject);
begin
  PegarHTML();
end;

procedure TFmNFeLoad_Web.Button2Click(Sender: TObject);
begin
  NovaConsulta();
end;

procedure TFmNFeLoad_Web.DeleteIECache;
var
  lpEntryInfo: PInternetCacheEntryInfo;
  hCacheDir: LongWord;
  dwEntrySize: LongWord;
begin { DeleteIECache }
  dwEntrySize := 0;

  FindFirstUrlCacheEntry(nil, TInternetCacheEntryInfo(nil^), dwEntrySize);

  GetMem(lpEntryInfo, dwEntrySize);

  if dwEntrySize>0 then
    lpEntryInfo^.dwStructSize := dwEntrySize;

  hCacheDir := FindFirstUrlCacheEntry(nil, lpEntryInfo^, dwEntrySize);

  if hCacheDir<>0 then
  begin
    repeat
      DeleteUrlCacheEntry(lpEntryInfo^.lpszSourceUrlName);
      FreeMem(lpEntryInfo, dwEntrySize);
      dwEntrySize := 0;
      FindNextUrlCacheEntry(hCacheDir, TInternetCacheEntryInfo(nil^), dwEntrySize);
      GetMem(lpEntryInfo, dwEntrySize);
      if dwEntrySize>0 then
        lpEntryInfo^.dwStructSize := dwEntrySize;
    until not FindNextUrlCacheEntry(hCacheDir, lpEntryInfo^, dwEntrySize)
  end; { hCacheDir<>0 }
  FreeMem(lpEntryInfo, dwEntrySize);

  FindCloseUrlCache(hCacheDir)
end; { DeleteIECache }

function TFmNFeLoad_Web.DownloadFile(SourceFile, DestFile: string): Boolean;
const BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array[1..BufferSize] of Byte;
  BufferLen: DWORD;
  f: File;
  sAppName: string;
begin
 sAppName := ExtractFileName(Application.ExeName);
 hSession := InternetOpen(PChar(sAppName),INTERNET_OPEN_TYPE_PRECONFIG,nil, nil, 0);
 try
   hURL := InternetOpenURL(hSession,PChar(SourceFile),nil,0,0,0);
   try
     AssignFile(f, DestFile);
     Rewrite(f,1);
     repeat
       InternetReadFile(hURL, @Buffer,SizeOf(Buffer), BufferLen);
       BlockWrite(f, Buffer, BufferLen)
     until BufferLen = 0;
     CloseFile(f);
     Result := True;
   finally
     InternetCloseHandle(hURL)
   end
 finally
   InternetCloseHandle(hSession)
 end;
end;

procedure TFmNFeLoad_Web.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  //
{
  if not FJaNavegouPrimeiraVez then
  begin
    FJaNavegouPrimeiraVez := True;
    //
    //UpdateControls;
    EmbeddedWB1.Go(Trim(IEAddress1.Text));
    FJaNavegouPrimeiraVez := True;
  end;
}
end;

procedure TFmNFeLoad_Web.FormCreate(Sender: TObject);
begin
  FJaNavegouPrimeiraVez := False;
  //NovaConsulta();
end;

procedure TFmNFeLoad_Web.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, TAlignment(taCenter), 2, 10, 20);
end;


procedure TFmNFeLoad_Web.GeraXml();
begin
  Geral.MB_Info('Deprecado! Solicite implementa��o � dermatek!');
(*
  {$IFNDef semNFe_v 0 2 0 0}
  FPath := UnNFeBaixaXMLdaWeb _ 0 2 0 0 .RecuperarXMLdaWeb(Memo2.Lines.Text);
  if (FPath <> '') and (FileExists(FPath)) then
  begin
    WBXML.Navigate(FPath);
    MessageBox(0,PChar('XML '+FPath+' gerado com sucesso!'),'Informa��o',
      MB_ICONINFORMATION+MB_TASKMODAL);
    btnNovaConsulta.Enabled := True;
    //btnPegarHTML.Enabled    := True;
  end;
  {$EndIF}
*)
end;

procedure TFmNFeLoad_Web.Label3Click(Sender: TObject);
begin
  NovaConsulta();
end;

procedure TFmNFeLoad_Web.NovaConsulta();
begin
{ Gera erro as vezes
  WBXML.Navigate('about:blank');
  WebBrowser1.Navigate('about:blank');
}
  //
  btnNovaConsulta.Enabled := False;
  btnGerarXML.Enabled     := False;
  edtcaptcha.Text:='';
  Button1.Enabled         := True;
  DeleteIECache;
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  WebBrowser1.Navigate(IEAddress1.Text);//'http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=');
end;

procedure TFmNFeLoad_Web.PegarHTML();
begin
{ Gera erro as vezes
  WBXML.Navigate('about:blank');
  WebBrowser1.Navigate('about:blank');
}
  //
  edtChaveNFe.Text := Geral.SoNumero_TT(edtChaveNFe.Text);

  if not NFeXMLGeren.VerificaChaveAcesso(edtChaveNFe.Text, False) then
     Exit;

  if MyObjects.FIC(Trim(edtCaptcha.Text) = '', edtCaptcha,
  'Digite o texto da imagem.') then
     Exit;

  Memo1.Lines.Clear;
  Memo2.Lines.Clear;

  Button1.Enabled         := False;
  //btnPegarHTML.Enabled    := False;
  btnNovaConsulta.Enabled := False;
  btnGerarXML.Enabled     := False;
  try
     WebBrowser1.OleObject.Document.all.Item('ctl00$ContentPlaceHolder1$txtChaveAcessoCompleta', 0).value := edtChaveNFe.Text;
     WebBrowser1.OleObject.Document.all.Item('ctl00$ContentPlaceHolder1$txtCaptcha', 0).value := edtCaptcha.Text;
     WebBrowser1.OleObject.Document.all.Item('ctl00$ContentPlaceHolder1$btnConsultar', 0).click;
  except
     btnNovaConsulta.Enabled := True;
     raise;
  end;
  PageControl1.ActivePageIndex := 0;
end;

function TFmNFeLoad_Web.StripHTML(S: string): string;
var
  TagBegin, TagEnd, TagLength: integer;
begin
  TagBegin := Pos( '<', S);      // search position of first <

  while (TagBegin > 0) do begin  // while there is a < in S
    TagEnd    := Pos('>', S);              // find the matching >
    TagLength := TagEnd - TagBegin + 1;
    Delete(S, TagBegin, TagLength);     // delete the tag
    TagBegin := Pos( '<', S);            // search for next <
  end;

  Result := S;                   // give the result
end;

procedure TFmNFeLoad_Web.WebBrowser1DocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
var
  k, i: Integer;
  Source, dest: string;
  textoNFe : IHTMLDocument2;
begin
  Application.ProcessMessages;
  if WebBrowser1.LocationURL = 'http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8=' then
  begin
    for k := 0 to WebBrowser1.OleObject.Document.Images.Length - 1 do
     begin
       Source := WebBrowser1.OleObject.Document.Images.Item(k).Src;
       if (Source = 'http://www.nfe.fazenda.gov.br/scripts/srf/intercepta/captcha.aspx?opt=image') then
       begin
         dest := ExtractFilePath(ParamStr(0)) + 'captcha.gif';
         DownloadFile(Source, dest);
       end;
     end;
     Image1.Picture.LoadFromFile(dest);
     //btnPegarHTML.Enabled := True;
  end
  else if WebBrowser1.LocationURL = 'https://www.nfe.fazenda.gov.br/portal/visualizacaoNFe/completa/Default.aspx' then
  begin
    WebBrowser1.Navigate('https://www.nfe.fazenda.gov.br/PORTAL/visualizacaoNFe/completa/impressao.aspx');
  end
  else if WebBrowser1.LocationURL = 'http://www.nfe.fazenda.gov.br/portal/consultaCompleta.aspx?tipoConteudo=XbSeqxE8pl8=' then
  begin
    textoNFe := WebBrowser1.Document as IHTMLDocument2;
    repeat
       Application.ProcessMessages;
    until Assigned(textoNFe.body);
    Memo1.Lines.Text := textoNFe.body.innerHTML;
    Memo2.Lines.Text := StripHTML(textoNFe.body.innerHTML);
    Memo2.Lines.Text := StringReplace(Memo2.Lines.Text,'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);

    i := 0;
    while i < memo2.Lines.Count-1 do
    begin
      if trim(Memo2.Lines[i]) = '' then
      begin
        Memo2.Lines.Delete(i);
        i := i - 1;
      end;
      if pos('function',Memo2.lines[i])>0 then
      begin
        Memo2.Lines.Delete(i);
        i := i - 1;
      end;
      if pos('document',Memo2.lines[i])>0 then
      begin
        Memo2.Lines.Delete(i);
        i := i - 1;
      end;
      if pos('{',Memo2.lines[i])>0 then
      begin
        Memo2.Lines.Delete(i);
        i := i - 1;
      end;
      if pos('}',Memo2.lines[i])>0 then
      begin
        Memo2.Lines.Delete(i);
        i := i - 1;
      end;

      i := i + 1;
    end;
    Image1.Picture      := nil;
    btnGerarXML.Enabled := True;
    GeraXml;
  end
  else if WebBrowser1.LocationURL = 'https://www.nfe.fazenda.gov.br/portal/inexistente_completa.aspx' then
  begin
    MessageDlg('NF-e INEXISTENTE na base nacional, favor consultar esta NF-e no site da SEFAZ de origem.',mtError,[mbok],0);
    Image1.Picture          := nil;
    btnGerarXML.Enabled     := True;
    btnNovaConsulta.Enabled := True;
  end
  else
  begin
    MessageDlg('Erro carregando URL: '+WebBrowser1.LocationURL,mtError,[mbok],0);
    Image1.Picture          := nil;
    btnGerarXML.Enabled     := True;
    btnNovaConsulta.Enabled := True;
  end;
end;

procedure TFmNFeLoad_Web.WebBrowser1ProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
 if ProgressMax = 0 then
  begin
    ProgressBar1.Visible := False;
    //lblStatus.Visible    := False;
    exit;
  end
 else
  begin
    ProgressBar1.Visible := True;
    //lblStatus.Visible    := True;
    try
       ProgressBar1.Max := ProgressMax;
       if (Progress <> -1) and (Progress <= ProgressMax) then
          ProgressBar1.Position := Progress
       else
        begin
          ProgressBar1.Visible := False;
          //lblStatus.Visible    := False;
        end;
    except
       on EDivByZero do
         exit;
    end;
  end;
end;

end.
