object FmNFeLoad_Web: TFmNFeLoad_Web
  Left = 339
  Top = 185
  Caption = 'NFe-LOADW-001 :: Importa'#231#227'o de NF-e da WEB'
  ClientHeight = 691
  ClientWidth = 907
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 907
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 859
      Top = 0
      Width = 48
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 812
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 327
        Height = 31
        Caption = 'Importa'#231#227'o de NF-e da Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 327
        Height = 31
        Caption = 'Importa'#231#227'o de NF-e da Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 327
        Height = 31
        Caption = 'Importa'#231#227'o de NF-e da Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 622
    Width = 907
    Height = 69
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 763
      Top = 15
      Width = 142
      Height = 52
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 761
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 907
    Height = 512
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 907
      Height = 512
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 907
        Height = 512
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 903
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 8
            Top = 0
            Width = 865
            Height = 24
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object IEAddress1: TEdit
              Left = 0
              Top = 0
              Width = 865
              Height = 21
              Align = alTop
              TabOrder = 0
              Text = 
                'http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=' +
                'completa&tipoConteudo=XbSeqxE8pl8='
            end
          end
          object Panel7: TPanel
            Left = 873
            Top = 0
            Width = 30
            Height = 24
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object SpeedButton1: TSpeedButton
              Left = 0
              Top = 0
              Width = 21
              Height = 21
              Caption = '...'
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 8
            Height = 24
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 39
          Width = 903
          Height = 184
          Align = alTop
          Caption = ' Dados para pesquisa: '
          TabOrder = 1
          object GroupBox3: TGroupBox
            Left = 611
            Top = 15
            Width = 290
            Height = 167
            Align = alRight
            Caption = ' Imagem anti loop: '
            TabOrder = 0
            object Panel9: TPanel
              Left = 2
              Top = 15
              Width = 286
              Height = 150
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Image1: TImage
                Left = 21
                Top = 11
                Width = 245
                Height = 97
                Center = True
              end
              object Label3: TLabel
                Left = 1
                Top = 120
                Width = 286
                Height = 25
                Cursor = crHandPoint
                Alignment = taCenter
                AutoSize = False
                Caption = 'Clique aqui caso n'#227'o consiga visualizar a imagem'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -8
                Font.Name = 'Verdana'
                Font.Style = [fsUnderline]
                ParentFont = False
                OnClick = Label3Click
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 2
            Top = 15
            Width = 609
            Height = 167
            Align = alClient
            Caption = 'Dados NF-e'
            TabOrder = 1
            object Panel10: TPanel
              Left = 2
              Top = 15
              Width = 605
              Height = 58
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 8
                Width = 155
                Height = 13
                Caption = 'Chave de acesso da nota fiscal: '
              end
              object Label2: TLabel
                Left = 334
                Top = 9
                Width = 169
                Height = 13
                Caption = 'Digite o c'#243'digo da imagem ao lado: '
              end
              object edtChaveNFe: TEdit
                Left = 8
                Top = 24
                Width = 324
                Height = 21
                TabOrder = 0
                Text = '15110604333952000188550010000017211109600237'
              end
              object edtCaptcha: TEdit
                Left = 334
                Top = 24
                Width = 72
                Height = 21
                TabOrder = 1
              end
            end
            object Panel11: TPanel
              Left = 2
              Top = 73
              Width = 605
              Height = 92
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object LaWarning2: TLabel
                Left = 9
                Top = 50
                Width = 500
                Height = 16
                Caption = 'OS ARQUIVOS AQUI GERADOS N'#195'O SUBSTITUEM O XML ORIGINAL DA NF-E!'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaWarning1: TLabel
                Left = 8
                Top = 48
                Width = 500
                Height = 16
                Caption = 'OS ARQUIVOS AQUI GERADOS N'#195'O SUBSTITUEM O XML ORIGINAL DA NF-E!'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaWarning3: TLabel
                Left = 9
                Top = 69
                Width = 655
                Height = 16
                Caption = 
                  'Solicite a cada fornecedor o envio dos xmls originais, pois '#233' ob' +
                  'rigat'#243'rio guard'#225'-los conforme legisla'#231#227'o!'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object LaWarning4: TLabel
                Left = 8
                Top = 68
                Width = 655
                Height = 16
                Caption = 
                  'Solicite a cada fornecedor o envio dos xmls originais, pois '#233' ob' +
                  'rigat'#243'rio guard'#225'-los conforme legisla'#231#227'o!'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -14
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object btnNovaConsulta: TButton
                Left = 189
                Top = 14
                Width = 75
                Height = 26
                Caption = 'Nova Consulta'
                Enabled = False
                TabOrder = 0
                OnClick = btnNovaConsultaClick
              end
              object btnGerarXML: TButton
                Left = 266
                Top = 14
                Width = 75
                Height = 26
                Caption = 'Gerar XML'
                Enabled = False
                TabOrder = 1
              end
              object Button1: TButton
                Left = 4
                Top = 14
                Width = 74
                Height = 26
                Caption = 'Salvar XML'
                TabOrder = 2
                OnClick = Button1Click
              end
              object BitBtn1: TBitBtn
                Left = 524
                Top = 12
                Width = 74
                Height = 25
                Caption = 'Ler'
                TabOrder = 3
                OnClick = BitBtn1Click
              end
              object Button2: TButton
                Left = 449
                Top = 12
                Width = 74
                Height = 25
                Caption = 'Conectar'
                TabOrder = 4
                OnClick = Button2Click
              end
            end
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 223
          Width = 903
          Height = 287
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 2
          object TabSheet1: TTabSheet
            Caption = 'Dados HTML'
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 895
              Height = 259
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Dados TXT'
            ImageIndex = 1
            object Memo2: TMemo
              Left = 0
              Top = 0
              Width = 898
              Height = 264
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' Dados XML "Recuperado"'
            ImageIndex = 2
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 895
              Height = 259
              Align = alClient
              TabOrder = 0
              object WBXML: TWebBrowser
                Left = 1
                Top = 1
                Width = 893
                Height = 257
                Align = alClient
                TabOrder = 0
                ExplicitWidth = 896
                ExplicitHeight = 262
                ControlData = {
                  4C0000004B5C0000901A00000000000000000000000000000000000000000000
                  000000004C000000000000000000000001000000E0D057007335CF11AE690800
                  2B2E126208000000000000004C0000000114020000000000C000000000000046
                  8000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000100000000000000000000000000000000000000}
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Navega'#231#227'o s'#237'tio Receita'
            ImageIndex = 3
            object WebBrowser1: TWebBrowser
              Left = 0
              Top = 0
              Width = 895
              Height = 259
              Align = alClient
              TabOrder = 0
              OnProgressChange = WebBrowser1ProgressChange
              OnDocumentComplete = WebBrowser1DocumentComplete
              ExplicitWidth = 898
              ExplicitHeight = 264
              ControlData = {
                4C000000805C0000C51A00000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E12620A000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' Teste Dermatek '
            ImageIndex = 4
            object MeTeste: TMemo
              Left = 0
              Top = 0
              Width = 895
              Height = 259
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Courier New'
              Font.Style = []
              Lines.Strings = (
                '//'
                '//'
                '//'
                
                  'Conhe'#231'a a NF-e | Servi'#231'os | Legisla'#231#227'o | Documentos | Downloads ' +
                  '| '#193'rea Restrita '
                'Servi'#231'os'
                'Consultar Resumo da NF-e'
                'Consultar NF-e Completa'
                'Consultar Inutiliza'#231#227'o'
                'Consultar Disponibilidade'
                'Consultar DPEC'
                'Realizar Upload DPEC'
                'Rela'#231#227'o de Servi'#231'os Web'
                'Agendamento SCAN'
                'Legisla'#231#227'o'
                'Ajustes SINIEF'
                'Atos COTEPE'
                'Conv'#234'nios'
                'Protocolos'
                'Documentos'
                'Manuais '
                'Esquemas XML'
                'Notas T'#233'cnicas'
                'Diversos'
                'Downloads'
                'Visualizador de DF-e'
                'Assinador'
                'Emissor de NF-e'
                'Estat'#237'sticas da NF-eNF-e Autorizadas '
                '3,262 bilh'#245'es N'#250'mero de Emissores '
                '666,381 mil ... saiba mais '
                #193'rea Restrita '
                'Central NF-e 0800 9782338'
                'Perguntas Frequentes '
                'Portais e Secretarias'
                
                  'Portais Estaduais da NF-e SelecioneAcreAlagoasAmazonasBahiaCear'#225 +
                  'Distrito FederalEspirito SantoGoi'#225'sMaranh'#227'oMato GrossoMato Gross' +
                  'o do SulMinas GeraisPar'#225'Para'#237'baParan'#225'PernambucoPiau'#237'Rio de Janei' +
                  'roRio Grande do NorteRio Grande do SulRond'#244'niaSanta CatarinaS'#227'o ' +
                  'PauloSergipeTocantins '
                
                  'Secretarias de Fazenda SelecioneAcreAlagoasAmap'#225'AmazonasBahiaCea' +
                  'r'#225'Distrito FederalEspirito SantoGoi'#225'sMaranh'#227'oMato GrossoMato Gro' +
                  'sso do SulMinas GeraisPar'#225'Para'#237'baParan'#225'PernambucoPiau'#237'Rio de Jan' +
                  'eiroRio Grande do NorteRio Grande do SulRond'#244'niaRoraimaSanta Cat' +
                  'arinaS'#227'o PauloSergipeTocantins  '
                'Voc'#234' est'#225' aqui: '
                'P'#225'gina Principal &gt; Servi'#231'os &gt; Consultar NF-e Completa '
                'Consultar NF-e Completa '
                
                  'Chave de Acesso15-1105-04.333.952/0001-88-55-001-000.001.612-157' +
                  '.312.525-0 '
                'N'#250'mero NF-e1612 '
                'Vers'#227'o2.00 '
                
                  'NFeEmitenteDestinat'#225'rioProdutos/Servi'#231'osTotaisCom'#233'rcio ExteriorT' +
                  'ransporteCobran'#231'aInf. AdicionaisAvulsa '
                'Dados da NF-e '
                'N'#250'mero1612'
                'S'#233'rie1'
                'Data de emiss'#227'o19/05/2011'
                'Valor Total da Nota Fiscal40.310,64'
                'EMITENTE '
                'CNPJ04.333.952/0001-88'
                'Nome/Raz'#227'o SocialM.J. NOVAES DE LIMA &amp;amp; CIA LTDA'
                'Inscri'#231#227'o Estadual152190414'
                'UFPA'
                'DESTINAT'#193'RIO '
                'CNPJ00.000.000/0000-00'
                'Nome/Raz'#227'o SocialVISHNU CHEMICAL LTD'
                'Inscri'#231#227'o Estadual'
                'UFEX'
                'EMISS'#195'O '
                'Processo0 - Aplicativo do Contribuinte '
                'Vers'#227'o do ProcessoBDER_1105071652'
                'Tipo de Emiss'#227'o1 - Normal '
                'Finalidade1 - Normal '
                
                  'Natureza da Opera'#231#227'oOUTRA ENTRADA DE MERCADORIA OU PRESTACAO DE ' +
                  'SERVICO NAO ESPE'
                'Tipo da Opera'#231#227'o0-Entrada '
                'Forma de Pagamento0 - '#192' vista '
                'Digest Value da NF-ehrqFNvKwsYd5tZIMJhRHwt/re6Q='
                'SITUA'#199#195'O ATUAL: Autorizada '
                'Ocorr'#234'nciaAutorizada '
                'Protocolo415110007792534'
                'Data/Hora19/05/2011 17:19:05'
                'Recebimento no Amb. Nacional19/05/2011 17:19:05'
                'Dados do Emitente'
                'Nome / Raz'#227'o SocialM.J. NOVAES DE LIMA &amp;amp; CIA LTDA'
                'CNPJ04.333.952/0001-88'
                'Endere'#231'oESTRADA DO OUTEIRO, S/N, LOTE 08 QUADRA 8'
                'Bairro/DistritoDISTRITO INDUSTRIAL DE ICOARAC'
                'CEP66813-250'
                'Munic'#237'pio1501402- BELEM'
                'Fone/Fax(91)3211-7800'
                'UFPA'
                'Pa'#237's1058 - BRASIL'
                'Inscri'#231#227'o Estadual152190414'
                'Munic'#237'pio da Ocorr'#234'ncia do Fato Gerador do ICMS 1501402'
                'C'#243'digo de Regime Tribut'#225'rio3 - Regime Normal '
                'Dados do Remetente '
                'Nome / Raz'#227'o socialVISHNU CHEMICAL LTD'
                'CNPJ00.000.000/0000-00'
                'Endere'#231'oVIA VUVARNA HOUSE - SOMAJIGUDA, IN, S/N, '
                'Bairro / DistritoSANGEET NAGAR COLONY'
                'CEP00000-000'
                'Munic'#237'pio9999999- EXTERIOR'
                'UFEX'
                'Pa'#237's3611 - INDIA'
                'Inscri'#231#227'o Estadual '
                '      $('#39'.oculta'#39').hide();'
                '      $(this).parent().parent().children('#39'label'#39').toggle();'
                
                  '      $(this).parent().parent().parent().parent().parent().next(' +
                  ').toggle();'
                'Dados dos Produtos e Servi'#231'os '
                'Num.'
                'Descri'#231#227'o'
                'Qtd.'
                'Unidade Comercial'
                'Valor(R$)'
                '1'
                'SULFATO BASICO DE CROMO UTILIZ'
                '20,000'
                'T'
                '39.310,64'
                'C'#243'digo do Produto 1565'
                'C'#243'digo NCM39269090'
                'CFOP3949'
                
                  'Indicador de Composi'#231#227'o do Valor Total da NF-e 1 - Valor deste I' +
                  'tem comp'#245'e o valor total da NF-e '
                'C'#243'digo EAN Comercial'
                'Unidade ComercialT'
                'Quantidade Comercial20,0000'
                'C'#243'digo EAN Tribut'#225'vel'
                'Unidade Tribut'#225'velT'
                'Quantidade Tribut'#225'vel20,0000'
                'Valor unit'#225'rio de comercializa'#231#227'o1.965,5320'
                'Valor unit'#225'rio de tributa'#231#227'o1.965,5320'
                'ICMS NORMAL e ST '
                'Origem da Mercadoria 1 - Estrangeira - Importa'#231#227'o direta '
                'Tributa'#231#227'o do ICMS41 - N'#227'o tributada '
                'DECLARA'#199#195'O DE IMPORTA'#199#195'O '
                'DI/DSI/DA '
                'Data Registro'
                'Local Desembara'#231'o Aduaneiro'
                'Data '
                'UF '
                'C'#243'digo do Exportador'
                '1108973525'
                '17/05/2011'
                'CEARAPORTOS - FORTALEZA'
                '17/05/2011'
                'CE'
                '1676'
                'ADI'#199#213'ES DA DI '
                'No. Adi'#231#227'o '
                'Item '
                'C'#243'digo Fabricante Estrangeiro '
                'Valor do Desconto '
                '1'
                '1'
                '1676'
                'NaN'
                'DOCUMENTO DE IMPORTA'#199#195'O '
                'N'#218'MERO DI 11108973525'
                'Totais'
                'ICMS '
                'Base de C'#225'lculo ICMS0,00'
                'Valor do ICMS0,00'
                'Base de C'#225'lculo ICMS ST 0,00'
                'Valor ICMS Substitui'#231#227'o0,00'
                'Valor Total dos Produtos 39.310,64'
                'Valor do Frete0,00'
                'Valor do Seguro0,00'
                'Outras Despesas Acess'#243'rias1.000,00'
                'Valor Total do IPI0,00'
                'Valor Total da NFe40.310,64'
                'Valor Total dos Descontos 0,00'
                'Valor Total do II0,00'
                'Valor do PIS0,00'
                'Valor da COFINS0,00'
                'Dados do Transporte'
                'Modalidade do Frete0-por conta do emitente '
                'VOLUMES '
                'Quantidade 800'
                'Esp'#233'cie SACOS'
                'Peso L'#237'quido 20.000'
                'Peso Bruto 20.530'
                'Dados de Cobran'#231'a'
                'FATURA'
                'N'#250'mero000-000000000'
                'Valor Original40.310,64'
                '          else'
                'Informa'#231#245'es Adicionais '
                'Formato de Impress'#227'o1 - Retrato '
                'Formato de Emiss'#227'o1'
                'D'#237'gito Verificador da Chave de Acesso0'
                'Identifica'#231#227'o do Ambiente1 ? Produ'#231#227'o '
                'Finalidade1'
                'Processo0'
                'Vers'#227'oBDER_1105071652'
                'INFORMA'#199#213'ES COMPLEMENTARES DE INTERESSE DO CONTRIBUINTE '
                
                  'Descri'#231#227'o DI: 11/0897352-5 DE 17/05/2011&amp;#13;&amp;#10;LOCAL ' +
                  'DE DESEMBARACO: CEARAPORTOS - FORTALEZA/CE&amp;#13;&amp;#10;CLAS' +
                  'SIFICACAO FISCAL 1 - 2833.29.60&amp;#13;&amp;#10;&amp;#13;&amp;#' +
                  '10;ICMS ISENTO CONFORME LIVRO I, ARTIGO 9O, INCISO XXII DO DECRE' +
                  'TO 37699/97&amp;#13;&amp;#10;&amp;#13;&amp;#10;DRAWBACK: 2010 00' +
                  '2 6451&amp;#13;&amp;#10;NF PARCIAL NO 02 - REFERENTE A NF NO 160' +
                  '8 DE 19/05/2011'
                
                  'Conhe'#231'a a NF-e | Servi'#231'os | Legisla'#231#227'o | Documentos | Downloads ' +
                  '| '#193'rea Restrita Portal da NF-e 2011 - Nota Fiscal Eletr'#244'nica '
                'Receita Federal '
                '//'
                ''
                '')
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 559
    Width = 907
    Height = 63
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 903
      Height = 46
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 12
        Top = 24
        Width = 879
        Height = 16
        TabOrder = 0
      end
    end
  end
end
