object FmOAuth: TFmOAuth
  Left = 0
  Top = 0
  Caption = 'OAuth'
  ClientHeight = 603
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object Browser: TWebBrowser
    Left = 0
    Top = 10
    Width = 555
    Height = 593
    Align = alClient
    TabOrder = 0
    OnProgressChange = BrowserProgressChange
    OnNavigateComplete2 = BrowserNavigateComplete2
    ExplicitLeft = 176
    ExplicitTop = 96
    ExplicitWidth = 300
    ExplicitHeight = 150
    ControlData = {
      4C000000E42D0000083100000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E12620A000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object PB1: TProgressBar
    Left = 0
    Top = 0
    Width = 555
    Height = 10
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 1
    ExplicitLeft = 545
    ExplicitWidth = 603
  end
end
