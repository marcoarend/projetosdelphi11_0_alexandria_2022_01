unit UnDmkHTML;

interface

uses
  Windows, Classes, 
  ActiveX,
  // Internet
  SHDocVw, WinInet, WSDLBind, ComObj,
  //
  StdCtrls, ExtCtrls, Messages, SysUtils, Graphics, Controls, Forms, Dialogs,
  Menus, UnInternalConsts, UnInternalConsts2, Math, UnMsgInt, Db, DbCtrls, Mask,
  Buttons, ZCF2, (*DBTables,*) mySQLDbTables, ComCtrls, (*DBIProcs,*) Registry, Grids,
  DBGrids, WinSkinStore, WinSkinData, CheckLst, printers, CommCtrl, TypInfo,
  ShlObj, RichEdit, ShellAPI, Consts, OleCtrls;

// const

type

  TUnDmkHTML = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function GetBrowserHtml(const webBrowser: TWebBrowser): String;
    function GetRawHtml(var web_browser: TWebBrowser): String;
    //procedure SaveURLasMHT(const url: String; const fileName: String);

  end;

implementation

var
  h_cachedInternet: HINTERNET;

{ TUnDmkHTML }

function TUnDmkHTML.GetBrowserHtml(const webBrowser: TWebBrowser): String;
(*
Obtain the HTML from the WebBrowser
The following function will extract the HTML from a WebBrowser, including the header block as well as the body of the HTML:
*)
var
  strStream: TStringStream;
  adapter: IStream;
  browserStream: IPersistStreamInit;
begin
  strStream := TStringStream.Create('');
  try
    browserStream := webBrowser.Document as IPersistStreamInit;
    adapter := TStreamAdapter.Create(strStream,soReference);
    browserStream.Save(adapter,true);
    result := strStream.DataString;
  finally
  end;
  strStream.Free();
end;

function TUnDmkHTML.GetRawHtml(var web_browser: TWebBrowser): String;
(*
Obtain the HTML from the browser cache
The following example shows how to retrieve the HTML from the browser cache:
*)
var
  http_handle: HINTERNET;
  buffer: array [0..20] of Char;
  url: String;
  bytes_read: DWORD;
begin
  url := web_browser.LocationURL;
  http_handle := InternetOpenUrl(h_cachedInternet,
    PChar(url),nil,0,INTERNET_FLAG_NO_UI,0);
  if http_handle = nil then
    result := ''
  else
  begin
    //--------------------------------------------------------------
    // Retrieve the URL data. Hopefully this should be straight from
    // the cache because of how the internet connection was defined.
    //--------------------------------------------------------------
    result := '';
    repeat
      InternetReadFile(http_handle,@buffer,Length(buffer),bytes_read);
      result := result + Copy(buffer,1,bytes_read);
    until bytes_read =0;
    InternetCloseHandle(http_handle);
  end;
end;

{
procedure TUnDmkHTML.SaveURLasMHT(const url, fileName: String);
(*
How to save the page displayed in a TWebBrowser to a single page .mht web archive file
*)
var
  imsg: IMessage;
  iconf: IConfiguration;
  stream: _Stream;
begin
  imsg := CoMessage.Create;
  iconf := CoConfiguration.Create;

  try
    imsg.Configuration := iconf;
    imsg.CreateMHTMLBody(url,cdoSuppressAll,'','');
    stream := imsg.GetStream;
    stream.SaveToFile(fileName, adSaveCreateOverWrite);
  finally
    imsg := nil;
    iconf := nil;
    stream := nil;
  end;
(*
To save the page currently visible in the browser use:
procedure SaveAsMHT(const webBrowser: TWebBrowser; const fileName: String);
begin
  SaveURLasMHT(webBrowser.LocationURL,fileName);
end;
*)
end;
}

initialization

//--------------------
// Initialise WinInet.
//--------------------
h_cachedInternet := InternetOpen(PChar(Application.title),
  INTERNET_OPEN_TYPE_PRECONFIG_WITH_NO_AUTOPROXY,nil,nil,
  INTERNET_FLAG_FROM_CACHE);

end.
