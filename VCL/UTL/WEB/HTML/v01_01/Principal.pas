unit Principal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ImgList, StdCtrls,OleCtrls,
  SHDocVw, Menus,WBFuncs,Registry;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Editar1: TMenuItem;
    mNovaJanela: TMenuItem;
    Abrir1: TMenuItem;
    SalvarComo1: TMenuItem;
    N1: TMenuItem;
    Imprimir1: TMenuItem;
    VisualizarImpresso1: TMenuItem;
    ConfigurarPgina1: TMenuItem;
    N2: TMenuItem;
    Propriedades1: TMenuItem;
    N3: TMenuItem;
    Sair1: TMenuItem;
    Recortar1: TMenuItem;
    Copiar1: TMenuItem;
    Colar1: TMenuItem;
    N4: TMenuItem;
    LocalizaNestaPgina1: TMenuItem;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    btnvoltar: TToolButton;
    btnavancar: TToolButton;
    btnparar: TToolButton;
    btnatualizar: TToolButton;
    ToolButton10: TToolButton;
    btnhome: TToolButton;
    ToolButton11: TToolButton;
    btnprocurar: TToolButton;
    StatusBar1: TStatusBar;
    ImageList1: TImageList;
    btnimprimir: TToolButton;
    Button1: TButton;
    ProgressBar1: TProgressBar;
    ComboBox1: TComboBox;
    WebBrowser1: TWebBrowser;
    OpenDialog1: TOpenDialog;
    procedure btnvoltarClick(Sender: TObject);
    procedure btnavancarClick(Sender: TObject);
    procedure btnpararClick(Sender: TObject);
    procedure btnatualizarClick(Sender: TObject);
    procedure btnhomeClick(Sender: TObject);
    procedure btnprocurarClick(Sender: TObject);
    procedure btnimprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure WebBrowser1CommandStateChange(Sender: TObject;
      Command: Integer; Enable: WordBool);
    procedure WebBrowser1DocumentComplete(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure WebBrowser1ProgressChange(Sender: TObject; Progress,
      ProgressMax: Integer);
    procedure FormResize(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(Sender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure WebBrowser1StatusTextChange(Sender: TObject;
      const Text: WideString);
    procedure WebBrowser1TitleChange(Sender: TObject;
      const Text: WideString);
    procedure mNovaJanelaClick(Sender: TObject);
    procedure Abrir1Click(Sender: TObject);
    procedure SalvarComo1Click(Sender: TObject);
    procedure Imprimir1Click(Sender: TObject);
    procedure VisualizarImpresso1Click(Sender: TObject);
    procedure Propriedades1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure Recortar1Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Colar1Click(Sender: TObject);
    procedure LocalizaNestaPgina1Click(Sender: TObject);
    procedure ConfigurarPgina1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure ExibirURLsVisitadas(Urls: TStrings);
var
Reg: TRegistry;
S: TStringList;
i: Integer;
begin
Reg := TRegistry.Create;
try
Reg.RootKey := HKEY_CURRENT_USER;
if Reg.OpenKey('Software\Microsoft\Internet '+
'Explorer\TypedURLs', False) then
begin
S := TStringList.Create;
try
reg.GetValueNames(S);
for i := 0 to S.Count - 1 do
begin
Urls.Add(reg.ReadString(S.Strings[i]));
end;
finally
S.Free;
end;
Reg.CloseKey;
end;
finally
Reg.Free;
end;
end;

procedure TMainForm.btnvoltarClick(Sender: TObject);
begin
//Serve para Voltar � p�gina anteriormente visitada.
webbrowser1.goback;
end;

procedure TMainForm.btnavancarClick(Sender: TObject);
begin
//Serve para van�ar para a pr�xima p�gina.
webbrowser1.GoForward;
end;

procedure TMainForm.btnpararClick(Sender: TObject);
begin
//Serve para parar a p�gina que est� sendo carregada.
webbrowser1.Stop;
end;

procedure TMainForm.btnatualizarClick(Sender: TObject);
begin
//Serve para Atualizar a p�gina Atual
webbrowser1.Refresh;
end;

procedure TMainForm.btnhomeClick(Sender: TObject);
begin
//Serve para ir at� a p�gina inicial da internet
webbrowser1.GoHome;
end;

procedure TMainForm.btnprocurarClick(Sender: TObject);
begin
// Essa fun��o, abre a p�gina de pesquisa padr�o do IE.
webbrowser1.GoSearch;
end;

procedure TMainForm.btnimprimirClick(Sender: TObject);
begin
WB_ShowPrintDialog(WebBrowser1);
end;    

procedure TMainForm.FormCreate(Sender: TObject);
begin
webbrowser1.GoHome;
ExibirURLsVisitadas(combobox1.Items);
end;

procedure TMainForm.ComboBox1KeyPress(Sender:
TObject; var Key: Char);
begin
  if (key=#13) then
    begin
    webbrowser1.Navigate(combobox1.Text);
    end;
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
webbrowser1.Navigate(combobox1.Text);
end;

procedure TMainForm.WebBrowser1CommandStateChange(Sender: TObject;
  Command: Integer; Enable: WordBool);
begin
  case Command of
    CSC_NAVIGATEBACK: begin
//Ativa e Desativa Automaticamente o Bot�o Voltar,
//Caso tenha alguma p�gina para voltar
        BtnVoltar.Enabled := Enable;
      end;
    CSC_NAVIGATEFORWARD: begin
//Ativa e Desativa Automaticamente o Bot�o Avan�ar,
//Caso tenha alguma p�gina para voltar
          BtnAvancar.Enabled := Enable;
      end;
  end;
end;

procedure TMainForm.WebBrowser1DocumentComplete(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
    ProgressBar1.Position := 0;
    StatusBar1.Panels[0].Text := '';
end;

procedure TMainForm.WebBrowser1ProgressChange
(Sender: TObject; Progress,ProgressMax: Integer);
begin
{Ele faz um rotina para saber se o valor Maximo do
Progressbar � maior que 1 e o valor minimo tbem
se for, ent�o ele faz a rotina}
   If (Progress>=1) and (ProgressMax>1)
    then
       begin
       //Ele tira uma valor percentual para colocar
       //no Progressbar
         ProgressBar1.Position := Round((Progress * 100)
         Div ProgressMax);
         ProgressBar1.Visible  := True;
        end
   else
      begin
          ProgressBar1.Position := 1;
          ProgressBar1.Visible  := False;
      end;
end;

procedure TMainForm.FormResize(Sender: TObject);
var
  r: TRect;
const
  SB_GETRECT = WM_USER + 10;
begin
  // Set Progressbar Position
  Statusbar1.Perform(SB_GETRECT, 1, Integer(@R));
  ProgressBar1.Parent := Statusbar1;
  ProgressBar1.SetBounds(r.Left, r.Top,
  r.Right - r.Left - 5, r.Bottom - r.Top);
end;

procedure TMainForm.WebBrowser1BeforeNavigate2(Sender: TObject;
  const pDisp: IDispatch; var URL, Flags, TargetFrameName,
  PostData,Headers: OleVariant; var Cancel: WordBool);
begin
{Ao Nevagar uma p�gina, ele coloca automaticamente
o endere�o dela no combobox, para sabermos que p�gina
estamos entrando, o endere�o da mesma}
combobox1.Text:=url;
end;

procedure TMainForm.WebBrowser1StatusTextChange(Sender:
TObject;
  const Text: WideString);
begin
StatusBar1.Panels[0].Text:=text;
end;

procedure TMainForm.WebBrowser1TitleChange(Sender: TObject;
  const Text: WideString);
begin
  MainForm.Caption:=text + ' - Kennedy Tedesco';
end;

procedure TMainForm.mNovaJanelaClick(Sender: TObject);
  begin
    WinExec(pchar(application.exename),SW_MAXIMIZE);
end;

procedure TMainForm.Abrir1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
   webbrowser1.Navigate(OpenDialog1.FileName) else
   exit;
end;

procedure TMainForm.SalvarComo1Click(Sender: TObject);
begin
  WB_Save(WebBrowser1);
end;

procedure TMainForm.Imprimir1Click(Sender: TObject);
begin
  WB_ShowPrintDialog(WebBrowser1);
end;

procedure TMainForm.VisualizarImpresso1Click(Sender: TObject);
begin
  WB_ShowPrintPreview(webbrowser1);
end;

procedure TMainForm.Propriedades1Click(Sender: TObject);
begin
  WB_ShowPropertiesDialog(webbrowser1);
end;

procedure TMainForm.Sair1Click(Sender: TObject);
begin
application.Terminate;
end;

procedure TMainForm.Recortar1Click(Sender: TObject);
begin
  WB_Cut(webbrowser1);
end;

procedure TMainForm.Copiar1Click(Sender: TObject);
begin
  WB_Copy(webbrowser1);
end;

procedure TMainForm.Colar1Click(Sender: TObject);
begin
  WB_paste(webbrowser1);
end;

procedure TMainForm.LocalizaNestaPgina1Click(Sender: TObject);
begin
  WB_ShowFindDialog(webbrowser1);
end;

procedure TMainForm.ConfigurarPgina1Click(Sender: TObject);
begin
  WB_ShowPageSetup(webbrowser1);
end;

end.
