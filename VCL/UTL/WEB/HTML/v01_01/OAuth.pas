unit OAuth;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.OleCtrls, SHDocVw, Vcl.ComCtrls;

type
  TOAuth2WebFormRedirectEvent = procedure(const AURL: string; var DoCloseWebView : Boolean) of object;

  TFmOAuth = class(TForm)
    Browser: TWebBrowser;
    PB1: TProgressBar;
    procedure BrowserNavigateComplete2(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
    procedure FormCreate(Sender: TObject);
    procedure BrowserProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
  private
    { Private declarations }
    FOnAfterRedirect: TOAuth2WebFormRedirectEvent;
    FLastURL: string;
  public
    { Public declarations }
    procedure ShowWithURL(const AURL: string);

    property LastURL: string read FLastURL;

    property OnAfterRedirect: TOAuth2WebFormRedirectEvent read FOnAfterRedirect write FOnAfterRedirect;
  end;

var
  FmOAuth: TFmOAuth;

implementation

{$R *.dfm}

procedure TFmOAuth.BrowserProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if Progress > 0 then
  begin
    PB1.Max      := ProgressMax;
    PB1.Position := Progress
  end else
  begin
    PB1.Position := 0
  end;
end;

procedure TFmOAuth.FormCreate(Sender: TObject);
begin
  FOnAfterRedirect := nil;

  FLastURL := '';
end;

procedure TFmOAuth.ShowWithURL(const AURL: string);
begin
  Browser.Navigate(AURL);
  Self.ShowModal;
end;

procedure TFmOAuth.BrowserNavigateComplete2(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
var
  LDoCloseForm : boolean;
begin
  FLastURL := VarToStrDef(URL, '');

  if Assigned(FOnAfterRedirect) then
  begin
    LDoCloseForm:= FALSE;

    FOnAfterRedirect(FLastURL, LDoCloseForm);

    if LDoCloseForm then
      Self.Close;
  end;
end;

end.
