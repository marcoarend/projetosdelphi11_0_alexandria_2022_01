unit UnVCL_PushNotificatios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls,

  JSON,
  System.Net.HttpClient,
  UnGrl_Vars, UnProjGroup_Consts, DmkGeral;

type
  TUnVCL_PushNotificatios = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  EnviaNotificacao(const Mensagem: String; const Tokens, FldExtras,
              MsgExtras: array of String; var Retorno: String): Boolean;
  end;

var
  VCL_PushNotificatios: TUnVCL_PushNotificatios;

implementation

{ TUnVCL_PushNotificatios }

function TUnVCL_PushNotificatios.EnviaNotificacao(const Mensagem: String; const
  Tokens, FldExtras, MsgExtras: array of String; var Retorno: String): Boolean;
const
  sProcName = 'TUnVCL_PushNotificatios.EnviaNotificacao()';
var
  client : THTTPClient;
  v_json : TJSONObject;
  v_jsondata : TJSONObject;
  v_registerids : TJSONArray;
  v_data : TStringStream;
  v_response : TStringStream;
  //-------
  //token_celular : string;
  codigo_projeto : string;
  api : string;
  url_google : string;
  //
  I, N: Integer;
begin
  Result := False;
  N := Length(FldExtras);
  if Length(FldExtras) <> Length(MsgExtras) then
  begin
    if Length(MsgExtras) < N then
      N := Length(MsgExtras);
    Geral.MB_Erro('Arrays de mensagens extras difere em ' + sProcName);
  end else
  try
    Screen.Cursor := crHourGlass;
    //
    //url_google := 'https://android.googleapis.com/gcm/send';
    url_google := 'https://fcm.googleapis.com/fcm/send'; // Firebase
    //token_celular := 'APA91bG1eDnP7_lgQhbSwJsX-Xvimd_RFG13Sutr363p_S5yA85xKatJ2yrAfuLuHkQMgCBkxKNdfXKTXvcFwwsVbM5sRcRjjjZR8ZYOSjehW-cqOiCAvlWHhNsOkzl8zoDbCYndBl1S'; // Token do celular...
    codigo_projeto := VAR_PushNotifications_CodigoProjeto; //'205807968282'; // codigo do projeto OverSerrMob
    api := VAR_PushNotifications_CodigoAPI; // 'AIzaSyAuoY0fIOMlbozcYpX6EUQXWtthRRCnRoI';  // API Dmk

    //--------------------------------

    v_registerids := TJSONArray.Create;
    for I := Low(Tokens) to High(Tokens) do
      v_registerids.Add((*token_celular*)Tokens[I]);

    v_jsondata := TJSONObject.Create;
    v_jsondata.AddPair('id', codigo_projeto);
    v_jsondata.AddPair('message', Mensagem);
    for I := 0 to N - 1 do
      v_jsondata.AddPair(FldExtras[I], MsgExtras[I]);

    v_json := TJSONObject.Create;
    v_json.AddPair('registration_ids', v_registerids);
    v_json.AddPair('data', v_jsondata);

    client := THTTPClient.Create;
    client.ContentType := 'application/json';
    client.CustomHeaders['Authorization'] := 'key=' + api;

    if VAR_PushNotificatios_Memo <> nil then
      VAR_PushNotificatios_Memo.Lines.Add(v_json.ToString);

    v_data := TStringStream.Create(v_json.ToString);
    V_data.Position := 0;

    v_response := TStringStream.Create;

    client.Post(url_google, v_data, v_response);
    v_response.Position := 0;

    Retorno := v_response.DataString;
    if VAR_PushNotificatios_Memo <> nil then
      VAR_PushNotificatios_Memo.Lines.Add(Retorno);

    Result := True;
    Screen.Cursor := crDefault;
  except
    on E: Exception do
    begin
      ShowMessage('Erro: ' + E.Message);
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
