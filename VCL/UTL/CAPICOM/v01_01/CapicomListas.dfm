object FmCapicomListas: TFmCapicomListas
  Left = 90
  Top = 156
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'ASS-DIGIT-001 :: Listas de Certificados Digitais'
  ClientHeight = 384
  ClientWidth = 723
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 723
    Height = 45
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 598
    object Label1: TLabel
      Left = 5
      Top = 4
      Width = 129
      Height = 13
      Caption = 'Reposit'#243'rio de Certificados:'
    end
    object cmbStores: TComboBox
      Left = 3
      Top = 19
      Width = 586
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = cmbStoresChange
      Items.Strings = (
        'Pessoal'
        'Outras Pessoas'
        'Autoridades de Certifica'#231#227'o Intermedi'#225'rias'
        'Autoridades de Certifica'#231#227'o Raiz Confi'#225'veis')
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 93
    Width = 723
    Height = 177
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 598
    object PageControl: TPageControl
      Left = 1
      Top = 1
      Width = 721
      Height = 175
      ActivePage = TabSheet6
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 596
      object TabSheet5: TTabSheet
        Caption = 'Pessoal'
        ImageIndex = 4
        object LVPessoal: TListView
          Left = 0
          Top = 0
          Width = 588
          Height = 147
          Align = alClient
          Columns = <
            item
              Caption = 'Nome'
              Width = 250
            end
            item
              Caption = 'Emitido por'
              Width = 200
            end
            item
              AutoSize = True
              Caption = 'Validade'
            end>
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnCustomDrawItem = LVPessoalCustomDrawItem
          OnDblClick = LVPessoalDblClick
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Outras Pessoas'
        ImageIndex = 5
        object LVOutras: TListView
          Left = 0
          Top = 0
          Width = 713
          Height = 147
          Align = alClient
          Columns = <
            item
              Caption = 'Nome'
              Width = 250
            end
            item
              Caption = 'Emitido por'
              Width = 200
            end
            item
              AutoSize = True
              Caption = 'Validade'
            end>
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnDblClick = LVOutrasDblClick
          ExplicitWidth = 588
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Autoridades de Certifica'#231#227'o Intermedi'#225'rias'
        ImageIndex = 6
        object LVIntermediarias: TListView
          Left = 0
          Top = 0
          Width = 588
          Height = 147
          Align = alClient
          Columns = <
            item
              Caption = 'Nome'
              Width = 250
            end
            item
              Caption = 'Emitido por'
              Width = 200
            end
            item
              AutoSize = True
              Caption = 'Validade'
            end>
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnDblClick = LVPessoalDblClick
        end
      end
      object TabSheet8: TTabSheet
        Caption = 'Autoridades de Certifica'#231#227'o Raiz Confi'#225'veis'
        ImageIndex = 7
        object LVRaiz: TListView
          Left = 0
          Top = 0
          Width = 588
          Height = 147
          Align = alClient
          Columns = <
            item
              Caption = 'Nome'
              Width = 250
            end
            item
              Caption = 'Emitido por'
              Width = 200
            end
            item
              AutoSize = True
              Caption = 'Validade'
            end>
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnDblClick = LVPessoalDblClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 723
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 598
    object GB_R: TGroupBox
      Left = 675
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 550
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 627
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 502
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 360
        Height = 32
        Caption = 'Listas de Certificados Digitais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 360
        Height = 32
        Caption = 'Listas de Certificados Digitais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 360
        Height = 32
        Caption = 'Listas de Certificados Digitais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 270
    Width = 723
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 598
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 719
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 594
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 314
    Width = 723
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitWidth = 598
    object PnSaiDesis: TPanel
      Left = 577
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 452
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 575
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 450
      object BitBtn1: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Selecionar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object BtImportar: TBitBtn
        Tag = 270
        Left = 112
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Importar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImportarClick
      end
      object BtExportar: TBitBtn
        Tag = 269
        Left = 204
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Exportar'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtExportarClick
      end
      object BtRemover: TBitBtn
        Tag = 12
        Left = 296
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Remover'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtRemoverClick
      end
    end
  end
  object odlgCertificados: TOpenDialog
    Left = 353
    Top = 175
  end
  object SaveDialog: TSaveDialog
    Left = 321
    Top = 175
  end
  object MainMenu: TMainMenu
    Left = 289
    Top = 175
    object fdfdf1: TMenuItem
      Caption = '&Arquivo'
      object mniSair: TMenuItem
        Caption = 'Sair'
      end
    end
    object Uteis1: TMenuItem
      Caption = '&'#218'teis'
      object ExportarRepositrioDeCertificados: TMenuItem
        Caption = 'Exportar &reposit'#243'rio de certificados'
        OnClick = ExportarRepositrioDeCertificadosClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object VerificarsAssinaturaDeDocumento: TMenuItem
        Caption = '&Verificar assinatura de documento'
        OnClick = VerificarsAssinaturaDeDocumentoClick
      end
      object VerificarsAssinaturaDeCodigo: TMenuItem
        Caption = 'Verificar assinatura de c'#243'dig&o'
        OnClick = VerificarsAssinaturaDeCodigoClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object CalcularHashDeUmDoc: TMenuItem
        Caption = 'Calcular &hash de um documento'
        object Calcular: TMenuItem
          Caption = 'Calcular'
          OnClick = CalcularClick
        end
        object N3: TMenuItem
          Caption = '-'
        end
        object SHA1: TMenuItem
          AutoCheck = True
          Caption = 'SHA-1'
          Checked = True
          GroupIndex = 1
          RadioItem = True
        end
        object SHA256: TMenuItem
          AutoCheck = True
          Caption = 'SHA-256'
          GroupIndex = 1
          RadioItem = True
        end
        object SHA384: TMenuItem
          AutoCheck = True
          Caption = 'SHA-384'
          GroupIndex = 1
          RadioItem = True
        end
        object SHA512: TMenuItem
          AutoCheck = True
          Caption = 'SHA-512'
          GroupIndex = 1
          RadioItem = True
        end
        object MD5: TMenuItem
          AutoCheck = True
          Caption = 'MD5'
          GroupIndex = 1
          RadioItem = True
        end
        object MD4: TMenuItem
          AutoCheck = True
          Caption = 'MD4'
          GroupIndex = 1
          RadioItem = True
        end
        object MD2: TMenuItem
          AutoCheck = True
          Caption = 'MD2'
          GroupIndex = 1
          RadioItem = True
        end
      end
      object CriptografiaAlgoritmo: TMenuItem
        Caption = 'Criptografia - algoritmo'
        object RC2: TMenuItem
          AutoCheck = True
          Caption = 'RC2'
          Checked = True
          RadioItem = True
        end
        object RC4: TMenuItem
          AutoCheck = True
          Caption = 'RC4'
          RadioItem = True
        end
        object DES: TMenuItem
          AutoCheck = True
          Caption = 'DES'
          RadioItem = True
        end
        object TripleDES: TMenuItem
          AutoCheck = True
          Caption = '3DES'
          RadioItem = True
        end
        object AES: TMenuItem
          AutoCheck = True
          Caption = 'AES'
          RadioItem = True
        end
      end
      object CriptografiaTamanhoDaChave: TMenuItem
        Caption = 'Criptografia - tamanho da chave'
        object Maximo: TMenuItem
          AutoCheck = True
          Caption = 'M'#225'ximo'
          Checked = True
          GroupIndex = 1
          RadioItem = True
        end
        object Tam40: TMenuItem
          AutoCheck = True
          Caption = '40-bit'
          GroupIndex = 1
          RadioItem = True
        end
        object Tam56: TMenuItem
          AutoCheck = True
          Caption = '56-bit'
          GroupIndex = 1
          RadioItem = True
        end
        object Tam128: TMenuItem
          AutoCheck = True
          Caption = '128-bit'
          GroupIndex = 1
          RadioItem = True
        end
        object Tam192: TMenuItem
          AutoCheck = True
          Caption = '192-bit'
          GroupIndex = 1
          RadioItem = True
        end
        object Tam256: TMenuItem
          AutoCheck = True
          Caption = '256-bit'
          GroupIndex = 1
          RadioItem = True
        end
      end
      object AbrirEnvelope: TMenuItem
        Caption = 'Abrir envelo&pe'
        OnClick = AbrirEnvelopeClick
      end
      object CriptografarArquivo: TMenuItem
        Caption = '&Criptografar arquivo'
        OnClick = CriptografarArquivoClick
      end
      object DescriptografarArquivo: TMenuItem
        Caption = '&Descriptografar arquivo'
        OnClick = DescriptografarArquivoClick
      end
    end
  end
  object popAssinatura: TPopupMenu
    Left = 383
    Top = 175
    object AssinarDocumento: TMenuItem
      Caption = '&Assinar documento'
      OnClick = AssinarDocumentoClick
    end
    object CoAssinarDocumento: TMenuItem
      Caption = '&Co-assinar documento'
      OnClick = CoAssinarDocumentoClick
    end
    object AssinarCodigo: TMenuItem
      Caption = 'A&ssinar c'#243'digo'
      OnClick = AssinarCodigoClick
    end
  end
  object PopEnvelope: TPopupMenu
    Left = 412
    Top = 175
    object CriarEnvelope: TMenuItem
      Caption = 'Criar &envelope'
      OnClick = CriarEnvelopeClick
    end
  end
end
