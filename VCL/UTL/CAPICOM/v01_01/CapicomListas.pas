///////////////////////////////////////////////////////////////////////
//
//  Este c�digo fonte � parte do livro
//  Assinatura Digital - Solu��o Delphi e CAPICOM.
//  Autor: Marcelo Lima (www.marcelolima.eti.br)
//  Editora: Visual Books
//  ISBN: 8575021710
//  Ano: 2005
//  Edi��o: 1
//  P�ginas: 162
//
//  O c�digo � fornecido da maneira que est� e o autor
//  n�o se responsabiliza pelo seu uso para qualquer
//  que seja a finalidade (do c�digo inteiro o de parte dele).
//  O autor n�o se responsabiliza por este c�digo, caso
//  ele n�o tenha sido obtido junto a editora ou com o
//  pr�prio autor.
//  N�o � intuito do autor fornecer  um c�digo que seja exemplo
//  de modelagem e formata��o, o objetivo � fornecer um c�digo
//  dit�tico e que demonstre claramente as principais funcionalidades
//  da CAPICOM.
//  Os c�digos deste exemplo est�o devidamente explicados
//  no livro Assinatura Digital - Solu��o Delphi e CAPICOM.
//
//  A unit CAPICOM_TLB.pas n�o � de autoria do autor. Esta unit
//  � criada de maneira autom�tica pelo Delphi a partir do
//  m�dulo CAPICOM.dll.
//
///////////////////////////////////////////////////////////////////////
unit CapicomListas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, CAPICOM_TLB, ComObj, Menus, Buttons,
  dmkImage, UnDmkEnums;

type

  TDinByteArray = Array of byte;

  TFmCapicomListas = class(TForm)
    cmbStores: TComboBox;
    PageControl: TPageControl;
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    odlgCertificados: TOpenDialog;
    SaveDialog: TSaveDialog;
    MainMenu: TMainMenu;
    fdfdf1: TMenuItem;
    mniSair: TMenuItem;
    Uteis1: TMenuItem;
    ExportarRepositrioDeCertificados: TMenuItem;
    popAssinatura: TPopupMenu;
    AssinarDocumento: TMenuItem;
    CoAssinarDocumento: TMenuItem;
    AssinarCodigo: TMenuItem;
    N1: TMenuItem;
    VerificarsAssinaturaDeDocumento: TMenuItem;
    VerificarsAssinaturaDeCodigo: TMenuItem;
    CriptografarArquivo: TMenuItem;
    N2: TMenuItem;
    CalcularHashDeUmDoc: TMenuItem;
    SHA1: TMenuItem;
    MD2: TMenuItem;
    MD4: TMenuItem;
    MD5: TMenuItem;
    SHA256: TMenuItem;
    SHA384: TMenuItem;
    SHA512: TMenuItem;
    N3: TMenuItem;
    Calcular: TMenuItem;
    DescriptografarArquivo: TMenuItem;
    PopEnvelope: TPopupMenu;
    CriarEnvelope: TMenuItem;
    CriptografiaTamanhoDaChave: TMenuItem;
    Maximo: TMenuItem;
    Tam40: TMenuItem;
    Tam56: TMenuItem;
    Tam128: TMenuItem;
    Tam192: TMenuItem;
    Tam256: TMenuItem;
    CriptografiaAlgoritmo: TMenuItem;
    RC2: TMenuItem;
    RC4: TMenuItem;
    DES: TMenuItem;
    TripleDES: TMenuItem;
    AES: TMenuItem;
    AbrirEnvelope: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    BtImportar: TBitBtn;
    BtExportar: TBitBtn;
    BtRemover: TBitBtn;
    TabSheet5: TTabSheet;
    LVPessoal: TListView;
    TabSheet6: TTabSheet;
    LVOutras: TListView;
    TabSheet7: TTabSheet;
    LVIntermediarias: TListView;
    TabSheet8: TTabSheet;
    LVRaiz: TListView;
    procedure cmbStoresChange(Sender: TObject);
    procedure ExportarRepositrioDeCertificadosClick(Sender: TObject);
    procedure AssinarDocumentoClick(Sender: TObject);
    procedure CoAssinarDocumentoClick(Sender: TObject);
    procedure VerificarsAssinaturaDeDocumentoClick(Sender: TObject);
    procedure AssinarCodigoClick(Sender: TObject);
    procedure VerificarsAssinaturaDeCodigoClick(Sender: TObject);
    procedure CalcularClick(Sender: TObject);
    procedure CriptografarArquivoClick(Sender: TObject);
    procedure DescriptografarArquivoClick(Sender: TObject);
    procedure CriarEnvelopeClick(Sender: TObject);
    procedure AbrirEnvelopeClick(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure BtExportarClick(Sender: TObject);
    procedure BtFecharClick(Sender: TObject);
    procedure BtRemoverClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure LVPessoalDblClick(Sender: TObject);
    procedure LVPessoalCustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure LVOutrasDblClick(Sender: TObject);
  private
    { Private declarations }
    function  SelCriptAlgo: CAPICOM_ENCRYPTION_ALGORITHM;
    function  SelCriptKeyLen: CAPICOM_ENCRYPTION_KEY_LENGTH;
    function  IdErro(Id: Integer): String;
    function  ChecarCertAssinatura(Certificado: ICertificate2): boolean;
    function  CarregarArquivo(NomeArquivo: String; out Dados: WideString ): Boolean; overload;
    function  CarregarArquivo(NomeArquivo: String; var Dados: TDinByteArray): Boolean; overload;
    procedure SaveBinFile(FileName: String; BinArray: OleVariant);
    procedure ListarCertificados(ListView: TListView; Certificados: ICertificates);
    procedure ExibirCertInfo(Certificado: ICertificate2);
    procedure ExtensoesCertificado(Certificado: ICertificate2; var Info: String);
    procedure PropriedadesEstendidasCert(Certificado: ICertificate2; var Info: String);
    procedure UsosDaChave(Certificado: ICertificate2; var Info: String);
    //procedure RestricoesBasicasInfo(Certificado: ICertificate2; var Info: String);
    procedure LVXXDblClick(Sender: TListView);
  public
    { Public declarations }
    FSelected: Boolean;
    FSerialNumber: WideString;
    FValidToDate: TDateTime;
  end;

var
  FmCapicomListas: TFmCapicomListas;

const
  CAPICOM_TRUST_IS_NOT_TIME_VALID                  = $00000001;
  CAPICOM_TRUST_IS_NOT_TIME_NESTED                 = $00000002;
  CAPICOM_TRUST_IS_REVOKED                         = $00000004;
  CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID             = $00000008;
  CAPICOM_TRUST_IS_NOT_VALID_FOR_USAGE             = $00000010;
  CAPICOM_TRUST_IS_UNTRUSTED_ROOT                  = $00000020;
  CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN          = $00000040;
  CAPICOM_TRUST_IS_CYCLIC                          = $00000080;
  CAPICOM_TRUST_INVALID_EXTENSION                  = $00000100;
  CAPICOM_TRUST_INVALID_POLICY_CONSTRAINTS         = $00000200;
  CAPICOM_TRUST_INVALID_BASIC_CONSTRAINTS          = $00000400;
  CAPICOM_TRUST_INVALID_NAME_CONSTRAINTS           = $00000800;
  CAPICOM_TRUST_HAS_NOT_SUPPORTED_NAME_CONSTRAINT  = $00001000;
  CAPICOM_TRUST_HAS_NOT_DEFINED_NAME_CONSTRAINT    = $00002000;
  CAPICOM_TRUST_HAS_NOT_PERMITTED_NAME_CONSTRAINT  = $00004000;
  CAPICOM_TRUST_HAS_EXCLUDED_NAME_CONSTRAINT       = $00008000;
  CAPICOM_TRUST_IS_OFFLINE_REVOCATION              = $01000000;
  CAPICOM_TRUST_NO_ISSUANCE_CHAIN_POLICY           = $02000000;
  CAPICOM_TRUST_IS_PARTIAL_CHAIN                   = $00010000;
  CAPICOM_TRUST_CTL_IS_NOT_TIME_VALID              = $00020000;
  CAPICOM_TRUST_CTL_IS_NOT_SIGNATURE_VALID         = $00040000;
  CAPICOM_TRUST_CTL_IS_NOT_VALID_FOR_USAGE         = $00080000;

implementation

uses UnMyObjects, UnMLAGeral, CapicomCertificado, dmkGeral;

{$R *.dfm}

procedure TFmCapicomListas.ListarCertificados(ListView: TListView;
  Certificados: ICertificates);
var
  Item       : TListItem;
  Ind        : Integer;
  Certificado: ICertificate2;
begin
  ListView.Items.Clear;

  if ( Certificados.Count > 0 ) then
  begin
    for Ind := 1 to Certificados.Count do
    begin
      OleCheck(IDispatch(Certificados.Item[Ind]).QueryInterface(ICertificate2, Certificado));
      //
      Item := ListView.Items.Add;
      Item.Caption := Certificado.GetInfo(CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME);
      Item.SubItems.Add(Certificado.IssuerName);
      Item.SubItems.Add(Geral.FDT(Certificado.ValidToDate, 2));
    end;
  end;
end;

procedure TFmCapicomListas.cmbStoresChange(Sender: TObject);
var
  CertStore: IStore3;
  NomeStore: String;
  ListView : TListView;
begin
  ListView := nil;
  //
  case cmbStores.ItemIndex of
    0:
    begin
      NomeStore                   := 'My';
      PageControl.ActivePageIndex := 0;
      ListView                    := LVPessoal;
    end;
    1:
    begin
      NomeStore                   := 'AddressBook';
      PageControl.ActivePageIndex := 1;
      ListView                    := LVOutras;
    end;
    2:
    begin
      NomeStore                   := 'CA';
      PageControl.ActivePageIndex := 2;
      ListView                    := LVIntermediarias;
    end;
    3:
    begin
      NomeStore                   := 'Root';
      PageControl.ActivePageIndex := 3;
      ListView                    := LVRaiz;
    end;
  end;

  CertStore := CoStore.Create;
  CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);
  //
  ListarCertificados(ListView, CertStore.Certificates);
  //
  CertStore := nil;
end;

procedure TFmCapicomListas.LVOutrasDblClick(Sender: TObject);
begin
  LVXXDblClick(LVOutras);
end;

procedure TFmCapicomListas.LVPessoalCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if Geral.ValidaDataBR(Item.SubItems[1], True, False) < Date then
    Sender.Canvas.Font.Color := clSilver
  else
    Sender.Canvas.Font.Color := clWindowText;
end;

procedure TFmCapicomListas.LVPessoalDblClick(Sender: TObject);
(*
var
  CertStore  : IStore3;
  NomeStore  : String;
  Certificado: ICertificate2;
  Ind        : Integer;
*)
begin
(*
  if ( (Sender as TListBox).ItemIndex < 0 ) then
    Exit;

  Ind := (Sender as TListBox).ItemIndex + 1;

  CertStore := CoStore.Create;

  if ( Sender = LVPessoal ) then
  begin
    NomeStore := 'My';
  end;

  if ( Sender = LVOutras ) then
  begin
    NomeStore := 'AddressBook';
  end;

  if ( Sender = LVIntermediarias ) then
  begin
    NomeStore := 'CA';
  end;

  if ( Sender = LVRaiz ) then
  begin
    NomeStore := 'Root';
  end;

  CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);

  OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));

  if ( MessageDlg('Exibir no formato padr�o?', mtConfirmation, [mbYes, mbNo], 0) = mrYes ) then
    Certificado.Display
  else
    ExibirCertInfo(Certificado);

  CertStore := nil;
*)
  LVxxDblClick(LVPessoal);
end;

procedure TFmCapicomListas.LVXXDblClick(Sender: TListView);
var
  CertStore  : IStore3;
  NomeStore  : String;
  Certificado: ICertificate2;
  Ind        : Integer;
begin
  //if ( (Sender as TListBox).ItemIndex < 0 ) then
  if ( (Sender as TListView).ItemIndex < 0 ) then
    Exit;

  Ind := (Sender as TListView).ItemIndex + 1;

  CertStore := CoStore.Create;

  if ( Sender = LVPessoal ) then
  begin
    NomeStore := 'My';
  end;

  if ( Sender = LVOutras ) then
  begin
    NomeStore := 'AddressBook';
  end;

  if ( Sender = LVIntermediarias ) then
  begin
    NomeStore := 'CA';
  end;

  if ( Sender = LVRaiz ) then
  begin
    NomeStore := 'Root';
  end;

  CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);

  OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));

  if ( MessageDlg('Exibir no formato padr�o?', mtConfirmation, [mbYes, mbNo], 0) = mrYes ) then
    Certificado.Display
  else
    ExibirCertInfo(Certificado);

  CertStore := nil;
end;

procedure TFmCapicomListas.ExibirCertInfo(Certificado: ICertificate2);
var
  Info          : String;
  ChavePrivativa: String;
const
  BR            = sLineBreak + sLineBreak;
begin

  Info := 'INFORMA��ES B�SICAS:' + BR;

  with Certificado do
  begin

    ChavePrivativa := 'N�o';
    if ( HasPrivateKey ) then
      ChavePrivativa := 'Sim';

    Info := Info +
      'Vers�o: '          +
      IntToStr(Version)   + BR +
      'N�mero de s�rie: ' +
      SerialNumber        + BR +
      'Validade: '        +
      FormatDateTime('DD/MM/YYYY', ValidFromDate) + ' - ' +
      FormatDateTime('DD/MM/YYYY', ValidToDate)   + BR    +
      'Imp. digital: '    +
      Thumbprint          + BR +
      'Chave privativa: ' +
      ChavePrivativa + BR +
      'Nome Assunto: '    +
      SubjectName         + BR +
      'Nome Emissor: '    +
      IssuerName          + BR +
      'Nome Assunto: '    +
      GetInfo(CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME) + BR +
      'Nome Emissor: '    +
      GetInfo(CAPICOM_CERT_INFO_ISSUER_SIMPLE_NAME)  + BR +
      'Email Assunto: '   +
      GetInfo(CAPICOM_CERT_INFO_SUBJECT_EMAIL_NAME)  + BR +
      'Email Emissor: '   +
      GetInfo(CAPICOM_CERT_INFO_ISSUER_EMAIL_NAME)   + BR +
      'UPN Assunto: '     +
      GetInfo(CAPICOM_CERT_INFO_SUBJECT_UPN)         + BR +
      'UPN Emissor: '     +
      GetInfo(CAPICOM_CERT_INFO_ISSUER_UPN)          + BR +
      'DNS Assunto: '     +
      GetInfo(CAPICOM_CERT_INFO_SUBJECT_DNS_NAME)    + BR +
      'DNS Emissor: '     +
      GetInfo(CAPICOM_CERT_INFO_ISSUER_DNS_NAME)     + BR;
  end;

  ExtensoesCertificado(Certificado, Info);
  PropriedadesEstendidasCert(Certificado, Info);
  UsosDaChave(Certificado, Info);

  if MyObjects.CriaForm_AcessoTotal(TFmCapicomCertificado, FmCapicomCertificado) then
  begin
    FmCapicomCertificado.memCertInfo.Clear;
    FmCapicomCertificado.memCertInfo.Text := Info;
    FmCapicomCertificado.ShowModal;
    FmCapicomCertificado.Destroy;
  end;
end;

procedure TFmCapicomListas.ExtensoesCertificado(Certificado: ICertificate2;
  var Info: String);
var
  Ind        : Integer;
  Extensao   : IExtension;
const
  BR         = sLineBreak;
begin

  Info := Info + 'EXTENS�ES:' + BR + BR;

  for Ind := 1 to Certificado.Extensions.Count do
  begin
    OleCheck(IDispatch(Certificado.Extensions.Item[Ind]).QueryInterface(IExtension, Extensao));
    Info := Info + Extensao.OID.FriendlyName  + ' (' +
            Extensao.OID.Value + '):' + BR +
            Extensao.EncodedData.Format(true) + BR + BR;
  end;

end;

procedure TFmCapicomListas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCapicomListas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSelected := False;
  PageControl.ActivePageIndex := 0;
end;

procedure TFmCapicomListas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCapicomListas.PropriedadesEstendidasCert(Certificado: ICertificate2;
  var Info: String);
var
  Ind        : Integer;
  ExtProp    : IExtendedProperty;
  Util       : IUtilities;
const
  BR         = sLineBreak;
begin

  Info := Info + 'PROPRIEDADES ESTENDIDAS:' + BR + BR;

  Util := CoUtilities.Create;

  for Ind := 1 to Certificado.ExtendedProperties.Count do
  begin
    OleCheck(IDispatch(Certificado.ExtendedProperties.Item[Ind]).QueryInterface(IExtendedProperty, ExtProp));
    case ExtProp.PropID of
      CAPICOM_PROPID_SUBJECT_PUBLIC_KEY_MD5_HASH:
      begin
        Info := Info + 'Chave p�blica (MD5):' + BR +
                Util.BinaryToHex(ExtProp.Value[CAPICOM_ENCODE_BINARY])
                + BR + BR;
      end;
    end;
  end;

end;

procedure TFmCapicomListas.UsosDaChave(Certificado: ICertificate2; var Info: String);
var
  Ind           : Integer;
  UsoDaChave    : IKeyUsage;
  EKU           : IEKU;
const
  BR         = sLineBreak;
  BoolVal    : array[Boolean] of string = ('N�o', 'Sim');
begin

  Info := Info + 'USO DA CHAVE:' + BR + BR;

  OleCheck(IDispatch(Certificado.KeyUsage).QueryInterface(IKeyUsage, UsoDaChave));

//  Certificado.BasicConstraints.

  if ( UsoDaChave.IsPresent ) then
  begin
    Info := Info +
       'Marcado como critico: ' +
       BoolVal[UsoDaChave.IsCritical] + BR +
       'Assinatura digital: ' +
       BoolVal[UsoDaChave.IsDigitalSignatureEnabled] + BR +
       'N�o-repudio: ' +
       BoolVal[UsoDaChave.IsNonRepudiationEnabled]   + BR +
       'Cifragem de chaves: ' +
       BoolVal[UsoDaChave.IsKeyEnciphermentEnabled]  + BR +
       'Cifragem de dados: ' +
       BoolVal[UsoDaChave.IsDataEnciphermentEnabled] + BR +
       'Acordo de chaves: ' +
       BoolVal[UsoDaChave.IsKeyAgreementEnabled]     + BR +
       'Assinatura de certificados: ' +
       BoolVal[UsoDaChave.IsKeyCertSignEnabled]      + BR +
       'Assinatura de CRLs: ' +
       BoolVal[UsoDaChave.IsCRLSignEnabled]          + BR +
       'Somente cifragem: ' +
       BoolVal[UsoDaChave.IsEncipherOnlyEnabled]     + BR +
       'Somente decifragem: ' +
       BoolVal[UsoDaChave.IsDecipherOnlyEnabled]     + BR + BR;
  end;

  Info := Info + 'USO EXTENDIDO DA CHAVE:' + BR + BR;

  for Ind := 1 to Certificado.ExtendedKeyUsage.EKUs.Count do
  begin
    if ( Certificado.ExtendedKeyUsage.IsPresent ) then
    begin
      OleCheck(IDispatch(Certificado.ExtendedKeyUsage.EKUs.Item[Ind]).QueryInterface(IEKU, EKU));
      case EKU.Name of
        CAPICOM_EKU_OTHER:
        Info := Info + 'Outro (' + EKU.OID + ')' + BR;
        CAPICOM_EKU_SERVER_AUTH: Info :=
        Info + 'Autent. de servidor (' + EKU.OID + ')' + BR;
        CAPICOM_EKU_CLIENT_AUTH:
        Info := Info + 'Autent. de cliente (' + EKU.OID + ')' + BR;
        CAPICOM_EKU_CODE_SIGNING:
        Info := Info + 'Assinatura de c�digo (' + EKU.OID + ')' + BR;
        CAPICOM_EKU_EMAIL_PROTECTION:
        Info := Info + 'Prote��o de e-mail (' + EKU.OID + ')' + BR;
        CAPICOM_EKU_SMARTCARD_LOGON:
        Info := Info + 'Smart card logon (' + EKU.OID + ')' + BR;
        CAPICOM_EKU_ENCRYPTING_FILE_SYSTEM:
        Info := Info + 'EFS (' + EKU.OID + ')' + BR;
      end;
    end;
  end;

end;

{  Desabilitado porque n�o usa
procedure TFmCapicomListas.RestricoesBasicasInfo(Certificado: ICertificate2; var Info: String);
const
  BR         = sLineBreak;
  BoolVal    : array[Boolean] of string = ('N�o', 'Sim');
begin

  if ( Certificado.BasicConstraints.IsPresent ) then
  begin
    with Certificado do
    begin
      Info := Info +
        'Cr�tica: ' +
        BoolVal[BasicConstraints.IsCritical]                 + BR +
        'CA:' +
        BoolVal[BasicConstraints.IsCertificateAuthority]     + BR +
        'PathLen presente: ' +
        BoolVal[BasicConstraints.IsPathLenConstraintPresent] + BR +
        'PathLen: ' +
        IntToStr(BasicConstraints.PathLenConstraint)         + BR;
     end;
  end;
end;
}

procedure TFmCapicomListas.BtRemoverClick(Sender: TObject);
var
  CertStore  : IStore3;
  NomeStore  : String;
  Certificado: ICertificate2;
  Ind        : Integer;
  ListView   : TListView;
begin
  ListView := nil;
  //
  case PageControl.ActivePageIndex of
    0:
    begin
      NomeStore := 'My';
      ListView  := LVPessoal;
    end;
    1:
    begin
      NomeStore := 'AddressBook';
      ListView  := LVOutras;
    end;
    2:
    begin
      NomeStore := 'CA';
      ListView  := LVIntermediarias;
    end;
    3:
    begin
      NomeStore := 'Root';
      ListView  := LVRaiz;
    end;
  end;

  if ( ListView.ItemIndex >= 0 ) then
  begin
    Ind := ListView.ItemIndex + 1;

    CertStore := CoStore.Create;

    CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);

    OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));
    CertStore.Remove(Certificado);

    cmbStoresChange(Sender);
  end;
end;

procedure TFmCapicomListas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCapicomListas.ExportarRepositrioDeCertificadosClick(Sender: TObject);
var
  CertStore   : IStore3;
  StoreStr    : WideString;
  NomeStore   : String;
  OutPut      : TStringList;
  Dados       : OleVariant;
  Util        : IUtilities;
  SaveAs      : CAPICOM_STORE_SAVE_AS_TYPE;
begin

  case PageControl.ActivePageIndex of
    0: NomeStore := 'My';
    1: NomeStore := 'AddressBook';
    2: NomeStore := 'CA';
    3: NomeStore := 'Root';
  end;

  if ( SaveDialog.Execute ) then
  begin
    CertStore   := CoStore.Create;
    CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);

    SaveAs := CAPICOM_STORE_SAVE_AS_SERIALIZED;
    if ( MessageDlg('Salvar como PKCS#7?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      SaveAs := CAPICOM_STORE_SAVE_AS_PKCS7;
    end;

    if ( MessageDlg('Salvar como Base64?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      StoreStr    := CertStore.Export(SaveAs, CAPICOM_ENCODE_BASE64);
      OutPut      := TStringList.Create;
      OutPut.Text := StoreStr;
      OutPut.SaveToFile(SaveDialog.FileName + '.p7b');
      OutPut.Free;
    end
    else
    begin
      Util     := CoUtilities.Create;
      StoreStr := CertStore.Export(SaveAs, CAPICOM_ENCODE_BINARY);
      Dados    := Util.BinaryStringToByteArray( StoreStr );
      SaveBinFile( SaveDialog.FileName + '.p7b', Dados );
    end;
  end;

end;

procedure TFmCapicomListas.SaveBinFile(FileName: String; BinArray: OleVariant);
var
  Stream               : OleVariant;
Const
  adSaveCreateOverWrite = 2;
  adTypeBinary          = 1;
  adModeReadWrite       = 3;
begin
  Stream      := CreateOleObject('ADODB.Stream');
  Stream.type := adTypeBinary;
  Stream.mode := adModeReadWrite;
  Stream.Open;
  Stream.write(BinArray);
  Stream.SaveToFile(FileName, adSaveCreateOverWrite);
  Stream.Close;
end;

function TFmCapicomListas.CarregarArquivo(NomeArquivo: String;
  out Dados: WideString): Boolean;
var
  FileStream : TFileStream;
  DadosStr   : String;

begin

  Result := True;
  try

    FileStream := TFileStream.Create(NomeArquivo, fmOpenRead, fmShareExclusive);
    SetLength(DadosStr, FileStream.Size);
    FileStream.Read(DadosStr[1], FileStream.Size);
    FileStream.Free;
    Dados := DadosStr;

  except

    on E: Exception do
    begin
      Result := False;
      MessageDlg(E.Message, mtError, [mbOk], 0);
    end;

  end;

end;

procedure TFmCapicomListas.AssinarDocumentoClick(Sender: TObject);
var
  CertStore     : IStore3;
  Certificado   : ICertificate2;
  Atributo      : IAttribute;
  Assinante     : ISigner2;
  SignedData    : ISignedData;
  Util          : IUtilities;
  Ind           : Integer;
  Desatachado   : Boolean;
  Documento     : WideString;
  Assinatura    : WideString;
begin

  if ( LVPessoal.ItemIndex < 0 ) then
    Exit;

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  if ( not ( SaveDialog.Execute ) ) then
    Exit;

  Desatachado := True;
  if ( MessageDlg('Assinatura atachada ao documento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes ) then
    Desatachado := False;

  if ( CarregarArquivo(odlgCertificados.FileName, Documento) ) then
  begin

    CertStore := CoStore.Create;
    CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_READ_ONLY);

    Ind := LVPessoal.ItemIndex + 1;
    OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));

    if ( not( ChecarCertAssinatura(Certificado) ) ) then
    begin
      MessageDlg('Escolha outro certificado.', mtError, [mbOk], 0);
      Exit;
    end;

    Assinante             := CoSigner.Create;
    Assinante.Certificate := Certificado;
    Assinante.Options     := CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY;//CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN;//

    Atributo              := coAttribute.Create;
    Atributo.Name         := CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
    Atributo.Value        := Now;
    Assinante.AuthenticatedAttributes.Add( Atributo );

    Atributo              := coAttribute.Create;
    Atributo.Name         := CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
    Atributo.Value        := ExtractFileName( odlgCertificados.FileName );
    Assinante.AuthenticatedAttributes.Add( Atributo );

    SignedData            := CoSignedData.Create;
    SignedData.Content    := Documento;

    Assinatura := SignedData.Sign( Assinante, Desatachado, CAPICOM_ENCODE_BINARY);

    Util := CoUtilities.Create;
    SaveBinFile(SaveDialog.FileName, Util.BinaryStringToByteArray(Assinatura));

  end;

end;

procedure TFmCapicomListas.BitBtn1Click(Sender: TObject);
var
  CertStore  : IStore3;
  NomeStore  : String;
  Certificado: ICertificate2;
  Ind        : Integer;
begin
  case PageControl.ActivePageIndex of
    0:
    begin
      Ind := LVPessoal.ItemIndex + 1;
      NomeStore := 'My';
    end;
    1:
    begin
      Ind := LVOutras.ItemIndex + 1;
      NomeStore := 'AddressBook';
    end;
    2:
    begin
      Ind := LVIntermediarias.ItemIndex + 1;
      NomeStore := 'CA';
    end;
    3:
    begin
      Ind := LVRaiz.ItemIndex + 1;
      NomeStore := 'Root';
    end;
    else begin
      Ind := -1;
      NomeStore := '???';
    end;
  end;
  if Ind = 0 then
  begin
    Geral.MensagemBox('Nenhum certificado foi selecionado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  CertStore := CoStore.Create;
  CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);
  OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));
  //
  FSerialNumber := Certificado.SerialNumber;
  FValidToDate  := Certificado.ValidToDate;
  //
  CertStore := nil;
  //
  if FSerialNumber <> '' then
  begin
    FSelected := True;
    Close;
  end;
end;

procedure TFmCapicomListas.BtExportarClick(Sender: TObject);
var
  CertStore  : IStore3;
  NomeStore  : String;
  Certificado: ICertificate2;
  Ind        : Integer;
  ListView   : TListView;
  SaveAs     : CAPICOM_CERTIFICATE_SAVE_AS_TYPE;
  Senha      : String;
  Ext        : String;
begin
  ListView := nil;
  //
  SaveAs            := CAPICOM_CERTIFICATE_SAVE_AS_CER;
  SaveDialog.Filter := '*.CER|*.cer';
  Ext               := '.cer';
  Senha             := '';
  case PageControl.ActivePageIndex of
    0:
    begin
      NomeStore := 'My';
      ListView  := LVPessoal;
      SaveAs            := CAPICOM_CERTIFICATE_SAVE_AS_PFX;
      SaveDialog.Filter := '*.PFX|*.pfx';
      Ext               := '.pfx';
    end;
    1:
    begin
      NomeStore := 'AddressBook';
      ListView  := LVOutras;
    end;
    2:
    begin
      NomeStore := 'CA';
      ListView  := LVIntermediarias;
    end;
    3:
    begin
      NomeStore := 'Root';
      ListView  := LVRaiz;
    end;
  end;

  if ( ListView.ItemIndex >= 0 ) then
  begin
    Ind := ListView.ItemIndex + 1;

    CertStore := CoStore.Create;

    CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore,
                   CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or
                   CAPICOM_STORE_OPEN_EXISTING_ONLY);

    OleCheck(
     IDispatch(
      CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2,
                                                       Certificado));

    if ( SaveDialog.Execute ) then
    begin
      if( SaveAs = CAPICOM_CERTIFICATE_SAVE_AS_PFX ) then
        InputQuery('Senha', 'Digite a senha do arquivo:', Senha);

      try
        Certificado.Save(SaveDialog.FileName + Ext,
                         Senha, SaveAs, 
                         CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN);
      finally
        if ( Length(Senha) > 0 ) then
          ZeroMemory(Addr(Senha), Length(Senha));
      end;
    end;

  end;

  SaveDialog.Filter := '';
end;

procedure TFmCapicomListas.BtFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCapicomListas.BtImportarClick(Sender: TObject);
var
  CertStore  : IStore3;
  NomeStore  : String;
  Senha      : String;
begin

  case PageControl.ActivePageIndex of
    0: NomeStore := 'My';
    1: NomeStore := 'AddressBook';
    2: NomeStore := 'CA';
    3: NomeStore := 'Root';
  end;

  if ( odlgCertificados.Execute ) then
  begin
    CertStore := CoStore.Create;
    CertStore.Open(CAPICOM_CURRENT_USER_STORE, NomeStore, CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED or CAPICOM_STORE_OPEN_EXISTING_ONLY);
    InputQuery('Senha', 'Digite a senha do arquivo:', Senha);
    try
      CertStore.Load(odlgCertificados.FileName, Senha, CAPICOM_KEY_STORAGE_EXPORTABLE);
    finally
      if ( Length(Senha) > 0 ) then
        ZeroMemory(Addr(Senha), Length(Senha));
      //ZeroMemory(@Senha[1], Length(Senha));
    end;
  end;

  cmbStoresChange(Sender);

  CertStore := nil;
end;

function TFmCapicomListas.ChecarCertAssinatura(
  Certificado: ICertificate2): boolean;
var
  UsoDaChave : IKeyUsage;
begin

  OleCheck(IDispatch(Certificado.KeyUsage).QueryInterface(IKeyUsage, UsoDaChave));

  Result := Certificado.HasPrivateKey            and
            UsoDaChave.IsPresent                 and
            UsoDaChave.IsDigitalSignatureEnabled and
            UsoDaChave.IsNonRepudiationEnabled;
end;

procedure TFmCapicomListas.CoAssinarDocumentoClick(Sender: TObject);
var
  CertStore     : IStore3;
  Certificado   : ICertificate2;
  Atributo      : IAttribute;
  Assinante     : ISigner2;
  SignedData    : ISignedData;
  Util          : IUtilities;
  Ind           : Integer;
  Desatachado   : Boolean;
  ArquivoAss    : String;
  Documento     : WideString;
  Assinatura    : WideString;
  Pkcs7         : TDinByteArray;
begin

  if ( LVPessoal.ItemIndex < 0 ) then
    Exit;

  Desatachado := True;
  if ( MessageDlg('Co-assinatura atachada ao documento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes ) then
    Desatachado := False;

  if not( odlgCertificados.Execute ) then
    Exit;

  if not( CarregarArquivo(odlgCertificados.FileName, Pkcs7) ) then
    Exit;

  ArquivoAss := odlgCertificados.FileName;

  if ( Desatachado ) then
  begin
    if not( odlgCertificados.Execute ) then
      Exit;
    if not( CarregarArquivo(odlgCertificados.FileName, Documento) ) then
      Exit;
  end;

  CertStore := CoStore.Create;
  CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_READ_ONLY);

  Ind := LVPessoal.ItemIndex + 1;
  OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));

  if not( ChecarCertAssinatura(Certificado) ) then
  begin
    MessageDlg('Escolha outro certificado.', mtError, [mbOk], 0);
    Exit;
  end;

  Assinante             := CoSigner.Create;
  Assinante.Certificate := Certificado;
  Assinante.Options     := CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY;//CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN;//

  Atributo              := coAttribute.Create;
  Atributo.Name         := CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
  Atributo.Value        := Now;
  Assinante.AuthenticatedAttributes.Add( Atributo );

  Atributo              := coAttribute.Create;
  Atributo.Name         := CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
  Atributo.Value        := ExtractFileName( odlgCertificados.FileName );
  Assinante.AuthenticatedAttributes.Add( Atributo );

  SignedData            := CoSignedData.Create;
  if ( Desatachado ) then
  begin
    SignedData.Content  := Documento;    
  end;

  Util       := CoUtilities.Create;
  Assinatura := Util.ByteArrayToBinaryString(Pkcs7);

  SignedData.Verify(Assinatura, Desatachado, CAPICOM_VERIFY_SIGNATURE_ONLY);

  Assinatura := SignedData.CoSign( Assinante, CAPICOM_ENCODE_BINARY );

  SaveBinFile(ArquivoAss, Util.BinaryStringToByteArray(Assinatura));

end;

function TFmCapicomListas.CarregarArquivo(NomeArquivo: String;
  var Dados: TDinByteArray): Boolean;
var
  FileStream  : TFileStream;
begin

  Result := True;
  try

    FileStream  := TFileStream.Create(NomeArquivo, fmOpenRead, fmShareExclusive);
    SetLength(Dados, FileStream.Size);
    FileStream.Read(Dados[0], FileStream.Size);
    FileStream.Free;

  except

    on E: Exception do
    begin
      Result := False;
      MessageDlg(E.Message, mtError, [mbOk], 0);
    end;

  end;
end;

procedure TFmCapicomListas.AssinarCodigoClick(Sender: TObject);
var
  CertStore     : IStore3;
  Certificado   : ICertificate2;
  Atributo      : IAttribute;
  Assinante     : ISigner2;
  SignedCode    : ISignedCode;
  Ind           : Integer;
begin

  if ( LVPessoal.ItemIndex < 0 ) then
    Exit;

  odlgCertificados.Filter := 'EXE|*.exe|DLL|*.dll|OCX|*.ocx|' +
                             'VBS|*.vbs|CAB|*.cab|CAT|*.cat';

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  CertStore := CoStore.Create;
  CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_READ_ONLY);

  Ind := LVPessoal.ItemIndex + 1;
  OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));

  Assinante             := CoSigner.Create;
  Assinante.Certificate := Certificado;
  Assinante.Options     := CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY;//CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN;//

  Atributo              := coAttribute.Create;
  Atributo.Name         := CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
  Atributo.Value        := Now;
  Assinante.AuthenticatedAttributes.Add( Atributo );

  Atributo              := coAttribute.Create;
  Atributo.Name         := CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
  Atributo.Value        := ExtractFileName( odlgCertificados.FileName );
  Assinante.AuthenticatedAttributes.Add( Atributo );

  SignedCode                := CoSignedCode.Create;
  SignedCode.FileName       := odlgCertificados.FileName;
  SignedCode.Description    := 'Aplicativo Win32';
  SignedCode.DescriptionURL := 'http://www.minhaempresa.com/descricao.pdf';

  //Assinante com EKU contendo 1.3.6.1.5.5.7.3.3
  SignedCode.Sign( Assinante );

  odlgCertificados.Filter := '';

end;

procedure TFmCapicomListas.VerificarsAssinaturaDeDocumentoClick(Sender: TObject);
var
  SignedData    : ISignedData;
  Util          : IUtilities;
  Certificado   : ICertificate;
  CertErro      : ICertificate;
  Cadeia        : IChain2;
  Desatachado   : Boolean;
  Documento     : WideString;
  Assinatura    : WideString;
  Pkcs7         : TDinByteArray;
  Ind,
  IndChain      : Integer;
  Assinante     : ISigner2;
begin

  Desatachado := True;
  if ( MessageDlg('Assinatura atachada ao documento?', mtConfirmation, [mbYes, mbNo], 0) = mrYes ) then
    Desatachado := False;

  if not( odlgCertificados.Execute ) then
    Exit;

  if not( CarregarArquivo(odlgCertificados.FileName, Pkcs7) ) then
    Exit;

  if ( Desatachado ) then
  begin
    if not( odlgCertificados.Execute ) then
      Exit;
    if not( CarregarArquivo(odlgCertificados.FileName, Documento) ) then
      Exit;
  end;

  SignedData            := CoSignedData.Create;
  if ( Desatachado ) then
  begin
    SignedData.Content  := Documento;
  end;

  Util       := CoUtilities.Create;
  Assinatura := Util.ByteArrayToBinaryString(Pkcs7);

  SignedData.Verify(Assinatura, Desatachado, CAPICOM_VERIFY_SIGNATURE_ONLY);

  for Ind := 1 to SignedData.Signers.Count do
  begin
    OleCheck(IDispatch(SignedData.Signers.Item[Ind]).QueryInterface(ISigner2, Assinante));
    Certificado := Assinante.Certificate;
    Cadeia      := CoChain.Create;
    if ( not Cadeia.Build(Certificado) ) then
    begin
      for IndChain := 1 to Cadeia.Certificates.Count do
      begin
        if ( Cadeia.Status[IndChain] <> 0 ) then
        begin
          MessageDlg('Problemas com certificado.' + IdErro(Cadeia.Status[IndChain]), mtError, [mbOk], 0);
          OleCheck(IDispatch(Cadeia.Certificates.Item[IndChain]).QueryInterface(ICertificate2, CertErro));
          CertErro.Display;
        end;
      end;
    end;
    Cadeia := nil;
  end;

end;

procedure TFmCapicomListas.CriptografarArquivoClick(Sender: TObject);
var
  EncryptedData: IEncryptedData;
  Dados        : WideString;
  DadosCrypt   : String;
  Senha        : String;
  MemStream    : TMemoryStream;
begin

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  if ( not ( SaveDialog.Execute ) ) then
    Exit;

  CarregarArquivo(odlgCertificados.FileName, Dados);
  EncryptedData                     := CoEncryptedData.Create;
  EncryptedData.Content             := Dados;
  EncryptedData.Algorithm.Name      := SelCriptAlgo;
  EncryptedData.Algorithm.KeyLength := SelCriptKeyLen;
  
  InputQuery('Senha', 'Senha:', Senha);

  EncryptedData.SetSecret(Senha, CAPICOM_SECRET_PASSWORD);
  ZeroMemory(Addr(Senha), Length(Senha));
  DadosCrypt                        := EncryptedData.Encrypt(CAPICOM_ENCODE_BASE64);

  MemStream    := TMemoryStream.Create;
  MemStream.Write(DadosCrypt[1], Length(DadosCrypt));
  MemStream.SaveToFile(SaveDialog.FileName);
  MemStream.Clear;
  MemStream.Free;

end;

function TFmCapicomListas.IdErro(Id: Integer): String;
begin

  case Id of
    0:                                               Result := 'OK';
    CAPICOM_TRUST_IS_NOT_TIME_VALID:                 Result := 'Per�odo inv�lido';
    CAPICOM_TRUST_IS_NOT_TIME_NESTED:                Result := 'Per�odo n�o aninhado';
    CAPICOM_TRUST_IS_REVOKED:                        Result := 'Revogado';
    CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID:            Result := 'Assinatura inv�lida';
    CAPICOM_TRUST_IS_NOT_VALID_FOR_USAGE:            Result := 'Inv�lido para uso';
    CAPICOM_TRUST_IS_UNTRUSTED_ROOT:                 Result := 'Raiz n�o confi�vel';
    CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN:         Result := 'Revoga��o n�o determinada';
    CAPICOM_TRUST_IS_CYCLIC:                         Result := 'Ciclico';
    CAPICOM_TRUST_INVALID_EXTENSION:                 Result := 'Extens�o inv�lida';
    CAPICOM_TRUST_INVALID_POLICY_CONSTRAINTS:        Result := 'inv�lido - Policy Constraint';
    CAPICOM_TRUST_INVALID_BASIC_CONSTRAINTS:         Result := 'inv�lido - Basic Constraint';
    CAPICOM_TRUST_INVALID_NAME_CONSTRAINTS:          Result := 'inv�lido - Name Constraint';
    CAPICOM_TRUST_HAS_NOT_SUPPORTED_NAME_CONSTRAINT: Result := 'Name Constraint n�o suportada';
    CAPICOM_TRUST_HAS_NOT_DEFINED_NAME_CONSTRAINT:   Result := 'Name Constraint n�o definida';
    CAPICOM_TRUST_HAS_NOT_PERMITTED_NAME_CONSTRAINT: Result := 'Name Constraint n�o permitida';
    CAPICOM_TRUST_HAS_EXCLUDED_NAME_CONSTRAINT:      Result := 'Name Constraint exclu�da';
    CAPICOM_TRUST_IS_OFFLINE_REVOCATION:             Result := 'Revogado (off-line)';
    CAPICOM_TRUST_NO_ISSUANCE_CHAIN_POLICY:          Result := 'TRUST_NO_ISSUANCE_CHAIN_POLICY';
    CAPICOM_TRUST_IS_PARTIAL_CHAIN:                  Result := 'Cadeia parcial';
    CAPICOM_TRUST_CTL_IS_NOT_TIME_VALID:             Result := 'CTL expirada';
    CAPICOM_TRUST_CTL_IS_NOT_SIGNATURE_VALID:        Result := 'Assinatura da CTL inv�lida';
    CAPICOM_TRUST_CTL_IS_NOT_VALID_FOR_USAGE:        Result := 'CTL n�o v�lida para uso';
    else
      Result := 'Problema desconhecido';
  end;
  
end;

procedure TFmCapicomListas.VerificarsAssinaturaDeCodigoClick(Sender: TObject);
var
  SignedCode    : ISignedCode;
begin

  odlgCertificados.Filter := 'EXE|*.exe|DLL|*.dll|OCX|*.ocx|' +
                             'VBS|*.vbs|CAB|*.cab|CAT|*.cat';

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  SignedCode                := CoSignedCode.Create;
  SignedCode.FileName       := odlgCertificados.FileName;

  SignedCode.Verify(False);
  SignedCode.Signer.Certificate.Display;

  odlgCertificados.Filter := '';

end;

procedure TFmCapicomListas.CalcularClick(Sender: TObject);
var
  HashedData: IHashedData;
  Dados     : WideString;
  AlgoHash  : CAPICOM_HASH_ALGORITHM;
begin

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  if ( SHA1.Checked ) then
    AlgoHash := CAPICOM_HASH_ALGORITHM_SHA1
  else
  if ( MD2.Checked ) then
    AlgoHash := CAPICOM_HASH_ALGORITHM_MD2
  else
  if ( MD4.Checked ) then
    AlgoHash := CAPICOM_HASH_ALGORITHM_MD4
  else
  if ( MD5.Checked ) then
    AlgoHash := CAPICOM_HASH_ALGORITHM_MD5
  else
  if ( SHA256.Checked ) then
    AlgoHash := CAPICOM_HASH_ALGORITHM_SHA_256
  else
  if ( SHA384.Checked ) then
    AlgoHash := CAPICOM_HASH_ALGORITHM_SHA_384
  else
    AlgoHash := CAPICOM_HASH_ALGORITHM_SHA_512;

  CarregarArquivo(odlgCertificados.FileName, Dados);

  HashedData           := CoHashedData.Create;
  HashedData.Algorithm := AlgoHash;
  HashedData.Hash(Dados);

  MessageDlg('Message digest:'#13 + HashedData.Value,
             mtInformation, [mbOk], 0);

end;

procedure TFmCapicomListas.DescriptografarArquivoClick(Sender: TObject);
var
  EncryptedData: IEncryptedData;
  Dados        : WideString;
  DadosDecrypt : String;
  Senha        : String;
  MemStream    : TMemoryStream;
begin

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  if ( not ( SaveDialog.Execute ) ) then
    Exit;

  CarregarArquivo(odlgCertificados.FileName, Dados);

  InputQuery('Senha', 'Senha:', Senha);

  EncryptedData := CoEncryptedData.Create;
  EncryptedData.SetSecret(Senha, CAPICOM_SECRET_PASSWORD);
  ZeroMemory(Addr(Senha), Length(Senha));
  EncryptedData.Decrypt(Dados);
  DadosDecrypt := EncryptedData.Content;

  MemStream    := TMemoryStream.Create;
  MemStream.Write(DadosDecrypt[1], Length(DadosDecrypt));
  MemStream.SaveToFile(SaveDialog.FileName);
  MemStream.Clear;
  MemStream.Free;

end;

procedure TFmCapicomListas.CriarEnvelopeClick(Sender: TObject);
var
  EnvData     : IEnvelopedData;
  CertStore   : IStore3;
  Certificado : ICertificate2;
  Ind         : Integer;
  Dados       : WideString;
  Envelope    : String;
  MemStream   : TMemoryStream;
begin

  if ( LVOutras.ItemIndex < 0 ) then
    Exit;

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  if ( not ( SaveDialog.Execute ) ) then
    Exit;

  CarregarArquivo(odlgCertificados.FileName, Dados);

  CertStore := CoStore.Create;
  CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'AddressBook', CAPICOM_STORE_OPEN_READ_ONLY);

  Ind := LVOutras.ItemIndex + 1;
  OleCheck(IDispatch(CertStore.Certificates.Item[Ind]).QueryInterface(ICertificate2, Certificado));

  EnvData                     := CoEnvelopedData.Create;
  EnvData.Algorithm.Name      := SelCriptAlgo;
  EnvData.Algorithm.KeyLength := SelCriptKeyLen;
  EnvData.Content             := Dados;
  EnvData.Recipients.Add(Certificado);
  
  Envelope := EnvData.Encrypt(CAPICOM_ENCODE_BASE64);

  MemStream    := TMemoryStream.Create;
  MemStream.Write(Envelope[1], Length(Envelope));
  MemStream.SaveToFile(SaveDialog.FileName);
  MemStream.Clear;
  MemStream.Free;
  
end;

function TFmCapicomListas.SelCriptAlgo: CAPICOM_ENCRYPTION_ALGORITHM;
begin

  if ( RC2.Checked ) then
    Result := CAPICOM_ENCRYPTION_ALGORITHM_RC2
  else
  if ( RC4.Checked ) then
    Result := CAPICOM_ENCRYPTION_ALGORITHM_RC4
  else
  if ( DES.Checked ) then
    Result := CAPICOM_ENCRYPTION_ALGORITHM_DES
  else
  if ( TripleDES.Checked ) then
    Result := CAPICOM_ENCRYPTION_ALGORITHM_3DES
  else
    Result := CAPICOM_ENCRYPTION_ALGORITHM_AES;
    
end;

function TFmCapicomListas.SelCriptKeyLen: CAPICOM_ENCRYPTION_KEY_LENGTH;
begin

  if ( Maximo.Checked ) then
    Result := CAPICOM_ENCRYPTION_KEY_LENGTH_MAXIMUM
  else
  if ( Tam40.Checked ) then
    Result := CAPICOM_ENCRYPTION_KEY_LENGTH_40_BITS
  else
  if ( Tam56.Checked ) then
    Result := CAPICOM_ENCRYPTION_KEY_LENGTH_56_BITS
  else
  if ( Tam128.Checked ) then
    Result := CAPICOM_ENCRYPTION_KEY_LENGTH_128_BITS
  else
  if ( Tam192.Checked ) then
    Result := CAPICOM_ENCRYPTION_KEY_LENGTH_192_BITS
  else
    Result := CAPICOM_ENCRYPTION_KEY_LENGTH_256_BITS;

end;

procedure TFmCapicomListas.AbrirEnvelopeClick(Sender: TObject);
var
  EnvData     : IEnvelopedData;
  Envelope    : WideString;
  Dados       : String;
  MemStream   : TMemoryStream;
begin

  if ( not ( odlgCertificados.Execute ) ) then
    Exit;

  if ( not ( SaveDialog.Execute ) ) then
    Exit;

  CarregarArquivo(odlgCertificados.FileName, Envelope);

  EnvData := CoEnvelopedData.Create;
  EnvData.Decrypt(Envelope);

  Dados   := EnvData.Content;

  MemStream    := TMemoryStream.Create;
  MemStream.Write(Dados[1], Length(Dados));
  MemStream.SaveToFile(SaveDialog.FileName);
  MemStream.Clear;
  MemStream.Free;

end;

end.
