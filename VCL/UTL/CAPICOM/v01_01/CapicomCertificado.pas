///////////////////////////////////////////////////////////////////////
//
//  Este c�digo fonte � parte do livro
//  Assinatura Digital - Solu��o Delphi e CAPICOM.
//  Autor: Marcelo Lima (www.marcelolima.eti.br)
//  Editora: Visual Books
//  ISBN: 8575021710
//  Ano: 2005
//  Edi��o: 1
//  P�ginas: 162
//
//  O c�digo � fornecido da maneira que est� e o autor
//  n�o se responsabiliza pelo seu uso para qualquer
//  que seja a finalidade (do c�digo inteiro o de parte dele).
//  O autor n�o se responsabiliza por este c�digo, caso
//  ele n�o tenha sido obtido junto a editora ou com o
//  pr�prio autor.
//  N�o � intuito do autor fornecer  um c�digo que seja exemplo
//  de modelagem e formata��o, o objetivo � fornecer um c�digo
//  dit�tico e que demonstre claramente as principais funcionalidades
//  da CAPICOM.
//  Os c�digos deste exemplo est�o devidamente explicados
//  no livro Assinatura Digital - Solu��o Delphi e CAPICOM.
//
//  A unit CAPICOM_TLB.pas n�o � de autoria do autor. Esta unit
//  � criada de maneira autom�tica pelo Delphi a partir do
//  m�dulo CAPICOM.dll.
//
///////////////////////////////////////////////////////////////////////
unit CapicomCertificado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, dmkGeral, Buttons, dmkImage, UnDmkEnums;

type
  TFmCapicomCertificado = class(TForm)
    memCertInfo: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtFechar: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmCapicomCertificado: TFmCapicomCertificado;

implementation

uses UnMyObjects;

{$R *.dfm}

procedure TFmCapicomCertificado.BtFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCapicomCertificado.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCapicomCertificado.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmCapicomCertificado.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
