unit CapicomGeral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts,
  // ACBr
  DMKpnfsConversao,
  // Microsoft
  CAPICOM_TLB, MSXML2_TLB, ComObj, StrUtils,
  // Dermatek
  dmkGeral;

type
  TCapicomGeral = class(TObject)
  private
    { Private declarations }
    function Teste(OK: Boolean; Sim, Nao: Variant): Variant;
  public
    { Public declarations }
    // CAPICOM
    function  ObtemCertificado(const NumeroSerial: String;
              out Cert: ICertificate2): Boolean;
    function  AssinarMSXML_NFSe_A(XML: WideString; Certificado: ICertificate2;
              Tipo: TNFSeTipoDoc; out XMLAssinado: WideString): Boolean;
    function AssinarMSXML_NFSe_B(XML : AnsiString;
                      Certificado : ICertificate2;
                      out XMLAssinado : WideString;
                      ALote: Boolean = False;
                      APrefixo3: string = '';
                      APrefixo4: string = '';
                      APrefixoL: string = '';
                      AProvedor: TnfseProvedor = proNenhum): Boolean;
// Alterado por Italo em 12/07/2012
    function AssinarMSXML_NFSe_C(XML : AnsiString;
                      Certificado : ICertificate2;
                      out XMLAssinado : AnsiString;
                      ALote: Boolean = False;
                      APrefixo3: string = '';
                      APrefixo4: string = '';
                      AProvedor: TnfseProvedor = proNenhum): Boolean;
  end;

//const

var
  UnCapicomGeral: TCapicomGeral;


const
  //INTERNET_OPTION_CLIENT_CERT_CONTEXT = 84;
  // assinatura
  DSIGNS = 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#"';

implementation

var
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;

{ TCapicomGeral }

function TCapicomGeral.Teste(OK: Boolean; Sim, Nao: Variant): Variant;
begin
  Result := Nao;
  if OK then
    Result := Sim;
end;

function TCapicomGeral.AssinarMSXML_NFSe_A(XML: WideString;
  Certificado: ICertificate2; Tipo: TNFSeTipoDoc; out XMLAssinado: WideString): Boolean;
var
 I, J, PosIni, PosFim : Integer;
 URI           : String ;
 xmlHeaderAntes, xmlHeaderDepois : WideString ;
 xmldoc  : IXMLDOMDocument3;
 xmldsig : IXMLDigitalSignature;
 dsigKey   : IXMLDSigKey;
 signedKey : IXMLDSigKey;
begin
  //if Pos('<Signature',XML) <= 0 then
  begin
    case Tipo of
      tdnfseDPS: I := pos('<InfDeclaracaoPrestacaoServico', XML);
      tdnfseLoteRps: I := pos('<LoteRps', XML);
    end;
    I := PosEx('Id=',XML,6) ;
    if I = 0 then
       raise Exception.Create('N�o encontrado in�cio do URI: Id=') ;
    I := PosEx('"',XML,I+2) ;
    if I = 0 then
       raise Exception.Create('N�o encontrado in�cio do URI: aspas inicial') ;
    J := PosEx('"',XML,I+1) ;
    if J = 0 then
       raise Exception.Create('N�o encontrado in�cio do URI: aspas final') ;

    URI := copy(XML,I+1,J-I-1) ;

    case Tipo of
      tdnfseDPS:
       XML := copy(XML, 1, pos('</InfDeclaracaoPrestacaoServico>',XML)-1) + '</InfDeclaracaoPrestacaoServico>';
      tdnfseLoteRps: XML := copy(XML, 1, pos('</EnviarLoteRpsSincronoEnvio>',XML)-1);
    end;

    XML := XML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />';
    XML := XML + '<Reference URI="#'+URI+'">';
    XML := XML + '<Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />';
    XML := XML + '<DigestValue></DigestValue></Reference></SignedInfo><SignatureValue></SignatureValue><KeyInfo></KeyInfo></Signature>';

    case Tipo of
      tdnfseDPS: XML := XML + '</Rps>';
      tdnfseLoteRps: XML := XML + '</EnviarLoteRpsSincronoEnvio>';
    end;

    //?
    end;

   // Lendo Header antes de assinar //
   xmlHeaderAntes := '' ;
   I := pos('?>',XML) ;
   if I > 0 then
      xmlHeaderAntes := copy(XML,1,I+1) ;

   xmldoc := CoDOMDocument50.Create;

   xmldoc.async              := False;
   xmldoc.validateOnParse    := False;
   xmldoc.preserveWhiteSpace := True;

   xmldsig := CoMXDigitalSignature50.Create;

   Geral.MensagemBox(XML, 'XML', MB_OK+MB_ICONINFORMATION);
   if (not xmldoc.loadXML(XML) ) then
   begin
     Geral.MensagemBox('N�o foi poss�vel carregar o arquivo: ' + #13#10 + XML, 'ERRO', MB_OK+MB_ICONERROR);
     raise Exception.Create('N�o foi poss�vel carregar o arquivo: '+XML);
   end;
   xmldoc.setProperty('SelectionNamespaces', DSIGNS);

   //
   xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');
   //
   //xmldsig.signature := xmldoc.selectSingleNode('Signature');

   if (xmldsig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   if NumCertCarregado <> Certificado.SerialNumber then
      CertStoreMem := nil;

   if  CertStoreMem = nil then
    begin
      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      for i:= 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
         begin
           CertStoreMem.Add(Cert);
           NumCertCarregado := Certificado.SerialNumber;
         end;
      end;
   end;

   OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey,PrivateKey));
   xmldsig.store := CertStoreMem;

   dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
   if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP. (assinatura digital)');

   signedKey := xmldsig.sign(dsigKey, $00000002);
   if (signedKey <> nil) then
    begin
      XMLAssinado := xmldoc.xml;
      XMLAssinado := StringReplace( XMLAssinado, #10, '', [rfReplaceAll] ) ;
      XMLAssinado := StringReplace( XMLAssinado, #13, '', [rfReplaceAll] ) ;
      PosIni := Pos('<SignatureValue>',XMLAssinado)+length('<SignatureValue>');
      XMLAssinado := copy(XMLAssinado,1,PosIni-1)+StringReplace( copy(XMLAssinado,PosIni,length(XMLAssinado)), ' ', '', [rfReplaceAll] ) ;
      PosIni := Pos('<X509Certificate>',XMLAssinado)-1;
      PosFim := Geral.PosLast('<X509Certificate>',XMLAssinado);

      XMLAssinado := copy(XMLAssinado,1,PosIni)+copy(XMLAssinado,PosFim,length(XMLAssinado));
    end
   else
     raise Exception.Create('Ocorreu uma falha ao tentar assinar o XML!');
      //raise Exception.Create('Assinatura Falhou.');
   if xmlHeaderAntes <> '' then
   begin
      I := pos('?>',XMLAssinado) ;
      if I > 0 then
       begin
         xmlHeaderDepois := copy(XMLAssinado,1,I+1) ;
         if xmlHeaderAntes <> xmlHeaderDepois then
            XMLAssinado := StuffString(XMLAssinado,1,length(xmlHeaderDepois),xmlHeaderAntes) ;
       end
      else
         XMLAssinado := xmlHeaderAntes + XMLAssinado ;
   end ;

   dsigKey   := nil;
   signedKey := nil;
   xmldoc    := nil;
   xmldsig   := nil;

   Result := True;
end;

function TCapicomGeral.AssinarMSXML_NFSe_B(XML : AnsiString;
                      Certificado : ICertificate2;
                      out XMLAssinado : WideString;
                      ALote: Boolean = False;
                      APrefixo3: string = '';
                      APrefixo4: string = '';
                      APrefixoL: string = '';
                      AProvedor: TnfseProvedor = proNenhum): Boolean;
var
 I,
 J,
 P,
 PosIni,
 PosFim,
 PosIniAssLote : Integer;

 URI, AID,
 Identificador : String;

 AXML,
 Assinatura,
 xmlHeaderAntes,
 xmlHeaderDepois : AnsiString;

 xmldoc    : IXMLDOMDocument3;
 xmldsig   : IXMLDigitalSignature;
 dsigKey   : IXMLDSigKey;
 signedKey : IXMLDSigKey;
begin
 AXML := XML;

 if ALote
  then begin
   Identificador := 'Id';
   I             := pos('LoteRps Id=', AXML);
   if I = 0
    then begin
     Identificador := 'id';
     I             := pos('LoteRps id=', AXML);
    end;
   if I = 0
    then begin
     Identificador := '';
     URI           := '';
    end
    else begin
     I := PosEx('"', AXML, I + 2);
     if I = 0
      then raise Exception.Create('N�o encontrei inicio do URI: aspas inicial');
     J := PosEx('"', AXML, I + 1);
     if J = 0
      then raise Exception.Create('N�o encontrei inicio do URI: aspas final');

     URI := copy(AXML, I + 1, J - I - 1);
    end;

//   if Identificador = 'id' then URI := '';

   AXML := copy(AXML, 1, pos('</'+ APrefixo3 + '' + APrefixoL + '>', AXML) - 1);

   if (URI = '') {or (AProvedor = profintelISS)}
    then AID := '>'
    else AID := ' '+Identificador+'="AssLote_'+ URI +'">';

   AXML := AXML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"'+ AID +
//                  SeSenao(URI = '', '>', ' '+Identificador+'="AssLote_'+ URI +'">') +
                 '<SignedInfo>'+
                  '<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>'+
                  '<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />'+
                  '<Reference URI="' + Geral.SeSenao(URI = '', '">', '#' + URI + '">') +
                   '<Transforms>'+
                    '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />'+
                   Geral.SeSenao(AProvedor = profintelISS, '',
                    '<Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />') +
                   '</Transforms>'+
                   '<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />'+
                   '<DigestValue></DigestValue>'+
                  '</Reference>'+
                 '</SignedInfo>'+
                 '<SignatureValue></SignatureValue>'+
                 '<KeyInfo>'+
                  '<X509Data>'+
                    '<X509Certificate></X509Certificate>'+
                  '</X509Data>'+
                 '</KeyInfo>'+
                '</Signature>';

   AXML := AXML + '</'+ APrefixo3 + '' + APrefixoL + '>';

  end
  else begin

   // Ao assinar um RPS a tag n�o possui prefixo
   APrefixo3 := '';

   if Pos('<Signature', AXML) <= 0
    then begin
     Identificador := 'Id';
     I             := pos('Id=', AXML);
     if I = 0
      then begin
       Identificador := 'id';
       I             := pos('id=', AXML);
       if I = 0
        then Identificador := '';
  //      raise Exception.Create('N�o encontrei inicio do URI: Id=');
      end;
     if I <> 0
      then begin
       I := PosEx('"', AXML, I + 2);
       if I = 0
        then raise Exception.Create('N�o encontrei inicio do URI: aspas inicial');
       J := PosEx('"', AXML, I + 1);
       if J = 0
        then raise Exception.Create('N�o encontrei inicio do URI: aspas final');

       URI := copy(AXML, I + 1, J - I - 1);
      end
      else URI := '';

     if (URI = '') or (AProvedor = profintelISS)
      then AID := '>'
      else AID := ' '+Identificador+'="Ass_'+ URI +'">';

     Assinatura := '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"' + AID +
//                        SeSenao(URI = '', '>', ' ' + Identificador + '="Ass_' + URI + '">') +
                    '<SignedInfo>' +
                    '<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>' +
                     '<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />' +
                     '<Reference URI="' + Geral.SeSenao(URI = '', '">', '#' + URI + '">') +
                      '<Transforms>' +
                       '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />' +
                       Geral.SeSenao(AProvedor = profintelISS, '',
                       '<Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />') +
                      '</Transforms>' +
                      '<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />' +
                      '<DigestValue></DigestValue>' +
                     '</Reference>' +
                    '</SignedInfo>' +
                    '<SignatureValue></SignatureValue>' +
                    '<KeyInfo>' +
                     '<X509Data>' +
                       '<X509Certificate></X509Certificate>' +
                     '</X509Data>' +
                    '</KeyInfo>' +
                   '</Signature>';

     case AProvedor of
      profintelISS,
      proSaatri:    begin
                     AXML := copy(AXML, 1, pos('</InfDeclaracaoPrestacaoServico>', AXML) - 1);
                     AXML := AXML + '</InfDeclaracaoPrestacaoServico>';
                     AXML := AXML + Assinatura;
                     AXML := AXML + '</Rps>';
                    end;
      else begin
            AXML := copy(AXML, 1, pos('</Rps>', AXML) - 1);
            AXML := AXML + Assinatura;
            AXML := AXML + '</Rps>';
           end;
     end;

    end;

  end;

 // Lendo Header antes de assinar //
 xmlHeaderAntes := '';
 I              := pos('?>', AXML);
 if I > 0
  then xmlHeaderAntes := copy(AXML, 1, I + 1);

 xmldoc := CoDOMDocument50.Create;

 xmldoc.async              := False;
 xmldoc.validateOnParse    := False;
 xmldoc.preserveWhiteSpace := True;

 xmldsig := CoMXDigitalSignature50.Create;

 //ShowMessage(IntToStr(pos('<');

 {
 Geral.MensagemBox(AXML, 'XML', MB_OK+MB_ICONINFORMATION);
 P := pos('<EnviarLote', AXML);
 ShowMessage(IntToStr(P));
 AXML := Copy(AXML, P);
 }
 AXML := Geral.Substitui(AXML, #13, '');
 AXML := Geral.Substitui(AXML, #10, '');
 Geral.MensagemBox(AXML, 'XML', MB_OK+MB_ICONINFORMATION);










 // AQUI � CARREGADO O XML!!!

 if (not xmldoc.loadXML(AXML))
  then raise Exception.Create('N�o foi poss�vel carregar o arquivo: ' + AXML);

 xmldoc.setProperty('SelectionNamespaces', DSIGNS);

 if ALote
  then begin
   if (URI <> '') {and (AProvedor <> profintelISS) }
    then xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature[@'+Identificador+'="AssLote_'+ URI +'"]')
    else xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');
//    else xmldsig.signature := xmldoc.selectSingleNode('.//' + APrefixoL + '//ds:Signature');
  end
  else xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');

 if (xmldsig.signature = nil)
  then raise Exception.Create('� preciso carregar o template antes de assinar.');

 if NumCertCarregado <> Certificado.SerialNumber
  then CertStoreMem := nil;

 if  CertStoreMem = nil
  then begin
   CertStore := CoStore.Create;
   CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

   CertStoreMem := CoStore.Create;
   CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

   Certs := CertStore.Certificates as ICertificates2;
   for i := 1 to Certs.Count do
    begin
     Cert := IInterface(Certs.Item[i]) as ICertificate2;
     if Cert.SerialNumber = Certificado.SerialNumber
      then begin
       CertStoreMem.Add(Cert);
        NumCertCarregado := Certificado.SerialNumber;
      end;
    end;
 end;

 OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey,PrivateKey));
 xmldsig.store := CertStoreMem;

 dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
 if (dsigKey = nil)
  then raise Exception.Create('Erro ao criar a chave do CSP.');

 signedKey := xmldsig.sign(dsigKey, $00000002);
 if (signedKey <> nil)
  then begin
   XMLAssinado := xmldoc.xml;

   // Teste! desmarquie!
   //XMLAssinado := StringReplace( XMLAssinado, #10, '', [rfReplaceAll] );
   //XMLAssinado := StringReplace( XMLAssinado, #13, '', [rfReplaceAll] );
   // FIM Teste!

 case AProvedor of
  profintelISS,
  proMaringa,
  proSaatri:    begin
                 //By Akai - L. Massao Aihara ==================================
                 //MUDA A ASSINATURA...
                 //para n�o criar uma variavel usei a XMLAssinado mesmo...
                 //como o acbr estava gerando algumas coisas que n�o tinha no exemplo da Prefeitura de PG
                 //so fiz um processo para remover o "excesso"!!!
                 if not ALote then
                  begin
                   AXML := copy(XML, 1, pos('</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>', XML) - 1);
                   AXML := AXML + '</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>';
                   I    := pos('<Signature', XMLAssinado);

                   XMLAssinado := copy(XMLAssinado, I, pos('</Signature>', XMLAssinado) - I);
                   I           := pos('>', XMLAssinado);
                   XMLAssinado := StringReplace(XMLAssinado, Copy(XMLAssinado, 54, I - 54), '', []);

//                   I           := pos('<KeyInfo>', XMLAssinado);
//                   XMLAssinado := StringReplace(XMLAssinado, copy(XMLAssinado, I, pos('</KeyInfo>', XMLAssinado) - I), '', []);
//                   XMLAssinado := StringReplace(XMLAssinado, '</KeyInfo>', '', []);

                   AXML := AXML + XMLAssinado;
                   AXML := AXML + '</Signature>';
                   AXML := AXML + '</Rps>';

                   XMLAssinado := AXML;
                  end
                  else
                  //Se for o LOTE...
                  //essa parte n�o precisava, ja a prefeitura ja estava retornando a msg "correta"...
                  //mas como no exemplo da prefeitura estava diferente, alterei tbm...
                  begin
                   AXML := copy(XML, 1, pos('</LoteRps>', XML) - 1);
                   AXML := AXML + '</LoteRps>';
                   I    := pos('</LoteRps>', XMLAssinado);

                   XMLAssinado := copy(XMLAssinado, I, pos('</' + APrefixoL + '>', XMLAssinado) - I);
                   XMLAssinado := StringReplace(XMLAssinado, '</LoteRps>', '', []);

                   // S� teste
                   {
                   I           := pos('>', XMLAssinado);
                   XMLAssinado := StringReplace(XMLAssinado, Copy(XMLAssinado, 54, I - 54), '', []);
                   }

//                   I           := pos('<KeyInfo>', XMLAssinado);
//                   XMLAssinado := StringReplace(XMLAssinado, copy(XMLAssinado, I, pos('</KeyInfo>', XMLAssinado) - I), '', []);
//                   XMLAssinado := StringReplace(XMLAssinado, '</KeyInfo>', '', []);

                   AXML := AXML + XMLAssinado;
                   AXML := AXML + '</' + APrefixoL + '>';

                   XMLAssinado := AXML;
                  end;
                 //Fim altera��o Akai ==========================================

                 (*
                 // Variavel XML contem o XML original de entrada na fun��o
                 // sem assinatura.
                 AXML := copy(XML, 1, pos('</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>', XML) - 1);
                 AXML := AXML + '</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>';
                 I    := pos('<Signature', XMLAssinado);
                 AXML := AXML + copy(XMLAssinado, I, pos('</Signature>', XMLAssinado) - I);
                 AXML := AXML + '</Signature>';
                 AXML := AXML + '</Rps>';

                 XMLAssinado := AXML;
                 *)
                end;
 end;



   if ALote
    then begin
     // Sugest�o de Rodrigo Cantelli
     PosIniAssLote := Pos('</'+ APrefixo3 + 'LoteRps>', XMLAssinado);

     if PosIniAssLote = 0
      then PosIniAssLote := Pos('</LoteRps>', XMLAssinado) + length('</LoteRps>')
      else PosIniAssLote := PosIniAssLote + length('</'+ APrefixo3 + 'LoteRps>');

     PosIni      := PosEx('<SignatureValue>', XMLAssinado, PosIniAssLote) + length('<SignatureValue>');
     XMLAssinado := copy(XMLAssinado, 1, PosIni - 1) +
                    StringReplace( copy(XMLAssinado, PosIni, length(XMLAssinado)), ' ', '', [rfReplaceAll] );

     // Sugest�o de Rodrigo Cantelli
     PosIniAssLote := Pos('</'+ APrefixo3 + 'LoteRps>', XMLAssinado);

     if PosIniAssLote = 0
      then PosIniAssLote := Pos('</LoteRps>', XMLAssinado) + length('</LoteRps>')
      else PosIniAssLote := PosIniAssLote + length('</'+ APrefixo3 + 'LoteRps>');

     PosIni      := PosEx('<X509Certificate>', XMLAssinado, PosIniAssLote) - 1;
     PosFim      := Geral.PosLast('<X509Certificate>', XMLAssinado);
     XMLAssinado := copy(XMLAssinado, 1, PosIni) +
                    copy(XMLAssinado, PosFim, length(XMLAssinado));
    end
    else begin
     PosIni      := Pos('<SignatureValue>', XMLAssinado) + length('<SignatureValue>');
     XMLAssinado := copy(XMLAssinado, 1, PosIni - 1) +
                    StringReplace( copy(XMLAssinado, PosIni, length(XMLAssinado)), ' ', '', [rfReplaceAll] );
     PosIni      := Pos('<X509Certificate>', XMLAssinado) - 1;
     PosFim      := Geral.PosLast('<X509Certificate>', XMLAssinado);
     XMLAssinado := copy(XMLAssinado, 1, PosIni) +
                    copy(XMLAssinado, PosFim, length(XMLAssinado));
    end;
  end
  else raise Exception.Create('Assinatura Falhou.');

 if xmlHeaderAntes <> ''
  then begin
   I := pos('?>',XMLAssinado);
   if I > 0
    then begin
     xmlHeaderDepois := copy(XMLAssinado,1,I+1);
     if xmlHeaderAntes <> xmlHeaderDepois
      then XMLAssinado := StuffString(XMLAssinado,1,length(xmlHeaderDepois),xmlHeaderAntes);
    end
    else XMLAssinado := xmlHeaderAntes + XMLAssinado;
  end;

 dsigKey   := nil;
 signedKey := nil;
 xmldoc    := nil;
 xmldsig   := nil;

 Result := True;
end;


function TCapicomGeral.AssinarMSXML_NFSe_C(XML: AnsiString;
  Certificado: ICertificate2; out XMLAssinado: AnsiString; ALote: Boolean;
  APrefixo3, APrefixo4: string; AProvedor: TnfseProvedor): Boolean;
var
 I,
 J,
 PosIni,
 PosFim,
 PosIniAssLote : Integer;

 URI, AID,
 Identificador : String;

 AXML,
 Assinatura,
 xmlHeaderAntes,
 xmlHeaderDepois : AnsiString;

 xmldoc    : IXMLDOMDocument3;
 xmldsig   : IXMLDigitalSignature;
 dsigKey   : IXMLDSigKey;
 signedKey : IXMLDSigKey;
begin
 AXML := XML;

 if ALote
  then begin
   Identificador := 'Id';
   I             := pos('LoteRps Id=', AXML);
   if I = 0
    then begin
     Identificador := 'id';
     I             := pos('LoteRps id=', AXML);
    end;
   if I = 0
    then begin
     Identificador := '';
     URI           := '';
    end
    else begin
     I := PosEx('"', AXML, I + 2);
     if I = 0
      then raise Exception.Create('N�o encontrei inicio do URI: aspas inicial');
     J := PosEx('"', AXML, I + 1);
     if J = 0
      then raise Exception.Create('N�o encontrei inicio do URI: aspas final');

     URI := copy(AXML, I + 1, J - I - 1);
    end;

//   if Identificador = 'id' then URI := '';

   if AProvedor = proMaringa then   (*Maringa Ini*)
     AXML := copy(AXML, 1, pos('</'+ APrefixo3 + 'EnviarLoteRpsSincronoEnvio>', AXML) - 1)
   else (*Maringa Fim*)
     AXML := copy(AXML, 1, pos('</'+ APrefixo3 + 'EnviarLoteRpsEnvio>', AXML) - 1);

   if (URI = '') {or (AProvedor = profintelISS)}
    then AID := '>'
    else AID := ' '+Identificador+'="AssLote_'+ URI +'">';

   AXML := AXML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"'+ AID +
//                  NotaUtil.SeSenao(URI = '', '>', ' '+Identificador+'="AssLote_'+ URI +'">') +
                 '<SignedInfo>'+
                  '<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>'+
                  '<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />'+
                  '<Reference URI="' + Geral.SeSenao(URI = '', '">', '#' + URI + '">') +
                   '<Transforms>'+
                    '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />'+
                   Geral.SeSenao(AProvedor = profintelISS, '',
                    '<Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />') +
                   '</Transforms>'+
                   '<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />'+
                   '<DigestValue></DigestValue>'+
                  '</Reference>'+
                 '</SignedInfo>'+
                 '<SignatureValue></SignatureValue>'+
                 '<KeyInfo>'+
                  '<X509Data>'+
                    '<X509Certificate></X509Certificate>'+
                  '</X509Data>'+
                 '</KeyInfo>'+
                '</Signature>';

   if AProvedor = proMaringa then   (*Maringa Ini*)
     AXML := AXML + '</'+ APrefixo3 + 'EnviarLoteRpsSincronoEnvio>'
   else  (*Maringa Fim*)
     AXML := AXML + '</'+ APrefixo3 + 'EnviarLoteRpsEnvio>';

  end
  else begin

   // Ao assinar um RPS a tag n�o possui prefixo
   APrefixo3 := '';

   if Pos('<Signature', AXML) <= 0
    then begin
     Identificador := 'Id';
     I             := pos('Id=', AXML);
     if I = 0
      then begin
       Identificador := 'id';
       I             := pos('id=', AXML);
       if I = 0
        then Identificador := '';
  //      raise Exception.Create('N�o encontrei inicio do URI: Id=');
      end;
     if I <> 0
      then begin
       I := PosEx('"', AXML, I + 2);
       if I = 0
        then raise Exception.Create('N�o encontrei inicio do URI: aspas inicial');
       J := PosEx('"', AXML, I + 1);
       if J = 0
        then raise Exception.Create('N�o encontrei inicio do URI: aspas final');

       URI := copy(AXML, I + 1, J - I - 1);
      end
      else URI := '';

     if (URI = '') or (AProvedor = profintelISS)
      then AID := '>'
      else AID := ' '+Identificador+'="Ass_'+ URI +'">';

     Assinatura := '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"' + AID +
//                        NotaUtil.SeSenao(URI = '', '>', ' ' + Identificador + '="Ass_' + URI + '">') +
                    '<SignedInfo>' +
                    '<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>' +
                     '<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />' +
                     '<Reference URI="' + Geral.SeSenao(URI = '', '">', '#' + URI + '">') +
                      '<Transforms>' +
                       '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />' +
                       Geral.SeSenao(AProvedor = profintelISS, '',
                       '<Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />') +
                      '</Transforms>' +
                      '<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />' +
                      '<DigestValue></DigestValue>' +
                     '</Reference>' +
                    '</SignedInfo>' +
                    '<SignatureValue></SignatureValue>' +
                    '<KeyInfo>' +
                     '<X509Data>' +
                       '<X509Certificate></X509Certificate>' +
                     '</X509Data>' +
                    '</KeyInfo>' +
                   '</Signature>';

     case AProvedor of
      profintelISS,
      proMaringa, (*Maringa*)
      proSaatri:    begin
                     AXML := copy(AXML, 1, pos('</InfDeclaracaoPrestacaoServico>', AXML) - 1);
                     AXML := AXML + '</InfDeclaracaoPrestacaoServico>';
                     AXML := AXML + Assinatura;
                     AXML := AXML + '</Rps>';
                    end;
      else begin
            AXML := copy(AXML, 1, pos('</Rps>', AXML) - 1);
            AXML := AXML + Assinatura;
            AXML := AXML + '</Rps>';
           end;
     end;

    end;

  end;

 // Lendo Header antes de assinar //
 xmlHeaderAntes := '';
 I              := pos('?>', AXML);
 if I > 0
  then xmlHeaderAntes := copy(AXML, 1, I + 1);

 xmldoc := CoDOMDocument50.Create;

 xmldoc.async              := False;
 xmldoc.validateOnParse    := False;
 xmldoc.preserveWhiteSpace := True;

 xmldsig := CoMXDigitalSignature50.Create;

 if (not xmldoc.loadXML(AXML))
  then raise Exception.Create('N�o foi poss�vel carregar o arquivo: ' + AXML);

 xmldoc.setProperty('SelectionNamespaces', DSIGNS);

 if ALote
  then begin
   if (URI <> '') {and (AProvedor <> profintelISS) }
    then xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature[@'+Identificador+'="AssLote_'+ URI +'"]')
    else xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');
//    else xmldsig.signature := xmldoc.selectSingleNode('.//EnviarLoteRpsEnvio//ds:Signature');
  end
  else xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');

 if (xmldsig.signature = nil)
  then raise Exception.Create('� preciso carregar o template antes de assinar.');

 if NumCertCarregado <> Certificado.SerialNumber
  then CertStoreMem := nil;

 if  CertStoreMem = nil
  then begin
   CertStore := CoStore.Create;
   CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

   CertStoreMem := CoStore.Create;
   CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

   Certs := CertStore.Certificates as ICertificates2;
   for i := 1 to Certs.Count do
    begin
     Cert := IInterface(Certs.Item[i]) as ICertificate2;
     if Cert.SerialNumber = Certificado.SerialNumber
      then begin
       CertStoreMem.Add(Cert);
        NumCertCarregado := Certificado.SerialNumber;
      end;
    end;
 end;

 OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey,PrivateKey));
 xmldsig.store := CertStoreMem;

 dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
 if (dsigKey = nil)
  then raise Exception.Create('Erro ao criar a chave do CSP.');

 signedKey := xmldsig.sign(dsigKey, $00000002);
 if (signedKey <> nil)
  then begin
   XMLAssinado := xmldoc.xml;
   XMLAssinado := StringReplace( XMLAssinado, #10, '', [rfReplaceAll] );
   XMLAssinado := StringReplace( XMLAssinado, #13, '', [rfReplaceAll] );

 case AProvedor of
  profintelISS,
  proMaringa, (*Maringa*)
  proSaatri:    begin
                 //By Akai - L. Massao Aihara ==================================
                 //MUDA A ASSINATURA...
                 //para n�o criar uma variavel usei a XMLAssinado mesmo...
                 //como o acbr estava gerando algumas coisas que n�o tinha no exemplo da Prefeitura de PG
                 //so fiz um processo para remover o "excesso"!!!
                 if not ALote then
                  begin
                   AXML := copy(XML, 1, pos('</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>', XML) - 1);
                   AXML := AXML + '</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>';
                   I    := pos('<Signature', XMLAssinado);

                   XMLAssinado := copy(XMLAssinado, I, pos('</Signature>', XMLAssinado) - I);
                   I           := pos('>', XMLAssinado);
                   XMLAssinado := StringReplace(XMLAssinado, Copy(XMLAssinado, 54, I - 54), '', []);

//                   I           := pos('<KeyInfo>', XMLAssinado);
//                   XMLAssinado := StringReplace(XMLAssinado, copy(XMLAssinado, I, pos('</KeyInfo>', XMLAssinado) - I), '', []);
//                   XMLAssinado := StringReplace(XMLAssinado, '</KeyInfo>', '', []);

                   AXML := AXML + XMLAssinado;
                   AXML := AXML + '</Signature>';
                   AXML := AXML + '</Rps>';

                   XMLAssinado := AXML;
                  end
                  else
                  //Se for o LOTE...
                  //essa parte n�o precisava, ja a prefeitura ja estava retornando a msg "correta"...
                  //mas como no exemplo da prefeitura estava diferente, alterei tbm...
                  begin
                   AXML := copy(XML, 1, pos('</LoteRps>', XML) - 1);
                   AXML := AXML + '</LoteRps>';
                   I    := pos('</LoteRps>', XMLAssinado);

                   if AProvedor = proMaringa then   (*Maringa Ini*)
                     XMLAssinado := copy(XMLAssinado, I, pos('</EnviarLoteRpsSincronoEnvio>', XMLAssinado) - I)
                   else  (*Maringa Fim*)
                     XMLAssinado := copy(XMLAssinado, I, pos('</EnviarLoteRpsEnvio>', XMLAssinado) - I);
                   XMLAssinado := StringReplace(XMLAssinado, '</LoteRps>', '', []);
                   I           := pos('>', XMLAssinado);
                   XMLAssinado := StringReplace(XMLAssinado, Copy(XMLAssinado, 54, I - 54), '', []);

//                   I           := pos('<KeyInfo>', XMLAssinado);
//                   XMLAssinado := StringReplace(XMLAssinado, copy(XMLAssinado, I, pos('</KeyInfo>', XMLAssinado) - I), '', []);
//                   XMLAssinado := StringReplace(XMLAssinado, '</KeyInfo>', '', []);

                   AXML := AXML + XMLAssinado;
                   if AProvedor = proMaringa then    (*Maringa Ini*)
                     AXML := AXML + '</EnviarLoteRpsSincronoEnvio>'
                   else (*Maringa Fim*)
                     AXML := AXML + '</EnviarLoteRpsEnvio>';

                   XMLAssinado := AXML;
                  end;
                 //Fim altera��o Akai ==========================================

                 (*
                 // Variavel XML contem o XML original de entrada na fun��o
                 // sem assinatura.
                 AXML := copy(XML, 1, pos('</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>', XML) - 1);
                 AXML := AXML + '</'+ APrefixo3 + 'InfDeclaracaoPrestacaoServico>';
                 I    := pos('<Signature', XMLAssinado);
                 AXML := AXML + copy(XMLAssinado, I, pos('</Signature>', XMLAssinado) - I);
                 AXML := AXML + '</Signature>';
                 AXML := AXML + '</Rps>';

                 XMLAssinado := AXML;
                 *)
                end;
 end;



   if ALote
    then begin
     // Sugest�o de Rodrigo Cantelli
     PosIniAssLote := Pos('</'+ APrefixo3 + 'LoteRps>', XMLAssinado);

     if PosIniAssLote = 0
      then PosIniAssLote := Pos('</LoteRps>', XMLAssinado) + length('</LoteRps>')
      else PosIniAssLote := PosIniAssLote + length('</'+ APrefixo3 + 'LoteRps>');

     PosIni      := PosEx('<SignatureValue>', XMLAssinado, PosIniAssLote) + length('<SignatureValue>');
     XMLAssinado := copy(XMLAssinado, 1, PosIni - 1) +
                    StringReplace( copy(XMLAssinado, PosIni, length(XMLAssinado)), ' ', '', [rfReplaceAll] );

     // Sugest�o de Rodrigo Cantelli
     PosIniAssLote := Pos('</'+ APrefixo3 + 'LoteRps>', XMLAssinado);

     if PosIniAssLote = 0
      then PosIniAssLote := Pos('</LoteRps>', XMLAssinado) + length('</LoteRps>')
      else PosIniAssLote := PosIniAssLote + length('</'+ APrefixo3 + 'LoteRps>');

     PosIni      := PosEx('<X509Certificate>', XMLAssinado, PosIniAssLote) - 1;
     PosFim      := Geral.PosLast('<X509Certificate>', XMLAssinado);
     XMLAssinado := copy(XMLAssinado, 1, PosIni) +
                    copy(XMLAssinado, PosFim, length(XMLAssinado));
    end
    else begin
     PosIni      := Pos('<SignatureValue>', XMLAssinado) + length('<SignatureValue>');
     XMLAssinado := copy(XMLAssinado, 1, PosIni - 1) +
                    StringReplace( copy(XMLAssinado, PosIni, length(XMLAssinado)), ' ', '', [rfReplaceAll] );
     PosIni      := Pos('<X509Certificate>', XMLAssinado) - 1;
     PosFim      := Geral.PosLast('<X509Certificate>', XMLAssinado);
     XMLAssinado := copy(XMLAssinado, 1, PosIni) +
                    copy(XMLAssinado, PosFim, length(XMLAssinado));
    end;
  end
  else raise Exception.Create('Assinatura Falhou.');

 if xmlHeaderAntes <> ''
  then begin
   I := pos('?>',XMLAssinado);
   if I > 0
    then begin
     xmlHeaderDepois := copy(XMLAssinado,1,I+1);
     if xmlHeaderAntes <> xmlHeaderDepois
      then XMLAssinado := StuffString(XMLAssinado,1,length(xmlHeaderDepois),xmlHeaderAntes);
    end
    else XMLAssinado := xmlHeaderAntes + XMLAssinado;
  end;

 dsigKey   := nil;
 signedKey := nil;
 xmldoc    := nil;
 xmldsig   := nil;

 Result := True;
end;

function TCapicomGeral.ObtemCertificado(const NumeroSerial: String;
  out Cert: ICertificate2): Boolean;
var
  Store        : IStore;
  Certs        : ICertificates;
  //CertContext  : ICertContext;
  i : Integer;
  //sXML: AnsiString;
begin
  Result := False;
  Cert   := nil;
  Store  := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open(CAPICOM_CURRENT_USER_STORE, 'MY', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     //Cria objeto para acesso a leitura do certificado
     Cert := IInterface(Certs.Item[i + 1]) as ICertificate2;
     if UpperCase(Cert.SerialNumber) = Uppercase(NumeroSerial) then
     begin
       //se o n�mero do serial for igual ao que queremos utilizar
       Result := True;
       Exit;
     end;
     i := i + 1;
  end;
  //
  if Result = False then
  begin
    Cert := nil;
    ShowMessage('N�o foi poss�vel carregar o certificado:' +
      #13#10 + 'N�mero Serial: ' + NumeroSerial + '!');
    {
    Geral.MensagemBox('N�o foi poss�vel carregar o certificado:' +
      #13#10 + 'N�mero Serial: ' + NumeroSerial + '!'),
      'Aviso', MB_OK+MB_ICONWARNING);
    }
  end;
  //
end;

end.
