unit CNPJa;
//   Token obtido em 16/11/2021 com cadastro no CNPJa com a conta do google mlarend2012@gmail.com
//   5f151f32-5617-4cf4-951c-be1d36088aa3-b055420d-7f41-4755-9d5c-78809f40bb56


interface

uses Classes, StdCtrls, Forms, Controls, dmkGeral, ExtCtrls, Windows, SysUtils,
  Dialogs, Messages, dmkEdit, dmkDBLookupComboBox, dmkEditCB, mySQLDbTables,
  Variants, UnDmkEnums;

type
  TCNPJa = class(TObject)
  private
    function ConsultaCNPJa(const CNPJ: String; var Retorno: String): Boolean;
  public
    { Public declarations }
    function  DefineDadosCNPJa(EdCNPJ, EdIE, EdRazaoSocial, EdFantasia,
              EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF,
              EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
              CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
              EdCodCNAE: TdmkEditCB; EdETe1, EdEEmail: TdmkEdit): Boolean;
var
  end;

var
  UnCNPJa: TCNPJa;

implementation

uses Pkg.Json.DTO, RootUnit, CNPJaConsulta;
(*
  MyDBCheck, Module, UnCEP, ModuleGeral, DmkDAC_PF,
  {$IfNDef SemEntidade1} EntiMapa, EntiRFCPF, EntiRFCNPJ, EntiRE_IE, {$EndIf}
  UnInternalConsts, UnDmkWeb, UnGrl_DmkREST, UnServices;
*)

{ TCNPJa }

function TCNPJa.ConsultaCNPJa(const CNPJ: String; var Retorno: String): Boolean;
const
  Texto =
'{"last_update":"2021-11-24T14:22:59.000Z","name":"GLAXOSMITHKLINE BRASIL LTDA","alias":"GLAXOSMITHKLINE - GSK - LABORATORIOS STIEFEL - STIEFEL","tax_id":"33247743000110","type":"MATRIZ","founded":"1966-08-29","size":"DEMAIS","capital":68' +
'6215293,"email":null,"phone":null,"phone_alt":null,"federal_entity":null,"registration":{"status":"ATIVA","status_date":"2005-11-03","status_reason":null,"special_status":null,"special_status_date":null},"address":{"street":"ESTRADA DOS ' +
'BANDEIRANTES","number":"8464","details":null,"zip":"22783110","neighborhood":"JACAREPAGUA","city":"RIO DE JANEIRO","state":"RJ","city_ibge":"3304557","state_ibge":"33"},"legal_nature":{"code":"2062","description":"Sociedade Empres�ria Li' +
'mitada"},"primary_activity":{"code":"2121101","description":"Fabrica��o de medicamentos alop�ticos para uso humano"},"secondary_activities":[{"code":"1099699","description":"Fabrica��o de outros produtos aliment�cios n�o especificados an' +
'teriormente"},{"code":"2063100","description":"Fabrica��o de cosm�ticos, produtos de perfumaria e de higiene pessoal"},{"code":"2121103","description":"Fabrica��o de medicamentos fitoter�picos para uso humano"},{"code":"4619200","descrip' +
'tion":"Representantes comerciais e agentes do com�rcio de mercadorias em geral n�o especializado"},{"code":"4644301","description":"Com�rcio atacadista de medicamentos e drogas de uso humano"},{"code":"4649402","description":"Com�rcio at' +
'acadista de aparelhos eletr�nicos de uso pessoal e dom�stico"},{"code":"5620103","description":"Cantinas - servi�os de alimenta��o privativos"},{"code":"7490104","description":"Atividades de intermedia��o e agenciamento de servi�os e neg' +
'�cios em geral, exceto imobili�rios"},{"code":"8292000","description":"Envasamento e empacotamento sob contrato"},{"code":"8621601","description":"UTI m�vel"},{"code":"8621602","description":"Servi�os m�veis de atendimento a urg�ncias, e' +
'xceto por UTI m�vel"},{"code":"8630503","description":"Atividade m�dica ambulatorial restrita a consultas"}],"membership":[{"name":"GRAZIELA FIORINI SOARES","tax_id":"***407757**","role":{"code":"05","description":"Administrador"}},{"nam' +
'e":"GLAXOSMITHKLINE BIOLOGICAL S.A.","tax_id":"06074092000130","role":{"code":"37","description":"S�cio Pessoa Jur�dica Domiciliado no Exterior"},"legal_rep":{"name":"ALBERTO M J J M G RAFAEL GONZAGA DE ORLEANS E BRAGANCA","role":{"code"' +
':"17","description":"Procurador"}}},{"name":"CINTIA LIGIERO BARROSO MAGNO","tax_id":"***334127**","role":{"code":"05","description":"Administrador"}},{"name":"ESTEBAN CARLOS GULLY","tax_id":"***479701**","role":{"code":"05","description"' +
':"Administrador"}},{"name":"ANDRE VIVAN DA SILVA","tax_id":"***116508**","role":{"code":"05","description":"Administrador"}},{"name":"SMITHKLINE BEECHAM (H) LIMITED","tax_id":"06096616000194","role":{"code":"37","description":"S�cio Pess' +
'oa Jur�dica Domiciliado no Exterior"},"legal_rep":{"name":"ALBERTO M J J M G RAFAEL GONZAGA DE ORLEANS E BRAGANCA","role":{"code":"17","description":"Procurador"}}}],"partnership":[],"simples_nacional":{"last_update":null,"simples_optant' +
'":null,"simples_included":null,"simples_excluded":null,"simei_optant":null},"sintegra":{"last_update":null,"home_state_registration":null,"registrations":[]},"files":{"registration":"https://api.cnpja.com/rfb/certificate?taxId=3324774300' +
'0110&pages=REGISTRATION&signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzOTUiLCJ1cmwiOiIvcmZiL2NlcnRpZmljYXRlP3RheElkPTMzMjQ3NzQzMDAwMTEwJnBhZ2VzPVJFR0lTVFJBVElPTiIsImlhdCI6MTYzNz' +
'kzMDQ0OSwiZXhwIjoxNjQ1NzA2NDQ5LCJhdWQiOiJodHRwczovL2NucGphLmNvbS8iLCJpc3MiOiJodHRwczovL2NucGphLmNvbS8ifQ.ApgflX40BZFhwMOtCX2ovFZaIrh3xPXNIpl6RjyQgvbLGmkrfuGJtjURYgr7lg99fD02FLmvUYN9Xf8vi-NfRoqr_KAe9PCt3hixzGXqNdBXF0RbyVORk33JKFGwqhIa3syL' +
'jk0kvACcot0qVc5MuZ5cV5EZfLwDpAWRMd1w3a9cgR_UfM1RfQ2V7p7wzHNETuap3apnPHjCEevQ0AVM0XlJ093hOIeHL1U9NmelNCBYCTRRMEp9Z5lnWFWQrlV975QC31qfcRqWWRaCa7MGOTKuYZjvT5uPPaHKSUJYbxkg5dkryQfrYyzShqGUd_Jg6tidcvD5hEW6AiOvSH7qAWzNDXK7k1MeLLAR89neRnztFsnUi' +
'MQ5RJkU8QiH2QCSuLesF2kIPaLRmk0t9hLX0IqgigJhk4zgXWS92c7DB_Eph0n9YM6DdM54fXvtHTcGP1B6GUlApsXQlIOf5wodJ0qYLku2T_lHFSyAdX68CDCvlPrIYO0hvM9FSDNwzHCkXfU0tMMIsd8PJNdXzCiRdi3YNTAB3pgd3F4_IlnDp1WtyKDy65Hy33Vzn6l2ZAJxhTti-vfPOf5t9eflSZk9OqliDg5wnm' +
'9H2xJpMIvpJPTiXQm79mvs1Ho1rEmiYG3r6_-MR5QeD_SnQ3ndMOdgI53C6COkPGtW-oA2zOnVtq8","membership":"https://api.cnpja.com/rfb/certificate?taxId=33247743000110&pages=MEMBERS&signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1OWNk' +
'NC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzOTUiLCJ1cmwiOiIvcmZiL2NlcnRpZmljYXRlP3RheElkPTMzMjQ3NzQzMDAwMTEwJnBhZ2VzPU1FTUJFUlMiLCJpYXQiOjE2Mzc5MzA0NDksImV4cCI6MTY0NTcwNjQ0OSwiYXVkIjoiaHR0cHM6Ly9jbnBqYS5jb20vIiwiaXNzIjoiaHR0cHM6Ly9jbnBqYS5jb20vI' +
'n0.CBuSymRIZEiY6LQ35RIq5HQg-ZE7Odzx9kF1p5dith0-q9QBkkRba_UlTdx7CthTbyfMAMLf2mqMY16oFMUqNYlEqY0eQjNMTwx8BAoPAyj0UhFODihNttFBJXhjwf8715Z_ZSAVymrhvmipyLHuCsLPIlY7ITkuLb__zE1TrHNXQCD-jSYqfK1CvacdYemDeJB1gcLVug2YouLiRjJR5DZ1bxqDU1kMXwaLJB4ngL' +
'JbWiVN70bGa9HNDfaUoGXWymmfYGfy_f4_PaAjZPBVZN_P1YrZG2DIlUqonDLPBRMiSvJtsHkp7D8FTQe8HeGbxtuorMWqdH0yYO7OCrmWn_8Er8biPj4MegJU8kCi3gZWXM9GbgRSiFgDwe9YZYjdbN5KhISqmdqgrlRSz88QHcKqlggI3TAOJRb2VpY-4N4sH2zy2Lb7YtBwh9NHkvKeakI5Oq_jsx9_uA_15Zprtq9' +
'x39mzL408FGLSceB_vI-FRPBWstrUck-UhJTb9X0OhHttPCAj7VREnfFDG6_IYJyt0RGvunsBvYcldV4dZPGOVAMUq4u7LPDGtwIXs8do7IzjwiyBuVZGrJig84_UOmYU33eF1h7Zl-VTFOMOa1zqDCfFQafGJ1irwxxqJjCzoML6TcXaTaC1qe_KtejvBYXmiEwuyBJMov6ATdHrKVQ"},"maps":{"roads":"https' +
'://api.cnpja.com/office/33247743000110/map?signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzOTUiLCJ1cmwiOiIvb2ZmaWNlLzMzMjQ3NzQzMDAwMTEwL21hcCIsImlhdCI6MTYzNzkzMDQ0OSwiZXhwIjoxNjQ' +
'1NzA2NDQ5LCJhdWQiOiJodHRwczovL2NucGphLmNvbS8iLCJpc3MiOiJodHRwczovL2NucGphLmNvbS8ifQ.JAifBakU2yuy3kgO6tty0eiIXpSesVCQul1U8H8lAieZ0dNE1x3RK7BP-2H5C6ITJwmq5Kcfsjej8bz4aUuYiHIxVWjnnwGd2tPgkiejtBlbtV9kTnIyI7gCUfjLmhDR7U6DdRwqCas0p-sU7wMPNCIlO' +
'z1a--z_Mizyr_z5T2hBATy84lmwvq2_4D20dX367anixeZyy-yOUOSb8Gsb_UWw7rmpmTeYXFRitXLBudAFeJqtMI8H4ZiUT8VYVyLaNbm1wVyHUaWnFSYFneVPV_juRoN_xzGRpV_oQxVQKP-GaQIwP18Y6WYKfsHfmOTiZPVXvNFkQq2n7TI6i15sHh8PNabD_bbBSjjSvHhtwQJ7rFXSgp0lpH_o9AbbBGfMZ5cE9B' +
'OK68X0Qztbsx_gpkgNnCUwPEeEEDJPlG7kON1VMiZZeEBtATTTJHbC9zqua8jpVgzEk-f05Ox51yntZzYX4QNZrjaK5w4NvLfknxCEusK2-np4cNRa0UpLgNM_rqoMGGf1BlpvrbXz4v-tfJeVX3i4Ipn6VPAdhpI5pA_lPH0xd6_ABpE5RRmbF_6ylCt78RtpEfrtfmXovs6dCiIFdSDBolEndvLnR77YNTZbIu7-MHm' +
'c_sLy9P_z58KwffLeujw6ClGTTX8OXjQYTqCf9THbXj-zY52SZ-ZE4dA","satellite":"https://api.cnpja.com/office/33247743000110/map?type=hybrid&signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzU' +
'zOTUiLCJ1cmwiOiIvb2ZmaWNlLzMzMjQ3NzQzMDAwMTEwL21hcD90eXBlPWh5YnJpZCIsImlhdCI6MTYzNzkzMDQ0OSwiZXhwIjoxNjQ1NzA2NDQ5LCJhdWQiOiJodHRwczovL2NucGphLmNvbS8iLCJpc3MiOiJodHRwczovL2NucGphLmNvbS8ifQ.F1qKl5cIJuleOZ13ckxFUpTzUKv-nar6B5g4K6A8kl9De6jjm' +
'dnvdAPrfZRlUkPBcA167IJ7yNQCHpX-7WZ3Y4Ex_F6lXYJ_xyc89tXagwng29hWkKyEn630qKl2_-A4vMjhjcEyu829wliwyvNo9XyY9J-nVtcUrTjQEAW2i6W0iALGjqpQ94U3jBF4GdGfyk_nl0ft_jhm0XS5JbmnjGdRXulI9Se5ALgrl8fHZYaW_z2zfmvJGLMVPHRO0ZcsgoAOzCC-VLQzaTwfBawIEQ8EmjYOYL' +
'Czv1OxWLtf8EVIn64E7h8-8A9HPqpb9Q4rOIGWVH6N55zfYOzZ0en3nuTK25_MP_jUElrhZHpxprEhsci5GfnIPOLj17vhqu3g3dNLpThtBQug8nmDJ7BPnzWSjBxxImFvLeAUqsR0ULetfqGMViE-aH_soRm0T3YzChCyHtUDplrxJHfCswWzIUrgMF8yJkNu1DwTAc2Gm21Dy9s3CsCmsK6DOoZNT99fxu7cJb2-BVN' +
'qs_vcWibG0zA4qmTt6WE9r5LWniESpcj_q9-G0aKApUa19aodf9pbnqPVOapnkUPzIfxL3KERiAsbw4yLtHC2eMXKDvjh3buG2nMbSlaXN7zb-FivxMFm9spi8Dpu7DX3_dacz8RALijUEIpbKOILqX2dh2TgapQ","street":"https://api.cnpja.com/office/33247743000110/street?signature=eyJh' +
'bGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzOTUiLCJ1cmwiOiIvb2ZmaWNlLzMzMjQ3NzQzMDAwMTEwL3N0cmVldCIsImlhdCI6MTYzNzkzMDQ0OSwiZXhwIjoxNjQ1NzA2NDQ5LCJhdWQiOiJodHRwczovL2NucGphLmNvbS8iLCJpc3Mi' +
'OiJodHRwczovL2NucGphLmNvbS8ifQ.KRuak0AD3qEfHM-OAzdB7IgyfiHt07htw6PAjT4tgKJP6l8h69Xnrw4N1A7QPJSU65yIuNBoA30FcLwPQEY5Cq0QYMrmu4_YzVe5QNVKItJrjA2M-tgjE8Vbo3V3sEHV1MoFgMj9p5TYAe5-HFzpO_OAldi1rmhO1_tSjHLkY7VPFREneNcATw6uWktS0H6ceoGa5n1Mbi75lJ' +
'yTvzauhbgvjcQ2rRdGIM9nHsAl2kkC6Lsy-qYjdlOLWZ6LXW6Avy2up-SPonMYt6DiBCJwwNR2vhOq5SJvvmfJr2YYFHbEEbxTVb9w4AKKjYo2XCW5ZrlLKna1tS-wtpxwizdEfHYX3UsifnEFa22XXHRmDjPCiwIo37XJfxZb5UoPGQubidv8iWigJd8hbLHEgaqogDf1P2e-mFcvAl-Ho5MsMDOgFlmyxesP3MQt_9C' +
'FdBAocMvvHE1q8gXwXAItw_ha9Hm-DAXj0v2fq58aMIMtp6Qa3dRpkUmFnqMAG-Va1utWLMa5nN31NRC4Blm1Bdd-oX1RbaBJkf8s9rimcUUmHulZoNDeQP3x9S18JE6YFzMZfjpMZU6M0UJ3-qR-JhpgWKPd_RJFzX14ZmUxT92falCgYnch_qIbxohE1sv1y78Gdn-NuqO2T76DQWTPfmbcCpXP311mcE63aRgXg4yB' +
'ayo"}}';
var
  Consultou: Boolean;
begin
  Consultou := False;
{
  Retorno := Texto;
  Result := True;
}
  Result := False;
  Retorno := '';
  Application.CreateForm(TFmCNPJaConsulta, FmCNPJaConsulta);
  with FmCNPJaConsulta do
  begin
    txtCNPJ.Text := Geral.SoNumero_TT(CNPJ);
    ShowModal;
(*
    Consultou := ConsultaCNPJ();
*)
    Consultou := FConsultou;
    if Consultou then
    begin
      Retorno := FResult;
      Result := True;
    end;
    //
    Destroy;
  end;
end;

function TCNPJa.DefineDadosCNPJa(EdCNPJ, EdIE, EdRazaoSocial, EdFantasia, EdCEP,
  EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF,
  EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
  CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
  EdCodCNAE: TdmkEditCB; EdETe1, EdEEmail: TdmkEdit): Boolean;
var
  Cnpj, Json: String;
  Root: TRootDTO;
  I, J: Integer;
  Inscricoes: String;
begin
{$IfNDef SemEntidade1}
  Inscricoes := EmptyStr;
  Cnpj := EdCNPJ.ValueVariant;
  //
  if Cnpj = '' then
  begin
    Geral.MB_Aviso('CNPJ n�o informado!');
    EdCNPJ.SetFocus;
    //
    Result := True;
    Exit;
  end;
  //
  Result := False;
  //
  Screen.Cursor := crHourGlass;
  //
  if ConsultaCNPJa(cnpj, json) then
  begin
    Root := TRootDTO.Create;
    try
      Root.AsJson := Json;

{
      ShowMessage(Root.Sintegra.Home_State_Registration);

      for I := 0 to Root.Sintegra.Registrations.Count - 1 do
      begin
          ShowMessage(Root.Sintegra.Registrations[I].Number);
      end;

      for J := 0 to Root.Secondary_Activities.Count - 1 do
      begin
          ShowMessage(Root.Secondary_Activities[J].Description);
      end;
}

      //EdCNPJ,
      // J� vem para pesquisa...
      //EdIE.ValueVariant:
      if EdRazaoSocial <> nil then
        EdRazaoSocial.ValueVariant := Root.Name;
      if EdFantasia <> nil then 
        EdFantasia.ValueVariant := Root.Alias;
      if EdCEP <> nil then 
        EdCEP.ValueVariant := Root.Address.zip;
      if EdRua <> nil then
        EdRua.ValueVariant := Root.Address.Street;
      if EdLograd <> nil then
        EdLograd.ValueVariant := 0;
      if EdCompl <> nil then 
        EdCompl.ValueVariant := Root.Address.Details;
      if EdNumero <> nil then
        EdNumero.ValueVariant := Geral.SoNumero_TT(Root.Address.Number);
      if EdBairro <> nil then
        EdBairro.ValueVariant := Root.Address.Neighborhood;
      if EdCidade <> nil then
        EdCidade.ValueVariant := Root.Address.City;
      if EdUF <> nil then
        EdUF.ValueVariant := UpperCase(Root.Address.State);
      if EdPais <> nil then
        EdPais.Text := 'BRASIL';
      if CBLograd <> nil then
        CBLograd.KeyValue := 0;
      if (EdCodMunici <> nil) then
      begin  
        EdCodMunici.ValueVariant := Root.Address.City_Ibge;
      end;  
      if CBCodMunici <> nil then
        CBCodMunici.KeyValue := Root.Address.City_Ibge;
      if EdCodiPais <> nil then
        EdCodiPais.ValueVariant := 1058;
      if CBCodiPais <> nil then
        CBCodiPais.KeyValue := 1058;
      if EdCodCNAE <> nil then
        EdCodCNAE.ValueVariant := Geral.SoNumero_TT(Root.Primary_Activity.Code);
      if CBCodCNAE <> nil then
        CBCodCNAE.KeyValue := Geral.SoNumero_TT(Root.Primary_Activity.Code);
      if EdETe1 <> nil then
        EdETe1.ValueVariant := Root.Phone;
      if EdEEmail <> nil then
        EdEEmail.ValueVariant := Root.Phone;
      //  
      if EdIE <> nil then
      begin
        if Root.Sintegra.Registrations.Count > 0 then
          EdIE.ValueVariant := Root.Sintegra.Registrations[0].Number;
        if Root.Sintegra.Registrations.Count > 1 then
        begin
          for I := 0 to Root.Sintegra.Registrations.Count - 1 do
            Inscricoes := Inscricoes + Root.Sintegra.Registrations[I].Number + sLineBreak;
        end;
      end;
      if Inscricoes <> '' then
        Geral.MB_Info('CNPJ com mais de uma I.E.:' + sLineBreak +
        Inscricoes);
      EdIE.SetFocus;
    finally
      Root.Free;
    end;
  end;
  //
  Screen.Cursor := crDefault;
  //
  Result := True;
{$EndIf}
end;

end.
