unit CNPJaConsulta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, Vcl.StdCtrls,
  dmkGeral, dmkEdit;

type
  TFmCNPJaConsulta = class(TForm)
    Button1: TButton;
    txtCNPJ: TEdit;
    IdHTTP: TIdHTTP;
    OpenSSL: TIdSSLIOHandlerSocketOpenSSL;
    MeJSON: TMemo;
    Button2: TButton;
    Label1: TLabel;
    Label11: TLabel;
    EdCNPJ: TdmkEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConsultou: Boolean;
    FResult: String;
    //
    function ConsultaCNPJ(): Boolean;
  end;

var
  FmCNPJaConsulta: TFmCNPJaConsulta;

implementation

{$R *.dfm}

uses Pkg.Json.DTO, RootUnit, ModuleGeral;

procedure TFmCNPJaConsulta.Button1Click(Sender: TObject);
begin
//
end;

procedure TFmCNPJaConsulta.Button2Click(Sender: TObject);
var
  Root: TRootDTO;
  I,J:Integer;
begin
   Root := TRootDTO.Create;
  try
    Root.AsJson := MeJSON.Lines.Text;

    ShowMessage(Root.Sintegra.Home_State_Registration);

    for I := 0 to Root.Sintegra.Registrations.Count - 1 do
    begin
        ShowMessage(Root.Sintegra.Registrations[I].Number);
    end;

    for J := 0 to Root.Secondary_Activities.Count - 1 do
    begin
        ShowMessage(Root.Secondary_Activities[J].Description);
    end;

   finally
    Root.Free;
  end;
end;

function TFmCNPJaConsulta.ConsultaCNPJ(): Boolean;
var
  vResult: string;
begin

  vResult := '';

  OpenSSL.SSLOptions.Method := sslvTLSv1_2;
  OpenSSL.SSLOptions.Mode := sslmClient;
  OpenSSL.PassThrough := True;
  IdHTTP.IOHandler := OpenSSL;

  IdHTTP.Request.UserAgent :=
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
  IdHTTP.Request.Accept := 'application/json, text/javascript, */*; q=0.01';
  IdHTTP.Request.ContentType := 'application/json';
  IdHTTP.Request.CharSet := 'utf-8';

{
  IdHTTP.Request.CustomHeaders.AddValue('authorization',
    'SUA CHAVE AQUI');
}
  IdHTTP.Request.CustomHeaders.AddValue('authorization', DModG.QrCtrlGeralCNPJaToken.Value);

  try

{
    vResult := IdHTTP.Get('https://api.cnpja.com.br/companies/' + txtCNPJ.Text +
      '?sintegra_max_age=1000&simples_max_age=1000');
}
    vResult := IdHTTP.Get('https://api.cnpja.com.br/companies/' + txtCNPJ.Text);

    MeJSON.Lines.Clear;
    MeJSON.Text := vResult;
    //Geral.MB_Teste(vResult);
    FResult := vResult;

    FConsultou := True;
    Result := True;
    Close;
  except
    on E: EIdHTTPProtocolException do
    Begin
      IdHTTP.Disconnect;
      ShowMessage(E.ErrorMessage);
    End;
  end;

end;

procedure TFmCNPJaConsulta.FormCreate(Sender: TObject);
begin
  FConsultou := False;
  //FResult := MeJSON.Text;
  MeJSON.Lines.Clear;
end;

end.
