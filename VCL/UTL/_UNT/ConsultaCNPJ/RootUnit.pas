unit RootUnit;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TMapsDTO = class
  private
    FRoads: string;
    FSatellite: string;
    FStreet: string;
  published
    property Roads: string read FRoads write FRoads;
    property Satellite: string read FSatellite write FSatellite;
    property Street: string read FStreet write FStreet;
  end;
  
  TFilesDTO = class
  private
    FMembership: string;
    FRegistration: string;
  published
    property Membership: string read FMembership write FMembership;
    property Registration: string read FRegistration write FRegistration;
  end;
  
  TRegistrationsDTO = class
  private
    FEnabled: Boolean;
    FNumber: string;
    FState: string;
  published
    property Enabled: Boolean read FEnabled write FEnabled;
    property Number: string read FNumber write FNumber;
    property State: string read FState write FState;
  end;
  
  TSintegraDTO = class
  private
    FHome_State_Registration: string;
    //FLast_Update: TDateTime;
    [JSONName('registrations')]
    FRegistrationsArray: TArray<TRegistrationsDTO>;
    [GenericListReflect]
    FRegistrations: TObjectList<TRegistrationsDTO>;
    function GetRegistrations: TObjectList<TRegistrationsDTO>;
  published
    property Home_State_Registration: string read FHome_State_Registration write FHome_State_Registration;
    //property Last_Update: TDateTime read FLast_Update write FLast_Update;
    property Registrations: TObjectList<TRegistrationsDTO> read GetRegistrations;
    destructor Destroy; override;
  end;
  
  TSimples_NacionalDTO = class
  private
    FLast_Update: TDateTime;
    FSimei_Optant: Boolean;
    FSimples_Included: TDate;
    FSimples_Optant: Boolean;
  published
    property Last_Update: TDateTime read FLast_Update write FLast_Update;
    property Simei_Optant: Boolean read FSimei_Optant write FSimei_Optant;
    property Simples_Included: TDate read FSimples_Included write FSimples_Included;
    property Simples_Optant: Boolean read FSimples_Optant write FSimples_Optant;
  end;
  
  TPartnershipDTO = class
  end;
  
  TMembershipDTO = class
  end;
  
  TSecondary_ActivitiesDTO = class
  private
    FCode: String;
    FDescription: string;
  published
    property Code: String read FCode write FCode;
    property Description: string read FDescription write FDescription;
  end;
  
  TPrimary_ActivityDTO = class
  private
    FCode: String;
    FDescription: string;
  published
    property Code: String read FCode write FCode;
    property Description: string read FDescription write FDescription;
  end;
  
  TLegal_NatureDTO = class
  private
    FCode: String;
    FDescription: string;
  published
    property Code: String read FCode write FCode;
    property Description: string read FDescription write FDescription;
  end;
  
  TAddressDTO = class
  private
    FCity: string;
    FCity_Ibge: String;
    FNeighborhood: string;
    FNumber: string;
    FDetails: String;
    FState: string;
    FState_Ibge: String;
    FStreet: string;
    FZip: String;
  published
    property City: string read FCity write FCity;
    property City_Ibge: String read FCity_Ibge write FCity_Ibge;
    property Neighborhood: string read FNeighborhood write FNeighborhood;
    property Number: string read FNumber write FNumber;
    property Details: String read FDetails write FDetails;
    property State: string read FState write FState;
    property State_Ibge: String read FState_Ibge write FState_Ibge;
    property Street: string read FStreet write FStreet;
    property Zip: String read FZip write FZip;
  end;
  
  TRegistrationDTO = class
  private
    FStatus: string;
    FStatus_Date: TDate;
  published
    property Status: string read FStatus write FStatus;
    property Status_Date: TDate read FStatus_Date write FStatus_Date;
  end;
  
  TRootDTO = class(TJsonDTO)
  private
    FAddress: TAddressDTO;
    FAlias: string;
    FCapital: Integer;
    FEmail: string;
    FFiles: TFilesDTO;
    FFounded: TDate;
    FLast_Update: TDateTime;
    FLegal_Nature: TLegal_NatureDTO;
    FMaps: TMapsDTO;
    [JSONName('membership')]
    FMembershipArray: TArray<TMembershipDTO>;
    [GenericListReflect]
    FMembership: TObjectList<TMembershipDTO>;
    FName: string;
    [JSONName('partnership')]
    FPartnershipArray: TArray<TPartnershipDTO>;
    [GenericListReflect]
    FPartnership: TObjectList<TPartnershipDTO>;
    FPhone: string;
    FPrimary_Activity: TPrimary_ActivityDTO;
    FRegistration: TRegistrationDTO;
    [JSONName('secondary_activities')]
    FSecondary_ActivitiesArray: TArray<TSecondary_ActivitiesDTO>;
    [GenericListReflect]
    FSecondary_Activities: TObjectList<TSecondary_ActivitiesDTO>;
    FSimples_Nacional: TSimples_NacionalDTO;
    FSintegra: TSintegraDTO;
    FSize: string;
    FTax_Id: String;
    FType: string;
    function GetSecondary_Activities: TObjectList<TSecondary_ActivitiesDTO>;
    function GetMembership: TObjectList<TMembershipDTO>;
    function GetPartnership: TObjectList<TPartnershipDTO>;
  published
    property Address: TAddressDTO read FAddress write FAddress;
    property Alias: string read FAlias write FAlias;
    property Capital: Integer read FCapital write FCapital;
    property Email: string read FEmail write FEmail;
    property Files: TFilesDTO read FFiles write FFiles;
    property Founded: TDate read FFounded write FFounded;
    property Last_Update: TDateTime read FLast_Update write FLast_Update;
    property Legal_Nature: TLegal_NatureDTO read FLegal_Nature write FLegal_Nature;
    property Maps: TMapsDTO read FMaps write FMaps;
    property Membership: TObjectList<TMembershipDTO> read GetMembership;
    property Name: string read FName write FName;
    property Partnership: TObjectList<TPartnershipDTO> read GetPartnership;
    property Phone: string read FPhone write FPhone;
    property Primary_Activity: TPrimary_ActivityDTO read FPrimary_Activity write FPrimary_Activity;
    property Registration: TRegistrationDTO read FRegistration write FRegistration;
    property Secondary_Activities: TObjectList<TSecondary_ActivitiesDTO> read GetSecondary_Activities;
    property Simples_Nacional: TSimples_NacionalDTO read FSimples_Nacional write FSimples_Nacional;
    property Sintegra: TSintegraDTO read FSintegra write FSintegra;
    property Size: string read FSize write FSize;
    property Tax_Id: String read FTax_Id write FTax_Id;
    property &Type: string read FType write FType;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;
  
implementation

{ TSintegraDTO }

destructor TSintegraDTO.Destroy;
begin
  GetRegistrations.Free;
  inherited;
end;

function TSintegraDTO.GetRegistrations: TObjectList<TRegistrationsDTO>;
begin
  if not Assigned(FRegistrations) then
  begin
    FRegistrations := TObjectList<TRegistrationsDTO>.Create;
    FRegistrations.AddRange(FRegistrationsArray);
  end;
  Result := FRegistrations;
end;

{ TRootDTO }

constructor TRootDTO.Create;
begin
  inherited;
  FRegistration := TRegistrationDTO.Create;
  FAddress := TAddressDTO.Create;
  FLegal_Nature := TLegal_NatureDTO.Create;
  FPrimary_Activity := TPrimary_ActivityDTO.Create;
  FSimples_Nacional := TSimples_NacionalDTO.Create;
  FSintegra := TSintegraDTO.Create;
  FFiles := TFilesDTO.Create;
  FMaps := TMapsDTO.Create;
end;

destructor TRootDTO.Destroy;
begin
  FRegistration.Free;
  FAddress.Free;
  FLegal_Nature.Free;
  FPrimary_Activity.Free;
  FSimples_Nacional.Free;
  FSintegra.Free;
  FFiles.Free;
  FMaps.Free;
  GetSecondary_Activities.Free;
  GetMembership.Free;
  GetPartnership.Free;
  inherited;
end;

function TRootDTO.GetSecondary_Activities: TObjectList<TSecondary_ActivitiesDTO>;
begin
  if not Assigned(FSecondary_Activities) then
  begin
    FSecondary_Activities := TObjectList<TSecondary_ActivitiesDTO>.Create;
    FSecondary_Activities.AddRange(FSecondary_ActivitiesArray);
  end;
  Result := FSecondary_Activities;
end;

function TRootDTO.GetMembership: TObjectList<TMembershipDTO>;
begin
  if not Assigned(FMembership) then
  begin
    FMembership := TObjectList<TMembershipDTO>.Create;
    FMembership.AddRange(FMembershipArray);
  end;
  Result := FMembership;
end;

function TRootDTO.GetPartnership: TObjectList<TPartnershipDTO>;
begin
  if not Assigned(FPartnership) then
  begin
    FPartnership := TObjectList<TPartnershipDTO>.Create;
    FPartnership.AddRange(FPartnershipArray);
  end;
  Result := FPartnership;
end;

end.
