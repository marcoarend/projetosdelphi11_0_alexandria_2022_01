object FmCNPJaConsulta: TFmCNPJaConsulta
  Left = 0
  Top = 0
  Caption = 'FmCNPJaConsulta'
  ClientHeight = 583
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 12
    Width = 29
    Height = 13
    Caption = 'Token'
  end
  object Label11: TLabel
    Left = 16
    Top = 36
    Width = 29
    Height = 13
    Caption = 'CNPJ:'
  end
  object Button1: TButton
    Left = 164
    Top = 32
    Width = 75
    Height = 27
    Caption = 'Pesquisa'
    TabOrder = 0
    OnClick = Button1Click
  end
  object txtCNPJ: TEdit
    Left = 48
    Top = 8
    Width = 953
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object MeJSON: TMemo
    Left = 20
    Top = 61
    Width = 981
    Height = 304
    Lines.Strings = (
      
        '{"last_update":"2021-11-19T13:34:48.096Z","name":"M L AREND & CI' +
        'A LTDA","alias":"DERMATEK","tax_id":"03143014000152","type":"MAT' +
        'RIZ","founded":"1999-05-05","size":"ME","capital":10000,"email":' +
        'null,"phone":"(44) 3226-5027","phone_alt":"(44) 9951-3333","fede' +
        'ral_entity":null,"registration":{"status":"ATIVA","status_date":' +
        '"2005-03-05","status_reason":null,"special_status":null,"special' +
        '_status_date":null},"address":{"street":"RUA DOUTOR SAULO PORTO ' +
        'VIRMOND","number":"1004","details":"SOBRADO 05","zip":"87005090"' +
        ',"neighborhood":"COND RESID CASTELVECCHIO","city":"MARING'#193'","sta' +
        'te":"PR","city_ibge":"4115200","state_ibge":"41"},"legal_nature"' +
        ':{"code":"2062","description":"Sociedade Empres'#225'ria Limitada"},"' +
        'primary_activity":{"code":"1510600","description":"Curtimento e ' +
        'outras prepara'#231#245'es de couro"},"secondary_activities":[{"code":"6' +
        '209100","description":"Suporte t'#233'cnico, manuten'#231#227'o e outros serv' +
        'i'#231'os em tecnologia da informa'#231#227'o"},{"code":"6202300","descriptio' +
        'n":"Desenvolvimento e licenciamento de programas de computador c'
      
        'ustomiz'#225'veis"}],"membership":[{"name":"MARCO LUCIANO AREND","tax' +
        '_id":"***717540**","role":{"code":"22","description":"S'#243'cio"}},{' +
        '"name":"CLEIDE MERLI AREND","tax_id":"***859889**","role":{"code' +
        '":"49","description":"S'#243'cio-Administrador"}},{"name":"MARCELO CR' +
        'ISTIAN WINTER","tax_id":"***563390**","role":{"code":"22","descr' +
        'iption":"S'#243'cio"}}],"partnership":[],"simples_nacional":{"last_up' +
        'date":"2021-11-19T13:34:48.096Z","simples_optant":true,"simples_' +
        'included":"2007-07-01","simples_excluded":null,"simei_optant":fa' +
        'lse},"sintegra":{"last_update":"2021-11-19T13:34:48.096Z","home_' +
        'state_registration":null,"registrations":[]},"files":{"registrat' +
        'ion":"https://api.cnpja.com/rfb/certificate?taxId=03143014000152' +
        '&pages=REGISTRATION&signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVC' +
        'J9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzO' +
        'TUiLCJ1cmwiOiIvcmZiL2NlcnRpZmljYXRlP3RheElkPTAzMTQzMDE0MDAwMTUyJ' +
        'nBhZ2VzPVJFR0lTVFJBVElPTiIsImlhdCI6MTYzNzMyODg5MSwiZXhwIjoxNjQ1M' +
        'TA0ODkxLCJhdWQiOiJodHRwczovL2NucGphLmNvbS8iLCJpc3MiOiJodHRwczovL'
      
        '2NucGphLmNvbS8ifQ.V8Jg094Z1Avu5FSJjLCTvIH3WQiVNfrpyNx3nSNOptViSF' +
        '7UtEh0BwTRNV6WodaURAq0UbSlOTBrqqo11kzCuU7GKnzGo0ZsfOyKet7ehL89OJ' +
        'yFN_eK5TaYOMTEjPWnbm3ffdZTEfU9Xuz7ooYU9yuiHTJ0UZN153fZ-e2Pijxt5e' +
        '6kkxSu-i32-2apY0qrGHtnL-nUNkbJhRziA7PHYe3MQ3kGs7dn1V2vGPEnWGTDVj' +
        'dTN7JZEm7LbQNKCkuvVgrG0-av2oM8dTYejy8V_jhUgdRBCXlLallpjNhu4wmqkT' +
        'Ny6twK_DrKwyUAr8cVVp3HhI-8YA7cOH6y3Q1daSYXIJ8L1X7vsYT0sVXJgS3hbD' +
        'jKwGLhIeY8fOiTXeCjzTplR07cB06g37rkMH6pbynW7ONKbCp8voUkDyxP0P3qpd' +
        'uOIQVbTop3WTWVLOXBToeRG2Aiod1199eCwO7o3_arMhuow1T-jei24Tr5gxDZzg' +
        'M4Qtb2ltEpAGcelPurHeyMZogQaq0CKgPP5VzHBlAnNsJcaon3E7N1w0Z6KZjgHi' +
        'gLrLBBB0wZ2u4rmlpe0AMksIIZNst6zDEmjdNy1n_thCK_iQogvVQw2CfqDmPKx5' +
        'yr4gtjUUepzmlgVyncmH9VjyGj7ODq1jlf5VTnEheveIJKLmxopcbk2rwGohI","' +
        'membership":"https://api.cnpja.com/rfb/certificate?taxId=0314301' +
        '4000152&pages=MEMBERS&signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpX' +
        'VCJ9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzU' +
        'zOTUiLCJ1cmwiOiIvcmZiL2NlcnRpZmljYXRlP3RheElkPTAzMTQzMDE0MDAwMTU' +
        'yJnBhZ2VzPU1FTUJFUlMiLCJpYXQiOjE2MzczMjg4OTEsImV4cCI6MTY0NTEwNDg'
      
        '5MSwiYXVkIjoiaHR0cHM6Ly9jbnBqYS5jb20vIiwiaXNzIjoiaHR0cHM6Ly9jbnB' +
        'qYS5jb20vIn0.RMO8N0dZmzdNcBjM6APS2B140vVLU_rR-PHQwgcU5dj2lMobA6s' +
        'Vy3saS1O8B2Mf5tDHujsTo_vAq_d94nyYleNsYp1WZXP7vG6-c7QQAsjlaJ8i8rw' +
        'JzsjgfbH-1nFlAn-Wu9d9kA2rfq_twkJQHP_ze0r8XiFoo0edTKt6qez-kKjAvVo' +
        'X6VQEVGkb6jFJLW5Ozn-cj7CKFtshSJ4pGiKoQVlobp1HH4mewFVDTgAUOGk4VBQ' +
        'LSuRI8i6SO0A2vKB3qDy2OSIZnHvN_10_yr2pjUJWfWXjfgKryP-OM7_dJ-JOv4b' +
        'v2Cd1wcS3fCM_iOgQd5Fbn0fjO8_hEZEjTY0pTbSB4EYRe23zYuJyyJo2RWgBJ9g' +
        'fq6moUPQY0jMCKf15DEYY4N86UPJeUiMoHGKpm2F0LFApRy4HTaUJ6D6lMVtWtZp' +
        'QZjraFyF9SuHYPhsaPSgub5CbnwiGy_lZcWEJJES5ypZyPuONgXTqb005ZAw9CO6' +
        'gCOA96QIKiGMYJSvpwAieMK4Qck50lbRWH-jy5Pll0bxGY6724KVl07u8YUIY3Gw' +
        'smJCEXBxzC_qcpH2EYOY5TPa5_DoCJT--bq5cpjMMdVZoLSnzIGrZwVTbt9OOwnr' +
        '2qtubp0apUdJI1-xrWrDthS7f3Pn3y3FfhOSjMq8aAWSzSc3P4l9CWj0"},"maps' +
        '":{"roads":"https://api.cnpja.com/office/03143014000152/map?sign' +
        'ature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1O' +
        'WNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzOTUiLCJ1cmwiOiIvb2ZmaWNlL' +
        'zAzMTQzMDE0MDAwMTUyL21hcCIsImlhdCI6MTYzNzMyODg5MSwiZXhwIjoxNjQ1M'
      
        'vCQhEd1FCukENKrQ5fR9s7Ng8YybqNeRGet5jUBIJFVJm2IUoPKjwHogiLGDU2uD' +
        'plKEWHtiNQAUvYYJFxHB4x8BSsisqfdGkrawIQ7KJCuoeBTv0CbpCWake5tPeH08' +
        'ilsNNcXU5UncIGdhQ8UIRjjqf6N4gVrrkiHUBg0eD27tgvGIuThDCmk2qf5kAoeQ' +
        'aSpOUHXF52r-h52DqVjDy1GbjkRPB_k5Ukc_u-_KCZzVaRVk_iSlytZSuazP1smP' +
        'jbypCYCu0d-sQGLfDgo7QiubqhZhoOgNl5owaihtgqTG3vWiGMtB508KtDjWJXL-' +
        '-jpv-ofHa_VDwNrWYVEcVwu3LwS5ItKAyUzd3zb_tmJd1z8Y2WcWIWp2U3RbEqI2' +
        'EvtZrTlSk8DmkIkINr3pNHL0exDOKSAkeCVHFIuItio8wWb5TvquALq3jpQ4ySls' +
        'BgBbA-B4T2PXnw0I7CEKnZB5yQ3dXuqk2DeMlCzBstI3QR-K0_5Y8mCOal3cbMM4' +
        '3d7nxcP1DZ7uXzjV0XIx6xCPmTXwBeaO-jo4xK6TvBSofB89WKzc_MtnHnFUTldn' +
        'M","satellite":"https://api.cnpja.com/office/03143014000152/map?' +
        'type=hybrid&signature=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c' +
        '2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZmUxYzUzOTUiLCJ1c' +
        'mwiOiIvb2ZmaWNlLzAzMTQzMDE0MDAwMTUyL21hcD90eXBlPWh5YnJpZCIsImlhd' +
        'CI6MTYzNzMyODg5MSwiZXhwIjoxNjQ1MTA0ODkxLCJhdWQiOiJodHRwczovL2Nuc' +
        'GphLmNvbS8iLCJpc3MiOiJodHRwczovL2NucGphLmNvbS8ifQ.Hk5YR8hQfipGTV' +
        'WauqhXN5MKGtoEmbyfvcwj8Hvnc7MFrw7bxmUY2n9I-2q3i16qyH8Tj0olidcXiT'
      
        'llUXn8cMum3ishkiZt8lV9sThug_VkLF8tds9NZf3A_iZ4iMq8qbIb1F_Sq9h6CV' +
        'd7QvE-yY2h_moF19K2yb2GBhxTntbsxdO-3urEpQJi3TKNMfzQdBbyKWm_k3n7j7' +
        'mErlwvcq4CM969liMAXBJSkkuF17nKNhdFZ1EYFoubGh8ZupvH7ZdOpMjNEViTSi' +
        'SwnMSSqxssklMLR452xbD8uiccqLwXidvFwsmUIoN3wRRWh9UWWwZmW1v-fCfmGr' +
        'xgK9kv7TBWW_7oHp12Oae9gMR3PDt_GE_UJPGVgzj6-lV_EvAO4f80ssJh3_vvbL' +
        '1fplpFB5dW1mpDpCrYMyxDXk-l3wzNi_BHmfYkGdWEqrInEL8wNJTFbfIFDol7W-' +
        'XQ6k9162tZ-R-GzkN0mvmze5x7mC1rYDaM20I-gbLkC2qH_B0ME1LBR07xt9weUz' +
        '9F-0VarhgutQ3g96YQ2BBF7vheziJTE28B_1UiHxhxWrz23ZjI0oAU08fIgNzgLm' +
        '7qbLmNfaJdupJmuEXxgk0j4jC0ICwsRwafwMBzJ_aciJpq4N-laX6LVm3yDaCqP8' +
        'we6EY7QQSQVkGshXJmdLGJyMi-NyM","street":"https://api.cnpja.com/o' +
        'ffice/03143014000152/street?signature=eyJhbGciOiJSUzI1NiIsInR5cC' +
        'I6IkpXVCJ9.eyJ1c2VySWQiOiIxY2Q1OWNkNC03NGUzLTRiYjYtOTdhYi0wMDdkZ' +
        'mUxYzUzOTUiLCJ1cmwiOiIvb2ZmaWNlLzAzMTQzMDE0MDAwMTUyL3N0cmVldCIsI' +
        'mlhdCI6MTYzNzMyODg5MSwiZXhwIjoxNjQ1MTA0ODkxLCJhdWQiOiJodHRwczovL' +
        '2NucGphLmNvbS8iLCJpc3MiOiJodHRwczovL2NucGphLmNvbS8ifQ.LSJnrwpqaj' +
        'gecPlhFm0rnP3lDUmQwCzSBQSAdP9aMDZvIPD15BcnQaeQv7tlhT5TGuzD6O00us'
      
        'gm5wjF3eE_PComU1E_ORmJDrBRQUMwm2ihzbetVDX_7_8dYKzyasG5NWxLGNz8yU' +
        'htCK2CehNfLIT6IuX9VJ3RIsh187TXR7JLOsl9j566Bj2CqjjpxFGqMwigWZpuql' +
        'tMUsUTkGZ5-_rHzam11-dqH_UmQxq4n4vb8Q28Ma71uZe4bLMR6mTvGC7tw-f6ZX' +
        'VMKWh7Mysn7s1alqstJCRZ4aXlJ3F2vWLQ1OrBpU1M2aRhd0PdNyQplss4Uz9O8g' +
        '4BQ_TPH8E8I6XhIAEMSH9kj6a02y-UO6Pf7LaNlRFP75OTnwgDehBsM4VF4i5Ayz' +
        '8ZtErsJII6a4QOnIf6zGeWHnDbx5FG9GjgmpAg2RHm1lhsnufjcuxkkoohwSJZlP' +
        'SXIw2NHWlXR0A2fx2ZewdVyF9ZeCJuV-vLxwDV4dKyCULFPWc2cHppjSqebyaLwz' +
        'k575yPe89EKYrvU5Ly3ksAKnJkxTkBWRvQCZ5YLkzlw6xVSSqOH5_f54jtgeSPTJ' +
        '5NFiYt_gRu4qJZB8dHz1olJeFbkYxhgev92UqratYZ2rWQGp8Xm46zIXGwPR_R-u' +
        'QjxrKoM50J4T8jpkgIDlQi4XuNdb4qHng"}}')
    TabOrder = 2
    WordWrap = False
  end
  object Button2: TButton
    Left = 702
    Top = 384
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 3
    OnClick = Button2Click
  end
  object EdCNPJ: TdmkEdit
    Left = 48
    Top = 32
    Width = 113
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    FormatType = dmktfString
    MskType = fmtCPFJ
    DecimalSize = 0
    LeftZeros = 0
    NoEnterToTab = False
    NoForceUppercase = False
    ForceNextYear = False
    DataFormat = dmkdfShort
    HoraFormat = dmkhfShort
    QryCampo = 'CNPJ'
    UpdCampo = 'CNPJ'
    UpdType = utYes
    Obrigatorio = False
    PermiteNulo = False
    ValueVariant = ''
    ValWarn = False
  end
  object IdHTTP: TIdHTTP
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 232
    Top = 144
  end
  object OpenSSL: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 360
    Top = 144
  end
end
