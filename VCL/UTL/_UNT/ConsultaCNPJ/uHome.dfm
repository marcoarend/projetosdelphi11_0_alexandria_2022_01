object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 583
  ClientWidth = 1023
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 256
    Top = 32
    Width = 75
    Height = 27
    Caption = 'Search'
    TabOrder = 0
    OnClick = Button1Click
  end
  object txtCNPJ: TEdit
    Left = 16
    Top = 32
    Width = 234
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object Memo1: TMemo
    Left = 16
    Top = 65
    Width = 761
    Height = 304
    Lines.Strings = (
      
        '{"last_update":"2021-04-23T09:19:19.000Z","name":"RAYANNE MORAES' +
        ' DOS SANTOS DA SILVA 02962757197","alias":"LION '
      
        'TECNOLOGIA","tax_id":"20760428000140","type":"MATRIZ","founded":' +
        '"2014-08-'
      
        '02","size":"ME","capital":30000,"email":"rayanne@liontecnologia.' +
        'com.br","phone":"(66) 9957-0948","phone_alt":null,"federal_entit' +
        'y":null,"registration":'
      
        '{"status":"ATIVA","status_date":"2014-08-02","status_reason":nul' +
        'l,"special_status":null,"special_status_date":null},"address":{"' +
        'street":"AV DOS '
      
        'PAVOES","number":"1144W","details":null,"zip":"78450000","neighb' +
        'orhood":"LOT.COLINA II","city":"NOVA '
      
        'MUTUM","state":"MT","city_ibge":"5106224","state_ibge":"51"},"le' +
        'gal_nature":{"code":"2135","description":"Empres'#225'rio (Individual' +
        ')"},"primary_activity":'
      
        '{"code":"4751201","description":"Com'#233'rcio varejista especializad' +
        'o de equipamentos e suprimentos de inform'#225'tica"},"secondary_acti' +
        'vities":'
      
        '[{"code":"4321500","description":"Instala'#231#227'o e manuten'#231#227'o el'#233'tri' +
        'ca"},{"code":"4752100","description":"Com'#233'rcio varejista especia' +
        'lizado de equipamentos '
      
        'de telefonia e comunica'#231#227'o"},{"code":"4759899","description":"Co' +
        'm'#233'rcio varejista de outros artigos de uso dom'#233'stico n'#227'o especifi' +
        'cados anteriormente"},'
      
        '{"code":"6190699","description":"Outras atividades de telecomuni' +
        'ca'#231#245'es n'#227'o especificadas anteriormente"},{"code":"7319002","desc' +
        'ription":"Promo'#231#227'o de '
      
        'vendas"},{"code":"8599603","description":"Treinamento em inform'#225 +
        'tica"},{"code":"9511800","description":"Repara'#231#227'o e manuten'#231#227'o d' +
        'e computadores e '
      
        'de equipamentos perif'#233'ricos"}],"membership":[],"partnership":[],' +
        '"simples_nacional":{"last_update":"2021-04-'
      
        '23T09:19:19.681Z","simples_optant":true,"simples_included":"2014' +
        '-08-02","simples_excluded":null,"simei_optant":true},"sintegra":' +
        '{"last_update":"2021-'
      
        '04-23T09:19:06.734Z","home_state_registration":"00135507197","re' +
        'gistrations":[{"number":"00135507197","state":"MT","enabled":tru' +
        'e}]},"files":'
      
        '{"registration":"https://api.cnpja.com.br/files/eyJ1IjoiMjdlMTE2' +
        'YzQtMjMxMS00YjUzLWI2MTItMDFjNGEyMDdiOTY1IiwieCI6IjIwNzYwNDI4MDAw' +
        'MTQwIiwidCI'
      
        '6IkNSIn0","membership":"https://api.cnpja.com.br/files/eyJ1IjoiM' +
        'jdlMTE2YzQtMjMxMS00YjUzLWI2MTItMDFjNGEyMDdiOTY1IiwieCI6IjIwNzYwN' +
        'DI4MDAwMT'
      'QwIiwidCI6IkNNIn0"},"maps":'
      
        '{"roads":"https://api.cnpja.com.br/files/eyJ1IjoiMjdlMTE2YzQtMjM' +
        'xMS00YjUzLWI2MTItMDFjNGEyMDdiOTY1IiwieCI6IjIwNzYwNDI4MDAwMTQwIiw' +
        'idCI6IkdSI'
      
        'n0","satellite":"https://api.cnpja.com.br/files/eyJ1IjoiMjdlMTE2' +
        'YzQtMjMxMS00YjUzLWI2MTItMDFjNGEyMDdiOTY1IiwieCI6IjIwNzYwNDI4MDAw' +
        'MTQwIiwidCI6'
      
        'IkdTIn0","street":"https://api.cnpja.com.br/files/eyJ1IjoiMjdlMT' +
        'E2YzQtMjMxMS00YjUzLWI2MTItMDFjNGEyMDdiOTY1IiwieCI6IjIwNzYwNDI4MD' +
        'AwMTQwIiwid'
      'CI6IkdUIn0"}}')
    TabOrder = 2
  end
  object Button2: TButton
    Left = 702
    Top = 384
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 3
    OnClick = Button2Click
  end
  object IdHTTP: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 232
    Top = 144
  end
  object OpenSSL: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 360
    Top = 144
  end
end
