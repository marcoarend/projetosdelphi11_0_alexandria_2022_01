unit uHome;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdIOHandler, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, IdSSLOpenSSL, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    txtCNPJ: TEdit;
    IdHTTP: TIdHTTP;
    OpenSSL: TIdSSLIOHandlerSocketOpenSSL;
    Memo1: TMemo;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Pkg.Json.DTO, RootUnit;

procedure TForm1.Button1Click(Sender: TObject);
var
  vResult: string;
begin

  vResult := '';

  OpenSSL.SSLOptions.Method := sslvTLSv1_2;
  OpenSSL.SSLOptions.Mode := sslmClient;
  OpenSSL.PassThrough := True;
  IdHTTP.IOHandler := OpenSSL;

  IdHTTP.Request.UserAgent :=
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
  IdHTTP.Request.Accept := 'application/json, text/javascript, */*; q=0.01';
  IdHTTP.Request.ContentType := 'application/json';
  IdHTTP.Request.CharSet := 'utf-8';

{
  IdHTTP.Request.CustomHeaders.AddValue('authorization',
    'SUA CHAVE AQUI');
}
  IdHTTP.Request.CustomHeaders.AddValue('authorization',
    '5f151f32-5617-4cf4-951c-be1d36088aa3-b055420d-7f41-4755-9d5c-78809f40bb56');

  try

    vResult := IdHTTP.Get('https://api.cnpja.com.br/companies/' + txtCNPJ.Text +
      '?sintegra_max_age=15&simples_max_age=15');

    Memo1.Lines.Clear;
    Memo1.Text := vResult;

  except
    on E: EIdHTTPProtocolException do
    Begin
      IdHTTP.Disconnect;
      ShowMessage(E.ErrorMessage);
    End;
  end;

end;

procedure TForm1.Button2Click(Sender: TObject);
var
  Root: TRootDTO;
  I,J:Integer;
begin
   Root := TRootDTO.Create;
  try
    Root.AsJson := Memo1.Lines.Text;

    ShowMessage(Root.Sintegra.Home_State_Registration);

    for I := 0 to Root.Sintegra.Registrations.Count - 1 do
    begin
        ShowMessage(Root.Sintegra.Registrations[I].Number);
    end;

    for J := 0 to Root.Secondary_Activities.Count - 1 do
    begin
        ShowMessage(Root.Secondary_Activities[J].Description);
    end;


  finally
    Root.Free;
  end;
end;

end.
