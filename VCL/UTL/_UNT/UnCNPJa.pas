unit UnCNPJa;
//   Token obtido em 16/11/2021 com cadastro no CNPJa com a conta do google mlarend2012@gmail.com
//   5f151f32-5617-4cf4-951c-be1d36088aa3-b055420d-7f41-4755-9d5c-78809f40bb56


interface

uses Classes, StdCtrls, Forms, Controls, dmkGeral, ExtCtrls, Windows, SysUtils,
  Dialogs, Messages, dmkEdit, dmkDBLookupComboBox, dmkEditCB, mySQLDbTables,
  Variants, UnDmkEnums;

type
  TUnConsultasWeb = class(TObject)
  public
    { Public declarations }
  end;

var
  UConsultasWeb: TUnConsultasWeb;

implementation

uses MyDBCheck, Module, UnCEP, ModuleGeral, DmkDAC_PF,
  {$IfNDef SemEntidade1} EntiMapa, EntiRFCPF, EntiRFCNPJ, EntiRE_IE, {$EndIf}
  UnInternalConsts, UnDmkWeb, UnGrl_DmkREST, UnServices;

{ TUnConsultasWeb }


function TUnConsultasWeb.ConsultaCNPJa(EdCNPJ, EdIE, EdRazaoSocial, EdFantasia,
  EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF,
  EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
  CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
  EdCodCNAE: TdmkEditCB): Boolean;
var
  Cnpj, Json: String;
  ObjCnpj: TCnpj;
begin
{$IfNDef SemEntidade1}
  Cnpj := EdCNPJ.ValueVariant;
  //
  if Cnpj = '' then
  begin
    Geral.MB_Aviso('CNPJ n�o informado!');
    EdCNPJ.SetFocus;
    //
    Result := True;
    Exit;
  end;
  //
  Result := False;
  //
  Screen.Cursor := crHourGlass;
  //
  try
    if Grl_DmkREST.ObtemDadosCnpj(False, Cnpj, Json) then
    begin
      ObjCnpj := TCnpj.Create(Json);
      //
      if ObjCnpj.Cnpj = '' then
      begin
        Geral.MB_Aviso('CNPJ n�o localizado!');
        Screen.Cursor := crDefault;
        exit;
      end;
      //
      if EdCNPJ <> nil then
        EdCNPJ.ValueVariant := ObjCnpj.Cnpj;
      if EdRazaoSocial <> nil then
        EdRazaoSocial.ValueVariant := ObjCnpj.RazaoSocial;
      if EdFantasia <> nil then
        EdFantasia.ValueVariant := ObjCnpj.Fantasia;
      if EdCEP <> nil then
        EdCEP.ValueVariant := ObjCnpj.Cep;
      if EdNumero <> nil then
      try
          EdNumero.ValueVariant := ObjCnpj.Numero
      except
          EdNumero.ValueVariant := 0;
      end;
      if EdCompl <> nil then
        EdCompl.ValueVariant := ObjCnpj.Complemento;
      if EdCodCNAE <> nil then
        EdCodCNAE.ValueVariant := Geral.SoNumero_TT(ObjCnpj.CnaeId);
      if CBCodCNAE <> nil then
        CBCodCNAE.KeyValue := Geral.SoNumero_TT(ObjCnpj.CnaeId);
      //
      U_CEP.ConsultaCEP4(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
        EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
      //
      EdIE.SetFocus;
    end;
  except
    ;
  end;
  //
  Screen.Cursor := crDefault;
  //
  Result := True;
{$EndIf}
end;



end.
