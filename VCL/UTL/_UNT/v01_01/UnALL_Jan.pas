unit UnALL_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  UnInternalConsts;

type
  TUnALL_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    //{$IfNDef NAO_USA_DB_GERAL}
    procedure MostraFormVerifiDB(Verifi: Boolean);
    //{$EndIf}

    {$IfNDef NAO_USA_MyDBCheck}
    procedure MostraFormLogOff(FmMLA: TForm; DataBases: array of TmySQLDatabase);
    {$EndIf}

    {$IfNDef SemBDTErceiros}
    procedure MostraFormVerifiDBTerceiros(Verifi: Boolean);
    {$EndIf}
  end;

var
  ALL_Jan: TUnALL_Jan;


implementation

uses
{$IfNDef SemBDTErceiros} VerifiDBTerceiros, {$EndIf}
{$IfNDef NAO_USA_MyDBCheck} MyDBCheck, LogOff, {$EndIf}
{$IfNDef NAO_USA_DB_GERAL} VerifiDB, ModuleGeral, {$EndIf}
Module;

{ TUnALL_Jan }

{$IfNDef NAO_USA_MyDBCheck}
procedure TUnAll_Jan.MostraFormLogOff(FmMLA: TForm; DataBases: array of TmySQLDatabase);
var
  I: Integer;
begin
  if (FmLogOff = nil) or (not FmLogOff.Showing) then
  begin
    if (FmMLA.Showing) then
      Exit;
    //
    if DBCheck.CriaFm(TFmLogOff, FmLogOff, afmoNegarComAviso) then
    begin
      if Length(DataBases) > 0 then
      begin
        SetLength(FmLogOff.FDataBases, Length(DataBases));
        for I := Low(DataBases) to High(DataBases) do
          FmLogOff.FDataBases[I] := DataBases[I];
      end;
      FmLogOff.ShowModal;
      FmLogOff.Destroy;
    end;
  end;
end;
{$EndIf}

procedure TUnALL_Jan.MostraFormVerifiDB(Verifi: Boolean);
begin
{$IfNDef NAO_USA_DB_GERAL}
  if VAR_IN_VERIFI_DB then
    Exit
  else
  begin
    try
      VAR_IN_VERIFI_DB := True;
      DModG.VerificaDB();
    finally
      VAR_IN_VERIFI_DB := False;
    end;
  end;
{$EndIf}
end;

{$IfNDef SemBDTErceiros}
procedure TUnALL_Jan.MostraFormVerifiDBTerceiros(Verifi: Boolean);
begin
  Application.CreateForm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros);
  with FmVerifiDBTerceiros do
  begin
    FVerifi := Verifi;
    ShowModal;
    FVerifi := False;
    Destroy;
  end;
end;
{$EndIf}

end.
