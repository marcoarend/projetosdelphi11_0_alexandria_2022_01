unit ResIntStrings;

interface

resourcestring
  SInvalidFieldSize = 'Tamanho de campo inv�lido';  //'Invalid field size';
  SInvalidFieldKind = 'Tipo de campo inv�lido (FieldKind)';  //'Invalid FieldKind';
  SInvalidFieldRegistration = 'Registro de campo inv�lido';  //Invalid field registration';
  SUnknownFieldType = 'O campo ''%s'' � de um tipo desconhecido';  //'Field ''%s'' is of an unknown type';
  SFieldNameMissing = 'Est� faltando o nome do campo';  //'Field name missing';
  SDuplicateFieldName = 'Nome duplicado do campo ''%s''';  //'Duplicate field name ''%s''';
  SFieldNotFound = 'O campo ''%s'' n�o foi encontrado';  //'Field ''%s'' not found';
  SFieldAccessError = 'Imposs�vel acessar o campo ''s'' como o tipo %s';  //'Cannot access field ''%s'' as type %s';
  SFieldValueError = 'Valor inv�lido para o campo ''%s''';  //'Invalid value for field ''%s''';
  SFieldRangeError = '%g n�o � um valor v�lido para o campo ''%s''. O tamanho permitido � %g at� %g';
    //'%g is not a valid value for field ''%s''. The allowed range is %g to %g';
  SInvalidIntegerValue = '''%s'' n�o � um valor inteiro v�lido para o campo ''%s''';
  //'''%s'' is not a valid integer value for field ''%s''';
  SInvalidBoolValue = '''%s'' n�o � um valor booleano v�lido para o campo ''%s''';
  //'''%s'' is not a valid boolean value for field ''%s''';
  SInvalidFloatValue = '''%s'' n�o � um valor de ponto flutuante v�lido para o campo ''%s''';
  //'''%s'' is not a valid floating point value for field ''%s''';
  SFieldTypeMismatch = 'Tipos diferentes para o campo ''%s'', esperado: %s atual: %s';
  //'Type mismatch for field ''%s'', expecting: %s actual: %s';
  SFieldSizeMismatch = 'Tipos diferentes para o campo ''%s'', esperado: %d atual: %d';
  //'Size mismatch for field ''%s'', expecting: %d actual: %d';
  SInvalidVarByteArray = 'Tipo de vari�vel ou tamanho inv�lido para o campo ''%s''';
  //'Invalid variant type or size for field ''%s''';
  SFieldOutOfRange = 'O valor do campo ''%s'' extrapolou seu tamanho';
  //'Value of field ''%s'' is out of range';
  SBCDOverflow = '(excesso)';  //'(Overflow)';
  SFieldRequired = 'O campo ''%s'' deve possuir um valor';  //'Field ''%s'' must have a value';
  SDataSetMissing = 'O campo ''%s'' n�o possui dataset';  //'Field ''%s'' has no dataset';
  SInvalidCalcType = 'O campo ''%s'' n�o pode ser do tipo calculado ou "lookup"';
  //'Field ''%s'' cannot be a calculated or lookup field';
  SFieldReadOnly = 'O campo ''%s"" n�o pode ser modificado'; //'Field ''%s'' cannot be modified';
  SFieldIndexError = 'O �ndice do campo extrapolou o tamanho m�ximo'; //'Field index out of range';
  SNoFieldIndexes = 'N�o existe nenhum �ndice ativo agora';//'No index currently active';
  SNotIndexField = 'O campo ''%s'' n�o � indexado e n�o pode ser modificado';
  //'Field ''%s'' is not indexed and cannot be modified';
  SIndexFieldMissing = 'Imposs�vel acessar o �ndice do campo ''%s''';  //'Cannot access index field ''%s''';
  SDuplicateIndexName = 'Nome do �ndice ''%s'' foi duplicado';  //'Duplicate index name ''%s''';
  SNoIndexForFields = 'N�o existe �ndice para o campo ''%s''';  //'No index for fields ''%s''';
  SIndexNotFound = 'O �ndice ''%s'' n�o foi encontrado';  //'Index ''%s'' not found';
  SDuplicateName = 'Nome duplicado ''%s'' em %s';  //'Duplicate name ''%s'' in %s';
  SCircularDataLink = 'N�o � permitido fazer refer�ncia circular em links de dados';
  //'Circular datalinks are not allowed';
  SLookupInfoError = 'A informa��o de "lookup" para o campo ''%s'' est� incompleta';
  //'Lookup information for field ''%s'' is incomplete';
  SDataSourceChange = 'O DataSource n�o pode ser modificado';  //'DataSource cannot be changed';
  SNoNestedMasterSource = 'Datasets aninhados n�o podem possuir um MasterSource';
  //'Nested datasets cannot have a MasterSource';
  SDataSetOpen = 'Imposs�vel realizar esta opera��o em um dataset aberto';
  //'Cannot perform this operation on an open dataset';
  SNotEditing = 'O dataset n�o est� em modo de edi��o ou inser��o';
  //'Dataset not in edit or insert mode';
  SDataSetClosed = 'Imposs�vel realizar esta opera��o em um dataset fechado';
  //'Cannot perform this operation on a closed dataset';
  SDataSetEmpty = 'Imposs�vel realizar esta opera��o em um dataset vazio';
  //'Cannot perform this operation on an empty dataset';
  SDataSetReadOnly = 'Imposs�vel modificar um dataset somente leitura';
  //'Cannot modify a read-only dataset';
  SNestedDataSetClass = 'Datasets aninhados necessitam ser herdeiros de %s';
  //'Nested dataset must inherit from %s';
  SExprTermination = 'Express�o de filtro terminada incorretamente';
  //'Filter expression incorrectly terminated';
  SExprNameError = 'Nome da campo indeterminado';  //'Unterminated field name';
  SExprStringError = 'String constant indeterminada';  //'Unterminated string constant';
  SExprInvalidChar = 'O caracter ''%s'' � inv�lido na express�o de filtro';
  //'Invalid filter expression character: ''%s''';
  SExprNoLParen = 'Era esperado ''('' mas foi encontrado %s';  //'''('' expected but %s found';
  SExprNoRParen = 'Era esperado '')'' mas foi encontrado %s';  //''')'' expected but %s found';
  SExprNoRParenOrComma = 'Era esperado ''('' ou '')'' mas foi encontrado %s';
  //''')'' or '','' expected but %s found';
  SExprExpected = 'Era esperado uma express�o mas foi encontrado %s';  //'Expression expected but %s found';
  SExprBadField = 'O campo ''%s'' n�o pode ser usado em uma express�o de filtro';
  //'Field ''%s'' cannot be used in a filter expression';
  SExprBadNullTest = 'NULL somente permitido com ''='' e ''<>''';
  //'NULL only allowed with ''='' and ''<>''';
  SExprRangeError = 'A constante extrapolou o tamanho';  //'Constant out of range';
  SExprNotBoolean = 'O campo ''%s'' n�o � do tipo booleano';  //'Field ''%s'' is not of type Boolean';
  SExprIncorrect = 'Forma��o incorreta da express�o de filtro';  //'Incorrectly formed filter expression';
  SExprNothing = 'nada';  //'nothing';
  SExprTypeMis = 'Tipos diferentes na express�o';  //'Type mismatch in expression';
  SExprBadScope = 'A opera��o n�o pode misturar valores agregados com valores "record-varying"';
  //'Operation cannot mix aggregate value with record-varying value';
  SExprNoArith = 'Aritm�tica n�o suportada na express�o de filtro';
  //'Arithmetic in filter expressions not supported';
  SExprNotAgg = 'A express�o n�o � uma express�o de agrega��o';
  //'Expression is not an aggregate expression';
  SExprBadConst = 'A constante n�o est� correta para o tipo %s';  //'Constant is not correct type %s';
  SExprNoAggFilter = 'Express�es de agrega��o n�o s�o permitidas em filtros';
  //'Aggregate expressions not allowed in filters';
  SExprEmptyInList = '"IN" n�o pode estar vazio na lista de atributos';  //'IN predicate list may not be empty';
  SInvalidKeywordUse = 'Uso inv�lido de palavra reservada';  //'Invalid use of keyword';
  STextFalse = 'Falso';  //'False';
  STextTrue = 'Verdadeiro';  //'True';
  SParameterNotFound = 'O par�metro ''%s'' n�o foi localizado';  //'Parameter ''%s'' not found';
  SInvalidVersion = 'N�o � possivel carregar os par�metros de liga��o';  //'Unable to load bind parameters';
  SParamTooBig = 'O par�metro ''%s'' n�o pode salvar dados com tamanho superior a ''%d'' bytes';
  //'Parameter ''%s'', cannot save data larger than %d bytes';
  SBadFieldType = 'O campo ''%s'' � de um tipo n�o suportado';  //'Field ''%s'' is of an unsupported type';
  SAggActive = 'A propriedade talvez n�o modofique enquanto a agrega��o estiver ativa';
  //'Property may not be modified while aggregate is active';
  SProviderSQLNotSupported = 'SQL n�o suportada: %s';  //'SQL not supported: %s';
  SProviderExecuteNotSupported = 'Execu��o n�o suportada: %s';  //'Execute not supported: %s';
  SExprNoAggOnCalcs = 'O campo ''%s'' n�o � do tipo correto de campo calculado para ser usado in agrega��o, use um "internalcalc"';
  //'Field ''%s'' is not the correct type of calculated field to be used in an aggregate, use an internalcalc';
  SRecordChanged = 'Registro modificado por outro usu�rio';  //'Record changed by another user';

  { DBCtrls }
  SFirstRecord = 'Primeiro registro';  //'First record';
  SPriorRecord = 'Registro anterior';  //'Prior record';
  SNextRecord = 'Pr�ximo registro';  //'Next record';
  SLastRecord = '�ltimo registro';  //'Last record';
  SInsertRecord = 'Inserir registro';  //'Insert record';
  SDeleteRecord = 'Excluir o registro';  //'Delete record';
  SEditRecord = 'Editar o registro';  //'Edit record';
  SPostEdit = 'Salvar o registro';  //'Post edit';
  SCancelEdit = 'Cancelar a edi��o';  //'Cancel edit';
  SRefreshRecord = 'Atualizar os dados';  //'Refresh data';
  SDeleteRecordQuestion = 'Excluir o registro?';  //'Delete record?';
  SDeleteMultipleRecordsQuestion = 'Excluir todos os registros selecionados?';
  //'Delete all selected records?';
  SRecordNotFound = 'Registro n�o localizado';  //'Record not found';
  SDataSourceFixed = 'Opera��o n�o permitida em um DBCtrlGrid';
  //'Operation not allowed in a DBCtrlGrid';
  SNotReplicatable = 'Este controle n�o pode ser usado em um DBCtrlGrid';
  //'Control cannot be used in a DBCtrlGrid';
  SPropDefByLookup = 'Propriedade j� definida pelo campo "lookup"';
  //'Property already defined by lookup field';
  STooManyColumns = 'O Grid requer visualiza��o de mais de 256 colunas';
  //'Grid requested to display more than 256 columns';

  { DBLogDlg }
  SRemoteLogin = 'Login Remoto';  //'Remote Login';

  { DBOleEdt }
  SDataBindings = 'Mesclando dados...';  //'Data Bindings...';

  Const
  {APLICATIVO}
  SMLA_APP = 'Dermatek Backup';
  SMLA_1ASPA = '"';
  SMLA_PONTO = '.';
  N = sLineBreak;
  SMLA_ORIGINALTRUE = 'Este campo n�o pode mais ser modificado.';
  SMLA_INFOEXPORTAR = 'Informe a quantidade (kg) a exportar:';
  SMLA_PESOINVALIDO = 'Peso inv�lido. O peso deve ser maior que 0 e menor que ';
  SMLA_MAIORQZERO = 'Valor deve ser maior que zero';
  SMLA_NUMEROINVALIDO = '" n�o � um n�mero v�lido!';
  SMLA_NUMEROINVALIDO2 = 'N�mero inv�lido';
  SMLA_VLRINVRESULTNEG = 'Valor inv�lido. O resultado n�o pode ser negativo.';
  SMLA_AVISOFORMCLOSE = 'Antes de fechar o formul�rio trave a ';
  SMLA_APPEAVISO = SMLA_APP+' - Aviso';
  SMLA_NUMINVCOR = ' n�o � um n�mero v�lido para a cor.';
  SMLA_NUMINVOS = ' n�o � um n�mero v�lido para a O.S.';
  SMLA_NUMINVQTDE = ' n�o � um n�mero v�lido para a quantidade.';
  SMLA_NUMINVPESO = ' n�o � um n�mero v�lido para o peso.';
  SMLA_NUMINVARTIGO = ' n�o � um n�mero v�lido para o artigo.';
  SMLA_NUMINVRECEITA = ' n�o � um n�mero v�lido para a receita.';
  SMLA_ORDEMATUALPOS = '"Ordem atual" deve ser um n�mero inteiro positivo!';
  SMLA_NOVAORDEMPOS = '"Nova ordem" deve ser um n�mero inteiro positivo!';
  SMLA_NUMINTEIRO = 'N�mero deve ser inteiro';
  SMLA_RESRTRICAOVAL = 'Restri��o de valor';
  SMLA_TINTAINVALIDA = 'N�mero de tinta inv�lido. N�o existe composi��o para esta tinta.';
  SMLA_TINTAINVALIDA2 = 'N�mero de tinta inv�lido. Valor deve ser entre 1 e 6.';
  SMLA_NUMLINHAS = 'N�mero de linhas em receita vazia';
  SMLA_NUMLACTO = 'Digite o n�mero do lan�amento.';
  SMLA_PARTEINSUMO = 'Digite parte da descri��o (nome) do insumo.';
  SMLA_PARTEMPRIMA = 'Digite parte da descri��o da mat�ria-prima.';
  SMLA_PARTEFORNECEDOR = 'Digite parte da descri��o do fornecedor.';
  SMLA_PARTECLIENTE = 'Digite parte da descri��o do cliente.';
  SMLA_PARTEARTIGO = 'Digite parte da descri��o do artigo.';
  SMLA_PARTEITEM = 'Digite parte da descri��o do item.';
  SMLA_PARTERECEITA = 'Digite parte da descri��o da receita.';
  SMLA_PARTEMAQUINA = 'Digite parte da descri��o da m�quina.';
  SMLA_PARTEFUNCAO = 'Digite parte da descri��o da fun��o.';
  SMLA_PARTEOPERACAO = 'Digite parte da descri��o da opera��o.';
  SMLA_PARTEPECA = 'Digite parte da descri��o da pe�a.';
  SMLA_PARTEESPESSURA = 'Digite parte da descri��o da espessura.';
  SMLA_PARTEFUNCIONARIO = 'Digite parte da descri��o do funcion�rio.';
  SMLA_PARTETINGIMENTO = 'Digite parte da descri��o do tingimento.';
  SMLA_PARTETINTA = 'Digite parte da descri��o da tinta.';
  SMLA_PARTECOR = 'Digite parte da descri��o da cor.';
  SMLA_PARTETURNO = 'Digite parte da descri��o do turno.';
  SMLA_PARTEFONTEHIDRICA = 'Digite parte da descri��o da fonte hidrica.';
  SMLA_PARTEFONTEAR = 'Digite parte da descri��o da fonte de ar comprimido.';
  SMLA_PARTECLASSE = 'Digite parte da descri��o da classe.';
  SMLA_PARTEUF = 'Digite parte da descri��o do estado (UF).';
  SMLA_PARTEIMPOSTOS = 'Digite parte da descri��o do conjunto de impostos.';
  SMLA_PARTEREPRESENTANTE = 'Digite parte da descri��o do representante.';
  SMLA_MARCA = 'Digite a marca a localizar.';
  SMLA_EXPORTACAOOK = 'Exporta��o efetuada com sucesso!';
  SMLA_ALTERABORTPROD = 'Altera��o abortada. O produto n� ';
  SMLA_ALTERABORTLOTE = 'Altera��o abortada. O lote n� ';
  SMLA_NAOLOCALIZADO = ' n�o foi localizado.';
  SMLA_BAIXCANCPROD = 'Baixa de estoque cancelada.'+N+'O produto n� ';
  SMLA_BAIXCANCLOTE = 'Baixa de estoque cancelada.'+N+'O lote n� ';
  SMLA_BAIXAESTQCANC = 'Baixa de estoque cancelada.';
  SMLA_PESAGEMERECEIT = 'Pesagem e Receitu�rio';
  SMLA_SELECOS = '� obrigat�rio selecionar uma O.S.';

implementation

end.

