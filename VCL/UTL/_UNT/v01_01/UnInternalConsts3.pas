unit UnInternalConsts3;

interface

uses  Windows, Controls;

var
  IC3_ED_Controle, IC3_ED_Doc, IC3_ED_VALEMPRESAV, IC3_ED_VALEMPRESAP : Double;
  IC3_ED_FatNum, IC3_ED_NF: Integer;
  IC3_ED_SerieNF: String = '';
  IC3_ED_RECIBO, IC3_ED_DIARECIBO: ShortInt;
  IC3_ED_Data, IC3_ED_Vencto: TDate;

  //Impressao  em DOS
  IC3_ImpDOS_Conta, IC3_ImpDOS_Controle, IC3_ImpDOS_Obs, IC3_ImpDOS_Natureza,
  IC3_ImpDOS_CFOP, IC3_ImpDOS_Data, IC3_ImpDOS_DataSai, IC3_ImpDOS_HoraSai,
  IC3_ImpDOS_Codigo, IC3_ImpDOS_NFValor, IC3_ImpDOS_NFTotal, IC3_ImpDOS_CIF,
  IC3_ImpDOS_Placa, IC3_ImpDOS_Nome, IC3_ImpDOS_QtdTransp, IC3_ImpDOS_Especie,
  IC3_ImpDOS_Marca, IC3_ImpDOS_Numero, IC3_ImpDOS_UFNOME, IC3_ImpDOS_NFPesoB,
  IC3_ImpDOS_NFPesoL, IC3_ImpDOS_MP, IC3_ImpDOS_CF, IC3_ImpDOS_SitTrib,
  IC3_ImpDOS_NOMEPROD, IC3_ImpDOS_UN, IC3_ImpDOS_Unidade, IC3_ImpDOS_PesoL,
  IC3_ImpDOS_Pecas, IC3_ImpDOS_M2, IC3_ImpDOS_P2, IC3_ImpDOS_PRECO,
  IC3_ImpDOS_ValorProd, IC3_ImpDOS_FatParcela, IC3_ImpDOS_Vencimento,
  IC3_ImpDOS_ValorInvoi: ShortString;

  //Fim Impressao em DOS

implementation

end.
