unit UnitWin;
{   https://www.swissdelphicenter.ch/en/showcode.php?id=410
==============================================================
...enumerate windows and child windows?
Autor: Thomas Stutz
[ Print tip ]

Tip Rating (33):
}

interface

uses
  //Winapi.Windows,
  Winapi.ShellApi, Controls,
  Windows, Messages, Forms, SysUtils, Classes, Menus, ComCtrls,
  //UnDmkEnums, DB, mySQLDbTables, UnInternalConsts, UnAppEnums
  Grids, DBGrids, Variants,
  DmkGeral, UnDmkWeb;

{**********************************************}
{  Exemplo 1
(*    function EnumChildWindowsProc(Wnd: HWnd; Form: TForm; TreeView: TTreeView): Bool; export;{$ifdef Win32} (*stdcall; {$endif}*)
(*    function EnumWindowsProc(Wnd: HWnd; Form: TForm; TreeView: TTreeView): Bool; export; {$ifdef Win32} stdcall; {$endif}*)
{**********************************************}
type
  PWindows = ^TWindows;
  TWindows = record
    WindowHandle: HWND;
    WindowText: string;
  end;

// Exemplo 2
{**********************************************}
{  Other Code by NicoDE
{**********************************************}

type
  PMyEnumParam = ^TMyEnumParam;
  TMyEnumParam = record
    Nodes: TTreeNodes;
    Current: TTreeNode;
  end;

type
  TUnitWin = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    // Exemplo 1
    //function EnumChildWindowsProc(Wnd: HWnd; Form: TForm; TreeView: TTreeView): Bool; export;{$ifdef Win32} stdcall; {$endif}
    //function EnumWindowsProc(Wnd: HWnd; Form: TForm; TreeView: TTreeView): Bool; export; {$ifdef Win32} stdcall; {$endif}
    // Exemplo 2
     function  EnumWindowsProc(Wnd: HWND; Param: PMyEnumParam): BOOL; stdcall;
     //
     procedure OpcoesDaInternet(Form: TForm; E_Message: WideString;
               PerguntaSobreTutorial, PerguntaSobreOpcoesInternet, PerguntaSobreSnapIn:
               Boolean); // OpcoesInternet InternetOptions Internet Options Opcoes da internet
    procedure MostraMMC_SnapIn(); //Movido de UnXXe_PF
  end;

var
  UnWin: TUnitWin;

implementation

uses
  UnMyObjects, UnDmkProcFunc;

var
  PNode, CNode: TTreeNode;
  AWindows: PWindows;

{ TUnitWin }

(*
function TUnitWin.EnumChildWindowsProc(Wnd: HWnd; Form: TForm;
  TreeView: TTreeView): Bool;
//function TUnitWin.EnumChildWindowsProc(Wnd: HWnd; Form: TForm; TreeView: TTreeView): Bool; export; {$ifdef Win32} stdcall; {$endif}
var
  Buffer: array[0..99] of Char;
begin
  GetWindowText(Wnd, Buffer, 100);
  //if StrLen(Buffer)  0 then
  if StrPas(Buffer) = '' then Buffer := 'Empty';
  new(AWindows);
  with AWindows^ do
  begin
    WindowHandle := Wnd;
    WindowText   := StrPas(Buffer);
  end;

  //CNode := Form1.TreeView1.Items.AddChildObject(PNode,
  CNode := TreeView.Items.AddChildObject(PNode,
                 AWindows^.WindowText + ':' +
                 IntToHex(AWindows^.WindowHandle, 8), AWindows);
  if GetWindow(Wnd, GW_CHILD) <> 0 then
  begin
    PNode := CNode;
    Enumchildwindows(Wnd, @EnumChildWindowsProc, 0);
  end;
  Result := True;
end;

function TUnitWin.EnumWindowsProc(Wnd: HWnd; Form: TForm;
  TreeView: TTreeView): Bool;
//function TUnitWin.EnumWindowsProc(Wnd: HWnd; Form: TForm; TreeView: TTreeView): Bool; export; {$ifdef Win32} stdcall; {$endif}
var
  Buffer: array[0..99] of Char;
begin
  GetWindowText(Wnd, Buffer, 100);
  //if StrLen(Buffer)  0 then
  if StrPas(Buffer) = '' then Buffer := 'Empty';
  new(AWindows);
  with AWindows^ do
  begin
    WindowHandle := Wnd;
    WindowText   := StrPas(Buffer);
  end;

  PNode := TreeView.Items.AddObject(nil, AWindows^.WindowText + ':' +
    IntToHex(AWindows^.WindowHandle, 8), AWindows);
  EnumChildWindows(Wnd, @EnumChildWindowsProc, 0);
  Result := True;
end;
*)

{
procedure TForm1.Button1Click(Sender: TObject);
begin
  EnumWindows(@EnumWindowsProc, Longint(Self));
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Dispose(AWindows);
end;
}

function TUnitWin.EnumWindowsProc(Wnd: HWND; Param: PMyEnumParam): BOOL; stdcall;
const
  MyMaxName = 64;
  MyMaxText = 64;
var
  ParamChild: TMyEnumParam;
  ClassName: string;
  WindowText: string;
begin
  Result := True;
  SetLength(ClassName, MyMaxName);
  SetLength(ClassName, GetClassName(Wnd, PChar(ClassName), MyMaxName));
  SetLength(WindowText, MyMaxText);
  SetLength(WindowText, SendMessage(Wnd, WM_GETTEXT, MyMaxText, lParam(PChar(WindowText))));
  ParamChild.Nodes   := Param.Nodes;
  ParamChild.Current := Param.Nodes.AddChildObject(Param.Current,
    '[' + ClassName + '] "' + WindowText + '"' + ' Handle: ' + IntToStr(Wnd), Pointer(Wnd));
  EnumChildWindows(Wnd, @TUnitWin.EnumWindowsProc, lParam(@ParamChild));
end;
// Exemplo de uso:
(*
procedure TForm1.Button1Click(Sender: TObject);
var
  Param: TMyEnumParam;
begin
  Param.Nodes := TreeView1.Items;
  Param.Current := TreeView1.TopItem;
  TreeView1.Items.BeginUpdate;
  EnumWindows(@EnumWindowsProc, lParam(@Param));
  TreeView1.Items.EndUpdate;
end;
*)

procedure TUnitWin.MostraMMC_SnapIn();
//procedure TUnXXe_PF.MostraMMC_SnapIn();
const
  Arq = 'C:\Dermatek\NFe\ConsoleNFe.msc';
begin
  if FileExists(Arq) then
      ShellExecute(Application.Handle, PChar('open'), PChar(Arq),   PChar(''), nil, SW_NORMAL)
  else
  begin
    Geral.MB_Info('O arquivo "' + Arq + '" n�o foi localizado!');
     Geral.MB_Info(Geral.ATS(['Clique em iniciar e digite mmc para abrir "Console de gerenciamento Microsoft".',
  'Clique em Arquivo e v� em Adicionar/remover sanp-in, ',
  'selecione certificados e clique em Adicionar, ',
  'escolha conta de computador e Conlcuir e depois OK. ',
  'Expanda Certificados e em seguida Autoridades de Certifica��o Raiz Confi�veis e Certificados. ',
  'Clique com o bot�o direito em Autoridade Certificadora Raiz Brasileira v5 e v� em Propriedades. ',
  'Desmarque Autentica��o de Cliente e marque Autentica��o do Servidor e clique em OK. ',
  'Feche o Chrome e entre no site novamente. ',
  ' ',
  ' ',
  '***  ATEN��O!!! **** ',
  '**** Para n�o desmarcar a "Autentica��o do servidor" sozinho:**** ',
  ' ',
  '>>>>    C:\_Sincro\Instaladores\gpedit.msc  <<<< ',
  ' ',
  'Usando o Editor de Pol�tica de Grupo Local (gpedit.msc), alterar a seguinte propriedade: ',
  ' ',
  'Pol�tica Computador Local > Configura��o do Computador > Modelos Administrativos > Sistema > Gerenciamento de Comunica��o da Internet > Desativar Atualiza��o Autom�tica de Certificados Raiz. ',
  ' ',
  'INGL�S ',
  'Local Computer Policy > Computer Configuration > Administratives Templates > System > Internet Communication Management > Internet Communication Settings > Turn off Automatic Root Certificates Update ',
  ' ',
  'Definir esta regra como "Habilitado". ',
  ' ',
  ' ',
  ' ',
  '>>> s e n�o achar, crie no RegEdit.Exe: ',
  ' ',
  'Turn off Automatic Root Certificates Update ',
  'This policy setting specifies whether to automatically update root certificates using the Windows Update website. ',
  ' ',
  'Typically, a certificate is used when you use a secure website or when you send and receive secure email. ',
  'Anyone can issue certificates, but to have transactions that are as secure as possible, certificates must be issued by a trusted ' +
  'certificate authority (CA). Microsoft has included a list in Windows XP and other products of companies and organizations that it considers trusted authorities. ',
  ' ',
  'If you enable this policy setting, when you are presented with a certificate issued by an untrusted root authority, your computer will not contact the Windows Update website to see if Microsoft has added the CA to its list of trusted authorities. ',
  ' ',
  'If you disable or do not configure this policy setting, your computer will contact the Windows Update website. ',
  ' ',
  'Supported on: At least Windows Server 2003 operating systems with SP1 or Windows XP Professional with SP2 ',
  ' ',
  ' ',
  ' ',
  'Registry Hive	HKEY_LOCAL_MACHINE ',
  'Registry Path ->	Software\Policies\Microsoft\SystemCertificates\AuthRoot ',
  'Value Name	DisableRootAutoUpdate ',
  'Value Type	REG_DWORD ',
  'Enabled Value	1 ',
  'Disabled Value	0 ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  'Em seguida no prompt de comando executei o seguinte comando: ',
  ' ',
  'gpupdate /force ',
  ' ']));
  end;
end;

procedure TUnitWin.OpcoesDaInternet(Form: TForm; E_Message: WideString;
  PerguntaSobreTutorial, PerguntaSobreOpcoesInternet, PerguntaSobreSnapIn:
  Boolean);
var
  Acoes: Integer;
begin
  if E_Message <> EmptyStr then
    Geral.MB_Erro(E_Message);
  //
  Acoes := 0;
  if PerguntaSobreTutorial then
    Acoes := Acoes + 1;
  if PerguntaSobreOpcoesInternet then
    Acoes := Acoes + 2;
  if PerguntaSobreSnapIn then
    Acoes := Acoes + 4;
  //
  Acoes := MyObjects.SelCheckGroup('Ajuda e configura��es NFe',
  'Selecione os itens a serem abertos',
  //Itens: array of String;
  [
  'Tutorial sobre configura��es conhecidas',
  'Op��es da Internet (Windows)',
  'Snap In (Windows)'
  ],
  (*Colunas*)1,  (*Default*) Acoes);
  //
  if dmkPF.IntInConjunto2(1, Acoes) then
  //if Geral.MB_Pergunta('Deseja abrir um tutorial sobre configura��es conhecidas?') = ID_YES then
    DmkWeb.MostraWebBrowser('http://www.dermatek.net.br/index.php?page=wfaqview&id=172', True, False, 0, 0);
  if dmkPF.IntInConjunto2(2, Acoes) then
  begin
    try
      ShellExecute(Form.HANDLE, 'open', 'control.exe',  'inetcpl.cpl', nil, SW_SHOWNORMAL);
    except
      try
        WinExec('control inetcpl.cpl', SW_SHOW);
      except
        ;
      end;
    end;
  end;
  if dmkPF.IntInConjunto2(4, Acoes) then
  begin
    MostraMMC_SnapIn();
  end;
end;

end.
