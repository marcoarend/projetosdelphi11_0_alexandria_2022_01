unit MyVCLSkin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables,
{$IfDef cSkinRank}
  WinSkinData, WinSkinStore,
{$EndIf}
  UnInternalConsts2, DmkDAC_PF,
{$IFDEF DELPHI12_UP}
  System.UITypes,
{$ENDIF}
  UnInternalConsts, Grids, DBGrids, DBCtrls, ImgList, dmkGeral, UnDmkProcFunc
  (*, UnGrl_DmkDB*);

type
  TRangeCheck = (dmkrc0e1, dmkrc0a2);
  THackDBGrid = class(TDBGrid);
  TMyVCLSkin = class(TObject)
  private
    (* N�o Usa
    procedure DrawGrid3States(Grade: TDBGrid; const Rect: TRect;
      FixedCols: Integer; Valor, MinVal, MaxVal: Double);
    *)
    { Private declarations }
  public
    { Public declarations }
    //==========================================================================
    procedure DrawGrid(Grade: TDBGrid; const Rect: TRect; FixedCols, Valor:
              Integer; Lista: Integer = 1);
    procedure DrawGrid4(Grade: TStringGrid; const Rect: TRect; FixedCols, Valor: Integer);
    // Procedurea functions de VCL Skin
    {$IfDef cSkinRank}
    procedure VCLSkinEscolhe(Dialog: TOpenDialog; VCLSkin: TSkinData);
    procedure VCLSkinDefineCores(MyQuery: TmySQLQuery);

    procedure DrawStringGrid(Grade: TStringGrid; const Rect: TRect; FixedCols,
              Valor: Integer; Ativo: Boolean);
    procedure DrawColorGrid(Grade: TDBGrid; const Rect: TRect; FixedCols,
              CorFundo, CorTexto: Integer);
    procedure PosicionaDBLookupComboBoxInDBGrid(Grade: TDBGrid; ComboBox:
              TDBLookupComboBox; const Rect: TRect; Largura: Integer);
    procedure DrawGrid3(Grade: TStringGrid; const Rect: TRect; FixedCols, Valor: Integer);
    procedure DrawGridPercent(Grade: TDBGrid; const Rect: TRect;
              FixedCols: Integer; Valor, MaxVal: Double);
    procedure MostraNomeSkin;

    function VCLSkinDefineUso(VCLSkin: TSkinData; SkinStore: TSkinStore): Boolean;
    function VCLSkinDefineUsoDeMinhasCores(Usar: Boolean; MyQuery: TmySQLQuery): Boolean;
    function VCLSkinVerificaUsoDeAparencias(MyQuery: TmySQLQuery):Boolean;
    function VCLSkinAtiva(Escolhe: Boolean; VCLSkin: TSkinData;
              SkinStore: TSkinStore): Boolean;
    function VCLSkinDefineAtivacao(Ativar: Boolean; VCLSkin:
             TSkinData; SkinStore: TSkinStore): Boolean;
    // Fim procedures e functions de VCL Skin
    {$EndIf}
    procedure DrawCheck2(ACanvas: TCanvas; const ARect: TRect; Checked: Integer;
              RangeCheck: TRangeCheck);
    procedure DrawGridCheck(Grade: TDBGrid; const Rect: TRect;
              FixedCols: Integer; Valor: Integer; RangeCheck: TRangeCheck);
  end;


var
  MeuVCLSkin: TMyVCLSkin;

implementation

uses UnMyObjects, Principal, MyGlyfs, UMySQLDB;

{$IfDef cSkinRank}

procedure TMyVCLSkin.VCLSkinDefineCores(MyQuery: TmySQLQuery);
var
  Aparencia: Integer;
begin
{$IfDef cSkinRank} //Berlin
  if VAR_SD1 <> nil then
  begin
    IC2_AparenciasTituloPainelItem   := clBtnFace;
    IC2_AparenciasTituloPainelTexto  := clWindowText;
    IC2_AparenciasDadosPainel        := clBtnFace;
    IC2_AparenciasDadosTexto         := clWindowText;
    IC2_AparenciasGradeAItem         := clWindow;
    IC2_AparenciasGradeATitulo       := clWindowText;
    IC2_AparenciasGradeATexto        := clWindowText;
    IC2_AparenciasGradeAGrade        := clBtnFace;
    IC2_AparenciasGradeBItem         := clWindow;
    IC2_AparenciasGradeBTitulo       := clWindowText;
    IC2_AparenciasGradeBTexto        := clWindowText;
    IC2_AparenciasGradeBGrade        := clBtnFace;
    IC2_AparenciasEditAItem          := clWindow;
    IC2_AparenciasEditATexto         := clWindowText;
    IC2_AparenciasEditBItem          := clBtnFace;
    IC2_AparenciasEditBTexto         := clWindowText;
    IC2_AparenciasConfirmaPainelItem := clBtnFace;
    IC2_AparenciasControlePainelItem := clBtnFace;
    IC2_AparenciasFormPesquisa       := clBtnFace;
    IC2_AparenciasLabelPesquisa      := clWindowText;
    IC2_AparenciasLabelDigite        := clWindowText;
    IC2_AparenciasEditPesquisaItem   := clWindow;
    IC2_AparenciasEditPesquisaTexto  := clWindowText;
    IC2_AparenciasEditItemIn         := $00FF8000;
    IC2_AparenciasEditItemOut        := clWindow;
    IC2_AparenciasEditTextIn         := $00D5FAFF;
    IC2_AparenciasEditTextOut        := clWindowText;
  end else
{$EndIf} //Berlin
  begin
    IC2_AparenciasTituloPainelItem   := clBtnFace;
    IC2_AparenciasTituloPainelTexto  := clWindowText;
    IC2_AparenciasDadosPainel        := clBtnFace;
    IC2_AparenciasDadosTexto         := clWindowText;
    IC2_AparenciasGradeAItem         := clWindow;
    IC2_AparenciasGradeATitulo       := clWindowText;
    IC2_AparenciasGradeATexto        := clWindowText;
    IC2_AparenciasGradeAGrade        := clBtnFace;
    IC2_AparenciasGradeBItem         := clWindow;
    IC2_AparenciasGradeBTitulo       := clWindowText;
    IC2_AparenciasGradeBTexto        := clWindowText;
    IC2_AparenciasGradeBGrade        := clBtnFace;
    IC2_AparenciasEditAItem          := clWindow;
    IC2_AparenciasEditATexto         := clWindowText;
    IC2_AparenciasEditBItem          := clBtnFace;
    IC2_AparenciasEditBTexto         := clWindowText;
    IC2_AparenciasConfirmaPainelItem := clBtnFace;
    IC2_AparenciasControlePainelItem := clBtnFace;
    IC2_AparenciasFormPesquisa       := clBtnFace;
    IC2_AparenciasLabelPesquisa      := clWindowText;
    IC2_AparenciasLabelDigite        := clWindowText;
    IC2_AparenciasEditPesquisaItem   := clWindow;
    IC2_AparenciasEditPesquisaTexto  := clWindowText;
    IC2_AparenciasEditItemIn         := $00FF8000;
    IC2_AparenciasEditItemOut        := clWindow;
    IC2_AparenciasEditTextIn         := $00D5FAFF;
    IC2_AparenciasEditTextOut        := clWindowText;
  end;
  VAR_USARAPARENCIA := Geral.ReadAppKeyCU('Usar_Aparencias', Application.Title,
    ktBoolean, False);
  if VAR_USARAPARENCIA  then
  begin
    {$IfNDef NAO_USA_SEL_RADIO_GROUP}
    if USQLDB.ObtemVersaoAppDB(MyQuery.DataBase) <> 0 then
    begin
      Aparencia := Geral.IMV(Geral.ReadAppKeyCU('Aparencia',
      Application.Title, ktString, '-1000'));
      if Aparencia = -1000 then
         Geral.WriteAppKeyCU('Aparencia', Application.Title, '0', ktString);
      MyQuery.Close;
      MyQuery.Params[0].AsInteger := Aparencia;
      UnDmkDAC_PF.AbreQueryApenas(MyQuery);
      if MyQuery.RecordCount < 1 then
      begin
        MyQuery.First;
        MyQuery.Last;
      end;
      if MyQuery.RecordCount > 0 then
      begin
        IC2_AparenciasTituloPainelItem   := MyQuery.FieldByName('TituloPainelItem').AsInteger;
        IC2_AparenciasTituloPainelTexto  := MyQuery.FieldByName('TituloPainelTexto').AsInteger;
        IC2_AparenciasTituloPainelItem   := MyQuery.FieldByName('TituloPainelItem').AsInteger;
        IC2_AparenciasTituloPainelTexto  := MyQuery.FieldByName('TituloPainelTexto').AsInteger;
        IC2_AparenciasDadosPainel        := MyQuery.FieldByName('DadosPainel').AsInteger;
        IC2_AparenciasDadosTexto         := MyQuery.FieldByName('DadosTexto').AsInteger;
        IC2_AparenciasGradeAItem         := MyQuery.FieldByName('GradeAItem').AsInteger;
        IC2_AparenciasGradeATitulo       := MyQuery.FieldByName('GradeATitulo').AsInteger;
        IC2_AparenciasGradeATexto        := MyQuery.FieldByName('GradeATexto').AsInteger;
        IC2_AparenciasGradeAGrade        := MyQuery.FieldByName('GradeAGrade').AsInteger;
        IC2_AparenciasGradeBItem         := MyQuery.FieldByName('GradeBItem').AsInteger;
        IC2_AparenciasGradeBTitulo       := MyQuery.FieldByName('GradeBTitulo').AsInteger;
        IC2_AparenciasGradeBTexto        := MyQuery.FieldByName('GradeBTexto').AsInteger;
        IC2_AparenciasGradeBGrade        := MyQuery.FieldByName('GradeBGrade').AsInteger;
        IC2_AparenciasEditAItem          := MyQuery.FieldByName('EditAItem').AsInteger;
        IC2_AparenciasEditATexto         := MyQuery.FieldByName('EditATexto').AsInteger;
        IC2_AparenciasEditBItem          := MyQuery.FieldByName('EditBItem').AsInteger;
        IC2_AparenciasEditBTexto         := MyQuery.FieldByName('EditBTexto').AsInteger;
        IC2_AparenciasConfirmaPainelItem := MyQuery.FieldByName('ConfirmaPainelItem').AsInteger;
        IC2_AparenciasControlePainelItem := MyQuery.FieldByName('ControlePainelItem').AsInteger;
        IC2_AparenciasFormPesquisa       := MyQuery.FieldByName('FormPesquisa').AsInteger;
        IC2_AparenciasLabelPesquisa      := MyQuery.FieldByName('LabelPesquisa').AsInteger;
        IC2_AparenciasLabelDigite        := MyQuery.FieldByName('LabelDigite').AsInteger;
        IC2_AparenciasEditPesquisaItem   := MyQuery.FieldByName('EditPesquisaItem').AsInteger;
        IC2_AparenciasEditPesquisaTexto  := MyQuery.FieldByName('EditPesquisaTexto').AsInteger;
        IC2_AparenciasEditItemIn         := $00D9FFFF;
        IC2_AparenciasEditItemOut        := MyQuery.FieldByName('EditAItem').AsInteger;
        IC2_AparenciasEditTextIn         := $00A00000;
        IC2_AparenciasEditTextOut        := MyQuery.FieldByName('EditATexto').AsInteger;
      end;
    end;
    {$EndIf}
    MyQuery.Close;
  end;
end;

function TMyVCLSkin.VCLSkinDefineUso(VCLSkin: TSkinData;
  SkinStore: TSkinStore): Boolean;
begin
  if Geral.ReadAppKey('VCLSkins', Application.Title, ktBoolean, True,
    HKEY_CURRENT_USER) = True then
  begin
    VCLSkinAtiva(False, VCLSkin, SkinStore);
    VAR_USAVCLSKIN := True;
    Result := True;
  end else begin
    VCLSkin.Active := False;
    VAR_USAVCLSKIN := False;
    Result := False;
  end;
end;

function TMyVCLSkin.VCLSkinAtiva(Escolhe: Boolean; VCLSkin: TSkinData;
SkinStore: TSkinStore): Boolean;
begin
  VAR_CAMINHOTXTBMP := Geral.ReadAppKeyCU('Principal', Application.Title+'\VCLSkins',
  ktString, CO_DIR_RAIZ_DMK + '\Skins\VCLSkin\Office 2007.skn');//CO_DIR_RAIZ_DMK + '\Skins\VCLSkin\'+'mxskin60.skn');
  VCLSkin.LoadFromCollection(SkinStore,1);
  VAR_IDTXTBMP := FmMyGlyfs.DefineIdTxtBmp;
  if not VCLSkin.active then VCLSkin.active:=true;
  if FileExists(VAR_CAMINHOTXTBMP) then
  begin
    VCLSkin.skinfile:= VAR_CAMINHOTXTBMP;
    if not VCLSkin.active then VCLSkin.active:=true;
  end;
  //
  Result := False;
  Geral.WriteAppKey('VCLSkins', Application.Title, True, ktBoolean,
  HKEY_CURRENT_USER);
  //FmPrincipal.sd1.DoSkinChanged;
  MostraNomeSkin;
end;

{$IfDef cSkinRank}
procedure TMyVCLSkin.VCLSkinEscolhe(Dialog: TOpenDialog; VCLSkin: TSkinData);
begin
  Dialog.filter:='Skin files (*.skn)|*.SKN';
  Dialog.initialdir:= CO_DIR_RAIZ_DMK + '\Skins\VCLSkin';
  if Dialog.execute then
  begin
    VCLSkin.skinfile:= Dialog.filename;
    VAR_CAMINHOTXTBMP := Dialog.filename;
    VAR_IDTXTBMP := FmMyGlyfs.DefineIdTxtBmp;
    Geral.WriteAppKeyCU('Principal', Application.Title+'\VCLSkins',
    Dialog.filename, ktString);

    Geral.WriteAppKey('VCLSkins', Application.Title, True, ktBoolean,
    HKEY_CURRENT_USER);
  end;
  if not VCLSkin.Active then VCLSkin.Active := True;
  MostraNomeSkin;
end;

function TMyVCLSkin.VCLSkinDefineAtivacao(Ativar: Boolean; VCLSkin:
TSkinData; SkinStore: TSkinStore): Boolean;
begin
  if Ativar then Result := VCLSkinAtiva(False, VCLSkin, SkinStore)
  else begin
    if VCLSkin.Active then VCLSkin.active:= False;
    Result := True;
    Geral.WriteAppKey('VCLSkins', Application.Title, False, ktBoolean,
    HKEY_CURRENT_USER);
  end;
end;
{$EndIf}

function TMyVCLSkin.VCLSkinVerificaUsoDeAparencias(MyQuery: TmySQLQuery): Boolean;
begin
  if VAR_USARAPARENCIA then
  begin
    Result := True;
    Geral.WriteAppKeyCU('Usar_Aparencias', Application.Title, True, ktBoolean);
    VAR_USARAPARENCIA := True;
    VCLSkinDefineCores(MyQuery);
  end else begin
    Result := False;
    Geral.WriteAppKeyCU('Usar_Aparencias', Application.Title, False, ktBoolean);
    VAR_USARAPARENCIA := False;
  end;
end;

function TMyVCLSkin.VCLSkinDefineUsoDeMinhasCores(Usar: Boolean; MyQuery: TmySQLQuery): Boolean;
begin
  if Usar then
    VAR_USARAPARENCIA := False else
    VAR_USARAPARENCIA := True;
  Result := not Usar;
  VCLSkinVerificaUsoDeAparencias(MyQuery);
end;

procedure TMyVCLSkin.DrawStringGrid(Grade: TStringGrid; const Rect: TRect;
 FixedCols, Valor: Integer; Ativo: Boolean);
begin
  Grade.Color := clBlack;
  with Grade.Canvas do
  begin
    if Ativo then Brush.Color := clYellow else
    begin
      case Valor of
         0: Brush.Color := clWhite;
         1: Brush.Color := clBlue;
         2: Brush.Color := clRed;
         else Brush.Color := clPurple;
      end;
    end;
    FillRect(Rect);
    TextOut(Rect.Left+2,rect.Top+2,'');
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

procedure TMyVCLSkin.DrawColorGrid(Grade: TDBGrid; const Rect: TRect; FixedCols,
CorFundo, CorTexto: Integer);
begin
  with Grade.Canvas do
  begin
    Brush.Color := CorFundo;
    Font.Color  := CorTexto;
    FillRect(Rect);
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

procedure TMyVCLSkin.PosicionaDBLookupComboBoxInDBGrid(Grade: TDBGrid; ComboBox:
  TDBLookupComboBox; const Rect: TRect; Largura: Integer);
(*var
  W1,H1,Glyph: Integer;
  Pos: MyArrayI02;*)
begin
  with Grade.Canvas do
  begin
    //H1 := THackDBGrid(Grade).DefaultRowHeight;
    //Pos :=  M L A G e r a  l.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    //FillRect(Rect);
    ComboBox.Left := Grade.Left + Rect.Left;
    ComboBox.Top  := Grade.Top  + Rect.Top;
  end;
end;

procedure TMyVCLSkin.DrawGrid3(Grade: TStringGrid; const Rect: TRect; FixedCols, Valor: Integer);
var
  W1,H1,Glyph: Integer;
  Pos: MyArrayI02;
  Bmp: TBitmap;
begin
  if Valor < 0 then Valor := 0;
  if Valor > 2 then Valor := 2;
  with Grade.Canvas do
  begin
    W1 := FmMyGlyfs.ListaCheck3.Width;
    H1 := FmMyGlyfs.ListaCheck3.Height;
    Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    Glyph := {(VAR_IDTXTBMP*2)+}Valor;
    Bmp := TBitmap.Create;
    try
      Bmp.Width := W1;
      Bmp.Height := H1;
      FmMyGlyfs.ListaChecks.GetBitmap(Glyph, Bmp);
      Draw(Pos[1], Pos[2], Bmp);
    finally
      Bmp.Free;
    end;
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

procedure TMyVCLSkin.DrawGridPercent(Grade: TDBGrid; const Rect: TRect;
  FixedCols: Integer; Valor, MaxVal: Double);
var
  W1,H1,Glyph: Integer;
  Pos: MyArrayI02;
  Bmp: TBitmap;
begin
  with Grade.Canvas do
  begin
    W1 := FmMyGlyfs.ListaCkPerc01.Width;
    H1 := FmMyGlyfs.ListaCkPerc01.Height;
    Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    if MaxVal = 0 then Glyph := 0 else
    if Valor >= MaxVal then Glyph := FmMyGlyfs.ListaCkPerc01.Count - 1 else
      Glyph := Trunc(Valor/MaxVal*FmMyGlyfs.ListaCkPerc01.Count);
    if Glyph < 0 then Glyph := 0;
    Bmp := TBitmap.Create;
    try
      Bmp.Width := W1;
      Bmp.Height := H1;
      FmMyGlyfs.ListaCkPerc01.GetBitmap(Glyph, Bmp);
      Draw(Pos[1], Pos[2], Bmp);
    finally
      Bmp.Free;
    end;
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

(* N�o usa
procedure TMyVCLSkin.DrawGrid3States(Grade: TDBGrid; const Rect: TRect;
  FixedCols: Integer; Valor, MinVal, MaxVal: Double);
var
  W1,H1,Glyph: Integer;
  Pos: MyArrayI02;
  Bmp: TBitmap;
begin
  with Grade.Canvas do
  begin
    W1 := FmMyGlyfs.ListaCkPerc01.Width;
    H1 := FmMyGlyfs.ListaCkPerc01.Height;
    Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    if MaxVal = 0 then Glyph := 0 else
    if Valor >= MaxVal then Glyph := FmMyGlyfs.ListaCkPerc01.Count - 1 else
      Glyph := Trunc(Valor/MaxVal*FmMyGlyfs.ListaCkPerc01.Count);
    if Glyph < 0 then Glyph := 0;
    Bmp := TBitmap.Create;
    try
      Bmp.Width := W1;
      Bmp.Height := H1;
      FmMyGlyfs.ListaCkPerc01.GetBitmap(Glyph, Bmp);
      Draw(Pos[1], Pos[2], Bmp);
    finally
      Bmp.Free;
    end;
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;
*)

procedure TMyVCLSkin.MostraNomeSkin;
begin
  if VAR_SKINUSANDO <> nil then
     VAR_SKINUSANDO.Text := '  '+ExtractFileName(VAR_CAMINHOTXTBMP);
end;
 {$EndIf}



function Max( Frst,Sec : LongInt ): LongInt;
begin
  if Frst >= Sec then
    Result := Frst
  else
    Result := Sec
end;

procedure TMyVCLSkin.DrawCheck2(ACanvas: TCanvas; const ARect: TRect; Checked: Integer;
RangeCheck: TRangeCheck);
var
  TempRect   : TRect;
  OrigRect   : TRect;
  Dimension  : Integer;
  OldColor   : TColor;
  OldPenWidth: Integer;
  OldPenColor: TColor;
  B          : TRect;
  DrawBitmap : TBitmap;
begin
  // Tirar texto da celula
  ACanvas.FillRect(ARect);
  ACanvas.TextOut(ARect.Left + 2, ARect.Top + 2, '');
  //
  OrigRect := ARect;
  DrawBitmap := TBitmap.Create;
  try
    DrawBitmap.Canvas.Brush.Color := ACanvas.Brush.Color;
    with DrawBitmap, OrigRect do
    begin
      Height := Max(Height, Bottom - Top);
      Width := Max(Width, Right - Left);
      B := Rect(0, 0, Right - Left, Bottom - Top);
    end;
    with DrawBitmap, OrigRect do ACanvas.CopyRect(OrigRect, Canvas, B);
    TempRect := OrigRect;
    TempRect.Top:=TempRect.Top+1;
    TempRect.Bottom:=TempRect.Bottom+1;
    with TempRect do
    begin
      Dimension := 13;//ACanvas.TextHeight('W')-3;
      Top    := ((Bottom+Top)-Dimension) shr 1;
      Bottom := Top+Dimension;
      Left   := ((Left+Right)-Dimension) shr 1;
      Right  := Left+Dimension;
    end;
    with ACanvas do
    begin
      OldColor    := Brush.Color;
      OldPenWidth := Pen.Width;
      OldPenColor := Pen.Color;
      Brush.Color := $00F2F3EF;
      FillRect(TempRect);
    end;
    with ACanvas,TempRect do
    begin
      Pen.Color := $00838481;
      Pen.Width := 1;
      MoveTo(Left , Top-1);
      LineTo(Right-1, Top-1);
      LineTo(Right-1, Bottom-1);
      LineTo(Left , Bottom-1);
      LineTo(Left, Top-1);
    end;
    if Checked > 0 then
    begin
      with ACanvas,TempRect do
      begin
        case RangeCheck of
          dmkrc0e1: Pen.Color := clBlack;
          dmkrc0a2:
            if Checked = 2 then
              Pen.Color := clBlack
            else
              Pen.Color := clSilver;//$00E0E0E0;
        end;
        //
        Pen.Width := 1;

        MoveTo( Left + 3, Top + 4);
        LineTo( Left + 6, Top + 7);

        MoveTo( Left +  3, Top +  5);
        LineTo( Left +  6, Top +  8);

        MoveTo( Left +  3, Top +  6);
        LineTo( Left +  6, Top +  9);

        MoveTo( Left +  6, Top +  5);
        LineTo( Left + 10, Top +  1);

        MoveTo( Left +  6, Top +  6);
        LineTo( Left + 10, Top +  2);

        MoveTo( Left +  6, Top +  7);
        LineTo( Left + 10, Top +  3);

        Pen.Color := clSilver;

        MoveTo( Left +  3, Top +  7);
        LineTo( Left +  6, Top + 10);

        MoveTo( Left +  6, Top +  8);
        LineTo( Left + 10, Top +  4);

      end;
    end;
    ACanvas.Pen.Color  := OldPenColor;
    ACanvas.Pen.Width  := OldPenWidth;
    ACanvas.Brush.Color:= OldColor;
  finally
    DrawBitmap.Free;
  end;
end;

procedure TMyVCLSkin.DrawGridCheck(Grade: TDBGrid; const Rect: TRect;
  FixedCols: Integer; Valor: Integer; RangeCheck: TRangeCheck);
{
var
  W1, H1: Integer;
  Pos: MyArrayI02;
}
begin
  DrawCheck2(Grade.Canvas, Rect, Valor, RangeCheck);
  {
  with Grade.Canvas do
  begin
    W1 := FmMyGlyfs.ListaCkPerc01[1].Bitmap.Width;
    H1 := FmMyGlyfs.ListaCkPerc01[1].Bitmap.Height;
    Pos :=  M L A G e r a l .CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    Draw(Pos[1],Pos[2],FmMyGlyfs.ListaCheck3[Valor].Bitmap);
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
  }
end;

procedure TMyVCLSkin.DrawGrid(Grade: TDBGrid; const Rect: TRect; FixedCols,
  Valor: Integer; Lista: Integer = 1);
var
  W1,H1,Glyph, Val: Integer;
  Pos: MyArrayI02;
  Bmp: TBitmap;
  ImageList: TImageList;
begin
  case Lista of
    2: ImageList := FmMyGlyfs.ListaChecks2;
  //3: ???
    4: ImageList := FmMyGlyfs.ListaChecks4;
    5: ImageList := FmMyGlyfs.ListaChecks5;
    else ImageList := FmMyGlyfs.ListaChecks;
  end;
  if Lista > 2 then
    Val := Valor
  else
  begin
    if Valor = 0 then
      Val := 0
    else
      Val := 1;
  end;
  with Grade.Canvas do
  begin
    W1 := ImageList.Width;
    H1 := ImageList.Height;
    Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    Glyph := (VAR_IDTXTBMP*2)+Val;
    Bmp := TBitmap.Create;
    try
      Bmp.Width := W1;
      Bmp.Height := H1;
      ImageList.GetBitmap(Glyph, Bmp);
      Draw(Pos[1], Pos[2], Bmp);
    finally
      Bmp.Free
    end;
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

procedure TMyVCLSkin.DrawGrid4(Grade: TStringGrid; const Rect: TRect; FixedCols, Valor: Integer);
var
  W1,H1,Glyph: Integer;
  Pos: MyArrayI02;
  Bmp: TBitmap;
begin
  if Valor < 0 then Valor := 0;
  if Valor > 2 then Valor := 2;
  with Grade.Canvas do
  begin
    W1 := FmMyGlyfs.ListaCheck3.Width;
    H1 := FmMyGlyfs.ListaCheck3.Height;
    Pos := dmkPF.CoordenadasIniciais(Rect.Left,Rect.Top,Rect.Right,Rect.Bottom,W1,H1);
    FillRect(Rect);
    //TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    Glyph := {(VAR_IDTXTBMP*2)+}Valor;
    Bmp := TBitmap.Create;
    try
      Bmp.Width := W1;
      Bmp.Height := H1;
      FmMyGlyfs.ListaCheck3.GetBitmap(Glyph, Bmp);
      Draw(Pos[1], Pos[2], Bmp);
    finally
      Bmp.Free;
    end;
  end;
  THackDBGrid(Grade).FixedCols := FixedCols;
end;

end.

