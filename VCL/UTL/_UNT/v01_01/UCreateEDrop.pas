unit UCreateEDrop;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables, dmkGeral;

type
  TNomeTabRecriaEDrop = ((*ntrtt_ConsAccCli,*) ntrtt_DistrCliAcc, ntrtt_PrintGrid);
  TAcaoCreateEDrop = (acDrop, acCreate, acFind);
  TUCreateEDrop = class(TObject)

  private
    { Private declarations }
    //procedure Cria_ntrtt_ConsAccCli(Qry: TmySQLQuery; Repeticoes: Integer);
    procedure Cria_ntrtt_DistrCliAcc(Qry: TmySQLQuery; Repeticoes: Integer);
    procedure Cria_ntrtt_PrintGrid(Qry: TmySQLQuery; Repeticoes: Integer);


  public
    { Public declarations }
    function RecriaDropedTab(Tabela: TNomeTabRecriaEDrop; Qry: TmySQLQuery;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
  end;

var
  UCriaEDrop: TUCreateEDrop;

implementation

uses UnMyObjects, Module;

{
procedure TUCreateEDrop.Cria_ntrtt_ConsAccCli(Qry: TmySQLQuery;
  Repeticoes: Integer);
begin
  Qry.SQL.Add('  Linha              int(11)      NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  Account            varchar(30)  NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  Segmento           varchar(5)   NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  NovoAcc            varchar(30)  NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  Cli_Codigo         int(11)      NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  NomeCli            varchar(80)  NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  Cidade             varchar(50)  NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  UF                 char(2)      NOT NULL DEFAULT "?",');
  //
  Qry.SQL.Add('  Ativo              tinyint(1)   NOT NULL DEFAULT "?"  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

procedure TUCreateEDrop.Cria_ntrtt_DistrCliAcc(Qry: TmySQLQuery;
  Repeticoes: Integer);
var
  I, J: Integer;
  L: String;
begin
  Qry.SQL.Add('  Linha              int(11)      NOT NULL DEFAULT 1,');
  for J := 65 to 67 do
  begin
    L := Char(J);
    Qry.SQL.Add('  ' + L + '_Codigo int(11)      NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  ' + L + '_Grupo  int(11)      NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  ' + L + '_CodCli int(11)      NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  ' + L + '_GU_Tip int(11)      NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  ' + L + '_FcXXX  int(11)      NOT NULL DEFAULT 0,');
    Qry.SQL.Add('  ' + L + '_NO_UNI varchar(100)                   ,');
    for I := 1 to Repeticoes do
      Qry.SQL.Add('  ' + L + '_Col' + Geral.FFN(I, 3) + ' int(11) NOT NULL DEFAULT 0,');
  end;
  //
  Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 1  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateEDrop.Cria_ntrtt_PrintGrid(Qry: TmySQLQuery; Repeticoes: Integer);
var
  I: Integer;
begin
  for I := 0 to Repeticoes -1 do
    Qry.SQL.Add('  Col' + FormatFloat('000', I) + ' varchar(255),');
  Qry.SQL.Add('  Ativo        tinyint(1) NOT NULL DEFAULT 1');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TUCreateEDrop.RecriaDropedTab(Tabela: TNomeTabRecriaEDrop; Qry:
  TmySQLQuery; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      //ntrtt_ConsAccCli:    Nome := Lowercase('_ConsAccCli_');
      ntrtt_DistrCliAcc:   Nome := Lowercase('_DistrCliAcc_');
      ntrtt_PrintGrid:     Nome := Lowercase('_PrintGrid_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria sem nome definido! (RecriaDropedTab)');
    Result := '';
    Exit;
  end;
  if Nome[1] <> '_' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria inv�lido (deve come�ar com "_"! (RecriaDropedTab)');
    Result := '';
    Exit;
  end;
  if Nome[Length(Nome)] <> '_' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria inv�lido (deve terminar com "_"! (RecriaDropedTab)');
    Result := '';
    Exit;
  end;

  // UniqueTableName
  begin
{ TODO : Criar Usuario pela WUsers }
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end;// else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    //ntrtt_ConsAccCli:   Cria_ntrtt_ConsAccCli(Qry, Repeticoes);
    ntrtt_DistrCliAcc:  Cria_ntrtt_DistrCliAcc(Qry, Repeticoes);
    ntrtt_PrintGrid:    Cria_ntrtt_PrintGrid(Qry, Repeticoes);
      //
      // ...
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o! (RecriaDropedTab)');
  end;
  Result := TabNo;
end;

end.

