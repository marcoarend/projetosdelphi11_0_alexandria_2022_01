unit ZCF2;

interface

uses
  SysUtils, Controls; // Controls -> Cursor

//const

var
  ZZGeral : Boolean;
  SomaTudo, SomaMaqu, SomaOper, SomaRece, SomaTint : Double;
  CalcPPCC : Boolean;
  CalcJuros : Double;
  MachineOperaTE, ChemicalsOperaTE : Integer;
  MudouJuros : Boolean;
  ZZAtBDadosShow : Boolean;
  ZZSitAtualiza : String; //Op��es e Principal
  // verifica se o usuario est� inserindo uma linha no meio da receita
  ZZNovaCorItem, ZZCorItemRecCount : Integer;
  ZZNovaCor2Item, ZZCor2ItemRecCount : Integer;
  ZZNovaTintaIts, ZZTintaItsRecCount : Integer;
  ZZNovaLinhaTEIts, ZZLinhaTEItsRecCount : Integer;
  ZZTintasItsJaEmEdicao : Boolean;
  ZZItsTinOpMudaTin : Boolean;
  ZZAtualiza : Boolean;
  ZZVerifMarca, ZZVerifMarca1 : Boolean;
  ZZAtzTaxas : Boolean;
  ZZFmPQVisible, ZZFmMPEVisible, ZZFmFunciVisible : Boolean;
  ZZFmMaquinasVisible, ZZFmCaldeirasVisible : Boolean;
  ZZAbrindoTabelas : Boolean;
  ZZSenha : String;
  ZZIndiceSenha : Integer;
  ZZAlteraMPE : Boolean;
  ZZSetorPMaq : Integer;
  ZZFreshTabs : Boolean;
  ZZGridRecSetFocus : Boolean;
  ZZReordemRec, ZZReordemTin , ZZReordemCor1, ZZReordemCor2 : Byte;
  ZZTerminate, ZZTabelasAbertas : Boolean;
  ZZTipoPesoOSAberta : Byte;
  ZZ_FSENHA : Byte; // 1=
  ZZ_FABERTURA : Byte;

implementation
end.
