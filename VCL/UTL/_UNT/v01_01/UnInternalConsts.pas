unit UnInternalConsts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mySQLDbTables,
  {$IfNDef VER310} (*dbtables,*)
    {$IfDef cAdvToolx} //Berlin
    AdvToolBar,
    {$EndIf} //Berlin
  {$EndIf} //Berlin
  StdCtrls, comctrls, Menus, extctrls, Variants,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection, ZConnection;

{

  Coment�rios:

  ////////////////////////////////////////////////////////////////////////////////
  Funcoes MySQL
  http://dev.mysql.com/doc/refman/5.5/en/func-op-summary-ref.html
////////////////////////////////////////////////////////////////////////////////

  Migracao XE2 para XE3+
  http://docwiki.embarcadero.com/RADStudio/XE6/en/Migrating_Delphi_Code_to_Mobile_from_Desktop
////////////////////////////////////////////////////////////////////////////////

  Inverter cores
procedure TMenuForm.InvertColors1Click(Sender: TObject);
var
  i : Integer;
begin
  i := Integer(DemoShape.Brush.Color) xor $FFFFFF;
  DemoShape.Brush.Color := TColor(i);
end;
////////////////////////////////////////////////////////////////////////////////

Registrar DLL no Windows
Registrar uma DLL no Windows
Registrar de DLL no Windows
regsvr32 msxml5.dll /s
regsvr32 capicom.dll
////////////////////////////////////////////////////////////////////////////////

-------------------------------------------------------------------------------

  Cliente1 := Produtos;
  Cliente2 := Servicos;
  Fornece1 := MP;
  Fornece2 := Insumos;
  Fornece3 := Transporte;


--------------------------
  ID Panels: VAR_PANEL   |
  ---------------------- |
  001 - Carteiras        |
  002 - Contas           |
  003 - Emissoes         |
  004 - Cad. Carteiras   |
  005 - Cad. Contas      |
  006 - Cad. Emissoes    |
  007 - Cad. G�neros     |
  008 - Cad. Grupos      |
  009 - Cad. Contatos    |
  010 - Cad. Empresas    |
  011 - Cad. Produtos    |
  012 - Ent. Produtos    |
  013 - Cad. Cxa Frente  |
  014 - Cad. Bombas      |
-------------------------|

VAR_FATID := -999999999;  Saldo inicial da Carteira X Conta (do plano de contas)
VAR_FATID := -01;  Transfer�ncias entre contas do plano de contas
VAR_FATID := 001;  \
VAR_FATID := 002;    \
VAR_FATID := 003;      \
VAR_FATID := 004;        \
VAR_FATID := 005;         |
VAR_FATID := 006;         |
VAR_FATID := 007;         |
VAR_FATID := 008;         |
VAR_FATID := 009;         |
VAR_FATID := 010;         |
VAR_FATID := 011;         |
VAR_FATID := 012;         |
VAR_FATID := 013;         |
VAR_FATID := 015;         |
VAR_FATID := 017;         |
VAR_FATID := 018;         \
VAR_FATID := 019;          | USAR PARA NFe! de 001 a 299
VAR_FATID := 020;         /
VAR_FATID := 021;         |
VAR_FATID := 022;         |
VAR_FATID := 023;         |
VAR_FATID := 024;         |
VAR_FATID := 030;         |
VAR_FATID := 031;         |
VAR_FATID := 032;         |
VAR_FATID := 033;         |
VAR_FATID := 034;         |
VAR_FATID := 035;         |
VAR_FATID := 081;         |
VAR_FATID := 101;        /
VAR_FATID := 102;      /
VAR_FATID := 103;    /
VAR_FATID := 105;  /
//
VAR_FATID := 201;  NFSe -> Faturamento
//
VAR_FATID := 300;
VAR_FATID := 310;
VAR_FATID := 311;
VAR_FATID := 312;
VAR_FATID := 400;  IRent -> Pagto aluguel
VAR_FATID := 500;  LeSew/GigaStore -> Pagto compras
VAR_FATID := 510;  LeSew/GGigaStore -> Pagto Vendas
VAR_FATID := 600;  Syndic/Sinker -> Arrecada��es
VAR_FATID := 601;  Syndic/Sinker -> Consumos
VAR_FATID := 610;  Syndic/Sinker -> Acertos de d�bitos (Reparcelamento)
VAR_FATID := 700;  LeSew -> Transfer�ncias entre contas
VAR_FATID := 701;  LeSew -> Despesas professor em viagem (ciclo)
VAR_FATID := 702;  LeSew -> Despesas de rateio (ciclo)
VAR_FATID := 703;  LeSew -> Despesas promotor (ciclo)
VAR_FATID := 704;  LeSew -> Despesas promotor (extras)
VAR_FATID := 705;  LeSew -> Pagamento comiss�es professor (extras)
VAR_FATID := 706;  LeSew -> Pagamento comiss�es professor (ciclo - autom�tico)
VAR_FATID := 711;  LeSew -> Rateio de creditos p/ contas (plano) c/ saldo controlado
VAR_FATID := 712;  LeSew -> Reten��o das comiss�es de professores
VAR_FATID := 721;  LeSew -> Venda de mercadorias pelo professor em viagem (ciclo de curso)
VAR_FATID := 731;  LeSew -> Empr�stimos a professores (Cr�dito e d�bito)
VAR_FATID := 750;  LeSew -> transferencias entre carteiras de acerto entre professor e promotor
VAR_FATID := 751;  LeSew -> transferencias entre carteiras de pagamento da comiss�o de alunos do professor
VAR_FATID := 752;  LeSew -> transferencias entre carteiras de devolu��o da comiss�o de alunos do professor
VAR_FATID := 801;  LeSew -> Venda de mercadorias para pontos de venda
VAR_FATID := 810;  LeSew -> Venda de lingerie
VAR_FATID := 851;  LeSew -> Comiss�o de venda de mercadorias nos pontos de venda
VAR_FATID := 901;  SafeCar -> Servicos
VAR_FATID := 902;  SafeCar -> Pecas
VAR_FATID := 903;  SafeCar -> Franquia
VAR_FATID := 1001;  BlueDerm -> Compra de PQ
VAR_FATID := 1002;  BlueDerm -> Transporte de PQ
VAR_FATID := 1003;  BlueDerm -> Compra de MP
VAR_FATID := 1004;  BlueDerm -> Transporte de MP
VAR_FATID := 1005;  BlueDerm -> Compra de PQ PS
VAR_FATID := 1006;  BlueDerm -> Transporte de PQ PS
VAR_FATID := 1013;  BlueDerm -> Venda de couro
VAR_FATID := 1014;  BlueDerm -> Transporte de venda de couro
VAR_FATID := 2001;  Academy ->

---------------------------------------
  ID Fatura: VAR_FATID [ -99 + X ]    |
  G�neros (Contas) de -100 em diante  |
  ----------------------------------- |
  -001 (-100) = PQ->Fornecedor        |
  -002 (-101) = PQ->Transportador  C  |
  -003 (-102) = MP->Fornecedor        |
  -004 (-103) = MP->Transportador  C  |
  -005 (-104) = PQ->Cliente           |
  -006 (-105) = PQ->Transportador V   |
  -007 (-106) = MP->Fornecedor Devol  |
  -008 (-107) = MP->Transportador  D  |
  -009 (-108) = PQ->Cliente Recompr   |
  -010 (-109) = PQ->Transportador R   |
  -011 (-110) = MP->MObra Servi�o     |
  -012 (-111) = MP->MObra Frete       |
  -013 (-112) = MP->Venda             |
  -014 (-113) = MP->Frete venda       |
  -015 (-114) = MP->Servi�o           |
  -016 (-115) = MP->Frete Servi�o     |
======================================
  -007 (-106) = Emporium->Recomp.Fat  |
  -008 (-107) = Emporium->Recomp.Frete|
  -009 (-108) = Emporium->Devol Fat   |
  -010 (-109) = Emporium->Devol Frete |
  -011 (-110) = Emporium->Mercadoria  |
  -012 (-111) = Emporium->Transportad.|
  -013 (-112) = Emporium->Venda       |
  -014 (-113) = Teach->Matr�cula      |
  -015 (-114) = Store001->ProdutoE    |
  -016 (-115) = Store001->FreteE      |
  -017 (-116) = Store001->ProdutoS    |
  -018 (-117) = Store001->FreteS      |
  -017 (-116) = Recovery->ProdutoM    |
  -018 (-117) = Recovery->FreteM      |
  -019 (-118) = Recovery->ProdutoE    |
  -020 (-119) = Recovery->FreteE      |
  -021 (-120) = Recovery->ProdutoS    |
  -022 (-121) = Recovery->FreteS      |
  -023 (-122) = Recovery->ColetorQ    |
  -024 (-123) = Recovery->MatrizQ     |
  -025 (-124) = Seda->Venda           |
  -026 (-125) = Seda->                |
  -027 (-126) = Seda->                |
  -028 (-127) = Seda->                |
  -029 (-128) = Seda->                |
  -030 (-129) = Seven-> Compra Fornc. |
  -031 (-130) = Seven-> Compra Transp.|
  -032 (-131) = Seven-> Filmes Compra |
  -033 (-132) = Seven-> Filmes Transp.|
  -034 (-133) = Seven-> Filmes Venda  |
  -035 (-134) = Seven-> Venda(Loca��o)|
  -03  (-13 ) = Seven->               |
  -03  (-13 ) = Seven->               |
  -03  (-13 ) = Seven->               |
  -03  (-13 ) = Seven->               |
  -040 (-139) = MStor->               |
  -041 (-140) = MStor->               |
  -042 (-141) = MStor->               |
  -043 (-142) = MStor-> Compra        |
  -044 (-143) = MStor-> Venda         |
  -045 (-144) = MStor-> Retorno Normal|
  -046 (-145) = MStor-> Devolu. Normal|
  -047 (-146) = MStor-> Retorno Garant|
  -048 (-147) = MStor-> Devolu. Garant|
  -049 (-148) = MStor-> Consig. Fornec|
  -050 (-149) = MStor-> Consig. Client|
  -051 (-150) = MStor-> Empres. Fornec|
  -052 (-151) = MStor-> Empres. Client|
  -053 (-152) = MStor->               |
  -054 (-153) = MStor->               |
  -055 (-154) = MStor->               |
  -056 (-155) = MStor->               |
  -057 (-156) = MStor->               |
  -058 (-145) = MStor->               |
  -059 (-158) = MStor->               |
  -060 (-159) = MStor->               |
  -061 (-150) = MStor->               |
  -062 (-151) = MStor->               |
  -063 (-152) = MStor-> Compra        |Frete
  -064 (-153) = MStor-> Venda         |Frete
  -065 (-154) = MStor-> Retorno Normal|Frete
  -066 (-155) = MStor-> Devolu. Normal|Frete
  -067 (-156) = MStor-> Retorno Garant|Frete
  -068 (-157) = MStor-> Devolu. Garant|Frete
  -069 (-158) = MStor-> Consig. Fornec|Frete
  -070 (-159) = MStor-> Consig. Client|Frete
  -071 (-160) = MStor-> Empres. Fornec|Frete
  -072 (-161) = MStor-> Empres. Client|Frete
  -073 (-162) = MStor->               |
  -074 (-163) = MStor->               |
  -075 (-164) = MStor->               |
  -075 (-165) = MStor->               |
  -077 (-166) = MStor->               |
  -078 (-167) = MStor->               |
  -079 (-168) = MStor->               |
  -080 (-169) = MStor->               |
  -081 (-170) = Servicos->Venda       |
  -082 (-171) = Servicos->            |
  -083 (-172) = Servicos->            |
  -084 (-173) = Servicos->            |
  -085 (-174) = Servicos->            |
  -086 (-175) = Servicos->            |
  -087 (-176) = Servicos->            |
  -088 (-177) = Servicos->            |
  -089 (-178) = Servicos->            |
-------------------------------------------------------------------------------
USO DE TAGS                                                                   |
                                                                              |
Tag = 901 -> Sistema m�trico (convers�o de m� > ft� > m�)                     |
Tag = 902 -> claculo kg unit * Vol = kg total; kg total * preco = Custo)      |
Tag = 903 -> Configura��o de impress�o pr�pria (Notas Fiscais)                |
Tag = 904 ->                                                                  |
------------------------------------------------------------------------------|

-------------------------------------------------------------------------------
  Sit                                                                         |
                                                                              |
       -2 -> Usado para editar em LocLancto                                   |
       -1 -> Usado para indicar que o registro j� est� selecionado em Import  |
        0 -> Emiss�o n�o baixada                                              |
        1 -> Emiss�o paga parcial                                             |
        2 -> Emiss�o quitada (Banco)                                          |
        3 -> Caixa ou C/C banco
                                                                              |
------------------------------------------------------------------------------|

C�DOGOS ASCII
Exemplo: ('O' ou 'o') + Alt:
  if ((key=79) or (key=111))and (Shift=[ssAlt]) then //
  begin
    OKClick();
  end;

 001         064 - @        114 - r       164 - �       214 - �
 002         065 - A 1      115 - s       165 - �       215 - �
 003         066 - B 2      116 - t       166 - �       216 - �
 004         067 - C 3      117 - u       167 - �       217 - +
 005         068 - D 4      118 - v       168 - �       218 - +
 006         069 - E 5      119 - w       169 - _       219 - �
 007         070 - F 6      120 - x       170 - �       220 - _
 008         071 - G 7      121 - y       171 - �       221 - �
 009         072 - H 8      122 - z       172 - �       222 - �
 010         073 - I 9      123 - { {     173 - �       223 - �
 011         074 - J 0      124 - |       174 - �       224 - �
 012         075 - K 1      125 - } {     175 - �       225-  �
 013         076 - L 2      126 - ~       176 - �       226 - �
 014         077 - M 3      127 - �       177 - �       227 - �
 015         078 - N 4      128 - �       178 - +       228 - �
 016         079 - O 5      129 - �       179 - +       229 - �
 030 -       080 - P 6      130 - �       180 - �       230 - �
 031 -       081 - Q 7      131 - �       181 - �       231 - �
 032 -       082 - R 8      132 - �       182 - �       232 - �
 033 - !     083 - S 9      133 - �       183 - �       233 - �
 034 -       084 - T 0      134 - �       184 - �       234 - �
 035 -       085 - U 1      135 - �       185 - �       235 - �
 036 - $     086 - V 2      136 - �       186 - �       236 - �
 037 - %     087 - W 3      137 - �       187 - +       237 - �
 038 - &     088 - X 4      138 - �       188 - +       238 - �
 039 - '     089 - Y 5      139 - �       189 - �       239 - �
 040 - (     090 - Z 6      140 - �       190 - �       240 - �
 041 - )     091 - [        141 - �       191 - +       241 - �
 042 - *     092 - \        142 - �       192 - +       242 - =
 043 - +     093 - ]        143 - �       193 - -       243 - �                          
 044 - ,     094 - ^        144 - �       194 - -       244 - �
 045 - -     095 - _        145 - �       195 - +       245 - �
 046 - .     096 - `        146 - �       196 - -       246 - �
 047 - /     097 - a        147 - �       197 - +       247 - �
 048 - 0     098 - b        148 - �       198 - �       248 - �
 049 - 1     099 - c        149 - �       199 - �       249 - �
 050 - 2     100 - d        150 - �       200 - +       250 - �
 051 - 3     101 - e        151 - �       201 - +       251 - �
 052 - 4     102 - f        152 - �       202 - +       252 - �
 053 - 5     103 - g        153 - �       203 - -       253 - �
 054 - 6     104 - h        154 - �       204 - -       254 - �
 055 - 7     105 - i        155 - �       205 - �       255 - �
 056 - 8     106 - j        156 - �       206 - -       255 - �
 057 - 9     107 - k        157 - �       207 - +
 058 - :     108 - l        158 - �       208 - �
 059 - ;     109 - m        159 - �       209 - �
 060 - <     110 - n        160 - �       210 - �
 061 - =     111 - o        161 - �       211 - �
 062 - >     112 - p        162 - �       212 - �
 063 - ?     113 - q        163 - �       213 - i
 064 - @
 065 - A
 066 - B
 067 - C
 068 - D
 069 - E
 070 - F
 071 - G
 072 - H                                   
 073 - I
 074 - J
 075 - K
 076 - L                                  
 077 - M                                  
 078 - N                                  
 079 - O                                  
 080 - P                                  
 081 - Q                                  
 082 - R
 083 - S                                  
 084 - T                                  
 085 - U
 086 - V                                  
 087 - W                                  
 088 - X                                  
 089 - Y
 090 - Z                                  
 091 - [
 092 - \                                  
 093 - ]                                  
 094 - ^                                  
 095 - _
 096 - `
 097 - a
 098 - b
 099 - c
 100 - d
 101 - e
 102 - f
 103 - g
 104 - h
 105 - i
 106 - j
 107 - k
 108 - l
 109 - m
 110 - n
 111 - o
 112 - p
 113 - q
 114 - r
 115 - s
 116 - t
 117 - u
 118 - v
 119 - w
 120 - x
 121 - y
 122 - z
 123 - { {
 124 - |
 125 - } {
 126 - ~
 127 - �
 128 - �
 129 - �
 130 - �
 131 - �
 132 - �
 133 - �
 134 - �
 135 - �
 136 - �
 137 - �
 138 - �
 139 - �
 140 - �
 141 - �
 142 - �
 143 - �
 144 - �
 145 - �
 146 - �
 147 - �
 148 - �
 149 - �
 150 - �
 151 - �
 152 - �
 153 - �
 154 - �
 155 - �
 156 - �
 157 - P
 158 - �
 159 - �
 160 - �
 161 - �
 162 - �
 163 - �
 164 - �
 165 - �
 166 - �
 167 - �
 168 - �
 169 - _
 170 - �
 171 - �
 172 - �
 173 - �
 174 - �
 175 - �
 176 - _
 177 - _
 178 - _
 179 - �
 189 - �
 190 - �
 191 - �
 192 - +
 193 - +
 194 - �
 195 - �
 196 - -
 197 - +
 198 - �
 199 - �
 200 - +
 201 - +
 202 - -
 203 - -
 204 - �
 205 - -
 206 - +
 207 - -
 208 - -
 209 - -
 210 - -
 211 - +
 212 - +
 213 - +
 214 - +
 215 - +
 216 - +
 217 - +
 218 - +
 219 - _
 220 - _
 221 - �
 222 - _
 223 - _
 224 - _
 225-  �
 226 - _
 227 - �
 228 - _
 229 - _
 230 - �
 231 - _
 232 - _
 233 - _
 234 - _
 235 - _
 236 - _
 237 - _
 238 - _
 239 - _
 240 - _
 241 - �
 242 - _
 243 - _
 244 - _
 245 - _
 246 - �
 247 - _
 248 - �
 249 - �
 250 - �
 251 - _
 252 - �
 253 - �
 254 - _
 255 - _

 }


type             // < 0    <=0      Tudo     <>0       >=0      >0
  TKindDepto = (kdNenhum, kdUH, kdObra, kdOS1, kdDpto);
  TGoToSignal = (gotoNeg, gotoNiZ, gotoAll, gotoNiP, gotoPiZ, gotoPos);
  TTypeLog = (ttlUnknow, ttlNone, ttlFiliLog, ttlCliIntLog, ttlCliIntUni);
  TSourceCalcCusto = (sccPreco, sccQuant, sccDesco, sccAcres, sccTotal, // Servi�os
                      sccPer, sccVal);                                  // Comissoes

const

  CO_COPIA = '[C�pia]';
  CO_FIVE_ASKS = '?????';
  CO_DERMATEK_SISTEMA = 'C:\Dermatek\Sistema\';
  clLaranja = $0023AAEF; // Laranja
  sTabLctErr = '!ERRO!';

  //
  CO_APP_TITLE_003 = 'CRED' + 'ITOR';
  CO_FATNUM_POR_COD_FAT = 0;
  CO_FATNUM_POR_COD_NNF = 1;
  CO_FATNUM_POR_COD_ORI = 2;
  //
  //
  //  Movido de UnDiario_Tabs
  CO_DIARIO_ASSUNTO_FLD_NEED_ClienteInterno  = 1;
  CO_DIARIO_ASSUNTO_FLD_NEED_Entidade        = 2;
  CO_DIARIO_ASSUNTO_FLD_NEED_Departamento    = 4;
  CO_DIARIO_ASSUNTO_FLD_NEED_Terceiro01      = 8;
  CO_DIARIO_ASSUNTO_FLD_NEED_Interlocutor    = 16;
  //  FIM Movido de UnDiario_Tabs


  CO_BUG_STATUS_0000_ABERTURA = 0000; //|"Abertura de OS"');
  CO_BUG_STATUS_0100_PRE_ATEN = 0100; //|"Pr�-atendimento"');
  CO_BUG_STATUS_0200_VISTORIA = 0200; //|"Vistoria"');
  CO_BUG_STATUS_0300_EM_ORCAM = 0300; //|"Em or�amento"');
  CO_BUG_STATUS_0400_ORCA_PAS = 0400; //|"Or�amento passado"');
  CO_BUG_STATUS_0449_ORCA_REP = 0449; //|"Or�amento reprovado"');
  CO_BUG_STATUS_0450_ORCA_OK_ = 0450; //|"Or�amento aprovado"');
  CO_BUG_STATUS_0500_AGEN_EXE = 0500; //|"Agendado a execu��o"');
  CO_BUG_STATUS_0550_AGEN_OK_ = 0550; //|"Confirmado Agendamento"');
  CO_BUG_STATUS_0600_EM_EXECU = 0600; //|"Em execu��o "');
  CO_BUG_STATUS_0700_EXE_FINA = 0700; //|"Execu��o finalizada"');
  CO_BUG_STATUS_0750_CEX_ENVI = 0750; //|"Comprovante Enviado"');
  CO_BUG_STATUS_0800_FATURADO = 0800; //|"Faturado"');
  CO_BUG_STATUS_1000_POS_VEND = 1000; //|"P�s venda"');'
  CO_BUG_STATUS_2000_O_S_BAIX = 2000; //|"OS baixada"');
  CO_BUG_STATUS_2500_O_S_NREA = 2500; //|"OS n�o realizada"');
  CO_BUG_STATUS_3000_O_S_CANC = 3000; //|"OS cancelada"');


  //////////////////////////////////////////////////////////////////////////////
  //                                                                          //
  //  N�O ESQUECER DE COLOCAR NO:                                             //
  // function TDmNFe_0000.NomeFatID_NFe(FatID: Integer): String;              //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////
  (*PQx*) VAR_FATID__006 = -006; // Balanco de PQx (diferenca do lancamento)
  (*PQx*) VAR_FATID__005 = -005; // Retorno de PQ sem entrada e/ou baixa no estoque para retorno (Retorno antecipado e incorreto)!)
  (*PQx*) VAR_FATID__004 = -004; // Baixa de Baixa de PQ (de retorno sem emissao de NFe)
  (*PQx*) VAR_FATID__003 = -003; // Baixa de entrada de PQ (de retorno sem emissao de NFe)
  (*Nfe*) VAR_FATID__002 = -002; // Baixa retroativa for�ada
  (*Nfe*) VAR_FATID__001 = -001; // Balanco de PQx (diferenca do lancamento)
  (*Nfe*) VAR_FATID_0000 = 0000; // Balanco de PQx (lancamento)
  // Mudado 2020-10-24
  //(*Nfe*) VAR_FATID_0001 = 0001; // Faturanento de pedido
  //(*Nfe*) VAR_FATID_0002 = 0002; // Faturamento sem pedido (balc�o tamb�m?)
  (*Nfe*) VAR_FATID_0001 = 0001; // Faturanento NF-e
  (*Nfe*) VAR_FATID_0002 = 0002; // Faturamento NFC-e
  // fim 2020-10-24
  (*Nfe*) VAR_FATID_0003 = 0003; // Sa�da condicional
  (*Nfe*) VAR_FATID_0004 = 0004; // Retorno Condicional
  (*Nfe*) VAR_FATID_0005 = 0005; // Entrada por compra
  (*Nfe*) VAR_FATID_0006 = 0006; // Frete de compra >> 2021-04-29
  (*PQx*) VAR_FATID_0010 = 0010; // Entrada de Uso e Consumo por NF (compra ou remessa de industrializacao)
  // ver!!!(*NfCe*) VAR_FATID_0011 = 0011; // NFCe ??? 2020-10-24 mas usa FatID=1 >> Pedido!!!
  (*Nfe*) VAR_FATID_0013 = 0013; // MPinIts (NFe ) no BlueDerm
  (*PQx*) VAR_FATID_0020 = 0020; // Entrada de Uso e Consumo por ajuste no balan�o; // 2023-10-09
  (*PQx*) VAR_FATID_0030 = 0030; // Entrada por dilu��o / mistura
  (*Nfe*) VAR_FATID_0050 = 0050; // Importa��o de NFe de emiss�o pr�pria
  (*Nfe*) VAR_FATID_0051 = 0051; // Entrada de Uso e consumo por NFe
  //(*Nfe*) VAR_FATID_0052 = 0052; // Entrada de Uso e consumo por SPED EFD
  (*Nfe*) VAR_FATID_0053 = 0053; // Registro de Chave NFe por Consulta de Distribuicao de DFe de interesse
  //(*Nfe*) VAR_FATID_0054 = 0054; // Registro de Chave NFe por Consulta de NFes Destinadas (NfeConsultaDest)
  (*SMI*) VAR_FATID_0061 = 0061; // Transferencia entre centros de estoque
  (*Tel*) VAR_FATID_0070 = 0070; // Entrada por importa��o de conta telef�nica
  (*Lei*) VAR_FATID_0071 = 0071; // Entrada por consumo por leitura (empresa)
  (*Nfe*) VAR_FATID_0099 = 0099; // Avulso (Manual)
  (*PQx*) VAR_FATID_0100 = 0100; // Apenas usado para separar Entradas de sa�das!!!!!!!!!!!!!!!!
  //
  (*Nfe*) VAR_FATID_0101 = 0101; // Gerado por Classifica��o de WB no Blue derm
  (*Nfe*) VAR_FATID_0102 = 0102; // CMPTOut no BlueDerm
  (*Nfe*) VAR_FATID_0103 = 0103; // MPInn no BlueDerm
  (*Nfe*) VAR_FATID_0104 = 0104; // Baixa por classifica��o de WB no Blue derm
  (*Nfe*) VAR_FATID_0105 = 0105; // Gerado por envio para industrializa��o por terceiros no Blue derm
  (*Nfe*) VAR_FATID_0106 = 0106; // Baixa por envio para industrializa��o por terceiros no Blue derm
  (*Nfe*) VAR_FATID_0107 = 0107; // Gerado por reclassifica��o de WB no Blue derm
  (*Nfe*) VAR_FATID_0108 = 0108; // Baixa por reclassifica��o de WB no Blue derm
  (*PQx*) VAR_FATID_0110 = 0110; // Baixa por pesagem

  (*Nfe*) VAR_FATID_0113 = 0113; // MPinIts (NF normal) no BlueDerm

  (*PQx*) VAR_FATID_0120 = 0120; // Baixa de Uso e Consumo por ajuste no balan�o; // 2023-10-09

  (*PQx*) VAR_FATID_0130 = 0130; // Baixa por dilui��o / mistura
  (*PQx*) VAR_FATID_0150 = 0150; // Baixa de pq no tratamento de efluentes (tabela pqt)
  (*Nfe*) VAR_FATID_0151 = 0151; // Entrada de Uso e consumo por NF modelo normal

  (*PQx*) VAR_FATID_0170 = 0170; // Baixa de pq (Devolu��o > tabela pqd)
  (*PQx*) VAR_FATID_0180 = 0180; // Baixa de IPI
  (*PQx*) VAR_FATID_0185 = 0185; // Baixa de Material de Manuten��o

  (*PQx*) VAR_FATID_0190 = 0190; // Baixa de pq (Outros > tabela pqo)

  (*Lct*) VAR_FATID_0201 = 0201; // FatID generico para Lct de faturamento mensal de servicos de NFS-e (NFSe)
  (*Lct*) VAR_FATID_0202 = 0202; // FatID generico para Lct de faturamento mensal de servicos de NFS-e (NFSe) atrav�s de boletos

  (*Nfe*) VAR_FATID_0213 = 0213; // MPinIts (SPED EFD) no BlueDerm

  (*Nfe*) VAR_FATID_0251 = 0251; // Entrada de Uso e consumo por importacao de SPED EFD

{
Entradas
301 -     : Cheques e duplicatas comprados

Sa�das
302 -     : Pago ao cliente
371 -     : Somas das entradas a prazo

Anal�tico das entradas que geram o 371 
303 -     : Taxas cobradas 
304 -     : Ocorr�ncias cobradas
305 -     : Recebimento de cheques devolvidos
306 -     : Recebimento de duplicatas vencidas
307 -       L I V R E (usar para outra entrada)
322 -     : Ad Valorem
333 -     : Fator de compra
341 - 349 : Impostos
35x -       L I V R E (usar para outros tipos de cobran�a ou impostos)
361 - 362 : Sobras ou faltas
  //
  // Colocar no MyListas:
  VAR_FATID_MIN = 300; TXT_VAR_FATID_MIN = '300';
  VAR_FATID_MAX = 399; TXT_VAR_FATID_MAX = '399';
}
  (*Lct*) VAR_FATID_0301 = 0301; TXT_VAR_FATID_0301 = '0301'; //  Cred itor > Compra de Direitos
  (*Lct*) VAR_FATID_0302 = 0302; //  Cred itor > Pagamento de Compra de Direitos
  (*Lct*) VAR_FATID_0303 = 0303; TXT_VAR_FATID_0303 = '0303'; //  Cred itor > Cobran�a de Taxas de Compra de Direitos
  (*Lct*) VAR_FATID_0304 = 0304; TXT_VAR_FATID_0304 = '0304'; //  Cred itor > Cobran�a de ocorr�ncia
  (*Lct*) VAR_FATID_0305 = 0305; TXT_VAR_FATID_0305 = '0305'; //  Cred itor > Juros de cheques devolvidos
  (*Lct*) VAR_FATID_0306 = 0306; TXT_VAR_FATID_0306 = '0306'; //  Cred itor > Juros de duplicatas vencidas
  (*Lct*) VAR_FATID_0307 = 0307; TXT_VAR_FATID_0307 = '0307'; //  Cred itor > Desconto no pagamento de cheques devolvidos
  (*Lct*) VAR_FATID_0308 = 0308; TXT_VAR_FATID_0308 = '0308'; //  Cred itor > Desconto no pagamento de duplicatas vencidas

  (*Lct*) VAR_FATID_0311 = 0311; TXT_VAR_FATID_0311 = '0311'; //  Cred itor > Total quita��o de cheque devolvido
  (*Lct*) VAR_FATID_0312 = 0312; TXT_VAR_FATID_0312 = '0312'; //  Cred itor > Total quita��o de duplicata vencida

  (*Lct*) VAR_FATID_0322 = 0322; TXT_VAR_FATID_0322 = '0322'; //  Cred itor > Cobran�a de Ad Valorem

  (*Lct*) VAR_FATID_0333 = 0333; TXT_VAR_FATID_0333 = '0333'; //  Cred itor > Cobran�a de Fator de Compra

  (*Lct*) VAR_FATID_0341 = 0341; TXT_VAR_FATID_0341 = '0341'; //  Cred itor > Cobran�a de IOC
  (*Lct*) VAR_FATID_0342 = 0342; TXT_VAR_FATID_0342 = '0342'; //  Cred itor > Cobran�a de IOFd
  (*Lct*) VAR_FATID_0343 = 0343; TXT_VAR_FATID_0343 = '0343'; //  Cred itor > Cobran�a de IOFv
  (*Lct*) VAR_FATID_0344 = 0344; TXT_VAR_FATID_0344 = '0344'; //  Cred itor > Cobran�a de CPMF
  (*Lct*) VAR_FATID_0345 = 0345; TXT_VAR_FATID_0345 = '0345'; //  Cred itor > Cobran�a de IRRF

  (*Lct*) VAR_FATID_0361 = 0361; TXT_VAR_FATID_0361 = '0361'; //  Cred itor > Sobra de valores no border�
  (*Lct*) VAR_FATID_0362 = 0362; TXT_VAR_FATID_0362 = '0362'; //  Cred itor > Falta de valores no border�

  (*Lct*) VAR_FATID_0365 = 0365; TXT_VAR_FATID_0365 = '0365'; //  Cred itor > Repasse de cheques de terceiros a clientes em border�

  (*Lct*) VAR_FATID_0371 = 0371; TXT_VAR_FATID_0371 = '0371'; //  Cred itor > Compra de Cr�ditos

  (*Lct*) VAR_FATID_0372 = 0372; TXT_VAR_FATID_0372 = '0372'; //  Cred itor > Lan�amentos financeiros de remessas Duplicatas
  (*Lct*) VAR_FATID_0373 = 0373; TXT_VAR_FATID_0373 = '0373'; //  Cred itor > Lan�amentos financeiros de remessas Cheques

  (*Lct*) VAR_FATID_0500 = 0500; //  LeSew/GigaStore -> Pagto compras
  (*Lct*) VAR_FATID_0510 = 0510; //  LeSew/GGigaStore -> Pagto Vendas
  (*Lct*) VAR_FATID_0600 = 0600; //  M�dulo boletos / Syndic / Sinker -> Arrecada��es
  (*Lct*) VAR_FATID_0601 = 0601; //  M�dulo boletos / Syndic / Sinker -> Consumos
  (*Lct*) VAR_FATID_0610 = 0610; //  M�dulo boletos / Syndic / Sinker -> Acertos de d�bitos (Reparcelamento)
  (*Lct*) VAR_FATID_0700 = 0700; //  LeSew -> Transfer�ncias entre contas
  (*Lct*) VAR_FATID_0701 = 0701; //  LeSew -> Despesas professor em viagem (ciclo)
  (*Lct*) VAR_FATID_0702 = 0702; //  LeSew -> Despesas de rateio (ciclo)
  (*Lct*) VAR_FATID_0703 = 0703; //  LeSew -> Despesas promotor (ciclo)
  (*Lct*) VAR_FATID_0704 = 0704; //  LeSew -> Despesas promotor (extras)
  (*Lct*) VAR_FATID_0705 = 0705; //  LeSew -> Pagamento comiss�es professor (extras)
  (*Lct*) VAR_FATID_0706 = 0706; //  LeSew -> Pagamento comiss�es professor (ciclo - autom�tico)
  (*Lct*) VAR_FATID_0711 = 0711; //  LeSew -> Rateio de creditos p/ contas (plano) c/ saldo controlado
  (*Lct*) VAR_FATID_0712 = 0712; //  LeSew -> Reten��o das comiss�es de professores
  (*Lct*) VAR_FATID_0721 = 0721; //  LeSew -> Venda de mercadorias pelo professor em viagem (ciclo de curso)
  (*Lct*) VAR_FATID_0731 = 0731; //  LeSew -> Empr�stimos a professores (Cr�dito e d�bito)
  (*Lct*) VAR_FATID_0750 = 0750; //  LeSew -> transferencias entre carteiras de acerto entre professor e promotor
  (*Lct*) VAR_FATID_0751 = 0751; //  LeSew -> transferencias entre carteiras de pagamento da comiss�o de alunos do professor
  (*Lct*) VAR_FATID_0752 = 0752; //  LeSew -> transferencias entre carteiras de devolu��o da comiss�o de alunos do professor
  (*Lct*) VAR_FATID_0801 = 0801; //  LeSew -> Venda de mercadorias para pontos de venda
  (*Lct*) VAR_FATID_0810 = 0810; //  LeSew -> Venda de lingerie
  (*Lct*) VAR_FATID_0851 = 0851; //  LeSew -> Comiss�o de venda de mercadorias nos pontos de venda
  (*Lct*) VAR_FATID_0901 = 0901; //  DControl -> Servicos
  (*Lct*) VAR_FATID_1001 = 1001; // Compra de PQ (uso e consumo)
  (*Lct*) VAR_FATID_1002 = 1002; // Frete de compra de PQ
  (*Lct*) VAR_FATID_1003 = 1003; // Compra de MP
  (*Lct*) VAR_FATID_1004 = 1004; // Frete de compra de MP
  (*Lct*) VAR_FATID_1005 = 1005; // Compra de PQ PS
  (*Lct*) VAR_FATID_1006 = 1006; // Frete de compra de PQ PS
  (*Lct*) VAR_FATID_1007 = 1007; // Venda de couro verde
  (*Lct*) VAR_FATID_1008 = 1008; // Frete de venda de couro verde
  (*???*) VAR_FATID_1009 = 1009; // Venda de couro V S (V S Out...)
  (*Lct*) VAR_FATID_1010 = 1010; // Comiss�o na compra de MP
  (*Lct*) VAR_FATID_1011 = 1011; // Transferencia de estoque VS

  (*Lct*) VAR_FATID_1013 = 1013; // Faturamento de pedido
  (*Lct*) VAR_FATID_1014 = 1014; // Frete de faturamento de pedido
  (*Lct*) VAR_FATID_1015 = 1015; // Pedido de compra Igap� (Pedido = FatID_Sub)
  (*Lct*) VAR_FATID_1016 = 1016; // Pedido de venda Igap� (Pedido = FatID_Sub)

  (*???*) VAR_FATID_1020 = 1020; // Pagamento de Mao de obra para terceiros

  (*Lct*) VAR_FATID_1030 = 1030; //lan�amentos financeiros de servi�os tomados
  (*Lct*) VAR_FATID_1031 = 1031; //lan�amentos financeiros de pagamento de tributos retidos de servi�os tomados

  (*Lct*) VAR_FATID_1041 = 1041; // Compra de WB/Semi > VSPlCCab
  (*Lct*) VAR_FATID_1042 = 1042; // Frete xde Compra de WB/Semi > VSPlCCab
  (*Lct*) VAR_FATID_1043 = 1043; // Comiss�o de WB/Semi > VSPlCCab

  //
  (*Nfe*) VAR_FATID_1110 = 1110; // Baixa por Pesagem no Blue Derm (Uso e consumo)
  (*Nfe*) VAR_FATID_1111 = 1111; // Baixa por Consumo Extra no Blue Derm (Uso e consumo)
  //


  (*Nfe*) VAR_FATID_1150 = 1150; // Baixa ETE no BlueDerm (Uso e consumo)
  (*Nfe*) VAR_FATID_1170 = 1170; // Devolu��o de Outros no Blue Derm (Uso e consumo)
  (*Nfe*) VAR_FATID_1190 = 1190; // Baixa de Outros no Blue Derm (Uso e consumo)
  //
  (*Nfe*) VAR_FATID_1801 = 1801; // Comiss�es de vendas NFe FatID_Sub > (0=Desconhecido, 1=Faturamento, 2=Recebimento total 3=Recebimento a cada parcela)
  (*Nfe*) VAR_FATID_1901 = 1901; // Frete SPEC EFD  (2101? - mudei 2011-08-20 antes de come�ar a usar!)
  //
  (*Lct*) VAR_FATID_2101 = 2101; // Academy -> Pagamento de Matr�cula
  (*Lct*) VAR_FATID_2102 = 2102; // Academy -> Compra de mercadorias diversas
  (*Lct*) VAR_FATID_2103 = 2103; // Academy -> Transporte de mercadorias compradas
  (*Lct*) VAR_FATID_2105 = 2105; // Academy -> Venda de mercadorias diversas

  (*Lct*) VAR_FATID_3001 = 3001; // ToolRent -> Faturamento parcial/ final de loca��o
  (*Lct*) VAR_FATID_3002 = 3002; // ToolRent -> Faturamento de servi�os (SrvL...)
  //.... Debitos
  (*Lct*) VAR_FATID_3011 = 3011; // ToolRent -> Pagamento de honor�rios (SrvL...)
  // ... Aloca��o para loca��o
  (*Lct*) VAR_FATID_3020 = 3020; // ToolRent -> Transfer�ncia do estoque para disponibiliza��o para loca��o
  (*Lct*) VAR_FATID_3021 = 3021; // ToolRent -> Entrada para loca��o sem transferir do estoque
  (*Lct*) VAR_FATID_3030 = 3030; // ToolRent -> Remo��o da loca��o devolvendo para o estoque
  (*Lct*) VAR_FATID_3031 = 3031; // ToolRent -> Remo��o da loca��o sem devolver para o estoque

  (*Lct*) VAR_FATID_4001 = 4001; // Bugstrol -> Faturamento de servi�o
  (*Nfe*) VAR_FATID_4101 = 4101; // Bugstrol -> Baixa na OS (tabela osfrmrec)
  (*Nfe*) VAR_FATID_4102 = 4102; // Bugstrol -> Baixa na OS (tabela osmomrec)
  (*Nfe*) VAR_FATID_4103 = 4103; // Bugstrol -> Baixa na OS (tabela ospipitspr > adiciona)
  (*Nfe*) VAR_FATID_4104 = 4104; // Bugstrol -> Baixa na OS (tabela ospipitspr > substitui)

  (*Nfe*) VAR_FATID_4201 = 4201; // YROd -> Baixa de Remessa de Consigna��o
  (*Nfe*) VAR_FATID_4202 = 4202; // YROd -> Entrada de Remessa de Consigna��o
  (*Nfe*) VAR_FATID_4203 = 4203; // YROd -> Baixa de Retorno de Consigna��o
  (*Nfe*) VAR_FATID_4204 = 4204; // YROd -> Entrada de Retorno de Consigna��o
  (*Nfe*) VAR_FATID_4205 = 4205; // YROd -> Baixa por Venda de Consignado
  (*Nfe*) VAR_FATID_4206 = 4206; // YROd -> Devolu��o de Venda de Consignado
  (*Lct*) VAR_FATID_4211 = 4211; // YROd -> Frete de Remessa de Consigna��o
  (*Lct*) VAR_FATID_4213 = 4213; // YROd -> Frete de Retorno de Consigna��o
  (*Lct*) VAR_FATID_4215 = 4215; // YROd -> Frete de Venda de Consignado
  (*Lct*) VAR_FATID_4216 = 4216; // YROd -> Frete de Devolu��o de Venda de Consignado
  (*Lct*) VAR_FATID_4217 = 4217; // YROd -> Comiss�o de vendas

  (*CTe*) VAR_FATID_5001 = 5001; // Faturamento de frete

  (*MDFe*) VAR_FATID_6001 = 6001; // Manifesto de Carga
  //
  (*Lct*) VAR_FATID_7001 = 7001; // YROd -> Faturamento parcial/ final de Venda / servi�os

  (*Agenda*) VAR_FATID_8001 = 8001; //Agenda -> Tarefas � partir de lan�amentos financeiros


  (*Nfe*) VAR_FATID_999999999 = 999999999; // Emiss�o manual de NFe
  //////////////////////////////////////////////////////////////////////////////
  //                                                                          //
  //  N�O ESQUECER DE COLOCAR NO:                                             //
  // function TDmNFe_0000.NomeFatID_NFe(FatID: Integer): String;                  //
  //                                                                          //
  //////////////////////////////////////////////////////////////////////////////

  //  Pares de tabelas com o StqMovItsA(B) (Campo ParTipo = Tipo - ParCodi = ID)
  GRADE_TABS_PARTIPO_0000 = 0000 ;  // Sem Par em outra tabela
  GRADE_TABS_PARTIPO_0001 = 0001 ;  // PQX.StqMovIts > StqMovItsA(B).ParCodi
  GRADE_TABS_PARTIPO_0002 = 0002 ;  // MPIn.StqMovIts > StqMovItsA(B).ParCodi
  GRADE_TABS_PARTIPO_0003 = 0003 ;  // Classificado??? > StqMovItsA(B).ParCodi
  GRADE_TABS_PARTIPO_0004 = 0004 ;  // spedefd????.LinArq > StqMovItsA(B).ParCodi (spedefd????.AnoMes > StqMovItsA(B).ParTipo
  GRADE_TABS_PARTIPO_0005 = 0005 ;  // spedefdC100 > PQE.Codigo
  GRADE_TABS_PARTIPO_0006 = 0006 ;  // spedefdC100 > MP?
  GRADE_TABS_PARTIPO_0007 = 0007 ;  // spedefdC100 > NFeCabA
  GRADE_TABS_PARTIPO_0008 = 0007 ;  // Bugstrol. > NFeCabA

  MSG_FORM_SHOW = '...';//'Preencha as informa��es e confirme clicando em "OK", ou clique em "Desiste" para sair sem salvar.';
  MSG_SAVE_XML = 'Salvando informa��es no arquivo XML';
  MSG_SAVE_SQL = 'Salvando informa��es na base de dados';
(*
  {$IFDEF DEFINE_VARLCT}
  VAR_LCT = 'lan'+'ctos';
  {$ELSE}
  //{$IFNDEF DEFINE_VARLCT}
  VAR_LCT = 'lan'+'ctos';
  {$ENDIF}
*)
{
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
    'Controle', FTabLctA, LAN_CTOS, 'Controle');
}
  // FolowStepLct
  CO_STEP_LCT_DELETE = 1; // Exclui lancto
  CO_STEP_LCT_DEVOLU = 2; // Devolve documento > Sit=6
  CO_STEP_LCT_DESDEV = 3; // Desfaz devolu��o de documento
  CO_STEP_LCT_INSPGC = 4; // Inclui pagamento de documento / duplicata
  CO_STEP_LCT_DELPGC = 5; // Exclui pagamento de documenro / duplicata
  // Fim FolowStepLct
  //
  VAR_DOTIMP = 250/90;
  CO_JOKE_SQL = '$#'; // Texto livre no SQLInsUpd
  LCT_MASK = '[$lct]';
  _ID_MASK = '[$_ID]';
  LAN_CTOS = 'lanctos';
  LAN_SDO_INI_FATID = '-999999999';
  // Syndic2
  TAB_ARI = 'arre' + 'its';
  TAB_CNS = 'cons' + 'its';
  TAB_PRI = 'prev' + 'its';
  TAB_PRV = 'prev';
  DELETE_FROM = 'DEL'+'ETE FROM ';
  // Credito2
  TAB_LOT = 'lot' + 'es';
  TAB_LOT_TXS = 'lot' + 'estxs';
  TAB_LOI = 'lot' + 'esits';
  FLD_LOIS = 'LotesIts';
  CO_NFes_XX = '_nfes_vs_';
  //
  VAR_U_H = 'UH';
  VAR_P_R_O_P_R_I_E_T_A_R_I_O = 'Propriet�rio';
  VAR_M_O_R_A_D_O_R = 'Morador';
  VAR_C_O_N_D_O_M_I_N_I_O = 'Condom�nio';
  VAR_U_H_LONGO = 'Unidade habitacional';
  //
                           (*1000 / 37.7953*)
  VAR_frCM = 26.4583162456;//26.4583333333;//24.384;//25.39;//37.7953;
  VAR_POLEGADA = 2.539;
  VAR_PE_QUADRADO = 10.76391;
  VAR_MOEDA_U = 'U$';
  VAR_MOEDA_R = 'R$';
  VAR_MOEDA_E = '�'; //simbolo euro
  VAR_PERIODO_NAO_LOC = 'Per�odo n�o localizado';
  //
  CO_DIR_RAIZ_DMK = 'C:\Dermatek';
  CO_DIR_RAIZ_DMK2 = 'C:/Dermatek';
  VAR_BASE64_PATH = 'C:\Dermatek\Base64\';
  VAR_BASE64_EXE  = 'C:\Dermatek\Base64\Base64.exe';

  // Nota fiscal eletr�nica - NF-e nfe
const
  FMinUF_IBGE   = 11;
  FMaxUF_IBGE   = 99;
  //
  NFE_EMI_VERSAO_MIN_USO = 2.00; // versao minima permitida para emiss�o de NFe
  NFE_EMI_VERSAO_MAX_USO = 3.10; // versao m�xima permitida para emiss�o de NFe
  NFE_EMI_VERSAO_VIGENTE = '4.00'; // versao vigente da NFe // NFE_VERSAO_VIGENTE
  NFE_DES_VERSAO_VIGENTE = '1.01'; // versao vigente da Consulta de NF-es Destinadas
  NFE_DFE_VERSAO_VIGENTE = '1.00'; // versao vigente da DFe das NFe
  //
  NFE_EXT_NFE_XML = '-nfe.xml';
  NFE_EXT_ENV_LOT_XML = '-env-lot.xml';
  NFE_EXT_REC_XML = '-rec.xml';
  NFE_EXT_PED_REC_XML = '-ped-rec.xml';
  NFE_EXT_PRO_REC_XML = '-pro-rec.xml';
  NFE_EXT_DEN_XML = '-den.xml';
  NFE_EXT_PED_CAN_XML = '-ped-can.xml';
  NFE_EXT_CAN_XML = '-can.xml';
  NFE_EXT_PED_INU_XML = '-ped-inu.xml';
  NFE_EXT_INU_XML = '-inu.xml';
  NFE_EXT_PED_SIT_XML = '-ped-sit.xml';
  NFE_EXT_SIT_XML = '-sit.xml';
  NFE_EXT_PED_STA_XML = '-ped-sta.xml';
  NFE_EXT_STA_XML = '-sta.xml';
  NFE_EXT_NFE_WEB_XML = '-recup-web.xml';
  // 2012-03-18
  NFE_EXT_EVE_ENV_LOT_XML = '-eve-env-lot.xml';
  NFE_EXT_EVE_RET_LOT_XML = '-eve-env-ret.xml';
  NFE_EXT_EVE_ENV_CCE_XML = '-eve-cce.xml';
  NFE_EXT_EVE_RET_CCE_XML = '-cce.xml';
  NFE_EXT_EVE_PRO_CCE_XML = '-pro-cce.xml';
  NFE_EXT_EVE_ENV_CAN_XML = '-eve-can.xml';
  NFE_EXT_EVE_RET_CAN_XML = '-can.xml';
  NFE_EXT_EVE_ENV_MDE_XML = '-eve-mde.xml';
  NFE_EXT_EVE_RET_MDE_XML = '-mde.xml';
  NFE_EXT_EVE_ENV_EPEC_XML = '-eve-epec.xml';
  NFE_EXT_EVE_RET_EPEC_XML = '-epec.xml';
  // Fim 2012-03-18
  // 2013-04-29
  //NFE_EXT_CON_NFE_DES_XML = '-con-nfe-des.xml';
  NFE_EXT_RET_NFE_DES_XML = '-ret-nfe-des.xml';
  NFE_EXT_PED_DOW_NFE_DES_XML = '-ped-dow-nfe-des.xml';
  NFE_EXT_RET_DOW_NFE_DES_XML = '-ret-dow-nfe-des.xml';
  NFE_EXT_RET_DOW_NFE_NFE_XML = '-ret-dow-nfe-nfe.xml'; // Salvar com '-nfe.xml'  !!!!
  // Fim 2013-04-29
  // 2014-10-29
  NFE_EXT_PED_DFE_DIS_INT_XML = '-ped-dfe-dis-int.xml';
  NFE_EXT_RET_DFE_DIS_INT_XML = '-ret-dfe-dis-int.xml';
  NFE_EXT_PED_DOW_NFE_CNF_XML = '-ped-dow-nfe-cnf.xml';
  NFE_EXT_RET_DOW_NFE_CNF_XML = '-ret-dow-nfe-cnf.xml';
  NFE_EXT_RET_CNF_NFE_NFE_XML = '-ret-cnf-nfe-nfe.xml'; // Salvar com '-nfe.xml'  !!!!
  // FIM 2014-10-29
  //////////////////////////////////////////////////////////////////////////////
  //
  //NFS-e
  // 2012-09-17
  { Falta fazer!
  NFSE_CAN_NFSE_ENV = '-can-nfse-env.xml';
  NFSE_CON_NFSE_FAIXA_ENV = '-con-nfse-faixa-env.xml';
  NFSE_CON_NFSE_RPS_ENV  = '-con-nfse-rps-env.xml';
  NFSE_CON_NFSE_SERV_PREST_ENV  = '-con-nfse-serv-prest-env.xml';
  NFSE_CON_NFSE_SERV_TOMAD_ENV  = '-con-nfse-serv-tomad-env.xml';
  NFSE_ENV_LOT_RPS_SINC_ENV = '-env-lot-rps-sinc-env.xml';
  }
  //NFSE_GER_NFSE_ENV = '-ger-nfse-env.xml';
  NFSE_DPS_GER = '-dps.xml';
  NFSE_RPS_ENV_LOT = '-env-lot-rps.xml';
  NFSE_RPS_REC_LOT = '-rec-lot-rps.xml';
  //NFSE_SUBS_NFSE_ENV = '-subs-nfse-env.xml';
  // Fim 2012-09-17

  // CTe
  CTE_EMI_VERSAO_MIN_USO = 2.00; // versao minima permitida para emiss�o de CTe
  CTE_EMI_VERSAO_MAX_USO = 2.00; // versao m�xima permitida para emiss�o de CTe
  //
  CTE_EMI_VERSAO_VIGENTE = '2.00'; // versao vigente da CTe
  CTE_DES_VERSAO_VIGENTE = '0.00'; // versao vigente da Consulta de CT-es Destinadas
  CTE_DFE_VERSAO_VIGENTE = '0.00'; // versao vigente da DFe das CTe
  //
  CTE_EXT_CTE_XML     = '-cte.xml';
  CTE_EXT_ENV_LOT_XML = '-envlot.xml';
  CTE_EXT_REC_XML     = '-rec.xml';
  CTE_EXT_PED_REC_XML = '-ped-rec.xml';
  CTE_EXT_PRO_REC_XML = '-pro-rec.xml';
  CTE_EXT_DEN_XML     = '-den.xml';
  CTE_EXT_PED_INU_XML = '-ped-inu.xml';
  CTE_EXT_INU_XML     = '-inu.xml';
  CTE_EXT_PED_SIT_XML = '-ped-sit.xml';
  CTE_EXT_SIT_XML     = '-sit.xml';
  CTE_EXT_PED_STA_XML = '-ped-sta.xml';
  CTE_EXT_STA_XML     = '-sta.xml';
  CTE_EXT_PED_EVE_XML = '-ped-eve.xml';
  CTE_EXT_EVE_XML     = '-eve.xml';
  CTE_EXT_EVE_ENV_CAN_XML =  '-ped-can.xml';
  CTE_EXT_EVE_RET_CAN_XML = '-ret-can.xml';
  //
  CTE_EXT_EVE_ENV_CCE_XML = '-eve-cce.xml';
  CTE_EXT_EVE_RET_CCE_XML = '-ret-cce.xml';
  CTE_EXT_EVE_PRO_CCE_XML = '-pro-cce.xml';
  //
  CTE_EXT_EVE_ENV_EPEC_XML =  '-ped-epec.xml';
  CTE_EXT_EVE_RET_EPEC_XML = '-ret-epec.xml';
  // Usar� ????
  CTE_EXT_RET_CTE_DES_XML = '-ret-cte-des.xml';
  CTE_EXT_PED_DOW_CTE_DES_XML = '-ped-dow-cte-des.xml';
  CTE_EXT_RET_DOW_CTE_DES_XML = '-ret-dow-cte-des.xml';
  CTE_EXT_RET_DOW_CTE_CTE_XML = '-ret-dow-cte-cte.xml'; // Salvar com '-cte.xml'  !!!!
  // Usar� ????
  CTE_EXT_PED_DFE_DIS_INT_XML = '-ped-dfe-dis-int.xml';
  CTE_EXT_RET_DFE_DIS_INT_XML = '-ret-dfe-dis-int.xml';
  CTE_EXT_PED_DOW_CTE_CNF_XML = '-ped-dow-cte-cnf.xml';
  CTE_EXT_RET_DOW_CTE_CNF_XML = '-ret-dow-cte-cnf.xml';
  CTE_EXT_RET_CNF_CTE_CTE_XML = '-ret-cnf-cte-cte.xml'; // Salvar com '-cte.xml'  !!!!

  // FIM Cte

  // MDFe
  MDFE_EMI_VERSAO_MIN_USO = 1.00; // versao minima permitida para emiss�o de MDFe
  MDFE_EMI_VERSAO_MAX_USO = 1.00; // versao m�xima permitida para emiss�o de MDFe
  //
  MDFE_EMI_VERSAO_VIGENTE = '1.00'; // versao vigente da MDFe
  MDFE_DES_VERSAO_VIGENTE = '0.00'; // versao vigente da Consulta de MD-es Destinadas
  MDFE_DFE_VERSAO_VIGENTE = '0.00'; // versao vigente da DFe das MDFe
  //
  MDFE_EXT_MDFE_XML     = '-mdfe.xml';
  MDFE_EXT_ENV_LOT_XML = '-envlot.xml';
  MDFE_EXT_REC_XML     = '-rec.xml';
  MDFE_EXT_PED_REC_XML = '-ped-rec.xml';
  MDFE_EXT_PRO_REC_XML = '-pro-rec.xml';
(*
  MDFE_EXT_DEN_XML     = '-den.xml';
  MDFE_EXT_PED_INU_XML = '-ped-inu.xml';
  MDFE_EXT_INU_XML     = '-inu.xml';
*)
  MDFE_EXT_PED_SIT_XML = '-ped-sit.xml';
  MDFE_EXT_SIT_XML     = '-sit.xml';
  MDFE_EXT_PED_STA_XML = '-ped-sta.xml';
  MDFE_EXT_STA_XML     = '-sta.xml';
  MDFE_EXT_PED_EVE_XML = '-ped-eve.xml';
(*
  MDFE_EXT_EVE_XML     = '-eve.xml';
  MDFE_EXT_EVE_ENV_CAN_XML =  '-ped-can.xml';
  MDFE_EXT_EVE_RET_CAN_XML = '-ret-can.xml';
*)
  //
  MDFE_EXT_ENV_XML         = '-env.xml';
  MDFE_EXT_EVE_ENV_CAN_XML = 'eve-can-env.xml';
  MDFE_EXT_EVE_RET_CAN_XML = 'eve-can-ret.xml';
  MDFE_EXT_EVE_ENV_ENC_XML = 'eve-enc-env.xml';
  MDFE_EXT_EVE_RET_ENC_XML = 'eve-enc-ret.xml';
  MDFE_EXT_EVE_ENV_IDC_XML = 'eve-idc-env.xml';
  MDFE_EXT_EVE_RET_IDC_XML = 'eve-idc-ret.xml';
  //
  MDFE_EXT_RET_DOW_MDFE_MDFE_XML = '-ret-dow-mdfe-mdfe.xml'; // Salvar com '-mdfe.xml'  !!!!
  MDFE_EXT_RET_CNF_MDFE_MDFE_XML = '-ret-cnf-mdfe-mdfe.xml'; // Salvar com '-mdfe.xml'  !!!!
(*
  MDFE_EXT_RET_DOW_MDFE_DES_XML = '-ret-dow-mdfe-des.xml';
  MDFE_EXT_PED_DFE_DIS_INT_XML = '-ped-dfe-dis-int.xml';
  MDFE_EXT_RET_DFE_DIS_INT_XML = '-ret-dfe-dis-int.xml';
  MDFE_EXT_PED_DOW_MDFE_CNF_XML = '-ped-dow-mdfe-cnf.xml';
  MDFE_EXT_RET_DOW_MDFE_CNF_XML = '-ret-dow-mdfe-cnf.xml';
*)
  // FIM MDFe

  //Movido para o UnGrl_Consts CO_RandStrWeb01 => FRandStrWeb01 = 'WXeq6ojTRJC70zBP';
  (*WEB*) CO_WEBID_PROMISSORIA = '435';
  (*WEB*) CO_WEBID_NFELAY      = '1679';//'911' = 3,10
  (*WEB*) CO_WEBID_SPEDEFD     = '438';
  (*WEB*) CO_WEBID_AVATAR      = '440';
  (*WEB*) CO_WEBID_BANCOS      = '608';
  (*WEB*) CO_WEBID_NFEWS       = '924';
  (*WEB*) CO_WEBID_SVG         = '935';
  (*WEB*) CO_WEBID_CTELAY      = '1192';
  (*WEB*) CO_WEBID_SPEDLAY     = '1245';
  //
  VAR_NomeAdvGlowBtn_TempoExec = '_Adv_Glow_Button_Favoritos_';
  VAR_NomeAdvPage_TempoExec = '_Adv_Page_Favoritos_';
  VAR_NomeToolBar_TempoExec = '_Adv_Tool_Bar_Favoritos_';
  //
  VAR_NomeBitBtn_TempoExec = '_Adv_Glow_Button_Favoritos_';
  VAR_NomeTabSheet_TempoExec = '_Adv_Page_Favoritos_';
  VAR_NomePanel_TempoExec = '_Adv_Tool_Bar_Favoritos_';
  //
  TAB_TMP_EMPRESAS = '_empresas_';
  TAB_TMP_INDIPAGS = '_Indi_Pags_';
  TAB_TMP_PLA_CTAS = '_pla_ctas_';
  //
  // tag de contratos (tags de n�veis)
  CO_IniTagNiv = '[TAG_NIV_';
  CO_CONSULTAR_NFE_DEST = 'CONSULTAR NFE DEST';
  CO_DOWNLOAD_NFE_DEST = 'DOWNLOAD NFE';
  //
  // Pe quadrado
  CO_M2toFT2 = 0.09290304;
  CO_FT2toM2 = 1 / 0.09290304;
  //
  //Padrao valor zero campo SQL
  //CO_0_GGXRcl = 0;
var
  VAR_GETCARTBCO: Integer = 0;

  VAR_CAN_CLOSE_FORM_MSG: Boolean = False;
  VAR_CAN_CLOSE_FORM_MAX,
  VAR_CAN_CLOSE_FORM_POS: Integer;
(*
  // Apenas Teste?
  VAR_INFO_FormMsg: Boolean = False;
  VAR_TEXT_FormMsg: WideString;
*)
  // Modulos
  VAR_USA_MODULO_CRO: Boolean = False;
  //
  VAR_ForcaBigIntNeg: Boolean = False;
  VAR_SEQ_ADV_COMPOS: Integer;

  //
  // Confirmados aqui para todos aplicativos:
  //
  VAR_LABEL1: TLabel = nil;
  VAR_LABEL2: TLabel = nil;
  VAR_IN_VERIFI_DB: Boolean = False;
  VAR_SHOW_ALL_SQL_TEXT: Integer = 0;
  VAR_MASK_ALL_SQL_TEXT: String;
  VAR_INF_SPED_EFD_VAL_ZERO: Boolean = False;
  VAR_INF_SPED_EFD_NOME_DESCR_COMPL: Boolean = False;
  //
  VAR_CtrlGeralMyPathsFrx: word = 0;
  VAR_CNPJ_A_CADASTRAR: String = '';
  {$IfDef cAdvToolx} //Berlin
  VAR_AdvToolBarPagerPrincipal: TAdvToolBarPager = nil;
  {$EndIf}
  VAR_PageControlMenuPrincipal: TPageControl = nil;
  VAR_PageControlFormsTabPrincipal: TPageControl = nil;
  VAR_FORM_VerificaSeTabFormExiste_LOCALIZADO: TForm;
  VAR_MULTIPLAS_TAB_LCT: Boolean;
  VAR_CAMBIO_DATA: TDateTime;
  VAR_CAMBIO_USD, VAR_CAMBIO_EUR, VAR_CAMBIO_IDX: Double;
  //
  VAR_HORARIO_DE_VERAO: Double = 0;
  //VAR_DIRETIVAS_COMPILER_VERSION_DEFINIDAS: Boolean = False;
  VAR_AgeCorBgst, VAR_AgeCorAvul: TColor;
  VAR_TEMPO_SHOW_FRX: TDateTime;
  VAR_PORQUE_VERIFICA: String;
  VAR_LAAVISO1, VAR_LAAVISO2: TLabel;
  VAR_LA_PRINCIPAL1, VAR_LA_PRINCIPAL2: TLabel; // FmPrincipal
  VAR_NAO_PERMITE_EXCLUIR_PID_DB: Boolean = False;
  VAR_ORDEM_TXT_POPUP_AGENDA: Integer;
  VAR_CaptionFormOrigem: String;
  VAR_NomeCompoF7: String;
  VAR_TemContratoMensalidade_FldCodigo: String = '';
  VAR_TemContratoMensalidade_FldNome: String = '';
  VAR_TemContratoMensalidade_TabNome: String = '';
  VAR_VISUALIZAR_MSG_TDmkACBrNFSe: Boolean = True;
  //VAR_FECHA_DOCK_FORM: Boolean;
  VAR_USA_IDX_REF_NO_GG1: Boolean = False;
  //Movido para o UnGrl_Consts => VAR_DERMA_AD: byte = 2;
  VAR_FORM_ID: String;
  VAR_FIN_SELFG_000_CLI, VAR_FIN_SELFG_000_FRN: String;
  VAR_LETRA_LCT: Char;
  VAR_NEXT_FRX_IN_DESIGN_MODE: Boolean = False;
  //{$IFDEF DEFINE_VARLCT}
  VAR_LCT: String =  'lanctos';
  VAR_TAB_LCT_SEL: String = '!ERRO!';
  //
  //{$ENDIF}

  VAR_KIND_DEPTO: TKindDepto = kdNenhum; // > antigo: VAR_TEM_UH: Boolean;
  VAR_TIPO_TAB_LCT: Integer = 0;
  VAR_DOWNLOAD_OK: Boolean;
  VAR_DATA_MINIMA: TDateTime;
  VAR_NOME_TAB_CLIINT: String = ''; // Nome da tabela dos CliInt. Ex.: 'cond';
  //VAR_LCT_ENCERRADO: TDateTime;
  VAR_VERIFI_DB_CANCEL: Boolean = False;
  VAR_FL_DataIni, VAR_FL_DataFim: TDateTime;
  VAR_TYPE_LOG: TTypeLog;
  VAR_CliIntUnico: Integer = 0;
  VAR_FilialUnica: Integer = 0;
  VAR_VLOCAL: Boolean;
  VAR_MyPID_DB_NOME, VAR_AllID_DB_NOME, VAR_RV_CEP_DB_NOME: String;
  VAR_MySyncDB_NOME: String = '';
  VAR_NaoReabrirLct, VAR_LIBERA_TODOS_FORMS: Boolean;
  VAR_NOMEBD, VAR_ADMIN: String;
  VAR_VK_13_PRESSED: Boolean;
  // Sal�rios
  VAR_FP_EMPRESA, VAR_FP_FUNCION: String;
  //
  VAR_FPEVENT: Integer;
  VAR_CONTASLNK: Integer;
  VAR_GETDATA: TDateTime;
  VAR_GET_SO_HORA: TTime;
  VAR_GETPERIODO_Ini, VAR_GETPERIODO_Fim: TDateTime;
  VAR_SELCOD, VAR_SELCO2, VAR_GETUSER: Integer;
  VAR_SELNOM, VAR_SELNO2, VAR_SELCODTXT: String;
  VAR_DBPATH: String;
  VAR_PERIODOBAL: String;
  VAR_MYPAGTOSCONFIG: Integer;
  VAR_CARTDEP: Integer;
  VAR_DATARESULT: TDateTime;
  VAR_SLOGAN_FOOTER,
  VAR_LOGONFPATH, VAR_LOGODUPLPATH, VAR_MEULOGOPATH, VAR_LOGOBIG1PATH: String;
  VAR_LOGONFEXISTE, VAR_LOGODUPLEXISTE, VAR_MEULOGOEXISTE, VAR_LOGOBIG1EXISTE: Boolean;
  VAR_RONDOSAFRA, VAR_CANCELA_UPD_COVERSAO_Z: Boolean;
  VAR_CALCULADORA_COMPONENTCLASS: TComponentClass;
  VAR_CALCULADORA_REFERENCE: TComponent;
  VAR_CPF_PESQ, VAR_CUSTOMER_NOME, VAR_CUSTOMER_CNPJ: String;
  VAR_DT_LIB_INI, VAR_DT_LIB_FIM: Integer;
  VAR_RANDNAME: String;
  VAR_SERVIDOR3: Integer;
  VAR_MYSQLSERVERDISPLAYNAME: String;
  VAR_SQLSCRIPT: AnsiString;
  VAR_FORMMUTEX: TForm;
  VAR_BREAKDOWNLOAD,
  VAR_USA_TAG_BITBTN, VAR_NAO_USA_TAG_NEM_NOME: Boolean;
  VAR_TIMERIDLE: TTimer;
  VAR_TIMERIDLEITERVAL: Integer = 180000;
  VAR_TIMEIDLE: Double = 0; //Segundos
  VAR_MAX_TIMEIDLE: Double = 0; //Segundos
  VAR_LOG_TIMEIDLE: Boolean = False; //Mostra tela de login se inativo
  VAR_REPRES_ENTIJUR1: String;
  VAR_IMPRECHEQUE: Integer;
  VAR_IP_LICENCA: String;
  VAR_PORCENTAGEM: Double;
  VAR_IMPCHEQUE: Integer;
  VAR_IMPCHEQUE_PORTA: AnsiString = 'COM2';
  VAR_IMPCH_IP: String = '127.0.0.1';
  VAR_IMPCHPORTA: Integer = 9520;
  VAR_BDsExtras: String;
  VAR_TITULO_IMP_STINGGRID: String;
  VAR_ENTICREDS: Boolean;
  VAR_CANCELA_PQ_SUB: Boolean;
  VAR_DATA_INI_ESCOLHIDA, VAR_DATA_FIN_ESCOLHIDA: TDateTime;
  VAR_LISTA_RATEIO_CONTAS: TStringList;
  VAR_TABVENDAS: Integer;
  VAR_CONNECTIPPROGRESS: TProgressBar;
  //VAR_CONNECTIPSTATICTX: TStaticText;
  VAR_CONNECTIPLABEL1TX, VAR_CONNECTIPLABEL2TX: TLabel;
  //VAR_MYSQLHOSTPORT    : Word; Usar VAR_PORTA
  VAR_APLICATION_TERMINATE: Boolean;
  VAR_REGEDIT_MyCEP: Boolean;
  VAR_IPBACKUP: String;
  VAR_NEWBAL: Integer;
  VAR_MASTER1: TMenuItem;
  {$IfDef cSkinRank} //Berlin
  VAR_SD1: TSkinData;
  {$EndIf}
  VAR_PRODUTOM: Integer;
  VAR_FMPRINCIPALLIBERADO: Integer;
  VAR_NOMEX_DESISTE: boolean;
  VAR_DEF_CLI1, VAR_DEF_CLI2, VAR_DEF_CLI3, VAR_DEF_CLI4,
  VAR_DEF_FOR1, VAR_DEF_FOR2, VAR_DEF_FOR3, VAR_DEF_FOR4,
  VAR_DEF_FOR5, VAR_DEF_FOR6, VAR_DEF_FOR7, VAR_DEF_FOR8,
  VAR_DEF_CORI: Boolean;
  //{$IfNDef VER310} //Berlin
  VAR_QRCARTEIRAS: TmySQLQuery;
  //{$EndIf}
  //{$IFNDEf SemDBLocal},
  TLocDB: String; // Usado para BD de terceiros!!!
  //{$EndIf}
  TPdxDB, TMeuDB, TMeuERP_CDR_DB: String;
  VAR_SKINCONTA: Integer;
  VAR_CORGRUPOXA1,
  VAR_CORGRUPOXA2,
  VAR_CORGRUPOXB1,
  VAR_CORGRUPOXB2,
  VAR_CORGRUPOYA1,
  VAR_CORGRUPOYA2,
  VAR_CORGRUPOZA1,
  VAR_CORGRUPOZA2,
  VAR_CORGRUPOA1,
  VAR_CORGRUPOA2,
  VAR_CORGRUPOB1,
  VAR_CORGRUPOB2,
  VAR_CORGRUPOC1,
  VAR_CORGRUPOC2,
  VAR_CORGRUPOD1,
  VAR_CORGRUPOD2,
  VAR_CORGRUPOZ1,
  VAR_CORGRUPOZ2: Integer;
 //
  VAR_CXEDIT_ICM: Boolean;
  VAR_AVISOSCXAEDIT: Integer;
  VAR_IMPRIMEFMT, VAR_IMPRIMETYP: Integer;
  VAR_SHOW_MASTER_POPUP, VAR_CANCEL_ENTER_TAB: Boolean;
  VAR_GOTOYJUST: Boolean;
  //
  VAR_LOGOFFDE     : TMenuItem;
  VAR_CAD_POPUP    : TPopupMenu;
  VAR_LOC_POPUP    : TPopupMenu;
  VAR_POPUP_ACTIVE_CONTROL: TWinControl;
  VAR_POPUP_ACTIVE_Numero: Boolean = False;
  VAR_POPUP_ACTIVE_FORM: TForm;
  //
  VAR_STDATALICENCA: TStatusPanel;
  VAR_STAVISOS     : TStatusPanel;
  VAR_STTERMINAL   : TStatusPanel;
  VAR_STEMPRESA    : TStatusPanel;
  VAR_SKINUSANDO   : TStatusPanel;
  VAR_STDATABASES  : TStatusPanel;
  VAR_STLOGIN      : TStatusPanel;
  VAR_STIMPCHEQUE  : TStatusPanel;
  // Espec�ficos
  VAR_STSP         : TStatusPanel;
  VAR_STECFNAME    : TStatusPanel;
  ///////////////
  MAR_GOTOTABELA      : String;//= 'Lotes';
  //{$IfNDef VER310} //Berlin
  MAR_GOTOMySQLTABLE  : TmySQLQuery;//= QrLotes;
  //{$EndIf}
  MAR_GOTONEG         : TGoToSignal;//= False;
  MAR_GOTOCAMPO       : String;//= 'OS';
  MAR_GOTONOME        : String;//= CO_NOME;
  //{$IfNDef VER310} //Berlin
  MAR_GOTOMySQLDBNAME : TmySQLDataBase;//= Dmod.MyDB;
  //{$EndIf}
  MAR_GOTOVAR         : Integer;//= 0;
  //
  VAR_GOTOTABELA, VAR_GOTOCAMPO, VAR_GOTONOME, VAR_GOTOEAN13: String;
  //{$IfNDef VER310} //Berlin
  VAR_GOTOMySQLDBNAME, VAR_GOTOMySQLDBNAME2, VAR_MYARQDB: TmySQLDatabase;
  VAR_GOTOMySQLTABLE : TmySQLQuery;
  //VAR_GOTOABSTABLE: TABSQuery; Movido para UnAllERPs_Vars por causa do dmkComp
  //{$EndIf} Berlin
  VAR_GOTOzSQLDBNAME, VAR_GOTOzSQLDBNAME2, VAR_MYzARQDB: TZConnection;
  VAR_GOTOzSQLTABLE : TZQuery;

  VAR_GOTOABSDBNAME: String;
  VAR_GOTONEG: TGoToSignal = gotoPiZ;
  VAR_GOTOVAR: ShortInt;
  VAR_GOTOVAR1: ShortString;
  ///////////////
  (*VAR_SQLy,*) VAR_SQLx, VAR_SQL1, VAR_SQL2, VAR_SQLa : TStrings;
  (*MAR_SQLy,*) MAR_SQLx, MAR_SQL1, MAR_SQL2, MAR_SQLa : TStrings;
  // Endosso
  VAR_LOC_LCTO_TRUE: Boolean;
  VAR_LOC_LCTO_CTRL, VAR_LOC_LCTO_SUB: Integer;
  VAR_LOC_LCTO_DEBI, VAR_LOC_LCTO_CRED: Double;
  // fim endosso;
  VAR_TITULO_FATNUM: String;
  //
  // FIM Confirmados para todos aplicativos:
  //
  VAR_MODELO, VAR_TIPOBJETO: Integer; // s� do Fyxer

  VAR_LTBT_L_M_D_P: Boolean;
  VAR_BDSENHA: String;
  VAR_ANTIGOCONTROL: TObject;
  VAR_CARTA,
  VAR_SALARIO: Integer;
  VAR_CONTASAL, VAR_CONTAVAL: Integer;
  VAR_SAUDACAOE, VAR_SAUDACAOM, VAR_SAUDACAOF, VAR_SAUDACAOA: String[50];
  VAR_NIVER, VAR_NIVERDDA, VAR_NIVERDDD: Integer;
  VAR_PRONOMEE, VAR_PRONOMEM, VAR_PRONOMEF, VAR_PRONOMEA: String[20];
  VAR_FORMFUNDOBMP: String;
  VAR_FORMFUNDOTIPO: Integer;
  VAR_SERVINTERV, VAR_SERVANTECIP, VAR_ADILANCTO: Integer; 
  VAR_HINARIO, VAR_MUSICA, VAR_TRIENIO, VAR_CULTO, VAR_LITURGIA: Integer; 
  VAR_FORMUSATAM, VAR_FORMHEIGHT, VAR_FORMWIDTH, VAR_FORMPIXESQ, VAR_FORMPIXDIR,
  VAR_FORMPIXTOP, VAR_FORMPIXBOT, VAR_FORMFOALT, VAR_FORMUSAPRO, VAR_FORMSLIDES,
  VAR_FORMNEG, VAR_FORMITA, VAR_FORMSUB, VAR_FORMEXT: Integer;
  VAR_FORMFOPRO: Double;
  VAR_SERVSMTP,
  VAR_NOMEMAILOC,
  VAR_MAILOC,
  VAR_MAILCCCEGA,
  VAR_CONEXAODIALUP,
  VAR_DONOMAILOC: String;
  VAR_CORPOMAILOC: String;
  VAR_PLACACARRO, VAR_PADRPLACACAR: String;
  VAR_VENDEOQUE: Integer;  // * Ver em Op��es de VAR_VENDEOQUE
  VAR_COMISSPROD_PERC, VAR_COMISSPROD_EDIT, VAR_COMISSSERV_PERC,
  VAR_COMISSSERV_EDIT: Integer;
  VAR_PAPERSET: Boolean;
  VAR_PAPERTOP, VAR_PAPERLEF, VAR_PAPERWid, VAR_PAPERHEI, VAR_PAPERFCL: Integer;
  VAR_CFOP2003, VAR_TsErrosAlertas: String;
  VAR_FIMPRIME, VAR_UNIDADE: Integer; 
  VAR_USACARTAOPONTO: Integer;
  VAR_MOSTRAAVISOSPROGRAMADOR: Boolean;
  VAR_FATORCRYPT: Integer; 
  VAR_NOMEEMPRESA, VAR_CNPJEMPRESA, VAR_NOMEDONO, VAR_CNPJDONO: String;
  VAR_FAZERBACKUP: Boolean;
  VAR_TIPOSPRODM_TXT: String;
  //
  VAR_MIDIA, VAR_MOTIVOF, VAR_MOTIVOE, VAR_GRUPOF, VAR_GRUPOE, VAR_ATOR,
  VAR_GENERO, VAR_PRODUTORA, VAR_DIRETOR, VAR_TICKET, VAR_PRECO,
  VAR_PESQENTIDADE: Integer;
  VAR_MYSQLBASEDIR, VAR_MYSQLDATADIR, VAR_ENTISTRING: String;
  VAR_BDNET, VAR_BDLOC: String;
  VAR_CEP: String;
  VAR_MP, VAR_NOMECOR, VAR_LINHA, VAR_CLASSE: Integer;
  //VAR_ESTQQ, VAR_ESTQV: Double;
  //Inicio Aparencia
  VAR_USARAPARENCIA: Boolean;
  VAR_CAMINHOTXTBMP: String;
  VAR_IDTXTBMP: Integer;
  VAR_CAMINHOSKINPADRAO: String;
  VAR_USAVCLSKIN: Boolean;
  VAR_COR_TIT_A, VAR_COR_TIT_B, VAR_COR_TIT_C, VAR_COR_AVISO_A, VAR_COR_AVISO_C: Integer;
  VAR_COR_POS_A_X, VAR_COR_POS_A_Y, VAR_COR_POS_B_X, VAR_COR_POS_B_Y: Integer;
  //Fim Aparencias
  VAR_IMAGEMFUNDO: String;
  VAR_AVISOIMAGEMFUNDO: Boolean;
  //============================================================================
  VAR_CONTROLAESTOQUE, VAR_CANCELA: Boolean;
  VAR_MOEDA, VAR_CIDADEPADRAO: String;
  VAR_TRAVACIDADE: Integer;
  VAR_MOEDAVAL: Double;
  VAR_TELA1, VAR_CHAMARPGTOSERV: Integer;
  VAR_VAPTINI, VAR_VAPTFIM, VAR_VAPTTEMPO: TTime;
  VAR_VAPTDESCO: Double;
  VAR_VAPTCOMBI, VAR_LOCPROMO, VAR_SINCROLIMITE: Integer;
  VAR_PROMOAPLIC: Integer;
  VAR_PROMOMANUAIS, VAR_PROMOCLUBES, VAR_PROMOVAPTS, VAR_DDCORRIDO,
  VAR_PROMOLIMITA: String[1];
  VAR_LOCTITULO, VAR_LOCRODAPE, VAR_LOCCONTRATO: String;
  VAR_LOCLINHASEJET, VAR_LOCCOLUNASIMP, VAR_TARIFACAO,
  VAR_CARTEIRADESPESAS, VAR_CARTTIPODESPESAS, VAR_TIPOATENDE,
  VAR_MUDACORCOMP: Integer;
  VAR_CONTVEN, VAR_CONTCOM, 
  VAR_CARTVEN, VAR_CARTCOM, VAR_CARTRES, VAR_CARTDES, VAR_CARTREG, VAR_CARTDEG,
  VAR_CARTCOE, VAR_CARTCOC, VAR_CARTEMD, VAR_CARTEMA, VAR_SERVICOM: Integer;
  VAR_MAXIADIANT, VAR_EXTRAPEND, VAR_COMISSLOC, VAR_COMISSDEV, VAR_COMISSPRO,
  VAR_COMISSTIC, VAR_COMISSPEN, VAR_COMISSADI, VAR_COMISSIND: Double;
  //

  VAR_LIMICRED: Double;
  VAR_MUDOUENTIDADES: Boolean;
  VAR_ASSISTIU: String[1];
  VAR_LANCTOCONDICIONAL1: Boolean;
  //
  VAR_UFPADRAO: Integer;
  //VAR_MYSQLHOST, VAR_MYSQLUSER: String;
  VAR_SQLHOST, VAR_SQLUSER: String;
  VAR_BACKUPDIR, VAR_RESTOREDIR: String;
  //VAR_BDCASHIER, VAR_BDCASHLOC: String; TMeuDB + TLocDB
  APP_LIBERADO: Boolean;
  VAR_APARENCIA: Integer;
  VAR_CONFIPAGE, VAR_CONFIETIQ: Integer;
  //
  VAR_TERCEIR2, VAR_CLIENTE1, VAR_CLIENTE2, VAR_CLIENTE3, VAR_CLIENTE4,
  VAR_FORNECE1, VAR_FORNECE2, VAR_FORNECE3, VAR_FORNECE4, VAR_FORNECE5,
  VAR_FORNECE6, VAR_FORNECE7, VAR_FORNECE8, VAR_QUANTI1NOME: String;
  VAR_EAN13: String[14];
  VAR_EAN13_INT: Integer;
  VAR_BDFINANCAS: String;
  //{$IfNDef VER310} //Berlin
  VAR_MyBDFINANCAS: TMySQLDataBase;
  //{$EndIf}
  VAR_FATPARCELA: Integer;
  VAR_PGTOVALOR: Double;
  VAR_ODBC: String;
  VAR_MYSQLPATH: String;
  VAR_CURSOR: TCursor;
  VAR_ACTIVEPAGE: Integer;
  VAR_EQUIPAMENTO, VAR_MARCAEQUIP, VAR_MODELOEQUIP, VAR_RAT, VAR_SERVICO,
  VAR_DESPESA, VAR_PRODUTO, VAR_PRODUTOG, VAR_DEFEITO,
  VAR_ENTIGRUPOS, VAR_COR, VAR_GRACOR, VAR_TAMANHO, VAR_SERVICOG: Integer;
  VAR_COLECAO, VAR_DEPTO, VAR_ARTIGO, VAR_MARCA, VAR_MARCACAR, VAR_MODELOCAR,
  VAR_TARIFA, VAR_CADASTRO, VAR_CADASTRO2, VAR_CADASTRO3, VAR_CAD_ITEM,
  VAR_ANT_SEL_CADASTRO: Integer;
  VAR_ANT_SEL_CAD_NOME, VAR_CADASTROX, VAR_CAD_NOME: String;
  VAR_CADTEXTO, VAR_REFEREN, VAR_LIVRES, VAR_DB: String; // [10]

  //Impressora Fiscal
  VAR_IF_DATA: TDate;
  VAR_IF_HORA: TTime;
  VAR_IMPDEF : Boolean;
  //EAN13
  VAR_PRODCOD: String[14];

  //Glyph_Imprime: TBitMap;
  VAR_LISTBOX1: TListBox;
  VAR_WINPATH : Array[0..144] of Char;

  VAR_PESANORMA, VAR_PESAMANUA, VAR_PESABALAN, VAR_PESACOMPA, VAR_PESAEXPER,
  VAR_PESAMISTU, VAR_PESAENTRA, VAR_PESACONSI, VAR_PESAPROVI: Integer;

  // ACESSO A FORMS:
  FM_MASTER : String[1];
  VAR_DESCMAX, VAR_DESCMAX2: Double;

  VAR_CLIENTEC, VAR_CLIENTEI, VAR_FORNECEI, VAR_FORNECEV, VAR_FORNECEF,
  VAR_FORNECET: String[1];
  VAR_PARAR: Boolean;
  VAR_APPTERMINATE: Boolean;
  VAR_VERSAO: Int64;

  VAR_ORDENAR: Integer;
  VAR_ETE, VAR_ACA, VAR_CAL, VAR_CUR, VAR_REC, VAR_IND: Integer;

  VAR_ECF_IF: Integer;
  VAR_VALORSENHA: Double;
  VAR_CLIENTE, VAR_ENTIDADE, VAR_TRANSPOR: Integer;
  VAR_ENTIDADENOME: String;
  VAR_CAIXA, VAR_TRANSACAO: Integer;
  VAR_VENDEDOR: Integer;
  VAR_CAMPOTRANSPORTADORA: String;
  
  VAR_DBNAME: String;
  VAR_CALCPRESS: Boolean;
  VAR_CALCFLOAT: Double;
  VAR_CALCTEXTO: String;
  
  VAR_TESTE : Boolean;  // S� em tempo de cria��o
  VAR_REL_EXTRATO : Boolean;
  VAR_REL_SALDO : Boolean;
  VAR_REL_AJUDA1 : Boolean;
  VAR_BALDATE : TDateTime;
  VAR_CXADATE : TDateTime;

  //VAR_FATID : Integer;
  VAR_FATIDTXT : Integer;
  //VAR_DROPPED : Boolean;
  VAR_CALC_EMP : Boolean;
  VAR_CALC_DIV : Boolean;
  VAR_EDITAR : Boolean;
  VAR_EDC_OK : Boolean;  // O emprestimo ou d�vida ser� editado
  VAR_LOOPEMPITS : Boolean;
  VAR_LOOPDIVITS : Boolean;
  VAR_DEL_EM_TUDO : Boolean;
  VAR_ID_FORM : Integer;
  VAR_TRASACTION_INFLACAO : Boolean;
  VAR_TRASACTION_CONTATOS : Boolean;
  VAR_NUMEDITEMP : Integer;
  VAR_TIPOEMP : Integer;
  VAR_TIPOACAO : Integer;
  VAR_MAISQ252 : Boolean;
  VAR_INFLACALC : Boolean;
  VAR_PANEL : Integer;
  //  2012-09-02
  //VAR_IMAGE, VAR_IMAGE2, VAR_IMAGE3 : String;
  VAR_LOCATE000  : Double;
  VAR_LOCATE001  : Integer;
  VAR_LOCATE002  : Integer;
  VAR_LOCATE003  : TDateTime;
  VAR_LOCATE004  : Integer;
  VAR_LOCATE005  : Integer;
  VAR_LOCATE006  : Integer;
  VAR_LOCATE007  : Double;
  VAR_LOCATE008  : Double;
  VAR_LOCATE009  : Double;
  VAR_LOCATE011  : Double;
  VAR_LOCATE012  : String;
  VAR_LOCATE013  : String;
  VAR_LOCATE014  : Integer;
  VAR_LOCATE015  : Double;
  VAR_LOCATE016  : Double;
  VAR_LOCATE060  : LARGE_INTEGER;
  VAR_CONSIGCANCEL : Boolean;
  VAR_TIPOMSG : String;
  VAR_ID_IMPORT : String;
  VAR_AJUDA : Boolean;
  VAR_INC_AJUDA : Boolean;
  VAR_INCLUI_STATE : Boolean;
  VAR_STRTEMPO : String;
  VAR_TEMPO : TDateTime;
  VAR_CARGA : Boolean;
  VAR_APP, VAR_APPNAME, VAR_SKIN, VAR_SKIN2, VAR_SKINBD : String;
  VAR_SKINALL: String;
  VAR_ASKMSGVISIBLE : Boolean;
  VAR_INTRANSACTION : Boolean;
  VAR_IMPORTCONTA : Integer;
  VAR_IMPORTANDO, VAR_FATURANDO : Boolean;
  VAR_RESERVAIMPORT : Boolean;
  VAR_FILEEXTRATO : Word;
  VAR_EMISSFATURA : Integer;
  VAR_EMPRESTANDO : String;
  VAR_ENDIVIDANDO : String;
  VAR_CONTROLCONTATO : Integer;
  VAR_INFLACAOSTATE : String;
  VAR_COR_EDIT : Integer;
  VAR_COR_RED : Integer;
  VAR_CONTAGENXLS : Integer;
  VAR_EXPKM : Boolean;
  VAR_KMINI, VAR_KMFIM, VAR_KMKM, VAR_KML, VAR_MKMKM, VAR_MKML: Double;
  VAR_SALDOTOTAL : Double;
  VAR_SALDOVENCIDO : Double;
  VAR_SALDOABERTO : Double;
  VAR_SALDOTTOTAL : Double;
  VAR_SALDOTVENCIDO : Double;
  VAR_SALDOTABERTO : Double;
  VAR_SALDOANT : Double;
  VAR_NOMEARQOPEN : String;
  VAR_LIST : Byte;
  VAR_FORM : Integer;
  VAR_NOMEFORMIMP, VAR_NOMEFORMIMP_ANCESTRAL : ShortString;
  VAR_CONTROLEATIVO : ShortString;
  VAR_DATACAIXA : TDateTime;
  VAR_FORMCAIXA : Boolean;
  VAR_DUPLICATA : Byte;
  VAR_CODPRESTAR : Integer;
  VAR_CXAGERALFIM : Byte;
  VAR_TEXTOCXASAIDA : String;
  VAR_TOTALCXA0 : Double;
  VAR_TOTALCXA1 : Double;
  VAR_TOTALCXA2 : Double;
  VAR_CONFENCERR : Boolean;
  VAR_ANTECIPABALANCETE : Boolean;
  VAR_BALDATAINI, VAR_BALDATAFIM : String;
  VAR_CONSULTACAIXA : Boolean;
  VAR_DELVENDAPROD : Boolean;
  VAR_USARSDOINI : Boolean;
  VAR_EDITALMC : Boolean;
  VAR_TERMINATE : Boolean;
  VAR_NAOBAIXOU : Boolean;
  VAR_NAOCRIAFATURA : Boolean;
  VAR_FATURA : Boolean;
  VAR_EXTRATO : Boolean;
  VAR_MSEXTRATO : Byte;
  VAR_IDOK : Boolean;
  VAR_GENHIST : Boolean;
  VAR_ALIASPATH : String;
  VAR_ALIASNAME, VAR_ALIASNAME2, VAR_ALIASLOCAL : String;
  VAR_VALSOMA : Integer;
  VAR_SOMAUSER : Integer;
  VAR_ENTRAPEDIDO : Boolean;
  VAR_TERMINAL, VAR_PORTA: Integer;
  VAR_IP, VAR_ESTEIP, VAR_USERBD, VAR_PASSWORD, VAR_TRUEPASSWORD : String;
  VAR_BDTYPE : String;
  VAR_EMITIDO2 : Boolean;
  VAR_CALC : Byte;
  VAR_KMINSERT : Boolean;
  VAR_KMMEDIAJAPOST : Boolean;
  VAR_COUNTEMISSCONTA : Integer;
  VAR_EDITVENDA : Boolean;

  //VAR_APPTERMINATE : Boolean; AtualizaVersao -> Function Boolean
  VAR_SENHA, VAR_SENHATXT : String;
  VAR_LIB_ARRAY_EMPRESAS_LISTA: array of integer;
  VAR_LIB_ARRAY_EMPRESAS_CONTA: integer;
  VAR_LIB_EMPRESA_SEL: Integer;
  VAR_LIB_EMPRESAS, VAR_LIB_FILIAIS, VAR_LIB_EMPRESAS_NOME: String;
  VAR_LIB_EMPRESA_SEL_TXT: String; // para info no form principal!
  // N�o usar mais
  //VAR_EMPRESA, VAR_FIRMA: Integer;
  //
  VAR_BOSS, VAR_LOGIN, VAR_BOSSLOGIN, VAR_BOSSSENHA : String;
  VAR_USUARIO, VAR_PERFIL, VAR_INDICESENHA, VAR_FSENHA, VAR_FUNCIONARIO, VAR_FUNCILOGIN,
  VAR_INSERIDOR, VAR_IDServr: Integer;
  VAR_TIPOVARCLI: String;
  VAR_SENHARESULT : ShortInt;
  VAR_TINTASCORESPESOCOR1, VAR_TINTASCORESPESOCOR2 : Double;
  VAR_TINTASCORESPESOCOR3, VAR_TINTASCORESPESOCOR4 : Double;
  VAR_TINTASCORESPESOCOR5, VAR_TINTASCORESPESOCOR6 : Double;
  VAR_TINTASCORESCCOR1, VAR_TINTASCORESCCOR2 : Double;
  VAR_TINTASCORESCCOR3, VAR_TINTASCORESCCOR4 : Double;
  VAR_TINTASCORESCCOR5, VAR_TINTASCORESCCOR6 : Double;
  VAR_CORES2CUSTO1, VAR_CORES2CUSTO2 : Double;
  VAR_CORES2CUSTO3, VAR_CORES2CUSTO4 : Double;
  VAR_CORES2CUSTO5, VAR_CORES2CUSTO6 : Double;

  VAR_PATRIMCOMPRA : Boolean;

  VAR_PRINCINSERT : Boolean;

  VAR_CUSTOCOR1, VAR_USOCOR1 : Double;

  VAR_DATACART, VAR_DATACONT, VAR_DATAEMIS : TDateTime;
  VAR_VALOROPER : Double;
  VAR_CENTRO, VAR_CENTROCONTA, VAR_CENTROCART, VAR_CENTROEMIS : Integer;

  VAR_DIVEDIT, VAR_EMPEDIT : Boolean;
  VAR_TINTASITSCUSTO1, VAR_TINTASITSCUSTO2, VAR_TINTASITSCUSTO3 : Double;
  VAR_TINTASITSCUSTO4, VAR_TINTASITSCUSTO5, VAR_TINTASITSCUSTO6 : Double;
  VAR_BOOLECOR, VAR_BOOLE1000 : String;

  VAR_PESQANALOGO : Integer;

  VAR_RECNO : Integer;

  VAR_SETOR, VAR_CENTROESTQ : ShortString;

  VAR_DIASCOMPRA : Double;
  VAR_SERVIDOR : ShortInt = 0; // 1-Cliente 2-Servidor 3-Ambos 4 ou 5-Cliente (apenas um) que atualiza o BD Servidor quando o servidor n�o possui o aplicativo

  VAR_EMPRESANOME : ShortString;

  VAR_GRANDEZA : ShortInt; // 0=Peso 1=Pecas 2=Area

  VAR_BD, VAR_SERVMONEY: ShortInt;
  VAR_BDMONEY: String;


 // VAR_ATUALIZAVERSAO : Boolean; AtualizaVersao -> Function Boolean
  VAR_EXEC_CURINGA: Boolean = False;
  VAR_TextoPesq_CURINGA: String = '';
  VAR_CODUSU: Integer; // Usado em Curinga
  VAR_TXTUSU: String;  // Usado em Curinga
  VAR_CODIGO: Integer; // Usado em Curinga
  VAR_CODIGO2: Double; // Usado em Curinga
  VAR_CODIGO3: TDate;  // Usado em Curinga
  VAR_CODIGO4: String; // Usado em Curinga
  VAR_CODIGO5: Int64; // Usado em Curinga
  VAR_CORDA_CODIGOS: String; // Usado em Curinga
  VAR_NOME_X, VAR_NOME_S: String;  // Usado em Curinga

  VAR_RECVAZIA: Integer;

  VAR_AVISO1 : String;

  VAR_CODPROC: Integer; // Codigo para procura ao criar forms

  VAR_RESPONSAVEL, VAR_FORNOME, VAR_ARTNOME, VAR_MATNOME, VAR_CORNOME: String;
  VAR_LINHAS, VAR_FULAO, VAR_OS, VAR_SP, VAR_OBS1, VAR_DEFPECAS: String;
  VAR_FORNECE, VAR_ARTNO, VAR_MATNO, VAR_CORNO, VAR_SPNO,  VAR_MISTURANO: Integer;
  VAR_CUSTOTON, VAR_CUSTOTOTAL, VAR_CUSTOM2, VAR_CUSTOAGUA: Double;
  VAR_QTDE, VAR_PESO, VAR_PESOMEDIO, VAR_AREAM2: Double;
  VAR_CALCCUSTOS, VAR_CALCCOMPROD, VAR_CALCPESAGEM, VAR_CALCPRECOS: Boolean;
  VAR_CALDEIRA, VAR_SETORNUM, VAR_ESPESSURA: Integer;
  VAR_TABITSREC, VAR_TABITSTIN, VAR_NO_TERC, VAR_HISTORICO: String;
  VAR_MEDIALINHAS, VAR_PESOCALC: Double;
  VAR_gT1, VAR_gT2, VAR_gT3, VAR_gT4, VAR_gT5, VAR_gT6, VAR_COUNTCALCREC: Integer;
  VAR_TINTAN1, VAR_TINTAN2, VAR_TINTAN3, VAR_TINTAN4, VAR_TINTAN5, VAR_TINTAN6: String[11];
  VAR_MEMO1 : AnsiString;

  VAR_COD, VAR_CONTA, VAR_CONTROLE: Integer;
  VAR_CONTA_NOME: String;

  VAR_ARQUIVO: String;

  VAR_ATUALIZANDO, VAR_SEMCONTROLECOR: Boolean;
  //VAR_FILHASABERTAS_1: Integer;

  VAR_FMOUTMSG: Shortint;
  VAR_STRDATAHORA: String;

  VAR_T_L_M_D_EDITNOME: String[50];
  VAR_T_L_M_D_EDITOLD, VAR_T_L_M_D_EDITNEW: String;
  VAR_MPPEMP, VAR_MPPCLI, VAR_MPPART, VAR_MPPESP, VAR_MPPCOR, VAR_MPPCLA : Integer;
//  VAR_MPPDIA: TDate;

  VAR_FONTE, VAR_FONTEBOLD, VAR_FONTEITALIC, VAR_FONTESUBLI: Boolean;
  VAR_FONTESIZE: Integer;
  VAR_FONTENOME, VAR_FONTECOR: String;

  VAR_PQELANCTO: Integer;
  VAR_BAIXADO: Integer;
  VAR_DATAINI, VAR_DATAFIM: ShortString;
  VAR_REABREFORM: Boolean;
  VAR_DUPLICCOD, VAR_DUPLICANT: Integer;
  VAR_LINHARECEITA: Integer;

  // MONEY
  VAR_CARTEIRA: ShortInt;
  VAR_CARTNUM: Integer;
  VAR_LANCTO, VAR_LANCTO2: Int64;
  VAR_TERCEIRO, VAR_INDICE: Integer;
  VAR_INDICEITS, VAR_INDICEITS2, VAR_DATAINSERIR: TDate;
  VAR_EMISSFAT: Integer;
  VAR_IMPLINHA: Integer;
  VAR_VALOREMITIRIMP: Double;
  VAR_QTDBD: Integer;
  VAR_VALOR: Double;
  VAR_GETVALOR_VAL: Double;
  VAR_GETVALOR_GET: Boolean;
  VAR_GETVALOR_TXT: String;
  VAR_GETTEXTO_GET: String; // Edi��o de Texto!
  VAR_GETEAN13_GET: String; // Codigo da barras
  VAR_ALTEROU: Boolean;     //
  VAR_PERIODO: Integer;
  VAR_CONTROLEBIG: Double;
  VAR_GETMINMAXDMK_Min, VAR_GETMINMAXDMK_Max: Double;
  // FIM  MONEY

  //Impress�o
  VAR_CARTOP, VAR_CARLEFT, VAR_IMPLINES: Integer;
  VAR_COMPRESS: Boolean;
  //Fim impress�o

  // MP
  VAR_ARTPED: Integer;
  // Fim MP

  //
  VAR_NOMETABELAMERCADORIA, VAR_NOMECAMPOMERCADORIA: String;
  //
  // Grade
  CO_STQ_CEN_CAD_NULL: Variant;
  // Fim Grade
  VAR_FORMTDI_NAME: String = '';
const
  VAR_IMPSizeAvance = 00;
  VAR_IMPSizeCompri = 08; VAR_WIDSizeCompri = 07; VAR_MESSizeCompri = 07; VAR_HEISizeCompri = 22;
  VAR_IMPSizeNormal = 15; VAR_WIDSizeNormal = 12; VAR_MESSizeNormal = 07; VAR_HEISizeNormal = 32;
  VAR_IMPSizeExpand = 30; VAR_WIDSizeExpand = 24; VAR_MESSizeExpand = 07; VAR_HEISizeExpand = 57;
  CO_POLEGADA       = 2.539;
  CO_AltLetraDOS    = 31; //21
  CO_LINHAS_IMP_DOS = 67;
  CO_CRYPTO1 = 70563;
  CO_CRYPTO2 = 87389;

  CO_FMTFLOAT0 = '#,###,##0';
  CO_FMTFLOAT1 = '#,###,##0.0';
  CO_FMTFLOAT2 = '#,###,##0.00';
  CO_FMTFLOAT3 = '#,###,##0.000';
  CO_FMTFLOAT4 = '#,###,##0.0000';
  CO_FMTFLOAT5 = '#,###,##0.00000';
  CO_FMTFLOAT6 = '#,###,##0.000000';
  //
  CO_FMTFLOAT0_NULL = '#,###,##0;-#,###,##0; ';
  CO_FMTFLOAT1_NULL = '#,###,##0.0;-#,###,##0.0; ';
  CO_FMTFLOAT2_NULL = '#,###,##0.00;-#,###,##0.00; ';
  CO_FMTFLOAT3_NULL = '#,###,##0.000;-#,###,##0.000; ';
  CO_FMTFLOAT4_NULL = '#,###,##0.0000;-#,###,##0.0000; ';
  CO_FMTFLOAT5_NULL = '#,###,##0.00000;-#,###,##0.00000; ';
  CO_FMTFLOAT6_NULL = '#,###,##0.000000;-#,###,##0.000000; ';
  //
  CO_ATE = ' at� ';

  CO_WEB_REGEDITCAM = '\Web\Users';

  // BDs
  CO_PARADOX = 'Paradox';
  CO_MYSQL = 'MySQL';
  // fim BDs

  // Tabelas - Usado para PerguntaAQuery (Localiza��o de grupo de registros)
  CO_ARTIGOSGRUPOS = 'ArtigosGrupos';
  CO_RECEITAS = 'Receitas';
  CO_TINTAS = 'Tintas';
  CO_ANALOGOS = 'Analogos';
  CO_PQ = 'PQ';
  CO_KM = 'km';
  CO_CARTEIRAS = 'Carteiras';  CO_CONTAS = 'Contas';
  CO_SUBGRUPOS = 'Subgrupos';
  CO_GRUPOS = 'Grupos';
  CO_CONJUNTOS = 'Conjuntos';
  CO_PLANO = 'Plano';
  CO_FORNECEDORES = 'Fornecedores';
  CO_CLIENTES = 'Clientes';
  CO_TERCEIROS = 'Terceiros';
  CO_AJCUSTOS = 'Ajcustos';
  CO_AJCUSTOSITS = 'AjcustosIts';
  CO_CONSIGNACOES = 'Consignacoes';
  CO_DIVIDAS = 'Dividas';
  CO_EMPRESTIMOSS = 'Emprestimos';
  CO_INFLACAO = 'Inflacao';
  CO_FATURAS = 'Faturas';
  CO_PESAR = 'Pesar';
  CO_PESAGEM = 'Pesagem';
  CO_PACKLIST = 'PackList';


  CO_MASTER = '31415926536';

  CO_USERSPSEQ = 1;
  CO_USERSPNOW = 'wkljweryhvbirt';
  CO_USERSP001 = 'wkljweryhvbirt';
  CO_LLM_SecureStr = 'oieih3v834';
  CO_CRYPT_CSRT = 'ohn8c6s3ia47j8q3l4';

  CO_INCLUSAO = 'Inclus�o';
  CO_ALTERACAO = 'Altera��o';
  CO_TRAVADO = 'Travado';
  CO_EXCLUSAO = 'Exclus�o';
  CO_IMPRESSAO = 'Impress�o';
  CO_DUPLICACAO = 'Duplica��o';
  CO_DESISTE = 'Desiste';
  CO_ASC  = ' ASC ';
  CO_DESC = ' DESC ';
  // Tamanho 15
  // Sit Lct: Todos
  CO_LIST_SITS_ALL = '0,1,2,3,4,5,6';
  // Sit Lct: At� quitados
  CO_LIST_SITS_OKA = '0,1,2,3';
  // Sit Lct: Parcial ou totalmente quitados
  CO_LIST_SITS_OKB = '1,2,3';
  // Totalmente quitados
  CO_LIST_SITS_OKC = '2,3';
  //
  CO_BLOQUEADO = 'Bloqueado';
  CO_CANCELADO = 'Cancelado';
  CO_DEVOLVIDO = 'Devolvido';
  CO_IMPORTACAO = 'Importa��o';
  CO_PAGTOPARCIAL = 'Pgto parcial';
  CO_QUITADA = 'Quitada';
  CO_VENCIDA = 'Vencida';
  CO_EMABERTO = 'Em aberto';
  CO_COMPENSADA = 'Compensada';
  CO_PREDATADO = 'Pr�-datado';
  CO_QUIT_AUTOM = 'Quit.Autom.';
  CO_REPARCELADO = 'Reparcelado';

  //Tamanho 15
  CO_DOMINGO       = 'Domingo';
  CO_SEGUNDAFEIRA  = 'Segunda-feira';
  CO_TERCAFEIRA    = 'Ter�a-feira';
  CO_QUARTAFEIRA   = 'Quarta-feira';
  CO_QUINTAFEIRA   = 'Quinta-feira';
  CO_SEXTAFEIRA    = 'Sexta-feira';
  CO_SABADO        = 'S�bado';
  //
  CO_JANEIRO       = 'Janeiro';
  CO_FEVEREIRO     = 'Fevereiro';
  CO_MARCO         = 'Mar�o';
  CO_ABRIL         = 'Abril';
  CO_MAIO          = 'Maio';
  CO_JUNHO         = 'Junho';
  CO_JULHO         = 'Julho';
  CO_AGOSTO        = 'Agosto';
  CO_SETEMBRO      = 'Setembro';
  CO_OUTUBRO       = 'Outubro';
  CO_NOVEMBRO      = 'Novembro';
  CO_DEZEMBRO      = 'Dezembro'; // +...
  //
  CO_JANEIRO_UPPER   = 'JANEIRO';
  CO_FEVEREIRO_UPPER = 'FEVEREIRO';
  CO_MARCO_UPPER     = 'MAR�O';
  CO_ABRIL_UPPER     = 'ABRIL';
  CO_MAIO_UPPER      = 'MAIO';
  CO_JUNHO_UPPER     = 'JUNHO';
  CO_JULHO_UPPER     = 'JULHO';
  CO_AGOSTO_UPPER    = 'AGOSTO';
  CO_SETEMBRO_UPPER  = 'SETEMBRO';
  CO_OUTUBRO_UPPER   = 'OUTUBRO';
  CO_NOVEMBRO_UPPER  = 'NOVEMBRO';
  CO_DEZEMBRO_UPPER  = 'DEZEMBRO'; // +...
  //
  CO_DESCONHECIDO  = 'Desconhecido';
  CO_OTIMIZADO = 'Otimizado';
  CO_BALANCO = 'Balan�o';
  CO_PENDENTE = 'Pendente';
  CO_CONFIRMADO = 'Confirmado';


  // Setores
  CO_CALEIRO = 'Caleiro';
  CO_CURTIM = 'Curtimento';
  CO_RECURT = 'Recurtimento';
  CO_ACABTO = 'Acabamento';
  CO_SOBRACA = 'Acabamento (Sobra)';
  CO_ETECALDEIRA = 'ETE e Caldeiras';

  // Centros de Estoque
  CO_BARRACA = 'Barraca';
  CO_DIVISORA = 'Divisora';
  CO_PRECURTIDO = 'Pr�-Curtido';
  CO_CURTIDO = 'Curtido';
  CO_SEMI = 'Semi';
  CO_PREACABADO = 'Pr�-Acabado';
  CO_EXPEDICAO = 'Expedi��o';

  // Tipos de artigo - 20 letras
  CO_FLOR = 'Flor';
  CO_RASPA = 'Raspa';
  CO_SUBPRODUTOS = 'Subprodutos';


  CO_N = sLineBreak;
  CO_FININI = 'Finan�as.ini';
  CO_CREDITO =  'Cr�dito';
  CO_DEBITO = 'D�bito';
  CO_VAZIO = '';
  CO_PONTO = '.';
  CO_COMA = ',';
  CO_ESPACO = ' ';
  CO_VIRGULA = ', ';
  CO_DE = ' de ';
  CO_NUMEAN13 = 'EAN 13';
  CO_NUMERICA = 'Num�rica';
  CO_ALFABETICA = 'Alfab�tica';
  CO_NOME_ENT = 'IF(Tipo=0, RazaoSocial, Nome)';
  CO_NOME = 'Nome';
  CO_CODIGO  = 'Codigo';
  CO_DESCRICAO  = 'Descricao';
  CO_NUMERO  = 'Numero';
  CO_PERIODO = 'Periodo';
  CO_DATA = 'Data';
  CO_EMP = '1';
  CO_DIV = '2';
  CO_TRUE = 'True';
  CO_CARGAINICIAL = 'Carga Inicial';
  CO_FORMATODATA = 'Formato da data';

  CO_IMPOSSCRIAR1 = 'c:\My Documents\Backups';
  CO_DIR1 = 'c:\My Documents';
  CO_AGUA = '�gua';
  CO_RECICLO = 'Reciclo';

  CO_TODOS = 'Todos';

  CO_TempoVAZIO = '';


  CO_1 = '1'; // Limpa C�lculo de empr�stimo para recalcular
  CO_2 = '2'; // Deleta totalmente um empr�stimo
  CO_3 = '3'; // Confirma emprestimo superior a 252 parcelas
  CO_4 = '4'; // Limpa C�lculo de empr�stimo para recalcular
  CO_5 = '5';
  CO_6 = '6';
  CO_7 = '7';
  CO_8 = '8';
  CO_9 = '9';
  CO_10 = '10';
  CO_11 = '11'; // Elimina��o simples em CarteirasIts
  CO_12 = '12'; // Elimina��o simples em ContasIts
  CO_13 = '13'; // Elimina��o simples em Emiss�esIts
  CO_14 = '14'; // Elimina��o simples em EmprestimosIts
  CO_15 = '15'; // Elimina��o simples em EmprestimosPgs
  CO_16 = '16'; // Elimina��o simples em DividasIts
  CO_17 = '17'; // Elimina��o simples em DividasPgs
  CO_18 = '18'; // Eliminacao Simples em Consignacoes (ainda n�o realizado)
  CO_19 = '19'; // Eliminacao Simples em ConsignacoesIts
  CO_20 = '20'; // Eliminacao Simples em Ajcustos
  CO_21 = '21'; // Eliminacao Simples em AjcustosIts
  CO_22 = '22'; // Eliminacao Simples em Faturas
  CO_23 = '23'; // Eliminacao Simples em ImportIts
  CO_24 = '24'; // Eliminacao Simples em FaturasPgs
  CO_25 = '25'; // Desfaz Baixa de Documento em conta (Grid Emissoes)
  CO_26 = '26'; // Eliminacao Simples em �ndices (Infla��o)
  CO_27 = '27'; // Eliminacao Simples em km
  CO_28 = '28'; // Eliminacao Simples em kmmedia
  CO_29 = '29'; // Eliminacao Simples em kmparticular
  CO_30 = '30'; // Devolver Emiss�o em Fatura
  CO_31 = '31'; // Criar ImportEmitido com todos docs abertos
  CO_32 = '32'; // Excluir varios registros em import
  CO_33 = '33'; // Excluir Tranfer�ncia em Carteiras
  CO_34 = '34'; // Excluir Compra de Patrimonio em Carteiras
  CO_35 = '35'; // Excluir Compra de Patrimonio em Contas
  CO_36 = '36'; // Excluir Compra de Patrimonio em Emissoes
  CO_37 = '37'; // Excluir Venda de Patrimonio em Carteiras
  CO_38 = '38'; // Excluir Venda de Patrimonio em Contas
  CO_39 = '39'; // Excluir Venda de Patrimonio em Emissoes
  CO_40 = '40'; // Exclusao total em importa��o (Desist�ncia)
  CO_41 = '41'; // Desfaz D�vida ou Empr�stimo em Importa��o (Desist�ncia)
  CO_42 = '42'; // Desfaz Emiss�o em Importa��o(Desist�ncia)
  CO_43 = '43'; // Desfaz Emitido em Importa��o(Desist�ncia)
  CO_44 = '44'; // Desfaz Consigna��o em Importa��o(Desist�ncia)
  CO_45 = '45'; // Desfaz baixa somente em emiss�o ap�s n�o localizar em conta.
  CO_46 = '46'; // Atribui valor zero para documento em ImportIts.

  CO_50 = '50'; // Localiza Ajuda de Custo duplicada

  CO_60 = '60'; // Confirma carga mais r�pida do aplicativo

  CO_71 = '71'; // Exclus�o do cadastro em carteiras
  CO_72 = '72'; // Exclus�o do cadastro em contas
  CO_73 = '73'; // Exclus�o do cadastro em emissoes
  CO_74 = '74'; // Exclus�o do cadastro em contatos
  CO_75 = '75'; // Exclus�o do cadastro em generos
  CO_76 = '76'; // Exclus�o do cadastro em grupos
  CO_77 = '77'; // Exclus�o do cadastro em empresas
  CO_78 = '78'; // Exclus�o do cadastro em Fornece
  CO_79 = '79'; // Exclus�o do cadastro em Produtos
  CO_80 = '80'; // Exclus�o do cadastro em Grupos de Produtos
  CO_81 = '81'; // Exclus�o do cadastro em Servicos
  CO_82 = '82'; // Exclus�o do cadastro em Turnos
  CO_83 = '83'; // Exclus�o do cadastro em (Prestar) Contas
  CO_84 = '84'; // Exclus�o do cadastro em Caixa Frentista
  CO_85 = '85'; // Exclus�o do bala�o
  CO_86 = '86'; // Exclus�o do ProdIts
  CO_87 = '87'; // Exclus�o de FSaida no caixa geral
  CO_88 = '88'; // Exclus�o de FSaidaIts no caixa geral
  CO_89 = '89'; // Exclus�o de ProdinPG
  CO_90 = '90'; // Exclus�o de Prodin   (Ainda n�o realizado)
  CO_91 = '91'; // Exclus�o de ProdinIT
  CO_92 = '92'; // Exclus�o de ProdinVC
  CO_93 = '93'; // Exclus�o de FSaida em Cxa Geral (CaixaMov)
  CO_94 = '94'; // Exclus�o de Cxa Geral e dependentes
  CO_95 = '95'; // Exclus�o de FVenda em Cxa Frentistas
  CO_96 = '96'; // Exclus�o de FServicos em Cxa Frentista
  CO_97 = '97'; // Exclus�o de FSaida em Cxa Frentista
  CO_98 = '98'; // Exclus�o de FComb em Cxa Frentista
  CO_99 = '99'; // Confirma fechamento caixa frentista
  CO_100 = '100'; // Exclus�o de bombas
  CO_101 = '101'; // Comita��o de transaction pendente
  CO_102 = '102'; // Contagem de G�neros sem endere�o para excel
  CO_103 = '103'; // Abrir arquivo no excel
  CO_104 = '104'; // Erro ao imprimir encerramento cxa geral
  CO_105 = '105'; // Confirma encerramento cxa geral
  CO_106 = '106'; // Faz balancete mesmo antes do fim do mes

  CO_1001 = '1001'; // Backup

  CO_SENHAB = 'aktiengelschaft';


  CO_TRANSPORTADOR   = 'Transportador';
  CO_CLIENTE         = 'Cliente';
  CO_FORNECE         = 'Fornece';
  CO_FORNECEDOR      = 'Fornecedor';
  //
  CO_SETOR2P         = 'Setor: ';
  CO_FORNECEDOR2P    = 'Fornecedor: ';
  CO_TRANSPORTADOR2P = 'Transportador: ';
  CO_ENDERECO2P      = 'Endere�o: ';
  CO_BAIRRO2P        = 'Bairro: ';
  CO_CIDADE2P        = 'Cidade: ';
  CO_UF2P            = 'UF: ';
  CO_CEP2P           = 'CEP: ';
  CO_PAIS2P          = 'Pa�s: ';
  CO_CNPJ2P          = 'CNPJ: ';
  CO_IE2P            = 'I.E.: ';
  CO_TELEFONE2P      = 'Telefone: ';
  CO_FAX2P           = 'Fax: ';
  CO_CONTATO2P       = 'Contato: ';
  CO_DATAENTREGA2P   = 'Data da entrega: ';

  CO_ORDEMCOMPRANUM  = 'Ordem de Compra N� ';
  CO_NOMEPRODUTO       = 'Nome do produto';
  CO_VOLUMES         = 'Vol.';
  CO_EMBALAGEM       = 'Emb.';
  CO_PESOL           = 'Peso L.';
  CO_PESOB           = 'Peso B.';
  CO_PRECO           = 'Pre�o';
  CO_ICMS            = '%ICMS';
  CO_IPI             = '%IPI';
  CO_CUSTO           = 'Custo';
  CO_VALOR           = 'Valor';
  CO_PRAZO           = 'Prazo';
  CO_CUSTOFINANCEIRO = '%CF';
  CO_PEDIDO          = 'Pedido';

  CO_TINTANUM        = 'Tinta n�';
  CO_TOTAL           = 'Total';
  CO_gm2             = 'g/m�';


  // MONEY
  CO_CAIXAS          = 'Caixas';
  CO_BANCOS          = 'Bancos';
  CO_EMISSOES        = 'Emiss�es';
  CO_EMISSS_PAGAR    = 'Emiss�es a Pagar';
  CO_EMISSS_AMBOS    = 'Emiss�es a Pagar e Receber';
  CO_EMISSS_RECEB    = 'Emiss�es a Receber';
  // FIM  MONEY

  CO_DEFNOME         = 'Defina um Nome';
  CO_DEFFLUXO        = 'Defina um Fluxo';

  CO_VENDIDO         = 'Vendido';
  CO_ELIMINADO       = 'Eliminado';
  CO_FURTADO         = 'Furtado';
  CO_EXTRAVIADO      = 'Extraviado';
  CO_DEFEITUOSO      = 'Defeituoso';
  CO_ENCOMENDADO     = 'Encomendado';
  CO_LOCADO          = 'Locado';
  CO_DISPONIVEL      = 'Dispon�vel';
  CO_RESERVADO       = 'Reservado';

  CO_MovimentoAberto = 'MOVIM. ABERTO';


  CO_NAO_RESPONDIDO  = 'N�o Respondido';

implementation

(* Op��es de VAR_VENDEOQUE

  -1: Nada
   0: Mensagem de erro
   1: Produtos
   2: Servi�os
   3: Produtos + Servi�os
*)
end.
